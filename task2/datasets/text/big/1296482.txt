Crossroads (1986 film)
{{Infobox film
| name           = Crossroads
| image          = Crossroadsposter1986.jpg
| caption        = Theatrical release poster Walter Hill
| producer       = Mark Carliner (producer) Mae Woods (associate producer) Tim Zinnemann (executive producer)
| writer         = John Fusco
| starring = {{Plainlist|
* Ralph Macchio
* Joe Seneca
* Jami Gertz
}}
| music          = Ry Cooder Steve Vai John Bailey
| editing        = Freeman A. Davies
| distributor    = Columbia Pictures
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| gross          = $5,839,000 (United&nbsp;States)
}} musical dramatic Walter Hill and features an original score featuring Ry Cooder and Steve Vai on the soundtracks guitar, and harmonica by Sonny Terry. Vai also appears in the film as the devils guitar player in the climactic guitar duel.

Fusco was a traveling blues musician prior to attending New York Universitys Tisch School of the Arts where he wrote Crossroads as a masterclass assignment under screenwriting giants Waldo Salt and Ring Lardner, Jr.. The student screenplay won first place in the national FOCUS Awards (Films of College and University Students) and sold to Columbia Pictures while Fusco was still a student.

== Plot ==
Eugene Martone (Ralph Macchio) is a classical guitar student at the Juilliard School for Performing Arts in New York City who has an obsession for the blues, especially the famed Robert Johnson. Most intriguing are the legends surrounding exactly how Johnson became so talented most notably the one claiming he "sold his soul to the Devil at the crossroads" along with a supposed lost song that Johnson never recorded.
Eugene meets blues musician Willie Brown in an old folks prison and plays him some blues on his guitar. Willie finally admits that he actually is Blind Dog Fulton, bandmate of Robert Johnson. Willie then says he knows the missing Robert Johnson tune in question but refuses to give it to Eugene unless the boy breaks him out of the facility and gets him to Mississippi, where he has unfinished business to settle. Eugene refuses, and Willie says he thought Eugene might be a "Lightnin Boy" but hes just a "chicken ass". Eugene reluctantly agrees to help, and they head to Memphis, Tennessee|Memphis, Tennessee.

Willie tells Eugene how he came to be in prison which ultimately led to his being in the hospital. Upon reaching Memphis, Eugene realizes that Willie has misled him, saying he had more money than he actually has. With only $40 to their name they end up “hoboing” from Memphis to rural Mississippi. "Lightnin Boy" becomes Eugenes blues name.

Shortly after arriving in Mississippi, Willie and Eugene see a train and Willie mimics the train on his harmonica, Eugene tries to do the same on his guitar but fails causing Willie to tell him that hes never going to get Johnsons lost song if he cant make "the Train talk". Eugene jokingly suggests to Willie that he will just do what Willie did and "sell his soul to the Devil at the crossroads", causing Willie to slap him angrily telling to never say that again.

During a small argument between Eugene, Willie scoffs at Eugenes old acoustic guitar, telling him "Muddy Waters invented electricity". The two end up in a pawn shop where Willie barters Eugenes wristwatch for an old Fender Telecaster and a Pignose amplifier which Eugene can hook onto his belt, enabling him to walk around and still play.

While looking for shelter from the rain, the two come across a teen runaway, Frances (Jami Gertz), who is hitchhiking to Los Angeles and has taken shelter in an abandoned house. When she sees the two men, she tries to make them leave by pulling a switchblade knife, but Willie slaps the knife out of her hand. Shortly after she leaves to try finding a ride, and Willie tells Eugene to pack up and follow her, saying that Frances attractiveness will get them more rides than Eugenes thumb.

Later that night after Willie and Eugene have an impromptu   outside a local bar which is broken up by the bar owner. Eugene realizes that Frances is being seduced by the bar owner. His attempt to rescue her leads to a struggle between Eugene and the bar owner, but Willie breaks into the room brandishing a pistol. The trio take the bar owners money and car, telling them that if he calls the cops in the next 24 hours, Frances will press statutory rape charges against the bar owner. The next day Willie takes them to a junkyard where they leave the car.

They seek shelter in an old barn where Frances and Eugene become romantic in a hayloft. However the owner of the barn and two deputy sheriffs discover them and take them to the sheriff. The sheriff allows them to go. Later, the three get a room at a motel with the leftover earnings from the earlier jam session. Willie, Eugene and Frances again get into a minor argument and Willie tells the two to go to the juke joint on the "your side of the road" and hell do his business on "his side of the road" meaning the White side and the Black side of the street but the two get into trouble after Frances steals a mans wallet. After she gives back the wallet the two head across the street where they nearly get in trouble again because of them being white and because of how Eugene is dressed with his guitar like hes ready to play "in a black mans juke" but during the confrontation Willie takes the stage and after getting everyones attention calls for "Lightnin Boy" to be allowed to the stage to perform for them, sparing Eugene from them taking his guitar away and possibly getting beaten. The two play a set that delights the crowd. A patron tells Frances that she recognizes Blind Dog Fulton and that she used to listen to him when she was a little girl.

Later that night Eugene and a drunken Willie get into yet another argument. Willie goes to his bed and passes out having a dream about the Devils Assistant who gave Willie the contract to sign when he made his deal at the Crossroads when he was young.

In the morning, Willie catches Frances with her bags packed and leaving to continue her trip to LA. After a short conversation Willie gives her some money and she leaves. Later when Eugene wakes, he realizes she has left him, breaking his heart. Willie confesses to him that there is no lost song and he only told him that he would teach it to him so Eugene would get him out of the home. He advises Eugene, "Ya gotta do it for yourself, thats what Robert would have told ya."

Now with a true feeling for the blues, Eugene plays his guitar and Willie sits listening nodding in approval acknowledging Eugene "Lightnin Boy" Martone as a true Blues Man.

After visiting a house that used to be a brothel that Willie, Robert and several other blues men frequented and talking to the granddaughter of the woman who owned the brothel, Willie asks her about the Crossroads and if she knew how to get him there. She tells him she knows the Crossroads and gets the men a ride.
 Trans Am Legba and is told that he has changed his name to "Old Scratch|Scratch". Again being offered a ride and turning it down, the car departs. Legba appears, and Willie tells him the contract is void on account that Willie never got what he wanted from the deal, but Legba says that the contract for Willies soul is still valid, even if Willie is ultimately unsatisfied with how his life turned out.

However Legba makes mention of another man who sold his soul named Jack Butler (Steve Vai) who plays guitar in Cutting Heads|head-cutting guitar duels. Eugene reminds Legba that Willie does not play guitar; Legba offers to let Eugene sit in for Willie in the duel. If Eugene wins, Willie gets his soul returned to him, but after asking what he gets if Eugene loses, Eugene puts up his own soul along with Willies despite Willies protests. Willie and Eugene are transported to a music hall, where Jack Butler is playing to a full house of people dancing.

After the performance Willie gives Eugene his   bag just before Legbas Assistant calls him to the stage. Without a word Eugene plugs in his guitar and the duel begins with neither guitarist able to out play the other. The duel seems to come to an end with Eugenes defeat, but Eugene falls back on his classical guitar training and plays a solo that Butler is unable to match, losing the duel. As a victorious Willie and Eugene play for the crowd in the music hall, Legba tears up the contract for Willies soul, freeing him from Hell.

Afterward Willie and Eugene are transported back to Crossroads where they start walking and Willie says he wants to go to Chicago. Eugene agrees to go with him. Willie tells Eugene that after they visit Chicago, hes on his own and that he needs to go on without him and spread the music past where he found it. Blind Dog extends his hand to Lightnin Boy to seal the deal that Lightnin will do that. The two Blues Men shake hands, making their own deal at the Crossroads.

==Cast==
*Ralph Macchio as Eugene Martone
*Joe Seneca as Willie Brown
*Jami Gertz as Frances
*Joe Morton as Scratchs Assistant
*Robert Judd as Scratch
*Steve Vai as Jack Butler
*Dennis Lipscomb as Lloyd
*Harry Carey, Jr. as Bartender John Hancock as Sheriff Tilford
*Allan Arbus as Dr. Santis
*Gretchen Palmer as Beautiful Girl / Dancer
*Al Fann as Pawnbroker
*Wally Taylor as O.Z.
*Tim Russ as Robert Johnson
*Tex Donaldson as John McGraw
*Guy Killum as Willie at 17
*Akosua Busia as Woman at Boardinghouse
*Edward Walsh as Harley Tethune
*Allan Graf as Alvin

==Production==
The script was an original by John Fusco, who had long been interested in blues music. He worked as a blues singer and musician but been warned to rest his vocals by a doctor. In 1981 his girlfriend, who was working at a rest home, told him that an old black man with a harmonica had been admitted. Fusco went to visit him and on the way dreamt up a story about what would happen if the player was a legendary blues player. This gave him the idea for the story. 

Fusco wrote the script as his Masters Thesis at New York University. It was only his second screenplay. Producer Mark Farliner acted as Fuscos independent adviser on it and later helped get it made.  Fusco was paid $250,000. 

Shooting took place on location in Louisiana as well as Hollywood. Blues legend Frank Frost makes a cameo.

"I think the blues still speaks to kids today," said Ry Cooder who did the music. "Its so old that its new." 

The filmmakers shot sad and happy endings and both were tested with audiences; the happy ending was chosen.  (The unhappy ending had Joe Senecas character dying. )

==Awards and nominations==

===Awards===
{|class="wikitable"
!Year
!Event
!Award
!Category
!Nomination
!Result
!Ref.
|- 1986
|Flanders International Film Festival Ghent Georges Delerue Prize Best Original Music Ry Cooder
| 
|  
|}

==Soundtrack==
Ry Cooder said he spent a year working on the soundtrack.   
 

==Response and reviews==
According to Ry Cooder, the film "went down the tubes". 

As of 2012, the film had a 79% certified "fresh" rating on Rotten Tomatoes. 

Roger Ebert in his review stated that the movie "borrows so freely and is a reminder of so many other movies that its a little startling, at the end, to realize how effective the movie is and how original it manages to feel despite all the plunderings." He praised the films acting and music, giving the movie 3.5 stars out of 4. 

==References==
 

== External links ==
 
*  
*  
*  
*   – Arlen Roths page about the shooting of the film

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 