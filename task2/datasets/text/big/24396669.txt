Vanished (2009 film)
{{Infobox Film
| name           = Vanished
| image          = Vanished1.jpg
| image_size     = 175px
| caption        = Theatrical release poster
| director       = Tom Som Matthew Robinson
| writer         = Matthew Baylis
| starring       = Saray Sakana Chea Vannarith
| music          = Simon Etchell Jerry T Jones
| cinematography = Bill Broomfield
| lighting       = In Ath
| editing        = Sao Yoeun
| distributor    = Khmer Mekong Films
| released       =  
| runtime        = 98 minutes
| country        = Cambodia
| language       = Khmer
| budget         = $70,000
}}
Vanished (  directed by Tom Som and starring Saray Sakana and Chea Vannarith. Set in the capital city, Phnom Penh, the movie tells a contemporary murder story. The two main themes deal with trust and the independence of young people in a rigidly hierarchical society. 

Filmed in August and September 2008, with post-production completed in July 2009, this is the second movie from the Cambodian film and TV production company Khmer Mekong Films (KMF). 
 Sorya cinema, Khmer supplemented with English subtitles.

The movie attracted 13,000 within the first 10 days in Phnom Penh, generating critical acclaim in Hollywoods Variety (magazine)|Variety   (the first-ever Variety review of a Cambodian film filed from Cambodia) and Cambodias Phnom Penh Post. 

== Plot==
Radio show presenter Maly deals with the personal problems of young Cambodians on a controversial nightly phone-in show. In quick succession, her co-host and producer are found dead. Maly is terrified her own life is in danger, even under police protection. As the threats mount and she feels increasingly at her wits’ end, the question for her and the audience is – will she crack before the murderer is caught?

== Cast ==
*Saray Sakana - Maly
*Chea Vannarith - Rith
*Pov Kisan - Heng
*Nop Sophorn - Chantha
*Pich Serey Rath - Kim
*Meng Savuth - Leap
*Lim Techmong - Sokhum

==References==
 

== External links ==
*  information on Khmer Mekong Films website


 
 
 
 
 