Pillars of the Sky
{{Infobox film
| name           = Pillars of the Sky
| image          = 
| image_size     = 
| caption        =  George Marshall Robert Arthur
| writer         = Sam Rolfe (screenplay) From the novel Frontier Fury by Will Henry
| narrator       =  Jeff Chandler Dorothy Malone   Maureen Hingert 
| music          = Joseph Gershenson
| cinematography = Harold Lipstein A.S.C.
| editing        = Milton Carruth, American Cinema Editors|A.C.E.
| studio         = Universal
| distributor    = 
| released       = October 12, 1956
| runtime        = 95 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = $1.5 million (US)  
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} George Marshall, Jeff Chandler and Dorothy Malone, with co-stars Ward Bond, Keith Andes and Lee Marvin. 

==Plot== Oregon Country 1868: Indian natives of many tribes trust Sgt. Emmett Bell, who rides into Dr. Joseph Holdens mission with his scouts. But troop and weapon movement by new U.S. Cavalry commanding officer Col. Steadlow has endangered the peace and angered the chiefs, in particular one called Kamiakin.

An outraged Bell tries to appeal to Steadlow as well as Capt. Tom Gaxton, whose wife Calla was once in love with him. Calla and another woman are taken captive and are rescued with Bell, rekinding their romance.

Cavalry soldiers are massacred by the Indians, and appeals for a truce go in vain. But a particularly bloodthirsty act by Kamiakin results in his being killed by one of his own, whereupon Bell and the chiefs agree to do whatevers necessary to restore the peace.

==Cast== Jeff Chandler as First Sergeant Emmett Bell 
*Dorothy Malone as Calla Gaxton

===co-starring===
*Ward Bond as Doctor Joseph Holden
*Keith Andes as Captain Tom Gaxton
*Lee Marvin as Sergeant Lloyd Carracart

===With===
 
 
*Sydney Chaplin as Timothy
*Willis Bouchey as Colonel Edson Stedlow
*Michael Ansara as Kamiakin
*Olive Carey as Mrs. Anne Avery Charles Horvath as Sergeant Dutch Williams Orlando Rodriguez as Malachi
*Glen Kramer as Lieutenant Winston 
 
*Floyd Simmons as Lieutenant Hammond
*Pat Hogan as Jacob
*Felix Noriego as Lucas Paul Smith as Morgan
*Martin Milner as Waco Robert Ellis as Albie
*Ralph J. Votrian as Music
 
*Walter Coy as Major Donahue
*Alberto Morin as Sgt. Major Frenchy Desmonde
*Richard Hale as Isaiah
*Frank de Kova as Zachariah Terry Wilson as Captain Fanning
*Philip Kieffer as Major Randall
*Gilbert Conner as Elijah
 

==Production==
The film was partly shot on location in Oregon over six weeks. At one stage it was announced the film was going to be directed by John Ford and star John Wayne. 

==Evaluation in film guides==
Leonard Maltins Movie Guide gives Pillars of the Sky 2½ stars (out of 4) in a one-sentence write-up which states that "Chandler is apt as swaggering army officer...", with Steven H. Scheuers Movies on TV also arriving at the 2½ stars (out of 4) rating, deciding that "Western fans will buy this tale of a no-account, hard-drinking, woman-chasing Sgt. who finally sees the error of his ways..."

The Motion Picture Guide makes it unanimous (among the three cited sources), with its 2½-star (out of 5) description of the production as "a relatively satisfying cowboys and Indians film starring Chandler as a cavalry scout who is literally a voice in the wilderness..." and, in conclusion, adds, " ood cast. The outdoor location shooting was done in Oregon".

==References==
 

==External links==
* 
* 
* 
* 
*  at TV Guide (1987 write-up was originally published in The Motion Picture Guide)

 

 
 
 
 
 
 
 