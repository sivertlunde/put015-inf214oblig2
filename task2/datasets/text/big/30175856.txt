The Athlete
 
 
{{Infobox film
| name           = The Athlete
| image          = 
| caption        = 
| director       = Davey Frankel Rasselas Lakew
| producer       = Davey Frankel Rasselas Lakew
| writer         = Davey Frankel Rasselas Lakew Mikael Aemiro Awake
| starring       = Rasselas Lakew
| music          = 
| cinematography = 
| editing        = Davey Frankel
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Ethiopia
| language       = Amharic, English, Oromo, Norwegian
| budget         = 
}} Best Foreign Language Film at the 83rd Academy Awards,    but it did not make the final shortlist.    It was the first Ethiopian film to be submitted in the category for Best Foreign Language Film.    The film has been reviewed in an international journal. 
 Rome Olympic Games as a complete unknown. However, the son of a shepherd ran barefoot and won the gold medal. Four years later, he repeated his feat at the Tokyo Olympic Games, becoming the first man to win the Olympic marathon twice in a row. A few years later, he suffered a car accident and lost the use of his legs. He died four years later.

==Cast==
* Rasselas Lakew as Abebe Bikila
* Dag Malmberg as Onni
* Ruta Gedmintas as Charlotte
* Abba Waka Dessalegn as The Priest

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Sub-Saharan African submissions for the Academy Award for Best Foreign Language Film
* Athletics at the 1960 Summer Olympics – Mens marathon

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 