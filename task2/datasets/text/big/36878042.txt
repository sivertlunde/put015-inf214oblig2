Nina, the Flower Girl
{{infobox film
| name           = Nina, the Flower Girl
| image          = Nina, the Flower Girl.jpg
| imagesize      =
| caption        =
| director       = Lloyd Ingraham
| producer       = D. W. Griffith
| writer         = Mary H. OConnor
| starring       = Bessie Love
| music          =
| cinematography = Frank Urson
| editing        =
| distributor    = Triangle Film Corporation
| released       = January 21, 1917
| runtime        = 5 reels
| country        = United States
| language       = Silent (English intertitles)
}} lost 1917 silent drama film produced by D. W. Griffith through his Fine Arts Films and distributed by Triangle Film Corporation. The film starred Bessie Love, an up-and-coming ingenue actress.  It also marked the final acting role for Elmer Clifton, who was by then moving on to directing full-time.

==Cast==
*Bessie Love as Nina
*Elmer Clifton as Jimmie
*Bert Hadley as Fred Townsend
*Loyola OConnor as Mrs. Townsend
*Alfred Paget as Archie Dean
*Fred Warren as Dr. Fletcher
*Adele Clifton as Fifi Chandler
*Rhea Haines as Lotta Jennie Lee as Ninas Grandmother
*Mrs. Higby as Mrs. Hicks

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 