Phaansi
{{Infobox Film
| name           =Phaansi  
| image          = 
| image_size     = 
| caption        = 
| director       = Harmesh Malhotra 
| producer       = 
| writer         = 
| narrator       =  Pran
| music          = Laxmikant Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1978
| runtime        = 
| country        =   India
| language       = Hindi 
| budget         = 
}} Pran and Ranjeet. The films music is by Laxmikant Pyarelal.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Humari Beqarar Si Nazar"
| Lata Mangeshkar
|-
| 2
| "Mil Jate Hai Milne Wale"
| Sulakshana Pandit
|-
| 3
| "Bachchiyan Jawan Te Boodhiya"
| Mohammed Rafi
|-
| 4
| "Jab Aati Hogi Yaad Meri"
| Mohammed Rafi, Sulakshana Pandit
|}
== External links ==
*  

 
 
 
 

 