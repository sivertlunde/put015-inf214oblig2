Dangerously Excited
{{Infobox film
| name           = Dangerously Excited
| image          = Dangerously_Excited_poster.jpg
| film name = {{Film name
 | hangul         =  
 | hanja          = 나는  이다
 | rr             = Naneun Kongmuwonida 
 | mr             = Nanŭn Kongmuwŏnida}}
| director       = Koo Ja-hong
| producer       = Jung Hye-young   Mapofilm
| writer         = Koo Ja-hong
| starring       = Yoon Je-moon   Sung Joon 
| music          = Jang Young-yu
| cinematography = Oh Jae-ho 
| editing        = Kim Woo-il
| distributor    = Next Entertainment World
| released       =  
| runtime        = 101 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =    
}} Udine Far East Film Festival.  

==Plot== Mapo district of Seoul. His department handles various issues like trash, noise, and public safety. Dae-hee isnt married, nor does he have a girlfriend at the moment. He also doesnt have many hobbies. Dae-hee enjoys memorizing facts from books to impress his colleagues and watching television until falling asleep. You can say he is conservative and his job at the city government office suits him well.

Within the Mapo district lies the Hongdae area famous for the thriving indie rock music scene. Dae-hee leaves with a co-worker to talk with a band who has received numerous complaints from neighbors about loud noise in a residential neighborhood.

The band is practicing for their next album which is coming out in the upcoming days. They are disheartened when Dae-hee and his co-worker informs them they have to stop playing in their rented basement. During their talk, a man comes by and offers the band to rent his attic room for a generous price. Dae-hee and his co-worker knows something is fishy, but just go about their business.

The next day, the band discovers their musical instruments and rent money stolen by the con man who offered to rent his apartment. Meanwhile, they become angry at civil worker Dae-hee who introduced them to the con man. They go visit Dae-hee at his office.

To avoid a scene in front of his boss, Dae-hee quickly offers the band his basement for their practice sessions. Dae-hee seems to have little interest in music or the kids. Yet, days later when 2 members quit the band, Dae-hee is recruited to join the band as their bassist...

==Cast==
*Yoon Je-moon - Han Dae-hee
*Sung Joon - Min-ki Kim Byul - Mi-sun Kim Hee-jung - Saku
*Seo Hyun-jung - Young-jin
*Kwon Soo-hyun - Soo
*Oh Gwang-rok - Bob Dylan (cameo) Go Chang-seok - music judge (cameo)
*Park Hae-il - Dae-hees brother (cameo)

==Critical reception==
The Korea Times calls the film "superb. Unlike the entertaining but ultimately forgettable slapstick comedies studios churn out year after year, the movie beautifully blends fun with serious existential problems. The nearly flawless screenplay is further lifted by seasoned actor Yoon Je-moons magnificent performance. His portrayal of Dae-hee is what makes this film work. Not in a single scene does his acting seem forced. His impenetrable face keeps viewers guessing. Behind it, his emotions can appear so simple, yet a perplexing psychological change is taking place. It is simply a pleasure watching this brilliance on screen."  

Koreanfilm.org finds it "wry, funny" and "one of those unexpected pleasures: a film that is able to take a very ordinary situation, and turn it into something vivid, memorable and thoroughly entertaining." Recognizable to many fans of Korean cinema for his supporting roles, Yoon Je-moon as the protagonist is described as "one of the most memorable screen performances of 2012." Koreanfilm.org adds that "one hopes that the films modest budget and lack of star power wont prevent it from reaching the widest possible audience."    

Twitch Film also praised Yoons performance, which "never veers into caricature. Instead, his slight mannerisms and idiosyncrasies add expressive layers to a standard fish-out-of-water character." Despite a few misgivings, the reviewer found the film "pleasant viewing" and that director Koo Ja-hong has "potential as a strong storyteller."  

The Hollywood Reporter was not so positive, finding "little else going on here to make the film as cutting edge as it wants to be." 

==References==
 

==External links==
*    
*    
*  
*  
*  

 
 
 