The Bear That Couldn't Sleep
{{Infobox Hollywood cartoon|
| cartoon_name = The Bear That Couldnt Sleep
| series = Barney Bear
| image =
| caption =
| director = Rudolf Ising
| story_artist =
| animator =
| voice_actor =
| musician =
| producer = Fred Quimby MGM Cartoons
| distributor = Metro-Goldwyn-Mayer (Original) Turner Entertainment (Later via Warner Bros.)
| release_date = June 10, 1939 (USA) Technicolor (two-color)
| runtime = 8:37
| movie_language = English
}}

The Bear That Couldnt Sleep is a 1939 animated short film, directed by Rudolf Ising for MGM as part of Metro-Goldwyn-Mayer Barney Bear series. Released with the movie 6,000 Enemies by MGM on June 10, 1939, the short is notable for featuring the first appearance of Barney Bear. Ising created the character Barney Bear in the late 1930s for MGM at this time, basing the sleepy-eyed character partially on himself.

==Plot==
As autumn draws to a close, and the first gentle blanket of snow covers the great brown forest. Barney Bear happily prepares for his long winters hibernation, but water leaks, a loose shutter, a noisy fire, a teakettle left on, and some stray embers all get in the way and keep him up until spring.

After Barney puts a "Do Not Disturb until Spring" sign on his front door, he locks it and sets his alarm to go off at Spring. When he finally heads to bed, he ties up a leaking root and accidentally bursts his hot water bottle with his sharp claws. Before he is able to cry about it, the leaky root he tied up also bursts, pouring water all over him. Soon afterwards his window bursts open, thanks to the wind, and blows a bunch of snow all over him. Barney finally boards the window up, and tries to go to sleep until the fireplace cracks and makes the kettle whistle. Barney removes the kettle, only to have some coal fly up from the fireplace and down his pants from behind and set his rear end on fire. Barney madly dashes outside and cools his backside down on some snow, but finds he cant get back inside his house because the door gets automatically locked and his window was boarded up from earlier. Barney crashes through his boarded up window and is still insomniac, mainly because of his record player which he destroys in seconds. With Spring closing in, Barney tries counting sheep and finally goes to sleep, only for his alarm clock to go off and looks outside his window to see that Winter is over, and unhappily mutters that "Its Spring!"

==Soundtrack== Lullaby (Cradle Song)
Written by Johannes Brahms  score
* Frühlingslied (Spring Song) Op.62 #6 (1842) Felix Mendelssohn-Bartholdy 
In the score for the spring sign 
Reprised when spring arrives
* My Grandfathers Clock
Written by Henry Clay Work 
Played in the score
* Sleep, Barney, Sleep Jimmie Rodgers 
Played and sung on a record
* Goodnight, Ladies
Written by E. P. Christy
Played in the score

==See also==
*The Rookie Bear
*Bah Wilderness
*Goggle Fishing Bear
*Wee-Willie Wildcat
*Bird-Brain Bird Dog

==External links==
*  
*   at the Big Cartoon Database

 
 
 
 
 


 