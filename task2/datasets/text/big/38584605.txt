Misti Cheler Dustu Buddhi
 
{{Infobox film
| name           = Misti Cheler Dustu Buddhi
| image          = Mishti Cheler Dustu Buddhi.jpg
| alt            = 
| caption        = Movie poster
| director       = Parthasarathi Manna
| producer       = Bidyut Chakraborty
| writer         = Soumitri Shankar Ghosh
| starring       = Abhishek Chatterjee Bhola Tamang Prasun Gain Master Ankit Seth Raju Majumder Indranil Sen Nupur
| music          = Tamal Dodo
| cinematography = R. Khatoi (Kunal)
| editing        = Manikundan Das
| studio         = 
| distributor    = Nupur Creations
| released       =  
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
Misti Cheler Dustu Buddhi (sometimes Mishti Cheler Dustu Buddhi) ( ) is a 2013 Bengali film directed by Parthasarathi Manna and Sudarhan Basu and produced by Bidyut Chakraborty under the banner of Nupur Creations. Music of the film has been composed by Dodo and Tamal. The film was released on January 4, 2013.

==Plot==
Misti Cheler Dustu Buddhi is actually about three thieves, Kalicharan and his two sidekicks, Puti and Baka who decide to rob and go back to their old tricks after Kalicharan is released from jail. They decide to rob the occupants of the Mitra household: an elderly man, Pradosh Mitra, his two sons and their families. They decide to carry out the robbery at a time when the house will have no occupants according to information received by them. Accordingly they act. But they couldnt be successful because, unknown to them, Pradosh and one of his grandsons, 7 year old Som have decided to stay back and create havoc for them. All their efforts to rob the house are in vain as little Som turns out to be more than a handful for all of them. 

==Cast==
* Abhishek Chatterjee
* Bhola Tamang
* Prasun Gayen
* Anjana Das
* Raju Majumdar
* Indranil Sen
* Nupur
* Hiran Chatterjee (special appearance)

==Soundtrack==
The soundtrack of Misti Cheler Dustu Buddhi is composed by Tamal and Dodo. The film has 5 songs which include 4 original songs and 1 remix.

===Tracklist===
{{Track listing extra_column = Singer(s) lyrics_credits = yes title1 = Misti Cheler Dustu Buddhi extra1 = Sidhu lyrics1 = Tamal, Somnath Ghoshal length1 =  title2 = Rupkathar Deshe extra2 = Kinjal, Saptangshu, Tamal lyrics2 = Tamal, Somnath Ghoshal length2 =  title3 = Murgi Chor extra3 = Jojo
|lyrics3 = Tamal, Somnath Ghoshal length3 =  title4 = Kolir Keshto extra4 = Somchanda Bhattacharya, Tamal lyrics4 = Tamal, Somnath Ghoshal length4 =   title5 = Murgi Chor (Remix) extra5 = Diya Das lyrics5 = Tamal, Somnath Ghoshal length5 =  
}}

==See also==
* Maach Mishti & More
* Target Kolkata Deewana
* Shunyo Awnko

==References==
 
 

 
 
 