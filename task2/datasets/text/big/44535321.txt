Rahasya Rathri
{{Infobox film 
| name           = Rahasya Rathri
| image          =  
| caption        = 
| director       = M. S. Kumar
| producer       = R. N. Brothers
| story          = M. S. Kumar
| writer         = Omkara Vani (dialogues)
| screenplay     = M. S. Kumar Vishnuvardhan Bharathi Bharathi Prakash Radha Ravi
| music          = Shankar Ganesh
| cinematography = Annayya Mallik Murugesh
| editing        = E. Arunachalam
| studio         = Janaranjan Pictures
| distributor    = Janaranjan Pictures
| released       =  
| runtime        = 126 min
| country        = India Kannada
}}
 1980 Cinema Indian Kannada Kannada film, directed by M. S. Kumar and produced by R. N. Brothers. The film stars Vishnuvardhan (actor)|Vishnuvardhan, Bharathi Vishnuvardhan|Bharathi, Prakash and Radha Ravi in lead roles. The film had musical score by Shankar Ganesh.  

==Cast==
  Vishnuvardhan
*Bharathi Bharathi
*Prakash
*Radha Ravi
*G. K. Raviraj
*Jeevan
*Shashikanth
*Padmapriya
*Jayamalini
*Halam
*Vani
*Vijayalakshmi
*Baby Preethi Narasimharaju in Guest Appearance Balakrishna in Guest Appearance
*Dinesh in Guest Appearance
*Hanumanthachari in Guest Appearance
*B. Jayashree in Guest Appearance
*B. Jaya (actress)|B. Jaya in Guest Appearance
*V. R. Ravikanth
 

==Soundtrack==
The music was composed by Shankar Ganesh. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Sogasaagide || S. P. Balasubrahmanyam, Vani Jayaram || Vijaya Narasimha || 03.11
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 