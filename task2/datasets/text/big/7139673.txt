The Sign of the Cross (film)
 
{{Infobox film
| name           = The Sign of the Cross
| image          = The sign of cross.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille
| screenplay     = {{Plainlist|
* Waldemar Young
* Sidney Buchman
}}
| based on       =  
| starring       = {{Plainlist|
* Fredric March
* Elissa Landi
* Claudette Colbert
* Charles Laughton
}}
| music          = Rudolph G. Kopp
| cinematography = Karl Struss
| editing        = Anne Bauchens
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         = $650,000
| gross          = 
}}
 original 1895 play by Wilson Barrett.
 Quo Vadis, and like the novel, take place in ancient Rome during the reign of Nero. The art direction and costume design were by Mitchell Leisen who also acted as assistant director. Karl Struss was nominated for an Academy Award for Best Cinematography. 
 The Ten The King of Kings (1927). It was filmed in Fresno, California|Fresno, California.

==Cast==
* Fredric March as Marcus Superbus, prefect of Rome
* Elissa Landi as Mercia Empress Poppaea Emperor Nero
* Ian Keith as Tigellinus
* Arthur Hohl as Titus
* Harry Beresford as Favius Fontelas
* Tommy Conlon as Stephan
* Ferdinand Gottschalk as Glabrio
* Vivian Tobin as Dacia
* William V. Mong as Licinius
* Joyzelle Joyner as Ancaria Richard Alexander as Viturius
* Nat Pendleton as Strabo
* Clarence Burton as Servillius
* Harold Healy as Tybul
* Robert Seiter as Philodemus Charles Middleton as Tyros

==Production notes==
* The famous scene in which Poppaea (Claudette Colbert) bathes in asses milk took several days to shoot. DeMille announced to the press that real asses milk was used; however, it was actually powdered cows milk. After a few days under the hot lights, the milk turned sour, making it very unpleasant for Colbert to work in the stench.   
* To save production expense during the Great Depression, existing sets were reused as well as costumes left over from the making of The Ten Commandments.  DeMille also attempted to provide out-of-work actors jobs as extras such as the crowd arena scenes. 

==Editing for reissue after enforcement of the production code==
 cut from negative for a 1938 reissue, but was restored by Universal Studios Home Entertainment|MCA-Universal for its 1993 video release.  Some gladiatorial combat footage was also cut for the 1938 reissue, as were arena sequences involving naked women being attacked by crocodiles and a gorilla. These were also restored in 1993. 

DeMille himself supervised a new version for its 1944 rerelease. New footage with a World War II setting, featuring actor Stanley Ridges (who did not originally appear in the film) was added to make the film more topical. In the new prologue, a group of planes is seen flying over what was ancient Rome. The conversation of the soldiers in one of the planes leads directly into the films original opening scene. The last few seconds of the edited version of the film showed the planes flying off into the distance, rather than simply fading out on the original closing scene of the movie.
 Universal Pictures, which now owns most pre-1950 Paramount sound features.

==Catholic Legion of Decency== Catholic Legion of Decency, an organization dedicated to identifying and combating objectionable content, from the point of view of the Church, in motion pictures. 

==See also==
*Nudity in film

==References==
{{reflist
| refs =
}}

; Bibliography
* {{cite book
  | last = Birchard
  | first = Robert S.
  | authorlink =
  | year = 2004
  | title = Cecil B. DeMilles Hollywood
  | publisher = University Press of Kentucky
  | isbn = 0-8131-2324-0
  | url = http://books.google.com/?id=YLPTleQHkrUC&printsec=frontcover#v=onepage&q&f=false
  | accessdate = 2011-09-12
  | ref = harv
  }}
* {{cite book
  | last = Black
  | first = Gregory D.
  | authorlink =
  | year = 1996
  | title = Hollywood Censored: Morality Codes, Catholics, and the Movies
  | publisher = Cambridge University Press
  | isbn = 0-521-56592-8
  | url = http://books.google.com/?id=ybKqnNNR7hwC&printsec=frontcover#v=onepage&q&f=false
  | accessdate = 2011-09-12
  | ref = harv
  }}
* {{cite book
  | last = Vieira
  | first = Mark A.
  | authorlink =
  | year = 1999
  | title = Sin in Soft Focus: Pre-Code Hollywood
  | publisher = Harry N. Abrams, Inc
  | location = New York
  | isbn = 0-8109-4475-8
  | ref = harv
  }}

; Online sources

* {{cite web
  | title = The Sign of the Cross
  | publisher = The Kinsey Institute
  | work = Sex in the Cinema
  | url = http://iub.edu/~kinsey/services/gallery/cinema/img.php?i=27
  | accessdate = 2011-09-12
  | ref =  
  }}
* {{cite web
  | last = Landazuri
  | first = Margarita
  | title = The Sign of the Cross
  | publisher = Turner Classic Movies
  | work = tcm.com
  | url = http://www.tcm.com/this-month/article/72483%7C0/The-Sign-of-the-Cross.html
  | accessdate = 2011-09-12
  | ref =  
  }}

==External links==
 
*  
*  
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 