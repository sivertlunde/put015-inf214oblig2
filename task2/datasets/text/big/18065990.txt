The River Niger (film)
 
{{Infobox film
 | name = The River Niger
 | image = The River Niger VideoCover.jpeg
 | image_size = 
 | caption = 
 | director = Krishna Shah Sidney Beckerman Joseph A. Walker (The River Niger|play, screenplay) Teddy Wilson Jerry Goldstein War
 | cinematography = Michael D. Margulies
 | editing = Irving Lerner
 | distributor = Cine Artists Pictures Continental Video Reel Media Intl
 | released = 14 April 1976
 | runtime = 105 min.
 | country = United States
 | language = English
}}
 Joseph A. play of the same title. The film was directed by Krishna Shah, and starred James Earl Jones, Cicely Tyson, and Louis Gossett, Jr. The film had a limited commercial release in 1976 and has rarely been seen in later years.

==Plot==
Johnny Williams (James Earl Jones) is a working house painter and amateur poet who is trying to live in a contemporary ghetto in Watts, Los Angeles|Watts, Los Angeles, California. Though he is trying to provide for his almost stable family, times are hard. Johnnys main pride and joy, his son Jeff (played by Glynn Turman) just returned from U.S. Air Force flight school, where he finally reveals that he flunked out, causing great disillusionment. This film follows Johnnys struggle and a few who try to help, including his physician friend Dr. Dudley Stanton (Louis Gossett, Jr.), who purchases Johnnys poems while treating his ailing wife Mattie (played by Cicely Tyson), whose cancer is recurring. When Johnnys son kills a local gang member, and the gang shoots a police officer, the situation escalates to a standoff with the police and another shootout in Johnnys house. The soundtrack is by War (band)|War, including the theme song "River Niger".

A highlight of the film is Jones sonorous rendition of the poem "The River Niger", which starts: "I am the River Niger, hear my waters." 

Another highlight is the "Stomping Out the Bullshit Scene" by Jones.

==References==
 

==External links==
* 
*  
* 

 
 
 