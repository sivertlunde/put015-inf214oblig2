Barricade (1939 film)
{{Infobox film
| name           = Barricade
| image          = Barricade1939.jpg
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Gregory Ratoff
| producer       = Edward Kaufman
| writer         = Granville Walker
| narrator       = 
| starring       = Alice Faye  Warner Baxter  Charles Winninger  Arthur Treacher   Keye Luke
| music          = David Buttolph
| cinematography = Karl Freund
| editing        = Jack Dennis
| studio         = 20th Century Fox
| distributor    = 
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Barricade is a 1939 adventure film directed by Gregory Ratoff and starring Alice Faye, Warner Baxter, Charles Winninger, Arthur Treacher, and Keye Luke.

==Plot==
A singer named Emmy (Faye) meets broken-down journalist Hank Topping (Baxter) while travelling across Mongolia by train. A romance sparks, but is soon interrupted by a fierce group of murderous bandits. Fleeing, Emmy and Hank team up with others, eventually culminating in a fierce shootout with the marauders.  If I recall correctly as a youngster of ten years, the Emmy and Hank team seek safety in a small fort or an antiquated country home located on barren lands.  As the bandits approach, they hide in a basement level, protected only by a floorboard cover.  As the bandits enter the building, the baby of Emmy and Hank begins to cry, thereby revealing the location of the couple and their team.  As the bandits begin to chop their way through the floorboards, a rescue squad on motorcycles speeds over a nearby hill towards the building, then succeeding to rescue those trapped below the floor.

==Behind the scenes==
*The film was viewed as being so mediocre by 20th Century Fox that it was shelved. A year later, with actress Alice Fayes popularity booming, the film was released to expected sub-par success.

*There were extensive revisions and retakes which eliminated actors J. Edward Bromberg and Joseph Schildkraut from the cast. The song "Therell Be Other Nights" by Lew Brown and Lew Pollack, recorded by Alice Faye, also was cut from the final print.

==Cast==
*Alice Faye as Emmy Jordan
*Warner Baxter	 as Hank Topping
*Charles Winninger	 as Samuel J. Cady
*Arthur Treacher as Upton Ward
*Keye Luke as Ling - Cadys Secretary
*Willie Fung as Yen - Cadys Major Domo
*Doris Lloyd as Mrs. Ward
*Eily Malyon as Mrs. Little - Head of Mission
*Joan Carroll as Winifred Ward (as Joan Carol)
*Leonid Snegoff as Boris - Russian Consul
*Philip Ahn as Col. Wai Kang
*Jonathan Hale	 as Assistant Secretary of State
*Moroni Olsen as Shanghai Managing Editor
*Harry Hayden	as Shanghai Telegraph Manager

==References==
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 

 
 