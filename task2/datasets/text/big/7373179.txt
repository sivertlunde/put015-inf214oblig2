Bedtime Story (film)
{{Infobox Film
| name           = Bedtime Story  
| image          = Bedtimestoryos.jpeg
| caption        = Original film poster
| director       = Ralph Levy
| producer       = Stanley Shapiro
| writer         = Stanley Shapiro   Paul Henning
| starring       = Marlon Brando  David Niven   Shirley Jones   Dody Goodman   Aram Stephan   Parley Baer   Marie Windsor   Rebecca Sand   Frances Robinson
| music          = Hans J. Salter
| cinematography = Clifford Stine
| editing        = Milton Carruth
| distributor    = Universal Pictures
| released       = June 10, 1964
| runtime        = 99 minutes
| country        = United States
| awards         =  English
| budget         = 
| gross = est. $3,000,000 (US/ Canada) 
| preceded_by    = 
| followed_by    = 
}} Robert Arthur as executive producer from a screenplay by Stanley Shapiro and Paul Henning. The music score was by Hans J. Salter and the cinematography by Clifford Stine.   
 Dirty Rotten Scoundrels starring Steve Martin and Michael Caine.

==Plot==
Lawrence Jameson (David Niven) is a refined, elegant con artist who operates in the French Riviera town of Beaulieu-sur-Mer. He masquerades frequently as the deposed prince of a small European country, seducing wealthy women into donating money and jewellery to his revolutionary "cause". Meanwhile, Corporal Freddy Benson (Marlon Brando) is a small-time operator in the US Army stationed in Germany, an expert at conning his way into the hearts (and wallets) of young women with sob stories about his sick grandmother. His attempt at seducing the daughter of a local burgomaster backfires when her father arrives home early, and Freddy is nearly arrested until he blackmails his Colonel into giving him an early discharge.

On a train to Beaulieu-Sur-Mer,  Freddy cockily displays his skill as a con man to Lawrence, whom he believes to be a henpecked husband. Lawrence, believing Freddys "poaching" will endanger his ability to procure bigger targets, decides to ensure Freddy does not set up in Beaulieu-Sur-Mer, first attempting to distract him into leaving town, and then, when that fails, arranging for his arrest. Lawrence also arranges for Freddy to be released and buys him a plane ticket to America. Unfortunately, one of Lawrences former conquests (Dody Goodman) is on the plane, and mistakes Freddy for a revolutionary in the employ of "The Prince". Thus clued-in to Lawrences true identity as a con man, Freddy returns to Beaulieu-Sur-Mer and blackmails Lawrence into taking him on as an apprentice.

Freddy is taught to play The Princes mentally challenged brother Ruprecht, an added tactic to scare women away from marrying the prince. They are successful but quarrel when Lawrence refuses to pay Freddy until he can learn to appreciate the culture necessary for Lawrences style of con. Freddy decides to set out on his own, but since Lawrence believes that there is not enough room in Beaulieu-Sur-Mer for both of them, the two agree to a bet in order to decide who is "King of the Mountain". The first one to steal $25,000 from a selected mark will be allowed to stay.

The two select Janet Walker (Shirley Jones), a naive American heiress, as their target. Freddy poses as a soldier who has suffered psychosomatic paralysis. He wins Janets affections with a sad story about being betrayed by his first love and convinces her that he needs $25,000 to pay for treatment by a celebrated Swedish psychiatrist, Dr. Emile Shauffhausen. As soon as Lawrence discovers this scheme, he masquerades as Dr. Shauffhausen, agreeing to treat Freddys "condition" with the stipulation that Janet pay the $25,000 directly to him. The two set up a war for Janets affections, ruthlessly sabotaging each other...with Lawrence coming out on top, more often than not.

Lawrence discovers that Janet is not wealthy after all, but on vacation as a contest winner for the soap company that employs her, and that she intends to sell off the remainder of her winnings to pay for Freddys treatment. Since he only preys on wealthy women who can afford to lose vast sums of money, Lawrence attempts to call off the bet. Freddy refuses, but suggests that they change the bet to be Janet herself: the first to get her into bed would be the winner. Lawrence refuses to try to seduce Janet, but bets that Freddy will fail to do so.

Freddy has Lawrence kidnapped by some paratroopers he has fooled into thinking Lawrence is trying to steal his girl. In an elaborate routine, he convinces Janet of his love by "conquering" his psychosomatic paralysis and walking. But it is revealed that Lawrence has been present the whole time, and he now declares that Freddy is cured.

Lawrence explains that the soldiers released him after he told them he was a RAF paratrooper during the war, then filled them in on Freddys lies. The soldiers, angry at being deceived, keep Freddy occupied at a party until Lawrence puts Janet on a train. However, just as the train is departing, Janet receives a telegram that the real Dr. Emil Shauffausen has been dead for over 40 years. Confused and distraught, she returns to her hotel room, where she finds Freddy, who apparently succeeds in seducing her.

Lawrence receives the news that Freddy was seen leaving Janets hotel room with her, so he gracefully accepts defeat. Freddy arrives at Lawrences château, but surprisingly, he has had a change of heart; he found he could not take advantage of Janet, that his feelings for her were genuine. Instead, he has married her, is going straight, and they are returning to America.

Lawrence reconciles with Freddy before he and Janet depart. He reflects, as he watches the honeymooners ship sail away, that the married Freddy has gotten the better deal in the end, but is distracted from this train of thought when his next target, a ravishing and extremely wealthy blonde, walks into the room.

== Production == Academy Award Pillow Talk).

==Cast==
*Marlon Brando ..... Freddy Benson
*David Niven ..... Lawrence Jameson
*Shirley Jones ..... Janet Walker
*Dody Goodman ..... Fanny Eubank
*Aram Stephan .... Andre
*Parley Baer .... Col. Williams
*Marie Windsor .... Mrs. Sutton
*Rebecca Sand .... Miss Trumble
*Frances Robinson .... Miss Harrington
*Henry Slate .... Sattler
*Norman Alden .... Dubin
*Susanne Cramer .... Anna
*Cynthia Lynn .... Frieda
*Ilse Taurins .... Hilda
*Francine York .... Gina

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 