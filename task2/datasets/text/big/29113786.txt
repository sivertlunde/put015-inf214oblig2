American Roulette (film)
{{Infobox film
| name           = American Roulette
| image          = American Roulette (film).jpg
| caption        = 
| director       = Maurice Hatton
| producer       = Graham Easton Verity Lambert
| writer         = Maurice Hatton Pablo Neruda
| screenplay     = 
| story          = 
| based on       =   Guy Bertrand Ricardo Sibelo
| music          = Michael Gibbs
| cinematography = Tony Imi
| editing        = Barry Peters
| studio         = 
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

American Roulette is a 1988 British thriller film directed by Maurice Hatton and starring Andy Garcia, Kitty Aldridge and Robert Stephens. 

==Plot summary==
A Latin American President is overthrown in a military coup and forms a government-in-exile in London. The Generals want him out of the way, and send agents to kill him. Meanwhile several of the worlds major intelligence agencies are also keeping a watch on him.

==Cast==
* Andy Garcia ...  Carlos Quintas 
* Kitty Aldridge ...  Kate Webber  Guy Bertrand ...  Ruben 
* Robert Stephens ...  Screech 
* Ricardo Sibelo ...  Miguel 
* Lino Omoboni ...  Alfonso 
* Ben Onwukwe ...  Hi-jacked Van Driver  Peter Guinness ...  Policeman 
* Al Matthews ...  Morrisey 
* Sheila Burrell ...  Rauls Neighbour 
* Alfredo Michelsen ...  Ramon 
* Carola Palacios ...  Inez 
* Adolpho Cozzi ...  Paco 
* Gloria Romo ...  Rauls Widow 
* Yves Aubert ...  Reporter at ICA 
* Juanita Waterman ...  Zoe 
* Kate McKenzie ...  Ms van Doorn 
* Christopher Rozycki ...  Vladimir 
* Boris Isarov ...  Nickolai 
* Rosalind Bennett ...  Pickwick

==References==
 

==External links==
*  

 

 
 
 
 
 


 
 