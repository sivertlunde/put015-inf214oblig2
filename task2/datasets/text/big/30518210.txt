The Big Hurt (film)
 
{{Infobox film
| name           = The Big Hurt
| image          = 
| image size     =
| caption        = 
| director       = Barry Peak
| producer       = Chris Kiely
| writer         = Barry Peak Sylvia Bradshaw
| based on = 
| narrator       =
| starring       = David Bradshaw
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 1986
| runtime        = 
| country        = Australia English
| budget         = AU $690,000 David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p240 
| gross = 
| preceded by    =
| followed by    =
}} thriller directed by Barry Peak starring David Bradshaw, Lian Lunson, Simon Chilvers, John Ewart, Alan Cassell.

It was shot over six weeks with money raised via Film_and_television_financing_in_Australia#History_of_Government_support|10BA. It was filmed on Super 16mm but later blown up to 35mm. 

==References==
 

==External links==
*  at IMDB

 
 
 


 