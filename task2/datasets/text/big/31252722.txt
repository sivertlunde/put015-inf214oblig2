The Mountain (1991 film)
{{Infobox film
| name           = The Mountain
| image          = 
| image size     = 
| caption        = 
| director       = Markus Imhoof
| producer       = Lang Film
| writer         = Thomas Hürlimann Markus Imhoof
| starring       = Susanne Lothar
| music          = 
| cinematography = Lukas Strebel
| editing        = Daniela Roderer
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Switzerland
| language       = German
| budget         = 
}}

The Mountain ( ) is a 1991 Swiss drama film directed by Markus Imhoof. It was entered into the 41st Berlin International Film Festival.   

==Cast==
* Susanne Lothar as Lena
* Mathias Gnädinger as Manser
* Peter Simonischek as Kreuzpointner
* Agnes Fink as Mutter Manser
* Jürgen Cziesla as Direktor
* Adolf Laimböck as Oberst
* Heinrich Beens as Pfarrer
* Ingold Wildenauer as Eisenbahner
* Branko Samarovski as Jetzeler (as Branko Samarowsky)
* Hans-Rudolf Twerenbold as Mann am Stammtisch
* Barbara Schneider as Freundin Herbert Müller as Fotograf

==References==
 

==External links==
* 

 
 
 
 
 
 
 