Love at Stake
{{Infobox film
| name           = Love at Stake
| image          = Love at Stake.jpg
| caption        = DVD cover John C. Moffitt John Daly, Derek Gibson, Michael Gruskoff, Donald C. Klune, Michael I. Levy	
| writer         = Lanier Laney and Terry Sweeny See below | Charles Fox
| cinematography = Mark Irwin
| editing        = Danford B. Greene
| studio         = De Laurentiis Entertainment Group Hemdale Film Corporation 
| distributor    = TriStar Pictures (later sold to Metro-Goldwyn-Mayer)
| released       = August 1, 1987
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $61,789 
}}	
 1987 comedy John C. Patrick Cassidy Dave Thomas and Stuart Pankin. Joyce Brothers makes a cameo appearance as herself.

The film is an obvious spoof of the infamous Salem witch trials, moving in the Mel Brooks comedy vein in Blazing Saddles, moving in the anarchic comedy films genre that was popularized in that time by the Monty Python films and from the Zucker, Abrahams and Zucker films.
 cult classic. Filming took place in Kleinburg, Ontario.

==Plot summary==
Miles Campbell, recent graduate of a Christian divinity university, arrives in Salem, Massachusetts to become the local parsons assistant. He meets with his childhood sweetheart, baker Sara Lee, and plans to marry her. Meanwhile, greedy Judge Samuel John arrives to meet with idiotic Mayor Upton to discuss plans for a (anachronistic) Mall for Salem. To acquire the necessary real estate they hatch a scheme to accuse certain villagers of witchcraft. When the accused are tried, convicted and burned, their land can be confiscated. The plan is succeeding, as the villagers, egged on by the parsons shrewish mother, enthusiastically accept the Judges message. Then saucy Faith Stewart (secretly a real witch) arrives from London for Thanksgiving with her cousins. Faith falls for Miles and accuses Sara of witchcraft. Miles must prove Saras innocence before she is burned at the stake.

==Cast of characters== Patrick Cassidy as Miles Campbell
*Kelly Preston as Sara Lee Georgia Brown as Widow Chastity
*Barbara Carrera as Faith Stewart
*Bud Cort as Parson Babcock
*Annie Golden as Abigail Baxter, Faiths cousin
*David Graf as Nathaniel Baxter, her husband
*Audrie J. Neenan as Mrs. Babcock, the parsons mother
*Stuart Pankin as Judge Samuel John Dave Thomas as Mayor Upton
*Anne Ramsey as Old Witch
*Mary Hawkins as Mrs. Priscilla Upton
*Jackie Mahon as Belinda Upton, the mayors daughter
*Norma MacMillan as Aunt Deliverance Jones, Saras aunt
*Joyce Brothers as Herself
*Colleen Karney as Adulteress
*Juul Haalmeyer as Executioner
*Julian Richings as Town Crier
*Danny Higham as Newsboy
*Marshall Perlmuter as Mr. Newberry
*Anna Ferguson as Mrs. Newberry
*Catharine Gallant as Constance Van Buren
*Jayne Eastwood as Annabelle Porter
*Nick Ramus as Chief Wannatoka

==Production==
*Director: John Moffitt (director)
*Writers: Lanier Laney and Terry Sweeney (both of Saturday Night Live fame)
*Producer: Michael Gruskoff Charles Fox
*Cinematography: Mark Irwin

==See also==
*Slapstick
*Anarchic comedy film

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 