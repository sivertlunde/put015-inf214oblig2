Cha cha cha (film)
{{Infobox film
 | name =Cha cha cha
 | image = Cha cha cha (film).jpg
 | caption =
 | director = Marco Risi
 | writer = 
 | starring =  
 | music =    	Marco I. Benevento
 | cinematography =   Marco Onorato
 | editing =  
 | producer = Italian
 }}
Cha cha cha is a 2013 Italian Crime film|crime-thriller film directed by Marco Risi.  Loosely inspired by the works of Raymond Chandler, it premiered at the 2013 Taormina Film Fest. 

== Cast ==
* Luca Argentero: Corso
* Eva Herzigová: Michelle
* Claudio Amendola: Torre
* Pippo Delbono: Lawyer Argento
* Bebo Storti: Massa
* Marco Leonardi: Photographer
* Pietro Ragusa: Muschio
* Shel Shapiro: Himself 
* Nino Frassica

==Production and Distribution==
The movie was produced by BiBi Film, Babe Films and RAI Cinema. The post-production was carried out by Reset VFX S.r.l..
The distribution of the movie is by 01 Distribution. 

==References==
 

==External links==
* 

 
 
 
 
 


 
 