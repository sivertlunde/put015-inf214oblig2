The Crimson Ghost
{{Infobox film
| name           = The Crimson Ghost
| image          = The Crimson Ghost poster.jpg
| director       =Fred C. Brannon William Witney
| producer       =Ronald Davidson
| writer         =Albert DeMond Basil Dickey Jesse Duffy Sol Shor Joseph Forte
| cinematography =Bud Thackery
| distributor    =Republic Pictures
| released       = 26 October 1946 (United States|U.S.) (serial) {{cite book
 | last = Mathis
 | first = Jack
 | title = Valley of the Cliffhangers Supplement
 | year = 1995
 | publisher = Jack Mathis Advertising
 | isbn = 0-9632878-1-8
 | pages = 3, 10, 94–95
 | chapter =
 }}  U.S. Early 1950s (TV)  U.S. 1966 (TV film) 
 | runtime         = 12 chapters / 167 minutes (serial)  6 26½-minute episodes (TV)  100 minutes (TV film) 
| country        = United States English
| budget          = $161,174
| awards         =
}} Republic Serial film serial Misfits adapted The Number of the Beast" by Iron Maiden.

==Plot==
  mysterious villain, the eponymous Crimson Ghost, is determined to steal a counter atomic device known as Cyclotrode X, which can short out any electrical device.

==Cast==
*Charles Quigley as Duncan Richards
*Linda Stirling as Diana Farnsworth
*Clayton Moore as Ashe
*I. Stanford Jolley as Doctor Blackton and the Voice of the Crimson Ghost
*Kenne Duncan as Dr. Chambers
*Forrest Taylor as Professor Van Wyck
*Emmett Vogan as Anderson
*Sam Flint as Maxwell Joseph Forte as Professor Parker/the Crimson Ghost
*Stanley Price as Count Fator

==Production==
The Crimson Ghost was budgeted at $137,912, although the final negative cost was $161,174 (a $23,262, or 16.9%, overspend). It was the most expensive Republic serial of 1946   It was filmed between 28 March and 24 April 1946 under the working title The Scarlet Shadow.   The serials production number was 1597. 

In order to prevent the audience deducing the identity of the Crimson Ghost, the studio cast stunt-man Bud Geary to embody the villain while several actors supplied the voice, including I. Stanford Jolley. Jolleys role was minor but he received fourth-billing and was therefore highly suspect. When The Crimson Ghost was unmasked in the 12th and final chapter, he proved to be yet another actor entirely, Joseph Forte, who had played a character seemingly above suspicion at that point in the serial.
 1937 and prior to this production had temporarily left the serial business to serve in World War II.

===Stunts===
*Dale Van Sickel as Duncan Richards (doubling Charles Quigley)
*Polly Burson as Diana Farnsworth (doubling Linda Stirling) Tom Steele as Ashe (doubling Clayton Moore & I. Stanford Jolley)
*Joe Yrigoyen as Duncan Richards & Count Fator (doubling Charles Quigley & Stanley Price)

===Special effects===
The special effects were produced by Republics Lydecker brothers.

==Release==
===Theatrical===
The Crimson Ghosts official release date is 26 October 1946, although this is actually the date the sixth chapter was made available to film exchanges. 

===Television===
In the early 1950s, The Crimson Ghost was one of fourteen Republic serials edited into a television series.  It was broadcast in six 26½-minute episodes.  The Crimson Ghost was one of twenty-six Republic serials re-released as a film on television in 1966.  The title of the film was changed to Cyclotrode "X".  This version was cut down to 100-minutes in length.   The Crimson Ghost was one of two Republic serials to be colorised in the 1990s. 

==Critical reception==
Cline believes that the Crimson Ghost is the most striking and visually fascinating villain in any film serial. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 52
 | chapter = 3. The Six Faces of Adventure
 }} 

==Chapter titles==
# Atomic Peril (20 min)
# Thunderbolt (13min 20s)
# The Fatal Sacrifice (13min 20s)
# The Laughing Skull (13min 20s)
# Flaming Death (13min 20s)
# Mystery of the Mountain (13min 20s)
# Electrocution (13min 20s)
# The Slave Collar (13min 20s) - a clipshow|re-cap chapter
# Blazing Fury (13min 20s)
# The Trap that Failed (13min 20s)
# Double Murder (13min 20s)
# The Invisible Trail (13min 20s)
 Source:   {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 244
 | chapter = Filmography
 }} 

==Influence==
The poster for a March 28, 1979 show at Maxs Kansas City featured the first use of the Crimson Ghost   by the band Misfits (band)|Misfits. The cover art for their Horror Business Vinyl record|EP, the bands third single, would be the first time that the character would appear on a release by the band. It would continue to be used as the bands mascot and its skull image continues to serve as the Misfits logo.
 The Number of the Beast" music video.

In the 2007 remake of Halloween (2007 film)|Halloween, a little boy (Skyler Gisondo playing Tommy Doyle) is wearing a Crimson Ghost costume.

==See also==
* List of film serials by year
* List of film serials by studio
* Misfits (band)

==References==
 

==External links==
* 

 
{{Succession box Republic Serial Serial
| before=Daughter of Don Q (1946 in film|1946)
| years=The Crimson Ghost (1946 in film|1946)
| after=Son of Zorro (1947 in film|1947)}}
 

 
 

 

 
 
 
 
 
 
 
 
 
 