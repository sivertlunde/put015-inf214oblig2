Akramana
{{Infobox film
| name           = Akramana
| image          =
| alt            =
| caption        =
| film name      = ಆಕ್ರಮಣ
| director       = Girish Kasaravalli
| producer       = Sharath Kumar S. Rao
| writer         = G. S. Sadashiva (dialogues)
| screenplay     = G. S. Sadashiva
| story          = B. V. Vaikunta Raju (Based on Novel)
| based on       =
| starring       = Vijayakashi Vaishali Kasaravalli Padmashree Chandrakumar Jain
| narrator       =
| music          = B. V. Karanth
| cinematography = S. Ramachandra
| editing        = Stanly
| studio         = Sri Sainath Productions
| distributor    = Sri Sainath Productions
| released       =  
| runtime        = 110 min
| country        = India Kannada
| budget         =
| gross          =
}}
 1980 Cinema Indian Kannada Kannada film, directed by Girish Kasaravalli and produced by Sharath Kumar and S. Rao. The film stars Vijayakashi, Vaishali Kasaravalli, Padmashree and Chandrakumar Jain in lead roles. The film had musical score by B. V. Karanth.  

==Cast==
 
* Vijayakashi
* Vaishali Kasaravalli
* Padmashree
* Chandrakumar Jain
* Sai Venkatesh
* Girish Kumar Yadav
* Prakash Reddy
* Srinath
* G. G. Hegde
* Bhaskar
* Venkata Rao
* Shivaramaiah
* Devaiah
* Dr Chitagopi
* S. N. Rotti
* K. S. Anantharam
* B. S. Jayaram
* Swarnamma
* Pushpa
* Dechamma
* Swarnamma
* Mala
* Master Sanjeev
 

==References==
 

==External links==
*  
*  

 

 
 

 