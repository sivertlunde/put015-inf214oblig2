The Smokers (film)
{{Infobox film
| name           = The Smokers
| image          = Smokersdvd.jpg
| image_size     = 
| caption        = DVD cover
| director       = Christina Peters 
| producer       = Kenny Golde  Nicholas M. Loeb
| writer         = Christina Peters Kenny Golde
| narrator       = 
| starring       = Dominique Swain Busy Philipps Keri Lynn Pratt Nicholas M. Loeb Oliver Hudson Ryan Browning Joel West  Thora Birch
| music          = Shane Baskerville Brent David Fraser Geoff Levin
| cinematography = J.B. Letchinger
| editing        = Elias Chalhub
| distributor    = MGM
| released       = 2000
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 2000 film directed and written by Christina Peters.   

==Plot==
Three rebellious teenage girls decide to even the score in the battle of the sexes. Looking back a few years after the events depicted, Jefferson Roth (who, along with her sisters are named after former presidents) tells the story of the last few months of her senior year at a Wisconsin boarding school when she and two girl friends, the naive Lisa and the outrageous Karen, conspire to use a pistol to turn the tables on males after a wealthy older man, with whom Karen had a one night stand, refuses to give her his home phone number.  They stage a sexual assault on David, Lisas on-and-off boyfriend, in an effort to try to be more like their male counterparts.  But, it backfires, as all three girls learn they are not able to have sex the way they feel a man can.  Their unfaithfulness to their own objective is summed up in Karens words, just prior to her tragic ending, "I wish I had a boyfriend."

==Cast==
*Dominique Swain as Jefferson Roth
*Busy Philipps as Karen Carter
*Keri Lynn Pratt as Lisa Stockwell
*Nicholas M. Loeb as Jeremy
*Oliver Hudson as David
*Ryan Browning as Dan
*Joel West as Christopher 
*Thora Birch as Lincoln Roth
*Tell Draper as Todd Manning
*Ryan Sasson as Ryan
*Jenne Zblewski as Charlotte

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 