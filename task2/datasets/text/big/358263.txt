The Player (film)
{{Infobox film
| name           = The Player
| image          = Player ver1.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Robert Altman David Brown Nick Wechsler
| writer         = Michael Tolkin
| starring       = Tim Robbins Greta Scacchi Fred Ward Whoopi Goldberg Peter Gallagher Brion James 
| music          = Thomas Newman
| cinematography = Jean Lepine
| editing        = Geraldine Peroni
| studio         = Avenue Pictures Spelling Entertainment
| distributor    = Fine Line Features (US) Pathé (UK)
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = $8 million
| gross          = $21,706,100
}} satirical film studio executive Griffin Mill (Tim Robbins) who murders an aspiring screenwriter he believed was sending him death threats.

The Player has many film references and Hollywood insider jokes, with around sixty Hollywood celebrities agreeing to make cameo appearances in the film. Altman stated, "It is a very mild satire," offending no one. DVD commentary on The Player. 

==Plot==
Griffin Mill (Tim Robbins) is a Hollywood studio executive dating story editor Bonnie Sherow (Cynthia Stevenson). He hears story pitches from screenwriters and decides which have the potential to be made into films, green-lighting only 12 out of 50,000 submissions every year. His job is endangered when up-and-coming story executive Larry Levy (Peter Gallagher) begins working at the studio. Mill has also been receiving death-threatening postcards, assumed to be from a screenwriter whose pitch he rejected.
 The Bicycle Thief. Mill goes to the theater in Pasadena, California|Pasadena, pretending to recognize Kahane in the lobby, and offers him a scriptwriting deal, hoping this will stop the threats. The two go to a nearby bar and have some drinks. Kahane gets intoxicated and rebuffs Mill’s offer; he calls Mill a liar, pointing out that he knows Mill didnt really go to the theater to see The Bicycle Thief because Kahane saw Mill wander in late and only catch the last five minutes of it. He also denies responsibility for sending Mill the postcards. Kahane continues goading Mill about his job security at the studio. In the bars parking lot, the two men fight. Suddenly enraged, Mill goes too far and accidentally drowns Kahane in a shallow pool of water, then makes it appear as though Kahane died during a botched robbery.

The next day, Mill receives a fax from his stalker. Thus, Mill has killed the wrong man, and the stalker apparently knows this. Mill attends Kahane’s funeral and gets along with June; she knows none of the other mourners. Studio chief of security Walter Stuckel (Fred Ward) confronts Mill about the murder and says that the police know Mill was the last one to see Kahane alive. Detectives Avery (Whoopi Goldberg) and DeLongpre (Lyle Lovett) suspect Mill is guilty of murder. They question him and DeLongpre keeps an eye on him. Mill receives a postcard from the writer suggesting they meet at a hotel bar. While Mill is waiting, he is cornered by two screenwriters, Tom Oakley (Richard E. Grant) and Andy Sivella (Dean Stockwell), who pitch Habeas Corpus, a legal drama featuring no major stars and with a depressing ending.  Mills stalker does not appear, but after leaving the club, Mill receives a fax in his Range Rover from the stalking writer, advising him to look under his raincoat. He discovers a live rattlesnake in a box, which a terrified Mill bludgeons with his umbrella.
 Oscar contender. Mills plan is to let Levy shepherd the film through production and have it flop. Mill will step in at the last moment, suggesting some changes to salvage the film’s box office, letting him reclaim his position at the studio.
 Desert Hot Springs resort and spa, consummating their relationship there. Mill confesses his role in Kahanes murder to her, but far from condemning him, June says she loves him. Mills attorney (Sydney Pollack) informs him that studio head Joel Levison (Brion James) has been fired, and that the Pasadena police want Mill to participate in a police lineup|lineup. An eyewitness has come forward who claims to have seen the murder. Mill gains a reprieve when the witness fails to identify him in the lineup.

One year later, studio power players are watching the end of Habeas Corpus with a new, tacked-on, upbeat "Hollywood" ending and famed actors in the lead roles. Mills plan to "save" the movie has worked and he is head of the studio. June is now Griffins wife and pregnant with his child. Bonnie objects to the changes and is fired by Levy, a decision Griffin does not overrule; he rebuffs Bonnie when she appeals her termination to him. Mill receives a pitch over the phone from Levy and a man who reveals himself as the postcard writer. The man pitches an idea about a studio executive who kills a writer and gets away with murder. Mill recognizes the pitch as blackmail and gives the writer a deal if he can guarantee the executive a happy ending. The writer’s title for the film is The Player.

==Cast==
* Tim Robbins as Griffin Mill
* Greta Scacchi as June Gudmundsdottir
* Fred Ward as Walter Stuckel
* Whoopi Goldberg as Detective Avery
* Peter Gallagher as Larry Levy
* Brion James as Joel Levison
* Cynthia Stevenson as Bonnie Sherow
* Vincent DOnofrio as David Kahane
* Dean Stockwell as Andy Civella
* Richard E. Grant as Tom Oakley
* Sydney Pollack as Dick Mellon
* Lyle Lovett as Detective DeLongpre
* Dina Merrill as Celia
* Jeremy Piven as Steve Reeves

===Cameos===
Most of the notable Hollywood actors who appeared as themselves in the film received "union scale", the minimum possible payment for their cameo appearances. 

  
*Steve Allen
*Richard Anderson
*Rene Auberjonois
*Harry Belafonte
*Shari Belafonte
*Karen Black Michael Bowen
*Gary Busey
*Robert Carradine
*Charles Champlin
 
*Cher
*James Coburn
*Cathy Lee Crosby
*Tim Curry
*John Cusack Brad Davis
*Paul Dooley
*Peter Falk
*Felicia Farr
*Katarzyna Figura
*Louise Fletcher
 
*Dennis Franz
*Teri Garr
*Leeza Gibbons
*Scott Glenn
*Jeff Goldblum
*Elliott Gould
*Joel Grey
*David Alan Grier
*Buck Henry
*Anjelica Huston
 
*Kathy Ireland Steve James
*Sally Kellerman
*Sally Kirkland
*Jack Lemmon
*Marlee Matlin
*Andie MacDowell
*Malcolm McDowell
*Jayne Meadows
*Martin Mull
 
*Nick Nolte
*Alexandra Powers
*Bert Remsen
*Burt Reynolds Jack Riley
*Julia Roberts
*Mimi Rogers
*Annie Ross
*Alan Rudolph
*Scott Shaw
 
*Jill St. John
*Susan Sarandon
*Adam Simon
*Rod Steiger
*Patrick Swayze
*Joan Tewkesbury
*Brian Tochi
*Lily Tomlin
*Robert Wagner
*Ray Walston
 
*Bruce Willis Marvin Young

 

==Production== The Long Goodbye) lost money or had trouble finding audiences despite the critical praise and cult adulation they received. Altman continued to work outside the studios in the late 1970s and throughout the 1980s, often doing small-budget projects or filmed plays to keep his career alive. The Player was a comeback to making films in Hollywood, although it was distributed by Fine Line Features rather than a major studio (FLF was a division of New Line Cinema). It ushered in a new period of filmmaking for Altman, who continued on to an epic adaptation of Raymond Carvers short stories, Short Cuts (1993).

===Opening sequence shot=== sequence shot lasts 7 minutes and 47 seconds without a single camera break. Fifteen takes were required to shoot this scene.   

===Intimate scene===
Altman was praised for the sex scene in which Robbins and Scacchi were filmed from the neck up. Scacchi later claimed that Altman had wanted a nude scene, but that it was her refusal which led to the final form. 

==Reception==
  Best Director Screenwriting Academy Academy Award Best Actor at the Cannes Film Festival. 

American Film Institute recognition:
*AFIs 100 Years... 100 Laughs - Nominated 

==References==
;Notes
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 