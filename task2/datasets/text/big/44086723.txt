Shishirathil Oru Vasantham
{{Infobox film 
| name           = Shishirathil Oru Vasantham
| image          =Sisirathil oru vasantham.jpg
| caption        =
| director       = Kothanda Ramaiah
| producer       =
| writer         = Kothanda Ramaiah
| screenplay     = Kothanda Ramaiah Shubha Sukumaran Ravikumar
| Shyam
| cinematography = K Ramachandrababu
| editing        = Radhakrishnan
| studio         = Yogideep Creations
| distributor    = Yogideep Creations
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, Ravikumar in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast== Shubha
*Sukumaran
*Kuthiravattam Pappu Ravikumar

==Soundtrack== Shyam and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Evide Thanal saakhikal || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Nenjil Nenchu || S Janaki, Chorus || Poovachal Khader || 
|-
| 3 || Oru Gaanam athil azhakidumoru || K. J. Yesudas || Poovachal Khader || 
|-
| 4 || Sandhyapole Kunkumam || Vani Jairam || Poovachal Khader || 
|}

==References==
 

==External links==
*  
 
 
 
 

 