Satan's Mistress
{{Infobox film
| name           = Satans Mistress
| image          = Poster of the movie Satans Mistress.jpg
| image_size     =
| caption        = Movie poster
| director       = James Polakof
| producer       = Beverly Johnson Michelle Morgan William H. Parker James Polakof Gary Rollason John J. Smith
| writer         = Beverly Johnson James Polakof
| narrator       =
| starring       = Lana Wood John Carradine Britt Ekland Kabir Bedi
| music          = Roger Kellaway
| cinematography = James L. Carter
| editing        = George Trirogoff
| distributor    =
| released       =  
| runtime        = 98 min.
| country        = United States English
| budget         =
| gross          =
}}

Satans Mistress (also known as Demon Rage, Fury of the Succubus and Dark Eyes)  is a horror movie released in 1982. It is about a sexually frustrated housewife, Lisa (played by actress Lana Wood), who having been distanced from her husband (Don Galloway as Carl) begins having nightly trysts with an apparition that gradually takes on the form of a tall, dark stranger (played by Kabir Bedi of Octopussy) who turns out to be a ghost from the other side. 
 Diamonds Are Forever.

The film was released the same year as the similarly themed The Entity, differing in that the sex in Satans Mistress is consensual.

The film has a cameo by veteran horror actor John Carradine as Father Stratten.

==References==
 

==External links==
*  

 
 
 
 

 