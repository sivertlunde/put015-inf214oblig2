Ha'penny Breeze
{{Infobox film
| name           = 
| image          = 
| image_size     = 
| caption        = 
| director       = Frank Worth
| producer       = Darcy Conyers
| writer         = Don Sharp Frank Worth
| narrator       = 
| starring       = Don Sharp
| music          = 
| cinematography = 
| editing        = 
| studio = The Storytellers
| distributor    = 
| released       = 1950
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = ₤8,000 
| gross          = 
}}
Hapenny Breeze is a 1950 British film.

==Plot==
David and his Australian friend Johnny return to a Suffolk village after the war to find the community completely dispirited. Gradually by their enthusiasm they win support for a scheme to enter a boat designed by David in a race.

==Production==
Australians Don Sharp and Frank Worth met in England and wrote the story together over a three-month period. They formed their own production company with Darcy Conyers and George N. Gregory and succeeded in raising finance with William Freshman attached as executive producer. 

Associated British Pathe offered to distribute on the understanding that everyone would be paid ten pounds a week. A Leeds auctioneer chipped in a few thousand pounds and the filmmakers provided money themselves.  

The film was shot in Pin Mill, a small fishing village in East Anglia. 

==Reception==
The critic from The Scotsman said the film had "too much of the naivete and the emotion of the amateur shine through... often the dialogue is trite and for most of the film the tempo is depressingly perambulatory" but praised the "cameras mobility and many a good character sketch by" the actors. "HAPENNY BREEZE": New British Productions
The Scotsman (1921-1950)   30 Dec 1950: 7.  "Has both charm and talent" said the Sunday Times. Powell, Dilys. "Heroinanity." Sunday Times   7 Jan. 1951: 2. The Sunday Times Digital Archive. Web. 16 Apr. 2014.
 

==References==
 

==External links==
*  BFI

 

 
 