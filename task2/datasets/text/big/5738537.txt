Zadar! Cow from Hell
{{Infobox film
|name=Zadar! Cow from Hell
|image=Zadar5.gif
|director=Robert C. Hughes
|writer=Merle Kessler
|producer=Steve Baker Scott Smith Robert Hughes Tom Higgins Jim Turner
|released=1989
|runtime=87 minutes
}}

Zadar! Cow from Hell is an Independent film|independent, low-budget film|low-budget comedy film directed by Robert C. Hughes. 

The film was released in 1989 and shown in select theaters, mostly located in eastern Iowa. In a two night theatrical run, the film grossed over USD|$25,000.  It also premiered at the Sundance Film Festival. 

The film was written by and cast with members of the Ducks Breath Mystery Theater, a successful comic acting troupe. The idea was originally introduced Merle Kessler, who was inspired by "Zarda", a dairy in Kansas City, that had a statue of a giant cow. The film was shot in and around Iowa City, Tipton, Solon and Mount Vernon, Iowa.

The plot of the movie revolves around a struggling Hollywood director who wished to return to his home state of Iowa and create a successful horror film. However, upon arriving in Iowa, the young director (played by Merle Kessler) is immediately distracted by old friends and family, a problem which will plague him throughout the making of his film, Zadar! Cow from Hell.

The film within a film is sprinkled with bits of humor, usually consisting of short one-liner joke|one-liners. However, the films defining moment comes when the individuals meant to be playing cows realize that they cannot remove the plastic horns from their heads. This causes chaos and frustration, and is later linked with the demise of the project.

While Zadar! Cow from Hell was eventually released on VHS and DVD, although not through a major distribution network. The film also gained attention from outside the movie-going community, eventually even having a meal named after it at the Hamburg Inn No. 2, a local diner. There, the "Zadar" omelette consists of two eggs, ground beef, cheese, and homefries.  There is also a reference to the movie in the role-playing video game game Neverwinter Nights, in which a cheat that summons killer cows is entered by typing "dm_cowsfromhell". 

==Complete Credited Cast==
*Robert C. Hughes - Director
*Bill Allard - Mr. Nifty
*Dan Coffey - Rex
*Merle Kessler - Sleepless Walker
*Leon Martell - Dan Tension Jim Turner - Max
*Deborah Gwinn - Amy Walker
*Eric Topham - Ralph Jr.
*Ned Holbrook  - Chip
*Toby Huss - Clerk
*Glen Jackson - Whitey the 2nd Clerk
*Harry Epstein - Angry Cow
*Ben Rollins - Protester
*Paul Neff - Feed Cap Guy John Horan - Boy in Crowd

==References==
 

==External links==
* 
* 

 
 
 
 
 
 