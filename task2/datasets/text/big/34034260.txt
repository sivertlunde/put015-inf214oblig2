Confessions of a Police Captain
{{Infobox film
| name = Confessions of a Police Captain
| image = Confessions of a Police Captain.jpg
| caption =
| director = Damiano Damiani
| writer = Damiano Damiani Fulvio Gicca Palli Salvatore Laurani
| starring = Franco Nero Martin Balsam
| music = Riz Ortolani
| cinematography = Claudio Ragona
| editing = Antonio Siciliano
| producer = Mario Montanari Bruno Turchetto
| distributor =
| released =  
| runtime = 101 minutes
| country = Italy
| language = Italian
| budget =
}} 
Confessions of a Police Captain  ( , also known as Confessions of a Police Commissioner to the District Attorney) is an Italian crime-drama film.

The film won the Golden Prize at the 7th Moscow International Film Festival in 1971      and the Prix lntemational de lAcademie du Cinema at the Étoile de Cristal Awards in 1972.    

==Plot==
D.A. Traini strives to convict Ferdinando Lomunno of playing a leading role in local organised crime. He learns that Commissioner Bonavia feels he has unfinished business with Lomunno because Lomunno once ordered the assassination of a politician who had been admired by Bonavia. While Traini takes his pride in going by the book, policeman Bonavia lost his confidence in common law and is about to become a vigilantism|vigilante.

== Cast ==
* Franco Nero: Deputy D.A. Traini
* Martin Balsam: Commissioner Bonavia
* Marilù Tolo: Serena Li Puma
* Claudio Gora: District Attorney Malta
* Luciano Catenacci: Ferdinando Lomunno
* Giancarlo Prete: Giampaolo Rizzo
* Arturo Dominici: Lawyer Canistraro
* Michele Gammino: Gammino
* Adolfo Lastretti: Michele Li Puma
* Nello Pazzafini: prisoner

==Releases==
Wild East released this on a limited edition R0 NTSC DVD alongside The Summertime Killer in 2010.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 

 
 