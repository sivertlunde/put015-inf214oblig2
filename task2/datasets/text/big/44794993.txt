Dobir Saheber Songsar
{{Infobox film
| name           = Dobir Saheber Songsar
| image          = 
| alt            =  
| director       = Jakir Hossain Raju
| producer       = Shish Monwar
| writer         = A J Babu
| starring       = Mahiya Mahi Bappy Chowdhury Asif Imrose Ali Raz
| music          = 
| cinematography = 
| editing        = Tawhid Hossain Chowdhury
| studio         = Jaaz Multimedia
| distributor    = Jaaz Multimedia
| released       =  
| country        = Bangladesh Bengali
| budget         = 
| gross          = 
}}
 Romantic Comedy film. Directed by Jakir Hossain Raju. Stars , Bappy Chowdhury, and many more. Releasing on 4 April 2014. Shakib Khan played a Cameo Role in this film.
==Plot==
This film is about Mr. Dobir Ali Raz  who has losted his second Daughter 
Mahiya Mahi in her childhood. Meanwhile He appointed two servants Kuddus Bappy Chowdhury and Kutub Asif Imrose. Can he find his missing daughter ???
==Cast==
* Mahiya Mahi as Mr. Dobirs Daughter
* Bappy Chowdhury as Kuddus 
* Ali Raj as Guljar
* Asif Imrose as Kutub
* Alekgander Bo as Villain
* Shakib Khan as Cameo Appearance

 