Anuragakottaram
{{Infobox film 
| name           = Anuragakottaram
| image          =
| caption        =
| director       = Vinayan
| producer       = Ramakrishnan
| writer         =  AK Sajan AK Santhosh Dileep Suvalakshmi Kalpana
| music          = Ilayaraja
| cinematography = Venugopal
| editing        = G Murali
| studio         = Prathyusha Films
| distributor    = Prathyusha Films
| released       =  
| country        = India Malayalam
}}
 1998 Cinema Indian Malayalam Malayalam film, Kalpana in lead roles. The film had musical score by Ilayaraja.   

==Cast== Dileep
*Suvalakshmi
*Jagathy Sreekumar Kalpana
*Cochin Haneefa
*Maniyanpilla Raju

==Soundtrack==
The music was composed by Ilayaraja and lyrics was written by Kaithapram. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chirichente Manassile   || K. J. Yesudas, KS Chithra || Kaithapram || 
|-
| 2 || Chirichente Manassile   || K. J. Yesudas || Kaithapram || 
|-
| 3 || Mohathin Mutheduthu || KS Chithra, Biju Narayanan || Kaithapram || 
|-
| 4 || Ponmaanam Ee Kaikalil   || Biju Narayanan, Sruthi || Kaithapram || 
|-
| 5 || Ponmaanam Ee Kaikalil   || Biju Narayanan || Kaithapram || 
|-
| 6 || Ponnum Thinkal Thaarattum || K. J. Yesudas || Kaithapram || 
|-
| 7 || Thenchodi Poove Maanmizhi Kanave || MG Sreekumar || Kaithapram || 
|}

==References==
 

==External links==
*  

 
 
 
 
 