Song of Surrender
{{Infobox film
| name           = Song of Surrender
| image          = 
| image_size     = 
| caption        = 
| director       = Mitchell Leisen
| writer         = Ruth McKenney Richard Bransten Richard Maibaum
| narrator       = 
| starring       = Wanda Hendrix
| music          = Victor Young
| cinematography = Daniel L. Fapp
| editing        = Alma Macrorie
| distributor    =  Paramount Pictures
| released       = October 28, 1949
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Song of Surrender is a 1949 drama film directed by Mitchell Leisen. It stars Wanda Hendrix and Claude Rains.  

==Cast==
*Wanda Hendrix as Abigail Hunt
*Claude Rains as Elisha Hunt
*Macdonald Carey as Bruce Eldridge
*Andrea King as Phyllis Cantwell
*Henry Hull as Deacon Perry Gordon Richards as Clayton

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 