Riders of the Whistling Skull
{{Infobox film
| name           = Riders of the Whistling Skull
| image          =Poster of the movie Riders of the Whistling Skull.jpg
| caption        =
| director       = Mack V. Wright Louis Germonprez (assistant)
| producer       = Nat Levine Sol C. Siegel (associate) Oliver Drake Bernard McConville John Rathmell William Colt MacDonald (novel)
| starring       = Bob Livingston Ray "Crash" Corrigan Max Terhune
| music          = Harry Grey (supervisor)
| cinematography = Jack A. Marta
| editing        = Murray Seldeen (supervising) Tony Martinelli
| distributor    = Republic Pictures
| released       = 4 January 1937
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Three Mesquiteers" Western B-movie Bob Livingston, Ray "Crash" Corrigan, and ventriloquist Max Terhune with his dummy Elmer. It was directed by Mack V. Wright, produced by Nat Levine and released by Republic Pictures.  The film is based on the 1934 novel by William Colt MacDonald.

==Plot== Indians and mummies enliven the proceedings.

==Cast== Bob Livingston as Stony Brooke, Mesquiteer
*Ray "Crash" Corrigan as Tucson Smith, Mesquiteer
*Max Terhune as Lullaby Joslin, Mesquiteer Mary Russell as Betty Marsh, daughter of the missing archeologist, Prof. Marsh Roger Williams as Rutledge, another member of the expedition to find Prof.  Marsh
*Fern Emmett as Henrietta McCoy
*C. Montague Shaw as Prof. Flaxon
*Yakima Canutt as Otah, an Indian guide travelling with the Mesquiteers
*John Ward as Prof. Brewster George Godfrey as Prof. Fronc
*Earle Ross as Prof. Cleary Frank Ellis as Coggins, the cook
*Chief Thundercloud as High Priest
*John Van Pelt as Prof. Marsh, an archeologist searching for a lost city in the American west

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 