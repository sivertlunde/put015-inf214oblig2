Story of Barbara
{{Infobox Film
| name           = Story of Barbara
| image          = 
| image size     = 
| caption        = 
| director       = Palle Kjærulff-Schmidt
| producer       = Bo Christensen
| writer         = Klaus Rifbjerg
| narrator       = 
| starring       = Yvonne Ingdal
| music          = 
| cinematography = Claus Loof
| editing        = Ole Steen
| distributor    = 
| released       = 17 April 1967
| runtime        = 84 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| preceded by    = 
| followed by    = 
}}

Story of Barbara ( ) is a 1967 Danish film directed by Palle Kjærulff-Schmidt. It was entered into the 17th Berlin International Film Festival.   

==Cast==
* Yvonne Ingdal - Barbara
* Peter Steen - Finn
* Ejner Federspiel - Barbaras Father
* Jørgen Buckhøj - Jørgen
* Hans Stephensen - Werner
* Lars Lunøe - Kjeld
* Thomas Nissen - Tom
* Allan De Waal - Gulle

==References==
 

==External links==
* 

 
 
 
 
 
 