All Ages: The Boston Hardcore Film
{{Infobox film
| name=All Ages: The Boston Hardcore Film
| image=
| caption = The movie poster for All Ages Boston Hardcore.
| starring= Early Boston hardcore community
| director=Drew Stone Duane Lucia Katie Goldman
| music=
| distributor=Gallery East
| released= 
| runtime=85 minutes
| language=English
}}
 American Hardcore" Director Paul Rachman, and Newbury Comics owner Michael Dreese. All Ages has been released on DVD with numerous extras and directors commentary.

==Plot==
The film explores the early Boston Hardcore music scene from the years 1981 through 1984 and delves deep into the social and communal aspects of that era; the community, culture, straight-edge and DIY (‘do it yourself’) ethic of the time. There are over fifty interviews, never before seen live footage, rare photographs, and dramatizations.  

Driven by a blazing soundtrack of classic first generation hardcore including SS Decontrol, Gang Green, Jerrys Kids, The F.U.s, etc., All Ages examines this vibrant period of American youth culture and its rich and productive history.  

==Production==
The film was shot and edited over a three year period, with many of the posters and photos resulting from a Facebook campaign to gather archival imagery coupled with scanning parties at the West End Museum. The majority of the personal interviews were shot at Suffolk University. Included in the film are over 120 images of legendary Boston punk rock photographer Phil In Phlash whose photography graced the cover of Bostons first two hardcore LPs This Is Boston, Not LA and The Kids Will Have Their Say.

==Soundtrack==
1. How Much Art - SS Decontrol DYS
3. I Don’t Belong - Jerrys Kids (band)|Jerrys Kids
4. Time Bomb - The Freeze
5. We Don’t Need It - Jerrys Kids (band)|Jerrys Kids
6. Not Normal - SS Decontrol
7. Complain - Impact Unit
8. Is This My World? - Jerrys Kids (band)|Jerrys Kids
9. F.U. - F.U.s
10. My Friend The Pit - Impact Unit DYS
12. This Is Boston Not LA - The Freeze DYS
14. Have Fun - Gang Green
15. Slam - Decadence
16. Now Or Never - The Freeze
17. Sick of Fun - Deep Wound
18. Snob - Gang Green
19. Straight Jacket - Jerrys Kids (band)|Jerrys Kids
20. Night Stalker - Impact Unit
21. Hazardous Waste - Negative FX
22. Young Fast Iranians - F.U.s
23. Trouble If You Hide - The Freeze DYS
25. My Machine Gun - Jerrys Kids (band)|Jerrys Kids

==References==
 

==External links==
*  
*  

 
 
 