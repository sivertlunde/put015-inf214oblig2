Little Red Monkey
{{Infobox film
| name           = Little Red Monkey
| image          = Little Red Monkey film poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Ken Hughes
| producer       = Alec C. Snowden
| writer         = Eric Maschwitz   James Eastwood   Ken Hughes
| narrator       = 
| starring       = Richard Conte Rona Anderson Russell Napier Sylva Langova
| music          = Trevor Duncan
| cinematography = Josef Ambor
| editing        = Inman Hunter   Geoffrey Muller
| studio         = Merton Park Studios
| distributor    = Anglo-Amalgamated Film Distributors|Anglo-Amalgamated (UK)   Allied Artists Pictures (US)
| released       = 3 June 1955 (W.Germany)   19 June 1955 (US)
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British spy Nuclear scientist, and are intriuged by strange reports received about the crimes.

The film was made by Anglo-Amalgamated at Merton Park Studios. The film was an international hit, and along with Confession (1955 film)|Confession proved a breakthrough for Anglo-Amalgamated. After its success the company began making more expensive productions, often importing American stars to give them more international appeal. 

==Cast==
* Richard Conte - Bill Locklin
* Rona Anderson - Julia Jackson
* Russell Napier - Superintendent John Harrington
* Sylva Langova - Hilde Heller, chief spy
* Colin Gordon - Harry Martin, reporter
* Donald Bisset - Editor Harris
* John King-Kelly - Spy Henchman
* Bernard Rebel - Vinson - Spy Henchman
* Arnold Marlé - Professor Leon Dushenko John Horsley - Detective Sergeant Gibson
* Jane Welsh -  Superintendent McCollum
* Theodore Wilhelm - Secretary of the International Social Club
* Colin Tapley -  Sir Clive Raglan
* Noel Johnson -  Detective Sergeant Hawkins
* Jessica Kearns -  Airport Hostess
* Geoffrey Denys -  Doctor Mayhew
* Gianfranco Parolini -  Inspector May
* Guy Deghy - Social Club Recreation Director
* Peter Godsell - Tommy McCollum
* Ed Devereaux - American Sailor
* Jon Farrell - American Newscaster
* Leonard Franks - Midget
* George Margo - American Sailor
* André Mikhelson - East German Chief of Border Guards
* Tony Sympson - Cab Driver

==References==
 

==Bibliography==
* Chibnall, Steve & McFarlane, Brian. The British B Film. Palgrave MacMillan, 2009.

==External links==
* 
* 

 

 
 
 
 
 
 
 

 