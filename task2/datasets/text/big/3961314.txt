Khamosh Pani
{{Infobox film
| name           = Khamosh Pani
| image          = Sw cover 2.jpg
| caption        = Khamosh Pani movie poster
| director       = Sabiha Sumar
| writer         = Paromita Vohra
| starring       = Kiron Kher Shilpa Shukla Aamir Malik
| distributor    = Shringar Films (India)
| released       =  
| runtime        = 105 minues
| country        = Pakistan
| language       = Punjabi
}} German production about a widowed mother and her young son set in a late 1970s village in Punjab (Pakistan)|Punjab, Pakistan which is coming under radical influence.

The film was released in India too. It was shot in a village in Pakistan and won 7 awards, including Golden Leopard (Best Film), Best Actress and Best Direction  at the 56th Locarno International Film Festival, Switzerland. 

==Plot== Punjab province of Pakistan. Ayesha is a middle-aged widow whose life is centred on her only son, the teenager Saleem, who is in love with Zubeida, a teenage village schoolgirl. Ayesha manages to support herself and her son by her late husbands pension and by giving lessons in the Quran to village girls. Generally well-liked and regarded, the only odd thing about Ayesha is that she refuses to go to the village well, having her neighbors daughters draw water for her. Some villagers such as Amin, the postman, are troubled by the recent hanging of former prime minister Zulfikar Ali Bhutto by the new military ruler, Gen. Zia-ul-Haq, who has promised to enforce Islamic law and encourages Islamic missionary and political groups to spread Islamism across Pakistan. Two activists from an Islamist group come to the village, and supported by the village "Choudhary" (landlord), start spreading their message of Islamic zealotry and gain recruits to fight the then-impending Soviet invasion of Afghanistan. The older men in the village react with disdain for their message of intolerance and puritanism, express cynicism at Zias continual postponement of democratic elections and are angered when the activists accuse them of being un-Islamic and traitors. Although they struggle amongst the older village men, the activists soon gain a following amongst the village youth, including Saleem. With a mixture of cajoling and intimidation, the activists bring Saleem with them to a political meeting in Rawalpindi, where the speakers exhort their audience to commit themselves to jihad (religious war) for the creation of a true Islamic state in Pakistan. Drawn by their missionary zeal and call to serve Islam and Pakistan, Saleem, who wants to lead a more meaningful life than that of a simple village farmer, abandons Zubeida and grows estranged from his mother. Ayesha tries to discourage him from following the Islamists, but fails. Saleem participates in the construction of a wall to surround the girls school to "protect" them and enforcing a shutdown of village shops during namaaz (prayer) time, in line with Zia-ul-Haqs Islamisation. Both Ayesha and Zubeida are deeply alarmed at Saleems transformation into an angry and hostile young man.
 independence of independence of Pakistan in 1947. Although the Sikh men, including her father, want her to jump, Veero refuses and runs away, with only her little brother following her briefly. She is later caught by Muslim rioters and is raped and imprisoned. The man who raped her later feels remorse, and offers to marry Veero. Left alone with the shame of being a raped woman and a non-Muslim in Pakistan, Veero accepts his proposal, converts to Islam, marries him and starts a new life as Ayesha, a Pakistani Muslim.

Saleem reports this to his zealot friends, and although they say they do not have a problem with Saleem, who declares he is a true Pakistani and true Muslim, they demand that Ayesha make a public declaration of her belief in Islam. Saleem conveys this demand, but Ayesha refuses. This prompts an increasing boycott of Ayesha from the villagers, including her best friends. For the first time in more than thirty years, she is forced to go to the well and fetch her own water. She meets her Sikh brother at the well and refuses to accompany him, condemning her father for encouraging her to kill herself and asking how he would feel to know that she was living as a Muslim. This does not change her growing isolation, with only Zubeida keeping in touch with her. Realizing that the wounds of her past would not go away, and that she was still not accepted as a Muslim and would not be able to lead a normal life, Ayesha commits suicide by jumping into the well. Saleem buries her, gathers his mothers papers and belongings and throws them into the village river.
 independence of Pakistan in 1947 and the atrocities associated with it.

==Cast==
* Kiron Kher
* Aamir Malik Arshad Mehmood
* Salman Shahid
* Shilpa Shukla
* Sarfaraz Ansari

==Awards==
* 2003: Locarno International Film Festival
** Bronze Leopard Award (Best Actress): Kirron Kher
** Don Quixote Award - Special Mention: Sabiha Sumar
** Golden Leopard (Best Film): Sabiha Sumar 
** Prize of the Ecumenical Jury: Sabiha Sumar
** Youth Jury Award - Special Mention: Sabiha Sumar 
* 2003: Nantes Three Continents Festival
** Audience Award: Sabiha Sumar
** Silver Montgolfiere: Sabiha Sumar
* 2003: Karachi International Film Festival
** Special Jurors Selection Ciepie
** Best Actress in a Leading Role:   Official website.  
** Best Screenplay: Paromita Vohra 

==Release Info==
Movie was screened in very few Film Festivals between 2003-2005. Released WorldWide.

===Screening in Film Festivals===

{| class="wikitable sortable"
|- style="background:#ccc; text-align:center;"
! Screening Date !! Film Festival !! Country
|- style="vertical-align: middle; text-align: center;"
| 2003-8-15 || Locarno Film Festival || Switzerland
|- style="vertical-align: middle; text-align: center;"
| 2007-09-04 ||  || 
|- style="vertical-align: middle; text-align: center;"
| 2008-12-07 ||  || 
|- style="vertical-align: middle; text-align: center;"
|   
|||  || 
|- style="vertical-align: middle; text-align: center;"
|   
|||  || 
|}

==References==
 

==External links==
*  
*   New York Times.

 

 
 
 
 
 
 
 