The White Raven (1917 film)
{{infobox film
| name           = The White Raven
| image          = The White Raven.jpg
| imagesize      =
| caption        =
| director       = George D. Baker
| producer       = Rolfe Photoplays
| writer         = Charles A. Logue (screen story) George D. Baker (scenario)
| cinematography = Arthur Martinelli
| editing        =
| distributor    = Metro Pictures
| released       =   reels
| country        = United States Silent (English intertitles)
}}
The White Raven is a  1917 American silent drama film produced by B. A. Rolfes Rolfe Photoplays and distributed by Metro Pictures. This drama stars Ethel Barrymore in an original screen story.  

A copy of the film is preserved at George Eastman House. 

==Cast==
*Ethel Barrymore - Nan Baldwin
*William B. Davidson - The Stranger
*Walter Hitchcock - John Blaisdell
*George A. Wright - Arthur Smithson
*Viola A. Fortescue - Mrs. Smithson
*Herbert Pattee - Bill Baldwin
*Mario Mejeroni - The Opera Impresario
*Phil Sandford - The Dance Hall Proprietor
*Ethel Dayton - Sylvia Blaisdell
*Ned Finlay - The Miner

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 


 