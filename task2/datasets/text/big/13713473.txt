Saneamento Básico
{{Infobox film
| name           = Saneamento Básico
| image          = Saneamento-basico-poster01.jpg
| caption        = Theatrical release poster
| director       = Jorge Furtado
| producer       = Guel Arraes Nora Goulart Luciana Tomasi
| writer         = Jorge Furtado
| starring       = Fernanda Torres Wagner Moura Camila Pitanga Bruno Garcia Lázaro Ramos Tonico Pereira and Paulo José
| music          = Leo Henkin
| cinematography = Jacob Solitrenick
| editing        = Giba Assis Brasil Globo Filmes Columbia Pictures  (theatrical) 
| released       = July 20, 2007
| runtime        = 112 minutes
| country        = Brazil Portuguese
| budget         =
}} Portuguese for Brazilian comedy film written and directed by Jorge Furtado. It stars Fernanda Torres, Wagner Moura, Camila Pitanga, Bruno Garcia, Lázaro Ramos, Tonico Pereira and Paulo José.

==Plot== federal government documentary about the construction of the tank, but the film has to be fictitious. Then, they decide to make a science fiction B movie which tells the story of a monster who lives in the building site of a tank.

==Cast==
*Fernanda Torres as Marina
*Wagner Moura as Joaquim
*Camila Pitanga as Silene
*Bruno Garcia as Fabrício
*Lázaro Ramos as Zico
*Janaína Kremer as Marcela
*Tonico Pereira as Antônio
*Paulo José as Otaviano

==Release==

===Critical reception===
According to Ana Paula Sousa, film reviewer of Carta Capital, Saneamento Básico is one of a few really funny films in recent Brazilian cinema.  On the other hand, Dayanne Mikevis, reviewer of Folha de São Paulo, says the film is a "light entertainment" and a "naïve comedy". 

===Box office===
As of August 12, 2007, Saneamento Básico had grossed R$1,066,051 and sold over one hundred and twenty thousand tickets in Brazil. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 