The Basketball Diaries (film)
{{Infobox film
| name           = The Basketball Diaries
| image          = The Basketball Diaries Poster.jpg
| caption        = Theatrical release poster
| director       = Scott Kalvert
| producer       = Liz Heller John Bard Manulis
| writer         = Jim Carroll   Bryan Goluboff  
| starring       = Leonardo DiCaprio Lorraine Bracco James Madio Mark Wahlberg Bruno Kirby
| music          = Graeme Revell
| cinematography = David Phillips
| editing        = Dana Congdon Island Pictures
| distributor    = New Line Cinema
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| gross          = $2,424,439 
}}
The Basketball Diaries is a 1995 American drama film directed by Scott Kalvert, starring Leonardo DiCaprio, Lorraine Bracco, James Madio, and Mark Wahlberg from the non-fiction work of the same name. The film centers on Jim Carroll (DiCaprio), a promising teenage basketball player who develops an addiction to heroin with his misguided friends.

The film was shot in New York City.

==Plot== adaptation of poet and memoirist Jim Carrolls (Leonardo DiCaprio) juvenile diaries chronicling his kaleidoscopic free-fall into the harrowing world of drug addiction. As a member of a seemingly unbeatable high school basketball squad, Jims life centers on the basketball court and the court becomes a metaphor for the world in his mind. A best friend who is dying of leukemia, a coach ("Swifty") who takes unacceptable liberties with the boys on his team, teenage sexual angst, and an appetite for cocaine and heroin all begin to encroach on young Jims dream of becoming a basketball star.
 New York become a refuge from his mothers mounting concern for her son. He cannot go home and his only escape from the reality of the streets is heroin for which he steals, robs and prostitutes himself. Only with the help of Reggie, an older neighborhood friend with whom Jim "picked up a game" now and then, is he able to begin the long journey back to sanity, which ultimately ends with Jims incarceration in Rikers Island. After months in prison, he leaves and later does a talk show about his drug life, after turning down free drugs from his old friend, Pedro.

The film is set in the early 1990s, while Carrolls actual book recounts experiences from growing up in the 1960s. Jim started out as a practice basketball player, and moved on to write The Basketball Diaries.

==Cast==
* Leonardo DiCaprio as Jim Carroll
* Lorraine Bracco as Mrs. Carroll
* Marilyn Sokol as Chanting Woman
* James Madio as Pedro
* Patrick McGaw as Neutron
* Mark Wahlberg as Mickey
* Roy Cooper as Father McNulty
* Bruno Kirby as Swifty
* Alexander Chaplin as Bobo
* Juliette Lewis as Diane Moody
* Michael Imperioli as Bobby
* Michael Rapaport as Skinhead
* Ernie Hudson as Reggie
* Manny Alfaro as Manny
* Cynthia Daniel as Winkie
* Brittany Daniel as Blinkie
* Jim Carroll  as  Frankie Pinewater, Credited as James Dennis Carroll

==Reception==
The film currently holds a 46% "Rotten" rating at the review aggregator website Rotten Tomatoes.  Roger Ebert gave two stars out of four, concluding, "At the end, Jim is seen going in through a "stage door," and then we hear him telling the story of his descent and recovery. We cant tell if this is supposed to be genuine testimony or a performance. Thats the problem with the whole movie."  Many critics also praised Wahlbergs performance. 

==Lawsuits== Jack Thompson brought this film into a $33 million lawsuit in 1999 claiming that the films plot (along with two internet pornography sites, several computer game companies, and makers and distributors of the 1994 film Natural Born Killers) caused the 14-year-old Michael Carneal to shoot members of a prayer group. The case was dismissed in 2001.  

The same year, the film became controversial in the aftermath of the Columbine High School massacre and the Heath High School shooting, when critics noted similarities between these shooting attacks and a fantasy sequence in the film in which the protagonist wears a black trenchcoat and shoots six classmates in his school classroom. The film has been specifically named in lawsuits brought by the relatives of murder victims.    

==Soundtrack==
The Basketball Diaries soundtrack was released in 1995 by PolyGram to accompany the film, featuring songs from Pearl Jam and PJ Harvey. AllMusic rated it three stars out of five. 

{{Track listing
| headline        =
| extra_column    = Artist
| writing_credits = yes
| title1 = Catholic Boy
| extra1 = Jim Carroll with Pearl Jam 
| writer1 = Jim Carroll
| length1 = 3:05
| title2 = Devils Toe
| extra2 = Graeme Revell with Jim Carroll
| writer2 = Jim Carroll
| length2 = 0:56 Down by the Water
| extra3 = P J Harvey 
| writer3 = P J Harvey
| length3 = 3:14
| title4 = What a Life!
| extra4 = Rockers Hi-Fi 
| writer4 = Glyn "Bigga" Bush, Richard "DJ Dick" Whittingham, Rob McKenzie
| length4 = 4:02
| title5 = I Am Alone
| extra5 = Graeme Revell with Jim Carroll
| writer5 = Jim Carroll
| length5 = 1:33 People Who Died The Jim Carroll Band 
| writer6 = Jim Carroll, Brian Linsley, Steve Linsley, Terrell Winn, Wayne Woods
| length6 = 5:00
| title7 = Riders on the Storm
| extra7 = The Doors 
| writer7 = Jim Morrison, John Densmore, Robby Krieger, Ray Manzarek
| length7 = 6:56
| title8 = Dizzy
| extra8 = Green Apple Quick Step 
| writer8 = Ty Willman, Mari Ann Braeden, Danny K, Bob "Mink" Martin, Steve Ross
| length8 = 3:10
| title9 = Its Been Hard
| extra9 = Graeme Revell with Jim Carroll
| writer9 = Jim Carroll
| length9 = 0:53
| title10 = Coming Right Along
| extra10 = The Posies 
| writer10 = Jon Auer, Ken Stringfellow
| length10 = 6:17
| title11 = Strawberry Wine
| extra11 = Massive Internal Complications
| writer11 = Salvadore Poe, Adam Flax
| length11 = 3:59
| title12 = Star
| extra12 = The Cult 
| writer12 = Ian Astbury, Billy Duffy
| length12 = 5:00
| title13 = Dream Massacre
| extra13 = Graeme Revell
| writer13 =
| length13 = 1:23
| title14 = Ive Been Down Flea 
| writer14 = Flea
| length14 = 4:38
| title15 = Blind Dogs
| extra15 = Soundgarden 
| writer15 = Chris Cornell, Kim Thayil
| length15 = 4:40
}}

{{Track listing
| headline        = Not featured on CD
| extra_column    = Artist
| writing_credits = yes
| title1 = Dancing Barefoot
| extra1 = Johnette Napolitano
| writer1 = Patti Smith, Ivan Kral
| length1 =
| title2 = Watusi Latin Boogaloo
| extra2 = The Joey Altruda Latin Explosion
| writer2 = Joey Altruda
| length2 =
}}

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 