Iniyathra
{{Infobox film
| name           = Iniyathra
| image          =
| caption        =
| director       = Srini
| producer       =
| writer         = Dr. Sivasankar
| screenplay     =
| starring       = Srividya Janardanan Kuthiravattam Pappu Nanditha Bose Shyam
| cinematography =
| editing        =
| studio         = Jayakrishna Movies
| distributor    = Jayakrishna Movies
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by Srini. The film stars Srividya, Janardanan, Kuthiravattam Pappu and Nanditha Bose in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Srividya
*Janardanan
*Kuthiravattam Pappu
*Nanditha Bose
*Prathapan
*Ravi Menon
*Urmila

==Soundtrack== Shyam and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aalinganathin sukhamaanu nee || Chorus, Jolly Abraham || Poovachal Khader || 
|-
| 2 || Eeranudukkum yuvathi || Vani Jairam, Karthikeyan || Poovachal Khader || 
|-
| 3 || Kaanaathe nee vannu || S Janaki || Poovachal Khader || 
|-
| 4 || Karayaan polum kazhiyaathe || S Janaki || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 