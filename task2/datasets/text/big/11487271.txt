Voice of a Murderer
{{Infobox film name        = Voice of a Murderer image       = Voice of a Murderer.jpg caption     = Voice of a Murderer poster film name   = {{Film name hangul      =     hanja       =  rr          = Geunom moksori mr          = Kŭnom moksori }} director    = Park Jin-pyo producer    = Lee Yu-jin writer      = Park Jin-pyo starring    = Sol Kyung-gu Kim Nam-joo cinematography = Kim Woo-hyung editing     = music       = Lee Byung-woo released    =   distributor = CJ Entertainment runtime     = 122 minutes language    = Korean budget      =   gross       =   
}} crime thriller (genre)|thriller-drama film written and directed by Park Jin-pyo, starring Sol Kyung-gu and Kim Nam-joo.       It was the third top-grossing domestic film of 2007, with 3,143,247 tickets sold.   The story is a fictionalized account of a real-life kidnapping case in 1991.  

==Plot==
The only son of famous news anchor Han Kyung-bae disappears without a trace. The kidnapper calls the nine-year-old boys mother, Oh Ji-sun, demanding   as ransom. The police assign a veteran cop and his so called special task force on the case. However, the kidnapper catches wind of the police involvement and threatens to cut out the boys heart and mail it to them. Han desperately tries to save his boy and obey the gangsters demands. However, he fails consistently thanks to the cops inocompetency and bad luck...later he starts telling what the kidnapper will do and...
==Cast==
*Sol Kyung-gu as Han Kyung-bae
*Kim Nam-joo as Oh Ji-sun Kim Yeong-cheol as Kim Wook-jung
*Song Young-chang as Captain Noh
*Go Soo-hee as Cha Soo-hee Kim Kwang-kyu as Chief investigator
*Choi Jung-yoon as Anchorwoman Ha Joo-won
*Na Moon-hee as Kyung-baes mother
*Yoon Je-moon as Priest
*Kang Dong-won as Kidnapper
*Park Jin-woo   as Detective Park
*Kim Ik-tae as Chief of police
*Jang Won-young as Dr. Choi
*Lee Hyung-chul as Han Sang-woo
*Jeon Hye-jin as Lee Ae-sook
*Im Jong-yoon as Lee Jae-joon
*Jo Seok-hyun as Detective Jo
*Kim Young-pil as Han In-seok

==Background==
The film is based on the real-life abduction of nine-year-old Lee Hyeong-ho on January 29, 1991. Despite 87 ransom calls from the kidnapper to the childs parents asking for   ( ), Lees dead body was found in a ditch near his home in Apgujeong-dong 44 days after he went missing; he had been killed two days after being kidnapped.  The killer was never caught, and the 15-year statute of limitations on the case expired in 2006. 

==Awards and nominations==
2007 Grand Bell Awards 
* Nomination – Best Actor – Sol Kyung-gu

2007 Blue Dragon Film Awards
* Nomination – Best Film
* Nomination – Best Actor – Sol Kyung-gu
* Nomination – Best Director – Park Jin-pyo

2007 Korean Film Awards
* Nomination – Best Sound – Kim Suk-won, Kim Chang-seop

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 