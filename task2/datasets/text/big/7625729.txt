Look What's Happened to Rosemary's Baby
 
 
{{Infobox television film
| name           = Look Whats Happened to Rosemarys Baby
| image          = 
| caption        = 
| genre          = Horror
| director       = Sam OSteen
| producer       = Anthony Wilson
| writer         = Ira Levin, Anthony Wilson
| starring       = Stephen McHattie Patty Duke George Maharis Broderick Crawford Ruth Gordon Ray Milland Tina Louise Charles Bernstein
| cinematography = John A. Alonzo
| editing        = Bob Wyman ABC
| studio         = Paramount Television The Culzean Corporation
| released       = October 29, 1976
| network        = ABC
| runtime        = 100 min
| country        = US English
| budget         = 
| preceded_by    = Rosemarys Baby (film)|Rosemarys Baby
| followed_by    = 
}}
Look Whats Happened to Rosemarys Baby is a 1976 TV movie, and a sequel to the 1968 film Rosemarys Baby (film)|Rosemarys Baby. It has little connection to the novel by Ira Levin, on which the first film was based.  It is not based on Levins sequel novel, Son of Rosemary, which was published later, although there are some similarities (e.g. the child in both stories is called Andrew/Andy).

The only actor to return from the first film is Ruth Gordon, as Minnie Castevet.  Sam OSteen, an editor on the first film, directed this sequel.  Patty Duke, who plays Rosemary, was considered for the role in the 1968 film.

==Plot==

===The Book of Rosemary===
The first scene opens with the coven preparing for a ritual, only to discover that Adrian (Rosemarys baby), now eight years old, is missing from his room. Knowing Rosemary (Patty Duke) must be responsible for this, the coven members use her personal possessions to enable the forces of evil to locate her. Rosemary and Adrian are hiding in a synagogue for shelter. While hiding there, supernatural events begin to affect the rabbis. However, as they are seeking sanctuary in a house of God, the coven is unable to affect them.

The next morning, Guy (George Maharis), now a famous movie star, gets a call from Roman Castevet (Ray Milland). Roman informs Guy that both Rosemary and Adrian are missing and that Rosemary may attempt to contact him. Later that night, Rosemary and Adrian are sheltering in a bus stop. Rosemary makes a phone call to Guy, while Adrian plays with his toy car nearby. As soon as Guy answers the phone, Rosemary immediately issues instructions on how to send her money. Outside, some local children start teasing Adrian and bullying him by stealing his toy car. Suddenly, in a fit of rage, Adrian knocks the children unconscious to the ground. After hearing all the noise, Rosemary hangs up the telephone and runs outside to find Adrian. Attempting to flee, the pair are accosted by Marjean (Tina Louise), a prostitute who was witness to the incident. Marjean offers them to hide the pair in her trailer.

After a while, Rosemary asks Marjean to go see what had happened with the children. After Marjean comes back, she lies and tells Rosemary that two boys were killed. Marjean is obviously a follower of Roman and Minnie (Ruth Gordon), but she offers to help Rosemary get a ride on a bus to escape. After a bus finally arrives later that night, Rosemary enters and the doors slam shut behind her before Adrian can get on. Rosemary turns to the driver, only to discover that bus is empty and is driving itself. Marjean holds Adrian in her arms as he sees his mother for the last time, being taken away by the possessed bus.

===The Book of Adrian===
Over twenty years later, an adult Adrian (Stephen McHattie) and his best friend, Peter (David Huffman), are detained by police for speeding. When Adrian arrives at his home, which is his "Aunt" Marjeans cheap casino, she confronts him about his reckless behavior. She tells him that shes always worried about him ever since his parents were "killed in an automobile accident".
 joyride and instigates a fight with a gang of violent bikers. Peter finds Adrian, who tells him what happened and how he has been suffering from strange nightmares and violent urges.

Later that night, Roman and Minnie arrive at the casino pretending to be Adrians aunt and uncle. As they prepare for his birthday party, Minnie drugs Adrian into unconsciousness and dresses him up in a costume and devil makeup. Peter, who notices something is wrong, becomes even more suspicious when he sees the movie star Guy Woodhouse arriving. After Guy and Roman join the rest of the coven, they begin to chant, attempting to invoke Satan. Although it initially seems as though the ritual failed, Adrian is possessed and runs out on the casinos dance floor. Roman soon realizes that Satan is using Adrian to possess all the innocent people on the dance floor. Guy becomes frightened and runs away. Peter intercepts Guy and attempts to make him help save Adrian. Guy panics when Peter struggles with him, so he electrocutes Peter with a broken power cord.

===The Book of Andrew===
Adrian regains consciousness with amnesia in a hospital. He is kept there against his will, as his fingerprints match the set police found on the broken power cord used to kill Peter. A nurse named Ellen (Donna Mills) tells him his name is "Adrian"; however, he insists his name is "Andrew", because he remembers his mother calling him "Andrew". Not knowing if Ellen will believe him or not, he is hesitant about telling her what he remembers about the cult. Ellen does believe him and helps him escape. When Guy is notified of Andrews escape from the hospital, he fears Andrew may follow him and kill him in a fit of rage.

On the run, Andrew and Ellen stop at a motel, where she seduces him. She then admits to him that she is a cult member, and she drugs and rapes him. He falls asleep having a terrible nightmare of Ellen as a type of harpy that tears at his chest. When Andrew later wakes up and goes outside looking for Ellen, a speeding car tries to run him down. Andrew manages to get out of the way; however, Ellen is hit. The car crashes, killing the driver whom Andrew discovers was Guy. Confused and scared, Andrew runs away into the night.

The film ends with Roman and Minnie sitting in the waiting room of a hospital to visit their pregnant granddaughter. After the doctor informs them that the pregnancy should continue as normal, their granddaughter is revealed to be Ellen who survived her injuries. During the end credits, Ellen is seen giving birth to Andrews baby, Rosemarys grandchild.

==Cast==
*Stephen McHattie: Adrian/Andrew
*Patty Duke: Rosemary Woodhouse
*Broderick Crawford: Sheriff Holtzman
*Ruth Gordon: Minnie Castevet
*Lloyd Haynes: Laykin
*David Huffman: Peter Simon
*Tina Louise: Marjean Dorn
*George Maharis: Guy Woodhouse
*Ray Milland: Roman Castevet
*Donna Mills: Ellen
*Philip Boyer: Adrian, age 8
*Brian Richards: Dr. Lister
*Beverly Sanders: Interviewer

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 