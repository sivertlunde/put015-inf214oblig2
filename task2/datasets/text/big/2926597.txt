A City of Sadness
{{Infobox film
| name           = A City of Sadness
| image          = City of Sadness.jpg
| caption        = Japanese DVD cover
| director       = Hou Hsiao-hsien
| producer       = Chiu Fu-sheng
| writer         = Chu Tien-wen Wu Nien-jen
| starring       = Tony Leung Chiu Wai Sung Young Chen Jack Kao Li Tian-lu
| music          = S.E.N.S.
| cinematography = Chen Huai-en
| editing        = Liao Ching-song
| studio         = 3-H Films
| distributor    = Era Communications (Intl rights)
| released       =  
| runtime        = 157 minutes
| country        = Taiwan Taiwanese Mandarin Mandarin Japanese Japanese Cantonese Shanghainese
| budget         =
}} Taiwanese historical White Terror" Kuomintang government (KMT) after their arrival from mainland China in the late 1940s, during which thousands of Taiwanese were rounded up, shot, and/or sent to prison. 

The film was the first to deal openly with the KMTs authoritarian misdeeds after its 1945 turnover of Taiwan from Japan, and the first to depict the 228 Incident of 1947, in which thousands of people were massacred.

A City of Sadness was the first Taiwanese film to win the Golden Lion award at the Venice Film Festival.

==Synopsis==
The film depicts the Lin familys experiences during the White Terror. The eldest brother Wen-heung (Sung Young Chen) is murdered by a Shanghai mafia boss, the middle brother Wen-leung (Jack Kao) suffers a traumatic brain injury in a KMT jailhouse, and the youngest brother Wen-ching (Tony Leung Chiu Wai), who is both deaf and mute, hopes to flee to the mountains with his friend to fight in the anti-KMT resistance movement. By the end of the film even the photographer Wen-ching has been arrested by the authorities, leaving only his wife to tell the story of the familys destruction.
 Japanese rule), Chen Yis ROC administration.

==Cast==
===The Lin Family===
*Tony Leung Chiu-Wai as Wen-ching
*Sung Young Chen as Wen-heung
*Jack Kao as Wen-leung
*Li Tian-lu as Ah-lu (Grandfather)

===Other Roles===
*Xin Shufen as Hinomi
*Wu Yi-Fang as Hinoiei
*Nakamura Ikuyo as Shizuko
*Jan Hung-Tze as Mr. Lin
*Wu Nien-jen as Mr. Wu
*Zhang Dachun as Reporter Ho
*Tsai Chen-nan as singer (cameo appearance)

==Production==
A City of Sadness was filmed on location in Jiufen, an old and declined gold mining town in northeast of Taiwan. The film revived Jiufen, and it became a popular tourist attraction. 
 The Puppetmaster (1993) and Good Men, Good Women (1995).

==Awards and nominations==
*46th Venice Film Festival
** Won: Golden Lion  
** Won: UNESCO Prize

*1989 Golden Horse Film Festival
** Won: Best Director – Hou Hsiao-hsien
** Won: Best Leading Actor – Sung Young Chen
** Nominated: Best Film
** Nominated: Best Screenplay – Chu Tien-wen, Wu Nien-jen
** Nominated: Best Editing – Liao Ching-song

*1989 Kinema Junpo Awards
** Won: Best Foreign Language Film – Hou Hsiao-hsien

*1991 Mainichi Film Concours
** Won: Best Foreign Language Film – Hou Hsiao-hsien

*1991 Independent Spirit Awards
** Nominated: Best Foreign Film – Hou Hsiao-hsien

*1991 Political Film Society
** Won: Special Award

==References==
 

==Further reading==
*Berenice Reynaud, A City of Sadness, British Film Institute, 2002.

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 