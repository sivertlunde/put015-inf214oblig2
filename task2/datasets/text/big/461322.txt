City of God (2002 film)
 
{{Infobox film
| name            = City of God
| image           = CidadedeDeus.jpg
| alt             = 
| caption         = Original poster
| director        = Fernando Meirelles Kátia Lund (co-director)
| producer        = Andrea Barata Ribeiro Mauricio Andrade Ramos Elisa Tolomelli Walter Salles
| screenplay      = Bráulio Mantovani
| based on        =   Alexandre Rodrigues Alice Braga Leandro Firmino Phellipe Haagensen Douglas Silva Jonathan Haagensen Matheus Nachtergaele Seu Jorge Roberta Rodrigues Graziella Moretto Antonio Pinto
| cinematography  = César Charlone (cinematographer)|César Charlone
| editing         = Daniel Rezende Wild Bunch
| distributor     = Miramax Films   Buena Vista International
| released        =  
| runtime         = 130 minutes 135 minutes  
| country         = Brazil Portuguese (Brazilian)
| budget          = $3.3 million 
| gross           = $30.6 million 
}} crime drama 1997 novel Cidade de Deus suburb of Rio de Janeiro, between the end of the 1960s and the beginning of the 1980s, with the closure of the film depicting the war between the drug dealer Lil Zé and vigilante-turned-criminal Knockout Ned. The tagline is "If you run, the beast catches you; if you stay, the beast eats you", a proverb analogous to the English "Damned if you do, damned if you dont".
 Alexandre Rodrigues, Vidigal and Cidade de Deus itself.
 Best Directing Best Editing Best Writing (Adapted Screenplay) (Mantovani). Before that, in 2003 it had been chosen to be Brazils runner for the Academy Award for Best Foreign Language Film, but it was not nominated to be one of the five finalists. If it had been nominated, it would have been ineligible the next year for any other category. The film continued to attain worldwide critical acclaim.
 City of Men, which share some of the actors (notably leads Douglas Silva and Darlan Cunha) and their setting with City of God.

==Plot==
Chickens are being prepared for a meal when a chicken escapes and an armed gang chases after it in a favela called the Cidade de Deus ("City of God"). The chicken stops between the gang and a young man named Rocket (Buscapé), who believes that the gang wants to kill him. A flashback traces Rocket, the narrator, back to the 1960s. He lived incredibly poor. 

Three impoverished, amateur thieves known as the "Tender Trio" – Shaggy, Clipper, and Goose – rob and loot business owners; Goose is Rockets brother. The thieves split part of the loot with the citizens of the City, and are protected by them in return. Several younger boys idolize the trio and one, Lil Dice (Dadinho), convinces them to hold up a motel and rob its occupants. The gang agrees, resolving not to kill anyone, and tell Lil Dice to serve as lookout. They give him a gun and tell him to fire a warning shot if the police arrive but an unsatisfied Lil Dice fires a warning shot mid-robbery and guns down the motel inhabitants once the gang have run off. The massacre brings the attention of the police, forcing the trio to split up: Clipper joins the church, Shaggy is shot by the police while trying to escape the favela, and Goose is shot by Lil Dice after taking the thieving boys money while his friend Benny (Bené) watches.

Later in the 70s, Rocket has joined a group of young hippies. He enjoys photography, and likes one girl, but his attempts to get close to her are ruined by a group of petty criminal kids known as "The Runts". Lil Dice now calls himself "Lil Zé" ("Zé Pequeno"), and along with Benny has established a drug empire by eliminating all of the competition, except for one dealer named Carrot, and forcing Carrots manager Blackie (Neguinho) to work for him instead.

A relative peace has come over the City of God under the reign of Lil Zé, who avoids police attention by having an initiate kill a Runt. Zé plans to kill Carrot, but Benny talks him out of it.

Eventually, Benny and his girlfriend decide to leave the City, but during the farewell party Zé is distracted, and Blackie accidentally kills Benny while trying to shoot Lil Zé. As Benny was the only man holding Zé back from taking over Carrots business, his death leaves Zé unchecked and Carrot kills Blackie for endangering his life.

Following Bennys death, Zé beats up a peaceful man named Knockout Ned and rapes Neds girlfriend. After Neds brother stabs Zé, his gang retaliates by killing his brother and firing on Neds house and killing his uncle. Ned, looking for revenge, sides with Carrot and eventually a war breaks out between Carrot and Zé.

As the 80s begin, both sides enlist more "soldiers", with Zé providing weapons for the Runts and eventually the reason for the war is forgotten. One day, Zé has Rocket take photos of him and his gang. After Rocket leaves his film with a friend who works at a newspaper, a female reporter publishes one of the prints, since nobody can get into the City of God anymore. Rocket takes a romantic interest in the reporter, eventually losing his virginity to her.

Rocket thinks his life is endangered but agrees to continue taking photographs, not realizing Zé is very pleased with increased notoriety. Rocket then returns to the City for more photographs, bringing the film to its beginning. Confronted by the gang, Rocket is surprised that Zé is asking him to take pictures, but as he prepares to take the photo after forgetting the chicken, the police arrive, who drive off when Carrot arrives. In the gunfight, Ned is killed by a boy who has infiltrated his gang to avenge his father, a security guard who was killed by Ned during a bank robbery. The police capture Lil Zé and Carrot, planning to give the media Carrot, whose gang never paid off the police, while they steal Zés money and let him go. He is then murdered by the Runts who intend to run the criminal enterprise themselves. Rocket secretly takes pictures of both scenes, as well as Zés dead body, and brings them back to the newspaper.

Rocket is seen in the newspaper office looking at all of his photographs through a magnifying glass, and deciding whether to publish the photo of corrupt cops and become famous or the photo of Lil Zés body and get an internship. He decides on the latter and the film ends with the Runts walking around the City of God, making a hit list of the dealers they plan to kill in order to take over the drug business. They mention that a Comando Vermelho ("Red Command") is coming.

==Cast==
Many characters are known only by nicknames. The literal translation of these nicknames is given next to their original Portuguese name; the names given in English subtitles are sometimes different.

{| class="wikitable"
|-
! width = 20%| Name 
! width = 20%| Actor(s) 
! width = 15%| Name in English subtitles 
! width = 45%| Description
|- Alexandre Rodrigues (adult) Luis Otávio (child)|| Rocket || The main narrator. A quiet, honest boy who dreams of becoming a photographer, and the only character who seems to keep from being dragged down into corruption and murder during the gang wars.
|- sociopathic drug sadistic pleasure in killing his rivals. When his only friend, Benny, is struck by fate, it drives him over the edge. "Dado" is a common nickname for Eduardo, and "inho" a diminutive suffix; "dado" also means "dice".  The fact that he becomes Zé Pequeno as an adult may suggest that his Christian name is José Eduardo – Zé is a nickname for José, while pequeno means "little". However, since the name was chosen in a religious ceremony, it may also be unrelated to his actual name.
|-
| Bené ("Benny")||  Phellipe Haagensen (adult) Michel de Souza (child) || Benny || Zés longtime partner in crime, he is a friendly City of God drug dealer who fancies himself a sort of Robin Hood, and wants to eventually lead an honest life.
|-
| Sandro, nicknamed Cenoura ("Carrot") || Matheus Nachtergaele|| Carrot || A smaller-scale drug dealer who is friendly with Benny but is constantly threatened by Zé.
|-
| Mané Galinha ("Chicken Manny")|| Seu Jorge || Knockout Ned || A handsome, charismatic ladies man. Zé rapes his girlfriend and then proceeds to massacre several members of Neds family. Ned joins forces with Carrot to retaliate against Zé. His name was changed for the English subtitles because in English, "chicken" is a term for a coward (in Brazil it denotes popularity among women). "Mané" is a nickname for Manuel.
|-
| Cabeleira ("Long Hair") || Jonathan Haagensen || Shaggy || Older brother of Bené ("Benny") and the leader of the Tender Trio ("Trio Ternura"), a group of thieves who share their profit with the population of the City of God.
|-
| Marreco ("Garganey") || Renato de Souza || Goose || One of the Tender Trio, and Rockets brother.
|-
| Alicate ("Pliers")|| Jefechander Suplino|| Clipper || One of the Tender Trio. Later gives up crime and joins the church.
|-
| Barbantinho ("Little twine")|| Edson Oliveira (adult) Emerson Gomes (child)|| Stringy || Childhood friend of Rocket.
|-
| Angélica||  Alice Braga|| Angélica|| An old friend and love interest of Rocket, and later Bennys girlfriend, who motivates him to abandon the criminal life.
|- drug addict.
|-
| Filé com Fritas ("Steak with Fries")|| Darlan Cunha|| Steak with Fries|| A young drug addict hired by Zés gang.
|- Charles Paraventi A Arms weapons dealer.
|- Marina Cintra Marina Cintra || A journalist for Jornal do Brasil, who hires Rocket as a photographer. Rocket has his first sexual experience with her.
|- Touro ("Bull")|| Luiz Carlos Ribeiro Seixas || Touro || An honest police officer.
|- A corrupt police officer.
|- Thiago Martins Child leader of the Runts gang
|- Marcos Junqueira Child leader of the Runts gang
|}

==Production==
On the bonus DVD, it is revealed that the only professional actor with years of filming experience was  , as it was thought better to create an authentic, gritty atmosphere. This way, the inexperienced cast soon learned to move and act naturally. 

Prior to City of God, Lund and Meirelles filmed the short film Golden Gate as a sort of test run.  Only after then was the casting for City of God finalized . He also made a couple other shorts.

Appropriately, the film ends eavesdropping on the machinations of the "Runts" as they assemble their death list. The real gang "Caixa Baixa" (Low Gang) is rumored to have composed such a list. After filming, the crew could not leave the cast to return to their old lives in the favelas. Help groups were set up to help those involved in the production to build more promising futures .

==Reception==

===Box office=== reais ($10.3&nbsp;million).  The film grossed over $7.5 million in the U.S. and over $30.5 million worldwide (in U.S. Dollars). 

===Critical reception=== 100 greatest films of all time. 
Critic Roger Ebert gave the film a four-star review, writing "City of God churns with furious energy as it plunges into the story of the slum gangs of Rio de Janeiro. Breathtaking and terrifying, urgently involved with its characters, it announces a new director of great gifts and passions: Fernando Meirelles. Remember the name.". 

City of God was ranked third in Film4s "50 Films to See Before You Die", and ranked No.7 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010. {{cite web 
| title = The 100 Best Films Of World Cinema   7. City of God 
| url = http://www.empireonline.com/features/100-greatest-world-cinema-films/default.asp?film=7 
| work=Empire 
}} 
  It was also ranked No.6 on The Guardian s list of "the 25 Best Action Movies Ever".   Film   The Guardian
| url = http://www.guardian.co.uk/film/2010/oct/19/city-god-action
| work=The Guardian
 | location=London
 | first=Killian
 | last=Fox
 | date=19 October 2010
}}
 
It was ranked 1# in Paste (magazine)|Paste magazines 50 best movies of the decade of the 2000s. 

====Top ten lists====
The film appeared on several American critics top ten lists of the best films of 2003. 
* 2nd – Chicago Sun Times (Roger Ebert)
* 2nd – Charlotte Observer (Lawrence Toppman)
* 2nd – Chicago Tribune (Marc Caro)
* 4th – New York Post (Jonathan Foreman)
* 4th – Time Magazine (Richard Corliss)
* 5th – Portland Oregonian (Shawn Levy)
* 7th – Chicago Tribune (Michael Wilmington)
* 10th – Hollywood Reporter (Michael Rechtshaffen)
* 10th – New York Post (Megan Lehmann)
* 10th – New York Times (Stephen Holden)

===Awards and nominations===
City of God won fifty-five awards and received another twenty-nine nominations. Among those:

{| class="wikitable"
! Organization !! Award !! Person !! Result !! Ref
|- Best Director || Fernando Meirelles ||  
|rowspan=4|  
|- Best Adapted Screenplay || Braulio Mantovani ||  
|- Best Cinematography || César Charlone (cinematographer)|César Charlone  ||  
|- Best Film Editing || Daniel Rezende ||  
|-
| AFI Fest || Audience Award || ||   ||  
|- Best Foreign Language Film || ||   ||  
|- Best Editing || Daniel Rezende ||   ||  
|- Best Foreign Film  || Andrea Barata Ribeiro,  Mauricio Andrade Ramos, Fernando Meirelles   ||   ||  
|-
| British Independent Film Awards ||  Best Foreign Independent Film || ||   ||  
|-
| Chicago Film Critics Association Awards || Best Foreign Language Film || ||   ||  
|- Best Foreign Language Film || ||   ||  
|-
| Golden Trailer Awards || Best Independent Foreign Film || ||   ||  
|- Best Film || ||   || rowspan=6 |  
|-
| Best Director || Fernando Meirelles ||  
|-
| Best Adapted Screenplay || Bráulio Mantovani  ||  
|-
| Best Cinematography || César Charlone ||  
|-
| Best Editing || Daniel Rezende ||  
|-
| Best Sound || Guilherme Ayrosa, Paulo Ricardo Nunes, Alessandro Laroca, Alejandro Quevedo, Carlos Honc, Roland Thai, Rudy Pi, Adam Sawelson ||  
|-
| Best Actor || Leandro Firmino ||   || rowspan=2|  
|- 
| Best Actress || Roberta Rodrigues ||  
|- 
| Best Supporting Actor || Jonathan Haagensen ||   || rowspan=2|  
|-
| Best Supporting Actor || Douglas Silva ||   
|-
| Best Supporting Actress || Alice Braga ||   || rowspan=2|  
|-
| Best Supporting Actress || Graziela Moretto ||  
|-
| Best Art Direction || Tulé Peak ||   ||  
|-
| Best Costume Design || Bia Salgado, Inês Salgado ||    ||  
|-
| Best Makeup || Anna Van Steen ||    ||  
|-
| Best Soundtrack || Antonio Pinto, Ed Côrtes ||    ||  
|- Best Foreign Language Film || Fernando Meirelles ||   ||  
|-
| Las Vegas Film Critics Society Awards  || Best Foreign Language Film ||  ||  ||  
|-
| Motion Picture Sound Editors || Best Sound Editing in a Foreign Film || Martín Hernández, Roland N. Thai, Alessandro Laroca ||   ||  
|- Best Foreign Language Film|| ||   ||  
|-
| Prism Awards || Best Theatrical Film || ||   ||  
|-
| Satellite Awards || Best Foreign Language Film || ||   ||  
|- Southeastern Film Best Foreign Language Film || ||   ||  
|-
| Toronto Film Critics Association Awards || Best Foreign Language Film || ||   ||  
|-
| Toronto International Film Festival || Visions Award – Special Citation || ||   ||  
|}

==Music== score to Antonio Pinto and Ed Córtes. It was followed by two remix albums. Songs from the film:
* "Alvorada" (Cartola / Carlos Cachaça / Herminio B. Carvalho) - Cartola
* "Azul Da Cor Do Mar" (Tim Maia) - Tim Maia
* "Dance Across the Floor" (Harry Wayne Casey / Ronald Finch) - Jimmy Bo Horne
* "Get Up (I Feel Like Being a) Sex Machine" (James Brown / Bobby Byrd / Ronald R. Lenhoff) - James Brown
* "Hold Back the Water" (Randy Bachman / Robin Bachman / Charles Turner) - Bachman–Turner Overdrive
* "Hot Pants Road" (Charles Bobbit / James Brown / St Clair Jr Pinckney) - The J.B.s
* "Kung Fu Fighting" (Carl Douglas) - Carl Douglas
* "Magrelinha" (Luiz Melodia) - Luiz Melodia
* "Metamorfose Ambulante" (Raul Seixas) - Raul Seixas
* "Na Rua, Na Chuva, Na Fazenda" (Hyldon) - Hyldon
* "Nem Vem Que Não Tem" (Carlos Imperial) - Wilson Simonal
* "O Caminho Do Bem" (Sérgio / Beto / Paulo) - Tim Maia
* "Preciso Me Encontrar" (Candeia) - Cartola
* "So Very Hard To Go" (Emilio Castillo / Stephen M. Kupka) - Tower of Power

== See also ==
 
* Docufiction
* List of docufiction films
* Boyz n the Hood

==References==
 

==External links==
 
*  
*    
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 