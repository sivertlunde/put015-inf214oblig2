Sweeney Todd (1928 film)
 
 
{{Infobox film
| name           = Sweeney Todd 
| image          =
| caption        = Walter West 
| producer       = Harry Rowson
| writer         = George Dibdin-Pitt    (play)
| starring       = Moore Marriott  Iris Darbyshire   Judd Green   Charles Ashton
| music          = 
| cinematography = 
| editing        = 
| studio         = QTS Productions
| distributor    = Ideal Films
| released       = September 1928
| runtime        = 6,200 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} British silent silent crime Walter West and starring Moore Marriott, Judd Green and Iris Darbyshire. It was adapted from a play by George Dibdin-Pitt based on the legend of Sweeney Todd.  It was made at Islington Studios.

==Cast==
* Moore Marriott - Sweeney Todd  Amelia Lovett 
* Judd Green - Simon Podge 
* Charles Ashton - Mark Ingestre  Johanna 
* Philip Hewland - Ben Wagstaffe 
* Harry Lorraine - Mick Todd  Tobias Wragg

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 