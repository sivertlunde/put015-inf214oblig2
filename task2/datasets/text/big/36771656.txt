Agni Putrudu
{{Infobox film
| name           = Agni Putrudu
| image          = 
| caption        = 
| director       = K. Raghavendra Rao
| producer       = Akkineni Venkat 
| writer         = Paruchuri Brothers  
| screenplay     = K. Raghavendra Rao Sarada Rajani Sivaji Ganesan Chakravarthy
| cinematography = K. S. Prakash 
| editing        = Kotagiri Venkateswara Rao
| studio         = Annapurna Studios
| distributor    =
| released       =   
| runtime        = 178 minutes
| country        = India
| language       = Telugu
| budget         = 
| gross          =
}}

Agni Putrudu is a 1987 Telugu film drama, produced by Akkineni Venkat on Annapurna Studios banner, directed by K. Raghavendra Rao. Starring Akkineni Nageswara Rao, Akkineni Nagarjuna, Sarada (actress)|Sarada, Rajani, Sivaji Ganesan played the lead roles and music composed by K. Chakravarthy|Chakravarthy. The film recorded as Average at box-office.   
==Cast==
 
* Akkineni Nageswara Rao as Hari Hara Bharadwaja
* Akkineni Nagarjuna as Kaalidasu
* Rajani as Usha  Sarada as Brahmaramba
* Sivaji Ganesan as Chaitanya   Satyanarayana as Bhupathi 
* Nutan Prasad as Govardhanam
* Gollapudi Maruti Rao as Deeshithulu  Rallapalli as Narahari
* Suthi Velu as Anantham 
* P. L. Narayana as Srisilam
* Chalapathi Rao as Inspector Sampath Kumar
* Bhimeswara Rao as Judge
* Balaji as Ganapathi
* Vidya Sagar   Chitti Babu as Ganapathis Henchmen 
* Mucharlla Aruna as Manga 
* Jyothi as Gayatri 
* Rajitha as Jahnavi  
* Krishnaveni as Nurse
* Heera as Sakkubais Sister
* Dubbing Janaki as Govardhanams Wife
* Tatineni Rajeswari as Deeshithulus Wife 
* Y.Vijaya as Sakkubai
  

==Soundtrack==
{{Infobox album
| Name        = Agni Putrudu
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1987
| Recorded    = 
| Genre       = Soundtrack
| Length      = 24:32
| Label       = LEO Audio Chakravarthy
| Reviews     =
| Last album  = Collector Gari Abbai   (1987) 
| This album  = Agni Putrudu   (1987)
| Next album  = Kirai Dada   (1987)
}}
The music was composed by K. Chakravarthy|Chakravarthy. Lyrics written by Veturi Sundararama Murthy. Music released on LEO Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Cheerelu Vidichina SP Balu, P. Susheela
|6:04
|- 2
|Yerra Yerrani Bugga  Mano (singer)|Mano, S. Janaki 
|6:00
|- 3
|Jayaya Jaya Bhadraya SP Balu
|3:13
|- 4
|Kamalam Kamalam SP Balu, S. Janaki
|4:09
|- 5
|Mudduko Muddettu SP Balu, P. Susheela
|5:06
|- 5
|Hrudaya Dhamarukam SP Balu, P. Susheela
|4:57
|}

==References==
 

==External links==
* 

 
 
 
 
 

 