G.I. Blues
{{Infobox film
| name           = G.I. Blues
| image          = G.I._Blues_Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Norman Taurog
| producer       = Hal B. Wallis
| writer         = {{Plainlist|
* Edmund Beloin
* Henry Garson
}}
| starring       = {{Plainlist|
* Elvis Presley
* Juliet Prowse
* Robert Ivers
}}
| music          = Joseph J. Lilley
| cinematography = Loyal Griggs
| editing        = Warren Low
| studio         = Hal Wallis Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $4.3 million     Michael A. Hoey, Elvis Favorite Director: The Amazing 52-Film Career of Norman Taurog, Bear Manor Media 2013 
}}
G.I. Blues is a 1960 American musical comedy film directed by Norman Taurog and starring Elvis Presley, Juliet Prowse, and Robert Ivers. The movie was filmed at Paramount Pictures studio, with some pre-production scenery shot on location in Germany before Presleys release from the army. Victor, Adam, The Elvis Encyclopaedia, pp. 190-191.  The movie reached #2 on the Variety weekly national box office chart in 1960. The movie won a 2nd place or runner-up prize Laurel Award in the category of Top Musical of 1960.

==Plot== Specialist 5 3rd Armored Blue Suede Shoes" sung by someone named Elvis on a jukebox.

To raise money, Tulsa places a bet with his friend Dynamite (Edson Stroll) that he can spend the night with a club dancer named Lili (Juliet Prowse), who is rumored to be hard to get since she turned down one other G.I. operator, Turk (Jeremy Slate). Dynamite and Turk have vied for women before when the two were stationed in Hawaii. When Dynamite gets transferred to Alaska, Tulsa is brought in to take his place. He is not looking forward to it, but must go through with it.
 Southern charm and calls Lili "maam." She at first sees Tulsa as another Occupation Duty GI. Then after a day on the Rhine, Lili begins to fall for him. Tulsas friend Cookie, meanwhile, falls in love with Lilis roommate, Tina (Letícia Román) from Italy. In the end, Ricks and Marlas baby son Tiger helps Tulsa win the bet for the outfit—and Lilis heart.

==Cast==
* Elvis Presley as Spec. 5 Tulsa McLean
* Juliet Prowse as Lili
* Robert Ivers as PFC Cookie James Douglas as Rick
* Letícia Román as Tina
* Sigrid Maier as Marla
* Scotty Moore as himself
* D.J. Fontana as himself
* Arch Johnson as MSG McGraw
* Kenneth Becker as Mac (as Ken Becker)
* Carl Crow as Walt
* Beach Dickerson as Warren
* Trent Dolan as Mickey
* Fred Essler as Papa Mueller
* John Hudson as CPT Hobart
* The Jordanaires as Themselves Mickey Knox as Jeeter
* Erika Peters as Trudy
* Jeremy Slate as Turk
* Edson Stroll as Dynamite 
* Ron Starr as Harvey
* Ludwig Stössel as Owner, puppet show

==Background==
By 1960 it had been two years since Presley had made his last film, King Creole. Despite his previous three films being mostly slammed by the critics, they warmed to King Creole and its star.  Presley felt confident that he had a future in acting after this praise and he was looking forward to returning to Hollywood after his time in the army.

The script was written by Edmund Beloin and Henry Garson, who had done the final revisions for Hal Wallis on Dont Give Up the Ship (film)|Dont Give Up the Ship. In 1958 they came up with an original treatment for an Elvis Presley movie called Christmas in Berlin. It was later known as Cafe Europa before becoming GI Blues. Michael A. Hoey, Elvis Favorite Director: The Amazing 52-Film Career of Norman Taurog, Bear Manor Media 2013 

Eight months prior to Presley being discharged, in August 1959, producer Hal Wallis visited with him in Germany to go over the script for G.I. Blues and film some on-location scenes.  Although some scenes were used in the final film, Presley did not film at any time during his time there.  Elvis double, Private First Class Tom Creel, was used for some shots. 

The U.S. Army supplied tanks and vehicles on manoeuvres to be used in the filming, and appointed public information officer John J. Mawn (1915–2007) as technical advisor for the film. Mawn had presided over Presleys military press conferences. 

Presley returned to the U.S. in March 1960 and began work on the film in late April.  

Hal Wallis originally wanted Michael Curtiz to direct but eventually selected Norman Taurog. Dolores Hart, Joan Blackman and Ursula Andress were all tested to play the female lead before deciding on Juliet Prowse. 

==Release==
The film, which was not well received by critics, was released on November 23, 1960, and finished the year as the fourteenth biggest box office grossing film of 1960 generating $4.3 million.  

Despite critics being dismissive of the overall plot, they did praise Presleys acting ability and the film was nominated for three awards in 1961: Best Soundtrack album Grammy, Grammy for Best Vocal Performance, Album, Male, and WGA Best Written Musical.  Presleys return to the screen led to a riot in a Mexico City theater showing G.I. Blues, prompting the Mexican government to ban Presleys movies. 

G.I. Blues reached No. 2 on Variety (magazine)|Varietys weekly list of top grossing films in 1960.

It was noted in Variety that "the film seems to be a leftover from the frivolous musicals of the Second World War."

G.I. Blues was ranked 14th in Varietys annual national box-office ratings for 1960.

The success of G.I. Blues may have ironically been the catalyst for the formulaic films that Presley was to make for much of the 1960s. His next two films, Flaming Star and Wild in the Country, were more straight acting vehicles, with fewer songs and a more serious approach to the plot lines.  However, despite Presley relishing a meatier role and enjoying the chance to act dramatically, both films were less successful at the box office than G.I. Blues had been, resulting in a return to the musical-comedy genre with Blue Hawaii as his next film role.  Blue Hawaii proved to be even more profitable than G.I. Blues and set in stone the future of Presleys Hollywood career.

===Soundtrack===
The G.I. Blues soundtrack album was nominated for two Grammy Awards in 1960 in the categories Best Sound Track Album Or Recording Of Original Cast From A Motion Picture Or Television and Best Vocal Performance Album, Male. Edmund Beloin and Henry Garson were both nominated in 1961 by the Writers Guild of America for G.I. Blues in the category of Best Written American Musical.

==Soundtrack==
See G.I. Blues (album)

==References==
 

==External links==
*  
*  
*  
*  

===Movie reviews===
*   by Chad Plambeck at  
*   by Dan Jardine	at  .
*   by Andy Webb at  .

===DVD Reviews===
*   By Noel Murray at  , August 29, 2007.
*   by Paul Mavis at  , August 6, 2007.
*   by Fusion3600 at  .

 

 
 
 
 
 
 
 
 
 
 
 