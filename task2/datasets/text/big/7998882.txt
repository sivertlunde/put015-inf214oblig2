Gold Raiders
{{Infobox Film
  | name             = Gold Raiders
  | image            = Goldraduier.jpg
  | director         = Edward Bernds Jack Schwarz
  | writer           = William Lively Elwood Ullman George OBrien Moe Howard Larry Fine Shemp Howard Sheila Ryan Lyle Talbot Clem Bevans Monte Blue John Merton Hugh Hooker
  | music            = Alex Alexander June Starr
  | cinematography   = Paul Ivano	 Fred Allen
  | distributor      = United Artists
  | released         =   |
  | runtime          = 55 42" |
  | country          = United States English
  | budget           = $50,000 
}}
 Western film George OBrien Three Stooges. The picture was OBriens last starring role and the only feature film released during Shemp Howards second tenure with the trio. 

==Plot==
As peddlers The Three Stooges help insurance agent George OBrien outwit a gang of desperados who are after a valuable gold-mine shipment, led by local bigwig Sawyer (Lyle Talbot).

==Production notes== Three Stooges, Curly in place of Shemp. Gold Raiders marked the second and last feature film with Shemp Howard as part of the Stooges since the acts first screen appearance, the 1930 film Soup to Nuts, which also featured the Stooges original leader Ted Healy.

The 56-minute Gold Raiders was economically filmed in five days by director Edward Bernds, who also directed several of the Stooges shorts. Filmed on December 26–30, 1950, Bernds later commented "I should have never made that picture. It was an ultra-quickie shot in five days at the unbelievable cost of $50,000 ($ }} today), which, even then, was ridiculously low. Im afraid the picture shows it!"   

It was originally released by independent producer Jack Schwarz through United Artists in 1951. Although the principals worked well together, plans to pursue an OBrien-Stooges series were abandoned. The film was reissued to theaters in 1958, and television distributor AAP issued two home-movie abridgements on 8mm film in the 1960s. Solomon, Jon. (2002) The Complete Three Stooges: The Official Filmography and Three Stooges Companion; Comedy III Productions, Inc., ISBN 0-9711868-0-4 
 
Out of circulation for years, Gold Raiders was released on DVD in 2006 by Warner Bros.

Sheila Ryan co-stars as the granddaughter of tipster doctor Clem Bevans, while silent-film star Monte Blue enjoys a larger part than usual as a local mine owner. Some of the stunts were performed by Hugh Hooker, who also plays a juvenile role.

==Cast== George OBrien ...	George OBrien
*Moe Howard	 ... Moe (billed as The Three Stooges)
*Larry Fine	 ...	Larry (billed as The Three Stooges)
*Shemp Howard ...	Shemp (billed as The Three Stooges)
*Clem Bevans ...	Doc Mason
*Sheila Ryan ...	Laura Mason
*Lyle Talbot ...	Taggart
*Monte Blue	 ...	John Sawyer
*Fuzzy Knight ...	Sheriff
*Hugh Hooker ... Sandy Evans
*John Merton ... Clete
*Remy Paquet ... Singer
*Al Baffert ...	Bartender (billed as Andre Adoree)
*Roy Canada ...	Slim
*Bill Ward ...	Henchman

==See also==
*The Three Stooges filmography

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 