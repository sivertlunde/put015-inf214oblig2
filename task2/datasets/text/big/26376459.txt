Game of Death (2010 film)
 
{{Infobox film
| name           = Game of Death
| image          = Game of death poster-1-.jpg
| caption        = Film Poster
| director       = Giorgio Serafini
| producer       = {{plainlist|
* Billy Dietrich
* Philippe Martinez
* Rafael Primorac
}}
| writer         = {{plainlist|
* Jim Agnew
* Megan Brown
}}
| starring       = {{plainlist|
* Wesley Snipes
* Zoë Bell
* Gary Daniels
* Robert Davi
}}
| music          = Jesse Voccia
| cinematography = Erik Curtis
| editing        = {{plainlist|
* Kevin Budzynski
* Todd C. Ramsay
}}
| studio         = Stage 6 Films Voltage Pictures
| distributor    = Sony Pictures Home Entertainment United States|ref2=   }}
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $13 million  
| gross          = $173,496 (UAE) 
}}
Game of Death is a 2010 American action film directed by Giorgio Serafini, and starring Wesley Snipes, Zoë Bell, Gary Daniels and Robert Davi. The film was released on direct-to-video|direct-to-DVD in the United States on February 15, 2011.

== Plot == Catholic priest. The mission begins when Joness mentor in the CIA, Dietrich, informs him that his next assignment is to gather intelligence for the possible prosecution of American citizens Frank Smith, an arms dealer, and John Redvale, a hedge fund manager. Jones succeeds in being hired as Smiths bodyguard and accompanies Smith to the Redvale building, where Smith is supposed to obtain $100,000,000 in cash. Unknown assailants attack the vehicle in which Smith and Jones are travelling.  While Jones is distracted, Dietrich, flying above them in a helicopter with several other CIA agents, discovers that the others are traitors when they kill him. They are after the $100,000,000.

Jones and Smith survive the attack partly because Smith has a heart attack just as it begins. The driver is killed; Jones takes the wheel, loses the killers, and drives Smith to Detroit Medical Center, where he is provided with lifesaving care. The CIA traitors show up and begin killing hospital staff. Jones, the primary target of the killers, eludes them and manages to take several of them out until Floria takes him prisoner and takes him to new team leader Zander. However, Jones is knocked unconscious and left to take the blame for deaths, while Smith is taken, along with a doctor to meet Redvale so that the killers can get his $100,000,000.

Redvale decides the best course of action is to let the killers have the money, then hunt them down and kill them many years later. Meanwhile, Jones steals an ambulance and drives to Redvales building to save the doctor and eliminate his former team members. Accomplishing both of these missions, Jones then eludes scores of Detroit Police Department officers and escapes with a bag which contains approximately $25 million. As Jones leaves the cathedral and the priest to whom he made confession, he leaves the bag behind. Walking past the basketball court, a young man tosses a basketball to him, which reminds Jones that he is forgiven.

== Cast ==
* Wesley Snipes as Marcus Jones
* Aunjanue Ellis as Rachel
* Zoë Bell as Floria
* Ernie Hudson as Clarence
* Quinn Duffy as John Redvale
* Gary Daniels as Zander
* Robert Davi as Frank Smith
* Ron Balicki as Jimmy

== Release == Optimum Home Entertainment released it in the United Kingdom on 21 February 2011.   

== Reception ==
Ian Jane of DVD Talk rated it 2.5/5 stars and wrote, "Game Of Death is not a film in the least bit concerned with strong characters or even believability but it does offer up enough mindless action and violence to keep and hold the pace."   David Johnson of DVD Verdict called it "a mediocre thriller highlighted by some nifty pugilism".   Jamie Russell of Total Film rated it 2/5 stars and called it "your standard CIA-Mafia-doublecrossing-flashback-nonsense". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 