The Hitler Gang
{{Infobox film
| name           = The Hitler Gang
| image          = 
| caption        = 
| director       = John Farrow
| producer       = Buddy G. DeSylva Joseph Sistrom Kurt Neumann
| starring       = 
| music          = 
| cinematography = Ernest Laszlo
| editing        = Eda Warren
| studio         = Paramount Pictures
| distributor    = 
| released       =  
| runtime        = 101 min.
| country        = United States
| language       = English 
| budget         = 
| gross          = 
}}

The Hitler Gang is a 1944 American pseudo-documentary film which traces Adolf Hitlers political rise, purportedly based only on documentary fact.  The filmmakers chose to avoid casting stars in the lead roles,  assembling instead a remarkable company of lookalikes to play Hitler, Goebbels, Hess and other leading Nazis.

==Plot==
In 1918 a young soldier called Adolf Hitler recovers from being gassed during World War I. At the behest of the German army, he joins German nationalistic parties, espousing theories that Germany lost the war because they were stabbed in the back.  He rises to become dictator of Germany.

==Cast== Bobby Watson as Adolf Hitler
* Roman Bohnen as Captain Ernst Röhm
* Martin Kosleck as Joseph Goebbels
* Victor Varconi as Rudolph Hess
* Luis Van Rooten as Heinrich Himmler
* Alex Pope as Hermann Göring Pastor Niemöller
* Poldi Dur as Geli Raubal Angela Raubal
* Reinhold Schünzel as General Ludendorff
* Sig Ruman as General von Hindenburg
* Alexander Granach as Julius Streicher
* Fritz Kortner as Gregor Strasser
* Tonio Selwart as Alfred Rosenberg
* Richard Ryen as Adolf Wagner
* Albert Dekker (narrator)

==See also==
* Hitlers Children (1943 film)|Hitlers Children
* Hitler – Dead or Alive
* The Strange Death of Adolf Hitler

==References==
 

==External links==
*  
*   
*   at TCMDB
*   at New York Times

 

 
 
 
 
 
 
 
 
 
 
 