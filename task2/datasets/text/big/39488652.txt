Citizen Koch
  Koch brothers in particular. {{cite news
|url=http://www.newyorker.com/reporting/2013/05/27/130527fa_fact_mayer?currentPage=all
|title=A Word from Our Sponsor: Public television’s attempts to placate David Koch
|date=May 27, 2013
|first=Jane
|last=Mayer
|authorlink=Jane Mayer Scott Walker and his relationship with the Koch Brothers.

Citizen Koch was completed using funds from a successful Kickstarter campaign    after allegations that PBS pulled funding initially promised to it.  Since at least half of PBS funding relies on donations, it has been alleged that financial support for the film was pulled because the content was not favourable to the Koch brothers, who are major donors to the network. {{cite news
|url=http://host.madison.com/news/local/city-life/citizen-koch-filmmakers-claim-pbs-killed-their-doc-to-please/article_12817724-9c77-5f96-81ef-e330f8e06dd3.html
|title=Citizen Koch filmmakers claim PBS killed their doc to please Koch brothers
|date=May 21, 2013
|first=Rob
|last=Thomas
|work=The Capital Times}}  {{cite news
|url=http://www.prwatch.org/news/2013/05/12118/pbs-killed-wisconsin-uprising-documentary-citizen-koch-appease-koch-brothers
|title=PBS Killed Wisconsin Uprising Documentary "Citizen Koch" To Appease Koch Brothers
|date=May 20, 2013
|first=Brendan
|last=Fischer
|work=PR Watch}} 

Regarding the allegations of censorship, and The New Yorker article which helped bring the case to public attention, the PBS ombudsman has stated that:   

 

The film was accepted by the Sundance Film Festival.   

==References==
 

==External links==
* 
* 

==Further reading==
*{{cite web
|url=http://www.newyorker.com/online/blogs/newsdesk/2013/05/stephen-colbert-on-david-koch-and-pbs.html
|date=May 23, 2013
|title=Stephen Colbert on David Koch and PBS
|first=Jane
|last=Mayer
|authorlink=Jane Mayer
|publisher=The New Yorker (blog)}}
*{{cite news
|url=http://thedianerehmshow.org/shows/2013-05-21/intersection-political-influence-and-journalism
|title=The Intersection Of Political Influence And Journalism
|date=May 21, 2013
|first=Diane
|last=Rehm
|authorlink=Diane Rehm
|publisher=The Diane Rehm Show / WAMU}}
*{{cite web
|url=http://gothamist.com/2013/05/20/how_billionaire_david_koch_gets_pbs.php
|title=How Billionaire David Koch Gets PBS To Dance, Monkey, Dance
|first=Christopher
|last=Robbins
|date=May 20, 2013
|publisher=Gothamist (blog)}}
*{{cite web
|url=http://host.madison.com/news/local/city-life/citizen-koch-filmmakers-claim-pbs-killed-their-doc-to-please/article_12817724-9c77-5f96-81ef-e330f8e06dd3.html
|title=Citizen Koch filmmakers Claim PBS Killed Their Doc to Please Koch Brothers
|date=May 21, 2013
|first=Rob
|last=Thomas
|publisher=The Capital Times}}
*{{cite web
|url=http://www.nytimes.com/2013/08/13/arts/television/the-documentary-citizen-koch-regains-money.html
|first=Brian
|last=Stelter
|title=The Documentary ‘Citizen Koch’ Regains Money
|date=August 12, 2013
|publisher=New York Times}}

 
 
 
 
 
 
 
 
 


 