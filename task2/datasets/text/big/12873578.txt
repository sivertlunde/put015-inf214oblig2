Palattu Koman
{{Infobox film
| name           = Palattu Koman
| image          = palattu koman.jpg
| image size     =
| caption        = Sathyan in the role of Palattu Koman 
| director       = Kunchacko
| producer       = Geetha Salam
| writer         = P. K. Sarangapani (dialogues)
| narrator       = Sathyan S. P. Pillai Rushyendramani Bahadoor  Manavalan Joseph
| music          = Baburaj
| cinematography = 
| editing        = S. Williams
| studio         = Excel Productions Udaya Studios
| distributor    = 
| released       = 1962
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Palattu Koman is a Malayalam film made in 
India  in 1962.  , Indian Cinema Database; Retrieved: 2007-09-07  The story of the film is based on the life of Palattu Koman, a pioneer of the Indian martial art form Kalaripayattu. The film was directed by Kunchacko  (1912–1976) and produced by Geetha Salam (Excel Productions). Sathyaneshan Nadar|Sathyan, S. P. Pillai, Rushyendramani and Bahadoor were in the leading cast.  The music director, Baburaj, wrote a critically acclaimed soundtrack that included the number "Poove Nalla Poove" sung by  Santha P. Nair and penned by Vayalar Ramavarma. Palattu Koman, produced by Udaya Studios under the banner of Excel Productions Productions, and directed by Kunchacko, was an adaptation of a ballad from the Vadakkan Paatu. Following the grand success of the screen adaptation of a similar ballad, Unniyarcha (1961 film)|Unniyarcha (1961), the producers picked up another such ballad for Palattu Koman.

The film tells the story of Palaattu Koman, a pioneer of kalarippayattu. `Komappan, is an epic poem written by the noted poet Kundoor Narayana Menon, who in 1912 popularised the ballad of Paalattu Koman and his love affair with his enemys sister. The script by Sarangapani for this film closely follows this epic poem by Kundoor.

The film was mainly shot at the Udaya Studios and the backwaters of Kerala. Leading stars of the time like Satyan, Ragini, Kottayam Chellappan, S. P. Pillai, Bahadur played major roles in the film. Rushyendramani (Sree Ramani in the title cards), the popular singing star of early Telugu cinema who was noted for her heroic roles, appeared in a major role. Besides sword fighting that was mandatory in these tales, the film included fights between man and wild animals. In fact, the heros fight with a tiger was a highpoint of the film. These scenes were canned impressively using the available equipment and techniques of the time. Such fight scenes were factors that helped make the film a box office success.

It is interesting to note that the producers had planned to name the film Konki Amma or Kunki Amma. The gramophone records of the film have the title as Konki Amma on them. This has caused confusion with regard to the credits of the songs.

==Plot==
The film tells the story of a feud between two families, Thonnooram Veedu and Kaippulli Paalattu Tharavadu. Chandrappan (Kottayam Chellappan) is the head of Thonnooram Veedu. He has an eye on the treasure kept in an underground cell of the Paalaattu mansion. A dispute between the children of the two families results in a bloody fight. Chandrappan kills the husband and all nine children of Kunki Amma (Rushyendramani) of the Paalaattu family.

Kunki Amma is rescued by the chieftain of the jungle where she was taken to be killed. She gives birth to a boy and takes an oath to destroy the entire Thonnooram Veedu. She reaches the Paalaattu in disguise along with her aide Pankan (S. P. Pillai). With the blessings of the snakes guarding the treasure at the mansion, Kunki succeeds in escaping from there with the treasure. Kunkis son Koman (Satyan) who undergoes training in martial arts becomes a master in the art. He starts off on a mission, to eradicate the Thonnooram Veedu clan and thereby to fulfil the desire of his mother.

Koman falls in love with Unniyamma (Ragini) without knowing that she is the sister of Chandrappan. Chandrappan and his group take Kunki Amma into custody and torture her. Koman defeats Chandrappan and frees Kunki Amma. All the members of Thonnooram Veedu, except Unniyamma, are killed. Koman marries Unniyamma bringing the film to a happy end.

==Cast==
  Sathyan
* Manavalan Joseph
* Boban Kunchacko Kanchana
* Ragini
* Bahadoor
* S.P. Pillai
* Kottayam Chellappan
* SJ Dev
* Velayudhan Nair
* Jijo
* KS Gopinath
* Kundara Bhasi
* Baby Seetha
* Sunny
* Baby Savithri
* Jayanthi (Old)
* Ochira Sankarankutty
* Sreeramani
 

==Songs==
Out of the 12 songs penned by Vayalar Rama Varma and set to tune by M. S. Baburaj, some of them turned super hits. The hit duet sung by A. M. Raja and P. Susheela, `Chandana pallakkil veedu kaanan vanna... is considered as one of the best romantic songs in the language. The duet by Santha P. Nair and Jikki, `Poove nalla poove... is another popular hit. The other hits from this film include `Ayyappan kaavil amme... (P. Leela), `Manassinakkathu oru pennu... (K. P. Udayabhanu). Will be remembered: The only Malayalam film in which the noted singing star of South Indian cinema, Rushyendramani acted. The film will also be remembered for its good music. 

==Soundtrack== 
The music was composed by MS Baburaj and lyrics was written by Vayalar Ramavarma and P. K. Sarangapani. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aanakeramalayilu || Jikki, KP Udayabhanu, KS George || Vayalar Ramavarma || 
|- 
| 2 || Aanakkaara || K. J. Yesudas, P Susheela || P. K. Sarangapani || 
|- 
| 3 || Ayyappan Kaavilamme || P. Leela || Vayalar Ramavarma || 
|- 
| 4 || Bhaagyamulla Thampuraane || Jikki, KS George || Vayalar Ramavarma || 
|- 
| 5 || Chaanchakkam || Chorus, Jikki || Vayalar Ramavarma || 
|- 
| 6 || Chandanappallakkil || P Susheela, A. M. Rajah || Vayalar Ramavarma || 
|- 
| 7 || Kanneeru Kondoru || P. Leela || Vayalar Ramavarma || 
|- 
| 8 || Maane Maane || Santha P Nair || Vayalar Ramavarma || 
|- 
| 9 || Manassinakathoru Pennu || KP Udayabhanu || Vayalar Ramavarma || 
|- 
| 10 || Ooruka Padavaal || PB Sreenivas || Vayalar Ramavarma || 
|- 
| 11 || Poove Nalla Poove || P. Leela, Jikki, Santha P Nair || Vayalar Ramavarma || 
|- 
| 12 || Urukukayaanoru Hridayam || P Susheela || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*http://www.malayalachalachithram.com/movie.php?i=105
* 

 
 
 
 
 