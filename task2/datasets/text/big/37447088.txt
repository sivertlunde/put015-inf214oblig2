Prabhuvinte Makkal
 
{{Infobox film
| title          = Prabhuvinte Makkal
| image          = 
| caption        = 
| director       = Sajeevan Anthikad
| producer       = M. Sindhu
| writer         = Sajeevan Anthikad Madhu Kalabhavan Mani Salim Kumar Jijoy
| music          = Arakkal Nandakumar Joy Cheruvathur
| cinematography = Manoj Narayanan Manju Lal
| editing        = 
| studio         = Free Thought Cinema
| distributor    = Free Thought Cinema
| runtime        = 174 minutes
| released       =  
| language       = Malayalam
| gross          = 
| country        = India
}}
Prabhuvinte Makkal is a 2012 Malayalam drama film directed by Sajeevan Anthikkad, starring Vinay Forrt, Jijoy  Swasika and Madhu (actor)|Madhu. The film concerns a controversial theme of atheism.

Prabhuvinte Makkal is the debutant venture of Sajeevan Anthikkad, who has some previous experience in documentary films. The story is loosely based on the life of a real person, who was known for his atheist views.   

The film was blocked on YouTube on 11 February 2015. It was uploaded on YouTube on 8 January 2015 by the Kerala Freethinkers Forum, a rationalist and secularist group. Other videos uploaded by the forum on religious bigotry, superstition, and rationalism were also blocked. According to the director, Hindu communal forces were behind the block. 

==Plot==
The film centres around a septuagenarian industrialist called Dayadananda Prabhu (Madhu). He has two sons, Mani (Jijoy)and Siddhartha (Vinay Fort). Mani is a confirmed atheist. But Siddhartha, in spite of being a physics student, and completing post-graduation in it with high rank, is more into spiritualism and oriental occult. Prabhu wants Siddhartha to marry Devika (Swaasika) his classmate, and join the same college as lecturer. But by this time, Sidhartha is hopelessly enamored by spiritualism and the works of Fritjof  Kapra, the Austrian born American physicist.  He suddenly leaves home to Hrishikesh, the sacred place of Hindus; he is in search of a guru who can teach him Ashtasiddhi (The eight divine yogic powers).

Siddhartha spends almost seven years in Hrishikesh and comes across many divine men and gurus there. At the end of his spiritual wanderings, he achieves ‘realisation’ – of  the dark and diabolic side of spiritual industry, and returns home as a totally changed man. Coming back to his village after eleven years, Siddhartha is overwhelmed at the sight of religious revival in his society. Even Prabhu who was once an irreligious man has become the devotee of a god man (Prakash Bare).  He is also pleasantly surprised to know that Devika still remains unmarried as she could never forget him.
 

Prabhu is about to hand over 60 acres of his land to the god man to build a super specialty hospital. Siddhartha is very upset with the developments. Before transferring the assets Prabhu dies in a car accident in mysterious circumstances. For the first time in his life, Siddhartha is forced into action. With the staunch support and help of his brother Mani, and  Devika, Sidhartha unravels the dark truth behind his fathers death. In this effort, he is supported by the timely and honest efforts of Adithya (Kalabhavan Mani), the Deputy Superintendent of Police.

==Cast==
* Vinay Forrt as Siddharthan
* Swasika as Siddharathans fiance Madhu as Siddharathans father
* Jijoy as Mani, Siddharathans brother
* Kalabhavan Mani as the police officer
* Prakash Bare as the god-man
* Salim Kumar
* Tovino Thomas
* Ambika Mohan
* Kulappulli Leela
* Narendra Nayak as himself in a special appearance

==Reception==
The film received mixed reviews. Paresh C. Palicha of Rediff.com rated the film   and said that the film "has some interesting insights into religious thought but peters out along the way." He concluded his review writing, "On the whole, Prabhuvinte Makkal, turns out to be a let down because it promised a lot before the release." 

==References==

 

 
 
 
 
 