Secrets of a Superstud
{{Infobox Film
| name           = Secrets of a Super Stud
| director       = Morton M Lewis Alan Selwyn
| producer       = Morton M Lewis
| writer         = Gerry Levy Morton M Lewis Alan Selwyn Mark Jones Michael Cronin Jeannette Charles Cosey Fanni Tutti John Shakespeare Derek Warne Ross MacManus
| editing         = Peter Pitt
| distributor    = Butchers Film 1976
| runtime        = 90 min
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Secrets of a Super Stud, also known as Its Getting Harder All the Time and Naughty Girls on the Loose is a 1976 British Sex Comedy, one of many to be also filmed in a hardcore version for export. The film was shot at Twickenham Film Studios under the title “Custer’s Thirteen”, the general release prints and the hardcore version were processed at Kay Labs at Highbury, North London.  

Details of the film’s hardcore version were leaked by the magazine Cinema X (July 1976; vol.8.no5, page 22), after the magazine had become disillusioned by certain British filmmakers refusal to acknowledge they were shooting hardcore “at Cinema X magazine we know which directors have shot porno; we’ve talked to their stars.  But its little use quoting them, when the directors, producers, above all their distributors, vociferously deny everything.  We prefer honesty in our pages.”

==Plot==

Custer Firkinshaw (Anthony Kenyon) the owner of Bare Monthly Magazine is up to his neck in dirty pictures and sexy secretaries.  His hedonistic ways are however temporary halted when his Uncle Charlie dies and Custer is pitted against his relatives. His Uncle leaves Custer a fortune in the will, but only on the condition that he marries and has a child within 12 months, otherwise it all goes to his relatives. Custer’s money grabbing Aunt Sophie (Margaret Burton) knows only to well about Custers swinging ways, so keep tabs on him by hiring a crooked private detective Bernie Selby (Alan Selwyn).  When Custer visits a doctor both parties discover that due to Custers oversexed lifestyle hes only got 13 units of sexually activity left, meaning he has only 13 more attempts to father a child.  

When Aunt Sophie learns of this she plans to stitch Custer up calling on Selby to hire girls to seduce Custer and use up those potent 13 units of sexual activity.  Thereon in, its a race against time as Custer tries to find a suitable bride to impregnate while Selby’s girls pose as cat burglars, lost neighbours and even drag up as meter inspectors in order to catch lure Custer into temptation.

==Songs==

Secrets of a Super Stud also contains several songs that relate the film’s plot as if it were a western, using many double-entendres (‘Custer’s last stand’, ‘quick on the drawers’). The songs were written by Ross MacManus, and performed by MacManus and Laura Lee. They include:

* It’s Getting Harder All the Time (sung by MacManus)
* Custer Firkinshaw (sung by MacManus)
* This is Your Last Stand (sung by Lee)

==References==

Simon Sheridan, Keeping the British End Up: Four Decades of Saucy Cinema (2011) (fourth edition) Titan Books

==External links==
*  
*  
* 

 
 
 
 