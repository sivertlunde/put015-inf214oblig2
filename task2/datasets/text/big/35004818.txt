Has God Forsaken Africa?
 

{{Infobox film
| name           = Has God Forsaken Africa?
| image          = 
| caption        = 
| director       = Musa Dieng Kala
| producer       = Colette Loumède Christian Medawar
| writer         = 
| starring       = 
| studio         = National Film Board of Canada 
| distributor    = National Film Board of Canada
| released       = 2008
| runtime        = 52 minutes
| country        = Canada
| language       = 
| budget         = 
| gross          = 
| screenplay     = Musa Dieng Kala
| cinematography = Alex Margineanu
| editing        = Marie Hamelin
| music          = Musa Dieng Kala
}}

Has God Forsaken Africa? (original French title: Dieu a-t-il quitté l’Afrique?) is a Canadian 2008 documentary film.

== Synopsis ==
Brussels, August 1999. Two teenagers from Guinea are found dead in the landing gear of a plane arriving from Conakry. Each year, thousands of young Africans risk their lives to flee the African continent. Shocked by this phenomenon, the Senegalese-born director Musa Dieng Kala returns to Dakar to try to understand why they do it. He films five young adults seeking to immigrate to the West at any cost reflecting the international indifference, the indifference of the African leaders and a society with no resources.

== Awards ==
* Fespaco (Uagadugú) 2009
* Rendez-vous du cinéma québécois 2009

== References ==
 

==External links==
*Watch   at the National Film Board of Canada

 
 
 
 
 
 
 
 
 
 