Bhajarangi
 
{{Infobox film
| name = ಭಜರಂಗಿ Bhajarangi
| image = Movie_Poster_Bhajarangi.jpg
| caption =  Theatrical Poster
Boxoffice = 44 crore theatrical collections + 10 crores from audio, satellite & T.V rights
| language = Kannada
| country = India
| starring = Shivrajkumar Aindrita Ray Rukmini Vijayakumar
| director = Harsha (director)|A. Harsha
| producer = Nataraj Gowda   Manjunath Gowda
| story = Harsha
| cinematography = Jai Anand 
| released =  
| music = Arjun Janya
| budget = 
| box office = 
}}

Bhajarangi ( ) is a Kannada-language Indian fantasy film starring Shivrajkumar and Aindrita Ray in the lead roles. Directed by choreographer-turned-director Harsha (director)|Harsha, the movie was released on 12 December 2013 and received positive response from critics. Bhajarangi collected a total of 12.5 crores in its 25 days run and became the highest-grossing Kannada film of 2013.   The film was declared a blockbuster at the box office.  . chitraloka.com. Retrieved on 12 July 2014. 

==Plot==
The story takes place in a hamlet called Ramadurga. A tantrik makes life hell for the villagers by looting their valuables, raping women and killing them. Jeevan (Shivarajkumar)comes to the hamlet to know his Janma rahasya (mystery of his birth). To his shock, he comes to know that he was the grandson of Bhajarangi (Shivarajkumar) who fought against the tantrik and saved the villagers from his harassment. But the intelligent tantrik gets rid of Bhajarangi in his own way by creating a demon in the form of a baby and makes Bhajarangi believe that he is his son. The story takes many interesting turns and twists & he defeats the tantrik.

==Cast==
* Shivrajkumar as Bhajarangi / Jeeva
* Aindrita Ray as Geetha
* Sadhu Kokila
* Bullet Prakash as James Bond
* Tabla Nani
* Girija Lokesh
* Rukmini Vijayakumar as Krishne
* Shivaram

The films introductory song features special appearances of the following actors: 
* Vijay Raghavendra
* Sri Murali
* Auditya
* Srinagar Kitty Yogesh
* Sathish Ninasam
* Arjun Janya

==Production==
Little is known of the story type and the script, but movie remains to be one of the most awaited Kannada movies of 2013.Actor Shivrajkumar had to lose weight for the movie and prepared for a six-pack body at the age of 51.  Major portion of the movie is shot around hesarghatta which lies to north west of Bengaluru.  . Indiaglitz.com (9 April 2013). Retrieved on 12 July 2014. 

Official trailer of the movie was launched in Triveni theatre Bangalore on September 2013 which was attended by Yash (Actor)|Yash. The introductory song featuring 6 leading actors of Kannada cinema was shot at Minerva Mills, Bangalore.

==Release==
The films producers Nataraj Gowda and Manjunath Gowda have teamed up with PVR Cinemas for the films domestic release. The film is expected to release simultaneously on 12 December 2013 in Karnataka, Maharashtra, Andhra Pradesh and Tamil Nadu in multiple theaters. The producers have reportedly booked more than 221 cinema halls across the Karnataka state alone. 

==Critical reception==
The Film opened to mostly positive reviews from the Critics.

* Times of India gave 4/5 stars and said "Shivarajkumar has combined all his experiences to give a brilliant performance. Be it action, sentiment or romance, Shivanna is at his best. Aindrita Ray too is at her best throughout, especially in the climax. Music by Arjun Janya is apt for the story and quite remarkable. Lokesh shines as a villain. Jai Anand is simply superb in his camerawork."  
 
* Chitraloka said "Shivarajkumar takes the cake for his easy in portraying both a lover boy and an action hero equally well. When he sheds his shirt to show off his six packs in the end, the joy of the fans sees no end. Harsha has done an excellent job all over. Arjun Janyas music has four good songs including the Boss song with guest appearance by six other film stars. Aindrita is bubbly as ever in a cute role. The actors who have played villains like Madhu, Chetan and Guru are a welcome change from the usual villans. If you have not booked your tickets already for Bhajarangi, do it fast. You will not get tickets easily for what is sure to be a blockbuster."  
 
* One India gave the film 3.5/5 stars and said "Bhajarangi is must watch movie for Shiv fans. It is also a movie, for which is to be admired by Kannada audience for its technical richness."   . Indiaglitz.com (13 December 2013). Retrieved on 12 July 2014. 
 
* India Glitz gave 8/10 and said "It is one of the brilliant executions of work from young and dashing director Harsha. It is time for Harshotsava! The 105th film of hat trick hero Shivarajakumar is magnificent in the second half and first half the film prepares for the second half big extravaganza."  

==Soundtrack==
The soundtrack and score for the film is composed by Arjun Janya.  

{{Infobox album  
| Name        = Bhajarangi
| Type        = Soundtrack
| Artist      = Arjun Janya
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    = Feature film soundtrack
| Length      = 
| Label       = Anand Audio
}}

{{Track listing
| total_length   = 
| lyrics_credits = 
| extra_column	 = Singer(s)
| title1         = Bossu Nam Bossu
| lyrics1  	= 
| extra1        = Arjun Janya
| length1       = 
| title2        = Jai Bhajarangi
| lyrics2 	= 
| extra2        = Shankar Mahadevan
| length2       = 
| title3        = Jiya Teri Jiya Meri
| lyrics3       =  Karthik
| length3       = 
| title4        = Re Re Bhajarangi
| extra4        = Kailash Kher
| lyrics4 	= 
| length4       = 
| title5        = Sri Krishna
| extra5       = Anuradha Bhat
| lyrics5 	= 
| length5       = 
}}

==References==
 

 
 
 
 