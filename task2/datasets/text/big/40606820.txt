The Pathfinder (1952 film)
{{Infobox film
| name           = The Pathfinder
| image          = Pathpos.jpg
| border         = yes
| caption        = Film poster
| director       = Sidney Salkow
| producer       = Sam Katzman
| writer         = 
| screenplay     = Robert E. Kent
| based on       = The Pathfinder by James Fenimore Cooper George Montgomery Helena Carter Jay Silverheels Michael Fox
| music          = Various
| cinematography = Henry Freulich 
| editing        = 
| studio         = Columbia Pictures
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Pathfinder is a 1952 Technicolor film inspired by The Pathfinder by James Fenimore Cooper.  Produced by  George Montgomery.

==Plot==
At the beginning of the French and Indian War in 1754, the Mingo Indians allied to the French massarcre the Mohican tribe allied to British.  Pathfinder and Chingachgook discover the only survivor, a child named Uncas.  Angered that the British did not protect their allies the Mohicans, Pathfinder gains entry to the British fort and threatens the Scottish commander Colonel Duncannon until it is discovered that the British were unaware due to a Mohican messenger being killed before he could bring the news.

Colonel Duncannon enlists Pathfinder and Chingachgook to spy for the British by posing as French sympathisers.  When Pathfinder says they would not be able to discover the plans of the French as they do not speak their language the Colonel assigns Alison, a fluent French speaker to them.  Pathfinder is dismayed that Alison is a woman but she earns her place by killing a Mingo with a pistol and infiltrating French society when they arrive at the French fort.  Alison discovers that the French have built a road along a mountain pass bringing supplies to the main French port that has a harbour for ships.  Blowing up the mountain road with black powder would deny supplies to the French fort meaning all their smaller outposts would fall to the English due to a scarcity of provisions.  

Alison came to the North American colonies to marry an English Captain who disgraced himself through alcoholism.  She unexpectedly meets him again as he has turned renegade, married a Mingo princess and has a commission in the French army. 


==Cast== George Montgomery Pathfinder   
Helena Carter  ...  Alison   
Jay Silverheels  ...  Chingachgook   
Walter Kingsford  ...  Col. Duncannon   
Rodd Redwing  ...   Chief Arrowhead    
Stephen Bekassy  ...  Col. Brasseau    
Elena Verdugo  ...  Lokawa    
Bruce Lester  ...  Capt. Clint Bradford    
Chief Yowlachie  ...  Eagle Feather    John Hart ...  French Sergeant   
Lyle Talbot ...  French Ship Captain    
Edward Coch ...  Uncas

==References==
 

==External links==
*  
*  


 
 
 
 
 
 
 
 
 