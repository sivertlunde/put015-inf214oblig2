The Woman in the Window
{{Infobox film
| name           = The Woman in the Window
| image          = WomanintheWindow.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| producer       = Nunnally Johnson
| director       = Fritz Lang
| screenplay     = Nunnally Johnson
| based on       =  
| starring       = Edward G. Robinson Joan Bennett Raymond Massey Dan Duryea
| music          = Arthur Lange
| cinematography = Milton R. Krasner
| editing        = Gene Fowler Jr. Marjorie Johnson
| studio         = International Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Woman in the Window is a 1944 film noir directed by Fritz Lang that tells the story of psychology professor Richard Wanley (Edward G. Robinson) who meets and becomes enamored of a young femme fatale. 
 The Grapes of Wrath (1940) and other John Ford films, and chose The Woman in the Window as its premiere project.  Director Fritz Lang substituted the films dream ending in place of the originally scripted suicide ending, to conform with the moralistic Production Code of the time.
 French film The Maltese Double Indemnity (1944), Laura (1944 movie)|Laura (1944), Murder, My Sweet (1944), and The Woman in the Window were released in France. 

==Plot==
After criminology professor Biesen, Sheri Chinen (2005). Blackout: World War II and the origins of film noir. Johns Hopkins University Press, ISBN 978-0-8018-8218-0  Richard Wanley sends his wife and two children off on vacation, he goes to his club to meet friends. Next door, Wanley sees a striking oil portrait of Alice Reed (Joan Bennett) in a storefront window. He and his friends talk about the beautiful painting and its subject. Wanley stays at the club and reads Song of Songs. When he leaves, Wanley stops at the portrait and meets Reed, who is standing near the painting watching people watch it. Reed convinces Wanley to join her for drinks.

Later, they go to Reeds home, but an unexpected visit from her rich lover Claude Mazard (Arthur Loft) leads to a fight in which Wanley kills Mazard. Wanley and Reed conspire to cover up the murder, and Wanley disposes Mazards body in the country. However, Wanley leaves many clues, and there are a number of witnesses. One of Wanleys friends from the club, district attorney Frank Lalor (Raymond Massey) has knowledge of the investigation, and Wanley is invited back to the crime scene, as Lalors friend, but not as a suspect. As the police gather more evidence, Reed is blackmailed by Heidt (Dan Duryea), a crooked ex-cop who was Mazards bodyguard. Reed attempts to poison Heidt with a prescription overdose when he returns the next day, but Heidt is suspicious and takes the money without drinking the drugs. Reed tells Wanley, who overdoses on the remaining prescription medicine.
 impossible match on action, Wanley awakens in his chair at his club, and he realizes the entire adventure was a dream in which employees from the club were main characters in the dream. As he steps out on the street in front of the painting, a woman asks Wanley for a light. He adamantly refuses and runs down the street.

==Cast==
* Edward G. Robinson as Professor Richard Wanley
* Joan Bennett as Alice Reed
* Raymond Massey as Dist. Atty. Frank Lalor
* Edmund Breon as Dr. Michael Barkstane
* Dan Duryea as Heidt/Tim, the Doorman
* Thomas E. Jackson as Inspector Jackson, Homicide Bureau
* Dorothy Peterson as Mrs. Wanley
* Arthur Loft as Claude Mazard/Frank Howard/Charlie the Hat check Man
* Iris Adrian as Woman who asks for a light

==Background==
As in Langs Scarlet Street, released a year later, Edward G. Robinson plays the lonely middle-aged man and Duryea and Bennett co-star as the criminal elements. The two films also share the same cinematographer - Milton R. Krasner - and several supporting actors.

==Reception==

===Critical response===
 
When the film was released, the staff at Variety (magazine)|Variety magazine lauded the film and wrote, "Nunnally Johnson whips up a strong and decidedly suspenseful murder melodrama in Woman in the Window. Producer, who also prepared the screenplay (from the novel Once off Guard by J.H. Wallis) continually punches across the suspense for constant and maximum audience reaction. Added are especially fine timing in the direction by Fritz Lang and outstanding performances by Edward G. Robinson, Joan Bennett, Raymond Massey and Dan Duryea."  The film holds a rare 100% fresh rating from Rotten Tomatoes.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*   essay by Spencer Selby at Film Noir of the Week
*  

===Streaming audio===
*   on Lux Radio Theater: June 25, 1945
*   on Theater of Romance: March 26, 1946

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 