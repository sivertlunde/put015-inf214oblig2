Geri's Game
{{Infobox film
| name           = Geris Game
| image          = Geris Game poster.jpg
| border         = yes
| caption        = Poster for Geris Game
| alt            = Poster for Geris Game
| director       = Jan Pinkava Karen Dufilho
| writer         = Jan Pinkava
| narrator       = Bob Peterson
| music          = Gus Viseur
| cinematography =
| editing        = Jim Kallett
| studio         = Pixar Animation Studios Walt Disney Pictures Buena Vista Pictures
| released       =  
| runtime        = 5 minutes
| country        = United States
| language       =
| budget         =
| gross          =
}}
 computer animated successful string Best Animated Short Film in 1998.

==Plot== Bob Peterson), elderly man piece except king and heart attack resigns the game and hands over a set of dentures as the prize. White Geri puts them in, then chuckles and grins in his victory, before the camera pulls back to reveal that he is alone at the chessboard.

==Development==
The first Pixar production to have a human main character, Geris Game was produced with the goal to "take human and cloth animation to new heights".   

The face of the character Geri resembles actor Jonathan Harris, who also provided the voice of Geri for his later appearance in Toy Story 2, where he is a toy repairman who fixes Sheriff Woody|Woodys torn arm. Chess pieces can be seen in his carrying case, in a nod to the short. Jan Pinkava, the films writer and director, has said the character is loosely based on himself and his elderly relatives, particularly his grandfather who was a frequent and avid chess player. 

This short movie served as a demonstration of a new animation technique using subdivision surfaces (NURBS was used to animate previous Pixar movies). 

==Chess inconsistencies==
The chess game itself has many mistakes with impossible moves being played and pieces changing locations between frames. The game begins 1.e4 e5 2.f3 d5 3.Be2 Nc6 4.Na3 Bxa3. At this point, the white bishop that had moved to e2 disappears from e2 and re-appears at f1. The game continues with 5.d4 Nxd4 6.Nh3 and then Black makes an impossible move, apparently capturing a pawn on c3 with his dark squared bishop. In the next frame that bishop is back on a3 and most of the subsequent moves are illegal/impossible. The game then fast-forwards to the end, where a checkmate already appears on the board, but neither player appears to understand that the game has ended. The white player switches the board around and then makes one last move to end the game.

==Release==
Geris Game premiered on November 24, 1997 at Laemmles Monica Theater in Santa Monica, California.    It was also attached to the theatrical release of A Bugs Life in 1998    and was subsequently featured on that films VHS and DVD releases.  A VHS recording of the short film was released in 1998, but it was not widely available and is considered rare. However, Geris Game can be found on the DVD Pixar Short Films Collection - Volume 1. It was also released on super-8 film with stereo sound.

==Awards==
;1997  Best Animated Short Film 
;1998 
* Anima Mundi Animation Festival – Best Film x2
* Annecy International Animated Film Festival – Jan Pinkava  Outstanding Achievement in an Animated Short Subject
* Florida Film Festival – Best Short
* World Animation Celebration – Best 3-D CGI by Professional Jan Pinkava 
* Zagreb World Festival of Animated Films – Internet Favourite

==References==
 

==External links==
*  
*  
*  
*  

 
{{succession box
| before = Knick Knack short films
| years  = 1997 For the Birds}}
 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 