Way for a Sailor
{{Infobox film
| name           = Way for a Sailor
| image          = Way for a Sailor.jpg
| image_size     =
| caption        =
| director       = Sam Wood
| producer       =
| writer         = Albert Richard Wetjen (book) Laurence Stallings W.L. River Charles MacArthur
| narrator       = John Gilbert Wallace Beery Leila Hyams
| music          =
| cinematography = Percy Hilburn Frank Sullivan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 85 minutes
| country        = United States English
| budget         =
}} John Gilbert.    The supporting cast includes Wallace Beery, Jim Tully, Leila Hyams, and Polly Moran.  The film was directed by Sam Wood, who insisted on no screen credit. The film is reputed to be so bad that the studio might have used it to sabotage leading man Gilberts career in the sound era due to animosity from Louis B. Mayer. MGM also produced a Spanish language version of this film, En cada puerto un amor, starring Jose Crespo and Conchita Montenegro.

==Plot==

 

==Cast== John Gilbert as Jack
*Wallace Beery as Tripod McMasters
*Jim Tully as Ginger
*Leila Hyams as Joan
*Polly Moran as Polly
*Doris Lloyd as Flossy

==References==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 


 