One Hamlet Less
{{Infobox film
| name           = One Hamlet Less
| image          = 
| caption        = 
| director       = Carmelo Bene
| producer       = Anna Maria Papi
| writer         = Carmelo Bene (from William Shakespeare)
| narrator       = Carmelo Bene
| starring       = Carmelo Bene
| music          = Carmelo Bene
| cinematography = Mario Masini
| editing        = Mauro Contini
| distributor    = 
| released       =  
| runtime        = 70 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

One Hamlet Less ( ) is a 1973 Italian drama film directed by Carmelo Bene. It was entered into the 1973 Cannes Film Festival.   

==Plot==
The film is a very loose version of the original tragedy of William Shakespeare|Shakespeare. Carmelo Bene is an obsessive actor who plays for various theaters tragedy Hamlet along with his theater company. Slowly he begins more and more to identify with the role and the psychology of the character until it becomes Hamlet himself.

==Cast==
* Carmelo Bene - Hamlet
* Luciana Cante - Gertrude
* Sergio Di Giulio - William
* Franco Leo - Horatio
* Lydia Mancinelli - Kate
* Luigi Mezzanotte - Laertes
* Isabella Russo - Ophelia
* Giuseppe Tuminelli - Polonius
* Alfiero Vincenti - Claudius

==References==
 

==External links==
* 

 

 
 
 
 
 
 