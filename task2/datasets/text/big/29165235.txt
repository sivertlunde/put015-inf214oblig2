Le Dindon
{{Infobox film
| name           = Le Dindon
| image          = 
| caption        = 
| director       = Claude Barma
| producer       = Silver Films, Armor Films (France)
| writer         = Georges Feydeau Jacques Emmanuel
| starring       = Nadine Alari Louis de Funès
| music          = Gérard Calvi
| cinematography = 
| editing        = 
| distributor    = Corona
| released       = 21 November 1951
| runtime        = 85 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French Comedy comedy film play "Le Dindon" (The Turkey). 

== Cast ==
* Louis de Funès: the manager
* Nadine Alari: Lucienne Vatelin, the notarys wife
* Jacqueline Pierreux: Armandine
* Denise Provence: Clotilde de Pontagnac
* Gisèle Préville: Maguy Pacarel
* Jane Marken: Mrs Pinchard
* Louis Seigner: Mr Pinchard
* Jacques Charon: Mr de Pontagnac Robert Hirsch: Mr Rédillon Jacques Morel: Maître Vatelin, notary and Luciennes husband
* Pierre Larquey: Géronte
* Fred Pasquali: Mr Pascarel
* Gaston Orbal: the commissioner
* Paul Bisciglia: Victor, the groom
* Léon Berton: the night look-out
* Marcel Méral: the second commissioner
* Georges Bever: a client
* Emile Mylos: Mr Grossback
* Jean Sylvère: the clerk

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 


 