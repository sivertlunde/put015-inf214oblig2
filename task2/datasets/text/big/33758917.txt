Dragnet (1954 film)
 {{Infobox film
| name           = Dragnet
| image          = Dragnet poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jack Webb
| producer       = Stanley D. Meyer
| screenplay     = Richard L. Breen Harry Essex Jack Webb Ben Alexander Richard Boone Ann Robinson Stacy Harris Virginia Gregg Vic Perrin
| music          = Walter Schumann Edward Colman
| editing        = Robert M. Leeds
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Ben Alexander, Richard Boone, Ann Robinson, Stacy Harris, Virginia Gregg and Vic Perrin. The film was released by Warner Bros. on September 4, 1954.  

==Plot==
 

== Cast ==
*Jack Webb as Sergeant Joe Friday Ben Alexander as Officer Frank Smith
*Richard Boone as Capt. James E. Hamilton
*Ann Robinson as Officer Grace Downey
*Stacy Harris as Max Troy
*Virginia Gregg as Ethel Starkie
*Vic Perrin as Deputy D.A. Adolph Alexander 
*Georgia Ellis as Belle Davitt
*James Griffith as Jesse Quinn
*Dick Cathcart as Roy Cleaver
*Malcolm Atterbury as Lee Reinhard
*Willard Sage as Chester Davitt
*Olan Soule as Ray Pinker 
*Dennis Weaver as Police Capt. R.A. Lohrman
*Monte Masters as Fabian Gerard
*Herb Vigran as Mr. Archer
*Virginia Christine as Mrs. Caldwell
*Guy Hamilton as Walker Scott
*Ramsay Williams as Wesley Cannon

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 