Blind Ambition (film)
{{Infobox film
| name           = Blind Ambition
| image          = Blind Ambition 2008 film.png
| caption        =
| director       = Bala Rajasekharuni
| producer       = 
| writer         = Alpesh Patel Bala Rajasekharuni
| starring       = {{Plainlist|
*Gulshan Grover
*Christopher Atkins 
*Vanessa Angel 
*Deep Katdare 
*Michelle Massey
*Bella Thorne}}
| music          = 
| cinematography = 
| editor         = 
| distributor    = 160-H Productions
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $1.200.000
| gross          = 
}}
Blind Ambition is an American drama film directed, produced and written by Bala Rajasekharuni.  It stars Gulshan Grover, Christopher Atkins, Vanessa Angel, Deep Katdare, Michelle Massey and Bella Thorne. The film was released on March 8, 2014.  The producers donated the proceeds to the India Center charity project. 

== Synopsis ==
Sapna Shah (Michelle Massey) is a teenage indian blind girl who runs a marathon as a tribute to the memory of her late mother.

== Cast ==
*Michelle Massey as Sapna Shah
**Jaysha Patel as Young Sapna
*Gulshan Grover as Dr. Shah
*Christopher Atkins as Wild Bill
*Vanessa Angel as Autumn Milestone
*Deep Katdare as Surya Shah
*Bella Thorne as Annabella
*Suny Behar as Henry Milestone
*Chris Blount as Thomas Peterson
*Brittney Bomann as Mandy
*Soren Bowie as Shane
*Erin Denardo as Jessica

==References==
 

== External links ==
* 

 
 
 
 
 
 


 