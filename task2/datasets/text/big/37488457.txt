Shadowland: The Legend
 
{{Infobox film

| name = Shadowland: The Legend

| image = USA film poster.jpg

| based on = Frank, Big Baba and Forty Thieves

| director = Johanna Kern

| writer = Johanna Kern

| producer = Johanna Kern

| studio = After Rain Films Film Factory Studio

| country = Canada

| language = English

| released =  
}}
Shadowland: The Legend, aka Shadowland, The Legend, is a 2012 Canadian independent fantasy family film.               The film is based on a stage production Frank, Big Baba and Forty Thieves, written, directed and produced by Johanna Kern,  who later adapted the story for the screen, as well as directed and produced it under After Rain Films banner              in association with Factory Film Studio.

== Plot ==
Frank (Nathan Pidgeon)      and his sister Caroline (Agnes Podbielska)   accidentally get transported to mythical Attic Town in a world of mystery, adventure and intrigue: Shadowland. The land is ruled and robbed by The Great Syndicate and, while scary Shadows kidnap anyone who is out after dark, the Patrolling Thieves make the daylight miserable for everyone. Not realizing that they shifted in space and time, the siblings split, while looking for directions. Frank meets Donlore (Andrew Guy),  a young Patrolling Thief, and trusts this shadowy figure despite his first instincts. Caroline finds herself on a farm outside of town. She soon sets on a journey to find her brother and help him realize his mistake. She has to find him before dark. When the night falls, he will remain one of the dark rulers forever and there will be no way back. In the meantime, the underground opposition of Attic Town begins to revolt.      

== Production ==
Shadowland: The Legend was shot under a working title Frank, Big Baba and Forty Thieves.              With its visual effects, and a large cast of hundreds of actors, young performers, professional martial artists and extras, the film has become an unusually big production for an independent film in the fantasy / family genre.       The Shadows were played by dozens of high-profile martial artists, including members of the Canadian martial artist team.   The film took several years in post-production, due to extensive financial problems and Visual Effects that took 4 and a half years to complete.   

== Music ==
 Piotr Kominek Lukasz Pilch.   iTUNES in the POP MUSIC section. 

== Cast ==

The film’s cast members are all Canadian talent, including first-timers as well as seasoned actors. Nathan Pidgeon (Frank)  and Agnes Podbielska (Caroline, Frank’s sister) are both first-timers to film.   Donlore, a Patrolling Thief who takes young Frank under his wing, is played by seasoned actor Andrew Guy, who has had roles in a number of feature films including The Rhino Brothers, Cooler Climate, Man with a Gun, Kissed and Intersection (with Richard Gere), as well as notable television guest appearances on Sliders, Highlander and others.  
The film also stars Mark Whelan (Revil), who is the face of the Sleeman™ franchise.  
Vieslav Krystyan plays the role of Centres, leader of the underground opposition. Krystyan, a seasoned actor, performed in numerous feature films and TV series including Fugitive Pieces, 54, Darkman III, Nikita, The Shrine, Soul Food, Relic Hunter, PSI Factor: Chronicles of the Paranormal, Kung Fu: The Legend Continues among others.  

== Distribution ==

"Shadowland: The Legend" was picked up for  , Amazon.com|Amazon, Google Play, Hulu, Fandor and Big Start.

== News ==
 Amazon and other retailers.

== References ==

 

== External links ==
*  
*  
*  

 
 
 
 
 