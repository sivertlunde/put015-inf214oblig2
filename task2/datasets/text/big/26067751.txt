El tejedor de milagros
{{Infobox Film
| name           = El tejedor de milagros
| image          = 
| image size     = 
| caption        = 
| director       = Francisco del Villar
| producer       = Rafael Lebrija
| writer         = Julio Alejandro Hugo Argüelles Emilio Carballido
| narrator       = 
| starring       = Pedro Armendáriz
| music          = 
| cinematography = Gabriel Figueroa
| editing        = José W. Bustos
| distributor    = 
| released       = 27 December 1962
| runtime        = 95 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| preceded by    = 
| followed by    = 
}}

El tejedor de milagros ("The Weaver of Miracles") is a 1962 Mexican drama film directed by Francisco del Villar. It was entered into the 12th Berlin International Film Festival.   

==Cast==
* Pedro Armendáriz
* Columba Domínguez
* Begoña Palacios Sergio Bustamante
* Enrique Lucero
* Aurora Clavel
* Hortensia Santoveña
* José Gálvez (actor)|José Gálvez
* José Luis Jiménez
* Pilar Souza
* Fanny Schiller
* Ada Carrasco
* Virginia Manzano
* Sadi Dupeyrón
* Lupe Carriles
* Miguel Suárez
* Victorio Blanco
* Socorro Avelar

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 