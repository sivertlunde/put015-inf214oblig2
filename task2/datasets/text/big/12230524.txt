Cold Steel (1987 film)
{{Infobox Film
| name           = Cold Steel
| image          = Cold Steel DVD cover.jpg
| image_size     = 
| director       = Dorothy Ann Puzo Michael Sonye
| producer       = Lisa M. Hansen Brad Davis Sharon Stone Jonathan Banks Adam Ant
| music          = David A. Jackson
| cinematography = Thomas F. Denove
| editing        = David Bartlett
| distributor    = Cinetel Films
| released       = December 11, 1987
| runtime        = 91 minutes
}} Brad Davis, Sharon Stone, Jonathan Banks and Adam Ant.

==Plot==
It begins when detective Johnny Modine (Davis) gets his Christmas celebration spoiled with the news about his fathers death, which is the work of psychopathic junkies who slashed the old man to death while robbing his store. Johnny is determined to find the person responsible and get his revenge, even if it means the end of his police career.

Johnny doesnt know that the murder was actually part of a sinister revenge plot directed against him. Leader of those murderous thugs is his former friend and colleague Isaac (Banks) who blames Johnny for the incident that left him crippled many years ago. But before he gets to Isaac, Johnny must overcome many obstacles, including Kathy (Stone), an attractive but mysterious woman with a hidden agenda.

==Home Video Release==

After the films theatrical run, the film was released on videocassette in March 1988 by RCA/Columbia Pictures Home Video. On December 2, 2002, Columbia TriStar Home Video released the film on DVD. The DVD has now been discontinued and as of May 26, 2010, no plans have been announced by Sony Pictures to re-release the DVD.

==External links==
*  

 

 
 
 
 


 