Are You Listening!
{{Infobox film
| name = Are You Listening!
| film name = শুনতে কি পাও!
| image = Are You Listening! (2012), film poster.jpg
| caption = Theatrical release poster
| director = Kamar Ahmad Simon
| producer = Sara Afreen
| co-producers =
| starring = 
| editing = Saikat Sekhareswar Ray
| released =  
| runtime = 90 minutes
| country = Bangladesh
| language = Bangla
| budget =
| gross = 
| studio =
| distributor = Cat&Docs
}} Cinema Vérité Direct Cinema style, a genre almost missing until now in Bangladesh. Set against the backdrop of Cyclone Aila|Aila, (a tidal surge) that swept over Bangladesh in 2009, the film celebrates the joy, dream and the resilience of the common people of Bangladesh.

==Plot==
Rakhi (27) lives with her man Soumen (32) and son Rahul (6) in a small village named Sutarkhali next to the mangrove forests of Sundarbans in the coastal belt of Bangladesh. Along with around hundred other families, they live on the land for generations. On 25 May 2009, when Rahul is only four years old, a tidal surge sweeps over the coasts of Bangladesh, flooding the entire village and the lands they cultivate. Life changes for Rakhi, Soumen and Rahul as the entire village takes refuge on an age-old dyke, surviving on relief from outside. Are You Listening! is about a mother’s hope to ensure a dignified future for her son. Its about a jobless husband’s frustration for failing to provide for his family, and a community’s struggle to get back the land they have lost.

==Production==
‘Cyclone Aila|Aila’, a tidal surge swept away the coasts of Bangladesh on 25 May 2009. In December the same year director Kamar Ahmad Simon started traveling the areas, approximately 200-kilometer stretch from Bhola to Shatkhira on local boat in three months span on and off, completely unplanned. On these trips he made several stops on different locations and engaged with local debates for hours on tea-chats. On coming back to Dhaka, Kamar extracted out of these informal-chats forming a script based on these very characters and locations that led him go back to them again and again. Later in this phase, for script development and production, the film won two competitive Grant Awards from Jan Vrijman Fund in 2010.    Kamar also participated in the IDFA Academy Summer School in August same year to develop the idea further,    and in 2011 the film idea was awarded as the Best Pitch in Asian Forum for Documentary held in Kolkata.   
 guerrilla warfare style. Sometimes the shooting was planned for a week but wrapped in three days, and sometimes the schedule extended even for three weeks without any pre-planning.

The director was obsessed with his own vision of making the film, and eventually failed to hold-up the team of the young enthusiasts. But the director’s adamancy paid off when locals kind of understood the director more than his team, and almost the entire village stood up lending a hand in any way they could support the production. Volunteers from the village were then groomed to assist in the production handling the logistics. The production continued for 20 months with a series of tours made in every other week or month. Satyajit Ray Film and Television Institute and was very impressed with his work. Saikat liked the rush but agreed to edit given that he will work with the entire 170 hours of rush, something that Kamar failed to make the local Bangladeshi editors agree.
 Cannes 2002, FIEPRESCI Award, Awards list Berlin Talent Campus, Berlinale 2012.    Yet the post-production greatly suffered due to funding crisis until again it won the competitive Grant Award from Visions Sud Est, Switzerland for post-production in 2012.   

==Crew==
* Kamar Ahmad Simon (Writer, Director & Cinematographer)
* Sara Afreen (Producer)
* Saikat Sekhareswar Ray (Editor)
* Sukanta Majumdar (Sound Editor & Designer)
* Delwar Hossain Maruf (Production Manager)
* Nishitchandra Mistri (Production Manager)
* Waseq Rahman (Researcher)
* Jenny Zerin (Researcher)

==Release==
The film had its world premiere at the 55th DOK Leipzig in Germany as the ‘Curtain-Opener’ of the festival on 29 October 2012.  In a rare move as a documentary, it was released in theater in Bangladesh on 21 February 2014    in   and successfully ran for four weeks. Before that Muktir Gaan by Tareque Masud was the only documentary that was released in theater in Bangladesh after one decade of its making and ran a week.

==Reception==
The Director of the European Documentary Network (EDN) and Danish film critic Tue Steen Müller wrote about the film in his blog, A classic humanistic, cinematically brilliant work…brings back memories of the The Apu Trilogy|Apu-trilogy of Satyajit Ray.    

The 25th International Documentary Festival of Amsterdam (IDFA) introduced the film as,  A gorgeously shot observational documentary… filming from an unusually low camera angle…  

The Jury awarding the ‘Grand Prix’ in the main international competition section of 35th Cinema du reel in Paris    cited,  With patience and intelligence in a hostile environment Kamar knew how to stay at the good distance of the people...he filmed to build a humanist movie...in the tradition of the feature length documentary narratives Are You Listening! is a visual symphony and an ode to life.    

Dubai based film programmer and critic Özge Calafato wrote in the 2013 spring edition of European documentary magazine, the quarterly DOX,  Strong political content with a distinct artistic vision…doesnt want to be confined to any strict interpretation of the genre.  

One of the most popular Bengali newspaper Prothom Alo, with the largest Bangladeshi readership, published the news of winning ‘Grand Prix’ in 35th Cinema du reel in Paris in highlighted box in back page as a national achievement news.   

Also, the 15th years anniversary special publication, a coffee table book on Bangladesh’s achievement of 15 years between 1998-2013’, Prothom Alo mentioned two films – ‘Shunte Ki Pao!’ (Are You Listening!)    and Matir Moina (The Clay Bird) by Kamar’s mentor Tareque Masud.   

==Reviews==
*  
*  
*  
*  
*  
*  

==Awards & Accolades==
# Golden Conch for Best Feature-Length Documentary, Mumbai International Film Festival (MIFF) 2014, India   
# Best Cinematography, Mumbai International Film Festival (MIFF) 2014, India   
# Audience Choice Award, Seattle South Asian Film Festival (SSAFF) 2014, Seattle, USA   
# Grand Prix for Best Feature-Length Documentary, 35th Cinéma du Réel 2013, France   
# Jury Award, Film South Asia 2013, Nepal   
# Opening Night Film, 55th  Dok-Leipzig, Germany 2012   
# Grant Award: Distribution, Movies That Matter 2013, Netherlands   
# Editing Lab Project, Campus Studio/ Berlinale 2012, Germany   
# Grant Award: Post-production, Visions Sud Est 2011, Switzerland   
# Best Pitch, DocedgeKolkata, Asian Forum for Documentary 2011, India   
# Grant Award: Script, Jan Vrijman Fund (JVF/ IDFA) 2010, Netherlands   
# Grant Award: Production, Jan Vrijman Fund (JVF/ IDFA) 2010, Netherlands   

==Festivals==
{| class="wikitable" border="1"
|-
! Festival
! Category
! Result
|-
| 55th DOK Leipzig 2012, Germany   
| Opening Film & International Competition
|  
|-
|   25th IDFA 2012, Netherlands   
| Official Selection: Panorama
| 
|-
| 35th Cinéma du Reel, Paris 2013, France   
| International Competition
! colspan="2" style="background: #A9F5A9;" | Grand Prix
|- Adana Golden Boll Film Festival 2013, Turkey   
| Official Selection
|
|-
| Antenna Documentary Film Festival, Sydney 2013, Australia   
| International Competition
|  
|-
| DOK.fest München 2013, Germany   
| International Competition
|  
|-
| Erasmus Huis International Documentary Film Festival 2013, Indonesia   
| Official Selection
|
|-
| Film South Asia Kathmandu 2013, Nepal   
| International Competition
! colspan="2" style="background: #A9F5A9;" | Jury Award
|-
| Globale Film Festival Berlin 2013, Germany   
| Official Selection
|
|-
| Millennium Documentary Film Festival 2013, Belgium   
| International Competition
|  
|-
| Patmos International Film Festival 2013, Greece   
| Official Selection
|
|-
| PriFilmFest, 2013, Kosovo   
| Official Selection
|
|-
| Schlosstheater Moers 2013, Germany   
| Official Selection
|
|-
| Take One Action Film Festival 2013, UK   
| Official Selection
|
|-
| Yamagata Documentary Film Festival 2013, Japan 
| New Asian Currents Competition
|  
|-
| Zagreb Human Rights Film Festival,2013, Croatia   
| Official Selection
|
|-
| Abu Dhabi International Documentary Film Festival 2013, Abu Dhabi   
| International Competition
|  
|-
| Festival de Films Documentaries de Lasalle 2013, France   
| Official Selection
|
|-
| Asiatica Film Mediale 2013, Italy   
| Official Selection
|
|-
| Mumbai International Film Festival (MIFF) 2014, India   
| International Competition
! colspan="2" style="background: #A9F5A9;" | Golden Conch for Best Documentary
|-
| Mumbai International Film Festival (MIFF) 2014, India 
| International Competition
! colspan="2" style="background: #A9F5A9;" | Best Cinematography Award
|-
| Dok Leipzig Lake Festival 2014, India
| Official Selection
|
|-
| NALSAR Film Festival, India   
| Official Selection
|
|-
| Blede Festival 2014, Slovenia   
| International Competition
|  
|-
| Stronger than Fiction Documentary Film Festival 2014, Australia   
| Official Selection
|
|-
| Kerala International Documentary & Short Film Festival 2014, India   
| Official Selection
|
|-
| Climatefilmfestival: To Future with Love, Stockholm, Sweden   
| Official Selection
|
|-
| Chicago South Asian Film Festival 2014, Chicago, USA   
| Official Selection
|
|-
| Seattle South Asian Film Festival 2014, Seattle, USA   
| International Competition
! colspan="2" style="background: #A9F5A9;" | Audience Choice Award
|-
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 