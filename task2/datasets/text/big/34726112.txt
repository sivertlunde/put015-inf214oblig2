A Drink in the Passage
 
 
 
{{Infobox film
| name           = A Drink in the Passage
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Zola Maseko
| producer       = David Max Brown
| writer         = Alan Paton
| narrator       = 
| starring       = 
| music          = 
| distributor    = Africa Film Library/ M-Net
| released       = 2002 
| runtime        = 26 minutes
| country        = South Africa
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
A Drink in the Passage Adapted from a short story by Alan Paton, the celebrated South African writer who gave the world Cry, The Beloved Country, this short film is a stark reminder of the inhumanity and indignities of Apartheid. A Drink in the Passage is the story of Edward Simelane who is awarded first prize in a national fine arts competition for his remarkable stone sculpture, called Mother and Child. But unbeknownst to him, the competition is for whites only. Nonetheless, the judging panel decides to stick to its decision. In the ensuing national furor, the absurdities of legalised racial discrimination are gently, but tellingly, revealed.. 
 
==References==
 
== External links ==
*  

 
 
 
 
 

 