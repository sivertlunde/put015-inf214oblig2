The Bad Seed (1956 film)
{{Infobox film
| name           = The Bad Seed
| image          = TheBadSeed1956.jpg
| caption        = 
| director       = Mervyn LeRoy
| producer       = Mervyn LeRoy
| screenwriter   = John Lee Mahin The Bad Seed&nbsp;by   The Bad Seed&nbsp;by   Patty McCormack Henry Jones  Eileen Heckart Evelyn Varden
| music          = Alex North
| cinematography = Harold Rosson
| editing        = Warren Low
| distributor    = Warner Bros.
| released       =  
| runtime        = 129 minutes
| country        = United States
| language       = English
| gross          = $4.1 million (US)  
}}
 Henry Jones, and Eileen Heckart
 of the same name) by Maxwell Anderson, which in turn is based upon William Marchs 1954 novel The Bad Seed. The play was adapted by John Lee Mahin for the screenplay of the film.

==Plot==
On her piano, Rhoda Penmark (Patty McCormack) plays the French song "Au clair de la lune", while her father (William Hopper) says his goodbyes to her and his wife, Christine (Nancy Kelly), as he goes away on military duty. Their neighbor and landlord, Monica Breedlove (Evelyn Varden), comes in with a present for Rhoda (a locket). Rhoda, looking pristine and proper in her perfect dress and pigtails, thanks Monica for the gift. She then tap dances on the hard floor. Monica notices the tap shoes, and Rhoda says that adding the taps to the shoes was her own idea. They then discuss a penmanship medal competition that Rhoda lost to her schoolmate, Claude Daigle; Monica speaks of it as a childish disappointment, but Rhodas face darkens with fury. Christine and Rhoda leave for the school picnic at a nearby lake.
 traumatized by seeing the boy’s corpse. When Rhoda returns, however, she is unfazed by the incident and goes about her daily activities.

Rhodas teacher later visits Christine, revealing that Rhoda was the last person seen with Claude that day on the wharf and that she was seen grabbing at Claudes penmanship medal. As the two women sit talking, Claudes mother, Mrs. Daigle (Eileen Heckart), enters, and drunkenly accuses Rhoda of knowing something that she is not telling.

Later that night, Christine finds the penmanship medal in Rhodas room and demands an explanation. Rhoda lies that Claude let her have the medal after she won a bet. Later, however, Christine catches Rhoda trying to sneak out to the incinerator to dispose of her tap shoes, and Christine realizes that Rhoda must have hit Claude with the shoes, which explains the half-moon shaped bruises on his forehead and hands. A tearful Rhoda admits that she killed the boy and also confirms Christines suspicion that she murdered a neighbor lady when they lived in Wichita, Kansas|Wichita, the sooner to obtain a trifle the old lady had promised her. Christine orders Rhoda to burn the shoes in the incinerator.

In the midst of the revelations about Rhoda, Christines vague suspicions about having been adopted are confirmed: she is the biological daughter of a notorious serial killer, Bessie Denker, and was adopted at two years of age by her foster father and his late wife. Christine now worries that Bessie (and therefore Christine herself) is the cause of Rhodas antisocial personality disorder|sociopathy, and that her homicidal behavior is Genetics|genetic, not subject to influence, let alone reversal, by parenting or a wholesome environment.
 Henry Jones), heckles her that she killed Claude with her shoes and that he, LeRoy, took the burnt shoes as evidence. When Rhoda reacts in anger, LeRoy realizes his accusation, made in jest, is actually true. He opens the incinerator and finds what remains of the shoes. Christine and Rhoda go to dinner at Monicas and Mrs. Daigle returns, drunk, demanding to speak with Rhoda. Outside, off camera, Rhoda sets a sleeping LeRoys bedding ablaze to keep her secret safe. From the apartment window, Christine and Monica watch him burn (the viewer only hears LeRoys screams).  Christine babbles incoherently after witnessing the death; Monica realizes that Christine believes Rhoda is guilty of something awful, but still has no inkling that Christine believes Rhoda to have committed murder. That night, a visibly calm Christine tells Rhoda that she dropped the medal into the lake, and then gives her daughter a lethal dose of sleeping pills, telling her theyre her new vitamins. Then she attempts to kill herself with a gunshot to the head (in the book and play, she succeeds). Instead of being killed, however, Rhoda and Christine are found and taken to a hospital and both survive. In the middle of the night, during a storm, Rhoda sneaks out in a rain slicker and goes to the lake and out on the wharf to try to find the medal. Lightning strikes her, killing her instantly, unlike in the novel and play.

At the end of the story, the cast is introduced during a theatrical-style curtain call. After her credit is read, Nancy Kelly delivers a spanking to Patty McCormack (both laughing). The spanking continues as the film fades out; a screen card then requests that the audience not divulge the ending.

==Cast==
  
* Nancy Kelly as Christine Penmark
* Patty McCormack as Rhoda Penmark Henry Jones as Leroy Jessup
* Eileen Heckart as Hortense Daigle
* Evelyn Varden as Monica Breedlove
* William Hopper as Col. Kenneth Penmark
 
* Paul Fix as Richard Bravo Jesse White as Emory Wages
* Gage Clarke as Reginald Reggie Tasker
* Joan Croydon as Claudia Fern (as Joan Croyden)
* Frank Cady as Henry Daigle
 

==Censorship==
Although the novel and play had the mother dying and the evil child surviving, the Hays Code did not allow for "crime to pay." The ending of the film thus has it the other way around, with Christines life being saved by the local hospital and Rhoda being struck down by lightning while trying to retrieve the penmanship medal from the lake.

In another move to appease the censors, Warner Bros. added an "adults only" tag to the films advertising.  . Turner Classic Movies. Retrieved February 11, 2014. 

==Reception==
This film was one of Warner Bros. biggest hits of the year, grossing $4.1 million and one the years top 20 at the box office.   Also, The Bad Seed was one of the ten most popular movies at the British box office in 1956. BRITISH. FILMS MADE MOST MONEY: BOX-OFFICE SURVEY
The Manchester Guardian (1901-1956)    28 Dec 1956: 3 
 Academy Awards===
;Nominations    Best Actress: Nancy Kelly Best Actress in a Supporting Role: Eileen Heckart Best Actress in a Supporting Role: Patty McCormack
*  

==Remakes== remade for television in 1985, adapted by George Eckstein and directed by Paul Wendkos. It starred Blair Brown, Lynn Redgrave, David Carradine, Carrie Welles, Richard Kiley, Chad Allen, and Christa Denton. This version used the original ending as in the March novel.

==Popular culture==
The band Nick Cave and the Bad Seeds is named after the novel, play, and film. 
{{cite web|url=http://www.nick-cave.com/_biography.php
|title=Nick Cave Online
|accessdate=2009-06-25
|publisher=}} 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 