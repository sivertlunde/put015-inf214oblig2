List of lesbian, gay, bisexual or transgender-related films of 1991
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1991. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==1991==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|-
|Absolutely Positive|| 1991 ||   ||   || Documentary||
|-
| || 1991 ||   ||   || Drama||
|-
|Daddy and the Muscle Academy|| 1991 ||   ||   || Documentary ||
|- Edward II|| 1991 ||   ||   || Historical, drama||
|- Fried Green Lesbian subtext 
|- High Heels|| 1991 ||   ||    || Comedy, Drama, Romance ||
|-
| || 1991 ||   ||  || Drama||
|-
|My Father Is Coming|| 1991 ||   ||  || Comedy, Romance ||
|-
|My Own Private Idaho|| 1991 ||   ||  || Drama||
|-
|The Lost Language of Cranes|| 1991 ||   ||   || Drama ||
|-
|Mast Kalandar|| 1991 ||   ||  || Comedy, Drama|| First openly gay character in Bollywood and Indian cinema.
|- Naked Lunch|| 1991 ||   ||       || Fantasy, crime,drama||
|-
|No Skin Off My Ass|| 1991 ||   ||   || Comedy, drama||
|-
|North of Vortex|| 1991 ||   ||    || Drama||
|- Our Sons|| 1991 ||   ||  || Drama||
|-
|Poison (film)|Poison|| 1991 ||   ||  || Drama||Poison was the winner of the Teddy Award for best feature film at the 41st Berlin International Film Festival.
|-
|Salmonberries|| 1991 ||   ||  || Drama||
|- The Silence of the Lambs|| 1991 ||   ||  || Thriller||
|-
|Thelma & Louise|| 1991 ||   ||     || Adventure, crime, drama ||
|-
|Todo por la pasta|| 1991 ||   ||   || Thriller ||
|-
|Young Soul Rebels|| 1991 ||   ||        || Drama||
|}

 

 
 
 