The Trail of the Octopus
 
 
{{Infobox film
| name           = The Trail of the Octopus
| image          = The Trail of the Octopus.jpg
| caption        = Theatrical poster to The Trail of the Octopus
| director       = Duke Worne
| producer       =
| story          = J. Grubb Alexander
| starring       = Ben F. Wilson Neva Gerber
| cinematography =
| editing        =
| distributor    = Film Clearing House S.A. Lynch Enterprises
| released       =  
| runtime        = 300 minutes (15 episodes)
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 mystery film serial directed by Duke Worne.   

==Cast==
* Ben F. Wilson as Carter Holmes
* Neva Gerber as Ruth Stanhope William Dyer as Sandy MacNab
* Howard Crampton as Dr. Reid Stanhope
* William A. Carroll as Omar Marie Pavis as Mlle. Zora Rularde
* Al Ernest Garcia (as Allen Garcia) as Jan Al-Kasim
* Al Ernest Garcia (as E(a)rnest Garcia) as Wang Foo
* Harry Archer as Raoul Bornay
* C.M. Williams as Aboul Shabistari

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 