Chinnari Mutha
{{Infobox film
| name           = Chinnari Muttha
| image          =
| director       = T. S. Nagabharana
| producer       = Nandakumar
| screenplay     = T. S. Nagabharana C. Aswath Master Vijay Raghavendra H. G. Dattatreya
| music          = C. Aswath
| cinematography = B. C. Gowrishankar
| editing        = Suresh Urs
| studio         = Shruthalaya Films
| released       = 1993
| runtime        = 116 minutes
| language       = Kannada
| country        = India
| awards         =
| budget         =
}}
 Master Vijay Raghavendra in the lead roles.
 Best Feature Best Children Best Music Best Child Best Female Playback Singer (Manjula Gururaj). 

==Cast== Master Vijay Raghavendra
* H. G. Dattatreya
* Avinash
* Krishne Gowda
* Ashok Badaradinni
* Jayaram
* K. S. Sridhar
* Master Pavan Kumar

==Soundtrack==
{{Infobox album
| Name        = Chinnari Mutha
| Type        = Soundtrack
| Artist      = C. Aswath
| Cover       = Chinnari Mutha album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 25 December 2000
| Recorded    =  Feature film soundtrack
| Length      = 48:53
| Label       = Akash Audio
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

C. Aswath composed the background score the film and the soundtracks, with lyrics for all the soundtracks penned by H. S. Venkateshamurthy. The album has eleven soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 48:53
| lyrics_credits = yes
| title1 = Mannalli Biddano
| lyrics1 = H. S. Venkateshamurthy
| extra1 = Ajith, Baby Rekha
| length1 = 5:31
| title2 = Rekke Iddare Saake
| lyrics2 = H. S. Venkateshamurthy
| extra2 = Baby Rekha
| length2 = 5:12
| title3 = Naavu Irruvaaga
| lyrics3 = H. S. Venkateshamurthy
| extra3 = Ajith, Baby Rekha
| length3 = 4:08
| title4 = Maarisha Maarisha
| lyrics4 = H. S. Venkateshamurthy Narasimha Nayak, Manjula Gururaj
| length4 = 5:30
| title5 = Ondu Eradu Mooru
| lyrics5 = H. S. Venkateshamurthy
| extra5 = Narasimha Nayak
| length5 = 3:55
| title6 = Oduva Siri Oduva
| lyrics6 = H. S. Venkateshamurthy
| extra6 = Narasimha Nayak, Baby Rekha, Sangeetha
| length6 = 1:42
| title7 = Myale Kuvukonda
| lyrics7 = H. S. Venkateshamurthy
| extra7 = Manjula Gururaj
| length7 = 5:19
| title8 = Halli Mukka Muthu
| lyrics8 = H. S. Venkateshamurthy
| extra8 = Ajith and others
| length8 = 3:17
| title9 = Chandra Ninge
| lyrics9 = H. S. Venkateshamurthy
| extra9 = Ajith and others
| length9 = 4:37
| title10 = Estondu Jana
| lyrics10 = H. S. Venkateshamurthy
| extra10 = Baby Rekha and others
| length10 = 5:30
| title11 = Chinnari Mutha
| lyrics11 = H. S. Venkateshamurthy
| extra11 = Narasimha Nayak
| length11 = 4:12
}}

==References==
 

==External links==
*  
* http://www.shruthalaya.com/cm-body.htm

 

 
 
 


 