Backfire (1950 film)
{{Infobox film
| name           = Backfire
| image          = Backfire film poster 1950.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Vincent Sherman
| producer       = Anthony Veiller Ben Roberts
| story          = Larry Marcus
| starring       = Edmond OBrien Virginia Mayo Gordon MacRae Dane Clark Viveca Lindfors
| music          = Daniele Amfitheatrof
| cinematography = Carl E. Guthrie
| editing        = Thomas Reilly
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =   "Of Local Origin." New York Times. January 26, 1950. 
| runtime        = 91 minutes
| country        = United States
| language       = English}}

Backfire is a 1950 crime film in the film noir style directed by Vincent Sherman and starring Edmond OBrien, Virginia Mayo, Gordon MacRae, Viveca Lindfors, and Dane Clark.
 Ben Roberts, and Ivan Goff. It is notable for launching the film noir careers of its writers and one of its actors.

Although Backfire was completed in October 1948, it was not released until January 1950. However, screenwriters Ivan Goff and Ben Roberts would go on to write White Heat the year after working on Backfire. Edmond OBrien would also star in White Heat, as well as in the seminal film noir, D.O.A. (1950 film)|D.O.A., in 1950.

==Plot==
Bob Corey (Gordon MacRae) is an American soldier badly wounded at the end of World War II, and undergoing a number of surgical operations on his spine at a military hospital in California. He is tended by a nurse, Julie Benson (Virginia Mayo), and they have fallen in love. Coreys military pal, Steve Connolly (Edmond OBrien), arrives in early November to discuss plans for the ranch they plan to purchase and operate together once Corey is out of the hospital. The two men pool their G.I. benefits (totaling $40,000) to do so. Coreys final surgery is in mid-December, but Connolly does not appear at the hospital afterward to see his friend. By Christmas, Corey is still in recovery but Connolly still remains absent. One night, as Corey lies semi-conscious in bed after being administered a sleeping drug, a woman with an foreign accent (Viveca Lindfors) appears at his  bedside. She says Connolly has been in a horrible accident; his spine is shattered and he wants to die, but she has refused to help him commit suicide. The woman asks Corey what to do, and he advises her to do nothing to harm Steve, and just to wait. Corey slips into unconsciousness, and the woman disappears.

After New Years, Corey is released from the hospital. He is immediately stopped by police detectives and then questioned by Captain Garcia (Ed Begley) of the Los Angeles Police, who tells him that Connolly is wanted for the murder of Solly Blayne (Richard Rober), a local high-stakes gambler and Racket (crime)|racketeer. Corey denies that Connolly would be mixed up in anything criminal. How could he be, if he were injured? Corey and Nurse Benson decide to talk to Mrs. Blayne (Frances Robinson). The film engages in a visual Flashback (narrative)|flashback, which depicts an unseen assassin gunning down Solly Blayne in his home one night. Window shades prevent Mrs. Blayne or the audience from seeing the murderer, who was outside the home. Mrs. Blayne calls for a doctor, but he arrives too late.

Corey learns from Garcia which hotel Connolly was staying in, and he lodges in Connollys old room in an attempt to understand his friends thinking and feelings. Corey encounters Sybil (Ida Moore), a gossipy old hotel maid, who says that Mr. Blayne often visited Connolly at the hotel. She also gives Corey a business card from a local funeral home. Corey visits the funeral home and discovers that another military friend, Ben Arno (Dane Clark), owns the mortuary. In another flashback, Arno describes how he went to a night of boxing matches where he saw Connolly fighting in the ring. Connolly lost his match (even though Arno does not believe he should have). Arno asks Connolly why he is boxing at his age, but Connolly refuses to explain other than to say he needs money.

Corey returns to the hotel, where he is asked by the desk clerk to pay Connollys hotel bill. Realizing Connolly made some local phone calls, Corey dials the numbers listed in the hotel records. A young woman answers the phone. Corey pretends to be Connolly and the voice on the phone unintentionally reveals Connolly had a girlfriend named Lysa Radoff. Corey asks for and is given the address of Radoffs rented home. Corey goes to Radoffs home, finds no one home, breaks in, and discovers that Radoff is the same woman who visited him in the hospital. One of Radoffs roommates, Bonnie Willis (Sheila MacRae), comes home. Corey pretends to be waiting for Radoff to arrive, and the chatty Willis provides him with the story of how Connolly and Radoff met.

In yet another flashback, the audience learns that Connolly was working for a local gambler named Lou Walsh. Walshs girlfriend was Lysa Radoff. One night, Connolly went to a nightclub to pick up Radoff and bring her to a party Walsh was hosting. Willis went along with them to the party. The three went to a large apartment Walsh was using as a high-stakes gambling den. Walsh entertains his guests by having beautiful women act as call girls; Radoff is one of the call girls. (Due to the prevalent censorship of the time, this is all handled very gingerly.) Connolly, unlike the other men, never paws and manhandles the girls, and he and Radoff fall in love. Pointedly, the flashback never shows Lou Walsh. Connolly is depicted meeting Solly Blayne, who is there gambling. Blayne offers Connolly a job as a highly paid gofer. The flashback ends. To escape further questioning, Corey runs out of the house while Willis is in the kitchen. Moments later, she is gunned down by an unseen assailant who fires through the window.

The next night, Garcia interrogates Corey and Nurse Benson and accuses them of interfering in the investigation and causing Willis death. Garcia is alerted by telephone that a local Chinese man, Lee Quong (Leonard Strong), has been shot and is claiming he has information on Steve Connolly. Garcia, Corey, and Benson race to the hospital to interrogate Quong. In another flashback, Quong relates how he was the butler and cook at a magnificent nearby home which Walsh purchased as a gift for Radoff. Walsh installed Connolly in the house as her bodyguard. Unwittingly, he put the two lovers together, and their relationship intensified. In the flashback, Quong relates that he eavesdropped on Connolly and Radoff as they made plans to run away and get married. Connolly went to the garage and backed the car up the steeply inclined driveway. Unbeknownst to Connolly, Walsh came home early and overheard Connolly professing his love to Radoff. Walsh released the parking brake on the car, and it rolled down the driveway and injured Connolly — crushing several of the vertebrae in his back. The flashback ends.  Quong says he was shot by Lou Walsh after Walsh realized Quong had seen him commit murder. Quong dies before he can reveal the address of the home.

Garcia now has evidence that Connolly was physically incapable of committing murder. Garcia tells the press that the murder weapon used to kill Solly Blayne was also used to kill Bonnie Willis. Acting on a hunch, Nurse Benson contacts Mrs. Blayne and asks her the name of the doctor she called the night her husband was murdered. Mrs. Blayne says it was Dr. Herbert Anstead.  Benson (dressed in her nursing uniform) goes to Dr. Ansteads office later that night, pretending to be a nurse retrieving some files for the doctor. The janitor lets her in. She is unable to locate Connollys medical file. Anstead (Mack Williams) himself arrives a few minutes later, and Nurse Benson hides. Anstead retrieves Connollys file from its hiding place, and attempts to destroy it. Nurse Benson prevents him from doing so,   and tells him Connolly was not in an accident but was a victim of attempted murder. Anstead forces Benson into a locked room. Using information obtained from Benson, Anstead calls Bob Corey to tell him where Connolly can be located, and Benson overhears the address. Just then, Lou Walsh (not shown on screen) enters the office and guns down Anstead. Walsh flees, and Nurse Benson is released minutes later by the janitor.
 throw the fight to get out of debt. Arno told Connolly that he led a double-life as the high-stakes gambler "Lou Walsh", and proposed using Connollys $40,000 to cheat Blayne out of tens of thousands of dollars at gambling. Connolly agreed. In yet another flashback, Radoff realizes that the brakes on her car work just fine, and that Connollys injuries were no accident. She attempts to leave, but Walsh strangles her. The flashback ends. Arno tells Corey he did not want to martyr Connolly for fear of losing Radoffs love, so he staged the accident. But once Radoff knew the truth, he was forced to kill her. Arno admits he began killing anyone who could connect Lysa to him or who knew about Connollys accident. Corey (still weak from his back surgery) is knocked to the ground and Arno prepares to shoot him. As Arno is about to kill Corey, an injured Connolly (his body encased in braces and plaster) launches himself down the stairs and stops Arno. The police, summoned by Nurse Benson, arrive. Arno attempts to flee, but is killed.

After a jump cut, Connolly is shown leaving the military hospital many months later, his injuries repaired by military surgeons. Bob Corey and his new wife, Julie, arrive and take Steve to their ranch.

==Production==

===Cast===
* Gordon MacRae as Bob Corey
* Edmond OBrien as Steve Connolly
* Virginia Mayo as Julie Benson
* Viveca Lindfors as Lysa Radoff
* Dane Clark as Ben Arno
* Ed Begley as Captain Garcia
* Sheila MacRae (as Sheila Stephens) as Bonnie Willis
* Mack Williams as Dr. Herbert Anstead
* Leonard Strong as Lee Quong
* Frances Robinson as Mrs. Blayne
* Richard Rober as Solly Blayne

===Script development, casting, and principal photography=== John Patricks play, The Hasty Heart. Sherman asked studio head Jack Warner if he could turn the play into a film, but Warner refused and put him to work on adapting "Into the Night" into a motion picture. 
 motion picture of the same name in 1960), as well as an unpublished screenplay, The Shadow, based on a Ben Hecht story. Although Goff and Roberts considered themselves comedy writers, Warners hired them to work on the crime story "Into the Night." McGilligan, p. 15.  Sherman met with Goff and Roberts over the weekend, and they talked through the storys problems. Sherman concluded that the film was still unworkable, but Goff and Roberts said they needed the work and continued to craft a screenplay. Sherman voiced his reservations to Jack Warner, but Warner told him if he agreed to do the film then Warner would do him a favor in return.  Sherman agreed.
 suspension by the studio, she relented to continue to receive her pay ("I sold out," she later said). 
 Biltmore Hotel, Los Feliz Bel Air neighborhood. Additional scenes were shot in the nearby city of Glendale, California.  Sherman later said that he believed Goff and Roberts had turned in a good script, and that the actors had done the best job they could.   He found Mayo to be a very nice person and an extremely competent actress, but without a lot of personal depth. 

==Reception==

===Critical response===
Although the film was completed in October 1948, it was not released until 1950. The film opened at The Globe cinema in New York City, New York, on January 26, 1950.  White Heat, starring James Cagney, Edmund OBrian, and Virginia Mayo, had been released to widespread acclaim and strong box office while Backfire remained unreleased. To take advantage of White Heats popularity, movie posters for Backfire prominently featured Mayo in a femme fatale pose (very unlike her character in the film) and contained the tag-line: "That White Heat girl turns it on again!" The poster also gave away the surprise conclusion to the film by depicting Dane Clark strangling Viveca Lindfors.  
 New York expository problems in the film, did not work.  Author Clive Hirschhorn noted in 1980 that there were so many coincidences in the film that any feeling of suspense was eliminated and the realism so essential to film noir dissipated.  Critic John Howard Reid assessed the film as "borderline" in 2006, but felt cinematography was effectively atmospheric and the action sequences fair. Reid, p. 25.   He found that the supporting players (OBrien, Begley, Lindfors, Clark, and Sheila MacRae) delivered performances remarkably superior to that of the two stars, and singled out Lindfors for her acting. 
 David Shipman felt Gordon MacRae was "wasted" in the picture. 

Swedish actress Viveca Lindfors was under contract to Warner Bros. for four pictures.  Unhappy with her work, however, the studio declined to pick up her option after her performance in Backfire.  Warners was much more pleased with the efforts of Goff and Roberts, and gave them a five-year contract to write screenplays.  They produced White Heat the following year.  In return for directing Backfire, Jack Warner permitted Vincent Sherman to direct The Hasty Heart,  which became a major hit for the studio.

==Home media==
The film was regularly screened on broadcast television in the United States in the 1950s and 1960s, although most airings trimmed Mayos part substantially.  Warner Bros. released the film on DVD on July 13, 2010, in its Film Noir Classic Collection, Vol. 5. 

==References==
 

==Bibliography==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 