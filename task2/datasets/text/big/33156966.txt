The Humming Bird
 
{{infobox film
| name           = The Humming Bird
| image          = The_Humming_Bird_1924_film_poster.jpg
| imagesize      =
| caption        =
| director       = Sidney Olcott
| producer       = Adolph Zukor Jesse Lasky
| writer         = Forrest Halsey (scenario)
| based on       =  
| starring       = Gloria Swanson
| music          =
| cinematography = Harry Fischbeck
| editing        = Patricia Rooney
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 mins.
| country        = United States
| language       = Silent English intertitles
}} silent crime drama film directed by Sidney Olcott and starring Gloria Swanson. Produced by Famous Players-Lasky and distributed by Paramount Pictures, the film is based on the play of the same name by Maude Fulton who also starred in the Broadway production.  A print of the film is housed at the Library of Congress and the Nederlands Filmmuseum.       

==Cast==
*Gloria Swanson - Toinette
*Edmund Burns - Randall Carey
*William Ricciardi - Papa Jacques
*Cesare Gravina - Charlot
*Mario Majeroni - La Roche
*Adrienne DAmbricourt - The Owl
*Helen Lindroth - Henrietta Rutherford
*Rafael Bonqini - Bouchet
*Regina Quinn - Beatrice
*Aurelio Coccia - Bosque
*Jacques DAuray - Zi-Zi

==Production notes==
The film was shot at the Kaufman Astoria Studios in Queens. 

==References==
 

==External links==
*  
*  
* 
*    at website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 