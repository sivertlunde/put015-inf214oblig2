Octaman
{{Infobox film
| name           = Octaman
| image          =  
| director       = Harry Essex
| producer       = Michael Kraike Harry Essex
| starring       = Pier Angeli Kerwin Mathews Jeff Morrow Read Morgan
| cinematography = Robert Caramico
| runtime        = 76 min
| released       =  
| country        = United States
| language       = English 
| budget         = $250,000
}}	
 monster film Rick Baker did the effects for.

==Plot==
A scientific expedition to a remote Mexican fishing community, led by Dr. Rick Torres (Kerwin Mathews) and Susan Lowry (Pier Angeli), discovers unhealthy amounts of radiation in the local waters. They find a small mutant octopus that can crawl on land, and Torres travels back to the States to present his findings, hoping to be granted more funding. Reception from the scientific establishment is lukewarm, so Torres makes a deal with Johnny Caruso, a circus owner who is interested in the bizarre mutation as a carny act. After their departure, a humanoid octopus, Octaman, attacks the camp and slaughters the remaining crew.
 RV a Indian man from the nearby village, says that a local legend about a creature said to be half man and half sea serpent is true, and offers to take the scientists to the lake where it is purported to live. Meanwhile, Octaman kills some villagers. The next day, the scientists find another small mutant octopus, and Octaman has gone to the camp and killed a crew member and escaped. Johnny, witness to the attack, decides to capture the monster for his circus.
 tranquilizing it and trapping it under a net. In the morning, however, a thunderstorm brings rain which revives the Octaman and allows it to escape. It moves to seize Lowry, but she manages to communicate with it and send it away.

Davido tracks Octaman into a cave. The others consider abandoning the pursuit, but Davido goads them on. Octaman chases them into the back of the cave, gaining enough time to block the cave mouth and seal them in. However, Davido manages to find another way out. They return to the RV, but find Octaman waiting for them inside. In order to spare her colleagues, Lowry communicates that she accepts to be captured. Now determined to kill the beast, the expedition members shoot it at close range, forcing it to release Lowry, and continue firing until the monster retreats into the lake and dies.

==Cast==
* Pier Angeli as Susan Lowry
* Kerwin Mathews as Dr. Rick Torres
* Jeff Morrow as Dr. John Willard
* David Essex as the Indian
* Read Morgan as the Octaman  

==Reception==
The film met with negative reception. It has a rating of 3.4/10 on Internet Movie Database|IMDb. Despite this, the film has obtained a cult following over the years.

==Home media==
Octaman was released on the VHS format by the European Video Corporation. A 40th anniversary widescreen DVD edition was released in 2012 by BayView Entertainment. 

==In popular culture== Fright Night Comics in a similarly-themed story entitled "Eight Arms to Hold You."

==References==
 

 

 
 