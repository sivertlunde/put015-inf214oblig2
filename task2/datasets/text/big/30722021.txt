Rani Lalithangi
{{Infobox film
| name = Rani Lalithangi ராணி லலிதாங்கி
| image = Rani lalithangi.jpg
| director = T. R. Raghunath
| writer = Thanjai N. Ramaiah Doss
| screenplay = Thanjai N. Ramaiah Doss
| starring = Sivaji Ganesan P. Bhanumathi Rajasulochana P. S. Veerappa
| producer =  Tiruchi R. Kalyanaraman
| music = G. Ramanathan
| cinematography = P. L. Rai
| editing = R. Rajagopal
| studio = T. N. R. Productions
| distributor = T. N. R. Productions
| released = 21 September 1957
| runtime = 153 mins
| country = India Tamil
| budget = 
}}

Rani Lalithangi ( ) is a Tamil language Folklore film starring Sivaji Ganesan and Bhanumathi Ramakrishna|P. Bhanumathi in the lead roles. The film, directed by T. R. Raghunath, had musical score by G. Ramanathan and was released on 21 September 1957. 

==Production==
This story about kings, queens, princesses and scheming chieftains was filmed earlier in 1935 as Lalithangi by the Madurai-based Royal Talkie Distributors, with T. P. Rajalakshmi and V. A. Chellappa.
Tiruchi R. Kalyanaraman started a production company named T. N. R Productions. TNR stood for the initials of lyricist Thanjai N. Ramaiah Doss whom Thiruchi R. Kalyanaraman held in high esteem. Rani Lalithangi was the maiden movie of this new company.
M. G. Ramachandran was initially cast as the hero of the film, and after a few sequences were shot, including a song and dance sequence featuring him and Rajasulochana, he withdrew from the film for reasons unknown and Sivaji Ganesan stepped in to play the lead role.

==Plot==
Lalithangi (Bhanumathi) is a queen who rules over her kingdom, showing the world that a woman could rule as effectively as a man. Another king, a kind-hearted man, has a son (Sivaji Ganesan), a talented sculptor and lover of fine arts. A scheming and ambitious chieftain (Veerappa), who has his eye on the princes kingdom, plans to kill him in the course of a gypsy dance. He also deputes an attractive dancer (Rajasulochana) to ensnare him, but the prince escapes her wiles and begins to hate women. Lalithangi who wants to win over the hero disguises herself as a yogini and hands over a love letter to the prince. This enrages him and he throws her out. Determined to win his heart, she learns dancing. During an art festival at the palace, Lalithangi succeeds in dancing and changing the princes mind! How she persuades the prince to fall in love with her and wins him as her life partner form the rest of the plot.

==Cast==
*Sivaji Ganesan
*P. Bhanumathi as Lalithangi
*Rajasulochana as Maya
*P. S. Veerappa as King Gantheeban
*K. A. Thangavelu
*Serukulathur Sama
*K. Sarangkapani
*R. Balasubramaniam as Emperor Ampalavanan
*S. D. Subbulakshmi as Empress Angaiyarkanni
*T. P. Muthulakshmi
*M. Saroja

==Crew==
*Producer: Tiruchi R. Kalyanaraman
*Production Company: T. N. R Productions
*Director: T. R. Raghunath
*Music: G. Ramanathan
*Lyrics: Thanjai N. Ramaiah Doss
*Screenplay: Thanjai N. Ramaiah Doss
*Dialogue: Thanjai N. Ramaiah Doss
*Art: Ganga
*Editing: R. Rajagopal
*Choreography: Pasumarthi Krishnamoorthy, S. Ganesam Pillai & Thangaraj
*Cinematography: P. L. Rai
*Stunts: Stunt Somu
*Dance: E. V. Saroja

==Soundtrack== Playback singers are T. M. Soundararajan, C. S. Jayaraman, Seerkazhi Govindarajan, S. C. Krishnan, D. B. Ramachandra, V. T. Rajagopalan, P. Leela, Jikki, T. V. Rathinam & A. G. Rathnamala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers|| Lyrics || Length (m:ss) 
|- Thanjai N. Ramaiah Dass || 03:12
|-
| 2 || Inbam Perinbam || P. Bhanumathi || 05:39
|-
| 3 || Sri Saraswathi Dhevimatha || D. B. Ramachandra & P. Leela || 03:03
|-
| 4 || Ettadi Koyilile || T. M. Soundararajan || 02:47
|-
| 5 || Bulbul Jodi || S. C. Krishnan & T. V. Rathinam || 04:09
|-
| 6 || Madhunilai Maaraadha || P. Bhanumathi || 
|-
| 7 || Idhu Poruththamaana || Jikki || 03:04
|-
| 8 || Kaadhalukku Kannillai || C. S. Jayaraman || 03:28
|-
| 9 || Ennai Ariyaamal Thulludhadi || P. Bhanumathi || 03:33
|-
| 10 || Aadunga Paadunga Odureenga || P. Leela & A. G. Rathnamala || 02:27
|-
| 11 || Natvaangam || Seerkazhi Govindarajan || 03:31
|-
| 12 || Bajanaikku || A. G. Rathnamala & V. T. Rajagopalan || 03:33
|-
| 13 || Kal Endraalum Kanavanaa  || P. Leela || 03:01
|}

==Reviews==
In spite of the interesting storyline, impressive performances by Bhanumathi, Sivaji Ganesan and Veerappa, song and dance sequences by Rajasulochana and pleasing photography, Rani Lalithangi did not do well at the box office.

==References==
*  
 

==External links==
 
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 