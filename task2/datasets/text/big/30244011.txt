Man from Frisco
{{Infobox film
| name           = Man from Frisco
| image          =
| caption        =
| director       = Robert Florey
| producer       =
| writer         = Arnold Manoff (screenplay) George Worthing Yates (story)
| starring       = 
| music          =
| cinematography = Jack A. Marta
| editing        = Ernest J. Nims
| distributor    = Republic Pictures
| released       = June 15, 1944
| runtime        = 91 min.
| country        = United States
| language       = English 
| budget         =
}}
 spy and Michael OShea Anne Shirley.

==Storyline==
 West Coast, but he is hindered by the former superintendent of the shipyard, Joel Kennedy.

A disappointed lover fails to deliver an important message on welds and it leads to the collapse of a new ships superstructure and the death of a boy. 

The subject of the film shows some degree of wartime propaganda.  The lead character is said to be based on the real-life Henry J. Kaiser, and the film is set in the Kaiser Shipyards.  Like the later Betrayal from the East (1945), Man from Frisco included actual radio reports of the negotiations with the Japanese before their attack on Pearl Harbor of December 7, 1941.

==Cast== Michael OShea – Matt Braddock   Anne Shirley – Diana Kennedy  
*Gene Lockhart – Joel Kennedy  
*Dan Duryea – Jim Benson  
*Stephanie Bachelor – Ruth Warnecke   Ray Walker – Johnny Rogers  
*Tommy Bond – Russ Kennedy  
*Robert Warwick – Bruce McRae  
*Olin Howland – Eben Whelock (as Olin Howlin)  
*Ann Shoemaker – Martha Kennedy   Russell Simpson – Dr Hershey  
*Stanley Andrews – Chief Campbell  
*Forbes Murray – Maritime Commissioner  
*Erville Alderson – Judge McLain

==Notes==
 

==External links==
*  at imdb.com
 
 
 
 
 
 
 
 
 
 

 