Thenthulli
{{Infobox film 
| name           = Thenthulli
| image          =
| caption        =
| director       = KP Kumaran
| producer       = PV Shajihan
| writer         = PV Muhammad
| screenplay     = PV Muhammad
| starring       = Srividya Sathaar Sukumaran Balan K Nair
| music          = K. Raghavan
| cinematography = Kannan Narayanan
| editing        = NP Suresh
| studio         = Shahija Films
| distributor    = Shahija Films
| released       =  
| country        = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film,  directed by KP Kumaran and produced by PV Shajihan. The film stars Srividya, Sathaar, Sukumaran and Balan K Nair in lead roles. The film had musical score by K. Raghavan.   

==Cast==
*Srividya
*Sathaar
*Sukumaran
*Balan K Nair
*Kuttyedathi Vilasini Priya
*Ravi Menon
*Santha Devi

==Soundtrack==
The music was composed by K. Raghavan and lyrics was written by PT Abdurahiman and Kasi Mohammed. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaalathe jayikkuvan || VT Murali || PT Abdurahiman || 
|-
| 2 || Moham ithalitta poovu || P Susheela || PT Abdurahiman || 
|-
| 3 || Moytheenmaala || Ummarukutty || Kasi Mohammed || 
|-
| 4 || Othupalliyil || VT Murali || PT Abdurahiman || 
|}

==References==
 

==External links==

 
 
 

 