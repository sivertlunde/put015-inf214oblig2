Afrita hanem
{{Infobox film
| name           = Afrita hanem
| image          = Samiaetfarid.jpg
| image_size     =
| caption        = 
| director       = Henry Barakat
| producer       = Farid Al Atrache
| writer         = Abo El Seoud El Ebiary
| narrator       = 
| starring       = Samia Gamal Farid Al Atrache Lola Sedki
| music          = Farid Al Atrache
| cinematography = Ahmed Adley
| editing        = Emile Bahri
| distributor    = 
| released       = 1949
| runtime        = 97 minutes
| country        = Egypt
| language       = Arabic
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Afrita hanem ( , English: Little Miss Devil) is a 1949 Egyptian film about Asfour, a poor singer, played by Syrian musician Farid Al Atrache, who falls in love with Aleya, the somewhat spoiled daughter of his boss.  

Asfour wants to marry Aleya, but her father wont let the marriage happen due to Asfours class status. Asfour turns to a genie for help, but the genie, a female genie named Kahramana, played by noted Egyptian actress and dancer Samia Gamal, falls in love with Asfour instead, and tries to manipulate his desires. 

According to the British Film Institute’s book 100 Film Musicals, Afrita hanem critiques modernity:  “Running through all these films (as through so many Indian films), exploring moral dilemmas in bourgeois family settings, is a discourse in which western modernity – cars, clothes, manners – is viewed negatively in relation to traditional values. The sage who presides over the genie in Afrita Hanem pops up from time to time to deliver homilies about materialistic greed and selfishness.” 

==References==
 

== External links ==
*  

 
 
 
 

 