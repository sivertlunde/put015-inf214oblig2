Last Dance (1996 film)
{{Infobox film
| name = Last Dance
| image = Last dancemp.jpg
| caption = Movie poster
| director = Bruce Beresford
| producer = Steven Haft
| writer = Steven Haft Ron Koslow
| starring = Sharon Stone Rob Morrow Randy Quaid Peter Gallagher
| music = Mark Isham Peter James John Bloom
| studio  = Touchstone Pictures Buena Vista Pictures
| released =  
| runtime = 103 minutes
| country = United States
| language = English
| budget =
| gross = $5,939,449 (USA sub-total)
}} 1996 film starring Sharon Stone, Rob Morrow, Randy Quaid and Peter Gallagher.

==Background==
"Last Dance" was filmed during the period of March 9, 1995 and May 19, 1995.  It was filmed in Nashville. 
 Dead Man Walking, which was an Academy Award-winning drama whose treatment of the death penalty theme was still fresh in the minds of audiences.    Sharon Stone was nominated for a Razzie Award in 1997 for "Worst New Star" based on her role in the film (as the new "serious" Sharon Stone). 

==Plot==
Cindy Liggett (Sharon Stone) is waiting on death row for a brutal double murder she committed in her teens, 12 years earlier. Clemency lawyer Rick Hayes (Rob Morrow) tries to save her, based on the argument that she was under the influence of crack cocaine when she committed the crime of which she was found guilty and that she is no longer the same person she had been at the time of the murder.

==Cast==
* Sharon Stone as Cindy Liggett
* Rob Morrow as Rick Hayes
* Randy Quaid as Sam Burns
* Peter Gallagher as John Hayes Jack Thompson as The Governor
* Jayne Brook as Jill
* Pamala Tyson as Linda, Legal Aid Attorney
* Skeet Ulrich as Billy, Cindys Brother Don Harvey as Doug
* Diane Sellers as Inmate Reggie
* Patricia French as Guard Frances Ralph Wilcox as Warden Rice
* Buck Ford as D.A. Rusk
* Dave Hager as Detective Vollo
* Christine Cattell as Louise
* Peg Allen as Helen
* Meg Tilly as stripper

==Critical reception== Barb Wire.

Judd Blaise of Allmovie gave the film two out of five stars. 

Roger Ebert of the Chicago Sun-Times gave the film two and a half stars out of four. He stated "This is potentially powerful material, and the movie handles it thoughtfully. It makes a good showcase for Stone. She does a good job of disappearing into the role. But the movie suffers from one inescapable misfortune: It arrives while "Dead Man Walking" is still fresh in our memory. That film was an unquestioned masterpiece, containing some of the best writing, acting and directing of recent years. "Last Dance" cant stand up against it. Too many of its scenes are based on conventional ideas of story construction. We can see the bones beneath the skin. The movie has a few scenes that really should have been rewritten before filming. Among the very best scenes in "Last Dance" are those leading up to the possible execution. Rick buys Cindy a dress she can wear into the death chamber, and as she unpacks it, the moment becomes very moving. Stone can be proud of her work in this movie, but the material is simply not as good as it needs to be, after "Dead Man Walking." That film reinvented the Death Row genre, saw the characters and the situation afresh, asked hard questions, and found truth in its dialogue. "Last Dance," by comparison, comes across as earnest but unoriginal. It might have seemed better if it hadnt been released in the shadow of "Dead Man Walking," but well never know." 

James Berardinelli of ReelViews gave the film two and a half stars out of four, whilst stating "Arriving at theaters in the wake of the far superior Dead Man Walking, Last Dance starts out with a serious handicap. This less compelling tale of living on death row has neither the depth nor feeling of Tim Robbins film, and, while Sharon Stone acquits herself admirably as the prisoner, Rob Morrow leaves something to be desired as the man on the outside who develops feelings for her. Hollywood rarely does this sort of hard-hitting story as well as independent productions, and this is a case-in-point. While Last Dance isnt strictly a gender-switched re-telling of Dead Man Walking, there are a number of obvious similarities. Director Bruce Beresford injects a little too much manipulation and melodrama into Last Dance. In actuality, Last Dance is more Ricks story than Cindys. Her character doesnt have much of an arc; his does. Stone is not a revelation, but, she gives the most impressive performance of a rather lackluster career. Throughout most of the film, there is a believable, haunted look in Stones eyes, and the questionably-structured conclusion has power largely because of the way she reacts to events. Ultimately, one of the biggest problems with Last Dance is that it doesnt take risks or break new ground. Its a little too safe, and, at times, that results in mediocre drama. Dead Man Walking showed the power this kind of film can have when handled well; this picture illustrates the effects of softening it for mainstream appeal. There are moments when Last Dance generates a legitimate emotional impact, but the road to the closing credits is littered with too many unnecessary distractions. So, while this movie is perfectly watchable, and even worth a marginal recommendation, in comparison to Dead Man Walking, it feels diluted." 

The New York Times writer Janet Maslin reviewed the film on May 3, 1996, stating "It isnt easy to make grand entrances into and exits from a tiny jail house visiting room, but Sharon Stone finds a way. In Bruce Beresfords "Last Dance," she brings impossible sparkle to what is (literally) a dead-end role and proves yet again that in an otherwise floundering film, she can be a lifesaver.  Fully transformed from Hollywood cupcake to tough cookie, Ms. Stone has more than proven how persuasively she can act, especially in such bitterly hard-edged roles. But the Death Row drama "Last Dance" proves a futile exercise in more ways than one. Here, as in the recent "Diabolique," Ms. Stones stellar presence and surprisingly intense performance are all that stand between the audience and the void. Its rare to find films so dependent on a star turn, or at least to find the star so markedly out of sync with her material. In "Last Dance," the viewer must wait patiently through rote exposition scenes until she first appears; when they finally come, her initial moments are raw and transfixing. But soon the story begins taking sentimental turns, and even Ms. Stones startling ferocity gets buried in sludge." 

Rolling Stone magazine writer Peter Travers reviewed the film on May 3, 1996, and he stated "Picture Sharon Stone with stringy red hair, stripped of lip gloss and flattering camera angles, in the role of a condemned murderess about to face the grim music of lethal injection. What the hell were they thinking? Perhaps another Oscar nomination to bookend the one Stone collected for Casino. Not bloody likely. Last Dance is a prison melodrama that embraces all the cliches that Dead Man Walking artfully dodged. Last Dance acts tough, but its heart is pure soap opera. Last Dance is fiction, freeing writer Ron Koslow to work over the audience with cheap tears and exploitative suspense. Without actressy emoting, Stone convincingly establishes Cindy as "wild" — pronounced with a seductively elongated l, as in lusty and lethal. Last Dance allows few opportunities for levity, but Stone grabs whats available. No wonder Rick falls in love with Cindy — so does the movie. Stone is known for bringing a feminist twist to her characters, often in defiance of her directors intentions. Think Basic Instinct. In Last Dance, she shows with eloquent precision the strength Cindy forges in the antiseptic isolation of prison. Its too bad that Stone didnt resist Beresfords insistence that she play all the death-row platitudes — from silent resignation to screaming breakdown. As for Stone, she should stay far, far away from those slick Ricks among filmmakers who promise the truth and deliver the mock." 

The Washington Post writer Desson Howe reviewed the film on May 03, 1996 and stated "Sharon Stones transition from vamping to acting continues its slow, painful progress in "Last Dance." This movie isnt quite "Dumb Blonde Walking" (for one thing, shes a brunette in this picture), but that satirical slur isnt so far off the mark. The Touchstone Pictures release, which has innumerable similarities to Tim Robbinss superior death row drama, "Dead Man Walking," is about as formulaic a picture as screenwriter Ron Koslow can hack. Although, one feels more disposed to saving Stone than having her killed, the emotional effect of "Last Dance" is strangely distancing. Perhaps thats because theres no sense of a vulnerable life on the line - just an acting career." 

Barbara Shulgasser of the San Francisco Chronicle reviewed Last Dance on May 03, 1996, stating "Owing to bad timing, its inevitable that "Last Dance" will forever be known to wags as "Dead Woman Walking". Its a neat, prettily tied-up package about that stuff we see every night on TV - corruption in high places, an inhumane system and selfishness. In other words, its simplistic, puerile rubbish. Im happy for Sharon Stone that she can summon tears in the name of thespian greatness, but, really its time for her to start picking difficult material if she really wants to become an actress." 

Edward Guthmann of San Francisco Chronicle also reviewed the film for the same issue, stating "Go ahead, call it "Dead Babe Walking." What with our star awaiting execution in a Death Row pod, and her attorney (Rob Morrow) struggling against the clock to win her clemency, comparisons are inevitable to Tim Robbins similar but far superior "Dead Man Walking." Stone is no Sean Penn, and "Last Dance" - despite able direction from Bruce Beresford and acting support from Morrow, Randy Quaid and Peter Gallagher - is an earnest, unremarkable addition to the Hollywood canon of prison movies. "Last Dance" should have been Stones opportunity to prove her acting credentials - to show that "Casino" was more than a one-time fluke orchestrated by Martin Scorsese. You can see how hard shes tried to cut to the bone of her character - to obliterate her image and locate the core of Cindy Leggetts rage and despair. She might have had that chance, if "Last Dance," didnt spend so much time on Morrows efforts to save Cindys life. Working off a script by Ron Koslow, Beresford doesnt seem nearly as interested in Stones character and her white-trash background as he is in the frustrating, protracted mechanics of a clemency appeal. The moments when "Last Dance" doesnt gloss over Stones character are the best, and they make you wish the movie had been restructured. Unlike "Dead Man Walking," which examined the same themes to brilliant effect, "Last Dance" never reaches our hearts, and never tells the real story of a death sentences emotional toll. It doesnt even come close." 

UK newspaper The Telegraph writer Anne Billson reviewed the film on 24 August 1996. She stated "At least Sharon emerges from this enterprise with a certain amount of dignity. Poor Rob Morrow, who plays the callow Clemency Board attorney who becomes emotionally involved with his no-hoper case, is saddled with an atrocious floppy hair-do and a wilfully unsympathetic role; hes a poor little rich kid who has beaten a fraud rap and landed his current job only because his brother is the governors chief-of-staff. Otherwise, its the usual compendium of clichés: grainy flashbacks to establish Sharons incontrovertible guilt, sheaves of bad drawings to establish her change of personality, fellow inmates exchanging sisterly "yo, bitch" banter, a governor with one eye on re-election, the crucial judge away on a fishing holiday, pro- and anti-capital punishment campaigners keeping vigil outside the penitentiary." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 