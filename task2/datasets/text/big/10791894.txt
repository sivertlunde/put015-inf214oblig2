Mullum Malarum
 
 
{{Infobox film
| name           = Mullum Malarum
| image          = Mullum Malarum Poster.jpg
| alt            = Poster dominantly showing Kali (Rajinikanth) wearing a brown shawl to conceal his lost arm.
| caption        = Theatrical release poster
| director       = J. Mahendran
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| producer       = Venu Chettiar
| music          = Ilaiyaraaja
| cinematography = Balu Mahendra
| editing        = D. Vasu
| released       = 15 August 1978
| runtime        = 135 minutes 
| country        = India Tamil
| studio         = Ananthi Films
}}
 Tamil drama film directed by J. Mahendran and produced by Venu Chettiar. The film, which features Rajinikanth, Sarath Babu, Fatafat Jayalaxmi and Shoba, marked Mahendrans directorial debut and was adapted from writer Umachandrans award-winning novel of the same name, albeit with several changes. It tells the story of Kali, a winch operator who conflicts with his superior Kumaran at a power plant, which eventually leads to the loss of his left arm and job.
 Glenmorgan and Kundha River|Kundha. After Mullum Malarum was complete, Chettiar was upset that the film had less dialogue than visuals, which he did not expect from Mahendran. The soundtrack was composed by Ilaiyaraaja.
 special prize Malayalam remake Hindi remake titled Pyari Behna in 1985.

==Plot==
Kali (Rajinikanth) is a winch operator at a village power plant. Although a reputed troublemaker, he occasionally performs good deeds for the local community. Kali and his younger sister Valli (Shoba) upon whom he dotes were orphaned during their childhood, and have no close family. They take in a poor itinerant named Manga (Fatafat Jayalaxmi) and her aged mother, and accommodate both in a neighbouring hut. Manga becomes fond of Kali, but he finds her gluttony disgusting.

The power plants newly appointed divisional engineer is Kumaran (Sarath Babu), an austere but fair boss. Kali and Kumaran have a difficult relationship, which worsens after Kumaran sees only Kalis negative side during a series of incidents. Kali nicknames Kumaran "Law Point" as he applies rules strictly. On a usual day, while Kali frolics in the river with Manga when he is supposed to be working, there is an emergency at the power plant while he is absent without leave. Kumaran suspends Kali from his job for dereliction of duty, ignoring his protests and threats.

Kali becomes inebriated and collapses in the middle of the road. A lorry then runs over his left arm, which is later amputated. Since he cannot perform his job one-armed, the power plant dismisses him from his job. Kali is now unemployed; he directs his anger and frustration at Kumaran. Manga feels guilty because she is responsible for Kalis predicament. On Vallis request, she marries Kali and takes care of all his needs.

Kumaran has developed a liking for Valli, and asks Kali for permission to marry her. Kali refuses the proposal due to his hatred for Kumaran, and instead makes arrangements to get Valli married to Murugesa (Venniradai Moorthy) a local, philandering grocer. Manga challenges Kalis decision, and the entire village rallies around Valli. Manga decides to get Valli married to Kumaran without Kalis approval. On the day of the wedding, the marriage procession passes Kali on its way to the temple. Valli however changes her decision, leaves the wedding party and returns to her brother. Kali then tells Kumaran that although he still dislikes him, he and Valli have his permission to marry.

==Cast==
*Rajinikanth as Kali, a winch operator
*Sarath Babu as Kumaran/Law Point, the newly appointed divisional engineer
*Fatafat Jayalaxmi as Manga, Kalis wife
*Shoba as Valli, Kalis younger sister and Kumarans love interest
*Venniradai Moorthy as Murugesa, a grocer and philanderer

==Production==
===Development===
 
Mullum Malarum is based on the novel of the same name, written by Umachandran and published in the Tamil magazine Kalki (magazine)|Kalki. The story, about the intimate feelings in the relationship of a brother and sister,  won the first prize in Kalki s Novel Short Story Competition held to mark the silver jubilee of Kalki in 1966.  J. Mahendran, who was already a successful screenplay and dialogue writer, briefed Mullum Malarum to the producer Venu Chettiar and he was impressed.  By adapting Umachandrans novel into a feature film, Mahendran thus made his debut as a director. He also wrote the dialogue for the film.  Chettiar produced Mullum Malarum under his banner Ananthi Films.  Ramasamy was recruited as the films art director, and D. Vasu as the editor.  Mahendran initially wanted Ramachandra Babu to be the cinematographer, but actor Kamal Haasans intervention meant that Balu Mahendra was signed up, making his debut in Tamil cinema.  

Mahendran only read a part of Umachandrans novel, but was particularly receptive to the winch operator Kali, his affection towards his sister and the way he loses his arm. From then onwards, the screenplay was developed by him, deviating from the novels plot. He decided to make a minimalist film without melodrama, overacting, excessive dialogue, or duets,  writing a screenplay according to his visualisation of it.  Among the differences between the film and the novel, Kali in the novel loses his arm to a tiger,  but in the film he loses it due to a lorry. 

===Casting===
Mahendran informed producer Venu Chettiar that he desired Rajinikanth to play the lead role, but the latter deprecated due to Rajinikanths dark complexion and because he was primarily experienced at portraying villains at that point.    Mahendran, however, refused to direct the film if Rajinikanth was not cast in the lead role.  A vehement dispute ensued between Chettiar and Mahendran, and the former eventually capitulated.  Chettiar was not convinced with the fact that a "villain" plays the main role as he felt it was "ridiculous" and "preposterous"; he voiced this opinion whenever he came to the location.  Rajinikanth was perturbed with the lack of faith in his acting and vowed to "put his heart and soul into the character Kali".  He was paid  13,000 (about   in 1978) ) per 1 US dollar (US$). }} for acting in the ﬁlm. 
 Latha claims that she was originally considered for the role, but she could not take it up due to her tight schedule. 

===Filming=== Orwo colour.  Balu Mahendra likened typical Indian hero-heroine dancing to "watching two drunken monkeys dancing", stating that this was the reason he "kept music as the background while the screen had lead characters expressing their emotions".  The film was intentionally made to defy the customs of traditional Tamil cinema, discarding all of the elements that Mahendran loathed.  Because Mahendran had no prior experience in film directing, Balu Mahendra assumed principal responsibility, involving himself in all aspects of screenplay, dialogue, camera angles, casting and editing, according to Mahendrans wishes.   
 Kundha and montage technique and Mahendran agreed, much to Sarath Babus disappointment.  After Mullum Malarum was complete, Chettiar was disorientated that the film had very little dialogue;  he had recruited Mahendran to direct the film because he was a successful screenplay and dialogue writer. The producer did not expect a film with limited dialogue and more visuals from him. 

==Themes==
{{multiple image
 
| align     = right
| direction = horizontal
| footer    = The characters of Kali and Valli represent thorn and flower respectively, hence the title of the film 
| width     = 
 
| image1    = Acacia thorns.jpg
| width1    = 175
| alt1      = 
| caption1  =

 
| image2    = Primula aka.jpg
| width2    = 187
| alt2      = 
| caption2  =
}} Fire and Ice.  Much like in Bairavi (1978), Rajinikanth and his sister have abusive parents in childhood and the brother is held responsible to safeguard his sister. Unlike Bairavi, the siblings in Mullum Malarum are not separated and this leads to Kalis protective attitude towards his sister Valli, bordering on obsessive love. In one scene, after lashing out at her in anger during the day, he applies henna to her feet at night when she is asleep. 
 plays god when he gives the villagers a ride on the power plants trolley which he operates, saving them form the exertion of walking. Thus, Kali is shocked when the divisional engineer Kumaran, a presentable and educated male, arrives. Being a subordinate, Kali cannot oppose Kumaran and his frustration threatens to disclose itself several times; it finally does when he is suspended. Kalis feelings manifest themselves in the song "Raman Aandaalum" with the lyrics, "It doesnt matter whether Rama or Ravana are reigning, I am the king of my own conscience", a clear reference to Kumarans authoritarian yoke to which Kali is bound. 

When Kalis arm is amputated, he feels helpless and emasculated and the engineer Kalis   , becomes an easy target. Kali refuses to see the benefits of his sister marrying into wealth and education, even when his wife Manga explains it to him, or when his sister expresses her desire for the union. Kalis words when Valli abandons her wedding proceedings to be with her brother are revealing: "My sister has shown all of you that I am the most important person in her life. I need only that happiness for the rest of my life. And it is with that pride and arrogance that I give my permission for my sister to marry." 

==Music==
 
 background score were composed by Ilaiyaraaja.  The lyrics for the songs were written by Panchu Arunachalam, Gangai Amaran and Kannadasan. The soundtrack album, which was released by EMI Records, includes only four of the films five songs; the song "Maan Iname" was omitted.       The film has no duet songs—a notable rarity for Tamil cinema.    The soundtrack and score were positively received.    

==Release and reception==
{{quote box
| quote = "I would not have made the film if Rajinikanth had not been in it. I had a producer who never turned up on the sets; but I had Rajinikanth, Ilaiyaraaja for the background score and Balu Mahendra for the camera. Naturally, the film was a success."
| source =&nbsp;– J. Mahendran, in August 2013 
| align = right
| width = 30%
}}
Mullum Malarums release encountered problems, but Haasan intervened  and it was released on 15 August 1978 Independence Day (India)|Indias Independence Day.     The films commercial performance in the first two weeks of its release was poor. Chettiar relinquished hope of the films success and refused to give anymore publicity for the film, saying "A good product needs no publicity, whereas a bad product cannot be pushed in the market however much you publicise it" and thought he was "doomed". In the third week of release,  crowds visited theatres in large numbers after the film received positive reviews in magazines and word of mouth appreciation spread, making it a huge commercial success and the film ran for 100 days in theatres.   while Balu Mahendra stated in his blog that it was the third week.  Gayathri Sreekanth states in The Name is Rajinikanth that the film "picked up by the end of the third week",  and J. Mahendran told the Tamil magazine Iniya Udhayam in 2013, that "in 3 weeks after the films release, it became a blockbuster hit as the tickets started selling in the black market". }}
 letter of Malayalam by Hindi by Bapu as Pyari Behna (1985).  A Telugu language|Telugu-dubbed version, titled Mullu Puvvu, was released on 26 October 1979.  The film was remade in Telugu as Seethamma Pelli (1984). 

===Contemporaneous reviews=== Kurinchi flowers in Tamil Cinema."  After watching the film, M. G. Ramachandran the then Chief Minister of Tamil Nadu told Mahendran:

 }}

===Reflective reviews===
 the content and the narrative. The way a scene starts, the way it finishes most of it gets played out there. And then, its just a question of being able to translate it well onto film."|source=—Director Mani Ratnam on Mullum Malarum in an interview with film critic Baradwaj Rangan. }}

Director Dhanapal Padmanabhan informed K. Jeshi of The Hindu that Mullum Malarum "scores on content, craft and extraordinary screenplay".  Rajinikanth in particular has been highly praised by critics, and his role as Kali is considered by   to be a highly challenging, complex role, which Rajinikanth effectively pulled off.  In October 2010, Amrith Lal of The Times of India stated that Mullum Malarum "revealed the potential of Rajini, the character actor."  In December 2012, playback singer Suchitra said, "Rajinis role as Kali in Mullum Malarum is my favourite for the following reasons one, it is the most honest on-screen depiction of the brother-sister relationship; and two, though it was only his third film, he was brilliant as the rough-hewn, obstinate winch operator and, in the movies latter half, as a frustrated individual rendered immobile due to an accident, yet trying to retain his dignity", and called Mullum Malarum her "favourite Rajini movie". 

G. Dhananjayan, the author of The Best of Tamil Cinema, 1931 to 2010 said, "If Paasamalar (1961) stood out for brother-sister relationship in a melodramic format, this film stands out for its realistic format for such a fine relationship". 
Maalai Malar stated that Rajinikanth lived through the role of Kali, praised Shobas acting and said Balu Mahendras cinematography is on par with international levels. It also said Mullum Malarum is one of those few films which cannot be destroyed by time.  Entertainment website IndiaGlitz praised Mahendran for having "used the novel to the best effects" and called the film as one of his most "precious contributions".  In 2002,  S. R. Ashok Kumar of The Hindu cited Shobas performance as the heros sister "a brilliant performance",  and in 2007, Settu Shankar of Oneindia.in called the film "a perfect blend of literature with mass entertainment". 

In December 2012, film journalist Sreedhar Pillai stated that Mullum Malarum was Rajinikanths best performance and the film was among his most "memorable movies".    In December 2009, D. Karthikeyan of The Hindu declared that Mullum Malarum would "remain etched in every film lovers memory by showing the best of Rajnikanths acting skills".   

===Accolades=== special prize Indian International Film Festival in 1979. 
 Sujatha included the film in his list of "ten best Indian films", and stated, "Mahendrans triumph was making superstar Rajnikanth act naturally".  In December 2013, The Times of India ranked the film fifth in its list of "Top 12 Rajinikanth movies" and said, "with this film, the talented actor dispelled whatever doubts remained about his acting ability".  Behindwoods listed Rajinikanths performance as one of his "Top 12 acting performances".  In July 2007, S. R. Ashok Kumar asked eight Tamil directors to list ten of their favourite films. Four of them K. Balachander, K. Bhagyaraj, Mani Ratnam and K. S. Ravikumar named Mullum Malarum as one of their ten top Tamil films. 

==Legacy== Murattu Kaalai (1980), Kai Kodukkum Kai (1984), Kaali (1980 film)|Kaali (1980) and Athisaya Piravi (1990) were named Kali. 
 Shankar revealed to The Hindu that he entered films "with dreams of directing films such as Mullum Malarum." But when nobody wanted to produce his script Azhagiya Kuyilae, he directed the big-budget Gentleman (1993 film)|Gentleman (1993), the success of which caught him in an image trap and he never got to make the small budget film.  On 19 August 2013, a day before Raksha Bandhan, film historian/actor Mohan Raman Twitter|tweeted, "Happy Pasa Malar /Mullum Malarum Day honour that brother-sister relationship", referencing both the films which received praise for their depictions of a brother-sister relationship. 

==Notes==
 

==References==
 

==Bibliography==
 
*  
*  Thorns Also Blossom| url=http://books.google.com/books?id=t6CGLgEACAAJ|year=2011|publisher=Galatta Media|isbn=978-81-921043-0-0|ref=harv}}
* 
*  
*  
*  
 

==External links==
*  
*   at Upperstall.com

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 