La Comédie humaine (film)
 
 
 
{{Infobox film
| name           = La Comédie humaine
| image          = La Comédie humaine poster.jpg
| border         = yes
| caption        = 
| film name = {{Film name| traditional    = 人間喜劇
| simplified     = 人间喜剧
| pinyin         = Ren Jian Xi Ju
| jyutping       = Jan4 Gaan1 Hei2 Kek6}}
| director       = Chan Hing-ka   Janet Chun
| producer       = 
| writer         = Chan Hing-ka   Janet Chun
| starring       = Chapman To   Wong Cho-lam
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Hong Kong
| language       =  
| budget         = 
| gross          = 
}} 2010 Cinema Hong Kong comedy film directed and written by Chan Hing-ka and Janet Chun. Released in cinemas on 8 July 2010, it premiered at the 31 March 2010 Hong Kong International Film Festival.

==Plot==
The story revolves around a Spring (To), a hitman from mainland China who is on a mission in Hong Kong with his partner Setting Sun (Hui). However, Spring falls ill and comes under the care of a screenwriter by the name of Soya (Wong) and they find themselves developing into a tight and everlasting friendship.

==Cast==
*Chapman To as Spring
*Wong Cho-lam as Soya
*Fiona Sit as Tin-Oi
*Kama Law as Maggie Chan
*Benz Hui as Setting Sun
*Chiu Tien-you
*Lee Lik-chi

==Critical reception==
The film has been generally received neutrally. Chan Hing-Kas vision was criticised for having been "poorly paced." 

==References==
 

==External links==
* 
* 
*  at the Hong Kong Cinemagic

 
 
 
 
 


 
 