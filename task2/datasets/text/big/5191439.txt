One Missed Call (2008 film)
{{Infobox film
|name=One Missed Call
|image=Onemissedcall posterbig.jpg
|caption=Theatrical release poster
|director=Eric Valette
|producer=Andrew Kosove Broderick Johnson Scott Kroopf Jennie Lew Tugend Lauren Weissman
|screenplay=Andrew Klavan based on= 
|starring=Shannyn Sossamon Edward Burns Ana Claudia Talancón Ray Wise Azura Skye
|music=Reinhold Heil Johnny Klimek
|cinematography=Glen MacPherson
|editing=Steve Mirkovich Intermedia Films Warner Bros. Pictures   Kadokawa Pictures  
|released= 
|runtime=87 minutes
|country=United States Japan Germany
|language=English
|budget=$20 million 
|gross=$45,847,751 
}} One Missed Warner Bros. Pictures. Despite being a moderate box office success, the film was panned by film critics often regarding it as the weakest Japanese horror|J-horror remake to be released.

==Plot== strange occurrences to the police; however, they think she is delirium|delirious.

Detective Jack Andrews believes her and together, they begin to unravel the mystery. They eventually trace the calls back to a woman named Marie Layton, who was apparently abusing her children, Ellie and Laurel. They learn that Ellie died of an asthma attack, and that Laurel is in a foster home after her mother went missing.

Believing that Marie is the source behind the murders, Beth travels to the recently burned St. Lukes hospital, where Marie was last seen. She finds Maries body there, apparently burned to death, clutching a cell phone. Maries corpse assaults Beth while weeping. It is later revealed that Maries spirit was actually protecting Beth, not trying to hurt her.

While visiting Laurel, Andrews goes to her room and finds her teddy bear with a video disc in its back. The disc is a video of a camera Marie hid to monitor Laurel and Ellie. Ellie had cut Laurels arm with a butcher knife. Marie had entered and realized that the abuse she has been blamed for has been Ellie all along. She then left to take Laurel to the hospital, locking Ellie in the bedroom. Ellie suffers an asthma attack and dies while dialing her mothers cell, making Marie the first real victim of the curse. Laurel tells Andrews that though Ellie hurt her, she always gave her the red hard candies, the ones found in the mouths of all the victims.

Andrews realizes that the force behind the murders is Ellie, and races to Beth. The two hear a knock on the door and as Andrews looks through the peephole, a knife stabs through it and kills him. Ellies spirit appears and reaches out to strangle Beth. The spirit of Marie appears and grabs Ellie saving Beth again. A red candy spills out of Andrews mouth and his cell begins to dial a number on its own, revealing that Ellies ghost is still out there, and more people will die.

==Cast==
 
* Shannyn Sossamon as Beth Raymond
** Alana Locke as Young Beth
* Edward Burns as Detective Jack Andrews
* Ana Claudia Talancón as Taylor Anthony
* Ray Wise as Ted Summers
* Azura Skye as Leann Cole
* Johnny Lewis as Brian Sousa
* Jason Beghe as Ray Purvis
* Margaret Cho as Mickey Lee
* Meagan Good as Shelley Baum
* Rhoda Griffis as Marie Layton
** Dawn Dininger as the ghost of Marie Layton
* Ariel Winter as Ellie Layton
** Sarah Jean Kubik as the ghost of Ellie Layton
* Raegan Lamb as Laurel Layton
* Karen Bayer as Mrs. Ford
* Dave Spector as Gary
* Mary Lynn Owen as Julie Cohn
* Roy McCrerey as Dr. Painter
* Greg Corbett as John 
* Bart Hansard as Howie
* Katie Kneeland as Maddie
* Jason Horgan as Dr. Brown
* Kaira Akita as Jewel
* Laura Harring as Mrs. Raymond
 

==Production== greenlit by Warner Bros. in early 2006, with Eric Valette signing as the films Film director|director.    The film began production in June 2006   in Atlanta|Atlanta, Georgia  with Edward Burns, Margaret Cho, and Shannyn Sossamon signing on.  On August 3, 2006, Ed Harris and Gabriel Byrne both signed on to appear in the film, however both withdrew due to unknown circumstances. 
 Skid Row front man Sebastian Bach in the hospital basement scene. The exact clip comes from Bachs scream at the beginning of "Midnight Tornado", a song from the bands 1989 debut album, Skid Row.

==Distribution==
===Release===
The film was intended for release on August 24, 2007, however was later pushed back to January 4, 2008.  On August 30, 2007, the films  " by   was used in television advertisements for the film.

===Marketing=== Apple iPhone.  

===Home media===
Both the DVD and Blu-ray Disc|Blu-ray were released on April 22, 2008 with no special features. The film was also released on iTunes and on the Xbox Live Marketplace.

==Reception==
===Box office===
One Missed Call opened in 2,240 theaters on January 4, 2008.  The film earned  5.2 million on its opening day  and $12,511,473 on its first weekend, ranking #5 at the North American box office.  The film grossed $26,890,041 in the United States and $18,957,710 overseas for a worldwide total of $45,847,751. 

===Critical reception===
The film was not screened for critics.  It was universally panned by critics, receiving a 0% "Rotten" approval rating and an average score of 2.5/10 based on 79 reviews on  .  Metacritic reported the film had an aggregate score of 24 out of 100, based on 14 reviews. 
 Final Destination The Ring Dark Water (2005), and Pulse (2006 film)|Pulse (2006).      

==Relationship with One Missed Call (2003)== original film. original characters Yumi, Yoko, Natsumi, Yamashita, and Kenji. Another reference is contained within the theatrical trailer; while Leann is walking down the street, a piece of the original ringtone from One Missed Call (2003) plays in the background until she falls from the overpass. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 