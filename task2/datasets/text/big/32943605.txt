The Wreck (1913 film)
 
{{Infobox film
  | name     = The Wreck
  | image    = 
  | caption  = 
  | director = W. J. Lincoln		
  | producer = 
  | writer   = W. J. Lincoln based on = poem by Adam Lindsay Gordon
  | starring = 
  | music    = 
  | cinematography = Maurice Bertel 
  | editing  = 
  | distributor = 
  | released = 1915
  | runtime  = 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

The Wreck is an Australian film directed by W. J. Lincoln based on a poem by Adam Lindsay Gordon about the ride to help by a farmhand who has witnessed a shipwreck. It is considered a lost film.

The movie was made in 1913 but not released until 1915. W. J. Lincoln later made a film of Gordons life, The Lifes Romance of Adam Lindsay Gordon (1916). 

==References==
 

==External links==
*  
*  at AustLit
*  at IMDB

 

 
 
 
 
 


 