Lonely Water
{{Infobox Film
  | name           = Lonely Water
  | image          = Lonelywater.jpg
  | image_size     = 
  | caption        = 
  | director       = Jeff Grant
  | producer       = 
  | writer         = 
  | starring       = 
  | narrator       = Donald Pleasence
  | music          = 
  | cinematography = 
  | editing        = 
  | distributor    = Central Office of Information
  | released       = 1973
  | runtime        = 1 minute 29 seconds
  | country        = United Kingdom English
  }}
 Public Information short film made for the Central Office of Information (COI).  The film aimed to warn children of the dangers of careless or foolhardy behaviour in the vicinity of water, and was shown regularly on TV for several years during breaks in childrens programming.  Lonely Water is widely recalled as one of the most memorable and chilling of PIFs.  In a poll carried out by the BBC on the 60th anniversary of the COI in 2006, Lonely Water was chosen as the UKs fourth-favourite PIF of all time and the highest ranked one-off production.   Dozens of comments attested to the films power and lasting impact on 1970s children. 

==Background== 
Lonely Water was produced by Illustra Films and directed by Jeff Grant, having been commissioned by the COI as a result of official concern over the high number of child fatalities in drowning accidents in the UK.  Other PIFs of the early 1970s to tackle the subject of water safety included an instalment of the Charley Says series and Learn to Swim featuring Rolf Harris, which were targeted at younger children and parents respectively.  Lonely Water however was aimed squarely at the 7&ndash;12 age group and was altogether darker in tone, featuring an eerie faceless black-robed and cowled figure and a memorably sinister voiceover by Donald Pleasence, whose echoing final words "Ill be back-back-back....!" are often said semi-jokingly to have terrified and traumatised an entire generation of British schoolchildren.  Writing for the British Film Institute, Katy McGahan says Lonely Water is "eerily redolent of Nicolas Roegs Dont Look Now" and "plays like a distilled horror film, deploying the menacing tone and special effects normally the preserve of X-rated cinema shockers."   

==Synopsis==
The camera pans across a mist-shrouded expanse of water clogged with fallen trees and branches, with a ghostly black-clad figure hovering hazily on the waters surface.  "I am the spirit of dark and lonely water: ready to trap the unwary, the showoff, the fool..." intones the voiceover, "...and this is the kind of place youd expect to find me.  But no-one expects to find me here", as the scene switches to a group of children playing on the muddy edge of a murky pool.  A boy is teetering on the steep, slippery bank attempting to retrieve a football from the pool with a stick.  "But that pool is deep. The boy is showing off...the bank is slippery," says the spirit, approaching from behind as the child slips into the water.

"The unwary ones are easier still," observes the spirit, as a boy is seen fishing in a duckpond, leaning out over the water as he holds onto a tree branch.  "The branch is weak, rotten; itll never take his weight."  The branch gives way and the boy tumbles into the pond as the spirit appears among the reeds.

A "Danger - No Swimming" sign appears on screen.  "Only a fool would ignore this, but theres one born every minute."  The lakeside is littered with rusting junk, and the bed of the lake is also strewn with hidden traps.  A boy has gone swimming and got into trouble, waving his arms and floundering.  "Its the perfect place for an accident."  The struggling boy is spotted by a group of children who find a big stick with which to help him out of the water.  "Sensible children!" says the spirit in exasperation as his robes collapse into a heap on the ground, "I have no power over them!"  The boy is rescued, shivering.  A girl is told to "go over and get that thing to wrap him in".  She picks up the robes, but throws them into the water in disgust.  (The girls line, which baffled many viewers, is "Urgh...orrible fing!" spoken in a strong London accent.)  As the robes sink under the water, the spirit voices his famous echoing threat: "Ill be back!"

==Production==

The script for Lonely Water was written by Christine Hermon for the COI and passed to Jeff Grant for direction. Grant recalled the moment he received the script, "I read it and got a bit of a shock. Gone was the gentle cajoling of Rolf Harris, who had recently appeared in a public information film aimed at persuading children to learn to swim. This one plumbed the darkness; it set out to scare."  It was posited that the COI were being "irresponsible" and "foolhardy" with such a frightening script, though Grant praised the creative latitude it gave him as director, "something you more often than not had to fight for". 

Lonely Water was filmed over two days a few miles north of London. Initial difficulties with the filming included getting the smoke to drift in the right direction, constructing a wooden platform for the spirit to stand on, and the noise of aircraft flying overhead due to the shooting location being close to Heathrow Airport. Planes can be heard in the tracking shot of the rusted cars and cookers, which were brought in specially for the filming.

==Reception==

Lonely Water has received virtual cult status due to living on in the minds of many of its viewers, who were mostly children at the time. Many of its viewers took notice of its message by not swimming in dangerous waters for safetys sake, or because the film had scared them so much that they never wanted to go swimming again. 

==References==
 

== External links ==
*  
*   available to view at The National Archives

 
 
 
 
 
 