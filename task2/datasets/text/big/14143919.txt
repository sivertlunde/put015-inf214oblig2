Jungle Raiders (serial)
{{Infobox film
| name           = Jungle Raiders
| image          =
| image_size     =
| caption        =
| director       = Lesley Selander
| producer       = Sam Katzman
| writer         = Ande Lamb George H. Plympton
| narrator       = Carol Hughes
| music          =
| cinematography = Ira H. Morgan Earl Turner
| distributor    = Columbia Pictures
| released       =  
| runtime        = 15 chapters (? min)
| country        = United States
| language       = English
| budget         =
| gross          =
}} Columbia Serial film serial.

==Plot==
Ann Reed travels to a mysterious land following her father, Dr. Murray Reed, who disappeared into its interior many years ago. Ann falls in with Bob Moore and Joe Riley who have just been mustered out of the military and plan to join Moores father who is researching rumors of a miracle healing drug used by the witch doctors of a mysterious tribe. The owner of the local trading post is determine to keep the scientists out of the area so he can locate a cache of jewels guarded by the tribe without outside interference...

==Cast==
* Kane Richmond as Bob Moore
* Eddie Quillan as Joe Riley, Bob Moores comedy sidekick
* Veda Ann Borg as Cora Bell Carol Hughes as Zara, the High Priestess Janet Shaw as Ann Reed John Elliott as Dr Horace Moore Jack Ingram as Tom Hammil Charles King as Jake Raynes, owner of the trading post Ernie Adams as Charley, a henchman
* I. Stanford Jolley as Brent, a henchman
* Kermit Maynard as Cragg, a henchman
* Budd Buster as Dr Murray Reed, adductee and Ann Reeds father Nick Thompson as the chief of the Arzecs
* Alfredo DeSa as Matu

==Production==
 

===Stunts===
* George Magrill
* Kermit Maynard
* Eddie Parker Wally West

==Chapter titles==
# Mystery of the Lost Tribe
# Primitive Sacrifice
# Prisoners of Fate
# Valley of Destruction
# Perilous Mission
# Into the Valley of Fire
# Devils Brew
# The Dagger Pit
# Jungle Jeopardy
# Prisoners of Peril
# Vengeance of Zara
# The Key to Arzec
# Witch Doctors Treachery
# The Judgment of Rana
# The Jewels of Arzec
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 241
 | chapter = Filmography
 }} 

==See also==
*List of film serials by year
*List of film serials by studio

==References==
 

==External links==
*  
*  

 
{{Succession box Columbia Serial Serial
| before=The Monster and the Ape (1945 in film|1945)
| years=Jungle Raiders (1945 in film|1945)
| after=Whos Guilty? (1945 in film|1945)}}
 

 
 
 

 
 
 
 
 
 
 


 