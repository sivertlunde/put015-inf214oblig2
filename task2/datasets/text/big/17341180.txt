Speaking in Strings
{{Infobox film
| name = Speaking in Strings
| image = Speaking in strings.jpg
| caption = Film poster
| director = Paola di Florio
| producer = Paola di Florio, Lilibet Foster
| writer =
| starring = Nadja Salerno-Sonnenberg
| music = Karen Childs
| distributor = Seventh Art Releasing
| released = DVD: June 26, 2001 Video: June 26, 2001 Theatrical: October 29, 1999
| runtime = 73 mins.
| country = United States English
| budget =
| gross = $6,859
| website =
}} 1999 documentary film directed by Paola di Florio. The film is based on the life of Italian-born violinist Nadja Salerno-Sonnenberg, and it received a nomination for Best Feature Documentary Film at the 72nd Academy Awards.   

==Content==
The film is based on the life and career of Italian-born classical violinist Nadja Salerno-Sonnenberg, and provides an insight into the style of the artist - who is noted by critics for putting her own emotions too much into her musical performances. The film started from Salerno-Sonnenbergs birth in Rome, and followed her story as she moved to United States at age 8 and was brought up by her mother in New Jersey. She grew up with her brother who is also a musician. The film concentrates on the violinists difficult childhood as she had to experience scorn and laughter by her friends when she played a recording of Brahms in front of the class. This is among the incidents that caused the artist to develop her overly emotional performing style and personality as she became a professional violinist and rose to stardom. In the documentary, Salerno-Sonnenberg talks about periods of depression in her life and her suicide attempt; she also discusses her views on how music is connected to human emotions.

==Production and release==
The documentary was directed by Paola di Florio, a childhood friend of Salerno-Sonnenberg.   from San Francisco Chronicle  Salerno-Sonnenberg appeared as herself in the film.  Speaking in Strings was co-produced by two companies, Asphalt Films and CounterPoint Films.   
 Park City, Utah in 1999.  This same year, the documentary appeared in several film festivals, including Florida Film Festival, Mill Valley Film Festival, Newport International Film Festival, and Seattle International Film Festival.  The film was released theatrically on October 29, 1999.  A DVD version was released on June 26, 2001.   

==Reception==
===Box office===
The film earned $1,519 in its opening weekend in one theater,   from Box Office Mojo  and went on to gross $6,859 domestically within two weeks from release. 

===Critical reaction===
The documentary received mixed response from critics. It got a score of 56 out of 100 based on 10 reviews at Metacritic,  and 3.5 out of 4 stars at TV Guide.  In general, critics agreed on the point that the film lacked information on the private life of Salerno-Sonnenberg, particularly her romance relationship which was mentioned during the documentary.   Edward Guthmann of San Francisco Chronicle gave generally positive reviews, stating that the film "takes us inside Salerno-Sonnenbergs experience, challenging us to respect a woman so naturally intense that she cant help wearing out friends and colleagues, and alienating members of her audience." 

Meanwhile,    Critic Robert Hilferty of Village Voice called the documentary "a clumsy labor of love with unforgivable lapses", and noted that it "fails to show why Salerno-Sonnenbergs controversial interpretations are so original and valid." However, he added that it "sometimes rises to a compelling portrait of a neurotic personality". According to Hilferty, the documentary leaves an impression that music is the factor that "ruined" the life of the violinist. 

===Nominations and awards===
Despite the mixed reviews by critics, Speaking in strings received a nomination for Best Feature Documentary at the 72nd Academy Awards.  The film won a Jury Award for Documentary Competition at the Newport International Film Festival   and the same year, won a cable Ace Award.

==References==
 

==External links==
*  at Internet Movie Database
*  CounterPoint Films Company Website
 
 
 
 
 
 
 