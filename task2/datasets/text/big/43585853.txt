The Outcasts of Poker Flat (1952 film)
{{Infobox film
| name           = The Outcasts of Poker Flat
| image          = Outcasts of Poker Flat 1952 film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Joseph M. Newman
| producer       = Julian Blaustein
| writer         = Edmund H. North
| story          = Bret Harte
| starring       = Anne Baxter Dale Robertson Miriam Hopkins
| music          = Hugo Friedhofer
| cinematography = Joseph LaShelle
| editing        = William Reynolds
| distributor    = 20th Century Fox
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} short story 1919 with Harry Carey 1937 with Preston Foster.

==Plot==
A murderous western outlaw (Mitchell), his wife (Baxter), a disgraced gambler (Robertson) and a faded dance hall floozie (Hopkins) and a few other socially undesirable characters are trapped in a snowbound mountain cabin. As the chances for rescue fade, the true natures of the cabins occupants rise to the surface.

==Cast==
* Anne Baxter as Cal
* Dale Robertson as John Oakhurst
* Miriam Hopkins as Mrs. Shipton aka The Duchess Cameron Mitchell as Ryker Craig Hill as Tom Dakin
* Barbara Bates as Piney Wilson
* William H. Lynn as Jake Watterson
* Dick Rich as Drunk

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 