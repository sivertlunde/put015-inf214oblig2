Robots (2005 film)
{{Infobox film 
| name           = Robots
| image          = Robots2005Poster.jpg 
| border         = yes
| caption        = Theatrical release poster
| director       = Chris Wedge William Joyce John C. Donkin
| screenplay     = David Lindsay-Abaire Lowell Ganz Babaloo Mandel
| story          = Ron Mita Jim McClain David Lindsay-Abaire
| starring       = Ewan McGregor Halle Berry Greg Kinnear Mel Brooks Amanda Bynes Drew Carey Robin Williams John Powell
| editing        = John Carnochan
| studio         = Blue Sky Studios 20th Century Fox Animation
| distributor    = 20th Century Fox
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $75 million
| gross          = $260,718,330
}}
 comedy film William Joyce, a childrens book author/illustrator. Originally developing a film version of Joyces book Santa Calls, Joyce and Wedge then decided to develop an original story about a world of robots. Joyce served as producer and production designer for the film. It features the voices of Ewan McGregor, Halle Berry, Greg Kinnear, Mel Brooks, Amanda Bynes, Drew Carey and Robin Williams. 

==Plot== anthropomorphic robots, Herb Copperbottom (Stanley Tucci), a dishwasher robot at Gunks Greasy Spoon diner, races through the streets of Rivet Town, elated that he is going to be a father. He and his wife, Lydia (Dianne Wiest), after 12 hours of "labor", finally manage to construct the baby. He is named Rodney (Ewan McGregor) and he becomes a young inventor who dreams of making the world a better place for everyone. Rodney idolizes Bigweld (Mel Brooks), a master inventor and owner of Bigweld Industries. During Rodneys adolescence, he invents a gadget, "Wonderbot" (Chris Wedge), intended to help his father clean the dishes at the restaurant. When Herbs grumpy supervisor, Mr. Gunk (Dan Hedaya), unexpectedly confronts them, Wonderbot panics and begins breaking dishes, resulting in Rodney being dismissed. Later, he decides to take his invention to Robot City to see Bigweld and get a job as an inventor so that he can help his family. He is encouraged by his father, who confides that he has always regretted not becoming a musician. Rodney arrives in Robot City and meets Fender (Robin Williams), a ramshackle robot scraping by through taking souvenir photos with a camera that has no film and selling maps to the stars homes. After a harrowing ride on the crosstown express with him, Rodney arrives at the gate of Bigweld Industries.

There he learns that Phineas T. Ratchet (Greg Kinnear) has taken over Bigweld Industries and is about to discontinue the manufacture of spare parts. Ratchet believes the company can make more profit if it stops making spare parts for older robots and focuses on selling more expensive upgrades. If any older robots protest, they are sent to the underground Chop Shop, where they are shredded and melted down by Ratchets imposing mother, Madame Gasket (Jim Broadbent). Rodney is forced out and finds a place to stay with Fender, at Aunt Fannys (Jennifer Coolidge) boarding house, along with an assortment of other "Rusties", older robots threatened with the Chop Shop. When the news gets out that spare parts have been discontinued by Bigweld industries, Rodney remembers Bigwelds slogan, "See a need, fill a need", and begins fixing old robots on his own. Upon learning of this financial threat, Gasket orders Ratchet to stop Rodneys work and kill Bigweld.

Rodney finds that Herb has fallen ill and cannot find replacement parts. Rodney decides to try to contact Bigweld directly, so he can beg him to make spare parts again. Wonderbot reminds him that the annual Bigweld Ball takes place that night. Rodney and Fender go to the ball in disguise, only to hear Ratchet announce that Bigweld was unable to attend. Rodney tries to confront Ratchet, but is stopped by security robots. He is saved by Cappy (Halle Berry), a beautiful robot-executive of the company who dislikes Ratchets scheme and together with Fender and his new girlfriend, Loretta Geargrinder (Natasha Lyonne), they escape from the ball.

Fender walks Loretta home, but is captured by a Sweeper and taken to the Chop Shop. He struggles to escape, losing the lower half of his body in the process and eventually escapes by reluctantly attaching a new pair of female legs. Meanwhile, Rodney and Cappy fly to Bigwelds home. When Rodney inadvertently knocks over a domino, setting off a chain reaction of dominoes, Bigweld appears. Rodney tries to convince Bigweld to return to the helm of his company and once again make spare parts available, but he refuses, stating that Ratchets money-making ideals have made him old-fashioned. Rodney calls his parents, intending to give up his ambition of becoming an inventor and return to Rivet Town, but his father again encourages Rodney to pursue his dream. Fender arrives and reveals that Ratchet has built a fleet of super-sweepers with the intention of rounding up and destroying all older robots, so Rodney rallies the Rusties to fight Ratchet. Bigweld eventually decides to come with the group, having realized what he meant to Rodney.

The group heads for Bigweld Industries, where Bigweld attempts to dismiss Ratchet. Ratchet damages Bigweld by hitting him on the head with a communications device, but escapes with Rodneys help. He is repaired just as they enter the Chop Shop, but is captured. A desperate battle ensues between Gaskets employees and Rodneys repaired robots. During the chaos, Bigweld is rescued by Rodney, the choppers and sweepers are destroyed, and Wonderbot kills Gasket by tossing her into the smelter, while Ratchet loses his upgrades and gets stuck on the ceiling with his father. Bigweld goes to Rivet Town to tell Rodneys parents that their son is now his right-hand inventor and eventual successor. Rodney makes his fathers dream come true by giving him a three-bell trumpet, playing by sheer improvisation that prompts the others to help add other musical tunes; Fender finds the new music to be a mix of Jazz and Funk — "Junk". Bigweld gives Diesel a brand new voice box and with it, he sings James Browns "Get Up Offa That Thing" as a victory celebration.

== Cast ==
 
* Ewan McGregor as Rodney Copperbottom, a young blue robot and aspiring inventor
* Halle Berry as Cappy, a worker at Bigweld Industries and Rodneys love interest
* Greg Kinnear as Phineas T. Ratchet, Rodneys nemesis and Madame Gaskets son and right-hand man
* Mel Brooks as Bigweld, the jolly inventor and owner of Bigweld Industries; until Rodney met him, Bigweld had slipped into depression at Ratchets actions
* Robin Williams as Fender Pinwheeler, an old red robot who befriends Rodney and is constantly falling apart
* Amanda Bynes as Piper Pinwheeler, a yellow robot who is Fenders younger sister who has a crush on Rodney
* Drew Carey as Crank Casey, an orange robot who befriends Rodney
* Jennifer Coolidge as Aunt Fanny, a kind motherly robot, who takes in "broke" robots; she has the unintentional effect of bumping people with her large rear
* Harland Williams as Lug, a large green robot who befriends Rodney along with his mute companion Diesel
* Jim Broadbent as Madame Gasket, a twisted, mavolent, and tyrannical robot and Ratchets mother
* Dianne Wiest as Lydia Copperbottom, Rodneys mother
* Stanley Tucci as Herb Copperbottom, Rodneys father and a dishwasher at Gunks
* Natasha Lyonne as Loretta Geargrinder, a receptionist at Bigweld Industries and Fenders love interest (voiced by Cat Deeley in the UK release)
* Paul Giamatti as Tim the Gate Guard
* Dan Hedaya as Mr. Gunk, Herb Copperbottoms grumpy boss

===Cameos===
* Brian Scott McFadden as Trashcan Bot (voiced by Vernon Kay in the UK release)
* Jay Leno as a Fire Hydrant
* Lucille Bliss as a Pigeon Lady
* Paula Abdul as Wristwatch #1
* Randy Jackson as Wristwatch #2 
* Ryan Seacrest as Wristwatch #3
* Al Roker as Mailbox
* Stephen Tobolowsky as Bigmouth Executive and Forge
* Randall Montgomery as Zinc
* Tim Nordquist as Tin Man Sir Terry Wogan in the UK release)
* James Earl Jones as Darth Vader (archive recording)
* James Brown as Diesel (singing voice, archive recording)

== Locations ==
 
Movie director Chris Wedge says New York City, Toronto and London, inspired him to make the city.

There are three parts of the city:
* High End District: The part of the city where the rich and famous robots live. Buildings and robots are all shiny and nearly everything is futuristic. Everyone here has a metal covering that hides all their inner workings. Bigweld Industries is here.

* Combustion District: Low-class place. It has numerous rust spots, and robots have the internal workings similar to a 1950s car at best. Aunt Fannys house is here, and the majority of the movie takes place here.

* Steam District: The lowest part of the city, and therefore the rustiest. Parts of the district resemble the inventions of the Industrial Revolution, and everywhere is filled with broken machines. Sweepers grab old robots and bring them to Madame Gaskets Chop Shop, where mutant robots then break and melt robots, turning them into upgrades.

Another major location of the film is Rivet Town, home to the Copperbottom family.  Two of the buildings there are Gunks Greasy Spoon and Flathead Floyds. Rivet Town is based on Watertown (city), New York|Watertown, New York, where movie director Chris Wedge lived during his teens.

== Release ==
Robots was released theatrically on March 11, 2005. The film was the first to feature the new trailer for  . The film also featured the exclusive trailer for  , then called Ice Age 2. 

===Home media===
The DVD and VHS of Robots were released on September 27, 2005.  The film was accompanied by an original short animated film based on Robots, titled Aunt Fannys Tour of Booty.   On March 22, 2011, it was released in High-definition video|high-definition on Blu-ray Disc|Blu-ray disc. 

==Reception==

=== Critical reaction ===
The film received mixed reviews from the critics. The review aggregator  , gave a score of 64 based on 33 reviews.   

=== Box office ===
The film was released March 11, 2005 in the United States and Canada and grossed $36 million in 3,776 theaters its opening weekend, ranking #1 at the box office.  It grossed a total of $260.7 million worldwide &ndash; $128.2 million in the United States and Canada and $132.5 million in other territories. 

===Accolades===
The film was nominated for many awards in the category of best animated film, as well as awards for character design, best animated character, voice casting, and sound editing. However, it only won one, the MTV (Mexico) Movie Award for best song, "Un Héroe Real". 

== Music ==

===Soundtrack===
{{Infobox album  
| Name        = Robots: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       =
| Released    =  
| Recorded    = hip hop, soul
| Length      =  
| Label       = Virgin Records
| Producer    =
| Reviews     =
}}

Robots: Original Motion Picture Soundtrack was released on March 1, 2005 by Virgin Records. 

{{Track listing
| total_length    =  
| extra_column    = Performer
| title1          = Shine
| extra1          = Ricky Fanté
| length1         = 4:08
| title2          = Right Thurr
| extra2          = Chingy
| length2         = 4:12
| title3          = Tell Me What You Already Did
| extra3          = Fountains of Wayne
| length3         = 1:59
| title4          = Wonderful Night
| extra4          = Fatboy Slim
| length4         = 2:46
| title5          = Get Up Offa That Thing" (Ali Dee Remix)
| extra5          = James Brown
| length5         = 3:40
| title6          = (Theres Gotta Be) More to Life
| extra6          = Stacie Orrico
| length6         = 3:23
| title7          = Loves Dance
| extra7          = Earth, Wind & Fire
| length7         = 4:29
| title8          = Low Rider War
| length8         = 3:15 I Like That
| extra9          = Houston (singer)|Houston, Featuring Chingy, Nate Dogg and I-20
| length9         = 3:58
| title10         = Silence Gomez
| length10        = 2:55
| title11         = Walkie Talkie Man
| extra11         = Steriogram
| length11        = 2:15
| title12         = Robot City John Powell, Featuring Blue Man Group
| length12        = 4:09
}}

;Other songs in the film include
* "Swordfishtrombones|Underground" - Tom Waits
* "Cant Get Enough of Your Love, Babe" - Barry White
* "...Baby One More Time (song)|...Baby One More Time" - Britney Spears See Me" - Melanie Blatt Survivor
* "From Zero to Hero" - Sarah Connor (singer)

===Score===
{{Infobox album  
| Name       = Robots: Original Motion Picture Score
| Type       = Film John Powell
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   =  Score
| Length     =  
| Label      = Varèse Sarabande Records|Varèse Sarabande
| Producer   = 
| Reviews =  John Powell film scores
| Last album = 
| This album = 
| Next album = 
}} John Powell and was released on March 15, 2005 by Varèse Sarabande Records. 

{{Track listing
| total_length    =  
| title1          = Overture
| length1         = 4:02
| title2          = Rivet Town Parade
| length2         = 0:54
| title3          = Bigweld TV / Creating Wonderbot
| length3         = 2:45
| title4          = Wonderbot Wash
| length4         = 2:08
| title5          = Train Station
| length5         = 3:50
| title6          = Crosstown Express
| length6         = 1:19
| title7          = Wild Ride
| length7         = 1:36
| title8          = Madam Gasket
| length8         = 1:00
| title9          = Chop Shop
| length9         = 1:50
| title10         = Meet The Rusties
| length10        = 2:06
| title11         = Bigweld Workshop
| length11        = 3:13
| title12         = Phone Booth
| length12        = 1:29
| title13         = Gathering Forces
| length13        = 3:28
| title14         = Escape
| length14        = 4:42
| title15         = Deciding to Fight Back
| length15        = 1:13
| title16         = Attack of the Sweepers
| length16        = 1:26
| title17         = Butt Whoopin
| length17        = 3:42
| title18         = Homecoming
| length18        = 1:33
| title19         = Dads Dream
| length19        = 1:25
}}

== See also ==
 
*Robots (2005 video game)|Robots (2005 video game)

== References ==
 

== External links ==
 
* 
* 
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 