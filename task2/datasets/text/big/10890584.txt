Aap Beati
{{Infobox film
| name           = Aap Beati
| image          = 
| image_size     = 
| caption        = 
| director       = Mohan Kumar
| producer       = Mohan Kumar
| writer         = 
| narrator       = 
| starring       = Shashi Kapoor Hema Malini Ashok Kumar
| music          = Laxmikant Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1976
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Aap Beati is a 1976 Hindi film. Produced and directed by Mohan Kumar the film stars Shashi Kapoor, Hema Malini, Ashok Kumar, Nirupa Roy, Premnath, Aruna Irani, Helen Jairag Richardson|Helen, Asrani, Sujit Kumar and Madan Puri. The films music is by Laxmikant Pyarelal

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ban Gaya Buddha Sharabi"
| Kishore Kumar, Manna Dey
|-
| 2
| "Hum To Garib Hai"
| Anuradha Paudwal
|-
| 3
| "Kismat Ki Baat"
| Kishore Kumar, Amit Kumar
|-
| 4
| "Meri Dilruba Mere Paas Aa"
| Kishore Kumar, Lata Mangeshkar, Usha Mangeshkar
|-
| 5
| "Neela Pila Hara Gulabi"
| Mahendra Kapoor, Manna Dey, Lata Mangeshkar
|}
== External links ==
*  

 
 
 
 

 