Bai Chali Sasariye
{{Infobox film
| name           = Bai Chali Sasariye
| image          = 
| caption        = 
| director       = Mohansingh Rathod   
| producer       = Bharat Nahta(Sundar Films) 
| writer         = 
| starring       = Jagdeep Lalita Pawar Neelu Vaghela Alankar Upasna Singh
| music          = O._P._Vyas|O.P.VYAS
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 151 minutes 
| country        = India Rajasthani
| budget         = 
| gross          = 
}}
 
Bai Chali Sasariye is a 1988 Rajasthani language film.  The movie ran for 100 days and thus created a history in Rajasthan cinema.  

It was reported in 2004 that the film helped revive interest in films made in the Rajasthani language,  but an article in 2005, speaking toward the decline of the Rajasthani film industry, reported that Bai Chali Sasariye was the only successful Rajasthani film over the previous 15 years.    This movie is remade in Hindi as Saajan Ka Ghar (1994) starring Juhi chawla and Rishi kapoor. 

==Plot==
 

==Box office==
It was the most successful Rajasthani movie in the period 1990-2005. 

==Songs==
All songs of this film were very popular and the title song "Bai Chali Sasariye" is being used as title song in the popular serial Balika Vadhu on Colors channel. Some songs of the film are:
*Bai chali sasariye
*Banna re
*Bhomli aayee
*Hiwde ro har
*Rupiyo to le mhen
*Talariya magariya
*Veera re

==See also==

 

==References==
 

==External links==
*  

 
 
 
 


 