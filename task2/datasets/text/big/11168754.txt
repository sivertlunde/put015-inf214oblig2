Mobutu, King of Zaire
{{Infobox film
| name           = Mobutu, King of Zaïre
| image          = DVD mobutu.jpg
| image_size     =
| caption        =
| director       = Thierry Michel
| producer       =      
| writer         =
| narrator       = Thierry Michel
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = 1999
| runtime        = 135 min
| country        =  Belgium French  English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} 1999 documentary film about Mobutu Sese Seko, the long-time President of Zaire.

==Awards==
* Mention of honor in "Seen by Africa" on 1999 - Montreal - Canada
* Nominee in Los Angeles by international Documentary Association (IDA) for the IDA Awards - United States
* Special mention during the delivery of European Films Awards for Berlin - Germany

 
 
 
 
 
 

 