The Giant of Marathon
{{Infobox film
| name           = The Giant of Marathon
| image          = Giantofmarathon.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Jacques Tourneur Mario Bava
| producer       = Bruno Vailati
| writer         = Alberto Barsanti (story) Ennio De Concini (writer) Augusto Frassinetti (writer) Raffaello Pacini (story) Bruno Vailati (writer)
| narrator       =
| starring       = Steve Reeves Mylène Demongeot Daniela Rocca Ivo Garrani Philippe Hersent
| music          = Roberto Nicolosi
| cinematography = Mario Bava Masino Manunza
| editing        = Mario Serandrei
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 90 minutes
| country        = Italy France
| language       = English budget = 
| gross = $2,735,000  . 
}}
 French international co-production sword and sandal film, loosely based on the Battle of Marathon.  It was directed by Jacques Tourneur and Mario Bava (Bava had to step in to complete the film). It starred Steve Reeves as Pheidippides|Phillipides.

==Plot==

This classical peplum tells a fictitious story set in 490 BC, the time of the Medic Wars during which Persian armies sweep the Ancient world. Having brought home to Athens the Olympic victors laurel crown, Phillippides joins as commander of the Sacred Guard, which is expected to defend the city-states liberty, a year after the expulsion of the tyrant Hippias. 

Athenian supporters of Hippias conspire, hoping to side Phillippides by marriage to Theocrites expensive servant Charis, and thus neutralize the guard. She fails to seduce him, as his heart is already taken by a young girl before the learns her name is Andromeda, daughter of Creuso. 

Everything personal is likely to be put on hold when the news breaks that the vast army of Darius, the Persian King of Kings, is marching on Greece, hoping that its internal division will make its conquest a walk-over. Theocrites instructs Miltiades to hold back the sacred guard to defend the temple of Pallas after a likely defeat, and proposes instead to negotiate terms with Darius, but is told an alliance with Sparta could save the Hellenic nation. 

Phillippides makes the journey and survives a fiendish attempt on his life by conspirators; he returns with Spartas engagement during the Persian attack in far greater numbers on Militiades valiant troops. Charis, left for dead after overhearing Dariuss orders, reaches the camp to tell that the Persian fleet, now commanded by the traitor Theocrites, is heading for the Piraeus to take Athens. Miltiades sends Phillippides ahead to hold out with the sacred guard until his hopefully victorious troops arrive, and after his perilous journey back they do a great job, proving that superior athletes can do better than traditional naval ramming tactics.
 

==Cast== Phillipides
*Mylène Demongeot as Andromeda, Creusos daughter
*Sergio Fantoni as Theocritus
*Daniela Rocca as Charis Callimachus
*Alberto Lupo as Miltiades
*Daniele Vargas as 	Darius I, King of Persia
*Miranda Campa as Andromedas handmaid
*Gianni Loti as Teucro
*Anita Todesco as Andromedas friend
*Ivo Garrani as Creuso
*Sergio Ciani as Euros
*Franco Fantasia Carlo Lombardi
*Ignazio Balsamo
*Gian Paolo Rosmino
*Walter Grant

==Production==
Ten days before the premiere, director Mario Bava was forced to reshoot exterior scenes because several extras were smoking cigarettes on camera. 

==Box Office==
The movie was very successful at the box office: according to MGM records the film earned $1,335,000 in the US and Canada and $1.4 million elsewhere resulting in a profit of $429,000. 

==Biography== 
* 

==See also==
* List of films based on military books (pre-1775)

==References==
 
==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 