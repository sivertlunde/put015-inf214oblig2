Lost Treasure (film)
{{Infobox film
| name           = Lost Treasure
| image          = Lost Treasure FilmPoster.jpeg
| caption        = 
| director       = Jim Wynorski
| producer       = Paul Hertzberg
| writer         = Harris Done Diane Fine
| starring       = Stephen Baldwin Nicollette Sheridan Coby Ryan McLaughin
| music          = Neal Acree
| cinematography = Andrea V. Rossotto
| editing        = Michael Kuge
| distributor    = 
| released       =   
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Lost Treasure is a 2003 action film starring Stephen Baldwin, Nicollette Sheridan and Coby Ryan McLaughlin. It was written by Harris Done and Diane Fine and directed by Jim Wynorski. This movie is about a treasure hunt on a tropical island. The plot involves a treasure supposedly hidden by Christopher Columbus|Columbus. The map that leads to it was split into two halves, so that one would need both pieces to find it.

== Plot ==
The movie takes place in the present day, when a policeman obtains one of the halves, and gives it to his brother to figure out. The policeman is then captured by a mobster who was looking for the treasure, who then forces him to come to the island to help him find it. Meanwhile, his brother hires a small plane (with a female pilot and two incidental passengers) to take him to the island, but a storm ends up stranding everyone. The two groups are forced to survive in the jungle while also clashing with each other.

==Cast==
*Stephen Baldwin as Bryan McBride
*Nicollette Sheridan as Carrie
*Coby Ryan McLaughlin as Det. Carl McBride
*Hannes Jaenicke as Ricaro Arterra Jerry Doyle as Det. Sean Walker
*Mark Christopher Lawrence as Danny G.
*Tami-Adrian George as Tammy
*Scott L. Schwartz as Crazy Joe

==External links==
* 
*  
* 

 
 
 


 