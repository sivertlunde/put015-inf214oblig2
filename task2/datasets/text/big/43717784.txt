Carnival Boat
{{Infobox film
| name           = Carnival Boat
| image          = Carnival Boat poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Albert S. Rogell
| producer       = Charles R. Rogers 
| screenplay     = James Seymour 
| story          = Marion Jackson Don Ryan William Boyd Ginger Rogers Fred Kohler Hobart Bosworth Marie Prevost
| music          = Max Steiner
| cinematography = Ted D. McCord
| editing        = John F. Link Sr. 
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 William Boyd, Ginger Rogers, Fred Kohler, Hobart Bosworth and Marie Prevost. The film was released on March 21, 1932, by RKO Pictures.  {{cite web|url=http://www.nytimes.com/movie/review?res=9A07E4D61F3FE633A25752C2A9659C946394D6CF|title=Movie Review -
  Carnival Boat - Life in a Lumber Camp. - NYTimes.com|publisher=|accessdate=9 September 2014}}  

==Plot==
 

== Cast ==	 William Boyd as Buck Gannon 
*Ginger Rogers as Honey
*Fred Kohler as Hack Logan
*Hobart Bosworth as Jim Gannon
*Marie Prevost as Babe
*Edgar Kennedy as Baldy
*Harry Sweet as Stubby
*Charles Sellon as Lane
*Eddy Chandler as Jordon 
*Walter Percival as DeLacey 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 