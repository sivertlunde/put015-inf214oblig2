The Mysterious Avenger
{{Infobox Film
| name           =The Mysterious Avenger
| image          = Poster of the movie The Mysterious Avenger.jpg
| image_size     = 
| caption        = 
| director       = David Selman
| producer       = 
| writer         = Ford Beebe (original story and screenplay) Peter B. Kyne (story)
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = Columbia Pictures
| released       = January 17, 1936
| runtime        = 54 minutes
| country        = United States English
| budget         = 
}} 1936 United American romantic western film directed by David Selman.

==Cast==

*Charles Starrett	... 	Ranny Maitland / Ranny Morgan
*Joan Perry	... 	Alice Lockhart
*Wheeler Oakman	... 	Brophy
*Edward LeSaint	... 	Lockhart
*Lafe McKee	... 	Maitland
*Hal Price	... 	Sheriff Jon Hall	... 	Lafe Lockhart (as Charles Locher)
*George Chesebro	... 	Henchman Foley
*Sons of the Pioneers... 	Musicians & Rangers
*Roy Rogers	... 	Musician Len (as Len Slye)
*Bob Nolan	... 	Musician Bob (as Sons of the Pioneers)
*Hugh Farr	... 	Musician Hugh (as Sons of the Pioneers)
*Karl Farr	... 	Musician Karl (as Sons of the Pioneers) Tim Spencer	... 	Musician Tim (as Sons of the Pioneers)
*Jack Carlyle... 	Texas Rangers Captain

== External links ==
*  

 
 
 
 
 
 
 
 


 