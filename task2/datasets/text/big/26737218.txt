House of Evil
{{Infobox film
| name           = House of Evil
| image          = House_of_Evil.jpg
| image_size     = 
| caption        = 
| director       = Jack Hill Juan Ibáñez
| writer         = Jack Hill Luis Enrique Vergara
| based on       = House of Evil by Edgar Allan Poe
| narrator       = 
| starring       = Boris Karloff
| music          = Enrico C. Cabiati Alicia Urreta
| cinematography = Raúl Domínguez Austin McKinney
| editing        = 
| studio         = Azteca Films Filmica Vergara S.A.
| distributor    = Columbia Pictures
| released       = 1968
| runtime        = 89 min
| country        = Mexico
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
House of Evil, alternately titled Dance of Death, is a 1968 horror film directed by Jack Hill. It stars Boris Karloff and Julissa. 
 The Snake People, The Incredible Invasion, and Fear Chamber. Karloffs scenes for all four films were directed by Jack Hill in Los Angeles in the spring of 1968. The films were then completed in Mexico. Stephen Jacobs, Boris Karloff: More Than a Monster, Tomohawk Press 2011 p 503-504 

==Plot synopsis==
Morhenge Mansion, 1900: The dying Matthias Morteval invites his dysfunctional relatives to his home for a will reading. However, he dies and soon the relatives are being murdered one-by-one by his living toys.

==Cast==
*Boris Karloff as Matthias Morteval
*Julissa as Lucy Durant
*Andrés García as Beasley
*José Ángel Espinoza|José Ángel Espinosa Ferrusquilla as Dr. Emery Horvath
*Beatriz Baz as Cordelia Rash
*Quintín Bulnes as Ivar Morteval
*Manuel Alvarado as Morgenstein Morteval Arturo Fernández as Fodor

==References==
 

==External links==
* 

 

 
 
 
 


 
 