Idanazhiyil Oru Kaalocha
{{Infobox film
| name           = Idanazhiyil Oru Kaalocha
| image          =
| caption        =
| director       = Bhadran
| producer       Johnson Sebastian. Sajan Mathew
| writer         = Balachandran Chullikkad
| screenplay     = Bhadran
| starring       = Karthika Vineeth Jayabharathi Thilakan
| music          = V. Dakshinamoorthy
| cinematography = U Rajagopal
| editing        = MS Mani
| studio         = Seven Arts International LTD
| distributor    = Seven Arts International LTD
| released       =  
| country        = India Malayalam

 1987 Cinema Indian Malayalam Malayalam film, directed by Bhadran and produced by Bhadran. The film stars Karthika, Vineeth, Jayabharathi and Thilakan in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
*Karthika
*Vineeth
*Jayabharathi
*Thilakan
*Adoor Bhasi Ashokan
*Sankaradi
*Kakka Ravi
*MG Soman
*Paravoor Bharathan

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by ONV Kurup and Traditional. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aavanippoovani || K. J. Yesudas, KS Chithra || ONV Kurup || 
|-
| 2 || Devante Chevadiyanayukilo || K. J. Yesudas || ONV Kurup || 
|-
| 3 || Karaagre Vasathe || Vijay Yesudas || Traditional || 
|-
| 4 || Thedithedi Ananju || K. J. Yesudas || ONV Kurup || 
|-
| 5 || Vaathilppazuthilooden || K. J. Yesudas || ONV Kurup || 
|-
| 6 || Vaathilppazuthilooden || KS Chithra || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 

 