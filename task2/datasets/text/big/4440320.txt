Highlander III: The Sorcerer
{{Infobox film 
| name           = Highlander: The Final Dimension
| image          = Highlander_3_poster.jpg
| screenplay     = Paul Ohl René Manzor Brad Mirman
| story          = Brad Mirman William N. Panzer
| based on       = Characters by Gregory Widen
| starring       = Christopher Lambert Mario Van Peebles Mako Iwamatsu
| director       = Andrew Morahan 
| producer       = Guy Collins Charles L. Smiley Claude Léger
| music          = J. Peter Robinson
| studio         = Fallingcloud Initial Groupe Lumière Pictures Miramax Films Transfilm
| distributor    = Dimension Films
| released       =  
| runtime        = 99 minutes
| country        = Canada France United Kingdom
| language       = English
| budget         = $26,000,000
| gross          = $12,308,080
}}

Highlander III: The Sorcerer, also known as Highlander III, Highlander III: The Magician, Highlander III: The Final Dimension, Highlander: The Final Dimension and Highlander 3: The Final Conflict, is the third installment in the Highlander (franchise)|Highlander film series. It was first released on November 30, 1994. A stand-alone alternate sequel to the original film, it is the final Highlander movie that focuses on Connor MacLeod as the main protagonist.

==Plot==
 
16th century
 Quickening cause the cave to collapse. The Highlander manages to escape in time, but Kane and his men are left trapped inside the depths of the mountain. Their situation prevents them from participating in "The Gathering" of 1985.

18th century

In 1788/1789, Connor was in France, where he makes the acquaintance of Sarah Barrington, an Englishwoman visiting relatives there, and who happens to resemble the future Alex Johnson. The two eventually become lovers. However, when the French Revolution begins, MacLeod becomes involved.

MacLeod is captured, and sentenced to death by guillotine for treason against King Louis XVI of France. His Immortal friend Pierre Bouchet explains that he was tired of his immortal life, and dupes the guards into executing him in his place. Connor is falsely reported deceased. Believing her lover dead, Sarah is left grieving. After his escape, MacLeod returns to discover that she has moved on with her life by marrying another man, and having children.

Modern day
 archaeologists have started excavating a cave in order to discover whether the legend of the sorcerer Nakano was based on fact. One of these archaeologists is Dr. Alexandra Johnson, whose interests in the legend eventually lead her to Connor MacLeod. Connor is intrigued by Alex due to her resemblance to Sarah.

The excavations free Kane, who immediately sets out in pursuit of Connor. MacLeod leaves John in the care of his friend Jack Donovan, and then departs to New York City to engage in the final showdown for the Prize. However, as Khabuls decapitated body is found in a hospital washroom, Lt. John Stenn goes on the trail of the main suspect of the 1985 "headhunter" case, Russell Nash. Russell Nash was the alias used by MacLeod during the time of the Gathering. As Alex investigates a piece of cloth found on the site, she discovers that it is a shred of a kilt, with a design that designates a branch of the MacLeod family. This leads her to Nash Antiques, where Connor has returned in preparation for the battle against Kane. The Highlander is confronted on Holy Ground at a former Buddhist shrine by Kane, who proceeds to challenge him. The resulting fight is a violation of the Immortal Golden Rule; the battle ends when MacLeods katana blade is shattered. Kane flees, and Connor decides to return to the Scottish Highlands to build another sword though his initial attempts are unsuccessful.

Alex tracks him down to give him a bar of finely refined steel that she found in Nakanos cave with which he is able to forge a new sword, and the two then become lovers. However, the trip is cut short, as MacLeod learns from Jack Donovan that his son John is on a plane bound for New York. MacLeod returns as quickly as possible, but hes too late. Kane abducts John, and holds him hostage to lure MacLeod to him. MacLeod meets Kane in an old church mission in Jersey City, NJ, and then follows him into an abandoned power plant for their final battle. The Highlander defeats Kane, wins the Prize by receiving the final Quickening, and returns to Scotland with Alex and John to live out the rest of his natural life.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-  Connor MacLeod / Russell Nash
|- 
| Mario Van Peebles || Kane
|-
| Deborah Kara Unger || Dr. Alexandra Johnson / Sarah Barrington
|-
| Martin Neufeld || Lt. John Stenn
|-
| Mako Iwamatsu || Nakano
|-
| Raoul Trujillo || Senghi Khan
|-
| Jean-Pierre Perusse || Khabul Khan
|-
| Daniel Do || Dr. Fuji Takamura
|-
| Gabriel Kakon || John MacLeod
|-
| Louis Bertignac || Pierre Bouchet
|-
| Michael Jayston || Jack Donovan
|}

==Reception==
Critical reaction to Highlander III has been  negative. It holds a 5% rating on Rotten Tomatoes, based on 20 reviews. 

Stephen Holden of The New York Times remarked, "How could an action-adventure film that cost $34 million, most of which clearly went into pyrotechnics, computerized special effects and scenic locations, end up looking cheap, silly and lifeless?   an incoherent mess   has performances that are one-dimensional even by the undemanding standards of the genre." 

The     it is really a copy of  ." 

Christopher Null of FilmCritic.com also gave Highlander III two stars out of five, saying: "The third in a line of increasingly perplexing Highlander movies, Highlander: The Final Dimension steals wholesale the plot from the original, just throwing in some fresh faces. ... Ultra-fans will rejoice in the face of the third installment—and its nowhere near as bad as Highlander II—but most of you can give it a pass." 

===Box office===

The movie debuted at No.2.  The following week it dropped to No.7. 

==Behind the scenes==

Reportedly, Christopher Lambert considered this film to be the "real" sequel to the original film, since the actual   was such a radical departure from what the first film established. The movie completely ignores the events of Highlander II and makes no mention of them. Also, in a 1996 Cinefantastique interview, Highlander producer William Panzer mentioned that several references to the TV series continuity were inserted into this film as a means of linking it to the TV universe.

Many of the locations in  . Plus, many sequences in New York were actually shot in Montreal, as well. Other scenes for this film were shot in Culture of Morocco#Movies in Morocco|Morocco.

The U.S. theatrical release was rated PG-13 (the rating is slightly noticeable in the poster featured on this page), and a slightly-longer R-rated Special Directors Cut was later released on home video with two sex scenes trimmed for theatrical release restored. Aside from this, additional violence was reinstated, mainly the shot of Kanes head rolling off. The PG-13 theatrical cut originally only showed Kanes head wobbling from side to side, then cutting immediately to the extreme close up on Connor saying "There can be only one."

==Music==

* "Ce He Mise le Ulaingt? The Two Trees" by Loreena McKennitt available on her album "The Mask and Mirror"
* "Bonny Portmore" by Loreena McKennitt, available on her album "The Visit" God Took A Picture" by Suze DeMarchi
* "Bluebeard (song)|Bluebeard" by Cocteau Twins
* "Dr. Feelgood (song)|Dr. Feelgood" by Mötley Crüe (instrumental riff)
* "Dummy Crusher" by Kerbdog
* "Little Muscle" by Catherine Wheel
* "Boom Boom" by Definition Of Sound James

==References==
 

==External links==
 
*  
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 