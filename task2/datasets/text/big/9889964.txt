Baaghi: A Rebel for Love
{{Infobox film
| name = Baaghi
| image=Baaghi A Rebel for Love.jpg
| writer = Javed Siddiqui
| story by = Salman Khan
| starring = Salman Khan Nagma Kiran Kumar Shakti Kapoor Mohnish Bahl Asha Sachdev
| producer = Nitin Manmohan, Neha Arts
| director = Deepak Shivdasani
| music    = Anand-Milind Sameer (lyrics)
| released = 11 December 1990
| runtime  = 157 minutes
| country  = India
| language = Hindi
| budget   = 45 lacs 
| gross    = 4 crore 
}}
Baaghi: A Rebel for Love is a Bollywood Action romantic drama film, starring Salman Khan, Nagma, and Shakti Kapoor, which was released in 11 December 1990. It was Nagmas first role in Bollywood; as the opening credits note, she was 15 years old when the film was released. The DVD cover has a warning noting that the movie is "suitable only for persons of 15 years and older," presumably because the plot revolves around prostitution. The subtitle "A Rebel for Love" does not appear on the DVD box, nor in the Hindi titles or license at the start of the movie. According to BoxOfficeIndia.com, Baaghi is reported to have been Bollywoods  seventh highest grossing film in 1990, despite its release in mid-December holiday. 

The movie opens with a dedication, which reads: "In this year of the girl child, we dedicate our film to those women, who have been victimised by lust and greed and are subjected to social rejection and also laud those who strived to uplift them." The movie was a hit. The story of Baaghi: A rebel for Love was Salmans idea for which he was given due credit.

== Plot ==

The story, based on an idea from Salman Khan, centres on Saajan, the son of a colonel in the Indian army, and Kaajal, a modest girl from "a respectable family." The film opens with Saajan travelling in a bus, when he catches a glimpse of Kaajal on another bus, and they are both smitten. They do not formally meet and, since Saajan is off to start at college, he does not think he will ever see her again. But his new friends at college, Buddha, Tempo and Refill, one night insist on visiting a brothel in a seedier part of Bombay. Saajan only reluctantly agrees, but ultimately refuses to select a prostitute &mdash; until he hears a new girl being beaten by her pimp and decides to protect her. To his surprise, it is Kaajal (called Paro at the brothel), who has been kidnapped by a pimp after she was tricked by a job offer in Bombay.

Kaajal, who has only very recently arrived at the brothel and is still a virgin, has adamantly refused to be a prostitute. This is why Jaggu, who runs the brothel, is beating her. When finally alone with Saajan as a paying client (although he does not do anything with her), Kaajal explains to him how she was forced to look for work after her parents deaths: This ultimately led to her travelling to Bombay and being kidnapped by Dhanraj and forced to work in Jaggus brothel. Thanks to Leelabai, the madame who helps run the brothel, Saajan is able to spend time with Kaajal, and Kaajal is somehow able to resist becoming a prostitute. Saajan and Kaajal fall in love, and he tries to find a way to get her out of the brothel before Kaajal gives up hope.

When Saajan is finally able to introduce Kaajal to his parents, they &mdash; not surprisingly &mdash; reject the idea of his marrying a girl from a brothel, even if she was taken there against her will and is from a respectable family. Since Saajans father, Col. Sood, was already angry with his son for refusing to follow family tradition to join the Indian army, this is the last straw. Saajan is kicked out of his house. He becomes, in his own words, a rebel, a word which is repeated several times in the movie. Since Kaajal is already rebelling against Jaggu because she believes in love, they are now both "rebels for love." Saajans college friends help Kaajal escape the brothel and flee to Ooty, near where her grandparents live. But just as they are about to be married with Kaajals grandparents consent, if not Saajans fathers, police arrive and take them back to Bombay, where they claim hes wanted for kidnapping "Paro".

Saajans father, on hearing of his heroics fighting Dhanrajs men to rescue Kaajal, has a new-found respect for his son, who had previously been a lazy drifter. With the help of Saajans friends, Col. Sood finds his son outside Jaggus brothel. There the "police" (who are actually working for Dhanraj) have returned Saajan and Kaajal to Dhanraj, who is preparing to punish them for her leaving the brothel. The intervention of Leelabai on Kaajals behalf leads to yet another fight, with several people switching alliances.

== Cast ==
* Salman Khan ... Saajan Sood
* Nagma ... Kaajal aka Paro
* Shakti Kapoor ... Dhanraj
* Kiran Kumar ... Colonel D.N. Sood
* Beena Banerjee ... Mrs. Vandana Sood
* Mohnish Behl ... Jaggu
* Asha Sachdev ... Leelabai
* Salim Khan ... Salim Pradeep Singh Rawat ... "Buddha"
* Raju Shrestha ... Raju / Refill
* Dinesh Hingoo .... College Principal
* Amita Nangia ..... Jaggus sister

==Music==

The soundtrack has seven songs composed by Anand-Milind with lyrics by Sameer. The music was hugely popular when it released and enjoys the honour of being played on radio stations even today. The most popular song of this film is "Chandni Raat Hain," sung by Kavita Krishnamurthy and Abhijeet Bhattacharya, who was re-launched by Anand-Milind with this film. Abhijeet and Anand-Milind went on to record several songs together since. 

The songs "Tapori" and "Chandni Raat Hai" were lifted from Illayarajas movies, Agni Natchathiram and Pudhu Pudhu Arthangal, respectively.

Anand-Milind were nominated at the Filmfare awards for Best Music but lost to Nadeem-Shravan for Aashiqui. A minor controversy arose when singer Amit Kumar was mistakenly nominated for "Chandini Raat Hai" at the Filmfare awards ceremony.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Title !! Singer(s) !! Length 
|-
| 1
| "Ek Chanchal Shokh Haseena" Abhijeet
| 06:40
|-
| 2
| "Kaisa Lagta Hai"
| Amit Kumar & Anuradha Paudwal
| 06:33
|-
| 3
| "Chandni Raat Hai" Abhijeet & Kavita Krishnamurthy
| 04:59
|-
| 4
| "Tapori"
| Amit Kumar & Anand Chitragupt
| 05:29
|-
| 5
| "Har Kasam Se Badi Hai" Abhijeet & Kavita Krishnamurthy
| 05:59
|-
| 6
| "Maang Teri Saja Doon Mein" Amit Kumar
| 01:29
|- 
| 7
| "Saajan O Saajan" Pramila Gupta
| 04:52
|}

==References==
  

==External links==
*  

 
 
 
 
 
 
 