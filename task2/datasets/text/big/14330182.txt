Dharm (film)
{{Infobox film
| name = Dharm
| image = Dharm.gif
| caption = Movie poster for Dharm
| director = Bhavna Talwar
| writer = Varun Gautam Vibha Singh
| starring = Pankaj Kapoor  Hrishitaa Bhatt Supriya Pathak
| producer = 
| distributor = 
| cinematography = 
| editing = Steven Bernard
| released =  
| runtime = 
| country = India
| language = Hindi
| music = 
}}
Dharm ( ) is a 2007 Hindi film directed by Bhavna Talwar and starring Pankaj Kapoor and Supriya Pathak in lead roles. This debut film of the director  addresses the theme of communal harmony. Most of it is shot in Varanasi.
 National Film Award, it won the Nargis Dutt Award for Best Feature Film on National Integration.  The film was premiered in the Tous Les Cinemas du Monde (World Cinema) section of 2007 Cannes Film Festival.  

==Plot==
 Benares and is about Pandit Chaturvedi (Pankaj Kapoor), a highly revered and learned Brahmin priest. A baby is abandoned by a woman and brought to his house by his daughter. He agrees to adopt the child due to requests from his wife (Supriya Pathak). Life takes a turn when the boys mother returns: The family finds out that the boy is Muslim after they have become attached to him. The family gives back the boy to his mother. Chaturvedi engulfs himself in purification processes to cleanse his body, mind, and soul due to contact with a Muslim soul. By the time Chaturvedi thinks he is fully purified, the child reappears &mdash; seeking refuge, due to Hindu-Muslim riots. This is when Chaturvedi realizes that the true religion is humanity.

==Critical reception==
Dharma was premiered  at the 60th  . 

At home, though it wasnt a box office success, it opened to excellent reviews from the critics  and later the world rights of the film were acquired by Films Distribution, France.  

Dharm was embroiled in a controversy in India, where it became one of the finalists for Indias official entry to the Oscars, a race it lost to  . 

==Cast==
* Pankaj Kapoor
* Supriya Pathak
* Hrishitaa Bhatt
* Pankaj Tripathi
* K. K. Raina
* Daya Shankar Pandey

==References==
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 
 