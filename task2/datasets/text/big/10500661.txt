Opening of the Kiel Canal
{{Infobox film
| name           = Opening of the Kiel Canal
| image          =
| caption        =
| director       = Birt Acres
| producer       = Birt Acres Robert W. Paul
| writer         =
| starring       =
| cinematography = Birt Acres
| editing        =
| distributor    =
| released       =  
| runtime        = 18 seconds
| country        = United Kingdom Silent
| budget         =
| gross          =
}}
 British Short short black-and-white silent documentary documentary news Kaiser Wilhelm II on 20 June 1895.

Unlike most of the films made by Birt Acres on this occasion, the video of this film survives and the original negative is held by the Science Museum (London). 

==Synopsis==
The film opens with a shot of the North Sea Canal with a winged statue on the far side. Three men row towards the shore in a small rowing boat while a small boy waits on the quayside and then turns to face the camera. 

==Legacy==
Film historian Nicholas Pronay has cited this film as one of the "archetypes of the newsfilm", as it was not "visually exciting", but interested audiences because it documented "an event over which there was already public sensitivity and which the mind of the audience would invest with great fascination".   

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 
 