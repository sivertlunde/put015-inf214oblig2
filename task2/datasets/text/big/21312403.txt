Nenè
{{Infobox film
| name           = Nenè
| image          = Nenè.jpg
| caption        = 
| director       = Salvatore Samperi
| producer       = 
| writer         = Alessandro Parenzo Salvatore Samperi
| starring       = Leonora Fani 
| music          = Francesco Guccini
| cinematography = Pasqualino De Santis
| editing        = Ezio Altieri
| distributor    = 
| released       = 1977
| runtime        = 108 minutes
| country        = Italy
| awards         =
| language       = Italian
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    =
}}
 1977 Cinema Italian drama film directed by Salvatore Samperi. The film is a historical drama, set in post-war Italy in 1948 during the first free elections after the war. It tells of a romance and a coming-of-age amid a difficult family life and amid national political tensions.

The film was an adaptation of the best-selling novel of the same name, written by Cesare Lanza. His novel won the Premio Sila award in 1976.

==Plot==
Ju is a nine-year-old boy growing up in the aftermath of World War II in Italy. He is observant of the difficulties surrounding him. Both his father and mother have suffered emotionally, though his mother has suffered more due to the ongoing sexual and physical abuse caused by her husband. 

Jus orphaned fifteen-year-old cousin Nenè, comes to live with his family. Through Nenè, Ju learns even more of the strange adult world that he has yet to enter. Nenè allows him to sleep in her bed and confides in him of her growing sexuality and her secret affair with a local Mulatto boy.

==Cast==
*Leonora Fani as Nenè
*Sven Valsecchi as Ju
*Tino Schirinzi as Father of Ju and Pa
*Paola Senatore as Mother of Ju and Pa
*Rita Savagnone as Teacher of Ju
*Vittoria Valsecchi as Pa
*Alberto Cancemi as Rodi
*Ugo Tognazzi as "Baffo", the barber

==Production notes==
The Italian censorship rating for the film is suitable for age 14 and above.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 

 