Lafayette Escadrille (film)
{{Infobox film
| name           = Lafayette Escadrille
| image          = Lafayette Escadrille poster.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = William A. Wellman
| producer       = William A. Wellman
| screenplay     = Albert Sidney Fleischman
| story          = William A. Wellman
| starring       =  
| narrator       = William A. Wellman  (uncredited) 
| music          = Leonard Rosenman	 
| cinematography = William H. Clothier	 	 
| editing        = Owen Marks
| studio         = Warner Bros.
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
 }}

Lafayette Escadrille, also known as Cest la Guerre, Hell Bent for Glory (UK) and With You in My Arms, is an American   and features David Janssen and Will Hutchins as well as Clint Eastwood in an early supporting role. It was the final film in the career of director William A. Wellman and is based on his original story. 

==Plot==
Thad Walker (Tab Hunter), a spoiled, rich kid from Boston, who had gotten in serious trouble with the law, fled to France to join the French Foreign Legion in World War I. In Paris, with companions, "Duke" Sinclair (David Janssen), Dave Putnam (Will Hutchins), Tom Hitchcock (Jody McCrea) and Bill Wellman (William Wellman Jr.), the boys stop at a bar and learn of the recent formation of the Lafayette Escadrille made up of American volunteer pilots who fly for France. The group of expatriates join up and learn to fly on training aircraft before becoming combat pilots.

While off duty, Walker meets and falls in love with Renée Beaulieu (Etchika Choureau), a common streetwalker with some sensitivity; she quits the oldest profession and takes a job, reforming for her American lovers sake. Walkers father beat him, and he resents any kind of authority. When a strutting, arrogant French officer (Marcel Dalio), irritated by the young mans inability to understand commands in French, strikes him, he knocks the officer to the ground, a very serious offense. Before he can be jailed, his pals smuggle Walker out of camp.  He then spends a great deal of time hiding in Paris in his sweethearts apartment. His friends continue with their training while Walker works for the Madam (Veola Vonn), hoping to make enough money to run away to South America with his girlfriend.
 Air Service.   Walker finally is able to fly a mission with the Lafayette Escadrille, where he proves to be a superb fighter pilot. Returning to Paris, Walker asks his friends to join him as he weds Renée.

==Cast==
  
* Tab Hunter as Thad Walker
*   as Renée Beaulieu
* Marcel Dalio as Drill Sergeant
* David Janssen as "Duke" Sinclair
* Paul Fix as U.S. General
* Veola Vonn as The Madam Dave Putnam
* Clint Eastwood as George Moseley
* Robert Hover as Dave Judd
* Tom Laughlin as Arthur Blumenthal
 
* Brett Halsey as Frank Baylies
* Henry Nakamura as Jimmy
* Maurice Marsac as Sgt. Parris
* Raymond Bailey as Amos J. Walker
* William Wellman Jr. as William A. Wellman Tom Hitchcock Lawrence "Red" Scanlon Dudley Tucker
* Sam Boghosian as Booth
 

Cast notes:
*The roles of "Duke" Sinclair (Reginald "Duke" Sinclaire), Dave Judd (Edward David Judd), Arthur Blumenthal and Frank Baylies (Frank Leamon Baylies) were also based by Wellman on actual members of the Escadrille Raoul Gervais Lufbery, is essentially a walk-on role.

==Production==
 
 Croix de Guerre with two palms. Silke 1980, p. 57. 

Although he considered Lafayette Escadrille a "personal project", the studio did not give Wellman the budget he demanded and continued to interfere with the project, to the extent that the decisions on starring roles, title, ending and other important aspects of the production were taken out of his hands,  including the title of the film: Wellmans original title was Cest la Guerre which the studio, despite his objections, changed to Lafayette Escadrille.   In casting, Wellman wanted Paul Newman and Clint Eastwood as the leads; studio head Jack Warner refused, and substituted teen idol Tab Hunter and David Janssen, with Eastwood moved to a minor role.  Warner also insisted Wellman make Darbys Rangers as a condition of financing Lafayette Escadrille. Nat Segaloff, Final Cuts: The Last Films of 50 Great Directors, Bear Manor Media 2013 p&nbsp;312–314 

The use of mocked-up Nieuport 28 and Thomas-Morse Scout fighters along with other period aircraft such as one real Fokker D.VII and the ubiquitous Travelair "Wichita Fokkers" were "lifted" from Wellmans earlier 1938 production, Men with Wings, an early color feature also directed by Wellman.  Principal photography took place primarily at the Hancock Santa Maria, California airport. Hollywood stunt pilot Paul Mantz, built a number of Blériot XI "Penguin" clipped-wing and full span training aircraft, used in the training sequences. 

Wellmans script was rewritten by Paul Fix.  Shooting took place October 19 to December 8, 1956. 

==Reception==
In the original cut of the film Tab Hunters character died at the end. However this was poorly received at previews and a new ending was shot in April–May 1957 where he lived. 
 William Clothier filmed the spectacular aerial sequences, evocative of those he shot in Wellmans earlier silent classic Wings (1927 film)|Wings), critics said the film falls far short of the classic status of the 1928 Oscar winner. The flying sequences were not enough to overcome a mediocre story and flat acting, aspects roundly panned by critics. Howard Thompson, reviewer for The New York Times called it a "flapdoodle" in his blistering review. Variety echoed other reviews, noting, "What could have been an reasonably good actioneer&nbsp;... has been badly marred by a flat predictability in plot, intrusion of an inept and, at times, ludicrously irrelevant romance and some quite dreadful dialog. 

The Lafayette Escadrille  was also totally disowned by those still alive who had flown as part of the fabled Lafayette Escadrille and the Lafayette Flying Corps, who were understandably upset at their portrayal, including Wellman who insisted that his producers credit be removed.  This was to be William Wellmans last directorial effort; it had started out to be a paean to his memories of the storied squadron, but ended up a target for insults, accusations and lawsuits, not the least of which were directed against Jack Warner and Warner Brothers Studios for their heavy-handed interference. Nixon, Rob.   Turner Classic Movies. Retrieved: March 12, 2012.  The film was shelved for two years, partly because of the wrangling between the two Hollywood heavyweights and partly because of the influence of Hunters later singing career, that necessitated a "pollyanna" ending grafted into the film.  Wellman was "heartbroken" with his treatment at the hands of Jack L. Warner, and kept his word that Lafayette Escadrille would be his last film. 

==References==
Explanatory notes
 

Citations
 

Bibliography
 
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Hunter, Tab and Eddie Muller. Tab Hunter Confidential: The Making of a Movie Star. New York: Algonquin Books, 2005. ISBN 978-1-56512-466-0.
* Parish, James Robert. The Great Combat Pictures: Twentieth-Century Warfare on the Screen. Metuchen, New Jersey: The Scarecrow Press, 1990. ISBN 978-0-8108-2315-0.
* Silke, James R. "Fists, Dames & Wings." Air Progress Aviation Review, Volume 4, No. 4, October 1980.
* Thompson, Frank T. William A. Wellman (Filmmakers Series). Metuchen, New Jersey: Scarecrow Press, 1983. ISBN 0-8108-1594-X.
* Wellman, William A. Go, Get em! The True Adventures of an American Aviator of the Lafayette Flying Corps. Boston: The Page Company, 1918.
* Wellman, William A. A Short Time for Insanity: An Autobiography. New York: Hawthorn Books, 1974. ISBN 0-8015-6804-8.
* Wellman, William Jr. The Man And His Wings: William A. Wellman and the Making of the First Best Picture. New York: Praeger Publishers, 2006. ISBN 0-275-98541-5.
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 