Me and You (film)
 
{{Infobox film
| name           = Me and You
| image          = Io e te (film).jpg
| caption        = Film poster
| director       = Bernardo Bertolucci
| producer       = Mario Gianani Lorenzo Mieli
| writer         = Bernardo Bertolucci Niccolò Ammaniti Umberto Contarello Francesca Marciano
| starring       = Jacopo Olmo Antinori Tea Falco
| music          = 
| cinematography = Fabio Cianchetti
| editing        = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Me and You ( ) is a 2012 Italian drama film directed by Bernardo Bertolucci. The film was screened out of competition at the 2012 Cannes Film Festival.      

==Cast==
* Jacopo Olmo Antinori as Lorenzo
* Tea Falco as Olivia
* Sonia Bergamasco as Lorenzos mother
* Pippo Delbono
* Veronica Lazar

==Reception==
Philip French of The Observer called it "a lightweight disappointing affair".   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 