Young Man of Manhattan
{{Infobox film
| name           = Young Man of Manhattan
| image          = Young Man of Manhattan poster.jpg
| caption        =
| director       = Monta Bell
| producer       = Monta Bell Daniel Reed Katherine Brush (novel) Robert Presnell Sr. (story) Norman Foster Ginger Rogers Charles Ruggles
| music          = Sammy Fain Pierre Norman W. Raskin Larry Williams
| editing        = Emma Hill
| studio         = Paramount Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         =
}}  Norman Foster, Ginger Rogers and Charles Ruggles. It had been filmed in New York City.

==Plot==
Jealousy comes between a young couple of newspaper people when the wife earns more money and becomes more famous than her husband. Especially his alcohol addiction becomes the dividing element, whereas the young Puff Randolph girl chasing him, and her editor falling in love with her are merely elements that challenge their love.

==Cast==
*Claudette Colbert as Ann Vaughn Norman Foster as Toby McLean
*Ginger Rogers as Puff Randolph
*Charles Ruggles as Shorty Ross
*Leslie Austin as Dwight Knowles
*Lorraine Aalbu, Aileene Aalbu, Fern Aalbu, Harriet Aalbu as the Sherman Sisters (as Aalbu Sisters)
*H. Dudley Hawley as Doctor

==Quotes==
*Puff Randolph: Cigarette me, big boy.

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 