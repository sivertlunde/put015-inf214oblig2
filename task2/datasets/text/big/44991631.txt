Shiva Kotta Sowbhagya
{{Infobox film 
| name           = Shiva Kotta Sowbhagya
| image          =  
| caption        = 
| director       = Hunsur Krishnamurthy
| producer       = V. Vargheese
| writer         = Hunsur Krishnamurthy
| screenplay     = Hunsur Krishnamurthy
| starring       = Lokesh Aarathi Jayanthi Rajanand
| music          = T. G. Lingappa
| cinematography = K. Mallik
| editing        = R. Rajan R. Rajashekar
| studio         = Santhosh Combines
| distributor    = Santhosh Combines
| released       =  
| country        = India Kannada
}}
 1985 Cinema Indian Kannada Kannada film, directed by Hunsur Krishnamurthy and produced by V. Vargheese. The film stars Lokesh, Aarathi, Jayanthi and Rajanand in lead roles. The film had musical score by T. G. Lingappa.  

==Cast==
*Lokesh
*Aarathi
*Jayanthi
*Rajanand
*B. K. Shankar
*Dingri Nagaraj
*Roopa Chakravarthy

==Soundtrack==
The music was composed by TG. Lingappa. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aduva Nalidaduva || Sulochana || Hunsur Krishna Murthy || 04.03
|- Manjula || Hunsur Krishna Murthy || 02.57
|-
| 3 || Darushanake Na Bande || Rama || Hunsur Krishna Murthy || 04.24
|- Manjula || Hunsur Krishna Murthy || 04.18
|-
| 5 || O Gajaraja || Jolly Abraham || Hunsur Krishna Murthy || 04.39
|- Susheela || Hunsur Krishna Murthy || 03.28
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 


 