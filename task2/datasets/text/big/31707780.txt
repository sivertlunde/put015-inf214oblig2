Guns in the Heather
{{Infobox film
| name           = Guns in the Heather
| image          = Guns in heather arg 1sh.jpg
| image_size     = 
| caption        = Spanish-language poster for European theatrical release as "El Secreto del Castillo" (English title&nbsp;— "The Secret of Boyne Castle") Robert Butler Ron Miller
| writer         = 
| narrator       = 
| starring       = Glenn Corbett
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = July 1969
| runtime        = 3:00:00 (180 minutes)
| country        = United Kingdom United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} family film Robert Butler Ron Miller. Walt Disneys Wonderful World of Color in the United States under the title Guns in the Heather, then re-edited for a European theatrical release under the English title, The Secret of Boyne Castle.  It was re-broadcast on American television in 1978 under the title The Spybusters.  The story is based on the 1963 novel Guns in the Heather, by Lockhart Amerman.

The film was primarily shot on location in Ireland, with additional scenes shot at Pinewood Studios near London, England.

==Cast==
*Glenn Corbett as Tom Evans
*Alfred Burke as Kersner
*Kurt Russell as Rich Evans
*Patrick Dawson as Sean OConnor
*Patrick Barr as Lord Boyne Hugh McDermott as Carleton  
*Patrick Westwood as Levick  
*Eddie Byrne as Bailey  
*Godfrey Quigley as Meister  
*Kevin Stoney as Enhardt
*Shay Gorman as Headmaster
*Niall Tóibín as Kettering
*Ernst Walder as Vollos
*Robert Bernal as Sgt. Clune
*Vincent Dowling as Maston John Horton as Stafford
*J. G. Devlin as Muldoon Nicola Davies as Kathleen Gerry Alexander as Paddy Eamon Morrissey as Hennessey
*Declan Mulholland as Retchick
*Mary Larkin as Mary
*Paul Farrell as Groundskeeper

==References==
 

==External links==
* 

 

 
 
 
 
 

 