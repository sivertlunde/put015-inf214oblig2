Eskrimadors
{{Infobox film
| name           = Eskrimadors
| image          = Eskrimadors film poster.jpg
| caption        = Theatrical poster
| director       = Kerwin Go
| producer       = Kerwin Go Jiji Borlasa
| writer         = Kerwin Go
| narrator       = Miguel Vasquez
| music          = Arnold Ang Jay Young
| cinematography = Ruel Dahis Antipuesto
| editing        = Bonbon Señorin
| studio         = 
| distributor    = Pointsource Films
| released       =  
| runtime        = 66 minutes
| country        = Philippines
| language       = Filipino Cebuano Tagalog 
| budget         =
| gross          =
}}
Eskrimadors is a 2010 Philippine documentary film about the Filipino martial arts eskrima, written and directed by Kerwin Go,      a Los Angeles trained cinematographer.  

==Background==
According to the Kerwin Go, the films writer and director, the idea of the film came about when the production staff thought of making a film that was uniquely   in the northern part of Cebu. 

==Synopsis==
The film documents the development of the martial arts of eskrima, tracing its origins from the tribal warfares of the Philippines to its practice among international martial artists. It include interviews with grandmasters Ciriaco “Cacoy” Canete, Dionisio Canete, and Undo Caburnay, and participation of groups such as Doce Pares, Lapunti Arnis de Abanico, Teovel’s Balintawak, Nickelstick Balintawak, and Liborio Heyrosa Decuerdas, in reenactments.  

==Release== bill proposing Eskrima as a national martial arts of the Philippines passed.

During the première, live demonstrations from different Eskrima groups including Cacoy Cañete, now in his 90s, opened the night. After the movie was screened, the Grandmasters were awarded their certificates of appreciation for their continued practice and promotion of the art of Eskrima. It was a historical moment as 12 Grandmasters were on the same stage at the same time. 

===Reception===
Twitch Film began their review by noting that action films were once "synonymous with the Philippines", and that even as much action as it has, Eskrimadors "is not an action film per se. It is a documentary, and a very good one at that."  The reviewer notes that writer Kerwin Go concentrated on eskrima in the film, a form of martial arts using rattan sticks that originated in island of Cebu... a form known in other parts of the Philippines as arnis.  The reviewer wrote that to martial arts enthusists, the "documentary is something of a well-packaged tribute" and an homage to a sport "that has been sadly relegated locally as mere curiosity when it has actually turned into a world-wide phenomenon". It is also shared that the film has "a distinctly solid narrative flow, and a visual flair that outwits the budgetary constraints of a local independent production. Its simply fantastic filmmaking." 
 Cebu Daily News made note that fight scenes in film are often the most difficult to shoot, as a miss-step in the choreography could make what was intended to be serious into something comic. They then praised the scenes in the film writing that that the sequence "mimics the rhythmic choreography of combat, a strange dance macabre."  They also noted that the artistic techniques used in re-enactments were subtle.  They wrote that in its depiction of the form of martial arts indigenous to the Philippines which saw its development in the Cebu region, the film proves to be "a definitive account of the story of Eskrima". 

On January 12, 2010, Karlon N. Rama of Sun Star Cebu wrote "Kerwin’s Eskrimadors offers a great visual experience. After the screening last week, I found myself wanting to rush back home and do amara."  He added, "to fully appreciate the film, it is not enough to just undergo the visual experience and leave it at that. One must also look at Kerwin’s other intent—to document eskrima as it has been practiced in Cebu."  

In his own review two weeks later, Bob Lim of Sun Star Cebu noted that martial artist Bruce Lee incorporated eskrima into his combat repertoire.  In describing the film, he reported that "the narrations are adroitly interspersed with well-choreographed actions scenes. One of the best parts of the film is the last portion where it shows the significant inroads of foreign eskrimadors to the sport," and "the viewer is treated to the incredible but lethal beauty of eskrima, its speed and energy presented in fast-paced editing, top caliber cinematography and a musical score that captures the up-tempo action." 

In an interview by Philippine Daily Inquirer, wherein he spoke about Filipino martial arts|FMA,  filmmaker Jay Ignacio stated that when he saw the preview of Eskrimadors he was "moved to tears", saying "It made me extremely proud to be a Filipino." 

==Participants==
* Ciriaco "Cacoy" Cañete
* Dionisio "Diony" Cañete
* Undo Caburnay
* Nick Elizar
* Uwit Jecong
* Fredo Carin
* Chito Velez
* Percival Pableo
* Rudy Rey
* Harry Talledo
* Ron Talledo
* Miguel Vasquez as narrator

==References==
 

   

   

   

 {{cite news|url=http://www.sunstar.com.ph/cebu/rama-eskrima-screen|title=Eskrima on screen|last=Rama
|first=Karlon N.|date=January 12, 2010|work=Sun.Star Cebu|accessdate=7 February 2011}} 

   

   

   

   

{http://www.manilatimes.net/index.ph/lifestyle/10016-eskrimadors}
 
 

== See Also ==
* Arnis
* Filipino Martial Arts

==External links==
*  
*  

 
 
 
 
 
 
 
 