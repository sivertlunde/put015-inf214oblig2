Romeo & Juliet: Sealed with a Kiss
 
{{Infobox film
| name           = Romeo & Juliet: Sealed with a Kiss
| image          = Sealedwithakiss.jpg
| caption        = Theatrical release poster
| director       = Phil Nibbelink
| producer       = Margit Friesacher Phil Nibbelink
| writer         = Phil Nibbelink William Shakespeare (play)
| narrator       = Michael Toland
| starring       = Daniel Trippett Patricia Trippett Chip Albers Michael Toland Stephen Goldberg Phil Nibbelink Chanelle Nibbelink
| music          = Christopher Page Tommy Carter Stephen Bashaw Jack Waldenmaier Steven Wenger Bill Holloman Curt Macdonald Kirk Cirillo
| editing        = Phil Nibbelink
| studio         = Phil Nibbelink Productions
| distributor    = Indican Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| gross          = $463,002  . Box Office Mojo. Retrieved August 3, 2012.  budget         = $2,000,000  . The Big Cartoon Database. Accessed on May 25, 2013. }} American animation|animated romantic fantasy film loosely following the play Romeo and Juliet by William Shakespeare. The film is about two star-crossed seals, Romeo and Juliet (played by Daniel and Patricia Trippet respectively), who fall in love against the wishes of their warring families. It was released in Europe in mid-2006 and on October 27 in the United States.
 Wacom tablet Flash 4 Moho software. Despite the films critically negative reviews, it won an award in Best in Show at the Southwest Film Festival.

==Plot== Steller and California pinniped|sea elephant seal, shark lives. Romeo (Daniel Tripett), Montagues only son, is depressed, wishing to fall in love with someone. His humorous friend, Mercutio (Chip Albers), urges him and another of his friends, Benvolio (Sam Gold), to go to a Capulet party later that evening. They attend the party, covered in white sand to look like Capulets, and Romeo falls in love with Juliet at first sight. Juliet, however, was promised by her father to marry the Prince, who attends the party. Romeo and his friends manage to wreak havoc, and are revealed to be Montagues. Later that evening, the plays balcony scene is recreated on a cliff on the beach where a tree grows. Romeo promises Juliet that they shall marry the next morning, and she will not have to marry the Prince.

Romeo begs Friar Lawrence, a sea otter, to wed them. After some thought, the friar believes their marriage will end the feud between their families, and agrees. Romeo and Juliet are wed that morning and traverse the sea in their happiness. However, even the other sea and land animals strongly oppose their being together. A fish finds them a lovely couple, but warns them that they will be in big trouble if the Prince finds out. Back on the beach, Mercutio is telling many jokes, which leads to him making insulting jokes against the Capulets, and the Prince is headed in that direction. When he arrives, Mercutio mocks him as well. Romeo rushes to aid his friend, but after a struggle Mercutio falls off the cliff where Juliet met Romeo the previous evening, and everyone thinks that he is dead. The Prince, jealous of Juliets affection for Romeo, exiles Romeo to Shark Island. In despair, Juliet seeks the Friars help, and he gives her a potion to put her in a deathlike state. Mercutio is revealed to be alive and sees the whole thing, remarking, "What a tangled web we weave."

Lawrence shows the Capulet seals that Juliet is "dead", right as they were celebrating the marriage. But Benvolio sees her as well, and swims to Shark Island to tell Romeo. The Friar chases him to stop him, but is attacked by a shark. After receiving the terrible news, Romeo heads to the shore to see if Juliet is truly dead. Friar Lawrence arrives too late and tries to follow Romeo, only to have his tail maimed by the shark. After an undersea chase and some help from the fish Romeo and Juliet met earlier, Lawrence escapes and heads to the beach. A heartbroken Romeo walks past the mourning Capulets and tries to kiss Juliet, only to have some of the potion slip into his own mouth, putting him in a deathlike state as well. Both groups of seals begin to weep for their loss, and Lawrence, who has just arrived, teaches them a lesson about where hatred leads them. Suddenly, Romeo and Juliet awaken, and all is well. Mercutio returns, and the Prince finds a new mate, a large elephant seal like himself. The movie ends with the two families at peace, and Romeo and Juliet remaining together.

==Cast and characters== Montague and Juliets love interest. He knows he should not go near the Capulets, but he loves Juliet. It is revealed that he would not let anyone kidnap Juliet; this is first revealed in a shipwreck scene, when an octopus hypnotizes and tangles Juliet and challenges Romeo to a duel. In the English version he is voiced by Alan Ritchson Capulet and Romeos love interest.
*Chip Albers as Mercutio, Romeos trouble-making best friend.
*Michael Toland as Capulet, Juliets father. / Friar Lawrence: A sea otter and a good friend of Romeo, he knows that Romeo and Juliet shouldnt be married, but realizes that the wedding will stop the rivalry of the two families.
*Stephen Goldberg as Montague, Romeos father.
*Phil Nibbelink as the Prince, a huge northern elephant seal who has a big nose, rancid breath, and a quick temper.
*Chanelle Nibbelink as Kissy the Kissing Fish, a sweet and cute little fish who acts as a comic relief. She does not like the water much, but adores Romeo, and enjoys kissing him. 
*Sam Gold as Benvolio, a good friend of Romeo who is not a trouble-maker like Mercutio.

==Production==

===Background===
Nibbelink, a former   & Leif Ericson: The Boy Who Discovered America.    Nibbelink decided to make Romeo and Juliet in 2000,  when he was finishing Leif Ericson,  and began work on it in early 2003.  Nibbelink wanted the film to be a family-friendly version of Shakespeares original tale, because of the few appropriate family films available at the time. 

===Animation===
 
The film took 4½ years of animating and required 112,000 frames, each of which were completed in under 2 minutes  and drawn on a  .  Nibbelink used Flash 4 because when he tried to migrate to Flash 5, it created forward-compatibility problems. Even cut-and-paste work using Flash 4 and Flash 5 launched at the same time created RAM issues and crashed. 

===Audio=== Spanish dub was originally done in Madrid.  Nibbelink said his daughters voice-over was completely unscripted. "I would  take these silly improvs that my little daughter would do. I mean, lines like, she would say, ‘Babies – p-ew! I hate stinky babies!’ I said, ‘That’s hilarious!’ So I would use it." 

The film had no official composer. Nibbelink bought melodies from royalty-free music compositions, and wrote the lyrics for the music.  These royalty free music companies were Intents Creative Music, British Audio Publishing, Crank City Music, Jack Waldenmaier Music Productions in association with The Music Bakery, River City Sound Productions, Bejoodle Music, Fresh Music, and Music 2 Hues. Three songs are included in the film: a version of "Twinkle Twinkle Little Star", performed by Chanelle Nibbelink and arranged by Elva Nibbelink, "Bite My Tail", performed by Michael Toland and arranged by Nibbelink, and "Singing Starfish", performed by Jennifer, Russell, and Gigi Nibbelink. 

==Release==

===Theatrical release===
 
Nibbelink sold the film to 800 people, and it was picked up by distributors MarVista Entertainment for release in foreign territories and Indican Pictures for a domestic release. 
A preview of the film was screened at  .  . Indian Television. November 26, 2005. Accessed on July 27, 2013.  It was released as Fofita, una foquita la mar de salada to 32 theaters in Madrid and Seville on June 23, 2006.   

Sealed with a Kiss was rated " ,    grossed $80,938,  with an average of $4,220.  That same week, it grossed a total $109,720.  . Box Office Mojo. Accessed on August 3, 2012.  The film closed its box office run on July 19, 2007,  having grossed just $895 that same week.  Despite being a minor box office bomb, as of June 2013, the film is the third most profitable film released by Indican Pictures. 

===Home media===
The film was released to DVD on June 12, 2007. Animatedviews.coms Ben Simon, in his review of the DVD, gave it an overall 7 out of 10, praising the large bonus content for a low-budget film, but criticizing the glitchy sound and audio of the feature.  A giveaway was previously held in 2005 by Abbey Home Media and Courier Kids, and the prize was a DVD copy of the film and a toy seal. Ten copies were given.  . Halifax Courier. October 27, 2005. Accessed on July 27, 2013. 

==Critical reception==
The film was negatively received by critics. Review aggregation website  . Retrieved August 3, 2012.   . Accessed on May 25, 2013.  LA Weekly s Luke Y. Thompson said that the film "should find its primary audience among college potheads who like to watch 70s Hanna-Barbera creations on the Cartoon Network late at night." Y. Thompson, Luke.  . Retrieved August 24, 2013.  Common Sense Media writer Renee Schonfeld said in his 2012 review that it was "an amateurish effort with a grating villain, tired jokes, and sub-par music". He gave it 2 out of 5 stars. 

In contrast, Reel.coms Pam Grady gave the film a 2.5 out of 4, saying that, "surprisingly, its not terrible".  Animatedviews.coms Ben Simon defended the film, saying it was "a charming little movie" that "remains faithful to Shakespeares text." Simon, Ben (August 10, 2007).  . Animated Views. Accessed on May 26, 2013.  The film won two awards at the Southwest Film Festival, in the animation and Best in Show category.  

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 