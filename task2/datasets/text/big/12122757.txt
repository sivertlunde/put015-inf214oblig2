Monte Walsh (1970 film)
{{Infobox film
| name = Monte Walsh
| image = Monte Walsh VideoCover.jpeg
| image_size =
| alt =
| caption =
| director = William A. Fraker Bobby Roberts
| writer = David Zelag Goodman Lukas Heller
| narrator =
| starring = Lee Marvin Jack Palance
| music = John Barry
| cinematography = David M. Walsh
| editing = Richard K. Brockway Ray Daniels
| studio = Cinema Center Films
| distributor = National General Pictures
| released =  
| runtime = 106 min.
| country = United States English
| budget =
| gross =
}}
 Jack Schaefer. The movie has little to do with the plot of Schaefers book. It was directed in 1970 by cinematographer William A. Fraker in his directorial debut, and starred Lee Marvin, Jeanne Moreau and Jack Palance. The movie was set in Harmony, Arizona. A Monte Walsh (2003)|made-for-TV remake was set in Wyoming and directed by Simon Wincer, with Tom Selleck and Isabella Rossellini playing the parts of Monte and Martine. The story has elements of a tragedy.

==Plot== Jim Davis), where they meet an old friend, Shorty Austin (Mitchell Ryan), another ranch hand.
 prostitute and saloon girl Martine Bernard (Jeanne Moreau), who suffers from tuberculosis. Chet, meanwhile, has fallen in love with Mary Eagle (Allyn Ann McLerie), a widow who owns a hardware store. As barbed wire and railways steadily eliminate the need for the cowboy, Monte and his friends are left with fewer and fewer options. New work opportunities are available to them, but the freedom of the open prairie is what they long for. Shorty loses his job and gets involved in rustling and killing, gunning down a local lawman. Then Monte and Chet find that their lives on the range are inexorably redirected.

Chet marries Mary and goes to work in the store, telling Monte that their old way of life is simply disappearing. Caught up in the spirit of the moment, Monte asks Martine to marry him, and she accepts. Monte goes on a drinking binge and rides a wild horse through town. A rodeo owner, Colonel Wilson (Eric Christmas), sees him and offers him a job. Monte considers the high salary, but decides the work is too degrading and refuses. Eventually, they all must say goodbye to the lives they knew, and try to make a new start. When Shorty shoots and kills Chet while trying to rob the store, Monte, distraught after the death of his beloved Martine, goes after him.

Shorty arrives and Monte chases him. Shorty shoots Monte, but runs off when the shot only hits his arm. Monte then manages to slip around Shorty and shoots him. As Shorty is dying, Monte tells him that he rode the wild horse.

==Cast==
* Lee Marvin as Monte Walsh
* Jeanne Moreau as Martine Bernard
* Jack Palance as Chet Rollins
* Mitch Ryan as Shorty Austin (as Mitchell Ryan) Jim Davis as Cal Brennan
* G.D. Spradlin as Hal Henderson
* John Hudkins as Sonny Jacobs (as John Bear Hudkins)
* Raymond Guth as Sunfish Perkins (as Ray Guth) John McKee as Petey Williams
* Michael Conrad as Dally Johnson
* Tom Heaton as Sugar Wyman
* Ted Gehring as Skimpy Eagans
* Bo Hopkins as Jumpin Joe Joslin
* John McLiam as Fightin Joe Hooker
* Allyn Ann McLerie as Mary Eagle Matt Clark as Rufus Brady
* Charles Tyner as unknown
* Jack Colvin as unknown

==External links==
*  
*   for the 1970 motion picture
*  
*  
*   for the 2003 television version


 
English-language films
 
 
 
 
 
 
 
 