High Barbaree (film)
{{Infobox film
| name           = High Barbaree
| image_size     = 
| image	=	High Barbaree FilmPoster.jpeg
| caption        =  Jack Conway
| writer         = Anne Morrison Chapin  Whitfield Cook Cyril Hume
| based on       =  
| starring       = Van Johnson June Allyson
| music          = Albert Sendrey
| cinematography = Sidney Wagner	 	 
| editing        = Conrad A. Nervig	 	 
| studio         = Metro-Goldwyn-Mayer 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         = $2,173,000  . 
| gross          = $3,083,000 
}} Jack Conway. Pacific theater was merged into a search for a mystical "High Barbaree".  
 

==Plot== PBY Catalina Cameron Mitchell). Thomas Mitchell).

His uncle had once discovered High Barbaree, a mythical island that the crew members now head for, using a lashed-up sail made from parachutes to convert the flying boat into a sailboat, but as water supplies dwindle and both men begin to succumb to the conditions, Alec continues his story of a childhood where he had dreamed of becoming a doctor like his father (Henry Hull). After completing the first two years of medical training, he had chosen to become a pilot and rose to the ranks of vice-president of the Case Aviation company, even winning the hand of the bosss daughter (Marilyn Maxwell). When his childhood sweetheart comes back for a visit, a terrible tornado   destroys the company and town, and while his father suffers a broken arm, Alec takes over the medical care of the victims that are in the town auditorium.

The coming of war precludes Alec continuing his medical career, and as a pilot in the highly specialized  PBY flying boats that harass the Japanese fleets, he is particularly successful until his last mission. After Joe dies, and Alec goes into a coma, High Barbaree seems only a dream when rescue comes at the last moment, as his uncle steers to the location he had once charted and finds Alec. On board, Nancy and the recovering Alec are finally reunited.

==Cast==
* Van Johnson as Alec Brooke 
* June Allyson as Nancy Frazier Thomas Mitchell as Capt. Thad Vail
* Marilyn Maxwell as Diana Case Cameron Mitchell as Lt. Joe Moore
* Claude Jarman Jr. as Alec (Age 14)
* Henry Hull as Dr. William G. Brooke
* Geraldine Wall as Mrs. Martha Brooke
* Barbara Brown as Della Parkson
* Paul Harvey as John Case Charles Evans as Colonel Taylor
* Joan Wells as Nancy (Age 12)  
* Audrey Totter as The voice of "Tokyo Rose"

 

==Production== The Yearling (1946). Quin, Elanor.   Turner Classic Movies. Retrieved: September 16, 2012. 

The studio followed the plotline of the original novel which had a "Romeo and Juliet" ending with Allysons character dying, Johnson hearing that her ship had been sunk, and subsequently dying before he is rescued. When previewed in Los Angeles with this ending, 40% of the audience cards wanted a happy ending with Johnson not dying. A costly $50,000 remake had both of the screen lovers surviving. 
 North Island, Coronado Island, California with principal photography wrapped on August 14, 1946. 

The use of the Ryan Aircraft plant in San Diego as the site for the lead actors test flying adventures included a lively wringing out of the Stinson L-1 Vigilant  and Ryan ST trainer, flown by Paul Mantz. 

==Reception== Thomas Mitchell as an old seafaring uncle.  

The film was a hit, earning $2,231,000 in the US and Canada and $852,000 elsewhere, but because of its high cost recorded a loss to MGM of $149,000. 

On January 24, 1949, "Lux Radio Theater" broadcast a 60-minute radio adaptation of the movie with Van Johnson reprising his film role. 

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Adcock, Al. US Liaison Aircraft in action (Aircraft in Action: No. 195). Carrollton, Texas: Squadron/Signal Publications, 2005. ISBN 978-0897474870.
* Davis, Ronald. Van Johnson: MGMs Golden Boy. Jackson, Mississippi: University Press of Mississippi, 2001. ISBN 978-1-57806-377-2.
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies." The Making of the Great Aviation Films. General Aviation Series, Volume 2, 1989.
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 