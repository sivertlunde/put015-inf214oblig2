Scenes from a Mall
{{Infobox film
| name           = Scenes from a Mall
| image          = Scenes from a mall poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Paul Mazursky
| producer       = Paul Mazursky
| writer         = Roger L. Simon Paul Mazursky
| starring       = Bette Midler Woody Allen
| music          = Marc Shaiman Fred Murphy
| editing        = Stuart H. Pappé
| studio         = Touchstone Pictures Buena Vista Pictures
| released       =  
| runtime        = 89 min.
| country        = United States
| language       = English
| budget         = $3 million
| gross          = $9,563,393 (USA)
}}
 1991 film directed by Paul Mazursky with a screenplay by Roger L. Simon and Mazursky, starring Bette Midler and Woody Allen in one of his few films where he only acted, and didnt also direct and/or produce. In the film, Allens character, Nick, is married to author Deborah, played by Midler. After years of a happy marriage, Nick reveals to her that he has had an affair. Deborah is shocked and requests a divorce, but later admits that she herself has been unfaithful. The film was shot at the Stamford Town Center in Stamford, Connecticut and the Beverly Center in Los Angeles, California.

==Cast==
*Bette Midler &ndash; Deborah Fifer
*Woody Allen &ndash; Nick Fifer
*Bill Irwin &ndash; Mime
*Daren Firestone &ndash; Sam
*Rebecca Nickels &ndash; Jennifer
*Paul Mazursky &ndash; Doctor Hans Clava
*Marc Shaiman &ndash; Pianist
*Joan Delaney &ndash; Woman interviewer
*Fabio Lanzoni &ndash; Handsome Man
*Dealin LaValley &ndash; Waiter

==Reception==

The film received a mixed reception, and currently rates at 33% on Rotten Tomatoes. At the time of its release, film critics Siskel & Ebert gave the film "Two Thumbs Down".    

==Box office==

The film was not a box office success but did manage to bring back its budget. 

==References==

 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 

 