Katyń (film)
{{Infobox film
| name           = Katyń
| image          = katyn movie poster.jpg
| image_size     =
| caption        = Polish release poster
| director       = Andrzej Wajda
| producer       = Michał Kwieciński
| writer         = Andrzej Wajda Przemysław Nowakowski
| based on    =  
| narrator       =  Artur Żmijewski Paweł Małaszyński
| music          = Krzysztof Penderecki
| cinematography = Paweł Edelman
| editing        = Milenia Fiedler Rafał Listopad
| distributor    = ITI Cinema
| released       =  
| runtime        = 115 minutes
| country        = Poland
| language       = Polish Russian German
| budget         = 15,000,000 PLN €4,000,000
}} Best Foreign Language Film for the 80th Academy Awards. 

==Background==
 
  POW officers taken prisoner during the Soviet 1939 invasion of Poland, the rest being Poles arrested for allegedly being "intelligence agents, gendarmes, spies, saboteurs, landowners, factory owners, lawyers, priests, and officials."
 fall of communism in Poland in 1989, the first non-communist Polish government immediately acknowledged that the crime was committed by the Soviets. In 1990, Mikhail Gorbachev acknowledged Soviet responsibility for the Katyn massacre for the first time.    In 1991, Boris Yeltsin made public the documents which had authorised the massacre. 

There are now some cemeteries of Polish officers in the vicinity of the massacres, but many facts of the event remain undisclosed to this day and many graves of the Polish POWs east of the Bug River are either still unmarked or in a state of disrepair.

==Plot==
  used by the NKVD for transport of prisoners; the bus was reconstructed for the purpose of the film.]]
The events of Katyn are relayed through the eyes of the women, the mothers, wives, and daughters of the victims executed on Stalins orders by the NKVD in 1940.
 Artur Zmijewski) is a young Polish captain in an Uhlan (light cavalry) regiment who keeps a detailed diary. In September 1939, he is taken prisoner by the Soviet Army, which separates the officers from the enlisted men, who are allowed to return home, while the officers are held. His wife Anna (Maja Ostaszewska) and daughter Weronika, nicknamed "Nika" (Wiktoria Gąsiewska), find him shortly before he is deported to the USSR. Presented with an opportunity to escape, he refuses on the basis of his oath of loyalty to the Polish military.

Helped by a sympathetic Soviet officer, Anna manages to return to the familys home in Cracow with her daughter. There, the Germans carry out Sonderaktion Krakau, shutting down Jagiellonian University and deporting professors to concentration camps. Andrzejs father is one of the professors deported; later, his wife gets a message that he died in a camp in 1941.

In a prisoner of war camp, Andrzej is detained for a while and continues to keep a diary. He carefully records the names of all his fellow officers who are removed from the camp, and the dates on which they are taken. During the winter, Andrzej is clearly suffering in the low temperature, and his colleague Jerzy (Andrzej Chyra) lends him an extra sweater. As it happens, the sweater has Jerzys name written on it. Finally, Andrzejs is taken from the camp, while Jerzy is left behind.

In 1943, the population of Cracow is informed by the occupying power about the Katyn massacre. Capitalizing on the Soviet crime, the Nazi propaganda publishes lists with the names of the victims exhumed in mass graves behind the advancing German troops. Andrzejs name is not on the list, giving his wife and daughter hope.

After the war, Jerzy, who has survived, has enlisted in the Ludowe Wojsko Polskie|Peoples’ Army of Poland (LWP), which is under the complete control of the pro-Soviet Polish United Workers Party. He feels personal loyalty to his friends, loves his country, and has sympathy for those who have suffered. He visits Anna and her daughter to tell them that Andrzej is dead. Apparently, when the list of the names of the victims was compiled, Andrzej was misidentified as Jerzy on the basis of the name in the sweater that Jerzy had lent to Andrzej; it was Andrzej who was killed, not Jerzy. Despondent that he is now forced to acknowledge a lie and to serve those who killed his comrades in Katyn, Jerzy commits suicide.

Evidence of Soviet responsibility for the Katyn massacre is carefully concealed by the authorities. However, a few daring people working with the effects of the victims eventually deliver Andrzejs diary to his widow Anna. The diary clearly shows the date in 1940 when he must have been killed from the absence of entries on subsequent days. The date of the massacre is crucial for assigning responsibility: if it happened in 1940, the USSR controlled the territory, while by mid-1941 the Germans took control over it.

The film ends with a re-enactment of parts of the massacre, as several of the principal characters are executed along with other soldiers.

The film includes excerpts from German newsreels presenting the Katyn massacre as a Soviet crime, and excerpts from Soviet newsreels presenting the massacre as a German crime. Some documentary footage of the scene of the massacre is shown as well.

==Production== Soviet invasion of Poland in 1939.

==Cast==
 ]]
* Andrzej Chyra, as Jerzy – Porucznik (1st Lieutenant) in the 8th Uhlan Regiment Artur Żmijewski, as Andrzej – Rotmistrz (Captain) of 8th Uhlan Regiment
* Maja Ostaszewska, as Anna, wife of Andrzej
* Wiktoria Gąsiewska, as Weronika ("Nika"), daughter of Andrzej and Anna
* Władysław Kowalski (actor)|Władysław Kowalski, as Jan, father of Andrzej and a professor at the Jagiellonian University|Kraków University
* Maja Komorowska, as Maria, mother of Andrzej
* Jan Englert, as General
* Danuta Stenka, as Róża, wife of the General
* Sergei Garmash, as Captain Popov - a sympathetic and protective Red Army officer
* Agnieszka Kawiorska, as Ewa, daughter of General and Róża
* Stanisława Celińska, as Stasia – a servant in the Generals house
* Paweł Małaszyński, as Piotr – an Air Force officer
* Magdalena Cielecka, as sister of Piotr
* Agnieszka Glińska, as sister of Piotr
* Anna Radwan, as Elżbieta – a relative of Anna
* Antoni Pawlicki, as Tadeusz, son of Elżbieta
* Alicja Dąbrowska, as an actress
* Jakub Przebindowski, as priest Wikary
* Krzysztof Globisz, as a medical doctor
* Oleh Drach, as a commissar

==Controversy==

There has been controversy over the politics surrounding the film. According to Wajdas production notes, the film was made under the honorary patronage of President Lech Kaczyński, a conservative politician. There was some controversy in Poland over how the then Polish authorities tried to use the film during the election campaign. 

There have been accusations that the portrayal of Soviet characters is one-dimensional.  However, in an attempt to create a nuanced distinction between Soviets (as a political group) and Russians (an ethnic group), the film presents one positive Soviet character, Captain Popov. All German characters are portrayed negatively.
 Soviet responsibility NKVD responsibility documents to that effect. 

In April 2009, the authorities of the Peoples Republic of China banned the movie from being distributed in the country due to its anti-communist ideology. However, pirate copies are widely available. 

==See also==
*Katyn Massacre
*Poland–Russia relations
* 

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  
*  
*  
*   by Pablo Iglesias Turrión

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 