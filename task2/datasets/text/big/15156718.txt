The Mating Call
{{Infobox film
| name = The Mating Call
| director = James Cruze
| producer = Howard Hughes (uncredited) Walter Woods (adaptation) Herman J. Mankiewicz (titles)
| based on =  
| starring = Thomas Meighan Evelyn Brent Renée Adorée Alan Roscoe
| music = Frances Ring Martin Roones Robert Israel (2004)
| cinematography = Ira H. Morgan
| editing = Walter Woods (uncredited)
| distributor = Paramount Pictures
| released =  
| runtime = 72 minutes (restored version)
| country = United States
| budget=$400,000   
| language = Silent film English intertitles
}} The Racket nude scene in the film.

==Plot==
Leslie Hatton, a poor farmer, becomes a captain and a war hero in World War I. While on a leave, he secretly marries Rose, the "village belle", but he only has time for a few kisses and a hug before he has to return to the fighting. After the Armistice, Major Hatton comes home, only to be told by Marvin Swallow that his wifes parents have had their marriage Annulment|annulled, as she was not of age. Rose married wealthy Lon Henderson and the couple went abroad. Les returns to farming.

One day, the Hendersons return. Rose, disillusioned by Lons repeated infidelity, throws herself at Les. He weakens and embraces her, but then Lon shows up. The two men struggle when Lon pulls out a gun. Fortunately no one is hurt, and Les invents a French wife on her way to the farm so he will be left alone.

He goes to Ellis Island in search of a real wife. An official directs him to Catherine and her parents, poor would-be immigrants who are facing deportation. He offers to marry her in exchange for the family being allowed to settle in America. Her parents strongly oppose the bargain, but she accepts. That night, Catherine is prepared to share her bed with her husband, but sensing her resigned attitude, Les decides at the last minute to sleep alone in another room. They gradually fall in love.

Meanwhile, Lon decides to break off his affair with young Jessie Peebles. When Marvin asks her to marry him, she asks for a little time to consider. Les later finds her lifeless body in a pond on his farm. Lon, a member of the local Ku Klux Klan-like Order, insinuates that Les must have had something to do with Jessies suicide. Les is taken at gunpoint to face vigilante justice. The head of the Order sends for Lon, but decides in his absence that the evidence is overwhelming, and Les is tied up and whipped. The men sent to fetch Lon find him dead in his office and Marvin hiding with a gun. They take him back to the Order meeting. He denies having killed Lon and produces Lons love letters to Jessie, exonerating Les. The head of the Order rules that, even if Marvin did not kill Lon, he would have been justified to do so. One of his men stages it to look like suicide. (Judge Peebles, Jessies father, is shown at home, unloading and cleaning his gun. One cartridge has been discharged.)

==Cast==
*Thomas Meighan as Leslie Hatton
*Evelyn Brent as Rose Henderson
*Renée Adorée as Catherine
*Alan Roscoe as Lon Henderson
*Gardner James as Marvin Swallow Helen Foster as Jessie
*Luke Cosgrove as Judge Peebles
*Cyril Chadwick as Anderson
*Will R. Walling
*Delmer Daves

==Background==
Although the story takes place immediately after World War I (1918-1919), all of Evelyn Brents and Helen Fosters clothes are strictly in the 1928 short skirt mode, completely out of place in the time frame of the story. The film does reflect, however, some of the societal issues following the war. During the war, women had greater freedom regarding employment and their role in society, and there was pressure after the end of the war for them to return to their pre-war status. 

Adorée received positive reviews for her performance in The Mating Call, even though it differed little from the wide-eyed "Euro-damsels" that were her trademark.

This film, long thought to be lost film|lost, was discovered in the archives of Howard Hughes memorabilia by curators at the University of Nevada, Las Vegas.   

==References==
 

==External links==
* 
* 
* 
*  at Virtual History

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 