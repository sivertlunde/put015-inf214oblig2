The Wesley's Mysterious File
 
 
{{Infobox film
| name           = The Wesleys Mysterious File
| image          = WesleysMysteriousFiles.jpg
| caption        = Film poster
| director       = Andrew Lau
| producer       = Wong Jing
| screenplay     = Wong Jing Thirteen Chan
| story          = Ni Kuang
| starring       = Andy Lau   Rosamund Kwan   Shu Qi
| music          = Chan Kwong-Wing Ken Chan
| cinematography = Andrew Lau Ko Chiu-Lam Horace Wong
| editing        = Marco Mak
| studio         = Teamwork Motion Pictures
| distributor    = China Star Entertainment Group
| released       =  
| runtime        = 87 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$9,057,015
}} Hong Kong action science fiction film directed by Andrew Lau starring Andy Lau, Rosamund Kwan and Shu Qi.  Hong Kong director Wong Jing also makes a cameo appearance.

==Summary==
The film tells the story of Wesley (Andy Lau) who works in San Francisco for a UN department investigating extra terrestrial sightings. When Fong (Rosamund Kwan) an alien from the Dark Blue Planet arrives in town seeking the remains of her brother, Wesley; Fong and two agents from a secret FBI alien department (Shu Qi and Roy Cheung) become entangled in a conspiracy involving the government departments and two different alien species.

==Cast==
*Andy Lau &ndash; Wesley
*Rosamund Kwan &ndash; Fong Tin Ai
*Shu Qi &ndash; Pak Sue
*Wong Jing &ndash; Dr. Kwok
*Mark Cheng &ndash; Kill
*Almen Wong &ndash; Rape
*Roy Cheung &ndash; Pak Kei Wai
*Samuel Pang &ndash; Tan
*Patrick Lung &ndash; Kill
*Yo Yo Fong &ndash; Ling Ling
*Thomas Hudak &ndash; Wilson
*Beverly Hotsprings
*Tré Shine 
*Vincent Zhao

 {{Cite web |url=http://www.imdb.com/title/tt0324518/ |title=The Wesleys Mysterious File 
 |accessdate=2 July 2010 |publisher=imdb.com}} 
   

==See also==
* Wisely Series, the novel series by Ni Kuang
* Films and television series adapted from the Wisely Series:
** The Seventh Curse, a 1986 Hong Kong film starring Chow Yun-fat as Wisely
** The Legend of Wisely, a 1987 Hong Kong film starring Sam Hui as Wisely
** The Cat (1992 film)|The Cat (1992 film), a 1998 Hong Kong film starring Waise Lee as Wisely
** The New Adventures of Wisely, a 1998 Singaporean television series starring Michael Tao as Wisely
** The W Files, a 2003 Hong Kong television series starring Gallen Lo as Wisely

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 