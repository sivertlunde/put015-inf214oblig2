Oh, Saigon
{{Infobox film
| name           = Oh, Saigon
| image          =
| image_size     = 
| caption        =  
| director       = Doan Hoang
| producer       = Doan Hoang 
| writer         = Doan Hoang Bret Sigler
| narrator       = 
| starring       = Nam Hoang  Anne Hoang  Van Tran  Doan Hoang  Hoàng Duc  Hoang Dzung  Nhat Hoang Dylan Le
| music          = Juan P. Buccella Malcolm Cross
| cinematography = Ham Tran Lara Frankena  Tim Furnish Doan Hoang
| editing        = Bret Sigler
| distributor    = 
| released       = 2007
| runtime        = 57 mins
| country        = United States, Vietnam
| language       = English, Vietnamese
| budget         = 
| preceded_by    = 
| followed_by    =  website =  
}}
 documentary by Vietnamese American director Doan Hoang about her familys separation during the fall of Saigon and her attempt to reunite them afterwards.  Oh, Saigon was executive produced by Academy Award and Emmy winner, John Battsek. Oh, Saigon received film grants from the Sundance Institute Documentary Fund, ITVS, and the Center for Asian American Media, and after its release, received a number of film festival awards and accolades.

==Plot==
 

==Development== Independent Television Service (ITVS),  the Center for Asian American Media,  and the Corporation for Public Broadcasting.   

Filming was done in the United States and Saigon. According to the official website: "The subjects are shot on location in the expanse of America and its suburbs, as well as Saigon’s vibrant, noisy streets, and the rarely-seen breathtaking backwaters of Vietnam – emphasizing the physical differences between two countries that shared a war. Archival footage, moody Super8mm landscapes, and motion-graphics-animated family photographs juxtaposed to clear, colorful DV, shot in a fluid cinema verité–style highlight changes and similarities between past and present." 

==Release==
 
Hoang premiered Oh, Saigon in March 2007 at the San Francisco International Asian American Film Festival  She then showcased the film at various film festivals, universities, and museum venues.

Hoang took the film to 16 countries, including a tour of Spain in 2011 and 2012 tour of Vietnam for the US State Department and American Documentary Showcase.

The film is currently available to view on Netflix  and Amazon.com. 

==Cast==
The main characters in the film are the Hoang family: 
* Nam Hoang as Nam - a South Vietnamese pilot who pulls his family out of Vietnam to settle in Kentucky
* Doan Hoang as Doan - Nams daughter and the films narrator.
* Hoang Hai as Hai - a Communist soldier who is Nams older brother. 
* Hoang Dzung as Dzung - Nams younger brother. He is a fisherman.
* Anne Hoang as Anne - Nams wife. She was a socialite in Saigon, but after the relocation, she works as a seamstress.
* Van Tran as Van - Annes daughter and Doans secret half sister. On the day of the airlift, she is left behind.

Also includes the following family members: 
* Nhat Hoang
* Dylan Tran Le

==Reception==
 

==Awards and nominations==
 
*Grand Jury Prize for Non-Fiction Feature Film – Los Angeles Asian Pacific Film Festival, May 2008
*Best Documentary Award -  42nd Brooklyn Arts Council International Film Festival, May 2008
*Best Brooklyn Film -  42nd Brooklyn Arts Council International Film Festival, May 2008
*Best of the Fest – Austin Film Festival, February 2008
*Best Documentary Nominee - San Francisco International Asian American Film Festival, March 2007
*Grand Jury Prize Nominee – Vietnamese International Film Festival, April 2009  

==References==
 

==External links==
*   
*  

 
 
 
 
 
 
 
 
 
 