Dear Father (film)
 
{{Infobox film
| name           = Dear Father
| image          = Hjboc9y5.jpg
| caption        = Film poster
| director       = Dino Risi
| producer       = Pio Angeletti
| writer         = Bernardino Zapponi Marco Risi Dino Risi Simon Mizrahi
| starring       = Vittorio Gassman
| music          = Manuel De Sica
| cinematography = Tonino Delli Colli
| editing        = Alberto Gallitti
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
Dear Father ( ) is a 1979 Italian drama film directed by Dino Risi. It was entered into the 1979 Cannes Film Festival, where Stefano Madia won the award for Best Supporting Actor.   

==Cast==
* Vittorio Gassman - Albino Millozza
* Andrée Lachapelle - Giulia Millozza
* Aurore Clément - Margot
* Stefano Madia - Marco Millozza
* Julien Guiomar - Parrella
* Joanne Côté - Laura
* Antonio Maimone - Enrico
* Andrew Lord Miller - James
* Piero Del Papa - Duilio
* Mario Verdon - Ugo
* Don Arrès - Marco
* Gérard Arthur - Rodolfo
* Sergio Ciulli - Gianni
* Clara Colosimo - Myrta
* Nguyen Duong Don - Pierre

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 