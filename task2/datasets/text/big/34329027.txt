Home Before Dark (film)
{{Infobox film
| name           = Home Before Dark
| image          = 
| image_size     = 
| caption        = 
| director       = Mervyn LeRoy
| producer       = Mervyn LeRoy
| writer         = Eileen Bassing Robert Bassing
| based on       =  
| narrator       =
| starring       = Jean Simmons Dan OHerlihy Rhonda Fleming Efrem Zimbalist Jr.
| music          = Franz Waxman
| cinematography = Joseph F. Biroc Philip W. Anderson
| distributor    = Warner Bros. Pictures
| released       = November 16, 1958
| runtime        = 136 min
| country        = United States English
| budget         = $1.39 million 
| gross          =
}}
 
Home Before Dark is a 1958   believed it contained perhaps her finest performance. 

==Story==
Portrait of a mind and marriage at a crisis point: Charlotte Bronn (Jean Simmons) leaves the Maraneck State Hospital mental institution, after a year, to resume her life in a chilly relationship with her distant and emotionally repressed professor husband (Dan OHerlihy), in her home, where they all live with Charlottes step-sister Joan (Rhonda Fleming) and Joans mother, the scattered chatterer Inez (Mabel Albertson). Arnold avoids Charlotte, sleeping on the couch instead of in their bed, and avoiding her company with a multitude of excuses. Part of Charlottes commitment to the mental facility hinged on her continuing suspicions of an attraction between her husband and her beautiful half-sister. On the night of a dinner with important faculty, dressed and coiffed to resemble Joan, she appears to be headed toward another breakdown, but finally hears the truth about his feelings from her husband. In an anti-Semitic subplot, a Jewish philosophy professor, Jake Diamond (Efrem Zimbalist, Jr.), a boarder in their home, offers his compassionate support.  But there seems to be little he can do.

==Cast==
* Jean Simmons - Charlotte Bronn
* Dan OHerlihy - Arnold Bronn
* Rhonda Fleming - Joan Carlisle
* Efrem Zimbalist, Jr. - Jacob "Jake" Diamond
* Mabel Albertson - Inez Winthrop Steve Dunne - Hamilton Gregory
* Joanna Barnes - Frances Barrett
* Joan Weldon - Cathy Bergner
* Kathryn Card - Mattie
* Marjorie Bennett - Hazel Evans
* Eleanor Audley - Mrs. Hathaway
* Johnstone White - Malcolm Southey

==DVD==
Home Before Dark was released to DVD by Warner Home Video on July 8th, 2011 via its Warner Archive MOD DVD service.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 