Suddenly (1954 film)
{{Infobox film
| name           = Suddenly 
| image          = Suddenly (1954 movie poster).jpg
| image_size     =
| alt            =
| caption        = Theater release poster Lewis Allen
| producer       = Robert Bassler Richard Sale
| narrator       =
| starring       = Frank Sinatra Sterling Hayden James Gleason Nancy Gates Kim Charney Paul Frees
| music          = David Raksin
| cinematography = Charles G. Clarke
| editing        = John F. Schreyer
| distributor    = United Artists
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
  Lewis Allen Richard Sale.  The drama features Frank Sinatra, Sterling Hayden, James Gleason and Nancy Gates, among others. 

The tranquility of a small town is jarred when the U.S. President is scheduled to pass through and a hired assassin takes over the Benson home as a perfect location to ambush the president.
 films in the public domain.

==Plot==
In post-war America, the President of the United States is scheduled to journey through the small town of Suddenly, California. Claiming to be checking up on security prior to his arrival, a group of FBI agents arrive at the home of the Bensons, on top of a hill that looks down upon the station where the Presidential train is due to stop. However, they soon turn out to be assassins led by the ruthless John Baron (Frank Sinatra), who take over the house and hold the family hostage.
 Secret Service agent in charge of the Presidents security detail. When he does, Baron and his gangsters shoot Carney and a bullet fractures Shaws arm.

Baron sends one of his two henchmen to double-check on the Presidents schedule but he is killed in a shootout with the police. Jud (James OHara), a television repairman, shows up at the house and also becomes a hostage. Pidge (Kim Charney) goes to his grandfathers dresser to fetch some medication and notices a fully loaded revolver which he replaces with his toy cap gun.

Baron is confronted by the sheriff on the risks and meaning of killing the President and Barons remaining henchman begins showing some reluctance. For Baron, however, these are the very least of his concerns and it soon becomes clear that he is a psychopath whose pleasure comes from killing who and why he kills being of little importance to him.

A snipers rifle has been mounted on a metal table by a window. Jud discreetly hooks the table up to the 5000 volt plate output of the family television. Pop Benson (James Gleason) then spills a cup of water on the floor beneath the table. Although the hope is that Baron will be shocked to death, his remaining henchman touches the table first and is electrocuted, firing the rifle repeatedly and attracting the attention of police at the train station as he struggles to free himself. Baron shoots Jud, disconnects the electrical hookup and aims the rifle as the presidents train arrives at the station, but to his surprise, doesnt stop (the stop having been canceled). Ellen Benson (Nancy Gates) shoots Baron in the chest and Shaw shoots him again. Barons last words are, "Dont... please."

==Cast==
 
* Frank Sinatra as John Baron
* Sterling Hayden as Sheriff Tod Shaw
* James Gleason  as Peter "Pop" Benson
* Nancy Gates as Ellen Benson
* Kim Charney as Peter Benson III (Pidge)
* Paul Frees as Benny Conklin, Barons Accomplice (also TV announcer voice)
* Christopher Dark as Bart Wheeler
* Willis Bouchey as Dan Carney, Chief Secret Service Agent Paul Wexler as Deputy Slim Adams
* James OHara as Jud Kelly
* Kem Dibbs as Wilson
* Clark Howat as Haggerty
* Charles Smith as Bebop Dan White as Desk Officer Burg
 

==Reception==

===Critical response===
  New York Times film critic Bosley Crowther, liked the direction of the film and the acting, writing, "Yet such is the role that Mr. Sinatra plays in Suddenly!, a taut little melodrama that...   shapes up as one of the slickest recent items in the minor movie league... we have several people to thank- particularly Richard Sale for a good script, which tells a straight story credibly, Mr. Allen for direction that makes both excitement and sense, Mr. Bassler for a production that gets the feel of a small town and the cast which includes Sterling Hayden, James Gleason and Nancy Gates." Crowther especially liked Sinatras performance. He wrote, "Mr. Sinatra deserves a special chunk of praise...In Suddenly! he proves it in a melodramatic tour de force." 

The staff at Variety (magazine)|Variety magazine also gave the film a good review and praised the acting. They wrote, "Thesp   inserts plenty of menace into a psycho character, never too heavily done, and gets good backing from his costar, Sterling Hayden, as sheriff, in a less showy role but just as authoritatively handled. Lewis Allens direction manages a smart piece where static treatment easily could have prevailed." 
 The Desperate Hours (1955). 

==History==

===Influence===
 
In 1959, five years after the release of Suddenly, a novel was published which had a remarkably similar ending. This was The Manchurian Candidate written by Richard Condon, a former Hollywood press agent recently turned novelist. His book also features a mentally troubled former war hero who, at the climax, uses a rifle with scope to shoot at a presidential candidate.
 a 1962 film, again starring Sinatra, but this time out to prevent an assassination being committed by Laurence Harvey.

A remake of Suddenly, starring Dominic Purcell and directed by Uwe Boll, was released in 2013. 

===Loss of copyright===
The films copyright was not renewed and it fell into the public domain; it can be downloaded and viewed for free online. 

The film is widely available from a number of discount/public domain distributors.  colorized for home video by Hal Roach Studios in 1986, rendering Sinatras blue eyes brown.  A remastered colorized version by Legend Films was released to DVD on June 16, 2009, which also includes a newly restored print of the original black & white film. 

==See also==
* Assassinations in fiction
* List of films featuring home invasions

==References==

===Notes===
 

===Bibliography===
 
* Selby, Spencer. Dark City: The Film Noir. Jefferson, North Carolina: McFarland Publishing, 1984. ISBN 0-89950-103-6.
*  , 3rd edition, 1992. ISBN 0-87951-479-5.
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 