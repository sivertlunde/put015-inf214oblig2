Cyborg (film)
 
{{Infobox film
| name           = Cyborg
| image          = Cyborgposter.jpg
| alt            = 
| caption        = Theatrical release poster 
| director       = Albert Pyun
| producer       = {{plainlist|
* Menahem Golan
* Yoram Globus
}}
| writer         = {{plainlist|
* Kitty Chalmers
* Daniel Hubbard-Smith
}}
| starring       = {{plainlist|
* Jean-Claude Van Damme
* Deborah Richter
* Vincent Klyn
* Dayle Haddon
}}
| music          = {{plainlist|
* Lalo Schifrin
* Kevin Bassinson
}}
| cinematography = Philip Alan Waters
| editing        = {{plainlist|
* Scott Stevenson
* Rosanne Zingale
}}
| studio         = The Cannon Group
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 82 minutes 
| country        = United States
| language       = English
| budget         = $500,000  
| gross          = $10.2 million 
}}
Cyborg, known in the UK as Cyborg 009,    is a 1989 American Martial arts|martial-arts science fiction film directed by Albert Pyun. Jean-Claude Van Damme stars as Gibson Rickenbacker, a mercenary who battles a group of murderous marauders led by Fender Tremolo (Vincent Klyn) along the East coast of the United States in a apocalyptic and post-apocalyptic fiction|post-apocalyptic future. The film was the first in Albert Pyuns Cyborg Trilogy. It was followed by 1993s Knights (originally entitled "The Kingdom of Metal: Cyborg Killer") and finally 1997s Omega Doom.

==Plot== plague known CDC — work on a cure to save what remains of humanity. To complete their work they need information stored on a computer system in New York City. Pearl Prophet volunteers for the dangerous courier mission and is made into a cyborg through surgical augmentation.

Pearl, accompanied by bodyguard Marshall Strat, retrieves the data in New York but is pursued by the vicious Fender Tremolo and his gang of pirates. Fender wants the cure so he can have a monopoly on its production. Strat, badly injured while fighting the pirates, tells Pearl to leave him and find a mercenary, known as a "slinger", who can escort her to safety. She gets cornered but is saved by a slinger named Gibson Rickenbacker. After she explains her situation,  they are overrun by Fenders gang, and Gibson is knocked out by falling debris. Fender demands that she accompany him to Atlanta or die.

Fenders gang slaughters a family and steals their boat. They head south for Atlanta via the Intracoastal Waterway with the captive Pearl. Gibson, who had been tracking the pirates, arrives at the scene of slaughter later that night. A shadowy figure attacks him, but he disables her. She turns out to be Nady Simmons, a young woman who mistook him as a pirate. Nady, whose family was wiped out by the plague, join Gibson. Gibson is less concerned with a cure for the plague than with killing Fender. Gibson and Nady trek southward through the wastelands, where bandits ambush them. Concerned for Nady, Gibson unsuccessfully attempts to convince her to stay away. After declining sex with Nady, Gibson reveals that all he cares about is revenge against Fender, who killed his lover.

Intercepting Fender and his crew near Charleston, South Carolina, Gibson defeats most of his men, but Fender shoots him with an air rifle. Now nursing a gunshot wound, Gibson realizes Haley is now a loyal member of Fender’s crew. He flees the pirates and ends up alone with Pearl and Nady. Pearl refuses to go with him — she calculates that Gibson is not strong enough to defeat Fender and will be unable to get her to Atlanta safely. She says she will go along with Fender and lure him to his death in Atlanta, where she has resources at her disposal. 

Tired, wounded, and badly outnumbered, Gibson flees with Nady through the sewer into a salt marsh, where they are pursued by the rest of the pirates and eventually separated from each other. Gibson is thoroughly beaten by Fender and crucified high on the mast of a beached, derelict ship. Haley lingers at the scene but leaves with Fender. Gibson spends the night on the cross. In the morning, near death, he kicks the mast repeatedly with his dangling foot in a last fit of rage. The mast snaps, sending him crashing to the ground, his arms still tied and nailed to the cross. Finally, Nady appears out of the marsh to free him.

Gibson and Nady intercept Fender once again in Atlanta, this time better prepared. Fender’s gang is taken down one by one until he and Gibson face off. During their fight, Nady rushes Fender with a knife, but he stabs and kills her. Gibson in turn stabs Fender in the chest. Thinking him dead, Gibson embraces Haley. However, Fender gets back up, and they continue to battle in a nearby shed, where Gibson finally kills Fender by impaling him on a meat hook. Gibson and Haley escort Pearl to her final destination, before heading back off.

==Cast==
* Jean-Claude Van Damme as Gibson Rickenbacker
* Deborah Richter as Nady Simmons
* Vincent Klyn as Fender Tremolo
* Dayle Haddon as Pearl Prophet
* Alex Daniels as Marshall Strat
* Blaise Loong as Furman Vux / Pirate / Bandit
* Ralf Möller as Brick Bardo (as Rolf Muller)
* Haley Peterson as Haley
* Terrie Batson as Mary
* Jackson Rock Pinckney as Tytus / Pirate

==Production== Masters of the Universe and a live-action version of Spider-Man. Both projects were planned to shoot simultaneously by Albert Pyun.  Cannon, however, was in financial trouble and had to cancel deals with both Mattel and Marvel Entertainment, the owners of He-Man and Spider-Man, respectively. Cannon had already spent $2 million on costumes and sets for both films, and decided to start a new project to recoup the money spent on them. Then Pyun wrote the storyline for Cyborg in one weekend. Pyun had Chuck Norris in mind for the lead, but co-producer Menahem Golan cast Jean-Claude Van Damme. The film was shot for less than $500,000 and was filmed in 23 days.  The film was entirely shot in Wilmington, North Carolina.

Several of the characters names are references to well-known manufacturers and models of guitars and other musical instruments.
:*Gibson Rickenbacker: Gibson Guitar Corporation|Gibson, Rickenbacker
:*Fender Tremolo: Fender Musical Instruments Corporation|Fender, Tremolo arm
:*Marshall Strat: Marshall Amplifiers, Fender Stratocaster
:*Les: Gibson Les Paul Prophet 5 synthesizer
:*Nady Simmons: Nady Sound Systems, Simmons electric drums

Jackson "Rock" Pinckney, who played one of Fenders pirates, lost his eye during filming when Jean-Claude Van Damme accidentally struck his eye with a prop knife. Pinckney sued Van Damme in a North Carolina court and was awarded $485,000.  

Violent scenes were heavily cut to gain an "R" rating rather than an "X", including a throat-slitting and some blood and gore during the village massacre. Also excised was the death of a man Van Damme was fighting, which caused an inconsistency that made him look like he suddenly disappeared.  

==Reception==
Cyborg received a generally negative reception from critics despite the box office success.     Review aggregator Rotten Tomatoes reports a 14% positive score, based on 14 reviews, certifying it "Rotten," with an average rating of 2.9/10.  The film debuted at number four at the American box office  and went on to gross $10,166,459. 

==Legacy==
===Sequels===
 , a direct-to-video release, followed in 1995. Both films bear little to no relation to the first film and were heavily panned by critics, even more than the original. 

===Alternate cut===
In 2011, director Albert Pyuns Curnan Pictures got hold of the missing tapes of the original cut of Cyborg through Pyuns original choice for score artist, Tony Riparetti. This directors cut of the film features Pyuns editing and previously unreleased scenes.  It is commercially available through the director himself.  Pyuns directors cut was released in 2014 in Germany with the films original title "Slinger".

===In popular culture===
American rapper  . The lyrics are slightly modified. The intro is also in the opening of the song "World Damnation" by the death metal band Mortician (band)|Mortician. The intro of Fender talking about death and starvation is thought as the official opening of metal band Chimairas song "Resurrection." It is often played at live shows as an intro. The same intro is also played the beginning of a song by Australian, Christian, gore-grindcore band Vomitorial Corpulence.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 