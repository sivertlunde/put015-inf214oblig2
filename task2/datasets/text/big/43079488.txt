Diary of a Chambermaid (2015 film)
{{Infobox film
| name           = Diary of a Chambermaid
| image          = Journal dune femme de chambre poster.jpg
| caption        = Film poster
| director       = Benoît Jacquot
| producer       = Jean-Pierre Guérin Kristina Larsen Delphine Tomson Luc Dardenne Jean-Pierre Dardenne
| screenplay     = Benoît Jacquot Hélène Zimmer
| based on       =  
| starring       = Léa Seydoux Vincent Lindon
| music          = Bruno Coulais
| cinematography = Romain Winding
| editing        = Julia Grégory
| studio         = Les Films du Lendemain JPG Films Les Films du Fleuve
| distributor    = Mars Distribution
| released       =  
| runtime        = 95 minutes 
| country        = France Belgium   
| language       = French
| budget         = €7.47 million     
| gross          = $1,776,079  
}}
 novel of the same name and stars Léa Seydoux as Célestine (Mirbeau)|Célestine, a young and ambitious woman who works as a chambermaid for a wealthy couple in France during the early twentieth century. 

It was screened in the main competition section of the 65th Berlin International Film Festival    and was released on 1 April 2015, by Mars Distribution.    

== Plot ==
Early twentieth century, in the provinces. Célestine, a young chambermaid who is much courted for her beauty, has just arrived from Paris to work for the Lanlaire family. Fending off her masters advances, Célestine also has to cope with the strict Madame Lanlaire who rules the household with an iron fist. There, she meets Joseph, a mysterious gardener, for whom she feels a fascination. 

== Cast ==
* Léa Seydoux as Célestine (Mirbeau)|Célestine
* Vincent Lindon as Joseph
* Clotilde Mollet as Madame Lanlaire
* Hervé Pierre as Monsieur Lanlaire
* Mélodie Valemberg as Marianne
* Patrick dAssumçao as Captain Mauger
* Vincent Lacoste as Georges
* Joséphine Derenne as Madame Mendelssohn
* Dominique Reymond as the recruiter  Rosette as Rose
* Adriana Asti as the madam
* Aurélia Petit as the mistress

== Production ==

===Development=== The Diary of a Chambermaid. Producer Kristina Larsen stated that "Jacquot’s version will be the most faithful adaptation of Mirbeau’s novel".  In February 2013, Marion Cotillard was in talks to play the central character Célestine, but later dropped out of the film over scheduling conflicts with Macbeth (2015 film)|Macbeth.   On 5 February 2014, director Benoît Jacquot confirmed in an interview Diary of a Chambermaid will begin shooting in the forthcoming summer, with Léa Seydoux and Vincent Lindon joining the cast of the film.  On 10 April, Cineuropa reported that the Île-de-France Region’s Support Fund for the Film and Audiovisual Technical Industry added €440,000 to the films funding. 

===Filming===
Principal photography commenced on 10 June 2014 and concluded on 30 July.  Filming took place in northern France (the heritage railway Chemin de Fer de la Baie de Somme, Le Crotoy, and Berck) and also at locations in and around Paris.     

==Release==
On 14 January 2015, it was announced that Diary of a Chambermaid had been selected to be screened in competition at the 65th Berlin International Film Festival.  

The film was released on 1 April 2015 in France. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 