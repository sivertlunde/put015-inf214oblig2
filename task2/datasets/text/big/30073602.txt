Secret of the Sultan
{{Infobox film
| name           = Secret of the Sultan
| image          = SecretoftheSultanTheatricalPoster.jpg
| alt            =
| caption        = Secret of the Sultan Theatrical Poster
| director       = Hakan Şahin
| producer       = Ömer Erbil
| writer         = Ömer Erbil
| starring       =   
| music          = Engin Arslan
| cinematography = Tolga Kutlar
| editing        = 
| studio         = Filmografi
| distributor    = 
| released       =  
| runtime        =
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
}} National Treasure" movies and Dan Browns "The Da Vinci Code" and "Angels & Demons"      

==Plot==
An American professor travels to Istanbul to find a mysterious century-old chest built by Sultan Abdülhamid II, which he believes is relevant to the present political power games over oil. His search leads him discover long-forgotten underground cloisters at Topkapı Palace, now the Topkapı Museum, where he confronts a patriotic museum curator and the Turkish intelligence. Conspiracies going back to the Gulf Wars, the Iran-Iraq War, World War I, and as far as the final years of the Ottoman Empire are revealed in the course of his investigation.

==Production==
The film was shot on location in Istanbul, Turkey, at such historic landmarks as Topkapi Palace, Hagia Sophia, Yildiz Palace and the Istanbul Archaeology Museums. 

==Release==
===Premiere===
The film premiered at a gala screening, attended by star Mark Dacascos, at Istinye Park, Istanbul, on  . 

==References==
 

==External links==
*    
*  

 
 
 
 
 
 
 