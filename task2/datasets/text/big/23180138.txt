Hyde Park Corner (film)
{{Infobox film
| name           = Hyde Park Corner
| image          =
| alt            =
| caption        =
| director       = Sinclair Hill
| producer       = Harcourt Templeman
| writer         = D.B. Wyndham-Lewis    Selwyn Jepson   Walter C. Hackett (play)
| starring       = Gordon Harker   Binnie Hale   Eric Portman   Gibb McLaughlin
| music          = Louis Levy
| cinematography = Cyril Bristow
| editing        = Michael Hankinson
| studio         = Grosvenor Films
| distributor    = Pathé Pictures International
| released       = 1935
| runtime        = 85&nbsp;min
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} comedy crime film directed by Sinclair Hill and starring Gordon Harker, Binnie Hale and Eric Portman. Harker portrays a policeman investigating a crime in 1930s London, which proves to have its origins in the 1780s.  The film takes its name from Hyde Park Corner in Central London where the events of the film occur. It was based on a play by Walter C. Hackett. The film was made at Welwyn Studios.

==Synopsis==
In the 1780s, after an evening of illegal gambling two of the participants fight a duel in which the wronged party is killed by the villain who has just cheated to win the newly built house at Hyde Park Corner from him. Officer Cheatle of the Bow Street Runners is able to arrest those present for gambling, but is unable to prove that a murder has occurred.

A hundred and fifty years later Constable Cheatle, his great grandson, is intrigued by reports of another murder at the same house in Hyde Park Corner. Cheatle sees this as a way of fulfilling his ambition to join the plainclothes detective branch. His attempts to solve the case are initially interrupted by Sophie, a petty criminal who he arrests while she is shoplifting in a department store. Eventually, with her help, he is able to uncover the true culprit of the crime which has its roots in the fatal evening in the eighteenth century.

==Cast==
* Gordon Harker - Constable Cheatle
* Binnie Hale - Sophie
* Eric Portman - Edward Chester
* Gibb McLaughlin - Sir Arthur Gannett
* Harry Tate - Taxi Driver
* Robert Holmes - Cncannon
* Eileen Peel - Barbara Ainsworth
* Donald Wolfit - Howard

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 