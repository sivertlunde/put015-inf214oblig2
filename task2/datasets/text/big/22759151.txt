Apogee of Fear
 
{{Infobox film
| name = Apogee of Fear
| image =
| caption  = film poster
| director = Richard Garriott
| producer = Tracy Hickman
| writer = Tracy Hickman
| starring = Yuri Lonchakov Michael Fincke Greg Chamitoff Richard Garriott
| music = Digital Juice (original version)
| cinematography = Richard Garriott
| editing = Tracy Hickman
| distributor    =
| released =
| runtime = 8 minutes
| country = United States
| language = English
| budget = 
}}
Apogee of Fear is the first science fiction film made in space.  Filmed by Richard Garriott from a script and production elements he contracted from fantasy novelist Tracy Hickman, the films principal photography was accomplished during Garriotts time aboard the International Space Station as a spaceflight participant on October 12, 2008.

== Plot == Sergey Volkov, Yuri Lonchakov, Oleg Kononenko, and astronauts Michael Fincke and Greg Chamitoff as the stars of the never-to-be-made film. Richard Garriott is billed as The Gunfight Participant – a satirical nod to his official status as a Spaceflight Participant. These six were all the crew aboard the International Space Station during Garriotts time aboard.  After this preview, the film proper begins.

A series of motion graphics under the credits leads us to a Soyuz spacecraft departing the International Space Station, bearing Garriott, Volkov, and Kononenko back to earth. Finke and Chamitoff wave goodbye but express their relief at Garriotts leaving as he was annoying them with his constant talk of computer games. Lonchakov insists that they all go back to work.
 juggle without Garriott, and Fincke knew Garriott was good at settling arguments about which of them was standing upside down.  At this point Lonchakov points out that the oxygen use aboard the station is too high. Everyone theorizes wildly about the cause which universally involve interstellar aliens invading the station. A search of ridiculous locations on the station ensues during which the surprising nature of the alien is discovered.
 The Day the Earth Stood Still, Forbidden Planet and Galaxy Quest.

== Cast ==
* Yuri Lonchakov as Himself
* Michael Fincke as Himself
* Greg Chamitoff as Himself
* Richard Garriott as "The Spaceflight Participant"
* Helen Garriott as "The Spaceflight Participants Mother"

== Pre-production ==
 Las Vegas.
 PowerPoint presentation ISS for the crew during filming.

== Production ==
Garriott, as a private space explorer, flew into earth orbit aboard Soyuz TMA-13 on October 12, 2008, carrying with him a project code named Icarus: a project to create the first science-fiction movie ever actually filmed in space.

The original script was written for the equal participation of all six people aboard. When Garriott presented the idea to the cosmonauts and astronauts aboard, all were enthusiastic about the project but ultimately Volkov and Kononenko declined to participate for personal reasons although they still supported the project. This required Garriott to restructure the shooting of the script and the script itself while on orbit.

Scenes were filmed in sequence during the crews personal time so as not to impact their work schedule in any way. The scenes were filmed over several days and thereby only took a few minutes to set up and shoot sequences on any given day.

== Post-production ==

Upon Garriotts return to his quarters in   format at 1080i HD resolution. This was subsequently rendered down to anamorphic widescreen NTSC standard.

== Release ==
The film is available as a special feature on the DVD Man on a Mission, Mike Woolfs documentary about Richard Garriotts trip into space aboard a Russian Soyuz rocket. The DVD is available from First Run Features.

== References ==
NEWS: http://www.slashgear.com/nasa-decides-to-let-richard-garriotts-apogee-of-fear-film-air-20210192 NASA decides to let Richard Garriott’s Apogee of Fear film air.

 

 
 
 
 
 