Madonna (2015 film)
 
{{Infobox film
| name           = Madonna
| image          = 
| caption        = 
| director       = Shin Su-won
| producer       = 
| writer         = 
| starring       = Seo Young-hee   Kwon So-hyeon
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 121 minutes
| country        = South Korea
| language       = Korean
| budget         = 
}}

Madonna ( ) is an upcoming South Korean drama film directed by Shin Su-won.  It has been selected to screen in the Un Certain Regard section of the 2015 Cannes Film Festival.    

==Plot==
Hae-rim, a nurses aide, takes care of a paralyzed patient named Cheol-oh who is in need of a heart transplant. To obtain Cheol-ohs wealth, his son Sang-woo is determined to do everything in his power to extend his fathers life. One day, a comatose young woman named Mi-na is brought to the hospital after a mysterious accident. In exchange for a sum of money, Hae-rim agrees to Sang-woos instructions that she track down Mi-nas family and get them to sign an organ donation consent form. Hae-rim learns that Mi-na is poor and nicknamed "Madonna," but the more she delves into Mi-nas past, she discovers horrific secrets.

==Cast==
*Seo Young-hee as Hae-rim
*Kwon So-hyeon as Mi-na 
*Kim Young-min as Sang-woo
*Ko Seo-hee as Hyeon-joo
*Yoo Soon-chul as Cheol-oh
*Ye Soo-jung as Foul-mouthed old woman
*Shin Woon-sub as Section chief Han
*Byun Yo-han as Hyeok-gyu
*Kim Ho-jung as Pimp
*Han Song-hee as Mi-young
*Lee Myung-haeng as Section chief Park
*Lee Sang-hee as Ah-ram
*Park Hyun-young as Joon-hee
*Jin Yong-wook as Jong-dae

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 