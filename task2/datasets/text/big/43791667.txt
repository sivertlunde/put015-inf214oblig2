The Journey (2014 Greek film)
{{multiple issues|

 
 
 
 
}}
{{Infobox film
| name = The Journey
| director = Lance Nielsen
| writer = Lance Nielsen
| starring = Jason Flemyng Linsey Coulson
| released =  
| country = Greece United Kingdom
}}
The Journey is a 2014 drama feature film set predominantly in Greece and written and directed by Danish filmmaker Lance Nielsen. It is a co-production between the UK and Greece. It stars Jason Flemyng, Lindsey Coulson, Duncan Pow, Dickon Tolson and Velile Tshabalala. Set on a fictional Greek island the film deals with several people facing the recent loss of someone they loved and how they overcome that loss by helping each other.

==Cast==
* Jason Flemyng as Ozzy
* Lindsey Coulson as Barbra
* Duncan Pow as Jason
* Dickon Tolson as Hector
* Mitchell Lewis as Atia
* Velile Tshabalala as Cytherea
* Shirin Youssefian as Rosy
* Dimtri Raft as Nondas
* Marc Zammit as Will
* Stelios Kalaitzis as The Priest
* Tasos Nousias as The Taxi Driver 
* Alex Heaton as Simon 
* Charlene Collins as Angela
* Carly Houston as Sharon

==Development==
The film was inspired by the circumstances of Lance Nielsen’s first visit to Greece in 2009.  When after the death of a close friend and his uncle on the same weekend he travelled to Greece on the advice of a stranger on the internet.  The film is dedicated to Angela Thomas who also had an episode of BBC Holby City dedicated  to her where she worked as a background artist. Though the film is not autobiographical a character based on Angela appears in the film played by Charlene Collins. 

==Casting==
The script was written in 2012 with Jason Flemyng in mind to play the lead character though later he would play a different role. Lindsey Coulson came on board to play Barbara once her schedule with Eastenders could be worked around.  Duncan Pow played the character of Jason, though later the cast would expand to include Dickon Tolson and Mitchell Lewis in the other two leading roles. Greek actor Tasos Nousias plays a cameo in the film. His role was intended to be larger but had to cut down due to scheduling conflicts. Velilie Tshabalala joined the cast in 2014 

==Production==
Lance Nielsen met several of the films Greek Producers while attending a film making course run by Chris Jones. They formed a company called 5 plus 1 Productions which along with Eagles Dare films acquired the resources to make the film happen. The film had no external source of funding and the total cash budget was either raised through Kickstarter and Indigogo or from Lance’s savings while many things were secured in Greece, including all the hotels for the cast and crew, for free, which made the filming possible. The Indigogo campaign raises some additional funds for the film in 2013. The Film raised £11,330   of its budget on Kickstarter in March 2014. Ultimately the cash budget for the film was just under £30,000

==Shooting==
Filming took place in May 2013 and May 2014 in Greece and October 2013 and July 2014 in London over a total of 28 days. The shooting was split over two years while additional funds could be raised to go back to Greece to complete the film. Filming took place on the islands of Aegina, Kea and briefly on Agistri. 

==References==
 

 
 