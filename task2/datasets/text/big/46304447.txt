Brèves de comptoir
{{Infobox film
| name           = Brèves de comptoir
| image          = 
| caption        = 
| director       = Jean-Michel Ribes
| producer       = Dominique Besnehard Michel Feller Jean-Michel Ribes Julien Deris David Gauquie Etienne Mallet
| writer         = Jean-Michel Ribes Jean-Marie Gourio
| based on       = 
| starring       = Chantal Neuwirth Régis Laspalès Yolande Moreau Valérie Mairesse André Dussollier
| music          = 
| cinematography = Philippe Guilbert
| editing        = Scott Stevenson
| distributor    = Diaphana Films
| studio         = Mon Voisin Productions Ulysse Films
| released       =  
| runtime        = 96 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $1,530,210 
}}
Brèves de comptoir (Counter brief) is a 2014 French ensemble comedy directed by Jean-Michel Ribes. 

==Plot==
The life of a small cafe in the suburbs, Swallow, it opened at six in the morning until closing.

==Cast==
 
* Chantal Neuwirth as The boss
* Didier Bénureau as The boss
* Régis Laspalès as Mussel
* Yolande Moreau as Madame Lamelle
* Valérie Mairesse as Madame Pelton
* André Dussollier as The politician
* François Morel (actor)|François Morel as Pivert
* Michel Fau as The writer
* Laurent Stocker as Monsieur Laroque Philippe Chevallier as Monsieur Latour
* Samir Guesmi as Couss
* Daniel Russo as Jacky
* Dominique Pinon as A taxi
* Grégory Gadebois as A taxi
* India Hair as The greedy woman
* Bruno Solo as Bolo
* Alexie Ribes as Gigi
* Michelle Bréant as Virginie
* Marcel Philippot as Monsieur Rabier
* Christian Pereira as The boy
* Laurent Gamelon as Rubens
* Annie Grégorio as The postmistress
* Olivier Saladin as Pulmoll
* Dioucounda Koma as Dakar
* Patrick Ligardes as The hairdresser
* Serge Bagdassarian as The philosopher
* Marc Bodnar as The man in love
* Lola Naymark as The young woman in love
* Grégoire Bonnet as The foreman
* Jean-Charles Clichet as Monsieur Pasta with Garlic
* Emeline Bayart as The impassive lady
* Jean-Claude Leguay as La Tonne
* Dominique Besnehard as Chorister
* Christine Murillo as The sellor
* Laurence Vielle as The Chic lady
* Jenny Clève as The old lady
* Alban Casterman as Monsieur Jean
* Eric Verdin as The sewerman
* Sébastien Thiery as The type cap
* Isabelle de Botton as The Player cards
 

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 