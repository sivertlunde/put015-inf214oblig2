A Maori Maid's Love
 
 
{{Infobox film
| name           = A Maori Maids Love
| image          = 
| image_size     =
| caption        = 
| director       = Raymond Longford
| producer       = Raymond Longford Lottie Lyell
| writer         = Raymond Longford Lottie Lyell
| narrator       =
| starring       = Lottie Lyell Raymond Longford
| music          =
| cinematography = 
| editing        = Lottie Lyell
| studio       = Vita Film Corporation The Zealandia Photo Pay Producing Co.
| distributor    = The Eureka Exchange (Aust)
| released       = 10 January 1916 (Australia) 3 November 1915 (NZ)
| runtime        = 5,000 feet (five reels) "Raymond Longford", Cinema Papers, January 1974 p51 
| country        = Australia
| language       = Silent film  English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}
 interracial romance between a white man and a Māori people|Māori girl. It is considered a lost film as there are no known copies. 

==Plot==
Graham, an unhappily married surveyor, goes on a job to New Zealand where he falls in love with a Maori woman. She becomes pregnant and died in childbirth. Graham puts his daughter in the care of Maori Jack, who later kills Graham. However his daughter (Lottie Lyell) inherits his property and falls in love with a jackeroo called Jim. 

==Cast==
*Lottie Lyell
*Raymond Longford
*Kenneth Carlisle
*Rawdon Blandford

==Production==
The film was shot on location in Rotorua and Auckland from August 1915, with finance from a Sydney company, Vita Film Corporation. It was the first of two films Longford and Lyell made in New Zealand, the other being The Mutiny of the Bounty (1916). 

==Release==

===Distribution difficulties===
Longford was unable to secure a release for the film in New Zealand. He blamed this on the influence of "the Combine" of Australasian Films and Union Theatres, who dominated distribution and exhibition at the time.  The film was given a limited release in Sydney at a cinema owned by Hubert and Caroline Pugliese. 

===Critical Reception===
The critic from the Sydney Sun called it "unquestionably the best moving picture produced up to date at this end of the world... there would be little need for importing films while Australia can make her own of such a standard." 

The Motion Picture News said the film "certainly could not be classed as a masterpiece. Reduced to three reels it would make a good, pleasing feature. The subtitles in their present state are crude and need revision. Director Raymond Longford had a hard task when he posed the Maori maids before the camera and deserves credit for the results obtained." 

Lottie Lyell edited the film for its British release. 

==Significance==
The movie is generally agreed to be the first full-length New Zealand feature film.  

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 

 
 
 
 
 
 
 
 
 
 
 