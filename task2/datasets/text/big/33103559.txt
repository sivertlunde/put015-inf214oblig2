The Waybacks (film)
 
 
{{Infobox film
  | name     = The Waybacks
  | image    = 
  | caption  = 
  | director = Arthur W. Sterry		
  | producer = Humbert Pugliese
  | writer   = 
  | based on = play by Philip Lytton novels by Henry Fletcher
  | starring = Vincent White
  | music    = 
  | cinematography = Ernest Higgins
  | editing  = 
| studio = Koala Films
  | distributor = 
  | released = 18 May 1918
  | runtime  = 7,000 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 play adaptation of a series of popular novels.    Only part of the film survives today.

==Plot==
The Wayback family visit Sydney from the bush. Dad and his son Jabex make friends with a group of bathing beauties at Bondi. Mum visits a fortune teller.

==Cast==
*Vincent White as Dads Wayback
*Gladys Leigh as Mums Wayback
*Lucy Adair as Tilly
*Louis Machilaton as Jabex
*Rose Rooney as Frances Holmes
*Harry Hodson as Dan Robins
*William Turner as Charley Lyons
*George Hewlitt as Nigel Kelvin
*Lance Vane as Jack Hinds

==Original play==
{{Infobox play
| name       = The Waybacks
| image      = The Waybacks.jpg
| image_size = 
| caption    = Poster from Tasmania production of play
| writer     = Philip Lytton
| characters =
| setting    = 
| premiere   = 1915
| place      = 
| orig_lang  = English
| subject    = 
| genre      = Rural comedy
}} On Our Selection, and enjoyed almost as much popularity at the box office during its original run.  

The plot involves the Wayback family visiting Sydney and having various adventures. 

==Production== The Life Story of John Lee, or The Man They Could Not Hang (1921). He appeared in the original stage production as Charley Lyons. 
 Windsor and in Sydney. Two of the cast, Gladys Leigh and Harry Hodson, reprised their roles from the stage production. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 79. 

==Release==
The film was a success at the box office and continued to be seen in cinemas until 1925.  It was re-released as The Waybacks of 1925.

Sterry planned a sequel, The Cornstalks, but it does not seem to have been completed. 

==References==
 

==External links==
*  
*   at National Film and Sound Archive
* 
*  – discusses The Waybacks
*  at AusStage

 
 
 
 
 
 
 
 
 
 