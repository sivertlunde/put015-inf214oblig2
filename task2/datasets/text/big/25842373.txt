The Stone Boy (film)
{{Infobox film
| name           = The Stone Boy
| image          = The Stone Boy.jpg
| image_size     =
| caption        =
| director       = Christopher Cain
| writer         =
| narrator       =
| starring       = Robert Duvall Frederic Forrest Glenn Close Wilford Brimley
| music          = James Horner
| cinematography =
| editing        =
| distributor    = 20th Century Fox
| released       =  
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
The Stone Boy is a 1984 film directed by Christopher Cain, and is based on the 1957 short story, "The Stone Boy," by the American author Gina Berriault. The film is about a family coping with an abrupt tragedy when the youngest child accidentally kills his older brother in a hunting incident.

==Cast==
* Robert Duvall 	... Joe Hillerman  
* Jason Presson 	... 	Arnold Hillerman  
* Glenn Close 	... 	Ruth Hillerman  
* Susan Rinell 	... 	Nora Hillerman 
* Dean Cain 	... 	Eugene Hillerman  
* Frederic Forrest 	... 	Andy Jansen   Cindy Fisher 	... Amalie  
* Gail Youngs 	... 	Lu Jansen  
* Wilford Brimley 	... George Jansen  
* Linda Hamilton	... 	Eva Crescent Moon Lady 
* Tom Waits	... 	Petrified man at carnival (Cameo appearance|cameo)

==External links==
* 

 

 
 
 
 
 
 
 
 


 