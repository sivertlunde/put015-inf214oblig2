Fashion Model
{{Infobox film
| name           = Fashion Model
| image          = 
| director       = William Beaudine
| producer       = 
| writer         = Victor Hammond and Tim Ryan Robert Lowery Tim Ryan
| music          = Edward J. Kay
| cinematography = Harry Neumann
| editing        = 
| distributor    = Monogram Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         = 
}} Robert Lowery, Tim Ryan, and written by Victor Hammond and Tim Ryan.     

The film is considered to be the third in a series of Beaudine films featuring Marjorie Weaver as a female detective after Detective Kitty ODay (1944) and Adventures of Kitty ODay (1944).       

==Cast== Robert Lowery as Jimmy OBrien
*Marjorie Weaver as Peggy Rooney Tim Ryan as OHara
*Lorna Gray as Yvonne Brewster
*Dorothy Christy as Madame Celeste
*Dewey Robinson as Grogan
*Sally Yarnell as Marie Lewis
*Jack Norton as Herbert
*Harry Depp as Harvey Van Allen
*Nell Craig as Jessica Van Allen Edward Keane as Jacques Duval John Valentine as Davis

==Plot==
When a prominent fashion model is murdered, a stockboy is accused of the crime must clear his name, working with another model.

==Release==
The 61 minute film premiered on March 2, 1945.   The Motion Picture Guide called it "light but entertaining".   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 