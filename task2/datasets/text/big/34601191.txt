Nandeeswarudu
{{Infobox film
| name           = Nandeeswarudu
| image          = 
| director       = Anji Sreenu Yarajala
| producer       = Dr. Kota Gangadhar Reddy   Segu Ramesh Babu
| writer         = Paruchuri Brothers  
| story          = Anji Sreenu Yarajala   Ravi Bhillagari
| screenplay     = Anji Sreenu Yarajala Nandamuri Taraka Ratna Jagapathi Babu Sheena Shahabadi
| music          = Parthasaradhi
| cinematography = N. Sudhakar Reddy
| editing        = K. V. Krishna Reddy
| studio         = KFC & SRB Art Productions
| distributor    = 
| released       =  
| runtime        = 2:38:13
| country        =   India
| language       = Telugu
}}
 Nandamuri Taraka Ratna, Jagapathi Babu, Sheena Shahabadi in lead roles and music composed by Parthasaradhi. The film is a remake of the Kannada film Deadly Soma.  The film recorded as flop at box-office.Dubbed in hindi as Darindigi Ka Anth. 

==Plot==
Nandu (Nandamuri Taraka Ratna) is a student who wants to become a police officer. His parents (Suman and Seetha) want him to lead a meaningful life. Nandu has a habit of getting into fights and this leads him to clash with a local gangster Baba (Ajay). A few shocking incidents take place and Nandu ends up in jail. Fed up with the way things are turning up, Nandu introspects and decides to redeem himself as a man of the people. He reshapes himself as Nandeeswarudu and starts fighting for people who are oppressed.
The government appoints Commissioner Eeswar Prasad (Jagapathi Babu) as a special officer to rid the city of gangsters. In the fight between Nandeeswarudu and Baba, only one emerges victorious. Who will it be? That forms the story.   

==Cast==
  Nandamuri Taraka Ratna as Nadeswarudu / Nandu
* Jagapathi Babu as Commissioner Eeswar Prasad
* Sheena Shahabadi Suman as Ananda Bhupathi
* Mannava Balayya|M. Balayah as Satya Murthy  
* Chalapathi Rao as DGP Ajay as Babanna
* Nagineedu as Home Minister
* Rajiv Kanakala as Saleem
* Benerjee as Benerjee
* G. V. Sudhakar Naidu as Vasanth
* Mukthar Khan as Naganna 
* Vijayachander as CM
* Prabhas Srinu as Seenu
* Sivaji Raja as Sivaji
* Director Sarath as Sarath
* Kota Shankar Rao as Shankar Rao Seeta as Lakshmi 
* Delhi Rajeswari as Yashoda
* Rachana Maurya as Item Number
* Lahari as Lahari
* Jayavani as Bangaram 
* Master Athulith as Young Nadeswarudu
 

==Soundtrack==
{{Infobox album
| Name        = 
| Tagline     = 
| Type        = film
| Artist      = Parthasaradhi
| Cover       = 
| Released    = 2011
| Recorded    = 
| Genre       = Soundtrack
| Length      = 28:52
| Label       = Aditya Music
| Producer    = Parthasaradhi
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

Music composed by Parthasaradhi. Lyrics written by Ram Paidesetti. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 28:52
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Racha Racha Ranjith
| length1 = 4:29

| title2  = Naa Rupe Mirchi  
| extra2  = Sravana Bhargavi
| length2 = 4:33

| title3  = Chettu Meeda Sunitha
| length3 = 4:28

| title4  = Bindas Bindas
| extra4  = Parthu
| length4 = 3:55

| title5  = Adire Andalu  Nandamuri Taraka Ratna,Kalpana
| length5 = 4:17

| title6  = Yegire
| extra6  = M. M. Keeravani
| length6 = 2:44

| title7  = Nandeeswarudu SP Balu
| length7 = 4:12
}}

==References==
 

 
 
 
 
 
 
 


 