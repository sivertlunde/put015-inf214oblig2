Rock 'n' Roll Nightmare
{{Infobox film
| name           = Rock n Roll Nightmare
| image          = Rocknrollnightmare.jpg
| image_size     =
| caption        = Special Edition DVD cover
| director       = John Fasano
| producer       = Jon Mikl Thor
| writer         = Jon Mikl Thor
| narrator       =
| starring       = Jon Mikl Thor Jillian Peri Teresa Simpson Frank Dietz Lara Daans
| music          = Jon Mikl Thor
| cinematography = Mark Mackay
| editing        =
| distributor    = Shapiro Entertainment (theatrical) Academy Entertainment (VHS)
| released       =  
| runtime        = 83 min
| country        = Canada
| language       = English
| budget         = $100,000
| gross          =
}}

Rock n Roll Nightmare (also known as The Edge of Hell) is a 1987 direct-to-video Canadian horror film directed by John Fasano, and stars heavy metal musician Jon Mikl Thor, Jillian Peri, and Teresa Simpson. 

==Plot==
Recording some new music in an isolated farmhouse, the band Triton gets more than they bargained for when something horrifying stirs in the darkness. Eternal evil haunts this place and the band members start turning into demons from Hell itself! After a day of making music – and making love – this band is starting to break up… one by one… limb by limb. The bands lead singer, John (Jon-Mikl Thor) Triton, holds the key to defeating this horror once and for all – a secret that culminates in a battle between good and evil! Triton versus the Devil himself!

==Production==
Production began in 1986 and the film had a budget of $100,000 and was shot in seven days. Originally titled The Edge of Hell, the filming was set to last ten days, but one of the producers had to shorten production due to a death in the family. The director used many friends as actors and the film was filmed in Ontario. The film had its title changed to help sell it in the markets.

==DVD release== Synapse films. 

== Sequel ==

In 2005 a sequel featuring Jon Mikl Thor was made. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 