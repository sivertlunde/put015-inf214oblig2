Zindagi (1964 film)
{{Infobox film
| name           = Zindagi
| image          = Zindagi film poster.jpg
| image_size     = 
| caption        = 
| director       = Ramanand Sagar
| producer       = Gemini Pictures
| writer         = 
| narrator       = 
| starring       = Rajendra Kumar Vyjayanthimala Prithviraj Kapoor
| music          = Shankar Jaikishan
| cinematography = 
| editing        = 
| distributor    = Gemini Pictures
| released       = 1964
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Zindagi is a Hindi movie produced by S. S. Vasan, Gemini Pictures and  directed by Ramanand Sagar. The film stars Rajendra Kumar, Vyjayanthimala, Raaj Kumar, Prithviraj Kapoor, Mehmood Ali|Mehmood, Jayant (actor)|Jayant, Jeevan, Leela Chitnis and Helen Jairag Richardson| Helen. The films music is by Shankar Jaikishan.

==Plot==
Beena works as a stage actress and lives a poor lifestyle with her widowed mother. One day while returning home she is molested by Bankhe and two other men, but Rajan comes to her rescue. He escorts her home and soon both of them fall in love with each other. Rajans dad, Rai Bahadur Gangasaran, is wealthy and does not approve of Beena at all. But when Rajan threatens to leave him, he changes his mind and permits them to get married. After the marriage they settle down to a harmonious life and soon Beena becomes pregnant. One night while driving, Rajan gets a flat tire, he leaves Beena in the car in order to get help. When he returns Beena has disappeared. She returns home the next day, and everyone is relieved to have her back. A few days later they get the news that Gopal, the Manager of the Theatre that Beena used to work with, has been arrested and charged with killing a man named Ratanlal. Gangasaran is on the jury and would like Gopal to get a fair trial. The police find out that a woman was present with Gopal at the night of the murder, but Gopal refuses to divulge her name. As a result of this refusal, he is found guilty. When the sentence is about to be passed, Beena suddenly appears in Court and testifies that she was with Gopal the night of the murder. Gopal is set free, but Beena is shunned by Rajan and Gangasaran and not only driven out of the house, but also from the town. The question does remain - what compelled Beena to spend the night with Gopal, and why was Gopal accused of killing Ratanlal?

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!!Lyricist
|-
| 1
| "Pahle Mile The Sapnon Mein"
| Mohammed Rafi
| Hasrat Jaipuri
|-
| 2
| "Ghungharwa Mora Chham Chham Baje"
| Mohammed Rafi, Asha Bhosle
| Hasrat Jaipuri
|-
| 3
| "Muskura Ladle Muskura"
| Manna Dey Shailendra
|-
| 4
| "Ham Pyar Ka Sauda Karte Hai"
| Lata Mangeshkar
| Hasrat Jaipuri
|-
| 5
| "Dil Ka Kanwal Denge"
| Lata Mangeshkar
| Shailendra
|-
| 6
| "Ek Naye Mehman"
| Lata Mangeshkar
| Shailendra
|-
| 7
| "Pyar Ki Dulhan Sada Suhagan"
| Lata Mangeshkar
| Hasrat Jaipuri
|- 8
| "Humne Jafa Na Seekhi"
| Mohammed Rafi
| Hasrat Jaipuri
|}

== External links ==
*  

 
 
 
 
 