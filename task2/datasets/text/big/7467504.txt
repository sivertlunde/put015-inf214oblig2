Fidibus
{{Infobox film
| name           = Fidibus
| image_size     = 
| image	=	Fidibus FilmPoster.jpeg
| caption        = 
| director       = Hella Joof
| producer       = Thomas Gammeltoft   Jannik Johansen (executive)
| writer         = Hella Joof
| narrator       = 
| starring       = Rudi Køhnke   Lene Maria Christensen   Jonatan Spang   Jokeren
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Denmark Danish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Danish film by Hella Joof from 2006. First presented at the Roskilde Festival, summer of 2006 but not in cinemas until 13. oct.

==Starring==
*Rudi Køhnke as Kalle, the main character
*Jonatan Spang, a Danish stand-up comedian, as Kalles friend Agger
*Jesper "Jokeren" Dahl, a Danish rapper, as the dealer Paten

===Plot===
Kalle, who is studying at the university, and his friend Agger eventually loses some hash that originally belongs to Paten (Abbreviation for "psykopaten", "the psychopath"). However, straight after Paten goes to jail, Kalle falls in love with his ignorant girlfriend Sabrina, though he has been warned not to touch Patens money, girl(s) or car.

==External links==
*  
*  

 
 
 
 


 