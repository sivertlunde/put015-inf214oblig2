Pocket Maar (1974 film)
{{Infobox film
| name           = Pocket Maar
| image          = Pocket Maar, 1974.jpg
| caption        = Film Poster
| director       = Ramesh Lakhanpal
| producer       = Ramesh Lakhanpal, Joginder Singh Luthra
| writer         = Vrajendra Gaur (dialogue)
| screenplay     = Kaushal Bharati  Shashi Bhushan (additional screenplay)
| story          = Mushtaq Jalili
| starring       = Dharmendra Saira Banu Prem Chopra
| music          = Original Score & Songs:  
| cinematography = Kishore Rege
| editing        = Pran Mehra   
| released       = 1974
| runtime        = 136 min.
| country        = India
| language       = Hindi
| budget         = 
}}

Pocket Maar ( : Pick-Pocket) is a Bollywood film of 1974 Hindi film directed by Ramesh Lakhanpal. The film stars Dharmendra, Saira Banu, Prem Chopra, Mehmood and Nasir Hussain. The films music is by Laxmikant Pyarelal.

==Plot==

Mr. Rai is a big businessman. Asha, Mr. Rais Grand Daughter. She loves Madan and marry with her. Mr. Rai dont know anything about Madan. If they say yes to the happiness of granddaughter. Shankar is a pick-pocket. Asha introduces Shankar to everyone as Madan. Who is the real Madan? Asha to do that?

==Soundtrack==

The lyrics for the film songs were written by Anand Bakshi, and the music was composed by Laxmikant-Pyarelal.

{{Track listing
| extra_column = Singer(s)
| title1 = Banda Parwar Mai Kaha Ye Aapki Mahfil Kaha | extra1 = Mohammed Rafi | length1 = 3:59 
| title2 = Chori Se Chupke Se Le Ke Tera Naam | extra2 = Lata Mangeshkar | length2 = 4:34  
| title3 = Na Tujhse Dur Ja Saku | extra3 = Mohammed Rafi | length3 = 3:23 
| title4 = Dushmani Hai Ye To Mohabbat Nahi | extra4 = Asha Bhosle | length4 = 
| title5 = Hum Jaan Lada Denge | extra5 = Mohammed Rafi | length5 = 
| title6 = Uyi Kya Hua Ho Gaya | extra6 = Manna Dey, Asha Bhosle | length6 = 
}}

==Cast==

* Dharmendra as Shankar
* Saira Banu as Asha Rai
* Prem Chopra as Madan Malhotra
* Mehmood as Sunder
* Nasir Hussain as Mr. Rai
* Shubha Khote as Sheela Verma
* Azra as Ganga Asit Sen as Dr. Sen
* Praveen Paul as Mrs. Verma
* Keshav Rana as Pran Lal
* Shanti Swaroop
* Prem Mannade
* Masood
* Baldev Mehta
* Radheshyam

==Crew==

*Director = Ramesh Lakhanpal
*Producer = Ramesh Lakhanpal and Joginder Singh Luthra
*Assistant Director = Roop Sharma, Vijay Maini, Roop Kumar
*Cinematography = Kishore Rege
*Film Editing = Pran Mehra
*Art Director = V. Jadhav
*Art Department = Abu Hassan Mistry
*Hair Stylist = Geeta, Flory Pattrick, Yen Young Sheish
*Sound Recordist = S.C. Bhambri
*Special Effects = Prafull Gade(main titles), Suresh Naik and M.A. Hafeez(special effects)
*Costumes : Naseem Banu (Saira Banus costumes), Kashinath, Dhiren Kumar and Shantaram Prabhulkar (Dress)
*Choreographer: Suresh Bhatt, Kamal, Surya Kumar, P.L. Raj

==External links==
 

 
 
 


 