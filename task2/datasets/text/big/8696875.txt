Carnies (film)
 
{{Infobox film
| name           = Carnies
| image          = carnies_wikipedia.jpg
| caption        =
| director       = Brian Corder
| producer       = John Corder Chris Staviski
| writer         = Ron Leming John B. Nash Doug Jones Lee Perkins
| music          =
| cinematography = Geoff Hamby  Brian Corder
| editing        =
| distributor    =
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
}} Doug Jones. 

==Plot==
In 1936 the Knuckles Brothers Show, a carnival sideshow, pulls into another in a long series of small towns, only to be plagued by a killer.

==Cast==
* Chris Staviski as Virgil Doug Jones as Ratcatcher David Markham as William Crowley
* Reggie Bannister as  Detective Conrad Ellison Denise Gossett as Helen
* Lynn Ayala as Zoe Lee Perkins as Professor James Algonquin

==Release==
The DVD release is set for October 12, 2010 on DVD and Video on Demand.  The theatrical release is part of the "Week of Terror" festival in the Bluelight Cinemas in Cupertino, California. 

==References==
 

==External links==
*   at Carnytown
*  
*  
*  

 
 
 

 