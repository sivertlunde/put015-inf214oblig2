The Miracle Worker (1962 film)
{{Infobox film
| name           = The Miracle Worker
| image          = MiracleWorkerPoster.JPG
| image_size     = 
| caption        = Original poster
| director       = Arthur Penn
| producer       = Fred Coe William Gibson
| starring       = Anne Bancroft Patty Duke 
| music          = Laurence Rosenthal
| cinematography = Ernesto Caparrós
| editing        = Aram Avakian
| studio         = Playfilm Productions
| distributor    = United Artists
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $500,000 
| gross          = $2.5 million   
}} William Gibson 1959 play The Story of My Life, the 1902 autobiography of Helen Keller.
 Best Director Best Actress Best Supporting Actress for Patty Duke. The Miracle Worker also holds a perfect 100% score from the movie critics site Rotten Tomatoes. 

==Plot synopsis== blind and deaf since infancy due to a severe case of scarlet fever, is frustrated by her inability to communicate and subject to frequent violent and uncontrollable outbursts as a result. Unable to deal with her, her terrified and helpless parents contact the Perkins School for the Blind for assistance. In response they send Anne Sullivan (Anne Bancroft), a former student, to the Keller home to tutor her. What ensues is a battle of wills as Anne breaks down Helens walls of silence and darkness through persistence, love, and sheer stubbornness.

==Cast==
* Anne Bancroft		: 	Anne Sullivan
* Patty Duke		: 	Helen Keller
* Victor Jory		: 	Captain Keller
* Inga Swenson		: 	Mrs. Keller
* Andrew Prine		: 	James Keller
* Brandon Rabe              :       The Blind Dog

==Production notes== Broadway production, United Artists executives wanted a bigger name cast as Anne Sullivan in the film adaptation. They offered to budget the film at $5 million if Elizabeth Taylor was cast but only $500,000 if director Arthur Penn insisted on using Bancroft. Penn, who had directed the stage production, remained loyal to his star.  The move paid off, and Bancroft won an Oscar for her role in the film.

Also despite the fact that Patty Duke had played Helen Keller in the play, she almost did not get the part. The reason was that Duke, 15 years old at the time, was too old to portray a seven-year-old girl, but after Bancroft was cast as Anne, Duke was chosen to play Helen in the movie.

For the dining room battle scene, in which Anne tries to teach Helen proper table manners, both Bancroft and Patty Duke wore padding beneath their costumes to prevent serious bruising during the intense physical skirmish. The nine-minute sequence required three cameras and took five days to film. 

The film was shot at Big Sky Ranch in Simi Valley, California and Middletown, New Jersey.

It was remade twice for television, in 1979 with Patty Duke as Anne and Melissa Gilbert as Helen and in 2000 with Alison Elliott and Hallie Kate Eisenberg in the lead roles.

The film ranked #15 on  .

==Reception==
In his review in the New York Times, Bosley Crowther observed, "The absolutely tremendous and unforgettable display of physically powerful acting that Anne Bancroft and Patty Duke put on in William Gibsons stage play The Miracle Worker is repeated by them in the film . . . But because the physical encounters between the two . . . seem to be more frequent and prolonged than they were in the play and are shown in close-ups, which dump the passion and violence right into your lap, the sheer rough-and-tumble of the drama becomes more dominant than it was on the stage . . . The bruising encounters between the two . . . are intensely significant of the drama and do excite strong emotional response. But the very intensity of them and the fact that it is hard to see the difference between the violent struggle to force the child to obey . . .  and the violent struggle to make her comprehend words makes for sameness in these encounters and eventually an exhausting monotony. This is the disadvantage of so much energy. However, Miss Bancrofts performance does bring to life and reveal a wondrous woman with great humor and compassion as well as athletic skill. And little Miss Duke, in those moments when she frantically pantomimes her bewilderment and desperate groping, is both gruesome and pitiable."   

TV Guide rates the film 4½ out of a possible five stars and calls it "a harrowing, painfully honest, sometimes violent journey, astonishingly acted and rendered."  

Time Out London opined, "Its a stunningly impressive piece of work . . . deriving much of its power from the performances. Patty Duke and Anne Bancroft spark off each other with a violence and emotional honesty rarely seen in the cinema, lighting up each others loneliness, vulnerability, and plain fear. What is in fact astonishing is the way that, while constructing a piece of very carefully directed and intelligently written melodrama, Penn manages to avoid sentimentality or even undue optimism about the value of Helens education, and the way he achieves such a feeling of raw spontaneity in the acting."  

==Awards and nominations==
 
*Academy Award for Best Actress (Anne Bancroft, winner)
*Academy Award for Best Supporting Actress (Patty Duke, winner)
*Academy Award for Best Director (Arthur Penn, nominee) Academy Award for Best Adapted Screenplay (William Gibson, nominee) Academy Award for Best Black-and-White Costume Design (Ruth Morley, nominee)

==See also==
*List of American films of 1962 Black

==References==
 

==External links==
* 

 
 
 
{{S-ttl|title=Academy Award winner for   Best Actress and  Best Supporting Actress}}
 
 

 
 


 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 