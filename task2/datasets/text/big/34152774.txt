The Bad Seed (1985 film)
{{Infobox film
| name           = The Bad Seed (1985 Film)
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Paul Wendkos
| producer       = 
| writer         = George Eckstein
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Chad Allen
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 94 min
| country        = USA English
| budget         = 
| gross          =
}}

 
 1954 novel 1956 movie directed by Mervyn LeRoy.

== Synopsis ==
Freak fatal accidents force a widow to realize her precocious 9-year-old daughter was born to kill.

== Detailed plot ==
Rachel is a 9-year-old girl who is perfection itself&nbsp;— unless shes crossed or challenged. Several disturbing recent incidents have led Rachels mother Christine to suspect that her child is a latent murderess. Upon discovering that she herself is the daughter of a convicted killer, Christine becomes convinced that sweet little Rachel is a "bad seed"&nbsp;— an inherent killer who feels no remorse, because she doesnt know any better.

== Cast ==

=== Major characters ===
*Blair Brown&nbsp;— Christine Penmark
*Lynn Redgrave&nbsp;— Monica Breedlove 
*David Carradine&nbsp;— Leroy Jessup  
*Carrie Welles&nbsp;— Rachel Penmark 
*Richard Kiley&nbsp;— Richard Bravo 
*David Ogden Stiers&nbsp;— Emory Breedlove Chad Allen&nbsp;— Mark Daigler

=== Minor characters ===
*Eve Smith&nbsp;— Mrs. Post 
*Carol Locatell&nbsp;— Rita Dailger
*Anne Haney&nbsp;— Alice Fern

==References==
 

== External links ==
*  

 

 
 
 
 


 

 
 