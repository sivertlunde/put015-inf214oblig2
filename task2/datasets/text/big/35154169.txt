Nadodi (1966 film)
{{Infobox film
| name           = NADODI
| image          = Nadodi (1966 film).jpg
| image_size     =
| caption        =
| director       = B. R. Panthulu
| producer       = B. R. Panthulu
| writer         = R. K. Shanmugam 
| narrator       = Bhaarthi
| music          = M. S. Viswanathan
| cinematography = V. Ramamoorthy
| editing        = R. Devarajan
| studio         = Padmini Pictures
| distributor    = Padmini Pictures
| released       = 14 April 1966
| runtime        = 155 minutes
| country        = India Tamil
| budget         =   7 lakhs
| gross          =   17 lakhs
| website        =
}}
 Indian Tamil cinema|Tamil-language film directed by B. R. Panthulu, starring M. G. Ramachandran in the lead role and B. Saroja Devi. The film ran for 70 days. 

==Plot==
Between Chennai, Madurai and Kodaikanal...

Thiyagu (M. G. Ramachandran) remains inconsolable since Meena (Bharathi Vishnuvardhan|Bhaarthi), his lover committed suicide because of his father, rich Dharmalingham (V. K. Ramasamy (actor)|V. K. Ramaswamy).

The latter refused categorically that Meena (Bharathi Vishnuvardhan|Bhaarthi) gets married to her beloved Thiyagu (MGR), under the pretext that it is up to a lower caste !

While the same father, an activist speaker travels the country, by crying out, by preaching, to anyone who might be listening the virtues of the abolition of this feudal system.

To take him in her own trap, break so his hypocrisy and in memory especially of her older sister, Radha (B. Sarojadevi), the second girl of Dharmalingham leaves on the meeting of the mysterious benefactor that seems being Thiyagu (MGR), decided well to get married to him.

Indeed, this good model son, adoptive, gave up his immense heritage, preferring to bequeath integrality to the blue-collar workers of his late father (P.S.Vengathachalam).

Thiyagu (MGR) ends up leaving the family field and is put in search of plenitude, by making of course its passage.

Radha (B. Sarojadevi), meanwhile, falls into the claws from Djambhu (M. N. Nambiar), a former gardener who holds for person in charge of all his misfortunes the father (V. K. Ramasamy (actor)|V. K. Ramaswamy) of this one, particularly the death of his mother and her little sister.

In reprisals, by reminding the contemptible words uttered by Dharmalingham (V. K. Ramasamy (actor)|V. K. Ramaswamy) against his blind little sister, Djambhu (M. N. Nambiar) decides to return the similar one.

He deprives Radha (B. Sarojadevi) of its sight and puts it at the corners streets so that she begs.

Later, the ways of Radha (B. Sarojadevi) and Thiyagu (MGR) cross.

Alas, neither one, nor the other can be recognized, be identified about it.

==Cast==
{| class="wikitable" width="100%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || as M.Thiyagu
|-
| M. N. Nambiar || as Djambhu
|-
| Nagesh || as "Valippu" Manikkum
|-
| V. K. Ramasamy (actor)|V. K. Ramasamy || as Dharmalingham
|-
| K. R. Ramasamy || as Nagendhran, the neurologist
|-
| V.Gopalakrishnan || as Thiyagu (impostor)
|-
| P.S.Vengathachalam ("Appa" Venkitachalam) || as Arumugham Mudhailaiyar, M.Thiyagu s adoptive father
|-
| S.M.Thirupadhisamy || as The grandfather
|-
| "Loose" Arumugham || as
|-
| Raja || as
|-
| S.A.J.Samy || as
|-
| Kumar || as
|-
| Kundhumani || as Pitchandhi
|-
| Kamatchinadhan || as
|-
| Kesevan || as
|-
| Samy || as
|-
| Thandhabani || as
|-
| P.S.Selvaraj || as
|-
| B. Sarojadevi || as Radha Dharmalingham (2nd daughter)
|- Bhaarthi (Introducing) || as Meena Dharmalingham (1st daughter)
|-
| P. K. Saraswathi || as Lakshimi Dharmalingham (wife)
|-
| Baby Shaguila || as Radjee Dharmalingham (3rd daughter)
|-
| Kumari || as
|-
| B. R. Panthulu (not mentioned ) || as The wise old man who comforts and who writes the words "Kadavul Thandha Paadam..."
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==
On 4 productions of Padmini Pictures, with the " Makkal Thilagum " MGR, only NADHODHI was turned in black and with for main feminine partner,  Abhinaya Saraswadi " B.Sarodja Devi.

The style of MGR adopted for NADHODHI, was small skullcap screwed on the head, a harmonium over the shoulder and the left leg of the pants tucked up in the middle-calf.

The director-producer B. R. Panthulu returns to his puppy loves, by making the actor, on NADHODHI, he makes it a very short appearance, according to DVD, (Pyramid Vision, of 0.26.37. in 0.29.44.) or (Raj Video Vision, of 0.16.19. in 0.19.40.) in old wise person poet who consoles the character played by MGR, the good Thiyagu and predicts him that a brilliant future, of the richest waits for him.

The scene where Thiyagu is arrested and sentenced for disorder to the order public, during a party of a local divinity, is pulled quite straight ahead by the classic American Modern Times (film) 1936, written and directed by Charlie Chaplin.

NADHODHI is built around one of the major plagues of the Indian society : the system of castes.

We already find this theme on a MGR of 1965, realized by T.R.Ramanna, the excellent PANAM PADAITHAVAN.

The sequence of the cure of the person of the main actress, B.Sarodja Devi is made more credible one by the advices of a true practitioner, the doctor P.S.Sari, thanked in the credits of opening of NADHODHI.

His homologue, in the screen, the doctor Nagendhran played here by the excellent actor K.R.Ramasamy, in guest of honor.

The big B.R.Panthulu helps to give a leg up in the Tamil cinema, the young one fellow countryman, beautiful Bharati, native also of the same state as the actress B.Sarodja Devi, the Karnataka.

Bharati will play in 2 other films with MGR. The same year, in 1966, the beautiful CHANDHRODAYAM and 6 years later, in ANNA MITHA KAÏ, on 1972.

His characters  do not enjoy a big luck beside MGR s movie. A little as those played by the other actress, charming Kantchana, seen on PARAKKUM PAVAI, on 1966 or still in NAAN YEN PIRANDHEN ?, 1972.

The actor veteran P.S.Venkitachalam, nicknamed affectionately and respectfully " Appa " Venkitachalam plays on NADHODHI, the good foster father, Arumugham of MGR-Thiyagu.

The actress P.K.Saraswathi who plays here, the mother-in-law of MGR, embodied her mother on his movie following, THANI PIRAVI, Devar Films of 1966.

K.R.Ramakrishnan says K.R.R., lighting stand-in and bodyguard of MGR plays the role of a police officer.

The composer M.S.Visuwanadhan did not arrange his effort for the "Makkal Thilagum" MGR in particular on NADHODHI because his music is an important element of the movie, repeatedly.

Indeed, the character of MGR, Thiyagu becomes by the strength of the events, a wandering always in the good heart, where he passes, he carries out the good and to live cheek of the music on his harmonium. Thus, the music of M.S.V. takes all its sense.

NADHODHI starts immediately by a song, "Ulagamengum Orey..." a duet intoned by the key couple of interpreter, T.M.S. - P.S., in the screen MGR-Bharati. The words are one more time of the feather of the poet Khannadhassan. Tom Dooley.

In the soundtrack of the movie, NADHODHI, the title " Kadhavul tandai pâdhum... " are the words sung as it is, to the screen, by the tenor T.M.Soundher Radjan.

On the vinyl of time, we find another version, which begins with " Kadhavul seîdai pâvum... ".

The great Khannadhassan so modified certainly, because of the censorship of time. Let us remember ourselves that we are in 1966.
 DMK did not pass yet, it is necessary to wait still for a few months, to wait until February 23rd, 1967...

==Songs==
{{tracklist	
| headline     = Tracklist 
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 
| title1       = Ulagamengum Orey
| extra1       = T. M. Soundararajan, P. Susheela
| lyrics1      = Kannadasan
| length1      = 3.24
| title2       = Kadavul seitha paavam
| extra2       = T. M. Soundararajan
| lyrics2      = Kannadasan
| length2      = 4.43
| title3       = Androru Naal
| extra3       = T. M. Soundararajan, P. Susheela
| lyrics3      = Kannadasan 
| length3      = 
| title4       = Naadu athai Naadu
| extra4       = T. M. Soundararajan, P. Susheela
| lyrics4      = Vaali
| length4      = 
| title5       = Paadum kural
| extra5       = P. Susheela
| lyrics5      = Kannadasan
| length5      = 
| title6       = Rassikathane intha
| extra6       = P. Susheela
| lyrics6      = Kannadasan
| length6      =
| title7       = Thirumbivaa
| extra7       = T. M. Soundararajan, P. Susheela
| lyrics7      = Vaali
| length7      =
| title8       = Kadavul Thandha paadam
| extra8       = T. M. Soundararajan
| lyrics8      = Kannadasan
| length8      = 
}}

==References==
 

==External links==
 

 
 
 
 


 