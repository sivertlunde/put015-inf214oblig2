Proibito
 
{{Infobox film
| name           = Proibito
| image          = Imaginediproibitoscena.jpg
| caption        = Film poster
| director       = Mario Monicelli
| producer       = Jacques Bar
| writer         = Suso Cecchi dAmico Mario Monicelli Grazia Deledda Giuseppe Mangione
| starring       = Mel Ferrer
| music          = Nino Rota
| cinematography = Aldo Tonti
| editing        = Adriana Novelli
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Proibito ( ) is a 1954 Italian drama film directed by Mario Monicelli and starring Mel Ferrer.   

==Cast==
* Mel Ferrer as Don Paolo Solinas
* Amedeo Nazzari as Costantino Corraine
* Lea Massari as Agnese Barras
* Henri Vilbert as Niccodemo Barras
* Germaine Kerjean as Maddalena Solinas
* Paolo Ferrara as Maresciallo Taddei
* Eduardo Ciannelli as Vescovo
* Decimo Cristiani as Antonio
* Ornella Spegni as La vedova Casu
* Memmo Luisi as Antioco
* Marco Guglielmi as Mareddu

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 