The Hairdresser's Husband
 
{{Infobox film
| name           = Le Mari de la coiffeuse
| image          = Le Mari de la coiffeuse.jpg
| caption        = Theatrical release poster
| director       = Patrice Leconte
| producer       = Thierry de Ganay Claude Klotz
| starring       = Jean Rochefort and Anna Galiena
| music          = Michael Nyman
| cinematography = Eduardo Serra
| editing        = Joëlle Hache 
| distributor    = AMLF
| released       =  
| runtime        = 82 minutes
| country        = France
| language       = French
| gross          = $2,150,082
}}
 French film Claude Klotz, and directed by Patrice Leconte|Leconte. Jean Rochefort stars as the title character.  Anna Galiena co-stars.

It won Patrice Leconte the Prix Louis Delluc.  In 1991 it was nominated for "Best Foreign-Language Film" in the British Academy Film Awards.

==Synopsis==
The film begins in a flashback from the titular character, Antoine.  We are introduced to his fixation with female hairdressers which began at a young age.  The film uses flashbacks throughout and there are frequent parallels drawn with the past.  Though Antoine tells Mathilde that the past is dead, his life is evidence that on some level the past repeats itself.  As a young boy he fantasised about a hairdresser who committed suicide and as a man in his 50s he begins an affair with a hairdresser which ends after ten years in her suicide.  However there are differences, Mathilde commits suicide because she is so happy she is afraid of the happiness she has found with Antoine ending.

We are unsure what Antoine has done with his life; we know, however, that he has fulfilled his childhood ambition: to marry a haidresser.  The reality proves to be every bit as wonderful as the fantasy and the two enjoy an enigmatic, enclosed and enchanting relationship.  The final sequence shows Antoine, in the salon, dancing to Eastern music just as he has done throughout his life.  His last line is the enigmatic comment that the hairdresser will return.

==Cast==
*Jean Rochefort as Antoine
*Anna Galiena as Mathilde
*Roland Bertin as Antoines father
*Maurice Chevit as Ambroise Dupré dit Isidore Agopian
*Philippe Clévenot as Morvoisieux
*Jacques Mathou as Mr. Chardon
*Anne-Marie Pisani as Madame Shaeffer

==Reception== Grand Prix of the Belgian Syndicate of Cinema Critics.

==Home media==
The Hairdressers Husband was released on DVD by Umbrella Entertainment in April 2011. The DVD is compatible region code 4.   

==Soundtrack==
{{Infobox album  
| Name        = The Hairdressers Husband
| Type        = soundtrack
| Artist      = Michael Nyman
| Cover       = 
| Released    = September 21, 1992
| Recorded    =
| Genre       = Middle Eastern
| Length      = 
| Label       = Soundtrack Listeners Communications
| Producer    = 
| Chronology  = 
| Last album  = The Michael Nyman Songbook  (1992)
| This album  = The Hairdressers Husband  (1992)
| Next album  = The Essential Michael Nyman Band  (1992)
| Misc        = 
}} composed by Michael Nyman and also contains a great deal of Middle Eastern popular music.  It was first released as an album in Japan by Soundtrack Listeners Communications (SLC) September 21, 1992 and has subsequently been released elsewhere.

==References==
 

==External links==
* 
*  
*  at Rotten Tomatoes
* 

 
 

 
 
 
 
 
 
 
 

 
 