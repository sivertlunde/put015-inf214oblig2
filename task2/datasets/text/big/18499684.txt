Easy Living (1949 film)
 
{{Infobox film
| name           = Easy Living
| caption        = Theatrical release poster
| image	         = Easy Living FilmPoster.jpeg
| director       = Jacques Tourneur
| producer       = Robert Sparks
| writer         = Irwin Shaw Charles Schnee
| starring       = Victor Mature Lucille Ball Lizabeth Scott
| music          = 
| cinematography = Harry J. Wild
| editing        = Frederic Knudtson
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
}} Los Angeles Rams football team.

==Plot== Gordon Jones) is released by the team. Pete gets advance after advance on his salary from Anne (Lucille Ball), the secretary of team owner and coach Lenahan (Lloyd Nolan).

One day, however, he goes secretly to see a doctor (Jim Backus) about various symptoms he has been experiencing and learns that he has a heart condition due to a childhood bout of rheumatic fever, one that could kill him if he continues playing football. He starts to tell his wife Liza (Lizabeth Scott), but changes his mind when she is cool to Holly, whom she describes as a has-been after he is gone.
 Art Baker). The older man is looking to replace his young girlfriend, Billy Duane, and dangles before Liza the prospect of redecorating his apartment. Knowing what he is after, Liza is willing to do whatever it takes to further her ambitions. 

Meanwhile, Pete is bitterly disappointed when his friend, retiring college head coach Virgil Ryan (Everett Glass), informs him that he cannot recommend him as his replacement because Liza is unsuitable for the duties of a coachs wife. Instead, the job is given to Petes teammate and friend, Tim "Pappy" McCarr (Sonny Tufts). Tim offers Pete the position of his assistant, but Pete turns it down. 

Afraid of physical contact, Pete turns in a very poor performance and loses the next game. Lenahan cannot afford another loss if he wants to make the playoffs (and earn $100,000), so he benches Pete in favor of Tim. Tim plays well, and they win their next game.

When Pete proposes taking the assistant coaching position, Liza breaks up with him. However, when she gets dumped by Howard, she tries unsuccessfully to get Pete back. Pete is given another chance at glory when Tim is injured, but ultimately tells his teammates about his condition and walks away from the game. Though Anne has made it clear that she loves him, Pete decides to take Liza back, making it clear, however, that it will be on his terms.

==Cast==
* Victor Mature as Pete Wilson
* Lucille Ball as Anne, Lenahans secretary
* Lizabeth Scott as Liza Wilson
* Sonny Tufts as Tim "Pappy" McCarr
* Lloyd Nolan as Lenahan Paul Stewart as Dave Argus, a reporter
* Jack Paar as Scoop Spooner
* Jeff Donnell as Penny McCarr Art Baker as Howard Vollmer Gordon Jones as Bill "Holly" Holloran
* Don Beddoe as Jaeger
* Richard Erdman as Buddy Morgan (as Dick Erdman) William "Bill" Phillips as Ozzie, the trainer
* Charles Lang as Whitey Kenny Washington as Benny Julia Dean as Mrs. Belle Ryan, Virgils wife
* Everett Glass as Virgil Ryan
* Jim Backus as Dr. Franklin (as James Backus) Robert Ellis as Urchin
* Michael St. Angel as Gilbert Vollmer (as Steven Flagg)
* Alex Sharp as Don
* Russell Thorson as Hunk "Eddie" Edwards (as Russ Thorson)
* June Bright as Billy Duane, who commits suicide when Howard Vollmer discards her
* Edward Kotal as Curly
* Audrey Young as Singer

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 