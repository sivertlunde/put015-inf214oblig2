The Attack of the Giant Moussaka
{{Infobox film
| name           = The Attack of the Giant Moussaka
| image          = Attack of the giant mousaka dvd.jpg
| image_size     = 
| caption        = French theatrical release poster
| director       = Panos H. Koutras
| producer       = Panos H. Koutras
| writer         = Panos Evagellidis Yorgos Korontsis
| starring       = Yannis Aggelakis Myriam Vourou Christos Mantakas Gregory Patrikareas Eugene Dimitriou Themis Bazaka Roubini Vasilakopoulou Jenny Balatsinou Dorothea Mercouri   Cine.gr (Greek)  Konstantinos Vita
| cinematography = Zafiris Epaminondas
| editing        = Elissavet Chronopoulou
| released       = 24 December 1999  
| runtime        = 90 minutes
| country        = Greece
| language       = Greek, English, French,  Japanese, Russian.

}}
The Attack of the Giant Moussaka  ( ;  ; 1999), is a   film festivals, and has achieved cult status. 

==Plot==
The city of Athens is at war with a terrifying gigantic moussaka accidentally produced when an ordinary serving is hit by a ray from an alien space ship. 

==Cast==
*Yannis Aggelakis  as Tara
*Myriam Vourou as Joy Boudala
*Christos Mantakas  as Alexis Alexiou
*Gregory Patrikareas  as Antonis Boudalas
*Eugene Dimitriou as Aris Boudalas
*Themis Bazaka  as Evi Bay
*Roubini Vasilakopoulou as Aleka Spay
*Jenny Balatsinou as doctor 
*Dorothea Mercouri  as Gora
*Hilda Iliopoulou as Daizy Karra
*Nikos Saropoulos as Dinos Dinou
*Michalis Pantos as Dimis
*Maria Kavadia as Chanel

==References==
 

== External links ==
*   Internet Movie Database
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 