Master of the World (1934 film)
 Master of the World}}
{{Infobox film
| name           = Master of the World 
| image          = Masterofworld.jpg
| image_size     = 
| caption        = 
| director       = Harry Piel
| producer       = 
| writer         = Georg Mühlen-Schulte
| narrator       = 
| starring       = Walter Janssen Sybille Schmitz Aribert Waescher Willi Schur
| music          = Fritz Wenneis
| cinematography = Ewald Daub
| editing        = 
| distributor    = 
| released       = 1934
| runtime        = 90 minutes
| country        = Nazi Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

The Master of the World ( ) is a  . Accessed 2008-02-07.) 

==Synopsis==
Walter Franck, playing "Wolf", as the half-crazy assistant to Walter Hanssens "Dr. Heller", an inventor of robots, murders his master, and attempts to take over the world with his death-ray equipped robots. He is prevented from attaining his goal by Siegfried Schuerenbergs "Baumann", a mining engineer, and dies at the hands of the robot creations. 

In the happy end of the movie, robots have been transformed into industrial laborers, thus freeing humans for a more worthy and humane life of working on small farms.

==Cast==
* Walter Janssen as Dr. Heller
* Sybille Schmitz as Vilma, seine Frau
* Walter Franck as Prof. Wolf
* Aribert Wäscher as Geheimrat Ehrenberg
* Siegfried Schürenberg as Werner Baumann
* Willi Schur as Karl Schumacher, Steiger
* Gustav Püttjer as Becker, Bergmann Klaus Pohl as Stöppke, Bergmann
* Oskar Höcker as Luppe, Bergmann
* Max Gülstorff as Neumeier, Berginspektor
* Otto Wernicke as Wolter, Oberingenieur
* Hans Hermann Schaufuß as Fischer, Bürodirektor

==References==
 

==External links==
*  at the Internet Movie Database
*  

 
 
 
 
 
 
 
 


 