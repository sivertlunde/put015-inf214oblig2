Magic Carpet Ride (film)
{{Infobox film
| name           = Magic Carpet Ride
| image          = MagicCarpetRide.jpg
| alt            =
| caption        = Theatrical Poster
| director       = Yılmaz Erdoğan
| producer       = Necati Akpinar
| writer         = Yılmaz Erdoğan
| starring       =   
| music          = Ozan Çolakoğlu
| cinematography = Uğur İçbak
| editing        = Mustafa Presheva
| studio         = Beşiktaş Kültür Merkezi
| distributor    =
| released       =  
| runtime        = 114 minutes
| country        = Turkey
| language       = Turkish
| budget         =
| gross          =
}}

Magic Carpet Ride (A.K.A.: Organized Jobs;  ) is a 2005 Turkish comedy film, written and directed by Yılmaz Erdoğan, about a small-time criminal who accidentally recruits a failed comedy Superman impersonator into his gang. The film, which went on nationwide release on  , is the first Turkish production to be shot in 1:2, 35mm CinemaScope format.

==Production==
The film was shot on location in Istanbul, Turkey.   

==Plot==
Asim Noyan and his gang make up a rambling collective, which concerns itself with a range of criminal activity, running from car theft to fraud. An inveterate womaniser, Asim meets the failed comedian Superman impersonator Samet while fleeing from an angry husband. Desperate Samet finds himself unwittingly implicated in the life of the gang. Meanwhile Umut, the daughter of Mr and Mrs Ocak, a highly literate but hard up couple, leads an altogether different life. The ways of Samet and Umut and Asims gang clash around a stolen car.

==See also==
* 2005 in film

==References==
 

==External links==
*   
*  

 
 
 
 
 
 

 
 