Håkan Bråkan & Josef
{{Infobox film
| name           = Håkan Bråkan & Josef
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Erik Leijonborg
| producer       = 
| writer         = 
| screenplay     = Lars Lundström based on Anders Jacobsson and Sören Olsson books
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          =  SF
| editing        = 
| studio         = 
| distributor    =  2004
| runtime        = 
| country        = Sweden Swedish
| budget         = 
| gross          = 
}} Swedish 2004 2004 comedy Sune spin-off spinoff character.

==Actors==
*Axel Skogberg - Håkan Bråkan|Håkan Jonas Karlsson - Josef
*Leo Holm - Sune Per Svensson - Rudolf
*Tintin Anderzon - Karin
*Emma Engström - Anna
*Ella Widmark - Isabelle
*Johan Rheborg - Ragnar
*Petter Westerlund - Ronny
*Henrik Hjelt - Larsa
*Rolf Skoglund -  Zoo shop cashier
*Miranda Nord - Sophie
*Bisse Unger - Pär Henrik Lundström - Ludde
*Rachel Mohlin - choirleader

==Recording==
Recordings began in the Stockholm district by late-August 2003, only to later continue in Trollhättan. 

==Soundtrack==
The original film soundtrack was released to CD in 2004 in music|2004. 

==Opening dates==
Opening dates 

{| class="wikitable" Country
!align="center"|Cinema opening date
|- Finland
|align="center"|27 2005
|- Norway
|align="center"|29 April 2005
|- Sweden
|align="center"|8 2004
|- USA
|align="center"|11 2006 (Portland International Film Festival)
|}

==Home video== 2005 the film was released to DVD and VHS. 

==References==
 

==External links==
*   

 
 
 
 
 
 
 
 
 