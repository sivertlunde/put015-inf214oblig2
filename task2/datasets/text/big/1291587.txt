Vanity Fair (2004 film)
 
{{Infobox film  name = Vanity Fair image = Vanity Fair 2004 poster.jpg caption =  director = Mira Nair  writer = William Thackeray  (novel)  starring = Reese Witherspoon Jonathan Rhys Meyers James Purefoy Gabriel Byrne Eileen Atkins Bob Hoskins Romola Garai Jim Broadbent producer = Janette Day  music = Mychael Danna editing = Allyson C. Johnson distributor = Focus Features  released =         runtime = 141 minutes  language = English country = United Kingdom United States budget = $23 million gross = $19,463,185
}} of the numerous television Becky Sharp.

The film was nominated for "Golden Lion" Award in 2004 Venice Film Festival.

==Plot==
  Rebecca "Becky" Sharp (Angelica Mandy), then a girl of ten, insists on having ten guineas, instead of four guineas, as the price of the painting. The painter explains that the model in the painting is Beckys mother, the painters late wife. Steyne agrees to pay the higher amount and leaves with the painting. The young Becky is seen moving to Miss Pinkertons Academy for Young Ladies after her fathers death.

The now-adult Becky Sharp (Reese Witherspoon) is preparing to leave the academy for a position as a governess. She travels by carriage to her new position, stopping on the way at the home of her best friend Amelia Sedley (Romola Garai) and her family. Amelia lives in a large estate as the daughter of a gentleman, though her father is not as wealthy as he appears. 
Becky meets Amelias brother Joseph "Jos" Sedley (Tony Maudsley), whos stationed in India. Jos finds that Becky is interested in India. During an outing at Vauxhall, Becky meets Amelias inattentive boyfriend, the dashing and self-obsessed Captain George Osborne (Jonathan Rhys Meyers), and his best friend, Captain William Dobbin (Rhys Ifans), who is secretly in love with Amelia. Jos is smitten with Becky, but in a private discussion, George convinces Jos to break off his attentions to the penniless girl. George is concerned that his father, a rich businessman whos a commoner, wont let him marry Amelia if Becky has also married into the Sedley family.

Becky enters the service of baronet Sir Pitt Crawley (Bob Hoskins), as a governess to his daughters. Sir Pitt has two sons, the pompous and pedantic elder son (Douglas Hodge), who also bears the name Pitt Crawley, and the dashing younger son, Captain Rawdon Crawley (James Purefoy). Sir Pitt is impressed with Beckys service, especially as Becky cleans up Sir Pitts home to welcome his elder half-sister, Miss Matilda Crawley (Eileen Atkins). Miss Crawley is impressed with Becky, when she finds that Becky speaks French. Becky explains that her mother was French while her father was an artist. Miss Crawley takes Becky to live with her in London. In London, Becky begins to see Captain Rawdon Crawley. Sir Pitt shows up in London. His wife has died, and he asks Becky to marry him. Becky refuses, revealing that shes already secretly married to Captain Rawdon Crawley. The misalliance so enrages Miss Crawley that she throws Becky out and refuses to see the couple. Becky and Rawdon live in an apartment in London, and Becky reveals that shes pregnant, and suggests that the baby may help to reconcile them with Miss Crawley.

Meanwhile, Amelias prospective father-in-law, Mr. Osborne (Jim Broadbent), is trying to arrange a new marriage for his son George. He introduces George to a young woman whose father made a fortune in Jamaica. George objects to the marriage as the woman is of mixed race. He insists that hes betrothed to Amelia, but Mr. Osborne insists that George marry the woman from Jamaica.

Amelias father, John, goes bankrupt, and the Sedley household goods are sold at auction. Lord Steyne  buys a landscape painted by Beckys father. Dobbin buys Amelias piano and gives it back to her, but Amelia thinks it is a gift from George. George then elopes with Amelia, and Mr. Osborne disinherits him.
 Duke of Wellingtons army. Becky and Amelia decide to accompany their husbands.

The newly-wedded Osborne is growing tired of Amelia, and he becomes increasingly attracted to Becky. At a ball in Brussels (based on the famous Duchess of Richmonds ball on the eve of the Battle of Waterloo), George gives Becky a note which she hides in her bodice. Amelia also attends the ball, but she becomes sick as the result of pregnancy. Rather than attend to his wife, George spends the evening dancing with Becky; Dobbin attends to Amelia instead. The ball is interrupted by an announcement that Napoleon has attacked, and the officers are given three hours to report to their units.

Amelia has been deeply hurt by her husbands attentions toward Becky, but Becky reassures her that George would do nothing to hurt her. That night, Rawdon discusses his finances with Becky, giving her all the money hes won at cards. The next day, Becky tries to flee the city. One of the other Englishmen wants to buy her horse, and she agrees provided he gives her a seat in his carriage. However, when Becky sees Amelia in the fleeing mob, she leaves the carriage to take Amelia back to Brussels where they wait out the battle.

In the ensuing Battle of Waterloo, George is killed. Amelia bears him a posthumous son, who is also named George. Mr. Osborne refuses to acknowledge his grandson. So Amelia returns to live in genteel poverty with her parents. Now-Major William Dobbin, who is young Georges godfather, begins to express his love for the widowed Amelia by small kindnesses. Amelia is too much in love with Georges memory to return Dobbins affections. Saddened, he transfers to an army post in India, to which Jos has already returned. Meanwhile, Becky also has a son, also named after his father. However, the army has been demobilized, and Rawdon, although now a colonel, has no income.

Several years pass, Miss Matilda Crawley and Sir Pitt Crawley have died, leaving the family fortune in the hands Sir Pitt the Younger. Rawdon and Becky reconcile with the family, but it does little to help their finances. Rawdon is now in debt due to his gambling. However, Lord Steyne comes to the rescue. Steyne offers to re-introduce Becky into London society. At her first outing, however, all the women ignore her until Lady Steyne takes pity on her and asks her to sing a song. Beckys singing attracts the admiration of both sexes, and shes now accepted.
Steyne arranges to send Beckys son, now around nine, to boarding school so he can have more time with her. Amelia agrees to turn her son over to her father-in-law who can give him a luxurious upbringing. Steyne arranges a bizarre entertainment for the king at which the wives of the lords perform a belly dance, with Becky as the featured dancer, which attracts the king.

Rawdon is thrown into debtors prison. Steyne tells Becky that he will arrange for Rawdons bail, but not until the following day. Hes price for Rawdons freedom is for her to sleep with him. Becky refuses, but Steyne tries to force himself on her. Rawdon returns at that moment, his debts having been paid by his family. He beats Lord Steyne and throws him down the stairs. However, Rawdon refuses to believe Becky when she insists that Steyne forced his attentions on her, and he leaves her. The army offers him a post as the garrison commander on Coventry Island, which he accepts.

Twelve years later, Becky is working as a card dealer at a casino in Baden-Baden in Germany. She meets the young George Osborne at a card table, who is traveling with his mother and Major William Dobbin. Dobbin tells Amelia he believes Becky killed Rawdon, but Amelia points out that Rawdon died of tropical fever.

Although Amelia is traveling with Dobbin, she still refuses to return his affections. Becky tells Amelia that she shouldnt be clinging to the memory of her late husband because George wasnt the saint she thought he was. To prove her point, Becky shows Amelia the letter that George had given her on the eve of Waterloo. Jos, who has been traveling with Major William Dobbin, is still enchanted by Becky. Becky marries Jos, and he takes her to India, where she had wanted to go.

===Alternate ending===
Becky Sharp arrives at her brother-in-laws house (Pitt Crawley). As Becky nears the house a servant questions her and tells her that the funeral of Sir Pitt Crawley is being held there. While talking to the servant, Becky spots a young man and asks the servant who it is. He replies: the new baronet, Sir Rawdon Crawley (Robert Pattinson), Beckys son. Sir Rawdon notices her and asks if there is any way he may help. Thinking that he doesnt know who she is, Becky replies that she isnt sure he would want to help her if he knew who she was. Rawdon reveals that he knows who she is, and when Becky tries to reach out for her son, he steps back. Becky then tries to convince him that she still loves him, even though she abandoned him. Lady Jane appears, in mourning, and Rawdon exclaims that "she is here to claim me as her own." Lady Jane replies that he is. Rawdon says that it is too late for her to play the mother now, before he turns and leaves. Lady Jane explains to Becky what has happened and leads her to her late husbands grave, where she makes a speech about "love is vanitys conqueror." During the speech, Beckys son stands behind her and takes her hand in his. The scene ends with Becky repeating Lady Janes words.

==Cast== Rebecca "Becky" Sharp Crawley
*Angelica Mandy as a young Becky Sharp
*Romola Garai as Amelia Sedley Osborne
*Sophie Hunter as Maria Osborne
*James Purefoy as Colonel Rawdon Crawley
*Jonathan Rhys-Meyers as Captain George Henry Osborne
*Rhys Ifans as Major William Dobbin
*Eileen Atkins as Miss Matilda Crawley
*Geraldine McEwan as the Countess of Southdown
*Gabriel Byrne as the Marquess of Steyne
*Bob Hoskins as Sir Pitt Crawley the Elder
*Douglas Hodge as Sir Pitt Crawley the Younger
*Natasha Little as Lady Jane Sheepshanks Crawley
*John Woodvine as Lord Bareacres
*Barbara Leigh-Hunt as Lady Bareacres Nicholas Jones as Lord Darlington
*Sian Thomas as Lady Darlington
*Trevor Cooper as General Tufto
*Kelly Hunter as the Marchioness of Steyne
*Camilla Rutherford as Lady Gaunt
*Alexandra Staden as Lady George
*Jim Broadbent as Mr. Osborne
*Tony Maudsley as Joseph "Jos" Sedley
*John Franklyn-Robbins as Mr. John Sedley
*Deborah Findlay as Mrs. Mary Sedley Daniel Hay as little George "Georgy" Osborne the Younger
*Tom Sturridge as a young George "Georgy" Osborne the Younger
*Kathryn Drysdale as Rhoda Swartz
*Ruth Sheen as Miss Pinkerton
*Richard McCabe as The King
*Gledis Cinque as an older Celia Crawley
* 
|date=April 22, 2011}} 

==Production==
The film adaptation of Vanity Fair had been in development for over 10 years, with writers Matthew Faulk and Mark Skeet working on the screenplay. Mira Nair became attached to the project in 2002 and scrapped most of the initial screenplay. She brought Julian Fellowes in to rewrite the film; he agreed with her that the character of Becky Sharp should be made more sympathetic than in the novel.  The ending was also changed, with Becky journeying to India with Joseph Sedley. The film had a budget of $23 million and originally was supposed to be in pre-production for 18 weeks. However, Reese Witherspoon became pregnant so it was necessary to speed up both pre-production and filming. Vanity Fair was shot in Bath, Somerset|Bath, Kent, the Chatham Dockyard,  and at Stanway House in Gloucester.   

==Reception==
Critics gave the film mixed reviews. Metacritic reported the film had an average score of 53 out of 100, based on 41 reviews.  Rotten Tomatoes maintains this film with a 51% rating.

Stephen Hunter of The Washington Post gave a positive review, calling the movie "Mira Nairs fine movie version of the 1848 book, in all its glory and scope and wit."  In the Charlotte Observer, Lawrence Toppman commented that "The filmmakers have wisely retained the main structure of the book" and that "The cast is uniformly good, even when dealing with sudden mood changes forced by the screenwriters need to move forward."  Meanwhile, Lisa Schwarzbaum, in her review in Entertainment Weekly, rated the film a B-, and added that the film "borders on perky – a duller, safer tonal choice for the story of a conniving go-getter whose fall is as precipitous as her rise." 

==Soundtrack==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Song !! Artist(s)!! Music !! Duration
|- Sissel Kyrkjebø||Mychael Danna||1:59
|- Nicholas Dodd||Mychael Danna||2:10
|- Nicholas Dodd||Mychael Danna||1:26
|- Custer Larue||Mychael Danna||2:05
|- Nicholas Dodd||Mychael Danna||1:43
|- Nicholas Dodd||Mychael Danna||1:08
|- Nicholas Dodd||Mychael Danna||2:48
|- Nicholas Dodd||Mychael Danna||1:35
|- Nicholas Dodd||Mychael Danna||0:28
|- Nicholas Dodd||Mychael Danna||2:03
|- Nicholas Dodd||Mychael Danna||2:00
|- Nicholas Dodd||Mychael Danna||1:38
|- Nicholas Dodd||Mychael Danna||1:14
|- Nicholas Dodd||Mychael Danna||3:11
|- Nicholas Dodd||Mychael Danna||2:37
|- Nicholas Dodd||Mychael Danna||1:28
|- Nicholas Dodd||Mychael Danna||2:03
|- Custer Larue||Mychael Danna||2:45
|- Nicholas Dodd||Mychael Danna||1:11
|- Hakim (Egyptian Mychael Danna||1:33
|- Nicholas Dodd||Mychael Danna||0:38
|- Nicholas Dodd||Mychael Danna||0:46
|- Nicholas Dodd||Mychael Danna||1:06
|- Nicholas Dodd||Mychael Danna||1:13
|- Gori Re Richa Sharma Richa Sharma, Shankar Mahadevan||Shankar-Ehsaan-Loy||4:26
|-
|}

==Accolades==
  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| 2004 Venice Film Festival
| Golden Lion
| Mira Nair
|  
|-
| rowspan="3"| 2005
| rowspan="2"| Satellite Award Best Art Direction and Production Design
| Maria Djurkovic and Tatiana Macdonald 
|  
|- Best Costume Design
| Beatrix Aruna Pasztor  
|  
|-
| London Critics Circle Film Awards British Supporing Actress of the Year
| Eileen Atkins
|  
|}
 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 