Thin Ice (2013 film)
 
 

Thin Ice  is a 2013 documentary film following geologist Simon Lamb on a search to understand the science behind climate change.  This is achieved by traveling the world and meeting a range of scientists, from biologists to physicists, who are investigating the climate. The films conclusion emphasises the scientific consensus on human-induced climate change.

The film was a joint initiative between Oxford University and Victoria University of Wellington, and premiered around the world on 22 April 2013, which is Earth Day.

==References==
 

==External links==
*  , from where the film can be viewed and a DVD purchased.

 
 
 
 
 

 