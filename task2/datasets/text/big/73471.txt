Flash Gordon (serial)
{{Infobox film
| name           = Flash Gordon
| image          = Flash Gordon (serial).jpg Ray Taylor (uncredited)
| producer       = Henry MacRae
| writer         = Basil Dickey Ella ONeill George H. Plympton Frederick Stephani Alex Raymond (based on the comic strip by)
| starring       = Buster Crabbe Jean Rogers Charles B. Middleton Priscilla Lawson Frank Shannon Richard Fryer Universal Pictures
| released       = 6 April 1936
| runtime        = 245 min (13 episodes)
| country        = United States
| language       = English
| budget         = $350,000 
}}
 Mongo and his encounter with the evil Emperor Ming the Merciless. Buster Crabbe, Jean Rogers, Charles B. Middleton, Priscilla Lawson and Frank Shannon played the central roles. In 1996, Flash Gordon was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Synopsis==

# The Planet of Peril
#: The planet Mongo is on a collision course with Earth. Dr. Alexis Zarkov takes off in a rocket ship to Mongo, with Flash Gordon and Dale Arden as his assistants. They find that the planet is ruled by the cruel Emperor Ming, who lusts after Dale and sends Flash to fight in the arena. Mings daughter, Princess Aura, tries to spare Flashs life.
# The Tunnel of Terror
#: Aura helps Flash to escape as Zarkov is put to work in Mings laboratory and Dale is prepared for her wedding to Ming. Flash meets Prince Thun, leader of the Lion Men, and the pair return to the palace to rescue Dale.
# Captured by Shark Men
#: Flash stops the wedding ceremony, but he and Dale are captured by King Kala, ruler of the Shark Men and a loyal follower of Ming. At Mings order, Kala forces Flash to fight with a giant octosak in a chamber filling with water.
# Battling the Sea Beast
#: Aura and Thun rescue Flash from the octosak. Trying to keep Flash away from Dale, Aura destroys the mechanisms that regulate the underwater city.
# The Destroying Ray
#: Flash, Dale, Aura and Thun escape from the underwater city, but are captured by King Vultan and the Hawkmen. Dr. Zarkov befriends Prince Barin, and they race to the rescue.
# Flaming Torture
#: Dale pretends to fall in love with King Vultan in order to save Flash, Barin and Thun, who are put to work in the Hawkmens Atom Furnaces.
# Shattering Doom
#: Flash, Barin, Thun and Zarkov create an explosion in the atomic furnaces.
# Tournament of Death
#: Dr. Zarkov saves the Hawkmens city from falling, earning Flash and his friends King Vultans gratitude. Ming insists that Flash fight a Tournament of Death against a masked opponent, revealed to be Barin, and then a vicious orangopoid.
# Fighting the Fire Dragon
#: Flash survives the tournament with Auras help, after she discovers the weak point of the orangopoid. Still determined to win Flash, Aura has him drugged to make him lose his memory.
# The Unseen Peril
#: Flash recovers his memory. Ming is determined to have Flash executed.
# In the Claws of the Tigron
#: Zarkov invents a machine that makes Flash invisible. Flash torments Ming and his guards. Barin hides Dale in the catacombs, but Aura has her tracked by a tigron.
# Trapped in the Turret
#: Aura realizes the error of her ways, and falls in love with Barin. She tries to help Flash and his friends to return to Earth&nbsp;— but Ming plots to kill them.
# Rocketing to Earth
#: Ming orders that the Earth people be caught and killed, but Flash and his friends escape from the Emperors clutches, and Ming is apparently killed in the flames of the "sacred temple of the Great God Tao". Flash, Dale and Zarkov make a triumphant return to Earth. 

==Cast==

* Buster Crabbe as Flash Gordon. Crabbe had his hair dyed blond in order to appear more like the comic strip Flash Gordon. He was very self-conscious about this and kept his hat on in public at all times, even with women present. He did not like men whistling at him. 
* Jean Rogers as Dale Arden. Rogers also had her hair dyed blonde, "apparently to capitalize on the popularity of Jean Harlow". Both the actress and the character were normally brunettes. 
* Charles B. Middleton as Ming the Merciless. Ming is characterised in the mould of Fu Manchu in this serial. 
* Priscilla Lawson as Princess Aura
* Frank Shannon as Hans Zarkov|Dr. Alexis Zarkov Richard Alexander as Prince Barin. Alexander helped to design his own costume, which included a leather chest plate painted gold. 
* Jack Lipson as Prince Vultan
* Theodore Lorch as Second High Priest
* James Pierce as Prince Thun. "Big Jim" Pierce played Tarzan in Tarzan and the Golden Lion. According to Tarzan creator Edgar Rice Burroughs he was the perfect portrayal of Tarzan. He married Burroughs daughter Joan Burroughs on 8 August 1928 after meeting her on the set of that film. As a wedding present, Burroughs included a clause in his next contract that stated that Pierce must play Tarzan. This contract later led to the filming of the serial Tarzan the Fearless. Despite the contract Pierce did not star after he was tricked into stepping aside in favor of fellow Flash Gordon actor Buster Crabbe (the first serial role for the future "King of the Serials"). 
* Duke York as King Kala
* Earl Askam as Officer Torch
* Lon Poff as First High Priest Richard Tucker as Professor Gordon
* George Cleveland as Professor Hensley
* Muriel Goodspeed as Zona

==Production==

According to Harmon and Glut, Flash Gordon had a budget of over a million dollars.  Stedman, however, writes that it was "reportedly" $350,000. 
 The Mummy The Invisible Ray (1936). Zharkovs rocket ship and scenes of dancers swarming over a gigantic idol were reused from Just Imagine (1930). Mings attack on Earth used footage from old silent newsreels. An entire dance segment from The Midnight Sun (1927) was used.  and much laboratory equipment came from Bride of Frankenstein (1935). The music was recycled from several other films, notably Bride of Frankenstein,  Bombay Mail, The Black Cat (both 1934), and The Invisible Man (1933).

Exterior shots, such as the crew from Earths first steps on Mongo, were filmed at Bronson Canyon. 

Crash Corrigan, who would later be the lead in other serials, wore a modified gorilla suit as the "Orangapoid". 

Flash Gordon was intended to regain an adult audience for serials.  It was shown in A Theaters in large cities across the United States. Many newspapers, including some not carrying the Flash Gordon comic strip, contained half and three-quarter page feature stories in their entertainment pages with Alex Raymond drawings and stills from the serial. 

Flash Gordon was the first outright science fiction serial, although earlier serials had contained science fiction elements such as gadgets. Six of the fourteen serials released within five years of Flash Gordon were science fiction. 

The serial film was subsequently released as a 72-minute feature named Rocket Ship. Alternate titles for this film include Spaceship to the Unknown and Atomic Rocketship. The TV version was named Space Soldiers. 

===Stunts===

* Eddie Parker doubled Buster Crabbe. 

==References==

{{Reflist
| colwidth = 30em
| refs =
 
{{cite journal
| last = Tracey
| first = Grant
| title = Images Journal Flash Gordon article
| work = ImagesJournal.com
| publisher = Images Journal
| issue = 4
| url = http://www.imagesjournal.com/issue04/infocus/flashgordon.htm
| accessdate = 2010-08-02
}}
 
 
{{cite book
| last = Harmon
| first = Jim
|author2=Donald F. Glut 
| authorlink = Jim Harmon
| title = The Great Movie Serials: Their Sound and Fury
| publisher = Routledge
| isbn = 978-0-7130-0097-9
| pages = 29–35, 38
| chapter = 2. "We Come from Earth, Dont You Understand?"
| year = 1973
}}
 
 
{{cite book
| last = Essoe
| first = Gabe
| title = Tarzan of the Movies
| publisher = Citadel Press
| isbn = 978-0-8065-0295-3
| pages = 56–7, 77
| chapter =
| year = 1972
}}
 
 
{{cite book
| last = Stedman
| first = Raymond William
| title = Serials: Suspense and Drama By Installment
| publisher = University of Oklahoma Press
| isbn = 978-0-8061-0927-5
| pages = 97–100, 102
| chapter = 4. Perilous Saturdays
| year = 1971
}}
 
 
{{cite book
| last = Cline
| first = William C.
| title = In the Nick of Time
| publisher = McFarland & Company, Inc.
| isbn = 0-7864-0471-X
| page = 17
| chapter = 2. In Search of Ammunition
| year = 1984
}}
 
 
{{cite book
| last = Cline
| first = William C.
| title = In the Nick of Time
| publisher = McFarland & Company, Inc.
| isbn = 0-7864-0471-X
| page = 32
| chapter = 3. The Six Faces of Adventure
| year = 1984
}}
 
 
{{cite book
| last = Reid
| first = John Howard
| year = 2007
| title = Science-fiction & Fantasy Cinema: Classic Films of Horror, Sci-fi & the Supernatural
| pages = 71–72
| publisher = Lulu.com
| isbn = 1-4303-0113-9
}}
 

}}

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
   
 
 
 
 
 