For Your Eyes Only (film)
 
 
{{Infobox film
| name = For Your Eyes Only
| image = For Your Eyes Only - UK cinema poster.jpg
| caption = British cinema poster for For Your Eyes Only, designed by Bill Gold
| starring = Roger Moore Julian Glover Carole Bouquet Chaim Topol Lynn-Holly Johnson
| alt= A graphic, taking up three quarters of the image, on black background with the bottom quarter in red. Above the picture are the words "No one comes close to JAMES BOND 007". The graphic contains a stylised pair of womens legs and buttocks in the foreground: a pair of bikini bottoms cover some of the bottom. The woman wears high heels and is carrying a crossbow in her right hand. In the distance, viewed between her legs, a man in a dinner suit is seen side on, carrying a pistol. In the red, below the graphic, are the words: "Roger Moore as Ian Flemings James Bond 007 in FOR YOUR EYES ONLY".
| based on =  
| screenplay = Michael G. Wilson Richard Maibaum John Glen
| producer = Albert R. Broccoli
| cinematography = Alan Hume
| music = Bill Conti
| editing = John Grover
| studio = Eon Productions
| distributor = United Artists
| released =  
| runtime = 127 minutes
| country = United Kingdom
| language = English
| budget = $28 million
| gross = $194.9 million
}} James Bond MI6 agent James Bond. John Glen, who had worked as editor and second unit director in three other Bond films.
 Melina Havelock, Live and On Her Majestys Secret Service.

After the science fiction-focused Moonraker (film)|Moonraker, the producers wanted a conscious return to the style of the early Bond films and the works of 007 creator Fleming. For Your Eyes Only followed a grittier, more realistic approach, and an unusually strong narrative theme of revenge and its consequences. Filming locations included Greece, Italy, Spain and England, with underwater footage being shot in The Bahamas.

For Your Eyes Only was released on 24 June 1981 to a mixed critical reception; the film was a financial success, generating $195.3 million worldwide. This was the final Bond film to be distributed solely by United Artists; the studio merged with Metro-Goldwyn-Mayer soon after this films release.

==Plot== Polaris submarines, Minister of Defence, Sir Frederick Gray and MI6 Chief of Staff, Bill Tanner, to retrieve the ATAC before the Soviets, as the transmitter could order attacks by the submarines Polaris ballistic missiles.

The head of the KGB, General Gogol has also learnt of the fate of the St Georges and already notified his contact in Greece. A marine archaeologist, Sir Timothy Havelock, who had been asked by the British to secretly locate the St Georges, is murdered with his wife by a Cuban hitman, Hector Gonzales. Bond goes to Spain to find out who hired Gonzales.
 Melina Havelock, Second World figure skater Bibi Dahl, to a biathlon course, a group of three men, which includes East German biathlete Eric Kriegler, chases Bond, trying to kill him. Bond escapes, and then goes with Ferrara to bid Bibi farewell in an indoor ice rink, where he fends off another attempt on his life by men in hockey gear. Ferrara is killed in his car, with a dove pin in his hand. Bond then travels to Corfu in pursuit of Columbo.

There, at the casino, Bond meets with Kristatos and asks how to meet Columbo, not knowing that Columbos men are secretly recording their conversation. After Columbo and his mistress, Countess Lisl von Schlaf, argue, Bond offers to escort her home with Kristatos car and driver. The two then spend the night together. In the morning Lisl and Bond are ambushed by Locque and Lisl is killed.  Bond is captured by Columbos men before Locque can kill him; Columbo then tells Bond that Locque was actually hired by Kristatos, who is working for the KGB to retrieve the ATAC. Bond accompanies Columbo and his crew on a raid on one of Kristatos opium-processing warehouses in Albania, where Bond uncovers naval mines similar to the one that sank the St Georges, suggesting it was not an accident. After the base is destroyed, Bond chases Locque and kills him.

Afterwards, Bond meets with Melina, and they recover the ATAC from the wreckage of the St Georges, but Kristatos is waiting for them when they surface and he takes the ATAC. After the two escape an assassination attempt, they discover Kristatos   point when Melinas parrot repeats the phrase "ATAC to St Cyrils". With the help of Columbo and his men, Bond and Melina break into St Cyrils, an abandoned mountaintop monastery. While Bond is climbing, Apostis attacks him, but is killed. As Columbo confronts Kristatos, Bond kills the biathlete Kriegler.

Bond retrieves the ATAC system and stops Melina from killing Kristatos after he surrenders. Kristatos tries to kill Bond with a hidden flick knife, but is killed by a knife thrown by Columbo; Gogol arrives by helicopter to collect the ATAC, but Bond destroys it first. Bond and Melina later spend a romantic evening aboard her fathers yacht.

==Cast== James Bond: MI6 agent 007, who is sent to retrieve a stolen "ATAC" system that could be misused for controlling British military submarines. For Your Eyes Only was the fifth of seven outings for Moore as Bond. Melina Havelock: The daughter of marine archaeologists who are murdered while tracking down the ATACs whereabouts. Bouquet had auditioned for the role of Holly Goodhead in Moonraker, but was unsuccessful. 
*  , eventually losing out to Moore.  Ferrari 125, which Fleming admired.  Topol suggested the pistachios as a trademark of the character, which are used in a scene to orient Columbos men on where to shoot. 
*  . 
* Michael Gothard as Emile Leopold Locque: A Belgian hired killer and associate of Kristatos.
*  , and the couple lunched with the films producer Albert Broccoli during filming. 
* John Wyman as Erich Kriegler: An East German Olympic class athlete and Kristatos henchman/KGB contact. Writer Jeremy Black said that he resembles Hans of You Only Live Twice and Stamper of Tomorrow Never Dies. 
* Desmond Llewelyn as Q (James Bond)|Q, the head of MI6s technical department. For Your Eyes Only was the tenth of 17 Bond films in which Llewelyn appeared. He appeared in more Bond films than any other actor  and worked with the first five Bond actors in the Eon-produced series.  Jill Bennett as Jacoba Brink: Bibis skating coach.
* Jack Hedley as Sir Timothy Havelock: A marine archaeologist hired by the British Secret Service to secretly locate the wreck of St. Georges.
* Lois Maxwell as Miss Moneypenny, Ms secretary. Maxwell played Moneypenny in fourteen Eon-produced Bond films from Dr. No (film)|Dr. No in 1962 to A View to a Kill in 1985; For Your Eyes Only was her twelfth appearance.
*  , a minister in the British government. The role, along with Bill Tanner as Chief of Staff, was used to brief Bond in place of M (James Bond)|M, following the death of Bernard Lee.  The Man with the Golden Gun, although in an un-credited capacity. Villiers presumed he would play the role of M in subsequent films and was disappointed not to be asked; the producers thought him too young for the role and wanted an actor in his 70s.  Luigi Ferrara: 007s MI6 contact in northern Italy.
*   and Moonraker (film)|Moonraker.
* Jack Klaff as List of James Bond henchmen in For Your Eyes Only#Apostis|Apostis: One of Kristatoss henchmen and chauffeur. Hector Gonzales: A Cuban hitman hired by Kristatos to kill the Havelocks.
* Charles Dance as Claus, an associate of Locque. The role was early in Dances career;  in 1989 he would play Ian Fleming in Anglia Televisions Goldeneye: The Secret Life of Ian Fleming, a dramatised portrayal of the life of Ian Fleming.  John Wells as Denis Thatcher|Denis.
* John Hollis as the "bald villain in wheelchair",  voiced by Robert Rietti.  The character appears in the pre-credits sequence and is both unnamed and uncredited. The character contains a number of characteristics of Ernst Stavro Blofeld,  but could not be identified as such because of the legal reasons surrounding the Thunderball (novel)#Controversy|Thunderball controversy with Kevin McClory claiming sole rights to the Blofeld character, a claim disputed by Eon.  Bob Simmons, who previously portrayed Bond in the gun barrel sequences in the first three films and SPECTRE agent Colonel Jacques Bouvar in Thunderball (film)|Thunderball, cameos as another villain as Gonzales henchman who falls victim to Bonds exploding Lotus Esprit|Lotus. 

==Production==
  John Glen was promoted from his duties as a film editor to director, a position he would occupy for four subsequent films.  The transition in directors resulted in a harder-edged directorial style, with less emphasis on gadgetry and large action sequences in huge arenas (as was favoured by Lewis Gilbert).  Emphasis was placed on tension, plot and character in addition to a return to Bonds more serious roots,  whilst For Your Eyes Only "showed a clear attempt to activate some lapsed and inactive parts of the Bond mythology." 
 Lotus blows Pennies from Heaven, Peter Lamont, who had worked in the art department since Goldfinger (film)|Goldfinger, was promoted to production designer. Following a suggestion of Glen, Lamont created realistic scenery, instead of the elaborate set pieces for which the series had been known. 

===Writing=== For Your Eyes Only.  Another set-piece from the novel of Live and Let Die – the keelhauling – which was unused in the film of the same name, was also inserted into the plot.  Other ideas from Fleming were also used in For Your Eyes Only, such as the Identigraph, which come from the novel Goldfinger, where it was originally called the "Identicast".  These elements from Flemings stories were mixed with a Cold War story centred on the MacGuffin of the ATAC.  An initial treatment for “For Your Eyes Only” was submitted by Ronald Hardy, an English novelist and screenwriter in 1979. Hardy’s treatment included the involvement of a character named Julia Havelock whose parents were assassinated by a man named Gonzales. 

For Your Eyes Only is noted for its pre-title sequence, described variously as either "out-of place and disappointing"  or "roaringly enjoyable".  The scene was shot in order to introduce a potential new Bond to audiences, thus linking the new actor to elements from previous Bond films  (see For Your Eyes Only (film)#Casting|casting, below).

The sequence begins with Bond laying flowers at the grave of his wife,  , who owned the film rights to Thunderball (novel)|Thunderball, which supposedly includes the character Ernst Stavro Blofeld, the organisation SPECTRE, and other material associated with the development of Thunderball.  Eon disputed McClorys ownership of the Blofeld character, but decided not to use him again: the scene was "a deliberate statement by Broccoli of his lack of need to use the character." 

===Casting=== The Spy The Professionals; Michael Billington, The Spy the eponymous spy in the British TV series of Quiller (TV series)|Quiller  (Jayston eventually played Bond in a BBC Radio production of You Only Live Twice in 1985).  Eventually, however, this came to nothing, as Moore signed on to play Bond once again.

Bernard Lee died in January 1981, after filming had started on For Your Eyes Only, but before he could film his scenes as M (James Bond)|M, the head of MI6, as he had done in the previous eleven films of the James Bond series. Out of respect, no new actor was hired to assume the role and, instead, the script was re-written so that the character is said to be on leave, letting Chief of Staff Bill Tanner take over the role as acting head of MI6 and briefing Bond alongside the Minister of Defence.  Chaim Topol was cast following a suggestion by Broccolis wife Dana, while Julian Glover joined the cast as the producers felt he was stylish – Glover was even considered to play Bond at some point, but Michael G. Wilson stated that "when we first thought of him he was too young, and by the time of For Your Eyes Only he was too old".  Carole Bouquet was a suggestion of United Artists publicist Jerry Juroe, and after Glen and Broccoli saw her in That Obscure Object of Desire, they went to Rome to invite Bouquet for the role of Melina. 

===Filming===
  007, similar to the one used in the film.|alt=A yellow car drives down a grassy road.]] Corfu Town, scenographic reasons.    Glen opted to use the local slopes and olive trees for the chase scene between Melinas Citroën 2CV and Gonzales men driving Peugeot 504s.  The scene was shot across twelve days, with stunt driver Rémy Julienne – who would remain in the series up until GoldenEye – driving the Citroën.  Four 2CVs were used, with modifications for the stunts – all had more powerful flat-four engines, and one received a special revolving plate on its roof so it could get turned upside down.   

In October filming moved to other Greek locations, including Meteora|Metéora and the Achilleion (Corfu)|Achilleion.    In November, the main unit moved to England, which included interior work in Pinewood, while the second unit shot underwater scenes in The Bahamas. On 1 January 1981, production moved to Cortina DAmpezzo in Italy, where filming wrapped on February.  Since it was not snowing in Cortina DAmpezzo by the time of filming, the producers had to pay for trucks to bring snow from nearby mountains, which was then dumped in the citys streets.   
 faked on The Deep, and filmed in either Pinewoods tank on the 007 Stage or an underwater set built in the Bahamas. Production designer Peter Lamont and his team developed two working props for the submarine Neptune, as well as a mock-up with a fake bottom. 

Roger Moore was reluctant to film the scene of Bond kicking a car, with Locque inside, over the edge of a cliff, saying that it "was Bond-like, but not Roger Moore Bond-like."  Michael G. Wilson later said that Moore had to be persuaded to be more ruthless than he felt comfortable.  Wilson also added that he and Richard Maibaum, along with John Glen, toyed with other ideas surrounding that scene, but ultimately everyone, even Moore, agreed to do the scene as originally written. 
  in Meteora served as a location|alt=A monastery stands atop a large mountain.]]
For the Meteora shoots, a Greek bishop was paid to allow filming in the monasteries, but the uninformed Eastern Orthodox monks were mostly critical of production rolling in their installations. After a trial in the Greek Supreme Court, it was decided that the monks only property were the interiors – the exteriors and surrounding landscapes were from the local government. In protest, the monks remained shut inside the monasteries during the shooting,   and tried to sabotage production as much as possible, hanging their washing out of their windows  and covering the principal monastery with plastic bunting and flags to spoil the shots, and placing oil drums to prevent the film crew from landing helicopters. The production team solved the problem with back lighting, matte paintings, and building both a similar scenographic monastery on a nearby unoccupied rock, and a monastery set in Pinewood.   
 The Spy Who Loved Me, undertook the stunt of Bond falling off the side of the cliff.    The stunt was dangerous, since the sudden rope jerk at the bottom could be fatal. Special effects supervisor Derek Meddings developed a system that would dampen the stop, but Sylvester recalled that his nerves nearly got the better of him: "From where we were  , you could see the local cemetery; and the box   looked like a casket. You didnt need to be an English major to connect the dots." The stunt went off without a problem. 
 bobsleigh track of Cortina dAmpezzo hoping to surpass his work in both On Her Majestys Secret Service and The Spy Who Loved Me.  To allow better filming, Bogner developed both a system where he was attached to a bobsleigh, allowing to film the vehicle or behind it,  and a set of skis that allowed him to ski forwards and backwards in order to get the best shots.  In February 1981, on the final day of filming the bobsleigh chase, one of the stuntmen driving a sleigh, 23-year-old Paolo Rigon, was killed when he became trapped under the bob. 
 RC car.  Since flying a helicopter through a warehouse was thought to be too dangerous, the scene was shot using forced perspective.  A smaller mock-up was built by Derek Meddings team closer to the camera that the stunt pilot Marc Wolff flew behind and this made it seem as if the helicopter was entering the warehouse.  The footage inside the building was shot on location, though with a life-sized helicopter model which stood over a rail. Stuntman Martin Grace stood as Bond when the agent is dangling outside the flying helicopter, while Roger Moore himself was used in the scenes inside the model.    

===Music===
 
 John Barry-influenced brass elements in the score, but also added elements of dance and funk music.    Whilst one reviewer observed that "Bill Contis score is a constant source of annoyance",  another claimed that "In the end, For Your Eyes Only stands as one of the best James Bond film scores of the 1980s." 

 . 

==Release and reception==
For Your Eyes Only was premiered at the Odeon Leicester Square in London on  ,  setting an all-time opening-day record for any film at any cinema in the UK with a gross of £14,998    (£ }} in   pounds )— ($29,696).  The film went on general release in the UK the same day. For Your Eyes Only had its North American premiere in the US and Canada on Friday, 26 June, at approximately 1,100 cinemas. 
 

The film grossed $54.8 million in the United States,  (equivalent to $101.5 million at 2011 ticket prices  or $ }} million in   dollars,  adjusted for general inflation) and $195.3 million worldwide,  becoming the second highest grossing Bond film after its predecessor, Moonraker.    This was the last James Bond film to be solely released by United Artists. Following the MGM and United Artists merger, the films were released by "MGM/UA Distribution Co". 

The promotional cinema poster for the film featured a woman holding a crossbow; she was photographed from behind, and her outfit left the bottom half of her buttocks exposed. The effect was achieved by having the model wear a pair of bikini bottoms backwards, so that the part seen on her backside is actually the front of the suit.  The poster caused some furore—largely in the US—with The Boston Globe and the Los Angeles Times considering the poster so unsuitable they edited out everything above the knee,    whilst the Pittsburgh Press editors painted a pair of shorts over the legs.  There was significant speculation as to the identity of the model before photographer Morgan Kane identified her as Joyce Bartle.  A number of items of merchandising were issued to coincide with the film, including a 007 digital watch and a copy of Melinas Citroën 2CV by Corgi Toys.  Citroën itself produced a special "007" edition of the 2CV, which even had decorative bullet holes on the door.  Marvel Comics also did a comic book adaptation (see section below). 

===Contemporary reviews===
Derek Malcolm in The Guardian disliked the film, saying it was "too long&nbsp;... and pretty boring between the stunts", although he admitted that the stunts were of a high quality.    According to Malcolm, Bond "inhabits a fantasy-land of more or less bloodless violence, groinless sex and naivety masked as superior sophistication", with Moore playing him as if in a "nicely lubricated daze".  Although Malcolm tipped the film for international box office success, he observed that he "cant quite see why the series has lasted so long and so strong in peoples affections."  Writing in The Observer, Philip French commented that "not for the first time the pre-credits sequence is the best thing about the film."    French was dismissive of Moores Bond, saying that Bond was "impersonated by Moore" and referred to Moores advancing years. 
 Ian Christie, writing in the Daily Express, said that it was not "much of a plot, but it has a touch of credibility which is a welcome change from some of its predecessors."    Overall, Christie thought, For Your Eyes Only was "one of the better Bonds, with a nice balance between humour and excitement and the usual bevy of beautiful girls."  Christies colleague in the Sunday Express, Richard Barkley praised the film, saying that For Your Eyes Only "is one of the most exciting yet". Barkley describes Moores Bond as having an "accustomed debonair calm and quiet authority". All told, Barkley thought "this Bond movie is smashing entertainment." 
 David Robinson, Time Out was brief and pithy: "no plot and poor dialogue, and Moore really is old enough to be the uncle of those girls." 

For the US press, Gary Arnold in The Washington Post thought the film was "undeniably easy on the eyes", and further added "maybe too easy to prevent the mind from wandering and the lids from drooping."    Arnold was also critical of the large set pieces, calling them "more ponderous than sensational" and that there was "no equivalent of the classic action highlights that can be recalled readily from "From Russia, With Love" or "You Only Live Twice" or "The Spy Who Loved Me" or "Moonraker." This is a Bond waiting for something inspired to push it over the top."  The New York Times critic Vincent Canby said that "For Your Eyes Only is not the best of the series by a long shot" although he does say that the film is "slick entertainment" with a tone that is "consistently comic even when the material is not." 

Jack Kroll in Newsweek dismissed the film, saying it was "an anthology of action episodes held together by the thinnest of plot lines", although he does concede that these set pieces are "terrific in their exhilaratingly absurd energy."  For Time (magazine)|Time magazine, Richard Corliss concentrated on the stunts, saying the team "have devised some splendid optional features for For Your Eyes Only" whilst also commenting on Roger Moore, saying that his "mannequin good looks and waxed-fruit insouciance" show him to be "the best-oiled cog in this perpetual motion machine."  Jay Scott of The Globe and Mail included it on his list of the years worst films.    "Repellant"    and "ambitiously bad". 

French filmmaker Robert Bresson admired the film. "It filled me with wonder because of its cinematographic writing&nbsp;... if I could have seen it twice in a row and again the next day, I would have done."   Elsewhere Bresson said he also loved the films ski chase. 

===Reflective reviews=== Time Out re-issued a review of For Your Eyes Only and observed that the film is "admirable in intent" but that it "feels a little spare", largely because the plot has been "divested of the bells and whistles that hallmark the franchise".   

James Berardinelli wrote that the film was "a solid adventure, although it could have been better",  while Danny Peary thought "There are exciting moments, but most of it is standard Bond fare," going on to describe For Your Eyes Only as "an attempt to mix spectacle with   tough, believable storylines of early Bond films&nbsp;...   is enjoyable while youre watching it. Afterward, its one of the most forgettable of the Bond series."  Raymond Benson, the author of nine Bond novels, thought For Your Eyes Only was Roger Moores best Bond film. 

Although Chris Nashawaty of Entertainment Weekly ranks Carole Bouquet playing Melina as the "worst babe" of the seven Roger Moore James Bond films,  his colleague, Joshua Rich disagreed, putting her tenth in the overall 10 Best Bond Girls listing from the 21 films released up to that point.  Entertainment Weekly also ranked Lynn-Holly Johnson as Bibi Dahl as ninth on their list of the 10 worst Bond girls from the choice of from the 21 films that had been released.  After 20 films had been released, IGN ranked Bouquet as fifth in their top 10 Bond Babes list,  and The Times thought she was sixth on their list of the Top 10 most fashionable Bond girls after 21 films had been released. 

===Accolades=== Best Original Best Original 1981 Academy Best Adapted Screenplay – Comedy or Musical Picture. 

==Comic book adaptation==
 
 
 pencilled by inked by Vincent Colletta and edited by Dennis ONeil. 

It was the second film in the series to have a comic book tie-in, following a Dr. No (comics)|Dr. No comic in 1962. Marvel Comics would go on to publish an Octopussy comic book adaptation in 1983.

==See also==
 
* Outline of James Bond

==References==
 

==Sources==
 
*  
*  
*  
*  
*  
*  
* {{Cite book |last= Chaykin |first= Howard |title= Howard Chaykin: Conversations |url= http://books.google.com/?id=5OVZ8cw5liAC&lpg=PP2&dq=%22For%20your%20eyes%20only%22%20marvel%20comic&pg=PP1#v=snippet&q=%22For%20your%20eyes%20only%22&f=false|year=2011 |publisher=UPM |isbn= 978-1-60473-975-6
|ref=harv}}
* 
*  
*  
*  
*  
*  
*  
*  
*  
 

==External links==
 
*   at the Metro-Goldwyn-Mayer site
*  
*  
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 