The First Time on the Grass
 
{{Infobox Film
| name           = The First Time on the Grass
| image          = 
| image size     = 
| caption        = 
| director       = Gianluigi Calderone
| producer       = Enzo Doria
| writer         = Gianluigi Calderone Vincenzo Cerami
| narrator       = 
| starring       = Anne Heywood
| music          = Fiorenzo Carpi
| cinematography = Marcello Gatti
| editing        = Nino Baragli
| distributor    = 
| released       = 1974
| runtime        = 95 minutes
| country        = Italy
| language       = Italian
| budget         = 
| preceded by    = 
| followed by    = 
}}

The First Time on the Grass ( , and known in the United States as Love Under the Elms) is a 1974 Italian drama film directed by Gianluigi Calderone. It was entered into the 25th Berlin International Film Festival.   

==Cast==
* Anne Heywood
* Mark Lester as Franz 
* Claudio Cassinelli as Hans
* Monica Guerritore as Margarite
* Giovanna Di Bernardo
* Bruno Zanin
* Vincenzo Ferro
* Janine Samona
* Anna Waidmann
* Giuseppe Winkler
* Lorenzo Piani

==References==
 

==External links==
* 

 
 
 
 
 
 
 