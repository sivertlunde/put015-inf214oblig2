Meet the Hollowheads
{{Infobox film
| name           = Meet the Hollowheads
| image          = MeetTheHollowheads.jpg | image_size=
| caption        = DVD cover
| director       = Thomas R. Burman
| producer       = Pippa Scott
| writer         = Thomas R. Burman Lisa Morton
| narrator       = John Glover Nancy Mette Richard Portnow Juliette Lewis
| music          = Glenn A. Jordan
| cinematography = Marvin V. Rush Carl Kress
| distributor    = Moviestore Entertainment 1989
| runtime        = 86 min.
| country        = U.S.A. English
| budget         =
| preceded_by    =
| followed_by    =
}} John Glover, Richard Portnow, and Joshua John Miller.  The film is a black comedy and satire of 1950s sitcoms set in a dystopic future populated by bizarre, tentacled creatures which function dually as household appliances and food.

As of December 2010, Meet the Hollowheads is Burmans only directorial effort.

==External links==
*  
* 
*  
*  

 

 
 
 
 
 
 
 


 