Fever (1981 film)
{{Infobox film
| name           = Fever
| image          = 
| image size     = 
| caption        = 
| director       = Agnieszka Holland
| producer       = 
| writer         = Andrzej Strug Krzysztof Teodor Toeplitz
| starring       = Barbara Grabowska
| music          = 
| cinematography = Jacek Petrycki
| editing        = Halina Nawrocka
| distributor    = 
| released       = February 1981
| runtime        = 122 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}

Fever ( ) is a 1981 Polish drama film directed by Agnieszka Holland. It was entered into the 31st Berlin International Film Festival where Barbara Grabowska won the Silver Bear for Best Actress.    The film takes place during the Revolution in the Kingdom of Poland (1905–07).

==Cast==
* Barbara Grabowska - Kama
* Adam Ferency - Wojtek Kielza
* Boguslaw Linda - Gryziak
* Olgierd Lukaszewicz - Leon
* Tomasz Miedzik - Kamil
* Aleksy Awdiejew - Governors Aide (as Aleksiej Awdiejew)
* Wiktor Grotowicz - Governors Butler
* Tadeusz Huk - Chemist
* Michal Juszczakiewicz - Michal
* Krzysztof Kiersznowski - Activist
* Marian Lacz - Kielzas Uncle
* Pawel Nowisz - Wartki
* Ryszard Sobolewski - Governor
* Michal Tarkowski - Doctor
* Krzysztof Zaleski - Czarny

==References==
 

==External links==
* 

 

 
 
 
 
 
 