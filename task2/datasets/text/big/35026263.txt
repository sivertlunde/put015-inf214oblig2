Naanga
{{Infobox film
| name = Naanga
| image = 
| caption =  Selva
| producer = 
| story = Selva
| screenplay = Selva
| starring = Nivas Adithan Sanjay Krishna Muneesh Vinod Uday Shakir Ashwin Raja Vishnu Priya Shivani Bhai Vaidehi Arasi
| music = Bala Bharathi
| cinematography = B. Balamurugan
| editing = 
| studio = Cinema Kottagai
| distributor = 
| released =  
| runtime =
| language = Tamil
| country = India
| budget = 
}} Tamil film directed by Selva (director)|Selva, featuring an ensemble cast of newcomers in the lead roles. The film, notably Selvas 25th directorial, revolves around a group of alumni from the 1985 batch of a Tiruchi College, who meet again in 2011.  It was released on 9 March 2012. 

==Cast==
* Nivas as Mani
* Sanjay Krishna as Chandran
* Muneesh as Baasha
* Vinod as Daya
* Uday as Babu
* Shakir
* Ashwin Raja
* Virumandi
* Vishnupriya (actress) as Devi
* Shivani Bhai
* Vaidehi
* Arasi Kasthuri
* Varun Raj Kapoor

==Production== Kasthuri was roped in for a pivotal role. 

Bala Bharathy and Balamurugan, who had been part of Selvas Amaravathi (1993 film)|Amaravathi (1993), best known as being actor Ajith Kumars debut venture, were signed on as the composer and cinematographer, respectively,  rejoining with Selva after nearly twenty years.    Bala Bharathy composed eights songs for the film in the same ragas used by Ilaiyaraaja in the 1980s.  Selva introduced Raghavan Urs as the editor, son on Suresh Urs, himself a noted editor in Indian cinema. 

==Reception==
M Suganth from  s critic Malathi Rangarajan in her review wrote: "A host of new faces, a fresh approach to narration, a rare storyline that gives equal importance to an entire group of friends — five in all — decent portrayals, a few clichés and a dose of melodrama comprise Selvas Naanga".  A reviewer from Behindwoods.com gave the film 1.5 out 5, noting that it was an "episodic and disjointed affair which never really takes off".  Indiaglitz.com cited: "Naanga promises aplenty. Selva has proved his worth yet again by narrating a feel-good story with rightly recreating the ambience and mood of 1980s".  Rohit Ramachandran from Nowrunning.com rated the film 2 out of 5, while calling it an "honest failure". 

==References==
 

 

 
 
 
 