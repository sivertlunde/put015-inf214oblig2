Night Plane from Chungking
{{Infobox Film   
| name           = Night Plane from Chungking
| image          = Night Plane from Chungking.jpg
| caption        = Original Spanish film poster
| director       = Ralph Murphy
| producer       = Michael Kraike Walter MacEwen
| writer         = Lester Cole Earl Fenton Theodore Reeves Sidney Biddell (adaptation) Harry Hervey (story) Robert Preston Ellen Drew Otto Kruger Stephen Geray
| music          = 
| cinematography = Theodor Sparkuhl
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 69 min
| country        = United States
| awards         = 
| language       = English
| budget         = 
}} Shanghai Express (1932).
 Robert Preston and Ellen Drew, with Otto Kruger and Stephen Geray.

==Plot==
A truck carrying passengers on a road to India is bombed by the Japanese during their invasion of China, due to the carelessness of one of the passengers, Albert Pasavy.

The wounded are flown to a secret air field. Among those met by U.S. pilot Nick Stanton are a beautiful Red Cross nurse, Ann Richards, and her traveling companion, Madame Wu, who is on a secret diplomatic mission. There is also Countess Olga, who is caught spying.

Nick and his co-pilot, Captain Po, try to fly them out, but the plane needs to make an emergency landing in a jungle. Olga is killed. Stanton learns that the spy was trying to get top-secret information to her superior.

Another of the passengers, Rev. Van der Linden, goes missing, but returns with food from a monastery. The reverend leads everyone on a long hike to the monastery, only to reveal there that he is a Nazi collaborator working with the Japanese. He demands to know where Olga is, not knowing she is dead.

Pasavy betrays the others, but is coldly shot. It is up to Nick to kill the Nazi and get Ann, Madame Wu and Po to safety.

==Cast== Robert Preston as Nick
* Ellen Drew as Ann
* Stephen Geray as Ven Der Lieden
* Otto Kruger as Pasavy
* Victor Sen Yung as Po
* Tamara Geva as Olga

== External links ==
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 


 