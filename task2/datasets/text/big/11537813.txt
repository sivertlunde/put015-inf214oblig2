Tere Mere Sapne (1971 film)
{{Infobox film
| name           =Tere Mere Sapne
| image          =TMSapne.jpg
| image_size     = 
| caption        = Vijay Anand
| producer       = Dev Anand Vijay Anand
| narrator       = Mumtaz Hema Malini
| music          = Sachin Dev Burman|S. D. Burman
| cinematography = V. Ratra Vijay Anand
| distributor    =
| released       = 1971
| runtime        = 171 minutes
| country        = India
| language       = Hindi
| budget         =
| preceded_by    =
| followed_by    =
}}
 Vijay Anand The Citadel, Bengali film Telugu film Madhura Swapnam. 

==Plot==
Dr. Anand Kumar attains his degree in medicine and re-locates to a small coal mining village to assist the local doctors there. Upon arrival, he is met by the ailing Dr. Prasad and his wife, and hired on a salary of Rs.250/- per month. He meets with the other doctors, namely Dr. Kothari, an alcoholic, and Dr. Bhutani, a dentist. Anand finds that he is saddled with all of Kotharis work as well his own because Kothari is drunk every night. He nevertheless carries on, aided by the local school-teacher, Nisha Patel, who he eventually marries. One day a proud father, Phoolchand, gives a baksheesh to Anand for the safe delivery of his first-born. This does not wager well with Mrs. Prasad, so she fires Anand. Meanwhile Anands wife meets with an accident resulting in an abortion and Anand cannot do anything against the wealthy person whose car hits Nisha. This accident transforms Anand from a normal, good natured doctor into a doctor who is after money. Anand and Nisha re-locate to Bombay, where Anand later establishes himself as a leading doctor, is honored for his thesis, and becomes the personal doctor of a leading Bollywood actress, Maltimala. When Dr. Kothari and Dr. Bhutani go to visit them, they find that while Nisha is still the same good-natured woman, who is now expecting a child, Dr. Anand is a changed individual.  The doctor does not keep relations with friends, lives a personal life, remains cold towards his wife, and is sadly unaware that she is even pregnant. Dr. Kothari starts a hospital in the village with the assistance of late Dr. Prasads wife. Nisha leaves Dr. Anand to go back to the village. The death of Phoolchands child at the hand of a dubious surgeon causes Dr. Anand to regret his new life. He goes back to the village. Nishas pregnancy gets into complications. Ultimately, Nisha and her child are saved by Dr Kotharis surgery. In the end, Dr. Anand is back in the village with Nisha and his child.

==Cast==
*Dev Anand as  Dr. Anand Kumar  Mumtaz as  Nisha Patel / Nisha Kumar
*Mahesh Kaul as  Dr. Prasad Kaul Vijay Anand as  Dr. Jagannath Kothari 
*Hema Malini as  Maltimala
* Tabassum as Maltimalas Hairdresser Agha as Dr. Bhutani
* Paro as Mrs. Prasad
* Leela Mishra as Nishas aunt
* D.K. Sapru as Phoolchand
* Jayshree T. as Dancer in song "Mera Saajan Phool Kamal Ka" Mumtaz Begum as Maltimalas Mother
* Prem Nath as Seth. Madhochand Dulari as Phoolchands Wife

==Locations==
Most of shooting of this film was done in the coal mines area of Chhindwara District, Madhya Pradesh, including Dongar Parasia.

==Soundtrack==
The Soundtrack of the movie is by Sachin Dev Burman and the lyrics were penned by Neeraj.
{| class=wikitable
|-
!Song
!Singers 
!Picturised on
|-
|"Phurr Ud Chala" Asha Bhosle Hema Malini
|-
|"Mera Saajan Phool Kanwal Ka" Asha Bhosle Jayshree T.
|-
|"Jaisa Radha Ne Mala Japi" Lata Mangeshkar Mumtaz (actress)|Mumtaz
|-
|"Ae Maine Kasam Li" Lata Mangeshkar, Kishore Kumar
|Mumtaz, Dev Anand
|-
|"Jeevan Ki Bagiya Mehkengi" Lata Mangeshkar, Kishore Kumar
|Mumtaz, Dev Anand
|-
|"Tha Thai Thatha Thai" Asha Bhosle, Chorus Hema Malini
|-
|"Mera Antar Ek Mandir Hai Tera" Lata Mangeshkar Mumtaz
|-
|"Zamaane Dhat Tere Ki" Manna Dey Agha (actor)|Agha
|}

==References==
 

==External links==
*  
* 
* 
* 

 

 
 
 
 
 
 
 