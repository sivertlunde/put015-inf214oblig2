Mangus!
 
{{Infobox film
| name = Mangus!
| image = Mangus (2011 film).jpg
| image_size = 
| alt = 
| director = Ash Christian
| producer = Ash Christian Heather Matarazzo Simon Millar
| writer = Ash Christian John Waters
| music = John Dufilho Musical: Scott Murphy Caroline Murphy
| cinematography = Lyn Moncrief
| editing = Scott D. Martin
| studio = Cranium Entertainment Filmgate
| distributor = Wolfe Video
| released =  
| runtime = 88 minutes
| country = United States
| language = English
}} John Waters.

==Plot== flamboyant classmate, loses the use of his legs. 

Farrells mother gathers signatures to get Mangus out of the play so that the role be offered to Farrell. The School District rules that Mangus cannot play Jesus. Play director Bruce (Leslie Jordan) is distraught by the decision, but still must give Mangus the bad news. When Mangus Sr. returns to the war, Raquel sends Mangus to live with his mother, a psychic named Cookie (Jennifer Coolidge), and his half-sister, Jessica Simpson, who live in a trailer with Cookies boyfriend, Buddy.

Mangus spends his time with his half-sister Jessica Simpson and with his mothers boyfriend Buddy, who is almost as young as Mangus. Jessica Simpson reveals to Mangus that she is a lesbian and meets Bobbie, a black woman who sells ice-cream from a truck. While children wait to buy ice-cream, Jessica and Bobbie have sex in the truck.
 John Waters), who encourages him to fight for the role. 
 flamboyant classmate, Farrell Williamson, who, while showing Mangus his routine, falls through the coffee table and is injured (his fate is not revealed until towards the end of the film).

Manguss father is injured in the war and returns home in a wheelchair. He decides to remarry Cookie.

Mangus believes Farrell is dead. He decides to leave town and go to Hollywood to pursue his dream of becoming a singer and disabled dancer. He takes with him his half-sister Jessica Simpson. But accidentally they end up in Hollywood, Florida. There Mangus receives a telegram offering him the part of Jesus again, despite his disability. Although Mangus and Jessica are out of money, they find a Greyhound employee who takes pity on them and they manage to return home.

When Mangus returns to school, Bruce tells him that since they didnt know he was coming back, they has offered the part to Harry. But Harry wants out of the role, and when Mangus asks him for the part, Harry gladly returns it to Mangus. 

During the show, Raquel and Buddy finally manage to hook up, but they accidentally stumble into the stage and Buddy is caught with Raquel with his pants down.

For no apparent reason, the musicals opening number is sung by Santa. The rest of the cast is dressed mostly like hippies from Jesus Christ Superstar. Near the end of musical, Mangus is attached to the cross in his wheelchair.

After the play, Manguss parents reconciled and the whole family go for ice cream.

==Cast==
* Ryan Nelson Boggus as Mangus Spedgewick
* Jennifer Coolidge as Cookie Richardson
* Heather Matarazzo as Jessica Simpson
* Leslie Jordan as Bruce Jackson John Waters as Jesus Christ
* Charles Solomon, Jr. as Mangus Spedgewick, Sr.
* Deborah Theaker as Raquel Spedgewick
* Peter S. Williams as Buddy Richardson
* John D. Montoya as Farrell Williamson
* Leticia Magana as Patricia Williamson
* Brittni Horton as Bobbi Jackson
* Zander Scott as Timmy Jones
* Laura Spencer (actor)| Laura Spencer as Kimmy Jones
* Lynn Ambrose as Flossy
* Rex Cumming as Randy Jenkins
* Rodney Shed as Harry

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 