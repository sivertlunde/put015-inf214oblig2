Exit to Eden (film)
{{Infobox film
| name           = Exit To Eden
| image          = exit_to_eden_poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Garry Marshall
| producer       = Garry Marshall Edward K. Milkis
| writer         = Deborah Amelon Bob Brunner
| based on       =  
| starring       = Dana Delany Paul Mercurio Rosie ODonnell Dan Aykroyd 
| music          = Patrick Doyle
| cinematography = Theo van de Sande
| editing        = David Finfer
| distributor    = Savoy Pictures
| released       =  
| runtime        = 114 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $6,841,570 
}} thriller film adapted to Anne Ramplings novel of the same name. The original music score was composed by Patrick Doyle.
 comedic detective story line written by the director. Several new characters were also created, including Dan Aykroyd and Rosie ODonnell as police officers pursuing diamond thieves to the Eden resort.

==Plot==
Elliot Slater is a young, attractive, Australian professional photographer living in Southern California. Having always been uncomfortable with his sexual proclivities, which tend toward the BDSM realm, he signs up for a dominatrix-themed vacation on a private tropical island known as "Eden" in the hopes of working through his discomfort.

Unbeknownst to him, prior to embarking on his journey of sexual discovery, he has unwittingly photographed an international jewel thief of whom no other photos exist. The jewel thief Omar and his criminal partner Nina are intent on recovering the film in order to retain Omars anonymity.

They follow the photographer to the island resort run by the dominatrix Mistress Lisa Emerson, posing as vacationers. Following a tip that Omar is on the island, undercover police officers Fred Lavery and Sheila Kingston also arrive, Sheila in the guise of a vacationer and Fred as a handyman.

Comedic antics ensue amid the activities of scantily clad guests and employees, acting out their dominant and submissive Fantasy (psychology)|fantasies.

In the course of Elliots experiences as Mistress Lisas personal submissive, including a scene where she ties him up and fondles his naked body (especially, his bare buttocks), the two begin to fall in love. The action comes to a climax on a quick trip to New Orleans, where Lisa reluctantly admits her feelings for Elliot, all the while tailed by Omar, who attempts to kill them.

Fortunately, Fred and Sheila save the day, sending Omar and Nina to jail, and receive commendations for solving the case.  Elliot returns to Eden and proposes to Lisa, who says yes.

==Cast==
* Dana Delany as Lisa Emerson
* Paul Mercurio as Elliott Slater
* Rosie ODonnell as Sheila Kingston
* Dan Aykroyd as Fred Lavery
* Donna Dixon as Freds ex-wife
* Hector Elizondo as Dr. Martin Helifax Stuart Wilson as Omar Iman as Nina Blackstone
* Sean OBryan as Tommy Miller
* Stephanie Niznik as Diana
* Phil Redrow as Richard
* Sandra Taylor as Riba
* Julie Hughes as Julie
* Laura Harring as M.C. Kindra
* Deborah Pratt as Dr. Williams
* Laurelle Mehus as Heidi
* Tom Hines as Nolan
* Alison Moir as Kitty
* James Patrick Stuart as James
* Rosemary Forsyth as Mrs. Brady John Schneider as Professor Collins

==Production==
The whips used and shown in detail were created by Janette Heartwood.  This was the last film produced by Edward K. Milkis before his death in 1996.

==Reception==
The film garnered attention during its release because of the BDSM themes, full frontal female nudity (including Delany), and because of the high profiles of the director, cast members, and the author. Promotional materials for the film included photos of Delany in dominatrix attire.

It was generally panned by critics, who expressed disappointment and confusion about the combination of the original story and the comedic elements.    The film maintains a 6% "rotten" rating at Rotten Tomatoes based on 16 reviews.  Roger Ebert gave the film 1/2 star out of four possible stars. 
 banned by the Saskatchewan Film and Video Classification Board. Critics were puzzled by the banning, as Saskatchewan was the only jurisdiction known to have kept the film out of theaters. After a brief media flurry, the Board lifted the ban a week later.  

===Awards and nominations===
  Razzie Award Worst Supporting The Flintstones Car 54 Worst Supporting Worst Screen Couple.

==Home media==
 
The film was released on VHS tape (NTSC) in May 1995, on DVD (NTSC Region 1) in April 2002 and on (PAL Region 2) in 2003 (German Version "Undercover Cop" with German and English language sound). The U.S. DVD is currently out of print, and as of November 2012, neither HBO nor Focus Features, the latter of whom has begun to acquire some of Savoys films, has announced any plans to release a new DVD of the film. For these reasons, copies of the original DVD can be found online being sold for very high prices.

==References==
 

==External links==
*  
*  
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 