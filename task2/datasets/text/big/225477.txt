Summer Wishes, Winter Dreams
{{Infobox film
| name           = Summer Wishes, Winter Dreams
| image          = Summer Wishes Winter Dreams.jpg
| image_size     =
| caption        =
| director       = Gilbert Cates
| producer       = Jack Brodsky
| writer         = Stewart Stern
| narrator       =
| starring       = Joanne Woodward Martin Balsam Sylvia Sidney Teresa Hughes
| music          = Johnny Mandel
| cinematography = Gerald Hirschfeld
| editing        = Sidney Katz
| studio         = Rastar
| distributor    = Columbia Pictures
| released       =  
| runtime        = 93 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Summer Wishes, Winter Dreams is a 1973 Technicolor film which tells the story of a New York City housewife who rethinks her relationships with her husband, her children and her mother. The movie stars Joanne Woodward, Martin Balsam, Sylvia Sidney and Teresa Hughes, was written by Stewart Stern, and directed by Gilbert Cates.
 Best Actress Best Actress in a Supporting Role (Sylvia Sidney).

==Plot summary==
 

==Cast==
* Joanne Woodward as Rita Walden
* Martin Balsam as Harry Walden
* Sylvia Sidney as Mrs. Pritchett
* Tresa Hughes as Betty Goody
* Dori Brenner as Anna
* Ron Richards as Bobby Walden
* Win Forman as Fred Goody
* Peter Marklin as Joel Nancy Andrews as Mrs. Hungerford
* Minerva Pious as Woman in Theatre
* Sol Frieder as Man in Theatre
* Helen Ludlam as Grandmother
* Grant Code as Grandfather
* Gaetano Lisi as Student in Theatre
* Lee Jackson as Carl

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 