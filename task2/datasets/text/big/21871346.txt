Without a Paddle: Nature's Calling
 
{{Infobox film
| name           = Without a Paddle: Natures Calling
| image          = Without a Paddle - Natures Calling.png
| image_size     = 185px
| caption        = DVD cover
| director       = Ellory Elkayem
| producer       = Amy Goldberg
| writer         = Stephen Mazur Oliver James  Amber McDonald
| music          = Thomas Chase
| cinematography = Thomas L. Callaway John Gilbert
| studio         = 
| distributor    = Paramount Famous Productions
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $4,422,827 (DVD rentals)  
}}

Without a Paddle: Natures Calling is a 2009 direct-to-video spin-off from the 2004 film Without a Paddle.  There is no connection to the first film and none of the original actors return.

It was released on DVD in the US on January 13, 2009 and in the UK on March 23, 2009.

==Synopsis== Oliver James and Kristopher Turner) reunite years later to take part in an unusual quest that involves traveling into the woods in search of Bens high school sweetheart. They are joined by their British rival Nigel (Rik Young) and, as the stakes get higher and the squirrels turn hostile, the hapless trio attempts to navigate a raging river while realizing that sometimes nature isnt all its cracked up to be.

==Cast== Oliver James as Ben
* Kristopher Turner as Zach
* Rik Young as Nigel
* Madison Riley as Earthchild/ Heather
* Amber McDonald as Thunderstorm
* Jerry Rice as Hal Gore
* Team-Provo as The Daddy
* Cory Bossom as Young Ben

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 


 