Pudhumai Penn (1984 film)
{{Infobox film
| name = Pudhumai Penn
| image =
| caption =
| director = P. Bharathiraja
| writer = Panju Arunachalam R. Selvaraj
| screenplay = P. Bharathiraja
| story = R. Selvaraj Pandiyan Revathi Prathap K. Pothan Y. G. Mahendra
| producer = M. Saravanan (film producer) |M. Saravanan M. Balasubramaniam
| music = Ilaiyaraaja
| cinematography = B. Kannan
| editing = V. Rajagopal
| studio = AVM Productions
| distributor = AVM Productions
| released = 1984
| runtime =
| country = India Tamil
| budget =
}}
 Pandiyan and Revathi.

==Cast== Pandiyan
* Revathi
* Prathap K. Pothan
* Y. G. Mahendra Janagaraj
* Chithra Lakshmanan

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vairamuthu || 04:20
|-
| 2 || Kadhal Mayakkam || P. Jayachandran, Sunandha || 06:12
|-
| 3 || Kanniyila Sikkaathaiya || Ilaiyaraaja || 04:18
|-
| 4 || Kasthoori Maane || K. J. Yesudas, Uma Ramanan || 05:09
|-
| 5 || Oh Oru Thendral || Malaysia Vasudevan || 04:40
|}

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 


 