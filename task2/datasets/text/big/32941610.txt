Seven Little Australians (1939 film)
 
{{Infobox film
| name           = Seven Little Australians
| image          = Sevenlittleaustpos.jpg
| image_size     = 
| caption        = Film poster
| director       = Arthur Greville Collins
| producer       = Arthur Greville Collins Edward H. OBrien
| writer         = Patrick V. Ryan
| based on = novel by Ethel Turner
| narrator       = 
| starring       = Charles McCallum
| music          = Nellie Weatherill
| cinematography = George Malcolm
| editing        = George Malcolm
| studio         = O.B. Pictures
| distributor = Universal Pictures 
| released       = 15 December 1939
| runtime        = 63 mins
| country        = Australia
| language       = English
| budget         = 
}} novel to be set in the present day.

==Synopsis==
Seven children live with their tyrannical father, Captain McCallum, and step mother Esther. The children can never make their father happy. The oldest girl, Judy, is sent to boarding school.

Judy runs away and falls ill. She is found by her father who allows her to go on holiday in the country to recuperate. The other children come along with her.

Judy is killed by a falling branch. The Captain becomes aware of how he has mistreated his family.

==Cast==
 
*Charles McCallum as Captain Woolcot Patricia McDonald as Esther
*Sandra Jaques as Meg
*Robert Gray as Pip
*Mary McGowan as Judy
*Janet Gleeson as Nell
*Ronald Rousel as Bunty
*Nancy Gleeson as Baby
*Donald Tall as the General
*Harold Meade as Colonel Bryant
*Nan Taylor as Mrs Bryant John Wiltshire as Gillet
*John Fernside as doctor
*Edna Montgomery as Aldith
*Howard Craven as Andrew
*Letty Craydon as Martha
*George Doran as groom
*Nesta Tait as Bridget
*Carl Francis as Fred Hassel
*Connie Martyn as Mary Hassel
*Nellie Lamport as school teacher
*Mary Swan as Marion
*Jean Hart as Betty
*Margaret Roussel as Doris
*Richard Dowse as Major Martin
*Norman Wait as Mr Hill
*Eve Wynne as Mrs Hill
*Pixie Murphy
 

==Production==
It was originally announced the movie would be made by noted theatre producer Sir Benjamin Fuller, his first exercise into movie making. Fuller said it would be part of a slate of projects, also including the story of Father Damien of Molokai and an adaptation of Norman Lindsays novel Redheap.  Fuller hired English director Arthur Greville Collins, who had directed several Hollywood films arrived in May 1939 to begin preproduction. Collins later said he was visiting Sydney on holiday when he read the book and decided to turn it into a film. 

The original plan was to cast one child from each of the six states of Australia, and one from New Zealand.  The children who were cast had a variety of experience – Mary McGrown, Ron Rousel and Sandra Jacque had a background in amateur theatre and radio; sisters Janet and Nancy Gleeson were pantomime and radio veterans at the age of seven.   

The script updated the story from 1894 to the 1930s. "Although the story has been modernised " said Collins, "we have made every effort to retain the same freshness and vivacity that has made the book such a definite part of Australian fiction." 

Despite announcements of Fullers involvement, the film was eventually produced by O.B. Pictures, a Sydney company headed by businessman Edward OBrien. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p186  They announced plans to make a series of other films with Collins. 

Shooting started in August 1939  and took place on location at Camden and in the studios of the Commonwealth Film Laboratories in Sydney. It took five weeks in all.   

==Reception==
Critical reaction was generally poor and the movie was not a financial success.   

==References==
 

==External links==
*  in the Internet Movie Database
*  at National Film and Sound Archive
*  at Oz Movies

 
 