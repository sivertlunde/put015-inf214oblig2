Surfacing (film)
{{Infobox film
| name = Surfacing
| image =
| image_size =
| caption =
| director = Claude Jutra
| producer = Beryl Fox Bernard Gordon
| starring =  
| music =  
| cinematography = Richard Leiterman
| editing = Toni Trow
| distributor =
| released =  
| runtime =  89 minutes
| country = Canada
| language = English
| budget =
}} Bernard Gordon as an adaptation of Margaret Atwoods novel Surfacing (novel)|Surfacing.

The films cast included Kathleen Beller, R. H. Thomson, Joseph Bottoms, Michael Ironside and Margaret Dragu.
 documentary filmmaker.  In addition, the film was criticized for casting Beller and Bottoms, actors from the United States, in a film adaptation of a novel with themes of Canadian nationalism, as well as for giving Bottoms top billing even though Bellers character was the novels primary protagonist. 
 Best Original Song category.

Jutra rebounded the following year with the more successful and better-received By Design. 

Many of the scenes were filmed in the Bon Echo Park area on Mazinaw Lake in south eastern Ontario Canada.

==References==
 

==External links==
*  
*  

 
 
 
 
 

 