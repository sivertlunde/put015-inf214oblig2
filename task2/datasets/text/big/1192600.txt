CKY (video series)
 
{{Infobox film
| name           = CKY (video series)
| image          = Ckyvideocovers.jpg
| caption        = The covers for all 4 videos: CKY, CKY2K, CKY3, and CKY4
| director       = Bam Margera, Joe Frantz, Ryan Gee, Brandon DiCamillo
| producer       = Bam Margera and Joe Frantz
| writer         = Bam Margera & Brandon DiCamillo
| screenplay     =
| story          =
| narrator       = Brandon DiCamillo
| starring       = CKY Crew Brandon DiCamillo Bam Margera Jess Margera Rake Yohn Raab Himself Chad Ginsburg Deron Miller Ryan Dunn Brandon Novak  MacGregor Huston (Unreleased Footage) CKY
| cinematography = Joe Frantz
| editing        = Bam Margera
| studio         = Bam Margera Productions
| distributor    = Landspeed, Ventura Distribution, SLAM! Films, Revolver Entertainment
| released       =
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The CKY video series is a series of videos produced by   and his band CKY (band)|CKY (with Deron Miller and Chad Ginsburg).  "CKY" stands for "Camp Kill Yourself". 

The videos feature Bam Margera, Brandon DiCamillo, their friends and Bams relatives performing various stunts and pranks, interspersed with skating and prank footage of Bam and other pros.  A trademark of the skating footage was to show unsuccessful trick attempts immediately followed by the same skater pulling the trick off. CKY started when Bam and his friends were in the same Graphics Arts class at school in West Chester, Pennsylvania. During class, they would go out to a field and film skits, eventually being compiled into the CKY series. In a 2002 interview Bam Margera said that more than 400,000 copies of the CKY series have been sold.  

== The CKY Crew ==
  CKY the band became popular, and touring and recording commitments took up most of his time.

The first video stars the crew as well as The Gill (Ryan Gee), Mike Maldonado, Chris Aspite aka Hoofbite, and Kerry Getz. Bams father, Phil Margera, were also featured (uncredited) in the first video. Bams ex-girlfriend Jenn Rivell and David Decurtis aka Naked Dave starred in the second video, which also features Bams mother April Margera (uncredited). Chris Raab was called Raab Himself in the third video, which also featured CKY band members Deron Miller and Chad Ginsburg as well as Tony Hawk and Brandon Novak. Bams uncle Vincent Margera aka "Don Vito" starred in the fourth video. Jenn Rivells daughter is also seen in some clips.

The CKY videos brought Bam and his friends to the attention of Jeff Tremaine who drafted them into the cast of Jackass (television series)|Jackass which aired for three seasons on MTV. Most Jackass skits featuring the CKY Crew were lifted from previously released CKY material or were recorded by the crew in West Chester, while the Los Angeles based faction of the Jackass team featuring Johnny Knoxville, Steve-O, Chris Pontius and others recorded in California. A subsequent MTV Spin-off (media)|spin-off entitled Viva La Bam followed Bam Margera and his crew as they tortured Bams family and generally wreaked havoc in West Chester and around the world.

==CKY==
{{Infobox film
| name           = CKY
| image          = Landspeedfront.jpg
| caption        = Original 1999 VHS release
| director       = Bam Margera
| producer       = Bam Margera, Brandon DiCamillo
| writer         = Bam Margera, Brandon DiCamillo The CKY crew CKY
| cinematography = Bam Margera
| editing        = Michael Nuit
| distributor    = Tum Yeto (1999); Ventura Distribution (somewhere later in 1999)
| released       =  
| runtime        = apprx. 45 minutes
| rating         = Rated R
| country        = United States
| language       = English 
| budget         =
}} Tum Yeto Jess Margeras CKY (band)|band. The video shows a mixed up variety of random, homemade, crazy humor, and mostly focuses on stunts, pranks, and crazy behavior, as well as skateboarding. The music features original songs by the band CKY (band)|CKY, including "96 Quite Bitter Beings". Landspeed originally produced the video, allowing Bam to distribute it in the public domain .

===Differences between the VHS and DVD versions===

*The copyright was taken off because it has Tum Yeto references.
*In the opening credits, the Landspeed title was taken off.
*A scene was also removed. It featured Brandon Dicamillo running around a Christmas parade as Santa Claus, because the real Santa was late. No one at the parade except for Bam and his crew knew that Brandon wasnt involved with the parade.
* In the Round 1 version of CKY there are clips removed, not only the Santa clip, but things such as bits and pieces and extended skits, and to make up for that it has more extra footage at the end than before
*Some skits in the DVD version are edited or cut short. Such as, Bam sings two Bon Jovi songs in the street to strangers in the VHS version (they cut one of them in the DVD version).

===CKY Documentary===
CKY Documentary is a compilation of footage from the first CKY video, it is in a double pack on the CKY Trilogy Round One DVD and CKY the box set (quadruple pack) DVD and show profiles of each member of the CKY Crew, including some older skits from when Bam was in high school and the story of how CKY was created. It is entirely in black and white.

==CKY2K==
{{Infobox film
| name           = CKY2K
| image          =
| caption        =
| director       = Bam Margera and Ryan Gee
| producer       = Bam Margera
| writer         = Bam Margera and Brandon DiCamillo The CKY crew CKY
| cinematography = Joseph Frantz
| editing        = Bam Margera
| distributor    = Ventura Distribution
| released       =  
| runtime        = apprx. 44 minutes
| rating         = Rated R
| country        = United States
| language       = English
| budget         =
}}
CKY2K, the second film in the CKY series, was released on May 22, 2000. The video features a trip to Iceland, the rental car, a baseball game, "Brans Freestyle", and other random items. The music features early versions of Flesh into Gear and Sporadic Movement by CKY (band)|CKY; as well as many other artists, including an opening scene with Rammstein, Iceland scenes with Björk and Orbital (band)|Orbital, and a skateboarding scene with Aphex Twin.

Near the end of the video, there is a scene in which a minor exposes his genitalia in front of a public restaurant. The video explained that "this is what happens when you tell Bam youll do anything to be in CKY." In 2003, the parents of the minor sued.  During the hearing, a County Court Judge ruled that images of the plaintiff "must be deleted from future versions" of the video.  Subsequently, CKY2K was discontinued due (in part) to copyright issues. The first major issue was from Björk.

===Iceland===

The video was notable for the documentary of their trip to Iceland. The music features "Hyper-Ballad" by Björk where Bam and the crew hang out on the top of a mountain with gorgeous views from the top.

As they stayed in a hotel somewhere in Iceland (the hotel at the airport), Brandon Dicamillo smeared feces on Ryan Dunn while he was sleeping on the bed. Smeared with feces, Ryan fought back by urinating on Brandon sleeping on the floor. Later, they argued in the car about the incident.
 Orbital playing "Halcyon + On + On". Later, Bam was outside Björk|Björks house calling her.

===The rental car===
 Cavalier after CKY

===Differences between the VHS and DVD versions===
* The warnings were taken off from the beginning of the VHS version, and used in the beginning of the DVD menu, which then shows Bam playing with HIM (Finnish band)|HIM.
* A small note at the end of "Brans Freestyleolk;" by CKY (band)|CKY, and featured some clips after the credits, showing Bam Margera filming for seven teen sips, which includes a short preview of his film at the very end of the video. In the DVD version, the end credits were cut short and instead played "One Last Time" by HIM and featured alternate footage, including more furniture sledding and Bam commanding his father Phil to do push-ups.

==CKY 3==
{{Infobox film
| name           = CKY3
| image          =
| caption        =
| director       = Bam Margera Brandon DiCamillo
| producer       = Bam Margera, Brandon DiCamillo
| writer         = Brandon DiCamillo and Bam Margera The CKY crew CKY
| cinematography = Joe Frantz
| editing        = Bam Margera
| distributor    = Ventura Distribution
| released       =  
| runtime        = apprx. 45 minutes
| rating         =
| country        = United States
| language       = English
| budget         =
}}
CKY 3, the third film in the CKY series, was released in 2001.  Soon after its release Margera and DiCamillo were sued over the fight scene involving Mike Vallely.  The issue was settled out of court in 2006.  It was also the first of the videos to be taken off the market due to copyright restrictions. Some of the musicians (or their legal representation) were unhappy with Margera using their music without their permission. This would later happen with CKY2K and the box set including all three videos and the documentary.

==CKY4: The Latest & Greatest==
{{Infobox film
| name           = CKY4: The Latest & Greatest
| image          =
| caption        =
| director       = Bam Margera
| producer       = Brandon DiCamillo and
| writer         = Bam Margera The CKY crew CKY
| cinematography = Joe Frantz
| editing        = Bam Margera
| distributor    = Slam Films/Ventura Distribution
| released       =  
| runtime        = apprx. 100 minutes
| rating         = Rated R
| country        = United States
| language       = English
| budget         =
}}
CKY 4, the fourth film in the series, was released on November 10, 2002. It has a marked improvement in editing techniques compared to the other films, mostly due to Bam Margeras newly acquired wealth and ability to purchase motion picture film cameras and utilize advanced post-production facilities. However, it also features a lot of older video material, some unseen, some extended and some off-cuts from previous CKY films. Scenes include Don Vitos 50 Shots of Peach Schnapps (Bonus Feature) and Ryan Dunn rolling off a roof in a barrel.

CKY4 is the only CKY video to be censored, and one of the few DVDs to have been censored three times.  Easter Egg coitus with altered color mixes to avoid nudity. This wasnt the first time CKY videos have attracted trouble of this sort, but none of the previous videos cut legal sexually themed scenes until this one in mid-2003. 
*In 2002, Brandon DiCamillo and CKY drummer Jess Margera wrote a rap about Masters Of The Universe characters Skeletor and Beast Man engaging in extreme homosexual behavior, called Skeletor vs Beastman. Mattel, owners of the Masters Of The Universe franchise, heard about the rap, and were displeased. Margera was sent a cease-and-desist order, and subsequent editions of the DVD have replaced this video with skateboarding footage.
*CKY guitarist Chad Ginsburg, a large fan of punk rocker G.G. Allin, visited his grave one day and after consuming an entire bottle of Jim Beam bourbon (Allins favorite beverage), he proceeded to urinate on the grave, and left an autographed copy of Infiltrate.Destroy.Rebuild. which was later stolen. Mixed in between the grave footage is CKY performing Allins song "Bite It, You Scum".
*At the end of the "lil key key" skit performed by Chris Raab, there was a "Secret Easter Egg" also got cut in 2003 for unknown reasons.

 
1ST Print (Barcode: 7 30475 82403 9)
Distance between barcode and top: 3mm
Distance between Parental Advisory and bottom: 4mm
Distance between Slam Films and bottom: 4mm
- Bam & Jenn Sex Scene, Full Shopping Cart Drama and Skeletor VS Beastman.
- Indoor skating (5:11 - 5:47), Sahara Hotnights "No Big Deal" skate segment + random skate footage
(7:45 - 10:25) and another skating part (21:55 - 22:41).
- Don Vitos neck and a shot of Bam giving the finger before Element Rooftop part. Cuts to Bam and
Vallely VS black cop (22:42 - 23:52)
- MISSING the scene where the old security lady goes to call the law.
- Nothing is blurred and all the easter eggs are is there.
Runtime - 1:04:28 (1:08:14) --> The GG Allin part works like an automatic easter egg on this print. On
30:35 it jumps into the GG part (03:46) and when finished, the movie continues from 30:35. If you fast
forward it will skip the GG scene.
Bonuses (21:25minutes) plays automatically after the main movie and are uncut.

_______________________________________________________________________________

2ND Print (Barcode: 7 30475 82403 9)
Distance between barcode and top: 2mm
Distance between Parental Advisory and bottom: 5mm
Distance between Slam Films and bottom: 5mm
- Full Shopping Cart Drama and Skeletor VS Beastman.
- The Bam and Vallely VS black cop is at 7:08 right after the other argument with the guy with the van in
the parking lot.
- The skateparts listed above for the 1st print are now compiled into one, and the music has been
replaced to HIM - Buried Alive By Love (7:40 - 12:17).
- Shot of KeyKey before the Element Rooftop part (24:01).
- The scene with the old security lady goes to call the law at 23:47. This scene was NOT in the 1st print.
- Nothing is blurred and all the easter eggs is there.
Runtime - 1:08:52. The GG Allin part runs normal like a part of the main feature and is calculated into
the total runtime.
Bonuses (21:25minutes) plays automatically after the main movie and are uncut.

_______________________________________________________________________________

3RD Print (Barcode: 6 34991 14192 5)
Distance between barcode and top: 4mm
Distance between Parental Advisory and bottom: 4mm
Distance between Slam Films and bottom: 4mm
- Skeletor VS Beastman
- The Bam and Vallely VS black cop is at 7:08 right after the other argument with the guy with the van in
the parking lot.
- The skateparts listed above for the 1st print are now compiled into one, and the music has been
replaced to HIM - Buried Alive By Love (7:40 - 12:17).
- Shot of KeyKey before the Element Rooftop part (24:01)
- The scene with the old security lady goes to call the law at 23:46. This scene was not in the 1st print.
- The "Fire In The Hole" drivethru prank in the extras is for some reason cut out, but the scene is still
listed in the scene selection.
- Nothing is blurred and all the easter eggs is there.
Runtime - 1:08:52. The GG Allin part runs normal like a part of the main feature and is calculated into
the total runtime.
Bonuses (20:09minutes) plays automatically after the main movie. Shorter than previous releases
because "Fire In The Hole" is missing and "Shopping Cart Drama" is cut.
_______________________________________________________________________________

The Skeletor vs Beastman is still listed as a selectable chapter on the fourth edition release of the DVD, but the skateboarding footage is shown.
 

==CKY: The Greatest Hits==

On November 2, 2014 MTV premiered a special in dedication of the CKY Videos, featuring new interviews of the crew, as well us un-released footage documenting the series as a whole.  The special was directed by long time CKY Crew member and cinematographer Joe Frantz. 

==Upcoming CKY Documentary Feature Film (2017)==
Joe Frantz has recently confirmed that he is currently directing a documentary feature film about CKY, which will depict the CKY Crews rise to pop culture, as well as the problems that swift fame brought to their friendships, relationships, and families. This documentary is calculated for release upon the commemoration of the 20th anniversary of CKY, approximately late 2017. The video will have unreleased footage of Ryan Dunn, Bam Margera, Chris Raab, Rake Yohn, MacGregor Huston, Brandon Novak, and Brandon Dicamillo.

==CKY Blu Ray Re-Releases==

The CKY   as well. On January 19, 2014, Frantz gave an update on the project via his Facebook page stating:   


==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 