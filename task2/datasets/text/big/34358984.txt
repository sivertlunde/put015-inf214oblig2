God's Country (1946 film)
{{Infobox film
| name           = Gods Country
| image          =
| caption        =
| director       = Robert Emmett Tansey
| producer       = William B. David
| writer         = James Oliver Curwood (novel) Frances Kavanaugh (screenplay) Robert Lowery Helen Gilbert Buster Keaton
| music          =
| cinematography = Marcel Le Picard Carl Wester
| editing        = Martin G. Cohn George McGuire
| distributor    = Screen Guild Productions
| released       =  
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Robert Lowery, Helen Gilbert and Buster Keaton.  It is a low-budget B Western set in the contemporary American West.

==Plot==
Lee Preston, aka Leland Bruce (Robert LOwery), kills a man in self-defense but flees to the redwood country when the law makes it a murder charge. There he meets Lynn OMalley (Helen Gilbert, the niece of "Sandy" McTavish (William Farnum) who runs the trading post. Lee learns the reason why this is good trapping country is because the timber barons across the lake are ruthlessly cutting the trees and driving the animals across the river. The trappers appeal to him to take a petition to the Governor which would prohibit the timber people from coming to their side of the lake. At first, because he is a wanted man, he refuses but does so later for the sake of the people even though he knows it will lead to his arrest.

==Cast== Robert Lowery as Lee Preston/Leland Bruce
*Helen Gilbert as Lynn OMalley/McTavish
*William Farnum as Sandy McTavish
*Buster Keaton as Old Tarp/Mr. Boone
*Si Jenks as Timber Cross
*Stanley Andrews as Howard King
*Al Ferguson as Turk Monroe
*Trevor Bardette as White Cloud
*Estelita Zarco as River Squaw
*Ace the Wonder Dog as Ace
*Jimmy the Crow as Jim

==External links==
*  

 
 
 
 
 
 


 