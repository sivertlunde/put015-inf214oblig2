Chemical Wedding (film)
{{Infobox Film
| name               = Chemical Wedding
| image              = Chemical-wedding.jpg
| caption            = Theatrical poster Paul McDowell Mike Shannon Bruce Dickinson Julian Doyle
| writer             = Bruce Dickinson Julian Doyle
| editing            = 
| producer           = David Pupkewitz Malcolm Kohll Ben Timlett Justin Peyton
| studio             = Bill&Ben Productions Focus Films
| distributor        = Anchor Bay Entertainment  (U.S.) 
| country            = United Kingdom
| released           = 4 May 2008
| runtime            = 
| language           = English
| budget             = 
}}
 Julian Doyle. heavy metal The Chemical Wedding in 1998, which, despite sharing the title and title track from the films soundtrack, is otherwise unrelated.

==Plot== VR machine, Professor Oliver Haddo, a modern Cambridge scholar, becomes possessed by the spirit of the infamous occultist Aleister Crowley, as the machines program has been corrupted by a former follower of Crowley. Resurrected 50 years after his death, Crowley begins his occult practices anew, seeking a new "scarlet bride" whom he can marry in an occult ceremony which will increase his power.

==Cast==
*Simon Callow as Professor Oliver Haddo/Aleister Crowley
*Kal Weber as Dr. Joshua Mathers
*Lucy Cudden as Lia Robinson
*Jud Charlton as Victor Nuberg Paul McDowell as Symonds
*John Shrapnel as Aleister Crowley (original)
*Terence Bayler as Professor Brent Mike Shannon as Alex
*Bruce Dickinson as Crowleys landlord and as a blind man

==Production== Andy Taylor, Paul Astrom-Andrews and Peter Dale.

Warner Music released the films soundtrack in the UK, while Edward Noeltners Cinema Management Group handled international sales.   The film received its world première at the Sci-Fi-London film festival on 4 May 2008. 

According to Rockerparis, Iron Maiden lead singer Bruce Dickinson was in Paris, France, on 26 November to promote the films DVD release. The screening and press conference were held in a private cinema in front of Europe 1 radio near the Champs Elysées.  Dickinson, who has a small cameo role in the film, has stated that, "On several levels, I think it will be nice for them ( ) to see somebody from Maiden doing something else that gets the bands name out there and also potentially gets a bit of respect for heavy metal and all the rest of it....But, in addition, I think theyll just enjoy it. Its a rollicking good story." 

== Reception ==
Channel 4 stated that "The look and feel of Chemical Wedding is evidently an homage to Hammer and early 1970s Brit horror-fantasy in general: that is to say, cheap. And though aiming to titillate, the execution is so naff it might as well be renamed Confessions Of A Cabbalist.". 

Horror.com, however, praised the film, calling it "a mixed bag of tricks to be sure, but its worth a look for the curio factor. (At least its not a remake, a J-horror knock-off, or torture porn.)" 

==Soundtrack==
Track Listing:

# "Chemical Wedding" – Bruce Dickinson
# "Hush Hush Here Comes the Bogie Man" – Henry Hall / Val Rosing
# "Fanlight Fanny" – George Formby Man of Sorrows" – Bruce Dickinson The Wicker Man" – Iron Maiden
# "Can I Play with Madness" – Iron Maiden
# "Separation" – Skin
# "Prélude à laprès-midi dun faune" – Debussy
# "The Hallelujah Chorus" – Handel
# "(Excerpt) Violin Concerto" – Mozart

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 