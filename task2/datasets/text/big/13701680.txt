Be My Guest (film)
{{Infobox film
| name           =Be My Guest 
| image          =Be My Guest 1965 film.jpg 
| image_size     = 
| caption        =DVD cover design 
| director       = Lance Comfort
| producer       =Lance Comfort 
| writer         =Lyn Fairhurst (story and screenplay)
| narrator       =  David Healy, Steve Marriott, John Pike
| music          = Shel Talmy
| cinematography = Basil Emmott
| editing        = 
| distributor    = Rank Organisation
| released       =1965, United Kingdom March 1965 USA 
| runtime        =82 minutes 
| country        =United Kingdom
| language       =English 
| budget         = 
| gross          = 
}} British black Humble Pie; and for the appearance of Jerry Lee Lewis who was at the peak of his career at the time of filming. It was released as a B movie to support the Morecambe and Wise feature film The Intelligence Men. 
 Live It Up!.

Noted American rock music producer Shel Talmy coordinated the films musical score. 

Talmy also composed the title music which was performed by The Niteshades, who also appeared in the closing scene. The recording, released on CBS Records, reached no.32 on pirate radio station Radio Londons chart in the summer of 1965.

==Cast==

===actors===
*David Hemmings as Dave Martin
*Steve Marriott as Ricky
*John Pike as Phil
*Andrea Monet as Erica
*Ivor Salter as Herbert Martin Diana King as Margaret Martin
*Avril Angers as Mrs. Pucil
*Joyce Blair as Wanda David Healy as Hilton Bass
*Tony Wager as Artie
*David Lander as Routledge
*Robin Stewart as Matthews
*Monica Evans as Dyllis
*Douglas Ives as Stewars

==Music performers==

=== rest of cast listed alphabetically ===
*Ken Bernard Himself - singer (as Kenny and the Wranglers)
*John Carpenter Himself - drummer of The Zephyrs (as Slash Wildly and the Cut-Throats)
*Terry Crowe	 singer (as The Plebs)
*Mick Dunford  guitarist (as The Plebs)
*John Hawken  pianist (as The Nashville Teens)
*John Hinde   bassist of The Zephyrs (as Slash Wildly and the Cut-Throats)
*Mike Lease   organist of The Zephyrs (as Slash Wildly and the Cut-Throats)
*Jerry Lee Lewis with The Nashville Teens
*Danny McCulloch Himself - bassist (as The Plebs)
*The Nashville Teens  
*John Peeby  guitarist of The Zephyrs (as Slash Wildly and the Cut-Throats) Ray Phillips  vocalist (with The Nashville Teens)
*Pete Shannon  bassist (with The Nashville Teens)
*Arthur Sharp  singer (with The Nashville Teens)
*Howard Roberts organist (with The Niteshades)
*Martin Davies bassist (with The Niteshades)
*Tony Lopez singer (with The Niteshades)
*Ricky King guitarist (with The Niteshades)
*Ed Sones singer (with The Niteshades)
*Keith Harvey drummer (with The Niteshades)

==References==
 

==External links==
* 
* 
*  

 

 
 
 
 
 
 