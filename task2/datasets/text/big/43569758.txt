Call Girl of Cthulhu
{{Infobox film
| name           = Call Girl of Cthulhu
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Chris LaMartina
| producer       = 
| writer         = Jimmy George Chris LaMartina
| screenplay     = 
| story          = 
| based on       =  
| starring       = David Phillip Carollo Melissa OBrien Nicolette le Faye
| narrator       = 
| music          = Chris LaMartina
| cinematography = Nick Baldwin
| editing        = Chris LaMartina
| studio         = Midnight Crew Studios
| distributor    = Camp Motion Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Call Girl of Cthulhu is a 2014 horror movie that was directed by Chris LaMartina.   The film had its world premiere on May 8, 2014 at the Maryland Film Festival and stars David Phillip Carollo as an artist that falls in love with a woman that ends up being the chosen bride of Cthulhu.  The movie is very loosely based on the writings of H. P. Lovecraft. 

==Synopsis==
Artist Carter (David Phillip Carollo) wants desperately to lose his virginity, but only to the right person. He thinks that hes found the right person in Riley (Melissa OBrien), a call girl with a strange birthmark on her rear end. Carters dreams of romance are soon ruined, as several Cthulhu-worshiping cultists see this birthmark as a sign that Riley is destined to become the bride of Cthulhu and bear his child. Its up to Carter, aided by Edna Curwen (Helenmary Ball) and Squid (Sabrina Taylor-Smith), to find a way to stop the cult from fulfilling their plans.

==Cast==
*David Phillip Carollo as Carter Wilcox
*Melissa OBrien as Riley Whatley
*Nicolette le Faye as Erica Zann
*Dave Gamble as Sebastian Suydum
*Helenmary Ball	as Professor Edna Curwen
*Sabrina Taylor-Smith as Squid
*Alex Mendez as Rick The Dick Pickman
*Craig Peter Coletta as Wilbur
*Elena Rose as Whitney
*George Stover as Walter Delapore
*Leanna Chamish as Detective Rita LaGrassi
*Troy Jennings as Ashton Eibon
*Stephanie Anders as Missy Katonixx
*Ruby Larocca as Billie
*Scarlett Storm as Georgia

==Reception==
Daily Dead gave Call Girl of Cthulhu a score of 3.5 (out of 5), stating that while the movie was "not for everyone" it would appeal to "those of you Lovecraft (and horror) fans out there looking for something just a little bit outside the norm".  Fangoria gave the movie three out of four skulls and praised its acting. 

==References==
 

==External links==
*  
*  
*  

 
 