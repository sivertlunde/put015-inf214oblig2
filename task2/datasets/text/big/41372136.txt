Manolesta
{{Infobox film
 | name = Manolesta
 | image = Manolesta.jpg
 | caption =
 | director = Pasquale Festa Campanile
 | writer =  
 | starring =  
 | music =  Detto Mariano
 | cinematography = Giancarlo Ferrando
 | editing =  
 | language = Italian 
 }}
Manolesta is a 1981 Italian comedy film directed by Pasquale Festa Campanile.    

== Plot ==
Gino Quirino aka Manolesta, a thief and a con man, lives on a barge anchored on the Tiber with Bruno, his young son. Gino ends up in the crosshairs of the social worker Dr. Angela De Maria, and he is forced to find an honest job.

== Cast ==

* Tomas Milian: Gino Quirino
* Giovanna Ralli: Dr. Angela De Maria
* Paco Fabrini: Bruno 
* Clara Colosimo: social worker
* Nello Pazzafini: Atacs driver
* Ennio Antonelli

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 