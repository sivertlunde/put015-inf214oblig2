Classic Dance of Love
{{Infobox film
| name           = Classic - Dance of Love
| image          = ClassicMithun.jpg
| director       = Babbar Subhash
| producer       = Babbar Subhash
| writer         = Babbar Subhash
| narrator       = 
| starring       = Mithun Chakraborty Rajesh Khanna Vikas Bharadwaj Meghna Naidu Naveen Nischol
| music          = Bappi Lahiri Sukhwinder Singh  
| Lyrics         = Sameer, Sukhwinder Singh, Babbar Subhash 
| cinematography = Manoj Gupta
| editing        = Arun
| genre          = Action
| distributor    = B.Subhash Movie Unit
| released       = March 18, 2005
| runtime        = 145 minutes
| country        = India
| language       = Hindi
| awards         = 
| budget         = 4 Crores
}}
 2005 Hindi Indian feature directed by Babbar Subhash, starring Mithun Chakraborty, Vikas Bhardwaj and Meghna Naidu with Shashi Kiran, Navin Nischol and Rajesh Khanna featuring in supporting roles. Mithun Chakraborty is the antagonist as well as the protagonist in the movie. 
 Dance Dance.

== Synopsis ==

Guru ,Dr. Ram Gopal Acharya (Mithun Chakraborty) teaches his disciples the core difference between paap (Sin) and Punya (Goods Deeds). One of his lifes thinking is that man should never succumb to Temptation and Sex to “Sinners” and only women is responsible to him to seduce men for money and ultimately destroy them.

Doli (Meghna Naidu) , bar dancer dances to entertain her customer, drawing false vision of lust and arousing passion in men through her sensual figure movements keeping dreams some day her Prince charming, riding on a white horse would come and take her away. Meanwhile she meets Suraj (Vvikas Bhardwaj), a London bases NRI comes India for business. Love blossoms between the two and Suraj transforms Doli from a symbol of disgrace into a symbol of pure and noble love.

But the unfortunately Dr. Acharyas loyalty, rather better to tell the wicked side of him , towards his friend J.K.Malhotra (Navin Nischol), father of Suraj, evokes to insult Doli and calls her a Prostitute, Sinner, Lady of the night, Adulteress and blames her for tarnishing Surajs reputation.Fatigued Doli finds no way other than the decision to taking refuge in Acharyas ashram. A victorious smile cures his lips, as Acharya cannot resist the lure, the forbidden joy of passion, which he always condemned - SEX.

Doli gradually realise Dr. Acharyas lustful intention to his disciples. Suraj rescues Doli from the evil clutches of Acharya as he tries to forcefully marry her. His heart is set on fire with the longing for Doli and his feet begin to quiver to the tune of Wild Romance, Sex and passion performing this story.

== Cast ==

* Mithun Chakraborty ...  Dr. Ramgopal Acharya
* Rajesh Khanna ...
* Vikas Bhardwaj ...  Suraj J. Malhotra (as Vvikas Bhardwaj)
* Meghna Naidu ...  Doli
* Krishna Bhatt
* Dinesh Hingoo ...  Sitaras assistant
* Shashi Kiran ...  Sadanand
* Vivek Mishran ...
* Navin Nischol ...  J.K. Malhotra
* Gyan Prakash
* Mansee Shah
* Himani Shivpuri ...  Sitaradevi

==Songs==
#  Mohabbat Hojaye - Sukhwinder Singh
# Classic Dance Of Love - Sukhwinder Singh
# Saathi Mere - Kailash Kher
# Main Tujhpe Qurbaan - Sukhwinder Singh
# Saiyaji - Shreya Ghoshal
# Aa Mujhe Dekh - Sunidhi Chauhan

== External links ==
*  
* http://www.bollydvd.net/Classic-Dance-Of-Love-1146_0

 
 
 
 