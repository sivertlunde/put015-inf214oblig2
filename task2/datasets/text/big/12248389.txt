Tom Sawyer (1907 film)
Tom Sawyer is a 1907 American silent film based on The Adventures of Tom Sawyer made by Kalem Studios in New York City, and was the first time Mark Twains character had appeared on film.

Very little else is currently known about the film, other than the screenplay was written by Gene Gauntier, the first of over 300 screenplays she eventually wrote.

==References==
*{{cite web | title = Tom Sawyer (1907) entry on IMDB | url =http://www.imdb.com/title/tt0255651/ |
accessdate = 2007-07-14}}
* 
*{{cite web | last = Gauntier | first = Gene | title = Blazing the Trail |
url =http://www.cinemaweb.com/silentfilm/bookshelf/4_blaze2.htm | accessdate = 2007-07-14}}

 

 
 
 
 
 
 
 
 
 
 
 
 


 