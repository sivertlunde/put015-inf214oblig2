East Side Story (1997 film)
{{Infobox film
| name           = East Side Story
| image          = 
| image_size     = 
| caption        = 
| director       = Dana Ranga
| producer       = Dana Ranga and Andrew Horn
| writer         = 
| narrator       = 
| starring       = 
| music          =
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1997
| runtime        = 75 min.
| country        = Germany
| language       = English, Russian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
East Side Story is a 1997 documentary directed by Dana Ranga. The film documents the Soviet Bloc musical genre, which first appeared under Stalin and spread to Soviet-occupied Eastern Europe. The film features interviews with actors, film historians, and audience members who reminisce on these unlikely films and their impact on Soviet Bloc life.

==Synopsis==
The film first looks at Grigori Alexandrov, a director who made his career in the 1930s musicals. He directed the unlikely hit  ] (1934), which had the backing of Maxim Gorky and the personal approval of Stalin. Alexandrov went on to create other propagandistic musicals, including Volga, Volga (1938), which details the story of peasants who boat to Moscow on the River Volga to become singers. These musicals starred his wife, Lyubov Orlova.
 My Wife Midnight Revue Beloved White Hot Summer (1968).

Many of the films were quite popular, but relatively few were made because their themes did not fit comfortably with the canons of Socialist realism.

==External links==
* 

 
 
 
 
 
 
 
 
 


 