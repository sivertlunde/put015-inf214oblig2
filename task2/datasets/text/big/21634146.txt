The Brute (1961 film)
{{Infobox Film
| name           = The Brute
| image          = 
| image_size     = 
| caption        = 
| director       = Zoltán Fábri
| producer       = 
| writer         = Zoltán Fábri Imre Sarkadi
| narrator       = 
| starring       = Ferenc Bessenyei
| music          = 
| cinematography = Ferenc Szécsényi
| editing        = Ferencné Szécsényi
| distributor    = 
| released       = 25 May 1961
| runtime        = 96 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Brute ( ) is a 1961 Hungarian film directed by Zoltán Fábri. It was entered into the 1961 Cannes Film Festival.     

==Cast==
* Ferenc Bessenyei - Ulveczki Sándor
* Tibor Bitskey - Gál Jani
* Mária Medgyesi - Monoki Zsuzsi
* Béla Barsi - Bíró
* György Györffy - Balogh
* Pál Nádai - Földházi
* Sándor Siménfalvy - Monoki, Zsuzsi apja
* Antal Farkas - Szûcs

==References==
 

==External links==
* 

 
 
 
 
 
 
 