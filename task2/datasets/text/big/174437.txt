Oh! What a Lovely War
 
 
 
{{Infobox film
| name = Oh! What a Lovely War
| image = Oh_what_a_lovely_war.jpg
| caption = Theatrical release poster
| director = Richard Attenborough Brian Duffy
| writer = Len Deighton (uncredited)
| starring = Dirk Bogarde Phyllis Calvert Jean Pierre Cassel John Clements John Gielgud Jack Hawkins Kenneth More Laurence Olivier Michael Redgrave Vanessa Redgrave Ralph Richardson Maggie Smith Susannah York John Mills
| music =
| cinematography = Gerry Turpin Kevin Connor
| studio  = Accord Productions
| distributor = Paramount Pictures
| released = 10 March 1969 (UK release)
| runtime = 144 min
| country = UK
| language = English
| budget =
}} Edward Fox, Susannah York, John Clements, Phyllis Calvert and Maurice Roëves.
 stage musical Oh, What a Lovely War!, originated by Charles Chilton as a radio play, The Long Long Trail in December 1961,     and transferred to stage by Gerry Raffles in partnership with Joan Littlewood and her Theatre Workshop in 1963. 

The title is derived from the music hall song Oh! Its a Lovely War, which is one of the major numbers in the film.

==Synopsis==
Oh! What A Lovely War summarises and comments on the events of World War I using popular songs of the time, many of which were parodies of older popular songs, and using allegorical settings such as Brightons West Pier to criticise the manner in which the eventual victory was won.

The diplomatic manoeuvrings and events involving those in authority are set in a fantasy location inside the pierhead pavilion, far from the trenches. In the opening scene various Foreign Ministers, generals and Heads of State walk over a huge map of Europe, reciting actual words spoken by these figures at the time. An unnamed photographer takes a picture of Europes rulers – after handing two red poppies to the   his Foreign Czar Nicholas Kaiser Wilhelm Sir Edward Grey little choice but to get involved. Italy reneges on her alliance with the Central Powers (she joined the Allies in 1915) but Turkey joins them instead.
 General Haig selling tickets – the film later follows the young Smith men through their experiences in the trenches. A military band rouses holidaymakers from the beach to rally round and follow – some even literally boarding a bandwagon. The first Battle of Mons is similarly cheerfully depicted yet more realistic in portrayal. Both scenes are flooded in pleasant sunshine.

When the casualties start to mount, a theatre audience is rallied by singing "Are We Downhearted? No!" A  .

The red poppy crops up again as a symbol of impending death, often being handed to a soldier about to be sent to die. These scenes are juxtaposed with the pavilion, now housing the top military brass. There is a scoreboard (a dominant motif in the original theatre production) showing the loss of life and yards gained.

Outside, Sylvia Pankhurst (Vanessa Redgrave) is shown addressing a hostile crowd on the futility of war, upbraiding them for believing everything they read in the newspapers. She is met with catcalls and jeered off her podium.
 Adieu la Field Marshal Sir John French as Commander-in-Chief of the British Forces. Haig is then mocked by Australian troops who see him inspecting British soldiers; they sing "They were only playing Leapfrog" to the tune of "John Brown".

An interfaith religious service is held in a ruined abbey. A priest tells the gathered soldiers that each religion has endorsed the war by way of allowing soldiers to eat pork if Jewish, meat on Fridays if Catholic, and work through the sabbath if in service of the war for all religions. He also says the Dalai Lama has blessed the war effort.

1916 passes and the films tone darkens again. The songs contain contrasting tones of wistfulness, stoicism, and resignation; including "The Bells of Hell Go Ting-a-ling-a-ling", "If The Sergeant Steals Your Rum, Never Mind" and "Hanging on the Old Barbed Wire". The wounded are laid out in ranks at the field station, a stark contrast to the healthy rows of young men who entered the War. The camera often lingers on Harry Smiths silently suffering face.

The Americans arrive, but are shown only in the disconnected reality of the pavilion, interrupting the deliberations of the British generals by singing "Over There" with the changed final line: "And we wont come back – well be buried over there!" The resolute-looking American captain seizes the map from an astonished Haig.

Jack notices with disgust that after three years of fighting, he is literally back where he started, at Mons. As the Armistice is sounding, Jack is the last one to die. There is a splash of red which at first glance appears to be blood, but which turns out to be yet another poppy out of focus in the foreground. Jacks spirit wanders through the battlefield, and he eventually finds himself in the room where the elder statesmen of Europe are drafting the coming peace - but they are oblivious to his presence. Jack finally finds himself on a tranquil hillside, where he joins his friends for a lie down on the grass, where their figures morph into crosses. The film closes with a long slow pan out that ends in a dizzying aerial view of countless soldiers graves, as the voices of the dead sing "Well Never Tell Them" (a parody of the Jerome Kern song "They Didnt Believe Me").

==Cast (in credits order)==

===Smith family===
*Wendy Allnutt as Flo Smith Colin Farrell the Irish actor of the same name) as Harry Smith
*Malcolm McFee as Freddie Smith John Rae as Grandpa Smith
*Corin Redgrave as Bertie Smith
*Maurice Roëves as George Smith
*Paul Shelley as Jack Smith Kim Smith as Dickie Smith
*Angela Thorne as Betty Smith
*Mary Wimbush as Mary Smith

===Also starring===
 
*Vincent Ball as Australian Soldier
*Pia Colombo as Estaminet Singer Czar Nicholas II
*Isabel Dean as Sir John Frenchs Lady
*Christian Doermer as Fritz
*Robert Flemyng as Staff Officer in Gassed Trench
*Meriel Forbes as Lady Grey President Poincaré David Lodge as Recruiting Sergeant
*Joe Melia as the Photographer Sir William Robertson
*Juliet Mills as Nurse
*Nanette Newman as Nurse
*Cecil Parker as Sir John
*Natasha Parry as Sir William Robertsons Lady
*Gerald Sim as Chaplain
*Thorley Walters as Staff Officer in Ballroom
*Dinny Jones as Chorus Girl 
*Anthony Ainley as Third Aide
*Penelope Allen as Solo Chorus Girl
*Maurice Arthur as Soldier Singer
*Freddie Ascott as Whizzbang Soldier Michael Bates as Drunk Lance Corporal
*Fanny Carby as Mill Girl
*Cecilia Darby as Sir Henry Wilsons Lady
*Geoffrey Davies as Aide Edward Fox as Aide
*George Ghent as Heckler
*Peter Gilmore as Private Burgess Ben Howard as Private Garbett Norman Jones as Scottish Soldier Paddy Joyce as Irish Soldier
*Angus Lennie as Scottish Soldier
*Harry Locke as Heckler
*Clifford Mollison as Heckler
*Derek Newark as Shooting Gallery Proprietor John Owens as Seamus Moore
*Ron Pember as Corporal at Station
*Dorothy Reynolds as Heckler
*Norman Shelley as Staff Officer in Ballroom
*Marianne Stone as Mill Girl
*John Trigger as Officer at Station
*Kathleen Wileman as Emma Smith at Age 4
 

===Guest stars===
*Dirk Bogarde as Stephen
*Phyllis Calvert as Lady Haig
*Jean-Pierre Cassel as French Colonel
*John Clements as General von Moltke
*John Gielgud as Count Leopold Berchtold Emperor Franz Josef I Kaiser Wilhelm II Field Marshal Sir John French Sir Henry Wilson (incorrectly shown as a full General, a rank he did not achieve until late 1917)
*Vanessa Redgrave as Sylvia Pankhurst Sir Edward Grey
*Maggie Smith as Music Hall Star
*Susannah York as Eleanor General (later Field Marshal) Sir Douglas Haig

==Production== Brian Duffy, Only When I Larf starring Richard Attenborough.  Deighton wrote the screenplay for Oh! What a Lovely War and the opening title sequence was created by Len Deightons lifelong friend Raymond Hawkey, the designer responsible for many of Deightons book covers in the 1960s.  In an attempt to shame other people whom he thought were claiming credit for things they hadnt actually done, Deighton decided not to be listed in the film credits, a gesture he later described as "stupid and infantile". 
 motifs from the stage production. These included the cricket scoreboards showing the number of dead, but Attenborough did not use the pierrot costumes. However, as many critics, including Pauline Kael,  noted, the treatment diminished the effect of the numbers of deaths, which appear only fleetingly. Nonetheless, Attenboroughs final sequence, ending in a helicopter shot of thousands of war graves is regarded as one of the most memorable moments of the film. According to Attenborough, sixteen thousand white crosses had to be hammered into individually dug holes due to the hardness of the soil. Although this is effective in symbolising the scale of death, the number of crosses was in fact less than the number of deaths in a single battle.
 Brighton station and Ovingdean (where hundreds of crosses were erected for the classic finale). 

==The song== male impersonator Ella Shields.  The first verse and the chorus follow:

:Up to your waist in water,
:Up to your eyes in slush –
:Using the kind of language,
:That makes the sergeant blush;
:Who wouldnt join the army?
:Thats what we all inquire,
:Dont we pity the poor civilians sitting beside the fire.

:Chorus
:Oh! Oh! Oh! its a lovely war,
:Who wouldnt be a soldier eh?
:Oh! Its a shame to take the pay.
:As soon as reveille is gone
:We feel just as heavy as lead,
:But we never get up till the sergeant brings
:Our breakfast up to bed
:Oh! Oh! Oh! its a lovely war,
:What do we want with eggs and ham
:When weve got plum and apple jam?
:Form fours! Right turn!
:How shall we spend the money we earn?
:Oh! Oh! Oh! its a lovely war.

Two pre-musical renditions, one from 1918, can be found at Firstworldwar.com.  Almost all of the songs featured in the film also appear on the CD41 album series Oh! Its A Lovely War (four volumes). 

==Box Office==
The film was the 16th most popular movie at the UK box office in 1969. 
==Awards==
* Golden Globe, Best Cinematography (Gerry Turpin) 1969
* BAFTA Film Award, Best Art Direction (Donald M. Ashton) 1970
* BAFTA Film Award, Best Cinematography (Gerry Turpin) 1970
* BAFTA Film Award, Best Costume Design (Anthony Mendleson) 1970
* BAFTA Film Award, Best Sound Track (Don Challis and Simon Kaye) 1970
* BAFTA Film Award, Best Supporting Actor (Laurence Olivier) 1970

==References in popular culture==
*BBC Radio 4s 15 Minute Musical portrayed Tony Blairs premiership in the style of Oh! What a Lovely War in a September 2006 episode entitled "Oh! What a Lovely Blair" Hair stated that "what a Lovely War" was what made him want to work on a musical dealing with war at a Google Talks event. 
*The song The Bells of Hell Go Ting-a-ling-a-ling was used as the play-out music for Ned Sherrins 1964 BBC-TV show Not So Much a Programme, More a Way of Life.
*Babyshambles named their live album "oh what a lovely tour" after this film.

==Notes==

===References===
 

===Sources===
* Banham, Martin, ed. 1998. The Cambridge Guide to Theatre. Cambridge: Cambridge University Press. ISBN 0-521-43437-8.
* Brockett, Oscar G. and Franklin J. Hildy. 2003. History of the Theatre. Ninth edition, International edition. Boston: Allyn and Bacon. ISBN 0-205-41050-2.
* Richard Eyre|Eyre, Richard and Nicholas Wright. 2000. Changing Stages: A View of British Theatre in the Twentieth Century. London: Bloomsbury. ISBN 0-7475-4789-0.

==External links==
* 
* 
* 

 
 

 

 

 
 
 
 
 
 
 
 
 
 
 