Christmas at Camp 119
{{Infobox film
| name           = Christmas at Camp 119
| image          = Christmas at Camp 119.jpg
| caption        = Film poster
| director       = Pietro Francisci
| producer       = Giuseppe Amato
| writer         = Giuseppe Amato Oreste Biancoli Vittorio De Sica Aldo Fabrizi
| starring       = Aldo Fabrizi
| music          = Angelo Francesco Lavagnino
| cinematography = Mario Bava Ferrer Tiezzi
| editing        = Gabriele Varriale
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Christmas at Camp 119 ( ) is a 1948 Italian comedy-drama film directed by Pietro Francisci and starring Aldo Fabrizi,  Vittorio De Sica and Peppino De Filippo.   

==Cast==

*Aldo Fabrizi as Giuseppe Mancini 
*Vittorio De Sica: Don Vincenzino 
*Peppino De Filippo: Gennarino Capece 
*Carlo Campanini: Scapizzono 
*Massimo Girotti: Nane 
*Alberto Rabagliati: Alberto 
*Carlo Mazzarella: Ignazio
*Aldo Fiorelli: Guido 
*Vera Carmi: The schoolteacher
*Margherita Bagni: Donna Clara
*Rocco DAssunta: Lojacono 
*Olga Villi: Mirella 
*María Mercader: Fiammetta
*Nando Bruno: Guide of Roma
*Adolfo Celi: John 
*Ave Ninchi: Miss Mancini
*Giacomo Rondinella: The Neapolitan Singer
* Pietro De Vico

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 