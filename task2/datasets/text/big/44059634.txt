Snehathinte Mukhangal
{{Infobox film
| name           = Snehathinte Mukhangal
| image          =
| caption        = Hariharan
| producer       = KC Joy
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan Madhu Jayabharathi Janardanan
| music          = M. S. Viswanathan
| cinematography = TN Krishnankutty Nair
| editing        = MS Mani
| studio         = Priyadarsini Movies
| distributor    = Priyadarsini Movies
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, Hariharan and produced by KC Joy. The film stars Prem Nazir, Madhu (actor)|Madhu, Jayabharathi and Janardanan in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
*Prem Nazir Madhu
*Jayabharathi
*Janardanan
*Kanakadurga Seema
*Vincent Vincent

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Mankombu Gopalakrishnan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Arariro En Janmasaabhalyam || P Susheela || Mankombu Gopalakrishnan ||
|-
| 2 || Arayarayo kingini arayo || P Susheela, Chorus, Jolly Abraham || Mankombu Gopalakrishnan ||
|-
| 3 || Gangayil Theerthamaadiya || P Susheela || Mankombu Gopalakrishnan ||
|-
| 4 || Jik Jik Theevandi || P Jayachandran, Ambili || Mankombu Gopalakrishnan ||
|-
| 5 || Pookkaalam Ithu pookkaalam || K. J. Yesudas || Mankombu Gopalakrishnan ||
|}

==References==
 

==External links==
*  

 
 
 


 