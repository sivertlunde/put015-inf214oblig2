Flower & Garnet
 
{{Infobox film
| name           = Flower & Garnet
| image          = Flower & Garnet poster.jpg
| director       = Keith Behrman
| producer       = Trish Dolman
| writer         = Keith Behrman
| starring       = Callum Keith Rennie Jane McGregor Colin Roberts  Dov Tiefenbach Peter Allen
| cinematography = Steve Cosens
| editing        = Michael John Bateman
| distributor    =
| released       = 26 August 2002 (Montréal Film Festival)
| runtime        = 103 min.
| country        = Canada
| language       = English
| budget         =
| gross          =
| website        =
}}
 Canadian film directed by Keith Behrman.

==Plot==
A father finds difficulties in expressing his love to his children. Garnet (played by Colin Roberts) and Flower (Jane McGregor) have grown up in an environment of stifled grief. Since their mother died, Ed (Callum Keith Rennie), their father, mostly just lives without a goal. Eight-year-old Garnet struggles to comprehend the world around him, while sixteen-year-old Flower seeks love with her new boyfriend. Forced to become a real parent to Garnet, Ed buys Garnet a gun and shows, for the first time, his real affection for the boy.

==Awards== Peter Allen won a Leo Award for his film score in 2003.   

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 