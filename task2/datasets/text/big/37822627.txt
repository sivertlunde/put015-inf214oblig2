The Old Curiosity Shop (1914 film)
{{Infobox film
| name           = The Old Curiosity Shop
| image          =
| caption        =
| director       = Thomas Bentley
| producer       = 
| writer         = Charles Dickens (novel)   Thomas Bentley
| starring       = Mai Deacon Warwick Buckland Alma Taylor
| music          =
| cinematography = 
| editing        = 
| studio         = Hepworth Company
| distributor    = 
| released       = 1914
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama film directed by Thomas Bentley and starring Mai Deacon, Warwick Buckland and Alma Taylor. It was based on the 1840 novel The Old Curiosity Shop by Charles Dickens, and was the first of three film adaptations of the story by Bentley. It was made by the Hepworth Company, the leading British film studio before the First World War.

==Cast==
* Mai Deacon as Little Nell
* Warwick Buckland as Grandfather Trent
* E. Felton as Quilp
* Alma Taylor as Mrs. Quilp
* Jamie Darling as The Single Gentleman
* Willie West as Dick Swiveller
* Billy Rex as Tom Codlin
* S. May as Sampson Brass
* Bert Stowe as Short
* Sydney Locklynne as Jerry
* Moya Nugent as Marchioness
* R. Phillips as Mrs. Jarley

==Bibliography==
* Giddings, Robert & Sheen, Erica. From Page To Screen: Adaptations of the Classic Novel . Manchester University Press, 5 May 2000
* Mee, John. The Cambridge Introduction to Charles Dickens. Cambridge University Press, 2010.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 