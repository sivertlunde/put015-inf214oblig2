Bad News Bears
 
 
{{Infobox film
| name = Bad News Bears
| image = Bad News Bears film.jpg
| alt = 
| caption = Theatrical release poster
| director = Richard Linklater
| producer = {{Plainlist|
* Richard Linklater
* J. Geyer Kosinski}}
| screenplay = {{Plainlist|
* Bill Lancaster
* Glenn Ficarra
* John Requa}}
| based on =  
| starring = {{Plainlist| 
* Billy Bob Thornton
* Greg Kinnear
* Marcia Gay Harden}}
| music = Ed Shearmur
| cinematography = Rogier Stoffers
| editing = Sandra Adair
| distributor = Paramount Pictures
| released =  
| runtime = 113 minutes  
| country = United States
| language = English Spanish
| budget = $35 million 
| gross = $34,252,847 
}} remake of the 1976 comedy film The Bad News Bears, produced by Paramount Pictures. It is directed by Richard Linklater and stars Billy Bob Thornton, Greg Kinnear, Marcia Gay Harden, Sammi Kane Kraft, and Jeffrey Tedmori.

==Plot==
Morris Buttermaker is a washed-up alcoholic minor-league baseball player who was kicked out of professional baseball for attacking an umpire. He works as an exterminator and is a crude womanizer. He coaches the Bears, a childrens baseball team with poor playing skills. They play their first game and do not even make an out before he forfeits the game. Amanda Whurlitzer, a skilled pitcher, is the 12-year-old daughter of one of his ex-girlfriends. At his request, she joins the team. Kelly Leak, a local troublemaker, also joins the team, and the Bears start winning games. After their first victory, Buttermaker takes them to Pizza Hut. The Bears eventually make it to the championship game. In the middle of that game, the Bears and Yankees fight. Later, Buttermaker changes the lineup, putting the benchwarmers in and taking out some of the good players. Garo runs to home plate, slides and gets tagged out. The whole game was riding on his attempt to score, however the Bears lose the game 8 to 7. After the game, Buttermaker gives them non-alcoholic beer, and they spray it all over each other. Although they did not win the championship, they have the satisfaction of trying, knowing that winning is not so important.

==Cast==
 
 
* Billy Bob Thornton as Morris Buttermaker
* Greg Kinnear as Roy Bullock
* Marcia Gay Harden as Liz Whitewood
* Chase Winton as Ms. Cleveland
* Arabella Holzbog as Shari Bullock
* Sonya Eddy as Saleslady
* Pancho Moler as Kevin
* Scott Adsit as Umpire
 
;The Bears
* Jeffrey Tedmori as Garo Daragabrigadien
* Sammi Kane Kraft as Amanda Whurlitzer
* Ridge Canipe as Toby Whitewood
* Brandon Craggs as Mike Engelberg
* Jeffrey Davies as Kelly Leak
* Timmy Deters as Tanner Boyle
* Carlos Estrada as Manuel Agilar
* Emmanuel Estrada as Jose Agilar
* Troy Gentile as Matthew Hooper
* Kenneth "K.C." Harris as Ahmad Abdul Rahim
* Aman Johal as Prem Lahiri
* Tyler Patrick Jones as Timmy Lupus
 
;The Yankees
* Carter Jenkins as Joey Bullock
* Seth Adkins as Jimmy
* Jack Acampora
* Bretton Bowman
* Ryan Cruz
* Nick Lovullo
* Richard Martinez
* Payton Milone
* Ernesto Junior Prado
* Kirby James Shaw
* Cody Ray Thompson
* Hayden Tsutsui
* Matthew Walker
 
;Hooters girls
* Nectar Rose as Paradise
* Lisa Arturo as Peaches
* Elizabeth "Liz" Carter as Chandalier
* Monique Cooper as Lolita
* Candace Kita as China
* Katie Luyben as Daisy
* Shamron Moore as Cherry Pie
 

==Release==
Bad News Bears opened on July 22, 2005 and ranked #5 at the North American domestic box office with $11,382,472.  The film ultimately earned $32,868,349 in North America and $1,384,498 internationally for a worldwide total of $34,252,847, becoming a box office bomb.   

==Critical response==
 
Critical response was mixed. Review aggregation website Rotten Tomatoes gives the film a score of 48% based on reviews from 158 critics.  Metacritic gives the film a score of 65%  based on reviews from 35 critics. 
 Friday Night Lights, yet   doesnt recycle from either movie; he modulates the manic anger of the Santa and the intensity of the coach and produces a morose loser who we like better than he likes himself."  James Berardinelli of ReelViews also gave Bad News Bears three stars out of four, calling it "an entertaining motion picture" that "wont make fans forget the original, but its not so feeble that it disappears into the earlier movies shadow." 

Giving the film two stars out of five, Don R. Lewis of Film Threat said that it has "a few laughs" but that it "just trudges on, going through the motions of the original with no spark" and that it "suffer  from the unbearable, crushing weight of political correctness."  Paula Nechak of the Seattle Post-Intelligencer called the film "simply another in a long line of utterly unnecessary remakes that, having nothing new to say, clutch at crassness and dumbness,"  while Mick LaSalle of the San Francisco Chronicle said that while "the screenplay   makes the most of Thorntons dry, skewed humor,   nothing happens here that would distinguish this film from other sports movies." 

==Legacy==
In a case of life imitating art, in 2012, a Los Angeles-area little league in financial difficulty received a donation from the owner of a local strip club in order to continue operating.  Comparisons were immediately made to a scene in Bad News Bears in which the team received a donation from "Little Bo Peep|Bo-Peeps Gentlemens Club" and is required to put the logo of the strip club on their uniforms.  The real-life club didnt require similar stipulations, preferring to donate in anonymity, however local civic leaders encouraged that the club in question be made public so that they could get the proper recognition they deserved.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 