The Devil's Rain
 
 
{{Infobox film name           = The Devils Rain image          = TheDevilsRain.jpg caption        = Theatrical release poster director       = Robert Fuest producer       = James V. Cullen Michael S. Glick writer         = James Ashton Gabe Essoe Gerald Hopman starring       = William Shatner Ernest Borgnine Tom Skerritt John Travolta Eddie Albert Anton LaVey music          = Al De Lory cinematography = Alex Phillips Jr.
| studio         =Sandy Howard Productions distributor  Bryanston Distributing Company  released       =   runtime        = 86 min. country        = United States language  English
}}
 Satanist Anton LaVey is credited as the films technical advisor and appeared in the film playing a minor role.

==Plot summary==
A curse hovers over the Preston family, caused by their betrayal of the Satanic priest Jonathan Corbis (Ernest Borgnine). Corbis has followed the Preston family for generations, in pursuit of a Satanic book through which he obtains great power. Corbis first captures the father, Steve Preston, who is allowed to escape home to warn his wife and younger son about Corbiss wrath, and to tell them to give the book to Corbis; at which point Steve Preston then melts into a waxy substance, apparently melting in the rain.

Mark Preston (Shatner) takes the book, hoping to meet with Corbis and defeat him. The two eventually meet in a ghost town in the desert, where Corbis gives Preston a drink of water from an old hand-pumped well; Preston drinks and then spits it out, proclaiming the water to be bitter. Corbis smiles and replies, "Sweet way to end a thirst, though."
 1911 .45 pistol on Corbis. Corbis asks, "Is THAT your faith?" at which point Preston tries to escape. Surrounded by his followers, Preston pulls out his cross, which then appears to transform into a snake, and he discards. He is promptly captured by the Satanic followers of Corbis. Corbis later begins a ceremony which wipes Marks memory clean with the "water of forgetfulness" in preparation for a ceremony later that evening.

Meanwhile Prestons older brother, Tom (Tom Skerritt), and his wife Julie, have gone to look for Mark; they are accompanied 
by Dr. Sam Richards (Albert), a psychic researcher. Tom witnesses his brothers total conversion to a soulless minion in a ceremony in which Corbis is so completely taken over by the devil that he transforms into a goat-like being. Tom is discovered but escapes the Satanists, and later meets up with Richards at the Satanic church, where they discover the source of Corbiss power—an ornately-decorated glass bottle known as The Devils Rain, which contains the souls of Corbiss converts.

Corbis and the Satanists then arrive at the church, and Richards threatens to destroy the Devils Rain but is over-powered by the acolytes. He then appeals to Marks lost humanity and convinces him to destroy the bottle, which he does, despite Corbis entreaties.

The Satanists melt in the rain as a storm rages as Tom and his wife make a hasty exit. But as Tom holds his wife, the audience discovers that it is actually Corbis he is embracing and his wifes soul has become somehow trapped within a new Devils Rain.

==Cast==
* Ernest Borgnine as Jonathan Corbis
* Eddie Albert as Dr. Sam Richards
* William Shatner as Mark Preston
* Ida Lupino as Emma Preston
* Tom Skerritt as Tom Preston
* Joan Prather as Julie Preston
* Keenan Wynn as Sheriff Owens
* John Travolta as Danny
* George Sawaya as Steve Preston
* Anton LaVey as High Priest of the Church of Satan
* Woodrow Chambliss as John

==Response==
The Devils Rain received a uniformly negative critical response, with the chief complaint being the incoherent storyline. The films refusal to provide adequate scares was also widely criticized. Vincent Canby in the New York Times noted that "The Devils Rain is ostensibly a horror film, but it barely manages to be a horror...It is as horrible as watching an egg fry."   Roger Ebert in the Chicago Sun-Times said "All of this would be good silly fun if the movie werent so painfully dull. The problem is that the materials stretched too thin. Theres not enough here to fill a feature-length film."  He gave the film 1½ stars out of four. 
 The Final Programme (1973). The Devils Rain suffered such a critical drubbing that Fuest immediately was forced to retreat to television, directing several nondescript Television movie|TV-movies and series episodes over the years. He has made only one additional theatrical feature, Aphrodite (film)|Aphrodite (1982), a softcore sex romp shot in Greece.
 a cult hed help popularize." 

==Notes==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 