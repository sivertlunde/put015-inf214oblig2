Days Like This (film)
{{Infobox film
| name           = Days Like This
| image          = 
| image_size     = 
| caption        = 
| director       = Mikael Håfström
| writer         = 
| narrator       = 
| starring       = Kjell Bergqvist
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = November 16, 2001
| runtime        = 95 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Days Like This ( ) is a 2001 film directed by Mikael Håfström. It stars Kjell Bergqvist and Carina Johansson. It won two awards at the 2002 Guldbagge Awards. 

==Cast==
*Kjell Bergqvist as Leif
*Carina Johansson as Lena
*Christian Fiedler as Evert
*Ulla-Britt Norrman-Olsson as Siv
*Lia Boysen as Malin

==References==
 

==External links==
* 
* 

 

 
 
 

 