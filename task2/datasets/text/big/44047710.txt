Enikku Njaan Swantham
{{Infobox film
| name = Enikku Njaan Swantham
| image =
| caption =
| director = P Chandrakumar
| producer = M. Mani
| writer = Dr. Balakrishnan
| screenplay = Dr. Balakrishnan Madhu Jagathy Jose Shubha Shubha
| Shyam
| cinematography = Anandakkuttan
| editing = G Venkittaraman
| studio = Sunitha Productions
| distributor = Sunitha Productions
| released =  
| country = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, Jose and Shubha in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast== Madhu as Vasu
*Jagathy Sreekumar as Kili Balan Jose as Mohan Shubha as Meenu Ambika as Geetha
*Nanditha Bose as Leela
*KPAC Sunny as Naanu
*T. P. Madhavan as Madhavankutty
*Aranmula Ponnamma as Madhavakuttys mother Meena as Vasanthy/Mohans mother
*Paravoor Bharathan as Mohans father

==Soundtrack== Shyam and lyrics was written by Bichu Thirumala and Sathyan Anthikkad. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Medamaasakkaalam || S Janaki || Bichu Thirumala || 
|- 
| 2 || Melam unmaadathaalam || S Janaki, P Jayachandran || Bichu Thirumala || 
|- 
| 3 || Minnaaminnippoomizhi || Jolly Abraham || Bichu Thirumala || 
|- 
| 4 || Parakotti Thaalam Thatti || SP Balasubrahmanyam, Chorus || Bichu Thirumala || 
|- 
| 5 || Poovirinjallo || K. J. Yesudas, P Susheela || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 


 