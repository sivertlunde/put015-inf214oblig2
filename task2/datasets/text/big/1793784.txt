Netaji Subhas Chandra Bose: The Forgotten Hero
 
 
 
{{Infobox film
| name = Netaji Subhas Chandra Bose: The Forgotten Hero
| caption =
| image = Bosefilm.jpg
| director = Shyam Benegal
| producer = Raj Pius Barbara von Wrangell
| writer = Atul Tiwari   Shama Zaidi
| starring = Sachin Khedekar   Kulbhushan Kharbanda   Rajit Kapur   Divya Dutta Arif Zakaria
| music = A. R. Rahman
| cinematography = Rajan Kothari
| editing = Sreekar Prasad
| studio = Sahara India Media Communication Ltd.
| released =  
| runtime = 222 minutes
| country = India Hindi
| budget = 300&nbsp;million
| gross =   240&nbsp;million
}} POWs of trial of INA war heroes at Red Fort and the freedom of India.
 Academy Award winner A. R. Rahman. The songs "Aazadi" and "Ekla Chalo" were particularly popular. "Zikr" is composed in the style of Islamic prayer chants Sufi style.

==Cast==
* Sachin Khedekar as Subhas Chandra Bose
* Kulbhushan Kharbanda as Uttamchand Malhotra
* Rajit Kapur as Abid Hasan
* Divya Dutta as Ila Bose
* Arif Zakaria as Major Dhillon
* Ila Arun as Ranu
* Pankaj Berry as Aabid Khan General Auchinlek
* Nalini Chatterjee as Meera
* Pradeep Kumar Das as Servant
* Chris England as CID Chief
* Arindham Ghosh as Subhas Chandra Boses Cousin Ahmed Khan as Mian Akbar
* Shakeel Khan as Sarat Bose Howard Lee as Bengal Governor
* Kunal Mitra as Ashok Bose
* Samiran Mukherjee as Subhas Chandra Boses Cousin
* Rohan Nicol as CID Officer
* Lal Babu Pandit as Checkpost Policeman
* Anna Prüstel as Emilie Schenkl
* Florian Panzner as Alexander Werth
* Surendra Rajan as Gandhiji
* Charu Rohatgi as Bibavati
* Alaknanda Roy as Prabhavati Bose
* Ashiesh Roy as Spy Police 2
* Sonu Sood as Shah Nawaz Khan (general)|Lt. Col. Shah Nawaz Khan
* Vikrant Chaturvedi as Prem Sahgal|Col. Prem Kumar Sahgal
* Rajeshwari Sachdev as Lakshmi Sahgal|Capt. Lakshmi Sehgal
* Udo Schenk as Adolf Hitler
* Jisshu Sengupta as Sisir Bose Ambassador Oshima
* Rakesh Shrivastav as Spy Police 1
* Arindam Sil as Jail Warden
* Sandeep Srivastava as Nambiar
* Lalit Tiwari as Checkpost Policeman
* Christian Willis as Jail Superintendent
* Rajpal Yadav as Bhagat Ram Talwar

== Reception ==
Bose: The Forgotten Hero, which offered a controversial view of the life of Bose, sparked protest in India. Director Benegal was forced to cancel its premiere in Calcutta. The film was fiercely opposed by the Forward Bloc party. The party was angry at the films suggestion that Bose secretly married an Austrian woman, Emilie Schenkl, in 1937, and that he died in a plane crash in Taiwan rather than fleeing to Russia in 1945 as some historians believe. 

BBC gave 3 stars out of 5 for Netaji Subhas Chandra Bose: The Forgotten Hero. Critic Jaspreet Pandohar called it "an informative and fascinating lesson worth sitting through" and "an absorbing drama." "Benegal is best known for his intimate portraits of Indian women, so it comes as some surprise that his latest film is a biopic of one of Indias most famous male icons, Subhas Chandra Bose. Benegal ensures Boses amazing but complex life story is peppered with just the right amount of detail so as to be easily understood. But what stops this film from becoming a   hit is its marathon length. At nearly three and a half hours, Sachin Khedekars gallant performance isnt enough to make this a rousing affair," Pandohar wrote in his analysis. 

Sachin Khedekars portrayal of Bose was praised by critics including Ziya us-Salam of The Hindu newspaper. "Khedekar may not win too many international awards for portraying Bose but accolades in India should come in thick and fast," she wrote in her review. "Benegal may not have put together an epic to challenge the lasting greatness of "Gandhi," Richard Attenboroughs tribute to our father of the nation. But nor has he had the advantage of such resources. Where Benegal deserves credit is not in the canvas of his work but the intellectual honesty he has brought to the film. He refrains from either diluting or distorting history to serve his ends." Salam also noted the limitations of a director working under a relatively small budget for a historical film. 

==Music==
{{Infobox album
| Name = Bose: The Forgotten Hero: The Original Motion Picture Soundtrack
| Type = Soundtrack
| Artist = A. R. Rahman
| Cover =
| Background = Gainsboro
| Released = 2004
| Recorded = Panchathan Record Inn A.M. Studios
| Genre = Soundtrack
| Length = 1:03:23
| Label = Times Music
| Producer = A. R. Rahman
| Last album =    (2004)
| This album = Bose: The Forgotten Hero (2005)
| Next album = Mangal Pandey - The Rising (2005)
}}
{{Album ratings|title=Soundtrack
| rev1      =Planet Bollywood | rev1Score =  link}}
 Indian National Czech Philharmonic Orchestra. Performers include the Western Choir Chennai (for "Aazadi") and the Mumbai Film Choir ("Hum Dilli Dilli Jayenge").

===Reception===
The soundtrack got high critical acclaim. A. R. Rahman received unanimous positive appreciations for his work. Popular music reviewing website Planet Bollywood gave 10 out of 10 stating "Bose – The Forgotten Hero is one of A.R. Rahmans and Javed Akhtars finest creations. Its   lack of mainstream compatibility and item numbers may hinder it from topping tabloid music charts, but that is barely a price to pay for having the distinction of creating musical storytelling of such high caliber. With three creative geniuses (A.R. Rahman, Javed Akhtar, and Shyam Benegal) at work, this quality soundtrack promises a very exciting movie to watch out for."  Another popular review website nowrunning gave a positive review stating "The entire album is created for a patriotic, period film and is totally different from the regular film albums that one hears nowadays. It is always a difficult task to write songs that can cause ones patriotism to surge and flow. To his credit, it must be said that Rahman has succeeded in this and is ably aided by some wonderful lyrics by Javed Saab. As usual, Rahman has used original compositions to enhance the value of original songs and words used by the Indian National Army, way back in the 40s. A good album, but one that may not get popular acclaim but will definitely appeal to a niche audience." 

The soundtrack is considered one of A. R. Rahman|Rahmans finest works and was particularly praised for grand orchestration.

===Track listing===
# "Aazadi" (4:55) – A.R. Rahman, Western Choir Chorus
# " 
# " , Sonu Nigam
# "Hum Dilli Dilli Jayenge" (2:49) – Mumbai Film Choir
# "Desh Ki Mitti" (5:34) – Anuradha Sriram, Sonu Nigam
# "Zikr" (4:44) – A R Rahman, Rafi, Rakeeb, Shaukat Ali
# "Ghoomparani" (4:25) – Sapna Mukherjee, Satyanarayan Mishra
# "Durga Pooja – Rhythm" (3:22) – Instrumental
# "Netaji – Theme 1" (1:22) – Instrumental
# "Afghanistan – Theme 1" (4:14) – Instrumental
# "Hitler Theme" (2:10) – Instrumental
# "Emilie Theme 1" (1:57) – Instrumental
# "Afghanistan – Theme 2" (1:19) – Instrumental
# "War Themes" (4:33) – Instrumental
# "Emilie Theme 2" (2:32) – Instrumental
# "Kadam Kadam Barhayae Ja – Orchestral version" (0:52) – Instrumental
# "Desh Ki Mitti – Orchestral version" (2:48) – Instrumental
# "U Boat Theme (Underwater battle)" (2:11) – Instrumental
# "Netaji – Theme 2" (4:44) – Instrumental
# "Jana Gana Mana (Full Orchestral Version)" (1:15) – Instrumental

==Awards== National Film Award
* Nargis Dutt Award for Best Feature Film on National Integration
*  

==See also==
* Indian National Army
* Azad Hind

==References==
 

==External links==
*  
*   at Allmovie

 
 
 

 
 
 
 
 
 
 
 
 
 