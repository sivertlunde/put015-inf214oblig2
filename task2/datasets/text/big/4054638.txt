AKA (film)
{{Infobox film
| name = AKA
| image = AKA Film Poster.jpg
| image_size =
| caption = Film poster
| director = Duncan Roy Richard West
| writer = Duncan Roy
| narrator =
| starring = Matthew Leitch Diana Quick George Asprey Lindsey Coulson Matt Rowe Steve Smith Scott Taylor Claire Vinson
| editing = Lawrence Catford Jon Cross Jackie Ophir
| distributor = Empire Pictures Inc.
| released = January 19, 2002
| runtime = 123 min
| country = United Kingdom
| language = English
| budget =
| gross =
| preceded_by =
| followed_by =
}} director and writer Duncan Britain and Texan Male prostitution|hustler.  It is largely an autobiographical account of Duncan Roys early life.

The screen consists of a row of three frames, showing three perspectives.

==Cast==
* Matthew Leitch as Dean Page
* Diana Quick as Lady Gryffoyn
* George Asprey as David
* Lindsey Coulson as Georgie
* Blake Ritson as Alexander Gryffoyn
* Peter Youngblood Hills as Benjamin Geoff Bell as Brian Page
* Hannah Yelland as Camille Sturton Daniel Lee as Jamie Page
* Bill Nighy as Uncle Louis Gryffoyn
* David Kendall as Lee Page
* Fenella Woolgar as Sarah
* Sean Gilder as Tim Lyttleton
* Robin Soans as Neil Frost
* Stephen Boxer as Dermot

==Reception==
The film has been nominated for several awards, especially in the gay community.
* 2002 — Nominated for the British Independent Film Awards.
* 2002 — Won the Seattle Lesbian & Gay Film Festival.
* 2002 — Won the Miami Gay and Lesbian Film Festival.
* 2002 — Won L.A. Outfest.
* 2002 — Won the Copenhagen Gay & Lesbian Film Festival.
* 2003 — Nominated for the BAFTA Awards.
* 2003 — Nominated for the Emden International Film Festival.
* 2004 — Won the Los Angeles Film Critics Association Awards.

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 
 