Yaar Pardesi
 
{{Infobox film
| name           = Yaar Pardesi 
| image          =
| alt            = 
| caption        = This film is a Punjabi romance, drama and comedy movie. It is starring Gurpreet Ghuggi also known as the king of comedy                 in Punjab.
| director       = Gurbir Singh Grewal
| producer       = Gurbir Singh Grewal   Sarabjeet Singh Bal   Inderjeet Singh Mann 
| story          = Gurbir Singh Grewal
| starring       = RJ Dhanveer  Raghveer Boli Claudia Ciesla Gurpreet Ghuggi
| music          = Surinder Bachan  Gurpreet Pandher
| studio         = AVI Films Enterprises
| released       =  
| country        = India
| language       = Punjabi
}}

Yaar Pardesi is a 2012 Punjabi comedy drama film directed by Gurbir Singh Grewal. The movie was released on 24 August 2012.  Gurpreet Ghuggi plays the lead role.

==Plot==

Anna (Claudia Ciesla) comes to Punjab to find her sisters husband who cheated her and disappeared from Canada, with all her sisters valuables along with Sehaj (Vandana Singh). Sehajs father wants to marry her to anyone who offers lot of money as Sehaj has permanent resident rights in Canada. Sehaj finds this and runs away from home and goes to Kukus home (Gurpreet Ghuggi), whom they earlier met at Delhi Airport. After finding out that Sehaj has permanent residency of Canada, two of Kukus friend Deep and Jashan tries to woo Sehaj. Sehaj is confused between the two and finally choose Jashan. A local businessman is interested in buying Kukus family land. But Kuku is not interested so the businessman often sends hence henchman to threaten him. Kukus sister is school teacher and on a day is invited for lunch at fellow teachers home. She takes Anna along with her, where she finds the photograph of the man she is looking for. That man turns out to be husband of Kukus sisters colleague Tarseem Singh (Binnu Dhillon). Kuku enlist Deeps school time friend Vijay, a suspended cop. Vijay kidnaps Ujagar Singh, the in-charge cop of the area, who is also accomplice in businessman and Tarseems crimes. Vijay threatens Ujagar with life and gets the truth out of him. Due to solving this case, Vijay gets his job back and arrest Tarseem and the businessman. Kuku, Anna, Sehaj and Jashan forms a committed relationship.

==The Casts==
* Gurpreet Ghuggi
* RJ Dhanveer
* Vandana Singh
* Claudia Ciesla
* Navdeep Kaler
* BN Sharma
* Raghveer Boli
* Tarsinder Singh
* Anita Shabdeesh
* Binnu Dhillon

==References==
 

==External links==
*  
*  

 

 
 
 
 

 