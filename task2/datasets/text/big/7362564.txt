Tale of Cinema
{{Infobox film
| name           = Tale of Cinema
| image          = Tale of Cinema film poster.jpg
| caption        = Theatrical poster
| director       = Hong Sang-soo
| producer       =
| writer         = Hong Sang-soo
| starring       = Kim Sang-kyung   Uhm Ji-won   Lee Ki-woo
| music          = 
| cinematography = 
| editing        =  
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| film name = {{Film name
 | hangul         =  
 | hanja          =   
 | rr             = Geukjangjeon
 | mr             =  Kŭkjangchŏn}}
}}
Tale of Cinema is the sixth film by critically acclaimed South Korean director Hong Sang-soo. It was entered into the 2005 Cannes Film Festival.    

==Plot==
As the film begins, Sangwon, an aimless and indecisive college student on school holiday after final examinations, avoids walking together with his older brother by instead taking a side street, where he finds a former girlfriend, Yongsil, working at an opticians store. Unsure of his own emotional preparedness in rekindling the relationship, he decides to watch a play while waiting for her to complete her work shift, delaying the decision to meet her later in the evening. The final words of anguish in the play, uttered by a desperately ill child unable to be comforted by his mother, would later be echoed by Sangwon from the rooftop of his parents apartment after his own failed act of despair. In the films corollary chapter, Tongsu, a struggling, rootless, and inscrutable filmmaker who has become obsessed with a short film directed by his former classmate - and in particular, the devoted and obliging woman in the film - encounters the young actress in person and begins to ingratiate himself into her company, acting out his projected image of her by imitating gestures and revisiting locations from the film in an attempt to realize his own created image of her.

==Cast==
* Kim Sang-kyung - Kim Dong-soo
* Lee Ki-woo - Jeon Sang-won
* Uhm Ji-won - Choi Young-shil

==See also==
*List of Korean language films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 