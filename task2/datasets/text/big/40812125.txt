Un Lac
{{Infobox film
| name           = Un Lac
| image          = Un lac.jpg
| caption        = 
| director       = Philippe Grandrieux
| producer       = 
| writer         = Philippe Grandrieux
| starring       = Dmitriy Kubasov Natálie Rehorová Alexei Solonchev Simona Huelsemann Vitaliy Kishchenko Artur Semay
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
}} French film directed by Philippe Grandrieux, starring Dmitriy Kubasov, Natálie Rehorová and Alexei Solonchev. The film won a Special Mention in the Orrizzonti Section at the 2008 Venice Film Festival.

==Plot==
Alexi is a man with a pure heart, a woodcutter and a prey to epileptic fits. He is entirely opened to the nature that surrounds him and terribly close to his younger sister, Hege. Their blind mother, their father and their little brother are the silent witnesses to their overwhelming love. A stranger arrives, a young man barely older that Alexi.

==External links==

*  

 
 
 
 

 