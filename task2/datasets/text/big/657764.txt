Uncle Buck
 
{{Infobox film name           = Uncle Buck image          = Uncle buck.jpg caption        = Theatrical release poster director  John Hughes producer       = John Hughes Tom Jacobson Tristin Rogerson-Dolley writer         = John Hughes starring       = John Candy Amy Madigan  music          = Ira Newborn editing  Lou Lombardo Tony Lombardo Peck Prior cinematography = Ralf D. Bode  studio         = Hughes Entertainment distributor    = Universal Pictures released       =   runtime        = 99 Minutes country        =   language  English
|budget         = $15 million gross          = $79.2 million
}} John Hughes and starring John Candy and Amy Madigan, with Jean Louisa Kelly, Gaby Hoffmann, Macaulay Culkin, Jay Underwood, and Laurie Metcalf in supporting roles.

==Plot== Indianapolis to heart attack. They make plans to leave immediately to be with him. After hearing the news, Tia accuses Cindy of abandoning him.

Bob suggests asking his brother, Buck (Candy), to come watch the children, to which Cindy objects. While they are upper middle-class suburbanites, he is unemployed, lives in a small apartment downtown, and earns his living by betting on rigged horse races. His girlfriend, Chanice (Madigan), owns a car repair business. They have been together for eight years now and she wants him to get a job so they can get married and have a family. She even offers him a job at her business. He accepts it, but isnt excited about it as he does not like to work and enjoys his freedom. Since no one else is available to help Bob and Cindy, they have no choice but to turn to him, who agrees to help. He informs Chanice that he will need to start work later as he tells of the family emergency and having to watch his nieces and nephew. Given that Buck has earned an anti-work reputation, Chanice thinks Buck is trying, as usual, to bluff his way out of working.

Buck hits it off with Miles and Maizy, but Tia is standoffish, and they engage in a battle of wills. When he meets her punk boyfriend, Bug (Underwood), he warns her to stay away from him for he is only interested in her for sex. From this point on, Buck repeatedly thwarts her plans to go on dates with Bug to keep them apart. Over the next several days, he deals with a number of situations, including taking the kids to his favorite bowling alley, making oversized pancakes for Miles birthday, dealing with a drunk clown, speaking with the school assistant principal about Maizy, and handling the laundry when the washing machine wont work.

When Buck threatens Bug with a hatchet in an attempt to "bury the hatchet" with him, Tia gets back at Buck by making Chanice think he is cheating on her with their neighbor, Marcy (Metcalf). One weekend, concerned after Tia sneaks away to a party, he decides to go looking for her rather than go to a horse race (he went so far as to taking Miles and Maizy, but changes his mind at the last second). He calls Chanice to come over and look after them as he goes to get Tia. At the party, thinking that Bug is taking advantage of her in a bedroom at the party, he forces the door open by unhinging it with a power drill, but walks in on Bug with another girl. He ties Bug up and throws him into the trunk of his car. After he finds Tia wandering the streets, she apologizes to him.  Buck lets Bug out of the trunk to apologize to her. When he is finally released, he takes back the apology and flees. Buck then begins striking him with golf balls, making him recant and apologize again. At home, she helps Buck reconcile with Chanice and tells her that he would be a good husband and father. It also seems that he will still take her job offer and start working for a living. 

Bob and Cindy return from Indianapolis; her father had survived the heart attack. Upon entering the house, Tia hugs her mother which surprises her, then returns it. She then tells Bob that things are going to be different from now on. Buck and Chanice leave that day to head back to Chicago, with him and Tia giving each other a friendly wave goodbye.

==Cast==
*John Candy as Buck Russell
*Jean Louisa Kelly as Tia Russell
*Macaulay Culkin as Miles Russell
*Gaby Hoffmann as Maizy Russell
*Amy Madigan as Chanice Kobolowski
*Jay Underwood as Bug
*Laurie Metcalf as Marcie Dahlgren-Frost
*Suzanne Shepherd as Asst. Principal Anita Hoargarth Mike Starr as Pooter the Clown
*Garrett M. Brown as Bob Russell
*Elaine Bromka as Cindy Russell
*Dennis Cockrum as Pal

==Production== Evanston was chosen for the exterior of the Russell house. The exteriors and practical locations were shot in Chicago, Cicero, Illinois|Cicero, Skokie, Illinois|Skokie, Northbrook, Illinois|Northbrook, Wilmette, Illinois|Wilmette, Winnetka, Illinois|Winnetka, Glencoe, Illinois|Glencoe, and Riverwoods, Illinois|Riverwoods.

==Reception==
The film received positive reviews from critics.  Review aggregator Rotten Tomatoes has given it a "Fresh" score of 64%, based on 21 reviews, with an average rating of 5.8/10. 

The film earned $8.8 million on its opening weekend to 1,804 theatres and was placed No. 1 at the box office.  Its US earnings were 18th in 1989, and the film has earned nearly $80 million worldwide since its release. 

==Television series==
 
A television series was broadcast on CBS in 1990. It starred Kevin Meaney as Buck, a slob who drinks and smokes. When Bob and Cindy die in a car accident, he is named as the guardian of Tia, Miles, and Maizy. The show was not received well by TV critics. After it was moved to Friday, in an attempt by CBS to establish a comedy night there, its ratings quickly plummeted and it was canceled.

==Remake==
 
In 1991, the film was remade in Malayalam language and released as Uncle Bun. 

==Home media release== The Great Outdoors (1988) and Going Berserk (1983). On February 8, 2011, it was released on Blu-ray Disc for the first time, and released again on June 28, 2011 on Blu-ray with a DVD and a digital copy.

==References==
 

==Further reading==
*  A recent, extended review of the film and its 2012 Blu-ray release.
==External links==
 
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 