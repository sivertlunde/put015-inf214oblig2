The Train Robbers
{{Infobox film
| name           = The Train Robbers
| image          = Poster - Train Robbers, The (1973) 01.jpg
| caption        = Theatrical poster
| director       = Burt Kennedy
| producer       = Michael Wayne
| writer         = Burt Kennedy
| starring       = {{plainlist|
* John Wayne
* Ann-Margret
* Rod Taylor Ben Johnson
* Christopher George
}}
| music          = Dominic Frontiere
| cinematography = William H. Clothier
| editing        = Frank Santillo
| studio         = Batjac Productions
| distributor    = Warner Bros. (USA) Columbia-Warner Distributions (U.K & West Germany) AB4 (2004 Belgium, TV) Craze Productions (2004, USA, Video) National Broadcasting Company (1975,TV) Warner Home Video (1980s-present (VHS, DVD  )
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| gross          = $2.6 million (US)  354,121 admissions (France) 
}}
 Western Technicolor Ben Johnson. The movie was written and directed by Burt Kennedy.

Rod Taylor is billed above the title with John Wayne and Ann-Margret but has a relatively small role. Stephen Vagg, Rod Taylor: An Aussie in Hollywood, Bear Manor Media 2010 p 172. 

Both character names played by Wayne and Ann-Margret are the male and female character names from the 1950s-era Wayne western film Hondo (film)|Hondo. It is uncertain if the fictional characters of The Train Robbers are supposed to be linked to the earlier film.

==Plot summary==
  Pinkerton agent.  

After a series of adventures & battles they return to Texas with the gold were there is one final battle.  The next day Lane & his men put Mrs. Lowe on a train to return the gold and tell her she can keep the reward for herself and her son.  As they are walking past the end of the train they meet the Pinkerton Agent who tells them, as the train is pulling out, that Matt Lowe was never married and that Mrs. Lowe is really a whore named Lilly who fooled them into helping her get the gold for herself.

==Cast==
* John Wayne as Lane
* Ann-Margret as Mrs. Lowe
* Rod Taylor as Grady Ben Johnson as Jesse
* Christopher George as Calhoun
* Bobby Vinton as Ben Young
* Jerry Gatlin as Sam Turner
* Ricardo Montalban as The Pinkerton man

==See also==
*John Wayne filmography

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 