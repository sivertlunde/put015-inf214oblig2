Transit (2006 film)
 
Transit ( , Peregon) is a 2006 film from prolific Russian writer-director Aleksandr Rogozhkin, which was presented at the Karlovy Vary International Film Festival. Several of his past films have screened there, including Life with an Idiot and the Chechen war drama Check Point (film)|Check Point (Blokpost), for which he won the Best Director Prize in 1998. Transit is a story set on a secret military transit base in the remote Chukotka region, where planes from allied forces came in from Alaska, including quite a few with female pilots, which of course attracted the attention of the mostly male Russian crew at the base.
 Aleksei Serebryakov, Daniil Strakhov, and Anastasya Nemolyaeva.

==Plot summary== Airacobra fighter planes across the ocean on Lend-lease. The orderly course of life is disrupted when it becomes clear that the American pilots are attractive and charming young women. The feelings of the Russian young men collide into barriers of culture and language resulting in a host of awkward, funny, and sometimes tragic situations.

It is the story of Russians, Americans, and natives of the Far North. It is the story of man and woman in war. Love and death are squeezed between the hills as human fates are destroyed and born.

==Cast== Aleksei Serebryakov as Captain Yurchenko
*Daniil Strakhov as Captain Lisnevsky
*Anastasiya Nemolyayeva as Irina Zareva
*Svetlana Stroganova as Valentina
*Yuri Itskov as Svist
*Stepan Abramov as Fitil
*Gennady Alekseev as Wilson
*Daniel Anderson as War Correspondent
*Anna Marina Bensaud as Lieut. Jackson
*Artem Bordovsky as Pulya
*Sarah Bulley as Lieutenant Dana Adams
*Christopher Delsman as Donald Svichkovsky aka Doc
*Andrei Fomin as Chernykh
*Caterina Innocente as Lieutenant Mary McClain
*Yevgeni Kachalov as Rozenfeld
*Roman Kelchin as Semen
*Sergei Konstantinov as Rintyn
*Dmitri Lysenkov as Baron
*Yekaterina Makarova as Rintyns Wife
*Oleg Malkin as Nulin
*Sergey Medvedev as Os
*Aleksandr Orlovsky as Vasilkov
*Yuri Orlov as Romadanovsky
*Sergei Pavlov as Roma
*Grant Petrosian as Vano
*Aleksey Petrov as Vasily
*Ivan Prill as Turovsky
*Sarah Margaret Rutley as Lieut. Tippy Kaufman
*Zakhar Ronzhin as Kaiser
*Anna Rud as Olga
*Andrey Shibarshin as Morze
*Mikhail Sivorin as Bologov
*Ruslan Smirnov as Tutko
*Kirill Ulyanov as Gutsava
*Anatoli Ustinov as Petrov
*Sergei Venzelev as Petya
*Artem Volobuev as Shmatko
*Nathan Thomas White as Pasco
*Trigg Hutchinson as the Corporal

==External links==
* 
*   and  

 
 
 
 
 

 