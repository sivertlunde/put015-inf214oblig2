Thank You (2011 film)
 
 
 
{{Infobox film
| name           = Thank You
| image          = Thank You Hindi Movie Poster.jpg
| caption        = Theatrical release poster
| director       = Anees Bazmee
| producer       = Ronnie Screwvala Twinkle Khanna
| writer         = Anees Bazmee Nisar Akhtar Ikram Akhtar
| based on       =  
| starring       = Akshay Kumar Sunil Shetty Amna Batool Bobby Deol Rimi Sen Celina Jaitley Irrfan Khan
| music          = Pritam
| cinematography = Ravi Yadav
| editing        = Steven H. Bernard
| studio         =
| distributor    = UTV Motion Pictures Hari Om Productions
| released       =  
| runtime        = 137 mins
| country        = India
| language       = Hindi
| budget         = 
}} 2011 Indian romantic comedy film directed by Anees Bazmee and produced by UTV Motion Pictures.    It features Akshay Kumar, Sunil Shetty, Bobby Deol, Sonam Kapoor, Irrfan Khan, Rimi Sen and.Celina Jaitley The film was released on 8 April 2011. Thank You is about three married men trying to have some fun outside their marriage. Akshay Kumar plays a detective who specialises in extra-marital relationships.   Most of the scenes in this film were shot in Vancouver and Toronto, Canada and Bangkok. Other Bollywood films which have focused around a similar concept are Masti (2004 film)|Masti, No Entry, Shaadi No. 1 and to a certain extent, Do Knot Disturb.

==Plot==

Raj (Bobby Deol), Yogi (Sunil Shetty) and Vikram (Irrfan Khan) are best friends and business partners who are married to Sanjana (Sonam Kapoor), Maya (Celina Jaitley), and Shivani (Rimi Sen), respectively. What their wives dont know is that their husbands cheat on them often with not one, but many women.

Yogi has already been caught by Maya after the latter hired her new-found friend Kishan (Akshay Kumar), a private detective, to check on Yogi. Now Yogis status in the marriage is reduced to that of a butler at home. Yet Yogi still has aspirations for more hanky panky if the right opportunity comes along. Vikram, on the other hand, maintains a strict hand on his wife and treats her condescendingly most of the time. 

Raj is in good shape, as Sanjana is quite gullible and suspects nothing. However, Sanjana finds out from Maya that the necklace that Raj had supposedly bought only for her actually was a buy one, get one free deal. So where is the other necklace? Shivani, Maya, and Sanjana all get together, and Maya decides to hire Kishan to tail Raj.

Kishan has built his reputation as a man who saves marriages by getting erring husbands to mend their ways, and is also an expert in playing the flute. When he is first introduced to Sanjana, he seems to fall in love at first sight, or so it is made to appear at this point in the film. Initially a sceptical Sanjana rebukes Kishans early attempts to prove that her husband is a lying cheat. This, however, only makes Kishan more determined.

Kishan first attempts to bring all the girlfriends of Raj, Vikram and Yogi to one party, at the same time when all the wives are there as well, to force a surprise face-off, thus exposing the cheating husbands. But Raj, with the help of vikram and yogi exclaiming them as the boyfriend manages to lie his way out of the situation convincingly. Frustrated, but now even more determined, Kishan hatches a new plan, to break the friendship of the three men, by portraying Yogi as a traitor.

Raj and Vikram, initially embarrassed, now become suspicious that someone is helping their wives behind the scenes, to ruin their marriages. They decide at this point to seek professional help and look to hire a private detective to find out who it is. In a twist of fate, they end up at Kishans office! However, Kishan pretends he has no idea who they are, and chooses to play a double game, agreeing to act as their detective while unknown to them being the very person working for their wives that they are looking for.

Kishan now records Yogi talking to Sanjana in a park, hires a voice-over artist, and dubs the tape with Yogi now appearing (in the voice over) as if he is betraying Raj and Vikrams secrets to Sanjana. Kishan then shows this altered version to Raj and Vikram, making them to feel betrayed by Yogi. However, as fate has it, they get a chance for revenge right away when Yogi calls and confides in them that he is off to see Sweetie, one of his old girlfriends.

Raj and Vikram are quick to reveal this information to Maya, as a result of which Yogi is caught red-handed by Maya, who goes ballistic at the site of Yogi in a hotel room with a girlfriend. Kishan also manages to get Raj, catching him standing in his bathrobe outside an apartment building window ledge due to his girlfriends husband unexpectedly showing up at the door inside. Kishan calls in the news channel and the police to get the stranded Raj broadcast live on TV. He then calls Sanjana, asking her to turn on the TV and see for herself what Raj is up to.

Caught red-handed, Raj asks Vikram for help, the later comes up with a ploy to make it appear that both Raj and Vikram are undercover police officers in real life. And as such, Raj was out on a mission when caught on camera and that the whole resulting "cheating husband outside window" scenario shown on TV was actually all a misunderstanding. The gullible Sanjana wholeheartedly buys into the story, much to Kishans frustration.

Kishan now hatches his own counter-plot and pretends to be the gangster don, King (Mukesh Tiwari) who is supposedly out to kill Raj and Vikram, thinking they are indeed police officers. When Raj and Vikram come face to face with King, they quickly backtrack on their story of being undercover police officers. However, King (Kishan) is able to scare Raj and Vikram into being blindfolded and taken as his prisoners. He then has them admitting, unknowingly in front of Sanjana and Shivani, that the whole secret agent thing is a fabrication and they have many girlfriends with whom they cheat on their wives regularly.

Sanjana and Shivani become extremely upset upon hearing these confessions, the latter fainting on the spot and Sanjana attempting suicide soon thereafter. However, Kishan rescues her and convinces her to work with him in a plan that will restore her marriage. The first step is for him to pretend to be Sanjanas new lover. He has Yogi give Raj and Vikram the information that Sanjana is now dating another man and loves him. The trio reunite and try ways to find out who this person is, nearly coming close to doing so on some occasions. Kishan also pretends to be Shivanis brother and has Vikram tricked into signing over all his property to Shivani under the ruse of a bank document. Thus, Vikram is thrown out of his own house by Shivani and her family. He next convinces the trio that the real King is the man Sanjana is having an affair with. Since Kings wife suspects him of having an affair, the trio are convinced to go to her and tell her everything. Along with her, they storm Kings house and Raj then beats up King after the latters wife has done so. However, this is revealed to be a misunderstanding as the woman with King at the time is not Sanjana, but somebody else. 

Completely out of options, Raj, Vikram and Yogi  go to Kishans office. While there, they accidentally figure out that Kishan is, in fact, the person behind all the troubles they are in with their wives. When Kishan arrives, they quickly surround him with the intent to harm him, when all of a sudden Sanjana walks in. She is quick to defend Kishan, who had earlier saved her from committing suicide. At this point Raj decides to divorce Sanjana, mistakenly assuming she is in love with Kishan.

Sometime later, with the divorce finalised, Raj, Vikram and Yogi are found drunk at a lake-side restaurant, when Kishan appears all of a sudden. He has with him wedding cards inviting the three over to a wedding. Raj assumes its Kishan and Sanjanas wedding, and starts to chase after Kishan. Since he is drunk, he keeps missing and then accidentally smashes a police officer. The three friends are then arrested, and put in jail. Where they reminisce about the tragedy that has befallen them. But the next morning, they are bailed out by the don King, much to their surprise.

Out of jail, and armed with a gun supplied by King, Raj, full of rage, heads straight for the wedding and straight away shoots Kishan, supposedly killing him on the spot. However, it turns out, the bullets were all fake blanks, planted by don in collaboration with Kishan himself. Kishan gets up and reveals that the wedding was actually supposed to be between Sanjana and the surprise groom, Raj himself, much to the embarrassment of Raj.

It becomes clear at this point that Kishan was never after Sanjana herself as has been made to appear throughout the film. But that Kishan did all this because he himself once had a wife (Vidya Balan), who committed suicide after finding out that Kishan was cheating on her. From that day on, Kishan decided to become a private detective going around saving marriages in crisis, like his own. After this emotional revelation, Raj, Yogi and Vikram promise to give up outside-relationships. All the couples get back together, Raj and Sanjana get remarried. At the reception, Sanjanas sister asks Sanjanas mother where he is, Sanajanas mother says that he has gone to rescue another marriage in crisis. It then shows Kishan walking in town, playing his flute.

==Cast==
* Akshay Kumar as Kishan, a Private Detective
* Sunil Shetty as Yogi Mathur
* Sonam Kapoor as Sanjana Malhotra
* Bobby Deol as Raj Malhotra
* Irrfan Khan as Vikram Chopra
* Rimi Sen as Shivani Chopra
* Celina Jaitley as Maya Mathur
* Chahat Khanna as Kanisha a.k.a. Kuku, Sanjanas sister
* Smita Jaykar as Sanjanas Mother
* Shillpi Sharma as Kammo
* Mukesh Tiwari as King
* Rakhee Tandon as Kings wife
* Ranjeet as TT (a Don)
* Vidya Balan as Kishans wife (Cameo Appearance)
* Mallika Sherawat as Razia (Special Appearance in item song)

==Production==
Sonam Kapoor replaced Katrina Kaif as the lead actress, since the director wanted to have a change with Akshay Kumars love interest. The film was shot at different locations in Vancouver, Toronto, Canada and Bangkok. It was made on a budget of   50 crore, while an additional amount of   80&nbsp;million was spent on promotions.   

==Reception==

===Critical===
Thank You opened to negative reviews from critics. Taran Adarsh of Bollywood Hungama rated the film with two stars out of five saying, "What couldve been an honest take on dishonesty fails to leave a mark eventually".  Rajeev Masand of CNN-IBN gave the film one star and stated, "Badly scripted, shoddily photographed and embarrassingly performed, Thank You is one of those rare films that gets absolutely nothing right."  Anupama Chopra of NDTV gave one and a half stars and commented, "My brains were battered to pulp and my eardrums are still recovering from Pritam’s cacophonous sound-track."  Nikhat Kazmi of the Times of India awarded two and a half stars saying, "You have a film that can be an average weekend getaway. Nothing more, not even Mallika Sherawats item number."  music by pritam

===Box office===
Thank You had a below average opening of Rs. 50.0&nbsp;million according to Box Office India.  It showed a 20% increase on Saturday, bringing the two-day total to Rs. 110&nbsp;million nett.  The film grossed approximately   320&nbsp;million by the end of the weekend  and   520&nbsp;million nett by the end of its first week.  Colors and T-Series respectively for a sum of   300&nbsp;million. 

==Interesting facts==
In scene where Kishan is escaping from pursuers the trick of Jackie Chans character Buck Yuen from his movie "The Accidental Spy" is reproduced. Particularly, Kishan runs into an alleyway, and sees large pieces of clothes hanging from above. He performs quick acrobatic moves to twist the material around himself in a manner of womens dress and covers his face. Thus, disguised in the clothes he manages to make his escape.

==Soundtrack==
{{Infobox album| 
| Name        = Thank You
| Type        = soundtrack
| Artist      = Pritam
| Cover       = imagesmusic.jpg
| Released    = 4 March 2011
| Recorded    =
| Producer =
| Genre       = Film soundtrack
| Length      =
| Label       = T-Series
}}
The films soundtrack is composed by Pritam. Lyrics are penned by Kumaar, Ashish Pandit and Amitabh Bhattacharya. The song "Pyaar Do Pyaar Lo" is taken from old film Janbaaz. An additional song "Viah Di Raat (Khushiyan Da Mela)" was used in the film but has not been included in the soundtrack album.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Pyaar Do Pyaar Lo
| extra1 = Mika Singh
| lyrics1 = Amitabh Bhattacharya
| length1 = 5:15
| title2 = Razia
| extra2 = Master Saleem, Ritu Pathak
| lyrics2 = Ashish Pandit
| length2 = 4:54
| title3 = Full Volume Richa Sharma, Suzanne DMello
| lyrics3 = Kumaar
| length3 = 4:07
| title4 = My Heart Is Beating
| extra4 = Sonu Nigam
| lyrics4 = Kumaar
| length4 = 3:54
| title5 = Pyaar Mein
| extra5 = Neeraj Shridhar, Javed Ali
| lyrics5 = Amitabh Bhattacharya
| length5 = 4:37
| title6 = Pyaar Do Pyaar Lo - Remix
| extra6 = Mika Singh
| lyrics6 = Amitabh Bhattacharya
| length6 = 4:39
| title7 = Razia - Remix
| extra7= Master Saleem, Ritu Pathak
| lyrics7 = Ashish Pandit
| length7 = 4:26
| title8 = Full Volume - Remix Richa Sharma
| lyrics8 = Kumaar
| length8 = 4:25
| title9 = My Heart Is Beating - Remix
| extra9 = Sonu Nigam
| lyrics9 = Kumaar
| length9 = 4:44
}}

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 