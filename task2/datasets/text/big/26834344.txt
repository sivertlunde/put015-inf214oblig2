Seetamalakshmi
{{Infobox film
| name           = Seetamalakshmi
| image          =
| image_size     =
| caption        =
| director       = K. Viswanath
| producer       = Murari – Naidu Jandhyala (dialogues)
| narrator       = Chandra Mohan Sridhar Vankayala Satyanarayana Tulasi
| music          = K. V. Mahadevan
| cinematography = U. Rajagopal
| editing        = G. G. Krishna Rao
| studio         =
| distributor    =
| released       = 1978
| runtime        =
| country        = India Telugu
| budget         =
}}
 Hindi as Sitara in 1980 starring Mithun Chakraborty and Zarina Wahab. 

==Plot==
Kondayya (Chandra Mohan (actor)|Chandramohan) and Seetalu (Talluri Rameshwari|Rameshwari) work in a touring theatre in a village Kurabalakota and in love. While essentially illiterate, they love watching movies and learn to recite the film dialogues and songs. A film producer who comes to the village and makes a false promise to make Seetalu as heroine in the films. Seetalu goes to Hyderabad along with Kondayya. After facing many difficulties, with the help of painter Sridhar, Seetalu finally becomes a heroine. Her success brings her relatives for want of her money. Kondayya slowly realizes that he cannot fit in that cine urban culture. He returns to the village. How the young lovers unite is the rest of the drama.

==Credits==

===Cast===
* Talluri Rameshwari	as 	Seetalu Chandra Mohan	as   Kondayya
* Sridhar
* Vankayala Satyanarayana     as   Railway Station Master
* Eashwara Rao
* Dubbing Janaki
* Master Tulasiram    as   Pallavi
* Master Hari
* Pallavi
* P. L. Narayana
* Sakshi Ranga Rao

===Crew===
* Director: K. Vishwanath
* Assistant Director: Nanduri Vijay
* Producers: Murari&nbsp;– Naidu
* Production Company : Yuva Chitra
* Story: K. Vishwanath Jandhyala
* Screenplay: K. Vishwanath, Jandhyala and K. Murari
* Art Director: Thota Tharani
* Director of Photography: U. Rajagopal
* Film Editing : G. G. Krishna Rao
* Music Director: K. V. Mahadevan
* Assistant Composer: Puhalendi
* Lyrics: Devulapalli Krishnasastri and Veturi Sundararama Murthy
* Playback singers: S. P. Balasubramanyam, P. Susheela, G. Anand, Vani Jayaram and Vijayalakshmi Sarma

==Soundtrack==
* Chalu Chalu Ee Virasalu (Lyrics: Devulapalli Krishnasastri; Singers: G. Anand and Vijayalakshmi Sarma
* Kokoroko Koruko Em Kavalo Koruko (Lyrics:   and S. P. Balasubramanyam)
* Maavi Chigugu Tinagane Koyila Palikena (Lyrics: Devulapalli Krishnasastri; Singers: P. Susheela and S. P. Balasubramanyam)
* Nuvvitta Nenitta Kookunte Inketta (Lyrics: Veturi Sundararama Murthy; Singers: P. Susheela and S. P. Balasubramanyam)
* Pade Pade Padutunna Padina Pate (Lyrics: Veturi Sundararama Murthy; Singer: P. Susheela; Cast: Tulasi)
* Seetalu Singaram Malakshmi Bangaram (Lyrics: Veturi Sundararama Murthy; Singers: P. Susheela and S. P. Balasubramanyam)
* Ye Pata Ne Padanu Bratuke Pataina Pasivadanu (Lyrics: Veturi Sundararama Murthy; Singers: P. Susheela, Vani Jayaram and Vijayalakshmi Sarma)

==References==
 

==External links==
*  

 
 
 
 
 