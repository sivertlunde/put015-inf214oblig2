Lapitch the Little Shoemaker
{{Infobox film
| name           = Lapitch the Little Shoemaker
| image          = Lapitch DVD cover (HR).jpg
| caption        = The DVD cover for Lapitch in its native Croatia. This design was also used for Egmont Publishings locally-published adaptation of the film.
| director       = Milan Blažeković
| producer       = Željko Zima Steffen Diebold
| writer         = Novel:   English adaptation: Alan Shearman
| starring       = Ivan Gudeljević Maja Rožman Tarik Filipović Pero Juričić Relja Bašić
| music          = Original version: Duško Mandić English version: Hermann Weindorf
| editing        = Mirna Supek-Janjić
| studio         = Croatia Film HaffaDiebold ProSieben
| distributor    = Croatia: Croatia Film Germany: ProSieben Home Entertainment (VHS)    United States:
   (DVD)
| released       =   
| runtime        = 97 minutes 91 minutes (US)
| country        = Croatia Germany
| language       = Croatian English
| budget         = Deutsche Mark|DM900,000    
| gross          = 
}}
 animated film The Magicians Hat (1990).    

It is based on The Brave Adventures of Lapitch, a 1913 novel by Croatian author Ivana Brlić-Mažuranić. In this adaptation, all of the characters are animals, and the title character is a mouse, rather than the human character of the original work.   As with the book, the film is about a shoemakers apprentice who leaves the confines of his ill-tempered master, and sets off on an adventure. During his journey, he befriends a circus performer named Gita, and fights against the evil Dirty Rat.
 1997 Academy Lapitch the Little Shoemaker, at the end of the 1990s.   and Croatia Film let Lapitch loose}}    

In February 2000, it first appeared in North America as the initial entry in Sony Wonders short-lived "Movie Matinee" video series. The Disney Channel also premiered it on U.S. cable television later that same month.   

==Plot==
 
 
:Original Croatian character names are in parentheses.
Lapitch (Šegrt Hlapić), a small orphan mouse, works in a small town as the apprentice of the Scowlers—a mean-mannered shoemaker (Majstor Mrkonja), and his kind-hearted wife (Majstorica). His dog, Brewster (Bundaš), keeps him company when he is alone.

Master Scowler awaits a visit from the Mayor (Gradonačelnik) and his son (both pigs); Lapitch has to make sure the pigs boots are the right size. When the piglet tries to put them on, things do not go well, and the two patrons leave for good. Lapitch tries to tell the Master it was not his fault, yet he still blames his young apprentice for the mistake.

Mistress Scowler apologises for her husbands bad behaviour. While he tidies up, she tells the young mouse that she and her husband used to be happier ages ago. Lapitch wants to know why, but the Mistress vows only to tell him when he is older.

Lapitch writes a letter to the Scowlers and leaves the village, wearing the piglets boots. Although Lapitch has said good-bye to Brewster, the dog joins him the following morning. Eventually, the two of them visit a young squirrel named Marco (Marko), who lives in a house with a blue star on its walls, and help him round up some geese that went astray while he was tending to the flock.

At evening, Marcos mother thanks them by serving a luscious supper. An awkward raccoon, Melvin (Grga), eavesdrops on the groups conversation. Hearing of a valuable treasure stored inside their house, he runs off to tell his boss, the evil Dirty Rat (Crni Štakor), about the goods.

When morning comes, Lapitch and Brewster say farewell to the squirrels. The road ahead, however, does not go smoothly: an afternoon of unusual weather culminates in an evening storm, and they must find shelter. When they do, under an old bridge they meet Dirty Rat himself, and sleep next to him. Because of Dirty Rat—"Dark Lord of the Hell" as he is known—Lapitch wakes up shocked to see his boots gone.
 ringmaster treated them badly and left them behind. During their journey, the two mice and their pets meet Melvins hardworking mother and help her chop wood. Worried about her sons bad deeds, Melvins mother gives Lapitch a silver coin for luck before they go off. The mouse later gives it to the raccoon, after he falls off Dirty Rats wagon. 

Soon, the group team up with some villagers to extinguish a fire, but the villagers make Melvin a suspect in the areas recent robbery sting. In addition, the group encounters a poor cat like Warthog named Yana (Jana), whose magic powers give Lapitch the courage to face the evil Dirty Rat.

After the gang comes to a circus, Lisa entertains the patrons of an under-used merry-go-round. As night falls, she reunites with her horse, Blanka (Zorka), after hearing its neigh. But a nasty surprise awaits everyone: Dirty Rat makes a deal with Lisas ringmaster in which he vows to reach Marcos house with a fast horse, and steal the family chest. Lapitch and friends plan to stop him for good when they hear this.

Later on, they meet Master Scowler, whom Melvin has just rescued. Scowler tells them he was robbed and tied up in a tree for two days. They all set forth to put an end to Dirty Rat; Melvin gives them a hand, but his boss ties him and swings him out of the way.

A determined Lapitch, guided by Yana and her wise advice, comes to terms with the villains schemes. Dirty Rat is so enraged that his horse charges straight at the little mouse, about to trample him. At the last moment, a bolt of lightning splits the harness; the Rat and his cart fall down a cliff, encased in huge boulders, Dirty Rat is presumably dead.

The clouds clear as soon as everyone celebrates. Then, Lisa tames Dirty Rats black horse, and gives it to Melvin, who promises to live a good life after what he has gone through, Lapitch was sad if Lisa was possibly killed by Dirty Rat.

The next day, Lapitch, Yana Marco and the rest arrive at Marcos house, where his mother shows them the familys valuable treasure; she also receives Melvins coin. When they leave, they meet Marcos father, gone for a long time after working in a faraway land.

Back home, a worried Mistress Scowler is delighted to see everyone back again—Brewster, Master Scowler, Lapitch and Lisa. By then, she and her husband finally recognise Lisa, the orphan circus star, as their lone child Susanna. The Scowlers have their happiest moment ever due to this; afterwards, Master plays a pleasant tune on his violin, and everybody dances to it. Eventually, the Scowlers rekindle their relationship, and the town dwellers celebrate along with them. In time, Lapitch becomes the most respected shoemaker they have ever known.

==Cast==
 
 
{| class="wikitable" border="1"
|-
! colspan="3"| Croatian version
|- Actor
! colspan="2"| Role
|-
! Croatian name !! English name
|-
| Ivan Gudeljević
| Šegrt Hlapić || Lapitch
|-
| Maja Rožman
| Gita || Lisa
|- Tarik Filipović
| Amadeus || Pico
|-
| Vlasnik vrtuljka || Merry-go-round owner
|- Pero Juričić
| Bundaš || Brewster
|-
| Vlasnik cirkusa || Ringmaster
|-
| Relja Bašić
| Crni Štakor || Dirty Rat
|-
| Vlado Kovačević
| Majstor Mrkonja || Master Scowler
|-
| Marina Nemet Brankov
| Majstorica || Mistress Scowler
|-
| Hrvoje Zalar
| Grga || Melvin
|-
| Ljiljana Gener
| Jana || Yana
|-
| Zorko Sirotić
| Marko || Marco
|-
| Ivana Bakarić
| Markova majka || Marcos Mother
|-
| Božidarka Frajt
| Grgina majka || Melvins Mother
|-
| Emil Glad
| Medo || Bear
|-
| Ivica Vidović
| Markov otac || Marcos Father
|-
| Mate Ergović
| Gostioničar || Innkeeper
|-
| Mladen Crnobrnja
| Ptica Štef || Bird
|-
| Slavko Brankov
| Jazavac || Badger
|-
| Sven Šestak
| Lisac || Fox
|-
| Marinko Prga
| Zec || Rabbit
|-
| Ivo Rogulja
| Gradonačelnik || The Mayor
|-
| Barbara Rocco
| Praščić || The Mayors Son
|-
| Zlatko Crnković
| Pripovjedač || Narrator
|}

{| class="wikitable" border="1"
|-
! colspan="2"| English version
|-
! Actor
! Role
|-
| Cathy Weseluck
| Lapitch   
|-
| Brendan Fraser
| Yana, Yana and Marcos mother
|-
| Mickey Rooney
| Marco, Additional Voices
|-
| Sharon Stone
| Lisa
|- Michael Dobson
| Additional Voices
|-
| Tim Curry
| Dirty Rat
|-
| Samantha Eggar
| Melvin
|-
| Maxine Miller
| Additional Voices
|-
| Gerard Plunkett
| Additional Voices
|-
| Jonathan Winters
| Additional Voices
|}

{| class="wikitable" border="1"
|-
! colspan="2"| German version 
|-
! Actor
! Role
|-
| Manuel Straube
| Lapitch
|-
| Julia Kaufmann
| Lisa
|-
| Michael Walke
| Melvin
|}

==Production== War of Independence.   During production, the crew used cel cameras dating as far back as 1938.  They also made Ivana Brlić-Mažuranićs title character a mouse, amid a roster of anthropomorphic animals. 

Director Milan Blažeković had previously worked on several animated shorts and Professor Balthazar episodes for Zagreb Film,  before heading on to direct 1986s The Elm-Chanted Forest and its 1990 sequel The Magicians Hat for the Croatia Film studio. For Lapitch, he also served as a screenplay writer and layout artist. The film featured a roster of well-known Croatian actors, among them Relja Bašić, Emil Glad, Tarik Filipović and Ivana Bakarić. Child actors Ivan Gudeljević and Maja Rožman played the title character and his girlfriend respectively.

Early in 1997, Croatia Film teamed up with two German entities—HaffaDiebold, a subsidiary of Constantin Medien, and the television station ProSieben—to create a new version of the film for the international market.    With several altered scenes, and a reworked ending, this version was first dubbed for the German-speaking market, before being sold to over 70 territories worldwide.   Swedens TV1000 was among the first stations to air the HaffaDiebold edit on television, under the title Lapitch den lilla skomakaren.  
 Airwaves Sound Design; Rainmaker Digital Pictures handled post-production. In this version, Canadian voice actress Cathy Weseluck provided the voice of Lapitch. 

==Music==
The music for the original version was composed by Duško Mandić, with songs written by crew member Pajo Kanižaj. Croatian musician Petar Grašo contributed to the production with the uncredited "Ljubav sve pozlati", part of which was heard in the end credits. Željko Zima, the films producer, directed its music video.   The song also appeared as the last track of Grašos 1997 debut album, Mjesec iznad oblaka, published by Orfej and Tonika.   
 Ariola label on 6 October 1997. 

{| class="wikitable" border="1"
|-
! English song
! Performed by
! Original Croatian track
|-
| "Shoe Song"
| Munich All-Stars Choir
| "Pjesma cipelica"
|-
| "Dirty Rat"
| Eric Brodka
| "Pjesma Crnog Štakora"
|-
| "Farmers and Friends Forever"
| Munich All-Stars Choir
| "Pjesma žetelaca"
|-
| "Take My Hand (Pop Version)" Not featured in the 2000 U.S. version. 
| Manuel Straube
| "Pjesma prognanika"
|-
| "Take My Hand"
| Jane Bogart
| "Ljubav sve pozlati"
|}

==Release and reception== Academy Award for Best Foreign Language Film in 1997, and was one of two animated submissions in that category, along with Hayao Miyazakis Princess Mononoke from Japan.   Neither film, however, made it to the final list of nominations.

Lapitch went on to become the most successful theatrical release of Croatias film industry (with over 355,000 viewers).    In France, it has sold over 300,000 copies since TF1 Video released it in 1999.   The German dub, from ProSieben Home Entertainment, sold up to 50,000 tapes by the end of 1999. 

In North America, it was the first film to be released in Sony Wonders "Movie Matinee" series.   Originally planned for 12 October 1999,  it was not available until 8 February 2000. This version runs 75&nbsp;minutes, edited down from the original 83-minute European cut. Coupons for the  , which gave it three stars out of five. 

The German television channel ProSieben first aired Lapitch on 6 June 1998.   On 27 February 2000, it received its U.S. television premiere on the Disney Channel. 

The Croatian subsidiary of Egmont Publishing released a book adaptation of the film in 2001.   Three years later, it saw its premiere on DVD, both in Croatia  and the United States. 
 a spin-off television series of 26 episodes, entitled Hlapićeve nove zgode in its native Croatia. The 26-episode series was a co-production of Croatia Film and EM.TV/HaffaDiebold, with animation by Barcelonas Neptuno Films. 

==See also==
*List of animated feature-length films
*Cinema of Croatia
*List of Croatian submissions for the Academy Award for Best Foreign Language Film
* 

==Notes==
 

==References==
 

==External links==
 
*   
* 
* 
* 

 
{{succession box
| before = Nausikaya (1996)
| title  = Croatian submission to Best Foreign Language Film category, Academy Awards (U.S.)
| years  = 1997
| after  = Transatlantic (1998 film)|Transatlantic (1998)
}}
{{succession box
| before = Pom Poko (1994) ( )
| title  = Animated submission to Best Foreign Language Film category, Academy Awards (U.S.)
| years  = 1997 (also Princess Mononoke  )
| after  = Manuelita (film)|Manuelita (1999) ( )
}}
 

 
 
 
 
 
 
 
 
 
 
 
 