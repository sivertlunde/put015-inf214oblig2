Bad Little Angel
{{Infobox Film
| name           = Bad Little Angel
| image          = File:Bad Little Angel poster.JPG
| image_size     = 
| caption        = Film poster
| director       = Wilhelm Thiele
| producer       = 
| writer         = 
| starring       = Virginia Weidler Gene Reynolds
| music          = 
| cinematography = 
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       = October 27, 1939
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Bad Little Angel is a 1939 inspirational drama film starring Virginia Weidler as an orphan named Patsy Sanderson, living in America around 1900.
 Margaret Turnbull.

==Cast==
*Virginia Weidler - Patricia Victoria Patsy Sanderson
*Gene Reynolds - Thomas Tommy Wilks
*Guy Kibbee - Luther Marvin Ian Hunter - Jm Creighton (Sentinel editor) Elizabeth Patterson - Mrs. Perkins
*Reginald Owen - Edwards, Marvins Valet
*Henry Hull - Red Wilks Lois Wilson - Mrs. Ellen Creighton Barbara Bedford - Mrs. Dodd (scenes deleted)
*Cora Sue Collins - Clarabella Dodd (scenes deleted)
*Sally Martin - Belinda (scenes deleted)
*Esther Dale - Miss Brown, Orphanage Secretary (uncredited)
*Byron Foulger - New Sentinel editor (uncredited)
*Harry Hayden - Mr. Simms, Man in Jims Office (uncredited) Russell Hicks - Maj. Ellwood, newspaper owner (uncredited) George Irving - Dr. Bell (uncredited)
*Mickey Kuhn - Bobby Creighton, Age 5 (uncredited) Terry - Dog (uncredited)
*Ann E. Todd - Libbit Creighton, Age 9 (uncredited)

== External links ==
*  

 
 
 
 
 
 
 


 