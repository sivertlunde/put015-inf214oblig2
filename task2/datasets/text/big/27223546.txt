Hansel & Gretel: Witch Hunters
 
 
{{Infobox film
| name           = Hansel & Gretel: Witch Hunters
| image          = Hansel and Gretel Witch Hunters .jpg
| image_size     = 215px
| alt            =  
| caption        = International release poster
| director       = Tommy Wirkola
| producer       = Will Ferrell Adam McKay Kevin Messick Beau Flynn
| writer         = Tommy Wirkola  Thomas Mann Derek Mears
| music          = Atli Örvarsson
| cinematography = Michael Bonvillain
| stereography   = Florian Maier
| editing        = Jim Page Siebzehnte Babelsberg Film
| distributor    = Paramount Pictures Metro-Goldwyn-Mayer
| released       =  
| runtime        = 88 minutes    97 minutes  (Directors cut|"extreme version") 
| country        = United States Germany  
| language       = English
| budget         = $50 million 
| gross          = $226.3 million    
}} horror comedy Thomas Mann, and Derek Mears.
 The Avengers The Bourne premiered in North America on January 25, 2013, in 2D, 3D, and IMAX 3D, as well in D-Box Technologies|D-Box motion theaters and select international 4DX theaters, and was rated R in the United States. The film had its home media release on June 11, including a longer, unrated version on Blu-ray Disc.

Hansel & Gretel: Witch Hunters was generally panned by mainstream critics, particularly for what they saw as its weak script and gratuitous violence. However, many horror genre critics were more positive, viewing the film as unpretentiously entertaining. The film topped the domestic box office on its opening weekend and was a major hit in Brazil, Russia, Germany, and Mexico. Its worldwide theatrical run gross exceeded $226 million for the production cost of $50 million. Due to the commercial success of the film, which was planned as the first part of a series, its sequel is currently in development, and has been set for a 2016 release date.

==Plot== witch hunters, famed for seeking out and destroying hundreds of witches. The pair find that they are somehow immune to spells and curses, but the incident in the gingerbread house has left Hansel Diabetes mellitus|diabetic. He needs a shot of insulin every few hours or he will get sick and die. 
 
Hansel and Gretel arrive in the town of Augsburg and immediately prevent Sheriff Berringer from executing a young accused witch named Mina, who Hansel decides is innocent. Mayor Englemann tells the crowd that he has hired the siblings to rescue several children who were presumably abducted by local witches. Berringer hires other hunters for the same mission in the hopes of disgracing the mayor and cementing his power. All but one of the sheriffs party are killed that night by the powerful grand witch Muriel, who sends one man back to the town tavern as a warning to the locals. Hansel and Gretel, along with the mayors deputy Jackson, capture one of Muriels witches and interrogate her. They discover that Muriel is preparing for the coming Blood Moon, where she plan to sacrifice twelve children in order to grant the witches an immunity to their weakness, fire. Muriel, accompanied by her witches and a troll servant named Edward, attack the town and abduct the final child. Muriel kills Jackson and launches Gretel out a window, rendering her unconscious. Gretel is rescued by Ben, a local teenager who is a fan of theirs and plans to be a witch hunter himself. Hansel grabs onto one witchs broomstick, but falls and is lost in the forest.
 
The next morning, Hansel is found stuck up a tree by Mina. She takes him to a nearby spring where she heals his wounds. Gretel searches for Hansel in the forest but is attacked by Sheriff Berringer and his posse. The men capture and beat Gretel before being stopped by Edward, who kills the sheriff and his men. Edward tends Gretels wounds and tells her that he helped her because trolls serve witches. Hansel and Gretel reunite at an abandoned cabin, which they discover is both a witchs lair and their childhood home. Muriel appears in front of them, telling them the truth of their past. She reveals that Hansel and Gretels mother was a grand white witch named Adrianna who married a farmer. On the night of the last Blood Moon, Muriel planned to use the heart of a white witch to complete her potion. She found Adrianna too powerful and decided to use Gretels heart instead. To get rid of Adrianna, Muriel revealed to the townspeople that Adrianna was a witch. The townspeople burned her alive and hanged Hansel and Gretels father. Following this revelation, the siblings battle Muriel before she stabs Hansel and abducts Gretel for the ritual.
 
Hansel wakes up with Mina, who reveals herself to be a white witch. She heals his wounds again and uses a grimoire that was found in the house to bless Hansels arsenal of weapons. Hansel, Mina, and Ben head out to disrupt the Witches Sabbath at a neaby mountain. Using the blessed guns, Hansel and Mina attack a large gathering of dark witches who came there from all over the world, massacring them and freeing the children. In the midst of the slaughter, Edward defies Muriels orders and releases Gretel before Muriel throws him off the cliff. Some witches attempt to flee the carnage but are killed by traps, and Muriel too tries to escape on a broomstick but is downed by Ben. Hansel pursues her immediately while Gretel stops to revive Edward. Hansel and Mina follow Muriels trail and battle her at the original gingerbread house.  Muriel injures Ben and kills Mina before Gretel arrives. The siblings and Muriel then engage in a vicious fight that ends with Muriel being decapitated by Gretel, and her head being thrown into the oven, and Gretel picking up Muriels magic wand. Back in Augsburg, they burn a heap of witches on a pyre and collect their reward.

Hansel and Gretel head out on their next hunt, now accompanied by Ben and Edward. Before the credits roll, they are all seen at work tracking down and slaying witches in a desert.

===Extended cut===
Aside from additional language, gore and sexual content, and a few extra lines, the extended version features a few extra scenes.  In one scene, Sheriff Berringer blames the Mayor for the witches attack and murders him in public. The scene where Berringer and his goons assault Gretel is also extended; it occurs in between the scene where Mina heals Hansel, and shows that the men plan to rape Gretel right before Edward kills them.

==Cast==
* Jeremy Renner as Hansel, the brother of Gretel and witch hunter who takes insulin following an incident at a witchs gingerbread house
** Cedric Eich as Young Hansel
* Gemma Arterton as Gretel, the sister of Hansel and witch hunter
** Alea Sophia Boudodimos as Young Gretel
* Famke Janssen as Muriel, an evil grand witch
* Pihla Viitala as Mina, a white witch who befriends Hansel and Gretel Thomas Mann as Benjamin "Ben" Walser, a teenage fan of Hansel and Gretel
* Derek Mears as Edward (in-suit performer), a troll that is in the services of Muriel
** Robin Atkin Downes as Edward (voice)
* Peter Stormare as Sheriff Berringer, the power-hungry and brutal sheriff of Augsburg who does not trust Hansel and Gretel
* Ingrid Bolsø Berdal as Horned Witch, a member of Muriels personal coven
* Joanna Kulig as Red-Haired Witch, another Muriels minion
* Bjørn Sundquist as Jackson
* Rainer Bock as Mayor Englemann, the Mayor of Augsburg who hires Hansel and Gretel
* Thomas Scharff as Hansel and Gretels father, an unnamed farmer
* Kathrin Kühnel as Adrianna, the grand white witch who was Hansel and Gretels mother
* Zoë Bell as Tall Witch, a witch who is captured by Hansel and Gretel
* Monique Ganderton as Candy Witch, the old witch who lived in the gingerbread house

==Production==
Wirkola got the idea to create a film based on the adult lives of Hansel and Gretel in 2007 while at film school in Australia. After being discovered by Gary Sanchez Productions, Wirkola pitched the idea at a meeting with Paramount Pictures and won a contract. Production began in March 2011 at the Babelsberg Studios in Germany and included extensive use of traditional special effects. In addition, Renner and Arterton had a month of training beforehand to prepare for the physical demands of their roles. In terms of the weapons and wardrobe, Wirkola wanted an old-world look with a modern touch, and he was adamant about filming outdoors in European nature rather than in a studio. The project was filmed in Germany and featured an international cast and crew.

===Development===
 
 Paramount two days after and we sold it."  In 2013, Wirkola commented, "I’m still surprised that they went for it, because it’s a crazy, rock n’ roll script. It’s full throttle, there’s lots of blood and gore and bad language, I often wonder how I got this movie made. It’s all across the world now. But people really seem to respond to it, which is what we hoped, that people would enjoy this ride." 

An announcement of Hansel & Gretel: Witch Hunters sparked a production of several other "  trend of witch-themed films, among them  .   

===Concept and design===
 

Wirkola said, "I have a strong memory from my childhood of just how dark and gruesome their tale was and I wondered what would have happened to the two of them when they grew up? They had this dark past and this intense hatred of witches. So as I thought about it, it made sense to me that of course they would be fated to become great witch hunters. We wanted it to feel like this could be happening 300 years ago but at the same time, there is a modern spin on all the action, characters and weaponry. It was a fun way to make a classical world feel fresh."  Wirkola said that he originally came with that idea in 2007 while studying film and television at Bond University in Australia, when he wanted to make it as just a short film,  and that the film school director Simon Hunter advised him: "Tom, dont ever speak of the idea again until you are in front of a Hollywood producer and I guarantee you will sell it."   

Gary Sanchez Productions  s Braindead (film)|Braindead for having been "a game-changer" for him. 

 
 Marlene Stewart created the films costumes, using traditional leather and linen but without an antique look. Its steampunk-like, Retro-futurism|retro-futuristic weapons were created by the weapon designer Simon Boucherie and Wirkola, who said they wanted Hansel and Gretels weapons to look as if the characters hand-made them.  Wirkola stated he "just wanted this crazy, mashed-up world where you can’t pinpoint where it is, or when it is" and the modern elements are there to "add to the fun and tone of the film." William Bibbiani,  , CraveOnline, January 31, 2013  He said, "We wanted the movie to feel timeless and for the movie to feel like a fairy tale, but still grounded. It was a lot of fun coming up with the different weapon designs and ways of killing witches. We mixed old and new elements. But no matter how modern some of the weapons are, they all have an old-fashioned feel and look like they could fit into this world." Daniel Rutledge,  , 3 News, 31 Jan 2013   

The Stone Circle witches looks were designed by Twilight Creations ( . We wanted to try to avoid the classical witch with the long nose stirring the pot. I really wanted them to be...dangerous, fast – they’re stronger than Hansel and Gretel...It’s a good basis for a villain."    He recalled that "the most fun was finding their look and sound. For the main witches, we found one animal to represent all of them, like Muriel is a wolf. It just helped us find the witches. In some ways, it feels like theyre the spawn of the dark places of nature. It should feel animalistic."    Asked if he was worried about "this perception that it might be interpreted as sexism|sexist", Wirkola said, "For me, that’s a classical villain from hundreds and hundreds of years ago. Yeah, I never worried about that, to be honest."  He added, "People forget how truly dark and twisted those stories are.   talks about a witch burning alive in an oven, screaming and scratching."  Ulrich Zeidlers concept art for some of the witches was released on the Internet. 

===Casting and characters===
  and Jeremy Renner at the films Australian premiere]]

Jeremy Renner, who played the films adult Hansel, has stated that his initial attraction came from a one sheet he was given even before seeing the script, showing Hansel and Gretel walking away and a witch burning at a stake in the background, which he found "incredibly interesting"  (this scene was included in the finished film). He added, "When I read the script, my first thought was, I can’t believe this hasn’t been done yet. It’s such a great idea with so much potential. That dynamic was definitely a big thing, I loved that what Tommy   wrote left so much room for character."    Wirkola said he wanted Renner in the role after seeing him in The Hurt Locker.  Wirkola, who described Hansel as the "loose cannon" of the duo,  gave him an additional character flaw of Diabetes mellitus|diabetes, in addition to the psychological scars that Hansel shares with his sister (in the original idea for the short film, Gretel was also suffering from eating disorder ).  Renner said it was a great escape for him "as this was a fairytale with no stress like the other action movies Id done recently. I was having so much fun hanging on a wire like Peter Pan, hanging onto a broom and doing other crazy stuff."  He stated, "That was one of the most fun jobs I’ve ever had because there’s something magical about that old world, fantasy thing." 

 

The role of adult Gretel, whom Wirkola wanted to be "a really, really strong and fun female character,"  was originally planned for  s, people are scared to put that in. I think it was important."  She said that she "loved every minute" of the production and did not want it to end, also crediting it for helping her overcome her fear of getting hurt. 
 the queen bee of witches",  , cinemit, 19 January 2013  was given to Dutch actress Famke Janssen.   Janssen too described the film as "Tarantino-esque", with "a lot of blood, gore and exploding witches."  Wirkola said he had a crush on Janssen since he saw her in GoldenEye as Xenia Onatopp, "an amazing villain,"  and that in his opinion her being not only a good actress but also a beautiful woman who is "huge" and "menacing somehow" made her "a perfect combination" of "sexy and dangerous".   Janssen said that at first the initial appeal was just money, but she quickly took a liking of the script and of Wirkola personally.    She also thought the idea of playing an "evil to the core" witch "was appealing and different", saying, "I hadnt done anything like it…A character like a witch feels like you would have so much freedom, because there are no restrictions as to what you can do."  Later, however, she was constantly being distracted by the special effects work and felt that she "really understood the character" only in one part of the film.  She found the time-consuming process of applying such makeup (taking three hours to apply and one hour to remove ) "very strange" and also felt restricted and afraid of acting over-the-top in her witch role. In the end, she still had "a lot of fun" playing someone who is completely evil,  and felt that it was "so empowering"  , VibenonFilms, 4 January 2013  to have "an inner witch to get in touch with once in a while."  Janssen said the film might appeal to women and girls, too: "Theres a brother and a sister story. And theres a romance in it too - but obviously not with the witch. Nobody falls in love with the witch." 

Edward, a morally conflicted troll enslaved by Muriel,  was voiced by  , saying it felt like a dream to him.  , EPIXHD, 25 January 2013 

===Filming===
Wirkola said, "From day one, I was very clear that I wanted to shoot this thing in Europe. I really wanted that European feel of cold mountains, big forests, that sort of spirit was important to me. Luckily we did get to shoot it in Germany which is the homeland of the fairy tale. Shooting in natural outdoor sets is very important to me, compared to working on a sound stage."  It took place in Germany, at the Babelsberg Studio in Potsdam-Babelsberg, in a filming location at an old forest near Berlin (production designer Stephen Scott said that he searched for and found what he believed looked like a "medieval forest" free of human interference ) and in the city of Braunschweig in Lower Saxony.    After the film was delayed to 2013, the crew did a "couple" of Pick-up (filmmaking)|re-shoots, including "a little bit" in the deserts of California (filming the post-ending scene, an extended version of which was also released in a promotional clip "The Desert Witch").    

The filming process began in March 2011, using   and the way he shoots action scenes. I think in a lot of modern action movies, it’s hard to see what’s going on.  . Actually the 3D thing wasn’t there in the beginning. It was something the studio suggested later on. We embraced it and I think it actually really helps in getting people into this fairy tale world."  The filming process took three months.  , PreviewWorld, 7 January 2013  A 12-minute B-roll footage was later released on the Internet. 
 David Leitch compared it to a "Jackie Chan hybrid of comedy and action."  Prior to the filming, Leitch organized a month-long boot camp in order to prepare Renner and Arterton (who said she was also glad that at least she could use her prior training that she received while studying at Royal Academy of Dramatic Arts|RADA) with extensive weapons, fight and stunt training.  Janssen said she enjoyed "flying", something that she always wanted to do, adding that despite her reputation as an action star it was the first film where she really had to do something physical (including suffering a minor accident on the set ),  because she could not use a double in the close-up scenes where she had the makeup on.  During one of the scenes, where Gretel is thrown through a wall and falls down several meters, a stunt double for Arterton was dangerously injured when a nail got lodged in her skull close to the brain; Arterton said she initially wanted to do this stunt herself but Wirkola would not let her.  Arterton herself suffered an injury when she sprained her ankle while running through the forest.  Stunts for the more aggressive witches were done by New Zealander stuntwoman and actress Zoë Bell. Bill Desowitz,  , Animation World Network, January 30, 2013  Renner did practically all of his stunt scenes himself.  , MSN, 13-01-15  Van McNeil,  , 99.1 The Mix, Jan 25 2013 

===Visual effects and post-production===
 

The films visual effects were created using mostly   movies and I just loved it."  Wirkola added: "I come from Norway where we can’t afford CGI. But this is a fantastical world of witches and trolls and I wanted to ground the movie where I could. The blood should look real."  Janssen, however, despite Wirkolas warnings, "was not entirely prepared for how involved and long that was going to be" and "actually wanted to burn the  s Spectral Motion, who also created and handled the animatronic troll.  Jon Farhat was the visual effects supervisor and aerial second unit director. 

Conceptual design and production studio Picture Mill collaborated with Wirkola and Messick to create the title and opening credits sequence telling some of the early adventures of Hansel and Gretel as they grew up to become famous witch hunters. It was created with Stereo3D Toolbox through a combination of hand-drawn illustrations, practical fire effects and CGI animation.  The digital color correction was supervised by Stefan Sonnenfeld.   

 

Janssen said the film is "definitely played with a bit of a wink and doesnt take itself too seriously."  Wirkola himself described it as "a little more grounded" and action-centered than Dead Snow.  He recalled that he has tried to downplay comedy elements: "If you go too far, it can turn into a spoof almost...We shot a lot more than what is in the movie of course and it’s just balancing it when you’re cutting."  Speaking of   and shooting went very smooth, but the post-production was very new to me and how they do things here with testing and the studio."  He said about the test screenings in particular: "I can see why they do it – there’s a lot of money involved and they want it to hit as broad as possible. But I think it’s a flawed process, I really do."  McKay said that Paramount executives might nevertheless regret the films R rating.  , Bloody-Disgusting.com, June 07, 2013   

==Music==
 ) created the   heritage and said that their discussions about the music for the film "might have had some of the most cold and dark humor of any Hollywood music meetings." 

The films soundtrack Hansel & Gretel Witch Hunters - Music from the Motion Picture was released in MP3 format by Paramount Music on January 22, 2013.     A physical soundtrack album was released on January 29, 2013 from La-La Land Records.  The song "Bundy (song)|Bundy" by Norwegian rock band Animal Alpha, that is used for the films end credits, is not included in the soundtrack, but only in the album Pheromones (album)|Pheromones.

{{Infobox album  
| Name       = Hansel & Gretel Witch Hunters - Music from the Motion Picture
| Type       = Soundtrack
| Artist     = Atli Örvarsson
| Cover      =  
| Released   = January 22, 2013 (digital), January 29, 2013 (physical)
| Recorded   = 
| Genre      = Orchestral score|Orchestral, Electronic music|electronic, choir
| Label      = Paramount Music, La-La Land Records
}}

{{tracklist
|title1=The Witch Hunters
|length1=2:29
|title2=Business Is Good
|length2=2:11
|title3=Trolls Serve Witches
|length3=3:33
|title4=Lost Children Crying, Vol. 2
|length4=2:33
|title5=You Do the Bleeding
|length5=3:34
|title6=There Are Good Witches in the World
|length6=4:11
|title7=This Place Could Use a Bit of Color
|length7=4:17
|title8=Goodbye Muriel
|length8=3:21
|title9=Dont Eat the Candy
|length9=3:48
|title10=Burn Em All
|length10=5:03
|title11=White Magic
|length11=1:52
|title12=Shoot Anything That Moves
|length12=3:29
|title13=The Fairy Tale
|length13=2:53
|title14=Augsburg Burns
|length14=4:20
}}

==Distribution==
  ]]

===Theatrical release=== trailer for the film was released on September 5, 2012. 

The film was again delayed by two weeks to January 25, 2013 in the United States and Canada. A statement from Paramount suggested that the film was delayed to enable it to be released in    and D-Box Technologies|D-Box motion enhancement technologies.  In Mexico, it was the first feature film to be shown in Cinemexs new X4D Motion EFX theater format, provided by MediaMation.  

===Home media=== AVC video UltraViolet digital copy and three behind-the-scenes special features: "Reinventing Hansel & Gretel" (15:41m), "The Witching Hours" (9:01m) and "Meet Edward the Troll" (5:25m).  In addition, just the theatrical cut of the film will be made available on a single DVD.   The unrated version is 10 minutes longer (98 minutes long). Paramount Home Media Distribution,  , PR Newswire. 

==Reception==

===Box office=== CIS ), which made its international prospects look good, according to Yahoo! News.    Entertainment Weekly predicted the film would "likely top the chart in its debut weekend" with $17 million;  similarly, MovieWeb predicted $17.5 million.  Paramount said they counted on $20 million.  Speaking on January 31, Wirkola claimed the film had so far "opened in 19 territories and went to number one in 18 of them."  The U.S. initial midnight screening at selected theaters made an estimated $500,000, a "so-so result".  The film took the top position on its first days screening at 3,373 locations across the country, earning an estimated $6 million and suggesting a three-day total of $15–17 million, according to various estimates.     
 blockbuster in the United States", it was much better received in other countries for a $205.9 million total haul to date, including $24.1 million in Brazil, $19 million in Russia, $14 million in Mexico and $12.8 million in Germany. Greg Gilman,  , Reuters, March 19, 2013.  The films theatrical run ended on April 25, with a total gross of $225.7 million including $55.7 million domestically. 

Overall, Hansel and Gretel: Witch Hunters was Paramount Pictures fourth highest grossing film of 2013, following new graphic novel and franchise entries:  , respectively.  It is estimated that, following published production costs, marketing and advertising fees, and the percentage of overseas revenue that was lost by the domestic studio, similar fantasy reboots such as  , and   of the recent entries. 

===Critical reception===
 

Hansel & Gretel: Witch Hunters was initially met with largely negative critical reviews,  with a 15% approval rating and an average rating of 4/10 from the 126 reviews on   of same."   ."  Renner said: "We knew this was never going to be a movie for the critics. Im just hoping that people go along and can have some fun with it. Its pure escapism."  McKay said he "sort took the critical reviews as almost kind of lazy, when they said it wasnt any good. Its just from a distance you could tell certain critics were just going to write it off immediately."  The film was one of the top seven contenders for the 2014 Academy Award in the category Best Make-Up and Hairstyling, but failed to receive a nomination.   

Claudia Puig of   gave it one star out of four, comparing this "big-budget faux fairy tale about skanky witches" to a "downscale video game for dull-eyed teens happy to lap up lame wisecracks and lots of gore."  According to   rated it a 4.5/10, stating "there are a few funny moments, but overall Hansel and Gretel: Witch Hunters is too similar to many films we’ve seen before."   Calum Marsh of Slant Magazine lambasted the films "sub-Tim Burton aesthetic" and wrote that "the result suggests A Knights Tale as penned by Seth MacFarlane," giving it zero out of four stars.  Scott A. Gray of Exclaim! praised some elements of the film, such as "lush and colourful art design that recalls a Guillermo Del Toro production, sound creature makeup and special effects, decently choreographed action scenes and a pair of leads who do their damnedest to sell the limp script", giving it a score of 4/10.  Chris Knight of The Vancouver Sun called it a "mess of a fairy tale", expecting "a wiccan outcry at the film’s depiction — nay, endorsement — of the torture of witch-folk."  Liam Lacey of The Globe and Mail gave it one star out of four, concerned how it "has an alarming number of females being strung up, burned, shot, decapitated and eviscerated." 

Some reception of the film, however, was much more positive, in particular by reviewers for horror outlets.  , the film "is a mixed bag" but "fortunately, the lows don’t appear as often as you would think," and, "if taken at face value, Hansel & Gretel is well worth the view."  William Bibbiani of   gave it four out of five stars, stating: "This has all the elements of a classic action flick, and its destined for cult status. Welcome to the most fun movie of 2013."  DVD Talks Jamie S. Rich summed up: "Is Hansel and Gretel: Witch Hunters a good movie? Probably not. At least not in any way that is defensible by regular critical standards. Is it a hell of a good time? Absolutely so. Unabashedly so. Thats all it wants to be." 

 

Neil Geinzlinger of  /zombie film|zombies, or if the hinted-at franchise potential has any legs, but this was a perfectly vulgar way to spend 90 minutes."  Tim Grierson of Screen International wrote that Hansel and Gretel "works best as an unapologetic B-movie action flick" and "feels like a first film in a franchise that’s meant to set up the main characters and conflicts, which can then be fleshed out in sequels" but "the problem is that there isnt enough here to warrant a return trip to this semi-magical land."  Richard Corliss of Time (magazine)|TIME agreed with this sentiment, stating that "one might be enough — too much, for some tastes."   

===Cult status===
At the conclusion of 2013, Hansel and Gretel: Witch Hunters made multiple annual movie lists dedicated to underappreciated or underrated films.     by a number of horror-fantasy film fans, as well as by some critics who initially saw no merit in movie. This significant financial and fan following have led some to question the initial perception of the film in relation to its debut in January, considered to be "a refuge for films that dont have a shot of generating good word of mouth or having a leggy run."  While Renners box office draw and the films "well-articulated premise" have been listed as major factors behind its  theatrical success, its ability to maintain a lasting fan following has also been attributed to its original concept, excellent gore and action sequences, Renners and Artertons tongue-in-cheek performances, and the films ability to not take itself too seriously.     The film was nominated for the 40th Peoples Choice Awards in the category Favorite Horror Movie, but lost to Carrie (2013 film)|Carrie.

==Sequel==
On March 19, 2013, Paramount announced that a sequel to the film is in the works due to overseas box office numbers. Hansel & Gretel: Witch Hunters producers Ferrell, McKay, Messick, and Flynn were reported to be expected to return.   In June 2013, McKay officially confirmed his involvement and said that "everyone feels like as good as the first one is, we can really jack it up a level with the second one. So, fingers crossed everyone will come back" (including Renner and Arterton).  McKay suggested that Paramount might insist harder to make the film rated PG-13 this time.  Talking about Wirkolas "pretty insane" ideas for the second film, McKay said this could be one of those instances where the sequel does go further than the first one."  Regarding the storyline, he offered one hint: "Think of different kinds of witchcraft."  It was revealed that the sequel will have a 2016 release date. 

In September 2014, Wirkola announced that he would not be returning to direct the sequel: “I have been lately attached to a few more things, it’s just a matter of time and priorities. I did  . I want to do something a little bit different now and not just do sequels. I did write the script and I hope to be involved in it. But yeah, I won’t be directing it.”  He said "the first film was a learning curve for me working in the studio system and the script I wrote for the original was so different  . We ended up taking a lot out and altering stuff that first time. I tried taking what I learned and still delivered a sequel script that’s an R-rated action film."  Cinema Blend reported Renner said that despite his busy schelude he agreed to reprise the role of Hansel because the first film "was a lot of fun doing the movie," but without Wirkola directing again he might be less likely to return for the sequel. 

==See also==
 
* Hansel & Gretel Get Baked
*   The Brothers Grimm
* List of films featuring diabetes
* List of 3D films
* List of 4DX motion-enhanced films
* List of D-Box motion-enhanced theatrical films
* List of films shot in digital
 

==Notes==
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 