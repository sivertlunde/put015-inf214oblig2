The Charge of the Light Brigade (1936 film)
{{Infobox film
| name           = The Charge of the Light Brigade
| image          = Thechargeofthelightbrigade1936.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Curtiz
| producer       = {{Plainlist|
* Samuel Bischoff
* Hal B. Wallis
* Jack L. Warner
}}
| screenplay     = {{Plainlist|
* Michael Jacoby
* Rowland Leigh
}}
| based on       =  
| starring       = {{Plainlist|
* Errol Flynn
* Olivia de Havilland
}}
| music          = Max Steiner
| cinematography = Sol Polito
| editing        = George Amy Warner Bros. Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $1,200,000 
| gross          = 
}}
 Sherwood Lake, Sierra Nevada mountains were used for the Khyber Pass scenes. 

The film starred Errol Flynn and Olivia de Havilland. The story is very loosely based on the famous Charge of the Light Brigade that took place during the Crimean War (1853–56). Additionally, the story line seems to include the Siege of Cawnpore during the Indian Rebellion of 1857.

This was the second of nine films in which Errol Flynn and Olivia de Havilland starred together.

== Plot == East India Company dominance over the Indian subcontinent. Perry has secretly betrayed Geoffrey by stealing the love of his fiancee Elsa (Olivia de Havilland).

During an official visit to local tributary rajah, Surat Khan (C. Henry Gordon), Geoffrey saves the rajahs life. Later, Surat Khan massacres the inhabitants of Chukoti (mainly the dependents of the lancers), and allies himself with the Imperial Russia|Russians, whom the British are fighting in the Crimean War. He spares Elsa and Geoffrey as they flee the slaughter to repay his debt to Geoffrey.
 vengeance are Light Brigade, Sir Benjamin Warrenton (Nigel Bruce). Vickers then orders the famous suicidal attack so the lancers can avenge the Chukoti massacre. He writes a note to Macefield explaining his actions and forces his brother Perry to deliver it, sparing him from almost certain death. Just as in real life, the attack succeeds in reaching the Russian artillery positions. There, Vickers finds and kills Surat Khan, at the cost of his own life.

After receiving Vickers note, Macefield takes responsibility for the charge and burns the note to protect Vickers good name.

==Cast==
 
* Errol Flynn as Major Geoffrey Vickers
* Olivia de Havilland as Elsa Campbell
* Patric Knowles as Captain Perry Vickers
* C. Henry Gordon as Surat Khan 
* David Niven as Captain James Randall
* Nigel Bruce as Sir Benjamin Warrenton
* Spring Byington as Lady Warrenton
* Donald Crisp as Colonel Campbell
* Henry Stephenson as Sir Charles Macefield
* G. P. Huntley, Jr. as Major Jowett
* E. E. Clive as Sir Humphrey Harcourt
* Robert Barrat as Count Igor Volonoff
* J. Carrol Naish as Subahdar-Major Puran Singh
* Walter Holbrook as Cornet Barclay
* Princess Baigum as Premas Mother
* Charles Sedgwick as Cornet Pearson
* Scotty Beckett as Prema Singh
* George Regas as Wazir
* Helen Sanborn as Mrs. Jowett
 

==Production==
===Development===
The charge had been portrayed in a British movie, The Jaws of Death, in 1930.
 Lives of a Bengal Lancer (1935) had been released to great popularity, ushering in a series of British Empire adventure tales. Michel Jacoby had developed a story based on the famous charge but, although Warners bought Jacobys script, the final script was closer to Lives of a Bengal Lancer. Tony Thomas, Rudy Behlmer * Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 45-50 

An original working title was The Charge of the 600. Tennyson s Celebrated "Charge of the Light Brigade " Inspiration for New Film: Miss Colbert Will Be Star of Frenchy Moroni Olsen Given Musketeer Role; Jones to Sing
Schallert, Edwin. Los Angeles Times (1923-Current File)   13 June 1935: 13.   

Warners wanted an all-British cast. Errol Flynn (Australian, but considered Irish) had made such a strong impression in Captain Blood he was removed from supporting Frederic March in Anthony Adverse to play the lead in Charge of the Light Brigade. Chaplins Big Business: Goldwyns Leading Lady: A New Romantic Hero Ian Hunter was connected to the film early on.. Irvin S Cobb, Famous Humorist, Signs to Star at Twentieth Century-Fox: Zanuck Picks Story for Writer-Player Francine Larrimore Arriving October 20 to Do Picture Work for M.-G.-M.; Featured Roles Awarded Melvyn Douglas
Schallert, Edwin. Los Angeles Times (1923-Current File)   18 Sep 1935: 9.   Anita Louise was announced as the female lead. SCREEN NOTES
New York Times (1923-Current file)   20 Mar 1936: 28.  

Patric Knowles had just joined Warner Bros at the recommendation of Irving Asher in London, the same man who recommended Errol Flynn. He was given the crucial support part of Flynns brother. Irvin Cobb, Film Future Secure, to Star in "Gentleman From Mississippi": Writers Thespianic Adventure Proceeds Claude Rains and Charles Boyer Both Will Have Fling at Napoleon Interpretation; Choir Singer in "Stagestruck"
Schallert, Edwin. Los Angeles Times (1923-Current File)   09 Mar 1936: 15.   The movie gave an early important role for David Niven. Bette Davis, Academy Winner, Will Break Up Film Duties With Vacation: Star Going to Palm Springs, Honolulu Randolph Scott Will Portray Scout Hawkeye in "Last of Mohicans;" Sol Lesser Plans Films for Twentieth-Century Release
Schallert, Edwin. Los Angeles Times (1923-Current File)   07 Mar 1936: 5.  

Edward G. Robinson tested for the role of the lead villain Surat Khan. Basil Rathbone was also considered before C Henry Gordon was cast. Franchot Tone Selected to Appear Opposite Jean Harlow in "Suzy" Film: George Fitzmaurice Will Direct Feature Harriet Hilliards Next Picture Will Be "Make a Wish;" Capra Seeking Tibetans; Beverly Roberts Wins Leading Part
Schallert, Edwin. Los Angeles Times (1923-Current File)   20 Mar 1936: 15.  

===Shooting===
Shooting started April 1936. Jungle Scene Gives Actors Extra Thrill
Shaffer, George. Chicago Daily Tribune (1923-1963)   15 Apr 1936: 22.  

During filming on location at Lone Pine California the unit helped put out a fire which started at a restaurant across the road from where the actors were staying. Movie Crew Aids Firemen
New York Times (1923-Current file)   02 Apr 1936: 29.  

There was some shooting done in Mexico where there were less restrictions on hurting animals. NEWS OF THE SCREEN: Forward the Light Brigade, to Mexico -- More Costumes for Hepburn -- Trivial Matters.
New York Times (1923-Current file)   15 June 1936: 24.  

===The Charge sequence=== The Charge of the Light Brigade. The lancers charge into the valley and brave the Russian cannons, and many are killed. Text from Tennysons poem is superimposed on the screen, coupled with Max Steiners musical score. Director Michael Curtiz, who did not have an excellent command of English, shouted "Bring on the empty horses", meaning "riderless horses". David Niven used this as the title of his book about the Golden Age of Hollywood.

The battlefield set was lined with trip wires to trip the cavalry horses. Dozens were killed during filming, forcing U.S. Congress to ensure the safety of animals in motion pictures. The ASPCA banned trip wires from films in its guidelines as well. Unlike the rest of Flynns blockbuster films, because of the use of trip wires and the number of horses killed, it was never re-released by Warner Brothers.

==Inaccuracies==
The film originally featured the Siege of Cawnpore during the Sepoy Rebellion. When someone pointed out that the Sepoy Rebellion took place three years after the Battle of Balaclava, the name of Cawnpore was hastily changed to Chukoti, and the rebellion was turned into a fictional uprising led by the fictional Surat Khan, the leader of the fictional country of Suristan, a vaguely Turkish country. Suristan is in fact an ancient Persian name for Syria. Niven comments on the change in his autobiography.

The reason for the Charge of the Light Brigade was shown in the film as being because the 27th Lancers changed the direction of the manoeuvre so as to invade the Russian camp to kill Surat Khan. It was actually as a result of a dispute between Lord Cardigan and Lord Raglan. Moreover, the Battle of Balaclava did not result in the fall of Sevastopol|Sebastopol, as is erroneously stated in the film.

Finally, the 27th Lancers are fictional as well. The 17th Lancers, 8th and 11th Hussars, and the 4th and 13th Light Dragoons made the real charge. A "27th Lancers" were not a part of the British Army until 1941.

The filmmakers were well aware of the historical inaccuracy of the film and were very open about it. In fact, at the very beginning of the film there is a disclaimer about the historical veracity of the film.

The filmmakers were also careless in the depiction of the Union Flag, which appears several times flying upside down. 

==Awards== Jack Sullivan won the Academy Award for Best Assistant Director for his work on the film, and the film was also nominated for the Academy Award for Sound (Nathan Levinson) and the Academy Award for Original Music Score.   

==See also==
* List of American films of 1936

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 