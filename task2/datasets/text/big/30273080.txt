Atlantic Flight
{{Infobox film
| name           = Atlantic Flight
| image          = Dick Merrill.jpg
| caption        = Theatrical release poster
| director       = William Nigh
| producer       = Scott R. Dunlap, Executive producer William Berke, Associate producer
| writer         = Scott Darling (Screenplay/story)  Erna Lazarus (Screenplay/story)
| starring       = Dick Merrill   Jack Lambie    J Paula Stone
| music          = 
| cinematography = Paul Ivano
| editing        = Roy V. Livingston (as Roy Livingston)
| distributor    = Monogram Pictures
| released       = August 25, 1937
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         = 
}}
Atlantic Flight is a 1937 film made by Monogram Pictures chiefs Trem Carr and W. Ray Johnston who had just reformed the studio after having had a troubled merger for two years with Herbert J. Yates and Republic Pictures. This film was conceived as a low-budget feature meant to capitalize on Dick Merrills and Jack Lambies historic "Coronation Flight", which made them world famous. Recreating the flight that made him famous, Dick Merrill, along with co-pilot Jack Lambie reprise their real-life roles as pilots.

==Plot== Stanley Trophy Race, to make her keep her promise to marry him, and resorts to desperate measures, knocking out Bennett.

When Bennett is unable to fly his untested racer, Edwards takes it up instead, but crashes. Strong realizes she is in love with Edwards, but he is critically injured. A doctor in England has life-saving serum that Bennett and Jack Carter (Jack Lambie) are determined to bring back to save their friend, by making a record-breaking round-trip to London. Coming back through a raging storm over the Atlantic, their aircraft is struck by lightning, disabling their radio. When contact is lost, Coast Guard ships are deployed, but the intrepid flyers make it in.

==Cast==
As appearing in screen credits: 
* Dick Merrill as Dick Bennett
* Jack Lambie as Jack Carter
* Paula Stone as Gail Strong
* Weldon Heyburn as Bill Edwards
* Milburn Stone as Henry Wadsworth "Pokey" Schultz
* Ivan Lebedeff as Baron Hayygard
* Lyle Morraine as Lyle, Navy Lt.

==Production== Lockheed Model 10E Electra dubbed "Daily Express". Hearst wanted to scoop other American newspapers by acquiring photos of the May 10, 1937 coronation of King George VI after the abdication of his brother, King Edward VIII. Merrill carried photos (not newsreels, as often claimed) of the Hindenburg disaster, which occurred shortly before the flight. Hearst papers on both sides of the Atlantic published the first pictures of each event, and Merrill earned the Harmon Trophy for 1937 for his achievement. Footage from this flight was used to make the Atlantic Flight. Dramatic air race and crash scenes were also used. Milburn Stone who played aircraft mechanic Henry Wadsworth "Pokey" Schultz, was in his first major role; his cousin, Paula Stone appeared as Gail Strong.

Both Dick Merrill and Jack Lambie, his real-life "Coronation Flight" co-pilot and co-star in the film, received $2,500 for their roles in this movie. Dick had not taken the filming seriously but gladly accepted the windfall. He was a teetotaler in an age when the "hard-drinking" "fun-loving" aerial adventurer was seen as the norm. Considered very easy-going yet serious, his one foible, however, was that he was an inveterate gambler throughout his life. Merrill blew his entire salary from the film at Santa Anita the weekend after shooting wrapped. Cooper. Ralph.   earlyaviators.com, October 13, 2009. Retrieved: July 16, 2010. 

Merrill made two more transatlantic flights, the last of these, on May 14, 1937, set the new record at 24 hours, 25 seconds.   time.com. Retrieved: September 7, 2009. 

===Aircraft used in the film=== Lockheed Model 10E Electra
* Northrop Gamma
* Vultee V-1

==Reception==
Strictly a "B" feature, the Atlantic Flight is best considered a historical record, with a great deal of aviation footage of the period to commend it.

==References==

===Notes===
 

===Bibliography===
 
* Backstreet, Jack.   IMDb.com. Retrieved: January 3, 2008.
* King, Jack. Wings of Man: The Legend of Dick Merrill. Seattle: Aviation Book Co., 1981. ISBN 0-911721-91-6.
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 