Rajakumaran (film)
{{Infobox film
| name           = Rajakumaran
| image          = Rajakumaran movie poster.svg
| image_size     = 
| caption        = Movie poster
| director       = R. V. Udayakumar Prabhu
| story          = Sujatha Udhayakumar
| screenplay     = R. V. Udayakumar
| writer         = Gokula Krishna (dialogues) Prabhu Meena Vijayakumar Sujatha Sujatha Koundamani Senthil Vadivelu Captain Raju
| music          = Ilaiyaraaja
| cinematography = Abdul Rahman
| editing        = B. S. Nagaraj N. Kapilan
| distributor    = Sivaji Productions
| studio         = Sivaji Productions
| released       = 14 January 1994
| runtime        = 145 minutes
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1994 Tamil language drama film directed by R. V. Udayakumar. The film features Prabhu (actor)|Prabhu, Meena Durairaj and Nadhiya in lead roles. The film was released on 14 January 1994 coinciding with Thai Pongal. The film was Prabhus 100th film and was distributed by Sivaji Productions.  

==Plot==

Rajakumaran (Prabhu (actor)|Prabhu) is the son of the village chief (Vijayakumar (actor)|Vijayakumar). Yuvaraj (Nassar) hates Rajakumaran and his village. Selvi (Meena Durairaj) and Vaidehi (Nadhiya) are in love with their cousin Rajakumaran but he chooses Vaidehi. Selvi decides to sacrifice her love, her father Selvaraj (Captain Raju) refuses to give his property to village people as promised and his only request is that Rajakumaran gets married with Selvi. Selvi convinced Rajakumaran to lie, he lies to her father and he gives his property. Vaidehi committed suicide and Rajakumaran reveals his lie. One day, some goons decide to put a bomb and Rajakumaran saves the village. The village thinks that the culprit was Thangaraj (Thyagu) because he wanted to revenge Rajakumaran after his lie. Angry, Thangaraj insults Rajakumarans father in public and Rajakumarans father died. Yuvaraj asks to Selvaraj and Thangaraj to marry Selvi, and they accept. The village was against the marriage. A man (R. V. Udayakumar) came and said that a deaf-mute girl, who learnt to read and to write, was the witness of Vaidehis murder by Yuvaraj. On the marriage day, Yuvaraj cancelled the marriage and decided to humiliate Selvi. Rajakumaran saves her and he decides to marry her.

==Cast==
 Prabhu as Rajakumaran
*Meena Durairaj as Selvi
*Nadhiya as Vaidehi
*Nassar as Yuvaraj Vijayakumar as Rajakumarans father Sujatha as Rajakumarans mother
*Koundamani as Maarusamy, the milkman Senthil as the postman
*Vadivelu as Vicharuva Veerasamy
*Captain Raju as Selvaraj, Selvis father Thyagu as Thangaraj
*Sathyapriya as Vaidehis mother
*R. V. Udayakumar in a cameo appearance

==Soundtrack==

{{Infobox album |  
  Name        = Rajakumaran |
  Type        = soundtrack |
  Artist      = Ilaiyaraaja |
  Cover       = |
  Released    = 1994 |
  Recorded    = 1993 | Feature film soundtrack |
  Length      = 33:36 |
  Label       = |
  Producer    = Ilaiyaraaja |  
  Reviews     = |
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1994, features 7 tracks with lyrics written by, the director himself, R. V. Udayakumar.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration 
|-  1 || Aadi Varattum || S. P. Balasubramaniam, K. S. Chithra || 5:16
|- 2 || Chinna Chinna Sol Eduthu || K. J. Yesudas, S. Janaki || 5:08
|- 3 || Ennavendru Solvathamma || S. P. Balasubramaniam || 5:04
|- 4 || Kaatule Kambakaatule || S. P. Balasubramaniam, S. Janaki || 5:01
|- 5 || Pottu Vachathu Yaaru || S. P. Balasubramaniam  || 4:01
|- 6 || Rajakumara Rajakumara || Mano (singer)|Mano, Sunandha || 4:25
|- 7 || Sithagathi Pookale || S. P. Balasubramaniam, K. S. Chithra || 4:41
|}

==References==
 

 

 
 
 
 
 