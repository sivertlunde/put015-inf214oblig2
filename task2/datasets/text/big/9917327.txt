The Lost Special (serial)
{{Infobox film
| name           = The Lost Special
| image size     =
| image	=	The Lost Special FilmPoster.jpeg
| caption        =
| director       = Henry MacRae
| producer       = Henry MacRae Ella ONeill George Morgan George H. Plympton Basil Dickey Arthur Conan Doyle (original story)
| narrator       =
| starring       = Frank Albertson Ernie Nevers Cecilia Parker Caryl Lincoln
| music          = John Hickson
| editing        = Alvin Todd Edward Todd
| distributor    = Universal Pictures
| released       =   5 December 1932
| runtime        = 12 chapters (240 min)
| country        =   English
| budget         =
| preceded by    =
| followed by    =
}} Universal Serial movie serial American Western (genre)|West.

This was the 84th serial (and the 16th serial with sound) to be released by Universal.

==Plot overview==
A special train carrying gold bullion is hijacked on its way from the Golconda Mine. Laying down portable tracks, the bandits take the train off the main line, hide it in an abandoned mine shaft, steal the gold, and pick up their makeshift tracks, leaving a mystery in their wake. Part owner of the mine, Potter Hood, and the railroad president, Horace Moore, search for the mysteriously disappeared train and gold. They are unaware, however, that the criminals are working secretly for Sam Slater, the other partner in the gold mine, who wants to sabotage mine operations enough that he can take over completely. Potters son, Tom Hood, arrives home from college and determines to solve the mystery with the aid of his pal Bob Collins. They board the gold-shipment special train on its next run. Meanwhile, newspaper reporter Betty Moore – who is niece to the railroad president – and her friend Kate Bland begin their own investigation. After the four youths foil an attempt at a second heist, they join forces. The next 11 chapters show the characters attempts to locate the "Lost Special" train and identify the ringleader.   

As is typical in serial films, each episode ends on a cliff-hanger. For example: the two girls are driving in a car along a road that parallels the speeding train, when a henchmans revolver shatters the windshield of Bettys car, blinding her and sending her car toward the on-rushing train. Other cliff-hanger endings include a runaway car sailing off the cliff into a lake and our heroes being trapped by rising water in a dungeon. 

==Cast==
*Frank Albertson as Tom Hood, son of Potter Hood
*Ernie Nevers as Bob Collins, Toms friend.
*Cecilia Parker as Betty Moore, Reporter and niece of Horace Moore Francis Ford as Potter Hood, part owner of the Golconda gold mine
*J. Frank Glendon as Sam Slater, part owner of the Golconda gold mine
*Frank Beal as Horace Moore, owner of the railroad
*Caryl Lincoln as Kate Bland, friend of Betty Moore
*Tom London as Dirk/Detective Dane
*Al Ferguson as Gavin
*Jack Clifford as Doran
*Edmund Cobb as Spike
*Joe Bonomo as Joe
*George Magrill as Lefty Harold Nelson as Professor Wilson

One of the leading man, Ernie Nevers, was an American Football star.   Another sports star, Reb Russell, was cast in the role of "One of Bobs College Pals."

==Stunts==
*George DeNormand
*George Magrill

==Production==
The advertising for the serial stated:  Heroes of Jungle Mystery"
"Terrifically fast, tremendously thrilling, and packed right with the sort of mystery that only the touch of a master writer can give!"    
"12 CHAPTERS OF TERRIFIC THRILLS and MYSTERY" 
}}

The Lost Special was Universals 84th serial. In terms of Universals serials in the sound era, it was the 16th.  
 George Morgan, and George Plympton. These scenarists specialized in believable, likable characters and snappy dialogue that enhances the story. 

The director was Henry MacRae. By the time he directed The Lost Special, MacRae had directed more than 100 short films and feature films. 

==Chapter titles==
# The Lost Special
# Racing Death
# The Red Lantern
# Devouring Flames
# The Lighting Strikes
# The House of Mystery
# The Tank Room Terror
# The Fatal Race
# Into the Depths
# The Jaws of Death
# The Flaming Forest
# Retribution
 Source:  

Each of the 12 chapters ran 20 minutes, for a total series screen time of 240 minutes.   

==See also==
*List of film serials by year
*List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{Succession box Universal Serial Serial
| before=The Jungle Mystery (1932 in film|1932)
| years=The Lost Special (1932 in film|1932)
| after=Clancy of the Mounted (1933 in film|1933)}}
 

 

 

 
 
 
 
 
 
 
 
 
 