Bulgarian Rhapsody
 
{{Infobox film
| name           = Bulgarian Rhapsody
| image          = Bulgarian Rhapsody.jpg
| caption        = Theatrical release poster
| director       = Ivan Nitchev
| producer       = Leon Edery Moshe Edery Nissim Levy Shaul Scherze
| writer         = Yurii Dachev Tatyana Granitova Jean Pierre Magro Ivan Nitchev Stefan Popov Kristiyan Makarov Anjela Nedyalkova
| music          = Stephan Dimitrov
| cinematography = Addie Reiss
| editing        = Tatyana Bogdanova Isaac Sehayek
| distributor    = Cinepaz EOOD Cinisima
| released       =  
| runtime        = 108 minutes
| country        = Bulgaria Israel
| language       = Bulgarian
| budget         = 1,776,200 Bulgarian lev
}}
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.    There was some controversy in the selection due to Nitchevs involvement with the Bulgarian National Film Council.   
 Bulgarian Judaism, which consists of the films: After the End of the World (1999) and The Journey to Jerusalem (2003). 

==Cast== Stefan Popov as Giogio
* Kristiyan Makarov as Moni
* Anjela Nedyalkova as Shelli
* Moni Moshonov as Moiz
* Stoyan Aleksiev as Abraham
* Alex Ansky as Albert
* Tatyana Lolova as Fortune
* Dimitar Ratchkov as Ivan Georgiev

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Bulgarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 