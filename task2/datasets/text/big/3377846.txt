Crime and Punishment in Suburbia
{{Infobox Film
| name           = Crime and Punishment in Suburbia
| image          = Crime and punishment in suburbia.jpg
| image_size     =
| caption        = Promotional movie poster for the film
| director       = Rob Schmidt
| producer       = Christine Vachon Pamela Koffler Larry Gross
| writer         = Larry Gross
| starring       = Monica Keena Ellen Barkin Michael Ironside Vincent Kartheiser Conchata Ferrell
| music          = Michael Brook
| cinematography = Bobby Bukowski
| editing        = Gabriel Wrye
| distributor    = United Artists
| released       =   January 24, 2000 (Sundance Film Festival)   August 25   March 28, 2001   May 3, 2002
| runtime        = 100 min.
| country        = United States|U.S.A. English
| budget         = 
| followed_by    = 
}}
Crime and Punishment in Suburbia is a 2000 film directed by Rob Schmidt and starring Monica Keena (as Roseanne Skolnick), Ellen Barkin (as Maggie Skolnick), Michael Ironside (as Fred Skolnick), James DeBello (as Jimmy) and Vincent Kartheiser (as Vincent).

==Synopsis== based on Jeffrey Wright) and leaves her to live alone with her alcoholic stepfather (Michael Ironside). One night during an alcohol-fueled rage, he rapes Roseanne.  Traumatized, she decides to take things into her own hands.  With the participation of her devoted and clueless boyfriend Jimmy (James DeBello) the twosome murder her stepfather in retribution, but Roseannes conscience quickly begins to unravel afterwards.

The story is narrated by one of Roseannes classmates, Vincent (Vincent Kartheiser), a boy who is as concerned with Roseannes well-being as he is obsessed with her.  As the plot develops he forges a relationship with her, consoling her and giving her advice while trying to point her toward redemption.   In the end it becomes possible that he, perhaps, might be her only salvation.

==Awards==
The film was nominated for the Grand Jury Prize, Dramatic at the 2000 Sundance Film Festival.

==External links==
*  
*  

 

 
 
 
 
 


 