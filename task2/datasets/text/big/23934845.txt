Ride Ranger Ride
{{Infobox film
| name           = Ride Ranger Ride
| image          = Ride_Ranger_Ride_Poster.jpg
| caption        = Theatrical release poster
| director       = Joseph Kane
| producer       = Nat Levine
| screenplay     = {{Plainlist|
* Dorrell McGowan
* Stuart E. McGowan
}}
| story          = {{Plainlist|
* Bernard McConville
* Karen DeWolf
}}
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* Kay Hughes
}}
| music          = Harry Grey (supervisor)
| cinematography = William Nobles
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 54 minutes
| country        = United States
| language       = English
}} Western film directed by Joseph Kane and starring Gene Autry, Smiley Burnette, and Kay Hughes. Based on a story by Bernard McConville and Karen DeWolf, and a screenplay by Dorrell and Stuart E. McGowan, the film is about a Texas Ranger working undercover to protect an Army wagon train full of ammunition and supplies. The Army doesnt believe him at first, until the Commanches arrive.   

==Plot== Fort Adobe, Texas. Their romantic aspirations are cut short when an Indian war party threatens to cause trouble. A peace treaty is signed with the help of Duval (Monte Blue), the forts interpreter. 

Gene and his buddies, Frog Millhouse (Smiley Burnette) and Rufe Jones (Max Terhune), suspect that Duval is working with the Indians, and they go to his canteen to investigate. Confronted by the Texas Ranger, Duval tries to kill Gene and a barroom brawl ensues. Afterwards, Gene and his friends are court-martialed for their involvement in the fight. When Gene tries to warn Colonel Summerall and Major Crosby about the interpreters involvement with the Indians, he is arrested for the murder of a Comanche brave. 

Known to the Indians as Chief Tavibo, Duval is now free to continue his plot to re-route a supply train so that the Comanches can attack and capture the cavalrys ammunition. After Gene escapes jail, he joins the bloody battle between the cavalry and the Comanches. Soon Frog arrives with the Texas Rangers to win the battle, during which Duval is killed and his true identity is revealed. After peace is restored, Colonel Summerall apologizes to Gene, and Gene wins Dixie as his bride.

==Cast==
* Gene Autry as Texas Ranger Gene Autry
* Smiley Burnette as Frog Millhouse
* Kay Hughes as Dixie Summerall
* Monte Blue as Duval, aka Chief Tavibo
* George J. Lewis as Lieutenant Bob Cameron
* Max Terhune as Rufe Jones
* Robert Homans as Colonel Summerall
* Lloyd Whitlock as Major Crosby
* Chief Thundercloud as Little Wolf The Tennessee Ramblers as Ranger Musicians 
* Jack Cheatham as Jailer (uncredited)
* Iron Eyes Cody as Comanche War Party Leader (uncredited)
* Nelson McDowell as Proctor (uncredited)
* Greg Whitespear as Crazy Crow (uncredited)
* Champion as Champ, Genes Horse (uncredited)   

==Production==
===Stuntwork===
* Joe Yrigoyen 

===Soundtrack=== Tim Spencer) by Gene Autry and The Tennessee Ramblers
* "On the Sunset Trail" (Sam H. Stept, Sidney D. Mitchell) by Gene Autry and The Tennessee Ramblers
* "La Cucaracha" played in the canteen
* "Song of the Pioneers" (Tim Spencer) by riders with the wagon train   

==Memorable quotes==
* Rufe Jones: Lieutenant, when you see Indians be careful—and when you dont see Indians, be more careful.
* Texas Ranger Gene Autry: I think youre walking into a trap. 
:Lt. Bob Cameron: Thats my responsibility, Autry. 
:Rufe Jones: Sure, but were with you! 
* Texas Ranger Gene Autry: Goodbye, Colonel, and remember this—make your peace signals with one hand, and keep your rifles ready in the other.
* Texas Ranger Gene Autry: Just saddle our horses and trot em by in front of the guard house. 
:Frog Millhouse: Horses? 
:Rufe Jones: Yeah, horses. You know, the things you fall off of. 

==References==
;Notes
 
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 