Into the Mirror
 
{{Infobox film name           = Into the Mirror image          = Into the Mirror movie poster.jpg caption        = Into the Mirror poster director       = Kim Sung-ho producer       = Kim Eun-young writer         = Kim Sung-ho starring       = Yoo Ji-tae Kim Hye-na Kim Myung-min music          = Mun Dae-hyeon cinematography = Jeong Han-cheol editing        = Kim Sun-min distributor    = Cinema Service released       =   runtime        = 113 minutes language       = Korean
}}
Into the Mirror ( ) is a 2003 South Korean horror film about a series of grisly deaths in a department store, all involving mirrors, and the troubled detective who investigates them. It was the debut film of director Kim Sung-ho.

==Plot==
After accidentally causing the death of his partner during a hostage situation, Wu Young-min quits the police force to work for his uncle as head security of Dreampia, an immense shopping center. Dreampia is currently in the rebuilding stages as a fire destroyed parts of it five years ago. The re-opening was scheduled in a few days, until some strange murders begin to occur in the building. It seems that the victims, all employees of the mall, have committed suicide in very gruesome and unconventional ways.

Young-min is very suspicious about the police explanation, and starts his own investigation, but unfortunately for him, an old acquaintance, Ha Hyun-su, is in charge of the police investigation. Hyun-su still blames Young-min for the death of their friend and is not interested in cooperation. The more clues they stumble on, the more strange and unnatural the truth becomes.

==Cast==
* Yoo Ji-tae as Wu Young-min
* Kim Hye-na as Lee Ji-hyun
* Kim Myung-min as Ha Hyun-su

==Remake==
An American remake was produced in 2008, titled Mirrors (film)|Mirrors. The story was changed for the most part, although the basic idea and several scenes remained intact.

Into the Mirror was released on the flip side of the unrated version of the Mirrors 2 DVD.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 
 