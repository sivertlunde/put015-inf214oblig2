The Broken Spur
{{Infobox film
| name           = The Broken Spur (1992)
| image          = Broken_Spur_Lobby_Card.jpg
| alt            =
| caption        = Lobby Card used in promotion for Sandpiper Productions The Broken Spur (1992), directed by Warren Chaney and starring Gabriel Folse, Jo Thompson-Schreiner and  Jayson Sutherland.
| director       = Warren Chaney Darlene Staffa (Assistant Director)
| producer       = Warren Chaney Beverly Wilson (Executive) David Sanders (Associate Producer)
| screenplay     = Warren Chaney
| starring       = Gabriel Folse Jo Thompson-Schreiner Kathy Lampkin Rusty Cox Jayson Sutherland
| music          = Ted Mason
| cinematography = Craig Bailey
| editing        = Herbert Williams
| studio         = Sandpiper Productions
| distributor    = Intercontinental Releasing Corporation Metro-Goldwyn-Meyer
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $1.8 million
| gross          = 
}}

The Broken Spur (1992) is a Sandpiper Productions western film released by the Intercontinental Releasing Corporation in 1992 and re-released by Metro-Goldwyn-Mayer in 1998.  The production, shot as a 3-D film was written and directed by Warren Chaney. Beverly Wilson was the Executive Producer and Warren Chaney, the Producer.   It starred Gabriel Folse, Kathy Lampkin, Rusty Cox, Jo Thompson- Schreiner and Jayson Sutherland.     It was one of the few westerns filmed in 3-D film|3-D utilizing a new filming technology for a later release to television. 

The movie tells the story of a working cowboy, who as a child witnessed his familys massacre by Indians. He is suddenly thrown together with a young Indian boy whose only remaining family, his grandfather, was brutally killed by white men. Together, the two must overcome their prejudice and hatred in order to survive and reach a common goal.  

==Plot==
 ]] Indian boy, Little Hawk (portrayed by Jayson Sutherland).    

As a child, Steele witnesses the vicious death of his family at the hands of an Indian raiding party. He is nearly killed but is rescued by his sister who sacrifices her life in the process. Growing to manhood, Steel becomes foreman of a large cattle ranch owned by a kindly but tough woman named Berdie (portrayed by Kathy Lampkin).
 longhorn cattle to the stockyards in the town of Eagle Mesa. As he leaves the cattle yard with his friend Rawhide Johnson (Rusty Cox), he encounters his girl, Beth Hendricks (Jo Thompson-Schreiner). Rawhide leaves the two to talk, saying that he is going to the general store in town for supplies.

 .]]
 .]]
Sandy meets Rawhide later in the town as a fight abruptly breaks out at the other end of the street. Three snarly looking men have assaulted an old Indian in town with his grandson, Little Hawk. Sandy sees the altercation but does not want to get involved. However, when he sees Slade (played by Charles Gunning) knock the boy down, it is too much for him. Sandy quick-mounts his horse, gallops down the street and leaps onto the three men. A fight ensues and when Steel is winning, the other two Jenkins (David Sanders) and Barton (David Wayne), grab their guns and attempt to shoot the cowboy. Rawhide arrives on the scene and stops them as Steele finishes the fight leaving Slade unconscious on the street.

The three bad men have been humiliated and slink away. Sandy and Rawhide now joined by Beth, help the grandfather and young child load their wagon. As the two Indians and their wagon pass down the street, Slade, Jenkins and Barton watch from a hidden recess by one of the buildings. Jenkins aims his pistol at the passing Indians. Slade stops him, cautioning his cohorts to wait until their targets are out of town.

As the sun passes behind a cloud, the three gunfighters ride atop a cliff and wait. Soon, they see Little Hawk and his grandfather riding through the canyon below them. Slade smiles, removes his rifle, takes aim and fires. The grandfather slumps forward and the two horses frightened by the sound break into a runaway gallop.

Sandy and Rawhide ride along a ridge some distance from the gunfighters. Rawhide hears the rifle shot as Steele spots the runaway team. He spurs his horse down the canyon’s side and gallops toward the speeding buckboard. He leaps from his horse onto wagon bed and makes his way to the reins. Atop the cliff, Slade fires his rifle once again. A bullet strikes the wagon strut causing Sandy to fall forward under the wagon. He quickly grabs the undercarriage, swings himself back up, and stops the runaways. However, it is too late as the grandfather is dying.

Before passing, the old Indian takes the cowboy’s hand along with Little Hawk’s and places one in the other. His request is that Sandy Steel care for the young boy after the grandfather dies.

Steele meets the hand of fate when he becomes responsible for Little Hawk, a young Indian boy whose only family, his grandfather, has just been killed by white men. The cowboy realizes that the child has no one to care for him yet the memories of his own family’s death are too close. He gets into a heated discussion with Berdie and Rawhide.
 .]]

Berdie takes a hard line with Sandy, accusing him of holding on to the pain and being insensitive. Sandy knows that she is right but has a difficult time letting go of his feelings. Unbeknownst to Sandy, Little Hawk overhears the conversation where Steele expresses his dislike for Indians. Little Hawk already hurt by the passing of his grandfather is hurt even more, thinking he is unwanted. He decides to leave the ranch and wakes early the next morning to do so. He finds and old gun and takes it, his Indian pony and runs away. His mission now, is to track, locate and kill the three men that murdered his grandfather.
 .]]

Later than morning Beth Hendricks arrives at the ranch to see whether she can help. She discovers that Little Hawk is missing and alerts Sandy and his friend, Rawhide. Together, the two set out to find the young boy before he can get hurt.

Sandy and Rawhide manage to find Little Hawk who has already found the gunfighters’ trail and is tracking them.  Little Hawk refuses to leave until the killers are found and justice is served.  Sandy and Rawhide decide to go forward and try to capture the bad men. They corner Jenkins but not before, he wounds Rawhide. A gunfight ensues at the top of a high cliff where Jenkins accidentally falls to his death. Sandy and Little Hawk bandage Rawhide and proceed on forward.

The Indian and Cowboy eventually track the remaining killers. Barton is captured as Slade tries to escape on horseback. Steele follows in pursuit, catches up with him and knocks him from his horse. The two tumble down a tall embankment and furious fight ensues. In the end, Sandy defeats Slade and the two bad guys are taken back to Eagle Mesa where they are tried and hung.

In the film’s final scene, Sandy and Rawhide are alone as the sun sets in the western skies. Sandy tells the story of his childhood and apologizes for the things he said that Little Hawk overhears. The little Indian nods in understanding. As Sandy extends his hand, Little Hawk takes it before erupting into a tearful hug. In the end, each has overcome their own prejudice and biases to find their better selves.

==Cast==
 .]]
* Gabriel Folse as Sandy Steele
* Rusty Cox as Rawhide
* Jayson Sutherland as Little Hawk
* Kathy Lamkin as Birdie
* Charles Gunning as Slade
* David Sanders as Jenkins
* David Wayne as Barton
* Jo Thompson-Schreiner as Beth Hendricks
* Jason Chaney as Sandy Steel as a Boy
* Zeke Mills as White Bear
* Raymond Czichos as Wally Bryan
* John Hervey as Marshal
* Lesley Chaney as Emily
* Beverly Wilson-Wilson as Pearl
* Roy Alan Wilson as Saloon Singer
* Keith Brun as Bartender
* Phil Nichols as Saloon Derelict Brawler
* Tony Hodges as John Avery
* Richard Ball as Red Eagle
* Melissa L. Nichols as Pony Express Rider
* Warren Chaney as Bannon
* Kris Eberspacher as Sandys Mother
* R.A. Hilder as Sandys Father

==Reviews==
Reviews for The Broken Spur were favorable at the time of release and afterwards.   Anne Clarke writing for the online Movies website wrote of actress, Kathy Lampkin (portrayed character of Berdie), "I just saw The Broken Spur (1992) that she (Lampkin) starred in. She was outstanding and this was one of the best little western movies I ever saw. I am still laughing at her characterization of the ranch owner." Ralph Grant in a New York Times review wrote, "Warren Chaney as the writer/director manages a superbly directed production that stirs adventure in the heart and an emotional chord within the soul. His film work makes an indelible imprint upon the viewer that is almost haunting in nature."   A writer at the Internet Movie Database said, "Gabe Folse is the lead and Jayson Sutherland portrays the young Indian. Together they will make you laugh and break your heart.". Also writing for IMDb, Lynn Gilkey states, "Chaney (the director) has a masterful touch for the western. It is a shame that he never did more of them after this release."  

==Production==
 , western town sets and sprawling vistas were part of The Broken Spur (1992)]]
The Broken Spur (1992) was filmed on location in west Texas at the legendary YO Ranch.     The ranch has been under one family’s ownership since its founding in 1880.  
In 1852, at the age of 14, the ranch’s founder Charles Schreiner, became the head of the family. At 18, he staked his first claim in the rugged country that he had grown to love. He later became one of the founders of the Texas Rangers and in the Civil War’s aftermath, Captain Schreiner, using rangy Texas Longhorns as a foundation, began amassing an empire that included banks, retail stores and 566,000 acres of ranchland.    It is in this setting that The Broken Spur (1992) was filmed.  

The ranch has one of the largest herds of longhorn cattle in the world.  During filming of the motion picture, over 500 head of cattle were used in the production. All of the weapons, wagons, stagecoaches and western gear were authentic to the period (1880s). 

The town of Eagle-Mesa was a western set built near Wimberley, Texas.   Hundreds of extras showed up in authentic western wear during the week of production there. Unlike many single street western sets of today, the Eagle-Mesa/Wimberley set had 6 cross streets as well as working stages. 

The western interiors were filmed at the YO Ranch House and at the nearby Dominion Ranch western facilities.  The remaining interiors and exteriors were filmed at the Sandpiper Productions studios near Houston, Texas. Aside from the livestock furnished by the YO Ranch, all other livestock came from the ranch of Executive Producer, Beverly Wilson.

==Distribution==

The Broken Spur (1992) was distributed by Intercontinental Releasing Corporation (IRC) in 1992 and later picked up for continuing distribution by Metro-Goldwyn-Meyer in 1998. It was released to home video on laser disc in 1998.   

==References==
 

==External links==
*  
*  
*  
*   List of Western Films of the 1990s
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 