Sternenberg (film)
{{Infobox film
| name           = Sternenberg 
| image          = 
| caption        = 
| director       = Christoph Schaub
| producer       = Bernard Lang
| writer         = Sabine Pochhammer Micha Lewinsky  
| starring       = Mathias Gnädinger Walo Lüönd Stephanie Glaser
| music          = Balz Bachmann Peter Bräker
| cinematography = Peter Indergand
| editing        = Marina Wernli
| distributor    = The Walt Disney Company (Schweiz) GmbH   
| released       =  
| runtime        = 90 minutes   
| country        = Switzerland
| language       = Swiss German
| budget         = 
}}
Sternenberg is a 2004 Swiss German language film. It was filmed and produced at locations in the canton of Zürich respectively in Switzerland, and is starring Mathias Gnädinger, Walo Lüönd and Stephanie Glaser.

== Cast ==
* Sara Capretti as Eva
* Mathias Gnädinger as Franz Erni
* Walo Lüönd as 
* Stephen Sikder as Babu Sivaganeshan 
* Daniel Rohr as 
* Hanspeter Müller as 
* Stephanie Glaser as 
* Ettore Cella as   

== Plot (excerpt) ==
Franz (Mathias Gnädinger) is shocked when he returns after 30 years to his hometown Sternenberg – the primary school of Sternenberg will be closed as there is a just a handfull of students in the small isolated village in the Töss Valley, and the teacher thus would lose her job. Franz is concerned personally, hides a secret, as the teacher, Eva Joos (Sara Capretti), is his daughter. But he did not dare to tell Eva that he is her father. And, Eva repeatedly stressed that her father was an "asshole" who has never been interested in his family; she would not want to meet him, even if she could. To help Eva and to get in touch to her, Franz decides to save the school, and practices his action in an unusual way: he subscribes as a student and assumes that he had not attended as a child full time school. He claims now his "right to education". The trick seems to work, and the minimum number of students is reached, but it needs to ensure that the school will continously be funded. Franz goes along with the children to school, thus Eva reacts very dismissive at first, but they getting closer...       

== Title == municipality of Sternenberg that merged in 2015 with Bauma ZH.

== Production ==
The film was shot at Sternenberg and in the Canton of Zürich and at various locations in Switzerland. Sternenberg was produced in Switzerland.

== Reception ==
Produced as a TV-Movie for the Swiss Television SRF, after its airing the producers decided to release it theatrically in Swiss German cinemas on 22 April 2004, becoming the most successful Swiss Film of 2004. The broadcast on SRF 1 on 3 October 2004 attraced more than 816,000 viewers.

== Festivals ==
* 2004 Baden-Baden TV Film Festival.   
* 2004 Genève Festival Tous Ecrans. 
* 2005 Solothurn Film Festival. 
* 2006 Pyongyang International Film Festival.  

== Awards ==
* 2004 Genève Festival Tous Ecrans, Prix FNAC du Public. 
* 2004 Prix Walo, Beste Filmproduktion. 
* 2004 Baden-Baden TV Film Festival, nominated as 3Sat Audience Award for Christoph Schaub. 
* 2005 Swiss Film Prize, nominated as Best Film (Bester Spielfilm) for Christoph Schaub. 
* 2005 Solothurn Film Festival, nominated as Best Fiction Film.  
* 2006 Pyongyang International Film Festival, Best Actress Award.  

== References ==
 

==External links==
* 

 
 
 
 
 
 
 
 
 