Private's Progress
{{Infobox film
| name           = Privates Progress
| image          = Privates Progress - 1956 poster.jpg
| caption = Original UK cinema poster John Boulting
| based on       =   John Boulting Frank Harvey
| starring       = Ian Carmichael Richard Attenborough Dennis Price Terry-Thomas Roy Boulting
| music          = John Addison Eric Cross
| editing        = Anthony Harvey
| studio  = Charter Film Productions
| distributor    = British Lion Films  
| released       =  
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross     = £310,870 (UK) 
}} Frank Harvey.

==Plot== Peter Jones), Windrush is a most reluctant soldier and struggles through basic training at Gravestone Barracks. Failing his officer selection board, he is posted to a holding unit, under the command of Major Hitchcock (Terry-Thomas). Most of the soldiers there are malingerers and drop-outs.

Windrush is finally posted to train as a Japanese interpreter, where he becomes the prize pupil; hes then contacted by his uncle,  Brigadier Tracepurcel (Dennis Price), now a senior officer in the War Office, to join a secret operation known only as "Hatrack". He is quickly commissioned and the operation is launched, Windrush becoming an unwitting participant in a scheme ostensibly to recover looted artworks from the Germans, but really to steal them and sell them to two crooked art dealers.

Windrush survives the operation, despite being briefly arrested by British forces whilst in German uniform, and is discharged from the army. Tracepurcel and his associate Private Cox (Richard Attenborough) fake their own deaths. Windrush returns to university after the war, and is surprised to receive a visit from Cox, who brings him an attache case. However, Cox is arrested as he leaves, he and Tracepurcel having been tracked as source of a counterfeit copy of one of the artworks. Windrush innocently reveals to the military police the contents of the case—a large sum of money—and is also arrested, assumed to be complicit in the fraud.

==Cast==
 
* Ian Carmichael as Stanley Windrush
* Richard Attenborough as Private Cox
* Dennis Price as Brigadier Bertram Tracepurcel
* Terry-Thomas as Major Hitchcock Peter Jones as Egan
* William Hartnell as Sergeant Sutton
* Thorley Walters as Captain Bootle
* Jill Adams as Prudence Greenslade
* Ian Bannen as Private Horrocks
* Victor Maddern as Private Blake
* Kenneth Griffith as Private Jones
* George Coulouris as Padre
* Derrick De Marney as Pat Ronald Adam as Doctor at medical hearing
* Miles Malleson as Windrush Sr.
* Sally Miles as Catherine
* David King-Wood as Gerald
* Brian Oulton as M.O. at Gravestone Camp
* Michael Trubshawe as Col. Fanshawe
* John Le Mesurier as Psychiatrist
* Robert Raglan as Gen. Tomlinson
* Henry Oscar as Art expert
* Christopher Lee as General von Linbecks aide
* Basil Dignam as Col. Martin (president of Selection Board) John Harvey as RAF officer at headquarters
* Glyn Houston as Corporal on sick call
* Lloyd Lamble as Officer at medical hearing David Lodge as Lance Corporal on guard duty, Holding Unit
* Marianne Stone as Expectant mother talking to Capt Bootle Michael Ward as Sidney (guest at party)
* John Warren as Sergeant Major Gradwick
* Trevor Reid as Adjutant
* Theodore Zichy as German Agent
 

==Production==
The film was primarily filmed at Shepperton Studios, however a number of scenes were filmed at Wantage Hall, a hall of residence for the University of Reading.

It was the first in a series of successful satirical comedies made by the Boulting brothers for their production company Charter Films. Their 1959 comedy Im All Right Jack featured many of the same actors and characters. A number of references are made to the events of Privates Progress.

==Reception==
The film was the second most popular movie at the British box office in 1956. BRITISH. FILMS MADE MOST MONEY: BOX-OFFICE SURVEY
The Manchester Guardian (1901-1959)   28 Dec 1956: 3 
==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 