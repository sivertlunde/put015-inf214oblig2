Suicide Kings
{{Infobox film
| name           = Suicide Kings
| image          = Suicide kings poster.jpg
| caption        = Theatrical release poster
| director       = Peter OFallon
| producer       = Morrie Eisenman Wayne Rice
| screenplay     = Josh McKinney Gina Goldman Wayne Rice	 	
| based on       =  
| starring       = Christopher Walken Denis Leary Henry Thomas Johnny Galecki Sean Patrick Flanery Jay Mohr
| music          = Graeme Revell
| cinematography = Christopher Baffa
| editing        = Chris Peppe
| studio         = Live Entertainment Dinamo Entertainment
| distributor    = Artisan Entertainment (DVD)
| released       = September 6, 1997 (premiere) April 17, 1998 (USA)
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $5 million
| gross          = $1,740,156 (domestic) 
}}
 high society twenty-somethings who kidnap Walken. It was based on Don Stanfords short story, The Hostage, and directed by Peter OFallon. The film was panned by critics and was a box office bomb.

==Plot==
The movie opens with Charlie Barret (Christopher Walken|Walken) walking to his private table in a restaurant, only to see two young men sitting at his table - Avery (Henry Thomas) and Max (Sean Patrick Flanery). Another young man who is friends with Avery and Max, Brett (Jay Mohr), joins them shortly after Charlie sits down and begins chatting with them. Charlie happens to know Averys father, and after an initial reluctance, is willing to go with the boys for a night on the town.

As the scene progresses, it shifts back and forth to the planning of what theyre going to do to Charlie - they plan to kidnap him by strapping him to his seat and use some chloroform to knock him out. Naturally, things dont go as smoothly as they wanted, and Charlie tries fighting back - but eventually succumbs and passes out.

When Charlie wakes up, he sees himself surrounded by the three men, and a fourth friend, T.K. (Jeremy Sisto), dressed in a doctors uniform, checking his vital signs. Its soon revealed that Charlie used to be Carlo Bartolucci, a mob figure. The boys then explain that Averys sister, Elise, has been kidnapped, and that the kidnappers (Frank Medrano and Brad Garrett) are demanding $2 million ransom for her release. They figure that Charlie still has connections to get that kind of money to the gangsters, and they want his cooperation. To ensure that Charlie knows how serious they are, Charlie is shown his pinkie finger, complete with a signet ring, cut off and floating in a bowl of ice - since the same was allegedly done to Averys sister. Part of the incentive that Charlie help them immediately is that there is a small window of time available where a hospital could successfully sew the finger back on and the clock was running out.

Charlie flies into a rage, stating "Im looking at dead men." However, he eventually agrees to help them. Part of the reason Charlie decides to help is that it becomes apparent towards the middle of the film that due to lifelong heavy drinking, Charlie has a major deficiency of Vitamin K, a major component needed for the body to form blood clots: Basically meaning that while a normal healthy person would have minimal bleeding after having his pinkie amputated (Especially considering that T.K. has medical training and bandaged up the wound correctly), Charlie, on the other hand, would continue to bleed to death without proper medical treatment. Charlie then contacts his lawyer, who in turn contacts Lono (Denis Leary), Charlies bodyguard, asking him to track Charlie down. Lono goes about his own investigation, asking for (and in some cases beating out) information from people, including the hostess, Jennifer (Nina Siemaszko) who usually waits on Charlie, and a friend of Charlies, Lydia (Laura San Giacomo). During the course of these conversations, an added backstory is shown about both Lono and Charlie, including how Charlie got his signet ring.

Charlie, meanwhile, tries to take the boys naïvete to his advantage. A fifth friend, Ira (Galecki) shows up &mdash; they are in his fathers house, and Ira didnt know anything about what they had planned. Charlie starts playing the boys off of each other, slowly getting information out of them, including how they got into this mess in the first place. After much cajoling and piecing information together, Charlie learns that Avery was actually the one responsible for his sisters kidnapping. The boys had, the previous summer, spent a wild weekend in Atlantic City, where they dropped tens of thousands of dollars on various bets. Avery had gotten in over his head wagering on a basketball game, and had to come up with a way to pay off his $50,000 debt. The two mobsters approached him and told him they would kidnap his sister and take the "ransom" as payment for the debt.

Lono eventually makes his way to Iras house and has Charlie removed from his restraints, around the same time that the money is sent to the two thugs. The next day, Charlie and Lono meet up with the two gangsters who had kidnapped Averys sister, around the same time that Avery learns that his sister is not at the hospital where she is supposed to have been. Charlie and Lono figure out that Elise had come up with a plan to fake the kidnapping and ask for a $2 million ransom; the thugs would get $1 million for playing along, while Max and Elise would walk away with the rest. Charlie and Lono track Max and Elise to a boat off a tropical island where, although Charlie understands their reasons for conning him, he has Lono presumably kill them both. We see the screen tint orange rotoscope as it goes out of focus, and hear two shots (presumably meaning their deaths). They have reclaimed the rest of their money.

===Alternate endings===
The film also features two alternate endings. In one of them, Charlie allows Max and Elise to live, but reclaims the $1 million, giving them a small amount of the money back. In the other ending, Charlie allows them to live, but takes his money, after which Lono shoots holes in the boat, causing it to slowly sink. However, test audiences didnt like these endings as much, feeling that Max and Elise needed to pay for the betrayal of their friends and grief they had caused.

==Production==
Josh McKenny, based his screen play on the short story written by Don Stanford in 1996 titled “Hostage” as indicated in the movies closing credits. The story and the movie have many similarities; however the new title and some of the adapted screenplay draw clear reference to similar events that actually took place in affluent Westchester County in the early 1980s when a group of Twentysomething (term)|twenty-somethings home from various Ivy League schools over several summers ran a lucrative gambling and money laundering enterprise that grew to become the largest in the Eastern United States. The group with ties to affluent Scarsdale, Bedford, Rye, Bronxville, and Fairfield  referred to themselves in documents, evidence and later by both the local and national press as vita privare regisio, Latin for "Suicide Kings". The movie makes strong reference to individuals associated with that group and similar events that actually took place including the kidnapping of a high-ranking member of organized crime.

==Cast==
{| class="wikitable"
|-
! Actor/Actress
! Role
|-
| Christopher Walken || Carlo Bartolucci/Charlie Barret
|-
| Denis Leary || Lono Veccio
|-
| Henry Thomas || Avery Chasten
|-
| Sean Patrick Flanery || Max Minot
|-
| Jay Mohr || Brett Campbell
|-
| Jeremy Sisto|| T.K.
|-
| Frank Medrano || Heckle
|-
| Brad Garrett || Jeckyll
|-
| Johnny Galecki || Ira Reder
|-
| Laura San Giacomo || Lydia
|-
| Laura Harris || Elise Chasten
|-
| Nina Siemaszko || Jennifer
|-
| Cliff DeYoung || Marty
|}

== References ==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 