Game Over (2012 film)
{{Infobox film
| name = Game Over
| image = Official Poster of Game Over (animated short film).jpg
| caption = Official Poster
| director = Seyed Mohsen Pourmohseni Shakib
| producer = Seyed Mansour Pourmohseni Shakib
Seyed Ebrahim Pourmohseni Shakib
| writer = Seyed Mohsen Pourmohseni Shakib
| runtime = 4 minutes
| country = Iran
| language = English
}}
Game Over (Persian: Bazi Tamam Shod, بازی تمام شد) is 2012 Iranian animated short film directed by Seyed Mohsen Pourmohseni Shakib. It is directors second short animation. Game Over currently has distribution with IndieFlix online video streaming service.

== Plot ==
In a war videogame, where even the sun is evil, four missiles fired from a military airplane decide to destroy the world. But fortunately the children who live inside that game find a way to stop them.

== Festivals & TV==

Festivals

2013
* National Student Film Festival - Iran  

* Watersprite: Cambridge International Student Film Festival - UK / Nominated For " Soundtrack Award "  

* International Motion Festival - Cyprus  

* Chilemonos International Animation Festival - Chile  

* International Student Film Festival - Mexico  

* Diversity In Animation Festival - Brasil  

* Open International Festival of Multimedia Art «Multimatograf» - Russia  

* Digital Graffiti Festival - USA  

* Vagrant Film Festival - Belarus  

* Seoul International Youth Film Festival - South Korea  

* Film Festival della Lessinia - Italy  

* Electric Lantern Festival - UK  

* Ecologico International Film Festival - Italy  

* No Gloss Film Festival - UK  

* 48,40 Frames - Kurzfilmfestival - Austria  

* South Texas Underground Film Festival - USA  

* Tehran International Short Film Festival - Iran  

* Festival Internacional de Cine, Arte y Cultura - Paraguay  

* Isfahan International Festival of Films for Children & Young Adults - Iran  

* Northern Wave International Film Festival - Iceland  

* Simultan Festival - Romania  

* FreeNetWorld International Film Fest - Serbia  

* ProFiRe Short Film Festival - UK  

2014 

* Montréal International Childrens Film Festival - Canada  

* Basij Honarmandan Provincial Film Festival - Guilan, Iran  

* Image Of The Year Festival - Iran  

* Athens Animation Festival - Greek  

* Golden Kuker: Sofia International Animation Film Festival - Bulgaria  

* Gothenburg Independent Film Festival - Sweden  

* Ordibehesht National Festival - Iran  

* BornShorts Film Festival - Denmark  

* Ekotopfilm - International Festival of Sustainable Development Films - Slovakia  

* YOUKI International Youth Media Festival - Austria

* Anim!Arte - International Student Animation Festival - Brazil  

* Karama Human Rights Film Festival - Jordan  

TV

* IRIB 4 - Iran / Tehran

* Transit TV (Out The Window) - USA / Los Angeles  

* Namayesh TV - Iran / Tehran

* 0 to 100 Television  

* Okto TV (Delete) - Asturia / Vienna  

Project

*  Side Street Projects by ArtNight Pasadena(CA - USA)  

== Credits   ==
* Art Director: Meghdad Akhavan

* Producer: Seyed Ebrahim Pourmohseni Shakib - Seyed Mansour Pourmohseni Shakib

* Screenwriter: Seyed Mohsen Pourmohseni Shakib

* Designer & Animator: Meghdad Akhavan

* Graphic: Saba Dargahi

* Colourist: Seyed Mansour Pourmohseni Shakib

* Sound Design & Music: Armin Bahari

* Edit: Ario Saffarzadegan

==References==
 

== External links ==
* 
* 

 
 
 