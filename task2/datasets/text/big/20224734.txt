Jane Eyre (1934 film)
{{Infobox film
| name           = Jane Eyre
| image          = Jane Eyre (1934 film) poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Christy Cabanne
| producer       = Ben Verschleiser
| writer         = Adele Comandini
| based on       =  
| starring       = Virginia Bruce Colin Clive
| music          =
| cinematography = Robert H. Planck  (as Robert Planck)
| editing        = Carl Pierson
| studio        = Monogram Pictures
| distributor    =
| released       =  
| runtime        = 62 min
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 romantic drama film directed by Christy Cabanne, starring Virginia Bruce and Colin Clive. It is based on the 1847 novel Jane Eyre by Charlotte Brontë, and is the first adaptation to use sound films|sound.

== Synopsis ==
  Victorian orphan secures a position as governess at Thornfield Hall. She falls in love with her employer.

== Cast == Jane Eyre
*Colin Clive as Edward Rochester
*Beryl Mercer as Mrs. Fairfax
*David Torrence as Mr. Brocklehurst
*Aileen Pringle as Lady Blanche Ingram
*Edith Fellows as Adele Rochester
* John Rogers as Sam Poole
*Jean Darling as Jane Eyre as a Child
*Lionel Belmore as Lord Ingram
*Jameson Thomas as Charles Craig
*Ethel Griffies as Grace Poole Bertha Rochester
*William Burress as Minister
*Joan Standing as Daisy
*Richard Quine as John Reed
*Gretta Gould as Miss Temple (uncredited) Anne Howard as Georgianna Reed (uncredited)
*Olaf Hytten as Jeweler (uncredited)
*Gail Kaye as Mary Lane (uncredited)
*Edith Kingston as Lady Ingram (uncredited)
*Desmond Roberts as Dr. John Rivers (uncredited)
*Clarissa Selwynne as Mrs. Reed (uncredited)
*Hylda Tyson as Bessie (uncredited) William Wagner as Halliburton (uncredited)

==Production==
Production began 17 May 1934 at General Service Studios.

== Soundtrack ==
*Adele sings the "Bridal Chorus" from the opera Lohengrin (opera)|Lohengrin, by Richard Wagner.
*Adele sings "My Bonnie Lies over the Ocean".

===Critical reception===
Critic Leonard Maltin gave the film 2 stars (out of four), describing it as a " hin version of the oft-filmed Bronte novel, produced by Monogram Pictures|Monogram, of all studios  Still, its not uninteresting as a curio." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 