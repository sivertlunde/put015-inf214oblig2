This Is the Night (film)
{{Infobox film
| name           = This Is the Night
| image          = This is the Night poster.jpg
| image_size     =
| caption        = Theatrical poster with Lilli Damita and Cary Grant
| director       = Frank Tuttle
| producer       = Benjamin Glazer (uncredited)
| writer         = Benjamin Glazer George Marion Jr. play Naughty Cinderella Avery Hopwood play Pouche Henry Falk René Peter
| narrator       =
| starring       = Lili Damita Charles Ruggles Roland Young Thelma Todd Cary Grant
| music          = Ralph Rainger W. Franke Harling (uncredited) John Leipold (uncredited)
| cinematography = Victor Milner
| editing        =
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 min
| country        = United States
| language       = English
| budget         =
| gross          =
}}

This Is the Night is a 1932 comedy film made by Paramount Pictures, directed by Frank Tuttle, and starring Lili Damita, Charles Ruggles, Roland Young, Thelma Todd, and Cary Grant.
 Good and Naughty (1926) with Pola Negri. 
 blue tint. Tinting is used on the restored 2011 DVD version released by Turner Classic Movies.  However, tinting was absent from recent prints prior to restoration.  The version shown on the TCM cable channel in the 1990s was not tinted.  

This is the Night is Cary Grants feature film debut. He disliked his role, believing that a man accepting the unfaithfulness of his wife so calmly was unbelievable. After seeing the film, he decided to quit the movie industry; his friend Orry-Kelly talked him out of it. 

==Plot==
When Claire Mathewsons (Thelma Todd) husband Stephen (Cary Grant) comes back unexpectedly from the 1932 Summer Olympics, where he was supposed to compete in the javelin throw, he discovers the train tickets for a romantic Venice getaway she has planned with her lover Gerald (Roland Young). Geralds friend Bunny (Charles Ruggles) lies and says that the tickets are actually for Gerald and his wife. With Stephen still suspicious, Gerald must find a fake wife to go to Venice with him. He tries to hire the actress Chou-Chou (Claire Dodd), but since her boyfriend is a jealous man, she gives the job to out-of-work Germaine (Lili Damita), who needs the 2000 franc fee to keep from starving. At first, Gerald thinks she is too demure, but she soon convinces him that she can pretend to be a glamorous wife.

The two couples go to Venice. Bunny, attracted to Germaine, decides to join them. On the train, Stephen questions Gerald and Germaine about how they met. When they arrive in Venice, Claire quickly becomes jealous, as both Stephen and Gerald seem fascinated by Germaine. Claire eventually demands that Gerald send Germaine away immediately, so he orders her to leave the next day. Meanwhile, a drunken Bunny climbs a ladder into Germaines bedroom and offers to take her away. After she turns down his offer, he falls into a canal on his way out and is apprehended by two policemen. Stephen believes he hears a burglar and goes to her room to investigate. The two are then caught in a seemingly compromising position by Gerald and Claire. However, Bunny reappears and explains what really happened. Her love for her husband rekindled, Claire breaks off her affair with Gerald. Germaine reveals to Gerald that she is not in fact Chou-Chou and decides to return to Paris, but Gerald catches up to her in a gondola and asks her to marry him.

==Cast==
* Lili Damita as Germaine
* Charles Ruggles as Bunny West
* Roland Young as Gerald Grey
* Thelma Todd as Claire
* Cary Grant as Stephen
* Irving Bacon as Sparks, Geralds chauffeur. In a running gag, he accidentally snags Claires dress repeatedly, for example in a limousine door or luggage, causing her to lose her clothing. 
* Davison Clark as Studio Official (uncredited)
* Gino Corrado as Manager of Neapolitan Hotel (uncredited)
* Claire Dodd as Chou-Chou (uncredited)
* Alex Melesh as Porter (uncredited)
* Donald Novis as Singing Gondolier (uncredited)
* Tiny Sandford as Porter (uncredited)
* Rolfe Sedan as Boulevardier (uncredited)
* Harry Semels as Man in the Manhole (uncredited)

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 