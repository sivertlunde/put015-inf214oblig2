Sunday Punch (film)
{{Infobox film
| name           = Sunday Punch
| image          = 
| image_size     = 
| caption        =  David Miller
| writer         = Fay Kanin Michael Kanin Allen Rivkin
| narrator       = 
| starring       = William Lundigan David Snell
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       = May 8, 1942
| runtime        = 76 min.
| country        = United States
| language       = English
| budget         = $305,000  . 
| gross          = $373,000 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} David Miller, and starring William Lundigan and Jean Rogers. 

==Plot==
Boxers managed by Bassler and trained by Roscoe live in an all-male Brooklyn boardinghouse, where the arrival of the landladys niece Judy gets their attention.

Judy gets to know Ken Burke, who quit medical school to try boxing, and Ole Jensen, the young janitor. Bassler is concerned that Kens interest in Judy is distracting him, so he tries to find her a job as a singer.

Ole packs a "Sunday punch" that knocks out a pro. He decides to be a prizefighter to earn money to impress Judy, but no one except "Pops" Muller will agree to train him. Ken and Ole rise in the ranks, but reject a $30,000 offer to fight each other due to their friendship.

Their managers conspire to set up the bout. Judy roots for Ole to win, but only because that way Ken might give up boxing and become a doctor. Their story climaxes with the big fight.

==Cast==
*William Lundigan as Ken Burke
*Jean Rogers as Judy Galestrum
*Dan Dailey as Olaf Jensen
*Guy Kibbee as Pops Muller
*J. Carrol Naish as Matt Bassler
*Connie Gilchrist as Ma Galestrum
==Reception==
The film earned $229,000 in the US and Canada and $144,000 elsewhere during its initial theatrical run, making MGM a loss of $79,000. 
==References==
 

==External links==
* 

 

 
 
 