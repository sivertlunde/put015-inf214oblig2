The Younger Brothers
{{Infobox film
| name           = The Younger Brothers
| image          = 
| image_size     = 
| caption        = 
| producer       = Saul Elkins
| director       = Edwin L. Marin
| writer         = 
| narrator       =  Wayne Morris Janis Paige
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Warner Brothers
| released       = May 3, 1949
| runtime        = 78 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Western film Wayne Morris and Janis Paige. 

==Plot==
Determined to reform from their outlaw ways, Cole, Jim and Bob Younger ride to Cedar Creek, Minnesota, where a parole hearing will be held. If they steer clear of trouble, the Youngers will be free to return home to Missouri and their farm.

A detective who blames the Youngers for losing his Pinkertons job, Ryckman, is eager to get even. He goads a younger Younger brother, Johnny, into a situation at a saloon where a man is killed. Ryckman urges townspeople to turn Sheriff Knudson against all the Youngers.

Katie Shepherd, who has a lawless band of her own, fails to persuade the Youngers to side with her, so she sets a trap. Cole, taken hostage, is forced to join Katies gang on a bank robbery or else Johnny will be harmed. Jim and Bob see their brother armed and riding with the outlaws, not knowing Coles been given an unloaded gun.

The robbery goes wrong and Katie is killed. Ryckman continues to come after the Youngers, surrounding their campsite with the intent to lynch them. In the end, though, the Youngers are cleared of wrongdoing and able to ride away free and clear.

==Cast== Wayne Morris as Cole Younger
*Janis Paige as Katie Shepherd  
*Bruce Bennett as Jim Younger  Geraldine Brooks as Mary Hathaway
*Robert Hutton as Johnny Younger Alan Hale as Knudson
*Fred Clark as Ryckman
*Jimmy Noel as River Rock Townsman (his first acting role)

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 