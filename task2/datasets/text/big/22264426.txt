Mask of the Avenger
{{Infobox film
  | name           = Mask of the Avenger
  | caption        =
  | image	=	Mask of the Avenger FilmPoster.jpeg
| director       = Phil Karlson
  | producer       = Hunt Stromberg
  | writer         = Ralph Gilbert Bettison George Bruce
  | narrator       =
  | starring       = John Derek Anthony Quinn Jody Lawrance
  | music          = Mario Castelnuovo-Tedesco
  | cinematography = Charles Lawton Jr.
  | editing        = Jerome Thoms
  | distributor    = Columbia Pictures Corporation
  | released       = June 27, 1951
  | runtime        = 83 min
  | country        = USA
  | language       = English
  | budget         =
  | gross          =
  | preceded_by    =
  | followed_by    =
    }}

Mask of the Avenger  is a 1951 adventure film starring John Derek, Anthony Quinn and Jody Lawrance.

Derek portrays Renatu Dimorna, the son of an Italian aristocrat, who vows revenge after his father is murdered during the European political upheaval of 1848.

==Plot==
 

==Cast==

*John Derek as Capt. Renato Dimorna
*Anthony Quinn as Viovanni Larocca
*Jody Lawrance as Maria dOrsini
*Arnold Moss as Colardi
*Eugene Iglesias as Rollo DAnterras
*Dickie Leroy as Jacopo (as Dickie LeRoy)
*Harry Cording as Zio
*Ian Wolfe as Signor Donner

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 

 