Havoc (1972 film)
 
{{Infobox film
| name           = Havoc
| image          = 
| image_size     = 
| caption        = 
| director       = Peter Fleischmann
| producer       = Peter Fleischmann
| writer         = Peter Fleischmann Martin Walser
| narrator       = 
| starring       = Vitus Zeplichal
| music          = 
| cinematography = Dib Lutfi
| editing        = Odile Faillot
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

Havoc ( ) is a 1972 West German thriller film directed by Peter Fleischmann. It was entered into the 1972 Cannes Film Festival.   

==Cast==
* Vitus Zeplichal – Hille
* Reinhard Kolldehoff – Pfarrer
* Silke Kulik – Dimuth
* Helga Riedel-Hassenstein – Mutter
* Ingmar Zeisberg – Sibylle
* Werner Hess – Dr. Raucheisen
* Christoph Geraths – Drogist
* Ulrich Greiwe – Student
* Frédérique Jeantet – Roswitha
* Gabi Will – Gabi
* Eugen Pletsch – Schüler
* Kurt Wörtz – Schüler
* Bernhard Kimmel – Deserteur
* Ludwig Beyer – Alter Waldhornspieler
* Rex Aquam Aquarillo – Rentner

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 