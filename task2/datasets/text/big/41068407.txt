Sonno Profondo
{{Infobox film
 | name = Sonno Profondo
 | image = 
 | caption =
 | director = Luciano Onetti
 | writer = Luciano Onetti
 | starring =  
 | music = Luciano Onetti  
 | cinematography =  
 | editing =  
 | producer = Nicolás Onetti
 | distributor =
 | released =  
 | runtime =
 | country = Argentina
 | language = Italian
 | budget =
 }}
Sonno Profondo is an Italian giallo film. In spite of being shot in Argentina, the film’s screenplay is written in Italian. The movie is set in the 1970s. Sonno Profondo is a feature film directed and written by Luciano Onetti and produced by Nicolás Onetti. The film had excellent critics after its World Premiere at Sitges 2013. 

The soundtrack was composed by Luciano Onetti true to the style of Ennio Morricone. Sonno Profondo won the Tabloid Witch Awards for “Best Music Soundtrack” (Hollywood Investigator, California, United States).

==Synopsis==
After murdering a woman, a killer that is traumatized from his childhood memories, gets a mysterious envelope slipped under his door. The hunter becomes the prey when he finds out that the envelope contains photos that show him killing the young woman.

==Accolades==
*Best Film, Hemoglozine 2013, Spain. 
{{cite news 
| url         = http://www.lanzadigital.com/cultura/%E2%80%98sonno_profondo%E2%80%99_se_corona_como_mejor_largo_del_hemoglozine_2013-55663.html
| title       = ‘Sonno profondo’ se corona como mejor largo del Hemoglozine 2013 
| publisher   = Lanza Digital
| author      = Belen Rodriguez
| date        = 2013-10-27
| page        = 
| location    = 
| isbn        = 
| accessdate  = 2013-11-21
| language    = Spanish 
| trans_title = Sonno profondo is crowned best Hemoglozine over 2013
| archivedate = 
| archiveurl  = 
| deadurl     = No 
| quote       = 
}}
 
*Best Music Soundtrack, Tabloid Witch Awards 2013, United States.
*Special Mention, Buenos Aires Rojo Sangre 2013, Argentina.
*SITGES 2013 International Film Festival, Spain.
*Festival Internacional de Cine de Mar del Plata 2013, Argentina.
*Móbido Fest 2013, México.
*Puerto Rico Horror Film Festival 2013, Puerto Rico.
*Horror-On-Sea 2014, United Kingdom.
*HorrorQuest 2013, United States.

==References ==
 

 
 
 


 
 