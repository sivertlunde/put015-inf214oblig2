Enemy (1990 film)
{{Infobox film
| name         = Enemy
| image        = Enemy1990.jpg
| caption      = DVD cover George Rowe
| producer     = Chosei Funahara Franz Harland Takahide Kawakami George Rowe
| story        = 
| based on     =  Mako
| music        = Mark Winner
| editing      = Branka Mrkic
| cinematography = Phil Parmet
| studio       = Antennafilms
| distributor  = Anchor Bay Entertainment
| released     = July 25, 1990 (USA)
| runtime      = 84 minutes
| country      = United States English
| budget       =
| gross        = 
}}
 George Rowe and starring Peter Fonda, who also co-wrote the screenplay.

==Plot==
A CIA agent posing as a journalist (Peter Fonda) assassinates a North Vietnamese official then escapes into the jungle, where he comes across a beautiful female spy (Tia Carrere). At first theyre enemies, but come to the realization that they must work together if they want to get out alive.

==Cast==
*Peter Fonda as Ken Andrews
*Tia Carrere as Mai Chang Mako as Trang
*James Mitchum as Maj. Bauer

==Production==
Enemy was filmed in the Philippines. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 