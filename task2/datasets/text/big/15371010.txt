Be a Wicked Woman
{{Infobox film name           = Be a Wicked Woman image          = Be a Wicked Woman.jpg caption        = South Korean Poster director       = Kim Ki-young  producer       = Kim Yu-bong writer         = Kim Ki-young starring       = Youn Yuh-jung Hyun Gil-soo music          = Han Sang-ki cinematography = Ham Nam-sub editing        = Hyeon Dong-chun distributor    = Yuseong Film released       =   runtime        = 98 minutes country        = South Korea language       = Korean
| film name      = {{Film name hangul         =       hanja          =  여  가 되라 rr             = Cheonsayeo aknyeoga doera mr             = Ch‘ŏnsayŏ aknyŏga toera}} budget         =  gross          = 
}}
Be a Wicked Woman ( ), also known as Angel, Become an Evil Woman,  is a 1990 South Korean film directed by Kim Ki-young.

==Synopsis==
Two women with unfaithful husbands make a deal to kill each others husbands. 

==Release==
Produced in 1990, Kim was so disappointed with the outcome that he chose not to release it. The film was submitted to the censors twice — once in 1990 and again in 1995, under the new title 죽어도 조은 경혐 (Jukeodo joeun gyeonghyeom) — but was not screened for the public until the 1998 Pusan International Film Festival, following Kims untimely death in a house fire. 

==Cast==
* Youn Yuh-jung 
* Hyun Gil-soo
* Kim Seong-geun

==Notes==
 

==See also==
* Throw Momma from the Train, a comedy with a similar premise.

==Bibliography==
*  
*  

 

 
 
 
 


 