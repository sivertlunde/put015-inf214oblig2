Johan (film)
{{Infobox film
| name = Johan
| image = File:Johan (Film poster).jpg
| caption = Theatrical release poster
| director = Mauritz Stiller
| producer =
| screenplay = Mauritz Stiller Arthur Nordén
| based on =  
| starring = Mathias Taube Jenny Hasselquist Urho Somersalmi Hildegard Harring
| cinematography = Henrik Jaenzon
| editing =
| studio = AB Svensk Filmindustri
| distributor = AB Svenska Biografteaterns Filmbyrå
| released =  
| runtime = 110 minutes
| country = Sweden Silent Swedish intertitles
| budget =
}}

Johan, also known as Troubled Waters or Rapids of Life is a 1921 Swedish film directed by Mauritz Stiller, based on the 1911 novel Johan (Juha) by Juhani Aho. It tells the story of a young girl, married to an older farmer, who elopes with a handsome stranger, crossing dangerous rapids on a small boat in the process.

== Synopsis ==
In a remote place of Sweden, men working on the digging of a canal to dry up a marsh are housed in the nearby farms. In one of them live Johan and his mother together with a young girl, Marit. One of the workers tries unsuccessfully to kiss her. Some time later, while Johan is out floating timber on the other side of the river, he falls and crushes his leg. He lights a fire that Marit sees from the house. She crosses the river on a rowing boat and carries him back home and takes care of him. On his sick-bed, he asks her to be his wife. She accepts it quite unresponsively. Johans mother is determined to avoid what she sees as a misalliance. He reminds her how 18 years before they had found her on the side of the road next to her dying mother. The mother orders Marit to leave the house but Johan, seeing the scene from his sick-bed orders her to stay and eventually marries her against his mothers will. 

However Marit is not happy with him. Sometimes, she remembers the handsome stranger who had kissed her and dreams of another life. She becomes more and more distant from him.

One day, the handsome stranger comes back and asks Marit for a room for the night. Marit serves dinner to him and Johan. He makes them drink heavy wine and offers her a scarf. Johan wants to pay him but the stranger laughingly throws the coins in the air, keeping only one of them and Johan crawls on the ground to pick-up the rest. The following day he tries to seduce her, pointing ouot what a lonely and uninteresting life she led. To fight the attraction she unbvoluntarily feels for him, she tries to concentrate on helping Johan cleaning his fishing nets. Suddenly they see on the other side of the river the fire meaning that Johans mother wants him to come and get her, they bioth freeze. Once Johan has left, she cant avoid confessing to the stranger that every summer her mother in law comes back to harass her. He tries again to kiss her but she runs indoor. The stranger shrugs his shoulders and leaves, passing Johans mother on his way to his boat. She begins immediately to scold Marit and, when the latter threatens to leave, answers that nobody is keeping her. Marit runs out of the house telling Joan that she is fleeing his mother. She waves to the stranger who was already in the middle of the river and he comes back to pick her up. When Johan hears someone crying outside, he thinks it is Marit but it is his maid who tells him that she saw the stranger carrying Marit to his boat and rowing towards the rapids. Johan runs along
the riverbank for hours without avail.

Marit and the stranger sail down the river on his small boat. As they reach the rapids, she is more and more scared and asks him to take her to land and to let her go. But when they reach the shore, she realises that she has no other choice than to follow him. Johan is determined to catch the man who stole away Marit from him. His mother tells him that she went freely but Johan does not believe her.

Once Marit and the stranger have reached the island where he lives in a shed, her happiness does not last very long. She does not like when he makes fun of her old husband and when she meets an old fisherman who tells her that the stranger brings a new summer-girl every year to his island, she feels regret and mourns what she has lost. She tells the old man what happened with her mother in law and how much she wants to go back to her husband. The old man finds Johan who had be travelling along the river and leads him to the island. When confronted to him the stranger says laughingly that he can have his wife back. Johan starts beating him for having stolen what was the most important for him. When the stranger answers that she has come on her own will he cannot believe it and he is flabbergasted when she confesses. She begs him to forgive her, saying that she lost her head and now knows it is him whom she loves. He tells her to get in the boat and they row homewards on the river and between them comes the stillness that happiness gives. 

==Cast==
 
* Mathias Taube as Johan
* Jenny Hasselqvist as Marit
* Karin Molander as Marthe
* Urho Somersalmi as the Stranger
* Lilly Berg as the Maid
* Nils Fredrik Widegren as the Old Fisherman

== Production == Lapland in Treaty of Tartu) Stiller chose to shoot it in the Swedish part of Lapland, near the city of Kalix. 

The most spectacular part of the film were shot on the river Kalix with its roaring Kamlunge rapids. No tricks or stuntmen were used for the filming and Stiller paid credit to the courage of Urho Somersalmi who steered the boat through the rapids and of Jenny Hasselqvist, who was the first woman to make the travel down the Kamlunge. The managers of the local log floating association advised against the attempt as the journey through the rapids was normally done with a crew of seven men. Stiller persuaded one of them to take him down to demonstrate it was possible. The run through the rapid was filmed both by cameras on the shore and by a camera on a second boat with six men.  

Stiller later told how the scene had to be shot on Friday the 13th, and how it was very close to a disaster. Somersalmi was washed overboard at one of the wildest part of the rapids and saved by Hasselqvist. Later, the boat with the camera started sinking and the film was saved by the photographer who jumped overboard and waded to the shore holding the camera high above his head. Stiller declared that he would never make another film in rapids. 

Local inhabitants were used as extras and the old man was played by a 70 years old local fisherman. 

==Release==
The film premièred in Sweden on 28 February 1921 and in Finland on 17 April 1921. {{Cite web|url=http://www.imdb.com/title/tt0012334/releaseinfo|title= Johan (1921)
Release Info|work=IMDB|accessdate=2015-04-11}}  It was distributed in 33 markets abroad. Juhani Aho was able to attend the Première in Helsinki a few months before his death. He had himself translated the intertitles and the presentation leaflet in Finish. He wrote a letter to Mauritz Stiller stating his impressions about the adaptation. He was very enthusiastic about the acting and the technical characteristics of the film but did not hide his disappointment about the happy end introduced by Stiller. 

The film was long considered lost after a fire at the Swedish Film Institute in 1941. Henri Langlois, creator of the Cinémathèque Française considered that is was one the three most important lost films. However, the largest part of one of the original negatives was found in the early 1960s. From this starting point, the film has been restored in black and white in 1974 and in a yellow and orange tinted version in 2001 which is now available on DVD with an original soundtrack by Alexander Popov.  

== Reviews ==
Reviews were mixed at the time of the Première, exuberant praise alternating with hesitant, sometimes even negative assessments. Some reviewers dismissed the film with general phrases, summaries of content, and a few kind words about the actors. Many of the reviews compared the film to Stillers earlier film  	
  which also included a scene in the rapids. Svenska Dagbladet wrote: ″This is one of the Directors best works, and in all respects a film of high rank (...) What was in The Song of the Scarlet Flower dull, artificial and incoherent, is in Johan strong, genuine and solidly built.″  

==References==
 

==External links==
*  
*  

 
 
 
 
 