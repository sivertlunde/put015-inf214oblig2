Under Secret Orders
{{Infobox film
| name           = Under Secret Orders
| image          = Under Secret Orders poster.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = Edmond T. Gréville
| producer       = Max Schach
| writer         = Adaptation: Marcel Achard
| screenplay     = Ernest Betts Jacques Natanson
| story          = Georges Hereaux Irma von Cube John Loder Dita Parlo Claire Luce
| music          = Hans May
| cinematography = Alfred Black
| editing        = Ray Pitt
| studio         = Grafton Films Trafalgar Film Productions
| distributor    = United Artists
| released       = December 1937 (UK) July 1, 1943 (US)
| runtime        = 84 minutes 66 minutes (US)
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Under Secret Orders, also known as Mademoiselle Doctor, is a 1937   

==Plot==
During the First World War, a female Doctor falls in love with one of her patients who turns out to be a German spy. She herself ends up working for German intelligence.

==Cast==
  
* Erich von Stroheim as Col. W. Mathesius / Simonis  John Loder as Lt. Peter Carr
* Dita Parlo as Dr. Anne-Marie Lesser 
* Claire Luce as Gaby, Renes girl 
* Gyles Isham as Lt. Hans Hoffman 
* Clifford Evans as Rene Condoyan  John Abbott as Armand  Anthony Holles as Mario 
 
* Edward Lexy as Carrs orderly 
* Robert Nainby as French General
* Bryan Powley as Col. Burgoyne, French Secret Service 
* Molly Hamley-Clifford as Madame Samuloi, Proprietor of the Blue Peacock 
* Raymond Lovell as Col. von Steinberg 
* Frederick Lloyd as Col. Marchand, Carrs boss 
* Claude Horton as Captain Fitzmaurice
 

Cast notes:
* Stewart Granger appears in a small role

==See also==
* Stamboul Quest &ndash; 1934 American film starring Myrna Loy Street of Shadows) &ndash; 1937 French film directed by G.W. Pabst
* Fräulein Doktor (film)|Fräulein Doktor &ndash; 1969 film, an Italian/Yugoslavian co-production

==References==
Notes
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 