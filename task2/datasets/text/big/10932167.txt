Emperor and His Brother
 
 
{{Infobox film name = The Emperor and His Brother image = Emperor and His Brother.jpg alt =  caption = Japanese DVD cover traditional = 書劍恩仇錄 simplified = 书剑恩仇录 pinyin = Shū Jiàn Ēn Chóu Lù}} director = Chor Yuen producer = Mona Fong screenplay = Ni Kuang story = Louis Cha starring = Ti Lung Jason Pai Lo Lieh Wen Hsueh-erh Ku Feng music = Eddie H. Wang cinematography = Wong Chit editing = Chiang Hsing-lung Yu Siu-fung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime =  country = Hong Kong language = Mandarin budget =  gross =
}} Louis Chas novel The Book and the Sword. Produced by the Shaw Brothers Studio, the film was directed by Chor Yuen and starred Ti Lung, Jason Pai and Lo Lieh in the leading roles.

==Plot== Manchus away. To hide the truth about his heritage, the emperor orders the two men to be killed, although one of them escapes and is captured again later. The Red Flower Societys chief Chen Jialuo turns out to be Qianlongs younger brother, and the two of them develop a tenuous relationship as the societys members attempt to rescue their captured comrade. They unleash an elaborate plan to kidnap the emperor in exchange for their comrade, and hope to persuade the emperor to join their cause. When the exchange takes place, Qianlong must ultimately continue impersonating a Manchu in order to remain in power as ruler of the Qing Empire. Qianlongs chief lieutenant, Zhang Zhaozhong, is bound and determined to wipe out Chen Jialuo and the Red Flower Society. His opportunity comes when a martial arts contest is scheduled to be held.

==Cast==
*Ti Lung as Chen Jialuo / Chen Shiguan
*Jason Pai as Qianlong Emperor
*Lo Lieh as Zhang Zhaozhong
*Ku Feng as Zhou Zhongying
*Wen Hsueh-erh as Zhou Qi
*Sun Chien as Xu Tianhong
*Chan Kei-kei as Luo Bing
*Tang Wai-ho as Wen Tailai
*Ku Kuan-chung as Yu Yutong
*Mandy Wong as Li Yuanzhi
*Wong Yung as Lu Feiqing
*Yeung Chi-hing as Zhao Banshan
*Kent Cheng as Yang Chengxie
*Chun Wong as Zhang Jin
*Linda Chu as Yu Ruyi
*Liu Lai-ling as Bian Wenlian
*Yau Chiu-ling as Wu Chanjuan
*Kwan Fung as Taoist Wuchen
*Ngai Fei as Wei Chunhua
*Yuen Wah as Chang Bozhi
*Wong Chi-ming as Shi Shuangying
*Lau Fong-sai as Xinyan
*Lee Kin-chung as Zhou Yingjie
*Teresa Ha as Mrs Zhou
*Lau Siu-kwan as Meng Jianxiong
*Wong Ching-ho as Song Shanpeng
*Jing Miao as Li Kexiu
*Yuen Ban as Bai Zhen
*Keung Hon as Wu Jiadong
*Shum Lo as Ma Shanjun
*Kam Kwan as Yu Wanting
*Ng Hong-sang as Chou Hu
*Yeung Hung as Chou Long
*Lam Sung-cheng as Long Tianxing

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 