The Twins Effect II
 
 
{{Infobox film name = The Twins Effect II image = TwinsEffect2_Poster.jpg alt =  caption = Film poster traditional = 千機變II花都大戰 simplified = 千机变II花都大战 pinyin = Qiān Jī Biàn Èr Huādū Dàzhàn jyutping = Cin1 Gei1 Bin3 Ji6 Fa1 Dou1 Daai6 Zin3}} director = Corey Yuen Patrick Leung producer = Albert Lee Zhao Jianguo writer = Chan Kin-chung Roy Szeto Chit Ka-kei Lam Suet Michelle Tsui starring = Charlene Choi Gillian Chung Jackie Chan Donnie Yen Jaycee Chan Daniel Wu Edison Chen Bolin Chen Tony Leung Ka-fai Qu Ying Fan Bingbing Jim Chim music = Tommy Wai cinematography = Chan Chi-ying Chan Kwok-hung editing = Cheung Ka-fai studio = Emperor Motion Pictures Emperor Classic Films Shenzhen Film Studio distributor =  released =   runtime = 104 minutes country = Hong Kong language = Cantonese budget =  gross = 
}} Twins in the leading roles. Co-stars include Donnie Yen, Daniel Wu, Edison Chen, Wilson Chen, Tony Leung Ka-fai, Qu Ying, Fan Bingbing and Jim Chim. Jackie Chan also makes an appearance, along with his son Jaycee Chan who is in his debut. The films original English working title was Huadu Chronicles: Blade of Rose and DVD release title is Blade of Kings.

==Plot==
The film is set in Flower Capital, a land ruled by an evil queen (Qu Ying), who started hating men after her lover, High Priest Wei Liao (Daniel Wu), betrayed her. All men in the kingdom are slaves to women. However, a prophecy foretells that one day, the Star of Rex will find and wield a mythic sword, rise to power, overthrow the queen, and restore the balance of the two sexes.

At the start of the movie, Crouching Tiger Hidden Dragon (Donnie Yen), a master swordsman who has made it his quest to overthrow the queens regime, has commissioned Peachy (Edison Chen) to steal for him a certain engraved stone from the queens palace.  Peachy is successful, but the queens soldiers and spies pursue him.  Before the stone can be recaptured, it comes into the possession of Peachys two friends Charcoal Head (Jaycee Chan) and Blockhead (Bolin Chen), who like Peachy earn a humble living by street-performing in a troupe led by their adoptive father, Blackwood.

The brothers set out to deliver the stone to Crouching Tiger Hidden Dragon, along the way discovering the stone is actually a map, which they assume will lead them to riches.  Before they can meet Crouching Tiger Hidden Dragon, they are intercepted by two lovely, but lethal, female warriors, Spring (Charlene Choi) and Blue Bird (Gillian Chung), each of whom is pursuing the pair for different reasons.  The four agree to follow the map together, eventually learning that the map leads to the mythic sword and that either Charcoal Head or Blockhead is the Star of Rex.  The journey takes them through dangerous terrain, culminating in an encounter with the Lord of Armour (Jackie Chan) who guards the way to the sword.

Even as the four make their way toward the sword, the queen is preparing for their destruction, using her army and her powerful magic.  A final battle will decide the fate of the land.

==Cast==
*Charlene Choi as Spring / 13th Master
*Gillian Chung as Blue Bird
*Jackie Chan as General Wai Shing / Lord of Armour (cameo appearance) 
*Donnie Yen as Crouching Tiger Hidden Dragon
*Jaycee Chan as Charcoal Head / Star of Rex
*Daniel Wu as High Priest Wei Liao
*Edison Chen as Peachy
*Bolin Chen as Blockhead
*Tony Leung Ka-fai as Master Blackwood
*Qu Ying as the Evil Queen
*Fan Bingbing as Red Vulture
*Jim Chim as Palupa
*Xie Jingjing as Marshall Edo Bowman Steven Cheung as a slave
*Kenny Kwan as a slave
*Chan Yu-sum as a slave
*Kam Siu-wai as a slave
*Mou Kit as a slave
*Leung Yuet-wan as a slave buyer
*Lee Nga as the palace guard commander

==External links==
* 
* 
* 
*  at the Hong Kong Movie Database

 

 
 
 
 
 
 
 
 