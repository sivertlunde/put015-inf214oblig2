Margam (film)
{{Infobox film
| name           = Margam
| image          = Still_from_Margam_malayalam_film.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Rajiv Vijay Raghavan
| producer       = Rajiv Vijay Raghavan
| writer         = Rajiv Vijay Raghavan Anvar Ali S.P. Ramesh
| based on       =  
| starring       = Nedumudi Venu Meera Krishna Shobha Mohan
| music          = Issac Thomas Kottukapally Venu
| editing        = Beena Paul
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} National Film Venu and the roles performed by Nedumudi Venu and Meera Krishna were also widely appreciated. 

==Plot==
The film tells the story of a revolutionary who, years later, realizes that his efforts have been wasted and witnesses the ill-fate of his co-rebels and leads a secluded life in a state of clinical depression. 

==Cast==
* Nedumudi Venu as Venukumara Menon, the protagonist. A former militant communist revolutionary, now living a secluded life as a parallel college lecturer.
* Meera Krishna as Prakrithi, Menons daughter
* Shobha Mohan as Elisabeth, Menons wife
* K. P. A. C. Lalitha Menons sister
*Valsala Menon Menons aunt
* Madambu Kunjukuttan
* V K Sreeraman
* Madhupal
* P. Sreekumar

==Awards== National Film Awards 
* Indira Gandhi Award for Best First Film of a Director
* Special jury mention - Nedumudi Venu

; Kerala State Film Awards  
* Kerala State Film Award for Best Film
* Kerala State Film Award for Best Screenplay - Rajiv Vijay Raghavan, Anvar Ali and S. P. Ramesh
* Kerala State Film Award for Best Actor - Nedumudi Venu
* Kerala State Film Award (Special Jury Award) - Meera Krishna Kerala State Film Award for Best Cinematography - Venu
* Kerala State Film Award for Best Background Music - Issac Thomas Kottukapally
* Kerala State Film Award for Best Sound Recordist - N. Harikumar

; Fajr Film Festival, Tehran, Iran
* Asian Competition - Best Script - Rajiv Vijay Raghavan, Anvar Ali and S. P. Ramesh

; Zanzibar International Film Festival, Zanzibar
* Golden Dhow-Best film  

; The South Soth Film Encounter, Asilah, Morocco 
* Golden Waves-Best Script Rajiv Vijay Raghavan, Anvar Ali and S. P. Ramesh
 
; Cine Pobre Film Festival,  Cuba
* Special Jury Prize for the film
* Best Actor prize Nedumudi Venu
 
; International Film Festival of Kerala, India
* FIPRESCI Special Mention - Margam

==References==
 

==External links==
*  
*  

 
 

 
 
 


 