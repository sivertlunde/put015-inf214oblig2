Tora-san's Cherished Mother
{{Infobox film
| name = Tora-sans Cherished Mother
| image = Tora-Sans Cherished Mother.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Shunichi Kobayashi Akira Miyazaki
| starring = Kiyoshi Atsumi Orie Satō
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 93 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  aka Tora-sans Homeward Journey, Am I Trying? Part II, and Torasan Pt. 2  is a 1969 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Kuruma Torajirō (Tora-san), and Orie Satō as his love interest or "Madonna".  Tora-sans Cherished Mother is the second entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
Tora-san hears that his mother is still alive and returns to Tokyo to track her down. He discovers that she is a geisha who had had only a brief affair with his father. While visiting the family, he falls in love with Natsuko, the daughter of an old teacher.       

==Cast==
* Kiyoshi Atsumi as Torajiro 
* Chieko Baisho as Sakura
* Chōchō Miyako as Okiku
* Orie Satō as Natsuko Tsubouchi
* Tsutomu Yamazaki as Kaoru Fujimura
* Chieko Misaki as Torajiros aunt
* Gin Maeda as Hiroshi Suwa
* Taisaku Akino as Noboru Kawamata
* Akiko Kazami as Osumi
* Hisao Dazai as Umetarō Katsura (Print Shop)

==Critical appraisal==
Kiyoshi Atsumi was given the Best Actor award at the Mainichi Film Awards for his performance in this film and the previous entry in the series, Its Tough Being a Man (also 1969). Yoji Yamada was also awarded Best Director at the ceremony for the same two films. 

The German-language site molodezhnaja gives Tora-sans Cherished Mother three and a half out of five stars.   

==Releases==
Tora-sans Cherished Mother was released theatrically on November 15, 1969. 

===Home media===
In Japan, the film was released on videotape in 1983 and 1995, and in DVD format in 2005 and 2008.  AnimEigo released the film on DVD in the US along with the other first four films in the Otoko wa Tsurai yo series on November 24, 2009.   

==References==
 

==Bibliography==

===English===
*  
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 
 
 