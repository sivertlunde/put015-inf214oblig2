The Road (2009 film)
 
{{Infobox film
| name           = The Road
| image          = The Road movie poster.jpg
| caption        = Theatrical release poster
| director       = John Hillcoat Nick Wechsler Steve Schwartz Paula Mae Schwartz
| screenplay     = Joe Penhall
| based on       =  
| narrator       = Viggo Mortensen
| starring       = Viggo Mortensen Kodi Smit-McPhee Robert Duvall Charlize Theron Guy Pearce Warren Ellis
| cinematography = Javier Aguirresarobe 
| editing        = Jon Gregory 2929 Productions
| distributor    = The Weinstein Company Dimension Films   Icon Productions  
| released       =  
| runtime        = 111 minutes 
| country        = United States
| language       = English
| budget         = $25 million   
| gross          = $27.6 million 
}} novel of the same name by American author Cormac McCarthy. The film stars Viggo Mortensen and Kodi Smit-McPhee as a father and his son in a post-apocalyptic wasteland.

Filming took place in Pennsylvania, Louisiana, West Virginia and Oregon. The film received a limited release in North American cinemas from November 25, 2009, and was released in United Kingdom cinemas on January 4, 2010.    

== Plot ==
A man and his young son struggle to survive after an unspecified cataclysm has killed most plant and animal life. Civilization has collapsed, reducing the survivors to scavenging and in most cases cannibalism. The duo search for supplies as they travel south on a road to the coast in the hope it will be warmer. The man is armed with a revolver, but carries just two Cartridge (firearms)|rounds. 
 flashbacks reveals that the mans wife had given birth to the child shortly after the catastrophe. The flashbacks show her deteriorating morale and physical condition in their home, eventually leading to the man cutting up floorboards for fuel. Through the flashbacks and other scenes, it is revealed that they had been saving three bullets as a last resort in case of capture, allowing them to commit suicide. When the man kills an intruder and is left with only two bullets, she states he has done this deliberately in an effort to avoid her suicide. She eventually walks off into the cold with almost no clothing.

After shooting a member of a gang who inadvertently stumbles upon them, the man is left with only one bullet. Later, exploring a large mansion, the pair discover prisoners in the basement; serving as a food supply for their absent captors. When the armed cannibals return, the man and his son hide. With discovery imminent, the man prepares to shoot his son, but the cannibals are distracted by the captives, and the pair get away.

Further down the road, they discover an underground shelter full of canned food and supplies. They feast, bathe and groom themselves. When the man hears rummaging noises around the entrance to the shelter, he decides they must leave. They later encounter a nearly-blind old man on the road. The son persuades his reluctant father to feed him something, and they share some time together.

Arriving at the coast, the man goes to scavenge what he can from a beached ship. He leaves his son to keep watch, but the boy falls asleep and they are robbed of everything. They chase the thief down, and the man demands to know why the thief has been following them. The thief denies that he has been. The father then takes everything from him, even his clothes. The boy is upset about what is essentially a death sentence, and the father relents. They go back, but cannot find the thief, so they leave behind his clothes and a can of food.

As they pass through a ruined town, the man is shot in his right leg with an arrow. He kills his ambusher with a flare gun he found on the ship, leaving the archers companion weeping over his dead body. The man again demands to know why they were being followed, and the charge is again denied. He is so weakened by the wound that they have to abandon their cart and most of their possessions. When his condition deteriorates, he realizes he is dying. He again emphasizes to his son the values of self-preservation and humanity.

After the father dies, the son is approached by a man who gives him the choice of joining him, a woman, their two children and their dog. The family had followed the pair for some time out of concern for the boy. The child joins them after being assured they are the "good guys".

== Cast ==
In the film, none of the characters are given a name, and the credits simply give their role in place of a name.   

* Viggo Mortensen as Man
* Kodi Smit-McPhee as Boy
* Charlize Theron as Woman, the Mans wife (appears in a series of Flashback (narrative)|flashbacks). Theron was a fan of the book and had worked with producer Nick Wechsler on the 2000 film The Yards.  The woman has a larger role in the film than in the book, with Hillcoat stating "I think its fine to depart from the book as long as you maintain the spirit of it."   
* Robert Duvall as Ely, the Old Man
* Guy Pearce as Veteran, a father wandering with his family
* Molly Parker as Motherly Woman, the Veterans wife Michael Kenneth Williams as Thief
* Garret Dillahunt as Gang Member

== Production ==
 
In November 2006, producer   was hired to script the adapted screenplay. Wechsler and his fellow producers Steve and Paula Mae Schwartz planned to have a script and an actor cast to portray the father before pursuing a distributor for the film.  By the following November, actor Viggo Mortensen had entered negotiations with the filmmakers to portray the father, though he was occupied with filming Appaloosa (film)|Appaloosa in New Mexico. 

The film had a budget of $20 million.  Filming began in the   and neighboring boroughs.  Filming was also done at the 1892 amusement resort (Conneaut Lake Park) after one of the parks buildings (the Dreamland Ballroom) was destroyed in a fire in February 2008. The beaches of Presque Isle State Park in Erie, Pennsylvania were also used.  Hillcoat also said of using Pittsburgh as a practical location, "Its a beautiful place in fall with the colors changing, but in winter, it can be very bleak. There are city blocks that are abandoned. The woods can be brutal." Filmmakers also shot scenes in parts of New Orleans that had been ravaged by Hurricane Katrina and on Mount St. Helens in Washington (U.S. state)|Washington.    The Abandoned Pennsylvania Turnpike, a stretch of abandoned roadway between Hustontown and Breezewood, Pennsylvania, was used for much of the production. 

Hillcoat sought to make the film faithful to the spirit of the book, creating "a world in severe psychological trauma|trauma," although the circumstances of the apocalyptic event are never explained.  Hillcoat said "Thats what makes it more realistic, then it immediately becomes about survival and how you get through each day as opposed to what actually happened."    Filmmakers took advantage of days with bad weather to portray the post-apocalyptic environment. Mark Forker, the director of special effects for the film, sought to make the landscape convincing, handling sky replacement and digitally removing greenery from scenes.   

== Release ==
 
The Road was originally scheduled to be released in November 2008. It was pushed back to be released in December, and then pushed back a second time to sometime in 2009. According to The Hollywood Reporter, the studio decided that the film would benefit from a longer post-production process and a less crowded release calendar.  A new release date was scheduled for October 16, 2009.    However, according to reports from Screen Rant and /Film, the Weinsteins had decided at the last minute to delay the film to November 25, 2009  as a possible move to make the film more of an Oscar contender, bumping their previous film set for that date, Rob Marshalls adaptation of the musical Nine (2009 live-action film)|Nine (which was also predicted to be a huge awards contender) into December 2009.
 34th Toronto International Film Festival.   

== Reception ==
=== Critical response ===
The film currently holds a 75% Fresh rating on review aggregator Rotten Tomatoes, based on 203 reviews, with an average rating of 7/10. The critical consensus states that "The Road s commitment to Cormac McCarthys dark vision may prove too unyielding for some, but the film benefits from hauntingly powerful performances from Viggo Mortensen and Kodi McPhee."    It also has a score of 64/100 on Metacritic, based on 33 reviews, indicating generally positive reviews from critics. 

A. O. Scott from At the Movies stated that while the film "hits a few tinny, sentimental notes", he "admire  the craft and conviction of this film, and   was impressed enough by the look and the performances to recommend that you see it."  Peter Travers from Rolling Stone calls the film a "haunting portrait of America as no country for old men or young". He states that "Hillcoat -- through the artistry of Mortensen and Smit-McPhee -- carries the fire of our shared humanity and lets it burn bright and true."  Joe Morgenstern from the Wall Street Journal states that viewers have to "hang on to yourself for dear life, resisting belief as best you can in the face of powerful acting, persuasive filmmaking and the perversely compelling certainty that nothing will turn out all right." 

Esquire (magazine)|Esquire screened the film before it was released and called it "the most important movie of the year" and "a brilliantly directed adaptation of a beloved novel, a delicate and anachronistically loving look at the immodest and brutish end of us all. You want them to get there, you want them to get there, you want them to get there—and yet you do not want it, any of it, to end."  IGN gave it four and a half out of a possible five stars, calling it "one of the most important and moving films to come along in a long time." 

In an early review, The Guardian gave the film four stars out of five, describing it as "a haunting, harrowing, powerful film," with Mortensen "perfectly cast" as the Man.  Roger Ebert awarded the film 3.5 out of 4 stars, praising Mortensen and Smit-McPhees work, but he did criticize the film for not being as powerful as the book.  Luke Davies of The Monthly described the film as "gorgeous, in a horrible way, but its greater coolness and distance shows just how difficult it can be to translate to screen the innate psychic warmth of great literature," and suggested the films flaws "might have to do with the directorial point of view—it all feels too detached, in a way that the book in its searing intimacy does not," concluding that the film has "too much tableau and not enough acting." 

A review in Adbusters disapproved of the product placement in the film,  but, as noted by Hillcoat, the references to Coca-Cola appear in the novel, and the company was in fact reluctant about the product being portrayed in the film.  The Washington Post said the film "is one long dirge, a keening lamentation marking the death of hope and the leeching of all that is bright and good from the world...It possesses undeniable sweep and a grim kind of grandeur, but it ultimately plays like a zombie movie with literary pretensions."  Tom Huddleston from Time Out calls the film "...as direct and unflinching an adaptation as one could reasonably hope for." He calls it "...certainly the bleakest and potentially the least commercial product in recent Hollywood history." He calls the movie a "...resounding triumph", noting its "stunning landscape photography   sets the melancholy mood, and Nick Cave’s wrenching score..."  Sam Adams from the Los Angeles Times notes that while "...Hillcoat certainly provides the requisite seriousness,   the movie lacks... an underlying sense of innocence, a sense that, however far humanity has sunk, there is at least some chance of rising again."  Kyle Smith from the New York Post states that "Zombieland was the same movie with laughs, but if you take away the comedy, what is left? Nothing, on a vast scale."  J. Hoberman from the Village Voice states that while "Cormac McCarthys Pulitzer Prize-winning, Oprah-endorsed, post-apocalyptic survivalist prose poem...was a quick, lacerating read", "...John Hillcoats literal adaptation is, by contrast, a long, dull slog."  Jake Coyle from the Associated Press stated that " dapting a masterpiece such as The Road is a thankless task, but the film doesnt work on its own merits". 
  
=== Accolades ===
{| class="wikitable"
|- style="background:#b0c4de; text-align:center;"
! Award !! Date of ceremony !! Category !! Recipient !! Result
|-
| Australian Film Institute December 11, 2010 Best Actor
| Kodi Smit-McPhee
|  
|-
| British Academy Film Awards February 21, 2010 Best Cinematography
| Javier Aguirresarobe
|  
|-
| rowspan="3" | Critics Choice Movie Awards January 15, 2010 Best Actor
| Viggo Mortensen
|  
|- Best Young Performer
| Kodi Smit-McPhee
|  
|- Best Makeup
!
|  
|-
| Denver Film Critics Society
| 2009
| Best Actor
| rowspan="3" | Viggo Mortensen
|  
|-
| Houston Film Critics Society December 17, 2009
| Best Actor
|  
|-
| rowspan="2" | San Diego Film Critics Society December 15, 2009 Best Actor
|  
|- Best Cinematography
| Javier Aguirresarobe
|  
|-
| Satellite Awards December 20, 2009 Best Art Direction and Production Design
| Chris Kennedy
|  
|-
| rowspan="2" | Saturn Awards June 24, 2010 Best Actor
| Viggo Mortensen
|  
|- Best Performance by a Younger Actor
| Kodi Smit-McPhee
|  
|-
| rowspan="2" | Scream Awards October 19, 2010
| Best Science Fiction Movie
!
|  
|-
| Breakout Performance – Male
| Kodi Smit-McPhee
|  
|-
| St. Louis Gateway Film Critics Association December 21, 2009 Best Supporting Actor
| Robert Duvall
|  
|-
| Toronto Film Critics Association December 16, 2009 Best Actor
| rowspan="2" | Viggo Mortensen
|  
|-
| Utah Film Critics Association
| 2009
| Best Actor
|  
|- Venice International Film Festival September 2–12, 2009
| Golden Lion
| John Hillcoat
|  
|-
| Visual Effects Society February 10, 2010 Outstanding Supporting Visual Effects in a Feature Motion Picture
!
|  
|-
| rowspan="2" | Washington D.C. Area Film Critics Association December 7, 2009 Best Actor
| Viggo Mortensen
|  
|- Best Adapted Screenplay
| Joe Penhall
|  
|}

== Home media ==
The DVD and Blu-ray versions were released on May 17, 2010 in the United Kingdom,    and on May 25, 2010 in the United States.   

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 