Here (2011 film)
{{Infobox film
| name           = Here
| director       = Braden King
| writer         = Braden King Dani Valent
| starring       = Ben Foster Lubna Azabal
| music          = Boxhead Ensemble
| cinematography = Lol Crawley
| editing        = Nat Sanders
| distributor    = Strand Releasing
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
| gross          = 
}}
Here is a 2011 American drama adventure film directed by Braden King who also co-wrote the movie with Dani Valent. The film stars Ben Foster and Lubna Azabal.

==Synopsis== San Francisco bay area, travels to Armenia to undertake a mapping survey in the rural areas of the country to confirm ground features and coordinates with GPS satellite data. There he meets Gadarine Nazarian (Azabal), a spirited Armenian ex-pat and professional photographer, who has returned home to face family issues. She decides to accompany Will on his journey, acting as his interpreter, while taking photographs too. They fall in love as they wander the countryside. Their affair lasts several days, but he has to move on.

==Production==
The film was shot on location in Armenia. 

==Reception==
Here garnered mixed to positive reviews currently holding a 77% positive rating on Rotten Tomatoes based on 13 reviews.  Stephen Holden of The New York Times said of the film, "Here, filmed by Lol Crowley, is still a stunner. Flawed as it is, I admire it immensely." 

==Awards and nominations==
*2011 Berlin Film Festival 
**Special Jury Prize (Won)

*2012 Independent Spirit Awards 
**Best Cinematography: Lol Crowley (Nominated)

==References==
 

==External links==
*  
*  
*  

 
 
 
 