Third Dimensional Murder
{{Infobox film
| name           = Third Dimensional Murder
| image          =
| image_size     =
| caption        =
| director       = George Sidney Pete Smith
| writer         = Jerry Hoffman
| narrator       = Pete Smith
| starring       = Pete Smith Ed Payson
| music          = David Snell (uncredited)
| cinematography = Walter Lundin (director of photography) B.C. Parker (camera operator)
| editing        = Phillip W. Anderson
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews Cineplex Entertainment|Loews Inc.
| released       =  
| runtime        = 8 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 3D short Pete Smith and released by Metro-Goldwyn-Mayer.  This is the last of the Audioscopiks 3D short film series, after Audioscopiks (1936) and The New Audioscopiks (1938).

==Synopsis== Indian warrior, an archer, and the Frankenstein monster (Ed Payson). The latter character was specifically modeled after Boris Karloff in Son of Frankenstein.

==Background==
The third and last in the Pete Smith Audioscopiks 3D series of shorts, Third Dimensional Murder this film used footage shot specifically for it, unlike the previous two shorts which utilized test footage shot by Jacob Leventhal and Jack Norling. Smith, Pete.  "Three Dimensionally Speaking" from New Screen Techniques (Quigley Publishing Company, 1953)  Pages 17–20. 
 Kiss Me Kate.)  
 anaglyph by Technicolor. This film opens in 2-D color, with a young woman showing how to hold the 3-D viewer. Prints for the two earlier films were also made by Technicolor to achieve the red-green anaglyph prints necessary for 3-D projection.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 