Halls of Montezuma (film)
{{Infobox film
| name           = Halls of Montezuma
| image          = Halls of Montezuma Poster.jpg
| caption        = Theatrical release poster
| writer         = Michael Blankfort
| starring       = Richard Widmark Richard Boone Jack Webb Martin Milner Neville Brand Jack Palance Reginald Gardiner Robert Wagner Karl Malden
| director       = Lewis Milestone
| editing        = William H. Reynolds
| producer       = Robert Bassler
| music          = Sol Kaplan Harry Jackson
| distributor    = 20th Century Fox
| released       = January 4, 1951
| country        = United States
| poster         =
| runtime        = 113 min.
| language       = English
| budget         =
| gross          = $2.65 million (US rentals) 
| awards         =
}}
 Academy Award-winner war in the Pacific was incorporated into the films cinematography.

The film, like Darryl F. Zanucks 1949 production Sands of Iwo Jima, was filmed on location at Camp Pendleton, California, with the full cooperation of the USMC.  Its title is a reference to the opening line from the Marines Hymn.

==Plot== prisoners in psychological migraines" for months. Anderson and his platoon have been fighting since Guadalcanal Campaign|Guadalcanal, and now only seven men remain of the original platoon. Although Doc urged Anderson to seek treatment in the United States, Anderson refuses to leave his men and has been relying on Doc to supply him with painkillers.

The men hit the beach and successfully dig in, despite an initial burst of resistance. As four days pass, the seven old-timers in Andersons platoon, including Pigeon Lane (Jack Palance), Sergeant Zelenko (Neville Brand), Slattery (Bert Freed), Coffman (Robert Wagner), and the unstable "Pretty Boy" Riley (Skip Homeier), grow weary of the constant threat of hidden Japanese snipers. One day, the men try to take a ridge of hills, but are beaten back by Japanese rockets, which come as an unpleasant surprise to the commanding officers. When Coffman (whom Anderson saved from drowning at Battle of Tarawa|Tarawa) is killed, Anderson is forced to take some more of Docs pills.

Anderson meets with other officers at battalion headquarters, where Gilfillan recounts the troubles they are having capturing prisoners and getting information from them. Sergeant Randolph Johnson (Reginald Gardiner), a Japanese linguist who uses psychology in interrogating prisoners, questions a POW who has been dubbed "Willie". As Gilfillan receives orders to stop the rockets within nine hours, before the next assault on the hills, Willie informs Johnson that the Japanese soldiers holding a cave stronghold are willing to surrender. Accompanied by Johnson and war correspondent Sergeant Dickerman (Jack Webb), Anderson leads a patrol to the cave, but they are ambushed and Zelenko is blinded.

The men capture the remaining Japanese, including a wounded officer, four laborers and a shell-shocked, elderly civilian. Anderson finds a map on the wounded officer. On the return trip, a sniper shoots at Pretty Boy, who kills him during hand-to-hand combat. The confrontation further unbalances him and he attempts to murder the prisoners. Lane then accidentally shoots and kills Pretty Boy while attempting to stop him. Doc also dies, but not before giving Dickerman a message for Anderson.

Anderson takes his prisoners to headquarters, where the wounded officer commits Seppuku|hara-kiri with a knife he had stolen from Johnson. While map expert Lieutenant Butterfield works on a Japanese map overlay found in Pretty Boys personal effects, Anderson and Johnson learn that one of the POWs is actually an important officer pretending to be a private. From his prideful statements, Johnson deduces where the rockets are located. Anderson learns that Conroy has been killed. Anderson takes the news hard and is ready to give up. Dickerman reads aloud Docs note, however, and Anderson, inspired by Docs appeal for him to be strong for the sake of those whom he survives, throws away his painkillers and again leads his men into battle. Then, as the film closes, U.S. Corsairs fly in and smash the Japanese position, leading Anderson to scream to his men: "Give em Hell", which they echo in unison.

==Cast==
 
*Richard Widmark as Lt. Anderson
*Jack Palance as Pigeon Lane
*Reginald Gardiner as Sgt. Randolph Johnson
*Robert Wagner as Coffman
*Karl Malden as Doc
*Bert Freed as Slattery
*Jack Webb as   Correspondent Dickerman
*Richard Boone as Lt. Col. Gilfillan
*Neville Brand as Sgt. Zelenko
*Martin Milner as Whitney
*Skip Homeier as Pretty Boy
*Don Gordon (actor) as Marine
*Joe Turkel as Marine
*Jack Lee (film director) as Courier
*Philip Ahn Nomura (alias of Maj. Kenji Matsuoda)
*Howard Chuman as Capt. Ishio Makino
*William Yokota as Old Japanese Man
*Rollin Moriyama as Fukado 
*Frank Kumagi as Romeo
*Jerry H. Fujikawa as Japanese soldier (Youguchi-The Winds of War:TV mini-series) 
*Frank Iwanaga as Japanese soldier (Security Officer-Godzilla, King of the Monsters!)
*Charles Lee as Japanese soldier
*Harris Matsushige as Japanese soldier
*Ishizo Sano as Japanese soldier
*Don Hicks as Lt. Butterfield
*Richard Hylton as   Conroy
 
 Paul Douglas were originally set to star in the picture.

==U.S. Marine Corps assistance==
The film used various locations around Camp Pendleton and the adjacent Pacific coast for the landing scenes. The USMC also provided accurate military equipment, such as weapons, tanks and uniforms, as well as providing the manpower to create the logistics of a wartime U.S. Marine battalion.

Serving U.S. Marines and Second World War veterans attended the films premières in New York and Los Angeles. The proceeds from the premières were donated to various charities associated with the United States Marine Corps. The studio also allowed the USMC to use the film for recruitment purposes. On January 11, 1951, the Hollywood Reporter noted that a full company of Marine recruits were to be sworn in at the films première in San Francisco.

==Additional notes==
This movie was the last American-made World War II film for Lewis Milestone. After this film, he made other films, from European films to the caper movie Oceans 11. The final war movie he made was the acclaimed Korean War film Pork Chop Hill, starring Gregory Peck. Of the Montezuma stars, three went to TV: after he played Chinese, Japanese, and Korean characters, Kung Fu. Route 66, The Swiss Family Robinson.

==References==
 

==External links==
*  
* 
*   
 
 

 
 
 
 
 
 
 