Five Dolls for an August Moon
{{Infobox film
| name           = Five Dolls for an August Moon
| image          = Fivedolls.jpg
| caption        = Original Italian poster
| director       = Mario Bava
| producer       = Luigi Alessi
| writer         = Mario di Nardo William Berger Edwige Fenech Renato Rossini
| music          = Piero Umiliani
| cinematography = Antonio Rinaldi
| editing        = Mario Bava
| studio         = Produzioni Atlas Consorziate
| released       = 14 February 1970
| runtime        = 78 min.
| country        = Italy
| language       = Italian ITL 65,000,000
}}

Five Dolls for an August Moon (Italian: 5 bambole per la luna dagosto) is a 1970 Italian giallo film directed by Mario Bava. It concerns a group of people who have gathered on a remote island for fun and relaxation. One of the guests is a chemist who has created a revolutionary new chemical process, and several of the attending industrialists are eager to buy it from him. Business problems become moot when someone begins killing off the attendees one by one.

== Plot synopsis ==
 
At the private island retreat of wealthy industrialist George Stark (Teodor Corra), a group of people have assembled for a weekend getaway. Among the guests is Professor Gerry Farrell (William Berger), a scientist badly in need of a vacation. The first night passes uneventfully, but Farrell is enraged the next morning to discover that Stark and several of the guests planned the weekend to coerce him to sell his latest invention: a formula for industrial resin.

Farrell is immediately established as a decent man, and although his marriage to his wife, Trudy (Ira von Furstenberg), appears to be content, she is actually involved in a secret romance with Starks artist wife Jill (Edith Meloni). Starks business partner, Nick (Maurice Poli), is verbally abusive to his coquettish wife Marie (Edwige Fenech), but does not object to her sleeping with other men, one of whom is the houseboy and manservant Charles (Mauro Bosco). Stark is less of a husband to Jill than he is a business manager. Stark arranges for Jill to have her paintings and artwork publicly displayed and is also a source of constant criticism. The sole happy couple appears to be Nicks co-worker Jack (Renato Rossini), and his wife Peggy (Helene Ronee). Also among the guests is Isabelle (Justine Gall), a teenage girl who is in Starks care while her parents are away.

As Stark, Nick, and Jack each privately offer Farrell a $1 million cashiers check for his formula, Jill makes a horrible discovery on the beach: the dead body of Charles. Having already sent the motor launch away to prevent Farrell from leaving the island, and with the radio strangely out of commission, Stark has no way of contacting the mainland. Charles body is moved into a large walk-in freezer where it hangs with the sides of meat.

Farrell is the next victim. While he is walking alone on the beach, a sniper shoots him. Trudy and Jill, walking hand-in-hand nearby, hear the gunshot and find Farrells dead body and run to tell the others. The sniper is revealed to be Isabelle, who places Farrells body in a rowboat and pushes him out to sea.

As tempers flare over Farrells death and disappearance, the killings escalate. Peggy is shot to death by an unseen assailant. Jack arrives on the scene first and accuses Stark, who denies involvement. Marie is found tied to a tree with a knife in her chest. Jill is found in her bathtub after being electrocuted. Each of the bodies are placed in the freezer. Stark, Jack, Nick, and Trudy hole up in Starks living room for the night, believing that the killer cannot strike without revealing him or herself. After bickering with Trudy, Nick storms off into the night. The next morning he is found dead, and his body is placed with the others.

Stark becomes the most likely suspect when he sneaks out of the house and uncovers a motorboat which will take him to the mainland. As he returns to the house to get supplies, Jack confronts him and reveals himself to be the killer. He and Trudy conspired to kill the others to steal their cashiers checks; Peggy was killed because she came close to uncovering their plan. Jack shoots Stark dead. Jack meets with Trudy in the freezer to exchange the cashiers checks for a microfilm with the formula for Farrells resin, but Trudy shoots him to keep all for herself. Jack shoots her before dying. Isabelle, who had been hiding in the secret passageways in the house, collects the three checks from Trudys handbag.

Several months later, a well-dressed Isabelle arrives at the state prison to visit a very-much alive Farrell who is incarcerated there. It turns out that Farrell didnt develop the resin, but stole it from the real inventor after murdering him. He concocted the entire murder-for-money scheme with Trudy; Jack was an unwitting stooge. To conceal his involvement, Farrell convinced Isabelle that the others were trying to kill him, and had her drug him to make him appear dead. Isabelle pushed him out to sea, hoping someone would rescue him. His rescuers were the police on their way to Starks island; the drug Isabelle used made him so delirious that he confessed the entire scheme. Isabelle gloats over her possession of the $3 million, and leaves Farrell to be hanged for his crimes.

== Cast ==
 William Berger: professor Fritz Farrel Ira von Furstenberg: Trudy Farrel
*Edwige Fenech: Marie Chaney Howard Ross: Jack Davidson
*Helena Ronee: Peggy Davidson
*Teodoro Corrà: George Stark
*Ely Galleani: Isabel 
*Maurice Poli: Nick Chaney

== Release ==
 Kino International released the film on Blu-ray in the United States.

== Critical reception ==
 AllMovie gave it two stars out of five, calling it "a confusing and not terribly exciting whodunit." 

In his review of the 2013 American Blu-ray release for Slant Magazine, Budd Wilkins writes "Five Dolls for an August Moon isnt top-tier Bava by any means, but for those with eyes to see, there are pleasures aplenty to be gleaned from this playfully abstract jeu desprit." 

In his commentary track for the Kino International Blu-ray release of the film, Mario Bava scholar Tim Lucas argues strongly against Bavas own assessment of the film as one of his worst and calls it one of his most beautiful and innovative films.

== Cast ==
 William Berger as professor Fritz Farrel
* Ira von Furstenberg as Trudy Farrel
* Edwige Fenech as Marie Chaney Howard Ross as Jack Davidson
* Helena Ronee as Peggy Davidson
* Teodoro Corrà as George Stark
* Ely Galleani as Isabelle
* Maurice Poli as Nick Chaney

== References ==

 

== External links ==

*  

 
 

 
 
 
 