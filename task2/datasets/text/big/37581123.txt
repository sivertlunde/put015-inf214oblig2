Our Own
{{Infobox film
| name           = Our Own
| image          = 
| caption        = 
| director       = Dmitri Meskhiyev
| producer       = Viktor Glukhov
| writer         = Valentin Chernykh
| starring       = Konstantin Khabenskiy Sergey Garmash Bohdan Stupka
| music          = Svyatoslav Kurashov
| cinematography = Sergei Machilsky
| editing        = Marina Vasilyeva
| studio    = SLOVO
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Russia
| language       = Russian
| budget         = $2,500,000  . kinopoisk.ru. 
| gross     = $120,000 (Russia) 
}}

Our Own ( , Transliteration|translit.&nbsp;Svoi) is a 2004 Russian drama film directed by Dmitri Meskhiyev. The film won the Golden George Award for Best Film at the 26th Moscow International Film Festival in June 2004. Meshiev also won the award for Best Director and Bohdan Stupka won the award for Best Actor.    

== Plot ==
It is October 1941. Soviet Russia has been attacked and invaded by the Germans. The country has begun total mobilization and fighting back. The Great Patriotic War had begun. The Germans occupy large parts of Soviet Russia, including the Pskov Region where some of the province is still free. In this province, a NKVD officer (Sergey Garmash) arrives on his motorcycle. He stops at the officers building and on the highest floor and speaks to a NKVD-Officer who seems to be a major. He hands him the personal documents of a fallen Red Army soldier. The main NKVD officer known as the "Checkist" eats porridge and drinks milk. Suddenly a German grenade explodes. German special assault troops enter the building and attack the NKVD staff and soldiers. "Checkist" and his NKVD comrade, a political instructor called "Livshits" (Konstantin Khabensky) flee, amid the chaos and gunfire they change clothes in a local laundry, knowing that German troops shoot Red Army commanders, officers and political commissars. Eventually they are caught by soldiers from a German motorized unit who invaded the province and taken many prisoners. (Both Red Armists and local civilians). The prisoners are led westward to the local dislocation point of the German army.

After marching for a time, the German soldiers stop and allow the prisoners to eat.  Livshits is threatened by a Red Armist who takes his food, threatening to give him up to the Germans, who would shoot him as a Jew and a politcommissar. But his throat is cut by "Checkist" later with a razor blade and he dies. After eating the prisoners continue to march. Livshits, "Checkist" and a young Soviet sniper Mitya Blinov (Mikhail Evlanov) decide to escape and flee the German soldiers. The three protagonists find shelter in Blinovs home village, Blyany, where his father Ivan Blinov (Bogdan Stupka) still lives. Blinov Snr allows the three escapees to hide in his barn. "Checkist" suspects that Blinov Sr. is a pro Nazi policeman but Blinov Sr. himself admits to being a village chief who is working for the Germans. The villages have found a way to manage their own interests. Arguments and conflicts appear between "Checkist" and Blinov Sr but the situation calms down and the men begin to cooperate. 

The threat of being reported and arrested by the Germans lead the three protagonists to kill a local pro Nazi policeman and possible traitor, called Mishka. They stab him to death with a knife made for slaughtering pigs, push his dead body and bike in a river, capturing his rifle (Soviet rifle of type "Mosin"). A week later they kill two German motorcyclists in a forest, five km from Blyany, capturing a German   machine gun and French cognac. These actions capture the attention of the local police chief Nikolai (Fedor Bondarchuk). He leads a raid through the five villages of the region he is responsible for and arrests a number of people as hostages, among them Blinov Srs daughters who are imprisoned together with the others and kept in a local school in a nearby province. Blinov Sr. drives to the command center to negotiate with the police chief. The police chief is willing to set the daughters free but only under one condition. Blinov Sr. must give his daughter-in-law Katerina (Anna Mikhalkova) to him as a wife. The marriage will take place in Blinov Sr.s village. If this happens his daughters would go free and his son Mitya (the escapee) would have his documents corrected. But Livshits and Checkist must be given up to him. Otherwise hostages will be shot. Blinov Sr learns that some hostages have been bought out but this is not an option for his daughters. The police chief is not loyal to the Soviets or the Germans but has his own interests. Blinov Sr. sees that the negotiations have failed and leaves. He returns to his village and arranges with "Checkist" and Livshits to shoot the police chief. When he comes to the village with his men, they open fire but he survives and flees to his neighboring village of Kurtsevo and calls a German hunter squad for help. The three protagonists flee into the Eastern forests of Pskov region, Livshits is wounded and sacrifices himself to allow the others to escape. They go to Kurtsevo from afar the sniper Mitya kills the police chief. Blinov Snr appears to turn on Chekist, who declares himself to be one of you. Blinov Snr lets him go and tells his son to follow him, to go with him and protect his homeland. Maybe not as soldiers but as partisans? Alone Blinov Snr must return to his village to try to save his daughters, to buy them out with two old gold coins.

== Cast ==
* Konstantin Khabenskiy – Livshits, political instructor/politcommissar
* Sergey Garmash – NKVD-Officer a.k.a "Checkist"
* Ben Perino – Tank Commander
* Mikhail Evlanov – Mitya Blinov, the Soviet sniper
* Bohdan Stupka – Ivan Blinov, father of Mitya and village chief
* Natalya Surkova – Anna
* Anna Mikhalkova – Katerina
* Fedor Bondarchuk – Police master

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 