Under the Same Moon
{{Infobox Film
| name           = Under the Same Moon
| image          = La Misma Luna,.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Patricia Riggen
| producer       = Ligiah Villalobos Gerardo Barrera
| writer         = Ligiah Villalobos
| starring       = Adrián Alonso Kate del Castillo Eugenio Derbez America Ferrera Jesse Garcia
| music          = Carlo Sillioto Los Tigres del Norte Song "Que Puedo Hacer" Author and composer: Rodolfo Garavagno-Henry Nelson Performer Pepito Pérez y Los Playeros
| cinematography = Checco Varese
| editing        = Aleshka Ferrero
| distributor    = Fox Searchlight Pictures The Weinstein Company
| released       = July 27, 2007 (festival) March 19, 2008   March 20, 2008  
| runtime        = 104 minutes
| country        = Mexico United States
| language       = Spanish English
| budget         = $500,000
| gross          = $23,311,391

}}
 Spanish and English directed by Patricia Riggen and starring Adrián Alonso, Kate del Castillo, and Eugenio Derbez. 

==Plot==
The film tells the story of Rosario (Kate del Castillo), a mother who illegally immigrates to the United States, and her nine-year-old son, Carlitos (Adrián Alonso). Rosario and Carlitos have not seen each other in four years, since Carlitos was five. Rosario, now living in Los Angeles, California, calls her son, (still in Mexico), every Sunday at 10 in morning from a payphone. Carlitos lives in a small Mexican village with his sick grandmother and his oppressive aunt and uncle, who try to take custody of him in order to get the money that Rosario sends to him. One day, while working for a woman named Carmen (Carmen Salinas), Carlitos encounters two American immigrant transporters (coyotes), Martha (America Ferrera) and David (Jesse Garcia), who offer to smuggle small children across the border.

When his grandmother passes away, Carlitos decides that he cannot live with his aunt and uncle, so he finds the two coyotes. Though he successfully crosses the border without being caught, the car that he hides in is towed away and he is separated from the two coyotes since they had parking violations and expired tags and didnt pay for them. Afterwards, Carlitos continues the journey, eventually staying with other illegal immigrants while they pick tomatoes. However, immigration police raid the building, and arrest most of the workers, but Carlitos and another worker named Enrique (Eugenio Derbez), who does not like Carlitos, escape. 

While traveling, Carlitos and Enrique arrive at a restaurant managed by a Native American man and his wife. Carlitos manages to gain employment for both Enrique and himself. At the restaurant, Carlitos looks up his absentee father, Oscar Aguilar Pons (Ernesto DAlessio), who he learned about from his aunt and uncle despite his grandmothers wishes, in a phone book, and they meet at a wholesale store and have lunch there. Oscar agrees to take Carlitos to Los Angeles to where Rosario is, but later changes his mind, angering Carlitos. 

In turn, Enrique decides to take Carlitos to Los Angeles. The two take a bus ride and make it to LA. Carmen manages a phone call to Rosario to let her know that Carlitos is missing and Rosario decides to leave on a bus back to Mexico. Meanwhile, Carlitos and Enrique searched the city for the pay-phone that his mother called him from every Sunday. Failing to find the pay-phone after hours, the two fall asleep on a park bench. As Rosario is about to depart on a bus ride, seeing a pay-phone by the bus reminds her of Carlitos and she gathers her thoughts to make a decision.

As the sun rises,  Carlitos is still sleeping on a park bench, Enrique leaves to buy food and Carlitos is spotted by the police. Carlitos is almost caught by the police, but Enrique throws a cup of coffee at the officers, provoking the officers to chase him instead. Carlitos manages to escape but Enrique is caught. He finds himself in a familiar place that matched all of his mothers descriptions and sees her across the street waiting at a payphone. Carlitos calls out to her and she is overjoyed to see that he is unharmed. There are too many cars for Carlitos to cross against the light, so the two anxiously wait on either side of the street. The movie ends on the image of the crosswalk changing to a walk signal.

==Cast==
* Adrián Alonso as Carlos "Carlitos" Reyes
* Kate Del Castillo as Rosario Reyes
* Eugenio Derbez as Enrique
* America Ferrera as Martha
* Jesse Garcia as Mike
* Maya Zapata as Alicia
* Gabriel Porras as Paco
* Sonya Smith as Mrs. Snyder
* Carmen Salinas as Doña Carmen
* Ernesto DAlessio as Oscar Aguilar Pons

==Reception==
===Critical response===
The film received generally favorable reviews from critics, getting a 72% fresh rating on Rotten Tomatoes and a 59/100 "mixed or average" rating on Metacritic. 

===Box office===
The film was released in the United States on March 19, 2008,  in 266 theaters.   (2008, March 23). Yahoo! Movies. Retrieved March 23, 2008. 

==Home release==
Under the Same Moon was released on DVD June 17, 2008, in the United States.

===Broadcasting history=== OTA during Mothers Day, May 10, 2009.

==References==
 

==External links==
*  
*  
*  
*   

 

 
 
 

 

 

 
 
 
 

 

 
 
 
 
 