Call of the Bush
 
 
{{Infobox film
| name     = Call of the Bush
| image    =
| caption  =
| director =
| producer =
| writer   =
| based on = Charles Woods
| music    =
| cinematography =
| editing  =
| studio = The Gaumont Agency
| distributor =
| released =  
| runtime  = 2,000 feet 
| language = Silent film English intertitles
| country = Australia
| budget =
}}
Call of the Bush is a 1912 Australian silent film. It is considered a lost film. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p 37 

==Plot==
The film was billed as "a story of the Australian bush, based on the incidents of the easy miner settlements."  It was divided into the following chapters:
*the squatters son
*a welcome home
*the shepherds daughter
*Bosun, the dog hero
*attacked by blacks
*the last cartridge
*a foul revenge
*wrongly accused
*a sundowner to the rescue
*great court scene. 

==Production==
This was the first film made in Australian by the Gaumont Company. 

==References==
 

==External links==
* 

 
 


 