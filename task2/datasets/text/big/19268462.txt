Value for Money
{{Infobox film
| name           = Value for Money
| image          = Value for Money.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Ken Annakin
| producer       = Sergei Nolbandov	(producer) Earl St. John (executive producer)
| writer         = Derrick Boothroyd (novel) R. F. Delderfield William Fairchild
| narrator       = 
| starring       = 
| music          = Malcolm Arnold
| cinematography = Geoffrey Unsworth
| editing        = Geoffrey Foot
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 1955 British comedy film directed by Ken Annakin and starring John Gregson, Donald Pleasence, Leslie Phillips, Joan Hickson, Derek Farr and Diana Dors.

==Cast==
* John Gregson as Chayley Broadbent
* Diana Dors as Ruthine West
* Susan Stephen as Ethel
* Derek Farr as Duke Popplewell
* Frank Pettingell as Mayor Higgins
* Charles Victor as Lumm
* Ernest Thesiger as Lord Dewsbury
* Jill Adams as Joy
* Joan Hickson as Mrs. Perkins
* Donald Pleasence as Limpy
* John Glyn-Jones as Arkwright
* Leslie Phillips as Robjohns
* Ferdy Mayne as Waiter
* Charles Lloyd-Pack as Mr. Gidbrook

==Synopsis==
A wealthy young man (John Gregson|Gregson) from Yorkshire visits a London nightclub and meets a performer (Diana Dors|Dors). She decides to take him for every penny he is worth, and he decides to let her.

==External links==
* 

 

 
 
 
 
 
 
 


 
 