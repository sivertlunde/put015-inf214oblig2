Ghosts Before Breakfast
 
{{Infobox film
| name           = Ghosts Before Breakfast
| image          =
| caption        =  Hans Richter
| producer       = Hans Richter
| writer         = Werner Graeff Hans Richter
| starring       = Werner Graeff
| music          = 
| cinematography = Reimar Kuntze
| editing        = 
| distributor    = 
| released       =  
| runtime        = 9 minutes
| country        = Weimar Republic 
| budget         = 
}}
 Hans Richter.      It utilizes stop motion for some of its effect and live action for others. The film does not present a coherent narrative, and includes a number of seemingly arbitrary images. The original soundtrack, written by Paul Hindemith, was destroyed by the Nazis, but new audio tracks have been created by artists such as The Real Tuesday Weld. British composer Ian Gardiner, who has written many scores for cinema and television, created a score for the film in 2006 (premiered by the Liverpool group Ensemble 10/10, directed by Clark Rundell). UK-based American composer Jean Hasse (Visible Music) wrote a score in 2008 for the UK-based ensemble Counterpoise (violin, trumpet, alto sax, piano).

==Cast==
* Werner Graeff
* Walter Gronostay
* Paul Hindemith
* Darius Milhaud
* Madeleine Milhaud
* Jean Oser
* Willi Pferdekamp Hans Richter

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 
 