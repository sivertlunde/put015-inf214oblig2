Le Viol du Vampire
{{infobox film
| name           = Le Viol du Vampire
| image          = Viol-Vampire,_jean_rollin-1968.jpg
| imagesize      = 250px
| caption        = Original poster
| director       = Jean Rollin
| producer       = 
| writer         = Jean Rollin
| starring       = Solange Pradle Bearard Letrou Catherine Deville Ursule Pauly Nicole Romain Marquis Polho Louise Horn Jacqueline Sieger
| music          = Yvon Garault
| cinematography = 
| editing        = Jean-Denis Bonan Mireille Abramovici
| distributor    = Les Films ABC
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = ₣500,000 
}} 1968 film directed by Jean Rollin. It was his directorial debut. The film consists of two parts: The Rape of the Vampire and The Vampire Woman/Queen of the Vampires. Originally, the film was only supposed to be a short, but a second part was filmed and added later so that it could be released as a feature film. 

Critical reaction to the film was very hostile. Its poetic spirit and strong inspiration from American serials did not seem to attract viewers or critics at the time of its release. The films reception was terrible and it provoked a scandal. 

Although the film was viewed negatively at the time of its release, Le Viol du Vampire remains an important film in the Rollin oeuvre. Indeed, several themes developed in his subsequent feature films were already present: vampires, a fascination with old cemeteries, lesbianism and a pronounced taste for eroticism. Some scenes and characters were even copied almost identically in his later films.

==Synopsis==
===The Rape of the Vampire===
Four sisters living in an old château are convinced that they are vampires. One believes she was raped by the villagers years before, and is blind. Another is afraid of sunlight. They all react violently to crucifixes. The sisters are being manipulated by a sinister old man who alternates between admonishing them to kill newcomers that threaten their exposure, and groping their breasts. The four seem to worship a bestial idol in the forest who speaks to them with a disembodied voice.

The newcomers are three Parisians, Thomas (Bernard Letrou), Brigitte (Solange Pradle) and Marc (Marquis Polho), who have come to the countryside to cure the sisters of their so-called illness. They do not believe that the sisters are vampires, and dont believe in vampires at all. Thomas is a psychoanalyist, determined to cure them from their madness. He believes it has been induced by the superstitious villagers, who have driven the confused women insane with their religious symbols and persecution. He tries to convince them that crucifixes and sunlight wont harm them, and that the blind sister can actually see. He takes all of this as proof that their vampirism is all in their minds. When one of the sisters fall for Thomas charms, the old man orders another sister to kill him, Brigitte and Marc. When this fails, he unleashes the peasants, who brutally murder all the women they can find, which also includes Brigitte.

Thomas asks one of the sisters to bite him to prove her wrong, and discovers she is, in fact, a vampire, and that he was misled by his own preconceptions. The two flee to the beach and are gunned down by Marc, who is distraught by Brigittes death at the hands of the peasants.

===Queen of the Vampires/The Vampire Woman===
The vampire queen (Jacqueline Sieger) is introduced. She briefly arrives by boat to the beach where the dead couple lies. She commands her hooded cohort to grab the old man and pin him down to the slab of rock, then proceeds to sacrifice him, and licks the knife covered in his blood. The vampire queen tells her leading female minion to dismember the bodies of Thomas and the vampire sister so that they dont come back to life, but she fails. It is later revealed that she is in rebellion against the vampire queen. The blood from the old man revives Thomas and the vampire sister.

The human doctor who runs the demented clinic is under the supervision of the vampire queen and he has been secretly searching for a cure for vampirism.

The vampires abduct Brigittes body from the cenotaph, and Thomas later discovers that Brigitte is alive. She tells him that he imagined the entire trip, but he doesnt believe her. He follows her to the hospital where she is listening to an instruction tape. He stops the tape and kills her.

The doctors plot is later uncovered. While the vampire queen stages a ceremony to marry the doctor to his assistant, her minions strip the assistant and whip her on the beach. The malcontents have not bowed to her rule and the revolution explodes, which ends with the vampires being killed and the vampire queen poisoned. Thomas and the vampire wall themselves in the cellar to await death. They do not wish to feed on the living, but are too afraid that if they stay free, their thirst will drive them to murder, so they sacrifice themselves instead, ending their freedom in each others arms. 

==Cast==
* Solange Pradle as Brigitte
* Bernard Letrou as Thomas
* Catherine Deville
* Ursule Pauly
* Nicole Romain as Marc
* Marquis Polho
* Louise Horn
* Doc Moyle
* Yolande Leclerc
* Philippe Druilette
* Jean Aron
* Mei Chen
* Edith Ponceau-Lardie
* Jean-Denis Bonan
* Jacqueline Sieger as the Vampire Queen (uncredited)
* Ariane Sapriel (uncredited)
* Alain Yves Beaujour (uncredited)
* Annie Merlin (uncredited)
* Oliver Rollin (uncredited)
* Barbara Girard (uncredited)
* Jean Rollin (uncredited)

==Production==
  fantasy genre. He accepted the proposal from Lavie enthusiastically. With producer Sam Selsky, he was given a budget of 200,000 francs, which gave him the opportunity to assemble a small team and start shooting the film.

===Casting===
Along with his assistants and technicians, Rollin could not bring in professional actors due to the low budget.  Among the cast playing the four vampire sisters were Ursule Pauly, a model, and Nicole Romain, a stripper. Solange Pradel (Brigitte), Bernard Letrou (Thomas) and Marquis Polho (Marc), who played the three heroes, had never acted in a movie beforel neither had the rest of the cast. None had undergone training in drama except Ariane Sapriel.

===Filming===
The filming of Le Viol du Vampire began in 1967 and took place almost entirely in the Paris suburbs at Claye-Souilly, around an old abandoned house which served as the home of the four vampire sisters. The surrounding woods and fields furnished the framework for the outdoor sequences. Only the death scene of Thomas and the vampire sister was not filmed in Île-de-France (region)|Île-de-France. Instead, Rollin chose a beach near Normandy, France|Normandy, Pourville-lès-Dieppe, which was dear to his heart since his teens and had already been used as a setting in 1958 for his first short film, Les Amours Jaunes (The Yellow Lovers). The beach was seen again on numerous occasions throughout the filming, as the team had to be innovative to compensate for the low budget, The same beach was used as a setting in his later films. During the scene, in which the four vampire sisters were attacked, the peasants were played by Polho and several members of the crew, who had been employed for the occasion as actors because Rollin could not hire extras.

==Release==
===Home media===
Unlike other Rollin films, Le Viol du Vampire remained unreleased on VHS in France for a very long time, due to it being shot in black and white. It was not until 2000 that Norbert Moutier, a friend of the director, finally issued it on VHS via NG Mount International. The DVD edition was released in France in 2003 by CRA.
 Salvation Films. 

The film was released in the US on March 19, 2002 by Image Entertainment. It is presented in 1.66:1, which was not enhanced for widescreen televisions. 

It was released again in Europe on October 20, 2007 by Encore Films in a restored version with a new aspect ratio of 1.78:1, and extra materials including a 28-page booklet. 

===Controversy===
On 27 May 1968, Le Viol du Vampire was released to theaters in Paris by Jean Lavie and his associates. Its release coincided with local political events, which resulted in it drawing large audiences. Due to strikes and riots, it was one of few theatrical productions available for viewing at the time. Screenings of the film unleashed taunts, jeers and threats of damage against Rollin. 

Jean Rollin explained in an interview: "Le Viol was a terrible scandal here in Paris. People were really mad when they saw it. In Quartier Pigalle|Pigalle, they threw things at the screen. The principal reason was that nobody could understand the story". 


==See also==
*Vampire film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 