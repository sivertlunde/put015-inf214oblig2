My Bare Lady (film)
{{Infobox film
| name           = My Bare Lady
| image          = My Bare Lady movie poster.jpg
| image_size     = 230px
| caption        = My Bare Lady movie poster
| director       = Arthur Knight producer = Tony Tenser (as "Phineas Lonestar Jnr")
| writer         = Jervis MacArthur
| narrator       = 
| starring       = Carl Conway  Julie Martin 
| music          = 
| cinematography = 
| editing        = 
| distributor    =  
| released       = 1963
| runtime        = 64 min.
| country        = United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} 1963 exploitation American woman visiting Great Britain who meets and falls in love with a U.S. Korean War veteran who is involved with a local nudist camp. The young woman is initially distressed at the man’s clothing-free lifestyle, but later changes her mind and sheds her garments when a kindly housekeeper relates a romantic story of a young couple who fell in love in Paris and later married at a British nudist colony.     Simon Sheridan, Keeping the British End Up: Four Decades of Saucy Cinema, Titan Books 2011 p 46-47 

My Bare Lady was also released with the titles Bare Lady, Bare World, Its a Bare World and My Seven Little Bares. 

==References==
 
VIRUS

==External links==
* 

 
 
 
 
 
 

 
 