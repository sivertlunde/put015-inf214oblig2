Street Hero
{{Infobox film name = Street Hero image = Street_Hero.jpg caption = director = Michael Pattinson writer = Jan Sardi producer = Paul Dainty Julie Morton starring = Bill Hunter Peta Toppano distributor = Village Roadshow Pictures released =   (Australia) runtime =  102 minutes country = Australia language = English budget = AU$2.5 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p150-151  gross = AU$729,344 (Australia)
}} 
 Bill Hunter AFI award.

==Plot==
Vinnie is a teenage boy who is an outcast at school, alienating teachers and students alike. He is a local courier for the local Mafia boss. He lives in welfare housing with his mother (Peta Toppano), a young brother and sister, and his Mums lover who he cant stand. Vinnie takes out his aggression with the world practicing boxing at the local gym. He is haunted by images of his father (when just a boy he witnessed his fathers murder) and his fathers boxing career. His music teacher (Sandy Gore) encourages him to get involved as a drummer with the school band, and his girlfriend Gloria (Sigrid Thornton) and others influence him to stay away from the Mafia.

==Awards== AFI awards in 1984 including Best Actor (Vince Colosimo), Best Supporting Actress (Sandy Gore), Best Supporting Actress (Peta Toppano), Best Original Screenplay (Jan Sardi), Best Original Music Score (Garth Porter), although it only won Best Achievement in Sound. 

==Cast==
* Vince Colosimo as Vinnie
* Sigrid Thornton as Gloria
* Sandy Gore as Bonnie Rogers Bill Hunter as Detective Fitzpatrick
* Ray Marshall as George
* Amanda Muggleton as Miss Reagan
* Peta Toppano as Vinnies Mother
* Luciano Catenacci as Ciccio
* Peter Albert Sardi as Joey
* Robert Noble as Mick OShea
* Tibor Gyapjas as Freddo
* Vince DAmico as Nino
* John Lee as Vice Principal
* Fincina Hopgood as Trixie
* John Murphy as Old Harry
* Tony Volpe as Sylvester
* Libbi Gorr as Alexia
* Laurie Dobson as SP Bookmaker
* Shane Feeney-Connor as Yokels Mate
* Pip Mushin as Yokels Mate
* Jenny Apps as Yokels Moll
* George Bidlo as Large Man
* Lois Collinder as Travel Lady
* George Harlem as Pimp
* Tiriel Mora as Junky
* Chris Hargreaves as Policeman
* Graham Brooke as 1st Boxing Opponent

==Production==
Colosimo, Sardi and Pattinson had previously made Moving Out (1982). When making that film Sardi and Pattinson discovered music teachers would help underprivileged children by encouraging them to become involved in music, which inspired this film. Pattinson took the script to Bonnie Harris of Roadshow Entertainment, who got Paul Dainty involved. 

The movie was a conscious effort on the part of Pattinson and Sardi to make something more commercial than their first film, while still having something based in reality.  The director later admitted that the choice of music used in the film - including Leo Sayer, Sharon ONeill and Dragon - was "maybe a bit too middle of the road". 

==Box office==
Street Hero took in $729,344 (AUS) at the box office, making it the 203rd most successful Australian Film (1996–2008). 

==Soundtrack==
 
# "Every Beat Of My Heart" (Garth Porter) - Jon English, Renee Geyer - 3:04 Dear Enemy - 4:19
# "Blood Red Roses" (Sharon O’Neill) - Sharon O’Neill - 4:58
# "Haunting Me" (Leo Sayer, Vini Poncia) - Leo Sayer - 4:52
# "Billy’s Theme"(Rock instrumental) (Garth Porter) - 2:05 Dragon - 3:55 Ross Wilson, David Pepperell, James Black) - Ross Wilson, James Black - 4:06
# "No Angels Tonight" (Garth Porter, Clive Shakespeare, Tony Leigh) - Daryl Braithwaite - 3:30
# "Something To Believe In" (Del Shannon, Wendy Matthews) - Del Shannon - 3:11
# "Every Beat Of My Heart" (Garth Porter, Red Symons, John Shaw) - The Streethero Orchestra - 3:09

==References==
 

==External links==
*  at IMDb
*  at Oz Movies
*  at Allmovie
*  at Screen Australia
*  at British Film Institute

 

 
 
 
 
 
 
 
 
 