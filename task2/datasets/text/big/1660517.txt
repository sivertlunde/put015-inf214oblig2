Alfalfa's Aunt
{{Infobox film
| name           = Alfalfas Aunt
| image          = Alfauunt TITLE.JPEG
| caption        =
| director       = George Sidney
| producer       = Jack Chertok
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Jackson Rose
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 37"
| country        = United States
| language       = English
| budget         = $17,972 Leonard Maltin|Maltin, Leonard and Bann, Richard W. (1977, rev. 1992). The Little Rascals: The Life and Times of Our Gang, p. 238. New York: Crown Publishing/Three Rivers Press. ISBN 0-517-58325-9 
}}
Alfalfas Aunt is a 1939 comedy short subject, the 176th entry in the Our Gang (Little Rascals) series originally created by Hal Roach. Produced by Jack Chertok for Metro-Goldwyn-Mayer and directed by George Sidney, the one-reel short was released to theaters in January 1939 by MGM. It is considered one of the best Our Gang shorts produced after Roach sold the series to MGM in mid-1938; the post-Roach era is generally considered a subpar era for Our Gang.

==Plot==
Alfalfas Aunt Penelope sends the Switzers a telegram that says that shes coming to visit. She has given up her pursuits of being a sculptor and has turned to writing murder mysteries. Just before Alfalfas parents leave to attend a meeting that night, Penelope reads Alfalfas father John a page of her story, which is written in the form of a letter:

"Dear X, 

I have discovered that only my nephew stands between me and the Switzer millions! So like the others, he shall die in agony - tonight - at the stroke of nine!"

After John and his wife Martha go out and leave Alfalfa in Penelopes care, Alfalfa stumbles upon the page that Penelope read to John. He becomes horrified, believing that his own aunt is plotting to murder him. He tells Penelope that he is going to bed and then summons the gang to help prevent him from being murdered. While dumping out poisons in the bathroom, Porky causes a sudden loud, long noise, which leads Penelope to believe that the house is being burglarized.

Eventually, Alfalfas parents return home and the gang proudly tell the adults that they saved Alfalfa from being killed by his aunt. Angered by the insanity and misunderstandings, Penelope decides to pack up and leave immediately ("How can an author write in a madhouse like this?!"). The entire situation is explained and all the misunderstandings are cleared up. Before Alfalfa is given a chance to apologize to his Aunt Penelope, John rewards him with a dollar bill just for getting rid of her.

==Cast==
===The Gang===
* Carl Switzer as Alfalfa Switzer
* Eugene Gordon Lee as Porky
* George McFarland as Spanky
* Billie Thomas as Buckwheat
* Gary Jasgar as Slapsie
* Leonard Landy as Leonard

===Additional cast=== Barbara Bedford as Martha Switzer, Alfalfas mother
* Marie Blake as Marthas sister and Alfalfas Aunt Penelope. William Newell as John Switzer, Alfalfas father

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 