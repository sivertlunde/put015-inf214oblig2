Room 304
 
{{Infobox film
| name           = Room 304
| image          = Room 304.jpg
| caption        = Film poster
| director       = Birgitte Stærmose
| producer       = Jesper Morthorst
| writer         = Kim Fupz Aakeson
| starring       = Mikael Birkkjær
| music          = 
| cinematography = Igor Martinovic
| editing        = 
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}
Room 304 ( ) is a 2011 Danish drama film directed by Birgitte Stærmose.    

==Cast==
* Mikael Birkkjær as Kasper
* Stine Stengade as Nina
* David Dencik as Martin
* Luan Jaha as Agim
* Ariadna Gil as Teresa
* Lourdes Faberes as Maid
* Ksenija Marinković as Elira
* Trine Dyrholm as Helene
* Magnus Krepper as Jonas
* Ivo Gregurević as Nebojsa

==References==
 

==External links==
*  

 
 
 
 
 
 

 