Oohalu Gusagusalade
{{Infobox film
| name            = Oohalu Gusagusalade
| image           = Oohalu Gusagusalade poster.jpg
| writer          = Srinivas Avasarala Rashi Khanna,  Srinivas Avasarala
| director        = Srinivas Avasarala
| cinematography  = Venkat C. Dileep Sai Korrapati, Rajani Korrapati
| editing         = Kiran Ganti
| studio          = Varahi Chalana Chitram
| country         = India
| released        = June 20, 2014
| runtime         = 128 min Telugu
| Kalyani Koduri
| budget          =   2.5 Crores
| gross           =   11 Crores (30 days)
}}
 Telugu romantic Sai Korrapati Rashi Khanna Cyrano de Bergerac and revolves around a young and selfish girl Prabhavati and two men Venky and Uday who love her.   
 Kalyani Koduri Hyderabad and Vishakhapatnam. After aggressive promotion, the film released worldwide on June 20, 2014.    Upon release, the film received positive reviews and went on to become a big hit, completing 50 days in 21 centers. 

==Plot== Rashi Khanna). Prabha has also come for a vacation  to Vizag for peace of mind as her parents are going through a divorce in Delhi. Venky and Prabha meet in a movie theater and come to know that they reside in the same apartment complex. Venky develops a crush on her but he is advised not to take any hasty steps by his uncle (Rao Ramesh) and aunt (Hema (actress)|Hema).

Venky and Prabha develop a good friendship and when Venky starts developing much stronger feelings for her, his uncle asks him to list at least 10 reasons why he loves her. He writes 100 reasons in a book and sings them all together once (Inthakante Vere - Version 1). On the birthday of Prabha, Venky takes her out to a nice dinner and proposes to her. She replies that though she likes him and treats him as her best friend, she cannot accept his proposal as she is only 19 and cant take such a big decision at that age. She asks him to continue their friendship and they can take a decision later. Venky is offended and reacts telling her that  he is new to "Delhi culture" with flirtatious and temporary relationships.  Annoyed, she leaves the restaurant and Venkys efforts to apologize to her fail completely as she leaves for Delhi the next day. After Venky narrates his story, both Venky and Uday say good bye to each other and part for the night. Next morning, Uday and Sirisha meet and all attempts by Uday to impress her fail. She decides to seek another alliance and to choose the best between the two. Then, Uday and his broker decide to send Venkys photograph as the new bridegroom. The plan is to portray him as a wastrel and Uday as a good friend, so that Sirisha accepts Uday. Venky is forced to act as per the plan under the threat that he will be dismissed from his job and his career will be ruined if he fails to do so. Uday also offers him a job as  a news reader in UB TV if he obeys the formers orders. Venky obliges and visits a temple next morning where he is scheduled to meet Sirisha. Here, he realizes that Sirisha is none other than Prabha, his former muse. To avoid an uncomfortable situation, Venky enlists the help of Chinta Guru Murthy a.k.a. Guru (Harish Koyalagundla), a NRI who befriends Venky during the temple visit.

Venky persuades Guru to pose as the prospective bridegroom, so that Prabha can reject him and go back to Uday.  When Prabha reaches home, she comes to know that bridegroom was Venky and not Gurumurthy. She wants to meet Venky and propose to him as she grew to cherish their relationship over the past few years. Next day, Uday and Venky sit in a coffee shop where Uday gives Venky a Bluetooth device and a smart phone for communication. Using that, Uday plans to seek Venkys real-time help while conversing  with Prabha. When Uday goes to bring the phones charger, Prabha meets Venky and proposes to him. Before Venky can respond, Uday reaches the location and Venky runs into washroom along with the phones charger. Via Bluetooth, Venky helps Uday converse eloquently with Prabha. The conversation of Uday and Prabha ends with a decision to meet at 7:00 PM at a hotel the next Friday for dinner. When Uday leaves, Venky comes back and he agrees to meet her at 9:00 PM on the same Friday near Necklace road. During the dinner, Uday wants to give a love letter to Prabha and Venky pens it in the form of a poem which can be sung as a song (Em Sandeham Ledu).

Uday speaks to Prabha while seeking guidance from Venky via bluetooth  and when she is about to leave, he gives her the love letter. She reads it during her journey to Necklace road and is impressed. When she meets Venky at Necklace road and proposes to him, he rejects her and she leaves dejected. Next night, Uday goes to Prabhas house with a bouquet when she is about to spend a little quality alone-time. But this time, neither Venkys phone signals work, nor his phones battery has sufficient charging. Thus, Uday can seek Venkys help only for a few moments and when the phone call ends, Uday proposes to prabha on the spur of the moment and tries to present her a ring. The plan backfires and Prabha asks Uday to leave her house. After leaving the house, Uday enlists the help of Venky again and sings a song praising her beauty (Inthakante Vere - Version 2). She is impressed and agrees to marry Uday. When Prabha wears the ring given by Uday, Venky is dejected and leaves home. At home, Venky is advised by his mother (Pragathi (actress)|Pragathi) that Uday is using him as a scapegoat. She advises him to resign from his job and win back Prabha. When Venky reaches the office next morning, his colleague Kalyani tells him that he is selected as the news reader for the evening news bulletin. He goes to Udays chamber and submits his resignation. When he is about to leave Venkys office to meet Prabha, Uday beats him up and ties him to a chair and covers his mouth with a tape. Then, Uday selects Vamana Rao as the evening bulletin news reader. However, Kalyani who saw Venky entering Udays chamber doubts Uday.Uday and Prabha meet up at a shopping mall and start to watch  UB TVs first ever news bulletin. Venky at Udays chamber causes a fire  in the room with a cigarette lighter to garner attention and  is rescued. Kalyani sends him to read weather telecast and when he is visible on screen, his parents and his uncle and aunt at Vishakhapatnam are happy for him. He then reveals the whole truth and proposes to Prabha telling her that he visited Vishakhapatnam every summer vacation since he met her though she never returned. Before Uday can stop Prabha from leaving him, he is attacked by Guru who locks him up in a restroom at the shopping mall. With the help of Guru, Prabha reaches UB TV office and after a small quarrel, Venky and Prabha unite. Before Uday can fire Venky, he is stopped by his father. Because of all these developments, the TRPs of UB TV cross TV9 and Venky becomes highly popular.  Uday makes Venky the news reader of evening bulletin and he assigns Vamana Rao to Teleshopping  The film ends with Venky helping Uday in flirting with a girl using Bluetooth communication.

==Cast==
*Naga Shourya as N. Venkateswara Rao a.k.a. Venky Rashi Khanna as Sirisha Prabhavathi a.k.a. Prabha
*Srinivas Avasarala as Uday Bhaskar a.k.a. Uday Surya as Uday Bhaskar father  
*Rao Ramesh as N. Venkateswara Rao Uncle  http://www.andhraheadlines.com/news/reviews/132317/review-oohalu-gusa-gusalade  Hema as N. Venkateswara Rao Aunty  
*Harish Koyalagundla as Guru Murthy a.k.a. Guru
*Posani Krishna Murali as Vamana Rao Prudhviraj as marriage broker Kalyani Koduri as Black ticket seller (cameo appearance)

==Production== Cyrano de Sai Korrapati NRI Venkat Kalyani Koduri was selected as the music director of the film and the recording of the songs started on October 1, 2013.    After rejecting 40 - 50 titles, the title Oohalu Gusagusalade, the title of a song from the N. T. Rama Rao film Bandipotu, was selected.   
 Rashi Khanna John Abraham Hyderabad and Vishakhapatnam with the use of limited resources and low budget.   Because of the same reason, lot of close up scenes were shot and the set properties in the background were changed so that each frame would look different as they did not have enough money to fill the entire frame with such properties.  The cinematographer said that there were limited lights and they had to work hard to make the scenes appear natural using those lights during the shoot at Vishakhapatnam. 

==Soundtrack==
{{Infobox album
| Name = Oohalu Gusagusalade
| Longtype = To Oohalu Gusagusalade
| Type = Soundtrack Kalyani Koduri
| Cover =
| Released = April 28, 2014
| Recorded = 2013 Feature film soundtrack
| Length = 15:13 Telugu
| Label = Vel Records Kalyani Koduri
| Last album = Anthaku Mundu Aa Taruvatha (2013)
| This album = Oohalu Gusagusalade (2014)
| Next album = Bandipotu(2015)
}}
 Kalyani Koduri Sunitha even before the start of the films production.  Though Anantha Sreeram was declared as the single card lyricist of the film during the launch of the movie, the final version featured Anantha Sreeram penning 2 of the 4 songs while the remaining two being penned by Sirivennela Sitaramasastri.  The audio was launched at Hyderabad in a promotional audio launch event on April 28, 2014 on Vel Records Label.  The soundtrack received positive response and were praised by the critics upon the films release on June 20, 2014.    Kalyani Koduri dedicated this soundtrack to music lovers on June 21, 2014 on the occasion of Worlds Music Day by releasing a press note. 

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 15:13
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Emiti Hadavidi
| lyrics1         = Anantha Sreeram
| extra1          = Deepu, Shravani V.
| length1         = 03:26
| title2          = Inthakante Vere (Version 1)
| lyrics2         = Sirivennela Sitaramasastri Hemachandra
| length2         = 03:45
| title3          = Em Sandeham Ledu
| lyrics3         = Anantha Sreeram Kalyani Koduri, Sunitha
| length3         = 03:52
| title4          = Inthakante Vere (Version 2)
| lyrics4         = Sirivennela Sitaramasastri Karunya
| length4         = 04:09
}}

==Release==
In early June 2014, the makers confirmed that the film would release on June 20, 2014 worldwide in theaters after being awarded with a clean U certificate from Central Board of Film Certification.  The distribution rights of Nizam, West, Guntur, Nellore and Bangalore were acquired by Suresh Movies. Vishakhapatnam & Krishna district distribution rights were acquired by Annapurna Distributions, East region distribution rights were acquired by 14 Reels Entertainment and Ceeded region rights were purchased by NVR Cinema. The Overseas theatrical distribution and screening rights were bought by CineGalaxy Inc., and the overseas theaters list was released by them on June 18, 2014.   As a part of the films promotion, 100 hoardings featuring the films posters were used in Hyderabad alone and posters were also used in various bus shelters and were used as bus stickers. The film released in 200 theaters worldwide on June 20, 2014.  

===Reception=== Oneindia Entertainment gave a review stating "Though Oohalu Gusagusalde has simple storyline, Srinivas Avasarala has created fantastic screenplay. The director has good grip on the narration of the whole show. Overall, Oohalu Gusagusalde is a good romantic comedy entertainer and it will impress audience in A centres. Go watch it this weekend" and rated the film 3.5/5.    Deccan Chronicle gave a review stating "The climax scene and the comedy routines fall a bit flat, still this breezy romantic entertainer is breath of fresh air. After watching all the regular masala action films, you can watch this film for its clean comedy sans double meaning dialogues" and rated the film 3.5/5.  IndiaGlitz gave a review stating "Oohalu Gusa Gusa Lade is an almost flawlessly written screenplay supported by deft execution. It is a rom-com that is high on writing and imaginative in terms of presentation" and rated the film 3.25/5.  Sify gave a review stating "Oohalu Gusagusalade is told in lighter manner. It has good writing and crackling romantic scenes with soothing music. The film has good scripting with some witty lines. Overall it is comedy that saves the film." 
 National Award Hindupur Member MLA Nandamuri Balakrishna, actor N. T. Rama Rao Jr. praised the films director and producer for their efforts.    Venkat C. Dileeps cinematography was praised by award winning cinematographers K. K. Senthil Kumar and P.G. Vinda. While K. K. Senthil Kumar appreciated the beach song for its colors and texture, P. G. Vinda appreciated the night photography. 

==References==
 

==External links==
* 

 

 
 
 