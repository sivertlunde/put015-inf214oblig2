Witness to Murder
 
{{Infobox film
| name           = Witness to Murder
| image          = Witnessmurder.jpg
| alt            = 
| caption        = Theatrical release poster Roy Rowland
| producer       = Chester Erskine
| screenplay     = Chester Erskine Nunnally Johnson
| starring       = Barbara Stanwyck George Sanders Gary Merrill
| music          = Herschel Burke Gilbert
| cinematography = John Alton
| editing        = Robert Swink
| editing        = Chester Erskine Productions
| distributor    = United Artists
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| grass          = 
}}
Witness to Murder is a 1954 suspense film starring Barbara Stanwyck.  While the film received moderately positive reviews, it ended up as an also-ran to Alfred Hitchcocks somewhat similar Rear Window, which opened less than a month later. The latter picture was a box-office hit. 

==Plot== strangled to death. The woman reports the killing to the police, but when the man named Albert Richter (George Sanders) notices detectives arriving downstairs, he moves the body.  When the police show up to his door, Richter acts nonchalant, and when no body is found, the police are convinced that the woman saw something in her dreams.  

The next day, the man puts the womans body in a trunk and leaves to disposes of the body.  While he is out, Draper notices that the mans apartment is for rent and is given a tour by the building manager (Dick Elliott). She finds torn drapery (which Richter dubiously re-ripped in front of the police) and a pair of womens earrings. Richter returns and sees Draper drive away to the police department with the earrings. He preemptively phones the police and Draper is then accused of robbery.  The two confront each other at the police station, but Richter opts not to press charges.  However, the scene leaves Police Lt. Lawrence Mathews (Gary Merrill) suspicious.

Mathews goes to Drapers apartment and tells her that Richter is an ex-Nazi who had been "Denazification|denazified" and is now an unsuccessful author who is marrying a wealthy heiress. The two meet again when the body of an unidentified woman is found in a park.  Draper comes off as conspiratorial and Mathews believes she is pretending and obsessing about the case -- he believes she is telling the truth that she saw something, but does not think what she saw was reality. She is forcibly admitted to an insane asylum after Richter surreptitiously types threatening messages from Draper to him to frame her as crazy and a threat to his safety.  

While Draper is away, Mathews and a fellow policeman go to the apartment building of the deceased woman to see if anyone there recognizes Richter -- no one does, and the police have no case.  After Draper is released, Richter is at her home and confesses that he killed the girl because she was insignificant to him and he did not want his future wealth to be threatened.  However, because she is officially labeled insane by the police and has no credibility, he does not fear admitting anything to her. 

Richter later returns to her home with a purported suicide note wherein Draper says she will kill herself, and he tries to push her out of her window. Just as he is about to throw her out, a policewoman buzzes at the door and Draper flees. She is pursued by Richter, as well as the police, who think she is suicidal.  Draper runs up a high-rise that is under construction, and gets to the tops and is cornered by Richter.  He pushes her off the tower, but there are a few construction planks below the precipice onto which she falls and is saved.  Mathews arrives and Richter attempts to push him off as well, but after a brief struggle, it is Richter who falls to his death.  Mathews rescues Draper and the police now come to believe her story.

==Cast==

 

* Barbara Stanwyck as Cheryl Draper
* George Sanders as Albert Richter
* Gary Merrill as Police Lt. Lawrence Mathews Jesse White as Police Sgt. Eddie Vincent Harry Shannon as Police Capt. Donnelly
* Claire Carleton as The Blonde Lewis Martin as Psychiatrist
* Dick Elliott as Apartment Manager Harry Tyler as Charlie
* Juanita Moore as Black Woman 
* Joy Hallward as Fellow Worker
* Adeline De Walt Reynolds as The Old Lady
* Claude Akins as Police Officer
* Sam Edwards as Tommy - Counterman
 

==Critical response==
Film critic Dennis Schwartz appreciated the work of cinematographer John Alton but gave the film a mixed review, writing, "The camerawork of John Alton is the star of this vehicle. His camerawork sets a dark mood of the Los Angeles scenario, escalating the dramatics with shadowy building shots. The twist in the story is that as upstanding a citizen as Stanwyck is, the authorities still side with the Nazi Sanders because he has a higher status. The noir theme of alienation is richly furnished. But the rub is in the storys credibility -- Stanwyck was just too strong a character to be so completely victimized." 

==Adaptations==
Witness to Murder was featured on  . Retrieved August 16, 2014. 

==References==
 

==External links==
*  
*  
*  
*   informational site by Mark Fertig
*  

 

 
 
 
 
 
 
 
 
 
 
 
 