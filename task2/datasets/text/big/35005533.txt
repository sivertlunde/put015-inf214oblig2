Yandé Codou, la griotte de Senghor
{{Infobox film
| name           = Yandé Codou, la griotte de Senghor
| image          = 
| caption        = 
| director       = Angèle Diabang
| producer       = Karoninka Africalia Belgium
| writer         = 
| starring       = 
| distributor    = 
| released       = 2008
| runtime        = 52 minutes
| country        = Belgium Senegal
| language       = 
| budget         = 
| gross          = 
| screenplay     = Angèle Diabang Brener
| cinematography = Florian Bouchet Fabacary Assymby Coly
| editing        = Yannick Leroy
| music          = Yandé Codou Sène, Wasis Diop, Youssou NDour
}}

Yandé Codou, la griotte de Senghor is a 2008 documentary film.

== Synopsis == Serer polyphonic poetry. This documentary, shot over four years, is an intimate portrait of the diva that traveled through the history of Senegal by the side of one of the country’s legendary figures, poet President, Léopold Sédar Senghor. A sweet and bitter story about greatness, glory and the passage of time.

== Awards ==
* Festival de Cine de Dakar 2008

== References ==
 

 
 
 
 
 
 
 
 
 
 


 
 