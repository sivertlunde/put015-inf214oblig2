The Four Year Plan
{{Multiple issues|
 
 
}}

{{Infobox film
| name         = The Four Year Plan
| runtime      = 
| creator      = Mat Hodgson
| writer       = Mat Hodgson
| director     = Mat Hodgson
| producer     = Mat Hodgson
| starring     = Flavio Briatore Amit Bhatia Alejandro Agag Neil Warnock
| editing      = 
| music        = 
| budget       = 
| country      = United Kingdom
| language     = English
| network      = 
| released     =  
}}
The Four Year Plan is a documentary film directed by Mat Hodgson about London based football club Queens Park Rangers.

The film chronicles the take over of the nearly bankrupt club in 2007 by a consortium of billionaires and their effort to promote the team to the Premier League by 2011. The consortium consisted of Bernie Ecclestone, Flavio Briatore and Alejandro Agag, steel magnate Lakshmi Mittal and Amit Bhatia. It is an observational documentary that follows the club from within the boardroom. The cameras for this documentary were brought in by the new owners to create the film, and although the club gave permission for the cameras to be there, they had no say on where on when the cameras would be filming. The title derives from a statement made by Briatore in 2007 where he declared his target to be Premier League in four years.

==Awards and honors==
The Four Year Plan won the Marbella International Film Festival Best Documentary Award in October 2011  and was generally released at the end of 2011.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 