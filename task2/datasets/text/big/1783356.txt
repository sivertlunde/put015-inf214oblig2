Cazuza – O Tempo Não Pára
{{Infobox film
| name           = Cazuza - O Tempo Não Pára
| image          =
| image_size     =
| caption        =
| director       = Walter Carvalho Sandra Werneck
| producer       = Daniel Filho Flávio R. Tambellini  (executive) 
| writer         = Fernando Bonassi Victor Navas
| narrator       = Daniel de Oliveira Marieta Severo Reginaldo Faria Andréa Beltrão Leandra Leal André Gonçalves Débora Falabella
| music          = Cazuza Guto Graça Mello
| cinematography = Walter Carvalho
| editing        = Sérgio Mekler
| distributor    = Columbia TriStar
| released       =  
| runtime        = 98 minutes
| country        = Brazil
| language       = Portuguese
| budget         =
}} Daniel de Oliveira as Cazuza. The film is based on the book by Cazuzas mother, Lucia Araujo. Cazuza - O Tempo Não Pára won a best actor award from the São Paulo Association of Art Critics Awards.

The movie was one of the most successful of the year in Brazil.

==Plot==

Focusing on Cazuzas personal life, the film chronicles his early career, his subsequent success, his drug use and his promiscuous lifestyle. It starts out in the early 1980s in Rio de Janeiro, showing his usual day-to-day life until he joins the band which would become Barão Vermelho. It then shows the bands rise to fame and its frequent "mutinies" which led him to pursue a solo career. Later, it depicts his struggle against the AIDS virus and his final days.

==External links==
* 
*   

 
 
 
 
 
 
 
 
 
 
 
 


 
 