El Alamein: The Line of Fire
{{Infobox film
 | name = El Alamein - The Line of Fire
 | image = El Alamein - The Line of Fire.jpg
 | caption =
 | director = Enzo Monteleone
 | writer =   
 | starring =  
 | music = Pivio and Aldo De Scalzi
 | cinematography = Daniele Nannuzzi
 | editing =   Cecilia Zanuso
 | producer =  
 }} 2002 Cinema Italian War drama|war-drama film written and directed by Enzo Monteleone.  The film won three David di Donatello awards (for best cinematography, best editing and best sound), a Nastro dArgento for best sound and a Globo doro for best new actor (to Paolo Briguglia).    

== Cast ==

* Paolo Briguglia: Soldier Serra
* Emilio Solfrizzi: Lt. Fiore
* Pierfrancesco Favino: Sgt. Rizzo
* Luciano Scarpa: Soldier Spagna
* Thomas Trabacchi: Capt. De Vita 
* Giuseppe Cederna: Capt. Medician 
* Roberto Citran: the Colonel 
* Silvio Orlando: the General

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 