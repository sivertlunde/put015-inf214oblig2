Out of Time (1988 film)
 
{{Infobox film
| name           = Out of Time
| image          = Out of Time (1988 film).jpg
| image_size     = 
| caption        =  Robert Butler
| producer       = 
| writer         = 
| narrator       = 
| starring       = Bruce Abbott Bill Maher Rebecca Schaeffer
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1988 
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}}

Out of Time is a 1988 science fiction film, starring Bruce Abbott and Bill Maher. It was a failed television pilot made into a television movie. 

==Synopsis== time machine, and enlists the aid of his legendary great-grandfather (Maher) in pursuing the crook.

==Cast==
*Bruce Abbott as Channing Taylor 
*Bill Maher as Maxwell Taylor 
*Rebecca Schaeffer as Pam Wallace 
*Kristian Alfonso as Cassandra Barber 
*Leo Rossi as Ed Hawkins 
*Ray Girardin as Capt. Krones 
*Adam Ant as Richard Marcus 
*Arva Holt as Capt. Stuart 
*Tom La Grua as Frank 
*Barbara Tarbuck as Dr. Kerry Langdon 
Directed by Robert Butler

==Production==
This movie was made after the popularity of the 1985 film Back to the Future. 

==Sources==
*TV Guide magazine 
*Article from Lansing State Journal newspaper of July 14, 1988. 
*IMDB movies website

==External links==
* 

 
 

 
 
 
 
 
 
 


 