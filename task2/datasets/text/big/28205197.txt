Mickey's Eleven
{{Infobox film
| name           = Mickeys Eleven
| image          =
| image_size     =
| caption        =
| director       = Albert Herman
| producer       = Larry Darmour
| writer         = Joseph Basil (story) E.V. Durling (story) Fontaine Fox (character creator) Earl Montgomery (story)
| narrator       = Jimmy Robinson Delia Bogard Monty Banks Jr. Kendall McComas
| music          =
| cinematography =
| editing        =
| distributor    = Film Booking Offices of America
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}
 silent short Mickey McGuire series starring a young Mickey Rooney. Directed by Albert Herman, the two-reel short was released to theaters in November 1927 by Film Booking Offices of America|FBO.

==Synopsis==
Mickey and the Scorpions play a game of football against Stinky Davis and his team.

==Notes==
An edited version was released to television in the 1960s as a part of the Those Lovable Scallawags with Their Gangs series.

==Cast==
*Mickey Rooney - Mickey McGuire Jimmy Robinson - Hambone Johnson
*Delia Bogard - Tomboy Taylor
*Unknown - Katrink
*Monty Banks Jr. - Stinky Davis
*Kendall McComas - Scorpions member

== External links ==
*  

 
 
 
 
 
 
 
 


 