That Certain Something
 
{{Infobox film
| name           = That Certain Something
| image          = 
| image_size     = 
| caption        = 
| director       = Clarence G. Badger
| producer       = Frederick Daniell
| writer         = Clarence G. Badger
| narrator       = 
| starring       = Megan Edwards   Thelma Grigg
| music          = 
| cinematography = Arthur Higgins
| editing        = Frank Coffey
| studio         = Argosy Films
| distributor   = RKO 
| released       = 1941
| runtime        = 90 mins
| country        = Australia
| language       = English
| budget    = £12,500 
}}
That Certain Something is a 1941 Australian musical about an American film director who decides to make a musical in Australia. It was the last movie directed by noted silent era director Clarence G. Badger.

==Plot==
A famous director, Robert Grimble, comes to Australia to make a film about pioneering women and seeks for an unknown to play the lead role. He casts socialist Miss Hemingway, who soon proves to be temperamental. She is tricked into walking off the job by Jimmie Jones  who wants his girlfriend Patsy cast. He succeeds and Patsy becomes a star.

==Cast==
 
*Megan Edwards as Patsy OConnell
*Thelma Grigg as Miss Hemmingway
*Georgie Stirling as Blanche Wright
*Lou Vernon as Robert Grimble
*Charles Kilburn as Allan Burke
*Joe Lawman as Bill Lake
*Howard Craven as Jimmie Jones
*Ronald Morse as Marcel du Bois
*Leslie Victor as Maurice Appleby
*Marshall Crosby
*Connie Martyn
*Raymond Longford
*Ross Vernon
*William Beresford
*John Byrne
*Arundel Nixon
*Francis Birtles as bushman 
 

==Production==
The movie was the first from Argosy Films and was made with the assistance of a bank overdraft from the New South Wales government. It was directed by Clarence Badger, a Hollywood director who had retired to Australia. The original title was Daughters of Australia. 

Megan Edwards had only appeared in a few stage shows before being cast in the lead.   She later received a three-year contract from a Hollywood manager. 

The seven-week shoot took place at Pagewood Studios, the first movie made there in three years. The colonial sequence was especially researched.  The camera crew included notable cameraman John Howes, who died aged 29. 

==Release==
Despite securing distribution from RKO, reception to the film from critics and the public was poor. 

==References==
 

==External links==
*  in the Internet Movie Database
*  at Oz Movies

 

 
 
 
 


 