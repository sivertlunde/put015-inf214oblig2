Enclosure (film)
{{Infobox film
| name           = Enclosure
| image          = 
| caption        = 
| director       = Armand Gatti
| producer       = Lado Vilar
| writer         = Armand Gatti Pierre Joffroy
| starring       = Hans Christian Blech
| music          = 
| cinematography = Robert Juillard
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France, Yugoslavia
| language       = French
| budget         = 
}}

Enclosure ( ) is a 1961 French-Yugoslav drama film directed by Armand Gatti. It was entered into the 2nd Moscow International Film Festival where Gatti won the Silver Prize for Best Director.   

==Cast==
* Hans Christian Blech as Karl
* Jean Négroni as David
* Herbert Wochinz as Scheller
* Tamara Miletic as Anna
* Maks Furijan as Weissenborn
* Stevo Zigon as Dragulavic
* Janez Vrhovec as Walter (as Janez Vrkovec)
* Michel Bouyer as Doctor Crémieux
* Janez Skof as Kapo #1
* Janez Cuk as Kapo #2
* Janko Hocevar as Jova
* Pero Kvrgic as Sanchez
* Lojze Potokar as Police Officer
* Frane Milcinski as Wagner

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 