Behind Office Doors
{{Infobox film
| name           = Behind Office Doors
| image          = BehindOfficeDoorsFilmPoster.jpg
| caption        = Theatrical Poster
| director       = Melville W. Brown   
| producer       = William LeBaron    Henry Hobart (associate)  Carey Wilson (screenplay) J. Walter Ruben (adaptation and dialogue) 
| based on       =   
| narrator       =
| starring       = Mary Astor Robert Ames Ricardo Cortez
| music          =
| cinematography = J. Roy Hunt
| editing        = Archie Marshek Rose Loewinger 
| distributor    = RKO Radio Pictures
| released       =   }}
| runtime        = 82 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
}}
 Carey Wilson and J. Walter Ruben, based on Alan Schultzs novel, Private Secretary.  It starred Mary Astor, Robert Ames and Ricardo Cortez, and revolved around the premise of "the woman behind the man".  While not received well by critics, it was well-received by the public at large.

==Plot==
 
Mary Linden (Mary Astor) is a receptionist at a paper milling company, who is secretly in love with one of the salesmen, James Duneen (Robert Ames).  Her extensive knowledge of the paper industry, the mill and its clients allows her to have input in company operations far outweighing her level as a receptionist.  As the current president of the company, Ritter (Charles Sellon), approaches retirement, Mary uses her knowledge and skill of company politics to enable James to make some important sales coups, after which she begins a fifth-column attempt to get him named as the next president.  James, for his part, is grateful to her for her help, but is completely oblivious to her romantic interest in him, preferring more of the party girl type.

When Ritter does retire, James wins the position, and Mary is promoted to be his personal secretary.  Still unaware of her feelings, he hires his latest party girl, Daisy (Edna Murphy), to work in the office, and report to Mary.  Mary is upset by this turn of events, but remains faithful to James, assisting him with running the company.  In fact, it is her knowledge and acumen which makes the company successful.  Mary even spurns the advances of several men, including the wealthy Ronnie Wales (Ricardo Cortes), who, although married, is estranged from his wife and wishes to pursue an affair with Mary.

However, when James becomes engaged to the daughter of a wealthy banker, Ellen May Robinson (Catherine Dale Owen), that is the straw which breaks Marys resolve.  She resigns from the company, and eventually agrees to go away with Ronnie for an assignation in Atlantic City.  Between the time of her resignation, and her agreeing to go away with Ronnie, the paper mill is already suffering terribly from a lack of good management, since most of James success was due to Marys guidance.  James tracks her down before she can give in to the libidinous advances of Ronnie, and begs Mary to return.  She is reluctant, until she discovers that James has broken off the engagement with Ellen, and upon her return to the company she is not only met with a job offer, but also a marriage proposal from James.

==Cast==
*Mary Astor as Mary Linden
*Robert Ames as James Duneen
*Ricardo Cortez as Ronnie Wales
*Catherine Dale Owen as Ellen May Robinson
*Kitty Kelly as Delores Kogan
*Edna Murphy as Daisy Presby
*Charles Sellon as John Ritter William Morris as Banker Charles H. Robinson
*George MacFarlane
 AFI database) 

==Soundtrack==
*"Three Little Words", music by Harry Ruby, lyrics by Bert Kalmar - played as dance music in the nightclub

==Reception==
While the public seemed to like the film,    critics like Mordaunt Hall of The New York Times were less kind, stating that the film "is a witless and interminably dull exhibition on which three capable players, Mary Astor, Robert Ames and Ricardo Cortez, have been sacrifi ed to very little purpose.   

==Notes== public domain in the USA due to the copyright claimants failure to renew the copyright registration in the 28th year after production. 

The working title for this film was the title of the novel on which it was based, Private Secretary. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 