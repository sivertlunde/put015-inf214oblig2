Last Will (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Last Will
| image          = 
| caption        = 
| director       = Brent Huff
| producer       =  
| screenplay     = Alan Moskowitz
| story          = Irmgard Pagan
| starring       =  
| music          = Pinar Toprak
| cinematography = Andrew Huebscher
| editing        = Tony Wise
| distributor    = Moviecid
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $5 million  
| gross          = 
}}
Last Will is a 2011 mystery drama film starring Tatum ONeal and Tom Berenger. It was shot in Kansas City, Missouri on a modest budget.

== Plot ==
A woman named Hayden is framed for the murder of her wealthy husband Frank. With all the evidence stacked against her, Detective Sloan arrests her and Hayden finds herself in the fight of her life as she tries to uncover the truth. Set against the backdrop of a wealthy Midwestern city, Last Will tells a story of deception, corruption and misguided family loyalties.

== Cast ==
* Tatum ONeal as Hayden Emery
* Tom Berenger as Frank Emery
* Patrick Muldoon as Joseph Emery
* Peter Coyote as Judge Garner
* Shawn Huff as Laurie Faber William Shockley as Michael Palmer
* Jeffery Dean as Virgil Emery
* James Brolin as Det. Sloan Moon Zappa as Belinda DeNovi

== References ==
*  
*  

 
 
 
 
 
 


 