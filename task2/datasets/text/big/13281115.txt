How About You (film)
{{Infobox film
| name     = How About You
| image    = Howaboutyouposter.jpg
| director = Anthony Byrne
| starring = Hayley Atwell   Vanessa Redgrave   Joss Ackland   Brenda Fricker   Imelda Staunton
| writer   = Jean Pasley (screenplay)   Maeve Binchy (short story) Noel Pearson   Sarah Radclyffe
| released =  :   November 14, 2008
| runtime  = 100 min.
| country  = Ireland
}}   Irish film directed by Anthony Byrne. The film is based on a short story sometimes published as "How About You" and sometimes published as "The Hard Core" in This Year It Will Be Different, a collection of short stories by Maeve Binchy.   It tells the story of a young woman named Ellie who is left in charge of the residential home run by her older sister, during Christmas period. Most of the residents have gone with their families during the holidays, but four residents, known as the hardcore, remain.  Their behavior will cause lots of trouble and will lead to the residence facing closure.

==Synopsis==
Ellie Harris (Hayley Atwell) goes to her sister Kates (Orla Brady) residential home, Woodlane, in search of a job. Ellie has some trouble adjusting to her new job but quickly befriends a resident named Alice (Joan OHara). Ellie decides to have talks with Alice and neglects parts of her job, much to her sisters annoyance. She also has trouble doing what she is told by the residents. Because of her friendship with Alice, Ellie decides to help her feel better by taking her down to the river (which is against the nurses orders) and giving her marijuana to ease her pain. The next day, however, Ellie finds out that Alice has died. 

Kate gets news that their mother had a stroke and decides to be with her. She asks Ellie to stay behind with the four residents that are staying over the Christmas holiday because Ellie and her mother do not get along. Georgia Platts (Vanessa Redgrave) is a former actress and singer. The Nightingale sisters, Heather (Brenda Fricker) and Hazel (Imelda Staunton), are spinsters who moved in after spending years taking care of their mother. Donald Vanston (Joss Ackland) is a recovering alcoholic who enjoys playing piano and used to be a judge. These four residents are driving out others with their eccentric ways. They also seem to believe that, because they pay to be there, they can act however they please. 

While Ellie is watching over them, she learns more about the residents. Donald requests his breakfast in his room at 6 oclock instead of in the dining room at the usual time at 8. After he gets upset that his order is wrong, she tells him that he can have breakfast in the dining room like everyone else. Donald reveals to Ellie that the music he plays at night is his late wifes favorite song and that he regrets not sobering up before she died.

Heather demands that she be given any letters addressed to her sister to keep from upsetting her. However, Kate neglected to tell Ellie this and she got a letter for Hazel from the postman. When Ellie goes to give the letter to Hazel in her room, she notices sketches and paintings in her room and realizes that Hazel is a talented artist. Hazel finds Ellie in her room and demands that she leave. Ellie then slips the letter under the door. It is then revealed that Heather has been writing as Hazel when Hazels son Simon, whom she was forced to give up for adoption, tried to find her. Heather later tells Hazel that she was afraid that Simon would take Hazel away from her. Hazel then tells Heather that she will always love her and they decide to meet Simon together in the new year.

Georgia constantly asks for Ellie to make her martinis and makes frequent trips to the local pub to entertain the patrons. Georgia tells Ellie that after her husband died, she decided to get back into the theater, but realized that everything had changed since she was young. She then moved into Woodlane because she hated being alone and that abusing everyone is "part of the fun".

Ellie decides to have a Christmas dinner with the residents and they all go out to buy decorations and food (Including Hazel, who is terrified of being outside the Woodlane property). They decide to have a drink in the pub and Ellie points out Hazels paintings hung on the wall behind them. Heather then shows everyone her talent at playing pool. They all drive back to the home while singing carols. They then have their Christmas dinner together and start to get along with each other.

The next day, Mr. Evans (Darragh Kelly), from the Department of Health, arrives and is shocked that Kate would leave residents alone. The residents band together saying that they wanted to stay and that they are a family. Donald then asks if he wants to stay for dinner, but Mr. Evans leaves, startled. He then comes back saying that he was upset because his wife just left him and he stays for dinner.

Later, when Kate returns, Heather is shown keeping a garden, Hazel is painting outside, and Donald and Georgia are handing out tea to some boys who are raking the lawn. Ellie then tells Kate that she has decided to leave and will spend some time with their mother. At the end, Donald and Georgia are dancing outside, singing "How About You?".

==Principal cast==
* Hayley Atwell as Ellie Harris
* Joss Ackland as Donald Vanston
* Vanessa Redgrave as Georgia Platts
* Brenda Fricker as Heather Nightingale
* Imelda Staunton as Hazel Nightingale
* Orla Brady as Kate Harris
* Joan OHara as Alice Peterson
* Darragh Kelly as Mr. Evans

==Production notes==
The film is dedicated to Joan OHara who died in July 2007, four months before its November release. 
 Witness for the Prosecution, a film adaptation of Agatha Christies novel. Vanessa Redgrave, who plays Georgia, is in another Christie adaptation, Murder on the Orient Express and plays Christie herself in Agatha (film)|Agatha.

==Reception==
The film was generally well received by critics, holding a 68% "fresh" rating on Rotten Tomatoes. 

==Evaluation in film guides==
Leonard Maltins Movie Guide (2013 edition) gave How About You 2 stars (out of 4), opining that " hat might have been an appealing tale of intergenerational bonding is ill conceived and often downright silly. Livens up in Atwells all-too-brief scenes with elderly, dying OHara". Videohounds Golden Movie Retriever (2011 edition) rated it slightly better, at 2½ bones (out of possible four), describing it as " ess twee than might be indicated (probably because the cast is terrific)".

==References==
 

==External links==
*  

 
 
 
 
 
 
 