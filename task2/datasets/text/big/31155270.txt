Thacholi Othenan (film)
{{Infobox film
| name = Thacholi Othenan
| image =
| caption =
| director = SS Rajan
| producer = TK Pareekutty
| writer = Vadakkan Pattu K Padmanabhan Nair (dialogues)
| screenplay = K Padmanabhan Nair Sathyan Madhu Madhu Sukumari Adoor Bhasi
| music = MS Baburaj
| cinematography = P Bhaskara Rao
| editing = G Venkittaraman
| studio = Chandrathara Productions
| distributor = Chandrathara Productions
| released =  
| country = India Malayalam
}}
  1964 Cinema Indian Malayalam Malayalam film, directed by SS Rajan and produced by TK Pareekutty. The film stars Sathyan (actor)|Sathyan, Madhu (actor)|Madhu, Sukumari and Adoor Bhasi in lead roles. The film had musical score by MS Baburaj.   

==Cast== Sathyan as Thacholi Othenan  Madhu as Payyambilly Chandu
*Sukumari as Unnichara
*Adoor Bhasi as Vanan Ambu
*P. J. Antony as Kathiroor Gurukkal
*Malathi as Thekkumpattu Kunjikanni Ambika as Chathothe Kunji Kunki
*Kunjandi as Kandacheri Chappan
*Kuttyedathi Vilasini 
*Sunny as Kadathanan Nambair
*Premji as Thacholi Valiya Kurup
*S. P. Pillai as Pulluvan Chandu
*Kuthiravattom Pappu as Ramar
*Kedamangalam Ali as Kaduvancheri Yamman Kidavu
*Kottayam Chellappan as Payyanadan Chindan Nambiar

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran and . 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Anjanakkannezhuthi || S Janaki, Chorus || P. Bhaskaran || 
|- 
| 2 || Appam Venam || P. Leela, Shantha P. Nair || P. Bhaskaran || 
|- 
| 3 || Ezhimalakkaadukalil || P. Leela || P. Bhaskaran || 
|- 
| 4 || Janichavarkkellaam   || P. Leela, Chorus || P. Bhaskaran || 
|- 
| 5 || Kanni Nilaavathu || P. Leela || P. Bhaskaran || 
|- 
| 6 || Kottum Njaan Kettilla || P. Leela, Chorus || P. Bhaskaran || 
|- 
| 7 || Naavulla Veenayonnu || KP Udayabhanu || P. Bhaskaran || 
|- 
| 8 || Nallolappainkili || P. Leela, Chorus || P. Bhaskaran || 
|- 
| 9 || Onningu Vannengil || S Janaki || P. Bhaskaran || 
|- 
| 10 || Thacholi Meppele || P. Leela, Chorus || || 
|}

==References==
 

==External links==
*  

 
 
 
 


 