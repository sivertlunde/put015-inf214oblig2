Camouflage (2001 film)
 
{{Infobox Film | 
  name            = Camouflage |
  image           = Camouflagedvd.jpg |
  caption         = dvd Release | 
  director        = James Keach |
  producer        = Patric D. Choi Anthony Esposito|
  writer          = Tommy Epperson Billy Bob Thornton |
  starring        = Leslie Nielsen Lochlyn Munro Vanessa Angel |
  music           = Bruce Young Berman Mark Mothersbaugh |
  cinemathography = Glen Macpherson |
  editing         = Heidi Scharfe|
  distributor     = Sunland Studios Inc.|
  released        = January 9, 2001 |
  runtime         = 93 min. |
  country         =  | English |
  budget          = |
  followed_by     = | 
}}

Camouflage is a 2001 comedy film/action film starring Leslie Nielsen and Lochlyn Munro.

== Cast ==
* Leslie Nielsen : Jack Potter 
* Lochlyn Munro : Marty Mackenzie
* Vanessa Angel : Cindy Davies William Forsythe : Sheriff Alton Owens
* Patrick Warburton : Horace Tutt, Jr.
* Tom Aldredge : Lionel Pond
* Frank Collison : Skinny Cop
* Suzan Krull : Skinny Woman
* C. Ernst Harth : Tiny
* Brahm Taylor : Christie
* Richard Faraci : Ned
* Belinda Montgomery : Diane

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 


 