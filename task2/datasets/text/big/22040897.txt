The Magician's Hat (film)
{{Infobox Film name = The Magicians Hat director = Milan Blažeković producer = Vlado Terešak writer = Fred Sharkey Ivo Škrabalo Sunčana Škrinjarić starring = Dragan Milivojević Emil Glad Ivo Rogulja studio = Croatia Film released = 1990 country = Yugoslavia language = Croatian
}}
 1990 animated film produced by the Croatia Film company and directed by Milan Blažeković. It is the sequel to 1986s The Elm-Chanted Forest.

==Story==
Thistle the magician teams up with his animal friends, along with some fairies and a volcano-dwelling dragon, to rescue their forest from the wrath of Mrazomor the Ice Emperor and his army of icy ghost witches.

==Release==
In late 2007, both this film and its predecessor, The Elm-Chanted Forest, received their DVD debut in the countries of former Yugoslavia from Happy TV. As of 2015, The Magicians Hat has seen no official release in North America.

==See also==
* Cinema of Yugoslavia
* Cinema of Croatia
* List of animated feature-length films

==External links==
*  

 
 
 
 
 
 
 
 
 



 
 