The Off Season
{{Infobox Film 
| name = The Off Season
| image = Poster of the movie The Off Season.jpg
| caption =
| director = James Felix McKenney 
| producer = Larry Fessenden
| writer = James Felix McKenney 
| starring = Angus Scrimm Larry Fessenden Christina Campanella Don Wood Ruth Kulerman Brenda Cooney Jamie Sneider Jordan Kratz
| music = 
| cinematography = 
| editing = 
| distributor = 
| released = 2004
| runtime = 
| country = United States
| language = English
| budget = 
}}
 2004 independent independent horror Larry Fessendens distributed through Lionsgate Home Entertainment.  

==Cast==
*Angus Scrimm &ndash; Ted
*Larry Fessenden &ndash; Phil
*Christina Campanella &ndash; Kathryn Bennett
*Don Wood &ndash; Rick Holland
*Ruth Kulerman &ndash; Mrs. Farthing
*Brenda Cooney &ndash; Nora
*Jamie Sneider &ndash; Stacy
*Jordan Kratz &ndash; Kurt

== Plot ==
In the beginning, The Off Season appears to be the story of a young couple from New York City moving up to Maine to get away from it all. Soon, strange things begin to happen in the one-room apartment that they occupy—things that the once close couple cannot discuss with each other. This is not a typical ghost story resulting from a tragic death, but the tale of a place haunted by guilt and the fear of abandonment.

Unlike most traditional ghost stories, which take place in an old mansion, castle or other giant structure, The Off Season is about the haunting of a tiny room. The main characters have nowhere to hide when confronted by unhappy spirits. It can be easy to run away and slam a door between yourself and something that scares you, but few things are more terrifying than being forced to look the source of your fear directly in the eye.

==Production==
The Off Season was shot in a motel in Old Orchard Beach, Maine, a town that neighbors the one in which writer and director James Felix McKenney grew up and shot his first feature, CanniBalistic!

While filming, the cast stayed at the Waves and Viking motels where the film was set.

==References==
 

==External links==
* 

 
 
 
 
 
 


 