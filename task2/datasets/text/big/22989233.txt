Mukhamukham
{{Infobox film
| name           = Mukhamukham (Face to Face)
| image          =
| director       = Adoor Gopalakrishnan
| writer         = Adoor Gopalakrishnan Karamana Janardanan Ashokan
| producer       = K. Ravindran Nair
| studio         = General Pictures
| distributor    =
| cinematography = Mankada Ravi Varma
| editing        = M. Mani
| released       =  
| runtime        = 107 mins
| country        = India
| language       = Malayalam
| music          = M. B. Srinivasan
}}
 directed by Adoor Gopalakrishnan.   

==Plot==
The film starts in the early 1950s showing Sreedharan, the protagonist, as a very popular communist leader and trade union activist. He is forced to go underground after his name is associated with the murder of the owner of a tile factory. He is considered to be dead by his party and they even erect a memorial for him. But he makes an unexpected comeback almost 10 years later, after the first communist ministry gained and lost power in Kerala and after the Communist Party of India has split. On his return, he spends his time sleeping and drinking. His comeback is first a puzzle and then an embarrassment to his comrades and family. As the disappointment on his new face grows, he is found murdered. The film ends when both the communist parties jointly celebrate his martyrdom.

== Cast == Ashokan as Sudhakaran as a man
* P. Gangadharan Nair as Sridharan
* Krishan Kumar		
* Vishwanathan as Sudhakaran as a boy
*Alummoodan Azeez

==Awards==
The film has been nominated for and won the following awards since its release:

;1984 FIPRESCI Price (New Delhi) National Film Awards (India)
* Won - Best Director - Adoor Gopalakrishnan
* Won - Best Screenplay - Adoor Gopalakrishnan
* Won - Best Feature Film in Malayalam
* Won - Best Audiography

==References==
 

==External links==
* 

 
 
 

 
 
 
 
 
 

 