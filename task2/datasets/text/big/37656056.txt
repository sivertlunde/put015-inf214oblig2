Civilian Clothes
{{infobox film
| name           = Civilian Clothes
| image          =
| imagesize      =
| caption        = Hugh Ford
| producer       = Adolph Zukor Jesse Lasky
| writer         = Clara Beranger(scenario)
| based on       =  
| starring       = Thomas Meighan Martha Mansfield
| music          =
| cinematography = Hal Young
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = USA
| language       = Silent film (English intertitles)

}} Hugh Ford. This film is based on a Broadway play, Civilian Clothes, by Thompson Buchanan. Thurston Hall played Meighans part in the play. It is preserved at Gosfilmofond Russian Archives, Moscow.     

==Cast==
*Thomas Meighan - Captain Sam McGinnis
*Martha Mansfield - Florence Lanham
*Maude Turner Gordon - Mrs. Lanham
*Alfred Hickman - William Arkwright
*Frank Losee - Walter Dumont
*Marie Shotwell - Mrs. Smythe
*Warren Cook - Mr. Lanham
*Albert Gran - Dodson
*Isabelle Garrison - Mrs. Arkwright
*Halbert Brown - Major General Girard
*Kathryn Hildreth - Elizabeth Lanham

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 