The Tooth Will Out
{{Infobox Film |
  | name           = The Tooth Will Out|
  | image          = Toothwillout 1sht.jpg|
  | caption        = |
  | director       = Edward Bernds
  | writer         = Edward Bernds|
  | starring       = Moe Howard Larry Fine Shemp Howard Vernon Dent Margie Liszt Dick Curtis Slim Gaut Emil Sitka|
  | cinematography =  | 
  | editing        =  |
  | producer       = Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 15 59"
  | country        = United States
  | language       = English
}}
The Tooth Will Out is the 134th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
After being fired from two jobs for breaking dishes , the Stooges end up being chased into a dental office. Eventually, the boys study, and graduate from dental school. The dean of the school (Vernon Dent) gives them their first recommendation to go out west (read: far away), and open a practice.
 Coke bottles, Dr. Shemp proceeds to drill the patients teeth until smoke rises from his mouth.

The appointment is abruptly cut short when an irate customer who claims to be the Sheriff (Dick Curtis) enters the office with a serious toothache. Feeling nervous, Shemp accidentally picks up the wrong book, entitled The Amateur Carpenter. They first rub sandpaper to his chest, and paste the inside of his hat which they thought it was to "varnish the lid". After discovering it is the wrong book, the boys go back to business seriously. They take the painful tooth, and yank it out as the patient is pulled off the chair, causing him to wake up. Unfortunately, Shemp extracted the wrong tooth, with the angry Sheriff chasing after the frantic Stooges.

==Production notes==
The Tooth Will Out was filmed on February 19-20, 1951.    The films title parodies the proverbial expression "The truth will out." 

The second half of this film, consisting of the dentist office scene, was originally filmed for inclusion in the previous film Merry Mavericks. However, the scene ran too long and had to be excised from the final cut. Rather than disposing of the surplus dentist footage, the storyline of The Tooth Will Out was built around it. 

This was the last film to feature longtime Stooge supporting actor Dick Curtis.    The voice from the living set of dentures is Vernon Dent. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 