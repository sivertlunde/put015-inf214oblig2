Wide Awake (2007 film)
 
{{Infobox film
| name           = Wide Awake
| image          = Wide Awake film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| rr             = Riteon
| mr             = Rit‘ŏn}}
| director       = Lee Gyu-man
| producer       = Gang Seong-gyu
| writer         = Lee Gyu-man Lee Hyun-jin
| starring       = Kim Myung-min
| music          = Choi Seung-hyun
| cinematography = Kim Dong-eun
| editing        = Steve M. Choe  Kim Chang-ju
| distributor    = CJ Entertainment
| released       =  
| runtime        = 115 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =  
}} 2007 South Korean film.

== Plot ==
In 1980s South Korea, a young boy is traumatised after experiencing anesthesia awareness during heart surgery, and no-one believes his story afterwards. Twenty-five years later, the doctors and nurses who operated on him begin to die under mysterious circumstances. Dr. Ryu Jae-woo, a surgeon married to Hee-jin, believes that the boy he remembers from his childhood is responsible for the deaths. The leading suspects are Lee Myeong-suk, who has been stalking Dr. Ryu, and the seemingly unhinged Uk-hwan. Hypnosis specialist Oh Chi-hoon also seems to know something about these deaths.

== Cast ==
* Kim Myung-min as Ryu Jae-woo
* Yoo Jun-sang as Uk-hwan Kim Tae-woo as Oh Chi-hoon
* Jung Yoo-suk Kim Yoo-mi as Seo Hee-jin
* Kim Roi-ha as Lee Myeong-suk
* Baek Seung-hwan
* Seo Young-hwa Lee Sung-min as Sang-woos father

== Release ==
Wide Awake was released in South Korea on 8 August 2007, " " (2007). Koreanfilm.org. Retrieved on 31 December 2008.  and its opening weekend was ranked fourth at the box office with 238,819 admissions.  The film went on to receive a total of 677,939 admissions nationwide,  with a gross (as of 16 September 2007) of US$4,569,126. 

== Awards and nominations ==
;2008 Grand Bell Awards
* Best Supporting Actor – Yoo Jun-sang
* Nomination – Best Music – Choi Seung-hyun

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 

 