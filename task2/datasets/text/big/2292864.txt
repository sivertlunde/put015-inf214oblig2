Jury Duty (film)
{{Infobox film
| name           = Jury Duty
| image          = Jury dutyposter.jpg
| image_size     =
| caption        = Promotional poster for the film
| alt            = A man dressed as lady liberty, blindfold worn as a headband, sword in one hand, the scales of justice in the other.
| director       = John Fortenberry
| producer       = Yoram Ben-Ami Peter M. Lenkov
| writer         = Neil Tolkin Barbara Williams Samantha Adams
| narrator       =
| starring       = Pauly Shore Tia Carrere Stanley Tucci Brian Doyle-Murray Abe Vigoda
| music          = David Kitay
| cinematography = Avi Karpick
| editing        = Stephen Semel
| studio         = Triumph Films
| distributor    = TriStar Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $21 million
| gross          = $17,014,653
}}
Jury Duty is a 1995 American comedy film directed by John Fortenberry and starring Pauly Shore, Tia Carrere, Stanley Tucci, Brian Doyle-Murray, Shelley Winters, and Abe Vigoda.

The film was actress Billie Birds last screen appearance.

== Plot== erotic dancer Charles Napier), Las Vegas chihuahua Peanut (who loves the game show Jeopardy!).

He rummages through the trash and finds his letter for jury duty that he had thrown away and decides to enter. Each of the jurors get their own free accommodation plus $5 a day. After several cases he gets chosen to be a juror for a murder case involving a man named Carl Wayne Bishop (Sean Whalen), a fast food employee who had been fired and is now accused of murdering several other fast food employees. The evidence against him gets stronger, when several employees appear as witnesses saying that he threatened to kill them.

All the jurors are staying at the Holiday Suite in the section that is being remodeled. Tommy seems to be enjoying himself, until he finds he is sharing his room with his former high school principal, who puts motivational tapes on at night to full blast. He makes a deal with the manager of the hotel who says that he will get a different room if he advertises their business during news broadcasts. Tommy agrees and ends up staying in a luxurious suite.

Finally the jurors go to the jury room for deliberation and appoint Tommy as their Jury foreman|foreman. The other jurors immediately think that Bishop is guilty and want to vote straight away. Not wanting to lose his suite and luxurious lifestyle, Tommy votes not guilty and stalls and prolongs the deliberations as well as continuously going over the evidence. After many delays, the other jurors become increasingly angry with Tommy, especially Monica (Tia Carrere), whom Tommy had fallen in love with, and threaten to throw him out of the jury unless he finds a better way to prove Bishops innocence.

After reading up on the law and further investigating, Tommy finds some evidence after seeing a picture of Carl Wayne Bishop on the window of a store, finally convincing the jurors that Bishop is not guilty; they celebrate. Unfortunately, later on, Monica finds Peanut and follows him up to Tommys luxury suite. Even though Tommy tries to explain that hes now a changed person, she is deeply upset on how he used them and walks out in tears.
 stun gun. The killer is revealed to be Frank, the man Tommy was looking for. Tommy arrives at the same house and relays his new evidence to Frank, asking Frank to help find Monica at the library she works at in order to convince her that his evidence is genuine.

When they arrive at the library, Frank reveals to Tommy and Monica that he was the killer. He ties them up and attempts to stab them with a sharp jagged knife. He tells them that he committed these terrible murders and framed Carl Wayne Bishop because he thought there was no way of truth and justice in the world. With Peanuts help, however, they manage to subdue Frank and knock him unconscious.

Tommy is given a cheque for his contribution in the case and Monica starts dating Tommy. He uses his earnings from erotic dancing towards law school and eventually becomes an Attorney at Law|attorney. Meanwhile, Peanut is shown accomplishing his lifelong dream of becoming a contestant on Jeopardy!.

==Cast==
  
*Pauly Shore as Thomas B. "Tommy" Collins
*Gizmo as Peanut
*Tia Carrere as Monica Lewis
*Stanley Tucci as Frank/Billy
*Brian Doyle-Murray as Harry
*Abe Vigoda as Judge Powell Charles Napier as Jed
*Sean Whalen as Carl Wayne Bishop
*Richard Edson as Skeets
*Richard Riehle as Principal Beasley
*Alex Datcher as Sarah
*Richard T. Jones as Nathan
  Jack McGee as Murphy, the security guard Sharon Barr as Libby Starling
*Nick Bakay as Richard Hurtz
*Ernie Lee Banks as Ray
*Shelley Winters as Mrs. Collins ("Mom")
*Dick Vitale as Hal Gibson
*Billie Bird as Rose William Newman as Judge DAngelo
*Jorge Luis Abreu as Jorge Siobhan Fallon as Heather
*Gregory Cooke as Reece Fishburne
*Mark L. Taylor as Russell Cadbury
 

== Production == Twelve Angry Men, which itself was adapted into film in 1957 starring Henry Fonda.

== Critical reception ==

At review aggregation site Rotten Tomatoes, the film received a score of 0%, based on 12 reviews with an average score of 2.4/10, as of July 26, 2014. 
 Movie and At the Movies, that Pauly Shore was the "cinematic equivalent of long fingernails, drawn very slowly and quite loudly over a gigantic blackboard" and noted that although his co-host, Gene Siskel, disliked Chris Farley, he would "rather attend a dusk-to-dawn Chris Farley film festival than sit through any 5 minutes of Jury Duty". Siskel agreed, referring to Shore as "aggravating".  Ebert estimated that Shores "appeal must be limited to people whose self-esteem and social skills are so damaged that they find humor, or at least relief, in at last encountering a movie character less successful than themselves". 
 Worst Actor for his performance in the film. 

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 

 