Dostana (2008 film)
 
 
{{Infobox film
| name           = Dostana
| image          = Dostana, 2008 film poster.jpg
| caption        = Theatrical release poster
| border         = yes
| director       = Tarun Mansukhani
| producer       = Hiroo Yash Johar Karan Johar
| story          = Tarun Mansukhani
| screenplay     = Tarun Mansukhani John Abraham Abhishek Bachchan Priyanka Chopra
| music          = Songs:  
| editing        =  Manan Sagar
| distributor    = Dharma Productions ShowMan Pictures Yash Raj Films
| released       =  
| runtime        = 142 minutes
| country        = India
| language       = Hindi
| budget         =  
| gross          =  (Share) 
}} John Abraham highest grossing film at the Indian box office in 2008. 

==Plot== John Abraham) run into each other while looking for an apartment to rent. They become enamoured of the same apartment; however, the owner refuses to sublet it to either one of them because her niece, Neha Melwani (Priyanka Chopra), will stay in the apartment alone and she does not want men living there with her. When a homosexual American soldier at a hot-dog stand mentions how much Sameer and Kunal remind him of his own relationship with his long-term partner, Sameer tries to persuade Kunal to pretend they are lovers so that they may live in the apartment. Kunal initially refuses but, desperate for the apartment, he decides to agree to it. Their plan works and they move in. Neha, Kunal and Sameer become good friends, and soon Kunal and Sameer develop feelings for Neha.

Nehas boss, Murli M (Boman Irani), resigns his job as the editor of Verve magazine and announces that he will soon appoint a successor. To impress him, Neha invites M for dinner at her house, telling him that she will introduce him to her homosexual friends. The evening starts off fun but dissolves into disaster when M discovers neither Sameer not Kunal is actually single. Sameers mother, who had received a letter at her house in London from the U.S. government confirming approval for Sameers application for domestic partnership with Kunal, abruptly brings the night to a close when she arrives at Nehas doorstep and discovers her sons homosexual proclivities. The next day at Nehas office, M announces that Abhimanyu Abhi Singh (Bobby Deol) is to be the new editor. Neha, deeply upset at being passed over for promotion, returns home where she is consoled by Sameers mother. In turn, Neha helps Sameers mother embrace her sons sexuality. Kunal and Sameer then help Neha with a project given to her by her new boss Abhi, with both Kunal and Sameer trying to impress Neha.

Sameer and Neha go shopping. While she is in the changing-room he musters the courage to tell her he loves her -- but she does not hear him. Instead, Kunal, who has s realised Sameers intentions, overhears Sameers confession and quickly puts a stop to his conversation with her, telling Sameer that he too loves Neha. Kunal hatches a plan, suggesting that Sameer meet him and Neha that night at a bar. This tricks Sameer into spending the night with Nehas aunt and her friends at a strip club while Kunal takes Neha on what he hopes to make a date -- though she perceives it as no more than a friendly outing that Sameer could not make it to, since she is convinced of their homosexuality. The next morning, Sameer is furious with Kunal, and the tension between the two baffles Neha.

Abhimanyu (nicknamed "Abhi") and Neha start dating, and Sameer and Kunal decide to team up and sabotage the relationship. Despite Sameer and Kunals efforts, Abhimanyu and Neha fall in love. Sameer and Kunal redirect their efforts into frightening Abhis ten-year-old son, Veer, about his future if his father marries Neha and Veer gains Neha as a stepmother. Neha discovers that Abhi plans to propose to her during a basketball game. Before half-time she pulls Kunal and Sameer aside and asks them for advice. The pair are shocked and tell her to say no, admitting that they are not homosexual and, in fact, are each in love with her. Meanwhile, Veer tells Abhi of his fears, begging his father not to marry Neha. During the game, Abhi hugs Neha and ends the relationship, citing Veers discomfort. Neha tearfully evicts Kunal and Sameer from her apartment and resigns from "Verve."

A few months later, Kunal and Sameer meet each other at court to collect their resident permits and realise that though they may not have had a romantic relationship with each other or with Neha, they had a wonderful friendship, which is incomplete without Neha. Kunal and Sameer reconcile. The two find Neha at a fashion show and try to apologise, but she refuses to see them. Abhi appears and Kunal asks when their wedding is. Neha tells him that they broke up because Veer was fearful of gaining her as a stepmother. Kunal and Sameer admit to their machinations, which infuriates both Neha and Abhi. Kunal and Sameer climb onto the stage and beg for Nehas forgiveness, with the crowd encouraging them and blowing kisses to Neha. The crowd ask the men to kiss each other, and they refuse. Neha and Abhi begin to turn away. At the last moment, Kunal forcibly kisses Sameer. Abhi is amazed and Neha, reluctantly impressed at the lengths the two will stretch themselves to in order to regain her trust, forgives them. Kunal and Sameer, desperate to secure Nehas happiness, each get down on one knee and propose to Abhi on behalf of Neha. Abhi, amused, agrees and he and Neha marry. 

Two months later, Neha, Kunal, and Sameer are knocking back beers on a sunny day in a Miami park. Neha playfully asks the two whether they might have felt anything for each other in the time they spent pretending to be homosexual. The men are defensive and Neha, tickled, takes her leave with a, "Sorry, touchy topic." Sam and Kunal, now alone, remember their kiss.

==Cast==
*Abhishek Bachchan as Sameer Kapoor (Sam) John Abraham as Kunal Chopra
*Priyanka Chopra as Neha Melwani
*Bobby Deol as Abhimanyu Singh, Nehas second boss (Cameo Appearance)
*Kiron Kher as Sameers mother
*Boman Irani as M aka Murli, Nehas first boss
*Sushmita Mukherjee as Nehas aunty
*Shilpa Shetty in a special appearance in the song "Shut Up & Bounce"

==Reception==

===Critical reception===
The film review site   makes note of the elaborate sequences and the possibility of the film becoming a financial success. However, it also mentions that the film is flawed in its depiction of homosexuality, calling it a "crucial blunder".  AOL India gave it 3 out of 5, with Noyon Jyoti Parasara calling Dostana "a fun film". 
 La Cage aux Folles ... is miles above Adam Sandlers Hollywood take on the topic".  Critic and author Maitland McDonagh in MissFlickChick.com, placing the film in a larger context, analysed that it "trades in stereotypes about swishy queers, shallow horndogs, frustrated career girls, guilt-wielding (s)mothers and overbearing aunties who need to chill out and get laid. And you know what? Its pretty damned funny and oddly subversive. Yes, everyone learns a lesson about tolerance without actually embracing an alternative lifestyle, but any movie that can finagle two macho movie studs into a full-on smooch that doesnt devolve into slapstick gay panic is venturing into risky territory". 

== Soundtrack ==
{{Infobox album  
| Name        = Dostana
| Type        = Album
| Artist      = Vishal-Shekhar
| Cover       = Dosalbum.jpg
| Released    =  30 September 2008 Feature film soundtrack
| Length      = 30:49
| Label       = Sony BMG
| Producer    = Vishal-Shekhar
| Last album = Bachna Ae Haseeno (2008)
| This album = Dostana (2008)
| Next album = Aladin (2009 film)|Aladin (2009)
}}
The films soundtrack was composed by Vishal-Shekhar. The album met with critical acclaim. According to the statistics published on IndiaFM, Dostana debuted at No. 4 on its Top Ten Soundtrack Sales Chart. Listeners registered on the website reviewed the album favourably and Dostanas music had an average rating of 4 stars out of 5.

{{tracklist
| headline            = Track listing
| music_credits       = no
| extra_column        = Artist(s)
| lyrics_credits      = yes
| total_length        = 30:49
| title1              = Jaane Kyun
| lyrics1             = Anvita Dutt Guptan
| extra1              = Vishal Dadlani
| length1             = 4:38
| title2              = Desi Girl
| lyrics2             = Kumaar
| extra2              = Shankar Mahadevan, Sunidhi Chauhan, Vishal Dadlani
| length2             = 5:07
| title3              = Maa Da Ladla
| lyrics3             = Kumaar
| extra3              = Master Saleem
| length3             = 4:05
| title4              = Shut Up & Bounce
| lyrics4             = Anvita Dutt Guptan
| extra4              = Vishal Dadlani, Sunidhi Chauhan
| length4             = 4:38
| title5              = Khabar Nahin
| lyrics5             = Anvita Dutt Guptan Amanat Ali
| length5             = 4:19
| title6              = Kuch Kum
| lyrics6             = Vishal Dadlani Shaan
| length6             = 5:41
| title7              = Maa Da Ladla
| lyrics7             = Kumaar
| extra7              = Master Saleem, Sunidhi Chauhan
| length7             = 4:02
| note7               = Mummy Mix
|}}

===Reception===
The soundtrack received generally favourable reviews. Joginder Tuteja of IndiaFM stated that "Dostana is an excellent album from Vishal-Shekhar. It does not carry a single number that doesnt work", giving the soundtrack 4 out of 5 stars. NDTV also gave the soundtrack 4 out of 5 stars, stating: "the soundtrack of the film will keep the cash registers ringing, with "Shut Up and Bounce" as an exception." Similarly, most music critics praised every track except "Shut Up and Bounce," which was a disappointment. Because of its familiar tune, the song drew comparisons with Kal Ho Naa Hos "Its the Time to Disco".

In contrast to IndiaFM and NDTV, Buzz18s Chandrima Pal gave the soundtrack 2.5 stars out of 5, stating that it "misses out on most of the spunk Vishal-Shekhar had in their previous works". Rediffs Sukanya Verma also gave the soundtrack 2.5 stars out of 5, stating that "although Dostanas music is nice and peppy for the most part, its nothing extraordinary." She added, Dostana "certainly wasnt the best from Dharma Productions". ApunKaChoice.com and The Times of India each rated the soundtrack a 3 out of 5. 

Later, a video Maa Da Laadla - Remix was released. It was also the ending theme for the movie.

===Controversy===
Pritish Nandy, owner of Pritish Nandy Communications, filed a court case against Dharma Productions for using the song "Kuch Kum" without his permission. He alleged that "Kuch Kum" was originally composed by Vishal-Shekhar for a different film to be directed by Tarun Mansukhani under his banner, but the project was shelved. When Vishal-Shekhar used "Kuch Kum"s composition in Dostana Pritish Nandy was "livid", "raising objections" against the use of the song. Dharma Productions settled the matter out of court, paying Nandy   in compensation. 

==Box office==
Dostana performed well both in India and overseas markets. Its opening weekend gross in the USA was $645,604 whilst in the UK, it was the third biggest opener behind Singh is Kinng and Race (2008 film)|Race  with £341,665. As of 21 December 2008, the film had grossed $1,444,246 after a 37-day run at the UK box office alone with its total production budget of $2 million being almost covered. In the USA, the film had a total gross of $1,243,910 as of 11 December 2008, whereas in countries such as Australia, South Africa, New Zealand, Fiji, Malaysia, Netherlands and India the total gross was $16,650,664. In India, (as of 12 December 2008) the film had earned $14,718,754 which was 82% of its total gross. As of 11 December 2008, the film had a grossed $17,894,574 (which is just over Rs. 850&nbsp;million) worldwide.    

==Awards==
Filmfare Award Best Movie Best Actor - Abhishek Bachchan Best Music Director- Vishal-Shekhar
International Indian Film Academy Awards
*Won -  
*Won -   
Stardust Awards
*Won -  
Star Screen Awards John Abraham

==Blu-ray release==
Yash Raj Films released Dostana on Blu-ray Disc|Blu-ray on 14 December 2009.

==Sequel==
In 2009 reports came out that director Tarun Mansukhani and Karan Johar are planning for a sequel of the film to cash in on its success, the sequel is titled Dostana 2. It was reported that along with the existing cast of the film (excluding Priyanka Chopra and Bobby Deol) Johar has roped in Katrina Kaif,  Arjun Rampal, Ritesh Deshmukh and Shreyas Talpade. Ritesh Deshmukh and Shreyas Talpade will play a real gay couple.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 