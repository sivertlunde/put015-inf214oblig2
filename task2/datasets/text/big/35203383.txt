The Police Murderer
 
{{Infobox film
| name           = The Police Murderer
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Peter Keglevic
| producer       = Hans Lönnerheden
| screenplay     = Rainer Berg Beate Langmaack
| based on       =  
| starring       = Gösta Ekman Kjell Bergqvist Rolf Lassgård
| music          = 
| cinematography = Wolfgang Dickmann
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 89 minutes
| country        = Sweden Germany
| language       = Swedish
| budget         = 
| gross          = 
}}
The Police Murderer ( ) is a 1994 Swedish police film about Martin Beck, directed by Peter Keglevic.

==Cast==
* Gösta Ekman as Martin Beck
* Kjell Bergqvist as Lennart Kollberg
* Rolf Lassgård as Gunvald Larsson
* Tomas Norström as Herrgott Nöjd
* Johan Widerberg as Kasper
* Anica Dobra as Kia
*   as Gunnar Danielsson
* Heinz Hoenig as Mård
* Jonas Falk as Stig Åke Malm
* Agneta Ekmanner as Greta Hjelm
* Stig Engström as Kaj Sundström
*   as Sigbrit Mård
*   as Mohlin
*   as Cecilia Sundström
* Petra Nielsen as Eva
*   as Mats
*   as Rick
* Mikael Persbrandt as Police Officer

==External links==
*  
*  

 

 
 
 
 
 
 