Tony (1982 film)
{{Infobox film|
| name = Tony
| image = 
| caption =
| director = H. R. Bhargava
| writer = H. R. Bhargava Lakshmi 
| producer = Mohan Sharma   Jairaj Singh
| music = Rajan-Nagendra
| cinematography = R. Chittibabu
| editing = Yadav Victor
| studio = Chaturbhuja
| released =  
| runtime = 123 minutes
| language = Kannada
| country = India
| budget =
}} 1982 Kannada Lakshmi and Srinath in lead roles. 

The film was a blockbuster with all the songs composed by Rajan-Nagendra considered evergreen hits. The same title name was reused in the 2013 feature film Tony (2013 film)|Tony.

== Cast ==
* Ambarish  Lakshmi
* Srinath 
* Sundar Krishna Urs
* Dinesh
* Shakti Prasad
* Shivaram
* Musuri Krishnamurthy
* Srilalitha
* Shobha
* Tiger Prabhakar in a guest role
* Uma Shivakumar in a guest role

== Soundtrack ==
The music was composed by Rajan-Nagendra with lyrics by R. N. Jayagopal and Doddarangegowda.  All the songs composed for the film were received extremely well and considered as evergreen songs.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Aanandave Mai Thumbide
| extra1 = S. P. Balasubramanyam, S. Janaki
| lyrics1 = Doddarangegowda
| length1 = 
| title2 = Cheluva Prathime Neenu
| extra2 = S. P. Balasubramanyam
| lyrics2 = R. N. Jayagopal
| length2 = 
| title3 = Preethi Beleyali
| extra3 = S. P. Balasubramanyam, S. Janaki
| lyrics3 = R. N. Jayagopal
| length3 = 
| title4 = Neeliya Baaninda
| extra4 = Ravi, Nagendra
| lyrics4= Doddarangegowda
| length4 = 
| title5 = Yaarige Yaaru Illa
| extra5 = Jayachandran
| lyrics5 = Doddarangegowda
| length5 = 
}}

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 


 

 