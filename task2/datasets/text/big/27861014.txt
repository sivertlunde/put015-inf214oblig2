À l'aventure
{{Infobox film
| name = À laventure
| image = 
| caption =
| director = Jean-Claude Brisseau
| producer =
| writer = Jean-Claude Brisseau
| starring = Carole Brana Étienne Chicot Lise Bellynck Nadia Chibani
| music = Jean Musy
| cinematography = Wilfrid Sempé
| editing = Lisa Heredia
| distributor =Axiom Films  
| released =  
| runtime = 90 minutes
| country = France
| language = French
| budget = 
}}
À laventure is a 2008 French film written and directed by Jean-Claude Brisseau. 

==Plot==
Sandrine, a young businesswoman, becomes dissatisfied with her ordinary life. She quits her job and leaves her boyfriend to embark on a number of erotic and bizarre adventures in hope of finding authenticity.

==Cast==
* Arnaud Binard as Greg
* Étienne Chicot as the man on the bench
* Jocelyn Quivrin as Fred
* Lise Bellynck as Sophie
* Carole Brana as Sandrine
* Nadia Chibani as Mina
* Estelle Galarme as Françoise
* Frédéric Aspisi as Jérôme
* Michèle Larue as Sandrines mother
* Manica Brini as sandrinas sister

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 
 