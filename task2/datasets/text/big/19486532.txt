Adhe Neram Adhe Idam
 
{{Infobox film
| name = Adhe Neram Adhe Idam
| image = 
| director = M. Prabhu
| writer = M. Prabhu Jai Vijayalakshmi Vijayalakshmi Rahul Madhav Nizhalgal Ravi Lollu Sabha Jeeva
| producer =  Hari Rama Krishnan
| music = Premji Amaran
| distributor = 
| released =  
| runtime = 
| country = India
| language = Tamil
| budget = 
}} Jai and Vijayalakshmi in lead roles. The film was released on 6 November 2009.

==Cast== Jai as Karthik Vijayalakshmi as Janani
* Rahul Madhav as Siva
* Nizhalgal Ravi
* Lollu Sabha Jeeva

==Soundtrack==
{{Infobox album
| Name        = Adhe Neram Adhe Idam
| Type        = soundtrack
| Artist      = Premji Amaran
| Cover       = Adhe Neram Adhe Idam.jpeg
| Background  = Gainsboro
| Released    =  
| Genre       = Soundtrack
| Length      = 24:26
| Label       = Think Music
| Producer    = Premji Amaran
| Last album  = Nannusire (2008)
| This album  = Adhe Neram Adhe Idam (2009)
| Next album  = Pookada Ravi (2009 hi)
}}

The soundtrack features 5 songs composed by Premji Amaran.

===Track listings===
# "Toshiba" (4:52) - Premji Amaran
# "Mudhal Murai" (4:59) - Harini, Tippu (singer)|Tippu, Haricharan
# "Vennilavu" (4:50) - Saindhavi, Vijay Yesudas
# "Athu Oru Kaalam" (4:39) - Haricharan, Premji Amaran
# "Nammoru Chennaiyila" (5:04) - Venkat Prabhu

 
 
 
 
 


 