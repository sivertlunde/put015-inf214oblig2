The Great Mountain Biking Video
 , 1987 - Photo by Rolf Schulze]]

The Great Mountain Biking Video is a mountain biking instructional videotape, produced in 1987 and released in 1988 by San Diego, USA video production company New & Unique Videos. 

== History ==

Patty Mooney    and Mark Schulze, an early user of the Helmet camera, learned about the sport of mountain biking while in Canada in 1986. In 1987, the couple produced the mountain biking video, "The Great Mountain Biking Video."   
The video featured mountain-bike racing professionals Ned Overend, Martha Kennedy, John Tomac, Julia Ingersoll, Tinker Juarez, Trials rider, Kevin Norton and Olympian John Howard (cyclist) who instructed viewers on how to properly ride a mountain bike.

The video was favorably reviewed by Video Choice,   The Video Rating Guide for Libraries,  and    

The Great Mountain Bike Video was one of the first instructional videos demonstrating mountain biking techniques, and also includes early use of the helmet camera.   

== References ==

 

*"An Athletes Guide to Cycling - Biking Videos" Detroit Metropolitan Woman, August 1994, p.&nbsp;61
*"Bikes, Races & Videotape" by Robert Hecker, California Bicyclist, November 1989, p.&nbsp;33
*"Cut & Print" by Jeff Stinchcomb, San Diego Pedal Guide, November/December 1994, p.&nbsp;8
*"El Gran Video de Ciclismo de Montana," Patria de Miami, Noviembre 21 de 1989, p.&nbsp;7
*"From New & Unique Videos," Publishers Marketing Association Newsletter, May 1989, p.&nbsp;10
*"Great Mountain Biking," Bicycle Business Journal, February 1989, p.&nbsp;39
*"Guide to Off-Road Videos," Mountain Bike Action, April 1991, p.&nbsp;127
*"Mountain Bike Video," Bicycling San Diego, Autumn 1989, p.&nbsp;9
*"Mountain Biking Books and Videos," Shape (magazine), April 1991, p.&nbsp;59
*"Mountain Biking Just Gets Better and Better" by Patricia Mooney, Womens Sports & Fitness, May 1989
*"New & Unique Video," American Bicyclist & Motorcyclist, September/October 1989, p.&nbsp;110
*"The Great Mountain Biking Video" by Bob Babbitt, Competitor Magazine, November 1988, p.&nbsp;4
*"The Great Mountain Biking Video" Review by Jamie Elvidge, Southwest Cyclist, March 1989, p.&nbsp;22
*"The Great Mountain Biking Video" Review by Sue-Ellen Beauregard, Booklist, May 1, 1989, p.&nbsp;1565
*"The Great Mountain Biking Video" Review, Bicycle Retailer and Industry News, September 1992, p.&nbsp;41
*"Two For the Trail", Mountain & City Biking, November 1989, p.&nbsp;80
*"Video Couple Ride Into Sunset Together, OC/SD Film & Video News, May 1990, p.26
*"Video Guide to Mountain Biking," American Bicyclist & Motorcyclist, March 1989, p.&nbsp;79
*"Videos," Kashi Bicycling and Adventure Travel Exposition, February 19, 1989, p.&nbsp;7
*"Winter Rechargers," Cyclist Magazine, December 1988, p.&nbsp;72
* 
* 
* 
* , James M. Craddock, Thomson Gale
* , Marquis Whos Who, 1989

== External links ==

 
 
 
 
 
 
 
 