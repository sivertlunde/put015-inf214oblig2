Il signor Max
{{Infobox film
| name           = Il Signor Max
| image          = IlsignorMax.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Mario Camerini
| producer       = C.O. Barbieri 
| writer         = Amleto Palermi (story) Mario Camerini (screenplay)
| narrator       = 
| starring       = Vittorio De Sica  Renzo Rossellini
| cinematography = Anchise Brizzi
| editing        = Mario Camerini Giovanna Del Bosco  
| distributor    = Ente Nazionale Industrie Cinematografiche
| released       = 5 October 1939 (US)
| runtime        = 84 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1937 Italy|Italian "Telefoni Bianchi|white-telephone" comedy film directed by Mario Camerini.

==Plot==
In Milan, Gianni is a young and poor newspaper seller who makes a dull and boring life. But Gianni is a friend of a wealthy aristocrat of the city which is called Count Max Varaldo, exactly equal to the poor sellers physical. Usually Count Max invites Gianni in his palace, because he likes to take an interest in the affairs of the people, and also because Gianni makes him laugh with his jokes. One day Count Max has to take a trip on a cruise, and leaves control of his palace to Gianni, who takes advantage. In fact the lover of Count Max, Lauretta, believes that Gianni is the noble, his boyfriend, and so the poor newspaper seller initiates a series of misunderstandings.

==Cast==
*Vittorio De Sica as Gianni/Max Varaldo 
*Assia Noris as Lauretta 
*Rubi Dalma as Donna Paola 
*Umberto Melnati as Riccardo 
*Lilia Dale as Pucci (billed as Adonella) 
*Virgilio Riento as Pepe 
*Mario Casaleggio as Uncle Pietro 
*Caterina Collo as Aunt Lucia 
*Ernesto Ghigi  as Pierino 
*Romolo Costa as Commandante Baldi 
*Lilia Silvi as the orchard seller
*Giuseppe Pierozzi as a taxi driver
*Albino Principe as Bubi Bonci 
*Clara Padoa  as Jeanne, the athlete on the train
*Luciano Dorcoratto as the guide 
*Desiderio Nobile as the major 
*Armando Petroni as the pharmacist  
*Edda Soligo as a girl at the ball 
*Gianfranco Zanchi as the real Max Varaldo

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 
 