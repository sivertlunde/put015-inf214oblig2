This Week of Grace
 
{{Infobox film
| name           = This Week of Grace
| image          =
| caption        =
| director       = Maurice Elvey
| producer       = Julius Hagen Jack Marks   Nell Emerald   Maurice Braddell Henry Kendall John Stuart
| music          = Percival Mackey
| cinematography = Sydney Blythe
| editing        = 
| studio         = Twickenham Studios
| distributor    = Radio Pictures
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
}} British comedy Henry Kendall John Stuart. housekeeper at the estate of a wealthy duchess. It was promoted with the tagline "Cinderella in modern dress".  It is notable for its songs written by Harry Parr-Davies, including "My Lucky Day" and "Happy Ending". 

==Plot==
Grace Milroy loses her job working at a factory. However, through a strange set of circumstances, she is taken on as housekeeper at the nearby Swinford Castle the home of the eccentric Duchess of Swinford. She is initially cold received by the other staff but she soon wins them over with her personality and hard work. While working there she falls in love with the Duchess nephew, Viscount Swinford and eventually marries him. Later when she wrongly believes him to have married her under the mistaken impression she is rich she leaves him and goes to take a job on the stage working in the chorus line. Eventually the misunderstanding is cleared up and the couple reconcile.

==Production==
The film was made by Twickenham Studios following a dispute between Radio Pictures, who owned the rights to Fields, and Associated Talking Pictures (ATP) who had previously made her films.  It was part of an attempt by Twickenham to move away from making Quota quickies towards more high-budget quality productions a strategy that continued until the bankcruptcy of its owner Julius Hagen. As the sound stage at Twickenham was already booked, filming was done at Ealing Studios, ironically owned by ATP.

==Reception==
The film is one of the least well-known of Fields work. It has been noted for its promotion of a national consensus between classes the first time this had been featured in a Fields film. It was theme which was to become a cornerstone of her work during her years of mainstream popularity.  It was well-received on its release with Kine Weekly observing that the film consolidated Fields as "Englands premier entertainer". 

==Cast==
* Gracie Fields - Grace Milroy Henry Kendall - Lord Clive Swinford John Stuart - Henry Baring
* Frank Pettingell - Mr Milroy
* Minnie Rayner - Mrs Milroy
* Douglas Wakefield - Joe Milroy
* Vivian Foster - Vicar
* Marjorie Brooks - Pearl Forrester
* Helen Haye - Lady Warmington
* Nina Boucicault - Duchess of Swinford

==Preservation status==
Thought to have been Lost film|lost, it was loaned to the British Film Institute as a result of its 2010 search for missing films, and a copy was made for the National Archive. 

==References==
 

==Bibliography==
* Richards, Jeffrey. The Age of the Dream Palace. Routledge & Kegan, 1984.
* Richards, Jeffrey (ed.). The Unknown 1930s: An Alternative History of the British Cinema, 1929- 1939. I.B. Tauris & Co, 1998.
* Shafer, Stephen C. British popular films, 1929-1939: The Cinema of Reassurance. Routledge, 1997.

 

==External links==
* 

 
 
 
 
 