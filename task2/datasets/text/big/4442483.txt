Fatty Finn (film)
 
{{Infobox film
| name           = Fatty Finn
| image          =
| image_size     =
| caption        = Maurice Murphy
| producer       = Brian Rosen
| writer         = Bob Ellis
| narrator       = Martin Lewis
| music          = Grahame Bond Rory ODonoghue
| cinematography =
| editing        = studio = Childrens Film Corporation Australian Film Commission Finn Productions
| distributor    = Roadshow
| released       = 18 December 1980
| runtime        = 91 minutes
| country        = Australia
| language       = English
| budget         = $350,000 
| gross          = AU $1,064,000 (Australia) 
| awards         =
}} Maurice Murphy and starring Ben Oxenbould with Rebecca Rigg. It is based on the 1930s cartoon-strip character, Fatty Finn, created by Syd Nicholls and is loosely based on the 1927 silent film, The Kid Stakes.

==Plot== Woolloomooloo in Sydney, New South Wales in 1930, the neighbourhood nice guys are led by Fatty (real name Hubert Finn), an ambitious 10 year old with an eye for making a Australian pound|quid. From shady frog jumping contests to a fixed goat race, Fatty uses his enterprise to raise enough money to buy a crystal set (radio without a separate power supply) thats worth seventeen shillings & sixpence (17/6), more than his Dad earns in a year. Bruiser Murphy the bully and his gang try to stop him. Fatty uses his brains against his enemies brawn to eventually triumph.

==Cast== Hubert Fatty Finn
* Rebecca Rigg as Tilly
* Jeremy Larsson as Headlights Martin Lewis as Skeet
* Hugo Grieve as Seasy
* Sandy Leask as Lolly Legs
* Greg Kelly as Bruiser Murphy
* Christopher Norton as Snootle
* Bert Newton as John Finn
* Noni Hazelhurst as Myrtle Finn Gerard Kennedy as Tiger Murphy
* Su Cruickshank as Mrs Murphy
* Lorraine Bayly as Maggie McGrath Bill Young as Officer Claffey
* Ross Higgins as Radio Announcer
* Peter Rowley as Chauffeur

==Production==
Screenwriter Bob Ellis says it was his idea to make the film. He complained about interference from the films producers, John Sexton and Yoram Gross, claiming Sexton in particular wanted a lot of changes, but changed his mind after David Puttnam praised Ellis original draft. He later said of the film that "all the performances are dreadful, the conspicuous exception being Bert Newtons." 

Of the $350,000 budget, $120,000 came from the AFC.

The movie was set in Woolloomooloo but the area had changed a lot since then so was shot in and around Glebe, New South Wales|Glebe. Filming took place in January and February 1980. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p343 

==Reception==

===Box Office===
Fatty Finn grossed $1,064,000 at the box office in Australia,  which is equivalent to $3,777,200
in 2009 dollars.

===Awards=== Australian Film Institute Awards in 1981 winning in the categories of Best Achievement in Costume Design and Best Original Music Score. 

==See also==
* Cinema of Australia

==References==
 

==External links==
* 
*  at Oz Movies
* 
* 
*  at Peter Malone
 
 

 
 
 
 
 
 