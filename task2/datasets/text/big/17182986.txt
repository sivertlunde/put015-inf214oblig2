Hearts at Sea
{{Infobox film
| name           = Cuori sul mare
| image          = Cuori sul mare doris dowling giorgio bianchi 002 jpg qzdh.jpg
| image_size     = 
| caption        = 
| director       = Giorgio Bianchi
| producer       = 
| writer         = Nicola Manzari
| narrator       = 
| starring       = 
| music          =Enzo Masetti 
| cinematography = Mario Craveri 
| editing        = 
| distributor    =  1950
| runtime        = 
| country        = Italy Italian
| budget         = 
}}
 1950 Cinema Italian adventure film directed by Giorgio Bianchi. Sophia Loren appears as an uncredited extra.   

==Cast==
* Doris Dowling - Doris 
* Jacques Sernas - Paolo Silvestri 
* Milly Vitale - Fioretta 
* Charles Vanel - Nurus 
* Marcello Mastroianni - Massimo Falchetti 
* Paolo Panelli - Un marinaio 
* Gualtiero Tumiati
* Enzo Biliotti
* Nicola Morabito
* Aldo Fiorelli
* Mimi Aylmer
* Dina Perbellini
* Sophia Loren - Extra (uncredited) 

==References==
 

== External links ==
*  

 
 
 
 
 
 

 