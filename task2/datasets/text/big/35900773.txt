Jalpari: The Desert Mermaid
{{Infobox film
| name           = Jalpari: The Desert Mermaid
| image          = Jalpari_film_poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Nila Madhab Panda
| producer       = Sushilkumar Agrawal
| based on       =
| screenplay     = Deepak Venkateshan
| writer         =
| starring       = Lehar Khan  Parvin Dabas   Harsh Mayar  Tannishtha Chatterjee  Rahul Singh  Krishang Trivedi
| music          = Midival Punditz / Deepak Pandit
| cinematography = 
| editing        = Apurva Asrani
| distributor    = Ultra Distributors
| released       =  
| runtime        = 100 Minutes 
| country        = India
| language       = Hindi
}} National Film best child Rahul Singh, Suhasini Mulay and V.M. Badola are on supporting roles. 
 female foeticide is an adventure film, which is screened at Marche du Cannes film festival. 

The movie has been nominated at the prestigious Asia Pacific Screen Awards (APSA). The sixth APSA ceremony will be organised at Queensland, Australia on November 23, 2012. 
The movie has been sent to the Academy of Motion Pictures as a direct entry for the Oscar Awards in the foreign film category. {{cite web|url=http://timesofindia.indiatimes.com/entertainment/bollywood/news-interviews/Jalpari-goes-to-the-Oscars/articleshow/17305297.cms|title=
Jalpari goes to the Oscars |accessdate=2012-11-21}} 
Recently the film received the prestigious Audience Choice Award in Minsk International Film Festival in Belarus & selected as an official entry for International Film Festival in Montreal which will take place in March 2013. The film also won MIP Junior Kids Jury Award at Cannes in October 2012.

==Plot==
A well-heeled family from tony neighborhood of New Delhi travels from the city through dusty roads that lead into the Madhogarh village, district Mahendragarh in Haryana, far out and buried in the debris of the past. Shreya, the tomboy of the family, finally arrives at her father’s village for the first time during her vacations, that too after coaxing her father, Dev, to the best of her abilities. She and her brother, Sam, had, in their imagination, spun the village right out of a fairytale, replete with streams, lakes and grasslands that will allow them to run free. But all they find are dusty alleys, dried up ponds and hostile playmates until they spin their magic and befriend this unknown place, especially some of its people, like the Pehelwan and the gang of kids led by Ajithe. The story revolves around Shreya and Sam’s adventures and misadventures which turn this dull place bereft of water into a land of enchantment, mischief and a million exciting exploits. But unknown places have many secrets, and here too, secret lurk at every corner. Strangely behaving villagers, a witch whom everyone seems to be terrified of, and a no-access zone beyond the hills intrigue Shreya, more so after housemaid Shabri tells her various mysterious stories about the village. Then, one night, Shreya sees Shabri and her husband Trilochan slink away, and starts following them, only to find out a horrifying secret the village harbors. At the end they were all shocked to know that they all kill the girl child before they born and they were scaring all the village at the name of a witch

==Cast==
* Lehar Khan as Shreya
* Parvin Dabas as Dev
* Harsh Mayar 
* Krishang Trivedi as Sam
* Tannishtha Chatterjee as Shabri (Maid) Rahul Singh
* Suhasini Mulay as Dadi
* V.M. Badola
* Ms. Madhulika Sen - Cameo as School Principal

===Release===
This film was released on August 31, 2012. 

== References ==
 

==External links==
*  

 
 
 
 