Grievous Bodily Harm
 
 

{{Infobox Film name           = Grievous Bodily Harm  image          =  caption        =  producer       = Richard Brennan director       = Mark Joffe  writer         = Warwick Hind starring  John Waters Bruno Lawrence music  Chris Neal cinematography = Ellery Ryan editing        = Marc van Buuren distributor    =   released       = 1988 runtime        = 96 minutes country        = Australia language       = English  budget         = AU $3.4 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p238-240  gross = $82,267 (Australia) $1 million (US sale)  
}} John Waters.

==Plot==
Crime reporter Tom Stewart (Colin Friels) and a cop (Bruno Lawrence) look for a deranged schoolteacher (John Waters) who goes on a murder spree while looking for the lover he thought to be dead.

==Cast==
*John Waters as Morris Martin
*Colin Friels as Tom Stewart
*Bruno Lawrence as Det. Sgt. Ray Birch
*Kim Gyngell as Mick
*Gary Stalker as Derek Allen
*Joy Bell as Claudine
*Shane Briant as Stephen Enderby
*Caz Lederman as Vivian Enderby
*John Flaus as Neil Bradshaw

==Production==
The script was written by Warwick Hind, a former executive at Greater Union. Errol Sullivan showed the script to Richard Brennan, who raised up to around a $1 million of the budget; the remainder was raised through Antony I. Ginnane. Richard Brennan says the actual cost of the film was $3 million but various fees put it up to $3.4 million. 

==Awards== AFI Awards in 1988, including best picture.

==Box Office==
Grievous Bodily Harm grossed $82,267 at the box office in Australia.  However it did sell to American company Fries Entertainment for over $1 million. 

==See also==
*Cinema of Australia

==References==
 

==Further reading==
* 

==External links==
* 
*Grievous Bodily Harm at the  
 

 
 
 
 
 
 


 
 