Revolutionary Road (film)
{{Infobox film
| name = Revolutionary Road
| image = Revolutionary road.jpg
| border = yes 
| caption = Theatrical release poster
| director = Sam Mendes
| producer = Bobby Cohen Sam Mendes Scott Rudin
| based on =  
| screenplay = Justin Haythe
| starring = Leonardo DiCaprio Kate Winslet Michael Shannon Kathryn Hahn David Harbour Kathy Bates Ty Simpkins
| music = Thomas Newman
| cinematography = Roger Deakins Tariq Anwar
| studio =  DreamWorks Pictures BBC Films
| distributor = Paramount Vantage  (United States)  United International Pictures  (Europe) 
| released =  
| runtime = 119 minutes
| country = United States United Kingdom
| language = English
| budget = $35 million
| gross = $75.2 million  . Box Office Mojo. Retrieved 2012-08-16. 
}} 1961 novel Richard Yates, Golden Globe Golden Globes, BAFTAs and three 81st Academy Awards|Oscars.

The film premiered in Los Angeles on 15 December 2008, followed by a limited U.S. release on 26 December 2008 and a wide U.S. release on 23 January 2009. In most other countries it was released between 15 and 30 January 2009.

==Plot==
In the late 1940s, Frank Wheeler (Leonardo DiCaprio) meets April (Kate Winslet) at a party. He is a longshoreman, hoping to be a cashier; she wants to be an actress. Frank later secures a sales position with Knox Machines, for which his father worked for twenty years, and he and April marry. The Wheelers move to 115 Revolutionary Road in suburban Connecticut when April becomes pregnant.

The couple becomes close friends with their realtor Helen Givings (Kathy Bates) and her husband Howard Givings (Richard Easton), and neighbor Milly Campbell (Kathryn Hahn) and her husband Shep (David Harbour). To their friends, the Wheelers are the perfect couple, but their relationship is troubled. April fails to make a career out of acting, while Frank hates the tedium of his work. Meanwhile, Helen asks the couple to meet her son, John (Michael Shannon), who had been declared insane, to try to help better his condition. They accept.

April wants new scenery and a chance to support the family so that Frank can find his passion, and so suggests that they move to Paris to start a new life away from the "hopeless emptiness" of their repetitive lifestyle.

The Givings (including John) talk with April and Frank. During the conversation, the Wheelers tell the Givings about their plans to live in Paris. Surprisingly, the only person that seems to comprehend their decision is John.

As the couple prepares to move, they are forced to reconsider. Frank is offered a promotion, and April becomes pregnant again. When Frank discovers she is contemplating having an abortion, he is furious and starts screaming at April, leading to a serious altercation, in which April says that their first child was a "mistake".

The next day, Frank takes the promotion and tries to accept his uneventful life. At the end of an evening at a jazz bar with the Campbells, Shep and April end up alone together and have sex in the car. Shep professes his long-held love for April, but she rejects his interest.

The following day, Frank confesses to having had an affair with a female assistant at his office, hoping to reconcile with April. To his surprise, April responds apathetically and tells him it does not matter as her love for him has gone, which he does not believe. The Givingses come over for dinner, and Frank announces to the guests that their plans have changed, as April is pregnant. John harshly lambasts Frank for crushing Aprils hope as well as his acceptance of his circumstances. Angered, Frank nearly attacks John, and the Givings hurry out. Afterwards, Frank and April have a severe verbal altercation, after which April flees the house.

Frank spends the night in a drunken stupor but is shocked to find April in the kitchen the next morning, calmly making breakfast. Aprils mood seems to have improved, but after bidding goodbye to Frank, she breaks down and prepares to perform her own vacuum aspiration abortion, which proves fatal. Shep goes to the hospital to support Frank, who hysterically tells him, "she did it to herself." April dies in the hospital due to complications following the abortion.

Frank moves to the city and starts selling computers. He spends all of his extra time with his children. A new couple, the Braces, buy the house and we hear Milly telling the story of the Wheelers to them. Shep stops the story and walks out of the house, crying.

Helen talks to her husband years later about how the Braces seem to be the best-suited couple for the Wheelers old house. When her husband mentions the Wheelers, Helen starts to talk about why she didnt like them. As she continues talking about all of the things that she didnt like about them, her husband turns off his hearing aid.

==Cast==
*Leonardo DiCaprio as Frank Wheeler
*Kate Winslet as April Wheeler
*Dylan Baker as Jack Ordway
*Zoe Kazan as Maureen Grube
*Kathy Bates as Helen Givings
*Michael Shannon as John Givings, Jr.
*Kathryn Hahn as Milly Campbell
*David Harbour as Shep Campbell
*Max Casella as Ed Small
*Richard Easton as Howard Givings
*Jay O. Sanders as Bart Pollock
*Ryan Simpkins as Jennifer Wheeler
*Ty Simpkins as Michael Wheeler

==Development== The Manchurian The Godfather Patrick ONeal. The actor loved the book and spent the rest of his life trying to finish a workable screenplay.  Yates read ONeals treatment of his novel and found it "godawful", but ONeal refused the writers repeated offers to buy back the rights. Yates died in 1992, ONeal two years later. 
 Little Children David Thompson eventually purchased the rights for BBC Films.    In March 2007, BBC Films established a partnership with DreamWorks, and the rights to the films worldwide distribution were assigned to Paramount Pictures, owner of DreamWorks. On 14 February 2008, Paramount announced that Paramount Vantage was "taking over distribution duties on Revolutionary Road".    The BBC hired Justin Haythe to write the screenplay because, according to the screenwriter, he was "hugely affordable". 

Kate Winslet sent producer Scott Rudin the script and he told her that her then husband, director Sam Mendes, would be perfect to direct it.  She gave Mendes Yates novel and told him, "I really want to play this part".    He read Haythes script and then the book in quick succession. Haythes first draft was very faithful to the novel, using large parts of Yates own language, but Mendes told him to find ways to externalize what Frank and April do not say to each other. 

Once Leonardo DiCaprio agreed to do the film, it went almost immediately into production.  DiCaprio said that he saw his character as "unheroic" and "slightly cowardly" and that he was "willing to be just a product of his environment".    DiCaprio prepared for the role by watching several documentaries about the 1950s and the origin of suburbs. He said that the film was not meant to be a romance and that he and Winslet intentionally avoided films that show them in romantic roles since Titanic (1997 film)|Titanic.  Both DiCaprio and Winslet were reluctant to make films similar to Titanic because "we just knew it would be a fundamental mistake to try to repeat any of those themes".    To prepare for the role, Winslet read The Feminine Mystique by Betty Friedan.   

Mendes had the cast rehearse for three-and-a-half weeks before principal photography and shot everything in sequence and on location.    Actor Michael Shannon said that he did not feel that on the set of the film there were any stars, but "a group of people united by a passion for the material and wanting to honor the book".      He said that Winslet and DiCaprio could only make such a good performance as a couple because they had developed a friendship since their work on Titanic. For Shannon, it was more important to prepare for the moment when he walked on the set than being concerned about the movie stars he was working with.  On the fight scenes between him and Winslet, DiCaprio said, "So much of what happens between Frank and April in this film is whats left unsaid. I actually found it a real joy to do those fight scenes because finally, these people were letting each other have it."  The shoot was so emotionally and physically exhausting for DiCaprio that he postponed his next film for two months. 
 claustrophobic dynamic and shot all of the Wheeler house interiors in an actual house in Darien, Connecticut|Darien, Connecticut. DiCaprio remembers, "it was many months in this house and there was no escaping the environment. I think it fed into the performances." They could not film in a period accurate house because it would have been too small to shoot inside.    Production Designer Kristi Zea is responsible for the "iconic, nostalgic images of quaint Americana", although she says that was "absolutely the antithesis of what we wanted to do".  Zea chose for the set of this film furnishings that "middle-class America would be buying at that time". 

During the post-production phase, Mendes cut 18&nbsp;scenes, or 20&nbsp;minutes to achieve a less literal version that he saw as more in the spirit of Yates novel. 

==Release==
Revolutionary Road had a limited release in the United States at three theatres on December 26, 2008, and a wide release at 1,058&nbsp;theatres on January 23, 2009. Revolutionary Road has earned $22.9&nbsp;million at the domestic box office and $51.7&nbsp;million internationally for a worldwide total of $74.6&nbsp;million. 

===Critical reception===
Revolutionary Road has received generally positive reviews from critics.

Kenneth Turan of the Los Angeles Times said:
 
 New York Daily News said:
 

Roger Ebert of the Chicago Sun-Times gave Revolutionary Road four stars out of four, commending the acting and screenplay and calling the film "so good it is devastating". He said of Winslet and DiCaprio, "they are so good, they stop being actors and become the people I grew up around." 
 Pinter at Strangers When We Meet because of its "narrow vision", even arguing that the television series Mad Men handles the issues of conformity, frustration, and hypocrisy "with more panache and precision". 

David Ansen of Newsweek said the film "is lushly, impeccably mounted—perhaps too much so. Mendes, a superb stage director, has an innately theatrical style: everything pops off the screen a little bigger and bolder than life, but the effect, rather than intensifying the emotions, calls attention to itself. Instead of losing myself in the story, I often felt on the outside looking in, appreciating the craftsmanship, but one step removed from the agony on display. Revolutionary Road is impressive, but it feels like a classic encased in amber." 

Owen Gleiberman of Entertainment Weekly graded the film B+ and commented:
 
 American Beauty... Once more, the suburbs are well-upholstered nightmares and its denizens clueless—other than one estranged male. Clearly, this environment attracts the dramatic sensibilities of this theater-trained director. Everything is boldly indicated to the audience from arch acting styles to the wink-wink, nod-nod of its design. Indeed his actors play the subtext with such fury that the text virtually disappears. Subtlety is not one of Mendes strong suits." 

Rex Reed of The New York Observer called the film "a flawless, moment-to-moment autopsy of a marriage on the rocks and an indictment of the American Dream gone sour" and "a profound, intelligent and deeply heartfelt work that raises the bar of filmmaking to exhilarating." 

Peter Travers of Rolling Stone called the film "raw and riveting" and commented, "Directed with extraordinary skill by Sam Mendes, who warms the chill in the Yates-faithful script by Justin Haythe, the film is a tough road well worth traveling . . . DiCaprio is in peak form, bringing layers of buried emotion to a defeated man. And the glorious Winslet defines what makes an actress great, blazing commitment to a character and the range to make every nuance felt." 

Mick LaSalle of the San Francisco Chronicle voted the film as his best of 2008. He commented, "Finally, this is a movie that can and should be seen more than once. Watch it one time through her eyes. Watch it again through his eyes. It works both ways. It works in every way. This is a great American film." 

It holds a 68% rating from critics on review aggregate website Rotten Tomatoes, based on 203 reviews, with the consensus being "Brilliantly acted and emotionally powerful, Revolutionary Road is a handsome adaptation of Richard Yates celebrated novel".  Metacritic lists it with a 69 out of 100, which indicates "generally favorable reviews", based on 38 reviews. 

===Top ten lists===
The film appeared on several critics top ten lists of the best films of 2008.     
* 1st – Mick LaSalle, San Francisco Chronicle
* 2nd – Rex Reed, New York Observer New York Daily News
* 6th – Peter Travers, Rolling Stone
* 7th – Lou Lumenick, New York Post
* 8th – James Berardinelli, ReelViews David Denby, The New Yorker
* Roger Ebert, Chicago Sun-Times (Ebert gave an alphabetical top 20 list)

==Accolades==
{| class="wikitable" style="font-size:100%"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Awards 
|- style="text-align:center;"
! style="background:#ccc;"| Award
! style="background:#ccc;"| Category
! style="background:#ccc;"| Recipient(s)
! style="background:#ccc;"| Outcome
|- Academy Awards Best Art Direction
| Debra Schutt and Kristi Zea
|  
|- Best Costume Design
| Albert Wolsky
|  
|- Best Supporting Actor
| Michael Shannon
|  
|- style="border-top:2px solid gray;" BAFTA Awards Best Actress
| Kate Winslet
|  
|- Best Costume Design
| Albert Wolsky
|  
|- Best Production Design
| Debra Schutt and Kristi Zea
|  
|- Best Adapted Screenplay
| Justin Haythe
|  
|- style="border-top:2px solid gray;" Costume Designers Guild
| Best Costume Design – Period Film
| Albert Wolsky
|  
|- style="border-top:2px solid gray;" Golden Globe Awards Best Motion Picture – Drama
|
|  
|- Best Actor – Motion Picture Drama
| Leonardo DiCaprio
|  
|- Best Actress – Motion Picture Drama
| Kate Winslet
|  
|- Best Director – Motion Picture
| Sam Mendes
|  
|- style="border-top:2px solid gray;" 29th London Film Critics Circle Awards
| Actress of the Year The Reader)
|  
|- style="border-top:2px solid gray;"
| Palm Springs International Film Festival
| Ensemble Cast 
| Leonardo DiCaprio, Kate Winslet, Michael Shannon, Kathryn Hahn, David Harbour, Kathy Bates, Dylan Baker and Zoe Kazan
|  
|- style="border-top:2px solid gray;" Screen Actors Guild Outstanding Performance by a Female Actor in a Leading Role – Motion Picture
| Kate Winslet
|  
|- style="border-top:2px solid gray;" Satellite Awards
| Top 10 Films of 2008
|
|  
|- Best Art Direction and Production Design
| Kristi Zea, Teresa Carriker-Thayer, John Kasarda, and Nicholas Lundy
|  
|- Best Film – Drama
|
|  
|- Best Actor – Motion Picture Drama
| Leonardo DiCaprio
|  
|- Best Adapted Screenplay
| Justin Haythe
|  
|- Best Supporting Actor – Motion Picture
| Michael Shannon
|  
|- style="border-top:2px solid gray;"
| St. Louis Gateway Film Critics Association Awards 2008|St. Louis Gateway Film Critics Association
| Best Actress
| Kate Winslet
|  
|- style="border-top:2px solid gray;" Vancouver Film Critics Circle
| Best Actress
| Kate Winslet (also for The Reader)
|  
|}
According to Academy rules, an actor or actress may receive only one nomination per category, and due to the difference in rules between the Golden Globes and Academy Awards, Kate Winslets role in The Reader was considered a leading one by the Academy, while the Golden Globe regarded it a supporting role. So, as Winslets performance in The Reader had been nominated for Best Actress in 2008 by the Academy, her performance in Revolutionary Road couldnt be nominated. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 