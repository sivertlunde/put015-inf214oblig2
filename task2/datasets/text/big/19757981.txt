The Big Premiere
{{Infobox film
| name           = The Big Premiere
| image          = Bigpremiere TITLE.JPEG
| image_size     =
| caption        = Title card Edward Cahn
| producer       = Jack Chertok Richard Goldstone for MGM
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       = David Snell
| cinematography = Paul Vogel
| editing        = Adrienne Fazan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 10 30"
| country        = United States
| language       = English
| budget         =
}}
 short comedy Edward Cahn.  It was the 188th Our Gang short (189th episode, 100th talking short, 101st talking episode, and 20th MGM produced episode) that was released.

==Plot==
The gang unintentionally wreaks havoc at the gala Hollywood premiere of the adventure epic "Gun Boats". Chased away by the angry authorities, the undaunted kids decide to stage their own movie premiere—and they even film a movie for the occasion. Alas, the gangs cinematic effort, entitled The Mysteeryus Mystery, is not quite as entertaining as the efforts by Buckwheat to remove his feet from a block of cement {{cite news
|url=http://movies.nytimes.com/movie/226197/The-Big-Premiere/overview |title=New York Times: The-Big-Premiere|accessdate=2008-10-08|work=NY Times | first=Lawrence | last=Van Gelder}} 

==Cast==
===The Gang=== Mickey Gubitosi as Mickey
* Darla Hood as Darla
* George McFarland as Spanky
* Carl Switzer as Alfalfa
* Billie Thomas as Buckwheat
* Shirley Coates as Muggsy
* Darwood Kaye as Waldo

===Additional cast===
* John Dilson as Theater owner
* Eddie Gribbon as Officer
* Ethelreda Leopold as Irma Acacia

===Barn extras===
Giovanna Gubitosi, James Gubitosi, Larry Harris, Arthur Mackey, Tommy McFarland, Harold Switzer, Clyde Wilson

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 