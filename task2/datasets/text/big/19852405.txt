Aux yeux du souvenir
{{Infobox film
| name           = Aux yeux du souvenir
| image          = Aux yeux du souvenir film poster.jpg
| image size     =
| caption        =
| director       = Jean Delannoy
| producer       =
| writer         = Jean Delannoy Henri Jeanson Georges Neveux
| narrator       =
| starring       = Michèle Morgan  Jean Marais
| music          = Georges Auric
| cinematography = Robert Lefebvre
| editing        = James Cuenet
| studio         = Les Films Gibé
| distributor    = Pathé Consortium Cinéma
| released       = 24 November 1948
| runtime        = 100 min
| country        = France
| language       = French
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Aux yeux du souvenir (English title: To the Eyes of Memory) is a 1948 French language motion picture romantic drama  directed by Jean Delannoy who co-wrote screenplay with Henri Jeanson and Georges Neveux. The film stars Michèle Morgan and Jean Marais. The admissions in France were 4,559,689 people.  It was nominated for Golden Lion (Jean Delannoy) at 1949 Venice Film Festival.

==Main characters==
*Michèle Morgan as  Claire Magny
*Jean Marais as  Jacques Forester
*Jean Chevrier as  Le commandant Pierre Aubry
*Robert Murzeau as  Paul Marcadout
*René Simon as  En personne / Himself
*Jacques Louvigny as  Le passager
*Jim Gérald as  Le major
*Jeannette Batti as  Ketty
*Colette Mars as  Marcelle Marinier

==Notes==
 

==External links==
* 
*  at Alice Cinema
* 
* 
*  at LE CINEMA FRANCOPHONE

 

 
 
 
 
 


 