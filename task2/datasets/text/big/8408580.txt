Last of the Red Hot Lovers
 
{{Infobox play
| name       = Last of the Red Hot Lovers
| image      = 
| image_size = 
| image_alt  = 
| caption    = 
| writer     = Neil Simon
| chorus     = 
| characters = Barney Cashman Elaine Navazio Jeanette Fisher Bobbi Michele
| mute       = 
| setting    = An apartment in the East Thirties. December, August and September - late afternoon.
| premiere   =   
| place      = Eugene ONeill Theatre New York City
| orig_lang  = English
| series     = 
| subject    = 
| genre      = Comedy
| web        = 
}}
 Broadway in 1969.

==Production==
The play opened on Broadway at the  , the costumes by Donald Brooks, and the lighting by Peggy Clark. 

Later in the run, Dom DeLuise replaced Coco and Cathryn Damon and then Rita Moreno replaced Lavin. 

The play, Coco, Lavin, and Moore all were nominated for Tony Awards. 

==Plot overview==
Barney Cashman, a middle-aged, married nebbish wants to join the sexual revolution before it is too late. A gentle soul with no experience in adultery, he fails in each of three seductions: 

*Elaine Navazio, a sexpot who likes cigarettes, whiskey, and other womens husbands;
*Bobbi Michele, an actress friend who he discovers is madder than a hatter; and
*Jeannette Fisher, his wifes best friend, a staunch moralist.

==Adaptations==
Simon adapted his play for a 1972 film directed by Gene Saks. The cast featured Alan Arkin, Sally Kellerman (as Elaine), Paula Prentiss (as Bobbi), and Renée Taylor (as Jeanette). 

==Reception==
Clive Barnes, in his review in The New York Times, wrote: "He is as witty as ever...but he is now controlling that special verbal razzle-dazzle that has at times seemed mechanically chill... There is the dimension of humanity to its humor so that you can love it as well as laugh at it." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 