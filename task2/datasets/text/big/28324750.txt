Duel of Hearts
{{Infobox television film name = Duel of Hearts image = image_size =  caption =  genre = Romantic drama distributor =  director = John Hough producer = Lew Grade John Hough Laurie Johnson screenplay = Terence Feely based on =   narrator =  starring = Michael York  Geraldine Chaplin Benedict Taylor music = Laurie Johnson cinematography = Terry Cole editing = Peter Weatherley	 	 studio = Gainsborough Pictures budget =  country = United Kingdom language = English network = TNT (US) released =   runtime = 95 min num_episodes =  website =
}}
 John Hough. Michael York, Geraldine Chaplin and Benedict Taylor.

== Plot ==
Lady Caroline Faye and Lord Vane Brecon meet under unusual circumstances and are immediately attracted to one another. Later, Lady Caroline learns that Lord Brecon has been accused of murder. She embarks on a plan to prove him innocent, while also warning him that that his cousin, Gervaise Warlingham, may be trying to frame him. Posing as a commoner, she accepts a position at Lord Brecons estate as companion to his mother. Will dangerous family secrets keep the lovers apart?

== Cast ==
* Alison Doody - Lady Caroline Faye
* Benedict Taylor - Lord Vane Brecon Michael York - Gervaise Warlingham
* Geraldine Chaplin - Mrs. Miller
* Billie Whitelaw - Dorcas
* Virginia McKenna - Lady Brecon
* Suzanna Hamilton - Harriet Wantage
* Jeremy Kemp - Lord Milborne Richard Johnson ...  Lord Belgrave 
* Beryl Reid ...  Lady Augusta Warlingham 
* Suzanna Hamilton ...  Harriet Wantage 
* Jolyon Baker ...  Lord Stratton 
* Adalberto Maria Merli ...  Grimaldi 
* Margaret Mazzantini ...  Zara 
* John Albasiny ...  Gideon  Tom Adams ...  Magistrate

== External links ==
*  

 
 
 
 
 
 
 
 
 


 
 