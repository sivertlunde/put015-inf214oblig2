Bandish (film)
 
{{Infobox film
| name           = Bandish
| image          =
| image size     =
| caption        =
| director       = K. Bapaiah
| producer       = D. Rama Naidu
| writer         =
| narrator       =
| starring       = Rajesh Khanna Hema Malini Danny Denzongpa Bindiya Goswami Tanuja Om Prakash G. Asrani
| music          = Laxmikant Pyarelal
| cinematography =
| editing        =
| studio         = Suresh Productions
| distributor    = Suresh Productions
| released       = 1980
| runtime        =
| country        = India Hindi
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Hindi film directed by K. Bapaiah and starring Rajesh Khanna, Hema Malini and Danny Denzongpa.  . film cast/crew, Retrieved 10-24-2008   , The Complete Index to World Film, Retrieved 10-24-2008  Prem Nagar with the same lead pair on 1974. The music is by Laxmikant Pyarelal.

==Plot Synopsis==

A spoilt young man is taught a lessen in humility by a girl and falls in love with her. He is devastated by her murder, and retires to a village where he is surprised to meet her lookalike.

==Plot==
Kishan (Rajesh Khanna) is the spoilt son of a rich businessman. He is a medical student, for whom the life is all about playing pranks and troubling people around. Finally he is taught a lesson in humility by fellow student, Madhu (Hema Malini). He undergoes a dramatic change in outlook and falls in love with her. But his joy is short-lived as Kapilkumar (Danny Denzongpa) kills Madhu as she is the only witness to a murder. On the deathbead, Kishan promises Madhu that he would kill whoever is responsible for the death. Kishan is devastated and carries on a man hunt for KapilKumar. He finally recognizes Kapil in a train and in the ensuing scuffle, Kapil pushes Kishan off the running train. When Kishan regains consciousness, he is amazed to see Madhu (Hema Malini). On following her, he realizes that he is the twin sister of Madhu, Chanchal. He then starts practising medicine in their small village as this was what Madhu wanted to do. With time, Both are drawn towards each other until one day Kapil is in front of them once again. What Kishan does not know is that Kapil is his elder brother who was separated when they were young.

==Reception==
The box office collections were Rs. 2.90 crores in 1980. http://www.ibosnetwork.com/asp/filmbodetails.asp?id=Bandish+(1980)  It received four stars in the Bollywood guide Collections. 

==Cast==

*Rajesh Khanna as Kishan  
*Hema Malini as Madhu / Chanchal  
*Bindiya Goswami as Shanti
*Danny Denzongpa as Kapilkumar  
*Tanuja as Chanda (Kapilkumars Wife)  
*Om Prakash as Kishans Dad  
*G. Asrani as Murli  

==Music==
*"Mere Hosh Lelo" - Kishore Kumar, Asha Bhosle
*"Rang Bhare Mausam Se" - Kishore Kumar, Asha Bhosle
*"Arey Bhago Arey Daudo" - Kishore Kumar, Asha Bhosle
*"Main Kaun Hoon" -  Lata Mangeshkar
*"Sanyaji Ney Ghar Banwaya"- Asha Bhosle
*"Instrumental"

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 
* 
*  

 
 
 
 
 
 


 