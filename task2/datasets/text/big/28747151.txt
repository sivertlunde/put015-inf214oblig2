77 Park Lane
 
 
{{Infobox film
| name           = 77 Park Lane
| image          = 
| caption        = 
| director       = Albert de Courville
| producer       = William Hutter
| writer         = Walter C. Hackett Michael Powell Reginald Berkeley
| screenplay     = 
| story          = 
| based on       =  
| starring       = Dennis Neilson-Terry Betty Stockfeld Malcolm Keen Ben Welden
| music          = 
| cinematography = Geoffrey Faithfull Mutz Greenbaum
| editing        = Arthur Seabourne
| studio         = 
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Park Lane, a woman tries to save her brother from ruin. A French-language version 77 rue Chalgrin was made at the same time.

==Cast==
* Dennis Neilson-Terry ...  Lord Brent 
* Betty Stockfeld ...  Mary Connor 
* Malcolm Keen ...  Sherringham 
* Ben Welden ...  Sinclair 
* Cecil Humphreys ...  Paul 
* Esmond Knight ...  Philip Connor 
* Molly Johnson ...  Eve Grayson 
* Roland Culver ...  Sir Richard Carrington 
* Molesworth Blow ...  George Malton  John Turnbull ...  Superintendent 
* Percival Coyte ...  Donovan

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 