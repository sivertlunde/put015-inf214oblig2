Sherman's March (1986 film)
{{Infobox film 
| name           = Shermans March
| image          = DVD cover of Shermans March (1986 film).jpg
| caption        = DVD cover
| director       = Ross McElwee
| producer       = Ross McElwee
| writer         = Ross McElwee
| starring       = Ross McElwee
| narrator       = Ross McElwee
| music          = 
| cinematography = Ross McElwee
| editing        = 
| distributor    = 
| released       = September 5, 1986
| runtime        = 155 minutes  English
| budget         =  
}} 1986 documentary Grand Jury prize at the 1987 Sundance Film Festival.  and in 2000, was selected for preservation in the U.S. National Film Registry.

==Background== Georgia and March to the Sea") during the American Civil War. A traumatic breakup McElwee experienced prior to filming made it difficult for him to separate personal from professional concerns, shifting the focus of the film to create a more personal story about the women in his life, love, romance, and religion. Other themes include the spectre of nuclear holocaust in the context of the Cold War and the legacy and complexity of General Shermans own life. 

Structurally, the film follows a repetitive narrative pattern.  McElwee becomes enamored with various women, eventually developing feelings for each of his subjects, only to have his romantic hopes dashed.    The film, in McElwees opinion, follows a personal essay form; a hybrid autobiographical interactive documentary form that exists between fiction and nonfiction. 

==Production==
According to McElwee, "  SN, a very highly developed piece of recording equipment that could fit on my belt. This technological improvement made shooting much easier. 
 Space Coast," but the day after filming the Scottish games, his sister "said&mdash;somewhat seriously, somewhat joking&mdash;You should use the camera as a way to meet women. Shes sincerely upset about my having ended my relationship with my girlfriend, and shes looking for ways to get me back on my feet. ... t the point when she gave me her advice about how to use the camera, I experienced a minor epiphany (feeling)|epiphany." 

McElwee set out with just $9,000,  and began conducting mostly impromptu interviews. "Pretty much I always walked in on them", he said, characterizing his methods; "I guess what my conversations have that conventional interviews dont is a serendipitous quality, and emotional charge that has something to do with the personal connection between the subject and the film-maker. I never came with a list of questions."  The film ultimately cost $75,000 to complete. 

Principal photography lasted about five months, and, according to McElwee, "Id guess the total amount of footage I actually shot was about 25 hours." The total "filming time" could be considered much more substantial, however: "I was almost always ready to shoot. I kept the camera within reaching distance, sometimes balanced on my shoulder...even between major portraits, when I was on the road, I was totally open to filming whatever might happen in a gas station or in a restaurant, or wherever. So in one sense you can count all that time as filming time." 

==Reception==
Vincent Canby, in a "NYT Critics Pick" review of the documentary, called McElwee a "film maker-anthropologist with a rare appreciation for the eccentric details of our edgy civilization"; the film "which was made in 1981, is a timely memoir of the 80s.  Its also a very cheerful recollection of the kind of self-searching, home-movie documentaries that Jim McBride, the director, and L. M. Kit Carson, the writer and actor, satirized so brilliantly in their fiction film, David Holzmans Diary.  
In an interview with Paula Hunt for MovieMaker Magazine in 1994, discussing the distribution of the film, Ross McElwee stated:
 The distributor of First Run Features saw Shermans March at the IFP (the Independent Feature Project) in New York and immediately said hed take it. I wanted to shop around a bit, because its a very small company and I wanted to see what else was available. I got turned down by every other middle range distributor. I didnt even bother to go to the studios or the major distribution outlets. First Run Features was the only company willing to take a chance on it and, in fact, it did terrifically well. According to their statistics, until Strangers in Good Company came along it was their top grossing film. Its supposed to be the tenth highest grossing feature documentary of all time. Isnt that incredible? I could never have imagined it being that kind of a film.  

As prelude to a summer 1988 Film Quarterly interview with McElwee, Scott MacDonald wrote: 
 We get to know McElwees (or McElwees filmic personas) hopes, concerns, nightmares; and we are behind the camera with McElwee as he uses the film-making process to forge new relationships and to revise previously important relationships. As is true in many literary first-person narratives, McElwees approach in Shermans March is simultaneously very revealing and somewhat mysterious: the candidness of the scenes is frequently startling, but the more the film &mdash; and McElwee-as-narrator &mdash; reveals, the more we realize that there are many aspects of the relationships he is recording that we are not privy to. We cannot help but wonder about the narrator as we experience things with him. 
 epic length, the films poor technical quality wears you out." 

According to a 1998 review in The Austin Chronicle, "Ross McElwee is a modern master of cinema vérité &mdash; rough, real-life documentary filmmaking that seeks to expose a subjects soul through its very lack of polish. In McElwees case, that subject is almost always himself. Insistently personal, always autobiographical, occasionally exploitative, watching McElwee is like watching someones (well-financed) home videos." 
 Grand Jury prize in the field of documentary at the 1987 Sundance Film Festival.  In 2000, the Library of Congress deemed the film "culturally, historically, or aesthetically significant" and selected it for preservation in the National Film Registry, calling it a "hilarious, one-of-a-kind romantic exploration of the South." 

In April 2004, Slant (magazine)|Slant magazine, reviewing the films newly released DVD, gave it   (three stars out of five), saying it "looks and sounds like its   from 1986, but no amount of dirt and noise (and theres some here and there) can diffuse any of the films magic." 

==References==
{{reflist|2|refs=
   from the Sundance Institute 
   
   
 Insdorf, Annette (September 7, 1986).   The New York Times 
 Timothy, Corrigan. The Essay Film: From Mantaigne, after Marker. Oxford University Press, 2011. 
}}

==External links==
*  from the Sundance Institute
* 
*  at Ross McElwees website 

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 