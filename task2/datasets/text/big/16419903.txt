Nobody Knows (1970 film)
{{Infobox film name           = Nobody Knows  image          = File:Nobody_Knows(1970)_poster.jpg caption        =  director       = Jang Il-ho producer       = Kwak Jeong-hwan writer         = Kim Ha-lim  starring       = Oh Yu-kyeong Noh Joo-hyun Choi In-suk music          = Park Chun-seok cinematography = Lee Mun-baek editing        = Kim Hui-su distributor    = Hapdong Film Coperation released       =   runtime        = 85 minutes country        = South Korea language       = Korean budget         =  gross          = 
| film name      = {{Film name hangul         =   hanja          =  rr             = Amudo moreuge mr             = Amudo morŭge}}
}} 1970 South Korean drama film directed by Jang Il-ho.

== Plot ==
Jeong-ah, who grew up at an orphanage, works as a maid, and is raped by Yeong, the son of her employer. Yeong was engaged to Hyeon-ju, but dislikes her and decides to marry Jeong-ah instead. He takes Jeong-ah to a church and celebrates their own wedding by themselves. Yeongs parents and relatives learn of the fact and go late to the church to celebrate with the new couple.

== Cast ==
* Oh Yu-kyeong ... Jeong-ah
* Noh Joo-hyun ... Yeong
* Choi In-suk ... Hyeon-ju
* Lee Nak-hoon
* Jung Hye-sun
* Choi Bool-am
* Lee Muk-won
* Choe Eun-jeong
* Gwon O-sang
* Yoon Il-ju

== References ==
 

== External links ==
*  
*   at the Korean Movie Database
*   at mydvdlist.com  

 
 
 

 