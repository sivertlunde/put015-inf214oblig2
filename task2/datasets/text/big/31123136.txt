The Fake (1927 film)
{{Infobox film
| name           = The Fake
| image          =
| caption        =
| director       = Georg Jacoby
| producer       = Julius Hagen
| writer         =  Frederick Lonsdale (play)   George A. Cooper    Henry Edwards Elga Brink Juliette Compton   Miles Mander
| music          =
| cinematography = William Shenton   Horace Wheddon
| editing        = Ivor Montagu
| studio         = Julius Hagen Productions
| distributor    = Williams and Pritchard Films 
| released       = September 1927
| runtime        = 8,500 feet  
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama Henry Edwards, Elga Brink and Juliette Compton.  It is based on a play by Frederick Lonsdale. It was made at Twickenham Studios in London.

==Plot==
An Member of Parliament|M.P. pressures his daughter to marry an aristocrat in spite of his drug addiction.

==Cast==
*  Henry Edwards (actor)| Henry Edwards - Geoffrey Sands 
* Elga Brink - Mavis Stanton 
* Juliette Compton - Mrs. Hesketh Pointer 
* Norman McKinnel - Ernest Stanton 
* Miles Mander - Honourable Gerald Pillick 
* J. Fisher White - Sir Thomas Moorgate 
* A. Bromley Davenport - Hesketh Pointer 
* Julie Suedo - Dancer 
* Ivan Samson - Clifford Howe 
* Ursula Jeans - Maid 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 

 