Sensation Seekers (film)
{{Infobox film
| name           = Sensation Seekers
| image          =
| caption        =
| director       = Lois Weber
| producer       = Universal Pictures Carl Laemmle
| writer         = Lois Weber (scenario)
| based on       = short story, Egypt, by Ernest Pascal
| starring       = Billie Dove Huntley Gordon
| music          =
| cinematography = Benjamin H. Kline Thomas Pratt
| distributor    = Universal Pictures
| released       = March 20, 1927
| runtime        = 70 minutes; 7 reels
| country        = USA
| language       = Silent..English intertitles
}}
Sensation Seekers is a 1927 silent film romantic drama directed by Lois Weber, produced and distributed by Universal Pictures and starring Billie Dove.   

The film and a trailer survive. 


==Cast==
*Billie Dove - Luena Egypt Hagen
*Huntley Gordon - Ray Sturgis
*Raymond Bloomer - Reverend Lodge
*Peggy Montgomery - Margaret Todd
*Will Gregory - Colonel Emory Todd
*Helen Gilmore - Mrs. Todd
*Edith Yorke - Mrs. Hagen
*Phillips Smalley - Mr. Hagen
*Cora Williams - Mrs. W. Symme
*Sidney Arundel - Deacon W. Symme
*Blackie Thompson - Rabbitt Smythe (*Clarence Thompson)
*Nora Cecil - Mrs. Lodge
*Frances Dale - Tottie
*Lillian Lawrence - Tibbett Sister
*Fanchon Frankel - Tibbett Sister

==References==
 

==External links==
* 
* 
*  available for free download at  
*several lobby posters:.. , .. ,.. ,..

 
 
 
 
 

 
 