Coldwater (2013 film)
{{Infobox film
| name           = Coldwater
| director       = Vincent Grashaw
| writers        = {{plainlist|
* Vincent Grashaw
* Mark Penney
}}
| starring       = P.J. Boudousqué
| released       = 
| runtime        = 104 minutes
| country        = United States
| language       = English
}}
Coldwater is a 2013 film, directed by Vincent Grashaw, from a screenplay written by Grashaw and Mark Penney.

==Plot==
Coldwater details the abuse of juvenile inmates in a privatized American wilderness reform facility.

==Cast==
*P.J. Boudousqué
*James C. Burns
*Chris Petrovski
*Octavius J. Johnson
*Nicholas Bateman
*Stephanie Simbari
*Clayton LaDue
*Mackenzie Sidwell Graff
*Chauncey Leopardi

==Development==
The film was first announced on June 4, 2012.  Grashaw got the idea for the film after a friend of his was sent to a reform facility.   

The first draft of the screenplay was written in 1999. 

P.J. Boudousqué had been cast in the lead role after reading two scenes. He also had roles in American Horror Story (2011) and Bones (TV series)|Bones (2005). 

Filming took place in Malibu and Ventura, California, in mid to late 2012.

==Reception==
Coldwater currently holds a 56% (rotten) rating on Rotten Tomatoes. 

Coldwater received positive reviews from critics from dozens of outlets since its World Premiere at SXSW and to date has garnered several awards. It won Best Film, Best Director (Vincent Grashaw), and Best Actor (P.J. Boudousqué) at the Las Vegas Film Festival   and went on to win the Audience Awards for Best Feature at the Fresh Film Festival in Prague, Czech Republic,  as well as at the Cinema at the Edge Film Festival in Santa Monica, California. 

The Hollywood Reporter s positive review stated, "Affecting drama exposes a brutal real-world phenomenon." and went further to state, "the picture has plenty of potential beyond the fest circuit if given the right kind of attention.". 

Smells Like Screen Spirit awarded it an 8/10 score, stating it was "truly a film of great performances". 

The London Film Review awarded it four out of five stars and called it "an effective, tense and well made thriller". 

Fangoria awarded it four stars and called it "a satisfying, lasting and chilling experience". 

Complex (magazine)|Complex gave the film an 8/10 score and said, "Grashaws intense debut generates the highest level of raw suspense..."  

Paste (magazine)|Paste magazine,  Flixist,  and Complex  all listed Coldwater in their breakdowns of Best Films at SXSW.

==Release== South by Southwest Film Festival in Austin, Texas.   To date It has screened domestically and abroad in several other film festivals, including: 

It will be released theatrically in the United States, United Kingdom, Australia,  Germany,  France,  and South Korea in 2014.

==References==
 

 
 