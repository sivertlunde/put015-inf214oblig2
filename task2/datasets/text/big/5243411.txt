Chori Chori
 
{{Infobox film
| name           = Chori Chori
| image          = Chori Chori 1956 film poster.jpg
| image_size     =
| caption        = Film poster
| director       = Anant Thakur
| producer       = L. B. Lachman
| writer         = Aghajani Kashmeri
| narrator       =
| starring       = Raj Kapoor Nargis
| music          = Shankar Jaikishan
| cinematography = V. N. Reddy
| editing        = S. Surya
| distributor    = AVM Productions
| released       = 1956
| runtime        = 158 min.
| country        = India
| language       = Hindi
| budget         =
}}
 Johnny Walker have character parts.  This was the last film of Nargis-Raj Kapoor, with Nargis doing one cameo appearance in the Raj Kapoor starrer Jagte Raho. The music of Chori Chori had popular tracks  including "Aaja Sanam", "Yeh Raat Bheegi Bheegi" in the voices of Lata Mangeshkar and Manna Dey, "Jahan Mein Jati Hun" by Lata and Manna and "Sawa Lakh Ki Lottery," by Mohammad Rafi.    The movie was inspired by the 1934 movie It Happened One Night. The movie would later inspire the unofficial 1991 Hindi re-make Dil Hai Ke Manta Nahin directed by Mahesh Bhatt and starring Aamir Khan and Pooja Bhatt.  Like Chori Chori it too became a hit.

==Plot==
 
Kammo (Nargis) lives a very wealthy lifestyle with her widowed multi-millionaire dad, Girhdarilal (Bhagwan Dada), who would like her to get married to someone who is not after their wealth. To his dismay, she chooses to marry a pilot named Sumankumar (Pran), who is known to womanize and for his greed. When he disapproves, she runs away. He advertises for her safe return and offers to pay Rs.1.25 lakh to the finder.

Four days later, Kammo returns and she is not the same anymore; she is more humble, sober, and respectful. Fully appreciative of this, Girdharilal agrees to her marriage to Sumankumar. Just before the marriage, a man named Sagar  (Raj Kapoor) approaches them &mdash; this visit will change their lives forever.

==Cast==
* Raj Kapoor as Sagar / Sultana Daku
* Nargis as Kammo Gope as Seth. Girdharilal
* Master Bhagwan as Bhagwan Pran as Sumankumar David Abraham as Banwarilal (as David)
* Mukri as Madarilal (as Muqri)
* Raj Mehra as Editor
* Indira Bansal as Shayars wife
* Amir Banu as Madarilals wife (as Ameer Banoo)
* Rajasulochana as Bhagwans wife (as Raja Sulochana)
* Neelam as Girdharilals Secretary Johnny Walker as Shayar
* Maruti as Postal Employee

==Awards==
* Filmfare Award for Best Music Director - Shankar Jaikishan

==Soundtrack==
{| class=wikitable
|-
!Song
!Singer(s)
!Lyricist
|- Aaja Sanam Lata Mangeshkar, Manna Dey Hasrat Jaipuri
|- All Line Clear Mohammed Rafi Hasrat Jaipuri
|- Jahan Mein Jati Hun Lata Mangeshkar, Manna Dey  Shailendra (lyricist)|Shailendra
|- Is Paar Sajan Lata Mangeshkar Hasrat Jaipuri
|- Man Bhawan Ke Ghar Lata Mangeshkar, Asha Bhosle  Shailendra
|- Panchi Banoon Urti Phiroon Lata Mangeshkar Hasrat Jaipuri
|- Rasik Balma Lata Mangeshkar  Hasrat Jaipuri
|- Sawa Lakh Ki Lottery Lata Mangeshkar, Mohammed Rafi   Shailendra
|- Yeh Raat Bheegi Bheegi Lata Mangeshkar, Manna Dey  Shailendra
|}

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 