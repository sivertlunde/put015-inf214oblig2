Don't Promise Me Anything
{{Infobox film
| name = Dont Promise Me Anything
| image =
| image_size =
| caption =
| director = Wolfgang Liebeneiner
| producer = Herbert Engelsing   Heinrich Jonen
| writer =  Charlotte Rissmann (play)   Bernd Hofmann   Thea von Harbou
| narrator =
| starring = Luise Ullrich   Viktor de Kowa   Heinrich George   Hubert von Meyerinck
| music = Georg Haentzschel  
| cinematography = Friedl Behn-Grund   Georg Bruckbauer  
| editing =  Walter von Bonhorst     
| studio = Terra Film
| distributor = Terra Film
| released = 20 August 1937 
| runtime = 104 minutes
| country = Nazi Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Dont Promise Me Anything (German:Versprich mir nichts!) is a 1937 German comedy film directed by Wolfgang Liebeneiner and starring Luise Ullrich, Viktor de Kowa and Heinrich George.  A perfectionist but talented artist is reluctant to sell his paintings, but because they need the money his wife sells them without his knowledge and claims to be the artist herself. However, when she is commissioned to pain a mural she turns to her husband for help.

==Main cast==
* Luise Ullrich as Monika 
* Viktor de Kowa as Maler Martin Pratt  
* Heinrich George as Kunsthändler Felder 
* Hubert von Meyerinck as Dr. Elk  
* Will Dohm as Konsul Brenkow  
* Charlott Daudert as Vera Brenkow 
* Hans Hermann Schaufuß as Hausbesitzer Herr Lemke 
* Wilhelm P. Krüger as Der Gasmann  
* Maria Wanck as Fräulein Klette, Felders Sekräterin  
* Leopold von Ledebur as Präsident der Akademie  
* Erich Dunskus as Gläubiger #1  
* Margot Erbst as Maria, Mädchen bei Pratt  
* Maria Loja as Frau Lemke  
* Hans Meyer-Hanno as Gläubiger  
* Karl-Heinz Reppert as Gläubiger 
* Walter Vollmann as Diener bei Felder

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 
 


 