Le Congo, quel cinéma!
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Le Congo, quel cinéma!
| image          = 
| caption        = 
| director       = Guy Bomanyama Zandu
| producer       = Zandu Films
| writer         = 
| starring       = 
| distributor    = 
| released       = 2005
| runtime        = 7 minutes
| country        = Democratic Republic of the Congo
| language       = 
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = Pierre Mieko
| editing        = Guy Bomanyama Zandu
| music          = 
}}

Le Congo, quel cinéma! is a 2005 documentary film directed by Guy Bomanyama-Zandu. 

== Synopsis ==
Congolese cinema came to light by means of propaganda and educational films during the colonial era. Nowadays, local productions have a hard time keeping their above water, and Congolese filmmakers wonder about the future of a cinema lacking any kind of support. The film is a documentary about Congolese three technicians (Claude Mukendi, Pierre Mieko, and Paul Manvidia-Clarr) and Ferdinad Kanza, a director who made films in the years 1970-1980 and work at the National Radio Television of Congo. 

== References ==
 
 

== External links ==
*  

 
 
 
 
 
 
 
 


 
 