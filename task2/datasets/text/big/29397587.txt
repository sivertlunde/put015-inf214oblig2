Olympiyan Anthony Adam
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Olympiyan Anthony Adam
| image          = Olympiyan Anthony Adam.jpg
| image_size     = 
| alt            = 
| caption        =  Bhadran
| producer       = Mohanlal
| writer         = Babu C. Nair(dialgues) Bhadran(Screenplay)
| narrator       =  Meena  Nassar
| music          = Ouseppachan
| cinematography = Sanjeev Shankar
| editing        = Sateesh
| Associate Director= V R Gopalakrishnan, Renji Lal
| Assistant Director= Rajesh K abraham     Pranavam Arts International
| distributor    = Pranavam Arts PJ Entertainments UK
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Meena and Nassar in the lead roles. The film was based on Hollywood film Kindergarten Cop starring Arnold Schwarzenegger.

==Plot==
Antony Vargheese (Mohanlal) is a police officer who has been sent to a school as a teacher in disguise, to get details about the notorious Roy Mamman((Nassar(actor)|Nassar)) who is behind terrorist attacks in the city. Antony knows Roy Mammans daughter Rose Mamman(Sanika Nambiar) is studying in that school, but he does not know which of the kids is Mammans. During his investigation he meets Angel Mary (Meena Durairaj|Meena), a teacher, and attracts her enmity. Antony discovers that Angel has a troubled past with her husband turning out to be a drug addict, thus spoiling her marriage life on the first day. Antony later falls in love with her.

He finds out who Mammans kid is by accident. He makes the school principal (Seema (actress)|Seema) call Mamman saying that his daughter is affected by rabies after a dog bite. Mamman rushes to see his kid, falling for the trap that Antony has set for him.

==Cast==
*Mohanlal ... C.T. Vargheese Antony I.P.S/Olympiyan Meena ... Angel Mary Nassar ... Roy Mamman / Loother Seema .. Principal
*Jagathy Sreekumar ... Vattoli
*K. B. Ganesh Kumar ... Policeman
* Chali Pala  ... Ummerkutty (police officer)
*Ajith Kollam ... Policeman
*Poornima Anand ... Lilly
*Valsala Menon
*Sanika Nambiar ...  Rosemol/Rose Mamman

==Soundtrack==
Olympiyan Anthony Adams music is done by Ouseppachan and lyrics by Gireesh Puthenchery.

{| class="wikitable"
! Song !! Singers !! Other notes
|- Sujatha ||
|-
| Kadambanaattu Kaalavela || M. G. Sreekumar ||
|-
| Kokki Kurukiyum ||  
|- Sujatha ||
|-
| Nilaapaithale || K. J. Yesudas|Dr. K. J. Yesudas ||
|-
| Nilaapaithale || K. S. Chithra ||
|-
| One Little || Ouseppachan ||
|-
| Peppara Perapera || Mohanlal ||
|}

==Response==

The film ended as an above average grosser. Yuvathurki, Bhadrans sophisticated screenplay failed to capture the common viewers mind, despite which Olympiyan Anthony Adam has many patrons even a decade after theatrical release. 

==External links==
*  

 
 
 
 
 