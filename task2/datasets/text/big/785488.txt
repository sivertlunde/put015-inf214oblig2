The Singing Ringing Tree
 
 

{{Infobox film
| name           = The Singing Ringing Tree
| image          =
| image_size     =
| caption        = Francesco Stefani
| producer       = Alexander Delete Francesco Stefani
| narrator       =
| starring       = Richard Krüger Eckart Dux Christel Bodenstein
| music          = Heinz-Friedel Heddenhausen
| cinematography = Karl Plintzner
| editing        =
| distributor    =
| released       = 13 December 1957
| runtime        = 73 mins
| country        = East Germany German
| budget         =
| preceded_by    =
| followed_by    =
}} East German Francesco Stefani.

==Plot summary==
The story concerns a beautiful but selfish and haughty princess who rejects the proposal of a wealthy prince.   She scorns the gifts he offers her, and says that she will marry him only if he brings her the mythical "singing ringing tree".  The prince locates the tree in the territory of an evil dwarf, who offers to give him the enchanted tree, on the understanding that, if the princess still rejects him, he will be in the dwarfs power and will be turned into a bear.  Because the tree will only sing and ring when the princess falls in love with the prince, she is disappointed in it and continues to reject the prince.  The prince is forced to return to the dwarfs lair and is turned into a bear.

The princess sends her father to find the singing ringing tree for her, but he is met by the prince, in the guise of a bear, who gives him the tree on condition that the king returns with the first thing the king sees on his return.  This turns out to be the princess, who is now delivered into the hands of the dwarf.  The dwarf, seeing the princesss self-centred behaviour, casts a spell to make her ugly.  The bear tells her that she will regain her beauty only if she changes her ways.  Gradually she is won over by the bear and becomes beautiful again.  Despite the dwarfs attempts to keep her and the prince apart, she eventually falls in love with him and the singing ringing tree finally lives up to its name.

==Cast==
In credits order.
* Christel Bodenstein as the haughty princess
* Charles Hans Vogt as the king
* Eckart Dux as the handsome prince/bear
* Richard Krüger as the dwarf
* Dorothea Tiesing as the nurse
* Günther Polensen as the captain of the guard
* Fredy Barton as the minister
* Egon Vogel as captain and master of ceremonies
* Paul Knopf as the guardian
* Paul Pfingst as Bauer
* Friedrich Teitge as the gardener
* Maria Besendahl (Anna-Maria Besendahl) as the herbalist

==Production== studio in dwarf was named Richard Krüger and died in Strasbourg, or was Hermann Emmrich who died in 1995 and is buried in Prenzlau in north eastern Germany. It is now thought that they were one and the same person. 

==Release==
After its release in East Germany, it sold 5,901,141 tickets in the country.  The film was then purchased by the BBC and cut into three parts to create a mini-series which was first broadcast on black and white television from 19 November 1964 to 3 December 1964 as part of Tales from Europe, with an English-language voice-over track (not dubbing (filmmaking)|dubbed, however, the original soundtrack was simply faded up and down). It was repeated many times through to 1980. In 1988 it was released on VHS video in Germany and it is now available on a DVD.

==Reviews==
A Radio Times readers poll in 2004 voted this programme the 20th spookiest TV show ever.

==The Fast Show parody== spoof of the series was created as a sketch in the last season of The Fast Show, entitled, The Singing Ringing Binging Plinging Tinging Plinking Plonking Boinging Tree.

==References==
 

==External links==
*   at bbc.co.uk
*  
*  
*  

 
 
 
 
 
 
 