The Humbling (film)
{{Infobox film
| name           = The Humbling
| image          = The Humbling film.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Barry Levinson
| producer       = Al Pacino Jason Sosnoff Monika Bacardi Ged Dickersin Kristina Dubin Andrea Iervolino Gisella Marengo
| writer         = Buck Henry Michal Zebede
| based on       =  
| starring       = Al Pacino   Greta Gerwig   Dianne Wiest   Charles Grodin   Kyra Sedgwick
| music          = Marcelo Zarvos
| cinematography = Adam Jandrup
| editing        = Aaron Yanes
| studio         = Ambi Pictures Hammerton Productions
| distributor    = Millennium Films
| released       =  
| runtime        = 112 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} erotic comedy comedy film directed by Barry Levinson and written by Buck Henry and Michal Zebede, based on the 2009 novel, The Humbling, written by Philip Roth. The film stars Al Pacino, Greta Gerwig, Dianne Wiest, Charles Grodin and Kyra Sedgwick. It was screened in the Special Presentations section of the 2014 Toronto International Film Festival    and in the Out of Competition section of the 71st Venice International Film Festival. The film was released on January 23, 2015, by Millennium Films.

==Synopsis==
Simon Axler is an aging actor with bouts of dementia and related issues. He is institutionalized after an incident during a Broadway play, then returns home, where he contemplates suicide. When he embarks upon an affair with a friends amoral lesbian daughter, his world starts to fall apart. It ends on stage, with even Axlers audience and fellow actors unsure whats real and whats not.

== Cast ==
* Al Pacino as Simon Axler
* Greta Gerwig as Pegeen Mike Stapleford
* Kyra Sedgwick as Louise Trenner
* Dan Hedaya as Asa
* Dianne Wiest as Pegeens Mother
* Charles Grodin as Jerry
* Dylan Baker as Dr. Farr

== Production ==
On February 4, 2014, it was announced that Millennium Films had acquired the worldwide rights to the film. 

==Reception==
The Humbling received mixed reviews from critics. On Rotten Tomatoes, the film has a rating of 51%, based on 49 reviews, with a rating of 5.4/10. The sites critical consensus reads, "The Humbling is an inarguable highlight of Al Pacinos late-period filmography, but thats an admittedly low bar that it doesnt always clear by a very wide margin."  On Metacritic, the film has a score of 59 out of 100, based on 25 critics, indicating "mixed or average reviews". 

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 