Red Salute
{{Infobox film
| name           = Red Salute
| image          = Red Salute 1935 Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Sidney Lanfield
| producer       = Edward Small
| screenplay     = {{Plainlist|
* Humphrey Pearson
* Manuel Seff Elmer Harris  
}}
| story          = Humphrey Pearson
| starring       = {{Plainlist|
* Barbara Stanwyck Robert Young
}}
| music          = Alfred Newman  
| cinematography = Robert H. Planck
| editing        = Grant Whytock
| studio         = Reliance Pictures
| distributor    = United Artists
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Robert Young. Based on a story by Humphrey Pearson, the film is about the daughter of an US Army general who becomes involved with a Communist agitator in order to annoy her father. The general has his daughter kidnapped and taken to Mexico, where he hopes she will forget her radical boyfriend and become attracted to a young handsome soldier. 

==Plot==
Drue Van Allen (Stanwyck) plays the daughter of a general, in love with a Communist agitator.  The general arranges for her to be taken off to Mexico against her will.  Determined to return to Washington, she falls in with a rowdy soldier (Young) who more-or-less accidentally goes AWOL, crosses the border in a stolen military vehicle, and kidnaps an easy-going traveler (Edwards) to hide in his homemade trailer.

==Cast==
* Barbara Stanwyck as Drue Van Allen Robert Young as Jeff
* Hardie Albright as Arner
* Cliff Edwards as Rooney
* Ruth Donnelly as Mrs. Rooney Gordon Jones as Lefty Paul Stanton as Louis Martin
* Purnell Pratt as General Van Allen
* Nella Walker as Aunt Betty
* Arthur Vinton as Joe Beal
* Edward McWade as Baldy
* Henry Kolker as Dean
* Allan Cavan as Army Officer
* Ferdinand Gottschalk as League Speaker
* Selmer Jackson as Army Officer
* David Newell as Student
* Bill Schrader Student fighter, rally attendee

==Production==
The original working title of the film was Her Uncle Sam. The film was made to cash in on the rise of radicalism in US colleges in the 30s.  Filming started in June 1935.  The film features the song "I Wonder Whos Kissing Her Now".

It was one of the first anti-Communist movies made in the US. This saw it re-released in 1948 with the rise in anti Communist feeling.  

The film is also known by its reissue title Her Enlisted Man.

==References==
 

==External links==
* 
*  (incomplete)

 
 

 
 
 
 
 
 
 
 
 
 
 


 