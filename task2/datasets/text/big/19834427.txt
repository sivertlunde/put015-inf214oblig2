Hanussen (1955 film)
 
{{Infobox film
| name           = Hanussen
| image          = 
| caption        = 
| director       = O. W. Fischer Georg Marischka
| producer       = Eberhard Klagemann
| writer         = Gerhard Menzel Curt Riess
| starring       = O. W. Fischer
| music          = 
| cinematography = Helmut Ashley
| editing        = Wolfgang Wehrum
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

Hanussen is a 1955 West German war film directed by and starring O. W. Fischer.   

==Cast==
* O. W. Fischer – Erik Jan Hanussen
* Liselotte Pulver – Hilde Graf
* Theodor Danegger
* Maria Dominique – Grace Coligny
* Werner Finck – Der Sachverständige
* Klaus Kinski – Erik von Spazier a.k.a. Mirko
* Reinhard Kolldehoff – Biberger
* Rolf Kralovitz
* Walter Ladengast
* Margrit Läubli – Tanzgirl
* Ludwig Linkmann – Herr Scholz
* Siegfried Lowitz – Der Staatsanwalt
* Erni Mangold – Priscilla Pletzak
* Franz Muxeneder – Jaroslav Huber
* Helmut Qualtinger – Ernst Röhm
* Kai-Siegfried Seefeld – (as Kai S. Seefeld)
* Hermann Speelmans – Maus
* Wastl Witt – Leopold Ebenseder

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 
 