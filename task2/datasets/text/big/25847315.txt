Tora-san, the Good Samaritan
{{Infobox film
| name = Tora-san, the Good Samaritan
| image = Tora-san, the Good Samaritan.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Rumi Sakakibara
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 92 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  is a 1971 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Rumi Sakakibara as his love interest or "Madonna".  Tora-san, the Good Samaritan is the seventh entry in the popular, long-running Otoko wa Tsurai yo series.

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Rumi Sakakibara as Hanako Ōta
* Sachiko Mitsumoto as Fuyuko
* Chōchō Miyako as Kiku
* Kunie Tanaka as Prof. Fukui
* Hiroshi Ōtsuka as Policeman
* Gin Maeda as Hiroshi Suwa
* Chieko Misaki as Torajiros aunt
* Hisao Dazai as Tarō Ume

==Critical appraisal==
For his work on Tora-san, the Good Samaritan, the previous entry in the Otoko wa Tsurai yo series, Tora-sans Shattered Romance, and the following entry, Tora-sans Love Call (all 1971), Yoji Yamada tied for Best Director at the Mainichi Film Awards with Masahiro Shinoda.  The German-language site molodezhnaja gives Tora-san, the Good Samaritan three and a half out of five stars.   

==Availability==
Tora-san, the Good Samaritan was released theatrically on April 28, 1971.  In Japan, the film has been released on videotape in 1983 and 1995, and in DVD format in 1998 and 2008. 

==References==
 

==Bibliography==

===English===
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 


 