Ice Poison
 
{{Infobox film
| name           = Ice Poison
| image          = Ice_Poison_Poster.jpg
| caption        = Poster of the film.
| director       = Midi Z
| producer       = Midi Z   Patrick Mao Huang
| writer         = Midi Z
| starring       = Shin-Hong Wang   Ke-Hsi Wu
| music          = 
| cinematography = Fan Sheng-Siang
| editing        = Lin Sheng-Wen   Midi Z
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Taiwan
| language       = Mandarin
| budget         = 
}}
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.   

==Cast==
* Shin-Hong Wang
* Ke-Hsi Wu

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Taiwanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 