Money No Enough
 
{{Infobox film
| name           = Money No Enough
| image          = Money No Enough VCD cover.jpg
| caption        = VCD cover for Money No Enough
| director       = Tay Teck Lock
| producer       = J.P. Tan
| writer         = Jack Neo
| starring       = {{plainlist|*Jack Neo Mark Lee
*Henry Thia}}
| cinematography = Kamis Bin Huri
| editing        = A. Supranamian
| distributor    = Shaw Organisation
| released       =  
| runtime        = 98 minutes 
| country        = Singapore
| language       = {{plainlist|
*Hokkien
*Singlish
*Mandarin }}
| budget         = 
| gross          = 
}}
 Mark Lee Singaporean film industry and pave the way for the emergence of other Singaporean cultural phenomena.

==Plot== Mark Lee), 4D and pursuing an insurance agent who he has a crush on.
 capital they have. However, at the opening ceremony of their business, Huis mother collapses and is taken to hospital, where she is diagnosed with leukaemia. Hui has three wealthy older sisters, but they refuse to help pay the medical bills, and his application for financial assistance is rejected because of his sisters high incomes. To compound matters, the loan sharks show up at the company to harass Ong. Ong and Hui ask to cash out their shares so they can pay the loan sharks and medical bills respectively, but all their money has already been spent on equipment and other business running costs. Huis mother then dies and at her wake, which raises ten thousand dollars of bai jin (contributions toward funeral expenses), the loan sharks turn up, pursue Ong and are arrested after a lengthy police chase.  Keong convinces his wife and daughter to enter an obstacle race where they win the first prize of S$100,000, which he uses to pay his creditors, and his family is reunited. The car polishing business is successful, and the three friends go on to become the directors of Autoglym Singapore.

==Production==
In the 1990s, Neo, Lee and Thia became well known in Singapore for their performances in the   film 12 Storeys, and saw potential in the then virtually nonexistent local film industry. Karl Ho, "Jack Neos touch of class", The Straits Times, 31 January 2002.  He wrote a screenplay about expatriates in the advertising industry, but decided the concept would not appeal to most Singaporeans, so he thought of writing a story about Ah Bengs (uneducated Chinese men), drawing on the humble backgrounds of Lee, Thia and himself. "Borrow from Ah Long?", The Straits Times, 7 May 1998.  Inspired, he contacted Tay Teck Lock, a former producer for Channel 8, and suggested they collaborate. They decided on a plot about three Singaporean men facing financial difficulties. Neo spent eight months writing the script, while Tay helped develop the characters and jokes. Mak Mun San, "Life after Money No Enough", The Sunday Times, 11 May 2008.  Despite the Speak Mandarin Campaign, Neo chose to use Hokkien dialogue to "reflect real life" and "reach a different audience".  Andrea Hamliton, "The Money Pours In", Asiaweek, 26 June 1998. 
 props that Board of Film Censors reviewed and approved Money No Enough,   distributor Shaw Organisation released the film on 21 screens on 7 May 1998.   Ong Sor Fern, "Delirious over Teenage sale", The Straits Times, 26 November 1998.  The success of the movie led to a dispute between Neo and JSP Films over their shares of the profits.  To mark its tenth anniversary, Money No Enough was released in cinemas again and was followed by a sequel, Money No Enough 2. 

==Reception==
Money No Enough earned S$50,000 from sneak previews and S$42,000 on its opening day,  then topped the local box office for a month.  In total, the film made S$5.8 million, Camilla Chiam, "Two Jack Neo movies slug it out", The Straits Times, 12 June 2002.  which remained the best box-office showing by a local movie until the record was broken by Ah Boys to Men in 2012. After its box office run, 70,000 VCDs of Money No Enough were sold, which remains a record for a Singaporean film.  Its success  sparked the film career of Jack Neo, who won the Best Director Award at the 1998 Silver Screen Awards,  and the development of the Singaporean film industry. Four more Singaporean movies were produced in 1998, two of which were described by critics as copycats of Money No Enough.  With its use of Hokkien and crude portrayal of Singaporean life, the film is also credited with paving the way for other Singaporean cultural phenomena such as mrbrown and TalkingCock. 

The movie received a mixed critical reception with LoveHKFilm.com commending the film as "an effective satire of...Singaporean culture" and noted that the actors "do a credible job representing characters from Singapores varying social strata",  while a  . Last accessed 31 July 2011.  Francis Dass of the New Straits Times wrote that Money No Enough was "spot-on" and "funny", but criticised the "clichéd script and the directors penchant for melodrama". 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 