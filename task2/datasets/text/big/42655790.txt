Girl's Blood
{{Infobox film
| name           = Girls Blood
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Koichi Sakamoto
| producer       =  
| writer         = 
| screenplay     = Takehiko Minato
| story          = 
| based on       =  
| narrator       = 
| starring       =  
| music          = Yasuhiro Mizawa
| cinematography = Shu G. Momose
| editing        = Hiroshi Sunaga
| studio         =  
| distributor    = Kadokawa
| released       =   
| runtime        = 118 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
 erotic action film directed by Koichi Sakamoto and written by Takehiko Minato, based on the novel Aka × Pink by Kazuki Sakuraba and distributed by Kadokawa. It was released on 22 February 2014. 

==Story==
Four girls take part in illegal underground fighting event "Girls Blood" held at an abandoned school building in Roppongi every night. The girls have their own stories and quirks from their private lives. Satsuki (Yuria Haga) suffers from a gender identity disorder, Chinatsu (Asami Tada) ran away from an abusive husband, Miko (Ayame Misaki) is a S&M queen and Mayu (Rina Koike) has a Lolita face.

==Cast==
*Yuria Haga as Satsuki  
*Asami Tada as Chinatsu 
*Ayame Misaki as Miko
*Rina Koike as Mayu
*Misaki Momose as Momomi
*Hideo Sakaki

==Reception==
Patryk Czekaj, in a review on Twitch Film, called the film a "especially entertaining and laugh-inducing" guilty pleasure. 

==References==
 

==External links==
*   
* 

 

 
 
 
 
 
 


 
 