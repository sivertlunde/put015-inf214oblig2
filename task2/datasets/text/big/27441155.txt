Kites: The Remix
{{Infobox film
| name           = Kites: The Remix
| image          = KITES THE REMIX-poster-FINAL.jpg
| alt            =  
| caption        = 
| director       = 
| producer       =  
| writer         = 
| screenplay     = 
| story          = 
| starring       = 
| music          = Graeme Revell
| cinematography = 
| editing        = Brett Ratner
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = India
| language       =  
| budget         = 
| gross          = 
}}
Kites: The Remix is the official English-language version of the 2010 film Kites (film)|Kites. It was produced and edited by Hollywood director Brett Ratner. The international version is 90 minutes long, as opposed to the Hindi version, which is 130 minutes long. This new version removes all of the original Hindi songs, except for the title track "Fire" which has an alternative English version that is heard during the end titles. Along with alternative footage and added action scenes, the movie is aimed at a younger, action-orientated audience.

While the Hindi version of Kites was scheduled for release on May 21, 2010, the international version was scheduled for release one week later on May 28, 2010. The movie was released in over 60 countries. It was rumored that this version would be dubbed in English, but it was later revealed that the two versions were simultaneously shot, and that most of the original Hindi-based music and all of the Hindi dialogue would be removed and replaced with English voiceovers and new music by Hollywoods composer Graeme Revell for the international version of the movie.

The trailer for Kites: The Remix was released on May 4, 2010.

==Plot== Las Vegas, Nick Brown), is about to marry a beautiful Mexican woman named Natasha (Bárbara Mori), whom J. knows as Linda, the last of the immigrant women he married. On the night before "Natasha" and Tonys wedding, Linda and J. spend a romantic but chaste night, humorously agreeing to a "divorce." A jealous, gun-wielding Tony, however, arrives at her apartment while J. is there, and after he hits her, Linda impulsively knocks him out with a heavy object while he tussles with J. Linda and J. go on the run toward Mexico, with Tony and police in pursuit.

==Characters/Cast==
*Hrithik Roshan as J.
*Bárbara Mori as Natasha a.k.a. Linda
*Kangana Ranaut as Gina  (Guest Appearance)
*Nicholas Brown as Tony   at ComingSoon.net 
*Kabir Bedi as Bob, Gina and Tonys father Jain, Samta.  , FreshNews.in, May 17, 2010 
*Yuri Suri as Jamaal, Bobs family chauffeur 

==Music==
Kites   original music is by  ", though Enya is not given any composers credit for the music in Kites. 

Kites: The Remix was almost completely re-scored with new music by Graeme Revell.

==Reception==
===UK Box Office===
The film fared very poorly and earned £9,110 on 31 screens, with the per-screen average working out to £294. 

===USA Box Office===
In the first week that both films were out together in the USA, Kites: The Remix did only one tenth of the business that the original Kites did, and less than half on a per-screen basis.    Box Office Mojo shows that while the original Kites film was able to become the first ever original Bollywood created film to score in the "Top Ten" of the overall Hollywood Box office tally on its opening weekend, the Kites: The Remix hybrid version only managed to place 51st on its first weekend.     

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 