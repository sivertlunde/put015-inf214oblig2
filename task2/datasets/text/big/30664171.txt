Sons of the Legion
{{Infobox film
| name           = Sons of the Legion
| image          =
| caption        = James P. Hogan Stuart Walker
| writer         = 
| starring       = 
| music          =
| cinematography = Charles Edgar Schoenbaum
| editing        = Anne Bauchens
| studio = Paramount Pictures
| distributor    = 
| released       =  
| runtime        = 60 min.
| country        = United States
| language       = English 
| budget         =
}}
 squadron in Legion Post. However, because the boys father wrongfully received a dishonorable discharge after World War I, his father cannot join the Legion and in turn his son cannot join the squadron. THE SCREEN IN REVIEW:  Too Hot to Handle, With Clark Gable and Myrna Loy, Opens at the Capitol Drums Shown at the Music Hall--Sons of the Legion at Criterion At theSquire At the Criterion At the Music Hall
By FRANK S. NUGENTT. M. P.B. C.B. R. C.. New York Times (1923-Current file)   30 Sep 1938: 24.  

== Cast ==

* Lynne Overman as Charles Lee
* Evelyn Keyes as Linda Lee
* Tim Holt as Steven Scott Elizabeth Patterson as Grandmother Lee
* William Frawley as Uncle Willie Lee Billy Cook as David Lee
* Billy Lee as Billy Lee
* Donald OConnor as Butch Baker
* Edward Pawley as Gunman Baker Richard Tucker as State Commander Johnnie Morris as Mickey
* Wally Albright as Harold
* Benny Bartlett as Red OFlaherty
* Lucille Ward as Margaret
* Ronald Paige as Boy  David Holt as Jimmy Hynes
* Georgie Billings as Skinny
* Sammy McKim as Spec
* Arthur Singer as Abie
* Walter Tetley as Shifty
* Charles Peck as Clarence
* Sonny Boy Williams as Dickie
* James T. Mack as Postman
* Carl Harbaugh as Customer
* Emmett Vogan as Policeman
* Sarah Edwards as Mrs. Hynes

==Production==
Tim Holt was loaned to Paramount from Walter Wanger. SCREEN NEWS HERE AND IN HOLLYWOOD: Plans to Produce Personal History Are Abandoned for the Present by Wanger TROPIC HOLIDAY TO OPEN Bob Burns and Martha Raye to Be Featured in Picture at Paramount This Morning Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   29 June 1938: 15.   Filming started 30 June 1938. SCREEN NEWS HERE AND IN HOLLYWOOD: Henry Fonda Will Be Seen in RKOs Mad Miss Manton Opposite Miss Stanwyck LORD JEFF OPENS TODAY Capitol Presents Metro Film Starring Mickey Rooney and Freddie Bartholomew Of Local Origin
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   30 June 1938: 21.  
==References==
 
== External links ==
*  
*  at TCMDB
 
 
 
 
 

 