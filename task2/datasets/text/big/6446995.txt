The Whole Nine Yards (film)
{{Infobox film
| name           = The Whole Nine Yards
| image          = Whole nine yards.jpg
| image_size     = 200px
| border         = yes
| caption        = Theatrical release poster
| director       = Jonathan Lynn David Willis
| writer         = Mitchell Kapner
| starring       = Bruce Willis Matthew Perry Rosanna Arquette Michael Clarke Duncan Natasha Henstridge Amanda Peet Kevin Pollak
| music          = Randy Edelman
| cinematography = David Franco
| editing        = Tom Lewis
| studio         = Morgan Creek Productions Franchise Pictures Rational Packaging Lansdown Films Warner Bros. Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States Canada
| language       = English French Spanish
| budget         = $41.3 million
| gross          = $106,371,651
}} crime comedy a popular expression of uncertain origin. The Whole Nine Yards was followed by the 2004 film The Whole Ten Yards.

==Plot==

Nicholas "Oz" Oseransky (Matthew Perry) is a Quebec dentist from Chicago, trapped in a miserable marriage to the daughter of his late ex-partner. His wife Sophie (Rosanna Arquette) and mother-in-law (Carmen Ferlan) hate him, and his deceased father-in-law has saddled him with a lot of debt. He cant divorce his wife because her father embezzled from the dental practice, and he has nothing but debt if they separate. As the movie opens, Sophie tells Oz she needs $5000, but wont say what for.

At the office, we are introduced to his assistant, Jill (Amanda Peet). Jill and Oz have lunch and she tells him that his wife isnt a good person. They joke about having her taken out of the picture, and Jill tells him to name a price. When Oz gets home, a new neighbor is moving in next door. Oz thinks that "Jimmy Jones" looks familiar, and after recognizing a tattoo on Jimmys forearm, realizes that hes Jimmy "the Tulip" Tudeski (Bruce Willis). Tudeski is an infamous contract killer from Chicago who was given amnesty after ratting out Lazlo Gogolak and other members of the Gogolak gang.

After an uncomfortable encounter, Oz runs home in a panic and tells Sophie theyre moving. But Sophie insists that Oz return to Chicago to collect a finders fee by giving up Jimmys whereabouts to Lazlos son, Janni (Kevin Pollak). Upon arriving in Chicago, Oz has lunch with an old friend and confesses that he has no intention of meeting with Janni. Unfortunately, when Oz returns to his hotel room, he is greeted by Frankie Figs (Michael Clarke Duncan) who turns out to be one of Jannis enforcers. Frankie roughs Oz up to get the truth out of him and then takes him to Janni.

At Jannis estate, Oz meets Cynthia (Natasha Henstridge), Jimmys estranged wife. Janni instructs Frankie to accompany Oz back to Canada and keep an eye on Jimmy until Janni and his men arrive to take him out. Back at the hotel, Cynthia asks Oz if he really knows where to find Jimmy, explaining that Janni and Jimmy both want her (as well as each other) dead in order to collect on $10 million, which is being held in trust in their names. Oz and Cynthia share a bottle of scotch and end up sleeping together. Afterwards, Oz vows to find a way to protect Cynthia from Jimmy.

Back in Canada, Frankie and a very nervous Oz enter another hotel room and see Jimmy waiting inside. It turns out that Frankie and Jimmy are old friends and together, are planning to take out Janni and Cynthia. Oz returns to work and tells Jill about the events in Chicago. Jill is extremely excited when she finds out Jimmy the Tulip is Ozs neighbor and makes Oz take her to meet him. Jill tells Jimmy that hes her hero and the reason she wants to be a contract killer. She also tells him that Sophie tried to use the $5000 to have Jill kill Oz, but Jill ended up liking Oz after getting to know him and refused the contract.

As Jimmy and Jill discuss the upcoming ambush, Oz gets angry and says that he wont stand by and let them kill Cynthia. Oz is back at home and tries to call and warn Cynthia about Jimmys plan, but she is already on the way with Janni. When they arrive at Ozs house, Oz gets a chance to warn Cynthia, while she warns him that Janni will kill him after killing Jimmy. The two watch from Ozs garage as Jannis gang walks into the trap.

Down the street, Sophie and her new hitter (Harland Williams) are in his car. She is discussing the payment and promises that her mother will pay the remainder of his fee after the job is done. The hitter is distracted when he recognized Janni Gogolak heading toward Jimmys house. When Sophie tells him whose house it is, he realizes whats about to happen, pulls out a gun and exits the car, taking the car keys with him.

When Janni enters the house, he is distracted by a naked Jill, and she, Jimmy and Frankie are easily able to kill Janni and his men. Oz and Cynthia make a break for it. Sophies hitter is in the yard when he hears Ozs car peel out behind him.  This draws Jimmys attention to the window. He has Jill get the mystery hitters attention and then shoots him. Sophie sees this and runs, screaming, down the street. When they get the body inside, they realize he is an undercover cop.

As they start to get a plan together to dispose of the bodies, Oz calls. While they are on the phone, Jill reveals that Oz slept with Cynthia in Chicago—which she thinks is great—but it infuriates Jimmy, who feels that friends shouldnt sleep with each others wives. Oz makes a deal with them that will benefit everyone. Back at his office, he alters the cops teeth to exactly match Jimmys. They then put the cops body and Jannis into Ozs car and set it on fire, rendering the bodies unidentifiable, except by dental records.  The next day, two investigators think that Janni and Jimmy are both dead. This allows Cynthia to collect the $10 million, which as a part of the deal, she has agreed to transfer to Jimmy in exchange for her and Ozs lives. The investigators also find the undercover cops car when they go to check out Jimmys house. Inside the car is a tape recorder with Sophie and the cops conversation to kill Oz, the contents of which are more than enough to put Sophie and her mother in prison, even though Sophie tearfully claims that Jill is the killer that she hired to kill the undercover cop, but the cop does not believe her story, and Oz plans to divorce her.

Some time later, after the death certificates have been processed, the key players meet up in the city. While Cynthia and Jill go to the bank to transfer the money, Oz heads to an art museum with Jimmy, where they will be around people and Oz thinks hell be safe. Jimmy has other plans though and takes Oz (along with Frankie Figs) out on a private yacht. Back at the bank, Jill asks Cynthia if shes tempted to take the money and run. Cynthia decides that she loves Oz and cant leave him to be killed by Jimmy. Jill tells Cynthia that Jimmy wants her and Oz to have $1 million as a wedding gift, and the two women make the transfers.

On the boat, Jimmy gets the call that the money has been received. He pulls out a gun and points it at Oz. Frankie grins knowingly before Jimmy turns and shoots him instead; Jimmy explains that he had to kill Frankie or Frankie would kill Oz and then potentially come after Jimmy himself or send others after him in the belief that Jimmy had gone soft. Oz is floored, but happy to be alive. He tells Jimmy that its obvious hes in love with "her." Jimmy acts confused, thinking Oz is referring to Cynthia. In the next shot, Jill runs on the boat and jumps into Jimmys arms. Oz leaves to meet Cynthia. As soon as he sees her, he asks her to marry him. She looks disappointed and asks if Jimmy told him (about the money.) Oz doesnt know what shes talking about, and so she happily agrees to marry him. In the closing scene, the two newlywed couples are on a hotel balcony overlooking Niagara falls, dancing to a bluesy version of Ira Gershwins "They All Laughed" as the credits roll.

==Cast==
* Bruce Willis as Jimmy Tudeski/"The Tulip"
* Matthew Perry as Nicholas Oseransky/"Oz"
* Rosanna Arquette as Sophie Oseransky
* Michael Clarke Duncan as Franklin Figueroa/"Frankie Figs"
* Natasha Henstridge as Cynthia Tudeski
* Amanda Peet as Jill St. Claire
* Kevin Pollak as Janni Pytor Gogolak
* Harland Williams as Special Agent Steve Hanson

==Reception==

===Box office===
The film grossed $57,262,492 during its U.S. theatrical run, with an additional $49,109,159 internationally. Its worldwide total is $106,371,651.  The film is Franchise Pictures only financial success.

===Critical reaction===
The Whole Nine Yards received mixed reviews from critics; review aggregator Rotten Tomatoes gives the film a score of 45% based on 100 reviews from critics, with an average rating of 5.2/10 and the sites consensus reading: "Despite a charming cast, The Whole Nine Yards could only tickle half of the critics funny bones. The other half thought it an underwhelming, depressing sitcom".  
Metacritic gives the film an average score of 47%, based on 32 reviews. 

Roger Ebert gave the film one of the more positive reviews, noting in particular that the highlight was Amanda Peets performance as Jill, which Ebert called "perfect". {{cite news
| author = Roger Ebert
| title = The Whole Nine Yards 
| url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20000218/REVIEWS/2180305/1023
| newspaper = Chicago Sun Times
}} 

==Legacy==
A sequel titled The Whole Ten Yards with most of the original cast was released on April 9, 2004.

Awara Paagal Deewana, a Bollywood Film remake|remake,  was released on 21 June 2002.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 