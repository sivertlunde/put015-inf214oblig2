Three Flavours Cornetto trilogy
 
 
{{Infobox film
| name           = Three Flavours Cornetto Trilogy
| image          = File:Three Flavours Cornetto Trilogy DVD.JPG
| alt            =
| caption        = Cover of The Three Flavours Cornetto Trilogy DVD box set
| director       = Edgar Wright
| producer       = Nira Park Tim Bevan   Eric Fellner  
| writer         = Edgar Wright Simon Pegg See below) Steven Price  
| cinematography = David M. Dunlap   Jess Hall   Bill Pope  
| editing        = Chris Dickens   Paul Machliss  
| studio         = Working Title Films Big Talk Productions StudioCanal   Film4 Productions   Relativity Media  
| distributor    = United Kingdom:     Focus Features  
| released       =  
| runtime        = 329 minutes
| country        = United Kingdom United States France (HF )
| language       = English
| budget         = $38 million
| gross          = $156.7 million
}} comedic genre The Worlds End (2013).
 Cornetto ice cream as a hangover cure for Frosts character in Shaun of the Dead based on his own experiences. Within Hot Fuzz, Wright included a brief couple of throwaway scenes that called back to the Cornetto joke in Shaun. On the promotional tour of Hot Fuzz during production of The Worlds End, one interviewer pointed out the use of Cornetto in the first two films, to which Wright jokingly stated they represented a trilogy comparable to Krzysztof Kieślowskis The Three Colors trilogy|Three Colours film trilogy.       
 The Worlds End features the green mint chocolate chip flavour (though only shown by a wrapper caught in the wind) in a nod to aliens and science fiction.  According to Wright, Walls (ice cream)|Walls, manufacturer of the Cornetto, were "very pleased with the namecheck".    

Wright considered each of the films a "Trojan horse", "genre films that have a relationship comedy smuggled inside a zombie movie, a cop movie and a sci-fi movie".  Thematically, Wright saw each of the films containing common themes of "the individuals in a collective   about growing up and   about the dangers of perpetual adolescence".  Wright reworked the script of The Worlds End to culminate and conclude on these themes.  The films are further linked by a common set of actors. Wright, Park, Pegg, and Frost collaborated previously in the TV series Spaced from 1999 to 2001. Martin Freeman, Bill Nighy, Rafe Spall, Julia Deakin, Patricia Franklin, and Garth Jennings appear in each of the films as well as other projects by Wright and Pegg. Clark Collis observes in Entertainment Weekly that the films also feature "a running gag involving garden fences". 

==Films==
===Shaun of the Dead=== romantic zombie apocalyptic uprising of zombies.   

The trilogys Cornetto reference begins with a scene in which Shaun buys a cone for his friend Ed (Frost) at his request as Ed wakes up groggy and badly hung over after a night of drinking. Director Edgar Wright has said that he used to use Cornettos as a hangover cure. 

===Hot Fuzz=== buddy cop comedy film|comedy. The film stars Simon Pegg and Nick Frost as two police officers who investigate a series of mysterious deaths in a small English village. The two officers purchase Cornetto cones at a convenience store at various times, and a scrap of the wrapper falls onto the counter when Peggs character later makes other purchases at a motorway service station.

===The Worlds End=== The Worlds science fiction apocalyptic Comedy film|comedy. The film follows a group of friends, led by Simon Pegg, reattempting an epic pub crawl during an alien occupation of their home town. In the final scene of the film, a Cornetto wrapper blows past in a breeze, briefly catching on a wire fence.

Wright said in an interview for Entertainment Weekly, "We thought it would be a funny idea to do a sci-fi film where even the people who are going to be your saviors are hammered." 

==Recurring cast==
{| class="wikitable"
|- The Worlds End
|-
| Simon Pegg || Shaun || Sgt. Nicolas Angel || Gary King
|-
| Nick Frost || Ed || PC Danny Butterman || Andy Knightley
|-
| Julia Deakin || Mother of Yvonne || Mary Porter || B&B Landlady
|-
| Reece Shearsmith || Mark ||  || Collaborator
|- Tyres ||  || Rev. Green
|-
| Peter Serafinowicz || Pete ||  || Ding Dong Home Owner
|-
| Martin Freeman || Declan || Met Sgt. || Oliver Chamberlain
|-
| Bill Nighy || Phillip || Met Ch Inp. || The Network
|-
| Rafe Spall || Noel || DC Andy Cartwright || House Buyer
|-
| Patricia Franklin || Spinster || Annette Roper || Upstairs Beehive Lady
|- David Bradley ||  || Arthur Webley || Basil
|-
| Paddy Considine ||  || DS Andy Wainwright || Steven Prince
|-
| Alice Lowe ||  || Tina || House Buyer
|}

Discussing The Worlds End, Wright said that any actor who appeared in the first two films would also appear in the third, adding, "We even got back Nicola Cunningham, who played Mary the zombie in Shaun of the Dead. And Mark Donovan, so the first two zombies from Shaun of the Dead are in this. The twins are in it." 

==Reception==
===Box office===
{| class="wikitable" style="text-align: center; width:70%;"
|-
! Film
! Release Date
! Budget
! Box office (Worldwide)
! Ref.
|-
| Shaun of the Dead
| 9 April 2004
| $6,000,000
| $30,039,392
|   
|-
| Hot Fuzz
| 14 February 2007
| $12,000,000
| $80,736,657
|   
|- The Worlds End
| 19 July 2013
| $20,000,000
| $46,089,287	
|    
|- Total
| $38,000,000
| $156,865,336
|}

===Critical response===
{| class="wikitable" style="text-align: center; width:70%;"
|-
! Film
! Rotten Tomatoes
! Metacritic
|-
| Shaun of the Dead
|  % (  reviews) 
| 76 (34 reviews) 
|-
| Hot Fuzz
|  % (  reviews) 
| 81 (37 reviews) 
|- The Worlds End
|  % (  reviews) 
| 81 (45 reviews) 
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 