Winning
 
 
{{Infobox film
| name     = Winning
| image          = Winning poster.jpg John Foreman 
| director       = James Goldstone 
| writer         = Howard Rodman 
| starring       = Paul Newman Joanne Woodward Robert Wagner
| music         = Dave Grusin Richard Moore
| editing         = Edward A. Biery Richard C. Meyer
| studio = Newman-Foreman Company Jennings Lang
| distributor    = Universal Pictures  
| released   =  
| runtime        = 123 min
| language = English
| budget =
| gross = $14,644,335 
}} 1969 United American motion picture starring Paul Newman and Joanne Woodward. The film is about a racecar driver who aspires to win the Indianapolis 500. A number of racecar drivers and people associated with racing appear in the film, including Bobby Unser, Tony Hulman, Bobby Grim, Dan Gurney, Roger McCluskey, and Bruce Walkup.

==Plot summary==
Professional racecar driver Frank Capua (Paul Newman) meets divorcee Elora (Newmans real-life wife Joanne Woodward). After a whirlwind romance they are married. Charley (Richard Thomas), Eloras teenage son by her first husband, becomes very close to Frank, and helps him prepare his cars for his races. But Frank is so dedicated to his career that he neglects his wife, who has an affair with Franks main rival on the race track, Luther Erding (Robert Wagner). Frank finds them in bed together and storms out. The couple separate, but Frank still sees Charley regularly. Franks bitterness fuels his dedication to his work, and he becomes a much more aggressive driver. At the Indianapolis 500, Elora and Charley watch while Frank drives the race of his life and wins. After winning, Frank attends a victory party. He is uninterested when attractive women throw themselves at him, and he slips away. Luther finds Frank and apologizes to him for the affair, but Frank punches him. Frank visits Elora and tells her he wants to start again. Elora is unsure. The film ends with a freeze-frame as the two look uncertainly at each other.

==Main cast==
 
*Paul Newman as Frank Capua
*Joanne Woodward as Elora Capua
*Robert Wagner as Luther Lou Erding Richard Thomas as Charley Capua
*David Sheiner as Leo Crawford
*Clu Gulager as Larry the Mechanic
*Barry Ford as Bottineau
*Karen Arthur as Miss Dairy Queen
*Bobby Unser as Himself
*Tony Hulman as Himself
*Bobby Grim as Himself
*Dan Gurney as Himself
*Roger McCluskey as Himself
*Harry Basch as The Stranger Lou Palmer as Indianapolis Interviewer
*Robert Quarry as Sam Jagin
*Robert Shayne as Well-Wisher at Indy Victory
*Maxine Stuart as Miss Redburnes Mother
*Bruce Walkup as Driver No. 1
*Eileen Wesson as Miss Redburne
 

==Production==
During preparation for this film, Newman was trained for the motorsport by drivers Bob Sharp and Lake Underwood, at a race track high performance driving school—which sparked Newmans enthusiasm for the sport and led to his participation as a competitor in sports car racing during the remainder of his life. He would eventually launch the much successful Newman/Haas Racing with his long time racing competitor and friend Carl Haas winning more that 100 races and 8 Driers Championships in IndyCar Series.

The film includes footage taken at the Indianapolis Motor Speedway, the legendary 2.5 mile track. Most of the footage is from the 1968 race. The accident during the first green flag is from the 1966 race.

==Reception==
===Box Office===
The film earned an estimated $6.2 million in rentals in North America.  It was the 16th most popular movie at the US box office that year. 
===Critical===
Quentin Tarantino, when asked about his favorite race car films, was not a fan of Winning. "I’d rather saw my fingers off than sit through that again," he said. 

==Soundtrack==
The film score was by Dave Grusin, and the original soundtrack album was issued on Decca Records.   The opening moments of the films theme, "500 Miles," was used by WEWS-TV in Cleveland in the 1970s and 1980s as the theme for their Million Dollar Movie.  

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 