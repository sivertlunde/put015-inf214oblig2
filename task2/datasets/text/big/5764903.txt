The Nativity Story
{{Infobox film
| name = The Nativity Story
| image = The Nativity Story.jpg
| caption = Theatrical release poster
| director = Catherine Hardwicke
| producer = Toby Emmerich Marty Bowen Wyck Godfrey Cale Boyter Catherine Hardwicke Mike Rich Tim Van Rellim
| screenplay = Mike Rich
| based on = The Second Testament
| starring = Keisha Castle-Hughes Shohreh Aghdashloo Oscar Isaac Stanley Townsend Ciarán Hinds Hiam Abbass Shaun Toub
| music = Mychael Danna
| cinematography = Elliot Davis
| editing = Robert K. Lambert Stuart Levy
| studio = Temple Hill Entertainment
| distributor = New Line Cinema
| released =  
| runtime = 101 minutes
| country = United States
| language = English
| budget = $35 million
| gross = $46,432,264
}}
 2006 epic epic biblical drama film based on the nativity of Jesus starring Keisha Castle-Hughes and Shohreh Aghdashloo. Filming began on May 1, 2006, in Matera, Italy, and Ouarzazate, Morocco. Other scenes were shot in Craco, ghost town in the Italian region of Basilicata, and Cinecittà, Rome. {{cite web|title=
Nativity (2006) Filming Locations|url=http://www.imdb.com/title/tt0762121/locations?ref_=tt_dt_dt|work=|publisher=IMDB|accessdate=2 December 2013}}  New Line Cinema released it on December 1, 2006, in the United States and one week later on December 8 in the European Union. The film premiered in Vatican City November 27, 2006.    The Nativity Story was the first film to hold its world premiere in Vatican City. 

==Plot==
The plot begins with the portrayal of the Massacre of the Innocents in the Nativity. The remainder of the movie portrays the annunciation (conception) and birth of Jesus Christ to explain why King Herod the Great (Ciarán Hinds) ordered the murder.
 Mary (Keisha betrothed to Joseph of Elizabeth (Shohreh priest Zechariah Zachariah (Stanley death by stoning according to the Mosaic Law. At first Joseph does not believe Marys explanation that she was visited by the angel Gabriel, but decides not to accuse her. Although he is still shocked and angry, Joseph is later visited in a dream by the Angel Gabriel, who tells him of Gods plan for Marys son. Joseph then believes Mary, and is ashamed for his earlier doubts.

Meanwhile, Caesar Augustus, the Roman emperor, has demanded that every man and his family across the Roman Empire must return to his place of birth for the census. As a direct descendant of King David, Joseph is forced to travel   across Palestines rocky terrain from Nazareth to Bethlehem, the place of his birth. With Mary on a donkey laden with supplies, the couple took nearly four weeks to reach Bethlehem. Upon arriving in town, Mary goes into labour, and Joseph frantically seeks a place for her to deliver. There is, however, no room in any inn or home partly because of the census, but at the last minute an innkeeper offers his stable for shelter.
 align to form a great star. This Star of Bethlehem appears before the Magi, after a visit by the Angel Gabriel. The Magi visit King Herod the Great and reveal to him that the Messiah is still a child and he will be a Messiah for the lowest of men to the highest of kings. Shocked by this, Herod asks that they visit the newborn Messiah and report the childs location back to him, under the pretence that he, too, would like to worship him (what the Magi did not know was that Herod wanted to kill the baby for fear of a new king taking his throne). Later, the Magi arrive at the stable in which Mary is giving birth to Jesus, and they present the Infant with gifts of gold, frankincense, and myrrh.

After having been warned by the Angel in a dream, the Magi avoid Herod, but return to their home via a different route. King Herod realises that the Magi have tricked him, and carries out his plan of killing every boy in Bethlehem under the age of two. Joseph is warned in a dream of the danger and flees to Egypt with Mary and the child named Jesus to Egypt until Herod the Great dies and his two remaining sons (Herod was so paranoid that he killed his own wives, sons, and daughters) named Archelaus and Antipas take over his kingdom and Rome divides the kingdom- Archelaus gets the kingdom of Jerusalem, Bethlahem and Antipas gets the kingdom of Nazareth, Gailee.  At the end of the movie, Joseph, Mary, and Jesus return; but do not go to Bethlahem for fear of Herods oldest son and instead go to Nazareth.

==Cast== Mary
* Elizabeth
* Joseph
* Zechariah
* Ciarán Hinds as King Herod the Great
* Alessandro Giuggioli as Prince Archelaus and Antipas, Herods two and remaining sons Anna
* Shaun Toub as Joachim
* Alexander Siddig as The Angel Gabriel

==Reception==

===Box office===
The Nativity Story opened to a modest first weekend at the domestic box office by grossing $7.8 million,  with a 39% increase over the extended Christmas weekend.  After its initial run, the film closed out with about $37.6 million in domestic gross and $8.8 million in foreign gross, resulting in a worldwide total of almost $46.4 million on a reported $35 million budget. 

===Critical response===
The movie received mixed reviews. Review aggregator Rotten Tomatoes reports that 38% of 128 film critics have given the film a positive review, with a rating average of 5.3 out of 10.  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 52 based on 28 reviews. 
 Hail Mary an interest in finding a kernel of realism in the old story of a pregnant teenager in hard times. Buried in the pageantry, in other words, is an interesting movie."  Ann Hornaday of The Washington Post concluded a positive review of the film stating, "The most intriguing thing about The Nativity Story transpires during the couples extraordinary personal journey, advancing a radical idea in an otherwise long slog of a cinematic Sunday school lesson: that Jesus became who He was not only because He was the son of God, but because He was the son of a good man." 

Conversely, many critics felt that the film did not take the story to new cinematic heights. Owen Gleiberman of Entertainment Weekly noted, "The Nativity Story is a film of tame picture-book sincerity, but thats not the same thing as devotion. The movie is too tepid to feel, or see, the light."  Kenneth Turan of the Los Angeles Times said, "This is not a chance to experience the most timeless of stories as youve never seen it before but just the opposite: an opportunity, for those who want it, to encounter this story exactly the way its almost always been told." 

==Incidents==
Keisha Castle-Hughes became pregnant during filming and received a lot of media attention. 

==Music==
Mychael Dannas score of the film was released as an album on December 5, 2006. The album was nominated for a Dove Award for Instrumental Album of the Year at the 39th GMA Dove Awards. 

An album of songs inspired by the film was also released under the title The Nativity Story: Sacred Songs. It featured music by artists like Point of Grace, Amy Grant, Jaci Velasquez, and others. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 