Kathanayakudu (2008 film)
{{Infobox film
| name           = Kathanayakudu
| image          = Kathanayakudu rajinikanth.jpg
| caption        =
| director       = P. Vasu
| producer       = Ashwini Dutt|C.Ashwini Dutt   G.P. Vjaykumar
| writer         = Marudhuri Raja   Sreenivasan
| screenplay     = P. Vasu Meena Nayanthara
| music          = G. V. Prakash Kumar
| cinematography = Arvind Krishna
| editing        = Saravana
| studio         = Vyjayanthi Movies Ayngaran (Worldwide) Pyramid Saimira (United States|U.S.)
| released       =  
| runtime        = 2:25:47
| country        = India
| language       = Telugu
}} Tamil as Kuselan. Both remake the Malayalam film Kadha Parayumbol; Starring Rajinikanth, Jagapati Babu Meena Durairaj|Meena, Nayantara in the lead roles and music composed by G. V. Prakash Kumar. The film recorded as Average at box office.
 cinema actor in their youth. However due to their different pursuit in careers they eventually are forced to part ways, one becoming a national figure, the other, a village barber. Decades later, the actor returns to the village to participate in his films shooting. Whilst the entire village become excited about the prospect of seeing the actor, the barber fears that his old friend would have forgotten him and would neglect him. The story explores the pressures of friendship.

==Plot==
An emotional tale, the story is about two childhood friends Balakrishna (Jagapathi Babu) and Ashok Kumar (Rajinikanth). Balu does not stop at anything to make his friend feel happy. As time passes by, they take different paths and Balu becomes a barber in a small village called Siricilla. He gets married to Devi (Meena) and has three children and leads a happy life. However, his finances are dry and he is often led to the door of Dharmaraju (Dharmavarapu), the financier. On the other hand, Balu has tough competition from the more smart and clever Kantha Rao (Sunil) who has a saloon right opposite to his shop. Balu is a man with self-respect and honesty. His life continues to roll until one day there is news that a film shooting is going to happen in a place near their village and the hero of the film is Superstar Ashok Kumar (Rajinikanth). The news spreads like wildfire and Balu knows this but hesitates to show himself as a friend of Ashok Kumar. On the other hand, folks around get to know the friendship of Balu and Ashok Kumar and in this process those who have been ridiculing him start doing favors for him only with the intention of meeting Ashok Kumar or at least watching him from outside. But hesitant Balu is unable to do what they want and soon people start shunning him, even his own children start showing anger towards him.

==Cast==
{{columns-list|3|
* Jagapati Babu as Bala Krishna / Balu Meena as Shridevi
* Nayanthara as herself
* Mamta Mohandas as assistant director Prabhu
* Vijayakumar as himself
* Brahmanandam Ali
* Sunil as Shanmugam
* M. S. Narayana
* Dharmavarapu Subramanyam
* Rajinikanth as Ashok Kumar
* Tanikella Bharani
* Nizhalgal Ravi Kondavalasa
* Rallapalli
* Vijayaranga Raju
* R. Sundarrajan (director)|R. Sundarrajan
* Manobala 
* Chinni Jayanth
* Rajababu Chitti Babu
* Ananth
* Gundu Hanumantha Rao 
* Gowtham Raju
* Mohan Ram
* Dalapathi Dinesh
* Aajam
* Potti Rambabu
* Venu
* Garimalla Visweswara Rao
* Duvvasi Mohan
* Narsing Yadav Geetha
* Rajitha
* Fatima Babu Sona
* Saira Banu
* Bhavana
* Shilpa
* Master Amal
* Baby Shafna
* Baby Revathi
}}

The lead cast in the bilingual (Tamil/Telugu) included: 
* Jagapati Babu as Bala Krishna. Bala Krishna, more commonly referred to as Bala is a barber. He lives care-free with his wife and three children despite his financial struggles. His salon fails to earn profits ude to his reluctance to take in assistance from others. Bala has a dilemma when his old friend Ashok Kumar returns to the village. Meena as Shridevi. Shridevi is the wife of Bala, who is commonly known for being rather boisterous and open hearted despite her poor state of living. She shares great affection for her husband despite the villagers hate towards him.
* Rajinikanth as Ashok Kumar. Ashok Kumar is dubbed as the "superstar" in South Indian cinema, who comes to the village to participate in his films shooting. The arrival of him prompts the villagers excitement, with all of them wanting to see the superstar.
* Nayantara as herself. Nayantara is the co-star to Ashok Kumar in the film under production in the village.

==Soundtrack==

{{Infobox album
| Name        = Kathanayakudu
| Tagline     = 
| Type        = film
| Artist      = G. V. Prakash Kumar
| Cover       = 
| Released    = 2008
| Recorded    = 
| Genre       = Soundtrack
| Length      = 29:43
| Label       = Big Music
| Producer    = G. V. Prakash Kumar
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

Music composed by G. V. Prakash Kumar. Music released on Big Music Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 29:43
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Cinema Cinema 
| lyrics1 = Ananta Sriram
| extra1  = Shankar Mahadevan 
| length1 = 6:08

| title2  = Challe Challe
| lyrics2 = Veturi Sundararama Murthy
| extra2  = Hariharan (singer)|Hariharan,Sujatha Mohan|Sujatha,Baby Ranjini,Baby Pooja  
| length2 = 6:13

| title3  = Om Zaarare
| lyrics3 = Ramajogayya Sastry
| extra3  = Daler Mehndi,K. S. Chithra|Chitra,Sadhana Sargam
| length3 = 7:12

| title4  = Vatche Vatche
| lyrics4 = Veturi Sundararama Murthy
| extra4  = Shreya Ghoshal
| length4 = 4:34

| title5  = Ra Ra Ra Ramayanna
| lyrics5 = Ananta Sriram 
| extra5  = Kailash Kher,V. Prasanna
| length5 = 5:36
}}
 Rajkumar are shown with each chorus of this song.  The verse alternates between clips shot of an actual movie crew filming on location, intercut with real-life superstar actor Rajinikanth — who plays a fictional superstar actor in this film — spoofing blockbuster movies such as Zorro, Lawrence of Arabia, House of Flying Daggers, and the James Bond series. 

{| class="wikitable"
|- style="background:#cccccf; text-align:center;"
! No. !! Song !! Singers !! Length (m:ss) !! Notes
|-
| 1|| " 
|-
| 2|| "Challe Challe" || Hariharan (singer)|Hariharan, Sujatha Mohan, Baby Ranjini & Baby Pooja || 6:13 || A melody picturized on Balakrishnans family
|-
| 3|| "Om Zaarare" ||   and Nayantara
|-
| 4|| "Vatche Vatche" ||  
|-
| 5|| "Ra Ra Ra Ramayanna" || Kailash Kher, V. Prasanna || 5:36 || An interlude with the villagers singing in praise of Balakrishnan after discovering his links with Ashok Kumar
|}

==Development==
Following P. Vasu and Rajinikanths film, Chandramukhi in 2005, Vasu had been keen to re-cast Rajinikanth in another role and, before signing Kuselan, he had narrated a story titled Vettaiyan, which would have been a sequel of a character featured in Chandramukhi.   
Early in 2008, Rajinikanth signed up for S. Shankars Enthiran, while Kathanayakudu was launched at the Taj Coromandel in Chennai on 14 January 2008 coinciding with Pongal. The director, P. Vasu signed up Rajinikanth and Jagapathi Babu to portray the lead roles, while director K. Balachander agreed to produce the Tamil version of the film along with G.P. Vijayakumars Seven Arts Productions, while Aswani Dutt agreed to produce the film in Telugu.  The film is a remake of the Malayalam movie, Kadha Parayumbol which was written by Sreenivasan who also played the lead role in the movie. The shooting lasted 82 days with the versions being shot simultaneously and that most of the movie was shot inside the Ramoji Rao film city, with other destinations including Kerala and Pollachi. The promotional event took place on 19 July 2008 at the Jawarhalal Nehru Stadium.   
 AVM Studios in Chennai with the leading artistes present. {{cite web|author=|year=2008|title=Kuselan pooja held
|publisher=Sify.com|accessdate=2008-03-07 |url=http://sify.com/movies/tamil/fullstory.php?id=14618571}}  P. Vasu, while talking to the media mentioned that the shooting lasted 82 days with the versions being shot simultaneously and that most of the movie was shot inside the Ramoji Rao film city, with other destinations including Kerala and Pollachi. {{cite web|author=|year=2008|title=P Vasu Interview
|publisher=Behindwoods.com.com|accessdate=2008-06-19|url=http://www.behindwoods.com/tamil-movie-news-1/jun-08-03/kuselan-19-06-08.html}} 

===Casting=== Venu Madhav all signed up to play the roles of Jagapati Babus fellow villagers.    Ileana DCruz turned down an offer to act in the film, citing date problems. {{cite web|author=|year=2008|title=Ileana refuses Rajinis Kuselan Telugu actors had been approached to be a part of a song in the project, but none were selected.      

Technicians were common in both versions in the two films with G. V. Prakash Kumar operating as the music composer, whilst Arvind Krishna was the cinematographer for the bilingual.   

==References==
 

==External links==
*  
*   At Basthi.com

 
 
 
 

 
 