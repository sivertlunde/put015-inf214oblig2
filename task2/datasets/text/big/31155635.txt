Postman (1967 film)
{{Infobox film 
| name           = Postman
| image          =
| caption        =
| director       = PA Thomas
| producer       = PA Thomas
| writer         = PA Thomas Jagathy NK Achari (dialogues)
| screenplay     = PA Thomas Sathyan Kaviyoor Hari
| music          = BA Chidambaranath
| cinematography = PB Maniyam
| editing        = Ceylon Mani
| studio         = Thomas Pictures
| distributor    = Thomas Pictures
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam film, Hari in lead roles. The film had musical score by BA Chidambaranath.   

==Cast== Sathyan
*Kaviyoor Ponnamma
*Thikkurissi Sukumaran Nair Hari
*O Ramdas
*T. R. Omana
*K. P. Ummer
*Kamaladevi
*K. V. Shanthi

==Soundtrack==
The music was composed by BA Chidambaranath and lyrics was written by Vayalar Ramavarma and Irayimman Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Arimullavalli || P Jayachandran || Vayalar Ramavarma || 
|-
| 2 || Gokulapaala || P. Leela || Vayalar Ramavarma || 
|-
| 3 || Kaarmukile - Kannuneer Kadal || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 4 || Kumbalam Nattu || B Vasantha, Zero Babu || Vayalar Ramavarma || 
|-
| 5 || Narthaki Narthaki || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 6 || Omanathinkal Kidaavo || K. J. Yesudas, B Vasantha || Irayimman Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 