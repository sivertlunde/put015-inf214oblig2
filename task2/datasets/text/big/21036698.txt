Amara Deepam (1956 film)
{{Infobox film
| name     = Amara Deepam அமர தீபம்
| image    = Amara_Deepam.jpg
| caption  = Official DVD Box Cover
| director = Tatineni Prakash Rao |T. Prakash Rao
| writer   = C. V. Sridhar
| screenplay = Tatineni Prakash Rao |T. Prakash Rao
| story    = C. V. Sridhar Savitri Padmini Padmini
| producer = S. Krishnamoorthy T. Govindarajan C. V. Sridhar
| music    = T. Chalapathi Rao G. Ramanathan
| cinematography = Kamal Ghose A. Vincent
| editing  = N. M. Sankar
| studio   = Venus Pictures
| distributor = Venus Pictures
| released = 20 June 1956
| runtime  = 162 mins
| country  = India Tamil
| budget   =
}}
 Savitri and Padmini in lead roles.  Venus Pictures, had musical score by T. Chalapathi Rao and G. Ramanathan and was released on 20 June 1956. 

==Plot==
An educated unemployed person Ashok (Sivaji Ganesan) falls in love with the daughter Aruna (Savitri (actress)|Savitri) of a wealthy Industrialist.

Aruna is taken hostage by her husband -to -be, Sukumar (M. N. Nambiar), and while Ashok trails them to save Aruna, he loses his memory in an accident. Ashok meets Roopa (Padmini (actress)|Padmini) and they begin to like each other.  Sometime later Aruna comes back and as Ashok fails to recognize Aruna things look complicated.

The rest of the film deals with how Ashok struggles with his memory and how both Aruna and Roopa try to win him over.

==Cast==
*Sivaji Ganesan as Ashok Savitri as Aruna Padmini as Roopa
*M. N. Nambiar
*Chittor V. Nagaiah
*K. A. Thangavelu 
*E. V. Saroja

==Crew==
*Producer: S. Krishnamoorthy, T. Govindarajan, C. V. Sridhar
*Production Company: Venus Pictures
*Director: Tatineni Prakash Rao |T. Prakash Rao 
*Music: G. Ramanathan & T. Chalapathi Rao
*Story: C. V. Sridhar
*Screenplay: Tatineni Prakash Rao |T. Prakash Rao
*Dialogues: C. V. Sridhar
*Art Direction: Thotta Venkateswara Rao
*Editing:N. M. Sankar
*Choreography: Hiralal
*Cinematography: Kamal Ghose, A. Vincent
*Stunt: None
*Dance: None

==Soundtrack== Playback singers are T. M. Soundararajan, A. M. Rajah, Seerkazhi Govindarajan, S. C. Krishnan, M. L. Vasanthakumari, Jikki, A. P. Komala, P. Suseela & T. V. Rathinam.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Nanaayam Manushanukku || T. M. Soundararajan || K. P. Kamatchi Sundharam || 02:43
|- 
| 2 || Jalilo Jimkana  || Jikki || Thanjai N. Ramaiah Dass || 03:51
|- 
| 3 || Thenunnum Vandu Maamalarai Kandu || A. M. Rajah & P. Suseela || K. P. Kamatchi Sundharam || 03:34
|- 
| 4 || Ellorum Koodi Aadi Paadi || Jikki || M. K. Aathmanathan || 03:56
|- 
| 5 || Nadodikkottam Nannga Jillelelo || T. M. Soundararajan, A. P. Komala, Seerkazhi Govindarajan & T. V. Rathinam || Udumalai Narayana Kavi || 05:41
|- 
| 6 || Pachchai Kilipaadudhu || Jikki || A. Maruthakasi || 03:13
|- 
| 7 || Kottaikatti Kaavikatti || S. C. Krishnan & A. P. Komala || K. S. Gopalakrishnan || 03:37
|- 
| 8 || Thumbam Soozhum Neram || Jikki || K. S. Gopalakrishnan  || 01:48
|-
| 9 ||  || M. L. Vasanthakumari ||  || 
|}

==References==
 

==External links==
*  

 
 
 
 


 