Invitation to a Gunfighter
 
{{Infobox film
| name           = Invitation to a Gunfighter
| image          =
| image_size     =
| alt            =
| caption        = Richard Wilson
| producer       = Richard Wilson
| writer         = Hal Goodman Larry Klein
| narrator       =
| starring       = Yul Brynner Janice Rule George Segal
| music          = David Raksin
| cinematography = Joseph MacDonald
| editing        = Robert C. Jones
| studio         = Hermes Productions
| distributor    = United Artists
| released       =  
| runtime        = 92 min.
| country        = United States English
| budget         = $1.8 million Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 146 
| gross          = $3.1 million 
| preceded by    =
| followed by    =
}}
 Western directed by Richard Wilson, starring Yul Brynner and George Segal. It was based on a 1957 teleplay by Larry Klien that appeared on Playhouse 90. 

==Plot==
 Confederate veteran Civil War and discovers his farm was sold by a banker named Brewster (Pat Hingle). His fiancee Ruth (Janice Rule) has married another man in his absence.
 Union sympathizers in town resent Weavers allegiance to the Rebels during the war. His town turns against him, and soon Brewster hires a mulatto gunman named dEstaing (Yul Brynner) to come to town and provoke Weaver into a fatal fight.

==Cast==
* Yul Brynner as Jules Gaspard dEstaing
* Janice Rule as Ruth Adams
* George Segal as Matt Weaver
* Alfred Ryder as Doc Barker
* Clifford David as Crane Adams
* Mike Kellin as Blind Union vet
* Brad Dexter as Kenarsie
* Pat Hingle as Sam Brewster
* Bert Freed as Sheriff
* John A. Alonzo as Manuel (Weavers friend in Mexicantown) (as John Alonzo)
* Curt Conway as McKeever
* Clarke Gordon as Hickman
* Gerald Hiken as Gully
* Strother Martin as Fiddler
* Clifton James as Tuttle
* Dal Jenkins as Dancer

==Reception==
The film recorded a loss of $900,000. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 


 