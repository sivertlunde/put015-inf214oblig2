Dhruva Thare
{{Infobox film
| name           = Dhruva Thare
| image          = 
| image_size     = 
| caption        = 
| director       = M. S. Rajashekar
| producer       = S. A. Govindraj   V. Bharathwaj
| writer         = Vijay Sasnur (Aparanji novel)
| narrator       =  Rajkumar Geetha Geetha Deepa
| music          = Upendra Kumar
| cinematography = B. C. Gowrishankar
| editing        = P. Bhaktavatsalam
| studio         = Nirupama Art Combines
| distributor    = 
| released       = 1985
| runtime        = 143 min
| country        = India
| language       = Kannada
| budget         =
}} Kannada film Geetha and Deepa in lead roles.  The movie is famous for its evergreen songs which was composed by Upendra Kumar. Based on the novel Aparanji written by Vijay Sasanur, the film was a major success at the box-office upon its release.

==Plot==
Sagar is a well known city lawyer who is at the truth side all the time. He falls in love with Sudha a college going student and gets married to her. He develops his inner talent of painting and goes commercial with it. A grave disaster strikes his family which later becomes a legal case. Would he still be a painter and let go of the case or fight the case for truth forms the rest of the story.

==Cast== Rajkumar as Sagar Geetha as Sudha
* Deepa
* Thoogudeepa Srinivas Balakrishna
* Shivaram
* Ashwath Narayan
* Rajanand

==Soundtrack==
{{Infobox album  
| Name        = Dhruva Thare
| Type        = Soundtrack
| Artist      = Upendra Kumar
| Cover       = 
| Released    =  1985
| Recorded    = Feature film soundtrack
| Length      =
| Label       = Sangeetha 
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Lyrics
|-
| 1 || Aa Moda Banalli || Rajkumar (actor)|Dr. Rajkumar, Vani Jayaram, Bangalore Latha || Chi. Udaya Shankar
|-
| 2 || Aa Rathiye Dharegilidante || Rajkumar, Bangalore Latha || Chi. Udaya Shankar
|-
| 3 || O Nalle Savinudiya || Rajkumar, Vani Jayaram ||Chi. Udaya Shankar
|-
| 4 || Nyayavelli Adagide  || Rajkumar, S. Janaki || Chi. Udaya Shankar
|-
|}

==Awards==
* Karnataka State Film Awards
# Second Best Film
# Best Story writer - Vijay Sasnur
# Best Cinematographer - B. C. Gowrishankar

==References==
 
*  

==External Source==
* 
* 

 
 
 
 
 


 