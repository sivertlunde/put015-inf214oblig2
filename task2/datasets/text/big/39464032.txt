Meschugge
{{Infobox film
| name        =Meschugge
| director    = Dani Levy
| image       = Meschugge.jpg
| producer    = Stefan Arndt
| associate producer = 
| writer      = Dani Levy Maria Schrader
| starring    = 
| distributor = Jugendfilm
| released    = 1998
| runtime     = 107 minutes
| country     = Germany English German German
| budget      =
| gross       =
}}

Meschugge (English title - The Giraffe) is a 1998 German thriller film directed by Dani Levy and set during World War II. The German title roughly translates as "Insanity|crazy". The English title refers to the nickname of a character who was once in charge of the Treblinka extermination camp. The film features mainly English dialogue though features German dialogue as well.

==Cast==
*Maria Schrader as Lena Katz
*Lukas Ammann as Eliah Goldberg
*Lynn Cohen as Mrs Fish
*Dani Levy as David Fish
*David Strathairn as  Charles Kaminski
*Nicole Heesters as Lenas mother
*Lukas Ammann as Eliah Goldberg

==Reception==
Variety (magazine)|Variety gave a mixed review, calling the film "slickly shot" though criticising the plot and dialogue as "ordinary".  The New York Times was much more critical, stating the English dialogue seemed like "badly translated German" and the plots "breathless incoherence   matched only by its wild implausibility." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 