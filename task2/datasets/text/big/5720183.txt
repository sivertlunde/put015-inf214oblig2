Jhoom Barabar Jhoom
 
 
 
{{Infobox film
| name = Jhoom Barabar Jhoom
| image = Jhoombarabarjhoom.JPG
| caption = Movie poster for Jhoom Barabar Jhoom
| writer = Habib Faisal
| starring = Abhishek Bachchan Bobby Deol Preity Zinta Lara Dutta
| director = Shaad Ali
| producer = Yash Chopra Aditya Chopra
| cinematography = Ayananka Bose
| editing = Ritesh Soni
| distributor = Yash Raj Films Pvt. Ltd
| released =  
| runtime = 132 min.
| language = Hindi
| country = India Gulzar (Lyrics)
| budget = Rs. 250&nbsp;million
}}
Jhoom Barabar Jhoom is a 2007 Bollywood film starring Abhishek Bachchan, Preity Zinta, Bobby Deol and Lara Dutta. It is directed and co-produced by Shaad Ali. The film is produced by Aditya Chopra and Yash Chopra under Yash Raj Films.    The film released on 15 June 2007.

== Plot ==
The film begins with a mysterious  -like musician (Amitabh Bachchan) leading the crowds at Waterloo station in London in a dance to the title song.
 Punjabi from assimilated into share a table in a café together and to kill the time they talk about how they met their partners-to-be.

Rikki says that he met his fiancé, Anaida Raza, at Hôtel Ritz Paris, the same night that Diana, Princess of Wales and Dodi Al-Fayed left the hotel to take their last journey together. Rikki explains "When two lovers die, another two are born", as he fell in love with Anaida that night. Alvira says she met her fiancé, the dashing lawyer Steve Singh at Madame Tussauds in London when he saved her from death by a falling Superman wax model. The encounter changed her life and she was smitten by the lawyer who also helped her sue Madame Tussauds for substantial damages.

As Rikki and Alvira talk they begin to enjoy each others company and their different backgrounds cease to matter. They exchange numbers and go to meet their partners. However, it is then revealed that they had not gone to meet their respective partners. Rikki is in fact at the train station to meet his business partner, while Alvira is meeting her relatives. As they leave the station separately, they realise they are in love, but believe their love is unrequited, each of the other.

The two get in touch when Alvira calls Rikki, pretending it was a wrong number. They decide to meet up at a disco where there is a dance competition but both try to maintain that they are engaged. Out of desperation, Rikki hires Laila (Lara Dutta), a prostitute, to pose as Anaida, and Alvira fools and then blackmails Satvinder (Bobby Deol) to pretend to be Steve. The four meet at a club and take part in the dance competition, while throwing insults at each other. Rikki and Laila emerge as the winners but Alvira is jealous and storms out in tears.

Satvinder goes to Rikkis flat and tells him that he has fallen in love with Laila. He also tells him that Alvira is not engaged to him. Rikki, realising what has happened, goes to see Alvira, who is at first moping in bed and later tries to convince her cousin not to marry her. Rikki calls her from a neighbours window. Then they confront each other and confess that they love each other and start to date while Satvinder and Laila go to Hollywood.

The film ends with the mysterious gypsy musician showing how Rikki and Alvira invented the stories about the non-existent lovers by taking inspiration from Alviras newspaper and Rikkis comic book.

== Cast ==
* Abhishek Bachchan as Rikki Thukra
* Bobby Deol as Steve Singh / Satvinder Singh
* Preity Zinta as Alvira Khan
* Lara Dutta as Anaida Raza / Laila
* Amitabh Bachchan as man singing at the train station (Cameo)
* Piyush Mishra as Hafeez (Huffy) Bhai
* Ameet Chana as Shahriyar
* Meera Syal as Satvinders mother
* Sanjeev Bhaskar as Shopkeeper
* Sagar Arya as Neil

== Production == John Abraham were first approached for the roles of Anaida Raza and Steve Singh.    When they refused Dutta and Deol were roped in. Amitabh Bachchan has been credited with a special appearance in the film.
 Green Lanes, Harringay in North London.

Abhishek Bachchan and Bobby Deol worked together for the first time in this film; their fathers (Amitabh Bachchan and Dharmendra) had worked together in the hit Sholay (1975). The film features a scene in which they travel in a scooter like their fathers had done in Sholay, only this time Bobby Deol is driving and Abhishek Bachchan is on the passenger seat.    The film re-unites Bobby Deol and Preity Zinta who starred in Soldier (1998 Indian film)|Soldier in 1998      as well as Abhishek Bachchan and Lara Dutta who co-starred in Mumbai Se Aaya Mera Dost.
 Stamford Bridge Chelsea football team would be part of the film,      but this scene did not appear in the completed film.

The first trailer for the film was shown on 27 April 2007 with the release of the film Ta Ra Rum Pum.   

== Soundtrack ==
{{Infobox album |  
 Name = Jhoom Barabar Jhoom
| Type = Album
| Artist = Shankar-Ehsaan-Loy
| Cover = Jhoom Barabar Jhoom OST.jpg
| Released =  15 May 2007 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  Yash Raj Music
| Producer = Shankar-Ehsaan-Loy
| Reviews =
| Last album =   (2006)
| This album = Jhoom Barabar Jhoom (2007)
| Next album = Heyy Babyy (2007)
}}

===Development=== hip hopish techno song during the climax scenes of the movie, which is picturised on Abhishek Bachchan, Preity Zinta, Bobby Deol and Lara Dutta. "Jhoom Jam" is the only instrumental in the album, which as title suggests, is the jam of the instrumental version of all track of the album.  

===Release history=== MP3 files, audio CD, and the DVD-Audio.

===Tracklist===

{| class="wikitable"
|-  style="background:#ccc; text-align:centr;"
! Song !! Singer(s)!! Duration !! Picturised on
|-
|Jhoom
| Shankar Mahadevan 
| 5.21
| Amitabh Bachchan
|-
| Kiss of Love
| Vishal Dadlani & Vasundhara Das
| 5.05
| Bobby Deol & Preity Zinta
|-
| Ticket to Hollywood
| Neeraj Shridhar & Alisha Chinai
| 4.38
| Abhishek Bachchan & Lara Dutta
|-
| JBJ
| Zubeen Garg, Shankar Mahadevan & Sunidhi Chauhan
| 4.22
| Abhishek Bachchan, Bobby Deol, Preity Zinta & Lara Dutta
|-
| Bol Na Halke Halke
| Rahat Fateh Ali Khan & Mahalakshmi Iyer
| 4.54
| Abhishek Bachchan & Preity Zinta
|-
| Jhoom Barabar Jhoom
| Krishnakumar Kunnath|KK, Sukhwinder Singh, Mahalakshmi Iyer & Shankar Mahadevan
| 7.04
| Abhishek Bachchan, Bobby Deol, Preity Zinta, Lara Dutta & Amitabh Bachchan
|-
|Jhoom Jam
| Instrumental
| 3.50 Credits Scenes/Bloopers
|}

===Reception===

{{Album ratings
| rev1 = Bollywood Hungama
| rev1Score =   
| rev2 = Planet Bollywood
| rev2Score =   
| rev3 = Business of Cinema
| rev3Score =   
}}
The soundtrack garnered positive reviews from almost all critics.

Joginder Tuteja of Bollywood Hungama said, Jhoom Barabar Jhoom is an album that has chart-buster written on it on bold letters. There are some albums that take time to register with music lovers. There are some that require strong word of mouth to carry them forward. There are some which are destined to be popular in the very first listening. Jhoom Barabar Jhoom of course belongs to the last category.    Sukanya Verma of Rediff described the album as high on melody, entertainment and attitude.  Gianysh Toolsee of Planet Bollywood in his review, stated, "SEL successfully manage to hit the bulls eye with their proper use of zingy sounds, effective arrangements, lively tunes, rhythmic variations and groovy beats."    "SEL, the most prolific composers of last year maintains their golden streak of success with JHOOM BARABAR JHOOM.", said the GlamSham review. Sanjay Ram of Businessofcinema.com stated that that the album is positively a must buy.   

The album featured in the "Top 10 soundtracks of the year" list by Bollywood Hungama  and Rediff 

===Chart performance===
The music for the film entered the charts at number 6    and in its second week climbed to number five.    By the first week of July, the album topped the music charts and stayed there for two weeks.    Despite the poor response to the film, the music has received much acclaim    On the week beginning 23 July the album was placed at number three    On the week beginning 17 September the soundtrack dropped to number 10.   

== Reception ==

=== Box office ===
The film opened to a 65–70% response, which was below expectations for a Yash Raj film.    As per IBOS, the film has opened to a good response and has taken the biggest opening of the year at the box office.   
 Punjab and Haryana.    The film has opened well in big cities such as Mumbai with a 75% turnout and Delhi which had a 70% response. In the south it opened to a poor response due to the release of the Tamil film Sivaji (film)|Sivaji. However, all the big theatres reached to a 90%+ response with many being 100%. 

International box office performance was considered to be average. In its first week, the film made £264,347 and debuted at number 6 in the UK charts. In the US, the film debuted at number 16 on the film chart making $455,257 in its first week. In Australia, the film made $91,485 debuting at number 10.    The film has made Rs 19–200&nbsp;million in its first week and debuted at number 1    but the collections have begun to fall.    The film has done better in the UK than in the US As of 27 June, the film was positioned at number 9 on the UK charts    and at 21 on the U.S chart.    The film saw a drop in collections, and has been declared a flop in India  but the collections have begun to fall.  The film has done better in the UK than in the US As of 27 June 2007, the film was positioned at number 9 on the UK charts  and at 21 on the US chart.  In the UK the film has performed moderately well. Overall, the film has been declared an above average.

=== Reviews ===
Indicine.com rated the film at 4 out 5, saying the movie is a fun ride and is well worth the ticket money. It is the kind of movie that can wash away your blues.    Taran Adarsh of indiafm.com gave the film a 1.5 out of 5 rating, saying the film is all gloss, no substance.    The lead actors have received mixed reviews with Taran Adarsh saying that the film belongs to Abhishek first and Preity next.  Hindustantimes.com gave the film 2 stars,    as did glamsham.com,    with the latter saying the performances were good. Sanjay Ram of businessofcinema.com said though Jhoom Barabar Jhoom offers the colour it is all but a short story stretched. The film received 2 out of 5 stars from the website    The film has been criticised for its screenplay but the lead actors have received mixed reviews.    Moviewalah.com gave the film 2 out of 5 stars but praised the performances to a certain extent    BollywoodArchive.com gave the film 2.5 out of 5 stars says its a good light hearted entertainer with good music from Shankar Ehsaan Loy   

== See also ==
*List of films set in London

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 