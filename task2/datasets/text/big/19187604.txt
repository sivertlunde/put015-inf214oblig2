Dvanáct křesel
{{Infobox film
| name           = Dvanáct křesel
| image          =Adolf Dymsza.jpg
| image_size     = 180px
| caption        = Adolf Dymsza in the film
| director       = Michał Waszyński Martin Frič
| producer       = 
| writer         = Ilf and Petrov (novel) Karel Lamač (screenplay)
| narrator       = 
| starring       = 
| music          =    
| cinematography =  
| editing        = 
| distributor    = 
| released       = 6 October 1933
| runtime        = 66 minutes
| country        = Poland Czechoslovakia
| language       = Czech
| budget         = 
}}
 1933 Poland|Polish-Czechoslovakia|Czechoslovak comedy film directed by Michał Waszyński and Martin Frič    freely based on the Russian novel "Twelve Chairs" by Ilya Ilf and Evgeny Petrov.

==Cast==
*Vlasta Burian ...  Ferdinand Šuplátko 
*Adolf Dymsza ...  antiquary Wladyslaw Kepka 
*Zula Pogorzelska ...  director of the orphanage 
*Zofia Jaroszewska ...  employee of the orphanage  
*Wiktor Biegański ...  professor - spiritist 
*Stanisław Belski ...  owner of the furniture shop 
*Wanda Jarszewska   
*Lo Kittay   
*Józef Kondrat ...  chauffeur 
*Eugeniusz Koszutski ...  clerk Repecki 
*Aniela Miszczykówna ...  dentist 
*Hanna Parysiewicz   
*Stefan Szczuka ...  auctioneer 
*Helena Zarembina

==References==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 


 
 