Executive Decision
 
{{Infobox film
| name           = Executive Decision
| image          = Executive decision ver1.jpg
| caption        = Theatrical release poster
| director       = Stuart Baird
| producer       = Joel Silver
| writer         = {{plainlist| Jim Thomas John Thomas
}}
| starring       = {{plainlist|
* Kurt Russell
* Halle Berry
* John Leguizamo
* Oliver Platt
* Steven Seagal
}}
| music          = Jerry Goldsmith Alex Thomson
| editing        = {{plainlist|
* Stuart Baird
* Frank J. Urioste
}} Warner Bros. Pictures
| released       =  
| runtime        = 133 minutes
| country        = United States
| language       = English
| budget         = $55 million   
| gross          = $122.1 million 
}}
Executive Decision is a 1996 American action film directed by Stuart Baird in his directorial debut. The film stars Kurt Russell, Steven Seagal, Halle Berry, Oliver Platt  and John Leguizamo. The film was released in the United States on March 15, 1996.

== Plot == Lieutenant Colonel Austin Travis leads an unsuccessful raid on a Chechen mafia safe house in Italy by a Special Forces (United States Army)|U.S. Army Special Forces team to recover a stolen Soviet nerve agent, DZ-5. One of his men is killed during the raid.

Dr. David Grant, a United States Naval Academy graduate and now a consultant for the U.S. Armys intelligence community, is informed that the worlds most feared terrorist, El Sayed Jaffa, has been taken into custody. Shortly after, Oceanic Airlines Flight 343, a Boeing 747-200, leaves Athens, Greece, bound for Washington, D.C. It is hijacked by Jaffas lieutenant, Nagi Hassan and a number of Jaffas men.

Grant is summoned to the Pentagon to join a team led by Travis which is being readied to intercept the hijacked plane. They listen to Hassans demand for the release of Jaffa. Grant, however, does not believe Hassan wants Jaffa released. He believes that Hassan actually arranged for Jaffas capture, that the hijacked plane is carrying a bomb loaded with DZ-5 nerve gas and that Hassan wants to detonate the bomb over U.S. airspace.

A plan is worked out that will involve a mid-air transfer of a special operations team onto the hijacked airliner using an experimental Lockheed F-117 Nighthawk|F-117 stealth aircraft. The plan is approved and Travis assembles his team at a U.S Air Force base. They board with Grant and engineer Dennis Cahill.

The boarding is only partially successful. When an operator, "Cappy", is seriously injured, Grant, who was supposed to stay put, boards to help lift Cappy into the plane. The 747 pulls up, though, putting too much stress on the boarding sleeve. Unable to board the plane, Travis sacrifices himself when he closes the 747s hatch, just as the sleeve breaks and he is blown from the F-117 into open air. Those who survived insertion make it to the 747s lower deck, but with half their equipment and no communication. It is assumed back at the Pentagon that the team did not make it aboard.

With limited options, the operators begin to search for the supposed DZ-5 bomb. Grant manages to make contact with a flight attendant, Jean, despite Hassans suspicions and asks her for assistance in finding the bombs remote detonator.

Officials decide to release Jaffa in order to resolve the situation. Meanwhile, Cappy and Cahill locate and start to dismantle the bomb. They discover that the bombs arming device is barometrically activated. They seemingly disarm the bomb, but it is revealed that there is another trigger.

Jaffa calls Hassan from a private jet, telling him he is free, but Hassan will not be swayed from his plan. Grant realizes that Hassans men dont know about the bomb, which means there is a sleeper on board, one passenger among 400.

U.S. Senator Mavros is called away from his seat to have a word with the President of the United States only to realize hes to be sacrificed as a warning that Hassan is serious. Hassan points a gun to Mavros head as he tries in vain to get the President to listen, but is shot in the head. However, Jean spots a man with an electronic device and informs Grant. Meanwhile, the soldiers manage to use the planes taillights and Morse code signal to the U.S. Navy fighter jets that they are on board and not to shoot them down.
 federal air explosive decompression which blows three passengers and Demou out of the plane. The remaining terrorists are killed during the exchange, the bomb is finally disarmed, and the plane is able to regain its stability. In a last act of desperation, a seriously wounded Hassan kills both pilots, hoping the bomb will detonate if the plane crashes. Hassan is killed by wounded operator "Rat".
 misses the Frederick Field, which is where he normally practices flying. Deciding to try and land the 747 there, with Jeans assistance, Grant makes a sloppy but safe landing as hes unable to stop before reaching the end of the airports relatively short runway. The 747 is slowed to a stop by ramming into a sand berm at the runways overrun area where emergency workers are able to safely evacuate the remaining passengers.

== Cast ==
* Kurt Russell as Dr. David Grant
* Steven Seagal as Lieutenant Colonel Austin Travis
* Halle Berry as Jean
* John Leguizamo as Master Sergeant Carlos "Rat" Lopez
* David Suchet as Nagi Hassan
* Oliver Platt as Dennis Cahill
* Joe Morton as Sergeant First Class Campbell "Cappy" Matheny
* B. D. Wong as Sergeant First Class Louie Jung Secretary of Defense Charles White
* Whip Hubley as Sergeant First Class Michael Baker
* Andreas Katsulas as El Sayed Jaffa
* Mary Ellen Trainor as Allison Marla Maples Trump as Nancy
* J. T. Walsh as Senator Mavros
* Ingo Neuhaus as Doc
* William James Jones as Catman
* Charles Hallahan as General Sarlow Airline Marshal George Edwards
* Jay Tavare as Nabill
* Christopher Maher as Terrorist Ray Baker as 747 Captain

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 63% of 38 surveyed critics gave the film a positive review; the average rating was 6/10.   Leonard Maltin called it "a tense, inventive thriller" which needed more editing.   Leonard Klady of Variety (magazine)|Variety wrote, "The pictures logic may be a bit fast and loose, but its action-and-excitement quotient is top-notch."   Roger Ebert rated it 3/4 stars and called it "a gloriously goofy mess of a movie". 
 The Island of Dr. Moreau. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 