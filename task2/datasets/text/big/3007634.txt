Dillinger (1973 film)
{{Infobox film
| name           = Dillinger
| image          = DillingerPoster1973.jpg
| caption        = Promotional poster
| director       = John Milius Lawrence Gordon Buzz Feitshans Robert Papazian
| writer         = John Milius Ben Johnson Harry Dean Stanton Cloris Leachman
| music          = Barry De Vorzon
| cinematography = Jules Brenner
| editing        = Fred R. Feitshans Jr.
| distributor    = American International Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $1 million A Million-Dollar Dillinger by AIP
Haber, Joyce. Los Angeles Times (1923-Current File)   13 June 1973: g18 
| gross          = $2 million (US and Canada rentals) 
}}
Dillinger is a 1973 gangster film about the life and criminal exploits of notorious bank robber John Dillinger.
 Ben Johnson as his pursuer, FBI Agent Melvin Purvis. It contains the first film performance by the singer Michelle Phillips as Dillingers moll as Billie Frechette. The film, narrated by Purvis, chronicles the last few years of Dillingers life (depicted as a matter of months) as the FBI and law enforcement closed in. The setting is Depression era America, 1933-34.

The film features largely unromanticized depictions of the principal characters. It was written and directed by John Milius for Samuel Z. Arkoffs American International Pictures.

Retired FBI Agent  .

The film was followed by two made-for-TV spin-offs:   (1974) (teleplay written by Milius) and The Kansas City Massacre (1975), both directed by Dan Curtis and each starring Dale Robertson as Purvis.

==Cast==
*Warren Oates as John Dillinger
*Harry Dean Stanton as Homer Van Meter Geoffrey Lewis as Harry Pierpont
*Steve Kanaly as Pretty Boy Floyd
*John P. Ryan as Charles Makley
*Richard Dreyfuss as Baby Face Nelson
*Frank McRae as Reed Youngblood Ben Johnson as Melvin Purvis
*Michelle Phillips as Billie Frechette
*Cloris Leachman as Anna Sage, "The Lady in Red"
*John Martino as Eddie Martin Sam Cowley
*Read Morgan as Big Jim Wollard
*Richard Eschliman as Machine Gun Kelly

==Production==

Dillinger! Was filmed in its entirety in Oklahoma. Much use of various local landmark buildings were used in the filming from Jet, Nash, Jefferson, and Enid, Oklahoma in the North, to Ardmore, the Chickasaw Country Club and the old iron truss bridge near Mannsville, Oklahoma in the South, the Skirvin Tower ballroom, and the Midwest Theater in Oklahoma City, filling in as the Biograph.

Many local would-be actors wound up immortalized on film, such as the warden of the prison, who was in real life, an Enid, Oklahoma postman. 
 
Milius agreed to write the script for a fraction of his usual price if AIP let him direct.  Milius later said in 2003:
 I look at it today and I find it very crude, but I do find it immensely ambitious. We didnt have a lot of money, or time, and we didnt have such things – we only had so many feet of track, stuff like that. So I couldnt do moving shots if they involved more than, what, six yards of track. We never had any kind of crane or anything. Thats the way movies were made then.  
Milius says he wanted to make a movie about Dillinger because "of all the outlaws, he was the most marvellous". 

==Fiction vs Facts==
*Theodore "Handsome Jack" Klutas is shown being killed by Melvin Purvis; in fact Klutas of the The College Kidnappers was killed by Chicago Police on January 6, 1934
*Wilbur Underhill is shown being shot and killed by Melvin Purvis, in fact Underhill died of wounds January 6, 1934.
*A Chicago bank Guard OMalley is killed by the Dillinger gang during a robbery attempt. In fact William Patrick OMalley was a member of the East Chicago Police force killed January 15, 1934
* The Little Bohemia Lodge Shootout implies that about four of the Dillinger gang are killed {including Herbert Youngblood} and half a score of federal agents were casualties. In fact in the raid the first three men being shot {by mistake by the FBI} were two YCC workers and a local resident {1 killed and two wounded} while lawman casualties were three-one FBI agent killed; one FBI agent wounded {Dazed} and one constable critically wounded}   
*Homer Van Meter is shown escaping from Little Bohemia and then being killed by Iowa Vigilantes. In fact he was killed in Saint Paul, Minnesota|St. Paul, Minnesota. {Dillinger gang member Tommy Carroll (criminal) was mortally wounded during a shootout with police in Waterloo Iowa June 7, 1934}
*Charles Makley is shown dying of a wound and buried in a grave by Dillinger; in fact Mackley was killed September 22, 1934 while trying to escape from prison. {Dillinger gang member John Hamilton (gangster) did die of wounds and whose remains were later found in a grave}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 