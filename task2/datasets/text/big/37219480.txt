My Man Is a Loser
{{Infobox film
| name           = My Man Is a Loser
| image          = My Man Is a Loser poster.jpg
| caption        = Theatrical release poster
| director       = Mike Young
| producer       =  
| writer         = Mike Young
| starring       =  
| music          =   
| cinematography = Harlan Bosmajian 
| editing        = Phyllis Housen 
| studio         = Step One Of Many Entertainment Imprint Entertainment Lionsgate
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         =
| gross          =
}}
My Man Is a Loser is a 2014 comedy film written and directed by comedian Mike Young.  Filming began in New York City in June 2012.  The film received a video on demand and limited theatrical release on July 25, 2014, by Lionsgate Films.  

==Synopsis==
The film follows two married friends, Marty (Michael Rapaport) and Paul (Bryan Callen), who hire their single playboy friend Mike (John Stamos) to "help them get their mojo back" in order to save their marriages.  Their plan backfires, leaving their wives unimpressed with their new personalities. 

==Cast==
*John Stamos as Mike, a "raunchy playboy".   Stamos joined the film in June 2012.   According to producer Eric Bamberger, Stamos was cast as a 40-something "ultimate playboy". 
*Michael Rapaport as Marty and Bryan Callen as Paul, two friends trying to save their marriages. 
*Tika Sumpter as Clarissa, Mikes bartender 

==Promotion==
Step One of Many Entertainment plans a heavily digitally-media-oriented promotion strategy for the film, focused on Twitter and Facebook.  The company states that it expects My Man Is a Loser to be "the heaviest digitally promoted Independent film to date". 

==References==
{{Reflist|colwidth=40em|refs=

 {{cite web
 | last = Patten | first = Dominic
 | title = John Stamos Joins ‘My Man Is A Loser’
 | date = 2012-06-12
 | work = Deadline Hollywood
 | publisher = PMC
 | url = http://www.deadline.com/2012/06/john-stamos-joins-my-man-is-a-loser/
}} 
 {{cite web
 | last = Sanford | first = Nicholas
 | title = CDL Exclusive: Interview with ‘My Man is a Loser’ Writer and Director Mike Young
 | date = 2012-07-02
 | work = Celeb Dirty Laundry
 | url = http://www.celebdirtylaundry.com/2012/cdl-exclusive-interview-with-my-man-is-a-loser-writer-and-director-mike-young-0702/
}} 
 {{cite web
 | last = Shaw | first = Lucas
 | title = Michael Rapaport, Hangover Actor Bryan Callen Star in My Man Is A Loser
 | date = 2012-05-26
 | work = The Wrap
 | url = http://www.thewrap.com/movies/article/michael-rapaport-hangover-actor-bryan-callen-star-my-man-loser-40231
}} 
 {{cite web
 | last = Talmon | first = Noelle
 | title = John Stamos, Michael Rapaport & Tika Sumpter Talk My Man Is A Loser
 | work = Starpulse
 | date = 2012-07-13
 | url = http://www.starpulse.com/news/Noelle_Talmon/2012/07/13/john_stamos_michael_rapaport_tika_sum
}} 
 {{cite web
 | last = Scordo | first = Lizbeth
 | title = John Stamos: Don’t call him Casanova
 | work = omg!
 | date = 2012-06-27
 | url = http://omg.yahoo.com/blogs/now/john-stamos-don-t-call-him-casanova-231644205.html
}} 
 {{cite web
 | title = Step One of Many Entertainment
 | year = 2012
 | url = http://steponeofmany.com/
}} 
 {{cite web
 | last = Kay | first = Jeremy
 | title = Distribution: SPC, Woody Allen reunite
 | work = Screen Daily
 | date = 2012-06-27
 | url = http://www.screendaily.com/news/distribution/distribution-spc-woody-allen-reunite/5065217.article
}} 
}}

==External links==
*  
* 
* 
*  

 
 
 
 
 