Vijay (1989 film)
{{Infobox film
| name           = Vijay
| image          =
| caption        =
| director       = B. Gopal
| producer       = Akkineni Venkat
| writer         = Paruchuri Brothers  
| screenplay     = B. Gopal
| starring       = Akkineni Nagarjuna Vijayashanti Mohan Babu Jayasudha Chakravarthy
| cinematography = S. Gopal Reddy
| editing        = K. A. Marthand
| studio         = Annapurna Studios
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
Vijay is a 1989 Telugu cinema|Telugu, action film produced by Akkineni Venkat on Annapurna Studios banner, directed by B. Gopal. Starring Akkineni Nagarjuna, Vijayashanti played the lead roles and music composed by K. Chakravarthy|Chakravarthy.The film recorded as Average at box-office.    

==Cast==
 
*Akkineni Nagarjuna as Vijay
*Vijayashanti
*Mohan Babu
*Jayasudha Jaggaiah
*Nutan Prasad
*Sarath Babu
*Allu Ramalingaiah
*Suthi Velu
*Narra Venkateswara Rao
*Chalapathi Rao
*Vijayaranga Raju
*P. L. Narayana
*Ramana Reddy
*Chidatala Appa Rao Chitti Babu
*Gowtham Raju Jenny
*Sandya 
*Tatineni Rajeswari
*Y. Vijaya
*Nirmalamma
 

==Soundtrack==
{{Infobox album
| Name        = Vijay
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1989
| Recorded    = 
| Genre       = Soundtrack
| Length      = 17:22
| Label       = Saptaswar Audio Chakravarthy
| Reviews     =
| Last album  = Attaku Yamudu Ammayiki Mogudu   (1989) 
| This album  = Vijay   (1989)
| Next album  = Gudachari 117   (1989)
}}

The music was composed by K. Chakravarthy|Chakravarthy. Music released on SAPTASWAR Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!lyrics!!length
|- 1
|Vaana Raatiri Aarubaita SP Balu,K. Chitra
|Jonnavithhula Ramalingeswara Rao
|3:55
|- 2
|Ayyayyo Chetilo Dabbulu Mano (singer)|Mano,P. Susheela,Madhavapeddi Suresh Veturi Sundararama Murthy
|3:39
|- 3
|Kalyana Sundariki SP Balu,S. Janaki Veturi Sundararama Murthy
|4:21
|- 4
|Yela Gela Gela  SP Balu,S. Janaki Veturi Sundararama Murthy
|3:35
|- 5
|Dangu Chikku Pillo SP Balu Jaladhi
|2:52
|}

==References==
 

 
 
 
 

 