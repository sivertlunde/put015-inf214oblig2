Music for Madame
{{Infobox film
| name           = Music for Madame
| image          = Music for Madame poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = John G. Blystone
| producer       = Jesse L. Lasky
| screenplay     = Gertrude Purcell Robert Harari 
| story          = Robert Harari  Grant Mitchell Erik Rhodes Lee Patrick Romo Vincent
| music          = Nathaniel Shilkret
| cinematography = Joseph H. August
| editing        = Desmond Marquette 
| studio         = Jesse L. Lasky Feature Play Company 
| distributor    = RKO Pictures
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Grant Mitchell, Erik Rhodes, Lee Patrick and Romo Vincent. The film was released on October 8, 1937, by RKO Pictures.   

==Plot==
 

== Cast ==
*Nino Martini as Nino Maretti
*Joan Fontaine as Jean Clemens
*Alan Mowbray as Leon Rodowsky
*Billy Gilbert as Krause
*Alan Hale, Sr. as Detective Flugelman Grant Mitchell as District Attorney Ernest Robinson Erik Rhodes as Spaghetti Nadzio Lee Patrick as Nora Burns
*Romo Vincent as Gas Truck Driver Frank Conroy as Morton Harding
*Bradley Page as Rollins
*George Shelley as Mr. Barret 
*Jack Carson as Assistant Director

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 