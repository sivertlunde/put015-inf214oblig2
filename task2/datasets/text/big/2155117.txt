Slaughter High
{{Infobox film
| name           = Slaughter High
| image          = SlaughterHighCover.gif
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = 
| director       = Mark Ezra   Peter Litten   George Dugdale
| producer       = Dick Randall   Stephen Minasian
| writer         = Mark Ezra   Peter Litten   George Dugdale
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Caroline Munro Simon Scuddamore Carmine Iannaconne
| music          = Harry Manfredini
| cinematography = Alan Pudney
| editing        = Jim Connock
| studio         = Spectacular Trading International
| distributor    = Vestron Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States   United Kingdom
| language       = English
| budget         = Unknown
| gross          = Unknown
}}

Slaughter High is a 1986 American/British independent slasher film written and directed by George Dugdale, Mark Ezra and Peter Litten.

== Plot ==

The day begins with a popular student, Carol Manning (Caroline Munro), jokingly asking Marty Rantzen (Simon Scuddamore), the school nerd, to have sex with her in the womens locker room. After he removes his clothes, Marty is surprisingly exposed to a group of students who tease him in several ways, including jabbing at his crotch with a pilum|javelin, giving him an electric shock and dunking his head into a toilet bowl. This prank ends when the students coach comes in to find out what is happening.
 marijuana joint laced with poison, which he smokes in the science lab where he is working on a chemistry project. The joint makes him so sick he runs to the mens room to vomit. While he is gone, Skip, one of the popular kids, rigs the experiment to explode in Martys face.

Upon Martys return, the set-up works, causing Marty to panic, and a great fire to erupt in the lab after he knocks over a Bunsen burner. In the struggle, Marty accidentally bumps into a shelf where a precariously placed jug of acid sits, causing the jug to fall and break, which splashes acid on Martys face, leaving him horribly disfigured. The sound attracts the attention of the students, who stand by the door in shock. As Marty is taken away on a stretcher into an ambulance, Carol approaches Marty on the stretcher to apologize, but Marty grabs her by the neck. She screams and wakes up in bed.

It is suddenly ten years later, Carol, now a rich and successful actress, gets a phone call from Manny her agent who tries to convince her to appear in another movie. Susan (Sally Cross) visits Carol and tells her that the she recently engaged and will try to make it for the class reunion later. Soon, everyone behind the prank are invited back to the school for a reunion. Upon arriving, they discover that the campus was shut down years ago and the building is in disrepair, awaiting demolition. They begin to think that one of the former students was behind the invitations, but everyone denies it.

They decide to break into the school to hang out and drink alcohol, and come across a room containing their old school lockers, which to their surprise are filled with their old belongings. They notice Martys old locker, and the alumni begin questioning what happened to him after the prank-gone-awry. Skip tells them that Marty is doing fine and probably forgot all about what happened. He has supposedly been in a mental institution.

The first victim is the caretaker, his head impaled on a coat peg and his arm nailed to a door by a mysterious killer dressed like a jester. Meanwhile, Carl tells Ted to show everyone how to "sink a beer" and Ted demonstrates. Suddenly, Ted gets sick, his stomach pops wide open and some of his innards are exposed. The "beer" is actually a lethal acid. Everyone screams and runs away except Shirley who stands in shock with some of Teds blood splashed onto her face. After Teds death, Carl goes to activate Carols car to go find help, however the Jester kills Carl by impaling him through the drivers seat. Back in the school building, Shirley goes to the locker room and gets in a tub to wash off the blood she has on her. When she turns on the faucet for water, acid fills the tub instead and she screams. Several people run in and find Shirleys skeleton in the tub, realizing that Marty has returned, bent on revenge from the accidental prank.

Afterwards, Susan arrives late for the reunion and she walks in through the front door which is unlocked and sees a life-size picture of Marty at the end of the hallway. Soon, hands punch through the poster and she appears off-screen. Inside, Joe finds a tractor and works on it, in hopes to escape the school by plowing through the doors and begins to work on it. Soon, the Jester visits Joe, as Joe is underneath the tractor. The Jester kicks the jack away and starts up the tractor as Joe is still underneath while the tractors running blades being to run underneath. Seeing Joe under the tractor, Joe tries to hold up the tractor, but the Jester cuts Joes arms with a folding knife and soon, the tractor falls on Joe and Joe is killed by the tractors running blades. After Joes demise, the Jester goes to where Stella and Frank are at, while having sex and the Jester electrocutes them both by hooking cables to the bed.

Later on, Nancy goes to check on Joe and finding him dead, and runs off to tell Skip and Carol. They also find Stella and Frank dead and Skip screams out into the hallways for Marty to show himself. Nancy becomes upset, blaming Skip and Carol for everything that has led to Martys accident, and Carol calms her down. As morning arrives, Skip wakes up and heads into the hallway, where he is grabbed. Despite not knowing where Skip went, Carol explains to Nancy that if Marty is behind all of the occurring murders, they just have to wait until April 1 which ends at noon which is the time Marty is disfigured and they only need one hour. Meanwhile, Skip wakes up and find himself hanging from a rope. He tries to shake himself free until the rope comes loose and he falls.  Carol and Nancy look around and realize that all the corpses are gone. The hear a noise and walk into a room with a video playing of the shower prank. Nancy sees her photograph marked out in a nearby yearbook and she runs. Nancy then falls in a cesspit and tries to use a metal pipe to pull herself up, but the Jester knocks her back into the cesspit and the Jester leaves. 

Back in the building, Carol finds the caretaker dead and hides in the girls locker room and hears a noise. She finds blood in one of the toilets and she flushes it, only to have it overflow. She picks up a baseball bat and during the process, she finds Susans body, hanged and having her throat slit. She runs in the hallway, only for the Jester to bust through the life-sized poster of Marty and Carol manages to beat down the Jester with the baseball bat. Soon, the Jester picks up the bat and searches in the hallways for Carol. As Carol continues to run downstairs to the auditorium and hides onstage, the Jester exchanges the baseball bat for the javelin and heads to the onstage and the Jester shoves his javelin through the curtain, which nearly misses Carol.

Carol runs into a room, where she finds a hatchet and she hides in the room, as the Jester still continues into the hallways to find Carol. Hearing footsteps, Carol swings the hatchet, mistakenly slamming the hatchet in Skips face, which also reveals that Skip survived his hanging. After having the Jester punching through a hallway glass window to grab her, Carol runs into a hallway, where the Jester tries to grab her again, but pushing the Jester through a window and landing in a gym floor. Carol thinking the Jester is dead, she throws his javelin to him and soon escapes. Seeing that it is now noon, the Jester gets back up with the javelin and proceeds his searching.

As Carol proceeds into another hallway, Carol continues to run as the Jester runs at her with the javelin. Having to flee from the Jester, she escapes into the girls locker room, inside the stall where the prank all started in. The Jester breaks in and finds Carol in the shower stall and pulls aside the curtain. The Jester is finally revealed to be indeed only, but Marty himself, revealing his disfigured face and all deranged and bent on revenge. Laughing, Marty impales Carol with the javelin through her stomach, finally completing his revenge.
 revenants and all of the victims chase after him chanting his name over and over again as they chase and corner him in the auditorium.
 mental hospital, restrained to a bed and wearing bandages on his face, having in fact dreamed the events of the reunion massacre that Marty had out of wishing revenge against all those who caused his disfigurement. As a doctor arrives into Martys hospital room, the nurse turns around, only to reveal that it is Marty wearing the nurses clothing while the nurse is laying dead in the bed. Marty then shoves the syringe into the doctors eye, killing him and the film ends with Marty then tears off half of his face with his bandages as well and he escapes the hospital, vowing to find and kill those who caused his disfigurement as the screen fades to black.

== Cast ==

* Caroline Munro as Carol Manning
* Simon Scuddamore as Marty Rantzen
* Carmine Iannaconne as Skip Pollack
* Donna Yeager as Stella Gary Martin as Joe
* Billy Hartman as Frank
* Michael Saffran as Ted Harrison
* John Segal as Carl Putney
* Kelly Baker as Nancy
* Sally Cross as Susan
* Josephine Scandi as Shirley Marc Smith as Coach
* Dick Randall as Manny
* Jon Clark as Digby

== Production ==
 slasher film of the same title scheduled for release the same year. 

The film was shot in England, and many of the actors use fake American accents.

Simon Scuddamore, who plays the films killer, committed suicide shortly after production of the film.
 score was Friday the 13th fame.

== Release ==

The film was given a limited release theatrically in the United States by Vestron Pictures in November 1986.
 R rating. For its video premiere, the distributor Vestron Video released both the censored theatrical version and an unrated version that contained the excised violence.

=== Home media ===

Slaughter High was released as April Fools Day on VHS in Japan only by Vestron International. To date, Japan is the only country to have a home video release of the film under its original title.
 Lionsgate released the film on DVD on 14 April 2009 in the USA as part of their Lost Collection.  The DVD contains the unrated Vestron fullscreen VHS master print.

Lionsgate re-released the film to DVD on 4 January 2011 in a 4-Film Collection set along with  .  Arrow Video released a Special Edition DVD in the United Kingdom in July 2011.

Slaughter High was released a third time on DVD in 2012 by Lionsgate in an eight horror film DVD set which also includes   and Chopping Mall.

== Reception ==
 AllMovie wrote, "Slaughter High gets a passing grade for the die-hard genre fans, but is worthless for most any other audience." 

== References ==

 

== External links ==

*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 