Just for Kicks (2003 film)
{{Infobox film
| name           = Just 4 Kicks!
| image          = Just4kicks.jpg
| image size     = 190px
| alt            = 
| caption        = Film Poster
| director       = Sydney J. Bartholomew Jr.
| producer       = Alessandro F. Uzielli
| writer         = 
| narrator       =  Tom Arnold  Bill Dawes
| music          = William Goodrum
| cinematography = Christopher C. Pearson
| editing        = Sam Seig
| studio         = Commotion Pictures 
| distributor    = MGM Home Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Tom Arnold as their father and coach.

==Synopsis==
The plot concerns two twins who adore soccer but whose team plays badly. They find Rudy to be the best soccer player ever. When their dad, who is their coach, has to go away on a business trip, the brothers mother takes over as the teams new coach. As their team members get ridiculed over having a girl as a coach, the twins set out to find yet a new coach for their team. They meet their new coach one night as the brothers are almost run over by a car and a stranger who displays exceptional soccer skills saves them. It turns out the stranger, who lives around the neighborhood, is a former soccer legend. With the help of the brothers mom, the new coach embarks them on the road to a championship from the local soccer league.

== External links ==
*  

 
 
 


 
 