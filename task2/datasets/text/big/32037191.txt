Tug (film)
{{Infobox film
| name           = Tug
| image          = Tug film.jpg
| alt            = 
| caption        = 
| director       = Abram Makowka
| producer       = Scott J. Brooks Hopwood DePree Rebecca Green
| writer         = Abram Makowka
| starring       = Sam Huntington Haylie Duff Zachary Knighton Sarah Drew
| music          = Jon Sadoff
| cinematography = Adam Stone
| editing        = David Hopper Tim Mirkovich
| studio         = TicTock Studios
| distributor    = Jumpstart Pictures
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
}}
Tug is a 2010 American romantic comedy film written and directed by Abram Makowka. Shown at the Newport Beach Film Festival & the Waterfront Film Festival. 

==Plot==
A small-town guy (Huntington) tries to decide between staying with his current girlfriend (Drew) or going back to his psycho ex (Duff). 

==Cast==
* Sam Huntington
* Haylie Duff as Kim
* Zachary Knighton as Judd
* Sarah Drew as Ariel
* Wendi McLendon-Covey as Taylor
* Maulik Pancholy as Carl
* Yeardley Smith as Mom
* Dennis North as Dad
* Skyler Stone as Agent
* David Zellner as Geno

==Release== Amazon streaming on February 19, 2013. http://www.amazon.com/Tug/dp/B00BI4S2ZM/ref=sr_1_1?s=movies-tv&ie=UTF8&qid=1361343581&sr=1-1&keywords=tug+haylie+duff 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 

 