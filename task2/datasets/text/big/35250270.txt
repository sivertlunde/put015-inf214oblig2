The Bandmaster (1931 film)
 
{{Infobox Hollywood cartoon|
| cartoon_name = The Bandmaster
| series = Oswald the Lucky Rabbit
| image = Bandmaster1931.jpg
| caption = Oswald tries to impress a baby hippo with music.
| director = Walter Lantz Bill Nolan
| story_artist = 
| animator = Clyde Geronimi Manuel Moreno Ray Abrams Fred Avery Lester Kline Chet Karrberg Pinto Colvig
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = May 18, 1931
| color_process = Black and white
| runtime = 6:04 English
| preceded_by = Country School
| followed_by =  Northwoods
}}

The Bandmaster is a 1931 short film by Walter Lantz Productions, starring Oswald the Lucky Rabbit. As with a few films from the series, the cartoon is in the public domain. {{cite web
|url=http://lantz.goldenagecartoons.com/publicdomain.html
|title=The Walter Lantz Cartune Encyclopedia: List of Shorts in the Public Domain
|accessdate=2011-06-08
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==Plot==
On the street, Oswald leads a handful of musicians whose devotedness towards him varies. Although the performance of the band showed some flaws, it mattered little to Oswald who simply carries on. Suddenly, the musicians decided to have a break inside the tavern, much to the rabbits surprise. Oswald tries to follow them to the place, only to be pushed back out.

Without a group to lead, the lonely Oswald wonders further on the street. He then notices a flock of birds on some powerlines, chirping and making various sounds. In no time the rabbit was elated, knowing he found something he could conduct as he starts swinging his hands. But the beautiful sight did not last long when a disturbed squirrel comes out of a post and pulls up a switch that electrocutes the birds.

Somewhere within the area, a nurse tells an infant to stay put in the stroller just before leaving. The infant isnt accustomed to being left alone, even for a few minutes, and therefore starts bawling. Oswald came by and decides to cheer up the child. The rabbit then picks up a discarded water pipe and plays it like a wind instrument. Various objects came to life and went dancing to Oswalds music, but the infant was less impressed. The nurse returns to the scene after several minutes. The naive nurse thought Oswald is disturbing the child with the sound, and therefore pounds the rabbit in the noggin. While Oswald remains dazed on the pavement, the infant, however, was most delighted and starts to giggle.

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 


 