Bottoms Up (1934 film)
{{Infobox film
| name           = Bottoms Up
| image          = BottomsUp1934poster.jpg
| image_size     =
| caption        = David Butler
| producer       = Buddy G. DeSylva
| writer         = David Buster Buddy G. DeSylva Sid Silvers
| narrator       =  John Boles Sid Silvers Herbert Mundin Howard Jackson (uncredited)
| cinematography = Arthur C. Miller
| editing        = Irene Morra
| studio         = Fox Film Corporation
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 85 min
| country        = United States
| language       = English
| budget         =
| gross          =
}} David Butler John Boles and Herbert Mundin, and features Thelma Todd in a supporting role.

==Plot==
The film tells the story of a promoter who helps a Hollywood extra actress toward stardom, however she turns from him toward her leading man.

==Cast==
*Spencer Tracy as Smoothie King 
*Pat Paterson as Wanda Gale  John Boles as Hal Reed 
*Sid Silvers as Spud Mosco aka Reginald Morris 
*Herbert Mundin as Limey Brook aka Lord Brocklehurst  Harry Green as Louis Baer aka Wolf 
*Thelma Todd as Judith Marlowe 
*Robert Emmett OConnor as Detective Rooney 
*Dell Henderson as Lane Worthing 
*Suzanne Kaaren as Wolfs Secretary  Douglas Wood as Baldwin

==External links==
* 
* 
* 

 

 
 
 
 
 
 


 