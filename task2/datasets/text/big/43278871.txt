Perfect Exchange
 
 
{{Infobox film
| name           = Perfect Exchange
| image          = PerfectExchange.jpg
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 至尊三十六計之偷天換日
| simplified     = 至尊三十六计之偷天换日
| pinyin         = Zhì Zūn Sān Shí Liù Jì Zhī Tōu Tiān Huàn Rì
| jyutping       = Zi3 Zyun1 Saam1 Sap6 Leok6 Gai3 Zi1 Tau1 Tin1 Wun6 Jat6 }}
| director       = Wong Jing
| producer       = Jimmy Heung
| writer         = 
| screenplay     = Wong Jing
| story          = 
| based on       =  Tony Leung
| narrator       = 
| music          = Philip Chan Sherman Chow
| cinematography = Andrew Lau Tony Miu
| editing        = Poon Hung
| studio         = Wins Entertainment
| distributor    = Gala Film Distribution
| released       =  
| runtime        = 105 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$17,912,327   
}}
 The Sting Hong Kong Tony Leung. The film was rated Category III by the Hong Kong motion picture rating system.

==Plot== Tony Leung), Lau Yiu Cho (Wan Chi Keung) and Chan Chung Ming (Natalis Chan) were gambling together. Mandy uses his girlfriend Lily (Christy Chung) and partner Gold Finger Chi (Liu Kai-chi), who are the card dealers, to help him cheat and won over HK$6 million. On the other hand, Lau lost up to HK$4 million. Afterwards, Mandy, Lau and Chan met Chung, who brought a number of his subordinates with him, at a bar. There, Lau felt he was cheated and a big fight occurred where Chung was injured.  Lau found out that the three of them cheated him and he blackmails Mandy to help him go to prison and interrogate his former subordinate Robinson Shun (Kwan Hoi-san), who stole a total of HK$300 million treasury bond from him. If Mandy manages to have Robinson tell him the whereabouts of the bond, Lau promises to pay Mandy HK$10 million as a reward. To get Mandy to prison, Lily sues him for sexual harassment. However, due to lack of evidence, the judge does not adopt the case. Mandy later gets himself to prison by throwing his shoe at the judge and he was sentenced to prison for a month for contempt of court and assaulting the judge.

In prison, in order to get close to Robinson and not be bullied by his inmates, Mandy bribes inmates Kei (Lee Siu-kei), Hung (Victor Hon) and Crazy Bill (William Ho) for protection. However, Mandy discovers the truth to the issue. Lau is actually Robinsons son in-law who killed his daughter for money whil telling the police that she was killed in a robbery. To avenge his daughter, Robinson cuts Lau with a knife and was farmed by Lau for murder, which led to his imprisonment. After hearing this, Mandy changes his mind and is determined to help Robinson seek revenge and punish Lau and Robinson promises Mandy to split half of his property to him after he successfully takes revenge.

In order to persuade Chung, a prison officer, to help him, Mandy promises to give him HK$30 million after he successfully takes revenge on Lau. After fighting with Laus subordinate Dinosaur (Chan Chi Fai),  who was also sent to prison to interrogate Robinson, Mandy, Chung and Dinosaur caused a prison riot which was also on the night of the engagement party between Lau and Mona (Anita Lee). Mandy takes advantage of the chaos and successfully escapes form prison. At Laus party, Mandy, Chung and Lily causes Lau to shoot Mona dead after a big fight where he was also arrested. Robinson also takes the long hidden bonds from a glass bottle and after the incident, Mandy, Chung and Robinson return to prison,

Due to their efforts and good behavior, the three of them were released from prion early. Since the bonds are deposited in a Switzerland bank, Chung has to wait four years to receive the HK$3 million that Mandy promised to give him earlier.

==Cast==
*Andy Lau as Mandy Chin
*Tony Leung Ka-fai as Prison inspector Chung Cho Hung
*Christy Chung as Lily
*Kwan Hoi-san as Robinson Shun
*Kingdom Yuen as Turkey Chu
*Anita Lee as Mona
*Natalis Chan as Chan Chung Ming (cameo)
*Liu Kai-chi as Gold Finger Chi
*Tommy Wong as Big Eye Kwong
*Wang Lung-wei as Pau
*Chan Chi Fai as Dinosaur
*Lee Siu-kei as Brother Kei
*Victor Hon as Brother Hung
*William Ho as Crazy Bill
*Wan Chi Keung as Lau Yiu Cho
*Teddy Yip as Chungs dad
*Lok Wai as Girlfriend of Chungs dad
*Pau Hon Lam as Judge
*Law Shu Kei as Prison Warden
*Fung Wai Lung as Chos thug
*Sam Ho as Dog
*Bobby Au-yeung
*Carol Lee
*Leung Siu Tik as Inmate
*Kwan Yung as Inmate
*Kong Miu Ting as Inmate / Prison Guard (2 Roles)
*Ng Kwok Kin as Prison guard
*Chan Siu Wah as Prison guard at disco
*Fei Pak as Prison guard
*Wong Wai Shun as Chos thug

==Reception==

===Critical===
Perfect Exchange received relatively positive scores of 6.1/10 stars from the   is very game and while Andy Lau is the cool presence in addition to a character rarely being in danger, hes the lesser part of the double act. It just doesnt seem like a fit to have Lau this time around in such a jarring Wong Jing film that mixes the crazy, the silly, Lau going into prison on a rape charge and various, jarring pieces of violence scattered throughout. But its bearable and actually funny in parts so therefore great success coming from Wong Jing." 

===Box office===
The film grossed HK$17,912,327 at the Hong Kong box office during its theatrical run from 30 September to 20 October 1993 in Hong Kong. 

==See also==
*Andy Lau filmography
*Wong Jing filmography
*List of Hong Kong Category III films

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 