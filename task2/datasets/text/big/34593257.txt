Mad Buddies
{{Infobox film
| name           = Mad Buddies
| image          = Mad Buddies.jpg
| image_size     = 
| caption        = Poster for the film
| director       = Gray Hofmeyr
| producer       = Helena Spring
| writer         =  
| narrator       = 
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| studio         = Touchstone Pictures Keynote Films Walt Disney Studios Motion Pictures 
| released       =  
| runtime        = 
| country        = South Africa
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Mad Buddies is a 2012 comedy film directed by Gray Hofmeyr, co-written by Gray Hofmeyr and Leon Schuster and starring Leon Schuster, Kenneth Nkosi, Tanit Phoenix and Alfred Ntombela. Walt Disney Studios Motion Pictures acquired the films distribution rights and released the film through the Touchstone Pictures banner. 

== Plot ==
Enemies, Boetie (Schuster) and Beast (Nkosi), are forced to embark on a road trip as unwitting subjects of a new TV reality show "Mufasa Mufasa, Walla Habibi", devised by a TV Producer, Kelsey, played by Tanit Phoenix. On camera, with the whole of South Africa in on the joke, the pair comes stuck at every stage of the journey until they discover that they have been conned and join forces to exact revenge.

== Cast ==
*Leon Schuster as Boetie
*Kenneth Nkosi as Beast
*Tanit Phoenix as Kelsey
*Alfred Ntombela as Minister Nda

==External links==
 
 
 
 
 

 
 