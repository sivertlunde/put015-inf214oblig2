Portrait of Clare
{{Infobox film
| name           = Portrait of Clare
| image          = "Portrait_of_Clare"_(1950).jpg
| image_size     =
| caption        = 
| director       = Lance Comfort
| producer       = Leslie Landau
| writer         = Adrian Alington Leslie Landau Francis Brett Young (novel)
| starring       = Margaret Johnston Richard Todd Robin Bailey  Ronald Howard
| music          = Leighton Lucas
| cinematography = Günther Krampf
| editing        = Clifford Boote ABPC
| distributor    = 
| released       = 13 November 1950
| runtime        = 100 mins
| country        = United Kingdom
| gross          = ₤100,643 (UK) 
| language       = English
}}
 1950 Cinema British drama Ronald Howard. 

==Plot==
As told in flashback to her granddaughter: the three marriages of Clare Hingston. These are: a young man who is killed, a priggish lawyer and a sympathetic barrister.

==Cast==
* Margaret Johnston - Clare Hingston
* Richard Todd - Robert Hart
* Robin Bailey - Dudley Wilburn Ronald Howard - Ralph Hingston
* Jeremy Spenser - Steven Hingston
* Marjorie Fielding - Aunt Cathie
* Molly Urquhart - Thirza
* Beckett Bould - Bissell Anthony Nicholls - Doctor Boyd
* Lloyd Pearson - Sir Joseph Hingston
* Mary Clare - Lady Hingston
* S. Griffiths-Moss - Bates
* Campbell Copelin - Inspector Cunningham
* Bruce Seton - Lord Steven Wolverbury
* Yvonne Andre - Marguerite

==Critical reception==
TV Guide wrote, "the story suffers from a slack pace, though Johnston adds a lot of charm and sincerity to her role."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 