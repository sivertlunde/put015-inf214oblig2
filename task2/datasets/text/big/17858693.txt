Tarzan of the Apes (1999 film)
{{Infobox Film 
| name = Tarzan of the Apes
| image = Tarzan of the Apes 1999 film.jpg
| caption = DVD cover
| director = 
| producer = Diane Eskenazi Darcy Wright Mark Young based on the novel by Edgar Rice Burroughs
| starring = 
| music = 
| cinematography = 
| editing = 
| distributor = Sony Wonder
| released = March 9, 1999
| runtime = 48 minutes
| country = 
| language = English
| budget = 
}} musical adventure Mark Young novel by 1999 film in which they are gorillas.

==Plot== Kala and Kerchak, members of the tribe of Brown Apes.

The baby, christened Tarzan ("White Skin") by Kala, grows to adulthood among the apes, and eventually discovers the cabin where his parents lived. He teaches himself to read and write using the books there. He also discovers a cache of gold coins secreted beneath a loose floorboard in the cabin.
 Jane Porter, her father, Professor Archimedes Porter, and Esmerelda, their cockney housekeeper, are shipwrecked at the same location, and soon Jane and Tarzan fall in love. The Porters and Esmerelda are rescued and sail to United States|America, believing that Tarzan has been killed.

Tarzan, with the help of Lieutenant Paul DArnot, sails to Baltimore, Maryland where he is reunited with Jane. Tarzans identity as Lord Greystoke is discovered, and he and Jane are married and return to Africa.

==Cast==
The voice cast was uncredited in this film.

==Songs==
"Child of My Dreams", "Tarzan of the Apes" and "Everlasting Love".

==Production notes==
The film was released as part of the Sony Wonder Enchanted Tales series.
 Disney Tarzan (1999 film)|Tarzan production.

==External links==
*  

 

 
 
 
 
 
 
 