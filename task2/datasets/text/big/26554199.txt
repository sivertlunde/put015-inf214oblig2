Bobbikins
{{Infobox film
| name           = Bobbikins
| image          =
| image_size     =
| caption        = Robert Day
| producer       = Oscar Brodney Bob McNaught
| writer         = Oscar Brodney
| narrator       =
| starring       = Shirley Jones Max Bygraves Philip Green
| cinematography = Geoffrey Faithfull
| editing        = Stan Hawkes Ralph Kemplen
| distributor    = Twentieth Century-Fox Productions
| released       = 28 July 1959
| runtime        = 89 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Robert Day. It stars Shirley Jones and Max Bygraves. 

==Plot==
 
This adventure follows the story of a young navy man, his wife (Shirley Jones) and their baby son, Bobby aka Bobbikins. To his surprise, Dad discovers his son talks, not baby-talk or gibberish but adult conversations with his father only. Bobbikins learns stock market tips and passes them to his Dad.

After making a killing on the stock market, problems really begin. The dad is presumed mad, the government is after him, and the break down of relations between the young couple ensues. But there is hope.

A comedy with a happy ending.

==Cast==
* Shirley Jones as Betty Barnaby
* Max Bygraves as Ben Barnaby
* Steven Stocker as Bobbikins Barnaby
* Billie Whitelaw as Lydia Simmons
* Barbara Shelley as Valerie
* Colin Gordon as Dr. Phillips
* Charles Bud Tingwell as Luke Parker
* Lionel Jeffries as Gregory Mason Charles Carson as Sir Jason Crandall
* Rupert Davies as Jock Fleming
* Noel Hood as Nurse David Lodge as Hargreave John Welsh as Admiral

==References==
 

==External links==
*  
*  
*  

 

 
 
 