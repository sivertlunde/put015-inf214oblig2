Red Tomahawk
{{Infobox film
| name           = Red Tomahawk
| image          = Red Tomahawk poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = R. G. Springsteen
| producer       = A. C. Lyles Steve Fisher
| story          = Andrew Craddock Steve Fisher 
| starring       = Howard Keel Joan Caulfield Broderick Crawford Scott Brady Wendell Corey Richard Arlen Tom Drake
| music          = Jimmie Haskell
| cinematography = W. Wallace Kelley 
| editing        = John F. Schreyer 
| studio         = A.C. Lyles Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film Steve Fisher. The film stars Howard Keel, Joan Caulfield, Broderick Crawford, Scott Brady, Wendell Corey, Richard Arlen and Tom Drake. The film was released on January 1, 1966, by Paramount Pictures.  

==Plot==
 

== Cast ==	
*Howard Keel as Capt. Tom York
*Joan Caulfield as Dakota Lil McCoy
*Broderick Crawford as Columbus Smith
*Scott Brady as Ep Wyatt
*Wendell Corey as Sy Elkins
*Richard Arlen as Deadwood Telegrapher
*Tom Drake as Bill Kane
*Tracy Olsen as Sal
*Ben Cooper as Lt. Drake
*Don "Red" Barry as Bly 
*Regis Parton as Prospector #3 
*Gerald Jann as Wu Sing
*Roy Jenson as Prospector Dan White as Ned Crone
*Henry Wills as Samuels
*Sol Gorss as Townsman / Roulette Player

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 