The Animal Project
 
{{Infobox film
| name           = The Animal Project
| image          = File:The Animal Project film poster.jpg
| caption        = Film poster
| director       = Ingrid Veninger
| producer       = 
| writer         = Ingrid Veninger
| starring       = Aaron Poole
| music          = 
| cinematography = Cabot McNenly
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Canada
| language       = English
| budget         = 
}}

The Animal Project is a 2013 Canadian drama film written and directed by Ingrid Veninger. It debuted at the Contemporary World Cinema section at the 2013 Toronto International Film Festival.

==Production==
Of his character Jason, Johnathan Sousa commented that he "is very courageous in the sense that he goes for what he believes in, rather than doing what the norm calls for. Through submitting to the project, all the actors give in to it and it opens up some really positive things in their lives."  Sousa, Évelyne Brochu, Cara Gee, and Megan Park were chosen for the festivals Rising Stars program. 

In a story about Canadian films at TIFF, Macleans dubbed Veninger "Toronto’s reigning queen of DIY cinema". 
 Friendly Rich & The Lollipop People appear in the film with "Sei Spento il Sole".   

==Cast==
* Aaron Poole as Leo
* Hannah Cheesman as Alice
* Sarena Parmar as Mira
* Joanne Vannicola as Morag
* Kate Corbett as Rosie
* Jessica Greco as Pippa
* Joey Klein as Saul
* Emmanuel Kabongo as Ray
* Lindsay Owen Pierre as Matteo

==Release==
The film premiered at the Contemporary World Cinema section at the 2013 Toronto International Film Festival.      

The film has also screened at the St. Johns International Womens Film Festival, with a one-hour "making of" talkback section.  In November, it will screen at the Denver Film Festival.

==Critical reception==
{{Album ratings
| rev1 = The Globe and Mail
| rev1Score =  
| rev2 = National Post
| rev2Score =    NOW Magazine
| rev3Score =   
| rev4 = Toronto Star
| rev4Score =   
}}
 NOW Magazine commented that "It may not be the sort of hyper-personal near-meta-fiction Veninger defined her career with, but The Animal Project seems to similarly prod the relationship between interiority and outside world."    In a review, the magazine gave it their highest rating, "NNNN", declaring it a transitional project for the director, who "upped her game without abandoning any of her characteristic whimsy." They note the film has a narrative story unlike her previous films, and a "more formal visual style". 

Highlighting actor Aaron Poole in an article, Torontoist suggested that the film was "heavy with emotional baggage but imbued with a lightness that comes from being partially improvised," suggesting it might "demonstrate a different side of Poole."  Ilse de Mucha Herrera for The Arts Scene website suggested that the film is "nothing short of delightful", a "refreshing" film with "a script full of passion with relatable characters and creative sequences," rating it 4/5. 

Isabel Cupryn of Canadian Film Review commented: "With pure honesty and unwavering compassion for its characters, its hard not to fall in love with this quiet, thoughtful film. The Animal Project is like its creator Ingrid Veninger — genuine and true to its heart." 
 The Grid suggested that the use of costumes is "less striking than its intended to be", but "the good moments... linger vividly in the rearview mirror," offering it 7/10. 

Pop culture website Dork Shelf suggested "the scattered moments of human observation that make up the film are almost always careful and poignant," but the overall film premise and characters "revelatory experiences" "never   genuine."  Canadian entertainment website Scene Creek also gave a less than enthused review, 2 out of 5 stars. Critic Danielle La Valle praised the father and son conflict, suggesting it should have been the films focus, praising Jacob Switzers "great skill and understatement" in the role. The review cites the dysfunction of Leo needing to disguise himself and offer free hugs at his sons high school, in order to feel comfortable hugging him. She suggests that the concept of obliterating comfort zones with the costumes is incorrect, as the outfits would add a layer of comfort through anonymity. 

Toronto Star connected the film with six other Canadian films screening at the festival, all with themes of identity.  Critic Linda Barnard deemed it 3/4 stars. 

Yahoo! TIFF Blog writer Will Perkins named it among the top five Canadian films screening at the festival. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 