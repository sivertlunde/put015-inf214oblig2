Aap Ki Khatir (1977 film)
{{Infobox film
| name           = Aap Ki Khatir
| image          = 
| image_size     = 
| caption        = 
| director       = Sudhendu Roy
| producer       = Harsh Kohli
| writer         =Sunder Dar   
|story=Indu Kohli,Uma Thakur, Pushpa Ghai	 	

| narrator       =  Nadira
| music          = Bappi Lahiri
| cinematography = Dilip Ranjan Mukhopadhyay	 
| editing        = Ravi Patnaik	
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1977 Bollywood film, Harsh Kohli produced and Sudhendu Roy directed the film.  The stars are Vinod Khanna, Rekha, Nadira (actress)|Nadira, Helen Jairag Richardson|Helen, Tun Tun and Om Shivpuri. The films music is by Bappi Lahiri.  The last song in the film "Bombay se Aaya Mera Dost" became a memorable hit.  Film is a remake of an American film titled For Petes Sake (film) (1974) that stars Barbra Streisand. 

==Plot==

Sagar (Vinod Khanna) defies his wealthy family by marrying a poor girl named Sarita (Rekha) and therefore is cut out of his familys inheritance.  He then decides to make a living by driving a cab.  Sarita feels guilty that he left his wealth behind to marry her.  She raises Rs.10,000 from a loan shark but lies to her husband saying she got it from her uncle.  She then asks her husband to give the money to a stockbroker friend to invest.  When the investment doesnt result in healthy dividends, Sarita becomes desperate to raise the money to repay the loan shark.  She inadvertently gets hooked up with a madam (Nadira (actress)|Nadira) who sends clients to her apartment.  Sarita gets into humorous incidents as she avoids contact with her male clients.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Bambai Se Aaya Mera Dost"
| Bappi Lahiri
|-
| 2
| "Humne To Kiya Kasam Aap Ke Khatir"
| Lata Mangeshkar
|-
| 3
| "Pyara Ek Bangla Ho"
| Bappi Lahiri, Lata Mangeshkar
|-
| 4
| "Raja Mere Tere Liye"
| Lata Mangeshkar
|-
| 5
| "Seedhi Sadhi Shehzadi"
| Kishore Kumar
|-
| 6
| "Shola Re Bhola Re"
| Bappi Lahiri, Shailender Singh, Usha Mangeshkar
|}

==References==
 

==External links ==
*  

 
 
 
 


 
 