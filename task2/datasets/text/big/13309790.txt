I Passed for White
{{Infobox film
| name           = I Passed for White
| image          = I Passed for White.jpg
| image_size     = 225px
| caption        = theatrical poster to I Passed for White
| director       = Fred M. Wilcox
| producer       = Fred M. Wilcox
| writer         = Fred M. Wilcox
| based_on       = novel by Reba Lee as told to Mary Hastings Bradley
| starring       = Sonya Wilde James Franciscus
| music          = Jerry Irvin John Williams
| cinematography = George J. Folsey George White Allied Artists Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,700,000 (US/ Canada) 
}}

I Passed for White is a 1960 film directed and adapted for the screen by Fred M. Wilcox from a novel of the same name by Reba Lee "as told to" Mary Hastings Bradley. The film stars Sonya Wilde, James Franciscus and features Jimmy Lydon, Patricia Michon, and Isabel Cooley. The film was released by Televista on March 18, 1960.

==Plot== passing as a white woman. The film begins with her being mistaken for a purely white woman by a white man who tries to hit on her repeatedly. Her brother, more obviously of mixed heritage, fights off the man. Bernices grandmother consoles her when she confides her troubles.

Later in the film, after a failed attempt at looking for employment as a black, she decides to leave town, use the name Lila Brownell and live as a white woman. On the plane to New York she meets and eventually marries the man of her dreams – Rick Leyton – only she hasnt told him she is part black. He and his rich family and friends are white. Her white friend Sally, and black maid Bertha both advise her not to tell him. She becomes pregnant, and fears the child will have black features or coloring – and gets a book to read about this unlikely possibility, which she hides, but Rick eventually discovers it, although their maid claims the book belongs to her.

Bernice/Lila goes into premature labor and has a stillborn child, but cries out "Is the baby black?" after she awakens from anesthesia. This leads Rick to suspect that his wife has been unfaithful. Eventually, she and her husband divorce without Bernice ever having revealed her true name or past. She then returns to her family in Chicago and her original identity.

==References==
Notes
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 