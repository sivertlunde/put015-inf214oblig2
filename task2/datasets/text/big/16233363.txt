Ali Baba and the Forty Thieves (1944 film)
{{Infobox film

 | name = Ali Baba and the Forty Thieves
 | caption = Original film poster
 | director = Arthur Lubin
 | producer = Paul Malvern
 | writer = Screenplay: Edmund L. Hartmann Jon Hall Leif Erickson  Kurt Katch  Turhan Bey Frank Puglia George Robinson
 | editing = Russell F. Schoengarth
 | distributor = Universal Pictures
 | released = United States: January 14, 1944
 | runtime = 87 minutes
 | country = United States
 | language = English
 | gross = 3,634,679 admissions (France) 
 | image= Ali baba 1944.jpg
}}
 tale of Arabian Nights and White Savage. 

The story takes place in Persia, yet Jamiel hoists the Flag of Saudi Arabia|shahada, which is the traditional Saudi Arabian flag.

==Plot== Mongolian conquest of Bagdad by Hulagu Khan (Kurt Katch). The caliph Hassan (Moroni Olsen) has escaped captivity, together with his young son Ali (Scotty Beckett), and prepares to regroup the renmants of his troops. While staying at the mansion of Prince Cassim (Frank Puglia), Ali and Cassims daughter Amara (Yvette Duguay), fearing that they will not see each other again, betroth themselves via Blood brother|blood-bond.

As the caliph prepares to leave, Mohammed stops him at the last moment. This, however, is the initiation for an ambush by the Mongols, to whom the cowardly prince has sworn allegiance; the caliph and his retinue are massacred, and only Ali escapes. Alone and lost in the desert, he comes across a mountainside where he sees a group of riders exiting a hidden cave. Deducing its opening phrase ("Open, Sesame!"), he enters the cave and finds it filled with treasure. When the 40 thieves return, they find the boy asleep in their hideout. Upon learning that he is the son of the caliph, and impressed by his courage and determination, the thieves allow him to stay, and their leader, Old Baba (Fortunio Bonanova), adopts him as his son, Ali Baba.
 Jon Hall), now a grown man, however is suspicious and decides to scout the caravan first, along with his nanny Abdullah (Andy Devine). The bride turns out to be Amara (Maria Montez), Cassims daughter, who is to be wed to the Khan in order to solidify Cassims somewhat shaky standing with the Mongols.

In the meantime, Amara decides to take a bath in the oasis, where Ali encounters her (they do not recognize each other, however). Taking her for a mere servant girl and passing himself off as a traveller, he asks her about the caravan, then more about herself. But then it turns out that the caravan is in fact heavily guarded; Ali is ambushed and captured, while Abdullah narrowly escapes. Upon learning that the servant girl is the bride of the Khan (her name is not mentioned), Ali curses her for her supposed treachery. Hurt by his words and in growing admiration for him and his cause, she asks her servant and bodyguard, Jamiel (Turhan Bey), who hero-worships the 40 thieves, to give Ali some water for the trip.

In Bagdad, Ali is presented to the Khan, though he is not recognized as the leader of the 40 Thieves, and bound to a pillory in the palace square for public execution the next day. Cassim visits him in private and discovers Alis true identity, but keeps the knowledge to himself. Soon afterwards, the thieves mount a rescue, but Old Baba is mortally wounded; Amara, who went to see Ali to clear the misunderstanding between them, is kidnapped, and Jamiel personally cuts Ali loose from his bonds. The thieves retreat into Mount Sesame.

The next day, the thieves capture Jamiel, who was tracking them. Ali recognizes him as a friend, and Jamiel, who swears allegiance to Ali Baba, is assigned as a spy in the palace. His first task is to deliver a ransom note to the Khan: in exchange for his bride, Hulagu Khan is to surrender the traitor Cassim. The thieves proceed to Cassims mansion to await the traitors arrival. When Amara walks into the garden, Ali recognizes her as his lost love, and with his re-awakened feelings for her he decides to release her without waiting for her father. This initially arouses the anger of his band, but they still remain loyal to him.

When Amara returns to Bagdad, her father confesses Alis true identity to her and the Khan. Hulagu Khan decides to hold the wedding immediately; Amara refuses, but the sight of her father being tortured (actually, a ruse) forces her to give in. Jamiel brings the news to Ali, who decides to free his love. In order to reach the palace unnoticed, he devises the plan to pose as a merchant from Basra who brings forty huge jars of oil as a wedding gift. Jamiel returns to the palace to relay the plan to Amara, but they catch one of her servants eavesdropping. The girl then relays the news to Cassim and the Khan, who decide to welcome Ali in a fitting manner.

At the wedding day, Ali does appear as the merchant and is admitted as a guest. During an interlude, sword dancers appear, who first perform their routine and then suddenly plunge their weapons through the jar covers - but the jars contain only sand. Upon discovering the exposure of the original plan, Ali had decided to make a few changes: most of the thieves came disguised in the crowd; some others were hidden in jars which were not brought before the Khan.
 Arabian flag atop the palaces highest tower.

==Cast==
*Maria Montez as Amara Jon Hall as Ali Baba
*Turhan Bey as Jamiel
*Andy Devine as Abdullah
*Kurt Katch as Hulagu Khan
*Frank Puglia as Prince Cassim
*Fortunio Bonanova as Old Baba
*Moroni Olsen as Caliph Hassan
*Ramsay Ames as Nalu
*Chris-Pin Martin as Fat Thief
*Scotty Beckett as Ali Baba as a Child
*Yvette Duguay as Amara as a Girl
*Noel Cravat as Mongol Captain
*Jimmy Conlin as Little Thief
*Harry Cording as Mahmoud
*Ed Agresti as Mongol Captain (uncredited)
*Richard Alexander as Mongol Guard (uncredited)
*Jerome Andrews as Dancer (uncredited)
*Robert Barron as Mongol Captain (uncredited)
*Alphonse Bergé as Tailor (uncredited)
*Eric Braunsteiner as Dancer (uncredited)
*Ed Brown as Dancer (uncredited)
*John Calvert as Thief (uncredited)
*Fred Cavens as Thief (uncredited)
*Dick DArcy as Dancer (uncredited)
*William Wee Willie Davis as Arab Giant (uncredited)
*Dick Dickinson as Thief (uncredited)
*Rex Evans as Arab Major Domo (uncredited)
*Martin Faust as Thief (uncredited)
*Alex Goudavich as Dancer (uncredited)
*Hans Herbert as Thief (uncredited)
*David Heywood as Thief (uncredited)
*James Khan as Persian Prince (uncredited)
*Ethan Laidlaw as Thief (uncredited)
*Pierce Lyden as Guard (uncredited)
*George Martin as Dancer (uncredited)
*Don McGill as Guard (uncredited)
*Art Miles as Mongol Guard (uncredited)
*Belle Mitchell as Nursemaid (uncredited)
*Alma M. Pappas as Princess Kanza Omar (uncredited)
*Theodore Patay as Arab Priest (uncredited)
*Joey Ray as Thief (uncredited)
*Pedro Regas as Thief (uncredited)
*Alex Romero as Dancer (uncredited)
*Angelo Rossitto as Arab Dwarf (uncredited)
*Charles Wagenheim as Barber (uncredited)
*Norman Willis as Guard (uncredited)
*Harry Woods as Mongol Guard (uncredited)

==Production==
The role of Jamiel was meant to be played by Sabu Dastagir|Sabu. However when he went into the army, the role was taken by Turhan Bey. SCREEN NEWS HERE AND IN HOLLYWOOD: Turhan Bey, Czech Actor, Will Appear Opposite Katharine Hepburn in Dragon Seed
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   02 Oct 1943: 19.  

Maria Montez admitted she only acted "three or four times" in the time. FAIR AND SULTRY: Maria Montez has changed in the last year--she says...
Mason, Jerry. Los Angeles Times (1923-Current File)   12 Mar 1944: F15.  

==References==
 

==External links==
*  
*  
 

 

 
 
 
 
 
 
 
 
 