Scream 4
{{Infobox film
| name           = Scream 4
| image          = Scream4Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Wes Craven Kevin Williamson
| writer         = Kevin Williamson
| based on       = List of Scream characters|Characters: Kevin Williamson
| starring       = Neve Campbell Courteney Cox David Arquette Emma Roberts Hayden Panettiere
| music          = Marco Beltrami
| cinematography = Peter Deming
| editing        = Peter McNulty
| studio         = Corvus Corax Productions Outerbanks Entertainment
| distributor    = Dimension Films
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = $40 million 
| gross          = $97,138,686 
}} Kevin Williamson, Ghostface once Gale Weathers-Riley, Dewey Riley once again team up to stop the murders, but not before having to learn from a new generation the "new rules" of surviving horror films.
 box office, Scream 4 is intended to be the first of a new trilogy. Williamson had to leave production early due to contractual obligations and Ehren Kruger (Scream 3) was brought in for re-writes. Campbell, Arquette and Cox are the only returning cast members from the previous films and were the first to sign on to the film in September 2009. Panettiere and Rory Culkin were the first of the new cast to sign on in May 2010. Ashley Greene was initially the choice of the lead character, Jill, but the role eventually went to Roberts. Filming took place in and around Ann Arbor, Michigan in June 2010 to September 2010, with re-shoots in early 2011.

Scream 4 was released on April 15, 2011, and took in $19.3 million its opening weekend in the United States and Canada, making it the second-lowest opening since the first film.

==Plot== Jenny Randall Marnie Cooper are attacked and brutally murdered in their home by a new Ghostface (Scream)|Ghostface.
 Rebecca Walters. Trevor Sheldon, Olivia Morris Kirby Reed Dewey Riley, Judy Hicks. Meanwhile, Gale Weathers, who is now married to Dewey, is struggling with writers block. She decides that the new Ghostface murders could revamp her career and begins delving into the case, despite Deweys protests.
 Kate Roberts Charlie Walker Robbie Mercer, who explain that the killer is following the rules of movie remakes to plan the murders. Charlie concludes that the killer will likely strike at a party being held that night. Gale goes to the party to investigate and is stabbed in the shoulder by Ghostface before Dewey arrives; he takes her to the hospital. Meanwhile, two police officers assigned to patrol Jills house discover a window to be empty and are both murdered by Ghostface. Kate, meanwhile, arrives home as Sidney receives a phone call threatening to murder Jill, prompting the two to attempt to save her, as Jill does not answer her phone. It is then revealed that Jill is not at home, but at Kirbys place for an after-party. Ghostface is revealed to be outside of the house and stabs Kate in the back through the front doors mail slot, killing her. Sidney then runs into a suspicious Judy while escaping from the house and heads to Kirbys house to save Jill.

Ghostface appears at the party and kills a drunken Robbie soon before Sidney arrives at the house. During a confrontation with Ghostface, Sidney instructs Jill to hide under a bed upstairs, while she narrowly escapes Ghostface. Kirby is forced to answer horror movie trivia to save Charlie, who is tied up outside. After she answers Ghostfaces questions, she goes outside to untie Charlie, believing that she has won the game. He suddenly stabs her in the stomach and reveals himself as the Ghostface killer, leaving her for dead. Sidney is confronted by Charlie and a second Ghostface, who is revealed as Jill. Jill was jealous of the fame and attention Sidney received in the wake of the murders. Jill then pulls Trevor, her ex-boyfriend, out of a closet and shoots him in the groin for his previous betrayal of their relationship, and then in the head. Jill betrays Charlie by stabbing him, intending to pin him as Trevors accomplice. Jill then stabs Sidney in the stomach, leaving her for dead. She then trashes the house and mutilates herself in order to stage the scene. Dewey, Judy, and the rest of the police then stumble upon the carnage.

Sidney and Jill are rushed to the hospital. Dewey visits Jill and tells her that Sidney and Gale are actually still alive; Jill offhandedly remarks that she and Gale would make a good book-writing pair because of their matching wounds. Once Dewey leaves, Jill makes her way to Sidneys room to finish her off and badly injures her. Visiting Gale in her room, Dewey suddenly realizes that Jills bizarre knowledge of her and Gales "matching wounds" implicates her in the murders. He finds her in Sidneys room, is knocked out by Jill, and is quickly pursued by Gale and Judy. Jill shoots Judy, incapacitating her. Just as Jill is about to shoot Gale, Sidney shocks Jill with a defibrillator and then shoots her in the chest, finally killing her. It is revealed that Judy was wearing a bulletproof vest and is uninjured. Dewey calls in the police, as media reporters outside confirm Jill as a survivor and a "hero, right out of the movies."

==Cast==
 
  Dewey Riley, the Sheriff of Woodsboro, now married to Gale Weathers.
* Neve Campbell as Sidney Prescott, the Protagonist, the cousin of Jill Roberts and the main target of the new Ghostface. Gale Weathers-Riley, former reporter, now married to Dewey Riley. Jill Roberts, cousin Of Sidney and the ex-girlfriend of Trevor Sheldon. Kirby Reed, a film geek and Jills best friend, love interest of Charlie Walker. Anthony Perkins, Deweys deputy, who is to watch over the Roberts Residence with Hoss. Ross Hoss, Deweys other deputy, assigned with Perkins to watch over the Roberts residence. Charlie Walker, a film geek, Kirbys love interest and Robbie Mercers best friend. Kate Roberts, Aunt of Sidney and Jills mother and the sister of the deceased Maureen Prescott. Judy Hicks, Deweys assistant deputy and a former student at Woodsboro High, seeming to know Sidney from back in high school. Rebecca Walters, Sidneys book assistant, who seems to care about money and fame and is later fired by Sidney. Olivia Morris, Jill and Kirbys best friend and seemingly not liking Sidney, remarking on her as "Angel of Death". Trevor Sheldon, Jills ex-boyfriend, recently dating Jenny Randell. Robbie Mercer, a film geek and Charlie Walkers best friend; seemingly has a crush on Olivia Morris.
* Anna Paquin as List of Scream characters#Chloe and Rachel|Rachel, the best friend of Chloe, who loathes the Stab sequels, claiming them to be repetitive. 
* Kristen Bell as List of Scream characters#Chloe and Rachel|Chloe, the best friend of Rachel, who loves the Stab films.  
* Lucy Hale as List of Scream characters#Sherrie and Trudie|Sherrie, Trudies best friend and a horror film lover. 
* Shenae Grimes as List of Scream characters#Sherrie and Trudie|Trudie, Sherries best friend, who is not the biggest horror movie fan. Brittany Robertson Marnie Cooper, Jennys friend, who doesnt really dig horror movies, especially the Stab franchise. Jenny Randall, Marnies Friend who seems to know and love all the Stab films except Stab 5.
* Roger L. Jackson as the voice of Ghostface (Scream)|Ghostface, The Main Antagonist in all the series (voice only) to disguise the original killers voice.

===Cameos===
* Nancy ODell as TV Host
* Wes Craven as Man at Out of Darkness booksale
* Matthew Lillard as Guy at Stab-a-Thon
 

==Production==

===Development===
Scream 4 was announced by The Weinstein Company in July 2008,  with Wes Craven saying that he would not mind directing the film if the script was as good as Scream (1996 film)|Scream.  In March 2010 it was confirmed that he would indeed direct  and stated that, "I am delighted to accept Bob Weinsteins offer to take the reins on a whole new chapter in Scream history. Working with Courteney, David and Neve was a blast ten years ago and Im sure it will be again. And I cant wait to find the talent that will bring new blood to the screen as well. Kevin is right on his game with the new script – the characters and story crackle with energy and originality – to say nothing of some of the most hair-raising scares Ive seen in a script since... well, since the original Scream series. Let me at it". 

In May 2010, Cathy Konrad, who produced the first three films in the series, filed a $3 million lawsuit against The Weinstein Company, alleging that they violated a written agreement that entitled her company, Cat Entertainment, first rights to produce all films in the series.  The Weinsteins argued that this contract required Konrads services be exclusive to the franchise, which Konrad calls "false pretext". claiming the previous film did not require this condition.  The suit accuses the Weinsteins of surreptitious behavior and "a scheme to force Plaintiffs to walk away from the Scream franchise without compensation,"  enabling them to cut costs by hiring someone else to produce (Cravens wife, Iya Labunka, not named in the suit).  In April 2011, it was reported that the Weinsteins had settled out of court with Konrad, the details remaining confidential, though it was claimed that she would receive a cash payment plus a percentage of the profits from Scream 4. 

===Writing=== Ghostface murders but have been numerous sequels to the Story within a story|film-within-a-film Stab. He also commented on the life status of Sidney Prescott, "Shes done her best to move on from the events that occurred in the previous films, even releasing a successful book". Craven said that endless sequels, the modern spew of remakes, film studios, and directors are the butts of parodies in the film. The main characters have to figure out where the horror genre is in current days to figure out the modern events happening to and around them. 

In an early draft of the script, Gale and Dewey had a baby, but was changed after it was decided bringing a baby into the film would make shooting "impossible". In another early form of the script, the opening scene involved Sidney going head-to-head with Ghostface and being left for dead. There would have been a two-year gap in the story while she recovered, however, Bob Weinstein feared it would slow the pace of the story and bringing in young characters would work out best. 

Scream 3 writer Ehren Kruger was brought in during production to do re-writes. Craven said, "Look, there was a bumpy period when things shifted over from Kevin to Ehren. I signed up to do a script by Kevin and unfortunately that didnt go all the way through the shooting. But it certainly is Kevins script and concept and characters and themes".  It was reported that the actors were not given the 140-page script  past page 75 in order to protect the identity of the Ghostface killer. 

===Casting===
 
In September 2009, Variety (magazine)|Variety reported that Neve Campbell, David Arquette, and Courteney Cox would return.  Craven briefly explained their roles in a later interview with Entertainment Weekly, saying "Its a total integration of those three and new kids. The story of Sid, Gale, and Dewey is very much a part of the movie."  At a press conference for Repo Men, Liev Schreiber — who played Cotton Weary in the first three films — stated there were no plans for his reprisal.  In an interview with FEARnet, Williamson continued to deny a rumor of Jamie Kennedy returning, "I would love nothing more than to have Jamie Kennedy in the film. However to have Randy in the film, it sort of just takes it... I mean Scream 2 was a lie, you know? Its a false move. So I just wont do it. I cant do that. I just wont do it."  In April, over 12 casting sides were released to the public to buy for auditions of the film. 
 cameo and cameos in the beginning of the film akin to Drew Barrymore and Jada Pinkett Smith in the first and second Scream.  Shenae Grimes and Lucy Hale also have cameos in the film. 

===Filming===
 
On a budget of $40 million, principal photography began on June&nbsp;28, 2010.  Filming was scheduled to end on September&nbsp;6, after a 42-day shoot, but instead concluded on September&nbsp;24.  Filming took place in and around Ann Arbor, Michigan.   Scenes portraying Woodsboro High School school featured in the original Scream film were shot at Woodworth Middle School in Dearborn, Michigan.  The former 16th District Court in Livonia, Michigan was used as a police station. 

In April&nbsp;2010, while scouting for a bookstore to use in the film, Craven spotted a new bookstore that had not yet opened in downtown Northville, Michigan named Next Chapter Bookstore Bistro. Craven instantly loved the building as well as the name and decided to use both in the film. He also hired the owners chef to prepare the food and pastry for a scene in the film. The scenes were shot the first week of July.  After the test screening in January, Craven and Weinstein did not think two scenes played well for the audience. Aimee Teegarden and Alison Brie returned to Detroit in late January and early February of 2011 for four days of additional shooting. The scenes involved Teegardens character who is stalked at her home and Bries character who is attacked in a parking garage.   

The film also extensively used computer-generated imagery for the first time in the franchise. For example, instead of using a "collapsing knife", the knifes blade was added during post-production with CGI effects.  Andersons death scene in which he is stabbed in the forehead and walks a few feet while talking before finally falling to his death, was not in the script but was inspired by a "real-life medical emergency" Craven had seen in a documentary about a person being stabbed through their head and walked into an emergency room. He thought it was "extraordinary if somebody was stabbed in the head and still be alive for a while". Craven also did not tell the studio that he was taking this approach for the death scene, jokingly saying he hoped he would not be fired the next day. 

==Music==
{{Infobox album
| Name        = Scream 4 Original Motion Picture Soundtrack
| Type        = Soundtrack
| Cover       = Scream 4 (Original Motion Picture Soundtrack).jpg 
| Longtype    =
| Artist      = Various artists
| Released    = April 12, 2011
| Recorded    = rock
| Length      =  
| Label       = Lakeshore Records
| Producer    = Buck Sanders, Richard Glasser
| Compiled by =
| Chronology  =
| Last album  =   (2000)
| This album  = Scream 4 Original Motion Picture Soundtrack (2011)
| Next album  = 
}}
The Scream 4: Original Motion Picture Soundtrack was released on April 12, 2011 by Lakeshore Records.  A score soundtrack was also released, on April 19, 2011 by Varèse Sarabande. 

{{tracklist
| collapsed       = no
| headline         = Scream 4: Original Motion Picture Soundtrack
| writing_credits = yes
| extra_column = Artist
| total_length = 35:51
| title1 = Something to Die For Felix Rodriguez
| extra1 = The Sounds
| length1 = 3:42
| title2 = Bad Karma
| writer2         = Desmond Child Ida Maria Sivertsen Stefan Tornby
| extra2 = Ida Maria
| length2 = 2:55
| title3 = Cup of Coffee 
| writer3         = Corey Marriott Jay Marriott Steve Turnock Liam Young 
| extra3 = The Novocaines
| length3 = 1:30
| title4 = Make My Body
| writer4         = Christophe Eagleton Kamtin Mohager
| extra4 = The Chain Gang of 1974
| length4 = 3:37
| title5 =  Dont Mess with the Original 
| writer5         = Marco Beltrami
| extra5 = Marco Beltrami
| length5 = 3:33
| title6 = Yeah Yeah Yeah
| writer6         = Jesper Anderberg Johan Bengtsson Fredrik Blond Maja Ivarsson Felix Rodriguez
| extra6 = The Sounds
| length6 = 3:31
| title7 = Run for Your Life
| writer7         = Tamara Schlesinger
| extra7 = 6 Day Riot
| length7 =  2:32
| title8 = Axel F
| writer8         = Harold Faltermeyer
| extra8 = Raney Shockne
| length8 = 3:01
| title9 =  On Fire
| writer9         = Jesse Laz Locksley
| length9 = 1:54
| title10 =  Devils
| writer10         = Eric Elbogen
| extra10 = Say Hi
| length10 = 2:20
| title11 = Denial
| writer11         = Lucas Banker Logan Conrad Mader
| extra11 = Stereo Black
| length11 =  3:43
| title12 =  Jills America
| writer12         = Marco Beltrami
| extra12 = Marco Beltrami
| length12 = 3:26
}}

==Sequel==
In an interview with the writer Kevin Williamson confirmed his contractual obligation for Scream 4 and Scream 5 and had submitted concepts for three films leading up to Scream 6, though his contract for the sixth film had not yet been finalized. In May 2011, executive producer Harvey Weinstein confirmed that a sequel was possible, saying that despite Scream 4 performing below The Weinstein Companys financial expectations, he was still happy with the gross it had accrued.   In April 2015, when asked about a potential Scream 5, Williamson stated hes looking for fresh actors for the upcoming films."   

===Home media===
Scream 4 was first released on DVD and Blu-ray Disc|Blu-ray in Mexico on August 5, 2011.    It was later released in the United Kingdom and Ireland on August 22, 2011,   - Amazon.com.uk  in Canada and the United States on October 4, 2011,   and in Australia and New Zealand on October 13, 2011.  The film has made roughly $4,103,282 in DVD sales in the United States, bringing the films lifetime gross to approximately $101,334,702.  In the US DVD and Blu-ray rental charts, Scream 4 entered at #2 on its week of release.  The film then spent 7 consecutive weeks inside the top twenty of the chart.  Scream 4 made its television debut on April 20, 2012 on cable channel Showtime.  In December 2012, Showtime featured Scream 4 during a free weekend preview, where the station would be available in over 80 million homes in America.  On April 19, 2013, Scream 4 was added to Netflixs instant online streaming service. 

To promote the DVD and Blu-ray release, Universal Studios produced "Terror Tram: SCRE4M For Your Life" as an event featured in its annual Halloween Horror Nights throughout September and October 2011. 

==Reception==

===Box office===
Scream 4 was released in 3,305 theaters on 4,400 screens and grossed over $1 million in its midnight opening,  grossing an additional $8.7 million on Friday and another $7 million on Saturday,   opening at second place for the weekend.  According to "industry experts", the films $19.3 million opening weekend was "disappointing",   experiencing the second-lowest opening of the Scream (film series)|Scream franchise.  In its first weekend worldwide the film took $37.3 million from 30 territories, behind only Rio (2011 film)|Rio which took $53.9 million from 62 territories. The film topped the box office in the United Kingdom taking over £2 million, came in second in France, third in Mexico and fourth in Australia.  In its second weekend in the United States, it fell to fifth place, taking in $7.2 million. Scream 4 has grossed $97,138,686 at the worldwide box office.    

{| class="wikitable" style="width:99%;"
|-
! rowspan="2" style="text-align:center;"| Release date (United States)  
! rowspan="2" style="text-align:center;"| Budget (estimated)
! colspan="3" style="text-align:center;"| Box office revenue 
|-
!align="center"   | United States/Canada
!align="center"   | Other markets
!align="center"   | Worldwide
|- April 15, 2011
|align="center"   | $40,000,000 
|align="center"   |$38,180,928
|align="center"   |$58,957,758
|align="center"   |$97,138,686
|-
|}

===Critical reviews===
Scream 4 received polarizing but positive reviews from critics, with some reviewers criticizing the films "dated" formula, but was considered an improvement over Scream 3. Review aggregate Rotten Tomatoes reports that 58% of critics have given the film a positive review based on 173 reviews, with an average score of 5.8 out of 10.  Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a 52 based on 32 reviews.  CinemaScore polls reported that the average grade moviegoers gave the film was a B- on an A plus to F scale. 
  The New Minneapolis Star Tribune gave the film a perfect score of four out of four stars, praising the combination of scares, comedy, and twists. 
 Denver Post stated that Scream 4 "pays plenty of homage to their 1996 original", but that it is not close to its greatness, despite calling it a "cut above most slasher flicks".  Lisa Schwarzbaum of Entertainment Weekly praised the film, stating "Its a giddy reminder of everything that made Scream such a fresh scream in the first place,"  while Betsy Sharkey of the Los Angeles Times wrote that "Scream 4 finds a way to live up to its gory past while it carves out new terrors in new ways."  Peter Travers of Rolling Stone gave the movie two out of four stars, criticizing the comedic overtones. 
 Teen Choice Best Horror Movie but lost to Paranormal Activity 2. 
 Ghostface came in third place for Best Villain at the Virgin Media Movie Awards. 

==References==
{{reflist|30em|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

 {{cite news|url=http://www.fearnet.com/news/b19483_lauren_graham_says_so_long_scream_4.html|title=
Lauren Graham Says "So Long" to Scream 4|last=McCabe|first=Joseph|work=FEARnet|publisher=Horror Entertainment, LLC|date=June 30, 2010|accessdate=June 30, 2010|archiveurl=http://www.webcitation.org/5rCu9Nh29|archivedate=July 13, 2010|deadurl=no}} 

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

<!--
   -->

   

   

   

   

   

   

   

   

   

}}

=== Other references ===
*  -->  -->
*  -->  -->

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 