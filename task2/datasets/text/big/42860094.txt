Indian Beauty
{{Infobox film
| name = Indian Beauty
| image = 
| alt =  
| caption =
| director = Shanti Kumar
| producer = Shanti Kumar, Sanyasi Raju
| screenplay = Shanti Kumar , Kishore Dhara,Katherine Torpey,Eric Zaccar
| story = Shanti Kumar
| starring =  
| music = Joy Kelvin
| cinematography = Suresh Rohin
| editing = Nagi Reddy
| studio  = Trinity International
| released =  
| runtime = 125 minutes
| country = India Telugu
| budget =
|
}} romantic film directed by Shantikumar starring Collin Mcgee, Sairao, Gopichand Lagadapati & Manish Dayal.

==Cast==
* Gopichand Lagadapati as Vishal
* Manish Dayal as Jack
* Tanikella Bharani as himself
* Bramhanandam as himself
* Jayalalita as herself
* Nagineedu as Vishals father
* Sai Rao as Swapna 
* Collin Mcgee as Dave
* Susan Slatin as Granny
* Sukruta Shankar as Radha

==Plot==
Indian culture interests Dave (Colin McGee). So he decides to work on its culture. Dave has a friend Jack (Manish Patel) who is born in America but has roots in India. Jack joins Dave on his project on Indian culture. Mr. Murthy, a friend of Jacks father, offers them a place to stay. Initially Mr. and Mrs. Murthy are keen on taking Jack as their son-in-law, but noticing his American ways they prefer a local boy Vishal (Gopichand Lagadapati).   

Mr. Murthys daughter, Swapna, is also interested in Indian culture and hence decides to help Dave with his project. In the process of working both Swapna & Dave are attracted to each other before they finally fall in love. Whether Swapna & Dave finally be successful in convincing the parents forms rest of the story.

== Casting & Production ==
Major portion of this film was shot in Hyderabad while few scenes were shot in the United States.  The producer & director of the film, Shanti Kumar  selected Collin & Sai Rao  as the main lead roles along with Manish Dayal.Later Gopichand Lagadapati was selected to play the antagonist. Bramhanandam, Tanikella Bharani, Jayalalita, Nagineedu have cameos in the film.  

==Soundtrack==

{{Infobox album
| Name       = Indian Beauty
| Type       = Soundtrack
| Artist     = Joy Kelvin
| Released   =  
| Recorded   = 2006 Feature film soundtrack
| Length     = 27:94 Telugu
| Label      = Madhura
}}

{{Track listing
| collapsed       =
| extra_column    = Singer(s)
| total_length    = 27:94
| writing_credits = no
| lyrics_credits  = no
| music_credits   = no
| title1          = Priyathama Chitra , Priyasri
| length1         = 5:08
| title2          = Summer
| extra2          = Tippu,Priyasri,Prajesh
| length2         = 5:06
| title3          = Holi
| extra3          = Karthik , Anuradha Sriram
| length3         = 4:37
| title4          = Sankranthi
| extra4          = Anitha Krishna , Balaji
| length4         = 5:08
| title5          = Dream Girl
| extra5          = Unknown
| length5         = 3:23
| title6          = Re-mix
| extra6          = Tippu, Priyasri, Madhulika
| length6         = 5:12

}}

==Critical reception==
The film received average reviews from the critics.

Now Running - On the whole an enjoyable fare that is bound to be helpful to people justifying their cross cultural marriages. 

One India - It is a film worth watching by one and all. However, the commercial values in the film are a little less. But a film that could be watched together by the entire family. 

==References==
 

==External links==
* 
*  

 

 
 