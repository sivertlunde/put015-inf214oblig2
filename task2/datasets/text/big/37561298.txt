Ramaiya Vastavaiya
 
 

{{Infobox film
| name           = Ramaiya Vastavaiya
| image          = R Vasta Vaiya.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Prabhu Deva Kumar Taurani
| screenplay = M. S. Raju  (Original Screenplay)  Shiraz Ahmed  (Adapted Screenplay) 
| story    = Veeru Potla
| writer = Shiraz Ahmed   (dialogues) 
| music = Original songs: Sachin-Jigar Background Score Sandeep Shirodkar
| cinematography = Kiran Deohans
| editing = Hemal Kothari
| studio         = Tips Industries Limited Tips Films
| based on = Nuvvostanante Nenoddantana by Prabhu Deva & M. S. Raju
| starring = Girish Kumar Shruti Haasan
| released =  
| runtime = 148 minutes
| country = India
| language = Hindi
}}

Ramaiya Vastavaiya(Telugu:Ramayya,will you come?;name of a popular Telugu folk song adopted by a 1950s Hindi film with same name and again re-adopted in 2013) is a 2013 Bollywood romance comedy film directed by Prabhudheva and produced by Kumar S. Taurani, under Tips. Film stars debutant Girish Kumar alongside Shruti Haasan in lead roles.

The theatrical trailer of Ramaiya Vastavaiya was uploaded on 25 April 2013, whilst the film released on 19 July 2013.  Something Something... Unakkum Enakkum (2006)" starred Jayam Ravi and Trisha Krishnan. The First half of the film is closely related to the Punjabi film Tera Mera Ki Rishta starring Jimmy Shergill and Kulraj Randhawa.

==Plot==
Ram (Girish Kumar) is a rich, city boy, born to billionaire parents and brought up in Australia. On the other hand, Sona (Shruti Hassan)is a traditional, simple desi girl from Punjab who is brought up by her only brother, Raghuveer (Sonu Sood). He is heartbroken when their father marries another woman and throws them out of the house, humiliating them on the way. Their mother dies and her tomb is built on the small land which they own until the zamindar tells them that it is his land, since their mother had taken a loan from the man. Raghuveer volunteers to work day and night, to pay off the loan as long as they dont tear down his mothers tomb. The Zamindar agrees and the local station master helps them. Slowly Raghuveer and Sona grow up. One day, Riya, Sonas best friend, comes to their house to invite Sona to their house as she is getting married. Riyas cousin, Ram also arrives on the same day with his mother, Ashwini (Poonam Dhillon).

Slowly Ram and Sona fall in love but Rams mother does not bear it as Sona is not as rich as them, and is thus not to their standards; Ram is also to be married to Ashwinis brothers business partners daughter, Dolly. Ashwini humiliates Sona as well as Raghuveer, who arrives a minute before, and both are thrown of the house after Ashwini accuses them of trying to entice and trap Ram. When Ram learns of this, he goes to Sonas house and pleads to her brother to accept him. Raghuveer gives him a chance, just like he was given a chance by the Zamindar when he was little. Ram is tasked to take care of the cows, clean up after them and grow more crops than Raghuveer by the end of the season; if he does not, Ram will be thrown out of the village and can never see Sona again. The Zamindar and his son is not happy as the Zamindars son wanted to marry Sona. With them and Dolly and her father trying to get Ram to lose the competition, Ram has to work hard for his love, eating red chillies and rice everyday, even though he cant bear it. Through many antics from the Zamindars side and Dollys side, Ram eventually proves his love for Sona to Raghuveer, and succeeds in growing more grains. However, Zamindar & his son kidnap Sona and then later tries to rape her. A fight takes place in which Ram kills the Zamindars son. Raghuveer, after realizing that Ram and Sona should be together, takes the blame for this and spends seven years in prison. The movie ends with Raghuveers release from prison which is also when Sona and Ram get married, in everyones presence. Ashwini then accepts Sona to be her daughter-in-law.

==Cast==
* Shruti Hassan as Sona
* Girish Kumar as Ram
* Sonu Sood as Raghuveer, Sonas elder brother
* Randhir Kapoor as Siddharth, Rams Dad
* Poonam Dhillon as Ashwini, Rams Mom
* Vinod Khanna as Station Master
* Paresh Ganatra as Bijlee
* Satish Shah as Krishnakant, Rams maternal uncle
* Nassar as Jayprakash (J.P.), Krishnakants business partner
* Govind Namdeo as Zamindar Sarfaraz Khan as Zamindars son Zakir Hussain as Rao
* Shiraz Ahmed as Jailor
* Pankhuri Awasthi as Dolly
* Anchal Singh as Riya
* Aanshul Trivedi as Anshul (Riyas Husband)
* Prabhu Deva in a special appearance in song "Jaadu Ki Jhappi"
* Jacqueline Fernandez in a special appearance in song "Jaadu Ki Jhappi"
* Harry Josh as Afzal
* Namit Shah (child artist)
* Aarti Puri as Gauri

==Production== Telugu directorial debut Nuvvostanante Nenoddantana, which itself was inspired by the 1980s Hindi cult movie Maine Pyar Kiya. It is also a Hindi remake of the Bengali film "I love you".

==Character map and remakes==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;"
| Nuvvostanante Nenoddantana (2005) (Telugu cinema|Telugu) || Something Something... Unakkum Enakkum  (2006) (Tamil cinema|Tamil) || Neenello Naanalle (2006) (Cinema of Karnataka|Kannada) || Ramaiya Vastavaiya (2014) (Bollywood|Hindi)   || I Love You (2007 Bengali film) ||   Suna Chadhei Mo Rupa Chadhei (Oriya)   ||Nissash Amar Tumi (2010) (Bengali language|Bengali) (Dhaliwood|Bangladesh) 
|-
| Srihari || Prabhu Ganesan || Vishnuvardhan (actor) || Sonu Sood || Tapas Paul || Siddhanta Mahapatra || Misha Sawdagor 
|- Payal Sarkar || Barsha Priyadarshini|| Apu Biswas  ||
|- Siddharth || Dev  Anubhav Mohanty|| Shakib Khan
|}

==Soundtrack==
{{Infobox album |  
 Name = Ramaiya Vastavaiya
| Type = Soundtrack
| Artist = Sachin - Jigar
| Cover =
| Released = 19 July 2013
| Genre = Film soundtrack
| Length = 
| Label = Tips Industries Limited
| Producer = Kumar Taurani
| Last album = Go Goa Gone (2013)
| This album = Ramaiya Vastavaiya (2013)
| Next album = Issaq (2013)
}}
The first song promo of the film was released on 10 May 2013, under the title of "Jeene Laga Hoon", sung by Atif Aslam and Shreya Ghoshal. The song was uploaded on the Tips Music Films YouTube and channel (tipsmusic) and crossed over 20 million views on YouTube, whilst the full soundtrack released on 15 May 2013. Jeene Laga Hoon was the second biggest hit of 2013 after Tum Hi Ho. All songs of this album are composed by Sachin - Jigar.

===Track listing===
{{track listing
| headline =
| extra_column = Singer(s)
| collapsed = no
| total_length = 26:00
| title1 = Jeene Laga Hoon
| extra1 = Atif Aslam, Shreya Ghoshal
| length1 = 3:57
| title2 = Hip Hop Pammi
| extra2 = Mika Singh, Monali Thakur
| length2 = 3:40
| title3 = Bairiyaa
| extra3 = Atif Aslam, Shreya Ghoshal
| length3 = 4:08
| title4 = Peecha Chhute
| extra4 = Mohit Chauhan
| length4 = 3:47
| title5 = Rang Jo Lagyo
| extra5 = Atif Aslam, Shreya Ghoshal
| length5 = 4:56
| title6 = Jadoo Ki Jhappi
| extra6 = Neha Kakkar, Mika Singh
| length6 = 3:37
| title7 = Jadoo Ki Jhappi (Reprise)
| extra7 = Mika Singh, Neha Kakkar
| length7 = 2:11
}}

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 