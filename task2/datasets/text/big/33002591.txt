Tahalka
 
{{Infobox film
| name           = Tahalka
| image          = tahalkafilm.png
| image_size     = 
| caption        = Promotional Poster
| director       = Anil Sharma
| producer       =
| writer         = Anil Sharma
| narrator       = 
| starring       = Dharmendra Naseeruddin Shah Aditya Pancholi Javed Jaffrey Amrish Puri
| music          = Anu Malik
| cinematography = 
| editing        = 
| distributor    = 
| released       =  June 26, 1992
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Tahalka  is a 1992 Bollywood film directed by Anil Sharma and featuring an ensemble cast including Dharmendra, Naseeruddin Shah, Aditya Pancholi, Javed Jaffrey, Amrish Puri, Mukesh Khanna, Ekta Sohini, Pallavi Joshi, Sonu Walia, Shikha Swaroop and Firdaus Dadi in her acting debut.    The film was the fifth highest grosser of 1992 and was declared a "HIT".  

==Plot==
General Dong (Amrish Puri) is an evil dictator of a kingdom called Dongrila. He has kidnapped dozens of school girls and plans to use them as suicide bombers to bomb populated areas in India. One of his hostages is Dolly (Firdaus Dadi) who is the daughter of Major Krishna Rao (Mukesh Khanna). Krishna Rao recruits the best Indian army officers Captain Ranvir (Naseeruddin Shah), Captain Aditya (Aditya Pancholi), Captain Javed (Javed Jaffrey), Captain Anju (Ekta Sohini) and former disgraced Major Dharam Singh (Dharmendra). Together they go on a perilous journey between the borders of China and India to rescue Dolly as well as dozens of school girls held in Dongs captive and destroy Dongs kingdom of Dong-rila.

==Cast==
*Dharmendra ..... Major Dharam Singh
*Naseeruddin Shah .... Captain Ranvir
*Aditya Pancholi .... Captain Rakesh
*Javed Jaffrey .... Captain Javed
*Mukesh Khanna .... Major Krishna Rao
*Amrish Puri .... General Dong
*Shammi Kapoor .... Brigadier Kapoor
*Ekta Sohini .... Captain Anju Sinha
*Pallavi Joshi ... Julie
*Shikha Swaroop .... Captain Cynthia
*Sonu Walia ... Jenny D Costa
*Firdaus Dadi .... Dolly
*Gulshan Grover as Allah Rakha

==Soundtrack ==
{{Infobox album Name     = Tahalka Type     = film Cover    = Released =  Artist   = Anu Malik Genre  Feature film soundtrack Length   = 51:33 Label    = T-Series
}}

The music composed by Anu Malik.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics ||Duration
|-
| 1 || Dil Diwane Ka Dola Dilbur Ke Liye... || Anuradha Paudwal, Kumar Sanu, Babla Mehta|| Santhosh Anand || 9.28
|-
| 2 || Meri Chaththri Ke Neeche Aaja... || Mohammed Aziz, Sudesh Bhonsle, Anu Malik || Hasrat Jaipuri ||  10.45
|-
| 3 || Dil Diwani Ka Dola Dilbur Ke Liye... || Anuradha Paudwal || Santhosh Anand || 1.40
|-
| 4 || Shom Shom Shom... || Amrish Puri || Anu Malik || 3.10
|-
| 5 || Eya Eya O... || Anuradha Paudwal, Anu Malik || Santhosh Anand || 8.35
|-
| 6 || Aap Ka Chehra, Aap Ka Jalwa... || Anuradha Paudwal, Mohammed Aziz || Hasrat Jaipuri || 8.00
|-
| 7 || Garam Dharam Aji Kaisi Sharam... || Anu Malik, Abhijeet Bhattacharya || Hasrat Jaipuri || 8.58
|}

==References==
 

==External links==
*  

 
 
 
 
 
 