All Over the Town
{{Infobox film
| name           = All Over the Town
| caption        = Original film poster
| image	=	All Over the Town FilmPoster.jpeg
| director       = Derek N. Twist
| producer       = Ian Dalrymple Michael Gordon
| writer         = 
| screenplay     = Michael Gordon Derek N. Twist
| story          = 
| based on       =   Sarah Churchill Ronald Adam
| music          = Temple Abady
| cinematography = C.M. Pennington-Richards
| editing        = Sidney Stone
| studio         = Wessex Film Productions Pinewood Films
| distributor    = 
| released       =  
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Sarah Churchill and Cyril Cusack. It was based on a novel by R.F. Delderfield.

==Plot== Sarah Churchill), who had taken his job when he enlisted. Later, Nat becomes the owner of the paper, but his employees strike, disagreeing with Nats stance on Tormouths housing scheme. The town supports Nat in the dispute. 

==Cast==
* Norman Wooland as Nat Hearn  Sarah Churchill as Sally Thorpe 
* Cyril Cusack as Gerald Vane  Ronald Adam as Sam Vane 
* Bryan Forbes as Trumble  James Hayter as Baines 
* Fabia Drake as Miss Gelding 
* John Salew as Sleek 
* Stanley Baker as Barnes 
* Edward Rigby as Grimmett 
* Patric Doonan as Burton 
* Eleanor Summerfield as Beryl Hopper 
* Sandra Dorne as Marlene 
* Trevor Jones as Tenor 
* Frederick Leister as Wainer
* Lydia Bilbrook as Mrs Vane (uncredited)

==Production== Jack Lee, both formerly of the Crown Film Unit.    The film was shot in Lyme Regis.   

==Reception==
The New York Times described it as a "slow, dogmatic little picture" with a "dog-eared" plot.    In The Times, the films plot was seen as unoriginal, executed "without inspiration or any originality of thought". 

==Later history== negative at BFI National Film and Television Archive. In 2005, the Lyme Regis Film Society commissioned the production of a new print from the negative. This copy of the film is housed in Lyme Regis Museum and has been shown at the local Regent Cinema on a few occasions. 

==References==
 

==External links==
*   BFI Film & TV Database

 

 
 
 
 
 
 
 
 
 