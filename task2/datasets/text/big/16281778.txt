Ballad of the Little Soldier
 
{{Infobox film
| name = Ballad of the Little Soldier
| image = Werner Herzog - Ballad of the little soldier 000021855.jpg
| caption = Opening shot of the film.
| director = Werner Herzog Denis Reichle
| producer = Werner Herzog
| writer = Werner Herzog Denis Reichle
| starring = Werner Herzog Denis Reichle
| narrator = Werner Herzog
| music = Isidoro Reyes Paladino Taylor
| cinematography = Jorge Vignati Michael Edols
| editing = Maximiliane Mainka Werner Herzog Filmproduktion
| distributor = New Yorker Films
| released =  
| runtime = 44 minutes
| country = West Germany
| language = English German Spanish Miskito
}} children soldiers Miskito Indians who used children soldiers in their resistance against the Sandinistas.

Herzog made and co-directed the film at the request of his friend Denis Reichle, who himself served as a child-soldier in the Volkssturm at age fourteen in the aftermath of World War II.    The film is often cited as Herzogs most explicitly political, though Herzog denies that he had any specific statement on the politics of the Sandinistas. Herzog has said that the film is about child soldiers, and could have been made in any of several countries where child soldiers exist. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 
 