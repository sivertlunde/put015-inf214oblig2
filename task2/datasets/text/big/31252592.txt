The Ghost Train (1931 film)
{{Infobox film
| name           = The Ghost Train
| image          = 
| image_size     = 
| caption        = 
| director       = Walter Forde
| producer       = Michael Balcon   Phil C. Samuel
| writer         = Arnold Ridley (play) Lajos Bíró Angus MacPhail Sidney Gilliat
| starring       = Jack Hulbert Cicely Courtneidge Ann Todd Cyril Raymond
| music          = 
| cinematography = Leslie Rowson
| editing        = Ian Dalrymple
| studio         = Gainsborough Pictures
| distributor    = Woolf & Freedman Film Service
| released       =  
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 comedy thriller The Ghost Train by Arnold Ridley. The films art direction is by Walter Murton.

Thought to have been Lost film|lost, the film was found as a result of a 1992 British Film Institute campaign to locate missing movies. 

==Cast==
* Jack Hulbert – Teddy Deakin
* Cicely Courtneidge – Miss Bourne
* Ann Todd – Peggy Murdock
* Cyril Raymond – Richard Winthrop
* Allan Jeayes – Dr. Sterling
* Donald Calthrop – Saul Hodgkin
* Angela Baddeley – Julia Price
* Henry Caine – Herbert Price
* Tracy Holmes – Charles Bryant
* Carol Coomb – Elsie Bryant

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 