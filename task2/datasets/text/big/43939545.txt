Valayapathi (film)
{{Infobox film
| name           = Valayapathi வளையாபதி
| image          =
| image_size     =
| caption        =
| director       = T. R. Sundaram
| producer       = T. R. Sundaram
| writer         = 
| screenplay     = 
| starring       = G. Muthukrishnan Sowkar Janaki T. A. Jayalakshmi K. K. Perumal
| music          = S. Dakshinamoorthy
| cinematography = M. Masthan 
| editing        = 
| studio         = Modern Theaters
| distributor    = Modern Theaters
| released       = 17 Oct 1952
| country        = India Tamil
}}
 1952 Cinema Indian Tamil Tamil film,  directed  and produced by T. R. Sundaram. The film stars G. Muthukrishnan, Sowkar Janaki, T. A. Jayalakshmi and K. K. Perumal in lead roles. The film had musical score by S. Dakshinamoorthy.   

==Plot==
The film is based on the epic Valayapathi from  one of the The Five Great Epics of Tamil Literature.

==Cast==
 
*G. Muthukrishnan as Valayapathi
*Sowkar Janaki as Sathyavathi 
*T. A. Jayalakshmi as Andhari 
*K. K. Perumal as Kanakavelalar 
*A. Karunanidhi
*T. P. Muthulakshmi
*M. S. S. Bhagyam
*Pulimoottai’ Ramasami
*M. N. Krishnan
*M. E. Havana
*S. R. Lakshmi
*K. K. Soundar
*S. M. Thirupathisami
*C. K. Soundararajan
*K. T. Dhanalakshmi
*M. A. Ganapathi
*‘Master’ Sudhakar
*‘Master’ Ramakrishnan
*A. S. Mani
 

==References==
 

==External links==
*  

 
 
 
 


 