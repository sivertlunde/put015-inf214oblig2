Violet Tendencies
 
{{Infobox film
| name = Violet Tendencies
| image = Violet Tendencies.jpg
| alt = 
| caption = 
| director = Casper Andreas
| producer = Casper Andreas Jesse Archer
| writer = Jesse Archer
| starring = Mindy Cohn Marcus Patrick
| music = Michael Barry
| cinematography = Timothy Naylor
| editing = Craig Cobb
| distributor = Embrem Entertainment
| released =  
| runtime = 99 minutes
| country = United States
| language = English
| budget = 
| gross = 
}}
Violet Tendencies is a 2010 romantic comedy film directed by Casper Andreas, written by Jesse Archer, and starring Mindy Cohn and Marcus Patrick. The film was released in the United States on November 19, 2010, and came out on video on demand in March 2011 and DVD on May 24, 2011.     

==Plot==
Violet (Cohn) is forty, fabulous, and the ultimate fag hag. Fed up with being single, she decides to distance herself from her gay friends in order to find a straight boyfriend. 

==Cast==
{| class="wikitable"
|-
! style="background:silver;"| Actor
! style="background:silver;"| Role
|-
| Mindy Cohn || Violet
|-
| Marcus Patrick || Zeus
|-
| Jesse Archer || Luke
|-
| Samuel Whitten || Riley
|-
| Casper Andreas || Markus
|-
| Kim Allen || Salome
|-
| Adrian Armas || Darian
|-
| Armand Anthony || Vern
|-
| Dennis Hearn || Bradleigh
|-
| Andrea Cirie || Donna
|-
| Sophia Lamar || Larice
|-
| Hilary Elliot || Marjorie Max
|-
| Margret R.R. Echeverria || Audrey
|-
| Shari Albert || Ashley
|-
| Michael Scott || Stephen Miser
|-
| Michelle Akeley || Darians Receptionist
|-
| Christopher L. Graves || Mike
|-
| John-Patrick Driscoll || The Mailman
|-
| Michael Cornacchia || Donnie the Waiter
|-
| Jonathan Chang || Andre
|-
| Brandon Gill || J Flame
|-
| Ben Pamies || Gerald
|-
| J.R. Rolley || Derek
|-
| Sonja Rzepski || Jenny
|-
| Howard Feller || Homeless Man
|-
| Fredrick Ford || Darians Trick
|-
| Kohl Beck || Beer Can Dan
|-
| Sal Blandino || Clipboard Fundraiser 1
|-
| Ali Mroczkowski || Clipboard Fundraiser 3
|-
| Ryan Turner || Clipboard Fundraiser 2
|}

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 