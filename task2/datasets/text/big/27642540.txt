No Sex Please, We're British (film)
{{Infobox film
| name           = No Sex Please: Were British
| image          = "No_Sex_Please,_Were_British"_(1973).jpg
| image_size     = 
| caption        = 
| director       = Cliff Owen
| producer       = John R. Sloan
| writer         = Alistair Foot Anthony Marriott (play) Brian Cooke Johnnie Mortimer (adaptation)
| narrator       = 
| starring       = Ronnie Corbett Ian Ogilvy Susan Penhaligon Beryl Reid Arthur Lowe Eric Rogers
| cinematography = Ken Hodges
| editing        = Ralph Kemplen
| distributor    = Columbia Pictures
| released       = 25 July 1973 (UK) 10 August 1979 (USA)
| runtime        = 91 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 British comedy film directed by Cliff Owen and starring Ronnie Corbett, Ian Ogilvy, Susan Penhaligon and Arthur Lowe. It is based on the 1971 play No Sex Please, Were British with a number of changes to the original plot.

==Synopsis== Windsor High farcical series of events including a bank inspector, the police and a local criminal to whom the pornography actually belongs.

==Main cast==
* Ronnie Corbett – Brian Runnicles
* Ian Ogilvy – David Hunter
* Susan Penhaligon – Penny Hunter
* Beryl Reid – Bertha Hunter
* Arthur Lowe – Mr Bromley Michael Bates – Mr Needham
* Cheryl Hall – Daphne David Swift – Inspector Paul
* Deryck Guyler – Park keeper
* Valerie Leon – Susan
* Margaret Nolan – Barbara
* Gerald Sim – Reverend Mower
* John Bindon – Pete
* Stephen Greif – Niko
* Michael Robbins – Car driver
* Frank Thornton – Glass Shop Manager
* Michael Ripper – Traffic warden
* Lloyd Lamble – American man
* Mavis Villiers – American lady
* Sydney Bromley – Rag & Bone Man
* Brian Wilde – Policeman in park
* Eric Longworth – Man with Lighter
* Edward Sinclair – Postman
* Robin Askwith – Bakers delivery man

==Critical reception==
*Writing in 1979 at the time of the American release, a reviewer for The New York Times wrote: "In its own way it is well done...(with) its simpleminded and by now rather outdated double and triple entendres" 
*TV Guide commented: "A pleasing performance from Corbett...saves this otherwise average British farce from the usual doldrums." 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 
 