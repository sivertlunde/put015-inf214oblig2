Starship Invasions
 
 
{{Infobox Film
| name           = Starship Invasions
| image          = Starship Invasions1977.jpg
| caption        = 
| director       = Ed Hunt
| producer       = Ed Hunt
| writer         = Ed Hunt
| narrator       = 
| starring       = {{plainlist|
* Robert Vaughn
* Christopher Lee
}}
| music          = 
| cinematography = 
| editing        =
| studio         = Hal Roach Studios 
| distributor    = Warner Bros.
| released       =  
| runtime        = 89 minutes
| country        = Canada
| language       = English
| budget         = $1 million 
| gross          = 
}} Ed Hunt and filmed in Toronto, Ontario. It was re-released in the United Kingdom as Project Genocide.

==Synopsis==
The plot concerns the black-clad Legion of the Winged Serpent, a rogue group of human-like telepathic aliens led by Captain Rameses (Christopher Lee). The Legions home planet Alpha in the Orion constellation is about to be destroyed in the imminent supernova of its star, and Rameses is leading a small force of flying saucers to Earth to examine its suitability for their race. Performing several alien abductions, they discover they are descendants of transplanted humans and that the planet is perfect for them. They plan to make way for themselves after killing everyone through the use of a device whose signals prompt people to commit suicide.

Opposing any such plan is the Intergalactic League of Races, a highly advanced group of bald, big-headed aliens from Zeta Reticuli. The League operates a hidden observation base on Earth in the form of a golden pyramid deep beneath the ocean. Rameses lands at the base pretending to be a friendly researcher, and the League reminds him that under the Galactic Treaty he is to have no contact with humans. He is then disturbed to see a television broadcast featuring human UFO expert and astronomer Professor Allan Duncan (Robert Vaughn) discussing Rameses abductions. He laughs it off and indulges in the local entertainment.

Rameses crew sabotages one of the Leagues saucers, which is later shot down approaching a US Army base. The League sends one of its ships to investigate, and Rameses and his crew kill everyone left in the base. One of the League saucers manages to return but its crew is killed in a shootout. A second destroys Rameses own ship, but combat causes its computer to burn out and prevent further action. Rameses then calls in his fleet, in hiding behind the Moon, to hunt down the remaining League ship. He also deploys the "extermination device", an orbiting, global-scale version of the suicide device. The US armed forces discover it, but are powerless to prevent the ensuing suicide epidemic.

The surviving League ship requires repair, and decides to contact Duncan for help. He enlists the help of Malcolm, a computer expert, who repairs the ship using parts picked up in downtown Toronto. They are discovered shortly after taking off and are intercepted by one of Rameses ships, but they shoot it down and it crashes into First Canadian Place. Duncan and Malcolms abduction makes the front page of the Toronto Star. After repairs and refueling they leave Earth in an attempt to enlist the help of other League ships. Malcolms improvised repairs burn out shortly past the Moon, so Duncans knowledge of the masses of the planets is put to use by Malcolms pocket calculator to plot their course to the outer solar system.

The ship successfully reaches a League squadron, and they set out to attack the Legion. Rameses uses the computer in the League base to calculate superior strategies and begins to destroy the League ships. One of the robots in the base is only damaged, not destroyed, and re-takes command. He causes the extermination unit to destroy itself, and then directs Rameses ships to collide with each other. His fleet destroyed, the super-weapon eliminated, and his sun gone supernova, Rameses crashes his ship into the Moon.

During the action the extermination unit had passed over Toronto, causing Duncans wife (Helen Shaver) to slash her wrists. The League races to Duncans home and easily revives her.

==Cast==
 
* Robert Vaughn as Professor Allan Duncan
* Christopher Lee as Captain Rameses
* Daniel Pilon as Anaxi
* Tiiu Leek as Phi
* Helen Shaver as Betty Duncan
* Henry Ramer as Malcolm
* Victoria Johnson as Gazeth
* Doreen Lipson as Dorothy
* Kate Parr as Diane Duncan
* Sherri Ross as Sagnac
* Linda Rennhofer as Joan
* Richard Fitzpatrick as Joe
* Ted Turner as Zhender Sean McCann as Carl
* Bob Warner as an Air Force General
 

==Production and release== Hal Roach Studios producers Earl A. Glick and Norman Glick bankrolled the production with $1 million.     Many elements of the film, including the design of the robots and the winged serpent emblem the black-clad villains wear, are taken from UFO accounts. 

Starship Invasions was released in VHS format by Warner Home Video.  It was also released in 1987 on video in the United Kingdom by Krypton Force under the title Project Genocide. 

Its French-language title was Linvasion des soucoupes volantes.   

==Critical reception== Globe and Mail reviewer Robert Martin panned Starship Invasions, likening the film to "those dubbed Japanese movies usually seen on Saturday afternoon television".     John Duvoli of The Evening News called it a "poor imitation" of previous science fiction films.   Janet Maslin of The New York Times wrote that the film is too low budget and derivative to even appeal to science fiction fans.   It remains Hunts most sought-after film. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 