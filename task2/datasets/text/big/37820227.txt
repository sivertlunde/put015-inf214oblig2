Chappy – That's All
{{Infobox film
| name           = Chappy - Thats All
| image          =
| caption        =
| director       = Thomas Bentley
| producer       = 
| writer         = Oliver Sandys (novel)   Eliot Stannard Lewis Gilbert
| music          =
| cinematography = 
| editing        = 
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       = July 1924  
| runtime        =
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    = silent drama Lewis Gilbert. It was based on a novel by Oliver Sandys. It was made at Stoll Pictures Cricklewood Studios.

==Cast==
* Joyce Dearsley as Chappy 
* Gertrude McCoy as Bettina  Lewis Gilbert as Piper 
* Eva Westlake as Mrs. Cherry 
* Edwin Greenwood as Slim Jim 
* Francis Lister

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 