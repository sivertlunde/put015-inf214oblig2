Mitt (film)
{{Infobox film
| name           = Mitt
| image          = Mitt film.jpg
| alt            = 
| caption        = 
| director       = Greg Whiteley Adam Leibowitz   Greg Whiteley Erin Whiteley 
| writer         = Greg Whiteley
| starring       = Mitt Romney
| narrator       = 
| music          = Perrin Cloutier
| cinematography = Greg Whiteley 
| editing        = Adam Ridley  Greg Whiteley 
| studio         = 
| distributor    = Netflix
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 2008 and 2012 presidential campaigns of former Massachusetts governor Mitt Romney. Mitt premiered at the Sundance Film Festival on January 17, 2014. The film was released on Netflix on January 24, 2014.    

==Background== Ann liked the idea. Although Whiteley had initially planned to only cover the 2008 election, he eventually ended up filming over a period of six years. 

==Synopsis== Republican Party presidential candidates. After he loses the Republican nomination, he returns in 2012 to challenge incumbent Barack Obama for the White House. Meanwhile, his wife is diagnosed with multiple sclerosis, and he worries about disappointing his supporters and family if he loses. 

==Reception==
Mitt received mainly positive reviews from critics. The aggregate Rotten Tomatoes site gives it an average rating of 81%. 

==References==
 

==External links==
*   on Netflix
* , written/directed by Greg Whiteley
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 

 
 