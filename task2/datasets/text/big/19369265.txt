Team Batista no Eikō
 Japanese mystery television show TBS suggested replacing the character with a young female resulting in Yuko Takeuchi being cast as in the role.    For the TV show, Taguchi is a man, with Atsushi Itō playing the role.

==Plot== Batista Operation Ministry of Health, Labour and Welfare, who re-launches the investigation on the basis that the deaths were actually murders.

==Film==
{{Infobox film
| name           = The Glorious Team Batista
| image          = The Glorious Team Batista-Movie.jpg
| caption        =
| director       = Yoshihiro Nakamura
| producer       = Kanjirō Sakura Akihiro Yamauchi
| writer         = Hiroshi Saitō Mitsuharu Makita
| narrator       = Hiroshi Abe
| music          = Naoki Satō
| cinematography = Yasushi Sasakibara
| editing        = Hirohide Abe
| distributor    = Toho
| released       =  
| runtime        = 118 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          = ¥1.6 billion JPY 
}} adapted for the screen by Hiroshi Saitō and Mitsuharu Makita. The film began shooting October 7, 2008 and was primarily shot in Tokyo. The Glorious Team Batista was in theaters on February 9, 2008 and became a commercial success, grossing 264 million yen from 284 screens its opening weekend. 

When The Glorious Team Batista was screened at the Udine Far East Film Festival, it received mediocre reviews. Todd Brown of Twitch wrote:

 "The characters are stock at best, the story arc blandly predictable and the ending drawn out beyond reasonable bounds.  The technical end is solid and the performances good enough but the script for this just feels far more like a lengthy episode of prime time network television from the pre-HBO era – much more Murder She Wrote or Matlock than Dexter – than it feels like the feature film – adapted from a popular mystery novel – that it is.  Horrible?  No, just horribly bland."  

Similarly, Ross Chen of Love HK Film wrote:

 "...The Glorious Team Batista is a ragingly obvious commercial film, with manufactured characters, drama, and situations that seem ripped from a pulpy bestseller you might find featured in an airport bookstore (Surprise! The film is based on a novel.). This is a big-screen medical thriller built for mainstream audiences, and it seldom delivers anything beyond the required or expected... Regardless, the whole is too glossy and commercial to be anymore more than an average medical thriller suited to undemanding audiences."  
 Hiroshi Abe will reprise their roles, with director Yoshihiro Nakamura also returning. Masato Sakai has been cast as the films antagonist.     

===Cast===
* Yuko Takeuchi &ndash; Kōhei Taguchi Hiroshi Abe &ndash; Keisuke Shiratori
* Koji Kikkawa &ndash; Kyoichi Kiryu
* Hiroyuki Ikeuchi &ndash; Ryo Narumi
* Tetsuji Tamayama &ndash; Toshiki Sakai
* Haruka Igawa &ndash; Naomi Otomo
* Hiromasa Taguchi &ndash; Takayuki Haba
* Yuu Shirota &ndash; Kōichirō Himuro
* Shiro Sano &ndash; Yuji Kakiya
* Yoko Nogiwa &ndash; Makoto Fujiwara
* Sei Hiraizumi &ndash; Seiichiro Kurosaki
* Jun Kunimura &ndash; Gonta Takashina

==TV Show==
{{Infobox television
| show_name            = Team Batista no Eikō
| image                =
| caption              = Mystery
| camera               =
| picture_format       = NTSC
| audio_format         =
| runtime              = 54 min./episode
| creator              =
| developer            =
| producer             = Yoko Toyofuku Kōichi Toda Kaoru Yamaki
| executive_producer   =
| starring             = Atsushi Itō Tōru Nakamura (actor)|Tōru Nakamura
| voices               =
| narrated             =
| theme_music_composer =
| opentheme            =
| composer             = Kei Haneoka Takeshi Senō
| endtheme             = Mamoritai Mono (Thelma Aoyama)
| country              =
| location             = Tokyo Japanese
| network              = Fuji TV
| first_aired          = October 14, 2008
| last_aired           = December 23, 2008
| num_series           =
| num_episodes         = 11
| list_episodes        =
| preceded_by          =
| followed_by          =
| related              =
| website              =
}}

===Cast===
* Atsushi Itō &ndash; Kōhei Taguchi
* Tōru Nakamura (actor)|Tōru Nakamura &ndash; Keisuke Shiratori
* Tsuyoshi Ihara &ndash; Kyoichi Kiryu
* Daisuke Miyagawa &ndash; Ryo Narumi
* Hiroki Suzuki &ndash; Toshiki Sakai
* Yumiko Shaku &ndash; Naomi Otomo
* Masahiro Toda &ndash; Takayuki Haba
* Yū Shirota &ndash; Kōichirō Himuro
* Shingo Tsurumi &ndash; Yūji Kakitani
* Yuko Natori &ndash; Makoto Fujiwara
* Ryuzo Hayashi &ndash; Kenta Takashina
* Kim Ji Sun &ndash; Kanako Miyahara
* Takaaki Enoki &ndash; Ichirō Kurosaki
* Chika Uemura &ndash; Kyoko Hoshino
* Akio Yokoyama &ndash; Shuzo Taguchi
* Risa Saiki &ndash; Midori Taguchi
* Mikeo Ishii &ndash; Akane Taguchi
* Kaoru Okunuki &ndash; Kishikawa Marie
* Hoka Kinoshita &ndash; Tamotsu Kishikawa

===Episodes===
{|class="wikitable" width="98%"
|- style="border-bottom:3px solid #CCF"
! Episode !! Title !! Writer !! Director !! Original airdate
{{Japanese episode list
 |EpisodeNumber=01
 |EnglishTitle=Puzzle ~Medical malpractice? Murder?!~
 |RomajiTitle=
 |Aux1=Noriko Goto
 |Aux2=Hisashi Ueda
 |KanjiTitle=謎 	医療ミスか? 殺人か!?
 |OriginalAirDate=2008-10-14
 |ShortSummary=
 }}
{{Japanese episode list
 |EpisodeNumber=02
 |EnglishTitle=Rumour ~Active Phase (Active Investigation) vs Passive Phase (Passive Investigation)~
 |RomajiTitle=
 |Aux1=Noriko Goto
 |Aux2=Hisashi Ueda
 |KanjiTitle=噂 	アクティヴ·フェーズ(能動的調査)  VS パッシヴ·フェーズ(受動的調査)
 |OriginalAirDate=2008-10-21
 |ShortSummary=
}}
{{Japanese episode list
 |EpisodeNumber=03
 |EnglishTitle=Bond ~Anesthesiologists confession~
 |RomajiTitle=
 |Aux1=Noriko Goto
 |Aux2=Kazuhisa Imai
 |KanjiTitle=絆 	麻酔科医の告白
 |OriginalAirDate=2008-10-28
 |ShortSummary=
}}
{{Japanese episode list
 |EpisodeNumber=04
 |EnglishTitle=Gaze ~Elites pride and weakness~
 |RomajiTitle=
 |Aux1=Noriko Goto
 |Aux2=Kazuhisa Imai
 |KanjiTitle=望 	エリートのプライドと弱点
 |OriginalAirDate=2008-11-04
 |ShortSummary=
}}
{{Japanese episode list
 |EpisodeNumber=05
 |EnglishTitle=Love ~With a criminals motive~
 |RomajiTitle=
 |Aux1=Noriko Goto
 |Aux2=Hisashi Ueda
 |KanjiTitle=恋 	犯人の目星はついた
 |OriginalAirDate=2008-11-11
 |ShortSummary=
}}
{{Japanese episode list
 |EpisodeNumber=06
 |EnglishTitle=Absolute crime in the operating room... You are the perpetrator
 |RomajiTitle=
 |KanjiTitle=オペ室の完全犯罪...犯人はお前だ
 |Aux1=Noriko Goto
 |Aux2=Hisashi Ueda
 |OriginalAirDate=2008-11-18
 |ShortSummary=
}}
{{Japanese episode list
 |EpisodeNumber=07
 |EnglishTitle=Real criminal appears ~Wrong deduction... the real criminal appears!~
 |RomajiTitle=
 |KanjiTitle=真犯人登場 	間違っていた推理...真犯人登場!
 |Aux1=Noriko Goto
 |Aux2=Kazuhisa Imai
 |OriginalAirDate=2008-11-25
 |ShortSummary=
}}
{{Japanese episode list
 |EpisodeNumber=08
 |EnglishTitle=Quivering scalpel ~Rifts between brother-in-laws... Abnormal occurrence during the operation!~
 |RomajiTitle=
 |KanjiTitle=震えるメス 	義兄弟の亀裂...オペで異常事態!
 |Aux1=Noriko Goto
 |Aux2=Kazunari Hoshino
 |OriginalAirDate=2008-12-02
 |ShortSummary=
}}
{{Japanese episode list
 |EpisodeNumber=09
 |EnglishTitle=False alibi and fatal mistake
 |RomajiTitle=
 |KanjiTitle=偽アリバイと致命的ミス
 |Aux1=Noriko Goto
 |Aux2=Hisashi Ueda
 |OriginalAirDate=2008-12-09
 |ShortSummary=
}}
{{Japanese episode list
 |EpisodeNumber=10
 |EnglishTitle=Final puzzle solving...This is the medical trick!!
 |RomajiTitle=
 |KanjiTitle=
 |Aux1=Noriko Goto
 |Aux2=
 |OriginalAirDate=2008-12-16
 |ShortSummary=
}}
{{Japanese episode list
 |EpisodeNumber=11
 |EnglishTitle=Absolute crime in only 3 seconds!!The most dangerous operation starts right now
 |RomajiTitle=
 |KanjiTitle=
 |Aux1=Noriko Goto
 |Aux2=
 |OriginalAirDate=2008-12-23
 |ShortSummary=
}}
 |} 

==References==
 

==External links==
*  
*  
*  
* 
* 

 
 
 
 
 
 
 
 
 
 
 