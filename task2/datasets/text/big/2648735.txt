Jane Eyre (1996 film)
{{Infobox film
| name        = Jane Eyre
| image       = Jane eyre ver1.jpg
| caption     = Theatrical release poster
| writer      = Hugh Whitemore Franco Zeffirelli (based on the novel by Charlotte Brontë)
| starring    = Charlotte Gainsbourg William Hurt Anna Paquin Joan Plowright
| director    = Franco Zeffirelli|
| producer    = Dyson Lovell
| music       = Claudio Capponi Alessio Vlad David Watkin
| editing     = Richard Marden
| distributor = Miramax Films (US) Pathé (UK)
| released    =  
| runtime     = 116 minutes
| language    = English French
| country     = France Italy United Kingdom United States
| gross       = $5,200,601 
}}
 novel of the same name. This Hollywood version, directed by Franco Zeffirelli, is similar to the original novel, although it compresses and eliminates most of the plot in the last quarter of the book (the running away, the trials and tribulations, new found relations, and new job) to make it fit into a 2-hour movie.

==Plot summary== Jane Eyre Maria Schneider), who is mentally ill and kept locked away in an upstairs attic with a nurse, Grace Poole (Billie Whitelaw).

The marriage is stopped by Berthas brother Richard Mason (Edward de Souza) and lawyer Briggs (Peter Woodthorpe). Jane flees, her world crashing down is a nightmare. She recovers in the parsonage of her original aunts home, discovers she is now a wealthy woman inheriting her long-lost uncles fortune in Madeira. She gets a proposal of marriage from Parson St. John Rivers but her heart and soul is with Rochester. She goes back to find Thornfield Hall burnt down but Rochester is crippled and blinded by the fire set by his mad wife, Bertha, who has killed herself in the fire. However, Janes love for Rochester remains undiminished; she nurses him back to health, he recovers his eyesight and they marry.

==Cast== Jane Eyre
*William Hurt as Edward Fairfax Rochester Young Jane Eyre
*Fiona Shaw as Mrs. Reed John Wood as Mr. Brocklehurst
*Geraldine Chaplin as Miss Scatcherd
*Amanda Root as Miss Temple
*Leanne Rowe as Helen Burns
*Richard Warwick as John
*Joan Plowright as Mrs. Fairfax
*Judith Parker as Leah
*Josephine Serre as Adele Varens
*Billie Whitelaw...Grace Poole
*Elle Macpherson as Blanche Ingram
*Julian Fellowes as Colonel Dent
*Edward de Souza as Richard Mason
*Peter Woodthorpe as Briggs
*Ralph Nossek as Reverend Wood Maria Schneider as Bertha Mason
*Samuel West as St. John Rivers
*Charlotte Attenborough as Mary Rivers

==Production==
The location for Thornfield Hall is Haddon Hall, Bakewell, Derbyshire, UK   Since Zeffirellis use of Haddon Hall, subsequent versions of Jane Eyre have used it and is now apparently synonymous with Thornfield Hall. Prior to Zeffirellis location use, Haddon Hall had been once used (before Jane Eyre) as the castle for The Princess Bride.

==Reception== Byronic type", but conceded that the film "has its moments". 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 