When a Man Loves a Woman (film)
 

{{Infobox film
 | name = When a Man Loves a Woman
 | image = When a man loves a woman.jpg
 | caption = Theatrical release poster
 | director = Luis Mandoki
 | writer = Ronald Bass, Al Franken
 | starring = Andy García Meg Ryan Lauren Tom Tina Majorino Mae Whitman Ellen Burstyn
 | producer = Jon Avnet
 | music = Zbigniew Preisner
 | cinematography = Lajos Koltai
 | studio = Touchstone Pictures Buena Vista Pictures
 | budget =
 | released = April 29, 1994 (limited)   May 13, 1994 (USA)
 | runtime = 126 min.
 | language = English
 | gross = $50,021,959
 | }}
 American romantic drama film written by Al Franken and Ronald Bass, starring Andy García, Meg Ryan, Tina Majorino, Mae Whitman, Ellen Burstyn, Lauren Tom and Philip Seymour Hoffman.
 alcoholic mother, Best Female song of the same name by Percy Sledge.

==Plot==
The movie chronicles one womans (Meg Ryan) alcoholism and her husbands (Andy Garcia) efforts to help her.

Meg Ryan plays Alice Green, a school counselor who has a serious drinking problem and is married to Michael (Andy Garcia), an airline pilot. Though shes lighthearted and loving, Alice is often reckless and, when drunk, even neglects her children, nine-year-old daughter Jess (Tina Majorino) from a previous marriage, and four-year-old daughter Casey (Mae Whitman), whose father is Michael. 

One afternoon, Alice enters their home in a drunken incoherent state.  She dismisses the reluctant caretaker, who leaves her alone with her children.  Still drinking, Alice is confronted by her eldest daughter concerned for her mothers welfare.  In return, she violently slaps Jess, who runs to her room crying.  Alice enters the shower.  Unable to control her balance and calling for Jess, she falls to the side and smashes through the shower door onto the bathroom floor.  Fearing her mother has died, Jess contacts Michael who immediately flies home to be by his wifes side.

After the incident and while in the hospital, Michael and Alice confront the truth about Alices drinking.  They jointly decide she must seek professional help for her alcoholism.  Upon release from the hospital, a timid Alice enters a rehabilitation clinic.

Michael finds himself now the main caretaker of their home and two children, a role he struggles to maintain along with his career as an airline pilot.  Meanwhile, at the clinic, Alice is flourishing; her recovery is painful but stabilizing and she is well liked and respected by both staff and fellow clinic tenants alike.  During a family visit day at the clinic, Alice immediately begins to rebuild her shattered bond with the children leaving Michael alone to wander the grounds uncomfortable and out of place in his wifes new lifestyle.

Alice returns home sober yet guarded.  She is vocal, strong and changed.  Michael is having trouble adjusting to Alices balance. He has become used to being the stable and controlled one in their relationship and is jealous of Alices lack of dependence and outside friendships.  Coming to terms with their estrangement, a reluctant Michael (believing therapy is for the weak) and a willing Alice see a marriage counselor who quickly establishes Michaels "co-dependency" on Alices role as an alcoholic.

Unable to find a medium and with tempers flared, Michael moves out and Alice becomes the main caretaker of their home and children. Alice is once again seen flourishing in her new role while Michael is unable to find control and seeks out a support group for spouses of alcoholics.  Initially shy, Michael becomes a more vocal member of the group and shares his sorrow over his lack of understanding for the gravity his wifes sobriety would have on him, his children and his marriage.

Alice and Michael singularly return to the hospital to celebrate the birth of their caretakers baby.  They spend time together and as they depart Alice asks Michael if he would attend her 180 day sober speech where she will acknowledge her failings and accomplishments.  She also tells him that she has been thinking about asking him to come home with her.  Michael tells Alice he has been offered a job in Denver.  For the first time since they both agreed Alice should enter rehab, they both agree Michael should take the position.
 
The penultimate scene is Alice as she stands on a stage and tells her sobriety story; the toll it took on her, her children and her marriage.  She is funny, confident, sad but optimistic.  Her audience is moved to tears.  Her speech ends and she is surrounded by well wishers.  Out of the crowd appears Michael.  At ease with himself and Alice, he explains what he missed along the way..."to listen, to really listen."  

They share an intense, longing, passionate kiss as the credits roll.

==Cast==
*Andy García as Michael Green
*Meg Ryan as Alice Green
*Lauren Tom as Amy
*Philip Seymour Hoffman as Gary
*Tina Majorino as Jess Green
*Mae Whitman as Casey Green
*Ellen Burstyn as Emily

== Release and reception == The Crow.  When a Man Loves a Woman currently holds a 70% fresh rating among critics at Rotten Tomatoes, although the film has a mixed reception and is seen unfavorably by many because of its length and the way it deals with alcoholism and stress in the family.   James Berardinelli stated that the "ending is too facile", and that the film took "longer than necessary to arrive at its resolution", adding that there are moments in the film where the script would strike a raw nerve with certain people because of how it judges alcoholics and deals with issues related to alcoholism.    However, he said that the "films poignancy is its strength, even if occasional didactic tendencies are its weakness".  David Denby of New York Magazine called it an "earnest and highly prolonged counseling disappointment", a "pushy therapeutic exercise" which, although intelligent, features "endless talk, a stunted mise en scène, and a moral atmosphere of dogged and literal-minded persistence" which "overvalues its own sobriety".   

== Soundtrack ==
 
# When a Man Loves a Woman
# Crazy Love
# El Gusto  
# Main Title  
# Garbage Compulsion
# Homecoming  
# I Hit Her Hard  
# Dressing Casey  
# Gary  
# Michael Decides  
# Alice & Michael  
# Running From Mercy by Rickie Lee Jones
# Im a Good Man by Robert Cray
# Everybody Hurts by R.E.M.
# Stewarts Coat by Rickie Lee Jones
 

== References ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 