Between Day and Night
{{Infobox film
| name           = Between Day And Night
| image          = 
| caption        = 
| director       = Horst E. Brandt
| producer       = 
| writer         = Erich Weinert
| starring       = Hermann Beyer
| music          = 
| cinematography = Günter Haubold
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = East Germany
| language       = German
| budget         = 
}}

Between Day and Night ( ) is a 1975 East German drama film directed by Horst E. Brandt. It was entered into the 9th Moscow International Film Festival.   

==Cast==
* Hermann Beyer as Carl
* Kurt Böwe as Erich Weinert Michael Christian as Fred
* Yelena Drapeko as Sinaida
* Wolfgang Greese as Wilhelm
* Gert Gütschow as R.
* Rolf Hoppe
* Stefan Lisewski as Hans K.
* Katja Paryla as Li Weinert
* Leonid Reutov as Wolodja
* Dietmar Richter-Reinick as Kumpel
* Gudrun Ritter as Carls Wife
* Gisela Stoll as Elsa
* Olga Strub as Marianne
* Rudolf Ulrich as Peter
* Manfred Zetzsche as Ernst

==References==
 

==External links==
*  

 
 
 
 
 
 
 