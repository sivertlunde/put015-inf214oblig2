Double-Cross (2014 film)
  }}

{{Infobox film
| name           = DOUBLE-CROSS
| image          = 
| caption        = Theatrical Release Poster
| director       = Pascal Aka
| producer       = Ama K. Abebrese D.R. Kufuor
| writer         = D.R. Kufuor
| starring       = Ama K. Abebrese John Dumelo 
| music          = Roger Ebo   Mensa Ansah   Obrafuor
| cinematography = Pascal Aka  Prince Dovlo
| editing        = Pascal Aka
| studio         = DBF Productions   Breakthrough Media Production
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Ghana
| language       = English
| budget         =
| gross          = 
}}
 epic romance romantic thriller thriller told, written, co-produced,  by D.R. Kufuor. It stars Ama K. Abebrese and John Dumelo as the main cast of the movie. The movie was shot predominately in the North Legon Area of Accra Ghana.

==Cast==

* Ama K. Abebrese as Effie Howard
* John Dumelo as Danny Frimpong
* Adjetey Anang as Ben Boateng
* Paulina Oduro as Obaabeng Frimpong
* Jasmine Baroudi as Vickie Mensah
* Samuel Odoi-Mensah as Johnny Yawson

==Release==
The World Première of Double-Cross was on October 31, 2014 at the Greenwich Odeon Cinema in London, England. The Ghana Première on February 6, 2014 at the Silverbird Cinema in Accra, Ghana.

==Critical response==
The film was met with generally positive reviews from critics. Babso.Org was impressed with the intrigue and suspense at the end and would say it was a good movie better than most of the movies seen and had to review.  Ghana celebrities did not particularly like the ending contrary to many comments by the attendants. 

==Accolades==
{| class="wikitable"
|- Year
!align="left"|Ceremony Category
!align="left"|Nominated
!Result
|- 2014
|align="left" Ghana Movie Awards Actress in a Leading Role Ama K. Abebrese
| 
|- Best Director Pascal Aka
| 
|- Best Editing Pascal Aka
| 
|- Best Cinematography Pascal Aka & Prince Dovlo
| 
|-
|align="left"|Make–Up & Hair Styling Nana Ama Atsu
| 
|- Best Picture
|align="elft"|D.R. Kufuor & Ama K. Abebrese
| 
|-
|}

 

==References==
 