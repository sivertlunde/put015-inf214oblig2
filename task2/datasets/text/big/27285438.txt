Uncertain Glory
{{Infobox film
| name           = Uncertain Glory
| image          = Poster of the movie Uncertain Glory.jpg
| image_size     = 220px
| caption        =
| director       = Raoul Walsh
| producer       = Robert Buckner
| writer         = László Vadnay Max Brand based on = original story by Joe May László Vadnay
| narrator       =
| starring       = Errol Flynn Paul Lukas
| music          = Adolph Deutsch
| cinematography = Sidney Hickox
| editing        = George Amy
| studio         = Thomson Productions
| distributor    = Warner Bros.
| released       =   (US) 1951 (France)
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
| gross          =1,022,524 admissions (France)  
}}

Uncertain Glory is a 1944 film directed by Raoul Walsh and starring Errol Flynn and Paul Lukas and. 

==Plot== French Sûreté Vichy police. To buy time in order to escape again, Picard persuades Bonet to let him pose as one of the saboteurs to save the hostages. In the course of enacting a story that will convince the Vichy that Picard is one of the escaped saboteurs, he and Bonet encounter the real saboteur, captured by the Vichy, and aid him in escaping.

Picard falls in love with Marianne (Jean Sullivan), a local girl, and with freedom just ahead for both of them, struggles with his conscience over the fate of the hostages, her trust in him, and his own perception of himself.

==Cast==
 
* Errol Flynn as Jean Picard
* Paul Lukas as Inspector Marcel Bonet
* Lucile Watson as Mme. Maret
* Faye Emerson as Louise
* James Flavin as Captain, Mobile Guard
* Douglass Dumbrille as Police Commissioner LaFarge (as Douglas Dumbrille)
* Dennis Hoey as Father Le Clerc
* Sheldon Leonard as Henri Duval
* Odette Myrtil as Mme. Bonet
* Francis Pierlot as Father La Borde
* Jean Sullivan (in her debut film) as Marianne 
* Victor Kilian as Latour
* Pedro de Cordoba as Executioner 
* Fred Cordova as Execution Guard
 

==Production==
In September 1942 it was announced that Flynn had signed a new contract with Warners for four films a year, one of which he was to also produce.  This was the first film produced under Flynns new contract with Warners which allowed him a say in the choice of vehicle, director and cast, plus a portion of the profits. He formed his own company, Thomson Productions, to make Uncertain Glory and planned to make a series of films with director Raoul Walsh. Thomas et al. 1969, p. 136. 

Paul Lukas - who had just had a big hit with Watch on the Rhine, was attached in July 1943.  Faye Emerson and Jean Sullivan were signed to support. DRAMA AND FILM: Wally Beery, Daughter May Do Film Together Constance Moore Will Appear Opposite George Murphy in Show Business
Schallert, Edwin. Los Angeles Times (1923-Current File)   27 Aug 1943: 12.  

Principal photography on Uncertain Glory started in August 1943.  During filming it was announced Warners would rush release plans on this and Passage to Marseilles, another drama set in occupied France. DRAMA AND FILM: La Dietrich Will Give Three Cheers for Boys Maureen OHara Todays Selection for Leading Femme Role in Army Wife
Scheuer, Philip K. Los Angeles Times (1923-Current File)   09 Sep 1943: 17.  

Some filming took place in the grape country in Escondido. While shooting there, labor-strapped farm hands insisted the unit had to pick grapes with them before they would allow filming to take place. ACTORS HAD TO HARVEST CROP
Los Angeles Times (1923-Current File)   23 May 1944: A10.  

==Reception==
The Washington Post said "Flynn has never given a more restrained, earnest and believable portrayal ... there is guile, sly humour, an appealing bravado, grim rebellion, gentleness, charm, in his drawing of a character that is alternately enigmatic and transparent. Mr Flynn is more of an actor than many have thought." 

==References==
Notes
 

Bibliography
 
* Behlmer, Rudy. Inside Warner Brothers, 1935-51.  London: Weidenfeld & Nicolson, 1987. ISBN 978-0-2977-9242-0.
* Thomas, Tony, Rudy Behlmer and Clifford McCarty. The Films of Errol Flynn. New York: Citadel Press, 1969. ISBN 978-0-80650-237-3.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 