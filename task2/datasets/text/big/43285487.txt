We Thieves Are Honourable (1956 film)
{{Infobox film
| name           = We Thieves Are Honourable
| image          =
| image_size     =
| caption        =
| director       = Pedro Luis Ramirez
| producer       =
| writer         = Enrique Jardiel Poncela  (play) Vicente Coello Vicente Escrivá
| narrator       =
| starring       = José Luis Ozores José Isbert Encarna Fuentes Rafael Bardem
| music          = Federico Contreras
| cinematography = Federico G. Larraya
| editing        = José Antonio Rojo 
| studio         = Aspa Producciones Cinematográficas
| distributor    = As Films
| released       =  
| runtime        = 89 minutes
| country        = Spain
| language       = Spanish
| budget         =
}} play of adapted into a 1942 film..

==Cast==
*   José Luis Ozores as El Castelar  
* José Isbert as El Tío del Gabán  
* Encarna Fuentes as Herminia 
* Rafael Bardem as Don Felipe Arévalo  
* Julia Caba Alba as Eulalia  
* Carlos Miguel Solá as Daniel El Melancólico  
* José Manuel Martín as Antón, el mayordomo  
* José Ramón Giner as Dr. Ramiro Laredo  
* Isabel Pallarés as Teresa, la nueva ama de llaves 
* Antonio Ozores as Menéndez  
* Nora Samsó as Doña Andrea  
* Pilar Gómez Ferrer as Monchi, la esposa del Dr. Laredo  
* Joaquín Roa as Vigilante  
* María Isbert as Berta  
* Emilio Santiago as El Titi  
* Juana Ginzo as Criada del Rastro  
* Julio Goróstegui 
* Manuel Aguilera as Benito Ortega  
* Jacinto San Emeterio as Empleado de la joyería  
* Juan Cazalilla as Otro empleado  
* Ángel Álvarez as Farmacéutico  
* Milagros Leal as Mujer de Benito 
* Alicia Palacios as Germana, la hermana de Don Felipe  
* Antonio Garisa as El Pelirrojo 
* José María Rodríguez as Sacristán  
* Antonio Molino Rojo as Invitado de la fiesta  

== References ==
 

== Bibliography ==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer, 2008. 

==External links==
*  

 
 
 
 
 
 
 

 