Oranje Hein (1925 film)
 
{{Infobox film
| name           = Oranje Hein
| image          = 
| image_size     = 
| caption        = 
| director       = Alex Benno
| producer       = 
| writer         = Alex Benno Herman Bouber
| narrator       = 
| starring       = 
| music          =   
| cinematography = H. W. Metman
| editing        = Piet Vermeulen
| distributor    = 
| released       = 2 October 1925
| runtime        =  
| country        = Netherlands
| language       = Silent
| budget         = 
}} 1925 Netherlands|Dutch silent film directed by Alex Benno.

==Cast==
* Johan Elsensohn - Hein de Klopper alias Oranje Hein
* Aaf Bouber - Aal, vrouw van Hein
* Maurits de Vries - Thijs
* Vera van Haeften - Ant, vrouw van Thijs
* Marie Van Westerhoven - Moeder van Ant
* August Van den Hoeck - Vader van Ant
* Heintje Davids - Naatje Visch
* Pauline Hervé
* Riek Kloppenburg - (as Rika Kloppenburg)
* Marie Schafstad
* Elize le Maire
* Jetty Kremer
* Anton Gerlach
* Piet Fuchs
* Harry Boda

== External links ==
*  

 
 
 
 
 


 