A River Changes Course
{{Infobox film
| name        = A River Changes Course
| image       =
| caption     = Promotional film poster
| director    = Kalyanee Mam
| starring    = Sari Math, Khieu Mok, Sav Samourn
| producer    = Kalyanee Mam, Ratanak Leng
Executive Produced by Youk Chhang
| editing     = Chris Brown
| music       = David Mendez
| released    =  
| runtime     = 83 minutes
| country     = Cambodia
| language    = Khmer, Jarai
}}
A River Changes Course is a 2013 documentary by Kalyanee Mam. The film explores the damage rapid development has wrought in her native Cambodia on both a human and environmental level.  The film premiered at the 2013 Sundance Film Festival on January 21, 2013 and won the Grand Jury Prize for World Documentary. The film also received the Golden Gate Award for Best Documentary Feature at the 2013 San Francisco International Film Festival. 

== Synopsis ==
"Weve worked so hard on this land,” says Sav Samourn. “And now theyve come to destroy it all. Sooner or later it will all be gone.”

A River Changes Course intimately captures the stories of three families living in Cambodia as they strive to maintain their traditional ways of life amid rapid development and environmental degradation.

Deep in the jungle, Sav Samourn struggles as large companies encroach and “progress” claims the life-giving forests. She discovers there’s little room for wild animals, ghosts – and the home she has always known.

In a fishing hamlet, Sari Math must quit school to help support his family. But as the fish catch dwindles, Sari and his family find their livelihood threatened.

In a village, Khieu Mok must leave to seek work in a Phnom Penh factory to help pay her family’s debts. But city life proves no better, and Khieu struggles between her need to send money home and her duty to be with her loved ones.

From Cambodia’s forests to its rivers, from its idyllic rice fields to the capital’s pulsing heart, forces of radical change are transforming the landscape of the country – and the dreams of its people.

The film’s original Khmer title, Kbang Tik Tonle, is a term that describes the traditional practice of dipping one’s hands into the water and drinking the water with both hands. This single act connects the Cambodian people to the water, to nature, and ultimately to life.

== Release == The San Francisco International Film Festival, Full Frame Documentary Film Festival, Seattle International Film Festival,  The Los Angeles Asian Pacific Film Festival, Environmental Film Festival at Yale, Environmental Film Festival in the Nation’s Capital, RiverRun International Film Festival, Atlanta Film Festival, Nashville Film Festival, Green Film Festival in Seoul, Docville International Documentary Film Festival, Lincoln Film Society - Season Of Cambodia, The Museum of Modern Art ContemporAsian Film Program, Sydney Film Festival, Biografilm Festival, Jerusalem Film Festival, DocFest Munich, and many more prior to its theatrical release and broadcast worldwide.

== Reception ==
During its theatrical run, the New York Times selected A River Changes Course as a "NYT Critics Pick".  Nicole Herrington found Ms. Mams "intimate portrait of three Cambodian families struggling to maintain their ways of life amid environmental degradation caused by development is a beautiful example of the form. Instead of delving into the politics of their plight, Ms. Mam delivers a universal story" and that "this human story is profound enough to stand on its own." 

Joe Morgenstern of the Wall Street Journal wrote, "Beautiful images can be a distraction in a serious documentary, but thats hardly the case here. They draw us in so we can better understand the hurtling changes that endanger the future of Cambodia and, by extension, much of the developing world." 

While Sheri Linden of the Los Angeles Times found "Mams camera work is exquisite in its immediacy and agility. One of the most striking aspects of her film is the intimacy it achieves without feeling intrusive or turning her subjects into fodder for a message." 

During its festival run, Doug Cummings at LA Weekly highlighted the film as one of four movies to see at the Los Angeles Asian Pacific Film Festival, regarding it as “a beautifully lensed and compelling portrait,” “Mam’s eye for composition is extraordinary,” and concluding that “never alarmist or sensational, the film grips with quiet sincerity.” 

Writing for the San Francisco Bay Guardian, Dennis Harvey picked the film as one of five must-see documentaries at the San Francisco International Film Festival calling it “a vividly-shot, meditative look at lives being forced to modernize.” 

The Huffington Post called the film “breathtakingly beautiful…breaks new ground,”  echoing the words of The Phnom Penh Post, which noted the film as “a profound new take on the Cambodian experience.” 
After its Australian premiere at the Sydney Film Festival, The AU Review found “The power of the film is in the telling of it”  while Bermuda News concurred that this “cinematically spectacular and sensory journey is a profound portrait.”  Khmericain, a multimedia website for Cambodian-Americans wrote this in their review: “The raw, straightforward portrayal compels us to think…and find parallels to our own experience.” 

== Awards ==
* Grand Jury Prize for World Cinema Documentary at the 2013 Sundance Film Festival. 
* Grand Jury Prize for Best Feature at the 2013 Atlanta Film Festival. 
* Center for Documentary Studies Filmmaker Award at the 2013 Full Frame Documentary Film Festival. 
* Human Rights Award at the 2013 River Run Film Festival.  
* Grand Jury Prize for Best Feature at the 2013 Environmental Film Festival at Yale.  
* Golden Gate Award for Best Documentary Feature at the 2013 San Francisco International Film Festival.  
* Best Director and Best Cinematography at the 2013 Los Angeles Asian Pacific Film Festival.  
* Grand Jury Conscience Award at the 2013 Docville International Documentary Film Festival.  
* Grand Jury Prize for Best Documentary Feature at the 2013 Green Film Festival in Seoul. 
*Best Documentary at the 2013 Tacoma Film Festival  
*Audience Award at the 2013 Tutti nello stesso piatto Festival  
*Nominated for the Independent Spirit Awards Stella Artois Truer Than Fiction Award,  presented to the director of a non-fiction film that displays the finest artistic vision.   
*Special Mention, International Documentary Association Pare Lorentz Award, which recognizes films that demonstrates exemplary filmmaking while focusing on the appropriate use of the natural environment, justice for all and the illumination of pressing social problems. 

== See also ==
* Kalyanee Mam(Mother Of Porn)
* Youk Chhang
* Documentation Center of Cambodia
*  
*  

== References ==
 

== External links ==
*  
*  

 
 
{{Succession box
| title= 
| years=2013
| before=The Law in These Parts
| after=The Return to Homs}}
 

 

 
 
 
 
 
 
 