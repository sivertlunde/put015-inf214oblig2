Goyband
{{Infobox Film
| name           = Goyband
| image          = 
| caption        =  
| director       = Christopher Grimm
| producer       = Christopher J. Scott David Schiavone Red Sky Pictures
| writer         = Christa McNamee Dan Bar Hava Christopher Grimm
| starring       = Adam Pascal Amy Davidson Natasha Lyonne Tovah Feldshuh Cris Judd
| music          = Dan Bar-Hava
| cinematography = 
| editing        = Glen L. Scott
| distributor    = 
| released       = December 13, 2008
| runtime        = 99 minutes
| country        = United States
| language       = English language|English, Yiddish
| budget         = 
| gross          = 
}}
  American Independent Independent comedy film directed by Christopher Grimm, starring Adam Pascal. Called a "gefilte fish out of water tale", this independent film is described as Dirty Dancing meets My Big Fat Greek Wedding with a touch of a hip hop Fiddler on the Roof. Fans of Adam Pascal and friends of the other actors have start a cult internet following, garnering interest in Goyband worldwide.

==Synopsis==
Fading from the spotlight of his late 90s mega-fame, boy-band icon Bobby Starr (Adam Pascal) is clinging to days gone by and begging his agent Murray to land him a decent gig. What Murray does land for Bobby is a full week headlining the grand opening of the worlds first Glatt Kosher hotel-casino, Mazel Hotel.
 Orthodox otherworld” is the hotel owners persuasive daughter Rebekka Hershenfeld (Amy Davidson), who has harbored a huge crush on Bobby since childhood. Rebekkas world is guided by her arranged betrothal to Haim (Benjamin Bauman), the son of Grand Rabbi Sheinman (Joel Leffert), who is supposed to issue the casinos all-important "Kosher certificate."

Rebekkas only release from the pressures of preparing to be a future Rebitsin (Rabbis wife) comes from singing along to Bobbys songs with her best friends Hani and Fani (Natasha Lyonne). They keep their practice session a secret, since their religion bars them from singing in public.
 David Stars instead of lemons, cherries and dollar signs, and chime Hava Nagila for lucky winners.

Arranging for Bobby Star to play at her fathers hotel might have started out as an act of rebellion for Rebekka, but when infatuation blossoms into romance, Bobby offers Rebekka a once-in-a-lifetime ticket to freedom from the constrictive life that threatens to hold her back.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Adam Pascal     || Bobby Starr 
|-
| Amy Davidson || Rebekka
|-
| Natasha Lyonne    || Fani
|-
| Zoe Lister-Jones || Hani
|-
| Tovah Feldshuh  || Leah
|-
| Dean Edwards   || Ty
|-
| Cris Judd     || Elliot
|-
| Erik Liberman      || Marvin
|- Joel Leffert || Grand Rabbi Sheinmann
|-
| Glen Wein     || Isaac
|-
| Larry Goldstein      || Raziel
|-
| Benjamin Bauman   || Haim Sheinmann
|-
| Bern Cohen     || Jeremiah the Gambler
|-
| Wendy Diamond & Lucky   || Wendy Diamond & Lucky  
|- William Wise      || Manny
|}

==Behind the Scenes==
Adam Pascal was already familiar with this kind of resort, having spent summers in the Catskills while growing up. 

==Cult following==
Fans, friends of the actors and crew, and the extras promoted Goyband for over two years while the film was being edited and readied for viewing. Chat rooms  are one place fans who had yet to see the film had expressed their interest in it due to the actors and subject matter.

==Name change==
On September 14, 2009, MarVista Entertainment announced both their distribution rights to the film Goyband and their choice to rename the movie Falling Star (note the one "r", which is different from the name of the title character). Though the name has changed, the original site of   has not.

==References==
 

==External links==
*  
*  
* http://www.rottentomatoes.com/m/falling_star/
*  
* https://www.youtube.com/watch?v=2x924gmwfjg
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 