Cinderella (1979 film)
Cinderella Soviet film created by the studio Soyuzmultfilm. It is based on Charles Perraults story, Cinderella.

== Storyline ==
After her mother died, Cinderella started to live with her spoiled stepmother, her two sisters, and her father. She did their chores and was a servant to the entire family. One day after the entire family leaves for the ball, Cinderella is left alone, along with doing chores that her stepmother has assigned her to do. After finishing everything, a Fairy Godmother appears, giving Cinderella a chance to go to the ball by creating her everything she needs for transport, especially a dress with slippers. The Fairy Godmother tells Cinderella that after twelve, everything shall fade away, and her voice trails of as Cinderella is being droven of to the ball in a carriage created from a pumpkin. At the ball, Cinderella meets a prince, and together they fall in love and sing their duet. However it is not long until twelve oclock has struck, making Cinderella run down the stairs of the ball, leaving one of her slippers behind. She is eventually quickly led back home, and everything turned back to the way it was. The family eventually returns, and the next day the prince and a couple of assistants have started coming over to each house to measure the slipper. At the time they reach the house of Cinderellas family, they have measured everybodys foot, with Cinderella being the only one who has her foot fit into the slipper. Together, Cinderella and the prince recognize each other, and they once again start to sing the same duet as they sang at the ball.

The cartoon features a magnificent musical content, presenting a classic Soviet animation.

== External links ==
*    
*  

 

 
 
 


 