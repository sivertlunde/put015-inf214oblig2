Inspiration (1915 film)
{{Infobox film
| name = Inspiration
| image = Inspiration (1915) Audrey Munson and Thomas A Curran.jpg
| caption = Audrey Munson in Inspiration
| director = George Foster Platt
| producer = Edwin Thanhouser
| writer = Virginia Tyler Hudson (scenario)
| starring = Audrey Munson Thomas A. Curran
| cinematography = Lawrence E. Williams 
| studio   = Thanhouser Film Corporation
| distributor = Mutual Film Corporation
| runtime = 
| released = November 18, 1915 (US) April 21, 1918 (US re-release)
| country = United States
| language = Silent (English intertitles)
}}
 silent drama directed by George Foster Platt and starring Audrey Munson. It is notable for being the first non-pornographic American film to feature full nudity of a woman. On reissue in 1918, the film was called The Perfect Model. All copies of the movie are believed to have been lost film|lost. 

== Plot == model to inspire his work. He finds a poverty-stricken girl who he thinks is the one he has been looking for. When she wanders off, he visits all the famous statues in Manhattan hoping to find her again.

== Cast ==
* Audrey Munson as The Model
* Thomas A. Curran as The Artist
* George Marlo as The Artists Friend
* Bert Delaney as The Artists Friend
* Carey L. Hastings - Undetermined Role
* Ethyle Cooke - Undetermined Role
* Louise Emerald Bates - Undetermined Role

==Reception==
  (center) as the artists model in the film, with sculptor Daniel Chester French (at left).]]

The censors were reluctant to ban the film, fearing they would also have to ban Renaissance art. The film was a box office success, though the reviews were very polarized.

==See also==
*Nudity in film
*List of lost films

==References==
 

== External links ==
 
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 