The Squatter's Daughter (1910 film)
 
{{Infobox film
  | name     = The Squatters Daughter
  | image    = 
  | caption  = 
  | director = Bert Bailey		 William Anderson
  | writer   =  play by Edmund Duggan Edmund Duggan Olive Wilton
  | music    = 
  | cinematography = Orrie Perry
  | editing  = 
  | distributor = Johnson and Gibson 
  | released = 4 August 1910
  | runtime  = 6,000 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = £1,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998,10    
  }}
 play by Edmund Duggan.

==Synopsis== Ben Hall which was not used when the play was adapted again in The Squatters Daughter (1933 film)|1933. 

==Cast==
*Olive Wilton as Violet Enderby
*Bert Bailey as Archie McPherson Edmund Duggan as Ben Hall
*J.H. Nunn as James Harrington
*Rutland Beckett as Dudley Harrington George Cross as Tom Bathurst
*George Mackenzie as Nick Harvey
*Temple Harrison as Nulla Nulla
*Edwin Campbell as Billy
*Eugenie Duggan 

==Production== William Anderson at the Kings Theatre Melbourne, many of whom had just appeared in The Man from Outback, also by Bailey and Duggan.  Theatre star Olive Wilton played the lead role, with Bailey and Duggan in support. One of her leading men, George Cross, later became a casting director for Cinesound Productions.

Shooting took place in Ivanhoe and other surrounding districts of Melbourne entirely outdoors, even interior scenes. "Under these circumstances brilliant sunshine was the main factor to be wooed", recalled Olive Wilton. "It seemed impossible to acquire sufficient light without a constant battle against high wind, which made these interior scenes a nightmare, with hair and clothes blowing in all directions." 

However the fact it was a movie allowed the demonstration of scenes only discussed in the play, such as Nulla escaping the bushrangers cave.  Other sequences praised by reviewers included the abduction of the equatters daughter, the pursuit by Ben Hall and his gang, Ben Halls last stand, the dash through the cataract, the farm house rope bridge, the waterfall, the shearing match, and a champion stock whip artist. 

It was advertised as being the most expensive movie ever made in Australia to that date, but this is unlikely. 

==Reception==
Screenings were often accompanied by a lecture.

The movie was a popular success at the box office, breaking records in Sydney and Melbourne, and enjoying long runs throughout the country.  It achieved a cinema release in England, one of the first Australian films to do so. Bert Bailey and Ken G. Hall tried to track down a copy of the movie when Hall directed a version in 1933 but was unsuccessful. No known copies of it exist today, and it is considered a lost film. 

The Perth Sunday Times called the film a "motion picture atrocity." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 