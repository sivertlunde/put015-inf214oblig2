I Know That You Know That I Know
 
{{Infobox film
| name           = I Know That You Know That I Know (Io so che tu sai che io so)
| image          = Io so che tu sai che io so.jpg
| caption        = Film poster
| director       = Alberto Sordi
| producer       = 
| writer         = Augusto Caminito Rodolfo Sonego
| starring       = Alberto Sordi
| music          = Piero Piccioni
| cinematography = Sergio DOffizi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 118 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

I Know That You Know That I Know ( ) is a 1982 Italian comedy-drama film directed by and starring Alberto Sordi. It was entered into the 13th Moscow International Film Festival where it won a Special Prize.   

==Plot==
Fabio spends a quiet family life with his wife Lydia. But one day the man discovers that his wife could be a secret agent, and that she has not said anything him for not disrupting the coniugal live. So Fabio begins to investigate and discovers that his wife is involved in intrigues was the fault of a corrupt politician, and then the man discovers that even has a rare disease that is slowly killing him. When Fabio thinks that there is no more hope, suddenly wakes up and discovers a secret that dream reality had not revealed.

==Cast==
* Alberto Sordi as Fabio Bonetti
* Monica Vitti as Livia Bonetti
* Isabella De Bernardi as Veronica Bonetti
* Salvatore Jacono as Cavalli
* Giuseppe Mannajuolo as The Detective
* Ivana Monti as Valeria
* Micaela Pignatelli as Elena Vitali
* Claudio Gora as Ronconi
* Pier Francesco Aiello as Marco
* Napoleone Scrugli as Mirko
* Cesare Cadeo as Reporter
* Sandro Paternostro as Himself
* Gianni Letta as Himself

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 
 