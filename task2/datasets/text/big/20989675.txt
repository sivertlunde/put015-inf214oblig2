Deliciosa Sinvergüenza
{{Infobox Film
| name           = Deliciosa sinvergüenza 
| image          = Deliciosasin.jpg
| caption        = Poster of Deliciosa sinvergüenza
| director       = René Cardona Jr.
| producer       = 
| writer         = René Cardona Jr.
| narrator       =  Lucero Pedro Romo Paco Ibáñez
| music          = Ricardo Carreón
| cinematography = 
| editing        = 
| distributor    = Buenavista 1990
| runtime        = 90 min
| country        = Mexico Spanish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Mexican motion picture released in 1990.

== Synopsis  ==

The nice wit and the Luceritos skill to transform and to change constantly of personality, confuse three lunatics and crazy agents of the international service that pursue with great earnestness to the dangerous and mischievous delinquent, causing amusing situations  .

== Cast == Lucero as Lucerito
* Pedro Romo as Agent 42
* Pablo Ferrel as Agent 41
* Paco Ibáñez as Agent 43
* Norma Lazareno as Mother superior
* Elvira de la Fuente as Sister Luna

== Promotion ==
This movie was promoted only in Mexico. After its release was promoted through video stores and eventually television.

== Soundtrack==
Almost all the song from the album Cuéntame were part of the movie. .

== External links ==
*  

 
 
 
 
 


 
 