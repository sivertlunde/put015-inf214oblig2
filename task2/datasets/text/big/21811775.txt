Snowmen (film)
{{Infobox film
| name           = Snowmen
| image          = Snowmen.jpg
| director       = Robert Kirbyson
| writer         = Robert Kirbyson
| producer       = Stephen McEveety
| starring       = Bobby Coleman Josh Flitter Ray Liotta Beverley Mitchell Bobbe J. Thompson Christopher Lloyd
| studio         = Mpower Pictures
| distributor    = Arc Entertainment, Heritage Films
| released       =  
| country        = United States, Australia
| language       = English
}}
Snowmen is an independent family film written and directed by  Robert Kirbyson and produced by Stephen McEveety Braveheart . It was released at the Tribeca Film Festival on May 1, 2010. 

== Plot ==
Billy Kirkland (Bobby Coleman), is a 10-year-old boy with cancer who is convinced that he has only weeks to live. He has two misfit best friends, Lucas Lamb (Christian Martyn) and Howard Garvey (Bobbe J. Thompson) who he attends school with. Wanting to remembered, Billy and hjs friends try to pursue fame but face a series of schemes which stand in their way, including their neighbourhood bullies.

Billy and his friends decide to try and create the most snowmen built in the world in one day. Their first attempt fails miserably because they dont have enough people. They begin a campaign to get more people to help by hanging out flyers and placing posters up around their school, in which Billy gives a speech in the cafeteria, which brings attention. They eventually appeal to the Mayor too, which brings their Principal on board, and go to the news channel to get it broadcast. Both the Mayor and the new channel agree to meet them on the day on the event.

The first part of the day goes well, and they build a lot of snowmen, but they begin to melt by the time the Mayor gets there with the news channel. Billy gets him to agree to delay the final count to give them more time to solve the problem, with Billy giving a small broadcast. But when they get back, their bully Jason Bound had knocked over loads of the snowmen, and corner Billy and his friends. The rest of the school the defends Billy in a snow fight. The day ends when Jason knocks Lucas over and hurts him. Billys father, Reggie Kirkland arrives, having seen Billy on television, and tells the Mayor and everyone Billy is not dying like he thought he was. The event is cancelled, upsetting Billy and his friends.

When their spirits get uplifted by the caretaker, the friends talk in a nearby park. Jason soon finds them and begins to push them around. When they push back, the chunk of ice Billy was standing on breaks, and falls in the water. The chunk of ice then fills the gap again, trapping Billy under the ice. Howard goes to get help, while Lucas tries to free Billy. Realising he went too far, Jason helps. Lucas manages to break the ice, but falls in when he tries to get Billy out. Both boys fall unconscious while Jason tries to grab Lucas using a hockey stick. He eventually rescues both boys and tries to carry them to get help. Howard then comes back with his father and Billys father, who take them to hospital, where they are both saved. While they recover in hospital, they see on the news that efforts to build the snowmen have been renewed, and students from other schools have come to help. The boys later join them and break the record while discovering that the only lasting legacy is friendship.

== Cast ==
*Bobby Coleman: Billy Kirkfield
*Bobbe J. Thompson: Howard Garvey
*Christian Martyn: Lucas Lamb
*Josh Flitter: Jason Bound
*Ray Liotta: Reggie Kirkfield
*Doug E. Doug: Leonard Garvey
*Christopher Lloyd: The Caretaker 
*Beverley Mitchell: Mrs. Sherbrook

==Reception==

The film received mainly positive to mixed reviews. The Hollywood Reporter thought the film "might prove a bit too offbeat for mainstream theatrical success, though it might well prove popular on DVD and cable."    Christianity Today thought the film was excellent, saying “SNOWMEN is funny, engaging, thought-provoking, meaningful, and one of the best family movies I’ve seen in a long time.”

==Accolades==
{| class="wikitable" style="font-size:90%;" ;"
|- style="text-align:center;"
! style="background:#cce;"| Award
! style="background:#cce;"| Category
! style="background:#cce;"| Outcome
|-
|Tribeca Film Festival
| Audience Award, Runner-up 
|  
|-
| Toronto International Film Festival for Children and Youth
| Best Feature Film
|  
|-
| Heartland Film Festival
| Heartland Truly Moving Picture Award 
|  
|-
| Dallas International Film Festival
| Audience Award 
|  
|}

==References==
 
 
==External links==
*  
*  

 
 
 