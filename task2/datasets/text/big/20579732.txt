Chetna
{{Infobox film
| name           = Chetna
| image          = Chetna.jpg
| image_size     = 
| caption        = 
| director       = B.R. Ishara
| producer       = I.M. Kunnu
| writer         = B.R. Ishara
| narrator       = 
| starring       = Shatrughan Sinha Anil Dhawan Rehana Sultan
| music          = Sapan Jagmohan
| cinematography = 
| editing        = Anil Choudhary
| studio         = 
| distributor    = 
| released       = 1970
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1970 Bollywood typecast thereafter.  One of its songs, "Main To Har Mod Par Tujhko Doonga Sada," is singer Mukesh (singer)|Mukeshs all-time memorable and popular song.

==Plot Summary==
In this film, actor Anil Dhawan (elder brother of director David Dhawan) plays the role of a shy and reclusive young man by the name of Anil, who gets introduced to a beautiful prostitute named Seema (Rehana Sultana) through his friend, Ramesh (Shatrughan Sinha). Anil and Seema become good friends through their increasing patience, acceptance, and understanding of each others ways. They eventually fall in love.

One day, Anil proposes to Seema, and although Seema is happy about this, shes very reluctant in taking the decision to proceed with him because shes not sure if she can lead a normal life after marriage. Still, she accepts the proposal, and Anil offers her to stay with him for some time before she marries him. Seema becomes convinced that Anil truly loves her, and that she can actually lead a normal life with him. She starts to plan for her marriage, and, during that time, a matter comes up in which Anil has to go out of town for a few days.

When he returns, he returns to an unrecognizable Seema. Seema takes up drinking, smoking, and develops a very nonchalant and distant attitude towards him. He wonders what might have happened to her during his absence. Unknowing to him, Seema comes to know that shes pregnant and she doesnt know who the father is. Not wanting Anil to be ridiculed and cursed by society, she consumes poison.

Major comeback in Indian cinema and exotic scenes.

==Cast==
*Anil Dhawan ...  Anil  
*Shatrughan Sinha ...  Ramesh 
*Rehana Sultan ...  Seema 
*Nadira ...  Nirmala 
*Laxmi Chhaya    Asit Sen   
*Manmohan Krishna ...  Dr. Mehra 
*Johnny Whisky   
*Master Amir

==Music==
The musical score for the film was composed by Sapan-Jagmohan. The lyrics were written by Naqsh Lyallpuri.
{| class="wikitable"
|-
|#
!Song
!Singer
|- 1
|Main To Har Mod Par Mukesh (singer)|Mukesh
|- 2
|Main To Har Mod Par (sad) Mukesh (singer)|Mukesh
|- 3
|Jeevan Hai Ek Bhool Sajanwa Suman Kalyanpur
|-
|}

== References ==
 

==External links==
*  

 
 
 
 
 


 
 