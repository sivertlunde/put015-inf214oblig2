Miracle in the Rain
{{Infobox Film
| name           = Miracle in the Rain
| image          = Milo Anderson-Jane Wyman on Miracle in the Rain.jpg
| caption        = Costume designer   and Jane Wyman on the set of Miracle in the Rain - publicity still
| director       = Rudolph Maté 
| producer       = Frank P. Rosenberg
| screenplay = Ben Hecht
| based on =  
| starring       = Jane Wyman Van Johnson
| music          = Franz Waxman
| cinematography = Russell Metty
| editing        = Thomas Reilly
| art director   = Leo K. Kuter
| studio = Warner Bros.
| distributor    = Warner Bros.
| released       =   March 31, 1956
| runtime        = 108 min.
| country        =   English
}} home front during World War II-themed novella by American writer Ben Hecht, published in the April 3, 1943 issue of The Saturday Evening Post weekly magazine  then, within six months, issued in booklet form  and, thirteen years later, following four live television productions (in 1947, 1949, 1950 and 1953) which reduced the story to plot essentials, was adapted by him into a Warner Bros. feature film released on March 31, 1956. 

==Film version== 1956 screenplay is directed by Rudolph Maté and stars Jane Wyman as a lonely New York City office worker and Van Johnson as the happy-go-lucky soldier whom she meets during a downpour.   Character actress Eileen Heckart, who plays Jane Wymans office friend, and Arte Johnson, who achieved TV fame twelve years later on Laugh-In, are seen here in their debut performances on the big screen. The music is by Franz Waxman and the black-and-white cinematography is by Russell Metty.  A subplot about the heroines father, which was neither in the original story nor in any of the television adaptations, became one of the elements in the embellished screenplay.   

The film was produced on location,  with several sequences filmed in Central Park and St. Patricks Cathedral (Manhattan)|St. Patricks Cathedral.  Ten months before its release, while appearing as the "Mystery Guest" on the May 22, 1955 episode of CBS live primetime weekly game show, Whats My Line?, Van Johnson mentioned that he was in New York shooting scenes for his new film, Miracle in the Rain.

The film earned an estimated $1.4 million in North American rentals during 1956. 

==Plot==
In 1942, a few months after Americas entry into World War II, secretary Ruth Wood (Jane Wyman) lives quietly in New York City with her physically and emotionally fragile mother, Agnes (Josephine Hutchinson). Ruths co-workers at Excelsior Shoe Manufacturing Company are her best friend Grace Ullman (Eileen Heckart) and Millie Kranz (Peggie Castle), an attractive blonde involved in an affair with her married boss, Stephen Jalonik (Fred Clark). Also in the office is Monty (Arte Johnson), a young shipping clerk classified by the draft as 4-F, who monitors the wars campaigns on a world map pinned to the wall. One evening after work, when a cloudburst forces Ruth and other pedestrians to take shelter in the vestibule of an office building, Arthur Hugenon (Van Johnson), a cheerful, talkative G.I. stationed in the area, surprises the shy Ruth by starting a conversation. When he invites her to dinner, she declines, saying that her housebound mother is expecting her. Undeterred, Art buys food for three at a delicatessen and accompanies Ruth home. Agnes, who has distrusted men since her husband Harry left her for another woman ten years earlier, receives Art with little enthusiasm. During the meal, Art, who grew up on a Tennessee farm, captivates Ruth with his stories and afterward entertains them by playing the piano. Upon finding the manuscript of an unfinished song Harry composed, Art asks permission to take it back to camp, where he and his army buddy Dixie will write lyrics for it. On the weekend, Art takes Ruth and Grace to a matinee. On their way to a restaurant, they stop at an auction and Ruth impulsively bids on an antique Roman coin, which she gives to Art for good luck. At the Café Normandy, where they have dinner, Ruth is unaware that the piano player is her father (William Gargan), whom she has not seen since he left Agnes. However, Harry recognizes Ruth and confides to his bartender friend Andy that he has been too ashamed to return to his family.
 Alan King), Paul Smith). With only a brief moment remaining before the trucks departure for the port where the troop ship awaits, he asks Ruth to marry him when he returns and, to allay her fears, says he still has the lucky Roman coin.
 special delivery man knocks on the apartment door and hands a letter from a battlefield chaplain informing her that Art died in combat and that his dying wish was that she be told about his love for her. Ruths tear drops on the letter and, in the following days and weeks, she is inconsolable despite the best efforts of her friends and co-workers. Millie, moved by Ruths misfortune, feels the need for a fresh and pure start, drops Jalonik as her lover and leaves the firm. Grace finds Ruth consumed by grief, sitting on a bench in Central Park, and takes her to St. Patricks Cathedral, where Ruth lights candles under the statue of Saint Andrew. Jalonik, hoping Ruth will fill the void left by Millie in his extra-marital life, takes her to Café Normandy and attempts to engage in a warm-up conversation, but Ruth is in such a despairing state that she pays no attention as he kisses her on the cheek. In the meantime, as Harry listens to the radio, he hears the familiar strains of his music since, before shipping out, Dixie made some suggestions to Art as to the possibility of marketing Harrys music with Arts lyrics as a professional song and subsequently submitted it for airplay under his own name. Puzzled, Harry dials Agnes number but, at the sound of her voice, his resolve falters and he hangs up without speaking. Having written numerous letters of explanation and contrition to Agnes, he continually found himself tearing them to bits, because of inability to face the hurt he caused her.

Ruth has been returning to the statue of St. Andrew and talking to the cathedrals young priest (Paul Picerni). Losing interest in life, she ignores a cold, which turns into pneumonia. Mrs. Hammer, the upstairs neighbor who has often helped Ruth care for Agnes, now helps Agnes nurse the bedridden Ruth. One rainy night, while Agnes has dozed off near her bedside, the feverish Ruth leaves the apartment just before Harry finally musters the courage to walk into the apartment with intention of asking Agnes forgiveness for leaving. Stunned at seeing him, Agnes also realizes that Ruth is missing, just as Grace telephones. Upon being told that Ruth has left her sickbed, Grace realizes that she must be heading for the cathedral.

Consumed by fever on the cathedral steps, Ruth hears Arts voice calling to her. In her delirium, she sees Art come to her and tell her that love never dies. Since he no longer possesses earthly means of holding on to the Roman coin she gifted to him, Art returns it to Ruth. A moment later, in the midst of the heavy, late evening rain, the priest finds Ruth unconscious on the steps, just as Grace arrives. Seeing the coin clasped in Ruths hand, he shows it to Grace, who recognizes it and realizes that, for a brief moment, Art had returned to Ruth, whose own tenuous hold on life remains clouded in uncertainty at the end.

==Cast==
 
   
*Jane Wyman  
*Van Johnson  
*Peggie Castle  
*Fred Clark  
*Eileen Heckart  
*Josephine Hutchinson  
*William Gargan  
 
*Marcel Dalio  
*George Givot  
*Barbara Nichols    
*Halliwell Hobbes  
*Paul Picerni   Alan King  
*Irene Seidner  
*Arte Johnson  
 

===Unbilled speaking roles (in order of appearance)===
*Walter Kingson  
*Marian Holmes  
*Ray Walker  
*Minerva Urecal  
*Frank J. Scannell  
*Peter Mamakos  
*Jess Kirkpatrick  
*Norbert Schiller   Charles Meredith  
*Grandon Rhodes  
*Harry Harvey, Jr.   Paul Smith  
*Michael Vallon  
*Glen Vernon  
*Malcolm Atterbury  
*Roxanne Arlen  

==Song==
The melody by Ruths father, to which Art added words and turned into a song, is "Ill Always Believe in You", music by Ray Heindorf and M. K. Jerome, lyrics by Ned Washington.

==Evaluation in film guides==
Leonard Maltins Movie Guide (2011 edition) gave Miracle in the Rain 2½ stars (out of 4), describing it as an "above-par soaper of two lost souls" and Steven H. Scheuers Movies on TV (1986–87 edition) also granted 2½ stars (out of 4), calling it a "sentimental womens picture" and evaluating that "the two stars do well and theres a good supporting performance by Eileen Heckart, as Janes friend".

Assigning 2 stars (out of 5), The Motion Picture Guide (1987 edition) opined that "Ben Hecht was usually a lot more cynical than what he showed in his novel and screenplay for this hankie-grabber". Evaluating that the presentation has "a bit of comedy from comic King and Nichols, as a stripper, but its otherwise heavy going", the write-up decides that "too many secondary stories and not enough on-screen time between Wyman and a living, breathing Johnson, are what detract from the picture". 

Among British references, veteran critic and   in his 1984 Good Film and Video Guide, also settles on 2 stars (out of 4), positing that "Ben Hechts screenplay is of the genre spiritual, and may have been undertaken cynically, but the director has set it in a realistic, working-class New York then rarely seen in movies, so it acquires a certain patina. And it is very nicely played, by Jane Wyman… and Alan King, as a soldier enamoured of his new wife (Barbara Nichols), an untalented cabaret artist."

==Television productions==

===NBC Television Theater===
During the late 1940s and early 1950s, at the start of a period in American TV which was subsequently characterized as the   of Broadway Previews (later changed to NBC Television Theater). On Sunday, February 23, 1947, from 9:05 to 9:50, presentation records indicate a sponsored (by Borden (company)|Borden) production of Ben Hechts Miracle in the Rain, adapted by NBCs resident director and head of the networks drama division, Fred Coe. The records are missing details regarding members of the cast or whether Coe also directed the episode. 

===Chevrolet on Broadway (1949)=== Mary Anderson To Each Best Supporting Welsh coal The Corn Jesse White, but the identities of their characters were not specified. 

===Studio One (1950)===
The following year, Westinghouse Studio One, which started, in 1948, as CBS first regularly-scheduled weekly anthology drama series and, unlike Chevrolet Tele-Theater, had an hour-long time slot, presented its adaptation (by David Shaw) on May 1, 1950. Directed by Franklin J. Schaffner, whose helming of Patton (film)|Patton, twenty years later, would win him an Academy Award for Best Director, the live production starred Jeffrey Lynn. In the three years between his film debut in 1938 and the start of his World War II service in 1941, Lynn played leads and second leads in eighteen films and was touted by his studio, Warner Bros., as a potential top star of the future. However, upon returning to the screen after a seven-year absence, he found that his initial six films in the 1948–50 period, including the acclaimed A Letter to Three Wives, did not restore his career as leading man and he turned to television, making his small-screen debut with Miracle in the Rain. The role of Ruth was given to Joy Geffen, a New York stage actress with a number of undocumented appearances during TVs earliest days and at least six recorded credits for live dramas airing between 1949 and 1953. The storyline allowed for two additional characters, Ruths mother, portrayed by Catherine Squire, and Ruths office friend, whose given name, in this adaptation, was changed from Grace to Flora Ullman, played by Eleanor Wilson. 

===Tales of the City (1953)=== Robert Stevens, William Prince Hollywood Canteen, Cyrano de Bergerac, and then devoted his career primarily to television and theater. Likewise, Phyllis Thaxter, at the start of a long TV career, after playing a couple of minor leads and several second and third leads in seventeen films produced between 1944 and 1952, made her first small-screen appearance in this twenty-six-minute miniaturization of Miracle in the Rain, which also included Una Merkel as Ruths mother and Mildred Dunnock as Grace. 

==References==
 

==External links==
*  
*  
*   at the Notable Names Database
*  
*  
*   at TV Guide (1987 slightly revised write-up was originally published in The Motion Picture Guide)
*   at DVD Verdict
*   at Classic Film Guide
*   at Cinema Crazed 
*   at Ozus World Movie Reviews
*   at Fantastic Movie Musings and Ramblings
*   at The Science Fiction, Horror and Fantasy Film Review
*   at Reel Film Reviews
*   at DVD Beaver

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 