Father's Little Dividend
{{Infobox film
| name           = Fathers Little Dividend
| image          = Fathers Little Dividend.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Vincente Minnelli
| producer       = Pandro S. Berman
| writer         = Albert Hackett Frances Goodrich Edward Streeter (characters) 
| narrator       = Don Taylor Paul Harvey
| music          = Albert Sendrey
| cinematography = John Alton
| editing        = Ferris Webster
| studio         =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 82 min
| country        = United States
| language       = English
| budget         = $941,000  . 
| gross          = $4,622,000 
}}
 Father of the Bride (1950).
 public domain Warner Bros. Entertainment (via ownership of Turner Entertainment) owns the distribution rights to the original, they have not yet released an authorized DVD release of the sequel, which is available on DVD through several other DVD distributors.

==Plot summary== Father of the Bride, newly married Kay Dunstan announces that she and her husband are going to have a baby, leaving her father, Stanley Banks, having to come to grips with becoming a grandfather.

Middle-class family man Stanley Banks reminisces on events of the past year: One afternoon, returning from the office feeling happy and energetic, Stanleys routine is interrupted when his wife Ellie tells him that they are having dinner with their daughter Kay and her husband, Buckley Dunstan, to hear some important news. Although Stanley is certain that it concerns Buckleys business, the newlyweds reveal that Kay is expecting a baby. Buckleys parents, Doris and Herbert, are delighted, as is Ellie, but Stanley broods that he is too young and vibrant to be a grandfather. Soon Ellie, flush with excitement, throws Kay a baby shower, something Stanley thinks is highway robbery not punishable by law. Later, Ellie suggests that they remodel their house to enable Kay, Buckley and the baby to move in with them, but Stanley puts his foot down. Ellie is near tears when the wealthy Dunstons announce that they are planning to add a wing to their home for the couple, but is overjoyed when Kay and Buckley reveal that they have just bought their own little house, enabling Ellie to have free rein helping Kay decorate.

After settling into their new home, Kay, who is very close to her father, expresses her concern that the baby will make a difference in her relationship with Buckley. Stanley comforts her by telling her how much he loved her as a baby. Soon the Banks and the Dunstans are trying to outdo one another buying gifts and making plans for the baby, up to his enrollment in college. One night, while listening to Ellie, Doris and Herbert bicker over what the baby should be named, Kay breaks down and runs to her room. Only Stanley, whom Kay feels is the sole parent who understands her is able to comfort her. The day after pledging to Kay that he will make certain that none of the in-laws will interfere again, Stanley drags Ellie to Kays physician, Dr. Andrew Nordell, anxious over the "modern" ideas Nordell has about a more natural method of childbirth and infant care.

For the next month or so, things remain calm, until Stanley is awakened by a late night call from Buckley, who says that Kay has left him "for good." After sneaking over to Buckley and Kays place, Stanley learns from a cab driver that Kay took a taxi to the Bankss. The two men then return to the Banks house, where the couple make up after Kays jealousy is revealed to be a misunderstanding stemming from Buckleys late nights working at the office. Kay, as well as Stanley, realize Buckleys devotion to his family.

As the babys birth approaches, nerves among the parents and grandparents become frayed. The eventual birth of a baby boy delights everyone, except Stanley, a distant and wary observer of the as-yet-unnamed baby, who starts to cry whenever Stanley comes near him.

When the baby is six months old, Kay joins Buckley on a brief business trip and leaves the baby with her parents, hoping to give Stanley time to grow closer to his grandson. One afternoon, while Kay is still away, Stanley takes the baby for a walk in the park. When the baby finally falls asleep in his carriage, Stanley joins some friendly neighborhood boys in a game of football and loses track of time. After the game, when Stanley cannot find the carriage, he frantically retraces his steps back to the house. Seeing through the window that Kay has returned early, he panics and takes a taxi to the local police station. There a befuddled Stanley confesses to the grim-faced police sergeant that he has lost the baby. Fearing the wrath of the police squad, who found the sweet-natured baby and have fallen in love with him, Stanley secretly prays that his grandson will not start to scream when he picks him up. To Stanleys relief, the baby is delighted to see him, and from that moment on, the two are devoted to each other. Some time later, at the babys christening, Stanley beams with pride as his grandson is finally named "Stanley Banks Dunstan." 

==Cast==
* Spencer Tracy  as Stanley T. Banks
* Joan Bennett  as Ellie Banks
* Elizabeth Taylor  as Kay Dunstan Don Taylor  as Buckley Dunstan
* Billie Burke  as Mrs. Doris Dunstan
* Moroni Olsen  as Herbert Dunstan
* Richard Rober  as Police Sergeant
* Marietta Canty  as Delilah
* Russ Tamblyn  as Tommy Banks
* Tom Irish  as Ben Banks
* Hayden Rorke  as Dr. Andrew Nordell
* Paul Harvey  as Rev. Galsworthy

==Production==
 
Fathers Little Dividend was semi-remade in 1995 as Father of the Bride Part II, with Steve Martin and Diane Keaton in Tracy and Bennetts roles. However, Keatons character also has a baby and the plot has many similarities to this movie.

Spencer Tracy and Joan Bennett had made films together two decades earlier, including Me and My Gal (1932), in which their characters marry, and She Wanted a Millionaire (1932). 

According to film critic Leonard Maltin, this film was one of the first examples of a proper movie studio sequel. 

==Reception==
According to MGM records the film earned $3,122,000 in the US and Canada and $1.5 million elsewhere, resulting in a profit of $2,025,000. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*    at Google Videos

 

 
 
 
 
 
 
 
 