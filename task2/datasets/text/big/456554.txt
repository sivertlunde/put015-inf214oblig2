Dynamite Chicken
{{Infobox film
| name = Dynamite Chicken
| image = Dynamite chicken dvd cover.jpg
| caption = The cover for the film
| director = Ernest Pintoff
| producer = Ernest Pintoff
| writer = Ernest Pintoff
| starring = Richard Pryor
| music =
| distributor = Tango Entertainment
| released =  
| runtime = 76 minutes
| country = United States
| language = English
| budget = 
}}
Dynamite Chicken is American comedy film from 1971, starring Richard Pryor. 

“A contemporary probe and commentary of the mores and maladies of our age... With shtick, bits, pieces, girls, some hamburger, a little hair, a lady, some fellas, some religious stuff, and a lot of other things” boasts the films opening titles. An American film from 1972 involving Richard Pryor, and partly funded by and featuring John Lennon and Yoko Ono. It is a collection of subversive comedy sketches and routines relating to the peace movement. Many famous figures appear as themselves in the film, including Joan Baez, Lenny Bruce, Leonard Cohen, Allen Ginsberg, Jimi Hendrix, B.B. King, Malcolm X (from archival footage), Andy Warhol, Al Capp, Muddy Waters, Sha Na Na, Al Goldstein and Yoko herself. 

==References==
 

==External links==
*  

 
 
 
 
 
 


 