The Challenge (1970 film)
{{Infobox film
| name           = The Challenge
| image          =
| image_size     =
| caption        =
| director       = George McGowan (as Alan Smithee)
| producer       = Jay Cipes Ed Palmer
| writer         = Marc Norman
| narrator       = Mako
| music          =
| cinematography =
| editing        =
| studio         = 20th Century Fox Television ABC Television
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Challenge is a 1970 made-for-television movie starring Darren McGavin and Mako (actor)|Mako. Director George McGowan chose to hide his involvement by using the pseudonym Alan Smithee. This was the last film appearance of Paul Lukas.

==Plot==
An American space satellite lands in an uninhabited area of the Pacific. To resolve a dispute over its possession that threatens to escalate into all-out war, the United States and an unnamed Asian country agree to settle the matter with a duel to the death between two champions on a deserted tropical island. As Vietnam veteran Jacob Gallery Darren McGavin (armed with two Madison M-50 9mm submachine guns placed side by side) and Yuro (Mako (actor)|Mako) stalk each other, a bond of respect forms between them. 

However, both countries cheat, each covertly sending a second man. Outraged, Gallery and Yuro eliminate their backups and continue with their deadly contest. In the end, Gallery emerges victorious, but before he can signal the outcome, he succumbs to his wounds.

==Cast==
*Darren McGavin as Jacob Gallery
*Broderick Crawford as Gen. Lewis Meyers Mako as Yuro
*James Whitmore as Overman
*Skip Homeier as Lyman George
*Paul Lukas as Dr. Nagy
*Sam Elliott as Bryant, Gallerys unwanted help
*Adolph Caesar as Clarence Opano
*Andre Philippe as Swiss official
*Arianne Ulmer as Sarah (as Arianhe Ulmer)

==Other==
Interestingly, in the 17th episode of TVs The Time Tunnel (1966), Mako played a similar role, as a Japanese solder in World War II in a battle to the death on an island with the two regulars of that show. The American  when he "wins" was supposed to signal his win. Due to his disgust with the interference by both countries and his respect for his adversary, he destroys the transmitter before dying.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 