Rocket-bye Baby
{{Infobox Hollywood cartoon
| series = Merrie Melodies
| image =
| caption =
| director = Chuck Jones
| story_artist = Michael Maltese Harry Love (special animation effects)
| layout_artist = Ernie Nordli
| background_artist = Phillip DeGuard
| musician = Milt Franklyn (arrangement)
| voice_actor = Daws Butler (uncredited) June Foray (uncredited)
| producer = Edward Selzer (uncredited)
| studio = Warner Bros. Cartoons
| distributor = Warner Bros. Pictures
| release_date = August 4, 1956 (USA)
| color_process = Technicolor
| runtime = 7 minutes
| movie_language = English
}}
 short in Roswell Incident.

The cartoon is one of very few Warner Brothers short films of the era that did not use Mel Blancs voice talent. Instead, Daws Butler, famous for the voices of Yogi Bear, Huckleberry Hound and other characters in the Hanna-Barbera oeuvre, and June Foray, most famous as the voice of Rocky the Flying Squirrel, provided the vocal content of the film. No recurring characters were used.

==Story==
The movie begins with a vignette showing the planets Mars and Earth, during which the narrator (voiced by Butler) explains that, in the summer of 1954, the planets came so close to each other that "a cosmic force was disturbed" and a baby destined for Earth arrived at Mars, and vice versa. Two comet-like bodies are shown colliding and then assuming paths distinct from their original directions of travel. The transit of one, colored green, is followed as it flies through Earths atmosphere, above hundreds of homes with strange-looking TV antennas, then arrives at a high-rise hospital.

Joseph Wilbur (voiced by Butler) is waiting with other anxious, heavily smoking fathers in the hospital waiting room. Finally, an announcement (voiced by Foray) comes over the P.A. that Joseph can see his baby. Excited, he presses against the glass of the nursery window while his baby is rolled in on a small gurney. The baby becomes visible, but wait! His head is green! Then, he jumps up from the gurney and we see that his head has two antennae that spark and make Morse-code style beeps! Joseph faints.

The next scene opens in a suburban neighborhood. Joseph is arguing with his wife, Martha (voiced by Foray). He is pleading to her to let the baby stay in the house. She counters that the baby needs sunshine and fresh air. So, Joseph pushes the baby in a stroller, fearful of being seen. While he is not looking, the baby crawls up on the stroller hood and beeps at Joseph, startling him. Then, the baby crawls up on a wall and communicates with a bee sitting on a nearby flower. We next see Joseph back at the house, pleading once again, unsuccessfully, to keep the baby home. Once again, hes pushing the stroller along when an elderly woman (voiced by Foray) begins to dote on the baby, picking him up and noting that hes "such a healthy green baby, too." As she begins to realize somethings strange, the baby beeps his antennae at her and steals her glasses. Horrified, Joseph hurries the baby back to the house. The elderly lady, still unusually calm, pulls a tuning reed out of her pocket and uses it before letting out two bloodcurdling screams.

In the next scene, Martha is beginning to worry about the baby. He is doing the familys income taxes, spelling out Albert Einstein|Einsteins Mass–energy equivalence with letter blocks, and creating a Tinkertoy (named "Stinkertoy" in the cartoon) model of the (fictional) illudium molecule made famous in the Marvin the Martian cartoons. We are also shown a model of the solar system made from a basketball and Christmas ornaments hung from the ceiling with string, and a graph on a chalkboard titled "Hurricane Possibilities for Year 1985." There are also plans not only to build a better mousetrap, but corresponding blueprints on how to build a better mouse. Agreeing that "he should play more," Joseph sits the baby in front of the TV, where "Captain Schmideo" is displaying a toy flying saucer being offered as a promotion for Cosmic Crunchies even though the screen identifies it as "Ghastlies", the "new wonder cereal made from unborn sweet peas." The baby brings in a T-square and triangle, measures the dimensions of the saucer displayed on the TV screen, and retires to his room, where he builds "his own toy spaceship."

Next, the family receives a letter from Mars delivered by a small rocket. Martha expresses comic relief that it was "only" that and not a letter from mother until both spouses realize the difference and both yell "Mars ?!" in shock. The message, from "Sir U. Tan of Mars" (a reference to a popular vegetable laxative, "Serutan"), relates the event portrayed in the opening scene, adding that the Martian babys name is "Mot." Furthermore, Tan states that their baby is on Mars and they call him "Yob." The Earthlings are cautioned to guard the baby carefully until the exchange can be made. At that moment, using his highchair as a launch pad, Mot launches his "toy" spaceship out the bedroom window. The frightened Joseph first chases him by foot, then by car. Joseph reaches a high-rise hotel just as Mot is flying into a window on an upper floor. Inside the auditorium, a UFO skeptic (voiced by Butler) is deriding the concept of "little green men from Mars" and "flying saucers" until the little green baby in his flying saucer stops right in front of him, after which the skeptic bursts into tears.

Joseph arrives just as Mot is flying out another open window. He tries, unsuccessfully, to grab the spaceship, after which he falls out the window. Mot flies up to a waiting mother ship, which takes him in. The view then changes to Joseph yelling in demand as to where Yob is as he falls to his death and the street far below.

The scene fades and wavers to the P.A. in the hospital waiting room, where Joseph, this time alone in the waiting room, is called to see his baby. He had apparently fallen asleep while reading a science magazine with the lead story of "Can we communicate with Mars?". Excited, he goes to the window of the nursery, this time to see a healthy human boy rolled in. He whistles with relief. In a twist ending, the view then zooms in to the babys wrist, where a bracelet is worn with the letters, "YOB."

==Availability==
*Rocket-bye-Baby is available on  , Disc 4.

==External links==
*  

 

 
 
 
 
 
 