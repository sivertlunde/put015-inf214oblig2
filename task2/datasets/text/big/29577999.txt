Cool It (film)
{{Infobox film
| name           = Cool It
| image          = Cool_it_poster.jpg
| caption        = 
| director       = Ondi Timoner
| producer       = Terry Botwick Ondi Timoner
| writer         = Terry Botwick Sarah Gibson Bjørn Lomborg Ondi Timoner
| starring       = Bjørn Lomborg
| based_on       =  
| music          = Sarah Schachner
| cinematography = Nasar Abich Jr.
| editing        = Debra Light Brian Singbiel David Timoner Interloper Films 1019 Entertainment
| distributor    = Roadside Attractions
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =$62,713 (US)
}} Danish political scientist Bjørn Lomborg. The film stars Lomborg, best known for authoring The Skeptical Environmentalist. It premiered in September in Canada at the 2010 Toronto International Film Festival and had a theatrical United States release on November 12, 2010. The film was directed by Ondi Timoner.   

==Content==
The film focuses on Lomborg lecturing and brainstorming ideas to make the environmental movement less propagandistic and more realistic, which includes de-emphasizing efforts to stop global warming. Interviews include many scientists and activists both supporting and disputing Lomborg. It also explicitly challenges Al Gores Oscar-winning environmental awareness documentary, An Inconvenient Truth (2006), and was frequently presented by the media in that light, as in the Wall Street Journal headline, "Controversial ‘Cool It’ Documentary Takes on An Inconvenient Truth."  

==Critical reception & box office==
While many non-scientist critics welcomed the upbeat, positive nature of the film,    most scientific critiques noted some scientific inconsistency and glossing over of facts, a summary of which can be found on the Yale Climate Media forum.  Reviews were generally favorable, with a media critic collective rating of 51% from Rotten Tomatoes  and 61% from Metacritic.  The Atlantic review described it as "An urgent, intelligent, and entertaining account of the climate policy debate, with a strong focus on cost-effective solutions."  At the box office, Cool It s US release grossed $62,713  (An Inconvenient Truth grossed $24,146,161 in the US). 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 