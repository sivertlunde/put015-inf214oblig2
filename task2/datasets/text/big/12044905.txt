Sinners (2007 film)
 
{{Infobox Film
| name           = Sinners
| image          = Poster of the movie Sinners.jpg
| caption        = 
| writer         = Screenplay: Tory Christopher
| producer       = Tony Hannagan, Matt Frye 
| starring       = Ben Kurland Matthew Christopher Sean Hoagland
| director       = Tory Christopher
| movie_music    = 
| editing        = Matthew Fry
| released       = June 4, 2007
| runtime        = 85 min. English
| music          = 
| awards         = Best Film, Swansea Bay Film Festival
| budget         = $30,000 
}} 2007 film|motion picture, based on true story and written and directed by Tory Christopher. Eleven Eleven Pictures produced the feature. The film stars Ben Kurland, Matthew Christopher, and Sean Hoagland.

==Plot==
Brother Jim Jefferies leads the "Congregation of the Cross" in the small West Texas town of Britten. To be sure his children never falter, Brother Jim relentlessly reminds his congregation of the dire consequences, should one stray from the Lords word. Temptation, however, can be great, and when sin is in the heart, a young man finds himself damned for all eternity, thus leading himself, his best friend, and his friends brother, on a cross country journey to avenge Gods will. Arriving in Los Angeles, they meet a cunning con man. Together, the four young men find themselves in the middle of dangerous street games that finally lead them to perpetrate the ultimate sin; or had the sin already been committed?

==Awards==
Best Film, Swansea Bay Film Festival 2007

==External links==
* 

 
 
 
 
 


 