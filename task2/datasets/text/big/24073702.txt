Louis, the Child King
{{Infobox film
| name           = Louis, the Child King
| image          =
| image size     =
| caption        =
| director       = Roger Planchon
| producer       = Margaret Ménégoz
| writer         = Roger Planchon
| narrator       =
| starring       = Maxime Mansion
| music          =
| cinematography = Gérard Simon
| editing        = Isabelle Devinck
| distributor    =
| released       = 21 April 1993
| runtime        = 160 minutes
| country        = France
| language       = French
| budget         =
| gross          = $3,218,364 
| preceded by    =
| followed by    =
}}

Louis, the Child King ( ) is a 1993 French drama film directed by Roger Planchon. It was entered into the 1993 Cannes Film Festival.   

==Cast==
* Carmen Maura - Anne dAutriche Louis XIV
* Paolo Graziosi - Mazarin
* Jocelyn Quivrin - Philippe, Duc dAnjou
* Hervé Briaux - Gaston dOrléans
* Brigitte Catillon - Duchesse de Chevreuse
* Irina Dalle - Grande Mademoiselle
* Serge Dupire - Prince de Condé
* Isabelle Gélinas - Duchesse de Châtillon
* Michèle Laroque - Duchesse de Longueville
* Aurélien Recoing - Coadjuteur de lArchevêque de Paris, futur Cardinal de Retz
* Régis Royer - Prince de Conti
* Vanessa Wagner - Charlotte de Chevreuse
* Maurice Barrier - Guitaud
* Marco Bisson - Le duc de Nemours
* Carlo Brandt - Duc de la Rochefoucauld

==References==
 

==External links==
* 

 
 
 
 
 
 

 