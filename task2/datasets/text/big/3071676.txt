Donkey Skin (film)
{{Infobox film
| name           = Peau dÂne
| image          = Peau_ane.jpg
| caption        = original film poster
| director       = Jacques Demy
| writer         = Jacques Demy Charles Perrault  (story) 
| starring       = Catherine Deneuve Jean Marais Jacques Perrin Delphine Seyrig
| producer       = Mag Bodard
| music          = Michel Legrand
| cinematography = Ghislain Cloquet
| editing        = Anne-Marie Cotret
| distributor    =
| released       = 1970
| runtime        = 90 minutes
| country        = France
| language       = French
| gross          = $13,191,456 
}}
Peau dÂne (English: Donkey Skin) is a 1970 French musical film directed by Jacques Demy. It is also known by the English titles Once Upon a Time and The Magic Donkey. The film was adapted by Demy from Donkeyskin, a fairy tale by Charles Perrault about a king who wishes to marry his daughter. It stars Catherine Deneuve and Jean Marais, with music by Michel Legrand.  Donkey Skin also proved to be Demys biggest success in France with a total of 2,198,576 admissions. http://www.jpbox-office.com/fichfilm.php?id=8909 

Peau dÂne is distributed on DVD in North America by Koch-Lorber Films, a subsidiary of Koch Entertainment.

==Plot==
The King (Jean Marais) promises his dying Queen that after her death he will only marry a woman as beautiful and virtuous as she. Pressed by his advisers to remarry and produce an heir, he comes to the conclusion that the only way to fulfil his promise is to marry his own daughter, the Princess (Catherine Deneuve). Following the advice of her godmother, the Lilac Fairy (Delphine Seyrig), the Princess demands a series of seemingly impossible nuptial gifts, in the hope that her father will be forced to give up his plans of marriage. However, the King succeeds in providing her with dresses the colour of the weather, of the moon and of the sun, and finally with the skin of a magic donkey that excretes jewels, the source of his kingdoms wealth. Donning the donkey skin, the Princess flees her fathers kingdom to avoid the incestuous marriage.

In the guise of "Donkey Skin", the Princess finds employment as the pig-keeper in a neighbouring kingdom. The Prince of this kingdom (Jacques Perrin) spies her in her hut in the woods and falls in love with her. Love-struck, he retires to his sickbed, and asks that Donkey Skin be instructed to bake him a cake to restore him to health. In the cake he finds a ring that the Princess has placed there, and is thus sure that his love for her is reciprocated. He declares that he will marry the woman whom the ring fits.

All the women of marriageable age assemble at the Princes castle and try the ring on one by one, in order of social status. Last of all is the lowly "Donkey Skin", who is revealed to be the Princess when the ring fits her finger. At the wedding of the Prince and the Princess, the Lilac Fairy and the King arrive by helicopter and declare that they too are to be married.

==Production==
Jacques Demy, fascinated by Charles Perraults fairy tale since childhood, was working on a script for the film as early as 1962. The involvement of Catherine Deneuve was instrumental in securing financing for the production. {{Cite journal
  | last = Hill
  | first = Rodney
  | title = Donkey Skin (Peau dâne)
  | journal = Film Quarterly
  | volume = 59
  | issue = 2
  | pages = 40–44
  | publisher = University of California Press
  | issn = 0015-1386
  | date = Winter 2005–2006
  | jstor = 3697283
  | doi=10.1525/fq.2005.59.2.40
}}
  
Numerous elements in the film refer to   and reverse motion.  

==Cast==
* Catherine Deneuve: La première reine (First Queen), la princesse (The Princess) "Peau dâne"
* Jean Marais: Le premier roi (The First King)
* Jacques Perrin: Le prince charmant (The Prince)
* Micheline Presle: La reine rouge (The Red Queen), la seconde reine (Second Queen)
* Delphine Seyrig: La fée des lilas (The Lilac Fairy)
* Fernand Ledoux: Le roi rouge (The Red King), le second roi (The Second King)
* Henri Crémieux: Le chef des médecins (The Doctor)
* Sacha Pitoëff: Le premier ministre (The Prime Minister)
* Pierre Repp: Thibaud
* Jean Servais: Récitant (Narrator voice)
* Georges Adet: Le savant (The Scholar)
* Annick Berger: Nicolette
* Romain Bouteille: Le charlatan (The Charlatan)
* Louise Chevalier: La vieille (The Old Woman)
* Sylvain Corthay: Godefroy
* Jacques Demy and Michel Legrand: voices

==External links==
* 
*  

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 