The Prisoner of Shark Island
 
{{Infobox film
| name           = The Prisoner of Shark Island
| image          = Prisonerofsharkisland.jpg
| image_size     =
| caption        = film poster by Joseph A. Maturo
| director       = John Ford
| producer       = Nunnally Johnson Darryl F. Zanuck
| writer         = Nunnally Johnson Frank McGlynn Francis McDonald
| music          = R.H. Bassett Hugo Friedhofer
| cinematography = Bert Glennon Jack Murray
| distributor    = Twentieth Century Fox
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
}}
 1936 film loosely based on the life of Samuel Mudd, produced by Darryl F. Zanuck, directed by John Ford, and starring Warner Baxter and Gloria Stuart.

==Plot== President Abraham Lincoln (Frank McGlynn Sr.), Samuel Mudd|Dr. Samuel Mudd (Baxter) gives treatment to a man with a broken leg who shows up at his door. Mudd does not know that the president has been assassinated and the man who he is treating is John Wilkes Booth (Francis McDonald). Mudd is arrested for being an accessory in the assassination and is sent to prison on the Dry Tortugas, described as in the West Indies and referred to in the film as "Americas own Devils Island".

After a period of ill treatment due to his notoriety, his skills as a doctor are requested by the Commandant of the prison. The island has been in the grip of a yellow fever epidemic and the official prison doctor has fallen ill. Dr. Mudd takes charge with the blessing of the Commandant and the cooperation of the soldier guards, and the yellow jack epidemic subsides.

In the end he receives a pardon and is allowed to return home.

==Historical accuracy==
 
The film portrays Dr. Mudd as an entirely innocent victim and scapegoat, while the actual historical details of the case and his ties to Booth cast a shadow on his true innocence.

The historically accurate parts of the movie are (1) that President Abraham Lincoln was assassinated, (2) by John Wilkes Booth who sought help for a broken leg from Dr. Samuel Mudd, (3) who was subsequently tried before a military commission, found guilty of aiding Booth, and sent to an island military prison, and (4) was a hero in treating those felled in a yellow fever epidemic.
 Fort Jefferson in the Dry Tortugas islands, Florida. None of the characters refer to the prison by its historical name, although Mudd does receive a letter correctly addressed to Fort Jefferson. One of Dr. Mudds slaves did not follow him to prison, and try to help him escape from there. Dr. Mudds wife and her father did not command a boat in an attempt to rescue Dr. Mudd from prison. Dr. Mudd did not engage in a running gun battle while trying to escape to his wifes fictional boat. He did try to escape a couple of months after arriving at Fort Jefferson by hiding aboard a visiting ship, but he was quickly discovered and returned to the fort. He was not placed in an underground pit as punishment for trying to escape. His punishment for trying to escape was 3 months in a large empty ground level gun room with four other prisoners. The men were allowed out of the gun room every day to work around the fort, etc.

==Cast==
* Warner Baxter - Dr. Samuel Alexander Mudd
* Gloria Stuart - Mrs. Peggy Mudd
* Claude Gillingwater - Col. Jeremiah Milford Dyer
* Arthur Byron - Mr. Erickson
* O. P. Heggie - Dr. MacIntyre Harry Carey - Commandant of Fort Jefferson Francis Ford - Cpl. OToole John McGuire - Lt. Lovett
* Francis McDonald - John Wilkes Booth Douglas Wood - Gen. Ewing
* John Carradine - Sgt. Rankin
* Joyce Kay - Martha Mudd
* Fred Kohler Jr. - Sgt. Cooper
* Ernest Whitman - Buck Milford
* Paul Fix - David Herold

==Adaptations==
*The film inspired a radio adaptation on the "Encore Radio Theater" in 1946. 
*The film also inspired the western film, Hellgate (1952 film)|Hellgate (1952). 
*A television adaptation The Ordeal of Dr. Mudd was released in 1980.

==References==
 

==Further reading==
*  A reconsideration of the film in the context of the 2012 film Lincoln (2012 film)|Lincoln.
*  A recent, positive review by the prolific online critic.

==External links==
*  
*  
*  
*   at Virtual History
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 