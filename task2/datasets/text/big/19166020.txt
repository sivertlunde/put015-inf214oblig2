The Moonraker
 
 
 
{{Infobox film
| name           = The Moonraker
| image          = Moonraker.jpg
| image_size     = 
| caption        = British cinema poster David MacDonald
| producer       = Hamilton G. Inglis
| writer         = Arthur Watkin (play) Robert Hall Wilfred Eades Alistair Bell George Baker Sylvia Syms Marius Goring
| music          = Laurie Johnson
| cinematography = Mutz Greenbaum Richard Best
| studio         = Associated British Picture Corporation 
| distributor    = Associated British-Pathé
| released       = 22 May 1958
| runtime        = 82 minutes
| country        = United Kingdom English
| budget         = 
| gross          = 
}} 1958 and David MacDonald George Baker, Sylvia Syms, Marius Goring, Gary Raymond, Peter Arne, John Le Mesurier and Patrick Troughton.
 royalist nobleman, the Earl of Dawlish, who leads a double life as a roundhead-baiting highwayman called "The Moonraker", who already has helped more than thirty royalists to escape to France.

==Synopsis== Charles Stuart Charles I. George Baker) prepares to smuggle him to safety in France, under the noses of Cromwells soldiers. According to the story, the hero is called "the Moonraker" after the smuggler term, Moonrakers, sometimes claimed to hide contraband in the village pond and to rake it out by moonlight.

==Cast== George Baker as the Moonraker, otherwise Anthony, Earl of Dawlish 
* Sylvia Syms as Ann Wyndham 
* Marius Goring as Colonel Beaumont 
* Peter Arne as Edmund Tyler 
* Clive Morton as Lord Harcourt  Charles Stuart 
* Richard Leech as Henry Strangeways 
* Iris Russell as Judith Strangeways 
* Michael Anderson Jr. as Martin Strangeways
* Paul Whitsun-Jones as Parfitt 
* John Le Mesurier as Oliver Cromwell 
* Patrick Troughton as Captain Wilcox 
* Julian Somers as Captain Foster 
* Sylvia Bidmead as Meg 
* Patrick Waddington as Lord Dorset
* Fanny Rowe as Lady Dorset
* Jennifer Browne as Henrietta Dorset 
* Richard Warner as Trooper  George Woodbridge as Captain Lowry
* Victor Brooks as Blacksmith

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 


 
 