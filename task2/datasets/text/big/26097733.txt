Robot Holocaust
{{Infobox Film
| name           = Robot Holocaust
| image          = Robot Holocaust 1986 Poster.jpg
| image_size     =
| caption        =
| director       = Tim Kincaid
| producer       = Tim Ubels
| writer         = Tim Kincaid
| narrator       =
| starring       = Norris Culf Nadine Hartstein Joel Von Ornsteiner
| cinematography =
| music          =
| editing        =
| distributor    =
| released       = April 1986 (Italy)
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Robot Holocaust is a 1986 film set in post-apocalyptic New York City.

==Plot summary==

Neo, a drifter from the atomic-blasted wastelands, and his klutzy robot sidekick arrive at a factory where slaves labor to fuel the Dark Ones Power Station. He meets a woman who convinces him to help rescue her father, a scientist who invented a device that can break the Dark Ones control over the factory slaves. Gathering a motley crew of allies on the way, Neo goes to the Power Station to confront the Dark Ones evil servants.

==Cast==
*Norris Culf  ...  Neo  
*Nadine Hartstein  ...  Deeja (as Nadine Hart)  
*J. Buzz Von Ornsteiner  ...  Klyton (as Joel Von Ornsteiner)  
*Jennifer Delora  ...  Nyla  
*Andrew Howarth  ...  Kai  
*Angelika Jager  ...  Valaria  
*   ...  Jorn  
*Rick Gianasi  ...  Torque  
*George Grey  ...  Bray (as George Gray)  
*Nicholas Reiner  ...  Haim  
*Michael Azzolina  ...  Roan  
*John Blaylock  ...  Korla  
*Michael Zezima  ...  Airslave Fighter  
*Edward R. Mallia  ...  Airslave Fighter (as Edward Mallia)  
*Amy Brentano  ...  Irradiated Female

==Release and Legacy==
The film was released theatrically in April 1986 in Italy. In the U.S., it was released directly to videocassette in January of the following year by Wizard Video. In 2001, MGM released an Amazon.com Exclusive VHS of the film. Despite being tied to Full Moon Features, the film has yet to see a DVD release. However, MGM released a widescreen version of the film on Hulu.com, fueling speculation that a DVD release is possible in the near future. On December 4, 2012, the Mystery Science Theater 3000 video releases#DVD releases|MST3K version of the film was released on DVD by Shout Factory.

The film was shown during the first season of Mystery Science Theater 3000, after fans (along with Joel and the bots) complained of never viewing any color films.

==External links==
*  

=== Mystery Science Theater 3000 ===
*  
*  

 
 
 
 
 
 
 
 


 