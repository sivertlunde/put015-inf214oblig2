Cone of Silence (film)
 
 
{{Infobox film
| name           = Cone of Silence
| image          = Cone-of-silence-movie-poster-1960.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Charles Frend
| producer       = Aubrey Baring David Beaty (novel)
| narrator       = Michael Craig Peter Cushing Bernard Lee
| music          = Gerard Schürmann Arthur Grant
| editing        = Max Benedict
| studio         = Aubrey Baring Productions Bryanston Films 
| distributor    = Universal–International Films 
| released       =  
| runtime        = 88 min. Black and white (UK)  76 min. Black and white (US)
| country        = United Kingdom English
| budget         =
}} Michael Craig, crash in David Beatys novel, Cone of Silence (1959), later renamed  Trouble in the Sky, the title of the film, as released in the United States.

==Plot== drag to such an extent that the aircraft cannot achieve flying speed.

Gort is reprimanded but is allowed to return to flying the Phoenix after a check flight under Captain Hugh Dallas (Michael Craig). Meanwhile, Gorts daughter Charlotte (Elizabeth Seal) refuses to believe he is at fault. Gorts flying skills are again called into question when an approach to Calcutta is apparently made dangerously low, causing the aircraft to hit a hedge just before the runway threshold. It is later discovered that there was no hedge at the threshold of the Calcutta runway, and that the piece of hedge wrapped round the undercarriage leg had actually come from Ranjibad, where the aircraft had been taken off by Captain Clive Judd (Peter Cushing) - this shows that Gort is not the only pilot to have problems taking off.
 unstick speed and keep the nosewheel on the ground until just before unstick speed is reached.

==Cast==
As appearing in screen credits (main roles identified):   IMDb. Retrieved: 5 December 2011. 
{| class="wikitable" width="45%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Michael Craig || Captain Hugh Dallas
|-
|  Peter Cushing || Captain Clive Judd
|-
|  Bernard Lee || Captain George Gort
|-
|  Elizabeth Seal || Charlotte Gort
|-
|  George Sanders || Sir Arnold Hobbes
|-
|  André Morell || Captain Edward Manningham
|- Gordon Jackson || Captain Bateson
|-
|  Charles Tingwell || Captain Braddock
|-
|  Noel Willman || Nigel Pickering
|-
|  Delphi Lawrence || Joyce Mitchell
|-
|  Marne Maitland || Mr. Robinson 
|- William Abney || First Officer 
|-
|  Jack Hedley || First Officer
|-
|  Simon Lack || Navigator
|}

 
==Production== David Beatys novel, Cone of Silence (1959), also known as Trouble in the Sky. Beaty was an ex-military and commercial pilot with BOAC who became an expert on human error in aviation incidents and accidents.   After beginning a writing career with his first novels revolving around aviation themes, Beaty went back to college to get his degree in psychology and became a civil servant in 1967. He wrote his first non-fiction work, The Human Factor in Aircraft Accidents in 1969, followed by other works, before he returned to the subject of his first non-fiction book in The Naked Pilot: The Human Factor in Aircraft Accidents (1991). The film Cone of Silence represented his concern that human factors were being ignored in the aviation industry. 
 Bristol Siddeley Engines Ltd. (BSEL). The majority of the film was shot on the sound stages at Shepperton Studios, Shepperton, Surrey, United Kingdom. Erickson, Glenn.   DVD Savant, 10 July 2010. Retrieved: 5 December 2011. Captain John C. Crewdson of Film Aviation Services was the technical coordinator for the production. 

 
===Representation of the "Phoenix" in the film=== Olympus turbojet Nenes mounted in the standard wing root location. The aircraft was painted in Atlas Aviation livery for its starring role as the "Phoenix" airliner, the only full-scale aircraft seen in the film. 

==Reception==
After its premiere in London, reviews of the Cone of Silence were generally positive. Gerard Schurmanns film score was notable "...film music which divorces it from the routine and the prosaic ... the scores are infused with a dynamism, an energy, which is not only compelling but impelling, the music always a cogent force on the soundtrack, driving all before it."  The authoritative Flight magazine concentrated on the aviation elements, stating, "Coming at a time when jet runway lengths, ground stall effects and unstick manual speeds are again under close review, this is a timely and exciting film; no pilot could see it without mentally following through every action of each take-off and landing sequence."   Flight, April 1960. Retrieved: 5 December 2011. 

Other reviews noted, "Somewhat talky with a lot of technical jargon thrown into the screenplay (based on actual events),  ... a fairly straightforward drama aided by a top notch cast of familiar Brit character actors." T.V. Guide, however, was not impressed. "This average drama has simplistic characterizations and poorly written dialogue."  

==References==
;Notes
 
;Bibliography
 
* Beaty, David. Cone of Silence. London: Pan Books, 1960.
* Jackson, A.J. Avro Aircraft since 1908. London: Putnam Aeronautical Books, 2000 (revised edition). ISBN 0-85177-797-X.
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 