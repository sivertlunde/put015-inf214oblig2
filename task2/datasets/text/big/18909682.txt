Neend hamarey khuwab tumharey (1971 film)
{{Infobox film
| name           = Neend hamarey khuwab tumharey
| image          =
| image_size     =
| caption        = Film poster of Neend hamarey khuwab tumharey
| director       = K. Khursheed
| producer       = K. Khursheed
| writer         =
| narrator       =
| starring       = Waheed Murad Deeba Nirala Aliya Talish
| music          = M. Ashraf
| cinematography =
| editing        =
| distributor    =
| released       = January 1, 1971
| runtime        = approx. 3 hours
| country        = Pakistan
| language       = Urdu
| budget         =
}}

Neend hamarey khuwab tumharey is a blockbuster Pakistani Urdu colour film, produced and directed by Khalid Khursheed. NHKT is a romantic and melodious film. It cast Waheed Murad, Deeba, Nirala, Aliya and Talish. 

==Release==
NHKT was released by K.K. Pictures on 1 January 1971 in Pakistani cinemas. In spite of worst political circumstances of the country, the film did well in the cinemas. It completed 15 weeks in main cinema and 51 weeks in other cinemas of Karachi and thus became a golden jubilee film. 

==Music==
The film has popular tracks Mera mehboob aagaya... and Naraaz na ho to arz karun... sung by Masood Rana. Music is composed by M. Ashraf and the lyricist is Kaleem Usmani. Playback singers are Masood Rana, Runa Laila, Mehdi Hassan and Mala (Pakistani singer)|Mala.

===Songography===
*Yeh dunya hai daulat walon ki... by Masood Rana
*Mera mehboob aagaya... by Masood Rana
*Naraaz na ho to arz karun... by Masood Rana Mala
*Haye mera jhumka... by Runa Laila
*Jo bazahir ajnabi hein... by Mehdi Hassan

==References==
 

 
 
 
 
 


 