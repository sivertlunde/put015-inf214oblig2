Jogayya
{{Infobox film
| name = Jogayya   ಜೋಗಯ್ಯ
| image = jogayya.jpg
| caption = Theatrical release poster
| writer = Prem
| starring = Shivarajkumar   Sumit Kaur Atwal Prem
| producer = Rakshita Prem
| released =   
| runtime =
| country = India
| language = Kannada
| music = V. Harikrishna
| awards =
| gross =  6 crores  (Music, Satellite and Distribution Rights)      5.5 crores  (first week screening)     
| budget =  22 crores  . Entertainment.oneindia.in (2011-08-18). Retrieved on 2012-05-15. 
}}
 Kannada language film starring actor Shivrajkumar and directed by Prem (Kiran)|Prem. This is the 100th film of Kannada actor Shivrajkumar  and the shooting began on his 49th birthday.  It was produced under the banner of Prem Pictures. Rakshita Prem, who is the wife of the director Prem, is the producer of this film. It is the sequel to the 2005 blockbuster film Jogi (film)|Jogi.  The film was released on 19 August 2011  in more than 245 theaters across Karnataka  to mixed reviews from critics.  

==Production== Vijay and 3D technology.  The film has been shot in Bangalore|Bengaluru, Mysore, Mumbai, Haridwar, Hrishikesh & other holy places of North India.

==Cast==
* Shivrajkumar as the protagonist Mahadeva alias Jogi
* Sumit Kaur Atwal as Vidya Gokhale
* Pooja Gandhi guest-starring as an advocate
* Ravishankar (of Payana fame)

==Release== Black tickets being sold at exorbitant rates of up to  3,000 per ticket was reported.   The movie has started seeing empty halls since the eighth week and has subsequently been replaced.Though the producers made money, the real losers were the theater owners and the distributors.

==Soundtrack==
The films music album was released on 19 May 2011.  Jogayya has eight songs (including a theme) composed by V. Harikrishna and Ashwini Recording Company has bought the music rights of the film. More than 100,000 CDs & cassettes were sold within 5 days of music release, which is a record in Kannada Film Industry.  Over 60,000 records were sold on the first day of market launch.    
{| class="wikitable"
|-
! # !! Title !! Singer(s)
|-
| 1 || Kuri Kolina || Kailash Kher, Shreya Ghoshal, Vijay Prakash
|-
| 2 || Gangeye Avana || Shankar Mahadevan
|-
| 3 || Thagalaakkonde || Upendra, Priya Himesh
|-
| 4 || Jogaiah Baa || Shreya Ghoshal
|-
| 5 || Yaru Kaanadooru || Sonu Nigam, Shreya Ghoshal
|-
| 6 || Hetthavalalla Avalu || Sonu Nigam
|-
| 7 || Odole || Prem (Kiran)|Prem, Shreya Ghoshal
|-
| 8 || Mother Theme || Instrumental
|}

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 
 