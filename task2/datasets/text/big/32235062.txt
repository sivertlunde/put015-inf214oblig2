Walang Sugat
 
  Filipino playwright Spanish Occupation three centuries of Spanish rule. 

==Description==
Set in the Philippine Revolution of 1896, Walang Sugat was first published in 1898, and first performed in 1902 at the Teatro Libertad.    The play is about the injustices Filipinos suffered under Spanish colonial rule,  including the oppression of Filipino prisoners by Spanish friars   for expressing their patriotism. 

Although Walang Sugat is one of the major and more popular zarzuelas in the Philippines,  it was one of the plays considered "too subversive" by the American colonial authorities, and its author Reyes was imprisoned.   A typical and traditional Filipino sarsuwela  or sarswela,  it portrays the dilemmas of domestic life through dialogue, song, and dance. In addition, the sarsuwela is laden with romance, humour, and conflict.  Reyes, also known as the "Father of the Tagalog Zarzuela" and under the nom de plume "Lola Basyang", wrote Walang Sugat as his "statement against American Imperialism|imperialism." 

=== Plot ===
Apart from the political themes, Walang Sugat is also a love story.    Towards the end of the Philippine Revolution, Tenyong leaves Julia to become a member of the Katipunan. In his absence, Julia is continuously pressured by her mother to marry the rich Miguel; she succumbs when she stopped receiving news from Tenyong. As Julia and Miguel are being wed, Tenyong arrives to interrupt the service, and is dying of injuries sustained in combat. Tenyong mentions his dying wish to Julia, but the play  features an "unexpected twist" that shows how Tenyong is able to outwit the persons separating him from his beloved Julia. 

==Modern productions==

===2009===
Walang Sugat was staged by the Barasoain Kalinangan Foundation Inc. (BKFI) – a theater group that received a GAWAD CCP para sa Sining (CCP Arts Award) from the Cultural Center of the Philippines – from 11–13 February 2009 at the University of the Philippines University Theater. It was presented by BKFI during the UP Sarsuwela Festival 2009 nationwide celebration.   

===2010=== National Artist and professor, Salvador Bernal.   

==Film adaptation==
Walang Sugat had been adapted into film twice, first in 1939 then in 1957.  

The 1939 film version was produced by Filippine Productions, and was directed by Enrique Herrera-Dávila, and starred Filipino actors Rosa del Rosario and Leopoldo Salcedo.   

The 1957 version was produced by LVN Pictures, under the direction of Lamberto V. Avellana. Among the Filipino actors who participated in the 1957 film adaptation included Rosa Aguirre, Miguel Anzures, Tony Dantes, Joseph de Cordova, Oscar Keesee, Mario Montenegro, Charito Solis, and José Vergara (actor)|José Vergara.   

==See also==
*Paglipas ng Dilim

==References==
 

 
 
 
 
 
 
 
 