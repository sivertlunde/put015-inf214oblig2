Revenge Quest
{{Multiple issues|
 
 
 
}}

{{Infobox film| name = Revenge Quest
 | image = Revenge Quest Cover Art.gif
 | caption = original cover art
 | director = Alan DeHerrera
 | producer = Jerry Phiffer
 | writer = Alan DeHerrera
 | starring = Brian Gluhak Christopher Michael Egger Jennifer Aguilar
 | music = Joseph Andolino
 | cinematography = Alan DeHerrera
 | editing = Alan DeHerrera
 | distributor = Vista Street Entertainment
 | released = 1995
 | runtime = 72 minutes
 | language = English
 | budget =
 | gross =
 }}
Revenge Quest is a 1995 direct to video action/science fiction thriller directed by Alan DeHerrera with an ensemble cast featuring Brian Gluhak, Christopher Michael Egger and Jennifer Aguilar. It takes place in 2031 in Los Angeles following the escape of a dangerous inmate from the fictitious Red Rock Prison on Mars. The film was released by Vista Street Entertainment. 

== Plot ==
Prison fugitive Trent McKormick exits an Amtrak train. He is searching for Julie Meyers, who testified against him. Elsewhere, Rick Castle is in a smoky bar flirting with the local girls in an murmured conversation. Meanwhile, a police officer enters Julie Meyers apartment without any explanation. Trent McKormick comes out of the shadows and kills the officer before leaving. Rick Castle is summoned by his boss and learns about Trents escape. He is assigned to protect Julie Meyers and bring in Trent alive.

McKormick, walks into Julies office and stabs her sexually-harassing boss in the throat with his a large knife. He tries to hunt Julie down at the office but cant find her. Rick finds the dead cop in Julies apartment and races to her office complex. He is attacked by a recently released ex-con. Rick is apparently responsible for the attackers prison time. Rick kills him and races to the Julies office.

In a stairwell shoot out, Rick protects Julie from Trent. He takes her to his apartment, where she takes a bath and has a flashback to the day that Trent vowed revenge. Meanwhile, Trent is in a rage and kills a random pedestrian. Rick takes to the streets in his Dodge Viper to search for McKormick. He meets an old friend from the police force at a cafe. After leaving, Trent brutally murders Ricks friend. Rick and Trent fight at an old junkyard during a tsunami. Trent taunts Rick during the fight and misses several opportunities to kill him. Julie emerges from the shadows and  shoots Trent. She and Rick embrace.

==Cast==

* Brian Gluhak as Rick Castle
* Christopher Michael Egger as Trent McKormick 
* Jennifer Aguilar as Julie Meyers

==Reception==
 
Revenge Quest currently holds a 1.8/10 rating on IMDB based on 18 votes. There are currently no ratings or reviews on Rotten Tomatoes, although there is an entry.

==DVD release==
Revenge Quest has not been officially released on DVD, yet it is available on DVD format in at least two action movie compilations on Amazon.com including the "Kill or Be Killed 4 Movie Pack" and the "Action Arsenal 10 Movie Pack".  

==Other formats==
Revenge Quest was released on VHS in 1995. 

==Notes==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 