Charlie's Farm
{{Infobox film
| name           = Charlies Farm
| image          = Charlies_farm_poster.jpg
| alt            = 
| caption        = 
| director       = Chris Sun
| producer       = Christopher Sun Laurence Duggan Debbie Rivers Sean Gannon Whitney Duggan
| written by     = Chris Sun
| screenplay     = Chris Sun Nathan Jones Allira Jaques Bill Moseley Kane Hodder Dean Kirkright Sam Coward
| studio         = Slaughter FX
| released       =  
| runtime   = 88 min
| country        = Australia
| language       = English
| budget         =  3,000,000
| gross          = 
}}
 Nathan Jones, Allira Jaques, Bill Moseley, Kane Hodder, Dean Kirkright and Sam Coward.

== Plot ==
In an effort to do something different, four friends head into Australia’s outback to explore Charlie’s Farm, the site where a violent family met their end at the hands of an angry mob. Despite all warnings, they persist in their horror-seeking adventure.
Once there, the four friends meet other transients and find Charlie’s Farm to be anything but horror inspiring. They are unaware that they are being watched.
While exploring the property to prove the myth, they encounter Charlie, who shows them that some legends never die.

== Cast ==
* Tara Reid as Natasha
* Nathan Jones (wrestler)| Nathan Jones as Charlie Wilson
* Allira Jaques as Melanie
* Bill Moseley as John Wilson
* Kane Hodder as Tony Stewart
* Dean Kirkright as Jason
* Sam Coward as Mick

==Release==
The film was premiered in Australian theaters on December 4, 2014 and released in United States on March 1, 2015. Blu-Ray and DVD of Charlies Farm will be released on 17th June 2015 in Australia and U.K.

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 