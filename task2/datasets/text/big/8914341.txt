Taking Five
{{Infobox film
| name           = Taking Five
| image          = Taking Five (film poster).jpg
| alt            = 
| caption        = DVD cover
| director       = Andrew Waller
| producer       = Kat Hantas Chesley Seals
| writer         = Shauna Cross Joey Zehr Ben Romans
| music          = 
| cinematography = Gonzalo Amat
| editing        = Seth Flaum
| studio         = Teek Films
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Taking Five is a 2007 film about two high school girls who kidnap a pop rock band. It premiered at the Tribeca Film Festival on April 28, 2007. The film was filmed at Juan Diego Catholic High School in Draper, Utah and at Hillcrest High School in Midvale, Utah.

== Plot==
Devon Thompson (Alona Tal) and Gabby Davis (Daniella Monet) are the ultimate fans of the band 5 Leo Rise (The Click Five). When energy drink Shift sponsors a free 5 Leo Rise concert for the high school that collects the most bottle labels, Devon and Gabby deliver with a frenzy that only star-struck fans can. When the labels are destroyed in an accidental fire caused by Devon when she was holding a lit sparkler, dreams of the concert are dashed not only for Gabby and Devon, but also for their entire school. Desperate to see their idols on their home turf and not to be social outcasts for the remainder of high school, the two friends take matters of delivering the band into their own hands. They decide to kidnap the band with the help of Lincoln (Marcus T. Paulk) and Pete (Jake Koeppl). They manage to kidnap four members of 5 Leo Rise, Ritchie, K.K, Scooter, and Mason and they leave K.k behind because Petes car is too small to fit them all. Scooter and mason agree to play at their school. Ritchie (Eric Dill) refuses to do it, so they decide to throw eggs at him and get Devon to dress up as the Velvet Raven (Ritchie’s favorite comic book babe) to convince Ritchie to play. He finally confesses that he lip syncs because he has stage fright. Gabby comes up with an idea to help him sing in front of a crowd by getting them to dress up in really funny costumes. He does not succeed because of Lincolns actions and storms off to Devon’s basement. He starts playing the song on the guitar and sings as well, but he doesn’t know Devon is standing there. He plucks up the courage to sing in front of Devon and then kisses her when suddenly Lincoln and Gabby walk in to tell Devon that they are in big trouble and then Gabby storms off. Lincoln kidnaps Ritchie because he kissed Devon. Gabby decides to get her stuff and leave, but she gets even with Devon by kissing Mason. Devon makes Lincoln get Ritchie back and then they decide to play at the school. Gabby stands up to Kira (Kate Albrecht) and the whole school is impressed. The band and Devon turn up at the school and they play for them. Ritchie manages to sing in front of a crowd proudly. The girls are happy and don’t know what to do when the concert is all over. Devons sister, Danielle (Christy Carlson Romano), runs off with Scooter and the rest of the band.

== Cast ==
* Alona Tal as Devon Thompson
* Daniella Monet as Gabby Davis
* Christy Carlson Romano as Danielle
* Bart Johnson as Mark Thompson
* Marcus T. Paulk as Lincoln
* Eric Dill as Ritchie  Joey Zehr as Mason Ben Romans as Scooter Ethan Mentzer as K.K. Joe Guese as Nikolai
* Jimmy Chunga as Special Agent George Sledge
* Michael Buster as Hollace
* Kate Albrecht as Kira
* Nenetzi Scott as Lainey
* Jake Koeppl as Pete

== Production crew ==
Taking Five is a Teek Films presentation and is produced by Kat Hantas and Chesley Seals and executive produced by Hagai Shaham (Mean Creak), Simon Franks, and Zygi Kamasa.

== Music ==
Taking Five features new songs from  , "No Such Thing" and "Friday Night" by Christy Carlson Romano.

== References ==
 

== External links ==
* 
* 

 
 
 
 
 
 