Three Men and a Girl
{{infobox_film
| title          = Three Men and a Girl
| image          = Three Men and a Girl (1919) - Ad 1.jpg
| image_size     =
| caption        = Ad for film
| director       = Marshall Neilan
| producer       = Adolph Zukor Jesse Lasky
| writer         = Edward Childs Carpenter (play:The Three Bears) Eve Unsell (scenario)
| starring       = Marguerite Clark Richard Barthelmess Percy Marmont Jerome Patrick
| music          =
| cinematography = Henry Cronjager
| editing        =
| distributor    = Paramount Pictures
| released       = March 30, 1919
| runtime        = 50 minutes
| country        = United States Silent (English intertitles)

}} lost  1919 American romantic comedy film directed by Marshall Neilan and starring Marguerite Clark. It was produced by Famous Players-Lasky and distributed by Paramount Pictures. The film is based on an off-Broadway play The Three Bears by Edward Childs Carpenter.   

==Plot==
As described in a film magazine,  Sylvia Weston (Clark) is a capricious young woman who says "I do NOT" when she leaves a rich groom at the alter. She runs away in her bridal gown to a bungalow she owns at Loon Lake, only to find it occupied by three men with grudges against women. They expel her and her old nurse to a nearby cabin and stake out a line over which the women are not to cross. One by one the three men come to love Sylvia. The two older men, thinking that she is unhappily married, propose to adopt her and provide her with some clothes other than her bridal gown and swimming suit, which is all she has at the cabin. The younger one, however, is wiser and wins her in the end.

==Cast==
*Marguerite Clark - Sylvia Weston
*Richard Barthelmess - Christopher Kent
*Percy Marmont - Dr. Henry Forsyth
*Jerome Patrick - Julius Vanneman
*Ida Darling - Theresa Jenkins Charles Craig - Dallas Hawkins
*Sidney DAlbrook - Guide
*Betty Bouton - Mrs. Julia Draper
*Maggie Fisher - Abbey (*billed Maggie H. Fisher)

==References==
 

==External links==
* 
* 
*Still taken during production, left to right:   (University of Washington Sayre collection)

 

 
 
 
 
 
 