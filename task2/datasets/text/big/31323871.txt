The Goodies (1959 film)
{{Infobox film
| name           =Musterknaben
| image          = 
| image size     =
| caption        =
| director       =Johannes Knittel
| producer       =Alexander Lösche
| writer         = Gerhard Bengsch
| narrator       =
| starring       =Hartmut Reck
| music          = Günter Hörig
| cinematography = Ernst W. Fiedler
| editing        =Wally Gurschke
| distributor    = PROGRESS-Film Verleih
| released       =27 November 1959
| runtime        =74 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} East German romantic comedy film, directed by Johannes Knittel. It was released in List of East German films|1959.

==Plot==
Bassi and Edwin are the two laziest, most irresponsible construction workers in their workers brigade. When they fall in love with their neighbors Thea and Susi, they pretend to be reliable young men and accompany their friends to a meeting of the local chapter house. To keep up appearances, the two are forced to undertake several duties which they would not have dreamed of doing otherwise. The plot turns into a chain of comical mistakes, but eventually the two new couples reunite and all ends well.

==Cast==
*Hartmut Reck as Bassi
*Rolf Herricht as Edwin
*Brigitte Krause as Thea
*Gudrun Wicherta s Susi
*Erwin Geschonneck as Susis father
*Peter Marx as Kaiser
*Fritz Diez as Weber
*Paul R. Henker as Scholz
*Hans Klering as Grimm
*Werner Lierck as Tetzlaff
*Gerd Biewer as Graf
*Sabine Thalbach as Röschen
*Jean Brahn as Fritz 
*Agnes Kraus as old teacher
*Lotte Loebinger as Klara 
*Werner Dissel as uncredited role

==Reception==
The West German Catholic Film Service regarded the film as one "with a weak plot line, the same old gags used over and over again."  The film critic of the journal Cinema wrote that "even in the 1950s, it was not witty." 

==References==
 

==External links==
*  
*  on DEFA Sternstunden.

 
 
 
 
 


 
 