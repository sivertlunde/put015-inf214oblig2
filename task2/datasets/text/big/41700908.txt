Soshur Bari Zindabad
 
{{Infobox film
| name           = Soshur Bari Zindabad
| image          = Shashurbari Zindabad.jpeg
| image_size     = 200px
| caption        =
| director       = Debashish Biswas
| producer       = Gayotry Biswas
| writer         = Dilip Biswas Riaz Shabnur Sonia Rahul Sajon Bulbul Ahmed Rina Khan Probir Mitra Doly Johur Misha Sawdagor ATM Shamsuzzaman
| music          = Emon Saha
| cinematography = Abul Khayer
| editing        = Aminurl Islam Mintu
| distributor    = Geti Kothachitra
| released       = 2002
| runtime        = 147 Minutes
| country        = Bangladesh Bengali
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
| imdb_id        = 2301157
}}

Soshur Bari Zindabad ( ) is a 2002 Bangladeshi film released on Eid al-Fitr|Eid-ul-Fitr.  The film marked the directorial debut of Debashish Biswas, son of Dilip Biswas. 

== Plot ==

 

== Cast == Riaz - Badhon
* Shabnur - Prema Chodhury 
* Sonia - Ria Chowdhury 
* Rahul - Sajal
* Sajon - Rafi
* Bulbul Ahmed - Arman Chowdhury 
* Rina Khan - Dilruba Chowdhury 
* Probir Mitra - Rajib Khondokar 
* Doli Johur - Rehana Akter
* Misha Sawdagor - Badhons friends 
* ATM Shamsuzzaman - Hekmi (Hekmat Ali Munshi)
* Afjal Shorif - Tota
* Amol Bose - Mr. Mojumdar

== Music ==
The music for the film was composed by Gazi Mazharul Anwar and directed by Emon Saha.

=== Song List ===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track!! Song !! Singer !! Notes !! Screen
|- 1
|Amar Bhalobashar Gari Chalu Holo Andrew Kishore
| Riaz (actor)|Riaz
|- 2
|Ami Chilam Eka Eka Tomar Sathe Holo Dekha Udit Narayan and 
| Riaz (actor)|Riaz and Shabnur
|- 3
|Tomar Amar Biyer Kotha Rakhbona Gopon Andrew Kishore] ও Samina Chowdhury Title song Riaz (actor)|Riaz and Shabnur
|-
|৪
|Soshur Bari Aisa Amar Asha Furaise
| Monir Khan 
| Mixed Music Riaz (actor)|Riaz
|-
|}

==References==
 

==External links==
*   - এ
 

 
 
 
 
 
 
 
 