Aakkramanam
{{Infobox film
| name           = Aakkramanam
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Sreekumaran Thampi   
| producer       = Sreekumaran Thampi, 	Bhavani Rajeswari Production
| writer         = Sreekumaran Thampi
| narrator       =  Madhu  Jayan  Balan K. Nair  Sathaar  Sreevidya  Jayabharathi    Shyam
| cinematography = 
| editing        = K. Narayanan
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Aakkramanam  (1981) is an Indian film in the Malayalam language by Sreekumaran Thampi     starring Madhu (actor)|Madhu, Jayan, Balan K. Nair, Sathaar, Sreevidya, and Jayabharathi. 

==Cast== Madhu
* Jayan
* Balan K. Nair
* Sathaar
* Sreevidya
* Jayabharathi
* Prameela

==Soundtrack==

{| class="wikitable" border="1"
! Song || Playback || Lyrics || Duration
|-
| "Eid Mubarak"
| K.J. Yesudas
| Sreekumaran Thampi
|
|-
| "Muthukudayenthi"
| KJ Yesudas, S. Janaki
| Sreekumaran Thampi
|
|-
| "Odum Thira Onnam Thira" P Jayachandran, Jolly Abraham, Sherin Peters
| Sreekumaran Thampi
|
|-
| "Lilly Lilly My Darling"
| S.P. Balasubramaniam, SP Shailaja, Chorus
| Sreekumaran Thampi
|
|-
| "Peethambaradhaariyitha"
| S Janaki
| Sreekumaran Thampi
|
|}

==References==
 

==External links==
*  

 
 
 


 