Alik Sukh
{{Infobox film
| name           = Alik Sukh.      
| image          = File:Alik Sukh poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| film name      = 
| director       = Shiboprosad Mukherjee Nandita Roy
| producer       = Atanu Roy Chowdhury
| writer         = Suchitra Bhattacharya
| screenplay     = Nandita Roy
| story          = 
| based on       =     
| narrator       = 
| starring       = Debshankar Haldar Rituparna Sengupta Sohini Sengupta
| music          = Anindya Chatterjee Nachiketa Chakraborty
| cinematography = Shirsha Roy
| editing        = Moloy Laha
| studio         = 
| distributor    = 
| released       =  
| runtime        = 137 minutes
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali psychological psychological thriller drama film directed by the duo Shiboprosad Mukherjee and Nandita Roy, starring Debshankar Haldar, Rituparna Sengupta and Sohini Sengupta in the lead roles. It released on July 19, 2013.                                          

== Plot ==
A renowned Gynaecology|gynaecologist, Dr Kingshuk Guha (Debshankar Haldar), finds himself in a professional crisis when one of his patients, Kabita Mondol (Sohini Sengupta), dies on the operating table while he is away buying a luxury apartment for his family. Kabita’s enraged husband (Biswanath Basu) and relatives attempt to mob the hospital. On hearing the news, Kingshuk’s wife, Rumi (Rituparna Sengupta), rushes to the hospital to be with her husband.  She happens to see Kabitas corpse lying unattended in the empty operation theatre and begins to imagine that the dead woman is communicating with her.

Well-off, in a loving marriage, and pregnant with the couple’s second child, Rumi begins to question her own right to happiness when her husband has been responsible for depriving another family of its loved one. She becomes emotionally alienated from Kingshuk when he refuses to accept culpability for Kabita’s death. Kingshuk, in turn, grows frustrated at Rumis lack of support as he faces investigation by a medical committee and a compensation claim of Rs. 1,000,000 by Kabita’s relatives. Kingshuk and Rumis relationship sours to a point that they cease speaking to each other.

Eventually, Rumi leaves with her son to stay for a while at her parents’ house. Kingshuk manages to reach an out-of-court settlement for Rs. 300,000 with Kabita’s relatives. Relieved and wishing to reconcile, he calls Rumi, announcing that he has "bought the patient party". Rumi is devastated at Kingshuk’s callous attitude and the lack of justice for Kabita. Soon after receiving the call, she suffers a fall and has a miscarriage, which leads to severe abdominal haemorrhaging. The tables are turned and Kingshuk finds himself in the same position as Kabitas crazed husband as the medical team at the local hospital takes its time in attending to Rumi.

Rumi survives. A penitent Kingshuk seeks forgiveness for his actions and promises to make things better between them. From Rumi’s perspective, Kabita finally experiences closure. Hopes for a happier future emerge.

== Cast ==
* Debshankar Haldar as Kingshuk Guha
* Rituparna Sengupta as Rammani
* Sohini Sengupta as Kabita
* Biswanath Basu as Biswajit
* Sayani Ghosh as Namita
* Dolon Roy
* Sumit Samaddar
* Soumitra Chatterjee

==Crew==
* Directed by Nandita Roy & Shiboprosad Mukherjee
* Produced by Atanu Raychaudhuri & Windows
* Screenplay & Dialogues by Nandita Roy & Shiboprosad Mukherjee
* Director of Photography Sirsha Ray
* Edited by Malay Laha
* Music by Nachiketa Chakraborty & Anindya Chatterjee
* Background score by Joy Sarkar
* Production Designer Amit Chatterjee
* Sound by Anirban Sengupta & Dipankar Chaki
* Costume by Dibyakanti Sen
* Publicity design by Saumik & Piyali
* Executive Producers Pooja Bose & Lakshita Dubey

==Direction==
Nandita Roy, also the screen playwright of the film has been working in the film industry for the past 28 years. She has worked in best banners with leading directors and in top production houses. She has worked in many National Award winning films and television serials. She has directed as well as produced a number of television serials. She was the Creative Director and first Programming Head of ETV Bangla, one of the leading channels of West Bengal and was responsible for its establishment in Kolkata. She is the member of Indian Film Director’s Association, Mumbai and Indian Film Writer’s Association, Mumbai. She has also co-directed Bengali films like Ramdhanu, Icche, Muktodhara and Accident (2012 film)|Accident.
Shiboprosad Mukherjee has been working in the television media for the last 15 years. He was associated with ETV Bangla, a 24 hours satellite channel from its inception and was responsible for its programme designing. He has been involved in several television serials as a producer and as a programme designer. As an actor, he has worked with many leading directors of this film industry. He has also co-directed Bengali films like Ramdhanu, Icche, Muktodhara and Accident (2012 film)|Accident.      

== Influences ==
In an interview to The Times of India, director Shiboprosad Mukherjee said that although the film is based on a novel, it was influenced by his fathers death on April 15, 1994. Regarding his personal experience regarding the lives of doctors, he said,  

== Soundtrack ==
Soundtrack of Alik Sukh has been composed by Anindya Chatterjee and Nachiketa Chakraborty.   

== Response ==
Alik Sukh has heavily stirred the conscience of all the individuals, irrespective of the profession they belong to. It has been critically acclaimed and has also contributed a big deal in raising the standard  of the tollywood film industry. It ran in theatres successfully for 50 days. Later, on the 15th of September,2013, it made an all-India release.                                                      

==Accolades==
Alik Sukh was premiered at the Marché du Film section in Cannes Film Festival in 2013. Nandita Roy and Shiboprosad Mukherjee received the Filmfare Awards East for Best Direction for the film Alik Sukh this year. In the same Awards ceremony, Rituparna Sengupta received the award for Best Actor and Anindya Chatterjee for Best Lyrics for Alik Sukh.  
 
=== Track listing ===
{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Singer(s)
| total_length    = 
| writing_credits = no
| lyrics_credits  = no
| music_credits   = no
| title1          = MBBS
| extra1          = Anindya Chatterjee
| length1         = 
| title2          = Ke Jane Thikana
| extra2          = Nachiketa Chakraborty
| length2         = 
| title3          = Na Re Na Ore Mon
| extra3          = Nachiketa Chakraborty
| length3         = 
| title4          = Raat Jaye
| extra4          = Anindya Chatterjee
| length4         = 
| title5          = Bidae Theme
| extra5          = Instrumental
| length5         = 
}}

==See also==
* Ramdhanu
* Muktodhara
* Icche Accident

== References ==
 

==External links==
 