Trane Tracks: The Legacy of John Coltrane
{{Infobox film
| name        = Trane Tracks: The Legacy of John Coltrane
| image       =
| writer      =
| narrator    =
| starring    = McCoy Tyner, Elvin Jones, Ron Carter, Benny Bailey, Eddie Marshall, Bishop Franzo Wayne King from the African Orthodox Church Saint John Coltrane
| director    =
| producers   =
| distributor = EFOR Films
| released    =  
| runtime     = 80 minutes English
}}

Trane Tracks: The Legacy of John Coltrane is a documentary film about saxophonist John Coltrane. 


==Exclusive interviews include==
*McCoy Tyner 
*Elvin Jones
*Ron Carter
*Benny Bailey 
*Eddie Marshall
*Bishop Franzo Wayne King from the African Orthodox Church Saint John Coltrane

==Tracks==
#So What (with the Miles Davis Quintet)
#Afro Blue
#Alabama
#Every Time We Say Goodbye
#Naima
#Impressions - version 1
#Impressions - version 2 (featuring Eric Dolphy)
#My Favorite Things - version 1
#My Favorite Things - version 2 (featuring Eric Dolphy)

==Featuring the following musicians==
*McCoy Tyner 
*Reggie Workman
*Jimmy Garrison
*Elvin Jones
*Eric Dolphy
*Miles Davis
*Wynton Kelly
*Paul Chambers
*Jimmy Cobb

==Also featuring video clips by the following musicians==
*Miles Davis
*Johnny Hodges
*Charlie Parker and Dizzy Gillespie
*Lester Young and Coleman Hawkins
*Thelonious Monk The Art Ensemble of Chicago The Elvin Jones Quartet 

==References==
 

 

 
 
 
 