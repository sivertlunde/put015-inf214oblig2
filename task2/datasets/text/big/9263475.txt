Lassie (1994 film)
{{Infobox film
| name           = Lassie
| image          = Lassie 1994 movie poster.jpg
| image_size     =
| caption        = Lassie 1994 release movie poster
| director       = Daniel Petrie
| producer       = Lorne Michaels
| writer         = Eric Knight Matthew Jacobs Gary Ross Elizabeth Anderson
| narrator       =
| starring       = Tom Guiry Helen Slater Jon Tenney Brittany Boyd
| music          = Basil Poledouris
| cinematography = Kenneth MacMillan
| editing        = Steve Mirkovich
| distributor    = Paramount Pictures 
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $9,979,683
}}
 adventure family film directed by Daniel Petrie and featuring the fictional collie Lassie.

==Plot==
The Turner family moves from the big city (Baltimore, Maryland) to the rural countryside in Tazewell County, Virginia, hoping to start a new life. The move creates problems for everyone, especially 13-year old Matt (Tom Guiry), who feels lost and alone in his new surroundings. But with the help of a stray Collie dog named Lassie that the family takes in, Matt learns to adjust and the two form an unbreakable bond. However, as his fathers planned job falls through, Matt with help from his grandfather, helps convince the family to start up a sheep farm. While the Turners get to work, a ruthless neighbor and sheep farmer, Sam Garland, is determined to stop at nothing to stop them, because it means that they will be occupying some grazing land that hes used in the past. Eventually Sam, with the help of his own sons Josh and Jim, steals the Turners new sheep herd, and kidnaps Lassie. However, she manages to escape, and she and Matt manage to claim their sheep back. However they end up in a scuffle with Josh and Jim, and Josh ends up struggling in a raging river, heading for a huge waterfall. Matt manages to rescue him, but is unable to save himself. Lassie then rescues Matt, but ends up going over the waterfall herself, to everyones dismay. Sam, after learning that Matt had saved his Joshs life, apologizes for his actions and for Lassies demise. However, Lassie manages to survive the waterfall, and although weakened, she returns home not too long afterwards.

==Cast==
* Howard as Lassie
* Tom Guiry as Matthew Turner
* Helen Slater as Laura Turner
* Jon Tenney as Steve Turner
* Brittany Boyd as Jennifer Turner
* Frederic Forrest as Sam Garland
* Richard Farnsworth as Len Collins Michelle Williams as April Porter
* Joe Inscoe as Pete Jarman
* Yvonne Brisendine as Mrs. Jarman
* Clayton Barclay Jones as Josh Garland
* Charlie Hofheimer as Jim Garland
* Jody Smith Strickler as Mildred Garland
* Margaret Peery as Mrs. Parker
* David Bridgewater as Customer
* Earnest Poole, Jr. as Highway Patrolman #1
* Jeffrey H. Gray as Highway Patrolman #2
* Robert B. Brittain as Grommet Fireman
* Rick Warner as Timid Neighbor
* Kelly L. Edwards as Smoking Girl
* Jordan Young as Smoking Boy
* Katie Massa as College Student

==Filming locations==
Lassie was filmed in Tazewell County, Virginia. 

==Reception==
Lassie was released to positive reviews.    As of December 2013, the film holds a "fresh" rating of 87% on Rotten Tomatoes, based on 15 reviews.

===Box office===
The movie debuted at No.9 at US box office.  

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 

 