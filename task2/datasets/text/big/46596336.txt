Sending of Flowers
{{Infobox film
| name =  Sending of Flowers
| image =
| image_size =
| caption =
| director = Jean Stelli
| producer = Claude Dolbert 
| writer =  Charles Exbrayat
| narrator =
| starring = Tino Rossi   Micheline Francey   Jean Brochard   Albert Dinan
| music = 
| cinematography = Marc Fossard
| editing = Pierre Delannoy
| studio = Coco Cinema 
| distributor = CCFC
| released =    26 May 1950 
| runtime = 102 minutes
| country = France  French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama film directed by Jean Stelli and starring Tino Rossi, Micheline Francey and Jean Brochard.  The film portrays the life of the composer Paul Delmet.

==Cast==
* Tino Rossi as Paul Delmet  
* Micheline Francey as Suzanne  
* Jean Brochard as Hippolyte  
* Albert Dinan as Rodolphe Salis 
* Arlette Merry as Sophie  
* Milly Mathis as Mme Sammos  
* André Dumas as Albert Mourssac 
* André Marnay as Le curé  
* Charles Lemontier as Fragerolles, un membre du Chat noir 
* Jean Berton as Un membre du Chat noir  
* Yvonne Dany 
* Paul Demange as Un spectateur timoré  
* Monique Gérard 
* Mag-Avril as Une bourgeoise  
* René Marc as Un membre du Chat noir  
* Frédéric Mariotti as Le plâtrier  
* Georges Paulais as Un bourgeois  
* Hélène Pépée 
* Hélène Remy 
* Renée Thorel 
* Edmond Van Daële as Le bedeau  

== References ==
 

== Bibliography ==
* Rège, Philippe. Encyclopedia of French Film Directors, Volume 1. Scarecrow Press, 2009.

== External links ==
*  

 
 
 
 
 
 
 
 

 

 