Cool Cat (film)
 
{{Infobox Hollywood cartoon
| cartoon_name = Cool Cat Cool Cat)
| image = 
| caption = 
| director = Alex Lovy
| story_artist = Bob Kurtz Ed Solomon Volus Jones LaVerne Harding
| voice_actor = Larry Storch
| musician = William Lava
| producer = William L. Hendricks
| layout_artist = David Hanan
| background_artist = David Hanan
| distributor = Warner Bros.-Seven Arts
| release_date = October 14, 1967 (USA)
| color_process = Technicolor
| runtime = 6:29
| movie_language = English
}}
 1967 animated Cool Cat. Warner Bros. animation studio, as the first cartoon in a long time to introduce a major new character (virtually every Looney Tunes cartoon made in the previous three years had been ones featuring Daffy Duck versus Speedy Gonzales, or Wile E. Coyote and the Road Runner, the latter were gone in 1966), who would go on to be the studios most prolific character between then and the studios final shutdown in 1969.

It was also the first cartoon to feature the redesigned opening and closing tiles with the "W7" logo, and a new version of the opening theme.

==Synopsis==
 
 I tawt I taw a puddy tat! A tiger-type puddy tat!" and pursues him while hidden inside Ella. He eventually blasts Cool Cat with a shotgun hidden in Ellas trunk, but Cool Cat bends over to sniff some flowers right as the shotgun fires, and Rimfire only succeeds in destroying his parasol. Cool Cat mistakes Ella for an actual elephant and warns "her" that someones shooting at them and that they should flee, which Cool Cat proceeds to do. Rimfire misinterprets this as Cool Cat warning him that someone else is attacking both of them, and tries to pilot Ella to safety, only to drive over the edge of a cliff and fall into a chasm.

Rimfire irately pushes Ella up the edge of the chasm back up to the jungle above, and Cool Cat helps Ella up to the top, not noticing Rimfires presence, and accidentally knocks the hunter back into the chasm. Still thinking that Ella is a real elephant, Cool Cat leads her away by her trunk to demonstrate some survival skills, while Rimfire climbs out of the chasm for a second time. As Cool Cat leads Ella through the jungle, an amorous male elephant gives chase to Ella, but is then caught by Rimfire who makes the opposite mistake to Cool Cat and assumes that the male elephant is in fact Ella. After yanking at the elephants skin (angering it in the process) looking for an entry hatch, Rimfire finally realizes that he is in fact dealing with a real elephant, who he then apologizes to, though this does not save him from being battered into the ground by the angry elephants trunk.

After recovering from his beating, Rimfire notices Cool Cat looking for food, and decides to throw him "an exploding pineapple" in the form of a hand grenade. Instead of yanking the grenades pin out with his teeth however, Rimfire unknowingly pulls his dentures out of his mouth with the pin, and then tosses the unarmed grenade at Cool Cat. The tiger does mistake it for a pineapple and offers it to Ella, but after the mechanical elephant fails to react to it he tosses the grenade away, causing it to end up back with Rimfire. In turn, Rimfire mistakes the chattering dentures for a tarantula and whips out his shotgun to blast it, detonating the grenade and destroying his dentures.
 bongo that he finds in the jungle and blast Cool Cat when he comes near, but Cool Cat plays Ella a tune on the bongo when he finds it, which leaves Rimfire dazed. He repeats his plan with a large bush, but Cool Cat decides to demonstrate some "Jungle Judo" on the bush, battering Rimfire in the process, then pushes Ella into the bush so that she can try the same thing, only for her to pass clean through the bush, then smash Rimfire on the side of a cliff. Exasperated, Rimfire finally gives up and tells Cool Cat that hes going to leave the jungle, only to discover that Ella has run out of petrol, and begins the 2000-mile journey to the nearest gas station. Cool Cat then opens Ellas rear hatch and checks the engine, and then remarks that "theyre not making elephants like they used to."

==External links==
*  

 
{{succession box |
before=None|
title= Cool Cat shorts |
years= 1967 |
after= Big Game Haunt}}
 

 
 
 