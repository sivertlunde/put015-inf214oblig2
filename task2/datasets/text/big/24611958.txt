Pensionat Paradiset
 
{{Infobox film
| name           = Pensionat Paradiset
| image          =
| image_size     =
| caption        =
| director       = Weyler Hildebrand
| producer       =
| writer         = Artur Enell Robert Wahlberg
| narrator       =
| starring       =
| music          = Eric Bengtson Jules Sylvain Evert Taube
| cinematography = Erik Bergstrand
| editing        = Rolf Husberg
| distributor    =
| released       = 8 February 1937
| runtime        = 79 minutes
| country        = Sweden
| language       = Swedish
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Pensionat Paradiset is a 1937 Swedish film directed by Weyler Hildebrand.

== Plot summary ==
 

== Cast ==
*Thor Modéen as Julle Bergström
*Maritta Marke as Lotta Bergström Nils Ericson as Nisse
*Julia Cæsar as Elvira Pettersson
*Greta Ericsson as Margit
*Carl Hagman as Don Carlos
*Lili Ziedner as Ms. Cronblom
*Linnéa Hillberg as Klingenhjelm
*Carl-Gunnar Wingård as Pålsson

== Soundtrack ==
*Thor Modéen - "En Äkta Mexikanare" (Music and Lyrics by Jules Sylvain and Sven Paddock)

== External links ==
* 
* 

 

 
 
 
 
 
 
 


 
 