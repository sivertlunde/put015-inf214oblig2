The Stars Shine (film)
{{Infobox film
| name           = The Stars Shine  (Original title: Es leuchten die Sterne)   
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Hans H. Zerlett
| producer       = Helmut Schreiber
| writer         = Hans Hannes Hans H. Zerlett
| screenplay     = 
| story          =
| based on       =  
| narrator       =  Paul Verhoeven Karel Stepanek 
| music          = Paul Lincke Ernst Kirsch Leo Leux Franz R. Friedl Mathias Perl
| cinematography = Georg Krause Bruno Mondi
| editing        = Ella Ensink
| studio         = Tobis Filmkunst
| distributor    = Tobis-Filmverleih  (Germany)  American Tobis Company  (United States) 
| released       =  
| runtime        = 
| country        = Nazi Germany
| language       = German
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 1938 Germany|German musical revue directed by Hans H. Zerlett and written by Zerlett and Hans Hannes.   

==Synopsis==
A young secretary leaves the country and travels to Berlin to seek work as an actress. In a comedy of errors, she is mistaken for a famous dancer, which results in her heading the cast of a star-studded musical. The plot acts as a backdrop for this musical revue film, which includes many German film, sports, and entertainment stars of the 1930s.

==Background== Stefan Szekely, Hungarian Jew. political message of the Nazi regime.   Es leuchten die Sterne was created, as were many German films of the period,  to act as a propaganda piece promoting the Third Reich as a cultural entity.   

===Release===
The film was first released in Germany on March 17, 1938. This was followed by a release in the   as Als de sterren schitteren (Flemish) and as Quand les étoiles brillent (French language|French); in Italy as Brillano le stelle; in Denmark as Funklende stjerner; in Greece as Lampoun t asteria; in France as Les étoiles brillent and as Vedettes follies; and in the Netherlands as Parade der sterren and Sterrenparade.  The film was released on DVD in its original German version on July 21, 2008 by Warner Home Video. 
 La Jana present in the studio. 

==Cast==
 
  La Jana as The Dancer
* Ernst Fritz Fürbringer as Hans Holger
* Fridtjof Mjøen as Werner Baumann Paul Verhoeven as Gebauer
* Karel Stepanek as Brandt
* Arthur Schröder as the Director
* Rosita Serrano as the Spanish singer
* Hermann Pfeiffer as the Production manager
* Rudolf Schündler as the Insurance man
* Vera Bergman as Carla Walden
 
* Carla Rust as Mathilde Birk
* Rudi Godden as Knutz the manager
* Elisabeth Wendt as Lisa Marwen
* Else Elster as Mrs. Knutz
* Eva Tinschmann as Mrs. Bökelmann
* Horst Birr as Kruse
* Erwin Biegel as Kellner
* Erika Steenbock as Ella the actress
* Heinz Piper as the Lyricist
* Kurt Mikulski as Böckelmann the makeup artist
 
;Featured appearances:
 
 
* Rudolf Caracciola
* Lil Dagover
* Karl Ludwig Diehl
* Käthe Dorsch
* Willi Forst
* Charles Francois
* Gustav Fröhlich
* Heinrich George
* Walter Gross Paul Hartmann
* Hilde Hildebrand
 
* Paul Hörbiger Paul Kemp
* Hermann Lang
* Wolfgang Liebeneiner
* Harry Liedtke
* Paul Lincke
* Theo Lingen Hans Moser
* Anny Ondra
* Harald Paulsen
 
* Ralph Arthur Roberts
* Max Schmeling
* Sybille Schmitz
* Albrecht Schoenhals
* Hans Söhnker
* Luis Trenker
* Olga Tschechowa
* Manfred von Brauchitsch
* Grethe Weiser
* Ida Wüst
 

==References==
{{reflist|refs=

 {{cite news
|url=http://movies.nytimes.com/movie/142618/Es-Leuchten-Die-Sterne/overview
|title=Es Leuchten Die Sterne (1938)
|work=The New York Times
|accessdate=24 December 2010}} 

 {{cite news
|url=http://select.nytimes.com/gst/abstract.html?res=F00D1FF63E5C1B7A93C3AB178ED85F4C8385F9
|title=The Screen
|last=Nugent
|first=Frank S.
|date=May 21, 1938
|work=The New York Times
|accessdate=24 December 2010}} 

 {{cite web
|url=http://www.allmovie.com/work/es-leuchten-die-sterne-142618
|title=Es Leuchten Die Sterne
|work=Allmovie
|accessdate=24 December 2010}} 

 {{cite web
|url=http://www.ofdb.de/film/41746,Es-leuchten-die-Sterne
|title=Es leuchten die Sterne
|work=OnlineFilmdatenbank
|language=German
|accessdate=24 December 2010}} 

 {{cite book
|last=Winker
|first=Klaus
|title=Fernsehen unterm Hakenkreuz: Organisation, Programm, Personal
|url=http://books.google.com/books?ei=wiwmTfL-DojQrQfy-5GXDA&ct=result&id=xCYbAQAAIAAJ&dq=Es+Leuchten+die+Sterne+La+Jana&q=1938+brachte+das+Fernsehen+Ausschnitte+aus+dem+Film+Es+leuchten+die+Sterne+(Aus+dem+Atelier+-+ins+Atelier)%3B+zu+Gast+im+dunklen+Studio+war+die+S%C3%A4ngerin+La+Jana.#search_anchor
|series=Volume 1 of Medien in Geschichte und Gegenwart
|year=1994
|publisher=Böhlau
|isbn=9783412035945
|page=231
|language=German}} 

 {{cite book
|author=Richard W. McCormick, Alison Guenther-Pal
|editor=Richard W. McCormick, Alison Guenther-Pal
|title=German essays on film
|url=http://books.google.com/books?id=wM031isoRQQC&dq=%22Es+leuchten+die+Sterne%22+1938&source=gbs_navlinks_s
|series=Volume 81 of German library
|year=2004
|publisher=Richard W. McCormick, Alison Guenther-Pal
|isbn=9780826415073
|page=309}} 

 {{cite book
|last=Hull
|first=David S. 
|title=Film in the Third Reich: A Study of the German Cinema, 1933-1945 
|url=http://books.google.com/books?id=g5qy-qkAleoC&pg=PA144&dq=%22Es+leuchten+die+Sterne%22&hl=en&ei=LiQUTf69FIqCsQPB4pCxAg&sa=X&oi=book_result&ct=result&resnum=4&ved=0CDEQ6AEwAw#v=onepage&q=%22Es%20leuchten%20die%20Sterne%22&f=false
|year=1969
|publisher=University of California Press
|isbn=9780520014893 
|page=144}} 

 {{cite book
|author=Hans-Michael Bock, Tim Bergfelder
|editor=Hans-Michael Bock, Tim Bergfelder
|title=The concise Cinegraph: encyclopaedia of German cinema
|edition=illustrated
|series=Film Europa: German Cinema in an International Context
|year=2009
|publisher=Berghahn Books
|isbn=9781571816559}} 

 {{cite book
|last=Leiser
|first=Erwin 
|title=Nazi cinema
|edition=illustrated
|series=Cinema two
|year=1974
|publisher=MacMillan Publishing Company
|isbn= 9780025702301}} 

 {{cite book
|author=Joseph Goebbels, Fred Taylor
|editor=Fred Taylor
|title=The Goebbels diaries
|edition=illustrated
|year=1982
|publisher=H. Hamilton
|isbn=9780241108932}} 

 {{cite book
|last=Romani
|first=Cinzia 
|title=Tainted Goddesses: Female Film Stars of the Third Reich
|isbn=0-9627613-1-1}} 

 {{cite book
|last=Kreimeier
|first=Klaus 
|title=The Ufa story: a history of Germanys greatest film company, 1918-1945
|url=http://books.google.com/books?id=I1u5qMPO0RkC&pg=PA235&lpg=PA235&dq=%22The+Stars+Shine%22,+1938+film&source=bl&ots=__b0LcNCOa&sig=UFp8FeAX2S4Y23ha4cathN0IlTg&hl=en&ei=ExQVTb7bGIW8sQO0-eWnAg&sa=X&oi=book_result&ct=result&resnum=7&ved=0CCsQ6AEwBg#v=onepage&q=%22The%20Stars%20Shine%22%2C%201938%20film&f=false
|edition=reprint, illustrated
|series=Volume 23 of Weimar and now Weimar and Now: German Cultural Criticism
|year=1999
|publisher=University of California Press
|isbn=9780520220690
|page=235}} 

 {{cite book
|last=Waldman
|first=Harry
|title=Nazi films in America, 1933-1942
|url=http://books.google.com/books?ei=zhUVTdK1FoPCvQPq2aHwDQ&ct=result&id=v6saAQAAIAAJ&dq=%22Es+leuchten+die+Sterne%22+1938&q=%22Es+leuchten+die+Sterne%22+#search_anchor
|year=2008
|publisher=McFarland
|isbn=978-0-7864-3861-7
|page=159}} 

}}

==External links==
*   at the Internet Movie Database
*   at Allmovie

 
 
 
 
 
 
 
 
 