Landslide (1940 film)
{{Infobox film
| name           = Landslide 
| image          =
| image_size     =
| caption        =
| director       = Arzén von Cserépy 
| producer       = 
| writer         =  János Kodolányi (play)   György Patkós
| narrator       = Antal Páger Ferenc Kiss   Marcsa Simon
| music          = Dénes Tóth
| cinematography = István Eiben
| editing        = László Cserépy   
| studio         = Népfilm
| distributor    =  
| released       = 28 January 1940
| runtime        = 92 minutes
| country        = Hungary Hungarian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Antal Páger, Ferenc Kiss. Although obstensibly a romance film, it contained significant amounts of propaganda supportive of the policies of Hungarys far-right government.  It was based on a play by János Kodolányi.

==Cast== Antal Páger as Kántor János 
* Olga Eszenyi as Julis,Kántor felesége  Ferenc Kiss as Böbek Samu 
* Marcsa Simon as Böbekné 
* Mária Keresztessy as Jehovás Zsófi 
* Tivadar Bilicsi as Weintraub 
* Zoltán Hosszú as Szektás Samu 
* Paula Bacsányi as Piókás szüle 
* Lenke Egyed as Mari néni 
* János Görbe as Szektás prédikátor 
* Ilona Kiszely as Kristóf Márika 
* Lajos Boray as Orvos 
* Irma Vass as Szektás Samu felesége 
* Lajos Gárday as Péter,halász 
* Ferenc Hoykó as Plébános 
* Miklós Sármássy as Falusi ember 
* Béla Tompa as Falusi ember 
* Jenö Danis as Falusi ember 
* Aladár Szilágyi as Falusi ember 
* Rezsõ Acsay as Falusi ember 
* Aranka Gazdy as Falusi nõ 
* Terus Kováts as Falusi nõ 
* József Pataky as Cigányprímás 
* Gyuszi Ungvári as Józsika,Kántorék fia 

==References==
 

==Bibliography==
* Cunningham, John. Hungarian Cinema: From Coffee House to Multiplex. Wallflower Press, 2004.

==External links==
* 

 
 
 
 
 
 
 

 