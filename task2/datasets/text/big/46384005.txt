The Green Devils of Monte Cassino
{{Infobox film
| name = The Green Devils of Monte Cassino
| image =
| image_size =
| caption =
| director = Harald Reinl Franz Seitz Rudolf Böhmler   Albert Armin Lerche   Michael Graf Saltikow
| narrator =
| starring = Joachim Fuchsberger   Antje Geerk   Ewald Balser
| music = Rolf A. Wilhelm    
| cinematography = Ernst W. Kalinke 
| editing =  Sophie Himpele   Ingeborg Taschner
| studio = Franz Seitz Filmproduktion 
| distributor = Constantin Film
| released =  23 April 1958
| runtime = 94 minutes
| country = West Germany German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Green Devils of Monte Cassino (German:Die grünen Teufel von Monte Cassino) is a 1958 French-German war film directed by Harald Reinl and starring Joachim Fuchsberger, Antje Geerk and Ewald Balser. 

==Cast==
*  Joachim Fuchsberger as Lt. Reiter  
* Antje Geerk as Inge  
* Ewald Balser as Lt. Schlegel  
* Elma Karlowa as Gina  
* Dieter Eppler as Karl Christiansen  
* Agnès Laurent as Hélène  
* Carl Wery as Gen. Heidenreich  
* Wolfgang Preiss as Munkler  
* Wolfgang Wahl as Greinert  
* Wolfgang Neuss as Neumann  
* Armin Dahlen as Pater Emmanuel  
* Harald Juhnke as Hugo Lembke  
* Leonard Steckel as Abbott  
* Jan Hendriks as Fausto  
* Albert Hehn as Maj. Zillert  
* Siegfried Breuer Jr. 
* Hans von Borsody  Walter Gross 
* Michl Lang
* Wolf Ackva 
* Rolf Castell 
* Hans Terofal as Friedrich  

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 

 