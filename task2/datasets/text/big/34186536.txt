Lost Islands (film)
 
 
Lost Islands is an Israeli film by the writer and director Reshef Levy, which came out in 2008. The film is named for the Australian television series The Lost Islands, was broadcast in Israel in which the plot occurs. The film was nominated for 14 Ophir Awards, of which it won four.

The film takes place in the early eighties in Kfar Saba, and tells the story of the Levy family, and especially of the identical twin sons, who fall in love with the same girl.

According to Levy, the film is freely based on personal experiences and his family its many children.

==Soundtrack==
As a film taking place in the early eighties, its soundtrack consists of typical songs of that time.
* The Lost Islands theme song Abracadabra
* Love Boat Theme
* Aquarius 
* Only You
* Dont You Want Me I Ran Forever Young
* Will You
* Moonligh Shadow
* Total Eclipse of the Heart
* Alone Again (Naturally)
* Come On Eilleen
* I Want to Know What Love Is
* Mad World
* It Must be Love

==External links==
 

 
 


 