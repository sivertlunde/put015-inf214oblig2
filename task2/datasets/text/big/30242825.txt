Jack Falls
 

{{Infobox film
| name           = Jack Falls
| image          = Jack_Falls_Theatrical_Release_Poster.jpg
| caption        = Theatrical Release Poster
| director       = Paul Tanter Alexander Williams
| producer       = Simon Phillips Dominic Burns Toby Meredith Patricia Rybarczyk
| writer         = Paul Tanter
| screenplay     = 
| story          = 
| based on       =   Simon Phillips Alan Ford Martin Kemp Neil Maskell Jing Lusi
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Lionsgate
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Simon Phillips, Alan Ford, Martin Kemp, Tamer Hassan, Olivia Hallinan, Doug Bradley, Jing Lusi and Zach Galligan and the third installment in the Jack Says Trilogy, the first ever British film trilogy according to the British Film Institute. Based on the graphic novel of the same title by Paul Tanter, the movie is a contemporary film noir shot in London in high contrast black and white with splashes of colour particularly reminiscent of the Robert Rodriguez film Sin City, but with a harder, grittier edge. Although a stand-alone film, it follows on from the films Jack Says and Jack Said.

==Plot==
Surviving a murder attempt in Amsterdam, former undercover police officer Jack Adleth returns to London to seek revenge and settle some old scores, but he soon finds himself in danger not just from his former criminal associates, but his old police colleagues too. As he battles to stay alive, he must also deal with the guilt from the consequences of his undercover life.

== Cast == Simon Phillips as  Jack Adleth 
*Tamer Hassan as  The Boss 
*Jason Flemying as Damien Alan Ford as Carter
*Dexter Fletcher as Detective Edwards
*Adam Deacon as Hogan Martin Kemp as Dr Lawrence
*Olivia Hallinan as Natasha
*Jing Lusi as Carly
*Doug Bradley as The Doctor
*Rita Ramnani as Erin
*Neil Maskell as Sid

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 