Onnaman
{{Infobox film
| name           = Onnaman
| image          = Onnaman.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Thampi Kannanthanam
| producer       =  R Mohan 
| writer         = Thampi Kannanthanam
| narrator       = 
| starring       = Mohanlal  Ramya Krishnan  Biju Menon Jagathi Sreekumar  
| music          = S P Venkatesh
                   Gireesh Puthenchery (Lyrics) 
| cinematography = 
| editing        =    
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Onnaman is a 2002 Malayalam film by Thampi Kannanthanam starring Mohanlal and Ramya Krishnan.

==Plot==
Onnaman movie is all about Ravishanker (Mohanlal) who is a street-urchin comes under the patronage of Salim Bhai( N F vargees ), a self-styled messiah of the masses and a don too. When Ravishankar grows up, he becomes an adviser to Salim Bhai. With the help of his gang of young friends, Ravishankar soon emerges as a leader of the poor and the oppressed. Naturally, the bad guys led by Gulab Chand Sha, resent him and do everything possible to eliminate Ravishankar.

How Ravishankar combats the evil forces with the help of his friends, his ladylove, the district collector Kamala ( Ramya Krishnan ) and the ACP Vishnu.S.Pillai ( Biju menon )form the rest of the story.
The movie displayed average results in the box office.

==Cast==
*Mohanlal as Ravisankar 
*Ramya Krishnan ... District Collector Kamala
*N. F. Varghese as Salim Bhai 
*Biju Menon as ACP Vishnu S. Pillai Vijayakumar  
*Jagadish
*Jagathi Sreekumar 
*Kavya Madhavan  
*Narendra prasad
*Jayabharathi 
*Rajesh Rajan ...  Friend 1 
*Manuraj ...  Friend 2 
*Saju Kodiyan ...  Friend 3 
*Harisree Martin ...  Friend 4 
*Pranav Mohanlal ...Young Ravisankar (Guest appearance)
*Vinayakan ...  Friend 5
*Haneef Kumaranellur
*Deepika Mohan

==External links==
*  
* http://popcorn.oneindia.in/title/2977/onnaman.html

 
 
 
 
 


 
 