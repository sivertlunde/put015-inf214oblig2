Evangelion: 3.0 You Can (Not) Redo
 
{{Infobox film italic title=no
| name = Evangelion: 3.0 You Can (Not) Redo.
| image = Evangelion 3 poster.png
| caption = Japanese theatrical poster
| director = Hideaki Anno (chief) Mahiro Maeda Kazuya Tsurumaki   Masayuki
| producer = Hideaki Anno Toshimichi Ohtsuki
| writer = Hideaki Anno
| music = Shiro Sagisu
| cinematography = Toru Fukushi
| based on =  
| starring = Megumi Ogata Megumi Hayashibara Yuko Miyamura Maaya Sakamoto Akira Ishida
| editing = Young Mi Lee Studio Khara
| distributor = T-Joy & Khara
| marketing = Jimmy Russel 
| released =  
| runtime = 96 minutes
| country = Japan
| language = Japanese
| gross = USD|$60,487,945   
}}
  animated film Neon Genesis Studio Khara and released in Japanese theaters on November 17, 2012.

==Plot==
  Earth orbit to retrieve a container in which EVA01|Unit-01 and Shinji Ikari are sealed. After Asuka grabs the container, it releases SEELE-built Evangelion Mark.04 Unmanned vehicle|drones. Mari cannot provide support due to her premature Atmospheric entry|reentry, leaving Asuka to battle the drones alone. As she cries for Shinji to do something, Unit-01 awakens and destroys the drones before deactivating and descending back to Earth with Asuka. On Earth, Kaworu Nagisa watches and says he has been waiting for Shinji.

Shinji is salvaged from Unit-01 and fitted with a collar bomb by WILLE, an organization formed to destroy NERV and its Evangelions, led by Misato Katsuragi and Ritsuko Akagi. Shinji is given a physical examination, but nearly everyone is indifferent towards him. When Shinji demands an explanation, more of SEELEs drones attack. WILLE manages to launch the flying battleship Wunder and destroy the drones.
 Evangelion Mark.09, seemingly piloted by Rei, then intercepts the Wunder and retrieves Shinji. Despite Ritsuko urging Misato to kill Shinji, she hesitates until they are out of range.

Rei brings Shinji to what remains of NERV Headquarters, where his father  .

Fuyutsuki invites Shinji to play shogi with him and reveals that Shinjis mother, Yui Ayanami, is inside Unit-01 as the control system and Rei belongs to a series of her Cloning|clones; the Rei which rescued him from the Wunder is only the latest clone and exhibits none of the personality traits of the original. The revelation causes Shinjis mental breakdown. On the day of the operation, Shinji is unsure whether to follow Gendos orders or Misatos plea to not pilot an Evangelion again, so Kaworu removes Shinjis collar and wears it as a sign of trust.

Shinji and Kaworu pilot Unit-13 into Terminal Dogma on their mission to use the Spears of Cassius and Longinus to undo Third Impact; Rei follows in Mark.09. When Unit-13 reaches Lilith (Neon Genesis Evangelion)|Liliths corpse, Kaworu realizes that both spears are the same type, not two different types as first expected. He pleads with Shinji to not remove them, but to no avail. Asuka and Mari arrive in their EVAs to stop Shinji. Nevertheless, Shinji removes the spears, causing Lilith to explode into LCL. By Gendos order, Mark.09 decapitates Mark.06 to release the Twelfth Angel, which is absorbed by Unit-13.

The awakened Unit-13 flies out of the Geofront and rises into the sky, starting Fourth Impact. Kaworu reveals he is the First Angel, now "cast down" to the Thirteenth. The collar detects Unit-13s awakening and activates. Rei loses control of Mark.09, which boards Wunder on its own in an attempt to take control of the ship. Rei ejects from her EVA, and Asuka blows up Unit-02 to destroy Mark.09. To halt Fourth Impact, Kaworu allows the choker to kill him, devastating Shinji. Mari then ejects Shinjis cockpit from Unit-13. WILLE recovers Unit-02 and Unit-08, then retreats. Asuka recovers an unresponsive Shinji and drags him through the remains of Tokyo 3, with Rei following.

==Cast==
 
{| class="wikitable"
|-
! Character
! Japanese
! English
|-
|    || Megumi Ogata || Spike Spencer
|-
|    || Akira Ishida || Jerry Jewell
|-
|   || Yuko Miyamura || Tiffany Grant
|-
|   || Megumi Hayashibara || Brina Palencia 
|-
|    || Kotono Mitsuishi || Allison Keith 
|- Yuriko Yamaguchi || Colleen Clinkenbeard
|-
|   || Maaya Sakamoto || Trina Nishimura
|- Kent Williams
|-
|    || Fumihiko Tachiki || John Swasey
|-
|     || Mariya Ise || Tia Ballard
|-
|     || Miyuki Sawashiro || Felecia Angelle
|- Phil Parsons
|-
|     || Akio Otsuka || Greg Dulcie
|-
|     || Anri Katsu || Aaron Roberts
|-
|    || Hiro Yuuki || Mike McFarland
|-
|     || Sayaka Ohara || Krishna Smitha
|-
|   || Miki Nagasawa || Caitlin Glass
|}

  New character.


==Marketing== Kaji yelling while pointing a pistol at someone, Mari confronting four Rei clones, a private conference between Mari and an unknown person, and a restored Asuka wearing an eyepatch (despite none of them appearing in the final film).

On August 26, 2011, a 15-second teaser trailer was shown after the Evangelion 2.0 You Can (Not) Advance television broadcast, featuring footage of Asuka piloting Unit-02 in space and a tentative release date in the fall of 2012.    On January 1, 2012, the official website was updated to reveal the official English title, You Can (Not) Redo, and a tentative release date for the fourth and final film in 2013.  The "EVA-EXTRA 08" live screening in Shinjuku revealed that the film would be released in Japan on November 17, 2012. 

A 30-second trailer streamed on Nico Nico Douga on October 17, 2012, showing Shinji, Rei, Kaworu, Mari, and Asuka.  A full trailer, the fifth promotional video for the film, was released on November 1, 2012, containing both footage from the August 26 Nico Nico Douga trailer as well as previously unseen footage. It also featured a new song by music composer Shirō Sagisu, titled "The Wrath of God, in All Its Fury". On November 14, a one-minute 48-second trailer was released privately on Hikaru Utadas YouTube channel. It features Hikaru Utadas new song, "Sakura Nagashi". On November 16, 2012, the first six minutes and thirty-eight seconds of the film were aired on Nippon Television to promote the films release at midnight on November 17.  An additional 17-second trailer was released on November 18, showing EVA Units 02 and 13, the airship Wunder, and the main cast of Evangelion thus far.

A new three-minute trailer was released on April 17 as the promotional video for the 3.0 video release, renamed Evangelion: 3.33 You Can (Not) Redo.

==Release==
The film was released in Korean theaters in April 2013. The Japanese Blu-ray and DVD was released on April 24, 2013.  

The film has been licensed by Funimation (North America), Madman Entertainment (Australia) and Manga Entertainment (United Kingdom) for home release on Blu-ray and DVD.   The US announcement was revealed once their Facebook page reached over 1 million likes and was later provided with a release date for February 2014.  However due to more demand on theatrical screenings the home release was delayed.  After numerous months with lack of information regarding both the theatrical release and home release, a FUNimation post on Facebook assured fans that the distributor is working closely with Studio Khara to ensure the English dub is closer to Kharas vision.  As a result, both the Australian and United Kingdom releases (both originally given a March 2014 release date) have been postponed until the situation has been sorted. In December 2014, Funimation confirmed that they still plan on releasing Evangelion 3.33 and that Khara is creating the English-language subtitle track for the Western release of the film.  

Before the delay, Madman organized screenings of 3.0 as part of its Reel Anime film festival in select cinemas across Australia from September to October 2013.  In the US, the film was screened as part of the Jpop Summit Festival 2013 in San Francisco on July 27, July 29 and August 4, 2013. It was also screened at Otakon 2013 in Baltimore, Maryland and at Anime Weekend Atlanta on September 28. The premiere of the English dub was screened at New York Comic Con on October 11, 2013, which was previously announced as a subbed screening. 

==Music==
The theme song for the film,  , was provided by   with the films score by Shirō Sagisu titled Shiro SAGISU Music from "EVANGELION: 3.0" You Can Not Redo which contains the "full" music track from the film as well as several bonus tracks not included in the film was released on November 28, 2012.  Another soundtrack disc contains the film edited version of the music, titled Evangelion: 3.0 You Can (Not) Redo Original Soundtrack was released on April 24, 2013 bundled with the first press edition of the films home video release.
==Reception==
===Box office===
The film was released in Japan on November 17, 2012. It earned Japans second-highest weekend box office of 2012 with 1,131,004,600 yen (US$13,913,200).  The film subsequently grossed the equivalent of over US $60 million at box office. 
===Awards and Nominations===
{| class="wikitable"
|-
! Year
! Award
! Category
! Result
|- 2013
| Japan Academy Award Animation of the Year
|  
|- Sitges Catalonian International Film Festival - Animat Award
| Best Animated Feature Film
|  
|}

==Sequel== EVA Unit 8+2, an apparent fusion of Unit-02 and Unit-08, seemingly fighting dark green copies of Mark.06. Misatos narration of the trailer suggests Shinji who "still lack  the will to live" finding a "place that teaches him hope", the implementation of the Human Instrumentality Project, and a final stand by WILLE and the Wunder (with the customary promise of fan service, "up to the end").

==References==
 

==External links==
 
*    
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 