The Velocity of Gary
 
{{Infobox Film
| name           = The Velocity of Gary
| image          = Velocity of gary poster film.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Dan Ireland
| producer       = Dan Lupovitz James Still
| based on       =  
| narrator       = Vincent DOnofrio
| starring       = Thomas Jane Vincent DOnofrio Salma Hayek Olivia dAbo Ethan Hawke Chad Lindberg
| music          = Peitor Angell
| cinematography = Claudio Rocha
| editing        = Luis Colina Debra Goldfield
| distributor    = Sony Pictures
| released       = July 16, 1999
| runtime        = 101 min.
| country        = United States English
| budget         = $4,000,000 (estimated)
| gross          =
}} James Still, based on his homonymous play. It stars Thomas Jane in the title role, along with Salma Hayek and Vincent DOnofrio.

== Synopsis == bohemian family, which includes Veronica, a still-active porn star, and Nat, a tattoo artist. Gary is also in love with Valentino, who is dying of AIDS. Through the stages of the disease, Mary Carmen and Gary argue over what kind of care he should be receiving, and who is going to supply that care. As Valentino draws near death, Mary Carmen finds out she is carrying Valentinos baby. The three take stock of themselves and their relationships with one another.

== Cast ==
* Vincent DOnofrio ... Valentino
* Salma Hayek ... Mary Carmen
* Thomas Jane ... Gary
* Olivia dAbo ... Veronica
* Ethan Hawke ... Nat
* Chad Lindberg ... Kid Joey Jason Cutler ... Romaine
* Shawn Michael Howard ... Coco
* Elizabeth DOnofrio ... Dorothy
* Hakan DOnofrio ... Running Boy
* Keegan de Lancie ... Choir Boy
* Ravell Dameron ... Receptionist in Clinic
* Yvette Diaz ... Young Mary Carmen
* Marion Eaton ... Miss Sweetheart
* Phillip Esposito ... Phone Sex Guy
* Luchisha Evans ... Waitress
* Michael Mantell ... Angry Customer
* Gloria Irizarry ... Mrs. Sanchez
* Ruby Rufus Isaacs ... Sleeping Beauty
* Khalil Kain ... Venus
* Arielle Santos ... Hope
* Stephen C. Marshall ... Angry Boss
* Hugh Palmer ... Paramedic
* John Panico ... Saxophone Player
* Cordelia Richards ... Jana Roberts

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 


 