The Hole in the Wall (1921 film)
 
{{infobox film
| title          = The Hole in the Wall
| image          =
| imagesize      =
| caption        =
| director       = Maxwell Karger
| producer       = Metro Pictures   Maxwell Karger
| writer         = Maxwell Karger (scenario)
| based on       =  
| starring       = Alice Lake Allan Forrest
| music          =
| cinematography = Allen Siegler
| editing        =
| distributor    = Metro Pictures
| released       =   reels (6,100 feet)
| country        = United States Silent  (English intertitles)

}} Fred Jackson.   

==Cast==
*Alice Lake - Jean Oliver
*Allan Forrest - Gordon Grant
*Frank Brownlee - Limpy Jim
*Charles Clary - The Fox
*William De Vaull - Deagon
*Kate Lester - Mrs. Ramsey
*Carl Gerard - Donald Ramsey John Ince - Inspector of Police
*Claire Du Brey - Cora Thompson

==Preservation status==
This film is now considered a lost film. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 

 