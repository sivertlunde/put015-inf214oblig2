A Private Function
{{Infobox Film
| name           = A Private Function
| image          = A Private Function.jpg
| caption        = Theatrical release poster
| director       = Malcolm Mowbray Denis OBrien   Mark Shivas
| writer         = Alan Bennett   Malcolm Mowbray
| starring = {{Plainlist|
* Michael Palin
* Maggie Smith
* Denholm Elliott
* Richard Griffiths
* Tony Haygarth Bill Paterson
* John Normington Liz Smith
* Alison Steadman
}}
| music          = John Du Prez
| cinematography = Tony Pierce-Roberts
| editing        = Barrie Vince
| studio         = HandMade Films
| distributor    = 
| released       = November 1984
| runtime        = 94 minutes
| country        = United Kingdom BAFTA Film Awards   Best actress   Best supporting actor   Best supporting actress
| language       = English
| budget         = £1.2million
| gross          = $2,527,088 
}} 1984 cinema British comedy film starring Michael Palin and Maggie Smith. The film was predominantly filmed in Ilkley, Ben Rhydding, and Barnoldswick West Yorkshire.  The film was screened in the Un Certain Regard section at the 1985 Cannes Film Festival.   

==Synopsis== Princess Elizabeth Prince Philip and illegally decide to raise a pig for that occasion. However, the pig gets stolen by Gilbert Chilvers (Michael Palin), who was encouraged to do so by his wife Joyce (Maggie Smith). Meanwhile a food inspector is determined to stop activities circumventing the food rationing.

==Cast==
*Michael Palin ... Gilbert Chilvers
*Maggie Smith ... Joyce Chilvers
*Denholm Elliott ... Dr Charles Swaby
*Richard Griffiths ... Henry Allardyce the Accountant
*Tony Haygarth ... Leonard Sutcliff the Farmer
*John Normington ... Frank Lockwood the Solicitor Bill Paterson ... Morris Wormold the Meat Inspector Liz Smith ... Joyces Mother
*Alison Steadman ... Mrs Allardyce Jim Carter ... Inspector Noble
*Pete Postlethwaite ... Douglas J. Nuttol the Butcher
*Rachel Davies ... Mrs Forbes, Mr Wormolds landlady

==Production==
Three pigs were used in the filming of A Private Function, all named Betty. Producer Mark Shivas was advised by Intellectual Animals UK that the pigs used should be female and six months old so as to not be too large or aggressive. However, the pigs were "unpredictable and often quite dangerous". During filming of one of the kitchen scenes, Maggie Smith was hemmed in by one of the pigs and needed to vault over the back of it in order to escape. 

==Awards==
The film won three  ) and Best Film.

==Musical adaptation== West End in April 2011, under the new title Betty Blue Eyes. It was produced by Cameron Mackintosh and ran for several months at the Novello Theatre. It starred Reece Shearsmith (of The League of Gentlemen fame) as Gilbert, and actress Sarah Lancashire as Joyce. 

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 