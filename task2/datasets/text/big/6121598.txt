Going Hollywood
{{Infobox film
| name           = Going Hollywood
| image          = Goinghollywood.jpg
| caption        = Theatrical release poster
| director       = Raoul Walsh
| producer       = Walter Wanger
| screenplay     = Donald Ogden Stewart
| story          = Frances Marion
| starring       = {{Plainlist|
* Marion Davies
* Bing Crosby
}}
| music          = Herbert Stothart
| cinematography = George J. Folsey Frank Sullivan
| studio         = {{Plainlist|
* Cosmopolitan Productions
* Metro-Goldwyn-Mayer
}}
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = $914,000  
| gross          = $583,232 Matthew Bernstein, Walter Wagner: Hollywood Independent, Minnesota Press, 2000 p434 
}}
Going Hollywood is a 1933 American musical film directed by Raoul Walsh and starring Marion Davies and Bing Crosby. It was written by Donald Ogden Stewart and based on a story by Frances Marion.  Going Hollywood was released by Metro-Goldwyn-Mayer on December 22, 1933.

==Plot==
A French teacher at an all-girl school who longs to find love. When she hears a young singer on the radio, she visits him and thanks him, which causes problems with another woman.

==Cast==
* Marion Davies as Sylvia Bruce
* Bing Crosby as Bill Billy Williams
* Fifi DOrsay as Lili Yvonne
* Stuart Erwin as Ernest Pratt Baker, Picture Producer
* Ned Sparks as Mr. Bert Conroy, Director
* Patsy Kelly as Jill Barker Bobby Watson as Jack Thompson, the Press Agent
* Three Radio Rogues as Group Performing Imitations
* Eddie Bartell as Radio Rogue
* Jimmy Hollywood as Radio Rogue
* Henry Taylor as Radio Rogue

==Soundtrack==
* "Going Hollywood" by Bing Crosby at the railroad station
* "Our Big Love Scene" by Bing Crosby
* "Beautiful Girl" by Bing Crosby
* "Just an Echo in the Valley" by Bing Crosby
* "Well Make Hay While the Sun Shines" by Bing Crosby and Marion Davies and chorus
* "Cinderellas Fella" by Fifi DOrsay, reprised by Marion Davies
* "Happy Days Are Here Again"
* "When the Moon Comes Over the Mountain" by Jimmy Hollywood
* "You Call It Madness (But I Call It Love)" by Henry Taylor imitating Russ Columbo
* "Remember Me" by Jimmy Hollywood
* "My Time Is Your Time" by Jimmy Hollywood
* "After Sundown" by Bing Crosby
* "Temptation (1933 song)|Temptation" by Bing Crosby

==Release==
Going Hollywood was released on home video in May 1993.   Warner released on DVD in July 2013.   

==Reception==
TV Guide called it "fluffy fun" with a "literate and amusing screenplay".   A reviewer on Turner Classic Movies praised Crosbys singing and said that his voice never falters.   Jamie S. Rich of DVD Talk rated it 3.5/5 stars and wrote, "Going Hollywood is almost the perfect Hollywood movie musical cliché." 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 