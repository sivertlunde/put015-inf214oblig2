Kasam Paida Karne Wale Ki
{{Infobox Film
| name = Kasam Paida Karne Wale Ki
| image = kasamPKWK.jpg
| image_size =
| caption = DVD cover of Kasam Paida Karne Wale Ki
| director = Babbar Subhash
| producer = Babbar Subhash
| writer =
| narrator =
| starring = Mithun Chakraborty Smita Patil Salma Agha Amrish Puri
| music = Bappi Lahiri
| cinematography =
| editing =
| distributor = B. Subhash Film Unit
| released = November 23, 1984
| runtime =
| country = India
| language = Hindi
| budget =
| preceded_by =
| followed_by =
| website =
}}
 Hindi feature film produced & directed by Babbar Subhash, starring Mithun Chakraborty, Smita Patil, Salma Agha, Karan Razdan, Geeta Siddharth and Amrish Puri.

==Plot==
Kasam Paida Karne Wale Ki is the story of Satish Kumar (Mithun Chakraborty). After the passing of his father, Satish is the only heir to a vast estate. As he is five years old, he can only inherit it fully when he becomes 20. The estate is placed under the care of his paternal uncle, Udaybhan Singh (Amrish Puri), who carefully drugs and intimidates Satish to such an extent that Satish is terrified of his uncles shadow.

Years go by. Satish has achieved adulthood, is married to Aarti (Smita Patil), and continues to live in fear of his uncle. After one night of bliss, Satish finds out that Aarti had married him to rob him of cash and jewellery. He is devastated. Uday asks Aarti to leave, and Satish follows her. Aarti asks him for forgiveness, and he does so. However, Udaybhan knifes him to death and has it written off by the police as suicide. Aarti swears to avenge this through her child in her womb. Years later, Aarti and her son, Avinash (Mithun Chakraborty), surface to avenge Satishs death — they find out that it is not easy to hoodwink a wily Udaybhan, and they could well be endangering their very own lives.

== Cast ==
* Mithun Chakraborty  as  Satish Kumar / Avinash S. Kumar
* Smita Patil  as  Aarti S. Kumar
* Salma Agha  as  Leena
* Amrish Puri  as  Udaybhan Singh
* Gita Siddharth  as  Satishs Daimaa
* Jagdish Raj  as  Judge
* Karan Razdan  as  Chanderbhan U. Singh
* Bob Christo  as  Udaybhans Assistant

==Soundtrack==
{{Track listing
| extra_column    = Singer(s)

| all_writing     =
| all_lyrics      =
| all_music       =

| title1          = Come Closer
| extra1          = Salma Agha

| title2          = Dance Dance
| note2           = I
| extra2          = Salma Agha, Vijay Benedict, Chorus

| title3          = Dance Dance
| note3           =II
| extra3          = Salma Agha, Vijay Benedict, Chorus

| title4          = Kasam Paida Karne Wale Ki
| extra4          = Vijay Benedict

| title5          = Jeena Bhi Kya
| note5           = I
| extra5          = Salma Agha, Vijay Benedict

| title6          = Jeena Bhi Kya
| note6           =II
| extra6          = Salma Agha, Vijay Benedict

| title7               = Jhoom Jhoom Baba
| extra7            = Salma Agha
}}

==Popular Culture==
Kasam Paida Karne Wale Ki features a dance sequence called Jeena Bhi Kya Hai Jeena Teri Ankhon Ke Bina clearly modeled on Michael Jacksons video "Michael Jacksons Thriller (music video)|Thriller", in which the ghoulish characters do a synchronised dance in a graveyard. The soundtrack of this song is inspired by Michael Jacksons "Billie Jean".

The film has recently gained attention for the   and   featuring Stig Dogg, "Cattivi e buoni" (2006) by Club Dogo and Come Closer (2009) by Onra. 
 Anurag Kashyaps Gangs of Wasseypur. However, the a from the word Kasam has been removed.

==Remake==
Kasam Paida Karne Wale Ki was remade as Mangamma Sabadham: Sujatha and Madhavi (actress)|Madhavi. It was released in 1985, directed by veteran K. Vijayan.  

==See also==
* Donga (film), a 1985 Tollywood film which also features a parody of Michael Jacksons "Thriller" video that has become an Internet meme.

==References==
 

 

==External links==
*  

 
 
 
 
 