Hinemoa (1914 film)
 
{{Infobox film
| name           = Hinemoa
| image          = Hinemoa 1914 still1.jpg
| image_size     =
| caption        = Promotional screen shot George Tarr
| producer       = Edward Anderson
| writer         =
| narrator       =
| starring       = Hera Tawhai (Hinemoa)  Rua Tawhai (Tutanekai) Miro Omahau (Tiki) Tamai Omahau (Ngararanui)
| music          =
| cinematography = Charles Newham
| editing        =
| distributor    =
| released       = 17 August 1914, Lyric Theatre, Auckland
| runtime        = 42 minutes
| country        = New Zealand
| language       = Silent
| budget         = ₤50
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 film of the same name shot by Gaston Méliès a year earlier. It was billed as "The first big dramatic work filmed and acted in the land of the Moa"   

==Plot==
The film told the Māori legend of  . No prints are known to have survived.

==Production==
The ₤50 budget was funded by Edward Anderson, of the Auckland Chamber of Commerce
The film was shot on location around Rotorua in only 8 days. All the cast members were drawn from the Reverend F.A. Bennetts Māori choir. 

==Legacy==
The film opened at the Lyric Theatre, Auckland during the first week of World War I, and performed well. It then toured the country and was later exhibited overseas.

The promotional image of Hinemoa featured on the 40c stamp of the New Zealand Post 1996 "Centenary of New Zealand Cinema" stamp issue. 

The New Zealand Film Archive lists in its holdings tape of an undated radio interview with George Tarr and Hera Tawhai Rodgers about the making of the film. (Rodgers was 76 at the time of the interview, so it was probably in the 1960s) 

==References==
 
 

 
 
 
 
 
 
 

 