The Good Soldier Schweik (1960 film)
 
{{Infobox film
| name           = The Good Soldier Schweik
| image          = The Good Soldier Schweik.jpg
| image_size     = 
| caption        = 
| director       = Axel von Ambesser
| producer       = Artur Brauner (producer)
| writer         = Jaroslav Hasek (novel) Hans Jacoby
| narrator       = 
| starring       = See below
| music          = Bernhard Eichhorn
| cinematography = Richard Angst Hermann Haller
| studio         = 
| distributor    = 
| released       = 1960
| runtime        = 96 minutes (Germany) 98 minutes (USA)
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

The Good Soldier Schweik (German:Der brave Soldat Schwejk) is a 1960 West German comedy film directed by Axel von Ambesser. Based on the satirical novel The Good Soldier Švejk by Jaroslav Hašek it depicts the adventures of a simple Czech soldier during World War I|World&nbsp;War&nbsp;I.

The film features Heinz Rühmann as the protagonist Schweik, and was nominated for the 1962 Golden Globe Awards.

== Plot summary ==
The Czech survivalist Schweik (played by Heinz Rühmann) is a dog trader based in Prague. He gets charged for lèse-majesté and is supposed to be jailed, but when the court finds him to be dim-witted, he is instead committed to a mental asylum. There, the doctors examine his physical and mental status. When one of the physicians accuses Schweik of being a simulator, Schweik assures him that he is an officially approved imbecile.

When World War I breaks out in 1914, Schweik is drafted into the Austro-Hungarian Army as a common soldier. Because he suffers from rheumatism, he is detached as a servant for Lieutenant Lukas (Ernst Stankovski). The latter is busy not getting transferred to the front line, so he can spend his time with gambling and with beautiful ladies. When Lukas loses his entire money and even his servant Schweik to a colonel in a game of cards, Schweik buys himself free with his own money and returns to Lukas.

One day, Schweik acquires a terrier. Lukas is delighted, even though his cat gets mauled by the dog. When Lukas goes for a walk with the terrier and with a girl named Gretl (Senta Berger), they meet his commanding officer who is the dogs proper owner. Accused of theft, Lukas is transferred to České Budějovice. When he and Schweik are on their way to České Budějovice by train, Schweik pulls the emergency break. Unable to pay the fine for misuse of the break, Schweik has to leave the train at the next station and continues his journey on foot. He is then arrested as a deserter and is even thought to be a Russian spy. But because of his naivety and clumsiness he is soon released.

Thereafter, Schweik wants to resume his service with Lieutenant Lukas, but the latter has already got a new servant. Schweik, however, is tasked with delivering a letter to Lukas lover. Shortly before delivering the letter, Schweik encounters his old friend Woditschka (Franz Muxeneder) and they get drunk together. In the evening, Schweik wants to finally get rid of the letter, but now the ladys husband has returned. In order to protect Lukas, Schweik pretends that he wrote the love letter himself.

Eventually, Lukas and Schweik are transferred to the Russian front. While being under hostile fire on the battlefield, Schweik finds a four-leaf clover and gives it to Lukas as a talisman. Shortly after though, Lukas is mortally wounded, and gets carried off the battlefield by Schweik. Schweik hides in a corn field and comes across a Russian soldier. They become friends and swap their uniforms. Schweik is then arrested by an Austrian patrol who consider him to be a Russian. He is charged with desertion and defection and is convicted to death by firing squad. However, just when he is to be executed, the war ends and Schweik is released. He returns home and meets Woditschka in his favourite pub. So, despite of the grand political scheme of the war, in the end, nothing has changed for Schweik.

== Cast ==
*Heinz Rühmann as Schwejk
*Ernst Stankovski as Oberleutnant Lukas
*Franz Muxeneder as Woditschka
*Ursula von Borsody as Kathi
*Erika von Thellmann as Baronin
*Senta Berger as Gretl
*Fritz Imhoff
*Jane Tilden as Housemaid
*Rudolf Rhomberg
*Fritz Eckhardt as Wendler
*Hugo Gottschlich as Wachtmeister Flanderka
*Michael Janisch
*Fritz Muliar as Russian Soldier
*Hans Unterkircher
*Erik Frey
*Erland Erlandsen
*Egon von Jordan
*Hans Thimig
*Guido Wieland
*Edith Elmay
*Alma Seidler

== Soundtrack ==
 

== Awards and nominations == Golden Globe for Best Foreign-Language Foreign Film (Samuel Goldwyn International Award). {{cite web|url=http://www.goldenglobes.org/browse/?param=/year/1961 |title=
The 19th Annual Golden Globe Awards (1962) |publisher=Hollywood Foreign Press Association |accessdate=25 February 2013}} 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 
 