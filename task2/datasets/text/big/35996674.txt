Epic (2013 film)
 
{{Infobox film
| name           = Epic
| image          = Epic (2013 film) poster.jpg
| alt            = A young red-headed girl surrounded by a snail and slug, a fairy queen, fairy warriors, a toad and a goblin-like creature in a batskin cloak. In the background is a mountain surrounded by hummingbirds. The words "Epic" are at the bottom in gold.
| caption        = Theatrical release poster
| director       = Chris Wedge
| producer       = {{Plainlist|
* Lori Forte
* Jerry Davis }}
| screenplay     = {{Plainlist| William Joyce
* James V. Hart
* Daniel Shere
* Tom J. Astle
* Matt Ember }}
| story          = {{Plainlist|
* William Joyce
* James V. Hart
* Chris Wedge }}
| based on       =  
| starring       = {{Plainlist|
* Colin Farrell
* Josh Hutcherson
* Amanda Seyfried
* Christoph Waltz
* Aziz Ansari
* Chris ODowd Pitbull
* Jason Sudeikis
* Steven Tyler
* Beyoncé|Beyoncé Knowles }}
| music          = Danny Elfman
| cinematography = Renato Falcão
| editing        = Andy Keir
| studio         = {{Plainlist|
* Blue Sky Studios
* 20th Century Fox Animation }}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 102 minutes  
| country        = United States
| language       = English
| budget         = $93 million      
| gross          = $268,426,634   
}} fantasy Action William Joyces Ice Age (2002) and Robots (2005 film)|Robots (2005). The film stars the voices of Amanda Seyfried, Josh Hutcherson, Colin Farrell, Christoph Waltz, Aziz Ansari, Chris ODowd, Pitbull (rapper)|Pitbull, Jason Sudeikis, Steven Tyler, and Beyoncé|Beyoncé Knowles. The film was released on May 24, 2013 to mixed critical reception, and earned $268 million on a $93 million budget.

==Plot==
17-year-old Mary Katherine, or M.K. (Amanda Seyfried), moves in with her eccentric scientist father Professor Bomba (Jason Sudeikis) who has been searching for tiny humanoid soldiers called Leafmen. They protect the forest Bomba lives near from evil creatures called Boggans and their malevolent leader Mandrake (Christoph Waltz). An independent young soldier named Nod (Josh Hutcherson) decides to quit much to the ire of the no-nonsense Leafmen leader Ronin (Colin Farrell).

The queen of the forest, Queen Tara (Beyoncé|Beyoncé Knowles), must choose an heir to her throne and goes out to a field of leaf pods, guarded by a laid-back slug named Mub (Aziz Ansari) and a wannabe Leafman snail named Grub (Chris ODowd). Immediately after she chooses a pod, the Boggans attack. Tara flees the area with the pod, and though her bodyguards do their best to protect her, they are soon overwhelmed by the sheer number of Boggans. Eventually, Ronin arrives for her and the pair fly off on his hummingbird mount. They are then attacked by Mandrake and his son Dagda (Blake Anderson). Dagda is killed by Ronin, but Tara is shot with an arrow by Mandrake.
 bugs on bullfrog named Bufo (Pitbull (rapper)|Pitbull) to throw the race. Before Bufo and his two henchmen can feed Nod to a snake for not throwing the race, Ronin intervenes and orders them to leave. A reluctant Nod joins him, M.K., Mub, and Grub after hearing about Queen Taras death which Bufo overhears.

Ronin, Nod, M.K., Mub, and Grub eventually find Nim Galuu (Steven Tyler) who leads them down to the scroll library, where M.K. discovers Taras brief message before shrinking her, and a message that will get her back to normal size. When Ronin leaves, Nod takes M.K. on a deer ride and they begin to fall in love. Meanwhile, Mandrake has had the Boggans bring Bufo to him, finding out the location of the pod. Mandrake goes to Nim Galuus place to steal the pod, which, if it blooms in darkness will help Mandrake destroy the forest. He takes the pod and kidnaps Mub and Grub. Ronin scolds Nod for not being there to protect the pod. To get into Boggan territory undiscovered, M.K., Nod, and Ronin set out to Bombas house to get some disguises, where M.K. learns that the Leafmen have deliberately been leading Bomba off their trail. Bomba sees that he has visitors and captures M.K., fainting when he sees her. M.K. marks the location of Moonhaven on a map Bomba has made of the forest before rejoining Nod and Ronin.

When they reach the Boggan land Ronin distracts the Boggans while M.K. and Nod rescue Mub, Grub, and the pod. Mandrake discovers them and orders the Boggans to stop them. M.K., Nod, Mub, and Grub escape alive, but Ronin sacrifices himself to ensure their escape. Before the full moon can sprout the pod at Moonhaven, Mandrakes bats block the light, causing the pod to begin sprouting in darkness. The Leafmen set out to fight the Boggans; M.K. tries to get help from her father by visiting his various cameras he had set in the forest. However, upon regaining consciousness, Bomba believes that he didnt really see M.K. and that he has been insane all these years, and shuts down all his cameras. He changes his mind when he sees a red push-pin that M.K. had put on his map.

Bomba is overjoyed to see that he has been right and he follows M.K. to Moonhaven. M.K. uses Bombas iPod to make bat sounds, luring Mandrakes bats away. Meanwhile, Mub and Nim Galuu try to stop Mandrake from reaching the pod, but are unsuccessful. Just then, Ronin appears, bearing scars and bruises from the Boggans. Mandrake manages to outdo him, but Ronin is defended by Nod, who finally realizes the importance of teamwork. Before Mandrake can obtain his victory, the pod blooms in moonlight, defeating the Boggans, and sealing Mandrake into a nearby tree.

The chosen heir is the flower child (Allison Weber) who helped save Tara earlier in the film. Grub becomes a Leafman, Nod and Ronin reconcile, and Nod and M.K. kiss before M.K. is returned to her original size. After reuniting with Bomba and becoming his assistant, the human family still keeps regular contact with their small friends as they continue the research of their world.

==Cast== William Joyces deceased daughter.   
* Josh Hutcherson as Nod, a rookie Leafman warrior. 
* Colin Farrell as Ronin, a seasoned Leafman warrior, leader of the Leafmen, Nods guardian and teacher, and a friend of Nods late father.   
* Christoph Waltz as Mandrake, the leader of the Boggans.    Waltz also voiced Mandrake in the German dub of the film since he can also speak German. 
* Aziz Ansari as Mub, a slug, Grubs best friend and caretaker of the pods.  
* Chris ODowd as Grub, a snail, Mubs best friend and fellow caretaker.  Pitbull as bullfrog who race fixer and a Businessperson|businessman. 
* Jason Sudeikis as Professor Radcliffe  Bomba, Mary Katherines father and a scientist  
* Steven Tyler as Nim Galuu, a Glowworm, a showman and keeper of magic scrolls that tell what has occurred during the times, in a tree 
* Beyoncé|Beyoncé Knowles as Queen Tara, the Mother Nature-like queen of the forest and Ronins childhood love. 
* Blake Anderson as Dagda, Mandrakes son and the Boggan general.      
* Allison Bills as Dandelion Jinn
* Jim Conroy as Roller Derby Race Announcer
* John DiMaggio as Pinecone Jinn Troy Evans as Thistle Jinn
* Jason Fricchione as Bufos Goon, a bug henchman of Bufo who beats up Nod.
* Judah Friedlander as Larry, a taxicab driver. 
* Helen Hong as Thistle Lady.
* Kelly Keaton as Berry Lady, a racer at the Roller Derby.
* Emma Kenney as Dandelion Girl, a humanoid dandelion.
* Kyle Kinane as Biker Dude, an insect racer at the Roller Derby.
* Anthony Lumia as Young Fruit Fly
* Todd Cummings as Old Fruit Fly
* Malikha Mallette as Jinn Mom, the mother of the Flower Kid.
* Joe Massingill as Stickman Jockey, a racer in the Roller Derby.
* Edie Mirman as Flower Jinn
* Rosa Salazar as Roller Derby Girl, a worker at the Roller Derby.
* Allison Webber as Flower Kid, a humanoid flower who becomes Queen Taras successor.
* Thomas F. Wilson as Finn, a Leafman who is a fellow soldier of Ronin.

==Production== William Joyces book, The Leaf Men and the Brave Good Bugs for Fox Animation. Joyce, who had already collaborated with Wedge as a designer and producer on the 2005 film Robots (2005 film)|Robots, was set to produce the film.  At one point, Wedge got permission to find a new home for the film and turned to Pixar, led by John Lasseter, a close friend that Wedge knew from working on Tron. When Pixar tried to close the rights for the film and start development, Fox changed their mind, and the film returned to Fox. The film was officially greenlit in 2009, under the title Leaf Men.  In May 2012, Fox announced the final title for the film (Epic), its first cast details, and a plot.  According to Wedge, he was not satisfied with the renaming, which was decided by the marketing department. He also expressed dissatisfaction with subtitles given to the film in some non-English countries, including the French subtitle, The Battle of the Secret Kingdom. 

Although the film is based on and borrows many characters from Joyces book, its plot has been significantly changed. Wedge explained: "But while Bill wrote a wonderful book, it is a quaint story. We wanted to make a gigantic action-adventure movie." To address online speculations about whether the film is similar to other films, like   or  . And it does immerse the audience completely in a world like Avatar. But it has its own personality." 

==Release==
The film was released publicly on May 16, 2013, in Argentina, Bolivia, Chile, Germany, Peru, Puerto Rico and Switzerland.  The film was released in the United States on May 24, 2013. 

===Box office===
Produced on a budget of $93 million,   Epic grossed $107,518,682 in North America, and $160,907,952 in other countries, for a worldwide total of $268,426,634.  In North America, the film earned $9.3 million on its opening day,  and opened to number four in its first weekend, with $33,531,068, behind Fast & Furious 6, The Hangover Part III and Star Trek Into Darkness.  In its second weekend, the film dropped to number five, grossing an additional $16,616,310.  In its third weekend, the film stayed at number five, grossing $11,876,003.  In its fourth weekend, the film dropped to number seven, grossing $6,284,905.  While the film was overshadowed by other animated films that summer including Monsters University and Despicable Me 2, the film finished in third out of six family films that summer, and became a moderate box office success. 

===Critical reception=== normalized rating out of 100 top reviews from mainstream critics, calculated a score of 52 out of 100 based on 29 reviews.  Audiences polled by the market research firm CinemaScore gave Epic an "A" grade on average and an "A+" among kids. 

Stephan Lee of   and Avatar (2009 film)|Avatar — but its a perfectly appealing explosion of color for a lazy summer day."  Michael Rechtshaffen of The Hollywood Reporter gave the film a mixed review, saying "Where the animated film comes up short is on the inspiration front -- despite the intriguing terrain, its stock inhabitants lack the sort of unique personality traits that would prevent them from feeling overly familiar."  Michael Phillips of the Chicago Tribune gave the film two stars, saying "Its difficult to keep its story and characters, or even its visual design, in your minds eye, in part because the five credited screenwriters overload the narrative with incident and threatening complication."  Kyle Smith of the New York Post gave the film one star and a half out of four, saying "It’s not that the plot of Epic is complex; it’s just untidy. There’s a lot going on that goes nowhere."  Moira Macdonald of The Seattle Times gave the film three and a half stars out of four, saying "The storys simple enough to appeal to young kids (the 8-year-old with me pronounced the movie "awesome"), but adults will enjoy the beautiful animation, whether 3D or 2D." 
 New York Time Out gave the film a C–, saying "Though technology has made massive leaps in the 15 years since A Bug’s Life, this tiny-creature toon can’t muster anything like the Pixar classic’s sense of wonder at seeing the minute made massive. Wonder requires a point of view—and Epic has none."  Zachary Wigon of The Village Voice gave the film a positive review, saying "With its array of goofy sidekicks (Aziz Ansari as a slug almost runs away with the whole picture) and carefully crafted relationships, Epic certainly manages to tell a compelling tale."  James Berardinelli of ReelViews gave the film two out of four stars, saying "Although the basic story is too juvenile and simplistic to entertain anyone with an age in the double-digit range, the themes and underlying ideas are too complicated to capture the attention of someone younger." 

Stephen Holden of the New York Times gave the film two and a half stars out of five, saying "As beautiful as it is, Epic is fatally lacking in visceral momentum and dramatic edge."  Betsy Sharkey of the Los Angeles Times gave the film two stars and a half out of five, saying "The emotional connection that should have the crowd cheering wildly for the heroes and booing the villains never clicks in."  Steven Rea of The Philadelphia Inquirer gave the two and a half stars out of four, saying "Its one of those - generic entertainment with a brave heroine, cutesy-poo supporting characters, parental figures who are either absent or absent-minded, etcetera, etcetera."  Stephen Whitty of the Newark Star-Ledger gave the film two stars out of four, saying "Anyone over the age of 7 or so - not to mention their accompanying adults - isnt going to find too much here to truly engage them, let alone linger past the final fade out."  Andrew Barker of Variety (magazine)|Variety gave the film a positive review, saying "If this is all familiar territory even to film-literate young children, its nonetheless executed with professionalism and a few dashes of panache."  Glenn Kenny of MSN Movies gave the film two out of five stars, saying "A 3D eco-fantasy whose mantra-like insistence that were all connected by nature is one of the main things that underscores the abject insincerity of the sentiment as the movie articulates it, Epic is very nearly epic in its stifling mediocrity." 

Colin Covert of the Star Tribune gave the film three and a half stars out of four, saying "Epic" may be the thinking familys best Saturday matinee of the summer. And the date movie of the season."  Ignatiy Vishnevetsky of The A.V. Club gave the film a C, saying "Epic is heavy on celebrity voices and light on imagination."  Jenny McCartney of The Daily Telegraph gave the film three out of five stars, saying "The diminution works its appeal once again in Epic – the latest film from the creators of Ice Age and Rio – which is just as well, because the rest of the narrative follows a rather predictable route."  Alonso Duralde of The Wrap gave the film a positive review, saying "When the filmmakers take little detours from the reluctant-warrior-accepts-his/her-destiny plot, it brings some desperately needed livening up to what otherwise feels like a crushingly by-the-numbers kid saga."  Steve Rose of The Guardian gave the film three out of five stars, saying "What stands out is the animation. The microcosmic woodland world is luminous and detailed, and theres a nice disconnect of scale whereby humans appear as lumbering, slow-motion giants."  Tom Russo of The Boston Globe gave the film two stars out of four, saying "Their fantastical great-outdoors adventure is certainly very pretty to look at, and has a general agreeability about it. But the movie would need to engage us far more powerfully for that hyperbolic title to fit." 

===Home media===
Epic was released on DVD, Blu-ray and Blu-ray 3D on August 20, 2013. 

==Accolades==
{| class="wikitable" style="width:95%;"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| rowspan="5"| Annie Awards  February 1, 2014
| Animated Effects in an Animated Production
| Alen Lai, David Quirus, Diego Garzon Sanchez, and Ilan Gabai
|  
|-
| Character Animation in an Animated Feature Production
| Thom Roberts
|  
|- Directing in an Animated Feature Production
| Chris Wedge
|  
|- Music in an Animated Feature Production
| Danny Elfman
|  
|-
| Production Design in an Animated Feature Production William Joyce
|  
|-
| Black Reel Awards 
| 2014
| Outstanding Voice Performance
| Beyoncé Knowles
|  
|- Casting Society of America Awards 
| November 18, 2013
| Outstanding Achievement in Casting - Animation Feature
| Christian Kaplan
|  
|-
| Motion Picture Sound Editors    
| February 16, 2014
| Best Sound Editing in an Animated Feature Film
| Randy Thom, Gwendolyn Yates Whittle
|  
|-
| Producers Guild of America Award  
| January 19, 2014
| Outstanding Producer of Animated Theatrical Motion Picture
| Jerry Davis, Lori Forte
|  
|-
| Satellite Awards  February 23, 2014 Best Motion Picture, Animated or Mixed Media
|
|  
|- Visual Effects Society Awards 
| rowspan=4 | February 12, 2014
| rowspan=2 | Outstanding Animated Character in an Animated Feature Motion Picture
| Bomba (Thom Roberts, Haven Gordon Cousins, Tim Bower, Daniel Lima)
|  
|-
| Mary Katherine (Jeff Gabor, Dylan C. Maxwell, Sang Jun Lee, Chris Pagoria)
|  
|-
| Outstanding Created Environment in an Animated Feature Motion Picture
| Pod Patch (Aaron Ross, Travis Price, Jake Panian, Antelmo Villarreal)
|  
|-
| Outstanding FX and Simulation Animation in an Animated Feature Motion Picture
| Boggan Crowds (Thierry Dervieux-Lecocq, David Gatenby, Mark Adams, Matthew D. Simmons)
|  
|-
| World Soundtrack Academy 
| October 25, 2013
| Film Composer of the Year Promised Land, and Silver Linings Playbook 
|  
|}

==Soundtrack==
{{Infobox album
| Name       = Epic: Original Motion Picture Soundtrack
| Type       = film
| Artist     = Danny Elfman
| Cover      =
| Released   = May 28, 2013
| Recorded   = 2013 Score
| Length     = 52:25
| Label      = Sony Classics
| Producer   =
| Chronology = Danny Elfman film scores
| Last album = Oz the Great and Powerful (2013)
| This album = Epic (2013)
| Next album = American Hustle (2013)
}} Sia an John Powell Ice Age David Newman.
 
; Track listing 
{{tracklist
| all_music = Danny Elfman, except where noted
| total_length = 52:25
| title1 = Leafmen
| length1 = 1:17
| title2 = Pursuit
| length2 = 2:40
| title3 = Tara’s Chamber
| length3 = 3:03
| title4 = Meet Dad
| length4 = 0:34
| title5 = Moonhaven Parade
| length5 = 1:27
| title6 = Alarms
| length6 = 0:41
| title7 = The Selection
| length7 = 2:13  
| title8 = Ambush
| length8 = 4:17
| title9 = Tara’s Gift
| length9 = 2:05
| title10 = Small
| length10 = 2:29
| title11 = Girl Meets Boy
| length11 = 3:24
| title12 = Rings of Knowledge
| length12 = 2:35
| title13 = Antlers
| length13 = 2:10
| title14 = Kidnapped
| length14 = 0:54
| title15 = In the House
| length15 = 3:39
| title16 = Many Leaves
| length16 = 1:54
| title17 = Escape
| length17 = 4:45
| title18 = False Start
| length18 = 3:07
| title19 = Epic Final Confrontation
| length19 = 3:19
| title20 = Return
| length20 = 4:18 
| title21 = Epic Finale
| length21 = 1:34
}}
{{Track listing
| headline        = Digital version
| total_length    = 55:50
| collapsed       = no

| title22 = Rise Up
| length22 = 3:25
| note22 = music by Beyoncé
}}
 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 