Monsoon Shootout
{{Infobox film
| name           = Monsoon Shootout
| image          = Monsoon shootout film poster.jpg
| alt            =
| caption        = Film Poster
| director       =  Amit Kumar
| producer       = Guneet Monga, Anurag Kashyap
| co-producers  = Arun Rangachari, Vivek Rangachari
| writer         =  Amit Kumar
| starring       = Nawazuddin Siddiqui Tannishtha Chatterjee
| music          = Gingger Shankar  
| cinematography = Rajeev Ravi
| editing        = Atanu Mukherjee
| studio         = Sikhya Entertainment, DAR Motion Pictures
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi}}
 Hindi film noir thriller directed by Amit Kumar and produced by Guneet Monga and co-produced by Anurag Kashyap, Arun Rangachari and Vivek Rangachari . The film stars Nawazuddin Siddiqui and Tannishtha Chatterjee in the lead roles. The film was shown during the 2013 Cannes Film Festival in the Midnight Screenings section.  The film is reported to release nationally in India on November 4th, 2014. 

==Cast==
* Nawazuddin Siddiqui
* Tannishtha Chatterjee
* Vijay Varma
* Neeraj Kabi
* Sreejita De

==Production and development== An Occurrence at Owl Creek Bridge, he became interested and fascinated by the idea of human decision-making, and how quick can one make a very difficult decision when ones life is at stake. This idea came to fruition in the form of Monsoon Shootout. 
 The Warrior where he met Trevor Ingman. Ingman decided to help seek finances for Kumars film starting in 2008, but was unable to do so because of the closure of the UK Film Council which was initially supposed to cover half the production costs. Kumar set sights to find an Indian production partner and in 2010, he ran into Guneet Monga who had produced Michael Winterbottoms Trishna (2011 film)|Trishna. When Kumar stated his lack of an Indian producer, Monga immediately jumped at the opportunity to fund the film. However, the producers decided that although they loved Kumars script and idea, they felt he needed to cast a star in the lead role. Kumar stated that "I just wanted to work with good actors". Kumar had the pleasure of working with Nawazuddin Siddiqui in his student short film The Bypass and decided to cast him in the lead role. 

==Cannes reception== Cannes reception for the film has been positive with many international critics taking a liking to the films noir, artistic detailing.

The Guardians Peter Bradshaw gave a very positive first look review of the film stating that its "a brash exploitation picture, a violent thriller on the tough streets of Mumbai about rule-breaking, bone-breaking cops" and "an entertaining popcorn movie with a twist, for which commercial success is on the cards." He described the film as "Dirty Harry meets Sliding Doors." 

 s performance, stating that "Most memorable of all is Siddiqui, who is every inch an unstoppable force of nature, and lucky we are that so much of the violence he wreaks happens off-camera." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 