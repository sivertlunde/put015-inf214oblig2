My Heart Is Mine Alone
 1997 cinema German experimental experimental drama German movie that usually requires more than one screening to decipher and is made for avant-garde devotees." 

==Plot== Jewish poetess Nazi poet Gottfried Benn is told largely through their poetry throughout the film. Lasker-Schüler is forced to leave the country because of the very ideology Benn espouses and while she drifts from country to country en route to Jerusalem, he eventually realizes his mistake when the Nazis condemn his artistic school.

==Cast==
*Lena Stolze as Else Lasker-Schüler
*Thomas Ruffer	as Berthold Lasker
*René Schubert	as Noble
*Katja Ruttloff as Else Lasker-Schülers Sister
*Anna Sanders as Edith Benn
*Cornelius Obonya as Gottfried Benn
*Tomek Schulz as Young Gottfried Benn
*Christian Schlemmer as Wassily Kandinsky
*Janina Berge as Young	Else Lasker-Schüler
*Stefan Ostertag as Franz Marc
*Nicolai Albrecht as Marc Chagall
*Inken Schmitz as Lasker Family Member
*Wolfgang Tebbe as Lasker Family Member
*Dagmar Bertram as Lasker Family Member
*Matthias Wessolek as Fat Cat
*Klaus Bunk as Herwarth Walden
*Sabine Panzer	as Nell Walden
*Oliver Grice as Gustav Benn
*Lothar von Versen as Peter Hille
*Julia Kiessling as Else Lasker-Schülers Sister
*Leonard Schnitman as Paul Schüler
*Nikolai Sirenko as Aaron Schüler
*Valentina Sirenko as Jeannette Schüler
*Bruno Dunst as Professor

==Release==
The film was released on DVD by Facets Multi-Media in 2008. 

==Reception== Berlin International Expressionist vampire,"  and critic Ed Soohoo, who wrote that it is "wonderful to see" Lena Stolze "once again on screen as she brings life to Else."  Critic Peter Nellhaus has praised the films "expressionist collage of conventional biographical re-enactment, stylized staging, and documentary" and wrote that he regards it as a "truthful film." 

==References==
 

==External links==
* 
* 
*  TCM Movie Database Film Portal  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 