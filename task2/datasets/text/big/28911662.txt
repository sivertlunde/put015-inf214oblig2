Oppol
{{Infobox film
| name           = Oppol
| image          = oppol.jpg
| image_size     = Menaka as Malu and Aravind as Appu.
| director       = K. S. Sethumadhavan
| producer       = Rosamma George
| writer         = M. T. Vasudevan Nair
| narrator       = Menaka Master Aravind Shankaradi Kaviyoor Ponnamma
| music          = M. B. Sreenivasan
| cinematography = Madhu Ambat
| editing        = G.Venkitaraman
| studio         =
| distributor    = Angel Films
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Oppol ( ), meaning Elder Sister, is a 1980 Malayalam film written by M. T. Vasudevan Nair and directed by K. S. Sethumadhavan. Balan K. Nair, Menaka (actress)|Menaka, Master Aravind, Kaviyoor Ponnamma and Shankaradi comprised the major cast. The film was based on a short story of the same name written by M. T. in 1975.    
 
==Plot==
The film revolves around Malu (Menaka (actress)|Menaka), her younger brother Appu (Aravind) and Malus husband Govindan (Balan K. Nair). Malu and 6-year old Appu were living together. When Malu is married to Govindan, an ex-military officer, she takes Appu with her to Govindans house. Govindan is a bit annoyed, but adjusts to the situation to win over his wife, who is much younger to him. Appu on the other hand becomes jealous of Govindan and worries he will isolate him from his dear sister. He attacks Govindan during honeymoon and Malu scolds him. The boy runs away from the house and Malu becomes distressed. Towards the end it is revealed that Appu is actually the son of Malu. When Govindan discovers the truth, he tirelessly searches for the boy and brings him back.

==Cast== Menaka
*Balan K Nair
*Master Aravind
*Kaviyoor Ponnamma
*Jose Prakash
*Sankaradi

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by P. Bhaskaran.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chaattal Mazhayum || Chorus, Malathi, Latha Devi || P. Bhaskaran ||
|-
| 2 || Ettumaanoorambalathil Ezhunnallathu || S Janaki || P. Bhaskaran ||
|-
| 3 || Ettumanoorambalathil (Plain) (Bit) || S Janaki || P. Bhaskaran ||
|-
| 4 || Pottikkaan Chennappol || K. J. Yesudas || P. Bhaskaran ||
|}

==Awards==
* National Film Award for Best Actor for Balan K. Nair    
* National Film Award for Best Child Artist for Aravind  
* National Film Award for Best Female Playback Singer for S. Janaki (song: "Ettumanoor Ambalathile")  
* Kerala State Film Award for Best Film
* Kerala State Film Award for Best Director for K. S. Sethumadhavan. 

==References==
 

==External links==
* 
*  
*  

 
 

 
 
 
 
 
 