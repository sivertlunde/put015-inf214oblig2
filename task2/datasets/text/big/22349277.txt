Brighty of the Grand Canyon
{{Infobox book
| name          = Brighty of the Grand Canyon
| image         = 
| caption = 
| author        = Marguerite Henry Wesley Dennis
| cover_artist  = 
| country       = United States English
| genre         = 
| publisher     = Rand McNally (1953) 
Aladdin (1991)
| pub_date      = November 1953 
| pages         = 222
| isbn          = 978-0-689-71485-6 (second printing)
| oclc          = 305533
| preceded_by   = 
| followed_by   =
}}
{{Infobox film
| name           = Brighty of the Grand Canyon
| image          =
| caption        =
| producer       = Stephen F. Booth Productions Norman Foster
| writer         = Marguerite Henry Norman Foster (teleplay)
| starring       = Joseph Cotten Karl Swenson Pat Conway Dick Foran Dandy Curran
| music          =
| cinematography =
| editing        =
| distributor    = Feature Film Corporation of America Western drama
| released       =  
| runtime        = 89 minutes English
}}
Brighty of the Grand Canyon is a 1953 childrens novel by Marguerite Henry and a 1967 film of the same name based on the novel. They present a fictionalized account of a real-life burro named "Brighty", who lived in the Grand Canyon of the Colorado River from about 1892 to 1922. 

==History==
Brighty first appears in the annals of history in 1890 in Flagstaff, Arizona, in the possession of two men who were on their way to the Grand Canyon. He is next recorded on the South Rim where he and his owners were seen to enter the Canyon via the Bright Angel Trail.  Shortly after that two herdsmen entered the canyon to attempt to find the remains of an earlier drowning victim along the Colorado.  Instead, they found a camp at the confluence of the Colorado and  Bright Angel Creek that was abandoned with the exception of Brighty. It appeared that the two men had saddled and ridden their horses down to and into the Colorado wherein they presumably drowned. Their identities were never determined and their bodies were never recovered. 
 North Rim, Brighty spent summers carrying water from a spring below the rim to accommodate tourists coming to the Canyon. He was gentle and popular with children. 

Brighty was the first to cross the suspension bridge built over the Colorado River at the base of the canyon, having helped in the building of the structure. The burro accompanied U.S. President Theodore Roosevelt while he hunted mountain lions.    (The role of Roosevelt is portrayed in the film by Karl Swenson.) 

==Book and film==
Henry penned her novel after she read an article about the Brighty in Sunset Magazine. It won the 1956 William Allen White Childrens Book Award.

Thomas McKee, the former manager of Wileys Camp on the North Rim of the Canyon, read Henrys novel and wrote to express his interest in the book. McKee told Henry that his son, Bob, was Brightys closest companion. He sent Henry a photograph of young Bob McKee sitting on Brightys back. The youngster hence became the composite character Homer Hobbs,  played in the film by Dandy Curran.   
 prospector played by Dick Foran and Joseph Cotten as Uncle Jim Owen.  Pat Conway appears as Jake Irons, who murders Old Timer for his copper ore. Uncle Jim then proceeds to help bring Irons to justice. 

==Brightys monument==
Brighty is honored with a bronze statue in the lobby of Grand Canyon Lodge,  a National Historic Landmark, {{cite web|url=http://tps.cr.nps.gov/nhl/detail.cfm?ResourceId=1846&ResourceType=Building
|title=Grand Canyon Lodge|accessdate=2010-02-01|work=National Historic Landmark summary listing|publisher=National Park Service}}  located near Arizona State Route 67 approximately 43 miles south of the junction with U.S. Route 89 alternate route. The monument of stone walls and timbers has a memorial inscription written by Marguerite Henry: "the artist captured the soul of Brighty, forever wild, forever free." 

==References==
 

 
 
 
 
 
 
 
 
 
 
 