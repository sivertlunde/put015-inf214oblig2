Kamen Rider ZO
{{Infobox film 
| name           = Kamen Rider ZO
| film name = {{Film name| kanji = 仮面ライダーZO
| romaji    = Kamen Raidā Zetto Ō}}
| director       = Keita Amemiya
| writer         = Noboru Sugimura Toei   Bandai
| starring       = Kou Domon Shohei Shibata Isao Sasaki
| music          = Eiji Kawamura Toei Co. Ltd 
| released       =  
| runtime        = 48 minutes
| language       = Japanese
}}

 , translated as Masked Rider ZO, is a 1993 Japanese tokusatsu movie produced by Toei Company, part of their Kamen Rider Series.   Directed by Keita Amemiya, the film was the first joint production between Toei Company Limited and Bandai.

A Sega CD full motion video based game was released for ZO in 1994,  and made its way to the United States in the form of The Masked Rider: Kamen Rider ZO.

As part of the 40th anniversary of the Kamen Rider Series, ZO was shown on Toeis pay-per-view channel during September 2011.  The films protagonist, Kamen Rider ZO makes appearances in the films of the later Kamen Rider Decade television series,  as well as appearing as a playable character the 2011 Nintendo DS video game All Kamen Rider: Rider Generation. 

==Plot==
  telepathic call and with an unconscious urge to protect Hiroshi Mochizuki, the son of Doctor Mochizuki. After an attempt to uncover the meaning of his transformation at Mochizuki Genetics, Masaru senses Hiroshi in danger and saves the boy from Doras as ZO. Masaru then reveals himself to Reiko and her karate class. ZO battles Koumori Man to cover Hiroshi and Reikos escape, when the two of them are sucked in a pocket dimension by Kumo Woman, both monsters having been created by Doras. ZO saves them and kills Kumo Woman before Koumori Man swoops down and snatches Hiroshi off, with ZO in pursuit. After saving Hiroshi, Masaru reveals to Seikichi that Dr. Mochizuki used him in his experiments. Refusing to believe it, Hiroshi runs off before Masaru finds him and fixes his watch, recognizing the melody that stirred him out of his rest as he helps Hiroshi cope with this new information. However, Koumori Man assumes Mochizukis form to lure Hiroshi away and captures him with Doras knocking Masaru out cold. Making his way to a complex, ZO kills Koumori Man before making his way to Hiroshi and Dr. Mochizuki, learning that the geneticist was the one who woke him up and that the Neo Organism has been acting on its own whim the entire time to become the perfect being. ZO attempts to fight Doras, only to be assimilated into the Neo Organism. Doras then proceeds to use the boy to force Mochizuki to complete its evolution. However, the watch manages to hold Doras at bay as ZO breaks out of the monster and Mochizuki sacrifices himself to destroy the pool, the Neo Organisms life source. The complex then self detonates as ZO and Hiroshi escape with their lives. Dropping Hiroshi with Seikichi, Masaru leaves to parts unknown.

==Characters==
*  : The hero of the story, Masaru was originally the assistant to Doctor Mochizuki until the doctor conducted experiments on him. Horrified at being made into a grasshopper-like cyborg, Masaru went into hiding in the mountains for two years and fell into a coma until he awoke with an unconscious urge to protect Hiroshi. Although he hates Mochizuki, Masaru eventually forgives him before learning the man was telepathically communicating with him. As Kamen Rider ZO, his only attacks are a Rider Punch called the   and a Rider Kick called the  . ZO also rides on the  , built by Dr. Mochizuki. Though resembling a regular motorcycle, the Z-Bringer changes along with ZO with ability to reach speeds up to   and resist heat up to 1,000 degrees. ZO can use the Z-Bringer to execute the   Rider Break. Another of ZOs features within his insectoid head called a  , a breathing apparatus stored around the mouthplate that shoots out vapors, baring resemblance to fangs when extended. An early design for Kamen Rider ZO had the hero wearing the trademark scarf of early Shōwa Kamen Riders, along with a more-traditional transformation belt design. His origins were also unrelated to the creation by Doctor Mochizuki. 
*  : A young boy who lives with his grandfather Seikichi and is targeted by the Neo Organism. Hiroshi longs to be with his father, who had gone missing years ago. As a memento, Doctor Mochizuki gave Hiroshi a pocket watch that played music. Hiroshi is at first afraid of ZO, but as ZO proves he truly was a friend, Hiroshi affectionately refers to ZO as "Onii-chan" or "brother".
*  : A mad scientist who turned Masaru into ZO and then created the Neo Organism. His dream was to create the "perfect lifeform", but instead it turned on him and was held captive by his creation. He uses grasshoppers as a means to contact ZO via telepathy.

===Neo Organism===
  is a pool of living fluid that relies on the very container its dwells in as its life support. It takes on the appearance of a deformed little boy in Hiroshis likeness, lacking emotion and having a god complex. The Neo Organism orchestrated the events in the movie and held its creator captive for two years when he refused to complete its evolution. It soon assumes the grotesque Kamen Rider-like form of  , to find Hiroshi to force Dr. Mochizuki to complete its evolution into the "Perfect Lifeform" so it can destroy the "inferior" human race and their imperfections. Doras is capable of regenerating itself from severe injuries, absorbing inorganic and organic materials to upgrade itself, altering its form into an orb or a bladed projective, generating lasers, and producing   from itself. When Doras manages to absorb ZO, it becomes a red fleshy version of itself called  . It then goes after Hiroshi, but the music from the pocket watch manages to give ZO the strength to break free from Doras body after Mochizuki destroys its life support. Reverting to its original state, Doras is finally killed by ZOs Rider Kick, the ZO Kick.

*  : A Doras Monster created by Doras, she is a large 4-legged inhuman spider woman who shot strong webbing to entangle Hiroshi. She also has a set of spider legs on her back and two spider legs for a left arm. During combat with the monster, ZO snapped one of Kumo Womans legs and used it to impale and instantly kill her. She was the only Kamen Rider monster to be animated by claymation and be a large puppet.

*  : A Doras Monster created by Doras, he is a black bat-like entity. Koumori Man is black, has small-but-long bat wings for ears that cover its head (unless he is flying), and an eye on each hand much like Doras. Koumori Man can extend its fingers to form wings for flight. It can also disguise itself as any human. It takes on the likeness of Mochizuki to trick Hiroshi and take him to its masters hideout. ZO kills Koumori Man easily with a ZO Punch by bursting into the monsters stomach.

==Cast==
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  

==Song==
* 
**Lyrics: Akira Ōtsu
**Composition: Eiji Kawamura
**Artist: infix
* 
**Lyrics: Akira Ōtsu
**Composition: Eiji Kawamura
**Artist: infix

==ZO vs. J==

The Super Imaginative Chogokin|S.I.C. Hero Saga story published in Monthly Hobby Japan magazine in the February to May 2005 issues for Kamen Rider ZO featured a crossover with Kamen Rider J and was titled  . In the story, the Neo Organism Doras gains the powers of the Fog Mother. It introduces the original characters  ,  , and  .

Chapter titles
# 
# 
# 
# 

==Adaptation== Ishinomori himself.

The movie was adapted into a full-motion   (the Neo Organisms dialogue is difficult to understand) as well as some scenes from the entire film being cut from the game.

==Adaption In Sabans Masked Rider==
  and Koumori Man became "Parasect" and appeared in the episode Cat-Atomic.

==References==
 

==External links==
*  

 

 
 
 
  