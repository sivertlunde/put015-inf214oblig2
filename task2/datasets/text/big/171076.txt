Mr. Holland's Opus
{{Infobox film
| name           = Mr. Hollands Opus
| image          = Mr Hollands Opus.jpg
| caption        = Theatrical release poster
| director       = Stephen Herek
| producer       = Ted Field Robert W. Cort Michael Nolin Patrick Sheane Duncan
| writer         = Patrick Sheane Duncan
| starring       = Richard Dreyfuss Glenne Headly Jay Thomas Olympia Dukakis William H. Macy Alicia Witt

| music          = Michael Kamen Oliver Wood
| editing        = Trudy Ship
| studio         = Hollywood Pictures Interscope Communications PolyGram Filmed Entertainment Buena Vista Pictures   Metro-Goldwyn-Mayer  
| released       =   
                     
| runtime        = 143 minutes
| country        = United States
| language       = English and American Sign Language
| budget         = 
| gross          = $106,269,971
}}
Mr. Hollands Opus is a 1995 American drama film directed by Stephen Herek, produced by Ted Field, Robert W. Cort, and Michael Nolin, and written by Patrick Sheane Duncan.  It stars Richard Dreyfuss in the title role, and the cast includes Glenne Headly, Olympia Dukakis, William H. Macy and Jay Thomas.
 Best Screenplay Best Actor – Motion Picture Drama (Dreyfuss), while the actor was also nominated for the Academy Award for Best Actor.

==Plot== orchestral music, the 30-year-old Holland accepts a teaching position.

Unfortunately for Holland, he is soon forced to realize that his position as a music teacher makes him a marginalized figure in the facultys hierarchy. He comes face to face with how seriously he is outranked by the high schools football coach, Bill (Jay Thomas), who ultimately becomes his best friend. Administrators, such as vice principal Gene Wolters (William H. Macy), dislike him, while others, including principal Helen Jacobs (Olympia Dukakis), remind him that he should not teach just because of financial reasons. It is Mrs. Jacobs scolding that helps Holland turn a corner. He starts to use rock and roll as a way to help children understand classical music. Reluctantly, he begins seeing his students as individuals and finds ways to help them excel.

When Iris becomes pregnant, Holland uses the money saved up for his orchestrating to buy a house. Their son Cole is born sometime during the summer after his first year of teaching. Holland is then assigned to be in charge of the school marching band. Bill helps him in exchange for allowing academically challenged football player/wrestler Louis Russ (Terrence Howard) to play the drums for academic credit, so he can keep his spot on the wrestling team.

The film marks the passing decades with newsreels about Vietnam, corresponding to the tragic combat death of Louis, and the death of John Lennon in 1980. The passage of time and the mysteries of personal growth are a frequent underlying theme in this film.

Hollands lack of quality time with his wife becomes problematic when their son, Cole, is diagnosed as deaf. Holland reacts with hostility to the news that he can never teach the joys of music to his own child. His wife willingly learns American Sign Language to communicate with their son, but Holland learns at a much slower rate, causing further estrangement within the family.
 Beautiful Boy, directing the song towards Cole.

Holland addresses a series of challenges created by people who are either skeptical of, or hostile towards, the idea of musical excellence within the walls of the average middle-class American high school. He inspires many students, but never has time for himself or his family, forever delaying the composition of his own orchestral work. Ultimately, he reaches an age when it is too late to realistically find financial backing or ever have it performed.

In 1995, the adversaries of the Kennedy High music program win a decisive institutional victory. Hollands longtime adversary Gene Wolters, assigned school principal when Jacobs retired, works with the school board to eliminate music, along with the rest of the fine arts program, in the name of necessary budget cuts, thereby leading to Glenns early retirement at the age of 60. Glenn is a realist who realizes that his working life is over. He believes that his former students have mostly forgotten him.

On his final day as a teacher, Iris and an adult Cole (who is now a teacher himself) arrive to help Holland pack up. Feeling despondent over his self-perceived lack of achievement, Holland is led to the school auditorium, where his professional life is surprisingly redeemed. Hearing that their beloved teacher is retiring, hundreds of his former pupils have secretly returned to the school to celebrate his career.

Hollands orchestral piece, never before heard in public, has been put before the musicians by his wife and son. One of his most musically challenged students, Gertrude Lang (Alicia Witt as a child and Joanna Gleason as an adult), who has become governor of the state, sits in with her clarinet. Gertrude and the other alumni ask the retiring teacher to serve as their conductor for the premiere performance of Mr. Hollands Opus ("The American Symphony"). A proud Iris and Cole look on, appreciating the affection and respect that Holland receives.

==Cast==
* Richard Dreyfuss as Glenn Holland
* Glenne Headly as Iris Holland
* Jay Thomas as Bill Meister
* Olympia Dukakis as Principal Helen Jacobs
* William H. Macy as Vice Principal (later Principal) Gene Wolters
* Alicia Witt as student Gertrude Lang
* Joanna Gleason as adult Governor Gertrude Lang
* Terrence Howard as Louis Russ
* Damon Whitaker as Bobby Tidd
* Jean Louisa Kelly as Rowena Morgan
* Alexandra Boyd as Sarah Olmstead
* Nicholas John Renner as Coltrane "Cole" Holland (age 6)
* Joseph Anderson as Coltrane "Cole" Holland (age 15)
* Anthony Natale as Coltrane "Cole" Holland (age 28)
* Beth Maitland as Deaf School Principal
* Balthazar Getty as Stadler

==Production== Ulysses S. Grant High School.   

===Archive footage===
Archive footage seen in the film includes:
* Martin Luther King, Jr.s speech Robert and John F. Kennedy
* Woodstock (film)|Woodstock
* Vietnam War
* The Rocky Horror Picture Show
* Saturday Night Fever
* Stop Making Sense
* Death of John Lennon
* Disco Demolition Night at Comiskey Park

==Music== classical music. Kamen also wrote An American Symphony, the work Mr. Holland is shown working on throughout the movie.

==Soundtrack releases==
Two soundtrack albums were released for this film in January 1996. One is the original motion picture film score|score, and includes all of the original music written for the film by Michael Kamen. The second album is a collection of popular music featured in the film:

# "Visions of a Sunset" – Shawn Stockman (of Boyz II Men)
# "1-2-3 (Len Barry song)|1-2-3" – Len Barry
# "A Lovers Concerto" – The Toys
# "Keep On Running" – Spencer Davis Group
# "Uptight (Everythings Alright)" – Stevie Wonder
# "Imagine (John Lennon song)|Imagine" – John Lennon The Pretender" – Jackson Browne Someone to Watch Over Me" – Julia Fordham
# "I Got a Woman" – Ray Charles
# "Beautiful Boy (Darling Boy)" – John Lennon
# "Coles Song" – Julian Lennon & Tim Renwick
# "An American Symphony (Mr. Hollands Opus)" – London Metropolitan Orchestra & Michael Kamen

==Reception==

===Box office===
In the United States, gross domestic takings totaled US$ 82,569,971. International takings are estimated at US$ 23,700,000, for a gross worldwide takings of US$106,269,971.  Rental totals reached US$ 36,550,000 in the US. Although the film is included amongst 1995 box office releases (it ranks as the 14th most successful film of that year), it was only released in a few theatres in New York and Los Angeles on December 29, 1995, because Disney felt, accurately, that Richard Dreyfuss performance had a good chance of getting an Oscar nomination if it beat that years in-theatre deadline.

===Critical===
Rotten Tomatoes gave the film a 74% Fresh rating.    Writer Patrick Sheane Duncan was nominated for the Golden Globe Award for Best Screenplay at the 53rd Golden Globe Awards. Dreyfuss was nominated for the Academy Award for Best Actor and the Golden Globe Award for Best Actor – Motion Picture Drama.

===The Mr. Hollands Opus Foundation===
Inspired by the motion picture, its composer, Michael Kamen, founded The Mr. Hollands Opus Foundation (MHOF) in 1996 as his commitment to the future of music education.   

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 