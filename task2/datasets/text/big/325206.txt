The Little Shop of Horrors
 Little Shop of Horrors}}
 
{{Infobox film
| name           = The Little Shop of Horrors
| image          = LittleShop.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Roger Corman
| producer       = Roger Corman
| writer         = Charles B. Griffith
| narrator       = Wally Campo
| starring       = Jonathan Haze Jackie Joseph Mel Welles Dick Miller Jack Nicholson Fred Katz Ronald Stein
| cinematography = Archie R. Gazelle  Vilis Lapenieks
| editing        = Marshall Neilan, Jr.
| studio         = Filmgroup
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = $28,000 Fred Olen Ray, The New Poverty Row: Independent Filmmakers as Distributors, McFarland, 1991, p 28-29  25,066 admissions (France) 
}}
 John Collier, about a man-eating plant.   However, Dennis McDougal in Jack Nicholsons biography suggests that Charles B. Griffith may have been influenced by Arthur C. Clarkes sci-fi short story, "The Reluctant Orchid.   The film stars Jonathan Haze, Jackie Joseph, Mel Welles and Dick Miller, all of whom had worked for Corman on previous films. Produced under the title The Passionate People Eater,       the film employs an original style of humor, combining black comedy with farce    and incorporating Jewish humor and elements of parody film|spoof.    The Little Shop of Horrors was shot on a budget of $30,000 in two days utilizing sets that had been left standing from A Bucket of Blood.            
 Black Sunday Little Shop 1986 feature film and enjoyed a Broadway revival, all of which have attracted attention to the 1960 film.

==Plot==
  Los Angeles Shiva (Leola butterwort and a Venus flytrap. Bashfully, Seymour admits that he named the plant "Audrey Jr.",  a revelation that delights the real Audrey.

From the apartment he shares with his hypochondriac mother, Winifred (Myrtle Vail), Seymour fetches his odd-looking, potted plant, but Mushnick is unimpressed by its sickly, drooping look. However, when Fouch suggests that Audrey Jr.s uniqueness might attract people from all over the world to see it, Mushnick gives Seymour one week to revive it. Seymour has already discovered that the usual kinds of plant food do not nourish his strange hybrid and that every night at sunset the plants leaves open up. When Seymour accidentally pricks his finger on another thorny plant, Audrey Jr. opens wider, eventually causing Seymour to discover that the plant craves blood. After that, each night Seymour nurses his creation with blood from his fingers. Although he feels increasingly listless, Audrey Jr. begins to grow and the shops revenues increase due to the curious customers who are lured in to see the plant.
 anemic and not knowing what to feed the plant, Seymour takes a walk along a railroad track. When he carelessly throws a rock to vent his frustration, he inadvertently knocks out a man who falls on the track and is run over by a train. Miserably guilt-ridden but resourceful, Seymour collects the body parts and feeds them to Audrey Jr. Meanwhile at a restaurant, Mushnick discovers he has no money with him, and when he returns to the shop to get some cash, he secretly observes Seymour feeding the plant. Although Mushnick intends to tell the police, he procrastinates by the next day when he sees the line of people waiting to spend money at his shop.

When Seymour later arrives that morning suffering a toothache, Mushnick sends Seymour to Dr. Farb, who tries to remove several of his teeth without anesthetic to get even with Seymour for ruining Farbs flowers. Grabbing a sharp tool, Seymour fights back and accidentally stabs and kills Farb. Seymour is horrified that he has now murdered twice and after posing as a dentist to avoid the suspicion of Farbs masochistic patient Wilbur Force (Jack Nicholson), Seymour feeds Farbs body  to Audrey Jr. The unexplained disappearance of the two men attract the attention of the police and Mushnick finds himself questioned by Det. Joe Fink (Wally Campo) and his assistant Sgt. Frank Stoolie (Jack Warford) (take-offs of Dragnet (TV series)|Dragnet characters Joe Friday and Frank Smith, ). Although Mushnick acts suspiciously nervous, Fink and Stoolie conclude that he knows nothing. Audrey Jr., which has grown several feet tall, is beginning to bud, as is the relationship between Seymour and Audrey (whom Seymour invites on a date).
 
When a representative of the Society of Silent Flower Observers of Southern California comes to the shop to check out the plant, she announces that Seymour will soon receive a trophy from them and that she will return when the plants buds open. While Seymour is on a date with Audrey, Mushnick stays at the shop to see that Audrey Jr. eats no one else. After trading barbs with the plant when Audrey awakens and requests to be fed, Mushnick find himself at the mercy of a robber (Charles B. Griffith) who believes that the huge crowd he had observed attending the shop indicated the presence of a large amount of money. To save his own life, Mushnick tricks the robber into thinking that the money is at the bottom of the plant who then eats him. Not only does the monstrous plants growth increase with this latest meal, but its intelligence and abilities do as well. It intimidates Mr. Mushnick, who is now more terrified than ever, but not so much that he will pass up on the money the plant is bringing in as an attraction. After he is forced to damage his relationship with Audrey to keep her from discovering the plants nature, an angry Seymour confronts the plant asserting he will no longer do its bidding just because it orders him. The plant then employs hypnosis on the feckless lad and commands him to bring it more food. He wanders the night streets aimlessly until pursued by a rather aggressively persistent high-end call girl, Leonora Clyde (Meri Welles), intent on making a score. Believing him harmless, she flirts with him to no avail until he inadvertently knocks her out with a rock and carries her back to feed Audrey Jr.

Still lacking clues about the mysterious disappearances of the two men, Fink and Stoolie attend a special sunset celebration at the shop during which Seymour is to be presented with the trophy and Audrey Jr.s buds are expected to open. As the attendees look on, four buds open and inside each flower is the face of one of the plants victims.  As the crowd breaks out in shock and fright, Fink and Stoolie realize Seymour is their culprit who flees from the shop with the police and Mushnick in hot pursuit.  Managing to lose them in a junk yard filled with sinks and toilets, Seymour eventually makes his way back to Mushnicks shop where Audrey Jr. is screaming to be fed. Blaming the plant for ruining his life, Seymour grabs a knife and climbs into Audrey Jr.s mouth in an ill thought out attempt to kill it.

Some time later; Audrey, Winifred, Mushnick, Fink, and Stoolie return to the shop where Audrey Jr. has begun to wither and die. As Winifred laments over how her son used to be such a good boy, the final bud opens to reveal the face of Seymour who pitifully moans "I didnt mean it!" before drooping over.

==Cast==
* Jonathan Haze as Seymour Krelboyne
* Jackie Joseph as Audrey Fulquard
* Mel Welles as Gravis Mushnick
* Dick Miller as Burson Fouch
* Myrtle Vail as Winifred Krelboyne
* Tammy Windsor as Shirley
* Toby Michaels as Shirleys friend
* Leola Wendorff as Mrs. Siddie Shiva
* Lynn Storey as Mrs. Hortense Fishtwanger
* Wally Campo as Detective Sergeant Joe Fink/Narrator
* Jack Warford as Detective Frank Stoolie
* Meri Welles as Leonora Clyde (credited as Merri Welles)
* John Shaner as Dr. Phoebus Farb
* Jack Nicholson as Wilbur Force
* Dodie Drake as Waitress
* Charles B. Griffith as Voice of Audrey Junior, Kloy, Drunk dental patient, Screaming patient, Flower shop robber
===Casting process===
Jackie Joseph was cast after Roger Corman saw her in a popular revue called the Billy Barnes Revue. He offered her the role without having to audition. "I heard Roger say afterwards that the quality he wanted in Audrey was sincere innocence," said Joseph. "Audrey just believed everything that was put in front of her."   accessed 18 April 2014 

==Development==
The Little Shop of Horrors was developed when director Roger Corman was given temporary access to sets that had been left standing from a previous film. Corman decided to use the sets in a film made in the last two days before the sets were torn down.     

Corman initially planned to develop a story involving a private investigator. In the initial version of the story, the character that eventually became Audrey would have been referred to as "Oriole Plove". Actress Nancy Kulp was a leading candidate for the part.  The characters that eventually became Seymour and Winifred Krelboyne were named "Irish Eye" and "Iris Eye."  Actor Mel Welles was scheduled to play a character named "Draco Cardala", Jonathan Haze was scheduled to play "Archie Aroma," and Jack Nicholson would have played a character named "Jocko". 

Charles B. Griffith wanted to write a horror film|horror-themed comedy film. According to Mel Welles, Corman was not impressed by the box office performance of A Bucket of Blood, and had to be persuaded to direct another comedy.  

Corman later claimed he was interested because of A Bucket of Blood and said the development process was similar to that of the earlier film, when he and Griffith were inspired by visiting various coffee houses. Corman:
 We tried a similar approach for The Little Shop of Horrors, dropping in and out of various downtown dives. We ended up at a place where Sally Kellerman (before she became a star) was working as a waitress, and as Chuck and I vied with each other, trying to top each other’s sardonic or subversive ideas, appealing to Sally as a referee, she sat down at the table with us, and the three of us worked out the rest of the story together.   accessed 20 April 2014  
The first screenplay Griffith wrote was Cardula, a Dracula-themed story involving a vampire music critic.  

After Corman rejected the idea, Griffith says he wrote a screenplay titled Gluttony,  in which the protagonist was "a salad chef in a restaurant who would wind up cooking customers and stuff like that, you know? We couldn’t do that though because of the code at the time. So I said, “How about a man-eating plant?”, and Roger said, “Okay.” By that time, we were both drunk." 

Jackie Joseph later recalled "at first they told me it was a detective movie; then, while I was flying back  , I think they wrote a whole new movie, more in the horror genre. I think over a weekend they rewrote it."  

The screenplay was written under the title The Passionate People Eater.    Welles stated, "The reason that The Little Shop of Horrors worked is because it was a love project. It was our love project." 

==Production==
 
The film was partially cast with stock actors that Corman had used in previous films. Writer Charles B. Griffith portrays several small roles. Griffiths father appeared as a dental patient, and his grandmother, Myrtle Vail appeared as Seymours hypochondriac mother.   Dick Miller, who had starred as the protagonist of A Bucket of Blood was offered the role of Seymour, but turned it down, instead taking the smaller role of Burson Fouch.   The cast rehearsed for three weeks before filming began.  Principal photography of The Little Shop of Horrors was shot in two days and one night.   
 residuals for all future releases of their work. This meant that Cormans B-movie business model would be permanently changed and he would not be able to produce low-budget movies in the same way. Before these rules went into effect, Corman decided to shoot one last film and scheduled it to happen the last week in December 1959.   

Interiors were shot with three cameras in wide, lingering master shots in single takes.   Welles states that Corman "had two camera crews on the set—thats why the picture, from a cinematic standpoint, is really not very well done. The two camera crews were pointed in opposite directions so that we got both angles, and then other shots were picked up to use in between, to make it flow. It was a pretty fixed set and it was done sort of like a sitcom is done today, so it wasnt very difficult." 

At the time of shooting, Jack Nicholson had only appeared in two film roles, and had only worked with Roger Corman once, as the lead in The Cry Baby Killer. According to Nicholson, "I went in to the shoot knowing I had to be very quirky because Roger originally hadnt wanted me. In other words, I couldnt play it straight. So I just did a lot of weird shit that I thought would make it funny."  According to Dick Miller, all of the dialogue between his character and Mel Welles was Ad libitum|ad-libbed.  During a scene in which writer Charles B. Griffith played a robber, Griffith remembers that "When   and I forgot my lines, I improvised a little, but then I was the writer. I was allowed to."  However, Welles states that "Absolutely none of it was ad-libbed   every word in Little Shop was written by Chuck Griffith, and I did ninety-eight pages of dialogue in two days." 

According to Nicholson, "we never did shoot the end of the scene. This movie was pre-lit. Youd go in, plug in the lights, roll the camera, and shoot. We did the take outside the office and went inside the office, plugged in, lit and rolled. Jonathan Haze was up on my chest pulling my teeth out. And in the take, he leaned back and hit the rented dental machinery with the back of his leg and it started to tip over. Roger didnt even call cut. He leapt onto the set, grabbed the tilting machine, and said Next set, thats a wrap."  By 9&nbsp;am of the first day, Corman was informed by the production manager that he was behind schedule. 

Exteriors were shot by Griffith and Welles over two successive weekends with $279 worth of rented equipment.   Griffith and Welles paid a group of children five cents apiece to run out of a subway tunnel.  They were also able to persuade winos to appear as extras for ten cents apiece.   "The winos would get together, two or three of them, and buy pints of wine for themselves! We also had a couple of the winos act as ramrods—sort of like production assistants—and put them in charge of the other wino extras."  Griffith and Welles also persuaded a funeral home to donate a hearse and coffin—with a real corpse inside—for the film shoot.  Griffith and Welles were able to use the nearby Southern Pacific Transportation Company yard for an entire evening using two bottles of scotch as persuasion.  The scene in which a character portrayed by Robert Coogan is run over by a train was accomplished by persuading the railroad crew to back the locomotive away from the actor. The shot was later printed in reverse.  Griffith and Welles spent a total of $1,100 on fifteen minutes worth of exteriors.  

The films musical score, written by cellist   and Creature from the Haunted Sea.   

Howard R. Cohen learned from Charles B. Griffith that when the film was being edited, "there was a point where two scenes would not cut together. It was just a visual jolt, and it didnt work. And they needed something to bridge that moment. They found in the editing room a nice shot of the moon, and they cut it in, and it worked. Twenty years go by. Im at the studio one day. Chuck comes running up to me, says, Youve got to see this! It was a magazine article—eight pages on the symbolism of the moon in Little Shop of Horrors."  According to Corman, the total budget for the production was $30,000.  Other sources estimate the budget to be between $22,000 and $100,000.   

== Release and reception ==
 

=== Release history ===
Corman had initial trouble finding distribution for the film, as some distributors, including American International Pictures, felt that the film would be interpreted as anti-Semitic, citing the characters of Gravis Mushnick and Siddie Shiva.         Welles, who is Jewish, stated that he gave his character a Turkish Jewish accent and mannerisms, and that he saw the humor of the film as playful, and felt there was no intent to defame any ethnic group.  The film was finally released by Cormans own production company, The Filmgroup Inc., nine months after it had been completed. 
 Black Sunday.  Little Shop of Horrors was re-released the following year in a double feature with The Last Woman on Earth. 

Because Corman did not believe that The Little Shop of Horrors had much financial prospect after its initial theatrical run, he did not bother to copyright it, resulting in the film falling into the public domain.   
   Because of this, the film is widely available in copies of varying quality. The film was originally screened theatrically in the widescreen aspect ratio of 1.85:1, but has largely only been seen in open matte at an aspect ratio of 1.33:1 since its original theatrical release. 

=== Critical and audience reception ===
The films critical reception was largely favorable, with modern review aggregate Rotten Tomatoes giving the film a "Tomatometer" score of 91%.  Variety (magazine)|Variety wrote, "The acting is pleasantly preposterous.   Horticulturalists and vegetarians will love it." 

Jack Nicholson, recounting the reaction to a screening of the film, states that the audience "laughed so hard I could barely hear the dialogue. I didnt quite register it right. It was as if I had forgotten it was a comedy since the shoot. I got all embarrassed because Id never really had such a positive response before." 

=== Legacy ===
The films popularity slowly grew with local television broadcasts throughout the 1960s and 1970s.   
 Little Shop Little Shop of Horrors, in 1986.  An animated television series inspired by the musical film, Little Shop, premiered in 1991. 
 colorized twice, RiffTrax On Kevin Murphy and Bill Corbett was released by RiffTrax in MP3 and DivX formats.  Legends colorized version is also available from Amazon Video on Demand, without Nelsons commentary. 

In November 2006, the film was issued by Buena Vista Home Entertainment in a double feature with The Cry Baby Killer (billed as a Jack Nicholson double feature) as part of the Roger Corman Classics series. However, the DVD contained only the 1987 colorized version of The Little Shop of Horrors, and not the original black-and-white version. 

It was announced on April 15, 2009 that Declan OBrien would helm a studio remake of the film.  "It wont be a musical" he told Bloody Disgusting in reference to the Frank Oz film from 1986. "I dont want to reveal too much, but its me. Itll be dark."  When speaking with Shock Till You Drop, he revealed "I have a take on it youre not going to expect. Im taking it in a different direction, lets put it that way." 

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  
*   at Legend Films
*  
*   at Trailers From Hell
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 