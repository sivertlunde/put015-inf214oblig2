El estudiante
 
 
{{Infobox film
| name           = El estudiante
| image          = El estudiante.jpg
| caption        = Theatrical release poster
| director       = Roberto Girault
| writer         = Roberto Girault
| starring       = Jorge Lavat Norma Lazareno José Carlos Ruiz Jeannine Derbez Siouzana Melikian Cristina Obregón
| distributor    = Halo Studio
| released       =  
| runtime        = 95 minutes 
| country        = Mexico
| language       = Spanish
}}
El estudiante ( ) is a 2009 Mexican film written and directed by Roberto Girault, and starring Jorge Lavat, Norma Lazareno, José Carlos Ruiz, Jeannine Derbez, Siouzana Melikián and Cristina Obregón. The film depicts the story of a 70 year old man called Chano, who decides to go to university despite his age.

== Plot ==
The story takes place in the city of Guanajuato. The story is about a 70 year old man called Chano who decides to enroll in a university course in order to study literature. Chano tries to break the generation gap using his passion for Don Quixote. He clashes with the different traditions and they all share their dreams and experiences.

==Cast==
* Jorge Lavat as Chano
* Norma Lazareno as Alicia
* Cristina Obregón as Carmen
* Pablo Cruz Guerrero as Santiago
* Siouzana Melikián as Alejandra
* Jorge Luiz Moreno as Marcelo
* Cuauhtémoc Duque as Eduardo
* Daniel Martínez as Héctor
* Jeannine Derbez as Sofía
* Silvia Santoyo as Lucía
* Raúl Adalid as Jorge
* Sofía Toache as Matilde
* Fernando Estrada as Álvaro
* José Carlos Ruiz as Don Pedro

== Production ==

===Filming===
The film was shot in well-known locations in Guanajuato city where they worked for 6 weeks.  The scenes inside the university were shot at  Universidad de Guanajuato.

== Release ==
The limited release of this film happened gradually in 100 locations in Mexico. It was also screened in various film festivals, including the Chicago Latino Film Festival and Latin American Film Festival of Grand Rapids, Michigan.

== Reception ==
The film won the Mexican film critics award "Diosa de Plata", despite not being nominated in any category by the Academy.

==External links==
*  

 
 
 
 
 
 
 
 
 
 