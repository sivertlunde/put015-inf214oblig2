The Bodyguard 2
{{Infobox film
| name           = The Bodyguard 2
| image          = The Bodyguard 2 movie poster.jpg
| image size     = 190px
| caption        = Thai theatrical poster.
| director       = Petchtai Wongkamlao
| producer       = 
| writer         = Petchtai Wongkamlao
| narrator       = 
| starring       = Petchtai Wongkamlao
| music          = 
| cinematography = 
| editing        = 
| distributor    = Sahamongkol Film International
| released       =  
| runtime        = 
| country        = Thailand Isan
| budget         = 
}} The Bodyguard, The Bodyguard 2 tells the origins of Petchtais bodyguard character, and like the first film, it features a host of cameo appearances by Thai celebrities, including action star Tony Jaa.

With a budget of over 100,000,000 baht, the film was the most expensive in Thai cinema before Ong Bak 2 surpassed it in late 2008.

==Plot==
 CIA agent, assigned to the same mission. Meanwhile, Khamlaos wife, Keaw, discovers that Khamlao had lied to her about his job in Thailand.

The end of the film leaves where the first film starts.

==Cast==
*Petchtai Wongkamlao as Khamlao / Khum Lhau
*Janet Keaw as Keaw
*Jacqueline Apitananon as Paula
*Sushin Kuan-saghaun as Sushin
*Surachai Sombatchareon as Surachai
*Pongsak Pongsuwan as Guru
*Tony Jaa as Elephant vendor

==External links==
*  

 
 
 
 
 
 
 
 