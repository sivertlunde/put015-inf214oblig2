The Concrete Jungle (film)
{{Infobox film
| name           = The Concrete Jungle
| image          = 
| caption        = 
| director       = Tom DeSimone
| producer       = Billy Fine Mark L. Rosen 
| writer         = Alan J. Adler
| starring       = Jill St. John Tracey E. Bregman Barbara Luna Sean OKane Sondra Currie
| music          = Joseph Conlan
| cinematography = Andrew W. Friend   
| editing        = Nino di Marco   
| distributor    = Motion Picture Marketing
| released       = September 3, 1982
| runtime        = 99 min.
| country        = United States
| language       = English
| budget         = $700,000 JAILED BAIT--A NEW WAVE OF WOMEN-IN-PRISON FILMS
BROESKE, PAT H. Los Angeles Times (1923-Current File)   19 May 1985: ac24. 
| gross = $11 million 
| preceded_by    = 
| followed_by    = 
}}
The Concrete Jungle is a 1982 American women in prison film directed by Tom DeSimone and featuring Jill St. John and Tracey E. Bregman. 

==Plot==
A womans boyfriend unsuspectingly uses her to carry a stash of cocaine in her skis and she is caught by airport security.  She is tried, convicted and sent to prison where she quickly learns to toughen up if she wants to survive.

==Cast==
*Jill St. John as Warden Fletcher 
*Tracey E. Bregman as Elizabeth (as Tracy Bregman)
*Barbara Luna as Cat (as Barbara Luna) 
*June Barrett as Icy
*Aimée Eccles as Spider    
*Sondra Currie as Katherine  Peter Brown as Danny
*Sean OKane as Athlete (uncredited)

==References==
 

==External links==
* 

 
 
 
 
 
 


 