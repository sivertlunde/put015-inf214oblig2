The Women in His Life
{{Infobox film
| name           = The Women in His Life
| image          = 
| caption        = 
| director       = George B. Seitz
| producer       = Lucien Hubbard
| writer         = F. Hugh Herbert
| starring       = Otto Kruger
| music          = 
| cinematography = Ray June
| editing        = Conrad A. Nervig
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
}}

The Women in His Life is a 1933 American crime film directed by George B. Seitz.   

==Cast==
* Otto Kruger as Kent Barry Barringer
* Una Merkel as Miss Simmy Simmons
* Ben Lyon as Roger McKane
* Isabel Jewell as Catherine Cathy Watson
* Roscoe Karns as Lester
* Irene Hervey as Doris Worthing
* C. Henry Gordon as Tony Perez
* Samuel S. Hinds as Thomas J. Worthing
* Irene Franklin as Mrs. Florence Steele / Madame Celeste
* Muriel Evans as Molly
* Raymond Hatton as Curly, Tonys bodyguard
* Jean Howard as Information girl Paul Hurst as Paul, Tonys bodyguard

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 