The Forsaken Land
 
{{Infobox film
| name           = The Forsaken Land
| caption        = 
| image	         = The Forsaken Land FilmPoster.jpeg
| director       = Vimukthi Jayasundara
| producer       = Chandana Aluthge
| writer         = Vimukthi Jayasundara
| starring       = Kaushalaya Fernando
| music          = Nadeeka Guruge
| cinematography = Channa Deshapriya
| editing        = Gisèle Rapp-Meichler
| distributor    = 
| released       = 14 May 2005
| runtime        = 108 minutes
| country        = Sri Lanka
| language       = Sinhala
| budget         = 
}}

The Forsaken Land ( ) is a 2005 Sri Lankan drama film directed by Vimukthi Jayasundara. It was screened in the Un Certain Regard section at the 2005 Cannes Film Festival, where it won the Caméra dOr.   

==Cast==
* Kaushalaya Fernando - Soma
* Nilupili Jayawardena - Lata
* Hemasiri Liyanage - Piyasiri
* Saumya Liyanage - Palitha
* Pumudika Sapurni Peiris - Batti
* Mahendra Perera

==Reception==
The film received fairly positive reviews from critics. The film holds a 63% rating on Rotten Tomatoes, with an average rating of 5.8/10.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 