Pete 'n' Tillie
{{Infobox film
| name           = Pete n Tillie
| image          = PeteTilliePoster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Martin Ritt
| producer       = Julius J. Epstein
| writer         = Peter De Vries (story) Julius J. Epstein
| starring       = Walter Matthau Carol Burnett
| music          = John Williams
| cinematography = John A. Alonzo
| editing        = 
| distributor    = Universal Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $14,999,969   
| preceded_by    = 
| followed_by    = 
}}
Pete n Tillie is a 1972 American comedy-drama film starring Walter Matthau and Carol Burnett in the title roles. Its advertising tagline was "Honeymoons over. Its time to get married."

 .

== Plot ==
Pete Seltzer (Matthau) is introduced to Tillie Schlaine (Burnett) at a party. Her friends Gertrude and Bert are the hosts and attempting to fix her up.

Pete is a confirmed bachelor with eccentric habits. When he isnt doing odd motivational research for a San Francisco firm, he plays ragtime piano and makes bad puns. He periodically pops in and out of Tillies life, going days without calling but showing up spontaneously at her door. When they finally make love, he learns Tillie is a virgin.

It appears Pete might still be seeing other women, but when he gets a promotion at work, Tillie announces its time to get married. They do, then buy a house and have a baby boy. Petes affairs, however, apparently continue, Tillie even needing to discourage one of his young lovers at lunch.

Years go by until one day 9-year-old son Robbie (Lee Montgomery) is stricken with a fatal illness. Pete tries to shield the boy by keeping him in what Tillie calls "a world of nonsense," but the inevitable death destroys Tillies religious faith and ruptures their marriage.

Tillie abstains from sex while Pete turns to drink and takes an apartment. Tillies depression is alleviated a bit by a friendship with Jimmy (Rene Auberjonois), who is gay but willing to marry her if that would make Tillie happy. Bert makes a pass at Tillie as well. When she and Jimmy conspire to make Gert (Geraldine Page) reveal her true age at long last, the result is a public brawl between the two women.

Tillie ends up in a sanitarium. Her life has come to a standstill until Pete turns up one day. When she sees the way their sons death affects him, after years of his hiding it, Tillie and Pete leave side by side.

== Cast ==
*Walter Matthau as Pete
*Carol Burnett as Tillie
*Geraldine Page as Gertrude
*Barry Nelson as Burt
*Rene Auberjonois as Jimmy Twitchell

== Reception == rentals in 1973. 

== Awards and nominations == Adapted Screenplay Supporting Actress.
 Best Actor – Motion Picture Musical or Comedy and won the 1973 BAFTA Award for Best Actor in a Leading Role for this and for Charley Varrick.

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 