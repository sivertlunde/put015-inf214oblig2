Secret of the Cave
{{Infobox film
|name=Secret of the Cave
|image=Secretofthecavemp.JPG
|caption=film poster
|director=Zach C. Gray
|producer=David George Benjamin Keith Mark Thomas Thomas L. Wentworth Joseph Kelly Niall OBrien Niamh Finn Maria McDermottroe
|music=John Carta
|distributor=First Look Studios
|released=October 20, 2006 September 4, 2007 (DVD)
|runtime=88 minutes
|language=English
}}
Secret of the Cave is a 2006 student film by the School of Visual Art and Design at Southern Adventist University. The film is an adaptation of the 1920 childrens story of the same name by Arthur S. Maxwell (also known as Uncle Arthur).  It was released on DVD in September 2007 and is distributed by First Look Studios in conjunction with Carmel Entertainment.

==Plot==
The film follows a young American boy named Roy Wallace (Kevin Novotny) who spends his summer in a tiny fishing village on the coast of western Ireland. After a short while, unexplainable events and deeds begin to occur and rumors of ghosts sweep the village. All these things are pointing to something mysterious that is going on in a nearby cave. Roy sets out to disprove the rumor and decides to explore the cave with his new, teenage local friends Oscar (Gareth OConnor) and Abbey (Niamh Finn). Roy faces his fears and discovers the secret of the cave.

==Cast==
* Kevin Novotny as Roy Wallace
* Patrick Bergin as Patrick Wallace Joseph Kelly as Peter McDonald Niall OBrien as Uncle Wallace
* Niamh Finn as Abbey
* Maria McDermottroe as Mrs. MacIntyre
* Noelle Brown as Aunt Mary Sean Murphy as Pastor
* Gareth OConnor as  Oscar
* Jim Roche as Edmund

==Book==
The film is based on the 1920 childrens book of the same name by Arthur S. Maxwell (also known as "Uncle Arthur"). {{cite web
|last=Ritrosky |first=Madelyn |year=2006
|url=http://emol.org/film/archives/heartlandfilmfestival/secretofthecave.html 
|title=Legendary Outlaws and Mysterious Caves – Oh My!
|publisher=Entertainment Magazine OnLine}}  {{cite book 
|url=http://books.google.com/books?id=7jTQAAAAMAAJ&lpg=PA1258&pg=PA1258#v=onepage&q&f=false |title=Catalog of Copyright Entries, Part 1, Volume 17, Issue 2 
|publisher=Library of Congress Copyright Office
|page=1258
|year=1920}}  {{cite book
|last=Maxwell|first=Arthur S. |year=1920 
|title=Secret of the Cave|isbn=0-8163-1317-2}}  
The book has sold more than 142,000 copies worldwide in English, Spanish, Portuguese, Icelandic, Danish, and Swedish. {{cite web
|url=http://secretofthecave.com/cave.html 
|title=Secret of the Cave official website 
|publisher=SecretOfTheCave.com |year=2006}} (see "Story," then "Background") 

The film differs from the book in a few ways, due mostly due to the fact that the book was set in 1920s and the film was set in the 2000s. The book has Roys two local friends as male, teenage brothers. The film has them as brother and sister.  In the film, Roy, the main character, is from America, {{cite web
|url=http://www.clevelandbanner.com/NF/omf/daily_banner/lifestyle_story.html?rkey=0068124+cr=gdn
|last=Difebbo|first=Arleah
|title= ‘Secret of the Cave:’ SAU students, faculty produce film
|publisher=Cleveland Daily Banner |date=September 9, 2007
|archiveurl=http://web.archive.org/web/20070919111754/http://www.clevelandbanner.com/NF/omf/daily_banner/lifestyle_story.html?rkey=0068124+cr=gdn
|archivedate=2007-09-19
|accessdate=2010-10-09}}  whereas the book suggests that Roy is from Europe.  The story is set in Scotland, while the film uses Ireland as a backdrop, and adds Abbey as a "strong female counterpart to Roy." Stanyer, Brent (February 11, 2008). ( . SpectrumMagazine.org. 

==Reception==
Madelyn Ritrosky, writing for Entertainment Magazine Online, described the Secret of the Cave  as a "fun, mysterious, adventurous family film that the entire family can enjoy."   
Richard Propes of Independent Critic gave the film a B- (2.5 stars), praising the production design, cinematography and some of the performances in the film, but noting the films "easygoing pace" as a "hard sell for American audiences". {{cite web
|last=Propes|first=Richard |year=2006
|url=http://theindependentcritic.com/secret_of_the_cave 
|title=Secret of the Cave review|publisher=IndependentCritic.com}}  
The film  won the 2006 Crystal Heart Award from the Heartland Film Festival in Indianapolis, Indiana. The Crystal Heart Award was presented to the crew and cast from the Secret of the Cave at the festival award gala by Jon Voight. {{cite web|last=Byrd|first=Alita |date=February 11, 2008
|url=http://spectrummagazine.org/articles/spectrum_interview/2008/02/11/behind_movie_camera 
|title=Behind the movie camera: David George talks about what its like to make a movie
|publisher=SpectrumMagazine.org}}  
It was also nominated as one of five films to receive Grand Prize for Best Dramatic Feature Award at the film festival. {{cite web
|url=http://www.timesfreepress.com/absolutenm/templates/local.aspx?articleid=21143&zoneid=77
|last=Herrington|first=Angie
|title=Southern Adventist students step out of classroom to film movie in Ireland
|publisher=Chattanooga Times Free Press
|date=September 8, 2007
|archiveurl=http://www.timesfreepress.com/news/2007/sep/07/SouthernAdventist-students-step-out-of-classroom/ |archivedate=2007-09-07
|accessdate=2007-10-28
}}  {{cite web
|url=http://wdef.com/blog/southern_adventist_university_student_production_secrets_of_the_cave_hits_dvd_today/09/2007
|last=Legge|first=Joe
|title=Southern Adventist University Student Production Secret Of The Cave Hits DVD Today WDEF |date=2007-09-04|accessdate=2007-10-28}}  
The film has received the Dove Foundation "Family-Approved" seal. {{cite web
|url=http://www.dove.org/reviewpopup.asp?Unique_ID=6615 
|title=Secret of the Cave 
|publisher=Dove Foundation
|work=Dove.org
}} 
The films website is a 2006 W3 Server Award winner. 

==Behind the scenes==
According to one of the films producers,  Secret of the Cave took three and a half years to produce.   The films website lists the shooting locations as Achill Island, County Mayo, Ireland. The island on which the film was shot has a broadband connection faster than most other rural parts of Ireland.  About 11 miles of raw footage was shot. The cave, a central part of the film, was not discovered until 2 or 3 days before the scheduled shoot. It was discovered by a crew member, walking during a break. 

==References==
 

==External links==
* 
* 
* 
* , September 28, 2006

 
 
 
 
 
 
 
 
 