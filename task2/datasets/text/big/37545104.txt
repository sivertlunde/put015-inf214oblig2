Healers of the Dead Sea
 
Healers of the Dead Sea, produced by John Marco Allegro and Douglas Edwards, is a 30-minute CBS documentary regarding Dead Sea Scrolls and the Essenes.    

Allegro narrated and had begun work on the film for the BBC in 1980, under the alternative title "The Mystery of the Dead Sea Scrolls". The film charted the discovery of the scrolls, showed how they had survived and emphasized their importance, guiding the viewer around the first century landscape of Qumran. Allegro aimed to increase public interest in the discovery by letting them imagine their way around the various features, showing its orientation towards Jerusalem  where the expected river of life-giving waters were assumed to have come from in some prophesied time in the future. It guided the viewer around the banquet hall, scriptorium, watercourses and baptizmal cisterns to give a feeling of reality to the times. Allegro also starts to discuss how to do Essene healing Magic (paranormal)|magic.   

==See also==
*The Dead Sea Scrolls and the Christian myth
*The Sacred Mushroom and the Cross

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 

 