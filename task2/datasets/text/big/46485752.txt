Surviving the Tsunami - My Atomic Aunt

 

{{Infobox film
| name           = Surviving the Tsunami - My Atomic Aunt
| image          = 
| alt            = My Atomic Aunt, Beyond the Wave (Germany)
| caption        = 
| film name      = Meine Tante aus Fukushima
| director       =  Kyoko Miyake 
(三宅響子（みやけ きょうこ）) 
| producer       = Gregor Streiber
| screenplay     = Kyoko Miyake and Friedemann Hottenbacher
| story          = 
| based on       = 
| starring       = 
| narrator       = Kyoko Miyake
| music          = L. Jack Ketch and Shigeru Umebayashi
| cinematography = Natsuumi Kozo and Shai Levy
| editing        = 
| studio         = 
| distributor    = Women Make Movies (United States), Inselfilm Productions (Germany), NHK (Japan)
| released       = March 8, 2013
| runtime        = 52/73 minutes 
| country        = Germany, Japan 
| language       = English, Japanese (with subtitles)
| budget         = 
| gross          = 
}}

Surviving the Tsunami - My Atomic Aunt is a 2013 documentary film written, directed, and co-produced by Kyoko Miyake remembers the Fukushima nuclear disaster that occurred on March 11, 2011 by focusing on how the disaster has affected her home town of Namie, Fukushima, Japan. The film develops into a personal reflection on how the Fukushima disaster has affected not only the world, but Miyake herself and the family she once knew. 	

==Director==

After obtaining her degree in English History from the University of Tokyo, Kyoko Miyake moved to England to study English Witchcraft at Oxford University as a Swire Centenary Scholar.   Umehara, Toshiya. "KYOKO MIYAKE: Film Director Always Looking for the Truth." The Asahi Shimbun. The Asahi Shimbun Company, 21 Mar. 2014. Web.     "My Atomic Aunt." BritDoc. The BRITDOC Foundation, 1 Apr. 2013. Web.    While on sabbatical from her doctoral program at Oxford, Miyake began to create historically themed independent documentary films. Her inspiration for her first international feature length documentary, Surviving the Tsunami, came from her familial connection to the Japanese city of Namie, which had to be completely evacuated after the meltdown of the Fukushima Number One nuclear plant, and her anger at the way the disaster was being portrayed by the larger, mainstream media outlets.   Kyoko has directed award winning documentaries including Hackney Lullabies, which have been screened in Berlin, Germany, London, England and Sydney, Australia. Kyoko Miyake currently resides in England.   

==Synopsis==
Surviving the Tsunami brings together social, environmental, and personal perspectives of the national catastrophe of the Fukushima nuclear meltdown. In the documentary, Kyoko Miyake travels back to her hometown in Namie, Fukushima, to revisit her old life and assess the trauma still lingering from the disaster. "Surviving the Tsunami - My Atomic Aunt." First Hand Films, 1 Jan. 2013. Web.  .   She revisits Namie, her mothers hometown and meets the people who depended on the success of the nuclear plant for their livelihood. The film also follows Bunsei Watanabe and Kyoko Miyakes Aunt Kuniko, two people who hope for the rejuvenation of Namie, despite the disaster that has occurred.  Despite having lost family, friends, and jobs due to the meltdown and subsequent fear of the contamination zone, these two individuals are determined to rebuild their towns and neighborhoods and bring back the sense of community they once had. The film follows the residents of Namie, with emphasis on the experiences of Aunt Kuniko, as they come to terms with the reality of living in or near the "radiation zone" left in the wake the plants nuclear meltdown.   Surviving the Tsunami offers a different perspective on Japanese culture, national identity, human adaption, and global nuclear energy and proliferation.   

==Production== 
The documentary was originally titled “Beyond the Wave,” yet the completed documentary was released as "My Atomic Aunt.” 
The film aired on seven television networks around the world in 2013, including the BBC, and NHK, Japans National public broadcasting organization.   

==Critical reception and reviews==
Surviving the Tsunami has a successful IMDb rating of 7.3 out of 10 stars.   

Jon Lynes, of Time Out London, describes Surviving the Tsunami, as "Haunting and moving ... as hard-hitting as it is simply presented... a timely reminder that the impact of the Fukushima disaster is still being felt....".   

==Awards, festivals, and screenings==

* Hamburg Film Festival
* Dok Leipzig
* EDS International Documentary Festival
* Sundance Institute Documentary Grant Winner, Spring 2012
* NaturVision, German Conservation and Sustainability Film Prize, July 2014
* Winner, PUMA Creative Catalyst Award, Channel4 BRITDOC Foundation
* Winner, TIFFCOM Best Pitch Award, at Tokyo International Film Festival 
* Winner, Best Pitch Award, Tokyo TV Forum 2011 
* Winner, Best Japan Pitch, Asian Side of the Doc
* Gold Plaque Award, Chicago International Film Festival, Television Awards, April 2014
* Official Selection, Hamburg Film Festival (Germany), September 2013
* Official Selection, Dok Leipzig (Germany), October 2013
* Official Selection, EBS International Documentary Festival (South Korea), October 2013 Official Selection, DocPoint (Finland), 2014
* Official Selection, Paris Human Rights Film Festival, 2014

==References==
 

==External links==
*  
*  
*  

 
 
 
 
  
 
 
 
 
 

 