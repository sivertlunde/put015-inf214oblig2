Saraswathi (film)
{{Infobox film 
| name           = Saraswathi
| image          =
| caption        =
| director       = Thikkurissi Sukumaran Nair
| producer       = AL Sreenivasan
| writer         = Tagore Thikkurissi Sukumaran Nair (dialogues)
| screenplay     = Thikkurissi Sukumaran Nair
| starring       = Prem Nazir Thikkurissi Sukumaran Nair Muthukulam Raghavan Pillai Jayalalithaa
| music          = MS Baburaj
| cinematography = V Selvaraj
| editing        = VP Krishnan
| studio         = ALS Productions
| distributor    = ALS Productions
| released       =  
| country        = India Malayalam
}}
 1970 Cinema Indian Malayalam Malayalam film,  directed by Thikkurissi Sukumaran Nair and produced by AL Sreenivasan. The film stars Prem Nazir, Thikkurissi Sukumaran Nair, Muthukulam Raghavan Pillai and Jayalalithaa in lead roles. The film had musical score by MS Baburaj.   

==Cast==
*Prem Nazir
*Thikkurissi Sukumaran Nair
*Muthukulam Raghavan Pillai
*Jayalalithaa
*K. P. Ummer Meena

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Thikkurissi Sukumaran Nair.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aaru Paranju || S Janaki || Thikkurissi Sukumaran Nair || 
|-
| 2 || Ethra Thanne || P. Leela || Thikkurissi Sukumaran Nair || 
|-
| 3 || Madhurappathinezhu || LR Eeswari || Thikkurissi Sukumaran Nair || 
|-
| 4 || Marathakamanivarnna || S Janaki || Thikkurissi Sukumaran Nair || 
|-
| 5 || Neeyoru Raajaavu || CO Anto, Zero Babu || Thikkurissi Sukumaran Nair || 
|-
| 6 || Om Harishree || K. J. Yesudas || Thikkurissi Sukumaran Nair || 
|-
| 7 || Pennu Varunne || LR Eeswari || Thikkurissi Sukumaran Nair || 
|}

==References==
 

==External links==
*  

 
 
 


 