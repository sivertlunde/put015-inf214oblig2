The Second Time (film)
{{Infobox film
| name           = The Second Time
| image          = 
| caption        = 
| director       = Mimmo Calopresti
| producer       = Nella Banfi Angelo Barbagallo Nanni Moretti Francesco Bruni Heidrun Schleef
| starring       = Francesca Antonelli
| music          = Franco Piersanti
| cinematography = Alessandro Pesci
| editing        = Claudio Cormio
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

The Second Time ( ) is a 1995 Italian drama film directed by Mimmo Calopresti. It was entered into the 1996 Cannes Film Festival.   

==Cast==
* Francesca Antonelli - Antonella
* Valeria Bruni Tedeschi - Lisa Venturi
* Simona Caramelli - Sonia
* Marina Confalone - Adele
* Roberto De Francesco - Enrico
* Orsetta De Rossi - Raffaella
* Paolo De Vita - Judge Di Biagio
* Nello Mascia - Doctor
* Valeria Milillo - Francesca
* Nanni Moretti - Alberto Sajevo
* Rossana Mortara - Student
* Antonio Petrocelli - Ronchi

==References==
 

==External links==
* 

 
 
 
 
 
 
 