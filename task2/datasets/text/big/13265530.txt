Follow Me Home (film)
{{Infobox Film
| name           = Follow Me Home
| caption        = A promotional film poster for Follow Me Home
 
| image_size     =
| caption        = Movie poster for Follow Me Home.
| director       = Peter Bratt
| writer         = Peter Bratt
| starring       = Alfre Woodard  Benjamin Bratt Jesse Borrego Steve Reevis Calvin Levels Salma Hayek
}}

Follow Me Home is a 1996 film directed by activist and filmmaker Peter Bratt. It explores spiritual and intercultural race relations through the lives of four artists, one African American, one Native American and two latin-american cousins, who embark on a crosscountry road trip to paint a mural on the White House. Along the way, they meet a mysterious African American woman bearing a deep secret.

==Distribution==
Follow Me Home has yet to be picked up by a major distribution company.  The film is currently being shown on request, usually at universities and community centers  followed by a discussion facilitated by Native American activist Lakota Harden.

==Awards==
Peter Bratt received the Audience Award for Best Feature at the 1996 San Francisco International Film Festival.  The film earned the Best Feature Film Audience Award at the 1996 San Francisco International Film Festival. It was also an Official Selection in the 1996 Sundance Film Festival. 

==References==
 

==External links==
*   at Native Networks
*    at Speak Out Now
*   at Speak Out Now
*  

 
 
 
 
 
 
 

 