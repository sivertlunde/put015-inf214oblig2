Hard Luck
 
{{Infobox film
| name = Hard Luck
| image = Hard luck dvd cover.jpg
| caption = DVD cover
| director = Mario Van Peebles
| producer = Donald Kushner Mario Van Peebles Brad Wyman
| writer = Larry Brand Mario Van Peebles
| story = Larry Brand James Liao Bill Cobbs Mario Van Peebles
| music = Tree Adams
| cinematography = Alex Nepomniaschy
| editing = Edward R. Abroms
| studio = MVP Films Raamen Filmworks Xit Financial Group
| distributor = Sony Pictures Home Entertainment
| released =  
| runtime = 101 minutes
| language = English
| budget = $12 million
}} thriller film James Liao and Bill Cobbs. The film was released on direct-to-video|direct-to-DVD in the United States on October 17, 2006.

Hard Luck features a collaboration between Wesley Snipes, Mario Van Peebles and Bill Cobbs reunited the trio for the first time since 1991s New Jack City. Wesley Snipes played Lucky, a down on his luck former criminal and drug dealer whose post prison trials and tribulations take him on a wild adventure.

==Plot==
As the film begins. A former criminal and drug dealer Lucky (Wesley Snipes) was released from prison and tries to lead a respectable life despite his troubled past. However, Lucky once or twice reflects on the wisdom of his grandfather who supposedly told him "sometimes without bad luck, it would seem you dont have any luck at all." Shortly after his release from prison, Lucky tries to get back on his feet but inexplicably ends up a victim of Hurricane Katrinas wrath, and loses his newly found life.

After Lucky returns to New York, Lucky is dealt another blow when the government withdraws funds for his yoga and samba dance classes which he uses to try to keep kids off the streets. This unfortunate event leads to Lucky enduring the bad influences of two old friends from his past life as a criminal, who lure Lucky to a strip club under the false pretense of his friends birthday. When a drug deal with some dangerous mobsters goes bad, and Lucky is now on the run with a feisty Puerto Rican stripper from the strip club, Angela (Jacquelyn Quinones), and over $500,000 in American currency, which is wired with marking dye.
 Sawtooth Killers". Chang (James Liao), an egotistical and maniacal accomplice to his old-fashioned girlfriend Cass (Cybill Shepherd), the mother of a mentally challenged man named Eugene (Mike Messier). Motivated by Casss son Eugenes unpleasant experiences with societys rejection of mentally challenged individuals, the couple brutally kidnaps, tortures, and presumably kills their abductees, recording their exploits on video, possibly for future viewing.

Angela and Lucky find themselves caught up in the path of violence, and Lucky ends up an unlikely hero by saving the life of a would be victim of the Sawtooth Killers. Lucky saves Captain Davis (Mario Van Peebles) from Casss son Eugene, who is armed with a shovel. Lucky chooses to have a positive outlook on the events by using perspective gained from his grandfather, characterizes the events as being "the luckiest day in an unlucky mans life." Lucky and Angela go on to receive a $200,000 reward which had been offered by the police for information leading the capture, arrest, and or prosecution of the Sawtooth Killers.

==Cast==
* Wesley Snipes as Lucky
* Jacquelyn Quinones as Angela
* Cybill Shepherd as Cass James Liao as Chang
* Aubrey Dollar as Rain
* Kevin Thoms as Roland
* Adrian Washington as Lucky
* Noah Fleiss as Sol Rosenbaum
* Bill Cobbs as Cobb
* Kevin Chapman as Franklin
* Tom Kemp as Gino Gambetta
* Mike Messier as Eugene
* Mario Van Peebles as Captain Davis
* Tony Hua as Detective Lee
* Luis Guzmán as Million Dollar Mendez
* Melvin Van Peebles as Hospital Prophet

==Production==
It is set and filmed at Rhode Island in 56 days on October 3 and November 28, 2005.

==Reception==
Hard Luck received mostly negative reviews with a score of 29% on Rotten Tomatoes.

==Home media== Region 1 in the United States on October 17, 2006, and also Region 2 in the United Kingdom on 26 February 2007, it was distributed by Sony Pictures Home Entertainment.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 