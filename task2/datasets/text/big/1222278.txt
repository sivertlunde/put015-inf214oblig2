Vincent (1982 film)
{{Infobox film
| name = Vincent
| image =
| image_size =
| caption =
| director = Tim Burton
| producer = Rick Heinrichs
| writer = Tim Burton
| narrator = Vincent Price
| technical director/animator = Stephen Chiodo
| music = Ken Hilton
| cinematography = Victor Abdalov
| editing = Walt Disney Productions Buena Vista Distribution
| released =  
| runtime = 5:52
| country = USA
| language = English
| budget = US$60,000
| website =
| animator = Stephen Chiodo
}}

Vincent is a 1982  .

The film is narrated by actor Vincent Price, a lifelong idol and inspiration for Burton. From this relationship, Price would go on to appear in Burtons Edward Scissorhands. Vincent Price later said that the film was "the most gratifying thing that ever happened. It was immortality — better than a star on Hollywood Boulevard". {{cite web
 | title=Vincent — A Matter of Pastiche
 | first=Michael
 | last=Frierson
 | authorlink=
 | publisher= Animation World Magazine (Issue 1.9)
 | url=http://www.awn.com/mag/issue1.9/articles/frierson1.9.html
 | accessdate=2007-01-22 }} 

==Plot==
Vincent is the poetry story of a 7-year-old boy, Vincent Malloy, who pretends to be like the actor Vincent Price (who narrates the film). He does experiments on his dog Abercrombie in order to create a horrible ravenous Zombie dog. He is obsessed with the tales of Edgar Allan Poe, and it is his detachment from reality when reading them that leads to his delusions that he is in fact a tortured artist, deprived of the woman he loves, mirroring certain parts of Poes "The Raven". The film ends with Vincent feeling terrified of being tortured by the going-ons of his make-believe world, quoting "The Raven" as he falls to the floor in frailty, believing himself to be dead.

==Production and release== Walt Disney Productions, Tim Burton found himself two allies in the shape of Disney executive Julie Hickson, and Head of Creative Development Tom Wilhite. The two were impressed with Burtons unique talents and, while not "Disney material", they felt he deserved to be given respect. As such, in 1982, Wilhite gave Burton $60,000 to produce an adaptation of a poem Burton had written titled Vincent. Burton had originally planned the poem to be a childrens short story book but thought otherwise.   
 German Expressionist films of the 1920s, Vincent imagines himself in a series of situations inspired by the Vincent Price/Edgar Allan Poe films that had such an effect on Burton as a child, including experimenting on his dog — a theme that would subsequently appear in Frankenweenie (1984 film)|Frankenweenie — and welcoming his aunt home while simultaneously conjuring up the image of her dipped in hot wax. Vincent Malloy, the main character in the film, bears a striking resemblance to Tim Burton himself. 

The film was narrated by Burtons childhood idol, Vincent Price, and marked the beginning of a friendship between them that lasted until Prices death in 1993. Burton credits the experience as one of the most shaping experiences of his life. 
 Disney vaults, Annecy Film Festival in France. Disney was pleased with the film, but they did not know what to do with it. 

==Cameos== James and the Giant Peach. The first two cameos were years before production of Nightmare, though Burton had toyed with the original story idea while still an animator at Walt Disney Animation Studios|Disney. 

The character Prince Vince, from the Beetlejuice (TV series)|Beetlejuice animated cartoon which would premiere 7 years later, is named after and bears a strong resemblance to Vincent Malloy, both in appearance and mannerisms.

==References==
 

==External links==
 
*   at Allmovie
*  
*  

 

 
 
 
 
 

 
 
 
 