Chopin: Desire for Love
{{Infobox film
| name     = Chopin: Desire for Love
| image          =
| producer         = Pawel Rakowski, Jerzy Antczak
| director       = Jerzy Antczak
| writer         = Jerzy Antczak, Jadwiga Baranska
| starring       = Piotr Adamczyk
| music         = Nikodem Wolk-Laniewski (sound)
| cinematography = Edward Klosinski
| editing         = Ewa Romanowska-Rózewicz
| released   = 2002
| runtime        = 134 minutes
| country  = Poland
| language = Polish
}}
 Polish pianist Fryderyk Chopin.

The plot covers the affair between Chopin and feminist writer George Sand. Chopins music is integral to the film - with pianist Janusz Olejniczak  playing more than 77 compositions. 
  

Two versions of the film were shot—in Polish and English—with British actors later lip-syncing the dialogue.   

Director Antczak spent 25 years writing the screenplay and six years raising the budget for the film.   

The film was screened at Houston Film Festival in 2003 and won the Gold Award for Best Cinematography and the Platinum Award for Best Drama. 

==Plot==

The film starts when Fryderyk Chopin is still a young man living with his parents and his two sisters in Warsaw where he frequently plays the piano and composes music for the decidedly unmusical Grand Duke Constantine. Shortly before the November Uprising, Chopins father urges him to leave for Paris, which Fryderyk does. Once in Paris he meets novelist George Sand, who has just split from her violent lover Jean Pierre Félicien Mallefille|Mallefille. Although he is immediately drawn to Sand, he initially refuses her advances. However, after several months, their mutual friend Albert urges Chopin to get to know George better and a passionate romance starts to build. During their affair, Chopin is diagnosed with tuberculosis and has to cope with a declining health. The relationship is further complicated by Georges two children: Maurice and Solange. While Maurices near-hysterical hatred of Chopin leads from one escalation to the other, Solange develops an obsessive love for Chopin which leads to a rivalry between Solange and her mother. After several years of constant fighting between Chopin, George, Maurice and Solange, the relationship ends and Chopin calls for one of his sisters to help him get through the last days of his life.

==Cast==
* Piotr Adamczyk as Frédéric Chopin: famous Polish composer and pianist.
* Danuta Stenka as George Sand: French author whose given name was Amandine Lucille Aurore Dudevant-Dupin.
* Adam Woronowicz as Maurice Dudevant: Sands son.
* Bozena Stachura and Sara Müldner as Solange Dudevant: Sands daughter. Müldner plays the young version of Solange Albert Grzymala: Chopins closest friend Ferenc Liszt: famous Hungarian composer and pianist.
* Jadwiga Baranska as Mrs. Justyna Chopin: Chopins mother
* Jerzy Zelnik as Mikołaj Chopin|Mr. Nicolas Chopin: Chopins father
* Agnieszka Sitek as Izabela Chopin: Chopins sister Ludwika Chopin: Chopins sister
* Anna Korcz as Baroness Charlotte de Rothschild: French socialite and art patron. Grand Duke Constantine Pavlovich Romanov: Polish Grand Duke of Russian ancestry who frequently called upon Chopin.  Grand Duchess Juliane: Grand Duke Constantines wife.
* Jacek Rozenek as Jean Pierre Félicien Mallefille: George Sands first lover 
* Marian Opania as Jan: Chopins servant

==References==
 

==External links==
* http://www.chopindesireforlove.com/
*  

 
 
 
 
 