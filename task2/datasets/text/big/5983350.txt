The Conquering Power
{{Infobox film
| name           = The Conquering Power
| image          = Theconqueringpower1921movieposter.jpg
| image_size     =
| caption        = Movie poster Rex Ingram
| producer       = Rex Ingram
| writer         = June Mathis
| based on       =   Ralph Lewis
| music          =
| cinematography = John F. Seitz
| editing        =
| distributor    = Metro Pictures
| released       =  
| runtime        = 89 minutes
| country        = United States English intertitles
| budget         =
| gross          =
}}
 silent romantic Rex Ingram Ralph Lewis. The film was based on the novel Eugénie Grandet by Honoré de Balzac. 

==Plot== Ralph Lewis). The miserly Grandet, despite being the wealthiest man in his province, forces his family to live in poverty and schemes to cheat his nephew out of his inheritance from his father.

Charles falls in love with Grandets daughter Eugenie (Alice Terry) but Grandet condemns their love, and sends Charles away. While Charles is away, Grandet kills Eugenies mother, which sends him further into a maddened state. Later, it is revealed that Eugenie is not really Monsieur Grandets daughter; if she knew, then she could reclaim all of the gold that originally belonged to her mother, leaving her father penniless. Monsieur Grandet has a violent argument with Eugenie, after she finds letters sent by Charles that her father had hidden, and Monsieur Grandet accidentally locks himself in a small room where he keeps his gold. He starts hallucinating and is eventually killed after becoming frantic.

Eugenie is now left an extremely wealthy young lady, which only intensifies the pressure put on her by two competing families to marry one of the suitors. She announces her engagement, but shortly after is reunited with Charles.

==Cast==
*Alice Terry as Eugenie Grandet
*Rudolph Valentino as Charles Grandet Ralph Lewis as Père Grandet
*Carrie Daumery as Madame Grandet
*Bridgetta Clark as Madame des Grassins
*Mark Fenton as Monsieur des Grassins
*Ward Wing as Adolphe des Grassins
*Eric Mayne as Victor Grandet
*Edward Connelly as Notary Cruchot
*George Atkinson as Bonfons Cruchot
*Willard Lee Hall as Abbé Cruchot

==References==
 

== External links ==
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 


 
 