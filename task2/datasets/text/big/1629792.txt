The Hockey Sweater
{{Infobox book
| italic title   = The Hockey Sweater
| name           = The Hockey Sweater
| image          = TheHockeySweater.png
| image_size     = 
| alt            = Illustrated book cover of a young boy pointing to a wall poster of Maurice Richards career statistics using a hockey stick as several of his friends look on
| caption        = The cover of The Hockey Sweater
| author         = Roch Carrier
| title_orig     = Le chandail de hockey
| orig_lang_code = fr
| translator     = Sheila Fischman Sheldon Cohen
| cover_artist   = 
| country        = Canada
| language       = English
| subject        = 
| genre          = 
| set_in         = 
| published      = 1979
| publisher      = Tundra Books
| pub_date       = 
| english_pub_date = 
| media_type     = 
| pages          = 
| awards         = 
| isbn           = 0-88776-169-0
}}
{{Infobox film
| name        = The Sweater
| image       = 
| caption     =  Sheldon Cohen
| producer    = {{Plainlist|
* Marrin Canell
* Derek Lamb (executive producer)
* David Verrall}}
| starring    = {{Plainlist|
* Roch Carrier (voice)
* Jean-Guy Moreau (voice)}}
| music       = Normand Roger
| editing     = David Verrall
| distributor = National Film Board
| released    =  
| runtime     = 10:21
| country     = Canada
| language    = {{Flatlist|
*French
*English
}}
}} Canadian author animated short Sheldon Cohen.
 sweaters with Richards number 9 on the back. When his mother orders a new sweater after the old one has worn out, he is mistakenly sent a sweater of Montreals bitter rival, the Toronto Maple Leafs, instead. Carrier faces the persecution of his peers and his coach prevents him from playing.

The Hockey Sweater is Carriers most famous work and is considered an iconic piece of Canadian literature. The story has sold over 300,000 copies and has been republished in numerous anthologies. It exemplifies the nations passion for hockey, and while it is often considered an allegory of the relationship and tensions that exist between francophones and English language|anglophones, the story is popular throughout the entire nation. A line from the story appeared on the Canadian five-dollar bill from 2001 until 2013.

==Background== anglophones escalated separate from Canada reached its peak in the late 1970s.    Seeking to explain Quebecs independence movement, the Canadian Broadcasting Corporation (CBC)s Toronto affiliate asked Roch Carrier, whose debut novel La Guerre, Yes Sir had been popular among both French and English Canadians, to explain "what does Quebec want?"   

Carrier spent several weeks trying to answer the question, ultimately producing what he described as a "flat essay" that was "dull as an editorial in a newspaper".    Three days before his deadline, Carrier informed the CBC that he would not be able to complete the project. He was told that the network had already booked studio time for him and had been promoting his appearance. As he remained unwilling to present his essay, Carrier was asked to write about anything he wanted to fill the time. 
 Eaton catalogues on my legs, and I stood up, and I was taller than my mom, and I had a stick in my hands, so I was stronger than my brother, and I felt that I was little me. So I started to write about that and it turned into the Hockey Sweater story." 

==Summary==
  sweater|alt=A young boy stands on a snow-covered street. He is wearing a dark-coloured sweater with a stylized maple leaf logo on the chest.]]
 Maurice "The Rocket" Richard.  He writes of how they emulated Richards style and mannerisms, and on the ice: "we were five Maurice Richards against five other Maurice Richards, throwing themselves on the puck. We were ten players all wearing the uniform of the Montréal Canadiens, all with the same burning enthusiasm. We all wore the famous number 9 on our backs." 

His old sweater having worn out, Carriers mother seeks to replace it. She writes a letter to Eatons in French to order a new sweater from their English-only catalogue. When the package arrives, the young Carrier is horrified to discover the sweater of the rival Toronto Maple Leafs was sent by mistake.  He argues with his mother, who refuses to return the sweater for fear of offending "Monsieur Eaton", an English-speaking fan of Toronto. 
 rink where forward position, five players on the ice. Carrier was so mad that he smashes his stick on the ice in frustration, for which the priest scolds him: "just because youre wearing a new Toronto Maple Leafs sweater unlike the others, it doesnt mean youre going to make the laws around here."   The priest sends Carrier to the church to pray for forgiveness, where Carrier instead asks God to send "a hundred million moths" to eat his Toronto Maple Leafs sweater. 

==Publication==
Carrier wrote the story in French, and it first appeared in 1979 under the title " " ("An abominable maple leaf on the ice") in a collection of his works called Les Enfants du bonhomme dans la lune (Children of the Man in the Moon). It appeared in an English translation by Sheila Fischman the same year as part of an English collection of Carriers work called The Hockey Sweater and other stories.    It has since been republished in numerous anthologies of Canadian and hockey literature.
 animated short Sheldon Cohen 1981 British Academy Film Awards. 

In 1982 Cohen approached May Cutler, founder of Tundra Books, to create an illustrated childrens book of the story. It was published in 1984 as The Hockey Sweater,  and by 2014 had sold over 300,000 copies.    Following the success of the book, Cutler asked Carrier to write three more stories of his childhood to be illustrated by Cohen, each covering a different sport in a different season. They were published as The Boxing Champion (1991), The Longest Home Run (1994) and The Basketball Player (1996). 

==Themes==
 
The passion Carrier and his friends had for the game of hockey, particularly for the Montreal Canadiens, is the dominant theme of the story. In introducing the film for his video anthology Leonard Maltins Animation Favorites from the National Film Board of Canada, American critic Leonard Maltin noted that hockey is "an obsession, a country-wide preoccupation that dominates many lives", particularly those of children. He argued that The Sweater is one of the National Film Boards best animated works that combined humour with cultural significance.  
 50 goals in a 50-game season.  Richard attended the 1980 premiere of The Sweater in Montreal, and according to Carrier, was moved to tears by the film. Richard also requested copies so that he could show it to his own children.   
 rivalry between the Canadiens and the Maple Leafs.   Carrier stated, however, he had no political motivations, and only wished to "tell a good little story". 

==Cultural impact==
The Hockey Sweater has achieved an iconic place in Canadian literature.    It is the defining work of Carriers career, and while he has lamented the fact that it has so overshadowed his other works, Carrier appreciates what its popularity has given him: "There is almost not one day in my life that there is not something nice that happens because of the story." 
 2001 series Canadian banknote.  The line, appearing in both French and English is: « Les hivers de mon enfance étaient des saisons longues, longues. Nous vivions en trois lieux : l’école, l’église et la patinoire; mais la vraie vie était sur la patinoire. » / "The winters of my childhood were long, long seasons. We lived in three places&nbsp;– the school, the church and the skating rink&nbsp;– but our real life was on the skating rink." It is accompanied by scenes of children playing outdoors in the winter, centred by one in a Montreal Canadiens sweater with Maurice Richards number 9 on their back. 
 Calgary Philharmonic and National Arts Centre Orchestras in 2012. 

==References==
;Footnotes
 

;Bibliography
*  
*  
*  
*  
* 

==External links==
*  
*  

 
 
 
 
 
 