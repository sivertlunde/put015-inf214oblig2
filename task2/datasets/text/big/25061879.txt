Golden Needles
{{Infobox film
| name           = Golden Needles
| image          = Golden_Needles.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Robert Clouse
| producer       = Fred Weintraub
| writer         = S. Lee Pogostin Sylvia Schneble Jim Kelly Burgess Meredith Roy Chiao
| music          = Lalo Schifrin
| cinematography = Gil Hubbs
| editing        = Michael Kahn
| studio         = 
| distributor    = American International Pictures
| released       =  
| runtime        = 92 mins.
| country        = United States
| language       = English
| budget         = under $1 million Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p292   
| gross           = Hong Kong dollar|HK$ 18.00 
}}
Golden Needles (also released under the title The Chase for the Golden Needles) is a 1974 American Action film|action/adventure film starring Joe Don Baker, Elizabeth Ashley, Ann Sothern, Jim Kelly, Burgess Meredith, and Roy Chiao. The film was directed by Robert Clouse and shot on location in Hong Kong.

==Plot==
A legendary statue has seven gold needles inserted in it, and an adult man will become a sexual superman when the needles are placed in the same position in his body. A colorful group of characters is all in on the hunt for the mysterious statue.

==Cast==
* Joe Don Baker as Dan
* Elizabeth Ashley as Felicity
* Ann Sothern as Fenzie Jim Kelly as Jeff
* Burgess Meredith as Winters

==Production==
The film was to star George Lazenby. 
==Discography==

The CD soundtrack composed and conducted by Lalo Schifrin is available on Music Box Records label ( ).

== Sources ==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 