Zaat
 
{{Infobox film
| name           = Zaat
| image          = Blood waters.jpg
| image size     = 225px
| caption        = theatrical poster
| director       = Don Barton
| producer       = Don Barton
| writer         = Story: Ron Kivett Lee O. Larew Screenplay: Don Barton
| starring       = Marshall Grauer
| music          = Jamie DeFrates Barry Hodgin Jack Tamul (electronic)
| cinematography = Jack McGowan
| editing        = George Yarbrough
| distributor    = Clark Distributors
| released       = January 1971
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $75,000
| gross          =
}}

Zaat (also known as The Blood Waters of Dr. Z, Hydra (Canadian bootleg title), Attack of the Swamp Creatures (US bootleg title) and Legend of the Zaat Monster (proposed re-release title) is a 1971  cult movie that gained significant exposure when it was used in an episode of movie-mocking television series Mystery Science Theater 3000 in May 1999. {{cite book| author = TV Guide| title = TV Guide: The Ultimate Resource to Television Programs on DVD
| year = 2005| publisher = St. Martins Griffin| isbn = 0-312-35150-X| quote = A human and two robots poke fun at egregiously B-rate sci-fi movies in this underground comedy series.| page = 171}} 

== Plot ==
The film begins with Nazi mad scientist Dr. Kurt Leopold in his lab, where he has lived alone for about 20 years (it is revealed later in the film that he graduated cum laude from MIT in 1934). He is contemplating his former colleagues laughter at his formula, which is described as " Z a A t  " (read Z-sub-A, A-sub-T, but which he simply calls "Zaat"). His formula can turn a man into a walking catfish. He injects himself with the serum and emerges from a tank as a giant fish-like creature.

His first act of revenge on the society that he feels has wronged him is to release several smaller walking catfish around the towns lakes and river (filmed in the St. Johns River near Green Cove Springs), an annoyance to the townspeople, and releases Zaat into the local water supply, rendering many of the townspeople ill.

Leopold decides to kill the colleagues that laughed at his work. He begins with a character named Maxson. In a lake where Maxson is fishing, Leopold swims under Maxsons boat, overturns it, and proceeds to kill Maxson and Maxsons son. Maxsons wife escapes, although she is in shock from the attack.

After killing Maxson, Leopold discovers a girl who is camping out alone on the shore of the lake. He approaches her, only to be deterred by her barking dog. The girl carries on with her business, unconcerned about the barking dog. Leopold retreats. Later, Leopold kills another colleague, Ewing.

His two colleagues now deceased, Leopold returns to the lake where the girl is still camping and waits for an opportunity to abduct her. His perseverance pays off when she strips down to a yellow bikini to go swimming. She dives into the lake, swimming carefree until Leopold catches her underwater. He swims with her to his lab, even as she struggles in vain to escape.

At the lab, the bikini-clad girl is lying strapped down in a mesh basket next to the large tank of Zaat. She is unconscious, and Leopold reveals his intentions to make her his mate. Leopold injects Zaat into her neck. As she is immersed into a tank of Zaat, the girl wakes up and struggles against the ropes holding her. The equipment malfunctions for reasons unknown, and her corpse, partially transformed, is pulled from the tank.

The movie strangely diverts from the storyline for approximately 10–15 minutes to show a lingering scene of the town sheriff Lou, watching a small group of youths playing religious folk music. After one of the youths (an acoustic guitarist, Jamie DeFrates, who also wrote the songs for the film) finishes leading the group in a song, the towns sheriff, Lou, places them all in the towns jail, presumably for their own protection. (This scene does not appear in all released versions.)

Leopold attempts to kidnap another mate. His choice is Martha Walsh, the lovely female member of the INPIT scientific team sent to investigate the weird happenings in the town (caused by Dr. Leopold). Leopold grabs her after her male counterparts leave her alone. Leopold takes her to his lab, but two of her companions (having unraveled the plot) are waiting there. Leopold kills them (including the sheriff) violently. He injects her with Zaat, readies her to be dunked into the tank, and makes his getaway, with canisters of Zaat. Marthas transformation does not go as planned and she gets saved from getting dunked in the tank by one of her dying male companions as Leopold flees toward the ocean. Despite being saved from the transformation, she appears to be in a trance and immediately follows Leopold into the sea. The movie ends ambiguously, with Leopold seen shot but not killed.

==Cast==
*Marshall Grauer as Dr. Kurt Leopold
*Wade Popwell as The Monster
*Paul Galloway as Sheriff Lou Krantz
*Gerald Cruse as Marine Biologist Rex Baker
*Sanna Ringhaver as INPIT Agent Martha Walsh
*Dave Dickerson as INPIT Agent Walker Stevens
*Archie Valliere as Deputy Sheriff
*Nancy Lien as Girl Camper
*Jamie DeFrates as Acoustic Guitarist

==Production== Switzerland and Marineland of Florida|Marineland.

==Release==
The movie was shown in Jacksonville as well as in theaters in mostly southern states during its original theatrical release. Lyons, Mike.  First Coast News (October 28, 2009)  It was shown on July 11, 2009 in Atlanta,  then in Jacksonville on October 28, 2009, reviving interest in the film. The movie was shown in Statesboro, Georgia on November 1, 2009, but received a poor response.
 Elvira hosting and spoofing the film throughout.

In February 2012, it was later issued on DVD/Blu-ray for the first time by Film Chest and HD Cinema Classics. Digitally restored in HD and transferred from original 35mm elements, the DVD/Blu-ray combo pack also contained a feature-length audio commentary by cast and crew, the original 35mm trailer, television spots, outtakes, a radio interview, a before-and-after restoration demo and an original movie art postcard. 

==Mystery Science Theater 3000==
Cult television series Mystery Science Theater 3000 featured Zaat in a season 10 episode under the title Blood Waters of Dr. Z.  The episode, which originally aired May 2, 1999,  mocked the films low-budget effects and general tepidity. Director Don Barton was reportedly annoyed with MST3K for mocking his movie, but later clarified that the only reason he was annoyed was because SyFy (then The Sci-Fi Channel) had failed to secure the proper rights to the film. Barton issued a cease and desist and a lawsuit, so SyFy pulled the episode, and only reran it twice two years later, when they had cleared the issue with Barton out of court. 

Shout! Factory released the MST3K episode on DVD, along with three others, on March 16, 2010 as part of Mystery Science Theater 3000 - Volume XVII, after managing to properly secure the rights.

==References==
 

== External links ==
*  
*  
*  
*  
*  
*  

=== Mystery Science Theater 3000 ===
*  
*  

 
 
 
 
 
 
 
 
 
 