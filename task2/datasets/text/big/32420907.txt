What the Swedish Butler Saw
 
{{Infobox film
| name = What the Swedish Butler Saw
| image = 
| image_size = 
| border = 
| alt = 
| caption = 
| director = Vernon P. Becker
| producer = Inge Ivarson
| writer = Vernon P. Becker Barry E. Downes
| based on =  
| starring = Ole Søltoft Sue Longhurst Charlie Elvegård Diana Dors
| cinematography = Tony Forsberg
| editing = Ingemar Ejve
| studio = Film AB Robur Unicorn Enterprises
| distributor = Independent International Pictures
| released =  
| runtime = 73 minutes  
| country = Sweden United States
| language = English
}} erotic sex comedy film directed by Vernon P. Becker and starring Ole Søltoft, Sue Longhurst and Charlie Elvegård. It is known by several alternative titles including A Man with a Maid, The Groove Room and Champagnegalopp.  The film is loosely based on the 1908 erotic novel The Way of a Man with a Maid.

During the 3-D revival of the 1980s, the film was re-released under the title "Tickled Pink", but the release did keep the "Swedish Butler" credit sequence intact.

The film was shot in Stereoscopic 3-D at studios in Stockholm with exteriors in Denmark.

==Plot== Victorian London, insane asylum to turn into a "love nest", unaware that Jack the Ripper still lives there.

==Cast==
* Ole Søltoft as Jack Armstrong
* Sue Longhurst as Alice Faversham
* Charlie Elvegård as Samson
* Malou Cartwright as Penny Martin Young as Jack the Ripper
* Steven Lund as young Jack
* Diana Dors as Madame Helena
* Joe Grey as Judge Pettibone
* Larry Leonard as Mr. Pendleton Peter Rose as Reverend Faversham
* Julie Bernby as Mrs. Faversham Barbara Hart as Mrs. Armstrong
* Gil Holmes as Mr. Armstrong
* Tina Monell as Marion Faversham

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 