Tom Brown's School Days (1940 film)
{{Infobox film
| name           = Tom Browns School Days
| image          = Tom Browns School Days (1940 film).jpg
| image_size     =
| caption        = DVD edition (2004) Robert Stevenson
| producer       = Charles Graham Baker|C. Graham Baker Gene Towne
| writer         = Thomas Hughes (novel) Walter Ferris Frank Cavett Gene Towne C. Graham Baker Robert Stevenson (add. dialogue)
| starring       = Sir Cedric Hardwicke Freddie Bartholomew Jimmy Lydon
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 86 minutes
| country        = United States English
| budget         =
| gross          =
}} 1940 Coming 1857 novel of the same name by Thomas Hughes.

In this version, emphasis is placed on the development of Headmaster Thomas Arnold and his reformist ideas concerning the English public school.    It was well received by critics, with Variety (magazine)|Variety praising it in a January 1940 review as "sympathetically and skillfully made, with many touching moments and an excellent cast". Hardwickes performance as Arnold was called "one of the best he has ever given," as the veteran actor convincingly tempered the headmasters strict demeanor with "the underlying sympathy, tolerance, quiet humour and steadfast courage" for which Arnold was acclaimed.  Jimmy Lydon as the title character was called "believable and moving in the early portions, but too young for the final moments". 

The June 27, 1940, debut of the film version at New York Citys Radio City Music Hall was chronicled in a photo spread by The New York Times, "showing some of the pastimes, curricular and otherwise", as the fight scene between Tom Brown and Flashman was captioned. 

==DVD version==
The 1940 film version was released on DVD in 2004, with a cover illustration of Dr. Arnold with Tom Brown and Flashman, as the famed educator admonishes the two after their fight.

==Cast==
 
*Cedric Hardwicke as Dr. Thomas Arnold
*Freddie Bartholomew as East
*Jimmy Lydon as Tom Brown
*Josephine Hutchinson as Mrs. Mary Arnold Flashman
*Polly Moran as Sally Harowell
*Hughie Green as Walker Brooke
*Ernest Cossart as Squire Brown
*Alec Craig as Old Thomas
*Gale Storm as Effie

==Reception==
The film recorded a loss of $110,000. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 