Kichiku Dai Enkai
 
{{Infobox film
| name           = Kichiku Dai Enkai
| image          = Kichiku_DVD_Cover.jpg
| caption        =
| director       =Kazuyoshi Kumakiri
| producer       =Kazuyoshi Kumakiri Tomohiro Zaizen
| writer         =Kazuyoshi Kumakiri
| starring       =Shigeru Bokuda Sumiko Mikami Shunsuke Sawada Toshiyuki Sugihara
| music          =
| cinematography = Kiyoaki Hashimoto
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Japan
| language       = Japanese
| budget         =
}}
Kichiku Dai Enkai is a 1997 Japanese film. It was written and directed by Kazuyoshi Kumakiri.

==Story==
In the year 1972, a small group of left-wing students are living at a dingy Tokyo apartment in the mountain village of Karuizawa in Nagano-ken, Japan. While the group leader, Aizawa waits to be released from prison, his promiscuous girlfriend, Masami is left in charge of the group. While waiting for Aizawas release, Masami uses her overt sexuality and engages in promiscuity with the fellow students. As the days pass by, things take a tragic turn. Within a few days of his release, the groups leader soon commits hari-kiri. After learning of Aizawas suicide, the shotgun- toting Masami loses control and the group quickly falls into self-destruction, as sex, distrust, paranoia, and violence overtakes the group. 

==Cast==
*Sumiko Mikami
*Shigeru Bokuda
*Shunsuke Sawada
*Toshiyuki Sugihara

==Production==
 

==References==
 

==External links==
* 
* Giuseppe Sedia (October 2006),   (Italian) at  .

 
 
 
 
 
 
 
 


 
 