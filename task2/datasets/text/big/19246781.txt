Homam (film)
{{Infobox film
| name           = Homam
| image          = Homam_Poster.JPG
| caption        = Promotional poster for the film
| writer         = Kona Venkat   
| story          = J. D. Chakravarthy
| screenplay     = J. D. Chakravarthy
| starring       = Jagapathi Babu J.D. Chakravarthy Mamta Mohandas Madhurima Tuli
| director       = J. D. Chakravarthy
| producer       = Kiran Kumar Koneru 
| music          = Nithin Raikwar   Amar Mohile   
| cinematography = Bharani K Dharan
| editing        = Bhanodaya  
| studio         = Shreya Productions
| distributor    = Sri Venkateswara Creations
| released       =  
| runtime        =
| country        = India
| language       = Telugu
}}
 2008 India, thriller film. Pradeep Rawat & Mahesh Manjrekar portray the supporting roles. The film recorded as Average at box office.

== Plot == Pradeep Rawat) brings him up like his own son and makes him an undercover police officer and hides his identity in a database. However, to nab an international mafia gang led by Daddy (Mahesh Manjrekar), Viswanath sends Malli in undercover to the mafia gang and the latter turns a trusted worker for Daddy. He almost believes Malli as his right hand. With the help of Malli, Viswanath and his team tries to corner Daddy and his gang, but could not arrest them due to lack of evidence. Malli falls in love with a doctor (Mamta Mohandas). He takes him to his ailing mother who was in the hospital. At this juncture, Daddy reveals that his person was in the police department when he is cornered by Viswanaths team without evidence. Chandu (J. D. Chakravarthy), who is also a cop was raised by Daddy. Chandu is in love with his new neighbor (Madhurima Tuli). Chandu is the one who was helping Daddys gang. At the same time, Viswanath also tells Daddy that his man was also with Daddys gang. So both the teams break their heads to find out the black sheep. But they could not do so. Malli once visits the hospital and doctors tell him that he should keep his mother happy always. Malli reveals his identity but is mother did not believe it. To prove his words correct, Malli tells Viswanath to visit his mother once. At the same time, Daddys gang attacks the building and in the melee, Viswanath dies. After a few days, Chandu, in order to escape from the blackmailing tactics of Daddy, shoots him to death in an encounter. Chandus still evil. Malli realizes that Chandu was working for Daddy. Malli proves that Chandus evil. Malli also proves that hes an undercover police officer. One of the officers kills Chandu. Mallis mother gets released from the hospital. Prasad wants Malli to go undercover again. This time, Mallis going undercover in Pakistan.

==Cast==
* Jagapathi Babu as Malli
* J. D. Chakravarthy as Chandu
* Mamta Mohandas
* Madhurima Tuli Pradeep Rawat as DIG Viswanath
* Mahesh Manjrekar as Daddy
* M. S. Narayana
* Krishna Bhagawan
* Brahmaji
* Raja Ravindra
* CVL Narasimha Rao
* Prabhakar
* Ram Jagan
* Tarzan
* Ahuti Prasad

==Soundtrack==
{{Infobox album
| Name        = Homam
| Tagline     = 
| Type        = film
| Artist      = Nithin Raikwar
| Cover       = 
| Released    = 2008
| Recorded    = 
| Genre       = Soundtrack
| Length      = 23:59
| Label       = Aditya Music
| Producer    = Nithin Raikwar
| Reviews     =
| Last album  =  
| This album  = 
| Next album  = 
}}

Music composed by Nithin Raikwar. Lyrics written by Suddala Ashok Teja Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 23:59
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Ye Pagale 
| extra1  = J.D. Chakravarthy,Shivani
| length1 = 3:52

| title2  = Yey Mister Ninne
| extra2  = Nihal,Shivani, Mamta Mohandas
| length2 = 3:31

| title3  = Pedavikidem Kasiro
| extra3  = Jagapati Babu,JD Chakravarthy,Mamta Mohandas
| length3 = 4:32

| title4  = Magaallu Mee Maatalo Madhusri
| length4 = 4:59

| title5  = Katti Naaku Gucchadammo 
| extra5  = Mahathi
| length5 = 3:31

| title6  = Homam
| extra6  = Vinod Rathod
| length6 = 3:31
}}

==Reception== 2002 Cinema Hong Kong crime thriller Infernal Affairs (which this movie was remade as 2006 Hollywood movie, The Departed, directed by Martin Scorsese). Director Chakravarthy corroborated this fact in an interview where he said that the movie The Departed (remade based on the original Infernal Affairs) was inspiration to some extent, but his film varied a lot in its narration and treatment.   

== References ==
 

 
 
 
 
 