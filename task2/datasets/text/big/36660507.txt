A Última Vez Que Vi Macau
 

{{Infobox film
| name           = A Última Vez Que Vi Macau
| image          = 
| alt            = 
| caption        = 
| director       = João Pedro Rodrigues   João Rui Guerra da Mata
| producer       = João Figueiras   Daniel Chabannes   Corentin Sénéchal
| writer         = 
| starring       = Cindy Scrash   João Pedro Rodrigues   João Rui Guerra da Mata
| music          = 
| cinematography = 
| editing        = Raphaël Lefèvre   João Pedro Rodrigues   João Rui Guerra da Mata
| studio         = 
| distributor    = Epicentre Films
| released       =   
| runtime        = 82 minutes
| country        = Portugal
| language       = Portuguese, English, Mandarin
| budget         = 
| gross          = 
}}
A Última Vez Que Vi Macau is a 2012 Portuguese film directed by João Pedro Rodrigues and João Rui Guerra da Mata.  It will be in competition for the Golden Leopard at the 2012 Locarno International Film Festival. It was shot in Macau. 

== Synopsis ==
Thirty years later I’m on my way to Macao where I have been since I was a child. I got an e-mail, in Lisboa, from Candy, a friend I hadn’t heard from in ages. She told me that she had been involved yet again with the wrong men and asked me to go to Macao where “strange and scary things” were happening.
Tired, after a long flight, I’m approaching Macao aboard the jetfoil which will take me back to the happiest time in my life.

==References==
 

==External links==
* 

 

 
 
 
 


 