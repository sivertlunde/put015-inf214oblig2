Miss Arizona (film)
 
{{Infobox Film
| name           = Miss Arizona
| image          = Miss_Arizona_(film).jpg
| caption        = Film poster
| director       = Pál Sándor (director)|Pál Sándor
| producer       = Jacopo Capanna
| writer         = Alfredo Gionotti
| narrator       = 
| starring       = Marcello Mastroianni
| music          = Armando Trovajoli
| cinematography = Elemér Ragályi
| editing        = Nino Baragli
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Miss Arizona is a 1987 Hungarian drama film directed by Pál Sándor (director)|Pál Sándor.   

==Cast==
* Marcello Mastroianni - Sandor Rozsnyai
* Hanna Schygulla - Mitzi Rozsnyai
* Alessandra Martines - Marta
* Urbano Barberini - Stanley
* Augusto Poderosi - German officer
* Juli Básti - Eva
* Dorottya Udvaros - Zsuzsa
* Gyula Szabó - Rozsnyai (voice)
* Mária Varga - Mitzi (voice)
* Kati Kovács - Mitzi (singing voice)
* Anna Fehér - Mitzi (voice)
* János Csernák - (voice)
* Berta Domínguez D. - (as Berta Domínguez)
* Hédi Temessy - (voice)
* Pál Mácsai - (voice)

==Critical reception==
Dan Pavlides of All Movie Guide said that the film had "uneven editing in places suggests that a lot of film ended up on the cutting-room floor." 

==References==
 

==External links==
* 

 
 
 
 
 
 