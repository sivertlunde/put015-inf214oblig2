Open Season (2006 film)
{{Infobox film
| name          = Open Season
| image         = Open Season.jpg
| border        = yes
| caption       = Theatrical release poster
| director      = Jill Culton Roger Allers Anthony Stacchi  (co-director)  Steve Moore John B. Carls
| writer        = Steve Bencich Ron J. Friedman Steve Moore
| starring      = Martin Lawrence Ashton Kutcher Gary Sinise Debra Messing Billy Connolly Jon Favreau Georgia Engel Jane Krakowski Gordon Tootoosis Patrick Warburton
| music         = Ramin Djawadi
| editing       = Ken Solomon Pam Ziegenhagen
| studio        = Sony Pictures Animation
| distributor   = Columbia Pictures
| released      =  
| runtime       = 86 minutes 
| country       = United States
| language      = English
| budget        = $85 million   
| gross         = $197,309,027 
}} IMAX 3D video game for the film was released on multiple platforms.

==Plot== garage of park ranger Beth, who raised him since he was a cub. One day, the hunting fanatic Shaw drives into town with the one-antlered deer Elliot strapped to the hood of his truck. Boog frees Elliot at the last minute and against his better judgment, before Shaw can go after him. Boog never expects to see his "buddy" again. Elliot follows Boog home and finds him sleeping in the garage and starts to throw rabbits at the window. He tells him to be "free" from his garage captivity and introduces Boog to a world of sweet temptations outside of the garage that he has unknown. When Boog becomes intoxicated with sugar sweets, events quickly spiral out of control as the two ransack the towns grocery store. Elliot escapes before Boog is caught by a friend of Beth, police officer Gordy. At the nature show, Elliot who is being chased by Shaw, sees Boog who attacks him, causing the whole audience in the show to panic before Boog threatens to hurt Elliot. Shaw attempts to shoot Boog, but Beth sedates them with a tranquilizer gun before he could, thanks to Gordy. Shaw flees before he can arrest him for shooting a gun in the town. The two trouble-makers are released into the Timberline National Forest, only three days before open season starts, but they are set over the falls, where they will be safe.
 current makes the doll flout out of Boogs paw. They ended up in a waterfall with the flood washes everyone in the forest before the water recedes. Boog then sees a teddy bear shape in the mud and thinks it is Dinklemen, but it turns out it is just a rabbit covered in mud.

At first everyone blames Boog who accuses Elliot of lying to him about leading him home. Elliot admits he thought that if Boog spent time with him, he would befriend him. Boog leaves to unwittingly finds Shaws log cabin. Shaw returns and talks to his gun "Loraine" and says he would take back what is his, discovers him (like Goldilocks and the Three Bears), and pursues him to the city road where Boog happens upon the glowing lights of Timberline. Instead of deserting his companions, Boog helps the other animals defend themselves using supplies taken from Bob and Bobbies (two "scientists" looking for Bigfoot) RV while their pet dachshund Mr. Weenie joins the wilds. The next day, Boog leads a rampage against the hunters, sending them running after McSquizzy blows up their trucks with a propane tank named "Mr. Happy". Shaw returns for a final battle and shoots Elliot in the process, which enrages Boog to tie up Shaw with his own gun. Boog rushes over to Elliots body but soon finds that Elliot survived the shot, only losing his second antler in the fight. Beth returns to take Boog back home where he will be safe, but instead he stays with his friends and all of the animals in the forest.

During the credits, Shaw is seen tarring and feathering|tarred, feathered and tied on the top of Bobbie and Bobs RV, who believe him to be Bigfoot.

==Voice cast==
* Martin Lawrence as Boog, a 900-pound, smooth talking grizzly bear.
* Ashton Kutcher as Elliot, a hyperactive and dull-witted, albeit clever mule deer.
* Gary Sinise as Shaw, the nastiest hunter in Timberline and Beths arch-rival.
* Debra Messing as Beth, a park ranger who raised Boog as a cub.
* Billy Connolly as McSquizzy, a grumpy, old Scottish-accented Eastern gray squirrel.
* Jon Favreau as Reilly, a hard-working North American beaver.
* Georgia Engel as Bobbie, an obese woman who is Bobs wife and Mr. Weenies owner.
* Jane Krakowski as Giselle, a beautiful mule deer doe and Elliots love interest. 
* Gordon Tootoosis as Gordy, Timberlines sheriff and Beths friend.
* Patrick Warburton as Ian, a vain mule deer stag and the leader of his herd.
* Cody Cameron as Mr. Weenie, Bob and Bobbies domesticated, German-accented dachshund.
* Danny Mann as Serge, a French-accented mallard duck.
* Matthew W. Taylor as Deni, a dumb, somewhat crazy and laconic mallard duck and Serges brother. / Buddy, a blue North American porcupine who searches for friends.
* Nika Futterman as Rosie, a Spanish-accented striped skunk.
* Michelle Murdocca as Maria, a striped skunk who is Rosies identical sister.
* Fergal Reilly as OToole, a North American beaver and one of Reillys men.

==Production==
  and Jill Culton, the directors of the film, at the 34th Annie Awards]] Steve Moore, CGI animated film Open Season. 

The film location was inspired by the towns of Sun Valley, Idaho and McCall, Idaho, and the Sawtooth National Forest. References to the Lawn Lake, Colorado, Dam flood, Longs Peak, and other points of interest in the area are depicted in the film. 

The Sony animation team developed a digital tool called shapers that allowed the animators to re-shape the character models into stronger poses and silhouettes and subtle distortions such as squash, stretch, and smears, typical of traditional, hand-drawn animation. 

===Casting===
To choose the voice cast, Culton blindly listened to audition tapes, unknowingly picking Lawrence and Kutcher for the lead roles.  Their ability to improvise significantly contributed to the creative process. "They really became meshed with the characters," said Culton.    Until the films premiere, Lawrence and Kutcher never met during production. 

==Reception==

===Critical reception===
Open Season received mixed reviews from critics. Critics of Rotten Tomatoes gave the film 48% (based on 100 reviews) with the consensus "Open Season is a clichéd palette of tired jokes and CG animal shenanigans that have been seen multiple times this cinematic year." 

  gave the film a mixed review, saying, "Its just okay, the animation is uninspired." 

===Box office===
Open Season opened #1 with $23 million on its opening weekend. It grossed $85.1 million in the United States and $112.2 million in foreign countries, making $197.3 million worldwide. 

===Accolades=== Annie Awards, including Best Animated Feature (lost to Cars (film)|Cars), Best Animated Effects, Best Character Design in a Feature Production, Best Production Design in a Feature Production, and Best Storyboarding in a Feature Production. 

==Home media== UMD Video on January 30, 2007.  It includes a new animated short called Boog and Elliots Midnight Bun Run. The film was later released to 3D Blu-ray on November 16, 2010. 

==Video game==
 
A video game based on the film was released on September 18, 2006, for PlayStation 2, Xbox, Xbox 360, Nintendo DS, Nintendo Gamecube, Game Boy Advance, PlayStation Portable, and Microsoft Windows.  For Wii, it was released on November 19, 2006, together with the consoles launch. 

==Soundtrack==
{{Infobox album
| Name        = Open Season
| Type        = Soundtrack
| Artist      = Paul Westerberg
| Cover       = Open season - soundtrack.jpg
| Released    = September 26, 2006
| Recorded    =
| Genre       =
| Length      = 41:29
| Label       = Lost Highway Records
| Producer    = Lou Giordano Dana Gumbiner
| Last album  =The Resterberg  (2005)
| This album  =Open Season (2006)
| Next album  =  (2008)
}} The Replacements. Rolling Stone gave the films soundtrack three stars out of five,  as did Allmusic. 

Track list:
{{tracklist
| all_music       = Paul Westerberg, except as noted
| extra_column    = Artist
| total_length    = 41:29
| title1          = Meet Me In The Meadow
| length1         = 4:29
| title2          = Love You In The Fall
| length2         = 2:50
| title3          = I Belong"
| length3         = 4:13
| title4          = I Wanna Lose Control (Uh-Oh)
| extra4          = Deathray
| length4         = 2:01
| title5          = Better Than This
| length5         = 2:55
| title6          = Wild Wild Life
| extra6          = Talking Heads
| length6         = 3:40
| title7          = Right to Arm Bears
| length7         = 2:05
| title8          = Good Day
| length8         = 4:18
| title9          = All About Me
| length9         = 4:32
| title10         = Wild As I Wanna Be
| extra10         = Deathray
| length10        = 2:54
| title11         = Whisper Me Luck
| length11        = 4:16
| title12         = I Belong (Reprise)
| extra12         = Pete Yorn
| length12        = 3:16
}}

Open Season - Original Motion Picture Soundtrack (10   LP) includes two songs that did not appear on the soundtrack CD: an alternative version of "I Belong" and a Paul Westerbergs version of "Wild as I Wanna Be." 

===Charts===
{| class="wikitable"
! Chart (2009)
! Peak position
|-
|U.S. Billboard (magazine)|Billboard Top Soundtracks 
|align="center"| 15
|-
|}

==Sequels==
 
Open Season was followed by two   (2009) and Open Season 3 (2011).

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 