As Above, So Below (film)
 
 
{{Infobox film
| name           = As Above, So Below
| image          = As Above, So Below Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = John Erick Dowdle
| producer       = Patrick Aiello Alec Hedlund
| writer         = Drew Dowdle John Erick Dowdle Ben Feldman Edwin Hodge François Civil Marion Lambert Ali Marhyar
| music          = Max Richter
| cinematography = Léo Hinstin
| editing        = Elliot Greenberg
| studio         = Legendary Pictures Universal Pictures
| released       =  
| runtime        = 93 minutes  
| country        = United States
| language       = English
| budget         = $5 million 
| gross          = $40.2 million   
}} found footage horror film Universal Pictures, Ben Feldman, Edwin Hodge, François Civil, Marion Lambert, and Ali Marhyar.

==Plot==
As Above So Below follows the story of Scarlett Marlowe (Perdita Weeks), a young alchemy scholar, continuing her dead fathers (Roger Van Hool) work searching for the philosophers stone, a legendary alchemical substance said to be capable of turning base metals such as lead into gold and granting eternal life, discovered by Nicolas Flamel.
 Ben Feldman), Catacombs beneath Paris, France. George explicitly refuses to go, but is driven into the caves with the group when they are pursued by a policeman. After crawling through a narrow tunnel which collapses, they find themselves before a door that Papillion is reluctant to breach, as the only people who have gone through, including Paps friend La Taupe (Cosme Castro), have never been seen again.

After proceeding deeper into the catacombs, they encounter La Taupe, who agrees to guide them out, and informs them that the way out is down. They eventually find a tomb with a preserved Templar Knight, a mound of treasure, and the Flamel Stone. Removing the stone, Scarlett realises too late that the treasure in the other room is a trap, and the room collapses. La Taupe is lost under the rubble and is abandoned by the group.

With the Flamel Stone, Scarlett magically heals the groups injuries. They find a drawing of a door on the ceiling along with a Gnostic Star of David, symbolizing "As above, so below," meaning there is a door hidden in the floor. Going through the opening, they find a tunnel marked with the phrase "Abandon all hope, ye who enter here" in Greek, identical to the entrance to Hell.

Passing through, they find a dark reflection of the crypt they just left, including La Taupe, who attacks Souxie by smashing her head into the rock floor, killing her. They realise they must continue and go deeper. Along the way, Benji the cameraman is pushed down a hole to death. Later they encounter a burning car and occupant (who is the same young man who told Scarlett to find Papillon as a guide), an incident from Papillons past who pulls in Papillon and then sinks into the floor. Papillon becomes partially submerged in the floor. They attempt to pull Papillon out of the ground, but to no avail. As they continue, they see apparitions of terrifying spirits and demons. Statues in the wall come to life and one attacks George, biting open his throat. After trying to drag him further, George murmurs "Vitriol," another riddle from earlier, and Scarlett realises the Flamel stone itself is yet another trap, and that only by returning it will she find the real stone, and runs off leaving Zed and George to hide, while seemingly knocking the apparitions out of her way.

She races back to the crypt, returns the stone and looks into a polished mirror on the wall where the stone was hidden, realising that her quest for the stone and belief in the philosophy "As above, so below," (explained as simply willing acts of magic to happen makes it so) has granted her the magical abilities of the stone. She returns to George, kissing him as she holds his wounds, healing them. She then explains that where they are is about confronting their regrets and rectifying their respective pasts, such as Georges brother who drowned after he got lost looking for help and her father who committed suicide, as he had called her that night but she refused to pick up the phone. She says they must accept their guilt in order to absolve themselves and escape. Zed confesses he has a son who he never took responsibility for and denied as his own, explaining the vision of a little boy that plagued him during their journey. With the cloaked figures (supposedly demons) closing in, the three accept what they have done before a hole they have discovered. Together they jump down, even though they know there is no logical way for them to survive. As they land at the bottom, they look up, see the hole they came down is no longer there. They find a manhole which they open, and come out onto a street in Paris. The remaining three embrace and Zed walks into the city.

A clip of Scarlett, telling her goals of the descent, is shown. The clip was recorded before the trip in the catacombs when Scarlet was oblivious of the horrors therein.

==Cast==
 
*   to recredit her father, an Alchemy Historian who hanged himself after allegedly going crazy. Scarlett has 2 PhDs (one in Urban Archaeology and one in Symbology), a Masters in Chemistry, can speak 4 languages along with 2 dead ones, and is a black belt in the martial art of Krav Maga.   Ben Feldman as George, Scarlets old love interest. George and Scarlett had initially separated prior to the film because she was more focused on her work than on the relationship.
* Edwin Hodge as Benji, Scarletts friend and motivated cameraman.
* François Civil as Papillon "Pap", the groups guide around the catacombs as he is familiar with the areas layout. 
* Marion Lambert as Souxie, Papillons girlfriend. 
* Ali Marhyar as Zed, Papillons friend
* Cosme Castro as La Taupe, The Mole in English, Papillons friend who disappeared in the catacombs two years prior while hunting for the treasure rumored to be in the catacombs with the philosophers stone.
* Hamid Djavadan as Reza, Mr. Marlowes friend.
* Théo Cholbi as Gloomy Teenager
* Emy Lévy as Tour Guide
* Roger Van Hool as Mr. Marlowe, Scarletts mentally unstable father who committed suicide by hanging after going insane looking for the legendary stone. He appears recurrently throughout the film as an apparition haunting Scarlett. 
* Olivia Csiky Trnka as Strange Young Woman, The leader of a group of female cultists who is seen throughout the film stalking the group.
* Hellyette Bess as Strange Old Woman
* Aryan Rahimian as Iranian Armed Guard
* Samuel Aouizerate as Danny
* Kaya Blocksage as Female Curator 
 

==Production==

===Filming===
Production for the film actually commenced in the real catacombs of Paris. There was very little use of props as the actors had to use the environment around them. Production in the actual catacombs was difficult for the cast and especially the crew as there was no electricity or cell phone service in the centuries old catacombs. 

==Distribution==

===Marketing=== trailer of the film was revealed on April 24, 2014.  YouTuber PewDiePie and his girlfriend CutiePieMarzia promoted the film by embarking on a quest into the catacombs, where they would be scared in a variety of ways. 

===Home media===
As Above, So Below was released on DVD and Blu-ray on December 2, 2014. 

==Reception==

===Critical reception===
 
As Above, So Below received generally negative reviews from critics. On Rotten Tomatoes, the film currently has a rating of 27%, based on 60 reviews, with an average rating of 4.4/10; the general consensus states, "After an intriguing setup that threatens to claw its way out of found-footage overkill, As Above, So Below plummets into clichéd mediocrity."  On Metacritic, the film currently has a rating of 38 out of 100, based on 23 critics, indicating "generally unfavorable reviews." 

===Box office===
The film grossed $8.3 million its opening weekend, finishing in 3rd place. As of November 4, 2014, the film has grossed $21.2 million in North America and $18.9 million in other territories, for a total gross of $40.1 million.   

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 