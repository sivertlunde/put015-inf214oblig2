The Stalking Moon
{{Infobox film
| name           = The Stalking Moon
| image          = TheStalkingMoonPoster.jpg
| image_size     =
| caption        =
| director       = Robert Mulligan
| producer       = Robert Mulligan Alan J. Pakula
| writer         = T. V. Olsen (novel) Wendell Mayes (adaptation) Alvin Sargent
| starring       = Gregory Peck Eva Marie Saint
| music          = Fred Karlin
| cinematography = Charles Lang
| editing        = Aaron Stell
| distributor    = National General Pictures (1968, original) Warner Bros. (2008, DVD)
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2.6 million (US/ Canada rentals) 
}}
 1968 western film in Technicolor starring Gregory Peck and Eva Marie Saint. It is directed by Robert Mulligan and based on the novel of the same name by T.V. Olsen.

==Plot==
U.S. Army soldiers round up a group of Indians, mostly women and children.  Surprisingly, they find among them a white woman and her half-Indian son.

Sam Varner (Gregory Peck) is a scout retiring from the Army to his ranch in New Mexico.  He agrees to escort Sarah Carver (Eva Marie Saint) and her son after she begs him.  She wants to leave immediately rather than wait five days for a military escort.

Varner takes them to a stage coach stop called Hennessy.  The boy runs away during the night.  Varner and Sarah go looking for him as a dust storm begins.  They find the boy and then hole up (literally) to wait out the storm.

When they return to the station, everyone there is dead, killed by the boys Indian warrior father, Salvaje (played by Nathaniel Narcisco).  Salvaje is greatly feared even among his own people - and with good reason: he is known to be a silent and ruthless killer.  Salvaje means "Ghost" in Apache, or in their own tongue: "He Who Is Not Here", meaning a dead man.   

Varner is upset that the womans impatience to leave has cost the people at the station their lives.  When the stagecoach does arrive, Varner puts the woman and boy on it and follows them to a rail station called Silverton.  He trades government letters of transport for train tickets to Topeka, Kansas.

After some careful consideration, Varner decides to invite the woman and her son to accompany him to his ranch where she can cook for him and an old man, Ned (played by Russell Thorson), who takes care of the ranch.  Sam sells his horse and they take the train to New Mexico.

They uneasily try to coexist.  The woman and her son are not talkative despite Sams best efforts. His friend Nick, a half-breed scout he has been friends with for ten years, shows up.  Nick tells him that Salvaje killed everyone at Silverton and even killed Sams old horse.  Its apparent that Salvaje is coming to the ranch to retake his son.  

Ned goes outside to feed his dog and finds it killed with an arrow.  In a blind rage, he runs into the trees after a Salvaje.  Sam tries to bring him back, but cant find him.  Shortly after, he hears Neds death scream.  Sam decides to go after Salvaje and create an opportunity for Nick to get a clear shot.  But, when Sam is being tracked, Nick jumps up to warn him and Salvaje kills him.  Nick dies in Sams arms.

Salvaje enters the ranch house through a window.  Sam blows out the kerosene lamp in order to hide in a dark corner.  Sam shoots at him with a rifle and Salvaje flees, but he leaves a trail of blood.

Sam trails him and steps into a booby-trap that Salvaje has rigged with a knife. Sam is stabbed in the left thigh and bleeds profusely enough that he has to apply a tourniquet.  The two men fight and eventually Sam shoots Salvaje three times as the warrior falls atop him, dying.

Sam manages to walk, stumble, and crawl back to the house, where Sarah rushes out to help him.

==Cast==
* Gregory Peck as Sam Varner
* Eva Marie Saint as Sarah Carver
* Robert Forster as Nick Tana
* Noland Clay as Boy
* Russell Thorson as Ned
* Frank Silvera as Major
* Lonny Chapman as Purdue
* Lou Frizzell as Stationmaster
* Henry Beckman as Sergeant Rudabaugh
* Charles Tyner as Dace Richard Bull as Doctor
* Sandy Brown Wyeth as Rachel
* Joaquín Martinez as Julio
* Boyd Morgan as Shelby
* Nathaniel Narcisco as Salvaje
* Roy Munson

==Production==
This film marked the reunion between director Robert Mulligan, producer Alan J. Pakula and actor Gregory Peck, six years after their collaboration on To Kill a Mockingbird (film)|To Kill A Mockingbird.

The movie was filmed on location in Red Rock Canyon, Nevada, Valley of Fire State Park, Nevada, and at the Samuel Goldwyn Studios in Hollywood.

==Reception==
Vincent Canby of the New York Times said: "There are some lovely individual things in The Stalking Moon—broad, Western landscapes, a moment in which Miss Saint suddenly catches her haggard look reflected in a train window, a scene in which Peck buys a railroad ticket at a desert crossing that explains the awful, dislocating distances on the frontier. Those, however, are random touches. ...Like Peck, the film moves stolidly forward with more dignity than excitement. ...Quite consciously, Mulligan and Alvin Sargent, who wrote the screenplay, have kept their focus on the poor whites, but unfortunately, none of them is especially interesting. They remain outlines for characters — the lonely frontiersman, the woman who has gone through horrors that are unspeakable (at least unspeakable in this film) to survive Indian captivity, and the small boy torn between two cultures."  

==DVD==
The Stalking Moon was released to DVD by Warner Home Video on August 26th, 2008 as a Region 1 DVD.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 