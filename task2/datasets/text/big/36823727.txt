Lucky Devils (1933 film)
{{Infobox film name            = Lucky Devils image           = director        = Ralph Ince producer        = writer          = Agnes Christine Johnston Ben Markson story           = Casey Robinson Bob Rose starring  William Boyd Bruce Cabot music           = cinematography  = editing         = studio          = RKO Radio Pictures distributor     = RKO Radio Pictures released        =   country         = United States runtime         = 60-70 minutes language        = English budget          = $117,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p55  gross           = $285,000 
}} William Boyd and Bruce Cabot, and features an early appearance by Lon Chaney Jr..

==Cast==
* William Boyd as Skipper Clark (as Bill Boyd)
* Bruce Cabot as Happy White
* William Gargan as Bob Hughes
* William Bakewell as Slugger Jones
* Lon Chaney Jr. as Frankie (as Creighton Chaney)
* Bob Rose as Rusty (as Robert Rose) Dorothy Wilson as  Fran Whitley
* Julie Haydon as Doris Jones
* Sylvia Picker as Midge
* Gladden James as Neville Silverman
* Edwin Stanley as Mr. Spence
* Roscoe Ates as Gabby (as Rosco Ates)
* Phyllis Fraser as Toots
* Betty Furness as Ginger
* Alan Roscoe as Mr. Hacket
* Charles Gillette as Cameraman
* Rochelle Hudson as Movie Star
* Ward Bond as Crewman (uncredited)

==Production== Bob Rose King Kong.

==Reception==
The film ended up making a profit of $65,000. 

== References ==
 

== External links ==
*  
* 

 
 
 
 
 
 
 

 