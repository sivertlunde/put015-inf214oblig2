Tillie and Gus
Tillie American comedy film starring W.C. Fields and directed by Francis Martin. The screenplay by Walter DeLeon is based on a short story by Rupert Hughes entitled Dont Call Me Madame.

==Plot==
 saloon in Danville to investigate the dead mans estate and the possibility of an inheritance.
 attorney Phineas Pratt (Clarence Wilson) claims the man died in debt, but he actually has swindled his daughter Mary Sheridan (Julie Bishop, billed under her real name, Jacqueline Wells) out of her rightful inheritance, including the family home, forcing her to move with her husband Tom Sheridan (Phillip Trent) and their infant son, King (Baby LeRoy) to a dilapidated ferry called the Fairy Queen&mdash;supposedly the one item left of the estate.
 missionaries newly returned from Africa by their relatives. Tillie plans to sell the boat and split the profits, but they become suspicious when Pratt expresses an inordinate interest in acquiring the seemingly unseaworthy boat, and they decide to help Mary and Tom refurbish it. Pratt, who has just purchased his own boat, the Keystone, tries to eliminate the competition by convincing the state inspection board to deny the Sheridans a ferry government-granted monopoly|franchise.

It is decided that the outcome of a Fourth of July boat race will determine who is awarded the franchise. Comic mayhem ensues when Gus does everything in his power to sabotage their rival, ultimately coming out ahead in the end. Tom tells Gus, "That ferryboat race was the worlds biggest gamble," to which Gus replies, "Well, dont forget, Lady Godiva put everything she had on a horse!"

==Production notes==
The climactic ferry race in the Paramount Pictures release was filmed on Malibu Lake.

==Cast (in credits order)==
*W.C. Fields as Augustus Winterbottom
*Alison Skipworth as Tillie Winterbottom
*Baby LeRoy as The King
*Julie Bishop (actress) as Mary Sheridan
*Phillip Trent as Tom Sheridan Clarence Wilson as Phineas Pratt George Barbier as Captain Fogg
*Barton MacLane as Commissioner McLennan
*Edgar Kennedy as Judge Robert McKenzie as Defense Attorney
*Ivan Linow as The Swede

==Principal production credits== Producer ..... Douglas MacLean
*Cinematography ..... Benjamin F. Reynolds Art Direction ..... Hans Dreier, Harry Oliver

==Critical reception==
In his review in the New York Times, Mordaunt Hall described the film as "a cheery absurdity" and added, "Insane as are the doings in this concoction, they succeed in being really funny. It is the sort of thing admirably suited to Mr. Fields peculiar genius." 

Time (magazine)|Time observed, "Part parody of Tugboat Annie, part pure farce, Tillie and Gus is one of the pleasanter chapters in the long and happy career of W. C. Fieldss famed unlighted cigar." 

==See also==
* Min and Bill 1930 film
* Tugboat Annie 1933 film

==References==
 

==External links==
* 
* 

 
 
 
 
 