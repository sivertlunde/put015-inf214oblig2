Voyager (film)
{{Infobox film
| name           = Voyager
| image          = Voyager (film) Poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Volker Schlöndorff
| producer       = Eberhard Junkersdorf
| writer         = 
| screenplay     = Rudy Wurlitzer
| based on       =  
| narrator       = Sam Shepard
| starring       = {{Plainlist|
* Sam Shepard
* Julie Delpy
* Barbara Sukowa
}}
| music          = Stanley Myers
| cinematography = {{Plainlist|
* Giorgos Arvanitis
* Pierre Lhomme
}}
| editing        = Dagmar Hirtz
| studio         = {{Plainlist|
* Action Films
* Bioskop Film
* Stefi 2
}}
| distributor    = Castle Hill Productions  
| released       =  
| runtime        = 117 minutes
| country        = Germany
| language       = English
| budget         = 
| gross          = $516,517  
}} Homo Faber by Max Frisch, the film is about a successful engineer traveling throughout Europe and the Americas whose world view based on logic, probability, and technology is challenged when he falls victim to fate, or a series of incredible coincidences.   

Voyager won the Bavarian Film Award for Best Production (Eberhard Junkersdorf), the German Film Award for Shaping of a Feature Film, and the Guild of German Art House Cinemas Award for Best German Film. It was also nominated for three European Film Awards for Best Film, Best Actress (Julie Delpy), and Best Supporting Actress (Barbara Sukowa), as well as a German Film Award for Outstanding Feature Film.   

==Plot==
In April 1957, engineer Walter Faber (Sam Shepard) is waiting to board a flight from Caracas, Venezuela to New York City when he meets a German, Herbert Hencke (Dieter Kirchlechner), who reminds him of an old friend. Before takeoff, Walter decides not to board the airplane, but when a flight attendant discovers him still in the terminal, she escorts him aboard. During the flight, the airplane develops engine trouble and crash lands in the desert near the Sierra Madre Oriental mountains. 

While the passengers and crew wait to be rescued, Walter discovers that Herbert Hencke is the brother of his old friend, Joachim (August Zirner), whom Walter has not seen since he left Zürich, Switzerland, twenty years ago. He also learns that Joachim married Walters former girlfriend Hannah (Barbara Sukowa), that they had a child together, and that they are now divorced. After writing a letter to his current married girlfriend, Ivy, ending their relationship, Walter thinks back on his days in Zurich falling in love with Hannah. He remembers proposing marriage to her after she revealed she was pregnant, and that she refused, saying she would terminate the pregnancy.

The passengers and crew are rescued and brought to Mexico City, where Herbert prepares to continue on to see his brother Joachim at his tobacco farm in Guatemala. Walter decides to accompany Herbert to see his old friend again. The journey is long and difficult. When the two finally arrive at the tobacco farm, they find Joachim has hanged himself.

Back in New York City, Walter returns to his apartment, only to find Ivy waiting for him. She received Walters letter ending their relationship, but simply does not acknowledge it. Needing to escape, he decides to leave for his Paris business trip a week early and take an ocean liner rather than fly. Feeling he has "started a new life" aboard the ship, Walter meets a beautiful young woman, Elisabeth Piper (Julie Delpy), whom he begins to call Sabeth. They spend time together, playing ping-pong, exploring the ship, and falling in love. On the last night of the voyage, Walter asks her to marry him, but she does not know how to respond, and they part without saying goodbye.

In Paris, Walter looks for Sabeth at the Louvre and they reunite. He offers to drive her to Rome, rather than have her hitchhike as shes planned, and she agrees. They drive south through France and stop for the night at a hotel near Avignon. Late in the evening, Sabeth comes to Walters room and they make love. They continue on their way through France and Italy, stopping at Florence and Orvieto before arriving in Rome. At Palatine Hill, Walter is captivated by the sculpture, Head of a Sleeping Girl. Walter learns that Sabeth is the daughter of his former girlfriend, Hannah—and possibly his own daughter. He becomes distant and refuses to tell Sabeth of what he suspects; Sabeth is upset with Walters sudden unexplained strange behavior. Walter finally reveals to Sabeth that he knew her mother and father in Zurich in the 1930s. 

Walter and Sabeth make their way to Greece, but Walter is troubled by the possibility that Sabeth may be his daughter. They sleep under the stars on a hill overlooking the Mediterranean Sea, and at sunrise Walter goes for a swim. While sleeping, Sabeth is bitten by a snake, jumps up in alarm, and falls, hitting her head on a rock. Walter rushes her to a hospital in Athens. Hannah arrives to look after her daughter. 

While Sabeth is treated at the hospital, Walter stays with Hannah at her house, recounting how he met Sabeth on the ship and how they travelled through Europe together. Hannah reveals that Sabeth is his daughter and asks him, "Walter, how far did you go with the child?" Devastated, Walter acknowledges he had sexual relations with her. Sabeth recovers from the snake bite and appears to be gaining strength, but suddenly dies from the head injury. 
 Athens Airport, Hannah and Walter embrace and say goodbye. Walter sits dejected in the airport terminal. When his flight is called, he remains seated, pondering his fate and existence.   

==Cast==
* Sam Shepard as Walter Faber
* Julie Delpy as Sabeth
* Barbara Sukowa as Hannah
* Dieter Kirchlechner as Herbert Hencke
* Traci Lind as Charlene
* Deborra-Lee Furness as Ivy
* August Zirner as Joachim Hencke
* Thomas Heinze as Kurt
* Bill Dunn as Lewin
* Peter Berling as Baptist
* Lorna Farrar as Arlette
* Kathleen Matiezen as Lady Stenographer
* Lou Cutell as New York Doorman
* Charley Hayward as Joe
* Irwin Wynn as Dick
* Warwick Shaw as 2nd officer 

==Production==

===Screenplay=== Homo Faber complex role during World War II. Frisch saw parallels between Switzerlands neutrality during the war and the thoughtlessness and casual neglect of the Swiss engineer toward the young Jewish woman, who bears his child on the eve of the war. The main character shows some resemblance to Frisch, a frustrated architect turned author.   
 Death of a Salesman with Dustin Hoffman as Willy Loman. By 1987, with his career stalled and his marriage breaking up, the 47-year-old Schlöndorff could perhaps identify with the novels main character, Walter Faber, and he decided to make a film based on the novel. 

After acquiring the film rights, Schlöndorff turned to Rudy Wurlitzer, who had written numerous screenplays, including Two-Lane Blacktop (1971), Pat Garrett and Billy the Kid (1973), and Walker (film)|Walker (1987). Wurlitzer and Schlöndorff made several important changes for the screenplay. The story became less a political morality tale than a haunting narrative of personal destruction.  While retaining the European settings and supporting characters, he changed the nationality of the protagonist, transforming Walter Faber into an American engineer. During the screenplays development, Schlondorff visited Max Frisch at his Munich apartment, seeking out and receiving the authors guidance and approval for the significant changes being made to his story. 

===Filming locations===
Voyager was filmed in the following locations:
* Germany
* Greece
* Mexico
* New York City, New York, USA
* Paris, France
* Perugia, Umbria, Italy 
* Rome, Lazio, Italy
* RMS Queen Mary   

==Critical response==
The film received generally positive reviews with some qualifications. In his review in The New York Times, Vincent Canby called the story "a modern variation on the Oedipus myth" and had a mixed reaction to the film, writing, "Voyager has been handsomely photographed in all sorts of exotic locations ... It is well acted by Mr. Shepard and Miss Delpy, and is full of moments so particular and odd that they invite belief. Yet its tale of fate and predestination seems, at last, to be not timeless but absurd."   

In his review in the Chicago Sun-Times, Roger Ebert gave it three stars and offered a mixed response, writing, "The end of Voyager does not leave us with very much ... Thinking back, we realize weve met some interesting people and heard some good talk, and that its a shame all those contrived plot points about incest got in the way of what was otherwise a perfectly stimulating relationship. This is a movie that is good in spite of what it thinks its about."    

In his review in The Washington Post, Desson Howe also offered a mixed response, writing, "To watch Voyager is to remember old-fashioned elements from the European films of  , the then-exciting work of the Antonionis and Fellinis. ... Delpys intrinsic mystique (not her acting) suggests the mystery heroines of old art films such as La Dolce Vita ... With a healthy dose of viewer indulgence, and the occasional blind eye to arty excess, theres something well-intentioned and uncommercial to celebrate."   

In their review in Spirituality & Practice, Frederic and Mary Ann Brussat called Voyager a "mesmerizing screen interpretation of a Max Frisch novel" with Sam Shepard delivering "his best performance in years." The Brussats conclude, "Voyager impresses with its top-drawer performances, its globe-trotting sophistication, and its literary treatment of fate."   

In his DVD Savant Review in DVD Talk, Glenn Erickson gave the "uncompromised adaptation" of Max Frischs novel an "Excellent" rating, writing, "Schlöndorff does a fine job of creating a 1957 ambience without Hollywood trappings ... The production helped revive an extinct Constellation "Connie" prop airliner for the movie, and staged an impressive desert crash without special effects. Glimpses of New York streets give an impression of late-50s cars, and a few costumes do the rest." Regarding Julie Delpys performance, Erickson noted, "The film positively glows thanks to the presence of 20 year-old Julie Delpy, who single-handedly gives the film the breath of life and the warmth of love. An overlooked commercial non-performer, Voyager is a very pleasurable experience."   

In his review in DVD Beaver, Gary W. Tooze called it "a very worthy film."   

==Awards and nominations==
The film won the Bavarian Film Award for Best Production (Eberhard Junkersdorf), the German Film Award for Shaping of a Feature Film, and the Guild of German Art House Cinemas Award for Best German Film. It was also nominated for three European Film Awards for Best Film (Eberhard Junkersdorf), Best Actress (Julie Delpy), and Best Supporting Actress (Barbara Sukowa), as well as a German Film Award for Outstanding Feature Film. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 