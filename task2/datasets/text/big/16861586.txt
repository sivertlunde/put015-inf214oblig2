Divya Shakti
 
{{Infobox film
| name           = Divya Shakti
| image          = Divya Shakti.jpg
| image_size     =
| caption        =
| director       = Sameer Malkan
| producer       = Dinesh Patel 
| writer         = Sameer Malkan Dilip Shukla
| narrator       =
| starring       = Ajay Devgan  Raveena Tandon  Satyendra Kapoor
| music          = Nadeem-Shravan  
| cinematography = Romesh Bhalla   
| editing        = Suresh Chaturvedi 
| distributor    = Sonu Films International
| released       = 19 February 1993
| runtime        = 180 min
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} 1993 Bollywood action film directed and written by Sameer Malkan. The film stars Ajay Devgan, Raveena Tandon and Satyendra Kapoor.

==Plot==
An idealistic journalist (Devgan) gets weary & tired of witnessing the reign of crime, police corruption & injustice in his city & decides to wage a one man war against the psychotic king maker Tau played by the legendary Amrish Puri. His journey costs him his limbs & loved ones as he goes on a vigilante style brute fest right into the lair & dark world of the two faced Tau & his cronies. Death & Destruction follow the war path.

==Cast==
* Ajay Devgan as  Prashant Varma 
* Raveena Tandon as Priya 
* Satyendra Kapoor as Monto 
* Shafi Inamdar as Police Commissioner Anand Deshmukh 
* Alok Nath as  the Professor 
* Natasha Sinha as  Shalini Verma 
* Anjan Srivastav as Pandey 
* Dinesh Hingoo as  Rustom 
* Abhimanyu as  Francis, Montos son
* Deep Dhillon as  Lalaa 
* Pankaj Berry as Inspector Abhay 
*Amrish Puri as  Tau (Mafia Don) 
*Manohar Singh as Priyas father 
*Shakti Kapoor as Bharat Acharya, Taus nephew

==Soundtrack==
{| class="wikitable "
|-
! Track # !! Title !! Singer(s)
|-
|-
| 1 || Aapko Dekh Kar || Kumar Sanu, Alka Yagnik
|-
| 2 || Batha Mujko Sanam Mere || Kumar Sanu, Alka Yagnik
|-
| 3 || Nahin Nahin Kabhi Nahin || Kumar Sanu, Alka Yagnik
|-
| 4 || Kare Kaise Ada Rab || Alka Yagnik
|-
| 5 || Sang Sang Chalungi Mein (Female) || Sadhana Sargam
|-
| 6 || Sang Sang Chalunga Mein (Male) || Kumar Sanu
|-
| 7 || O Mere Gudde Raja || Kumar Sanu, Asha Bhosle
|-
| 8 || Aao Na Mujhse Pyar Karo || Alka Yagnik
|}

==External links==
*  

 
 
 
 
 


 
 