Absolut Warhola
{{Infobox film
| name           = Absolut Warhola
| image          = tlareleasingabsolutewarhola.jpg
| caption        =
| director       = Stanislaw Mucha
| producer       =
| writer         = Stanislaw Mucha
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =  Pegasos Film   TLA Releasing  (US) 
| released       = 2001
| runtime        = 80 min
| country        = Poland
| language       = Slovak
| budget         =
| preceded by    =
| followed by    =
}}

Absolut Warhola is a 2001 film directed by Stanislaw Mucha about Andy Warhols extended family, whom he never met, from rural Slovakia. 

The film follows the filmmakers as they travel through eastern Slovakia to interview Warhols surviving relatives, ethnic-Ruthenians living near the Polish border in Miková, and to visit the Andy Warhol Museum of Modern Art in Medzilaborce. The museum is shown to be in a poor state, with the museum director and staff openly soliciting donations from the viewer and giving out the museums bank account details.

The living situation in the underdeveloped rural east of Slovakia prior to European Union|EU-membership is also shown. The film also notes the levels of homophobia still present in this part of the world, with several of Warhols relatives openly criticizing the "rumours" of his sexuality (although his homosexuality is common knowledge in the West). One man even goes as far to suggest that Valerie Solanas, the radical feminist and misandrist who attempted to assassinate Warhol, was in fact a spurned former lover.

== References ==

 

== External links ==

*  
*  

 
 
 
 
 
 


 