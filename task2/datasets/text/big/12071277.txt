Shattered Soul
Shattered Turkish crime-thriller film starring Demet Evgar, Tamer Karadağlı and Levent Üzümcü, and directed by Mustafa Altıoklar.

== Plot ==
Seemingly content with the way her life goes, and deeply in love with her psychiatrist husband Doruk, Beyza is thrown off-balance by strange, occasional memory blackouts. Meanwhile, a number of mutilated legs found around Istanbul push the city into the terror of a serial murderer. Police Lieutenant Fatih investigates the gruesome murders with his new expert partner, Doruk. As the police follow the trail of the murderer, Beyza faces the truth about herself: a relationship, which even she cannot explain, exists between herself and the victims.

== Cast ==
*Demet Evgar - (as Beyza Türker)
*Tamer Karadağlı - (as Fatih)
*Levent Üzümcü - (as Doruk Türker)
*Arda Kural - (as Naim)
*Engin Hepileri - (as Hüseyin)
*Mine Cayiroglu - (as Serap)
*Berrak Tüzünataç - (as Figen)
*Elif Dağdeviren - (as Doctor)
*Damla Basak - (as Elif)
*Engin Altan - (as Koray)

==References==
* 

 
 
 
 
 
 
 


 
 