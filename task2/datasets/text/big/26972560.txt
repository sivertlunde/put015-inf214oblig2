Upper World (film)
{{Infobox film
| name           = Upper World
| image          = Poster of the movie Upper World.jpg
| image_size     =
| caption        =
| director       = Roy Del Ruth
| producer       =
| writer         = Ben Hecht (story) Charles MacArthur (story, uncredited) Eugene Walter (story, uncredited) Ben Markson
| narrator       =
| starring       = Warren William Mary Astor Ginger Rogers
| music          =
| cinematography =
| editing        =
| studio         = Warner Bros.
| distributor    =
| released       =  
| runtime        = 70-73 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Upper World is a 1934 drama film starring Warren William as a married railroad tycoon whose friendship with a showgirl, played by Ginger Rogers, leads to blackmail and murder. 

==Cast==
*Warren William as Alexander Stream
*Mary Astor as Mrs. Hettie Stream
*Ginger Rogers as Lilly Linda
*Andy Devine as Oscar Dickie Moore as Tommy Stream
*Ferdinand Gottschalk as Marcus
*J. Carrol Naish as Lou Colima (as J. Carroll Naish)
*Sidney Toler as Officer Moran
*Henry ONeill as Banker Making Toast at Banquet
*Theodore Newton as Reporter Rocklen
*Robert Barrat as Police Commissioner Clark
*Robert Greig as Marc Caldwell
*Frank Sheridan as Police Inspector Kellogg
*John Qualen as Chris
*Willard Robertson as Police Captain Reynolds

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 


 