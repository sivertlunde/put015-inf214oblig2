Sati Sulochana
 
{{Infobox film
| name           = Sati Sulochana
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Yaragudipati Varada Rao
| producer       = Chamanlal Doongaji   Shah Bhurmal Chamanlalji
| writer         = 
| screenplay     = Bellave Narahari Shastri
| story          = 
| based on       =   
| narrator       = 
| starring       = Subbaiah Naidu Tripuramba R. Nagendra Rao D. A. Murthy Rao Yaragudipati Varada Rao|Y. V. Rao C. T. Sheshachalam
| music          = R. Nagendra Rao H. R. Padmanabha Sastry
| cinematography = 
| editing        = 
| studio         = Prabhat Studio
| distributor    = 
| released       = 3 March 1934
| runtime        = 173 minutes
| country        = India
| language       = Kannada
| budget         =  40,000
| gross          = 
}}

Sati Sulochana ( ) is an      It is also the first film to be screened in the erstwhile Mysore Kingdom.   

==Plot== Sulochana from Sulochana commits sati sacrifice.

==Cast==
* Subbaiah Naidu|M. V. Subbaiah Naidu as Indrajit Sulochana
* R. Nagendra Rao as Ravana
* Lakshmi Bai as Mandodari
* Yaragudipati Varada Rao|Y. V. Rao as Lakshmana
* C. V. Seshachalam as Narada
* D. A. Murthy Rao as Rama
* S. K. Padmadevi as Sakhi
* Indubala

==Background== Marwari businessman Sulochana respectively.

==Production==
Sachin Nayaka chose to shoot the film at Chatrapathi Studio in Kolhapur. The production was started in December 1933 and took 2 months to complete. Shooting was entirely done in natural sunlight and the total amount spent for production was  40,000. The film involved shooting a war scene and this was done using 2 cameras.     The film was released on March 3, 1934 at Paramount cinema theatre (later called as Parimala talkies) near the City Market of Bangalore. The length of the film was 173 minutes.  Being the first Kannada talkie film, it ran house-full at Bangalore for six weeks. 

==See also==
* List of Kannada films
* Kannada cinema

==Notes==
 

== External links ==
*  

 
 
 