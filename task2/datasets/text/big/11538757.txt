Café Elektric
{{Infobox film
| name           = Café Elektric
| image          = CaféElektric.jpg
| image_size     =
| caption        = Film poster
| director       = Gustav Ucicky Count Sascha Kolowrat-Krakowski
| writer         = Jacques Bachrach  Felix Fischer (play)
| narrator       =
| starring       = Marlene Dietrich Willi Forst Gerhard Gruber
| cinematography = Hans Androschin
| editing        =
| distributor    = Sascha-Film
| released       = November 25, 1927
| runtime        = 80&nbsp;minutes
| country        = Austria Silent
| budget         =
}}

Café Elektric (1927 in film|1927) is an Austrian film directed by Gustav Ucicky.

== Plot outline ==
The daughter of a wealthy industrialist (Marlene Dietrich) falls for a pickpocket (Forst).

== Cast ==
* Willi Forst ... Fredl
* Marlene Dietrich ... Erni Göttlinger
* Fritz Alberti ... Kommerzialrat Göttlinger
* Anny Coty ... Göttlingers Freundin
* Igo Sym... Max Stöger
* Vera Salvotti ... Paula
* Nina Vanna ... Hansi
* Dolly Davis
* Albert Kersten ... Herr. Zerner

== External links ==
*  

 

 
 

 
 
 
 
 
 