170 Hz
{{Infobox film
| name           = 170 Hz
| image          = 170Hz2011Poster.jpg
| caption        = Film poster
| director       = Joost van Ginkel
| producer       =
| writer         =  Michael Muller
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =   
| runtime        = 
| country        = Netherlands Dutch
| budget         = 
}}
170&nbsp;Hz is a Dutch film about the love of two deaf children. Although some dialogues are in Dutch, most of it is in Dutch Sign Language  and subtitled. The film was first presented on the Dutch Film Festival in September 2011, and was released theatrically on 1 March 2012. The title refers to the frequency of 170 Hertz, the highest frequency which still can be heard by main character Nick (e.g. the sound of a motor bike). 

==Prizes==
The film has won the Audience Award at the Gouden Kalf Awards 2011.  Furthermore, the film was nominated for the "Best Sound Award" and Gaite Jansen for "best actress" at these awards. 

==Cast==
* Gaite Jansen as Evy Michael Muller as Nick

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 