Odahuttidavaru
Not to be confused with Odahuttidavaru (1969 film)

{{Infobox film name           = Odahuttidavaru image          = image_size     = caption        = director       = Dorai-Bhagawan producer       = S. P. Varadaraj writer         = S. K. Bhagavan narrator       = starring  Rajkumar Ambareesh Madhavi Srishanti music          = Upendra Kumar cinematography = B. C. Gowrishankar editing        = P. Bhaktavatsalam studio         = Sri Lakshmi Art Combines released       =   runtime        = 140 minutes country        = India language       = Kannada budget         =
}}
 Kannada romantic Rajkumar and Madhavi and Srishanti.  The film is produced by S. P. Varadaraj for Sri Lakshmi Art Combines. The film was the last work of the acclaimed poet and lyricist Vijaya Narasimha.
 
The story revolved around the two farming brothers who undergo various traumas in the relationships due to the external forces and fight them to reunion again. The film met with immense appreciation from the critics and was a blockbuster at the box-office.

==Cast== Rajkumar 
* Ambareesh Madhavi 
* Srishanti
* Vajramuni
* K. S. Ashwath Balakrishna
* Umashree 
* Shanthamma
* Sathyabhama
* B. Jaya (actress)|B. Jaya

==Soundtrack==

{{Infobox album  
| Name        = Odahuttidavaru
| Type        = Soundtrack
| Artist      = Upendra Kumar
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 29:05
| Label       = Sangeetha
}}

The music of the film was composed by Upendra Kumar, with lyrics penned by M. N. Vyasa Rao, Chi. Udaya Shankar, Sri Ranga, Geethapriya, Hamsalekha and Vijaya Narasimha. The album consists of six soundtracks.  It was received exceptionally well by the critics and the audience.

{{Track listing
| total_length   = 29:05
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Madhura Ee Kshana
| lyrics1  	= M. N. Vyasa Rao
| extra1        = Rajkumar (actor)|Rajkumar, Manjula Gururaj
| length1       = 4:49
| title2        = Nambi Kettavarillavo
| lyrics2 	= Chi. Udaya Shankar
| extra2        = Rajkumar
| length2       = 4:52
| title3        = Bennina Hinde 
| lyrics3       = Sri Ranga
| extra3  	= S. P. Balasubrahmanyam, Sangeetha Katti
| length3       = 4:45
| title4        = Sole Geluvendu Baalali
| extra4        = Rajkumar
| lyrics4 	= Geethapriya
| length4       = 4:23
| title5        = Naanu Naanu Neenu
| extra5        = Rajkumar, S. P. Balasubrahmanyam, Manjula Gururaj, Sangeetha Katti
| lyrics5       = Hamsalekha
| length5       = 4:57
| title6        = Janakana Maatha
| extra6        = S. P. Balasubrahmanyam
| lyrics6       = Vijaya Narasimha
| length6       = 5:19
}}

==Awards== Rajkumar

==References==
 

==External source==
*  
*  

 
 
 
 
 
 