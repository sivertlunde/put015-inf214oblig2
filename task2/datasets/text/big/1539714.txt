Pink Floyd – The Wall
 
 
 

{{Infobox film name           = Pink Floyd – The Wall image          = Pink Floyd The Wall.jpg image_size     = 215px alt            =  caption        = Theatrical release poster director       = Alan Parker  Gerald Scarfe (animated scenes) producer  Alan Marshall screenplay     = Roger Waters based on       =   narrator       = Pink Floyd
| starring = {{plainlist|
* Bob Geldof
}} music          = Pink Floyd  with  Bob Ezrin Michael Kamen cinematography = Peter Biziou editing        = Gerry Hambling studio  Goldcrest Films International Tin Blue distributor    = Metro-Goldwyn-Mayer released       =   runtime        = 95 minutes country        = United Kingdom language       = English budget         = $12 million  gross          = $22,244,207   Box Office Mojo. Retrieved 1 January 2014. 
}}
 
 
 
{{Infobox album
| Name = Pink Floyd - The Wall
| Type = soundtrack
| Artist = Pink Floyd
| Released = Unreleased
| Recorded = 1981-82
| Genre = Progressive rock
| Chronology = Pink Floyd soundtracks
| Last album = Obscured by Clouds (1972)
| This album = Pink Floyd - The Wall (1982)
| Next album = La Carrera Panamericana (1992)
| Misc          = 
{{Singles
| Name     = Pink Floyd - The Wall
| Type      = soundtrack
| Single 1     = When the Tigers Broke Free
| Single 1 date =  
 }}
}}
Pink Floyd – The Wall is a 1982 British Live-action/animated film|live-action/animated musical film directed by Alan Parker based on the 1979 Pink Floyd album The Wall about a confined rocker whos driven into insanity and constructs a wall to be protected from the world around him. The screenplay was written by Pink Floyd vocalist and bassist Roger Waters. The film is highly metaphorical and is rich in symbolic imagery and sound. It features very little dialogue and is mainly driven by the music of Pink Floyd.

The film contains fifteen minutes of elaborate animation sequences by the political cartoonist and illustrator Gerald Scarfe. It was the seventh animated feature to be presented in Dolby Stereo.

==Plot== while defending the Anzio bridgehead during World War II, in Pinks infancy.
 discovers a overprotective mother. 

As an adult, Pink eventually marries, but he and his wife soon grow apart. While he is in the United States on tour, Pink learns that his wife is having an affair. He turns to a willing groupie, whom he brings back to his hotel room only to trash it in a fit of violence, terrifying the groupie out of the apartment.
 The Dam Busters on television, morphs into his neo-fascist alter-ego. Pinks manager, along with the hotel manager and some paramedics, discover Pink and inject him with drugs to enable him to perform. 

Pink fantasises that he is a dictator and his concert is a Neo-Nazi rally. His followers proceed to attack ethnic minorities, and Pink holds a rally in suburban London, singing "Waiting for the Worms". The scene is intercut with images of animated marching hammers that goose-step across ruins. Pink screams "Stop (Pink Floyd song)|Stop!" and takes refuge in the toilets at the concert venue, reciting poems. 
 on trial, and his sentence is "to be exposed before   peers." The judge gives the order to "tear down the wall". Following a prolonged silence, the wall is smashed.

Several children are seen cleaning up a pile of debris after an earlier riot, with a freeze-frame on one of the children emptying a Molotov cocktail.

==Cast==
*Bob Geldof as Floyd Pinkerton (Pink)
**Kevin McKeon as Young Pink
**David Bingham as Little Pink
*Christine Hargreaves as Pinks mother
*Eleanor David as Pinks wife
*Alex McAvoy as Teacher
*Bob Hoskins as Rock-and-roll manager
*Michael Ensign as Hotel manager
*James Laurenson as J.A. Pinkerton
*Jenny Wright as American groupie
*Margery Mason as Teachers wife
*Ellis Dale as English doctor
*James Hazeldine as Lover
*Ray Mort as Playground father
*Robert Bridges as American doctor
*Joanne Whalley, Nell Campbell, Emma Longfellow, and Lorna Barton as Groupies Philip Davis and Gary Olsen as Roadies

==Production==

===Concept===

In mid-1970s, as Pink Floyd gained mainstream fame, Waters began feeling increasingly alienated from their audiences: Audiences at those vast concerts are there for an excitement which, I think, has to do with the love of success. When a band or a person becomes an idol, it can have to do with the success that that person manifests, not the quality of work he produces. You dont become a fanatic because somebodys work is good, you become a fanatic to be touched vicariously by their glamour and fame. Stars—film stars, rock n roll stars—represent, in myth anyway, the life as wed all like to live it. They seem at the very centre of life. And thats why audiences still spend large sums of money at concerts where they are a long, long way from the stage, where they are often very uncomfortable, and where the sound is often very bad.     Wish You Were Here, "Have a Cigar"). The concept of the wall, along with the decision to name the lead character "Pink", partly grew out of that approach, combined with the issue of the growing alienation between the band and their fans.    This symbolised a new era for rock bands, as Pink Floyd "explored (...&nbsp;) the hard realities of being where we are", drawing upon existentialists, namely Jean-Paul Sartre.   

===Development===
Even before the original Pink Floyd album was recorded, a film was intended to be made from it.  However, the concept of the film was intended to be live footage from the The Wall Tour|albums tour, with Scarfes animation and extra scenes.    The film was going to star Waters himself.  EMI did not intend to make the film, as they did not understand the concept. 
 adapted to punk musician Behind the Wall, both Waters and Geldof later admitted to a story during casting where Geldof and his manager took a taxi to an airport, and Geldofs manager pitched the role to the singer, who continued to reject the offer and express his contempt for the project throughout the fare, unaware that the taxi driver was Waters brother, who promptly proceeded to tell Waters about Geldofs opinion.

  Hey You" still had not been properly shot by the end of the live shows.  Parker also managed to convince Waters and Scarfe that the concert footage was too theatrical and that it would jar with the animation and stage live action. After the concert footage was dropped, Seresin left the project and Parker became the only director connected to The Wall. 

===Filming===
During production, while filming the destruction of a hotel room, Geldof suffered a cut to his hand as he pulled away the venetian blinds. The footage remains in the film. Also, it was discovered while filming the pool scenes that Geldof did not know how to swim. Interiors were shot at Pinewood Studios, and it was suggested that they suspend Geldof in Christopher Reeves clear cast used for the Superman (1978 film)|Superman flying sequences, but his frame was too small by comparison; it was then decided to make a smaller rig that was a more acceptable fit, and he simply lay on his back. 

The war scenes were shot on Saunton Sands in North Devon, which was also featured on the cover of Pink Floyds A Momentary Lapse of Reason six years later. 

==Release==
The film was shown "out of competition" during the 1982 Cannes Film Festival. 

{{quote box|width=250px|salign=right|quote=The premiere at Cannes was amazing – the midnight screening. They took down two truckloads of audio equipment from the recording studios so it would sound better than normal. It was one of the last films to be shown in the old Palais which was pretty run down and the sound was so loud it peeled the paint off the walls. It was like snow – it all started to shower down and everyone had dandruff at the end. I remember seeing Terry Semel there, who at the time was head of Warner Brothers, sitting next to Steven Spielberg. They were only five rows ahead of me and Im sure I saw Steven Spielberg mouthing to him at the end when the lights came up, what the fuck was that? And Semel turned to me and then bowed respectfully.

What the fuck was that?, indeed. It was like nothing anyone had ever seen before – a weird fusion of live-action, story-telling and of the surreal.|source=Alan Parker }}
 Richard Wright, Roger Taylor, Lulu and Andy Summers. 

===Critical reception===
  James Guthrie, Eddy Joseph, Clive Winter, Graham Hartstone & Nicholas Le Messurier;    and Best Original Song for Waters. 

The film received generally positive reviews. Reviewing The Wall on their television programme   2010.

While Rotten Tomatoes ranked the film with a critics review of 72% rating (of 17 reviews), the community of the website ranked the film with an 88% (out of 375 reviews). Danny Peary wrote that the "picture is unrelentingly downbeat and at times repulsive&nbsp;... but I dont find it unwatchable – which is more than I could say if Ken Russell had directed this. The cinematography by Peter Bizou is extremely impressive and a few of the individual scenes have undeniable power." 
 In the Studio with Redbeard" episodes of The Wall, A Momentary Lapse of Reason and On an Island) that the conflict between him and Waters started with the making of the film. Gilmour also stated on the documentary Behind The Wall (which was aired on the BBC in the UK and VH1 in the US) that "the movie was the less successful telling of The Wall story as opposed to the album and concert versions."

Although the symbol of the crossed hammers was a creation of the film and not related to any real racist group, it was adopted by white supremacist group the Hammerskins in the late 1980s.   

==Documentary==
A documentary was produced about the making of Pink Floyd – The Wall entitled The Other Side of the Wall that includes interviews with Parker, Scarfe, and clips of Waters, originally aired on MTV in 1982. A second documentary about the film was produced in 1999 entitled Retrospective that includes interviews with Waters, Parker, Scarfe, and other members of the films production team. Both are featured on The Wall DVD as extras.

==Soundtrack==
Song changes from album:
{| class="wikitable"  style="width:75%; margin:1em auto 1em auto;"
|-
! style="width:50%;"| Track !! Changes
|-
|"When the Tigers Broke Free" 1 || New song, edited into two sections strictly for the film, but would later be released as one continuous song. {{Cite book publisher = Reynolds and Hearn, isbn = 1-903111-82-X pages = 107–110p. last = Bench first = Jeff title = Pink Floyds The Wall location = Richmond, Surrey, UK year = 2004 The Final Cut.
|-
|"In the Flesh?" ||Extended/re-mixed/lead vocal re-recorded by Geldof. 
|-
|"The Thin Ice" ||Extended/re-mixed  with additional piano overdub in second verse, baby sounds removed.
|- Another Brick Extra bass parts, which were muted on the album mix, can be heard.
|- New song. 
|-
|"Goodbye Blue Sky" ||Re-mixed. 
|-
|"The Happiest Days of Our Lives" ||Re-mixed. Helicopter sounds dropped, teachers lines re-recorded by Alex McAvoy. 
|- Another Brick in the Wall, Part 2" ||Re-mixed  with extra lead guitar, childrens chorus part edited and shortened, teachers lines re-recorded by McAvoy and interspersed within childrens chorus portion.
|-
|"Mother (Pink Floyd song)|Mother" ||Re-recorded completely with exception of guitar solo and its backing track. The lyric "Is it just a waste of time?" is replaced with "Mother, am I really dying?", which is what appeared on the original LP lyric sheet. 
|- A full-length song which begins with the music of, and a similar lyric to "Empty Spaces". This was intended to be on the original album, and in fact appears on the original LP lyric sheet. At the last minute, it was dropped in favour of the shorter "Empty Spaces" (which was originally intended as a reprise of "What Shall We Do Now"). A live version is on the album Is There Anybody Out There? The Wall Live 1980–81. 
|- Young Lust" Screams added and phone call part removed. The phone call part was moved to the beginning of What Shall We Do Now
|-
|"One of My Turns" ||Re-mixed.
|- Shortened and remixed.
|- Another Brick in the Wall, Part 3" ||Re-recorded completely  with a slightly faster tempo.
|- Goodbye Cruel World" ||Unchanged.
|- Classical guitar re-recorded, this time played by David Gilmour with a leather pick, as opposed to the album version, which was played finger-style by Joe DiBlasi.
|- Musically unchanged, but with different clips from the TV set.
|-
|"Vera (song)|Vera" ||Unchanged.
|-
|"Bring the Boys Back Home" ||Re-recorded completely with brass band and Welsh male vocal choir extended and without Waters lead vocals. 
|-
|"Comfortably Numb" ||Re-mixed with Geldofs scream added. Bass line partially different from album.
|- In the Flesh" ||Re-recorded completely with brass band and Geldof on lead vocals. 
|-
|"Run Like Hell" ||Re-mixed and shortened.
|- Shortened but with extended coda.
|- Geldof unaccompanied The Final Cut.
|-
|"Stop (Pink Floyd song)|Stop" ||Re-recorded completely  with Geldof unaccompanied on lead vocals. (The audio in the background of this scene is from Gary Yudmans introduction from The Wall Live at Earls Court.)
|- The Trial" ||Re-mixed.
|-
|"Outside the Wall" ||Re-recorded completely  with brass band and Welsh male voice choir. Extended with a musical passage similar to "Southampton Dock" from The Final Cut.  
|} The Show Must Go On". "Hey You" was deleted as Waters and Parker felt the footage was too repetitive (eighty percent of the footage appears in montage sequences elsewhere)  but available to view as in worn black and white work print form as a bonus feature on the DVD release under the name "Reel 13". 

A soundtrack album from  .

==Chart positions==
{| class="wikitable"
|- Year
!align="left"|Chart Position
|- 2005
|align="left"|Australian ARIA DVD Chart
|align="left"|#10
|}

==References==
 

==External links==
 
 
*  by Bret Urick
* 
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 