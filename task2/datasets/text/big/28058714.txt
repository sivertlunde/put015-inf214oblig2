Naanu Nanna Kanasu
{{Infobox film
| name           = Naanu Nanna Kanasu
| image          = Naanu Nanna Kanasu poster.jpg
| alt            =  
| caption        = Film poster
| director       = Prakash Rai
| producer       = Prakash Rai Shailaja Nag B. Suresh
| writer         = Radha Mohan
| screenplay     = Prakash Rai
| story          = 
| starring       = Prakash Rai Amoolya
| music          = Hamsalekha
| cinematography = Ananth Urs
| editing        = J. N. Harsha
| studio         = 
| distributor    = 
| released       = 14 May 2010
| runtime        = 136 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}

Naanu Nanna Kanasu is a 2010 Indian Kannada-language film directed by Prakash Rai starring Prakash Rai and Amoolya. It is the Kannada Version of the Tamil hit film Abhiyum Naanum that was produced by Prakash Rai himself.  Ramya was originally cast as the lead in the film, but she was later replaced by Amoolya.  In Bangalore, the film ran in theatres for 17 consecutive weeks. 

==Cast==
* Prakash Rai as Uthappa
* Amoolya as Kanasu Sithara as Kalpana
* Ramesh Arvind as Jayanth
* Achyuth Kumar
* Rajesh as Uthappas friend
* Veena Sundar 
* Sihi Kahi Chandru

==Audio==
All the songs in the film, the lyrics and the background scores were composed by the veteran music director Hamsalekha|Dr.Hamsalekha.
{{Infobox album  
| Name        = Naanu Nanna Kanasu
| Type        = Soundtrack
| Music      = Hamsalekha|Dr.Hamsalekha
| Lyrics     = Hamsalekha|Dr.Hamsalekha 
| Cover       = 
| Released    = 2010
| Recorded    =  Feature film soundtrack
| Length      =  
| Label       = Anand Audio
| Website     = 
}}
#"Putta Putta" (Sonu Nigam)
#"Ondu Maamara" (Kailash Kher)
#"Mundooduva" (Sonu Nigam)
#"Balukthalamma" (Shreya Ghoshal)

==References==
 

==External links==
*  

 
 
 
 
 
 


 