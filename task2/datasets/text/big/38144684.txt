Losing Chase
{{Infobox film
| name        = Losing Chase
| director    = Kevin Bacon
| image       = Losing chase.jpg
| producer    = Milton Justice
| associate producer = Anne Meredith Kyra Sedgwick
| writer      = Anne Meredith
| starring    = Helen Mirren Kyra Sedgwick Beau Bridges
| distributor =
| released    =  
| runtime     = 92 minutes
| country     = United States English
| budget      =
| gross       = $64,161   
}}
 Showtime  where it appeared on August 18    though it was later given a limited theatrical release in December following its positive reception. The film is set on the island Marthas Vineyard. It was nominated for three awards at the 54th Golden Globe Awards.   

==Plot==
Following a nervous breakdown and upon returning home following treatment, Chase Phillips (Helen Mirren) is hired a helper, Elizabeth (Kyra Sedgwick) by her husband, Richard (Beau Bridges). Chase is opposed to being given a helper and treats both her husband and Elizabeth with contempt. Chase snidely asks Elizabeth what she plans to do with the money she earns from being a helper; Elizabeth replies that she is not doing it for the money. Chases two young sons, Little Richard (Michael Yarmush) and Jason (Lucas Denton), are returned to the family home the same day Elizabeth arrives. The youngest son, Jason, is eager to see his mother but Little Richard treats her with hostility.

The following day after questioning her, Elizabeth tells the children that she has one sibling, a sister who is "unwell" and that her mother is dead. When asked what happened to her mother, Elizabeth states, "She had a hard time living." Jason later relays this information to Chase. The next day Richard befriends Elizabeth whilst she is planting tomatoes, which causes tension between him and Chase.

Later Elizabeth takes a day off and flies to visit her sister Catherine who is in an institution. After a brief conversation Catherine suddenly becomes enraged at Elizabeth, accusing her of various things. After Catherine is escorted away by a nurse, Elizabeth returns to the Phillips residence, where she discovers Chase ripping up all the tomatoes she planted out of spite. Elizabeth lashes out at Chase, initially over Chases behaviour however as her tearful rage continues she screams at Chase for commuting suicide while Elizabeth and her sister were so young and for ruining their lives. Taken back, Chase manages to calm Elizabeth, telling her she is not her mother. Elizabeth realises what she has been saying, and sobbingly embraces Chase, who returns the hug.

The following day Chases attitude has improved, and she befriends Elizabeth. Richard announces that he will be returning to working away from home, leaving Elizabeth to continue caring for Chase and the children. Chase accompanies Elizabeth and the boys and leaves the house for the first time since returning from treatment. Chase and Elizabeths friendship grows; Chase tells Elizabeth how the expectations to get married and conform to small town life caused her original breakdown.

As the days go by closeness grows between Elizabeth, Chase and the children. Chase gives her old car to Elizabeth as a gift for everything Elizabeth has done for her. Chase laments that Elizabeth will be leaving when summer is over.

At a day on the beach Chase watches Elizabeth go swimming as she naps. When she awakes she cannot see Elizabeth in the water and immediately panics. Chase frantically tries to get Little Richard to help her search for Elizabeth. Elizabeth, who had finished swimming and has been preparing lunch, hears the argument between Little Richard and Chase and comes to investigate. As Elizabeth walks away with Chase, trying to figure out exactly why Chase is so upset, Chase kisses her on the lips. Elizabeth initially kisses her back, but then states she cannot do it and runs away. Little Richard sees the kiss.

As Chase and Elizabeth deny their feelings for each other tensions grow between both of them and Little Richard. Elizabeth contemplates leaving. Little Richard calls his father in distress. Richard returns and confronts his wife. Chase confesses she is in love with Elizabeth, who has made her feel alive again in a way that he never could. Richard takes Elizabeth home the next morning whilst Chase is still sleeping. Chase races to the dock to be able to say goodbye, and insists that Elizabeth take the car she previously gave to her. Chase and Elizabeth embrace before Elizabeth leaves.

Chase narrates an epilogue. She breaks up with Richard, though they remain on good terms and share custody of the children. She states she still sees Elizabeth in her dreams. She concludes with the line, “It was one hell of a storm. Will there be others? There usually are. You just have to wait and see."

==Cast==
*Helen Mirren as Chase Phillips
*Kyra Sedgwick as Elizabeth Cole
*Beau Bridges as Mr. Richard Phillips
*Michael Yarmush as Little Richard Phillips
*Lucas Denton as Jason Phillips

==Release==
The film earned $64,161 at the box office from a limited release at 10 theatres.  It opened in Manhattan on December 6, 1996. 

The film received a positive reception at the Sundance Film Festival.    Caryn James from The New York Times called it an "exquisitely written and acted film",  though criticised the romantic twist between Chase and Elizabeth as a "trendy way to punch up the story".  Leo Russo from Boxoffice (magazine)|Boxoffice also criticised the romance as "a cheap way out for a film that had promise."  Alison Macor from The Austin Chronicle gave the film 3 stars out of 5, praising the main actors and stating, "Kevin Bacon proves as adept at directing as he is at acting." Macor concludes the film may "give Bacon the attention his work deserves but rarely receives, making him known for something other than the cult party game “Six Degrees of Kevin Bacon.”   

The film was nominated for three Golden Globe Awards, one of which it won. It received a nomination for Best Miniseries or TV Film and Beau Bridges was also nominated for Best Actor in a Miniseries or Television Film for his portrayal of Mr. Richard Phillips.  Helen Mirren won the Golden Globe Award for Best Actress in a Miniseries or Television Film for her portrayal of Chase Phillips. 

{| class="wikitable"
! Year
! Group
! Award
! Nominee
! Result
|- 54th Golden 1996
|rowspan=3|Golden Globe Awards Best Actor – Miniseries or Television Film Beau Bridges
|  
|- Best Actress – Miniseries or Television Film Helen Mirren
|   
|- Best Miniseries or TV Film
|Losing Chase
|  
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 