You Can't Fool Your Wife
{{Infobox film
| name           = You Cant Fool Your Wife
| image          = You Cant Fool Your Wife poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Ray McCarey
| producer       = Cliff Reid 
| screenplay     = Jerome Cady
| based on       =   James Ellison Robert Coote Virginia Vale Emma Dunn Elaine Shepard
| music          = Roy Webb
| cinematography = J. Roy Hunt	
| editing        = Theron Warth 	
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 James Ellison, Robert Coote, Virginia Vale, Emma Dunn and Elaine Shepard. The film was released on May 21, 1940, by RKO Pictures.   

==Plot==
 
Young married couple Andrew Hinklin and Clara Hinklin née Fields, who were college sweethearts, are well matched: both are unexciting and unmotivated, only wanting to carve out a plain, simple uninteresting life for themselves. Their marriage is not helped by Claras opinionated mother living with them in their small one bedroom apartment. However, Clara does wish that their life would be a little more exciting as Andrew said on their honeymoon that their married life would be, least of all by Andrew acknowledging their latest wedding anniversary, their fifth. Claras wish takes an unexpected turn when Andrew, at work, is assigned to show the visiting Mr. Battingcourt Jr. - the younger half of the head of their London office and who is majority shareholder of their accounting firm - a good time while hes in the US. "Batty" as he is affectionately called by his friends is a party animal, and Andrew, who Batty rechristens "Hinky", feels he has to party along all in the name of job security.

Clara feels that she is losing her stable husband Andrew to Hinky the party animal. Feeling he is partly to blame for the Hinklins marital problems, Batty advises Clara that she can make herself more exciting to Hinky by changing her demeanor and appearance, more like Mercedes Vasquez, a beautiful and exciting woman who Clara could resemble if made up correctly. Clara agrees to Battys plan to come to one of their parties masquerading as exotic Latina Dolores Alvaradez, to woo Hinky and thus ultimately show him that she can be exotic like he probably now wants. Complications ensue when others find out about Battys scheme and when Mercedes Vasquez also attends that party leading to a few mistaken identities.

== Cast ==
*Lucille Ball as Clara Fields Hinklin / Mercedes Vasquez James Ellison as Andrew Hinkie Hinklin
*Robert Coote as Batty Battincourt
*Virginia Vale as Sally
*Emma Dunn as Mother Fields
*Elaine Shepard as Peggy 
*William Halligan as J.R. Gillespie, Sr.
*Oscar OShea as Dr. Emery, Colony College Chaplain
*Norman Mayes as Porter at Dock
*Patsy OByrne as Hotel Maid Charlie Hall as Ritz Amsterdam Bellboy
*Dell Henderson as Ritz Amsterdam Manager
*Harrison Greene as Sullivan, the House Detective
*Hobart Cavanaugh as Potts, GBG & P Vice President
*Walter Sande as Mr. Gillespie, Jr.
*Walter Fenner as Walker
*Irving Bacon as Lippincott, GBG & P Clerk
*Elaine Shepard as Peggy

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 