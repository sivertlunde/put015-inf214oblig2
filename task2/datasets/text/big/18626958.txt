May Minamahal (film)
{{Infobox film
| name = May Minamahal
| image =
| caption =
| director = Jose Javier Reyes
| producer = Charo Santos-Concio, Lily Y. Monteverde
| starring = Aga Muhlach  Aiko Melendez
| distributor = Star Cinema
| released =  
| runtime =
| country = Philippines Tagalog
}}
 Filipino film English is "Loving Someone". 

== Plot ==
Only son, Carlitos, suddenly finds himself the man of an all-female household when his father dies of a heart attack. He falls in love with an offbeat girl and is forced to make a choice between following his heart or risk losing the love of the family which has nurtured him all his life.

== Cast ==
* Aga Muhlach as Carlitos
* Aiko Melendez as Monica
* Boots Anson-Roa as Becky	
* Ronaldo Valdez as Cenon	
* Agot Isidro as Trina
* Aljon Jimenez as Leo		
* John Estrada as Jun		
* Liza Lorena as Ines		
* Claudine Barretto	as Pinky
* Marita Zobel as Gloria		
* Bimbo Bautista as Jun		 
* Nikka Valencia as Mandy		
* Ogie Diaz	as Didoy	
* Gina Leviste as Ellen				
* Alma Lerma as Geronilla (in TV series is Yvette)		
* George Lim as Tito Momoy		
* Fina Peralejo as Kristine

== TV series ==
 

In 2007, May Minamahal was remade into a TV series by ABS-CBN. The lead characters were Anne Curtis and Oyo Boy Sotto.

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="20%"| Award-Giving Body
! width="35%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 1993
| rowspan="9" align="left"| Metro Manila Film Festival   Second Best Film
| align="center"| May Minamahal
|  
|- Best Director
| align="center"| Jose Javier Reyes
|  
|- Best Actor
| align="center"| Aga Muhlach
|  
|- Best Supporting Actor
| align="center"| Ronaldo Valdez
|  
|- Best Art Direction
| align="center"| Benjie de Guzman
|  
|- Best Story
| align="center" rowspan=2| Jose Javier Reyes
|  
|- Best Screenplay
|  
|- Best Float
| align="center"| May Minamahal
|  
|-
| align="left"| Gatpuno Antonio J. Villegas Cultural Awards
| align="center"| May Minamahal
|  
|}

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 
 