The World Unseen
{{Infobox film
| name = The World Unseen
| image = The World Unseen Poster.jpg
| caption = Theatrical release poster
| director = Shamim Sarif
| producer = Hanan Kattan
| writer = Shamim Sarif
| starring = Lisa Ray Sheetal Sheth
| music = Richard Blackford
| cinematography = Michael Downie
| editing = David Martin
| distributor = Enlightenment Productions Regent Releasing
| released =  
| runtime = 94 minutes
| country = South Africa United Kingdom
| language = English
}} Indian South African women who fall in love in a racist, sexist, and homophobic society.

Ray and Sheth also star together in another Shamim Sarif movie, I Cant Think Straight, released in November 2007.

The World Unseen was made with the assistance of the National Film and Video Foundation of South Africa, which took a minority equity stake in the film.

==Synopsis==
In 1950s South Africa, a land torn apart by apartheid, Amina epitomizes individuality and freedom. She runs the Location Café, a haven of fun, food, and festivities open to all. Amina defines her own laws and lives on her own terms undeterred by the reproving police and the disparaging Indian community.

Miriam demurely follows conventions and makes no demands on life. Her world is confined to being a doting mother to her three children and a subservient wife to her chauvinistic husband Omar.

Amina has a covert business partner, Jacob, who is barred from owning a business because he is ‘coloured. He fancies Madeleine, a local white postmistress, but the indignities and injustices of the prevalent law thwart their desire to pursue a relationship.

Omar’s sister Rehmat married a white man against rules that forbid mixed marriages. When she needs protection from police, Amina shelters her, and her charm and strength of character captivate Miriam, who secretly rejoices when Amina accepts a farming job in her backyard.  Amina notices Miriam’s inherent kindness and silent dedication, and the mutual attraction between them grows. They bare their hearts to each other and their emotions get entangled. They contrive another reason to meet: driving lessons.

The inescapable social distance between them makes them question their feelings, but in the midst of hatred and oppression their only refuge is love.

In the resplendent South African landscape with retro music strewn in the background, The World Unseen explores Miriam’s relationship with Amina and how it empowers her to make personal choices that change her world.

==Cast==
*Lisa Ray as Miriam, a wife and mother who has recently immigrated to South Africa.
*Sheetal Sheth as Amina, a free spirited café owner.
*Parvin Dabas as Omar, Miriams chauvinistic and frustrated husband and one of the films primary antagonists. David Dennis as Jacob, Aminas business partner, who is Coloured. White love interest who runs the local post office.
*Colin Moss as De Witt, a policeman and one of the films primary antagonists.
*Nandana Sen as Rehmat
*Natalie Becker as Farah, Omars lover
*Rajesh Gopie as Sadru Bernard White as Mr. Harjan
*Avantika Akerkar as Mrs. Harjan
*Amber Rose Revah as Begum
*Leonie Casanova as Doris, a waitress at Aminas café shop

== Reception ==

=== Critics === SAFTAs including Best Director and Best Writing Team. However, the film was almost universally panned by critics at large, receiving a 25% "rotten" score and an average rating of 4.6 on review aggregator Rotten Tomatoes. 

=== Awards and honours ===
{| class="wikitable" style="font-size:95%;" ;
|- style="background:#ccc; text-align:center;"
! colspan="4" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Name
! Outcome
|- South African Film and Television Awards   Best Director Shamim Sarif Won
|- Best Cinematographer Mike Downie Won
|- Best Supporting Actor David Dennis Won
|- Best Supporting Actress Natalie Becker Won
|- Best Ensemble Cast The World Unseen Won
|- Best Writing Shamim Sarif Won
|- Best Editor Ronelle Loots, David Martin Won
|- Best Production Designer Tanya van Tonder Won
|- Best Costume Designer Danielle Knox Won
|- Best Make Up/Hair Stylist  Caera OShaughneey Won
|- Best Sound Designer Barry Donnelly Won
|- Phoenix International Film Festival    World Cinema Best Director Shamim Sarif Won
|- Clip Film Festival, USA  Best Director, Feature Shamim Sarif Won
|- Grand Canarias G&L International Film Festival  Best Actress Sheetal Sheth Won
|- Miami Gay & Lesbian Film Festival  Audience Award, Best Feature
|The World Unseen Won
|- Verzaubert Film Festival  Silver Medal
|The World Unseen Won
|- Paris Feminist & Lesbian Film Festival  Audience Award, Best Feature
|The World Unseen Won
|- Rehoboth Film Festival  Best Debut Feature
|The World Unseen Won
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 