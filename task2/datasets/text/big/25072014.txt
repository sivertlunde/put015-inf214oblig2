Destiny's Son
{{Infobox film
| name           = Destinys Son
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Kenji Misumi 
| producer       = 
| writer         = Kaneto Shindo
| screenplay     = 
| story          = 
| narrator       = 
| starring       = Raizo Ichikawa
| music          = 
| cinematography = 
| editing        = 
| studio         = Daiei Film
| distributor    = 
| released       = 1962
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
| based on = a novel by Renzaburo Shibata
}}

  is a 1962 Japanese chambara film directed by Kenji Misumi starring Raizo Ichikawa and written by Kaneto Shindo, released by Daiei Film.  The film is based on one of the novels in the series of Nemuri Kyoshirō, written by Renzaburo Shibata.

==Plot==
The film opens with Shiho Fujimura as a female assassin, seeking to kill her lords mistress for what she sees as the good of her clan.  She succeeds, but is punished for her act, sentenced to death, but it is her husband (Shigeru Amachi) who executes her.  Attempting to deal with the guilt of his action, he becomes a monk and sends his son to be fostered by another family.  The rest of the film follows the young boy as he grows to become a skilled swordsman (Raizo Ichikawa).

Not knowing much of his true past, he sets out on a three year journey at the age of 20 as a means of self-discovery.  At the end of his journey, he returns home with an incredibly defined and near unbeatable sword style and develops an intimidating presence.  He gains notoriety for his skills, but this spurs jealousy and betrayal and his foster family is wiped out by assassins.  Before his foster father dies, he passes on the information about his birth.  The swordsman then sets off to find his father the monk who killed his mother.

==Cast==
*Raizo Ichikawa as Takakura Shingo
*Masayo Banri
*Junichiro Narita
*Shiho Fujimura
*Shigeru Amachi
*Mayumi Nagisa
*Matasaburo Tanba
*Teru Tomota
*Eijiro Yanagi

==Production==
Kenji Misumi - Director 
Kaneto Shindo - Writer

==Film festivals==
Destinys Son has been part of a number of film festivals celebrating the Chambara genre, and also careers of Raizo Ichikawa and Kenji Misumi in the last decade. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 


 