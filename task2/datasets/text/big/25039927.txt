Four Sheets to the Wind
{{Infobox film
| name           = Four Sheets to the Wind
| image          = Four Sheets to the Wind.jpg
| caption        = 
| director       = Sterlin Harjo
| producer       = Chad Burris Ted Kroeber (executive producer) Cheyenne Fletcher
| writer         = Sterlin Harjo Laura Bailey Jeri Arredondo
| music          = Jeffery Johnson
| cinematography = Fredrick Schroder
| editing        = David Michael Maurer
| storyboard     = 
| studio         = Indion Entertainment Group Kish Productions Dirt Road Productions
| distributor    = First Look Studios (United States|U.S.)
| released       =  
| runtime        = 84 minutes
| country        = United States Creek
}}
Four Sheets to the Wind is a 2007 independent drama film written and directed by Sterlin Harjo.  It was Harjos first feature film, and won several awards at the 2007 Sundance Film Festival and American Indian Film Festival.

==Plot==
The film tells the story of Cufe, a young  , August 27, 2006.  Andrew Horton, Joanna E. Rapf, eds., A Companion to Film Comedy ( . 

==Production==
The script was developed with the support of the Sundance Institute, and was filmed in Holdenville, Oklahoma (Harjos hometown) and in Tulsa.    Harjo has commented that one of his purposes in writing the script was to react against expectations and stereotypes, for example by depicting Cufe "drinking a beer" while not making alcoholism a central issue, and showing him becoming involved with Francie without making the movie into "an issue-driven interracial relationship story". 

==Reception==
The film premiered at the  , March 2011 (updated April 2014). 
 Hollywood Reporter/Associated Garden State and complimented its "wonderfuly oddball comedy" as well as its "heart-felt" message. 

==Cast==
* Cody Lightning – Cufe Smallhill
* Tamara Podemski – Miri Smallhill Laura Bailey – Francie
* Jeri Arredondo – Cora Smallhill
* Jon Proudstar – Jim
* Mike Randleman – Sonny
* Richard Ray Whitman – Frankie Smallhill

==Awards and nominations==
{| class="wikitable"
|- bgcolor="#CCCCCC" align="center"
! Year || Award || Winner/Nominee ||  Category || Result
|-
|rowspan="7" | 2007 || rowspan="5" |  American Indian Film Festival || Sterlin Harjo || Best Film ||  
|-
| Sterlin Harjo || Best Director ||  
|-
| Cody Lightning || Best Actor ||  
|-
| Tamara Podemski || Best Actress ||  
|-
| Jeri Arredondo || Best Supporting Actress ||  
|-
|rowspan="2" | Sundance Film Festival || Sterlin Harjo || Grand Jury Prize - Dramatic ||  
|-
|| Tamara Podemski || Special Jury Prize - Dramatic ||  
|-
|rowspan="1" | 2008 ||  Independent Spirit Awards || Tamara Podemski || Best Supporting Female ||  
|-
|}

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 