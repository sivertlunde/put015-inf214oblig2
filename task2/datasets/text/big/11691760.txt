The Desert of the Tartars
 
{{Infobox film
| name           = The Desert of the Tartars
| image          = The Desert of the Tartars.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Valerio Zurlini
| producer       = Michelle de Broca Bahman Farmanara Enzo Giulioli
| writer         = Jean-Louis Bertucelli
| screenplay     = André G. Brunelin
| story          = 
| based on       = The Tartar Steppe by Dino Buzzati
| narrator       = 
| starring       = Vittorio Gassman Jacques Perrin Helmut Griem
| music          = Ennio Morricone
| cinematography = Luciano Tovoli
| editing        = Raimondo Crociani
| studio         = Cinecittà Studios
| distributor    = Filmverlag der Autoren Quartet Films NoShame Films
| released       =   
| runtime        = 140 minutes
| country        = Italy France Germany
| language       = Italian
| budget         = 
| gross          =
}}
 director Valerio Zurlini with an international cast, including Jacques Perrin, Vittorio Gassman, Max von Sydow, Francisco Rabal, Helmut Griem, Giuliano Gemma, Philippe Noiret, Fernando Rey, and Jean-Louis Trintignant. The cast also included Iranian film veteran actor Mohammad-Ali Keshavarz.

It is based on the Dino Buzzatis novel The Tartar Steppe. The film omits certain parts of the novel, especially those relating to the lives of Drogos friends in his home town.

It was filmed in Arg-e Bam, Iran, and was released on 29 October 1976 in Italy. It was shown as part of the Cannes Classics section of the 2013 Cannes Film Festival. 

The films visual style was influenced by the work of Italian painter Giorgio de Chirico. 

==Plot==
The film tells the story of a young officer, Giovanni Drogo (Jacques Perrin), and the time that he spent guarding the Bastiani Fortress, an old, unmaintained border fortress. It is noted for its scenery, the lighting and cinematography, and the psychological questions it raises.

==Cast==
* Jacques Perrin: Sten. Giovanni Drogo
* Vittorio Gassman: Count Giovanbattista Filimore
* Giuliano Gemma: Magg. Matis
* Helmut Griem: Lt. Simeon
* Philippe Noiret:  General
* Fernando Rey: Lt. Col. Nathanson
* Laurent Terzieff: Lt. Pietro Von Hamerling
* Jean-Louis Trintignant: Magg. Med. Rovine
* Max von Sydow: Cpt. Ortiz
* Giuseppe Pambieri: Lt. Rathenau
* Francisco Rabal: M.llo Tronk
* Lilla Brignone: mother of Drogo

==Awards==

===Won===
* David di Donatello Awards 1977:
**David Award - Best Director: Valerio Zurlini
**Dacid Award - Best Film
**Special David Award - Best Actor: Giuliano Gemma
*Italian National Syndicate of Film Journalists 1977:
**Silver Ribbon Award - Best Director: Valerio Zurlini

===Nominated===
*Italian National Syndicate of Film Journalists 1977:
**Silver Ribbon Award - Best Supporting Actor: Giuliano Gemma

==References==
 

==External links==
* 
*  
*Federica Capoferri (1998).  . Italica (journal)|Italica 75 (2), 226-241.

 
 
 
 
 
 
 
 
 
 
 
 