Malabar Wedding
 
{{Infobox film
| name           = Malabar Wedding
| image          = Malabar Wedding.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Rajesh Faisal
| producer       = K. B. Devarajan   T. Santhosh Kumar
| writer         = Ramesh Madhavan
| narrator       =  Indrajith  Janardhanan
| Rahul Raj
| cinematography = Vel Raj
| editing        = Arun Kumar   
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 Indrajith and Gopika in lead roles.

The story is based on a custom called Sorakalyanam which is prevalent in some parts of Malabar (Northern Kerala)|Malabar. In this custom, before the marriage, friends have fun with the bride and groom, by making false stories and playing practical jokes upon them.

Rahulraj scored the music for this film.

==Plot==
A wedding planner Thaaraavu Manikkuttan (Indrajith Sukumaran|Indrajith) and his friends always have fun at their friends by putting the bride or groom in difficult positions. Often they play pranks on them, in turn humiliating them and laughing at their expense. Everyone knows that they will be at the receiving end, one day. 

When Manikkuttan gets married to Smitha (Gopika), his friends take revenge on them in the same fashion in which they were fooled. They mix alcohol in the soft drink which was given to Manikkuttan. He gets intoxicated and makes a mess when Smithas parents arrive at their home. Later at night, they place a small firecracker next to their bedroom to disturb them during their first night. When the firecracker explodes, some unexpected events happen, and Smitha is found bleeding and unconscious in the bedroom, with her arm cut.

Smitha is hospitalized, and her parents blame it on Manikkuttan. Manikkuttan and his friends feel sorry for her. But Smitha revels that it was not a suicide attempt: Her hand was cut by accident when her brother came and visited them at night and tried to kill Manikkuttan. Manikkuttan learns that she has a brother, who was adopted by her parents at a very young age and was very possessive about his sister. In the fear of losing his sister, he had already interfered with one marriage, thus making the bride withdraw from it. Smitha tells Manikkkuttan that she loves her brother very much, and she misses him badly. Her brother overhears that and becomes happy leaving them to live their own life on their own.

==Cast== Indrajith ...  Thaaraavu Maanukkuttan
*Gopika ...  Smitha
*Suraj Venjaramood ...  Pookkatta Satheesan Janardhanan ...  Paappan
*T. P. Madhavan ...  Thampi
*Mamukkoya ...  Moonga Avukkar Abubacker
*Manianpilla Raju ...  Unni
*Kiran Raj ...  Bhaskaran
* M. R. Gopakumar
*K. T. S. Padannayil ...  Maman (as Padannayil)
*Machan Varghese ...  Purushu
*Bijukuttan ...  Irumpan Sadu
*Atlas Ramachandran ...  Sreeraman
*Santhosh Jogi ...  Choonda Sugunan
*Sajith Raj ...  Devan
* Zeenath
*Manka Mahesh ...  Madhavi
*Ambika Mohan ...  Smithas mother
*Lakshmipriya
*T. N. Gopakumar ...  himself Mahesh

==External links==
*  
* http://www.nowrunning.com/movie/4861/malayalam/malabar-wedding/preview.htm
* http://www.rediff.com/movies/2008/mar/24ssmw.htm
* http://www.indiaglitz.com/channels/malayalam/article/37191.html
* http://popcorn.oneindia.in/title/511/malabar-wedding.html

 
 
 
 
 
 