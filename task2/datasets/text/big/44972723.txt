Sandhya Raga
{{Infobox film 
| name           = Sandhya Raga
| image          =  
| caption        = 
| director       = A C Narasimha Murthy S K Bhagvan
| producer       = A C Narasimha Murthy A Prabhakara Rao
| writer         = A Na Krishna Rao (Based on Novel &#8216;Sandhya Raga&#8217;)
| screenplay     = Bhagvan Rajkumar Udaykumar Narasimharaju K. S. Ashwath
| music          = G. K. Venkatesh Balamurali Krishna
| cinematography = B Dorairaj
| editing        = N C Rajan
| studio         = Shailashree Productions
| distributor    = 
| released       =  
| country        = India Kannada
}}
 1966 Cinema Indian Kannada Kannada film, Narasimharaju and K. S. Ashwath in lead roles. The film had musical score by G. K. Venkatesh and Balamurali Krishna.  

==Cast==
  Rajkumar
*Udaykumar Narasimharaju
*K. S. Ashwath
*H R Shastry
*Raghavendra Rao
*Kuppuraj
*Krishna Shastry
*Venkatappa
*Narasimhan
*Radhakrishnan
*Master Venkatesh
*Ashwath Narayan
*Kashinath
*Shankar
*Bharathi
*Pandari Bai
*Jr Revathi
*Indrani
*Shanthamma
*Shanthala
*Shailashree
 

==Soundtrack==
The music was composed by G. K. Venkatesh.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ee Pariya Sobagaava || Pt. Bhimsen Joshi, M. Balamurali Krishna || Purandaradasa || 03.26 
|- 
| 2 || Kannadathi Thaaye Baa || Pt. Bhimsen Joshi || GV. Iyer || 03.47 
|- 
| 3 || Nambide Ninna Nada Devathe || Pt. Bhimsen Joshi || GV. Iyer || 03.45 
|- 
| 4 || Theliso Illa Mulugiso || Pt. Bhimsen Joshi || Purandaradasa || 02.38 
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 