The Circle (2014 film)
 
{{Infobox film
| name           = The Circle (Der Kreis)
| image          = The Circle (2014 film), poster.jpg
| caption        = 
| director       = Stefan Haupt
| producer       = Ivan Madeo, Urs Frey
| writer         = Stefan Haupt, Christian Felix, Ivan Madeo, Urs Frey
| starring       =  
| music          = Federico Bettini
| cinematography = Tobias Dengler
| editing        = Christoph Menzi
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Switzerland
| language       = German
| budget         = 
}}
 The Circle, scapegoated for the murders of several gay men in the city. 
 drag entertainer who enter a lifelong romantic relationship through their involvement in the group. The film intersperses a scripted dramatic depiction of the story, in which the couple are portrayed by Matthias Hungerbühler and Sven Schelker, with documentary interviews with the real Ostertag and Rapp. The films cast also includes Marianne Sägebrecht, Anatole Taubman, Antoine Monot, Jr., Stefan Witschi and Markus Merz.
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.       Director Stefan Haupt said "its an honour to represent Switzerland".   

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Swiss submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 