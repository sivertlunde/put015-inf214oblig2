Desire (1958 film)
{{Infobox film
| name           = Desire
| image          = 
| caption        = 
| director       = Vojtěch Jasný
| producer       = 
| writer         = Vladimír Valenta
| starring       = Václav Babka
| music          = 
| cinematography = Jaroslav Kucera
| editing        = Jan Chaloupek
| distributor    = 
| released       =  
| runtime        = 
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

Desire ( ) is a 1958 Czechoslovak film directed by Vojtěch Jasný. It was entered into the 1959 Cannes Film Festival.     

==Cast==
* Václav Babka
* Eva Blazková
* Vladimír Brabec
* Jana Brejchová
* Vlastimil Brodský
* Vera Bublíková
* J. Chrencík
* Milada Davidová
* Anton Gýmerský
* Blazena Holisová
* Jan Jakes
* Zdenek Kutil
* Marie Kyselková
* Václav Lohniský - Michal
* Anna Melísková
* Vladimír Mensík
* Frantisek Musálek
* Jirí Pick
* Václav Polácek
* Ilja Racek - Frantisek (segment "Maminka")
* Zdenek Rehor
* Otto Simánek
* Jan Skopecek
* Vera Tichánková
* Jirí Vala
* Frantisek Vnoucek

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 