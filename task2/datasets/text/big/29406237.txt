The Refuge (film)
{{Infobox film
| name           = The Refuge
| image          = Le refuge, film poster.jpg
| caption        = Theatrical release poster
| director       = François Ozon
| producer       =  
| writer         =  
| starring       =  
| music          = Louis-Ronan Choisy
| cinematography = Mathias Raaflaub
| editing        = Muriel Breton
| distributor    =
| released       =  
| runtime        = 88 minutes
| country        = France
| language       = French
| budget         = 
}}
The Refuge or Hideaway is a 2009 French drama film directed by François Ozon and starring Isabelle Carré and French singer Louis-Ronan Choisy, who wrote the music for the film and the title song. The script was written by Ozon with Matthieu Hippeau.

The film won the Special Prize of the Jury at the 2009 San Sebastián International Film Festival.    It opened in Paris January 27, 2010 to generally lukewarm but not unkind reviews. It was released in the US by Strand. It was part of the uni-France/Film Society of Lincoln Center series the Rendez-Vous with French Cinema in March 2010 with screenings at the Walter Reade Theater and IFC Center.
==Plot==

Louis and Mousse, a couple in their early 30s, are doing drugs in bed in a luxurious half empty Parisian apartment. A drug dealer brings them six grams of heroin and Louis injects Mousse and himself with it. The next morning, rising early; Louis gives himself another shot, which is fatal.
 
Louis mother arrives trying to rent the apartment, and she discovers the couple: Louis is dead from an overdose, but Mousse is alive. She is taken to a hospital where she finally awakens. Mousse is informed of the death of her boyfriend and that she is pregnant. After Louis funeral and burial, his mother, bluntly, tells the confused Mousse that they do not want an heir for her death son and that they have made arrangements to terminate the pregnancy. Louis’ brother, Paul, looks on, empathizing with Mousse.

Some months later, Mousse has found refuge in a seaside country house where she lives as a recluse during the pregnancy she has decided to keep. The house has been lent to her by an older man who was Mousse’s lover when she was sixteen years old. In her hideaway, she takes her time to meditate on her future while living with limited funds and dependent on the methadone she must take in order to stay off heroine. Emotionally guarded, her only contact with the outside world is Serge, a local young man, who delivers her food. 

Mousse’s quiet existence is disrupted when Paul, Louis’ brother, on his way to Spain, stops at the house to see how she is doing. Paul’s visit is not only unexpected, but unwelcome. However, she allows him to stay.  As they begin to share the house and to talk, Mousse warms up to him and they become friends. Paul sees in Mousse a kindred spirit. He tries to get her to go out, something she has not done, preferring to stay home, away from people. Paul finally convinces her to go to the beach with him. There, she is distraught by a well-intentioned, but overbearing woman who gives her unwelcome stories and advice about being pregnant. Talking to Paul, Mousse realizes that he is very different from his brother. This is not surprising as he tell her that he is not Louis biological brother, but was adopted. Paul is gay and not a threat to the very pregnant Mousse. However, she gets jealous when she finds out that Paul has spent the night in the house with Serge. They had met at the nearby village beginning an affair. Upset, Mousse is rude to Serge.

One day, at an outdoor cafe, Mousse’ meets an attractive man who hits on her. He is straightforward and invites her to his room overlooking the water to make love to her. At first she is game, but at the last minute she rejects his advances and asks him to caresses her instead of having sex. After a night of dancing and drinking at a local disco with Paul and Serge, Mousse and Paul share confidences. Paul talks about his adoption while Mousse begins to achieve a degree of emotional closure about her realtionship with Louis.

When one night Paul returns home drunk, Mousse helps him to go to bed. As they have tender feeling for each other, they make love. Although both are happy about what has transpired, it is time for Pauls departure. He promises to visit her in Paris when she has the baby.

Mousse give birth to baby girl, Louise, and Paul comes to see them at the hospital. Their reunion is a joyfull event. Mousse tells Paul to look after the baby while she takes a brief cigarette break. In fact she leaves for good. Not ready to be a mother, she feels Paul will be a much better parent to her baby.

==Cast==
*Isabelle Carré as Mousse
*Louis-Ronan Choisy as Paul
*Pierre Louis-Calixte as Serge
*Melvil Poupaud as Louis
*Claire Vernet as the mother
*Nicolas Moreau as the rich seducer
*Marie Rivière as the woman at the beach
*Jérôme Kircher as the doctor
* Jean-Pierre Andréani as the father
*Emile Berling as the drug dealer

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 