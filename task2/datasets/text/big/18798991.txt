The Angel (film)
The 2007 short sci-fi martial arts horror film directed by Paul Hough starring Eddie McGee, Celine Tien and Thomy Kessler. It was inspired by Eddie McGee, a disabled actor who was trained in wire-work. The short film received multiple film festival awards.

==Awards==

*Best Film at London Sci-Fi Film Festival Sci-fi-london,
*Best Film at Newport Beach Film Festival  ,
*Best Film at Show Off Your Shorts Film Festival  
*Audience Award at Fantasia Film Festival,
*Best Film at Long Island Film Festival  ,
*Best Film at California Film Festival,
*10 Degrees Hotter Award at The Valley Film Festival  

==Other screenings==

*The USA Film Festival, Kansas City Jubilee, Waterfront Film Festival, NYC Downtown Shorts, StoneyBrook, Del Rey Beach, Fangorias *Weekend Of Horrors,
*The Florida Film Festival,
*The Atlanta Film Festival,
*The Silver Lake Film Festival,
*The Denver International Film Festival,
*The Gen Art Film Festival,
*The Nashville Film Festival,
*Palm Springs International Film Festival

THE ANGEL screened on CANAL+ in France on August 10, 2008  

==Reviews==
*Filmthreat.com  

==External links==
*  

 
 
 
 


 