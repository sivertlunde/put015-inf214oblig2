The Prisoner of Zenda (1913 film)
{{Infobox film
| name           = The Prisoner of Zenda
| image          = 
| image_size     = 
| caption        =  Hugh Ford
| producer       = Albert W. Hale Adolph Zukor
| writer         = Hugh Ford
| story          = 
| based on       =  
| narrator       =  James K. Hackett Beatrice Beckley David Torrence
| music          = 
| cinematography = 
| editing        = 
| studio         = Famous Players Film Company
| distributor    = 
| released       =  
| runtime        = Four (five?) reels
| country        = United States
| language       = Silent
| budget         = 
| gross          = 
}} novel of Hugh Ford, James K. Hackett, Beatrice Beckley and David Torrence.   

In 1913 Adolph Zukor lured Hackett from the stage to star in a role which Hackett had played in the theater numerous times. Since feature films were in their infancy, Hackett was at first reluctant to take the part, so Zukor tried to convince Hackett in person, and as Neal Gabler writes, "When Hackett came to visit Zukor, he was the very picture of the faded matinee idol. He wore a fur-collared coat with frayed sleeves and carried a gold-headed cane". 

According to silentera.com, the film was thought to have been Lost film|lost,  but the Library of Congress possesses two paper positive prints and the International Museum of Photography and Film at George Eastman House also has a partial positive print. 

==Cast==
* James K. Hackett as Rudolf Rassendyll / King Rudolf of Ruritania
* Beatrice Beckley as Princess Flavia
* David Torrence as Michael, Duke of Strelsau
* Fraser Coalter as Colonel Sapt (as Frazer Coulter)
* William R. Randall as Fritz von Tarlenheim (as C. R. Randall)
* Walter Hale as Rupert of Hentzau
* Frank Shannon as Detchard
* Minna Gale as Antoinette de Mauban (as Mina Gale Haines)
* Charles Green as Johann
* Tom Callahan as Josef
* Sidney Barrington as Marshal Strakencz

==See also==
* List of rediscovered films

==References==
 

==External links==
*  at Silent Era
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 