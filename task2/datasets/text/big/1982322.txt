H. G. Wells' War of the Worlds (2005 film)
 
 
 
{{Infobox film
| name           = H. G. Wells War of the Worlds
| image          = HG Wells War of the Worlds 2005.jpg
| caption        = Poster
| director       = David Michael Latt
| producer       = David Rimawi
| writer         = David Michael Latt, Carlos De Los Rios 
| line producer  = Leigh Scott
| starring       = C. Thomas Howell  Rhett Giles  Tinarie Van Wyk-Loots Andy Lauer  Peter Greene  Jake Busey
| based on       = The War of the Worlds by H. G. Wells
| music          = Ralph Rieckermann
| cinematography =
| editing        = David Michael Latt
| distributor    = The Asylum
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
}} science fiction horror film film based on the same source.
 invasion of extraterrestrials in 1953 film in which the main character actively tries to repel the aliens.

The DVD was released on June 28, one day before Dreamworks film, and stars  , was released in 2008.

==Plot== alien "Tripod (War of the Worlds)|walker" emerges from the meteorite and massacres the witnesses with a Heat-Ray, George barely escaping with his life. George decides to meet with his younger brother Matt in Hopewell before moving on to D.C., leaving as the aliens destroy his hometown.
 chemical weapons to disperse their opposition.

The two seek refuge in the abandoned house of a veterinarian for food and medical supplies when the neighborhood is flattened by another of the aliens ships. Hiding in the houses ruins deep in the aliens camp, Victor concludes that God has abandoned them before rejecting his religion altogether. Rations become sparse and the aliens begin harvesting humans. George finds rabies vaccines with the hope that it can spread among the aliens and give them a chance to escape. Victor regains his faith after George successfully infects one of the aliens, but only before it kills Victor and leaves. George realizes after a few days that the aliens have left the area and he continues his journey on foot to Washington D.C.. George reunites with Kerry and Samuelson, who has made himself a general and is building a resistance under his own dictatorship. Samuelson senselessly murders Kerry, and George in turn murders Samuelson.

George finally reaches Washington, which is completely destroyed. He offers his life to a lone alien, but it drops dead in front of him. A handful of human survivors emerge and reveal to George that the aliens have been dying from either the Earths air or by a virus. George finds Felicity and Alex alive.

==Cast==
*C. Thomas Howell as Dr. George Herbert
*Andy Lauer as Sgt. Kerry Williams
*Rhett Giles as Pastor Victor
*Tinarie Van Wyk-Loots as Felicity Herbert
*Jake Busey as Lt./General Samuelson
*Peter Greene as Matt Herbert
*Dashiell Howell as Alex Herbert
*Edward DeRuiter as Max

==Adapting the novel== Byron Haskin/George 1988 television tripods have been changed to six-legged crab-like machines called "walkers" (a result of allowing the effects team creative freedom).
 Martians (though version of television series version.
 1953 film 2005 film), leaving their ability to conquer unexplained. The aliens do have a substance similar to the black smoke, but is more of a dense green toxic gas unable to rise above ground level, allowing survivors to escape by getting to high places.

The protagonist is George Herbert, a reference to H. G. Wells.  Rather than being a writer, as in the novel, he is an Astronomy|astronomer. The film leaves the eve of the war storyline and its characters almost completely absent. He also has a son, who is portrayed by Dashiell Howell, who is actually the son of Georges actor C. Thomas Howell.

Despite these differences, George goes through much of what befalls the novels protagonist, even in sacrificing himself to the Martians, only for them to drop dead of infection. He is also separated from his family and tries to reunite with once the invasion begins, Like the novel, they are alive in the conclusion. Georges brother, a United States Army Rangers|Ranger, is less fortunate; he is seen only briefly after being fatally wounded in an attack by the invaders. In the book, the narrators brother takes up a major narrative role.

A major deviation from the text is that the protagonist actually tries to produce a means of stopping the Martians, but whether or not he is directly responsible for their downfall is ambiguous. There is a theme of disease throughout - Georges son is seen suffering a mild cold, while George himself suffers a major fever which leaves him incapacitated for two days.

The novels Artilleryman is divided into two characters. The first, Kerry Williams, exhibits the defeated status. He accompanies George as they move to unaffected areas, meeting soldiers oblivious to the danger they will soon face, until they become separated when George takes refuge underwater to elude the Martians. After his ordeal in the ruined house, George encounters same defeated Williams again. Instead, the other personality, portrayed in the novels later stages, is Lt. Samuelson.

The novels unnamed Curate is films Pastor Victor. While the two are very similar, the pastor is optimistic and is sure that the invasion is the Rapture. However, his faith is deeply shaken when he meets a congregate who screams against God for the loss of her family, causing the Pastor to question why he himself has yet to be taken. Unlike the Curate, the Pastor keeps his composure when hes trapped in the ruined house as he wrestles with his thoughts. Where the Curate had to be subdued in the novel, the Pastor regains his faith just before he is killed by the Martians.
 1953 film. The aliens hands deliberately resemble those of the Martians of the 1953 film, and the protagonists of each film both actively try to weaken the aliens, another deviation from the novel.

==Reception==
The film has received mixed audience reception with a 45% on Rotten Tomatoes and also only four known critic reviews which were mixed.

==Sequel==
 
 Christopher Reid.

==See also== War of the Worlds, Steven Spielbergs 2005 blockbuster which is also a contemporary adaptation of H. G. Wells novel.
*H.G. Wells The War of the Worlds (2005 film)|H.G. Wells The War of the Worlds, a more faithful, lower-budget film adaptation of the novel. The Pianist, a 2002 film that inspired this films themes.
*The Day the Earth Stopped, another Asylum film starring C. Thomas Howell that features an invasion by extraterrestrials.

==References==
 

==External links==
*  Official page.
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 