Tukuma (film)
 
{{Infobox film
| name           = Tukuma
| image          = 
| caption        = 
| director       = Palle Kjærulff-Schmidt
| producer       = 
| writer         = Palle Kjærulff-Schmidt Josef Tuusi Motzfeldt Klaus Rifbjerg
| starring       = Thomas Eje
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}
 Best Foreign Language Film at the 57th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Thomas Eje as Erik
* Naja Rosing Olsen as Sørine
* Rasmus Lyberth as Rasmus
* Benedikte Schmidt as Elizabeth
* Rasmus Thygesen as Otto

==See also==
* List of submissions to the 57th Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 