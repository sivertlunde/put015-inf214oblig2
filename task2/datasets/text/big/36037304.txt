The Earl of Essex (film)
{{Infobox film
| name           = The Earl of Essex 
| image          = 
| image_size     = 
| caption        = 
| director       = Peter Paul Felner
| producer       = 
| writer         = Peter Paul Felner   Louis Rokos
| narrator       = 
| starring       = Eugen Klöpfer   Fritz Kortner   Werner Krauss   Eva May
| music          = 
| editing        = 
| cinematography = Karl Hasselmann   Fritz Stein   Fritz Arno Wagner
| studio         = National-Filmverleih
| distributor    = 
| released       = 14 September 1922 
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German silent silent historical Der Graf Elizabethan England and based in turn on the 1678 work Le Comte dEssex by Thomas Corneille.

==Cast==
* Eugen Klöpfer - Graf Essex 
* Fritz Kortner - Lord Nottingham 
* Werner Krauss   
* Friedrich Kühne - Cecil 
* Eva May - Lady Rutland 
* Erna Morena - Lady Nottingham 
* Charles Puffy - Cuff 
* Magnus Stifter - Lord Southampton 
* Agnes Straub   
* Rosa Valetti   
* Ferdinand von Alten - Raleigh

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 


 
 