She (1965 film)
 
 
{{Infobox film
| name           = She
| image          = She_movie.jpg
| image_size     = 275px
| caption        = film poster by Tom Chantrell Robert Day
| producer       = Michael Carreras
| screenplay     = David T. Chantler
| based on       =   John Richardson Rosenda Monteros Christopher Lee James Bernard
| cinematography = Harry Waxman
| editing        = James Needs Eric Boyd-Perkins
| studio         = Hammer Film Productions
| distributor    = Associated British Picture Corporation|Warner-Pathé Distributors (UK) Metro-Goldwyn-Mayer (US)
| released       = 18 April 1965 (UK) 9 June 1965 (US)
| runtime        = 106 minutes
| country        = United Kingdom
| language       = English
| budget         = £323,778 Bruce G. Hallenbeck, British Cult Cinema: Hammer Fantasy and Sci-Fi, Hemlock Books 2011 pp. 146–157 
| gross          = $1,700,000 (US/ Canada rentals)  284,961 admissions (France) 1,346,650 admissions (Spain) 
}}
 Robert Day John Richardson, Rosenda Monteros and Christopher Lee. The film was an international success and led to a 1968 sequel, The Vengeance of She, with Olinka Berova in the title role.

==Plot== John Richardson) and their orderly Job (Bernard Cribbins) embark on an expedition into a previously unexplored region of north-east Africa.  They discover the lost city of Kuma after Leo receives a mysterious map revealing the citys whereabouts.

This lost realm is ruled by Ayesha (Ursula Andress), who is also known as "She-Who-Must-Be-Obeyed."  Ayesha is an immortal queen and high priestess who believes Leo is the reincarnation of her former lover, the priest Kallikrates (whom she killed when she found him in an intimate embrace with another woman about two thousand years before).  Ayesha tries to convince Leo to walk into a bonfire after it has turned blue, which happens once certain astronomical conditions have occurred.  It will only remain in this condition for a short period and only happens on certain rare occasions.  By entering the fire, Leo himself will become immortal.

As this is occurring, Ayeshas army is attacked by her enslaved tribesmen, the Amahagger.  Although Ayesha had oppressed the Amahagger for 2,000 years, the uprising was triggered by the queen, in a fit of jealousy, executing Ustane (Rosenda Monteros), an Amahagger woman who had developed a relationship with Leo.

Ustanes father Haumeid (André Morell) who, like his daughter, had befriended Leo, Holly and Job, is outraged at his daughters execution and incites the Amahagger into the uprising.  Ayeshas army is overwhelmed during the fierce battle against the poorly equipped, yet numerous Amahagger.

Whilst the uprising is occurring, Leo battles Bilali (Christopher Lee), Ayeshas fanatical priest, who wants immortality for himself, believing it is his due after his years of selfless service.  After battling Leo and finally gaining the upper hand, Bilali, giving Leo up for dead, attempts to enter the blue flames himself and become immortal when he is killed by Ayesha who spears him in the back.

Ayesha takes Leos hand and leads him into the fire.  Upon entering the fire, Leo becomes immortal, however Ayeshas second exposure to the fire destroys her immortality and she dies almost instantly as the centuries catch up with her and she ages millennia in a few seconds.  The film ends with a despondent Leo stating that he doesnt care when the fire will next burn blue but it will find him waiting for it.

==Cast==
* Ursula Andress as Ayesha
* Peter Cushing as Holly
* Bernard Cribbins as Job John Richardson as Leo Vincey
* Rosenda Monteros as Ustane
* Christopher Lee as Billali
* André Morell as Haumeid Princess Soraya as Soraya

==Production== James Bond film Dr. No (film)|Dr. No  &ndash; who signed a two picture deal with Seven Arts as a guarantee for her husband John Derek. She would thus become the first Hammer film to be built around a female star. 

Hammer pitched the project to Universal, who turned it down. Hinds then arranged for Berkley Mather to write a script, but the project was turned down again by Universal, and then by   

Principal photography commenced in southern Israels Negev Desert on 24 August 1964, with scenes also shot at MGMs Elstree Studios in London when Hammers Bray Studios proved to be too small for the project.  It was the most expensive film Hammer had made up until that time,  but upon release it was a hit both in North America and in Europe. 

Although the studio was pleased with the look of Ursula Andress in the film &ndash; as lit by Harry Waxman and costumed by Carl Toms and Roy Ashton &ndash; they found her Swiss German accent to be offputting, and had her entire part re-dubbed by actress Monica Van Der Syl, who maintained a slight accent so as not to throw the films audience, who would be familiar with the way Andress spoke from seeing her in Dr. No. 

==See also==
* She (novel)
* She (1935 film)
* She (1982 film)
* The Vengeance of She

==References==
Notes
 

==External links==
*  
*  
*  
*  

 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 