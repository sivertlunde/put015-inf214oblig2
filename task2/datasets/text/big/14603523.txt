Nijinsky (film)
{{Infobox film
| name           = Nijinsky
| image          = NijinskyPoster.JPG
| image_size     =
| caption        = Theater poster
| director       = Herbert Ross
| producer       = Nora Kaye Stanley OToole Harry Saltzman
| writer         = Hugh Wheeler Romola Nijinsky Vaslav Nijinsky
| narrator       =
| starring       = Alan Bates George De La Peña Leslie Browne Alan Badel Jeremy Irons
| music          =
| cinematography = Douglas Slocombe William Reynolds
| studio         = Hera Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 129 min.
| country        = United States English
| budget         =
| gross          = $1,047,454 
}}
 American biographical film directed by Herbert Ross. Hugh Wheeler wrote a screenplay that explores the later life and career of Vaslav Nijinsky; it was based largely on the premier danseurs personal diaries (a bowdlerized 1936 version was edited and published by his wife, Romola de Pulszky), and her 1934 biography of Nijinsky, largely ghostwritten by Lincoln Kirstein, who later co-founded the New York City Ballet.

==Synopsis== madness by both his consuming ambition and self-enforced heterosexuality. He became involved with Romola de Pulszky, a society girl who joined impresario Sergei Diaghilevs Ballets Russes specifically to seduce Nijinsky. After a series of misunderstandings with Diaghilev, who is both his domineering mentor and possessive lover, Nijinsky succumbs to Romolas charms and marries her. After this, his gradual decline from artistic moodiness to a diagnosis of schizophrenia begins.

==Principal cast==
*Alan Bates ..... Sergei Diaghilev 
*George de la Peña ..... Vaslav Nijinsky 
*Leslie Browne ..... Romola de Pulsky 
*Carla Fracci ..... Tamara Karsavina 
*Ronald Pickup ..... Igor Stravinsky 
*Vernon Dobtcheff ..... Sergei Grigoriev 
*Frederick Jaeger ..... Gabriel Astruc 
*Janet Suzman ..... Emilia Marcus 
*Siân Phillips ..... Lady Ripon Baron de Gunzburg 
*Colin Blakely ..... Vassili 
*Ronald Lacey ..... Léon Bakst 
*Jeremy Irons ..... Mikhail Fokine  Maestro Cecchetti 
*Hetty Baynes ..... Magda

==Principal production credits== 
    
 Producers ..... Harry Saltzman, Nora Kaye Musical Conductor ..... John Lanchbery
*Cinematography ..... Douglas Slocombe Production Design ..... John Blezard

  
 Art Direction ..... George Richardson  Alan Barrett Ballet mistress ..... Irina Baronova

 

==Soundtrack== 
    
 Invitation to the Dance" from Le Spectre de la Rose by Carl Maria von Weber
*Scheherazade (Rimsky-Korsakov)|Scheherazade by Nikolai Rimsky-Korsakov Prelude à lAprès-midi dun faune by Claude Debussy
*Jeux by Claude Debussy

  

*Carnaval (Schumann)|Carnaval by Robert Schumann
*The Rite of Spring by Igor Stravinsky
*Prince Igor by Alexander Borodin
* ´´Petrouchka by Igor Stravinsky 

==Production notes==
* Harry Saltzman purchased the rights in 1969 from film director Charles Vidors widow.    Saltzman had originally promised to let Ken Russell direct the film, but due to a falling out, Saltzman hired Tony Richardson to direct. The film was cancelled during pre-production. After the success of Herbert Rosss ballet film The Turning Point (1977 film)|The Turning Point, Saltzman approached Ross to direct; Ross was initially unenthusiastic. 
* This was Herbert Ross second film to focus on the world of ballet. In his 1977 film he had worked with Mikhail Baryshnikov and other members of the American Ballet Theatre. Baryshnikov turned down the role of Vaslav Nijinsky. At the American Ballet Theatre, he was promoted to the role of Artistic Director.
* Nijinsky was the film debut of Jeremy Irons. It was the second to last film produced by famed Harry Saltzman (after he gave up his share of the James Bond rights). 
* The Los Angeles Philharmonic Orchestra and the London Festival Ballet were featured in the dance sequences. David Hersey of the National Film Theatre in London designed the theatrical lighting in these scenes.

==Critical reception==
Reception to Nijinsky is mixed. It holds a 40% rating and an average score of 5.6/10 at Rotten Tomatoes. 

In his review in Time (magazine)|Time, Richard Schickel opined, "Some people will be titillated by the openness with which homosexual love is portrayed in the film. But this is mostly a slow, cautious biography, elegantly attentive to Edwardian decor and dress. It slights Nijinskys melodramatic story and, finally, offends with its relentless reductionism. There are times when excesses of good taste become a kind of bad taste, a falsification of a subjects spirit and milieu. This is never more true than when the troubles of a genius are presented in boring and conventional terms." 
 Death in Venice … the first major studio film to centre on a male homosexual relationship (albeit a doomed one) without being moralistic … director Ross and writer Hugh Wheeler … do right by their male characters (Alan Bates, in particular, is a plausibly adult Diaghilev), their grasp of the historical reconstructions seems more than competent, and their dialogue and exposition are unusually adroit. Best of all, they never show ballet for its own sake, and have the courage to keep emotional dynamics in the forefront throughout." 

Channel 4 says, "What could have been a powerful period drama quickly descends into soap opera territory … but its always watchable, and director Ross … laces the action with some well-choregraphed dance." 

Director Tony Richardson, who had intended to direct the planned 1970 film on Nijinsky, considered this 1980 film a "travesty". 

==References==
 

==External links==
* 

 
 

 

 
 
 
 
 
 
 
 
 
 
 