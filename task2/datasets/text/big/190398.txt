Interrupted Melody
{{Infobox film
| name           = Interrupted Melody
| image          = Interrupted Melody.jpg
| image_size     =
| caption        = Original French poster
| director       = Curtis Bernhardt  Jack Cummings 
| writer         = Marjorie Lawrence (autobiography)
| based on       = Interrupted Melody 1949 autobiography 
| screenplay     = Sonya Levien William Ludwig
| narrator       = 
| starring       = Glenn Ford Eleanor Parker Roger Moore Cecil Kellaway
| music          = Alexander Courage, Adolph Deutsch
| cinematography = Joseph Ruttenberg, Paul Vogel John D. Dunning
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $2,367,000  . 
| gross          = $4,028,000   
}}
 biographical musical Jack Cummings from a screenplay by Marjorie Lawrence, Sonya Levien, and William Ludwig.

The operatic sequences were staged by Vladimir Rosing. The film stars Glenn Ford, Eleanor Parker, Roger Moore and Cecil Kellaway. The singing voice of Lawrence was provided by Eileen Farrell; Farrell also appears on screen as a student struggling to hit a high note in a scene with the singing teacher Mme. Gilly (Ann Codee).

==Plot summary==
 
The story traces Marjories (Eleanor Parker) long, hard road to the top, her success on two continents, and her turbulent marriage to American doctor Thomas King (Glenn Ford). While touring South America in 1941, Lawrence is stricken with polio, which not only abruptly ends her career but briefly robs her of the will to live. 

==Cast==
* Glenn Ford as Dr. Thomas King
* Eleanor Parker as Marjorie Lawrence
* Roger Moore as Cyril Lawrence
* Cecil Kellaway as Bill Lawrence
* Peter Leeds as Dr. Ed Ryson
* Evelyn Ellis as Clara
* Walter Baldwin as Jim Owens
* Ann Codee as Mme. Gilly
* Leopold Sachse as Himself
* Stephen Bekassy as Comte Claude des Vignaux

==Reception==
According to MGM records the film made $1,801,000 in the US and Canada and $2,227,000 overseas resulting in a profit of $101,000. 
 Best Actress Best Costume Design, Color for Helen Rose.

==Musical tracks== original motion picture soundtrack album.
* "O don fatale" from Giuseppe Verdi|Verdis Don Carlos
* Act 1 finale from Verdis Il trovatore
* "Un bel dì" from Giacomo Puccini|Puccinis Madama Butterfly
* "Habanera (aria)|Habanera" from Georges Bizet|Bizets Carmen
* "Seguidilla#Seguidilla aria|Seguidilla" from Carmen Samson and Delilah
* Brünnhildes Immolation Scene from Richard Wagner|Wagners Götterdämmerung
* Excerpts from Wagners Tristan und Isolde
* "Annie Laurie" by Alicia Scott
* "Over the Rainbow" by Harold Arlen
* "Voi que sapete" from Wolfgang Amadeus Mozart|Mozarts The Marriage of Figaro
* Medley: "Anchors Aweigh" by Charles A. Zimmermann; "Marines Hymn", based on works by Jacques Offenbach; "Dont Sit Under the Apple Tree" by Sam H. Stept
* "Quando men vo" (Musettas Waltz) from Giacomo Puccini|Puccinis La bohème
* "Waltzing Matilda", traditional

==References==
 

==External links==
*  
*  
*  
*  
*   with Robert Osborne and Renée Fleming, Turner Classic Movies
*  

 

 
 
 
 
 
 
 
 
 
 
 