Telephone Operator (film)
{{Infobox film
| name           = Telephone Operator
| image          =File:Telephone Operator poster.JPG
| image_size     =
| caption        =Film poster
| director       = Scott Pembroke
| producer       = Lon Young (associate producer)
| writer         = John W. Krafft (story) Scott Darling (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Gilbert Warrenton
| editing        = Russell F. Schoengarth
| distributor    =
| released       = 7 December 1937
| runtime        = 70 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Telephone Operator is a 1937 American film directed by Scott Pembroke.

It is not related to the similarly-titled "Operator!" (1938 and 1969) AT&T archive films.

== Plot summary ==
A telephone operator covering for an absent friend is flooded with calls seeking emergency assistance as the Riverdale Dam bursts and the community falls victim to a major deluge.

== Cast ==
*Judith Allen as Helen Molly
*Grant Withers as Red
*Warren Hymer as Shorty
*Alice White as Dottie Stengal
*Ronnie Cosby as Ted Molloy
*Pat Flaherty as Tom Sommers
*Greta Granstedt as Sylvia Sommers
*William Haade as Heaver
*Cornelius Keefe as Pat Campbell
*Dorothy Vaughan as Mrs. Molloy

== Soundtrack ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 