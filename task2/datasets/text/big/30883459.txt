Little Treasure
 

{{Infobox film
| name           = Little Treasure
| image          = LittleTreasuremovie.jpg
| caption        = Promotional poster
| director       = Alan Sharp
| producer       = Herb Jaffe Joanna Lancaster Richard Wagner Theodore R. Parvin
| writer         = Alan Sharp
| starring       = Margot Kidder Ted Danson Burt Lancaster
| music          = Leo Kottke
| cinematography = Álex Phillips Jr 
| editing        = Garth Craven
| distributor    = TriStar Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
}}
Little Treasure is a 1985 American action drama starring Ted Danson, Margot Kidder and Burt Lancaster.  The film, written and directed by Alan Sharp, deals with the strained relationship between a bank robber father and his daughter, a stripper.

==Plot==
Margot Kidder plays Margo, a stripper searching for her estranged, bank robber father (Burt Lancaster) in a remote part of Mexico.  Along the way, Margo meets an American ex-pat drifter (Ted Danson), and together they search for her missing father and eventually his lost, buried fortune.

==Cast==
* Margot Kidder as Margo
* Ted Danson as Eugene Wilson
* Burt Lancaster as Delbert Teschemacher
* Joseph Hacker as Norman Kane
* Malena Doria as Evangelina John Pearce as Joseph
* Gladys Holland as Sadie
* Bill Zuckert as Charlie Parker
* James Hall as Chuck
* Glenda Moore as Kanes Friend
* Rodolfo De Alexandre as Bird Seller
* Lupe Ontiveros as Market Voice #1

==External References==
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 