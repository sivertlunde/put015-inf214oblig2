Psycho II (film)
 
 
{{Infobox film
| name             = Psycho II
| image            = Psycho ii.jpg
| image_size       = 220px
| border           = yes
| alt              = 
| caption          = Theatrical release poster Richard Franklin
| producer         = Hilton A. Green Bernard Schwartz Tom Holland
| starring = {{plainlist|
* Anthony Perkins
* Vera Miles
* Meg Tilly
* Robert Loggia
}}
| music            = Jerry Goldsmith
| cinematography   = Dean Cundey
| editing          = Andrew London Universal Pictures
| released         =  
| runtime          = 113 minutes  
| country          = United States
| language         = English
| budget           = United States dollar|US$5 million   
| gross            = US$34,725,000 
}} Richard Franklin Tom Holland. music score was composed by Jerry Goldsmith.

The film did well financially, grossing over $34 million,    which in-part led to two further sequels. The film was received moderately well critically, and has a 59% fresh rating at   (1990).
 Psycho II by Robert Bloch, which he wrote as a sequel to his original novel Psycho (novel)|Psycho.

Psycho II takes place 22 years after the first film, Norman Bates is released from the mental institution and returns to the house and Bates Motel to continue a normal life. However, it soon becomes apparent that his troubled past is going to continue to haunt him.

==Plot==
Norman Bates (Anthony Perkins) is released from a mental institution after spending 22 years in confinement. Lila Loomis (Vera Miles), sister of Marion Crane, vehemently protests with a petition that she has been circulating with signatures of 743 people, including the relatives of the seven people Norman killed prior to his incarceration, but her plea is dismissed. Norman is taken to his old home behind the Bates Motel by Dr. Bill Raymond (Robert Loggia), who assures him everything will be fine.

Norman is introduced to the motels new manager, Warren Toomey (Dennis Franz). The following day, Norman reports to a prearranged job as a dishwasher and busboy at a nearby diner, run by a kindly old lady named Emma Spool (Claudia Bryar). One of his co-workers there is Mary Samuels (Meg Tilly), a young waitress. After work, Mary claims she has been thrown out of her boyfriends place and needs a place to stay. 

He carries Ms. Spools body upstairs to Mothers room and we hear Mothers voice warn Norman not to play with "filthy girls" and telling him nobody loves him like his mother. Norman reopens the Bates Motel and stands in front of the house, waiting for new customers as Mother watches from the window upstairs.

==Cast==
* Anthony Perkins as Norman Bates Lila Loomis
* Robert Loggia as Dr. Bill Raymond
* Meg Tilly as Mary Samuels
* Dennis Franz as Warren Toomey
* Hugh Gillin as Sheriff John Hunt
* Robert Alan Browne as Ralph Statler
* Claudia Bryar as Emma Spool
* Ben Hartigan as Judge
* Lee Garlington as Myrna

==Production==
In 1982, author Robert Bloch published his novel Psycho II, which satirized Hollywood slasher films. Upset by this, Universal decided to make their own version that differed from Blochs work.  Originally, the film was intended as a TV movie|made-for-cable production.    Anthony Perkins originally turned down the offer to reprise the role of Norman Bates, but when the studio became interested in others (including Christopher Walken), Perkins quickly accepted.    The studio also wanted Jamie Lee Curtis (daughter of Psycho star Janet Leigh) to play the role of Mary Loomis.   Meg Tillys character assumes the name "Mary Samuels," which is a nod to the false name "Marie Samuels" that Janet Leighs character, Marion Crane, assumed when she signed the motel register in the original film.
 Richard Franklin Tom Holland to write the screenplay after Franklin had seen The Beast Within, which Holland had written.  Holland stated: "I approached it with more trepidation because I was doing a sequel to Psycho and I had an overwhelming respect for Hitchcock. You didnt want to mess it up, you really had almost a moral obligation to make something that stayed true to the original and yet updated it the same time. It really was the next step, what happens when Norman gets out". 

Hilton A. Green, assistant director of the original Psycho (1960 film)|Psycho, was contacted and asked if he wanted to produce the film. Green, fearing that Hitchcock may not have approved of sequels to his films, called Hitchcocks daughter Patricia Hitchcock and asked what she thought of the film.  Patricia Hitchcock gave her blessing to the film; saying that her father would have loved it. 

==Filming== Richard Franklin, filming lasted 32 days.  The film was made much like the first film.  It was mostly shot on the Universal backlot and in a number of sound stages.  Several props and set pieces from the original film were found by set designers John W. Corso and Julie Fletcher.  The town of Fairvale (seen when Lila Loomis is tailed by Dr. Raymond) is actually Courthouse Square, which is located on the Universal Studios backlot in California. 

Both Franklin and Holland wanted the film to be a tribute to Alfred Hitchcock and the original film.  To accomplish this, they added various in-jokes such as the scene when Mary and Norman first go into Normans mothers room, before they turn the lights on, Alfred Hitchcocks silhouette is visible on the wall to the far right. Franklin also repeated various shots from the original film such as the shot where Norman walks into the kitchen and sets his jacket down on the chair. The final pages of the shooting script werent distributed to cast and crew until the last day of filming. 

The last shot of the film with Norman standing in front of the house was used as a Christmas card for various crew members.  When Universal presented concept art for the one sheet film poster, director Franklin was not pleased with it.  It was editor Andrew London who came up with the idea of using the Christmas card photo as the film poster and also came up with the tagline: Its 22 years later and Norman Bates is coming home. 

==Music==
Film composer Jerry Goldsmith was hired to write the music for the film. Goldsmith was a long time friend of "Psycho" film composer Bernard Herrmann. On some film assignments Goldsmith would discover that the director had used some of Herrmanns music from other films as temporary soundtracks. Goldsmith would often joke when he discovered this ("Not Benny again!");  when he conducted a rerecording of "The Murder" for the opening of Psycho II he suggested that Herrmann "must be rolling over in his grave." 

Goldsmith had written a theme for Norman Bates that was rejected but used for Segment 2 of  . 

MCA Records released a 30-minute album on LP and cassette; in 2014 Intrada issued the complete score.

Intrada tracklisting (cues in bold appear on the original album):

# The Murder - Bernard Herrmann 0:59 
# Psycho II – Main Title 1:39 
# The House 1:51 
# Mothers Hand 1:54 
# Old Weapons 0:41 
# Cheese Sandwich 0:31 
# Mothers Room 2:05 (called "New Furniture" on the 1983 album)
# Out To Lunch 2:00 
# No Note 1:05 
# The Peep Hole 1:47 
# Toomys Death 1:11 
# Peep Hole #2 (Revised) 0:55 
# Mothers Room #2 4:28 (called "Mothers Room" on the 1983 album)
# Basement Killing 1:18 
# New Furniture 0:44 
# Its Starting Again 0:40 
# A Night Cap 1:08 
# Blood Bath 4:01 
# Dont Take Me 5:39 
# Shes Not Dead 1:16 
# Hello Mother 2:52 
# The Cellar 4:48 
# Its Not Your Mother 5:11 
# Expected Guest 2:44 
# Psycho II – End Title (Revised) 4:18 
# Sonata #14 (Moonlight), Op. 27, No. 2 – 1st Mvt - Ludwig van Beethoven 1:51 
# Sonata #8 (Pathetique), Op. 13 – 2nd Mvt - Ludwig van Beethoven 1:04 
# Peep Hole #2 (Original) 0:56 
# Mothers Room #2 (Alternate No. 1) 4:28 
# Mothers Room #2 (Alternate No. 2) 4:28 
# Psycho II – End Title (Original Version) 4:18

==Release==
When the film opened on June 3, 1983, it earned $8,310,244 in its opening weekend at #2 (behind  ) and went on to gross over $34 million. 
===Critical reception===
  At the Movies, specifically for its failure to live up to the original.  "I think the ghost of the original," said Siskel, "obviously hangs over this movie, and its too bad because its a nicely made picture."  Ebert wrote of the film: "If youve seen Psycho a dozen times and can recite the shots in the shower scene by heart, Psycho II is just not going to do it for you. But if you can accept this 1983 movie on its own terms, as a fresh start, and put your memories of Hitchcock on hold, then Psycho II begins to work. Its too heavy on plot and too willing to cheat about its plot to be really successful, but it does have its moments, and its better than your average, run-of-the-mill slasher movie." 

In the British magazine Empire (magazine)|Empire, film critic Kim Newman gave the film three out of five stars, calling Psycho II "a smart, darkly-comic thriller with some imaginative twists", writing, "The wittiest dark joke is that the entire world wants Norman to be mad, and ‘normality’ can only be restored if he’s got a mummified mother in the window and is ready to kill again."   

==Home media==
Psycho II has been released five times on  .  Shout Factory, under their Scream Factory logo, released Psycho II on DVD & Blu-Ray on September 24, 2013 under their "Collectors Edition" line-up.
 VOD of Mike Nelson, Kevin Murphy of Mystery Science Theater 3000|MST3K fame.) Many comments focused on the films lack of living up to the original and the implausibility of the multiple-murder Norman Bates making it back into society and being largely welcomed. 
 Bates Motel on DVD as part of its "4-Movie Midnight Marathon Pack".

==See also==
 
* Psycho (1960 film)|Psycho, directed by Alfred Hitchcock
* Psycho (1998 film)|Psycho (1998 film), a remake directed by Gus Van Sant
* Psycho III, a 1986 sequel to the first and second films
*  , a 1990 sequel/prequel to the first film Bates Motel, a 1987 television film
* The Psycho Legacy, a 2010 documentary about the series
* Bates Motel (TV series), a 2013 television series that reboots the story in modern-day

==References==
 

==External links==
 
*  
*  
*  
*   at Psychomovies.net
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 