Rokunin no ansatsusha
{{Infobox Film
| name           = Rokunin no ansatsusha 六人の暗殺者
| image          = Rokunin no ansatsusha poster.jpg
| caption        = Japanese movie poster
| director       = Eisuke Takizawa
| producer       = Nikkatsu Hiroshi Sano (producer)
| writer         = Ryuzo Kikushima (writer)
| starring       = 
| music          = Masaru Sato
| cinematography = Harumi Fujii
| editing        = 
| distributor    = Nikkatsu
| released       =   
| runtime        = 107 min.
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
| 
}} Japanese film drama directed by Eisuke Takizawa.

The film won 1956 Blue Ribbon Awards for best screenplay by Ryuzo Kikushima. 

== Cast ==
* Shōgo Shimada (actor)|Shōgo Shimada
* Osamu Takizawa
* Isao Yamagata
* Kunitaro Sawamura

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 


 