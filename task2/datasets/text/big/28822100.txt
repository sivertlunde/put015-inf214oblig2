A Touch of the Other
{{Infobox film
| name           = A Touch of the Other
| director       = Arnold L. Miller
| producer       = Leslie Berens Arnold L. Miller
| writer         = Frank Wyman
| screenplay     = 
| story          = 
| based on       =  
| starring       = Kenneth Cope Shirley Anne Field Hélène Françoise Timothy Craven
| music          = John Hawkins
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1970
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

A Touch of the Other is a 1970 British drama film directed by Arnold L. Miller and starring Hélène Françoise, Kenneth Cope and Shirley Anne Field.  A London private detective finds himself the target of gangster. Delger, "the man who gets things done", finds himself involved in Londons vice world, in between sleeping with his two neighbours Elaine (Shirley Anne Field) and Wendy (Hélène Françoise), a masseuse who "cant give a man a massage without turning him on".

==Cast==
* Hélène Françoise ...  Wendy
* Kenneth Cope ...  Delger
* Shirley Anne Field ...  Elaine
* Timothy Craven ...  Webber
* Vasco Koulolia ...  Hughes
* Noel Davis ...  Max Ronieau
* Renny Lister ...  Sheila
* Sarah Kemp ...  Shirley (as Gypsie Kemp)
* Paul Stassino ...  Connelly
* Jon Laurimore ...  Det. Sgt. Masterson
* Peter Bland ...  Sgt. Phillips
* Vanda Godsell ...  Angela
* Martin Wyldeck ...  Traylor

==References==
 

==External links==
*  

 
 
 
 
 


 