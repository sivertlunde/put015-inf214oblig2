Dakbaksho
 
{{Infobox film
| name           = Dakbaksho
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        =
| director       = Prosenjit Choudhury Abhijit Chowdhury
| producer       = Supriti Choudhury Prosenjit Choudhury
| writer         = 
| screenplay     = Prosenjit Choudhury Abhijit Chowdhury
| story          = Prosenjit Choudhury
| based on       = Shakti Chattopadhyay’s poem Postman in the Autumn’s wood
| narrator       = 
| starring       = Satrajit Sarkar Supriti Choudhury Pradip Roy
| music          = Prosenjit Choudhury
| cinematography = Prosentjit pro
| editing        = Prosentjit pro Prasenjit Choudhury
| studio         = 
|| publicity design = Eastern Woods Productions
| distributor    = 
| released       = June 26 2015
| runtime        = 130 minutes
| country        = India Bengali
| budget         =  
| gross          = 
}} Indian psychological thriller film directed by Prosenjit Choudhury. The film the film explores the crisis of communication between individuals and identities. The film stars Supriti Choudhury, Satrajit Sarkar and Pradip Roy in principal roles. The film is scheduled for release on 26th of June, 2015.

== Synopsis ==
Telegram services has been shut down for ever. Postal service is waiting for the death knell as well. At this time, one starts dropping letters in a letter box, which has been abandoned for many years. These letters carry no addresses on them. The contents of the letters are suspicious and clearly indicate a conspiracy; a sinister one, to murder someone. 

Parallelly, for a reason unknown, perhaps political interference, Srija, a renowned documentary filmmaker, detaches herself from her job, while a mysterious photographer makes way into her house. As the dark recesses of the characters are revealed the audience is hurled into a tale of deception, horror and faced with questions regarding the identity of the girl, the intentions of the photographer and finally how is all this related to a letter box?
And a game begins, a mystery unfolds.

==Cast==
* Supriti Choudhury as Srija: a young Bengali Documentary Filmmaker
* Satrajit sarkar as Avro: a mysterious photographer
* Pradip Roy as The Old man: an elderly mysterious figure who seems to be conducting a grave mission
* Pradip Moulik as Barunda: Srijas friend
* Subrata Chokroborty as Ambar: Srijas Huband
* Bitan Biswas
* Nibedita Nag Tahabildar
* Ruby Santra
* Priya Dutta
* Pubali Saha
* Ilika Bandyopadhyay as Ambars Mistress
* Prosenjit Choudhury: Cameo 
* Abhijit Choudhury
* Debopriya  Roy
* Tapan  Bhattacharya
* Nasir  Mallick
* Amit  Mahato
* Batuk  Mukherkee

==Production==
===Development===
Dakbaksho, for Prosenjit Choudhury, is an effort to create a visual story out of Shakti Chattopadhyay’s famous poem “Postman in the Autumn’s wood”. The biggest challenge was to create the silence out of all the cacophonies, to search the pain and emptiness out of all the drama, humour and thrill of the story.

===Filming=== Talsari & Ichapore.

==Music==
The music for the film has been composed by Prosenjit Choudhury with help from Satrajit sarkar, Abhijit choudhury and Mainak Bhowmik.
The songs, including Kakeder Gaan & Shopno Dekhi Shopno Dekhao Tai are voiced by Manomoy Bhattacharya, Rupankar, Mom, Sunetra & Abhik.

==Release==
The film is scheduled for a release in mid 2015.
 

==External links==
*  
*  

 
 
 
 
 
 
 