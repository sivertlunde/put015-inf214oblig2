What Price Hollywood?
{{Infobox film
| name           = What Price Hollywood?
| image          = File:What Price Hollywood window card.jpg
| image_size     = 225px
| caption        = Theater window card
| director       = George Cukor
| producer       = Pandro S. Berman David O. Selznick
| writer         = Gene Fowler Rowland Brown Ben Markson Jane Murfin
| based on       = Adela Rogers St. Johns (story)
| starring       = Constance Bennett Lowell Sherman
| music          = Max Steiner
| cinematography = Charles Rosher
| editing        = Del Andrews Jack Kitchin RKO Pathé
| distributor    = RKO Pictures|RKO-Pathé Distributing Corp.
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $416,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p39 
| gross          = $571,000 
}}
 American drama film directed by George Cukor and starring Constance Bennett with Lowell Sherman. The screenplay by Gene Fowler, Rowland Brown, Ben Markson, and Jane Murfin is based on a story by Adela Rogers St. Johns.

==Plot==
Brown Derby waitress Mary Evans (Constance Bennett) is an aspiring actress who has an opportunity to meet film director Maximillan Carey (Lowell Sherman) when she serves him one night. He is very drunk but is charmed by the young girl, and he invites her to a premiere at Graumans Chinese Theatre. Adhering to his policy of living life with a sense of humor, he picks her up in a jalopy rather than a limousine and then gives the parking valet the car as a tip.

Max takes Mary home with him after the event, but the next morning remembers nothing about the previous night. She reminds him he promised her a screen test and expresses concern about his excessive drinking and flippant attitude, but he tells her not to worry.

Marys first screen test reveals she has far more ambition than talent, and she begs for another chance. After extensive rehearsals, she shoots the scene again, and producer Julius Saxe (Gregory Ratoff) is pleased with the result and signs her to a contract. Just as quickly as Mary achieves stardom, Max finds his career on the decline, and he avoids a romantic relationship with her for fear she will be caught up in his downward spiral.
 Neil Hamilton). He genuinely loves her and, although he is jealous of the demands made on her by her career, he convinces her to marry him, against Julius and Maxs better judgment. Lonny becomes increasingly annoyed by the dedication of his movie star wife to her work, and finally walks out on her. After their divorce is finalized, Mary discovers she is pregnant.

Mary wins the Academy Award for Best Actress, but her moment of glory is disrupted when shes called upon to post bail for Max after hes arrested for drunk driving. She takes him to her home, where he wallows in self-pity despite her encouragement. Later, alone in Marys dressing room, he stares at his dissolute image in the mirror and compares it to a photograph of himself in earlier days. Finding a gun in a drawer, he kills himself with a bullet to the chest.

Mary becomes the center of gossip focusing on Maxs suicide. Hoping to heal her emotional wounds, she flees to Paris with her son and reunites with Lonny, who begs her to forgive him and give their marriage another chance.

==Cast==
*Constance Bennett as Mary Evans
*Lowell Sherman as Maximilian Max Carey Neil Hamilton as Lonny Borden
*Gregory Ratoff as Julius Saxe
*Brooks Benedict as Muto, Diner Who Will Put Mary In Pictures
*Louise Beavers as Bonita, Marys Maid

==Production== John McCormick Tom Forman, who committed suicide following a nervous breakdown.   

Producer David O. Selznick wanted to cast Clara Bow as the female lead, but executives at RKOs New York offices were hesitant to invest in a Hollywood story because similar projects had been unsuccessful in the past. By the time Selznick convinced them the picture had potential, Bow was committed to another film. 
 A Star 1954 musical remake starring Judy Garland.  

==Reception==
Variety (magazine)|Variety called the film "a fan magazine-ish interpretation of Hollywood plus a couple of twists" and added, "George Cukor tells it interestingly. Story . . . has its exaggerations, but they can sneak under the line as theatrical license." 

TV Guide rated it three out of four stars and commented, "Though the conclusion is a pat romantic ending, this is a strong drama that shows the real Hollywood behind the glamorous facades."  

According to RKO records the film lost $50,000. 

==Awards and honors== The Champ.

==References==
Notes
 

==External links==
*  
*  
*  
*  

 

 
 
 

 
 
 
 
 
 
 
 
 
 
 