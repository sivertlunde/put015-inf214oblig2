Gun Fight (film)
{{Infobox film
| name           = Gun Fight
| image size     = 
| image	         = Gunfightpos.jpg	
| caption        = Film poster
| director       = Edward L. Cahn
| producer       = Edward Small (executive) Robert E. Kent
| writer         = Richard Schayer Gerald Drayson Adams
| based on       = story by Gerald Drayson Adams
| starring       = James Brown Joan Staley Gregg Palmer
| music          = Paul Sawtell Bert Shefter
| cinematography = Walter Strenge
| editing        = Robert Carlisle 
| distributor    = United Artists
| studio         = Zenith Pictures
| released       = 1 May 1961
| runtime        = 67 mins
| country        = USA
| language       = English
| budget         = 
}}
Gun Fight is a 1961 Western  directed by Edward L. Cahn
for Robert E. Kents Zenith Productions that was released through United Artists.  The film features location shots from the Grand Teton National Park.

==Plot== 7th Cavalry and is looking forward to going into business with his brother Brad in Wyoming who has told him he has a large ranch with 2,000 head of cattle.  He discovers that his brother and his associates are in a much different kind of business that Wayne wants no part of.

==Cast== James Brown   ...  Wayne Santley 
*Joan Staley   ... Nora Blaine 
*Gregg Palmer   ...  Brad Santley 
*Ron Soble   ...  Pawnee 
*Ken Mayer   ... Joe Emery 
*Walter Coy   ...  Sheriff  


==References==
 

==External links==
*  at IMDB
 
 

 
 
 
 
 
 
 

 