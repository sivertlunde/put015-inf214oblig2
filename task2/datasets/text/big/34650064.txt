On the Top of the Cherry Tree
{{Infobox film
| name = Горе на черешата Gore na chereshata On the Top of the Cherry Tree
| image = On the Top of the Cherry Tree.jpg
| caption = 
| imagesize = 
| director = Mariana Evstatieva — Biolcheva
| writer = Rada Moskova
| cinematography =  Atanas Tasev
| starring = Veselin Prahov Todor Trankarov Konstantin Kotsev Anton Gorchev
| music =  Boyana 
| released = 1984
| country = Bulgaria
| runtime = 90 minutes
| language = Bulgarian
}}

On the Top of the Cherry Tree (  / Gore na chereshata) is a Bulgarian comedy-drama film released in 1984 in cinema|1984, directed by Mariana Evstatieva — Biolcheva, starring Veselin Prahov, Todor Trankarov, Konstantin Kotsev and Anton Gorchev.

The movie is in the scope of the so-called “Childhood genre”, featuring children in the main parts. In some way it is a sequel of A Dog in a Drawer released two years earlier, both films written by Rada Moskova and starring the child actor Veselin Prahov. The main characters are again urban kids and teenagers with busy parents. They wander in the neighborhood in search of stories, usually using their unadulterated imagination. On the Top of the Cherry Tree strengthened Prahovs status as a superstar of the Bulgarian cinema from the 1980s. The movie became one of the hits of that time.

==Cast==
In the roles of the children are:
*Veselin Prahov as Lin
*Todor Trankarov as Tony 
*Tsvetana Uzunova as Ina
*Emil Dimitrov as Emil

In the roles of the adults are:
*Konstantin Kotsev as Peshev
*Mincho Minchev as himself, the violinist
*Anton Gorchev as Tonys father
*Lyben Chatalov as Lins father
*Rositsa Grigorova as Lins mother
*Lyuba Aleksieva as Grandma Tsetska
*Pavel Popandov as a dogs trader

==References==
 

===Sources===
*    
*    

==External links==
*  
*   at the Bulgarian National Television  

 
 
 
 


 
 