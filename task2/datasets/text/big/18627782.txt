What's Cookin'?
{{Infobox film
| name           = Whats Cookin?
| image          = Whats Cookin? poster.jpg
| image_size     = 
| caption        = 
| director       = Edward F. Cline
| producer       = 
| writer         = Jerome Cady Stanley Roberts Haworth Bromley
| starring       = The Andrews Sisters Jane Frazee Robert Paige Gloria Jean
| music          = 
| cinematography = Jerome Ash
| editing        = Arthur Hilton
| distributor    = Universal Pictures
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Whats Cookin? is a 1942 American musical film starring The Andrews Sisters, Jane Frazee, Robert Paige and Gloria Jean. The film was directed by Edward F. Cline and is based on the story Wake Up and Dream written by Edgar Allan Woolf.

==Plot==
Mrs. Murphys Theatrical Boarding House is a place where young performers reside. A group of those young people try to escape after finding out they are unable to pay the rent. However they get caught by the landlady and fellow tenant Marvo the Great is forced to sell his clothes to pay the rent. They next set out to the radio network WECA to visit singer Anne Payne. Anne is a former boarding house member who now works at the radio station with the Andrews Sisters and Woody Herman and His Orchestra. When Marvo is later conversing with Anne at her apartment, her wealthy neighbour Sue Courtney drops in their conversation and wonders if she can join the group.

Meanwhile at the Courtney estate, Sues uncle and aunt, J.P. and Agatha, meet with their advertising counselor Bob Riley. She complains that the radio station only plays classical songs. Sue offers to help them out by asking her new friends to make swing music for the radio station. They do so and Bob notices Anne, whom he immediately falls in love with. Sue meanwhile falls in love with young performer Tommy.

==Cast==
*The Andrews Sisters as Themselves
*Jane Frazee as Anne Payne
*Robert Paige as Bob Riley
*Gloria Jean as Sue Courtney
*Leo Carrillo as Marvo the Great
*Billie Burke as Agatha Courtney Charles Butterworth as J.P. Courtney
*Grace McDonald as Angela
*Donald OConnor as Tommy
*Peggy Ryan as Peggy
*Franklin Pangborn as Professor Bistell
*Susan Levine as Tag-a-long
*The Jivin Jacks and Jills as Themselves
*Woody Herman as Himself Charles Lane as K.D. Reynolds

==Soundtrack==
*Golden Wedding
**Written by Gabriel Marie
**Special arrangement by Woody Herman
**Played by Woody Herman and His Orchestra 

*Woodchoppers Ball
**Written by Joe Bishop
**Played by Woody Herman and His Orchestra

*Ill Pray for You
**Written by Arthur Altman and Kim Gannon
**Played by Woody Herman and His Orchestra
**Sung by Jane Frazee, Gloria Jean, and The Andrews Sisters

*What to Do?
**Written by Sid Robin
**Played by Woody Herman and His Orchestra
**Sung by The Andrews Sisters

*You Cant Hold a Memory in Your Arms
**Written by Hy Zaret and Arthur Altman
**Played by Woody Herman and His Orchestra
**Sung by Jane Frazee

*Blue Flame
**Written by James Noble
**Played by Woody Herman and His Orchestra

*Lo, Hear the Gentle Lark
**Sung by Gloria Jean

*If
**Played by the studio orchestra
**Sung by the studio vocal group

*Love Laughs at Anything
**Written by Gene de Paul, and Don Raye
**Sung by Gloria Jean

*Pack Up Your Troubles in Your Old Kit Bag and Smile, Smile, Smile!
**Music by Felix Powell
**Lyrics by George Asaf
**Played by Woody Herman and His Orchestra
**Sung by Jane Frazee, Gloria Jean, and The Andrews Sisters

*Amen Spitural
**Written by Vic Schoen, and Roger Segure
**Played by Woody Herman and His Orchestra
**Sung by Jane Frazee, Gloria Jean, and The Andrews Sisters

==Reception==
The New York Times noticed there is "plenty cookin in this brisk, breezy Andrews Sisters vehicle". It also called the supporting cast "topheavy".  The website Answers.com gave the film one star. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 