24 (2001 film)
{{Infobox film
| title          = 24
| image          = 
| caption        = 
| director       = David Beránek
| producer       = Jana Bartoňová Adolf Zika
| writer         = Jana Doležalová
| starring       = 
| music          = David Rotter
| cinematography = Jan Bartoň
| editing        = David Beránek
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
}}
24 is a 2001 Czech thriller film directed by David Beránek, starring Barbora Seidlová and Martin Trnavský. It takes place during 24 hours and follows a man and a woman on the run on the Czech countryside. The film was released through Bontonfilm on 11 January 2001. 

==Cast==
* Pavel Trnavský as Martin
* Barbora Seidlová as dívka
* Jiří Tomek as grandfather

==References==
 

 
 
 
 
 
 