The Perfect Flapper
{{Infobox film
| name        = The Perfect Flapper
| image       = Perfect flapper.jpg
| producer    = Earl Hudson
| starring    = Colleen Moore Sydney Chaplin Phyllis Haver Lydia Knott John Francis Dillon
| writers     = Jessie Henderson (story) Joseph F. Poland
| studio      = Associated First National
| distributor = First National
| released    =  
| runtime     = English intertitles
| country     = United States
| music       =
| budget      =
}}
 Flaming Youth. Flaming Youth but did not make it to the theaters until after) and Painted People. The film is preserved at the Library of Congress along with a trailer. 

==Story==
Young debutante Tommie Lou is unpopular. At her coming-out party, she turns to jazz antics to liven things up. After drinking punch spiked with alcohol (illegal at the time, as the film was made during Prohibition), she gets drunk and runs off to a road house with the husband of a friend. Nothing happens between them, but the action provokes a split between the husband and his wife. She contrives to get the couple back together, falling for the wifes divorce lawyer, and in the end everyone lives happily.

==Cast==
*Colleen Moore - Tommie Lou Pember
*Sydney Chaplin - Dick Trayle
*Phyllis Haver - Gertrude Trayle
*Lydia Knott - Aunt Sarah Frank Mayo - Reed Andrews
*Charles Wellesley - Joshua Pember

==Background==
 
The film was made in the wake of the tremendous hit   at $798,777 by 1928; The Perfect Flapper earned $531,008.56. 
 

==References==
 

==Bibliography==
*Jeff Codori (2012), Colleen Moore; A Biography of the Silent Film Star,  ,(Print ISBN 978-0-7864-4969-9, EBook ISBN 978-0-7864-8899-5).

 
 
 
 
 
 
 
 
 