Anna and the Moods
{{Infobox film
| name           = Anna and the Moods
| image          = AnnaPoster en.jpg
| caption        = 
| director       = Gunnar Karlsson
| producer       = Hilmar Sigurðsson Arnar Thorisson
| writer         = 
| screenplay     = Sjón
| story          = Sjón
| narrator       = Terry Jones
| starring       = Björk Damon Albarn Sjón
| music          = Julian Nott
| cinematography = 
| editing        =  CAOZ Studio
| distributor    = 
| released       =  
| runtime        = 27 minutes
| country        = Iceland
| language       = Icelandic language|Icelandic/English
| budget         = 
| gross          = 
}}
 computer animated film by the Icelandic digital design and animation company CAOZ in Reykjavík. The plot centers on a girl named Anna Young who contracts a horrible illness that makes her incredibly moody.
 Icelandic and English. Anna and the Moods won the Edda Award for Best Short Film.

== Plot ==
Anna Young has always been the perfect daughter. One day, Annas attitude dramatically changes, leaving her parents worried. Unable to figure out what has happened to their daughter, Annas parents take her to Dr. Artmanns clinic for unruly children. At the clinic Anna is exposed to a variety of obstacles in a complex labyrinth. The only way out of the maze for Anna is to complete the tasks in the right way. Anna, however, decides to create mischief to escape. This leads Dr. Artmann to come to a shocking conclusion about Anna, the cure of which is a nasty surprise for her parents. 

== Cast ==

* Terry Jones, narrator
* Björk as Anna Young
* Damon Albarn as Mr. Young
* Thorunn Larusdottir as Mrs. Young
* Sjón as Dr. Artmann
* Jón Páll Eyjólfsson as Granny, Annas Uncle, Hunter, Michael, MUM figure
* Hreidar Smárason as Little Brother
* Martin Regal as Normal Boy
* Ámundi Sigurdsson as Healthy Boy
* Einar Örn Benediktsson as Peter the Goth
* Stefán Karl Stefánsson, special ad lib and extras

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 


 
 