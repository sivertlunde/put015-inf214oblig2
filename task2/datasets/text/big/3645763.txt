Terminal Station (film)
{{Infobox film
| name           = Terminal Station
| image          = Terminal Station poster.jpg
| caption        = 
| director       = Vittorio De Sica
| producer       = Vittorio De Sica
| writer         = Cesare Zavattini (story) Luigi Chiarini Giorgio Prosperi   Jennifer Jones Richard Beymer
| music          = Alessandro Cicognini
| cinematography = Aldo Graziati
| editing        = Eraldo Da Roma Jean Barker
| distributor    = Columbia Pictures De Sica Productions Selznick International Pictures
| released       =        
| runtime        = 89 minutes
| country        = Italy United States
| language       = Italian English}}

Terminal Station ( ) is a 1953 film by Italian director Vittorio De Sica. It tells the story of the love affair between an Italian man and an American woman. The film was entered into the 1953 Cannes Film Festival.   

==Production== Jennifer Jones. The production of the film was troubled from the very beginning. Carson McCullers was originally chosen to write the screenplay, but Selznick fired her and replaced her with a series of writers, including Paul Gallico, Alberto Moravia and Capote.  Disagreements ensued between De Sica and Selznick, and during production, Selznick would write 40- and 50-page letters to his director every day, although De Sica spoke no English. After agreeing to everything, De Sica has said, he simply did things his way. 

Montgomery Clift sided with De Sica in his disputes with Selznick, claiming that Selznick wanted the movie to look like a slick little love story, while De Sica wanted to depict a ruined romance. "Love relationship are ludicrous, painful, and gigantically disappointing. This couple loves each other but they become unconnected." 
 Robert Walker, and badly missed her two sons, who were at school in Switzerland. P. Bosworth, Montgomery Clift, p. 246  She had been married to Selznick less than two years at that point, and they were having difficulties in the marriage.

The original release of the film ran 89 minutes, but it was later re-edited by Selznick down to 64 minutes and re-released as Indiscretion of an American Wife (and as Indiscretion in the UK). Clift declared that he hated the picture and denounced it as "a big fat failure.".  Critics of the day agreed, giving it universally bad reviews.  The two versions have been released together on DVD by The Criterion Collection. A 1998 remake of the film was made for television under the title Indiscretion of an American Wife.

==Cast==
* Jennifer Jones as Mary Forbes
* Montgomery Clift as Giovanni Doria
* Richard Beymer as Paul
* Gino Cervi as Police Commissioner

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 

 