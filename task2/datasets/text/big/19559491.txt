Oskar (film)
 
{{Infobox film
| name           = Oskar
| image          = Oskar 1962 Gabriel Axel.jpg
| caption        = Poster
| director       = Gabriel Axel
| producer       = Erik Overbye
| writer         = Bob Ramsing
| starring       = William Bewer
| music          = Ib Glindemann
| cinematography = Henning Bendtsen
| editing        = Lars Brydesen
| distributor    = 
| released       =  
| runtime        = 
| country        = Denmark
| language       = Danish
| budget         = 
}}
Oskar is a 1962 Danish film directed by Gabriel Axel and starring  .    

==Cast==
*   - Chauffør hos Rosentorn
* Vera Gebuhr - Charlotte, stuepige
* Judy Gringer - Tina, stuepige hos Bang
* Lone Hertz - Vibeke Bang, Bernhards datter
* Ebbe Langberg - Oskar, chauffør
* Ghita Nørby - Eva Hansen, kontorpige
* Dirch Passer - Martin Kristiansen, direktør
* Birgitte Reimer - Mona Bang, Bernhards kone
* Ove Sprogøe - Bernhard Bang, direktør
* Axel Strøbye - Egon Larsen, massør

==References==
 

==External links==
* 
*  at the Danish National Filmography

 

 
 
 
 
 