Angel Dog
 

{{Infobox film
| name           = Angel Dog
| image          = 
| alt            = 
| caption        = Film poster
| director       = Robin Nations
| producer       = Kevin Nations
| writer         = Robin Nations
| starring       = Jon Michael Davis Farah White Richard Dillard Mona Lee Fultz Maurice Ripke Ashley Hallford
| music          = Peter Himmelman
| cinematography = Kevin Nations
| editing        = Kevin Nations
| studio         = The Nations
| distributor    = Screen Media Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
}}

Angel Dog is a 2011 family film about a dog who bonds with Jake, a survivor of a car accident, and helps him get over a tragic loss. This film is written and directed by Robin Nations and produced and cinematography by Kevin Nations.  The two are a husband and wife team that go by The Nations.  The film’s score was composed and performed by Peter Himmelman, a critically acclaimed American singer-songwriter Grammy Award nominee.

The film premiered in Los Angeles, California at the 2011 International Family Film Festival on March 19, 2011.

==Mission==
After The Nations produced and directed their 2nd film, “Leftovers”,  the husband and wife team, inspired by their love of dogs, set out to make a family dog film.  “Angel Dog” marked their first push towards their mission to “Bring back family movie night”.  The Nations goal is to bring to market good wholesome films the entire family can watch.

==Plot==
This heartwarming film is about a dog named Cooper who is the lone survivor of a terrible car accident. Jake, a family patriarch, loses his wife and children in the accident. Not being a dog person, Jake is angry and resentful toward the dog for even surviving. However, eventually Jake bonds with Cooper, and this bond ends up being the one thing that gets him out of bed in the morning, the one thing that helps him to go on living after such a tragic loss.

==Cast==
*Jon Michael Davis as Jake Bryant
*Farah White as Caroline Mason
*Richard Dillard as Seth
*Mona Lee Fultz as Bobbie
*Maurice Ripke as Trey
*Ashley Hallford as Nita

==Reception==
“Angel Dog” received a 5 Dove rating from The Dove Foundation, which is the foundation’s highest honor possible.  Reviewer, Edwin L. Carpenter stated, “Angel Dog” is a movie which adults along with kids will enjoy. Don’t let the title fool you. The movie deals with something everyone can relate to: death and loss, and the subsequent grieving process. Yet “Cooper”, a stray, is a lovable dog who seems to have a mission: to help those who are lonely following a loss. Then he moves on to the next need. 

==References==
 

==External links==
*  
*  
*  

 
 
 