Aptharakshaka
{{Infobox film
| name           = Aptharakshaka
| image          = Aptharakshaka.jpg
| caption        = Promotional poster
| director       = P. Vasu
| producer       = Krishna Kumar (Krishna Prajwal)
| writer         = P. Vasu Bhaskar Vishnuvardhan Avinash Sandhya Vimala Bhavana  Lakshmi Gopalaswamy Komal Kumar Srinivasa Murthy Ramesh Bhat
| music          = Gurukiran
| cinematography =
| editing        = Suresh Urs
| studio         = Udaya Ravi Films
| distributor    = Krishna Prajwal
| released       =  
| runtime        = 152 minutes
| country        = India
| language       = Kannada
| budget         =   15 crore
| gross          =    55 crore
}} Vishnuvardhan in posthumous film Komal among blockbuster film Apthamitra also directed by Vasu and starring Vishnuvardhan.
 Telugu as Venkatesh Daggubati. Telugu as " Raja Vijaya Rajendra Bahadur (Nagavalli - Return of Chandramukhi)" and in hindi as " Sab Ka Rakhwala" under the production of Sumeet Arts.

== Plot ==
The story starts off with an ancient painting of Nagavalli (Vimala Raman), distributed as a prize to Bharata Natyam dancer Saraswathi (Lakshmi Gopalaswamy), her husband and her family. On the Engagement day of Gowri (Sandhya (actress)|Sandhya), one of her friends had fainted by encountering a huge thirty-feet Snake, and the bridegroom had run away from the family house fearing of something. All strange incidents happen as the family called for a snake charmer to the house, but the snake charmer had died when he attempted to make the serpent appear. The members of the family are psychologically affected by the presence of Nagavallis painting and things have not been going well in the house. So they decide to contact Acharya Ramchandra Shastry (Avinash), an astrologer cum sage. The father of the three daughters tells the Acharya that Saraswathi and her husband met with a fatal accident and died, after the bharathanatyam competition. The Acharya takes the help of Dr Vijay (Vishnuvardhan (actor)|Vishnuvardhan), a psychiatrist, to solve the problem. All directions point to the huge portrait of Nagavalli, and is observed that the portrait is the cause for this. Everyone is warned to not go to the outhouse or to the room where Nagavallis painting is. One night, Dr. Vijay goes to the outhouse as he heard anklet sounds, and he sees another smaller portrait of Nagavalli in the outhouse. It is then revealed that Saraswathi is still alive, but became mad after the truck accident in which her husband died while he was carrying the painting of Nagavalli, and that she said that no one accepted to marry the second daughter Geetha because Saraswathi was mad and so Geetha vowed that she wouldnt marry, and since they didnt want Gowris situation to be like this, they lied saying Saraswathi was dead, and locked her up in the outhouse. In a few occasions, someone attempted to murder the Acharya twice. Suspecting Saraswathi, the acharya asks the family members to bring Saraswathi to the temple and that he will show everyone that Nagavalli is in Saraswathis body, but as Saraswathi steps the temple, all the animals run out of the temple. Suspecting each and every person in the family, Dr. Vijay starts to investigate everything so he goes to the library to read a book based on Vijaya Rajendra Bahaddurs life and further information on Nagavalli too.

The investigation also takes back to around 125 years when Raja Vijaya Rajendra Bahaddur (Vishnuvardhan) used to live and how his enmity with Nagavalli has been going on since centuries. Dr. Vijay becomes very shocked to see the Vijaya Rajendra Bahaddur for two reasons: 1. The portrait he saw in the old palace 5 years ago, was in fact the portrait of Vijaya Rajendra Bahaddurs elder brother, Vinaya Rajendra Bahaddur, who was killed by Vijaya Rajendra Bahaddur himself for the sake of ruling the kingdom. 2. Vijaya Rajendra Bahaddur resembles Dr. Vijay. It is revealed in the book that he had attempted to escape as one of his men had spread a dirty rumour about him, which caused the whole Ramnagara Dominion and the people to go and slaughter and assassinate him... but the book doesnt say if he got assassinated, or if he really committed suicide or if he is alive. Dr. Vijay finishes reading the book and whilst signing the ledger, he reads the name above him: Nagavalli... and he understands that she had read the book before he had. To further investigate about Nagavalli, Dr. Vijay goes to Nagavallis place in Peddapuram, Andhra Pradhesh and an old man told the Doctor that Nagavallis family died back at around a hundred years and told him that he was the second person to enquire about Nagavallis family, and as the Doctor asked who that person was, the old man told him that a girl came and inquired about Nagavalli, and he also says that she was fine whilst asking the questions, but when she returned from Nagavallis place, she was holding a portrait of Nagavalli and ran away like a mad girl, so the Doctor goes to Nagavallis house and realizes that the portrait in the outhouse was the portrait of Nagavalli which had been taken, as he glanced at an empty wall with a rectangular marking outline. Dr. Vijay also investigates about Saraswathis husbands death and the snake charmers death, and he comes with information that the snake charmers death and Saraswathis husbands death had nothing to do with Nagavalli. Dr. Vijay reveals that Raja Vijaya Rajendra Bahaddur is still  alive when he printed out an astrological profile and showed the astrologer, who revealed it. So Dr. Vijay goes to the Fort where the Raja is, and at the time, the Acharya performs a pooja to try to bring Nagavalli out of Saraswathis body. The Doctor luckily escapes from the invincible Raja, and when the Acharya asks for Saraswathis name, she says her name as Nagavalli Saraswathi, but is revealed that she is still mad, but not affected by Nagavalli. Dr. Vijay comes back to cure Saraswathi from her madness, and he succeeds, but the problem of Nagavalli is not finished yet, and he knows who is affected by Nagavalli. Later the Doctor reveals to everyone that Gowri is affected by Nagavalli as he makes her angry and change personality from Gowri to Nagavalli in front of everyone, then she swoons after she returns to normal. Then later, he explains to everyone that Gowri had written both the names while she signed the ledger: Gowri in English when she was borrowing the book, Nagavalli in Telugu when she was returning the book, and that it was also her who scared the bridegroom away on the Engagement day and sat downstairs like as if she didnt know anything. In order to know more about Nagavallis lover Ramanatha (Vineeth), she went to Nagavallis house as Gowri, and returned with Nagavallis photo, completely as Nagavalli. Gowri later goes to the fort where Vijaya Rajendra Bahaddur is, to kill him. Dr. Vijay, knowing Gowri would have definitely seek revenge goes to the fort where the Raja almost gets burnt to death, but he survives when it starts to rain, then Dr. Vijay has a battle with him, and the Raja almosts decapitates Gowri/Nagavalli, but before he does so, the Raja gets struck by a lightning; thus, throwing his sword above and as it comes down, it stabs the Raja in the neck, who then dies. Gowri is no more affected by Nagavalli anymore and is going to marry the bridegroom she scared away, and Geetha is going to marry too and it all comes to a happy ending.

== Cast == Vishnuvardhan as Dr. Vijay / Vijayarajendra Bahaddur Sandhya as Gowri / Nagavalli (present)
* Vimala Raman as Nagavalli (past)
* Avinash as Ramachandra Acharya
* Lakshmi Gopalaswamy as Saraswathi
* Komal Kumar as Dr. Srinath Bhavana as Neetha
* Vinaya Prasad Suja
* Srinivasa Murthy
* Ramesh Bhat
* Rajesh as Veera Sena

==Production==

===Casting=== Sneha would play the lead role. But due to her busy schedule, Sneha was unable to join the sets and then she was replaced by Sandhya (actress)|Sandhya. Later Bharatha Natyam dancer actress Vimala Raman was selected for the role of Nagavalli, a dancer in the film. Vishnuvardhan died following a cardiac arrest on 30 December 2009. Vishnuvardhan had completed the voice dubbing before his death.

===Filming===
The film started filming in March 2009 in Palani, Tamil Nadu.The majority of shooting was done in Mysore. 

==Awards== Vishnuvardhan won the Karnataka State Film Award for Best Actor for his scintillating and splendid performance in this film.
 Filmfare Awards
;;Won Best Supporting Actor - Avinash Best Male Playback Singer - S. P. Balasubrahmanyam|Dr.S. P. Balasubrahmanyam - "Gharane" Best Lyricist - Kaviraj - "Gharane"
;;Nominated Best Film - Krishna Kumar Best Supporting Actress - Lakshmi Gopalaswamy Best Music Director - Guru Kiran

==Soundtrack==
{{Infobox album
| Name        = Aptharakshaka
| Type        = soundtrack
| Artist      = Guru Kiran
| Cover       =
| Released    = 2010
| Recorded    =
| Genre       = Film soundtrack
| Length      =
| Label       = Swarna Audio
| Producer    =
}}

The soundtrack of the film is popular. 

{{Track listing
| extra_column = Performer(s)
| title1 = Chamundi Taayi Aane | extra1 = S. P. Balasubrahmanyam|SPB, Gurukiran
| title2 = Kabadi Kabadi | extra2 = SPB, Shamitha Malnad
| title3 = Garane Gara Garane | extra3 = SPB Nanditha
| title5 = Omkara | extra5 = Lakshmi Nataraj
| title6 = Kabadi Kabadi | extra6 = Karthik (singer)|Karthik, Shamita Malnad
}}

==Reception==

===Critics===
The film got almost good reviews from critics. Vishnuvardhan (actor)|Vishnuvardhans performance was praised. 

===Box office===
The grand opening in Bangalore was well attended, with black market tickets selling for up to Rs 3000 Rs. 

== References ==
 

==External links==
*  
 

 
 
 
 
 
 
 
 
 
 