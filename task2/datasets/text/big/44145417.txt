Enganeyundashaane
{{Infobox film
| name           = Enganeyundashaane
| image          =
| caption        =
| director       = Balu Kiriyath
| producer       = Renji Mathew
| writer         = K Viswanathan Balu Kiriyath (dialogues)
| screenplay     = Balu Kiriyath Menaka C. I. Paul
| music          = Raveendran
| cinematography = Ashok Chowdhary
| editing        = VP Krishnan
| studio         = Centenary Productions
| distributor    = Centenary Productions
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Menaka and C. I. Paul in lead roles. The film had musical score by Raveendran.   

==Cast==
*Mammootty
*Thilakan Menaka
*C. I. Paul
*Kundara Johny Kunchan
*Thodupuzha Vasanthi

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Balu Kiriyath. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chakravarthi || K. J. Yesudas, Raveendran || Balu Kiriyath || 
|-
| 2 || Pinangunnuvo || S Janaki || Balu Kiriyath || 
|-
| 3 || Pinangunnuvo || P Jayachandran || Balu Kiriyath || 
|-
| 4 || Sopana gaayikaye || K. J. Yesudas, S Janaki, Raveendran || Balu Kiriyath || 
|}

==References==
 

==External links==
*  

 
 
 

 