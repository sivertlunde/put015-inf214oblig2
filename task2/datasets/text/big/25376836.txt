Good Evening, Mr. Wallenberg
{{Infobox film
| name           = Good Evening, Mr. Wallenberg
| image          = 
| caption        = 
| director       = Kjell Grede
| producer       = Katinka Faragó
| writer         = Kjell Grede
| starring       = Stellan Skarsgård
| music          = 
| cinematography = Esa Vuorinen
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 118 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}

Good Evening, Mr. Wallenberg ( ) is a 1990 film about Swedish World War II diplomat Raoul Wallenberg, who was instrumental in saving the lives of thousands of Hungarian Jews from the Holocaust. He is played by Stellan Skarsgård.
 Best Direction Best Screenplay (Kjell Grede), and Best Cinematography (Esa Vuorinen).    It was also entered into the 41st Berlin International Film Festival.   

==Cast==
*Stellan Skarsgård as Raoul Wallenberg
*Katharina Thalbach as Mária
*Károly Eperjes as László Szamosi
*Miklós Székely B. as Ferenc Moser
*Erland Josephson as Rabbi
*Franciszek Pieczka as Papa
*Jesper Christensen as Officer at Watteau
*Ivan Desny as General Schmidthuber (a portrayal of real-life Pál Szalai)
*Géza Balkay as Gábor Vajna
*Percy Brandt as Swedish Ambassador
*Tamás Jordán as member of the Jewish Council
*Andor Lukáts as  Father at the train 
*Gábor Reviczky as Officer at the Gate
*László Soós as Adolf Eichmann

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 