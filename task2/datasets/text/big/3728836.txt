I Am Trying to Break Your Heart: A Film About Wilco
{{Infobox film
| italic title = force
| name = I  Am Trying to Break Your Heart: A Film About Wilco
| image =I-am-trying-to-break-your-heart.jpg
| caption =  Sam Jones Sam Jones Gary Hustwit
| writer = 
| starring = Jeff Tweedy John Stirratt Leroy Bach Glenn Kotche Jay Bennett Tony Margherita
| music = Wilco
| cinematography = 
| editing = Erin Nordstrom
| distributor = Cowboy Pictures  Plexifilm
| released = 2002
| runtime = 92 minutes English
| budget = 
|}} Sam Jones which follows the American alt-country rock band Wilco through the creation and distribution of their fourth studio album Yankee Hotel Foxtrot. The film provides insight into the politics of bands and their record labels, as well as an inside look at Wilco during the creation of what many critics describe as a landmark album. The title of the film shares its name with the first track of Yankee Hotel Foxtrot.

== Overview ==
With Wilco nearing completion of their album Yankee Hotel Foxtrot, conflict arose between the band and its record label Reprise Records|Reprise, a division of the Warner Music Group. Wilcos prior albums hadnt performed to Reprises sales expectations and Reprise were concerned with how to market the new album. They consequently rejected the work and dropped Wilco from the label.

With a completed album and no contractual obligations to Reprise, Wilco made the album available to download on their website. Awareness of the new album became apparent and Wilcos profile was rising. In response, another record label, Nonesuch Records, offered Wilco a new record contract. Nonesuch Records is a division of Warner Music Group, like Reprise, so Wilco were essentially paid twice for the album by the same record company.

Other scenes depicted the breakdown of the relationship between members Jeff Tweedy and Jay Bennett, and Tweedys debilitating migraines.

== Film festival screenings ==
The film was submitted to and selected as part of the Official Selections of the Los Angeles, London and Stockholm International Film Festivals in 2002.

== DVD releases ==
(Note that there are two versions of the film on DVD: The original two-disc set, and a newer version  which omits Disc 2 and is generally less expensive.)

Disc 1
*The film (digital transfer, 16x9 anamorphic presentation)
*Feature commentary from director Sam Jones and Wilco
*Original theatrical trailer
*English subtitles for the hearing impaired

Disc 2
*Over 70 minutes of extra footage, featuring 17 additional Wilco songs, alternate versions of songs from Yankee Hotel Foxtrot, live concert performances and new unreleased songs
*I Am Trying To Make A Film making-of featurette

Plus:
*Deluxe 40-page booklet with filmmakers diary, exclusive photos and liner notes from Rolling Stones David Fricke

=== DVD Disc 2 bonus track listing ===
* "Pot Kettle Black" *
* "Busy Bee Monkey Song" (also known as "Monkey Mess")
* Being There (Wilco album)|"Why Would You Wanna Live"
* "Pieholden Suite" *
* Acuff-Rose Music|"Acuff-Rose"
* "Please Tell My Brother"
* "Cars Cant Escape" *
* "Im the Man Who Loves You" *
* "Magazine Called Sunset"
* "Wait Up"
* "Radio Cure" *
* "Monday"
* "Not for the Season"
* "Sunken Treasure"

All songs written by Jeff Tweedy, except as noted *, written by Jeff Tweedy and Jay Bennett

== References ==
 
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 