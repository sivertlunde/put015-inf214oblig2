Story of a Poor Young Man (1968 film)
{{Infobox film
| name = Story of a Poor Young Man 
| image = 
| image_size =
| caption =
| director = Enrique Cahen Salaberry
| producer = Carlos García Nacson 
| writer =   Octave Feuillet  (novel)   Virgilio Muguerza 
| narrator =
| starring = Leo Dan   Niní Marshall   Rafael Carret   Guillermo Battaglia
| music = José Carli   Lucio Milena
| cinematography = Mario Pagés     
| editing =   Jacinto Cascales 
| studio = Producciones García Nacson 
| distributor = Artistas Argentinos Asociados 
| released = 11 April 1968
| runtime = 90 minutes
| country = Argentina Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} musical drama film of the same name, which was based on a novel by Octave Feuillet. 

==Cast==
* Leo Dan as Leonardo Martinéz Dan  
* Niní Marshall as Carolina 
* Rafael Carret as Mariano 
* Erika Wallner as Margarita Quijano  
* Guillermo Battaglia as Santiago Quijano  
* Roberto Airaldi as Don Lorenzo 
* Silvina Rada as Luisita 
* Los Títeres de Horacio
* Santiago Ayala   
* Norma Viola   
* Susana Giménez
* Abel Sáenz Buhr

== References ==
 

== Bibliography ==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.  

== External links ==
* 

 
 
 
 
 
 
 
 

 

 