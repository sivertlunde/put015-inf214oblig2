Alemão (film)
{{Infobox film
| name           = Alemão
| image          = Alemão Film Poster.jpg
| caption        = Theatrical release poster
| director       = José Eduardo Belmonte
| producer       = 
| writer         = Gabriel Martins
| starring       = Antônio Fagundes  Cauã Reymond  Caio Blat  Gabriel Braga Nunes  Marcello Melo Jr.   Milhem Cortaz
| music          = 
| cinematography = 
| editing        = 
| distributor    = Downtown Filmes 
| studio         = RT Features
| released       =  
| runtime        = 
| country        = Brazil
| language       = Portuguese
| budget         = BRL|R$ 4 million   
}}
Alemão is a 2014 Brazilian action film|action-drama film directed by José Eduardo Belmonte starring Antônio Fagundes, Cauã Reymond, Caio Blat, Gabriel Braga Nunes, Marcello Melo Jr. and Milhem Cortaz. 

The story presented in the film takes place a few days before the invasion of the Complexo do Alemão by the military police, which occurred on [[2010 Rio de Janeiro Security Crisis|
November 26, 2010]]. 

==Plot==
Doca (Otávio Müller), Danillo (Gabriel Braga Nunes), Carlinhos (Marcello Melo Jr.), Branco (Milhem Cortaz) and Samuel (Caio Blat) are five policemen working undercover at the Complexo do Alemão, an area which gathers several slums and is considered one of the most dangerous places in Rio de Janeiro.   Their operation goes wrong after the government threatens to raid the place using military vehicles, which leads local drug lord Playboy (Cauã Reymond) to cut off all the slums communications, leaving them unable to contact their boss, Samuels father sheriff Valadares (Antonio Fagundes) and forcing them to hide at the basement of Docas pizzeria, which serves as their base of operations. Their situation is worsened when Valadares assistant Pixixo (Izak Dahora) is almost killed by Playboys men and drops a briefcase containing details on all the policemen infiltrated in the slums.

All five police officers manage to meet at the pizzeria, some barely escaping from the drug dealers. Carlinhos is the last to arrive and is thus immediately blamed by Branco (the chief of the operation) as the one who gave up their real identities. He denies, but ends up handcuffed. Carlinhos is also the lover of Leticia (Aisha Jambo), sister of Senegal (Jefferson Brasil), Playboys right-hand man.

Meanwhile, Mariana (Mariana Nunes), a local resident who works as a cleaner, leaves her son at home and enters the pizzeria with her own key to claim her services money. She goes down into the basement and is captured by Branco. Even after Doca explains that shes innocent and has a son, Branco forbids her of getting out, fearing she may reveal their location, specially after he discovers her sons father is Playboy.

Meanwhile, tension grows as the government assembles military forces to raid the slums and Valadares is frustrated at not being able to contact his son and his officers. Playboy has his henchmen interrogate people on the streets and in their houses, seeking information leading to the police officers.

Knowing that Carlinhos is in love with Leticia, Playboy convinces Senegal to use his own sister as a bait for the officer. He puts her in a car and travels around the slum, threatening to kill her and using a loudspeaker to make his intentions known to Carlinhos wherever he is. Carlinhos is finally allowed by Branco to go rescue the girl. After hearing Mariana say that police officers always leave their partners behind when hell breaks loose, Branco goes to his aid. He tells Senegal that they have Mariana, Playboys long-time passion, and that he must let them go if they want Mariana alive. After the officers leave, Senegal decides to break into their pizzeria, but without asking Playboy first.

He assembles a bunch of gunmen and raids the pizzeria. No one survives the resulting gunfight but for Leticia and Mariana. Mariana is later found by Pixixo, who puts her on the phone with Valadares. She confirms that no officer survived the battle, and Valadares collapses, crying over the loss of his son.

The film ends with scenes and facts about the 2010 Complexo do Alemão raid, which are followed by images of the 2013 protests in Brazil.

== Cast ==
* Caio Blat as Samuel
* Milhem Cortaz as Branco
* Otávio Müller as Doca
* Cauã Reymond as Playboy
* Gabriel Braga Nunes as Danillo
* Marcello Melo Jr. as Carlinhos
* Antonio Fagundes as sheriff Valadares
* Mariana Nunes as Mariana
* Jefferson Brasil as Senegal
* Aisha Jambo as Leticia
* Izak Dahora as Pixixo

==References==
 

==External links==
*  

 
 
 
 
 
 
 