Texas Masquerade
{{Infobox film
| name           = Texas Masquerade
| image          = Texas Masquerade poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = George Archainbaud
| producer       = Harry Sherman
| screenplay     = Jack Lait Jr. Norman Houston  William Boyd Andy Clyde Jimmy Rogers Don Costello Mady Correll Francis McDonald
| music          = 
| cinematography = Russell Harlan
| editing        = Walter Hannemann 	
| studio         = Harry Sherman Productions
| distributor    = United Artists
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film William Boyd, Andy Clyde, Jimmy Rogers, Don Costello, Mady Correll and Francis McDonald. The film was released on February 8, 1944, by United Artists.  

==Plot==
 

== Cast ==		 William Boyd as Hopalong Cassidy
*Andy Clyde as California Carlson
*Jimmy Rogers as Jimmy Rogers
*Don Costello as Ace Maxson
*Mady Correll as Virginia Curtis
*Francis McDonald as Sam Nolan Russell Simpson as J.K. Trimble
*J. Farrell MacDonald as John Martindale
*Nelson Leigh as James Corwin Robert McKenzie as Marshal Rowbottom
*Pierce Lyden as Henchman Al
*June Pickerell as Mrs. Emma Martindale 
*Bill Hunter as Deputy Lou Sykes
*John Merton as Henchman Jeff 

== References ==
 

== External links ==
*  
 
 
 
 
 
 
 
 
 
 

 