Vampire in Brooklyn
{{Infobox film
| name           = Vampire in Brooklyn
| image          = Vampire in brooklyn.jpg
| caption        = The movie cover for Vampire in Brooklyn.
| director       = Wes Craven
| producer       = Eddie Murphy Mark Lipsky Charles Murphy Michael Lucker Chris Parker
| story          = Eddie Murphy Vernon Lynch Charles Murphy John Witherspoon Zakes Mokae Joanna Cassidy
| music          = J. Peter Robinson
| editing        = Patrick Lussier
| cinematography = Mark Irwin
| studio         = Eddie Murphy Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 102 minutes
| language       = English
| budget         = $14 million  
| gross          = $19,751,736   
}}
 1995 United American horror horror comedy comedy film John Witherspoon, Charles Q. Italian gangster.

Vampire in Brooklyn was the final film produced under Eddie Murphys exclusive contract with Paramount Pictures, that began with 48 Hrs. and included the Beverly Hills Cop franchise.

==Plot summary== New York, Italian mobsters. Just as the two goons are about to kill Julius, Maximillian, a suave, mysterious vampire (who arrived on the ship in his intricately carved coffin), intervenes and kills them. Soon after, Maximillian infects Julius with his vampiric blood, turning Julius into a decaying ghoul and claiming that it has benefits; he then explains that he has come to Brooklyn in search of the Dhampir daughter of a vampire from his native Caribbean island in order to live beyond the night of the next full moon.
 mentally ill mother (a paranormal researcher) some months before. As she and her partner, Detective Justice, are in the middle of investigating the murders on the ship, Rita begins having strange visions about a woman who looks like her, and begins asking questions about her mothers past. When she tells Justice about having "a strange feeling" about the investigation, he is skeptical, which frustrates Rita. The visions are presumably influenced in part by her vampire heritage; this is hinted at a few times throughout the first two-thirds of the story. Rita is completely unaware of this heritage, and believes she is losing her mind, similar to what happened to her mother.

Maximillian initiates a series of sinister methods to find out more about Rita and to further pull her into his thrall, including seducing and murdering her roommate Nikki, as well as disguising himself as her preacher and a lowlife crook. Max, in these disguises, misleads Rita into thinking Justice slept with Nikki, making her jealous and angry with him.

After saving Rita from being run down by a taxicab, Maximillian takes her to dinner. Rita is taken with Maximillians suave charm, and begins to fall in love with him. While dancing with her, he bites her.

Later the next day, Justice finds Rita in her apartment; Rita has been asleep all day with her apartment completely darkened. Justice informs Rita that Nikki has been found dead, and vows to help her understand her strange visions, as one of them had correctly foretold Nikkis murder. Rita tearfully forgives Justice, while berating herself for not listening to his side of the story, and is happy he is now beginning to understand her.

The two friends then embrace, and begin kissing passionately. Releasing these long-repressed emotions begins Ritas transformation into a vampire, and just as she is ready to bite the unsuspecting Justice in the neck, she sees her reflection disappearing in her bedroom mirror - a sure sign that she is transforming into one of the "undead". Horrified, she races to Maxs apartment to confront him about the changes occurring in her.

Max explains himself, and by doing so, Rita, who already blames his biting her neck for "turning" her, deduces that he is also responsible for all the murders she and Justice are investigating. Rita further finds out that Maximillian was sent to her by her father (a vampire, making Rita a dhamphir), whom she has long been curious about; his death at the hands of vampire hunters was what drove Ritas mother insane.

Max tries to convince a hysterical Rita that she will be happier as a vampire instead of remaining in the human world, where he feels she will remain out of place and misunderstood by society. Justice plans to rescue Rita from Max, and seeks help and advice from Dr. Zeko, a vampire expert they visited earlier in the murder investigation. Zeko explains that years ago, he knew Ritas mother while she was doing her research on the vampires of the Caribbean islands, and that she surrendered to evil by falling in love with Ritas vampire father. To avoid becoming a vampire, Rita must refrain from drinking the blood of an innocent human victim; also, Maximillian must die before the next full moon. Zeko gives Justice an ancient dagger with instructions to either kill Maximillian or risk being killed by Rita.

By the time Justice reaches her, Rita is lying inside Maxs coffin, almost completely changed into a vampire, and threatens to bite Justice. Justice and Maximillian engage in a battle, during which Justice loses Zekos dagger on the floor. Maximillian encourages Rita to finish Justice off and complete the transformation. Rita rejects life as a vampire, and drives Zekos dagger through Maximillians heart, causing him to disintegrate; as her vampire self is heartbroken over the death of Max, she changes back into a normal human. Rita and Justice then embrace with a passionate kiss.

Meanwhile, Julius, now completely decayed, enters his masters limousine. He happens upon Maximillians ring and puts it on, at which point he instantly transforms into a fully intact member of the undead. (It is implied that one of the benefits of his having been a ghoul is that he is now well-endowed). Overjoyed, he tells his uncle Silas, "Theres a new vampire in Brooklyn, and his name is Julius Jones!"

==Cast==
* Eddie Murphy as Maximillian / Preacher Pauley / Guido
* Angela Bassett as Detective Rita Veder
* Allen Payne as Detective Justice
* Kadeem Hardison as Julius Jones John Witherspoon as Silas Green
* Zakes Mokae as Dr. Zeko
* Joanna Cassidy as Captain Dewey
* W. Earl Brown as Police Officer
* Simbi Khali as Nikki

==Production==
African-American stunt performer Sonya Davis was fatally injured performing a   backward fall. 

==Rating== MPAA rated the film "R" for "strong language and vampire violence". In the UK, the movie was given a "15" certificate. The film was rated MA15+ in Australia for "medium level coarse language,   horror theme ".

==Reception== The Nutty Professor).   
Rotten Tomatoes gives the film a score of 10% based on reviews from 29 critics, the sites consensus is: "Neither scary nor very funny, this misguided effort never lives up to its premise." 

 , but without sufficient purpose, and my overall memory of it is people screaming in the shadows. To call this a comedy is a sign of optimism; to call it a comeback for Murphy is a sign of blind faith."

In the retrospective book Wes Craven: The Art of Horror, the author, John Kenneth Muir said "Given the fact that A Vampire in Brooklyn is an entry in an over-exposed horror genre (the vampire film) and an uneasy mix of humor and horror, it is amazing that it is successful at all.   The chemistry between Bassett and Murphy is strong, Kadeem Har ison and John Witherspoon are adept at comedy, the special effect sequences and transformations are startling, and the overall 1930s-40s mood is charming." He also praised J. Peter Robinsons musical score, calling it "delightful".  

In an interview with Rolling Stone, Murphy gave a reason Vampire in Brooklyn was a failure. "The only way I was able to do Nutty Professor and to get out of my Paramount deal, I had to do Vampire in Brooklyn. But you know what ruined that movie? The wig. I walked out in that longhaired wig and people said, Oh, get the fuck out of here! What the hell is this?" 

On numerous interview, Wes Craven says the movie was difficult to make because Murphy doesnt want to be funny and want to play his character totally straight. 

During an interview with A.V. Club, John Witherspoon says Vampire in Brooklyn was "one of my favorite movies. I had the chance to holler and scream." About Craven he says "Wes Craven, oh my God, he’s funny; he’s hilarious. But so, they let me ad-lib. But the worst thing about ad-libbing is that when you shoot it again, you don’t remember what you said. So he would take notes and tell me what I said. I said, “I said that?” So many lines that you say you forget that you say anything—you’re just ad-libbing, you’re not committing it to memory. So it was kind of difficult working with him, because he shot a lot of scenes, you know, instead of shooting one scene and get the genius of it all, he’d shoot it from different angles. So now I gotta think about what I said. He had a little pencil and he wrote it down, he came up to me said, “I want you to say that again, that was so funny.” That was kind of a difficult movie. But by the end of it, I just stuck with the script." 

==See also== Stunts that have gone wrong (List of film accidents)
* Vampire film

==References==
 

 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 