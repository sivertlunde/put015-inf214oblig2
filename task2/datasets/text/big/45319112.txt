Lottery (film)
 
 
{{Infobox film
| name           = Lottery
| image          = 
| alt            = 
| caption        = Movie poster
| film name      =  
| director       = Hemant Prabhu
| producer       = Govind Satnam
| writer         = Aadesh K. Arjun
| screenplay     = Hemant Prabhu
| story          = 
| based on       =  
| starring       = Abhijeet Sawant  Rucha Gujarathi
| narrator       =  
| music          = Santosh Singh & Sanjay Pathak  
| cinematography = Prakash Kutty 
| editing        = 
| studio         =  
| distributor    =  
| released       =  
| runtime        = 
| country        =  
| language       = Hindi
| budget         = 
| gross          =
}} Abhijeet and Rucha Gujarathi in title roles while some ensemble cast have supporting roles.The movie is produced by Govind Satnam, directed by Hemant Prabhu and story is written by Aadesh K. Arjun.            

==Plot==
The movie revolves around "love triangle". The male is attracted to two females and wins a lottery. To win again, he must consider killing one of the two females.

==Cast==
{| class="wikitable sortable"
|-
! # !! Actor !! Role

|-
 Abhijeet || Rohit Awasthi  
|-

| 02 || Rucha Gujarathi || Soha
|-

| 03 || Manisha Kelkar || Simran Kapoor
|-
|}

==See also==
 
* Cinema of India
* Bollywood
* Abhijeet Sawant
* Rucha Gujarathi
== References ==
 

 
 
 
 

 