Manly Times
{{Infobox film
| name = Мъжки времена Mazhki vremena ( )
| image = Manly Times.jpg
| caption = 
| imagesize = 
| director = Eduard Zahariev
| writer = Nikolay Haytov
| cinematography =  Radoslav Spasov
| starring = Grigor Vachkov Mariana Dimitrova Velko Kanev Pavel Popandov
| music = Kiril Donchev
| studio = Studio of Featured Films (SFF)|SFF, a Film Unite Hemus 
| released = 1977
| country = Bulgaria
| runtime = 125 minutes
| language = Bulgarian
}}

Manly Times (  / Mazhki vremena) is a Bulgarian drama film released in 1977 in cinema|1977, directed by Eduard Zahariev, starring Grigor Vachkov, Mariana Dimitrova, Velko Kanev and Pavel Popandov. The screenplay, written by Nikolay Haytov is based on the short stories Manly Times and Wedding from his book Wild Stories (1967).

In a colourful folklore manner, the movie presents an old tradition of stealing girls for brides that was a reputable occupation in the Rhodope Mountains region. The interaction between a brilliant literature and skilful directing as well as the memorable performances by Vachkov and Dimitrova received a broad critical acclaim and turned the film into one of the classics of the Bulgarian cinematography.   

==Cast==
*Grigor Vachkov as Banko (the stealing women expert)
*Mariana Dimitrova as Elitsa (the stolen girl)
*Velko Kanev as Velko (candidate husband) 
*Pavel Popandov as Dinko
*Nikola Todev as Kara Kolyo
*Teofil Badelov as Gelyo
*Trayan Yankov as Ilcho Georgi Dimitrov as Petko

==References==
 

===Sources===
*    
*    

==External links==
*  
*   at the Bulgarian National Television  

 
 
 
 
 
 
 

 
 