Sweet Savior
{{Infobox film
| name           = Sweet Savior
| image          = 
| alt            = 
| caption        = 
| director       = Bob Roberts
| producer       = Bob Roberts
| writer     =  Matt Cavanaugh based on = an original story by Bob Roberts
| narrator       = 
| starring       = Troy Donahue
| music          = Jerry Barry Gilbert Splacin
| cinematography = Victor Petroshevitz
| editing        = John Connoll
| studio         =
| distributor    = Trans World Attractions
| released       =1971
| runtime        = 92 mins
| country        = United States
| language       = English
| budget         = 
| gross = 
}}
Sweet Savior is a 1971 film starring Troy Donahue as a Charles Manson-type cult leader. 

It was also known as The Love Thrill Murders.
==Production==
Lloyd Kaufman was production manager.

Troy Donahue said while promoting the film:
 I play Moon, a religious creep who murders a lot of people, a real heavy trip. But I dont want anyone to think Im playing it in some phony exploitation flick that takes advantage of the Manson case to make a fast buck. I dont like many things, man, but I dig this picture... Were trying to show both sides of the problem. The Hollywood glamor society is as guilty as the depraved hippy cults. They pick up people on the Sunset Boulevard and tease them. When they made fun of Manson they picked on the wrong guy. I was up at the Tate house. It was a freaky scene. Sure I met Manson at the beach playing volleyball. Troy Donahue--from Beachboy to Jesus Freak
Reed, Rex. Chicago Tribune (1963-Current file)   08 Aug 1971: e2.   
Director Bob Roberts said:
 I had the idea not to make the Mason story per se but to inform people the Sharon Tate thing was not just an isolated incident. Many other cults are murdering people. Theyre just not as publicised. There are a lot of so-called families like Masons with one dictatorial leader who controls his group through drugs, pills, sex, LSD and many other ways. These people are a threat to the fabric of society because they commit murder without conscience.   
==References==
 
==External links==
* 
*  at TrashFilm Guru

 
 