Buck Privates Come Home
{{Infobox film
| name           = Buck Privates Come Home
| image          = buckprivatescomehome.jpg
| caption        = Theatrical release poster Charles Barton Robert Arthur John Grant Frederic I. Rinaldo Robert Lees Tom Brown Nat Pendleton Beverly Simmons
| music          = Walter Schumann
| cinematography =
| editing        = Edward Curtiss
| distributor    = Universal-International
| released       = April 4, 1947
| runtime        = 77 min.
| language       = English
| budget         = $1,167,500 
| gross = $2,365,000 
| country        = United States
}}
Buck Privates Come Home is a 1947 film starring the comedy team of Abbott and Costello.  It is a sequel to their 1941 hit, Buck Privates.
 Russ Conway (in the role of an unnamed medic).

==Plot== Joan Fulton) who delivers her to immigration officials in New York.  However, during a shift change at the office, Evey is mistaken for a neighborhood kid and set free.  Meanwhile, Herbie and Slicker are back to their pre-war occupation of peddling ties in Times Square. Collins is back at his old job as well, as a police officer assigned to the same beat. He is about to arrest the boys when Evey shows up and helps them escape.
 Tom Brown).

At one point, Herbie and Slicker purchase what seems to be an ideal home for $750, but the seller doesnt want to let them see the interior prior to purchase. Before Herbie can get the front door open, the seller gives a signal, and a truck hauls off the façade, revealing that the boys had just purchased a broken down old bus. The two have to fix it up to use as a home.

Bill is a midget car racer. He is sure he will win the $20,000 prize at the Gold Cup Stakes, but his car is being held at a local garage until past due bills are paid. Herbie and Slicker use their separation pay and loans from their old service pals to get the car out of hock. Collins, however, has other plans. He had been demoted repeatedly to ever less desirable beats thanks to the boys escaping from him.  He stakes out the garage in hopes of catching them and returning Evey to the immigration authorities to get himself back in good favor with his boss.  He eventually chases them to the track, where Herbie gets in Bills race car and leads everyone on a wild chase through the streets of New York.

Herbie is eventually caught, but not before the head of an automobile company is impressed enough to order twenty cars and 200 engines.  With his financial future secure, Bill can now marry Sylvia and adopt Evey. Slicker and Herbie will be allowed to visit Evey if they get jobs. Collins captain suggests that they join the police force, which they do...with Collins as their instructor!

==Production==
It was filmed from November 18, 1946 through January 23, 1947. It was originally budgeted at $1,167,500 but came in $34,500 over budget making it the most expensive Abbott and Costello vehicle produced at Universal. The leads were paid $196,133. 

Arthur T. Horman, the writer for the original movie, Buck Privates wrote the first treatment for this sequel, titled The Return of the Buck Privates, but it was not used. Furmanek, Bob and Ron Palumbo (1991). Abbott and Costello in Hollywood. New York: Perigee Books. ISBN 0-399-51605-0 

There is a joke used in the film that is often attributed to Benjamin Franklin.    Herbie says, "Id rather marry a homely girl than a pretty girl anyway," to which Slicker replies, "Why?"  Herbie responds, "Well, if you marry a pretty girl, she is liable to run away."  Evey chimes in with, "But Uncle Herbie, isnt a homely girl liable to run away too?"  Herbies response is simple, "Yeah, but who cares?"

When Costello drives the midget car through the rear of a movie theater, there is a poster that shows a fictional film, Abbott and Costello in Romeo Junior  on the wall.  Scenes of Abbott and Costello in Romeo and Juliet outfits, with Betty Alexander as Juliet, were filmed and were intended to be playing on the screen of the theater, but the scene was deleted. 

==Routines==
*The Sawhorse Table takes place on the ship.  Costello creates a table by putting a board on a sawhorse.  Every time one of them puts something down on one end of the table, the other one puts something on the other end to balance it.  Neither one of them are aware of what is happening, that is until the table finally loses its balance and a cake flies off the table into Pendeltons face.

==DVD releases==
This film has been released three times on DVD.  Originally released as single DVD on April 8, 1998, it was released twice as part of two different Abbott and Costello collections.  The first time, on The Best of Abbott and Costello Volume Two, on May 4, 2004, and again on October 28, 2008 as part of Abbott and Costello: The Complete Universal Pictures Collection.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 