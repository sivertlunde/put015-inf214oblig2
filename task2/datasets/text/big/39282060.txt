And God Smiled at Me
{{Infobox film
| name      = And God Smiled at Me
| image_size   =
| image	= File:And God Smiled at Me.jpg
| alt      = 
| caption    = Promotional movie poster
| director    = Danny Holmsen Mar S. Torres
| producer    = Vera- Perez Family
| writer     = 	
| screenplay   = 	
| story     = 
| based on    =
| starring    = Nora Aunor Tirso Cruz III Luis Gonzales Lucita Soriano
| music     = Danny Holmsen	 
| cinematography = Felipe Santiago	 
| editing    = 	 	
 | studio     = 
| distributor  = Sampaguita Pictures VP Pictures
| released    = October 14, 1972 
| runtime    =
| country    = The Philippines Filipino
| budget     = 
| gross     = 
}}

And God Smiled at Me is a 1972 family drama film produced by Sampaguita Pictures and VP pictures.  It is the official entry to the 3rd Quezon City Film Festival. The movie gave Aunor her first best actress award and marks her as a dramatic actress.  The movie was also the top grosser of the film festival for that particular year.

==Cast==
*Nora Aunor ... Celina
*Tirso Cruz III ... Carding
*Luis Gonzales... Damian
*Lucita Soriano ... Olga
*Naty Santiago ... Celinas Mother
*Nenita Jana ... Cardings Mother / Laundry Woman

==Trivia==
This was Aunors first film that gave her, her first ever best actress trophy.

==Awards and recognition==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Group
! width="25%"| Category
! width="25%"| Nominee
! width="10%"| Result
|-
| rowspan="8" align="center"| 1972
| rowspan="8" align="left"| 3rd Quezon City Film Festival
| align="left"| Manuel L. Quenzon Award for Best Picture
| align="center"| 
|  
|-
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Best Actor
| align="center"| Tirso Cruz III
|  
|-
| align="left"| Best Supporting Actress
| align="center"| Lucita Soriano
|  
|-
| align="left"| Best Cinematography
| align="center"| Felipe Santiago
|  
|-
| align="left"| Best Sound
| align="center"| Flaviano Villareal
|  
|-
| align="left"| Best Musical Score
| align="center"| Danny Holmsen
|  
|}

==References==
 

== External links ==
*  

 
 
 
 
 