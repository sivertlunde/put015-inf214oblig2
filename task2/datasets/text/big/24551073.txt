Funny About Love
 
{{Infobox film
| name           = Funny About Love
| image          = Funny About Love.jpg
| alt            = Gene Wilder has a confused look on his face as an infant sits on his head holding a clock
| caption        = Theatrical release poster
| director       = Leonard Nimoy
| producer       = Jon Avnet Jordan Kerner
| writer         = Norman Steinberg David Frankel
| based on       =   by Bob Greene
| starring       = Gene Wilder Christine Lahti Mary Stuart Masterson
| music          = Miles Goodman Fred Murphy
| editing        = Peter E. Berger
| studio         = Duffy Films
| distributor    = Paramount Pictures
| released       =  
| runtime        = 101 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $8,141,292 
}}
Funny About Love is a 1990 American romantic comedy film directed by Leonard Nimoy and starring Gene Wilder. With a screenplay by Norman Steinberg and David Frankel, the film is based on the article "Convention of the Love Goddesses" in Esquire (magazine)|Esquire Magazine by Bob Greene. 

==Plot==
New York cartoonist Duffy Bergman marries gourmet chef Meg Lloyd. Meg wants to have a baby. Duffy agrees, but after unsuccessful attempts, Duffy encourages her to focus on her career and come back to the child issue later. After his mothers death, however, Duffy becomes fixated on wanting to have a child. Meg no longer sees this as a priority, as shes trying to open her own restaurant. The two start to have marital problems, leading to a separation. 

Duffy travels to Arizona to speak at a Delta Gamma sorority convention. He explains that the Delta Gammas have always been his dream girls—his Love Goddesses. There he meets the much younger Daphne Delillo, and when she moves to New York to work as a network sports reporter, their attraction develops into a relationship. Daphne becomes pregnant. Duffy is happy to father a child, but uncomfortable with how fast this relationship is progressing. When she has a miscarriage, Daphne breaks up with him, believing that they were really staying in the relationship for the baby.

At his fathers wedding, Duffy hears news about Meg and decides to go to her restaurant. He tries to reconcile with her, insisting that he doesnt care if they remain childless as long as he can be with her. Duffy discovers that Meg has adopted a baby boy.

==Cast==
* Gene Wilder as Duffy Bergman
* Christine Lahti as Meg Lloyd Bergman
* Mary Stuart Masterson as Daphne
* Robert Prosky as E.T.
* Stephen Tobolowsky as Hugo
* Wendie Malick as Nancy
* Anne Jackson as Adele
* Susan Ruttan as Claire
* Freda Foh Shen as Nurse
* Regis Philbin as himself
* Patrick Ewing as himself

==Release==
===Box office===
Funny About Love opened in 1,213 theaters on September 21, 1990 and grossed $3,036,352 in its opening weekend, landing at #5, behind Goodfellas, Postcards from the Edge s second weekend, Ghost (1990 film)|Ghost s eleventh, and Narrow Margin.  The film would eventually gross $8,141,292 in the domestic box office.   

===Critical reception===
The film received extremely negative reviews; based on 7 reviews, the film has a 0% rating on review aggregator website Rotten Tomatoes. 

Janet Maslin of the New York Times found the film average.
 

Roger Ebert of the Chicago Sun-Times had nothing but disdain for the film, giving it only a half of a star rating out of the four stars scale he uses.
 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 