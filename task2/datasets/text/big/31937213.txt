Auld Lang Syne (1929 film)
{{Infobox film
| name           = Auld Lang Syne
| image          = 
| image_size     = 
| caption        =  George Pearson George Pearson George Pearson
| starring       = Harry Lauder Dorothy Boyd Patrick Aherne   Dodo Watts
| music          = Robert Burns (songs)
| cinematography = Bernard Knowles
| editing        = 
| studio         = Welsh-Pearson 
| distributor    = Paramount British Pictures
| released       = April 1929
| runtime        = 75 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} George Pearson and starring Harry Lauder, Dorothy Boyd, and Patrick Aherne. It was originally made as a silent film, but in September 1929 sound was added.   It was shot at Cricklewood Studios in Cricklewood, London.

==Cast==
* Harry Lauder - Sandy McTavish
* Dorothy Boyd - Jill Bray
* Patrick Aherne - Angus McTavish
* Dodo Watts - Marie McTavish
* Hugh E. Wright - Wullie McNab

==References==
 

==Bibliography==
* John Mundy, The British Musical Film (Manchester University Press, 2007)
* Paul Matthew St. Pierre, Music Hall Mimesis in British Film, 1895-1960: On the Halls on the Screen (Associated University Press, 2010)

==External links==
* 

 

 
 
 
 
 
 
 


 
 