Wanted (2011 film)
{{Infobox film
| name = Wanted
| image = Wanted 2011.jpg
| caption = 
| director = B.V.S.Ravi
| producer = Ananda Prasad
| writer =  Gopichand Deeksha Seth Chakri
| cinematography = Rasool Ellore
| editing = 
| studio = 
| distributor = Bhavya Creations
| released =  
| runtime = 
| country = India
| language = Telugu
| budget =
| gross =
}} action film Gopichand and Deeksha Seth in the lead. Rasool Ellore has handled the camera while music was composed by Chakri (music director)|Chakri. The film released on 26 January 2011. The film was dubbed in Hindi as Jaanbaaz Ki Jung and in Tamil as Vengai Puli. 

==Plot==
Rambabu (Tottempudi Gopichand|Gopichand) is a happy go lucky chap who does nothing for career, and doesnt mind a fight for the heck of it. In spite of this, his parents love him completely because they think they have earned enough for him. Rambabu comes across Nandini (Deeksha Seth), a house surgeon. When the girl saves his moms (Jayasudha) life, Rambabu falls for her and chases her. After almost following her everywhere, saving her from goons few times, Rambabu gets frustrated and asks her what she wants him to do to prove his love for her. The girl asks him to kill Basava Raju (Prakash Raj) and company, as a revenge for killing her honest cop father (Nassar) and her entire family. Rambabu goes after the goons and kills them one by one and finally unites with Nandini.

==Cast== Gopichand
* Deeksha Seth
* Jayasudha
* Prakash Raj Chandramohan
* Brahmanandam
* Nassar Ali
* Raghu Babu
* Subbaraju
* Ahuti Prasad

==Soundtrack==
{{Infobox album
| Name = Wanted
| Longtype = to Wanted
| Type = Soundtrack
| Artist = Chakri (music director)
| Cover = 
| Released = 
| Recorded = 2010-2011 Feature film soundtrack
| Length =  Telugu
| Label = Aditya Music
| Producer = Chakri (music director)
| Reviews =
| Last album = Baava (2010)
| This album = Wanted (2011)
| Next album = Jai Bolo Telangana (2011)
}}
 Chakri has composed the original score and soundtracks for the film.. Bhaskarabhatla has penned the lyrics for the songs. 

{{Track listing
| extra_column = Performer(s)
| title1 = Arakilo Pogaru | extra1 = Ranjith (singer)|Ranjith|04.46
| title2 = Yevo Pichhi Veshalu | extra2 = Javed Ali|05.53
| title3 = Dil Mera Dhak Dhak | extra3 = Udit Narayan, Smita|04:47
| title4 = Cheppana Cheppana | extra4 = Chakri (music director)|Chakri, Kousalya|05:36
| title5 = A for Angel | extra5 = Krishna Chaithanya, M M Sreelekha |03:47 
}}

==References==
 

 
 
 