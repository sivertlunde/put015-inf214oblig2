The Saint Takes Over
{{Infobox Film name            = The Saint Takes Over caption         =   image	         = The Saint Takes Over FilmPoster.jpeg director        = Jack Hively producer        = Howard Benedict writer  Frank Fenton  based on characters created by Leslie Charteris starring        = George Sanders Wendy Barrie Jonathan Hale music           =   cinematography  = Frank Redman editing         = Desmond Marquette distributor     = RKO Radio Pictures released        =    (Premiere)  runtime         = 69 min. language        = English budget          = 
}}
 film series about Simon Templar, a.k.a. "The Saint", the Robin Hood-inspired crimefighter created by Leslie Charteris. George Sanders returned as Templar, with Wendy Barrie playing his latest romantic interest, in her second of three appearances in the Saint film series (playing a different role each time).

==Plot==
On an ocean liner making its way to New York, Simon Templar (George Sanders), "the Saint", rescues a fellow passenger (Wendy Barrie) from card cheats, though she refuses to give him her name and is offended when he kisses her without invitation. He later sends the mysterious woman a rose corsage by way of apology. 
 Paul Guilfoyle) and the murder of the main prosecution witness, Johnny Summers.

Egan orders two henchmen to pick up the woman passenger when the ship arrives. Fortunately, Templar is able to foil them, and the woman drives off in a taxi. Templar goes to see Fernack.

Weldon sends Gates to rob Egan, but Egan catches the safe cracker. At gunpoint, Gates confesses that Welden sent him. Egan orders him to lure his boss into a trap, but after Gates leaves, an unseen shooter kills Egan. Templar and Fernack meet when they both sneak into Egans place. The Saint finds the hidden camera and later develops the photograph. He also picks up a clue, a rose petal.

Welden assumes Gates killed Egan and has the $90,000, despite Gates protestations. Templar blackmails Gates into helping him in exchange for not giving the police the photograph and telling Weldon that Gates does not have the money. However, when they go to see Weldon, they find him dead, and once again, Fernack is already there.

Templar, assisted by Gates, kidnaps Sloan, the most likely of the survivors to talk, but they are followed. When Templar leaves Sloan guarded by Fernack, Sloan is shot and killed through Fernacks basement window. They take the body back to Sloans place, but the suspicious police burst in and take them by surprise. Only Templar manages to escape.

He waits for the murderer in Reeses apartment (Bremer being out of town). He is unsurprised when the woman shows up. She is Ruth Summers, Johnnys sister out for revenge. He offers to help her.

Templar has Gates "betray" him to Bremer and Reese. They catch him searching Bremers office. He offers to trade the $90,000 for his life, but insists they tell him everything. Their unwitting confessions are broadcast to the police via a hidden microphone and radio transmitter. When the police arrive, Bremer escapes by the fire escape. In the alley, he encounters Ruth. Each fatally shoots the other. Ruth makes a deathbed confession to the three previous murders, then dies before Templar can tell her something important.

==Cast==
* George Sanders as Simon Templar / The Saint
* Wendy Barrie as Ruth Summers
* Jonathan Hale as Inspector Henry Fernack Paul Guilfoyle as Clarence "Pearly" Gates
* Morgan Conway as Sam Reese
* Robert Emmett Keane as Leo Sloan
* Cy Kendall as Max Bremer (as Cyrus W. Kendall) James Burke as Patrolman Mike
* Robert Middlemass as Captain Wade
* Roland Drew as Albert "Rocky" Weldon
* Nella Walker as Mrs. Lucy Fernack, Henrys wife
* Pierre Watkin as "Big" Ben Egan

== References ==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 