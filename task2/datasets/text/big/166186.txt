Victim (1961 film)
 
 
{{Infobox film name = Victim image      = Victim 1961 poster.jpg caption    = Original 1961 British quad format cinema poster director   = Basil Dearden writer     = Janet Green, John McCormick producer   = Michael Relph starring   = Dirk Bogarde, Dennis Price, Sylvia Syms music  Philip Green cinematography = Otto Heller editing    = John D. Guthridge studio     = Allied Film Makers
|distributor= Rank Film Distributors budget     = £153,756 Alexander Walker, Hollywood, England, Stein and Day, 1974 p157  released   =   runtime    = 96 minutes country    = United Kingdom language   = English
|}} Odeon Cinema in Leicester Square on 31 August 1961.  On its release in the United Kingdom it proved highly controversial and was initially banned in the United States. 

== Plot ==
A successful barrister, Melville Farr (Dirk Bogarde) has a thriving London practice. He is on course to become a Queens Counsel and people are already talking of his being appointed a judge. He is apparently happily married to his wife, Laura (Sylvia Syms).

Farr is approached by "Boy" Barrett (Peter McEnery), a younger working class man with whom Farr shared a romantic but asexual relationship. Farr rebuffs the approach, thinking Barrett wants to blackmail him about their relationship. What Farr does not know is that Barrett himself has fallen prey to blackmailers who know of their relationship. The blackmailers have a picture of Farr and Barrett in a vehicle together, in which Barrett is crying. Barrett has been trying to reach Farr to appeal for help, because Barrett has stolen £2,000 from his employers to pay the blackmail and the police suspect him. Farr intentionally avoids him. Barrett is soon picked up by the police, who discover why he was being blackmailed. Knowing it will be only a matter of time before he is forced to reveal Farrs identity as the other man, Barrett hangs himself in a police cell.

Learning the truth about Barrett, Farr takes on the blackmail ring and recruits a friend of Barretts to investigate for him. The friend identifies a gay hairdresser who has also been victimised by the ring, but the hairdresser refuses to identify his tormentors. When the hairdresser is visited by one of the blackmailers, he suffers a heart attack. Prior to his death, he phones Farrs house and leaves a mumbled message about another victim of the ring.

Farr contacts this victim, a famous actor, but the actor refuses to help him, preferring to pay the blackmailers to keep his secret. Laura finds out about Barretts death and confronts her husband, demanding he tell her the truth. After a heated argument, Farr admits that before their marriage he had had a relationship with another man; the man subsequently killed himself when the relationship ended. He had told Laura about this before they married and promised that he no longer had such urges. Laura, seeing Farrs relationship with Barrett as a betrayal, decides to leave him.

The blackmailers vandalise Farrs property, painting "FARR IS QUEER" on his garage doors. Farr resolves to help the police catch them and promises to give evidence in court, despite knowing that the ensuing press coverage will certainly destroy his career. The blackmailers are identified and arrested. Farr tells Laura to leave before the ugliness of the trial, but that he will welcome her return afterward. She tells him that she believes she has found the strength to return to him. Farr burns the picture that incriminated him.

== Cast ==
 
* Dirk Bogarde as Melville Farr
* Sylvia Syms as Laura Farr
* Dennis Price as Calloway Nigel Stock as Phip
* Peter McEnery as Boy Barrett
* Donald Churchill as Eddy Anthony Nicholls as Lord Fullbrook
* Hilton Edwards as P.H.
* Norman Bird as Harold Doe
* Derren Nesbitt as Sandy Youth
* Alan MacNaughtan as Scott Hankin
* Noel Howlett as Patterson
* Charles Lloyd-Pack as Henry
* John Barrie as Detective Inspector Harris
* John Cairney as Bridie
* David Evans as Mickey
* Peter Copley as Paul Mandrake
* Frank Pettitt as Barman
* Mavis Villiers as Madge
* Margaret Diamond as Miss Benham
* Alan Howard as Frank
* Dawn Beret as Sylvie John Bennett as Undercover Detective (uncredited) John Boxer as Policeman in Cell (uncredited) Frank Thornton as George, Henrys assistant (uncredited)
 

==Background and production==
  Sexual Offences Act, which implemented the recommendations of the Wolfenden report, homosexual acts between males were illegal in England and Wales. There were prosecutions and Sunday newspapers gave space to the court reports. Yet, by 1960, the police were as relaxed as possible over the old laws. There was a feeling that the code violated decent liberty. But police restraint did not deter the menace of blackmail.

Scriptwriter Janet Green had already previously collaborated with Basil Dearden on a previous British "social problem" film, Sapphire (film)|Sapphire, which had dealt with racism against Afro-Caribbean immigrants to the United Kingdom in the late 1950s. After reading the Wolfenden report and aware of the context of several high-profile prosecutions against gay men, she became a keen supporter of homosexual law reform.
 Ill Met Doctor film A Tale of Two Cities. He was flirting with a larger, Hollywood career—playing Liszt in Song Without End. Bogarde was suspected to be homosexual, living in the same house as his business manager, Anthony Forwood, and was compelled every now and then to be seen in public with attractive young women. He seems not to have hesitated over the role of Farr. Similarly, Sylvia Syms never flinched from the part of his wife, though apparently several actresses had turned it down.

Other gay cast members included Dennis Price and Hilton Edwards. Though it mostly treats homosexuality in a non-sensationalised manner, there is one rather catty aspect to the film—Prices character (a prominent gay theatre star) would have been fairly easy for contemporary audiences to identify with Noël Coward.

The script was originally entitled Boy Barrett, changing to Victim late in production. A number of controversial scenes were cut during discussions with the BBFC, including scenes with teenagers. 

== Reaction == sociologically significant film; many believe it played an influential role in liberalising attitudes (as well as the laws in Britain) regarding homosexuality.    It was not a major hit but by 1971 had earned an estimated profit of £51,762. 
 British Board of Film Censors originally gave the film an X rating. In a letter to the filmmakers, the BBFC secretary raised four objections to the film. First, a male character says of another man, "I wanted him". Second, references to "self-control" in the revised script were left out of a filmed discussion of homosexuality, leaving the discussion "without sufficient counterbalance". Third, the film implies that homosexuality is a choice, which "is a dangerous idea to put into the minds of adolescents who see the film". Finally, when the blackmailer Brenda unleashes a tirade against homosexuality, her popular view will be discredited since she is such an unsympathetic character. 

== Hollywood Production Code == 

In the United States, the use of the term "homosexual", and its opposition to anti-gay criminal laws, kept the film from receiving approval by the Hollywood Production Code.  A few years prior to Victim, the filmmakers of Suddenly, Last Summer (film)|Suddenly, Last Summer (1959) had persuaded the code censors to allow their film to use homosexuality as a plot device, but only if presented through cryptic innuendos, and film had to illustrate the "horrors of such a lifestyle". 

The film Victim, in contrast, was deemed to be too frank and liberal in its treatment of homosexuality, and, thus, was initially not given approval by the censorship code.   However, in 1962, the Hollywood Production Code had agreed to lift the ban on films using homosexuality as a plot device.   A few years later the code itself would be replaced by the Motion Picture Association of America, which introduced age-appropriate classification for films.

Initially, Victim was generally classified as an "adult" film, often with the X classification that was initially given to pornographic films.  However, as anti-gay prejudices declined, the rating classifications for the film were also revised. 

When Victim was released on VHS in America (1986), it was given the PG-13 rating.  Likewise, when Victim was re-released in the United Kingdom it was reclassified with the much milder PG/12 rating. 

== The use of the term "queer" == 
 red herring fraudsters who write begging letters.

==Home media==
The film was released by the Criterion Collection in January 2011.  However, the release did not receive a spine number; it was instead released as part of an "Eclipse" box set.

== See also ==
 
* Different from the Others (Anders als die Andern)—a 1919 German film seen as a precursor of Victim.
* The Childrens Hour (film)|The Childrens Hour—a similar film, starring Audrey Hepburn and Shirley MacLaine and also released in 1961, that was controversial for its storyline dealing with lesbianism.
* List of lesbian, gay, bisexual or transgender-related films

==References==
 

==Further reading==
* John Coldstream: Victim: BFI Film Classics: British Film Institute/Palgrave-Macmillan: 2011: ISBN 978-1-84457-427-8.
* Richard Dyer: "Victim: Hegemonic Project" in Richard Dyer: The Matter of Images: Essays on Representation: London: Routledge: 2002.
* "William Drummond" (pseud. Arthur Calder-Marshall): Victim (1961), Corgi: film novelisation
* Patrick Higgins: Heterosexual Dictatorship: Male Homosexuality in Postwar Britain: London: Fourth Estate: 1996: ISBN 1857023552
* Philip Kemp: "I Wanted Him: Revival: Victim" Sight and Sound 15:8 (August 2005): 10.
* Andrew Watson:   History Today: 65.20 (September/October 2011): 15–17.

==External links==
*  
*  
*  
*  

 

 
   
 
 
 
 
 
 
 
 
 