The Murder in the Museum
{{Infobox film
| name           = The Murder in the Museum
| image          = The Murder in the Museum (1934) - Title.jpg
| image_size     = 190px
| caption        = title sequence screenshot
| director       = Melville Shyer
| producer       = Willis Kent (producer)
| writer         = F.B. Crosswhite (writer)
| narrator       =
| starring       = John Harron
| music          = James Diamond
| editing        = S. Roy Luby
| studio         = Willis Kent Productions
| distributor    = Marcy Films
| released       = 27 May 1934
| runtime        = 65 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Murder in the Museum is a 1934 American film directed by Melville Shyer. The film is also known as The Five Deadly Vices (American reissue title).

== Plot summary ==
A city councilman is murdered while investigating allegations of drug dealing going on at a disreputable sideshow. The daughter of the chief suspect then teams up with a newspaper reporter to find the real killer.

== Cast ==
*Henry B. Walthall as Bernard Latham Wayne, alias Prof. Mysto
*John Harron as Jerry Ross
*Phyllis Barrington as Lois Brandon Tom OBrien as Alfred Carr
*Joseph W. Girard as Police Commissioner Brandon
*Symona Boniface as Katura the Seeress Donald Kerr as Museum Tour Guide
*Sam Flint as Councilman Blair Newgate John Elliott as Detective Chief Snell
*Steve Clemente as Pedro Darro
 
== Reuse of film ==
Footage of a belly dancer shot for the film was reused in the 1943 film Confessions of a Vice Baron.

== External links ==
 
* 
* 

 
 
 
 
 
 


 