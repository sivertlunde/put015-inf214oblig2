Walkout (film)
{{Infobox film
| name =Walkout
| image =Walkout film.jpg
| caption = Theatrical poster
| director = Edward James Olmos
| producer = Moctesuma Esparza Robert Katz
| screenplay = Marcus DeLeon Ernie Contreras Timothy J. Sexton
| story = Victor Villaseñor
| starring = Alexa Vega Efren Ramirez Michael Peña Tonantzin Esparza
| cinematography = Donald M. Morgan
| music = Rosino Serrano
| editing = Michael McCusker
| distributor =
| released =  
| runtime = 120 minutes
| country = United States
| language = English Spanish
| budget =
}} 2006 Home HBO film based on a true story of the 1968 East L.A. walkouts. It premiered March 18, 2006 on HBO. Starring Alexa Vega, Efren Ramirez and Michael Peña, the film was directed by Edward James Olmos. Moctezuma Esparza, one of the students who was involved in the walkouts, was the films executive producer and many of the actors playing parents in the film were also protesters.
 House bill would have made it a felony (rather than a misdemeanor) to be in the US illegally.  The bill was the catalyst for the 2006 U.S. immigration reform protests. Other student Walk Out protests in May 2006 were in part inspired by the film.  

==Cast==
*Alexa Vega as Paula Crisostomo
*Michael Peña as Sal Castro 
*Yancey Arias as Panfilo Crisostomo 
*Laura Harring as Francis Crisostomo  
*Efren Ramirez as Bobby Verdugo 
*Jeremy Ray Valdez as Robert Avila
*David Warshofsky as Lloyd Hurley  
*Tonantzin Esparza as Vickie Castro
*Holmes Osborne as Principal Ingles 
*Tim DeKay as Mr. Peck 
*Jonathan Hernandez as Fernie Flores
*Fidel Gomez as Carlos Montes
*Veronica Diaz-Carranza  as Yoli Rios 
*Germaine De Leon as Harry Gamboa
*Edward James Olmos as Julian Nava
*Jerry Hernandez as Ray Chapa

==Synopsis== East Los Angeles and they decide to try to change the way students are treated. They are punished for speaking Spanish in school, their bathrooms are locked during lunch, they are forced to do janitorial work as a punishment and many in the high school administrations actively dissuade the less promising students from attending college. Inspired by her Chicano teacher Sal Castro (Michael Peña) and despite opposition from her father (Yancey Arias), Paula joins in and helps hand out surveys to students to suggest improvements to the schools. Each East LA high school has two or three students who are in the group; Paula particularly becomes interested in Robert (Jeremy Ray Valdez). 

However, the school board refuses to consider the suggestions so Paula urges the students to walk out of school. The police find out and the principal threatens to expel Paula if she walks out. Paulas father urges against her plan of "walking out." He believes that the group is a bunch of "agitators." 5 East LA schools successfully walk out and the school board says they might consider their demands, but Paulas father throws her out of the house for her role in the walkout. The students decide to walk out in only half of the schools the next day, but the police arrest and beat the protesters. None of the footage appears on the news and the students are painted as violent agitators with Communist ties. Paula decides to invite the students families to the protests, hoping their presence will deter police brutality. When the students walk out again their families come to support them and it appears that they have won when the school board agrees to hear their complaints. Paula invites Robert to prom, but while she is getting ready the police suddenly arrest 12 of the leaders of the student movement, When Paula goes to Sal for advice she discovers that Robert (who is an undercover LAPD officer) has arrested him. The students are charged with conspiracy to disrupt a school with a maximum penalty of 66 years. Paula is defeated, but her father urges her not to give up and she helps to stage a massive protest outside the jail. Robert is on duty there and tries to stop her, but she continues leading the crowd until all 12 students and Sal are released.

==References==
 

== External links ==
*  


 
 
 
 
 