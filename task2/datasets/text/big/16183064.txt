The Foot Fist Way
{{Infobox film
| name           = The Foot Fist Way
| image          = Foot fist way.jpg
| caption        = Promotional film poster
| alt            = 
| director       = Jody Hill
| producer       = Erin Gates Jody Hill Robbie Hill Jennifer Chikes Ben Best
| starring       = Danny McBride Ben Best Mary Jane Bostic
| music          = Pyramid The Dynamite Brothers
| cinematography = Brian Mandle
| editing        = Zene Baker Jeff Seibenick Will Ferrell and Adam McKay You Know I Cant Kiss You, Inc.
| distributor    = Paramount Vantage (USA) Alliance Films (Canada) Momentum Pictures (UK)
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $79,000   
| gross          = $245,292
}} The Los Sundance that same year. The film was released on DVD in 2008.

==Plot== black belt Ferrari and expo to Ben Best) board breaking, which he wins. At the following belt ceremony, Fred reads a new student pledge that he has written, which outlines the goals and responsibilities of Taekwondo.

==Cast==
* Danny McBride as Fred Simmons, a black belt and instructor of Taekwondo in a small southern town. Ben Best as Chuck "The Truck" Wallace, a B movie action star. Best is also a member of the music band Pyramid, who supplied songs to the films soundtrack. Other members of the band played bit parts as Chucks friends at his party.
* Mary Jane Bostic as Suzie Simmons, Freds unfaithful, spandex-clad wife.
* Spencer Moreno as Julio, Freds overweight adolescent second-in-command at the dojang.
* Carlos Lopez IV as Henry, a meek youth who finds self-confidence through Taekwondo.
* Jody Hill as Mike McAlister, Freds intense friend and fellow Taekwondo black belt.

All martial artists in the film, excluding McBride and Best, were played by true Taekwondo practitioners, many from   in Concord NC.

==Promotion==
McBride appeared as a guest on Late Night with Conan OBrien on February 26, 2008 as the character Fred Simmons.    Many viewers were not familiar with either the character or the actor and as a result there was initially much speculation as to whether the seemingly disastrous Tae Kwon Do demonstration, during which Simmons asked for a "redo" after a failed block-splitting attempt - and awkward interview, during which he repeatedly lashed out at fellow guest Will Ferrell for dancing around in a sexual nature during his interview segment earlier, were real or staged. Among the only immediate clues to suggest the interview was a setup was when the website for the film was flashed onscreen during the interview.

==Critical reception==
Variety gave the film a fairly positive review stating that the film is "crying out to be discovered by midnight movie mavens".  It currently holds a 57%, "Rotten" rating on review aggregator website Rotten Tomatoes, based on 76 reviews.  The website Metacritic, which averages leading film critics from across the United States, gave the film a 63 out of 100 based on 22 reviews, which is considered "generally favorable." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 