Come On George!
{{Infobox film
| name = Come On George!
| image = "Come_On_George!"_(1939).jpg
| image_size =
| caption = Danish poster
| director = Anthony Kimmins
| producer = Jack Kitchin  
| writer = Leslie Arliss   Val Valentine   Anthony Kimmins 
| narrator =
| starring = George Formby   Patricia Kirkwood   Joss Ambler   Meriel Forbes
| music = Ernest Irving 
| cinematography = Ronald Neame
| editing = Ray Pitt
| studio =  Associated Talking Pictures  Associated British 
| released = November 1939
| runtime = 88 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 
 produced by Hal Erickson wrote in Allmovie, "Come on George!" was a product of George Formbys peak movie years."  It concerns the world of horse racing, and Formby, who had once been a stable apprentice, did his own riding in the film.  Songs featured are
"Im Making Headway Now", "I Couldnt Let The Stable Down", "Pardon Me", and "Goodnight Little Fellow, Goodnight."   

==Plot==
This farce stars comedian George Formby, who plays George, a stable boy. He also has the unique ability to soothe an anxious racing horse. Expectedly, George races the horse and wins. 

==Cast==
* George Formby as George
* Patricia Kirkwood as Ann Johnson
* Joss Ambler as Sir Charles Bailey - the owner of the racehorse Maneater
* Meriel Forbes as Monica Bailey
* Cyril Raymond as Jimmy Taylor
*George Hayes as Bannerman
* George Carney as Sergeant Johnson
* Ronald Shiner as Nat
* Gibb McLaughlin as Dr MacGregor
* Hal Gordon as Stableboy
* Leo Franklyn as Bannermans Trainer

A young Dirk Bogarde makes an uncredited appearance as an extra. 

==Critical reception==
* TV Guide wrote, "Formby was a major comedic star in the late thirties, but this is one of his lesser efforts." 
* Halliwells Film Guide noted a "standard comedy vehicle,  well-mounted, with the star at his box office peak."  
*In "thiswaydown.org", Finn Clark wrote, "the first half isnt very good...However the story picks up as it moves along until by the end, its a charming little romp that made me laugh and made me happy." 

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 