On the Wrong Track
 
 
{{Infobox film
| name           = On the Wrong Track
| image          = OntheWrongTrack.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD cover
| film name      = {{Film name
| traditional    = 毀滅號地車
| simplified     = 毁灭号地车
| pinyin         = Huì Miè Hào Dì Chē 
| jyutping       = Wai2 Mit6 Hou6 Dei6 Ce1 }}
| director       = Clarence Fok
| producer       = Mona Fong
| writer         = 
| screenplay     = Shaw Creative Group
| story          = 
| based on       = 
| narrator       = 
| starring       = Andy Lau Jeem Yim Elliot Ngok Prudence Liew Winnie Chin
| music          = Michael Lai
| cinematography = Siu Yuen Chi
| editing        = Chiang Hsing Lung Lau Shiu Kwong
| studio         = Shaw Brothers Studio
| distributor    = Shaw Brothers Studio
| released       =  
| runtime        = 88 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$2,554,649
}}
 1983 Cinema Hong Kong action drama film directed by Clarence Fok and starring Andy Lau, Jeem Yim, Elliot Ngok, Prudence Liew and introducing the then newcomer actress Winnie Chin. The film is Laus third film and also his first leading film role.

==Cast==
*Andy Lau as Paul Chan
*Jeem Yim as Dee
*Elliot Ngok as Chan Sir, Paul and Dees father
*Prudence Liew as Sze
*Winnie Chin as girlfriend of Chan Sir
*Wai Kei Shun
*Tin Mat as Mrs. Cheung
*Stephen Shiu
*Tai Yuet Ngor
*Lau Kwok Sing as King Kong
*Hui Ying Ying as store owner
*Chan Po Cheung
*Ricky Wong
*Ngai Tim Choi as guard
*Rocky Wong as guard
*Ng Wui as Uncle Cheung
*Cheung Lai Ping as Chius date
*Danny Poon as Roger
*Chin Tsi-ang
*Yat Boon Chai as cop
*Cheung Chi Hung as refugee at camp
*Luk Ying Hung as cop
*Fong Yue
*Wong Kung Miu as teacher
*Cheung Chok Chow
*Ngai Tung Gwa

==Theme song==
*Break Into New Grounds (闖進新領域)
**Composer: Michael Lai
**Lyricist: Cheng Kwok Kong
**Singer: Leslie Cheung

==Box office==
The film grossed HK$2,554,649 at the Hong Kong box office during its theatrical run from 15 to 22 April 1983 in Hong Kong.

==See also==
*Andy Lau filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 