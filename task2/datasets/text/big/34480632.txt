Orbis Pictus (film)
{{Infobox film
| name           = Orbis Pictus
| image          = Orbis Pictus film.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Martin Šulík
| producer       = Rudolf Biermann
| writer         = 
| screenplay     = Martin Šulík  Marek Leščák  Ondrej Šulaj
| story          = 
| based on       = 
| narrator       = 
| starring       = Dorota Nvotová  Marián Labuda  Božidara Turzonovová  Július Satinský  Emília Vášáryová  František Kovár  
| music          = Vladimír Godár
| cinematography = Martin Štrba
| editing        = Dušan Milko
| studio         = 
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = Slovakia  Slovak
| budget         = 
| gross          = 
}}
 Slovak feature film|film, starring Dorota Nvotová, Marián Labuda, Božidara Turzonovová, Július Satinský, Emília Vášáryová and František Kovár . The film, directed by Martin Šulík,   won Special Award of the Jury at the International Filmfestival Mannheim-Heidelberg in Baden-Württemberg, Germany as the Best Film in 1997.   

==Plot Summary==
After being kicked out of boarding school sixteen-year-old Terezka goes on a fantasy-filled surrealistic journey to find her estranged mother.  Along her way to Bratislava she comes in contact with a mystical  and strange cast of characters. 

==Cast==
*Dorota Nvotová as Terezka
*Marián Zednikovič as chaffeur
*Július Satinský as Drusa
*Hana Gregorová as bride
*Anton Šulík as father
*Marián Labuda as Emil
*Božidara Turzonovová as Marta 
*Emília Vášáryová as mother
*František Kovár as Tomáš
*Mojmír Caban as railroader
*Ľubica Krkošková as railroaders wife
*Donato Moriconi as Momo
*Jakub Ursíny as groom
*Oľga Vronská as old woman
*Róbert Zimermann as Imriško

===Additional credits===
* František Lipták - art director
* Milan Čorba - costume designer sound
* Anita Hroššová - makeup artist
* Erik Panák - assistant director
* Dalibor Michalčík - assistant camera
* Viktor Fančovič - camera operator
* Jana Liptáková - script editor
* Ján Kittler - assistant sound conductor
* sound mix

==Awards ==
{| class="wikitable"
|- Year
! Nomination
! style="width:150px;"|Award(s)   Category
!width="60"|Result
|- 1997
|Orbis Pictus KFN SSN - Critics Award Best Film
| 
|-
|Božidara Turzonovová Czech Lions Best Supporting Actress
|  
|- Martin Štrba IGRIC Award Best Camera
| 
|- Martin Šulík Best Director
| 
|- 1998
|International IFF Mannheim-Heidelberg Special Award of the Jury
| 
|-
|Orbis Pictus Alpe Adria Cinema Peace Award
| 
|}
;Notes
*A   Won Klára Issová for her role of Veronika in Nejasná správa o konci sveta by Juraj Jakubisko.  The second nominee included Vilma Cibulková (for her role of in Výchova dívek v Čechách by Petr Koliha). 

==See also==
*List of Slovak submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 