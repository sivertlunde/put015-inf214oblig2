Scene Onnu Nammude Veedu
 
{{Infobox film
| name = Scene Onnu Nammude Veedu
| image =
| caption =
| director = Shyju Anthikkad
| producer =K.K.Narayandas & Jins Puliparambil
| writer = Shylesh Divakaran
| starring = {{Plainlist| Lal
* Navya Nair
* Thilakan
* Asif Ali Malavika
* Shritha Sivadas
* Lalu Alex
* Harisree Ashokan
}}
| music = Ratheesh Vegha
| cinematography = Alagappan
| editing = Saajan V
| studio =  
| distributor = Ramya Movies
| released =  
| country = India
| language = Malayalam
| gross =
}}
 Malayalam film directed by Shyju Anthikkad.   

==Background== Lal plays the character of Ottapalam Unni and Navya Nair returned to Malayalam cinema as the character Manju, who is a teacher. K.K Narayanadas produced the movie. It is the last time Thilakan appeared in a film.

== Cast == Lal as Ottappalam Unni
* Navya Nair as Manju
* Thilakan as Vishwan (Dubbed by his son Shobi Thilakan)
* Asif Ali as Himself
* Lalu Alex as K.K.Jose
* Shritha Sivadas as Parvathy
* Harisree Ashokan as Bhasi
* Sudheesh Raghavan
* Kalabhavan Shajon
* Shivaji Guruvayoor as Rajan
* Manikuttan as Rafique
* Ambika Mohan as Shyama Teacher Augustine 
* Shammi Thilakan
* Sasi Kalinga
* Urmila Unnu as K.K.Joses wife
* Swapna Menon as K.K.Joses daughter
* Anjali Upasana as TV Reporter

==References==
 

==External links==
*   at the Malayalam Movie Database

 
 
 


 