The Crunch Bird
{{Infobox Hollywood cartoon
| cartoon name      = The Crunch Bird
| series            = 
| image             = 
| image size        = 
| alt               = 
| caption           = 
| director          = Ted Petok	 	
| producer          = Ted Petok
| voice actor       = Len Maxwell
| studio            =   Maxwell-Petok-Petrovich Productions Regency Films
| distributor       = Regency Films
| release date      = 1971
| color process     = 
| runtime           = 2 min.
| country           = USA
| language          = English
}}
The Crunch Bird (El pájaro crujiente) is an animated short by Ted Petok, Joe Petrovich, and Len Maxwell. It won the 1971 Academy Award for Best Animated Short Film. Joe Petrovich animated the cartoon. Len Maxwell provided the voices for the husband, wife, and pet shop owner. Ted Petok was the producer.

A woman goes into a pet store seeking a gift for her husband. She decides to get him a bird with a most unusual talent, but her gift brings about an unforeseen result.

With a running time of only two minutes, it is one of the shortest films to receive an Academy Award.

==Plot==
A Narrator tells the short about a woman finding a gift for her husband who is at a busy job, She tries to find a store. But her husband isnt a sports fan or a television watcher, So she looks at the pet store and thinks her husband would love a pet, The woman enters the store and asks the pet shop owner which kind of pet would her husband love. The pet shop owner tells her if she would want a dog, cat, or monkey but she thinks her husband wouldnt want any of those kinds of pets. But upon seeing her last choice a bird she agrees to buy it, But the pet shop owner warns her about the bird and shows her what the bird does, So he commands the bird to destroy a wooden chair the bird flies to it and chews up all the chair. The woman impressed by the birds talent she still agrees to buy it and the pet shop owners does give the bird to her. The woman goes home and shortly the husband returns home tired because of a hard day at his job. Upon seeing the bird he gets mad at his wife and while scolding her he accidentally tells the bird to chew him which the bird flies over to him and the screen fades to black with the birds munching noises heard in the ending credits implying the bird chewed his butt.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 

 