Edge of Tomorrow (film)
 
 
{{Infobox film
| name                  = Edge of Tomorrow 
| image                 = Edge of Tomorrow Poster.jpg
| alt                   = A man and a woman, wearing battle armor, holding large guns, and looking battle-worn, stand against an urban background devastated by war. The sky is golden, meteors are falling, and Nelsons Column can be seen in the background.
| caption               = Theatrical release poster
| director              = Doug Liman
| producer              = {{Plain list |
* Erwin Stoff
* Tom Lassally
* Jeffrey Silver
* Gregory Jacobs
* Jason Hoffs}}
| screenplay            = {{Plain list |
* Christopher McQuarrie
* Jez Butterworth
* John-Henry Butterworth
}}
| based on               =  
| starring               = {{Plain list |
* Tom Cruise
* Emily Blunt
* Bill Paxton
* Brendan Gleeson }}
| music                  = Christophe Beck
| cinematography         = Dion Beebe
| editing                = {{Plain list |
* James Herbert
* Laura Jennings}}
| production companies   = {{Plain list | Village Roadshow
* Dune Entertainment|RatPac-Dune Entertainment
* 3 Arts Entertainment Viz Productions
}}
| distributor            = Warner Bros. Pictures
| released               =   
| runtime                = 113 minutes  
| country                = United States 
| language               = English
| budget                 = $178 million 
| gross                  = $369.2 million 
}}
Edge of Tomorrow (also marketed with the tagline Live. Die. Repeat.)  is a 2014 American science fiction action film starring Tom Cruise and Emily Blunt. Doug Liman directed the film based on a screenplay adapted from the 2004 Japanese light novel All You Need Is Kill by Hiroshi Sakurazaka. The film takes place in a future where Earth is invaded by an alien race. Major William Cage (Cruise), a public relations officer inexperienced in combat, is forced by his superiors to join a landing operation against the aliens. Though Cage is killed in combat, he finds himself in a time loop that sends him back to the day preceding the battle every time he dies. Cage teams up with Special Forces warrior Rita Vrataski (Blunt) to improve his fighting skills through the repeated days, seeking a way to defeat the extraterrestrial invaders.
 Warner Bros. Leavesden Studios outside London, and other locations such as Trafalgar Square and Saunton Sands. Nine companies handled the visual effects.

The film was released in theaters on the weekend of  , 2014, in 28 territories, including the United Kingdom, Brazil, Germany, Spain, India, and Indonesia. On the weekend of  , 2014, it was released in 36 additional territories, including North America (United States and Canada), Australia, China, and Russia. The film grossed over   in theaters worldwide and received largely positive reviews from critics.

==Plot== public affairs officer and former advertising executive, to cover combat on the beaches of France during the next days assault on the Mimics. Cage objects to the dangerous assignment and threatens to use his public relations skills to turn the public against Brigham when the casualties start increasing from the invasion. General Brigham has Cage arrested; Cage is knocked out during an ensuing escape attempt. He wakes in handcuffs at a forward operating base at Heathrow Airport and discovers he has been labeled a deserter and put on combat duty for the invasion under the command of Master Sergeant Farell.

The invasion is a disaster for the humans. Cage manages to kill a large Mimic but dies as he is sprayed with its acid-like blood. He then wakes up at Heathrow the previous morning. No one believes his story that he knows the invasion will fail. He repeats the loop of dying on the beach and waking at Heathrow until he encounters Sergeant Rita Vrataski (Emily Blunt). She recognizes his ability to anticipate events and tells him to locate her the next time he "wakes up".

Together, Cage and  Vrataski meet up with Dr. Carter, a former government scientist and expert in Mimic biology. Cage learns that the kind of Mimic he killed in his first loop, an "Alpha", resets time when it is killed to give the Mimics an advantage in battle. Cage inherited this ability when he was doused in the Alphas blood as they both died. Vrataski had gained this ability in a recent battle but lost it after receiving a blood transfusion. She tells Cage that they must hunt the Mimics hive mind, the Omega.

Over innumerable successive time loops, Vrataski molds Cage into a far more effective soldier. Frustrated by his continued failures, though, he retreats to a London pub, only to discover the Mimics will overrun the city after their invasion on the beach. He and Vrataski then spend several loops learning how to survive the battle on the beach and get inland based on his vision of the Omega hiding within a Bavarian Alps dam. After numerous loops end in Vrataskis death, Cage decides to hunt the Omega alone, abandoning her and the rest of the invasion to doom on the beach. When he arrives at the dam, he discovers that the Omega is not there. He manages to kill himself before an Alpha can steal his blood and prevent him from resetting the day. Back at Heathrow, he tells Vrataski and Carter that his vision was a trick.
 Ministry of Defence in search of a prototype built by Carter that will allow Cage to discover the Omegas true location. After several failed loops they obtain the device, which reveals that the Omega is located under the Louvre Pyramid in Paris. They are injured as they flee; Cage is saved by a blood transfusion, but it removes his ability to reset the day.

Vrataski frees Cage and they return to Heathrow, where they convince his squad to help destroy the Omega. The other squad members sacrifice themselves to get Cage and Vrataski beneath the Louvre. Vrataski distracts a waiting Alpha while Cage advances on the Omega. The Alpha kills Vrataski and mortally wounds Cage, but not before Cage primes and drops a grenade belt into the Omegas core, destroying it, which neutralizes all other Mimics.

Cages dying body floats down into a rising cloud of the Omegas blood. Regaining the power to reset himself, Cage wakes up en route to his meeting with Brigham the day before. Brigham announces that Mimic activity has ceased following a power surge in Paris. With the events leading to his arrest never happening, Cage travels to Heathrow on his own, retaining his original officer rank. None of his former squad mates recognize him. He finds Vrataski, who greets him with the same initial rudeness as previous loops, causing Cage to smile.

==Cast==
 
* Tom Cruise as Major William Cage
* Emily Blunt as Sergeant Rita Rose Vrataski
* Bill Paxton as Master Sergeant Farell
* Brendan Gleeson as General Brigham
* Kick Gurry as Griff
* Dragomir Mrsic as Kuntz
* Charlotte Riley as Nance
* Jonas Armstrong as Skinner
* Franz Drameh as Ford
* Masayoshi Haneda as Takeda
* Tony Way as Kimmel
* Noah Taylor as Dr. Carter
 

Cruise, known for performing his own stunts in his films, also did so in Edge of Tomorrow. He and Blunt wore heavy metal suits that depicted their characters battle suits.  Blunt trained three months for her role, "focusing on everything from weights to sprints to yoga, aerial wire work and gymnastics", and studying the Israeli combat system Krav Maga.   

Paxton was cast in a supporting role and also wore a battle suit in the film. The actor said he suspected that he was cast in the film because of his well-known role in the 1986 science fiction film Aliens (film)|Aliens. 

==Production==
 
* Doug Liman – director
* Christopher McQuarrie – co-writer
* Jez Butterworth – co-writer
* John-Henry Butterworth – co-writer
* Erwin Stoff – producer
* Tom Lassally – producer
* Jeffrey Silver – producer
* Gregory Jacobs – producer
* Jason Hoffs – producer
* Dion Beebe – cinematographer
* Oliver Scholl – production designer
* Kate Hawley – costume designer
* James Herbert – editor
* Laura Jennings – editor
* Christophe Beck – composer Nick Davis – visual effects supervisor
 
  Viz Productions, on a budget of  .  The film was directed by Doug Liman based on a screenplay adapted from the 2004 Japanese light novel All You Need Is Kill by Hiroshi Sakurazaka. 

===Development and writing=== optioned the making a The Black List, a survey of most-liked unproduced screenplays. 

In June 2011, Joby Harold was hired to rework the screenplay.  By September, Warner  approached Brad Pitt to star;  after his decline, the studio then approached Tom Cruise.  Once Cruise accepted, the script changed the age of the leading role to fit the actors.  In December 2011, Cruise officially joined the film.     Emily Blunt entered negotiations to star opposite Cruise in April 2012.  Screenwriting duo Roberto Orci and Alex Kurtzman also delivered a draft of their own. 
 Jack Reacher.  While reading the earlier script, McQuarrie "understood very clearly what the premise of the story was and what they were looking for in terms of characters".  Even if the previous scripts were darker, Cruise stressed the importance of the story’s humor to McQuarrie.    The actor compared Cages violent demises to Wile E. Coyote and The Road Runner, declaring "Its fun coming up with new ways to kill yourself."  

The screenplay did not yet have a satisfactory ending, and despite the producers and studio executives worried about starting filming without a set conclusion, Liman opted to finish the script during principal photography.  McQuarrie at one point suggested adding a twist involving the Mimics figuring out Cages attack on Paris and resetting time during his strike, but discarded it as "you were so exhausted by the time you got to that point.”  Eventually, McQuarrie considered that focusing on the comedic aspects meant "it needed to end in a way that wasn’t harsh", and thus opted to end the plot where it started, on the helicopter bringing Cage to London, fulfilling the notion that  "comedies generally have to go back to the way things were". 

===Filming=== Leavesden Studios near London. Warner Bros. purchased Leavesden as a permanent studio site after previously renting space there for its production of the Harry Potter (film series)|Harry Potter films.  Though Liman  intended to film the beach battle on location, the studio instead had a beach set built at the studio site.  The set was surrounded by chroma key green screens, which the visual effects artists later used to extend the beach with plates shot at Saunton Sands in North Devon.    It was intended for the battle scenes to be reminiscent of coastal battles during World War II such as the Invasion of Normandy and the Battle of Dunkirk. 

Principal photography began at Leavesden on  , 2012.    The Los Angeles Times said on the second day, Liman "demanded a total reshoot of everything filmed on Day 1", which concerned producers.   Filming on the beach set was scheduled to last two weeks, but extended to nearly three months due to what the Los Angeles Times called "the directors self-described workshop-y filming style".  Filming also took place in Trafalgar Square in London on  , 2012. The square was closed to the public, and tanks were brought in to film the action scenes.  A former army base in the village of Barton Stacey in Hampshire, was also used as a filming location for two weeks. 
 rainy British weather since the film was supposed to be set in one day and had to maintain the same weather.  The beach set in particular became boggy and muddy, requiring the effects artists to enhance the environment with digital sand and surf.   Though filming concluded by August 2013, actor Jeremy Piven was added to the cast, and extra scenes including him were filmed;  ultimately, Piven did not appear in the finished film.  Cinematographer Dion Beebe made his first feature film with Liman, with whom he had worked previously on commercials. Beebes approach was to develop "a world under siege, but not a bleak, dark, post-apocalyptic landscape”;  Beebe preferred to avoid the saturated bleach bypass look. 35mm film was used instead of digital cameras to evoke the World War II footage that provided inspiration for the battle scenes.   

===Battle suit design===
Production designer Oliver Scholl and his team worked with lead builder Pierre Bohanna to develop concept art for several battle suit options based on contemporary, real-world   quality.  s Armored ExoSuits | work=The Credits | publisher=Motion Picture Association of America | date=June 3, 2014 | accessdate=June 3, 2014 }} 

The battle suit weighed   on average. One of the heavier versions was around   due to being equipped with a mock sniper rifle and rocket launcher.  Each actor needed four people to help put on the battle suit. Initially, Tom Cruise took   to put on the suit and another   to remove it. Ultimately, the time was reduced to  .   Between takes, the actors would be suspended by chains from iron frames to take the weight of the suits off their shoulders.   

===Visual effects===
Nine companies handled the visual effects for Edge of Tomorrow under VFX supervisor Nick Davis.  Davis worked with the crew of The Third Floor on the films previsualization process. Sony Pictures Imageworks (SPI) worked on the first two acts of the film and created over  , including photorealistic environments, battle scenes, and computer-generated creatures and characters. One major shot involved covering London Heathrow Airport with military troops, vehicles, and aircraft; SPI split some of the work with RodeoFX. Cinesite joined late in the production and developed   for ten key sequences, with 189 appearing in the final cut.   

   MPC created the Omega in a digital environment into which the effects artists composited underwater footage filmed at Leavesdens water tank. 
 photomodeling from dropships were Quad TiltRotor.  Aside from the crashed ship on the beachhead and a gimbal set to depict the plane used by Cages squad, the film used digital models for most ships. The computer-generated dropships had some of Imageworks heaviest detail given the proximity of the actors to the aircraft in the camp scenes; the effects artists wanted to make sure the ships broke apart in a realistic way during the crashes. 
 converted the World War Z and Gravity (film)|Gravity.  The company made use of scans of the casts faces from film production while vendor Nvizible helped the company convert the hologram table used by Dr. Carter. 

===Music===
{{Infobox album
| Name        = Edge of Tomorrow: Original Motion Picture Soundtrack
| Type        = Film score
| Artist      = Christophe Beck
| Cover       = 
| Released    = June 3, 2014
| Recorded    =
| Genre       = Soundtrack
| Length      = 45:30
| Label       = WaterTower Music
| Producer    = 
}} John Powell, horns and trumpets, but he said Liman "preferred a non-traditional approach, driven by percussion and distorted orchestra". To that end, Beck used the pizzicato playing technique, "not in the traditional, plinky-plinky-isnt-this-funny way, but a little darker, and always accompanied by some higher concept synth colors".  The distorted orchestral samples enhanced the comedic tone of the extended sequences where Cage recurrently dies in battle, as the director felt it was important for the audience to find humor in this sequence.  With Limans approach, the composer said there were "only a couple of traditional themes" in the film, including one for Emily Blunts character Rita. 

{{Track listing
| headline        = Additional music
| music_credits   = yes

| title1          = This Is Not the End
| music1          = Fieldwork
| length1         =
| title2          = Massive Mellow
| music2          = Daniel Lenz
| length2         = 
| title3          = Railroad Track
| music3          = Willy Moon
| length3         =
| title4          = Trip Into The Light
| music4          = Jeremy and the Harlequins
| length4         = Love Me Again John Newman
| length5         = 
}}

==Release==

===Marketing===
{{Quote box quote  = "I think the word kill in a title is very tricky in todays world. I dont know that people want to be bombarded with that word. I dont know that people want to be opening the newspaper and seeing that word. We see it enough in kind of real newspaper headlines, and I dont think we need to see it when were looking at a movie." source = — Producer Erwin Stoff on changing the title  width  = 25% align  = right
}} San Diego, Turner Broadcasting, a subsidiary of Time Warner like the studio Warner Bros., promoted the film across its TV properties, including CNN, TNT (TV channel)|TNT, TBS (TV channel)|TBS, Adult Swim, TruTV, and Funny or Die. Variety (magazine)|Variety said the move "put forth the notion that buying bigger packages of advertisements across a TV company’s holdings is a viable option in an increasingly fragmented TV-viewing landscape". Turner also launched a website which would unlock film-related content like "a 3D game, back stories and artwork" if its promotional hashtag was circulated enough through the social media website Twitter. 

Viz Media released a new edition of the light novel on  , 2014, retitled Edge of Tomorrow.  It also published a graphic novel adaptation of the light novel on  , 2014. 
 YouTube personalities and participants from the TV series American Ninja Warrior. Warner Bros. based the teams on the soldiers from "J Squad" in the film. To promote teamwork, the two teams competed in a Tough Mudder obstacle course.   

===Box office forecast===
  The Fault counterprogramming to The Fault in Our Stars since that films demographic is women under 25 years old. 
 Jack Reacher (2012), and Oblivion (2013 film)|Oblivion (2013)—failed to gross more than   in North America. The website forecast that Edge of Tomorrow would gross   in North America and   in other territories.  TheWrap said that the studio focused on theatrical releases in other territories where Cruise "remains a major force" in drawing audiences.  Variety, writing from the US perspective, said, "Media reports have been quick to speculate that Edge of Tomorrow may be one of the summers first big bombs based on the lack of enthusiasm by U.S. audiences. That may come to pass, but these reports downplay the centrality of foreign markets in todays globalized movie industry." 

In the week prior to the release of Edge of Tomorrow in North America, its estimated opening-weekend gross increased from the   range to  .   

===Theatrical run  ===
Edge of Tomorrow initiated its theatrical run in several territories on  , 2014, and rolled out to a total of   for its opening weekend of  , 2014.   It grossed   on its  opening weekend.  For the second weekend of  , 2014, it was released in 36 additional territories.  Edge of Tomorrow grossed $  in North America and $  in other territories for a worldwide total of $ .    After the films theatrical run, Entertainment Weekly said it had a "lukewarm box-office reception" despite praise from critics. 

====Opening weekend====
 s London IMAX on  , 2014   |alt=Façade of the London IMAX theater.]]
The film had premiere screenings in London, Paris, and New York City on  , 2014. The cast and the crew mimicked the films time loop premise by attending the premieres in a single day, traveling westward to attend them on a staggered schedule.    The film was screened in New York City at  , the time chosen to refer to the film title.  The film was released in theaters in  —including the United Kingdom, Brazil, Germany, Spain, and Indonesia—on the weekend of  , 2014.    Certain territories with strong association football followings were chosen so the film could screen to audiences before the month-long 2014 FIFA World Cup began on  , 2014. Edge of Tomorrow competed against Maleficent (film)|Maleficent starring Angelina Jolie, which opened the same weekend in  . 

On its opening weekend in   across  , Edge of Tomorrow grossed  .    The Hollywood Reporter called the films debut a "soft" opening. In many territories, Edge of Tomorrow ranked third behind fellow new release Maleficent and holdover  . These included the United Kingdom, where the film ranked third and grossed  , where Cruises 2013 film Oblivion had opened with  , and Germany, with an income of   compared to Oblivion s  . Edge of Tomorrow ranked first in Indonesia and Taiwan, grossing   and  , respectively.  Its opening weekend in   in Indonesia was Tom Cruises biggest opening to date in the country.  The film also grossed   in Italy and   in Spain.    Deadline.com said the film had good word of mouth, citing significant increases in Saturday grosses compared to the Friday grosses in the United Kingdom, Germany, and Spain.  Bloomberg Businessweek reported that   was grossed in the first week of release and summarized its debut, "While it did solid business in Asia, its reception in Germany, France, and the U.K. has been tepid." 

====Second weekend==== Dragon Boat a five-day holiday) marked Tom Cruises highest opening weekend in both countries.  
 Fandango reported a "B+" grade, where younger filmgoers gave "A" and "A–" grades.    The Los Angeles Times said the disappointing box office performances of non-franchise films Edge of Tomorrow and Blended (film)|Blended, both produced and distributed by Warner Bros. Pictures, indicated risky investments by the studio, which had better success earlier in the year with franchise films The Lego Movie and Godzilla (2014 film)|Godzilla (2014). 

====Subsequent weekends====
In its second weekend of release in North America (  2014), Edge of Tomorrow had a "light" second weekend in box office performance|second-weekend drop of 43% due to word of mouth and grossed   on the second weekend.  In the same weekend in territories outside North America, the film was on  . With approximately   admissions, it grossed  . China, Russia, and South Korea respectively had the films largest weekend grosses among the territories.  In South Korea, the film ranked first at the box office for two consecutive weekends, grossing a total of   by  , 2014. 

In Japan, Edge of Tomorrow was released on  , 2014,  under the light novels title All You Need Is Kill.  The film opened second in the weekend rankings behind Maleficent, with an intake of $4.5 million.   , with $15.3 million, Edge of Tomorrow is the sixth highest-grossing foreign movie of the year in Japan, and the 21st overall. 

===Home media===
Edge of Tomorrow was released on DVD, Blu-ray Disc, and video on demand in the United States on  , 2014. The Blu-ray disc includes over   of bonus features.  The home releases packaging downplays the original Edge of Tomorrow title in favor of placing more prominence on the films original tagline, "Live. Die. Repeat." Media critics believed that the re-branding was an attempt by Warner Bros. to re-launch the films marketing following its poor U.S. box office performance. Posters for the films theatrical release had similarly placed a larger emphasis on the "Live. Die. Repeat." tagline than the actual title of the film.       Similarly, some digital retailers listed the film under the title Live Die Repeat: Edge of Tomorrow.  The film ranked first in home media sales for the week beginning  ,  with 62% of sales coming from the Blu-ray version. 

==Reception==

===Critical response=== rating average of 7.5 out of 10. Based on the reviews, the website gave the film a score of 90%.  Another aggregator Metacritic surveyed 43 critics and assessed 35 reviews as positive and eight as mixed, with none negative. Based on the reviews, it gave the film a score of 71 out of 100, which it said indicated "generally favorable reviews". 
 The Bourne Identity (2002). Chang said the screenwriters, with the assistance of the editors, "tell their story in a breezy narrative shorthand (and at times, sleight-of-hand), transforming what must surely be an unbelievably tedious gauntlet for our hero into a deft, playful and continually involving viewing experience". Regarding the relationship between Cruise and Blunts characters, Chang said "Liman handles it with a pleasing lightness of touch that extends to the proceedings as a whole." He also commended the visual effects of the "expertly designed Mimics" as well as Dion Beebes cinematography.   

Todd McCarthy, writing for The Hollywood Reporter, said the film was "a narratively ambitious sci-fi actioner" that "takes a relatively playful attitude toward the familiar battle tropes". McCarthy said despite the humor, he found the time loop premise "tedious" and that "the final stretch becomes dramatically unconvincing and visually murky". The critic called the effects "exciting, convincing and gritty" and applauded Gleeson and Paxton in their supporting roles.    Kenneth Turan of the Los Angeles Times gave the film a positive review, considering the film "a star-driven mass-market entertainment thats smart, exciting and unexpected while not stinting on genre satisfactions" that broke a strain of "cookie-cutter, been-there blockbusters". 

The film was also included by various critics in their year-end 2014s best films list. Wesley Morris, who won the Pulitzer Prize for Criticism in 2011, listed Edge of Tomorrow as the sixth best film of the year;  similarly, Manohla Dargis of The New York Times included the film in her list of films released in 2014 "that meant the most" to her. 

===Accolades===
The theatrical trailer for Edge of Tomorrow was nominated at the 15th Annual   and Godzilla, respectively.  The film also received nominations at the 2014 Teen Choice Awards for Best Action Film, Best Action Actor (Tom Cruise), and Best Action Actress (Emily Blunt), but lost to Divergent (film)|Divergent.  The Japanese government gave director Doug Liman the Annual Japan Cool Content Contribution Award, an accolade that recognizes creatives who popularize Japanese media for worldwide audiences. 

Edge of Tomorrow was nominated for Critics Choice Movie Awards in the following categories: Best Visual Effects, Best Action Movie, Best Actor in an Action Movie, and Best Actress in an Action Movie, with Blunt winning the latter.  

 
{| class="wikitable plainrowheaders" 
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | List of accolades received by Edge of Tomorrow
|- style="text-align:center;"
! style="background:#ccc;" width="30%"| Award
! style="background:#ccc;" width="25%"| Category
! style="background:#ccc;" width="35%"| Recipient(s) and Nominee (s)
! style="background:#ccc;" width="10%"| Result
|- 2014 Alliance of Women Film Journalists Awards
| Kickass Award for Best Female Action Star
| Emily Blunt
|  
|-
| Most Egregious Age Difference Between The Leading Man and The Love Interest 
| Tom Cruise and Emily Blunt
|  
|- 
| scope="row"| 42nd Annie Awards
| Outstanding Achievement, Animated Effects in a Live Action Production
| Steve Avoujageli, Atsushi Ikarashi, Pawel Grochola, Paul Waggoner, Viktor Lundqvist 
|  
|-  2014 Boston 3rd Boston Online Film Critics Association Awards
| Best Editing
| James Herbert, Laura Jennings 
|  
|- 20th Critics 20th Critics Choice Movie Awards
| Best Actress in an Action Movie
| Emily Blunt
|  
|-
| Best Visual Effects
| 
|  
|-
| Best Action Movie
| 
|  
|-
| Best Actor in an Action Movie
| Tom Cruise 
|  
|- 
| scope="row"| 2014 Central Ohio Film Critics Association Awards
| Best Overlooked Film
| 
|  
|- 2014 Denver Film Critics Society Awards
| Best Science-Fiction/Horror Film
|
|  
|-
| 20th Empire Awards  Best Actress
| Emily Blunt
|  
|- Golden Trailer 2014 Golden Trailer Awards
| Best Action
| 
|  
|-
| Best Summer Blockbuster 2014 Trailer
|
|  
|- 
| scope="row"| 18th Las Vegas Film Critics Society Awards
| Best Editing
| James Herbert, Laura Jennings 
|  
|- 35th London Film Critics Circle Awards
| British Actress of the Year
| Emily Blunt
|  
|- 15th Phoenix Film Critics Society Awards
| Overlooked Film of The Year
| 
|  
|-
| Best Stunt
| 
|  
|-
| Best Visual Effects
| 
|  
|- San Diego 19th San Diego Film Critics Society Awards Best Editing
| James Herbert, Laura Jennings 
|  
|- 41st Saturn Awards  Best Science Fiction Film
| 
|  
|- Best Director
| Doug Liman
|  
|- Best Writing
| Christopher McQuarrie, Jez Butterworth and John-Henry Butterworth
|  
|- Best Actor
| Tom Cruise
|  
|- Best Actress
| Emily Blunt
|  
|- Best Editing
| James Herbert and Laura Jennings
|  
|- Best Special Effects Nick Davis, Jonathan Fawkner and Matthew Rouleau
|  
|- 2014  Science 2.0 Science In Film Awards 
| Best Picture
| Doug Liman 
|  
|- Teen Choice Teen Choice Awards 2014
| Choice Movie Action/Adventure
| 
|  
|-
| Choice Movie Actor: Action Adventure
| Tom Cruise
|  
|-
| Choice Movie Actress: Action Adventure
| Emily Blunt
|  
|- Visual Effects 13th Visual Effects Society Awards
| Outstanding Virtual Cinematography in a Photoreal/Live Action Feature Motion Picture
| Albert Cheng, Jose Enrique Astacio Jr., Michael Havart, Dion Beebe
|  
|-
| Outstanding Effects Simulations in a Photoreal/Live Action Feature Motion Picture
|  Steve Avoujageli, Pawel Grochola, Atushi Ikarashi, Paul Waggoner
|  
|-
| Outstanding Compositing in a Photoreal/Live Action Feature Motion Picture
| Craig Wentworth, Matthew Welford, Marie Victoria Denoga, Frank Fieser
|  
|}

==Social commentary==

===Gender roles===
 
Emily Blunt plays Sergeant Rita Vrataski, a veteran who guides and trains Tom Cruises character, Major William Cage. Blunt said of her role, "In these male-fueled genres, its usually the woman whos holding the hand of the guy and hes running through explosions leading her, and I wanted to be doing the leading."  Chris Nashawaty, reviewing the film for Entertainment Weekly, called it "the most feminist summer action flick in years".  Bustle s Alicia Lutes described Rita as "ruthless and exacting in her takedown ... of a bunch of aliens" and said, "This is very much counter to the age-old ideals about ladies being the constant, delicate flowers of emotional heartstring-pulling." Lutes noted how Cages strength depended on Ritas guidance, as "she trains him, aids him, and protects him (and in turn the fate of humanity) time and time again."  Blunt herself found it "joyous" that Cruise played "a character who was so useless at what   was doing” in contrast to a strong female.  Tasha Robinson, writing a piece in The Dissolve about "strong female characters" that lack real purpose in films, said Rita in Edge of Tomorrow was an exception. Robinson acknowledged that Rita existed to support Cage in his trials but believed that "the story doesnt degrade, devalue, weaken, or dismiss her". 

In contrast, The Wire s Esther Zuckerman criticized the inclusion of a romantic relationship in the film and said of the two characters kiss, "Theres a case to be made that the kiss is simply an acceptance of their fate, but everything we know about Rita up until this point implies that shes a dedicated soldier, and making her a sudden romantic betrays her character." Zuckerman added, "Thats not to say she cant soften up a bit as humans do, but the moment reads less like shes accepting her humanity and more like the filmmakers had to acknowledge two attractive leads ... who should lock lips because thats what men and women do in movies."  Monika Bartyzel in The Week also criticized the romance in the film, stating that Rita is the one who kisses Cage, despite knowing him for only a day where he had known her for multiple days via time loop. Bartyzel said Ritas portrayal was part of a commonly seen motif in which a female character helps a male "Chosen One" character. Bartyzel said the phenomenon was "the new normal because it allows Hollywood to appeal to feminist concerns while continuing to feed male wish fulfillment". The journalist said Rita "at her most powerful" ultimately serves "to make the male hero into a fighter like herself". 

===Comparison to video games=== respawning feature level when film adaptations of video games,  s}} Angela Watercutter said Limans film was more successful for  basing itself around the mediums narrative structure, and for its "ability to continue after Game Over and discover something new". 

==See also==
* List of films featuring time loops
* List of films featuring powered exoskeletons
* List of science fiction films of the 2010s
* The Defence of Duffers Drift, a 1904 short book with a similar premise

==Notes==
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 