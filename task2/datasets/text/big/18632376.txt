Turkey Shoot (1982 film)
 
 
 
{{Infobox film
| name           = Turkey Shoot
| image          = Turkeyshootposter.jpg
| caption        = Theatrical release poster
| image_size     = 250px
| director       = Brian Trenchard-Smith
| producer       = William Fayman  
| screenplay     = Jon George Neill D. Hicks
| story          = David Lawrence George Schenck Robert Williams Michael Craig Carmen Duncan Brian May
| cinematography = John R. McLean
| editing        = Alan Lake
| studio         = Hemdale FGH Filmco Second FGH Films
| distributor    = Roadshow (Australia)
| released       =   (Australia) October 1983 (US)
| runtime        = 93 min.
| country        = Australia
| language       = English
| budget         = A$3.2 million 
| gross          = A$321,000 (Australia)
}} Australian dystopian Michael Craig. The cast is a mix of international actors, Australian soap opera actors and television personalities.

The film is notable for its extreme violence and sadistic prison sequences, and it features plot elements of The Most Dangerous Game, but rather than having human targets hunted for sport by a madman on his own island, the story features a concentration camp known as "The Establishment", which offers the opportunity to rich adventurers with legal immunity. AskMen labeled it "Easily the cheapest and nastiest piece of mainstream celluloid ever stitched together by our mad cinematic scientists". 

==Synopsis== Michael Craig) Roger Ward), the prisoners accept a deadly deal. They will be human prey in a "turkey shoot", which Thatcher has organised for Secretary Mallory (Noel Ferrier), and VIPs Jennifer (Carmen Duncan) and Tito (Michael Petrovich). If they can evade the heavily armed guests in the surrounding jungle until sundown, Chris, Rita and Paul will be set free. As the "turkey shoot" progresses, the tables are turned, and the prisoners become the hunters, culminating in a free-for-all slaughter terminated by a government napalm airstrike.

==Cast==
* Steve Railsback as Paul Anders
* Olivia Hussey as Chris Walters Michael Craig as Charles Thatcher
* Carmen Duncan as Jennifer
* Noel Ferrier as Secretary Mallory
* Lynda Stoner as Rita Daniels 
* Roger Ward as Chief Guard Ritter
* Michael Petrovitch as Tito
* Gus Mercurio as Red
* John Ley as Dodge
* Bill Young as Griff
* Steve Rackman as Alph
* John Godden as Andy
* Oriana Panozzo as Melinda

==Production==
According to Brian Trenchard-Smith, the film was always meant to be a satire.
 The original script was sort of like I Am a Fugitive From a Chain Gang meets The Most Dangerous Game, with 70 pages of Chain Gang and Most Dangerous Game lasted about 35 pages. I didnt think that was good balance.  Also it was set in the depression era Deep South. We had tax based financing in place on condition that it was supposed to be set in Australia. So I suggested we set it in the future, and make it more universal. We wanted to make a tongue in cheek but gutsy action movie with subtext about corporate fascism at the beginning of the Reagan era. (He had Australia frightened...Ronald Ray Gun cartoons were often spray painted on Sydney walls in 1981)  So we hired Neill & Jon to redo it. Then of course,  there were days when I was writing pages myself during the shooting.   accessed 21 October 2012   Australian film tax exemption scheme 10BA. Under 10BA film costs were subsidized by the Australian government, and directors tended to cast foreign leads in the hope of boosting success in Europe or the Americas.

The movie was shot in northern Queensland near Cairns. According to Brian Trenchard-Smith the schedule was slashed from 44 days to 28 just prior to filming.   accessed 28 September 2012  David Hemmings was one of the executive producers and shot some second unit which Trenchard-Smith says caused some tension:
 He did not think much of me. He knew that a Completion Gurantor had me waiting in the wings to take over an earlier film Hemmings was directing if he continued to remain behind schedule. He was having an on again off again affair with a member of the cast. When he made fun of me in front of my wife and new born son once in the production office, I poured beer on his head. He was a little more careful with me after that.  
Lynda Stoner had insisted on no nudity when accepted the role, but when she arrived on set she found out she was required to do some. She objected, pressure was put on her, so she compromised and did a back shot but says she always regretted it. Olivia Hussey had a miserable time during filming, hating coming to work and being terrified of the Australian bush. 

==Release==
Turkey Shoot grossed $321,000 at the box office in Australia,  which is equivalent to $984,110 in 2009 dollars.

The film was released theatrically in the United States by New World Pictures as Escape 2000 in October 1983. 
 British Prime Minister.
 
The film was released on special edition DVD by Anchor Bay Entertainment. 

===2008 Melbourne International Film Festival===
Turkey Shoot featured in a Focus on Ozploitation collection of 1970s and 1980s Australian exploitation films, including Barry McKenzie Holds His Own, Dead End Drive-In and Razorback (film)|Razorback. These over-the-top B grade films were characterized by lashings of gratuitous sex, violence and fuel-injected muscle car mayhem which pushed the boundaries of audience taste to new limits.   

==Remake== Turkey Shoot was released sometime in 2014. Directed by Jon Hewitt and also produced by Antony I. Ginnane. 

==See also==
*Cinema of Australia
* , documentary that featured Turkey Shoot

== References ==
 

==Bibliography==
*  

==External links==
*  
*   at Australian Screen Online
*  at Oz Movies
*   Australian Centre for the Moving Image
*  on The Talkhouse]
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 