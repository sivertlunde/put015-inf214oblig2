Haseena Maan Jayegi
 
{{Infobox film
| name           = Hasina Maan Jayegi
| image          = HasinaMaanJaayegi.jpg
| image_size     =
| caption        = 
| director       = Prakash Mehra
| producer       = 
| writer         = S. M. Abbas
| narrator       =  Johnny Walker
| music          = Kalyanji Anandji
| cinematography = N. Satyen
| editing        = R. Mahadik
| distributor    = 
| released       = 1968
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Johnny Walker. Rafi duet Bekhudi Mein Sanam being the most popular of them all. 

Prakash Mehra made his debut with this film. 

== Plot ==
Indian Army Officer Kamal (Shashi Kapoor), and Archana (Babita) get married. Shortly thereafter he is called to the front as war has broken out. There, he meets Rakesh - his look-alike - who plans to kill Kamal and take his place. Kamal fights and emerges the winner. To escape prosecution/court martial, he runs from the front and returns home. Army personnel think Rakesh has become a deserter and that Kamal is missing in action and believed dead. The army personnel notify Archana, who is thrown in a dilemma, as to whether it is Rakesh or Kamal that she is spending time with?

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Lyricist
|-
| 1
| "Dilbar Dilbar Kahte Kahte Hua Diwana"
| Mohammed Rafi, Lata Mangeshkar
| Qamar Jalalabadi
|-
| 2
| "Bekhudi Mein Sanam"
| Mohammed Rafi, Lata Mangeshkar
| Akhtar Romani
|-
| 3
| "O Dilbar Janiye"
| Mohammed Rafi
| Prakash Mehra
|-
| 4
| "Chale The Saath Milkar"
| Mohammed Rafi
| Qamar Jalalabadi
|-
| 5
| "Mere Mehboob Mujhko Itna Bata"
| Manna Dey, Asha Bhosle
|
|-
| 6
| "Suno Suno Kanyaon Ka Varnan"
| Mohammed Rafi, Mahesh Kumar 
|
|}

==References==
 

== External links ==
*  

 
 
 
 
 

 