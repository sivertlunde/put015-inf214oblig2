Without a Clue
{{Infobox film
| name = Without a Clue
| image = Without_a_clue.jpg
| caption = original film poster
| director = Thom Eberhardt
| writer = Larry Strawther Gary Murphy Paul Freeman
| producer = Marc Stirdivant
| music = Henry Mancini ITC Entertainment Group
| distributor = Orion Pictures
| released = 1988
| runtime  = 107 min.
| country = United Kingdom
| language = English
| budget =
| gross = $8,539,181
}} 1988 United British comedy film directed by Thom Eberhardt and starring Michael Caine and Ben Kingsley.

==Plot==
Sherlock Holmes is a fictional character created by Doctor Watson|Dr. John Watson (Ben Kingsley) as the central character in a series of short stories published in Strand Magazine. Watson uses the character to enable him to solve crimes incognito, so as not to disrupt his career as a doctor during a period when he was applying for a post at an exclusive hospital, one in which the senior staff would frown on Watsons "hobby". Although he does not secure the job, Watson decides to satisfy public demand to see Holmes in person by hiring unemployed actor Reginald Kincaid (Michael Caine) to play the part of the fictional detective.

Continuing to investigate cases, now with Kincaid as "Holmes" by his side, Watson is the detective hidden behind the charade of the fictional Sherlock Holmes. Kincaid must rely on direction from Watson, memorizing the doctors exacting, detailed instructions every step of the way.

After a major case at a museum, Kincaid oversteps his boundaries with Watson, who fires him. Watson wants to write the character off and tries to start a new series about "The Crime Doctor" with Watson himself being recognized as the great detective. The Strands editror, Norman Greenhough, Peter Cook is quite cold to the idea as is everyone else.

With a new crime, Watson finds he is unable to get information on his own; only when he mentions "Holmes" does he get anywhere. As the crime becomes a major case, the British government seeks the aid of "Sherlock Holmes" and no one else, including Watsons "Crime Doctor".  The mystery involves the Bank of England £5 banknote printing plates that have been stolen, with the printing supervisor, Peter Giles, having gone missing on the night of the robbery. With the counterfeiting of these five pound notes, the collapse of the British Empires economy would prove inevitable.
 Wiggins (Matt Matthew Savage), the leader of the boys Watson pays to keep an eye on people, whom he calls the "Baker Street irregulars," in contrast to the "regular" London police force.
 Paul Freeman) is the mastermind behind the scheme.  Watson is apparently killed, forcing "Holmes" to solve the case on his own. Having tracked the villain to an abandoned theatre, "Holmes" discovers Watson is still alive. The two work together to free Giles and defeat Moriarty.

Watson has a new appreciation for Kincaid and the public is assured that the team of Sherlock Holmes and Dr. John Watson will continue their detective work.

==Context== Conan Doyle (also a physician) who tired of his fictional creation Holmes and tried unsuccessfully to kill him off. Thirdly, the character of Norman Greenhough, is inspired by Herbert Greenhough Smith, the very real editor of Strand Magazine whose faith in the Holmes/Watson characters brought fame and revenue to both writer and periodical.

==Cast==
{| class="wikitable"
|-
| Actor/Actress || Role
|-
| Michael Caine || Sherlock Holmes / Reginald Kincaid
|-
| Ben Kingsley || Doctor Watson|Dr. John Watson
|- Inspector George Lestrade
|-
| Lysette Anthony || Lesley Giles (Imposter)
|- Paul Freeman Professor James Moriarty
|-
| Pat Keen || Mrs. Hudson
|- Wiggins
|-
| Nigel Davenport || Lord Smithwick
|-
| Tim Killick || Sebastian Moran
|- Greenhough
|- John Warner || Peter Giles
|-
| Matthew Sim || Lesley Giles (Real)
|-
| Fredrick Fox || Priest
|-
| Harold Innocent || Lord Mayor Gerald Fitzwalter Johnson
|- George Sweeney || John Clay
|-
| Murray Ewan || Archie
|-
| Jennifer Guy || Lord Mayors daughter
|}

==Reception==

The movie has received decent reviews from critics, and is frequently included in "top 10" lists of Sherlock films. 

Roger Ebert, however, gave the film two stars:   

 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 