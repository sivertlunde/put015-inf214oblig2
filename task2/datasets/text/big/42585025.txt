A Grande Vitória
{{Infobox film
| name           = A Grande Vitória
| image          = A Grande Vitória Poster.jpg
| caption        = Theatrical release poster
| director       = Stefano Capuzzi
| producer       = Stefano Capuzzi
| writer         = Stefano Capuzzi Paulo Marcelo do Vale Tavares
| based on =  
| starring       = Caio Castro Sabrina Sato Suzana Pires Moacyr Franco
| music          = João Carlos Martins
| cinematography = 
| editing        = 
| studio         = Alfa Filmes
| distributor    = Downtown Filmes  Paris Filmes
| released       =  
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          =  sports drama film based on the book Aprendiz de Samurai by Max Trombini. This film adaptation was directed by Stefano Capuzzi Lapietra. Starring Caio Castro and Sabrina Sato, it tells the true story of the judoka Max Trombini, who had a humble and troubled childhood. Through judo, the boy becomes involved with martial arts principles and thus learn to settle down emotionally and starts building a career, becoming one of the greatest judo technicians in Brazil.   

==Plot==
Max Trombini had a humble and troubled childhood. Abandoned by the father when still young, he was raised by his mother and grandfather, who died when he was 11 years old. Disgusted, he began to be involved in various confusions in his hometown, Ubatuba, and then in Bastos, where he lived. It was through learning martial arts, particularly judo, which he managed to establish emotionally and to build a career. {{Cite web|url=http://cinema10.com.br/filme/a-grande-vitoria |title=A Grande Vitória
 |accessdate=2014-04-25 |work=Cinema10}} 

==Cast==
* Caio Castro as Max
* Felipe Falanga as Young Max
* Sabrina Sato as Alice
* Ênio Gonçalves as Benedito
* Suzana Pires as Teresa
* Domingos Montagner
*Moacyr Franco
*Rosi Campos
*Tato Gabus Mendes

==Production==
===Filming===
In order to interpret the judoka Max Trombini, Caio used his own experience in the sport. He practiced judo as a child, but he didnt get very far in terms of graduation. Some scenes of the film were filmed at the traditional academy Vila Sônia, in São Paulo. It was there that the Olympic champion Aurélio Miguel began practicing the sport and where Caio made an immersion with renowned technicians as the masters Massao and Luiz Shinohara, current mens team coach. {{Cite web|url=http://www.cbj.com.br/noticias/3816/filme-%E2%80%9Ca-grande-vitoria%E2%80%9D-chega-dia-8-de-maio-aos-cinemas.html |title=Filme “A Grande Vitória” chega dia 8 de maio aos cinemas
 |accessdate=2014-04-25 |work=Brazilian Judo Confederation|CBJ}} 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 