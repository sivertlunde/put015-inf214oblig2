Akropol
{{Infobox Film
| name = Akropol
| director = Pantelis Voulgaris
| producer = 
| director of photography = Dinos Katsouridis
| writer = Screenplay: Pantelis Voulgaris
| starring = Lefteris Voyatzis   Stavros Paravas   Konstantinos Tzoumas   Themis Bazaka   Sotiria Leonardou   Antzela Gerekou   Andigoni Alikakou   Manos Vakousis   Yannis Sampsiaris   Eirini Inglesi   Olga Damani   Popi Papadaki   Despo Diamantidou
| music =  Composer: Yiorgos Mouzakis
| editing = 
| distributor = Alco Films
| released = Greece 4 January 1995
| runtime = 125 minutes
| country = Italy   Germany   Bulgaria   Greece
| language = Greek
}}

Akropol (aka Acropole) is a 1995 musical film by Alco Films (with  F.F. Film House Ltd, Greek Film Centre and ET 1). It is directed by Pantelis Voulgaris, and was filmed wholly in Sofia, Bulgaria.

==Cast==
*Lefteris Voyatzis - Lakis Loizos
*Stavros Paravas - Antonis o Prigipas
*Konstantinos Tzoumas - Platon
*Themis Bazaka - Rena
*Sotiria Leonardou - 		
*Antzela Gerekou - Zozo
*Andigoni Alikakou - 		
*Manos Vakousis - Foteinias
*Yannis Sampsiaris - 		
*Eirini Inglesi - 		
*Olga Damani - Marika
*Popi Papadaki - 	
*Despo Diamantidou - Antonis Mother

==Soundtrack==
This list only contains some of the songs featured in Akropol.

* "Kleise ta matia sou"
Written by Nikos Portokaloglou 
Performed by Nikos Portokaloglou & Melina Kana

* "Maharagias"
Written by Nikos Portokaloglou 
Performed by Melina Kana

==External links==
* 

==References==
* 
* 

 
 
 
 
 
 
 
 

 