Puzzlehead
{{Infobox film
| name           = Puzzlehead
| image          = Puzzlehead 2004 poster.jpg
| caption        = Theatrical release poster
| director       = James Bai
| producer       = James Bai Stephen Bai Jason Kraut
| writer         = James Bai Robbie Shapiro Mark Janis
| music          = Max Avery Lichtenstein
| cinematography = Jeffery Scott Lando
| editing        = Miranda Devin
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
}} Robbie Shapiro, and Mark Janis. It was written and directed by James Bai and the film debuted at the Tribeca Film Festival on April 21, 2005 before opening in limited release in New York City on March 23, 2006.

== Plot ==
Walter, a scientist living in a dark world where technology has been outlawed, secretly works to create a self-aware android in his own likeness. This android, named Puzzlehead by Walter, acts as the scientists companion and his connection to the outside world; all the time developing his own personality and self-awareness in the manner of a learning child. The android and his maker turn against one another when Puzzlehead pursues Julia, a woman who does not know Walter has feelings for her.

==Awards and nominations==
* Official selection to the 2005 Tribeca Film Festival
* Won the Jury Award for Best Special Effects at the Austin Fantastic Festival.

== Cast ==
* Stephen Galaida - Puzzlehead/Walter Robbie Shapiro - Julia

== Filming locations ==
New York, USA
* Brooklyn, New York

== External links ==
*  
*  
*  

 
 
 


 