Cross My Heart and Hope to Die (film)
 
{{Infobox film
| name           = Cross My Heart and Hope to Die
| image          = 
| caption        = 
| director       = Marius Holst
| producer       = Petter J. Borgli
| writer         = Lars Saabye Christensen Marius Holst
| starring       = Martin Dahl Garfalk
| music          = 
| cinematography = Philip Øgaard
| editing        = 
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}
Cross my Heart and Hope to Die ( ) is a 1994 Norwegian film directed by Marius Holst. It is loosely based on Lars Saabye Christensens novel Gutten som ville være en av gutta.

==Cast==
* Martin Dahl Garfalk as Otto
* Trond Halbo as Johnny
* Jan Devo Kornstad as Frank
* Kjersti Holmen as Mother
* Reidar Sørensen as Father
* Bjørn Sundquist as Bulken
* Bjørn Floberg as Wiik
* Gisken Armand as Sager
* Ingar Helge Gimle as Gregers
* Per Oscarsson as Piano tuner
* Karl Bomann-Larsen as Judge

==Awards==
The film won the Blue Angel award and was nominated to the Golden Bear at the 45th Berlin International Film Festival.    It was the Norwegian entry to the Academy Award for Best Foreign Language Film in 1995.

==References==
 

==External links==
*  
*  
*   at the Norwegian Film Institute

 

 
 
 
 
 


 