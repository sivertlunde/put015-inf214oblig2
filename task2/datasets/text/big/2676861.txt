Kinky Boots (film)
 
{{Infobox film
| name           = Kinky Boots
| image          = Kinky Boots (movie poster).jpg
| image_size     = 
| alt            = A long thigh-high red boot, fills the foreground on the left, three people stand in the distance on the back right 
| caption        = Theatrical release poster 
| director       = Julian Jarrold
| producer       = Suzanne Mackie Nick Barton Peter Ettedgui Mairi Brett
| writer         = Geoff Deane Tim Firth
| starring       = Joel Edgerton Chiwetel Ejiofor Sarah-Jane Potts Jemima Rooper Linda Bassett Nick Frost Robert Pugh
| music          = Adrian Johnston
| cinematography = Eigil Bryld
| editing        = Emma E. Hickox
| studio         = BBC Films
| distributor    = Miramax Films Touchstone Pictures
| released       =  
| runtime        = 106 minutes   
| country        = United Kingdom United States
| language       = English
| budget         =  
| gross          = $9,950,133  
}}
Kinky Boots is a 2005 British-American comedy-drama film written by Geoff Deane and Tim Firth, and directed by Julian Jarrold. Based on a true story, the movie tells of a struggling British shoe factorys young, strait-laced owner, Charlie, who forms an unlikely partnership with Lola, a drag queen, to save the business. Charlie develops a plan to produce custom footwear for drag queens, rather than the mens dress shoes that his firm is known for, and in the process, he and Lola discover that they are not so different after all.

==Plot== football is being explained the value of shoes and the family livelihood as shoemakers.  

The film cuts to present day in the town of Northampton, in the East Midlands of England, where Charlie Price, the boy more interested in football, is attempting to save the family shoe factory, which has been floundering since his father died. While on a business trip to London to sell the companys extra stock, Charlie encounters a woman being harassed by drunken hoodlums and intervenes to his detriment. He wakes up back stage, in the dressing room of Lola, a sassy drag queen performer and alter ego of Simon (the pier boy). Charlie is intrigued when he sees that drag queens high heels snap easily since they have only womens shoes to wear, rather than those that can support the weight of a larger male body frame. Back in Northampton, while in the process of making his workers redundant, one employee, Lauren, gives Charlie the idea of looking for a niche market product to save the business. Charlie then recruits Lauren to assist him in designing a high-heeled boot for Lola, initially as a thank-you, but later as a means of finding a niche to save the factory.

When their initial designs are met with scorn by Lola, Charlie and Lauren bring her on as a consultant. The road is initially bumpy: many of the male employees are uncomfortable with Lolas presence and the new direction, and Charlies relationship with his fiancée, Nicola, begins to deteriorate as she encourages him to sell the company. Although things improve when Lola turns down her personality and starts making friends, matters take a turn for the worst when Charlie is invited to showcase the new boots in Milan; the strain he puts on his employees causes most of them, including Lola, to walk out.  

Charlies fiancée arrives at the factory, furious that he took out a second mortgage on their house to save the company. Nicola insists that he sells the company, but Charlie is determined to save it and the jobs of his employees. The argument (which ends with Nicola leaving Charlie) is broadcast on the factorys PA system, which is overheard by Lauren and Lolas bitterest opponent at the factory, a chauvinistic male worker who turns over a new leaf after Lola had graciously allowed him to win the arm wrestling match he was the champion in, and rallies the factory workers to make the boots in time for Charlie and Lauren to get to Milan. After arriving in Milan with no one to model the boots, Charlie is forced to go onstage and model the boots himself as the ultimate symbol of his dedication to his workers and his acceptance of Lola. After tripping and ultimately falling flat on his face, Lola and her posse of drag queens arrive, put on a spectacular runway show, and save the day. 

In the films denouement, Lola headlines her own show and sings a song in honor of the "kinky boots factory" of Northampton. Most of the key workers are in attendance and enjoying themselves, including Charlie and Lauren, who have become a couple.

==Cast==
* Joel Edgerton as Charlie
**Sebastian Hurst-Palmer as Young Charlie
* Chiwetel Ejiofor as Lola
** Courtney Philip as Young Lola
* Sarah-Jane Potts as Lauren
* Jemima Rooper as Nicola
* Nick Frost as Don
* Linda Bassett as Melanie
* Robert Pugh as Harold Price
* Ewan Hooper as George
* Stephen Marcus as Big Mike
* Mona Hammond as Pat
* Kellie Bright as Jeannie
* Joanna Scanlan as Trish
* Geoffrey Streatfeild as Richard Bailey
* Leo Bill as Harry Sampson

==Background== documentary series Trouble at the Top, broadcast on 24 February 1999, inspired the film   the former featured Steve Pateman struggling with possible closure of W.J. Brooks Ltd, a family-controlled Earls Barton, Northamptonshire shoe factory, that soon catered to the male market for fetish footwear under the "Divine" brand.   

==Soundtrack==
The Kinky Boots: Original Soundtrack was released on April 11, 2006 by Hollywood Records, Inc. 
# "Whatever Lola Wants" – Chiwetel Ejiofor (2:12)
# "In These Shoes" – Kirsty MacColl (3:39)
# "I Want to Be Evil" – Chiwetel Ejiofor (2:34)
# "Mr. Big Stuff" – Lyn Collins (4:00)
# "Its a Mans Mans Mans World" – James Brown (3:17)
# "I Put a Spell on You" – Nina Simone (2:36)
# "The Prettiest Star" – David Bowie (3:09)
# "Together We Are Beautiful" – Chiwetel Ejiofor (4:10)
# "Yes Sir I Can Boogie" – Chiwetel Ejiofor (4:20)
# "Wild Is the Wind (song) | Wild Is the Wind" – Nina Simone (6:59)
# "The Red Shoes" – Adrian Johnston (4:26)
# "Steel Shank" – Adrian Johnston (3:39)
# "Free to Walk" – Adrian Johnston (3:39)

The following songs are included in the film but are not on the Original Soundtrack:
* “My Heart Belongs to Daddy – Chiwetel Ejiofor
* “These Boots Are Made for Walkin” – Chiwetel Ejiofor
* "Summer Holiday (song) | Summer Holiday" – Jemima Rooper

==Release==
===Critical response   ===
The film received mixed reviews on release, with critics decrying the "formulaic Britcom plot". Critics aggregated by Rotten Tomatoes gave the film a combined rating of 57% based on 107 reviews.   

===Box office===
The film earned a total of $9,941,428 internationally.   

==Awards==
Ejiofor was nominated for a Golden Globe Award for Best Actor – Motion Picture Musical or Comedy.

==Musical adaptation== Kinky Boots Broadway in April 2013, following an out-of-town try-out at the Bank of America Theatre in Chicago; Cyndi Lauper, music and lyrics; and Harvey Fierstein, co-wrote book.  Director Jerry Mitchell is also the choreographer. 
 Billy Porter and Stark Sands.  At the 67th Tony Awards (2013) won six Tony Awards, including Tony Award for Best Original Score (Lauper, first sole female winner), Best Actor (Porter) and Best Musical. 

==See also==
* Cross-dressing in film and television
* Transvestism
* Fetishism

==References   ==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 