Junior Mandrake
 
{{Infobox film
| name           = Junior Mandrake
| image          = Junior Mandrake.jpg
| image_size     = 175px
| alt            = 
| caption        =  Ali Akbar
| producer       = Mummy Century , Shameer Thukalil
| writer         = Benny P Nayarambalam
| narrator       = 
| starring       = Jagathy Sreekumar  Jagadeesh  Rajan P. Dev Berny Ignatius
| lyrics         = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Ali Akbar. Jagathy Sreekumar, Jagadish and Rajan P. Dev plays the lead roles in this film. Though the film did excellent business in box office, it gained the present cult following release of the movie in Home videos and Cable television networks.

A sequel of Junior Mandrake namely "Senior Mandrake" was released in 2010 by the same director, but it bombed at the box office due to poor script.

== Plot ==

Nambiar (Rajan P Dev), a building contractor, is a die-hard atheist. However he is plagued by many a problem in his contracting business, particularly due to the conspiracies plotted by his competitor, Shankara Pillai (Janardhanan). Compelled by his wife Sudharma, who is devoted to God, and his children Pradeep and Sandeep (Jagadeesh and Kalabhavan Navas respectively) Nambiar consents to consult an astrologer Thanku Panikkar (M.S.Thrippunithura) to find if there is any supernatural cause behind their problems. Thanku Asan ascertains that there is an evil spirit residing in the bust of "Junior Mandrake" (in fact a bust in the shape of the antogonist in the movie) kept in their house.  Nambiar had been entrusted to handover this bust to someone in the airport in India when he returned from Singapore, but no one had showed up to receive it. Thanku Panikkar prescribes the evil spirit can be got rid of only by presenting the bust to someone who may receive it "with pleasure", who in turn will be taken over by the evil spirit. Nambiar manages to present this bust to his adversary Shankara Pillai on his birthday party, and Thanku Panikkar is apparently proved right as the bad luck of Nambiar is now shifted to Shankara Pillai, also turning Nambiar into an ardent believer in God in the process. 

Shankara Pillai comes to know about this soon and tries to get rid of the bust by any means. The bust make various journeys back and forth, finally ending up with Omana Kuttan (Jagathy Sreekumar), driver of Menon (Paravoor Bharathan) who is the father in law of Nambiar. Omana Kuttan soon chances to discover that there is in fact a treasure of precious stones hidden inside this bust. He takes some stones with him, and keeps the rest over the roof of the rented house he was living in. By the time he came back after selling these stones the house had been rented out by the owner as a police station. Omana Kuttan tries to retrieve his treasure from the roof of building by various means, generating some comedy in the process, but ends up in a mental hospital during this endeavor. At the same time the owner of the treasure, a bald headed underworld don in whose shape the bust was made, who was so far in jail in Singapore, is now out searching for it. He tracks down all those involved in the journey of the bust, starting with Shankara Pillai, Nambiar, his two sons, Menon and Omana Kuttan. In the climax the villain ends up in police custody in his attempt to recover the treasure from the police station by a vicious plan, and the treasure goes to the government. The story ends with a happy note with Pradeep joined with the daughter of Shankara Pillai with whom who was in love.

== Cast ==
* Jagathy Sreekumar as Omana Kuttan
* Jagadish as Pradeep
* Rajan P. Dev as Nambiar
* Kalabhavan Navas as Sandeep
* Reena  as  Sudharma  
* Paravoor Bharathan as Menon Janardhanan as Shankara Pillai
* M.S.Thrippunithura as Thanku Asan
* Cochin Haneefa as Police Officer Kalpana as Vandana

== External links ==
*  

 
 
 
 