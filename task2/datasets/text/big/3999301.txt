Phenomena (film)
 
{{Infobox film
| name           = Phenomena
| image          = Phenomena-poster.jpg
| caption        = Italian poster
| director       = Dario Argento
| producer       = Angelo Jacono Dario Argento
| writer         = Dario Argento Franco Ferrini
| starring       = Jennifer Connelly Daria Nicolodi Dalila Di Lazzaro Donald Pleasence Patrick Bauchau
| music          = Claudio Simonetti Fabio Pignatelli Bill Wyman Simon Boswell (solo voice theme song Pina Magri)
| cinematography = Romano Albani
| editing        = Franco Fraticelli
| distributor    = Titanus New Line Cinema  (USA, theatrical)   Anchor Bay Entertainment  (USA, DVD) 
| released       = January 31, 1985 (Italy)  August 2, 1985 (USA)
| runtime        = 110 minutes Edited version: 82 minutes
| country        = Italy Italian German German English English
| budget         = $3,800,000 (estimated)
| preceded_by    =
| followed_by    =
}} Italian horror Swiss boarding school who discovers she has psychic powers that allow her to communicate with insects, and uses them to pursue a serial killer who is butchering young women at and around the school.
 heavy metal horror video Clock Tower.

==Plot==
After missing the bus in the Swiss countryside, a tourist, Vera Brandt (Fiore Argento), tries looking for help. She comes across a home, and upon entering, she is attacked by an unknown stranger, who then proceeds to chase and behead her. Eight months later, Jennifer Corvino (Jennifer Connelly), arrives at the Swiss Richard Wagner Academy for Girls, chaperoned by Frau Brückner (Daria Nicolodi), who places her with roommate Sophie (Federica Mastroianni). While sleepwalking through the academy and out onto the roof, Jennifer witnesses a student being murdered. She awakens and falls, fleeing and eventually becoming lost in the woods. Forensic entomologist John McGregor (Donald Pleasence)s chimpanzee attendant Inga (Tanga) finds her and leads her to him. Witnessing her apparent interaction with his insects, McGregor comes to believe she has a special gift for telepathy with them. Inspector Rudolf Geiger (Patrick Bauchau) is also in the case alongside McGregor. Back at the academy, the headmistress has Jennifer medically tested via EEG for her sleepwalking. The procedure makes Jennifer uneasy when she gets brief visions of the previous nights events. The headmistress then has Sophie look after Jennifer in case she sleepwalks again. 
 Great Sarcophagus fly, which is drawn to decaying human flesh. He explains that the maggots Jennifer found are actually the larvae of this insect and theorises that the larvaes presence on the glove indicates that the killer has been keeping his victims close to him post-mortem, unintentionally collecting the larvae on himself whilst physically interacting with the victims. He soberly warns Jennifer that they are dealing with a psychopath, based upon the killers evident desire to retain his headless victims until they rot to bone. Later at the academy, when the other students taunt Jennifer for her alleged connection to insects, she summons a swarm of flies which covers the entire building, then faints. Convinced that Jennifer is "diabolic" and possibly responsible for the killings, the headmistress arranges for her to be transferred to a mental hospital for the criminally insane. However, Jennifer flees to McGregors home just in time to evade the transfer.
 Great Sarcophagus fly and suggests she use it to help track the murderer. When the fly leads her to the same house that Vera had found earlier in the movie, Jennifer is told to leave when the real estate agent catches her, assuming her to be a thief. Unbeknownst to the both of them, a rotting hand is shown underneath the floorboards. Geiger arrives as well and gets some information from the agent before leaving. Later that night, McGregor is murdered in his home after Inga is distracted and locked outside. With nowhere left to go, Jennifer calls her fathers lawyer Morris Shapiro (Mario Donatone) for help. He alerts Brückner, who finds Jennifer and offers to let the girl stay at her house overnight. Once there, Brückners behavior changes. She advises Jennifer to take some pills before she goes to bed; when Jennifer does so, she becomes sick, assuming that the pills were poisonous and coughs them up. After leaving the bathroom, she attempts to call Morris, but gets knocked unconscious with a piece of wood by Brückner, who incarcerates her in the house. Geiger arrives to the house and is apparently attacked by Brückner, alerting Jennifer that something is wrong.

After coming to a brief time later, Jennifer engineers her escape through a large hole in the floor that leads through a tunnel to a dungeon and into a basement. She falls in a pool infested with maggots and dead bodies, whilst Geiger is also present in the room, though above Jennifer in the pool, struggling to free himself from some chains attached to his wrists. Brückner appears and teases Jennifer for a moment, but Geiger frees himself from the chains and holds Brückner just enough to let Jennifer escape. Following her escape from the pool room, she passes a room from which she hears sobbing, subsequently finding Brückners son alone and crying in the back corner of this room. When the child (Davide Marotta) turns around, it is revealed that he has a hideously deformed face, being the result of a rape (hence the mirrors were covered as he does not like seeing himself). He chases Jennifer until she gets outside and onto a motorboat. Just as she finally manages to get the motor started after many attempts and begins to leave the dock, he rushes from the trees and leaps onto the mototboat with his lance in hand, in a desperate attempt to kill her, but she summons a swarm of flies that attack him, causing him to fall into the water. Jennifer is also forced to jump into the water as the motorboat explodes, whereupon the child grabs her, as he is not yet dead. Rising into the flaming waters, he is eventually killed. Jennifer reaches the shore just as Morris appears. However Brückner reappears and decapitates him. She then leans over Jennifer threatening her with the same fate before revealing that she was the one who murdered McGregor and Geiger out of fear that harm would have befallen her son. Suddenly, Inga attacks Brückner and kills her with a razor. With the ordeal over, Jennifer and the chimp embrace.

==Cast==
* Jennifer Connelly as Jennifer Corvino
* Daria Nicolodi as Frau Brückner
* Dalila Di Lazzaro as Headmistress
* Patrick Bauchau as Inspector Rudolf Geiger
* Donald Pleasence as Professor John McGregor
* Fiore Argento as Vera Brandt
* Federica Mastroianni as Sophie
* Fiorenza Tessari as Gisela Sulzer
* Mario Donatone as Morris Shapiro
* Francesca Ottaviani as Nurse
* Michele Soavi as Kurt, Geigers Assistant
* Franco Trevisi as Real Estate Agent
* Davide Marotta as Frau Brückners son

==Distribution==
For its U.S. release, Phenomena was retitled as Creepers by New Line Cinema and heavily edited to remove nearly thirty minutes of footage and went to U.S. theaters and drive-ins in August of 1985. The murder sequences were shortened by several seconds to remove gore, a sequence where the second victim is spotted and chased by the killer was removed, and two lengthy scenes (which make up the bulk of the missing footage) involving Jennifer Connellys character telling her roommate the story about how her mother abandoned the family on Christmas Day (as Argentos own mother had done) and her character receiving a brain scan after the second murder were removed for the purposes of speeding up the flow of the film. The film was a box office hit in Italy gaining 515,034 admissions, and grossed €203,657 in Spain. 

Anchor Bay released the film to DVD in 1999, restoring the films proper title and all missing scenes from the original US release. The original cut (or Integral cut) of the film is available in Europe, on Region 2 DVD. The Integral cut (or Integral Hard, as it is known in Japan) never existed in English, but only in the dubbed Italian version, and adds only a few seconds of cut footage to some scenes, mostly of B-roll material. 

On March 7, 2011, Phenomena was released on Blu-ray in the UK, by film company Arrow Films|Arrow. There are many special features on the disc, such as "The Making of Phenomena" and interviews with the director and cast, excluding Jennifer Connelly.

==Soundtrack== Goblin and Simon Boswell, providing some of his earliest film scoring work. Songs by Iron Maiden, Motörhead, Bill Wyman and Andi Sexgang are also featured. The solo soprano voice in the theme song "Phenomena" as well as in "Jennifers Friends" and "The Wind" is Pina Magri.
 Patrick , and a song by Frankie Goes to Hollywood. "Valley Bolero", a Bill Wyman cue that appears when Sophie is killed, has not been released on any version. The film used two cues from Dawn of the Dead during television broadcasts.

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 