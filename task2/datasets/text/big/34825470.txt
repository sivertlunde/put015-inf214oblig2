Listening (film)
{{Infobox film
| name           = Listening
| image          =
| caption        =
| director       = Kenneth Branagh
| producer       = Simon Moseley
| writer         = Kenneth Branagh
| based on       =
| narrator       =
| starring       = Frances Barber Paul McGann
| music          = Alex Thomson
| editing        = Neil Farrell
| distributor    =
| released       =  
| runtime        = 25 minutes
| country        = United Kingdom
| language       = English
| budget         =
}}

Listening is a 2003 short film written and directed by Kenneth Branagh, starring Frances Barber and Paul McGann.  The film won Best Director, Short Film at the Rhode Island International Film Festival and was fourth runner-up for Best Short Film at the Seattle International Film Festival. 

==Plot==
The plot follows the two main characters, a woman played by Frances Barber and a man played by Paul McGann, who meet and develop a romance while staying at a spa where talking is not allowed.

==References==
 

==External links==
*  

 

 
 
 
 

 