Five Weeks in a Balloon (film)
{{Infobox film
| name = Five Weeks in a Balloon
| image = Fiveweekspos.jpg
| caption = Original film poster
| director = Irwin Allen
| producer = Irwin Allen
| based on = Five Weeks in a Balloon
| writer = Jules Verne (novel) Charles Bennett Fabian Barbara Eden Cedric Hardwicke Peter Lorre Richard Haydn Barbara Luna Billy Gilbert Herbert Marshall Reginald Owen Henry Daniell Mike Mazurki Alan Caillou Ben Astar Raymond Bailey Chester the Chimp
| music = Paul Sawtell
| cinematography = Winton Hoch, ASC
| editing = George Boemler
| studio = Cambridge Productions
| distributor = 20th Century Fox
| released =  
| runtime = 101 minutes
| country = United States
| language = English
| budget = $2.34 million 
| gross = $1.2 million 
}}
 novel of the same name by Jules Verne filmed in CinemaScope.  It was produced and directed by Irwin Allen; his last feature film in the 1960s before moving to producing several science fiction television series.  Though set in Africa, it was filmed in California. Balloonist Donald Piccard acted as the films technical advisor. For visual effects, a model of the balloon was used as well as a full-sized unicorn gondola hung on a crane.

==Plot==
 
 

The film begins with the flight of the balloon Jupiter, invented by Professor Fergusson (Cedric Hardwicke). In the unicorn-shaped gondola, the passengers Chiddingfold (Ronald Long) and Sir Henry Vining (Richard Haydn) scream in horror as the balloon rapidly descends, but the Professor remains calm as he planned for this to happen. He then signals the pilot Jacques (Fabian Forte|Fabian) to ascend the balloon, who explains how balloon is able to ascend and descend without the loss of gas or ballast. The balloon successfully lands and attracts a crowd, but Sir Henry and the other passenger are dissatisfied after the seemingly disastrous descent. Sir Henry, the president of the Royal Geographic Society, refuses to fund the Professors plan to explore east Africa, while Chiddingfold leaves claiming he has an "appointment". The Professor seems out of financial backing for his plans, but an American journalist talks to Fergusson about having his nephew and star reporter Donald OShea (Red Buttons) as part of Fergussons plan to explore east Africa.

When he arrives at the Prime Ministers office in Zanzibar, he is given the mission to use his craft to claim areas of uncharted west Africa for the Crown before slave traders make their claim to the territory. Fergusson agrees. The following day, Consul tells the bad news that the slave traders know of his plan and would reach the river Volta in six weeks, leaving him with five.  To make matters worse, the Queen has sent Sir Henry Vining to accompany him. He wants to be referred to as General Vining and proclaims himself the "expert on Africa."

Meanwhile in the marketplace, Jacques finds OShea rescuing a slave girl named Makia (Barbara Luna) and they bring her along, and they go to the Consuls office. Consul Townsend orders Makia to be returned to her owner, but she fights the slave trader and leaves on a horse. The people in the marketplace become outraged and threaten to destroy Jupiter. OShea begins to be viewed as a troublemaker and as the balloon takes off he is shocked to know that the plans of the journey have been changed. The balloon lands in a forest and they have dinner. As they converse at the table they notice their food is being taken away and find Makia hiding under the table. She says she hid up in the crows nest of the balloon and will not leave OShea, who does not want her and offers her to the others on board. Disappointed she tries to leave into the dangerous wilderness only to be stopped and eventually agrees to be a passenger aboard. Meanwhile, a wild chimpanzee walks out of the forest and joins the crew. As the rest of the crew sleeps, Jacques and the chimp drink coffee. Makia comes out of the gondola unable to sleep in fear of the wilderness and suggests Jacques to buy her. Jacques explains how slavery is uncivilized and explains marriage. Lightning is heard and they take off the balloon.
 Moon God. The crew have dinner in the Sultans palace, where a slave trader named Ahmed (Peter Lorre) comes in with a kidnapped American woman (Barbara Eden). Ahmed points at the moon emerging in the clouds, to which the Sultan (Billy Gilbert) proclaims them as fakes. The crew escape quickly, but Ahmed climbs a ladder aboard. When they land they find out that Ahmed has stolen diamonds and medals from the Sultan. The American woman introduces herself as Susan Gale explains that she is a missionary. The professor explains they are headed for the Volta river to claim the land and stop slavery, but claims OShea doesnt care about the slave trade. He tries to explain that he is only a journalist but Susan interrupts saying "trafficking human lives is everyones concern, either youre for it or against it," to which Ahmed says "Im for it."

The balloon continues its progress and lands near a forest. Susan is still angry at Donald and still thinks he doesnt care about slavery but Makia explains how he saved her from slavers. Meanwhile, OShea goes out hunting with a gun provided by General Vyning. He walks into the forest and sees a lion, but when he tries to shoot it he realizes that the gun is unloaded. He then falls into a trap set by natives, who free him, but when he runs away they chase after him with spears. Quickly the crew takes the balloon off as a spear narrowly misses them inside the gondola. As they fly again, Donald and Susan build a relationship. After they land again, however, OShea accidentally hammers the anchor away, setting the balloon free. The anchor luckily catches the top of a tree, but everyone including Susan becomes disappointed at him. The Professor and General consider offering OShea to the slavers, as they tell how he was only aboard to carry testimony for Americans, but now they had a less troublesome Susan Gale. However, OShea wins back the crews trust when he spots out a sandstorm. The balloon lands in an oasis and the crew relaxes. Ahmed, however, is ordered to work and thinks he is being treated as a slave, telling Jacques "Im not a slave, I sell them!" A gunshot is heard and the Sheiks men on horseback come to arrest the Professor, Vyning, Makia and Susan, taking them to Timbuktu. Ahmed and Jacques stay the gondola while OShea, who was picking dates, and the chimp stay in the trees until the men leave.

In Timbuktu, the arrested crew are set to die as infidels and Makia be sold as a slave under the order of the Sheik Ageiba. Disguised as Arabs, Jacques, Ahmed and Donald purchase Makia from a slave trader (also played by Billy Gilbert). Makia tells of the plan to kill the rest of the crew and they fly their balloon to the top of a tower to save them. After defeating many of the men, they take off, but a sword is thrown onto the balloons envelope, causing a slow leak to occur. The Professor, now knowing the slavers have less than two days, feels certain defeat, but Donald suggests they fly over night.

The following day, they reach the Volta river, but sword plugged on the envelope starts to rip away, causing the balloon to descend. Everything is thrown overboard to help the balloon gain some lift as they fly towards a bridge, but see the slavers. The head slaver shoots the balloon causing it to deflate as the crew climb into the crows nest. The anchor is thrown onto the bridge and the gondola is released, causing the balloon to ascend and break the bridge, causing most of the slavers to drown. The head slaver, however, survives with their flag. The balloons crew swim across the river, except for Ahmed, who remains seated on the balloons drifting envelope because he cant swim. Donald goes back to retrieve the Union Jack as the envelope raft tumbles over the waterfall. Ahmed tells Donald to jump with the flag. He then kills the slave trader by throwing a dart into his chest, sending him falling into the water. The crew are finally proud that OShea had done something triumphant, but he falls into the water. Susan tries to save him and falls in the water and they kiss, while Jacques and Makia also kiss. General Vyning admits to Professor Fergusson that he was wrong about his balloon. The film ends with the chimpanzee finding a companion for itself.

==Cast==
* Red Buttons as Donald OShay Fabian as Jacques
* Barbara Eden as Susan Gale
* Cedric Hardwicke as Fergusson
* Peter Lorre as Ahmed
* Richard Haydn as Sir Henry Vining
* Barbara Luna as Makia
* Billy Gilbert as Sultan
* Herbert Marshall as The Prime Minister
* Reginald Owen as Consul
* Henry Daniell as Sheik Ageiba
* Mike Mazurki as Slave Captain
* Alan Caillou as Inspector
* Ben Astar as Myanga
* Raymond Bailey as Randolph
* Chester the Chimp as "The Duchess"

==Production==
In 1955 Tony Curtis announced plans to produce and star in a version of the novel for his own company and hired Kathleen Dormer to write a script. Drama: Glenn Ford, Donna Reed Join in Quacks Expose; Lassie Goes Big-Screen
Scheuer, Philip K. Los Angeles Times (1923-Current File)   18 Nov 1955: B9.   In June 1961 Irwin Allen announced he had secured rights to the novel after six years of negotiation and would likely make the film at 20th Century Fox. FIRST VERNE BOOK TO BECOME MOVIE: Irwin Allen Acquires Five Weeks In a Balloon
By HOWARD THOMPSON. New York Times (1923-Current file)   07 June 1961: 47.  

One of the themes of Five Weeks in a Balloon is a race.  Verne’s novel features the Professor attempting to make discoveries ahead of other explorers whilst Allen’s film has the Professor trying to beat the claims of a slave trading expedition. There was also a race between two producers attempting to be the first to film the story; Irwin Allen, producer of Five Weeks in a Balloon, and the Woolner Brothers, who in 1961 made Flight of the Lost Balloon directed by Nathan Juran.  Though Verne’s novel was in the public domain, Fox and Allen brought legal pressure against the Woolners to drop all mention of Jules Verne from their film.  The Woolner’s also were stopped from using another title for the film, Cleopatra and the Cyclops, intended to exploit the hype of Fox’s own Cleopatra (1963 film)|Cleopatra.   Allens film is played much more for comedy than Jurans film.

In Vernes novel and the Woolner Brothers film the balloon was named the Victoria.  Allens film renames it the Jupiter with Allen giving the name Jupiter II to the spaceship in Lost in Space.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 