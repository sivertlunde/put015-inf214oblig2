Villa-Lobos: A Life of Passion
 
{{Infobox film
| name           = Villa-Lobos: A Life of Passion
| image          = Villa-Lobos A Life of Passion.jpg
| caption        = Theatrical release poster
| director       = Zelito Viana
| producer       = Zelito Viana
| writer         = Joaquim Assis
| starring       = Antônio Fagundes Marcos Palmeira Letícia Spiller
| music          = Silvio Barbato
| cinematography = Walter Carvalho
| editing        = Eduardo Escorel
| studio         = Mapa Filmes
| distributor    = Riofilme United International Pictures
| released       =  
| runtime        = 130 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = R$874,453  ($376,886)
}}

Villa-Lobos: A Life of Passion ( ) is a 2000 Brazilian drama film directed by Zelito Viana. It was entered into the 22nd Moscow International Film Festival.    It won the 2nd Grande Prêmio Cinema Brasil for Best Score,  and the 5th Brazilian Film Festival of Miami for Best Art Direction.  Its production started in 1997 as Villa Lobos - História de Uma Paixão. 

==Cast==
* Antônio Fagundes as Heitor Villa-Lobos
** Marcos Palmeira as young Heitor Villa-Lobos
* Letícia Spiller as Mindinha
* Ana Beatriz Nogueira as Lucília
* José Wilker as Donizetti
* Marieta Severo as Noemia
* Othon Bastos as Raul
* Emílio de Melo as Arthur Rubinstein
* Antonio Pitanga as Joaquim

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 
 