Yella (film)
 
{{Infobox film
| name           = Yella
| image          = File:Yellaposter.jpg
| image size     =
| caption        = Release poster Christian Petzold
| producer       = Florian Koerner von Gustorf   Andreas Schreitmüller   Caroline von Senden   Michael Weber Christian Petzold
| narrator       =
| starring       = Nina Hoss   Devid Striesow   Hinnerk Schönemann   Burghart Klaußner   Barbara Auer   Christian Redl   Selin Bademsoy   Wanja Mues
| music          = Stefan Will
| cinematography = Hans Fromm
| editing        = Bettina Böhler
| distributor    = Schramm Film Koerner & Weber
| released       =  
| runtime        = 89 min
| country        = Germany
| language       = German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} thriller film Christian Petzold.  The film is an unofficial remake of the 1962 film Carnival of Souls. 

==Plot==

 

==Cast==
* Nina Hoss - Yella
* Devid Striesow - Philipp
* Hinnerk Schönemann - Ben
* Burghart Klaußner - Dr. Gunthen
* Barbara Auer - Barbara Gunthen Christian Redl - Yellas father
* Selin Barbara Petzold - Dr. Gunthens daughter
* Wanja Mues - Sprenger

==Production==
The films title was inspired by actress Yella Rottländer who was the female lead in the 1974 film, Alice in the Cities. 
Yella was filmed in Germany in Hannover, Lower Saxony and, Wittenberge, Brandenburg. 
 Christian Petzolds "Gespenster" trilogy. The first being The State I Am and the second being Gespenster. 

==Release==

 

==Reception==
 
Critical reception for the film gas been mostly positive.

Roger Ebert praised the film awarding it 3  1/2  / 4 stars sating, "The writer-director, Christian Petzold, uses a spare, straightforward visual style for the most part, except for those cutaways to trees blowing in the wind whenever we heard the harsh bird cry. He trusts his story and characters. And he trusts us to follow the business deals and become engrossed in the intrigue. I did". Ebert also praised male leads Striesow and Schoenemann calling the similarities of both characters physical presences as being "unsettling". 

It currently has an 81% on the review website Rotten Tomatoes, the sites consensus saying "Chilly and haunting, Yellas atmosphere gets under the skin". 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 