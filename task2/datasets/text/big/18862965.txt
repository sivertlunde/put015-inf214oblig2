Badhaai Ho Badhaai
 
 
{{Infobox film
| name           = Badhaai Ho Badhaai
| image          = Badhaai_Ho_Badhaai.jpg
| director       = Satish Kaushik
| producer       = Anil Kapoor
| writer         = Kavita Choudhary
| story          = Vikraman
| starring       = Anil Kapoor  Shilpa Shetty  Keerti Reddy
| editing        = Sanjay Verma
| cinematography = Rajiv Jain
| country        = India
| music          = Anu Malik
| distributor    = Kapoor & Kaushik Entertainment Pvt.Ltd
| released       = 14 June 2002
| runtime        =
| language       = Hindi
| imdb_id        =
| awards         =
| budget         =
}}
 The Nutty Professor. 

It was shot over 45 days in Delhi. 

==Plot==
The DSouza and the Chaddha families are neighbors and have been good friends for as long as they can remember, and despite their differing religions and cultural beliefs, celebrate Diwali and Christmas with cheer and gusto. Then Anjali Chaddha and Anthony DSouza fall in love, and this brings an end to the friendship. Anjali and Anthony elope, marry, and re-locate to another place, leaving the two families to live with bitterness the rest of their lives. Twenty-seven years later, a young man claiming to be the son of Anjali and Anthony comes to visit his paternal and maternal grandparents, and is cold-shouldered by both families. He persists and wins over the hearts of his grandparents on both sides, but before he can win over his Jassi mama and Moses chacha, they find out that Raja is not who he claims to be. In actuality, Shilpa Shettys character is the granddaughter.

==Cast==
* Anil Kapoor: Raja
* Shilpa Shetty: Banto Betty / Tina
* Keerti Reddy: Florence DSouza
* Amrish Puri: Mr Chaddha
* Farida Jalal: Mrs Chaddha
* Kader Khan: Ghuman Singh Rathod
* Anang Desai: Moses DSouza
* Rohini Hattangadi: Rosy DSouza
* Vinay Jain: Ranjit Chaddha
* Dinesh Kaushik: Banto Bettys father
* Mushtaq Khan: Bali
* Suresh Menon: Lucky Iyer
* Govind Namdeo: Jassi Chaddha
* Hemant Pandey: Writer
* K. Vishwanath: Mr DSouza

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Raag Banke"
| Shaan (singer)|Shaan, Alka Yagnik
|-
| 2
| "Teri Zindagi Mein Pyar Hai"
| KK (singer)|KK, Alka Yagnik
|-
| 3
| "Dil Bata Mere Dil Bata"
| Sonu Nigam
|-
| 4
| "Badhaai Ho Badhaai"
| Udit Narayan
|-
| 5
| "Jogan Jogan"
| Kunal Ganjawala, Preeti, Pinky
|-
| 6
| "Tehro Zaraa"
| Sonu Nigam, Alka Yagnik
|}

==References==
 

==External links==
*  

 
 
 
 
 

 