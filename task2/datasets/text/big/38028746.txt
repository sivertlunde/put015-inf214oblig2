Jaan Ki Kasam
{{Infobox film
| name           = Jaan Ki Kasam
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sushil Malik	 
| producer       = Javed Riaz
| writer         = Baba Khan
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Krishna Saathi Ganguly
| music          = Nadeem-Shravan
| cinematography = Deepak Duggal	
| editing        = Ashok Honda
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Sushil Malik and produced by Javed Riaz. It stars Krishna and Saathi Ganguly in pivotal roles. 
 

The movie is about how a family (Suresh Oberoi, Archana Puran Singh, Tanvee Sharma ) travelling by air meet with an accident. Tanvee is the only one who survives and starts to live on an isolated island where she meets Khurshid (Rajan). The story is about their love and survival as they grow up.

==Cast==
* Krishna...Rajan
* Saathi Ganguly...Meenu
*Tanvee Sharma.... As little Meenu and Kumars daughter
* Suresh Oberoi...Kumar
* Archana Puran Singh...Kumars Wife
* Raza Murad...Paras Seth
* Ranjeet...Jagdish
* Pramod Moutho...Bajrangi
* Avtar Gill...Sher Khan
* Vikas Anand...Bholu Kaka

==Soundtrack==

The Soundtrack Of Jaan Ki Kasam Is Composed By The Music Duo Nadeem Shravan.The Song Lyrics Planned By Sameer.

{| class="wikitable "
! # !! Title !! Singer(s)
|-
| 1
| "Barsaat Ho Rahi, Barsaat Hone De"
| Kumar Sanu, Anuradha Paudwal
|-
| 2
| "Cham Cham Chamke Chandni"
| Anuradha Puadwal
|-
| 3
| "I Just Call To Say I Love You"
| Udit Narayan, Anuradha Paudwal
|-
| 4
| "Jo Hum Na Milenge, To Gul Na Khilenge"
| Kumar Sanu, Anuradha Paudwal
|-
| 5
| "So Ja Chup Ho Ja"
| Kumar Sanu, Anuradha Paudwal
|}

==References==
 

==External links==
* 

 
 
 
 


 