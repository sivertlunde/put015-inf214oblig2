The Lost Shoe
{{Infobox film
| name           = The Lost Shoe
| image          = 
| image_size     = 
| caption        =  Ludwig Berger 
| producer       = Erich Pommer
| writer         = Clemens Brentano   E.T.A. Hoffmann   Ludwig Berger 
| narrator       =  Paul Hartmann   Heinrich George
| music          = Guido Bagier
| editing        =
| cinematography = Otto Baecker   Günther Krampf
| studio         = Decla-Bioscop  UFA
| released       = 5 December 1923
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent fantasy Ludwig Berger Paul Hartmann UFA conglomerate.

==Cast==
* Helga Thomas as Marie  Paul Hartmann as Anselm Franz 
* Frida Richard as Patin 
* Hermann Thimig as Baron Steiß-Steßling 
* Lucie Höflich as Countess Benrat 
* Mady Christians as Violante 
* Olga Tschechowa as Estella 
* Max Gülstorff as Baron von Cucoli 
* Gertrud Eysoldt as Rauerin 
* Leonhard Haskel as Prince Habakuk XXVI 
* Werner Hollmann as Count Ekelmann 
* Georg John as Jon 
* Emilie Kurz as Princess Alloysia 
* Paula Conrada Schlenther as Princess Anastasia 
* Arnold Korff

==References==
 

==Bibliography==
* Hardt, Ursula. From Caligari to California: Erich Pommers life in the International Film Wars. Berghahn Books, 1996.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 