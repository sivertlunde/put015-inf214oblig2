Project Shadowchaser
{{Infobox film|
| name = Project Shadowchaser
| image =Project Shadowchaser.jpg
| caption =
| director = John Eyres
| writer = Stephen Lister
| producer = Geoff Griffiths and John Eyres
| starring = Frank Zagarino Martin Kove Meg Foster Paul Koslo Joss Ackland Ricco Ross
| cinematography = Alan M. Trow
| editing = Delhak Wreen
| music = Gary Pinder
| distributor = EGM
| released =  
| awards =
| budget =
| runtime = 97 min.
| language = English
| country = United States
}}

Project Shadowchaser, also known as Shadowchaser and Project: Shadowchaser, is a 1992 science fiction film by director John Eyres. It is the first installment in the Project Shadowchaser film series.

==Synopsis==

Armed terrorists, led by a super android, take over a high rise hospital with the presidents daughter as hostage. The FBI gets the hospital architect out of cryogenic prison and desperately asks for his help.

==Production==

Project Shadowchaser was shot at Pinewood Studios.  Most of the set was used from Alien 3.

==Release==

The film was released on videocassette in the summer of 1992 by Prism Pictures. To this very day, the film has never been released on R1 DVD and as of March 18, 2010, no plans have been announced to release the film onto a region 1 DVD.

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 