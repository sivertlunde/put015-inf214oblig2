Man in Demand on All Sides
{{Infobox film
| name           = Man in Demand on All Sides
| image          = Man in Demand on All Sides.jpg
| alt            = 
| caption        = 
| film name      = 
| director       =Miroslav Cikán
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       = Based on the play by Hans Jaray 
| starring       =  Ljuba Hermanová, Vera Ferbasová, and Lída Baarová
| narrator       = 
| music          = 
| cinematography = Jan Stallich
| editing        = 
| studio         = Lepka
| distributor    = 
| released       =1934  
| runtime        =85 minutes
| country        =Czechoslovakia
| language       = 
| budget         = 
| gross          = 
}}
Man in Demand on All Sides (Pán na roztrhání) is a 1934 Czechoslovak film, directed by Miroslav Cikán.  It stars  Ljuba Hermanová, Vera Ferbasová, and Lída Baarová.

==Cast==
*Ljuba Hermanová as Rita
*Vera Ferbasová as Slávka
*Lída Baarová as Eva Svobová
*Frantisek Cerný 		
*Frantisek Smolík as Karel Svarc
*Jan W. Speerger 		
*Nora Stallich 		
*Václav Trégl as Komorník Filip
*Josef Zezulka as Assistant in the Avion Factory

==References==
 

==External links==
*  at the Internet Movie Database

 
 
 
 
 