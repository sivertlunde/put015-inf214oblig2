Dream Girl (2009 film)
{{Infobox film
| name     = Dream Girl
| image    = 
| caption  = Movie poster for Dream Girl
| director = Ashok Pati
| producer = Ramesh Prasad
| based on =  
| writer   = Ashok Pati
| starring = Sabyasachi Mishra Priya Chowdhury Bijay Mohanty Aparajita Mohanty Mihir Das Jairam Samal
| music    = Prashant Padhi
| cinematography = Sitanshu Mahapatra
| editing  = Chandra Sekhar Misra Prasad Productions
| distributor = Rajshree
| released =  
| runtime  = 
| country  = India
| language = Oriya
}}
Dream Girl ( ) is a 2009 Indian Oriya film under Prasad Productions (P) Ltd., directed by Ashok Pati. {{cite web|url=http://www.expressbuzz.com/edition/story.aspx?Title=Dream+Girl&artid=hPgGyGfUBrI=&SectionID=sPqk7hE5Bqg=&MainSectionID=ngGbWGz5Z14=&SectionName=qREFy151z8Q5CNV7tjhyLw==&SEO=|title=Dream Girl oriya movie authered by Shyamhari Chakra
 Bhalobasa Bhalobasa.

== Synopsis ==
The story is about a boy and his family. The story is about all the fathers, all the relations, the so-called generation gap, and especially about a dream girl, who has changed the boy’s life. The story can be treated as a moral for each and every family in an entertaining way. 
When a baby is born and learns to walk, his father holds his hand and teaches him to walk. But what will be the situation if the father will not leave the hand even after 24 years? This is the situation of the boy in this story. He wants freedom in his life and wants to live by his own choice, but he always has to accept his father’s decision as his one and only choice. He is bent on marrying a girl of his own choice, and how successful he, it is depicted in the movie. {{cite web|url=http://dreamgirl.fullorissa.com/default.html|title=Dream Girl/Story review
}} 

==Cast==
*Sabyasachi Misra - Sanju
*Priya - Khusi
*Mihir Das
*Aparajita Mohanty
*Bijaya Mohanty
*Bobby Mishra
*Pintu Nanda
*Jairam Samal
*Sofia alam
*Debu Bose
*Saheb Singh
*Jina
*Jiban
*Chunga
*Roja
*Florence
*Chuni 

== Music ==
The music of the film composed  by Prashant Padhi. The tracks from the film include:
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="centre"
! Track
! Singer
! Lyricist
 
|- Aakhi Mora Khola Thae Kehi Jane 
| Tapu Mishra & Udit Narayan
| Arun Mantri	
|-
| Jahna Soichi Soi Padile Tara  
| Kumar Bapi & Tapu Mishra
| Arun Mantri	
|-  
| Priyare Priyare Najare Najare
| Krishna Beura 
| Nizam
|-  Sathire Sathire Pahili Pritire
| Tapu Misra 
| Nizam
 
|}

== References ==
 

== External links ==
*    
*  

 

 
 
 
 