Fed Up (film)
{{Infobox film
| name           = Fed Up
| image          = Fed Up poster.jpg
| caption        = Theatrical poster
| director       = Stephanie Soechtig	
| producer       = Stephanie Soechtig Sarah Olson Eve Marson Kristin Lazure Sarah Gibson Katie Couric
| writer         = Stephanie Soechtig Mark Monroe
| starring       =
| narrator       = Katie Couric
| music          = 
| cinematography = Scott Sinkler
| editing        = Brian David Lazarte Tina Nguyen Dan Swietlik
| studio         = Atlas Films
| distributor    = The Weinstein Company|RADiUS-TWC
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1,538,899
}} obesity in processed foods are an overlooked root of the problem, and points to the monied lobbying power of "Big Sugar" in blocking attempts to enact policies to address the issue.      

==Synopsis==
Fed Up shows how the first dietary guidelines issued by the U.S. government 30 years ago overlooked the role of dietary sugar in increasing risks of Obesity in the United States|obesity, diabetes, and associated ill-health outcomes, particularly in children.  Since these guidelines effectively condoned unlimited addition of sugar to foods consumed by children, sugar consumption has greatly increased, obesity has skyrocketed, and generations of children have grown up far fatter than their parents.  These children face impaired health and shorter lifespans as a result.    As the  relationship between the high-sugar diet and poor health has emerged, entrenched sugar industry interests with almost unlimited financial lobbying resources have beaten back attempts by parents, schools, states, and in Congress to provide a healthier diet for children.

==Production, premiere and release ==
American journalist and TV personality Katie Couric produced the documentary and is its narrator.   The film premiered in competition category of U.S. Documentary Competition program at the 2014 Sundance Film Festival on January 19, 2014.   

After its premiere at the 2014 Sundance Film Festival, The Weinstein Company|RADiUS-TWC acquired distribution rights of the film.   The film was released on May 9, 2014 in United States.   
==Reception==
Fed Up received a positive response from critics. Review aggregator Rotten Tomatoes reports that 82% of 61 film critics have given the film a positive review, with a rating average of 7 out of 10. 

In her review for LA Weekly, Amy Nicholson praised the film by saying that "Fed Up is poised to be the Inconvenient Truth of the health movement. (Which makes sense - producer Laurie David worked on both.)"  Geoffrey Berkshire in his review for Variety (magazine)|Variety said that "Stephanie Soechtigs documentary effectively gets the message out about Americas addiction to unhealthy food."  Robert Cameron Fowler from Indiewire in his review said that "Fed Up is a glossy package that gets its warnings across loud and clear: we need to change what we eat." 

Justin Lowe of The Hollywood Reporter praises the film as highly relevant, though overly-detailed -- the "Highly relevant film diminishes its central message with distracting details." 

As Manohla Dargis succinctly summarizes in her New York Times review 
:Recent research ... indicates that calories in fruit are not the same as those in soda, a conclusion that is part of the big picture in “Fed Up,” a very good advocacy documentary ...   A whirlwind of talking heads, found footage, scary statistics and cartoonish graphics, the movie is a fast, coolly incensed investigation into why people are getting fatter. It also includes some touching video self-portraits by some young people who belong to the almost 17 percent of children and adolescents, 2 to 19, who are considered obese.

==See also==

* List of films featuring diabetes

==References==
{{Reflist|refs=

   

   

   

   

}}

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 