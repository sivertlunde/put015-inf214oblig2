Tansen (film)
 
 
{{Infobox film
| name           =Tansen 
| image          = Tansen poster.jpg
| image_size     = 
| caption        = 
| director       =Jayant Desai
| producer       =Ranjit Movietone
| writer         = {{plain list|
* Jayant Desai  (screenplay) 
* Munshi Dil  (dialogue) 
* D. N. Madhok  (dialogue) 
}}
| narrator       = 
| starring       ={{plain list|
*K. L. Saigal
*Khursheed Bano
*Mubarak
*Nagendra
*Kamala Chatterjee
}}
| music          = Khemchand Prakash Pandit Indra  (lyrics) 
| cinematography = Gordhanbhai Patel 
| editing        = N.V. Morajkar
| distributor    = Ranjit Movietone
| released       =  
| runtime        = 122 minutes
| country        = India Hindi
| budget         = 
| gross          =   
}}

Tansen  is a 1943 Indian Bollywood film directed by Jayant Desai and featuring K. L. Saigal and Khursheed Bano in the lead roles. The film was based on Tansen,    the 16th century musician in the court of Mughal emperor, Akbar.The film featured 13 hit songs, performed by the leads, including  More Balapan Ke Saathi, Rum Jhum Rum Jhum Chal Tihari, Kahe Guman Kare Gori, Bina Pankh Ka Panchhi, Sapt Suran Teen Gram, Diya Jalao and Baag Laga Doon Sajni.    It was the second highest grossing Indian film of 1943.    In 2009 it was reported that another film based on the life of Tansen would be directed by Satish Kaushik. 

== Plot ==

Tansen becomes orphan at a very young age and lives with his paternal uncle. He goes to a music teacher to learn music and returns to his native village after many years of training. In the village the shepherdess Tani is considered a gifted singer. Tansen falls in love with the shepherdess Tani. Tani rescues an elephant and Tansen tames it by his singing. This captures the attention of Raja Ramchandra. He becomes Tansens friend. At the same time in Agra, the Mughal emperor Akbar regrets why there is no musician in his navratnas. Akbar sends a few of his workers to the countryside to find out the best musician. While his workers are searching the best musician they come across Tansen, who while singing for Tani brings a leafless tree to bloom. The workers are overwhelmed at this and ask Tansen to go to the court of Akbar along with them. Tansen refuses at first by saying that he will sing for no one other than her sweetheart Tani. After repeated persuasions he finally agrees to go to Agra where he enthralls Akbar by his singing. Akbar appoints him as one of his navratnas. The other musicians in the royal court see this move as a threat to their reputation and status in the court.
 raag has the magic of igniting things. Akbar asks Tansen to sing the Deepak raag but again he refuses.Finally on repeated persuasions by the sick princess, Tansen agrees to sing. Tansen ignites the candles placed near him when he is singing. He continues singing in extreme heat and gets badly burnt. When Akbar learns from Azam Khan (the court physician) that Zehen Khan and the other court musicians were plotting against Tansen he orders the arrest of them all. Meanwhile Tansens condition worsens. Birbal, the court philosopher suggests to Akbar that a singer who can sing the Malhar raag (known for bringing heavy rain) should be searched for. Akbar himself searches for such a singer but is unable to find one. The dying Tansen wishes to meet his friend Raja Ramchandra. Along the way to his village Tansen meets Tani near a fountain and she heals him by singing the Megh Malhar raag.

== Cast ==

* K. L. Saigal as Tansen
* Khursheed Bano as Tani   
* Mubarak as emperor Akbar
* Nagendra as Birbal
*Kamaladevi Chattopadhyay as the princess   

== Production==
 Muslim League was demanding the creation of a separate state for Muslims. The films portrayal of the Mughal era was seen as "affirming the place of the Muslim in India rather than interpreting history."    The film has also been categorised as one of the films in which the "Muslims were portrayed in terms of exotic otherness."   

==Music==
The music for the film was prepared by Khemchand Prakash. The lyricist was Pandit Indra.  The lead actor Kundan Lal Saigal had also sung some of the films songs. Saigal had sung the song Diya Jalao in the Deepak raga|raag,    Kahe Gumaan kare re Gori in Gaara raag and Baag laga dun Sajani in the Megh Malhar raag. Prakash had used a piano for the music of the film. This was considered a minor flaw in a period film. The song Mohe panghat pe nandlal chedd gayo re composed by the musician Naushad for the film Mughal-e-Azaam was inspired by the song Kahe Gumaan kare re Gori.   
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! Title !! Singer(s)
|-
|Ghata Ghanaghor Ghor Khursheed Bano
|-
|Aao Gori Aao Shyama Khursheed Bano
|-
|Kahe Guman Kare Re Gori Kundan Lal Saigal
|-
|Rumajhum Rumajhum Chaal Tihaari Kundan Lal Saigal
|-
|Baag Laga Dun Sajani Kundan Lal Saigal
|-
|More Balpan Ke Sathi Chhaila Bhul Jaio Na Kundan Lal Saigal, Khursheed Bano
|-
|Sapt Suran Tin Graam Gaavo Sab Gunijan Kundan Lal Saigal
|-
|Ho Dukhiya Jiyara Rote Naina Khursheed Bano
|-
|Ab Raja Baye More Balam Khursheed Bano
|-
|Binaa Pankh Panchhi Hun Main Kundan Lal Saigal
|-
|Diya Jalao Kundan Lal Saigal
|-
|Din Soona Sooraj Bina Kundan Lal Saigal
|-
|Baraso Re Khursheed Bano
|}

==References==
 

== External links ==
 
*  
*  

 
 
 
 
 
 
 
 
 