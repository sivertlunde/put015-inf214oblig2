Home Room (film)
{{Infobox Film
|  name           = Home Room
|  image          = Home-room movie poster.jpg
|  size           = 220
|  caption        = DVD cover
|  writer         = Paul F. Ryan
|  starring       = Erika Christensen Busy Philipps Victor Garber Agnes Bruckner
|  director       = Paul F. Ryan
|  producer       = Ben Ormand
|  distributor    = DEJ Productions
|  released       = 12 April 2002
|  runtime        = 133 minutes
|  country        = United States
|  language       = English Mike Shapiro
|  awards         =
|  budget         =
}}
Home Room is an independent film starring Erika Christensen, Busy Philipps and Victor Garber. It premiered in the Taos Talking Pictures Film Festival on 12 April 2002, and made its limited theatrical release on 5 September 2003.

==Plot==
 gothic student who is now under the attention of the detective in charge of the case, Det. Martin Van Zandt (Garber).

The school principal asks Alicia to visit Deanna in the hospital. Right away, their differences are evident. Alicia is an outsider from a single-parent family who shuns the society that similarly shuns her, while Deanna is from a wealthy family, gets good grades and is popular with her classmates.

At first, Deanna seems upbeat and cheerful, but soon it becomes apparent that beneath this exterior are psychological scars left behind by the incident. Alicia starts to empathize with her, as she herself is battling her own demons as well, including a previous suicide attempt. Through these similar emotional bonds, the two form an unlikely friendship as they both try to cope with their separate psychological problems.

==Home Room and Columbine==
Even though he started writing the script before the event, director Paul F. Ryan later based the film on the Columbine High School massacre; the film was released only three years after the incident. Ryan and Christensen visited Columbine High School before the films release to speak to students, faculty and parents, who received a private screening of the film. The response was generally positive and Ryan has since returned as a guest of the school twice. http://www.moviemaker.com/hop/vol3/06/screenwriting.html   

While a large part of the public wishes to figure out why such massacres happen, some have lauded Home Room simply for not explaining why they happen; the film does not place blame on violent video games or movies, and concludes that finding a single reason for these events is impossible.
 Littleton afterwards. CNN reported the story for about two weeks, then left. The rest of America moved on, but the people in Littleton didn’t. How do you start living your life again after such a terrible thing?" 

==Reception==
The film received mixed reviews from critics, however Rotten Tomatoes reported that 76% of the audience liked it.

==References==
 

==External links==
* 
*   
* 

 
 
 
 
 
 
 