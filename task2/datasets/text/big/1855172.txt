Pocket Money
 
{{Infobox film
| name           = Pocket Money
| image          = Pocket money.jpg
| image_size     =
| caption        = Theatrical poster.
| director       = Stuart Rosenberg John Foreman
| screenplay     = Terrence Malick
| based on       = Jim Kane by Joseph P. Brown
| starring       = Paul Newman,  Lee Marvin,  Strother Martin
| music          = Alex North
| cinematography = László Kovács (cinematographer)|László Kovács
| editing        = Bob Wyman
| studio         = First Artists
| distributor    = National General Pictures
| released       =  
| runtime        = 102 mins.
| country        = United States English
| budget         =
| gross          =
}} 1972 film directed by Stuart Rosenberg, from a screenplay written by Terrence Malick and based on the novel Jim Kane (1970) by Joseph P. Brown. The movie stars Paul Newman and Lee Marvin and takes place in 1970s Arizona and northern Mexico.
 Fred Graham.

==Plot==
Broke and in debt, an otherwise honest cowboy known as Jim Kane (Newman) gets mixed up in some shady dealings with Stretch Russell (Rogers) and Bill Garrett (Martin), a crooked rancher. Russell tells Kane to escort 200 head of cattle from Mexico to the United States for a good sum of money. Kane agrees and brings along his friend Leonard (Marvin) to aid him. Unfortunately, the two come upon many unexpected events that often deter them from completing their job.

==Cast==
Paul Newman	 ... 	Jim Kane 
	Lee Marvin	... 	Leonard 
	Strother Martin	... 	Bill Garrett 
	Wayne Rogers	... 	Stretch Russell 
	Hector Elizondo	... 	Juan 
	Christine Belford	... 	Adelita 
	Kelly Jean Peters	... 	Sharon (Kanes ex-wife) 
	Gregory Sierra	... 	Guerro Chavarin (as Gregg Sierra)  Fred Graham	... 	Uncle Herb 
	Matt Clark	... 	American prisoner 
	Claudio Miranda	... 	Manisterio Publico 
	Terrence Malick	... 	Worksman

==Reception==
The film received primarily mediocre to negative reviews. Roger Ebert of the Chicago Sun-Times gave the film two stars out of four and wrote, "The movie seems to be going for a highly mannered, elliptical, enigmatic style, and it gets there. We dont."  The film currently has a 60% rating on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 