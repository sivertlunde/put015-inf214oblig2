Goldie (film)
{{Infobox film
| name           = Goldie
| image          = Goldie.(1931).film.poster.jpg
| caption        = Goldie (1931) Movie Poster
| director       = Benjamin Stoloff William Fox
| writer         = Paul Perez Gene Towne
| starring       = Spencer Tracy Warren Hymer Jean Harlow
| music          = R.H. Bassett Hugo Friedhofer Ernest Palmer
| editing        = Alex Troffey Fox Film Corporation
| released       =  
| runtime        = 68 minutes
| country        = United States English
}} A Girl in Every Port.

==Plot==
In every port, sailor Bill, Spencer Tracy, meets girls, who sailor Spike, Warren Hymer, has already met, and talked into getting his signature tattoo. When Bill and Spike finally meet, they become friends. Then, they meet Carny high diver, Goldie, Jean Harlow.

==Cast==
*Spencer Tracy as Bill
*Warren Hymer as Spike
*Jean Harlow as Goldie
*George Raft as a pickpocket (uncredited)
 Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 17 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 