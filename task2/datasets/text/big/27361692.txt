Rage (1966 film)
{{Infobox Film name       = Rage image      = caption    =  director   = Gilberto Gazcón producer   =  writer     = Gilberto Gazcón music =  cinematography  =  editing =  starring   = Glenn Ford
|distributor=  Cinematográfica Jalisco S.A. released   = December 1966 runtime    = 103 min  country   = United States Mexico language  English Spanish Spanish
|}}

Rage is a 1966 U.S./ Mexican Drama film, starring Glenn Ford and written and directed by Gilberto Gazcón.

==Plot== Mexican border town. A man dying of rabies staggers into town and soon Dr. Reuben himself is bitten by the same dog that had bitten the stranger. He must now get others to help him reach a city hospital within 72 hours, before the effects of the disease becomes hopeless.

==Cast==
*Glenn Ford as Doc Reuben
*Stella Stevens as Perla
*David Reynoso as Pancho
*Armando Silvestre as Antonio

== References ==
*The film daily, Volumen 129
* 

 
 
 
 
 
 
 
 


 
 