Mod (film)
{{Infobox film
| name           = Mod (English : Turn)
| image          = Key_art_Mod-Movie-Poster.jpg
| alt            =  
| caption        =
| director       = Nagesh Kukunoor
| producer       = Sujit Kumar Singh Elahe Hiptoola Nagesh Kukunoor
| story          = Nagesh Kukunoor
| starring       = Ayesha Takia Rannvijay Singh Raghuvir Yadav
| music          = Tapas Relia
| cinematography = Chirantan Das
| editing        = Sanjib Datta
| studio         =
| distributor    = Kukunoor Movies Shreya Entertainment
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}

Mod ( ) is a movie by Nagesh Kukunoor, starring Ayesha Takia and Rannvijay Singh, Raghuvir Yadav, Tanvi Azmi and Anant Mahadevan. The movie was released on 30 September 2011.

==Plot== Ayesha Takia). The story takes place in a beautiful hill station called Ganga in Southern Indias Nilgiris district, where Ananya resides with her father, Ashok Mahadeo (Raghuvir Yadav), and aunt, Gayatri Garg (Tanvi Azmi). Ashok is the head of the local Kishore Kumar fan club while Gayatri runs a restaurant. Ananyas mother left home years ago to pursue her dreams. Ashok still waits for her to return. A local shopkeeper, Gangaram, has feelings for Ananya.

In order to support herself and her father, Ananya runs a watch repair store where she ends up meeting a stranger named Andy (Rannvijay Singh). He comes everyday to get his water-logged watch repaired and leaves a   100 note in the form of an Origami swan. Eventually, they fall in love. The story takes a dramatic turn when Ananya discovers the truth about Andy, who claims to be her classmate from school. He admits that he had a crush on her and had even agreed to wait 10 years to see her. She takes a liking to him and both hang out together while she attempts to deal with her creditor, Gangaram, and increasing pressure from R.K. Constructions who want to build a resort in the area. Her world shatters when she finds out that Andy had been killed several years ago, and the man claiming to be him is actually an inmate at the local mental institute. 

==Cast== Ayesha Takia as Ananya Mahadeo
* Rannvijay Singh as Andy/Abhay
* Raghuvir Yadav as Ashok Mahadeo
* Tanvi Azmi as Gayatri Garg
* Anant Mahadevan as Dr. Reddy
* Gulfam Khan as Nurse Kutty
* Prateeksha Lonkar as Mrs. Raymond
* Nakul Sahdev as young Andrew Raymond

==Music==
The music for the film was composed by Tapas Relia. 

{{track listing
| lyrics_credits =
| extra_column = Singer(s)
| length = no
| title1 = Tu Hi Tu
| extra1 = Shivam Pathak & Shreya Ghoshal
| length1 = 5:32
| title2 = Ai Meri Jaaniya
| extra2 = Shivam Pathak
| length2 = 4:14
| title3 = Chand Pal Ke Hamsafar
| extra3 = Shankar Mahadevan & Shreya Ghoshal
| length3 = 6:20
| title4 = Aaj Main Ho Gayi Jawaan
| extra4 = Raghubir, Shreya Ghoshal & Hrishikesh
| length4 = 4:49

| title5 = Tu Hi Tu
| note5 = Unplugged
| extra5 = Vijay Prakash
| length5 = 4:48
| title6 = Aaj Main Ho Gayi Jawaan
| note6 = Remix
| extra6 = Raghubir & Shreya Ghoshal
| length6 = 4:38
| total_length = 29:01
}}

==Reception==
The film received a lukewarm response from most of the critics. Komal Nahta the editor of Koimoi.com quoted "Mod will not be able to score at the ticket windows" and gave it one star.  Leading Bollywood portal Movie Talkies stated "Mod, not quite the turning point for Nagesh Kukunoor". 

==References==
 

==External links==
*  
*  

 

 
 
 
 


 