Seetarama Kalyanam
{{Infobox film
| name           = Seeta Rama Kalyanam
| image          = Seetha Rama Kalyanam 1961.jpg
| image_size     =
| caption        = DVD cover of Seetha Rama Kalyanam film
| director       =N. Trivikrama Rao  National Art Theater, Madras
| writer         = Dhanekula Buchi Venkata Krishna Chowdhary (screen adoptation) Samudrala Raghavacharya (Dialogues )
| narrator       =
| starring       = N. T. Rama Rao Harinath Gitanjali Kanta Rao Chittor V. Nagaiah 
| music          = Gali Penchala Narasimha Rao
| cinematography = Ravikant Nagaich
| editing        = S. P. S. Veerappa
| studio         =
| distributor    =
| released       = 6 January 1961
| runtime        =
| country        = India Telugu
| budget         =
}}
Seeta Rama Kalyanam is a 1961 Telugu film starring N. T. Rama Rao. 

==Plot==
The story primary concentrates on Ravana, a key character in Ramayana. It begins with the birth of Lord Rama and ends with his Marriage with Seetha.

==Cast==
*Harinath as Lord Rama
*Gitanjali as Goddess Seeta
*Nandamuri Taraka Rama Rao as Ravana
*Kanta Rao as Narada Maharshi
*Chittor V. Nagaiah as Dasarathudu
*Mikkilineni as Janakudu
*Chhaya Devi
*B. Saroja Devi as  Mandodari
*Gummadi Venkateswara Rao as Viswamitra Maharshi
*Shobhan Babu as Laxmana
*Kommineni Seshagiri Rao as Bharata
*Kasturi Siva Rao
*A. V. Subba Rao
*Vallabhajosyula Sivaram
*Sarathi as Nalakoobara

==Soundtrack==
There are about 17 songs and poems in the film. 
* Daanava Kula Vairi Darpambu Varninchu (poem) (Singer: Ghantasala Venkateswara Rao)
* Deva Deva Parandhama Neelamegha Shyama (Singer: P. B. Srinivas)
* He Parvati Naadha Kailasa Sailagravasa (Singer: Ghantasala)
* Inupa Kattadalu Gattina Munule Aina (poem) (Singer: P. B. Srinivas)
* Jagadeka Maata Gauri Karuninchave (Singer: P. Susheela)
* Janakundu Sutudu Janmambu Chesina Vanita (poem) (Lyrics: Dhanekula; Singer: Ghantasala)
* Jaya Govinda Madhava Damodara (Singer: Ghantasala)
* Jayatvadadabhra Vibhrama Bhrama Bhujanga (Singer: Ghantasala)
* Kanarara Kailasha Nivasa Palendudhara Jatadhara (Singer: Ghantasala)
* Lakshmi Ksheera Samudraraja Tanayam (slokam) (Singer: M. S. Rama Rao)
* Nelatha Ituvanti Nee Maata (poem) (Lyrics: Dhanekula; Singer: Ghantasala)
* O Sukumara Ninugani Murisitira (Singers: P. Leela and Ghantasala)
* Padave Ragamayi Veena Padave Ragamayi (Singer: P. Susheela)
* Parama Shivacharaparulalo Atyanta Priyudannna (poem) (Singer: Ghantasala)
* Polupagu Brahma Vamsamuna Butti (poem) (Singer: P. Susheela)
* Sarasala Javaralanu Nene Kada (Singer: P. Leela)
* Veyi Kannulu Chalavugaa Vedukaina Ma Seetanu Chuda (Singer: P. Leela)

==Awards== National Film Awards
*     

==References==
 

==External links==
*  
 

 
 
 