Lord Loveland Discovers America
{{Infobox film
| name           = Lord Loveland Discovers America
| image          = Arthur Maude - 1916 movie poster.jpg
| image_size     =
| caption        =
| director       = Arthur Maude
| producer       =
| writer         = Charles Norris Williamson|C. N. & Alice Muriel Williamson|A. M. Williamson (novel)
| narrator       =
| starring       = Arthur Maude Constance Crawley
| cinematography =
| editing        = America Film Company
| distributor    = Mutual Film (USA)
| released       =  
| runtime        = 5 reels (about 12 minutes per reel)
| country        = United States Silent English English intertitles
| budget         =
| gross          =
| website        =
| amg_id         =
}}

  Charles and Alice Williamson. 

==Plot==
Plagued by creditors, but with no money to pay his debts, Lord Loveland (Arthur Maude) leaves England and sails to the United States hoping to find a wealthy heiress to marry. During the voyage, he makes friends with a lady playwright named Leslie Dearmer (Constance Crawley), whom he likes, but believing her not to be the wealthy heiress he seeks, he is reluctant to develop their friendship further. Leslie likes Loveland also and tries to help him after the ship arrives in port. 

Unfortunately, they part unexpectedly when Loveland joins a theater troupe and suddenly leaves town, hoping that he can make some quick money as the troupe tours across the country. But when the troupe performs one of Leslies plays without her permission, she tracks them down, not realizing that Loveland is among them. 

Finally catching up with the troupe, Leslie is surprised and overjoyed to find Loveland once again, and she gives him a job driving her car. Although Loveland still intends to marry a millionaire, he falls in love with Leslie, and after she tells him that she actually is an heiress, both their dreams come true when he asks her to marry him.

==Cast==
*Arthur Maude
*Constance Crawley
*William A. Carroll George Clancey
*Nell Franzen
*William Frawley Charles Newton

==References==
 

==External links==
 
* 

 
 
 
 
 
 