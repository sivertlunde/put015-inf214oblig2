Kulam (film)
{{Infobox film
| name           = Kulam
| image          =
| caption        =
| director       = Lenin Rajendran
| producer       = 
| writer         = Lenin Rajendran (dialogues)
| based on =  
| screenplay     = Lenin Rajendran Vijayaraghavan Jagathy Sreekumar
| music          = MG Radhakrishnan
| cinematography = Madhu Ambat
| editing        = B. Lenin V. T. Vijayan
| studio         = Usha Priya Movie Makers
| distributor    = Usha Priya Movie Makers
| released       =  
| country        = India Malayalam
}}
 1997 Cinema Indian Malayalam Malayalam film, Vijayaraghavan and Jagathy Sreekumar in lead roles. The film had musical score by MG Radhakrishnan.    
{{Cite web
  | title = The politics of a relationship
  | last    = Jayakumar
  | first   = G.
  | publisher  = THE HINDU, Jan 27, 2006
  | url   = http://www.hindu.com/fr/2006/01/27/stories/2006012701150100.htm
  | year  = 2006
}} 

==Cast==
*Suresh Gopi
*Bhanupriya Vijayaraghavan
*Jagathy Sreekumar
*Thilakan
*Bharath Gopi
*Nassar Raghavan

==Soundtrack==
The music was composed by MG Radhakrishnan and lyrics was written by V Madhusoodanan Nair. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Agni Sathyangal || V Madhusoodanan Nair || V Madhusoodanan Nair || 
|-
| 2 || Chandana Shilayil || KS Chithra, MG Radhakrishnan || V Madhusoodanan Nair || 
|-
| 3 || Enthamme chundathu   || KS Chithra || V Madhusoodanan Nair || 
|-
| 4 || Enthamme chundathu   || K. J. Yesudas || V Madhusoodanan Nair || 
|-
| 5 || Thiranthu Paarthen || SP Balasubrahmanyam || V Madhusoodanan Nair || 
|}

==References==
 

==External links==
*  

 
 
 

 