N.Y.H.C. (film)
{{Infobox Film
| name           = N.Y.H.C.
| image          =  
| caption        =
| director       = Frank Pavich
| producer       = Frank Pavich Stephen Scarlata Anthony Edwards (EP) Dante Di Loreto (EP)
| writer         = 
| narrator       =  108 No Redeeming Social Value District 9
| music          = 
| cinematography = Henryk Tzvi Cymerman
| editing        = Jason Airey Frank Pavich
| distributor    = HALO 8 Entertainment
| released       =   1999
| runtime        = 87 min. US
| English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} New York hardcore scene.  Filmed in the summer of 1995, it was completed and self-released on VHS by the director in 1999.
 H 2 O).

==Synopsis== hardcore music Hare Krishna devotees.

==Live performances==
The featured bands are:
*Madball
*25 ta Life
*Vision of Disorder
*Crown of Thornz 108
*No Redeeming Social Value
*District 9

==Interviewees==
Interviewees include: Freddy Madball (Madball)
*Rick ta Life (25 ta Life)
*Roger Miret (Agnostic Front) John Joseph (Cro-Mags) Lord Ezec (Crown of Thornz)
*Mike Dijan (Crown of Thornz)
*Kevin Gill (SFT Records)
*Barbara Kenngott (SFT Records)
*Vic DiCara (108)
*Rasaraja dasa aka Rob Fish (108)
*Trivikrama dasa (108)
*Jimmy Gestapo (Murphys Law (band)|Murphys Law)
*Tommy Rat (Rejuvenate)
*Dean Miller (No Redeeming Social Value)
*Vinnie Value (No Redeeming Social Value)
*Kent Miller (No Redeeming Social Value)
*Mike Dixon (No Redeeming Social Value)
*Chris Wynne (In Effect Magazine)
*Tim Williams (VOD)
*Mike Kennedy (VOD)
*Mike Fleischmann (VOD)
*Brendan Cohen (VOD)
*Matt Baumbach (VOD)
*Myke Rivera (District 9)
*Cesar Ramirez (District 9)
*Todd Hamilton (District 9)
*Loki Velasquez (District 9)

Notable in this film is interview footage of Roger Miret while using a wheelchair due to a vertebrae injury sustained at a show.

==Film Festivals==
*Chicago Underground Film Festival - 1999
*Euro Underground Film Festival - 1999 Hot Springs Documentary Film Festival - 2000
*New York Underground Film Festival - 2000
*Sound Unseen Film & Music Festival - 2000
*Mostra Internacional de Cinema (Brazil) - 2001

==Soundtrack== compact disc. It was intended to help raise funds for the completion of the documentary itself (which was finally completed & released in 1999). This CD featured live tracks as well as interview clips.  The featured bands are Madball, 25 ta Life, Vision of Disorder, 108 (band)|108, Crown of Thornz, No Redeeming Social Value and District 9.
{{Infobox album |  
  Name        = N.Y.H.C. Documentary Soundtrack  |
  Type        = soundtrack |
  Artist      = |
  Cover       = Nyhccoundtrack.jpg|
  Released    = 1996 |
  Recorded    = Recorded 1995 |
  Genre       = Hardcore punk |
  Length      = 73 minutes |
  Label       = S.F.T Records |
  Producer    = Velebit Productions|}}
All tracks were recorded live in the summer of 1995.  It is currently out of print.

===Track listing===

#Crown of Thornz - Juggernaut
#25 ta Life - Da Lowdown
#Kevin Gill of SFT Records (interview dialogue)
#Vision of Disorder - Suffer
#Virginia (interview dialogue)
#District 9 - Fool Freddy Madball (interview dialogue)
#Madball - New York City
#Rasaraja dasa of 108 (interview dialogue)
#108 - Solitary
#Vinnie & Dean of NRSV (interview dialogue)
#No Redeeming Social Value - New 64
#Myke & Todd of District 9 (interview dialogue)
#District 9 - Victim
#Rick ta Life (interview dialogue)
#25 ta Life - Separate Ways John Joseph of the Cro-Mags (interview dialogue)
#108 - Holyname
#Tim & Brendan of Vision of Disorder (interview dialogue)
#Vision of Disorder - Formula for Failure
#Roger Miret of Agnostic Front (interview dialogue)
#Madball - Its Time
#Madball - Crucified (with Roger Miret on Vocals)
#Dean & Vinnie of NRSV with Chris of In Effect Magazine (interview dialogue)
#No Redeeming Social Value - No Regrets
#Tim Williams, Matt Baumbach, Mike Kennedy & Mike Fleischmann of Vision of Disorder (interview dialogue)
#Vision of Disorder - D.T.O.
#Ezec of Crown of Thornz (interview dialogue)
#Crown of Thornz - Crown of Thornz

==External links==
*  
*  
*   on Myspace 
*   on   2006
 

 
 
 
 
 
 
 