Prathyartha
{{Infobox film
| name       = Prathyartha
| image      = Prathyartha.jpg
| director   = Sunil Kumar Desai
| producer   = G.V. Srinivas B.G. Manjunath
| story      = Sunil Kumar Desai
| screenplay = Sunil Kumar Desai
| starring   =  
| music      = Ilaiyaraaja
| cinematography = H.C. Venu
| editing    = R. Janardhan
| studio     = 
| distributor = 
| released   =  
| country    = India
| language   = Kannada
| runtime    = 
| budget     = 
| gross      = 
| website    =  
}}
 Kannada suspense-thriller film directed by Sunil Kumar Desai featuring Ramesh Aravind, Raghuvaran, Girish Karnad and Sudeep in the lead roles. The film features background score and soundtrack composed by Ilaiyaraaja.  

==Cast==
* Ramesh Aravind  
* Raghuvaran 
* Girish Karnad
* Sudeep 
* Avinash
* Shivaram
* Kashi 
and others... 

==Shooting place==
Entire movie story revolves around the Mysore Palace during dasara season.

==Awards==
{| class="infobox" style="width: 22.7em; text-align: left; font-size: 85%; vertical-align: middle; background-color: #eef;"
|-
| colspan="3" |
{| class="collapsible collapsed" width="100%"
! colspan="3" style="background-color: #d9e8ff; text-align: center;" | Awards and nominations
|- style="background-color:#d9e8ff; text-align:center;"
!style="vertical-align: middle;" | Award
| style="background:#cceecc; font-size:8pt;" width="60px" | Wins
| style="background:#eecccc; font-size:8pt;" width="60px" | Nominations
|-
|align="center"|
;Karnataka State Film Awards
| 
| 
|}
|- style="background-color:#d9e8ff"
| colspan="3" style="text-align:center;" | Totals
|-
|  
| colspan="2" width=50  
|-
|  
| colspan="2" width=50  
|}
Karnataka State Film Awards :- Best Screenplay - Won - Sunil Kumar Desai (1998–99)  

==References==
 

==External links==
*  

 
 
 
 


 