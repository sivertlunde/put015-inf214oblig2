The Local Bad Man
{{Infobox film
| name           = The Local Bad Man
| image          = DVD cover of The Local Bad Man.jpg
| caption        = Film DVD cover
| director       = Otto Brower
| producer       = M.H. Hoffman Jr.
| writer         = {{plainlist|
*Peter B. Kyne (story)
*Philip Graham White (scenario)
}}
| starring       = Hoot Gibson
| cinematography = {{plainlist| Tom Galligan
*Harry Neumann
}}
| editing        = Mildred Johnston
| released       =  
| runtime        = 59 minutes
| country        = United States
| language       = English
}}
 Western American film directed by Otto Brower.

== Cast ==
*Hoot Gibson as Jim Bonner
*Sally Blane as Marion Meade
*Hooper Atchley as Joe Murdock Edward Hearn as Ben Murdock
*Edward Peil Sr. as Sheriff Hickory
*Jack Clifford as Railroad Station-Agent Andrew McKee
*Skeeter Bill Robbins as "Skeeter" Cole
*Milton Brown as "Horsetail" Wright

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 

 