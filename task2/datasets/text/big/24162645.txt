Hearts in Exile (1929 film)
{{Infobox film
| name           = Hearts in Exile
| image          = Hearts in Exile (Swedish poster)- 1929.jpg
| image_size     =
| caption        = 1929 Swedish release poster
| director       = Michael Curtiz
| producer       = Warner Brothers
| writer         = John Oxenham (play) Harvey Gates (scenario) De Leon Anthony (intertitles)
| narrator       =
| starring       = Dolores Costello Grant Withers
| music          = William Rees Thomas Pratt
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}} talking romance film produced and distributed by Warner Brothers and directed by Michael Curtiz. It was also released in a silent version with music and effects. It starred Dolores Costello in a story based on the 1904 play by John Oxenham. A surviving 1915 film starring Clara Kimball Young was produced. Hearts in Exile was released in both silent and sound versions. 

This is now considered a lost film.   

==Cast==
* Dolores Costello - Vera Zuanova
* Grant Withers - Paul Pavloff James Kirkwood - Baron Serge Palma
* George Fawcett - Dmitri Ivanov
* David Torrence - Governor
* Olive Tell - Anna Reskova
* Lee Moran - Professor Rooster
* Tom Dugan - Orderly
* Rose Dione - Marya William Irving - Rat Catcher
* Carrie Daumery - Baroness Veimar

==References==
 

==See also==
*List of lost films

==External links==
*  
* 
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 