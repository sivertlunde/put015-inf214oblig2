High Hopes (2006 film)
{{Infobox film
| name = High Hopes
| image =
| caption = 
| director = Joe Eckardt
| producer = Larry Bain Joe Eckardt Cecily Gambrell Danny Trejo
| writer = Cecily Gambrell Colby Kane
| starring = Jason Mewes Lacey Chabert Andy Dick Danny Trejo Edward Furlong David Faustino Gina DeVettori
| cinematography = Brian Bernstein Dana Gonzales    
| production designer =
| music = Didier Rachou    
| original songs =
| editing = Joseph Lorigo    
| animation title sequence = 
| production company =
| distributor = Lions Gate
| released =  
| runtime = 
| country = United States
| language = English 
}}
High Hopes also billed as Nice Guys is a 2006 film directed by Joe Eckardt.  

==Plot==
Hollywood hopeful Tom Murphy and his posse of pals conspire to get into the big leagues. Pinning their hopes of industry success on Toms famous girlfriend starring in their first feature, falls to pieces when she dumps him. Tom and his pals learn of another possibility and devise a plan to steal a fenced case of government issued marijuana, return it to the FBI and use the reward money to finance their movie. Little do they know a scorned girlfriend and her deaf mute brother have other plans.

==Cast==
*Jason Mewes as Quebert
*Lacey Chabert as Cindy
*Andy Dick as Patrick
*Danny Trejo as Shady
*Jennifer Finnigan as Morgan
*Edward Furlong as Tye
*Robert Rodriguez as Mr. Lewis
*Dallas Page as Sleezy Guy
*David Faustino as Ben
*Corin Nemec as Tom
*Michael DeLorenzo as Rocko
*Jason Marsden as Wendell
*Gina DeVettori as Carly
*Cecily Gambrell as Julie
*Ted Raimi as Special Agent Brown

==References==
 

==External links==
* 
* 

 
 
 
 
 


 