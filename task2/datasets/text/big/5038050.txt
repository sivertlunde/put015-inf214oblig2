Hardly Working
 
 
{{Infobox film
| name = Hardly Working
| image = hardlyworking.jpg
| caption = U.S. theatrical poster
| director = Jerry Lewis
| writer = Jerry Lewis Michael Janover
| starring = Jerry Lewis Susan Oliver Deanna Lund
| producer = Igo Kantor James J. McNamara
| distributor = 20th Century Fox
| editing = Michael Luciano
| cinematography = James Pergola
| music = Morton Stevens
| released =  
| runtime = 91 minutes
| language = English
| country = United States
| budget  = US$3.4 million   
| gross = $8,860,000 (US/ Canada)  540,740 admissions (France)   at Box Office Story 
}}

Hardly Working is a comedy film directed by and starring Jerry Lewis.  It was filmed in 1979, and was released in Europe in 1980 and in the United States on April 3, 1981 through 20th Century Fox.

==Plot==
Bo Hooper (Lewis), a clown, finds himself unemployed when the circus where he works suddenly closes.  He winds up living with his sister (Susan Oliver), against the wishes of her husband Robert (Roger C. Carmel).  From there he goes from job to job, wreaking havoc along the way.  He finally finds some stability as a postal worker, until he finds out that his boss is his girlfriends father.  The father hates all mail carriers because his daughters ex-husband was one, so he tries to wreck Bos life, but Bo overcomes the odds and succeeds not only at work, but at impressing the father.

==Production==
This was Lewis "comeback" film, as it was his first released film since 1970s Which Way to the Front?.  In between, he filmed The Day the Clown Cried, which, to date, is unreleased.  
 The Patsy. The Family Jewels.

Production was halted for about six months in 1980 after it ran out of money, with Lewis himself declaring personal bankruptcy.  Because of this, there are notably many continuity issues throughout the film.  
 cameo as a disco dancer.

Lewis also played the part of the Little Ol Lady dressed in drag.  During the closing credits this part was credited to "Joseph Levitch", which is Lewis birth name.

==Release==
By 1980, Hardly Working grossed US$25 million throughout Europe and South America; this success convinced 20th Century Fox to pick up the independent production for distribution in the United States.     The U.S. cut was trimmed from the European prints    by around 20 minutes. 

==Reviews==
The movie was critically panned; Roger Ebert gave it zero stars and called it "one of the worst movies ever to achieve commercial release in this country   no wonder it was on the shelf for two years before it saw the light of day."   In his Movie Guide, Leonard Maltin gave it two stars out of four with this comment: "Not a very good movie; the opening montage   is much funnier than anything that follows." 

==References==
 

==External links==
* 

 

 
 
 
 
 