Meat Grinder
{{Infobox film
| name          = The Meat Grinder
| image         = Meat-grinder-thai-film.jpg
| image_size    =
| caption       = Theatrical poster
| director      = Tiwa Moeithaisong
| producer      = Phra Nakorn Film Co. Ltd.
| writer        = Tiwa Moeithaisong
| starring      = Mai Charoenpura Anuway Niwartwong Wiradit Srimalai Rattanaballang Tohssawat Duangta Tungkamanee
| music          = 
| cinematography = Tiwa Moeithaisong
| editing        = Tiwa Moeithaisong
| studio         = 
| distributor    = 
| released       = 2009
| runtime        = 170 minutes
| country        = 
| language       = Thai language
| budget         = 
| gross          = 
}} Thai film director Tiwa Moeithaisong is a horror movie tackling cannibalism.

==Synopsis==
Buss (Mai Charoenpura) is a disturbed woman who hears voices in her head and is tormented by visions. Having been taught some pretty dubious and unconventional food preparation and cooking skills by her mother, she decides to open up a noodle stall, using the body of a man left over from a riot as the main ingredient. Soon enough, the customers are turning up in droves for her delicious meals, and life is starting to look good after a nice young man takes an interest in her. However, her past comes back to haunt her, and as her mental state breaks down, yet more people end up on the chopping block or hanging up on meat hooks in her basement.

The film also tackles the themes of mental illness and the mistreatment of women, with Buss behavior being depicted as being part of an ongoing cycle of violence that she suffered as a child, and which she is now passing on to her own daughter.

==Cast==
*Mai Charoenpura as Buss		
*Anuway Niwartwong		
*Wiradit Srimalai		
*Rattanaballang Tohssawat		
*Duangta Tungkamanee

==Censorship==
Meat Grinder was released in March 2009, just a few months before the ratings system came into effect in Thailand. After beef noodle shop owners and vendors protested, the movie was reportedly ordered to cut certain scenes.

The title for the Thai main release was also changed from "Guay-dteow Neua Kon" (ก๋วยเตี๋ยว เนื้อ คน), literally "human meat noodles" to "Cheuat Gon Chim" (เชือด ก่อน ชิม), "carve before tasting", to remove direct reference to noodles, a staple in Thai food. 

==Award nominations==
In 2010, the film was nominated for three awards at the Thailand National Film Association Awards
*"Best Actress" for Mai Charoenpura
*"Best Art Direction"
*"Best Make-up" for Siwakorn Suklankarn and Nattakarn Uthaiwan

==References==
 

 
 
 