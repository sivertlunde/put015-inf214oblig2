Star in the Night
 
{{Infobox film
| name           = Star in the Night
| image          = Star_in_the_Night.jpg
| caption        = Title card
| director       = Don Siegel
| producer       = Gordon Hollingshead
| writer         = Robert Finch (story) Saul Elkins (adaption) Donald Woods Rosina Galli
| music          = William Lava
| cinematography = Robert Burks
| editing        = Rex Steele
| distributor    = Warner Bros.
| released       =  
| runtime        = 22 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 short drama debut film, 1946 for Best Short Nativity story, set on Christmas Eve at a desert motel in the Southwestern United States.

== Plot ==
Christmas Eve in a lonely desert in the  s have just bought out christmas presents from a store, although they actually dont need them. One of the cowboys says that he just had the feeling that he should buy gifts to give them to someone. The cowboys see a flashing star in the distance, which they ride over to investigate. The star is actually a second hand star, used by the Italian-American Nick Catapoli for his little motel in the desert. A mysterious hitchiker appears at Nicks motel who states that he just wants to come in from the cold for a little while. Nick and the Hitchhiker have a discussion about christmas. While the hitchiker tries to explain the true meaning of Christmas with love, goodwill and brotherhood; Nick opposes the holiday: He thinks that people behave bad during most of the year, but then try to behave in a fake-friendly way at Christmas. Nick shows the hitchiker his motel customers as examples: Miss Roberts complains about the noise of christmas carolers; the businessman Mr. Dilson is furious about the shirt-cleaning service that Nick uses and a travelling couple demands to get extra blankets for their room.

A young Mexican-American couple, Jose and Maria Santos, arrives at the motel hoping to get lodging. There are no cabins available, so Rosa accommodates them in a small shed next to the hotel. Maria is expecting a baby and is in a somewhat critical condition without a doctor. When the motel lodgers find out about Marias approaching birth they try to help her. The lodgers forget their selfishness and now react in a social way: For example, the businessman who was angry about the poor work of the laundry on his "expensive shirt", now gives his shirt as a blanket to the birth of the child. After the successful birth, the three cowboys appear at the motel and give their presents to the child. Nick learns that there is still goodness in the world and is now positive about Christmas. He even gives the hitchiker, who observed the situation, a cup of coffee and his coat. He wishes "Merry Christmas" to the hitchiker who now leaves the hotel. At the end, Nick sees how much the birth of the child in his shed resembles the Nativity Story and cries.

==Cast==
* J. Carrol Naish as Nick Catapoli Donald Woods as the Hitchhiker Rosina Galli as Rosa Catapoli Anthony Caruso as José Santos 
* Lynn Baggett as Maria Santos
* Irving Bacon as Mr. Dilson, Traveling Salesman
* Dick Elliott as the Traveling Husband
* Claire Du Brey as the Travelers Wife
* Virginia Sale as Miss Roberts, woman trying to sleep Johnny Miles as the Cowboy (riding left)
* Richard Erdman as the Cowboy (riding middle)
* Cactus Mack as the Cowboy (riding right)

== Background == Nativity story and also adds some elements from A Christmas Carol to it. The original story was written by Robert Finch while Saul Elkins adapted it for a film. Produced with a rather minor budget and character actors, it was the film debut of Don Siegel who later directed mostly action movies like Dirty Harry. Previously Siegel had worked as a Second-Unit-Director and cutter. The cinematography was made by a young Robert Burks who later worked for Alfred Hitchcock. 
 1946 for Best Short Subject (Two-Reel). The Academy Award for Best Short Documentary Film was won by Hitler Lives, also directed by Don Siegel. After his success with both movies he went to make feature films.

==Release==
The film in its entirety is available as a bonus feature on the DVD release of Christmas in Connecticut, released by Warner Home Video in 2005.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 