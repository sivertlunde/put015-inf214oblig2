Benny and Babloo
{{Infobox film
| name           = Benny and Babloo
| image          = bennyandbabloo.jpg
| caption        = Official Movie Poster
| director       = Yunus Sajawal
| producer       = Umesh Chouhan & Bablu Uddin
| starring       = Kay Kay Menon Rajpal Yadav Riya Sen  Muskkaan   Shweta Tiwari   Rukhsar Rehman  Natassha  Richa Chadda  Maushumi Udeshi  Hiten Paintal   Aashif Sheikh
| writer         = Yunus Sajawal
| music          = Amjad Nadeem
| cinematography = 
| editing        = 
| released       =  
| country        = India
| language       = Hindi
| Budget          = 50 Crores
}}
Benny and Babloo is a 2010 Bollywood satirical comedy film, produced by Umesh Chouhan and directed by Yunus Sajawal. The film was released in October 2010 under the Chamunda Films banner. The film stars Kay Kay Menon, Rajpal Yadav, Riya Sen, Shweta Tiwari, Rukhsar Rehman, Natassha, Richa Chadda, Maushumi Udeshi, Hiten Paintal and Aashif Sheikhin prominent roles.    

==Plot==
Smitten by the glamor of Mumbai, two friends, Benny (Kay Kay Menon) and Babloo (Rajpal Yadav) land up in distinctly different jobs - Benny as the bellboy of a five-star hotel and Babloo as a waiter in a ladies service bar.

Benny believes his job is better than Babloos and makes fun of him on many occasions. Bennys pride fades away as they both realize that even though their job may look different on the outside, theyre quite similar on the inside.

Benny witnesses several criminal activities at the hotel, from drug abuse to political fiascos while Babloo sees the human side of the ladies bar. The truth stares them in the face that five-star hotels are worse than ladies bars.  

Critical Reception

This film got widely positive reception.This depicted the real situation of bar dancers and condition of Five star hotels.Kay Kay Menon and Rajpal Yadav bring  the reality in characters.It is an acclaimed art comedy and appreciated by critics.

==References==
 

==External links==
* 
* 

 
 
 


 