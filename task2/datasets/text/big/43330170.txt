Riders of the Night
{{infobox film
| title          = Riders of the Night
| image          = Riders of the Night.jpg
| imagesize      =
| caption        = lobby poster
| director       = John H. Collins
| producer       =
| writer         = John H. Collins(story) Albert S. Le Vino(writer)
| starring       = Viola Dana
| music          =
| cinematography = John Arnold
| editing        =
| distributor    = Metro Pictures
| released       = April 29, 1918
| runtime        = 5 reels
| country        = USA
| language       = Silent film..(English intertitles)
}}
Riders of the Night is a 1918 silent film drama directed by John H. Collins and starred his wife Viola Dana. It was produced and distributed by the Metro Pictures company. 

A print is held at EYE Institute, Amsterdam aka Filmmuseum Amsterdam. 

==Cast==
*Viola Dana - Sally Castleton
*George Chesebro - Milt Derr
*Clifford Bruce - John Derr Russell Simpson - Sallys Grandfather
*Mabel Van Buren - Sallys Aunt
*Monte Blue - Jed, The Killer

==References==
 

==External links==
* 
* 

 
 
 
 
 


 