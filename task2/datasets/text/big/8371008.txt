Anna Christie (1930 German-language film)
{{Infobox film
| name           = Anna Christie
| image          = Anna Christie 1930 film.jpg
| caption        = (Poster for original English-language version)
| director       = Jacques Feyder
| producer       = 
| writer         = Walter Hasenclever Frances Marion Hans Junkermann Salka Viertel
| music          = 
| cinematography = William H. Daniels
| editing        = Finn Ulback
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = German
| budget         = 
}} play of the same title and filmed following the release of the Anna Christie (1930 film)|English-language original version of the same adaptation earlier the same year.  Both versions feature leading actress Greta Garbo. In the early years of sound films, Hollywood studios produced Multiple-language version|foreign-language versions of some of their films using the same sets and sometimes the same costumes, with native speakers of the language usually replacing some or all of the original cast. While many of those versions no longer exist, the German-language version of Anna Christie survives.

The film was produced by Metro-Goldwyn-Mayer at their Culver City, California studio in July and August of 1930 (the English-language original had been filmed there in October and November of 1929).  It premiered in Cologne, Germany on December 2, 1930.  Garbo is the only cast member in both versions and noticeably differs in her appearance in the two. The German dialog was written by Walter Hasenclever and Frank Reicher, for the most part very closely following Frances Marions original adaptation. The film was directed by Jacques Feyder using the same cinematographer, Garbo favorite William H. Daniels, but a different crew. 

According to the 2005 DVD release of the film, which includes both the English and German versions, Garbo much preferred the German version.

==Plot==
Chris Christofferson (Hans Junkermann), the alcoholic skipper of a coal barge in New York, receives a letter from his estranged twenty-year-old daughter Anna "Christie" Christofferson (Greta Garbo). She tells him that shell be leaving Minnesota to stay with him. Chris had left Anna 15 years ago to be raised by relatives who live on a farm in the countryside of St. Paul. Chris has never visited his daughter in St. Paul and consequently has not seen her for the past 15 years.

Anna Christie arrives, an emotionally wounded woman with a dishonorable, hidden past. In order to survive, she has worked in a brothel for two years. She moves to the barge to live with her father and one night Chris rescues the sailor Matt (Theo Shall) and two other displaced sailors from the sea. Anna and Matt soon fall in love with one another and Anna has the best days of her life. However, when Matt proposes to marry her, she is reluctant and haunted by her recent past. Matt insists and compels Anna to tell him the truth. She opens her heart to Matt and her father, disclosing the dark secrets of her past.

==Cast (in credits order)==
*Greta Garbo as Anna Christie
*Theo Shall as Matt Burke Hans Junkermann as Chris Christofferson
*Salka Viertel as Marthy Owens

==Notes==
 

==References==
* {{Cite book
 | last = Vieira
 | first = Mark A.
 | title = Greta Garbo: A Cinematic Legacy
 | year = 2005
 | publisher = Harry A. Abrams
 | location = New York
 | isbn = 978-0-8109-5897-5
 | ref = harv
 }}
* {{Cite book
| last = Paris
| first = Barry
| title = Garbo
| year = 1994
| publisher = Alfred A. Knopf
| isbn = 0-8166-4182-X ref = harv
}}

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 