Freedomland (film)
{{Infobox film
| name           = Freedomland
| image          = Freedomland Poster.jpg
| caption        = Theatrical release poster
| director       = Joe Roth
| producer       = Scott Rudin Charles Newirth
| based on       =  
| writer         = Richard Price	
| starring       = Samuel L. Jackson Julianne Moore
| music          = James Newton Howard
| cinematography = Anastas Michos
| editing        = Nick Moore
| studio         = Revolution Studios
| distributor    = Columbia Pictures
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English Italian
| budget         = $37.7 million
| gross          = $14,655,628
}}
 Richard Price novel of the same name, which touches on themes of covert racism.

==Plot==
Brenda (Julianne Moore) walks through a predominantly African American housing project and enters an emergency room, apparently in shock and with cut and bleeding hands.  Police detective Lorenzo Council (Samuel L. Jackson) is sent to take a statement from Brenda, who says that her car has been stolen. When Lorenzo gets there, she reveals that her young son, Cody, was in the back seat of the car.  The police frantically begin searching for Cody.

Brendas brother, Danny (Ron Eldard), a police officer in a neighboring town, calls a massive police presence in to search the housing project for clues.  This angers the residents who protest their innocence.  Lorenzo begins to suspect that Brenda is holding back details from him and pressures her to tell the truth.  She insists that she has told the truth and would never harm her son. With a sketch artist she produces a picture of the man she says stole her car. Dannys white coworkers arrest a man from the housing project who they think matches the picture. Danny flies into a rage and beats him.  

Desperate to find Cody, Lorenzo enlists the aid of a volunteer group which helps search for missing children.  He suggests that they search Freedomland, an abandoned foundling hospital nearby.  As they search, the groups leader Karen Collucci (Edie Falco) talks with Brenda.  Collucci had lost her own son years before and convinces Brenda to admit that Cody is dead.  She leads them to a nearby park where they find Codys body in a shallow grave, covered with heavy rocks.  

Lorenzo realizes that Brenda could not have moved the rocks herself.  Under interrogation Brenda admits to having been engaged in an affair with a man named Billy (Anthony Mackie) who lived in the projects.  She would give Cody cough syrup so that he would fall asleep and she could visit Billy.  On the night in question she had returned to find Cody dead, having drunk a whole bottle of cough syrup.  Billy helped her bury his body.  When the police go to arrest Billy, they are confronted by residents angry over previous unfounded police harassment and a riot erupts.  Brenda is charged with criminal neglect, and Lorenzo promises to visit her in jail.

==Cast==
* Samuel L. Jackson as Officer Lorenzo Council
* Julianne Moore as Brenda Martin
* Edie Falco as Karen Collucci
* Ron Eldard as Danny Martin William Forsythe as Boyle
* Aunjanue Ellis as Felicia
* Anthony Mackie as Billy Williams
* LaTanya Richardson as Marie
* Clarke Peters as Reverend Longway
* Peter Friedman as Lt. Gold
* Domenick Lombardozzi as Leo Sullivan
* Aasif Mandvi as Dr. Anil Chatterjee
* Philip Bosco as Priest
* Dorian Missick as Jason Council
* Marlon Sherman as Cody

==Reception==
Freedomland was poorly received by critics, as the film currently holds an approval rating of 23% on Rotten Tomatoes based on 149 reviews. The consensus states: "Poorly directed and overacted, Freedomland attempts to address sensitive race and class issues but its overzealousness misses the mark."

The film was a box office failure, grossing only $14.6 million worldwide when compared to its budget of $37.7 million.

==References==
 
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 