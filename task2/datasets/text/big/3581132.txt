Trunk to Cairo
{{Infobox film
| name = Trunk to Cairo
| other name = 
| image = Ttcpos.jpg
| caption = Original film poster
| writer = Marc Behm Alexander Ramati
| starring = Audie Murphy George Sanders
| director = Menahem Golan
| producer = Menahem Golan
| music = Dov Seltzer
| cinematography = Mimish Herbst
| editing = Danny Shik
| distributor = American International Pictures
| released =  
| runtime = 103 minutes
| country = Israel
| language = English
}}
Trunk to Cairo is a 1966 spy film distributed by American International Pictures. 

==Plot==
Mike Merrick (Audie Murphy) is an American agent who is sent to meet with Professor Schlieben (George Sanders) a German scientist. During the mission it is revealed that the professor is a Neo-Nazi, developing a weaponized rocket that can be used against the Western world. Merrick now must destroy the rocket plans hidden in Schliebens lab. Things are further complicated when Radical Muslims insist on destroying the rocket themselves and Merrick. After kidnapping Schliebens daughter he must now escape Middle Eastern intelligence agencies against impossible odds.

==Cast==
*Audie Murphy  ....   Mike Merrick
*George Sanders  ....   Professor Schlieben
*Marianne Koch  ....   Helga Schlieben
*Hans von Borsody  ....   Hans Klugg
*Joseph Yadin  ....   Captain Gabar
*Gila Almagor  ....   Yasmin
*Eytan Priver  ....   Jamil
*Bomba Zur  ....   Ali
*Zeev Berlinski
*Shlomo Paz
*Anna Schnell 
*Eliezer Young

==Trivia== The Quiet American.
 diplomatic mail."

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 