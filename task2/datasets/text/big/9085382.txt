Limehouse Blues (film)
{{Infobox film
| name           = Limehouse Blues
| image          =
| image_size     =
| caption        =
| director       = Alexander Hall
| producer       = Arthur Hornblow Jr.
| writer         = Cyril Hume Grover Jones Arthur Phillips
| narrator       =
| starring       = George Raft Jean Parker Anna May Wong Kent Taylor
| music          = Sam Coslow John Leipold
| cinematography = Harry Fischbeck William Shea
| distributor    = Paramount Pictures
| released       = December 11, 1934
| runtime        = 63 mins
| country        = United States English
| budget         =
| preceded_by    =
| followed_by    =
}} 1934 crime Chinese district Limehouse Blues." The movie was directed by Alexander Hall.

The film was not a commercial success when first released. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 52 

==Cast==
*George Raft as Harry Young
*Jean Parker as Toni
*Anna May Wong as Tu Tuan
*Kent Taylor as Eric Benton
*Montagu Love as Pug Talbot

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 