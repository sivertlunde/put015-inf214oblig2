San Andreas (film)
{{Infobox film
| name           = San Andreas
| image          = San Andreas poster.jpg
| alt            =
| caption        = Teaser poster
| director       = Brad Peyton
| producer       = Beau Flynn   Hiram Garcia   Tripp Vinson Chad Hayes   Jeremy Passmore   Andre Fabrizio
| starring       = Dwayne Johnson   Alexandra Daddario   Carla Gugino   Kylie Minogue   Paul Giamatti
| music          = Andrew Lockington
| cinematography = Steve Yedlin
| editing        = Bob Ducsay
| studio         = New Line Cinema   Flynn Picture Company   Village Roadshow Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $100 million 
| gross          = 
}}
 adventure thriller thriller disaster disaster film Chad Hayes, based on the original script by Jeremy Passmore and Andre Fabrizio. The film stars Dwayne Johnson, Kylie Minogue, Carla Gugino, Alexandra Daddario, and Paul Giamatti.

Principal photography of the film began on April 22, 2014 in Australia and wrapped up on July 27 in San Francisco. The film will be released worldwide in 2D and 3D on May 29, 2015.

== Plot ==
After a devastating earthquake hits California, a Los Angeles Fire Department rescue-helicopter pilot (Dwayne Johnson) and his ex-wife (Carla Gugino) attempt to leave Los Angeles and head to San Francisco to find and rescue their estranged daughter (Alexandra Daddario).

== Cast ==
* Dwayne Johnson  as Ray
* Alexandra Daddario  as Blake
* Carla Gugino  as Emma, Rays estranged wife
* Colton Haynes  as Joby
* Art Parkinson  as Ollie
* Archie Panjabi  as Serena Todd Williams  as Marcus Crowlings
* Ioan Gruffudd  as Daniel Riddick
* Will Yun Lee  as Dr. Kim Chung
* Kylie Minogue  as Beth Riddick
* Paul Giamatti

== Production ==
=== Development === Chad Hayes were tapped by the studio to pen the film again, after Carlton Cuse and Allan Loeb re-wrote the script.    The film will be produced by New Line and Village Roadshow Pictures, along with Flynn Picture Company and Australian limited Village Roadshow.   

=== Casting === Todd Williams also joined the film. Hell play Marcus Crowlings, an old Army friend of Johnson.    On April 15, 2014, Colton Haynes was added to the cast of the film.    On April 29, Ioan Gruffudd joined the cast of the film. Gruffudd will play Daniel Reddick, a wealthy real estate developer who’s engaged to Johnson’s ex-wife.    On May 28, Will Yun Lee joined the cast to play Dr. Kim Chung, the co-director of the Cal-Tech Seismology Lab in the film.    On June 11, Australian actress Kylie Minogue joined the film.   

=== Filming === Variety reported Gold Coast, Ipswich and Gods of Egypt had started production in Australia, and San Andreas was set to begin soon after.  On April 16, 2014, Johnson tweeted photos from the training for the film.  
 Elizabeth Street in Brisbane. 
 Embarcadero on Lombard streets Russian Hill. The Armory. Fairmont Hotel, California Street Financial District,  which wrapped-up the shooting on July 27, 2014.

=== Music ===
On July 24, 2014, it was announced that Andrew Lockington would be composing the music for the film. 
 Sia singing "California Dreaming" by The Mamas and the Papas.

== Reception ==
=== Marketing ===
An image from the film featuring Johnson was revealed on March 17, 2014.  The first trailer for the film,  was released on the December 10, 2014. The second trailer for the film was released on March 9, 2015.

=== Release ===
On December 5, 2013, Warner Bros. set the disaster film for a June 5, 2015 release, and the film would be released in 2D and 3D.    Later on October 21, WB moved the films release date a week earlier to May 29, 2015. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 