Raja Bakthi
{{Infobox film
| name           = Raja Bakthi ராஜ பக்தி
| image          = Raja_bakthi.jpg
| image_size     = 
| caption        = Film Poster
| director       = K. Vembu
| producer       = P. Rajamanickam Chettiar
| writer         = Ku. Rajvelu M. S. Muthukrishna Vadhiyar
| story          = V. C. Gopalarathinam
| narrator       = Padmini Pandari Bai
| music          = G. Govindarajulu Naidu
| cinematography = R. Sampath
| editing        = K. Shankar
| studio         = Nandhi Productions
| distributor    = Nandhi Productions
| released       = 27 May 1960 
| runtime        = 150 minutes
| country        = India Tamil
| budget         =
}} 1960 Tamil historical fiction epic film written by V. C. Gopal Rathnam and directed by K. Vembu. The film starred Sivaji Ganesan, Vyjayanthimala and Bhanumathi Ramakrishna|P. Bhanumathi in the lead with T. S. Balaiah, Padmini (actress)|Padmini, Pandari Bai, M. N. Nambiar, E. V. Saroja and Stunt Somu as the ensemble cast, and was produced by P. Rajamanickam Chettiyar of Nandhi Pictures. The films music was composed by G. Govindarajulu Naidu and was filmed by R. Sampath. The film is about an ambitious Queen makes an unsuccessful attempt to rule her country by eliminating the King and Prince with the help of her Army Commander.  

==Production== Marathi writer Joshi and was adapted for the Marathi stage dramas. Made as Tamil movie in 1937 by V. C. Gopalratnam for Chellam Talkies and directed by Sundar Rao Nadkarni. The cast were B. Ramakrishnaiah Panthulu|B. R. Panthulu, Rathnabai, Saraswatibai, Pattabiraman and R. B. Lakshmidevi.

==Plot==
The film is about an ambitious Queen makes an unsuccessful attempt to rule her country by eliminating the King and Prince with the help of her Army Commander. General Vikranthan rescues the young prince and plans to retake the throne from the usurper and place the rightful heir on it.

==Cast==
*Sivaji Ganesan as General Vikrangkadhan
*Vyjayanthimala as Princess Mrinalini
*P. Bhanumathi as Empress Maharasai Padmini as Queen Sarojini
*Pandari Bai as Princess Devasena
*T. S. Balaiah  as King Dhurjeyan
*M. N. Nambiar as General
*E. V. Saroja as Dancer
*Stunt Somu as Princes Bodygourd

==Crew==
*Producer: P. Rajamanickam Chettiar
*Production Company: Revathi Productions
*Director: K. Vembu
*Music: G. Govindarajulu Naidu
*Lyrics: Ponmudi, A. Maruthakasi, Tamizh Azhagan and Ku. Rajavelu.
*Story: V. C. Gopalarathinam
*Screenplay: Ku. Rajavelu & M. S. Muthukrishna Vadhiyar
*Dialogues: Ku. Rajavelu & M. S. Muthukrishna Vadhiyar
*Art Direction: 
*Editing: K. Shankar
*Choreography: 
*Cinematography: R. Sampath
*Stunt: 
*Dance:

==Soundtrack== Playback singers are M. L. Vasanthakumari, P. Suseela, Jikki, Raavu Balasaraswathi|R. Balasaraswathi, Radha Jayalakshmi, K. Jamuna Rani & A. P. Komala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Lyrics || Length (m:ss)
|-
| 1 || Aararo Nee Aararo || Jikki || || 02:36
|-
| 2 || Ennai Konjam Paaru || A. P. Komala || || 02:25
|-
| 3 || Kadhal Tholvi || P. Susheela || || 02:40
|-
| 4 || Kadhal Vetri || P. Susheela || || 02:34
|-
| 5 || Kaliyuka Malarmaran || Radha Jayalakshmi || || 02:39
|-
| 6 || Karka Kasadara Katravai || M. L. Vasanthakumari || || 02:50
|-
| 7 || Par Muzhudum Irul Parappum || Raavu Balasaraswathi|R. Balasaraswathi || || 02:46
|}

==References==
 

==External links==
*  
*   at Upperstall.com

 
 
 
 
 