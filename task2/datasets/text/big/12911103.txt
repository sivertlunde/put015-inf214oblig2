Villu (film)
{{Infobox film
| name = Villu
| image = Villu-2.jpg
| director = Prabhu Deva
| story = AC Mugil Rebel Ravi Sachin Bhowmick  (uncredited) 
| screenplay = Prabhu Deva
| writer    = AC Mugil Rebel Ravi (dialogue) Vijay Nayanthara Ranjitha Prakash Raj
| producer = Ayngaran International|K. Karunamoorthy C. Arunpandian
| cinematography = Ravi Varman
| music = Devi Sri Prasad
| studio = Ayngaran International
| distributor = Ayngaran International
| released =  
| runtime = 150 minutes
| editing = Kola Bhaskar
| country = India
| language = Tamil.
| budget = 
| box office = 
}} action thriller thriller film Vijay in Geetha play supporting roles whilst Prabhu Deva, Mumaith Khan, Zabyn Khan, and Kushboo Sundar appear in item numbers. The film is produced and distributed by Ayngaran International and composed by Devi Sri Prasad. The movie was dubbed into Hindi as Ek Aur Jaanbaaz Khiladi and in Telugu as Yamakantri. It is a remake of the Hindi film Soldier (1998 Indian film)|Soldier starring Bobby Deol and Preity Zinta. The film was released on 12 January 2009 receiving mixed to negative reviews, and thus, ending up as a Below Average grosser at box office .  

==Plot==
 

Inspector Joseph (Manoj K. Jayan) is called in for a secret meeting by a police official. He learns that their long-awaited target, Raaka (Adithya), will be arriving secretly in a cargo shipment by sea and that he should be caught at the site. Joseph informs Pugazh (Vijay (actor)|Vijay) through a secret instant message that Raaka will be arriving at the harbour and tells him about their special operation to catch the culprit. Joseph and his team disguise themselves as harbour workers at the harbour and await for Raaka. Then Pugazh carries on with the special operation and succeeds in killing the culprit. Pugazh phones and informs Joseph that their "operation was a success". Later, Pugazh leaves to a Tehsildars house in a village for a wedding. He meets Janavi (Nayantara), and tries to woo her. Later on after persistent wooing Janavi falls for Pugazh.

On the occasion, Pugazh sends an instant message back to Inspector Joseph, saying that his next mission was complete, which was making Janavi fall in love with him. Joseph tells Pugazh it is now time to target Raakas superior commander, J. D. (Prakash Raj). Janavi informs her father J. D., a rich businessman in Munich, Germany, of her love with Pugazh. J. D. tells Janavi to bring Pugazh to Munich. Pugazh meets J. D. in Munich and is told that he is not worthy enough to marry his daughter. Pugazh tells J. D. that it was he who killed Raaka. Pugazh is told by Joseph to go to a hotel in Munich. After a conflict in the hotel with the hotels pimp (Sriman (actor)|Sriman), he is targeted by the pimp and his boss Shaan (Devaraj (actor)|Devaraj), who is J. D.s business partner in crime.

They follow Pugazh to the airport where Pugazh goes to pick up his mother (Geetha (actress)|Geetha), who lost his daughter (Kowsalya Ganesan) during his childhood. Shaan then decides not to shoot Pugazh, after seeing that Pugazhs mother is none other than his wife, who left him because of him being a corrupt army cadet and receiving money for helping the enemy terrorists of India. Shaan holds a party for finding his long-lost son, Pugazh. J. D. also comes to know that Pugazh is his partners own son. Shaan takes Pugazh to his secret locker room and hands him over his secret Blu-ray disc containing all the evidence of J. D. and his plans. Shaan tells Pugazh that he should retrieve the discs that J. D. has hidden in order to take over all his business, once he marries Janavi. Pugazh then takes his father on an airplane and tells him something that shocks him.They then fight and the plane crashes into a cliff, killing Shaan, while Pugazh escapes from the plane with the help of a parachute.

Pugazh meets J. D. and tells him that he killed Shaan. Pugazh also tells him that he was not really Shaans son and that he tricked him into believing that he was Shaans real son who was lost long ago. Pugazh tells J. D. that he will bring Shaans henchmen over, pretending to kill J. D. and eventually kill them instead. A happy J. D. tells Pugazh to proceed. Just then, J. D. records Pugazhs conversation with him and sends it to Shaans henchmen, telling them to kill Pugazh. When Pugazh brings them to J. D. s place, Pugazh is targeted by them and gunpoint and is threatened to tell them who he really is and why he killed Shaan. Then a few bombs set by Pugazh blasts and all of Shaans henchmen get killed and Pugazh escapes.

Pugazh sends the Blu-ray disc he received from Shaan to Inspector Joseph in India, who found Raaka alive (Pugazh simply poked Raaka with a knife and thought that Raaka died, but it wasnt so). Raaka injures Joseph and escapes from Josephs custody. Raaka then informs J. D. that he is alive. J. D. sends his men to bring Pugazh over. While Janavi, who is not aware of the conflict going on between Pugazh and J. D., listens to the two aconfronting each other, J. D. asks for the disc back from Pugazh. Janavi finally finds out about Pugazh and tells Shaans wife about Pugazhs identity. Shaans wife is already aware of Pugazhs true identity and knows that he is not her real son. She also tells Janavi the reason why he killed Shaan and why he is after J. D. and about Pugazhs real father, Major Saravanan (Vijay (actor)|Vijay).

Major Saravanan was an Indian Army officer, who was a fellow friend of Inspector Joseph. He was told to undertake a peacekeeping mission task force. Meanwhile, J. D. and Shaan, who were the corrupt army cadets seeking money from the terrorists and willing to help them, plan their means of killing Saravanan. During the mission, J. D. and his corrupt army men murder Saravanan while on duty. J. D. acts as an eyewitness account, telling the army commanders that they killed Saravanan, whom they claim was corrupt and tried to attack them while helping the terrorists, only for the safety of the others soldiers and themselves. Abiding the final court hearing regarding the incident, the army takes away Saravanans badges, uniform and titles at his funeral. Pugazhs mother (Ranjitha) tries to stop them, claiming that her husband was not a betrayer to the country,but in vain. When Saravanans body was to be cremated, the villagers throw out the body into a ditch, claiming that it was a sin to burn his body on their soil. Pugazh and his mother cry as they could not find Saravanans body in the blowing sand. Pugazhs mother sits in a temple that night with Pugazh. She sends Pugazh off with Shaans wife, fearing the village might accuse Pugazh to be a "child of a national criminal. " Pugazh and his mother did not see each other since then.

In the present, J. D. takes Pugazh to India to retrieve the Blu-ray disc back. They meet Joseph who is at gunpoint by Raaka. When J. D. s henchmen get out of a car to get the disc, Pugazh flees with J. D. to the temple where his mother is at. After an emotional moment with his aged mother, J. D. s men arrive at the place. They kill Inspector Joseph and hurt Pugazh. Pugazh becomes weak, unable to fight, and J. D. buries him alive in the sand. Pugazhs mother cries out loud to God to save his son. Just then, heavy winds blow, slowly blowing the sand away. Pugazh then jumps out of the sand with rage. J. D. then fights an intense battle with Pugazh. Now, J. D. is weakened by Pugazh and the villagers began to gather around them. Pugazh ask  J. D. to tell the villagers the truth. J. D. tells the villagers that Major Saravanan was actually an honest man and it was himself who helped the terrorists. After his confession, Pugazh kills J. D. at once. The films credits role as the army force gives back Saravanans army badge and uniform to Pugazhs mother.

==Cast==
  Vijay as Pugazh and Major Saravanan.
* Ranjitha as Major Saravanans wife/Pugazhs Mother
* Nayanthara as Janavi, J. D.s daughter
* Prakash Raj as J. D., the one who killed Pugazhs father, Saravanan
* Vadivelu as Maadaswamy {Maada}, the audiographer
* Manoj K. Jayan as Inspector Joseph, Saravanans relative Devaraj as Shaan, who was involved in the murder of Pugazhs father
* Anand Raj as J.Ds and Shaans Henchman Sriman as Max/Gandy Geetha as Shaans Wife
* Vaiyapuri as Pugazhs Friend
* Dhamu as Pugazhs Friend Aarthi as Janavis Friend
* Raj Kapoor
* Varnika as Janavis Friend
* T. P. Gajendran as Marriage guest
* Mumaith Khan in a special appearance in the song "Daddy Mummy"
* Kushboo Sundar in a Guest Appearance in song "Rama Rama"
 

==Production==
 ]]
Prabhu Deva was interested in remaking the Bollywood film Soldier in Tamil. Along with announcing his venture in Bollywood titled Wanted Dead and Alive, which was the remake of Pokkiri, he launched his next Tamil venture in June 2008. Ayngaran International was to produce the film as well. The film was initially titled as Pugazh.    Prabhu Deva later announced the titled to be Singam. However, a copyright issue was brought up concerning the title Singam, which was already announced as the title for a film by director Hari (director)|Hari.  Prabhu Deva then changed the titled to Vill, meaning "bow" in Tamil, a more formal spelling of Villu. The team later found out that S. J. Suryah was to use the title Vill for a Telugu film. Suryah, with the negotiation of Vijay, later changed his films title    The title Villu subsequently became the films official name. 

Actress Ranjitha was selected to play Vijays mother. 
Director Prabhu Deva started shooting for  Villu with the teams first location being Palani, Tamil Nadu.  The teams second location was set to be in Karaikudi. Prabhu Deva had reportedly planned two song sequences to be shot in European countries.  Another song sequence was shot in Bangkok, Thailand. Later, reports claimed that the films script and story would deal with a majority of the film taking place in Italy. Prabhu Deva had reportedly planned two song sequences to be shot in European countries.  In an interview with Prabhu Deva, a month prior to the films release, Prabhu Deva stated the film will be a "Tamil version of a James Bond film."  It was reported that Prakash Raj was missing from shooting when the crew was shooting the climax. 

Devi Sri Prasad, Kola Bhaskar and Ravi Varman were confirmed the films film composer|composer, editor, and cinematographer respectively while FEFSI Vijayan was chosen as the stunt coordinator. 
 Vijay was Napoleon were to be given a supporting role, but later the role went to Manoj K. Jayan. Vaiyapuri, Kushboo Sundar and Kovai Sarala were also said to be given roles in the film. However, Sundar was later confirmed an item number appearance while Sarala was to sing a song in the film.

The video containing Vijay fighting at ice factory was leaked in the net. 

==Megacow==

The Megacow is an anthropomorphic Bull that fights Vadivelu in the Pasture in the scene where he gets lost. It is one of the few Indian Movie Monsters to appear in filim, aside the Rakshashas.

==Soundtrack==
{{Infobox album |  
 Name = Villu
| Type = Soundtrack
| Artist = Devi Sri Prasad
| Cover =
| Released = 14 December 2008
| Recorded = Feature film soundtrack
| Length =
| Label = Ayngaran Music An Ak Audio 
| Producer =
| Reviews =
| Last album = King (2008 film)|King (2008)
| This album = Villu (2009)
| Next album = Arya 2 (2009)
}}

The soundtrack for this film was composed by Devi Sri Prasad.
{| class="wikitable"
|- style="background:#cccccf; text-align:center;"
| No. || Song || Singers || Length (min:sec) ||Lyrics ||Picturization
|- Vijay and Kushboo, Prabhu Deva
|- Vijay and Nayantara
|- Vijay and Nayantara
|-
| 4|| "Daddy Mummy" ||   and Zabyn Khan
|- Vijay and Nayantara
|-
| 6|| "Jalsa Jalsa"  (Remix) || Devi Sri Prasad, Baba Sehgal || 04:00 || Rohini ||
|- Vijay and Nayantara
|-
| 8|| "Vaada Maappilley" ||   || Vijay, Nayantara and Vadivelu
|}

==Release==
The satellite rights of the film were sold to Kalaignar TV|Kalaignar.

==Reception==
This film released on 12 January 2009 on the same day Prabhu Deva and Vijays previous film Pokkiri (2007 film)|Pokkiri had released. Produced at a cost of  17 crore, the film received mainly negative reviews.  Behindwoods.com gave 1.5 on 5 and said "With tacky production values, shabby cinematography and amateurish direction Villu comes across as a more than two-hour long." while another website stated "On the whole, Vijays Villu is an action-packed mass masala film for his ardent fans but with loose ends."Rediff reviewed the film "Leave your brains behind and prepare to enjoy the adventures of a Tamil James Bond and rated 3/5".  IndiaGlitz called it "loose strings with style". 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 