The House on Trubnaya
 
{{Infobox film
| name           = The House on Trubnaya
| image          = The House on Trubnaya.jpg
| image_size     =
| caption        = Film poster
| director       = Boris Barnet
| producer       =
| writer         = Nikolai Erdman Anatoli Marienhof
| starring       = Vera Maretskaya
| cinematography = Yevgeni Alekseyev
| editing        =
| distributor    =
| released       =  
| runtime        = 64 minutes
| country        = Soviet Union
| language       = Russian
| budget         =
}}

The House on Trubnaya ( , Transliteration|translit.&nbsp;Dom na Trubnoy) is a 1928 comedy film directed by Boris Barnet and starring Vera Maretskaya.    

==Cast==
* Vera Maretskaya - Parasha Pitunova - housemaid
* Vladimir Fogel - Mr. Golikov - hairdresser
* Yelena Tyapkina - Mrs. Golikova
* Sergei Komarov - Lyadov
* Anel Sudakevich - Marisha-maid
* Ada Vojtsik - Fenya
* Vladimir Batalov - Semyon Byvalov - chauffeur
* Aleksandr Gromov - Uncle Fedya
* Vladimir Uralsky (as V. Uralsky)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 