Something for the Boys (film)
{{Infobox film
| name           = Something for the Boys
| image          = Something for the Boys (1944).jpg
| image size     = 250px
| alt            =
| caption        =
| director       = Lewis Seiler
| producer       = Irving Starr
| writer         = Herbert Fields Dorothy Fields
| based on       = Cole Porter Broadway musical Robert Ellis Helen Logan Frank Gabrielson Michael OShea
| music          = Leigh Harline Cyril J. Mockridge Ernest Palmer Robert L. Simpson
| studio         =
| released       =  
| distributor    = 20th Century-Fox
| runtime        = 87 minutes
| country        = United States
| language       = English
}} 1944 musical Michael OShea, Vivian Blaine, Phil Silvers, Sheila Ryan and Perry Como.
 Robert Ellis, eponymous Broadway Oscar for Born Yesterday, makes an brief appearance in this film.

==Plot== Michael OShea), who was a well-known orchestra leader before joining the military, arrives with some of his men, including Sgt. Laddie Green (Perry Como). Rockie explains that the married service men of nearby Camp Dixon want to rent rooms in the manor for their wives, who have been unable to live close to their husbands due to a lack of available housing. The men pitch in and help the cousins fix up the manor, although Chiquita is continually bothered by the fact that she can pick up radio programs on the fillings in her teeth. Rocky, who has begun a romance with Blossom, suggests that they put on a musical show to raise funds for the renovations. On the day that "The Old Southern Corn Revue" is to open, Blossom is stunned by the arrival of Melanie Walker (Sheila Ryan), a snobbish, rich woman, who Rocky is forced to admit is his fiancé. Melanie, believing that Rocky has arranged for the manor to be let just for her, imperiously announces the changes she intends to make, and the infuriated Blossom refuses to speak to Rocky. Before the show, Rocky explains to a disapproving Chiquita that he loves only Blossom. The show is a big success, and the next morning, Chiquita advises Blossom to fight for Rocky if she loves him. The snooty Melanie ends up covered with eggs after she tries to work one of Harrys new inventions, and she seeks solace from Lt. Ashley Crothers (Glenn Langan). While the lieutenant is there, he discovers that Harry is hosting a dice game for some of the soldiers, none of whom have wives staying at the manor. Crothers arrests the soldiers and recommends that the house be posted as off-limits for all military personnel. Col. Grubbs approves Crothers suggestion, and soon the wives are packing to leave. Meanwhile, after Harry learns that carborundum from the defense plant got into Chiquitas fillings and is causing her to receive radio programs, he decides to build an invention around the idea. One afternoon, Rocky comes to the house to try to talk to Blossom, who refuses to see him. Rocky is supposed to be on duty for war games, and is captured by the "enemy" army, which has taken over the manor as its headquarters. Hoping to save both his stripes and the manor, Rocky enlists the aid of Chiquita and Harry, who begin building a transmitter to send a message to Rockys unit via Chiquitas teeth. The message is sent, and the cousins distract the "enemy" army with a song and dance show while Rockys army assembles for its attack. Soon after, Rockys side has prevailed in the maneuvers, and in appreciation of Blossom, Chiquita and Harrys help, the off-limits sign is removed and the manor is once again the site of much happiness for the military men and their wives. At a celebratory party, Rocky announces that he has been selected for officers candidate school, and the happy Blossom reconciles with him. 

==Production==
  in a scene cut from the movie.]] of the same name, which featured songs by Cole Porter and was a starring vehicle for Ethel Merman, who played Blossom. However, the film version uses only the title song from Porters score, and otherwise features no music from the stage show. In addition, Chiqutas role was expanded for Miranda: in the musical, it is Blossom who receives messages from her fillings. 
 Michael Todd and Savoy Productions for production of the musical, then purchased the screen rights to it in 1943. Although the legal records give the purchase price as $265,000, a 12 Mar 1943 HR news item lists the amount paid by Fox as $305,000. Per the agreement with Todd, Fox could not release the film until at least the summer of 1944, which, as HR noted, was to "enable the stage original to play the key cities and tour without competition from the celluloid version." Only one of Cole Porters songs for the musical was included in the film version. HR news items note that William Perlberg was originally slated to produce this film, Irving Cummings was to direct it and Betty Grable was to star in it. In Jan 1944, H. Bruce Humberstone was assigned to direct the picture and Brenda Marshall was set for the female lead. According to HR, Humberstone auditioned The Jeepers, a seven-piece novelty orchestra, but they do not appear in the finished film. Although a 23 Feb 1944 HR news item noted that dance director Nick Castle was working with Carmen Miranda to prepare a "four-movement, symphonic treatment" of the popular song "Mairzy Doats," the number was not included in the film. In Apr 1944, HR noted that Scott Elliott had been tested for the film, but his participation in the finished picture has not been confirmed. Although a HR news item and studio press releases include Billie Seward, Stanley Prager, Chester Conklin, Harry Seymour and Jo-Carroll Dennison in the cast, they do not appear in the completed film. The picture marked the screen debuts of popular singer Perry Como and actor Rory Calhoun, who appeared under the name Frank McCown. Modern sources also include Judy Holliday in the cast. A studio credit sheet lists the films running time as 78 min.
 , Phil Silvers and Vivian Blaine in a scene from the film.]] Greenwich Village, a 1944 Twentieth Century-Fox picture starring Carmen Miranda, to write Mirandas dialogue for this film, because "they wrote especially for her, with mispronunciations, etc., and she is very funny when she is given this style of writing." The Greenwich Village screenwriters did not contribute to Something for the Boys, but Mirandas trademark mangling of the English language is included in the film. The studio records also note that the "Southland Routine," which is performed by Phil Silvers, includes excerpts from the following songs: "Southland" by Silvers, Harold Adamson and Jimmy McHugh; "Dixies Land" by Dan Emmet; "All Over Gods Heaven," traditional spiritual; "Shortnin Bread," words by Jacques Wolfe, music traditional; "Indian Dance" by Urban Theilman; and "Climin Up Dem Golden Stairs" by McHugh and Adamson. The studio records contain letters from songwriters Jule Styne and Sammy Cahn, who stated that the "Southland Routine" was based on their work. In early 1945, Twentieth Century-Fox paid the composers three thousand dollars not to pursue their claim. According to a 3 Feb 1943 HR news item, owners of the radio show The Court of Missing Heirs filed an infringement of copyright lawsuit against the producers and owners of the play Something for the Boys. The owners of the radio program alleged that the play infringed on their shows premise. The disposition of the suit is unknown. 

== Cast ==
* Carmen Miranda as Chiquita Hart   Michael OShea as Sergeant Ronald Rocky Fulton  
* Vivian Blaine as Blossom Hart  
* Phil Silvers as Harry Hart  
* Sheila Ryan as Melanie Walker  
* Perry Como as Sergeant Laddie Green  
* Glenn Langan as Lieutenant Ashley Crothers

 
  Carmen Miranda Michael OShea Vivian Blaine Phil Silvers Sheila Ryan Perry Como
 
 

==Release==
The film was released on November 1, 1944.

=== DVD release ===
The film was released on DVD in June 2008 as part of Foxs "The Carmen Miranda Collection." 

=== Critical reception ===
On the films release on November 1, 1944, Writing for The New York Times, Bosley Crowther said that Something for the Boys features a "variety of musics, much gay humor and a superabundance of beautiful girls. As for tempestuous Miss Miranda, she is still rather fearful to behold, and sings (...) as a human radio set."  The Time (magazine) |Time magazine wrote that the film "turns out to have nothing very remarkable. There is not Carmen Miranda." {{cite news|url=http://content.time.com/time/magazine/article/0,9171,883929,00.html|title=Cinema, Also Showing Dec. 11, 1944
|date=December 11, 1944|work=|page=Time (magazine) |Time|accessdate=March 10, 2014}}  
 
"It should be more fun than it is," wrote Dave Kehr in the Chicago Reader.  Variety (magazine)|Variety thought that it was "sufficiently diverting and tuneful to warrant more than moderate success at the box-office." Perry Como, the magazine said, "makes a good appearance before the cameras" and his two songs were "quite listenable and well sold."  

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 

 