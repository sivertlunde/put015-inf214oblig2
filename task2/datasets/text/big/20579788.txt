Puraskar
{{Infobox film
| name           = Puraskar
| image          = Puraskar.jpg
| image_size     = 
| caption        = 
| director       = 
| producer       = 
| writer         =
| narrator       = 
| starring       =Joy Mukherjee and Farida Jalal
| music          =R.D. Burman  S.H. Bihari (lyrics)
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1970
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1970 Bollywood action film. The film stars Joy Mukherjee and Farida Jalal.

==Cast==
*Abhi Bhattacharya ...  Mr. Das / Mr. Dinanath
*Faryal ...  No.7 / Anita
*Bipin Gupta
*Rajan Haksar ...  Raghu
*Helen Jairag Richardson|Helen...  Rita
*Farida Jalal ...  Reshma
*I. S. Johar ...  Sumesh Ram Kumar
*Joy Mukherjee ...  Rakesh
*Sapana ...  Renu Das

Rakesh and Sumesh are two plain-clothes agents in the citys Criminal Investigations Department. They have been entrusted the task of locating a missing, believed to be abducted scientist, Mr. Das. They are assisted in this search by Renu Das, the daughter of Mr. Das. They manage to locate Mr. Das, and bring him safely to his house. His daughter is delighted to have her dad back. But then a series of odd coincidences lead Renu to conclude that her father has changed for the worse since she last saw him.

==Music==
* ae meri jaan chaand sa gora mukhda aapka (Asha Bhosle, Mukesh)
* dekh to kya hai aaj ki mehfil (Asha Bhosle)
* nateeja hamaari mohabbat ka kya hai (Asha Bhosle, Mukesh)
* nazar mein bijli badan mein shole (Mahendra Kapoor)
* yeh nasheeli yeh nasheeli meri aankhen (Asha Bhosle)

==External links==
*  

 
 
 
 
 


 
 