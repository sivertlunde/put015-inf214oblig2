The Green Promise
{{Infobox film
| name           = The Green Promise
| image          =Poster of the movie The Green Promise.jpg
| image_size     = 
| caption        =
| director       = William D. Russell
| producer       = Monte Collins (producer) Glenn McCarthy (executive producer) Robert Paige (producer)
| writer         = Monte Collins (screenplay and story)
| narrator       =
| starring       = See below
| music          = Rudy Schrager John L. Russell
| editing        = Richard W. Farrell
| distributor    = RKO Pictures
| released       =    |ref2= }}
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Green Promise is a 1948 American film directed by William D. Russell.  The film was co-produced by Houston oilman Glenn McCarthy and leading man Robert Paige the display to concept and meaning of the 4-H Club and highlight farming issues such as soil erosion, government programs, and individual enterprise. The title refers to a scene where the local preacher gives a sermon on a story from the Book of Exodus of Gods "green promise" to lead Moses into a land of milk and honey. The preacher reminds his congregation that the fulfillment of the promise requires faith, difficult journeys and open-mindedness.

It is currently in the public domain.

==Plot Summary==

It tells  the story of a farmer, Matthews, his son and three daughters. Leaving behind a failing farm, the family travel to pastures new. Help is proffered by agricultural county agent, David Barkley, who is immediately attracted to Matthewss eldest daughter, Deborah. Needing Deborah to run his house, and fearing David may take her away, Matthews refuses Davids advice and help. He is pig-headed and sleeps through the pastors sermon on education, understanding and tolerance. It is Susans (Matthewss youngest daughter) yearning ambition to raise lambs. Though only ten years old, she joins the 4-H club, secures a loan from the bank and buys two lambs. She cares for them like she is a mother. Barkley strongly advises Matthews against chopping down the forest atop a hill on his land. Matthews goes ahead and sells the land to a logging company. The forest gone, a great storm comes and washes the mud down the incline towards the homestead. Fearful for her beloved lambs, Susan braves the dangerous conditions to rescue the lambs. David returns and rescues both Susan and the lambs. Deborah is atop the hill trying to move rocks in order to build a dam to save their home from the torrent of water. David arrives and pulls her back just as the land falls away. Realizing how close to harm his daughters have come because of his ignorance and selfishness, Matthews apologizes to his family and welcomes David into their home. The community of 4-H members arrive to help them clean up the land following the storm damage.

==Cast==
*Marguerite Chapman as Deborah Matthews
*Walter Brennan as Mr. Matthews
*Robert Paige as David Barkley
*Natalie Wood as Susan Anastasia Matthews
*Ted Donaldson as Phineas Matthews
*Connie Marshall as Abigail Matthews Robert Ellis as Peter "Buzz" Wexford
*Jeanne LaDuke as Jessie Wexford
*Irving Bacon as Julius Larkins
*Milburn Stone as Rev. Jim Benton
*Geraldine Wall as Mrs. Wexford

==Production==
 Goldwyn Studios, Natalie Wood was supposed to run across a bridge which would collapse after she reached the opposite side into the torrent of water beneath. However, the stunt went wrong and the bridge collapsed while Natalie was still on it. She fell into the water, banging her left wrist bone as she fell. Though she managed to pull herself back up, the young actress was shaken and her wrist was broken. The bone was not set correctly and healed so that it protruded from the wrist. She would thereafter disguise the protruding bone with a bracelet.

A nationwide contest for a 4-H Club girl to have a small part in the picture was won by Jeanne LaDuke who shared a tutor with Natalie. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 