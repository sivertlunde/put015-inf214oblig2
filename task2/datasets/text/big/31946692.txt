Thin Ice (2011 film)
{{Infobox film
| name = Thin Ice
| director = Jill Sprecher
| image	=	Thin Ice FilmPoster.jpeg
| producer = Mary Frances Budig Elizabeth Redleaf Christine K. Walker
| writer = Jill Sprecher Karen Sprecher
| starring = Greg Kinnear Alan Arkin
| music = Jeff Danna Dick Pope
| editing = Lee Percy
| studio  = Werc Werk Works
| distributor = ATO Pictures
| released =  
| runtime = 114 minutes   93 minutes  
| country = United States
| language = English
| budget = 
| gross =
}}
Thin Ice (formerly The Convincer) is a 2011 comedy-drama film starring Greg Kinnear, Alan Arkin and Billy Crudup, directed by Jill Sprecher. 

==Plot==
Wisconsin insurance salesman Mickey Prohaska is in desperate financial straits. His wife JoAnn has thrown him out of the house, and he is willing to do or say practically anything to sell anybody a policy. Mickey accompanies another insurance agent he has just met, Bob Egan, to the farm of an elderly man named Gorvy Hauer. The house is a mess and Gorvy is absent-minded, perhaps a tad senile. He does not have much money and, the little he has, he seems to keep in a jar. Mickey tries to sell the old man some insurance anyway.

Gorvy also has an old violin. A man named Dahl, an appraiser from Chicago who has a shop filled with musical instruments, examines the violin and says it is somewhat rare and actually worth $25,000. Complications arise when Mickeys plan to steal the instrument is discovered by a small-time ex-con named Randy Kinney, who not only will not leave Mickey alone, but turns violent and bludgeons one of Gorvys neighbors to death.

Continued inspection and research on the violin values it at forty times the original estimate. Mickey now is in possession of a million-dollar instrument, but Randy wants his share and threatens to kill Mickey if he does not get it. Mickey thinks he can still come out of this all right, until he discovers that he is in way over his head.

==Cast==
*Greg Kinnear as Mickey Prohaska
*Alan Arkin as Gorvy Hauer
*Billy Crudup as Randy Kinney
*Lea Thompson as Jo Ann Prohaska
*Bob Balaban as Leonard Dahl
*David Harbour as Bob Egan
*Michelle Arthur as Karla Gruenke
*Mike Hagerty as Jerry

==Production==
Filming began in February and ended in March 2010, and, though set in Wisconsin, was shot during winter in Minnesota, particularly Bloomington, Minnesota|Bloomington, St. Paul, Minnesota|St. Paul and Minnetonka, Minnesota|Minnetonka.

==Re-editing controversy==
Entitled The Convincer, the initial edit of the film was screened in January 2011 at the Sundance Film Festival. Distribution rights were purchased by ATO Pictures. Both ATO and production company Werc Werk Works demanded that director/writer Jill Sprecher make sweeping changes to the film from the version shown at Sundance in order to speed the film up. The production company claims that Sprecher "refused to be part of the process" while Sprecher states that she was never allowed to review the distributors notes to make any comments on them.  The production company and distributor made the revisions without Sprechers input, replacing the original composer, Alex Wurman, and the original editor, Stephen Mirrione.  Sprecher is currently not allowed to speak to the press about the situation for legal reasons, but she previously stated she is "heartbroken and devastated" and she would remove her name from the film, but under the terms of her contract she is not allowed to do so. The completely re-cut film was retitled Thin Ice.  Sprecher reportedly learned of the name through the internet and not from the production company. 

==Reception==
The Convincer received generally positive reviews from critics at the Sundance Film Festival. Kyle Smith of the New York Post said in his review; "Kinnear is ideal for this role" and ultimately The Convincer turns out to be an ambitiously structured and clever scam movie".  Rob Nelson of Variety (magazine)|Variety said; "there are pitch-perfect comic notes from the whole ensemble (...) Stephen Mirriones editing hits all the right beats, and Popes brilliantly composed widescreen images of the Wisconsin tundra are as bright as any noirs could be". 

The re-cut Thin Ice received uniformly negative responses ("disappointing," "poorly edited," "a stinker") after screenings at B-List festivals in October 2011. 

Thin Ice received positive responses from critics with a "fresh" rating of 71% on Rotten Tomatoes, based on 59 reviews. 

==Release==
The film premiered locally in Minneapolis at the Walker Art Center on October 12, 2011, completely re-cut and retitled as Thin Ice.

Following the further festival run, the film was released theatrically in February 2012.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 