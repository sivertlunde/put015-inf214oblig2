The Abduction Club
 
{{Infobox film
 | name        = The Abduction Club
 | caption     = The Abduction Club film poster
 | image	=	The Abduction Club FilmPoster.jpeg
 | writer         = Richard Crawford, Bill Britten
 | starring       = Alice Evans Daniel Lapaine Sophia Myles Matthew Rhys director       = Stefan Schwartz producer       = David Collins
 | distributor = Pathe Distribution (UK)
 | released    = 19 July 2002 min
 English
 | budget      =
 | music       = Shaun Davey
 | cinematography  = Howard Atherton
 | awards      =
}}
The Abduction Club is a British film directed by Stefan Schwartz. Based loosely on real events, the plot centres on a group of outlaws who abduct women in order to marry them. It was written by Richard Crawford and Bill Britten.

==Synopsis==
Set in 18th century Ireland, the story follows the exploits of two financially destitute young bachelors, Garrett Byrne and James Strang. Both being younger sons, and therefore not eligible to inherit titles and estates, they become members of an infamous society known as the Abduction Club, whose main aim is to woo and then abduct wealthy heiresses in order to marry them (therefore providing themselves with financial security). The men decide to set their sights on the beautiful yet feisty Kennedy sisters, Catherine and Anne, but are unprepared for the negative reaction they are to receive, and they soon find themselves on the run across the glorious Irish countryside (with the sisters in tow) from Annes cold-hearted admirer, John Power, who doesnt take kindly to the news of their kidnapping, and with the help of the embittered Attorney General Lord Fermoy, implicates Byrne and Strang in the murder of a Redcoat soldier.

==Cast==
*Alice Evans as Catherine Kennedy
*Daniel Lapaine as Garrett Byrne
*Sophia Myles as Anne Kennedy
*Matthew Rhys as James Strang Liam Cunningham as John Power
*Edward Woodward as Lord Fermoy
*Patrick Malahide as Sir Myles Tom Murphy as Knox

==External links==
* 

 
 
 
 