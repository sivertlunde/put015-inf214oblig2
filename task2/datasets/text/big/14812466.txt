The Damned United
 
 
 
{{Infobox film
| name           = The Damned United
| image          = The damned united poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Tom Hooper
| producer       = Andy Harries Grainne Marmion
| screenplay     = Peter Morgan
| based on       =  
| starring       = Michael Sheen Timothy Spall Colm Meaney Jim Broadbent
| music          = Rob Lane
| cinematography = Ben Smithard
| editing        = Melanie Oliver Screen Yorkshire
| distributor    =Sony Pictures Classics 
| released       =  
| runtime        = 97 minutes 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $4,091,378 (worldwide)   
}} sports drama Leeds United Screen Yorkshire and Columbia Pictures. Sony Pictures Entertainment distributed the film. The film was originally proposed by Stephen Frears, but he pulled out of the project in November 2007. Hooper took his place and film was shot from May to July 2008. The film marks the fifth collaboration between screenwriter Peter Morgan and actor Michael Sheen. The film was released in the United Kingdom on 27 March 2009, and in North America on 25 September.

==Plot== Leeds United. Derby County Peter Taylor (Timothy Spall), has not joined him.
 1968 FA First Division Dave Mackay League championship Stephen Graham) sarcastically wishes Clough well for the semi-final. Juventus defeat them 3-1, and Clough publicly lambasts Longson. 
 Brighton & Hove Albion. They agree to take the jobs after taking an all-expenses-paid holiday in Majorca. During the holiday that summer, Clough agrees to take control of Leeds after being approached by their representatives.  Taylor, however, argues the case for staying at Brighton, and after a bitter quarrel, the two go their separate ways.

Back in the storylines "present”. Clough alienates his players in their first training session, first by telling them that they can throw away any awards they have won because they "never won any of them fairly", and then making them start with a 7-a-side game, which Bremner points out Don Revie never made them do. Clough reminds them that he is not Mr Revie and threatens a severe punishment for any player who mentions the former managers name or methods again.
 Charity Shield Liverpool at season and are in danger of relegation only one season after winning the title. After Bremner and the players air their grievances to the board, the club terminates Cloughs contract after just 44 days - though he forces them to pay an enormous severance package. Afterwards, Clough agrees to do a final interview with Yorkshire Television, but finds Revie there to confront him, bringing the two face to face at last. Clough accuses Revie of being cold-hearted and fundamentally dishonest, both as a person and a football manager, and Revie in turn brands Clough as inflexible and egocentric. Clough brings up the incident in the 1968 FA Cup, and Revie claims to have not known who the rookie manager was at the time (a doubtful claim considering that Revie was known for meticulously researching every opponent his team faced). After the interview, Clough drives down to Brighton to patch things up with Taylor. It involves Clough literally on his knees, grovelling at Taylors dictation, but they reconcile.
 Nottingham Forest, where he repeated his prior achievements with Derby by taking them up and winning the First Division, and this time bettered both Revie and his own spell at Derby by winning two European Cups in succession in 1979 and 1980. The film ends by branding Clough "the best manager that the English national side never had".

==Cast== Derby County Leeds United Peter Taylor, Hartlepool United Brighton & Hove Albion Jimmy Gordon, Cloughs assistant at Leeds and trainer at Derby 
* Elizabeth Carling as Barbara Clough, Cloughs wife 
* Oliver Stokes as Nigel Clough, Cloughs younger son
* Ryan Day as Simon Clough, Cloughs elder son
* Peter Quinn as referee Bob Matthewson 

;Leeds United England manager
* Henry Goodman as Manny Cussins, Leeds chairman 
* Jimmy Reddington as Keith Archer, Leeds general manager
* Liam Thomas as Les Cocker, Revies trainer at Leeds and England David Harvey
* Lesley Maylett as Paul Reaney
* Chris Moore as Paul Madeley
* John Savage as Gordon McQueen Norman Hunter
* Tom Ramsbottom as Trevor Cherry
* Matthew Storton as Peter Lorimer Peter McDonald as Johnny Giles, Revies recommended replacement  Stephen Graham as Billy Bremner, Leeds captain Mitchell, Wendy (21 June 2008). " ", ScreenDaily.com, Emap Media. Retrieved on 24 October 2008. 
* Bill Bradshaw as Terry Yorath Eddie Gray Allan Clarke Joe Jordan
* Joe Dempsie as Duncan McKenzie

;Derby County
* Jim Broadbent as Sam Longson, Derby chairman Dave Mackay, Derby player and later manager
* Martin Compston as John OHare, Derby and later Leeds player John McGovern, Derby and later Leeds player
* Giles Alderson as Colin Todd, Derby player
* Stewart Robertson as Archie Gemmill, Derby player
* Laurie Rea as Terry Hennessey, Derby player 
* Tomasz Kocinski as Roy McFarland, Derby player

;Minor characters
* Mark Bazeley as Austin Mitchell
* Mark Jameson as head groundsman
* David Stevenson as reporter
* Nathan Head as reporter
* Chris Wilson as FA disciplinary
* Ralph Ineson as a crazy wild-haired reporter
* William Martyn Conboy as Key Reporter, Revie Departure Scene

==Production==

===Development=== The Deal and The Queen. He was chosen because of his physical resemblance to Clough. Staff (16 February 2007). " ", BBC News website (BBC News). Retrieved on 24 April 2008.  When Frears suggested to Sheen that he play the part, Sheen "rolled his eyes and burst into a wonderful impersonation" of Clough. Solomons, Jason (11 November 2007). " ", The Observer, Guardian News and Meda. Retrieved on 24 April 2008.  Sheen said Clough was "one of those people whos decided hes going to shape the rest of the world in his image. Inevitably theres something in us that recognises that thats playing with fire and the gods will have to strike you down." 
 Northern Counties East League, Northern Premier League, or higher. Casting for extras took place on 20 and 21 May. " ". Chesterfield F.C. website (16 May 2008). Retrieved on 17 May 2008. 

===Filming===
  for filming.]] Studios in Beeston and Adel, Leeds|Adel.  The training ground used by Derby County was the quarry football pitch which is nearby to the Elland Road Stadium.

During the week of 23 June, filming took place in Scarborough, North Yorkshire|Scarborough, in place of Brighton.  Interior scenes were filmed in the Victoria Sea View Hotel and the Esplanade Hotel.   Exteriors were filmed on the Queens Parade and at nearby Scalby Mills. Computer-generated imagery was added in post-production to make Scarborough look like Brighton. Filming then moved on to Saddleworth before concluding in Majorca. Beever, Kirsty (26 June 2008), " ", Scarborough Evening News, Johnston Press. Retrieved on 2 September 2008. 

===Music===
Rob Lane composed the film score in December 2008. 

==Release== FA Cup Derby County Nottingham Forest. 

The distribution rights were originally pre-sold to Ealing Studios International. Sony Pictures Entertainment made a higher offer to the production companies and will now distribute the film worldwide.  It was released in the UK on 27 March 2009. 

A gala screening of the film was held at the 2009 Toronto International Film Festival in September 2009; the film went on limited release in the United States on 9 October that year. 

The film earned a total of $US3,604,339 in the UK and Republic of Ireland, and in Canada and the US, it took $US441,264; the worldwide box office take was $US4,045,603. 

==Reception==
The Damned United was generally well received by film critics. It currently holds a 94% fresh rating on Rotten Tomatoes  and an overall rating of "universal acclaim" on Metacritic.  Roger Ebert gave the film three and a half stars out of four, and praised Sheen for portraying "modern British icons so uncannily that hes all but disappeared into them". Peter Bradshaw of The Guardian gave the film four stars, calling it "fresh, intelligent...   terrifically involving", and also praised Sheen and Meaneys performances.  Empire (film magazine)|Empires reviewer William Thomas awarded The Damned United three stars out of five. He praised the film for "capturing the emotional toil of football", although added that it "struggles to find its stride". 
 Nigel said he did not intend to watch the film and that those in football who had seen it had told him it bore "no resemblance" to what actually happened.  Sonys decision to release the film six days after what would have been Cloughs 74th birthday was also criticised. Staff (1 December 2008). " ", Derby Evening Telegraph, Derby Telegraph Media Group. Retrieved on 5 December 2008. 

Producer Andy Harries responded to the Clough familys criticisms by stating that "The filmmakers" goal is to tell a wonderful and extraordinary story with universal themes of success, jealousy and betrayal". Harries added that without adding fictional elements the film would not have been as exciting to watch.    He also reassured Cloughs family that the film would be a more sympathetic portrayal of Clough than in the book. Writer Peter Morgan claimed that he did not feel the films accuracy was of major importance. 
 Peter Taylor. He praised the performance of the actors, however, particularly that of Sheen.  BBC Sport journalist Pat Murphy, a personal friend of Clough, noted 17 factual inaccuracies in the film, including various errors regarding the timing of events. He also dismissed as "absolute nonsense" a scene where Clough stays in the Derby dressing room during a match against Leeds, too nervous to watch.  {{cite AV media people     = Russell Fuller (presenter) date      = 18 March 2009 title      = 5 live Sport url        = http://www.bbc.co.uk/fivelive/programmes/fivelivesport.shtml medium     = Radio broadcast publisher  = BBC Radio 5 Live accessdate = 18 March 2009
}}  

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
;Video interviews
*  . talkSPORT Magazine (15 January 2009)
*  . Screen Yorkshire (March 2009)

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 