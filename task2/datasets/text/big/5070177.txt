On the Border of Hopetown
 
  Australian short produced and directed by Wayne Coles-Janess.

==Synopsis== dramatic chronicle service station located in the Wheat Belt of rural Australia.

 

==Festivals and awards==
Awards List

*AFI Finalist (Exhibited Nationally) Australian Film Institute Awards (Australia)
*Golden Gate Awards - San Francisco International Film Festival (USA)
*"Official Selection"- Elektrozine Ibiza 99 (Spain)
*"Diploma DHonneur" - FIFREC Nime (France)
*"Operas Primas" Santafe de Bogota International Film Festival (Colombia)
*Melbourne International Festival (Australia)
*"Official Selection" - St Kilda Film Festival (Australia)
*Australian Society of Cinematographers (Australia)
*"Official Selection" - 41st International Film Festival Mannheim (Germany)
*"Official Selection" 40th Columbus International Film and Video Festival (USA)
*"Official Selection" - Festival de Cine de Huesca (Spain)
*FilmVideo 43rd Mostra Internazionale di Montecatini Terme (Italy)
*Festival ListHamadan International Short Film Festival (Iran)
*32nd World Festival of Short Films (Belgium)
*Bathurst Film Festival (Australia)
*8th International Short Film Market Clermont-Ferrand (France)
*International Festival of Documentary and Short Films of Bilbao (Spain)
*"Operas Primas" - Santafe de Bogota International Film Festival (Colombia)
*8th Calcutta International Film Festival (India)
*"INTERNATIONAL SELECTION" - Antalya Golden Orange International Film Festival (Turkey)
*Festival International du Jeune Cinema (Montreal, Canada)
*The Rhode Island International Film Festival (USA)
*New York Independent Film and Video Festival (USA)

==Reviews==

"Direction and acting skills shine in this movie. - Your attention is grabbed by the real life "everyday" experiences of people passing through." - Juror Golden Gate Awards - San Francisco International Film Festival.

== Related Pages ==

*In the Shadow of the Palms
*Bougainville - Our Island Our Fight
*Life at the End of the Rainbow
*Wayne Coles-Janess

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 


 