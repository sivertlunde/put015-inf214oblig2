Forever Plaid: The Movie
{{Infobox film
| name           = Forever Plaid: The Movie
| caption        = Theatrical Poster
| image          = Forever Plaid- The Movie FilmPoster.jpeg
| director       = Stuart Ross Barney Cohen Benni Korzen Suren Seron Christopher Gosch
| writer         = Stuart Ross David Engel Larry Raben Daniel Reichard David Hyde Pierce
| music          = David Snyder
| cinematography = Christopher Gosch
| editing        = Nicholas Allen J.R. Lizarraga
| studio         = The Company Pictures Hudson Pictures Pterodactyl Productions Stray Angel Films
| distributor    = National CineMedia
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Forever Plaid: The Movie (aka Forever Plaid 20th Anniversary Special) is a 2008 American musical film, a recording of a live performance of a revival to the 1990 off-Broadway musical comedy Forever Plaid. Directed and written by Stuart Ross, this film was released on July 9, 2009. The films running time is 90 minutes, and was filmed at CBS Columbia Square, in Los Angeles.

==Cast==
*Stan Chandler - Jinx David Engel - Smudge
*Larry Raben - Sparky
*Daniel Reichard - Francis
*David Hyde Pierce - The Narrator
*Traci Bingham - The Usher (scenes deleted)
*Jo Anne Worley - The Usher (scenes deleted)
*Rogina Gosch - Señorita Casabas

==External links==
*  
* 
*  news at Broadway World.com
*  review at Broadway World.com
*   images at gettyimages
*   home page
*   at Playbill News
*   at THEATER MANIA

 
 
 


 