Forgetting Sarah Marshall
{{Infobox film
| name           = Forgetting Sarah Marshall
| image          = Forgetting sarah marshall ver2.jpg
| caption        = Theatrical release poster
| director       = Nicholas Stoller
| producer       = Judd Apatow Shauna Robertson Rodney Rothman
| writer         = Jason Segel
| starring       = Jason Segel Kristen Bell Mila Kunis Russell Brand
| music          = Lyle Workman
| cinematography = Russ T. Alsobrook
| editing        = William Kerr
| studio         = Apatow Productions
| distributor    = Universal Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $30 million   
| gross          = $105,173,115   
}} romantic comedy-drama film directed by Nicholas Stoller and starring Jason Segel, Kristen Bell, Mila Kunis and Russell Brand. The film, which was written by Segel and co-produced by Judd Apatow, was released by Universal Studios. Filming began in April 2007 at the Turtle Bay Resort on the North Shore of Oahu Island in Hawaii. The film was released for North American theaters on April 18, 2008 and in the UK a week later on April 25, 2008.

The story revolves around Peter Bretter, who is a music composer for a TV show that happens to feature his girlfriend, Sarah Marshall, in the lead role. After a five-year relationship, Sarah abruptly breaks up with Peter. Devastated by this event, he chooses to go on a vacation in Hawaii, in order to try to move forward with his life. Trouble ensues when he runs into his ex on the island as she is vacationing with her new boyfriend.

==Plot==
Composer Peter Bretter ( -like television show, Crime Scene: Scene of the Crime. One day, as Peter stands stark naked in their apartment, Sarah announces that she is breaking up with him. Devastated, and unable to banish his grief through one-night stands, Peter takes a trip to Hawaii. However, the vacation is ruined when he learns that Sarah and her new rockstar boyfriend Aldous Snow (Russell Brand) are also guests of the resort. Taking pity on him, hotel concierge Rachel (Mila Kunis) offers him an expensive suite for free in exchange for cleaning up the room himself.

Peter begins spending time with Rachel and starts to develop feelings for her. Meanwhile, the relationship between Sarah and Aldous begins to falter. Much of the discord is triggered by the news that Sarah’s TV show has been canceled and Aldous is about to embark on a world tour with his group, Infant Sorrow, for eighteen months. During a day of surf and sand Aldous and Peter run into each other and begin talking. Inadvertently, Aldous informs Peter that he and Sarah began having sex a full year before she broke up with Peter. When Peter confronts Sarah, she tells him she began feeling disconnected emotionally from him and she couldnt do anything to make their relationship work. Further exacerbating the situation is Sarahs obvious jealousy of the budding relationship between Peter and Rachel, while Peter (through observing Sarahs relationship with Aldous) begins to realize that his relationship with her wasnt as great as he remembered.

Later, Sarah, Aldous, Peter and Rachel share an awkward dinner together. After the dinner, Peter takes Rachel back to his hotel room and they begin to have sex. Sarah hears them through the wall and initiates sex with Aldous, moaning loudly for the benefit of the couple next door, and Rachel and Peter turn the situation into a competition and become even louder. When Aldous realizes Sarah is clearly putting on a performance to provoke a reaction from Peter, he stops the sex and tells her the trip was a mistake as shes clearly not over Peter. They bicker furiously, prompting Aldous to announce that the relationship is over. The next day Peter encounters Aldous, and learns that he and Sarah have broken up and that he is flying back to England. Peter goes to Sarahs room to console her where she admits she still loves him and tries to rekindle their romance. The two start to engage in sexual activity, but Peter abruptly cuts it off because of his feelings for Rachel, and his now ambivalent feelings towards Sarah. Peter immediately goes to Rachel to confess what happened, but she is hurt and demands that he leave and never contact her again. Before leaving, Peter takes down a naked picture of Rachel from a local bar despite enduring a beat-down from the owner.
 Dracula puppet comedy-rock opera, A Taste for Love. He sends an invitation to Rachel for the opening night performance. Although extremely hesitant at first, Rachel eventually decides to attend. After the performance Rachel congratulates Peter and tells him shes looking into attending school in the area. She leaves so Peter can bask in the success of his show, but quickly returns to Peters dressing room to tell him she misses him. Peter tells her that he has missed her too. The film ends as they embrace and kiss.

==Cast==
*  -based rock opera involving puppets called A Taste for Love in which he performs as Count Dracula.
* Kristen Bell as Sarah Marshall: A famous working actress for the TV show Crime Scene: Scene of the Crime who dated Peter before leaving him for rock star Aldous Snow.
* Mila Kunis as Rachel Jansen: A receptionist for the Hawaiian resort Turtle Bay with whom Peter forms a relationship during his stay at the hotel.
*  .
*  .
* Liz Cackowski as Liz Bretter: Brians wife who was his one and only girlfriend and who usually sneaks in on Brians computer conversations.
* Jack McBrayer as Darald Braden: A guest at the resort who does not like certain aspects of sexual intercourse and is having trouble satisfying his new wife.
* Maria Thayer as Wyoma Braden: The sex-hungry wife of Darald.
* Jonah Hill as Matthew Van Der Wyk: A waiter at Turtle Bay and obsessive fan of Aldous who constantly does awkward and random things. Aldous describes him as "an eccentric and confident young man".
* Paul Rudd as Chuck / Kunu:  A strange surfing instructor who often forgets people and who is 44 years of age by his own estimate.
* Jason Bateman as Animal Instincts Detective: A character on Sarah Marshalls latest show, Animal Instincts.
* William Baldwin as Himself / Det. Hunter Rush: In the films universe, William Baldwin is the "hard-to-love" co-star on Sarahs show. He is referred to as "Billy Baldwin" and Peter briefly suspects him of having an affair with Sarah.
* Kristen Wiig as Prana: A yoga instructor (unrated version).
*  , and hated it there, until he moved to Oahu, where he learned to name over two hundred different kinds of fish.
* Teila Tuli as Kimo: A cook at Turtle Bay.
* Branscombe Richmond as Keoki: The bartender who beats up Peter when he takes down Rachels picture from the bar.

===Puppeteers===
The puppets seen in the film were created by Jim Hensons Creature Shop. The following have performed the puppets in this film: 

* Carol Binion  
* Tim Blaney  
* Julianne Buescher - Angel
* Kevin Carlson  
* Leslie Carrara-Rudolph - One of Brides of Dracula|Draculas Brides
* BJ Guyer   Mummies
* Scott Land  
* Drew Massey - Right Hand of Count Dracula
* Michael Oosterom  
* Michelan Sisti  
* Victor Yerrid - Right Hand of Van Helsing

==Filming==
All filming locations were in the state of Hawaii and in Los Angeles. While filming, lead actor Jason Segel told a New York Times interviewer that the naked breakup scenes were based on a real-life experience he had.  The film features a great deal of improvised dialogue; according to director Nicholas Stoller, its "60 or 70 percent scripted and then 30 or 40 percent improv". 

Stoller stated that Judd Apatow was very involved in the casting process and the development of the script. Regarding the nudity in the film, he added that the first draft of the script called for the (Peter Bretter) character to get dressed after the breakup, but he thought it would be funnier if the character stayed naked the entire time. However, he confirmed the picture of Mila Kunis used in the film was created on a computer and not real. 

==Release==

===Critical reception===
Forgetting Sarah Marshall has received positive reviews from numerous critics. Review aggregator Rotten Tomatoes reports that 84% of 179 critics have given the film a positive review, with a rating average of 7.0 out of 10. The sites consensus is that "With ample laughs and sharp performances, Forgetting Sarah Marshall finds just the right mix of romantic and raunchy comedy."   Metacritic reported the film had an average score of 67 out of 100, based on 37 reviews. 

Matt Pais of the Chicago Tribune said its "the kind of movie you could watch all day because, like a new flame, you cant get enough of its company and are just glad to see where it takes you."  Richard Roeper highly praised the film for its laugh-out-loud moments as well as its worthiness to be an instant classic and went as far as to say he would put it on his list of 50 favorite comedies of all time. 

Other positive reviews come from Entertainment Weekly who gave the film a B+ and applauded "Jason Segels riff on varieties of male bewilderment,"  and Mick LaSalle of the San Francisco Chronicle, who wrote "Segels breakthrough movie, Forgetting Sarah Marshall, deserves to ride the wave of the latest, hottest micro-trend in pictures: the romantic comedy for guys."   

===Box office===
The film was promoted with a "teaser" billboard campaign, featuring the text "I hate Sarah Marshall" and the URL for the films website.

In its opening weekend, the film grossed $17.7 million in 2,798 theaters in the United States and Canada, ranking #2 at the box office behind The Forbidden Kingdom, and averaging $6,335 per theater in the US and per theater in Canada.  It opened behind other Apatow productions such as Superbad (film)|Superbad, Knocked Up, The 40-Year-Old Virgin and Talladega Nights,    but ahead of contemporary Apatow films Walk Hard  and Drillbit Taylor. 

As of February 2009, Forgetting Sarah Marshall has grossed an estimated total of $105.2 million worldwide; $63.2 million in North America and $42 million in other territories. 

===Home media===
The DVD and Blu-ray disc was released on September 30, 2008. At the DVD sales chart, Forgetting Sarah Marshall opened at #2 and sold 652,000 units, translating to $12,905,492 in revenue. As of (November 2009) 1,785,744 DVD units have been sold, acquiring revenue of $29,145,295. This does not include Blu-ray sales/DVD rentals. 

It was released in a single-disc DVD edition, a three-disc collectors DVD edition, a two-disc Blu-ray edition, and the Ultimate Unrated Comedy Collection containing the collectors editions of Forgetting Sarah Marshall, The 40-Year-Old Virgin and Knocked Up on either DVD or Blu-ray Disc.  It was released on DVD in Australia (region 4) on August 20, 2008 in a single and 2-Disc Unforgettable Edition and was also released on Blu-ray disc in Australia on November 5, 2008.

==Music== Blondie and Kenny Loggins were also used in previews for the film.

===Soundtrack===
{{Infobox album  
| Name        = Forgetting Sarah Marshall: Original Soundtrack
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       =
| Released    = April 22, 2008
| Recorded    =
| Genre       = Soundtrack
| Length      = Verve Forecast/Capitol Capitol
| Producer    =
| Reviews     =
| Last album  =
| This album  = Forgetting Sarah Marshall: Original Soundtrack (2008)
| Next album  =
}}

The soundtrack of Forgetting Sarah Marshall was released on April 22, 2008.

{{Track listing
| music_credits = Yes
| title1            = Love You Madly Cake
| length1           = 3:58
| title2            = Weve Got to Do Something Infant Sorrow
| length2           = 3:33
| title3            = You Cant Break a Heart and Have It
| note3             = Black Francis
| length3           = 2:37
| title4            = Get Me Away From Here, Im Dying 
| note4             = Belle & Sebastian
| length4           = 3:25
| title5            = More Than Words
| note5             = Aloha Sex Juice
| length5           = 3:12
| title6            = Draculas Lament
| note6             = Jason Segel
| length6           = 1:23
| title7            = Inside of You Infant Sorrow
| length7           = 2:50
| title8            = Fucking Boyfriend
| note8             = The Bird and the Bee
| length8           = 3:14
| title9            = Intensified 68 (Music Like Dirt)
| note9             = Desmond Dekker
| length9           = 2:43
| title10           = Nothing Compares 2 U Hawaiian
| length10          = 5:58
| title11           = Baby
| note11            = Os Mutantes
| length11          = 3:37
| title12           = These Boots Are Made for Walkin
| note12            = The Coconutz
| length12          = 2:52
| title13           = A Taste for Love
| note13            = Forgetting Sarah Marshall Cast
| length13          = 2:04
| title14           = The Secret Sun
| note14            = Jesse Harris
| length14          = 3:45
| title15           = Everybody Hurts
| note15            = The Coconutz
| length15          = 6:03
| title16           = Animal Instincts
| note16            = The Transcenders featuring J7 DStar
| length16          = 1:14}}
 311 can New York band Ambulance Ltd.

==Awards and nominations==
Forgetting Sarah Marshall was nominated for five awards for the 2008 Teen Choice Awards, though it did not win any awards. The nominations were:
* Movie, Breakout Female: Kristen Bell
* Movie, Breakout Female: Mila Kunis
* Movie, Breakout Male: Jason Segel
* Movie, Romantic Comedy
* Movie, Actress Comedy: Kristen Bell

On  , Forgetting Sarah Marshall was voted "The Funniest Film of 2008".

==Follow-up spin-off==
 
A spin-off released June 4, 2010 reuniting director Stoller and producer Judd Apatow with stars Russell Brand and Jonah Hill. Brand reprises his role of Aldous Snow, while Hill plays an entirely new character. Bell also briefly reprises her role as Sarah Marshall where she appears in a promo for a new NBC drama Blind Medicine where she portrays a visually impaired surgeon.

==References==
 

==External links==
 
 
* 
* 
* 
* 
* 
;Fictional websites created for the film
* 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 