30 Days (1999 film)
{{Infobox film
| name           = 30 Days
| image          =
| caption        = 
| director       = Aaron Harnick
| producer       = Matthew Rego Michael Rego Arielle Tepper Madover Hank Unger
| writer         = Aaron Harnick
| starring       = Ben Shenkman Arija Bareikis Alexander Chaplin
| music          = Andrew Sherman Stephen J. Walsh
| cinematography = David Tumblety
| editing        = Sean Campbell
| distributor    = Arrow Releasing
| released       = United States 15 September 2000
| runtime        = 87 minutes
| country        = United States English
| budget         = 
| gross          = 
}}
30 Days is a 1999 American independent film written and directed by Aaron Harnick. It stars Ben Shenkman and Arija Bareikis and premiered at the 1999 Toronto International Film Festival on 11 September. It was subsequently released in the United States on 15 September 2000.

==Plot==
Jordan (Shenkman) is a successful businessman with commitment issues, so friends decided to match him with an NBC worker, Sarah (Bareikis) who is going through some emotional issues. Theya agree to sleep together after Jordan reveals he doesnt know the name of a woman he recently slept with. But later they decide to become a romantic couple. However after their first fight, the relationship is over. Jordan later realises how important Sarah is to him and proceeds to try to win her back.  

==Cast==
*Ben Shenkman as Jordan Trainer
*Arija Bareikis as Sarah Meyers
*Alexander Chaplin as Mike Charles
*Bradley White as Tad Star Thomas McCarthy as Brad Drazin
*Catherine Kellner as Lauren
*Jerry Adler as Rick Trainer
*Barbara Barrie as Barbara Trainer
*Arden Myrin as Stacey
*Mark Feuerstein as Actor
*Lisa Edelstein as Danielle
*Tina Holmes as Jenny

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 