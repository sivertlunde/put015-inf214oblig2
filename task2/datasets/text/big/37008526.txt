A Late Quartet
{{Infobox film
| name           = A Late Quartet
| image          = A Late Quartet Poster.jpg
| caption        = Theatrical release poster
| director       = Yaron Zilberman
| producer       = Yaron Zilberman Mandy Tagger Vanessa Coifman David Faigenblum Emanuel Michael Tamar Sela
| writer         = Yaron Zilberman Seth Grossman
| starring       = Philip Seymour Hoffman Christopher Walken Catherine Keener
| music          = Angelo Badalamenti
| cinematography = Frederick Elmes
| editing        = Yuval Shar
| studio         = Opening Night Productions RKO Pictures
| distributor    = Entertainment One
| released       =  
| runtime        = 106 minutes  
| country        = United States
| language       = English
| budget         =
| gross          = $1,562,546 
}}
A Late Quartet is a 2012 American drama film co-written (with Seth Grossman), produced, and directed by Yaron Zilberman.   The film uses chamber music played by the Brentano String Quartet and especially, String Quartet No. 14 (Beethoven)|Beethovens Op. 131.  The film was released in Australia as Performance.

==Plot==
As the Fugue String quartet approaches its 25th anniversary, the onset of a debilitating illness to cellist Peter Mitchell (Christopher Walken), forces its members to reevaluate their relationships. After being diagnosed with Parkinsons disease, Peter announces his decision to play one final concert before he retires. Meanwhile the second violinist, Robert (Philip Seymour Hoffman), voices his desire to alternate the first violinist role, long held by Daniel (Mark Ivanir). Robert is married to Juliette (Catherine Keener), the viola player of the group. Upon discovering Juliette does not support him in this matter, Robert has a one-night stand. Further complicating matters, their daughter, Alexandra (Imogen Poots), begins an affair with Daniel, whom her mother once pined for. Yet bound together by their years of collaboration, the quartet will search for a fitting farewell to their shared passion of music and perhaps even a new beginning.

==Cast==
* Philip Seymour Hoffman as Robert Gelbart
* Christopher Walken as Peter Mitchell
* Catherine Keener as Juliette Gelbart
* Mark Ivanir as Daniel Lerner
* Imogen Poots as Alexandra Gelbart
* Anne Sofie von Otter as Miriam
* Madhur Jaffrey as Dr. Nadir
* Liraz Charhi as Pilar
* Wallace Shawn as Gideon Rosen
* Nina Lee (of the Brentano String Quartet) as herself, in the closing scene

==Adaptation from source material==
The scene in which Peter Mitchell tells his music class an anecdote about meeting Pablo Casals is adapted from an anecdote found in Cellist, the autobiography of cellist Gregor Piatigorsky; the circumstances of the encounter and the pieces played are changed in the film, but Casalss words are essentially identical to those recounted by Piatigorsky. 

==Soundtrack== String Quartet No. 14 in C-sharp minor, Op.&nbsp;131, performed by the Brentano String Quartet
*   in F minor, 3rd Movement (music)|movement, performed by the Brentano String Quartet
* Uri Caine: "City Nights", performed by Uri Caine
* Cristian Puig: "Bulerias Del Encuentro" (flamenco), performed by Cristian Puig and Rebeca Tomas
*  ", Op. 20, performed by Mark Steinberg
* Jonathan Dagan: "Salty Air" (from Rivers and Homes), performed by j.viewz
*   in E-flat major, BWV&nbsp;1010, performed by Nina Lee (Brentano String Quartet)
*  " from Die tote Stadt, performed by Anne Sofie von Otter (mezzo-soprano), Bengt Forsberg (piano), Kjell Lysell and Ulf Forsberg (violins),   (viola), Mats Lidström (cello)
The films stage performances were filmed in the Metropolitan Museum of Arts Grace Rainey Rogers Auditorium, the same stage where the Guarneri Quartet gave its farewell concert in 2009.

==Reception==
A Late Quartet received generally positive reviews, currently holding a 79% "fresh" rating on Rotten Tomatoes. 

==See also==
* Late string quartets (Beethoven)

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 