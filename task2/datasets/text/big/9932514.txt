Maid to Order
 
{{Infobox film
| name           = Maid to Order
| image          = Maidtoorderposter.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Amy Holden Jones
| producer       = Mort Engelberg
| writer         = Amy Holden Jones Perry Howze Randy Howze
| starring       = Ally Sheedy Beverly DAngelo Michael Ontkean Valerie Perrine Dick Shawn Tom Skerritt Merry Clayton
| music          = Georges Delerue Shelly Johnson
| editing        = Sidney Wolinsky
| distributor    = The Vista Organization
| released       =  
| runtime        = 93 min
| country        = United States Spanish
| budget         = $3 million
| gross          = $9,868,521
}} 1987 comedy film|comedy/fantasy film starring Ally Sheedy.

==Plot==
Jessie Montgomery (Ally Sheedy), a spoiled rich girl whose hard partying life style and lack of self-respect, as well as a lack of respect for others, finds herself facing her worst fear. When Jessie gets arrested for drunk driving and drug possession, she finally pushes her father (Tom Skerritt) beyond his limits. Beside himself with frustration and disappointment, he says the one thing he thought hed never say - he wishes hed never had a daughter. In pops Stella (Beverly DAngelo), a fairy godmother. Stella casts a spell that causes Charles to no longer have a daughter, and creates an existence where Jessie must make it on her own. Jessie is then forced to find work as a maid for an eccentric rich couple, The Starkeys, (Valerie Perrine and Dick Shawn). 

Through her experiences with the other people in the mansion (former singer-turned-cook Audrey, Hispanic servant Maria, and chauffeur Nick) Jessie learns the true meaning of love, friendship, and self-respect. When she chooses the happiness of her new friends over her own, she is rewarded with having her old life more or less returned to her.

This film is an unusual variation on the   is not the means to a better life for the heroine but rather the archenemy|nemesis. Stella is Jessies primary obstacle to achieving her wish of regaining her old spoiled Beverly Hills lifestyle.

==Reception==

Reviews were mixed. Roger Ebert of the Chicago Sun-Times "found it too easy to anticipate most of the big moments and too hard to believe that Sheedy was really a spoiled, mean-spirited rich bitch."   But Janet Maslin in the New York Times praised Sheedy, saying her "petulant manner and her air of faint distaste for her surroundings are just right for this role. And she shows herself to be an able physical comedienne."  

==Filming Locations==

Jessies fathers mansion is located at 365 S Hudson Street, Los Angeles, California.
The Starkey mansion, where Jessie worked, is located at 32596 Pacific Coast Hwy, Malibu, California

==Availability==

The movie was released on VHS by International Video Entertainment in 1988 and again in 1991 by Avid Home Entertainment. In 2002, Artisan Entertainment released the film on DVD without bonus features and was presented only in full screen. The DVD has now been discontinued for no given reason. As of December 21, 2009, Lions Gate have yet to announce any plans for a new DVD. In September 2010 the movie became available to view on Netflixs Watch Instantly, but has since been removed.

==Taglines==
Worse help is hard to find.

She was raised in one mansion. Now shes got to clean another.

==Characters==
* Ally Sheedy – Jessie Montgomery
* Beverly DAngelo – Stella Winston
* Michael Ontkean – Nick McGuire
* Valerie Perrine – Georgette Starkey
* Dick Shawn – Stan Starkey
* Tom Skerritt – Charles Montgomery
* Merry Clayton – Audrey James
* Begoña Plaza – Maria
* Rain Phoenix – Brie Starkey (as Rainbow Phoenix)

==Soundtrack==

"Spirit In The Sky"
Performed by Dr. and The Medics
Music and Lyrics by Norman Greenbaum

"Im On My Own"
Performed by Craig Thomas
Music and Lyrics by Ralph Jones and Claudette Raiche

"Clean Up Woman"
Performed by Bekka Bramlett
Music and Lyrics by Clarence Reid and Willie Clark

"I Can Still Shine"
Performed by Merry Clayton
Music and Lyrics by Ashford and Simpson

"Its In His Kiss"
Performed by Merry Clayton
Music and Lyrics by Rudy Clark

"976-Self Service"
Music by Ralph Jones and Claudette Raiche

"Fernando the Champ"
Music and Lyrics by Rudy Regaiado
 

==External links==
*  
*  
*  
*  

==References==
 

 

 

 
 
 
 
 
 
 