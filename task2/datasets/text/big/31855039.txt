The Prince and the Surfer
{{Infobox film
| name           = The Prince and the Surfer
| image          = ThePrinceandtheSurferVHSCover.jpg
| image_size     = 225px
| caption        = VHS Cover
| director       = Arye Gross Gregory Gieras  . IMDB full credits (Verified complete). 
| executive producer = 
| producer       = Steven Paul Patrick Ewald Hank Paul Dorothy Paul
| writer         = Gregory Poppen (screenplay) Mark Twain book "The Prince and the Pauper"
| narrator       = Jon Voight
| presenter      = Jon Voight
| starring       = Sean Kellman Robert Englund Vincent Schiavelli 
| music          = Erik Lundmark
| cinematography = Thomas Harding
| editing        = Dennis OConnor
| distributor    = A-pix
| copyrightholder= Contemporary Visions, Inc.
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Prince and the Surfer is a   and Gregory Gieras, and stars Sean Kellman, Robert Englund, and Vincent Schiavelli.

==Synopsis== skitches with the limo on its way back to the hotel, and meets Edward in his suite; the two hatch their plan to trade places, and separate. Unbeknownst to them, the evil Minister Kratski plots to undermine the Queens efforts, and instead convert Gelfland into Golfland. While pursuing princess Galina, Cash learns of the ministers plan. Meanwhile Edward pursues Cashs friend Melissa, enjoying and enduring life in the suburbs. In the end, Cash and Edward reunite to save the day.

==Cast==
*Sean Kellman as Cash Canty/Prince Edward of Gelfland
*Robert Englund as Kratski
*Vincent Schiavelli as Baumgarten
*Jennifer ONeill as Queen Albertina
*Jason Strickland as CT
*Linda Cardellini as Melissa
*Shepard Koster as Peter
*Arye Gross as Vince
*C. Thomas Howell as Dean
*Katie Johnston as Galina
*Allyce Beasley as Constance
*Gregory Poppen as Bodybuilder
*Steven Green as Bodyguard
*Lawrence DiBlasio as Bodyguard
*Alyse Mandel as Maid

The film also featured uncredited performances by Timothy Bottoms as Johnny Canty, Phil Bowers, Jason Reid as Riff, Jon Voight as the films presenter, and Denise Wilson as Tattooed Woman. 

==Production and release==
 Los Angeles, and Venice, Los Angeles, California|Venice, California, USA.
 PG for "brief mild language". 

==Reception==
Reviews of the film were mildly positive. Brian Webster of online Apollo Guide gave the film a score of 69 percent, calling the film "decent, but unspectacular family entertainment, with plenty of minor thrills and laughs for the eight to 12-year-old set."  Though he found it to be predictable, with nothing to particularly recommend, he also found "no major failings". He granted that secondary characters were mere "stereotypes (including those played by the screenwriter and one of the co-directors)", but that the young stars had "energy and just enough acting talent to pull it off."  He found Cardellini to be "a decently spunky Melissa". 

The Pound Shop movie review site relates that it is "fairly clear that this is a movie for the kiddies", but that it was "pretty unreasonable" to not have "one single instance of surfing". The reviewer found that the "stand out elements of this movie" include "Erik Lundmark’s casio keyboard score. While clearly made with a very limited amount of time, and money, I feel it is one of the few movie soundtracks that really help move the film along."  Also, the "banter between the incompetent security guards at the resort actually finds its mark every now and again as well," and overall, the acting was satisfactory. Summarizing, the review stated the film is "worth a pound." 

AllRovi.com reviewer Mark Deming gave the film two (of five) stars. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 