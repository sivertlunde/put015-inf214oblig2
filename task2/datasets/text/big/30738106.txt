Spin (2007 film)
{{Infobox film
| name           = Spin
| image          = Spin-Movie-Poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Henry Pincus
| producer       = Paul Schiff Mickey Barold Stone Douglass Tai Duncan
| screenplay     = Henry Pincus Adam Campbell Katie Cassidy Lauren German
| music          = BC Smith
| cinematography = Steve Gainer
| editing        = Jeff Wishengrad
| studio         = Paul Schiff Productions
| distributor    = THINKFilm
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Adam Campbell, Katie Cassidy and Lauren German. 

==Plot==
Several different people, including a DJ, an aspiring actress and her friends, a meth user and a charming British pick-up artist, attempt to recall the events of the previous evening at a popular Los Angeles night club.

==Cast==
* Patrick Flueger as Ryan, the DJ Adam Campbell as Mick
* Bijou Phillips as Aubrey
* Chris Lowell as Delaney
* Katie Cassidy as Apple Spears
* Lauren German as Cassie
* Michael Biehn as Tony Russo
* Amber Heard as Amber
* Danny Masterson as Derek

==Reception==

===Critical response===
When the film opened, Dennis Harvey, film critic at Variety (magazine)|Variety magazine, panned the film, writing, "You could get a concussion diving into the emotional shallows of You Are Here, whose half-dozen or so Los Angeles twentysomething body-beautifuls get strung out on drink, drugs and each others hotness.  A rom-com Pulp Fiction-alized out with tricky flashback/forward episodic structure, pics aggressively trendy, empty early progress suggests Go (1999 film)|Go shrunk to fit the brainpan of a Paris Hilton universe. It grows less irksome eventually, but ThinkFilm Slamdance pickup remains too unironically airheaded for wide exposure. Cast of recognizable cuties should serve it well in DVD release, however. 

==References==
 

==External links==
*  
*  

 
 
 
 
 