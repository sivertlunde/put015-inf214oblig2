For Those Who Think Young (film)
 
{{Infobox film
| name           = For Those Who Think Young
| caption        =
| image	         = For Those Who Think Young FilmPoster.jpeg
| director       = Leslie H. Martinson
| producer       = Hugh Benson
| writer         = James OHanlon George OHanlon Dan Beaumont
| story          = Dan Beaumont
| starring       = James Darren Pamela Tiffin Paul Lynde Tina Louise Bob Denver Nancy Sinatra Robert Middleton Woody Woodbury
| music          = Jerry Fielding
| cinematography = Harold E. Stine
| editing        = Frank P. Keller
| distributor    = United Artists
| released       =  
| runtime        = 96 min.
| country        = United States
| awards         =
| language       = English
| budget         =
| gross = $1,600,000 (US/ Canada) 
}}

For Those Who Think Young is a 1964 beach party film shot in Techniscope, directed by Leslie H. Martinson and featuring James Darren, Pamela Tiffin, Paul Lynde, Tina Louise, Bob Denver, Nancy Sinatra, Robert Middleton, and Woody Woodbury.

==Plot==
Rich kid and party animal Gardner Pruitt III (James Darren), known as “Ding” to his friends, is on the prowl for a new conquest in the form of teenager Sandy Palmer (Pamela Tiffin). In the meantime, Ding’s influential grandfather, B.S. Cronin (Robert Middleton) wants to curtail the romance and shut down a popular local college teen hangout.

Sandy’s guardians Sid Hoyt (Paul Lynde) and Woody Woodbury (playing himself) get mixed up in the proceedings, with Woody becoming the college kid’s hero at the hangout. That sends up a red flag to the college administration, which sends in Dr. Pauline Swenson to investigate allegations of underage drinking.

When the clever kids discover that ex-gangster Grandpa Cronin used to be a bootlegger, they blackmail him into keeping the club open.

==Cast==
James Darren 	... 	
Gardner Ding Pruitt III 
Pamela Tiffin 	... 	
Sandy Palmer 
Paul Lynde... 	
Uncle Sid Hoyt 
Tina Louise 	 	... 	
Topaz McQueen 
Bob Denver 	... 	
Kelp 
Nancy Sinatra 	... 	
Karen Cross 
Robert Middleton 	... 	
Burford B. Sanford Nifty Cronin 
Claudia Martin 	... 	
Sue Lewis 
Ellen Burstyn 	... 	
Dr. Pauline Swenson (billed as Ellen McRae)  
Woody Woodbury 	... 	
Uncle Woody Woodbury 
Louis Quinn 	... 	
Gus Kestler 
Sammee Tong 	... 	
Clyde 
Jimmy Griffin  ...  Singer  
George Raft  ...  Detective Lieutenant    Roger Smith  ...  Smitty the Detective  
Addison Richards  ...  Dean Watkins   
Paul Mousie Garner  ...  Mousie  
Benny Baker  ...  Lou   
Anna Lee  ...  Laura Pruitt   
Jack La Rue  ...  Cronins Business Associate  
Allen Jenkins  ...  Col. Leslie Jenkins   Robert Armstrong  ...  Norman Armstrong

==Production==
The film was made by Frank Sinatras Essex Productions with director Leslie H. Martinson chosen for his fast-paced work on Warner Bros. Television as the films production was just 18 days.  All the beach scenes were shot at Mailbu Beach in one day.  It featured the film debuts of Nancy Sinatra and Claudia Martin (daughter of Dean Martin) as well as a leading role for nightclub comedian Woody Woodbury, then the host of televisions Who Do You Trust?.

Ellen Burstyn, who plays the part of Dr. Pauline Swenson, is listed in the credits as Ellen McRae, and famous surfer Mickey Dora is a college boy extra.
 Burbank customizer George Barris originally to be his personal car. 

The film was notable for extensive product placement for Pepsi Cola and several other companies, the title of the film being a Pepsi marketing slogan of the time. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 174 

==Music==
Jerry Fielding, later famous for his television themes, composed the score for the film.

Mack David and Jerry Livingston wrote "For Those Who Think Love," sung by James Darren over the opening credits.

Bob Denver sings "Ho Daddy, Surfs Up" and "Ho Daddy, Surfs Up (Reprise)."
 The Challengers). 

==References==
 

==External links==
*  

 

 
 
 