Cairo (film)
{{Infobox film
| name           = Cairo
| image          = Cairo 1942.jpg
| caption        = Theatrical poster
| director       = W. S. Van Dyke
| producer       = Joseph L. Mankiewicz (uncredited)
| writer         = Concept: Ladislas Fodor
| screenplay     = John McClain Robert Young
| music          = Herbert Stothart
| cinematography = Ray June 
| editing        = James E. Newcom
| studio         = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $924,000 Turk, Edward Baron "Hollywood Diva: A Biography of Jeanette MacDonald" (University of California Press, 1998)  "The Eddie Mannix Ledger." Margaret Herrick Library, Center for Motion Picture Study (Los Angeles). 
| gross          = $1,197,000  
}}
 MGM and Loews Cineplex Entertainment|Loews, and directed by W. S. Van Dyke. The screenplay was written by John McClain, based on an idea by Ladislas Fodor about a news reporter shipwrecked in a torpedo attack, who teams up with a Hollywood singer and her maid to foil Nazi spies.  The music score is by Herbert Stothart. This film was Jeanette MacDonalds last film on her MGM contract. 

The film was poorly received upon its initial release. 

==Plot==
  Robert Young) as her butler. What Marcia doesnt know is that Smith is an American newspaperman, who strongly suspects that she is a Nazi spy (the real enemy agent is Mrs. Morrison (Mona Barrie).
==Cast==
 
* Jeanette MacDonald as Marcia Warren Robert Young as Homer Smith, aka Juniper Jones
* Ethel Waters as Cleona Jones, Marcias Maid
* Reginald Owen as Philo Cobson Grant Mitchell as Mr. O.H.P. Boggs
* Lionel Atwill as Teutonic gentleman
* Eduardo Ciannelli as Ahmed Ben Hassan
* Mitchell Lewis as Ludwig
* Dooley Wilson as Hector
* Larry Nunn as Bernie
* Dennis Hoey as Col. Woodhue
* Mona Barrie as Mrs. Morrison Rhys Williams as Strange man
* Cecil Cunningham as Mme. Laruga
* Harry Worth as Viceroy Hotel bartender Frank Richards as Alfred
* Faten Hamama as Amina
 

==Reception==
According to MGM records. the film earned $616,000 in the U.S. and Canada and $581,000 elsewhere, meaning the studio recorded a loss of $131,000. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
Joseph L. Mankiewicz

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 