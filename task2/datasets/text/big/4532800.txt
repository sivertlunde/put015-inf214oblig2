The Serpent's Kiss
{{Infobox Film
| name           = The Serpents Kiss
| image          = The Serpents Kiss.jpg
| image_size     = 
| caption        = 
| director       = Philippe Rousselot
| producer       = 
| writer         = Tim Rose Price 
| narrator       = 
| starring       = Ewan McGregor Greta Scacchi Pete Postlethwaite
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1997 film directed by Philippe Rousselot. It is a story about a Dutch garden architect named Meneer Chrome (Ewan McGregor) who has been hired by a wealthy metalworker (Pete Postlethwaite) to create an extravagant garden. The film also stars Greta Scacchi and Richard E. Grant.

The film was entered into the 1997 Cannes Film Festival.    The film was Rousselots only directed feature throughout his career.

==Plot== Heal yourself!"

As the garden grows increasingly more complex and Smithers approaches bankruptcy, Fitzmaurice realizes that Chromes affection for Anna is making him a liability; first he threatens to reveal Chromes true identity to Smithers, then he plots to kill him by poisoning. Fitzmaurices plan backfires, however, and he falls victim to his own poison. Juliana abandons her adulterous intentions, which her husband remains unaware of, and turns to support Smithers, who is bankrupt from his garden project obsession. Chrome, revealed to be the real Chromes assistant, goes to the sea with Anna, where she throws away her book of poetry.

==Cast==
*Ewan McGregor as Meneer Chrome 
*Greta Scacchi as Juliana 
*Pete Postlethwaite as Thomas Smithers 
*Richard E. Grant as James Fitzmaurice 
*Carmen Chaplin as Thea / Anna 
*Donal McCann as Physician 
*Charley Boorman as Secretary 
*Gerard McSorley as Mr. Galmoy 
*Britta Smith as Mrs. Galmoy 
*Susan Fitzgerald as Mistress Clevely 
*Pat Laffan as Pritchard 
*Rúaidhrí Conroy as Physicians Assistant 
*Henry King  as Lead Reaper

==Locations==
*Part of the film was shot in the County Clare village of Sixmilebridge in the Republic of Ireland.

==Beginning of a Friendship==
This film also marks the start of   and is referenced in the documentary.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 