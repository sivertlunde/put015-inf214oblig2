Uncle Joe Shannon
{{Infobox film
| name           =Uncle Joe Shannon
| image          = Uncle_Joe_Shannon.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Joseph Hanwright
| producer       = Robert Chartoff Irwin Winkler
| writer         = Burt Young
| starring       = Burt Young
| music          = Bill Conti Bill Butler
| editing        = Don Zimmerman
| studio         = United Artists
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Uncle Joe Shannon is a 1978 American drama film directed by Joseph Hanwright and written by Burt Young, who also stars. The film was produced by Robert Chartoff and Irwin Winkler and distributed by United Artists.

==Plot==
A trumpet player, Joe Shannon believes he has little left to live for when he tragically loses both his wife and child. Only a new relationship with a disadvantaged boy is keeping him from sinking into depressions permanent depths.

==Cast==
*Burt Young as Joe Shannon
*Doug McKeon as Robbie
*Madge Sinclair as Margaret
*Jason Bernard as Goose
*Bert Remsen as Braddock
*Allan Rich as Dr. Clark

==Reception==

Jennifer Dunning of The New York Times wrote that Youngs screenplay was unsurprising and overly sentimental. She found the film to be "full of odd improbabilities," though she commended director Joseph Hanwright in his debut for drawing good performances from the cast. Dunning also reported of the cinematography, "Bill Butlers cameras capture the slick night surfaces and ruined faces of the street people of Los Angeles so that, clichés in themselves, they have the impact of sudden revelation." 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 