Catnapped!
{{Infobox film name = Totsuzen! Neko no Kuni Banipal Witt image =  caption =  romaji = Totsuzen! Neko no Kuni Baniparu Uitto director = Takashi Nakamura producer = Tarō Maki Yoshimi Asari Hiroaki Inoue writer = Takashi Nakamura Chiaki J. Konaka screenplay = story = narrator =  starring = Hiroaki Hori Mirai Sasaki Fumihiko Tachiki Noriko Hidaka Mayumi Iizuka music = Shigeaki Saegusa cinematography =  artdirector =  editing = Takeshi Seyanna studio =  production =  distributor = T&K Telefilm released =   runtime = 76 minutes country = Japan language = Japanese budget =  gross =
}}
 1995 Japanese anime feature film, directed, created and written by Takashi Nakamura, who was also its character designer. The animation was produced by Triangle Staff. The theme song of the film was performed by Mayumi Iizuka.

==Plot==
For a whole week, Toriyasu and his little sister, Meeko, have been missing their pet dog, Papadoll. Toriyasu thinks he ran off, but Meeko claims it was an alien abduction. Though Toriyasu mocks Meeko for her over-imaginative ways, she isnt far off from the truth. On the way to school, Meeko sees what looks like a cat in clothes slipping into the shadows. Later that evening, three anthropomorphic feline scientists, Henoji, Suttoboke and HoiHoi go into Toriyasu and Meekos room intending to take Toriyasu with them on a trip. Instead they end up waking and taking Meeko as well. She accompanies them in their vehicle (a balloon that resembles a cheshire cat) with a tired Toriyasu.
 be destroyed. The two children soon find that Banipal Witt is far different than their home: Three minutes in the human world is one day in Banipal Witt and the cats are only anthropomorphic in Banipal Witt (Henoji having remarked that, because of the human world, their life expectancy has decreased). Both Toriyasu and Meeko are turned into anthropomorphic kittens  by the sun of Banipal Witt (which is magical in nature) as soon as they set foot on Banipal Witt.

Soon, the kids meet the leader of a resistance: Master Sandada, a powerful wizard. Sadly, in the absence of the trio sent to find Toriyasu and Meeko, Sandada has fallen victim to a curse from Lady Buburina, a dictator-like princess that has gone insane and (due to an enchantment) turns anyone she touches into a balloon. Sandada was unable to protect himself, because his last line of defense- a mystic glove called the Sorcerers Arm -was stolen by DohDoh, his apprentice that had fallen to insanity due to a curse. Master Sandada explains that he brought Toriyasu here to catch Papadoll, who has supposedly been wreaking havoc across Banipal Witt.

ChuChu, DohDohs younger sister and the strongest fighter for the resistance, comes to warn that Papadoll has been abducting more villagers and is approaching their location fast. Papadoll soon arrives and the kids cant believe their eyes: Their dog has been turned into a giant, flying monster. DohDoh leaves Sandada with an ultimatum from Buburina: If Sandada doesnt surrender before the next sunrise, great disaster shall befall Banipal Witt. Meeko manages to get Papadoll to calm down, but an interruption from Toriyasu sends Papadoll into a fit of rage. In the confusion, Meeko is taken hostage and things begin to look grim. 

Some time later, Sandada explains that when a creature from another world is touched by the sun twice, they become what Papadoll has become. Expectedly, Toriyasu freaks out, demanding to go home, but the others manage to reason with him because of the fact that he needs to save his sister.

Meanwhile, at the palace, the prisoners (including Meeko) attempt to break out. Meeko comes close, but ends up trapped near the throne room. Listening in on a conversation between Buburina and her parents, she finds out that Buburinas power is supposed to be a punishment that was cast by a wizard who sought revenge against her for sending his daughter to her death (even though Buburina claimed that it wasnt her fault).

Back at Sandadas manor, Toriyasu (along with Henoji, Suttoboke, HoiHoi and ChuChu) are sent by Sandada to Buburinas palace to knock Papadoll out with a pill that contains sleeping powder that Sandada claims would make an animal like Papadoll sleep for a week and Papadolls chain. Before leaving, Sandada says that only Toriyasu has the power to turn Papadoll back to normal.

Later that night, Buburina calls an assembly to inform her prisoners of their fate. She tells her prisoners of her plan to create a giant mouse that would be used to wake the Sleeping Cat should anyone rebel against her, sending Banipal Witt into turmoil. Meeko, however, ridicules Buburinas plan, claiming that Papadoll wouldnt do that. Meeko demands the return of Papadoll and insults the princess by calling her a witch. Buburina, having been upset by Meekos insult, tries to turn her into a balloon and pop her, but finds she cannot (the curse only affects people from Banipal Witt). Realizing that Meeko is a human, Buburina plans to make Meeko her new monster slave once the sun rises. She then turns all her prisoners into balloons (so that she can finish her mouse balloon) and throws Meeko inside also.

Under cover of night, Toriyasu and the others prepare to sneak into the castle to free Papadoll and save Meeko. Unfortunately, the plan goes awry, as Toriyasu accidentally wakes Buburina up when the rope he is on slips, setting off alarms all over the castle. Toriyasu and ChuChu make an escape, while Suttoboke (who broke off from the group because he got Meekos scent) runs into DohDoh in an attempt to save Meeko. 

Quickly, Toriyasu and ChuChu (after having a bonding moment) regroup in time to see Suttoboke and Meeko release the mouse balloon. With Buburinas plan falling apart at the seams, DohDoh goes to try to kill Meeko and Suttoboke, while Toriyasu and ChuChu go to try to get Papadoll under control and stop Buburina. 

In the ensuing air battle, Toriyasu suffers through a temporary depression caused by DohDohs taunting (having remembered a time when he beat Papadoll to release some built-up anger that had been caused by teasing from three neighborhood bullies). DohDoh, however, is dealt a hard fate by karma, as he breaks the mouse balloon when he gets too close to the castle and loses his hold on the Sorcerers Arm. Toriyasu, remembering what Master Sandada had said before he left, regains faith in himself and tries to take Papadoll back. Buburina, however, refuses to give back Papadoll and nearly makes Toriyasu fall to his death after Buburina tears Papadolls collar off. 
 flashback form). Toriyasu then goes insane for only a few seconds before regaining composure after landing on one of Buburinas victims. Using the victim as a means of transportation (even though the said victim tries to protest as best he can to this idea), he reaches Papadoll (avoiding seeking missiles fired by Buburina) and regains control of him with ChuChus help. However, the signal tower (the enormous flare gun-like device that lights Banipal Witts sun every morning) goes off, leaving Toriyasu and the others only a few seconds to save Meeko. 

Quickly, they rescue Meeko and make it home. Buburina and DohDoh both end up with miserable fates: Buburina is left sinking into a small lake, and DohDoh tangled in what remains of the mouse balloon.

Toriyasu and Meeko return to normal (no longer transformed by Banipal Witts sun), Toriyasu and ChuChu develop a relationship and the cats say that the two can visit any time and they can also choose to be cats again if they want. Before returning home, Meeko makes a prediction that Buburina will make a comeback. 

The next morning, Toriyasu and Meeko return to a normal life. Unbeknownst to the children, Meeko is right again: The cats go to Toriyasu and Meekos school due to some urgent business that came up only recently, leaving the movie on a cliffhanger.

==Staff==
*Original Creator, Director, Character Designs: Takashi Nakamura
*Screenplay: Takashi Nakamura, Chiaki J. Konaka
*Producers: Tarō Maki, Yoshimi Asari, Hiroaki Inoue
*Art Director: Shinji Kimura
*Music: Shigeaki Saegusa
*Sound Director: Shigeharu Shiba
*Production: Triangle Staff
*Distribution: T&K Telefilm

 Sources:          

==Cast==
The actors are listed original voice actor first, English voice actor second.

*  of the movie. He is the older brother of Meeko. When first brought to Banipal Witt, he is freaked out by the prospect of a strange, new world that is hidden from sight, but he eventually warms up to the people of Banipal Witt and makes a few friends along the way (most notably ChuChu, who seems to be his love interest by the end of the film). Though he is, at first, uncaring as to the fact that Papadoll has gone missing, he begins to show care towards Papadoll, and becomes determined to bring him back home. Because he is a human, he is left unaffected by Buburinas curse.

*  as a prisoner early in the film, and the only prisoner not to be affected by Buburinas curse (because she is a human). She and Suttoboke are very close, Suttoboke acting as somewhat of a father figure to her. Unlike her brother, she was excited to be heading to another world.

*  of the movie. She is a spoiled, rotten-to-the-core   towards water (as is shown near the end of the film, when her evil ways turn against her).

*  or warrior of some kind. Apparently, she was looking forward to seeing a human (for reasons unknown), but is less than happy when Toriyasu shows up and begins to act like a jerk. However, as time goes on, she begins to respect and (eventually) love Toriyasu.

* . Near the end of the film, he regains the Sorcerers Arm and everyone affected by Buburinas curse is restored (including himself).

* s sent by Master Sandada to find Toriyasu. He serves as the leader of the little expedition to the human world. He is generally pessimistic with many situations (such as Meeko wanting to come with Toriyasu to Banipal Witt). He serves as the lookout on the Tomcat, as well as a timekeeper. Despite the fact he is a scientist, he is dressed very much like a nobleman.
 pilot on the Tomcat. Despite being a scientist, he is dressed like an actual pilot.
 pilot on the Tomcat. Despite the fact he is a scientist, he is dressed very much like a butler.

* " Papadoll, bringing him to Banipal Witt. Due to over-exposure from the sun, he mutated into a gigantic, flying dog monster. He had been in Banipal Witt for so long that he had forgotten who he really was. Only Toriyasu had the power to restore Papadoll to his former self. Papadoll was (of course) restored to his former self by the end of the movie, and once again became Toriyasus friend. He was the only one to see Henoji, Suttoboke and HoiHoi enter Toriyasu and Meekos bedroom.

*  of Master Sandada, who was driven insane by a cursed painting of Buburina. Shortly after, he stole Master Sandadas invaluable and powerful Sorcerers Arm and abandoned him to serve Buburina. He serves as Buburinas right-hand man. He desires Buburinas affection (due to the curse from the painting), which makes him a very dedicated minion. His cruelty towards Toriyasu, Meeko and the members of the resistance eventually comes back to turn against him, as he is ultimately tangled in the remains of the giant mouse balloon that Buburina made.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 