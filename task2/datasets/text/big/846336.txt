Tokyo Story
{{Infobox film
| name           = Tokyo Story
| image          = Tokyo Story poster.jpg
| caption        = Japanese theatrical release poster
| director       = Yasujirō Ozu
| producer       = Takeshi Yamamoto
| writer         = Kōgo Noda Yasujirō Ozu
| narrator       =
| starring       = Chishū Ryū Chieko Higashiyama Setsuko Hara
| music          = Kojun Saitō
| cinematography = Yūharu Atsuta
| artdirector    =
| editing        = Yoshiyasu Hamamura
| studio         = Shochiku
| distributor    =
| released       =  
| runtime        = 136 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}} British Film Institute lists of the greatest films ever made.

==Plot summary== Onomichi in southwest Japan with their unmarried youngest daughter Kyōko (played by Kyōko Kagawa). They have a total of 5 children, who are all grown up. The couple travel to Tokyo to visit their son and daughter and daughter-in-law.

Their eldest son, Kōichi (Sō Yamamura), is a pediatrician married to Fumiko. Kōichi and Fumiko have two sons, Minoru and Isamu. Shūkichi and Tomis eldest daughter, Shige (Haruko Sugimura), is married to Kurazō. Shige runs a hairdressing salon. Kōichi and Shige are both busy with work and their families, and do not have much time for their parents. Only the couples widowed daughter-in-law, Noriko (Setsuko Hara), goes out of her way to entertain them.  She takes Shūkichi and Tomi on a sightseeing tour of metropolitan Tokyo.
 hot spring spa at Atami, but the parents return early because the nightlife at the hotel interrupts their sleep.  When they return, Shige explains that she sent them to Atami because she wanted to use their bedroom for a meeting. Tomi goes to stay with Noriko, whose husband, Shōji, died eight years ago in the war. Tomi advises Noriko to remarry. Shūkichi, meanwhile, gets drunk with some old friends, then returns to Shiges salon.

The couple remark on how their children have changed, and they leave for home.  During the journey Tomi is taken ill, and they make an unplanned stop at Osaka, where they had planned to meet their youngest son, Keizō (Shirō Ōsaka), without disembarking from the train.  When they reach Onomichi, Tomi becomes critically ill.  Kōichi, Shige and Noriko rush to Onomichi, on receiving telegrams, to see Tomi, who dies shortly afterwards. Keizō arrives late as he is outstationed.

After the funeral, Kōichi, Shige and Keizō decide to leave immediately, with only Noriko not returning. After they leave, Kyōko complains to Noriko that they are selfish and inconsiderate. Noriko responds that everyone has their own life to lead and that the drift between parents and children is inevitable.

After Kyōko leaves for school, Noriko informs her father-in-law that she must return to Tokyo that afternoon. Shūkichi tells her that she has treated them best despite not being related by blood. Noriko insists on her own selfishness; Shūkichi credits her protests to humility. He gives her a watch from the late Tomi as a memento, and advises her to remarry. Noriko breaks down in tears and confesses her loneliness.  At the end, the train with Noriko speeds from Onomichi back to Tokyo, leaving behind Kyōko and Shūkichi.

==Hirayama family tree==

*Shūkichi (Grandfather) and Tomi (Grandmother)
**Kōichi (eldest son)
***Fumiko (Kōichis wife)
***Minoru (Kōichis son)
***Isamu (Kōichis son)
**Shige (eldest daughter)
***Kurazō (Shiges husband)
**Shōji (2nd son, deceased)
***Noriko (Shōjis wife)
**Keizō (youngest son)
**Kyōko (youngest daughter)

==Cast==
* Chishū Ryū as Shūkichi Hirayama
* Chieko Higashiyama as Tomi Hirayama
* Setsuko Hara as Noriko Hirayama
* Haruko Sugimura as Shige Kaneko
* Sō Yamamura as Kōichi Hirayama
* Kuniko Miyake as Fumiko Hirayama
* Kyōko Kagawa as Kyōko Hirayama
* Eijirō Tōno as Sanpei Numata
* Nobuo Nakamura as Kurazō Kaneko
* Shirō Ōsaka as Keizō Hirayama
* Hisao Toake as Osamu Hattori
* Teruko Nagaoka as Yone Hattori
* Mutsuko Sakura as a patron of the Oden Restaurant
* Toyo Takahashi as Shūkichi Hirayamas neighbour (as Toyoko Takahashi)
* Tōru Abe as a railway employee
* Sachiko Mitani as Norikos neighbour 
* Zen Murase as Minoru Hirayama, Kōichis son 
* Mitsuhiro Mori as Isamu Hirayama, Kōichis son 
* Junko Anami as a beauty salon assistant 
* Ryōko Mizuki as a beauty salon client 
* Yoshiko Togawa as a beauty salon client 
* Kazuhiro Itokawa as a student 
* Keijirō Morozumi as a police agent 
* Tsutomu Nijima as Norikos office boss 
* Shōzō Suzuki as Norikos office colleague 
* Yoshiko Tashiro as a hotel maid 
* Haruko Chichibu as a hotel maid 
* Takashi Miki as a singer 
* Binnosuke Nagao as the doctor at Onomichi

==Production==
  Onomichi for another month before shooting started. Shooting and editing the film took place from July to October 1953. Ozu used the same film crew and actors he had worked with for many years. 

==Reception and legacy== greatest films" The Mirror in 2002, and in 2012 it topped the poll, receiving 48 votes out of the 358 directors polled.    

It holds a 100% "Fresh" rating on the review aggregate website Rotten Tomatoes, based on 38 critical reviews, with also the highest average critical score on the website at 9.7/10.  John Walker, former editor of the Leslie Halliwell|Halliwells Film Guides, places Tokyo Story at the top of his published list of the best 1000 films ever made. Tokyo Story is also included in film critic Derek Malcolms The Century of Films, {{cite journal
|first=Derek|last=Malcolm
|authorlink=Derek Malcolm
|journal=The Guardian
|url=http://www.guardian.co.uk/culture/2000/may/04/artsfeatures1
|title=Yasujiro Ozu: Tokyo Story
|date=4 May 2000|accessdate=7 August 2012}}  {{cite book|first=Derek|last=Malcolm|authorlink=Derek Malcolm
|title=A Century of Film |year=2000|publisher=IB Tauris|pages=85–87}}  a list of films which he deems artistically or culturally important, and Time (magazine)|Time magazine lists it among its Times All-TIME 100 Movies|All-Time 100 Movies. 
Roger Ebert includes it in his series of great movies,  and Paul Schrader placed it in the "Gold" section of his Film Canon. 

==Influence==
 Cherry Blossoms German director Doris Dörrie drew inspiration from Tokyo Story in particular with regard to the family constellation and the death of the mother in a resort at the seaside. 

In 2013 Yōji Yamada made a remake under the title Tōkyō Kazoku.

==Style==
Like all of Ozus sound films, Tokyo Story&#39;s pacing is slow, {{cite book|author1=David Bordwell|author2=Kristin Thompson|title=Film History: An Introduction|year=2003|edition=2nd
|publisher= McGraw-Hill|page= 396}}  yet has a rhythm which made Ozus film a popular entertainment in Japan.    Important events are often not shown on screen, only being revealed later through dialogue. For example, the train journeys to and from Tokyo are not depicted.  A distinctive camera style is used, in which the camera height is low and almost never moves; film critic Roger Ebert notes that the camera moves once in the film, which is "more than usual" for an Ozu film. {{cite web
|url=http://www.rogerebert.com/reviews/great-movie-tokyo-story-1953 |date=November 9, 2003
|title=Tokyo Story Movie Review & Film Summary (1953)
|first=Roger|last=Ebert|accessdate=6 August 2012}} 

==Release==
Tokyo Story was released on November 3, 1953 in Japan.

===Home media=== Tartan Video in Region 2. In 2010, the BFI released a Region 2 Dual Format Edition (Blu-ray + DVD). {{cite web |url=http://filmstore.bfi.org.uk/acatalog/info_17155.html
|title=Tokyo Story: Dual Format Edition|accessdate=2 August 2012}}  Included with this release is a standard definition presentation of Brothers and Sisters of the Toda Family.

== See also ==
* List of films considered the best

==References==
 

==Further reading==
*  Collection of seven essays about the film.

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 