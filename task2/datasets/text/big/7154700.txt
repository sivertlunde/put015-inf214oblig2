Retro Puppet Master
 
{{Infobox film
| name           = The Retro Puppet Master
| image          = Retro_Puppet_Master.jpg
| caption        = DVD cover
| director       = David DeCoteau
| producer       = Charles Band Kirk Edward Hansen Vlad Paunescu Dana Scanlan Mona C. Vasiloiu
| writer         = Charles Band Benjamin Carr David Schmoeller
| starring       = Greg Sestero Brigitta Dau Jack Donner Stephen Blackehart  and Guy Rolfe
| music          = John Massari
| cinematography = Viorel Sergovici
| editing        = Don Adams Full Moon Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
}}

Retro Puppet Master (also known as Retro Puppetmaster) is a 1999  , and stars  .

==Plot== Toulon and his little friends are still on the run, and decide to hide in the Kolewige, an inn thats 4 miles from the Swiss border. Blade finds the wooden head of an old puppet named Cyclops in their trunk, and when Toulon sees it, he then tells his puppets the adventures with the woman he loves, and his retro puppets, starting in Cairo, Egypt, in 1902.
 secret of life, and is fleeing the servants of an evil Egyptian god, named Sutekh. Two servants, imbued with magical power from Sutekh, attack him, but are killed easily by Afzel and his own magical power. After dispatching the two servants he begins his journey to Paris.

Meanwhile, Sutekh has given life to three of his oldest servants—Egyptian mummies. After they rise from their chamber they too begin to pursue Afzel.  Cut to Paris, where a young Andre Toulon is putting on a puppet show of Dantes Divine Comedy.  Watching from the crowd is Elsa, who has left her Cruel|cold-hearted abusive ambassador father and harmless mother (who wasnt in this) to view the wonders of the country and has decided to see the play. In the sewers nearby the three mummies have hired two thugs to kill Afzel.  The thugs are necessary since Afzel has the power to sense the coming of the mummies.

Afzel is beaten brutally until Elsa, leaving the theater, sees them and cries for help. The thugs then scatter, leaving Toulon and Elsa to pick Afzel up and bring him inside. Later, when he stirs from his sleep, he talks with Toulon and reveals that he knows the secret of life, and its the only thing that can protect humankind when the elder gods rise up in 100-1,000 years, and needs to pass it on to Toulon. Toulon is skeptical until Afzel begins to make the puppets move. Now Toulon realizes he is genuine and begins to learn his powers. Then Afzel starts to make the puppets draw a barrier thats supposed to protect him if the servants come back.

While stepping outside for a brief moment, Toulon begins to talk to a beggar who has sat on the steps since the beginning of the movie.  However, he is dead, and when Andre becomes aware of this he begins to mourn.  Afzel tells him to bring him inside to teach him the true secret of life. After bringing him inside they use a ring to transfer the soul of the beggar to the puppet, "Pinhead". The puppet starts to move but, after a few questions the puppet runs away into the theater. Later on, Elsa returns to talk to Toulon, until her fathers rude servants come by, and they take Elsa and Toulon back to her house, and after Toulon talks to the father, he gets knocked out, and is then thrown into the woods.

The next day, back at the theater, Valentin storms in as the barrier written on paper falls off the wall. The three mummies see their chance to attack and begin their rampage through the theater. Valentin finishes repairing the door as two of the mummies break in and kill him. Vigo runs backstage to Duval and Latour and falls dead. Duval stabs the lead mummy in his hand as the mummy kills him with his other hand. Latuor gets out a gun and shoots the lead mummy three times as the other mummy comes in through the back door and all three of them use their magic to kill him.  Afzel appears and says Sutekh shall not claim his life, and kills himself with his own magic. Satisfied with victory, the mummies begin to leave. Once Toulon returns from the woods, he sees what has happened and acts quickly by putting their souls inside his puppets.

The men return, having sensed someone with the knowledge of the secret of life, and try to kill Toulon.  His puppets, however, kill one of the mummies with Six Shooter by shooting the chandelier chains, causing it to crush the servant. The two henchmen retreat in order to plot the capture of Elsa to lure Toulon into a trap.

Feeling that he had won the battle, Andre and his puppets go to a train station to leave Paris to Kara, Togo before things start to get bad, not realizing they had already begun. The servants killed her parents, the guards and capture Elsa, and then send a dream to Toulon that shows Elsa tied up, and a train. Knowing the meaning, he quickly changes trains to Merca, Somalia and gets his puppets ready for the showdown. When the train leaves, Andre looks around, having released his puppets and letting them follow him throughout the train, until finally coming to the last car and finding Elsa tied up like in his dream. The two men appear and ask for the secret of life. Andre displays the scroll where the secret is written and asks, "How do you know I havent copied it?" to which the leader states he did not have enough time to do so, but still is doubtful he hadnt. Andre, noticing he is distracted, attacks the leader while his puppets attack the other. A large struggle breaks out, and the other henchman is killed with the final leader of the three being thrown from the car. Freeing Elsa, the group rides away in the train, beginning their adventure. After telling the story, the puppets wonder what happened to the other puppets. Andre tells them thats another story, which he will tell them in the future.

===Timeline Issue===
* In  , it is hinted that Andre Toulon killed himself on March 15, 1945, shortly before  .

==Cast== Elder Andre Toulon Young Andre Toulon The Room, includes two chapters on his casting in and filming of Retro Puppet Master.
* Brigitta Dau – Elsa
* Stephen Blackehart – First Servant
* Jack Donner – Afzel
* Robert Radoveanu – Second Servant
* Vitalie Bantas – Third Servant
* Sandu Teodor – Latour
* George Calin – Valentin
* Juliano Doman – Vigo
* Vlad Dulea – Duval
* Dan Fintescu – Beggar
* Serban Celea – Father
* Elvira Deatcu – Margarette
* Claudiu Trandafir – Leader
* Marcello Cobzariu – First Tough
* Viorel Manole – Second Tough
* Mihai Verbinschi – First Pursuer
* Adrian Ciobanu – Second Pursuer
* Razvan Popa – Assistant
* Aurelian Popa – Official
* Cristian Irimia – Conductor
* Ion Bechet – Ticket Agent
* ? - Sutekh (voice)

===Featured puppets=== Blade
* Pinhead
* Leech Woman Jester
* Tunneler Six Shooter

===Retro Puppets=== Retro Blade Retro Pinhead Drill Sergeant (Retro Tunneler) Retro Six-Shooter Doctor Death Cyclops

==External links==
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 