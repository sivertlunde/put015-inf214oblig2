Letter at Dawn
{{Infobox film
 | name =Letter at Dawn
 | image = Letter at Dawn.jpg
 | caption =
 | director = Giorgio Bianchi
 | writer = Aldo De Benedetti Renzo Rossellini
 | cinematography =   Václav Vích   Augusto Tiezzi
 | editing =  
 | producer =Giuseppe Amato
 | distributor =
 | released =  1948
 | runtime =
 | awards =
 | country = Italy
 | language =Italian
 | budget = 
 }} 1948 Cinema Italian  drama film directed by Giorgio Bianchi.      

In 2008 it was restored and shown as part of a retrospective "Questi fantasmi: Cinema italiano ritrovato" at the 65th Venice International Film Festival. 

== Cast == 

*Fosco Giachetti as  Carlo Marini
*Jacques Sernas as  Mario Maggi
*Lea Padovani as  Anna Maggi
*Olga Villi as  Renata Tatiana Pavlova as Countess Koloshky
*Vittorio Sanipoli as  Enrico Verri
*Franca Marzi as  Lilly 
*Margherita Bagni as  Sister Maria della Carità
*Nerio Bernardi as  Augusto
*Paolo Ferrari as  Augustos son
*Salvo Randone as  Donati 
*Ernesto Calindri as the public prosecutor

==References==
 

==External links==
* 
 
 
 
 
 


 