Mamma Mia! (film)
{{Infobox film
| name           = Mamma Mia!
| image          = MammaMiaTeaserPoster.JPG
| alt            =  
| caption        = Promotional poster
| director       = Phyllida Lloyd
| producer       = Judy Craymer Gary Goetzman  Tom Hanks
| screenplay     = Catherine Johnson
| based on       =  
| starring       = Meryl Streep Pierce Brosnan Colin Firth Stellan Skarsgård Julie Walters Dominic Cooper Amanda Seyfried Christine Baranski
| music          = ABBA
| cinematography = Haris Zambarloukos
| editing        = Lesley Walker
| studio         = Littlestar Playtone
| distributor    = Universal Pictures
| released       =  
| runtime        = 109 minutes
| country        = United Kingdom United States Sweden
| language       = English
| budget         = $52 million
| gross          = $609.8 million 
}} adapted from West End/2001 Broadway Mamma musical of pop group Universal Pictures Mamma Mia". Meryl Streep heads the cast, playing the role of single mother Donna Sheridan. Pierce Brosnan (Sam Carmichael), Colin Firth (Harry Bright), and Stellan Skarsgård (Bill Anderson) play the three possible fathers to Donnas daughter, Sophie, played by Amanda Seyfried.

== Plot ==
 
  I Have a Dream") to different men.

Sophies bridesmaids, Ali and Lisa, arrive. Sophie reveals that she found her mothers diary and has three possible fathers: architect Sam Carmichael from the United States, adventurer and writer Bill Anderson from Sweden, and banker Harry Bright from the United Kingdom. She invited them without telling her mother, believing that after she spends time with them she will know who her father is (" ") and swear not to reveal her secret.
 Mamma Mia"), and is adamant that they leave. She confides in Tanya and Rosie ("Chiquitita") that she is uncertain which of the men is Sophies father. Tanya and Rosie rally her spirits by getting Donna to dance with the female staff and islanders ("Dancing Queen"). Sophie finds the men aboard Bills yacht, and they sail around Kalokairi ("Our Last Summer") and tell stories of Donna as a carefree girl. Sophie plans to tell her fiancé Sky about her ploy, but loses her nerve. Sky and Sophie sing to each other ("Lay All Your Love on Me"), but Sky is snatched for his bachelor party.
 Super Trouper"). The festivities are interrupted by the arrival of Sam, Bill and Harry. Sophie decides to talk with each of her prospective dads alone. While her girlfriends dance with the men ("Gimme! Gimme! Gimme! (A Man After Midnight)"), Sophie learns from Bill that Donna received the money to invest in her villa from his great aunt Sofia. Sophie guesses she must be Sofias namesake and Bill is her father. She asks him to give her away and keep their secret from Donna until the wedding. Sophies happiness is short-lived as Sam and Harry each tell her they must be her dad and will give her away ("Voulez-Vous (song)|Voulez-Vous"). Sophie cannot tell them the truth and, overwhelmed by the consequences of her actions, faints.

In the morning, Rosie and Tanya assure Donna they will take care of the men. Bill and Harry are about to confide in each other, but are interrupted by Rosie. Donna confronts Sophie, believing Sophie wants the wedding stopped. Sophie says that all she wants is to avoid her mothers mistakes. Donna is accosted by Sam, concerned about Sophie getting married so young. Donna confronts him and they realize they still have feelings for each other ("SOS (ABBA song)|SOS"). Tanya and young Pepper continue their flirtations from the previous night ("Does Your Mother Know"). Sophie confesses to Sky and asks for his help. He reacts angrily to Sophies deception and she turns to her mother for support. As Donna helps her daughter dress for the wedding, their rift is healed and Donna reminisces about Sophies childhood and how quickly she has grown ("Slipping Through My Fingers"). Sophie asks Donna to give her away. As the bridal party walks to the chapel, Sam intercepts Donna. She reveals the pain she felt over losing him ("The Winner Takes It All").
 When All Is Said and Done"), which prompts Rosie to make a play for Bill ("Take a Chance on Me"). All the couples present proclaim their love ("Mamma Mia" reprise). Sophie and Sky sail away ("I Have a Dream" reprise).

During the principal credits, Donna, Tanya and Rosie reprise "Dancing Queen", followed by "Waterloo (ABBA song)|Waterloo" with the rest of the cast. Amanda Seyfried sings "Thank You for the Music" over the end credits, followed by an instrumental of "Does Your Mother Know".
 

== Cast ==
* Meryl Streep as Donna Carmichael (née Sheridan), Sophies mother, owner of the hotel Villa Donna, and wife of Sam at the end.
* Amanda Seyfried as Sophie Sheridan, Donnas daughter, Skys fiancée.  American architect.  English banker; based on "Our Last Summer", which he sings at one point. Swedish sailor and travel writer.
* Dominic Cooper as Sky, Sophies fiancé, designing a website for the hotel.
* Julie Walters as Rosie Mulligan , one of Donnas former bandmates in Donna and the Dynamos; an unmarried fun-loving author.
* Christine Baranski as Tanya Chesham-Leigh, Donnas other former bandmate; a rich three-time divorcee.
* Philip Michael as Pepper, Skys best man who likes Tanya. He is also a bartender.
* Juan Pablo Di Pace as Petros.
* Ashley Lilley as Ali, close friend of Sophie and her bridesmaid.
* Rachel McDowall as Lisa, close friend of Sophie and her bridesmaid. Enzo Squillino as Gregoris, one of Donna Sheridans employees.
* Niall Buggy as Father Alex, priest who nearly married Sophie and Sky, but ends up marrying Sam and Donna.

;Cameo appearances and Uncredited Roles
* Benny Andersson as Piano player
* Björn Ulvaeus as Greek god
* Rita Wilson as Greek goddess

  appeared together with the films cast in 2008.]]

== Musical numbers ==
 
The following songs are included in the film, of which 18 (including a hidden track) are on the soundtrack album, and 3 are excluded: I Have a Dream" – Sophie
# "Honey, Honey" – Sophie, Ali and Lisa
# "Money, Money, Money" – Donna, Tanya, Rosie and Company Mamma Mia" – Donna, Sophie, Ali, Lisa and Greek Chorus
# "Chiquitita" – Rosie, Tanya and Donna
# "Dancing Queen" – Tanya, Rosie, Donna and Company
# "Our Last Summer" – Harry, Bill, Sam and Sophie
# "Lay All Your Love on Me" – Sky, Sophie, Skys Bachelor party friends. Super Trouper" – Donna, Tanya and Rosie
# "Gimme! Gimme! Gimme! (A Man After Midnight)" – Sophie, Donna, Tanya, Rosie, Ali, Lisa and Greek Chorus
# "Voulez-Vous (song)|Voulez-Vous" – Donna, Sam, Tanya, Rosie, Harry, Bill, Sky, Ali, Lisa and Pepper The Name of the Game" – Sophie (deleted scene)
# "SOS (ABBA song)|SOS" – Sam, Donna and Greek Chorus
# "Does Your Mother Know" – Tanya, Pepper, Lisa, Guys and Girls
# "Slipping Through My Fingers" – Donna and Sophie
# "The Winner Takes It All" – Donna
# "I Do, I Do, I Do, I Do, I Do" – Sam, Donna and Company When All Is Said and Done" – Sam, Donna and Company
# "Take a Chance on Me" – Rosie, Bill, Tanya, Pepper, Harry and Company
# "Mamma Mia!" (Reprise) – Company
# "I Have a Dream" (Reprise) – Sophie
# "Dancing Queen" (Reprise) – Donna, Rosie and Tanya
# "Waterloo (ABBA song)|Waterloo" – Donna, Rosie, Tanya, Sam, Bill, Harry, Sky and Sophie
# "Thank You for the Music" – Sophie

== Production ==
  Damouchari in the Pelion area of Greece. On Skopelos, Kastani beach on the south west coast was the films main location site.  The producers built a beach bar and jetty along the beach, but removed both set pieces after production wrapped. {{cite news
| title = Mamma Mia! – Unfazed by the fuss in Skopelos
| publisher = The Telegraph | date = 2008-07-15
| url = http://www.telegraph.co.uk/travel/artsandculture/2229868/Mamma-Mia---Unfazed-by-the-fuss-in-Skopelos.html
| location=London
| first=Paul
| last=Mansfield
| accessdate=May 12, 2010
}}  A complete set for Donnas Greek villa was built at the 007 stage at Pinewood Studios and most of the film was shot there. Real trees were utilised for the set, watered daily through an automated watering system and given access to daylight in order to keep them growing.
 Lime Street in the City of London. He dashes down the escalators and through the porte-cochere, where yellow cabs and actors representing New York mounted police were used for authenticity. 
 ]]
 Hong Kong and Whampoa dockyards. {{cite web
|title= 45’ Teak Ketch 1933. Yacht for sale from classic yacht broker in Poole
|url= http://www.sandemanyachtcompany.co.uk/details/?id=233
|work= Sandeman Yacht brokerage Poole
|publisher= Sandeman Yacht Company
|date=
|accessdate=2009-09-28}}  
{{cite web
|title= Tai-Mo-Shan
|url= http://www.coburgbrokers.com/tai.html
|work= Coburg Yacht Brokers website
|publisher= Coburg Yacht Brokers
|accessdate=2009-09-28}} 
 Postcards from A Prairie Home Companion. {{cite news
| title = Meryl Streep the singing and dancing queen
| publisher = The Telegraph | date = 2008-07-04
| url = http://www.telegraph.co.uk/arts/main.jhtml?xml=/arts/2008/07/04/bfmeryl104.xml
| location=London
| first=John
| last=Hiscock
| accessdate=May 12, 2010
}}  She had been a fan of the stage show Mamma Mia! since seeing it on Broadway in September 2001, when she had found the show an affirmation of life in the midst of the destruction of 9/11. 

== Release ==
Though the world premiere of the film occurred elsewhere, most of the media attention was focused on the Swedish premiere, where Anni-Frid Lyngstad and Agnetha Fältskog joined Björn Ulvaeus and Benny Andersson with the cast at the Rival Theatre in Mariatorget, Stockholm, owned by Andersson, on July 4, 2008. It was the first time all four members of ABBA had been photographed together since 1986. {{cite web
|author=Sandra Wejbro
|title=ABBA återförenades på röda mattan (Swedish)
|url=http://www.aftonbladet.se/nojesliv/film/article2836959.ab
|publisher=Aftonbladet
|date=2008-07-04
|accessdate=2008-07-06
}}  

=== Reception ===
  Escape to the Movies said it was "so base, so shallow and so hinged on meaningless spectacle, its amazing it wasnt made for men".  The Daily Telegraph stated that it was enjoyable but poorly put together ("Finding the film a total shambles was sort of a shame, but I have a sneaking suspicion Ill go to see it again anyway."),  whereas Empire (film magazine)|Empire said it was "cute, clean, camp fun, full of sunshine and toe tappers." 
 New York Creative Loafing Charlotte said he "looks physically pained choking out the lyrics, as if hes being subjected to a prostate exam just outside of the cameras eye." 

=== Box office ===
Mamma Mia! grossed a worldwide total of $602,609,487 and is the fifth highest-grossing film of 2008.    It eventually became the highest grossing musical of all time in terms of worldwide take. It is the third highest-grossing film of 2008 internationally (i.e., outside North America) with an international total of $458,479,424 and the thirteenth highest gross of 2008 in North America (the US and Canada) with $144,130,063.
 the sixth highest grossing film of all time at the UK box office (behind Skyfall, Avatar (2009 film)|Avatar, Titanic (1997 film)|Titanic, Toy Story 3, and Harry Potter and the Deathly Hallows – Part 2).  The film opened at #1 in the U.K, taking £6,594,058 on 496 screens. It managed to hold onto the top spot for 2 weeks, narrowly keeping Pixars WALL-E from reaching #1 in its second week.

When released on July 3 in Greece, the film grossed $1,602,646 in its opening weekend, ranking #1 at the Greek box office. 
 The Dark Into the Woods. 

== Awards and nominations ==
 
* Golden Globe Awards 
** Best Motion Picture: Comedy or Musical—nominated
** Best Actress in a Motion Picture: Comedy or Musical (Meryl Streep)—nominated

* BAFTA Awards 
** Outstanding British Film—nominated
** The Carl Foreman Award for Special Achievement by a British Director, Writer or Producer for their First Feature Film (Judy Craymer)—nominated
** Outstanding Music (Benny Andersson, Björn Ulvaeus)—nominated

*Golden Raspberry Awards   
** Worst Supporting Actor (Pierce Brosnan)—won

== Sequel ==
Because of the films financial success, Hollywood studio chief David Linde, the co-chairman of Universal Studios told The Daily Mail that it would take a while, but there could be a sequel. He stated that he would be delighted if Judy Craymer, Catherine Johnson, Phyllida Lloyd, Benny Andersson, and Björn Ulvaeus agreed to the project, noting that there are still plenty of ABBA songs to use. 

== Home media ==
In November 2008, Mamma Mia! became the fastest-selling DVD of all time in the UK, according to Official UK Charts Company figures. It sold 1,669,084 copies on its first day of release, breaking the previous record (held by Titanic) by 560,000 copies. By the end of 2008, The Official UK Charts Company declared it had become the biggest selling DVD ever in the UK, with one in every four households owning a copy (over 5 million copies sold).  The record was previously held by   with sales of 4.7 million copies.

In the United States the DVD made over $30 million on its first day of release. 

By December 31, 2008, Mamma Mia! had become the best-selling DVD of all time in Sweden with 545,000 copies sold. 

;Single-disc features
* Sing-along The Name of the Game" deleted musical number director Phyllida Lloyd

The single-disc DVD released in Sweden on 26 November contains all of the following:
* Sing-along
* "The Name of the Game" deleted musical number
* Deleted scenes
* Outtakes
* The Making of Mamma Mia! featurette
* Anatomy of a Musical Number: "Lay All Your Love on Me"
* Becoming a Singer featurette
* A look inside Mamma Mia! featurette
* "Gimme! Gimme! Gimme!" music video cameo
* Audio commentary with director Phyllida Lloyd
* German and English audio Swedish and Icelandic

;2-disc special edition
The widescreen single-disc includes a bonus disc which includes:
* Limited time only digital copy
* Deleted scenes and outtakes
* The Making of Mamma Mia! featurette
* Anatomy of a Musical Number: "Lay All Your Love on Me"
* Becoming a Singer featurette Amanda
* On Location in Greece featurette
* A Look Inside Mamma Mia! featurette
* "Gimme! Gimme! Gimme!" music video
* Björn Ulvaeus cameo

;Blu-ray exclusives
* Universal Pictures U-Control 
* Behind the Hits (details and trivia of the music while the musical performance plays)
* Picture-in-picture (access to cast and crew interviews and behind the scene while the movie plays)

;Collectors Edition with Exclusive Bonus Disc
A bonus disc is included in a separate case and bundled with the movie, and includes: 
* A Talented Trio: The Creators of Mamma Mia! (creator bios)
* Christine Baranski and the Boys (choreography of "Does Your Mother Know?")
* Breaking Down "Voulez-Vous" (choreography)
* More of "Gimme! Gimme! Gimme!" (choreography)
* Meryls Big Number (choreography of "Mamma Mia")
* VH1s 10 Most Excellent Things: Mamma Mia! (documentary clip)

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 