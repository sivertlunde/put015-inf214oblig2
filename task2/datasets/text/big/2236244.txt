London Suite (play)
London Suite is a play by Neil Simon, consisting of four one-act plays. London Suite also was a 1996 television movie. It is in a similar style to Simons earlier works Plaza Suite and California Suite.

==Productions== Daniel Sullivan, the cast featured Jeffrey Jones, Carole Shelley, Amy Ryan, Paxton Whitehead, Barbara Dirickson, Sean G. Griffin and Rex McDowell. The plays are "Going Home", "Settling Accounts", "Diana and Sidney" (which involve two characters from California Suite), and "The Man on the Floor". 
 
London Suite opened   (Lauren, Grace and Annie), Jeffrey Jones (Brian and Mark) and Brooks Ashmanskas (Bellman).   The production was nominated for the Outer Critics Circle Awardas Outstanding Off-Broadway Play. 

==Plot== Hyde Park in a series of four plays:

*Settling Accounts
Brian Cronin and Billy Fox. Brian is a successful Welsh novelist; Billy is his manager, whom Brian has caught in the process of running off with all his money. In the film, this pair was changed to Debra and Paul Dolby, a newlywed couple from New York who are on their honeymoon. Paul has disappeared after the couple had a fight on the plane, and now Debra is caught in a chain of increasingly ridiculous lies when she runs into his relatives at the hotel.

* Going Home
Sharon and Lauren Semple. A mother and daughter on a shopping trip. Lauren convinces her mother to go on a date with an elderly man, even though Sharon is heavily against it.

* Diana and Sidney
Sidney and Diana Nichols. A divorced couple: she is an actress, and he is living with his male lover. Now he claims he needs money from her to help pay the medical bills for his partner who is dying from AIDS related complications. The pair are based on characters from California Suite, played in the 1978 film by Michael Caine and Maggie Smith.

* The Man on the Floor American couple Wimbledon Championships. Their plans are put on hold, however, when Anne loses the tickets, Marks back gives out, and they are asked to move because they have accidentally been given Kevin Costners suite.

With the exception of Sidney and Dianas storyline (and Brian and Billys in the play), the plots are largely comedy|comedic.

==Film==
The film, a Hallmark Entertainment production, was televised on September 15, 1996. The film was directed by Jay Sandrich with the screenplay by Simon,   and starred:

*Kelsey Grammer - Sidney Nichols
*Patricia Clarkson - Diana Nichols
*Julia Louis-Dreyfus - Debra Dolby
*Jonathan Silverman - Paul Dolby
*Madeline Kahn - Sharon Semple
*Margot Steinberg - Lauren Semple
*Michael Richards - Mark Ferris
*Julie Hagerty - Anne Ferris
*Richard Mulligan - Dennis Cummings, Sharons date.
*Kristen Johnston - Grace Chapman, Dianas assistant.
*Jane Carr - Mrs. Sitgood, the concierge. magician with backs!")
*William Franklyn - Widely, the hotel supervisor

==References==
 

 

 
 