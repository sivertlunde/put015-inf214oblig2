Best Boy (film)
 
{{Infobox film
| name           = Best Boy
| image          =
| image_size     = 
| caption        = 
| director       = Ira Wohl
| producer       = 
| writer         = Ira Wohl
| narrator       = 
| starring       = 
| music          = 
| cinematography = Tom McDonough
| editing        = 
| distributor    = International Film Exchange
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
}}
Best Boy is a 1979 documentary made by Ira Wohl. The film received critical acclaim, and won many awards including the Academy Award for Best Documentary Feature in 1979.   

The film follows Iras mentally handicapped cousin, Philly Wohl, who at that time was 52 years old and still living with his elderly parents. Ira forces his aunt and uncle to realize that they will not be around to care for Philly forever, and that they must start making preparations for when that time should come. Philly then begins to attend classes in New York City to learn how to take care of himself and become independent.

Phillys father, Max Wohl, dies during the course of the film. His mother, Pearl, died in 1980. Philly is now an  , and he lives in a group home where he has learned to basically take care of himself. 

A sequel entitled Best Man: Best Boy and All of Us, 20 Years Later, was produced in 1997.
Following the sequel, in 2006, Ira made Best Sister which rounded off the trilogy by looking at the effect Phillys sister had on his current life.

==References==
 

==External links==
* 
*  


 

 
 
 
 
 
 
 
 

 