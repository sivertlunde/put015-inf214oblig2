Haathon Ki Lakeeren
{{Infobox film
| name           = Haathon Ki Lakeeren
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        =  Chetan Anand
| producer       = Chetan Anand	
| writer         = Veera Singh
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Sanjeev Kumar Zeenat Aman Jackie Shroff Priya Rajvansh
| music          = Pyare Mohan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood Chetan Anand. It stars Sanjeev Kumar, Zeenat Aman, Jackie Shroff and Priya Rajvansh in pivotal roles.

==Cast==
* Sanjeev Kumar...Dr. Bhanu Pratap
* Zeenat Aman...Geeta L. Singh
* Jackie Shroff...Lalit Mohan Singh
* Priya Rajvansh...Mala R. Singh Yadav
* Sudhir Dalvi...Singing Fakeer
* Bharat Kapoor...Lalits lawyer
* Subha Khote...Leela B. Pratap
* Dina Pathak...Geetas mom

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Hai Tuhi Zindagi Meri"
| Bela Sulakhe
|-
| 2
| "Rone Se Bichhde Yaar"
| Alka Yagnik, Bela Sulakhe
|-
| 3
| "Ho Yeh Jawani" Jatin
|-
| 4
| "Bekhudi Mein"
| Bela Sulakhe
|-
| 5
| "Chalti Nahin Insaan Ki"
| Mahendra Kapoor
|}
==External links==
* 

 
 
 

 