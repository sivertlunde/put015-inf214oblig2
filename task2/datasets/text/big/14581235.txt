Scanners II: The New Order
{{Infobox film
| title = Scanners II: The New Order
| image = Scanners2Poster.jpg Christian Duguay
| producer = Pierre David  
| writer = Characters: David Cronenberg Screenplay: B.J. Nelson
| starring = David Hewlett Deborah Raffin Yvan Ponton Isabelle Mejias
| music = Marty Simon
| cinematography = Rodney Gibbons
| editing = Yves Langlois
| studio = Malofilm
| distributor =  Triton Pictures  
| released =  
| runtime = 104 minutes
| country = Canada
| language = English
}} Christian Duguay. telepathic and telekinetic abilities) to do his bidding. 


==Plot== intern named David Kellum (David Hewlett) discovers that he has mental abilities allowing him to read and control the minds of others. When he moves from his country home to the city to continue his studies, he discovers he has difficulty controlling this ability because of the congestion of so many minds as well as the ability to hear many voices which greatly overwhelms him at first.

He stumbles across someone robbing a store and mentally kills the gunman. Police Commander John Forrester watches the stores security tape and plans to secretly enlist David to work for him. He tells David that he knows what he is, a Scanner, and that there are others like him around the world. He asks for Davids help tracking down elusive criminals and David agrees.

After capturing of a man who put strychnine in milk containers across the city, Forrester introduces David to another Scanner named Peter Drak, who also works for him. Drak proves to be more aggressive when using of his powers. Forrester teaches David some techniques using Drak as a test subject. Drak considers Kellum an enemy but is injected with the drug Eph2, a variant of Ephemerol, which calms him before he could harm David.

The drug Ephemerol was what originally created the Scanners. It was given to ease the discomfort of expecting mothers in the 1950s. A side effect caused the babies to develop into Scanners. Forrester tells David that although Eph2 manages to calm a Scanners mind, the drug is addictive and that he should never use it. Forrester encourages him to develop and control his mental abilities on his own, free from the drug.
 Chief of Police. Forrester reassures him that it is the best for everyone and that together they can stop the crime. David, feeling guilty, disagrees and questions Forresters agenda. He explains to the mayor how he forced her to appoint Forrester and apologizes. They then plan a way to remove Forrester from office. David tells Forrester that he is quitting, so Forrester has Drak attack David and kill the mayor before she can react. The mayor is killed but David escapes and hides at  his parents home.

He asks his parents about his abilities, and they tell him he was Adoption|adopted. They explain that his birth parents were Cameron Vale and Kim Obrist (Stephen Lack and Jennifer ONeill, respectively, from the first film Scanners). Vale and Obrist told them about his abilities and that he was in danger. They took David in as their own. David needs to go for a walk but while he is gone, Drak and his accomplices kill Davids mother and leave Davids father for dead. When David returns, his father tells about his older sister, Julie Vale. He leaves his father with the paramedics to search for his sister.

He finds Julie in a secluded cabin. She confirms who she is and explains that their parents were killed by Forrester for resisting him. She also states that her former boyfriend Walter agreed to test the earlier version 1 Ephemerol and was one of the first Scanners to use the drug, a more unstable version. Walter was kidnapped by Forrester and never seen again. Julie agrees to help David.

Together they go to Forresters secret compound and discover that Walter is alive and one of the test subjects addicted to Eph2. Julie and David disable the perimeter guards but inside the compound Julie is tranqilized by a Tranquilliser gun|dart. David leaves her behind so he can destroy the research and free the test subject Scanners.

Drak attacks David in the test subject quarters and tries to kill him. Drak almost succeeds, but is stopped by a combined attack by all the test subjects, which psychically drains Draks life force. The process reverses the physical damage the addicted Scanners were suffering from, as well as Davids injuries from Draks attack.

Forrester arrives on the scene, in response to David and Julies assault. At the same time television news reporters and camera crews also show up. He denies the existence of Scanners and any ties to the mayors death. David mentally forces Forrester to admit his involvement and motivation in front of the cameras. Afterward, Forrester grabs a gun and tries to shoot David, but David and Julie physically deform Forrester using telekinesis, all while cameras continue to record. David announces that Scanners mean no harm and only want to live in peace.

==Cast==
* David Hewlett as David Kellum
* Deborah Raffin as Julie Vale
* Yvan Ponton as Commander John Forrester
* Isabelle Mejias as Alice Leonardo
* Raoul Trujillo as Peter Drak Tom Butler as Doctor Morse
* Dorothée Berryman as Mayor
* Emily Eby as Reporter
* Vlasta Vrána as Lt. Gelson

==Release== Malofilm Home Europe by Starz Home Entertainment. Shout! Factorys new horror label Scream Factory released a Region 1 DVD/Blu-ray edition on September 10, 2013.

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 