The Shameless
 
{{Infobox film
| name           = The Shameless
| image          = 
| director       = Oh Seung-uk
| producer       = 
| writer         = Oh Seung-uk
| starring       = Kim Nam-gil   Jeon Do-yeon
| music          =  
| cinematography =   
| editing        = 
| studio         = Sanai Pictures
| distributor    = CJ Entertainment
| released       =  
| runtime        = 118 minutes
| country        = South Korea
| language       = Korean
}} romantic film noir thriller (genre)|thriller. 

The Shameless will make its world premiere in the Un Certain Regard section of the 2015 Cannes Film Festival.    

==Plot==
When mob boss Lee learns that his mistress, bar girl Kim Hye-kyung is having an affair with mob enforcer Park, Lee approaches small-time crooked detective Jung Jae-gon and promises him a substantial reward if he goes after Park. Jae-gon agrees, but a botched arrest sends Park on the run, and Jae-gon decides the best way to find him again is by sticking with Hye-kyung, hoping she will lead him to his suspect. Threatening his way into an undercover job at the nightclub she works at, they become friends and he wins her trust. But when Park returns asking Hye-kyung for money, Jae-gons newfound feelings of love and jealousy rise to the surface. 

==Cast==
*Kim Nam-gil as Jung Jae-gon
*Jeon Do-yeon as Kim Hye-kyung
*Park Sung-woong as Park
*Kwak Do-won as Lee
*Kim Min-jae
*Lee Dong-jin as Hwang Choong-nam
*Ha Eun-jin 
*Ji Seung-hyun
*Jo Won-hee

==Production==
This is the second film Oh Seung-uk has directed, 15 years after his directorial debut Kilimanjaro (2000). Oh was a prominent screenwriter in the 1990s, having written Green Fish (1997) and Christmas in August (1998).   
 Big Match. He was replaced by Kim Nam-gil. 

The Shameless began filming in June 2014. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 