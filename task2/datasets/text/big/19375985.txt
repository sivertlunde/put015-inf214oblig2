Great (1975 film)
{{Infobox Film
| name           = Great
| image          = Great Bob Godfrey 1975.jpg
| image_size     = 220px
| caption        = Title card of the film
| director       = Bob Godfrey
| producer       = 
| writer         = 
| narrator       = 
| starring       =  
| music          = Jonathan P. Hodge
| cinematography = 
| editing        = 
| distributor    = British Lion Films
| released       =  
| runtime        = 27 mins 50 secs
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 British Lion.
 BAFTA award for Best Animated Film in 1976.      

==Background==
The film recounts the life and works of the 19th century British civil engineer and architect Isambard Kingdom Brunel in a way that is affectionate while often tongue-in-cheek. The narrator, voiced by Harry Fowler, explains the triumphs and setbacks of Brunels career, comparing him to Archimedes, Isaac Newton and Albert Einstein. Richard Briers provides the voice of Brunel. There are numerous songs in the film, including "Get a big top hat if you want to get ahead". Great is primarily an animated film, although it is mixed media, combining some live action sequences with the animation.   

In an interview with The Guardian in April 2001, Bob Godfrey explained how the film came about: Id been reading a book about Brunel so I asked British Lion, who backed Kama Sutra  , if I could have some money to make a half-hour cartoon about a Victorian engineer. Yes, they said, heres £20,000. They thought the sun shone out of my arse at the time. Theyd have given me money to animate a toilet if Id asked them."    

Great is not available on DVD or Blu-ray. The official website of Bob Godfrey has stated that this is due to the films copyright status.   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 