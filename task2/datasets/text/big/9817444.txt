The Saga of Gosta Berling
{{Infobox film
| name            = The Saga of Gosta Berling
| image           = Gösta Berlings saga.jpg
| caption         = Theatrical release poster
| director        = Mauritz Stiller
| producer        =  
| screenplay      = Mauritz Stiller Ragnar Hyltén-Cavallius
| based on        =  
| starring        = Lars Hanson Greta Garbo
| cinematography = Julius Jaenzon
| editing         = 
| studio           = AB Svensk Filmindustri
| distributor     = AB Svenska Biografteaterns Filmbyrå
| released        = 10 March 1924
| country         = Sweden
| runtime         = 86 minutes (Part I) 80 minutes (Part II) 183 minutes 
| language        = Silent film Swedish intertitles
| budget          =  
}} novel of the same name by the Swedish author and Nobel Prize winner Selma Lagerlöf. It is also known as Gosta Berlings Saga, The Story of Gosta Berling and The Atonement of Gosta Berling.

==Plot==
The priest Gosta Berling gets fired because of his inappropriate life style. He entertains a wealthy lady who in return supports him. Following a variety of occurrences he teams up with former duchess Dohna. Together they start over.

==Cast==
* Lars Hanson as Gösta Berling
* Greta Garbo as Elizabeth Dohna
* Sven Scholander as Sintram
* Gerda Lundequist as Margaretha Samzelius
* Ellen Hartman-Cederström as Märtha Dohna
* Mona Mårtenson as Ebba Dohna
* Torsten Hammarén as Henrik Dohna
* Jenny Hasselqvist as Marianne Sinclaire
* Sixten Malmerfelt as Melchior Sinclaire
* Karin Swanström as Gustafva Sinclaire Oscar Byström as Patron Julius
* Hugo Rönnblad as Beerencreutz
* Knut Lambert as Örneclou
* Svend Kornbeck as Christian Bergh
* Otto Elg-Lundberg as Samzelius

==Release==

===Versions===
 , Solna Municipality|Solna.|right|200px|thumb]]
The film was originally released as two parts in Sweden, Gösta Berlings saga del I on 10 March 1924, and Gösta Berlings saga del II seven days later.    The two-part version was also used in Finland and Norway, but for the rest of the world a shorter, one part export version was made.

In 1927 the film was recut, shortening it by almost half the running time. This is the version that was archived for the future. In 1933 a sound version was released theatrically in Stockholm, with the intertext cut out, along with additional cutting and altered chronology for some of the scenes.

Twenty years later most of the missing material was discovered, and a restored version with new intertext was again released in theatres. This version has since had new parts added throughout the years as they have been found, but about 450 meters of film from the original cut is still missing.  In 2005, the film was given a new score by pianist and silent film music composer Matti Bye.

===Home media=== Kino International with the support of the Swedish Film Institute. The new release includes English subtitles, the new music score by Matti Bye, and restoration of the film to a length of 185 minutes. A Swedish DVD followed in 2007.

==References==
 

==External links==
 
*  

 
 
 
 
 
 
 
 
 
 