Legally Blonde
 
 
{{Infobox film
| name           = Legally Blonde
| image          = Legally blonde.jpg
| caption        = Theatrical release poster
| director       = Robert Luketic
| producer       = Ric Kidney Marc E. Platt Kirsten Smith
| based on       =  
| starring       = Reese Witherspoon Luke Wilson Selma Blair Matthew Davis Victor Garber Jennifer Coolidge
| music          = Rolfe Kent
| cinematography = Anthony B. Richmond
| editing        = Anita-Brandt Burgoyne Garth Craven Marc Platt Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $18&nbsp;million
| gross          = $141,774,679 
}} Kirsten Smith, Legally Blonde Amanda Brown.   
 law degree, along with Luke Wilson as a young attorney she meets during her studies, Matthew Davis as her ex-boyfriend, Selma Blair as his new fiancée, Victor Garber and Holland Taylor as law professors, Jennifer Coolidge as a manicurist, and Ali Larter as a fitness instructor accused of murder.
 Best Actress MTV Movie Best Female Performance.
 San Francisco Palace Theatre Broadway on West End. The large ambitious scores to both feature films were written by Rolfe Kent and were orchestrated by Tony Blondal. They featured a 90-piece orchestra and were recorded at the Sony Scoring Stage in Culver City, CA. 

==Plot==
  sorority president Elle Woods majors in fashion merchandising and is hopelessly in love with her boyfriend, Warner Huntington III, who will attend Harvard Law School the following year. She excitedly expects him to ask her to marry him, but he breaks up with her instead, saying that he has to be with someone more "serious" if he plans on a career in politics.  
 manicurist at a local salon, a fellow law student, David, and Emmett, a young attorney. Elle later helps Paulette gain custody of her dog back from her ex-husband, and she also helps her seduce the delivery man on whom she has a crush, while pretending to be a scorned ex of Davids to help him land a date.

After Vivian tricks Elle into attending a party in a Playboy Bunny costume where she retaliates by insulting her, Elle has a discussion with Warner and finally realizes he will never respect her. Now determined to succeed on her own, Elle studies hard and impresses her professors and classmates in many occasions, proving herself enough for Vivian to consider her a threat, and wins an internship with Professor Callahan, as do Warner and Vivian. They work with Callahan along with Emmett as an associate attorney, to defend Brooke Taylor-Windham, a famous fitness instructor accused of murdering her much older billionaire husband, Hayworth Windham. Brooke was once Elles fitness instructor and a member of her sorority. Elle believes Brooke is innocent, but Brookes stepdaughter, Chutney, and the household cabana boy say she is guilty, and that they saw Brooke standing over Windhams dead body, covered in his blood, while Brooke testifies that she loved her husband and only found him after he had been shot to death.

Brooke refuses to provide Callahan an alibi, but when Elle visits her in jail, Brooke admits that she had liposuction on the day of the murder. Public knowledge of this fact would ruin Brookes reputation as a fitness instructor, so Elle agrees to keep it secret and refuses to reveal the alibi to Callahan. Impressed by her integrity, Vivian starts to befriend Elle, also admitting that Warner was put on Harvards wait-list and only got in because his father pulled some strings. Elle also becomes disillusioned with Warner when he suggests she reveal Brookes alibi and break her friends trust just to further her career.

The case against Brooke begins to weaken when Elle deduces that the cabana boy is gay after he correctly identifies Elles shoe style. Callahan brushes off Elles opinion as ridiculous, but during the cross-examination, Emmett tricks him into identifying his boyfriend in court, proving that his testimony about having an affair with Brooke was a lie.

Impressed by her performance, Callahan discusses Elles future with her and then makes sexual advances on her, which Elle immediately rejects. Overhearing part of the conversation, Vivian is frustrated with Callahan choosing Elle for sexual reasons, and confronts Elle without knowing all the facts. Elle decides to leave law school, but not before she tells Emmett the truth and how Callahan was getting a bit too comfortable around her. Professor Stromwell, who once removed Elle from her class for being unprepared, helps reinvigorate Elle. Meanwhile, Emmett explains Elles encounter with Callahan to Vivian and Brooke. Brooke is enraged and Vivian realizes her mistake. Before the trial continues, Brooke dismisses Callahan and hires Elle as her new attorney with Emmett supervising.

Elle begins shakily while cross-examining Chutney, who testifies that she was home during her fathers murder, but did not hear the gunshot because she was in the shower washing her hair after getting her hair permed earlier that day. Elle gets Chutney to reconfirm her story, then reveals that washing permed hair within the first 24 hours would have deactivated the ammonium thioglycolate, and Chutneys curls are still intact. Exposed, Chutney reveals that she accidentally killed Hayworth because she thought he was Brooke, whom she hated for marrying her father because she was Chutneys age. Brooke is exonerated, and Chutney is arrested. After the trial, Warner tries to reconcile with Elle, but she rejects him, explaining that she needs a boyfriend who is less of a "bonehead" if she is to be a partner at a law firm by the time she is 30.

Two years later, Elle, who has graduated with high honors, is the class-elected speaker at the ceremony, and has been invited into one of Bostons best law firms; Vivian is now Elles best friend and has called off her engagement with Warner, who graduated without honors, no girlfriend, and no job offers; Paulette has married her delivery man and is expecting a baby girl to be named after Elle; and finally, Emmett has started his own practice, is now Elles boyfriend, and will propose to her that night.

==Cast==
* Reese Witherspoon as Elle Woods, a bubbly, intelligent and outgoing sorority girl
* Luke Wilson as Emmett Richmond, a young attorney
* Selma Blair as Vivian Kensington, a law student
* Matthew Davis as Warner Huntington III, Elles boyfriend
* Victor Garber as Professor Callahan, a Harvard law professor
* Jennifer Coolidge as Paulette Bonafonté (and later Parcelle), Elles manicurist and confidante

The cast also includes: 

* Holland Taylor as Professor Stromwell, a Harvard law professor; 
* Ali Larter as Brooke Taylor-Windham, a famous fitness instructor accused of murdering her husband; 
* Jessica Cauffiel and Alanna Ubach as Margot and Serena, fellow sorority sisters and close friends of Elle; 
* Oz Perkins as "Dorky" David Kidney, a law student; 
* Linda Cardellini as Chutney Windham, Brookes stepdaughter; 
* Raquel Welch as Mrs. Windham-Vandermark, Chutneys mother and Mr. Windhams ex-wife;  Bruce Thomas as Paulettes UPS delivery man; 
* Francesca Roberts as criminal court judge Marina R. Bickford; 
* Meredith Scott Lynn as Enid Wexler, a law student and outspoken feminist.

==Production== Napoleon and So You Think You Can Dance. 
 The Importance of Being Earnest) in that city. The real Harvard only appears briefly in certain aerial shots.

In the novel and original script, Warner and Elle attend Stanford Law School. Stanford, however, disapproved of the script, and the setting was changed to Harvard Law School.  

The producers intentionally gave Elle a different hairstyle for every scene.
 chihuahua apparently The Rainmaker, A Time to Kill. Additionally, Grishams novel The Pelican Brief features its own Professor Callahan with a penchant for inappropriate relationships with law students. The opening song and main theme, "Perfect Day", was performed by Hoku.

==Reception== 2001 Golden Golden Globe Golden Globe for Best Actress – Musical or Comedy. 

==Soundtrack==
*"Watch Me Shine" by Joanna Pacitti Fatboy Slim
*"Cant Get Me Down" by Lo-Ball
*"We Could Still Belong Together" by Lisa Loeb
*"Dont Need You To (Tell Me Im Pretty)" by Samantha Mumba
*"One Girl Revolution" by Superchick
*"Magic" by The Black Eyed Peas featuring Terry Dexter Sex Machine" by Mýa
*"Thats the Way (I Like It)" by KC and the Sunshine Band Hot Chocolate
*"Get Down on It" by Kool & the Gang Love Is Krystal
*"A Thousand Miles" by Vanessa Carlton
*"Baby, Come Over (This Is Our Night)|Baby, Come on Over" by Samantha Mumba
*"Perfect Day" by Hoku
*"Ooh La La" by Valeria

==Musical==
 
In 2007, a  , in which the winner would take over the role of Elle on Broadway. Bailey Hanks from Anderson, South Carolina, won the competition.
 West End Simon Thomas and Ben Freeman as Warner. The cast over the three-year run has also included Alex Gaumond, Denise Van Outen and Lee Mead.

==See also==
*  (2003) – the sequel to the film.
*  (2007) – the musical based on the film
*Legally Blondes (2009) – the spin-off to the film

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 