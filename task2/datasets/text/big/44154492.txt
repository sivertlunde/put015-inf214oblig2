Ashtapadi (film)
{{Infobox film
| name           = Ashtapadi
| image          =
| caption        =
| director       = Ambili
| producer       =
| writer         = Perumpadavam Sreedharan
| screenplay     = Perumpadavam Sreedharan Menaka Devan Devan Sukumari Adoor Bhasi
| music          = Vidyadharan
| cinematography = N Karthikeyan
| editing        = MV Natarajan
| studio         = Ambal Films
| distributor    = Ambal Films
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by Ambili. The film stars Menaka (actress)|Menaka, Devan (actor)|Devan, Sukumari and Adoor Bhasi in lead roles. The film had musical score by Vidyadharan.   

==Cast== Menaka
*Devan Devan
*Sukumari
*Adoor Bhasi
*Bharath Gopi
*Babu Namboothiri
*Ravi Menon

==Soundtrack==
The music was composed by Vidyadharan and lyrics was written by  and P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chandana charchitha || Kavalam Sreekumar ||  || 
|-
| 2 || Gopa kadamba nithambam || K. J. Yesudas ||  || 
|-
| 3 || Maanava hridayathin || K. J. Yesudas || P. Bhaskaran || 
|-
| 4 || Manjuthara ||  ||  || 
|-
| 5 || Pandu pandoru Kaalathu || Sujatha Mohan || P. Bhaskaran || 
|-
| 6 || Vinninte virimaaril || K. J. Yesudas || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 