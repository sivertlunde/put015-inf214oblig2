Alexander (1996 film)
{{Infobox film
| name           = Alexander
| image          = 
| image_size     =
| caption        =  Keyaar
| producer       = Meena Panchu Arunachalam
| screenplay     = Keyaar
| story          = Panchu Arunachalam
| starring       =  
| music          = Karthik Raja
| cinematography = Raja Rajan
| editing        = B. Lenin V. T. Vijayan 
| distributor    =
| studio         = P.A. Art Productions
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Tamil
}}
 1996 Tamil Tamil action film directed by Kothanda Ramaiah|Keyaar. The film features Vijayakanth and Sangita in lead roles. The film, produced by Meena Panchu Arunachalam, had musical score by Karthik Raja and was released on 10 November 1996 as a Deepavali release.    

==Plot==

Ashok brothers are three brothers who smuggle drugs and they are unstoppable. Only Alexander (Vijayakanth), a CBI officer, can arrest them.

==Cast==

*Vijayakanth as Alexander
*Sangita as Priya
*Prakash Raj as Ashok
*Srihari as Hari
*Kavita as Thayamma 
*Jeeva as Brinda
*Nizhalgal Ravi as Gandhirajan
*Vadivukkarasi
*Rajasekhar as Rajasekhar 
*Laxmi Rattan as Choudary 
*Aditya Kumar
*Bobby

==Soundtrack==

{{Infobox album |  
| Name        = Alexander
| Type        = soundtrack
| Artist      = Karthik Raja
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack |
| Length      = 23:19
| Label       = 
| Producer    = Karthik Raja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Karthik Raja. The soundtrack, released in 1996, features 5 tracks with lyrics written by Vaali (poet)|Vaali, Panchu Arunachalam and Parthi Bhaskar.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Duration
|-  1 || Alexander || 3:36
|- 2 || Koothadichu || 5:25
|- 3 || Nadhiyoram || 4:52
|- 4 || Rajarajan Nane || 4:40
|- 5 || Thaathi Thaathi || 4:44
|}

==References==
 
 

 
 
 
 