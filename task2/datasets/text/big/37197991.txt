Maniacal (film)
{{Infobox film
| name           = Maniacal
| image          = ManicalSlasher.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by Screen Entertainment
| director       = Joe Castro
| producer       = David Sterling
| writer         = Eric Spudic
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = 
| music          = Paul Mitrisin
| cinematography = Jeff Leroy
| editing        = Steven J. Escobar
| studio         = Wildcat Entertainment   Sterling Entertainment
| distributor    = Screen Entertainment
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Maniacal is a 2003 horror film directed by Joe Castro, and written by Eric Spudic.

== Plot ==

While his sister Janet is out with friends one night in 1994, the disturbed Gilbert Gill uses a hammer to wound his alcoholic father and kill his sexually abusive stepmother before being arrested by Officer Spiegel. Gilbert is placed in the Hitchberg Sanitarium, where he is tormented by hallucinations of his stepmother.

Three years later, on the day he is scheduled to visit his father and sister at home, Gilbert escapes the sanitarium, murdering several employees and patients on his way out. Hijacking a pickup truck, Gilbert breaks into his old house, creates makeshift weapons out of some assorted objects and goes to a horror shop, where he steals a clown mask and slits the throat of the clerk.

While Mr. Gill and Spiegel search for Gilbert, Janet, Spiegels daughter D.J. and their friend Brooke have a slumber party and horror film marathon at the Spiegel house, being joined by D.J. and Brookes boyfriends, and their friend Lance. When D.J., Brooke and their respective boyfriends go to the bedrooms to have sex they are murdered by Gilbert, who then goes after Janet and Lance. When Mr. Gill and Spiegel arrive, Gilbert wounds his father, blows Spiegels head off with his shotgun, and crushes Lances skull.

As Gilbert begins strangling Janet, she starts rambling about he is going to be punished and go to Hell, and that he must ask God for forgiveness. Gilbert suffers a psychotic break, drinks cleaning chemicals he finds in the Spiegels washroom, and dies in his sisters arms.

== Cast ==

* Perrine Moore as Janet Gill
* Lee Webb as Gilbert Gill
* Carl Darchuk as Garrett Gill
* Brannon Gould as Lance
* Heather Chase as D.J. Spiegel
* Jon Prutow as Josh
* David Ortega as Dane
* Carol Rose Carver as Brooke
* Deborah Huber as Nancy Gill
* Michael Robert Nyman as Officer Spiegel
* Darlene Tygrett as Nurse #1
* Vida Ghaffari as Nurse #2
* Beth Friedlaender as Sissy
* Otto Sturcke as Marco
* David Scalzetto as Eddie Brandt
* Mary Ann Springer as Kid in Park

== Reception ==

The acting, plot, and effects were all criticized by The Worldwide Celluloid Massacre, which categorized the film as "Worthless".  Cold Fushion Video Reviews found Maniacal derivative, weak, and riddled with plot holes.  Another scathing critique was given by Eat Horror, which wrote that the film "stands out as one of the worst slashers Ive ever seen". 

Monsters at Play also stated the plot was clichéd, but commended the gore effects, editing, and attractiveness of the female cast.  A score of two out of four was given by The Video Graveyard, which said the film was "nothing special, but watchable" despite the lame dialogue, occasionally dodgy gore, and weak third act. 

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 