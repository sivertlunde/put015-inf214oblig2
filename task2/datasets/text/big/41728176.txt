Heart of the Wilds
 
{{infobox film
| title          = Heart of the Wilds
| image          =
| imagesize      =
| caption        =
| director       = Marshall Neilan
| producer       = Adolph Zukor Jesse Lasky
| writer         = Charles Maigne(scenario)
| based on       =  
| starring       = Elsie Ferguson
| music          =
| cinematography = Walter Stradling
| editing        =
| distributor    = Artcraft Pictures
| released       =   (or Sept. 1 1918) reels
| country        = USA
| language       = Silent film English intertitles

}} lost  1918 silent film drama directed by Marshall Neilan and starring Elsie Ferguson. The story is from Gilbert Parker which Edgar Selwyn based his play Pierre of the Plains on. Ferguson had become a star in 1908 in Selwyns Broadway play.  

==Cast==
*Elsie Ferguson - Jen Galbraith
*Thomas Meighan - Sergeant Tom Gellatly
*Joseph W. Smiley - Peter Galbraith (*as Joseph Smiley) Matt Moore - Val Galbraith
*E. L. Fernandez - Pierre (billed Escamillo Fernandez)
*Sidney DAlbrook - Grey Cloud

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 

 