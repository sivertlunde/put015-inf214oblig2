Spanish Teen Rally
{{Infobox film
| name = Spanish Teen Rally (English)   (Spanish)
| image =
| image_size = 
| alt = 
| caption = 
| director = Amparo Fortuny
| producer = Mikel Iribarren Amparo Fortuny
| writer = Amparo Fortuny
| narrator =
| music =  (Ley y moral) Carl Davis (Intolerance) Joseph Carl Breil (Intolerance)
| cinematography = Carlos Beltrán Lázaro
| editing = Mikel Iribarren
| studio = Mikel Iribarren P.C.
| distributor =
| released = 2014
| runtime = 53 minutes
| country = Spain Spanish Valencian
| budget = 
| gross =
}}
Spanish Teen Rally is a 2014 Spanish documentary film directed by Amparo Fortuny.  The film delves into the world of a group of teenagers whose protests in defense of Public Education triggered the so-called Valencian Spring (political terminology)|Spring.     It was the very first experience of protest for many of them and lead to their crash with reality.   Not only did these teenagers become the symbol of the student protests throughout Spain;  they also became a reflection of the youths disenchantment facing an uncertain future in a country where the austerity policies are beginning to seriously affect society.

In the documentary we find testimonials from students from 13 to 19 years of age with shocking images of the demonstrations together with foregrounds of the young protagonists enduring police charges and the destruction of children icons (dolls, balloons, cakes and confetti, etc..)

==Starting point== demonstrations to protest against the cutbacks in the educational budgets of the Valencian Autonomous Community. The police’s performance in those demonstrations was extremely controversial and appeared in many international media.    This sparked the interest of Parents Associations and both Student and International Organisations, such as Amnesty International  and Save the Children.  

==Music==
The soundtrack is formed by the track "Law and Moral" of the band from Murcia Klaus & Kinski and a small piece of Joseph Carl Breil’s composition for the 1916 film Intolerance (film)|Intolerance by D. W. Griffith.

"Law and Moral" is the leit motif of the documentary.  The video clip repeatedly conveys, from a light hearted point of view, the attitude of the main characters in the film.  This attitude on occasions is energetic and enthusiastic, and nihilist and disenchanted on others.
 battle and fall of Babylonia (minute 90’). The film alternatively describes stories of injustice. Each of these stories, from different ages and cultures, share the themes of inhumanity, intolerance, hypocrisy, persecution, discrimination and injustice that different social, religious and political systems have reached throughout time. 

Intolerance was restored in 1989 by Thames Television for Channel 4. The version that appears in Spanish Teen Rally is the digital recording by the Symphonic Radio Orchestra of Luxembourg conducted by Carl Davis.  

==References==
 

==External links==
 
*  
* Official Website  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 