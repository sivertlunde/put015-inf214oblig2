Salt on Our Skin
{{Infobox film
| name           = Salt on Our Skin
| image          = Salt on Our Skin.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Andrew Birkin
| producer       = Bernd Eichinger   Martin Moszowicz 
| writer         = 
| screenplay     = Andrew Birkin  Bee Gilbert
| story          = 
| based on       =  
| starring       = Greta Scacchi  Vincent DOnofrio  
| music          = Klaus Doldinger	
| cinematography = Dietrich Lohmann	 	
| editing        = Dagmar Hirtz	 	
| studio         = 
| distributor    = Warner Bros. Pictures International
| released       = 17 September 1992 	
| runtime        = 110 minutes
| country        = Germany   France  Canada
| language       = English
| budget         = 
| gross          = 
}}
 Salt on Our Skin is a 1992 romantic-drama film, directed by Andrew Birkin, starring Greta Scacchi and Vincent DOnofrio. The film is based on Benoîte Groult’s novel Les vaisseaux du coeur published in 1988. The plot follows the passionate relationship between mismatched lovers, a Parisian intellectual woman and a Scottish fisherman, brought together by lust. Their passionate encounters over 30 years make both their lives worth living. Scacchi and DOnofrio were a real life couple at that time.

==Plot==
On the death of her lover, George McEwan, a half French/half Scottish woman, recounts in flashback her passionate relationship with Gavin McCall, a humble fisherman. Their romance goes back in time more than thirty years.

In the 1950s, George, then a vivacious young woman living in France, comes to Scotland to spend her long summer vacation with her younger sister, Frédérique. While helping the locals take in the straw, she meets an attractive and simple man Gavin, whose sister, Mary, becomes one of her friends. Smitten with each other, George and Gavin begin a torrid affair going for a moonlit swim. As the summer ends, George and her sister return to attend school in Paris where they live with their parents. Mary asks the two sisters to be her bridesmaids. Back in Scotland for Mary’s wedding, George and Gavin continue their romance escaping the wedding celebration on a cave at the seashore.

Months later, back in France, George is surprised by Gavin’s visit. She shows him Paris while they continue their love affair. He asks her to marry him but she refuses. George wants to study at the Sorbonne and pursue her intellectual goals. She does not see herself as the wife of a fisherman in a Scottish Village. Realizing that, for her, the cultural and life style differences between them are too great to overcome, she refuses his marriage proposal. Their lives take separate ways and they  stop seeing each other. George becomes an intellectual and feminist activist. She marries and has a son.
Ten years later, George, now divorced, moves to Quebec pursuing an academic career teaching and writing a book on womens studies. Her second marriage to Sidney, an intellectual, proves to be dry and ends in divorce.

Georges friend, Ellen, a fellow feminist writer, invites her to accompany her for a summer trip to England. On a street in London, she comes across Gavin once again. He still resents her refusal to marry him a decade ago. She would like to rekindle their romance. Gavin, hurt and still in love with her, initially refuses. Soon, however, he has a changed of heart and accepts to go with George to a romantic vacation in Saint Thomas, U.S. Virgin Islands. From then on, they begin to meet each other with some frequency through the years, loving each other despite their differences. Scotland, Montreal, and the St. Bernard de Clairvaux Church in Florida, served as the background for their encounters.

George and Gavin’s romance ends abruptly with his death. At the funeral, Josie, Gavin’s widow, gives George a letter Gavin left to her. It reads: Before you came in to my life, I believed that each day resemble the other. And they will continue this way until I died. Since you, and don’t ask me to explain, I only know that I want you in my arms from time to time if you wanted too. A thought that you exist somewhere and that you think of me sometime helps me to live...

==Cast==
*Greta Scacchi as George McEwan
*Vincent DOnofrio as Gavin McCall
*Shirley Henderson as Mary McCall
*Anaïs Jeanneret as Frédérique McEwan
*Petra Berndt as Jodie
*Claudine Auger as Mrs. McEwan
*Laszlo I. Kish as Angus
*Barbara Jones as Ellen
*Rolf Illig as Mr. McEwan
*Hanns Zischler as Sidney
*Barbara Jones as Ellen
*Charles Berling as Roger
*Sandra Voe as Mrs. McCall
*Philip Pretten as Al

==Reception==
The film was shot in Montreal, Scotland,  Saint Thomas, U.S. Virgin Islands, Paris and at St. Bernard de Clairvaux Church in North Miami Beach.  It premiered in Germany on 17 September 1992. In the United States it was released under the title Desire. The film was a commercial disappointment. It was also ill received by film critics who single out the credibility and motivations of the main characters.  Allon & Patterson,  Contemporary British and Irish Film Directors: A Wallflower Critical Guide, p. 30 

==DVD release==
Salt on Our Skin is available in Region 2 DVD.

==References==
;Notes
 

;Bibliography
*Allon, Yoram and Patterson, Hannah   Contemporary British and Irish Film Directors: A Wallflower Critical Guide. Wallflower Pres, 2002. ISBN 1903364213

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 