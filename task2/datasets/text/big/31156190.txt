Inqulab Zindabbad
{{Infobox film 
| name           = Inqulab Zindabbad
| image          = 
| caption        =
| director       = KS Sethumadhavan
| producer       = KSR Moorthy
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan Sathyan Madhu Madhu Sheela Jayabharathi
| music          = G. Devarajan
| cinematography = Melli Irani
| editing        = TR Sreenivasalu
| studio         = Chithranjali
| distributor    = Chithranjali
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by KS Sethumadhavan and produced by KSR Moorthy. The film stars Sathyan (actor)|Sathyan, Madhu (actor)|Madhu, Sheela and Jayabharathi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  Sathyan as Venugopalan Madhu as Shreedharan
*Sheela as Rajamma
*Jayabharathi as Vasanthi
*Adoor Bhasi as Panjali Raman Nair Prema as Devaki 
*Sankaradi as Neerkunnam Neelaambaran
*Shobha as As Child artist
*Varghese GK Pillai as Circle Inspector
*Janardanan
*Kuttan Pillai
*Menon
*N. Govindankutty as District Secretary
*Nambiar
*Nellikode Bhaskaran as Govindan
*PO Thomas
*Panicker
*Panjabi
*Paravoor Bharathan as Head Constable Mathan
*Philomina as Narayani     
*Roja Ramani
*Susheela
*Vasu
*Veeran as Adv. Cherian
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by OV Usha and Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aarude Manassile || P. Leela || OV Usha || 
|-
| 2 || Alakadalil Kidannoru || P. Madhuri, KP Brahmanandan || Vayalar Ramavarma || 
|-
| 3 || Inquilab Zindabad || P Jayachandran, Chorus || Vayalar Ramavarma || 
|-
| 4 || Pushyaraaga Mothiramittoru || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 