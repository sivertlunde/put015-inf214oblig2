100,000 Pounds
{{Infobox film
| name           = Ekato hiliades lires 100,000 Pounds Εκατό χιλιάδες λίρες
| image          =
| caption        =
| director       = Alekos Leivaditis
| producer       =
| writer         = Nikos Tsiforos
| starring       = Mimis Fotopoulos Dinos Iliopoulos Kaiti Panou Alekos Leivaditis Giorgos Damasiotis Tzeny Stavropoulou Giannis Ioannidis Andreas Mitakis Soula Emanouil Filos Filippidis
| cinematography =
| photographer   =
| music          = Menelaos Theofanidis
| editing        =
| distributor    = Mer Film
| released       =  
| runtime        = 79 minutes
| country        = Greece
| language       = Greek
}}
 Greek film directed by Alekos Leivaditis and starring Mimis Fotopoulos, Dinos Iliopoulos and Kaiti Panou. The film was produced by Mer Film. The film is 79 minutes and sold 35,784 tickets.

The film was directed by Alekos Leivadiris (a famous actor and a director in theatre and film) with operator (photographer) Prodromos Meravidis and writer Nikos Tsiforos. The music was made by Menelaos Theofanidis.

Before the movie was released to the screen, it had two titles: 100,000 Pounds and Gabri me  . Later on, it kept the first one.

==Cast==
*Mimis Fotopoulos
*Dinos Iliopoulos
*Kaiti Panou
*Alekos Leivaditis
*Giorgos Damasiotis
*Tzeny Stavropoulou
*Giannis Ioannidis
*Andreas Mitakis
*Soula Emanouil
*Filios Filippidis

==External links==
* 

 
 
 
 
 