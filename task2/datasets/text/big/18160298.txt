The Ballet of Change: Piccadilly Circus
 

  was the first movie to be shown on the Coca-Cola billboard in November 2007]]
 Piccadilly Circus Lights in London. On 23 November 2007, as part of an event, that included screenings of films onto St. Martin in the Fields Church, Trafalgar Square and the façade of the Odeon Leicester Square, The Ballet of Change: Piccadilly Circus aired at 19:30 on the Coca-Cola billboard. Coca-Cola have had a sign at Piccadilly Circus since 1955 with the current version from September 2003, when the previous digital projector board and the site formerly occupied by Nescafé was replaced with a state-of-the-art LED video display that curves round with the building.

==The Ballet of Change: Piccadilly Circus==
On the evening of the event, those wishing to watch the film with the accompanying music downloaded it from www.theballetofchange.co.uk and brought it to the respective film sites on MP3 players.
 Science Museum, Transport Museum as well as the BBC archive.

The event and films were made by not-for-profit television production company   with the involvement of 20 students from Bexley Business Academy and Produced and Directed by Paul Atherton and funded by the Heritage Lottery Fund.

==Credits==

*Producer and Director:  Paul Atherton
*Editor:                  
*Composer:               Damian Coldwell
*Senior Researcher:      Emily Thomas
*Stills Photographer:    Chloé Flexman

==Websites==

#	http://www.theballetofchange.co.uk
#	http://www.simpletvproductions.co.uk/Q&D%20In%20Production.html
#	http://www.hlf.org.uk/grantsdatabasecrawl/crawl2?projectID=22131
#	http://www.youtube.com/watch?v=TvHXFB4K4No
#       http://www.pablopost.co.uk/Ballet_of_change.html

==External links==
*  
*  
*  
*  
#	http://www.bignewsday.com/story.asp?code=RN2144075l&news=historic_london_landmarks_to_screen_film
#	http://www.lifestyleextra.com/ShowStory.asp?story=Kl2144075C&news_headline=historic_london_landmarks_to_screen_films
#	http://www.pressbox.co.uk/detailed/Arts/Ballet_of_Change_An_Historical_Film_Event_156541.html
#	http://www.timeout.com/london/aroundtown/events/621671/ballet_of_change.html
#	http://www.broadcastnow.co.uk/opinion_and_blogs/letters/broadcast_letters__november_9.html

 
 
 
 
 
 