The Colonel (1974 film)
{{Infobox Film
| name           = The Colonel
| image          = The Colonel DVD.jpg
| caption        = The DVD cover.
| director       = Chatrichalerm Yukol
| producer       = 
| writer         = 
| narrator       = 
| starring       = Sompop Benjatikul Naiyana Chaiwanan
| music          = 
| cinematography = 
| editing        = 
| distributor    = Mangpong 
| released       = 1974 Richardson, Thomas. 1993  , Cornell University; retrieved from Internet Archive 2008-01-27  Richardson, Thomas. 1993  , Cornell University; retrieved via Internet Archive 2008-01-27 
| runtime        =  Thailand
| Thai
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1974 Cinema Thai action film|action-thriller film directed by Chatrichalerm Yukol.  

==Plot==
 Thai intelligence officer who must assume the identity of a slain lieutenant colonel in a neighboring countrys military who was working as a double agent.

==Cast==
* Sompop Benjatikul as The Colonel
* Naiyana Chaiwanan as Lamduan

==DVD==
The film has been released in Thailand on a PAL-encoded DVD region codes|all-region DVD with English subtitles by Mangpong.

==References==
 

 
 
 
 
 
 


 
 