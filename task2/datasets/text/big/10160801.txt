The Man with the Rubber Head
{{Infobox film
| name = Lhomme à la tête de caoutchouc
| image = Lhommealatêtet.jpg
| image size =
| caption =
| director = Georges Méliès
| producer =
| writer = Georges Méliès
| narrator =
| starring =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 3 minutes
| country = France
| language = Silent film
| budget =
}} 1901 silent silent France|French fantasy film directed by Georges Méliès. It was filmed in 1901 and released in 1901.

It was released by Mélièss Star Film Company and is numbered 382–383 in its catalogues, where it was advertised as a grande nouveauté. 

==Synopsis==
A chemist in his laboratory places upon a table his own head, alive; then fixing upon his head a rubber tube with a pair of bellows, he begins to blow with all his might. Immediately the head increases in size and continues to enlarge until it becomes truly colossal while making faces. The chemist, fearing to burst it, opens a cock in the tube. The head immediately contracts and resumes its original size. He then calls his assistant and informs him of his discovery. The assistant, wishing to experiment for himself, seizes the bellows and blows into the head with all his might. The head swells until it bursts with a crash, knocking over the two experimenters. The chemist then literally kicks his assistant from the lab in anger.

==Overview==
The film was made after A Trip to the Moon. To create the illusion of an expanding head, Melies "zoomed" in on his own head with a camera and superimposed this onto the film. He received the idea from Albert A. Hopkins Magic - Stage Illusions and Scientific Diversions. 

==References==
 

==External links==
* 
*  
*  
*   from the Cinémathèque française

 

 
 
 
 
 
 
 
 
 


 
 