Carolina (1934 film)
{{Infobox film
| name           = Carolina
| image          =
| image_size     =
| caption        = Henry King
| producer       = Reginald Berkeley Play: Paul Green Robert Young Stepin Fetchit  Shirley Temple
| music          = Louis De Francesco
| cinematography = Hal Mohr
| editing        = Robert Bassler
| distributor    = Fox Film Corporation
| released       =   
| runtime        = 85 minutes
| country        = United States
| language       = English
}}
Carolina is a 1934 American romantic comedy film directed by Henry King.  The screenplay by Reginald Berkley is based on the play, The House of Connelly by Paul Green.   The film stars Janet Gaynor, Lionel Barrymore, and Robert Young with a supporting cast featuring Stepin Fetchit and Shirley Temple in a romanticized story about a post-Civil War family in the fading South regaining its former prestige.

==Cast==
*Janet Gaynor as Joanna Tate
*Lionel Barrymore as Bob Connelly Robert Young as Will Cromwell
*Henrietta Crosman as Mrs. Ellen Connelly Richard Cromwell as Alan
*Mona Barrie as Virginia Buchanan
*Stepin Fetchit as Scipio
*Ronnie Cosby as Harry Tate
*Jackie Cosbey as Jackie Tate
*Almeda Fowler as Geraldine Connelly
*Alden "Stephen" Chase as Jack Hampton
*Shirley Temple as Joan Connelly (uncredited)

==Production==
Shirley Temple was too young to read so her mother taught her the lines by reading them to her.  In this way, the child actress learned every line in the script.  In one scene when Lionel Barrymore forgot his lines Shirley infuriated him by prompting him.  Mrs. Temple hoped the film would be her daughters big break but it was not to be.  Most of Shirleys scenes and all her dialogue were cut except for a few brief moments with Gaynor and Young at films end.  Her name did not appear in the credits (Edwards 45-6).

==See also==
*Lionel Barrymore filmography

==References==
*  

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 