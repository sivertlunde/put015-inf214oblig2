A Song of Kentucky
{{infobox film
| name           = A Song of Kentucky
| image          =
| imagesize      =
| caption        =
| director       = Lewis Seiler William Fox Chandler Sprague(associate producer)
| writer         = Sidney D. Mitchell(story) Archie Gottler(story) Con Conrad(story) Frederick Hazlitt Brennan(scenario & dialogue)
| starring       = Lois Moran Dorothy Burgess
| cinematography = Charles G. Clarke
| editing        = Carl Carruth
| distributor    = Fox Film Corporation
| released       = November 10, 1929
| runtime        = 79 minutes
| country        = United States
| language       = English
}} lost 1929 romantic drama film produced and distributed by the Fox Film Corporation. It is an early sound film with full dialogue. It was directed by Lewis Seiler and starred Lois Moran and Dorothy Burgess.   

==Cast==
*Lois Moran - Lee Coleman
*Joseph Wagstaff - Jerry Reavis
*Dorothy Burgess - Nancy Morgan
*Douglas Gilmore - Kane Pitcairn
*Herman Bing - Jake Kleinschmidt
*Hedda Hopper - Mrs. Coleman
*Edwards Davis - Mr. Coleman
*Bert Woodruff - Steve

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 


 