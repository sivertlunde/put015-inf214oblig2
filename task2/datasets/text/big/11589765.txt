In the Name of the Law (1949 film)
{{Infobox film
|image=In the Name of the Law (1949 film).jpg
|name=In the Name of Law
|director=Pietro Germi
|writer=Pietro Germi Federico Fellini Giuseppe Mangione Aldo Bizzarri Tullio Pinelli
|producer=Luigi Rovere
|starring=Massimo Girotti Saro Urzì Jone Solinas Lando Buzzanca 
|music=Carlo Rustichelli
|cinematography=Leonida Barboni 
|distributor=Lux Film
|released=November 22, 1950 (Italian release)
|runtime=100 min Italian
}}

In the Name of the Law (or In nome della legge) is a 1949 Italian language mafia drama film directed by Pietro Germi. It Is based on Giuseppe Guido Lo Schiavos   novel Piccola pretura.  Federico Fellini co-wrote the script. The style of the film is close to Italian neorealism film movement.

==Cast==
*Massimo Girotti - Il pretore Guido Schiavi
*Jone Salinas  - La baronessa Teresa Lo Vasto
*Camillo Mastrocinque - Il barone Lo Vasto
*Charles Vanel - Massaro Turi Passalacqua
*Saro Urzì - Il maresciallo Grifò
*Turi Pandolfini - Don Fifì
*Umberto Spadaro - Lavvocato Faraglia
*Saro Arcidiacono - Il cancelliere
*Ignazio Balsamo - Francesco Messana
*Nanda De Santis - Lorenzina La Scaniota
*Nadia Niver - Bastianedda
*Aldo Sguazzini	
*Alfio Macrì - Il sindaco Leopoldo Pappalardo
*Carmelo Olivero - Don Peppino

==Awards==
The film won 3  Nastro dArgento: Best Actor (Massimo Girotti), Best Supporting Actor (Saro Urzì) and a special award for Pietro Germi.

==External links==
* 

 

 
 
 
 
 
 
 

 
 