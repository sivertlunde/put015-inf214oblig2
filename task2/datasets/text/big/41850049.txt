Ouija (2014 film)
 
 
{{Infobox film
| name           = Ouija
| image          = Ouija 2014 poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Stiles White
| producer       = {{Plain list |
* Jason Blum
* Michael Bay
* Andrew Form
* Bradley Fuller
* Bennett Schneir}}
| writer         = {{Plain list |
* Juliet Snowden
* Stiles White
}}
| starring       = {{Plain list |
* Olivia Cooke
* Ana Coto
* Daren Kagasoff Douglas Smith
* Bianca A. Santos
}}
| music          = Anton Sanko
| cinematography = David Emmerichs
| editing        = Ken Blackwell
| studio         = {{Plain list |
* Platinum Dunes
* Hasbro
* Blumhouse Productions
}} Universal Pictures
| released       =  
| runtime        = 89 minutes  
| country        = United States
| language       = English
| budget         = $5 million    
| gross          = $101,456,010 
}} supernatural horror Douglas Smith, and Bianca A. Santos, Ouija was released on October 24, 2014.  It is the first Hasbro property adaptation produced by Platinum Dunes and Blumhouse Productions, as well as Hasbros first horror film. Despite receiving negative reviews, the film was a box office success, grossing over $101 million against its $5 million budget, also grossing twenty  times its budget.
 sequel is set for release on October 21, 2016.

==Plot==
 
The film begins with a flashback, where we see two young girls, Laine Morris and Debbie Galardi, playing with a Ouija board in Laines room. Debbie tells Laine the rules of the Ouija board. They proceed by saying "as friends weve gathered, hearts are true. Spirits near we call to you". Debbie also tells Laine that if you look through the magnifying glass in the middle of the planchette, you can see any spirits in the room, as it is the "eye to the other side". The two girls are interrupted by Laines sister, Sarah. Debbie notices Laine seems frightened, and she assures her "its only a game".

The story moves to present day, where Debbie is playing with the board by herself in front of her fireplace. She moves the planchette to goodbye, and she burns the board. Laine calls Debbie and asks her to come outside so they can go to a school sporting event together. Debbie explains to Laine that she found a Ouija board in her house, that she played alone and weird things have been happening and decides that she isnt going to go to the game. Laine offers to stay, but Debbie insists that she go saying they will meet for breakfast. A door opens on its own and the gas stove burner turns on by itself, but Debbie brushes both off and goes upstairs. She is shocked when she finds the board on her bed, in perfect condition. She steps on the planchette, which has mysteriously appeared by her feet. She picks it up and looks through it, and her eyes suddenly turn a foggy white color. She tears the Christmas light decoration off her wall, and hangs herself over the banister.

The following morning, Laine is with her boyfriend Trevor at a diner, where their friend Isabelle also works. While chatting, Laine receives a text message from her father asking her to come home, and when she arrives at her house she is informed of Debbies apparent suicide. A viewing is held at Debbies home, where Debbies friends mourn her death. They wonder if there was anything they could have done to stop it from happening. Laine wanders upstairs to Debbies room, where she is greeted by Debbies mom. Debbies mom gives Laine a box full of some of Debbies possessions, including a video camera. She also thanks Laine for agreeing to watch over the house while they are gone for an unknown amount of time. After the viewing, Laine is back at home and watches some of the videos of her and Debbie on the camera. Her sister, Sarah, is about to sneak out of the house, and the two sisters get into an argument about Laine trying to act like their mom, since she hasnt been around often. The next day, Laine and Sarahs father leaves for a business trip and tells them that they can call if they need him, or to go to Nona, their housekeeper. 

The next day, Laine and Trevor go to Debbies house to water the plants and check the pool cover. Laine hears a noise upstairs and goes to investigate. She finds Debbies Ouija board in the closet, and she tells Trevor about how she and Debbie would play it and answer each others questions about their crushes, etc. when they were little. At school the next day, Laine asks Debbies boyfriend Pete if he noticed anything strange about Debbie before she killed herself. He tells Laine that she didnt talk to him as often, and wouldnt invite him inside her house, the same way Debbie didnt let Laine into her home the night before her suicide. The next day at the diner, Laine is explaining to Trevor and Isabelle that she feels as though Debbie is still "here" in her home. She wants to use the Ouija board to contact Debbie to say goodbye, and wants her friends to participate in holding the seance. Trevor and Iz agree to do so.

The night the seance is to be held, Laine catches Sarah trying to sneak out again. She forces Sarah to come along with her since she cannot trust her alone. Laine decides to hold the seance at Debbies house, since it is where she died. It is apparent that Laines friends and her sister are hesitant to play the game, but they know its what is right for Laine. While Laine is upstairs getting the Ouija board, Trevor, Iz, and Sarah are turning on the lights in the home. They are frightened to see Pete has come through the back door with his spare key. 

The teens decide to play in the dining room. Before beginning the seance, Laine places a photo of Debbie under the corner of the Ouija board. She begins to ask questions. At first, the planchette doesnt move and everyone dismisses the seance as a joke, and the group accuses each other of moving the planchette. Laine asks if anyone is present, and the game piece moves on the board to "yes". She then asks if the spirit has something to tell them, and it spells out "Hi friend". Laine asks who they are talking to, and the planchette moves to "D". She believes its Debbie. Isabelle is frightened, and momentarily takes her hands off the board. Laine asks her to put her fingers back on the planchette. There is a noise upstairs, and Trevor reassures everyone that they are in an old house. Laine tells Debbie that they just wanted a chance to say goodbye, and the planchette moves to "goodbye". The lights go out. Laine immediately gets up to investigate, and Trevor confirms the power is out. In the kitchen, the gas stove top burner is on by itself, much like the night Debbie died. Laine tells Trevor that she believes Debbie wants to tell them something. Back in the dining room, Pete, Iz, and Sarah ask each other if they were moving the planchette and they all say no. Pete suggests Laine was moving it herself, as a way of giving herself closure. He then hears a sound and goes to investigate. He notices the chandelier in the entryway is moving back and forth, and he sees the reflection of a woman in a mirror propped against the wall. Something then pushes him into the mirror, cutting his hand. The friends decide to leave, having said goodbye successfully. A few days pass and Nona discovers the Ouija board in Laines room and warns her of the dangers of attempting to contact the dead, and asks her to never use the board again. Laine agrees. 

Strange events begin to occur among the friends. Trevor is riding his bike through a tunnel, when he steps off his bike to walk it. He hears strange noises, and discovers "Hi friend" written on the wall of the tunnel after a piece of chalk rolls toward him. Isabelle is leaving work when she notices the words "Hi friend" written on the inside of her car, where a hand from inside the vehicle smudges the letters. Pete finds the words carved into his desk at home. Laine and Sarah are at home when Laine notices the front door is open. She calls out to whoever is there, and the door slams shut. Sarah and Laine hide in Laines closet for a few minutes, and Laine discovers that someone typed "Hi friend" on her laptop.
 
The friends decide that Debbie is trying to tell them something, and decide to hold a seance again at her house. They call out to Debbie and tell her they got her signs, and ask for a sign that she herself is present. A chair is pulled out from the end of the table on its own. The planchette flies off the board, but the group continues the seance. Laine asks if Debbie actually killed herself and the board answers "no". She then asks if someone hurt her and the board says "yes". The responses briefly stop, and Pete decides to ask the board if Debbie remembers their first date at "the lookout". The board replies "yes", to which Pete is unnerved and realizes they arent talking to Debbie, as he never took her to the lookout. Laine asks if they are actually talking to Debbie, and the board answers "no". Pete asks who they are talking to and the board spells out "DZ".  Laine asks if they were talking to DZ the other night and the board says "yes". Sarah asks if they were ever talking to Debbie, to which the planchette moves to "no". Trevor tells Laine they can stop playing. Laine asks if Debbie made contact with DZ and the board says "yes". Laine asks where DZ is, and Sarah suggests the chair. Laine looks through the planchette and sees no one in the chair, then turns towards Sarah and sees a young girl with her mouth stitched closed. Laine throws the planchette down and it begins to spell on its own. It spells out "run", then "shes coming". They ask who is coming, to which DZ replies "mother". Laine looks through the planchette to see DZ pointing towards the stairs, and Laine looks over to see an apparition of a woman gliding towards her screaming. Laine throws the planchette down again, and the board flies up into the air. The group decides to stop playing the game. The same night, Laine discovers trough Debbies digital camera videos that Debbie found the board in her attic after cleaning it. She recorded herself playing the game alone, and came in contact with DZ.

Isabelle arrives at home from work and its preparing herself a bath. While flossing, her mouth is stitched shut by the floss, and her eyes turn white. She levitates off the ground and up into the air, and is then dropped, hitting her head hard on the sink, killing her. At school the next day, a counselor tells Laine that she has people to talk to while she is grieving over the loss of two friends. Laine tells him that he has no idea what he is talking about. She confronts Trevor and Pete in the hall, and Trevor is upset that Laine made them play the game. He tells her that DZ is watching everything they do, that "its coming for all of us. So whos next?" Laine persuades Pete to help her figure out exactly what is going on.  The two go to Debbies house and Laine goes into the attic to see if she can find anything. While looking around, Pete hears noises in the house. Laine finds a box full of old vintage photos, as well as an old doll. She calls Pete over to take the box that she found, and the flashlight she was using moves on its own, pointing towards the other end of the attic. A figure appears, startling Laine. 

Going through the box of photos, Laine and Pete learn that Debbies home holds a dark secret: a young girl named Doris Zander went missing from the home, and shortly thereafter her sister, Paulina, killed their mother. The two discover that Doris sister is still alive in a mental institution, and Laine visits her, claiming to be her niece. Laine informs Paulina that she found photos of her and her sister Doris, and that she believes Doris is still alive in Debbies house. Paulina explains to Debbie that her mother practiced mediumship, and that Doris was very interested in her mother’s practices. Paulinas mother used Doris as a vessel for the spirits she came in contact with to use to speak. Over time, Paulinas mother began to go insane, and the voices of the spirits inside of Doris became too much for her to handle. She killed Doris and sewed her mouth shut. Paulina then killed her mother. Paulina asks how Laine was able to see Doris, to which she suspects a spirit board. She informs Laine that she played in a graveyard, because Doris body is within the home. Paulina tells Laine that, while she can only see the spirits through the planchette for now, the energy is growing stronger. There are two points of connection between Laine and her friends with the spirits, the body of Doris and the Ouija board itself. Paulina informs Laine that the connection is hard to break, but it can be done by locating Doris body in Debbies basement in a "secret room" made by Paulinas mother, and cutting the stitches on Doris mouth. But this wont be able to be done without a fight from Mother.

Laine, Sarah, Trevor, and Pete all return to Debbies home and go into the cellar. While looking for the secret room, Laine notices Mothers shadow on the wall. Mother lashes out towards Trevor and drags him into a room, where Pete follows in attempt to rescue him. The door to the room is slammed shut, and will not open. Pete and Trevor urge Laine to continue to try and find the body. Laine finds the entrance to the room, and successfully manages to cut the stitches on Doris mouth. Mothers spirit appears, exclaiming "no", but Doris spirit also manifests and screams at Mother, unleashing the voices of the spirits trapped within her. She is able to overcome Mother, whose spirit bursts into ashes. The group believes that everything has been resolved, but Doris later kills Pete at his home after disguising herself as Debbie briefly. 

Laine confronts Paulina at the mental institution and learns that Doris and Paulina were possessed and that Mother was trying to defend herself against them. Mother was not the evil one. Laine is horrified, and turns to Nona for answers. Nona explains that the only way to stop the ghost is to burn Doriss body with the Ouija board, and to hurry before Doris grows too powerful. Trevor arrives at Debbies house before Laine and Sarah, and he is drowned by Doris. When Laine and Sarah enter Debbies home, they find what appears to be Trevor, standing in the living room, soaking wet. He then explodes, splashing water everywhere. Laine and Sarah have to continue moving forward. In the cellar, Laine instructs Sarah to go and get Doris body and starts a fire in the furnace. Sarah goes into the secret room, but Doris spirit intervenes, attempting to sew Sarahs mouth shut. Laine begins to play a game alone, insisting that Doris come and play, to which Doris must. Doris grabs Laines arm and begins to twist it around, and Laines eyes turn white. Just before anything worse can happen, Debbies spirit appears and places her hand on the planchette, and Laines eyes clear up. Sarah and Laine succeed in burning Doriss corpse and the Ouija board, and Doris is defeated. 

The sisters return home, happy that everything is over. Laine takes a shower and flosses, then returns to her room, only to find the planchette on her desk. She picks it up and lifts it to her face and looks through the magnifying glass, and the film ends.

==Cast==
 
* Olivia Cooke as Laine Morris 
** Afra Sophia Tully as young Laine
* Ana Coto as Sarah Morris 
** Izzie Galanti as young Sarah
* Daren Kagasoff as Trevor  Douglas Smith as Pete 
* Bianca A. Santos as Isabelle 
* Matthew Settle as Anthony Morris 
* Lin Shaye as Paulina Zander
* Shelley Hennig as Debbie Galardi 
** Claire Beale as young Debbie
* Vivis Colombetti as Nona 
* Robyn Lively as Mrs. Galardi
* Sierra Heuermann as Doris Zander
** Sunny May Allison as young Doris
* Claudia Katz (ll) as Mother
 

==Production==
===Development===
On May 28, 2008, The Hollywood Reporter reported that Platinum Dunes Michael Bay, Andrew Form, and Bradley Fuller are producing a film based on Hasbros supernatural board game Ouija, with Universal Studios also attached.  On February 2, 2009, Fuller stated that the film would be big like Pirates of the Caribbean. 
 Scott Stewart. TheWrap confirmed that Universal was in talks with McG to direct the supernatural action-adventure, with a release date set for 2012.  On April 19, 2011 Evan Spiliotopoulos was set to rewrite the script already written by Horowitz & Jefferies, while McG was set to direct the film. 

On June 16, 2011, screenwriter Simon Kinberg was set to work on the films script.  On August 23, 2011, it was confirmed that Universal had officially dropped the Hasbro board-game adaptation,  the reason being the high budget. 
 Disney did The Lone Ranger.  On November 29, 2011, THR reported that Marti Noxon would be working on the script of the board game adaptation.  On March 5, 2012, THR confirmed that Universal and Hasbro were back in business on Ouija, still without director and writer, and were set to release the film in 2013 with Blumhouse Productions Jason Blum also on board to produce the low-budgeted film.  On July 16, 2012, Universal hired Juliet Snowden and Stiles White to rewrite and direct the project for a 2013 release.   

===Casting=== Douglas Smith Erin Moriarty (who was not in the finished film), Ana Coto, and Vivis Colombetti, and filming was set to begin in Los Angeles that week.    On December 13, 2013, Matthew Settle was added to the cast of the film, playing Anthony Morris, father of Laine Morris (Cookes character).   

===Filming=== Filming began in mid-December 2013 in Los Angeles. 

===Reshoots===
According to Olivia Cooke, half of the film was re-shot at the studios request. The story was substantially changed in the process, including the back story and look of Doris Zander, and the characters of Mrs. Zander and Paulina were added. Some scenes from the original version of the film are still present in promotional material for the film, including trailers and Facebook photos. 

==Release==
Universal released Ouija in the United States on October 24, 2014.   

===Box office===
;North America John Wick ($14.2 million).  The film played 75% under-25 years old and 61% female on its opening weekend. 

;Other territories
Ouija was released in five international markets and earned $1.3 million from 234 screens. The film went to number two in Malaysia ($545,000), number four in Taiwan ($331,000), number two in Singapore ($238,000) and also number four in Poland ($137,000).  In its second weekend the film earned $5.7 million from 1,166 screens in 19 territories for a two weekend international total of $7.7 million and a worldwide total of $42.7 million. It went number one in the UK, Indonesia and the Philippines. In the UK, the film earned $2.2 million on its opening weekend which is the second biggest opening weekend for a horror film in 2014 only behind Annabelle (film)|Annabelle ($3.1 million). 

===Critical response===
  
Ouija received negative reviews from critics. Review aggregation website  , another review aggregator, gave the film a weighted average score of 38 out of 100, based on reviews from 22 critics, indicating "generally unfavorable reviews".  Audiences surveyed by CinemaScore gave the film a C grade. 

Although the film received negative reviews, Ouija did extremely well at the box office: with a budget of $5m, it earned $100m. 

==Sequel==
Throughout January 2015, reports of a sequel were announced. In February 2015, it was confirmed the film was in development and had no release date with Jason Blum stating "  Were a ways away  ."  in April 2015, it was announced that the sequel would be released on October 21, 2016. With a script by Mike Flanagan and Jeff Howard (Oculus (film)|Oculus). The film is produced by Michael Bay , Brad Fuller and Andrew Form, Jason Blum,  Brian Goldner and Stephen Davis. A director and cast has yet to be announced. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 