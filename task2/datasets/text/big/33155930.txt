Chouans!
{{Infobox film
| name           = Chouans!
| image          = Chouans (film).jpg
| caption        = Theatrical release poster
| director       = Philippe de Broca
| producer       = Ariel Zeitoun
| writer         =  
| starring       =  
| music          = Georges Delerue
| cinematography = Bernard Zitzermann
| editing        = Henri Lanoë
| distributor    = UGC
| released       =  
| runtime        = 143 minutes
| country        = France
| language       = French
}}

Chouans! is a 1988 French historical adventure film directed by Philippe de Broca and starring Sophie Marceau, Philippe Noiret, and Lambert Wilson.    Based on the 1829 novel Les Chouans by Honoré de Balzac, the film is about a woman who must choose between two brothers on opposite sides of the French Civil War of 1793.    For her performance in the film, Sophie Marceau received the Cabourg Romantic Film Festival Award for Best Actress.   

==Plot==
In 1793, during the French Revolution, a young woman named Céline (Sophie Marceau), who was adopted by Count Savinien de Kerfadec, must choose between two men who have been raised like her brothers, Tarquin Larmor (Lambert Wilson) and Aurèle de Kerfadec (Stéphane Freiss), while they take opposite sides in the conflict. Tarquin, also adopted by the Count, is a partisan of the New Republic and defends the new political system; Aurèle, the Counts natural son, supports the Royalist side. Both sons are in love with Céline. After the Republican Army decimates Western France, an insurgence of peasants, clergy, and aristocrats loyal to the Royalists stage a counterrevolution.

==Cast==
* Philippe Noiret as Savinien de Kerfadec 
* Sophie Marceau as Céline
* Lambert Wilson as Tarquin Larmor
* Stéphane Freiss as Aurèle de Kerfadec
* Charlotte de Turckheim as Olympe de Saint-Gildas
* Jean-Pierre Cassel as Baron de Tiffauges
* Roger Dumas as Bouchard 
* Raoul Billerey as Grospierre 
* Jacqueline Doyen as Adélaïde, lAbbesse Marie de lAssomption
* Vincent Schmitt as Lote 
* Claudine Delvaux as Jeanne 
* Jean Parédès as le Chapelain 
* Isabelle Gélinas as Viviane 
* Vincent de Bouard as Yvon 
* Maxime Leroux as Le Prêtre réfractaire 
* Luc-Antoine Diquéro as Le Sergent Pierrot 
* Claude Aufaure as Croque-au-sel 
* Michel Degand as Le Prêtre jureur   

==Production==
===Filming locations===
* Baden, Morbihan, France 
* Belle Île, Morbihan, France 
* Brittany, France 
* Fort-la-Latte, Côtes-dArmor, France 
* Locronan, Finistère, France 
* Meucon, Morbihan, France 
* Poul-Fétan, Quistinic, Morbihan, France 
* Sarzeau, Morbihan, France 
* Île dHoedic, Morbihan, France   

==Awards and nominations==
* 1988 Cabourg Romantic Film Festival Award for Best Actress (Sophie Marceau) Won
* 1989 César Award for Most Promising Actor (Stéphane Freiss) Won
* 1989 César Award Nomination for Best Costume Design (Yvonne Sassinot de Nesle)

==References==
 

==External links==
*  

 
 
 
 
 
 