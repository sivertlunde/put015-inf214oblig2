The Jury of Fate
 
{{Infobox film
| name           = The Jury of Fate
| image          = The Jury of Fate.jpg
| image_size     = 
| caption        = 
| director       = Tod Browning
| producer       = B. A. Rolfe
| writer         = Finis Fox June Mathis
| starring       = William Sherwood Mabel Taliaferro
| music          = 
| cinematography = Charles W. Hoffman
| editing        = 
| distributor    = 
| released       =  
| runtime        = 5 reels (approximately 50 minutes)
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}

The Jury of Fate is a lost  1917 American drama film directed by Tod Browning.    

==Cast==
* William Sherwood - Donald Duncan
* Mabel Taliaferro - Jaques Jeanne
* Frank Bennett - François Leblanc (as Frank Fisher Bennett)
* Charles Fang - Ching
* Albert Tavernier - Henri Labordie
* Bradley Barker - Louis Hebert
* H. F. Webber - Duval Hebert

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 