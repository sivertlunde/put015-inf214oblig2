Boom! (film)
 
{{Infobox film
| name           = Boom!
| image          = Boom!poster.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Joseph Losey
| producer       = John Heyman Lester Persky (assoc. producer)
| writer         = Tennessee Williams
| based on =  
| narrator       =
| starring       = Elizabeth Taylor Richard Burton Noël Coward Joanna Shimkus John Barry
| cinematography = Douglas Slocombe
| editing        = Reginald Beck
| distributor    = Universal Pictures
| released       = May 28, 1968
| runtime        = 113 min
| country        = United Kingdom English Italian Italian
| budget         = $3.9 million Alexander Walker, Hollywood, England, Stein and Day, 1974 p345 
| gross          =
| preceded by    =
| followed by    =
}}
Boom! is a 1968 British drama film starring Elizabeth Taylor, Richard Burton and Noël Coward, directed by Joseph Losey, and adapted from the play The Milk Train Doesnt Stop Here Anymore by Tennessee Williams.

==Plot== The Angel of Death".
 allegorical and Symbolist significance.  Secondary characters chime in, such as "the Witch of Capri" (Coward).  The movie mingles respect and contempt for human beings who, like Goforth, continue to deny their own death even as it draws closer and closer.  It examines how these characters can enlist and redirect their fading erotic drive into the reinforcement of this denial.

==Reception==
The film was received poorly by critics, and maintains an 8% rating at Rotten Tomatoes. Rotten Tomatoes page: " " 
 John Waters admires the film, and chose it as a favorite to present in the first Maryland Film Festival in 1999. The films poster is visible in Waters 1972 film Pink Flamingos. In an interview with Robert K. Elder for his book The Best Film Youve Never Seen, Waters describes the film as "beyond bad. It’s the other side of camp. It’s beautiful, atrocious, and it’s perfect. It’s a perfect movie, really, and I never tire of it.” 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 