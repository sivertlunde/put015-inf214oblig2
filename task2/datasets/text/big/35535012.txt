Posti (film)
{{Infobox film name = Posti image =  image_size =  border =  alt =  caption =  director = K.D. Mehra producer = Kwatra Production writer =  screenplay =  story =  based on =   narrator =  starring = Majnu Shyama Bhag Singh Ramesh Thakur music = Sardul Singh Kwatra cinematography =  editing =  studio =  distributor = released = 1950 runtime =  country = India language = Punjabi The songs were written by Manohar Singh Sehrai budget =  gross =
}}

Posti ( ) is a 1950 Punjabi film and the first production of Kwatra Production (of Sardul Singh Kwatra and Harcharan Singh Kwatra),    directed by K.D. Mehra (Krishan Dev Mehra), starring Majnu and Shyama in lead roles. Shyama was the principal female actress of this movie  
    It did very well at the box office and was a hit not only in the East Punjab Circuit, but also in the Delhi area.  A noted playback singer, Asha Bhosle, made her debut with the film.  

== Music ==
 Punjab and Punjabi Kurhmai in 1941, but in the film Posti her songs became real hits.  Popular Songs of the film include do guttan kar merian by Asha Bhosle, kajjle di paanian dhaar by Rajkumari and a duet ja bhaira posti by Mohd. Rafi and Shamshad Begum were hits. 

== See also ==

*Bhangra (film)|Bhangra
*Lachhi
*Do Lachhian

== References ==
 

== External links ==
* 

 
 
 
 
 


 