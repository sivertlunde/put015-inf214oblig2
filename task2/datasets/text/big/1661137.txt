Elizabethtown (film)
{{Infobox film
| name        = Elizabethtown
| image       = Elizabethtown poster.jpg 
| caption     = Theatrical release poster
| director    = Cameron Crowe
| writer      = Cameron Crowe
| narrator    = Orlando Bloom
| starring    = Orlando Bloom Kirsten Dunst Susan Sarandon Alec Baldwin Nancy Wilson
| producer    = Cameron Crowe Tom Cruise Paula Wagner
| cinematography = John Toll
| editing     = David Moritz
| studio      = Cruise/Wagner Productions Vinyl Films
| distributor = Paramount Pictures
| budget      = $45 million
| gross       = $52,034,889  . Box Office Mojo. Retrieved 2010-10-28. 
| released    =  
| runtime     = 123 minutes
| language    = English
| country     = United States
}} romantic tragicomedy-drama film written and directed by Cameron Crowe starring Orlando Bloom and Kirsten Dunst.  Alec Baldwin has a small role as a CEO of an athletic shoe company and Susan Sarandon appears as a grieving widow.

==Plot==
Drew Baylor (Orlando Bloom|Bloom) is an intelligent young man and designer for a shoe company. When his latest design, hyped to be a great accomplishment in his life, has a flaw that will cost the company $972 million to correct, Drew is shamed by his boss (Alec Baldwin) and his coworkers before he is dismissed. Disappointed in his failure, and the subsequent breakup with his girlfriend Ellen (Jessica Biel), he plans an elaborate suicide by taping a butcher knife to an exercise bike, only to be stopped at the last moment by a tearful call from his sister Heather that his father died of a heart attack while visiting family in Elizabethtown, Kentucky. Drew volunteers to retrieve the body, following a memorial service when his mother Hollie (Susan Sarandon|Sarandon) refuses to go, following a dispute between her and the rest of the Kentucky Baylors, who consider them "Californian" despite the fact they were in California for a little over a year 27 years before.

On the flight to Kentucky, Drew meets Claire (Kirsten Dunst|Dunst), an optimistic, enthusiastic and kind flight attendant who gives him a seat in first class, due to the plane being empty, so she does not have to travel all the way back to coach. She proves helpful and happy to an otherwise despondent Drew, giving him directions, instructions and tips on getting to his destination before they part ways. When he gets to Elizabethtown, Drew is met with the love of the family, though he is somewhat goaded by being a "California Boy" and he makes the arrangements for a cremation at Hollies request despite the familys objections. While staying at a hotel, where a wedding reception is being held for the weekend, Drew calls his mother and sister, then his ex-girlfriend as he continues to struggle with his suicidal thoughts. Finally, he calls Claire, who relieves his anxiety and the two of them talk for hours. She impulsively suggests they drive out to meet before she has to depart on a flight to Hawaii that morning and they meet and talk.

Drew comes to grips with his fathers death, and while he is visiting his Aunt Dora, his uncle Bill remarks on how his father would look in the suit. Drew realizes that he hadnt given the suit to the mortuary to be cremated, and has second thoughts on the procedure. He rushes out to stop the cremation but is too late and is given his fathers ashes. Claire returns from her flight and unexpectedly meets him at the hotel where they become friends with Chuck and Cindy, whose reception is the one being held there. Drew and Claire sleep together, but when she tells him she loves him, he responds with regret that he failed his company and failed at his life, admitting he was contemplating suicide. Claire shrugs it off, saying that its only money and leaves upset when Drew does not respond.

Hollie and Heather arrive for the service, and Hollie gives an amusing anecdote with her eulogy, likening herself as a comedian, before dancing to their song. Claire arrives, and during a bands presentation of the song Freebird a prop lights on fire, setting off the sprinkler system, during which Drew and Claire kiss passionately. Claire tells Drew to take one final trip with his father, giving him a map and marking special stops to make along the way. Drew follows the map home, spreading his fathers ashes at memorable sites until the map gives him a choice; to either follow the map home, or follow new directions. He chooses the latter, which leads him to a small town fair, where he encounters Claire waiting for him. The two kiss and Drew finally realizes what Claire has been telling him all along: life is going to be filled with fierce battles, but through the battles, redemption is found and results in a glorious life.

==Cast==
* Orlando Bloom as Drew Baylor
* Kirsten Dunst as Claire Colburn
* Susan Sarandon as Hollie Baylor
* Alec Baldwin as Phil DeVoss
* Bruce McGill as Bill Banyon
* Judy Greer as Heather Baylor
* Jessica Biel as Ellen Kishmore Paul Schneider as Jessie Baylor
* Loudon Wainwright III as Uncle Dale
* Gailard Sartain as Charles Dean
* Jed Rees as Chuck Hasboro Jim Fitzpatrick as Rusty
* Paula Deen as Aunt Dora
* Dan Biggers as Uncle Roy
* Alice Marie Crowe as Aunt Lena
* Tim Devitt as Mitch Baylor
* Ted Manson as Sad Joe
* Shane Lyons as Charlie Bill
* Emily Rutherfurd as Cindy Hasboro
* Nigel Miguel as Basketball Coach 

==Production== Chris Evans, Biel auditioned for the female lead, but was given a smaller role as Drews then-girlfriend.

There is a character named Ben who is mentioned as a love interest of Claire. In the original cut of the film, Ben is revealed to be Claires brother.
 Brown Hotel and Cave Hill Cemetery. Although the exterior, lobby, and corridors of the Brown Hotel are seen, a passable replica of the Brown Hotels Crystal Ball Room was re-created on a soundstage.  While Orlando Bloom is supposedly traveling to "Elizabethtown" by car, he is going the incorrect direction on the road. He is also pictured going through the Cherokee Park tunnel, which happens to be on I-64. Elizabethtown is on I-65, about 60 miles in the other direction.
 Elizabethtown itself Meade County, near Brandenburg, Kentucky|Brandenburg.  Filming also took place in Scottsbluff, Nebraska;  Eureka Springs, Arkansas; Memphis, Tennessee; and Oklahoma City. 

In the original cut of the film shown at the Toronto Film Festival, an epilogue reveals that the shoe designed by Drew turns out to be a hit, as it whistles with every step. This was cut from the release version of the film to prevent the ending seeming overly-drawn out. 
 Joni Mitchells painting Hyde Park appears in this film. Previously, one of her paintings had appeared in Crowes Vanilla Sky.

==Release==

===Critical reception  ===
The film received mostly negative reviews by critics, citing mostly Kirsten Dunsts performance and lack of originality. Rotten Tomatoes gives the film a 28% "Rotten" rating based on 166 reviews. The sites consensus is "This story of a floundering shoe designer who returns home for a family tragedy gets lost in undeveloped plot lines and lackluster performances."  It holds a Metacritic score of 45 out of 100. 
 Meet Cute" in movie history. He goes on to say that although the film is nowhere near one of Crowes great films like Almost Famous, it is sweet and good-hearted and has some real laughs.     Ebert would later reprint on his site an analysis of the film pointing out various plot elements supporting the idea of Claire being an angel. 

===Manic Pixie Dream Girl===
In his review, Nathan Rabin of The A.V. Club created the term "Manic Pixie Dream Girl" to describe the "bubbly, shallow cinematic creature" stock character type that Dunst plays in the film.         

===Box office===
Elizabethtown was commercially released on October 14, 2005 in the United States. It was distributed to 2,517 theaters,  and grossed $4,050,915 on its opening day. At the end of its opening weekend, the film had grossed $10,618,711, making it the third biggest opening for that weekend. Overall, the film grossed $52,034,889 worldwide within its release of 68 days. 

==Soundtrack==
  rock songs, and Kentucky natives My Morning Jacket portray a fictional rock group named Ruckus who reunite during the film.

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 