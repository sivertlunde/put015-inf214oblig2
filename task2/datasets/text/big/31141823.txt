Emerald of the East
 
 
{{Infobox film
| name           = Emerald of the East
| image          =
| caption        =
| director       = Jean de Kuharski
| producer       = 
| writer         = Jerbanu Kothawala (novel)   Lothar Knud Frederik   Jean de Kuharski   
| starring       = Joshua Kean   Mary Odette   Lya Delvelez   Gillian Dean
| music          = Ed May
| cinematography =  George Pocknall   Joe Rive
| editing        = 
| studio         = British International Pictures   British Pacific
| distributor    = Wardour Films
| released       = January 1929
| runtime        = 5,600 feet 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}}
Emerald of the East is a 1929 British adventure film directed by and featuring Jean de Kuharski. It also starred Joshua Kean, Mary Odette and Lya Delvelez.  It was based on a novel by Jerbanu Kothawala. It was one of a growing number of British films to be set in India during the era. 

==Plot== India British troops attempt to rescue the kidnapped son of a Maharaja.

==Cast==
* Joshua Kean - Lt. Desmond Armstrong 
* Mary Odette - Nellum 
* Jean de Kuharski - Maharajah Rujani 
* Lya Delvelez - The Maharanee 
* Gillian Dean - Evelyn Gordon 
* Maria Forescu - The Chieftainess 
* Kenneth Rive - Maharaj Kumar 
* Promotha Bose - Vaghi

==References==
 

==Bibliography==
* Lahiri, Shompa. Indians in Britain: Anglo-Indian encounters, race and identity, 1880-1930. Frank Casss, 2000.
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 