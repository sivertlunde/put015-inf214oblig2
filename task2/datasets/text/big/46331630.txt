Death Duel of Kung Fu
 
{{Infobox film
 | name           = Death Duel of Kung Fu 形手螳螂腿
 | image          = 
 | caption        = 
 | director       = William Cheung Ki
 | action director = Chin Yuet-Sang
 | producer       = Chang Yan Tao See Lai Woo
 | writer         = Chan Wa Cheung San Yee
 | starring       = John Liu (Actor) Don Wong Tao Eagle Han-ying
 | music          = 
 | cinematography = William Cheung Ki
 | distributor    = Kee Woo Films
 | released       = 1979
 | country        = Taiwan
 | runtime        = 82 minutes Cantonese
 | budget         =
 }}

Death Duel of Kung Fu is 1979 martial art movie, directed by William Cheung Ki and starring John Liu, Don Wong Tao and Eagle Han-ying. It`s also known as Showdown of the Master Warriors and Return of the Secret Rivals as an alternate title. With in say, the movie has no relationship with the Secret Rivals trilogy but most of the filming locations took places in South Korea although the story is set in China. It`s also Eagle Han-ying`s first Hong Kong movie debut.

==Plot==
It opens with the top fighter and general Sun Chin Qwei (Don Wong) kills marshall Tao and heads to the Taiwan to join the army however it is later pursued by the lord To Ko Lam (Eagle Han). Qwei escapes and along the way he fell in love with the Japanese girl Keigi who is actually a descent of To Ko Lam but later fall in love with Qwei. Meanwhile northern kick boxing champion Sun Hsun (John Liu) is giving an Sun Chin Qwei a hard time but later they join forces together to defeat To Ko Lam.

==Casts==
*John Liu (Actor) as Sun Hsun
*Don Wong Tao as Sun Chin Qwei
*Eagle Han-ying as To Ko Lam 
*Kim Chung Ja as Keigi
*Wu Jia Xiang as Marshall Tao (cameo)
*Chan Yiu Lam as Ka Yee Kee
*Peter Chan as Marshall Tao`s assistant (cameo)
*Chung Fat as To Ko Lam`s sword fighter
*King Lee Chung as To Ko Lam`s swords fighter
*Tang Ti (extra)
*Cheung Tin Ho (extra)
*Gam Hing Ji (extra)
*Chin Yuet Sang (stunts)
*Corey Yuen Kwai (stunts)
*Mang Hoi (extra, stunts)
*Jackie Chan

==Reception==
Outside the film the movie got positive receptions. IMDb gave 6.4/10 and Amazon.com gave 4.1/5. 

==Production==
The movie was filmed in South Korea. Some of the locations took from the same location from the Secret Rivals. Once again Don Wong and John Liu teamed up together. 

==External link==
* 

 
 
 
 
 