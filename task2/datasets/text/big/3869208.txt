Let It Be (1970 film)
 
 
 
{{Infobox film
| name           = Let It Be
| image          = Film4.jpg
| caption        = Mock-up of theatrical release poster (US)
| director       = Michael Lindsay-Hogg
| writer         =
| starring       = The Beatles
| producer       = Neil Aspinall
| music          = John Lennon Paul McCartney George Harrison Ringo Starr
| cinematography = Anthony B. Richmond
| editing        = Tony Lenny Apple Films
| distributor    = United Artists
| released       =  
| runtime        = 80 minutes 
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
|}}
 rooftop concert by the group, their last performance in public. Released just after the album, it was the final original Beatles release.

The film was originally planned as a television documentary which would accompany a concert broadcast. When plans for a broadcast were dropped, the project became a feature film. Although the film does not dwell on the dissension within the group at the time, it provides some glimpses into the dynamics that would lead to the Beatles break-up.
 Academy Award for Best Original Song Score for the film.

==Coverage==
The film observes The Beatles ( , providing the hammer blows on "Maxwells Silver Hammer", and Yoko Ono, dancing with Lennon.
 Apple headquarters, Dig It," Linda Eastmans Heather plays Let It Be", and "The Long and Winding Road".
 unannounced concert from the studio rooftop. They perform "Get Back," "Dont Let Me Down (The Beatles song)|Dont Let Me Down," "Ive Got a Feeling," "One After 909," and "Dig a Pony," intercut with reactions and comments from surprised Londoners gathering on the streets below. The police eventually make their way to the roof and try to bring the show to a close, as the show was disrupting businesses lunch hour nearby. This prompts some ad-libbed lyrical asides from McCartney: during the second performance of Get Back, he sings, "Get back, Loretta&nbsp;... youve been out too long, Loretta&nbsp;... youve been playing on the roofs again&nbsp;... and your mummy doesnt like that&nbsp;... it makes her angry&nbsp;... shes gonna have you arrested! Get back, Loretta!". In response to the applause from the people on the rooftop after the final song, McCartney says, "Thanks Mo!" (to Ringos wife Maureen) and Lennon quips, "Id like to say thank you on behalf of the group and ourselves, and I hope we passed the audition!" 

==Production==

===Concept=== The Beatles (the "White Album") wrapped up in October 1968, McCartney concluded that the group needed to return to their roots for their next project. The plan was to give a live performance featuring new songs, broadcast as a television special and recorded for release as an album. (At one point McCartney considered launching a tour; however, the idea was quickly shot down by the other members.) Unlike their recent albums, their new material would be designed to work well in concert, without the benefit of overdubs or other recording tricks.   
 The Magic Christian, it was agreed to start rehearsals without a firm decision on the concert location. Lewisohn (2000), pp. 306–307. 
 16 mm for use as a separate "Beatles at Work" television documentary which would supplement the concert broadcast.  To facilitate filming, rehearsals would take place at Twickenham Film Studios in London. Michael Lindsay-Hogg was hired as the director, having previously worked with The Beatles on promotional films for "Paperback Writer", "Rain (The Beatles song)|Rain", "Hey Jude" and "Revolution (song)|Revolution".

===Filming===
The Beatles assembled at Twickenham Film Studios on 2 January 1969, accompanied by the film crew, and began rehearsing. Cameraman Les Parrott recalled: "My brief on the first day was to shoot The Beatles. The sound crew instructions were to roll/record from the moment the first Beatle appeared and to record sound all day until the last one left. We had two cameras and just about did the same thing."  The cold and austere conditions at Twickenham, along with nearly constant filming and sessions starting much earlier than the Beatles preferred schedule, constrained creativity and exacerbated tensions within the group. The sessions were later described by Harrison as "the low of all-time" and by Lennon as "hell&nbsp;... the most miserable sessions on earth." 

The infamous exchange between McCartney and Harrison occurred on Monday, 6 January.  Around lunchtime on Friday, 10 January, tensions came to a head and Harrison told the others that he was leaving the band.   This entire episode is omitted from the film.  He later recalled: "I thought, Im quite capable of being happy on my own, and if Im not able to be happy in this situation Im getting out of here. So I got my guitar and went home and that afternoon wrote  , “would give him full scope to play his guitar.”     Years later, Clapton commented on the absurdity of this idea: “There may have been  . The problem with that was I had bonded or was developing a relationship with George, exclusive of them. I think it fitted a need of his and mine, that he could elevate himself by having this guy that could be like a gunslinger to them. Lennon would use my name every now and then for clout, as if I was the fastest gun. So, I don’t think I could have been brought into the whole thing because I was too much a mate of George’s.” 

 
At a meeting on 15 January, Harrison agreed to return with the conditions that elaborate concert plans be dropped and that work would resume at Apple Corps|Apples new recording studio. At this point, with the concert broadcast idea abandoned, it was decided that the footage being shot would be used to make a feature film.  Filming resumed on 21 January at the basement studio inside Apple headquarters on Savile Row in London.  Harrison invited keyboardist Billy Preston to the studio to play electric piano and Hammond Organ|organ.  Harrison recalled that when Preston joined them, "straight away there was 100% improvement in the vibe in the room. Having this fifth person was just enough to cut the ice that wed created among ourselves."  Filming continued each day for the rest of January.
 Teddy Boy," Every Night"), All Things Must Pass," "Isnt It a Pity"). The group also experimented with some of their previous songs ("Love Me Do," "Help! (song)|Help!," "Lady Madonna," "You Cant Do That") and played "I Lost My Little Girl" – which was the first song written by McCartney, when he was 14. 
 unannounced lunchtime concert on the roof of the Apple building. On 30 January, The Beatles with Preston played on the rooftop in the cold wind for 42 minutes, about half of which ended up in the film. The Beatles started with a rehearsal of "Get Back," then played the five songs which are shown in the film. After repeating "Ive Got a Feeling" and "Dont Let Me Down," takes which were left out of the film, the Beatles are shown in the film closing with another pass at "Get Back" as the police arrive to shut down the show. On the 31st, the last day of filming and recording, the Beatles reconvened in the Apple buildings basement studio. They played complete performances of "Two of Us," "The Long and Winding Road," and "Let It Be," which were included in the film as the end of the Apple studio segment, before the closing rooftop segment. 

===Post-production===
A rough cut of the movie was screened for The Beatles on 20 July 1969. Lindsay-Hogg recalled that the rough cut was about an hour longer than the released version: "There was much more stuff of John and Yoko, and the other three didnt really think that was appropriate because they wanted to make it a nicer movie. They didnt want to have a lot of the dirty laundry, so a lot of it was cut down." Unterberger (2006). pp. 332–333.  After viewing the released version, Lennon said he felt that "the camera work was set up to show Paul and not to show anybody else" and that "the people that cut it, cut it as Paul is God and were just lyin around&nbsp;..." 

Lindsay-Hogg omitted any reference to Harrison leaving the sessions and temporarily quitting the group, but managed to keep some of the interpersonal strains in the final cut, including the McCartney/Harrison exchange which he had captured by deliberately placing the cameras where they would not be noticed. He also retained the scene that he described as "the back of Pauls head as hes yammering on and John looks like hes about to die from boredom."   

In early 1970 it was decided to change the planned name of the film and the associated album from Get Back to Let It Be, matching the groups March 1970 single release. The final version of the film was blown-up from full-frame 16&nbsp;mm to 35 mm film for theatrical release, which increased the films graininess. To create the wider theatrical aspect ratio, the top and bottom of the frame was cropped, necessitating the repositioning of every single shot for optimum picture composition.

==Soundtrack==
While the album Let It Be contains many of the song titles featured in the film, in most cases they are different performances. The film has additional songs not included on the album.

The following songs are listed in the order of their first appearance, with songwriting credited to Lennon–McCartney except where noted.

*"Pauls Piano Intro"
**based on "Adagio for Strings" (Samuel Barber),  and titled "Pauls Piano Piece" on Let It Be... Naked
*"Dont Let Me Down (The Beatles song)|Dont Let Me Down"
*"Maxwells Silver Hammer"  Two of Us" 
*"Ive Got a Feeling"  
*"Oh! Darling"
*"One After 909"
*"Jazz Piano Song" (McCartney/Ringo Starr|Starkey) 
*"Across the Universe" Sulpy and Schweighardt, p. 106 
*"Dig a Pony" 
*"Suzy Parker" (Lennon/McCartney/George Harrison|Harrison/Starkey) Sulpy and Schweighardt, p. 148 
*"I Me Mine" (Harrison)  
*"For You Blue" (Harrison) 
*"Bésame Mucho|Bésame mucho" (Consuelo Velázquez/Sunny Skylar) 
*"Octopuss Garden" (Starkey) 
*"Youve Really Got a Hold on Me" (Smokey Robinson) 
*"The Long and Winding Road" 
*Medley: Rip It Up" (Robert Blackwell/John Marascalco)
**"Shake Rattle and Roll" (Jesse Stone, under his working name Charles E. Calhoun) 
*Medley: Kansas City" Jerry Leiber/Mike Stoller) Sulpy and Schweighardt, p. 277  Richard Penniman/Enotris Johnson)
**"Lawdy Miss Clawdy" (Lloyd Price)  Dig It"  Let It Be"
*"Get Back"

==Release and reception== Oscar for Original Song Grammy for Best Original Score". 

Initial reviews were generally unfavorable; the British press were especially critical,  with The Sunday Telegraph commenting that "it is only incidentally that we glimpse anything about their real characters—the way in which music now seems to be the only unifying force holding them together, and the way Paul McCartney chatters incessantly even when, it seems, none of the others are listening."  Time said that "rock scholars and Beatles fans will be enthralled" while others may consider it only a "mildly enjoyable documentary newsreel." 

Later reviews were more favourable, although rarely glowing, as the historical significance of the films content factored into critics assessments. Leonard Maltin rated the film as 3 out of 4 stars, calling it "uneven" and "draggy", but "rescued" by The Beatles music.   The TLA Video & DVD Guide, also rating it as 3 out of 4 stars, described the film as a "fascinating look at the final days of the worlds most famous rock group, punctuated by The Beatles great songs and the legendary rooftop concert sequence.&nbsp;... It is important viewing for all music fans."  Rotten Tomatoes reported that 75% of twelve critics reviews were positive; user reviews were 86% positive. 

Lindsay-Hogg told Entertainment Weekly in 2003 that reception to Let It Be within the Beatles camp was "mixed";  he believes McCartney and Lennon both liked the film, while Harrison disliked it because "it represented a time in his life when he was unhappy&nbsp;... It was a time when he very much was trying to get out from under the thumb of Lennon–McCartney." 

==Home media==
The film was first released on VHS tape in 1981 by  ). The lack of availability has prompted considerable bootlegging of the film, first on VHS and later on DVD, derived from copies of the early 1980s releases. The early eighties also saw the film released on VHS and Betamax in Germany and the Netherlands, these versions were not the same transfer as the USA release, as they were based on the native 4:3 aspect ratio from the original 16mm negative, thus presenting the film as less cropped than the US releases.
 The Beatles Anthology documentary. After additional remastering, a DVD release was planned to accompany the 2003 release of Let It Be... Naked, including a second DVD of bonus material,  but it never materialised. In February 2007, Apple Corps Neil Aspinall said, "The film was so controversial when it first came out. When we got halfway through restoring it, we looked at the outtakes and realised: this stuff is still controversial. It raised a lot of old issues."   

An anonymous industry source told the Daily Express in July 2008 that, according to Apple insiders, McCartney and Starr blocked the release of the film on DVD. The two were concerned about the effect on the bands "global brand&nbsp;... if the public sees the darker side of the story. Neither Paul nor Ringo would feel comfortable publicising a film showing The Beatles getting on each others nerves&nbsp;... Theres all sorts of extra footage showing more squabbles but its questionable if the film  will ever see a reissue during Paul and Ringos lifetime." 

==References==
;Notes
 

;Bibliography
*  
*  
*   series]] | publisher=Continuum International | year=2004 | isbn=0-8264-1634-9}}
*  
*  

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 