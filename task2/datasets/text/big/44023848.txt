Missi (film)
{{Infobox film
| name           = Missi
| image          =
| caption        =
| director       = Thoppil Bhasi
| producer       = MO Joseph
| writer         = Pamman Thoppil Bhasi (dialogues)
| screenplay     = Thoppil Bhasi Lakshmi Mohan Sharma Sam Sankaradi
| music          = G. Devarajan
| cinematography = Balu Mahendra
| editing        = MS Mani
| studio         = Manjilas
| distributor    = Manjilas
| released       =  
| country        = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film,  directed by Thoppil Bhasi and produced by MO Joseph. The film stars Lakshmi (actress)|Lakshmi, Mohan Sharma, Sam and Sankaradi in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  Lakshmi
*Mohan Sharma
*Sam
*Sankaradi
*Babu Joseph
*Janardanan
*K. P. Ummer
*MG Soman Sudheer
*Vallathol Unnikrishnan
*Vidhubala
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Madhu Alappuzha, Mankombu Gopalakrishnan, Bharanikkavu Sivakumar and Bichu Thirumala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anuraagam Anuraagam || K. J. Yesudas || Madhu Alappuzha ||
|-
| 2 || Gangaa pravaahathil || P Jayachandran || Mankombu Gopalakrishnan ||
|-
| 3 || Harivamshaashtami || P. Madhuri || Bharanikkavu Sivakumar ||
|-
| 4 || Kunkumasandhya || P Susheela || Mankombu Gopalakrishnan ||
|-
| 5 || Urangoo Onnurangoo || P. Madhuri || Bichu Thirumala ||
|}

==References==
 

==External links==
*  

 
 
 


 