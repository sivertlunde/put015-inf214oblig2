Dimboola (film)
 
 
{{Infobox film
| image          =
| name           = Dimboola
| director       = John Duigan
| producer       = Max Gillies John Timlin John Weiley John Power
| based on       =  
| starring       = Bruce Spence
| music          = George Dreyfus Tom Cowan
| editing        = Tony Paterson
| distributor    = Greater Union Umbrella Entertainment Videoscope
| released       = 1979
| runtime        = 89 minutes 
| country        = Australia
| language       = English
| budget         = A$350,000 
}} play of the same name by Jack Hibberd and was principally filmed on location in Dimboola, Victoria.

==Plot==
English journalist arrives in a small country town to observe a wedding.  

==Cast==
* Bruce Spence as Morrie McAdam
* Natalie Bate as Maureen Delaney
* Max Gillies as Vivian Worcester-Jones Bill Garner as Dangles Jack Perry as Horrie
* Esme Melville as April
* Dick May as Shovel
* Irene Hewitt as Florence
* Val Jellay as Aggie
* Chad Morgan as Bayonet
* Max Cullen as Mutton Terry McDermott as Darcy

==Production==
The movie was shot in Dimboola, Jeparit and Melbourne. The budget was originally $420,000 but was reduced to $350,000. $120,000 came from the Victorian Film Corporation, $75,000 from the New South Wales Film Corporation, $80,000 from Greater Union, and the rest from private investment. Jack Clancy, "Dimboola", Cinema Papers, October/November 1978, pp. 99–101 

John Duigan had written all his previous movies himself, and worked in a realist style whereas Hibberds writing was more theatrical. Hibberd had trouble collaborating and Duigan feels they had entirely different interpretations of the material which hurt the final movie. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980. pp. 178–180 

The character of the English journalist was added for Max Gillies. The film plays down the differences between the Catholic and Protestant families.

==Songs==
*"Were Riding to the Never Never", music and lyrics: Letty Katts
*"Will You Love Him Tomorrow", music and original lyrics as "Will You Love Me Tomorrow" written by Gerry Goffin and Carole King, new lyrics George Dreyfus
*"The Sheik of Araby", music and lyrics by  Harry B. Smith and Francis Wheeler, performed by the The Captain Matchbox Whoopee Band Red River Valley", traditional
*"Danny Boy", music by Frederic Weatherly 
*"Flame" and "Femme Fatale", written and sung by Frankie Raymond

==Reception==
The film was a box office disaster. 

==Home Media== Pram Factory, a 1994 documentary on the Australian Performing Group. 

A single DVD edition of Dimboola was released by Umbrella Entertainment in October 2008 with fewer special features. 

==See also==
*Cinema of Australia

==References==
 

==External links==
*  
*  at Oz Movies
* 

 

 
 
 
 
 
 
 
 

 
 