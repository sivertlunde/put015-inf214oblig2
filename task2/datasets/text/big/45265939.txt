Running Man (2015 film)
{{Infobox film
| name           = Running Man
| image          = Running Man 2015 film poster.jpg
| alt            = 
| caption        = 
| film name      = 奔跑吧！兄弟 
| directors      = Hu Jia Cen Junyi
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = 
| cinematography = 
| editing        = 
| production companies = Zhejiang Blue Star International Media Co., Ltd Huayi Brothers Media Group Co.,Ltd Wanda Media Co., LTD
| distributors   = Wanda Media Co., LTD Wuzhou Film Distribution Co.,Ltd Huayi Brothers Media Group Co.,Ltd
| released       =   
| runtime        = 88 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = US$70 million 
}} reality comedy film directed by Hu Jia and Cen Junyi. It was released on January 30, 2015.  Its a film adaptation of a Zhejiang Television series Hurry Up, Brother. 

==Cast==
*Angelababy
*Wang Baoqiang Li Chen
*Michael Chen
*Ryan Cheng
*Wong Cho-lam
* 
*Lynn Hung
* 
* 
*Kim Jong-kook

==Box office==
As of February 22, 2015, Running Man has grossed around US$70 million in China.   

The film topped the box office during its opening weekend in China earning US$31.86 million from 155,940 screenings and 7.28 million admissions ahead of Hollywood blockbuster,   which was in its second weekend run.    It remained at the summit in its second weekend earning $24.49 million.   

==References==
 

 
 
 
 


 
 