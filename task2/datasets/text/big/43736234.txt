The Light of Western Stars (1930 film)
{{Infobox film
| name           = The Light of Western Stars
| image          = Poster - Light of Western Stars, The (1930) 03.jpg
| caption        =
| director       = Otto Brower Edward H. Knopf
| producer       =
| based on       =  
| writer         = Grover Jones William Slavens McNutt
| starring       = Richard Arlen Mary Brian
| music          = Charles Midgley
| cinematography = Charles Lang
| editing        = Jane Loring
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
}} The Light of Western Stars. Richard Arlen and Mary Brian were the stars. 

==Cast==
*Richard Arlen - Dick Bailey
*Mary Brian - Ruth Hammond
*Harry Green - Plotz aka Pie-Pan
*Regis Toomey - Bob Drexell
*Fred Kohler - H. W. Stack
*Guy Oliver - Sheriff Grip Jarvis

additional cast
*George Chandler - Slig Whalen
*William Gillis - Barfly
*William Le Maire - Griff Meeker
*Lew Meehan - Rifleman
*Gus Saville - Pop
*Syd Saylor - Square Toe

==References==
 

==External links==
* 
* 
 

 
 
 
 
 
 
 


 