Vandanam
{{Infobox film
| name = Vandanam
| image = Vandanam.jpg
| caption = DVD cover
| director = Priyadarshan
| producer = P. K. R. Pillai
| writer = V. R. Gopalakrishnan Mukesh Jagadeesh Ganesh Kumar Sukumari Maniyanpilla Raju  Cochin Haneefa
| music = Songs:  
| cinematography = S. Kumar
| editing = N. Gopalakrishnan
| distributor = Shirdi Sai Films
| studio = Shirdi Sai Creations
| released =  
| runtime = 168 minutes
| country = India
| language = Malayalam
| budget =
| gross =  1.7 cr
}}
 Ganesh Kumar, Maniyanpilla Raju, and Cochin Haneefa. This was the follow-up film of Priyadarshan and producer P. K. R. Pillai after the commercially successful Chithram (1988).

The story of the film is adapted majorly from the 1987 American film Stakeout (1987 film)|Stakeout.  Priyadarshan later remade it as the Telugu film Nirnayam (1991 film)|Nirnayam (1991).

==Plot== Ganesh Kumar), his cell mate. After a failed attempt to chase and nab the duo, the officials draws out several plans to capture both. Police authorities strongly feel that Fernandez will land up in Banglore and may try to meet up with his only daughter. Unnikrishnan (Mohanlal) and K.Purushothaman Nair (Jagadish), two inspectors from Kerala police are sent to Bangalore to follow the activities of Gadha (Girija Shettar), daughter of Fernandez. Upon reaching Bangalore, Unnikrishnan meets Peter (Mukesh (actor)|Mukesh), his college mate, who is also now an inspector with Karnataka police. Unnikrishnan and Peter makes up a team and sidelines Purushothaman Nair, who struggles hard to cope up with Kannada language. The police team stays at a room just opposite to where Gadha stays as a paying guests. Gadha, working at an advertisement agency as a copy writer is staying along with Maggie Aunty (Sukumari), who is constantly in fight with her son (Kuthiravattom Pappu). Unnikrishnan and Peter, who are born flirts, are more interested in drinking and playing cards than investigation. K.P.Nair tries to report this to the authorities, but fails in getting both in control. Unnikrishnan slowly develops a crush towards Gadha, which gets transformed into a full fledged passion, but Gadha never accepts it. Unni befriends with Maggie aunty and tries all ways to woo Gadha. Meanwhile Unni and Peter finds out that Fernandez is in the city and is constantly in touch with Gadha. The police commissioner (M. G. Soman) is in all efforts to nab Fernandez. Unnikrishnan decides to use Gadha to reach out Fernandez. Gadha reveals to Unni about the past of Fernandez, when he was a college professor, who was loved and cared by the students. The sincere professor questioned the college authorities on many administration malpractices, which made him a dust in their eyes. The son of the police commissioner was punished by professor, which led to serious fracas in college and authorities were forced to take action on him under severe protests from students. In retaliation, he attacked professor while traveling with a female student. The girl was raped and killed and the professor was falsely convicted in the case. Now, professor is out of jail to avenge on it. Unnikrishnan tries to convince professor with the help of Gadha, but he fails to give up. Meanwhile, an old man, who is a client of the ad agency, where Gadha works, likes her and asks her to join him as his secretary abroad. Gadha, who had by then developed a soft feeling towards Unni lets him to bring his mother (Kaviyoor Ponnamma) to see her. At the same time, Professor succeeds in killing the son of the commissioner. He orders a shoot at sight at Fernandez. Professor goes berserk and loses mental stability and decides to plant a bomb at the city stadium, which might kill hundreds of civilians. Unnikrishnan asks Gadha to make him give up this heinous plan, but he refuses. That evening, Fernandez plants a bomb, but police team under Unnikrishnan corners him and takes him to the tower. Professor, despite repeated requests denies to explain how defuse the bomb. But Unnikrishnan successfully defuses the bomb, thereby saving the lives of hundreds. But Gadha, who actually wanted to join with Unni, couldnt join with him and decides to go with the old man to London. The film ends with both the cars with Gadha and Unni stopping at a junction, when the signal turns green, they move to opposite roads.

==Cast==
* Mohanlal as Unnikrishnan
* Girija Shettar as Gadha
* Nedumudi Venu as Kurian Fernandez
* Saleema as Mercy Sulakshana as Kurian Fernandezs Friend Mukesh as Peter
* M.G. Soman as Police Commissioner of Bangalore
* Jagadish as K. Purushothaman. Nair
* Kaviyoor Ponnamma as Unnikrishnans mother
* Sukumari as Maggie Aunty (Gadhas landlord)
* Manian Pillai Raju as Harihar Iyer
* K. B. Ganesh Kumar as Raghu Janardhanan as M.P.
* Cochin Haneefa as M.P.s Lawyer

==Soundtrack==
The songs of this film were composed by Ouseppachan with lyrics penned by Shibu Chakravarthy.
{| class="wikitable"
|-
! Track !! Song Title !! Singer(s) !! Raga
|- Chorus || Madhyamavati
|-
| 2 || Kavilinayil || M. G. Sreekumar, Sujatha, Ouseppachan ||
|-
| 3 || Anthiponvettam || M. G. Sreekumar, Sujatha, Chorus || Madhyamavati
|-
| 4 || Meghangale || Nedumudi Venu, Sujatha || Mohanam
|-
| 5 || Thillana || M. G. Sreekumar || Vrindavani sarang
|-
| 6 || Anthiponvettam || M. G. Sreekumar, Chorus || Madhyamavati
|}

==Production==
* The story has certain subplots inspired from the American movie Stakeout (1987 film)|Stakeout.
* Vandanam was later remade in Telugu as Nirnayam (1991 film)|Nirnayam by Priyadarshan, starring Nagarjuna Akkineni and Amala (actress)|Amala.
* Girija Shettar was chosen for the role after Priyadarshan watched her in Geethanjali (1989 film)|Geethanjali. This was her only Malayalam film released, as her second film Dhanushkodi directed by Priyadarshan starring Mohanlal and Raghuvaran was shelved. 

==References==
 

==External links==
*  

 

 
 
 
 
 