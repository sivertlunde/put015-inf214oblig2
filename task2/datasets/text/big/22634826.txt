Kamen Rider Decade: All Riders vs. Dai-Shocker
{{Infobox film
| name           = Kamen Rider Decade: All Riders vs. Dai-Shocker
| film name = {{Film name| kanji = 劇場版 仮面ライダーディケイド オールライダー対大ショッカー
| romaji = Gekijōban Kamen Raidā Dikeido: Ōru Raidā tai Daishokkā}}
| image          = Decade-Shinkenger.jpg
| alt            = 
| caption        = Film poster for both Kamen Rider Decade: All Riders vs. Dai-Shocker and  
| director       = Osamu Kaneda
| writer         = Shōji Yonemura Toei
| starring       = Masahiro Inoue Ryouta Murai Kanna Mori Tatsuhito Okuda Kimito Totani Renji Ishibashi Ren Osugi Moe Arai Tetsuo Kurata Ryuichi Oura Toshiki Kashu Gackt
| music          = Toshihiko Sahashi Shuhei Naruse Toei Co. Ltd
| released       =  
| runtime        = 66 minutes 79 minutes (Directors Cut)
| language       = Japanese
}}
  is the film adaptation of the 2009  . The catchphrases for the movie are   and  . In its first week in theaters, it opened at the #1 spot, edging out   (#3),  earning over 2 billion yen (approx. US$21M) in box office revenues.  It is the highest grossing Kamen Rider film to date.

==Production and casting== Kotaro Minami Shouichi Tsugami. New actors portraying old redesigned characters include Renji Ishibashi as Doctor Shinigami and Ren Osugi as Ambassador Hell. The film also features Moe Arai as Tsukasas sister Sayo Kadoya and Ryuichi Oura as Nobuhiko Tsukikage, Decades incarnation of Shadow Moon. Filming began in May 2009. Telebikun Magazine June 2009 

Gackt, also celebrating his 10th anniversary as a solo artist, released "The Next Decade"  for the films theme song, as he had released "Journey through the Decade" as the theme song for the television series.  Gackt also appears in the film as Riderman|Jōji Yūki. Telebikun Magazine, August 2009 
 Kamen Rider Double. 

To promote the film, Ren Osugi and Renji Ishibashi appeared as their characters in Tokyo promoting themselves as members of the Dai-Shocker Party, tying in with the House of Representatives of Japan election of 2009.  Also for the film, 7-Eleven stores in Japan have contests to win All Riders vs. Dai-Shocker collectibles, in addition to selling special Kamen Rider Decade merchandise with Ganbaride cards and Heisei Rider 10th Anniversary merchandise. 
Also aired on Indosiar.

==Plot==
 

The Hikari Studio arrives in a new A.R. World where, after seeing that Tsukasas photos of this world are coming out perfectly, Natsumi realizes they may be in Tsukasas home world. Tsukasa realizes he remembers the mansion depicted on the backdrop that took them here. Finding the manor, Tsukasa opens the door as he Natsumi and Yuusuke meet a girl named Sayo,who identifies Tsukasa as her older brother. As Sayo learns that Tsukasa lost his memories, revealing how he left her a year ago, her caretaker Nobuhiko Tsukikage arrives and confronts him about leaving his sister alone for so long. But when the Nine Worlds fusion starts to influence the World of Decade, Tsukikage reveals that Tsukasas journey has now enabled a solution to the crisis by determining the strongest Kamen Rider in existence: The Rider Battle. When his memories return, Tsukasa gives Tsukikage the clearance to make it happen. Three days later, after numerous matches, it comes down to a 6-man tag team match between the team of Decade, Kuuga, and Diend vs. V3, BLACK, and Super-1. With Diend running off and Kuuga wounded, Decade is forced to defeat all three on his own. Upon victory, a castle emerges from the stadium with Yusuke and Natsumi finding themselves in the throne room surrounded by various monsters as they are welcomed to Dai-Shocker before Eijiro appears as he suddenly reveals himself to be Doctor Shinigami, toasting to their leaders return: Tsukasa. Revealing all of their actions were to ensure Dai-Shocker would achieve its goal of dominion of all the saved worlds, Tsukasa sends the horrified Natsumi and Yusuke into the sewers.

The two escape to the Kadoya mansion, learning of Tsukasas childhood from Sayo before learning that she and Tsukikage are with Dai-Shocker as the latter becomes Shadow Moon while the former uses her Stone of the Earth on Yusuke, unleashing the greatest and most dangerous of Kuugas powers within him. Yusuke tells Natsumi to run away before the powers are unleashed. At DaiShockers castle, seeing the worlds still gathering, Tsukasa learns from Tsukikage that the Battle Fight was a farce as Sayo arrives, declaring herself High Priestess Bishium as Tsukikage reveals he outlived his purpose before dropping himself into the sewers, where he is attacked by Yusuke as Kuuga Rising Ultimate Form, under the control of Sayo. Tsukasa goes to the Hikari Studio to try to reconcile with Natsumi, who can no longer trust him for using her to achieve his ends. Tsukasa pleads with her as the Hikari Studio is now his only home, but Natsumi still refuses, as she is now alone in the world. The next day, Dai-Shocker begins its attack across the worlds starting with the World of Decade. Finding herself in the middle of it, Natsumi is saved by Narutaki into another conquered world before they run into Daiki who had disguised himself as one of the attacking Dai-Shocker Soldiers. Seeing hope in Daikis ability to travel across worlds, Narutaki sees the only way to possibly save the worlds is to gather the Kamen Riders.

Elsewhere, with no one to turn to, Tsukasa is confronted by Jōji Yūki who attempts to get revenge on him for having lost his arm while working for Dai-Shocker. He is about to finish Tsukasa off until he realizes that Tsukasa has changed and that he is now Dai-Shockers target. Yūki pulls off his arm, revealing its robotic replacement and allows Tsukasa to escape as he prepares his transformation into Riderman. Elsewhere, after an attempt to recruit Kamen Riders Ohja and Kick Hopper ends bad, Diend and Natsumi are attacked by a Dai-Shocker platoon under General Jark. Though he summons Raia, Gai, and Punch Hopper to distract their new enemies as he flees the area with Natsumi, Diend is cornered by Jark as Tsukasa arrives, now intent on fighting Dai-Shocker and saving his friends. With Diends help, Decade drives off the monsters as Jark is destroyed by Diend.
 Kamen Rider Double (Shotaro Hidari & Phillip) tips the scales as he takes out Shadow Moon on their own before taking his leave, pinning Shadow Moon into the castle. All of the other Riders join Decade in an All Rider Kick, destroying Shadow Moon and the Dai-Shocker castle. The resulting shock awakens King Dark as Diend arrives with Kamen Rider J. Using the Decadriver Final FormRide card, he turns Decade into the Jumbo Decadriver which then turns J into Decade Complete Form Jumbo Formation and he destroys King Dark with a variation of the Dimension Kick, using the other Riders as giant Kamen Ride Cards. After parting ways with the Riders, and making amends with Sayo as she begins her own journey, Tsukasa begins his true journey within the Hikari Studio, his true home, where Eijiro reappears as if nothing had happened.

==Characters==
  Nobuhiko Tsukikage who can transform into Shadow Moon.

==Internet spin-off films==
Much like with the  , Kamen Rider Decade: All Riders vs. Dai-Shocker will had a series of Internet and cellphone spin-off films titled  . Thirty of these short films are made, taking place on the  , with the first aired on July 17, 2009. 
There were main five different formats for the web movies: 
*  where topics like should Tackle count as a Kamen Rider are brought up to Toei to make it so.
*  to show off a Kamen Riders suit and explanation behind the design.
*  sports competitions between the Kamen Riders.
*  and   quiz shows with Tsukasa, Natsumi, Yusuke, Daiki as contestants while Kenichi Suzumura and Narutaki are the hosts with suit actor interviews.
*  where Decade teaches a random person how to assume a Rider transformation pose.

==Cast==
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  : Gackt
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  ,  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :   
*  :  
*  :  
*  :  
* Voice of other Kamen Riders and other Dai-Shocker members:  ,  ,  ,  , SATOSHI

==Theme song==
* "The Next Decade"
** Lyrics: Shoko Fujibayashi
** Composition: Ryo (of defspiral)
** Arrangement: Kōtarō Nakagawa, Ryo
** Artist: Gackt

==References==
 

==External links==
*   - Official movie website hub  
*   - Official movie website  
*   - Official net movie website  

 
 
 

 
 
 
 
 