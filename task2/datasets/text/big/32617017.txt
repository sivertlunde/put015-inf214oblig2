List of horror films of 1982
 
 
A list of horror films released in 1982 in film|1982.

{| class="wikitable sortable" 1982
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|- The Aftermath
| Steve Barkett || Sid Haig ||   ||
|- Alone in the Dark
| Jack Sholder || Jack Palance, Donald Pleasence, Martin Landau ||   ||  
|-
!   |   James Olson, Burt Young ||   ||  
|-
!   | The Appointment
| Lindsey C. Vickers || Edward Woodward, Jane Merrow ||   ||  
|-
!   | Bakterion
| Tonino Ricci || David Warbeck, Janet Agren, José María Labernié ||    ||
|- Basket Case
| Frank Henenlotter || Kevin Van Hentenryck, Terri Susan Smith, Beverly Bonner ||   ||  
|-
!   | The Beast Within
| Philippe Mora || Ronny Cox, Bibi Besch ||   ||  
|-
!   | Blood Song
| Robert Angus, Alan J. Levi || Frankie Avalon, Donna Wilkes, Dane Clark ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as:Dream Slayer.}}
|-
!   | Blood Tide
| Richard Jefferies || James Earl Jones, José Ferrer, Lila Kedrova ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as:Bloodtide (USA), Demon Island (USA) (TV title), The Red Tide.}}
|-
!   | Boardinghouse (film)|Boardinghouse
| John Wintergate || Kalassu, Alexandra Day ||   ||  
|-
!   | The Boogens
| James L. Conway || Fred McCarren ||   ||  
|-
!   | The Boogeyman
| Jeffrey C. Schiro || Bert Linder, Mindy Silverman, Bobby Persicelli ||   ||  
|- Cat People John Heard ||   ||  
|-
!   | Creepshow
| George A. Romero || Hal Holbrook, Adrienne Barbeau, Fritz Weaver ||   ||  
|-
!   | Curse of the Cannibal Confederates
| Tony Malanowski || Rebecca Bach, Christopher Gummer ||   ||  
|-
!   | Dark Sanity Martin Green || Aldo Ray, Kory Clark, Chuck Jamison ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as:Straight Jacket.}}
|-
!   | Death Screams David Nelson || Susan Kiger, Jennifer Chase, Andria Savio ||   ||  {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as:House of Death.}}
|-
!   | Dogs of Hell
| Worth Keeter || Earl Owensby, Bill Gribble ||   ||
|-
!   | Dont Go to Sleep Richard Lang  || Dennis Weaver, Valerie Harper, Ruth Gordon || || Television film 
|-
!   | La Villa delle anime maledette
| Carlo Ausino || Beba Loncar, Jean-Pierre Aumont, Annarita Grapputo ||   || {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as:House of the Cursed Spirits (Europe: English title)
, House of the Damned (International: English title).}}
|-
!   | The Dorm That Dripped Blood
| Stephen W. Carpenter, Jeffrey Obrow || Laurie Lapinski, Stephen Sachs, David Snow ||   ||  
|-
!   | Forbidden World Michael Bowen ||   ||  
|-
!   | Friday the 13th Part III
| Steve Miner || Dana Kimmell, Paul Kratka, Richard Brooker ||   ||  
|-
!   |   Tom Atkins, Stacey Nelkin, Dan OHerlihy ||   ||  
|-
!   | Honeymoon Horror Harry Preston || Paul Iwanski ||   ||
|-
!   | Hospital Massacre
| Boaz Davidson || Barbi Benton ||   ||
|-
!   | Humongous Paul Lynch David Wallace, John Wildman ||   ||  
|- Invitation to Hell
| Michael J. Murphy || Becky Simpson, Joseph Sheahan ||   ||
|-
!   | The Last Horror Film David Winters || Caroline Munro, Joe Spinell, Judd Hamilton ||   ||  
|-
!   | The Living Dead Girl
| Jean Rollin || Marina Pierro, Françoise Blanchard, Carina Barone ||   ||  
|-
!   | Madman (1982 film)|Madman
| Joe Giannone || Gaylen Ross ||   ||  
|-
!   | Manhattan Baby Christopher Connelly ||   ||  
|-
!   | The New York Ripper
| Lucio Fulci || Babette New, Jack Hedley, Zora Kerova ||   || Slasher film 
|- Next of Kin Tony Williams Alex Scott ||    || |  
|-
!   | Night Warning
| William Asher || Jimmy McNichol, Susan Tyrrell, Bo Svenson ||   ||  
|-
!   | Parasite (film)|Parasite
| Charles Band || Robert Glaudini, Demi Moore ||   ||  
|-
!   | Pieces (film)|Pieces
| Juan Piquer Simón || Christopher George, Lynda Day George, Frank Braña ||     ||  
|-
!   | Poltergeist (1982 film)|Poltergeist
| Tobe Hooper || Craig T. Nelson, JoBeth Williams, Beatrice Straight ||   ||  
|-
!   | Q (film)|Q
| Larry Cohen || Michael Moriarty, Candy Clark, David Carradine ||   ||  
|-
!   | Satans Mistress
| James Polakof || Britt Ekland, Lana Wood ||   ||  
|-
!   | The Sender Roger Christian || Kathryn Harrold, Zeljko Ivanek, Shirley Knight ||   ||  
|- The Slayer
| J.S. Cardone || Sarah Kendall, Frederick Flynn, Carol Kottenbrook ||   ||  
|-
!   | The Slumber Party Massacre
| Amy Holden Jones || Robin Stille ||   ||  
|-
!   | Tenebrae (film)|Tenebrae John Saxon, Daria Nicolodi ||   ||  
|- The Thing
| John Carpenter || Kurt Russell, Keith David ||   ||  
|-
!   | Unhinged (1982 film)|Unhinged
| Don Gronquist || Laurel Munson, Janet Penner, Sara Ansley ||   ||  
|-
!   | Variola Vera Goran Marković || Rade Šerbedžija, Erland Josephson, Rade Marković ||   ||
|-
!   | Visiting Hours
| Jean-Claude Lord || Michael Ironside, Lee Grant, Linda Purl ||   || {{hidden|multiline=y|fw1=normal
|1=Alternative titles
|2=Also known as:Get Well Soon, Terreur à lhôpital central (Canada: French title), The Fright .}} 
|}

==References==
 

==Citations==
 
*  <!--
-->
*  <!--
-->
 

 
 
 

 
 
 
 