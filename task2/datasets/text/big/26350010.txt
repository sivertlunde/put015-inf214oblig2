Miss You like Crazy (film)
{{Infobox film
| name           = Miss You Like Crazy
| image          = Mylc-main-poster-2.png
| caption        = Theatrical movie poster
| director       = Cathy Garcia-Molina
| producer       = Charo Santos-Concio Malou Santos
| writer         = Vanessa Valdez
| screenplay     = Vanessa Valdez Tey Clamor Juan Miguel Sevilla
| starring       =  
| music          = Jessie Lasaten
| cinematography = Manuel Teehankee
| editing        = Marya Ignacio
| studio         = Star Cinema
| distributor    = Star Cinema
| released       =  
| runtime        = 120 minutes
| country        = Philippines
| language       =  
| preceded_by    = 
| followed_by    = 
| budget         = 
| gross          = ₱143 Million  
}} 2010 Cinema Filipino romance film starring John Lloyd Cruz and Bea Alonzo. It was Star Cinemas post-valentine presentation for 2010. 

The film was released on United States in select theatres in Los Angeles, CA and in Honolulu, HI. 

== Plot==
The story is a flashback of the five years (2005 to 2010) of love affair involving the characters of Allan Alvarez (John Lloyd Cruz) and Mia Samonte (Bea Alonzo).  The opening scene was in a Pasig River ferry boat.  One of the passenger, Allan, was sad and confused if he really loved his then live-in partner, Daphne Recto (Maricar Reyes). While another passenger, Mia, was downtrodden by family problems. To express her heartaches, Mia would write messages on stones and would leave them anywhere, Allan picked up one of those, they got acquainted and their romantic story began.
 
Later, in one of their trysts they met an old man ( ) in Paco Park who predicted that they were meant for each other and would end up together although it would take a difficult five-year ride.   
 
Allan was torn between two loves. Although he knew that he loved Mia more, he procrastinated in his choice.  Mia left for Malaysia.  Two years after, when Allan  finally broke free from his indecision, he went to Malaysia to look for Mia only to find out that she was already engaged to another guy.  It was now Mias turn to make a choice.  She chose the new guy who loved her so much even though she honestly knew in her heart that she still loved Allan. This move is due to the fact that Mia can easily support her family with the new guy who is super rich than Allan who is now dirt poor.
 
Allan did not lose hope.  He patiently waited for Mia for another three years.  He firmly believed that she would come back to him as predicted by the old man earlier in the story.  True enough, the Malaysian guy let Mia go as he was aware of who Mia truly wanted and that he is tired of supporting her and her extended family.  On the very same date foreseen by the old man, Mia returned to the Philippines, saw Allan waiting for her, and embraced each other.

==Cast==
*John Lloyd Cruz as Alan Alvarez
*Bea Alonzo as Mia Samonte
*Maricar Reyes as Daphne Recto  Gerald Hans Isaac as Mi
*Ryan Eigenmann as Nick
*Ina Feleo as Lianne
*Ketchup Eusebio as Jona
*Tirso Cruz III  as Ramon Recto
*Bembol Roco as Efren Samonte
*Maritess Joaquin as Agnes Recto
*Sylvia Sanchez as Sol Samonte
*JM De Guzman as JM Recto (as Juan Miguel de Guzman)
*Diane Medina as Anette Samonte
*Sabella Bte Mustapha Kamal as Young Azrina
*Salsabil Bte Mustapha Kamil as Child Azrina
*Jins Shamsuddin as Mirs Grandfather (as Tan Sri Jins Shamsuddin)
*Neil Coleta as Micoy Samonte
*Patrick Moreno as Gbert Samonte
*Jane Oineza as Karen Samonte (as Elizabeth Jane Oineza)
*  as Ulysses
*Sid Lucero as Stephan
*Justin Cuyugan as Aries
*Irene Contreras as Nicks Sister
*Ed Bouffard as Nicks Father (as Eduardo Bouffard)
*  as Temi
*Malou de Guzman as Cristy
*Harish as Party Guest (uncredited)

== Reception==
===Soundtrack===
The film had two versions of the song, Miss You Like Crazy by Natalie Cole. The main version was sung by Erik Santos, while the other was sung by Aiza Seguerra.

===Gross===
According to Star Cinema, the film opened with a ₱18 Million gross. In a span of five days, Miss You Like Crazy has grossed more than ₱114 million.  According to Box Office Mojo, the film already grossed up to $3,183,529 (or ₱143 Million) in its six weeks of running. 

Art Directors: Lesley Anne Padilla & Gino Valloyas
Set directors: Eirenne Reyes & Erwin Abdon
Props master: Ryan Sabio

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2011
| rowspan="2" align="left"| GMMSF Box-Office Entertainment Awards  
| align="left"| Film Actor of the Year
| align="center"| John Lloyd Cruz
|  
|- Film Actress Bea Alonzo|| 
|}

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 