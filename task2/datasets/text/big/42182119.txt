Haalu Jenu
{{Infobox film
| name           = Haalu Jenu
| image          = 
| image_size     = 
| caption        = 
| director       = Singeetham Srinivasa Rao
| producer       = Parvathamma Rajkumar Singeetam Srinivasa Rao
| writer         = Singeetham Srinivasa Rao   Chi. Udaya Shankar (screenplay)
| narrator       =  Rajkumar Madhavi Madhavi Roopa
| music          = G. K. Venkatesh
| cinematography = S. V. Srikanth
| editing        = P. Bhaktavatsalam
| studio         = Poornima Enterprises
| distributor    = 
| released       = 1982
| runtime        = 157 min
| country        = India
| language       = Kannada
| budget         =
}}
 Kannada film Madhavi and Roopa Devi in lead roles.  The movie is famous for its evergreen songs which was composed by G. K. Venkatesh.  The film was a major success at the box-office upon its release.

==Cast== Rajkumar  Madhavi 
* Roopa Devi
* Thoogudeepa Srinivas
* Chi. Udaya Shankar
* Shivaram
* Musuri Krishnamurthy
* Shakti Prasad
* M. S. Umesh

==Soundtrack==
{{Infobox album  
| Name        = Haalu Jenu
| Type        = Soundtrack
| Artist      = G. K. Venkatesh
| Cover       = 
| Released    =  1982
| Recorded    = Feature film soundtrack
| Length      =
| Label       = Sangeetha
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| Track # || Song || Singer(s) || Lyrics
|-
| 1 || Aaneya Mele  || Rajkumar (actor)|Rajkumar, Sulochana || Chi. Udaya Shankar
|-
| 2 || Haayagi Kulithiru Neenu || Rajkumar, Saritha || Chi. Udaya Shankar
|-
| 3 || Baalu Belakayithu || Rajkumar ||Chi. Udaya Shankar
|-
| 4 || Haalu Jenu Ondada Haage  || Rajkumar || Chi. Udaya Shankar
|-
| 5 || Pogaadirelo Ranga  || S. Janaki || Purandara Dasa 
|-
|}

==Awards==
* Karnataka State Film Awards
# Best Film - Parvathamma Rajkumar and Singeetam Srinivasa Rao Rajkumar
# Best Editing - P. Bhaktavatsalam

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 

 