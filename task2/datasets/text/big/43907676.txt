Natale a New York
{{Infobox film
 | name = Natale a New York
 | image = 
 | caption =
 | director = Neri Parenti
 | writer =   Neri Parenti, Fausto Brizzi, Marco Martani, Alessandro Bencivenni, Domenico Saverni
 | starring =  Christian De Sica, Massimo Ghini, Fabio De Luigi, Sabrina Ferilli, Claudio Bisio
 | music = Bruno Zambrini
 | cinematography = Daniele Massaccesi
 | editing =   Luca Montanari Luigi De Laurentiis 
 | released =  
 | country = Italy
 | language  = Italian
 | runtime = 111 min  
 }}
Natale a New York (Christmas in New York) is a 2006 Italian comedy film directed by Neri Parenti, with Christian De Sica and Massimo Ghini.

==Plot==
During the holidays of Christmas, three groups of funny characters depart from Italy to spend the Christmas season in New York. Camillo is a penniless pianist who meets the fortune marrying a rich woman. However Camillo, although the happy living in comfort with a dependent daughter (Canalis), is still in love with Barbara, a Roman working with whom he had an affair. Barbara, however, is now married to Claudio, but this is segretely in love with the daughter of Camillo. 

Dr. Severino Benci ships to New York his assistant Filippo, with a mandate to deliver a gift to his son Francesco, who studies in the most important university in the city. But Francesco is a pleasure-loving slacker who opens an illegal trade of joints. Filippo covers both he and his friend Paolo, and hes having fun with them, while falls in love with a beautiful pompom girl. But Filippo shouldld marry with the love of his life...

==Cast==
*Christian De Sica: as Camillo "Lillo" Ferri
*Sabrina Ferilli: as Barbara Ricacci
*Massimo Ghini: as Claudio Ricacci
*Fabio De Luigi: as Filippo Vessato Paolo Ruffini: as Paolo Benci
*Francesco Mandelli: as Francesco Benci
*Alessandro Siani: as Lello
*Claudio Bisio: as Dott. Severino Benci
*Elisabetta Canalis: as Carlotta Ferri
*Fiorenza Marchegiani: as Milena Ferri
*Amanda Gabbard: as Mary
*Erika J. Othen: as Sylvia
*Pamela Paul: as Marys mother
*Roy Yeager: as Marys father
*Jesse Buckler: as Director Roosevelt Hotel
*Eric Bright: as George
*William J. Vila: as the taxi driver
*Bob Senkewicz: as the Doctor
*Michael Muldoon: as Detective John Palmer
*Peter Stoehr: as priest
==Soundtrack== Steve Edwards - World, Hold On (Children of the Sky) (Radio Edit) Michael Gray feat. Shelly Poole - Borderline (Michael Gray)|Borderline (Radio Edit)
# Lily Allen - Smile (Lily Allen)|Smile (Radio Edit)
# The Kooks - Naïve
# Corinne Bailey Rae - Put Your Records On
# Seth Lakeman - Lady of the Sea (Hear her Calling) (New Radio Version)
# Montefiori Cocktail - Crazy (Radio Edit)
# Kenny - I Love New York
# The House Keepers - Feel Da Feeling (Radio Edit)
# Karis - ke feat. Groove In Mind - Dancer
# T-bone - And I Feel Like
# Roselyn - Music In Me
# Big apple - In The City (House Mix) Resound - La mia faccia inutile (Lei che ride) (Edit)
# Paolo Sandrini - Green Sea

==External links==
*   su Internet Movie Database