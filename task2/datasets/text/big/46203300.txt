Peace, Perfect Peace
{{Infobox film
| name           = Peace, Perfect Peace
| image          =
| caption        =
| director       = Arrigo Bocchi
| producer       = 
| writer         = Kenelm Foss   Charles Vane 
| cinematography = 
| editing        = 
| studio         = Windsor Films
| distributor    = Walturdaw 
| released       = December 1919
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama Armistice that ends the First World War allows French and British soldiers to return home.

==Cast==
*   Hayford Hobbs as Poilu  
* Mary Odette as Marie Odette  
* Mary Marsh Allen as Mrs. Atkins   Charles Vane as Pensioner  
* Bert Wynne as Tommy Atkins 
* Evelyn Harding as Mother  
* Chubby Hobbs as Child  

==References==
 

==Bibliography==
* Low, Rachael. The History of British Film, Volume III: 1914-1918. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 