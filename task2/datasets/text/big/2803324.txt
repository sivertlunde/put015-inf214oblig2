The Best of The Beta Band
{{Infobox album  
| Name        = The Best of The Beta Band
| Type        = greatest
| Artist      = The Beta Band
| Cover       = The Best of The Beta Band.jpg
| Released    = 3 October 2005
| Recorded    = 1997–2004
| Genre       = Folk rock, Psychedelic rock, Trip-Hop, various
| Length      =  Regal
| Producer    = The Beta Band, Colin "C-Swing" Emmanuel
| Last album  = Heroes to Zeros (2004)
| This album  = The Best of The Beta Band - Music (2005)
| Next album  = 
}}

{{Album ratings
| rev1      = Allmusic
| rev1Score =   
| rev2      = NME
| rev2Score = (favourable) 
| rev3      = Pitchfork Media
| rev3Score = (7.2/10) 
| rev4      = PopMatters
| rev4Score = (7/10) 
| rev5      = Robert Christgau
| rev5Score = C+ 
| rev6      = Rock City
| rev6Score =    Stylus
| rev7Score = B+ 
| rev8      = The Times
| rev8Score =   
}}
 CD and EPs and three albums released by the band in its seven-year lifespan.

==Music track listing==
===CD one===
The best of the studio recordings, as selected by the band
# "Dry the Rain"
# "Inner Meet Me"
# "She’s the One"
# "Dr. Baker"
# "It’s Not Too Beautiful"
# "Smiling" (edit)
# "To You Alone"
# "Squares"
# "Human Being" (radio edit)
# "Gone"
# "Broke" (radio edit)
# "Assessment" (radio edit)
# "Easy"
# "Wonderful"
# "Troubles"
# "Simple"

===CD two===
Live at Shepherds Bush Empire, 30 November 2004
# "It’s Not Too Beautiful"
# "Squares"
# "Inner Meet Me"
# "Simple"
# "She’s the One"
# "Easy"
# "Dr. Baker"
# "Dry the Rain"
# "Quiet"
# "Broke"
# "Assessment"
# "Dogs Got a Bone"
# "House Song"
{{Infobox film
| name           = The Best of The Beta Band
| image          = The Best of The Beta Band (Film).jpg
| image_size     =
| caption        =
| director       =
| producer       =
| writer         =
| narrator       =
| starring       = The Beta Band
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = English
| budget         =
}}

==Film==
===Trailers===
# "Chalk and Cheese"
#* Directed by John Maclean
# "King Rib"
#* Directed by Pete Rankin, Steve Mason
# "Highland Fidelity"
#* Directed by John Maclean
# "Old Jock Radio"
#* Directed by Pete Rankin

===Promos===
# "Inner Meet Me"
#* Directed by John Maclean
# "Los Amigos Del Beta Bandidos"
#* Directed by The Beta Band
# "Dance Oer The Border"
#* Directed by John Maclean
# "Smiling"
#* Directed by Mark Szaszy and Corinne Day
# "Brokenupadingdong"
#* Directed by Josh Eve
# "Squares"
#* Directed by John Maclean
# "Al Sharp"
#* Directed by John Maclean
# "Assessment"
#* Directed by John Maclean and Robin Jones
# "Lion Thief"
#* Directed by Andy Cranston
# "Wonderful"
#* Directed by Nina Chakrabarti
# "Trouble"
#* Directed by John Maclean
# "Out-Side"
#* Directed by Robin Jones and John Maclean
# "Rhododendron"
#* Created by Robin Jones
# "Country Bird"
#* Directed by Steve Mason and Pete Rankin
# "Simple"
#* By Andrew Keller
# "Weirds Way"
#* Directed by Steve Mason and Pete Rankin
# "Remote Troubles"
#* Directed by John Maclean and Robin Jones

===Documentaries===
* The Depot To Monte Cristo
** Directed by Sam Tyler
* 1997–2004
** Directed by John Maclean
* Let It Beta
** Directed by Pete Rankin

===Live at the Shepherds Bush Empire===
Live at Shepherds Bush Empire, 29 November 2004
# "Inner Meet Me"
# "Dry The Rain"
# "Assessment"
# "Broke"

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 