Warriors of Virtue
{{Infobox film
| name           = Warriors of Virtue
| image          = Warriors of virtue.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Promotional poster
| director       = Ronny Yu Dennis Law Christopher Law Jeremy Law Patricia Ruben
| writer         = 
| screenplay     = Michael Vickerman Hugh Kelley
| story          = 
| based on       =  
| narrator       =  Doug Jones Don Davis
| cinematography = Peter Pau David Wu
| studio         = China Film Co-Production Corporation Law Brothers Entertainment
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 101 minutes
| country        = United States China
| language       = 
| budget         = $35 million
| gross          = $3.2 million
}}

Warriors Of Virtue is a 1997 Chinese-American  , was made in 2002.

==Plot==
Ryan Jeffers suffers a disability to his leg, preventing him from trying out for sports and fitting in with other kids at school. He is currently the waterboy of his schools football team and has a crush on the quarterback Brads girlfriend. He often seeks escape through comic books and dreams of adventure, hiding the depression of his disability from his mother. One day, the owner of his favorite restaurant, his friend Ming gives him a manuscript of Tao, representing the five elements; Earth, Fire, Water, Wood and Metal, and advises Ryan to live his life no matter his physical limits. That night, Ryan and his best friend Chucky are approached by Brad and his friends and they suggest an initiation for their group. Leading them to a water plant, Ryan is told he needs to cross a narrow pipe in order to sign his name on a wall of graffiti. Ignoring Chuckys protests, Ryan attempts to cross the pipe, but a water pipe opens up and throws him into the water.

Ryan wakes in a strange forest and is attacked by assailants who are drawn off by a creature from the lake. He screams and runs in fear, but soon realizes his leg works. He meets a dwarven man named Mudlap, who leads him to a beautiful girl named Elysia who tells him that he is in Tao; Ryan tells her about the manuscript, which had been stolen with his backpack. Believing it to be the Manuscript of Legend, Elysia takes Ryan to Master Chung and he meets four of the five warriors; anthropomorphic kangaroos each representing an element: Lai; Warrior of wood, Chi; Warrior of Fire, Tsun; Warrior of Earth; and Yee, the Warrior of Metal. He is told that Yun; the Warrior of Water had left them following an earlier conflict and Ryan thinks that the creature that saved him is Yun and he has the manuscript. He is told that the manuscript would be sought by Kemodo; a man who betrayed the Warriors and is stealing from the Lifesprings of Tao in order to stay young forever, and that they are protecting the last spring. While talking to Elysia, Ryan is captured by General Grillo, but is saved by Yun who admits he doesnt have the book, leading Ryan to believe Kemodo has it and he convinces Yun to return to the Lifespring.

Yun, Yee and Chi go after the manuscript and fall into a trap after being betrayed by Elysia, who joined Kemodo as vengeance against Yun for killing her brother by accident. They are nearly killed in a trap, but narrowly escape using their skills and they return to the Lifespring to prevent Kemodo from ambushing the others. Kemodo attempts to kidnap Ryan but instead fights Chung; the battle is brutal, but Chung is defeated and killed by Kemodo. Ryan flees, wanting to return home, but Mudlap leads him into Grillos arms and he is kidnapped. After waking, Elysia explains of Yun killing her brother and tries to convince him to read from the book so that Kemodo could possibly invade his world for more Lifesprings. Ryan realizes he cant read the book and this upsets Kemodo who tries to strike Ryan down, Elysia interferes and is killed by another of his henchmen; Barbarocious, whom he kills in rage as Ryan escapes. Kemodo, now growing unhinged returns to the Lifespring and challenges the Warriors to one-on-one combat, splitting into five versions of himself. He taunts and defeats the warriors while Ryan; after getting an apology from Mudlap on his betrayal finds an inscription in the manuscript. Facing Kemodo and taunting him, Ryan tricks Kemodo into using his power on him, weakening him so that the warriors can use their powers to purify his spirit, reforming him to a kind man looking for his home. Ryan, now mortally wounded is surrounded by his friends and Yee astonishes everyone by thanking Ryan, speaking for the first time in many years.

Suddenly, Ryan is back at the water plant before crossing the pipe. Realizing his desperation to fit in led to his accident, he changes it this time, refusing to go through with it. The water pipe opens like it did before, trapping Brad on the other side, but his insults to his friends only prompt them to leave him behind for the police to find. That night at home, Ryan apologizes to his mother for an earlier argument. When he goes to bed, he offers to tell his dog about Tao.

==Cast==
* Mario Yedidia - Ryan Jeffers
* Angus Macfadyen - Komodo
* Marley Shelton - Elysia Doug Jones - Yee: Warrior of Metal (Virtue of Righteousness)
* Chao-Li Chi - Master Chung Jack Tate - Yun: Warrior of Water (Virtue of Benevolence)
* Don W. Lewis - Lai: Warrior of Wood (Virtue of Order), Major Keena
* J. Todd Adams - Chi: Warrior of Fire (Virtue of Wisdom)
* Adrienne Corcoran - Tsun: Warrior of Earth (Virtue of Loyalty)
* Michael J. Anderson - Mudlap
* Tom Towles - General Grillo
* Lee Arenberg - Mantose
* Dennis Dun - Ming
* Roy Cebellos - Willy Beast
* Jason Hamer - Mosely
* Teryl Rothery - Kathryn Jeffers
* Ricky DShon Collins - Chucky
* Michael Dubrow - Brad
* Ying Qu - Barbarocious
* Stuart Kingston - Dullard
* Michael Vickerman - Dragoon Commander
* Adam Mills - Toby

===Voice cast===
* Scott McNeil - Yun: Warrior of Water (Virtue of Benevolence) Doug Parker - Yee: Warrior of Metal (Virtue of Righteousness), Chi: Warrior of Fire (Virtue of Wisdom)
* Kathleen Barr - Tsun: Warrior of Earth (Virtue of Loyalty) Dale Wilson - Lai: Warrior of Wood (Virtue of Order)
* Mina E. Mina - Master Chung
* Drew Reichelt - Dullard
* Venus Terzo - Barbarocious
* Colin Murdock - Dragoon Commander
* Jay Brazeau - Willy Beast Garry Chalk - Mosely
* Ian James Corlett - Major Keena
* Ward Perry - Villager
* Shane Meier - Toby Michael Dobson - Chi: Warrior of Fire (Virtue of Wisdom, uncredited)

==Reception==
Warriors of Virtue received overwhelmingly negative reviews from critics. Film critic Kale Klein of the Carlsbad Current-Argus was so physically distressed by the film that he actually vomited during the initial screenings.  On an episode of Siskel and Ebert, Gene Siskel voted thumbs down and described Warrior of Virtue as "Generic junk made for the international action market, a cheap hybrid of Power Rangers and Ninja Turtles." It currently has an 18% rating on Rotten Tomatoes based on 11 reviews. 

==Sequels== Nathan Phillips Kevin Smith (his final role) as Dogon, a villain bent on taking over our world and Tao.

==References==
 

==External links==
*   - Doug Jones as Yee
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 