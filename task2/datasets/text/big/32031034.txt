The Beggar Maid (film)
{{Infobox film
| name           = The Beggar Maid
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Herbert Blaché
| producer       = Lejaran A. Hiller Isaac Wolpe
| writer         = Reginald Denny
| screenplay     = 
| story          =  Tennyson poem Sir Edward painting  Reginald Denny Mary Astor 
| narrator       = 
| music          = 
| cinematography = Lejaran A. Hiller
| editing        = 
| studio         = Triart Picture Company  Paramount Pictures Corporation
| released       = September 1921
| runtime        = 2 reels
| country        = United States Silent
| budget         = 
| gross          = 
}}
 silent film Tennyson poem painting of Sir Edward Reginald Denny and Mary Astor.  

==Plot==
This short film is the story of the youthful, idealistic Earl of Winston: an aristocrat who is hopelessly in love with the title character. Shes the orphaned daughter of one of the Earls gardeners, who dutifully tends to her sick brother. Because of their different backgrounds, the Earl is unsure that a marriage will result in happiness. Will true love prevail?
 
==Cast== Reginald Denny as The Earl of Winston
*Mary Astor as the Beggar Maid

==Background==
 
The poem by Tennyson and the painting by Burne-Jones which were the inspiration for the film:
  
Her arms across her breast she laid;
 She was more fair than words can say;
 Barefooted came the beggar maid
 Before the king Cophetua.
 In robe and crown the king stept down,
 To meet and greet her on her way;
It is no wonder, said the lords,
She is more beautiful than day.

As shines the moon in clouded skies,
 She in her poor attire was seen;
 One praised her ankles, one her eyes,
 One her dark hair and lovesome mien.
 So sweet a face, such angel grace,
 In all that land had never been.
 Cophetua sware a royal oath:
This beggar maid shall be my queen!
  
:::by Lord Alfred Tennyson (1809-1892)
==References==
 
==External links==
* 

 
 
 
 
 
 
 
 
 


 