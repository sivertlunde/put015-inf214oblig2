Dil Ka Rishta
{{Infobox film
| name           = Dil Ka Rishta
| image          = Dilkarishta_dvdcover.jpg
| director       = Naresh Malhotra
| producer       = Aditya Rai
| writer         = Shabbir Boxwala Vrinda Rai
| budget       = 1 crore
| starring       = Arjun Rampal Aishwarya Rai
| music          = Nadeem-Shravan
| cinematography = Ashok Mehta
| editing        = Naresh Malhotra
| studio         = Tips Films box office = 50 crore
| distributor    = Target Films Tips Industries Limited
| released       = 17 January 2003
| runtime        = 151 mins
| country        = India
| language       = Hindi
}}

Dil Ka Rishta ( : دل کا رشتہ,  ) is a Bollywood movie, directed by Naresh Malhotra and produced by Aditya Rai, Aishwarya Rais brother. It was produced and distributed under Aishwaryas own Target Films banner and Tips Industries Limited. The film was released on 17 January 2003. The film stars Arjun Rampal and Aishwarya Rai in the lead roles, with Isha Koppikar, Rakhee and Paresh Rawal in supporting roles and Priyanshu Chatterjee making a special guest appearance. Dil Ka Rishta grossed 77,60,00,000 in India. It had a very good opening in England and it was the only Indian film at 12th position that time. 

==Plot==
Dil Ka Rishta is about a young man named Jai (Arjun Rampal). He is very wealthy and supports many charities. One day, he accompanies his friend Anita (Ishaa Koppikar) to a school for the deaf. There, he meets Tia (Aishwarya Rai), a teacher, and falls in love with her. He helps her wherever he can and tries to be near her—and he believes that Tia feels something for him, too. When he finally admits his feelings, Tia reveals the existence of her fiancé Raj (Priyanshu Chatterjee) and leaves Jai. Despite being betrothed, Jai continues to pursue Tia. Tia marries Raj and they have a son named Anshu. The trio live a poor 
but happy life together. Jai is devastated, since several attempts to win over Tias heart, fail. One night, being completely drunk, he drives home and causes an accident: Anita, who was with Jai in the car, dies immediately and so does Raj, who happened to be the driver of the other car. Tia herself survives but loses her memory.

The doctors believe that Tia will not be able to handle the return of her memories and advise Tias mother (Raakhee) to move her daughter to a place where nothing reminds her of her past. Jai wants to look after Tia, since he is responsible for her condition and offers to take Tia and her mother to South Africa to live with him so that she can recuperate. Tias mother accepts for Tias sake, though she loathes Jai for taking away her son-in-law and her daughters memories. Tia is told that Anita was her best friend, who died in a car accident, and Anshu is Jai and Anitas son. Tia is told that Anshu loves his "aunt" very much and that she should look after the boy so he wont miss his mother too much.

In Africa, Tias mother realizes Tias emerging feelings for Jai and warns him not to do anything inappropriate. Jai looks after Tia, but whenever she is trying to get close to him, he keeps his distance. Even as Jais father (Paresh Rawal) tries to set them up, Jai refuses her—even though he loves her more than anything else. Finally Tias mother accepts the fact that her daughter is in love with Jai and agrees so that Tia can be a mother to Anshu: they can get married. However Jai is still feeling guilty for the whole situation, 
Tia tried to get him to snap out of it, which leads to an altercation. Tia flees and Jai follows her. When she threatens to commit suicide Jai is forced to admit that Anshu is Tias son, Anita was only his friend, and that her husband died in the accident as well.

Jais father, who has finally arrived with Tias mother, explains that Jai is very much in love with Tia, but he hopes that she regains her memory and punishes him for what he did: killing her husband in the car accident. However Tia forgives Jai, telling him that the accident was her destiny, and if he had not crashed into her, someone else would have. She decides to leave Jai and takes Anshu with her. Jai stops her: He wants to be a father for Anshu, tells her that he loves her and asks her to marry him, which she accepts.

==Cast==
* Arjun Rampal as Jai
* Aishwarya Rai as Tia
* Priyanshu Chatterjee as Raj
* Isha Koppikar as Anita
* Raakhee as Mrs. Sharma (Tias mother)
* Paresh Rawal as Mr. Mehta (Jais father)

==Soundtrack==
{{Infobox album
|  Name        = Dil Ka Rishta
|  Type        = Soundtrack
|  Artist      = Nadeem-Shravan
|  Cover       = 
|  Released    = October 25, 2002
|  Recorded    = 2002 Feature film soundtrack
|  Length      =  Tips Music Films
|  Producer    = Nadeem-Shravan
|  Reviews     = 
|  Last album  = Jeena Sirf Merre Liye (2002)
|  This album  = Dil Ka Rishta (2002)
|  Next album  = Indian Babu (2003)
}}
{{Album ratings
| rev1 = Planet Bollywood
| rev1Score =      
}}
The music is composed by Nadeem-Shravan with lyrics by Sameer (lyricist)|Sameer. Manish Dhamija of Planet Bollywood in his review gave the album 7 stars out of 10. 

{| border="2" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track #!!Title !! Singer(s)!!Pictured on!!Length
|-
| 1
|"Saajan Saajan" Kumar Sanu, Alka Yagnik & Sapna Awasthi Arjun Rampal, Aishwarya Rai 
| 06:20
|-
| 2
|"Dil Ka Rishta" Kumar Sanu, Alka Yagnik & Udit Narayan Arjun Rampal, Aishwarya Rai, Priyanshu Chatterjee
| 05:04
|-
| 3
|"Dayya Dayya Dayya Re" Alka Yagnik Aishwarya Rai
| 04:43
|-
| 4
|"Haste Suraj Ki" Udit Narayan
|
| 01:32
|-
| 5
|"Dil Chura Le" Kumar Sanu & Alka Yagnik Arjun Rampal, Aishwarya Rai
| 04:48
|-
| 6
|"Haye Dil Mera Dil" Kumar Sanu & Alka Yagnik Arjun Rampal, Aishwarya Rai
| 04:27
|-
| 7
|"Kitna Majboor Ho Gaya Hu Main" Kumar Sanu
|
|01:35
|-
| 8
|"Saajan Saajan II" Jaspinder Narula, Kumar Sanu
|
| 05:27
|- 9
|"Dil Ka Rishta (Sad)" Babul Supriyo & Sarika Kapoor
|
| 04:59
|}

==References==
 

==External links==
*  

 
 
 
 
 
 