Sea Monsters: A Prehistoric Adventure
{{Multiple issues|
 
 
}}

{{Infobox Film 
| name           = Sea Monsters: A Prehistoric Adventure 
| image          = Sea Monsters - A Prehistoric Adventure Coverart.png
| caption        = DVD cover of Sea Monsters: A Prehistoric Adventure.
| writer         = Mose Richards
| narrator       = Liev Schreiber
| director       = Sean MacLeod Phillips 
| second director= David Bryant
| photography    = Liam Owen
| editing        = Jonathan P. Shaw National Geographic National Geographic Warner Bros. 
| released       = October 5, 2007 
| runtime        = 40 minutes Richard Evans, David Rhodes and Peter Gabriel
| country        =  English
| budget         = 
| gross          = 
| followed_by    = 
}}
 National Geographic ambient score. 3D and requires 3D glasses to view correctly.

==Plot== paleontologists discover a rare fossil in Kansas. The fossil was previously exposed by a summer rain, and it appears to be a marine reptile, tracing back over 70 million years.

The protagonist of the story is Dolly, a female Dolichorhynchops, who travels the Kansas Inland Sea, 80 million years ago during the late Cretaceous period with her family.

They see and encounter various creatures, including

*Tylosaurus
*Xiphactinus
*Cretoxyrhina
*Ammonite

She gets attacked by a shark (Cretoxyrhina) which kills her mother. Dolly survives with a tooth embedded in her flipper. Later, Dollys brother is swallowed whole by a young Tylosaurus, then an older Tylosaurus kills the younger one, leaving Dolly alone. Then she becomes a mother and has three young of her of own. After seasons of traveling around the Inland sea, Dolly dies peacefully of old age.

==Fossil finds in the film==

The film has cut to many fossil finds of the marine animals featured in the film.

*  discovered Dolichorhynchops, 95% of them juveniles.
 Permian Basin.
 Levi and George discover a 29-foot-long Tylosaurus.

*Phosphate Mine, Negev Desert, Israel, 1998: A quarry in Europe contained part a Tylosaurus fossil.
 George Sternberg, Charless older son, makes a discovery in Gove County, Kansas. A 13-foot-long Xiphactinus containing, below the ribs, a 6-foot-long fish, a Gillicus, which took up about half of the length of the Xiphactinus, killing it instantly.

*  shark.

*The Netherlands, 1998: A Dutch quarry reveals a Cretoxyrhina skeleton.

* , in Rapid City, South Dakota, reveal a Tylosaurus skeleton.

==Crew== director
* Liam Owen – photography
* Jonathan P. Shaw – edition

==Cast==
(In order of appearance)

* Liev Schreiber - narrator
* Jennifer Aguilar - Female grad student
* Albert J. Burnes - Male grad student
* Shari Phillips - Camping mother
* Charlotte Salem - Camping daughter
* Thea Phillips - Camping daughter (voice)
* Joe Salem  - Camping father
* James Rudnick - Australian scientist 1
* Kevin Stidham - Australian scientist 2
* C.J. Johnson - Road worker
* Kathleen Leighton - Texas collector 1
* Casey Myers - Texas collector 2 Charles Sternberg
* Doug Kisgen - Levi Sternberg George Sternberg
* Frank Novak - George Sternberg (60s)
* Michael McConnohie George Sternberg (voice)
* Daniel Coldham - Phosphate Mine worker 1
* Derek Gamble - Phosphate Mine worker 2
* Curtis Hawkins - Phosphate Mine worker 3
* Efi Kamara - Phosphate Mine worker 4
* Brian Lemmons - Phosphate Mine worker 5
* Nissam Mina - Phosphate Mine foreman
* Liam Owen - Photographer
* Walley Thornton - Reporter
* Paul W. Burmaster - North Dakota collector 1.
* Christopher Wheatly - North Dakota collector 2.
* Chris Koeberl - Netherlands scientist 1
* Jack van Dijk - Netherlands scientist 2
* Michael Ashcroft - South Dakota scientist 1
* Michael Sorich - South Dakota Scientist 1 (voice)
* Michael Ashcraft - South Dakota scientist 2

==Creatures featured==
Most of the creatures are later identified on the official website and the DVD.

*Ammonite
*Baculite (identified as "straight-shelled ammonites")
*Bananogmius, an extinct genus of bony fish
*Caproberyx, an extinct genus of bony fish
*Cretoxyrhina,a large shark
*Dolichorhynchops (often shortened to "dollies" in the story), a genus of plesiosaur and the main animal in the film.
*Enchodus, an extinct genus of bony fish
*Gillicus, a relatively small, 2-meter long ichthyodectid fish
*Gorgosaurus, a genus of tyrannosaurid theropod dinosaur
*Henodus (cameo), a placodont with an elaborate shell of the Late Triassic period
*Hesperornis, an extinct genus of flightless aquatic birds
*Inoceramus, an extinct genus of giant clam
*Jellyfish (live-acted)
*Kronosaurus (cameo),an extinct genus of short-necked pliosaur
*Leptecodon, a genus of prehistoric fish
*Nothosaurus (cameo),an extinct genus of sauropterygian reptile
*Platecarpus, an extinct genus of aquatic lizard belonging to the mosasaur family
*Protosphyraena, a fossil genus of swordfish-like marine fish
*Protostega, an extinct species of marine turtle
*Pteranodon, one of the largest pterosaur genera
*Squalicorax, a genus of extinct lamniform shark
*Styxosaurus, a genus of plesiosaur of the family Elasmosauridae
*Temnodontosaurus (cameo), a big ichthyosaur 
*Tusoteuthis, a genus of Cretaceous cephalopod molluscs
*Tylosaurus, a big mosasaur
*Uintacrinus (identified as "crinoid"), a floating colonial crinoid
*Xiphactinus, a 4.5 to 5 m (15 to 20 feet) long predatory bony fish

==Video game==
Sea Monsters: A Prehistoric Adventure is a video game from DSI Games and Zoo Digital Publishing, it was released on the Wii, PlayStation 2, and Nintendo DS on October 25, 2007. The game, developed by Destination Software, Inc. allows players to interact in a prehistoric world of sea creatures. Players can control Thalassomedon, Henodus, Temnodontosaurus, Tylosaurus, Dolichorhynchops and Nothosaurus. One of the main features is the whole adventure setting, it is almost completely a free-roam game with challenges and the most important task: to recover all the hidden fossils.

The game received poor reviews across all platforms.      Cheat Code Centrals Amanda L. Kondolojy considered the Wii version of the game to be conceptually interesting, but lacking in most other respects; in particular its imprecise and frustrating controls were singled out for criticism. 

==Soundtrack== National Geographic decided not to release the soundtrack, but two of the songs are available to download on the composers website and the end credits song, sung by Peter Gabriel ("Different Stories Different Lives") is also available to listen to at http://www.lunapendium.com/.

==Reception==
Despite the video games poor review, the film received rave reviews for its storyline, music and outstanding special effects. Review website Rotten Tomatoes reports the film has a List of films with a 100% rating on Rotten Tomatoes|100% "Fresh" rating from 12 positive reviews, something rarely seen on the website.

==See also==
*Walking with Dinosaurs
*Sea Monsters
*Walking with Monsters

==References==
 
http://www.nationalgeographic.com/seamonsters/

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 