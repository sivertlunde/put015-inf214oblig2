The Mirror (1975 film)
{{Infobox film
| name           = Mirror
| image          = Zerkalo.gif
| caption        = RUSCICO DVD cover
| director       = Andrei Tarkovsky
| producer       = Erik Waisberg
| writer         = Aleksandr Misharin Andrei Tarkovsky
| narrator       = Innokenty Smoktunovsky Arseny Tarkovsky
| starring       = Margarita Terekhova Ignat Daniltsev Larisa Tarkovskaya Alla Demidova Anatoli Solonitsyn Tamara Ogorodnikova
| cinematography = Georgi Rerberg
| music          = Eduard Artemyev
| editing        = Lyudmila Feiginova
| distributor    = 
| released       = 7 March 1975 (Soviet Union|USSR)
| runtime        = 107 minutes
| country        = Soviet Union
| language       = Russian
| budget         = SUR 622,000 {{cite book
  | last = Tarkovsky
  | first = Andrei
  | authorlink = 
  |author2=transl. by Kitty Hunter-Blair 
  | title = Time Within Time: The Diaries 1970–1986
  | publisher = Seagull Book
  | year = 1991
  | location = Calcutta
  | page = 77 (July 11, 1973)
  | url = 
  | doi = 
  | id = 
  | isbn = 81-7046-083-2}} 
| preceded_by    = 
| followed_by    = 
}}
 Russian art film directed by Andrei Tarkovsky. It is loosely autobiographical, unconventionally structured, and incorporates poems composed and read by the directors father, Arseny Tarkovsky. The film features Margarita Terekhova, Ignat Daniltsev, Alla Demidova, Anatoli Solonitsyn, Tarkovskys wife Larisa Tarkovskaya and his mother Maria Vishnyakova, with a soundtrack by Eduard Artemyev.
 oneiric images, stream of consciousness technique in Modernist literature. It has also found favor with most Russians for whom it remains their most beloved of Tarkovskys works. 

The main concept of Mirror dates as far back as 1964. Over the years Tarkovsky wrote several screenplay variants, at times working with Aleksandr Misharin. Their mutually developed script initially was not approved by the film committee of Goskino, and it was only after several years of waiting that Tarkovsky would be allowed to realize the project. At various times the script was known under different names, most notably Confession and A White, White Day. The completed film was initially rejected by Goskino, and after some delay was given only limited release in the Soviet Union. 

Mirror has grown in reputation over many years and ranked ninth in Sight and Sounds 2012 directors poll of the best films ever made.  

==Plot==
===Structure and content===
Mirror depicts the thoughts, emotions and memories of Alexei, or Alyosha (Ignat Daniltsev), and the world around him as a child, adolescent, and forty-year-old. The adult Alexei is only briefly glimpsed, but is present as a voice-over in some scenes including substantial dialogue. The structure of the film is discontinuous and nonchronological, without a conventional plot, and combines incidents, dreams and memories along with some news-reel footage. The film switches among three different time-frames: prewar (1935), war-time (1940s), and postwar (1960s or 70s).

Mirror draws heavily on Tarkovskys own childhood. Memories such as the evacuation from Moscow to the countryside during the war, a withdrawn father and his own mother, who actually worked as a proof-reader at a printing press, feature prominently.

===Synopsis===
The film opens with Alexeis adolescent son Ignat (also played by Ignat Daniltsev) switching on a televisor and watching the examination of a stammerer by a physician. After the opening titles roll, a scene is set in the countryside during prewar times in which Alexeis mother Maria (Margarita Terekhova) — also called Masha and Marusya — talks with a doctor (Anatoli Solonitsyn) who chances to be passing by. The exterior and interior of Alexeis grandfathers country house are seen. The young Alexei, his mother and sister watch as the family barn burns down. In a dream sequence Maria is washing her hair. Now in the postwar time-frame, Alexei is heard talking with his mother Maria on the phone while rooms of an apartment are seen. Switching back to the prewar time-frame, Maria is seen rushing frantically to her work-place as a proof-reader at a printing press. She is worrying about a mistake she may have overlooked, but is comforted by her colleague Liza (Alla Demidova), who then abruptly reduces her to tears with withering criticism. Back in postwar time, Alexei quarrels with his wife, Natalia (also played by Margarita Terekhova), who has divorced him and is living with their son Ignat. This is followed by news-reel scenes from the Spanish Civil War and of a balloon ascent in the U.S.S.R. In the next scene, set in Alexeis apartment, Ignat meets with a strange woman (Tamara Ogorodnikova) sitting at a table. At her request, Ignat reads a passage from a letter by Pushkin and receives a telephone call from his father Alexei. The strange woman vanishes mysteriously. Switching to war-time, the adolescent Alexei is seen undergoing rifle training with a dour instructor, intercut with news-reel footage of World War II and the Sino-Soviet border conflict. The reunion of Alexei and his sister with their father (Oleg Yankovsky) at wars end is shown. The film then returns to the quarrel between Alexei and his wife Natalia in the postwar sequence. Switching again to prewar time, vistas of the country house and surrounding countryside are followed by a dreamlike sequence showing a levitating Maria. The film then moves to the postwar time, showing Alexei apparently on his death-bed with a mysterious malady. The final scene plays in the prewar time-frame, showing a pregnant mother, Maria, intercut with scenes showing Maria young and old. (Old Maria is played by Tarkovskys own mother, Maria Vishnyakova.)

==Cast==
* Margarita Terekhova as the young Maria/Natalia
* Filipp Yankovsky as the child Alexei
* Ignat Daniltsev as the adolescent Alexei/Ignat
* Larisa Tarkovskaya as Nadezhda 
* Alla Demidova as Liza, Marias friend at printing house
* Anatoli Solonitsyn as Forensic doctor & pedestrian
* Tamara Ogorodnikova as Strange woman at the tea table
* Oleg Yankovsky, Alexeis father
* Maria Vishnyakova as the elderly Maria
* Innokenty Smoktunovsky as the adult Alexei (voice only)
* Arseny Tarkovsky as Narrator/Poet (voice only)

==Production==

===Writing=== Andrei Rublev. These episodes were published as a short story under the title A White Day in 1970. The title was taken from a 1942 poem by his father, Arseny Tarkovsky. In 1968, after having finished Andrei Rublev, Tarkovsky went to the cinematographers resort in Repino intending to write the script for The Mirror together with Aleksandr Misharin. This script was titled Confession and was proposed to the film committee at Goskino. Although it contained popular themes, for example, a heroic mother, the war and patriotism, the proposal was turned down. The main reason was most likely the complex and unconventional nature of the script. Moreover, Tarkovsky and Misharin clearly stated that they did not know what the final form of the film would be – this was to be determined in the process of filming. {{cite book
  | last = Tarkovsky
  | first = Andrei
  | authorlink = 
  |author2=edited by William Powell 
  | title = Collected Screenplays
  | publisher = Faber & Faber 
  | year = 1999
  | location = London
  | pages = 
  | url = 
  | doi = 
  | id = 
  | isbn = 
}}
 

With the script being turned down by the film committee, Tarkovsky went on to make the film Solaris (1972 film)|Solaris. But his diary entries show that he was still eager to make the film. Finally, the script was approved by the new head of Goskino, Filipp Ermash in the summer of 1973. Tarkovsky was given a budget of 622,000 Soviet ruble and 7500 metres (24,606 feet) of Kodak film, corresponding to 110 minutes, or roughly three takes assuming a film length of 3000 metres (10,000 feet). {{cite book
  | last = Tarkovsky
  | first = Andrei
  | authorlink = 
  |author2=transl. by Kitty Hunter-Blair 
  | title = Time Within Time: The Diaries 1970–1986
  | publisher = Seagull Book
  | year = 1991
  | location = Calcutta
  | page = 77 (July 11, 1973)
  | url = 
  | doi = 
  | id = 
  | isbn = 81-7046-083-2
}}
 

Several versions of the script for Mirror exist, as Tarkovsky constantly rewrote parts of the script, with the latest variant of the script written in 1974 while he was in Italy. One scene that was in the script but that was removed during shooting was an interview with his mother. Tarkovsky wanted to use a hidden camera to interview her on the pretext that it was research for the film. This scene was one of the main reasons why Vadim Yusov, who was the camera-man for all of Tarkovskys previous films refused to work with him on this film. {{cite book
  | last = Tarkovsky
  | first = Andrei
  | authorlink = 
  |author2=transl. by Kitty Hunter-Blair 
  | title = Time Within Time: The Diaries 1970–1986
  | publisher = Seagull Book
  | year = 1991
  | location = Calcutta
  | pages = 60–61 (September 17, 1972)
  | url = 
  | doi = 
  | id = 
  | isbn = 81-7046-083-2
}}
  
At various times, the script and the film was known under the titles Confession, Redemption, Martyrology, Why are you standing so far away?, The Raging Stream and A White, White Day (sometimes also translated as A Bright, Bright Day.). Only while filming Tarkovsky decided to finally title the film Mirror.  (The final film does indeed feature several mirrors with some scenes shot in reflection.)
 Andrei Rublev is seen on a wall.  Mirror thus forms the third part, together with Tarkovskys previous film Solaris (1972 film)|Solaris which was made in 1972 and which references Andrei Rublev by having an icon made by him being placed in the main characters room,  in a series of three films by Tarkovsky referencing Andrei Rublev.

===Casting===
Initially Tarkovsky considered Alla Demidova and Swedish actress Bibi Andersson for the role of the mother. In the end Margarita Terekhova was chosen. {{cite book
  | last = Tarkovsky
  | first = Andrei
  | authorlink = 
  |author2=transl. by Kitty Hunter-Blair 
  | title = Time Within Time: The Diaries 1970–1986
  | publisher = Seagull Book
  | year = 1991
  | location = Calcutta
  | page = 41 (August 20, 1971)
  | url = 
  | doi = 
  | id = 
  | isbn = 81-7046-083-2
}}
 

===Filming===
Filming began in September 1973 and ended in March 1974. The outdoor scenes were shot in Tutshkovo near Moscow. The indoor scenes were shot at the Mosfilm studio. {{cite book
  | last = Tarkovsky
  | first = Andrei
  | authorlink = 
  |author2=transl. by Kitty Hunter-Blair 
  | title = Time Within Time: The Diaries 1970–1986
  | publisher = Seagull Book
  | year = 1991
  | location = Calcutta
  | pages = 78, 92–93 (September 30, 1973 & March 8, 17, 1974)
  | url = 
  | doi = 
  | id = 
  | isbn = 81-7046-083-2
}}
 

The completed film was initially rejected by Filipp Ermash, the head of Goskino in July 1974. One reason given was that the film is incomprehensible. Tarkovsky was infuriated about this rejection and even toyed with the idea of going abroad and making a film outside the Soviet Union. Mirror was ultimately approved by Goskino without any changes in fall 1974. {{cite book
  | last = Tarkovsky
  | first = Andrei
  | authorlink = 
  |author2=transl. by Kitty Hunter-Blair 
  | title = Time Within Time: The Diaries 1970–1986
  | publisher = Seagull Book
  | year = 1991
  | location = Calcutta
  | pages = 96–97 (July 27, 29 & August 1, 1974)
  | url = 
  | doi = 
  | id = 
  | isbn = 81-7046-083-2
}}
 

==Distribution==
 
Mirror never had an official premiere and had only a limited, second category release with only 73 copies. Although it was officially announced for September 1975, it was shown as early as March 1975.

==Responses==
 
Despite very limited distribution, Mirror was well received by the audiences. Goskino did not allow it to be shown at the Cannes Film Festival. The managing director of the festival, Maurice Bessy, was sympathetic to Tarkovsky. Upon hearing that Mirror would not be allowed to be shown in Cannes, he unsuccessfully threatened to not take any other Soviet film. {{cite book
  | last = Tarkovsky
  | first = Andrei
  | authorlink = 
  |author2=transl. by Kitty Hunter-Blair 
  | title = Time Within Time: The Diaries 1970–1986
  | publisher = Seagull Book
  | year = 1991
  | location = Calcutta
  | pages = 106–109 (March 2, April 8, 11, 1975)
  | url = 
  | doi = 
  | id = 
  | isbn = 81-7046-083-2
}}
  In 2012, Will Self argued that The Mirror remains the most beautiful film ever made. {{Cite web
|url= http://thequietus.com/articles/09180-looking-in-looking-out-2012
|title= Looking In, Looking Out Film Festival 
|publisher= The Quietus date = 2012-06-27
|accessdate=2012-06-29}}
 

==Trivia==
* Wintertime scenes in the Mirror echo   and  , the latter of which is shown several times in Solaris (1972 film) |Solaris.
* The scene where the neighbour tries on earrings offered by the main heroine, references the painting Girl with a Pearl Earring by Johannes Vermeer. 
* A recitative from Johan Sebastian Bachs Johannes-Passion , ("Und siehe da, der Vorhang im Tempel...") features in the score, as does the opening chorus from the Johannes-Passion ("Herr, unser Herrscher, dessen Ruhm in allen Landen herrlich ist!") accompanying the films memorable closing scene.
* Another of Bachs works Das alte Jahr vergangen ist Chorale (BWV 614) from  Orgelbüchlein is heard during the opening credits. Andrei Rublev is seen on a wall. 

==Further Reading==
Mirror, Natasha Synessios; 2001, I.B.Tauris & Co Ltd
==References==
 

==External links==
*  
*  
*   at official Mosfilm site with English subtitles
*    
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 