The Custom Mary
{{Infobox film
| name = The Custom Mary
| image      = 
| caption = 
| director   = Matt Dunnerstick
| producer   = Becky Wang
| writer     = Matt Dunnerstick
| starring   = Alicia Sixtos James Jolly Travis Hammer Henry LeBlanc Dave Vescio Bill McKinney Eileen Dietz
| music      = Timo Chen
| cinematographer= Alison Kelly
| distributor= GoDigital
|released=  
| runtime    = 81 minutes Spanish
| music      = 
| budget     =
| gross      = 
}} Los Angeles, the film tells the story of young Latina in East Los Angeles, who meets an African-American Lowrider, and struggles to reconcile her faith and blossoming love affair while becoming dangerously involved in a religious attempt to clone Jesus.

==Plot==
Searching for purpose and meaning in the world, a young Latina in East Los Angeles becomes dangerously involved with a storefront church where a young minister enthralls her. At the same time, she meets Joe, a self-empowered African-American lowrider mechanic.

Mary, struggling to reconcile her faith and her blossoming love affair with Joe, is pulled into a group from the church who believes they can clone Jesus, with Mary’s help. Mary becomes pregnant, and as the day of birth approaches, both Joe and the preachers fight for her attention. Battling real and imagined truths, Mary begins a secret journey to the desert, seeking answers in a surprising and surreal way.

The Custom Mary is a combination of a classic, indie love story and an exploration of faith and fanaticism, set amidst the gritty urban world of lowrider culture. 

==Notables==
The film features one of the last performances of the late Bill McKinney.

Music by the Mexican indie band, Porter (band)|Porter, appears throughout the film.

==Exhibition and responses==
The film was first shown at the 2011 New York International Latino Film Festival and also at the 2012 San Diego Black Film Festival, where it simultaneously won Best Cutting Edge film and Best Religious Topic Film, in addition to being nominated for Best Director, Best Feature, and Best Overall Film, It went on to play through the indie film circuit, particularly at Latino and Black film festivals, winning 8 awards and 4 nominations in the process. In 2013, it was distributed in the US by GoDigital.

==Cast==
* Alicia Sixtos — Mary
* James Jolly — Joe
* Travis Hammer — Paul Jr.
* Henry LeBlanc — Paul Sr.
* Spencer Scott — Jesus (voice)
* Dave Vescio — Pat
* Graciela Schrieber — Law of the Lowriders
* Hale — Choir Mary
* Bill McKinney - The Silent Boss
* Eileen Dietz — Mother

==Cameos== Disinformation & current host of the online blog, Dangerous Minds, appears as a TV Reporter.
* Janina Gavankar, of The L Word and True Blood notoriety, appears as Ms. Rime.

==Review==
* On TheEspresso.com: "The Custom Mary is a welcome expanding of vision and a modern parable worthy of your time." 

== References ==
 

==External links==
*  
* 
* 

 
 
 
 
 