Così parlò Bellavista
{{Infobox film
| name     = Così parlò Bellavista
| image    =Cosi parlo Bellavista.jpg
| director = Luciano De Crescenzo
| producer = Mario Orfini
| story = Luciano De Crescenzo (novel)
| writer = Luciano De Crescenzo Riccardo Pazzaglia
| starring = Luciano De Crescenzo   Isa Danieli 
| cinematography = Dante Spinotti
| music = Claudio Mattone
| country = Italy
| language = Italian
| runtime = 105 minutes
| released =  
}}
Così parlò Bellavista is a 1984 Italian comedy film based on the novel of the same name by Luciano De Crescenzo. De Crescenzo directed the film and also played the main role. For this film De Crescenzo won David di Donatello and Nastro dArgento for  best new director, while Marina Confalone  won the same awards in the best supporting actress category.   

== Plot summary ==
In Naples, Professor Bellavista is a retired man, passionate about the philosophy and thought of Ancient Greece. He helds every day, in her luxurious apartment, his lessons of life to the poor-nothing (his friends), who are dazzled by his reasoning. One day, however, the quiet life of the building of Bellavista will be disturbed by the arrival of a director of Milan. Between Naples and Milan theres bad blood, because the Neapolitans are accustomed to enjoy a quiet life, always based on the "philosophy of pleasure and delay", while the Nordic countries are very strict and punctual.

== Cast ==
* Luciano De Crescenzo - Professor Bellavista
* Isa Danieli - Mrs Bellavista
* Lorella Morlotti - Patrizia
* Geppy Gleijeses - Giorgio
* Marina Confalone -  La cameriera 
* Renato Scarpa - Cazzaniga
* Antonio Allocca -  "Core Ngrato"
* Nunzio Gallo -  Camorrista
* Riccardo Pazzaglia -  Luomo del cavalluccio rosso

== References ==

  

== External links ==
* 

 
 
 

 