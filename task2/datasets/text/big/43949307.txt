Louisiana Territory (film)
{{Infobox film
| name           = Louisiana Territory
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Harry W. Smith Peter Scoppa (assistant)
| producer       = Jay Bonafield Douglas Travers
| writer         = Jerome Brondfield
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = George Bassman
| cinematography = Harry W. Smith
| editing        = Milton Sherman
| studio         = RKO Pictures|RKO-Pathé RKO Radio Pictures
| released       =    |ref2= }}
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Louisiana Territory is a 1953 American historical drama film directed by Harry W. Smith (who also photographed the film), from an original screenplay by Jerome Brondfield. Produced by RKO Pictures|RKO-Pathé, it was distributed by its sister company, RKO Radio Pictures, who premiered the film in New Orleans on October 14, 1953, with a national release two days later, on October 16. The film stars Val Winter, Leo Zinser, Julian Miester, and Phyliss Massicot.

==References==
 

 

 
 
 
 
 