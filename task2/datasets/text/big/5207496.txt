Hav Plenty
 
{{Infobox Film
| name           = Hav Plenty
| image          = Hav_Plenty_DVD.jpg
| director       = Christopher Scott Cherot
| producer       = Bridget D. Davis Christopher Scott Cherot Dana Offenbach Kenneth "Babyface" Edmonds Robyn M. Greene S.J. Cherot  Tracey Edmonds
| writer         = Christopher Scott Cherot
| narrator       = Christopher Scott Cherot
| starring       = Christopher Scott Cherot Chenoa Maxwell Reginald James Robinne Lee Tammi Katherine Jones Lisa Coleman Wendy Melvoin
| cinematography = Kerwin DeVonish
| editing        = Christopher Scott Cherot
| distributor    = Miramax Films
| released       = June 19, 1998 (U.S.)
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $65,000
| gross          = $2,284,034 
}}

Hav Plenty is a 1998 American independent film released by Miramax Films, based on an eventful weekend in the life of Lee Plenty (Christopher Scott Cherot), written and directed by Cherot. The film is based on the true story of Chris Cherots unrequited romance with Def Jam A&R executive Drew Dixon.

==Production==
Financing for the film came from Cherots time as a New York City cab driver,    and a third mortgage on his mothers home. Principal photography took eighteen days in and around New York City and New Jersey.

Upon completion of principal photography, Cherot was out of money again, and it took him almost a year to complete his edit and make a screening print of the film.   In May 1997, at his first "cast-and-crew screening" in a small screening room in New York City, Hav Plenty producer Robyn M. Greene by chance ran into Warrington Hudlin and Bill Duke in the lobby of the building and invited them up to view the film. Immediately after the screening, Hudlin invited Cherot to participate in the inaugural year of the Acapulco Black Film Festival, now the American Black Film Festival. Cherot accepted on the spot, and one month later, in June 1997, Hav Plenty was the opening night film in Acapulco, the first film at the first festival.   

After seeing Hav Plenty at the Acapulco Black Film Festival, Tracey Edmonds and Kenneth "Babyface" Edmonds offered to attach their names to the film and record a new soundtrack, consequently attracting an intense amount of media attention to what was previously a small, obscure independent movie. Three months later, after a screening at the Toronto International Film Festival in September 1997, Harvey Weinstein offered to buy Hav Plenty for an amount between $1.5 – $2.3&nbsp;million. The entire time that passed between Cherots first obscure screening in New York City to Weinsteins multi-million dollar handshake-deal in Toronto was four months. 

According to an interview with Chris Cherot,  Miramax wanted to give the movie a happier ending. They compromised by adding the "one year later" scene which shows a happier ending, while at the same time leaves room for argument that Hav and Lee didnt end up together.

After screenings at the Sundance Film Festival in January 1998 to start the official "buzz", Miramax theatrically released Hav Plenty in the United States on June 19, 1998, with worldwide distribution following afterward.

==Cast==
* Christopher Scott Cherot – Lee Plenty
*Betty Vaughn – Grandma Moore
* Chenoa Maxwell – Havilland Savage
*Chuck Baron – Mr. Savage
* Hill Harper – Michael Simmons
*Kim Harris – Bobby Montgomery
*Margie St. Juste – Alexandria Beaumont
*Reginald James – Felix Darling
* Robinne Lee – Leigh Darling
*Tammi Katherine Jones – Caroline Gooden

Cameo appearances by:
* Kenneth "Babyface" Edmonds – Lloyd Banks
* Lauryn Hill – Debra (Tru Love version of Caroline)
* Mekhi Phifer – Harold (Tru Love version of Felix)
* Nia Long – Trudy (Tru Love version of Havilland) Rozonda "Chilli" Thomas – Kris (Tru Love version of Leigh)
* Shemar Moore – Chris (Tru Love version of Lee)
* Tracey Edmonds – Amy Madison

==Critical reception==
Upon initial release, the film received near unanimous praise from major trades and newspapers. Stephen Holden (The New York Times) said, "With his self-deflating cool and amused insight into the shallowness of the buppie world in which he drifts, Lee is one of the most original and likable characters to pop up in a movie in quite a while."   Emanuel Levy (Variety) said, "Christopher Scott Cherot makes a splashy debut as writer, director, editor and star of this fresh, bittersweet, modern-day love story that recalls the early work of Woody Allen."     Duane Byrge of The Hollywood Reporter wrote, "Screenwriter-director Cherot has dished up a dicey, romantic riposte, stuffing it with the real makings of romantic comedy: individual insecurities, desires and fears."     Lisa Schwarzbaum (Entertainment Weekly) wrote: "  may be new to the movie game, but he announces himself with such confidence and force of personality, you know a noteworthy talent has arrived."     Kevin Thomas at The Los Angeles Times observed, "The pleasure in watching Hav Plenty comes from seeing Cherot discover the possibilities of the medium as he goes along... As it unfolds, repartee gives way to an increasing sense of the visual, and by the time the film is over, Cherot has discovered how potent   can be in repose..".  The San Francisco Chronicle remarked, "Hav Plenty harks back to a different temperament with considerable charm."     With the exception of Roger Ebert,    nearly all major reviewers were particularly impressed with Cherot’s absurdist, witty writing and personable, on-screen charisma; Entertainment Weekly included Cherot on its year-end "It-List".   

==Awards & nominations== 1997 Acapulco Black Film Festival
*Best of Festival – Christopher Scott Cherot (winner)
 1998 Sundance Film Festival
*Grand Jury Prize, Dramatic – Christopher Scott Cherot (nominated)
 1999 Acapulco Black Film Festival
*Best Screenplay – Christopher Scott Cherot (winner)

==Soundtrack==
 
A soundtrack containing hip hop and R&B music was released on June 9, 1998 by Sony Music Entertainment. It peaked at No.&nbsp;39 on the Billboard 200|Billboard 200 and No.&nbsp;6 on the Top R&B/Hip-Hop Albums.

==References==
  

==External links==
*  
*  

 
 
 
 
 