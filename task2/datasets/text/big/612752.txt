Wizards (film)
 
{{Infobox film
| name           = Wizards
| image          = Wizards poster.jpg
| caption        = Theatrical release poster
| director       = Ralph Bakshi
| producer       = Ralph Bakshi
| writer         = Ralph Bakshi
| narrator       = Susan Tyrrell Bob Holt Jesse Welles Richard Romanus David Proval Steve Gravers
| music          = Andrew Belling
| cinematography = Ted C. Bemiller
| editing        = Donald W. Ernst
| distributor    = 20th Century Fox
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = $2 million 
| gross          = $9,000,000
}} magic and one representing the forces of industrial technology. It was written, produced, and directed by Ralph Bakshi.   
 Fritz the cult classic.

==Plot== torturing small projector and reels of Nazi propaganda footage, which he uses first to inspire his own soldiers and later to distract the elves, whom his own people can then overcome. 

  loses the teleports Avatar and Elinore to a snowy mountaintop. Lost there, they are rejoined by Weehawk and Peace. They join the encamped army of an elf General preparing to attack Scortch One the following day, but Blackwolf launches a sneak attack that night, and a battle-tank approaches the camp. Peace fires on the tank, but Elinore kills him. She jumps into the tank and escapes as Avatar and Weehawk watch in confusion.
 controlling her mind ever since she first touched Peace. Blackwolf declares his magic superior to Avatars and demands his surrender; whereupon Avatar offers to show Blackwolf a trick he learned from their mother.  Avatar then pulls a pistol from his sleeve and kills Blackwolf. Upon Blackwolfs death, his castle collapses. With the projector destroyed, the mutants give up fighting; and with Montagars safety secured, Weehawk returns home, while Avatar and Elinore decide to start their own kingdom elsewhere.

==Cast== Bob Holt – Avatar, an old but powerful wizard
* Jesse Welles – Elinore, Avatars love interest
* Richard Romanus – Weehawk, a noble elf warrior
* David Proval – Necron 99/Peace, Blackwolfs former minion. He is renamed Peace by Avatar.
* Steve Gravers – Blackwolf,  Avatars evil brother
* James Connell – President
* Mark Hamill – Sean, king of the mountain fairies
* Susan Tyrrell – Narrator (uncredited)
* Ralph Bakshi – Fritz/Lardbottom/Stormtrooper (uncredited)
* Angelo Grisanti – Larry the Lizard (uncredited)

==Production==
 
Ralph Bakshi had long had an interest in fantasy, and had been drawing fantasy artwork as far back as 1955, while he was still in high school.    Wizards originated in the concept for Tee-Witt, an unproduced television series Bakshi developed and pitched to CBS in 1967.  In 1976, Bakshi pitched War Wizards to 20th Century Fox. Returning to the fantasy drawings he had created in high school for inspiration, Bakshi intended to prove that he could produce a "family picture" that had the same impact as his adult-oriented films.   
 allegorical comment Jews looking for a homeland, and about the fact that fascism was on the rise again".   
 Ian Miller pencil tests. Philippine government to leave for the United States until two months afterward, and later found that by the time he had arrived in the U.S., not only had the films animation had been completed, but Niños visa did not allow him to submit freelance work on any other projects. 
 Hey Good Lucas thought he should do it, and he got not only Wizards, he got Star Wars."    Bakshi had wanted a female narrator for his film, and he loved Susan Tyrrells acting. Tyrrell performed the narration for the film, but Bakshi was told that he couldnt credit her for her narration. Years later, Tyrrell told Bakshi that she got most of her work from her narration on the film, and that she wished she had allowed him to put her name on it.  
 Ian Miller, who produced the gloomy backgrounds of Scortch, and Mike Ploog, who contributed likewise for the more arcadian landscapes of Montagar. 
 Lord of El Cid, Battle of Alexander Nevsky for rotoscoping. Live-action sequences from Patton (film)|Patton were also featured. 
 Vaughn Bodes Ivan the Jerry Becks Animated Movie Guide, Andrew Leal writes that "The central figure, Avatar   sounds a great deal like Peter Falk, and clearly owes much to cartoonist Vaughn Bodés Cheech Wizard character." 

As War Wizards neared completion, Lucas requested that Bakshi change the title of his film to Wizards in order to avoid conflict with Star Wars, and Bakshi agreed because Lucas had allowed Mark Hamill to take time off from Star Wars in order to record a voice for Wizards. 

==Response and legacy==
Although Wizards received a limited release, it was very successful in the theaters that showed it, and developed a worldwide audience.  According to Bakshi, he was once interviewed by a German reporter who was unsure as to why the Nazi Swastika was used to represent war.  Bakshi said "I didnt get any criticism. People pretty much loved Wizards."    Film website Rotten Tomatoes, which compiles reviews from a wide range of critics, gives the film a score of 61%, considered "Fresh". 

Audio clips from the film have been sampled by  . 

20th Century Fox Home Entertainment responded to an online petition created by Animation on DVD.com and written by Keith Finch demanding the films release on DVD.      The disc, released on May 25, 2004, featured an audio commentary track by Bakshi and the interview segment Ralph Bakshi: The Wizard of Animation. Bakshi has stated that Wizards was always intended as a trilogy. One of the sequels was pitched to Fox, who have yet to greenlight the project. 

In late 2004, a Wizards II graphic novel went into production, produced by Bakshi. The stories will be from the Wizards "universe" and each story will be created by a different artist.  In September 2008, it was announced that Main Street Pictures would collaborate with Bakshi on a sequel to Wizards.  

20th Century Fox released a Special Edition Blu-ray Disc of the film on March 13, 2012, to commemorate the films 35th anniversary.  The disc includes the special features from the DVD, along with a 24-page book including rare artwork from the film and an introduction from Bakshi. 

==Sequel plans==
Bakshi mentioned he had plans for a sequel entitled Wizards 2 that involved the relationship between Avatar and Elinore. Bakshi said the plot would be "where   doesnt work out, and Weehawk gets in the way", The sequel was never developed due to production difficulties and the other projects on which Bakshi was then focused. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*   at the official Ralph Bakshi website.

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 