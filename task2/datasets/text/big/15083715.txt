Mantra (film)
{{Infobox film
| name           = Mantra
| image          =
| caption        =
| director       = Tulasi Ram
| producer       = Ravi Prakash & Kalyan Ram
| writer         = Ravi Prakash Sivaji  Charmy Kaur
| music          = Anand
| cinematography = Sivendra
| editing        = Upendra
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| gross          =
}}
Mantra ( ) is a 2007 Telugu thriller film starring  Sivaji (Telugu actor)|Sivaji, Charmy Kaur and Kausha. This film was directed by Tulasi Ram and produced by Ravi Prakash and Kalyan Ram.

==Plot==
The story revolves around a haunted farmhouse Mantra Nilayam, which is owned by Mantra (Charmi). She tries to sell the house but due to mysterious deaths at the house, nobody comes forward to buy it. A professor, however, agrees to buy the property on the condition that someone stay in the house for three months. Hero (Sivaji (Telugu actor)|Sivaji), a goon who looks after land settlements, is ready to stay in the house for the commission promised once the house is sold. The remainder of the film involves how Hero solves the mystery behind the deaths.

==Production==
* Production was formally launched at a function in Annapura studios on October 27, 2007. Regular shooting began November 2 and was completed in 40 working days.

* The setting for Mantra Nilayam was erected at Shamshabad exclusively and most of the film was shot there.

==Cast== Sivaji
* Charmy Kaur
* Kausha Rach
* Karuna Sri
* Chitram Seenu Vijay
* Jeeva
* Mallikharjuna Rao Rallapalli

==Release==
The film was released on December 14, 2007. Although most movies at the end of year could not do well, Mantra proved to be a success with 80 to 90% collections. Charmys popularity among the male youth came in as an added advantage to the movie.   Suspense thriller is not a popular genre in Telugu film industry but commercial success of the film  A film by Aravind changed the notion.  Idlebrain, a popular website on Telugu film industry gives the film a rating of 3/5.  Vijayanand Movies acquires the theatrical rights of the movie for worldwide except India.
Bhavani Media has acquired DVD rights. 

==References==
 

==External links==
* 
* 

 
 
 
 
 


 