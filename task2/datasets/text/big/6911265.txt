Man of the Year (1995 film)
{{Infobox Film
| name           = Man of the Year
| image          = MOTYfoto.jpg
| caption        = original poster
| director       = Dirk Shafer
| producer       = Matt Keener
| writer         = Dirk Shafer
| starring       = Dirk Shafer   Vivian Paxton   Bill Brochtrup   Beth Broderick
| music          = Peitor Angell   Barry Stich   Eric Vetro
| cinematography = Stephen Timberlake
| editing        = Barry S. Silver   Ken Solomon
| distributor    = Seventh Art Releasing
| released       = September 15, 1995 (Toronto Film Festival)
| runtime        = 86 minutes
| country        =  
| awards         =  English
| budget         = 
| gross          = $209,935 
| preceded_by    = 
| followed_by    = 
}}
 The Maury Povich Show and The Jerry Springer Show (along with an early appearance on Dance Fever) and recreations of events like his Playgirl photoshoots, his "fantasy date" with a Playgirl reader and the death of his friend Pledge Cartwright (played by actor Bill Brochtrup) of an AIDS-related illness to relate the story.

==Critical response==
Variety (magazine)|Variety gave Man of the Year a generally favorable review, calling the film "pleasant to watch and intermittently clever." {{cite news 
  | last = Levy
  | first = Emmanuel
  | title = Man of the Year
  | work = Variety
  | date = 1995-07-17
  | url = http://www.variety.com/review/VE1117904280.html?categoryid=31&cs=1
  | accessdate = 2008-03-23}}  However, it notes that Shafers writing is "uneven" and that the films "structure is a bit repetitive."  The New Yorker largely concurred, noting that Shafer "keep  condescension at bay with some nice comic spins" {{cite news 
  | last = Diones
  | first = Bruce
  | title = Man of the Year
  | work = New Yorker 
  | date = 1996-04-01
  | url = http://www.newyorker.com/arts/reviews/film/man_of_the_year_shafer
  | accessdate = 2008-03-23}}  but finding the use of the death of Shafers friend as Shafers catalyst for coming out to be self-serving. The San Francisco Chronicle was far harsher, deriding the film as a "vanity" production and complaining "Theres no shape to Man of the Year, no forward movement. Man of the Year doesnt even have the benefit of being hip." {{cite news 
  | last = LaSalle
  | first = Mick
  | title = Year of Living Vainly
  | work = San Francisco Chronicle 
  | date = 1996-03-01
  | url = http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/1996/03/01/DD12074.DTL
  | accessdate = 2008-03-23}}  The New York Times, however, found the film "gently satirical" {{cite news 
  | last = Holden
  | first = Stephen
  | title = Literally and Figuratively, Unclothing a Dreamboat
  | work = New York Times 
  | date = 1996-03-15
  | url = http://movies.nytimes.com/movie/review?_r=1&res=9803E4DD1639F936A25750C0A960958260&oref=slogin
  | accessdate = 2008-03-23}}  with the use of real clips from Shafers various talk show appearances creating a "tone of vertiginous loopiness."  The Times also saw the metaphor in Shafers experience to the pressure that society put on gay people to pretend to be straight.

==DVD release==
Man of the Year was released on Region 1 DVD on February 23, 1999.

==References==
 

==External links==
*   at Internet Movie Database

 
 
 
 
 
 
 