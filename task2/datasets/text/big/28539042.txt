The Man in the Mirror (1936 film)
 
{{Infobox film
| name = The Man in the Mirror
| image =The Man in the Mirror (film).jpg
| image_size =
| caption =
| director = Maurice Elvey
| producer = Julius Hagen
| writer = William Garrett (novel)   Hugh Mills  F. McGrew Willis
| starring = Edward Everett Horton Genevieve Tobin Ursula Jeans  Garry Marsh
| music =
| cinematography = Curt Courant
| editing = Ralph Kemplen
| studio = Twickenham Studios
| distributor = Wardour Films (UK)
| released =  
| runtime = 71 minutes
| country = United Kingdom
| language = English
| budget =
}} British comedy reflection in the mirror comes to life. The reflection then begins to live the wild life that the man had always dreamed of.

==Cast==
* Edward Everett Horton as Jeremy Dilke
* Genevieve Tobin as Helen Dilke
* Ursula Jeans as Veronica Tarkington
* Garry Marsh as Charles Tarkington
* Aubrey Mather as Bogus of Bokhara
* Alastair Sim as Interpreter
* Renee Gadd as Miss Blake
* Viola Compton as Mrs. Massiter
* Stafford Hillard as Dr. Graves
* Felix Aylmer as Earl Of Wigan
* Merle Tottenham as Mary
* Syd Crossley as Minor role

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Sutton, David R. A chorus of raspberries: British film comedy 1929-1939. University of Exeter Press, 2000.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 