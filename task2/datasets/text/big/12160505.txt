Sugar Cookies (film)
{{Infobox film
  | name = Sugar Cookies
  | image =Poster of the movie Sugar Cookies.jpg
  | caption = 
  | director = Theodore Gershuny
  | producer = Ami Artzi Garrard Glenn Jeffrey Kapelman Lloyd Kaufman Oliver Stone
  | writer = Theodore Gershuny Lloyd Kaufman
  | starring = George Shannon Mary Woronov Lynn Lowry Monique van Vooren Maureen Byrnes
  | music = Gershon Kingsley
  | cinematography = Hasse Wallin
  | editing = Dov Hoenig
  | distributor = Troma Entertainment
  | released = 1973
  | runtime = 82 minutes
  | language = English
  | budget =  $100,000
  | preceded by =
  | followed by =
  }}
 

Sugar Cookies (also known as Love Me My Way) is a 1973 soft-core crime film erotica directed by Theodore Gershuny. It was co-written by future president of Troma Entertainment Lloyd Kaufman and produced by future director Oliver Stone.

When an attractive young model (Lynn Lowry) is tricked into committing suicide on camera by her pornographer boyfriend, her lesbian partner (Mary Woronov) has a vengeful plan: with the help of an exact lookalike of her lover, she hatches a scheme to take down the sleazy drug dealer for good.

Kaufman wrote the film in 1973 and, along with Theodore Gershuny and Ami Artzi, formed a film company called Armor Films in order to produce it. Kaufman was able to raise $100,000 himself and was set to direct, hiring friend Oliver Stone to associate produce. Before production, Kaufman decided to hand the position of director over to the more experienced Gershuny, who, in return, rewrote Kaufmans script and cast his then-wife Mary Woronov.

==External links==
* 
*  – at the Troma Entertainment movie database

 
 
 
 
 
 
 

 