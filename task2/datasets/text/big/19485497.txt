I dag begynder livet
{{Infobox Film
| name           = I dag begynder livet
| image          = 
| image size     = 
| caption        = 
| director       = Lau Lauritzen, Jr. Alice OFredericks
| producer       = Henning Karmark
| writer         = Paul Sarauw
| narrator       = 
| starring       = Sigfred Johansen
| music          = 
| cinematography = Karl Andersson
| editing        = Marie Ejlersen
| distributor    = 
| released       = 26 December 1939
| runtime        = 109 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
| preceded by    = 
| followed by    = 
}}

I dag begynder livet is a 1939 Danish family film directed by Lau Lauritzen, Jr. and Alice OFredericks. 

==Cast==
* Sigfred Johansen - Peter Sommer
* Berthe Qvistgaard - Korpigen Vera Holm
* Eigil Reimers - Danseinstruktør Børge Ravn
* Bodil Steen - Karin Sommer
* Ib Schønberg - Betjent Emil Lundstrøm
* Maria Garland - Johanne Lundstrøm
* Tove Arni - Korpigen Margot
* Lise Thomsen - Korpigen Nelly
* Sigrid Horne-Rasmussen - Korpigen Ditten Larsen
* Tove Wallenstrøm - Laura, chorus girl
* Per Gundmann - Pianist
* Kaj Mervild - Kriminalassistent Klitgaard
* Hans Egede Budtz - Kriminalkommisær Frederiksen

==External links==
* 

 
 

 
 
 
 
 
 
 