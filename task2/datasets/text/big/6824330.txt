Kummeli: Kultakuume
{{Infobox Film name           = Kummeli: Kultahuume image          =  caption        =  director       = Matti Grönberg producer       = Markus Selin writer         = Heikki Vihinen   Timo Kahilainen starring       = Heikki Vihinen   Timo Kahilainen   Heikki Silvennoinen   Heikki Hela   Vesa-Matti Loiri   Jukka Puotila   Mari Turunen   Kari Hietalahti music          =  cinematography = Petri Rossi editing        = Kauko Lindfors distributor    = FinnKino released       =   runtime        = 92 min. country        = Finland language  Finnish
|budget         = 
}}
Kummeli: Kultakuume (Kummeli: Goldrush) is a 1997 Finnish  . Kultakuume has a single plot whereas Stories was composed of long-sketches.

==Plot==
The story is set in 1984. Elmeri Hautamäki (Silvennoinen) is a man who has lived his entire life in a mental institution. He escapes with his homosexual nurse Janne-Petteri Broman (Hela) and is joined by the Kagelberg-twins Jönssi and Dille (Vihinen and Kahilainen) on a quest for the hidden gold treasure of his father Kyrpä-Jooseppi Hautamäki, who was killed by a retreating German Waffen SS soldier Peter North (Jukka Puotila) back in 1944, in Lapland (Finland)|Lapland. Their escape leads them to be pursued by not only the Mental Institutes doctors Rasikangas and Kulokoski (Mari Turunen and Kari Hietalahti) but by the police as well.

The motley crew arrives at Elmeris fathers site but are unable to extract a reasonable amount of gold from the river. At the dead of night Jönssi stumbles on a buried German motorbike and the bodies of two dead SS-troopers. They find out that the motorbikes side-cart carries a chest full of Third Reich gold.

Later a police-pursuit lands the group on the grounds of gay baron Eugen von Lahtinen (Vesa-Matti Loiri) during a poetry-themed spring-celebration. The gold-exchanger that the group goes to, Karl-Heinz Rummenigge (Oiva Lohtander), turns out to be working for a secret Nazi-organization. Through this contact Peter North, still alive and well, learns that Elmeri has found his gold and he returns to Finland. When Hautamäki returns to exchange the rest of the gold for cash he ends up in hand-to-hand combat with North. Hautamäki wins the fight (by asking North for a cigarette).

The gold is split among the good guys, Broman marries baron von Lahtinen, becoming the mistress of his estate. Jönssi becomes the owner of the food-processing plant which he and his brother were fired from at the beginning of the film. Dille becomes a professor at the University of Tampere. Elmeri marries a call-girl named Vanessa and has many children. He takes Peter Norths name in order to stay out of the mental asylum. Peter North ends up locked up in a Mental Institution under Hautamäkis name for the rest of his life.

==Characters==
*Elmeri Hautamäki is the son of the prospector, Kyrpä-Jooseppi Hautamäki. Elmeri has lived his entire life in a mental institution. His violent hormone-activity makes him very unpredictable, he also appears to possess super-human strength, easily throwing several heavy opponents with one hand while enraged over not being offered a cigarette. However, Elmeri speaks in perfect literary Finnish and is usually very well behaved.
*Janne-Petteri Broman is an homosexual nurse who Elmeri convinces to help him escape when he shows him the gold flakes that were recovered from his fathers excrement. At first Broman does not take kindly to Jönssi and Dille who Elmeri befriends after Bromans SEAT Málaga crashes with their Dodge Aspen. Mirroring a common Finnish stereotype, Bromans speech is sometimes littered with Swedish utterances.
*James "Jönssi" Kagelberg is the taller of the Kagelberg twins, their mother died when the two were just children and they were abandoned by their father. The state tried to reintroduce the twins into society starting from the 70s, with very little success. Jönssi is fond of Finnish iskelmä-songs and is heard singing them throughout the movie. Jönssi is named after James Dean.
*Dean "Dille" Kagelberg is the shorter and the crosseyed one of the Kagelberg twins. Dille often confronts people with philosophical anecdotes. He is named after Dean Martin and not James Dean like his brother. He is known for his catch-phrase Legendaarista meaning Legendary.
:Example of use:
:-This is a BX windshield? How much did it cost?
:-Nothing!
:-Legendary.
*Peter North is a former Nazi officer who had to leave a treasure of gold behind him during the Lapland War, and decades after returns to seek for it.

== External links ==
*  

 
 
 