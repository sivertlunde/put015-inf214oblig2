Stand Up, Virgin Soldiers
{{Infobox film
| name           = Stand Up, Virgin Soldiers
| image_size     = 
| image	         = Suvspos.jpg	
| caption        = Film poster
| director       = Norman Cohen
| producer       = Greg Smith
| writer         = Leslie Thomas
| based on      = novel by Leslie Thomas
| narrator       = 
| starring       = Robin Askwith Nigel Davenport
| music          = Ed Welch
| cinematography = Ken Hodges
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 87 minutes United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} The Virgin Soldiers (1969).  The screenplay was written by Leslie Thomas based on his 1975 novel of the same name.

==Cast==
*Robin Askwith as Pte Brigg
*Nigel Davenport as Sgt Driscoll
*George Layton as Pte Jacobs 
*John Le Mesurier as Col Bromley-Pickering
*Warren Mitchell as Morris Morris 
*Robin Nedwell as Lt Grainger 
*Edward Woodward as Sgt Wellbeloved
*Irene Handl as Mrs Phillimore 
*Pamela Stephenson as Bernice 
*Lynda Bellingham as Valerie 
*David Auker as Lantry
*Fiesta Mei Ling as Juicy Lucy
*Miriam Margolyes as Elephant Ethel
*Patrick Newell as M.O. Billings

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 