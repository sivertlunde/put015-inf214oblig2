Free Wheeling
{{Infobox film
| name           = Free Wheeling
| image          = Free wheeling.JPEG
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Robert F. McGowan Hal Roach
| writer         = Hal Roach H. M. Walker
| narrator       =
| starring       =
| music          = Leroy Shield Marvin Hatley
| cinematography = Art Lloyd
| editing        = Richard C. Currier
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 19 34" 
| country        = United States
| language       = English
| budget         =
}}
 short comedy film directed by Robert F. McGowan.  It was the 117th (29th talking episode) Our Gang short that was released.

==Plot==
Confined to a neck brace, poor little rich boy Dickie would like to play with the neighborhood kids, but his overprotective mother will not let him. On the sly, however, Dickie sneaks out of his bedroom in search of adventure in the company of his best pal, Stymie. Purchasing a ride on the donkey-driven "taxicab" piloted by Breezy Brisbane, the boys, along with hitchhikers Spanky and Jacquie Lyn, experience enough thrills and excitement to last a lifetime when the taxi begins rolling down a steep hill minus brakes.   

==Cast==
===The Gang=== Dickie Moore as Dickie Matthew Beard as Stymie
* Dorothy DeBorba as Dorothy
* Kendall McComas as Breezy Brisbane
* George McFarland as Spanky
* Jackie Lyn Dufton as Jacquie 
* Douglas Greer as Douglas

===Additional cast===
* Bobby Mallon as Kid who gets paddled by the gang
* Johnnie Mae Beard as Stymies mother
* Harry Bernard as Roadside Worker
* Estelle Etterre as Dickies Nurse
* Dick Gilbert as Roadside worker
* Creighton Hale as Creighton, Dickies father
* Theresa Harris as Maid Jack Hill as Officer sent skyward
* Ham Kinsey as Roadside worker
* Wilfred Lucas as The specialist Anthony Mack as Man who gets socked while asleep by lamppost 
* Lillian Rich as Dickies mother
* Joe the Monk as Monkey
* John Collum as Undetermined role
* Jimmy Daniels as Undetermined role
* Bobby DeWar as Undetermined role

==Note== AMC airing from 2001 to 2003.

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 