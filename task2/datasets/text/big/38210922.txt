My Lady Boss
{{Infobox film
| image            = My Lady Boss Official Poster.jpg
| name             = My Lady Boss
| caption          = Theatrical release poster
| director         = Jade Castro 
| producer         = Jose Mari Abacan Annette Gozon-Abrogar Lily Monteverde Roselle Monteverde
| music            = Pearlsha Abubakar
| sound            = Mike Idioma
| cinematography   = Jay Abello
| story            = Annette Gozon-Abrogar
| screenplay       = 
| writer           = 
| editing          = Vanessa De Leon
| starring         = Richard Gutierrez Marian Rivera
| distributor      =  
| released         = July 3, 2013
| country          = Philippines
| language         =  
| runtime          = 103 Mins.
| budget           = P10 Million
| gross            = P19,452,412 
}}

My Lady Boss is a Filipino romantic comedy film directed by Jade Castro, starring Richard Gutierrez and Marian Rivera. It is produced by GMA Films together with Regal Films. On April 8, 2013, after a series of postponed showings, it is released nationwide on July 3, 2013.      

==Background and development==
The film was first announced by the lead actress, Marian Rivera on December 3, 2012 during an interview with Philippine Entertainment Portal.    It is going to be the second film which stars Rivera and actor Richard Gutierrez together following the My Best Friends Girlfriend produced by the same film outfit in 2008.     On a set visit by Samantha Portillo of GMA Network, Gutierrez states that "..after 5 years, finally, we get to do a movie", while Rivera says that her working relationship with Richard is better than ever.  On an interview on The Philippine Star, Gutierrez talked about the film saying "..It’s entertaining and light. It feels good doing a movie of this type again. I was looking forward to this movie after Seduction. We enjoy doing the movie. Although the acting is serious, it is lighter.".    He further added that the film, though still a romantic-comedy, is more mature than BFGF(My Besftriends Girlfriend). 
 The Proposal(2009) produced by Mandeville Films.   An assumption which proved to be inaccurate because the story of My Lady Boss is not similar to the Proposal, other than the leading actress is the boss of the leading actor.  The Proposal is a film about a Canadian lady Executive working in the United States who is forced to pretend to be engaged to her American assistant in order that she will not be deported back to Canada.  My Lady Boss, on the other hand is about a failed young businessman who is forced to seek employment in order to get back to the good graces of his disappointed rich grandfather. On January 2013, two short teasers were shown on television, but it was removed on-air after its playdate pushback. The full trailer was first released on the #PPSummerShake episode of Sunday variety show, Party Pilipinas. The theme song for the movie is a new rendition by Aicelle Santos and Gian Magdangal to "Ill Never Go" originally sung by Nexxus.    An acoustic version by Rita Iringan and a band version by Kristofer Martin will also be used on the film.  

===Filming=== West Fairview, Quezon City. 

===Release And Box Office Status===
The film was originally slated for a February 14, 2013 release date but it was postponed to April 10, 2013 because production failed to finish shooting the movie on time. But GMA Films moved it again because it would conflict with several Hollywood Summer Blockbuster releases. The movie was finally shown on July 3, 2013.

The film grossed P19.45 million after its two weeks of showing.

==Plot==
Zach (Gutierrez) is a rich boy forced to find and keep a job after a major blunder in a company he set up. He ends up working for Evelyn, whom he discovers to be the boss from hell. In the long run, he sees her for who and what she really is. Meanwhile, Evelyn (Rivera) is an uptight and tough Brand Manager who hires an assistant Brand Manager. When she  finds herself  dumped by her boyfriend, Evelyn seeks comfort in Zach, her assistant who shows a different side of him. As they get to know each other more and as their encounters become more intimate, they begin to ask themselves if what they feel for each other is for real. The problem is romance between boss and subordinate in a company is not allowed. Things get complicated when an office romance develops between the unlikely pair.   

==Cast==
*Marian Rivera as Evelyn Valejo Lontoc  
*Richard Gutierrez as Zach Rhys Estrella 
*Rocco Nacino as Henry Posadas Fonte 
*Tom Rodriguez as Timothy "Tim"
*Sandy Andolong as Myrna Lontoc
*Maricel Laxa as Lorna Ongpauco-Villega/L.O.V
*Sef Cadayona as Nonoy
*Ronaldo Valdez as Carlos
*Matet De Leon as Ruby
*Jace Flores as Deo
*Ruru Madrid as Elvin Lontoc
*Betong Sumaya as Sponky
*Dion Ignacio as Eugene Lontoc
*Kathleen Hermosa as Edna Lontoc
*Jackielou Blanco as Diana
*Pinky Amador as Liza
*Regine Tolentino as Lydia
*Mikey Bustos as Norman
*Chloe McCully as Chancy
*Benjie Paras
*Victor Aliwalas
*Gerard Pizzaras
*Petra Mahalimuyak

 

== References ==
 

==External links==

 
 
 
 
 
 
 
 