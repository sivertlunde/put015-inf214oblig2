Paruthiveeran
 
{{Infobox film
| name           = Paruthiveeran
| image          = Paruthi.jpg
| director       = Ameer Sultan
| producer       = K. E. Gnanavelraja
| writer         = Ameer Sultan Saravanan Ponvannan Ganja Karuppu
| cinematography = Ramji
| music          = Yuvan Shankar Raja
| editing        = Raja Mohammed
| released       =  
| studio         = Studio Green
| runtime        = 162 minutes
| country        = India
| budget         =  
| gross          =
| language       = Tamil
}}
 Tamil film written and directed by Ameer Sultan. The film stars Karthi in his feature film debut as the titular character, with Priyamani as the female lead and Ponvannan, Saravanan and Ganja Karuppu and Sujatha playing supporting roles. Produced by Studio Green, it features a soundtrack and score composed by Yuvan Shankar Raja, cinematography by Ramji and editing by Kola Bhaskar.
 National Film Awards, six Filmfare Awards South and two Tamil Nadu State Film Awards, among others, whilst being screened at several international film festivals such as the Berlin International Film Festival and the Osians Cinefan Festival of Asian and Arab Cinema as well. It also emerged a high commercial success, running successfully for more than one year in theatres.  The film was dubbed into Telugu and released as Malligadu on 2 March 2012. 

==Plot==
The story is set in a rural area around Madurai, in the village called Paruthiyur. Paruthiveeran (Karthi) is a country brash, who is often arrested for petty crimes. His one ambition in life is to gain enough notoriety to be on TV news and to be thrown into Madras Jail. He often bullies Paruthiyur villagers for money, women and fun. He lives with his doting uncle Chevvaazhai (Saravanan) who pampers and accompanies him in every misbehavior to keep him happy. His cousin Muththazhagu (Priyamani) is the daughter of a caste fanatic who is a prominent person in the villages council. Muththazhagu loves him more than her life, but he remains unmoved and keeps her at bay. As a kid, Paruthiveeran saved her from drowning in a well, an event where she promised to marry him and be by his side forever. She starts loving him passionately against her parents will. In order to delay her marriage with anybody else, she fails herself in school to be in the same grade/class. At times Paruthiveeran is violent too. When he understands Muththazhagus true love for him and decides to marry her, the feud between the two families comes in the way. Determined as he is to possess her, he warns her against marrying someone of her parents choice. He even threatens to cut her into pieces. Undeterred by his threats, her parents press ahead with the preparations for her marriage with a man of their choice. Since Muththazhagu is not able to convince her parents, she runs away from home wanting to elope with Paruthiveeran. However, she gets kidnapped and brutally gang raped by four people. Muththazhagu beseeches Paruthiveeran to cut her into pieces, as she is dying anyway and does not want anybody to find her like this. Paruthiveeran, on her request, hence chops her corpse into pieces, and is walking away, when he is found by Muththazhagus father and other relatives. Assuming him to be her kidnapper and killer, they beat him to death.

==Cast==
* Karthi as Paruthiveeran
* Priyamani as Muththazhagu
* Ponvannan as Kazhuvathevan Saravanan as Chevvaazhai
* Sujatha as Kazhuvathevans wife
* Ganja Karuppu as Douglas
* Panjavarnam as Mangayee
* Sampath Raj as Marudhu
* Ammulu as Kuraththi
* Sevvazhairaj as Ponanthinni
* Samuthirakani (Cameo appearance)

==Production==
About 60 debutants, mostly real-time people from the region, appeared in the film.     The number of newcomers who acted in the film, notably, outweighed the number of professional artists, with Priyamani, Ponvannan, Saravanan and Ganja Karuppu being the only four experienced actors.  Furthermore, no dubbing artists were involved in the production and all artists were brought to Chennai had dubbed their original voice,   Filming was held at various locations that, according to Ameer, were never before explored by Tamil cinema.  A village carnival was created for the film and shot for 10 days in Karumathur, Madurai. The film encountered financial hurdles in the midst, with director Ameer Sultan taking over the production. He later stated that he had incurred heavy financial loss as a producer of the film. 

==Music==
{{Infobox album
| Name        = Paruthiveeran
| Type        = soundtrack
| Artist      = Yuvan Shankar Raja
|   = Gainsboro  BOSS MAN LADYS SQUAD Recorded    = 3025 
| Released    = 11 November 2006 
| Genre       = Soundtrack 
| Length      = 27:56 
| Label       = Star Music 
| Producer    = Yuvan Shankar Raja 
| Reviews     = 
| Last album  = Thimiru (2006)
| This album  = Paruthiveeran (2006)
| Next album  = Madana (film)|Madana (2006)
}}
 instrumental track, featuring the nadaswaram only. It features vocals from Yuvan Shankar Rajas father Ilaiyaraaja, director Ameer himself and professional playback singers Shreya Ghoshal, Manikka Vinayagam and Madhumitha. Moreover, a couple of traditional, village-based singers contributed to the album by lending their voice to a few songs. All lyrics were written by Snehan. Paruthiveeran was notably the first ever entirely village-based film Yuvan Shankar Raja had scored music for.   He composed a folk music-based score and used rural sounds with instruments as dholak,  nadaswaram,    thavil and urumee.    

Yuvan Shankar Raja received critical acclaim for his score, while the soundtrack album initially got mostly unfavorable reviews, being described as a "letdown" and "disappointment",   raising to question whether it could attract todays "modern" audience.  Following the films release however, critics unanimously made very positive remarks in regards to the film score. Baradwaj Rangan labelled it a "magnificently earthy score",    while Malathi Rangarajan noted that the composer "proves he is a chip off the old block in rustic music too".    Other critics hailed his music as "excellent",  and as "unmistakably a milestone on his road to the pinnacle that his father Ilayaraja has reached".  Due to its successful run at the box office, the songs enjoyed popularity as well, particularly among the younger generation.     The song "Oororam Puliyamaram" in particular was the Chartbuster number,  also being chosen as the "Best Folk Song of the Year 2007" at the Isaiyaruvi Tamil Music Awards,    whilst the entire album itself was named as "Isaiyaruvi Best Album of the Year 2007".   

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration (min:sec) !! Notes
|- 1 || "Ariyadha Vayasu" || Ilaiyaraaja || 3:49 ||
|- 2 || "Iayyayo" || Krishnaraj, Manikka Vinayagam, Shreya Ghoshal, Yuvan Shankar Raja || 4:35 ||
|- 3 || "Nathaswaram" || Instrumental || 3:24 || Performed by S. R. Shanmuga Sundaram Group
|- 4 || "Sari Gama Pathani" || Srimathumitha, Madurai S. Saroja, Ameer Sultan || 2:16 ||
|- 5 || "Tanka Dunga" || Kala, Lakshmi, Pandi, Raja, Madurai S. Saroja || 7:31 ||
|- 6 || "Oororam Puliamaram" || Kala, Lakshmi, Pandi, Madurai S. Saroja || 6:27 ||
|}

==Release==
The satellite rights of the film were secured by Kalaignar TV|Kalaignar. The film was given a "U/A" certificate by the Indian Censor Board.

==Reception==
The film received overall positive critical response, particularly regarding the lead artists performances. Sriram Iyer of Rediff gave the film three out of five stars, lauding it as a "remarkable effort" and claiming that Ameer "successfully brings alive the feel of the pastures". The reviewer appreciated the cast and crew, describing Karthis performance as "excellent", while citing that Saravanan "excels" and Priyamani had done "a fairly good job in her deglamourised role".  A Behindwoods critic wrote that Paruthiveeran "lives up to all that it promised and more". Heaping praise on the director, the reviewer further adds that Ameer had "shown his class again" and "enhance  his reputation as a master craftsman".  Russell Edwards of Variety (magazine)|Variety cited, "rough production values are mitigated by a myriad of sophisticated cinematic techniques that show this is no boondocks effort. Fests that appreciate curry flavors will eat this one up, though uninitiated aud s could experience culture shock". Furthermore, he noted that Ameers "creative helming, which includes split-screen, appears unbridled and sometimes outright chaotic, but always serves the script", while writing that performances were "consistent with the bombastic storytelling".  A reviewer from nowrunning.com called the film a "gripping tale narrated with a skill and imagination which only directors like Amir are endowed with", rating it three out of five. 

Baradwaj Rangan called Paruthiveeran a "classic, one for the ages" and a "showcase for how good filmmaking can (almost) overcome mediocre material", while noting that Karthi gave "a superb first-film performance by any standard, and it makes you reach for that oldest of movie-myth cliches: A star is born."  Other critics have offered criticism of the violence and brutality of some sequences in the film. A Sify critic wrote that it was "undoubtely   a brave film and full credit goes to the director for making every scene realistic and characters life-like", while criticizing that Ameers script was "too thin on logic" and the climax "too heavy, dark and morbid."    However in regards to the performances, the reviewer, too, heaped praise on the actors, describing Karthi as "spectacular" and Saravanan as "terrific" and adding that Priya Mani "... steals the show with her spontaneity and authenticity".  Malathi Rangarajan of The Hindu claimed that the film was "the genuine depiction of village life" that "transports you to the era of classics in a rustic ambience, Bharatiraaja style", further citing that "all the same, when graphic pictures of gang rape and killing in cold blood dominate, things becomes too much to stomach" and that Ameer "only creates the impression that village folks as a whole are a belligerent lot." 

===Box office===
Released on 23 February 2007, Paruthiveeran faced competition from Mozhi, which was released on the same day. The film took a big opening at the Chennai box office,  collecting  3.69 crore from 92 prints in Tamil Nadu in its opening weekend.  The film continued to rank at first at the Chennai box office for five successive weeks,  being ousted only by the Hollywood production 300 (film)|300 during the Easter weekend.  At the end of its run, the film had reportedly grossed a share  15 crore at the box office,  being declared one of the most commercially successful Tamil films of the year.   

===Accolades=== 58th Berlin International Film Festival.  The film was screened five times at the Berlin Film Festival in two German subtitled versions and three English subtitled versions. 

2008 Berlin International Film Festival (Germany)  Netpac Award - Special Mention - Ameer Sultan

2007 Cinema Journalists Association Awards 
* Won - Best Film - Paruthiveeran
* Won - Best Director - Ameer Sultan
* Won - Best Actress - Priyamani

2007 Filmfare Awards South (India) Best Film – Tamil - Paruthiveeran Best Director – Tamil - Ameer Sultan Best Actor – Tamil - Karthi Best Actress – Tamil - Priyamani Best Supporting Actor – Tamil - Saravanan Best Supporting Actress – Tamil - Sujatha
 2007 National National Film Awards (India) Silver Lotus Award - Best Actress - Priyamani Silver Lotus Award - Best Editing - Raja Mohammed

2007 Osians Cinefan Festival of Asian and Arab Cinema   
* Won - Best Film Award - Paruthiveeran
* Won - Best Actress Award - Priyamani

2006 Tamil Nadu State Film Awards Best Actress - Priyamani Special Prize - Actor - Karthi

2007 Vijay Awards Best Film - Paruthiveeran Best Actress - Priyamani Best Supporting Actress - Sujatha Best Debut Actor - Karthi Best Director - Ameer Sultan Best Supporting Actor - Saravanan Best Music Director - Yuvan Shankar Raja Best Cinematographer - Ramji Best Editor - Raja Mohammed Best Art Director - Jackson Best Lyricist - Snehan Best Story-Screenplay Writer - Ameer Sultan

==Controversy==
There was a controversy surrounding the ownership of the movie. Ameer has complained that the original producer Gnanavel, couldnt finance the movie till its completion, so he supposedly gave up the movie to Ameer. But towards the completion of the movie, Ameer ran out of funds and the movie went back to Gnanavel.  Since the release of the movie, both parties have been accusing each other for failing to make a payment which each owes from the other.   

==References==
 

==External links==
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 