The Return of Clint the Stranger
{{Infobox film
| name           = The Return of Clint the Stranger (aka Theres a Noose Waiting for You...Trinity!)
| image          = Clintthestranger.jpg
| caption        = DVD cover art
| director       = Alfonso Balcázar
| producer       = Enzo Doria
| writer         = Enzo Doria Giovanni Simonelli
| starring       = Klaus Kinski
| music          = 
| cinematography = Jaime Deu Casas
| editing        = Teresa Alcocer
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Italy Spain
| language       = Spanish
| budget         = 
}}
 Western film directed by Alfonso Balcázar and starring Klaus Kinski.     

==Cast== George Martin - Clint Murrayson
* Marina Malfatti - Norma Murrayson
* Klaus Kinski - Scott Daniel Martín - Slim
* Augusto Pescarini - Jimmy Murrayson
* Francisco José Huetos - Mr Scranton
* Susanna Atkinson - Betty Murrayson (as Billy)
* Willi Colombini - (as Willy Colombini)
* Luis Ponciado - Brandon
* Gaspar Indio González - Sheriff Culver (as Indio González)
* Manuel Muñiz - Telegrapher (as Pajarito)
* Manuel Sas - Bill McKinley
* Manuel Bronchud - Slims Right Hand
* Ricardo Moyán
* Gustavo Re - Blinky - bartender
* Adolfo Alises - Mr Thompson
* Miguel Muniesa - Ben
* Vittorio Fanfoni - Murdoch brother
* Luigi Antonio Guerra - Murdoch brother

==Releases== Clint the Nevadas Loner.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 