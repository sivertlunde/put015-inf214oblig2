Baptists at Our Barbecue
{{Infobox film
| name        = Baptists at Our Barbecue
| image       =
| director    = Christian Vuissa
| starring    = Dan Merkley  Steve Anderson Heather Beers Wayne Brennan Katherine Brim Jan Broberg Felt Bonnie Burt Michael Anthony Christian Bernie Diamond Frank Gerrish
| distributor = Blue Crow Productions
| released    = 2004
| runtime     = 88 min.
| language    = English
| music       =
| awards      =
| budget      =
| tagline     = 262 Mormons, 262 Baptists, Heaven help us.
}} Mormon audiences, humor that people who are not LDS are not likely to get, as well as some humor aimed at non-Mormon audiences.

==Plot==
"Baptists at Our Barbecue" is the story of the small town of Longwinded, Arizona, USA, a divided, feuding town of 262 Mormons and 262 Baptists.  Its also a story about one man who will try anything to end the ridiculous feud, bring the town together, and keep the peace-loving girl of his dreams from leaving town. 

==Main cast==
 
*Dan Merkley as Tartan
*Heather Beers as Charity
*Jan Broberg Felt as Tartans Mom
*Dane Stephens as Sheriff Bob
*Frank Gerrish as Brother Hatch
*Tony Larimer as Pastor Stevens
 

==Soundtrack==
 
* "New Emotion" – Ryan Shupe & the Rubberband 
* "Mircle" – Jamen Brooks 
* "Lucia" – The Court & Spark 
* "A Thousand Miles" – Angela Pace 
* "Long Old Time" – Micah Dahl Anderson 
* "Ghosts Are Good Company" – Bishop Allen 
* "Grew Young" – Sweethaven 
* "Quarry Anthem" – Big Smith 
* "Women Thoughts" – Greg Duckwitz 
* "Head Up the Mountain" – Jamen Brooks 
* "O Sister, There Thou Art" – Micah Dahl Anderson 
* "Stretch On, Road" – Night In Wyoming 
* "The Calm Before the Storm" – Greg Duckwitz 
* "Hallelujah II" – The Court & Spark 
* "If" – The Happies 
* "Perfectly Complicated" – Tiffany Fronk 
* "Lets Have a Barbecue!" – Matt Mattson 
 

==References==
 

==External links==
* 
*  

 
 
 
 
 
 