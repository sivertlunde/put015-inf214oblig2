The Hollywood Sign (film)
{{Infobox film
| name           = The Hollywood Sign
| image          = 
| alt            = 
| caption        = 
| director       = Sönke Wortmann
| producer       = Leon de Winter Gerhard Meixner Angela Roessel
| writer         = Leon de Winter
| starring       = Tom Berenger Rod Steiger Burt Reynolds Jacqueline Kim
| music          = Peter Wolf
| cinematography = Wedigo von Schultzendorff
| editing        = Edgar Burcksen
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States Netherlands Germany
| language       = English
| budget         = 
| gross          = 
}}

The Hollywood Sign is a 2001 comedy/suspense drama film directed by Sönke Wortmann and written by Leon de Winter. It stars Tom Berenger, Rod Steiger and Burt Reynolds as three washed-up actors risking their lives to make a comeback, as well as Jacqueline Kim.

==Plot== Las Vegas casino - which originated as a movie plot written by one of the actors ex-girlfriends - who could not get her script produced. Desperate to make a come-back, the three risk their lives for production capital... by writing themselves into the "script."

==Cast==
* Tom Berenger as Tom Greener
* Jacqueline Kim as Paula Carver
* Rod Steiger as Floyd Benson
* Burt Reynolds as Kage Mulligan
* Al Sapienza as Rodney
* Dominic Keating as Steve
* Eric Bruskotter as Muscle
* David Proval as Charlie
* Kay E. Kuter as Robbie Kant

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 


 