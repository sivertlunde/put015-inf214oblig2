Million Dollar Legs (1939 film)
{{Infobox film
| name           = Million Dollar Legs
| caption        = 
| image	=	Million Dollar Legs FilmPoster.jpeg
| director       = Nick Grinde Edward Dmytryk (uncredited)
| producer       = William C. Thomas Richard English Lewis R. Foster
| starring       = Betty Grable
| music          = 
| cinematography = Harry Fischbeck
| editing        = Arthur P. Schmidt
| distributor    = Paramount Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
}}
 American comedy John Hartley and Donald OConnor.
 Million Dollar Legs.

==Cast==
* Betty Grable as Carol Parker John Hartley as Greg Melton Jr.
* Buster Crabbe as Coach Jordan (as Larry Crabbe)
* Donald OConnor as Sticky Boone
* Jackie Coogan as Russ Simpson
* Dorothea Kent as Susie Quinn
* Joyce Mathews as Bunny Maxwell
* Peter Lind Hayes as Freddie Ten-Percent Fry (as Peter Hayes)
* Richard Denning as Hunk Jordan
* Phil Warren as Buck Hogan
* Edward Arnold Jr. as Blimp Garrett
* Thurston Hall as Gregory Melton Sr.
* Roy Gordon as Dean Wixby
* Matty Kemp as Ed Riggs
* William Tracy as Egghead Jackson

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 


 