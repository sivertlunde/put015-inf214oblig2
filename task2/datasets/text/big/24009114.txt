Strange Illusion
{{Infobox film
| name           = Strange Illusion
| image          = Strange illusion Poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Edgar G. Ulmer
| producer       = Leon Fromkess
| screenplay     = Adele Comandini
| story          = Fritz Rotter
| narrator       =
| starring       = Jimmy Lydon Warren William 
| music          = Leo Erdody
| cinematography = Philip Tannura Benjamin H. Kline Eugen Schüfftan
| editing        = Carl Pierson
| distributor    = Producers Releasing Corporation
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Strange Illusion is a 1945 American film noir directed by Edgar G. Ulmer.  According to noir historian Spencer Selby the film is, "A stylish cheapie by the recognized master of stylish cheapies." 

==Plot summary==
An adolescent believes that his widowed mothers suitor may have murdered his father.

==Cast==
* Jimmy Lydon as Paul Cartwright
* Warren William as Brett Curtis
* Sally Eilers as Virginia Cartwright
* Regis Toomey as Dr. Martin Vincent
* Charles Arnt as Prof. Muhlbach
* George Reed as Benjamin
* Jayne Hazard as Dorothy Cartwright
* Jimmy Clark as George Hanover
* Mary McLeod as Lydia
* Pierre Watkin as Dist. Atty. Wallace Armstrong John Hamilton as Bill Allen, Bank President
* Sonia Sorel as Charlotte Farber
* Victor Potel as Mac - Game Warden
* George Sherwood as Langdon
* Gene Roth as Detective Sparks aka Sparky

==Reception==

===Critical response=== Freudian dream analysis, but it was unconvincing as a melodrama, the script was weak, the plot was full of holes and the acting was as lame as it gets...Whats interesting is that the film is shot as an intense dream sequence in shadowy black-and-white hues and its sense of delirium powerfully filters through the story almost wiping away the unconvincing heavy-handed performances of the villains and the mummified acting by the leads. Its a film where Ulmers unique style and his film noir moody interjections work better than the deviative mystery story." 

Critic Matthew Sorrento of Film Threat also lauded the film: "Though saddled with the script’s fetish for Freud, Ulmer stylizes his thriller without sending it adrift. Like his other great films, Strange Illusion is a shaggy quickie that takes fine shape throughout." 

==See also==
* List of films in the public domain in the United States

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 