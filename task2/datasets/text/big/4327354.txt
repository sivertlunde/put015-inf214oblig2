Baby Burlesks
{{Infobox film title          = Baby Burlesks  director       = Charles Lamont Ray Nazarro producer       = Jack Hays image          = Glad Rags to Riches Temple.jpg caption        = Shirley Temple in Glad Rags to Riches released       =   -   editing        = William Austin Howard Dillinger Arthur Ellis runtime        = 10-11 minutes cinematography = Sidney Wagner Dwight Warren music          = Alfonso Corelli writer         = Jack Hays  Charles Lamont distributor  Educational Film Fox Film starring       = Georgie Billings Danny Boone, Jr. Eugene Butler Marilyn Granas Philip Hurlic Gloria Anne Mack  Arthur J. Maskery Jimmie Milliken Dorian Samson Georgie Smith Shirley Temple country        = United States language       = English
}}

Baby Burlesks  is the collective series title of eight thematically unrelated one-reeler films produced by Jack Hays and directed by Charles Lamont for Educational Pictures in 1932 and 1933. The eight films are satires on major motion pictures, film stars, celebrities, and current events,  and sometimes contain material that may be considered racist or sexist.  Cast members are preschoolers clad in adult costumes on the top and diapers fastened with large safety pins on the bottom. 

Many of the children employed in the series were recruited from Meglin Kiddies|Meglins Dance School in Hollywood,  and, when not rehearsing or shooting, were sent out by the studio as advertising models for a variety of products (including breakfast cereals and cigars) in order to underwrite the costs of film production. 

The series is notable for featuring three-year-old Shirley Temple in her first screen appearances. In her 1988 autobiography, the actress describes the Baby Burlesks as "a cynical exploitation of our childish innocence".  She also said the films were "the best things I ever did". 

==Filmography==
All eight films in the Baby Burlesks series were produced by Jack Hays and directed by Charles Lamont, except the first, Runt Page, which was directed by Ray Nazarro.  Rehearsals took place over a week or two for each film, with no pay, and then were shot quickly in two days.  As a star, Temple, received $10 a day.   In 2009, all eight films were available on videocassette and DVD.

*Runt Page, directed by Ray Nazarro, was released on April 11, 1932, and distributed by Universal Pictures.  The 10-minute film is a spoof of the play The Front Page with Temple playing Lulu Parsnips, a take-off on Louella Parsons, and Georgie Smith playing Raymond Bunion, a take-off on Damon Runyon.  The film is notable for being Temples first film appearance. In her autobiography, Shirley Temple Black wrote that Runt Page was "  dismal failure in the marketplace,   its sale   abandoned".  Unlike the other films, the young actors voices are dubbed by adults.
 War Babies What Price Glory? and was originally titled What Price Gloria?.   The film is set in Buttermilk Petes Cafe where child performers dance, play music, and drink and spill milk.  As a character called Charmaine, Temple spoofs Dolores Del Rio and speaks her first on-screen words, "Mais oui, mon cher".  Georgie Smith and Eugene Butler play doughboys.  Others in the cast are Dorian Samson, Georgie Billings, and Philip Hurlic.
 Lois Wilson Indians and pelted with clods of dirt until rescued by Georgie Smith. Others in the cast are Eugene Butler as Gene, Philip Hurlic as Dynamite, Arthur J. Maskery as an Indian Chief, Jimmie Milliken as Baby, and Dorian Samson as Kalimo.

*Glad Rags to Riches was released on February 5, 1933, and was distributed by Educational Film Exchanges.  The 11-minute film stars Temple as Le Belle Diaperina, a Gay Nineties chanteuse at the Lullaby Lobster Palace who must decide whether to marry a rich nightclub owner or a country boy. The film features her first on-screen tap dance and song, "Shes Only a Bird in a Gilded Cage".   Others in the cast are Eugene Butler as the Nightclub Owner, Lawrence Harris  as a Policeman, Marilyn Granas as the Maid, Georgie Smith as Elmer, Nells boyfriend, and Dorian Samson as security guard.

*Kiddin Hollywood  was released March 14, 1933, and distributed by Fox Film.  The 10-minute film stars Temple as a former beauty queen reduced to scrubbing soundstage floors until discovered by director Frightwig von Stumblebum, a satire of Eric von Stroheim, and made a star as Morelegs Sweetrick, a play on Marlene Dietrich. It is considered the best of the series. 

*The Kids Last Fight  was released on April 23, 1933, and distributed by Fox Film. The 11-minute film is a satire on Jack Dempsey and the boxing world.  Georgie Smith plays boxer Diaper Dampsey whose girlfriend (Temple) is kidnapped by gangsters before a big fight.  Others in the cast are Lawrence Harris as Pop Skull McGee, Arthur J. Maskery as Dampseys Manager, Sidney Kibrick as a kidnapper, Philip Hurlic as Dampseys corner man, and Marilyn Granas as Lulu Parsnips.

*Polly Tix in Washington was written by the films director,   summarizes, "Jack Hayss intentions were obvious. The Baby Burlesks were meant to titillate male matinee audiences".   The script required Temple to take a ride in an ostrich-drawn cart but the frightened bird bolted and Temple came close to being killed. 

*Kid in Africa  was released on October 6, 1933, and distributed by Fox Film. The 10-minute film stars Temple as Madam Cradlebait, a missionary and captive of jungle savages rescued from the cooking pot by a Tarzan-like character called Diaperzan (Danny Boone, Jr.), who arrives on the back of an elephant.  A group of black children playing savages were directed to run and fall en masse when shot by arrows from the good guys.  Unbeknownst to the children, a piano wire was strung shin-high in their path and they fell into a yowling heap, some with bleeding shins.  The film was the most tasteless in the series, and, with its release, the series was discontinued.  The film strangely includes an advertisement for Ex-Lax, a sign for which appears on a post in front of the Hotel "Waldorf-Astoria Hotel|Squaldorf". 

==See also==
* Shirley Temple filmography

==References==
 
;Works cited
*  
*  
*  

== External links ==
* 
* 
* 
* 
* 
*  at the Internet Movie Database
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 