Terraferma (film)
{{Infobox film
| name           = Terraferma
| image          = Terraferma poster.jpg
| caption        = Theatrical release poster
| director       = Emanuele Crialese
| producer       = Marco Chimenz Giovanni Stabilini Riccardo Tozzi    Fabio Massimo Cacciatori (Associate Producer)
| writer         = Emanuele Crialese Vittorio Moroni
| starring       = Mimmo Cuticchio Giuseppe Fiorello Donatella Finocchiaro
| music          = Franco Piersanti
| cinematography = Fabio Cianchetti
| editing        = Simona Paggi
| studio         = Cattleya
| distributor    = 01 Distribution
| released       =  
| runtime        = 88 minutes
| country        = Italy
| language       = Italian
| budget         = € 7.85 million 
}} Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.   

== Plot ==
On Linosa, fishermen are punished for saving illegal immigrants (boat people) from the sea and, back on shore, letting them go, because this amounts to facilitating illegal immigration. Therefore young local Filippo does not allow them on his boat. Several die, and Filippo changes his mind about the matter: he helps a family consisting of a mother, a little boy and a newborn baby, to leave to the Italian mainland.

== Cast ==
* Filippo Pucillo as Filippo
* Donatella Finocchiaro as Giulietta
* Mimmo Cuticchio as Ernesto
* Giuseppe Fiorello as Nino
* Timnit T. as Sara
* Claudio Santamaria as Santamaria
*Tiziana Lodato as  Maria

== Production == France 2 Cinéma and money from the National Center of Cinematography and the moving image|CNC. The total budget was 7.85 million euro.   

== Release == Special Jury Prize at the Venice Film Festival, the equivalent of third place.  It was released in Italy on 7 September 2011 through 01 Distribution. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 68% of 19 surveyed critics gave the film a positive review; the average rating was 6.6/10.  Jay Weissberg of Variety (magazine)|Variety described it as "a well-made movie with no pretension but also no crying need to be at a major film festival."  Deborah Young of The Hollywood Reporter called it "a morally passionate social drama, muted by overly familiar storytelling."  Gary Goldstein of the Los Angeles Times wrote, "The thematically rich production is grounded in deep moral and emotional reflection."  Chuck Wilson of The Village Voice called it predictable and heavy-handed. 

== See also ==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Italian submissions for the Academy Award for Best Foreign Language Film

== References ==
 

== See also ==
* Movies about immigration to Italy

== External links ==
*    
*  

 
 
 
 
 
 
 
 
 