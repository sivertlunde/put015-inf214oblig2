Murderous Maids
{{Infobox film
| name = Les Blessures assassines
| director = Jean-Pierre Denis
| image	= Murderous Maids FilmPoster.jpeg
| caption = Film poster
| producer = Laurent Pétin  Michèle Pétin 
| screenplay = Jean-Pierre Denis Michèle Pétin 
| based on   =  
| starring = Sylvie Testud   Julie-Marie Parmentier   Isabelle Renauld     François Levantal   Dominique Labourier   Jean-Gabriel Nordmann   Marie Donnio   Nadia Barentin 
| editing = Marie-Hélène Dozo
| cinematography = Jean-Marc Fabre
| studio         = ARP Sélection StudioCanal
| distributor    = ARP Sélection
| runtime = 94 minutes
| country = France
| language = French
| released =   
| budget = €4,100,000
| gross = $225,390 
}}
 French film directed by Jean-Pierre Denis, released in 2000, which tells the true story of two French maids, Christine and Lea Papin. The screenplay by Jean-Pierre Denis with Michèle Pétin, was based on the book  Laffaire Papin by Paulette Houdyer. It told the story of the double murder committed by the maids, which made sensational headlines in France in 1933. The film had 360,846 admissions in France. 

==Plot==
Christine Papin (Sylvie Testud), and Léa Papin (Julie-Marie Parmentier) are sisters with a troubled past, who work as maids in Le Mans, France. After a string of domestic jobs, they start working for the Lancelin family, which consists of Monsieur Lancelin, his wife and their adult daughter Genevieve. Christine sees in Madame Lancelin a mother figure, in spite of her severity. But their wretched background — an indifferent mother, a drunken, abusive father and time spent in orphanages — casts a shadow over the girls. Over time, their ill-fated situation darkens and they withdraw into themselves. Finally, after six years of service, they end up committing a particularly brutal crime on February 2, 1933: killing Madame Lancelin and her daughter after gouging their eyes out.

== Cast ==
* Sylvie Testud as Christine Papin 
* Julie-Marie Parmentier as Léa Papin 
* Isabelle Renauld as Clémence 
* François Levantal as Le Gazé 
* Dominique Labourier as Madame Lancelin 
* Jean-Gabriel Nordmann as M. Lancelin 
* Marie Donnio as Geneviève Lancelin 
* Nadia Barentin as La mère supérieure

==Awards and nominations==
Sylvie Testud won the César Award for Most Promising Actress in 2001 for her performance as Christine Papin. The film was nominated for the César for best film,  and Jean-Pierre Denis for best director.

== References ==
 

==External links==
*  

 
 
 
 
 
 
 

 
 