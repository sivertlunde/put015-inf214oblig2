Five Summer Stories
{{Infobox film
| name           = Five Summer Stories
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       =  
| producer       =  
| writer         =  
| screenplay     =  Jim Freeman and Greg MacGillivray,
| based on       =  
| starring       = David Nuuhiwa, Eddie Aikau, Gerry Lopez, and Sam Hawk
| narrator       =   Honk
| cinematography = 
| editing        = 
| studio         =  
| distributor    =  
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  
}} Jim Freeman and Greg MacGillivray, starring David Nuuhiwa, Eddie Aikau, Gerry Lopez, and Sam Hawk. Its VHS re-release was in 1994, followed by a DVD release the same year.  The Original Sound Track from Five Summer Stories was composed and recorded by the Southern California native band, Honk (band)|Honk.   The soundtrack was released on LP in 1972  and re-released on CD in 1992. 
 John Lamb, who was among the first to animate surfing and skateboarding, and went on to win an Academy Award (1980) for invention of the Lyon Lamb Video Animation System.

==Reception==
Called a "cinematic cult classic",  the film is generally acknowledged as the start of the second generation of surf films, with the first generation being typified by Bruce Browns The Endless Summer. 

==References==
{{reflist | refs=
   
   
   
   
   
}}

==External links==
* 

 

 
 
 
 
 
 
 