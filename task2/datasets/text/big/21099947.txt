Rusty Knife
{{Infobox Film
| name           = Rusty Knife
| image          =
| caption        = Japanese movie poster Toshio Masuda
| producer       = Nikkatsu, Takiko Mizunoe
| writer         = Shintaro Ishihara
| starring       = Yujiro Ishihara
| music          = Masaru Sato
| cinematography = Kurataro Takamura
| editing        = Masanori Tsujii
| distributor    =  1958 
| runtime        = 90 min.
| country        = Japan
| awards         =  Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} action Cinema Japanese film Toshio Masuda. Rusty Knife was part of the Nikkatsu film studios wave of Japanese noir films, made in order to compete with popular American and French films at the Japanese box office.  The film became more widely available outside Japan only when Janus Films released a special set of Nikkatsu noir films on DVD, as part of the Criterion Collection.  The other films in the set are A Colt Is My Passport, Take Aim at the Police Van, Cruel Gun Story, and I Am Waiting.

==Plot==
Yukihiko Tachibana is an ex-convict trying to begin a new life after he is released from prison. Unable to forget the rape and consequent suicide of his girlfriend, he seeks revenge against a crime syndicate while resisting the urge to kill again. Meanwhile the district attorney, Karita, and his men try to build a case against the same syndicate.

== Cast ==
* Yujiro Ishihara as Yukihiko Tachibana
* Mie Kitahara as Keiko Nishida
* Shoji Yasui as Karita
* Mari Shiraki as Yuri
* Joe Shishido as Shimabara
* Akira Kobayashi as Makoto Terada
* Masao Shimizu as Shingo Mano
* Noaki Sugiura as Seiji Katsumata
* Toshio Takahara as Takaishi

== References ==
 

== External links ==
*  

 

 
 
 
 
 


 