The Last Castle
 
{{Infobox film
| name = The Last Castle
| image = The Last Castle Theatrical.jpg
| alt = 
| caption = Theatrical release poster
| director = Rod Lurie
| producer = Robert Lawrence Don Zepfel
| writer = David Scarpa Graham Yost Sam Mercer
| starring = Robert Redford James Gandolfini Mark Ruffalo Delroy Lindo
| music = Jerry Goldsmith Tom Waits Shelly Johnson
| editing = Michael Jablow Kevin Stitt Blinding Edge Pictures DreamWorks Pictures
| released =  
| runtime = 132 minutes
| country = United States
| language = English
| budget = $72 million   
| gross = $27.6 million 
}}
The Last Castle is a 2001 American action drama film directed by Rod Lurie, starring Robert Redford, James Gandolfini, Mark Ruffalo and Delroy Lindo.

The film portrays a struggle between inmates and the warden of a military prison, based on the  , court martialed and sentenced for insubordination, challenges the commandant, a colonel, over his treatment of the prisoners. After mobilizing the inmates, the former general leads an uprising aiming to seize control of the prison.

The film was released on October 19, 2001, in the United States, grossing about $28 million worldwide. The low gross, in comparison to its high production and marketing expenses, resulted in its being considered a box office bomb.      

==Plot==
Lieutenant General Eugene Irwin (Robert Redford) is brought to a maximum security military prison to begin a ten-year sentence for his decision (in violation of a presidential order) to send U.S. troops on a mission in Burundi, resulting in the deaths of eight soldiers. Colonel Winter (James Gandolfini), the prisons commandant, is a great admirer of the general but is offended by a comment he overhears: Irwin criticizes Winters much-prized military artifacts collection, calling it something no actual battlefield veteran would ever have.

Winter, who has never seen combat, resents the remark. He then takes exception to what he perceives as Irwins attempt to change the attitudes of the prisoners, his admiration for Irwin fading fast. On one occasion, Irwin is punished harshly after stopping a guard from clubbing a prisoner, Corporal Ramon Aguilar (Clifton Collins, Jr.), who had made the mistake of saluting Irwin in the prison yard.

Continuing to observe acts of cruelty, Irwin attempts to unify the prisoners by building a "castle wall" of stone and mortar at the facility, which in many ways resembles a medieval castle. Envying the respect Irwin is clearly receiving, Winter orders his guards to destroy the wall. Aguilar, directly involved in its construction, takes a stand before the bulldozer. Winter orders a sharpshooter to fire a normally non-lethal rubber bullet directly at Aguilars head, killing him.

After the wall is destroyed, Irwin and the inmates pay final respects to Aguilar in formation. Winter later tries to make amends with Irwin, who calls him a disgrace to the uniform and demands his resignation.

The prisoners begin to behave like soldiers around Irwin, using code words and gestures, infuriating the commandant. Winter reaches out to an anti-social prisoner named Yates (Mark Ruffalo), a former officer and Apache helicopter pilot convicted of running a drug-smuggling ring. Yates is bribed to inform about Irwins plans in exchange for a reduced sentence.

Irwin organizes a plot to throw the prison into chaos. His intent is to show a friend, Brigadier General Wheeler (Delroy Lindo), the commandants superior officer, that the commandant is unfit and should be removed from command under the Uniform Code of Military Justice. During a visit, Winter receives a letter threatening the kidnapping of General Wheeler by the prisoners. After ordering his men into action, Winter discovers that the scheme was a fake. Irwin orchestrated it as a way to detect how prison guards would react during an actual uprising.

Yates becomes the key to their plan, tasked with stealing a U.S. flag from the wardens office and seizing a Bell UH-1 helicopter used by guards. The inmate revolt begins.

Using improvised weapons—some resembling medieval or Roman ones, such as a trebuchet and a testudo formation—and the tactics of a military unit, the prisoners capture an armored vehicle and the helicopter. The prisoners place a call to Wheelers headquarters and inform him of the riot. Winter has little time to regain control before Wheeler will arrive to see the prison under siege. He orders the use of live ammunition against the prisoners.
 upside down, a classic signal of distress. Irwins men create havoc but ultimately are confronted by overwhelming numbers of guards, all armed with live ammunition. Knowing further resistance would only mean a massacre, Irwin orders the prisoners to stand down. Winter has successfully halted the uprising, but Irwin nonetheless elects to personally hoist the flag.

Unable to make him stop, Winter orders his men to open fire on Irwin before the upside-down U.S. flag is flown. They refuse to do so on the orders of Winters second-in-command, Captain Peretz. The colonel cannot persuade anyone else to follow his command, so he proceeds to shoot Irwin fatally himself.

Peretz places the commandant under arrest. The prisoners salute the flag and Winter now sees that Irwin has actually raised the flag in the correct manner. It flies above the prisons walls as General Wheeler arrives and Colonel Winter is led away in handcuffs. The story ends with the inmates building a new wall as memorial to their fallen comrades. Aguilar and Irwins names are among those carved onto the castles wall.

==Cast==
* Robert Redford as Lieutenant General Eugene Irwin
* James Gandolfini as Colonel Winter
* Mark Ruffalo as Yates
* Delroy Lindo as Brigadier General Wheeler Steve Burton as Captain Peretz
* Paul Calderón as Sergeant Major Dellwo
* Clifton Collins, Jr. as Corporal Aguilar Robin Wright (uncredited) as Rosalie Irwin, the generals daughter

==Production==
  The Green Last Dance, Gothic and castle-like appearance. The state of Tennessee offered to provide the location rent-free, with exemption from the states 6&nbsp;percent state sales tax.  James Gandolfini earned $5&nbsp;million for co-starring in the film after finishing the third season of The Sopranos in March 2001. 

A crew of 150 worked on refurbishing existing buildings and constructing new buildings in a time limit of nine weeks. A wall   long and   high was built, serving as the prisons entrance. A metal walkway and two towers were also built as vantage points for the guards. The film required an office with a large window through which the warden could watch the inmates; this was constructed by the production crew. Director Rod Lurie insisted on having the prisoners cells face each other, but this is not the case at the Tennessee State Prison. To solve the problem, production designer Kirk Petruccelli created cells in a warehouse near the prison.   

===Cinematography===
To show the balance of power, the film crew used multiple cinematography techniques involving different displays of color, lighting, camera and costumes. In the wardens office intense color was used to reflect freedom or power, in contrast to the washed-out colors from the less powerful yard. The contrasts shift as the story progresses, showing the increasing power of the prisoners. The American flag in the yard is described by Petruccelli as "the heart of The Castle" and is the only exemption to the washed-out color palette. 
 Shelly Johnson, in collaboration with director Lurie and the design team, also used lighting and camerawork to signify the shifting of powers. For example, the yard is at first naturally lit and more influenced by daylight, in contrast to Winters office, which is artificially illuminated by lamps. As the film progresses, the office is more fully infiltrated by exterior light through a broken window. The shift of power is also emphasized through camera techniques. Hand-held cameras were used when filming in the yard to make the audience feel as if they were "participants in the action". However, a very precise, sterile camera composition was used in the wardens office. The prisoners world gets more precise during the film, while the colonels world is filmed more loosely. 

Costume designer Ha Nguyen also demonstrates this contrast in the clothing of the cast. The film starts with the prisoners having their clothing divided by ethnicity, with African Americans wearing different headwear, Latinos wearing vests and various arm accessories, and the White Americans in cut-off t-shirts. After the arrival of General Irwin, the prisoners start wearing more similar clothing in a "sharp military manner". The uniforms of the prisoners change from the usual chocolate brown color to light grey, because of its muddled look on film and excessive darkness in some scenes. Ha Nguyen also contrasted the non-battlefield ribbons found on Colonel Winters uniform with the battlefield medals found on General Irwins uniform (seen only in the opening scene as Irwin is inducted into the prison). 

The wall created by the prisoners in the middle of the yard also represents change and incarnation. What is at first a "discombobulated mess" representing the lack of unity among prisoners later becomes a perfect wall, a "powerful symbol of the results of   leadership". 

===Effects===
Special effects supervisor Burt Dalton and stunt coordinator Mic Rodger created the battle weapons used in the final scenes. The trebuchet, used by prisoners to throw rocks, was capable of throwing a   rock a distance of   with an accuracy of ten feet around the target. The water cannon had the power to shoot   of water per second. Some of the cast did their own stunts, including Mark Ruffalo, who performed one scene hanging from a helicopter. Interiors of the helicopter were not created with blue screen effects; instead, a special gimbal was used to hold a full-sized UH-1 Iroquois|Huey-A type military helicopter. The gimbal was capable of rotating the helicopter 360&nbsp;degrees and vertically moving it 20&nbsp;feet. The gimbal was controlled by a computer, allowing Dalton to precisely set speed and movement; this ensured precise repeatability for multiple takes.   

==Release==
 
Prior to release, DreamWorks pulled the original  poster from circulation, which depicted an American flag flying upside down (a standard distress call), due to concerns about public sensitivity related to the September 11 attacks.  

The film was released on October 19, 2001, in 2,262 North American theaters, grossing $7,088,213 on its opening weekend with an average of $3,133 per theater. The release spanned 63&nbsp;days (9 weeks), closing on December 20, 2001, with a total domestic gross of $18,244,060.  The film grossed $9,398,647 overseas, with the lowest earning in Egypt ($5,954) and the highest ($1,410,528) in Germany. 

==Reception==
  normalized rating out of 100 to reviews from mainstream critics, the film has received an average score of 42/100, based on 32 reviews. 

Mick LaSalle from the San Francisco Chronicle mentioned the cast, describing Redford as "no George C. Scott" and Gandolfini as an unusual choice to play an icy intellectual. LaSalle stated that "The Last Castle, on the surface, seems like a naive film about a great leaders capacity to inspire", but at closer look "seems to mean one thing but means another upon reflection". In general, LaSalle is in favour of the movie. 

Roger Ebert from the Chicago Sun-Times saw it as "a dramatic, involving story" but criticized its "loopholes and lapses." Ebert noted that Irwin is no less evil than Winter and that they both "delight in manipulating those they can control."  He pointed out that the film fails to portray how the prisoners manufacture the weapons and hide them under Winters observation. 

It received 3 out of 5 stars on IGN; the review noted that though a well paced and well acted film, it "suffers from this overall militaristic, streamlined approach." 

Kenneth Turan of the Los Angeles Times said the films "pretensions lead to a slow, even stately pace, what should be crackling confrontations between Irwin and Winter end up playing more like a tea party than a Wagnerian battle of wills." 

 , Spartacus, and Norma Rae." 

Variety (magazine)|Variety wrote: "Much of the potential dramatic juice has been drained out of The Last Castle, a disappointingly pedestrian prison meller that falls between stools artistically and politically." 

Claudia Puig of USA Today criticized the writing, citing "a losing battle with an implausible script." 

Elvis Mitchell of the New York Times wrote: "The movie is exuberant, strapping and obvious&mdash;a problem drama suffering from a steroid overdose." 

===Accolades=== Taurus World Stunt Award for best fire stunt and was nominated for best aerial work and best stunt coordination sequence.  Clifton Collins, Jr. was nominated for an ALMA Award in the "Outstanding Supporting Actor in a Motion Picture" category. 

==References==
 

==External links==
*  
*  
*  
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 