Streetballers
{{Infobox Film name = Streetballers image = Streetballers.jpg image_size =  caption   = Theatrical release poster  director = Matthew Scott Krentz producer =  writer =  narrator =  starring = Jimmy McKinney Matthew Krentz Adrieanne Perez music =  cinematography =  editing =  distributor =  released = North America: August 21, 2009 runtime =  country = United States language = English budget =  gross = 
}}

Streetballers is a 2009 independent film by Matthew Scott Krentz. The film tells the story of a friendship between two junior college basketball players, one black, one Irish-American, both trying to use streetball as their escape.  Jacob Whitmore, the black player is played by former University of Missouri basketball player Jimmy McKinney. Director Matthew Krentz plays the white basketball player John Hogan.

The film was shot in St. Louis, Missouri with an entirely local cast and crew. Krentz is from Webster Groves, one of the citys inner-ring suburbs.

==Plot==
Two basketball players are drawn into the lures of crime and gambling on the courts of St. Louis, Missouri tough Northside neighborhoods. Constantly searching for sanity in the midst of alcoholism, racism, and drugs, John Hogan (played by the film director Matthew Scott Krentz)and Jacob Whitmore (played by Jimmy McKinney) find release and therapy while competing at one of the most competitive street courts in the U.S.

Both must spend an entire summer helping one another overcome adversity. Their dedication and love for the game of basketball transcend from the playground courts into each of their dysfunctional households, where the two boys played the constant role of father figure.

Jacob has no choice but to play in an underground league, constantly fighting to keep his cousin Damon out of debt and possible death. John’s overpowering guilt and family trauma erupt into a state of beautiful confusion with each spiritual intervention by Terry Gibson, a neighborhood boy killed in a drunk-driving accident.

Together, the two paint a sad and hopeful portrait with their innocence, concerns, and faith in the unknown.

<!--
TO BE EXPANDED
==Production==

http://www.riverfronttimes.com/2008-07-16/film/shooting-stars-st-louis-and-homegrown-hoopsters-shine-in-streetballers
http://sports.espn.go.com/espn/wire?section=ncb&id=3514264
http://www.stltoday.com/stltoday/entertainment/reviews.nsf/movie/story/A135EFD4BC93D4EB86257617006C7238?OpenDocument

-->

==Reception==
Although the film opened in only two theaters, its per-screen opening weekend viewership was high. At more than $5700 per screen, it outstripped better-known films with nationwide showing such as   to rank ninth in the country in per-screen viewership.  

The film made a similar per-screen showing during its opening weekend in Los Angeles, taking 14th place in per-screen viewership, but grossing "more than $6,000". 

==Cast==
*Jimmy McKinney ... Jacob Whitmore
*Matt Krentz ... John Hogan
*Adrieanne Perez ... Sarah
*Eric Fletcher ... Terry Gibson
*Craig Thomas ... Damon
*Patrick Rooney ... Michael
*Justin Tatum ... Dante
*Caitlin Howley ... Caiti
*MaryBeth Scherr ... Kat
*Jordan Ward ... Ty
*Peggy Neely-Harris ... Auntie Rose
*Brian Lane ... Stepdad
*Craig Hawksley ... Mr. Henry
*Ryan Johnson ... Rufus
*Tracy Taylor ... UFC Fighter / Dantes Muscle
*Earl Easy Leonard ... Dantes Muscle
*Mike Rodgers ... UFC Fighter
*James Slim Cunningham ... Skinny Rick
*John Griffin ... Helicopter
*John Oden ... Big T
*Brandon Whitemore ... B
*Eric ... RW aka Re-Wind

==Awards==
*Won Jury Award - Honorable Mention for Best Feature Film at the Hollywood Black Film Festival (2008)
*Won Audience Choice Award Best Feature Film at the St. Louis International Film Festival(2008)
**Also won Best Dramatic Feature prize at the same festival

==References==
 

==External links==
*   - the films official site
*  

 
 
 