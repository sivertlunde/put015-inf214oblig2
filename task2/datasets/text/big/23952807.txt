Kurt & Courtney
 
 
{{Infobox film
| name           = Kurt & Courtney
| image          = kurtandcourtneydvd.jpg
| caption        =
| director       = Nick Broomfield
| producer       = Nick Broomfield
| writer         = Nick Broomfield
| narrator       = Nick Broomfield
| starring       =
| music          = David Bergeaud Dylan Carlson
| cinematography = Joan Churchill Alex Vender
| editing        = Mark Atkins Harley Escudier
| distributor    = Capitol Films
| released       =  
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
Kurt & Courtney is a 1998 documentary film by Nick Broomfield investigating the circumstances surrounding the death of Kurt Cobain, and allegations of Courtney Loves involvement in it.

==Overview==
The documentary begins as an investigation of the circumstances surrounding Cobains death and the theories which sprung up afterwards. Cobain was legally declared to have committed suicide but has been alleged by some who worked on the case, to have been murdered, in some allegations at Courtney Loves instigation.

As Broomfield investigates the claims surrounding Cobains death, his emphasis moves from the murder theories and onto an investigation of Love herself, including an accusation that she supports the suppression of free speech, and her fame after Cobains death.

The film was due to play the Sundance Film Festival but Love threatened to sue the festivals organizers if they screened the film.  Broomfield removed all of Nirvanas music and replaced it with music from bands mainly from the Seattle area. However when shown on the BBC, the film contained Nirvana (band)|Nirvanas 1991 performance of "Smells Like Teen Spirit" from Top of the Pops.

While the initial focus of the film was to explore the possible murder of Cobain, Courtney Loves refusal to license any of Cobains music and her unwillingness to speak on camera was used by Broomfield as evidence of her censorship of free speech.

===Music===
Because of Loves refusal to license Nirvanas music for the project, Nick Broomfield was forced to use various other bands from the Pacific Northwest. Notable amongst these were Zeke (band)|Zeke, the Dwarves, Rozz Rezabek and the Theater of Sheep, and Earth (American band)|Earth.

==Synopsis==
The film begins with a recap of Cobains death and the media coverage which followed. Broomfield then interviews Cobains aunt Mari who helped his love for music when he was a child. This interview is followed up with several from friends and schoolteachers who knew Cobain when he was growing up before moving onto Cobains relationship with Courtney Love.

After establishing the background the film moves on to detail the accusations that Cobain was murdered. Broomfield interviews Tom Grant, a private investigator who has alleged that Love may have conspired to kill her husband, and wants the case re-opened by the Seattle Police Department. Grant was hired by Love, but thinks it was just so people would believe that she was innocent. Hank Harrison, Courtney Loves father, is interviewed, and states he also believes that Cobain may have been killed in a conspiracy organised by Love. He has written two books about Cobains death.

The film also includes interviews with Portland drug culture celeb and former stripper, Amy Squier, about her explicit and personal knowledge of Kurt and Courtneys heroin use, and an interview with punk singer and media sideshow El Duce (real name Eldon Wayne Hoke), who claimed that Love offered him $50,000 to kill Cobain. El Duce claimed in the film that he knew who killed Cobain, but said he would "let the FBI catch him." Two days after that interview was filmed, El Duce was killed when he was hit by a train.

The film also includes an interview with musician and friend of Cobains Dylan Carlson, who had bought the shotgun that Cobain eventually used to kill himself.

Broomfield eventually moves away from the alleged conspiracy and the film turns into an investigation of Courtney Loves alleged suppression of free speech. Included in the film are phone calls from MTV saying that they were pulling out of financing the film (which was completed thanks to financing from private investors and the BBC), due to presumed pressure from Love.
 Victoria Clarke (who wrote the book Nirvana: Flower Sniffin, Kitty Pettin, Baby Kissin Corporate Rock Whores with Britt Collins) about how Love and Cobain had threatened her while doing research for her book on Cobain and Nirvana. Broomfield includes clips in the film of the threats made by Cobain and Clarke details the story of Love assaulting her.

The film concludes with Broomfield taking the stage at an ACLU meeting (where Love is a guest speaker) to publicly question Love about her attempts to suppress free speech and the irony of her representing the ACLU. He is pulled from the stage by Danny Goldberg, Cobains former manager.

==Box office==
Riding a wave of controversy, Kurt & Courtney opened in one North American theatre on 27 February 1998, where it grossed $16,835 in its opening weekend. The films final $668,228  gross was respectable considering the films limited release (only 12 theatres at its widest point), independent distribution, documentary nature, and mixed reviews.

==See also==

*Soaked in Bleach (2014 docudrama on the same subject)

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 