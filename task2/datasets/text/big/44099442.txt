Mitti Aur Sona
 
{{Infobox film
| name           =Mitti Aur Sona
| image          = 
| image_size     = 
| caption        =  Shiv Kumar	 
| producer       =Pahlaj Nihalani
| writer         =Mulraj Rajda and Debu Sen
| narrator       = 
| starring       =Chunky Pandey Sonam
| music          = Bappi Lahiri
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1989
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 Shiv Kumar and starring Chunky Pandey, Sonam,  Neelam Kothari,  Gulshan Grover,  Vinod Mehra. 

Vijay (Chunky Pandey) is from a wealthy family and is a college student. Anupama (Neelam) is his best friend and the parents of both expect them to get married. But when Vijay meets Neelima (Sonam) he falls in love with her. But his parents are against the match as Neelima is an orphan who is poor and also of questionable character. Vijay leaves home to work as a truck driver.


==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Zindagi mein pehli pehli baar"
| Asha Bhosale Shabbir Kumar
|-
| 2
| "Mitti Ban Jaye Sona"
| Asha Bhosale Amit Kumar
|-
| 3
| "Tum bewafa ho"
| Mohammed Aziz
|}


 
 
 

 