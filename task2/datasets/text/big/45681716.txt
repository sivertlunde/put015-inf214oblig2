Rogues of the Turf
{{Infobox film
| name           = Rogues of the Turf 
| image          =
| caption        =
| director       = Wilfred Noy
| producer       = 
| writer         = John F. Preston (play) Fred Groves James Lindsay
| music          = 
| cinematography = 
| editing        = 
| studio         = Carlton Films 
| distributor    = Butchers Film Service 
| released       = January 1923 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent sports Fred Groves, James Lindsay.  A plot is hatched to kidnap a race horse.

==Cast== Fred Groves as Bill Higgins  
* Olive Sloane as Marian Heathcote   James Lindsay as Capt. Clifton  
* Mavis Clair as Nellie Flaxman  Bobby Andrews as Arthur Somerton  
* Clarence Blakiston as Sir George Venning  
* Dora Lennox as Rose  
* Nell Emerald as Nurse 
* James Reardon as Rogue

==References==
 

==Bibliography==
* Low, Rachael. The History of the British Film 1918-1929. George Allen & Unwin, 1971.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 

 