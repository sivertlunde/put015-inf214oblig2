Cheriya Kallanum Valiya Policum
 
{{Infobox film
| name           = Cheriya Kallanum Valiya Policum
| image          = Cheriya Kallanum Valiya Policum.jpg
| alt            =  
| caption        = 
| director       = Haridas Kesavan
| producer       = Anand Kumar
| writer         = 
| screenplay     = 
| story          =  Mukesh Dhanya Mary Varghese Jagadeesh Suraj Venjaramood
| music          = Thej
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Cheriya Kallanum Valiya Policum is a 2010 Malayalam film directed by Haridas Kesavan. Mukesh (actor)|Mukesh, Dhanya Mary Varghese, Jagadish and Suraj Venjaramood plays the lead roles in this film.

== Plot ==
Cheriya Kallanum Veliya Policeum tells the story of a mysterious man Sadasivan (Mukesh (actor)|Mukesh) who walks into a house at a village called Pancharakkara, knowing that the man of the house Kumaran (Jagadeesh) has committed suicide. Soon he becomes dear to everyone around, and is a big help to Kumarans widow Soumini (Vidya) who has been left high and dry after her husbands demise.

The whole movie has been set around a house, where a death has taken place. Everyone who could probably add to the village scenario is around; starting from the Panchayat President who sneaks around with ulterior motives in mind at midnight, to the comrade, the local tea shop owner, the tailor and the drunkard to name a few.

Amidst these several caricatures, most of which serve no definite purpose, one surprisingly stands out. Kuttappan (Kochu Preman), the blind man who hops into a car to show the guests their way is an eye-opener. He is there on almost every scene and there are quite a few occasions when this diminutive man throws in some big surprises at us.The movie was a surprise hit at the box office due to its low cost and satellite revenues.

== Cast == Mukesh as Sadasivan
* Jagadish as Kumaran
* Alan Prince
* Vidya as Soumini
* Dhanya Mary Varghese as Sumi
* Kochu Preman as Kuttappan
* Jagathy Sreekumar Innocent
* Mala Aravindan
* Suraj Venjaramood
* Indrans
* Kozhikode Narayanan Nair
* Salim Kumar
* Ponnamma Babu

== External links ==
* http://popcorn.oneindia.in/title/7060/cheriya-kallanum-valiya-policum.html
* http://www.nowrunning.com/movie/7382/malayalam/cheriya-kallanum-valiya-policeum/index.htm
* http://www.indiaglitz.com/channels/malayalam/preview/12135.html

 
 
 


 