The Great Adventure (1918 film)
{{Infobox film
| name           = The Great Adventure
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Alice Guy-Blaché
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =     
| starring       = Bessie Love
| narrator       = 
| music          = 
| cinematography = George K. Hollister John G. Haas 
| editing        = 
| studio         = Pathé Exchange
| distributor    = Pathé Exchange
| released       =   reels   
| country        = United States Silent (English intertitles)
| budget         = 
| gross          =  
}}
The Great Adventure, also known as Her Great Adventure  and Spring of the Year,  is a 1918 silent film directed by Alice Guy-Blaché, and starring Bessie Love.

The film is extant at the BFI National Film and Television Archive (London). 

==Production==
Shot at Solax Studios in Fort Lee, New Jersey.  

==Plot==
An amateur performer (Love) has found local success and acclaim, but dreams of stardom on Broadway. She goes to New York, finds work in a Broadway chorus, and takes over the lead role when the star walks out of the production.    

==Cast==
*Bessie Love as Ragna "Rags" Jansen    
*Flora Finch as Aunt
*Chester Barnett as Billy Blake Donald Hall as Mr. Sheen
*Florence Short as Hazel Lee
*Walter Craven
*Jack Dunn

==Reception==
Love received good reviews for her performance, called "likable",  but the film itself did not.   It was said that the plot "stretches the imagination of the spectator."  Despite the critical reception, the film was commercially successful. 

==References==
;Notes
 

;Bibliography
*  

==External links==
* 
* 
* 

 
 
 
 
 

 