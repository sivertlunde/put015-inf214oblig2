One Good Turn (1936 film)
One British comedy film directed by Alfred J. Goulding and starring Leslie Fuller, Georgie Harris and Hal Gordon.  Two coffee-stall worker try to prevent their landladys daughter being cheated by a villainous theatre producer. 

==Partial cast==
* Leslie Fuller - Bill Parsons
* Georgie Harris - Georgie
* Hal Gordon - Bert
* Molly Fisher - Dolly Pearson
* Basil Langton - Jack Pearson
* Clarissa Selwynne - Ma Pearson
* Faith Bennett - Violet
* Arthur Finn - Townsend

==References==
 

==Bibliography==
* Shafer, Stephen C. British popular films, 1929-1939: The Cinema of Reassurance. Routledge, 1997.
* Sutton, David R. A chorus of raspberries: British film comedy 1929-1939. University of Exeter Press, 2000.

==External links==
* 

 
 
 
 
 
 


 