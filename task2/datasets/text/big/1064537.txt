Santa Sangre
 
{{Infobox film
| name           = Santa Sangre
| image          = Santasangre.jpg
| image_size     = 215px
| alt            = 
| caption        = Original US release poster
| director       = Alejandro Jodorowsky
| producer       = Claudio Argento
| writer         = Alejandro Jodorowsky Roberto Leoni Claudio Argento Axel Jodorowsky Blanca Guerra Adan Jodorowsky Guy Stockwell
| music          = Simon Boswell
| cinematography = Daniele Nannuzzi
| editing        = Mauro Bonanni
| distributor    = Mainline Pictures Expanded Entertainment
| released       =  
| runtime        = 123 minutes  
| country        = Mexico Italy
| language       = English
| budget         = $787,000
}} Horror film directed by Alejandro Jodorowsky and written by Jodorowsky along with Claudio Argento and Roberto Leoni. Divided into both a flashback and a flash-forward, the film, which is set in Mexico, tells the story of Fenix, a boy who grew up in a circus, and his life through both adolescence and early adulthood.

==Plot==
The film starts with a naked figure sitting in a tree in what looks like a mental asylum. Nurses come out to him, bringing a plate of conventional food and also one of a raw fish. As they try to coax him off of his perch, it is the fish that persuades him to come down. As the nurses get him to put on some overalls, the viewer sees that he has a tattoo of phoenix on his chest.

===Flashback===
The film flashes back into Fenixs childhood, which he spent performing as a "child magician" in a circus run by his father Orgo, the knife-thrower, and his mother Concha, a trapeze artist and aerialist. The circus crew also includes, among others, a tattooed woman, who acts as the object of Orgos knife-throwing feats, her adopted daughter Alma (a hearing-impaired, voiceless mime and tightrope walker whom Fenix fancies), Fenixs midget friend Aladin, a pack of clowns and a small elephant. Orgo carries on a very public flirtation with the tattooed woman, and their knife-throwing act is heavily sexualized.

Concha is also the leader of a religious cult that considers, as its patron saint, a little girl who was raped and had her arms cut off by two brothers. Their church is about to be bulldozed at the behest of the owner of the land, and the followers make one last stand against the police and the bulldozers. A Roman Catholic monsignor drives into the conflict, saying that he will prevent its demolition, but after he enters the temple to inspect it he deems it blasphemous and unworthy (the girl worshipped is no saint, he says, and the supposed pool of "holy blood" at the center of the edifice contains just red paint), so the demolition is carried out. Fenix leads Concha back to the circus, where she finds out about Orgos affair, but Orgo, being also a hypnotist, puts Concha in a trance and has sex with her.

The circus elephant then dies, much to Fenixs grief, and a public funeral is conducted, in which the elephant is paraded through the city inside a giant casket. The casket is then dropped into the city dump, where scavengers open it up and proceed to carve up the elephant and take away the Elephant meat|meat. Orgo consoles his son by tattooing a spread-eagled phoenix onto his chest, identical to the one on his own chest, using a knife dipped in red ink. This tattoo, Orgo says, will make Fenix a man.

Later on, Concha, during her act, sees Orgo and the tattooed woman sneak out of the big top. She chases after them and, seeing them sexually engaged, pours a bottle of sulphuric acid onto Orgos genitals. Orgo retaliates by cutting off both her arms (much like the girl previously venerated). He then walks into the street and slits his throat. Fenix witnesses this, locked inside a trailer. He then sees the tattooed woman driving off with Alma.

===Flash-forward===
Back in the present, Fenix is taken out to a movie theater, along with other patients, most of whom suffer from Down syndrome. A pimp intercepts them and persuades them to take cocaine and follow him to meet an overweight prostitute. Fenix then spots the Tattooed Woman, who is now a prostitute, and becomes filled with rage. Back in the asylum, Fenixs armless mother Concha calls out for him from the street and he escapes by climbing down a rope from his cell window. The Tattooed Woman is shown trying to prostitute Alma, who runs away and sleeps on the roof of a truck. The Tattooed Woman is then mutilated and killed by unseen womans hands.

Mother and son go on to perform an act whereby he stands behind her and moves his arms, to make it look as if Conchas arms are moving. But Concha soon starts to use her sons hands to kill those women who she deems a threat to her, including a young woman whom he kills with a knife-throw, as well as a cross-dressing wrestler, whom he slashes with a sword. It is revealed in a dream that he has killed many more women, all of whose memories haunt him.

Alma finds Fenix and, together, they plan to run away from Concha and her house. She tries to force Fenix to murder Alma as well, but, after a struggle, he manages to plunge a knife into Conchas stomach, but she doesnt die and reveals to him that she will always be inside him, vanishing before his eyes. Through a series of flashbacks, it is revealed that Concha actually died after being maimed by Orgo, and that Fenix has kept a mannequin of his armless mother, while performing on stage and at home. He destroys the home-made temple and throws away the mannequin with the help of his imaginary childhood friends, Aladin and the clowns.

The police are waiting outside the house and order Fenix and Alma to put their hands up. Both comply, and Fenix watches his own hands with awe as he does so. He realises that he has regained control of them.

==Cast== Axel Jodorowsky - Fenix
** Adan Jodorowsky - young Fenix
* Blanca Guerra - Concha
* Guy Stockwell - Orgo
* Thelma Tixou - The Tattooed Woman
* Sabrina Dennison - Alma
** Faviola Elenka Tapia - young Alma
* Teo Jodorowsky - Pimp
* Ma. De Jesus Aranzabal - Fat Prostitute
* Jesús Juárez - Aladin
* Sergio Bustamante - Monsignor
* Gloria Contreras - Rubi
* S. Rodriguez - Santa
* Zonia Rangel Mora - Trini
* Borolas - Box-office attendant

==Release== Anchor Bay was released in 2004. 
 rated NC-17 R rating for "bizarre, graphic violence and sensuality, and for drug content".

==Reception==
Santa Sangre has received predominantly positive reviews, with a reviewer from the British Film 4 describing it as "One of Jodorowsky’s finest films" which "resonates with all the disturbing power of a clammy nightmare filtered through the hallucinatory lens of 1960s psychedelia."  Roger Ebert gave the film a positive review, and said that he believed it carried the moral message of genuinely opposing evil, rather than celebrating it like most contemporary horror films. Ebert described it as "a horror film, one of the greatest, and after waiting patiently through countless Dead Teenager Movies, I am reminded by Alejandro Jodorowsky that true psychic horror is possible on the screen – horror, poetry, surrealism, psychological pain and wicked humor, all at once."  In recognition of its critical success, Santa Sangre ranks 476th on Empire (film magazine)|Empire magazines 2008 list of the 500 greatest movies of all time. 

The film holds an 85% rating on review aggregator website Rotten Tomatoes based on 39 reviews; the consensus states: "Those unfamiliar with Alejandro Jodorowskys style may find it overwhelming, but Santa Sangre is a provocative psychedelic journey featuring the directors signature touches of violence, vulgarity, and an oddly personal moral center." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 