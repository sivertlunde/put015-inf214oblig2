Blond Arrow
{{Infobox film
| name           = Blond Arrow
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Florián Rey
| producer       = Cesáreo González
| writer         = 
| narrator       = 
| starring       = Alfredo Di Stefano Antonio Casal Antonio Ozores
| music          = 
| cinematography = 
| editing        = 
| studio         = Suevia Films
| distributor    = 
| released       = 6 March 1956
| runtime        = 84 minutes
| country        = Spain
| language       = Spanish
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} 1956 cinema Spanish film directed by Florián Rey and starring Alfredo Di Stéfano, Antonio Casal and Antonio Ozores.  Di Stéfano, one of the stars of the Real Madrid side of the 1950s, appears as himself. The films title refers to his nickname. It was made by Suevia Films, Spains leading film studio of the era.

== Plot ==
The film is a biography of Alfredo Di Stéfano who travelled from Argentina to Madrid to try to win fame and fortune as a football player.

==References==
 

==Bibliography==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer 2008.

== External links ==
*  

 

 
 
 
 
 
 
  
 
 