Open City (film)
{{Infobox film
| name           = Open City
| image          = Open City film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =    
| hanja          =    
| rr             = Mubangbi dosi
| mr             = Mubangbi tosi}}
| director       = Lee Sang-gi
| producer       = Jeon Ho-jin
| writer         = Lee Sang-gi
| starring       = Kim Myung-min Son Ye-jin
| music          = Angelo Lee
| cinematography = Shin Ok-hyun
| editing        = Shin Min-kyung
| distributor    = CJ Entertainment
| released       =  
| runtime        = 112 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}} 2008 South Korean film.

== Plot ==
Jo Dae-yeong is a police officer investigating a pickpocket ring with ties to the Japanese crime syndicate, Yakuza. One day he rescues Baek Jang-mi from danger, only to discover that she is the boss of the gang he has been tracking.

== Cast ==
* Kim Myung-min ... Jo Dae-yeong
* Son Ye-jin ... Baek Jang-mi
* Kim Hae-sook ... Kang Man-ok
* Son Byeong-ho ... Oh Yeon-soo
* Shim Ji-ho ... Choi Seong-soo
* Yoon Yoo-sun ... Jo Soo-hyeon
* Kim Byeong-ok ... Hong Ki-taek/Hong Yong-taek
* Ji Dae-han ... Kim Kwang-seob
* Park Gil-soo ... Son Yong-soo
* Do Gi-seok ... Lee Won-jong
* Kim Joon-bae ... Franken

== Release ==
Open City was released in South Korea on 10 January 2008, " ". Koreanfilm.org. Retrieved on 26 November 2008.  and on its opening weekend was ranked second at the box office with 449,669 admissions.  The film sold a total of 1,613,728 admissions nationwide,  and as of 24 February 2008 had grossed a total of  . " ". Box Office Mojo. Retrieved on 26 November 2008. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 