That Championship Season (1982 film)
{{Infobox film
| name =That Championship Season
| image    = ThatChampionshipSeason.jpg
| caption  = DVD cover for That Championship Season (1982) Jason Miller
| producer = Menahem Golan Yoram Globus Jason Miller
| starring = Robert Mitchum Martin Sheen Bruce Dern Paul Sorvino Stacy Keach
| music    = Bill Conti John Bailey
| editing  = Richard Halsey Cannon Films
| released = 1982
| runtime  = 110 minutes
| country  = United States
| language = English
| budget   =
}} Jason Millers play of the same name. It stars Robert Mitchum, Martin Sheen, Bruce Dern, Stacy Keach and Paul Sorvino and was filmed on location in Scranton, Pennsylvania where it is set.
 a new Showtime in which he played Mitchums role as the coach. This version co-starred Vincent DOnofrio, Gary Sinise, Tony Shalhoub and Terry Kinney.

==Plot==
It has been 25 years since the 1957 Fillmore High School (fictional) basketball team won the Pennsylvania state championship. The coach and four of the victors regularly gather to relive the glory of their shining moment.

As teenage teammates they could read each others moves on the court without fail.  As middle-aged men, each is facing his own different mid-life crisis. With a former coach that still addresses problems as if his boys are having a bad game, the friends longtime loyalty to one another begins to unravel.

George Sitkowski (Bruce Dern) is mayor of Scranton and engaged in a fierce campaign for re-election. James Daley (Stacy Keach) is an overwrought and underpaid school principal while his brother, Tom (Martin Sheen), has become a drifter with a serious drinking problem. Phil Romano (Paul Sorvino) is the wealthiest among them. He often bends the law and even betrays a friend to indulge his own needs, but George badly needs his support.

The intended celebratory nature of this reunion is quickly dissipated. Various contentions arise among the four old teammates, who quickly turn on one another. The coachs bigotry—then and now—and his selfish disregard for fair play are brought again to the surface. The absence of the teams star player, who hates the coach, serves to further spotlight the futility and hollowness of this gathering.

==Cast==
*Robert Mitchum as Coach Daniel B. Delaney
*Martin Sheen as Tom Daley
*Stacy Keach as James Daley
*Bruce Dern as George Sitkowski
*Paul Sorvino as Phil Romano
*Arthur Franz as  Macken

==Production==
Jason Millers play opened off-Broadway in 1972. After critical acclaim and 144 performances, it moved to a Broadway theater for a run of 844 more performances before closing on April 21, 1974.

For years Miller attempted to bring the play to the big screen before succeeding in 1982. As writer and director, he insisted on filming the exteriors in Scranton, Pennsylvania, where the story takes place. Miller was raised and educated there and his intent was to showcase the city and its people.
 Nay Aug Park (featuring a political rally using the townspeople as extras), the Delaware, Lackawanna & Western Railroad headquarters, the Martz Trailways terminal, Lackawanna Avenue, the city council chambers and more.
 score by The Right For Your Eyes Only) was supplemented by the West Scranton High School Band. No soundtrack was ever released.
 Jason Miller The Exorcist). Before he dropped out, Friedkin offered the part of Coach Delaney to William Holden. Holden was interested but died before he could accept the part. 

The stars and director were paid $250,000 each plus a percentage of the profits. The film did a great deal to help the reputation of the fledgling Cannon Films, who financed the movie. Andrew Yule, Hollywood a Go-Go: The True Story of the Cannon Film Empire, Sphere Books, 1987 p25 

==Awards==
Bruce Dern won the Silver Bear for Best Actor at the 33rd Berlin International Film Festival.   

However, the film was not a hit at the box office, and it lost money. 

==Home media==
In 2004, the film was released on DVD by MGM Home Entertainment.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 