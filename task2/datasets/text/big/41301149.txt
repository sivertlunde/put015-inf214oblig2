Pure (2009 film)
{{Infobox film
| name     = Pure
| image    = Pure film.jpg
| director = Lisa Langseth
| producer = 
| writer   = Lisa Langseth
| starring = Alicia Vikander Samuel Fröler
| cinematography = 
| music    = 
| country  = Sweden
| language = Swedish
| runtime  = 97 minutes
| released =  
}}
Pure is a 2009 Swedish drama film directed and written by Lisa Langseth. Its Swedish language title is Till det som är vackert which approximately translates to "To that which is beautiful".

Lead actress Alicia Vikander won the prestigious Swedish Guldbagge Award for best actress for her role in this film.

== Plot ==
Katarina (played by Alicia Vikander) is twenty years old and lives around Gothenburg. She didnt finish school and her mother is an alcoholic. Through a YouTube-video she is touched by the music of Wolfgang Amadeus Mozart. She applies for a receptionist job at the concert hall where she meets conductor Adam (played by Samuel Fröler).

== Cast ==
* Alicia Vikander as Katarina
* Samuel Fröler as Adam

== External links ==
*  

 
 
 
 
 


 