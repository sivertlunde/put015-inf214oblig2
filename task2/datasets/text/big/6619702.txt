Pippi Longstocking (1997 film)
 
{{Infobox Film
| name           = Pippi Longstocking
| image          = Movie poster pippi.jpg
| image_size     = 190px
| alt            = 
| caption        = Video release poster
| director       = Clive A. Smith Michael Hirsh
| writer         = Frank Nissen
| based on       =   Dave Thomas Wayne Robson Carole Pope
| music          = Anders Berglund Asher Ettinger Tony Kosinec
| editing        = Noda Tsarmados
| studio         = Nelvana Limited AB Svensk Filmindustri
| distributor    = Legacy Releasing   Warner Bros. Family Entertainment  
| released       = August 22, 1997
| runtime        = 75 minutes 81 minutes  
| country        = Sweden Germany Canada
| language       = English
| budget         = 80 million kr ($11.5 million)
| gross          = $505,335 
}}
Pippi Longstocking is a 1997  .

==Plot==
The film begins with Pippi (Melissa Altro) sailing around the world with her father, Captain Efraim Longstocking (Gordon Pinsent), her pet horse, Horse, her pet monkey, Mr. Nilsson, and various members of the ships crew. One night during a hurricane, the captain is washed over board into the sea. As he drifts off, he calls to Pippi that he will "meet her in Villa Villekulla". To that effect, Pippi and her pet animals make their way home, Villa Villekulla, to await his return. Not long after arriving, she makes friends with the two children across the street — Tommy and Annika (Noah Reid and Olivia Garratt), who are captivated by her free spirit and fun-loving attitude. They soon convince her to go to school (for the first time in her life) where she gets into trouble, despite winning the hearts of her classmates.

Pippi also soon attracts the attention of a local social worker, Mrs. Prysselius (Catherine OHara), who conspires to put her into foster care. When Miss Prysselius goes to speak with the local law enforcement of the need for the girl to be placed in a home for orphans, she lets certain details (her lack of adult supervision, living alone, having a large supply of gold coins kept out in the open, and most of all, leaving her door unlocked) be revealed to a pair of thieves already in jail. The thieves, Bloom and Thunder-Karlsson, decide to rob Pippi themselves once they break out of jail.

Pippi and her friends take part in many adventures and close-calls, winning over almost everyone, with the exception of Miss Prysselius and Tommy and Annikas parents. Just when Miss Prysselius has had enough and is about to drag Pippi straight to the childrens home herself, Pippis father returns to take her back to their life on the sea. However, Pippi decides that she cant leave her new friends and decides to stay in Villa Villekulla.

==Songs==

The show begins with Pippi singing "Theres Magic Everywhere", in which she recalls her adventures undergone while traveling at sea.
As she walks through town on her way to Villa Villekulla, she sings "Come and Be Happy", a song describing various lightening events.  
When Tommy and Annika come over to her house for the first time, she sings the pancake making song, a fast-paced song where Pippi adds "ingredients", which are actually enjoyable trips.  This song somewhat conveys that happiness is the key to enjoying life.  
After the villains overhear Mrs. Prysellius ranting about Pippis shortcomings, they sing a song, imagining the many things they will do after they successfully burgle Pippis chest of gold coins.
The rest of the movie requires no songs, except for the credits where "Theres Magic Everywhere" is played.

==Cast==
* Melissa Altro as Pippilotta Delicatessa "Pippi" Longstocking
* Catherine OHara as Mrs. Prysselius
* Gordon Pinsent as Captain Efraim Longstocking Dave Thomas as Thunder-Karlsson
* Wayne Robson as Bloom
* Carole Pope as Teacher
* Noah Reid as Tommy Settegren
* Olivia Garratt as Annika Settegren
* Jan Sigurd as Const. Kling
* Tomas Bolme as Const. Klang
* Richard Binsley as Mr. Nilsson Michael Bell as animal effects

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at British Film Institute|bfi.org.uk (with cast and crew credits)

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 