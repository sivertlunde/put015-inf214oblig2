Teen Kanya
 
{{Infobox film
| name = Teen Kanya
| image = Dvd teen kanya satyajit ray.jpg
| caption = A poster for Teen Kanya
| director = Satyajit Ray
| producer =
| writer = Satyajit Ray (screenplay) Rabindranath Tagore (stories)
| starring =Soumitra Chatterjee   Aparna Sen  
| music =
| cinematography =
| editing =
| distributor = Sony Pictures
| released = 5 May 1961
| runtime = 173 min.
| language =
| budget =
| preceded_by =
| followed_by =
}} Bengali anthology film directed by Satyajit Ray, and based upon short stories by Rabindranath Tagore. The title means "Three Girls", and the films original Indian release contained three stories. However, the international release of the film contained only two stories, missing out the second ("Monihara: The Lost Jewels"). This version was released on VHS in 1997 under the title Two Daughters. However, there are now DVD versions available that contain all three films.

==Cast==

===1.The Postmaster ===
*Chandana Banerjee -Ratan
*Nripati Chatterjee -Bishey
*Anil Chatterjee -Nandal
*Khagen Pathak -Khagen
*Gopal Roy -Bilash

===2.Monihara (The Lost Jewels)===
*Kali Banerjee -Phanibhushan
*Kanika Majumdar -Manimalika
*Kumar Roy -Madhusudhan
*Gobinda Chakravarti -Schoolmaster and narrator

===3. Samapti (The Conclusion)===
*Soumitra Chatterjee -Amulya
*Aparna Sen -Mrinmoyee
*Sita Mukherjee -Jogmaya
*Gita Dey -Nistarini
*Santosh Dutta -Kishori
*Mihir Chakravarti -Rakhal
*Devi Neogy -Haripada

==Awards== National Film Awards
*   - Samapti   

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 

 