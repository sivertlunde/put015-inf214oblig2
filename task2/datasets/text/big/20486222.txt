Evel Knievel (film)
{{Infobox film
| name           = Evel Knievel
| image          = Evel Knievel 1971.jpg
| director       = Marvin J. Chomsky George Hamilton Joe Solomon
| writer         = Alan Caillou John Milius George Hamilton Sue Lyon Bert Freed Patrick Williams
| distributor    = Fanfare Films
| released       =  
| running time   = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $4,000,000 (US/ Canada rentals) 
}}
 George Hamilton as motorcycle daredevil Evel Knievel. 

==Story==
The story is a biography of the famed motorcycle daredevil, who grew up in Butte, Montana. The film depicts Knievel reflecting on major events in his life, particularly his relationship with his girlfriend/wife, Linda. The film opens with Knievel at the Ontario Motor Speedway in Ontario, California. Knievel is speaking directly to the camera describing his upcoming daredevil motorcycle jump:

:Ladies and gentlemen, you have no idea how good it makes me feel to be here today. It is truly an honor to risk my life for you. An honor. Before I jump this motorcycle over these 19 cars&nbsp;— and I want you to know theres not a Volkswagen or a Datsun in the row&nbsp;— before I sail cleanly over that last truck, I want to tell you that last night a kid came up to me and he said, "Mr Knievel, are you crazy? That jump youre going to make is impossible, but I already have my tickets because I want to see you splatter." Thats right, thats what he said. And I told that boy last night that nothing is impossible. Now they told Columbus to sail across the ocean was impossible. They told the settlers to live in a wild land was impossible. They told the Wright Brothers to fly was impossible. And they probably told Neil Armstrong a walk on the moon was impossible. They tell Evel Knievel to jump a motorcycle across the Grand Canyon is impossible, and they say that every day. A Roman General in the time of Caesar had the motto: "If it possible, it is done. If it is impossible, it will be done." And that, ladies and gentlemen, is what I live by.

Following his introduction, the story follows a flashback narrative through Knievels life.

The film ends with Knievel successfully completing the jump at the Ontario Motor Speedway and riding off onto a dirt road which leads to the edge of the Grand Canyon. (At the time of production, the real Evel Knievel was hyping a jump over the Canyon.)

==Monologue==
As the movie closes over the Grand Canyon, George Hamilton delivers a voice-over monologue in the Knievel character. In the monologue, he describes himself as the "last gladiator", which would later be used by the real Evel Knievel in his 1998 documentary, The Last of the Gladiators.

Below is a transcript of the monologue from the movie:
:Important people in this country, celebrities like myself — Elvis, Frank Sinatra, John Wayne — we have a responsibility. There are millions of people that look at our lives and it gives theirs some meaning. People come out from their jobs, most of which are meaningless to them, and they watch me jump 20 cars, maybe get splattered. It means something to them. They jump right alongside of me — they take the bars in their hands, and for one split second, they’re all daredevils. I am the last gladiator in the new Rome. I go into the arena and I compete against destruction and I win. And next week, I go out there and I do it again. And this time — civilization being what it is and all — we have very little choice about our life. The only thing really left to us is a choice about our death. And mine will be — glorious.

==Cast== George Hamilton as Evel Knievel
*Sue Lyon as Linda
*Bert Freed as Doc Kincaid Rod Cameron as Charlie Knesson
*Dub Taylor as Turquoise Smith
*Ron Masak as Pete
*Hal Baylor as The Sheriff

==Production==
===Development===
George Hamilton was writing a story about a bronc rider who became a motorcycle rider. While preparing to film it, he interviewed various stunt men for the lead role and heard about Knieval. He visited the stuntman in a San Fransisco hospital and found him more fascinating than what he was writing. In 1970, Hamilton stated that:
 In America weve long had a theory that all men have an equal right to become everything they want. But theres a new theory being pushed on us - that every man has to be something whether he wants to or not. Thats what the theory of Evil Knievel is about. Hes an individual who doesnt care about establishment or hippie, both have their phony sides. Im not sure why Evil does what he does on a motorcycle. But I do know that by the time the picture is finished Ill be able to say it in one sentence. Hollywood Today: Norma Takes a Look at the Reasons Why Cutie-pie, Georgie Boy Survived the Survivors
Norma Lee Browning. Chicago Tribune (1963-Current file)   08 Feb 1970: n2.   
The screenplay was originally written by  , Uni of California 2006 p 289 

Milius says Knievel "saw himself as the new gladiator of the new Rome, something larger than a daredevil. He saw the whole spectacle of civilization and the absurdity of what its turned into, and he fit into that." 

Milius later called Hamilton "a wonderful guy, totally underrated. A great con-man, thats what he really is. He always said, Ill be remembered as a third-rate actor when in fact, Im a first-rate con man." 

Hamilton later recalled:
 Milius made me read the script to Evel. I realized he was kind of a sociopath and was totally messed. Then all of sudden Evel started to adopt lines out of the movie for himself. So his persona in the movie became more of his persona in real life. He would have been every kid’s hero on one hand, but then he went and took that baseball bat and broke that guy’s legs and that finished his career in the toy business. Evel was very, very difficult and he was jealous of anybody that was gonna play him. He wanted to portray himself and he did go and make his own movie later on. He had a great perception of this warrior that he thought he was and that was good. Then he had this other side of himself where he’d turn on you in a minute. Success is something that you have earn. You have to have a humility for it, because it can leave you in a second. It may remember you but it can sure leave you. I think if you don’t get that and you don’t have gratitude for what you are and where you are it doesn’t come back and it goes away forever.  

===Shooting===
The picture was directed by Marvin J. Chomsky and was released on September 10, 1971. 

Much of the film was shot in Butte, Montana. Actual footage of Knievel jumping his motorcycle was used throughout the film.  Additionally, Knievel performed a series of new jumps at the Ontario Motor Speedway for the production, including a spectacular record jump of 129 feet over 19 cars that was included in the film (Knievel held the record for jumping a Harley-Davidson motorcycle over 19 cars for 27 years, until broken by Bubba Blackwell in 1998).  Knievel received a flat rate of $25,000 for his rights and the consulting fee. 

The motion picture has fallen out of copyright and is in the public domain. The most common version of the film (available on DVD and various streaming media) is from a faded 16&nbsp;mm print. Scratches on both the audio and video track are easily detectable.
 Patrick Williams. The title song, "I Do What I Please", is played throughout the film, including the opening and closing credits, and the montage of the real Evel Knievels stunt riding.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 