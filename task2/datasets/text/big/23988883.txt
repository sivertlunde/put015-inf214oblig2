The Panther's Claw
 
{{Infobox film
| name           = The Panthers Claw
| image          = 
| caption        = 
| director       = William Beaudine
| producer       = Lester Cutler (producer) T.R. Williams (associate producer)
| writer         = Anthony Abbot (story) Martin Mooney (screenplay)
| narrator       = 
| starring       = See Cast
| music          = 
| cinematography = Marcel Le Picard
| editing        = Frederick Bain
| distributor    = Producers Releasing Corporation 
| released       =  
| runtime        = 70 minutes
| country        = United States English
| budget         = 
| gross          = 
| website        = 
}}

The Panthers Claw is a 1942 American film directed by William Beaudine. A Producers Releasing Corporation picture.

== Plot summary ==

Police see Digberry climbing over a wall, out of a graveyard at one a.m. He has left a thousand dollars inside on a grave, as ordered in a letter he received. The money is now gone. They take him to headquarters. He reveals the letter came from “the Panther” and is now believed as others have had letters from the same character and Digberry meets a group of them. They all got typewritten letters demanding a thousand dollars too but unlike him they consulted the police. The group all turn out to be connected (Digberry makes wigs). Baritone, Enrico Lombardi looks to be a possible candidate so the Commissioner orders him brought in.

Lombardi tries to force himself on Nina Politza and hits Digberry who intervenes, for bringing him into this affair. Nina is reported as sailing on a ship to Buenos Aries, as planned. Digberry receives a call and goes out. While out, the police are let into his apartment and try his typewriter to see if the “H” matches that on the Panther’s letters and if his cat’s inked paw is the same mark as on the letters. They are in both cases.

Digberry returns and is taken to headquarters. Digberry now tells the Commissioner he wrote the letter to blackmail himself to cover up from his wife and five (grown up) daughters the thousand dollars he drew out of his account but refuses to say what it was for. Meanwhile a woman has been found murdered in his apartment block and next to her is a signed picture of Digberry. Investigation reveals that the dead woman, wearing a gray wig, is Nina. Digberry’s unlikely alibi is that he was waiting on a street corner as per a phone call he had received.

Orders go out to pick up Nina’s ex-husband and Lombardi and the Commissioner follows up a wig lead Digberry gave him, about gray hair found from an inferior wig to the kind Digberry makes. The wig maker, Wilkins tells him it could be from a wig he made for Galloway or for Lombardi. Captain Walters (Captain because he served in France in WWI) who managed Nina, offers a thousand dollar reward for the capture of her murderer. A report is given that Lombardi is out of town and no such person as Galloway or the address given the wig maker exists.

Wilkins is to be at the police station next morning to see if he can recognise who bought the second gray wig from a list of suspects. Lombardi is finally caught, and has a gun with one shot fired. That night, Wilkins is shot and killed to stop him identifying a possible murderer. Digberry’s apartment is searched and a missing insurance policy found, signed over to him, worth $20,000. The murder weapon, put in one of his drawers by an unseen person is found too.

Digberry returns and explains to the Commissioner that he loaned her the thousand dollars because though everyone thought she was well off, she was broke. She signed the insurance over to him as surety. She could not afford to go away as before, so got off the ship before it sailed and planned to stay in town in disguise (the wig, etc.) as the knowledge that she had no money could have hurt her career. Digberry is off to headquarters again where the Commissioner finally reveals all and the villain is taken away to jail. The name of the killer is on the "discussion" page.

The picture was dedicated to the NYPD. Anthony Abbot the writer named one of the main characters in it after himself. The Panther never existed in any form and was just something made up by Digberry very early in the picture.

== Cast ==
*Sidney Blackmer as Police Commissioner Thatcher Colt
*Rick Vallin as Anthony Tony Abbot
*Byron Foulger as Everett P. Digberry
*Herbert Rawlinson as District Attorney Bill Dougherty
*Lynn Starr as Miss Spencer
*Barry Bernard as Edgar Walters
*Gerta Rozan as Nina Politza
*Thornton Edwards as Enrico Lombardi John Ince as Police Captain Mike Flynn
*Martin Ashe as Officer Murphy Walter James as Police Captain Tim Henry
*Frank Darien as Samuel Wilkins
*Joseph DeVillard as Antonio Spagucci
*Jack Van as Giuseppe Bartarelli
*Willy Castello as John Martin George Harry Clark as Officer Lou Levinsky
*Lew Leroy as Apartment Manager Billy Mitchell as Nicodemus J. Brown
*Florence OBrien as Petunia
*Harte Wayne as Coroner
*Pat McKee as Joe Morgan
*Casey MacGregor as Officer Flanagan
*Billy Snyder as Hotel Manager

Dick Rush appears uncredited as Lombardis Police Escort

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 