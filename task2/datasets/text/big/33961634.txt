Chingari (2012 film)
 
 
 
{{Infobox film
| name           = Chingari
| image          = Chingari.jpeg
| alt            =  
| caption        =  Harsha
| producer       = B. Mahadevu
| writer         = 
| screenplay     = 
| story          =  Darshan Deepika Bhavana Srujan Lokesh Yashas Surya
| music          = V. Harikrishna
| cinematography = H. C. Venu
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India
| language       = Kannada
| budget         =    
| gross          = 
}}  Kannada action Darshan and Deepika Kamaiah in the lead roles. The film is directed by Harsha (director)|A. Harsha and V. Harikrishna is the music director of the film.It is also dubbed into Hindi as Chingaara. {{cite web url = http://www.chitraloka.com/2011/11/20/chingaari-release-on-january-27
| archiveurl = http://wayback.archive.org/web/20120302131527/http://www.chitraloka.com/2011/11/20/chingaari-release-on-january-27
| title = Chingaari Release on January 27
| archivedate = 2 March 2012
| deadurl = yes
| publisher = chitraloka.com 
| date = 20 November 2011 
| accessdate = 8 February 2013
}}  B. Mahadevu produced the film with a budget of  .   It is Inspired by Hollywod flick Taken.

== Plot ==
The movie revolves around a Super cop C.C.B. officer Dhanush (Darshan) as he sets out to rescue his kidnapped girlfriend in Switzerland who is about to be sold into Human trafficking|prostitution.

== Cast == Darshan as Dhanush
* Deepika Kamaiah as Geetha Bhavana
* Srujan Lokesh
* Yashas Surya
* Vera Prunn
* Richard Cobill (actor) as Jamshem

==Release==
The movie got released in over 180+ theaters on 3 February 2012. {{cite web
| url = http://www.chitraloka.com/2012/02/03/chingari-darshans-biggest-multiplex-release/
| archiveurl = http://wayback.archive.org/web/20120322225606/http://www.chitraloka.com/2012/02/03/chingari-darshans-biggest-multiplex-release/
| archivedate = 22 March 2012
| deadurl = yes 
| title =  Chingaari Darshans Biggest Multiplex Release
| date = 3 February 2012
| publisher = chitraloka.com 
| accessdate = 8 February 2013
}}  Chingaari collected about   gross in its first day, {{cite web
| url = http://www.chitraloka.com/component/content/article/17-exclusive/577-chingaari-collects-two-crores-on-day-one.html 
| title = Chingaari Collected Two Crores on Day One
| publisher = chitraloka.com 
| accessdate = 8 February 2013
}}  Chingari also released in some of the Bangalore theaters which were screening only non-Kannada films with a great success. {{cite web
| url = http://www.chitraloka.com/2012/02/06/chingaari-in-majority-of-non-kannada-theatres
| archiveurl = http://wayback.archive.org/web/20120311014854/http://www.chitraloka.com/2012/02/06/chingaari-in-majority-of-non-kannada-theatres
| archivedate = 11 March 2012
| deadurl = yes
| title = Chingaari in Majority of Non-Kannada Theatres
| publisher = chitraloka.com 
| date = 11 March 2012
| accessdate = 8 February 2013
}} 

==Soundtrack==
{{Infobox album  
| Name        = Chingari
| Type        = Soundtrack
| Artist      = V. Harikrishna
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    = Feature film soundtrack
| Length      = 
| Label       = Ashwini Audio
}}
{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Baare Baare Kaviraj
| extra1        = Kailash Kher
| length1       = 
| title2        = Gamanava
| lyrics2 	= Jayanth Kaikini
| extra2        = Javed Ali, Shreya Ghoshal
| length2       = 
| title3        = Kai Kaiyya
| lyrics3       = Yogaraj Bhat
| extra3 	= V. Harikrishna
| length3       = 
| title4        = Nee Midiyuve
| extra4        = Soumya Mahadevan Kaviraj
| length4       = 
| title5        = Chingari Theme
| extra5        = Instrumental
| lyrics5 	= 
| length5       = 
}}
{{Album ratings
 
| rev1 =Times of India
| rev1Score =  
| rev2 =CNN-IBN
| rev2Score =    
| rev3 =Rediff
| rev3Score =     
| rev4 =Bangalore Mirror
| rev4Score =   
||}}

The audio release function of Chingaari is directed by dance director turned director Harsha.A (Director/Choreographer)|Harsha.A. Sudeep made the audio release on 2 January 2012. V. Harikrishna has composed 5 songs set to the lyrics of Kaviraj, Yogaraj Bhat and Jayanth Kaikini {{cite web
| url = http://www.chitraloka.com/2012/01/03/chingaari-audio-released/
| archiveurl = http://wayback.archive.org/web/20120108072343/http://www.chitraloka.com/2012/01/03/chingaari-audio-released/
| archivedate = 8 January 2012
| date = 3 January 2012
| deadurl = yes
| title = Chingaari Audio Released
| publisher = chitraloka.com 
| accessdate = 8 February 2013
}} 

==Reception==

Rediff gave 3 stars for the movie and appreciated Venus camerawork and also appreciated Darshan and Deepika Kamaiah performance.  CNN IBN gave it 3 stars and said, "Darshan impresses with his stylish looks, comical acts and breathtaking action sequences. He dominates the entire film with his superb screen presence." 

==Box office==
Its first week gross collections crossed   60.7&nbsp;million, according to the distributor Prasad. The distributor’s share would be about   50&nbsp;million in the first week of its release.It completed 50 days in many theaters. {{cite web
| url = http://www.chitraloka.com/2012/02/10/samarth-ventures-happy-about-chingaari 
| archiveurl = http://wayback.archive.org/web/20120318151546/http://www.chitraloka.com/2012/02/10/samarth-ventures-happy-about-chingaari 
| archivedate = 18 March 2012
| deadurl = yes
| date = 10 February 2012
| title = Samarth Ventures Happy about Chingaari
| publisher = chitraloka.com 
| accessdate = 8 February 2013
}}  {{cite web
| author = Ramchander 
| url = http://entertainment.oneindia.in/kannada/news/2012/darshan-chingari-upendra-arakshaka-100212.html 
| title = Darshan starrer Chingari overpowers Upendras Arakshaka – Oneindia Entertainment 
| publisher = Entertainment.oneindia.in 
| date = 10 February 2012 
| url = http://www.chitratara.com/show-content.php?id=3560&ptype=News&title=CHINGARI%20PRODUCER%20DEEP%20ANGUISH%20ON%20DARSHAN 
| title = Chingari Producer Deep Anguish On Darshan! 
| publisher = Chitratara.com 
| date = 3 March 2012
| accessdate = 8 February 2013
}}  

==References==
 

==External links==
*  
*  

 
 
 
 
 