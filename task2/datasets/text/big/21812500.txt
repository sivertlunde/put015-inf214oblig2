The Who at Kilburn: 1977
 
 
{{Infobox film
| name           = The Who at Kilburn: 1977
| image          = 
| image size     =
| border         =
| alt            =
| caption        =
| director       = Jeff Stein The Who Chris Stamp Kit Lambert
| producer       = Nigel Sinclair
| writer         =
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       = Roger Daltrey Pete Townshend John Entwistle Keith Moon Johnny Kidd Allen Toussaint Mose Allison
| cinematography =
| editing        = Mark Stepp Parris Patton
| studio         = Spitfire Pictures
| distributor    = Image Entertainment
| released       =  
| runtime        = 138 minutes
| country        =
| language       = English
| budget         =
| gross          =
}}
 Gaumont State London Coliseum on 14 December 1969. The film restoration was produced by Nigel Sinclairs Spitfire Pictures in association with Trinifold Management.
 The Kids The Kids The Kids Who Are You", which would also be their last performance of the song with Moon on stage.

The Coliseum concert on 14 December 1969 was recorded during a tour of European opera houses. Combined with the features in the Extras, nearly the entire performance can be seen.

== Background, production and restoration ==
The footage for the two concerts that appeared on The Who at Kilburn: 1977 was found by Nick Ryle in 2002. The first recorded performance, at the Coliseum, was filmed by Chris Stamp and Kit Lambert—who were the groups managers at that time—using 16 mm film with the music being recorded on a two-track tape recorder. The concert at the Gaumont State Theatre was filmed by Jeff Stein and the Who using 35 mm film and a 16-track tape recorder.   
 Paul Crowder. The sound on the recordings was mixed and balanced by Paul Clay with help from Jon Astley. The film footage was edited by Mark Stepp and Parris Patton. 

== Songs performed ==
=== Disc 1: The Who at Kilburn 1977 ===
# "I Cant Explain" (3:10)
# "Substitute (The Who song)|Substitute" (2:59)
# "Baba ORiley" (5:18)
# "My Wife" (7:02)
# "Behind Blue Eyes" (3:33)
# "Dreaming from the Waist" (5:10)
# "Pinball Wizard" (2:43)
# "Im Free (The Who song)|Im Free" (2:45)
# "Tommys Holiday Camp" (1:30)
# "Summertime Blues" (3:45)
# "Shakin All Over" (5:00) My Generation" (3:44) Join Together" (2:28) Who Are You" (6:04)
# "Wont Get Fooled Again" (8:49)
# "(End Credits)" (1:33)

=== Disc 2: Main Title – The Who at the Coliseum 1969 ===
# "Heaven and Hell" (4:23)
# "I Cant Explain" (2:47) Fortune Teller" (2:39)
# "Tattoo (The Who song)|Tattoo" (3:37)
# "Young Man Blues" (11:10)
# "A Quick One, While Hes Away"   (3:48) Happy Jack" (2:15)
# "Im a Boy" (2:54)
# "Theres a Doctor" (0:22)
# "Go to the Mirror!" (3:25)
# "Im Free (The Who song)|Im Free" (2:23)
# "Tommys Holiday Camp" (0:48)
# "See Me, Feel Me" (5:07)
# "Summertime Blues" (3:21)
# "Shakin All Over" (7:12)
# "My Generation" (15:07)
# "(End Credits)" (1:30)

== Extras ==
On Disc 2 in the main title movie (total time of 1:12:48), the full performances of the groups   (performed just before "Happy Jack") is the only song performed at the show that is missing from the DVD. The movie trailer for the entire release is also included in the extras.

=== A Quick One, While Hes Away ===
# "Introduction" (6:03)
# "A Quick One, While Hes Away" (11:46)

=== Tommy ===
# "Overture (The Who song)|Overture" (5:44)
# "Its a Boy" (0:33)
# "1921" (2:33)
# "Amazing Journey / Sparks" (8:27)
# "Eyesight to the Blind (The Hawker)" (1:57)
# "Christmas" (3:15)
# "The Acid Queen" (3:27)
# "Pinball Wizard" (2:46)
# "Do You Think Its Alright?" (0:22)
# "Fiddle About" (1:15)
# "Tommy, Can You Hear Me?" (0:56)
# "Theres a Doctor" (0:22)
# "Go to the Mirror!" (3:25)
# "Smash the Mirror" (1:15)
# "Miracle Cure" (1:15)
# "Sally Simpson" (4:01)
# "Im Free (The Who song)|Im Free" (2:25)
# "Tommys Holiday Camp" (0:58)
# "Were Not Gonna Take It (The Who song)|Were Not Gonna Take It" (8:47)

=== "The Who at Kilburn Trailer" ===
# "(Trailer)" (1:31)

== Charts ==
{|class="wikitable sortable" border="1" Chart (2008) Peak position
|- Australian Top 40 Music DVDs  33
|- Belgium (Flanders) Top 10 Music DVDs  9
|- Belgium (Wallonia) Top 10 Music DVDs  5
|- Italian Top 20 Music DVDs  9
|- Netherlands Top 30 Music DVDs  23
|- Swedish Top 20 DVDs  11
|-
|}

== References ==
 

 

 
 
 
 
 
 
 