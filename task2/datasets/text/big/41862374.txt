Tiger House
 
{{Infobox film
| name           = Tiger House
| image          = Tiger House film poster.jpg
| alt            = Four home-invading men stand in the foreground with a scaled-up teenage woman in the background armed with a bow and arrow, with a burning house between the foreground and background.
| caption        = Promotional poster
| film name      = 
| director       = Thomas Daley
| producer       = {{Plainlist |
* Richard Mansell
* Tarquin Glass
* Ronnie Apteker
}}
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist |
* Kaya Scodelario
* Dougray Scott
* Ed Skrein
}}
| music          = Roger Goula
| cinematography = 
| editing        = Gary Forrester
| production companies = {{Plainlist |
* Glass Man Films
* Tiger House Film
}}
| distributor    = Koch Media  (United Kingdom) 
| released       =  
| runtime        = 
| country        = {{Plainlist |
* United Kingdom
* South Africa
}}
| language       = English
| budget         = 
| gross          = 
}}
Tiger House is an upcoming English-language thriller film directed by Thomas Daley and starring Kaya Scodelario, Dougray Scott, and Ed Skrein. Scodelario stars as an injured gymnast who must defend her boyfriends house against a home invasion. The film is produced by the United Kingdom-based Glass Man Films and the South Africa-based Tiger House Film. The film, set in the United Kingdom, was filmed in South Africa. Koch Media acquired distribution rights for the United Kingdom and plans to release Tiger House in the second quarter of 2015.

==Premise==

Screen Daily reported the premise, "An injured gymnast... must defend her boyfriends house from a gang of armed robbers." 

==Cast==

*Kaya Scodelario 
*Dougray Scott 
*Ed Skrein 
*Langley Kirkwood 
*Brandon Auret 

==Production==
 Cape Town, South Africa. Filming also took place in Wynberg, Cape Town|Wynberg, a suburb of Cape Town. With a cast of nine actors, filming lasted for five weeks. Post-production of the film has been completed in the UK.    The film was completed by late 2014. 

==Release==

Altitude Film Sales began to sell distribution rights in May 2013.  In the following December, ZDF Enterprises acquired distribution rights for German-speaking territories in Europe, and Gulf Film acquired rights for the Middle East.  In November 2014, Koch Media acquired rights to distribute Tiger House in the United Kingdom and scheduled it to be released in the second quarter of 2015.   

==See also==

*List of films featuring home invasions

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 