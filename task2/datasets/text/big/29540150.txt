Players (film)
 
 
 

{{Infobox film
| name           = Players
| image          = Players Official.jpg
| alt            = 5 people are seen in the poster, three men and two women. Two of the men are sitting atop of gold bricks, holding guns. The other three are standing behind them. Both women are wearing revealing tops, showing their torso, one of them also has a gun in her hand.
| caption        = Theatrical release poster
| director       = Abbas-Mustan
| producer       = Pappu Vaswani   Mohammed Burmawala   Abbas-Mustan  Viacom 18
| based on       =  
| story          = Troy Kennedy Martin   (Original Story)   Nikhat Bhatty   Bhaskar Hazarika   (Story Adapted) 
| screenplay     = Troy Kennedy Martin   (Original Screenplay)   Donna Powers   Wayne Powers   (1st Screenplay Adapted)    Rohit Jugraj   Sudip Sharma   (2nd Screenplay Adapted) 
| writer         = Sudip Sharma   (Dialogue) 
| starring       = Abhishek Bachchan Bipasha Basu Sonam Kapoor  Neil Nitin Mukesh Bobby Deol Sikandar Kher Omi Vaidya
| music          = Songs: Pritam Background Score: Sandeep Shirodkar
| cinematography = Ravi Yadav
| editing        = Hussain Burmawala
| studio         = Burmawala Partners
| distributor    = Viacom 18
| released       =   (Dubai)     (Kuwait)     (London and worldwide) 
| runtime        = 163 minutes
| country        =
| language       = Hindi
| budget         =   to     
| gross          =   
}}
 Indian action Abbas and Hollywood blockbuster, The Italian British caper of the same name.  Players employs the same plot as the 2003 version, while making the characters and incidents completely different.    
 automobile expert explosives expert, expert hacker prosthetic makeup artist, who plan to steal gold worth   from a moving train. During the robbery they are double crossed by members of their own team. 

Players was named one of the most anticipated Bollywood films of 2012,  as it received hype ever since it was announced to be a remake of the The Italian Job, was made on a huge budget,  had a multistar cast, was filmed in foreign locations as New Zealand, Russia and even the North Pole—which was a first for a Bollywood film—   and was heavily promoted.  However, upon release the film received mixed reviews from Film critics|critics, with wide criticism drawn towards its length and pace and opened to a poor response at the box office, despite having a wide release.  The shocking failure of the film led to many trade industry analysts reinstating the belief of a jinx in Bollywood that the first-released film of the year always fails at the box office, which happened to the films Halla Bol (2008), Chandni Chowk To China (2009), Pyaar Impossible! and Dulha Mil Gaya (2010).
==Plot==
 thieves in Don and explosives expert prosthetic makeup artist Sunny (Omi Vaidya), and an illusionist Ronnie (Bobby Deol).
 satellite system and Ronnie uses his illusion to trick the soldiers guarding the gold while the rest of the team transfer the gold from the train. The robbery is executed successfully, however while celebrating Spider double crosses the gang and tries to flee with all of the gold. Spider is stopped by Ronnie, who is then shot multiple times by Spiders assassins. The group are chased by the assassins and Ronnie and Riya are killed. Charlie, Bilal and Sunny manage to escape after Spider blows up the location and flees with the gold. Charlie calls Victor to tell him about the betrayal, but Naina overhears the conversation. At the same time Spiders assassins break into the house and murder Victor. Naina decides to cut all contact with Charlie. Over the course of a year, Charlie contacts various gold dealers to find which one has the stolen gold that Spider escaped with.

In New Zealand, the gang locate a dealer who is in contact with Spider. Charlie explains to Naina that the gang only wanted to steal the gold so they could help achieve Victors dream of building a big orphanage. Naina understands and teams up with Charlie to exact revenge on Spider. In New Zealand they discover that Riya is still alive and living there. She explains that she also wanted revenge on Spider and provides the gang with the information about Spiders residence. Unaware of her plans, Spider flirts with Naina and takes her to his villa. Spider finds a hidden camera in Nainas hair and realises that she is working with Charlie, who runs in to rescue her and tells Spider that he will steal all of the gold within 48 hours. Spider tries to move the gold to another country, but the gang, using three large trucks and by hacking into the traffic system, take the gold and hide it inside three Mini Coopers. Just as they are about to leave New Zealand, they are caught by Spider and discover that Riya was working with Spider all along.

Spider tries to force the gang to hand over the gold, but the trunks of the Mini Coopers are empty. Spider betrays Riya and threatens to kill her. Before Charlie can reveal the real location of the gold, Riya shoots herself and Spider stabs Charlie. Spider tries to escape, but Sunny and Bilal stop him and beat him up badly. Naina, who wants revenge for the death of Victor, shoots Spider thrice. It is then revealed that the real Mini Coopers containing the gold are elsewhere. As the four leave the scene, Spider makes a final call to the Russian mafia and tells them that Charlie has stolen Russian gold. The gang take the real Mini Coopers and leave for India, but the Mafia stop them to check the cars and once again they are found to be empty. It is then revealed that the Mini Coopers were made out of the actual gold itself. In the end, Bilal opens a car dealership business, Sunny becomes a theatre actor, Charlie and Naina open Victors dream orphanage and Charlie raises Ronnies daughter as his own.

==Cast==
*Abhishek Bachchan as Charlie
*Sonam Kapoor as Naina
*Bipasha Basu as Riya
*Neil Nitin Mukesh as Spider
*Bobby Deol as Ronnie
*Sikandar Kher as Bilal
*Omi Vaidya as Sunny
*Johny Lever as MC
*Vinod Khanna as Victor Braganza
*Vyacheslav Razbegaev as General
*Sumit Sarkar as Commissioner of Police
*Aftab Shivdasani as Raj (Special Appearance)
*Shweta Bhardwaj as Shaila (Special Appearance)

==Production==

===Development===
Development on the project began in late 2009, when Viacom 18 approached Abbas-Mustan with the idea of remaking the The Italian Job into a desi Indian version, and bought the remake rights from the original producers Paramount Pictures. At first Abbas-Mustan were hesitant, but after watching the 2003 remake of The Italian Job they decided it would be a good idea to Indianise it. They spent around a year working on the script and making a few tweaks including two new characters, played by Basu and Deol.  

===Casting=== the original. negative role, her family problems with the Bachchan Family|Bachchans.    Deepika Padukone was also backed out, as she just like Chopra felt "the female lead does not have a strong part to play".  Chopra was eventually dropped in a controversial matter, citing differences with Bachchan and the directors.   Later Abbas Mustan claimed that the film was never offered to Chopra.   

At one point the film had no three male leads and no female lead, which caused the directors to panic,    after Kaif also backed out due to date problems. As the directors wanted a younger girl she was eventually replaced by Sonam Kapoor.  Kapoor walked out of the film, as the director she was working with on another film titled, Mausam (2011 film)|Mausam, asked to concentrate solely on his film.   However, she was signed again, as she managed to a lot dates, but to due to heavy pressure from both projects, she passed out on the sets of Mausam.

Rumours circulated that Mukesh had been dropped, due to the poor commercial business of his Lafangey Parindey (2010), although it had been a profitable venture.  He later slammed the rumours saying "Arrey issme real ya unreal status ki kya baat hai? (There is no question of real and unreal status) Its sad that I am giving clarification on this when the fact is that I am already preparing for my role. The film is pretty much on and I am also not going anywhere".  As Basu was approached, she immediately agreed after she read the script and was "blown away" by it. She had previously worked with Abbas-Mustan in her debut film Ajnabee (2001 film)|Ajnabee (2001) and in Race (2008 film)|Race (2008).   Veteran actor Vinod Khanna made a comeback since his cameo appearance in Dabangg (2010), and was chosen to play a small but key role as the mastermind of the heist over the likes of other Indian senior actors Amitabh Bachchan, Rishi Kapoor and Dharmendra.   Aftab Shivdasani was roped in to play a guest cameo appearance.   

It was Abbas Mustans first time working with all the actors except for Basu and Deol, who they had previously cast together in Ajnabee. The latter in five films,  along with Johnny Lever who has played minor roles in all but two of their films.  It was also the second pairing of Bachchan and Kapoor, who reunited after Delhi-6 (2009).  Bachchan had also worked with Basu and Sikandar Kher (son of actors Anupam Kher and Kirron Kher) in Dhoom 2 (2006) and Khelein Hum Jee Jaan Sey respectively.

===Pre Production === shooting could being, the entire cast underwent physical training to the requirement of the roles they were playing. Bachchan was required to tone up the most as he was the lead character and had to perform dangerous train stunts.  For her role Kapoor, was required to learn to drive a car, although she had no previous experience. She practised in the small lanes of Juhu and after reassurance from her father Anil Kapoor to the directors, Kapoor drove, only to crash the car. Abbas Burmawalla commented on this "Our action director Allan Amin felt that she should get comfortable zipping around in the Mini Cooper. So about 10 days before we started shooting, Sonam started practicing driving the Mini. However, on the first day, she accelerated while reversing, and banged into a lamppost".   Basu was required to wear a bikini during one of the songs,  had gained weight during the year and worked with personal trainer Paul Britto, to ensure the perfect body. Her regime included functional training with abdominal exercises, kickboxing, aerobic exercise, stretching and weight training.   Shweta Bhardwaj, who was playing a pivotal character in the film, was also required to physically tone up for an entirely different bikini scene. 

===Filming===
 s used in the production, (this one was driven by Sonam Kapoor in the film) on location in Wellington, New Zealand]]
 Miramar beach in Goa.  
 Te Papas Cuba Mall an earthquake hit the country.  

The third schedule of shooting, which was the longest and most important, began on April 2011 in parts of Siberia, St Petersburg and Murmansk.  For the shooting in Siberia, the directors had a lot of difficulty acquiring permission.    Similarly they had also faced several problems during the shooting in new Zealand. Describing this ordeal Mustan Burmawalla said "We had to seek lot of permissions for a traffic jam scene to be shot in New Zealand and that took a long time. While, in Russia we faced language problem, we appointed about 25 interpreters. We needed them to help us about basic things like where to go to eat etc,".  Due to the difficulty allotting dates, Bachchan had to leave another project by Ram Gopal Varma titled Department (film)|Department, whom Varma would later blame for the dismal performance of the film at the box office.   The sequences involving the train were directed by Filmfare award winning, stunt director Allan Amin. Bachchan refused to have a body double for the scenes and performed all the dangerous stunts himself. During the sequence, he had to balance himself atop two trains which were travelling at approximately 100&nbsp;km/h. He hit his head on a rod on top of the train, but still managed to complete the scene.   During another scene Bachchan was left hanging by a rack on the train for around 20 hours.  While filming took place there, a group of Russian soldiers learnt that an Indian film was shooting nearby and joined the crew on the sets and requested to meet all the stars. 

Some of the scenes were filmed close to the Arctic Circle, Arctic Ocean and at the North Pole, which was the first time a Bollywood film had been filmed there.    However a slight problem arose; no food was available because of the extremely cold climate and everyone had to manage on packets of soups.  Shooting in Russia ended in late July 2011. The cast flew back to India, finalised a few last minute shots and songs, before the film went into the post production stage.

==Release==
{{Quote box quote  = "Our brother (editor) Hussain Burmawala took six-and-a-half months to finish the editing. This was our most difficult film to edit. Believe it or not, the film was always shot with multi-camera setups. At times, there used to be six and even seven cameras capturing the action scenes." source = Films editing work done by Hussain Burmawala  width  = 30%}}
 Agent Vinod Bollywood film of 2012. 

===Pre-release=== Hollywood films into Indian-esque classic thrillers, returned to direction after quite some time. They had promised to break the boundaries of the action genre, as the major action stunts and sequences in the film were performed on par with Hollywood by the actors themselves.  This is rarely seen or done in Bollywood.  Kapoor who had previously played mostly girl next door type roles, brought a completely different look to her previous films, which attracted positive attention and was tagged as "sexy" by the media once the films promos released.      Another reason the film had created curiosity was because none of the actors expect Basu had enjoyed much box office success,  while the rest were relative newcomers. Bachchan, who had been in the industry for over 10 years, had endured a series of flops, in spite of this the directors were confident of him. 

Players was named as one of the most-awaited films of 2012 by Mid Day, IBN Live, Hindustan Times, India Today and The Times of India.    

===Promotion=== teasers and promo was revealed on 3 November 2011, on Kaun Banega Crorepati, which featured Abhisheks father, Amitabh Bachchan as the host.   Amitabh Bachchan also helped to promote the film on his blog and Twitter feed.  The theatrical trailer was also released on 3 November 2011, on Players official YouTube channel and Facebook page. 
 promotion schedule doughnut act and a 360-degree feedback|360-degree wheelie.  
 season 5 during the semi finals, four days before Players was released.  Players was heavily promoted in London with help from the cast of the 2003 remake of The Italian Job, including Charlize Theron, Mark Wahlberg, Edward Norton and the director F. Gary Gray.   The film was released in UK cinemas by B4U Pictures.  The producers of Players held "Go for Gold" competitions where the winner would meet the cast and spend a day with them. 

===Premiere=== premiere in Global Village traditional red carpet, a golden carpet was used in keeping with the theme of the film.   Kapoor caused controversy over the outfit she had worn during the premiere, giving her a busty look and was verbally attacked by Fashion bloggers.   

==Reception==

===Critical reception===
Players received a mixed reception from Film critics|critics.  Joginder Tuteja from IndiaGlitz gave it 4 out of 5 stars and said "Fantastic - Thats what you end up exclaiming once the last twist of Players takes you by complete surprise. Oh yes, there are around half a dozen or more twists in this film, something which was pretty much expected since the men at the helm of affairs are Abbas-Mustan".  Subhash K. Jha from Bollywood Hungama wrote, "Players does a first-rate job of adapting The Italian Job. Its slick and well-written, eye-catching and intelligent, witty and delectable. It beats every other action-adventure flick in recent Bollywood history".  Oneindia.in also gave a positive review, and said that, "Players electrifies movie buffs with its hi-voltage action. The highlights of the movie are wonderful performances by the lead actors, stylised stunts, adrenaline-pumping chases, stunning visuals, Pritams songs and comic sequences. But the editing work is a big let down".  Nikhat Kazmi of The Times of India gave the film a rating of 3.5 out of 5 stars and wrote, "Music by Pritam is good while it lasts. Some editing sorely needed, visual imagery quite polished, and the look as usual is of new age Bollywood."  Daily Bhaskar rated it 3 out of 5 and said, "Direction by Abbas-Mustan, cinematography and an edgy plotline are the strong points. Music, dialogues and performances (besides Bipasha and Neils performance) are the weak points".  Piyali Dasgupta from NDTV gave it 3 out of 5 stars and said, "Players ends up as a fun weekend flick, especially for action buffs. Abhishek Bachchan shows some of the style of the Dhoom (film series)|Dhoom series. Neil Nitin Mukesh, largely off the radar after an intense performance. Bipasha too shows a hint of Race with her glamourous role and that red bikini. Sonam Kapoor somewhat redeems herself as an actor playing the good geek Naina. But dont expect much from Pritam’s music.  Zee News gave it 3 out of 5 stars and said, "Abbas Mustan’s Players is a slick, fast paced and racy thriller. Technically the film is brilliant, but when it comes to drama and emotions, the film falls flat on its face. In comparison to The Italian Job, Players’ is long and overstretched.  Troy Ribeiro from Cinebasti gave it 3 out of 5 and wrote, "Sleekly made with fine action, crisp razor sharp edits, speeding cars and exciting photography, giving you glimpses of scenic Siberia, Amsterdam and New Zealand, the film reminds you of a Bond film. Ravi Yadavs cinematography and Husain Burmawalas editing are worth a mention. Technically, Abbas Mustan have churned out a very fine movie".  Kaveree Bamzai of India Today also gave it 3 out of 5. 

Players also received a number of negative reviews.  Taran Adarsh from Bollywood Hungama gave it 2 out of 5 stars and wrote, "On the whole, PLAYERS rides mainly on the clout of its credible director duo  , daredevil stunts and stunning visuals. But, most importantly, it is deficient of a captivating screenplay".  Rajeev Masand of IBN Live rated it 2 out of 5 and wrote, "Players is a fastpaced khichdi. Abhishek Bachchan is earnest, and thankfully dials down his trademark smugness to play the ringleader of the team. Neil Nitin Mukesh gets some interesting scenes to sink his teeth into. Bipasha Basu fills out a bikini nicely, but poor Sonam Kapoor changes her costumes and her hairstyle more frequently than her expressions.  What could have been a satisfying entertainer doesnt quite achieve its potential.  Karan Anshuman of the Mumbai Mirror gave it 2 out of 5 stars.  Aniruddha Guha from DNA India awarded it 2 out of 5 and wrote, "The writing is juvenile, the dialogues a joke, the acting over-the-top, yet everyone seems to be taking themselves so seriously".  Sukanya Verma of Rediff also gave the film a rating of 2 out of 5 stars and wrote, "Players is lacklustre and unimaginative".  Mayank Shekhar of the Hindustan Times gave it 1.5 out of 5.  Komal Nahta from Koimoi rated it 1 out of 5 and wrote, "Whats good? The train robbery sequence; some comedy in the second half; Omi Vaidya and Neil Nitin Mukesh’s acting. Whats bad? The loose and uninteresting screenplay; the poor music; the dull acting of Abhishek, Sonam and Bobby".  Yahoo! Movies gave the film a rating of "Game Over" and said that, "the remake of "The Italian Job is definitely a job taken not so seriously". 

===Box office===

====India====
Players had a wide release of over 2000 prints, but despite the film having an extensive promotion schedule it only opened to around 35-40% occupancy,   as it failed to make any headway over the weekend, grossing  . Daily collections were   on Friday  and   on Saturday.   There was a small rise on Sunday as the film made  .   On Monday, its revenues decreased as it made  . 
	
Collections decreased throughout the first week. On Tuesday the film grossed  , on Wednesday it made   and on Thursday  , making the total first week revenues  .   The second Friday was extremely poor; Players grossed just 60 lakhs compared to its reasonable first Friday gross of  .  Over its second weekend, even with a small increase on Saturday, the film only earned  , taking its total 10-day collections to around  .  In its second full week Players grossed     and in its third week it grossed   taking the total three-week revenues to around  .  Players saw a further drop in its fourth week, grossing 8 lakhs, due to the release of Agneepath (2012 film)|Agneepath. After six weeks Players ended its total lifetime collections at  . It was reviewed as below average by Boxoffice-India.   

====Overseas==== Event and Hoyts. Players exceeded the previous record held by Don 2 (2011), which was released two weeks earlier with nine prints.  

==Soundtrack==
{{Infobox album
| Name        = Players
| Type        = Soundtrack
| Artist      = Pritam
| Cover       =
| Released    = 30 November 2011
| Genre       = Film soundtrack
| Length      = 46:24
| Label       = T-Series
| Producer    = Pritam
| Last album = Mausam (2011 film)|Mausam (2011)
| This album = Players (2011) Agent Vinod (2012)
}}
 composed and Blue Stahli. 

===Track listing===
{{track listing
| headline =
| extra_column = Singer(s)
| music_credits = no
| total_length = 46:24

| title1 = Jis Jagah Pe Khatam (Players Theme)
| extra1 = Neeraj Shridhar, Siddharth Basrur, Mauli Dave
| length1 = 4:12

| title2 = Jhoom Jhoom Ta Ja
| extra2 = Ritu Pathak
| length2 = 5:02

| title3 = Ho Gayi Tun
| extra3 = Yashita Yashpal, Bob
| length3 = 3:47

| title4 = Buddhi Do Bhagwaan
| extra4 = Shruti Pathak, Abhishek Bachchan
| length4 = 4:42

| title5 = Dil Yeh Bekaraar Kyun Hai
| extra5 = Mohit Chauhan, Shreya Ghoshal
| length5 = 4:38

| title6 = Jhoom Jhoom Ta Hoon Main
| extra6 = Siddharth Basrur
| length6 = 4:49

| title7 = Dil Yeh Bekaraar Kyun Hai (Reprise)
| extra7 = Nikhil DSouza, Priyani Vani
| length7 = 5:17

| title8 = Jhoom Jhoom Ta Hoon Main (Film Version)
| extra8 = Arijit Singh
| length8 = 5:09

| title9 = Jis Jagah Pe Khatam (Remix)
| extra9 = Neeraj Shridhar, Siddharth Basrur, Mauli Dave
| length9 = 3:30

| title10 = Dil Yeh Bekaraar Kyun Hai (Remix)
| extra10 = Mohit Chauhan, Shreya Ghoshal
| length10 = 5:17
}}

===Reception=== Dum Maaro Dum and Mausam (2011 film)#Soundtrack|Mausam, Pritam dishes out yet another album that is a mix of sure-shot winners and drab space fillers." 

===Remix Plans===
{{Quote box quote  = "Tamma Tamma Loge was really in contention. Since it sounds quite similar to Jumma Chumma (both songs were actually based on Mory Kantes Tama Tama), it was a no brainer to work on either of the two. However, the decision was soon reversed as much time had already been spent in contemplation and multi city promotional tour was keeping the cast busy as well. Abhishek, Sonam, Bipasha and Neil have been travelling extensively and it would have been impossible to shoot the song, edit, promote and then include in the final print". source = -Pritam Chakraborty (Players music composer)  width  = 40%}}
Because the album received mixed reviews from critics, Viacom 18 and T-Series decided that Pritams score had not lived up to the audience’s expectations as well as Abbas-Mustans reputation of having hit music in their films. Pritams last work with director duo in Race (2008) had sold well and helped the films initial run at the box office  and they decided to remix the hit song Jumma Chumma from Bachchans father Amitabh Bachchans hit film Hum (film)|Hum (1991).  However Abbas-Mustan were not very keen on this, causing tension between them and the producers.  There were also plans to remix the song Tamma Tamma Loge from Sanjay Dutts Thanedaar (1990), but plans to remix the song were scrapped as there was less than two weeks before the release of the film.   

==Controversies==
{{Quote box quote  = "There was damage to some locations. They worked 30 something, 32 days on the trot. Thats a safety concern in itself. Making it easier isnt necessarily the best approach. What we need to do is find a way to get the two industries to work together, because they have quite a different way of working from us." source = -President of the New Zealand Film Technicians, Guild Alun Bollinger  width  = 30%}} During the shooting in New Zealand, authorities at an airport refused to give permission to film a scene, having decided it was too dangerous. An aeroplane was to fly a few inches above three cars, which were being driven by Bachchan, Kapoor and Kher. The stunt directors Mark and Allan Amin rehearsed the scene and the airport authorities finally gave permission because it was an important scene.  The New Zealand Government and the local film crew were also unhappy and claimed the crew of Players had disregarded New Zealand laws and cared little for health and safety concerns, took months to pay invoices and damaged some locations.  John Key, the Prime Minister of New Zealand, had declared tax breaks for Bollywood films shooting in New Zealand to attract more film-makers. After Players did well at the New Zealand box office, laws allowing more film crews and actors to shoot there were to be enacted, causing concerns among the New Zealand people and authorities. 
 one directed by Rohan Sippy, Bachchans long time family friend. It was reported that Chopra had refused to star in another one of his film, due to this Bachchan had asked her to be dropped.   However Abbas Burmawalla cleared Bachchans name, saying "The star cast of The Italian Job was finalized long time back. We had signed Abhishek Bachchan, Neil Nitin Mukesh and Katrina Kaif in the first go and Katrina was always our first choice so there was no question of talking to Priyanka Chopra. She was never approached or spoken to for this role so how can Priyanka lose out on a role she was never even considered for?".  
 Censor Boards middle finger Australia vs India 2011-12 series. 

==Impact==
After the poor financial performance of Players, Bachchans lack of success continued, and he was dropped from his next film role.  The unexpected failure of the film led to many trade industry analysts reinstating the belief of a jinx in Bollywood that the first-released film of the year always fails at the box office, which happened to the films Halla Bol (2008), Chandni Chowk To China (2009), Pyaar Impossible! and Dulha Mil Gaya (2010).   This had been happening for years until 2011 when No One Killed Jessica became a "semi hit"  supposedly breaking the jinx. The analysts were confident that Players would be successful.  {{cite web|title=Will Players break the curse?
|url=http://www.hindustantimes.com/Entertainment/Bollywood/Will-Players-break-the-curse/Article1-782373.aspx|work=Hindustan Times|author=Navdeep Kaur Marwah|date=14 December 2011|accessdate=12 February 2012}} 
 satellite rights that were sold to Zee TV for an approximate  . The total loss amounted to around  . 

==See also==
* Bollywood films of 2012
* The Italian Job (1969)
* Charlie Croker
* Heist film

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 