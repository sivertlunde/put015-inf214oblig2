Saxon (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Saxon
| image          = 
| caption        = 
| director       = Greg Loftin
| producer       = Elise Valmorbida
| writer         = Greg Loftin
| starring       = Sean Harris Sarah Matravers Michelle Connolly Henry Kelly
| music          = Michael Portman Vincent Browett
| studio         =  Sillwood Films
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = English
}}
  thriller and spaghetti western by turns. The story revolves around a reformed villain who is compelled to undertake a desperate mission which unleashes the demons of his past.

==Principal cast==
*Sean Harris
*Sarah Matravers
*Michelle Connolly
*Henry Kelly
*Tony O’Leary
*Drew Edwards
*Stephen Manwaring
*Paul McNeilly
*Divian Ladwa

==Principal crew==
*Director: Greg Loftin
*Producer: Elise Valmorbida
*Screenplay: Greg Loftin
*Line producer: Sam Parsons
*Director of photography: Steven Priovolos
*Music: Michael Portman and Vincent Browett
*Executive producers: Elise Valmorbida, Greg Loftin, Jack Fidler, Barry Bassett

==Development==
Saxon is the brainchild of writer-director Greg Loftin, who developed the original screenplay over several years, quoting as influences  .”

==Acclaim==
Saxon won Official Selection in the 61st Edinburgh Film Festival under the artistic direction of Hannah McGill.
 Santander Film Festival.
First-time producer Elise Valmorbida, also a published writer (Matilde Waltzing, The Book of Happy Endings, The TV President), was honoured as a Skillset/EIFF Trailblazer, one of ten film-making talents in the UK to win this accolade.
EIFF Artistic Director Hannah McGill wrote of Saxon: “Startling UK thriller brings the grit of 70s Hollywood to a messed-up London estate… Sizzling with tension and vivid, near-surreal imagery, this is a forceful and confident debut.”

Saxon went on to further festival success as Official Selection in the British Film Festival in Israel (January 2008) and the European Independent Film Festival (March 2008) where the film won the award for BEST EUROPEAN DRAMATIC FEATURE. On 21 April 2008, Saxon had its English premiere at The East End International Film Festival, where it was nominated for the award of Best UK Debut Feature. Featured on BBC London, the film screened to a sold-out cinema, with a waiting list for returns.

The film is released in the UK 12 January 2009.

==Synopsis==
London, the present. Soon after leaving prison, Eddie has his eye cut out by a loan shark chasing an old debt.  Eddies other eye will only be spared upon repayment. Desperate for cash, Eddie phones Linda, a childhood sweetheart. She lives in SAXON - a ghost-town of grim flats run by a corrupt council. Linda is very wealthy. Her husband Kevin won a million pounds on a TV quiz show. But Kevin has gone missing, feared dead. Eddie offers his services as an amateur sleuth, and so embarks on a comically gruesome journey through the surreal underworld of SAXON: the place where he grew up, the place where his mother works as a prostitute, the place where he murdered a bailiff.

==Micro-budget EIS==
Saxon was funded entirely by private equity, including the producers’ own mortgage. The film’s financing structure was first announced publicly in ScreenFinance magazine: “Sillwood Films is launching a micro-budget Enterprise Investment Scheme to finance its urban thriller Saxon. Unusually… The EIS will be used to give a tax incentive to individuals who have already put money into the film.”
As with many micro-budget productions, Saxon’s cast and crew worked on the basis of part-deferred fees. Other professionals worked for company shares rather than cash. Post-production was delivered by St Anne’s Post (part of the giant Ascent Media Group) under the direction of Patrick Malone.

==References==
 
* ScreenFinance Volume 18, Number 22, November 30, 2005
* Angel Magazine, April 2006

==External links==
*  
*  
*  
*  
*  

 
 