Bakit Hindi Ka Crush ng Crush Mo?
 
{{Infobox film
| name             = Bakit Hindi Ka Crush ng Crush Mo?
| image            = Bakit_Hindi_Ka_Crush_Ng_Crush_Mo?_film.jpg
| caption          = Theatrical movie poster
| director         = Joyce Bernal
| producer         = Charo Santos-Concio Malou N. Santos
| writer           = Irene Villamor
| story            = Irene Villamor
| screenplay       = Carmi Raymundo Ramon Bautista Bakit Hindi Ka Crush ng Crush Mo? by Ramon Bautista
| starring         = Kim Chiu Xian Lim
| cinematography   = Dan Villegas
| editing          = Beng Bandong Joyce Bernal Glenn Ituriaga 
| music            = Carmina Cuya 
| distributor      = Star Cinema
| released         =  
| runtime          = 
| country          = Philippines
| language         =  
| budget           = PHP 15 million (estimated) 
| gross            = Domestic:   PHP 102,600,634   International:   $2,352,001 
}}
 Filipino film adaptation of Ramon Bautistas Bakit Hindi Ka Crush ng Crush Mo? (book)|self-help book of the same name. The romantic comedy film is produced by Star Cinema and is part of its 20th Anniversary presentation. It tells the story of a brainy ugly duckling girl and her journey as she turns into someone whos worth loving. Ramon Bautista also stars in the film as a taxi driver. It is the first movie team up of Kim Chiu and Xian Lim and is directed by Joyce Bernal.    

==Sypnosis==
Sandy Veloso (Chiu) is a modern-day ugly duckling who is obsessed with her boyfriend Edgardo (Cipriano). After he breaks with her, Sandy is left to ponder why she feels unwanted.

Matters turn worse for Sandy when she is laid off by her company, under the leadership of its new bachelor CEO, Alex Prieto (Lim). Realizing his mistake when employees belatedly put in a good word for Sandy, Alex hires her back, and the two start forming an unlikely friendship.

In a love story about "the ugly duckling in all of us and how we turn into someone worth loving," Alex at one point even offers Sandy a make-over so she will no longer cry over a failed relationship.

==Cast==
*Kim Chiu as Sandy Veloso
*Xian Lim as Alex Prieto
*Kean Cipriano as Edgardo Salazar
*Mylene Dizon as Pamela
*Freddie Webb as Don Antonio Prieto
*EJ Jallorina as Max Veloso

*Ramon Bautista as Himself / taxi driver
*Cholo Barretto as Nathan
*Ashley Robert Corbette as Ashly
*Anika Mendez as Nikky
*Cedric Lucero as Ceddy
*Jojit Lorenzo as Julius
*Cheska Inigo as Patty Prieto
*Sarah Gaugler as Liza
*Bianca Valerio as Bianca Valerio
*Jeremiah Sird as Eric Rodney

===Special Participation===
*Tonton Gutierrez as Miguel Prieto
*Angeline Quinto as Tangerine
*Pokwang as Cora Veloso
* Dianne Medina
* Jireh Lim as himself
* Lilia Cuntapay
* Dawn Jimenez

==Promotion==
Actors Kim Chiu and Xian Lim became guests to several ABS-CBN TV shows to promote the film. Among these are Gandang Gabi Vice and Banana Split.

===Theme Song===
{{Infobox single  
| Name = Bakit Hindi Ka Crush ng Crush Mo?
| Cover = 
| Artist = Zia Quizon
| from Album = A Little Bit Of Lovin
| Released =  
| Format = {{flat list|
*CD single digital download
}}
| Recorded =  Pop
| Tagalog
| Length = 4:11 
| Label = {{flat list|
*Star Records PolyEast
}}
| Writer = Jungeee Marcelo
| Producer = 
}}
Bakit Hindi Ka Crush ng Crush Mo?, sang by Zia Quizon and written by Jungee Marcelo, is the theme song of the movie. It premiered on July 8 at My Only Radio FM radio station and was released as a single on July 17.    It is also included in her second studio album, A Little Bit of Lovin.

===Music video===
The music video for the song was released on ABSstarcinemas YouTube channel on July 23. The videos plot features Quizon at a photo shoot session while being pursued by a man who works on set, played by Ramon Bautista, the author of the book to which the film is primarily based. 

===Accolades=== 27th Awit Awards for the Awit Award for Best Song Written for Movie/TV/Stage Play on December 12, 2014. 

==Critical reception==

Bakit Hindi Ka Crush ng Crush Mo? mostly received positive reviews from critics. Jerald uy of   stated that "Bakit Hindi Ka Crush ng Crush Mo? reminds us of the stupid things we can do for love but offers it in a fresh way." The movie was given an 8.5/10 "certified fresh" from both moviegoers and movie critics alike.

===Commercial performance===
 The Wolverine.

== References ==
 

==External links==
* 
* 

 
 
 
 
 
 