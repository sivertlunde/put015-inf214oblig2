The Place Beyond the Winds
 
{{Infobox film
| name           = The Place Beyond the Winds
| image          = 
| caption        = 
| director       = Joe De Grasse
| producer       = 
| writer         = Harriet T. Comstock Ida May Park Lon Chaney Dorothy Phillips
| music          = 
| cinematography = King D. Gray
| editing        =  Universal Pictures
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama Lon Chaney. Four of the five reels survive in the film archive in the Library of Congress.   

==Cast==
* Dorothy Phillips - Priscilla Glenn
* Jack Mulhall - Dick Travers Lon Chaney - Jerry Jo
* Joe De Grasse - Anton Farwell
* C. Norman Hammond - Nathan Glenn
* Alice May Youse - Mrs. Glenn
* Grace Carlyle - Joan Moss
* Countess Du Cello - Mrs. Travers
* William Powers - Dr. Leeydward

==See also==
* List of incomplete or partially lost films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 