The Nature of the Beast (film)
{{Infobox film
| name           = The Nature of the Beast 
| image          =
| caption        =
| director       = Cecil M. Hepworth
| producer       = Cecil M. Hepworth
| writer         = E. Temple Thurston 
| starring       = Alma Taylor   Gerald Ames  James Carew
| cinematography = 
| editing        = 
| studio         = Hepworth Pictures
| distributor    = Butchers Film Service
| released       = June 1919
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}
The Nature of the Beast is a 1919 British silent drama film directed by Cecil M. Hepworth and starring Alma Taylor, Gerald Ames and James Carew.  During the First World War a Belgian refugee marries a British aircraft manufacturer, but a former German enemy tries to force her to give over secret documents. It was based on a 1918 novel of the same title by E. Temple Thurston.

==Cast==
*   Alma Taylor as Anna de Berghem  
* Gerald Ames as John Ingledew  
* James Carew as Kleinenberger  
* Gwynne Herbert as Mrs. de Berghem  
* Stephen Ewart as Sir James Standish 
* Mary Dibley as Lady Standish 
* Victor Prout as Mr. de Berghem  
* Christine Rayner as Guest  
* John MacAndrews as Friend  

==References==
 

==Bibliography==
* Palmer, Scott. British Film Actors Credits, 1895-1987. McFarland, 1988. 

==External links==
* 

 
 
 
 
 
 
 
 
 

 