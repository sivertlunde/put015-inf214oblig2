Minecraft: The Story of Mojang
{{Infobox film
| name           = Minecraft: The Story of Mojang
| image          = Minecraftthestoryofmojangposter.jpg
| caption        = Standard DVD cover
| director       = Paul Owens
| producer       = {{plainlist| Paul Levering
* Peter De Bourcier
* Burnie Burns    }}
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plain list|   Markus Notch Persson
* Jerry Holkins
* Mike Krahulik
* Geoff Keighley
* Logan Decker
* Stephen Totilo
* Evan Lahti
* Minecraft Teacher   SeaNanners
* The Shaft  
* Paul Soares Jr
* The Yogscast  
* Chris Hecker Todd Howard
* Tim Schafer
* Peter Molyneux}}
| music          = Willpixel
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 2 Player Productions
| released       =  
| runtime        = 104 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
 2012 documentary about the history of the company Mojang and its creation, Minecraft. Minecraft is an open-world game where players can craft, build and mine. The film features interviews of key personnel of the company including Markus Persson and Jens Bergensten.    The film was produced by 2 Player Productions and production was funded through a Kickstarter campaign.       

The film premiered first on Xbox Live on December 22, 2012, and was made available for download and streaming the following day.   2 Player Productions also uploaded the documentary to torrent index The Pirate Bay but urged people to consider purchasing the film. 
== Content == development process of the game.  The film also features insights from many people in the video gaming industry to discuss Minecraft s popularity influence.  Interviews from Persson and other Mojang employees give an insight on creation and expansion of the studio. 

== Soundtrack ==
The films soundtrack was created by Daniel Rosenfeld, who has also created most of the music and sounds for the game itself. The music featured is from Rosenfelds one album which contains 31&nbsp;tracks. The CD features remixes by crashfaster, Danimal Cannon, Bud Melvin and minusbaby. 

{{Track listing
| collapsed = yes
| title1          = cliffside hinson
| length1         = 3:46
| title2          = surface pension
| length2         = 6:34
| title3          = independent accident 
| length3         = 4:11
| title4          = danny makes chiptune
| length4         = 2:46
| title5          = the first million
| length5         = 3:19
| title6          = certitudes
| length6         = 4:33
| title7          = impostor syndrome
| length7         = 2:24
| title8          = buildup errors
| length8         = 3:26
| title9          = for the sake of making games
| length9         = 1:33
| title10         = preliminary art form
| length10        = 3:07
| title11         = lawyer cage fight
| length11        = 1:58
| title12         = lost cousins
| length12        = 1:31
| title13         = total drag
| length13        = 2:47
| title14         = drunken carboni
| length14        = 3:04
| title15         = the weirdest year of your life
| length15        = 3:58
| title16         = swarms
| length16        = 2:51
| title17         = diskdance
| length17        = 2:22
| title18         = pr department
| length18        = 1:25
| title19         = faux video production
| length19        = 3:08
| title20         = one last game
| length20        = 1:19
| title21         = this doesnt work
| length21        = 5:30
| title22         = wooden love
| length22        = 1:33
| title23         = disco, C418 - I glove thy flob
| length23        = 1:47
| title24         = post success depression
| length24        = 3:12
| title25         = social lego
| length25        = 4:51
| title26         = jayson glove
| length26        = 3:18
| title27         = clumsiness and innovation
| length27        = 3:03
| title28         = no pressure
| length28        = 3:04
| title29         = one
| length29        = 2:27
| title30         = Nifflas, C418 - fifflas
| length30        = 1:34
| title31         = Laura Shigihara, C418 - tsuki no koibumi
| length31        = 5:07
}}
 

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 