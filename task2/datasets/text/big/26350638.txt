Dersimiz: Atatürk
{{Infobox film
| name           = Dersimiz: Atatürk
| image          = DersimizAtaturkFilmPoster.jpg
| image size     =
| alt            = 
| caption        = Film poster
| director       = Hamdi Alkan
| producer       = Birol Güven
| writer         = Turgut Özakman
| narrator       = 
| starring       =  
| music          =  
| cinematography = Ferhat Öçmen
| editing        = Turgut Özakman
| studio         =  
| distributor    = Cine Film
| released       =  
| runtime        =  
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = $2,230,496
| preceded by    =
| followed by    = 
}}
Dersimiz: Atatürk is a 2010 Turkish biographical film directed by Hamdi Alkan based on the life of Mustafa Kemal Atatürk.

==Production==
The film was shot on location in Ankara, Çanakkale and Istanbul, Turkey.   

== Plot ==
A group of primary school students are assigned homework for which they will have to study the life of Mustafa Kemal Atatürk, the founder of the Turkish Republic. An elderly historian will lead the way during the kids’ exploration of Atatürk’s life story, telling them everything about his childhood, his years as a student and his military career, and also taking the kids on a virtual journey among the most important fronts fought during the Turkish War of Independence.

==Cast==
* Halit Ergenç as Kemal Atatürk
* Çetin Tekindor as Historian Grandfather
* Doğa Rutkay as Latife Hanım

==Release==
The film opened in 259 screens across Turkey   at number two in the Turkish box office chart with an opening weekend gross of $527,066.   

==Reception==

===Box office===
The movie has made a total gross of $2,230,496. 

== See also ==
* 2010 in film
* Turkish films of 2010
* Mustafa (film)|Mustafa a 2008 biographical film directed by Can Dündar
* Veda (film)|Veda a 2010 biographical film directed by Zülfü Livaneli

==References==
 

==External links==
*   for the film (Turkish)
*  

 
 
 
 
 
 
 
 
 