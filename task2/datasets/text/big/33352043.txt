Occupant (film)
{{Infobox film
| name           = Occupant
| image          =
| alt            =  
| caption        = Occupant Movie Poster at GSIFF
| director       = Henry S. Miller
| producer       = Blair Rosenfeld  Susan Dickey  Michelle Kalsi
| writer         = Jonathan Brett
| based on       =  
| starring       = Van Hansis Cody Horn Thorsten Kaye
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}}

Occupant is a 2011 American thriller film featuring Van Hansis as Danny, a young New Yorker about to inherit an old apartment.  It was written by Jonathan Brett and directed by Henry S. Miller and shot in New York City, USA.  It also stars Cody Horn and Thorsten Kaye.

The film premiered on the opening night of the Gotham Screen Film Festival & Screenplay Contest on October 14, 2011 in New York City. On October 18, 2011, the film was released on-demand in many cable systems and on most digital platforms (i.e., iTunes, Amazon, Blockbuster, PlayStation) in the U.S. and Canada. A full list of providers, as well as trailers, interviews and other information can be viewed at the movies  . The DVD will be released in November 2011. 

==Synopsis==
This is a psychological thriller about the value of patience and importance of human contact.

The movie opens as an old woman dies in her bed. The following day her estranged grandson, Danny, comes to the rent controlled apartment to ID the body, and the buildings strange doorman encourages him to squat in the apartment until a lawyer friend of the doorman can transfer to lease to Danny permanently.

Danny is instructed to not leave the apartment for 12 days by the Lawyer and then by the doorman Joe. Joe becomes his only contact to the outside world and brings him groceries, since if Danny leaves the apartment himself, the landlord could lock him out. After about three or four days he starts switching from the kid who had to be convinced to stay by the doorman, to the kid who feels that the apartment was for him, and only him.  As the days progress further, Danny begins to suffer from cabin fever leading to delusions and hallucinations. Also during this time strange things begin to happen and other people that enter the apartment go missing.

By the 9th day his cabin fever has progressed to the extreme. On this day Danny’s cat Ziggy goes missing, Danny finds a bloody claw then goes crazy smashing through walls, thinking Ziggy is hidden behind it. The doorbell buzzes and neighbor tells him to keep down the racket. Then the power goes out.

On day 10 Danny requests razor wire and cutters from the doorman. Danny explains that he thinks the landlord stole his cat and cut his power. He turns on a light and the power is back on, but claims they are just messing with his head. He then sets up razor wire in his apartment as booby traps. When he calls for Joe the doorman, a new doorman shows up, claiming that Joe is sick and Danny must leave the apartment.

Danny now, set off by the loss of his cat and the doorman he knows, begins making crude weapons from things around the house.

Day 11 opens with him on the couch surrounded with razor wire, holding a crude weapon made from a tennis racket. The power is off again. When the power comes back on he starts smashing a chair, making too much noise. Another buzz of the doorbell brings the police, telling him to keep down the racket. The police officer also asks what the smell in the apartment is. Danny answers that it is dinner. Danny re-enters the apartment and opens a casserole in the oven to find his pet cat, charred.

Day 12 he calls his lawyer and gets no answer. So constructs another crude weapon of nails in a swinging door.  He stands in front of the door and flashes back to some better times, then sees his dead grandmother’s face as she died and uses the swinging door weapon on himself. Later in the day, the doorbell buzzes and it’s his lawyer telling him the court order came though and the apartment is finally his.

The movie ends with the son of the new tenants, finding the video blog camera hidden in a piano. The child watches the SD card which reveals that Danny was a sleepwalker, who murdered every person and animal that entered the apartment.

==External links==
*  
*  
* 

 
 
 
 