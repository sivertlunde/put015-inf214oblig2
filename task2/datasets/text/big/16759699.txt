The Irishman
 
{{infobox book |  
| name = The Irishman
| title_orig =
| translator =
| image =  
| caption =
| author = Elizabeth OConner
| illustrator =
| cover_artist =
| country = Australia
| language = English
| series =
| genre = Novel
| publisher = Angus and Robertson, Australia
| release_date = 1960
| english_release_date = Hardback & Paperback)
| pages =
| isbn =
| preceded_by =
| followed_by =
}}
The Irishman is a Miles Franklin Award-winning novel by Australian author Elizabeth OConner.

The novel deals with the experiences of Paddy Doolan, an Irish teamster, and his sons in the Gulf Country in the north of Australia.

==Film adaptation==
{{Infobox film
| name           = The Irishman
| image          = 
| image size     =
| caption        = 
| director       = Donald Crombie
| producer       = Anthony Buckley
| writer         = Donald Crombie
| based on = 
| narrator       = Michael Craig Simon Burke Robyn Nevin
| music          = 
| cinematography = 
| editing        = 
| studio = Forest Home Films South Australian Film Corporation
| distributor    = 
| released       = 1978
| runtime        = 
| country        = Australia English
| budget         = AU $840,000 (est.) David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p147-149 
| gross = AU $622,000 
| preceded by    =
| followed by    =
}}

In 1978, the book was adapted for the screen in a film of the same name starring   for whom it was a labour of love.  

===Production===
The movie was budgeted at $767,000 but went $80,000 over.  It was filmed in May through to July in 1977.

===Release===
The film was a disappointment at the box office. Crombie later said he thought it was five to ten minutes too long, and suffered from being released soon after The Mango Tree, another coming of age period movie set in Queensland, when there was a backlash against period films. Peter Beilby & Scott Murray, "Donald Crombie", Cinema Papers, Oct-Nov 1978 p131-132 

==See also==
 

* South Australian Film Corporation

==References==
 
* 
==External links==
*  at Oz Movies

 
 
 
 
 

 
 
 
 
 
 
 
 


 