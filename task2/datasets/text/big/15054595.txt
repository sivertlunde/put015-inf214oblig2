List of Telugu films of 2001
 
  Tollywood (Telugu Hyderabad in the year 2001 in film|2001.

==2001==
{| class="wikitable"
|-
! Title !! Director !! Cast !! Genre !! Notes/Music
|-
|Adhipathi|| Ravi Raja Pinisetty  || Mohan Babu, Akkineni Nagarjuna, Preeti Jhangiani, Soundarya || Action film|Action|| Remake of Malayalam film
|-
|Ammaye Navvite|| Jyothi Kumar || Rajendra Prasad, Bhavana (Kannada actress)|Bhavana, Brahmanandam || Drama ||
|- Singeetam Srinivasa Rao || Akkineni Nagarjuna, Raveena Tandon || Drama ||
|-
|Ammai Kosam|| Muppalaneni Siva || Vineeth, Meena (actress)|Meena, Ravi Teja, Venu || Comedy ||
|-
|Anandam|| Srinu Vaitla || Jai Akash, Rekha Vedavyas || Romance || Hit
|- Action ||
|- Suresh Krishna Suresh Krishna || Akkineni Nagarjuna, Simran (actress)|Simran, Reema Sen || Romance ||
|- Balakrishna Nandamuri, Shilpa Shetty, Anjala Zaveri || Romance ||
|- Tarun (actor)|Tarun, Richa Pallod || Romance ||
|- Suresh Krishna ||Chiranjeevi, Simran (actress)|Simran, Ashima Bhalla || Family ||
|- Darling Darling|| V. Samudra || Meka Srikanth|Srikanth, Shaheen Khan || Romance ||
|- Malavika || Romance ||
|-
|Devi Putrudu|| Kodi Ramakrishna || Daggubati Venkatesh|Venkatesh, Soundarya, Anjala Zaveri || Fantasy ||
|- Akkineni Nagarjuna, Soundarya, Shenaz Treasurywala || Action ||
|- Sujatha || Action ||
|- Sai Kumar Sai Kumar || ||
|-
|Ishtam (2001 Telugu film)|Ishtam|| Vikram Kumar, Raj Aditya || Charan, Shriya Saran || Romance ||
|-
|Itlu Sravani Subramanyam|| Puri Jagannadh || Ravi Teja, Tanu Roy || ||
|-
|Jabili|| S. V. Krishna Reddy  || Dileep, Rekha Vedavyas || Romance ||
|- Sai Kumar, Neelambari, Thriller Manju || Action ||
|-
|Kushi (Telugu film)|Khushi||S. J. Suryah || Pawan Kalyan, Bhoomika Chawla, Mumtaj || Romance||  block buster hit
|-
|Maa Aavida Meda Ottu Mee Aavida Chala Manchidi|| E. V. V. Satyanarayana || Meka Srikanth, Laya (actress)|Laya, Raasi, Vadde Naveen || Family ||
|-
|Manasantha Nuvve || V. N. Aditya || Uday Kiran, Reema Sen|| Romance ||
|- Devotional ||
|-    GunaSekhar || Chiranjeevi, Simran (actress)|Simran, Sanghavi || Action film|Action||
|- Hit
|-
|Muthyam|| ||Rohit, Anu || 
Romance |
|-
|Naa manasista raa(film)| Naa manasista raa
||srikanth, richa ,soundarya||
|- Pratap ||Jagapathi Laya || Romance ||
|- Balakrishna Nandamuri, Action || Industry Blockbuster
|-
|Ninnu Choodalani|| Pratap || Jr. NTR, Raveena Rajput || Action/ Romance ||
|- Vijaya Bhaskar||Daggubati Venkatesh|Venkatesh, Arthi Agarwal || Comedy||
|- Teja (film Teja ||Uday Anitha || Romance || Industry BlockBuster
|-
|Parthale Paravasam|Paravasam||K.Balachander ||R. Madhavan, Simran Bagga, Sneha (actress)|Sneha, Lawrence Raghavendra || Romance || Dubbed from Tamil film Parthale Paravasam
|- Daggubati Venkatesh|Venkatesh, Simran || ||
|- Sai Kiran, Laya || ||
|- Balasekaran ||Tarun Sneha ||Romance film|Romantic||
|- Romantic Black Dark Comedy ||
|- Rajasekhar (actor)|Rajasekhar, Family ||
|- Akkineni Nagarjuna, Sumanth, Bhoomika Chawla || Drama ||
|- Suresh Varma||Jr. NTR, Sonali Joshi || Action ||
|-
|Student No.1||S. S. Rajamouli||Jr. NTR, Gajala ||Action film|Action|| 
|-
|Thank You Subba Rao||E. V. V. Satyanarayana || Srihari, Prakash Raj, Ramya Krishna || ||
|- Tottempudi Gopichand|Gopichand, Sneha || ||
|-
|Veedekkadi Mogudandi?|| E. V. V. Satyanarayana || Venu, Shruthi Raj, Geethu Mohandas || Comedy ||
|-
|}
 
 
 

 
 
 
 