Shipwrecked (1990 film)
{{Infobox film
| name           = Shipwrecked
| image          = Haakon_Haakonsen.jpg
| caption        = International theatrical release poster
| director       = Nils Gaup
| producer       = John M. Jacobsen Nigel Wooll
| writer         = O.V. Falck-Ytter (novel) Nils Gaup Bob Foss Greg Dinner Nick Thiel
| starring       = Stian Smestad Gabriel Byrne Louisa Haigh Trond Peter Stamsø Munch Bjørn Sundquist Eva von Hanno Kjell Stormoen Guy Fithen
| music          = Patrick Doyle
| cinematography = Erling Thurmann-Andersen
| editing        = Nils Pagh Andersen
| studio         = Walt Disney Pictures AB Svensk Filmindustri Buena Vista Pictures
| released       =            
| runtime        = 92 minutes
| country        = Norway Sweden United States
| language       = English
| budget         = 60 million Norwegian Kroner
| gross          = $15,104,156
}}
Shipwrecked ( ) is a 1990 family   (Haakon Haakonsen: A Norwegian Robinson). In Norway, it was titled "Haakon Haakonsen".

The movie was produced by a consortium of Scandinavian companies and released in the US in an English-language version by Walt Disney Pictures.

==Plot==
Haakon Haakonsen (Stian Smestad), a young Norwegian boy in the 1850s, becomes the sole support of his family as a cabin boy on a ship after his father is injured. Jens (Trond Peter Stamsø Munch), a family friend and a fellow shipmate of Haakon’s father, becomes an older brother to Haakon on their voyage.

At first, Haakon has a difficult time adjusting to life at sea, but eventually earns the respect of his shipmates while earning the distrust of the first mate, supposedly known as Howell - after Haakon catches the first mate with weapons he shouldnt have. The first mate forces him to promise not to reveal to the captain about the guns, and then keeps tabs on Haakon.
 captain (who may have been poisoned by the first mate), a suspicious new batch of sailors come on board immediately after Howells promotion to the top spot. Work onboard ship soon becomes terrible for Haakon, Jens, and the original sailors, which intensifies when the new captain finds Mary (the stowaway girl trying to reach her uncle in Calcutta) and demands whomever has been fraternizing with her to show his face. Haakon admits responsibility, and the captain sentences him to be lashed with the cat onine tails. Jens protests, saying that whipping Haakon would be an effective death penalty, to which Howell orders that Jens will be punished instead. However, before anyone can be flogged, the court martial is interrupted by a terrible storm that damages the mainmast, then sinks the sink. Haakon takes advantage of this to escape the doomed ship and wakes up on a deserted tropical island.

After searching the island, Haakon discovers treasure as well as wanted posters for an English pirate named Merrick, who looks identical to Howell. (In the first scene of the film, the real Howell was murdered by Merrick, who then stole the identity as a doppelganger). Haakon discovers that the suspicious shipmates (a.k.a the new batch of abusive shipmates who were brought on after Merricks captaincy) are really piracy|pirates, who arguably stored the treasure during a hot pursuit from naval forces, and will soon return to the island to extract their loot.

Following a self-training with a sword and gun, Haakon even manages to use a horn when a gorilla attacks. This makes the gorilla docile, who then starts to befriend Haakon, behaving like a pet. Although Haakon adjusts to the island, he misses Jens, and moreso his family back in Norway. One day, Haakon sees smoke from a distant island and tries various attempts to get there on a raft of his own.
 natives doing a night dance. Haakon eventually finds Mary with a bunch of traveling natives. When he sees one of the natives dragging Mary, Haakon springs into action frightening the natives with a gunshot until he backs into Jens, who explains that the natives are peaceful and saved his and Marys lives.

Mary explains that they have been traveling island to island looking for Haakon. As the three leave for Haakons island the next day, Haakon realizes that the pirates are returning to the island. Haakon and Jens managed to get help from Berg and Steine (Knut Walle and Harald Brenna), two of Jens friends who also survived the sinking ship. Haakon manages to outsmart the pirates and he, Jens, Berg, and Steine return to their native Norway. Each man kept a small share of the treasure, with Haakon deciding to use his share to help his impoverished family.

Back home in Norway, Haakon reunites with his family and introduces them to Mary. His parents agree to take her in until she can reestablish contact with her own relatives.

Note: There are two or three different (slightly) versions of this movie.

==Cast of Characters==
* Haakon Haakonson (Stian Smestad) - A Norwegian youth who signs on as cabin boy aboard a ship whose captain is his fathers close friend. At first he is shy and timid from years of being teased and picked on because he is poor, but after being stranded alone on the island and forced to survive, he becomes more confident.
* Lt. John Merrick (Gabriel Byrne) - The pirate captain who poses as a member of the English military, he eventually takes over the ship Haakon is on after the original captain mysteriously dies and proceeds to make life difficult for everyone. 
* Jens (Trond Peter Stamsø Munch) - The family friend of Haakons family and a sailor. He sailed with Haakons father and considers himself Haakons guardian on the ship after promising Haakons father to look out for him. 
* Mary (Louisa Milwood-Haigh) - A young stowaway searching for her uncle in Calcutta. She escaped from a workhouse in Australia, where she was placed after the death of her parents. 
* Berg (Knut Walle) - One of Jens shipmates.
* Steine (Harald Brenna) - One of Jens shipmates.

==Production== Daniel Defoes Robinson Crusoe, and was published in 1719.

The film was shot on location in Fiji, Norway, Spain and the United Kingdom.

==Reception==
The film has received favorable reviews as a well-made adventure for families.

===Awards===
Shipwrecked was nominated for three Young Artist Awards in 1992: 
* Best Family Motion Picture — Drama
* Best Young Actor Starring in a Motion Picture — Stian Smestad
* Best Young Actress Co-starring in a Motion Picture — Louisa Haigh

==External links==
*  
*  
*  
*   at Rotten Tomatoes.
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 