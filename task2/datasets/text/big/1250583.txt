Leo (film)
{{Infobox film
| name           = Leo
| image          = 
| caption        = 
| director       = José Luis Borau
| producer       = José Luis Borau
| writer         = José Luis Borau
| starring       = Icíar Bollaín
| music          = 
| cinematography = Tomàs Pladevall
| editing        = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Leo is a 2000 Spanish drama film, written and directed by José Luis Borau and starring Icíar Bollaín and Javier Batanero. It was nominated for six Goya Awards in 2001, and won the award for Best Director. It was also entered into the 22nd Moscow International Film Festival.   

==Cast==
* Icíar Bollaín as Leo
* Javier Batanero as Salva
* Valeri Yevlinski as Gabo
* Rosana Pastor as Merche
* Luis Tosar as Paco
* Charo Soriano as Leonor

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 