Namma Samsara
{{Infobox film
| name           = Namma Samsara
| image          = 
| caption        = 
| director       = Siddalingaiah
| producer       = Srikanth Nahata   Srikanth Patel
| writer         = Siddalingaiah
| screenplay     = Siddalingaiah
| story          = G. Balasubramanyam
| based on       =  
| narrator       =  Rajkumar  Bharathi
| music          = M. Ranga Rao
| cinematography = Srikanth
| editing        = S. P. N. Krishna   T. P. Velayudham
| studio         = Srikanth & Srikanth Enterprises
| distributor    = 
| released       = 1971 
| runtime        = 140 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Rajkumar and Bharathi in the lead roles.  The film was released under Srikanth & Srikanth Enterprises banner and produced by Srikanth Nahata and Srikanth Patel.

== Cast == Rajkumar  Bharathi  Balakrishna
* Padmanjali
* Rajashankar
* Dinesh
* B. V. Radha
* Advani Lakshmi Devi
* Papamma

== Soundtrack ==
The music of the film was composed by M. Ranga Rao and lyrics for the soundtrack written by Chi. Udaya Shankar and R. N. Jayagopal.  The title song sung by P. B. Sreenivas, P. Susheela and S. P. Balasubrahmanyam was hugely popular and considered one of the evergreen songs in Kannada cinema.

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|- 
| 1
| "Namma Samsara Ananda Sagara"
| P. B. Sreenivas, P. Susheela, S. P. Balasubrahmanyam
|-
| 2
| "Hennu Endare"
| P. B. Sreenivas, S. Janaki
|-
| 3
| "Sharanara Kaayo"
| B. K. Sumithra
|-
| 4
| "O My Darling"
| L. R. Eswari
|-
| 5
| "Namma Samsara" (sad)
| P. B. Sreenivas, P. Susheela
|-
|}

==See also==
* Kannada films of 1971

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 

 