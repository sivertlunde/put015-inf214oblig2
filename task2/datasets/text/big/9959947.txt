The Roaring West
{{Infobox Film
| name           = The Roaring West
| image_size     = 
| image	=	The Roaring West FilmPoster.jpeg
| caption        =  Ray Taylor
| producer       = Henry MacRae Ella ONeill Robert C. Rothafel Ed Earl Repp
| narrator       =  Walter Miller
| music          = David Klatzkin Richard Fryer William A. Sickner
| editing        = Irving Applebaum Saul A. Goodkind Alvin Todd Edward Todd
| distributor    = Universal Pictures
| released       = 8 July 1935
| runtime        = 15 chapters (310 min)
| country        = United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Universal Serial film serial.

==Cast==
* Buck Jones as Montana Larkin
* Silver as Silver, Montanas horse
* Muriel Evans as Mary Parker
* Frank McGlynn Sr. as Jinglebob Morgan Walter Miller as Gil Gillespie
* Eole Galli as Ann Hardy
* Harlan Knight as Clem Morgan
* Fred Santley as a saloon singer William Desmond as Jim Parker
* Pat J. OBrien as Cowhand Steve
* Tiny Skelton as Cowhand Happy Charles King as Henchman Tex
* William L. Thorne as Marco Brett
* George Ovey as Cowhand Shorty
* Dick Rush as Sheriff Clark

==Production==

===Stunts=== Cliff Lyons doubling Buck Jones
* Babe DeFreest Wally West

==Chapter titles==
# The Land Rush
# The Torrent of Terror
# Flaming Peril
# Stampede of Death
# Danger in the Dark
# Death Rides the Plains
# Hurled to the Depths
# Ravaging Flames
# Death Holds the Reins
# The Fatal Blast
# The Baited Trap
# The Mystery Shot
# Flaming Torrents
# Thundering Fury
# The Conquering Cowpunchers
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 213
 | chapter = Filmography
 }} 

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 
{{succession box  Universal Serial Serial 
| before=The Call of the Savage (1935)
| years=The Roaring West (1935)
| after=Tailspin Tommy in the Great Air Mystery (1935)}}
 

 

 
 
 
 
 
 
 
 
 


 