Whip It (film)
{{Infobox film
| name           = Whip It
| image          = Whip It (2009 film) poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Drew Barrymore
| producer       = Drew Barrymore Barry Mendel
| screenplay     = Shauna Cross
| based on       =   Eve Juliette Daniel Stern
| music          = The Section Quartet
| cinematography = Robert D. Yeoman
| editing        = Dylan Tichenor
| studio         = Mandate Pictures Flower Films Dune Entertainment Babe Ruthless Productions
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 111 minutes  
| country        = United States
| language       = English
| budget         = $15 million 
| gross          = $16,633,035 
}} sports comedy-drama film written by Shauna Cross, based on her novel Derby Girl. The film is directed and co-produced by Drew Barrymore in her directorial debut. It stars Ellen Page as a teenager from the fictional town of Bodeen, Texas, who joins a roller derby team. The film received generally positive reviews from critics but was a box office disappointment, making $16.6 million worldwide against its $15 million budget.   

==Plot==
Bliss Cavendar (Ellen Page) is a misfit in the small town of Bodeen, Texas, with no sense of direction in her life. Her mother, Brooke (Marcia Gay Harden), a former beauty queen, pushes her to enter beauty pageants despite her lack of interest. During a shopping trip to Austin with her mother, Bliss encounters three roller derby team members. Intrigued, she and her friend Pash attend a roller derby bout under the pretense of going to a football game, where they see the Holy Rollers defeat the Hurl Scouts, a perennially unsuccessful derby team.

Returning to Austin, Bliss lies about her age and tries out for the Hurl Scouts, discovering her natural skating abilities in the process. After realizing she needs to be ruthless in roller derby, Bliss also sees she needs to take charge in other aspects of her life. One involves Bliss love interest, a lanky young rock guitarist and singer named Oliver (Landon Pigg) whom she meets via her exposure to roller derby. They enjoy a whirlwind romance before Oliver leaves for a tour, taking a T-shirt Bliss gave him to remember her by, in exchange for his jacket. She later finds a picture of him at a gig with another girl, who is wearing her T-shirt. She burns his jacket in retribution and subsequently breaks up with him following his return, although he vehemently denies that anything happened.
 Daniel Stern) who seldom opposes his wifes parental decisions. Although, eventually, Bliss father convinces her mother to let Bliss out of a pageant (which is at the same time as the championship roller derby game) and convinces the Hurl Scouts to come get Bliss for the bout. Various other sub-plots include her relationship with Pash, and confrontations with a stuck up snob at school. Pash is fine with Bliss new path, until she gets arrested with an open container of beer while she is waiting for Bliss, who has left to go find Oliver. Eventually, Pash gets together with their manager and forgives Bliss. The movie ends with the Hurl Scouts narrowly losing the championship match and everyone finally getting along; the team chants "Were number two!" which was the same thing they chanted when they lost their first match.

==Cast==
 (in order of appearance) 
* Ellen Page as Bliss Cavendar (Babe Ruthless)
* Alia Shawkat as Pash
* Marcia Gay Harden as Brooke Cavendar Daniel Stern as Earl Cavendar
* Carlo Alban as Dwayne (Birdman)
* Landon Pigg as Oliver
* Jimmy Fallon as Hot Tub Johnny Rocket
* Kristen Wiig as Maggie Mayhem
* Zoë Bell as Bloody Holly Eve as Rosa Sparks
* Drew Barrymore as Smashley Simpson Andrew Wilson as Razor
* Juliette Lewis as Iron Maven
* Ari Graynor as Eva Destruction
* Har Mar Superstar as Fight Attendants Coach

==Production== adapted her 2007 Young-adult fiction|young-adult novel Derby Girl for the screen and pitched the script to different production companies while simultaneously pitching its source material to various publishers. Once Barrymore got involved, she and Cross worked for months on script revisions, with Barrymore pushing her to "avoid her storys tidier prospects, to make things more raw and open ended." 
 Production began Birch Run. Real roller girls were selected from local Michigan teams such as the Detroit Derby Girls, and the Grand Raggidy Roller Girls.    Several scenes were also shot in Austin, Texas.       Some scenes were also shot in Hamtramck, Michigan at Hamtramck High School as well as Ferndale, Michigan at Ferndale High School.

Whip It was produced by Barry Mendel and Drew Barrymore; a co-production between Mandate Pictures and Barrymores Flower Films. The film was distributed by Fox Searchlight Pictures.

==Release==
 
Whip It was premiered at the Toronto International Film Festival in September 13, 2009 and was theatrically released on October 2, 2009.
===Home media===
Whip It was released on DVD and Blu-ray in January 26, 2010 by 20th Century Fox Home Entertainment.

==Reception==
===Critical response=== average score of 7.1/10.    Metacritic calculated an average score of 68, based on 31 reviews.   

A. O. Scott called the film "predictable", but said "You might, nonetheless, want to see this movie, even — or maybe especially — if you have seen Billy Elliot or Bend It Like Beckham. Familiarity is not always a bad thing, and if the script, by Shauna Cross, piles sports movie and coming-of-age touchstones into a veritable cairn of clichés, the cast shows enough agility and conviction to make them seem almost fresh."  Roger Ebert said its a "coming-together of two free spirits, Drew Barrymore and Ellen Page, and while it may not reflect the kind of female empowerment Gloria Steinem had in mind, it has guts, charm, and a black-and-blue sweetness."  According to The Miami Herald, "Whip It is completely predictable from the first frame. It also is ridiculously, utterly entertaining. Drew Barrymores smashing directorial debut harkens back to an era in which Hollywood studio pictures could still move and enthrall the audience while plying in hoary cliches." 

===Box office===
Whip It was a financial disappointment.        Initial studio estimates showed Whip It in a tie for #6 in its opening weekend, tying with the widely released  , and it wound up taking sixth place with $4,650,812.  The film grossed $13,043,363 in North America. It grossed $3,589,672 in foreign movie sales, for a grand total of $16,633,035 world wide. Production cost of the film was $15 million.   

===Awards===
Drew Barrymore was nominated for a Bronze Horse at the Stockholm Film Festival.

==Premiere==
  skaters play an exhibition bout at the Toronto International Film Festival premiere of Whip It, September 13, 2009]]
The world premiere of the film was held at the 2009 Toronto International Film Festival in Toronto|Toronto, Canada.    The majority of the films main cast attended the premiere. As part of the festivities, members of local league Toronto Roller Derby held an exhibition bout at Yonge Dundas Square in downtown Toronto.  Toronto Roller Derby skaters, like those in other cities with major roller derby leagues, also helped promote the film in advance of the screening by wearing costumes from the film and skating around town and performing stunts while handing out flyers and giveaway items. Toronto Roller Derby credits their luck in being able to skate at the festival for helping expand their fanbase as well as their skater rosters.   

==Soundtrack==
{{Infobox album  
| Name        = Whip It
| Type        = Soundtrack
| Artist      = various artists
| Cover       =
| Released    = September 29, 2009
| Recorded    =
| Genre       =
| Length      =
| Label       = Rhino Entertainment
| Producer    =
}}

The film has a 58-song playlist, with a wide range of styles and genres.  Nineteen tracks appear on the CD of the soundtrack, with additional tracks at various MP3 stores. According to Allmusic, "The disc is a blend of familiar old standbys (including a glittering remix of the Chordettes "Lollipop") and indie acts (among them Barrymores ex-boyfriend Fabrizio Morettis band Little Joy), achieving the kind of safely edgy balance that embodies the Fox Searchlight aesthetic (that is, its quirky enough to appeal to the cool kids, but never strays too far from the mainstream)." 

;Track listing
 
# Tilly and the Wall – "Pot Kettle Black"
# Ramones – "Sheena Is a Punk Rocker"
# Cut Chemist featuring Hymnal – "Whats the Altitude"
# The Breeders – "Bang On"
# The Raveonettes – "Dead Sound"
# Clap Your Hands Say Yeah – "Blue Turning Grey"
# Jens Lekman – "Your Arms Around Me"
# Gotye – "Learnalilgivinanlovin" Peaches – "Boys Wanna Be Her"
# Dolly Parton – "Jolene (song)|Jolene" 38 Special – "Caught Up in You" Adam Green – "Never My Love" Goose – "Black Gloves"
# The Ettes – "Crown of Age"
# Landon Pigg featuring Turbo Fruits – "High Times"
# Little Joy – "Unattainable" Lollipop (Squeak E. Clean & Desert Eagles remix)"
# The Go! Team – "Doing it Right"
# Apollo Sunshine – "Breeze"
# MGMT - "Kids"
# Turbo Fruits – "Fun Dream Love Dream" (Amazon MP3 version track)
#  Young MC – "Know How" (iTunes version bonus track)
# The Section Quartet – "The Road to Austin" (iTunes version bonus track)
# Radiohead - "No Surprises"
# Gilberto Gil - "Domingo no Parque"  

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 