The Anderssons Rock the Mountains
{{Infobox film
| name           = The Anderssons Rock the Mountains
| image          = Sune i fjällen 2014.jpg
| alt            =
| caption        = Some of the main persons behind the film. From left: Producer Teresa Alldén-Willey, actors Anja Lundqvist and William Ringström samt and producer Patrick Ryborn.
| director       = Gustaf Åkerblom
| producer       = Teresa Alldén-Willey, Patrick Ryborn
| writer         = Hannes Holm
| based on       =  
| starring       = Morgan Alling, Anja Lunqvist, William Ringström
| music          = 
| cinematography = Mats Axby
| editing        = 
| studio         = Eyeworks Scandi Fiction, Nordisk film, Sveriges Television|SVT, Film i väst, Nouvago Capital
| distributor    = Nordisk Film
| released       =  
| runtime        = 
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
The Anderssons Rock the Mountains  ( ) is a Swedish comedy film|comedy, childrens film|childrens and family film, which opened on 19 December 2014. The film, partly recorded in Åre, is the final in a trilogy of Sune films.

==Plot==
The film tells the story about when the Anderssons family go on a skiing vacation.  to the Scandinavian Mountains.

==Actors==
Following actors appear.  

*William Ringström - Sune
*Morgan Alling - Rudolf
*Anja Lundqvist - Karin
*Julius Jimenez Hugoson  - Håkan
*Hanna Elffors Elfström - Anna
*Julia Dufvenius - Sabina Erik Johansson - Pontus
*Kajsa Halldén - Sofie
*Kalle Westerdahl - Ragnar
*Frida Hallgren - Yvonne
*Malte Gårdinger - Santos
*Bonn Ulving - Pär Päron
*Manuel Dubra - Santos father
*Patrik Zackrisson - olycks-Jörgen
*Kristina Korths-Aspegren - olycks-Kicki Martin Andersson - reindeer herd
*Emma Melkersson - Linda
*Emanuel Edoff - Peder
*Pontus Eklöf - Roger
*Jörgen Persson (skådespelaren)|Jörgen Persson - man with sled dog
*Ebbe Olsson - Thomas father
*Wiliam Mehler - Thomas
*Jimmy Söderblom - ice sculptor
*Maja Fahl - headteacher
*Jon Olsson - himself
*Jessica Almenäs - Lets Dance programme host
*Tony Irving - Lets Dance jury member
*Dermot Clemenger - Lets Dance jury member
*Ann Wilson - Lets Dance jury member
*Fie Wersén - Sofie (stand-in)
*Colin Wasell - Sune (stand-in)
*Felicia Hållstrand - Anna (stand-in)
*Fredrik Neiman - Rudolf/dog sled-driver (stand-in)

==Music==
Music 
 Vikingarna
*Canelloni, Macaroni with Lasse Holm La belissima estate (One Beautiful Summer)
*Diggi-Loo Diggi-Ley La Bamba with The Music Super Men
*De sista ljuva åren Papaya Coconut with Kikki Danielsson Swedish National Skiing Team

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 