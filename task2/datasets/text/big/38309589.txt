The Odds Against Her
{{Infobox film
| name           = The Odds Against Her
| image          =
| caption        =
| director       = Alexander Butler  Jack W. Smith
| writer         =  George Foley
| music          = 
| cinematography = 
| editing        = 
| studio         = Barker Films
| distributor    = Jury Films
| released       = November 1919
| runtime        = 5 reels  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama film directed by Alexander Butler and starring Milton Rosmer, Edna Dormeuil and Lorna Della. It was made at Ealing Studios.

==Cast==
* Milton Rosmer as Leo Strathmore
* Edna Dormeuil as Nanette
* Lorna Della as Lolita Rios George Foley as The Baron
* Thomas H. MacDonald
* Nancy Kenyon
* Vernon Davidson
* André Randall

==References==
 

==Bibliography==
* Bamford, Kenton. Distorted Images: British National Identity and Film in the 1920s. I.B. Tauris, 1999.
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

==External links==
* 

 
 
 
 
 
 
 
 
 


 