Invisible Adversaries
  1977 cinema Austrian experimental experimental drama film directed by Valie Export, her debut feature film. 

==Plot==
Set in contemporary Vienna, the film involves a photographer, Anna, who discovers that extraterrestrial beings are colonizing the minds of her fellow citizens by raising the human aggression quotient. The outer world immediately becomes disjointed, yet the inner world does too, as Anna and her lover, Peter, try to hang onto their deteriorating relationship.

==Cast==
*Susanne Widl as Anna
*Peter Weibel as Peter
*Josef Plavec
*Monica Helfer-Friedrich
*Dominick Dusek
*Herbert Schmid
*Edward Neversal
*Helke Sander

==Release==
The film was released on DVD by Facets Multi-Media in 2011 in a limited edition of 500 copies. 

==Reception== The Heights wrote that "I was expecting a great feminist work here, but got mediocrity. I cannot recommend that you see this film," though he nevertheless praised the film as a "cornucopia of video spectacle   Export delights, confounds and amuses the audience with her antics." 

==References==
 

==External links==
*  at    
* 
* 
*  TCM Movie Database Film Portal  
 
 
 
 
 
 
 
 
 
 
 