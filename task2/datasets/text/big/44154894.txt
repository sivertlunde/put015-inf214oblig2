Oru Madapravinte Katha
{{Infobox film
| name           = Oru Madapravinte Katha
| image          =
| caption        =
| director       = Alleppey Ashraf
| producer       =
| writer         = Alleppey Ashraf
| screenplay     = Alleppey Ashraf Shubha
| music          = G. Devarajan
| cinematography = S Kumar
| editing        = A Sukumaran
| studio         = Indukala
| distributor    = Indukala
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, Shubha in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir
*Mammootty
*Sankaradi Shubha
*Ajayan
*Maniyanpilla Raju
*K. P. A. C. Azeez
*Bheeman Raghu
*Kundara Johny
*Kuthiravattam Pappu Nalini
*Ramu
*Renuchandra Seema
*Vanitha Krishnachandran
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Muthe va va mutham tha tha || K. J. Yesudas, Sonia (Baby Sonia) || Yusufali Kechery || 
|-
| 2 || Njaanoru Malayaali     || K. J. Yesudas || Yusufali Kechery || 
|-
| 3 || Njanoru malayali   || K. J. Yesudas || Yusufali Kechery || 
|-
| 4 || Njanoru malayali   || K. J. Yesudas || Yusufali Kechery || 
|-
| 5 || Vaanil neelima || K. J. Yesudas, P. Madhuri || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 