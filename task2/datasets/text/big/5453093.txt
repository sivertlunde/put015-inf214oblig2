Waco (1966 film)
{{Infobox Film
| name           = Waco 
| image_size     = 
| image	=	Waco FilmPoster.jpeg
| caption        = 
| director       = R. G. Springsteen
| producer       = A. C. Lyles
| writer         = Max Lamb Steve Fisher Harry Sanford
| narrator       = 
| starring       = Howard Keel Jane Russell Brian Donlevy
| music          = Jimmie Haskell
| cinematography = Robert Pittack
| editing        = 
| distributor    = 
| released       = 1 September 1966 
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1966 western film starring Howard Keel and Jane Russell, directed by R. G. Springsteen, written by Max Lamb, Steve Fisher, and Harry Sanford, and released by Paramount Pictures.
 Darker than Amber.
 Terry Moore, Robert Lowery.

==Plot==
A town in Wyoming is up in arms. Somebody has shot the sheriff, Billy Kelly, and things are so bad that preacher Sam Stone and businessman George Gates implore the mayor, Ned West, to bring in outside help.

They arrange for a gunfighter called "Waco" to be pardoned by the governor and sprung from jail. Waco rides to town and immediately cleans it up, defying boss Joe Gore by becoming sheriff, firing the deputy and bringing in old partner Ace Ross to be by his side.

Preacher Stone is happy but wife Jill is not. She used to be involved with Waco, who isnt going to like it that she got married while he was behind bars.

Also unhappy is rancher Ma Jenner, whose boys Ike and Pete seek revenge on her behalf because Waco killed their brother. And mayor West has troubles of his own because of an attack on his daughter Patricia.

It all comes down to a gunfight. Waco is outnumbered, particularly after Ace chickens out and abandons him, but the mayor and preacher come to his aid. Gore and the Jenners end up dead, but so does Stone, which means Jill and Waco can be together again.

==Cast==
*Howard Keel as Waco
*Jane Russell as Jill Stone
*Brian Donlevy as Ace Ross
*Wendell Corey as Preacher Sam Stone Terry Moore as Dolly
*John Agar as George Gates
*Richard Arlen as Sheriff Billy Kelly
*DeForest Kelley as Bill Rile Robert Lowery as Mayor Ned West

== External links ==
* 

 
 
 


 