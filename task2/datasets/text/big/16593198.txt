A Yank at Eton
{{Infobox film
| name           = A Yank at Eton
| image_size     =
| image	         = A Yank at Eton FilmPoster.jpeg
| caption        = Lobby card
| director       = Norman Taurog
| producer       = John W. Considine Jr. Thomas Phipps Lionel Houser Ian Hunter Peter Lawford
| music          = Bronislau Kaper Charles Lawton Jr.
| editing        = Albert Akst
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =$751,000  . 
| gross          = $2,677,000 
}}

A Yank at Eton is an American comedy/drama film. It was the 1942 sequel to the 1938 A Yank at Oxford. It tells the tale of a cocky youth (Rooney) unwillingly yanked from an American school who has to move to England, where he is sent to attend the elite Eton College.  With England at war, all of the movie was filmed in the United States, not at Eton.

Much of the storyline relates to the misunderstandings arising from differences between the two countries cultures, customs and language. At first these cause the boy anger and confusion, particularly against the traditional practices of fagging and physical hazing inflicted at Eton on the lower boys by the uppers. The film caricatures Etonian manners and behavior as snobbish and stuffy, but in due course Rooneys character settles down, stops being rebellious and comes to realize that, beneath the different habits and views, "Yanks" and "Limeys" have basic values in common and can get along when they have to.

The propaganda intent, as U.S. troops poured into the U.K. to join World War II in 1942, was evidently to show that Americans and Britons could set aside their superficial differences and pull together in the common cause of the war effort.

The film has the Eton boating song as its theme tune (played at a faster tempo than usual), though no boating is shown in the film.

==Plot summary==
 

==Cast==
* Mickey Rooney as Timothy Dennis
* Edmund Gwenn as Headmaster Justin Ian Hunter as Roger Carlton
* Freddie Bartholomew as Peter Carlton
* Marta Linden as Winifred Dennis Carlton
* Juanita Quigley as Jane "The Runt" Dennis
* Alan Mowbray as Mr. Duncan
* Peter Lawford as Ronnie Kenvil
* Raymond Severn as Isaac "Inky" Weeld

==Reception==
The movie was very popular: according to MGM records it earned $1,542,000 in the US and Canada and $1,135,000 elsewhere, giving the studio a profit of $1,101,000. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 