Sitara (film)
{{Infobox film
 | name = Sitara
 | image = Sitaramithun.jpg
 | caption = DVD Cover
 | director = Meraj
 | producer = 
 | writer = 
 | dialogue =  Kanhaiyalal
 | music = R.D. Burman
 | lyrics = Gulzar
 | associate director = 
 | art director = 
 | choreographer = 
 | released = June 13, 1980
 | runtime = 125 min.
 | language = Hindi
 | budget =   2 Crores
 | preceded_by = 
 | followed_by = 
 }}
 Indian feature directed by Meraj, starring Mithun Chakraborty, Zarina Wahab and Kanhaiyalal (actor)|Kanhaiyalal. 

==Plot==

Sitara is the love story of two childhood lovers, Dhania and Kundan. They comes to Bombay, due to their economic conditions and they start working in a Cinema hall. Dhania is fascinated by films and has hidden desire to become an actress, so she often imitates the dances shown on movies. One day Dhania plays the record of the songs and start dancing assuming herself as the heroine of the film. She pulls Kundan also to join her and both involves in the dance, forgetting every thing. One person related with films watches them and he is impressed by the skill of Dhania and he helps her to become a filmstar Sarita from Dhania. Now Kundan feels lonely in this glamour world and their love is lost somewhere. He still loves her, but she is lost in her dreams of becoming a superstar. Depressed Kundan decides to return to his village. Sitara, explores the life of actors coming from small towns and villages.

==Cast==

*Mithun Chakraborty
*Zarina Wahab
*Kanhaiyalal

==Soundtrack==
{{Infobox album Name  Sitara
|Type     = film Cover    = Released =  Artist   = R.D. Burman Genre  Feature film soundtrack Length   = 14.24 Label    = Polydor
}}

The films biggest attraction is the Music given by R.D. Burman. The Lyrics were written by Gulzar

*Yeh Saye hain (These are the shadows) –  Asha Bhonsle Bhupinder Singh
*Sajana ka kangna (bracelet of lover) –  Asha Bhonsle & Bhupinder Singh
*Sath Sath tum chalo (You walk along with me) –  Asha Bhonsle & Bhupinder Singh

==References==
 

==External links==
*  

 
 