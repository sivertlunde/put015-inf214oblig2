Dosti Dushmani
{{Infobox film
| name      = Dosti Dushmani
| image     = DostiDushmani.jpg
| caption   = Vinyl Record Cover
| director  = T. Rama Rao
| writer    = 
| starring  = Jeetendra Rajinikanth Rishi Kapoor Kimi Katkar Poonam Dhillon Bhanupriya
| producer  = T.RamaRao
| music     = Laxmikant Pyarelal
| editor    = 
| released  = 31 October 1986
| runtime   = 
| language  = Hindi
| budget    =  41 lakh
}}

Dosti Dushmani is a Hindi film is Produced & directed by T. Rama Rao on Srinath Productions, presented by A.Ranga Rao. It stars Jeetendra, Rajinikanth, Rishi Kapoor, Poonam Dhillon, Kimi Katkar, Bhanupriya,Pran, Kadar Khan, Asrani, Shakti Kapoor and Amrish Puri.  It is remake of Telugu Film Mugguru Mitrulu.

==Plot==
Dr.Sandeep Kumar (Jeetendra), Inspector Ranjith (Rajinikanth) & Advocate Prakash (Rishi Kapoor) are thickest, best & childhood friends, the three of them grew up together because Prakash is often and Ranjith’s father Daga (Amrish Puri) is a smuggler who left him alone because of his honesty and took his mother & sister with him so Sandeep’s parents (Pran & Shavukar Janaki) brought them up. Ranjith is an honest police officer, Sandeep is a well known doctor in the city, Prakash is a leading criminal lawyer three of them has always clash with Daga who became a big gangster and doesn’t know that Ranjith is his son. Ranjith & Prakash always carrels with Sandeep because the only problem with Sandeep is he is very short temper, he cannot withstand himself if somebody does any injustice to anyone. One day Daga’s gangster (Guru Bachan) kidnaps and rapes Ranjith’s wife Lata (Poonam Dhillon) who is pregnant which is seen by Sandeep and he bits him very hard and takes promise from Lata that she does not reveal this secret. Daga takes advantage of this he kills the gangster and traps Sandeep in that case. Ranjith arrests Sandeep and Prakash takes up the case but Sandeep doesn’t reveal the truth to protect Lata so Prakash makes a story that Sandeep has made the crime to protect his wife to save him, he brings a girl Rekha (Bhanu Priya) as Sandeep’s wife whom Sandeep has protected from few gangsters once a while. Meanwhile Sandeep loves and really marries Rekha and Prakash also loves a thief Shanthi (Kimi Katkar) whose brother is killed by Daga and for whom she is under search. At the end Ranjith comes know the truth, he accepts Lata even though she got raped and he recognizes Daga is his father and Rekha is his sister whom Daga left in streets in the childhood. Finally Sandeep, Ranjith & Prakash join together and defeat Daga.

==Cast==
* Jeetendra as Dr.Sandeep Kumar
* Rajinikanth as Inspector Ranjith
* Rishi Kapoor as Advocate Prakash
* Amrish Puri as Daga Pran as Sandeeps Father
* Kader Khan as Nishaan
* Shakti Kapoor as Kamal
* Shafi Inamdar as Police Commissioner
* Asrani as Lawyer
* Guru Bachan as Rowdy
* Poonam Dhillon as Lata
* Kimi Katkar as Shanthi
* Bhanupriya as Rekha Shavukar Janaki as Sandeeps Mother

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jhatke Pe Jhatke"
| Mohammed Aziz, Kavita Krishnamurthy
|-
| 2
| "Saat Baras Ka Dulha"
| Shailender Singh, Mohammed Aziz, Kavita Krishnamurthy
|-
| 3
| "Ithlaye Kamar Band Kamre Mein"
| S. P. Balasubrahmanyam, S. Janaki
|-
| 4
| "Munne Ki Amma"
| Mohammed Aziz, Kavita Krishnamurthy
|-
| 5
| "Yaaro Hum Ko Dekh"
| Suresh Wadkar, Mohammed Aziz, Shailender Singh
|}

== External links ==
*  

 
 
 
 
 

 