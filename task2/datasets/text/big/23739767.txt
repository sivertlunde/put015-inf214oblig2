David Livingstone (film)
 
 
 
{{Infobox film
| name =  David Livingstone 
| image =
| image_size =
| caption =
| director = James A. FitzPatrick
| producer = James A. Fitzpatrick
| writer = W.K. Williamson  
| narrator = Marian Spencer   James Carew   Pamela Stanley
| music = Gideon Fagan 
| cinematography = Hone Glendinning 
| editing = 
| studio = FitzPatrick Pictures  MGM 
| released = November 1936
| runtime = 71 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical adventure Marian Spencer and James Carew. It portrays the expedition of the British explorer David Livingstone to Africa to discover the source of the Nile, his disappearance, and the expedition to find him led by Henry Morton Stanley|Stanley.   The film was made at Shepperton Studios for distribution by Metro-Goldwyn-Mayer|MGM. 

==Main cast==
* Percy Marmont as David Livingstone  Marian Spencer as Mary Moffatt 
* James Carew as Gordon Bennett 
* Pamela Stanley as Queen Victoria  Hugh McDermott as H.M. Stanley

==References==
 

==Bibliography==
* Harper, Sue. Picturing the Past: The Rise and Fall of the British Costume Film. British Film Institute, 1994.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 

 
 