Gagarin: First in Space
{{Infobox film
| name           = Gagarin. Pervyy v kosmose  (English: Gagarin: First in Space) 
| image          = Gagarin FIRST IN SPACE.jpg
| alt            = 
| caption        = English release film poster 
| film name      = 
| director       = Pavel Parkhomenko
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       = Vostok 1 
| starring       =  
| narrator       = 
| music          = George Callis
| cinematography = Anton Antonov
| editing        =  
| studio         = Kremlin Films
| distributor    =  
| released       =   
| runtime        = 108 minutes
| country        = Russia
| language       = Russian  (with other-languages subtitled) 
| budget         = $9.5 million (estimated)
| gross          =  
}} Russian  docudrama biopic about the first man in space Yuri Gagarin on the mission of Vostok 1. It was released by Central Partnership theatrically in Russia on June 6, 2013, and in the United Kingdom on DVD on June 23, 2014 by Entertainment One.  The films running time of 108 minutes approximates the time it took Gagarin to go around the earth before returning. It stars Yaroslav Zhalnin as Soviet fighter pilot and cosmonaut Yuri Gagarin. The film received mixed reviews, with some critics praising the films acting, direction and storytelling with others touching on the films "cheap-looking" visual effects and attempting to market the films artistic style to Alfonso Cuaróns science fiction epic Gravity (film)|Gravity. The film received criticism for its state funding and ignoring the aftermath of the flight.

==Plot==
On April 12, 1961, Soviet cosmonaut Yuri Gagarin blasted off in a Vostok rocket, becoming the first human in space and orbiting Earth for 108 minutes. He was one of the first group of cosmonauts who were selected from over three thousand fighter pilots throughout the Soviet Union.

The legendary top twenty who were selected were the ace of aces and none of the pilots knew which amongst them would make history on the first manned flight. Once chosen Gagarin is fast tracked to train for the unknown and the trip of a life time!

Whilst strapped into his rocket, Gagarin reflects on his life which is intercut with the determination of the Russian space team and their untiring efforts to conquer space.

==Cast==
 
 
* Yaroslav Zhalnin as Yuri Gagarin
* Mikhail Filippov as Sergey Korolev
* Olga Ivanova as Valentina Ivanova
* Vadim Michman as German Titov
* Vladimir Steklov as Nikolai Kamanin
* Viktor Proskurin as Yuris father
* Nadezhda Markina as Yuris mother
* Daniil Vorobyov as Grigory Nelyubov
* Inga Strelkova-Oboldina as Adilya Kotovskaya
* Sergey Tezov as Colonel Karpov
* Anatoliy Otradnov as Andrian Nikolaev
* Vladimir Chuprikov as Nikita Khrushchev
* Sergey Laktyunkin as Valeriy Bykovsky
* Anatoliy Gushchin as Alexei Leonov
* Dmitriy Tikhonov as Konstantin Feoktistov
* Sergey Kalashnikov as Pavel Popovich
* Anzor Kamariddinov as Mars Rafikov
* Sergey Kagakov as Oleg Ivanovsky
* Nina Esina as Zoya
* Olga Yakovtseva as Tanya
* Danila Bukrin as Boriska
* Vadim Golishnikov as Boris Rauschenbach
 

==Home Media==
The film was released on DVD and Digital Download on 23 June 2014 by Entertainment One. Despite being filmed in HD the film has currently not been released on Blu-ray, however it is in HD on Amazon Instant Video. A Russian release date has not been confirmed though expected soon.

==Reception==
Gagarin: First in Space received mixed reviews. Michael Dodd of Bring the noise gave the film a 7/10. He said "Despite solid acting and superb direction there is an area in which this film unfortunately suffers, and that is its special effects. Shots of Gagarin’s rocket, both on the launch pad and flying above the Earth, look cheap and unconvincing, which is not good when all the press surrounding the movie".  He said that the film was trying to copy the artistic style of Gravity (film). This is present in the films DVD cover with resembles the style of Gravity.

Gary Collinson of FlickeringMyth.com disagrees with Dodd regarding the special effects. Collinson says, "The CGI, billed in promotional materials as “amazing” is just okay, but given the fairly low budget, it’s good enough and certainly doesn’t weaken the impact of the space flight sequences." He concludes, "This will undoubtedly appeal to space and history buffs. It’s not as gripping and interesting as it could, perhaps should, have been but it’s efficiently made and at least does the historic moment justice." Collinson rates the film at 3/5 stars on both scales ("film" and "movie") used on FlickeringMyth. 

==Controversy==

===Criticism===
The film has been criticized for not portraying the aftermath of the flight. The film does not touch on Gagarins death, for which there are many conspiracy theories.    

===State Funding===
A reviewer on the TV channel Rain accused Parkhomenko of having "made a deadly retro film as if he was turning a feature from (Soviet mouthpiece daily) Pravda into a film". The film got accused of being government controlled, the film being state funded. 

==Relationship to other films==
The film tells the same story as the British documentary film First Orbit. First Orbit re-uses footage of the original flight. First Orbit has received acclaim for its historical accuracy since release.
Critics have noted similaritys to the 2013 British-Mexican Science-Fiction epic Gravity (film)|Gravity. It was mentioned that Gagarin might be trying to use the popularity of Gravity as a marketing ply. This is particularly noticeable in the UK DVD release that used the same font and colours as the release of Gravity.

==References==
 

==External links==
*   at the Internet Movie Database
*   (Russian)

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 