The Heart of Nora Flynn
 
{{Infobox film
| name           = The Heart of Nora Flynn
| image          = Theheart of nora flynn-1916-scene.jpg
| caption        = Scene from the film
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille
| writer         = Jeanie MacPherson Hector Turnbull
| starring       = Marie Doro
| cinematography = Alvin Wyckoff
| editing        = Cecil B. DeMille
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}

The Heart of Nora Flynn is a 1916 American silent drama film directed by Cecil B. DeMille. The film is reportedly preserved at George Eastman House.       

==Cast==
* Marie Doro as Nora Flynn
* Elliott Dexter as Nolan
* Ernest Joy as Brantley Stone
* Lola May as Mrs. Stone
* Billy Jacobs as Tommy Stone Charles West as Jack Murray
* Peggy George as Anne Stone
* Mrs. Lewis McCord as Maggie The Cook

==See also==
*The House That Shadows Built (1931 promotional film by Paramount)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 