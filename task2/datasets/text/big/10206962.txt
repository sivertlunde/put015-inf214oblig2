Wonder Boys (film)
{{Infobox film
| name           = Wonder Boys
| image          = OriginalWBposter.jpg
| caption        = Theatrical release poster
| director       = Curtis Hanson
| producer       = Curtis Hanson Scott Rudin
| based on       =  
| screenplay     = Steven Kloves
| starring       = Michael Douglas Tobey Maguire Frances McDormand Katie Holmes Rip Torn Robert Downey, Jr. 
| music          = Christopher Young
| cinematography = Dante Spinotti
| editing        = Dede Allen
| studio         = Paramount Pictures Curtis Hanson Productions Mutual Film Company Scott Rudin Productions BBC MFF Feature Film Productions Marubeni Tele München Fernseh Produktionsgesellschaft Toho-Towa
| distributor    = Paramount Pictures Warner Bros.
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = $55 million
| gross          = $33,426,588
}} novel of the same title by Michael Chabon. Michael Douglas stars as professor Grady Tripp, a novelist who teaches creative writing at an unnamed Pittsburgh university. He has been unable to finish his second novel, his young wife has left him, and he is having an affair with the Chancellor of the university (Frances McDormand), who is the wife of the chairman of his department. Gradys editor (Robert Downey, Jr.) is in town to see his new book and becomes interested in a book that one of Gradys students (Tobey Maguire) has just completed.
 Rochester and Rostraver Township.

After the film failed at the box office, there was a second attempt to find an audience with a new marketing campaign and a November 8, 2000, re-release, which was also a financial disappointment.

==Plot== Richard Thomas), is the chairman of the English department in which Grady is a professor.  Gradys third wife, Emily, has just left him, and he has failed to repeat the grand success of his first novel, published years earlier.  He continues to labor on a second novel, but the more he tries to finish it the less able he finds himself to invent a satisfactory ending.  The book runs to over two and a half thousand pages and is still far from finished.  He spends his free time smoking Cannabis (drug)|marijuana.

Gradys students include James Leer (Tobey Maguire) and Hannah Green (Katie Holmes).  Hannah and James are friends and both very good writers.  Hannah, who rents a room in Gradys large house, is attracted to Grady, but he does not reciprocate.  James is enigmatic, quiet, dark and enjoys writing fiction more than he first lets on.

During a party at the Gaskells house, Sara reveals to Grady that she is pregnant with his child.  Grady finds James standing outside holding what he claims to be a replica gun, won by his mother at a fairground during her schooldays.  However, the gun turns out to be very real, as James shoots the Gaskells dog when he finds it attacking Grady.  James also steals a very valuable piece of Marilyn Monroe memorabilia from the house.  Grady is unable to tell Sara of this incident as she is pressuring him to choose between her and Emily.  As a result, Grady is forced to keep the dead dog in the trunk of his car for most of the weekend.  He also allows James to follow him around, fearing that he may be depressed or even suicidal.  Gradually, he realizes that much of what James tells him about himself and his life is untrue, and is seemingly designed to elicit Gradys sympathy.

Meanwhile, Gradys editor, Terry Crabtree (Robert Downey Jr.), has flown into town on the pretense of attending the universitys annual WordFest, a literary event for aspiring authors.  In reality, Terry is there to see if Grady has written anything worth publishing, as both mens careers depend on Gradys upcoming book.  Terry arrives with a transvestite whom he met on the flight, called Antonia "Tony" Sloviak (Michael Cavadias).  The pair apparently become intimate in a bedroom at the Gaskells party, but, immediately afterwards, Terry meets James and becomes infatuated with him, and Tony is unceremoniously sent home.  After a night on the town, Terry and James semi-consciously flirt throughout the night, which eventually leads up to the two spending an intimate night together in one of Gradys spare rooms.
 Pittsburgh Police arrive with Sara to escort James to the Chancellors office to discuss the ramifications of his actions.  The memorabilia is still in Gradys car, which has conspicuously gone missing.  The car had been given to him by a friend as payment for a loan, and, over the weekend, Grady has come to suspect that the car was stolen.  Over the course of his travel around town, a man claiming to be the cars real owner repeatedly accosted Grady.  He eventually tracks the car down, but in a dispute over its ownership the majority of his manuscript blows out of the car and is lost.  The cars owner gives him a ride to the university with his wife, Oola, in the passenger seat, with the stolen memorabilia.

Grady finally sees that making things right involves having to make difficult choices.  Grady tells Oola the story behind the memorabilia and allows her to leave with it.  Worried that Gradys choice comes at the expense of damaging Jamess future, Terry convinces Walter not to press charges by agreeing to publish his book, "a critical exploration of the union of Joe DiMaggio and Marilyn Monroe and its function in American Mythopoeia|mythopoetics", tentatively titled The Last American Marriage.

The film ends with Grady recounting the eventual fate of the main characters – Hannah graduates and becomes a magazine editor; James was not expelled, but drops out and moves to New York to rework his novel for publication; and Terry Crabtree "goes right on being Crabtree."  Grady finishes typing his new book (now using a computer rather than a typewriter), which is an account of the events of the film, then watches Sara and their child arriving home before turning back to the computer and clicking "Save."

==Cast==
* Michael Douglas - Professor Grady Tripp
* Tobey Maguire - James Leer
* Robert Downey, Jr. - Terry Crabtree
* Frances McDormand - Chancellor Sara Gaskell
* Katie Holmes - Hannah Green
* Rip Torn - Quentin "Q" Morewood Richard Thomas - Walter Gaskell
* Richard Knox - Vernon Hardapple  Jane Adams - Oola
* Alan Tudyk - Sam Traxler
* George Grizzard - Fred Leer 
* Kelly Bishop - Amanda Leer
* Philip Bosco - Emilys Father
* Michael Cavadias - Miss Antonia "Tony" Sloviak

==Production==
After L.A. Confidential (film)|L.A. Confidential, Curtis Hanson was working on a screenplay of his own and reading other scripts with a keen interest for his next film.    Actress Elizabeth McGovern advised Hanson to work with screenwriter Steve Kloves. When he was given the writers script for Wonder Boys and was told that Michael Douglas was interested in starring, he "fell in love with these characters – and they made me laugh".     Hanson also identified with Grady Tripp and the "thing building up inside him: frustration, hunger, yearning, et cetera". 

===Screenplay===
Kloves, best known for writing and directing The Fabulous Baker Boys, returned to the film business after a self-imposed seven-year retirement to adapt Michael Chabons novel for the money and because he identified with Grady.    He was originally going to direct the film as well but bowed out and Hanson came on board.    Kloves had never adapted a novel before but was encouraged by Chabon to make the material his own. Additional changes were made once Hanson came on board. For example, he felt that James Leer would be a fan of Douglas Sirks films as opposed to Frank Capra as he is in the novel. 

===Casting===
Paramount Pictures was not interested in making a quirky, character-driven comedy/drama until Douglas agreed to work well below his usual large fee.  The actor gained 25 pounds for the film by eating pizza, subs and drinking a lot of beer.    One of the challenges for Hanson was to take a plot that, as he put it, "is meandering and, apparently, sort of aimless", and a character that "does things that even he doesnt really know why hes doing them", and try to create a "feeling of focus" to keep the audience interested.  Another challenge the director faced was working in actual locations in very cold weather that was constantly changing. 

Robert Downey Jr. was on probation during the winter of 1999 when Hanson considered him for a role in Wonder Boys.    Hanson was cautious because of the actors drug history and concerned because it would be a tough film shot in sequence in Pittsburgh in the winter. Downey flew to Pittsburgh and had a long dinner conversation with Hanson where they addressed his problems. The actor demonstrated a commitment to the project and Hanson hired him. Reportedly, Downey acted in a professional manner for the entire four-and-a-half month shoot but after it ended, he returned to Los Angeles and violated his parole. 

===Principal photography===
Paramount suggested shooting Wonder Boys in Toronto or New York City but after reading the book, Hanson realized how important Pittsburgh was to the story, that it was a "wonder boy", much like the films main protagonist Grady Tripp, its a city that had this glorious past of wealth and success that ended. And then it had to deal with figuring out whats next. What happens after triumph?"    Wonder Boys was filmed in Pittsburgh, including locations at Carnegie Mellon University, Chatham College, and Shady Side Academy. Other Pennsylvania locations included Beaver, Rochester, and Rostraver Township. Hanson felt that Pittsburgh was "right, emotionally and thematically" for the film.  The city was experiencing a mild winter during the films shoot and they had to use a lot of artificial snow. 

Hanson contacted Dante Spinotti about working on the film in November 1998.     They had worked previously together on L.A. Confidential. Spinotti had six weeks of pre-production, which he used to perform a variety of tests and shoot a number of important background plates for several scenes that take place at night, in cars. He knew that these scenes included some very critical acting and suggested using the green screen process for greater control.  During pre-production, Hanson and Spinotti used the Kodak Pre View System to storyboard complicated sequences by altering digital still images in a way that simulated the imaging characteristics of camera films. Hanson suggested Spinotti see The Celebration for its technique of keeping the camera extremely close to the actors and carrying deep focus from one actor to the other. Spinotti suggested using a hand-held camera so that the film would not look static. On the first day of shooting, they incorporated some hand-held shots. Hanson liked the results and they used the technique extensively for the rest of the shoot. 

==Soundtrack==
{{Infobox album 
| Name        = Wonder Boys (Music from the Motion Picture)
| Type        = Soundtrack
| Border      = yes
| Artist      = Various artists
| Cover       =
| Released    = February 15, 2000
| Recorded    = 2000
| Genre       = Soundtrack
| Length      = 51:46
| Label       = Sony
| Producer    =
| Last album  =
| This album  =
| Next album  =
}}
Hanson had been a fan of Bob Dylans music since childhood and a great admirer of his soundtrack for Pat Garrett and Billy the Kid.  Dylan admired Hansons previous film, L.A. Confidential and after much convincing, screened 90 minutes of rough footage from Wonder Boys. Hanson picked Dylan because, as he said, "Who knows more about being a Wonder Boy and the trap it can be, about the expectations and the fear of repeating yourself?"   
 Academy Award Best Original Song. Hanson also created a music video for the song, filming new footage of Bob Dylan on the films various locations and editing it with footage used in Wonder Boys as if Dylan were actually in the film. According to Hanson, "Every song reflects the movies themes of searching for past promise, future success and a sense of purpose". 

===Track listing===
# Bob Dylan – "Things Have Changed" 5:10
# Buffalo Springfield – "A Childs Claim to Fame" 2:12 No Regrets" 3:52 Old Man" 3:23
# Bob Dylan – "Shooting Star" 3:09
# Tim Hardin – "Reason to Believe" 2:00
# Little Willie John – "Need Your Love So Bad" 2:17
# Bob Dylan – "Not Dark Yet" 6:30
# Clarence Carter – "Slip Away" 2:32
# Leonard Cohen – "Waiting for the Miracle" 7:43
# Bob Dylan – "Buckets of Rain" 3:23
# John Lennon – "Watching the Wheels" 3:32
# Van Morrison – "Philosophers Stone" 6:03

==Reaction==
===Box office===
In its opening weekend, the film opened at #7 in the North American box office and grossed a total of USD $5.8 million in 1,253 theaters. It went on to gross a total of $33.4 million worldwide. 

===Reception===
Wonder Boys currently holds an 81 percent "fresh" rating at   wrote, "The movies frivolous touches and eccentric details emphasize its dry, measured wit and the power of comedy to underscore serious ideas. Massively inventive, Wonder Boys is spiked with fresh, perverse humor that flows naturally from the straight-faced playing".    gave the film a "C+" rating and Owen Gleiberman wrote, "Curtis Hanson may have wanted to make a movie that gleamed with humanity as much as L.A. Confidential burned with malevolence, but hes so intent on getting us to like his characters that he didnt give them enough juice".  Looking back in his Salon.com review, critic Andrew OHehir felt that Hanson, "and cinematographer Dante Spinotti capture both Pittsburgh (one of the most serendipitously beautiful American cities) and the netherworld of boho academia with brilliant precision. If you went to a liberal-arts college anywhere in the United States, then the way Gradys ramshackle house looks in the wake of Crabs enormous all-night party should conjure up vivid sense-memories". 

=== Re-release ===
 Many critics blamed Paramounts initial ad campaign for the film not finding a mainstream audience. The  .)" {{cite news
 | last = Brodesser
 | first = Claude
 | coauthors =
 | title = Par to repackage Wonder Variety
 | pages =
 | language =
 | publisher =
 | date = May 22, 2000
 | url = http://www.variety.com/article/VR1117781947.html?categoryid=13&cs=1
 | accessdate = 2007-05-23 }}  The   to mind."  Hanson said that the poster made Douglas look "like he was trying to be Robin Williams".  In an interview with Amy Taubin, Hanson said, "The very things that made Michael and I want to do the movie so badly were the reasons it was so tricky to market. Since films go out on so many screens at once, theres a need for instant appeal. But Wonder Boys isnt easily reducible to a single image or a catchy ad line".  Hanson felt that the studio played it safe with the original ad campaign. They also released it a week after the Academy Award nominations were announced and the studio spent more money promoting the films of theirs that were nominated and not enough on Wonder Boys. The studio pulled the film out of theaters and quickly canceled the video release. Hanson and the films producer Scott Rudin lobbied to have it re-released.  They designed a new campaign  including posters and a trailer for the re-release that emphasized the ensemble cast. 

==Awards and Nominations==
===Awards=== Toronto Film Critics Association Awards (2000)
**TFCA Award – Best Supporting Male Performance: Tobey Maguire Los Angeles Film Critics Association (2000)
**LAFCA Award – Best Actor Michael Douglas
**LAFCA Award – Best Supporting Actress: Frances McDormand
*Boston Society of Film Critics (2000):
**BSFC Award – Best Screenplay: Steve Kloves
**BSFC Award – Best Supporting Actress: Frances McDormand USA Golden Globes (2001):
**Golden Globe –   USA Academy Awards (2001):
**Academy Award –   Broadcast Film Critics Association Awards (2001):
**Critics Choice Award – Best Screenplay: Steve Kloves
**Critics Choice Award – Best Supporting Actress: Frances McDormand Florida Film Critics Circle Awards (2001):
**FFCC Award – Best Supporting Actress: Frances McDormand
*L.A. Outfest (2001):
**Screen Idol Award – Robert Downey Jr.
*Las Vegas Film Critics Society Awards (2000):
**Sierra Award – Best Screenplay: Steve Kloves
**Sierra Award – Best Song: Bob Dylan Satellite Awards (2001):
**Golden Satellite Award – Best Performance by an Actor in a Motion Picture, Comedy or Musical: Michael Douglas Southeastern Film Critics Association (2001):
**SEFCA Award – Best Actor: Michael Douglas
*USC Scripter Award (2001):
**USC Scripter Award – Michael Chabon (writer) and Steve Kloves (screenplay)

===Nominations===
*Teen Choice Awards (2000)
**Teen Choice Award – Choice Liar: Tobey Maguire Grammy Awards (2001):
**Best Song Written for a Motion Picture, Television or Other Visual Media: Bob Dylan Golden Globes (2001):
**Golden Globe – Best Motion Picture
**Golden Globe – Best Performance by an Actor in a Motion Picture: Michael Douglas
**Golden Globe – Best Screenplay: Steve Kloves Academy Awards (2001):
**Academy Award – Best Editing: Dede Allen
**Academy Award – Best Writing: Steve Kloves
*Art Directors Guild (2001):
**Excellence in Production Design Award – Feature Film BAFTA Awards (2001):
**BAFTA Film Award – Best Performance by an Actor in a Lead Role: Michael Douglas
**BAFTA Film Award – Best Adapted Screenplay: Steve Kloves Broadcast Film Critics Association Awards (2001):
**Critics Choice Award – Best Picture Chicago Film Critics Association (2001):
**CFCA Award – Best Actor: Michael Douglas
**CFCA Award – Best Picture
**CFCA Award – Best Screenplay: Steve Kloves
*GLAAD Media Awards (2001):
**GLAAD Media Award – Outstanding Film
*Las Vegas Film Critics Society Awards (2000):
**Sierra Award – Best Actor: Michael Douglas
**Sierra Award – Best Editing: Dede Allen
**Sierra Award – Best Supporting Actress: Frances McDormand
*London Critics Circle Film Awards (2001):
**ALFS Award – Actor of the Year: Michael Douglas
**ALFS Award – Screenwriter of the Year: Steve Kloves Online Film Critics Society (2001):
**OFCS Award – Best Actor: Michael Douglas
**OFCS Award – Best Ensemble Cast Performance
**OFCS Award – Best Screenplay: Steve Kloves
*Phoenix Film Critics Society (2001):
**PFCS Award – Best Actor in Leading Role: Michael Douglas
**PFCS Award – Best Actor in Supporting Role: Tobey Maguire
**PFCS Award – Best Actress in Supporting Role: Frances McDormand
**PFCS Award – Best Director: Curtis Hanson
**PFCS Award – Best Original Song: Bob Dylan
**PFCS Award – Best Picture
**PFCS Award – Best Screenplay: Steve Kloves Satellite Awards (2001):
**Golden Satellite Award – Best Motion Picture, Comedy or Musical
**Golden Satellite Award – Best Original Song: Bob Dylan USA Writers Guild (2001):
**WGA Award – Best Screenplay Based on Material Previously Produced or Published: Steve Kloves

==References==
 

== External links ==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 