Dog's Heads
 
{{Infobox film
| name           = Dogs Heads
| image          = 
| caption        = 
| director       = Martin Frič
| producer       = 
| writer         = Alois Jirásek Martin Frič
| starring       = Vladimír Ráz
| music          = 
| cinematography = Jan Stallich
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

Dogs Heads ( ) is a 1955 Czech drama film directed by Martin Frič, based on the novel of the same name by Alois Jirásek.    It was entered into the 1955 Cannes Film Festival.     

==Cast==
* Vladimír Ráz - Jan Sladký-Kozina
* Jana Dítetová - Hancí
* Jarmila Kurandová - Mother
* Zdenek Stepánek
* Frantisek Kovárík - Krystof Hrubý
* Ladislav Pesek - Rehurek
* Jirina Steimarová - Dorla
* Jaroslav Prucha - Pribek
* Frantisek Smolík - Pribek, senior
* Jana Stepánková - Manka
* Bohumil Svarc - Serlovský
* Jirí Dohnal - Adam Ecl Ctverák
* Jaroslav Vojta - Pajdár
* Milos Nedbal - Lamminger z Abenreuthu
* Milos Kopecký - Kos, správce
* Marie Brozová
* Marta Fricová - Barbora

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 