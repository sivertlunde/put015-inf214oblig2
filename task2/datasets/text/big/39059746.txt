Paisa Paisa (film)
{{Infobox film
| name           = Paisa Paisa
| image          = Paisa Paisa film poster.jpg
| alt            =  
| caption        = Film poster
| director       = Prasanth Murali
| producer       = Raj Zacharias
| writer         = Rajesh Varma Prashanth Murali
| starring       =  
| music          = Songs: Eby Salvin Background Score: Rahul Raj
| cinematography = Kishore Mani
| editing        =Nikhil Venu 
| studio         = Celebs and Red Carpet
| distributor    =Celebs and Red Carpet Release 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}
 Sandhya and Daniel Balaji in the lead roles.  Set against the backdrop of Chennai and Kochi, Paisa Paisa is a multi-narrative realistic thriller showing how money can affect the lives of people.  The film released on June 28, 2013.  The songs were composed by debutant, Eby Salvin while Rahul Raj composed the background score.

==Plot==
The story unfolds the events happening in just four hours of a day in two cities - Kochi and Chennai. Balu (Aju Varghese) who goes to Chennai to attend an interview falls into a trap in the few minutes he goes out of the call centre office to make a phone call. He contacts his friend Kishore (Indrajith Sukumaran|Indrajith), an ad filmmaker and his friend, to help him with some money. Kishore, who is in Kochi, has an equally crucial day. His wife Surya (Mamta Mohandas) is returning to his life as the couple has been living separate for some time. Kishore sets aside his plans to reconcile with his wife to help his friend. However, he fails to gather enough money to help Balu. How money changes the life of these characters forms the rest of the story. 

==Cast==
* Indrajith Sukumaran as Kishore
* Aju Varghese as Balu
* Mamta Mohandas as Surya Sandhya as Kumudam
* Daniel Balaji
* Apoorva Bose as Pooja
* Aparna Nair
* Rajeev 
* Risabava
* Sasi Kalinga as Aalikka
* Jojo
* Anoop Chandran

==Production==
Directed by debutante Prasanth Murali, an erstwhile assistant to V. K. Prakash, the script for the flick was jointly penned by the director and Rajesh Varma.  Model-turned-actress Amruta Patki featured in the promotional song of this film.  The shooting of the film started at Chennai in November 2012. 

==References==
 

==External links==
*  

 
 
 
 
 