Une nuit à l'Assemblée Nationale
{{Infobox film
| name           = Une nuit à lAssemblée Nationale
| image          = 
| caption        = 
| director       = Jean-Pierre Mocky
| producer       = Jean-Pierre Mocky André Djaoui
| writer         = Jean-Pierre Mocky Patrick Rambaud
| based on       = 
| starring       = Jean Poiret Michel Blanc Jacqueline Maillan Roland Blanche Bernadette Lafont Isabelle Mergault Jean Benguigui Darry Cowl Josiane Balasko
| music          = Gabriel Yared
| cinematography = Marcel Combes
| editing        = Jean-Pierre Mocky Marielle Guichard Sophie Moyse Hélène Sicard
| distributor    = Bac Films
| studio         = Cinémax Koala Films SGGC
| released       =  
| runtime        = 88 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $1,670,017 
}}
Une nuit à lAssemblée Nationale (A Night at the National Assembly) is a 1988 French comedy film directed by Jean-Pierre Mocky.

==Plot==
Arbeit Walter, an ecologist practicing naturism, accompanies his deputy mayor Dugland to Paris where he should be awarded the Legion of Honour. Soon after their arrival, Walter discovers that defrauded Dugland for this distinction. Annoyed, he walks the halls of the National Assembly and causing panic in services.

==Cast==
 
* Jean Poiret as Octave Leroy
* Michel Blanc as Walter Arbeit
* Jacqueline Maillan as Henriette Brulard
* Roland Blanche as Marius Agnello
* Bernadette Lafont as Madame Dugland
* Luc Delhumeau as Aimé Dugland
* Michel Francini as Colonel Raoul Dugommier
* Martyne Visciano as Marie-Hermine Leroy
* Isabelle Mergault as Fernande
* Dominique Zardi as Fricasset
* Jean Benguigui as Marcel
* Darry Cowl as Kayser
* Jean Abeillé as Plumet
* Jean-Pierre Clami as Delapine
* François Toumarkine as Tutti-Frutti
* Sophie Moyse as Olympe
* Louis Sautelet as Jean-François
* Marjorie Godin as Madame Boulet
* Josiane Balasko as The journalist
* Vadim Cotlenko as The President of the National Assembly
* Julien Verdier as The great counselor
 

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 