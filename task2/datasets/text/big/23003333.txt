Weasel While You Work
{{multiple issues|
 
 
 
 
 
}}
{{Infobox Hollywood cartoon|
| cartoon_name = Weasel While You Work
| series = Merrie Melodies (Foghorn Leghorn)
| director = Robert McKimson
| story = Mike Maltese
| animator = Warren Batchelder Ted Bonnicksen George Grandpre Tom Ray
| voice_actor = Mel Blanc
| musician = John Seely John Burton, Sr. (uncredited)
| distributor = Warner Bros.
| release_date = September 6, 1958
| color_process = Technicolor
| runtime = 6 mins
| movie_language = English
}}
Weasel While You Work is a 1958 Merrie Melodies animated short starring Foghorn Leghorn, Barnyard Dawg, and the weasel from earlier shorts like Plop Goes the Weasel and Weasel Stop. Unlike many Foghorn shorts, this one takes place during the winter; as such, many of the gags involve snow and ice. The title is a pun on "whistle while you work".

==Summary==
At wintertime, Foghorn grabs Barnyard Dawg from his doghouse, coats him in snow, and puts snowman decorations on him. Dawg emerges from the snowman and says hell "moidah da bum!". His plan is to sharpen Foghorns ice skates so much when Foghorn can fall through the ice. Sure enough, this occurs after Foghorn makes a circle. Foghorns plan to get back at Dawg is to roll a snowball down the hill, which gets bigger as it rolls. Dawg notices just in time and gets out of the way. The now giant snowball hits a curve and flies right back at Foghorn, burying him. After Foghorn emerges, uttering in anger "That dogs like taxes. He just dont know when to stop.", the weasel from "Plop Goes the Weasel" and "Weasel Stop" approaches and gnaws Foghorns leg. Foghorn tells the weasel that hell get him some venison. In the next scene, Foghorn bowls a snowball at Barnyard Dawgs doghouse. When Dawg chases Foghorn, Foghorn places antlers on Dawgs head, then tells the weasel to go after the venison, so the weasel gnaws on Dawgs leg. Dawg notices hes wearing antlers and removes them, telling the weasel hes been tricked and that hell get him some real food. Later, Foghorn goes sledding and hits a tree log that Dawg put down to block his path. While Foghorn is down, Dawg covers him in water, which freezes him. Dawg tells weasel to eat hearty, so the weasel hits the frozen Foghorn with an axe. Foghorn says hes got a splitting headache and falls in half. To get revenge, Foghorn uses a pool cue to poke Dawg out of his doghouse and into a bag, which causes Dawg to flop around like a seal. The weasel grabs Dawg and takes him to be cooked. While shaking pepper on him, Dawg sneezes, which causes an avalanche of snow to fall on him. A furious Dawg tells the weasel that hes a dog and that he should get a chicken instead ("What you want is a chicken! R-A-T. Chicken."). Later, Foghorn is skiing and accidentally hits a tall pole. Foghorn slides down the pole and into a cooking pot, where the weasel is hungrily waiting. Foghorn bolts; Dawg gives the weasel a club, who proceeds to chase Foghorn. He stops when he notices a huge Foghorn ice statue, and proceeds to gnaw on it. The real Foghorn, watching from behind a tree, thinks his troubles are over until at least the Fourth of July. In the final setpiece, Foghorn pulls on what he thinks is Barnyard Dawgs tail from his doghouse, but its revealed to be a lit rocket that shoots Foghorn into the sky. Dawg remarks: "The Fourth of July came a little oily this year! Heh heh heh..." and the cartoon ends.

==Notes==
* This is one of six cartoons scored by using stock music by John Seely of Capitol Records from the Hi-Q (production music)|Hi-Q library because of a musicians strike in 1958. The others are A Bird in a Bonnet, Hook, Line and Stinker, Pre-Hysterical Hare, Gopher Broke, and Hip Hip-Hurry!.

 
 
 
 