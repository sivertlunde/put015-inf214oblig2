Everest (1998 film)
{{Infobox film
| name           = Everest
| image          = 
| caption        =
| director       = Greg MacGillivray, David Breashears
| producer       = Stephen Judson, Alec Lorimore, Greg MacGillivray Tim Cahill, Stephen Judson
| starring       = Beck Weathers, Jamling Tenzing Norgay, Araceli Segarra, Ed Viesturs, Paula Viesturs, Sumiyo Tsuzuki
| narrator        = Liam Neeson Steve Wood, Daniel May, George Harrison  (songs) 
| cinematography = David Breashears
| editing        = Stephen Judson
| distributor    = MacGillivray Freeman Films  (theatrical)   Miramax  (home video) 
| released       = March 6, 1998
| runtime        = 45 minutes
| country        =United States English
| budget         =
| preceded_by    =
| followed_by    =
}}

Everest is a 70mm American documentary film from MacGillivray Freeman Films about the struggles involved in climbing Mount Everest, the highest mountain peak on Earth, located in the Himalayan region of Nepal. It was released to IMAX theaters in March 1998 and became the highest-grossing film made in the IMAX format.

== Production == Sherpa mountaineer Tenzing Norgay.   
 mountain climbers became trapped by a blizzard near the summit. The film includes footage of these events,  as the IMAX team assist Beck Weathers and other survivors.  Producer and co-director Greg MacGillivray later said that while editing the documentary for release, he and Breashears decided to focus more on the tragedy, due to the popularity of Jon Krakauers book about the 1996 disaster, Into Thin Air. MacGillivray reasoned: "Ten million people have read that book, so we had to address the issue. And I think it strengthened the film."   

==Reception== Museum of Science on March 4, 1998 before going on general release in IMAX cinemas across the United States two days later.  According to an article published late that month in the Los Angeles Times, it attracted mainly favorable reviews.    The film
subsequently opened in Australia on March 19 and Switzerland on March 20, with other European premieres, including at the London Trocadero, following during April and May.   

Everest grossed $128 million worldwide during its theatrical run – a figure that remains the highest gross for an IMAX documentary.  With domestic takings of $87,178,599,  it is the second highest-grossing film (documentary or otherwise) to never reach the top ten in the weekly North American box office charts,  and also the second highest-grossing film never to have made the weekly top five. 

== DVD and soundtrack album== Miramax on December 12, 1999. It includes a "Making of" featurette, an extended interview with Beck Weathers, deleted scenes, climber video journals, and a 3D map of Mount Everest.
 Steve Wood Daniel May Tibetan folk style as part of their film score.    The Everest soundtrack album was released by Ark 21 Records,  on March 10, 1998.  The music was performed by the Northwest Sinfonia, with May credited as conductor.   
 All Things This Is Life Itself". Harrison agreed to their use in the film on the understanding that his name would not be used for publicity.  According to author Elliot Huntley, MacGillivray chose Harrisons music for its "spiritual quality" and "his ties to eastern religion". 

== Quotes ==
{{cquote|Narrator:  Just above the high camp, a climber named Beck Weathers had been out in the storm for over 22 hours. He had been left for dead by other climbers, and then, nearly blind, his hands literally frozen solid, Beck stood up, left his pack, and desperately tried to walk.

Weathers: All I knew was that as long as my legs would run and I could stand up, I was gonna move toward that camp, and if I fell down, I was gonna get up. And if I fell down again, I was gonna get up. And I was gonna keep movin till I either hit that camp, or walked off the face of that mountain.
}}

{{cquote|Paula Viesturs:  The difference between me and Ed is, when we go for a five-hour bike ride, I call it a workout … He calls it a warm-up.
}}

== Cast ==
* Liam Neeson as Narrator (voice)
* Beck Weathers as Himself Sherpa Tenzing Norgay, as Himself
* Araceli Segarra as Herself
* Ed Viesturs as Himself
* Paula Viesturs as Herself
* Sumiyo Tsuzuki as Herself

== See also == The Climb The Alps

==References==
 

#   from Rotten Tomatoes
#   from The Numbers

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 