Dil Hi To Hai (1963 film)
{{Infobox film
| name           = Dil Hi To Hai
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = C.L. Rawal P.L. Santoshi	 
| producer       = B.L. Rawal
| writer         = C.L. Rawal
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Raj Kapoor Nutan Roshan
| cinematography = Dharam Chopra
| editing        = Pran Mehra
| studio         = 
| distributor    = Rawal Films
| released       =    
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}

Dil Hi To Hai is a 1963 Indian Bollywood film directed by C.L. Rawal, P.L. Santoshi and produced by B.L. Rawal. It stars Raj Kapoor and Nutan in pivotal roles. 

==Cast==
* Raj Kapoor...Yusuf / Chand / Khan
* Nutan...Jamila Banu
* Agha (actor)|Agha...Bashir
* Pran (actor)|Pran...Sheku / Yusuf
* Nasir Hussain...Khan Bahadur 
* Padmini Priyadarshini...Bahar - Courtesan
* Sabita Chatterjee...Razia - Bashirs Wife

==Music==
It has a popular song "Laga chunri me daag", sung by Manna Dey.

==References==
 

==External links==
* 

 
 
 

 