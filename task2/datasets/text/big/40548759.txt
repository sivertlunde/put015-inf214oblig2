Hi-Ho Mistahey!
 
 
{{Infobox film
| name           = Hi-Ho Mistahey!
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Alanis Obomsawin
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = National Film Board of Canada
| distributor    = National Film Board of Canada
| released       =  
| runtime        = 99 min 43 s
| country        = Canada
| language       = 
| budget         = 
| gross          = 
}}

Hi-Ho Mistahey! is a 2013 National Film Board of Canada feature documentary film by Alanis Obomsawin that profiles Shannens Dream, an activist campaign first launched by Shannen Koostachin, a Cree teenager from Attawapiskat, to lobby for improved educational opportunities for First Nations youth. 

The film premiered on 7 September 2013 at the  , 15 September 2013. 

The films title is Cree for "I love you forever." Obomsawin has said she heard about Koostachins story from childrens rights activist Cindy Blackstock.  Obomsawin had been in the community of Attawapiskat working on this film when the Attawapiskat housing and infrastructure crisis broke. So she put this project aside and completed her film The People of the Kattawapiskak River, before completing Hi-Ho Mistahey!   

The film was a shortlisted nominee for the  , 13 January 2014. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 
 
 