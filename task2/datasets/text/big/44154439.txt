Mahabali (film)
{{Infobox film
| name           = Mahabali
| image          =
| caption        =
| director       = J. Sasikumar
| producer       = EK Thyagarajan
| writer         =
| screenplay     =
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Unnimary
| music          = M. K. Arjunan
| cinematography =
| editing        =
| studio         = Sree Murugalaya Films
| distributor    = Sree Murugalaya Films
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by J. Sasikumar and produced by EK Thyagarajan. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Unnimary in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Prem Nazir
*Jayabharathi
*Adoor Bhasi
*Unnimary
*MG Soman
*TG Ravi

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Pappanamkodu Lakshmanan and . 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aashritha valsalane || Seerkazhi Govindarajan || Pappanamkodu Lakshmanan || 
|-
| 2 || Maveli Naadu Vaaneedum Kaalam || P. Madhuri, Chorus ||  || 
|-
| 3 || Sougandhikangal vidarnnu || Vani Jairam, Krishnachandran || Pappanamkodu Lakshmanan || 
|-
| 4 || Sudarsanayaagam || KP Brahmanandan, Chorus || Pappanamkodu Lakshmanan || 
|-
| 5 || Swarangal paadasarangalil || Vani Jairam, Lathika || Pappanamkodu Lakshmanan || 
|}

==References==
 

==External links==
*  

 
 
 

 