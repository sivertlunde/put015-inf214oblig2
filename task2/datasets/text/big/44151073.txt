Amme Narayana
{{Infobox film
| name           = Amme Narayana
| image          =
| caption        =
| director       = NP Suresh
| producer       = Purushan Alappuzha
| writer         = Purushan Alappuzha Alappuzha Karthikeyan (dialogues)
| screenplay     = Purushan Alappuzha Baby Shalini
| music          = A. T. Ummer
| cinematography = PN Sundaram
| editing        = NP Suresh
| studio         = Sreedevi Films
| distributor    = Sreedevi Films
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Baby Shalini in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Prem Nazir as Vedan / Senadhipathi Chanthu / Vilwamangalam Swamikal
*Srividya as Aadhi Paraashakthi / Lakshmi / Saraswathi / Parvathi / Chottanikkara Amma
*Sukumaran as Suresh Baby Shalini
*Jagannatha Varma
*Mala Aravindan as Kuttan Namboothiri
*Ranipadmini as Naanikkutti (Yakshi)
*Sathyakala as Rajeshwari
*Shanavas Sumithra as Vedante Bhaarya

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Poovachal Khader, Koorkkancheri Sugathan and . 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amme Amme Malankurathiyamme || K. J. Yesudas, Chorus || Poovachal Khader || 
|-
| 2 || Amme Naarayana || K. J. Yesudas || Koorkkancheri Sugathan || 
|-
| 3 || Chandraarkka vaameshwari devi || K. J. Yesudas || Poovachal Khader || 
|-
| 4 || Madhyannaarkka Sahasrakodi || KP Brahmanandan ||  || 
|-
| 5 || Manchaadikkutti Malavedakkutti || Ambili, KP Brahmanandan || Poovachal Khader || 
|-
| 6 || Nithyaanandakari || K. J. Yesudas ||  || 
|-
| 7 || Sindooraaruna Vigrahaam || KP Brahmanandan ||  || 
|-
| 8 || Sreemad Chandana || KP Brahmanandan ||  || 
|}

==References==
 

==External links==
*  

 
 
 

 