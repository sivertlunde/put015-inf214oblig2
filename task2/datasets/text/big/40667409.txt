What If... (2012 film)
{{Infobox film
| name           = What If...
| image          = 
| image size     =
| caption        =
| director       = Christoforos Papakaliatis
| producer       =
| writer         = 
| narrator       =
| starring       = Christoforos Papakaliatis   Marina Kalogirou
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 111 minutes
| country        = Greece Greek
| budget         =
| preceded by    =
| followed by    =
}}
What If... is a 2012 Greek drama film directed by Christoforos Papakaliatis.  The film won the Best Sound award in Hellenic Film Academy Awards.   

==Cast==
*Christoforos Papakaliatis - Dimitris
*Marina Kalogirou - Christina
*Maro Kontou - Elenitsa Kokovikou
*Giorgos Konstadinou - Antonakis Kokovikos

==Awards==
{| class="wikitable"
|+ List of awards and nominations 
! Award !! Category !! Recipients and nominees !! Result
|- Hellenic Film 2013 Hellenic Best Sound||Marinos Athanassopoulos, Aris Louziotis, Alexandros Sidiropoulos, Costas Varybopiotis || 
|}

==References==
 

==External links==
*  

 
 


 