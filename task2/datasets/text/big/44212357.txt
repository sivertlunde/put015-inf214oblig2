Doctor Crippen (1942 film)
{{Infobox film
| name =  Doctor Crippen 
| image =
| image_size =
| caption =
| director = Erich Engels
| producer =  Eduard Kubat   Alf Teichs
| writer =  Walter Ebert    Georg C. Klaren   Kurt E. Walter    Erich Engels
| narrator =
| starring = Rudolf Fernau   René Deltgen   Anja Elkoff   Gertrud Meyen
| music = Bernhard Eichhorn      
| cinematography = E.W. Fiedler   
| editing =  Erich Palme      
| studio = Terra Film
| distributor = Deutsche Filmvertriebs 
| released = 6 November 1942   
| runtime = 87 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Doctor Crippen or Doctor Crippen on Board (German:Dr. Crippen an Bord) is a 1942 German crime film directed by Erich Engels and starring  Rudolf Fernau, René Deltgen and Anja Elkoff.  

==Cast==
* Rudolf Fernau as Hawley Harvey Crippen|Dr. Crippen
* René Deltgen as Düwell  
* Anja Elkoff as Cora Crippen  
* Gertrud Meyen as Lucie Talbot  
* Rolf Weih as Arnoldi  
* Max Gülstorff as Kendall  
* Paul Dahlke as Verteidiger  
* O.E. Hasse as Prof. Morrison 
* Heinz Schorlemmer as Petersson, Steward auf der "Montrose"  
* Walter Lieck as Michels  
* Robert Bürkner as Nicolin  
* Ernst Waldow as Dr.Hidden  
* Wilhelm Bendow as Harras  
* Günther Hadank as Staatsanwalt  
* Ernst Leudesdorff as Gerichtsvorsitzender   Knut Hartwig as 1. Offizier   Adolf Fischer as Funker Menton  
* Olga Engl as Eine Nachbarin Dr. Crippens  
* Hella Thornegg as Bertha, eine Nachbarin Dr. Crippens  
* Louis Brody as Pedro  
* Meta Weber as Frau Martinetti 
* Albert Johannes as Herr Martinetti  
* Josef Keim as Gerichtsarzt  
* Elisabeth Scherer as Rosy Dupont, Hausmädchen bei Dr. Crippen  
* Käthe Jöken-König as Frau von Otti  
* Ernst G. Schiffner as Ein Gerichtsreporter  
* Peter Busse as Der Mann Otti aus dem Puplikum beim Verwandlungskünstler  
* Leo Peukert as Passagier  
* Edith Wolff as Morrisons Assistentin 
* Albert Lippert as  Ein Gast bei Dr. Crippen 
* Arthur Reinhardt as Wachtmeister bei der Gerichtsverhandlung

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 