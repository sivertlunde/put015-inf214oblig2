The Gate of Heavenly Peace
 

{{Infobox Film
| name           = The Gate of Heavenly Peace
| image          = Gate_title.jpg
| image_size     =
| caption        = 
| director       = Richard Gordon  Carma Hinton
| producer       =
| writer         = Geremie Barmé  John Crowley 
| narrator       = Deborah Amos Wang Dan  Wuer Kaixi
| music          = Mark Pevsner
| cinematography = 
| editing        =
| distributor    = 
| released       = 1995
| runtime        = 180 min
| country        =  
| language       = English  Mandarin
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Gate of Heavenly Peace ( ) is a 1995 documentary film, produced by Richard Gordon and Carma Hinton, about the Tiananmen Square protests of 1989.

==Synopsis==
The Gate of Heavenly Peace is a three-hour documentary film about the 1989 protests at Tiananmen Square, which culminated in the violent government crackdown on June 4. The film uses archival footage and contemporary interviews with a wide range of Chinese citizens, including workers, students, intellectuals, and government officials, to revisit the events of “Beijing Spring.” From the beginning of the protests in mid-April to the night of June 3–4, the film provides a “meticulous day-by-day chronicle of the six-week period… This unglamorous but absorbing film interweaves videotaped scenes of the demonstrations and conversations with leaders and participants with an explanatory narration into an account that is as clear-headed as it is thorough and well-organized.” 

Among those interviewed are Liu Xiaobo, Wang Dan, Wuer Kaixi, Han Dongfang, Ding Zilin, Chai Ling, Dai Qing, Feng Congde, and Hou Dejian.

In addition, The Gate of Heavenly Peace examines the deeper history behind the demonstrations, providing historical and cultural context for the famous images that the Western media flashed around the world. The film explores the symbolic importance of Tiananmen Square and also looks at earlier political movements in China from the May Fourth Movement of 1919 to the Cultural Revolution of 1966-76 to the Tiananmen Incident of 1976. In so doing, the film considers the ways in which the political habits and attitudes that came to inform public life in China over the past century also shaped the events of 1989.

Pauline Chen writes: “The Gate of Heavenly Peace illuminates how images of these movements, filtered and refracted through propaganda, emotion, and imperfect memory, provided inspiration and models for the participants, both students and government, in the 1989 events. The students thought they were emulating the May 4 leaders, forgetting that those students returned to school and worked for more gradual social change after successfully drawing attention to Chinas political and social problems. To some Communist Party members, however, the 1989 mass student demonstrations may have chillingly recalled the chaos and terror of the Cultural Revolution. Both the Cultural Revolution and the 1976 Tiananmen Incident attest to how extremists have used popular uprisings as excuses to get rid of their moderate rivals; the reactionary Deng Xiaoping, who favored greater economic freedom, was blamed by Mao for the 1976 Tiananmen Incident and forced from his position. Unfortunately, todays hard-liners have also learned the lesson from these events. The moderate reformers Zhao Ziyang and Yan Mingfu, Chinas best hope for democratic reform, were ousted from power following the June 4 crisis.” 

==Production==
The Gate of Heavenly Peace was produced and directed by Richard Gordon and Carma Hinton, who have collaboratively made numerous films about China. Their production company, the Long Bow Group, is a small non-profit company based in the Boston area. According to Gordon, "One of the reasons we wanted to make   was to give more depth to   movement and not just show the final, violent conclusion, which is where people tend to focus.” “Tiananmen Square Story on Film,” Paul Desruisseaux, The Chronicle of Higher Education, Oct. 27, 1995  Despite the hundreds of hours of Western media coverage, Hinton felt that "everything was reduced to slogans and hand clapping. I wanted to hear more Chinese voices, because I knew they would show a range of opinion. We felt a film that did that could help open up peoples minds about these events. Its not all black and white."  In addition to Hinton and Gordon, China scholars such as Geremie Barmé, Gail Hershatter, and Jeffrey Wasserstrom helped to provide context and perspective. Over three hundred hours of archival footage were collected, and the film took over five years to complete. Says Hinton, "It was not an easy decision to get into something like this. I knew that any documentary – to say nothing of something of this scale, between the funding and the research and the actual making of the film – would probably take years of our lives. Once we decided to make the film, it did take nearly six years." 

==Controversy==
The Gate of Heavenly Peace sparked controversy before it had even been completed. The film was part of a growing debate over the history of 1989; according to an article that appeared in The New York Times on April 30, 1995, “a central question for many in the student movement, and for some historians, is whether moderation gave way to extremism during those six weeks and whether the more radical student leaders spurned opportunities to declare victory by ending the demonstrations and preserving, perhaps, the reformist trend that was still a prominent feature of the Chinese leadership." 

The New York Times article also quoted from a controversial interview that was used in The Gate of Heavenly Peace. On May 28, 1989, just days before the massacre, American journalist Philip Cunningham interviewed one of the student movement’s most prominent leaders, Chai Ling. In this interview, Chai indicated that “the hidden strategy of the leadership group she dominated was to provoke the Government to violence against the unarmed students. With statements   like ‘What we are actually hoping for is bloodshed’ and ‘Only when the square is awash with blood will the people of China open their eyes,’ Ms. Chai denounced those students who sought to bring an end to the occupation of the square.” (op. cit.) The May 28, 1989 interview was undertaken at Chai Lings request. She then asked Cunningham to release it internationally as her political statement on the student movement. The Gate of Heavenly Peace makes extensive use of this interview (necessitated in part by Chai Ling’s repeated refusal to be interviewed for the film).  
	
The filmmakers suggest that “the hard-liners within the government marginalized moderates among the protesters (including students, workers and intellectuals), while the actions of radical protesters undermined moderates in the government. Moderate voices were gradually cowed and then silenced by extremism and emotionalism on both sides.”   In following the fate of these “moderate voices,” the film raises questions about some of the decisions that were made by a few of the student leaders. For this, the film was angrily condemned by many in the Chinese exile community, including Chai Ling herself. For example, in April 1995 – well before the film had even been completed (it premiered in October 1995) – Chai wrote, "Certain individuals, for the sake of gaining approval of the   authorities, have racked their brains for ways and means to come up with policies for them. And there is another person with a pro-Communist history   who has been hawking   documentary film for crude commercial gain by taking things out of context and trying to show up something new, unreasonably turning history on its head and calling black white." 

The notion of extremism on both sides (i.e., hardliners among the government and students) was not a view unique to The Gate of Heavenly Peace. In a review published in The American Historical Review in October 1996, Michael Sheng writes, “The filmmakers... interpret the   movement with passion and intellectual vigor," noting that "angry voices from the Chinese exile community have denounced the film and its directors as loudly as the Beijing government. The filmmakers, however, are not the first or the only ones to hold the argument that the polarizing approach of both sides made the tragedy inevitable. Many scholars, observing the divisions within the student leadership as well as in the government, have formed opinions similar to that of the filmmakers…. One of   Tsou’s findings   is that the kind of two-sided confrontation in the spring of 1989 is nothing new; it is deeply rooted in the traditions of Chinese political culture. Tsou’s arguments are convincing in light of Chai Ling’s behavior. Her ‘binary’ approach and intolerant attitude of ‘if you are not with me, you are against me’ is well documented in the film... And it becomes clearer in the debate over the film, when she attacked the filmmakers as co-conspirators of the Beijing regime.” 

Such an attitude – the "binary approach and intolerant attitude" – persisted years after the film had been completed. In 2007, Chai Ling, her husband Robert Maginn (a former partner at Bain, the CEO of Jenzabar, and chairman of the Massachusetts Republican Party), and their company, Jenzabar, sued the filmmakers over their website (www.tsquare.tv), accusing them of being "Motivated by ill-will, their sympathy for officials in the Communist government of China, and a desire to discredit Chai...."  In a New Yorker article about Chai Ling and the lawsuit, Evan Osnos noted, "For the record, to anyone with knowledge of the film, the notion that it is sympathetic to the Chinese government is laughable..."  Public Citizen – a consumer advocacy group whose litigating arm focuses on cases involving consumer rights, separation of powers, open government, and the First Amendment, among others  – offered to represent the filmmakers pro bono;  the Berkman Center for Internet & Society at Harvard Law School wrote an Amicus Curiae brief.  Although the lawsuit was dismissed by a Massachusetts court in December 2010, Jenzabar chose to appeal this decision. It was upheld by the Massachusetts Appeals Court in October 2012, and the Supreme Judicial Court of Massachusetts declined further review. A motion for attorney fees by the Long Bow Group, on the other hand, was granted, with the judge stating that Jenzabar had "subjected Long Bow to protracted and costly litigation not to protect the good will of its trademark from misappropriation, but to suppress criticism of Jenzabars principals and its corporate practices."  

During the course of the lawsuit, Chai Ling accused the filmmakers of aligning themselves not only with the Chinese government, but with an even greater force, denouncing them as "tools of Satan."  There were many others, however, who signed a letter condemning the lawsuit. Among those who gave their support to the filmmakers were prominent dissidents, some of whom had been jailed by the Chinese government for their role in the Tiananmen demonstrations. Additional signatories included Chinese students who had participated in the 1989 movement, noted artist Ai Weiwei, and scholars and professors from approximately two hundred fifty universities and colleges around the world.  Articles in the Boston Globe ("A Victory for Free Speech"),  The New Yorker (The American Dream: The Lawsuit),  and The Guardian ("From democracy activist to censor?")  criticized the lawsuit, and Boston’s PBS station Frontline stated that the lawsuit “poses first amendment issues and is a potential threat to all newsgathering, reportorial and academic sites.” 

While the lawsuit focused on the website, some student leaders continued to criticize the film. Feng Congde wrote an Open Letter in May 2009, referring to what he said was "false reporting and editing" with regard to Chai Ling in The Gate of Heavenly Peace. The filmmakers posted both Fengs letter and a lengthy response on their website in July 2009; in this response, they write, "The alleged falsehoods that are described in the Open Letter simply do not exist in the film." They go on to provide a detailed examination of Fengs charges, citing specific examples from the film. 

==Chinese Government Opposition to the Film==
The Gate of Heavenly Peace was completed in the fall of 1995, and premiered that October at the New York Film Festival. This time, controversy would be generated not by the exiled dissident community, but by the Chinese government. According to Newsweek (Oct. 9, 1995), the Chinese Consulate in New York was “not happy” to hear that The Gate of Heavenly Peace would be part of the festival.  Consular officials said that “the film was an insult to China, and unless it was removed from the festival, they would be forced to withdraw   Shanghai Triad.” 

Other film festivals were also subjected to similar pressures; for example, in an official letter to the director of Filmfest DC, the Press Counsel of the Embassy of the People’s Republic of China in Washington D.C. wrote, “As is well known, a very small number of people engaged themselves in anti-government violence in Beijing in June 1989 but failed. The film the Gate of Heavenly Peace sings praise of these people in total disregard of the facts. If this film is shown during the festival, it will mislead the audience and hurt the feelings of the 1.2 billion Chinese people. … Therefore, it is necessary and appropriate to withdraw this film from the festival.” The director of Filmfest DC did not comply. 

Other festival directors were less resolute in the face of pressure to withdraw the film, as discussed in "Technical Problems...à la Chinoisie" in DOX Documentary Film Magazine. 

==Awards==
The Gate of Heavenly Peace received a  , Golden Spire, San Francisco International Film Festival, and Best of Festival, New England Film & Video Festival.

==Reviews==
The Gate of Heavenly Peace has been widely and positively reviewed in the press. Some examples are below; more excerpts are available on  .

David Ansen, Newsweek – “deep, powerful and rivetingly complex” 

Charles Taylor, Boston Phoenix – “The Gate of Heavenly Peace, Richard Gordon and Carma Hiltons magnificent and devastating three-hour documentary on the 1989 Chinese democracy movement, which culminated with the tragedy at Tiananmen Square, has the richness, clarity, and complexity that only the best documentaries afford… It is certainly one of the great documentaries of the past 20 years.” 

Michael Blowen, "TV Week" magazine, The Boston Globe – "In The Gate of Heavenly Peace (the literal translation of the name Tiananmen), the causes, effects and fallout from the six-week protest that led up to the Chinese governments crackdown on dissidents are detailed with intelligence, grace and toughness. Filmmakers Carma Hinton and Richard Gordon have transformed news into history, and history into art." 

Jonathan Rosenbaum, The Chicago Reader – “An immensely valuable three-hour documentary… This film is likely to revise the very terms of your understanding of the pivotal events it considers.” 

==References==
 

==External links==
* 
* 
* 
*  at the Internet Movie Database

 
 
 
 
 
 
 
 
 