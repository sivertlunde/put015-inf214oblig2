Naa Alludu
{{Infobox Film | 
  name     = Naa Alludu |
  image          =|
    writer         = Vara Mullapudi | Ali |
  director       = Vara Mullapudi |
  producer       = A. Bharati (producer)|A. Bharati |
  distributor    = |
  editing        = Kotagiri Venkateswara Rao |
  released       = January 14, 2005 |
  runtime        = 161 minutes |
  cinematography = |
  country        = India | Telugu |
  music          = Devi Sri Prasad |
  awards         = |
  budget         = |
}} Telugu comedy film written and directed by Vara Mullapudi.  The film stars Jr. NTR, Shriya Saran, Genelia DSouza, and Ramya Krishnan.  Despite the huge cast, the film was the biggest flop of 2005 in Cinema of Andhra Pradesh|Tollywood. It was Also Dubbed in Hindi Under the title "Main Hoon Gambler".

==Plot==

Karthik (NTR Jr.) challenges a level headed industrialist Bhanumati (Ramya Krishnan) that he would marry either of her daughters, to take revenge on her for insulting him in his interview to get a job in her company. Meghana (Shriya Saran) falls for the young man, just for the one accidental kiss he gives her. The younger one Gagana (Genelia DSouza) too follows suit, and now Karthik has a grip over both of Bhanumati’s daughter. Competing with him is Bhanumati’s nephew (Rajiv Kanakala), whose father (Charan Raj) too encourages him to chase his cousins, and marry either of them to inherit their property.  It is revealed in the flashback, that Bhanumati leaves her husband Suman, and Karthik is her nephew.  How the responsible son-in-law sets his mom-in-law right and eliminates the villains from the game, is what the story is about.

== Cast ==
* NTR Jr. ... Karthik/Murugan swami
* Shriya Saran ... Meghana
* Ramya Krishnan ... Bhanumati
* Genelia DSouza ... Gagana Ali

==Soundtrack==
{{Track listing
| extra_column = Singer(s)
| title1 = Andhala Bomaro | extra1 = Venu, Sumangali | length1 = 04:23
| title2 = Are Sayara | extra2 = Karthik (singer)|Karthik, Malathi Lakshman | length2 = 06:01
| title3 = Kandhi Chenu Kada | extra3 = Jassie Gift, Kalpana | length3 = 06:14
| title4 = En Peru Murugan | extra4 = Shankar Mahadevan, Grace Karunas | length4 = 04:55
| title5 = Nadumu Chooste | extra5 = Tippu (singer)|Tippu, K. S. Chithra | length5 = 04:58
| title6 = Pattuko Pattuko | extra6 = Ranjith (singer)|Ranjith, Padmavathi | length6 = 04:17
| title7 = Pilla Choodu | extra7 = Pushpavanam Kuppusamy, Prasanna Rao, Kalpana | length7 = 04:02
}}

==Trivia==
Dubbed into Tamil as Madurai Mappilai

==External links==
* 

 
 
 
 


 