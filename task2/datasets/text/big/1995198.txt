Izo
 
{{Infobox film
| name = Izo
| image =  Izoposter.jpg
| caption =
| director = Takashi Miike
| writer =Shigenori Takechi
| starring = Kazuya Nakayama Takeshi Kitano Bob Sapp
| producer =Taizô Fukumaki Fujio Matsushima
| distributor =
| released = 21 August 2004 (Japan)
| runtime =128 min.
| language = Japanese English
| budget =
| preceded_by =
| followed_by =
}}

IZO is a 2004 Japanese film, directed by Takashi Miike. The main character of the film is Izo Okada (1832–1865), the historical samurai and assassin in 19th century Japan who was tortured and executed by beheading in Tosa Domain|Tosa.

Izo appeared previously in Hideo Goshas Hitokiri (1969), then played by Shintaro Katsu. However, Miikes portrayal of the character (or rather his spirit) transcends reality (and time and space) and is more of a surrealist exposé of Izos exceedingly bloody yet philosophical encounters in an afterlife heavy on symbolism, occasionally interrupted by stock footage of World War II accompanied by acid-folk singer Kazuki Tomokawa on guitar. Kazuya Nakayama plays Izo, and the countless characters he encounters on his journey include for instance Takeshi Kitano and Bob Sapp.

==Cast ==
* Kazuya Nakayama - Okada Izo
* Kaori Momoi	
* Ryuhei Matsuda
* Ryôsuke Miki		
* Yuya Uchida		
* Masumi Okada
* Hiroki Matsukata
* Hiroshi Katsuno Masato
* Bob Sapp
* Takeshi Caesar
* Takeshi Kitano
* Daijiro Harada
* Renji Ishibashi
* Mickey Curtis
* Kazuki Tomokawa
* Hiroyuki Nagato
* Susumu Terajima
* Ken Ogata

==Awards==
Izo was awarded the best Special Effects prize at the Catalonian International Film Festival.   

==External links==
* 
*  
Reviews
* 
* 
* 
* 
* 
* 
See also
* 

==References==
 

 

 
 
 
 
 
 


 