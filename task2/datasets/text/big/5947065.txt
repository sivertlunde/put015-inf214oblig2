The Whispering Shadow
{{Infobox film
| name           = The Whispering Shadow
| image          = The Whispering Shadow.png
| image size     =
| caption        =
| director       = Colbert Clark Albert Herman
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Mascot Pictures 1933
| runtime        = 12 chapters (225 min)
| country        =   English
| budget         =
| preceded by    =
| followed by    =
}}
The Whispering Shadow (1933 in film|1933) is a Serial film, starring Béla Lugosi in his first of five serial roles. Lugosi received $10,000, the highest known salary of his career, for this film. (He also received the same amount for The Mystery of the Marie Celeste aka The Phantom Ship in 1935).  The serial was filmed in only 12 days.  It was the last film for actor Karl Dane.

==Plot==
The Shadow in The Whispering Shadow is the underworld mastermind.  He has invented a device that allows him to kill by radio control. He, along with several other persons, seeks the Czars jewels. The series is notable for the constant false clues and decoy actions that make nearly everybody a suspect. Despite being the voice of The Shadow, Lugosi is a red herring. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut 
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 341–344
 | chapter = 14. The Villains "All Bad, All Mad"
 }} 

==Production==
The cinematography mimicked that of Karl Freund in Universals Dracula (1931 English-language film)|Dracula - for example, using close ups of the actors eyes - in order to take advantage of Bela Lugosis fame as the star of that film.   The shadow of The Shadow is not real; It was drawn in later by animators.   Harmon and Glut comment on that "If Street & Smith, owners of the original The Shadow|  Shadow of magazine and radio fame, had found out about the owner of the whisper, they might have sued." 
The serial was later edited down to a feature length edition (as was common in those days).

==Chapter titles==
# Master Magician
# The Collapsing Room
# The All-seeing Eye
# The Shadow Strikes
# Wanted for Murder
# The Man Who Was Czar
# The Double Room
# The Red Circle
# The Fatal Secret
# The Death Warrant
# The Trap
# King of the World
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 209
 | chapter = Filmography
 }} 

==See also==
* Béla Lugosi filmography
* List of film serials
* List of film serials by studio

==References==
 

==External links==
*  
*  

 
{{Succession box Mascot Serial Serial
| before=The Devil Horse (1932)
| years=The Whispering Shadow (1933) The Three Musketeers (1933)}}
 

 

 
 
 
 
 
 
 
 
 


 