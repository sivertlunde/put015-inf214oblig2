A Farewell to Arms (1932 film)
{{Infobox film
| name           = A Farewell to Arms
| image          = Poster - A Farewell to Arms (1932) 01.jpg
| caption        = Theatrical release poster
| director       = Frank Borzage
| producer       = {{Plainlist|
* Edward A. Blatt
* Benjamin Glazer
}}
| screenplay     = {{Plainlist|
* Benjamin Glazer
* Oliver H.P. Garrett
}}
| based on       =  
| starring       = {{Plainlist|
* Helen Hayes
* Gary Cooper
* Adolphe Menjou
}}
| music          = Milan Roder
| cinematography = Charles Lang
| editing        = {{Plainlist|
* Otho Lovering
* George Nichols, Jr.
}}
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $900,000 FILM COSTS HIT BOTH EXTREMES: POVERTY ROW SPENDS LESS, BIG STUDIOS MORE MILLION-DOLLAR FEATURES "SHOOT THE WORKS" INEXPENSIVE "ARTY" HIT DUE TO MAKE APPEARANCE
Schallert, Edwin. Los Angeles Times (1923-Current File)   16 Oct 1932: B13.  
| gross          = 
}} romance drama public domain (in the USA) due to the failure of the last claimant, United Artists, to renew its copyright registration in the 28th year after publication.  The original Broadway play starred Glenn Anders and Elissa Landi.  

==Plot== Italian front during World War I, Frederic Henry (Gary Cooper), an American serving as an ambulance driver in the Italian Army, delivers some wounded soldiers to a hospital. There he meets his friend, Italian Major Rinaldi (Adolphe Menjou), a doctor. They go out carousing, but are interrupted by a bombing raid. Frederic and English Red Cross nurse Catherine Barkley (Helen Hayes) take shelter in the same place. The somewhat drunk Frederic makes a poor first impression.

Rinaldi persuades Frederic to go on a double romantic date with him and two nurses, Catherine and her friend Helen Ferguson (Mary Philips). However, Rinaldi becomes annoyed when Frederic prefers Catherine, the woman the major had chosen for himself. Away by themselves, Frederic learns that she was engaged to a soldier who was killed in battle. In the darkness, he romantically seduces her, over her half-hearted resistance, and is surprised to discover she is a virgin.

Their romantic relationship (forbidden by army regulation) is discovered. At Rinaldis suggestion, Catherine is transferred to Milan. When Frederick is wounded by artillery, he finds himself in the hospital where Catherine now works. They continue their affair until he is sent back to the war. Now pregnant, Catherine runs away to Switzerland, but her many letters to her beloved sweetheart/lover are intercepted by Rinaldi, who feels he needs to rescue his friend from the romantic entanglement. Meanwhile, Frederics letters to her are sent to the hospital which she has abandoned.

After a time, Frederic cannot stand being away from Catherine any longer. He deserts his post and heads out in search of her. Returning first to the hospital in Milan, he attempts to convince the reluctant Ferguson to reveal Catherines whereabouts to him. Displaying animosity toward Frederic, all she reveals finally is that Catherine has left and is pregnant with Frederics child. Rinaldi visits him at the hotel where he is hiding, and, upon hearing of Catherines pregnancy, out of remorse for having interfered with their correspondence, tells Frederic where she is living. He rows across a lake to her. Meanwhile, Catherine is delighted when she is told she has finally received some mail, but faints when she is given all of her romantic love letters, marked "Return to Sender". She is taken to the hospital, where her child is delivered stillborn. She herself is in grave danger. Frederic arrives, and just as an armistice between Italy and Austria-Hungary is announced, Catherine tragically dies, with him at her side.

==Cast==
* Helen Hayes as Catherine Barkley
* Gary Cooper as Lieutenant Frederic Henry
* Adolphe Menjou as Major Rinaldi
* Mary Philips as Helen Ferguson
* Jack La Rue as Priest
* Blanche Friderici as Head Nurse
* Mary Forbes as Miss Van Campen
* Gilbert Emery as British Major 

==Critical reception==
  and Helen Hayes]]
In his review in The New York Times, Mordaunt Hall wrote, "There is too much sentiment and not enough strength in the pictorial conception of Ernest Hemingways novel ... the film account skips too quickly from one episode to another and the hardships and other experiences of Lieutenant Henry are passed over too abruptly, being suggested rather than told ... Gary Cooper gives an earnest and splendid portrayal   Helen Hayes is admirable as Catherine ... another clever characterization is contributed by Adolphe Menjou ... it is unfortunate that these three players, serving the picture so well, do not have the opportunity to figure in more really dramatic interludes."   

Dan Callahan of Slant Magazine notes, "Hemingway ... was grandly contemptuous of Frank Borzages version of A Farewell to Arms ... but time has been kind to the film. It launders out the writers ... pessimism and replaces it with a testament to the eternal love between a couple."   

Time Out London calls it "not only the best film version of a Hemingway novel, but also one of the most thrilling visions of the power of sexual love that even Borzage ever made ... no other director created images like these, using light and movement like brushstrokes, integrating naturalism and a daring expressionism in the same shot. This is romantic melodrama raised to its highest degree."   

Channel 4 describes it as "an excellent adaptation ... the two leads are ideal and irresistible here, particularly a reliably sensitive Cooper, who milks his everyman appeal to great effect."

==Awards and nominations==
The film won two Academy Awards and was nominated for another two:   
* Academy Award for Best Picture (nominee) 
* Academy Award for Best Art Direction (nominee)
* Academy Award for Best Cinematography (winner)
* Academy Award for Sound - Franklin Hansen (winner)

==See also==
 
* List of films in the public domain
* The House That Shadows Built (1931 promotional film by Paramount with excerpt of film showing Eleanor Boardman, later replaced by Hayes)

==References==
 

==External links==
 
*  
*  
*  
*  

===Streaming audio=== Studio One, February 17, 1948 NBC University Theater, August 6, 1948

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 