The Details (film)
{{Infobox film
| name           = The Details
| image          = 
| caption        = 
| director       = Jacob Aaron Estes
| writer         = Jacob Aaron Estes
| producer     = Mark Gordon Bryan Zuriff Hagai Shaham
| starring       = Tobey Maguire Elizabeth Banks Dennis Haysbert Ray Liotta Kerry Washington Laura Linney
| music          = tomandandy
| cinematography = Sharone Meir
| editing        = Madeleine Gavin
| studio         = LD Entertainment
| distributor    = The Weinstein Company
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Details is a 2011 film directed by Jacob Aaron Estes.  When a family of raccoons discover worms living underneath the sod in Jeff and Nealys backyard, this pest problem begins a darkly comic and wild chain reaction of domestic tension, infidelity and murder. It has a rating of 45% on Rotten Tomatoes based on 33 reviews. 

==Cast==
* Tobey Maguire as Jeff Lang
* Elizabeth Banks as Nealy Lang
* Kerry Washington as Rebecca Mazzoni
* Ray Liotta as Peter Mazzoni
* Laura Linney as Lila
* Dennis Haysbert as Lincoln

==Plot== Maguire and Elizabeth Banks|Banks) are a young Seattle couple. Jeff is a doctor and Nealy owns a small shop. They have a two-year-old son and are looking to lay down some new grass in their backyard. However, since there are worms in the grass as it is laid down, a group of raccoons manage to destroy it on a regular basis. This prompts Jeff to go to great lengths to rid their yard of the raccoons. He decides to order poison online and he applies it to a can of tuna fish. Not long later, Jeff and Nealys next door neighbor Lila (Laura Linney|Linney) comes to Jeff and reports that her cat, Matthew, is missing. He knows nothing of the disappearance, but hopes for the best.

After a huge argument between Jeff and Nealy, over how obsessive Jeff has been to handle the raccoon situation, Jeff meets up with an old childhood friend, Rebecca (Kerry Washington|Washington). They go back to her place and sit in a collectible car owned by her husband, Peter (Ray Liotta|Liotta). They smoke some marijuana and joke about cheating on their spouses. This leads to them having sex in the garage.

Not long later, while Jeff is on his way to work, Lila tries desperately to get his attention. He manages to avoid her, but Lila remains persistent and follows Jeff to his office. However, once Jeff arrives, he sees Peter in his office, looking desperate. He silently confronts Jeff about his extra-marital affair with Rebecca and admits that she told him everything. He doesnt say much, but as he leaves, Jeff notices Lila standing near the door, who also leaves. This leads Jeff to believe that she had heard the whole conversation.
 fellates Jeff ejaculate and tries to throw Lila off of him. Lila appears to be so turned on by the intercourse, that she rejects him trying to pull away, and it is too late.

Jeff comes home and Nealy tells him that Peter had stopped by earlier that day. In a quick desperation to get all of the previous events behind him, Jeff meets up with Peter at his restaurant to inquire about why Peter had stopped by. Peter says that he is willing to give Jeff a choice: call Nealy to admit his infidelity, or give him $100,000 for his silence. Jeff decides to think about it. He then realizes that he only has about $27,000 in his bank account and doesnt have enough to bribe Peter, but yet, he still doesnt want Nealy to know what had happened.
 equity on his house. Peter takes the money and throws it into the river. Peter chastises Jeff for the decisions that he has made. Peter says that instead of being in his current position, Jeff couldve called Nealy and confessed the truth. Peter backs up his judgment with a story that he had once almost killed a child with his car in a drunk driving accident. He had the choice of getting off by hiring a big-time lawyer that he could have afforded, by defense that he was just under the legal limit. But instead, he decided to plead guilty, give the childs family some money in damages and serve one month in prison. He felt good by making that decision, because it was the right thing to do. Peter also says that if Jeff keeps making the same decisions he made before, then his life will be completely ruined. This prompts Jeff to start making more moral decisions in his life.

Jeffs friend and basketball pal, Lincoln (Dennis Haysbert|Haysbert) is slowing dying of kidney failure. After doing some research, Jeff decides to offer Lincoln one of his kidneys due to them being the same blood type. Lincoln gracefully accepts and a kidney transplant occurs. As Jeff is lying in the hospital bed, in a weak state, Lila appears and reveals that she is pregnant with Jeffs child. A dazed Jeff doesnt react immediately, but meets with Lila sometime later in a coffee shop and strongly says that he will not be a father to the baby. Lila says she wont expect any close relationship or support from Jeff, but wants Jeff to be around for the babys sake. Jeff is not convinced and gets in a more vulnerable state.

Jeff reveals his previous predicaments to Lincoln. He also talks about a dream where a couple of football players run toward Lila. But instead of throwing a football at her, he throws an arrow. Jeff is not sure of what the significance of that dream was. Lincoln decides for himself that since Jeff saved his life by giving him his kidney, on top of helping him getting a job coaching a girls basketball team, that  he should return the favor by helping Jeff get rid of his problems. He does so by buying an archery set and hunting down Lila, while she is on a walk. Lincoln appears with the bow ready to fire and Lila pleads for her life. He doesnt fire right away and appears to be coming to his senses. Lila tries to get away, but Lincoln decides to shoot her. He fires the arrow and it impales Lila, killing her.

Nealy calls Jeff and says that she had heard about Lilas death and that the police are interviewing everyone in their block. Jeff is shocked and starts to feel guilty. Lincolns church decides to offer a ceremony, honoring Jeff in his saving Lincolns life with the kidney transplant. Everyone in the church is in high spirits, except Jeff. He shares a silent exchange with Lincoln, knowing that it was he who killed Lila, since he killed her with an arrow, like in Jeffs dream.

On the way home after the service, Jeff decides to pull the car over and tell Nealy everything. Before he does, Jeff sees a raccoon crossing the road and puts the car in gear. He kills the raccoon and Nealy is in a shocked state. Jeff then confesses everything that has happened: his affairs with Rebecca and Lila, Lilas pregnancy with his child, to being responsible for Lilas death. Jeff also throws in the fact that he cheated his way through medical school. Nealy starts to laugh, which confuses Jeff. Nealy reveals that for the past six months, she was having an affair with a common customer at her shop. This doesnt surprise Jeff too much, since he feels that he deserves whatever Nealy throws back his way. But both of them decide to put everything behind them and move forward with a fresh start.

Sometime later, Jeff has the grass rolled up and hauled away. He replaces the grass with rows of flowers, which keeps the raccoons at bay. Nealy is shown to be pregnant with their second child.

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 