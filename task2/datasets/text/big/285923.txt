Love & Pop
{{Infobox film
| name           = Love & Pop
| image          =
| caption        =
| director       = Hideaki Anno
| producer       = Toshimichi Otsuki
| writer         = Hideaki Anno   Akio Satsukawa
| starring       = Asumi Miwa   Kirari   Hirono Kudō   Yukie Nakama
| music          =
| cinematography =
| editing        =
| distributor    = Toei Company
| released       =
| runtime        = 110 minutes
| country        = Japanese
| budget         =
| preceded by    =
| followed by    =
}}

  is director   aspect, distorts (with effects such as a fisheye lens), confuses, and makes use of overlays stacked in layers to convey the characters emotions.
 English DVD-Video|DVD Kino on Video.

==Story== Japanese high school girls who engage in enjo kōsai, or compensated dating. This is a practice in Japan where older businessmen pay teenage girls more commonly to simply spend time with them, or rarely for prostitution.  The movie is also a coming-of-age story.  The main character, Hiromi, does not have the direction in life that her friends already have.  Hiromis friends were going to buy Hiromi a ring, but Hiromi refuses to take all the money because she does not want her friends to be jealous.  Hiromi goes on dates by herself to get money for the ring. Soon, she gets in over her head.  Hiromi falls too far into the world of enjo-kosai as she tries to hold on to a "friends forever" vision of the past.

==Personnel==
*Director: Hideaki Anno
*Original Story: Ryū Murakami
*Script: Akio Satsukawa

===Cast===
*Asumi Miwa -  
*Kirari -  
*Hirono Kudō -  
*Yukie Nakama -  
*Mitsuru Hirata
*Mitsuru Fukikoshi
*Moro Morooka
*Tooru Tezuka
*Ikkei Watanabe
*Tadanobu Asano
*Kotono Mitsuishi
*Akira Ishida
*Verbal Dialing Information Voice: Megumi Hayashibara
*Nana Okata
*Leo Morimoto

==DVD Variations==
An SR-Ban version was released in Japan on the 24 July 2003. Apparently, it is the directors cut, has 2 minutes of extra footage and has been transferred directly from the original tape, whilst the American and Japanese DVDs have been transferred appears to come from a different source. The American version appears to be the original interlaced version, whilst the Japanese, non-directors cut, appears to have been de-interlaced and given the impression of a pseudo progressive style. Both non-directors cut have more subdued colours, whilst the directors cut is more vivid and the motion is fluid. 

==Reception==
iSugoi.com gave the film an overall positive review, and commented that "Such issues as self value, respect and friendship are brought up numerous times within the film, and it’s through these attributes that the film delivers a hauntingly poignant statement concerning a societal problem that exists within modern day Japan. It’s a problem that can’t easily be fixed, but films like Love & Pop courageously attempt to address it and more importantly provide an outlet in which to discuss it." 

==References==
 

== External links==
* 
* 
*  
* http://replay.waybackmachine.org/200011201945/http://www.gainax.co.jp/special/pop/intro-e.html
* http://replay.waybackmachine.org/200012041126/http://www.gainax.co.jp/special/pop/cast-e.html

 

 
 
 
 
 
 
 