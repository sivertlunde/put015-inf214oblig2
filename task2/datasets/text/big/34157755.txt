The Rise of Susan
{{infobox film
| name           = The Rise of Susan
| image          = The-rise-of-susan-lobbyposter-1916.jpg
| imagesize      =
| caption        = Lobby poster.
| director       = Stanner E. V. Taylor
| producer       =
| writer         = Frances Marion
| starring       = Clara Kimball Young
| music          =
| cinematography = Hal Young
| editing        =
| distributor    = World Film Company
| released       = December 18, 1916
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}}
The Rise of Susan is a 1916 American silent film made by the Peerless Film Company and distributed by World Film which starred Clara Kimball Young. Remnants of a print survive in the Library of Congress missing several reels. A fuller version may exist at the George Eastman House.   

==Cast==
*Clara Kimball Young - Susan
*Jennie Dickerson - Mrs. Joseph Luckett
*Warner Oland - Sinclair La Salle
*Marguerite Skirvin - Ninon Eugene OBrien - Clavering Gordon

==References==
 

==External links==
 
*  
* 

 
 
 
 
 
 
 


 