The Bituminous Coal Queens of Pennsylvania
{{Infobox film
| name = The Bituminous Coal Queens of Pennsylvania
| image =
| caption = David Hunt Jody Eldred
| producer = Patricia Heaton
| cinematography = Jody Eldred René Jung
| editing = Edgar Burcksen Fabian
| music = Michael Wolff
| released =  
| runtime = 89 minutes
| language = English
| country = United States
| distributor = Netflix
| budget = $350,000   
}} David Hunt and Jody Eldred about the fiftieth annual "Pennsylvania Bituminous Coal Queen" beauty pageant which took place on Sunday, August 17, 2003 at the State Theatre Center for the Arts (Uniontown, Pennsylvania). The film is produced by Hunts wife Patricia Heaton, and prominently features actress Sarah Rush, who was herself a Coal Queen in her youth. Heaton describes the film as "an homage to small town America". 

The film follows the past and present contestants and winners of the annual beauty pageant sponsored by the bituminous coal industry of Greene County, Pennsylvania and was shot over a 10 day period in August 2003.   The movies budget swelled from $45,000 to $350,000 after the licensing for the various song snippets that appear in the contestants acts. 

A memorable character is the pageants stage manager, who is easily offended when the contestants make requests and suggestions. He has since been fired. 

{| class="wikitable"
|+ Featured contestants
! Name !! Act !! Representing
|-
| Dana Bukovitz || Tap dance || Frazier High School
|-
| Alyssa Corfont || Tap dance to "Fame (Irene Cara song)|Fame" || Waynesburg Central High School
|-
| Elizabeth Gessner || Piano and voice performance of "Change the World" || Geibel Catholic High School
|-
| Mary Hawkins || Dance || Jefferson-Morgan High School
|-
| Christine Henry || Dance || Mapletown Junior-Senior High School
|- Carmichaels Area High School
|-
| Abbey Lion || Dance || Laurel Highlands High School
|- All That Jazz" || Uniontown Area Senior High School
|- All That Jazz" || Bethlehem Center High School
|- The Rose" || Albert Gallatin Area High School
|-
| Laura Yost || Xylophone || Clay-Battelle High School
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 