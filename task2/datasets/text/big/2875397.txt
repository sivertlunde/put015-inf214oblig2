Empire of the Ants (film)
{{Infobox film
| name = Empire of the Ants
| image = Empire of the ants.jpg
| caption =Film poster by Drew Struzan 
| director = Bert I. Gordon
| producer = Samuel Z. Arkoff (executive producer) Bert I. Gordon (producer)
| writer = Bert I. Gordon Jack Turley Robert Lansing John Carson
| music = Dana Kaproff
| cinematography = Reginald Morris
| editing = Michael Luciano
| studio = Cinema 77
| distributor = American International Pictures
| released =  
| runtime = 89 min.
| language =
| country = United States
| budget =
| gross = $2.5 million Richard Nowell, Blood Money: A History of the First Teen Slasher Film Cycle Continuum, 2011 p 257 
}} science fiction horror film co-scripted and directed by Bert I. Gordon. {{cite news
 | first=A. H. | last=Weiler | date=June 30, 1977
 | title=Movie Review: Empire of the Ants A Snails-Pace Film | work=The New York Times
 | url=http://movies.nytimes.com/movie/review?res=9A0CE1D91E38E53BBC4850DFB066838C669EDE
 | accessdate=2010-02-10
}}  Based very loosely on the short story Empire of the Ants by H.G. Wells, the film involves a group of prospective land buyers led by a land developer, pitted against giant, mutated ants.
 The Food The Island of Dr. Moreau.

==Plot==
  in the movie]]

A heavenly paradise becomes a hellish nightmare when a toxic spill turns harmless ants into gigantic rampaging monster insects. 

The opening narration briefly introduces the viewer to the ant and its behavior.  It takes note how ants use pheromones to communicate, and how they cause an obligatory response that must be obeyed.  "But humans dont have to worry about that...."  As the opening credits roll, barrels of radioactive waste are being dumped off a boat into the ocean.  Eventually one of the barrels washes up on the shore and begins to leak a silvery goo attractive to local ants.  

Meanwhile, shady land developer Marilyn Fryser (Joan Collins) takes a bunch of new clients to view some beach-front property on a nearby island.  In reality the land is worthless, but the trip is cut short by the group stumbling upon the lair of gigantic ants.  The ants destroy their boat and chase the group through the woods. Fleeing for their lives through the wilderness and losing many of their party along the way, the remaining survivors eventually discover the local island town.  But their safety is short-lived when they realize that not only are the giant ants feeding on the local sugar factory, but at the behest of the humans.  The queen ant, using pheromones has the entire town completely under control, and the giant ants are determined to exterminate humankind and build an evil empire.  However, the survivors manage to escape and burn the sugar factory, killing the giant ants, and leave the island by a speedboat.

==Special effects==
 
As with most Bert I. Gordon films, the giant ants are rendered by way of photographically enlarged animal footage and rear projection. The size of the ants varies from scene to scene. They often appear to suddenly crawl off the ground and walk vertically upwards in thin air. This is due to real ants being placed in a set lined with photographs of the locations where the scenes were shot. When an ant walked onto the rear photograph it appeared to walk on the sky. Where the film calls for actors to interact with the ants, large mock-up props are used. On the props, Joan Collins later said she did not like working with the ant props as they bumped and scratched the actors, including herself.

==Cast==
* Joan Collins as Marilyn Fryser Robert Lansing as Dan Stokely
* John David Carson as Joe Morrison
* Albert Salmi as Sheriff Art Kincade
* Jacqueline Scott as Margaret Ellis 
* Pamela Susan Shoop (as Pamela Shoop) as Coreen Bradford
* Robert Pine as Larry Graham

==References==
 

==Further reading==
*Weaver, Tom (2010). "Robert Pine on Empire of the Ants (1977)". Sci-Fi Swarm and Horror Horde: Interviews with 62 Filmmakers. McFarland. ISBN 978-0-7864-4658-2. pp. 382-385.

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 