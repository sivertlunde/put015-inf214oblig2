Lonesome Cowboys (1968 film)
{{Infobox film
| name           = Lonesome Cowboys
| image          = Lonesomecowboys.png
| caption        = original film poster
| director       = Andy Warhol
| producer       = Paul Morrissey
| writer         = Paul Morrissey
| narrator       = Viva Julian Burroughs 
| music          =
| cinematography = Paul Morrissey
| editing        = Paul Morrissey
| distributor    = Sherpix
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Lonesome Cowboys (1968) is a film by American filmmaker Andy Warhol. Written by Paul Morrissey, the film is a satire of Hollywood western films|westerns. The film won the Best Film Award at the San Francisco International Film Festival.
 Old Tucson and Rancho Linda Vista Dude Ranch in Oracle, Arizona. The film features Warhol superstars Viva (Warhol Superstar)|Viva, Taylor Mead, Eric Emerson, and Joe Dallesandro. The plot is loosely based on Romeo and Juliet, hence the names Julian and Ramona of the two leads. In August 1969, the film was seized by police in Atlanta, Georgia and the theater personnel arrested. 
 
==Cast==
*Joe Dallesandro as Little Joe
*Julian Burroughs as Brother
*Eric Emerson as Eric	
*Tom Hompertz as Julian	
*Taylor Mead as Nurse Viva as Ramona DAlvarez
*Louis Waldon as Mickey
*Francis Francine as Sheriff

==Remakes==
A 2010 remake by Marianne Dissard titled Lonesome Cowgirls,  was shot in Tucson, Arizona.

==See also==
*Andy Warhol filmography

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 