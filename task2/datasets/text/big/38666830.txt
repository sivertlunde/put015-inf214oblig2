The Numbers Station
  
{{Infobox film
| name           = The Numbers Station
| image          = The Numbers Station pre-release poster.jpg
| alt            = 
| caption        = Pre-release cinema poster
| director       = Kasper Barfoed
| producer       = Sean Furst Bryan Furst Nigel Thomas
| screenplay     = F. Scott Frazier
| starring       = John Cusack Malin Åkerman
| music          = Paul Leonard-Morgan
| cinematography = Óttar Guðnason
| editing        = Chris Gill Per Sandholt
| studio         = Furst Films Matador Films
| distributor    = Image Entertainment (US)
| released       =   
| runtime        = 89 minutes
| country        = United Kingdom United States Belgium
| language       = English
| budget         = 
| gross          = 
}}
The Numbers Station is a 2013 British-American-Belgian action thriller film, starring John Cusack and Malin Åkerman, about a burned-out CIA black ops agent assigned to protect the code operator at a secret American numbers station somewhere in the British countryside.   Retrieved 2013-02-28 

The film was directed by Danish director Kasper Barfoed, and the camera work was by Icelandic cinematographer Óttar Guðnason. It was produced by brothers Sean and Bryan Furst of American Furst Films and Nigel Thomas at British production and film finance company Matador Pictures.

==Plot==
A CIA operative, Emerson Kent, is sent to kill a man who owns a bar. Before being killed by Kent, he reveals he is a former agent who wanted to retire. A witness flees the scene, accidentally leaving his wallet behind. Kent finds the wallet and tracks the witness to his home, where he kills him. Kent spares the life of mans daughter, who follows him outside, hysterical.  As Kent tries to convince his boss, Michael Grey, not to kill her, Grey strikes Kent on the back and he falls to the ground. Kent and the woman share a last look at each other as Grey kills her. Kent is transferred to Suffolk, England to watch over a numbers station. While there, he befriends Katherine and is haunted by memories of the woman who was killed. When the numbers station comes under attack, Kent and Katherine barricade themselves inside. One assassin is already inside the secure station and, after a lengthy shootout, is killed by Kent.

Kent requests assistance, and the operator tells him help will arrive in four hours; since the code has been compromised, he must kill Katherine. Kent notices that Katherine has a serious leg wound and dresses her wound. Kent receives an update from the operator, and, when he reports that he has not killed Katherine yet, the operator orders him to do so immediately. Kent contemplates her death but ultimately decides to recruit her help in tracing fifteen unauthorized messages sent from the station. On the computer, Kent and Katherine discover dossiers of fifteen different government officials, including Grey. The unauthorized codes are instructions to assassinate the officials, and Katherine is to be eliminated so she cant cancel the broadcasts. Kent says that the assassinations would leave the intelligence world crippled and the world unrecognizable.

Kent tricks the telephone operator by giving him a false confirmation code; the operator offers Kent a deal, and Kent pretends to have killed Katherine. Kent escapes to his car, where he recovers a cell phone, and then races back to protect Katherine, who has cracked the code and is broadcasting orders to cancel to previous instructions. Katherine leaves her station when she sees the assassin, but he manages to wound her before Kent kills him. Katherine insists that Kent complete the final cancellation order, and he leaves her side briefly. When he returns, he administers an anesthetic, and she asks him if she will wake. Kent reassures her that he will not kill her, but he reports her as dead to Grey. Kent spreads C4 explosives throughout the base and drops Kathetines jewelry on the floor.  After he carries Katherine outside, the base explodes, destroying all evidence.

Kent hijacks a car, only to realize that the driver is the telephone operator. When Kent asks him who he works for, the operator replies that he used to work for the same people that Kent does, but he now works for the other side; they are just as twisted, but they pay better. The two shoot each other at the same time, but Kent survives and attempts to drive away, only to fall unconscious from shock and crash. Kent wakes in a hospital and discovers that Katherine is alive. Grey steps in and says that she is a liability, but Kent is able to save her life by convincing Grey that she is responsible for saving his life. Grey volunteers to find their bodies at the ruins of the station, and, as the end credits roll, cars are shown passing over the Orwell Bridge in Ipswich at night, implying that Kent and Katherine escaped.

==Cast==
* John Cusack as Emerson Kent, a disgraced CIA black ops agent who is assigned to watch over a numbers station in England.
* Malin Åkerman as Katherine, a code operator at the English numbers station.
* Liam Cunningham as Michael Grey, Emersons boss. Lucy Griffiths as Meredith
* Richard Brake as Max
* Joey Ansah as Derne
* Hannah Murray as Rachel Davis, a young girl.

==Release==
The Numbers Station premiered at the 2012 American Film Market.   The movie was pre-released in early April 2013 on Apple Inc.|Apples iTunes video store and video streaming service Netflix.

==Reception==
Rotten Tomatoes, a review aggregator, reports that 29% of 24 surveyed critics gave the film a positive review; the average rating was 4.8/10.   Metacritic rated the film 39/100 based on eight reviews.   Justin Chang of Variety (magazine)|Variety wrote, "This glum, juiceless spy thriller is unlikely to find an audience on any frequency."   Jeannette Catsoulis wrote, "this dreary spy drama is as flat and airless as the concrete bunker in which it unfolds."   Robert Abele of the Los Angeles Times called it "a predictable hodgepodge of uninteresting psychological cat-and-mouse".   John Hazelton of Screen Daily called it "a serviceable CIA spy thriller whose solid international cast, led by John Cusack, doesn’t quite make up for the film’s lack of scope and flair."   One of the movies most positive reviews came from Chuck Wilson of the Village Voice who wrote, "there are some decent shootouts, but the movies strongest assets are the soulful performances from John Cusack and Malin Akerman".  Another positive review came from Lou Lumenick of The New York Post who rated the film three out of four stars and stated, "Kasper Barfoed, a Danish director in his English-language debut, makes great use of the main location, a bunker on a former airbase in the UK, as well as a novel premise. With Cusacks help, Barfoed holds your interest without resorting to car chases, a rarity in a contemporary thriller." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 