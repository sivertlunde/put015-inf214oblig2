Hawaii Calls (film)
 
{{Infobox film
| name           = Hawaii Calls
| image          = Hawaii Calls.jpg
| image_size     =
| caption        =
| director       = Edward F. Cline
| producer       = Sol Lesser
| writer         = Don Blanding (novel) Wanda Tuchock (writer)
| narrator       =
| starring       = See below
| music          = Hugo Riesenfeld
| cinematography = Jack MacKenzie
| editing        = Arthur Hilton
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
}}

Hawaii Calls is a 1938 American film directed by Edward F. Cline, produced by Sol Lesser Productions and Bobby Breen Productions, and released by RKO Radio Pictures.

==Cast==
*Bobby Breen as Billy Coulter
*Ned Sparks as Strings
*Irvin S. Cobb as Captain OHare
*Warren Hull as Commander Milburn
*Gloria Holden as Mrs. Milburn
*Juanita Quigley as Doris Milburn
*Mamo Clark as Hina
*Pua Lani as Pua
*Raymond Paige as Himself
*Herbert Rawlinson as Harlow
*Dora Clement as Mrs. Harlow
*Philip Ahn as Julius
*Donald Kirke as Regon
*William Abbey as Lonzo
*Ward Bond as Muller
*Birdie De Bolt as Aunty Pinau
*Laurence Duran as Banana
*William Harrigan as Blake
*Ruben Maldonado as Solly
*Aggie Auld as Hula Dancer
*Cy Kendall as Hawaiian Policeman

==Soundtrack==
* Bobby Breen - "Down Where the Trade Winds Blow" (Written by Harry Owens)
* Bobby Breen - "Hawaii Calls" (Written by Harry Owens)
* Bobby Breen - "Thats the Hawaiian in Me" (Written by Johnny Noble and Margarita Lake)
* "España (Chabrier)|España" (Music by Emmanuel Chabrier)
* "Macushla" (Words by Josephine V. Rowe, music by Dermot Macmurrough)
* "Song Of The Islands (Na Lei O Hawaii)" (Words and Music by Charles E. King)

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 