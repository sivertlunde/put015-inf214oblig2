Swordfish (film)
{{Infobox film
| name           = Swordfish
| image          = Swordfish movie.jpg
| caption        = Theatrical release poster
| director       = Dominic Sena
| producer       = Joel Silver Jonathan D. Krane
| writer         = Skip Woods
| starring       = John Travolta Hugh Jackman Halle Berry Don Cheadle Vinnie Jones
| music          = Christopher Young Paul Oakenfold Paul Cameron
| editing        = Stephen E. Rivkin
| studio         = Village Roadshow Pictures Silver Pictures Warner Bros. Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $102 million   
| gross          = $147,080,413 
}}
 action crime crime thriller thriller film computer hacker who is targeted for recruitment into a bank robbery conspiracy because of his formidable hacking skills. The film was a slight box office success and was negatively received by critics upon release. It was also notable for Halle Berrys first topless scene.   

== Plot ==
In a restaurant in Los Angeles, a man discusses the 1975 film Dog Day Afternoon before walking off, with numerous SWAT officers pointing guns at him and a dishevelled man following him out. They walk to a nearby building with gunmen and hostages strapped with bombs and ball-bearings. One of the gunmen is killed by a sniper; when the hostage is forcibly taken away, the explosives detonate via a proximity trigger, killing her and others. The film then flashes back four days.
 hacker who Carnivore program with a potent computer virus, delaying its deployment by several years. For this, he was arrested by Agent J.T. Roberts (Don Cheadle), convicted of computer crimes and spent two years in United States Penitentiary, Leavenworth|Leavenworth.

A condition of his parole is that he is forbidden from touching, much less using, a computer. His ex-wife, Melissa (Drea de Matteo), has sole custody over their daughter Holly and a restraining order against Stanley from seeing Holly, despite her alcoholism, her job as a porn actress, and her marriage to her employer.
 Finnish hacker Los Angeles and meet Gabriel in a night club. Gabriel pressures Stanley right then and there to hack a government system in 60 seconds while simultaneously being held at gunpoint by Gabriels bodyguard and right-hand man, Marco (Vinnie Jones) and receiving fellatio from a young woman (Laura Lane). Stanley has succeeded in hacking the system, passing Gabriels test.

Roberts realizes that Stanley was recruited to replace Torvalds, and begins surveying him, watching as he picks Holly up from school(Melissa being unconscious in an alcoholic stupor at the time) and drives her home. Confronting him afterwards, Stanley attacks Roberts and attempts to escape, only for one of Robertss subordinates to hit him with his car. However, Roberts then says that he wont arrest him for visiting Holly, as he is more interested in Gabriel, who has a disturbing amount of resources and no traceable past. When Stanley confronts Ginger, she admits that shes undercover from DEA.
 Senator Reisman (Sam Shepard), who discovers that the FBI has caught onto Gabriel and attempts to pull the plug. After Gabriel refuses to terminate his plans, Reisman attempts to have Gabriel killed, which fails. Gabriel tracks the Senator down while he is fly fishing in Bend, Oregon and kills him. Afterwards, he kills Melissa and her new boyfriend, then kidnaps Holly.

Stanley & Roberts confront Gabriel, who talks about Dog Day Afternoon. Gabriel proceeds with his plan and raids the local branch of the WorldBanc. In the process, he takes hostages, puts explosives on them and deploys Stanleys hydra. Having realized that Ginger is from DEA, Gabriel threatens to kill Ginger if Stanley doesnt deploy the hydra. Stanley deploys the hyrda, but Ginger is killed anyway. After stealing the $9.5&nbsp;billion he boards the hostages and his crew on a bus out of the WorldBanc. Gabriel demands a plane at the local airport (a hostage negotiation cliché) but it was a diversion. An S-64 Aircrane swoops down, lifts the bus and releases it on the rooftop of a skyscraper.  From the rooftop, Gabriel seemingly departs with his team in a helicopter, which Stanley shoots down with a rocket-propelled grenade. At the morgue, Stanley and Agent Roberts learn that the body recovered from the helicopter is that of a former Mossad agent named Gabriel Shear (possibly a mocked up dummy as Stanley saw the double in the "freezer" at Gabriels house). Stanley recognizes this corpse from "Gabriels" house. Stanley realizes that DEA has no record of anyone named Ginger. DEA also does not have a picture of Ginger. Gingers corpse is actually missing. After realizing that Ginger was wearing a bullet-proof jacket, Stanley realizes that Ginger and (imposter Gabriel) were partners in crime all along, making the entire operation a misdirection. Despite this, Stanley doesnt tell Roberts or the rest of the police that (Ginger & imposter Gabriel) are still alive. Roberts releases Stanley from prison, allowing him to have full custody of Holly.

Ginger and Gabriel are next seen in Monte Carlo transferring the $9.5&nbsp;billion into other accounts. Finally, while Ginger and Gabriel look on, a yachts destroyed as a news anchor is heard reporting that a suspected terrorist died on the yacht, the third such successful counter-terrorism operation in as many weeks.

== Cast ==
*John Travolta as Gabriel Shear
*Hugh Jackman as Stanley Jobson
*Halle Berry as Ginger Knowles
*Don Cheadle as Agent J.T. Roberts
*Sam Shepard as Senator James Reisman
*Vinnie Jones as Marco
*Drea de Matteo as Melissa
*Rudolf Martin as Axl Torvalds
*Zach Grenier as Assistant Director Bill Joy
*Camryn Grimes as Holly Jobson
*Angelo Pagan as Torres
*Kirk B. R. Woller as Axls Lawyer
*Carmen Argenziano as Agent
*Tim DeKay as Agent
*Laura Lane as Helga

== Reception ==
The film received a great deal of press initially because it featured Halle Berrys first topless scene. She was paid an extra $500,000 on top of her $2&nbsp;million fee to appear topless in this film. Critics said the scene looked forced, thrown into the film just to garner press. Berry said she did the topless scene, knowing it was gratuitous, to overcome the fear of appearing nude onscreen. 

Only 26% of critics gave the film a positive review according to Rotten Tomatoes.  In a review for The New York Times, Stephen Holden wrote: 

 

According to Box Office Mojo, the film grossed over $147&nbsp;million in worldwide box office receipts on a production budget of $102 million.  John Travoltas performance in the film earned him a Razzie Award nomination for Worst Actor (also for Domestic Disturbance).

== DVD alternate ending ==
The DVD version contains an alternate ending wherein Ginger is told in the bank that the account is already almost empty, alluding to the possibility that Stanley has played one final trick on them and taken the money himself. When Ginger tells Gabriel about this, he takes it in stride and asks her to join him on a trip to Istanbul. In a companion scene to the alternate ending, Stanley is shown on a trip with his daughter in a brand new RV. While eating at a diner, Stanley is shown transferring billions of dollars to various charities before continuing his trip.

== Soundtrack ==
  London Sire Records, Inc. It contains 15 tracks. The films orchestral score was written by Christopher Young with several electronic additions by Paul Oakenfold. Fragments from the score were added to the official soundtrack, but were remixed by Oakenfold. A more complete release was issued as an award promo, which is known for its rarity. 

== See also ==
* Swordfish (password) – Origin of the films title
* Linus Torvalds – The name of the hacker/cracker, Axl Torvalds, is an homage to the author of the Linux kernel, Linus Torvalds.
* Bill Joy – The name of the FBI Assistant Director, Bill Joy, appears to be an homage to the American computer scientist and Sun Microsystems co-founder, Bill Joy.
* Incompatible Timesharing System – Another homage; the operating system running on the old PDP-10 where Stanley Jobson hid his worm.
* Art Rooney – The source of Gabriel Shears "dont mistake kindess for weakness" quote.

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  
*   on helicopter sequence from the Erickson Air-Crane website

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 