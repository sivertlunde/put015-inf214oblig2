Single Wives
{{Infobox film
| name           = Single Wives
| image          = File:Single Wives lobby card.jpg
| image size     = 
| border         = 
| alt            = Lobby card
| caption        = Lobby card
| director       = George Archainbaud
| producer       = Corinne Griffith
| writer         = Marion Orth (Scenario)
| screenplay     = 
| story          = Earl Hudson
| based on       =  
| starring       = Corinne Griffith Milton Sills Lou Tellegen Kathlyn Williams
| music          = 
| cinematography = 
| editing        = Arthur Tavares
| studio         =  Associated First National Pictures
| released       =   
| runtime        = 80 mins.
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          =
}}
 silent drama film produced by and starring Corinne Griffith, and distributed by First National Pictures.

==Cast==
*Corinne Griffith - Betty Jordan
*Milton Sills - Perry Jordan
*Kathlyn Williams - Dorothy Van Clark
*Phyllis Haver - Marion Eldridge
*Phillips Smalley - Tom Van Clark
*Jere Austin - Dr. Walter Lane
*Lou Tellegen - Martin Prayle
*Henry B. Walthall - Franklin Dexter
*John Patrick - Billy Eldridge

==Preservation status==
Single Wives is preserved at the George Eastman House film archive.  

==References==
 

==External links==
*  
*  
*  
* 
 
 
 
 
 
 
 
 
 


 