Cutting Moments
{{Infobox Film
| name = Cutting Moments
| image =Cutting Moments VHS cover.jpg
| caption = Cutting Moments VHS cover
| writer = Douglas Buck
| starring = Nica Ray Gary Betsworth Jared Barsky  
| director = Douglas Buck
| producer= Douglas Buck
| released = November 8, 1997
| runtime = 25 minutes
| country = United States
| language = English
}}
Cutting Moments (Daybreak) is a 1997 short film written, produced and directed by Douglas Buck,   in cooperation with The New School.  The film was re-released in August 2004 as part of Bucks suburban holocaust collection Family Portraits: A Trilogy of America. 
{{cite news
|url=http://www.filmmakermagazine.com/archives/online_features/suburban_holocaust.php
|title=Suburban Holocaust
|last=Kipp
|first=Jeremiah 
|date=July 9, 2004
|publisher=Filmmaker Magazine
|accessdate=2009-11-18}} 

== Synopsis ==
In the center of a monotonous-like subdivision, Sarah and Patrick have been living with their emotions kept isolated between themselves and their son. Patrick seems to be cold and harsh by ignoring Sarahs existence, and the affection Patrick once had for her has all but disappeared. It comes to the point that his sexual urges are manifested in his actions toward his son, Joey. Sarah is left in bewilderment when she hears the discussion of her husband and son at nighttime; she even asks herself, "What have I done?" When Sarah has run out of options for trying to bring back the spark in their marriage, she tries to do the unthinkable.

== Cast ==
*Nica Ray as Sarah 
*Gary Betswortas Patrick 
*Jared Barsky as Joey

==Recognition== 
Anita Gates of New York Times wrote of the film "scenes are almost unwatchable but have a curious, grotesque power", and of Bucks work, "There is a sober intelligence behind his low-budget gore, but its shrill excess drowns out the ring of truth". 
{{cite news
|url=http://movies.nytimes.com/2004/10/13/movies/13fami.html?_r=1
|title=A Sadistic Fathers Legacy Cuts a Swath of Suffering
|last=Gates
|first=Anita
|date=October 13, 2004
|publisher=New York Times
|accessdate=2009-11-18}}   Brian Bertoldo of Film Threat wrote that the film "brings the viewer into a nightmare of insanity and mutilation as a married couple come apart at the seams", and noting that while real suburbia horror is almost too common to be a horror theme, "What does work is the gory execution, thats not something youll see on the 11 oclock news."     

===Awards and nominations===
* 1997, 3rd place in Best Short Film, Fant-Asia Film Festival 
* 1998, Nomination for an International Horror Guild Award

== References ==
 

== External links ==
*  at the Internet Movie Database

 
 
 
 