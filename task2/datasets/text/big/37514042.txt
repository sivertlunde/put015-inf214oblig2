A Bottle in the Gaza Sea
{{Infobox film
| name           = A Bottle in the Gaza Sea
| image          = A Bottle in the Gaza Sea Poster.jpg
| caption        = Film poster
| director       = Thierry Binisti
| producer       = TS Productions France 3 Cinéma EMA Films (Canada) Lama Films (Israel)
| writer         = Thierry Binisti, Valérie Zenatti
| starring       = Agathe Bonitzer Mahmoud Shalabi Hiam Abbass
| music          = Benoît Charest
| cinematography = Laurent Brunet
| editing        = Jean-Paul Husson
| distributor    = Roissy Films
| released       =  
| runtime        = 100 minutes
| rating         =
| country        = France Canada Israel Arabic French French
| budget         = €2,000,000 (estimated)
}}
A Bottle in the Gaza Sea ( ,  ) is a 2011 drama directed by Thierry Binisti. The film, an international co-production shot in French, Hebrew and Arabic, is based on the French young adult novel Une bouteille dans la mer de Gaza by Valérie Zenatti, originally published in 2005 and adapted for the screen by Zenatti and Binisti.

==Plot== Gaza with Palestinian living in Gaza, discovers the bottle and tries to answer Tals question by initiating an email correspondence. Their mutual suspicion soon develops into a tender friendship.

==Cast==
*Agathe Bonitzer as Tal Levine
*Mahmoud Shalabi as Naïm Al Fardjouki
*Hiam Abbass as Intessar
*Riff Cohen as Efrat
*Abraham Belaga as Eytan Levine
*Jean-Philippe Écoffey as Dan Levine
*Smadi Wolfman as Myriam
*Salim Dau as Ahmed
*Loai Nofi as Hakim
*François Loriquet as Thomas Morin
*Abdallah El Akal as Daoud

==External links==
*    
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 