Fever Pitch (2005 film)
{{Infobox film
| name           = Fever Pitch
| image          = Fever Pitch US.jpg
| caption        = Theatrical release poster Peter Farrelly Robert Farrelly
| producer       = Amanda Posey Alan Greenspan Gil Netter Drew Barrymore Nancy Juvonen Bradley Thomas
| screenplay     = Lowell Ganz Babaloo Mandel
| based on          =  
| starring       = Drew Barrymore Jimmy Fallon JoBeth Williams KaDee Strickland Craig Armstrong
| cinematography = Matthew F. Leonetti
| editing        = Alan Baumgarten Conundrum Entertainment
| distributor    = 20th Century Fox
| released       =  
| runtime        = 103 minutes
| language       = English
| budget         = $30 million
| gross          = $50,451,307 
}} of the of the same name, a best-selling memoir in the United Kingdom. Hornby also wrote the screenplay for the original film, but had no input for the American remake.
 League title win in 1989, and the remake on the Boston Red Soxs 2004 World Series Championship.

==Plot== Red Sox game with his Uncle Carl. His uncle treated him like a son because he had no children of his own. A narration explains that ever since that day, Ben became a Red Sox Nation|die-hard Red Sox fan. Just about everything he owns bears the Red Sox name, emblem or the image of a Red Sox player (with the exception of his toilet paper, which bears the New York Yankees insignia). Ben inherited his uncles season tickets when he died. The story picks up 23 years later with Ben (Jimmy Fallon) as a school teacher who is still rather immature for his age. He meets Lindsey Meeks (Drew Barrymore), a professionally successful workaholic executive. When he first asks her out, she rejects him, but she later changes her mind and agrees to go out with him.
 first date, relationship with each other.
 Mike Myers.  She eventually recovers, but stops going to the games.
 obsession with against the Yankees in order to go with Lindsey to her friends birthday party.  Ben and Lindsey have a wonderful time together, and after making love, he tells her it was one of the best nights of his life.  Moments later, Ben receives a call from his ecstatic friend Troy who informs him that the Red Sox overcame a seven run deficit in the bottom of the ninth inning to pull off one of the greatest comebacks in team history.  Ben becomes irate that he missed such a historic Red Sox moment, greatly hurting Lindseys feelings.  After Lindsey miserably declares he has broken her heart, he and Lindsey separate for a while.

Ben soon misses Lindsey, and visits her in a futile attempt to reconcile.  He eventually feels her loss so deeply that he plans to sell his season tickets to Chris, Lindseys girlfriend Robins husband, in order to prove that she means more to him than the Red Sox do.  Lindsey finds out about his plan during the celebration for her much-anticipated promotion.  Immediately leaving the celebration, she rushes to the ballpark to try to stop him. She gets in during the 8th inning of the Red Sox—Yankees playoff game when the Sox are just 3 outs away from being swept.  Ben is actually in the process of signing a contract with Chris as they sit in the stands.  Because she is unable to reach Ben from her section in Fenway Park in time to stop him from signing the contract, she illegally runs across the field, deftly avoiding security personnel as she eventually reaches him.  She tears Chris contract in pieces and explains that if he loves her enough to sell his seats, then she loves him enough not to allow him to do so.  The two reunite and kiss in front of the entire crowd.

The narration explains how the Red Sox won that game and then beat the Yankees three more times to win the American League pennant, later sweeping the National League champion St. Louis Cardinals in four games for their first World Series title in 86 years. Ben and Lindsay get married, and she gets pregnant. The narration explains that the baby will be named after one of the players, Ted Williams (for Ted Williams) if its a boy and Carla Yastrzemski (for Carl Yastrzemski) if its a girl, with the narration hoping for a boy.

==Cast==
 
* Drew Barrymore as Lindsey Meeks
* Jimmy Fallon as Ben Wrightman
* Jason Spevack as Ben in 1980
* Jack Kehler as Al
* Scott Severance as Artie
* Jessamy Finet as Theresa
* Maureen Keiller as Viv
* Lenny Clarke as Uncle Carl
* Ione Skye as Molly
* Siobhan Fallon Hogan as Lana
* KaDee Strickland as Robin
* Marissa Jaret Winokur as Sarah
* Evan Helmuth as Troy
* Brandon Craggs as Casey
* Brett Murphy as Ryan
* Isabella Fink as Audrey
* Miranda Black as Carrie
* Greta Onieogou as Tammy
* Johnny Sneed as Chris
* James Sikking as Doug Meeks
* Michael Rubenfeld as Ian
* Willie Garson as Kevin
* Armando Riesco as Gerard
* Zen Gesner as Steve
* JoBeth Williams as Maureen Meeks
* Mark Andrada as Ezra
* Charlotte Sullivan as Spin Instructor
* Scott Desano as Binocular Guy
* Lizz Alexander as Charlene
* Shary Guthrie as Christie
* Don Gavin as Cop Andrew Wilson as Grant Wade / Patrick Lyons
* Martin Roach as Husband
* Gina Clayton as Lady at Other Table
* Wayne Flemming as Leon
* Jackie Burroughs as Mrs. Warren
* Stephen King as Himself Kris Williams as Herself
 

==Production== 2004 ALCS World Series Busch Stadium in character. When the Red Sox made the final out to secure a 3-0 win over the Cardinals that broke the Curse, FOX cameras on the live broadcast caught Barrymore and Fallon, as Lindsey and Ben, running onto the field and kissing to celebrate. 

Originally, Shawn Levy, who was a huge fan of the works of Nick Hornby for years, was attached to direct with Gwyneth Paltrow playing Lindsey.  However, after reading the script, Paltrow found it mediocre and she turned down the role.  Brian Robbins replaced Levy, but he quit the project as well.  After Drew Barrymore replaced Paltrow and Jimmy Fallon joined the cast, Jay Russell,  P.J. Hogan,  Luke Greenfield,  and Mira Nair were all rumored candidates to direct until the studios hired the Farrelly brothers to take the helm for the film.

==Reception==

===Critical response=== weighted average score out of 100 to reviews from mainstream critics, the film received an average score of 56 based on 37 reviews. 

From a cinematographic and literary perspective, the film received some favorable criticism from experts Roger Ebert  and James Berardinelli. 

The film opened at #3 and grossed $12.4 million in its opening weekend. The final North American gross of the film was $42,071,069, and the worldwide gross was $50,451,307.   

===Fan response===
"Sports Guy" Bill Simmons of ESPN disliked the film because he regarded it as a "chick flick" disguising itself as a sports movie and said that no Red Sox fan would give up season tickets for love, but Red Sox Nation voted to award Fallon honorary membership for playing Ben so convincingly in spite of Fallon being a Yankee fan.

==Soundtrack==
{{Infobox album  
| Name        = Fever Pitch: Music from the Motion Picture
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    =  
| Recorded    = 
| Genre       = 
| Length      = 54:23 
| Label       = BulletProof Music/Rykodisc
| Producer    = 
| This album  = Fever Pitch (2005)
| Next album  = 
}}
#The Standells - "Dirty Water"
#Dropkick Murphys - "Tessie"
#Tears for Fears - "Who Killed Tangerine?"
#Popium - "Sooner or Later" Ivy - "Thinking About You"
#Nick Drake - "Northern Sky" Marah - "My Heart Is the Bums on the Street"
#Steve Wynn - "Second Best"
#The J. Geils Band - "Whammer Jammer" (Live Version) 
#The Human League - "(Keep Feeling) Fascination" Chic - "Dance, Dance, Dance (Yowsah, Yowsah, Yowsah)"
#Joe Pernice - "Moonshot Manny"
#Jonathan Richman - "As We Walk to Fenway Park in Boston Town"
#Mad Larry - "Window Pane" Hurricane Smith - "Oh, Babe, What Would You Say?"

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*   at the Baseball Movie Guide

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 