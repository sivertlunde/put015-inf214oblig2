Agnes Escapes from the Nursing Home
  London Film Showtime Cable Network, The Movie Channel, and PBS.

 

== Reviews==
"Threaded through this program are four short films, of which the most remarkable (are) Agnes Escapes From the Nursing Home, a cel-animation by the extremely gifted Eileen OMeara, whose colors and images are delicate and mysterious..."
Los Angeles Times, Sheila Benson

== Synopsis==
“The message of this (film) depends solely on the viewer’s interpretation of how Agnes escapes from the nursing home. The reviewer envisions Agnes escaping through death, thereby experiencing freedom from a life of many choices...”
 

== Festivals and screenings==
*Sundance Film Festival The London FIlm Festival
*Chicago International Film Festival
*Krakow Film Festival
* 
* 

== References==
*  
*  
*  

== External links==
*  
*  
* 
*  
* 

== See also==
*That Strange Person UNICEF Cartoons for Childrens Rights
*Eileen OMeara

 
 
 
 
 
 
 
 
 