A Gathering of Eagles
 
 
{{Infobox film
| name           = A Gathering of Eagles 
| image          = Gathering_of_Eagles_poster.jpg
| caption        = Promotional poster for A Gathering of Eagles
| director       = Delbert Mann 
| producer       = Sy Bartlett
| writer         = Sy Bartlett Robert Pirosh Barry Sullivan 
| music          = Jerry Goldsmith Tom Lehrer
| cinematography = 
| editing        = 
| distributor    = Universal International Pictures 
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $3,346,500 
| gross = $2,500,000 (US/ Canada) 
}} Above and Beyond and Toward the Unknown, films written by his collaborator, Beirne Lay, Jr..  The film was directed by Delbert Mann.
 wing commander.  He must shape up his wing and men to pass a grueling operational readiness inspection (ORI) that the previous commander had failed and for which he had been relieved of his command. Caldwell is also recently married to an English wife, and as a tough commanding officer doing whatever he has to do to shape up his command, his wife sees a side to him that she hadnt seen before.
 Barry Sullivan, Kevin McCarthy, Robert Lansing, Leif Erickson, and Richard Anderson.

==Plot==
 Major General Kevin McCarthy), Air Police Omaha headquarters and soberly informs him, "It doesnt look too good so far, sir."
 Colonel Jim General Hewitt Leif Erickson) coolly informs him that the wing commander at Carmody "didnt have what it takes" and must be replaced. Hewitt offers the job to Caldwell, who enthusiastically accepts. To Caldwell, this is a highly enviable career move, one made more auspicious when he discovers that his good friend and Korean War buddy, Colonel Hollis Farr (Rod Taylor), is the vice wing commander. Barely able to conceal his excitement, he telephones his English wife Victoria (Mary Peach) to tell her the news.

Soon after he arrives at Carmody in a  ), who, as Caldwell soon learns, drinks heavily. Eventually, Caldwell forces Fowler to retire early, and tells him straight-out that his drinking is the cause. He also alienates the wing maintenance officer, Colonel "Smokin Joe" Garcia (Henry Silva) by telling him that he must learn to delegate authority&nbsp;and when Garcia applies for a transfer to a B-58 Hustler|B-58 bomber wing, Caldwell refuses to act on it. Farr protests that Caldwell is "going out on a limb," to which Caldwell replies with a biting rhetorical question, "Whats wrong with that?"
 brigadier general commanding the 904ths parent air division to replace Farr as vice wing commander. He says, "I inherited the most popular wing vice commander in SAC&nbsp;but one who will not assume responsibility!" He then sharply contradicts Farrs rosy approval of the wings performance during a post-mission critique of B-52G and KC-135A aircraft commanders and their crews as a prelude to informing Farr that he is fired. This almost causes the final breach between Caldwell and his wife, especially since gossip has had Farr and Victoria drifting into an affair&nbsp;a rumor to which Caldwell lends no credence, but one that Victoria has heard, leading her to think that she is in some way responsible for Farrs impending dismissal.

Soon after, while Caldwell visits Fowler in a San Francisco hospital to snap him out of his depression (mood)|depression, he receives a call from the operations chief saying that an unidentified aircraft is "on final approach, no emergency declared." Suspecting another ORI, Caldwell orders the officer to notify the battle staff at once. Caldwell cannot return to base fast enough, however, and Farr must assume command in his absence. In this capacity, Farr makes a key decision: to launch a B-52 which cannot produce full power on one of its engines, a violation of peacetime flight safety regulations, because "Were simulating wartime conditions."

After another B-52 must abort its mission, General Kirbys "score" of that mission will make the difference between passing and failing. Kirby confronts Farr about the decision, and Caldwell immediately defends it, stating he would have made the same call. But Kirby, a former wing commander himself, surprises both by saying that he, too, would have done the same, and that he will not score the mission as an abort. Tellingly, he actually smiles at Caldwell as he says this. Caldwell congratulates Farr in a manner strongly suggesting he will retain Farr as his vice commander, saying that Farr has finally learned "how...it feels out on that limb" and "might actually get to like it out there". Victoria, for her part, realizes the value of Caldwells policies&nbsp;especially when General Kirby wants to see her about the bases Family Support Program.

==Background/Production== General Curtis 456th Strategic Aerospace Wing.  The military housing used in several scenes are still used today as base housing for senior commanders of the 9th Reconnaissance Wing.

It was originally announced John Gavin would support Rock Hudson. Pawnbroker Will Be Steiger Vehicle: McGiver Back at Funmaking; Curious Case of Lotte Lenya
Scheuer, Philip K. Los Angeles Times (1923-Current File)   03 Apr 1962: C9.  
 Rod Taylor, as Hollis Farr, performs this song at a party for officers and their wives. Most of Lehrers work is satirical, and the lyrics and music for this song are quite typical of Lehrer.

As Sy Bartlett and Delbert Mann were filming at SAC Headquarters at Offutt AFB in Omaha, Nebraska|Omaha, they noticed that SAC personnel were unusually tense. They would later learn, when President John F. Kennedy would make this fact public, that SAC had recently learned of Nikita Khrushchevs plan to introduce nuclear-tipped  ballistic missiles into Cuba. 

A subplot in the film where Colonel Farr has an affair with Colonel Caldwells wife, played by Mary Peach, was removed during post production.  The original choice for Mary Peachs role was Julie Andrews but according to screenwriter Robert Pirosh, Delbert Mann thought Andrews could not act, only sing.  Pirosh felt there was no rapport between Hudson and Peach. p.99 Davis, Ronald L. Robert Pirosh Interview Words into Images: Screenwriters on the Studio System  
Univ. Press of Mississippi, 01/05/2007 

==Cast== Colonel James Caldwell, Commander, 904th Strategic Aerospace Wing (904th SAW) Colonel Hollis Farr, Vice Commander, 904th SAW
*Mary Peach as Victoria Caldwell, Caldwells wife Barry Sullivan Colonel William Fowler, Base Commander of Carmody Air Force Base Kevin McCarthy Major General SAC Inspector General Major General John Aymes, commander of the 904th SAWs parent Air Division Colonel Joe "Smokin Joe" Garcia, Director of Maintenance, 904th SAW
*Leora Dana as Evelyn Fowler, Col Fowlers wife Robert Lansing as Senior Master Sergeant Banning, B-52 line chief
*Richard Le Pore as Staff Sergeant Kemler, boom operator on KC-135 tanker crew "Ramrod 67" Captain Linke, aircraft commander of B-52G bomber crew "Ranger 21" Colonel Ralph Josten, Director of Operations, 904th SAW Leif Erickson General Hewitt, Commander-in-Chief, Strategic Air Command (CINCSAC)
*Louise Fletcher as Mrs. Kemler, Sergeant Kemlers wife

==Allusions to actual history==
This film depicts the operations of a typical Strategic Aerospace Wing (nuclear bombers, aerial refueling aircraft and ICBMs) of the Strategic Air Command during the early 1960s. The depiction of unannounced Operation Readiness Inspections (ORI) accurately reflects events at SAC bases during this time period. When General Curtis LeMay became SAC Commander in 1948, he undertook a number of base inspections, frequently flying unannounced to a SAC base. LeMay insisted on rigorous training and very high standards of performance for all SAC personnel, be they officers, enlisted men, aircrews, mechanics, or administrative staff, and reportedly commented, "I have neither the time nor the inclination to differentiate between the incompetent and the merely unfortunate." A poor showing during one of these inspections could, and often did, result in the immediate replacement of that bases wing commander, as well as on-site demotions for poorly performing airmen (or immediate on the spot promotions for officers and airmen who performed well).

One anonymous commentator at IMDb stated that he was a member of the wing where filming took place, and vouched for the authenticity of Air Force procedures depicted in this film. Four commentators have left entries at the Turner Classic Movies database entry. Each one claims to have been in SAC during the period of history depicted in this film—and one man states that he was not only stationed at Beale AFB, but also flew the T-33 Shooting Star|T-33 "chase plane" and worked with Rod Taylor (who, as Col Farr, is depicted as flying the plane when Col Caldwell has to break off air refueling because of a broken fuel line in the cockpit of his B-52G). These men criticize the film for the following errors:
 NCOs (holding Airmen would do the hands-on work.
*The Titan I missile silos would not be within plain sight of the end of the runway, as shown in the film, but would probably be 20 to 45 miles distant. Those shown appear to have been at the Chico, CA site, which was near Beale and located on a small hill, allowing the B-52s to be higher above the terrain than they appear in the title shot.
*Firefighters would not spray carbon dioxide or any other cold aerosol on the overheated brakes of a B-52 that had to land without flaps. Doing so would cause the disk brakes to explode.
 President John F. Kennedy hangs in Colonel Caldwells office as part of a photographic depiction of the chain of command, providing an obvious time frame reference.
 456th Aerospace Wing of the Strategic Air Command.

The term "Operational Readiness Inspection" (ORI) was actual terminology in the 1960s, 1970s, 1980s and 1990s applied to exercises evaluating the combat readiness of Strategic Air Command|SAC, Tactical Air Command|TAC, USAFE and PACAF units to carry out their nuclear mission. An ORI would involve the unannounced arrival of a KC-135 with approximately 70 personnel on board who would then conduct a week-long evaluation of every aspect of that bases and wings readiness. Graham, Richard H., SR-71 Blackbird: Stories, Tales, and Legends, Zenith Press, 2002, p. 20. Preview available at: 
http://books.google.com/books?id=ZHkAJAq-2HQC&pg=PA21&lpg=PA20&ots=9BO56pqSBu&dq=gen+curtis+lemay+unannounced+base+insections&output=html_text  .  One of the TCM Database commentators vouches specifically for the ORI as depicted, and also for the Minimum Interval Take Off (MITO) sequence (in which Colonels Caldwell and Farr witness B-52Gs taking off fifteen seconds apart on average).

==Reception==
This film received relatively weak critical reviews and did poorly at the box office. After more than forty years, opinions vary widely as to the causes of the poor reception. The period in which this film was released is notable for the release, one or two years later, of a number of films that were decidedly unsympathetic to the US military (Dr. Strangelove, Fail-Safe (1964 film)|Fail-Safe, etc.). These films appeared to much greater critical acclaim and box-office reception.

This film received an Academy Award nomination for Best Sound Effects (Robert L. Bratton) in 1963.

==See also==
* List of American films of 1963
*Strategic Air Command (film)
*Bombers B-52

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 