Life (1999 film)
 
{{Infobox film
| name=Life
| image= Life dvd movie cover.jpg
| caption = Theatrical release poster
| director = Ted Demme
| producer = Brian Grazer Eddie Murphy
| writer = Robert Ramsey   Matthew Stone
| story = Eddie Murphy
| starring = Eddie Murphy Martin Lawrence Obba Babatundé Ned Beatty Bernie Mac Miguel A. Nuñez Clarence Williams III Bokeem Woodbine
| music= R. Kelly  Wyclef Jean
| editing = Jeffrey Wolf
| cinematography = Geoffrey Simpson
| studio = Imagine Entertainment
| distributor = Universal Pictures
| released =  
| runtime = 109 minutes
| country = United States
| language = English
| budget = $80 million
| gross = $73,345,029  	 
}} 1999 Cinema American comedy-drama wrongly convicted life sentence in prison. The film was the last R rated role to date for Eddie Murphy who has stuck mainly to family friendly films for the past 15 years of his career.

==Plot==
 
Elderly inmate, Willie Long (Obba Babatundé) attends the burial of two friends who recently perished in an infirmary fire in a Mississippi prison. He begins telling the two young inmates digging the graves (Heavy D and Bönz Malone) his friends life story.
 hustler and petty thief, and Claude, an honest, yet often selfish minded man, has just been accepted for a job as a bank teller at First Federal Of Manhattan. They are both at a club called Spankys when Ray picks Claude as his mark to pick-pocket. Later they both end up in the bad graces of the clubs owner Spanky (Rick James). Ray is in trouble for running numbers on Spankys territory. Claude is in trouble because he does not have any money to pay for the dinner he just ate at Spankys club since he was jacked by two men he owes money.  Ray arranges for himself and Claude to do some Smuggling|boot-legging to pay off their debt.

They head down south from New York in order to buy a carload of Mississippi hooch (alcohol). Unfortunately, before they can get back to New York, a man named Winston Hancock (Clarence Williams III), who swindles Ray in a card game, is murdered outside of a juke joint by the towns white sheriff, Warren Pike (Ned Vaughn). As Ray and Claude are walking outside talking about what happened in the club, Hancock is thrown onto Claude by a pulley of some sort. Some rednecks come up on them and realize Hancock is dead. They take Ray and Claude to the jail at gunpoint. A short time later, they go to trial, are convicted, and sentenced to life.

True to the time period and the south, Ray and Claude are sent to an infamous  ), Jangle Leg (Bernie Mac), Radio (Guy Torry), Goldmouth (Michael Taliferro), Cookie (Anthony Anderson) and Pokerface (Barry Shabaka Henley), dodging the guards Sergeant Dillard (Nick Cassavetes) and Hoppin Bob (Brent Jennings) as well as having their own friendship grow. Each inmate has his own different personality. Though Sgt. Dillard and Hoppin Bob are strict on them, they both have friendly "soft spots" for all of their inmates. Ray gets into a jam while defending Claude over a piece of cornbread that Goldmouth demands but Claude is being polite (unaware that he is taking advantage of him). Goldmouth gets angry when Ray keeps running his mouth and says that hell take his instead of Claudes. Ray threatens that if he takes his cornbread it will be "consequences and repercussions" (which leads to a fight and Goldmouth wins). After the fight, Ray and Goldmouth become friends.

Sometime later, one night, Ray explains his dream of having his own nightclub called "The Boom Boom Room". A dream sequence features Biscuit imagining himself as a female singer, Jangle Leg and Radio are in the band, Cookie is a hungry restaurant patron, Pokerface is a lucky gambler, Goldmouth is the guard at the door and Claude is a mistreated waiter. The sequence ends with Hoppin Bob as a cop who demands everyone to leave the club (but it was actually Hoppin Bob interrupting by telling the inmates to go to bed).
 Tallahatchie before being captured; they are sentenced to a week in solitary confinement.
 mute inmate Negro League scout who states that he can get him out of prison if will play baseball. Ray and Claude, seeing a shot at freedom, tell the scout to put a word in for them as well (as they relate to Cant-Get-Right in that they can coax him best to play) but to no avail. During a dance social, a prisoner named Biscuit confides to Ray that he is going to be released; however, Biscuit did not want to return to his family, being a homosexual man, so he commits suicide by deliberately running across the "firing line", getting himself shot. Jangle Leg (Bernie Mac) is allowed to cross the line to retrieve his partners body. After Cant-Get-Right is released to play baseball for the Pittsburgh Crawfords, Ray devises an escape but Claude wants no part of it. Claude is upset with the fact that Cant-Get-Right was released without them, and this leads to an argument that results in Ray and Claude going their separate ways. Ray and Claude dont speak to one another for 28 years (1944-1972). As the years went by, each and every one of Claude and Rays friends aged and passed on, leaving them with their only surviving friend Willie.   
 Malcolm X The African-American Civil Rights Movement, the Apollo 11 moon landings, and Muhammad Alis quote. In 1972, Ray and Claude are now elderly. Willie is too old and weak to walk and he is now in a wheelchair. Hoppin Bob passed on years ago and Sgt. Dillard still runs the camp, still annoying Ray and Claude. One day Ray and Claude were sent to live and work at Superintendent Dexter Wilkins(Ned Beatty) mansion. Claude forms a friendship with Wilkins, and is entrusted to drive and pick up the new superintendent (R. Lee Ermey), who is be none other than Sheriff Pike, the man who framed them 40 years earlier.
 heart attack in his bathroom before he can do so.

In 1997 (present day), Ray and Claude are now very elderly and living in the prisons infirmary. Claude tells Ray of yet another plan he has devised, but Ray is skeptical. On that same night, the infirmary catches fire and everyone makes it out safely except for Ray and Claude.

Willie concludes the tale by outlining Claude’s plan. The two bodies being buried were taken from the morgue; Ray and Claude had planned to escape during the fire by hiding on the departing fire trucks. When the workers ask why the plan didnt work, Willie tells them that he "never said it didnt work". Willie wheels away, as the inmates realize that the bodies they buried are not Ray and Claude.

Ray and Claude are back in New York, at a New York Yankees baseball game. The film concludes by revealing that the bad-luck buddies are again on good terms, living together in Harlem.

==Cast==
 
*Eddie Murphy as Rayford "Ray" Gibson
*Martin Lawrence as Claude Banks
*Obba Babatundé as Willie Long
*Ned Beatty as Dexter Wilkins
*Nick Cassavetes as Sergeant Dillard
*Bernie Mac as Jangle Leg
*Miguel A. Núñez Jr. as Biscuit
*Bokeem Woodbine as Cant Get Right
*Brent Jennings as Hoppin Bob
*Anthony Anderson as Cookie
*Barry Shabaka Henley as Pokerface
*Michael Taliferro as Goldmouth
*Guy Torry as Radio
*Sanaa Lathan as Daisy
*ONeal Compton as Superintendent Abernathy
*Noah Emmerich as Stan Blocker
*Rick James as Spanky
*R. Lee Ermey as Older Sheriff Pike
*Ned Vaughn as Younger Sheriff Pike
*Clarence Williams III as Winston Hancock
*Heavy D as Jake
*Kenn Whitaker as Issac
*Bonz Malone as Leon
*Lisa Nicole Carson as Sylvia
*Poppy Montgomery as Older Mae Rose
 

==Box office==
Life was released on April 13, 1999 in North America. The film grossed $73,345,029 worldwide against an $80 million budget, making it a financial disappointment.  

==Reception==
The film has received mixed reviews and currently has a 50% approval rating on Rotten Tomatoes. 

==Location== Rockwell Defense Plant in California. 

==Soundtrack==
 
A soundtrack containing hip hop and R&B music was released on March 16, 1999 on Rock Land/Interscope Records. It peaked at 10 on the Billboard 200|Billboard 200 and 2 on the Top R&B/Hip-Hop Albums and was certified platinum with over 1 million copies sold on June 18, 1999.

==Awards and nominations== Academy Award Best Makeup (2000)
* NAACP Image Award
** nominated for Outstanding Motion Picture (2000)
* BMI Film & TV Awards
** (won) for Most Performed Song from a Film (2000)
* Blockbuster Entertainment Awards
** nominated with Eddie Murphy for Favorite Comedy Team (2000) for the movie
** nominated for Favorite Song from a Movie (Fortunate)

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 