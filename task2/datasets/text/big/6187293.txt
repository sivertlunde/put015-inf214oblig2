Sennin Buraku
{{Infobox animanga/Header
| name            = Sennin Buraku
| image           =  
| caption         = Sennin Buraku comic strip by Kō Kojima
| ja_kanji        = 仙人部落
| ja_romaji       = 
| genre           = Romantic comedy, Satire, Sex comedy   
}}
{{Infobox animanga/Print
| type            = manga
| author          = Kō Kojima
| publisher       = Tokuma Shoten
| demographic     = 
| magazine        = Weekly Asahi Geinō
| first           = October 1956
| last            = August 2014
| volumes         = 
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = Fūryū Kokkei-tan: Sennin Buraku
| director        = Morihei Magatani
| producer        = 
| writer          = 
| music           = 
| studio          = Shintoho
| released        = February 28, 1961
| runtime         = 83 minutes
}}
{{Infobox animanga/Video
| type            = tv series
| director        = Shigeharu Kaneko
| producer        = 
| writer          = 
| music           = 
| studio          = Eiken (studio)|Tele-Cartoon Japan
| network         = Fuji TV
| first           = September 4, 1963
| last            = February 23, 1964
| episodes        = 23
| episode_list    = 
}}
 

  is a manga series by Kō Kojima which runs in the adult magazine Weekly Asahi Geinō, published by Tokuma Shoten in Japan. It is the longest running comic with only one artist, being published weekly since October 1956, and the longest-running strip ever in Japan.    By contrast, Golgo 13 is the longest running manga to be serialized in a dedicated manga magazine with Doraemon the second longest, and Kochira Katsushika-ku Kameari Kōen-mae Hashutsujo (Kochi-Kame) the third longest (Asahi Geino is not a dedicated manga magazine). While Sennin Buraku has been running for more years than Peanuts, Charles M. Schulzs strip has more "episodes" as it ran daily rather than weekly.  The story was a romantic comedy taking place in historical China, and it was quite risqué for its time.  The characters were very traditionally dressed (e.g. all wearing hanfu).  Although the anime is very hard to find, it has been rerun on Japanese television, its intro and outro has appeared on DVD, and an episode has resurfaced on Nico Nico Douga.

Sennin Buraku was the first late night anime, broadcast shortly before midnight on Fuji TV from September 4, 1963 to February 23, 1964.  This was the first anime series produced by Eiken (studio)|Tele-Cartoon Japan, and a page exists on their website about it.    The series was in black and white and ran for 23 episodes. A live action movie was released in 1961, titled  .      

With the August 7, 2014 issue, it was announced that the series would be placed on hiatus. The death of the artist on April 14, 2015 puts any future chapters of Sennin Buraku in doubt. 

==Plot summary== disciple Zhi pleasures of the flesh.  He has fallen for three pretty sisters who live nearby, much to Lao Shis annoyance.

==Manga==
While the manga has run in Weekly Asahi Geinō for over fifty years, there have been no translations of it.

==Live action movie==
The 83-minute live action movie was titled Fūryū Kokkei-tan: Sennin Buraku, and was released in theaters by Shintoho on 1961-02-08.

===Cast===
*Akiko Matsuyama: Mayumi Ōzora 
*Tenpei Naiki: Yōichi Numata 
*Yōko Kondō: Mako Sanjō 
*Doctor: Bokuzen Hidari  Akihiro Maruyama 
*Daikichi Narayama: Bunta Sugawara 

===Staff===
*Director: Morihei Magatani
*Planning: Mitsuo Nakatsuka 
*Screenplay: Isao Matsumoto 
*Cinematographer: Shigenobu Yoshida 
*Art Director: Haruyasu Kurosawa 
*Music: Keitarō Miho 

==Anime series==
Each episode of the anime series was 15 minutes long. The first eight episodes were broadcast from 23:40 to 23:55 on Wednesday nights on Fuji TV following the world news, and episodes nine through 23 were broadcast from 23:30 to 23:45 on Monday nights.

The opening theme song, Sennin Buraku no Thema, was sung by Three Graces, arranged by Tōru Kino  and the lyrics were written by Takeo Yamashita .

===Cast===
*Sennin: Hyakushō Sanyūtei (2nd)|Hyakushō Sanyūtei  Ichikawa Danjūrō
*Yoshiaki Hanayagi 
*Tomoko Kokai 
*Ichirō Nagai

===Staff===
*Director: Shigeharu Kaneko 
*Screenplay: Akira Hayasaka , Tōru Kino TCJ

 Sources:     

==References==
 

==External links==
*  
*   in Animemorial (contains episodes list + screenshot)
*  
*  

 
 
 
 
 
 
 
 
 
 
 