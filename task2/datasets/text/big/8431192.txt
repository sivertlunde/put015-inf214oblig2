The Prophecy: Uprising
 
{{Infobox film
| name            = The Prophecy: Uprising
| image           = Prophecy_uprising.jpg
| caption         = 
| director        = Joel Soisson
| producer        = Ron Schmidt
| writer          = John Sullivan, Joel Soisson
| starring        = Sean Pertwee John Light Kari Wührer Jason London Doug Bradley 
| music           = Joseph LoDuca
| cinematography  = 
| editing         = Kirk Morri
| distributor     = Dimension Films
| released        =  
| runtime         = 88 minutes
| language        = English
| budget          = 
}}
The Prophecy: Uprising is a 2005 fantasy horror-thriller film and the fourth motion picture (of five) in The Prophecy (franchise)|The Prophecy series. This chapter does not feature series regular Christopher Walken, instead starring Doug Bradley, British actor Sean Pertwee, and frequent horror-film actor Kari Wührer in the lead roles. 

This installment continues with the tale of war between angels. In the first war between the angels, Lucifer was cast out of heaven and became the creator of hell, he had some angels following him. Soon another war between angels started, this time there are two camps - one faction who hate humans (and call them monkeys) and want them to fall from Gods grace, and the second group who help humans.

==Plot==
The movie begins with the story of a woman named Allison (Kari Wührer) who has come into possession of The Lexicon, a mysterious book of prophecies that writes itself. This book contains a 23rd chapter of the book of Revelations, which is still not complete. The last chapter depicts the end of the war of angels. One of the angels who fell with Satan, Belial (now a demon), wants this book.  

Allison finds the mysterious book from a priest who died reading it. Listening to the voices in her head that guide her actions throughout the movie, she takes the book for safe keeping.  While searching for Allison and the book, and to avoid detection, Belial murders people and takes their form.   
 John Light) seeks the help of a cop who had provided information to secret police about his parents. His parents and baby sister were brought to one of the secret police headquarters and tortured. His baby sister gets hurt and is given up for adoption. It is revealed that she is Allison. Satan brings the cop to the house which was the site of the inhumane tortures, making it his domain. Allison, with guidance of the voices in her head, reaches the same place, followed by Belial. It is the only place where Belial cannot hurt Allison. It is a place of evil, which makes it Satans area, and he offers Allison and her brother protection. 

Here, the cop confesses for his sins and seeks forgiveness but is rebuked by his sister. It is here that real motives are revealed. Belial, who was loyal to Satan, now wants to get some advantage from war of angels. He is frustrated by the inertness of Satan and now wants an aggressive hell. Satan, who had initially opposed God for his love for humans, helps humans to fight Belial. He does not want another hell. As per him, only one hell is enough for the world. He helps the cop and Allison to kill Belial and then he absorbs Belials soul. 

The movie concludes at dawn, when Satan tells Allison that for the present the war of angels is over, but will not be for long. Showing her glimpses of her future, he advises her to keep the book safe.

==Cast==

*John Light - John Riegert / Satan
*Sean Pertwee - Dani Simionescu
*Kari Wuhrer - Allison
*Jason Scott Lee - Dylan
*Jason London - Simon
*Doug Bradley - Laurel
*Georgina Rylance - Calra
*Stephen Billington - Ion
*Dan Chiriac - Serban
*Boris Petroff - Father Constantin
*Alin Cristea - Cantor

==Development==
The Prophecy: Uprising and   were filmed simultaneously. Furthermore, these films are the first to not have Christopher Walken and Steve Hytner reprise their roles as the Archangel Gabriel and the coroner Joseph, respectively.

==Home video==
Several of the The Prophecy sequel films including this film have been released direct-to-DVD.

==Sequels==
Follows:
  (2000)

Followed by:
  (2005).

==External links==
 
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 