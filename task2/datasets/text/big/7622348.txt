Tunisian Victory
{{Infobox Film name      =Tunisian Victory caption   = image	   =Tunisian Victory FilmPoster.jpeg directors =Frank Hugh Stewart  Roy Boulting  John Huston   Anthony Veiller  producer  = writer    = starring  =Leo Genn (narrator) Burgess Meredith Bernard Miles music     =William Alwyn Dimitri Tiomkin cinematography = editing   = distributor =Butchers MGM (US) released  =March 16, 1944 runtime   =75 min budget    = country   =    language  =English
}} American propaganda film about the victories in the North Africa Campaign.
 Operation Torch / Operation Acrobat to the liberation of Tunis. Interspersed in the pure documentary format are the narrative voices of supposed American and British soldiers (voiced by Burgess Meredith and Bernard Miles respectively), recounting their experience in the campaign. The British and American talk separately until the end of the film when they have a dialogue, agree to co-operate after the end of the war, with the other Allied nations to create a more just and peaceful post-war order.

  (right)  confers with Captain Roy Boulting of the British Army Film Unit on the editing of the film in February 1944]] Hugh Stewart and Roy Boulting.

== References ==
 

== See also ==
* List of Allied Propaganda Films of World War 2
*The True Glory (1945)
* Army Film and Photographic Unit

== External links ==
*  
*  
* "Tunisian Victory" and Anglo-American Film Propaganda in World War II http://www.encyclopedia.com/doc/1G1-18516915.html

 
 
 
 
 
 
 
 

 