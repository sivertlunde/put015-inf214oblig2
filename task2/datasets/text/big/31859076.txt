Old Mother Riley at Home
{{Infobox film
| name           = Old Mother Riley at Home
| image          = "Old_Mother_Riley_at_Home"_(1945).jpg
| image_size     =
| caption        = 
| director       = Oswald Mitchell
| producer       = Louis H. Jackson
| writer         = George A. Cooper Oswald Mitchell 
| story          = Joan Butler Ralph Temple
| narrator       = 
| starring       = Arthur Lucan   Kitty McShane   Freddie Forbes 
| music          = Percival Mackey James Wilson
| editing        = Douglas Myers
| studio         = British National Films
| distributor    = Anglo-American Film Corporation
| released       = 24 December 1945 (UK)
| runtime        = 76 minutes	
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}Old Mother Riley at Home is a 1945 British comedy film directed by Oswald Mitchell and starring Arthur Lucan, Kitty McShane and Freddie Forbes.  It is the 11th film in the long running Old Mother Riley series.

==Plot==
Mother Rileys daughter Kitty has run off with her new and (so says Mother Riley), "no good" boyfriend. With the aid of Kittys true love Dan, Mother Riley tracks the runaways, and discovers them in a gambling den. 

==Cast==
* Arthur Lucan - Mrs Riley
* Kitty McShane - Kitty Riley
* Freddie Forbes - Mr Bumpton
* Richard George - Dan
* Willer Neal - Bill
* Wally Patch - Bouncer
* Kenneth Warrington - Boss
* Angela Barrie - Duchess
* Janet Morriso - Mary
* Elsie Wagstaff - Mrs. Ginochie
* Henry B. Longhurst - Commissionaire

==Critical reception==
TV Guide called the film, "one of the weaker series entries."  

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 
 