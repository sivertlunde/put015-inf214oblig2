Mexican Spitfire Out West
{{Infobox film
| name           = Mexican Spitfire Out West
| image          = Mexican Spitfire Out West poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Leslie Goodwins
| producer       = Lee S. Marcus Cliff Reid
| screenplay     = Charles E. Roberts Jack Townley 
| story          = Charles E. Roberts  Donald Woods Elisabeth Risdon Cecil Kellaway
| music          = Roy Webb
| cinematography = Jack MacKenzie
| editing        = Desmond Marquette 	
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Donald Woods, Elisabeth Risdon and Cecil Kellaway. The film was released on November 29, 1940, by RKO Pictures.   

==Plot==
 

== Cast ==
*Lupe Vélez as Carmelita Lindsay
*Leon Errol as Uncle Matt Lindsay / Lord Basil Epping Donald Woods as Dennis Denny Lindsay
*Elisabeth Risdon as Aunt Della Lindsay
*Cecil Kellaway as Mr. Chumley
*Linda Hayes as Elizabeth Price
*Lydia Bilbrook as Lady Ada Epping Charles Coleman as Ponsby, the Butler
*Charles Quigley as Mr. Roberts Eddie Dunn as Mr. Skinner
*Grant Withers as Withers Tom Kennedy as Taxi Driver

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 