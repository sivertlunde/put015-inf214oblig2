Kolberg (film)
{{Infobox film
| name           = Kolberg
| image          = 
| image_size     = 
| caption        = 
| director       = Veit Harlan
| producer       = Veit Harlan
| writer         = Veit Harlan Alfred Braun Joseph Goebbels  
| narrator       = 
| starring       = Kristina Söderbaum Heinrich George Paul Wegener Horst Caspar Gustav Diessl Otto Wernicke Kurt Meisel
| music          = Norbert Schultze
| cinematography = Bruno Mondi
| editing        = Wolfgang Schleif Ufa Filmkunst GmbH (Herstellungsgruppe Veit Harlan)
| distributor    = Deutsche Filmvertriebs GmbH
| released       =  
| runtime        = 110 minutes
| country        = Nazi Germany
| language       = German
| budget         = 
| gross          = 
}}
 German historical film directed by Veit Harlan. One of the last films of the Third Reich, it was intended as a Nazi propaganda piece to shore up the will of the German population to resist the Allies.
 Kolberg in defence of French troops between April and July 1807, during the Napoleonic Wars. In reality, the French captured the town; in the film, the French abandon the siege.

== Plot == War of Breslau through August von Francis II of Austria, whom the script has Gneisenau call "an Emperor who abandoned the German people in their hour of need".

The scene set, the film moves to 1806 and a Kolberg not yet affected by war, where the inhabitants are shown enjoying life, and the towns leaders, Nettelbeck chief among them, discuss Napoleon I of France|Napoleons proclamations, and what it will mean to them. Some see the French victories as a good thing, some wonder whether to leave.  Nettelbeck alone is set on resisting the French. The film continues in this vein, with Nettelbeck struggling against cowardice, lethargy and the old-fashioned ideas of the garrison commander, to defend his city against the approaching French. Nettelbeck creates a citizen militia, in spite of the best efforts of the regular army, has supplies collected, and strongly opposes the idea of surrender.

Finally, having been threatened with execution, and convinced that Kolberg can only be saved if a great leader can be found, Nettelbeck sends Maria on the dangerous journey to Königsberg whither the Court of Prussia has retreated, to meet with the King and with Queen Louise of Mecklenburg-Strelitz|Louise, who was described by Napoleon as "the only man in Prussia". Marias journey leads to the energetic and charismatic Gneisenau being sent to Kolberg. After an initial confrontation with Nettelbeck, in order to show that there is only one leader in Kolberg, and that Gneisenau is that leader, the two work together with the army and the citizens to save the city from the French. After Kolberg is (unhistorically) saved, the film returns to 1813 after the Convention of Tauroggen, a time when Napoleon was defeated in Russia, and Prussian leaders wonder whether it is time to turn openly against him. Frederick William is convinced by Gneisenau to do so, and sits down to write the proclamation An Mein Volk ("To my People") announcing the War of Liberation.

==Cast==
* Kristina Söderbaum as Maria
* Heinrich George as Joachim Nettelbeck
* Paul Wegener as General Loucadou
* Horst Caspar as August Neidhardt von Gneisenau
* Gustav Diessl as Ferdinand von Schill
* Otto Wernicke as Farmer Werner
* Kurt Meisel as Claus Claus Clausen as Frederick William III of Prussia
* Irene von Meyendorff as Louise of Mecklenburg-Strelitz
* Jaspar von Oertzen as Prince Louis Ferdinand of Prussia (1772–1806)
* Jakob Tiedtke as Reeder
* Paul Bildt as Rektor

==Production==
The film is based on the autobiography of Joachim Nettelbeck, mayor of Kołobrzeg|Kolberg. Joseph Goebbels explicitly ordered the use of the historical events for a film, which he regarded as highly suitable for the circumstances Germany faced. 

Kolberg entered production in 1943, and was made in Agfacolor with high production values. At a cost of more than eight million marks, it was the most expensive German film of the second World War, with the actual cost suppressed to avoid public reaction.  At a time when the war was turning against Germany, thousands of 
soldiers were used in the film. 

Principal cinematography took place from 22 October 1943 to August 1944. The exteriors were shot in Kolberg and environs, Königsberg, Berlin and environs, Seeburg and Neustettin.   

To film scenes with snow during summer, 100 railway wagons brought salt to the set in Pomerania. The film was finally completed at the Babelsberg Studios at Potsdam while the town and nearby Berlin were being steadily bombed by the Allies. Two extras were killed during the making of the film when an explosive charge went off too early.

==Release==
The film opened on 30 January 1945 in a temporary cinema (U.T. Alexanderplatz) and at Tauentzien-Palast in Berlin, and ran under constant threat of air raids until the fall of Berlin in May. Simultaneously with the opening in Berlin it was shown to the crew of the naval base at La Rochelle at the Théâtre de la Ville.  It was also screened in the Reich chancellery after the broadcast of Hitlers last radio address on 30 January. One of the last films of the Third Reich, it never went into general release. 
 full siege (sometimes called the second Siege, or second Battle, of Kolberg), with around 70,000 trapped civilians and military. House-to-house fighting caused devastation. Kolberg fell to Soviet and Polish forces on 18 March. Many civilians escaped by sea, and those who survived were permanently expelled along with all Germans in east Pomerania. The ruined town of Kolberg became part of Poland and is now known as Kołobrzeg.

The film was re-released in 1965, with an attached documentary, and is now available on DVD.

In Germany, it is a Vorbehaltsfilm, available for screening from the holder of the rights (Friedrich Wilhelm Murnau Foundation) only under special conditions.   

== See also ==
*List of German films 1933-1945
*Nazism and cinema

==References==
 

==External links==
*  
*  
*  

{{Externalimage
|align=left
|width=300px
|image1=  
|image2=  
|image3= (the statists for these scenes came directly from the battlefields of the ongoing Second World War) 
}}

 
 
 
 
 
 
 
 
 
 
 