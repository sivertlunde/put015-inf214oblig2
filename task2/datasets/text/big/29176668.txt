From Prada to Nada
{{Infobox film
| name           = From Prada to Nada
| image          = From Prada to Nada Poster.jpg
| caption        = Theatrical poster
| director       = Angel Gracia
| producer       = Gary Gilbert Linda McDonough Gigi Pritzker Chris Ranta
| screenplay     = Luis Alfaro Craig Fernandez Fina Torres
| starring       = Camilla Belle Alexa Vega Wilmer Valderrama Nicholas DAgosto Kuno Becker Adriana Barraza
| music          = Heitor Pereira
| cinematography = Héctor Ortega
| editing        = Brad McLaughlin
| studio         = Televisa Odd Lot Entertainment Lions Gate Pantelion Films
| runtime        = 107 minutes
| released       =  
| country        = United States
| language       = English, Spanish
| budget         =
| gross          = $3,836,357   
}}
From Prada to Nada is an American romantic comedy film directed by Angel Gracia and produced by Gary Gilbert, Linda McDonough, Gigi Pritzker and Chris Ranta. The plot was conceived from Jane Austens Sense and Sensibility.    The screen play was adapted by Luis Alfaro, Craig Fernandez and Fina Torres to be a Latino version of the English novel, where two spoiled sisters who have been left penniless after their fathers sudden death are forced to move in with their estranged aunt in East Los Angeles. 
 limited theatrical release in the United States on January 28, 2011. In the United States, this film grossed $3 million theatrically;  the box office result met Pantelions expectation.  

==Plot== Nora studying Mary shopping. They head to their home, Casa Bonita, for their fathers birthday. While dancing with their father, he falls and dies.

At the funeral they meet Gabe Dominguez, a half brother who resulted from an affair their father had years ago. At the reading of their fathers will, they discover that they are bankrupt. Nora and Mary sell their house to Gabe, who lets them live with him and his wife, Olivia. They meet Olivias brother, Edward Ferrars|Edward, when he visits for lunch. During lunch, Olivia tells the sisters that they must move out of the house. Before they leave, their maid gives Nora a box left by their father.

The two sisters go to East L.A. to live with their maternal aunt, Aurelia, where they are introduced to Bruno, their aunts neighbor. Later, Nora opens the box from her father and finds letters addressed to Gabe, which his mother returned unopened. Nora decides to quit law school and get a job and tells Mary to finish college. Their aunt sells Marys car and Prada purse (from Prada to nada). Edward arrives and gives Mary and Nora stuff from their old home, offering Nora a job in his law office - which she declines. Nora finds a job on her own, and on the bus ride to work she meets a woman who has been fired from her job as a maid. When Nora arrives at her new job she learns that her boss is Edward. Nora and Edward work together on the case for the cleaning ladies, winning a judgment when they discover that there has been a fraud with their payroll.

Mary returns to college, where she meets and flirts with teaching assistant Rodrigo. She gets a ride home from him, directing him to her childhood mansion rather than her aunts house in East L.A. They later share a kiss over lunch and, later still she admits that she no longer lives in the mansion, suggesting to Rodrigo that he buy her old house.

Mary visits Brunos house and make a deal that, if Bruno can make the aunts backyard look good for a party dedicated to Mexican Independence Day, he can have a dance with Mary. On the day of the party, Bruno watches how Mary dances with Rodrigo. Bruno later declines Marys offer of a dance to complete the decoration agreement. Edward later arrives at the party, finding Nora drunk on tequila. They go into the kitchen where they kiss. After that, Nora feels bad because she is not supposed to act like that and tells Edward that she needs to focus on her career.

The next day Nora finds out that she has received a promotion, that involves a transfer to another department, away from Edward. When Rodrigo drops Mary at her aunts home after she spends the night with him, he tells her he is going to Mexico for a few weeks. When Nora asks Mary if she loves Rodrigo, Mary tells her that she just wants to go back home and that Rodrigo can make that happen, for which Nora calls her a whore and Mary retorts that she would rather be that than a spinster like Nora. Mary and Nora start to ignore each other. Nora quits Edwards law firm and opens a free legal service from her aunts home.

Upon Gabes insistence, Olivia invites the sisters to attend Edwards engagement party at their former home. When they arrive at the party they are welcomed lovingly by their former house staff. Mary and Gabe go their fathers office - the one part of the house that Gabe wouldnt let Olivia remodel. Mary gives Gabe the letters from their father and he tearfully realises that their father wanted to be in his life. Meanwhile, Nora tells Edward why she doesnt count on personal relationships, due to losing both her parents and that her rejection of Edward wasnt because she didnt love him. Mary sees Rodrigo at the party with another woman who turns out to be his wife. Rodrigo tells Mary that he did, indeed, take her recommendation to buy the house. Heartbroken, Mary leaves the party and tries to go home but has a traffic accident.

Gabe comes over to visit his sisters in the hospital and reveals without sadness that he and Olivia split up. Mary returns home in a wheelchair and neck brace. The next day, when she goes over to Brunos house, Mary discovers detailed designs for the wheelchair ramp he made for her, her old car mirror that he fixed and realises Bruno showed more genuine care for her than Rodrigo ever did. When she finds him in the back yard teaching kids how to paint, the two admit their feelings for each other and share a kiss. Edward arrives with another removal van full of furniture. He reveals to Nora that they bought the house across the street from her aunt. He presents her with the front door key, attached to an engagement ring and declares his love for her. The film ends with all the family and friends celebrating at Nora and Edwards street party wedding.

==Cast==
* Camilla Belle as Nora Dominguez &ndash; the elder, serious, sister. She is a law student who does not want to put relationships above her career. She starts to fall for Olivias brother, Edward and marries him at the end of the film. Counterpart to Sense and Sensibilitys character Elinor Dashwood. sex with wealthy Rodrigo to get back to Beverly Hills, then finds out he is married. She then falls for sensible artist Bruno, her aunts neighbor. Counterpart to Marianne Dashwood. Colonel Brandon.
* Nicholas DAgosto as Edward &ndash; the brother of Olivia who falls for Nora and becomes her husband. Counterpart to Edward Ferrars.
* Kuno Becker as Rodrigo Fuentes &ndash; the married love interest of Mary. At first they date until Mary finds out he is married. Counterpart to John Willoughby.
* Adriana Barraza as Aureliae Dominguez &ndash; the aunt of Nora and Mary. Counterpart to Mrs. Jennings. Fanny Dashwood. Henry Dashwood. John Dashwood.
* Catalina López as Trinita
* Luis Rosales as Juan

==Release==
From Prada to Nada was released on Blu-ray Disc|Blu-ray and DVD May 3, 2011.

===Accolades===
{| class="wikitable"
|-
! Award !! Category !! Recipient(s) !! Result
|- Alexa Vega ||  
|}

==References==
 

==External links==
 
*  
*  
*  
*  
*   at Metacritic
*  

 

 
 
 
 
 
 
 
 
 
 
 