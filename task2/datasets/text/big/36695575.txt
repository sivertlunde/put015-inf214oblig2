Morgan (film)
 
{{Infobox film
| name = Morgan
| image = Morgan-film-by-michael-ackers.jpg
| caption = Theatrical release poster
| director = Michael D. Akers
| writer = Michael D. Akers  Sandon Berg
| starring = Leo Minaya   Jack Kesy 
| producer = Michael D. Akers   Sandon Berg   Israel Ehrisman
| music = Ryan Rapsys
| cinematography =  Chris Brown
| editing = Michael D. Akers
| distributor = United Gay Network
| released =  
| runtime = 
| country = United States
| language = English
| budget = 
| gross = 
}}
Morgan is a 2012 film directed by Michael Akers and his fourth long feature film. The film produced by Michael D. Akers, Sandon Berg and Israel Ehrisman and starred Leo Minaya as Morgan Oliver and Jack Kesy as Dean Kagen.   

==Plot==
A gay and paralyzed young athlete attempts to live his life in a situation far from desirable. After an accident leaves him paralyzed from the waist down, Morgan Oliver (played by Leo Minaya), is first seen wallowing in a state of depression, drowning his sorrows in beer as he watches bicycle racing (the sport that at once defined his sense of purpose and drove him to his catalytic accident) on television. He meets Dean Kagan (played by Jack Kesy) who helps him through the way and a romantic relationship develops between the two. Once Morgan knows about the New York Haven Cycle Race, he decides to take part in the race with the help of Kagan and sponsorship from Tread Bike Shop. 

==Cast==
*Leo Minaya as Morgan Oliver
*Jack Kesy as Dean Kagen
*Ben Budd as Wesley
*Theodore Bouloukos as Dr. Thomas
*Darra Boyd as Lane Williams
*Madalyn McKay as Peg Oliver

==Production== paraplegic while casting for an earlier film Phoenix. A young, handsome wheelchair-using paraplegic actor had submitted his portfolio for a possible role in a film. Although the screenplay for Phoenix had been written and no role was envisioned for a paraplegic acting role, they were intrigued enough by him personally, they decided to write a screenplay around a paraplegic athlete.    After many interviews with other wheelchair-using gay men, their collection of stories became the basis for the film Morgan.

One odd thing was that the race was supposed to take place during Memorial Day. However, the filming was clearly done during the winter time. There were no flowers in bloom, no blue skies, and the trees were missing all of their leaves. 

==Reception==
One film reviewer wrote: "Morgan is not a sophisticated film, but it is a very human one. Morgan has an abundance of heart and speaks to the travails of the disabled in a way that is rarely addressed in cinema, queer or otherwise. Well Done."    Another reviewer  wrote, "The cringe-inducing acting coupled with a bizarrely written script dont make Morgan a story of triumph. When you think the film starts to take itself seriously, you see an actor do something that no rational human would do in this circumstance. Method acting would have been welcomed because the amount of fake crying done in this film made me want to turn off the television."

==Soundtrack==
Music is by Ryan Rapsys. The film also includes original music by
*Diamond Underground
*Molly Mguire
*Ben Darwish
*David Raleigh
*Miles the Band
*Nicholas Wells

==Screenings ==
The film was screened at many festivals including
*Official selection at Outfest, Los Angeles
*Official selection at Cleveland International Film Festival
*Official selection at Frameline, San Francisco
*Kansas City Gay & Lesbian Film Festival
*North Carolina Gay & Lesbian Film Festival
*Reeling Chicago Gay & Lesbian Film Festival
*Sacramento International Gay & Lesbian Film Festival
*San Diego Gay & Lesbian Film Festival

==Awards==
"Audience Award" at:
*Sacramento International Gay & Lesbian Film Festival
*San Diego Gay & Lesbian Film Festival
 
"Jury Prize" at:
*QCinema, Ft. Worth Gay & Lesbian Film Festival
*Kansas City Gay & Lesbian Film Festival
*Reeling, Chicago Gay & Lesbian Film Festival
*Tucson "Out in the Desert" Film Festival

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 