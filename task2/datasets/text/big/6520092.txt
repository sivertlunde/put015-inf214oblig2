Dreadnaught (film)
 
 
{{Infobox Film
| name           = Dreadnaught 
| image          = 
| image_size     = 
| caption        = 
| director       = Yuen Woo-ping
| producer       = 
| writer         = Wong Jing 
| narrator       = 
| starring       = Yuen Biao   Bryan Leung   Kwan Tak-hing 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 90 minutes
| country        = 
| language       = Cantonese
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}} 1981 Hong martial arts action film directed by Yuen Woo-ping and starring Yuen Biao, Bryan Leung, and Kwan Tak-hing as Wong Fei Hung.

== Plot ==
A short-tempered, violent criminal named the "White Tiger" is on the run from the police and joins a theater troupe to hide out, killing anyone who angers him or who suspects his identity. One person he unsuccessfully tries to kill several times is a cowardly laundry man named "Mousy" who manages to escape by fleeing. When a very close friend of Mousys is killed by the White Tiger, Mousy overcomes his cowardliness enough to seek revenge. 

In a scene early on in the movie Mousy is washing the laundry with his bossy sister.  After complaining about the repetitiveness of laundry work, his sister scolds him and demands he wash the clothes in the "family way."  This leads to a scene with Yuen Biaos character flipping the clothes around with his hands and wringing them out with powerful squeezing from this index finger and middle finger.  These abilities turn out to be related to kung-fu methods, as Mousy eventually uses the same laundry method to defeat "White Tiger."

==Cast==
*Yuen Biao - Mousy
*Bryan Leung - Leung Foon
*Yuen Shun Yee - White Tiger
*Kwan Tak-hing - Wong Fei Hung
*Lily Li Li - Mousys sister 
*Fan Mei-Sheng - Marshal Pao 
*Brandy Yuen - Marshals assistant
*Fung Hak-On - Demon Tailor
*Danny Chow - Gorgeous Koon
*Yuen Woo Ping - extra

== External links ==
*   at Hong Kong Cinemagic
*  
*  

 

 
 
 
 
 
 
 
 
 

 
 