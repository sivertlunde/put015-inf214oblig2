Darkon (film)
{{Infobox film
| name           = Darkon
| image          = Darkon poster.jpg
| caption        = Promotional poster for Darkon
| director       = Luke Meyer, Andrew Neel
| producer       = Tom Davis Ethan Palmer Christopher Kikis Thoma Kikis Nicholas Levis Domenic Romano (associate producer) Cherise Wolas Alan Zelenetz
| writer         =  
| starring       = Skip Lipman Daniel McCarthur Rebecca Thurmond Kenyon Wells Andrew Mattingly
| music          = Jonah Rapino
| cinematography = Karl F. Schroder Hillary Spera
| editing        = Brad Turner
| studio         = SeeThink Films Ovie Entertainment
| distributor    = Independent Film Channel  (TV) 
| released       =  
| runtime        = 89 Minutes
| country        = United States English
| budget         =
}}
Darkon is an award-winning feature-length documentary film that follows the real-life adventures of the Darkon Wargaming Club in Baltimore, Maryland, a group of fantasy LARP|live-action role-playing (LARP) gamers. The film was directed by Andrew Neel and Luke Meyer.
 South By Southwest (SXSW) Film Festival in Austin, Texas. Darkon is an official selection playing at Hot Docs, Maryland Film Festival, Silverdocs, LA Film Festival, Britdoc and Melbourne International Film Festival.

The film was produced by Ovie Entertainment and SeeThink Films.

John Hodgman was hired to write a scripted film adaptation of the documentary.       However plans fell through, but an excerpt of the unproduced screenplay was read on his podcast Judge John Hodgman. 

== Reception ==
Darkon was received extremely well by critics. The film has an 88% rating on Rotten Tomatoes, based on the reviews of 18 critics. 

== References ==
 

==External links==
*  
* 
* 

 
 
 
 
 
 
 
 
 


 