What Money Can't Buy (film)
{{Infobox film
| name           = What Money Cant Buy
| image          = 
| alt            = 
| caption        = 
| director       = Lou Tellegen
| producer       = Jesse L. Lasky
| screenplay     = George Broadhurst Beulah Marie Dix
| starring       = Jack Pickford Louise Huff Theodore Roberts Hobart Bosworth Raymond Hatton James Cruze
| music          = 
| cinematography = Paul P. Perry 
| editing        = 
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Lou Tellegen and written by George Broadhurst and Beulah Marie Dix. The film stars Jack Pickford, Louise Huff, Theodore Roberts, Hobart Bosworth, Raymond Hatton and James Cruze. The film was released on July 16, 1917, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Jack Pickford as Dick Hale
*Louise Huff as Princess Irenia
*Theodore Roberts as Madison Hale
*Hobart Bosworth as Govrian Texler
*Raymond Hatton as King Stephen III
*James Cruze as Ferdinand Vaslof
*James Neill as The Cardinal
*Bliss Chevalier as Countess Bonaco

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 