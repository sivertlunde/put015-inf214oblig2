Wonderland (1931 film)
{{Infobox Hollywood cartoon|
| cartoon_name = Wonderland
| series = Oswald the Lucky Rabbit
| image = 
| caption = 
| director = Walter Lantz Bill Nolan
| story_artist = 
| animator = Manuel Moreno Ray Abrams Fred Avery Lester Kline Vet Anderson Pinto Colvig
| voice_actor = 
| musician = James Dietrich
| producer = 
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = October 26, 1931
| color_process = Black and white
| runtime = 6:49 English
| The Hunter
| followed_by = The Hare Mail
}}

Wonderland is a short animated film by Walter Lantz Productions, and part of a long-running short film series featuring Oswald the Lucky Rabbit. Contrary to its title, the film is not an adaptation of Alice in Wonderland but that of Jack and the Beanstalk.

==Plot==
Oswald lives in a house with his grandmother. Although they have a low income, they dont feel so down. One day the landlord pays them a visit. The landlord tells them they have been unable to pay rent to the house in a timely manner, and therefore they need to move out in a few hours. While the grandmother is saddened by this, Oswald offers her hope as he intends to sell their cow.

Oswald heads to the outdoors, where he sells the cow to a wizard. In exchange, the wizard offers a sack of beans. Nevertheless, Oswald is pleased of what he got as he happily rushes back towards the house. But on the way, he stumbles, and the beans in the bag drop into a hole in the ground. In no time the beans grow into a huge stalk that stretches toward the sky. One of the stalks stems catches Oswald, and carries him upward.

When the stalk reaches its peak, Oswald is surprised to see a large castle in the clouds. Intrigued by the castles presence, Oswald enters a window to explore the building. Upon going inside, Oswald finds an anthropomorphic doll tied onto a clocks pendulum. Oswald comes to and frees her. As gratitude, the doll tells him there is a hen that lays golden eggs.

Oswald and the doll reach a room where the special hen is located. When the doll asks for an egg, the hen is considerate to lay one. But when Oswald himself asks, the hen, for some reason, refuses. The hen goes to make some noise which wakes up a giant bull. And when the bull pursues them, Oswald and the doll have no choice but to leave. After exiting the castle, they climb down the stalk. The giant bull follows them in the same way.

Back on the ground, Oswalds grandmother is outdoors in another meeting with the landlord who comes to signal her familys move. When the grandmother spots Oswald coming down the stalk, the ruthless landlord goes on the cut down the tall plant with an ax. But in doing so, the landlord ends up smashed under the giant bull who is unconscious. Oswald and the doll make it safely to the ground, and celebrate.

==Home media==
The cartoon is available in the Attack of the 30s Characters film collection. {{cite web
|url=http://lantz.goldenagecartoons.com/1931.html
|title=The Walter Lantz Cartune Encyclopedia: 1931
|accessdate=2011-05-23
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==References==
 

==External links==
*  at the Big Cartoon Database
* 

 

 
 
 
 
 
 
 
 
 


 