Everybody's Baby: The Rescue of Jessica McClure
{{Infobox Film
| name           = Everybodys Baby: The Rescue of Jessica McClure
 
| caption        = American DVD cover
| director       = Mel Damski
| producer       = Diana Kerew (producer) Patricia Clifford (executive producer)
| writer         = David Eyre, Jr.
| starring       = Beau Bridges Pat Hingle Roxana Zal Will Oldham Whip Hubley
| music          = Mark Snow Shelly Johnson
| editing        = Ron Binkowski
| distributor    = Industrial Distribution
| released       = 21 May 1989 (USA)
| runtime        =
| country        =  
| awards         = English
| budget         =
| preceded_by    =
| followed_by    =
}}

Everybodys Baby: The Rescue of Jessica McClure (1989) is a dramatic television film for American Broadcasting Company|ABC, based on the true story of the rescue of 18-month-old Jessica McClure in a water well|well. It featured, as extras, many participants in the actual rescue and its coverage.

==Plot==
Based on the true story of Jessica McClure, the child who fell down an abandoned water well while playing one afternoon in her aunts backyard in Midland, Texas. She was stuck in the well 22 feet down and it took rescuers 58 hours to get her out. There was fear that if they shook the ground too much with machinery they could cause her to fall further down and die.  Rescuers managed to successfully excavate alongside the well shaft and bring her to safety.

==Cast==
* Beau Bridges &ndash; Police Chief Richard Czech
* Pat Hingle &ndash; Fire Chief James Roberts
* Roxana Zal &ndash; Cissy McClure
* Will Oldham &ndash; Chip McClure
* Whip Hubley &ndash; Robert ODonnell
* Robin Gammell &ndash; MSHA Investigator Thomas Kaye
* Walter Olkewicz &ndash; Police Officer Andy Glasscock
* Rudy Ramos &ndash; Police Officer Manny Beltran
* Jack Rader &ndash; Public Information Officer James White
* Guy Stockwell &ndash; Bill Jones
* Daryl Anderson &ndash; Richard Armstrong
* Mills Watson &ndash; Charles Boler
* Patty Duke &ndash; Carolyn Henry Jessica McClure Jessica McClure

==External links==
*  
*  
*  

 

 
 
 
 
 
 

 