Jesse James at Bay
{{Infobox film
| name           = Jesse James at Bay
| image_size     = 
| image	=	Jesse James at Bay FilmPoster.jpeg
| caption        = 
| director       = Joseph Kane
| producer       = Joseph Kane (associate producer)
| writer         = Harrison Jacobs (story) James R. Webb (screenplay)
| narrator       = 
| starring       = Roy Rogers
| music          = Paul Sawtell William Nobles
| editing        = Tony Martinelli
| studio         = 
| distributor    = 
| released       = 1941
| runtime        = 56 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Jesse James at Bay is a 1941 American film directed by Joseph Kane and starring Roy Rogers.

==Cast==
*Roy Rogers as Jesse James / Clint Burns
*George Gabby Hayes as Sheriff Gabby Whitaker
*Sally Payne as Polly Morgan
*Pierre Watkin as Phineas Krager - Land Dealer
*Ivan Miller as Judge Rutherford
*Hal Taliaferro as Paul Sloan, Lawyer
*Gale Storm as Jane Fillmore, St. Louis Journal Reporter
*Roy Barcroft as Henchman Vern Stone
*Jack Kirk as Henchman Rufe Balder

==Soundtrack==
*Roy Rogers - "You for Me" (Written by Sol Meyer)
*Roy Rogers - "The Old Chisholm Trail" (Traditional)

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 