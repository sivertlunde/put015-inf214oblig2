Windhorse (film)
{{Infobox film
| name           = Windhorse
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        =  Paul Wagner
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Dadon Jampa Kalsang Tamang Richard Chang
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = United States
| language       = 
| budget         = 
| gross          = 
}} Paul Wagner. The leading roles were played by Dadon, Jampa Kalsang Tamang and Richard Chang.

The film premiered at the Toronto International Film Festival on September 16, 1998.

== Story ==

The film is based on a true story that happened in Tibet. Three young Tibetan relatives, Dorjee, his sister Dolkar and their cousin Pema, saw how Chinese soldiers murder their grandfather in Western Tibet.

After the murder the family decides to move to Lhasa where the three relatives evolve differently. Dolkar becomes a pop-star and assimilates into contemporary Chinese culture. Dorjee turns to hating the Chinese government. Pema becomes a Buddhist nun, joining a nunnery miles away from Lhasa. Pema is involved in a demonstration against the government and she is arrested. 

== Awards and nominations ==
{| class="wikitable"
!Year
!Awards
!categories
!nomination
!result
|- 1998 || Florida Film Festival || Audience Award for Best Feature  || Paul Wagner || style="background:#90ff90;"| won
|- Grand Jury Award || Paul Wagner || nominated
|}

== References ==
 

 
 
 
 
 
 
 
 


 