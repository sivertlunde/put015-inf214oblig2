Campus Confessions
{{Infobox film
| name            = Campus Confessions
| image           =
| image_size      =
| caption         =
| director        = George Archainbaud
| producer        =
| writer          = Lloyd Corrigan
| narrator        =
| starring        =
| music           = Henry Sharp
| editing         = Stuart Gilmore
| distributor     = Paramount Pictures
| released        =  
| runtime         = 67 min.
| country         = United States
| language        = English
}}

Campus Confessions is a 1938 American film directed by George Archainbaud, featuring Betty Grable in her first starring role, and American basketball player Hank Luisetti in his only film appearance.

== Cast ==

* Betty Grable as Joyce Gilmore
* Eleanore Whitney as Susie Quinn William Henry as Wayne Atterbury, Jr.
* Fritz Feld as Lady MacBeth
* John Arledge as Freddy Fry
* Thurston Hall as Wayne Atterbury, Sr.
* Roy Gordon as Dean Wilton
* Lane Chandler as Coach Parker
* Richard Denning as Buck Hogan
* Matty Kemp as Ed Riggs
* Sumner Getchell as Blimp Garrett
* Hank Luisetti as Himself

== External links ==
*  
*  
 
 
 
 
 
 
 


 