Love Marriage (1975 film)
{{Infobox film
| name           = Love Marriage
| image          =
| caption        = Hariharan
| producer       = GP Balan Hariharan T. Damodaran (dialogues) Hariharan
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Jose Prakash
| music          = Ahuan Sebastian
| cinematography = TN Krishnankutty Nair
| editing        = VP Krishnan
| studio         = Chanthamani Films
| distributor    = Chanthamani Films
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film, Hariharan and produced by GP Balan. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Jose Prakash in lead roles. The film had musical score by Ahuan Sebastian.  

==Cast==
 
*Prem Nazir as Madhu
*Jayabharathi as Manju
*Adoor Bhasi as Menon
*Jose Prakash as Prakash
*Manavalan Joseph as Doctor
*Pattom Sadan
*Sankaradi as Major Nair
*Sreelatha Namboothiri
*Azhikkode Balan as Badran
*Bahadoor as Gopi
*K. P. Ummer as Raju Meena as Mini/Meenakshiyamma
*Rani Chandra as Viji Reena
*Sadhana Sadhana as Kaanchi
*TS Muthaiah as RK Nair
*TP Madhavan as Police Officer
*Sreekala
*Swapna
*Treesa
*Vijaya
*Rathidevi
 

==Soundtrack==
The music was composed by Ahuan Sebastian and lyrics was written by Mankombu Gopalakrishnan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Eeswaranmaarkkellaam || P Jayachandran, Ayiroor Sadasivan || Mankombu Gopalakrishnan ||
|-
| 2 || Kaaminimaarkkullil || Vani Jairam, Ambili || Mankombu Gopalakrishnan ||
|-
| 3 || Ladies Hosteline  || P Jayachandran || Mankombu Gopalakrishnan ||
|-
| 4 || Neelaambari || K. J. Yesudas || Mankombu Gopalakrishnan ||
|-
| 5 || Prasadakunkumam || AM Raja || Mankombu Gopalakrishnan ||
|-
| 6 || Vrindaavanathile Raadhe || K. J. Yesudas, Zero Babu || Mankombu Gopalakrishnan ||
|}

==References==
 

==External links==
*  

 
 
 


 