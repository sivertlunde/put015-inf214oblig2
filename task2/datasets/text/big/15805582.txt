The Golden Stallion (1927 film)
 
{{Infobox film
| name           = The Golden Stallion
| image          = 
| caption        = 
| director       = Harry S. Webb
| producer       = Nat Levine
| writer         = William A. Berke Carl Krusada
| narrator       = 
| starring       = Maurice Lefty Flynn Joe Bonomo
| distributor    = Mascot Pictures
| released       =  
| runtime        = 10 episodes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 Western film serial produced by Nat Levines Mascot Pictures (the first serial produced by Mascot) and directed by Harry S. Webb.   

==Cast==
* Maurice Bennett Flynn as Wynne Randall Joe Bonomo as Ewart Garth
* Jay J. Bryan as Black Eagle
* Ann Small as Watona
* White Fury the Horse as The Golden Stallion
* Bert De Marc
* Billy Franey
* Tom London as Jules La Roux Molly Malone as Joan Forythe
* Burr McIntosh as Elmer Kendall
* Josef Swickard as John Forsythe

==Plot==
Two men search for a fabled gold mine. The clue to the mines location is branded on the neck of a wild horse, "White Fury", and the men battle each other to capture the horse first.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 