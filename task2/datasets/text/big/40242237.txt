The Perfect Match (1991 film)
 
 
{{Infobox film
| name           = The Perfect Match
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 富貴吉祥
| director       = Stephen Shin
| producer       = Norman Chan
| writer         = Tony Leung Hung-Wah Lam Tan-Ping Lam Wai Lun
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = George Lam Maggie Cheung Jacky Cheung Vivian Chow
| music          = Danny Chung Tang Siu-Lam
| cinematography = Lee Kin Keung
| editing        = Cheung Kwok Kuen Wong Wing-ming
| studio         = 
| distributor    = 
| released       =  
| runtime        = 86 min
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$ 13.49 M.
}}
The Perfect Match (富貴吉祥) is a 1991 Hong Kong film directed by Stephen Shin.

==Cast==
* George Lam as Koo
* Maggie Cheung as Carrie Kam
* Jacky Cheung as Jacky Kam
* Vivian Chow as Philidonna
* Dennis Chan	
* Chan Man-Ho as Madonnas maid
* Cheng Chi-Gong		
* Cheng Chi-Keung		
* Cheng Pak Lam		
* Hau Woon Ling		
* Benz Hui as Koos Manager
* Tony Leung Hung-Wah	
* Cynthia Khan as Philidonnas Cousin
* Lai Bei-Dak
* Lam Chi-wah	
* Brenda Lo	
* Lydia Shum as Madonna
* Sin Gam-Ching	
* Tse Wai-Kit	
* Wong Kim-Ching		
* Manfred Wong		
* Yip Hon Leung		
* Kingdom Yuen as Managers Assistant

==External links==
*  
*  
 
 
 
 

 