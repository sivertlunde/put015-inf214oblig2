Carnival (1931 film)
{{Infobox film
| name=Carnival
| image= 
| caption= 
| director=Herbert Wilcox
| producer= Harley Knoles Herbert Wilcox
| writer= Donald Macardle
| starring= Matheson Lang Joseph Schildkraut Kay Hammond Chili Bouchier
| music= 
| cinematography = Freddie Young Jack Parker
| editing= Maclean Rogers
| studio=British & Dominions Film Corporation
| distributor= 
| released=  
| runtime=88  minutes
| country= United Kingdom
| language= English
| budget= 
| gross= 
}}
Carnival is a 1931 British drama film directed by Herbert Wilcox and produced by his British & Dominions Film Corporation, starring Matheson Lang, Joseph Schildkraut, Kay Hammond and Chili Bouchier.  During a performance of Othello a jealous actor attempts to strangle his wife who he believes has committed adultery.  It was a remake of the 1921 film Carnival (1921 film)|Carnival. The French musician Alfred Rode appears with his band.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 