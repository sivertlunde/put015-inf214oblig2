Giv Gud en chance om søndagen

{{Infobox film
| name           = Giv Gud en chance om søndagen
| image          = 
| image size     = 
| caption        = 
| director       = Henrik Stangerup
| producer       = Peer Guldbrandsen
| writer         = Henrik Stangerup
Jørgen Stegelmann
| narrator       = 
| starring       = Henning Moritzen
| music          = 
| cinematography = 
| editing        = 
| distributor    = Asa Studio   
| released       = 22 April 1970
| runtime        = 94 minutes
| country        = Denmark Danish
| budget         = 
| preceded by    = 
| followed by    = 
}}
 Danish film. The title is Danish for "Give God a chance on Sunday".

It stars Ulf Pilgaard, Ove Sprogøe and Lotte Tarp. 

==Cast==
*Ulf Pilgaard - Rev. Niels Riesing
*Lotte Tarp - Hanne Riesing
*Vibeke Reumert - Hannes Mother
*Ove Sprogøe - Præst ved Roskilde Domkirke
*Ole Storm - Thorsen
*Rachel Bæklund - Fru Thorsen
*Erik Nørgaard - Lærer Petersen
*Ebbe Kløvedal Reich - Himself
*Erik Halskov-Jensen - Arbejder I grusgrav
*Annelise Halskov-Jensen - Arbejderens kone
*Knud Jansen - Rideskoleeje
*Leif Mønsted - Vækkelsesprædikant
*Niels Ufer - Teolog
*Jørgen Schleimann - Teolog
*Arne Skovhus - Teolog
*Johannes Møllehave - Teolog
*Karl Andersen - Stoffer
*Vilhelm Rigels - Organist
*Henrik Stangerup - Kistebærer
*Ole Michelsen
*Pernille Kløvedal   

==External links==
*  

==References==
 

 
 
 
 

 