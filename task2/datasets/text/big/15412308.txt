Maidan-E-Jung
 
 
{{Infobox film
| name           = Maidan-E-Jung 
| image          = 
| caption        =
| director       = K.C. Bokadia|
| producer       = Dilip Kankaria  
| writer         = K.C. Bokadia, Subhash Jain (scenario)
| starring       = Akshay Kumar Dharmendra Karishma Kapoor
| music          = Bappi Lahiri
| cinematography = Sudarshan Nag
| editing        = Govind Dalwadi
| distributor    = Devyang Arts
| released       = 14 April 1995
| runtime        = 
| country        = India
| language       = Hindi budget          =   gross           =  
}}

Maidan-E-Jung is 1995 Hindi film directed by K.C. Bokadia and starring Dharmendra, Akshay Kumar, Karishma Kapoor. Other cast members include Amrish Puri, Manoj Kumar (in his last film appearance), Jayapradha, Gulshan Grover, Kader Khan, Shakti Kapoor, Mukesh Khanna and Dina Pathak.
==Plot==
Daata Guru (Amrish Puri) is a wealthy, powerful, and influential head of a village. He owns and controls all the villagers and their properties. Anyone who dares to raise their voice against him is crushed by his employee, Shankar (Dharmendra), and his sons, Guman (Gulshan Grover) and Karan (Akshay Kumar). When Daata Guru learns that Shankar has abducted his widowed daughter-in-law, Lakhsmi (Jaya Pradha), and has married her, he is enraged and wants Shankar dead. Daata Guru also instigates Karan against Shankar, and threatens the villagers to cut off their water supply if they do not turn in Shankar to him.
==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CACCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Teera Bole"
| Gurdas Mann, Ila Arun
|- 
| 2
| "Kya Baat Hai Too"
| Kumar Sanu, Alka Yagnik
|- 
| 3
| "Kahi Pyaasi Raat"
| Amit Kumar, Sapna Mukherjee
|- 
| 4
| "Koi Haal Mast Koi Chaal"
| Vinod Rathod, Mahendra Kapoor
|- 
| 5
| "Shaam Dhal Rahi Teri Yaad"
| Kumar Sanu, Sadhana Sargam
|- 
| 6
| "Lo Phagun Ritu Aa Gayee"
| Udit Narayan, Vinod Rathod, Sadhana Sargam
|- 
| 7
| "Memsaab O Memsab"
| Sheeba, Baba Sehgal
|- 
| 8
| "Dil Mein Tere"
| Jolly Mukherjee, Alka Yagnik
|}

== External links ==
*  

 
 
 


 