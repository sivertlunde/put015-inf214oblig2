Bride of Re-Animator
{{Infobox film
| name = Bride of Re-Animator
| image = Bride of Re-Animator.jpg
| image_size = 
| alt = 
| caption = Film poster
| director = Brian Yuzna
| producer =  Brian Yuzna
| screenplay = {{plainlist|
* Rick Fry
* Woody Keith
* Brian Yuzna
}}
| based on =  
| starring = {{plainlist|
* Jeffrey Combs
* Bruce Abbott
* Fabiana Udenio
* Kathleen Kinmont
}}
| music = Richard Band
| cinematography = Rick Fichter
| editing = Peter Teschner
| studio = Wild Street Pictures
| distributor = 50th Street Films
| released =  
| runtime = 96 minutes  
| country = United States
| language = English
}} science fiction horror film directed by Brian Yuzna and was written by Yuzna, Rick Fry and Woody Keith. H. P. Lovecraft wrote the original serialized story, titled Herbert West–Reanimator, from which the characters were derived.  The plot roughly follows episodes "V. The Horror from the Shadows" and "VI. The Tomb-Legions" of the original. The film stars Bruce Abbott, Claude Earl Jones, Fabiana Udenio, David Gale, Kathleen Kinmont, and Jeffrey Combs.

Bride of Re-Animator is the sequel to Stuart Gordons Re-Animator (1985) and is followed by Yuznas Beyond Re-Animator (2003).

==Plot== Peruvian civil war. In the chaos of battle and with plenty of casualties to work on, they are free to experiment with Wests re-animation reagent. When their medical tent is stormed by the enemy troops, West and Cain return home to Arkham, Massachusetts. There, they resume their former jobs as doctors at Miskatonic University Hospital, and West returns to the basement laboratory of Cains house to continue his research.

Using parts pilfered from both the hospitals morgue and from the cemetery conveniently located next door, West discovers that his reagent can re-animate body parts by themselves. He becomes determined to create an entire living person from disparate body parts. West discovers the heart of Megan Halsey, Cains fiancée, in the hospital morgue. With the promise to use her heart to re-animate a new Megan, West convinces Cain to help him with his project. Also stored in the morgue is the rest of the evidence from the previous "Miskatonic Massacre". Inside, pathologist Dr. Wilbur Graves discovers a vial of Wests reagent and the severed head of Dr. Carl Hill. Using the reagent, he re-animates Hills head.

Meanwhile, police officer Lt. Leslie Chapham begins investigating West and Cain. He bears a grudge against the pair, as they were the only unaffected survivors of the Miskatonic Massacre; the dead body of Chaphams wife was re-animated into a crazed zombie during the incident. Chapham suspects West and Cain were responsible. When he stops by their house to question them, he discovers Wests corpse-filled lab and the two get into an ugly confrontation. A fight ensues and West ends up killing Chapham by means of cloth treated with a chemical which causes cardiac arrest when inhaled (a product of Wests research into obtaining the freshest possible corpses for his experiments). West then re-animates the police officer with the intention of covering up his crime. Chapham violently wanders out of the house and into the cemetery next door.
 hypnotic powers, Hill commands Chapham to force Dr. Graves to stitch bat wings onto his neck, giving him back his mobility. He also extends his mental control to all of the zombie survivors of the Miskatonic Massacre.

When one of Cains patients, the beautiful Gloria, dies, West collects the last piece he needs for his creation: her head. With a complete body stitched and wired together, West and Cain inject the re-animation reagent into Megs heart. While waiting for the reagent to take effect, a package is delivered to their house. West retrieves and opens it. From inside, Hills winged head flies out. Simultaneously, all of the zombies he controls break into the house. West retreats back to the basement lab, where his creation, the Bride, has awoken.
 tissue rejection.

Hill and his zombies force West, Cain and Francesca to retreat through the wall of the lab and into a crypt in the neighboring cemetery. Inside, all of Wests prior test subjects arise and make their way towards him, stopping only when Herbert commands them to. The unstable crypt begins to collapse, trapping Hill, West and the zombies. Cain and Francesca manage to escape the debris and claw their way to the surface of the cemetery together. Hill, stuck in the debris, laughs manically, while Megans heart, still in the hand of the bride, stops beating.

==Cast==
* Jeffrey Combs as Dr. Herbert West
* Bruce Abbott as Dr. Dan Cain
* Claude Earl Jones as Lt. Leslie Chapham
* Fabiana Udenio as Francesca Danelli David Gale as Dr. Carl Hill
* Kathleen Kinmont as Gloria
* Mel Stewart as Dr. Graves
* Irene Forrest as Nurse Shelley
* Michael Strasser as Ernest
* Mary Sheldon as Meg Halsey

===The Re-Animated===
* Marge Turner as Elizabeth Chapham
* Johnny Legend as Skinny Corpse
* David Bynum as Black Corpse
* Noble Craig as Crypt Creature
* Kim Parker as Crypt Creature
* Charles Schneider as Crypt Creature
* Rebeca Recio as Crypt Creature
* Jay Evans as Crypt Creature

==Production==
Actress Barbara Crampton did not come back for the sequel.  She said in an interview that her agent convinced her not to take a cameo appearance, as he felt that it was beneath her to have such a small role.   The film was originally going to feature Herbert West in the White House.  This concept was recycled in a later sequel. 

==Reception==
 ,   recommended it to fans of the first film but said that mainstream audiences would dislike it.   Vincent Canby of The New York Times wrote, "Bride of Re-Animator is less a sequel to the critically praised 1985 horror film Re-Animator than a rehash based on the same H. P. Lovecraft stories."   Ty Burr of Entertainment Weekly rated it C+ and called it "an anemic shadow of the first film".   Writing in The Zombie Movie Encyclopedia, academic Peter Dendle called it "a pointless and forgettable sequel". 

Bride of Re-Animator was nominated for two awards by the Academy of Science Fiction, Fantasy & Horror Films in 1991. It was nominated for the Saturn Award for Best Horror Film and Jeffrey Combs was nominated for Saturn Award for Best Supporting Actor.

==Sequels==
The film was followed by Beyond Re-Animator.

==References==
 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 