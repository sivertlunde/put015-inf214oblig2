Highlander: The Search for Vengeance
{{Infobox film
| name        = Highlander: The Search for Vengeance
| image       = Highlander-search-of.jpeg
| caption     =
| director    = Yoshiaki Kawajiri Hiroshi Hamasaki  (co-director) 
| producer    = Masao Maruyama Peter Davis Bill Panzer Thomas K. Gray Galen Walker
| writer      = David Abramowitz Jim Byrnes Kathleen Barr Ogie Banks Madhouse Imagi Animation Studios
| distributor = 
| released    =  
| runtime     = 85 minutes English Japanese Japanese
| music       = Jussi Tegelman Nathan Wang
| budget      = 
}}
 Madhouse Studio, Sci Fi Channels Ani-Monday block.

==Synopsis==
The story centers on characters who are immortal.

The lead character, Colin MacLeod, became an Immortal after his first death in 125 AD in Roman Britain, when his village was attacked by the conquering Romans.  Another Immortal, Marcus Octavius, was leading the Roman Empires military forces in hopes of creating his dream of a utopian society.  Octavius killed Colins wife, but was not able to kill Colin whose unconscious body was dragged by a horse to within Stonehenge, a "holy ground" in the story, where Immortals are forbidden to fight.

Waking days later within Stonehenge, Colin is left confused about who and what he is, and why he is still alive. It is at this moment that the spirit of a former druid of this holy site, Amergan, begins communicating with MacLeod and explains to him what he is.  Colin learns of The Game from Amergan, and the druid becomes his lifelong teacher and conscience.

The movie interlaces flashback scenes of this and Colins following plight for vengeance throughout time, as Marcus attempts to re-enact his utopian society through force and fear.  Throughout the next two millennia, Marcus serves as a high-ranking member of various powerful empires, ranging from the British Empire to Nazi Germany.  Marcus wants to encourage the development of a utopian world-empire, but in contrast, the side he chooses tend to be ruthless and authoritarian (e.g. Nazi Germany).  For 2000 years, Colin clashes with Marcus multiple times throughout history, always fighting on the opposing side trying to bring down which ever authoritarian regime Marcus is supporting.  While Colin is often badly beaten, neither one ever succeeds in killing the other. Though Colin is ostensibly in the role of the "barbarian" and Marcus is the bringer of "civilization", Marcus cares more about building an empire and less about peoples well-being, with an "ends-justify-the-means" mentality. 
 Quickening destroys the virus in question.

Following the defeat of Marcus, Colin leaves New York for an unknown destination with a new sense of belief and purpose in life with the first time in his life not meant for revenge.

==Cast==
{| class="wikitable"
|- 
! Role !! Japanese voice actor !! English voice actor
|- 
| Colin MacLeod || Shun Oguri || Alistair Abell
|- 
| Marcus Octavius || Koichi Yamadera || Zachary Samuels
|-
| Amergan || Kosei Tomita || Scott McNeil
|-
| Dahlia || Romi Park || Debi Mae West
|-
| Kyala || Megumi Hayashibara || Janyse Jaud
|- Jim Byrnes
|-
| Rudy || Yusaku Yara || Jim Byrnes
|-
| Moya || Yurika Hino || Kathleen Barr
|-
| Gregor Lab Director || Takaya Hashi || Scott McNeil
|-
| Joe || Minami Takayama || Ogie Banks
|}

==MacLeod heritage==
In the story, Colin was born in Roman Britain, but his true surname, which is never given, was not MacLeod at birth.  One of the flashback scenes shows him achieving this honor after he fought alongside Clan MacLeod in an unknown battle, presumably because Octavius was fighting for the opposition.  The Clansmen honor Colin, and name him a MacLeod for giving his life for their cause.  Once he regains life, as all Immortals do, Colin adopts the name, and keeps it for the rest of his life, as it was with the MacLeods that Colin learned the values that would one day assist him in overcoming his unyielding lust for vengeance.

==Directors cut==
Producer H. Galen Walker said in an interview: "Theres about seven or eight additional scenes in the Japanese version that we cut out in the U.S.—one, just for timing, and just for pace of story. That was the big issue. Im sure Mr. Kawajiri was really unhappy about the cut, but this was what the other producers thought was best for everybody." An opening exposition text-sequence, which is traditional for Highlander films, was also added for the American version. Walker said that Kawajiris original cut "will probably be out later this year," in an interview conducted in June 2007.  More than a year later, the directors cut was finally released in Japan on December 5, 2008 with the running time of 96 minutes (10 minutes longer than the U.S. version). 

==Reception==
Critical reaction to Highlander: The Search for Vengeance has been generally favorable, and more favorable in general than the live-action Highlander sequels. Chris Wyatt of IGN awarded the film a score of 9 out of 10, saying: "Highlander: The Search for Vengeance is the best thing to happen to Highlander fans since the original film.   violent, dramatic, sexy, and actually smart. ... Combat sequences, including the obligatory sword fights, are nothing less than stellar." 

Rob Lineberger of DVD Verdict gave The Search for Vengeance a score of 96 out of 100, saying: "Not only does Highlander: The Search for Vengeance live up to the glory days of the franchise, it exceeds them in many ways. ... Kawajiris movie is crammed full of breathtaking compositions and sophisticated effects,   the character depictions are also impressive. ... Great animation, superb music, and engaging voice acting complement the story. Not only is it a must-see for Highlander fans, but for fans of edgy anime with a mature bent." 

Todd Douglass of DVD Talk said: "The story stands up quite well and frankly it reinvigorates a franchise that has otherwise turned stagnant. ... As a lover of Highlander and anime, The Search for Vengeance was more or less what I wanted it to be. ...   is worth a look if youre even slightly interested in it."  Jason Cook of The Spinning Image said that "The Search for Vengeance is a thoroughly enjoyable animated feature coupling classy visual flourishes with a lean plot. ... Anime fans will want to seek this out due to its directors pedigree, but those unfamiliar with the genre are still advised to give this a go." 
 1986 original, but it does so unapologetically, and with a genuine passion, too. ... Harper added that "in terms of pure entertainment, its a hell of a lot more satisfying than any of the other sequels." 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 