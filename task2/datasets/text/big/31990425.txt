The Russian Singer
{{Infobox film
| name           = The Russian Singer
| image          = 
| caption        = 
| director       = Morten Arnfred
| producer       = Erik Crone William Aldridge Leif Davidsen
| starring       = Ole Lemmeke
| music          = 
| cinematography = Alexander Gruszynski
| editing        = Lizzi Weischenfeldt
| distributor    = 
| released       =  
| runtime        = 119 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

The Russian Singer ( ) is a 1993 Danish thriller film directed by Morten Arnfred. It was entered into the 43rd Berlin International Film Festival.   

==Cast==
* Ole Lemmeke as Jack Andersen
* Elena Butenko as Lili
* Vsevolod Larionov as Colonel Gavrilin Igor Volkov as Basov Aleksandrovitj
* Igor Yasulovich as Pjotr Demichev
* Jesper Christensen as Castensen
* Erik Mørk as C.W.
* Andrei Yurenyov as Tushin (as Andrei Yurenev)
* Igor Statsenko as Dima
* Oleg Plaksin as General Panyukov
* Vladimir Troshin as General Vlasov
* Yuriy Sherstnyov as Panyukovs Lawyer
* Vladimir Grammatikov as Nikolaj Davidovitj Klejmann

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 