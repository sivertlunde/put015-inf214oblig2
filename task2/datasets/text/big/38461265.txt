Peter Voss, Hero of the Day
{{Infobox film
| name           = Peter Voss, Hero of the Day 
| image          = 
| image_size     = 
| caption        = 
| director       = Georg Marischka 
| producer       = Kurt Ulrich
| writer         = Curt J. Braun   Peter Dronte   Gustav Kampendonk 
| narrator       =  Peter Vogel
| music          = Erwin Halletz Hermann Haller
| cinematography = Klaus von Rautenfeld
| studio         = Kurt Ulrich Filmproduktion
| distributor    = 
| released       = 22 December 1959
| runtime        = 105 minutes
| country        = West Germany German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} comedy crime Peter Voss, novel of the same title by Ewald Gerhard Seeliger.

==Cast==
* O.W. Fischer as Peter Voss
* Linda Christian as Grace McNaughty
* Walter Giller as Bobby Dodd Peter Vogel as Prinz José Villarossa
* Ingmar Zeisberg as Dolly
* Peter Mosbacher as Baron de Clock
* Helga Sommerfeld as Mary de la Roche
* Ludwig Linkmann as Rechtsanwalt Perrier
* Ralf Wolter as Charley, der Jockey
* Ady Berber as Leslie aus Texas
* Stanislav Ledinek as Präsident
* Lucie Englisch as Haushälterin

==Bibliography==
* Popa, Dorin. O.W. Fischer: seine Filme, sein Leben. Wilhelm Heyne, 1989.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 