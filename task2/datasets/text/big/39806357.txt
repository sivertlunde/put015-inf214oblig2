The Wife of Forty Years
{{Infobox film
| name           = The Wife of Forty Years
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Oswald 
| producer       = Richard Oswald
| writer         = Richard Oswald
| narrator       = 
| starring       = Diana Karenne   Vladimir Gajdarov   Sig Arno   Paul Otto
| music          = Willy Schmidt-Gentner
| editing        =
| cinematography = Jack Hermann   Theodor Sparkuhl
| studio         = Richard-Oswald-Produktion
| distributor    = 
| released       = 12 April 1925
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| 
}} German silent film directed by Richard Oswald and starring Diana Karenne, Vladimir Gajdarov and Sig Arno. The films art direction was by Paul Leni. 

==Cast==
* Diana Karenne as die Frau 
* Vladimir Gajdarov as Er 
* Sig Arno as Der Hausfreund
* Paul Otto as Der Mann 
* Dina Gralla as die Tochter 
* Harry Hardt   
* Eva Speyer   
* Hugo Döblin   
* Gerti Kutschera   
* Mercedes Erdmann

==References==
 

==Bibliography==
* Kasten, Jürgen & Loacker, Armin. Richard Oswald: Kino zwischen Spektakel, Aufklärung und Unterhaltung. Verlag Filmarchiv Austria, 2005.

==External links==
* 

 

 
 
 
 
 
 


 