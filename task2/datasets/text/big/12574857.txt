Bow Barracks Forever
 
{{Infobox film
| name           = Bow Barracks Forever image           = Bow Barracks Forever poster.png
| image size     =200px
| caption        = Bow Barracks Forever poster
| director       = Anjan Dutt
| producer      = Tapan Biswas Rangita Pritish Nandy Bobbie Ghosh
| writer         = Anjan Dutt
| narrator       =
| starring       = Lilette Dubey Victor Bannerjee Neha Dubey Clayton Rodgers Moon Moon Sen Sabyasachi Chakrabarty Sohini Paul
| music          = Neel Dutt Anjan Dutt Usha Uthup
| cinematography = Indranil Mukherjee
| editing        = Arghakamal Mitra
| distributor    = Cinemawalla
| released       =  
| runtime        = 118 minutes
| country        = India English
| budget         =
| gross          =
}}

Bow Barracks Forever is a 2004 Indian film directed by Anjan Dutt about Anglo-Indians and their difficulties in retaining their identity since the end of British India.

==Plot==
The film is about the disaster of the human spirit. It is not easy to fight back the march of progress. Progress brings with it change, often painful, that breaks continuity and destroys tradition, history, power and passion of communities that have lived and grown together over decades. Anjan Dutts film captures the real-life story of a tiny, resolute Anglo-Indian community right in the heart of bustling north Kolkata trying desperately to keep alive its hopes, dreams, aspirations and identity, as the world around them changes swiftly and tries to impose that change on them and their lives.

==Reception==

One Anglo-Indian viewer, Neville Rosario, reacted: "Once again, Anglo-Indians are stereotyped and depicted in a very bad light. This is not at all what typical Anglo-Indians are like. The movie depicts them as philanderers, wife-beaters, women being cheap, the men as drunkards and being overly emotional, lacking good sense, overly abusive, lazy and having a poor work ethic, which is not at all the case for a majority of our community. Many AIs have achieved distinction in their fields, are duty conscious. There are some among the community who may be alcoholic or ignorant or wife beaters etc., though not among the majority of persons in the community. I feel this is a very unfair and stereotypical depiction and totally untrue. The movie has some value to people who live or lived in the once great city of Calcutta, because of familiar streets and the story centering around the building of the Bow Barracks but beyond the details, the storyline, characteriaation and antics are somewhat infantile and immature in their scope. I was fairly disappointed during and after watching this movie." 

==See also==
* Bow barracks - a locality of Kolkata.

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 