Twist and Shout (film)
 
{{Infobox film
| name           = Twist and Shout
| image          = 
| caption        = 
| director       = Bille August
| producer       = Per Holst
| writer         = Bjarne Reuter Bille August
| starring       = Adam Tønsberg
| music          = 
| cinematography = Jan Weincke
| editing        = 
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}
 Best Foreign Language Film at the 58th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Adam Tønsberg as Bjørn
* Lars Simonsen as Erik
* Camilla Søeberg as Anna
* Ulrikke Bondo as Kirsten
* Bent Mejding as Eriks father
* Aase Hansen as Eriks mother
* Arne Hansen as Bjørns father
* Lone Lindorff as Bjørns mother Thomas Nielsen as Bjørns littlebrother Henning
* Malene Schwartz as Kirstens mother
* Troels Munk as Kirstens father
* Helle Spangaard as Kirstens sister Inge

==See also==
* List of submissions to the 58th Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 