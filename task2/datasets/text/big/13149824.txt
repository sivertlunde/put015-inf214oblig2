Bloodstone (1988 film)
{{Infobox film
| name           = Bloodstone
| image          = Bloodstone (1988 film) poster.jpg  
| caption        = Theatrical poster
| director       = Dwight H. Little
| producer       = Ashok Amritraj Sunanda Murali Manohar
| writer         = Nico Mastorakis Anna Nicholas Jerry Grant
| cinematography =
| editing        =
| distributor    = Omega Entertainment
| released       = October 7, 1988
| runtime        = 91 minutes
| country        = United States India English
| budget         =
| followed_by    =
}}

Bloodstone is a 1988 American mystery-adventure film produced by Ashok Amritraj and Sunanda Murali Manohar, directed by Dwight H. Little and written by Nico Mastorakis; starring Brett Stimely, Rajinikanth and Anna Nicholas. It was filmed in southern India, the story revolving around a mythical ruby called the Bloodstone. Bloodstone set records for hype and awareness among the audience before release. It opened to generally positive reviews and broke several box-office records upon release. 

==Plot==
 taxi driver Shyam Sabu (Rajinikanth) to Van Hoevens thugs. Sabu realizes that he is being followed, but he does not see the huge ruby fall into a crevice inside his trunk while dropping Sandy and Stephanie at their hotel. Soon Sandy and Sabu are both in action; Sandy in a brutal fight with two thieves ransacking his room and Sabu in a life-or-death car chase and shootout.

Stephanie gets kidnapped by Van Hoevens thugs. Van Hoeven contacts Sandy and proposes Stephanie in exchange for the gem at the waterfalls on the road to Bangalore. Without the ruby, Sandy has no option but the police, but Sabu produces the stone, igniting a vicious fight between the two equally matched men, ending in a second agreement. Sandy will pay well, but the ruby is his to exchange for Stephanies life. They prepare for a conflict while Sabu lets his heathen friends know that their help may be needed. At every corner, the bungling Ramesh is attempting to keep up with the furious pace, always managing somehow to fail. Sandy and Sabu are ambushed while exiting the rain forests but survive the shootout and capture two of Van Hoevens men, learning of the lone secret passage before tossing them over the waterfalls.

Van Hoeven is giving a festive, lavish party. But Sandy and Sabu have penetrated the palace with plans of their own, when they... drop unexpectedly. The ruby proves to be a fake and they are moments from execution when Sabus heathens storm the fortress and a bloody mêlée ensues. Sandy, Stephanie and Sabu escape directly into the gun barrels of Rameshs men. They are clean, Van Hoeven is arrested and Inspector Ramesh is a hero... but the Bloodstone is still missing.

==Cast==

*Brett Stimely	... 	Sandy McVey
*Rajinikanth	... 	Shyam Sabu Anne Nicholas	... 	Stephanie
*Charlie Brill	... 	Inspector Ramesh
*Jack Kehler	... 	Paul Lorre
*Christopher Neame	... 	Van Hoeven
*Tej Sapru  	... 	Manu
*Bob Christo	... 	Haggerty
*Deep Dhillon	... 	Tanjeer
*Carol Teasdale	... 	Anna
*Laura Albert	... 	Kim Chi
*Marjean Holden	... 	Shirley
*Dhanushkodi	... 	Maniam
*Bill Marley	... 	British husband
*Janet Lord	        ... 	British wife

==References==
*  
*  

 

 
 
 
 
 
 
 
 
 