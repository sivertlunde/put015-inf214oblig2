Sunghursh
 
 
{{Infobox film
| name           = Sunghursh
| image          = Sunghurshfilm.jpg
| image size     = 180 px
| director       = Harnam Singh Rawail
| producer       = Harnam Singh Rawail
| story          = Layli Asmaner Ayna by Mahasweta Devi
| screenplay     = Anjana Rawail  : Gulzar and Abrar Alvi
| starring       = Dilip Kumar Vyjayanthimala Balraj Sahni Sanjeev Kumar
| music          = Naushad
| cinematography = R. D. Mathur
| editing        = Krushna Sachdev
| distributor    = Shemaroo Entertainment
| studio         = Rahul Theatre
| released       = 27 July 1968
| runtime        = 158 minutes
| country        = India
| language       = Hindi
}}
 Hindi film directed and produced by Harnam Singh Rawail. It is based on a short story Layli Asmaner Ayna in Bengali language by Jnanpith Award-winning writer Mahasweta Devi, which presents a fictionalised account of vendetta within a thuggee cult in the holy Indian town of Varanasi. The film stars Dilip Kumar, Vyjayanthimala, Sanjeev Kumar, Balraj Sahni, Jayant (actor)|Jayant, Deven Verma, Durga Khote and Iftekhar.

The music is by Naushad and lyrics for the songs are by Shakeel Badayuni. Naushad and Badayuni had worked together on multiple films previously and were "the most sought after" composer-lyricist duo of the time in Bollywood. Sunghursh was popularly mistaken to be a debut film of Sanjeev Kumar. It did fairly well commercially on release and is often called as a "classic" Bollywood film. It is the last film where Kumar and Vyjayanthimala worked together. The actors had done a maximum number of films together in the lead roles, and every one was a commercial success.

The director Harnam Singh Rawails son Rahul Rawail, who is also a director, paid a tribute to this film by titling one of his as Jeevan Ek Sanghursh (1990) starring Anil Kapoor and Madhuri Dixit.   

==Plot==
Bhavani Prasad (Jayant) is a powerful Shakta priest at Kashi. Prasad, a devotee of the black goddess Kali and a thuggee, religiously follows a practice to murder wealthy travelers who stay in his pilgrim guesthouse and offers them as a sacrifice to Kali. Prasads son Shankar (Iftekhar) does not agree to such practices, opposes his father and decides to leave the village with his wife and their three children: Kundan (Dilip Dhavan), Yashoda and Gopal. Prasad forcibly takes Kundan with him to follow in his footsteps and forbids him from seeing the rest of the family.

Young Kundan is now being raised by his grandfather Prasad who desires to have Kundan as his successor, head of a temple and pilgrim guesthouse on the bank of the Ganges river. Prasad mysteriously kills his son and puts blame on his enemy and nephew, Naubatlal (D. K. Sapru). Prasad had earlier killed Naubatlals father. When Naubatlal learns the truth, he decides to take revenge but Prasad kills Naubatlal before he could so anything. Naubatlals family decides to leave the village and settle down Calcutta where his two young sons, Dwarka (Sanjeev Kumar) and Ganeshi Prasad (Balraj Sahni), work as merchants. They learn about their fathers death from their mother (Mumtaz Begum) and swear to avenge their fathers death by killing Prasad and his grandson Kundan (Dilip Kumar).

Kundan continues to serve Prasad in the Banaras temple but does not follow the practices of killing people. When he is invited to his younger sister, Yashodhas marriage (Anju Mahendru), Kundan gets a chance to visit his mother and grandmother and his siblings after many years. One day Kundan meets Laila-E-Aasmaan (Vyjayanthimala), a courtesan, in the temple who has come from Calcutta after her madams death. Kundan falls in love with Laila only to realise that she was his childhood friend, Munni. Kundan proposes Laila, but Prasad does not agree to the marriage knowing that Laila was hired by Dwarka and Ganesh Prasad to seduce Kundan and bring him to them.

Kundan decides to end the feud with Dwarka, but Dwarka does not co-operate. Dwarka attacks Kundan but gets killed my him. As repentance for his grandfathers sins, Kundan decides to serve Ganeshi and takes another identity of Bajrangi to make peace with him, only to realise that Ganeshi Prasad is in love with Laila and wants to marry her as his second wife.

==Cast==
 
* Dilip Kumar as Kundan S. Prasad / Bajrangi
* Vyjayanthimala as Munni / Laila-E-Aasmaan
* Balraj Sahni as Ganeshi N. Prasad
* Sanjeev Kumar as Dwarka N. Prasad Jayant as Bhawani Prasad
* Durga Khote as Bhawani Prasads wife
* Anju Mahendru as Yashoda S. Prasad
* Sunder as Mama, Kundans uncle.
* Ulhas as Bhim
* Deven Verma as Nisar
* Padma Khanna as Mamas wife
 
* Sulochana Latkar as Shankars wife
* Iftekhar as Shankar B. Prasad
* Urmila Bhatt as Kunti G. Prasad
* Padma Rani as Dwarkas wife
* D.K. Sapru as Naubatlal Prasad foster father foster mother
* Jagdish Raj as Raja Saheb
* Mehmood Junior as Dwarkas son Mumtaz Begum as Naubat Lals wife
 

==Production== Sadhana to Naya Daur (1957) were then said to have romantic affair. The actors parted away after Vyjayanthimala worked with Raj Kapoor in the 1964 Hindi film Sangam (1964 Hindi film)|Sangam. Thus, most of the scenes between the two actors for Sungharsh were shot separately.  

When the film was nearcompletion, it was rumoured that with the increasing conflicts between the two leading actors, Vyjayanthimala would be replaced by another actress, Waheeda Rehman. Rehman had already replaced Vyjayanthimala in another Hindi film starring Kumar, Ram Aur Shyam, (1967) which was being shot simultaneously with Sungharsh. Vyjayanthimala readily declined the claim of her leaving the film when it was about to finish its shooting.  Sungharsh was the last film where Kumar and Vyjayanthimala worked together. By then, both the actors had done the maximum number of films together and each was a commercial success. 

Sanjeev Kumar, who had previously acted in theatre and other smaller film productions, was noticed through his performance of negative role through Sungharsh and he then shot to fame.  He was commended for his role while a newcomer as compared with established actors like Dilip Kumar and Balraj Sahni.  The film was popularly mistaken to be his debut. 

As Sungharsh was set in Varanasi during the 19th century, Rawail took special care about the costumes and sets to create the look for the period.  The actor-director Rajesh Roshan had worked as an assistant director on the film. 

==Soundtrack== Baiju Bawra (1952), Mother India (1957), Mughal-e-Azam (1960) and more are quite popular. They had worked with Rawail and given the musical hit Mere Mehboob in 1963.  The films soundtrack has seven songs sung by Mohammad Rafi and Lata Mangeshkar with one song sung by Asha Bhosle. All are solo songs where Naushad used the music from the regions of Awadh and eastern Uttar Pradesh.  But critics have considered these compositions "below par" as compared with Naushads other work. 
 Bhairavi Raga. Chief Ministers of Bihar Lalu Prasad Yadav during the election campaign for Bihar Legislative Assembly election, 2010.  

{{Track listing
| total_length = 29:41
| extra_column = Singer(s)
| title1 = Chhedo Na Dil Ki Baat | extra1 = Lata Mangeshkar | length1 = 03:43
| title2 = Ishq Diwana | extra2 =  Mohammad Rafi | length2 = 03:59
| title3 = Tasveer-E-Mohabbat | extra3 = Asha Bhosle | length3 = 04:15
| title4 = Mere Paas Aao Nazar To Milao | extra4 = Lata Mangeshkar | length4 = 03:36
| title5 = Mere Pairon Mein Ghunghroo | extra5 = Mohammad Rafi | length5 = 04:53
| title6 = Agar Yeh Husn Mera | extra6 = Lata Mangeshkar | length6 = 04:55
| title7 = Jab Dil Se Dil | extra7 = Mohammad Rafi | length7 = 04:20
}}

==Awards== Best Actor Bengal Film Annual BFJA Awards. 

{| class="wikitable"
|-
! Award
! Ceremony
! Category
! Nominee
! Result
!  
|- Filmfare Awards 16th Filmfare Awards Filmfare Award Best Actor Dilip Kumar
| 
| align="center" |   
|- Bengal Film Journalists Association Awards 32nd Annual BFJA Awards Bengal Film Fourth Best Indian Film Harnam Singh Rawail
| 
|align="center" rowspan="5"|   
|- BFJA Awards Best Actor Dilip Kumar
| 
|- Best Actress Vyjayanthimala
| 
|- Best Supporting Actor Jayant
| 
|- Best Dialogue Gulzar & Abrar Alvi
| 
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 