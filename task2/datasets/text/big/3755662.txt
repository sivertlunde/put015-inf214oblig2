The Bachelor (1999 film)
{{Infobox film
| name           = The Bachelor
| image          = Bachelorposter1999.jpg
| caption        = Theatrical release poster
| director       = Gary Sinyor
| producer       = Jeffrey T. Barabe Bing Howenstein Lloyd Segan Seven Chances) Jean C. Havez (1925 screenplay Seven Chances)
| starring       = Chris ODonnell Renée Zellweger Artie Lange Edward Asner Hal Holbrook Peter Ustinov Brooke Shields Mariah Carey John Murphy
| cinematography = Simon Archer
| editing        = Robert M. Reitano Florence Vinger
| studio         = George Street Pictures
| distributor    = New Line Cinema
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $51 million
| gross          = $36,911,617
| awards         =
}}

The Bachelor is a 1999 romantic comedy film directed by Gary Sinyor and written by Steve Cohen. It is a remake of the 1925 film Seven Chances and stars Chris ODonnell and Renée Zellweger.  The film was also the debut of R&B singer Grammy-winner Mariah Carey as an actress.

==Plot==
Jimmie Shannon ( ), in the most romantic restaurant in town—only he spoils the special occasion with a very crude approach. Anne is going to  ), and his grandfathers two friends and colleagues (Ed Asner and Hal Holbrook) must find a bride in the next few hours. With a priest (James Cromwell) in tow, they begin.
 Napa for the night.
 oil futures trader, who, as it turns out, is engaged already. Second is Zoe (Stacy Edwards), a clingy window dresser. When Jimmie goes to see her, just after promising hed never leave her for another woman, he runs off after a woman in the street whom he thinks is Anne (which turns out not to be). He returns to find Zoe has set a mannequin on fire in effigy of him. He also strikes out with a melodramatic opera singer (Mariah Carey) and a tough-as-nails cop (Jennifer Esposito). Soon his list is depleted, but his last choice accepts— brittle, chain-smoking socialite Buckley (Brooke Shields), who detests Jimmie but wants his money to prop up her waning family fortune. As the priest tries to conduct the ceremony, she gradually learns the other conditions of the will: she and Jimmie must have children within five years, spend no more than one night apart per month, and need to stay married for at least ten years. Horrified, she drives off in a convertible. 

Anne misses Jimmie and heads back to the city. Trying to locate Jimmie, she calls Marco to arrange dinner with Jimmie. Desperate, Marco had earlier placed an ad for a bride in the newspaper. He figures a few women will show up at the appointed time and church for the offer.

As everyone scrambles to help Jimmie save the family business, Jimmie realizes the "effect" of marriage, as the kindly priest reveals how he took on the priesthood after his beloved wife died, and that he was proud to be married and produce a wonderful family in the process.

Realizing that he truly loves Anne and is ready to take the plunge, Jimmie, after being up all night, rests in the church where Marco had promised to deliver a bride. He awakens to find hundreds of women dressed as brides waiting for him. After trying to settle the women down, Marco lies and says it was all a prank. This angers them, and they try to rip the two men to shreds. Marco reveals that Anne is on her way back, so Jimmie flees to the train station, ordering a cake on the way. He makes it there after being chased by hundreds of brides. He finds Anne in the train, but she has discovered a newspaper with its front page asking, "Would you marry this man for $100 million?" with Jimmies picture beside. She is upset, but he professes his love for her and they reconcile.

Natalie finds a discarded wedding dress in the station, and Anne puts it on in the bathroom. She opens the door to see hundreds of would-be brides run past, chasing Jimmie. Jimmie flees through the streets of San Francisco. He eventually climbs up a flight on a fire escape ladder and shouts for Anne, as the would-be brides gather en masse below. The priest begins to conducts the ceremony over a loudspeaker from inside a police car, causing many brides to attack the car, and chaos ensues. Anne, in the crowd, makes her way through and up to Jimmie. Natalie yells at everybody to "Shut up!".  Anne convinces the other women to be happy and let it be her day.

The priest finishes the ceremony by pronouncing them husband and wife, to cheers from all, and Jimmie and Anne kiss. She then tosses her bouquet into the teeming crowd below.

==Cast==
*Chris ODonnell - Jimmie Shannon
*Renée Zellweger - Anne Arden
*Artie Lange - Marco
*Mariah Carey - Ilana
*Edward Asner - Sid Gluckman
*Hal Holbrook - Roy ODell
*James Cromwell - The Priest
*Marley Shelton - Natalie Arden
*Peter Ustinov - Grandad James Shannon
*Katharine Towne - Monique
*Rebecca Cross - Stacey
*Stacy Edwards - Zoe
*Sarah Silverman - Carolyn
*Jennifer Esposito - Daphne
*Brooke Shields - Buckley Hale-Windsor
*Anastasia Horne - Peppy Boor Pat Finn - Bolt

==Production==
There is not (nor ever was) a train to/from Napa/San Francisco; artistic license helps the plot along.

==Reception==
The Bachelor received negative reviews from critics, despite mixed reception from viewers. It currently holds a 9% rating on Rotten Tomatoes based on 67 reviews.

===Box office=== House on Haunted Hill making $7.5 million USD in its opening weekend.  The Bachelor ultimately grossed $37 million worldwide, and it was a major box office slump against its $51 million budget.

== References ==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 