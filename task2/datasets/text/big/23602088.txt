The Overcoat (1959 film)
{{Infobox film
| name           = The Overcoat
| image          = Overcoat (1959 film).jpg
| caption        = Film poster
| director       = Aleksey Batalov
| producer       = 
| screenplay     = Leonid Solovyov
| story          = Nikolai Gogol
| starring       = Rolan Bykov
| music          = Nikolai Sidelnikov
| cinematography = Genrikh Marandzhyan
| editing        = 
| distributor    = 
| studio         = Lenfilm
| released       =  
| runtime        = 75 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}
The Overcoat ( ) is a 1959 Soviet film directed by Aleksey Batalov, based on Nikolai Gogol story "The Overcoat".

==Cast==
* Rolan Bykov - Akaki Akakiyevich
* Yuri Tolubeyev - Petrovich
* Aleksandra Yozhkina - Petrovichs Wife
* Elena Ponsova - Landlady
* Georgi Tejkh - Important Person
* Nina Urgant		
* Aleksandr Sokolov		
* Rem Lebedev		
* Aleksey Batalov		
* Georgii Kolosov
* Nikolai Kuzmin		
* Mikhail Ladygin		
* Pyotr Lobanov
* Vladimir Maksimov		
* Gennadi Voropayev	

==External links==
*  

 

 
 
 
 
 
 

 