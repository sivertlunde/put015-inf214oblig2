An Angel at My Table
 
{{Infobox film
| name           = An Angel at My Table 
| image          = Angel_at_my_table_movie_poster.jpg Laura Jones
| based on       =  
| starring       = Kerry Fox
| director       = Jane Campion
| producer       = Grant Major Bridget Ikin
| music          = Don McGlashan
| cinematography = Stuart Dryburgh
| editing        = Veronika Jenet ABC Television New Zealand Channel 4 Hibiscus Films
| distributor    = Sharmill Films (AU) Fine Line Features (US) Artificial Eye (UK)
| released       =  
| country        = Australia New Zealand United Kingdom
| runtime        = 158 minutes
| language       = English
| budget         = 
| gross          = $1,054,638   
}}

An Angel at My Table is a 1990 New Zealand-Australian-British  film directed by Jane Campion.  The film is based on Janet Frames three autobiographies, To the Is-Land (1982), An Angel at My Table (1984), and The Envoy from Mirror City (1984).  The film was very well received, winning multiple awards including at the New Zealand Film and Television awards, the Toronto International Film Festival and received second prize at the Venice Film Festival. 

==Synopsis==
An Angel at My Table is a dramatisation of the autobiographies of New Zealand author   (adult). The film follows Frame from when she grows up in a poor family, through her years in a mental institution and into her writing years after her escape.

==Cast==
*Kerry Fox as Janet Frame (adult)
*Alexia Keogh as Janet Frame (adolescent)
*Karen Fergusson as Janet Frame (child)
*Iris Churn as Mother
*Kevin J. Wilson as Father
*Melina Bernecker as Myrtle
*Glynis Angell as Isabel
*Mark Morrison as Bruddie Frame (child)
*Sarah Llewellyn as June Frame (child)
*Natasha Gray as Leslie
*Brenda Kendall as Miss Botting
*Martyn Sanderson as Frank Sargeson

==Awards==
*New Zealand Film and TV Awards (1990):
**Best Cinematography: Stuart Dryburgh
**Best Director: Jane Campion
**Best Film
**Best Performance in Supporting Role: Martyn Sanderson
**Best Female Performance: Kerry Fox
**Best Screenplay: Laura Jones
*Toronto International Film Festival (1990):
**International Critics Award: Jane Campion
*Valladolid International Film Festival (1990):
**Best Actress: Kerry Fox
*Venice Film Festival (1990)
**Elvira Notari Prize: Jane Campion
**Filmcritica "Bastone Bianco" Award: Jane Campion
**Grand Special Jury Prize: Jane Campion
**Little Golden Lion Award: Jane Campion
**OCIC Award: Jane Campion
*Belgian Syndicate of Cinema Critics (UCC) (1992): Grand Prix
*Chicago Film Critics Association (CFCA) (1992):
**CFCA Award: Best Foreign Language Film
*Independent Spirit Awards (1992)
**Best Foreign Film: Jane Campion

==Impact and reception==
An Angel at My Table was the first film from New Zealand to be screened at the Venice Film Festival, where it received multiple standing ovations and was awarded the Grand Special Jury Prize despite evoking yells of protest that it did not win The Golden Lion.  In addition to virtually sweeping the local New Zealand film awards, it also took home the prize for best foreign film at the Independent Spirit Awards and the International Critics Award at the Toronto Film Festival. The film not only established Jane Campion as an emerging director and launched the career of Kerry Fox, but it also introduced a broader audience to Janet Frames writing.

Roger Ebert gave the film 4 out of 4 stars, stating; "  tells its story calmly and with great attention to human detail and, watching it, I found myself drawn in with a rare intensity".  The film also received praise in The Guardian where Derek Malcolm called it "one of the very best films of the year". The Sydney Morning Herald described the film as "deeply moving" and "visionary" while Variety described it as being "totally absorbing".

==References==
 

== External links ==
*  
*   at  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 