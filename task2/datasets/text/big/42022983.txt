Akhire Akhire
{{Infobox film
| name = Akhire Akhire
| image = akhire akhire.jpg
| director = Susanta Mani
| producer = Sitaram Agrawal
| screenplay = Srikant Goutam
| story = Siddharth Agrawal
| starring = Babushan Jhilik Bhattacharya Siddhanta Mahapatra Bijay Mohanty Aparajita Mohanty Smita Mohanty Harihara Mohapatra
| studio = Sarthak Entertainment
| released =  
| country = India Oriya
}} Oriya romance, drama, comedy film  produced by Sitaram Agrawal. The film features Babushan and Jhilik in the lead roles.The film is a remake of Telugu movie Ishq.   

==Synopsis==
Shiva’s parent and his sister Priya leave him alone due to his mischievous personality. But after Shiva promises to be gentle Priya with her parent return to Shiva. Meanwhile Priya encounter love interest with Rahul and by knowing this Shiva tries to kill Rahul and held unsuccessful. Rahul tries to maintain closer relation with Priya’s parent and at last Shiva realizes his mistake and helps unite Rahul and Priya.

==Cast==
* Babushan  ...   Rahul
* Siddhanta Mahapatra  ... 	Shiva
* Jhillik Bhattacharya  ... 	Priya  
* Samresh Rautrai  ... 	Kala  
* Bijoy Mohanty  ... 	Shivas father
* Aparajita Mohanty  ... 	Shivas mother
* Harihara Mahapatra  ... 	Rahuls friend
* Smitha Mohanty  ... 	Jaya
* Sasmita Pradhan  ... 	Puja

==Soundtrack==
The soundtrack of the film was composed by Malay Misra and  was distributed worldwide by Sarthak Music.

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Akhire Akhire (title song)
| note1           = 
| lyrics1         = Srikant Gautam
| extra1          = Namita Agarwal
| title2          = De De De Mo Maal Ta De
| note2           =  
| lyrics2         = Srikant Gautam 
| extra2          = Nibedita
| title3          = Runu Jhunu Chudi Kahe
| note3           = 
| lyrics3         = Srikant Gautam 
| extra3          = Namita Agarwal 
}} 

== References ==
 

==External links==
*  

 
 
 


 