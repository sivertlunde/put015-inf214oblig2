Road to Happiness
{{Infobox film
| name           = Road to Happiness
| image          =
| image_size     =
| caption        =
| director       = Phil Rosen
| producer       = Scott R. Dunlap Matt Taylor (story "First Performance") Robert Hardy Andrews (adaptation and screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Harry Neumann
| editing        = Carl Pierson
| distributor    =
| released       = 9 January 1942
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Road to Happiness is a 1942 American film directed by Phil Rosen.

== Plot summary ==
  John Boles) has just 
returned from Europe eager to see his family. Charley Grady (Roscoe Karns), his agent,
informs Jeff that his wife, Millie (Mona Barrie) has divorced him and has remarried to a millionaire, Sam Rankin.
Jeff has discovered that his narcissistic ex has sent Danny to military boarding school because
she would rather socialize with her friends.

Danny is glad his father is home from his two year baritone opera studies and is
happy to live with him again, although they share one room in a boarding house.
Jeff and Danny have no money and Jeff cannot find a singing
job.  He finds a radio job with an acting part as an Indian on a cowboy show.

Danny realizes he is in the way of his fathers dream to sing and tries to push him away,
but admits to his father that he cant lie to him.  His father insists on continuing
to act so that they can spend more time together.

Jeff gets his long awaited chance ....

== Cast == John Boles as Jeff Carter
*Mona Barrie as Millie Rankin
*Billy Lee as Danny Carter
*Roscoe Karns as Charley Grady
*Lillian Elliott as Mrs. Price
*Paul Porcasi as Pietro Pacelli
*Selmer Jackson as Sam Rankin
*Brandon Hurst as Swayne
*Sam Flint as Colonel Gregory
*Antonio Filauri as Almonti
*Harland Tucker as Foster

== Soundtrack ==
* John Boles - "Danny Boy"
* John Boles - "Vision Fugitive" (aria from Massenets "Herodiade")
* John Boles - "America"

== External links ==
* 
* 
 

 
 
 
 
 
 
 
 


 
 