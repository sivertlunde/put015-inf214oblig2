Rani Honnamma
{{Infobox film
| name           = Rani Honnamma
| image          = 
| caption        = 
| director       = Ku. Ra. Seetharama Shastry
| producer       = T. S. Karibasaiah
| writer         = 
| screenplay     = Ku. Ra. Seetharama Shastry
| story          = 
| based on       =  
| narrator       =  Rajkumar  Leelavathi
| music          = Vijaya Bhaskar
| cinematography = K. Prabhakar   Babulnath
| editing        = Bal G Yadav   M. Thathayya
| studio         = 
| distributor    = Girija & Suryaprabha Productions
| released       =  
| runtime        = 137 minutes
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 historical drama Rajkumar and Leelavathi in the lead roles.

== Cast == Rajkumar
* Leelavathi 
* Lalitha Rao
* B. Jayashree Narasimharaju
* Balakrishna
* M.N Lakshmi Devi
* Papamma
* G. V. Iyer

== Soundtrack ==
The music for the film was composed by Vijaya Bhaskar and lyrics for the soundtrack penned by Ku. Ra. Seetharama Shastry. The songs "Haarutha Doora Doora" and "Jeevana Hoovina Haasige" were received very well and considered one of the evergreen hits in Kannada film industry.

===Tracklist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Lyrics
|- 
| 1
| "Kolu Gejje Kaalu Gejje"
| S. Janaki
| Ku. Ra. Si
|-
| 2
| "Ettha Hogenu"
| P. Susheela
| Ku. Ra. Si
|-
| 3
| "Malagida Haavidu"
| P. Susheela
| Ku. Ra. Si
|-
| 4
| "Jeevana Hoovina Haasige"
| P. B. Sreenivas, P. Susheela
| Ku. Ra. Si
|- 
| 5
| "Haarutha Doora Doora"
| P. B. Sreenivas, P. Susheela
| Ku. Ra. Si
|-
| 6
| "Baara Neera Manohara"
| P. Susheela
| Ku. Ra. Si
|-
| 7
| "Naa Thaalalarenu"
| P. Susheela
| Ku. Ra. Si
|-
| 8
| "Srikrishnanekinta"
| P. B. Sreenivas, S. Janaki
| Ku. Ra. Si
|-
| 9
| "Raja Rajeshwariya"
| P. B. Sreenivas
| Ku. Ra. Si
|-
|}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 