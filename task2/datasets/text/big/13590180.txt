Magic Kitchen
 
 
{{Infobox film
| name           = Magic Kitchen
| image          = MagicKitchen.jpg
| caption        = Film poster
| director       = Lee Chi-Ngai
| producer       = Lee Chi-Ngai  John Chong
| writer         = Lee Chi-Ngai
| starring       = Andy Lau Sammi Cheng Jerry Yan Maggie Q Nicola Cheung Stephen Fung
| music          =
| cinematography =
| editing        = Kwong Chi-Leung Media Asia Films Sil-Metropole Organisation Media Asia Distribution
| released       =  
| runtime        =
| country        = Hong Kong
| language       = Cantonese
| budget         =
}} 2004 Cinema Hong Kong film, directed by Lee Chi-Ngai.

==Synopsis==
Yau became the proprietor and head chef of a successful restaurant/cafe since her mother died six months ago.  Yau cooks according to the large collection of recipes her mother, who had tremendous talent as a culinary artist but never found success in the restaurant scene due to sexism, created over the years, but has no confidence in herself as a chef.  When she visits Japan with her assistant Ho at the invitation of an Iron Chef style cooking show, she has a chance run-in with an old boyfriend, Chuen.  Over the next weeks back in HongKong, Yau struggles with resurgent feelings for Chuen even as she discovers that one of her best friends has started seeing him.  In the mean time, her steadfast supporter and assistant for the last three years, Ho, pushes her to be more bold in her art and experiment.  Love and food intersects with surprising twists as she finally decides to leave the past behind and compete in the cooking challenge.  In the end, love is the secret ingredient.

==Starring==
*Jerry Yan : Ho
*Sheila Chan : Yaus mother
*Sammi Cheng : Yau
*Nicola Cheung : Kwai
*Stephen Fung : Joseph
*Asuka Higuchi : Presenter of the "King Chef Show"
*Andy Lau : Chuen Yao (guest star)
*Law Kar-ying : Yaus father
*Maggie Q : May Anthony Wong : Tony Ho Michael Wong : Mook
*Daniel Wu : Kevin
*Clarence Hui
*Vincent Kok
*Law Wai-Keung
*Lee Lik-Chi
*Teddy Lin
*William So
*Michael Tong

   

==See also==
*Andy Lau filmography

==References==
 

==External links==
*  

 
 
 
 
 

 