Wild Bill (1995 film)
{{Infobox film
| name           = Wild Bill
| image          = Wild Bill (film poster).jpg
| alt            = 
| caption        = Original film poster Walter Hill
| producer       = Richard D. Zanuck Lili Fini Zanuck
| based on       =     Walter Hill
| narrator       = John Hurt
| starring       = Jeff Bridges Ellen Barkin John Hurt Diane Lane Keith Carradine Christina Applegate Bruce Dern James Gammon David Arquette Marjoe Gortner
| music          = Van Dyke Parks
| cinematography = Lloyd Ahern II
| editing        = Freeman A. Davies
| studio         = 
| distributor    = United Artists
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $30 million
| gross          = $2,193,982
}}
 Western film about the last days of legendary lawman Wild Bill Hickok. It stars Jeff Bridges, Ellen Barkin, John Hurt and Diane Lane. Walter Hill, with writing credits also going to Pete Dexter, author of the book Deadwood, and Thomas Babe, author of the play Fathers and Sons.

==Plot==

A well-known lawman and scout of the 19th Centurys western frontier, Wild Bill Hickok (Jeff Bridges) has drifted to Deadwood, South Dakota|Deadwood, Dakota Territory.  Jack McCall (David Arquette) is a young man whose mother and family have been slighted by Bill in the past, and is out for revenge.

Troubled by his on-again, off-again relationship with a woman called Calamity Jane (Ellen Barkin), haunted by the ghosts of his past, and struggling with failing eyesight, Wild Bill faces with grave concern the arrival of this dangerous newcomer to town.

== Cast == James Butler "Wild Bill" Hickok
* Ellen Barkin as Calamity Jane
* John Hurt as Charley Prince
* Diane Lane as Susannah Moore
* Keith Carradine as Buffalo Bill
* David Arquette as Jack McCall
* Christina Applegate as Lurline Newcomb
* Bruce Dern as Will Plummer
* James Gammon as California Joe
* Marjoe Gortner as Preacher
* James Remar as Donnie Lonigan
* Karen Huie as Song Lew
* Steve Reevis as Sioux Chief
* Pato Hoffmann as Cheyenne Leader
* Robert Knott as Dave Tutt Dennis Hayden as Phil Coe
* Peter Jason as Dave McCandless
* Lee de Broux as Carl Mann

==Production==

===Script===
The play Fathers and Sons had been on Broadway in 1978; Deadwood was published in 1986. Hill says he took details of the town from the novel but the relationship between McCall and Hickok was mostly from the play. The Wild Bill of History, Here Mostly Made Up: The Wild Bill of History, Here Made Up Waiter Hills script told of the last days of the visually impaired, opium-addicted gunslinger Bill Hickok.
By JAMIE DIAMOND. New York Times (1923-Current file)   26 Nov 1995: H13. 

Thomas Babe, the author of the play, says he entirely made up the character of McCall, who he turned into Hickoks illegitimate son.. "I chose lurid," he says. "That he would drown cats, sleep with his mother, put on a dress was a portrait of nihilism. He was someone Wild Bill would not have as a son." 

Babes play was seen in Los Angeles in 1980 by Walter Hill, who had been considering a film on Hickok. Hill:
 I was interested that Wild Bill was interested in the thing that finally killed him, his own legend. He was this kind of expansive American personality, a historical artifact... Babe was more interested in Jack   and the Oedipal issues, questions of identity and sexual identity. So what was at the heart of the play wasnt for me. But everything around the heart was.  
Hill optioned the play and a screenplay about Hickok by Ned Wynn. In 1989 he wrote his own screenplay.

Meanwhile the team of Richard and Lili Zanuck had optioned Dexters novel Deadwood after they hired him to write the script for the movie Rush. The Zanucks said they were interested in the project because it explored the nature of celebrity in a Western context. "Figures like Wild Bill were like rock stars," said Lili Zanuck. "They had sex appeal." 
 Walter Hill.  Hill took material from Dexters novel for the atmosphere of the town and relied on Babes play heavily for the third act, the last hours of Hickok.

===Shooting===
Hill said that Jeff Bridges was "an actor I greatly love... a very nice man, decent, hard working, got along well, no problems" but that there "was always a kind of tension between Jeff and myself" because "Jeff does a lot of takes, I dont. My focus is very intense, but when it gets to be you just doing it again and again I lose it and I find an awful lot of performers go stale. He would always have an idea he thought he could make something better."   accessed 12 July 2014 

==Reception==
The film received mixed reviews, with a 41% rating on Rotten Tomatoes.  and a 5.9 on the Internet Movie Database.   Roger Ebert gave the film two stars out of four, criticizing its pacing and plot.  He recognized the films ambition, aiming for "elegy" and "poetry" in its final act, but ultimately described it as flawed, writing, "We can see where its headed, although it doesnt get there."   In a positive reivew, Bruce Fretts of Entertainment Weekly wrote that the movie "succeeds as a character study of a man whose idiosyncratic code of justice eventually catches up with him," and complimented Jeff Bridges acting as vital to the films success.   Variety (magazine)|Variety, while also praising Jeff Bridges performance, took a critical stance, observing that the film "comes to a near dead-stop in the final stretch." 

Wild Bill bombed at the box office. Produced on a budget of $30 million, it took in just over $2 million in the United States alone.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 