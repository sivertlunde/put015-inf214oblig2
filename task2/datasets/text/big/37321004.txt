Gatchaman (film)
{{Infobox film
| name           = Gatchaman
| image          =  
| alt            = 
| caption        = Theatrical release poster
| director       = Toya Sato 
| producer       = 
| writer         = Yusuke Watanabe 
| based on       =  
| starring       = Tori Matsuzaka Gō Ayano Ayame Goriki Tatsuomi Hamada Ryohei Suzuki
| music          = Nima Fakhrara
| cinematography = 
| editing        = 
| studio         = Nippon Television Nikkatsu Django Film 
| distributor    = Toho
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese English
| budget         = 
| gross          = 
}}

  is a 2013 Japanese tokusatsu movie directed by Toya Sato based on the classic 1970s anime television series Science Ninja Team Gatchaman.   

==Plot==
By the year 2050 AD, a mysterious organization called Galactor has occupied half of the Earth and threatens to exterminate the human race. Around the same time, the International Science Organization had uncovered mysterious stones that bear unusual powers. It is said that one person out of approximately eight million is able to harness the power of the stones; they are known as a "Receptor". Dr. Kozaburo Nambu gathers a team of these five lucky Receptors together. Known as the "Gatchaman" team, it is their mission to defeat Galactor.

==Cast==
* Tori Matsuzaka as Ken Washio
* Gō Ayano as Joe Asakura
* Ayame Goriki as Jun Ohtsuki
* Tatsuomi Hamada as Jinpei Ohtsuki
* Ryohei Suzuki as Ryu Nakanishi
* Gorō Kishitani as Dr. Nambu
* Ken Mitsuishi as Dr. Kirkland
* Eriko Hatsune as Naomi
* Nakamura Shidō II|Shidō Nakamura as Iriya

==Production and release==
 

The theme song for the movie is "Niji wo Motsu Hito", performed by popular Japanese rock band Bump of Chicken.

==Reception==
 

In Japan, it opened in fifth place with ¥119,201,780 (approx. US$1,171,977) and had earned ¥401,196,315 (approx. US$3,944,512) by its third weekend. 

==References==
 

==External links==
*    

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 