Thirunakkara Perumal
{{Infobox film
| name           = Thirunakkara Perumal
| image          = Thirunakkara Perumal.jpg
| caption        = 
| director       = Prasad Valacherry
| producer       = Pallath Jain
| writer         = Kumarakam Baburaj
| narrator       = 
| starring       = Babu Antony Riyaz Khan
| music          = Surya Narayanan
| cinematography = Anandakuttan, Marimuthu
| editing        = P. C. Mohanan
| distributor    = 
| released       =  
| runtime        = 
| country        = Indian
| language       = Malayalam
| budget         = 
| gross          = 
}} Madhu and Seema in the lead roles.

== Plot ==
Thirunakkara Perumal is develops in Perumatthurkkara village. Both Hindus and Christians lived there peacefully.The Temple is off Perumathur Kovilakam, during the time of festival people of both religion took their part in ceremonies. If church people do not take part in the Temple festival, that year festival would not be taken place. As part of the ritual, Lakshmikutty Thampuratty (Seema (actress)|Seema) of kovilakam takes the sword and lamp of the temple.

After some years she fell in love with Mathai (Madhu (actor)|Madhu) of Kalappurakkal tharavad and later marries him. After this incidence, friendly relationship between the Hindus and Christians became stronger than before. Kulathungal Chacko and his brothers envied on this harmonious relation ship and later leads to changes in the story.

== Cast == Madhu ... Mathai Seema ... Lakshmikutty / Achamma
* Babu Antony ... Job
* Riyaz Khan ... Jose
* Lena Abhilash .. Gracy
* Lakshmi Sharma ... Sreekutty
* Kalasala Babu
* T. P. Madhavan
* Chali Pala  ... Pathrose
* Sreelatha Namboothiri ... Shoshamma
* Sadiq
* Jagathy Sreekumar Kalpana
* Anil Murali
* Kollam Thulasi ... Vasu
* Geetha Salam
* Ambika Mohan ... Sreekuttys mother
* Mahima

==External links==
* http://www.nowrunning.com/movie/5814/malayalam/thirunakkara-perumal/index.htm
* http://popcorn.oneindia.in/title/1260/thirunakkara-perumal.html

 
 
 


 