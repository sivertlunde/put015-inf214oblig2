Koyla
 
 
 
{{Infobox film
| name           = Koyla कोयला  
| image          = Koyla.jpg
| caption        = DVD Cover
| director       = Rakesh Roshan
| producer       = Rakesh Roshan
| story          = Rakesh Roshan
| screenplay     = Ravi Kapoor Sachin Bhowmick
| starring       = Shahrukh Khan Madhuri Dixit Amrish Puri
| music          = Rajesh Roshan Vangelis
| lyrics         = Indeevar
| cinematography = Sameer Arya
| editing        = Sanjay Verma
| distributor    = Filmkraft Productions Pvt. Ltd.
| released       =  
| runtime        = 167 minutes
| country        = India
| language       = Hindi
| budget         =   
| gross          =  
|}}

Koyla ( ,  }},  ) is a 1997 Hindi action thriller film produced and directed by Rakesh Roshan. The film stars Madhuri Dixit, Shahrukh Khan, Amrish Puri, Himani Shivpuri, Johnny Lever, Ashok Saraf and Kunika. Mohnish Behl is featured in a guest appearance. 

==Plot==
Raja Saab (Amrish Puri) is a powerful and greedy elderly owner of a coal mine. Some years ago he discovered diamonds in his mine and became rich, turning himself into the self-proclaimed king of the area and ruling over the workers with the help of his sadistic and cruel brother, Brijwa. In addition to the rest of the household, Raja has a mute slave named Shankar (Shahrukh Khan) who is completely loyal to him.
	
A glutton for young women, Raja keeps his secretary, Bindya (Deepshikha) as his mistress, though he is frequently unable to perform due to his old age. His doctor suggests that a different woman could properly arouse him. The next day, Raja comes across the beautiful Gauri (Madhuri Dixit), a poor but spirited village girl, and becomes obsessed with her. Desperate to have her, Raja extends a marriage proposal to Gauris aunt and uncle, along with lavish gifts. Gauri, not knowing who her suitor is, refuses to marry without first seeing the groom, regardless of his wealth and status. Knowing that she would never agree to marry such an old man, Raja sends a picture of the young and handsome Shankar instead. Gauri falls in love with Shankars picture and agrees to the marriage, already dreaming of her life as his bride.
	
Gauri discovers that she is marrying Raja and faints. Raja carries her through the last of the rites, thereby forcibly marrying her. When Gauri awakens later in his room Raja tries to have sex with her but she fights back. Raja leaves, claiming that it will be more fun to break her over time and one day she would come to him willingly. Later, Gauri attempts to escape but is caught by Brijwa. He returns her to Raja who hits her in front of the household. Shankar witnesses this and he and his friend, Chhote, set out to uncover what was really going on. They learn the truth of Rajas deceit from the doctor and return just in time to see Gauri try to hang herself. Shankar saves her but then she vents her wrath on him, believing that he was part of Rajas plan to entrap her and not realizing that he was mute. Chhote then arrives and tells Gauri the truth as Shankar slips away before she can apologize.
 	
That night Bindya, frustrated with Raja for being unable to perform once again, goes looking to seduce Shankar, but runs into Brijwa instead who tries to rape her. Shankar hears her cries and rescues her. When Brijwa claims to Raja that Shankar tried to rape his mistress, Raja beats Shankar badly until Bindya intervenes, confessing that she sought out Shankar, not the other way around. Furious at her betrayal, Raja sells Bindya to the local whore house, instructing the madam to sell her frequently as punishment. Gauri, who now believes Shanker is innocent, attends to his wounds and forms a bond with him.
	
Gauris brother, Ashok, comes home and learns that his sister was married during his absence. Raja threatens Gauri that if she doesnt lie to her brother that she is happy, then Raja will have him killed. Ashok buys her performance and leaves, but Shankar, who cant stand to see Gauri in pain, runs after him and conveys what is really going on by writing a message in the dirt. Ashok returns and witnesses Rajas cruelty to his sister. He tries to take her away but he is murdered by Raja. Before dying he entrusts Gauris care to Shankar and begs him to take his sister away from there. Shankar fights off Raja and Brijwa and the two escape into the mountains.
	
Raja and the local law enforcement go off in search of the pair, but his men are slowly picked off by Shankar. Raja is advised by Brijwa to retreat as they dont know the jungle like Shankar and are a disadvantage. Raja agrees and the forces leave. Shankar and Gauri celebrate, admitting their love for one another and rejoicing in their freedom. Suddenly, Gauri is shot in the arm and a troop of soldiers encircle the pair. Raja had actually gathered reinforcements and returned to capture them. After the men beat Shankar badly, Raja slits his throat and tosses him off the mountain side. Gauri is taken back to the village and sold to the same whorehouse as Bindya. Bindya, taking pity on Gauri, and because she owes a debt to Shankar for saving her that night, protects the woman he loves, fighting off anyone who tries to touch her.
	
Maenwhile, a village boy finds Shankar hanging from a tree at the base of the mountain and realizes that hes still alive, though just barely. Shankar is taken back to the boys home and operated on by an elderly healer. When Shankar awakens, the man tells him that he found that Shankar wasnt actully mute by birth, but endured some kind of physical trauma at young age that scarred his vocal chords. Its revealed through a flashback that Shankars father, Hariya Thakur, was a worker in the coal mine. It was he, not Raja who had first discovered the diamonds. He informed Raja, who was just a foreman at the time, and ran home to tell his wife. However, once he arrives home, Hariya and his wife are murdered by two men. Shankar witnesses this and threatens to tell everyone what he saw. Just then, an unknown third man comes up behind him and forces coal into his mouth, destroying his voice. Later, Raja consoles Shankar and, when the boy spots the two murderers driving in the village, Raja has them killed by the D.I.G. (head policemen). Indebted to him, Shankar becomes Rajas slave and had remained loyal up until Gauris arrival. The healer tells Shankar that, during the surgery, hed found a suppressed nerve that was preventing Shankar from being able to speak. The healer had repaired the damage, allowing Shankar to be able to use his voice once again. Shankar heals quickly, desperate to return and save Gauri.
	
Back at the palace, Raja prepares to welcome two old friends, Vedji and Dilavar, who turn out to be non other than the two men who murdered Shankars parents all those years ago. Raja learns of Bindya protecting Gauri from harm and sends Brijwa to the town to stop it. While trying to fight off the men, Bindya is stabbed to death by Brijwa and Gauri is taken to the market to be auctioned off. Shankar arrives just in time to see Vedji and Dilavar buy Gauri. Before he can catch them, he is spotted by Brijwa. The two fight and Shankar eventually kills Brijwa by throwing him headfirst into a coal oven. Shankar then chases down the two men and kills Vedji by dropping him off a cliff. Before he dies, Vedji confesses that the third man, the one who had stuffed burning coal down Shankars throat, was Raja himself. Raja had his friends kill Shankars father so that he could claim the diamonds as his own, then, with the help of D.I.G., faked Vedji and Dilavars deaths and gained Shankars trust. Shankar reunites with Gauri and the two decide to make Raja pay for what hes done.
	
Shankar sends Vedjis body back to Raja with a tape recorded message promising the deaths of the remaining Dilavar, D.I.G., and Raja. Raja, who doesnt recognise Shankars voice, has no idea who the new enemy is. That night, with the help of Chhote and Rajas doctor, Gauri lures Dilavar to the coal mine where Shankar is waiting. Shankar kills him and hangs his body at the mine for all to see. In the morning, Raja and D.I.G. arrive at the mine and see Dilavars body. Shankar reveals himself as the mysterious voice and tells the gathered mine workers the truth about Raja and what happened to his parents all those years ago. The miners revolt and, in the ensuing fight, Shankar kills D.I.G. by forcing a grenade in his mouth. Shankar then chases Raja down and exacts his revenge by setting Raja on fire and watching him burn to death. In the end, Shankar and Gauri embrace each other, both of them free at last.

==Cast==
* Shahrukh Khan as Shankar	
* Madhuri Dixit as Gauri	
* Amrish Puri as Raja Sahab	
* Johnny Lever as Shankars friend (Chhote)	
* Deepshikha as Bindya	
* Salim Ghouse as Brijwa (Raja Saabs brother)	
* Ranjeet as Dilavar	
* Ashok Saraf as Vedji	 Pradeep Rawat as D.I.G.	
* Kunika as Raseli	
* Himani Shivpuri as Chandabai (Pimp/Madam)	
* Mohnish Behl as Ashok (Special appearance)
* Razzak Khan as a party guest	
* Shubha Khote as Gauris aunt

== Soundtrack ==	
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"	
|- bgcolor="#CCCCCC" align="center"	
! # !! Title !! Singer(s)!! Length
|-	
| 1 	
| "Tanhai Tanhai"	
| Alka Yagnik, Udit Narayan	
| 05:35	
|- 	
| 2 	
| "Badan Juda Hote Hain"	
| Kumar Sanu, Preeti Singh	
| 10:30	
|-	
| 3	
| "Dekha Tujhe To"	
| Kumar Sanu, Alka Yagnik	
| 07:32	
|- 	
| 4	
| "Sanson Ki Mala Pe" 	 Kavita Krishnamurthy	
| 06:47	
|-	
| 5 	
| "Bhang Ke Nashe Mein"	
| Alka Yagnik 	
| 06:07	
|- 	
| 6	
| "Ghunghte Mein Chanda Hai"	
| Udit Narayan	
| 06:17	
|}
Certain Instrumental pieces used in the film appeared to be inspired by OST of 1492:Conquest of Paradise composed by Vangelis.

==Box Office==
Koyla performed moderately at the box office.

==References==
 

== External links ==
*  

 

 
 
 
 
 