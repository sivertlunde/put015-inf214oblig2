Partners (2009 film)
 
{{Infobox film
| name = Partners
| director = Peter James Iengo
| producer = Peter James Iengo
| writer = Peter James Iengo
| cinematography = Peter James Iengo
| starring = Christoper Iengo Adam Piacente Anthony Vaccarella Aaroon Katter
| music = Christopher Sauter
| distributor = Synkronized US LLC
| released =  
| runtime = 130 minutes
| country = United States
| language = English
| budget = $50,000
| gross = $5.68 million
}}
Partners is a film released in 2009.

==Production==
Shooting took place all in New York, Brooklyn, Staten Island and Long Island.  Produced by indie filmmaker Peter James Iengo. Equipment by Abel Cine Tech and TCI, shot in HD using the JVC GYHD100U.  Filming took place in 2008 and wrapped in the summer or 09.  Partners went on to win Best Crime Drama for feature film at the 2009 Staten Island Film Festival.

== References ==
* 

== External links ==
*  

 
 


 