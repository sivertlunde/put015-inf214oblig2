Kie Kahara
{{Infobox film
| name = Kie Kahara
| image = 
| caption        = Poster
| director = Nitai Palit
| writer =   Nitai Palit,  Gopal Chotrai,   Bhim Singh
| starring =  Sarat Pujari  Saudamini Misra  Dhira Biswal  Lila Dulali
| producer =  Dhira Biswal
| music   = Akshaya Mohanty
| cinematography = Dinen Gupta
| editing = Ramesh Joshi
| studio = Pancha Sakha Films
| released = 12th June 1968
| country  = India
| runtime =  178 min Oriya 
| website        = 
| amg_id         = 
}}
 Oriya film directed by Nitai Palit.    The film reflects the socio-economic conflicts of the declining Zamindar families, which was struggling to keep up social status with great difficulties. 

==Synopsis==
Prakashs father leaves him after his wifes death and gives Prakashs custodian to his sister Mamata. Mamata gives a good education to Prakash and he becomes an engineer. Prakash gets a job in town, In the town, he meets Rajendra Samantrais family and stays in their home as tenant. Rajendra Samantrais forefathers are Zamindar. Rajendra struggles enough to fulfill the  demands of his wife and her status. in the meanwhile Rajendras daughter Malati falls love with Prakash. Rajendra accepts their love ship but Subhadra prefers Nagen, who is the son of a wealthy contractor Nabin Mahapatra over Praksah. Subhadra believes Nagens wealth can helps them to maintain their status. When Nagen comes with an offer for his marriage with Malati, Rajendra straightly  denies this. The offended Nagen tries disrupt Rajendras family. Also Nagen gets success to create a confusion Prakash and Malati. At last Prakashs father arrives the scene and ends all confusion. At last Prakash and Malati gets married.

==Cast==
* Sarat Pujari - Parakash
* Saudamini Misra - Malati
* Samuel Sahu - Rajendra Samantrai 
* Lila Dulali - Subhadra
* Dhira Biswal - Nagen Mahapatra
* Ramchandra Pratihari - 	Nabin Mahapatra
* Bhanumati Devi - Mamata	
* Sagar Das -Ashok	
* Shyamalendu Bhattacharjee	- Servant
* Niranjan Satapathy -	Kartik 
* Narendra Behera	- Prakashs friend
* Bimal Choudhury - Prakashs father		
* Geetarani		
* Saraswati		
* Pira Misra		
* Bhim Singh

==Soundtrack==
The music for the film is composed by Akshaya Mohanty.  
{| class="wikitable sortable" style="text-align:center;"
|-
! Song
! Lyrics
! Singer(s)

|- Chakori Jharana Akshaya Mohanty
|- Hai Madabhari Shyamal Mitra
|- Hema Harini Nirmala Mishra, Shipra Bose
|- Udasi Aakhi Arati Mukherjee, Ramola 
|- Udi Udi Akshaya Mohanty Akshaya Mohanty
|- Phulei Rani Debdas Chotrai Akshaya Mohanty, Sipra Bose
|- Paradesi Bandhu Debdas Chotrai || Sipra Bose
|}    

==Box office==
The film created sensation and ran to packed houses for about five months in Cuttack city only.  It was the first Oriya film to run more than 100 days at several theaters.  The Film became a Box office hit. 

==References==
 

==External links==
* 

 
 
 
 