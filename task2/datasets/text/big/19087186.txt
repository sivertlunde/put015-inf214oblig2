Noah's Ark (2007 film)
 
{{Infobox Film
| name           = Noahs Ark
| image          = 
| caption        = 
| director       = Juan Pablo Buscarini
| producer       = Pablo Bossi Alejandro Cacetta Roberto Di Girolamo Juan Pablo Galli Giuliana Migani Ariel Saúl Camillo Teti Patricio Tobal Juan Vera
| writer         = Axel Nacher Fernando Schmidt Enrique Cortés (screenplay adaptation)  Barbara Di Girolamo Juan Pablo Buscarini (script collaborator)
| starring       =See Cast
| music          = Andrés Goldstein Daniel Tarrab
| cinematography = 
| distributor    = Buena Vista International
| released       =   (Argentina)
| runtime        = 88 minutes
| country        = Argentina Italy Spain
| language       = English/Spanish
}}

Noahs Ark, http://www.filmexport.com/interno/what%27s_new/immagini/presbook_english.pdf  El Arca ("The Ark") in the original English/Spanish version, is a 2007 Argentine-Italian film directed by Juan Pablo Buscarini and based on Noahs Ark.  The film focuses more on the animals point of view. The story tends to follow the traditional biblical story; however, both the humans and the animals involved are seen as "talking" creatures throughout the film.

==Plot==
In the opening scene, animals and humans are seen acting out the seven deadly sins: pride (the peacock), envy (the snake), sloth (the sloth), lust (the hedgehog), gluttony (the toad), wrath (the mandrill), and greed (the human). This results in their mutual doom, as they are killed or captured and taken to market where the survivors are enslaved and the remains of the killed animals are sold.
 Judgment Day upon the sinful world. A chance encounter with the gentle and devoted Noah, who purchases the freedom of the enslaved man despite his own poverty, convinces him to give humanity one last chance.
 Close Encounters of the Third Kind) and begins cutting down his fathers forest to begin its construction.

Noah sells his home to the greedy, dishonest merchants Farfan and Esther for a flock of pigeons to deliver messages to all the animals of the world. However, the birds immediately abandon their task and head for a jungle strip club to celebrate their freedom. Only one pigeon, Pepe, remains on mission, but is attacked by the animals he encounters until he is rescued by the lioness Kairel, secretary to King Sabu and Queen Oriana. Kairel delivers Noahs message to Sabu, who calls an emergency meeting of the animal world.

Sabus spoiled libertine son Xiro has gotten a mangled portion of one of Noahs messages and mistakes the announcement of apocalyptic doom as an invitation to a pleasure cruise. Xiro throws a tantrum after Kairel disqualifies all of Xiros potential cruise-mates for lack of intelligence, and the tiger Dagnino sees an opportunity to seize the crown of the post-flood worlds animal kingdom.

As Noah single-handedly completes the ark (an impressive but not Biblically-accurate craft with a Ships wheel|helm, thousands of luxury cabins, and other features more suited to a modern cruise liner), the animals arrive en masse. Kairel has been sent along to organize and supervise the trip, but the procession grinds to a halt when the herbivores demand assurance that the carnivores wont devour them once theyre aboard. Xiro (who has spent the entire trip making out with the mean-spirited lioness Bruma in a private Litter (vehicle)|litter) fails to make a decision, allowing Dagnino to gain the animals respect by declaring that he will personally punish any act of violence on the voyage.

As the rain begins to fall, the animals stampede onto the ark. Bruma pauses to taunt the doomed Kairel, only to be smashed through the gangplanks by a toppling hippopotamus. Xiro impulsively grabs Kairel and brings her aboard the ark in Brumas place.
 I Will Survive. However, Panthy is part of Dagninos small cabal of carnivores who intend to discredit and overthrow Xiro, then establish a new order in which all prey species are bred and fattened in captivity until they are consumed.

Kairel finally gets Xiro to face the truth of their situation, and Xiro makes a sincere effort at governance. He grows closer to Kairel, but finds bureaucracy stifling and is still infatuated with Panthy, to Kairels jealous dismay.

Trapped below decks with the animals, Farfan and Esther disguise themselves as a fictional animal species ("grasswhoppers") to avoid being discovered as humans. The disguises backfire when the pairs misadventures leave them under a pile of animal dung which is hauled to the top deck for disposal. Believing a pair of dangerous wild animals have escaped the hold, Noahs sons and daughters attempt to catch or kill them. In the chaos, Farfan, Esther, and Noah plunge back into the depths of the ark. Noahs eldest son, Japeth, volunteers to rescue him, but quickly retreats upon hearing frightening noises below (which turn out to be the hippopotamus suffering intestinal distress). Noahs faithful pigeon Pepe tries to fly down to Noah, but instead plummets helplessly because of his injuries. Noahs sons fight over the helm and break it. Ashamed and desperate, Noahs family prays for Gods help.

Farfan and Esther knock Noah unconscious and abandon him. Feeling cocky with Noah out of the way, Farfan begins taunting the animals until he inadvertently strikes Dagnino. The two humans escape uninjured, but Dagnino tears off the lower half of Farfans hide, which is all he needs for his scheme. Panthy lures Xiro to her cabin, where Dagninos minions use the torn disguise and tomato juice to frame Xiro for the murder of the grasswhoppers. Dagnino assumes control and has Xiro locked up in a storeroom, but Xiros herbivore friends quickly unravel the deceit and convince Kairel of the truth.

Noah regains consciousness and tends to Pepe before sending him on a mission to find land. Xiro has been imprisoned directly below and overhears Noahs words of encouragement, mistaking them for a personal message from "the voice of his lineage." When his friends free him, Xiro races to confront Dagnino, who has captured the other herbivores. (Xiros group challenges Dagnino with the Maori ritual known as a haka.) The battle between Dagnino and Xiro ends when the ark, having drifted into the Arctic, runs into an ice floe. The sudden stop pitches Dagnino and his minions through a wall, in which their heads are stuck fast.

The animals begin to panic again, threatening to flee onto the ice until Xiro finally asserts his authority with a mighty roar. Xiro delivers a speech that rallies the animals behind him (even many of Dagninos gang, to Dagninos chagrin). On the upper deck, Noah has made his way back up to his family, who are likewise now rallied behind him. Noah and his sons begin repairing the helm.
 pitch for the ships torches will melt the ice, and puts the animals to work spreading barrels of it across the floe. Xiro lights the pitch, breaking up the floe and freeing the ark.

Farfan and Esther, believing the ark has run aground, break out and fall onto the floe just as the ark departs. They are last seen fleeing the hungry polar bears who have elected to remain behind in their natural habitat.

Pepe returns to the ark with an olive leaf, but is once again inadvertently clobbered before he can reach his goal. Xiro and Kairel reconcile their different ways as the animals (including a caged Panthy, and Dagninos gang still stuck in the section of wall) celebrate with a party on the open deck. God enjoys the festivities from above, but admonishes Angel for leaving the rainbow on.

During the end credits, God and Angel bicker over the contents of Gods work-in-progress book.

==Home media==
El Arca was released on DVD in the United States by Shout! Factory under the title Noahs Ark on March 11, 2014. This edition has both an English and a Spanish audio track, with English subtitles.

The feature film has been noticeably edited to remove sexual references and some crude humor. Edits include (but are not limited to) the show inside the jungle strip club, a pigeon defecating, Xiros discovery and misreading of the torn message from Noah, Xiros complaint about Kairels rejection of each of his potential cruise-mates, the entirety of Panthys performance of I Want To Live, a background running gag about the donkeys assorted trysts aboard the ark and the scenes of God and Angel arguing during the end credits, as well as individual lines from Panthys attempts to seduce Xiro and Dagninos threat against Kairel.

==English-language cast==
* . In fact, God takes more interest in the creation of the Bible than he does for the well-being of the mortals who were chosen to create the new world. This form of God is depicted as a man of African descent with blond hair (as opposed to most other depictions, where God is designed as a Caucasian man with white hair).
*  and is often asking God for advice.
*Joe Carey- Noah: An elderly man with the heart of a saint and the main protagonist (for the humans point of view). A holy man, he and his family are chosen to be the saviors of humanity and all of the animals. He is easily sighted by his long grey and white beard, his glasses and his simple maroon clothes.
*Kay Brady- Naama: Noahs wife. Though she has been willing to stand beside Noah for years, even she has become concerned for Noahs mental health. Like Noah, she is heavily religious. During the trip to the New World, Naama has to do everything she can to ensure that her family stays together.
*Andrio Chavarro- Japeth: Noahs middle child. He is distinguished by his brown hair and his fairly light-colored clothes.
*Oscar Cheda- Shem: The oldest of Noahs children. He is the most loyal of Noahs sons and (after Naama) is often the first to stand by Noah. He is more heavily-set than his brothers, which helps him stand out amongst them, and he also stands out because of his red hair, his beard and his blue clothes.
*Brandon Morris- Ham: The youngest of Noahs children (and the only African one). He sticks out a bit more than his brothers because of his large, black afro and colorful red clothes.
*Lissa Grossman- Miriam: One of Noahs daughters-in-law. More emotional than the other females of the house, she is often the one who is hurt the most easily; however, she is much kinder than her other sisters-in-law. She is married to Ham. She stands out because of her red hair and blue clothes.
*Loren Lusch- Sara: Another of Noahs daughters-in-law. The most temperamental of the group, she is married to Shem. She tends to be very frank with the people she meets. She stands out because of her dark hair and white clothes.
*Aubrey Shavonn- Edith: The last of Noahs daughters-in-law. Calm and cool, she brings some sense to any situation (though she can be a tad sarcastic, if the need arises). She is married to Japeth. She stands out because of her blond hair and red clothes.
* , he is the only one of his kind to actually deliver the messages that Noah sent out to the animals. A running gag throughout the movie is that he is constantly getting hurt (quite by accident, in most cases).
*  who was the king of all animals before the flood. Receiving Noahs message of the coming apocalypse from Pepe, he calls a gathering of all the animals in his kingdom to warn his people. Though he doesnt force the animals to go to Noahs ark (as some animals believed it to be a trap), he made it clear that if Noahs apocalyptic warning wasnt heeded and if it turned out to be a reality, those who failed to listen would have only themselves to blame. Knowing that his life will be ending soon, he (and the other animals whose times are almost up) stays behind and lets the procession of animals leave for the ark, declaring them "the salvation of the new world". It is believed that he was a wise and just ruler (which may explain why all of the animals were willing to congregate on his order).
*Heidi Harris- Queen Oriana: A lioness who was the wife of King Sabu and the queen of all the animals before the flood. Knowing that the time would come when her son, Xiro, would succeed her and her husband, King Sabu, she is more than willing to allow Xiro to partake in the journey to the New World. She trusts Kairel deeply, having her as a royal aide and trusting her to do some important tasks (i.e. ensuring that the pilgrimage to the ark runs smoothly).
*  and the main protagonist for the animals point of view. The son of King Sabu and Queen Oriana, Xiro is expected to be a leader to all of the animals; despite this, the prince is (at first) more concerned with having fun and enjoying himself (though he does mature as the story goes on). Xiro is something of a womanizer, as he is easily swayed by beautiful creatures, which often leaves him in dire situations. Despite his being a predator, he actually has a friendship with herbivores (among them Alvaro the pig, a panda and a mole).
*Keller also served as the voice of Farfan: One of the two con artists who proves to be a thorn in the side of Noah, his family and the animals. He is somewhat arrogant, as he states that he "has a gift for finding idiots". The shorter of the two con artists, he is more easily distinguished than his female partner-in-crime by his green skin and his baldness. The Jungle Book.
*Heidi Harris- Bruma: A lioness     and a minor antagonist from the animal side of the story. Though she chosen as one of Xiros potential mates, Kairel (who was ordered to screen each of Xiros potential companion candidates) rejected her because she was "too skinny   no brains"; despite this, Xiro took her along as a companion, anyways. It is apparent that she isnt a very good person, as Kairel claims that, in her youth, she did some regrettable things that made her a wholly unimpeachable woman; she insists, though, that she "had no idea what she was doing". Its apparent that she and Kairel dont get along; to be honest, she hasnt built up much of a good relationship with the other animals in the jungle, either (judging the fact that most of them have nothing nice to say to her). She never lives long enough to get on-board Noahs ark because of an unexpected chain reaction accident that ultimately either smashes her flat (courtesy of a hippopotamus) or leaves her to drown, thus causing her to die.
*Chloe Dolandis- Kairel: A lioness. It is apparent that she is high-ranking in the lion society, as she served as an aide to Queen Orianna. She can be strict (having worked with royals, she most likely had to learn about responsibility very quickly) and has a secret crush on Xiro. 
*Antonio Amadeo- Pity The Parrot: One of the more prominent birds in the movie, Pity is famous as an entertainer in the arks comedy club "The Dive". Having been around humans most of his life, Pity always has a hilarious story to tell. He sticks out because of his partly overgrown upper beak, his body (which is mostly covered in green feathers) and his loud voice.
*  and the main antagonist from the animals point of view. Realizing all too well that the oncoming flood will mean a new world order, he conspires with several other carnivores in an effort to take over the ark and make himself king. Possessing both savage brawn and impressive mental prowess, he plots to disgrace Xiro and take his place as king of the beasts. Despite the fact that there is a female tiger already on board the ark, Dagnino seems to be in a relationship with Panthy.
* . He prefers to let his brute strength do the work for him, as he isnt much of a talker. He serves as something of an enforcer to Dagnino. He seems to be blind in his left eye (which is entirely yellow).
* . He is usually seen sitting on Cocos shoulder in a fashion similar to the way that a parrot sits on a partially blind mans shoulder.
* . He is perhaps the most minor of the animal mutineers, as he rarely (if ever) says anything.
* . Despite being a mutineer, he actually seems to care for the well-being of Xiro.
* . Being the only female of the animal mutineers, she uses her feminine looks and her brains to help her fellow conspirators. She serves as the super-star of The Dive.
*  and one of the minor protagonists of the movie. Though he is hesitant to trust predators at first, as the story goes on, he develops something of a friendship with Xiro. Though he is somewhat low on the food chain, he is surprisingly brave, as he openly shows his hatred towards most of the carnivores.
*Amy London- Esther: The second of the con artists. Apparently, she comes from a family of criminals, as she mentions that her grandfather was a swindler, but he ended up killed by a former partner (who may have been Farfan, given his reaction). She has some proficiency with a needle. The taller of the two con artists, she is distinguished by a healthier skin coloration, red hair and earrings.
*  and one of the minor protagonists for the animals point of view. Despite being a female, she is something of a brute, as she can easily take out anyone (regardless as to whether they are friend or foe) with but one punch. Despite this, she is a softie deep down, as she does care deeply for her mate, Bombay, and will protect him to the end. Her shaggy haircut and dark fur help her to stick out amongst a crowd.

===Additional voices===
*Rusty Allison 
*Robin Barson 
*David Driesin 
*John Felix 
*Christy Hardcastle 
*David K. Wait 
*Josh Wetherington 
*Stacey Schwartz 
*David Steel

==References==
 
#  .

==External links==
*  

 
 
 
 
 
 
 
 
 
 