Mt. Zion (film)
 
 
 
{{Infobox film
| name           = Mt Zion
| image          = MtZionPoster.jpg
| caption        = 
| director       = Tearepa Kahi
| producer       = Quinton Hita Christina Milligan
| writer         = Tearepa Kahi Stan Walker David Wikaira-Paul Darcey-Ray Flavell-Hudson Temuera Morrison Troy Kingi Miriama Smith Stan Walker
| cinematography = Fred Renata
| editing        = Paul Maxwell
| studio         = Small Axe Films
| distributor    = NZ Film Commission/20th Century Fox Sony Music Australia
| released       =  
| runtime        = 
| country        = New Zealand Australia
| language       = Māori English
| budget         = 
| gross          = $200,707
}} Stan Walker and Temuera Morrison.  This film marks the acting debut for singer Stan Walker.  The world premiere of the film was held at the Event Cinema at Manukau on 4 February 2013.
==Cast==
*Stan Walker as Turei
*Temuera Morrison as Dad
*Miriama Smith as Layla
*Darcy Ray Flavell-Hudson		
*Will Hall		
*Kevin Kaukau as Booker
*Troy Kingi
*Graham Ryan
*David Wikaira-Paul
==Production==
Filmed in Pukekohe.

==Plot==
Tureis family are hard-working potato farm workers in rural New Zealand. A talented musician, Turei dreams of his band being the support act for Bob Marleys 1979 tour. But its a dream that challenges the traditions and values of his upbringing and will set him at odds with his family - particularly his father, a true man of the land.

==Reception==
Russell Bailey from New Zealand Herald gave the film 4/5 saying it is a smart, finely-observed, heartfelt drama of good humour and decent tunes against an authentic local setting.
  Jake Wilson from Sydney Morning Herald gave it 1 and a half stars saying the film doesnt have the charm or skill for a big feelgood success and Walker only relaxes when he sings. 

==Soundtrack==
The soundtrack was released on 15 March 2013. 
 Third World, Max Merritt & The Meteors, and Herbs (band)|Herbs.

===Track listing=== Take It Easy" - Stan Walker
#"Mt Zion" - Small Axe
#"Heat Wave" - Small Axe
#"Dreadlock Holiday" - 10cc
#"(You Gotta Walk) Dont Look Back" - Peter Tosh
#"Now That We Found Love" - Third World
#"The Harder They Come" - Jimmy Cliff
#"Funky Kingston" - Toots and the Maytals
#"Burning Spear" - Marcus Garvey
#"I Can See Clearly Now" - Johnny Nash
#"Hoki Mai" - Prince Tui Teka
#"Soul Deep" - Small Axe
#"Lion Trail" - Small Axe featuring Troy Kingi
#"Maunga Hiona" - Small Axe featuring Che-fu
#"Azania (Soon Come)" - Herbs
#"I Need Your Love" - Golden Harvest
#"Slippin Away" - Max Merritt & The Meteors
#"Army" - Small Axe
#"Basket" - Small Axe

==Weekly charts==
In New Zealand The Album debuted at #11, before peaking at #1 on 18 February 2013.

{| class="wikitable sortable plainrowheaders" Chart (2013) Peak position
|- New Zealand Singles Chart 
| style="text-align:center;"| 1
|-
|}

==References==
 
 

==External links==
*  
* 

 
 
 
 
 