Detective School Dropouts
{{Infobox film
| name           = Detective School Dropouts
| image          = DetectiveSchoolDropoutsMoviePoster.jpg
| image size     = 
| alt            = 
| caption        = Out-of-print 1986 VHS video cover.
| director       = Filippo Ottoni
| producer       = Yoram Globus Menahem Golan
| writer         = David Landsberg Lorin Dreyfuss
| starring       = David Landsberg Lorin Dreyfuss Valeria Golino
| studio         = The Cannon Group
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = United States/Italy
| language       = English
| budget         = 
| gross          = $22,123 
}}
Detective School Dropouts is a 1986 action-comedy co-starring and written by David Landsberg and Lorin Dreyfuss for The Cannon Group. 

==Plot==
Two bumbling private detectives get themselves hired to find a missing person. They quickly find themselves in the middle of a mob war that takes place between New York City and Rome, when it turns out that the missing person is somebody the mob wants to stay missing.

==Cast==
*David Landsberg as  Donald Wilson
*Lorin Dreyfuss as  Paul Miller 
*Christian De Sica as Carlo Lombardi George Eastman as Bruno Falcone
*Valeria Golino as  Caterina Zanetti 
*Mario Brega as  Don Lombardi
*Rik Battaglia as Don Zanetti 
*Alberto Farnese as  Don Falcone 
*Ennio Antonelli as  Riccardino
*Giancarlo Prete as Mario Zanetti

==Overview==
The film was not successful at the box office, but it has gained cult film status in recent years, with out-of-print, used VHS tape copies selling on Ebay and Amazon.com for as much as $50.00 USD. It is noted for its low-budget production values, cheesy 80s synth soundtrack, and being the only film released from the Cannon Group to have lost money at the box office. However, it is also representative of 80 culture at the time when slapstick briefly resurfaced in popularity with American audiences.

"Detective School Drop Outs" was released on DVD (via Manufacture on Demand) from MGM on Oct. 2011.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 


 