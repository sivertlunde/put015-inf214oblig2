Baltic Storm
{{Infobox Film
| name           = Baltic Storm
| image          = Baltic Storm (2005).jpg
| image_size     = 
| caption        = DVD cover
| director       = Reuben Leder
| producer       = Kaj Holmberg 
Jutta Rabe
| writer         = Reuben Leder
| narrator       = 
| starring       = Greta Scacchi Jürgen Prochnow Rein Oja Donald Sutherland
| music          = Mauri Sumén
| cinematography = Nicolas Joray    
Robert Nordström   
| editing        = Alan Strachan
| distributor    = 
| released       = October 16, 2003
| runtime        = 110 minutes
| country        =  English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Baltic Storm is a 2003 film written and directed by  , the film focuses on the confirmed transport of defense materials by the Swedish Armed Forces, and alleged cover-up of the true cause of the disaster.

==Plot==

The plot portrays the Swedish government as being responsible for using the ship to covertly transport Russian high-tech components to the United States. The story is uncovered by a young female journalist, not unlike Rabe herself. According to Rabe, divers hired by the Swedish government (signing contracts swearing lifetime secrecy) spent hours breaking into cabins frantically searching for a black attaché case carried by a Russian space technology dealer, Aleksandr Voronin (who died 2002). She highlighted US interest in various Soviet technology, including nuclear-powered satellites. She also suggested that panic about the stability of some form of nuclear device is the most likely reason behind the initial Swedish government suggestion of burying the wreck in concrete, a highly unusual proposal for a wreck, reminiscent of Chernobyls sarcophagus.

==Cast==
*Greta Scacchi stars as Julia Reuter, a young female journalist who examines the story of MS Estonia.
*Jürgen Prochnow as Erik Westermark, a passenger who survives the sinking and later helps Reuter to uncover the facts.
*Rein Oja as captain Arvo Kallas (based on captain Avo Piht), who survives the disaster and is later kidnapped by US Army.
*Donald Sutherland as Lou Aldryn, an American official who deals with transport of defense materials and cover-up.

==External links==
* 
*  

 
 
 
 
 
 
 
 


 