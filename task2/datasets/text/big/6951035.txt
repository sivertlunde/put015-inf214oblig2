The Glass Mountain (film)
{{Infobox Film
| name           = The Glass Mountain
| caption        = 
| image	=	The Glass Mountain FilmPoster.jpeg
| director       = Edoardo Anton Henry Cass
| producer       = Joseph Janni Frederick Zelnic
| writer         = Emery Bonnet Henry Cass John Cousins John Hunter Joseph Janni 
| starring       = Michael Denison Dulcie Gray
| music          = Nino Rota
| cinematography = William McLeod
| editing        = Lister Laurance
| distributor    = Eagle-Lion Films
| released       = 1949
| runtime        = 88 min.
| country        =   English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}The Glass Mountain is a black and white British romantic film drama released in 1949 in film|1949.  It starred Michael Denison, Dulcie Gray and Valentina Cortese.  The film was a popular success of its day, and was re-released in the UK in 1950 and 1953.    It  features acclaimed classical vocalist Tito Gobbi as himself, with the orchestra and chorus of the Venice Opera House.  The theme music by Nino Rota is memorable, and was also a contemporary hit.   It was mainly filmed on location in the Dolomites and at Venices La Fenice Opera House.

The Guardian wrote, "most reference books now deride the film, but at a time when Britain was emerging from the war into a period of grey austerity, The Glass Mountain and movies like it were a popular tonic. Set in the beautiful Dolomite mountains, with graceful performers and a nostalgically slow pace, it was one of the most successful British films to that date. The part of the composer was taken by Denison, with whom (Dulcie) Gray starred on stage and screen so many times that the Denisons became one of the "royal families" of the British entertainment scene."  

A tale from peasant folklore concerns a mountain made of glass and a mans attempts to climb it, to win the love of a princess. For each step he takes, he slides back two steps; so, cleverly, he turns about and climbs it backwards, gaining double elevation with each downward step.   

==Plot==
A composer discovers that inspiration for his greatest work may come at the expense of his marriage. Richard Wilder (Michael Dennison) was an RAF pilot in World War II whose plane was shot down over the Italian Dolomite Mountains. His life is saved by Alida (Valentina Cortese), a beautiful woman working with the anti-fascist resistance, who nurses Wilder back to health and with whom Wilder has an affair. She tells him a local legend about two lovers - one a ghost who leads her faithless partner to his doom over a mountain precipice on the Glass Mountain. When the war ends, Wilder returns to his English home and wife Ann (Dulcie Gray), and begins composing an opera based on the legend of Dolemite, the Glass Mountain, which has begun to haunt him. His English home however, does not prove conducive to  creativity, so he returns to the source of his inspiration, to Italy, and to Alida, and is able to successfully complete his work. But he has fallen in love with Alida, and with the triumph of his opera in Venice, must now choose between his muse and his wife, as the mythical and modern levels of the legend of the Glass Mountain coincide.  

==Cast==
*Michael Denison as Richard Wilder
*Dulcie Gray as Anne Wilder
*Valentina Cortese as  Alida Morrosini Sebastian Shaw as Bruce McLeod
*Tito Gobbi as Himself (in opera)
*Elena Rizzieri as Herself (in opera)

==Critical reception==
*TV Guide wrote, "Corteses performance is outstanding; Denison and Gray, husband and wife in reality, handle their familiar relationship well, but the real stars of the picture are the music, with operatic baritone Gobbi, and the beautiful mountain scenery."  
*The New York Times wrote of the Dolomites setting, "this breathtaking, snowy backdrop for ski scenes as well as romance has its own charm. However, the main business at hand has no more individuality than if it had been filmed in Leicester Square...It is pedestrian stuff saved from being banal by a few performances, the authentic backgrounds and some lilting arias. "   Time Out wrote, "solidly directed, lavishly mounted romantic tosh."  
*Leonard Maltins Movie and Video Guide wrote, "beautifully made film of a British composer who writes an opera, inspired by majestic Italian Alps. A treat for music lovers, with many singers from La Scala appearing in opera sequence."  

==Musical Numbers==
*"Wayfarer" (Vivian Lambelet & Elizabeth Anthony) first sung by Michael Denison
*"La Montanara" (Ortelli & Pigarelli) sung by Tito Gobbi
*"The Glass Mountain" (Nino Rota) sung by Tito Gobbi and Elena Rizzieri
*Opera sequences sung by Elena Rizzieri & Tito Gobbi of the Scala Opera House, Milan  

==References==
 

==External links==
* 
*  on iTunes

 
 
 
 
 
 
 
 