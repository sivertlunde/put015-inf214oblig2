Bindhaast
{{Infobox Film
| name           = Bindhaast
| image          = Bindhaast Marathi Movie.JPG
| image_size     =
| caption        =
| director       = Chandrakant Kulkarni
| producer       = Matchindra Chate
| writer         = Ajit Dalavi, Prashant Dalavi
| narrator       =
| starring       = Reema Lagoo Sharvari Jamenis Gautami Kapoor Mona Ambegaonkar
| music          = Ashok Patki,  Milind Joshi
| cinematography =
| editing        =
| distributor    =
| released       = 18th June 1999
| runtime        = 155 min
| country        = India
| language       = Marathi
| budget         = 1.5 cr
| gross          = 7 cr
| preceded_by    = 
| followed_by    =
}}
Bindhast (English:Carefree) is a 1999 thriller Marathi film directed by Chandrakant Kulkarni and produced by Matchindra Chate of Chate Coaching Classes. The film is known for its all-women cast.   

==Summary==
Two girls find themselves in a big trouble when their prank turns deadly. When they find are wrongly charged with murder of a man they dont even know, they have to run from law and do anything to prove their innocence. The girls have got each others back, but are they prepared to handle the truth?

==Plot==
Mayuri (Gautami Kapoor) and Vaijayanta aka Vaiju (Sharvari Jamenis) are two fast friends studying in the same girls college. When a girl stab by her boyfriend after being harassed and blackmailed by a boy named Rahul. ACP Nisha Velankar (Mona Ambegaonkar) arrests him and throws him in jail, where he later commits suicide. Nisha is an ex-alumni of the college and later gets invited as chief guest of the colleges silver jubilee function. Mayuri and Vaiju decide to carve their own destinies after hearing a soul stirring speech from Nisha, some days before the function.

Here, Mayuris Aunt Attu (Reema Lagoo) decides to hand over the business to Mayu. In desperation Vaiju lies to Attu that Mayu is in love with a pilot named Mahesh Mukadam whom she met on a trip to Goa. Attu is visibly upset, but decides to support her niece. Later, Mayu starts receiving phone calls from some Mahesh Mukadam whom she actually does not know.  Everything goes fine, until Mayu really gets a call from "Mahesh" a day before the function. Deciding that someone has caught upon their plan and is deciding to blackmail them, they call this Mohan in their hostel.

The girls are armed, waiting for him to turn up, when they learn that Sheela, one of their class mate is also present in her room, since she is ill. Vaiju while putting off the gallery bulb, suddenly, both hear gunshots and rush to find the so-called "Mahesh" dead. Each girl assumes the other to be the killer, until they find out that none of them fired the fatal bullet. The girls finally decide to dispose off his dead body, but it suddenly goes missing. Here, the function abruptly comes to a halt when Mohans body falls on the stage from the A/C pipes.

Things take a strange turn when it appears that Mayus gun has been fired. The girls are arrested, where they tell the whole story to Nisha, but she derisively brushes it off. Mayus gun is found a perfect match for the bullet. Then suddenly, a woman claiming to be Maheshs mother arrives in the police station. Realizing that they are getting deeper in this mess, the girls escape from the police station. After contacting their fellow students and teachers, upon which they learn that Sheela is also missing since the day of murder and Attu was also seen washing her hands continuously in her bungalow where the girls initially gets shelter.

The girls first confront Attu, who tells them that she was full aware of their charade from the start. Attu claims that Mahesh was actually Vicky, a young entrepreneur whom she had roped in to mess with the girls. Attu claims that she knew nothing about "Maheshs mother". Here, Maheshs "mother" starts stalking the girls secretly. The girls spy upon a friend of Sheela and manage to find out her location. Soon, the girls confront and overpower Sheela in her hiding place and after gagging her, call Nisha.

After the girls let Sheela talk, she drops a bombshell that the real killer is none other than Nisha. Sheela tells that she heard the sounds in the corridor and entered in time to see Nisha pulling out her gun and gunning Mohan down in front of her. Then, she saw Sheela and tried to kill her too, forcing her to flee. After realizing the implications, the girls patch up with Sheela and decide to give Nisha a fight. Nisha arrives and corners the girls, but before she can kill them, Maheshs mother appears with the police and tells her to drop the weapon.

Nisha is adamant, until the woman produces a wheelchair bound girl, whom Nisha had met earlier in a mental asylum. Later, in front of the panel, "Maheshs mother" introduces herself as special officer Seema Srivastava. Seema tells them that she was actually sent from Delhi to investigate Rahuls death, since forensics indicated foul play. However, by the time she came, Mahesh was killed and the girls were arrested. She knew that the girls were innocent and that Nisha was somehow connected to this case too and decided to use the girls as a lure and get Nisha. She was aided by Nishas fellow cop to spy on her.

In front of the same panel, Nisha reveals that "Mahesh" was actually a conman who had destroyed the lives of many girls, Nishas own sister included. Her sister was none other than the wheelchair ridden girl. Nisha saw Mahesh in the hostel and confronted him. However, he started to run away, which angered her. After Sheela disappeared, Nisha removed the dead body. While searching Mayus gun, Nisha realized that her gun was the same make as that of Nishas and had her framed.

After the girls are exonerated, a huge comeback party is arranged for Mayu, Vaiju and Sheetal. Everything is going great until a huge bouquet from "Mahesh Mukadam" shows up for Mayu. Seeing the expressions of others, she realizes that none of them sent it. Suddenly, everybody gets a shock when Attu leaps out from behind the bouquet. After the initial shock subsides, everybody ends up in peals of laughter.

==Cast==
* Reema Lagoo as Aasavari (Attu). She has brought up Mayu single handedly after her parents died and now wants to hand over the business to her niece.
* Sharvari Jamenis as Vaijayanti Patil (Vaiju). Mayus best friend and the daughter of a school teacher.
* Gautami Kapoor as Mayuri (Mayu)
* Meenal Pendse as Sheela. Sheela is arch nemesis of both Mayu and Vaiju.
* Mona Ambegaonkar as ACP Nisha Velankar. Nisha hates crimes against women and is an ex-alumni of the college Mayu and Vaiju are attending.
* Reshma Polekar as Nishas fellow cop.
* Nirmiti Savant as Teacher
* Deepa Shriram as Principal
* Seema Biswas as CBI Officer Seema Srivastava.

==Awards==
The film won numerous awards at the Maharashtra State Film Awards. Best lyrics, screen-play and dialogues awards went to Vasu Vaidya, Ajit Dalavi and Prashant Dalavi respectively. 

==References==
 

== External links ==
*  

 
 
 
 
 