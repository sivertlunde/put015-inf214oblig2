Akitsu Springs
{{Infobox film
| name           = Akitsu Springs
| image          = Akitsu Springs.jpg
| image_size     = 
| caption        = Original Japanese Poster.
| director       = Yoshishige Yoshida
| producer       = Masao Shirai 
| writer         = Shinji Fujiwara (novel) Yoshishige Yoshida (screenplay)
| starring       = Mariko Okada Hiroyuki Nagato
| music          = Hikaru Hayashi
| cinematography = Toichiro Narushima
| editing        = Yoshi Sugihara 
| studio         = 
| distributor    = Shochiku
| released       =  
| runtime        = 113 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 1962 Japanese film directed by Yoshishige Yoshida, starring Mariko Okada and Hiroyuki Nagato.

==Plot==
The film is set at the Akitsu health spa. Just after World War II, Shusaku Kawamoto (Hiroyuki Nagato) who is suffering from tuberculosis, has come to Akitsu to die. One young girl, Shinko (Mariko Okada), refuses to let him and invigorates him on the slow path to recovery.  They fall in love, and in one of his darker moments, Shusaku asks Shinko to join him in double suicide. She accepts his love but not ready to die. Neither, it transpires, is Shusaku, and slowly they part. Time passes, Shinko finds out that Shusaku is married with a child, and he seems to have no interest in her any more.

== Cast ==
*Mariko Okada as Shinko
*Hiroyuki Nagato as Shusaku Kawamoto
*So Yamamura as Mikami 
*Jukichi Uno as Kenkichi Matsumiya 

== Awards == Performance by an Actress in a Leading Role for Mariko Okada     Performance by an Actress in a Leading Role for Mariko Okada    

== References ==
 

==External links==
*  
*  

 

 
 
 


 