Chanakyan
{{Infobox film
| name = Chanakyan
| image = Chanakyan (film).jpg
| image_size =
| caption =
| director = T K Rajeev Kumar Navodaya
| writer = John E
| narrator = Madhu Urmila Matondkar
| music = Mohan Sithara
| cinematography = Saroj Padi
| editing = Raghupathi
| distributor =
| released = 1989
| runtime =
| country = India
| language = Malayalam
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 1989 Cinema Indian feature directed by Madhu and Thilakan.  

==Plot==

Chanakyan is the revenge story of a person (Kamal Haasan) on a state chief minister (Thilakan) using Character assassination trap.

==Summary==

The film opens with a high tech persons (Kamal Hasan) attempt to kill Chief Minister Madhava Menon (Thilakan), but due to circumstances, he fail in his attempt. Later Johnson (Kamal Haasan) happens to watch a Mimicry show of an artist (Jayaram) imitating several celebrity voices. Johnson convinces Jayaram himself as a government employee and makes him to mimic the voice of Madhava Menon reading out false promises through TV and radio to the public and the image of Madhava Menon gets affected in public.

Madhava Menon appoints his friend and DIG, K. Gopalakrishna Pillai (Madhu (actor)|Madhu) to investigate this case and he finds out foul play.
 assassinate the character of the Chief minister and the public is now against the Chief minister. 

In the thrilling climax, Johnson forces Madhava Menon to shoot him and Johnson dies thus making the politician a murderer and destroying his political future.

==Cast==

*Kamal Hasan as Johnson
*Thilakan as Madhava Menon Madhu as DIG K. Gopalakrishna Pillai
*Urmila Matondkar as Renu
*Jayaram as Jayaram Sithara as Geethu
*Sabitha Anand as Jessy
* Santha Devi as Johnsons mother
*P.C.George as Police Inspector
* Jagadish
* Jagannathan as Madhava Menons aide
* Poojappura Radhakrishnan as Security guard
* Sainuddin as Mimicry artist
* M. S. Thripunithura as Geethus father
* Kollam Thulasi

==References==
 

==External links==
*  

 
 
 
 
 