When the Boys Meet the Girls
{{Infobox film
| name           = When the Boys Meet the Girls
| image          =
| alt            = 
| caption        = 
| director       = Alvin Ganzer
| producer       = Sam Katzman
| writer         = Guy Bolton (play) Jack McGowan (play) Robert E. Kent (screenplay)
| narrator       = 
| starring       = Connie Francis Harve Presnell
| music          = Fred Karger
| cinematography = Paul Vogel Ben Lewis
| studio         = Four-Leaf Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

When the Boys Meet the Girls is a 1965 American musical film, directed by Alvin Ganzer and starring Connie Francis and Harve Presnell. Guy Bolton and Jack McGowan were both uncredited in their roles as the writers for the play on what the film is based.

==Cast==
*Connie Francis - Ginger Gray
*Harve Presnell - Danny Churchill
*Peter Noone - Himself (as Hermans Hermits)
*Karl Green - Himself (as Hermans Hermits)
*Derek Leckenby - Himself (as Hermans Hermits)
*Keith Hopwood - Himself (as Hermans Hermits)
*Barry Whitwam - Himself (as Hermans Hermits)
*Louis Armstrong - Himself
*Domingo Samudio - Sam the Sham (as Sam the Sham) Dave Martin - Pharaoh (as The Pharaos)
*Ray Stinnet - Pharaoh (as The Pharaos) Jerry Patterson - Pharaoh (as The Pharaos)
*Butch Gibson - Pharaoh (as The Pharaos)
*Liberace - Himself
*Sue Ane Langdon - Tess Rawley
*Fred Clark - Bill Denning
*Frank Faylen - Phin Gray
*Joby Baker - Sam
*Hortense Petra - Kate Stanley Adams - Lank
*Romo Vincent - Pete
*Susan Holloway - Delilah
*Russell Collins - Mr. Stokes (as Russ Collins)
*Pepper Davis - Himself
*Bill Quinn - Dean of Colby (as William I. Quinn)
*Tony Reese - Himself

== Soundtrack ==
{{Infobox album  
| Name        = When the Boys Meet the Girls
| Type        = soundtrack
| Artist      = Various artists
| Cover       = 
| Released    = 1966
| Recorded    = 
| Genre       = 
| Length      = 29:51 MGM
| Producer    =  
| Reviews     = 
| Chronology = Hermans Hermits British
| Last album  = A Must to Avoid (1966)
| This album  = When the Boys Meet the Girls (1966)  Hold On! (1966) 
| Misc  = {{Extra chronology
 | Artist  = Hermans Hermits American
 | Type  = soundtrack
 | Last album  = Hermans Hermits on Tour (1965) 
 | This album  = When the Boys Meet the Girls (1966)  Hold On! (1966) 
 }}
}}

=== Track listing ===
{{Track listing
| headline        = Side one
| extra_column    = Artist(s)
| all_writing     = George Gershwin (music), Ira Gershwin (lyrics) except where noted
| writing_credits = yes
| title1          = When the Boys Meet the Girls Jack Keller, Howard Greenfield
| extra1          = Connie Francis
| length1         = 2:04
| title2          = Monkey See, Monkey Do
| writer2         = Johnny Farrow Sam the Sham and The Pharaohs
| length2         = 2:33
| title3          = Embraceable You
| writer3         = 
| extra3          = Harve Presnell
| length3         = 2:56
| title4          = Throw It Out of Your Mind
| writer4         = Unknown Louis Armstrong & Orchestra
| length4         = 2:10
| title5          = Mail Call
| writer5         = Fred Karger, Bein Weisman, Sid Wayne
| extra5          = Connie Francis
| length5         = 2:17
| title6          = I Got Rhythm
| writer6         = 
| extra6          = Connie Francis & Harve Presnell
| length6         = 3:34
}}

{{Track listing
| headline        = Side two
| extra_column    = Artist(s)
| writing_credits = yes
| title1          = Listen People
| writer1         = Graham Gouldman
| extra1          = Hermans Hermits
| length1         = 2:31
| title2          = Bidin My Time
| writer2         = 
| extra2          = Hermans Hermits
| length2         = 2:26
| title3          = Embraceable You
| writer3         = 
| extra3          = Connie Francis
| length3         = 2:12
| title4          = Aruba Liberace
| writer4         = Liberace
| extra4          = Liberace
| length4         = 2:46 But Not for Me
| writer5         = 
| extra5          = Connie Francis & Harve Presnell
| length5         = 2:58
| title6          = I Got Rhythm
| writer6         = 
| extra6          = Louis Armstrong & Orchestra
| length6         = 1:24
}}

==Awards==
In 1966, the year after the film was released, it received some prestigious awards at that years Laurel Awards ceremony. Harve Presnell was nominated for a Golden Laurel in the category of Musical Performance, Male for his talented musical numbers. Though he did not win, he was awarded 3rd place. Connie Francis was also nominated for a Golden Laurel in the category of Musical Performance, Female for her musical numbers. She did not win either, but came a gratifying 4th place.

==External links==
* 
*  
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 