Watch on the Rhine
 
 
{{Infobox film
| name            = Watch on the Rhine
| image           = Watch on the Rhine poster.jpg
| caption         = Theatrical release poster
| director        = Herman Shumlin
| producer        = Hal B. Wallis
| based on        = Watch on the Rhine (play)
| writer          = Lillian Hellman
| screenplay      = Dashiell Hammett
| starring        = Bette Davis Paul Lukas Geraldine Fitzgerald Lucile Watson Beulah Bondi George Coulouris
| music           = Max Steiner
| cinematography  = Merritt B. Gerstad Hal Mohr
| editing         = Rudi Fehr
| distributor     = Warner Bros.
| released        =  
| runtime         = 114 minutes
| country         = United States
| language        = English
| budget          =
}}
 play of the same title by Lillian Hellman. 

==Plot== Donald Woods) and their mother Fanny (Lucile Watson) in Washington, D.C. For the past seventeen years, the Muller family has lived in Europe, where Kurt responded to the rise of Nazism by engaging in anti-Fascist activities. Sara tells her family they are seeking peaceful sanctuary on American soil, but their quest is threatened by the presence of houseguest Teck de Brancovis (George Coulouris), an opportunistic Romanian count who has been conspiring with the Germans in the nations capital.

Teck searches the Mullers room and discovers a gun and money intended to finance underground operations in Germany in a locked briefcase. Shortly after, the Mullers learn resistance worker Max Freidank has been arrested, and because he once rescued Kurt from the Gestapo, Kurt plans to return to Germany to assist Max and those arrested with him. Aware Kurt will be in great danger if the Nazis discover he is returning to Germany, Teck demands $10,000 to keep silent, and Kurt kills him. Realizing the dangers Kurt faces, Fanny and David agree to help him escape.

Time passes, and when the Mullers fail to hear from Kurt, Joshua announces he plans to search for his father as soon as he turns eighteen. Although distraught by the possibility of losing her son as well as her husband, Sara resolves to be brave when the time comes for Joshua to leave.

==Production==
The Lillian Hellman play had enjoyed a respectable run of 378 performances on Broadway theatre|Broadway.  Feeling its focus on patriotism would make it an ideal and prestigious propaganda film at the height of World War II,  Jack L. Warner paid $150,000 for the screen rights.    Stine, Whitney, and Davis, Bette, Mother Goddam: The Story of the Career of Bette Davis. New York: Hawthorn Books 1974. ISBN 0-8015-5184-6, pp. 170-172  Higham, Charles, The Life of Bette Davis. New York: Macmillan Publishing Company 1981. ISBN 0-02-551500-4, pp. 164-166 

Because Bette Davis was involved with Now, Voyager, producer Hal B. Wallis began searching for another actress for the role of Sara Muller while Hellmans lover Dashiell Hammett began writing the screenplay at their farm in Pleasantville, New York. Irene Dunne liked the material but felt the role was too small, and Margaret Sullavan expressed no interest whatsoever. Edna Best, Rosemary DeCamp, and Helen Hayes also were considered.

For the role of Kurt Muller, Wallis wanted Charles Boyer. He, however, felt his French accent was wrong for the character,  so the producer decided to cast Paul Lukas, who had originated the role on Broadway and had been honored by the Drama League of New York for his performance. 

Meanwhile, Hammett was sidelined by an injured back, and by the time he was ready to resume work on the script Now, Voyager was close to completion. Wallis sent Davis,  a staunch supporter of Franklin D. Roosevelt and a fierce opponent of the Nazi Party, the screenplay-in-progress and she immediately accepted the offer. 
 Hays Office suggested it be established Kurt was killed by the Nazis at the end of the film in order to show he paid for his crime. Playwright Ross objected and the studio agreed Kurt had been justified in shooting Teck, and the scene remained. 

Filming began on June 15, 1942, and did not go smoothly.  Beginning only a week after Now, Voyager had ended production, Davis was working without a substantial vacation and was on edge. As a result, she immediately clashed with Herman Shumlin, who had directed the play but had no experience in film, and tended to ignore his suggestions.
 Republican whose Democratic Davis. 

Several exterior scenes shot on location in Washington had to be cut from the film prior to its release due to wartime restrictions on the filming of government buildings. 

When Wallis announced he was giving Davis top billing, she argued it was ridiculous to do so given hers was a supporting role. The studios publicity department argued it was her name that would attract an audience and, despite her resistance, the films credits and all promotional materials listed her first. 

Davis and Lukas reprised their roles for a radio adaptation that aired in the January 10, 1944 broadcast of The Screen Guild Theater.

==Cast==
* Bette Davis as Sara Muller 
* Paul Lukas as Kurt Muller 
* Geraldine Fitzgerald as Marthe de Brancovis 
* Lucile Watson as Fanny Farrelly 
* Beulah Bondi as Anise 
* George Coulouris as Teck de Brancovis  Donald Woods as David Farrelly 
* Donald Buka as Joshua Muller

==Critical reception==
Bosley Crowther of the New York Times called it "a distinguished film — a film full of sense, power and beauty" and added, "Its sense resides firmly in its facing one of civilizations most tragic ironies, its power derives from the sureness with which it tells a mordant tale and its beauty lies in its disclosures of human courage and dignity. It is meager praise to call it one of the fine adult films of these times." He continued, "Miss Hellmans play tends to be somewhat static in its early stretches on the screen. With much of the action confined to one room in the American home, development depends largely on dialogue — which is dangerous in films. But the prose of Miss Hellman is so lucid, her characters so surely conceived and Mr. Shumlin has directed for such fine tension in this his first effort for the screen that movement is not essential. The characters propel themselves." In conclusion, he said, "An ending has been given the picture which advances the story a few months and shows the wife preparing to let her older son follow his father back to Europe. This is dramatically superfluous, but the spirit is good in these times. And it adds just that much more heroism to a fine, sincere, outspoken film."  

Variety (magazine)|Variety called it "a distinguished picture . . . even better than its powerful original stage version. It expresses the same urgent theme, but with broader sweep and in more affecting terms of personal emotion. The film more than retains the vital theme of the original play. It actually carries the theme further and deeper, and it does so with passionate conviction and enormous skill . . . Just as he was in the play, Paul Lukas is the outstanding star of the film. Anything his part may have lost in the transfer of key lines to Bette Davis is offset by the projective value of the camera for closeups. His portrayal of the heroic German has the same quiet strength and the slowly gathering force that it had on the stage, but it now seems even better defined and carefully detailed, and it has much more vitality. In the lesser starring part of the wife Davis gives a performance of genuine distinction."  

Davis stated in a 1971 interview with Dick Cavett, that she played the role of the wife for name value because the studio did not consider the film a good financial risk, and that her name above the credits would draw audiences. Davis gladly took the secondary role because she felt the story was so important, and that Miss Hellmans writing was super brilliant 

The National Board of Review of Motion Pictures observed, "Paul Lukas here has a chance to be indisputably the fine actor he always has shown plenty signs of being. Bette Davis subdues herself to a secondary role almost with an air of gratitude for being able to at last be uncomplicatedly decent and admirable. It is not a very colorful performance, but quiet loyalty and restrained heroism do not furnish many outlets for histrionic show, and Miss Davis is artist enough not to throw any extra bits of it to prove she is one of the stars."  

==Awards and nominations== New York Film Critics Circle Award for Best Picture.

It was also nominated for the Academy Award for Best Picture.

Paul Lukas won the Academy Award for Best Actor, the Golden Globe Award for Best Actor – Motion Picture Drama (the first time the award was presented), and the New York Film Critics Circle Award for Best Actor.
 For Whom Academy Award Howard Koch for Casablanca (film)|Casablanca.

==DVD release==
On April 1, 2008, Warner Home Video released the film as part of the box set The Bette Davis Collection, Volume 3.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 