This May Be the Last Time
{{Infobox film
| name           = This May Be the Last Time
| image          = TMBLT poster.jpg
| alt            = 
| caption        = Film poster
| director       = Sterlin Harjo
| producer       = Sterlin Harjo Christina D. King Matt Leach
| writer         = 
| starring       = 
| music          = Ryan Beveridge
| cinematography = Sterlin Harjo Shane Brown Matt Leach
| editing        = Matt Leach
| studio         =   Sundance Channel
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English Mvskoke 
}}
This May Be the Last Time is a 2014 American documentary film produced and directed by Sterlin Harjo.   The film had its world premiere at 2014 Sundance Film Festival on January 19, 2014.  
 Sundance Channel acquired the distribution rights of the film. The film will have a TV premiere in Spring 2014.  

==Synopsis==
The film narrates that when in 1962 Pete Harjo mysteriously went missing after his car crashed on a rural bridge in Sasakwa, Oklahoma, members of Seminole Indian community searched for him while singing songs of faith and hope that had been passed on for generations.

==Reception==
This May Be the Last Time received positive reviews from critics. Guy Lodge of Variety (magazine)|Variety, said in his review that "Filtering painstaking research on the evolution of Creek Nation hymns through a tragic narrative from Harjos family history, the directors first nonfiction feature is artful and illuminating."  Justin Lowe in his review for The Hollywood Reporter praised the film by saying that "The mystery of Pete Harjo’s disappearance turns out to be somewhat more prosaic, although Harjo plays out developments in the missing-person search skillfully enough to maintain interest, much in the storytelling tradition of his tribal elders."  Amanda Rock in her review for Slugmag said that "This film gives insight into a small community that supported each other through a difficult time, both physically and spiritually." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 