Vaayai Moodi Pesavum
{{Infobox film
| name           = Vaayai Moodi Pesavum
| image          = Vaaiyai moodi pesavum.jpg
| alt            = 
| caption        = Poster
| director       = Balaji Mohan
| producer       = S. Sashikanth Varun Manian
| story          = Balaji Mohan
| screenplay     = Balaji Mohan
| starring       = Dulquer Salman Nazriya Nazim Madhoo Abhinav R
| music          = Sean Roldan
| cinematography = Soundararajan 
| editing        = Abhinav Sunder Nayak
| studio         = Y NOT Studios Radiance Media
| distributor    = 
| released       = 25 April 2014
| runtime        = 
| country        = India Tamil
| budget         = 
| gross          = 
}}
 Tamil     romantic comedy film directed by Balaji Mohan starring Dulquer Salman and Nazriya Nazim. Sean Roldan scored the music, while Soundararajan was the cinematographer and Abhinav Sunder Nayak worked as the editor. Filming began in November 2013. The film was simultaneously made in Malayalam with the same lead actors and slightly changed supporting actors list, under the title Samsaaram Aarogyathinu Haanikaram. 

==Cast==
* Dulquer Salmaan as Arvind
* Nazriya Nazim as Anjana
* Madhoo as Vidhya
* Abhinav R as Vinodh
* Abhishek Shankar as Vidhyas husband
* Pandiarajan as Minister Sundaralingam
* Arjunan (actor)| Arjunan as Sathish
* Kali Venkat as Palani
* John Vijay as "Nuclear Star" Bhoomesh
* Robo Shankar as Mattai Ravi
* Ramesh Thilak as Ganesh
* Vinu Chakravarthy as Adhikesavan
* Balaji Mohan as Tv News Reporter
* Nakshatra Nagesh as Saraswathi
* Kamala Krishnaswamy as the Orphanages Patron and Arvinds Mother Figure
* Sathyapriya as Adhikesavans Wife
* RJ Balaji as himself

==Plot summary==
The story takes place in Panimalai, a hill city, during a spring season. The film begins with RJ Balaji, playing himself, coming to Panimalai to be a guest of a live radio show. In the middle of the show, he starts coughing severely and suddenly loses his voice. He is diagnosed with a new type of virus called Dumb flu, that causes sudden dumbness and sometimes even death.

Aravindh, (Dulquer Salman) is a sales rep working with a glue company.  His dream is to become a radio jockey, and even attends interviews with a FM station. As Dumb Flu spreads across the city, the state Government sends the health minister Sundaralingam (Pandiarajan) to Panimalai to take care of the situation. The people are asked to undergo a medical checkup to check whether they are affected by the virus or not. Aravindh goes to the hospital and meets Anjana (Nazriya Nazim), a junior doctor, who believes that talking is the only cause of all the problems in the world and if people keep things to themselves things will be very fine. Anjana is in a relationship with Vinodh, a possessive guy who always commands Anjana on what she should do and even what she should wear. Anjana lives with her Dad and Stepmom Vidhya (Madhoo). Since Anjana lost her mom at an young age, she is reluctant to accept Vidhya as her mom and always maintains a distance from her.  Vidhya is an aspiring writer, who is trying to get her Husbands attention and support to write her third book. Her husband finds no time to talk to her as he is too busy with his work.

Panimalai comes into the news for another issue, where, Bhoomesh (John Vijay), a film superstar, who has gone to shoot one of his films, is being opposed by Mattai Ravi (Robo Shankar), a drunkard who is the president of the Drinkers Association, over the issue of Bhoomesh showing drunkards in a bad light in his films. The Drinkers Association and Bhoomeshs fans, lead by (Ramesh Thilak) form groups to fight over this issue.

Aravindh and Anjana begin a friendship and he insists her that if everything is spoken directly from the heart, there would be no problem between anyone. He asks her to speak openly with Vinodh and sort out the difference of opinion between them. She does not agree to this, and Aravindh challenges her that, if he successfully stops the feud between Bhoomesh and Mattai Ravi just by means of speaking, Anjana should talk openly with Vinodh to sort out their issues. They both agree on the challenge and mark a deadline day. Aravindh tries so many ways to stop the feud between Bhoomeshs fans and the Drinkers association, but it ends up in an even bitter fight. As time goes by, Aravindh falls in love with Anjana, not knowing that she is engaged to Vinodh.

Aravindh is brought up in an Orphanage that belongs to Adhikesavan (Vinu Chakravarthy), a stubborn old man, who is constantly asking the Orphanage to be vacated so that he can rent it to richer tenants. Though Aravindh helps the Children in the Orphanage by donating some of his money, it is not enough to save the Orphanage. Aravindh tries to speak to him and sort the issue, but Adhikesavan is too hesitant even to speak with him.

But Aravindh and his friend kidnap the stubborn man and leave him in his sons house making a close relationship. 

The final report by the health organisation says that the dumb flu spreads only by means of speaking, and issues a speaking ban in the town. Then everyone one the town tries to adjust to living with not speaking. Aravindh conducts the meeting with Drinkers Association and Bhoomeshs fans and they reconcile. 

A cure for the illness is invented, but if the virus has already infected a person, the cure has a 50% probability of a side effect that the patient might lose the ability to speak. The movie ends with everyone getting cured but minister Sundaralingam , who has been faking the illness given cure medicine on the stage for a photo op and losing his voice for real.

==Production==
In June 2013, the Radiance Group led by Varun Manian entered a five-movie deal with YNOT Studios and venture into film production with a new entertainment company called Radiance Media.  Varun stated that Radiance Media will be producing their first film with director Balaji Mohan for a bilingual film and plan to launch Dulquer Salman in Tamil.     Nazriya Nazim was signed as the heroine and Anirudh Ravichander was reported to be the music director.  About her role Nazriya said, "Anjana, my character, is somebody I havent played in my career yet".  Anirudh later backed out   and independent musician Ragahvendra, known under his stage name Sean Roldan, replaced him and made his debut as a film composer.    Soundararajan was signed up as the cinematographer.  Abhinav Sunder Nayak was signed in as the editor. 

The film started filming in Munnar on November 4, with Madhoo joining the cast of the film, making her comeback in the Tamil and Malayalam film industries.  

==Soundtrack==
{{Infobox album|  
| Name        = Vaayai Moodi Pesavum
| Longtype    =
| Type        = soundtrack
| Artist      = Sean Roldan
| Release     = March 14, 2014
| Cover       = 
| Caption     = Feature film soundtrack
| Length      = 22:27 Tamil
| Label       = Think Music 
| Producer    = Sean Roldan
| Last album  = 
| This album  = Vaayai Moodi Pesavum (2014)
| Next album  = Sathuranga Vettai (2014)
}}

The soundtrack for the film was composed by Sean Roldan, who made his debut as a composer in the film industry, through the venture. The audio was launched in Chennai on 14 March 2014 by veteran director Mani Ratnam. 

{{track listing
| extra_column = Singer(s)
| music_credits = no
| lyrics_credits = yes
| total_length = 22:27
| title1     = Shut up! Vaaya Moodu! Pesadhe!
| extra1     = Sean Roldan, Balaji Mohan
| lyrics1    = Balaji Mohan
| length1     = 2:21

| title2     = Kadhal Ara Onnu Vizundhuchu
| extra2     = Sean Roldan, Shakthisree Gopalan
| lyrics2    = Muthamil
| length2    = 3:49

| title3     = Mr.Fix-It Theme
| extra3     = 
| lyrics3    =
| length3    = 2:31

| title4     = Podhum Nee Ini Varundhadhe
| extra4     = Haricharan
| lyrics4    = Muthamil
| length4    = 4:36

| title5     = Udaigiren Udaigiren
| extra5     = Aalap Raju
| lyrics5    = Madhan Karky
| length5    = 2:29

| title6     = Beedhiya Kelappa Jazz Theme
| extra6     = 
| lyrics6    = 
| length6    = 1:55

| title7     = The Sound of Silence
| extra7     = 
| lyrics7    = 
| length7    = 1:59

| title8     = Maatra Paravai
| extra8     = Pradeep, Kalyani Nair
| lyrics8    = Madhan Karky
| length8    = 2:57
}}

==Release==  Zee Thamizh.   The film was given a "U" certificate by the Indian Censor Board.

==Critical reception==
The film received positive reviews from critics. Sify wrote, "Vaayai Moodi Pesavum is refreshingly fresh, quirky and innovative. It is one of the best romcom’s laced with satire in recent times, and a lead pair that crackles", going on to add, "Balaji Mohan has reinvented the romcom formula".  The New Indian Express wrote, "The film may fall short in its entertainment quotient as compared to the director’s earlier film. But Mohan should be appreciated for his wacky, daring and innovative attempt".  The Hindu wrote, "The film is filled with gags, satire and a cheeky commentary on life. While the laughter is loud and frequent in the theatre, half an hour later you wonder what the reason was for all the fuss. The film doesnt have a taut storyline that lingers in your mind. If that had been nailed, this would have been an experiment that really worked".  Times Of India gave the film 4/5 and wrote, "Vaayai Moodi Pesavum is truly an ambitious effort, at least by Indian cinema standards, and whats truly heartening is that Balaji Mohan succeeds in his attempt.  Deccan Chronicle gave it 3.5/5 and wrote, "there are many subtexts to the film – a little too many to delve into. However, like fine wine and certain genres of music, it does take a little getting used to. Vaayai Moodi Pesavum is definitely a bold attempt".  Indo-Asian News Service|IANS also gave 3.5/5 and called it "an almost brilliant film that contradicts itself at several junctures for reasons that are never explained and left to be figured out by the viewer".  3.5/5 was the rating given Rediff that wrote, "A totally new concept coupled with the director’s unique narrative style and a screenplay loaded with satire and comedy, makes Vaayai Moodi Pesavum, a thought-provoking and thoroughly enjoyable film".  Behindwoods gave 3.25/5 and wrote, "Apart from the whacky concept, its seemingly quotable animated characters, the locations, color, styling, music by Sean Roldan and the cinematography of Soundarrajan quite simply make Vaayai Moodi Pesavum the coolest film this summer".  S. Rajasekar of Cinemalead gave 3.25/5 and called the film "Classy, pleasant and feel good.".  OneIndia gave 3/5 and concluded that the film is "A light-hearted family entertainer even though there are double entendre jokes at parts.".  Indiaglitz gave 3/5 for the film and concluded that the film is "Pleasant and makes you feel good, so talk it out!".  Bharath Vijaykumar of Moviecrow gave 3.25/5 and concluded, "VMP is a refreshingly original attempt at clean humour. This crazy attempt from Balaji Mohan definitely warrants the support of fans who want different type of films to hit our
screens more consistently.". 

In contrast, Gautaman Bhaskaran of the Hindustan Times gave 2/5 and wrote, "For a good part, the film runs without dialogues, though the loud, almost irritatingly intrusive, background score robs the work of, what could have been otherwise, beautiful silence. And, what is downright silly is a government minister camping in the town – a man who is caught between a smart personal assistant and a haughty chief minister. Salman as the sales guy promoting an adhesive seems to hold out some promise, but Nazim and Madhoo sparkle. And with Mohan himself portraying a witty television news anchor, the movie has its bright moments, but not enough to get more stars. If only there was greater finesse in treatment and the excision of some scenes, Vaayai Moodi Pesavum could have been gripping."  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 