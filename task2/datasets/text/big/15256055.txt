The Fall of Fujimori
{{Infobox Film | name = The Fall of Fujimori
| image          =
| caption        = 
| director       = Ellen Perry
| producer       = Ellen Perry
| writer         = Ellen Perry, Kim Roberts, Zack Anderson
| starring       = 
| music          = 
| cinematography = Junji Aoki, Mel Henry, Ellen Perry
| editing        = Kim Roberts
| distributor    = 
| released       = 2005
| runtime        = 83 min.
| language       = English
| budget         =  
}}
The Fall of Fujimori is a 2005 documentary film about Peruvian President Alberto Fujimori, who fled the country for Japan in 2000 to avoid facing 21 charges of corruption, murder and human rights abuses. Then, five years later, Fujimori flew into Chile and declared his intention of once again running for president in 2006. He was promptly arrested. 
 PBS as Point of View series in 2006. It has been met with high critical acclaim, and received a 92% "Fresh" rating on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  
*  
*   - PBSs site dedicated to the film

 
 
 
 
 
 

 
 