Plata Quemada
{{Infobox film
| name           = Plata quemada
| image          = Plata quemada Poster.jpg
| alt            = 
| caption        = Theater release poster Eduardo Noriega Leonardo Sbaraglia Pablo Echarri
| director       = Marcelo Piñeyro
| producer       = Diana Frey Oscar Kramer
| screenplay     = Marcelo Piñeyro Marcelo Figueras
| based on       =   
| music          = Osvaldo Montes
| cinematography = Alfredo F. Mayo
| editing        = Juan Carlos Macías
| studio         = Oscar Kramer S.A. Cuatro Cabezas
| distributor    = Lider Films Alta Films 
| released       =  
| runtime        = 125 minutes
| country        = Argentina France Spain Uruguay Spanish
| budget         = $7,000,000
| gross          = $44,753,475
}} Argentine action Eduardo Noriega, Pablo Echarri, Leticia Brédice and Ricardo Bartis, it is based on Ricardo Piglias 1997 Planeta prize-winning novel of the same name.  The novel was inspired by the true story of a notorious 1965 bank robbery in Buenos Aires.
 Goya Award for Best Spanish Language Foreign Film in 2002. It was partly funded by INCAA.

==Plot== Eduardo Noriega), a drifter, meet in the bathroom of a Buenos Aires subway station, and from that moment they are inseparable. They become known as "the twins", but their relationship is in fact that of lovers and, soon, as partners in crime. At a point when their relationship is already turning difficult, the plot sets in.

Their love and loyalty to each other is tested when "the twins" join a plan to hold up an armored truck together with a group of seasoned  ), a sedative addict whos been carrying on an affair with the luscious Vivi (Dolores Fonzi); a 16-year-old nymphet; the trios boss Fontana (Ricardo Bartis); and the elderly lawyer Nando (Carlos Roffé), who is past the days of any professional illusions and helps make connections to find a good team for the crime.

Angel is wounded by police gunfire during the robbery, provoking Nene to kill all of the guards and police in a fit of rage. Two police officers are among the victims, and so the police of Buenos Aires start a major search for the culprits. They soon find a lead to Vivis apartment, where the gang had been hiding out, but by this time, all except Vivi have escaped to neighboring Uruguay. The police force Vivi to give away their plans, and the search is broadened to Uruguay.

Meanwhile, the gang needs to wait for new passports (to be arranged by a dubious character played by Héctor Alterio) for their escape from Uruguay.

They take refuge in an empty apartment in Montevideo, but the organisation of the documents takes longer than originally planned. The waiting is especially hard on Nene: Angel—who is described as constantly "hearing voices" and seems to suffer a slight form of schizophrenia—has been rejecting intimate contact with Nene since at least the beginning of the escape, for reasons indirectly connected with his condition.

Nene eventually decides to break curfew and "get some air", and he, Angel, and Cuervo go and enjoy themselves at a fair. That evening outside of the apartment is not their last though, partly because Angel continues to reject any contact and does not even speak to Nene anymore.

On one evening, Nene ends up verbally and sexually abusing another gay person, but finally he meets the prostitute Giselle (Leticia Brédice) and starts a relationship with her, even mentioning a solo escape with her.

When Nando is caught by the police, the group is forced to abandon their refuge. Fontana goes off on his own, but Nene brings Angel and Cuervo to hide out in Giselles apartment before they leave the city at night.

Angel had sensed that Nene had been cheating on him, and he soon understands where Giselle fits in. Before it can come to a fight, however, he also hears from Giselle that Nene still cares for him and suffered from his rejection. Giselle tells Nene that she has a cousin who lives near the border and she can arrange passage, but not for three men the police are after. She says that a couple, a man and a woman, will be able to get across easily. When she forces him to choose between her and Angel, he chooses Angel.

Nene tells Giselle that he will find her and kill her if she turns them in. Despite his threats, Giselle goes straight to the police to turn in the gang. Before the group takes to their heels, police have already surrounded the building. At first, the trio believes they will be able to escape, thinking that the police will not endanger the haul (several million dollars) or the lives of a large number of policemen. In high spirits, the three of them set to defend the apartment and their freedom, while Nene and Angel rekindle their relationship and spend some short and erotic moments of mutual happiness. After the first wave of attacks, a contented Angel even says the "voices" he always heard have fallen silent.

The group soon finds out, however, that there is no way out. Cuervo dies in an attempt to sally, and Nene and Angel remain waiting for the next wave of police attacks on the apartment. Seeing that they can save neither their own lives nor the money, they burn the entire haul in a final outburst of joie de vivre.

Finally, Nene catches a bullet and dies in Angels arms. Angel still holds Nene and sends a volley of bullets in the direction of the approaching police every now and again, when the lights fade out and the audience is left with the sound of the final fusillade of the police machine guns. It is implied that Angel committed suicide by cop.

==Cast==
* Leonardo Sbaraglia as El Nene Eduardo Noriega as Ángel
* Pablo Echarri as El Cuervo
* Leticia Brédice as Giselle
* Ricardo Bartis as Fontana
* Dolores Fonzi as Vivi
* Carlos Roffé as Nando
* Daniel Valenzuela as Tabaré
* Héctor Alterio as Losardo
* Claudio Rissi as Relator
* Luis Ziembrowsky as Florian Barrios
* Harry Havilio as Carlos Tulian
* Roberto Vallejos as Parisi
* Adriana Varela as Cantante Cabaret

==Background==

===Filming locations===
Filming took place in both Buenos Aires and Montevideo.

=== Historical background ===
The film is based on the true story of a hold-up in Buenos Aires in 1965 and the subsequent flight of the criminals to nearby Montevideo. The film makes several changes about the main characters, usually following Ricardo Piglias book. El Nene, whose real name was Brignone, was actually the educated son of a judge; in an interview he said that he never knew what would happen with the money. He described Dorda –renamed Angel in the film– as the most good-natured of the robbers, whereas Malito –Fontana in the film– supposedly evoked fear. From pictures of Malito, he was probably considerably younger than the character in the film. El Cuervos real name was Mereles. according to private entries to a discussion forum on the film Plata quemada, namely the entries Plata quemada (28) (9 December 2003) and Plata quemada (36) (4 November 2005) on:   (Spanish; retrieved 7 April 2007) 

The film has been said to be more authentic than Ricardo Piglias book on which it was based.  The dates indicated at the end of the film (28 September to 4 November 1965), however, are not entirely accurate as the final attack on the fugitives hiding place in Montevideo took place on Saturday, 6 November 1965. Three days earlier, the fugitives had stolen a car to exchange it for the stolen one they had, and thereby had provoked a shooting with the Uruguayan police; the shooting had taken place next to the TV channel canal 8 from Montevideo,  which would later be the only channel with live coverage of the siege. 

In the siege, both El Nene and Dorda/Angel died; according to a police officer who was interviewed for a documentary about the events and the release of Piñeyros film, the bodies of the two were found holding hands; they were, however, separated before the official police photographs were taken from the location. Both were half-naked and in underwear, in a pool of blood, as shown in the film. In contrast to the movie, El Cuervo/Mereles was mortally wounded, but did not die during the siege. Spat on, hit, kicked and insulted by the police, he was relocated to a public hospital, where he died a few hours later. The three were anonymously buried in the Northern Cemetery of Montevideo at 8am on Wednesday, 10 November 1965. Only El Nenes body was reclaimed by his family and transferred back to Argentina. In February 1966, Malito/Fontana was seen in Flores, a Buenos Aires neighborhood, and there shot by the police. 

==Distribution==
The film opened wide in Argentina on May 11, 2001. In the United States it opened on October 4, 2002, on a limited basis.

The film was shown at various film festivals, including: the Toronto Film Festival, Canada; the Palm Springs International Film Festival, United States; the Berlin International Film Festival, Germany; and others.

===Release dates===
* Argentina: May 11, 2001.
* Uruguay: June 2, 2001.
* Spain: September 1, 2001.
* France: February 14, 2002.

==Awards==
Wins
* Havana Film Festival: Best Cinematography, Alfredo F. Mayo; Best Sound, Carlos Abbate and José Luis Díaz; 2001.
* Goya Awards: Goya; Best Spanish Language Foreign Film, Marcelo Piñeyro, Argentina; 2002.
* Argentine Film Critics Association Awards: Silver Condor; Best Adapted Screenplay, Marcelo Piñeyro and Marcelo Figueras; 2001.
* Glitter Awards: Glitter Award; Best Feature voted by the U.S. Gay Film Festivals, Marcelo Piñeyro; 2002.

Nominations
* Argentine Film Critics Association Awards: Silver Condor, Best Actor, Leonardo Sbaraglia; Best Cinematography, Alfredo F. Mayo; Best Director, Marcelo Piñeyro; Best Editing, Juan Carlos Macías; Best Music, Osvaldo Montes; Best New Actress, Dolores Fonzi; 2001.
* Fotogramas de Plata, Spain: Fotogramas de Plata, Best Movie Actor, Eduardo Noriega; 2001.
* MTV Movie Awards, Latin America: MTV Movie Award, MTV South Feed (mostly Argentina) – Favorite Film, Marcelo Piñeyro; 2001.

==References==
 

==External links==
*  
*   at the cinenacional.com  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 