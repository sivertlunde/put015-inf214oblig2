No Way to Heaven
{{multiple issues|
 
 
}}

{{Infobox film
| image          =
| name           = No Way to Heaven
| director       = Janos Tedeschi    Christof Schaefer
| released       =  
| runtime        = 74 minutes
| country        = Switzerland
| language       = English, Swiss-German, Hindi, Gujarati
| producer       = Janos Tedeschi Christof Schaefer
}}
No Way to Heaven is a 2008 documentary feature about people allegedly living on light (Breatharianism), directed and produced by Swiss filmmakers Janos Tedeschi and Christof Schaefer. It is the world’s very first full-length film on this topic.

No Way to Heaven premiered in Switzerland in January 2008. The film had theatrical release in Switzerland in January 2009 and went on to screen in Estonia and the Netherlands.

==Synopsis==
No Way to Heaven begins on a remote island in Southeast Asia. There, Fritz Joss, a young man from the Swiss mountains has decided to stop eating – once and for all. He plans to find sustenance in pure (sun-)light by applying what is known as the "21 day process". Fritz sets off on his endeavour, but the planned conversion to "breatharianism" is much more difficult than anticipated.

==Cast==
The film features encounters with the following characters:
* Fritz Joss, a young Swiss yogi who stops eating and drinking by applying the "21-day process". Prahlad Jani, an 82-year ascetic Hindu, who for 74 years has survived allegedly without food and liquid.
* A team of doctors, headed by Inedia#2003 Tests|Prof. Dr. Sudhir Shah, a neurologist and respected scientist at Sterling Hospital Ahmedabad, India, who have conducted several case studies on breatharianism, most notably with Prahlad Jani in 2004 and 2010.
* Niranjan Bhagat, who gazes at the sun (sungazing) every day for 45 minutes and hence claims does not need to eat.
* Sanjay Padhiyar, a 110-year old Indian saint, who tells the story of how she forgot to eat during several years of meditation, living as a recluse in a cave in northern India.

DVD Extras:

* Jasmuheen, an Australian esoteric teacher and author, who introduced breatharianism (and the 21-day process) in her best-selling book "Living on Light" to Western audiences in the late 90s.
* Dr. Michael Werner, a German chemist, anthroposophist and author of "Leben durch Lichtnahrung", himself a breatharian of multiple years.
* Inedia#2003 Tests|Prof. Dr. Sudhir Shah (extended interview).
* Swami Vivekananda Saraswati, a prominent yogi and teacher of Fritz Joss.

==External links==
*  
*  

 
 
 
 
 
 


 
 