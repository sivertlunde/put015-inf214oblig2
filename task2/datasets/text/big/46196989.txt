The Girl from Chicago (1927 film)
(for a 1932 film directed by Oscar Micheaux, see The Girl from Chicago)
{{Infobox film
| name           = The Girl from Chicago
| image          =
| caption        =
| directed       = Ray Enright Frank Shaw (assistant)
| producer       = Warner Brothers Graham Baker (scenario) Red Book June 1923, Button, Button, by Arthur Somers Roche
| starring       = Myrna Loy Conrad Nagel
| cinematography = Hal Mohr
| editing        =
| distributor    = Warner Brothers
| released       = November 19, 1927
| runtime        = 70 minutes
| country        = USA
| language       = Silent..English titles
}} lost    1927 silent film criminal romance drama  directed by Ray Enright and starring Myrna Loy and Conrad Nagel. It was produced and distributed by the Warner Brothers. The film later had a Vitaphone track of sound effects and music added.   

The film is one of the earliest starring roles for Loy who at this time, 1927, didnt usually star but was a supporting player. Warners took a chance casting her in a principal part. 

==Cast==
*Conrad Nagel - Handsome Joe
*Myrna Loy - Mary Carlton William Russell - Big Steve Drummond
*Carroll Nye - Bob Carlton
*Paul Panzer - Dopey
*Erville Alderson - Colonel Carlton

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 


 
 