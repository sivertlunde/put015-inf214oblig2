The Spectacular Now
 
{{Infobox film
| name = The Spectacular Now
| image = The Spectacular Now film.jpg
| alt =
| caption = Theatrical release poster
| director = James Ponsoldt
| producer = {{Plain list|
* Tom McNulty
* Shawn Levy
* Michelle Krumm
* Andrew Lauren
}}
| screenplay = {{Plain list|
* Scott Neustadter
* Michael H. Weber
}}
| based on =  
| starring = {{Plain list|
* Miles Teller
* Shailene Woodley
* Brie Larson
* Jennifer Jason Leigh
* Kyle Chandler
 
}}
| music = Rob Simonsen
| cinematography = Jess Hall
| editing = Darrin Navarro
| studio = {{Plain list|
* Andrew Lauren Productions 21 Laps
* Global Produce
}} A24 
| released =   
| runtime = 95 minutes 
| country = United States
| language = English
| budget = $2.5 million 
| gross = $6.9 million   
}}
 romantic comedy-drama premiered at the 2013 Sundance Film Festival, where it garnered critical acclaim. The film was a considerable box office success, making nearly three times its budget. Since its release, The Spectacular Now has gained a cult following, and is regarded as one of the most realistic depictions of a film in its genre.

==Plot==
Sutter Keely (Miles Teller) is a high school senior, charming and self-possessed. Hes the life of the party, loves his job at a men’s clothing store, and has no plans for the future. He is also a budding alcoholic. His girlfriend, Cassidy (Brie Larson), unable to cope with his alcoholism and lack of ambition, decides to dump him. Drinking to numb his pain, Sutter, passed out on a strangers lawn,  is awakened one morning after an alcohol-induced blackout, by Aimee Finicky (Shailene Woodley), a  girl who wears no makeup and reads science fiction and manga during her free time. Sutter helps her with her paper route, and ultimately invites her to have lunch with him. 

Sutter, struggling with geometry, asks Aimee to tutor him, ultimately doing so in order to get closer with her. He invites her to a party, but temporarily loses interest when he sees Cassidy and asks her to have a drink with him. Cassidy soon leaves with her boyfriend Marcus (Dayo Okeniyi), and Sutter redirects his attention to Aimee. He introduces her to alcohol, and the two go for a walk. During the walk, Aimee brings up getting accepted into a college in Philadelphia, but does not feel as though she will be able to go because she must take care of her demanding mother. Sutter teaches Aimee to stand up to her mom, and the two kiss. 

When he wakes up the next morning, Sutter realizes that he asked Aimee to prom while drunk, and instant messages Cassidy to hang out. Cassidy breaks down, tearfully telling Sutter that she can no longer avoid her future and that she needs to grow up, even if he does not. Marcus later confronts Sutter asking what is going on with him and Cassidy. Sutter calms him down by reassuring him that Cassidy is done with him. Marcus asks a surprised Sutter why Cassidy cannot like him in the same way that she liked Sutter. Sutter responds by saying Marcus and Cassidy are perfect for each other and that he does not need to try to be like him. Marcus thanks Sutter for the advice and tells Sutter he Is not the joke everyone thinks he is.

Sutter avoids Aimee knowing he cant commit to taking her to prom. Aimees friend Krystal (Kaitlyn Dever) tells him not to treat Aimee badly as she is a genuinely good person and doesnt need Sutter hurting her. Sutter takes her advice and asks Aimee to dinner with his estranged sister Holly (Mary Elizabeth Winstead). At the dinner Aimee talks frankly about the death of her father and her dreams of a perfect marriage, charming everyone including Sutter. Sutter and Aimee begin to get serious, having sex for the first time. Sutter buys Aimee a flask as a gift when they attend prom, and despite a moment where Sutter dances with Cassidy, their night goes well. She tells him that she stood up to her mom and is going to Philadelphia, inviting him to live with her and attend a Junior College. 

Sutter, after numerous attempts to get his fathers phone number from his mom, receives it from his sister and plans to spend a day with his dad. He brings Aimee with him, and they meet him at a motel just as he is leaving for the bar and invites the two to join him. Sutters father (Kyle Chandler) shares similar hedonistic views of life as Sutter, but after revealing that he left his mother (instead of being kicked out as Sutter had thought previously), he leaves to go off with a woman. Sutter is forced to pay the tab and wait for his dad to return. After waiting for an hour they return to the bar to see that Sutters dad has forgotten about them and is back to drinking with his friends. Angry and drunk, Sutter snaps at Aimee when she attempts to comfort him, confessing her love for him. Sutter forces her to get out of the car, and she is hit by an oncoming car. 

Aimee comes out of the accident with only a broken arm, and forgives Sutter for the incident. Sutter, however, has clearly been damaged by his experiences with his father, and his drinking worsens. After graduation Cassidy informs Sutter she is going to California with Marcus. Sutter suggests he pay them a visit but Cassidy declines, telling him she must stay out of his influence for her own good. His boss, Dan (Bob Odenkirk) tells him that he will be letting go of one of his clerks. He informs Sutter that he wants him to work at the store, but Sutter must promise never to come in intoxicated again. Sutter honestly states that he does not feel he is capable of keeping such a promise and shakes a disappointed and concerned Dans hand. He then goes out for a night at the bar, leaving Aimee to get on the bus to Philadelphia by herself, heartbroken. 

Sutter crashes into his mailbox after a night of heavy drinking and tearfully breaks down in front of his mom, saying he is exactly like his father and that he is an awful person. His mother assures Sutter he is not like his father as he has the biggest heart of anyone she knows. Finally able to recognize that he is his own greatest limitation, Sutter commits himself to becoming a more mature and responsible person. He drives to Philadelphia and finds Aimee as she is leaving class. Aimee looks at him and begins to smile as the screen cuts to black.

==Cast==
* Miles Teller as Sutter Keely
* Shailene Woodley as Aimee Finecky
* Brie Larson as Cassidy
* Jennifer Jason Leigh as Sara Keely
* Kyle Chandler as Tommy Keely
* Mary Elizabeth Winstead as Holly Keely
* Dayo Okeniyi as Marcus
* Andre Royo as Mr. Aster
* Bob Odenkirk as Dan
* Kaitlyn Dever as Krystal
* Masam Holden as Ricky
* Gary Weeks as Joe
* Whitney Goin as Mrs. Finecky
* Nicci Faires as Tara

==Production==
  did. Athens was such an obvious candidate as a setting to shoot the film in – and it was really the only place I wanted to make the film. Filming in Athens was incredibly meaningful to me. We shot in the streets and houses of my childhood!" 

==Reception==
===Box office===
The Spectacular Now opened in limited release in North America on August 2, 2013 in 4 theaters and grossed $197,415 with an average of $49,354 per theater and ranking #30 at the box office. The films wide release was in 770 theaters and it ended up earning $6,854,611 domestically and $63,980 elsewhere for a total of $6,918,591, above its estimated $2.5 million budget.  

===Critical response===
The Spectacular Now was warmly received at the 2013 Sundance Film Festival. Based on 148 critic reviews, it obtained a "Certified Fresh" score of 93% on Rotten Tomatoes with an average rating of 7.8 out of 10. The critical consensus states "The Spectacular Now is an adroit, sensitive film that avoids typical coming-of-age story trappings."    The film also has a score of 82 out of 100 on Metacritic based on 42 critics, indicating "universal acclaim".   
 Say Anything" period. Woodley is beautiful in a real person sort of way, studying him with concern, and then that warm smile. We have gone through senior year with these two. We have known them. We have been them."  

Richard Roeper of the Chicago Sun-Times also gave the film four stars out of four, which he described as "the best American movie of the year so far" and summarized his review by adding, "The Spectacular Now will bring you back to that time in your life when you were trying to soak in every moment, because everyone told you there’s nothing better than your last year in high school."    In The Hollywood Reporter, critic Todd McCarthy called the film "a sincere, refreshingly unaffected look at teenagers and their attitudes about the future... Ordinary in some ways and extraordinary in others, The Spectacular Now benefits from an exceptional feel for its main characters on the parts of the director and lead actors." 
 The Perks of Being a Wallflower, saying "like them, it’s a movie about the experience of being caught on the cusp and truly not knowing which way you’ll land."   

In Variety (magazine)|Variety, critic Rob Nelson wrote, "The scars and blemishes on the faces of the high-school lovers in The Spectacular Now are beautifully emblematic of director James Ponsoldts bid to bring the American teen movie back to some semblance of reality, a bid that pays off spectacularly indeed."  Cinema Blend called it "the rare Sundance coming-of-age story that feels like it matters,"  adding, "The Spectacular Now is an instant MVP of the first half of the festival, with potential breakout hit written all over it... youll be hearing a lot about this one down the road, and its got the goods to live up to the hype." Phoebe Reilly of Spin (magazine)|Spin called the film "the next great teen movie" and "truly remarkable". She acclaimed Teller and Woodley for their "absurdly natural performances", with Sutter "uniquely irresistible" and Aimee "a perfect repertoire of nervous giggles and awkward mannerisms." 

==Accolades== Special Jury Award for Acting.   

{| class="wikitable" style="font-size: 95%;"
|+ List of awards and nominations
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| rowspan="3"| Alliance of Women Film Journalists 
| rowspan="3"| December 16, 2013
| Best Adapted Screenplay
| Scott Neustadter and Michael H. Weber
|  
|-
| Best Breakthrough Performance
| Shailene Woodley
|  
|-
| Best Depiction of Nudity, Sexuality or Seduction Award
| Shailene Woodley and Miles Teller
|  
|-
| rowspan="2"| Central Ohio Film Critics  
| rowspan="2"| January 2, 2014	
| Breakthrough Film Artist
| Brie Larson  
|  
|-
| Most Overlooked Film	
| The Spectacular Now
|  	
|-
| Gotham Awards   December 2, 2013
| Best Actress
| Shailene Woodley
|  
|- Independent Spirit Awards   29th Independent March 1, 2014 Best Screenplay
| Scott Neustadter and Michael H. Weber
|  
|- Best Female Lead
| Shailene Woodley
|  
|- Indiana Film Critics Association  December 16, 2013
| Best Picture
| The Spectacular Now
|  
|-
| Best Actor
| Miles Teller
|  
|-
| Best Actress
| Shailene Woodley
|  
|-
| Best Adapted Screenplay
| Scott Neustadter and Michael H. Weber
|  
|- National Board of Review   December 4, 2013
| Top Ten Independent Films
| The Spectacular Now
|  
|-
| North Carolina Film Critics Association  
| January 12, 2014
| Best Adapted Screenplay
| Scott Neustadter and Michael H. Weber
|  
|-
| Phoenix Film Critics Society   
| December 17, 2013
| The Overlooked Film of the Year
| The Spectacular Now
|  
|-
| San Diego Film Critics Society 
| December 11, 2013 Best Supporting Actress
| Shailene Woodley
|  
|-
| San Francisco Film Critics Circle 
| December 15, 2013 Best Adapted Screenplay
| Scott Neustadter and Michael H. Weber
|  
|-
| Seattle International Film Festival  
| June 9, 2013
| Youth Jury Award for Best FutureWave Feature
| The Spectacular Now
|  
|-
| St. Louis Gateway Film Critics Association  December 16, 2013 Best Adapted Screenplay
| Scott Neustadter and Michael H. Weber
|  
|- Sundance Film Festival 2013 Sundance January 26, 2013
| Special Jury Award for Acting
| Miles Teller and Shailene Woodley
|  
|-
| Grand Jury Prize: U.S. Dramatic
| James Ponsoldt
|  
|-
| Washington D.C. Area Film Critics Association   December 9, 2013 Best Adapted Screenplay
| Scott Neustadter and Michael H. Weber
|  
|}

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 