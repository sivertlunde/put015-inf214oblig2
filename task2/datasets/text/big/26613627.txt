The Master (2009 film)
{{Infobox film
| name           = The Master 
| image          = UstaFilmPoster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Bahadır Karataş
| producer       = Mete Özok 
| writer         = Ayfer Tunç   Bahadır Karataş
| starring       = Yetkin Dikinciler   Fadik Sevin Atasoy   Şevket Çoruh
| music          = Ömer Özgür
| cinematography = Mirsad Herović
| editing        = Evren Aksoy
| studio         = Filmpark
| distributor    = 
| released       =  
| runtime        = 118 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
}}
The Master ( ) is a 2009 Turkish drama film, directed by Bahadır Karataş, starring Yetkin Dikinciler as a small town car mechanic in who builds a small biplane in his backyard. The film, which went on nationwide general release across Turkey on  , won a Best Supporting Actress nomination for Hasibe Eren at the 3rd Yeşilçam Awards.

== Production ==
The film was shot on location in Eskişehir, Turkey.   

== Plot ==
Dogan, a car mechanic in a small town, is obsessed with              flying. Hes been building a small biplane in his backyard for               years. His wife Emine does not share his affinity with aviation               and feels neglected in their marriage. When Dogans plane crashes during               an aeronautics fair, she gives him an ultimatum and he is left at a               crossroads. Will he still pursue his passion for flying, or will he be               content with a humble life with his wife, solely as a car mechanic?

== See also ==
* 2009 in film
* Turkish films of 2009

==References==
 

==External links==
*    
*  

 
 
 
 
 
 

 