Life in a Fishbowl
 
{{Infobox film
| name           = Life in a Fishbowl
| image          = Life in a Fishbowl.jpg
| caption        = Film poster
| director       = Baldvin Zophoníasson
| producer       = 
| writer         = Baldvin Zophoníasson Birgir Örn Steinarsson
| starring       = Hera Hilmar
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 130 minutes
| country        = Iceland
| language       = Icelandic
| budget         = 
}}
 Best Foreign Language Film at the 87th Academy Awards, but was not nominated.   

==Cast==
* Hera Hilmar as Eik
* Thor Kristjansson as Sölvi
* Ingvar Þórðarson as Hannes
* Sveinn Ólafur Gunnarsson as Gústi
* Þorsteinn Bachmann as Móri
* Laufey Elíasdóttir as Aníta
* Birgir Örn Steinarsson as Rakari / Barber
* Markus Reymann as Gerald

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Icelandic submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 