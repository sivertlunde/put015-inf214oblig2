Mehbooba
 
{{Infobox film
| name           = Mehbooba
| image          = Mehbooba1976.jpg
| image_size     = 
| caption        = 
| director       = Shakti Samanta
| producer       = Mushir-Riaz
| writer         = Gulshan Nanda
| narrator       = 
| starring       = Rajesh Khanna Hema Malini Prem Chopra Nazir Hussain
| music          = Rahul Dev Burman Anand Bakshi (lyrics)
| cinematography = Aloke Dasgupta
| editing        = Bijoy Chowdhury
| distributor    = 
| released       = 19 July 1976
| runtime        = 
| country        = India Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Mehbooba is a 1976 Hindi film produced by Mushir-Riaz and directed by Shakti Samanta. The film stars Rajesh Khanna, Hema Malini and Prem Chopra. The music is composed by Rahul Dev Burman. The plot it is based on Gulshan Nandas novel Sisakate Saaz, and Nanda also wrote the screenplay himself.

==Plot==

Singer Suraj (Rajesh Khanna) is stranded in a resthouse during a rainstorm. While there he experiences the feeling he has been there before. He feels himself transported to another lifetime, and is drawn to a woman named Ratna (Hema Malini). He is unable to forget the experience and, investigating further, discovers that in his past life he was a Chief Singer in the emperors court - his name was Prakash, he was in love with a courtesan named Ratna, much to the chargin and anger of his family and the emperor, and they were tragically separated. He subsequently comes across a portrait of Ratna, and shortly meets a gypsy, Jhumri (also Malini) who looks exactly like Ratna. Soon Jhumri is also able to remember her past life and her love for Prakash to the chagrin of Rita Malhotra (Asha Sachdev) who loves Suraj, and Appa (Prem Chopra), the son of the leader of the gypsies (Madan Puri), who will stop at nothing to make Jhumri his wife. Appa steals the portrait, and sets the gypsies against Suraj, so that he can marry Jhumri, and perhaps separate the two lovers again.

==Fact==
Kishore Kumar too wanted to sing Mere Naina Sawan Bhadho that was initially written for Lata Mangeshkar. R. D. Burman agreed and wanted to record Kishore Kumar first. Kishore Kumar insisted Lata Mangeshkar record first. Though unacceptable at first, R. D. Burman yielded. Lata Mangeshkar recorded the song with great difficulty as it demanded great vocal strengths with long notes (reference needed). Kishore Kumar had his version recorded. Till date his version is more famous than Lata Mangeshkars (reference?). He listened to the song for 7 days before recording (reference?). This Song Is Based On "Raga Shivranjani"
This Song is considered as the most beautiful song sung by Kishore Da. We can feel the extreme variations and voice range in this song.

==Cast==
{|class="wikitable"
|-
!Actor/Actress !!Character/Role !!Notes
|- Rajesh Khanna
|Prakash/Suraj
|- Hema Malini
|Ratna/Jhumri
|- Prem Chopra Appa
|- Asrani (actor)|Asrani Sinha
|- Asha Sachdev Rita Malhotra
|- Harindranath Chattopadhyay
|Ritas father
|- Sujit Kumar
|Dr. Vinod
|- Manmohan Krishna Bandhe Ali Khan as Manmohan Krishan
|- Madan Puri Sardar
|- Meena T. Gauri
|- Nazir Hussain Guruji
|as Nazir Hussain
|- Leela Mishra
|Jhumris mother as Leela Misra
|- Alankar Joshi Buntu
|as Master Alankar
|- Chandrashekhar
|Thakur Himmat Singh
|- Yogeeta Bali Jamuna Singh Guest Appearance
|-
|}

==Crew==
*Director: Shakti Samanta
*Story: Gulshan Nanda
*Screenplay: Gulshan Nanda
*Dialogue: Akhtar Romani
*Producer: Mushir Alam, Mohammad Riaz
*Production Company: M. R. Productions
*Cinematographer: Aloke Dasgupta
*Editor: Bijoy Chowdhary
*Art Director: Shanti Dass
*Stunts: M. B. Shetty
*Costume and Wardrobe: Bhanu Athaiya, Jaya Chakravarthy, Dattaram, Shalini Shah, Shriman
*Choreographer: Suresh Bhatt, Gopi Krishna
*Music Director: Rahul Dev Burman
*Lyricist: Anand Bakshi
*Playback Singers: Manna Dey, Kishore Kumar, Lata Mangeshkar

==Soundtrack==
Song Mere Naina Sawan Bhadon is an evergreen number and Parbat Ke Peechhe too was popular number.
{|class="wikitable"
|-
!Song Title !!Singers !!Time
|- Mere Naina Sawan Bhadon Lata Mangeshkar
|6:20
|-	 Mere Naina Sawan Bhadon Kishore Kumar 
|5:25
|-	 Gori Tori Paijaniya Manna Dey 
|6:00
|- Parbat Ke Peechhe Lata Mangeshkar, Kishore Kumar 
|3:55
|-	 Jamuna Kinare Aa Ja Lata Mangeshkar 
|5:05
|-	 Chalo Ri Chalo Ri Lata Mangeshkar 
|4:00
|-	 Aapke Shahar Mein Aai Hoon Lata Mangeshkar 
|5:00
|-
|}

== External links ==
*  

 
 
 
 
 
 
 