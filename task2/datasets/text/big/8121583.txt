Houdini (film)
{{Infobox film
| name           =  Houdini
| image          = Houdini.jpg
| caption        = Original film poster George Marshall 
| producer       = George Pal Berman Swarttz
| writer         = Philip Yordan Harold Kellock (book)
| starring       = Tony Curtis Janet Leigh Torin Thatcher
| music          = Roy Webb 
| cinematography = Ernest Laszlo 
| editing        = George Tomasini
| distributor    = Paramount Pictures
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $1.8 million (US) 
| preceded_by    = 
| followed_by    = 
}}
Houdini is a 1953 biographical film about the life of the magician and escapologist Harry Houdini, played by Tony Curtis, co-starring with then-wife Janet Leigh.
 George Marshall and produced by George Pal from a screenplay by Philip Yordan, based on the book Houdini by Harold Kellock. The music score was by Roy Webb and the cinematography by Ernest Laszlo. The art direction was by Albert Nozaki and Hal Pereira and the costume design by Edith Head.

The film details a highly fictional account of Houdinis life. The film follows his most dangerous stunts and magic tricks along with his love Bess Houdini. 

The death of the magician is depicted in the film as a failure to escape the Chinese Water Torture Cell; in real life Houdini died of peritonitis.

==Plot==
The following synopsis is copied nearly word-for-word from the Turner Classic Movies website.

In the 1890s, young Harry Houdini is performing with a Coney Island carnival as Bruto, the Wild Man, when Bess, a naive onlooker, tries to protect him from the blows of Schultz, his "trainer." Harry then appears as magician The Great Houdini and, spotting Bess in the audience, invites her on stage. Harry flirts with the unsuspecting Bess during his act, but she flees from him in a panic. When Bess shows up to watch Harry perform two more times, however, he corners her. Bess admits her attraction, and soon after, the two appear at Harrys mothers house, newly married. Bess becomes Harrys onstage partner, touring the country with him, but soon grows tired of the low pay and grueling schedule. 
 German magician, retired at the height of his career after performing a similar feat, fearful of his own talents. Bess then persuades Harry to give her his prize, a single, round-trip boat ticket to Europe, so that she can cash it in for a down payment on a house. 

Later, at the factory, Harry locks himself inside one of the big safes, determined to make an escape. Before he can get out, however, the foreman orders the safe blown open, then fires Harry. That night, in front of his mother, Harry and Bess argue about their future, and frustrated by Besss insistence that he quit magic, Harry walks out. Soon, a contrite Bess finds Harry performing with a carnival and presents him with two one-way tickets to Europe. Sometime later, at a London theater, Harry and Bess are concluding their magic act when a reporter named Dooley challenges Harry to break out of one of Scotland Yards notoriously secure jail cells. Harry, who hired Dooley to issue the challenge, accepts the challenge, unaware that the jails cells do not have locks in the door, but on the outside wall. Despite the added difficulty, the dexterous, determined Houdini picks the cell lock and appears on time for his next performance. Now billed as the "man who escaped from Scotland Yard," Harry begins a successful tour of Europe with Bess. 

In Berlin, Harry is joined by his mother and begins searching for the reclusive Von Schweger. While performing an impromptu levitation trick with Bess at a restaurant, Harry is arrested for fraud. During his trial, Harry denies that he ever made claims to supernatural powers, insisting that all his tricks are accomplished through physical means. To prove his point, Harry locks himself in a safe in the courtroom and breaks out a few minutes later, noting that safe locks are designed to keep thieves out, not in. Vindicated, Harry then goes to see Von Schweger, who finally has responded to his queries, but learns from Von Schwegers assistant, Otto, that the magician died two days earlier. Otto reveals that Von Schweger summoned Harry to ask him the secret of "dematerialization," a feat he accomplished once but could not repeat. Although Harry demurs, Otto insists on becoming Harrys new assistant and travels with him to New York. 

There, Harry finds he is virtually unknown, so for publicity, hangs upside down on a skyscraper flagpole, constrained by a straitjacket. Harry executes the escape and soon makes a name for himself in United States|America. To prepare to be submerged in a box in the chilly Detroit River, Harry bathes in an ice-filled bathtub. During the trick, which takes place on Halloween, the chain holding the box breaks, and the box drops upside down into an opening in the ice-covered river. Although Harry manages to escape from the box, the current drags him downstream, and he struggles to find air pockets under the ice and swim back to the opening. Above, Bess and the horrified audience assume Harry has drowned and proclaim his demise. To Besss relief, Harry shows up later at their hotel, saying that he heard his mothers voice, directing him toward the opening. Just then, Harry receives word that his mother died at the exact time that he heard her voice. 

Two years later in New York, Harry, who has not performed since his mothers death, reveals to Simms, a reporter, that he has been trying to contact his mothers spirit, without success. Harry invites Simms to attend a seance with him, and after the medium appears to have communicated with his mother, Harry and Otto expose her as a fake. After a public crusade against phony mediums, Harry decides to return to the stage and builds a watery torture cell for the occasion. Terrified, Bess threatens to leave Harry unless he drops the dangerous trick, and he agrees not to perform it. Before the show, Harry admits to Otto that his appendix is tender, but goes on, despite the pain. When the audience noisily demands that he perform the advertised "water torture" trick, Harry succumbs and is immersed, upside down, in a tank of water. Weak, Harry cannot execute the escape and loses consciousness. Otto breaks the tanks glass, and after reviving, the now-dying Harry vows to a weeping Bess that he will come back.

==Cast==
*Tony Curtis as Harry Houdini
*Janet Leigh as Bess Houdini
*Torin Thatcher as Otto 
*Angela Clarke as Harrys Mother 
*Stefan Schnabel as German Prosecuting Attorney 
*Ian Wolfe as Malue 
*Sig Ruman as Schultz 
*Michael Pate as Dooley 
*Connie Gilchrist as Mrs. Shultz 
*Malcolm Lee Beggs as British Jail Warden 
*Frank Orth as Mr. Hunter 
*Barry Bernard as Inspector Marlick
*Douglas Spencer as Simms

==References==
 

==External links==
*  
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 
 