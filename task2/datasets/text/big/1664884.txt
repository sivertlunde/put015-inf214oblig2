Night Train (1959 film)
 
{{Infobox film
| name           = Night Train
| image          =
| image_size     =
| caption        =
| director       = Jerzy Kawalerowicz
| producer       =
| writer         =  
| narrator       =
| starring       =  
| music          = Andrzej Trzaskowski
| cinematography = Jan Laskowski
| editing        = Wieslawa Otocka
| distributor    = Telepix
| released       =  
| runtime        = 93 minutes
| country        = Poland
| language       = Polish
| budget         =
| preceded_by    =
| followed_by    =
}} 1959 film directed by Jerzy Kawalerowicz.

==Plot==

Two strangers, Jerzy (Leon Niemczyk) and Marta (Lucyna Winnicka), accidentally end up holding tickets for the same sleeping chamber on an overnight train to the Baltic Sea coast. Also on board is Martas spurned lover, who will not leave her alone. When the police enter the train in search of a murderer on the lam, rumors fly and everything seems to point toward one of the main characters as the culprit.

==Critical Response==
 Polish School, Hitchcockian atmosphere, the unimaginably tight shots and the overall sense of claustrophobia and dread evoke the sense of disappointment following in the wake of 1956 and the end of the Polish Spring. All of Kawalerowicz’s films deal with individual fate in a society being crushed by overwhelming external forces, whether war or politics, in an attempt to examine moral choice under pressure. Night Train is no exception, only here he has created an allegory of misfits among a society of passengers, a society that is predictable, suspicious of individuality, and eager to punish. All of Poland escaping though the night to the end of the line. Ironically, the film may represent in its way the end of the Polish School as well.

==2009 Remake==
 The American remake of 2009, which went straight to video, was written and directed by Brian King and stars Danny Glover, Leelee Sobieski, and Steve Zahn.

== See also ==
*Cinema of Poland
*List of Polish language films

==External links==
* 

 

 
 
 
 
 
 
 


 
 