The Silent Partner (1917 film)
{{infobox_film
| name           = The Silent Partner
| image          =
| imagesize      =
| caption        =
| director       = Marshall Neilan
| producer       = Jesse Lasky
| writer         = George DuBois (scenario)
| story          = Edmund Goulding 
| cinematography = Walter Stradling
| editing        =
| distributor    = Famous Players-Lasky Paramount Pictures
| released       = May 10, 1917
| runtime        = 60 mins.
| country        = United States
| language       = Silent English intertitles
}}
 silent drama in 1923, and also released by Paramount Pictures.

==Cast==
*Blanche Sweet - Jane Colby
*Thomas Meighan - Edward Royle
*Henry Herbert - Harvey Wilson
*Ernest Joy - David Pierce
*Mabel Van Buren - Edith Preston
*Florence Smythe - Mrs. Preston
*Mayme Kelso - Mrs. Wilson


==See also==
*Blanche Sweet filmography

==References==
 

==External links==
*  
*  


 
 
 
 
 
 
 
 
 
 
 

 