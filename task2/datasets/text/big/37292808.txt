The Matsugane Potshot Affair
{{Infobox film
| name           = The Matsugane Potshot Affair
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Nobuhiro Yamashita
| producer       = Yuji Sadai
| writer         = Kōsuke Mukai Kumiko Sato Nobuhiro Yamashita
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Hirofumi Arai
| music          = Pascals
| cinematography = Takahiro Tsutai
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 122 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2006 Japanese drama film directed by Nobuhiro Yamashita, starring Hirofumi Arai.  

==Plot==
The lives of twin brothers Kotaro and Hikaru change when a womans body is found on the outskirts of Matsugane after being hit by a car. Kotaro, a police officer, discovers that the woman is still alive and later finds out that it was his brother who had almost killed her as well.

==Cast==
*Hirofumi Arai as Kotaro Suzuki
*Takashi Yamanaka as Hikaru Suzuki
*Tomokazu Miura as Toyomichi Suzuki
*Midoriko Kimura as Misako Suzuki
*Tamae Ando as Haruko Kuniyoshi
*Setsuko Karasuma as Izumi Kuniyoshi
*Miwa Kawagoe as Miyuki Ikeuchi
*Yūichi Kimura as Yuji Nishioka
*Mari Nishio as Yoko Togashi
*Ken Mitsuishi as Detective

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 


 
 