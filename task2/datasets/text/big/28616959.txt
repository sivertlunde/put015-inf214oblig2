Riding on Air
{{Infobox film name           = Riding on Air image          =Poster of the movie Riding on Air.jpg image_size     = caption        = director       = Edward Sedgwick producer       = David L. Loew writer         = Richard Macaulay (Elmer Rice character)  Richard Macaulay (screenplay) and  Richard Flournoy (screenplay) narrator       = starring       = See below music  Arthur Morton  Marlin Skiles cinematography = Alfred Gilks editing        = Jack Ogilvie distributor    = released       =   runtime        = 70 minutes country        = United States language       = English budget         = gross          =
}}

Riding on Air is a 1937 American film directed by Edward Sedgwick.

The film is also known as All Is Confusion in the United Kingdom.

==Plot summary== HAM radio operator, and gadget crazy Elmer Lane, in 1930s rural America.  In love with the beautiful Betty, he does everything he can to buy the paper outright; so, he can win her.  But, somehow something always comes out of the blue: gangsters, smugglers, murdered mobsters, rival newspaper reporters, con artists, police, new inventions, and small dogs, all get in the way. Its all "Riding on Air" how this fun, wild, ride, will land, or if the parachute will even open.

==Cast==
*Joe E. Brown - Elmer Lane
*Guy Kibbee - J. Rutherford "Doc" Waddington
*Florence Rice - Betty Harrison
*Vinton Hayworth - Harvey Schuman
*Anthony Nace - Bill Hilton
*Harlan Briggs - Mr. Harrison
*Andrew Tombes - Eddie Byrd
*Clem Bevans - Sheriff Harry C. Bradley - Mayor

==Soundtrack== Henry Cohen and Edward Sedgwick)

==External links==
* 
* 

 
 
 
 
 
 
 


 