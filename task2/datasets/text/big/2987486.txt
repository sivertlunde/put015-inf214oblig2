Proof (1991 film)
{{Infobox film
| name           = Proof
| image          = Proof_movie.jpg
| image_size     =
| caption        = DVD cover
| director       = Jocelyn Moorhouse
| producer       = Lynda House
| writer         = Jocelyn Moorhouse
| starring       = Hugo Weaving, Geneviève Picot, Russell Crowe
| music          = Not Drowning Waving Martin McGrath
| editing        = Ken Sallows Roadshow Entertainment
| released       =   15 August 1991
| runtime        = 86 minutes
| country        = Australia
| language       = English
| gross          = $524,668 Domestic 
}}
 

Proof is a 1991 Australian drama film by Jocelyn Moorhouse starring Hugo Weaving, Geneviève Picot and Russell Crowe. It was chosen as "Best Film" at the 1991 Australian Film Institute Awards, along with 5 other awards, including Moorhouse for "Best Director", Weaving for "Best Leading Actor", and Crowe for "Best Supporting Actor".

==Plot== blind photographer. Through a series of Flashback (narrative)|flashbacks, Martin is shown as a child, distrustful of his own mother, as she describes to him the garden outside his bedroom window. She tells him that someone is raking leaves, but he cant hear the sound and angrily decides she is lying to him.

This childhood experience strongly affects Martin as an adult, as he anticipates that sighted people will take advantage of his blindness to lie to him, or worse yet, pity him. He has become a resentful, vaguely bitter person who spends his days taking photographs of the world around him, then having various people describe them. He uses these photographs and the Braille descriptions he stamps on them as "proof" that the world around him really is as others describe it to him. He also takes secret pleasure in rebuking the romantic advances of Celia (Picot), his housekeeper. Celia harbors a deep-seated and possibly obsessive crush on Martin, as evidenced by the scores of photographs of him adorning the walls of her flat, and takes out her frustration at her unrequited love by tormenting Martin in small ways, such as rearranging the furniture in his house. Martin keeps Celia around because her love and hatred of him means he knows she cant pity him.

One day, Martin encounters Andy (Crowe), and is pleased with the depth and detail with which Andy describes his photos. The two become fast friends, and Martin soon comes to trust him implicitly. The jealous Celia is threatened by Andys increasing presence in Martins life. She seduces Andy, and when Martin catches the two in the act, Andy reluctantly lies to him about it. Celia recognizes this opportunity to foil Martin yet again, and sets up a series of events leading Martin to discover Andys dishonesty. Martin, devastated, is plunged into a deep despair, and breaks off his friendship with Andy. Andy later confronts him, and tries to convince him that everyone has flaws, and shouldnt be judged on such simple terms. "People lie," he tells Martin, "but not all the time. And thats the point." Martin doesnt respond, but is swayed by Andys impassioned words. Near the storys conclusion, Martin decides to fire Celia, but acknowledges his own role in purposely antagonizing her in their love-hate relationship. Despite his openness she is angry that her efforts have gone to waste, and when asked to return her key to Martins house, she throws it in the sink.

Finally, Martin asks Andy to describe one last photo for him, one he has kept locked away for years. Andy does so, knowing nothing of its significance. It is a photo of the garden from Martins childhood, taken moments after his mother described it on that fateful day. Andys detailed description includes the iconic man raking leaves Martins mother had told him about, that he had rejected for all these years. This revelation provides Martin with his proof, and a much-needed emotional release.

==Production==
The film took four years to go from script to finished film. 
==Awards==
* 1991 Won  ), Best Actor in Supporting Role (Russell Crowe), Best Director, Best Film, and Best Screenplay
* 1991 Nominated  )
* 1991 Won  
* 1991 Won  
* 1992 Won  
* 1992 Won  

==Box Office==
Proof grossed $2,163,958 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 