Savages (1972 film)
{{Infobox film
| name           = Savages
| image          = Merchant and Ivory Savages DVD.jpg
| caption        = DVD cover James Ivory
| producer       = Ismail Merchant Joseph J.M. Saleh (executive) Anthony Korner (associate) James Ivory) Ultra Violet Salome Jens Kathleen Widdoes Thayer David Asha Puthli Martin Kove
| music          = Joe Raposo Bobby Short (theme song)
| cinematography = Walter Lassally
| editing        = Kent McKinney
| distributor    = Angelika Films
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         =
}}
 1972 Merchant James Ivory and screenplay by George W. S. Trow and Michael ODonoghue, based on an idea by Ivory.
  The Exterminating Angel, in which guests at an elegant dinner party become bestial. Writing began in late 1968 and continued through 1969. Its first showing came at the Cannes Film Festival in May 1972.

==Synopsis==
In contrast to Buñuels story, Savages starts when a tribe of primitive "mudpeople" performing a sacrifice encounter a croquet ball, rolling through their forest. Following it, they find themselves on a vast, deserted Westchester estate in the 1930s.

Entering, they begin to become civilized and assume the stereotypical roles and dress of people at a weekend party. There follows an allegory of upper-class behavior. At last, they begin to devolve toward their original status, and after a battle at croquet, they disappear into the woods. James Ivorys Savages (1972).]]

==Reception== Variety however, noted that "the playing has flair and grace." 

==Release==
This film has been released on DVD in 2004 as part of the Merchant-Ivory Collection produced by Criterion.

==Sources==
*Mr. Mike: The Life and Work of Michael ODonoghue by Dennis Perrin, 1999. ISBN 0-380-72832-X.

==References==

 

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 
 
 