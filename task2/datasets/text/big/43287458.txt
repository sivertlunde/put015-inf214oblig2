Men Without a Fatherland
{{Infobox film
| name = Men Without a Fatherland
| image =
| image_size =
| caption =
| director = Herbert Maisch
| producer = Bruno Duday  
| writer =  Gertrude Von Brockdorff  (novel)   Lotte Neumann    Ernst von Salomon       Walter Wassermann   Herbert Maisch
| narrator =
| starring = Willy Fritsch   Maria von Tasnady   Willy Birgel   Grethe Weiser
| music = Henri Rene  
| cinematography = Konstantin Irmen-Tschet  
| editing =  Milo Harbich   Gottfried Ritter     UFA 
| distributor = UFA
| released = 16 February 1937  
| runtime = 105 minutes
| country = Nazi Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Men Without a Fatherland (German:Menschen ohne Vaterland) is a 1937 German drama film directed by Herbert Maisch and starring Willy Fritsch, Maria von Tasnady and Willy Birgel. 

==Cast==
*  Willy Fritsch as Obeerleutnant Maltzach 
* Maria von Tasnady as Irene Marellus  
* Willy Birgel as Baron Falen  
* Grethe Weiser as Jewa - Chansonette  
* Siegfried Schürenberg as Hauptmann Angermann 
* Werner Stock as Leutnant Berndt  
* Erich Dunskus as Steputat - Unteroffizier 
* Josef Sieber as Pleikies - Bursche 
* Willi Schaeffers as Soykas - Landesrat  
* Nicolas Koline as Diener Stepan 
* Luis Rainer as Wolynski  
* Aribert Grimmer as Bauer Rauta  
* Lissy Arna as Mila Wentos 
* Hans Stiebner as Kigull 
* Maria Loja as Orla, Wirtin  
* Traute Baumbach as Maruschka, ein russisches Bauernmädel  
* Tamara Höcker as Ein kommunistisches Bauernmädel  
* Valy Arnheim as Ein russischer Kommandeur 
* Jur Arten as Ein russischer Offizier  
* Boris Alekin as Ein Kellner im Sarasan  
* Johannes Bergfeldt as Ein russischer Offizier  
* Zlatan Kasherov
* Werner Kepich as Ein russischer Agent 
* Herbert Klatt as Ein Soldat der Kompanie Maltzach  
* Karl Meixner as Ein Aufwiegler 
* Hermann Meyer-Falkow
* Hans Meyer-Hanno 
* Erich Nadler as Ein russischer Agent  
* Hellmuth Passarge as Ein Soldat der Kompanie Maltzach  
* Gustav Püttjer 
* Arthur Reinhardt as Ein Soldat der Kompanie Maltzach 
* Jakob Sinn as Zweiter Unteroffizier der Kompanie Maltzach 
* Theo Stolzenberg as Popott - Besitzer des Tanzlokals Sarasan  
* Albert Venohr as   Emmi de Néve     
* Hugo Gau-Hamm  
* Alexander Golling as Ischnikoff  
* Kai Möller as Ein Soldat der Kompanie Maltzach  
* Walter Raat-Kraatz 
* Hans Sobierayski as Ein Kapellmeister 
* Anny von Bornsdorf

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.
* Richards, Jeffrey. Visions of Yesterday. Routledge & Kegan Paul, 1973.


== External links ==
*  

 

 
 
 
 
 
 