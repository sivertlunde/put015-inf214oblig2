Vandikkari
{{Infobox film 
| name           = Vandikkari
| image          =
| caption        =
| director       = P. Subramaniam
| producer       = P Subramaniam
| writer         = Ponkunnam Varkey
| screenplay     = Ponkunnam Varkey
| starring       = Thikkurissi Sukumaran Nair Muthukulam Raghavan Pillai Adoor Pankajam S. P. Pillai
| music          = G. Devarajan
| cinematography = ENC Nair
| editing        = N Gopalakrishnan
| studio         = Neela
| distributor    = Neela
| released       =  
| country        = India Malayalam
}}
 1974 Cinema Indian Malayalam Malayalam film, directed and produced by P. Subramaniam . The film stars Thikkurissi Sukumaran Nair, Muthukulam Raghavan Pillai, Adoor Pankajam and S. P. Pillai in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Thikkurissi Sukumaran Nair
*Muthukulam Raghavan Pillai
*Adoor Pankajam
*S. P. Pillai
*Vijayasree

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Idavappaathikkoluvarunnu || P. Madhuri || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 