Aborto
 
{{Infobox film
| name           = Aborto
| image          = Aborto movie poster.jpg
| image_size     = 200px
| border         = 
| alt            = 
| caption        = 
| director       = Arindam Sil
| producer       = Globsyn Media Ventures, Cinemine Entertainment Pvt. Ltd., Maple Productions
| writer         = 
| screenplay     = 
| story          = Arindam Sil
| based on       =  
| narrator       = 
| starring       = Joya Ahsan, Tota Roy Chowdhury, Reshmi Ghosh, Abir Chatterjee, Kaushik Ganguly , Saswata Chatterjee
| music          = Bickram Ghosh
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =    
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}} Bengali film released on 1 March 2013 and directed by Arindam Sil. In this film Sil will do his directorial debut.   According to Sil, this film would be the greatest tribute to Satyajit Ray that has ever been made.       The screenplay was written by filmmaker Atanu Ghosh

== Plot ==
Shyamal Sen(Tota Roychowdhury) is GM sales for an MNC and is competing for the Vice President post of his company alongside Ranadip aka Runu Sanyal (Saswata Chatterjee). Shyamal hails from a middle-class family in Sonarpur where he has grown up with his elder brother (Kaushik Ganguly). He is now married to Charu(Joya Ahsan). Shyamal has always been jealous of his elder brother because he felt that his elder brother got all the attention in the family and was more intelligent among the siblings. He burned off his brothers certificates in a feat of jealousy as a child. This is shown in the films opening credits.

When Charu conceives, Shyamal persuades her for an abortion saying he has a training in London and cannot jeopardize his career at this juncture. Shyamal gradually becomes distant from his family in pursuit of a successful career. He moves to a swanky apartment in Kolkata leaving behind his ancestral home in Shonarpur. They soon adapt to the hi-life in Kolkata however his wife Charu still longs for the simple life back in Sonarpur.

The plot heats up when Runu Sanyal tries to sign up Hari Bose(Abir Chatterjee) as the brand ambassador for their company and Shyamal tries to crack a successful deal. Both of them confident that these achievements could put them in the VPs shoes. In the meanwhile Charu meets Hari Bose at the club. Hari is attracted to her and they end up going on a date on her birthday. The rat race gets dirty when Runu Sanyal uncovers the fact that Shyamal has slept with his Assistant DuliReshmi Ghosh while they were gone for a business trip and this puts Shyamal in a dicy position. Unable to face the truth Shyamal escaped to his ancestral home in Sonarpur and finds solace in his brothers kind words.

On Charus insistence Hari Bose turns down the deal with the company saying that Runu Sanyal has made this agreement with him on an understanding that he has to impart a percentage of his remuneration to Runu. All these incidents lead the Chairman of the company to select Shyamal as the new VP. Assistant Duli delivers the letter to Shyamals residence.

The film ends as Shyamal, now a changed man comes back to his wife and she welcomes the Vice President home.

== Cast ==
* Joya Ahsan 
* Tota Roy Chowdhury 
* Reshmi Ghosh 
* Abir Chatterjee 
* Kaushik Ganguly 
* Saswata Chatterjee
* Pijush Ganguly

== See also ==
  
* Balukabela.com
* Shunyo E Buke

== References ==
 

 
 


 