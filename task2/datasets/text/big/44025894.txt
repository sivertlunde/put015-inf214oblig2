A Tailor-Made Man
{{Infobox film
| name           = A Tailor-Made Man
| image          = A Tailor-Made Man (1922) - Grandin & Ray.jpg
| alt            = 
| caption        = Still with Ethel Grandin and Charles Ray
| director       = Joe De Grasse Charles Ray
| screenplay     = Albert Ray
| based on       =   Charles Ray Irene
| music          = 
| cinematography = George Meehan George Rizard
| editing        = Harry L. Decker 
| studio         = Charles Ray Productions
| distributor    = United Artists
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          = 
}}
 comedy silent Charles Ray, Tom Ricketts, Ethel Grandin, Victor Potel, Stanton Heck, Edythe Chapman, and Irene (costume designer)|Irene. The film was released on August 5, 1922, by United Artists.   Its survival status is classified as unknown,  which suggests that it is a lost film.

==Plot==
 

== Cast == Charles Ray as John Paul Bart
*Tom Ricketts as Anton Huber 
*Ethel Grandin as Tanya Huber
*Victor Potel as Peter
*Stanton Heck as Abraham Nathan
*Edythe Chapman as Mrs. Nathan Irene as Miss Nathan 
*Frederick A. Thomson  as Mr. Stanlaw 
*Kate Lester as Mrs. Stanlaw
*Jacqueline Logan as Corinne Stanlaw Frank Butler as Theodore Jellicot
*Douglas Gerrard as Gustavus Sonntag
*Nellie Peck Saunders as Kitty Dupuy
*Charlotte Pierce as Bessie Dupuy
*Thomas Jefferson as Gerald Whitcomb
*Henry A. Barrows as Hobart Sears 
*Eddie Gribbon as Russell
*Michael Dark as Cecil Armstrong
*Isabel Vernon as Mrs. Fitzmorris 
*Aileen Manning as Miss Shayn
*John McCallum as Butler
*William Parke Jr. as Rowlands
*Frederick Vroom as Harvey Benson
*Harold Howard as Arthur Arbuthnot
*S.J. Bingham as Cain
*Frederick Sullivan as Flynn

== References ==
 

== External links ==
 
*  

 
 
 
 
 
 
 
 
 
 


 