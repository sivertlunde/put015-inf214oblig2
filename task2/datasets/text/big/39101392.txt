Dead Before Dawn
{{Infobox film
| name           = Dead Before Dawn (3D)  Dead Before Dawn poster
| alt            =  
| caption        =
| director       = April Mullen
| producer       = April Mullen Tim Doiron
| screenplay     = Tim Doiron
| starring       = Devon Bostick Martha MacIsaac Christopher Lloyd Brandon Jay McLaren Brittany Allen April Mullen Tim Doiron Kyle Schmid
| music          = James Robertson Daniel Grant
| editing        = Luke Higinson
| studio         = WANGO Films
| distributor    = Gaiam Vivendi (US)
| released       =  
| runtime        = 88 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = $608,881.00 (Russia) 
}} zombie and demon.   Fangoria. Retrieved 29 May 2013.  It is Canadas first stereoscopic live-action 3D feature film,   Bloody-Disgusting.com. Retrieved 29 May 2013.  and Mullen is the first female to direct a live action, fully stereoscopic 3D feature film. 

== Plot ==
Casper Galloway, an overly cautious boy, has become afraid of anything dangerous since the day his father died at the Occult Barn, his grandfathers shop. When his grandfather Horus receives a lifetime achievement award, Casper mans the shop so Horus can collect the trophy. Horus warns "no-one is to come within spit distance" of an urn kept on a high shelf topped with a skull; Horuss father imprisoned a malevolent spirit inside the urn and it will curse the person that lets it out. The girl Casper has a crush on visits and he tries impressing her with a free purchase for loyal patronage to the Occult Barn. However, more of their friends arrive. Casper takes down the urn to show off to the girl and clumsily drops it. Casper tries warning them of the danger, but they laugh at him. Skeptical, they invent a curse: anyone with whom they make eye contact after 10pm (since Midnight is cliche) kills themselves and comes back as a "zemon", a zombie possessed by the evil spirit. The zemons spread their infection through hickeys rather than bites, and those infected kill themselves and become zemons. They are given till morning to undue the curse. However, they were too stupid to come up with an easy way to break the curse. 

When Casper spends the entire evening preparing for the curse, his concerned mother believes him to be on drugs. Once 10 oclock comes, Casper decides the curse is fake, makes eye-contact with his mother, and explains something happened at the Occult Barn, ending her worries. At a football game, players and cheerleaders begin killing themselves, and Casper and his friends realize the curse they made up has come true. Casper finds his mother dropped a toaster into the bathtub with her, and he flees when she comes back a zemon. Luckily, shes hit by a car; however Casper makes eye-contact with a passenger, forcing him to run again. Another of the group had stupidly ignored the curse and made eye-contact with everyone he met that night, speeding things up.

Everyone meets up, deciding to search the Occult Barns books for the way to break the curse. Horus returns, horrified to learn of the idiocy that occurred in his absence. Before he can explain how to undo the curse, he succumbs to it and jams his trophy into his skull; however, he tells them they have to dig. They retrieve the items needed to seal the spirit again: a human skull from the biology class, a mug to substitute an urn, and the watch belonging to Horus father (which they have to get out of his grave). However, most of the group is killed by the zemons, leaving only Casper and his crush. A final line in the instructions reveals that Casper must give his life to undo the curse. He sets off a grenade, ready to die, but instead time rewinds to the moment right before the urn broke; everyone retains memory of what happened (ensuring they dont cause another curse), but the urn is still replaced with the mug.

When Casper and his girlfriend graduate, they visit Horus and explain that they wish to become Occult Barn employees to pay for their new place. Overjoyed, Horus decides to take his first vacation ever; his attire suggests, hes going to Hawaii. After he leaves, the couple make out and accidentally knock down the mug. Horrified, they state, "Were so dead."

== Cast ==
* Devon Bostick as Casper Galloway
* Christopher Lloyd as Horus Galloway
* Martha MacIsaac as Charlotte Baker
* Brandon Jay McLaren as Dazzle Darlington
* Brittany Allen as Lucy Winthrop
* April Mullen as Becky Fords
* Tim Doiron as Seth Munday
* Kevin McDonald as Professor Reginald Duffy
* Ellen Dubin as Beverly Galloway
* Kyle Schmid as Patrick Bishop
* Rossif Sutherland as Burt Rumsfeld
* Dru Viergever as Zemon Josh
* Max Topplin as Dave
* Boyd Banks as the gas station attendant

== Production ==
The film was shot entirely Stereoscopic 3D  in 20 days in and around the Niagara Falls region of Canada in 2011. For their environmentally friendly practices, the film was awarded the Green Screen Award by Planet in Focus.  April Mullen used a new, immersive approach to the 3D found in the film. WANGO Films focused heavily on 3D throughout the conception of the idea; writing the script, choosing the locations, deciding blocking, designing the shots and makeup all with the 3D in mind.  The film was shot on two Red One cameras. 

== Release ==
The film has sold internationally  and Gaiam Vivendi picked up all rights in the United States.   The film premiered in North America at the Tiff Next Wave Film Festival.   It was released to Canadian theaters and on video on demand 2 August 2013, and it was theatrically released in America on 6 September 2013.   It was released on home video 1 October 2013. 

== Reception ==
Dead Before Dawn 3D has received largely mixed reviews. Lauren Taylor of Bloody Disgusting rated it 1.5/5 stars and wrote that the film has "moments of hilarity" but is "more concept than substance."   Anthony Arrigo of Dread Central rated it 2/5 stars and wrote, "Dead Before Dawn makes a commendable effort to introduce something new to the world of zombies, but those fresh concepts quickly lose their luster under the weight of a nonsensical script and hackneyed characters that will hold little value to audiences."   Gary Goldstein of the Los Angeles Times called it a "grade-Z horror comedy" that "makes the Scary Movie spoofs look positively brainy."   HorrorNews.net called it "a fresh perspective on a popular horror theme".  Frank Scheck of The Hollywood Reporter stated that "the broad, sophomoric gags generate few if any real laughs". 

=== Awards ===
The film won the Perron Crystal Award  while in Belgium for Best Live Action 3D feature film. The award was given by the Stereo Media Film Festival in conjunction with the International 3D Society. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 