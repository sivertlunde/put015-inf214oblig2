The Killing Room
{{Infobox film
| name           = The Killing Room
| image          = The Killing Room 2009.jpg
| alt            =  
| caption        = DVD cover
| director       = Jonathan Liebesman
| producer       = Ross M. Dinerstein Bobby Schwartz Guymon Casady Ben Forkner
| writer         = Gus Krieger Ann Peacock
| starring       = Chloë Sevigny Nick Cannon Timothy Hutton Clea DuVall Shea Whigham Peter Stormare Brian Tyler
| cinematography = Lukas Ettlin
| editing        = 
| studio         = 
| distributor    = ContentFilm
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Killing Room is a 2009 psychological thriller film directed by Jonathan Liebesman and starring Chloë Sevigny, Nick Cannon, and Timothy Hutton. It premiered at the 2009 Sundance Film Festival.  It is being distributed internationally by ContentFilm. 

==Plot==
Four individuals sign up for a psychological research study only to discover that they are now subjects of a brutal, modern version of the Project MKULTRA indoctrination program.    One by one, the subjects are brought into a large, white room, in which the tables and chairs have been bolted to the floor.

They are each given a questionnaire to fill out. In the meantime, a researcher enters the room, ostensibly to give an overview of the study. He indicates to the subjects—three men and a woman—that the study will take approximately eight hours to complete, at which time they will each be paid $250. Upon completing his introduction, the researcher shoots the female subject in the head with a gun and promptly leaves the room.

Over the next few hours, the remaining three male subjects will be subjected to additional physical and psychological brutality. Only one subject will survive the ordeal. This subject manages to escape into the building. The loudspeaker gives details of where the subject is in the building. It is then realized that the subject is going where he is supposed to be. He ends up in a room with two other males tied to their chairs. The loudspeaker then states that phase 2 is to begin.
 cells (a comparison noted in the film), by developing "civilian weapons" akin to suicide bombers.

==Reception==
The film met mixed reviews upon release, garnering a score of 67% on ratings aggregation website Rotten Tomatoes.    MTVs Larry Carroll held the film in high regard as he nominally labeled it as the “best movie” at Sundance 2009, praising it as “brutal, daring and utterly unpredictable”. Alongside other films with a claustrophobic air, he characterised it as “Cube (film)|Cube with better actors. Reservoir Dogs without the hipness. Lifeboat (film)|Lifeboat with a modern spin on war-time paranoia.”   

==Cast==
*Chloë Sevigny  as Ms. Emily Reilly
*Nick Cannon  as Paul Brody
*Timothy Hutton  as Crawford Haines
*Shea Whigham  as Tony Mazzola
*Peter Stormare  as Dr. Phillips
*Clea DuVall  as Kerry Isalano

==References==
 

==External links==
* 

 

 
 
 
 