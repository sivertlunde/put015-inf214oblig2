Road to Paradise (film)
{{infobox film
| name           = Road to Paradise
| image          = The_Road_to_Paradise_1930_Poster.jpg
| imagesize      =
| caption        =
| director       = William Beaudine
| producer       = First National Pictures
| writer         = F. Hugh Herbert
| based on       =  
| starring       = Loretta Young Jack Mulhall Raymond Hatton
| music          = Leo F. Forbstein
| cinematography = John F. Seitz
| editing        = Edward Schroeder
| studio         =  
| distributor    = First National
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
}}
Road to Paradise is a 1930 talkie|all-talking Pre-Code Hollywood|pre-code drama film produced and distributed by First National Pictures, a subsidiary of Warner Bros., and starring Loretta Young, Jack Mulhall and Raymond Hatton. It was directed by William Beaudine and is based on a 1920 Dodson Mitchell play called Cornered.  The film was a remake of a 1924 silent version, entitled Cornered, which was also directed by William Beaudine.

==Synopsis==
Loretta Young plays the part of an orphan who has been raised by two thieves (Raymond Hatton and George Barraud) and does not know that she has a twin sister who is now a wealthy socialite (Loretta Young as Margaret Waring). One day, while she is  dining at a Chinese restaurant with her two guardians, they notice the wealthy socialite and are taken aback at how closely she resembles Young. Hatton and Barraud convince Young that she should impersonate the socialite so that they can enter her house and steal the contents of her safe. Young enters the house and meet Jack Mulhall who senses something different about Waring and immediately falls in love with Young. When night falls, Young lets Hatton and Barraud into the house and they attempt to open the safe. Waring happens to enter the house and is shocked to find a woman that looks like her. She is wounded by Barraud and Young tricks the police into thinking that Waring is an imposter and thief. Even though Mulhall knows the truth, he keeps quiet because he is in love with Young. Eventually Young discovers that Waring is her twin sister when they discover that they have matching lockets. The charges against Waring are dropped and Young accepts Mulhalls proposal of marriage.

==Preservation==
The film survives intact has been showed on television and cable. Road to Paradise has been preserved at the Library of Congress.   The film is available on DVD from the Warner Archive Collection as a double bill with another Loretta Young film Week-End Marriage.

==Cast==
*Loretta Young - Mary Brennan/Margaret Waring
*Jack Mulhall - George Wells
*Raymond Hatton - Nick
*George Barraud - Jerry The Gent
*Kathlyn Williams - Mrs. Wells
*Fred Kelsey - Police Inspector Casey
*Purnell Pratt - Police Inspector Updike
*Ben Hendricks Jr. - Flanagan
*Dot Farley - Lola
*Winter Hall - Brewster The Butler
*Georgette Rhodes - Yvonne, French Maid
*Wong Chung - Waiter
*Jim Farley - Police Officer Farley
*Bess Flowers - Nurse
*Clarence Geldart - Doctor Tom Wilson - Jerry the Greek

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 