Palais Royale (film)
 
{{Infobox film
| name           = Palais Royale
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Martin Lavut
| producer       = David Daniels Lawrence Zack
| writer         = Hugh Graham
| screenplay     = Hugh Graham, Joanne McIntyre, David Daniels. 
| story          = by Hugh Graham. 
| based on       =  
| narrator       = 
| starring       =  Kim Cattrall Matt Craven Kim Coates Dean Stockwell
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes 
| country        =   English
| budget         = 
| gross          = 
}} 
Palais Royale (alternative titles Smoke Screen or Smokescreen) is a 1988 Canadian comedy film.

==Plot==
This dark crime comedy is set in 1959 where Gerald Price (Matt Craven) is a newcomer to Toronto. He competes with mobster Tony Dicarlo (Kim Coates) for the affections of Odessa Muldoon (Kim Cattrall). Meanwhile Michael Dattalico (Dean Stockwell) is eager to expand his organized crime business in Toronto.  

==Cast==
* Kim Cattrall as Odessa Muldoon
* Matt Craven as Gerald Price
* Kim Coates as Tony Dicarlo
* Dean Stockwell as Michael Dattalico
* Henry Alessandroni as Dominic
* Victor Ertmanis as Sal David Fox as Bob
* Brian George as Gus
* Sean Hewitt as Mr. Gillis Michael Hogan as Sergeant Leonard
* Helen Hughes as Mrs. McDermott
* Sam Malkin as Sam
* Dee McCafferty as Officer Nichol
* Robin McCulloch as Rick
* Mario Romano as Joey

==Release==
The film premiered at the Toronto International Film Festival on 10 September 1988, then given a general Canadian release on 12 May 1989.     The film was also released under the titles Smoke Screen or Smokescreen.  

==References==
 

==External links==
*  

 
 
 
 