At Ground Zero
 
{{Infobox film
| name           = At Ground Zero
| image          = At_Ground_Zero_1994_Poster.jpg
| caption        = Original Film Poster
| director       = Craig Schlattman
| producer       = Ivon Vasali Craig Schlattman
| writer         = Craig Schlattman
| starring       = Thomas Jane (credited as Tom Elliott) Aysha Hauer Brian Brophy
| music          = Fran Banish
| cinematography = Bubba Bukowski Jock Petersen Kevin Hudnell
| sound          = James Shortz Tim Tembruell Mikki Skitz
| editing        = Lester Fatt
| distributor    = Filmopolis Pictures
| released       = 1994
| runtime        = 112 min.
| language       = English
| budget         =
}} Independent feature Rutger Hauers Schlattman as Bubba. After receiving critical praise in the European and American press, At Ground Zero was given no advertising and a small release by a minor distributor, Filmopolis Pictures. It has since gone on to be an Indie favorite.

== Plot ==

Tom and Aysha (character names) are at the economic bottom of the LA scene; no money, on drugs, and selling themselves to exist. Tom brains his drug dealer, steals his drugs and the two split town by bus and thumb, heading ‘home’. Hitching a ride from a quixotic, dangerous character, Bubba, they put up with his advances until he too is ejected from his own car as the two free themselves from all restraint. Aysha starts a real spiral into heroin, as Tom joins in, but spirits are high. They run into a hilarious character, Carman, who they enlist in their fun, as the trip escalates into the threesomes’ investigation of freedom, drugs, and abandonment. Carman leaves the duo, and Aysha falls into a drug appetite Tom can’t understand and in frustration he hurts her. Together, but separate, they endure the car ride as they head for Minneapolis and home. In the end, the couples explore the detritus of their family, and lives, and even in betrayal have passion for each other. On a final, bittersweet note, Carman continues his self-destructive ways, as some hope endures.

== Cast ==

* Thomas Jane (credited as Tom Elliott) as Thomas Quinton Pennington
* Aysha Hauer as Aysha Almouth
* Brian Brophy Carman
* Craig Schlattman as Bubba

== Analysis ==
 Michelangelo Antonioni’s films, and has been called a contemporary Easy Rider.

== Production ==
 Brian Brophy’s LA apartment and let him read for the part. Craig cast him on the spot and they prepared to go on the road in the next couple of days.
 Corpus Christi where part of the cast and all of the crew returned to LA. Tom and Aysha then proceeded to Minneapolis with Craig as director and crew, where the culminating scenes were shot. On the return to LA they grabbed scenes and stole locations along the way. At Ground Zero is a fine example of guerrilla filmmaking at its best. Barely any production budget, some credit cards, a car, some gas, and a desire to finish the film is what motivated everyone. Cast and crew of At Ground Zero were handed the script and asked to make their decision to work on the film based on their reaction to the script, and a realistic understanding of the ‘budget’ they had to work with. Craig has said the edgy, Documentary film|documentary, direct cinema feel of this film was intentional, and pushed as far as possible considering the budget available. All the actors were given great leeway with the script and encouraged to use their improvisational skills to expand the characters, keep the energy high, and heighten the ‘realistic’ tone of this story. Craig has said the production technique of using the ups and downs and emotional swings of being with ‘strangers’ for a month on the road was a conscious decision to expand the emotional swings of the lead characters and their disintegrating relationship, and was helpful for the actors as the story, and everyones patience, headed for the culminating scenes.

== Reception ==
 Rotterdam International Film Festival to enthusiastic, full houses, and went on an extensive festival run in the US and Europe, garnering very positive reviews along the way –

 
"An impressive debut. With dark, dead-pan humor, experimental visual techniques and solid performances, At Ground Zero is a promising first feature." 
Jamie Painter – Film Threat  
 
 

"Powerful and distinctive. Impressive. ...a raw, edgy tale, shot through with dark humor... Schlattman directs with terrific wit and immediacy. Well acted...winning portrayals." 
Kevin Thomas – LA Times 
 

 
"Life on the road with a couple of junkies doesnt get much more interesting than in this impressive first feature by Craig Schlattman. This is a small treasure of independent filmmaking."  
Matt Langdon – LA Weekly  
 

== External links ==
*   at the Internet Movie Database
*   – official film website
*   – production company official website
*   (credited as Tom Elliott) at the Internet Movie Database
*   at the Internet Movie Database
*   at the Internet Movie Database
*   at the Internet Movie Database

 
 
 