Be-Imaan
{{Infobox film
| name           = Be-Imaan
| image          = 
| caption        = 
| director       = Sohanlal Kanwar
| producer       = Sohanlal Kanwar
| eproducer      = 
| aproducer      = 
| writer         = Sachin Bhowmick (story) Ram Kelkar (screenplay) Ved Rani (dialogue)
| starring       = Manoj Kumar Raakhee Premnath
| music          = Shankar Jaikishan
| cinematography = Radhu Karmakar
| editing        = Nand Kumar
| distributor    = 
| released       = 1972
| runtime        = 
| country        = India
| awards         = 
| language       = Hindi/Urdu
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Be-Imaan ( : بے ایمان) is a 1972 film directed by Sohanlal Kanwar. The film stars Manoj Kumar, Raakhee, Premnath, Pran (actor)|Pran, Prem Chopra and Tun Tun. The music is by Shankar Jaikishan. The film was remade in Tamil as En Magan with Sivaji Ganesan.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Patla Patla Reshmi Roomal"
| Mahendra Kapoor, Asha Bhosle
|-
| 2
| "Jai Bolo Be-Imaan Ki" Mukesh
|-
| 3
| "Dekhoji Raat Ko Julam Ho Gaya"
| Asha Bhosle
|-
| 4
| "Yeh Raakhi Bandhan Hai Aisa"
| Mukesh, Lata Mangeshkar
|-
| 5
| "Ek Ek Ginwata Hoon"
| Mukesh
|-
| 6
| "Hum Do Mast Malang"
| Kishore Kumar, Mahendra Kapoor
|-
| 7
| "Main To Chali Hoon Wahan"
| Sharda Rajan Iyengar|Sharda, Asha Bhosle
|}

==Awards==
*Filmfare Best Movie Award
*Filmfare Best Actor Award for Manoj Kumar
*Filmfare Best Director Award for Sohanlal Kanwar.
*Filmfare Best Music Director Award for Shankar Jaikishan Mukesh
*Filmfare Best Lyricist Award for Verma Malik
*Filmfare Best Supporting Actor Award for Pran (actor)|Pran, who refused to accept on the ground that the Filmfare award for Best Music should have gone to Paakeezah and not Shankar Jaikishan for Be-Imaan. 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 