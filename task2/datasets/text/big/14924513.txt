15 August (2001 film)
 
{{Infobox film
| name           = 15 August
| image          = 15 Août (movie poster).jpg
| caption        = Theatrical release poster
| director       = Patrick Alessandrin
| producer       = Luc Besson Bernard Grenet
| writer         = Lisa Alessandrin Richard Berry Charles Berling Jean-Pierre Darroussin
| music          = 
| cinematographer = Damien Morisot
| editing        = Yann Malcor
| distributor    = Gallo Film
| released       = 18 April 2001
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 2001 Cinema French film directed and written by Patrick Alessandrin.  

==Plot==
Some middle aged men quickly have a crisis on their hands when their wives depart leaving them to look after the boisterous kids.

==Cast== Richard Berry as Max 
* Charles Berling as Vincent 
* Jean-Pierre Darroussin as Raoul 
* Mélanie Thierry as Julie 
* Selma El Mouissi as Nina, Maxs daughter 
* Manon Gaurin as Alice, Maxs daughter
* Quentin Pommier as Arnold, Vincents son
* Thomas Goulard as Sébastien, Vincents son 
* Ludmila Mikaël as Louise Abel 
* Blandine Bury as Stéphanie 
* Dimitri Radochevitch as neighbour 
* Catherine Hosmalin as neighbour 
* Annette Merchandou as Madame André 
* Marie-Christine Demares as Madame Michaud, the real estate agent 
* Jean-François Gallotte as Fabrice, Julies father 
* Luc Sonzogni as swimming monitor

==References==
 

== External links ==
*  
*  

 
 
 
 
 


 
 