Let There Be Light (film)
{{Infobox film
| name           = Let There Be Light
| image          = Lettherebehuston.jpg
| caption        = Screenshot of the film
| director       = John Huston
| producer       = Charles Kaufman (uncredited)
| narrator       = Walter Huston (uncredited)
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         =
}} 1946 United American documentary film directed by John Huston.

The film was the last in a series of three films directed by Huston while serving in the United States Army Signal Corps. This documentary film follows 75 U.S. soldiers who have sustained debilitating emotional trauma and Depression (mood)|depression. A series of scenes chronicles their entry into a psychiatric hospital, their treatment and eventual recovery. Some of the treatments involved then-new drugs and hypnosis, and the impression was given of miraculous cures, though the narration says that there will be continuing psychiatric care. {{Citation | last = Canby   | first = Vincent   | title = LET THERE BE LIGHT, JOHN HUSTON VS. THE ARMY | newspaper = New York Times | date = January 16, 1981 | url =
http://www.nytimes.com/1981/01/16/movies/let-there-be-light-john-huston-vs-the-army.html
  | accessdate = Sep 2010}} 
 Deer Park, Long Island, New York which between 1944 and 1946 was part of Mason General Hospital, a psychiatric hospital run by the United States War Department named for an Army doctor and general.

The film was controversial in its portrayal of shell-shocked soldiers from the war. "Twenty percent of our army casualties", the narrator says, "suffered psychoneurotic symptoms: a sense of impending disaster, hopelessness, fear, and isolation."     Apparently due to the potentially demoralizing effects the film might have on recruitment, it was subsequently banned by the Army after its production, although some pirated copies had been made.  Military police once confiscated a print Huston was about to show friends at the Museum of Modern Art. The Army claimed it invaded the privacy of the soldiers involved, and the releases Huston had obtained were lost; the War Department refused to get new ones.   The release in the 1980s by Secretary of the Army Clifford Alexander, Jr. was attributed to his friend Jack Valenti who worked to get the ban lifted.  The National Archives now sells and rents copies of the film, and as a government work it is freely copied.

The film was screened in the Un Certain Regard section at the 1981 Cannes Film Festival.   

In 2010, this film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".      
 The Master, written and directed by Paul Thomas Anderson, borrowed lines and themes from Let There Be Light, which was included as an extra on the DVD/Blu-ray releases of The Master.

==References==
 

==External links==
* 
* 
* 
*  from the United States Governments "FedFlix" collection at archive.org

 

 
 
 
 
 
 
 
 
 
 
 