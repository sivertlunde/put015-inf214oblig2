Watership Down (film)
 
 
 
{{Infobox film
| name = Watership Down
| image = Movie_poster_watership_down.jpg
| image_size = 
| alt = A sunset depicting Bigwig in a snare, with the title in fancy font and the credits below.
| caption = US cinema poster Martin Rosen John Hubley  
| producer = Martin Rosen
| screenplay = Martin Rosen
| based on =  
| narrator = Michael Hordern
| starring = John Hurt Richard Briers Michael Graham Cox Simon Cadell Harry Andrews Zero Mostel
| music = Angela Morley Malcolm Williamson
| editing = Terry Rawlings
| studio = Nepenthe Productions CIC  Avco Embassy Pictures  
| released =  
| runtime = 101 minutes
| country = United Kingdom
| language = English budget = $4.8 million FILM CLIPS: Rabbit Test a Rivers Conception
Lee, Grant. Los Angeles Times (1923-Current File)   23 May 1977: e9.   gross = $3,713,768 (US) 
}} animated adventure adventure drama Martin Rosen and based on the novel Watership Down by Richard Adams. It was financed by a consortium of British financial institutions. Originally released on 19 October 1978, the film was an immediate success and it became the sixth most popular film of 1979 at the British box office.  It was the first animated feature film to be presented in Dolby surround sound.
 Bright Eyes", which was written by songwriter Mike Batt, briefly features.

==Plot==
According to Adams Lapine language, culture and mythology, the world was created by the god Frith, who represents the Sun. All animals lived harmoniously, but the rabbits eventually multiplied, and their appetite led to a food shortage. At the prayers of the desperate animals, Frith warned the rabbit prince El-ahrairah to control his people, but was scoffed at. In retaliation, Frith gave special gifts to every animal, but some animals he made predators to prey upon the rabbits. Satisfied that El-ahrairah ("Prince with a Thousand Enemies") had learned his lesson, Frith also gave the rabbits speed and cunning; while many would seek to kill them, the rabbits could survive by their wits and quickness.
 seer has an apocalyptic vision and goes with his older brother Hazel to beg the chief to have the warren evacuated, but they are dismissed and attempt to make an exodus themselves. The group meets resistance from the warrens police force called the Owsla, but eight manage to fight and escape: Fiver, Hazel, Bigwig, Blackberry, Pipkin, Dandelion, Silver and Violet. They travel through the dangerous woods and make it to a bean field to rest. In the morning, Violet is killed by a hawk, leaving the group without a female.
 snare trap. Fiver attempts to get help from their hosts, but is ignored. Bigwig is freed after nearly dying. As Fiver reveals, the warren is fed by a farmer who snares rabbits in return for his food and protection from predators. After Bigwigs narrow escape, the other rabbits willingly follow Fivers and Hazels advice and set out once more.
 hutch of Watership Down, where the rabbits settle.

They settle in, developing their own warren, with Hazel as chief. They befriend an   injured seagull, Kehaar, who offers to survey the local area for does. The rabbits return to Nuthanger to free the does; Hazel is shot by a farmhand and presumed dead, but Fiver has a vision and follows the apparition of the Black Rabbit of Inlé (a rabbit version of the Grim Reaper) to his injured brother. Kehaar returns and while removing buckshot pellets from Hazels leg, reports of Efrafa, a large warren with many females. Holly, who encountered Efrafa, begs them not to go there, describing it as a totalitarian state, run by vicious and heavily territorial rabbits. Hazel feels they have no choice but to go there. Bigwig infiltrates the colony and is made an Owsla officer by the cruel chief, General Woundwort. Bigwig recruits several potential escapees to his cause, including Hyzenthlay, an idealistic doe and Blackavar, a scarred attempted escapee. They flee, with Woundwort and his Owsla in pursuit. Using a boat to float down the river, they evade capture, helped by Kehaar. That night, Kehaar leaves for his homeland, with the gratitude of the warren.

Several days later, Efrafan trackers discover their trail and follow them to Watership Down. Hazel offers a treaty with Woundwort, who dismisses Hazel, telling him to turn over Bigwig and the other deserters or he will kill the entire warren. The Watership rabbits barricade their warren and are besieged by the Efrafans. Fiver slips into a trance, in which he envisions a dog loose in the woods. His moans inspire Hazel to free the dog from Nuthanger and lead him to the warren to intervene. He escapes with Blackberry, Dandelion and Hyzenthlay.

Hazel prays to Frith, offering his life for that of those in the warren, a bargain Frith acknowledges, but does not accept, as the outcome is ultimately up to Hazel. Hazel frees the dog while his companions bait it into following them to Watership Down; Hazel is attacked by the cat, but saved by Lucy (the owner of the hutch rabbits). When the Efrafans break through the warrens defences, Woundwort leads the attack. Blackavar confronts Woundwort, but is overpowered and killed. Bigwig ambushes Woundwort and they fight to exhaustion. The dog arrives and kills many of the Efrafan soldiers. Hearing the commotion, Woundwort abandons Bigwig and fearlessly confronts the dog. No trace of Woundwort is found, leaving his fate ambiguous.

Years later, the warren is thriving. An elderly Hazel is visited by the Black Rabbit Of Inlé, who invites him to "join his Owsla", assuring him of Watership Downs perpetual safety. Reassured, Hazel accepts and dies peacefully. Hazels spirit follows the Black Rabbit Of Inlé through the woodland and trees towards the Sun, which metamorphoses into Frith, and the afterlife.

==Cast==
{| class="wikitable"
|-

| Hazel || John Hurt
|-

| Fiver || Richard Briers
|-

| Bigwig || Michael Graham Cox
|-
 John Bennett
|-

| Threarah || Ralph Richardson
|-

| Blackberry || Simon Cadell
|-

| Pipkin || Roy Kinnear
|-

| Silver || Terence Rigby
|-

| Dandelion || Richard OCallaghan
|-

| Cowslip || Denholm Elliott
|-

| Kehaar || Zero Mostel
|-

| Woundwort || Harry Andrews
|-

| Hyzenthlay || Hannah Gordon
|-

| Campion || Nigel Hawthorne
|-

| Cat || Lynn Farleigh
|-

| Blackavar || Clifton Jones
|-

| Vervain || Derek Griffiths
|-

| Frith || Michael Hordern
|-

| Black Rabbit || Joss Ackland
|-
|}

==Production==
The film was originally to be directed by John Hubley, who died in 1977. His work can still be found in the film, most notably in the "fable" scene. He was replaced by the films producer Martin Rosen, his directorial debut.

After the genesis story rendered in a narrated simple cartoon fashion, the animation style changes to a detailed, naturalist one, with concessions to render the animals anthropomorphic only to suggest they have human voices and minds, some facial expressions for emotion and paw gestures. The animation backgrounds are watercolours. Only one of the predators, the farm cat, Tab, is given a few lines, the rest remaining mute.

The backgrounds and locations, especially Efrafa and the nearby railway, are based to the diagrams and maps in Richard Adamss book. Most of the locations in this movie either exist or were based on real spots in Hampshire and surrounding areas.

Although the film was fairly faithful to the novel, several changes were made to the storyline, mainly to decrease overdetailed complexity and improve pace and flow of the plot. In addition, the order in which some events occur is re-arranged. Unlike many animated features, the film faithfully emulated the dark and violent sophistication of the book. As a result, many reviewers took to warning parents that children might find the content disturbing. When the film was first submitted to the British Board of Film Classification, the BBFC passed the film with a U certificate (suitable for all ages), deciding that "whilst the film may move children emotionally during the films duration, it could not seriously trouble them once the spell of the story is broken and a U certificate was therefore quite appropriate". BBFC Examiners Report 15 February 1978 http://www.bbfc.co.uk/sites/default/files/attachments/Watership-Down-report.pdf  However in 2012, the BBFC admitted that it had "received complaints about the suitability of Watership Down at U almost every year since its classification".   

This attitude extended to when the animated Watership Down (TV series)|Watership Down TV series was marketed with the producers making an effort to reassure parents that the violence was softened and that the main characters would not be permanently harmed in their adventures. Though the third season took a slightly darker tone to try and attract more of an audience, though because of the darker tone, that season was never aired.

Some marketers in the U.S. were also worried that the main promotional poster (see above) appeared too dark and might scare some children. The poster is actually showing Bigwig in a snare (his distinctive fur is clearly visible), and the image on the poster does not appear in the film, which has a far bloodier depiction of the scene.

===Music===
The musical score was by Angela Morley and Malcolm Williamson, Morley replacing Williamson after the composer had fallen behind and only composed the prelude and main title theme in sketch form.  A list of the musical cues for the film can be found on the composers website, which also gives information about the different composers working on the project. 
 Bright Eyes", which was written by the British singer and songwriter Mike Batt.

==Reception==
 
The film was an immediate success at the UK box office and has received a generally positive critical reception, with an 80% Fresh rating on Rotten Tomatoes, and a rating of 67% from select critics.  The film was nominated for Hugo Award for Best Dramatic Presentation in 1979. In 2004, the magazine Total Film named Watership Down the 47th greatest British film of all time and it was also ranked 15th in the "100 Greatest Tearjerkers".

Investors in the film reportedly received a return of 5,000% on their investment. Alexander Walker, Icons in the Fire: The Rise and Fall of Practically Everyone in the British Film Industry 1984-2000, Orion Books, 2005 p6 

Despite its success at the UK box office, the film underperformed at the US box office, earning only US$3 million against a budget of $4 million.

The film is infamous for its dark tone and the fact that children are, to this day, still scarred from its scenes of violence.

==Media==

===Picture book===
A picture book of the animated film was also produced, titled The Watership Down Film Picture Book. Two editions of the book were published, one a hard-cover, the other a reinforced cloth-bound edition. The contents include stills from the film linked with a combination of narration and extracts from the script, as well as a preface written by Richard Adams and a foreword written by Martin Rosen. 

===Home releases===
* Watership Down (region 1, USA, currently out of print) (2002)
* Watership Down 25th Anniversary Edition (region 4, Australia) (2003) (Big Sky Video)
* Watership Down (Australia, 2005, Umbrella Entertainment)
* Watership Down Deluxe Edition (region 1, USA) (7 October 2008)
* Watership Down The Criterion Collection (region 1, USA) (24 February 2015) 
Watership Down was originally scheduled to be released on   presentation. 

===International distribution===
;Theatrical
* UK: Cinema International Corporation Filmways Australasian Company
* US:  
* Netherlands: Concorde Film
* Finland: Suomi-Filmi

;Video/DVD Video Collection International (1986)/Guild Home Video (1987)/PolyGram Video (1990s)/Warner Home Video (2000s)/Universal Pictures (2013–present)
* USA/internationally: Warner Home Video (1983–present)
* Finland: Finn Innovation Products (1995) / Future Film Ltd (2005)
* Australia: CIC Video (1980s-1999)/Roadshow Home Video (1999–2000)/Blue Sky Video (2005)
The film has also been dubbed in Japanese language|Japanese.  

===TV series===
Almost twenty years later a TV series with the same title Watership Down (TV series)|Watership Down was created. It has thirty-nine episodes and was loosely based on the events of the film, albeit the storyline is more child-friendly. However, the third season does take a slightly darker turn than the first two seasons did.

===Reboot===
 the book of the same name is in early productions.  

==References==
 

==External links==
*  
*   
*  
*  
*  
*  
*   by Gerard Jones

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 