Widow's Perverted Hell
{{Infobox film
| name = Widows Perverted Hell
| image = Widows Perverted Hell.jpg
| image_size = 210px
| caption = Theatrical poster for Widows Perverted Hell (1991)
| director = Hisayasu Satō 
| producer = 
| writer = Kyōko Godai
| narrator = 
| starring = Yuri Hime Shōichiro Sakata Kiyomi Itō Yutaka Ikejima
| music = 
| cinematography = Masashi Inayoshi
| editing = Shōji Sakai
| studio = Shishi Productions
| distributor = Xces Films
| released = April 26, 1991
| runtime = 61 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1991 Japanese pink film directed by Hisayasu Satō. It won the Best Film 6th place award at the Pink Grand Prix.    

==Synopsis==
A married couple enjoy engaging in S&M sexual behavior, and the wife, Yuri, asserts, "The ultimate SM play is death." After her husband dies, supposedly accidentally, Yuri is left alone, grieving and self-destructive. Her mental state deteriorates as she watches and masturbates joylessly to the home-made SM videos she made with her husband. In the films climax, Yuri goes to a public square, nude and bound, and pleads with passing strangers to aid her masturbation.    

==Cast==
* Yuri Hime ( )   
* Shōichiro Sakata ( )
* Kiyomi Itō
* Yutaka Ikejima
* Aya Midorikawa ( )
* Kōichi Imaizumi ( )

==Release and critical appraisal==
 
Hisayasu Satō filmed Widows Perverted Hell from a script by Kyōko Godai for Kan Mukais Shishi Productions and it was released theatrically in Japan by Xces Films on April 26, 1991.  At the fourth annual Pink Grand Prix, the Japanese pink film community chose it as the sixth best pink film release of the year.  On April 9, 2010 it was re-released as  .      

Allmovie judges most of Widows Perverted Hell to be a "rather lackluster", "routine" film from Satō, which is intended as a "dark meditation on the unhinging effects of grief". However, according to the review, the final sequence makes the film "worth seeing". "Like Divine (actor)|Divines memorable strut through the streets of Baltimore in Pink Flamingos, this scene was shot guerrilla-style, with no warning, and some of the reactions from unsuspecting pedestrians are absolutely priceless."    In their Japanese Cinema Encyclopedia: The Sex Films, the Weissers give the film two-and-a-half out of four stars, writing that this is " nother trenchant example of Satos guerilla shooting". 

==Bibliography==

===English===
*  
*  
*  
*  

===Japanese===
*  
*  
*  

==Notes==
 

 
 
 
 
 

 
 

 
 
 
 
 


 
 