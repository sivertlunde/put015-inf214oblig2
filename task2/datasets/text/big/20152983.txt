The Count of Monte Cristo (1929 film)
{{Infobox film
| name           = Monte Cristo
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Henri Fescourt
| producer       = Louis Nalpas
| writer         = Alexandre Dumas, père (novel) Henri Fescourt (screenplay)
| narrator       = 
| starring       = 
| music          = Marc-Olivier Dupin (2006 Arte TV restoration)
| cinematography = 
| editing        = 
| studio         = Films Louis Nalpas
| distributor    = Terra - United Artists
| released       = 25 October 1929 (Part 1) 1 November 1929 (Part 2)
| runtime        = 218 minutes
| country        = France Silent (French French intertitles)
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 
 French silent film directed by Henri Fescourt, and is a film adaptation of The Count of Monte Cristo, a novel by Alexandre Dumas, père. 

==See also==
*1929 in film

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 


 