Revelation (1924 film)
{{Infobox film
| name           = Revelation
| image          =
| caption        =
| director       = George D. Baker
| producer       =
| writer         = George D. Baker, based on a Mabel Wagnalls story
| starring       = Viola Dana Monte Blue Lew Cody
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn
| released       =  
| runtime        =
| country        = United States English intertitles
| budget         =
}} 1924 film starring Viola Dana, Monte Blue and Lew Cody. The film was directed and written by George D. Baker, based upon a popular novel, The Rosebud of a Thousand Years. Ms. Viola Dana was one of the top stars of the newly amalgamated MGM, a lively comedienne who enjoyed a long career that faded with the emergence of the talkies.  In 1918 Metro Pictures (current MGM) filmed Revelation and starring Alla Nazimova, directed by George D. Baker.

==Plot==
Joline Hofer (Viola Dana) is a profligate Montmartre dancer who left her illegitimate child in a convent. Paul Granville (Monte Blue) is an American artist who becomes smitten by the dancer, and uses her for his portraits of great women. When one of Pauls paintings, of the Madonna, appears to result in a miracle, Jolines life is changed forever, as she reforms, reclaims her child and marry the artist.

==Cast==
* Viola Dana - Joline Hofer
* Monte Blue - Paul Granville Marjorie Daw - Mademoiselle Brevoort
* Lew Cody - Count Adrian de Roche
* Frank Currier - Prior
* Edward Connelly - Augustin
* Kathleen Key - Madonna
* Ethel Wales - Madame Hofer
* George Siegmann - Hofer
* Otto Matieson - Du Clos
* Bruce Guerin - Jean Hofer

== Citations ==
 

==External links==
* 

 
 
 
 
 
 
 
 


 