Striker (2010 film)
{{Infobox film
| name           = Striker
| image          = Striker poster.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Chandan Arora
| producer       = Chandan Arora
| writer         =  
| starring       =  
| music          = 
| cinematography = P. S. Vinod
| editing        = Sajit Unnikrishnan
| studio         =  
| distributor    = Studio 18
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
Striker is a 2010 Bollywood action-drama film written, directed and produced by Chandan Arora.  It stars Siddharth Narayan|Siddharth, Vidya Malvade, Padmapriya Janakiraman|Padmapriya, Nicolette Bird, Anupam Kher, Seema Biswas and Aditya Pancholi. The film had a theatrical release in cinemas throughout India on 5 February 2010. It also premiered on YouTube the same day, thus becoming the first ever Indian film to premiere on YouTube internationally on the same day as its domestic theatrical release. 

== Plot ==
Born into a poor family, Surya grows up with few luxuries. Poor health keeps him away from school often and that is when his elder brother, Chandrakant, introduces him to carrom. Winning the Junior Carrom Championship at 12 is not enough to keep Suryas fire for the sport burning through adolescence. Hopes for a job in Dubai replace the passion for carrom as Surya grows into a young man.  Duped by a bogus overseas employment agency, Surya loses all his hard earned money he had saved for going to Dubai. Surya is forced to cross paths with Jaleel.

Since the 70s when the settlements in the ghetto began, Jaleel had acquired a strong hold in the area. He had his hands in every illicit activity since then. Feeding on the weaknesses of people, Jaleel was the self-proclaimed king of Malwani. Reintroduced to carrom by his childhood friend Zaid, this time to the carrom hustling scene, Surya starts playing again. Being gypped of his hard earned money by the same man who had caused misery for many families; Surya decides to take on Jaleel on his turf. His patience and cool attitude are Suryas biggest strengths. But life has its own ways of testing it.

== Cast == Siddharth as Suryakant Sarang
* Usha Jadhav as Rajni Padmapriya as Madhu
* Aditya Pancholi as Jaleel
* Ankur Vikal as Zaid
* Anupam Kher as Inspector Farooque
* Seema Biswas as Suryas Mother
* Vidya Malvade as Devi Sarang
* Anoop Soni as Chandrakant Sarang
* Nicolette Bird as Noorie
* Bhavya Gandhi as young Suryakant

==Box office==
The film fared very poorly at the Indian box office. Though it was given only a one week theatrical run, it garnered critical acclaim. However, being the first film in history to premiere the same day on YouTube, the film garnered over 800,000 views in the first 2 weeks. Estimated profits from this are said to be over USD 1mn.

==Soundtrack==
{{Infobox album
| Name = Striker
| Type = Soundtrack
| Artist = Various Artists
| Cover =
| Released = 12 January 2010
| Recorded = 2009
| Genre = Film soundtrack
| Length = 39:95
| Label = T-Series Siddharth
| Reviews =
| Last album =
| This album =
| Next album =
}}
 Jeetendra Joshi, Swanand Kirkire and Gulzar (lyricist)|Gulzar. Siddharth himself is the album producer and has lent his voice for two of the songs. The album was released on 12 January 2010.

{{tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    = 39:35
| lyrics_credits  = yes
| music_credits   = yes
| title1          = Aim Lagaa
| lyrics1         = Nitin Raikwar & Blaaze
| music1          = Blaaze
| extra1          = Blaaze
| length1         = 3:07
| title2          = Aim Lagaa - Ricksha Mix
| lyrics2         = Nitin Raikwar & Blaaze
| music2          = Blaaze
| extra2          = Blaaze
| length2         = 3:19
| title3          = Bombay Bombay
| lyrics3         = Prashant Ingole
| music3          = Amit Trivedi Siddharth
| length3         = 5:07
| title4          = Cham Cham
| lyrics4         = Jeetendra Joshi
| music4          = Shailendra Barve
| extra4          = Sonu Nigam
| length4         = 7:12
| title5          = Haq Se
| lyrics5         = Nitin Raikwar
| music5          = Yuvan Shankar Raja
| extra5          = Siddharth & Yuvan Shankar Raja
| length5         = 5:43
| title6          = Maula
| lyrics6         = Swanand Kirkire
| music6          = Swanand Kirkire
| extra6          = Swanand Kirkire
| length6         = 4:52
| title7          = Pia Saanvara
| lyrics7         = Jeetendra Joshi
| music7          = Shailendra Barve
| extra7          = Sunidhi Chauhan
| length7         = 5:05
| title8          = Yun Hua Gulzar
| music8          = Vishal Bhardwaj
| extra8          = Vishal Bhardwaj
| length8         = 5:10
}}

==References==
 

==External links==
*  
*   at Sulekha

 
 
 
 
 
 