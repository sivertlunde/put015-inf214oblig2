Cannibal Apocalypse
{{Infobox film
| name           = Cannibal Apocalypse
| image          = Cannibal Apocalypse.jpg
| image_size     = 
| caption        = Theatrical poster to Cannibal Apocalypse (Apocalypse domani, 1980)
| director       = Antonio Margheriti
| producer       = Maurizio Amati, Sandro Amati 
| writer         = Antonio Margheriti, Dardano Sacchetti	
| narrator       =  John Saxon John Morghen
| music          = Alexander Blonksteiner
| cinematography = Fernando Arribas
| editing        = Giorgio Serrallonga
| distributor    = 
| released       = 
| runtime        = 
| country        = 
| language       = Italian
| budget         = $75,000
| gross          = N/A
| preceded_by    = 
| followed_by    = 
}} 1980 zombie John Saxon. {{cite web | url=
http://movies.nytimes.com/movie/25294/Cannibals-in-the-Streets/overview?scp=1&sq=Cannibal%20Apocalypse%20&st=cse | title=Cannibal Apocalypse | author=New York Times | publisher=New York Times | accessdate=2011-04-26}} 

==Plot== craving for human flesh. In Atlanta, Georgia, some years later, Hopper wakes up from a nightmare about this incident, and then receives a phone call from Bukowski, who invites his old comrade out for a drink. The call came in at an inopportune moment, as a young neighbour girl, Mary (Cindy Hamilton), was trying to seduce him, so he turns down the invitation. Hopper falls for her charms and as they are about to engage in oral sex, he bites her. After hearing from his concerned wife that a Vietnam vet barricaded himself in a mall, he gets into his car. Just as he is about to leave, Mary reveals that she enjoyed the bite. After arriving on the scene, he convinces Bukowski to surrender to the police. While being hauled away to the hospital, Bukowski bites a police constable. 

After returning home, Hopper instructs his wife to wait for him in the house, and walks in front of Marys window. Later, he admits to the incident and his wish to bite a fellow-human, to his wife. At the hospital, Bukowski and fellow vet Tommy (Tony King) get into a fight with the guards, and Bukowski bites the leg of nurse Helen (May Heatherly). Dr Mendez (Ray Williams) calls Jane Hopper and tells her that Norman might be experiencing the same symptoms and that she should bring him over to the hospital for a checkup, while he is listening in on the conversation, after which he leaves the house.

The police get the coroners report on the mall shootout and it contains warnings of cannibalism. At the same time, Jane meets Dr. Mendez, who has been trying to seduce her before. They are at a fancy piano bar, where he explains to her that the virus causes a biological mutation, due to psychic alteration. Norman voluntarily goes to the hospital and talks to a doctor about his symptoms and his well-founded suspicions on Dr. Mendez intentions with his wife. The doctor also takes a blood sample.

At the police station, the bitten constable shoots a fellow cop, bites another cop and ends up shot by Captain McCoy (Wallace Wilkinson). Norman comes across his sedated fellow vets Charlie and Tommy. He experiences a flashback to the initial incident where he got bit by an infected POW. The doctor gets attacked by Helen while inspecting Hoppers blood sample. First she bites off his tongue and then she bashes his head in with a rock. In an ambulance the bitten cop gets aggressive before she dies. In the hospital, Helen frees Tommy and Charlie. Norman kills a staff member who tries to notify the authorities.

Norman, Helen, Tom and Charlie leave the hospital in an ambulance. Jane returns to an empty house and searches for Norman when the phone rings and Dr. Mendez notifies her that Norman disappeared. The police are on high alert and looking for the escapees. The escapees break into a tire shop and find a gun. Charlie uses an Angle grinder to slice up a victim. Tommy steals a station wagon and the infected leave with a pistol and a bag of snacks.

They get confronted by a biker gang that Charlie already fought at the mall, killing one of their members. After defeating the gang, they run away on foot just as the police arrive on the scene and escape into the sewer. The police coordinate a blockade of the sewer exits in order to trap the suspects. At the Hopper residence, Jane is unable to make a telephone call and goes next door to ask Marys aunt to use the phone. Mary and her brother greet her and let her use the phone to call Dr. Mendez, but the aunt is nowhere to be seen. The children behave suspiciously and ask her how come her phone is not working, but they let her leave.

In the sewer, Helen gets attacked by a rat just as the cannibals are about to slip away from the police. She ends up being shot along with a cop. Charlie is killed by the police, while Norman is wounded. Tom goes berserk, attacks the police and is killed with a Flamethrower. Even with a wounded leg, Norman crawls out of the sewer, steals a car and leaves the scene.

Jane tries to leave the house in her car but cannot start it. As she comes back into the house, she hears strange noises and calls for Norman. She finds him in his dress uniform, telling her to stay away. Norman is hurt and dying, and Dr. Mendez walks into the house. Jane runs over to him, but he bites her as Norman shoots him. Jane points his gun at her own head and two shots are heard just as the police arrive on the scene.

The infected children watch as the bodies are being hauled away. The boy asks Mary if they will be looking for their aunt. Her hand is seen inside the fridge.

==Production==
  and Wes Cravens New Nightmare, was cast due to his initial interest in the unusual story twist. Unaware it was an Italian exploitation movie on cannibalism, he soon lost interest when made aware but had already signed and committed to the picture. He has claimed never to have watched the movie and was fine with its banning in the UK, claiming it was foul and in bad taste. It is also claimed that Saxons subdued performance in the picture was due to his recent divorce and financial strain thereof.
 Video Nasties" list, and the distributors were successfully prosecuted for obscenity. The film was finally re-released in the UK in 2005, but cut out a brief 2 second shot during the sewer shootout in which some a rat gets splattered with burning napalm, as the BBFC felt this was in breach of the Cinematograph Films (Animals) Act 1937.

Quentin Tarantino, himself an avid exploitation movie fan, has claimed to have a love for this and other movies directed by Antonio Margheriti. Its funky, 70s discoesque score has generated much love, mirth or curiosity due to its bloody backdrop.

==Release==
The film was given a limited release theatrically in the United States starting in 1981. 

The film was officially released on DVD by Image Entertainment as part of the Euroshock Collection in 2002.   This version is currently out of print.

==Critical reception==
Patricia MacCormack of Senses of Cinema critiqued, "Although the film has been maligned by critics, who claim Margheriti disinherited his adeptness at the gothic by meddling in the vulgar genre of high-gore, the sympathies he evokes for perversion as at turns tragic pathology and strange alternative desire, the disdain with which he represents hyperreal examples of ‘normal’ male sexuality and the extraordinary versions of human flesh he presents for our pleasure, a pleasure which compels us into a world of perversion and desire beyond the palatable, are all continued thematically if not stylistically in this film. Margheritis use of Radice and gore brings him from the gothic worlds of Bava and Freda into a subgenre more often associated with Fulci, Umberto Lenzi and Deodato, yet he remains faithful to his perverse paradigms. 

Dean Winkelspecht of DVD Town said, "Visually speaking, the film looks quite good. The gore is not as awe-inspiring as what we see in todays films, but it is a step above what was seen a few years later in The Evil Dead. Margheretis use of Atlanta gave the film a different look than other genre entries, but at times it felt as if the director went overboard on scenery and this slowed the film down during some sequences. The fashion and look of the actors is very dated and there are no elaborate sets, but the photography and effects transcend what is typically found in a zombie/cannibal picture. Cannibal Apocalypse is first and foremost, a celebration of gore. It is however, an entertaining one." 

==References==
 

==External links==
* 
* 
*  is available for free download at the Internet Archive

 
 
 
 
 
 
 