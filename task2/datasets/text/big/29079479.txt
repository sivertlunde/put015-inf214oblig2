Take Aim
{{Infobox film
| name           =  Take Aim
| image          = 
| alt            = 
| caption        = 
| director       = Igor Talankin 
| producer       = 
| writers        = Danil Granin
| screenplay = Igor Talankin, Danil Granin
| narrator       = 
| starring       = Sergei Bondarchuk
| music          = Alfred Schnittke
| cinematography = Naum Ardashnikov
| editing        =  
| studio         =  Mosfilm
| distributor    = 
| released       = 1 March 1975
| runtime        = 158 minutes (combined)
| country        = Soviet Union  East Germany
| language       = Russian, German, English
| budget         = 
| gross          = 
}} Soviet film directed by Igor Talankin.

==Plot==
The film depicts the nuclear arms race that took place between all sides in the Second World War and the beginning of the Cold War. The first part centers on the war years, dealing with the Manhattan Project and the American effort to beat the Germans to the bomb, as well as with Stalins decision that the USSR must have its own atomic project. The second part displays the Soviet post-war nuclear program. The plot deals mainly with the personal dilemmas facing all the scientists who worked on the atomic weapons.   

==Production==
The film was produced solely by Mosfilm, without a direct participation of DEFA, and yet several East German actors were invited to play the German historical figures. Fritz Diez, who appeared as Hitler for the fifth time in his career, was given also the role of Otto Hahn.  
 Syrian citizen who worked for Mosfilm - decided to create the required sequence by trickling a drop of orange-tinted perfume into a watery solution of Aniline and filming it close up.       

==Reception==
The film won the 1975 Kishinev All-Union Film Festival Grand Prize. Talankin received the Silver Pyramide in the 1977 Cairo International Film Festival. 

==Main cast==
*Sergei Bondarchuk as Igor Kurchatov.
*Nikolai Volkoff as Abram Ioffe.
*Yakov Tripolski as Joseph Stalin.
*Nikolai Zasukhin as Vyacheslav Molotov.
*Mark Prudkin as Albert Einstein.
*Sergei Yursky as J. Robert Oppenheimer.
*Boris Ivanov as Leó Szilárd.
*  as Leslie Groves.
*Fritz Diez as Otto Hahn/ Adolf Hitler.
*Horst Schulze as Werner Heisenberg. Siegfried Weiß as Niels Bohr .

==References==
 

==External links==
*  on the IMDb.

 
 
 
 
 
 