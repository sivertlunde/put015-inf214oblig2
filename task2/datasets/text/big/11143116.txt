Kaakki Sattai
 
 
{{Infobox film
| name           = Kaakki Sattai
| image          = Kaakki Sattainew.jpg
| caption        =  Rajasekhar
| producer       = G. Thyagarajan V. Thamizhagan
| writer         = A. L. Narayanan Madhavi  Ambika  Rajeev Bob Christo Thengai Srinivasan
| music          = Ilaiyaraaja
| cinematography = V. Ranga
| editing        = K. R. Krishnan
| studio         = Sathya Movies
| distributor    = Sathya Movies
| released       =  
| runtime        = 143 minutes
| country        = India
| language       = Tamil
| budget         = 
| gross          =  3.7 crore
}}
 1985  Indian feature directed by Rajeev and Thengai Srinivasan. The film was panned by critics and block buster hit at the box office.

==Plot summary==

Kaakki Sattai has Murali (Kamal Haasan), a young man who trains very hard to become a police officer. Uma (Ambika (actress)|Ambika) is his neighbour and they fall in love with each other while Uma supports Murali to achieve his goal but he is rejected after the physical test.

Angered by the rejection, Murali turns into a local rowdy. He causes trouble in his district and comes to the attention of a notorious smuggler and murderer, Vicky (Sathyaraj) and his partner Anand (Rajeev (Tamil actor)|Rajeev). After initial conflict between the two, Vicky recruits rowdy Murali into his gang. Murali becomes his most trusted member. He grows intimate with a girl, Anita (Madhavi (actress)|Madhavi), who is looking for personal revenge against Anand.

But soon Vickys activities time and again, are foiled by the police and he finds out that they have a mole in his gang. Uma spots the changed Murali with Anita and becomes furious and breaks up with him. Then Murali reveals the truth to her. He was not rejected by the police but was recruited by a senior officer to bring down Vicky and Anands gang. He became a rowdy to get into the gang and is acting as a mole to bring the gang down. They patch things up but Vicky finds out the truth too. In the climax, Anita kills Anand. Vicky kidnaps Uma after killing Anita and when police arrive to arrest him and blackmails Murali to help him escape. After initially helping his escape, Murali rescues Uma and Vicky is killed in the blast.

==Cast==

*Kamal Haasan - Murali Madhavi - Anita Ambika - Uma
*Sathyaraj - Vicky Rajeev - Anand
*Thengai Srinivasan
*Bob Christo
*V. Gopala Krishnan
*Kallapetti Singaram
*Y. Vijaya
*Kanchana

==Soundtrack==
{{Infobox album Name     = Kaakki Sattai Type     = film Cover    = Kaakkisattaiaudio.jpg Released =  Artist   = Ilaiyaraaja Genre  Feature film soundtrack Length   = 24:48 Label    = 
}}
The  soundtrack was composed by Maestro Illayaraja.     

{|class="wikitable"

! #
! Track
! Singer(s)
! Lyrics
! Picturised On
|-
| 1
| Kanmaniyae 
|S. P. Balasubrahmanyam, S. Janaki Pulamaipithan
|Kamal Ambika
|-
| 2
| Nama Singaari Saraku
| S. P. Balasubrahmanyam Vaali (poet) Vaali
|Kamal Hassan & Dancers
|-
| 3
| Poo Potta Dhavani
|S. P. Balasubrahmanyam, S. Janaki Avinashi Mani Kamal Hassan, Madhavi
|-
| 4
| Vaanile Thenila
|S. P. Balasubrahmanyam, S. Janaki
|Na. Kamarasan Kamal Hassan, Ambika
|-
| 5
| Pattu Kannam
|S. P. Balasubrahmanyam, P.Susheela Muthulingham
|Kamal Hassan, Ambika
|}

==References==
 

==External links==
*  

 
 
 
 
 
 