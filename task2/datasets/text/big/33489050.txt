Kavya's Diary
{{Infobox film
| name           = Kavyas Diary
| image          = Kavyas Diary.JPG
| alt            =  
| caption        = Movie Poster of Kavyas Diary.
| director       = V. K. Prakash|V.K.Prakash
| producer       = Manjula Ghattamaneni Sanjay Swaroop
| writer         = Indira Productions Creative Unit Shashank Indrajith Indrajith Satyam Rajesh Leena
| music          = Manu Ramesan
| cinematography = Shyam Dutt
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = 
| language       = Telugu
| budget         =   4 Crores
| gross          = 
}} Telugu film The Hand That Rocks the Cradle. 

==Plot==
Raj (Indrajeet) and Pooja (Manjula Swaroop) are a married couple who move into a new house with their children. Desperate for a nanny to take care of their children, they hire Kavya who is unemployed, after she saves Poojas life. Kavya grows close to the family and they come to consider her part of the family. However, as the movie progresses, it is revealed that Kavya was the wife of a gynecologist who committed suicide after Pooja accused him of sexually assaulting her. Other patients supported this claim and he eventually committed suicide. In an attempt to save him, Kavya accidentally miscarries. She eventually tries to destroy the family and kill Pooja to get revenge for her loss. The movie is a remake of the English movie, The Hand that Rocks the Cradle.

==Characters==
*Pooja (Manjula Ghattamaneni) 
*Kavya (Charmy Kaur)
*Abhi (Shashank (actor)|Shashank)
*Raj (Indrajith Sukumaran|Indrajith)
*Athi D (Satyam Rajesh)

==References==
 

==External links==
* 
* http://www.indiaglitz.com/channels/telugu/review/10730.html
* http://www.idlebrain.com/movie/archive/mr-kavyasdiary.html

 

 
 
 