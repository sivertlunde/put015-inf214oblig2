Bits of Life
{{Infobox film
| name           = Bits of Life
| image          = Bitsoflife-newspaperadvert-1922.jpg
| image_size     =
| caption        = Newspaper advertisement.
| director       = Marshall Neilan
| producer       = Marshall Neilan
| screenplay     = Lucita Squier
| story          = 
| based on       = Short stories by Thomas McMorrow, Walter Trumbull, Hugh Wiley, Marshall Nielan 
| narrator       = Lon Chaney
*Noah Beery, Sr.
*Anna May Wong}}
| cinematography = David Kesson
| editing        = Associated First National
| released       =  
| runtime        = 60 minutes
| country        = United States Silent (English intertitles)
| budget         =
| gross          =
}} American film Lon Chaney and Noah Beery, Sr. For her performance in this film, Anna May Wong received her first screen credit.  Bits of Life is considered lost film|lost. 

==Plot==
The film is an anthology of four stories: "The Bad Samaritan", "The Man Who Heard Everything", "Chinese Story" or "Hop",  and "The Intrigue". 

==Cast==
* Wesley Barry as Tom Levitt, a boy
* Rockliffe Fellowes as Tom Levitt Lon Chaney as Chin Chow
* Noah Beery, Sr. as Hindoo
* Anna May Wong as Toy Sing, Chin Chows Wife John Bowers as Dentists Patient

==References==
{{reflist|
   
 Gan, Geraldine. "Anna May Wong". Lives of Notable Asian Americans: Arts, Entertainment, Sports (The Asian American Experience). New York: Chelsea House Publishers, 1995. ISBN 0-7910-2188-2. p. 84. 
   
   
}}

== External links ==
 
*  

 

 
 
 
 
 
 
 
 
 
 


 