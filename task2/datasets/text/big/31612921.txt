The Wedding in Monaco
{{Infobox film
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       =Jean Masson	
| producer       = Jean Masson
| writer         = 
| starring       =Grace Kelly Rainier III, Prince of Monaco
| music          =Stan Kenton Daniel White
| cinematography = 
| editing        = 
| studio         = MGM
| distributor    = 
| released       =17 May 1956
| runtime        = 31 min.
| country        =      
| language       = English French gross = $159,000  . 
}} 1956 documentary covering the celebrations in Monaco leading up to wedding of Prince Rainier III to Grace Kelly. The 31-minute color CinemaScope film was directed by Jean Masson and released by Metro-Goldwyn-Mayer, Kelly’s film studio before her retirement from acting. 
==Reception==
According to MGM records the film earned $108,000 in the US and Canada and $51,000 elsewhere resulting in a profit of $15,000. 
==See also==
* 

==References==
 

==External links==
*  
 
 
 
 
 
 

 