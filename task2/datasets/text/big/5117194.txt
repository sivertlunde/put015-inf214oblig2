Hum Kisise Kum Naheen
 
{{Infobox film
| name           = Hum Kisise Kum Naheen
| image          = humkisise.jpg
| image_size     =
| caption        = Film Poster
| director       = Nasir Hussain
| producer       = Nasir Hussain
| writer         = Sachin Bhowmick
| narrator       = Tariq Khan Amjad Khan Om Shivpuri Zeenat Aman Tom Alter
| music          = R.D. Burman
| cinematography = Munir Khan
| special effects = Rauko Effects Services 
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}}
 Action film, produced and directed by Nasir Hussain. The film became a "super hit" at the box office, and took the third top spot at the box office in 1977.  
 Tariq Khan, Amjad Khan, Zeenat Aman, Om Shivpuri, Jalal Agha and Tom Alter.

== Cast ==

* Rishi Kapoor as Rajesh / Fake Manjeet Kumar Dana 
* Kajal Kiran as Kajal Kisharina Tariq Khan as Sanjay Kumar
* Amjad Khan as Saudagar Singh
* Zeenat Aman as Sunita Kewalchand
* Om Shivpuri as Ram Kumar
* Murad as Rajeshs Father
* Tom Alter as Jack
* Vimal Ahuja as Ranjeet Kumar Dana
* Ajit Khan as Sunitas Rich Father
* Bhushan Tiwari
* Sanjeev Kumar  Special Appearance

== Plot ==
 Tariq Khan), who is completely unaware that his bicycle has 25 Crore Rupees worth of diamonds hidden in its toolbox. Saudagar Singh (Amjad Khan) is after the diamonds and according to his plans he and his partner Ranbir Kumar Dana set a trap for Rajesh telling a false story to him. The whole plot is pretty speedy after that with Rajesh pretending as Manjeet Kumar Dana is to fall in love with Kajal (Kajal Kiran) who is actually in love with her childhood love, Sanjay. There are a series of meetings between the two.A few years ago when mother-less Kajals father Kishorilal was in deep financial crisis, Sanjays father gave shelter to Kajals father, who has now become filthy rich. The promise of getting Sanjay and Kajol married is forgotten when Kishorilal Kajals father insults them and forgets the promise that was made years prior. To unfold the plot, Sanjay becomes Manjeets manager. Saudagar Singh (Amjad Khan) uses Manjeet to get the diamonds himself for the climax.

== Special Effects ==
Special effects were added by Rauko Effects Service 

== Soundtrack ==

{{Infobox album
| Name     = Hum Kisise Kum Naheen 
| Type     = Soundtrack
| Artist   = R.D. Burman
| Cover    =
| Released = 1977
| Recorded = 
| Genre    = Hindi Film Soundtrack
| Length   = 
| Label    =  Saregama-Hmv
| Producer = 
| Reviews  = 
}}

The soundtrack of the film was composed by R.D. Burman, and it included 9 original songs. The song Bachna Ae Haseeno was one of the biggest chart-busters of 1977, and was remade into a film, Yash Chopras, Bachna Ae Haseeno.

==Tracklist==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Notes
|-
| "Bachna Ae Haseeno"
| Kishore Kumar
| Picturized on Rishi Kapoor. (This song was later remixed for the title track of Bachna Ae Haseeno and picturised on Rishi Kapoors son, Ranbir Kapoor, along with Minisha Lamba, Bipasha Basu & Deepika Padukone.)
|-
| "Chand Mera Dil"
| Mohammad Rafi
|  
|-
| "Dil Kya Mehfil Hai"
| Kishore Kumar  
| 
|- 
| "Hum Kisi Se Kum Nahin"
| Mohammad Rafi & Asha Bhosle
| This song is a Qawwali. Picturised on Rishi Kapoor and Zeenat Aman.
|-
| "Hum Ko To Yaara Teri Yaari"
| Kishore Kumar & Asha Bhosle 
|  
|-
| "Kya Hua Tera Wada"
|  Mohammad Rafi & Sushma Shreshta Tariq Khan. National Award.
|-
| "Mil Gaya Humko Saathi"
| Asha Bhosle & Kishore Kumar  
|
|-
| "Tum Kya Jano Mohabbat"
| R.D. Burman
|
|-
| "Yeh Ladka Hai Allah" 
| Asha Bhosle & Mohammad Rafi 
|
|}

== Awards And Nominations ==
 National Film Awards - 1977 
* National Film Award for Best Male Playback Singer - Mohammad Rafi
;Filmfare Awards - 1977 
;;Wins
* Filmfare Award for Best Male Playback Singer - Mohammad Rafi - "Kya Hua Tera Wada"
* Filmfare Award for Best Cinematographer - Munir Khan
* Filmfare Award for Best Art Direction - Shanti Dass
;;Nominations
* Filmfare Award for Best Music Director - R.D. Burman
* Filmfare Award for Best Female Playback Singer - Sushma Shreshta - "Kya Hua Tera Wada"
* Filmfare Award for Best Supporting Actor - Tariq 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 