Troma's War
 
{{Infobox film
| name           = Tromas War
| image          = Tromas war poster.jpg
| image size     = 
| alt            = 
| caption        = Tromas War movie poster Michael Herz Lloyd Kaufman
| producer       = Michael Herz Lloyd Kaufman
| writer         = Lloyd Kaufman
| narrator       = 
| starring       = Carolyn Beauchamp Sean Bowen Rick Washburn Patrick Weathers Jessica Dublin Eugene Ara Romanoff
| music          = Christopher De Marco
| cinematography = James Lebovitz
| editing        = Brian Sternkopf
| studio         = 
| distributor    = Troma Entertainment
| released       =  
| runtime        = 87 minutes
| country        = United States Spanish
| budget         = $3,000,000
| gross          = 
| preceded by    = 
| followed by    = 
}} Michael Herz and Lloyd Kaufman (credited as Samuel Weil). It began production in 1986 and was released in theaters in 1988 shortly after Class of Nuke Em High was done making its rounds at the box office.

== Plot synopsis == Nam vet, manages to kill one of the attackers, he and the fellow passenger, everyman Taylor, lead the other survivors to a safe haven, only to have part of their number captured. Brought to a training base, the captured are forced to watch a neo-Nazi horrifically murder an air steward and a priest. He remaining passengers gather what weapons they can find and make a rough attempt at storming the camp in hopes of recovering their lost friends. 
At the camp, a musicians girlfriend is shot and killed by an obese terrorist wielding an AK-47 assault rifle. The bandleader, Sean, attacks and loses a fight with the Nazi, and the rockers female bandmate is taken away to be attacked and raped by "Señor Sida" (Mr. AIDS), leader of the AIDS brigade who hope to begin an US AIDS epidemic.

Meanwhile, an attractive African American lady is taken to the brains of the operation, a pair of twins conjoined at the head. Parker and Taylor lead the remaining passengers in a surprise attack on the terrorists hideout. Señor Sida is dispatched by his victim via a crossbow bolt in the scrotum.  Having been transformed by her experience, the African American lady hunts down the Twins and violently hacks them apart with a machete.  She then goes after a terrorist who tried to have his way with her and stuffs a grenade in his mouth.

Realising the terrorists are prepared to invade America, the remaining survivors are motivated to take steps in attacking the terrorists main headquarters the next morning. Later that night, the formerly hostile Taylor and Lydia share an intimate moment. The men prepare to storm the camp but refuse to let the women go with them so they dont put their lives at risk. Hardwick, a depressed widower, volunteers to drive a truck full of explosives into a cargo boat hauling the terrorists into the US.  Hardwick however is shot down, ruining their plan.  The Americans do their best,  eliminating mass numbers of the enemy.  Running out of ammo the group awaits their last moments when the females ride in and help kill off the terrorists. A timid, morbidly obese passenger named Cooney musters his courage and decides to fill Hardwicks role. He fights his way through the terrorists blocking his way until he gets to the truck. Just as the boat is taking off, Cooney drives the truck off a ramp into the boat, destroying it. The unlikely heroes mourn the loss of Cooney, until he appears unharmed, having ditched the truck as at the last minute.

== Production == The Toxic Avenger and Class of Nuke Em High, Troma began production on what was intended as a criticism of President Ronald Reagans attempt to glamorize armed conflict. The film was initially rejected by the MPAA as too violent to even receive an R rating, so several scenes were cut from to the film, including the entire AIDS subplot. Despite this the film was rejected a second time leading to even heavier cuts. The butchered end result of the film was poorly received. In the aftermath of the films poor financial performance, Troma experienced financial hardship, jettisoning the company from the Hollywood mainstream.

Tromas Wars budget was $3,000,000, making it Tromas most expensive film to date.

==External links==
*  

 

 
 
 
 
 
 
 
 