The Wine of Summer
{{Infobox film
| name           = The Wine of Summer
| image          = The_Wine_of_Summer_Poster.jpg
| caption        = Official poster
| director       = Maria Matteoli Sean Walsh
| writer         = Maria Matteoli
| starring       = Elsa Pataky Sonia Braga Ethan Peck Najwa Nimri Bob Wells Marcia Gay Harden
| music          = Andrew Collberg Mario Matteoli Nicholas Dominic Talvola
| cinematography = Andrew Rydzewski
| editing        = Malcolm Desoto Maria Matteoli
| studio         = Public Media Works
| distributor    = 
| released       = 
| runtime        = 
| country        = United States
| language       = English Spanish
| budget         = 
| gross          = 
}} romantic drama film written, directed and produced by Maria Matteoli, starring Elsa Pataky, Sonia Braga, Ethan Peck, Najwa Nimri, Bob Wells and Marcia Gay Harden.

==Plot==
James (Peck), at the age of 27 quits his law career in pursuit of his childhood dream of becoming an actor. While studying acting under the tutelage of Shelley (Harden), he becomes engrossed in Carlo Lucchesis play, Tinto de Verano, which is set in Spain. James’ girlfriend Brit (Chow) leaves him, and he spontaneously flies to Spain, where he encounters the playwright Lucchesi (Wells) at a bookstore in Barcelona. Lucchesi is in a relationship with a much younger woman, Veronica (Pataky), but still nurtures an old love for his long lost muse, Eliza (Braga), a novelist, who happens to be visiting her son, Nico (Talvola) a trumpet player who also lives in Barcelona. In the golden backdrop of Spain, these characters find their fates intertwined. 

==Cast==
*Elsa Pataky as Veronica
*Sonia Braga as Eliza
*Ethan Peck as James
*Najwa Nimri as Ana
*Bob Wells as Carlo
*Marcia Gay Harden as Shelley
*Kelsey Chow as Brit
*Jonathan D. Mellor as Henry
*Mimi Gianopulos as Nina
*Michael Scott Allen as Michael
*Dominic Allburn as Roberto
*Francesc Prat as Frankie
*Afrika Bibang as Serafina
*Nicholas Dominic Talvola as Nico
*Andrea L. Hart as Nicole

==Production==
===Filming===
Filming started in October 2011 and was shot on location in Barcelona, Spain and Eureka, California|Eureka, California.    

===Release===
The film was shown at the 2013 Douro Film Harvest in Portugal. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 