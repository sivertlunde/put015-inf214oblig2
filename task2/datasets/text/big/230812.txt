Private Pluto
{{Infobox Hollywood cartoon cartoon name=Private Pluto
|series=
|image=
|caption=
|director=Clyde Geronimi
|producer= Walt Disney story artist= voice actor=Pinto Colvig, Dessie Flynn, Jimmy MacDonald
|musician=
|animator= layout artist= background artist=
|studio=The Walt Disney Company
|distributor= RKO Radio Pictures release date=  color process=Technicolor
|runtime=7 minutes
|country=United States
|language=English preceded by=Pluto and the Armadillo followed by=First Aiders
}} Pluto is in the army and he gets antagonized by two chipmunks. These two chipmunks would later be known as Chip & Dale. It is the first appearance of Chip & Dale in Disney history.

==Synopsis==
This short showcases Pluto as a soldier. Pluto is a guard dog on a military base. Hes told there are saboteurs, and is assigned to guard a pill-box (gun emplacement). First, Pluto tries to follow marching orders, contorting himself into quite a mess. Then, he engages in hijinks with two chipmunks who are using a cannon to store and crack their nuts, and a war of wits naturally ensues.

==Voice cast== Pluto
*Dessie Flynn as chipmunk (Dale) James MacDonald as chipmunk (Chip)

==References==
# 

==External links==
* 

 
 
 
 
 

 