The Understudy (film)
 
 
{{Infobox Film name = The Understudy image =  caption = Theatrical release poster director = Hannah Davis writer = David Conolly Hannah Davis starring = Scott Cohen Reiko Aylesworth David Melville Gloria Reuben music = Carl Davis studio =Mansion Pictures released =  runtime = country = United Kingdom language = English
}}
 Hannah Davis and is their second feature-length film through their Mansion Pictures flagship, which is currently traveling the Film Festival circuit around the world.  

==Plot==

Rebecca is an unemployed actress who is living with an equally unsuccessful screenwriter, Sarfras. Rebecca makes a living by caring for a blind and diabetic woman while going from one disastrous relationship to the next.

Rebecca is invited to understudy the famous movie star Simone Harwin (from the Drive By trilogy), in the play Electra. Although Rebeccas unfulfillment is compounded: despite outshining the Action Star with her own talent, Rebecca is treated as second class, either bullied or ignored by the cast and crew including the director Ian, the stage manager Alison. Her salvation lies within the relationship with the seemingly perfect Firefighter Bobby.

Accidents start to disrupt the leading ladies of Electra and Rebeccas star begins to rise, suspicion surrounds her. Can Rebecca hold on to the leading role and her freedom?

==Cast==
*Marin Ireland as Rebecca
*Paul Sparks as Bobby
*Aasif Mandvi as Sarfras
*Richard Kind as Ian
*Tom Wopat as Detective Jones
*Gloria Reuben as Greta
*Reiko Aylesworth as Kinsky
*Kerry Bishé as April
*Jean Boht as Mrs. Davido

==References==
 

==External links==
*  
*  

 
 
 
 
 


 