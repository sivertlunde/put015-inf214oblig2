Alexandria Again and Forever
 
{{Infobox film
| name = Alexandrie encore et toujours aka Alexandria again and forever
| image = Alexandrie Encore Et Toujours.jpg
| image_size = 
| caption =  
| director = Youssef Chahine 
| producer = French Ministry of Culture, La Sept, Ministere des Affaires Etrangeres, Misr International Films, Pa
| writer = 
| narrator = 
| starring = 
| music = 
| release = 
| source = Msir International Films
| cinematography = 
| editing = 
| released = 1989
| runtime = 106 minutes
| country = Egypt
| language = Arabic,English sub titles
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
| distributor =  /M-Net
}} 
Alexandria again and forever ( . This 104-minute-long film is mainly in Arabic, with English-language subtitles. It was preceded by the films Alexandra...Why? (1978) and An Egyptian Story (1982). 

Following a violent outburst with Amr, his favorite actor, the director Yehia Eskandarany (Youssef Chahine) is forced to scrutinize his entire career under scrutiny. Nothing looks familiar any longer. His country and African cinema he loves so much, also looks strange. Little by little, while remembering his first film with Amr, Yehia traces the infiltration of the influential petro-dollar into the Egyptian cinema industry. But was it really money that caused the rift between him and Amr? Or was it the strikingly beautiful Nadia?

==References==
{{Reflist|refs=
 {{Citation
 | last1     = Pratt
 | first1    = Douglas
 | title     = Doug Pratts: Movies, Television, Music, Art, Adult and More!
 | volume    = 1
 | publisher = UNET 2 Corporation
 | pages      = 39–40
 | year      = 2004
 | isbn      = 1932916008
 | url       = http://books.google.com/books?id=DTUw1SDQECoC&pg=PA39
 | postscript= .
}} 
}}

==External links==
*  
*  

 
 
 
 

 