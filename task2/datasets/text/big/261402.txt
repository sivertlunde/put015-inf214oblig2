Robin Hood: Prince of Thieves
 
 
 
{{Infobox film
| name           = Robin Hood: Prince of Thieves| image = Robin hood 1991.jpg
| caption        = Theatrical release poster
| alt            = A bowman, ready to release a fiery arrow. Below two figures, beside a tree, silhouetted against a lake background.  Kevin Reynolds
| producer       = Pen Densham Richard Barton Lewis John Watson
| screenplay     = Pen Densham John Watson
| story          = Pen Densham
| starring       = Kevin Costner Morgan Freeman Christian Slater Alan Rickman Mary Elizabeth Mastrantonio
| music          = Michael Kamen
| cinematography = Douglas Milsome Peter Boyle Morgan Creek 
| distributor    = Warner Bros.
| released       =  
| runtime        = 155 minutes
| country        = United States United Kingdom
| language       = English
| budget         = $48 million 
| gross          = $390,493,908
}}
 Kevin Reynolds. Maid Marian of Dubois, and Alan Rickman as the Sheriff of Nottingham.
 second highest grossing film of 1991. Rickman received the BAFTA Award for Best Actor in a Supporting Role, while the films theme song, "(Everything I Do) I Do It for You", by Bryan Adams, was nominated for the Academy Award for Best Song.

==Plot== Robin of Locksley (Costner), Richard the Moor named Azeem (Freeman) in the process, but Peter dies while escaping and has Robin swear to protect his sister Marian (Mastrantonio). Robin returns to England with Azeem, who has vowed to accompany him until the debt of saving his life is repaid.
 witch Mortianna Bishop of Hereford (Harold Innocent). At Locksley Castle, Robins father (Brian Blessed) is killed by the Sheriffs men after refusing to join them.

Robin returns to England to find his father dead, his home in ruins, and the Sheriff and his men oppressing the people. While fleeing the Sheriffs forces, Robin and Azeem encounter a band of outlaws hiding in Sherwood Forest, led by Little John (Nick Brimble). Among the band is Will Scarlet (Slater), who holds a belligerent grudge against Robin and does not hesitate to show Robin his true feelings. Robin ultimately assumes command of the group, encourages his men to fight against Nottingham, and trains them to defend themselves. They rob soldiers and convoys that pass through the forest, then distribute the stolen wealth among the poor. One of their early targets is Friar Tuck (Mike McShane), who subsequently joins these Merry Men, and Marian also begins to sympathize with the band and renders Robin any aid she can muster. Robin’s successes infuriate the Sheriff, who increases the maltreatment of the people, resulting in more support for Robin Hood.
 Celtic warriors from Scotland to bolster his forces, the Sheriff manages to locate the outlaws hideout and launches an attack, destroying the forest refuge. He confines Marian when she tries to summon help from France. In order to consolidate his claim to the throne, the Sheriff proposes to Marian (who is Richards cousin), claiming that if she accepts, he will spare the lives of the captured outlaws. Nevertheless, several of the rebels are due to be executed by hanging as part of the wedding celebration. Among the captured is Will Scarlet, who seemingly makes a deal with the Sheriff to find and kill Robin in order to be set free.
 illegitimate half-brother; Wills mother was a peasant woman with whom Robins father took comfort after Robins mother had died. Robins anger toward his father caused him to separate from her and leave Will fatherless. Despite Robins anger toward his father for being with another woman, he is overjoyed to learn that he has a brother and embraces Will.

On the day of the wedding and hangings, Robin and his men infiltrate Nottingham Castle, freeing the prisoners. Although Robins band originally planned to free their friends and retreat, Azeem reveals himself and his willingness to fight the Sheriff, turning the peasants to revolt. After a fierce fight, Robin kills the Sheriff but is attacked by Mortianna, who charges with a spear. Azeem slays Mortianna, fulfilling his vow to repay his life debt to Robin. Tuck also kills the Bishop, burdening him with treasure and throwing him out a window.

Robin and Marian profess their love for each other and marry in the forest. Their wedding is briefly interrupted by the return of King Richard (Sean Connery), who blesses the marriage and thanks Robin for his deeds.

==Cast==
* Kevin Costner as Robin Hood (Robin of Locksley) 
* Morgan Freeman as Azeem
* Christian Slater as Will Scarlet Marian Dubois
* Alan Rickman as George, Sheriff of Nottingham 
* Geraldine McEwan as Mortianna
* Mike McShane as Friar Tuck
* Brian Blessed as Lord Locksley
* Michael Wincott as Guy of Gisborne
* Nick Brimble as Little John
* Harold Innocent as the Bishop of Hereford
* Walter Sparrow as Duncan Daniel Newman as Wulf King Richard
* Jack Wild as Much the millers son
* Daniel Peacock as Bull

==Production==
 . Today known as the "Robin Hood Tree". ]]
Principal exteriors were shot on location in the   in Buckinghamshire was used for the outlaws encampment, Aysgarth Falls in Yorkshire for the fight scene between Robin and Little John, and Hardraw Force in North Yorkshire was the location where Marian sees Robin bathing.  Sycamore Gap on Hadrians Wall was used for the scene when Robin first confronts the Sheriffs men.   Chalk cliffs at Seven Sisters, Sussex were used as the locale for Robins return to England from the Crusades. 
	
Interior scenes were completed at Shepperton Studios in Surrey, England.   

On the DVD commentary for ITVs   assassin character named Nasir in the film. That character was a creation of Carpenter and is exclusive to the Robin of Sherwood series. Once the creators of Robin Hood: Prince of Thieves realized there was potential copyright infringement, they changed the characters name from Nasir to Azeem. Carpenter also explains that Costner and others involved in the film have admitted to watching Robin of Sherwood as inspiration for their film.  

==Soundtrack==
 
The original music score was composed, orchestrated and conducted by Michael Kamen.

;Track listing
{{Infobox album
| Name = Robin Hood: Prince of Thieves (Original Soundtrack)
| Type = Film score
| Artist = Michael Kamen
| Cover = 
| Released = July 2, 1991
| Length = 60:22
| Label = Morgan Creek Productions
| Reviews =
}}
  
# "Overture/A Prisoner of the Crusades" (8:27)
# "Sir Guy of Gisborne/The Escape to Sherwood" (7:27)
# "Little John/The Band in the Forest" (4:52)
# "The Sheriff and His Witch" (6:03)
# "Maid Marian" (2:57)
# "Training/Robin Hood, Prince of Thieves" (5:15)
# "Marian at the Waterfall" (5:34)
# "The Abduction/The Final Battle at the Gallows" (9:53)
# "(Everything I Do) I Do It for You" - Bryan Adams (6:33)
# "Wild Times" – Jeff Lynne (3:12)

==Reception==
  
Critics gave the film mixed reviews.   gave the film a negative review, with Vincent Canby writing that the movie is "a mess, a big, long, joyless reconstruction of the Robin Hood legend that comes out firmly for civil rights, feminism, religious freedom and economic opportunity for all."  The Los Angeles Times found the movie unsatisfactory as well.  Costner was criticised for not attempting an English accent.  Review aggregation website Rotten Tomatoes, retrospectively collected reviews from 52 critics to give the film a score of 50%. 
 Worst Supporting Nothing but Trouble. 

===Box office===
Despite mixed critical reception, the movie was a box office hit. It made $25 million gross in its opening weekend and $18.3 million in its second weekend. The film would eventually make $390,493,908 at the global box office, making it the second-highest grossing film of 1991, immediately behind  . It enjoyed the second-best opening for a non-sequel, at the time.    

== Adaptations ==
 
A tie-in   was released in 1991 for the Nintendo Entertainment System and Game Boy.  Developed by Sculptured Software Inc. and Bits Studios, respectively, and published by Virgin Games, Inc., it was featured as the cover game for the July 1991 issue of Nintendo Power magazine.

A toy line was released by Kenner, consisting of action figures and playsets.  Notably, all but one of the figures were slightly modified from Kenners well known Super Powers line, while Friar Tuck, as well as the vehicles and playset, were modified from Star Wars: Return of the Jedi toys.

==See also==
 
* Princess of Thieves
*  
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  , a 1991 Entertainment Weekly cover story about the films tumultuous production.
*  
*   Filming locations at Movieloci.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 