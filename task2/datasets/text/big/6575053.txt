DeepStar Six
{{Infobox Film
| name           = DeepStar Six
| image          = DeepStarSix Poster-1-.jpg
| image_size     = 
| caption        = 
| director       = Sean S. Cunningham
| producer       = Sean S. Cunningham Patrick Markey
| screenplay     = Lewis Abernathy  Geof Miller
| story          = Lewis Abernathy Matt McCoy Nia Peeples Cindy Pickett Marius Weyers
| music          = Harry Manfredini
| cinematography = Mac Ahlberg
| editing        = David Handman
| studio         = Carolco Pictures
| distributor    = TriStar Pictures
| released       =   January 13, 1989      April 13    May 31
| runtime        = 99 minutes
| country        = United States English
| gross          = $8,143,225
}}
 science fiction Matt McCoy. 

==Plot==
DeepStar Six is a prototype deep-sea colony funded by the United States Navy with a collection of 11 military and civilian crew members in the last week of their six month tour. The brainchild of Dr. Van Gelder, the base is allowed to conduct its experiment on underwater colonization provided that he also oversee the installation of a nuclear missile storage platform nearby. Nearing the last days of his deadline, Van Gelders plans are threatened when geologist Burciaga (Elya Baskin) discovers a massive cavern system at the installation site. Van Gelder orders the submarine crew to use depth charges to collapse the cavern, to the dismay of Dr. Scarpelli (Nia Peeples), who wants to preserve the cavern to study the potentially primordial ecosystem she suspects lies inside.  

The detonation causes the site floor to partially collapse into a massive fissure. Submarine pilots Osborne (Ronn Carroll) and Hodges (Thom Bray) send an unmanned probe to explore, but lose contact and venture in after it. Upon finding the probe, they detect a large sonar contact moments before being attacked and killed by an unseen force. The aggressor attacks the Sea Track observation pod, crippling it and sending it teetering on the edge of the ravine. Joyce Collins (Nancy Everhard) is trapped in the pod with Burciaga, who succumbs to his injuries. Captain Laidlaw (Taurean Blacque) and submarine pilot McBride (Greg Evigan)--who is also Collins paramour--attempt a rescue. After barely escaping the mysterious sonar contact, they dock with the pod and rescue Collins, but the unstable hatch door closes on Laidlaw. Mortally wounded, he floods the compartment, forcing McBride and Collins to return to DeepStar Six without him. 

The survivors prepare to return to the surface after the Navy accepts their emergency situation, but the missile platform must be secured before they can leave it unattended. Without Laidlaw, fastidious facility technician Snyder (Miguel Ferrer) is forced to rely on preset computer protocol to secure the platform. When prompted by the computer to explain the reason, Snyder reports "aggression," due to the attacking creature. The computer interprets Snyders definition of "aggression" as a military assault and instructs him to detonate the missiles remotely. Snyder complies and the ensuing detonation creates a shockwave that virtually destroys DeepStar Six and cripples the cooling system for the bases nuclear reactor. With failed life support, they begin repairs to restore power and pressure for the decompression procedure. 
 Matt McCoy) ventures outside in a JIM suit to repair external damage for the air support system, but his work lights attract the creature and it attacks him. The crew manage to retrieve his suit and haul him through the airlock, but the creature forces its way inside and bites through the armored suit, cleaving him in half and entering the dive center. The team retreats as the creature consumes the panic-stricken Scarpelli. Arming themselves with shotguns and shark-darts (harpoons with explosive carbon-dioxide cartridges), they move back inside the dive room to finish their work. They succeed, but the creature charges and Van Gelder accidentally backs into Snyders harpoon and is killed by explosive gas expansion before they escape to the medlab. Snyders mental state, already stressed from the claustrophobic environment, begins to unravel with guilt and the fear that the others blame him for everything that has transpired. After a hallucination of a vindictive Van Gelder, Snyder jumps into the escape pod and launches for the surface. Because he has not undergone decompression, the pressure changes from the ascent cause him to burst. 

McBride braves the flooded base and swims to the mini-sub with the intention of connecting it to the decompression chamber and converting it into an escape vessel. During his absence, the creature bursts into the lab and forces Dr. Norris (Cindy Pickett) to attack with an overcharged defibrillator. As Collins and McBride escape into the decompression chamber, Norris electrocutes herself and the creature. Once the decompression is complete, they flee the base in the sub minutes before the reactor explodes. The sub breaches the ocean surface and they inflate the emergency life raft. They are interrupted as the creature bursts from the water and attacks the submarine, trapping McBride inside. McBride opens the submarines fuel tanks into the sea and ignites it with a flare gun. The submarine explodes, killing the creature in the process. Collins is distraught until McBride surfaces beside the raft, having dived under prior to the blast. They begin paddling across the ocean, waiting for a Navy rescue team to arrive.

==Cast==
* Taurean Blacque as Captain Phillip Laidlaw, station commander
* Nancy Everhard as Joyce Collins
* Cindy Pickett as Dr. Diane Norris, physician
* Miguel Ferrer as Snyder, mechanic
* Greg Evigan as McBride, submarine pilot Matt McCoy as James Jim Richardson
* Nia Peeples as Scarpelli, marine biologist
* Marius Weyers as Dr. John Van Gelder
* Elya Baskin as Burciaga
* Thom Bray as John Johnny Hodges
* Ronn Carroll as Osborne

==Production==
Producer Cunningham developed the idea in 1987 with the express purpose of being the first release on the slate of upcoming underwater action/sci-fi films.   

Originally, Robert Harmon was going to direct the film.  However, when he left, Cunningham stepped in to direct the film with a budget of $8,000,000. 

The creature was initially designed by Chris Walas, who then turned his production designs over to FX head Mark Shostrom.  Shostrom made slight alterations and changed the creatures color scheme. 

==Release==
DeepStar Six was poorly received by critics and viewers. It has a 23% score on Rotten Tomatoes (2.5 out of 10).
The film was released by TriStar Pictures in the United States on January 13, 1989. It opened on 1,117 screens and debuted in eighth place with a weekend total of $3,306,320. Its final box office total was $8,143,225. 
  The Rift (Endless Descent) and The Abyss. With the exception of The Abyss, none of these films were box office hits.

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 