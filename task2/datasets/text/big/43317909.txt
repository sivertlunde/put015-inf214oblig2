Thangamana Thangachi
{{Infobox film
| name           = Thangamana Thangachi
| image          = 
| image_size     =
| caption        = 
| director       = Senthilnathan
| producer       = K. R. Prabhu K. R. Ganesh K. R. Suresh
| writer         = K. Rajan
| starring       =  
| music          = Sankar Ganesh
| cinematography = M. Kesavan
| editing        = J. Elango
| distributor    =
| studio         = Ganesh Cine Arts
| released       =  
| runtime        = 120 minutes
| country        = India
| language       = Tamil
}}
 1991 Tamil Tamil drama film directed by Senthilnathan. The film features R. Sarathkumar and Aamani in lead roles. The film had musical score by Sankar Ganesh and was released on 15 January 1991.   

==Plot==
 Mansoor Ali Khan) rapes the young girls as he wish. At Seethas wedding, Lakshmis brother kidnaps Seetha, then he rapes and kills her. Vijay decides to take revenge.

==Cast==

*R. Sarathkumar as Vijay
*Aamani (credited as Meenakshi) as Lakshmi Mansoor Ali Khan Senthil
*Jaiganesh as Marappan
*Shanmugasundaram
*K. R. Ganesh as Kannan Sonia as Geetha
*Venniradai Moorthy as Kottapatti Kunjithapatham
*Vimalraja
*K. R. Prabhu as Prabhu
*Ragapriya as Seetha
*Kamala Kamesh as Meenakshi
*Vijayachandrika
*V. R. Thilagam
*Loose Mohan
*Sivaraman
*Chitraguptan as Kili
*Chithra Master Tinku as Vijay (young) Baby Sujitha as Lakshmi (young)
*Mary
*Vellai Subbaiah
*K. Natraj
*K. Rajan as Paramasivam
*Senthilnathan

==Soundtrack==

{{Infobox album |  
| Name        = Thangamana Thangachi
| Type        = soundtrack
| Artist      = Sankar Ganesh
| Cover       = 
| Released    = 1991
| Recorded    = 1991 Feature film soundtrack |
| Length      = 18:04
| Label       = 
| Producer    = Sankar Ganesh
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Sankar Ganesh. The soundtrack, released in 1991, features 5 tracks with lyrics written by Kalidasan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Chinnachiru Ponmane || Minmini || 5:02
|- 2 || Mano || 4:15
|- 3 || Kalyaana Ponne  (sad)  || Prabhakar || 0:36
|- 4 || Vaaya Katti Vayatha Katti || Swarnalatha || 4:20
|- 5 || Vaanam Paadikootame || Minmini || 3:51
|}

==References==
 

 
 
 
 
 