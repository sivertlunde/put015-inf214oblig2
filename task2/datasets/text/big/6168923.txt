American Cowslip
{{Infobox film
| name = American Cowslip
| image = American Cowslip.jpg
| caption = 
| director = Mark David
| producer = Mark David Ronnie Blevins Brent Clackson
| writer = Mark David Ronnie Gene Blevins Christopher Morrison
| starring = Ronnie Gene Blevins Val Kilmer Diane Ladd Rip Torn Cloris Leachman Priscilla Barnes Hanna R. Hall
| music = Joseph Blaustein Mark David
| cinematography = Mark David
| editing = Jonathan Lucas Mark David
| studio = 
| distributor = Buffalo Speedway Film Company
| released =  
| runtime = 113 minutes
| country = United States
| language = English
| budget = 
| gross = 
}} Sweet Thing (1999), and his second, acclaimed feature, Intoxicating (film)|Intoxicating (2003). This was Peter Falks final film before his death in 2011.

==Plot== agoraphobic heroin diabetic and abusive father (Bruce Dern).

==Cast==
* Ronnie Gene Blevins as Ethan Inglebrink
* Val Kilmer as Todd Inglebrink
* Diane Ladd as Roe
* Rip Torn as Trevor OHart
* Cloris Leachman as Sandy
* Lin Shaye as Lou Anne
* Josh Perry as Billy
* Peter Falk as Father Randolph
* Bruce Dern as Cliff
* Tom Huddlestone as Rabbi Dave
* Priscilla Barnes as Samantha
* Hanna R. Hall as Georgia
* Blake Clark as Grimes
* Trevor Lissauer as Jim Bob

==External links==
*  
*  
*  

 
 
 
 
 
 

 