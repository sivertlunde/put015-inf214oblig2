Fit (2010 film)
 

{{Infobox film
| name = Fit
| director = Rikki Beadle-Blair
| producer = Carleen Beadle   Rikki Beadle-Blair   Diane Shorthouse
| writer = Rikki Beadle-Blair
| editing = Edmund Swabey
| distributor = Peccadillo Pictures
| released =   
| runtime = 108 minutes
| country = United Kingdom
| language = English
| italic title = force
}}
 straight millennial students taking drama and dance class. The original play had been developed in 2008 to address the growing problem of homophobic bullying in British schools, and was especially created for KS3 students, with a specific focus on learning objectives from the National Curriculum including PHSE and Citizenship. The film itself was opened in the form of an introductory chapter, with six interlinking chapters of fifteen minutes, each focusing on one of the main characters in a first-person perspective of their life, views and problems. The DVD release of Fit also contained five video diaries for each of the characters, giving students and other viewers the opportunity to listen to the characters talking more in-depth about their feelings and the situation they are facing.   


The 2008 play performed in theatre venues including -

*The Drill Hall 
*The Birmingham Rep 
*The Contact Theatre
*The Unity Theatre, Liverpool The CCA, Glasgow
 The Guardians website.       The 2008 cast had included Duncan MacInnes, Ludvig Bonin, Sasha Frost, Lydia Toumazou, Stephen Hoo, Jay Brown and Rikki Beadle-Blair. In the film adaption in 2010, there was a new script, with the original cast staying as the central characters, and being joined by others. The film has won many awards  and the leading actors fly to Los Angeles for Outfest Film Festival for the World Premiere of Fit at the Directors Guild of America on Sunset Blvd. 

==Cast==

===Principal cast===
*Duncan MacInnes as Terry "Tegs", a geek|geekish, quiet boy. He is shy and chooses to keep his head down despite verbal and physical bullying from Ryan, Isaac and other students, who think he is something of a sissy and therefore gay, at one point considering self-harming with a knife after a brick is thrown through his window. He is forced to rebuff Jordan after he admits that he loves him and tries to kiss him. However, they make up, and it is shown he is straight when he develops an attraction to fellow student Molly, who he later starts going out with.
*Ludvig Bonin as Jordan, a budding footballer and the best friend of Tegs. He supports Tegs through constant verbal and physical bullying from students who are suspicious that he is gay, and when Jordan admits he loves him and tries to kiss him, Tegs, who is actually straight, is forced to admit that he doesnt love him in that way. Regardless, Jordan later makes up with him, their friendship now closer than ever, and comes out to his adoptive parents and little brother Linus.
*Sasha Frost as Karmel - a feminine girl but also a lesbian; something she finds difficult to tell her friends, even Lee. Her parents are both uncaring towards her or her feelings and are openly homophobic - her father warns her that Lee, who he automatically presumes to be a lesbian because of "Butch and femme|butch" behaviour stereotypes, wont be tolerated if she tries to make a move onto Karmel. She later reveals her sexuality to Lee and then the class - despite being afraid of Lee knowing, she is still one of her closest friends -  and begins going out with Kim Taylor. Their relationship becomes rocky when Karmel attempts to ignore her attraction to women and tries to go out with Tyler, though they make it past this.
*Lydia Toumazou as Lee, a tomboy, whom is close to her brothers but closer to her best friend Karmel. She suffers taunts and verbal abuse from some members of the class who presume she is gay due to butch stereotypes, as well as criticism from her brothers, who think she would be better accepted by people if she dressed like a girl. She is hurt when Karmel reveals that she hid her sexuality out of her fear for Lee rejecting this, but forgives her. To the shock of her siblings, she comes home in a mans car, after which they accept that she is straight, regardless of her personality.
*Stephen Hoo as Ryan, who lives with his sister and father. He bullies Tegs heavily as he thinks he is gay, but this is due to the fact that he cannot accept his own homosexuality, which he hides from his friends. When they find out, Isaac becomes temporarily abusive towards him, once beating him up when drunk after he discovers Ryan is attracted to Tegs.
*Jay Brown as Isaac, Ryans friend. He has a tense relationship with his father, and initially bullies Tegs with Ryan, beating Ryan up after he discovers he is gay. When his dad, who is openly homophobic, finds out Ryan is gay, he throws Isaac out on the streets. Isaac eventually accepts Ryans sexuality and becomes mates with him at the end of the film.
*Rikki Beadle-Blair as Loris, openly gay - one sign is the pink leotard he wears when teaching the dance and drama class - and friendly and supportive to everyone. As he cares for peoples feelings despite their sexuality, he tries to avoid "giving up" on any of the students who display homophobia to others in the class.

===Supporting/others===
*Alexis Gregory as Luca
*Andy Williamson as Jacek
*Donovan Cary as Principal Vernon Bailey
*Jack Shalloo as Charlie
*Ambur Khan as Nina
*Jason Maza as Tyler
*Katie Borland as Molly, a love interest and friend of Tegs
*Alex Papadakis as Aristos
*David Avery as Marios
*Tom Ross-Williams as Jamie
*Jennifer Daley as Kim Taylor, Karmels girlfriend
*Dani Bright as Keli Stern
*Vincent Williams as Jonas
*Richard Tan as Vander
*Davina Dewrance as Darcy James
*Tigger Blaize as Alex
*Rachel Lynes as Charlotte
*Marcus Kai as Ethan
*Gary Beadle as Karmels Dad
*Jo Castleton as Karmels Mum
*Dann Kharsa as Straight Bloke
*Hannah Chalmers as Straight Girl
*Samantha Lyden as Bus Stop Rita
*Jeanette Rourke as Tegs Mum
*Ryan Quartley as Linus, Jordans little brother
*Janine Stride as Jordans Mum
*Matt Ray Brown as Jordans Dad
*Kyle Treslove as Toby
*Michael Warburton as Jonesy
*Helen Russell-Clark as Ryans Sister
*Chooye Bay as Ryans Dad

==References==
 

==External links==
* 


 
 
 
 
 
 