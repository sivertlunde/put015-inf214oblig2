The Sentimental Swordsman
 
 
{{Infobox film name = The Sentimental Swordsman image = TheSentimentalSwordsman.jpg alt =  caption = Film poster traditional = 多情劍客無情劍 simplified = 多情剑客无情剑 pinyin = Duōqíng Jiànkè Wúqíng Jiàn jyutping = Do1 Cing4 Gim3 Haak3 Mou4 Cing4 Gim3}} director = Chor Yuen producer = Run Run Shaw screenplay = Chor Yuen story = Gu Long starring = Yueh Hua Fan Mei-sheng Ku Feng music = Frankie Chan cinematography = Wong Chit editing = Chiang Hsing-lung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 96 minutes country = Hong Kong language = Mandarin budget =  gross = HK$1,603,264
}} Yueh Hua, Xiaoli Feidao Series of novels.

It was one of Shaw Brothers highest grossing films in the studios history, and it was followed by a sequel, Return of the Sentimental Swordsman, in 1981. An in-name-only "sequel", Perils of the Sentimental Swordsman, was released in 1982, with no relation to the principal character of Little Flying Dagger Li.

== Synopsis ==
Due to his own extreme ideals, famed swordsman Li has lost everyone dear to him. After his life is saved by a rival swordsman, Lis overwhelming pride makes him forsake the woman he loves and lets her marry his saviour. Now resigned to traveling the country with his loyal aide, Lis only comfort is alcohol and the simple life he has now accepted. On one such journey, the lonely swordsman befriends the exceptionally skilled, yet secretive Fei who has his own pressures to contend with. Li then finds himself embroiled in a battle to own the highly prized gold armour shirt — a protective vest that can withstand any blow.

As he discovers that many of the people he meets have a hidden agenda, Li realizes that Fei is the only man he can truly trust. This new friendship is put under a test when the sentimental swordsman is hunted down by numerous hired killers and framed for a series of crimes he didnt commit. The person behind Lis troubles proves to be elusive, though all the clues seem to point to the legendary Plum Blossom Bandit, a disguised figure whose identity has long proved elusive to the martial world.

== Cast ==
  
* Ti Lung as Little Flying Dagger Li
* Ching Li as Lin Xian Er
* Derek Yee as Ah Fei Yueh Hua as Long Xiao Yun
* Candice Yu as Shi Yin
* Fan Mei-sheng as Chuan Jia
* Ku Feng as Zhao Zheng Yi
* Norman Chu as Iron Flute
* Ngai Fei as Iron Sword Tian Qi
* Yuen Wah as You Long Sheng
* Goo Man-chung as Gong Sun
* Cheng Miu as Shaolin abbot
* Yeung Chi-hing as Monk Xin Mei
* Ku Kuan-chung as Monk Xin Jian
* Chan Shen as Shaolin monk
* Wang Sha as Doctor Mei Er
* Shum Lo as Hua Feng
* Lee Sau-kei as Zhu Ge Lei
* Keung Hon as Golden Lion Escort man in blue
* Wong Pau-gei as Golden Lion Escort man
* Fung Hak-on as White Snake
* Alan Chui as Black Snake
* San Kuai as Black Snakes man
* Yuen Yat-choh as Black Snakes man
* Brandy Yuen as Black Snakes man
* Wong Chi-keung as Black Snakes man
* Chan Kwok-kuen as Black Snakes man in yellow
* Lau Jun-fai as Black Snakes man / Yuns man
* Chiang Nan as 5 Poisons Kid with cold needle
* Fung Ging-man as 5 Poisons Kids man with bun
 
* Cheung Chok-chow as 5 Poisons Kids man selling bun
* Lui Hung as 5 Poisons Kids woman with noodle Lo Wai as 5 Poisons Kids man
* Wong Chi-ming as 5 Poisons Kids man
* Chui Fat as 5 Poisons Kids man
* Ng Hong-sang as Yuns invited hero
* Jamie Luk as Yuns invited hero
* Stephan Yip as Yuns invited hero
* Tang Tak-cheung as Yuns invited hero
* Yuen Cheung-yan as Yuns invited hero
* Wang Han-chen as Golden Palm Wang Chen
* Lam Ching-ying as Yuns man
* Chik Ngai-hung as Yuns man
* Liu Wai as Shen Lao San
* Yuen Bun as Shaolin disciple
* Chan Dik-hak as Shaolin disciple
* Law Keung as Shaolin disciple
* To Wing-leung as Shaolin monk
* Ting Tung as Huans escort to Shaolin
* Sai Gwa-pau as cook
* Tsang Choh-lam as waiter
* Wong Kung-miu as waiter
* Fung Ming as restaurant customer
* Cheung Sek-au as restaurant customer
* Ng Yuen-fan
* Lau Wai-man
* Lam Choi
* Kong Chuen
 

== See also ==
* The Romantic Swordsman (1978 TV series)|The Romantic Swordsman (1978 TV series)
* The Romantic Swordsman (1995 TV series)|The Romantic Swordsman (1995 TV series)
* Flying Daggers

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 