Tiger Cage (film)
 
 
{{Infobox film
| name           = Tiger Cage
| image          = TigerCage.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 特警屠龍
| simplified     = 特警屠龙
| pinyin         = Tè Jǐng Tú Lóng
| jyutping       = Dak6 Ging2 Tou4 Lung4 }}
| director       = Yuen Woo-ping
| producer       = Stephen Shin
| writer         = 
| screenplay     = Anthony Wong Kim Yip
| story          = 
| based on       = 
| narrator       = 
| starring       = Simon Yam Carol Cheng Jacky Cheung Irene Wan Donnie Yen Bryan Leung Ng Man-tat Wang Lung-wei
| music          = Donald Ashley Norman Wong
| cinematography = Lee Kin Keung Chan Man To
| editing        = Lee Yim Hoi Chan Kei Hop Kwong Chi Leung Cheung Kwok Kuen
| studio         = Sammo Hung#D&B Films Company Ltd|D&B Films
| distributor    = 
| released       =  
| runtime        = 89 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$11,534,315
}}
 Hong Kong action film directed by Yuen Woo-ping and starring Simon Yam, Carol Cheng, Jacky Cheung, Irene Wan and Donnie Yen. The film was followed by a sequel Tiger Cage 2, released two years later in 1990 featuring a new storyline, with Yuen Woo-ping returning as director and Donnie Yen and Carol Cheng returning in different roles.

==Plot==
Inspector Michael Wong (Simon Yam), alongside Shirley Ho (Carol Cheng), Brother Lung (Bryan Leung), Fan Chun Yau (Jacky Cheung), Uncle Tat (Ng Man-tat) and Terry (Donnie Yen) are all part of the Anti-Drug Unit of the Royal Hong Kong Police Force. The relationship between them are like brothers in general. During an operation, they worked together to bust a drug dent but the chief drug trafficker Swatow Hung (Wang Lung-wei) escaped. Just as they were celebrating their success, Hung found them and killed Lung. In order to avenge Lung and find Hung, they took a variety of means and finally found and killed Hung. During an accident, Yau filmed Tat trading with a drug trafficker and tells Terry and Michael. After Michael found out, in order to protect himself, he killed Terry and framed Yau, who was then suspended from duty, which caused Yau to suspect something fishy. One day, Yaus girlfriend Amy (Irene Wan) found a videotape at home and discovered that Michael and Tat were accomplices, but in order to save Yau, she handed the tape to Michael, which also led to her fatal demise. At this point, Yau and Shirley discover the truth to everything, that Hung luckily escaped from the drug bust earlier and came back to revenge on Lung, who was an undercover cop, on the night before his wedding with Shirley. Yau vows to bring the perpetrator to justice and dig out the mole inside the police force. Terry, who was partners with Tat, discovered his relationship with the drug traffickers and conflicted for his loyalty to Tat or his ethnic standards. Tat was used by his superior Michael and was ultimately abandoned and killed. Shirley finally discovers the truth to the case but now it is difficult for all of them to escape death.

==Cast==
*Simon Yam as Michael Wong
*Carol Cheng as Shirley Ho Suet Ling
*Jacky Cheung as Fan Chun Yau
*Irene Wan as Amy
*Donnie Yen as Terry
*Bryan Leung as Brother Lung
*Ng Man-tat as Uncle Tat
*Wang Lung-wei as Swatow Hung
*Vincent Lyn as Vincent
*Yuen Shun-yi as Hungs brother
*Yuen Cheung-yan as Policeman
*Michael Woods as Drug dealer from USA
*Paul Wong as Man gets kicked in drug bust
*Chan King as Policeman
*Yuen Woo-ping as Master Eight
*Fung Hak On as Hungs buddy
*Stephen Berwick as Drug dealer from USA
*Wai Kei Shun as Officer Wai
*Peter Mak as Policeman
*Jameson Lam as Policeman at Lungs party
*Chang Sing Kwong
*Lam Foo Wai as Mechanic
*Fei Pak as Policeman at Lungs funeral

==Box office==
The film HK$11,534,315 at the Hong Kong box office during its theatrical run from 28 July to 10 August 1988 in Hong Kong.

==See also==
*Jacky Cheung filmography
*Donnie Yen filmography

==External links==
* 
*  at Hong Kong Cinemagic
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 