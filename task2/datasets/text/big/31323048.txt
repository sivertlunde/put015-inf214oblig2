Don't Forget My Little Traudel
{{Infobox film
| name           =Dont Forget My Little Traudel
| image          = 
| image size     =
| caption        =
| director       = Kurt Maetzig
| producer       =Hans-Joachim Schoeppe
| writer         = Kurt Barthel
| narrator       =
| starring       = Eva-Maria Hagen
| music          = Hans Hendrik Wehding
| cinematography = Erwin Anders
| editing        =Ilse Peters
| distributor    = PROGRESS-Film Verleih
| released       = 15 November 1957
| runtime        =86 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Dont Forget My Little Traudel (  comedy film, directed by Kurt Maetzig. It was released in List of East German films|1957. 

==Plot==
Traudel is a war orphan, whose mother had died in the Ravensbrück concentration camp after refusing to renounce her love for a Czechoslovak prisoner. The only remnant the daughter has from her mother is a letter ending with the words "dont forget me, my little Traudel". When she turns seventeen, she flees the orphanage and ventures to Berlin, where she meets policeman Hannes, who falls in love with her and even forges documents for her. He is caught, but is only slightly reprimanded, and marries her. 

==Cast==
* Eva-Maria Hagen: Traudel Gerber
* Horst Kube: Hannes Wunderlich
* Günther Haack: Wolfgang Auer 
* Erna Sellmer: Mrs. Palotta
* Günther Simon: Police officer
* Fred Delmare: Traudels roommate
* Paul R. Henker: man in the shop 
* Sabine Thalbach: cashier 
* Nico Turoff: night watchman 

==Production==
At the late 1950s, the East German cultural establishment allowed a certain liberalization in the national cinema industry, and a series of entertainment-oriented films was produced by   when making Dont Forget My Little Traudel, and included a scene in which Eva-Maria Hagens skirt fluttered in the wind in a manner reminiscent of Marilyn Monroes famous appearance. The movie was the actress debut on screen.  

==Reception==
Although Traudel was a commercial success, Maetzig complained that most critics did not respond well to the film: Mikhail Romm told that the director "betrayed Socialist Realism" after watching it.  The East German authorities continued to see it in negative light after its release.  

Antonin and Miera Liehm called the film "a tale of cheap sentimentality".  Joshua Feinstein wrote that "the director was certainly not above... depicting women in demeaning conventions... as a voluptuos ditz".  Sabine Hake noted that the film, while presenting the adventures of a teenage girl, still used the conventions of class struggle and other communist motifs when depicting society.  

==References==
 

==External links==
*  
*  on cinema.de.
 

 
 
 
 
 