No Mercy (film)
{{Infobox Film
| name           = No Mercy
| image          = Nomercyposter.jpg
| image_size     =
| caption        = Theatrical release poster Richard Pearce
| producer       = D. Constantine Conte
| writer         = Jim Carabatsos
| starring       = Richard Gere Kim Basinger
| music          = Alan Silvestri
| cinematography = Michel Brault
| editing        = Gerald B. Greenberg Bill Yahraus
| studio         = Delphi Productions
| distributor    = TriStar Pictures
| released       = December 19, 1986
| runtime        = 106 minutes
| country        = United States
| language       = English
| gross          = $12,303,904
}} 1986 film starring Richard Gere and Kim Basinger about a cop who accepts an offer to kill a Cajun gangster.

==Plot==
Eddie Jilette is a Chicago cop on the vengeance trail as he follows his partners killers to New Orleans to settle his own personal score. Eddie and a Cajun sexpot, Michele, flee through the Louisiana bayous from a murderous crime lord who wants his baby doll back and to destroy the Chicago detective who would avenge his partners murder. The sexy swamp girl finds herself falling for Eddie, although they clash repeatedly while handcuffed together as they attempt to elude the brutal underworld figure and his henchmen.

==Cast==
* Richard Gere - Eddie Jillette
* Kim Basinger - Michel Duval
* Jeroen Krabbé - Losado
* George Dzundza - Captain Stemkowski
* Gary Basaraba - Joe Collins
* William Atherton - Allan Deveneux
* Terry Kinney - Paul Deveneux
* Ely Pouget - Julia Fischer
* Bruce McGill - Lieutenant Hall
* Ray Sharkey - Angles Ryan
* Marita Geraghty - Alice Collins
* Aleta Mitchell - Cara
* Fred Gratton - Quiet
* Dionisio - Pinto
* Kim Chan - Old Asian Man

==Reception==
No Mercy received negative reviews from critics and currently holds a 15% rating on Rotten Tomatoes.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 