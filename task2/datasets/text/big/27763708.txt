They Came to Rob Las Vegas
{{Infobox film
| name           = They Came to Rob Las Vegas
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        =  Antonio Isasi
| producer       = Nat Wachsberger Antonio Isasi
| writer         = 
| screenplay     = {{Plainlist |
*Antonio Isasi
*Lluis Josep Comeron
*Jorge Illa
*Jo Eisinger
}}
| story          = 
| based on       =  
| starring       = {{Plainlist |
*Gary Lockwood
*Elke Sommer
*Lee J. Cobb
*Jack Palance
}}
| music          = Georges Garvarentz
| cinematography = Juan Gelpí
| editing        = Emilio Rodríguez
| studio         = 
| distributor    = Warner Bros.-Seven Arts (USA)
| released       = {{Plainlist |
 
 
}}
| runtime        = 129 minutes
| country        = 
| language       = {{Plainlist |
* English (USA) Spanish (Spain)
}}
| budget         = 
| gross          = 
}}
 heist to Las Vegas. Its Spanish title was Las Vegas 500 Milliones. Filming took place in California, Nevada, and Spain.

==Plot==
The wealthy Skorsky runs an armored-car service with high-tech surveillance and weaponry. He also has ties to a criminal organization and is being investigated by Douglas, a government law-enforcement agent.

Tony Ferris takes a job as a dealer in a Las Vegas casino to become acquainted with Ann Bennett, a compulsive gambler who works for Skorsky, a married man who has designs on her. At first she is offended at realizing that Tony seduced her simply to learn more about Skorskys organization, but eventually she assists his plan to pull off a heist of an armored car in a remote part of the desert between Las Vegas and Los Angeles.

==Cast==
* Gary Lockwood&nbsp;– Tony Ferris
* Elke Sommer&nbsp;– Ann Bennett
* Lee J. Cobb&nbsp;– Steve Skorsky
* Jean Servais&nbsp;– Gino
* Georges Géret&nbsp;– Leroy
* Jack Palance&nbsp;– Douglas
* Fabrizio Capucci&nbsp;– Cooper
* Roger Hanin&nbsp;– The Boss
* Gustavo Re&nbsp;– Salvatore Daniel Martín&nbsp;– Merino
* Maurizio Arena&nbsp;– Clark
* Enrique Ávila&nbsp;– Baxter
* Gérard Tichy&nbsp;– Sheriff Klinger
* Rubén Rojo&nbsp;– Brian

==References==
 
 
* 
* 
* 
* 
* 
* 
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 