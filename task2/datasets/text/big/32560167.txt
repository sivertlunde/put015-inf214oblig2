From the Sky Down
 
 
{{Infobox film
| name           = From the Sky Down
| image          = From the Sky Down.jpg
| alt            = A black-and-white image of Larry Mullen, Jr. in front of a large cloud of smoke coming from a car. He is staring upwards with his body arched backward. The title "From the Sky Down" is written on the left, with film credits below.
| caption        = Film poster featuring Larry Mullen, Jr.
| director       = Davis Guggenheim
| producer       = {{plainlist |
* Ted Skillman
* Belisa Balaban
* Davis Guggenheim
* Brian Celler
}}
| starring       = {{plainlist |
* Bono
* Adam Clayton
* The Edge
* Larry Mullen, Jr.
}}
| music          = Michael Brook
| cinematography = Erich Roland
| editing        =  {{plainlist |
* Jay Cassidy
* Geraud Brisson
}}
| distributor    = Universal Music Group
| studio         = Documentary Partners
| released       =  
| runtime        =  {{plainlist |
* 85 minutes (directors cut)
* 74 minutes (Achtung Baby box set)
}}
| country        = United States
| language       = English
| budget         = 
}}
 rock band U2 and the production of their 1991 album Achtung Baby. The film documents the albums difficult recording period, the band members relationships, and the groups creative process. Guggenheim, who was commissioned by U2 to create the film to commemorate the records 20th anniversary, spent several months in 2011 developing the documentary. The band were filmed during a return visit to Hansa Studios in Berlin where parts of the album were recorded, and during rehearsals in Winnipeg for the Glastonbury Festival 2011. The film contains unreleased scenes from the groups 1988 motion picture Rattle and Hum, along with archival footage and stills from the Achtung Baby recording sessions. Development of the albums emblematic song  "One (U2 song)|One" is recounted through the replaying of old recording tapes.
 Super Channel. 2013 Grammy Award for Best Long Form Music Video.

==Background== American music for the project was variously labelled as "pretentious"    and "misguided and bombastic".    The groups high exposure and their reputation for being overly serious led to accusations of grandiosity and self-righteousness.   In addition to the criticism they faced, U2 dealt with internal creative dissatisfaction; lead vocalist Bono believed they were musically unprepared for their success, and drummer Larry Mullen, Jr. said, "We were the biggest, but we werent the best".   Towards the end of the Lovetown Tour in 1989, Bono announced onstage that it was "the end of something for U2", and that "we have to go away and&nbsp;... dream it all up again". 

Wishing to reinvent themselves and seeking inspiration from  ".  With improved morale, the group completed the album in Dublin in 1991. In November, Achtung Baby was released to critical acclaim. Musically, it incorporated influences from the alternative rock, electronic dance music, and industrial music of the time.    Thematically, it was a more introspective and personal record; it was darker, yet at times more flippant than the bands previous work. The album and the subsequent multimedia-intensive Zoo TV Tour were central to the groups 1990s reinvention, whereby they replaced their earnest public image with a more lighthearted and self-deprecating one.  Achtung Baby has been one of the groups most commercially successful records, selling 18&nbsp;million copies.   

==Production==
  was allowed access to the bands archives for the film, and despite the groups reservations, he was given final say over the films content.]]
To commemorate the 20th anniversary of Achtung Baby s original release, U2 reissued the record in several formats in October 2011.   Leading up to the anniversary, the band was unsure how much attention to pay to a past album, as they are still actively recording. Guitarist The Edge said, "How big a deal do we make of an anniversary when were in the middle of what were doing now? We had a hard time figuring that out. Were not a heritage act. Were still very active. But this record was so pivotal that we felt it was OK to revisit it."  Director Davis Guggenheim was subsequently commissioned by the band to make a film about Achtung Baby in six months. As a fan of U2 since his youth and having previously collaborated with The Edge for the 2008 documentary It Might Get Loud, Guggenheim agreed.  His goal for the film was to explain how U2 managed to remain together for so long when most rock groups are undone by internal conflict;  he described the bands longevity as "fighting against that law of physics".  Guggenheim also sought to tell the story of how the band transformed themselves musically over the course of Achtung Baby s recording sessions.   

  to revisit Achtung Baby.|alt=A ballroom with hardwood floors, and a ceiling decorated with chandeliers and octagon-shaped tiles. The wall on the left is decorated with dark wood panels, red walls, and windows. On the right is a small stage with a piano.]]
While U2 were on the South American leg of their U2 360° Tour in March–April 2011, Guggenheim requested complete access to the bands archives in Dublin. To his surprise, they complied.   While researching footage, the director was impressed by unused Rattle and Hum video that he found, describing it as "rare and beautiful".    Some of the footage included scenes of Bono throwing a tantrum in a dressing room and the band performing in a blues club. Guggenheim conducted the audio interviews with the band members that comprise much of the film while they were in Santiago, Chile.  After their touring obligations in South America ended, the band met Guggenheim in Berlin for two days in May "to go back to the scene of the crime".     They were filmed at Hansa Studios performing songs from the album and speaking to Guggenheim in lengthy individual interviews.  Additionally, the group were filmed touring Berlin and driving a Trabant,  an automobile that previously appeared in the album artwork for Achtung Baby and was incorporated into the lighting system of the Zoo TV Tour.      

The band expected a less personal treatment for the film and were at times uncomfortable with the extent to which Guggenheim probed into their history. Responding to the bands concerns, Guggenheim said, "I tell the story thats in front of me, warts and all. In the rock n roll business, its about adding layers. My process strips layers away. Rock stars are more comfortable creating an aura and mystique." Despite U2s discomfort, they allowed Guggenheim to have creative control over the film.  He remarked, "They said from the beginning, we want you to make the movie that you want to make and they let me make the movie I wanted to make. It was pretty astounding. I think part of it is the trust we gained doing It Might Get Loud, they sort of let me have a free hand."  Bono said that it was the least involved the band had ever been in a U2 project.  Additional filming took place on May 27 at Burton Cummings Theatre in Winnipeg during the bands rehearsals for the Glastonbury Festival 2011.   A rough cut of the movie was shown to the group in July, much to their satisfaction. According to Guggenheim, "They were over the moon. They loved it."  The only request the band made was that the film should be shortened in length, and Guggenheim agreed. 

==Release== Showtime television BBC Worldwide Super Channel on November&nbsp;19, 2011.  Standalone copies of the full, 85-minute directors cut of From the Sky Down were released on Blu-ray and DVD on December&nbsp;12, 2011, with bonus footage of the band performing at Hansa Studios and a question-and-answer session with Bono, The Edge, and Guggenheim from TIFF.  

==Reception== DAT tapes Mysterious Ways" and "One", along with the animated scenes.  Drew McWeeny of HitFix wrote that the documentary is not a "complete record" of Achtung Baby s conception, but that it "offers fans a rare glimpse at the process behind U2s music, and for non-fans, it attempts to set a context in which they can appreciate what it is that U2 accomplished". Like other reviewers, he highlighted the scene in which the band revisits the recording of "One" as one of the films most important. McWeeny ended his review by saying, "This may not be everything I wanted from the movie, but its solid, and the glimpse we get of real creative alchemy is impressive, indeed." 

Other reviewers were more critical. Neil Genzlinger of The New York Times said the film "has a few segments that get beyond platitudes". He enjoyed the sequences that highlighted individual songs but noted the documentary "doesnt have much in the way of full songs".  Steven Zeitchik of the Los Angeles Times called From the Sky Down a "procedural look" at the albums inception. Although he enjoyed Bonos humorous insights, Zeitchik said that "much of the movie is abstract, insider stuff about how he and others find inspiration".  John DeFore of The Hollywood Reporter pointed out the limited scope of the film, calling it "neither a comprehensive portrait nor one of those tossed-off featurettes that would be at home only as the filler for a commemorative Achtung Baby boxed set". Commenting on Guggenheims filmmaking style, DeFore judged that some of his attempts to make the film more "movie-ish" failed to enhance the subject material. In the reviewers opinion, this gave the "impression of a filmmaker who can tell this story competently but isnt quite up to making a lasting film about one of rock historys most successful bands".  Steven Hyden of The A.V. Club gave the film a C+, calling it "occasionally enlightening but mostly frustrating". The review lamented the lack of coverage of most of the albums songs, and in Hydens opinion, it was ironic that the band was trying to live down the "ego-inflating" Rattle and Hum in a film that he also considered "ego-inflating". 
 2013 Grammy Award for Best Long Form Music Video. 

==References==
;Footnotes
 

;Bibliography
 
*  
*  
*  
 

==External links==
* 

 
 
 

 
 
 
 
 
 
 