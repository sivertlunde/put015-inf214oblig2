Seethakoka Chiluka (1981 film)
{{Infobox film
| name        = Seetakoka Chiluka
| image       = Seethakoka Chiluka 1981.jpg
| image_size  =
| caption     = Film poster
| director    = P. Bharathiraja
| story = Manivannan
| screenplay  = P. Bharathiraja
| starring    =  
| producer    =  Poornodaya Movies
| music       = Ilaiyaraja
| cinematography = B. Kannan
| released    = 1981
| country     = India
| language    = Telugu 
}} Telugu romance film created by Poornodaya Movies and directed by P. Bharathiraja|Bharathiraja. After it was  remade from the Tamil version Alaigal Oivathillai,  Karthik Muthuraman reprised his role from the original film. Aruna Mucherla is the female lead character. The film was premiered at the 9th International Film Festival of India 

==Summary==
Raghu (Karthik) and the men of his village roam the village teasing girls. One day Karuna (Aruna) enters the village. Raghu and his friends make mischief with her but when they come to know that Karuna is Davids (Sharath Babu) sister they become afraid of her. Raghus mother is a classical music teacher. Karuna wants to learn classical singing hence she goes to Raghus house. Slowly Raghu and Karuna fall in love with each other. When Raghus mother comes to know about their love she warns Raghu about the difference between him and Karuna. Karuna belongs to a Christian community whereas Raghu is a pure Hindu. Moreover Karunas brother David is a heartless person who may even kill Raghu for loving Karuna. But Raghu and Karuna explore their love even when all the villagers are against them. They leave the village to fly to their lovely world, like butterflies, hence the title Seethakoka Chilaka.

==Cast==
* Karthik Muthuraman
* Aruna Mucherla
* Sharath Babu

==Awards== National Film Awards - 1981 Best Feature Film in Telugu

;Nandi Awards Best Feature Film - Gold

;Filmfare Awards South Best Film – Telugu

==References==
 

 
 

 
 
 
 
 
 
 
 