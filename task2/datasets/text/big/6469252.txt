Shooter (2007 film)
{{Infobox film
| name           = Shooter
| image          = Shooter2007Poster.jpg
| caption        = Theatrical release poster
| director       = Antoine Fuqua
| producer       = Lorenzo di Bonaventura
| based on       =  
| screenplay     = Jonathan Lemkin
| starring       = Mark Wahlberg Danny Glover Ned Beatty Michael Peña Tate Donovan Kate Mara Elias Koteas Rade Šerbedžija  Justin Louis  Rhona Mitra
| music          = Mark Mancina
| cinematography = Peter Menzies Jr.
| editing        = Conrad Buff Eric Sears
| studio         = di Bonaventura Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         = $61 million
| gross          = $95.7 million  . Box Office Mojo. IMDb. Retrieved 2011-11-25. 
}}

Shooter is a 2007 American   secret private military company unit. It was released in cinemas on March 23, 2007.

==Plot==
Gunnery Sergeant Bob Lee Swagger (Mark Wahlberg), a Force Recon Marine Scout Sniper, is on a recon mission in Africa with his spotter, Lance Corporal Donnie Fenn (Lane Garrison). During the mission, they begin taking fire from the enemy. As they are calling in for backup the duos Commander orders the mission to be shut down, leaving Swagger and Fenn as expendable. Swagger survives by shooting and destroying an enemy helicopter, but Fenn is killed.
 manhunt for the injured sniper. However, he meets a rookie FBI special agent, Nick Memphis (Michael Peña), disarms him, and steals his car.

After his escape, Swagger takes refuge with Sarah Fenn, Donnies wife. He later convinces her to help him contact Memphis with information on the conspiracy. Memphis is blamed for Swaggers escape and is informed that he will face disciplinary review but argues that, given Swaggers training and experience, it is surprising that the president survived and the archbishop standing several feet away was killed. He independently learns that Swagger may have been framed for the assassination and is further made suspicious when the officer that shot Swagger was murdered hours later.
 Senator Charles Meachum. Swagger records the ex–snipers confession of his involvement in the African massacre and then, with Memphis assistance, escapes from an ambush by mercenaries.
 kidnapped Sarah to lure Swagger out of hiding. With his new evidence and cat and mouse strategy, Swagger and Memphis are able to rescue her when Colonel Johnson and Senator Meachum arrange a meeting to exchange their hostage for the evidence of their wrongdoing. The Senator is allowed to escape, while Swagger and Memphis surrender to the Federal Bureau of Investigation|FBI.

Later, Swagger is brought before the attorney general and the FBI director in a closed-door meeting with Colonel Johnson, Memphis, and Sarah also present. Swagger quickly clears his name by loading a round into his rifle (which is there as evidence since it was supposedly used in the killing), aims it at the Colonel and pulls the trigger — which fails to fire. Swagger explains that every time he leaves his house, he removes the firing pins from all his guns, replacing them with slightly shorter ones, thus rendering them inoperable until he returns. Although Swagger is exonerated, Colonel Johnson cannot be charged with his crime as the Eritrean massacre was outside American legal jurisdiction. The attorney general tells Swagger that he himself must abide by the law. "Its not the Wild West where you can clean up the streets with a gun, even though sometimes thats exactly whats needed." 

Afterwards, as Johnson and Senator Meachum plan their next move, Swagger breaks in and kills both conspirators, arranging for the house to blow up as if by accident. In a final scene, he is driven away by Sarah.

==Cast==
 
 
* Mark Wahlberg as Gunnery Sergeant Bob Lee Swagger
* Michael Peña as Special Agent Nick Memphis
* Danny Glover as Colonel Isaac Johnson
* Kate Mara as Sarah Fenn
* Ned Beatty as Senator Charles F. Meachum
* Elias Koteas as Jack Payne
* Rhona Mitra as Special Agent Alourdes Galindo Jonathan Walker as Brent Dobbler
* Justin Louis as Special Agent Howard Purnell
* Tate Donovan as Russ Turner
* Rade Šerbedžija as Michailo Sczerbiak
* Lane Garrison as Lance Corporal Donnie Fenn
* Alan C. Peterson as Officer Stan Timmons
* Brian Markinson as Attorney General Russert
* Levon Helm as Mr. Rate
* Mike Dopud as Lead Mercenary
* Dean McKenzie as Archbishop
 

==Production==
===Locations=== Ashcroft and Cache Creek.  The assassination scenes were filmed in Philadelphia. The crowd scenes with the President and the Archbishop were filmed in Independence National Historical Park in front of Independence Hall. The sniper location was created using the exteriors of the church steeple at the junction of New Street and North 4th Street. These were then combined with an elevated view from another building to create a fictional vista of the park. Swaggers escape was filmed in New Westminster along the Fraser River. The car chase that ends when it plunged into the river was filmed down 6th Street and off the Westminster Quay. The following scene of Swagger clinging to the side of a dredger was also filmed on the Fraser River near the Pattullo Bridge.

===Sniper weapons and tactics===
Shooter depicts a number of sniper tactics, thanks to the guidance of former U.S. Marine scout sniper Patrick Garrity, who trained Mark Wahlberg for the film. Garrity taught Wahlberg to shoot both left and right-handed (the actor is left-handed), as he had to switch shooting posture throughout the movie, due to Swaggers sustained injuries. He was also trained to adjust a weapons scope, judge effects of wind on a shot, do rapid bolt manipulation and develop special breathing skills. His training included extreme distance shooting (up to 1,100 yards), and the use of camouflage ghillie suits. Fuqua appointed Garrity as the films military technical advisor. 

In the special features of the DVD, Garrity is interviewed pointing out that the shot fired in the assassination would not have hit the archbishop straight on, as in the film. When a round is fired it will fall from 30–40 feet depending on the distance of the shot. To compensate, the round is fired at an arc calibrated by how far the round is going to fall, the distance of the shot, temperature, humidity, wind and the curvature of the earth. In his interview Garrity said "At 1,800 yards, because of the hydrostatic shock that follows a large caliber, high velocity round such as the .408 Chey Tac (which is used in the shot), the target would literally be peeled apart and limbs would be flying 200 feet away". The exit wound on the archbishops head would have been too extreme to show in movie theaters. Instead, the movie depicts a much less graphic representation of the assassination.
 M68 Close Colt M1911-A1 Cooey model 60.

Also appearing in the film is a Precision Remotes Telepresent Rapid-Aiming Platform (TRAP), a remotely operated weapon platform that accepts a standard rifle. Precision Remotes website  appears in the film, and the company is credited in the closing credits.

==Music==
  Otis Taylor plays over the end of the film and credits.

==Release==
 
===Box office===
Shooter grossed $47 million in North America and $48.7 in other territories for a total gross of $95.7 million, against its $61 million budget.   

The film grossed $14.5 million in its opening weekend, finishing in 3rd at the box office behind TMNT (film)|TMNT ($24.3 million) and 300 (film)|300 ($19.9 million).

 
===Critical reception===
Shooter was met with mixed reviews from critics. On Rotten Tomatoes, the film holds a 48% approval rating, based on 147 reviews, with an average rating of 5.6/10. The sites consensus reads, "With an implausible story and numerous plot holes, Shooter fails to distinguish itself from other mindless action-thrillers."    Metacritic assigns the film a weighted average score of 53 out of 100, based on 33 critics, indicating "mixed or average reviews".    

Empire (magazine)|Empire magazine praised the movie: "The sequel-ready Swagger challenges Bournes supremacy with an impressive shoot-em-up, work-it-out action drama". 
 Vice President Dick Cheney.   

===Home media===
 
The DVD was released on June 26, 2007, reaching the top of the sales charts. 

==See also==
 
* Sniper (1993 film)|Sniper (1993 film)
* Assassinations in fiction

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 