The Free Will
{{Infobox film
| name           = The Free Will
| image          = 
| caption        = 
| director       = Matthias Glasner
| producer       = Frank Döhmann  Christian Granderath Andrea Hanke
| writer         = Judith Angerbauer Matthias Glasner Jürgen Vogel
| starring       = Jürgen Vogel Sabine Timoteo
| cinematography = Matthias Glasner Ingo Scheel
| editing        = Mona Bräuer Matthias Glasner Julia Wiedwald
| released       =  
| runtime        = 163 minutes
| country        = Germany
| language       = German, French
| budget         = 
}}

The Free Will ( ) is a 2006 German   received the Silver Bear for Outstanding Artistic Achievement and director Matthias Glasner received the Prize of the Guild of German Art House Cinemas. The film was also shown at various film festivals throughout 2006 and 2007, and Jürgen Vogel received Best Actor awards at Chicago International Film Festival and Tribeca Film Festival.

==Cast==
* Jürgen Vogel as Theo Stoer
* Sabine Timoteo as Netti Engelbrecht
* André Hennicke as Sascha

==External links==
*  

 
 
 
 
 
 

 
 