Bikhre Moti
{{Infobox film
| name           = Bikhre Moti
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Tapi Chanakya
| producer       = Omi Arora	
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Jeetendra Babita
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film starring Jitendra and Babita, directed by Tapi Chanakya and produced by Omi Arora. 

==Cast==
* Jeetendra
* Babita
* Nazir Hussain
* Tarun Bose Helen
* Asit Sen
* Sujit Kumar
* Kamini Kaushal
* Irshad Panjatan

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jalti Rahe Meri Mamta Ki Jyoti"
| Mahendra Kapoor
|- 
| 2
| "O Zara Dekh Ke Chalna Hay Hay"
| Mohammed Rafi
|-
| 3
| "Ye Jo Ladki Hai Ye Jo Ladka Hai"
| Mohammed Rafi, Lata Mangeshkar
|- 
| 4
| "Ye To Mister Bechain Hain (Main Bhi Sochoon Door)"
| Asha Bhosle
|}
==External links==
* 

 
 
 
 

 