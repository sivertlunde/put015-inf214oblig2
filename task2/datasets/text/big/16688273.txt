Storm in a Teacup (film)
 
 
{{Infobox film
| name           = Storm in a Teacup
| image          = Storm in a Teacup.jpg
| image_size     =
| caption        = Theatrical poster
| director       = {{plainlist|
* Ian Dalrymple
* Victor Saville
}}
| producer       = Victor Saville
| based on       = {{plainlist|
* Sturm im Wasserglas (German)  by Bruno Frank
* Storm in a Teacup (UK) and Storm Over Patsy (US) by James Bridie
}}
| screenplay     = {{plainlist|
* Donald Bull
* Ian Dalrymple
}}
| starring       = {{plainlist|
* Vivien Leigh
* Rex Harrison
* Cecil Parker
* Sara Allgood
}}
| music          = Frederick Lewis
| cinematography = Mutz Greenbaum
| editing        = {{plainlist|
* Cyril Randell Hugh Stewart
}}
| distributor    = United Artists (US)
| studio         = London Films
| released       =  
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
Storm in a Teacup is a 1937 British  .    A reporter writes an article that embarrasses a politician. Meanwhile, the newspaperman is also attracted to his targets daughter.

==Plot summary==
 

==Cast==
* Vivien Leigh as Victoria Gow
* Rex Harrison as Frank Burdon Provost William Gow
* Sara Allgood as Honoria Hegarty
* Ursula Jeans as Lisbet Skirving
* Gus McNaughton as Horace Skirving
* Edgar K. Bruce as McKellar (as Edgar Bruce) Robert Hale as Lord Skerryvore
* Quentin McPhearson as Baillie Callender (as Quinton Macpherson)
* Arthur Wontner as Procurator Fiscal
* Eliot Makeham as Sheriff
* George Pughe as Menzies
* Arthur Seaton as Police Sergeant
* Cecil Mannering as Police Constable
* Ivor Barnard as Watkins

==Reception== The Montreal Gazette wrote, "the excellent story is done fullest justice by the directors, Victor Saville and Dalrymple, and by the large and often-brilliant cast."  The critic for Boys Life called it "a riot of fun for the audience."   

The number of favourable reviews grew over time. Leonard Maltin rated this movie three out of four stars and called it "witty social comedy."    The book Guide to British Cinema considered this film as one of Victor Savilles "well-crafted, genre films" and "the breezy Rex Harrison&#x2013;Vivien Leigh social comedy."    The book British Film Directors: A Critical Guide called it "a whimsical comedy with anti-fascist undercurrents."    The book A Chorus of Raspberries: British Film Comedy 1929–1939 considered this film "one of the best British comedies of the decade."   

Anne Edwards, author of the 1977 biography of Vivien Leigh, considered this film a "funny but inconsequential comedy;" nevertheless, she called Leighs performance "witty and warm" for her role that "could not have given   much pride of accomplishment." Edwards. p.68. 

==References==
 

===Bibliography===
*  , 1977. Print. ISBN 0-671-22496-4.

==Further reading==
* McFarlane, Brian, ed.; Anthony Slide, asst. ed. The Encyclopedia of British Film: Second Edition – Fully Updated and Revised. London: Methuen Publishing, 2005. Print. ISBN 978-0-413-77526-9.
* Moore, Rachel. "Love Machines." Film Studies 4 (2004): 2–3. Web. 4 Jan 2012.  
*   Dover Publications, Inc., 1985. Print. ISBN 0-486-24860-7.
* Library of Congress. Copyright Office. "Dramatic Compositions."  . Washington: United States Government Printing Office|GPO, 1939. 46. Google Books. Web. 4 Jan 2012.
* Library of Congress. Copyright Office. "Motion Pictures."  . Washington: United States Government Printing Office|GPO, 1938. 347. Google Books. Web. 5 Jan 2012.
*Library of Congress. Copyright Office. "Federal Register: April 17, 1998 (Volume 63, Number 74): Notices: Page 19287-19366." United States Government Printing Office|GPO, 1998. 19299-300. Web. 6 Jan 2012  .  .

== External links ==
*  
*  
*  
*  
*   at "Public Domain" of the Peter Rodgers Organization official website

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 