The Deadly Mantis (1978 film)
 
 
{{Infobox film
| name = Shaolin Mantis
| image_size =
| image = Shaolin Mantis FilmPoster.jpeg
| caption =
| director = Lau Kar-leung
| producer =
| writer =
| narrator =
| starring = David Chiang Lily Li  Norman Tsui Lau Kar Wing
| music =
| cinematography =
| editing =
| distributor = Shaw Brothers Studio
| released =  
| runtime = 96 minutes
| country = Hong Kong Mandarin
| budget =
}} 1978 Shaw Brothers film directed by Lau Kar-leung, starring David Chiang and Liu Chia Hui.

==Plot==
Wei Fung (David Chiang), a young scholar recruited by the Emperor to infiltrate a group of rebels in the Tien Clan in order to get evidence of the clans connection to Ming loyalists, the rebel spy network and anti-Ching activities. If Wei fails in his mission, his own well-connected family will be punished.

Wei Fung encounters Tien Chi-Chi (Huang Hsin-Hsiu aka Wong Hang Sau), the granddaughter of the leader of the rebel group. When he is hired as Chi-Chis new instructor he realizes this is his best opportunity to infiltrate the rebel clan. Events become complicated when Chi-Chi begins to fall in love with Wei Fung at the same time that her grandfathers spies discover Wei Fungs true motives for seeking them out. When Chi-Chi learns that Wei Fung is about to be assassinated she pleads for his life. Grandfather Tien (Lau Kar Wing), unwilling to cause his granddaughter heartbreak, relents and decides that if Chi-Chi can persuade Wei Fung to marry her and never leave their familys villa, he will be allowed to live. If he refuses he will be killed on the spot.

Back at the Emperors court, the Emperor praises Wei Fung and his father for Wei Fungs success in eliminating an entire rebel nest and exposing their spy network. When the Emperor offers up a toast, Wei Fungs father poisons both his and his sons cup of wine. As soon as Wei Fung drinks from the cup, his father proudly proclaims his allegiance to the rebels. A shocked Wei Fung turns to his father who explains that Wei Fung has in fact helped a tyrant kill heroes working to overthrow his diabolical regime. Wei Fungs father dies as soldiers come to protect their emperor before Wei Fung can kill him. The film ends in freeze frame with Wei Fung fighting the emperors elite royal bodyguards.

==External links==
* 

 

 
 
 
 
 
 
 
 