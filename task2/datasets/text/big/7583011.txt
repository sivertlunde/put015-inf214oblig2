Incubus (2006 film)
{{Infobox film
| name           = Incubus
| image          = Incubus Teaser Poster.jpg
| caption        = Theatrical teaser poster.
| director       = Anya Camilleri Adam Shapiro
| writer         = Gary Humphreys
| starring       = Tara Reid, Akemnji Ndifernyan, Alice OConnell
| music          = Simon Boswell
| cinematography = John Lynch
| editing        = Andy McGraw, John Wilson
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = United Kingdom
| language       = Spanish/English
| budget         = 
}} horror Thriller thriller film by Sony Pictures Home Entertainment that was directed by Anya Camilleri and stars actress Tara Reid.  The film was released on May 3, 2006 and had an internet premiere on AOL during Halloween 2006.  An unrated version was released to DVD on February 6, 2007. The film has billed itself as the first Download To Own video.   

== Plot ==
Seeking refuge from a torrential storm, Jay, her brother, and three friends break into what they think is an abandoned recycling plant. (A fourth friend, Karen, decides not to enter the building and leaves.) They find two dead people, who appear to have killed each other, and a Sleeper - a coma patient hooked up to life support in a triple-locked, shatter-proof observation room. Closer examination reveals a disturbing truth: the Sleeper is Orin Kiefer, a murderer executed by lethal injection six years earlier.

Jay, her brother and friends search for a way out of the factory. A psychopathic man roaming the building attacks and kills her brother.

When Peter falls asleep, his dreams are invaded and his mind is controlled by the comatose madman. The madman turns Peter into a deranged killer like himself, and he tries to attack Jay and the remaining friends, but they make it safely to the observation room and try to sort the situation out.

They discover that the Sleeper in the locked room possesses the power of an Incubus demon, and can invade and control another humans mind through their dreams. To test this theory, they tell Holly to go to sleep. She is the weakest of the three, and if she is infected like Peter she can at least be tied up and easily kept under control without being hurt. She agrees and eventually falls asleep.

Jay and Bug wait, not expecting anything to happen. After a few minutes, Holly wakes up and tries to attack Bug and Jay. They try to get her to snap out of the spell of the Incubus, but when they fail they realize that the only way to stop her is to kill her, which they do. Angry, disgusted, and scared, Bug turns and attacks the comatose madman, still peacefully sleeping and dreaming. He rips off the machinery that keeps him in his unconscious state. Bug and Jay run to get out of the factory.

Peter is still waiting for them, and Bug quickly attacks him with a hammer. As he dies, Peter reveals that he is no longer under the control of the Incubus. As it turns out, the reason he is no longer psychotic is that the Incubus is no longer asleep. Because the machinery keeping him comatose was destroyed, he was able to free himself, wake up, and kill. He kills Bug by snapping his neck, and then goes after Jay.

Jay tricks the Incubus by making it look as though she has managed to escape onto the factorys roof. After she kills him she makes the mistake of falling asleep and dreams of the Incubus. In the dream he manages to get inside of her and when she wakes up she is delirious.

As sheriffs arrive on the scene, they discover the dead Incubus and the bloody Jay. They wrongly assume she is a killer, though she does not say a single word. She is led into a police car, in handcuffs, past Karen, who is in another police car. The movie ends with a brief shot of Jays eyes, which suddenly exhibit an alarming expression revealing that she has become a psychopathic killer, possessed by the Incubus.

== Cast ==
* Tara Reid as Jay
* Akemnji Ndifornyen as Bug
* Alice OConnell as Holly
* Russell Carter as Josh
* Christian Brassington as Peter
*  Mihai Stanescu as Sleeper
* Monica Dean as Karen
* Sandu Mihai Gruia as Dr. Gregg
* Luana Stoica as Dr. Yousov
* Ioan Brancu as Crazy Man
* Dan Mason as The Sheriff
* Silviu Olteanu as The Deputy Martin Sherman as Orin Kiefer (uncredited)

==Reception==
Critical reception was predominantly negative and review aggregator Rotten Tomatoes hosts three reviews for Incubus, all negative.  Dread Central, Entertainment Weekly, and DVD Talk all gave negative reviews for Incubus, all of whom heavily criticized Reids acting in the film.   

== References ==
 

== External links ==
*  
*  

 
 
 
 