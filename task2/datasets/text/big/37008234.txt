Stand Up Guys
{{Infobox film
| name           = Stand Up Guys
| image          = Stand up guys poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Fisher Stevens
| producer       = Sidney Kimmel Tom Rosenberg Gary Lucchesi Jim Tauber
| writer         = Noah Haidle
| starring       = Al Pacino Christopher Walken Alan Arkin Julianna Margulies
| music          = Lyle Workman
| cinematography = Michael Grady
| editing        = Mark Livolsi Sidney Kimmel Entertainment Lakeshore Entertainment Lionsgate
| released       =  
| runtime        = 95 minutes  
| country        = United States
| language       = English
| budget         = $15 million 
| gross          = $3,310,031 
}} crime comedy film directed by Fisher Stevens and starring Al Pacino, Christopher Walken, and Alan Arkin. The film was released in North America on February 1, 2013.   Stand up guy is an American phrase meaning a loyal and reliable friend. 

==Plot==
Released from prison after serving 28 years, Val reunites with his old friend and partner Doc. Doc has been ordered by mob boss Claphands to kill Val as soon as he gets out. One of the first things Val wants is to visit a local brothel, which is run by Wendy. After Val is unable to perform, he and Doc break into a drugstore where Val consumes a larger dose of Viagra than what is prescribed. After a return to the brothel, Val tells Doc that hes ready to party. They go to a club, where Val shares an intimate dance with a younger woman.

After snorting some pills, Val passes out, so Doc contemplates killing him then and there. Instead, he takes Val to a hospital where the head nurse is Nina, their old friend Hirschs daughter. Thereafter, they go to a local diner where Val correctly guesses that Doc is to kill him. Doc says hes been given until 10 a.m. to do the job, or else he will be killed as well. Val breaks into a black Dodge Challenger SRT8, and they go to get Hirsch, who had once been their getaway driver. Hirsch gets behind the wheel and almost immediately into a highway chase with the police. Asked what he wants to do next, the widowed Hirsch chooses the brothel, since he hasnt slept with anyone since his wife.

After they leave, the three men hear sounds coming from the Challengers trunk. In it, they find a naked Sylvia, who was kidnapped by men who abused her, then threw her into the trunk. Sylvia tells them where the kidnappers are. Val and Doc go there, then shoot a couple of them. After tying everyone up, Sylvia comes in with a baseball bat, whereupon Doc and Val leave her to her revenge. Back at the car, Doc and Val find that Hirsch has died. They break the news to Nina, who helps the two bury her father at the cemetery. Doc and Val return to a diner, where the young waitress (who waits on them several times) is revealed to be Docs granddaughter Alex, who adores Doc, but has no clue who he is. Doc makes a phone call to Claphands, begging him to show Val mercy, since he has only a few years left. Claphands reveals that he knows about Alex, and that he will hurt her if Doc doesnt complete the job.

Doc writes a letter to Alex, puts his home keys inside the envelope, then pins it to the wall of the diner. Walking down the street at sunrise, Val spots a church and he goes inside to give a priest his confession. Next, they break into a tailor shop, where they try on suits. Two thugs working for Claphands interrupt them, pestering Doc to get the job done. Doc and Val shoot them both. Alex goes to Docs apartment. The phone rings. Its Doc and he asks what she thinks of his paintings of sunrises. When he was painting, Doc says, he was thinking of her. A shoe box in the closet is filled with cash. The rent has been paid in advance for a year. He tells Alex goodbye and Alex calls him her grandfather. He tells Alex that he loves her and hangs up.

Strolling down to Claphands warehouse, Doc and Val draw their pistols and open fire. A firefight commences, during which Doc and Val shoot at Claphands and his men, killing both of the latter. A firefight between them and Claphands ensues. The camera pans above the building, and the sky turns into one of Docs sunrise paintings.

==Cast==
* Al Pacino as Valentine "Val"
* Christopher Walken as Doc
* Alan Arkin as Richard Hirsch
* Julianna Margulies as Nina Hirsch
* Mark Margolis as Claphands
* Katheryn Winnick as Oxana
* Vanessa Ferlito as Sylvia
* Addison Timlin as Alex
* Lucy Punch as Wendy
* Bill Burr as Larry
* Courtney Galiano as Lisa

==Release==
The film premiered at the Chicago International Film Festival on October 12, 2012 and was shown next at the Mill Valley Film Festival.

==Reception== Best Original Song for "Not Running Anymore" by Jon Bon Jovi. 

The film has received mostly mixed reviews: as of July 2013, it holds a 38% rating on review aggregator Rotten Tomatoes based on 105 critics with a weighted average of 5.2/10, while 49% of the audience has judged it with an average score of 3.2/5.  Slant Magazine gave the film two and a half stars out of four: 
 Apatow Video VOD knockoff. Your mileage with Stand Up Guys will depend on how much despair youre willing to endure in order to get to the worthwhile stuff&mdash;scenes in which the rookie filmmakers get out of the way and let the veteran actors play off of each other. 
 a grade of B-, and concluded the following: 

 Directing his first dramatic feature, Fisher Stevens does his best to give these gravel-voiced legends room to strut their stuff. But thats the problem: The movie is too much of a wide-eyed, ramshackle homage to 70s-acting-class indulgence. It needed much more shape and snap. Still, when Alan Arkin joins the party as a dying colleague, his antics—at least once he gets behind the wheel of a stolen car—give the film a fuel injection. Stand Up Guys reminds you that these three are still way too good to collapse into shticky self-parody, even when theyre in a movie thats practically begging them to. 

Roger Ebert of the Chicago Sun-Times enjoyed the film, giving it three and a half stars out of four, saying: "Apart from any objective ranking of the actors, Walken is a spice in any screenplay, and in Stand Up Guys, theres room for at least as much spice as goulash. Director Fisher Stevens begins with a permissive screenplay by Noah Haidle that exists in no particular city, for no particular reason other than to give the actors the pleasure of riffing through more or less standard set-pieces." 
 tropes only to subvert them and broaden them and bring out their truth." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 