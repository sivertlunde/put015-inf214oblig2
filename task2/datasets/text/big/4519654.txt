Salakhain
 
{{Infobox film
| name           = Salakhain
| image          = Salakhain cover.jpg
| caption        = 
| director       = Shehzad Rafiq
| producer       = Khalil Rana & Khwaja Rashid
| writer         = Amjad Islam Amjad Meera Saud Saud Ahmed Butt  Farooq Zamir
| music          = M. Arshad  Ali Jan
| editing        = 
| released       = August 13, 2004
| runtime        =
| country        = Pakistan
| language       = Urdu
| budget         = 
| domestic gross = 
| preceded_by    = 
| followed_by    =
}}

Salakhain (  film which was released in 2004. Stars included Ahmed Butt, Zara Sheikh and Meera (actress)|Meera. Music was composed by M Arshad. Sajid Hassan and Saud were cast as villains with Shafi Muhammad as their boss.

== Synopsis ==
Salakhain is an Urdu feature film set in the backdrop of Gawalmandi, Lahore. It is the story of a young man who is passionate about life and dreams of a bright and prosperous future. Little does he know what life has in store for him? He is destined for a future engulfed with hatred, anger and revenge.

Introducing Ahmad, Pakistans most sought after model and winner of LUX Style Awards 2003 for Best Model, in the Lead role, Salakhain is devoid of Compassion and sympathy. Full of nonstop action, Salakhain is a thriller sure to make you sit upright in your chair.

==Plot==
 
Salakhain tells the story of an innocent, hardworking student called Faizan (Ahmed Butt) who comes from a lower-middle-class family and who is in love with an equally innocent and earnest looking young girl. Faizan is the good son and wants to live with to his fathers dream which is to make a business and improve the lifestyle of their family. Unfortunately the dreams are shattered while he was taking his exams as he is implicated  in a crime a dispute with the booti mafia (people facilitating cheating during exams), this results to his fathers death and mothers mental health due to shock, one following the other in quick succession.

In jail he meets a man called Zaigham (Saud) and becomes friends with him who knows his enemies well and has his own issues to pick up with them. Zaigham gets Faizan out of lock-up. Faizan now wants revenge for his parents. The rest is as predictable as any action movie made anywhere in the world - a journey towards retribution, passing through the maze called politics and crime.

Zara sheik the girl  faizan love gets married to (sami khan) a police officer who is looking for faizan since he is a criminal and zara hides his picture when she sees it in samis wanted list so he wont know how he looks like. meera and faizan team up to kill sajid hassan who is the leader. meera wants to kill him because he killed her sister by throwing acid on her face. faizan finds his mother and at the end in a shootout faizan finda out that zara is married and meera and faizan are killed in a shootout.

==Reception==
Lahore: Golden Jubilee, 73 weeks (Gulistan 35, Capital 9, Plaza 9, Empire 8 weeks). Completed continuously one year in Lahore. It was a Super Hit.

The reason why this movie clicked and why others made similarly did not (and will not) do well lies in how and how much filmmakers are able to stretch the limits of the possible. How much experimentation is done within a given form is what gives a film a fresh image that can attract audiences.

What goes to the credit of makers of Salakhain is that they seem to know their limits. But this ability to remain within a given framework also turns out to be the films most significant flaw. The director and the writer are unable to put the possibilities to test. They have succeeded in as much as that they have produced something that scores seven out of ten on technical grounds. But they have failed as much as that they have come up with a film that scores dismally as far as the storyline is concerned.

The film is shot on the most breathtaking and exquisite locations in Pakistan and the pleasure of watching the movie on the silver screen will be greatly enhanced by
the digital (DTS) sound.

The decision as to which subjects can be banked upon for the creation of a successful flick is highly personal and may depend upon various reasons for various persons. But as a general rule it is money that makes the mare go. The basic consideration for most of the moviemakers remains - or should remain - commercial, though there have been plenty of notable exemptions to this rule. Anyone attempting to have a go at a productive commercial venture needs to keep some basic facts in mind though, the most significant being lessons from the past. One needs to tread carefully where others have faltered quite frequently. The nexus between student leaders, crime and politics is one theme that has been used successfully only very sparingly so one wonders why the producers decided to repeat it.

==Cast==
* Zara Sheikh
* Sami Khan (actor) Meera
* Saud Ali
* Ahmed Butt
* Shafi Mohammad
* Sajid Hasan

== External links ==
*  
*  
*  

 
 
 
 
 