Anaconda 3: Offspring
  
{{Infobox television film
| image = Anaconda_3_DVD.jpg
| caption = DVD cover
| director  = Don E. FauntLeRoy 
| producer = Alison Semenza
| writer = Nicholas Davidoff David Olson  Anthony Green John Rhys-Davies
| music = Peter Meisner 
| cinematography = Don E. FauntLeRoy 
| editing = Scott Conrad 
| studio = Stage 6 Films Hollywood Media Bridge Castel Film Romania SciFi Channel
| released =  
| runtime  = 91 minutes
| country  = United States Romania
| language = English
}}
 Sci Fi Channel on July 26, 2008 and released on DVD on October 21, 2008. It stars David Hasselhoff, Crystal Allen and John Rhys-Davies.

The film was followed by   (2009), as well as the spin-off crossover Lake Placid vs. Anaconda (2015).

== Plot ==
A very large female anaconda captured from the Amazon River basin and another larger and smarter male anaconda are being held in a scientific lab owned Wexel Hall for experimentation. The research project is led by Dr. Amanda Hayes (Crystal Allen) and funded by Murdoch (John Rhys-Davies), a well-known industrialist. While visiting the facility, Murdoch agitates the male anaconda with a large flashlight, and has it gassed to calm it down. However, as Murdoch, his assistant Pinkus (Ryan McCluskey) and a scientist leave, the anaconda breaks through his enclosures wall. As it escapes, it kills many of the people working in the lab, though Cupboard escapes the destruction. In looking through the facility, Amanda and another researcher realize that the second (Queen) anaconda has also escaped. Murdoch calls in a team of animal hunters, led by Hammett (David Hasselhoff), to capture both snakes. Amanda and Pinkus go with them. At a small farm in the middle of the woods, the owner is eaten alive by one of the snakes.

The hunting party arrives later and begin formulating a plan of attack. During the first confrontation with the snake, two of the party are killed: Captain Gronzy is stabbed by the genetically altered anacondas very sharp-knifed tail and Dragosh gets his head bitten off by the snake. Hammett arrives and gives the team a lecture on how to kill the snake. As the group splits up, Amanda heads off in a car with two other team members, Victor (Toma Danilă) and Sofia (Mihaela Elena Oros). During the next confrontation with the snake, a car crash results, with the anaconda spitting acidic venom in Victors face, burning it, injuring Sofia with a broken leg and leaving Amanda in the car. As Amanda is about to help her, the snake reappears and devours Sofia. Amanda is rescued by Hammett and both of them escape.

As they regroup, Amanda reluctantly reveals that the queen anaconda is pregnant and will give birth to more genetically "special" offspring in less than 24 hours. The teams wants to call in the military, but Hammett forbids it and threatens Amanda with jail for her role in creating it. In the morning, the party begins searching for the snake, but Pinkus has his hand bitten off by the anaconda and is killed. While Hammett searches for the snake on foot, Amanda and two of the three remaining hunters, Nick (Patrick Regis) and Amanda, spot the snakes going into an old factory, and follow them in to plant explosives around the building. Before they can finish, the male attacks. Nick helps Amanda escape but is wounded by the snake as it impales him with its tail, but Nick is able to discharge a grenade, killing both him and the snake. Hammett and his last remaining hunter, Andrei (Alin Olteanu), arrive and hear the grenade go off. As the two of them meet Amanda inside the factory, Andrei moves to set the charges, but Hammett kills him, causing Amanda to realize that Hammett is working for someone (Murdoch) wanting a live baby anaconda. Inside the building, the queen gives birth. After wounding Hammett, Amanda sets the timer on the explosives and escapes from the building, leaving Hammett to be attacked by the baby snakes while he tries to reach the bomb. Once she is at a safe distance, the explosives detonate. Amanda, sitting in a roadside, burns all of her documentation on the snake research and leaves into the sunset. Meanwhile, one of Murdochs men drives by heading from the factory where he found one baby snake still alive  .

==Cast==
* David Hasselhoff as Hammett
* Crystal Allen as Dr. Amanda Hayes
* Ryan McCluskey as Pinkus
* Patrick Regis as Nick Anthony Green as Captain Grozny
* John Rhys-Davies as Murdoch
* Alin Olteanu as Andrei
* Toma Danilă as Victor
* Milhaela Elena Oros as Sofia

== Reception ==
Similar to many made-for-television movies, the film received poor reviews for having almost nothing to do with the previous films, unoriginal confusing plot, wooden acting and low-budget special effects. It currently holds 2.7 on IMDB.

==Home media==
Anaconda 3: Offsping was released on DVD on October 21, 2008 by Sony Pictures Home Entertainment.

==See also==
*List of killer snake films

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 