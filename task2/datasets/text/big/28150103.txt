Pigs Have Wings (film)
{{Infobox film
| name           = Pigs Have Wings
| image          = Pigs have wings poster.jpg
| caption        = Film poster
| director       = Paolo Pietrangeli
| producer       = 
| writer         = Marco Lombardo Radice Giuseppe Milani Paolo Pietrangeli Lidia Ravera
| starring       = Franco Bianchi
| music          =  Giovanna Marini
| cinematography = Dario Di Palma
| editing        = Ruggero Mastroianni
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Pigs Have Wings ( ) is a 1977 Italian drama film directed by Paolo Pietrangeli and based on the book of the same name by Lidia Ravera and Marco Lombardo Radice. It was entered into the 27th Berlin International Film Festival.   

==Cast==
* Franco Bianchi
* Lou Castel
* Benedetta Fantoli
* Susanna Javicoli
* Marco Lucantoni
* Cristiana Mancinelli
* Anna Nogara

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 