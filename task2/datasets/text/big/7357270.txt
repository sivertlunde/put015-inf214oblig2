The Dog Problem
 
  directed and written by Thousand Words Films.
 Steve Jones Ultimate Fighters Tito Ortiz and Kimo Leopoldo.

==Plot==
In the film, Solo (Ribisi) is a down-on-his-luck writer who is encouraged by his psychiatrist (Cheadle) to get a dog. Solo meets his love interest (Collins), whom he assumes to be a dog owner when meeting her at a dog play park, but dog problems stand in their way.

==Cast==
;Main cast
* Giovanni Ribisi as Solo Harrington
* Lynn Collins as Lola
* Scott Caan as Casper
* Kevin Corrigan as Benny

;Supporting cast
* Mena Suvari as Jules
* Sarah Shahi as Candy
* Tito Ortiz as Frank
* Kimo Leopoldo as Ted
* Brian Goodman as Joe the Guard
* Joanna Krupa as Taffy
* Med Abrous as Brad
* Melissa Keller as Escalator Girl
* Jimmy the Dog as Spot
* Don Cheadle as Dr. Nourmand Steve Jones as Himself

==Reception==
The Dog Problem was screened at the 2006 Toronto Film Festival.  Erik Childress of eFilmCritic.com says it is, "A romantic comedy which doesnt force its charm down your throat and instead allows the viewer in to peek around its quirks and discover the true heart beating within." 

==Soundtrack==
The film features a score by Mark Mothersbaugh, and the music used during the title sequence is based on the Devo song "Gut Feeling".

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 


 