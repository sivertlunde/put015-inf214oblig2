The Open Door (film)
 
 
{{Infobox film
| name           = The Open Door
| image          = 
| caption        = 
| director       = Doc Duhame
| producer       = 
| screenwriter   = Doc Duhame
| starring       = Catherine Munden Sarah Christine Smith Ryan Doom
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
}}
The Open Door is a 2008 Australian horror/thriller film, directed by Doc Duhame and starring Catherine Munden, Sarah Christine Smith, and Ryan Doom. Mundens character finds her vengeful wishes toward people coming true.

==Plot==
Anjelica sits at home, angry at being grounded from going to a hip party. She tunes into a legendary pirate radio broadcast, hosted by a strange figure known as the Oracle, that only appears on the nights of the full moon. Angelica wishes ill upon her boyfriend and friends at the party along with her parents, and soon, strange sights and sounds begin, and even stranger things happen to the people around her.

==Cast==
* Catherine Munden - Angelica (as Catherine Georges)
* Sarah Christine Smith - Staci
* Ryan Doom - Owen
* Daniel Booko - Spike
* Mike Dunay - Brad
* Guy Wilson - Jerry
* Jacob Head - Jimmy
* Ian Kitzmiller - Scott
* Jessica Anne Osekowsky - Heather
* Kate Enggren - Angelicas Mom
* Clint Carmichael - Angelicas Dad

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 