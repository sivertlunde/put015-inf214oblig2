Villa for Sale
{{Infobox film
| name           = Villa for Sale
| image          = 
| image_size     = 
| caption        = 
| director       = Géza von Cziffra
| producer       = Mihály Körner
| writer         = Imre Harmath   László Vadnay   Géza von Cziffra 
| narrator       = 
| starring       = Ernő Verebes Ida Turay Gyula Kabos   Lili Berky
| music          = Pál Gyöngy 
| editing        = József Szilas
| cinematography = István Eiben 
| studio         = Hunnia Filmgyár
| distributor    = 
| released       = 10 April 1935
| runtime        = 83 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Hungarian comedy film directed by Géza von Cziffra and starring Ernő Verebes, Ida Turay and Gyula Kabos. After he goes away on holiday, a wealthy mans servant accidentally puts his villa up for sale.

==Cast==
* Ernő Verebes - Hódy György 
* Ida Turay - Anni 
* Gyula Kabos - Buckó József 
* Lili Berky - Tóni néni 
* Rózsi Csikós - Teri 
* Gyula Gózon - Rizling, Buckó barátja 
* István Somló - Betörõ 
* Sándor Pethes - Dani, a betörõ barátja 
* Gusztáv Pártos - Bedõ úr 
* Tivadar Bilicsi - Gereblyei Tivadar 
* László Keleti - Dadogó vevõ  Gyula Justh - János, az inas

==External links==
* 

 

 
 
 
 
 