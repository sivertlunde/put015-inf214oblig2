Popcorn (1991 film)
{{Infobox Film
| name           = Popcorn
| caption        = 
| image          = Popcorn FilmPoster.jpeg
| director       = Mark Herrier Alan Ormsby (uncredited)
| producer       = Ashok Amritraj Howard Hurst Torben Johnke Sophie Hurst Bob Clark (uncredited) Tod Hackett (screenplay) Tony Roberts Ray Walston Derek Rydall
| music          = Paul Zaza
| cinematography = Ronnie Taylor
| editing        = Stan Cole
| distributor    = Studio Three Film Corporation Europa Home Vídeo e Penthouse Films (VHS/Brasil)
| released       =   }}
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $4,205,000 (USA) 
}}
Popcorn is a 1991 American horror film directed by Mark Herrier and written by Alan Ormsby.

== Plot ==
 College film student and aspiring film writer Maggie Butler (Jill Schoelen) has been having recurring dreams of a young girl named Sarah who is caught in a fire and being chased by a strange man who is trying to kill her. She records what she remembers on an audio tape and plans on making the story into a film. Maggie lives with her mother Suzanne (Dee Wallace Stone), who has been receiving strange, demonic prank calls recently. On her way to class, a boy she has been seeing named Mark (Derek Rydall) embraces her and tries to get her go to his place. Maggie shuns his advances, explaining that she cant be distracted from writing her script.
 Tony Roberts) films within a film. Muir (2011), p. 170-172  Mosquito is a 3D film, The Attack of the Amazing Electrified Man was released with an accompanying "Shock-o-Scope" gimmick (electrical "buzzers" in seats), and The Stench is a Japanese film released in Odorama. Muir (2011), p. 170-172 

As they go through Dr. Mnesynes trunks of equipment, they find a short cult film called Possessor, that greatly resembles Maggies dreams. Davis informs them that back in the 1970s the films creator, Lanyard Gates, went on to kill his own family on stage in the live final scene of the film before setting the theater on fire, trapping the audience inside. Maggie soon becomes obsessed with the film and trying to figure out why she has been dreaming about it. When she asks her mother if she has heard of Gates or Possessor, Suzanne becomes uncomfortable; she urges Maggie to quit the festival and wants them to go away together. After Maggie goes to bed, Suzanne receives another prank call from someone she believes to be Gates. He tells her to meet him at Dreamland so they can talk and advises her to bring a gun.

On the night of the festival, Maggie is working the box office when Mark arrives with another girl, Joy (Karen Witter). Later, a man buys a ticket and calls Maggie "Sarah" before he walks away.  Maggie chases after him and asks her classmate Tina (Freddie Marie Simpson) to watch the box office for her. Unable to find the man in the theater, Maggie goes up to the projection booth to tell Toby that she thinks she saw Gates. Toby decides to check the theater himself and leaves Maggie to run the projection booth. Meanwhile, Mark has left his date to sneak up on Maggie to scare her and Davis is busy preparing the giant mosquito model to fly across the theater from backstage that he operates with a remote control. Once the mosquito is released above the audience, a figure is shown up in the catwalks above the professor with another remote control. Davis then notices that his controller is no longer working. The mosquito model turns around and zooms back down the track to the professor, stabbing him in the chest with the giant stinger. The scene cuts to a laboratory of sorts, where a figure is seen making a mask of the dead professors face.

Meanwhile, Maggie is trying to explain the story of Gates to Mark and that she thinks he is there when Toby comes back to the booth upset and accuses them of playing a joke on him by locking him out of the theater. The second film starts and we see Bud (Malcolm Danare), who is wheelchair-bound, preparing the seat shockers from a control panel above the audience. Maggie makes her way back to the box office to an annoyed Tina, who starts looking for Davis.  She asks Bud if he has seen him, but he says he hasnt and that he is having trouble with the control panel, would she send Davis down there to help him. Maggie is then shown listening to her audio recordings when the tape is cut off with a message from Gates. As she tries to exit the box office, she opens the door on Mark, knocking him down and unknowingly smashing the recorder in the process. Maggie tries to prove to him that Gates is really there when she realizes she accidentally destroyed the tape. They go looking for Tina who probably saw Gates while Maggie was away.

Tina finds Davis securing the mosquito on the catwalk and goes up to meet him, where it is revealed that they have been having an affair. She is unaware that he is the killer posing as the professor, and he strangles her with a rope. Mark and Maggie find her shortly thereafter, but the killer manipulates her voice and body in the darkness and the fact that she is dead goes unnoticed.

We then see a figure enter the control booth who Bud thinks is Tina. He is then strapped into a makeshift electric chair that is set to go off when a series of lights comes on. Bud frantically tries to free himself as the light sequence begins. As he is electrocuted, the theater loses power. Meanwhile, Maggie and Mark continue searching the theater with the help of Cheryl (Kelly Jo Minter). Mark falls down the stairs and Maggie leaves him with Cheryl while she finds her way to Bud, only to find him dead. As she approaches his body, he turns around and its Gates. Maggie screams and runs away, having flashbacks of Possessor. It is then that she realizes that she is Sarah Gates, Lanyard is her father, and Suzanne is not her mother but her aunt who saved her from being killed so long ago. She runs into Toby and tells him everything she remembers, believing that Gates has returned for her. Muir (2011), p. 170-172  She and Toby enter the basement to find the circuit breakers and Toby falls, disappearing into the darkness. With only a flashlight, Maggie hears odd noises and thinks she sees Davis and Tina, but runs into Gates. The power comes back on and Maggie finds herself in the killers lair, strapped into the chair he uses to make facial masks of his victims.

The killer is revealed to be Toby, who was badly burned as a child after attending the showing of Possessor with his mother, who was a member of Gates film cult and was killed in the fire.  He holds Maggie and Suzanne responsible for his injuries and the loss of his mother, and plans to exact his revenge on them by re-enacting the final scene of Possessor onstage that evening, only with the ending that Gates had originally intended. He then wheels out Suzanne whom he has cast full-body, pointing her gun.

Meanwhile, Cheryl and Joanie (Ivette Soler) tend to Marks injuries as they are met with an upset Joy, who tells Mark she saw Maggie and Toby together and that they went back to Tobys place. Mark leaves to find them and Joanie realizes she is late for her Odorama cue and goes to join Leon (Elliott Hurst) to activate the odor pellets during the third film. Leon leaves to go to the bathroom and is met by a mirror image of himself at the next urinal. After urinating on him, the doppelganger attacks Leon and locks him in one of the stalls and drops some type of material into the toilet that creates a thick smoke and Leon passes out. Soon after, there is an explosion from the stall and Leon is killed. Toby returns to the booth dressed as Leon with the plan to stab Joanie from behind, but spares her when she mentions her unrequited love for Toby. Shocked by this, Toby gets upset and storms out to continue setting up for the final scene of Possessor.

Mark arrives at Tobys apartment to find his landlord evicting him and throwing his personal belongings outside. His walls are plastered with articles concerning the incident, pictures of his facial reconstruction, and pictures of Maggie with scissors through her eyes. Realizing she is in real danger, Mark rushes back to the theater but the front doors are locked. He scales the building, climbs through a window on the balcony level and finds the final scene of Possessor playing on stage. Maggie has been drugged and placed in a metal dress so she has no mobility except for her head. She pleads to the audience to save her, but they believe this is all part of the show. Mark uses his belt on the mosquito track and zip-lines onto the stage, causing the secured mosquito to unlatch and swing out across the stage. The stinger stabs Toby in the chest, killing him. Mark releases Maggie and Suzanne as the crowd erupts in applause.

== Cast ==
* Jill Schoelen as Maggie
* Tom Villard as Toby
* Dee Wallace Stone as Suzanne
* Derek Rydall as Mark
* Malcolm Danare as Bud
* Kelly Jo Minter as Cheryl
* Ivette Soler as Joanie
* Elliott Hurst as Leon
* Freddie Marie Simpson as Tina Tony Roberts as Mr. Davis
* Ray Walston as Dr. Mnesyne

== Production ==
Popcorn was filmed entirely in Kingston, Jamaica. Alan Ormsby was originally the films director.   Ormsby was replaced by Porkys actor Mark Herrier a few weeks into filming.     The original lead Amy ONeill was replaced by Jill Schoelen at this time as well. 

Ormsby is credited with directing all three of the main films within a film, while Herrier is credited with filming the present-day portions of the film. Muir (2011), p. 170-172 

== Analysis ==
 The Last The Guardian The Crush The Temp (1993), Hideaway (film)|Hideaway (1995), and Scream (1996 film)|Scream (1996). Muir (2011), p. 10-11  He believes this trend was a result of the studio desire for generic, wide-appeal films. Muir (2011), p. 10-11 

Muir believes the film itself was part of another trend of the time. Horror films which were both postmodernist films and self-reflective. Popcorn took inspiration from the history of the horror films, from the 1950s onwards. While Wes Cravens New Nightmare (1994) and In the Mouth of Madness (1995) used metafiction as one of their themes. Muir (2011), p. 10-11 
 Jack Arnold. Nuclear weapons testing has caused desert mosquitoes to grow into giant monsters, in a plot resembling Them! (1954) and The Deadly Mantis (1957). The film includes stock characters and situations, such as a dedicated lady scientist and the military insisting on using a nuclear weapon to annihilate the monster. Muir (2011), p. 170-172  The gimmick accompanying Mosquito is a life-sized version of the giant mosquito which slides down a rope above the heads of the film audience. This is a tribute to Emergo, the Castle-devised gimmick accompanying House on Haunted Hill (1959). The original gimmick featured a glowing skeleton sliding down a rope. Muir (2011), p. 170-172  The title of The Attack of the Amazing Electrified Man seems to be a homage to The Amazing Colossal Man (1957), while the visual style of this film is similar to the works of William Cameron Menzies. It includes influences from German Expressionism, with "exaggerated shadows and menacing low-angles". Muir (2011), p. 170-172  The accompanying gimmick, "Shock-o-Scope", seems to be a rename of Percepto, the electric gimmick which accompanied The Tingler (1959). Muir (2011), p. 170-172  The Stench is fashioned after Japanese films, imported and dubbed for the American market. Its accompanying gimmick is an obvious variation of Smell-O-Vision, the gimmick used in Scent of Mystery (1960). Muir (2011), p. 170-172  Stranger than them is the Possessor. It features extreme close-ups, and functions as a mix between a snuff film and a product of Psychedelia. Its protagonist Lanyard Gates has similarities to cult leader Charles Manson. Muir (2011), p. 170-172 

The frame story is instead a rather typical slasher film. The killer impersonates his victims through use of masks, and his goal is the performance of a snuff-show in front of a live audience. His motivation lies in a crime of the past which scarred him for life. Maggie serves at the final girl of the film, accompanied by a heroic boyfriend. As to the identity of the killer, the film employs a suitable red herring for misdirection. Muir (2011), p. 170-172  Muir observes, however, that the film does not use slasher films themselves as part of its self-reflecting depiction of the horror genre. The characters dont seem aware of the relevant tropes, nor do they seem aware of their presence in a slasher film-like situation. Unlike their counterparts in Scream and I Know What You Did Last Summer (1997). Muir (2011), p. 170-172 
 marquee fall on the ground and in their place appears a new sign: Possessor. Actually no character in this film, including the killer has the ability to do something like this. Muir (2011), p. 170-172 

== Release ==

=== Box Office ===
The film was not a box office success. 

=== Home video ===
Popcorn was initially released on home video in June 1991.   Variety (magazine)|Variety reported in 1993 that home video sales equaled $2,043,179. 

Elite Entertainment released a DVD edition of Popcorn in 2001. Special features include theatrical trailers, TV spots and promotional footage. The DVD was discontinued as of January 4, 2010. 

A domestic Blu-ray release is planned for release through Synapse Films.

== Reception ==
 
  of The New York Times called it "the best spoof of its kind since Alligator."   Kevin Thomas of the Los Angeles Times called it an "ingenious and spoofy little shocker".   Owen Gleiberman of Entertainment Weekly rated it B and wrote, "Though it isnt even trying to scare you, this is a very nifty black-comic horror movie, one of the rare entries in the genre with some genuine wit and affection."   Richard Harrington of The Washington Post wrote that it "has several good ideas that, unfortunately, go unrealized."   Stephen Hunter of The Baltimore Sun wrote, "Popcorn isnt too clever by half, but only by seven-sixteenths. Its so busy being droll and ironic it forgets to be any good."   Chris Hicks of the Deseret News wrote, "On the whole, "Popcorn" is so amateurish in its  development, with pseudo-hip dialogue that drops movie references every few  lines, it winds up being neither scary nor funny."   Gary Thompson of the Philadelphia Daily News wrote that the film spoofs were inspired, but the rest of the film is much worse.   Reviewing the 2001 DVD release, Adam Tyner of DVD Talk called it "a wildly entertaining movie",  and Patrick Naugle of DVD Verdict called it "a fun little flick." 

== Sources ==
*  
*  
*  

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 