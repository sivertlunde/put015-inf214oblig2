Lalanam
{{Infobox film
| name           = Lalanam
| image          =
| caption        =
| director       = Chandrasekharan
| producer       =
| writer         =
| screenplay     = Innocent Siddique Siddique
| music          = S. P. Venkatesh
| cinematography =
| editing        = G Murali
| studio         = Thandu Films
| distributor    = Thandu Films
| released       =  
| country        = India Malayalam
}}
 1996 Cinema Indian Malayalam Malayalam film, Innocent and Siddique in lead roles. The film had musical score by S. P. Venkatesh.   

==Cast==
*Sukumari
*Jagathy Sreekumar Innocent
*Siddique Siddique
*Baby Shamili
*N. F. Varghese
*Shanthi Krishna
*Vinaya Prasad

==Soundtrack==
The music was composed by S. P. Venkatesh and lyrics was written by Gireesh Puthenchery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Manjakkiliye || KS Chithra || Gireesh Puthenchery || 
|-
| 2 || Manthravaadiyaay || K. J. Yesudas, KS Chithra || Gireesh Puthenchery || 
|-
| 3 || Snehalaalanam || K. J. Yesudas || Gireesh Puthenchery || 
|}

==References==
 

==External links==
*  

 
 
 

 