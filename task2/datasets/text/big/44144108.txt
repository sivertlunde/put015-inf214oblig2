Veruthe Oru Pinakkam
{{Infobox film 
| name           = Veruthe Oru Pinakkam
| image          =
| caption        =
| director       = Sathyan Anthikkad
| producer       =
| writer         = Dr. Balakrishnan
| screenplay     = Dr. Balakrishnan
| starring       = Nedumudi Venu Poornima Jayaram
| music          = Raveendran
| cinematography = Anandakkuttan
| editing        = G Venkittaraman
| studio         = AG Films
| distributor    = AG Films
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, directed by Sathyan Anthikkad. The film stars Nedumudi Venu and Poornima Jayaram in lead roles. The film had musical score by Raveendran.   

==Cast==
*Nedumudi Venu
*Poornima Jayaram

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Sathyan Anthikkad. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Manasse Ninte || K. J. Yesudas || Sathyan Anthikkad || 
|-
| 2 || Ragavum Thalavum || K. J. Yesudas, S Janaki || Sathyan Anthikkad || 
|-
| 3 || Veruthe Veruthe Oru Pinakkam || Raveendran || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 

 