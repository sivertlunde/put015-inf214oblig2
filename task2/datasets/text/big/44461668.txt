Exclusive Story
{{Infobox film
| name           = Exclusive Story
| image          = Exclusive Story poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = George B. Seitz
| producer       = Lucien Hubbard
| screenplay     = Michael Fessier 
| story          = Martin Mooney
| starring       = Franchot Tone Madge Evans Stuart Erwin Joseph Calleia Robert Barrat J. Farrell MacDonald Edward Ward
| cinematography = Lester White 
| editing        = Conrad A. Nervig
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Exclusive Story is a 1936 American drama film directed by George B. Seitz and written by Michael Fessier. The film stars Franchot Tone, Madge Evans, Stuart Erwin, Joseph Calleia, Robert Barrat and J. Farrell MacDonald. The film was released on January 17, 1936, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Franchot Tone as Dick Barton
*Madge Evans as Ann Devlin 
*Stuart Erwin as Timothy Aloysius Higgins
*Joseph Calleia as Ace Acello
*Robert Barrat as Werther
*J. Farrell MacDonald as Michael Devlin Louise Henry as Tess Graham
*Margaret Irving as Mrs. Higgins
*Wade Boteler as ONeil
*Charles Trowbridge as James Witherspoon Sr.
*William "Bill" Henry as James Witherspoon Jr.
*Raymond Hatton as City Editor
*J. Carrol Naish as Comos 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 