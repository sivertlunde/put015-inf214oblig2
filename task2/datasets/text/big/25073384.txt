The Grace Card
{{Infobox film
| name           = The Grace Card
| image          = The Grace Card.jpg
| alt            = 
| caption        = 
| director       = David Evans
| producer       = Howard Klausner John R. Saunders
| writer         = Howard Klausner
| starring       = Louis Gossett, Jr. Michael Joiner Michael Higgenbottom Stephen Dervan
| music          = 
| cinematography = John Paul Clark
| editing        = Mark Pruett
| studio         = GraceWorks Pictures Provident Films Affirm Films Samuel Goldwyn Films
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $200,000
| gross          = $2,430,735 
}}
The Grace Card is a 2010 Christian drama film directed by David G. Evans. It intends to illustrate the everyday opportunities that people have to rebuild relationships and heal deep wounds by extending and receiving God’s grace. The film stars Louis Gossett, Jr., Michael Higgenbottom and Michael Joiner.    It was released on February 25, 2011 to 363 theaters, grossing $1,010,299 on opening weekend.

==Plot==
When Mac McDonald (Michael Joiner) loses his son in an accident, the ensuing 17 years of bitterness and pain erodes his love for his family and leaves him angry with almost everyone, including God. Macs rage damages his career in the police department, and his household is as frightening as anything he encounters on the streets of Memphis. Money is tight, arguments with his wife are common, and his surviving son Blake is hanging with the wrong crowd and in danger of failing school.

Things become heated when Mac is partnered with Sam Wright (Mike Higgenbottom), a rising star on the force who happens to be a part-time pastor and a family man. Sam never expected to be a police officer. He feels called to be a minister like his grandfather. In addition to leading a small, start-up church, Sam works as a police officer to provide for his family. When he gets promoted to Sergeant, however, Sam starts questioning if his true calling might be police work.

Can Sam and Mac somehow join forces or is it nearly impossible for either of them to look past their differences, especially their Race (classification of humans)|race?

==Production== Sherwood Baptist guys have opened up a door for all of us, proving that faith-based features can stand up in the marketplace... Maybe it’s just because so few films coming out of Hollywood reflect the values we   hold dear."   
 Cordova was the films "sponsoring church." Church volunteers worked in the catering, wardrobe, hair and makeup departments, in addition to the professional filmmakers who handled the films technical aspects. Director David Evans said about 90 percent of the 40 full-time crew members were Mid-Southerners, as were almost all the 100-plus supporting actors and Extra (actor)|extras.  They volunteered more than 10,000 hours, working day and night to shoot the film in 28 days.    The films budget was only $200,000.

==Release==
On its opening day of wide release, The Grace Card grossed $360,000, and its opening weekend was $1,010,299.  It was released to 352 theaters, which gives it a solid $2,870 per-theater-average.

===Reception===
Critical reviews for the film has been mixed. Rotten Tomatoes currently gives the film a 35% approval rating, based on 26 reviews.  Critics listed on Metacritic have given The Grace Card a 43 out of 100 score, an average of 14 reviews currently available.  Critics from Variety (magazine)|Variety, The Hollywood Reporter and The New York Times gave the film a positive review, while critics for the New York Post, Orlando Sentinel and New York Daily News were not as receptive.

Steve Persall of the St. Petersburg Times said in a positive review, "This is a solid, sincere affirmation of faith and forgiveness. Praise the Lord, and pass the popcorn."  Mike Hale of The New York Times said "Responses to religious films are bound to be personal, so at the risk of sounding patronizing, Ill say that my main reaction to The Grace Card was one of pleasant surprise at its competence."  Orlando Sentinels Roger Moore said, "The eggshells the screenwriter and director walk on distance the story from the reality it aims to imitate. And that robs this tale of loss, grief and redemption of its punch." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 