Septien
 

{{Infobox film
| name           = Septien
| image          = 
| alt            = 
| caption        = 
| director       = Michael Tully
| producer       = 
| writer         = Michael Tully Onur Tukel Robert Longstreet
| starring       = Michael Tully Onur Tukel Robert Longstreet Rachel Korine
| music          = Michael Montes
| cinematography = Jeremy Saulnier
| editing        = Marc Vives
| studio         = 
| distributor    = IFC Films
| released       = 
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 independent film directed by Michael Tully. The films narrative concerns the return of bearded athlete Cornelius Rawlings to his familys Tennessee farm eighteen years after he disappeared, and the strange new life he forms with his brothers Ezra and Amos. 

==Cast==

*Michael Tully as Cornelius Rawlings 
*Onur Tukel as Amos Rawlings 
*Robert Longstreet as Ezra Rawlings 
*Rachel Korine as Savannah 

==Release==
Septien was premiered at the 2011 Sundance Film Festival, and subsequently screened within such festivals as Buenos Aires International Festival of Independent Cinema|BAFICI, International Film Festival Rotterdam, South by Southwest, Sarasota Film Festival, and Maryland Film Festival. 

It was acquired for distribution by IFC Films.

==External links==
*  
*  
*  

 
 
 

 