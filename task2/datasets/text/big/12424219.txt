Displaced (2006 film)
{{Infobox film
| name           = Displaced
| image          = Displaced-cover.jpg
| caption        = Displaced US DVD artwork
| director       = Martin Holland
| producer       = Mark Strange Chee Keong Cheung(executive) Mike Leeder (executive)  Malcolm Hankey (co)
| writer         = Screenplay: Martin Holland Story: Carol Anne Strange
| starring       = Mark Strange Ian McKellen Stephanie Fend Cathy Miller Graham Brownsmith Stephanie Renke Malcolm Hankey
| music          = Paul King
| editing        = Martin Holland
| studio         = Skylandian Entertainment
| distributor    = Silverline Entertainment
| release        =  
| runtime        = 100 minutes
| country        = United Kingdom English
}}
 2006 Cinema British feature film produced by Skylandian Pictures. Produced by Mark Strange and directed by Martin Holland, the film took six years to make and secured a US distribution deal with Silverline Entertainment at the end of 2005.

==Plot==
Stel, a humanoid alien (Mark Strange), teams up with a British soldier John Marrettie (Malcolm Hankey). They are searching for a top secret file which contains information about advanced energy production, captured alien spacecraft and the extraterrestrial pilots. The missing include Stel’s father, Arakawa, was shot down and imprisoned by Core – a paramilitary group.

The Displaced file is being held by a renegade special forces’ leader, Wilson (Graham Brownsmith) who plans to sell the information on the black market to the highest bidder. Marrettie is forced to help Stel in the search to locate the file.

==Cast==
*Mark Strange as Stel
*Malcolm Hankey as John Marrettie

==External links==
*  

 
   
   
   
   
   


 