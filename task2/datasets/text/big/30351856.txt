The Drums of Jeopardy (1931 film)
{{Infobox film
| name           = The Drums of Jeopardy
| caption        =
| image	=	The Drums of Jeopardy FilmPoster.jpeg
| director       = George B. Seitz
| producer       = Phil Goldstone (producer) The Drums of Jeopardy) Florence Ryerson (writer)
| starring       = Warner Oland as Boris Karlov
| music          = Val Burton Arthur Reed Otto Ludwig
| distributor    = Tiffany Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Drums of Jeopardy is a 1931 American film directed by George B. Seitz.

The name "Boris Karlov" was used from MacGraths book and for the 1922 Broadway play, but by 1923 with actor Boris Karloff using the similar sounding variation, the film version renamed the character, played by Wallace Beery,  "Gregor Karlov". In the 1931 film version, however, with Warner Oland playing the character, the mad scientists name is restored to "Boris Karlov," less than a  year before Frankenstein would make Boris Karloff a household word for generations.

== Cast ==
*Warner Oland as Dr. Boris Karlov
*June Collyer as Kitty Conover
*Lloyd Hughes as Prince Nicholas Petroff
*Clara Blandick as Abbie Krantz
*Hale Hamilton as Martin Kent
*Wallace MacDonald as Prince Gregor Petroff
*George Fawcett as General Petroff
*Florence Lake as Anya Karlov
*Mischa Auer as Peter
*Ernest Hilliard as Prince Ivan Petroff

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 