Dr. Jekyll and Mr. Hyde (1920 film)
 
 
{{Infobox film
| name           = Dr. Jekyll and Mr. Hyde
| image          = Dr Jekyll and Mr Hyde 1920 poster.jpg
| caption        = A promotional film poster
| director       = John S. Robertson
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Thomas Russell Sullivan Clara Beranger
| based on       =   Charles W. Lane Nita Naldi
| music          = 
| cinematography = Roy F. Overbaugh
| editing        = 
| studio         = Famous Players-Lasky/Artcraft Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 67 minutes (80 minutes)
| country        = United States
| awards         = 
| language       = Silent film   English intertitles
| budget         =  
}}
  horror silent film, produced by Famous Players-Lasky and released through Paramount Pictures|Paramount/Artcraft. The film is based upon Robert Louis Stevensons novella The Strange Case of Dr Jekyll and Mr Hyde and starring actor John Barrymore.
 public domain.

This story of split personality has Dr. Jekyll a kind and charitable man who believes that everyone has two sides, one good and one evil. Using a potion, his personalities are split, creating havoc.  

== Plot ==
Henry Jekyll (John Barrymore) is a doctor of medicine, but he is also an "idealist, philanthropist." When he is not treating the poor in his free clinic, he is in his laboratory experimenting. Sir George Carew (Brandon Hurst), the father of his fiancée, Millicent (Martha Mansfield), is "piqued" by Dr. Jekyll. "No man could be as good as he looks," Carew says.
  as a dance hall owner]]
Following dinner one night, Carew taunts Dr. Jekyll in front of their friends, Edward Enfield (Cecil Clovelly), Dr. Lanyon (Charles Lane) and Utterson (J. Malcolm Dunn) proclaiming "In devoting yourself to others, Jekyll, arent you neglecting the development of your own life?"
"Isnt it by serving others that one develops oneself," Jekyll replies.
"Which self? Man has two - as he has two hands. Because I use my right hand, should I never use my left? Your really strong man fears nothing. It is the weak one who is afraid of experience. A man cannot destroy the savage in him by denying its impulses. The only way to get rid of a temptation is to yield to it. With your youth, you should live - as I have lived. I have memories. What will you have at my age?"

And thus the seed is sown, and Jekyll begins his experiments. As he observes, "Wouldnt it be marvellous if the two natures in man could be separated - housed in different bodies? Think what it would mean to yield to every evil impulse, yet leave the soul untouched!"
Finally, Jekyll develops a potion that turns him into a hideously evil creature that he calls Edward Hyde. As this creature, he is not recognizable as Dr. Jekyll, and, so, to facilitate the comings and goings of Hyde, he tells his servant, Poole (George Stevens), that Hyde is to have "full authority and liberty about the house."

Jekyll thus begins to live his double life. Hyde sets up a room in one of the seediest parts of London. He brings in a girl from the dance hall, Gina (Nita Naldi), to live with him there and frequents opium dens, dance halls, and bars - any place that satisfies his evil desires.
Although Jekyll has developed a potion that will also return him to his original appearance and character as Dr. Jekyll, each time he takes the potion to become Edward Hyde, he worsens. He not only looks more evil, he becomes more evil, as well.

Millicent Carew is worried about the absence of her fiancé, so Sir George goes to call on Jekyll to see what is the matter. Although Jekyll is not home when he calls, Sir George encounters Hyde in the street just as he knocks a small boy to the ground injuring him. To make recompense for his actions, he goes and gets a check which he returns to the boys father. Carew notices that the check has been signed by Dr. Jekyll. He confronts Poole who tells him the story of Edward Hyde.

In the meantime, Hyde/Jekyll has returned to the lab and, after drinking the potion, returns to his original self. Sir George finds him in the lab and demands to know his relationship with "a vile thing like Hyde?"

"What right have you to question me - you who first tempted me?" says Jekyll.
Sir George angrily retorts that unless Jekyll is forthcoming with an explanation, he must object to his marriage to Millicent. This angers Jekyll to the point that he suddenly becomes Hyde, right in front of Sir Georges eyes, without benefit of the potion. Sir George runs into the courtyard where Hyde catches him and clubs him to death with his walking stick.
Hyde runs to his apartment and destroys any evidence that may link him to Jekyll. He eludes the police by only minutes and returns to his lab where he is able to drink the potion that restores him as Jekyll.

In the ensuing days, as Millicent grieves, Jekyll is tortured by his misdeeds. Soon, the drug needed to make the potion that will return him as Dr. Jekyll is depleted and cannot be found in all of London. Jekyll stays locked up in his lab fearing he may become Hyde at any moment.
Millicent finally goes to see him, but just as she is about to enter the lab, he begins to transform into Hyde. Jekyll consumes the poison in the ring he took from the Italian dancer before he opens the door, fully transformed into Hyde. He lets her in, locks the door and grabs her in his arms. Suddenly, he starts convulsing. Millicent runs from the lab and when Lanyon comes in, he finds Hyde sitting in a chair, having just died, and his appearance returned to that of Dr. Jekyll. He discerns that Jekyll committed suicide, and calls the others (Poole, Utterson and Millicent) in, but declares to them that Hyde has killed Dr. Jekyll. In the final shot, Millicent is grieving next to the body of Dr. Jekyll.

== Cast ==
* John Barrymore as Dr. Henry Jekyll / Mr. Edward Hyde
* Brandon Hurst as Sir George Carew
* Martha Mansfield as Millicent Carew, Sir Georges daughter
* Charles Willis Lane as Dr. Richard Lanyon
* George Stevens as Poole, Jekylls butler
* Nita Naldi as Miss Gina, Italian artist
* Louis Wolheim as Dance Hall proprietor (*uncredited)
* Cecil Clovelly as Edward Enfield
* J. Malcolm Dunn as John Utterson

=== Uncredited ===
* Alma Aiken as Extra (*uncredited)  
* Edgard Varèse as Policeman (uncredited) Julia Hurley San Francisco Chronicle, Sunday April 25, 1920; CINEMA, "John Barrymore and Julia Hurley (Imperial)", caption under pic,
 for Dr. Jekyll & Mr. Hyde (courtesy of ebay for scanned newspaper article)  as Hydes Old Landlady (*uncredited)

== Production ==
 
* The early part of Jekylls initial transformation into Hyde was achieved with no makeup, instead relying solely on Barrymores ability to contort his face. 
* In one scene, as Hyde reverts to Jekyll, one of Hydes prosthetic fingers can be seen to fly across the screen, having been shaken loose by Barrymores convulsions. stage version by Thomas Russell Sullivan starring Richard Mansfield. This 1920 film version used the plays concept of Jekyll being engaged to Carews daughter, and Hyde beginning a romance with a dance-hall girl.

== Appearances in other films==
* The films public domain status makes its use common in documentaries about silent films and horror films.
* Characters played by Tim Daly and Lysette Anthony are depicted viewing a scene from this film in Dr. Jekyll and Ms. Hyde.
* In the seventh episode of the HBO television series Boardwalk Empire (Home (Boardwalk Empire)|Home, originally aired 31 October 2010), Lucy Danziger (Paz de la Huerta) views the film alone in a mostly-empty theater after entreating her lover, Nucky Thomson (Steve Buscemi) to go with her.
* In Queen (band)|Queens hit music video for "Under Pressure".
* Some clips were featured in Rob Zombies music video for "Dragula (song)|Dragula".

==See also==
* List of American films of 1920
*The House That Shadows Built (1931 promotional film by Paramount)

==References==
;Notes
 

;Bibliography
* {{Citation
  | title = The Overlook Film Encyclopedia
  | url = http://books.google.com/books?id=3-ehQgAACAAJ&dq=overlook+film+encyclopedia
  | editor-last = Hardy
  | editor-first = Phil
  | editor-link = Phil Hardy (journalist)
  | volume = 3
  | publisher = Overlook Press
  | year = 1995 
  | isbn=0-87951-624-0 }}

==External links==
 
*  
*  
*  
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 