Oka Radha Iddaru Krishnula Pelli
{{Infobox film
| name = Oka Radha Iddaru Krishnula Pelli
| image = 
| alt =  
| caption =
| director = G.Nageswara Reddy
| producer = DVV Danayya J Bhagavan
| writer = Janardhan Maharshi  (Story)  Marudhuri Raja (dialogues)  
| screenplay = G.Nageswara Reddy Srikanth   Prabhu Deva   Namitha Chakri
| cinematography = Bhoopati
| editing = Gowtam Raju
| studio = Sri Balaji Creations
| distributor = 
| released = 
| runtime =
| country = India
| language = Telugu
| budget =
| gross =
}}

Oka Radha Iddaru Krishnula Pelli   ( ) is a 2003 Telugu film directed by G.Nageswara Reddy and produced by DVV Danayya and J Bhagavan under Sri Balaji Creations. The film stars Meka Srikanth|Srikanth, Prabhu Deva and Namitha  in the lead roles.  The film was remade in Tamil as Enga Raasi Nalla Raasi.

==Cast== Srikanth
*Prabhu Deva
*Namitha
*Tanikella Bharani Chandra Mohan
*Chalapathi Rao Sunil
*Brahmanandam
*MS Narayana
*Jaya Prakash Reddy
*L.B. Sriram
*Kovai Sarala
*Delhi Rajeswari Melkote

==Music==
The music was composed by Chakri (music director)|Chakri. 
{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
! No. !! Song !!Lyrics!! Singers
|- 1 ||"Sriramachandra" ||Bhaskarabhatla,Ravi Varma ||Kousalya, Udit Narayan
|- 2 ||"Aaku Chakri (music director)|Chakri, Shreya Ghoshal
|- 3 ||"Nagunde Paidipalli Srinivas ||Kousalya, Shankar Mahadevan
|- 4 ||"Lavvudoma" Bhuvanachandra ||Kousalya
|- 5 ||"Chilaka Kandikonda ||Kumar Sanu,Shreya Ghoshal
|- 6 ||"Garam Garam " ||Kandikonda||Chakri,Kousalya
|}

==References==
 

==External links==
*  

 
 
 
 
 