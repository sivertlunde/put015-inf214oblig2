Casting By
{{Infobox film
| name           = Casting By
| image          = Casting By Film Theatrical One Sheet Poster.jpg
| image_size     = 
| border         = 
| alt            = Black and white picture of Marion Dougherty and George Roy Hill with the title Casting By and credits underneath
| caption        = Theatrical release poster Tom Donahue
| producer       = Kate Lacey Tom Donahue Joanna Colbert Ilan Arboleda
| starring       = Marion Dougherty Lynn Stalmaster Woody Allen Jeff Bridges Glenn Close Robert De Niro Richard Dreyfuss Robert Duvall Clint Eastwood Danny Glover Diane Lane Bette Midler Al Pacino Robert Redford Martin Scorsese John Travolta Jon Voight
| music          = Leigh Roberts
| cinematography = Peter Bolte
| editing        = Jill Schweitzer
| distributor    = Submarine Deluxe
| released =  
| runtime        = 89 minutes
| country        = United States
| language       = English
}}
 Tom Donahue.  It combines over 230 interviews, extensive archival footage, animated stills and documents to tell the untold tale of the Hollywood casting director.

The film had its international premiere at the Toronto International Film Festival (TIFF) and its US premiere at the New York Film Festival in 2012.  It was acquired by HBO Documentary Films in Toronto and had its US Broadcast premiere as part of the HBO Documentary Summer Series on August 5, 2013.  In September 2013, the US theatrical rights were acquired by Submarine Deluxe.   

==Synopsis==
The film is a celebration of the casting profession, highlighting its previously unsung role in film history while also serving as an elegy to the lost era of the New Hollywood.  It focuses on casting pioneer Marion Dougherty, an iconoclast whose exquisite taste, tenacity and gut instincts brought a new kind of actor to the screen that would mark the end of the old studio system and help to usher in this revolutionary new period.  The film draws a line through the last half century to show us the profession’s evolution from studio system type-casting to the rise of large ensemble films populated with unique and diverse casts.

In their July 25, 2013 cover story on the film ("Rise of the Casting Directors"), Back Stage wrote, "the film features what is arguably the greatest assemblage of talking-head star power in any documentary ever made." 

The interviewees include numerous Hollywood legends: Woody Allen, Ed Asner, Jeanine Basinger, Ned Beatty, Tony Bill, Peter Bogdanovich, Stephen Bowie, Jeff Bridges, Glenn Close, Ronny Cox, Robert De Niro, Richard Donner, Richard Dreyfuss, Robert Duvall, Clint Eastwood, Mel Gibson, Danny Glover, Taylor Hackford, Paul Haggis, Jerome Hellman, Buck Henry,  Arthur Hiller, Norman Jewison, Diane Lane, Ed Lauter, Norman Lear, John Lithgow, Gary Marsh, Paul Mazursky, Bette Midler, Al Pacino, David Picker, Robert Redford, James Rosin, John Sayles, Jerry Schatzberg, Martin Scorsese, Terry Semel, Ralph Senensky, James Sheldon, Cybill Shepherd, Susan Smith, Oliver Stone, Mel Stuart, John Travolta, Jon Voight, Paula Weinstein, Burt Young.

==Accolades/awards==
Casting By was named one of the top 5 documentaries in December 2013 by the National Board of Review of America in November 2013.  
As the society celebrated the Academy’s decision to create a casting directors branch for awards, Casting By received an Artios Award and a standing ovation for its pivotal role in spurring the Academy’s decision on the night of the 29th Annual Artios Awards.  
 CSA president Richard Hicks took the stage and recapped "a great year" for casting directors, including the achievement of "branch status" at the Academy, which he said was "a sign that perceptions are beginning to change -- we thank you, Academy." He then acknowledged Casting By, calling it "a beautiful gift" to the memory of its principal subject, the late Marion Dougherty, and all members of the profession. "If were lucky," he continued, "it might just get nominated for an Academy Award|Oscar."  

In February 2014, the National Alliance for Women in Media recognized Casting By as an outstanding program for its role in supporting women’s achievements in all facets of media and entertainment as the documentary earned a Gracie Award.  In July 2014, Casting By was nominated for Outstanding Arts and Culture Programming for the 35th Documentary Emmy Awards. 

===Woody Allens Open Letter===

Woody Allen wrote a rare, open letter to The Hollywood Reporter magazine a week after Casting By opening in New York on Nov. 1, 2012, and recognized his very own casting director, Juliet Taylor (who is one of Marion Dougherty’s protégées), for her tenacity and insight in her casting work for his films.   

==DVD/Blu Ray Release Information==

Casting By will be released on DVD/Blu Ray through First Run Features on September 16, 2014.    

==References==
 

==External links==
*  
*     
*  
*   at HBO

 
 
 
 
 
 
 
 