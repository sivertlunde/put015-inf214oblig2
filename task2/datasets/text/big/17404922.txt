Bright Star (film)
 
{{Infobox film
| name           = Bright Star
| image          = Bright star.jpg
| caption        = Theatrical release poster
| director       = Jane Campion
| producer       = Jan Chapman Caroline Hewitt
| writer         = Jane Campion Paul Schneider Kerry Fox Thomas Sangster
| music          =
| cinematography = Greig Fraser
| editing        = Alexandre de Franceschi
| studio         = BBC Films Screen Australia UK Film Council New South Wales Film and Television Office Pathé  Apparition (USA) Warner Bros. (UK/France)
| released       =  
| runtime        = 119 minutes 
| country        = United Kingdom Australia
| language       = English
| budget         = $8.5 million
| gross          = $14,350,945
}} 62nd Cannes Film Festival, and was first shown to the public on 15 May 2009.    The films title is a reference to a sonnet by Keats named "Bright star, would I were steadfast as thou art", which he wrote while he was with Brawne.

==Plot== Dilke family. Charles Brown Paul Schneider)—Keats friend, roommate, and associate in writing—occupying the other side.

Though Fannys flirtatious personality contrasts with Keats notably more aloof nature, she begins to pursue him after she has her siblings, Samuel and Toots, obtain his book of poetry "Endymion (poem)|Endymion". Despite her efforts to interact with the poet it is only after witnessing her grief for the loss of his brother Tom that Keats begins to open up to Fannys advances, when he spends Christmas with the Brawne family. After Keats begins to give poetry lessons to Fanny it becomes apparent that their attraction is mutual, however Fanny is nevertheless troubled in regard to Keats reluctance to pursue her, for which her mother (Kerry Fox) surmises, "Mr Keats knows he cannot like you, he has no living and no income."

It is only after Fanny receives a valentine from Brown that Keats passionately confronts them and wonders if they are lovers. Brown, who sent the valentine in jest, warns Keats of Fanny, claiming that she is a mere flirt playing a game. On the other hand, Fanny, hurt by the accusations and Keats lack of faith in her, ends their lessons and leaves. It is not until after the Dilkes move to Westminster that spring, leaving the Brawne family six months half rent in their home neighboring Brown, that Fanny and Keats resume their interaction and fall deeply in love. The relationship comes to an abrupt end, however, after Brown leaves for his summer rental with Keats, so that Keats may earn some money. Though Fanny is heartbroken, she is comforted by Keats love letters. When the men return in the autumn, Fannys mother shows concern, feeling that Fannys attachment with the poet will hinder her from being courted. Secretly, however, Fanny and Keats are engaged.

When Keats becomes ill the following winter, he spends several weeks recovering until spring, for which his friends begin a collection of funds so that he may spend the next winter in Italy where the climate is warmer. After impregnating the maid Abigail, however, Brown is unable to accompany him. Though Keats manages to find residence in London for the summer, he is taken in to live with the Brawne family following an incident in relation to his illness. It is here that, after his book sells with moderate success, Fannys mother gives Keats her blessing to marry Fanny once he returns from Italy. The night before Keats must leave for Italy he and Fanny say their tearful goodbye in privacy, and in February—while still in Italy—Keats dies of complications from tuberculosis, just as his brother Tom did earlier in the film.

In the last moments of the film Fanny cuts her hair in an act of mourning, dons black attire, and walks the snowy paths outside that Keats had walked many times in life. It is there that she recites the love sonnet he had written for her, "Bright Star", as she grieves the death of her lover.

==Cast==
* Ben Whishaw as John Keats. Keats was one of the key figures in the second generation of the Romantic movement despite the fact that his work had been in publication for only four years before his death. During his lifetime his poems were not generally well received by critics and at age 25 he died believing he was a failure. However, his reputation grew and he held significant posthumous influence on many later poets.
* Abbie Cornish as Fanny Brawne. Like the real life Fanny Brawne, Fanny in the film is a fiery and fashionable eighteen-year-old who spends her time creating dresses, hats, and various other garments. She is also something of a flirt and enjoys going to balls, inducing jealousy in Keats. Though the real life Fanny Brawne went on to marry and have children, she never parted with Keatss love letters. Paul Schneider as Charles Armitage Brown, Keats best friend.
* Kerry Fox as Fannys mother, a widow.
* Thomas Sangster as Samuel Brawne, Fannys brother.
* Antonia Campbell-Hughes as Abigail ODonaghue Brown, housemaid and mother of Charles Browns child.
* Claudie Blakley as Mrs Dilke
* Gerard Monaco as Charles Dilke
* Olly Alexander as Tom Keats, Keats brother
* Samuel Roukin as John Hamilton Reynolds Leigh Hunt Samuel Barnett as Joseph Severn

==Production==
In addition to "Bright Star" several other poems are recited in the film, including "La Belle Dame Sans Merci" and "Ode to a Nightingale". Both Campion and Whishaw completed extensive research in preparation for the film. Many of the lines in the script are taken directly from Keats letters.  Whishaw, as well, learned how to write with a quill and ink during filming. The letters that Fanny Brawne receives from Keats in the film were actually written by Whishaw in his own hand.

Janet Patterson, who has worked with Campion for over 20 years, served as both costume designer and production designer for the film. 

The Hyde House and Estate in Hyde, Bedfordshire, substituted for the Keats House in Hampstead. Campion decided that the Keats House (also known as Wentworth Place) was too small and "a little bit fusty".    Some filming also took place at Elstree Studios. 

==Critical reception==
The film garnered positive reviews from critics. Review aggregator Rotten Tomatoes reports that 82% out of 153 professional critics gave the film a positive review, with an average score of 7.2 and the site consensus being: "Jane Campions direction is as refined as her screenplay, and she gets the most out of her cast – especially Abbie Cornish – in this understated period drama."  SBS awarded the film five stars out of five, commenting that "If Campion intended to inspire an appreciation and rediscovery of Keats poetry, she has not only succeeded but herself created an artistic monument to his life, love, poetry and soul." Craig Mathieson, in the same review, hailed  Bright Star as "Jane Campion’s ... best work since The Piano, her epochal 1993 masterpiece."  

The film did not go unnoticed in the poetry world.  Poet and scholar Stanley Plumly, the author of Posthumous Keats: A Personal Biography (W.W. Norton, 2008), wrote of the films writing and direction: "Jane Campion has understood the richly figurative in Keats’ life without sacrificing the literal wealth of its texture. She has evoked the mystery of his genius without giving up the reality of its dailiness. ... Love, the poems, and Keats’s poorly diagnosed yet terminal illness all move in parallel, though in Campion’s film it is love — made brighter by the intensity of mortality — that defines her subject. But even passion here is understated, as it must have been in real life — given the conventions — for these two intense individuals. The much-reviewed scene in which the would-be lovers, in a bedroom, are speaking back and forth lines from Keats’s newly composed ballad La Belle Dame… surely qualifies as flesh-made-word love-making. The scene gorgeously represents what poetry as well as love are about — the spiritual inseparable from the carnal." 

==Box office==
Bright Star grossed $3,110,560 at the box office in Australia. 

==Awards==

===Won===
* British Independent Film Awards, Best Technical Achievement (for cinematography)
* Heartland Film Festival, Truly Moving Sound Award
* National Society of Film Critics, Best Supporting Actor

===Nominations===
* 82nd Academy Awards, Best Achievement in Costume Design
* British Academy of Film and Television Arts, Best Costume Design
* British Independent Film Awards, Best Director
* British Independent Film Awards, Best Actress
* British Independent Film Awards, Best Supporting Actress
* Cannes Film Festival, Golden Palm
* César Award for Best Foreign Film
* Chicago Film Critics Association Awards, Best Actress
* Chicago Film Critics Association Awards, Best Cinematography
* Critics Choice Award, Best Costume Design
* London Critics Circle Film Awards, British Film of the Year
* London Critics Circle Film Awards, Actress of the Year
* Satellite Awards, Best Motion Picture, Drama
* Satellite Awards, Best Director
* Satellite Awards, Best Actress in a Motion Picture, Drama
* Satellite Awards, Best Screenplay, Original

==Soundtrack== Amazon Digital) Mark Bradshaw with dialogue from the film voiced by Cornish and Whishaw.  

===Track listing===
# "Negative Capability" – 3:55 La Belle Dame Sans Merci" – 2:28
# "Return" – 0:58
# "Human Orchestra" – 1:48
# "Convulsion" – 0:52 Bright Star" – 1:49
# "Letters" – 3:49
# "Yearning" – 2:24
# "Ode to a Nightingale" – 5:24

==Book of Love Letters and Poems== Penguin and includes an introduction written by Campion.  The content of the book is as follows:

===Letters from John Keats to Fanny Brawne===
* I-IX: Shanklin, Winchester, Westminister
* X-XXXII: Wentworth Place
* XXXIII-XXXVII: Kentish Town – Preparing For Italy

===Poems===
* Bright star! would I were steadfast as thou art
* The Eve of St Agnes
* A Dream, after reading Dantes Episode of Paola and Francesca
* La Belle Dame sans Merci. A Ballad
* Ode to Psyche
* Ode on Melancholy
* Ode on Indolence
* Lamia
* The day is gone, and all its sweets are gone!
* What can I do to drive away
* I cry your mercy, pity, love – ay, love!
* To Fanny
* This living hand, now warm and capable

===Illustrations===
* Sketch of John Keats sleeping (28 January 1821) by Joseph Severn
* Silhouette of Fanny Brawne, after Augustin Edouart
* Facsimiles of Keatss handwriting, from his letters to Brawne

==References==
 

==External links==
*  
*  
*  
*   about John Keats, Fanny Brawne, and the poem "Bright Star" from Keats, the biography by Andrew Motion that inspired the film.

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 