The Years Between (film)
{{Infobox film
| name           = The Years Between
| image          = "The_Years_Between"_(1946_film).jpg
| caption        = Danish poster
| director       = Compton Bennett 
| producer       = Sydney Box Betty E. Box
| writer         = Muriel Box Sydney Box
| based on       = the play by Daphne Du Maurier
| starring       = Michael Redgrave Valerie Hobson Flora Robson 
| cinematography = Reginald H. Wyer
| music          = Benjamin Frankel
| editor         = Gordon Hales
| studio = Sydney Box Productions
| distribution   = General Film Distributors (GFD)
| released       = 8 July 1946	
| runtime        = 88 minutes
| country        = United Kingdom English
| gross = 
}}  The Years Between by Daphne du Maurier.

==Plot==
The film charts the homecoming of a British MI6 officer, who had been working with the French Resistance and was presumed dead. His wife is about to marry again, and to become an MP, and  all must now readjust to the new situation.  

==Cast==
* Michael Redgrave as Michael Wentworth  
* Valerie Hobson as Diana Wentworth  
* Flora Robson as Nanny  
* James McKechnie as Richard Llewellyn  
* Felix Aylmer as Sir Ernest Foster  
* Dulcie Gray as Jill   John Gilpin as Robin Wentworth  
* Edward Rigby as Postman  
* Esma Cannon as Effie  
* Lyn Evans as Ames  
* Wylie Watson as Venning  
* Yvonne Owen as Alice  
* Muriel Aked as Mrs. May  
* Joss Ambler as Atherton  
* Ernest Butcher as Old Man

==Critical reception==
The New York Times wrote, "an intimate and sometimes touching tale...Intelligently handled by Compton Bennett who directed the drama with an eye toward distilling character and perception from his cast...But it is rather unfortunate that the casts intense and genuine portrayals are not matched by the over-all effect of this serious but heavy vehicle."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 