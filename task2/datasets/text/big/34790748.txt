Alambrista!
{{Infobox film
| name           = Alambrista!
| image          = Alambrista!.jpg
| image_size     = 
| caption        =  Robert M. Young
| producer       = Michael Hausman Irwin Young Robert M. Young
| narrator       = 
| starring       = Domingo Ambriz
| music          = Michael Martin Murphey Robert M. Young
| editing        = Edward Beyer
| studio         = Filmhaus
| distributor    = First Run Features
| released       = October 16, 1977
| runtime        = 110 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Robert M. Young. It stars Domingo Ambriz and Trinidad Silva.  It won four awards in 1977. 

==Cast==
*Domingo Ambriz as Roberto
*Trinidad Silva as Joe
*Linda Gillen as Sharon
*Ned Beatty as Angelo Coyote
*Jerry Hardin as Man in cafe
In 2003 it was re-edited and remastered with a new soundtrack by Jose "Dr. Loco" Cuellar, Greg Landau, Francisco Herrera and Tomas Montoya and was re-released with a book on University of New Mexico Press.

==Awards==
*Winner Golden Camera - Cannes Film Festival (Robert M. Young)
*Winner Interfilm Award - Mannheim-Heidelberg International Film Festival (Robert M. Young)
*Winner Golden Seashell - San Sebastian International Film Festival (Robert M. Young)
*Winner OCIC Award - San Sebastian International Film Festival (Robert M. Young)

==References==
 

==External links==
* 
* 

 
 
 

 
 
 


 