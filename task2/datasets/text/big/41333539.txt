Once Upon a Time in Shanghai (film)
 
 
{{Infobox film
| name           = Once Upon a Time in Shanghai
| image          = OnceUponaTimeinShanghai.jpg
| alt            = 
| caption        = China film poster
| film name      = {{Film name
| traditional    = 惡戰
| simplified     = 恶战
| pinyin         = È Zhàn
| jyutping       = Ngok3 Zin3 }}
| director       = Wong Ching-po
| producer       = Wong Jing Andrew Lau Connie Wong
| writer         = Angela Wong
| starring       = Sammo Hung Philip Ng Andy On Michelle Hu Chen Kuan-tai Mao Junjie
| music          = 
| cinematography = 
| editing        = Wenders Li
| studio         = Mega-Vision Pictures See Movie Limited Henan Film and Television Production Henan Film Studio Meiya Great Wall Media (Beijing)
| distributor    = Mei Ah Entertainment
| released       =  
| runtime        = 95 minutes
| country        = Hong Kong
| language       = Cantonese Mandarin
| budget         = 
| gross          = 
}} action choreography by Yuen Woo-ping and Yuen Cheung-yan. The film is a remake of the 1972 film Boxer from Shantung which starred Chen Kuan-tai, who also appears in a supporting role in this film.  

==Plot==
Possessing unique super strength and martial arts skills since childhood, Ma Yongzhen (Philip Ng) travels from his hometown to Shanghai to seek a livelihood. There he meets rising gang boss Long Qi (Andy On), whose ruthlessness and powerful martial arts skills have put the four gang leaders of Shanghai’s reigning Axe fraternity on notice and pushed their territory from four even parts of Shanghai to just only half of it under the Axe fraternity and half under his own reign. Both youths possess insanely good martial arts skills. Ma righteously challenges Long Qi over his criminal activities at the pier with a duration of a cigarette in which if Ma won, he could have the entire truckloads of Opium for himself. Ma won the fight but instead of taking it and selling it out, he burnt the Opium right in front of Long Qi and Long Qi is impressed by Ma’s stalwart character and awesome martial arts skills. Soon, Long Qi offers to be Ma’s benefactor.

Ma doesn’t wish to be a criminal, but uses his connection with Long Qi to snag honest jobs for himself and his friends. Eventually both youths become good friends. Long Qi persuades Ma to join him to fight against the Axe fraternity but is refused by Ma. At the same time, Ma is warned by former axe gang boss, Master Tie (Sammo Hung), not to be involved in the fight between Long Qi and the Axe fraternity.

The Axe fraternity holds a grudge against Long Qi for stealing their territories and botched the deal between the Japanese and the Axe fraternity so they plot to assassinate him. Ma, helplessly watches as Long Qi dies, and Master Tie is brutally murdered which eventually leads him to embark on a path of vengeance.

==Cast==
*Sammo Hung as Master Tie
*Philip Ng as Ma Yongzhen
*Andy On as Long Qi
*Michelle Hu as Tie Ju
*Chen Kuan-tai as Baldy Bai
*Mao Junjie as Sheng Xiangjun
*Jiang Luxia as Tie Mei
*Yuen Cheung-yan as Laughing Buddha
*Fung Hak On as Scruffy Chou

==See also==
*Sammo Hung filmography
*Wong Jing filmography

==References==
 


==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 