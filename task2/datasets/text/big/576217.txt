Don't Tell Mom the Babysitter's Dead
{{Infobox film
| name = Dont Tell Mom The Babysitters Dead
| image = Dont Tell Mom The Babysitters Dead.jpg
| caption = Theatrical release poster
| director = Stephen Herek Robert F. Newmyer Brian Reilly Jeffery Silver Julia Phillips
| writer = Neil Landau Tara Ison
| starring = Christina Applegate Joanna Cassidy John Getz Keith Coogan Josh Charles David Newman
| cinematography = Tim Suhrstedt
| editing = Larry Bock HBO Pictures Outlaw Productions
| distributor = Warner Bros.
| released = June 7, 1991
| runtime = 102 minutes
| country = United States
| language = English
| budget = $10 million
| gross = $25,196,249 
}} American comedy film directed by Stephen Herek and starring Christina Applegate, Joanna Cassidy, Josh Charles, and David Duchovny.

The plot focuses on seventeen-year-old Sue Ellen Crandell, whose mother leaves for a two-month summer vacation in Australia, putting all five siblings in the care of a strict tyrannical elderly babysitter. When the babysitter suddenly dies in her sleep, Sue Ellen assumes the role as head of the household to prevent her mother from returning home early. She fakes a resume to get a job in the fashion industry, but proves capable and lucky enough to succeed.

==Plot==
Sue Ellen Crandell is a 17-year-old high school graduate who, due to a lack of funds, cannot go to Europe for the summer with her friends. However, Sue Ellen remains optimistic about a summer of freedom with her siblings: stoner Kenny, tomboy Melissa, ladies man Zach, and TV fanatic Walter, while their mother travels to Australia with her boyfriend. Much to Sue Ellens dismay, her mother hires a live-in babysitter, Mrs. Sturak, a seemingly sweet, humble old woman who assures Mrs. Crandall that she can take care of all five children. As soon as Mrs. Crandall leaves, Mrs. Sturak shows her true colors as a tyrant, quickly drawing the ire of the children. However, she later dies of a heart attack. When her body is discovered by Sue Ellen, the children agree to stuff the babysitter in a trunk and drop her off at a local funeral home and keep her car. They discover that the envelope given to Mrs. Sturak by their mother with their summer money is empty; she had it on her when they delivered her body to the funeral home.

With no money to pay the familys bills, Sue Ellen finds work at a fast food restaurant called Clown Dog. Despite a budding relationship with her co-worker named Bryan, she quits because of the obnoxious manager. Sue Ellen then forges a résumé under the guise of a young fashion designer and applies at General Apparel West (GAW), hoping to secure a job as a receptionist. However, Rose Lindsey, a company executive, finds her résumé so impressive that she offers Sue Ellen a job as an administrative assistant, much to the chagrin of Carolyn, a receptionist on Roses floor who was initially in line for the job. While having dinner at a restaurant that night, Mrs. Sturaks car is stolen by drag queens, forcing Sue Ellen to call in a favor from Bryan to bring them home. Sue Ellen then obtains the keys to her mothers Volvo, and begins stealing from petty cash at GAW to support the family, intending to return it when she receives her paycheck.
 Vassar and they make plans to get together for dinner.

In the end, Sue Ellen and Bryan make up, but are soon interrupted by Mrs. Crandall, who inquires about Mrs. Sturaks whereabouts. As the credits roll, the scene cuts away to the cemetery, where two morticians look over a gravestone that reads "Nice Old Lady Inside, Died of Natural Causes."

==Filming==

The house that was used in this movie is located in Canyon Country, California. 

==Cast==
*Christina Applegate as Sue Ellen Crandell
*Joanna Cassidy as Rose Lindsey
*John Getz as Gus Brandon
*Keith Coogan as Kenneth "Kenny" Crandell
*Josh Charles as Bryan
*Concetta Tomei as Mrs. Crandell
*David Duchovny as Bruce
*Kimmy Robertson as Cathy Henderson
*Jayne Brook as Carolyn
*Eda Reiss Merin as Mrs. Sturak
*Robert Hy Gorman (credited as Robert Gorman) as Walter Crandell
*Danielle Harris as Melissa Crandell
*Christopher Pettiet as Zachary "Zach" Crandell
*Jeff Bollow as Mole
*Michael Kopelow as Hellhound
*Dan Castellaneta (voice) as Animated Mrs. Sturak

==Soundtrack==
* "Draggin the Line", performed by Beat Goes Bang Alias
* "What She Dont Know", performed by Flame Valentine
* "Chains", performed by Lorraine Lewis
* "I Only Have Eyes For You", performed by Timothy B. Schmit
* "The Best Thing", performed by Boom Crash Opera
* "Viva La Vogue", performed by Army of Lovers
* "Stampede", performed by Brad Gillis
* "Bitter", performed by Terrell Modern English
* "Children of the Fire", performed by Mike Reeves Valentine
* Spinal Tap
* "As Time Goes By", written by Herman Hupfield
* "(Love Is) The Tender Trap", lyrics and music Sammy Chahn and Jimmy Van Heussen
* "Twilight Zone Theme", written by Marius Constant

==Reception==
Review aggregation website Rotten Tomatoes gives the film a score of 31% based on reviews from 26 critics, with an average score of 4/10.    The New York Post called Dont Tell Mom the Babysitters Dead the best teen hit of 1991. 

==Remake==
In June 2010, reports surfaced that a remake of the film, produced by The Mark Gordon Company, is currently in the works.  As of April 2015, further details are unknown. 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 