The Common Law (film)
{{Infobox film
| name           = The Common Law
| image          = File:TheCommonLaw.1931.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical poster of film
| director       = Paul L. Stein 
| producer       = Charles R. Rogers
| screenplay     = John Farrow
| based on       =  
| starring       = Joel McCrea Constance Bennett Lew Cody
| music          = Arthur Lange
| cinematography = Hal Mohr
| editing        = Charles Craft
| studio         = RKO Radio Pictures
| distributor    = 
| released       =    |ref2= }}
| runtime        = 75 minutes 
| country        = United States
| language       = English
| budget         = $339,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p39 
| gross = $713,000 
}} talking film era.  The sexual drama stars Constance Bennett and Joel McCrea in the title roles. It was received well both at the box office and by film critics, becoming one of RKOs most financially successful films of the year. The novel, The Common Law, had been filmed before in 1923 with Corinne Griffith starring.

==Plot==
Valerie West is a young American ex-patriot living with her wealthy lover, Dick Carmedon, in Paris.  Tired of the relationship, she moves out, after which she meets another American ex-pat, struggling artist John Neville.  She begins modeling for Neville, including some racy nude sittings.  At first the relationship is purely business, but the two soon fall in love, and she moves in with him.  The two begin to live an idyllic life, despite Carmedons attempts to get Valerie Back.

Unbeknownst to Valerie, Neville is not truly a poor artist, being the son of a wealthy family from Tarrytown, New York.  When Nevilles sister, Clare Collins, learns of the two living together out of wedlock in Paris, she decides to put an end to the relationship.  She enlists a friend of her brothers, Sam, whom goes to Neville and tells him about Valeries past relationship with Carmedon.  Unaware of the hypocrisy of his own actions, since he is living with Valerie in an identical relationship as she had with Carmedon, he becomes extremely jealous.  Unable to cope with his jealousy, Valerie moves out.

As the months go by, Neville runs into Valerie at a nightclub, where she is out with Querido.  While Neville is again jealous, he hides it.  Seeing him, Valerie understands her feelings about him and asks him to take her back, which he willingly does, shortly after which he asks her to marry him.  She defers, wanting to make sure that their feelings for one another are for real before making that commitment. When Clare hears about their reconciliation, she informs Neville that their father is very ill, and asks that he return home.  

Bringing Valerie with him, Neville returns to the family estate in Tarrytown. In a last ditch effort to break them up, Clare throws a party on the family yacht, to which she also invites Carmedon and an old girlfriend of Nevilles, Stephanie Brown.  However, the plan backfires when Carmedon attempts to sexually assault Valerie, and Neville intervenes, knocking Carmedon out.  Valerie finally understands that he feels the same way about her as she does about him, and the two dash off to find a justice of the peace to marry them.

==Cast==
* Constance Bennett as Valerie West
* Joel McCrea as John Neville
* Lew Cody as Dick Carmedon Robert Williams as Sam
* Hedda Hopper as Mrs. Clare Collis
* Marion Shilling as Stephanie Brown Walter Walker as John Neville, Sr.
* Paul Ellis as Querido
* Yola dAvril as Fifi

(as per American Film Institute|AFIs database )

==Production== silent era, both with a Selznick producing.  The first, in 1916, was produced by Lewis J. Selznick starred Clara Kimball Young and Conway Tearle. Lewis was the father of David O. Selznick.  Davids brother, Myron Selznick|Myron, would remake the film, this time in 1923, in which Tearle would again star, this time opposite Corinne Griffith. 

In February 1931, RKO announced it had purchased the rights to Chambers novel.  Charles R. Rogers made the announcement, as well as stating that Constance Bennett would be the star of the film, and that John Farrow would be adapting the screenplay.  Production on the film was to be scheduled to begin as soon as Bennett finished shooting on Lost Love, however, due to a illness for Bennett, the start of production was delayed slightly. In mid-March, it was announced that Paul L. Stein would helm the film.  By the end of March, more roles were cast, with Joel McCrea, Lew Cody, Gilbert Roland, Walter Walker, Marion Shilling, and Robert Williams being assigned to the film. By this point, the only major role still to cast was that of Nevilles sister, Clare.  The final major role would be cast in the beginning of April, with Hedda Hopper being chosen to play Clare.  The Common Law went into production in mid-April 1931. Gwen Wakeling, who was the head of costuming for RKO, designed the costumes.   Small roles would go to Erin La Bissionaire,  Julia Swayne Gordon,  Nella Walker,  and Yola dAvril in the role of Fifi.  Margot De La Falaise, the wife of the Comte Alain De La Falaise, made her film debut in this picture, also in a small role.  Alain was the younger brother of RKOs chief of French film versions, Henri. 

During production a yacht built for American financier, E. H. Harriman, was used as the setting for films climactic scene.   One of the most expansive scenes in the film, where Neville runs into Valerie a month after she leaves him, was set at a nightclub during the famous Four Arts Ball, which was held annually in Paris.  The scene required many of the female extras used in the scene had to wear full body makeup, due to the scant nature of their costumes.  By the middle of June, shooting on the film had wrapped.   

Williams appearance in this film would lead to him signing on as a contract player with RKO.  

==Release==
The film premiered at the Mayfair theater in New York on July 17, 1931; and was released nationally the following Friday, on July 24.  The publisher of Chambers novel, Grosset & Dunlap, re-issued the novel in a special edition which featured a picture of Bennett on a wrapper, and were prominently featured in sales displays to coincide with the films opening. 

==Critical response==
The New York Age gave the film a very positive review, calling Bennetts performance, "matchless".  While praising the performances of Bennett and McCrae, as well as singling out the scene at the Four Arts Ball, The Film Daily gave the film a lukewarm review, stating that "... the story itself doesnt produce much of a dramatic punch due to lots of talk and little action."    The Reading Times gave the film a glowing review, calling Bennett "superb", and the rest of the cast "excellent".  Modern Screen called the film a "lavish production", and gave high marks to Bennett and the rest of the cast, stating "The star and an excellent cast imbue the old tale of artists and models with an up-to-date flavor, and the problem presented is one that will ever hold popular appeal."   Another favorable review was given by Motion Picture Daily, calling it a "sophisticated drama", and praising the performances of Bennett and McCrae, although they did counsel that the film was not suitable for children.  Photoplay also warned against bringing children to see the film, and while they lauded the performances of Bennett, McCrea, Hopper, and Cody, they did not think much of the film, calling it a "poor adaptation of Robert Chambers best seller".  Screenland rated the film one of their "Six Best Pictures of the Month" in October 1931, and Bennetts performance one of the ten best.  Silver Screen magazine merely gave the film a "good" rating. 

The sexual relationships became an issue for the Hays Commission, which was in its infancy at this time in the Pre-Code Hollywood|Pre-Code era, since the film promoted non-marital sex.  

==Box office==

According to RKO records, the film made a profit of $150,000.  The financial success of the film led to a new weekly record being set for RKO theaters in August 1931.  The Common Law was one of the few financial successes for RKO Pictures in 1931.   

==References==
 

==External links==
*  at TCMDB
*  at IMDB

 

 
 
 
 
 
 
 
 