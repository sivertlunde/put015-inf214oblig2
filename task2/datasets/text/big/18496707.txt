Mask of Desire
{{Infobox film
| name           = Mask of Desire मुकुण्डो
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Tsering Rhitar Sherpa
| producer       = Tsering Rhitar Sherpa
| screenplay     = Kesang Tseten
| based on       = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Ranjan Palit
| editing        =  NHK Japan
| distributor    = 
| released       = 
| runtime        = 
| country        = Nepal
| language       = Nepali
| budget         = 
| gross          = 
}}
 Nepali film directed by Tsering Rhitar Sherpa. It was Nepals submission to the 73rd Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.        

==Plot==

The setting is the increasingly less visible ritual face of Kathmandu. Dipak, boyishly handsome, in his mid 1930s, a former football player in the army, works as an uniformed guard for a successful business. Saraswati, younger by two years, is a homely, virtuous woman, who adores her footballer husband. They live in a modest two-room apartment on the second floor of an old brick building with their two young girls, an ordinary, humble family, happy in most respects.

There is much anxiety over Saraswati’s imminent pregnancy. For, if there is one thing that life - or the gods - hasn’t given them, it is a son. Not unusual for a family in their society, it is an unresolved thread in their otherwise contented lives. Dipak wants a son, Saraswati wants him to be happy – these are sometimes deep-seated, other times lurking, desires which, if realized, will make their lives that much better.

One day, while doing her usual prayers at the Shiva-lingam shrine, a sadhu mysteriously appears and tells her only the goddess Tripura at the small brick shrine by the riverbank can answer her prayers. Saraswati does so hesitantly, and when a son is born she begins to believe her prayers were responsible. The couple’s joy is short-lived, however, for a few weeks later, the infant dies, bringing in its wake sorrow, anger, and guilt.

The once-happy family begins to breaks down. Saraswati becomes intermittently ill and begins to experience bouts of depression. When she finally goes back to the goddess who she believes gave her a son, only to cause her greater sorrow, the same sadhu appears and suggests she get “treated” by the jhankrini who is the spirit medium of the Tripura goddess.

Parallelly, we learn that the jhankrini, reputed as a healer, has her own particular history. As a young woman she had been married off to a mentally disturbed boy-husband, who kept running away from her and from life apparently, eventually committing suicide. It is known that people in vulnerable and unstable states are often “chosen” to be mediums for deities. Gita’s marriage and her sick husband’s tragic fate had made her gravely ill, causing her intense emotional turmoil, breakdowns, and visions, until she was diagnosed as being a “possessed”.

The jhankrini remains deeply ambivalent about the role that is thrust on her, and deeply yearning human love, however. All along, her relative and man-attendant warns her not to digress from her extraordinary path, hinting at the dire consequences of doing so.

It is not only fate but also their inner wants that bring Dipak, Saraswati and Gita (the jhankrini) together. Saraswati’s “treatment” by the jhankrini appears to be efficacious. In time the three become socially acquainted.
Saraswati is grateful to the jhankrini and also fascinated by the spirit medium, the jhankrini is touched by Saraswati’s and Dipak’s relationship, and by the boyish innocence of Dipak, which eventually serves as a foil for her misdirected passion.  Dipak sidesteps Gita’s advances but is all the same smitten by the allure of something much larger than himself.

The loss of her son, the failure to please Dipak forever, as well as her fascination of and jealousy for Gita all combine to plunge Saraswati into a depression that threatens her sanity. Saraswati decides to act. She goes back to the shrine of the goddess at the riverbank, where it all began. Her communion with the goddess compels her to return to Gita, for the higher good, for herself and her husband, in spite of her baser feelings for Gita.

When Gita learns that Dipak approves of his wife seeking her healing, she feels redeemed. She prays to the goddess to grant her the powers to heal this time. For lately, she has been experiencing lapses to this God-given faculty, reminding us of her attendant’s warnings.

The day for Saraswati’s ritual cure falls on an important religious festival, when devotees pull around a gigantic wooden chariot with the gods inside it, a time of great festivity marked by an extreme collective frenzy. The healing is proceeding smoothly when something snaps. Suddenly the jhankrini and the patient are locked in a fierce tussle, as each one screams to onlookers that the other is bewitched, carrying the “bokshi”, as it is said.

The jhankrini beats Saraswati with the fearsome instruments reserved for driving out the “bokshi”, Saraswati with her hands and teeth in desperate defence of her life, while the intoxicated devotees beat drums in echo of the ritual frenzy outside. From such a finale - perhaps not of entirely human energies - only one emerges triumphant, but in the eerily quiet aftermath of the frenzied festival, we are left disturbed and unsure of just who was the one bewitched.

==Cast==

* Gita: 	Jhankrini/Healer
* Saraswati:	The victim
* Dipak:	Saraswati’s husband
* Ama:		Dipak’s mother
* Jethi:	Saraswati’s elder daughter
* Kanchi:	Saraswati’s younger daughter

==Reception==

The film received very good critical responses, and is generally hailed as a landmark Nepali film. The films use of color and off beat story telling was unprecedented in Nepal. The film was Nepals selection for OSCAR Academy Awards Consideration in Best Foreign Language Film in 2000. It went to be screened in many prestigious international film festivals like San Francisco International Film Festival, Vancouver International Film Festival, Goteborg International Film Festival, Fribourg International Film Festival, Fukuoka International Film Festival, Mumbai International Film Festival.

==References==
 

==External links==
* 

==See also==
 
*Cinema of Nepal
*List of submissions to the 73rd Academy Awards for Best Foreign Language Film

 
 