Sissi – The Young Empress
{{Infobox Film
| name           = Sissi - Die junge Kaiserin
| image          = Sissi - The young Empress.jpg
| caption        = Theatrical release poster
| director       = Ernst Marischka
| producer       = Karl Ehrlich Ernst Marischka
| writer         = Ernst Marischka
| starring       = Romy Schneider Karlheinz Böhm Magda Schneider Gustav Knuth Vilma Degischer Josef Meinrad
| music          = Anton Profes
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 101 min.
| country        = Austria
| awards         =  German
| budget         = 
}}

Sissi – The Young Empress ( ) is a 1956 film directed by Ernst Marischka and starring Romy Schneider, Karlheinz Böhm, Magda Schneider, Uta Franz, Gustav Knuth, Vilma Degischer and Josef Meinrad. It was entered into the 1957 Cannes Film Festival.    It is the second film in the Sissi trilogy, following Sissi (film)|Sissi and preceding Sissi - Fateful Years of an Empress.

==Plot== equal standing in the Empire. The movie concludes with her being crowned Queen of the Hungarians in Budapest.  (In fact the coronation was not held until 1867, but "Sissi--The Young Empress" brings the event forward in time to make the ceremony appear to be a confirmation of Sissis improving status as empress.)

== Starring == Elisabeth of Austria, or "Sissi"
* Karlheinz Böhm - Emperor Franz Joseph I of Austria
* Vilma Degischer - Archduchess Princess Sophie of Bavaria|Sophie, Franz Josephs mother Franz Karl, Franz Josephs father Ludovika in Bavaria, Sissis mother Max in Bavaria, Sissis father
* Josef Meinrad - Major Böckl
* Richard Eybner - Postmaster of Ischl
* Walter Reyer - Count Gyula Andrássy|Andrássy
* Senta Wengraf - Countess Bellegarde
* Helene Lauterböck - Countess Esterhazy-Liechtenstein

==References==
 

==External links==
*  
*   

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 