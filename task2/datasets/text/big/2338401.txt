Proof of Life
 
{{Infobox film
| name           = Proof of Life
| image          = Proof of Life film.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Taylor Hackford
| producer       = Taylor Hackford Charles Mulvehill
| writer         = Tony Gilroy David Morse David Caruso Gottfried John
| music          = Danny Elfman
| cinematography = Sławomir Idziak
| editing        = Sheldon Kahn John Smith
| studio         = Castle Rock Entertainment Bel-Air Entertainment Anvil Films
| distributor    = Warner Bros.
| released       =  
| runtime        = 135 minutes
| country        = United States Italian Spanish Spanish
| budget         = $65 million
| gross          = $62.8 million
}} action thriller Vanity Fair magazine article "Adventures in the Ransom Trade,"   and Thomas Hargroves book The Long March To Freedom   in which Hargrove recounts how his release was negotiated by Thomas Clayton, played by Russell Crowe, who went on to be the founder of kidnap-for-ransom consultancy Clayton Consultants, Inc.
 tabloid press David Morses stand-in.  He was killed in an on-set accident during a scene in which Morse was not available, due to a family illness. 

==Plot== David Morse), has been hired to assist with building a dam. Though Alice is unhappy at this most recent move, she agrees to stay. When Peter is in the city one day, a convoy of automobiles (including his) is ambushed by guerrilla rebels of the Liberation Army of Tecala (ELT). Believing that Peter actually works for an oil pipeline company, ELT soldiers abduct him and lead him into the countrys jungles.
 Green Beret.

 

Over the next several months, Thorne uses a radio to talk with an ELT contact, and the two argue over terms for Peters release—including a ransom payment that Alice can afford.  With much downtime between conversations, Thorne and Alice talk, and an implicit attraction between the two seems to emerge.  After much negotiation, it appears that the ELT will release Peter for a sum of $650,000.

Meanwhile, Peter is led through the jungle by a group of younger rebels before arriving at the main jungle camp.  There, he meets another hostage, Kessler, a missionary and former member of the French Foreign Legion, who has lived in the camp for nineteen months.  The two concoct a plan to escape through the jungle.  During their attempt, they are tracked by the ELT.  Kessler falls into a river and manages to evade capture, but Peter steps on a trap and is recaptured.  Kessler is found and hospitalized.  In the hospital, he meets Alice and, having heard a gunshot at the time Peter was recaptured, tells Alice he believes her husband is dead.

Thorne refuses to believe this, but he is unable to contact his ELT radio negotiator.  Luckily, Alices housekeepers young assistant reveals the true identity of the ELT radio contact; she knows his voice quite well because her mother has done laundry service for him in the past.   Thorne goes to the Tecala Armed Forces Demonstration Parade and confronts the ELT contact, who is actually a high-ranking government official.  The contact confirms that Peter is indeed alive, but because Peter has seen secret ELT maps the opportunity for a deal has passed, and the ELT army will no longer negotiate.

At Thornes urging Alice convinces the Tecala government the ELT is about to mount an attack on the pipeline being built through their territory. The government army crossing the river draws the bulk of the ELT army out of the camp to counter-attack. This provides an opportunity for Thorne, Dino, and several associates to insert by helicopter and raid the ELT base.  They overcome the resistance of the minimal defense left holding the camp and free not only Peter but also an Italian hostage held there as well.  And so Peter is rescued and brought back safely to Alice.  Thorne and Alice share a final moment together wherein the unrequited bond between them is painfully expressed, and the movie ends with a poignant image of a lonely hero.

==Cast==
* Meg Ryan as Alice Bowman
* Russell Crowe as Terry Thorne David Morse as Peter Bowman
* Pamela Reed as Janis Goodman
* David Caruso as Dino
* Anthony Heald as Ted Fellner Michael Byrne as Lord Luthan
* Stanley Anderson as Jerry
* Gottfried John as Eric Kessler
* Alun Armstrong as Wyatt
* Michael Kitchen as Ian Havery
* Margo Martindale as Ivy
* Mario Ernesto Sánchez as Arturo Fernandez
* Pietro Sibille as Juaco
* Vicky Hernández as Maria
* Norma Martínez as Norma
* Carlos Blanchard as Carlos

==Background== Andean countries.

The ELTs characterization appears to be primarily based on the Revolutionary Armed Forces of Colombia (FARC).  Coincidentally, Colombias second largest guerrilla group is the National Liberation Army (Colombia)|Ejército de Liberación Nacional or ELN.

===Tecala=== fictional South Soviet Unions dissolution in 1991, the ELTs primary source of funding fell through, and they began kidnapping people for ransom to fund their operations. A map seen in the film is that of Ecuador. The countrys capital Quito was chosen along with the eastern jungle and the nearby city of Baños de Agua Santa in the Ecuadorian Andes.

==Release ==
 
The film opened in wide release in the United States on December 8, 2000 for 2,705 screens.

The opening weekends gross was $10,207,869 and the total receipts for the U.S. run were $32,598,931. The international box-office receipts were $30,162,074, for total receipts of $62,761,005. The film was in wide release in the U.S. for twelve weeks (eighty days). In its widest release, the film was featured in 2,705 theaters across the country. 

==Reception==

===Critical response===
Stephen Holden, film critic for The New York Times, did not think the film worked well and opined that the actors did not connect. He wrote, "  a gaping lack of emotional connection among the characters in a romantic triangle that feels conspicuously unromantic... what ultimately sinks this stylish but heartless film is a flat lead performance by the eternally snippy Meg Ryan... Ms. Ryan expresses no inner conflict, nor much of anything else beyond a mounting tension. Even when her wide blue eyes well up with tears, the pain she conveys is more the frustration of a little girl who has misplaced her doll than any deep, empathetic suffering." 

Critic David Ansen gave the film a mixed review, writing,
 Bogart once would have played—the amoral tough guy who rises to the moral occasion—and Crowe gives it just the right note of gravel-voiced masculinity. But neither Crowe, Ryan nor the topical subject keeps Proof of Life from feeling recycled. For all the up-to-the-minute research, the movie still gives off the musty scent of Hollywood contrivance.  

Review aggregator Rotten Tomatoes gives the film a score of 40%, based on 115 reviews. 

===Awards===
  Best Original Gladiator (Hans Zimmer).

{| class="wikitable"
|-
! Award
! Category
! Nominee
! Result
|- Blockbuster Entertainment Awards
| Favorite Actor – Suspense
| Russell Crowe
| rowspan="4"  
|-
| Favorite Actress - Suspense
| Meg Ryan
|-
| Favorite Supporting Actor – Suspense
| David Caruso
|-
| Favorite Supporting Actress – Suspense
| Pamela Reed
|- Satellite Awards Best Original Score
| Danny Elfman
|  
|}

==See also== Colombian armed conflict
* Kidnappings in Colombia

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
  
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 