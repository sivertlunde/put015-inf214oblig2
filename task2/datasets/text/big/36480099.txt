Just Dogs
{{Infobox Hollywood cartoon
| cartoon name      =Just Dogs
| series            = Silly Symphonies
| image             =
| image size        = 
| alt               = 
| caption           = 
| director          = Burt Gillett
| producer          = Walt Disney
| story artist      = 
| narrator          = 
| voice actor       = Pinto Colvig
| musician          = Bert Lewis David Hand Fred Moore Harry Reeves
| layout artist     = 
| background artist = 
| studio            = Walt Disney Productions
| distributor       = United Artists
| release date      = July 30, 1932
| color process     = Black and White
| runtime           = 7 min
| country           = United States
| language          = English
| preceded by       = Mickey in Arabia
| followed by       = Mickeys Nightmare
}}

Just Dogs is a 1932 Silly Symphonies animated film, directed by Burt Gillett. It marked the first solo appearance of Pluto (Disney)|Pluto.

==Summary==
Plutos cage-mate at the dog pound breaks out and lets all the other dogs out as well. In the park, the dog who helped Pluto earlier keeps following him too closely for Plutos tastes, until he digs up a huge bone and gives it to Pluto (who doesnt particularly want to share). But soon all the other escaped dogs are chasing after the bone.

==External links==
*  

 

 
 
 
 
 
 
 


 