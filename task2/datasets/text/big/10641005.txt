Bewafai
{{Infobox film
| name = Bewafai
| image = 
| caption = 
| director = R.Thyagarajan
| writer =  Pran Padmini Kohlapure Tina Munim
| producer = C.Dhandayudapani
| music = Bappi Lahiri
| lyrics = Indivar Farook Kaiser Hasan Kamal
| cinematography =
| released = 20 September 1985
| runtime = 2 hours 10 minutes 
| language = Hindi
| country        = India
| budget =
}}

Bewafai is a 1985 Hindi film starring Rajesh Khanna and Rajinikanth. This was the first film starring these two actors.  The film was directed by R. Thyagaarrajan.  Dhandayuthapani produced only four films  in Hindi with Bewafai being the most successful of them. Bewafai became asuccess and grossed Rs 11.95&nbsp;crore at the box office in 1985.   

==Synopsis==
Bewafai was a romantic film revolving mostly around a woman named Asha (Tina Munim) who has been in love with a man named Ashok Nath (Rajesh Khanna) since her childhood. But somehow she was never able to convey her feelings for him throughout her childhood and college days. Now Ashok works as her dads employee.  She becomes very possessive about him to the point where she cannot stand the thought of another woman trying to woo him. The possessiveness becomes so strong that Asha beats up a woman named Vinny (Meenakshi Seshadri) over Ashok. She tries her best to ensure that she keeps herself aware of Ashoks whereabouts. Renu (Padmini Kolhapure) falls in love with Ashok and when this fact is known to Asha she becomes heartbroken. She comes to know that Ashok has been taking care of Renu for a long time. Asha decides to meet Renu to teach her a lesson but when she finds that Renu is a mental patient and Ashok has been nursing her, she changes her heart and begins to assist Ashok in doing service to the mentally imbalanced Renu. Their lives take an unexpected turn when an unknown person shoots Ashok and leaves him mortally wounded. The rest of the story is about whether Asha gets a chance to convey her feelings for Ashok, how Renu became a mental patient, the relation between Renu and Ashok, how Ashok solves the problem and how the mystery gets unfolded.

==Cast==
*Rajesh Khanna as Ashok Nath
*Meenakshi Seshadri as Vinny
*Rajinikanth as  Ranvir
*Tina Munim as Asha Pran as Diwan Sardarilal
*Padmini Kolhapure as Renu
*A.K. Hangal as Harihar Nath

==Music==
*"Hum Apni Wafa Yaad Dila Di Nahin Sakte"– Kishore Kumar
*"Shukriya Dil Diya Shukriya Dil Liya" – Kishore Kumar
*"Aaiyena Kasam Se Jaaiye Na Kasam Se" – Asha Bhosle
*"Lachakta Jism Jalte Lab Sulagta Husn Tera Hai " – Asha Bhosle
*"Ek Nazar Bass Dekhein Aap" – Asha Bhosle
*"Suno Zara Mere Sanam Dil Ki Sada Meri Kasam " – Lata Mangeshkar
*" Piya Aa Piya Aa Piya Aa O Piya Piya Piya" – Asha Bhosle
*" Hum Apni Wafa Yaad Dila Di Nahin Sakte"– Lata Mangeshkar
*" O Mera Daata Mera Maalik Mera Maula Tu" – Asha Bhosle
*" Sweet Sweet Sweet Sweet Oh No I Am Seventeen" – Asha Bhosle

==References==
 

==External links==
*  

 
 
 
 