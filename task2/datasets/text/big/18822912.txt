The Kovak Box
 
{{Infobox Film 
| name = The Kovak Box
| image          = The Kovak Box.jpg|
| producer       = Álvaro Augustín
| caption        = 
| director       = Daniel Monzón
| writer         = Daniel Monzón Jorge Guerricaechevarría David Kelly
| music          = Roque Baños
| cinematography = Carles Gusi
| editing        = Simon Cozens
| distributor    = First Look Studios
| released       =  
| runtime        = 102 min.
| country        = Spain United Kingdom
| language       = English
| budget         =  
}} British thriller David Kelly. It is set on the island of Mallorca.

== Synopsis ==
David Norton (Hutton) is used to being in control. As a best-selling science fiction author, he decides the fate of his characters; his heroes, his villains, their lives and deaths.

He is invited to an idyllic Mediterranean island for a conference. His fiancee receives a strange call and jumps to her death from their hotel balcony. Soon after that, David discovers he has been lured into a trap.

As he begins piecing answers together, people start inexplicably committing suicide all around him. Now David has become the reluctant hero of his own living story, and this time he has no idea how it will end.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 


 