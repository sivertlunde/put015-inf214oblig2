Far North (2007 film)
 
{{Infobox Film
| name           = Far North
| image          = 
| image_size     = 
| caption        = 
| director       = Asif Kapadia
| producer       = 
| writer         = Asif Kapadia, Tim Miller, Sara Maitland (story)
| narrator       = 
| starring       = Michelle Yeoh, Michelle Krusiec, Sean Bean 
| music          = Dario Marianelli
| cinematography = Roman Osin
| editing        = Ewa J. Lind
| distributor    = 
| released       = Dec. 26, 2007
| runtime        = 89 min
| country        = UK, France
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} independently produced film by director Asif Kapadia, based on a short story by Sara Maitland. It was screened at various film festivals in 2007 and 2008 before a US DVD release on September 23, 2008.

== Plot ==
The film opens up with a voiceover of a middle-aged woman named Saiva stating that a shaman said she was cursed at birth and would bring harm to anyone she cared for. While she and a girl named Anja are camping in a subarctic region of an unknown spot, she takes one of her sled dogs and brutally slits its throat after calming it , prior to what the shaman said about her at birth. She and Anja then set out to relocate somewhere, since theyve been spotting danger here and there. On the way, they almost get caught by Soviet soldiers. Fearing the worst, Saiva ventures out with Anja far north to the arctic tundra, where she believes that it is so remote that no one would ever find them. After a long paddle north, they pitch camp on a beach, where they set up their cozy furred yurt. 
 flashback somewhere in her earlier years somewhere south, after being abolished from her tribe. In her flashback, she encounters a man inviting her into his tribe and the two fall in love with each other after he gives her a wolf claw on a necklace. Going back to the present, Saiva and Anja live a harsh brutal life living in the tundra hunting animals for their survival. One day while hunting alone, Saiva encounters a badly wounded lone man nearly frozen and starved to death, who is Unconsciousness|unconscious. After she tries to loot him, the man regains consciousness and asks for her hospitality. Accepting it, Saiva brings and cleans his wounds in her yurt. She introduces him to Anja after she returns, and the man later claims to be a soldier by the name of Loki. 

The next day on a hunting trip with them, Loki asks what he can do in return for them, with Saiva and Anja asking for him bringing them a reindeer. After a failed seal hunt, Loki tells Saiva in their yurt that he was an escapee from soldiers that came to clear out the tundra, and that if they find him theyll shoot him. He then shows them his portable hand-cranked radio, the only clue that the story takes place somewhere in the last half of the 20th century. Early in the next mourning, Loki tries to keep his promise by bringing them a reindeer for food. But just as hes about to shoot one, he gets captured by a couple Soviet soldiers. Just as theyre about to put him in their boat, he fights back and kills both of them. He then comes back with their looted supplies and gives them to Saiva and Anja for use. Over the next few months Loki and Anja fall in love with each other. Saiva tries to warn him about her secret, but Anja interrupts her and they hang out with each other more and more. Another flashback scene shows Saiva finding all of her tribe dead, except for the man that she fell in love with and a baby. After being caught, one of the soldiers who slaughtered her village rapes her after slitting the mans throat. She finds the baby after that in one of the yurts, and one of the soldiers tells her to cooperate with her or hell skin the baby alive. Later when she and the soldiers where crossing a glacier, she used a knife to cut the rope, causing all of the soldiers to fall into a Fracture (geology)|crevice. She then takes the baby with her, with it being the possible the baby who became Anja and was raised by Saiva. 

Once again going back to the present, Anja and Loki hang out more and more throughout the following months. One night when Saiva is out hunting, they have sexual intercourse. Loki also goes out hunting a lot from time to time, eventually accomplishing his  promise on killing a reindeer. As their love builds up, they plan on leaving to go to civilization and have a family of their own. When Anja tells this to Saiva, Saiva gets into a state of shock and is speechless. As tension builds up Saivas mind gets more and more worked up with depression of her daughters leave; even when Anja asks her to come with them she is speechless. The night before they leave Loki goes out hunting for the night. When Anja returns to the yurt, Saiva suddenly speaks and tricks Anja into combing her hair and brutally chokes Anja to death with her own hair, prior to the dog in the begging. She then brutally and horribly skins Anjas face an disguises herself with it to not terrorize Loki. When Loki comes back, he has another sexual intercourse with Saiva, assuming that she is Anja. But he then discovers Saivas fake face by seeing her face crumple up. Petrified, he flees the yurt naked into the frozen tundra, and probably froze to death eventually. The film ends with Saiva sobbing in her yurt, probably because she realized what she had done.

==Cast==
* Michelle Yeoh as Saiva
* Michelle Krusiec as Anja
* Sean Bean as Loki

==External links==
* 
*http://www.hollywoodreporter.com/hr/film/reviews/article_display.jsp?rid=9698
*http://www.screendaily.com/far-north/4034321.article
*http://www.variety.com/review/VE1117934537.html
*http://www.channel4.com/film/reviews/film.jsp?id=166538&section=review&page=all#reviewnav

 

 
 
 