Maverick (film)
 
{{Infobox film
| name           = Maverick
| image          = Maverick movie.jpg
| caption        = Theatrical release poster
| director       = Richard Donner 
| producer       = Bruce Davey Richard Donner
| writer         = William Goldman
| based on       =  
| starring       = {{plainlist|
*Mel Gibson
*Jodie Foster
*James Garner Graham Greene
*James Coburn 
*Alfred Molina }}
| music          = Randy Newman
| cinematography = Vilmos Zsigmond
| editing        = Stuart Baird Mike Kelly
| studio         = Icon Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 127 minutes 
| country        = United States
| language       = English
| budget         = $75 million 
| gross          = $183,031,272 
}}
 Western comedy of the Graham Greene, James Coburn, Alfred Molina and a large number of cameo appearances by Western film actors, country music stars and other actors.

The film received a favorable critical reception for its light-hearted charm, and was financially successful, earning over $180 million during its theatrical run. It received a nomination for an Academy Award for Best Costume Design (April Ferry).

==Plot==
The story, set in the American Old West, is a first-person account by wisecracking gambler Bret Maverick (Mel Gibson) of his misadventures on the way to a major five-card draw poker tournament. Besides wanting to win the tournament for the prize money, he also wants to prove, once and for all, that he is the best card player of his time.

Maverick rides into the fictional town of Crystal River intending to collect money owed to him, as he is $3,000 short of the tournament entry fee of $25,000. While in Crystal River, he encounters three people: an ill-tempered gambler named Angel (Alfred Molina), a young con artist calling herself Mrs. Annabelle Bransford (Jodie Foster), and lawman Marshal Zane Cooper (James Garner). The first two are also rival poker players.
 Indians led Graham Greene). swindle a Russian Grand Duke, with Maverick collecting the $1,000 that Joseph owes him.

Angel receives a mysterious telegram ordering him to stop Maverick from reaching the tournament. He also learns that Maverick had conned him in Crystal River. Angel catches up with Maverick, beats him up, and attempts to hang him from a tree. Maverick escapes after the tree branch breaks under his weight, and makes it to the poker tournament aboard the paddle steamer Lauren Belle. Angel already has a seat in the game, while Cooper has been engaged to oversee its security. Learning that Bransford is still short $4,000 of the entry fee, and still being $2,000 short himself, Maverick finds the Grand Duke on board and cons him out of the money they both need.

After the others are eliminated, the four finalists are Maverick, Bransford, Angel, and  ," and Maverick calls without looking at his new card. When the three reveal their hands, that card turns out to be the ace of spades, giving Maverick the championship. An enraged Angel draws his gun, but he and his stooges in the audience are gunned down by Cooper and Maverick.

Instead of presenting the $500,000 grand prize to Maverick, Cooper steals it and escapes from the boat; Maverick stops Duvall from shooting him. During a nighttime meeting in the woods, Cooper and Duvall discuss the scheme they have just carried out: Angel was working for Duvall, and Coopers job was to steal the money and split it with Duvall and Angel if anyone except either of those two won the tournament. Duvall overpowers Cooper and prepares to shoot him, but they are interrupted by Maverick, who steals the money back and leaves them with a single gun to settle their argument. It turns out to be unloaded; Cooper beats up Duvall, but stops short of killing him and sets out after Maverick.

Later, in a bathhouse, Cooper catches up to a relaxing Maverick and briefly threatens to shoot him. The two then drop their pretense, acknowledging each other openly as father and son; they had in fact conspired all along to get the $500,000. As Cooper enjoys a bath of his own, Bransford enters and robs the two, having surmised the relationship from their similar mannerisms. After she escapes, Maverick tells Cooper that she only got half the money, the rest being hidden in his boots. He admits that he allowed Bransford to steal from him so that he can enjoy chasing her down to recover that money.

==Cast==
* Mel Gibson as Bret Maverick
* Jodie Foster as Annabelle Bransford
* James Garner as Marshal Zane Cooper Graham Greene as Joseph
* Alfred Molina as Angel
* James Coburn as Commodore Duvall Geoffrey Lewis as Matthew Wicker
* Paul L. Smith as The Archduke
* Max Perlich as Johnny Hardin
 Robert Fuller, Doug McClure and Henry Darrow as riverboat poker players; Dan Hedaya as Twitchy, another Riverboat poker player; Dennis Fimple as Stuttering, a player beaten by the Commodore; Bert Remsen as an elderly riverboat gambler beaten by Maverick;  and Margot Kidder as missionary Margaret Mary in an uncredited appearance.   on IMDb (cited 9 April 2014)  
 Lethal Weapon film series starring Glover and Gibson as cop partners. Their meeting in Maverick sees them share a moment of recognition,  and as he leaves, Glover says Roger Murtaughs catchphrase: "Im getting too old for this shit."

Country singers also cameo including Carlene Carter as a waitress, Waylon Jennings and Kathy Mattea as a gambling couple with concealed guns, Reba McEntire, Clint Black as a sweet-faced gambler thrown overboard for cheating, and Vince Gill and his then-wife Janis Gill as spectators.

==Production==
The steamboat used in the film—dubbed the Lauren Belle—was the Portland (steam tug 1947)|Portland, the last remaining sternwheel tugboat in the US, that at the time belonged to the Oregon Maritime Museum in Portland. Over several weeks, the boat was decorated to alter its appearance to resemble a Mississippi style gambling boat, including the addition of two decorative chimneys.  In August 1993, the production requested permission to film scenes of the riverboat along the Columbia River in Washington State. The artificial smoke released by the boats chimney was considered to violate air-quality laws in Washington and Oregon and required approval for the scenes before their scheduled filming date in September 1993.  After filming concluded, the decorations were removed and the boat was returned to its original state. 

In Five Screenplays with Essays, Goldman describes an earlier version of the script, in which Maverick explains he has a magic ability to call the card he needs out of the deck. Although he is not able to do so successfully, the old hermit he attempts to demonstrate it for tells him that he really does have the magic in him. 
{{cite book
|url= http://books.google.com/books?id=zEfycPl7hjQC
|title= William Goldman: Five Screenplays with Essays
|first= William
|last= Goldman
|authorlink= William Goldman
|pages= 474–479
|publisher= Hal Leonard Corporation
|year= 2000
|isbn= 978-1-55783-362-4
}}  This scene was shot with Linda Hunt playing the hermit but it was felt it did not work on the context of the rest of the movie and was cut. 

==Reception== average rating of 6 out of 10&nbsp;&ndash;&nbsp;on the review-aggregate website Rotten Tomatoes, which said, "It isnt terribly deep, but its witty and undeniably charming, and the cast is obviously having fun." 

James Berardinelli, from reelviews.net, gave the film three and a half stars out of four. He stated, "The strength of Maverick is the ease with which it switches from comedy to action, and back again....its refreshing to find something that satisfies expectations."  Reviewing it for the Chicago Sun-Times, Roger Ebert gave the film three stars out of a possible four, writing: "The first lighthearted, laugh-oriented family Western in a long time, and one of the nice things about it is, it doesnt feel the need to justify its existence. It acts like its the most natural thing in the world to be a Western." 

===Box office===
The film earned $101,631,272 (55.5%) in North America and $81,400,000 (44.5%) elsewhere for a worldwide total of $183,031,272.  This gross made it the number 12 highest grossing film in North America and the number 15 highest grossing film worldwide of 1994. As of 2013, the film is the number 6 highest grossing Western film in North America. 

Pre-release tracking showed that the film would open strongly,.  During its opening weekend in North America, Maverick earned $17.2 million million from 2,537 theaters – an average of $6,798 per theater – ranking as the number 1 film of the weekend,  and took a total of $41.8 million over its first two weeks of release. 

The movie was a box office success as it grossed over $183 million worldwide.  

==Soundtrack==
 

The soundtrack featured three chart singles: "Renegades, Rebels and Rogues" by Tracy Lawrence,    "A Good Run of Bad Luck" by Clint Black (which also appeared on his album No Time to Kill),  and "Something Already Gone" by Carlene Carter. Also included on the album was an all-star rendition of "Amazing Grace", from which all royalties were donated to the Elizabeth Glaser Pediatric AIDS Foundation.   

==References==
{{reflist|2|refs=

   

   

   

   

   

   

   

}}

==External links==
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 