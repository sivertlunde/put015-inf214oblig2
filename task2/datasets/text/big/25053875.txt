The Tender Tale of Cinderella Penguin
{{Infobox Film
| name = The Tender Tale of Cinderella Penguin
| image =http://www.google.com/url?sa=i&source=images&cd=&docid=i8px2OJbaGdxLM&tbnid=fJXObEpeBXf80M:&ved=0CAgQjRw&url=https%3A%2F%2Fwww.nfb.ca%2Ffilm%2Fthe_tender_tale_of_cinderella_penguin&ei=YwaBU7v1Ko-JogT_oYLgDw&psig=AFQjCNFxVlH4EC6LDX5NmRIrOO6ktBZ30Q&ust=1401051107767420
| image_size =204 x 306
| caption =
| director = Janet Perlman
| producer = Janet Perlman
| writer = 
| narrator =
| starring = 
| music = 
| cinematography =
| editing =
| studio = National Film Board of Canada
| distributor =
| released = 1981
| runtime = 10 minutes
| country = Canada
| language = 
| budget =
| gross =
| preceded_by =
| followed_by =
}}

The Tender Tale of Cinderella Penguin is a 1981 animated short by Janet Perlman that comically adapts the tale of Cinderella with penguins. Produced by the National Film Board of Canada, it was nominated for an Academy Award for Best Animated Short Film at the 54th Academy Awards, losing to another animated short from Montreal, Frédéric Backs Crac. The Oscar nomination was the fourth in five years for executive producer Derek Lamb, also Perlmans husband. The film also received a Parents Choice Award.       Perlman adapted her film into the 1992 childrens book, Cinderella Penguin, published by Kids Can Press of Toronto.   

==See also==
*Bully Dance

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 
 