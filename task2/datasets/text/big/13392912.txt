Evil Laugh
 
 
{{Infobox Film
| name           = Evil Laugh
| image          = 
| caption        = 
| director       = Dominick Brascia
| producer       = Johnny Venocur
| writer         = Steven Baio  
| starring       = Myles OBrien, Dominick Brascia, Jerold Pearson, Kim McKamy
|
| music          = 
| cinematography = 
| editing        = 
| distributor    = Cinevest Entertainment Group/Baio-Brascia-Venokur Productions/Wildfire Productions/Celebrity Video Presentations March 1988
| runtime        = 87 mins USA
| English
}} slasher that touched on a tongue-in-cheek approach, with avid Fangoria reader Barney providing the voice of reason.

==Cast==
{| class="wikitable"
|-
! Actor / Actress
! Character
|-
| Kim McKamy
| Connie
|-
| Steven Baio
| Johnny
|-
| Tony Griffin
| Sammy
|-
| Jerold Pearson
| Barney
|-
| Myles OBrien
| Mark
|-
| Jody Gibson
| Tina
|-
| Howard Weiss
| Mr. Burns
|-
| Karyn OBryan
| Betty
|-
| Susan Grant
| Sadie
|-
| Gary Hays
| Jerry
|-
| Hal Shafer
| Chief Cash
|-
| Johnny Venocur
| Freddy
|-
| Tom Shell
| Delivery Boy
|-
| Dominick Brascia
| Evil Laugher
|}

==External links==
* 
*  

 
 
 
 
 
 
 

 

 