Jalatharangam (film)
{{Infobox film
| name = Jalatharangam
| image =
| caption =
| director = P Chandrakumar
| producer = RM Sreenivasan
| writer = Dr. Balakrishnan
| screenplay = Dr. Balakrishnan Madhu Sheela Adoor Bhasi Baby Babitha
| music = A. T. Ummer
| cinematography = Anandakkuttan
| editing = G Venkittaraman
| studio =
| distributor =
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed by P Chandrakumar and produced by RM Sreenivasan. The film stars Madhu (actor)|Madhu, Sheela, Adoor Bhasi and Baby Babitha in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
   Madhu 
*Sheela 
*Adoor Bhasi 
*Baby Babitha
*Kuthiravattam Pappu 
*P. K. Abraham  Sumithra  Vincent 
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Sathyan Anthikkad and Dr. Balakrishnan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aadyamaay Kandanal || K. J. Yesudas || Sathyan Anthikkad || 
|- 
| 2 || Kaakkayennulla Vaakkinartham || P Jayachandran, Shanthi, Sherin Peters || Dr. Balakrishnan || 
|- 
| 3 || Oru Sundaraswapnam || K. J. Yesudas || Sathyan Anthikkad || 
|- 
| 4 || Sakhi Sakhi Chumbanam || K. J. Yesudas || Dr. Balakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 


 