Chithramela
{{Infobox film
| name           = Chithramela
| image          = Chithramela.jpg
| caption        =
| director       = T .S. Muthiah
| producer       = T .S. Muthiah
| writer         = M. K. Mani S. L. Puram Sadanandan T. E. Vasudevan Bhavanikkutty Sreekumaran Thampi
| based on       = Sharada Sheela K. P. Ummer Kottayam Chellappan S. P. Pillai Adoor Bhasi Bahadoor Manavalan Joseph
| music          = G. Devarajan
| cinematography = N. S. Mani
| editing        = G. Venkitaraman
| studio         = Sree Movies
| distributor    = Central Pictures
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Chithramela is a 1967  . Retrieved 11 July 2011.  The cast includes Prem Nazir, Sharada (actress)|Sharada, Sheela, K. P. Ummer, Kottayam Chellappan, S. P. Pillai, Adoor Bhasi, Bahadoor and Manavalan Joseph.

==List of short films==

{|bgcolor="#d3d3d3" class="wikitable"
|-
!Short film
!Genre
!Writer
!Actors
|-
|Nagarathinte Mukhangal Crime thriller
|Screenplay: M. K. Mani Dialogues: S. L. Puram Sadanandan
|Sheela, K. P. Ummer, Kottayam Chellappan, Baby Rajani, Baby Usha
|-
|Penninte Prapancham Comedy film|Comedy
|Idea: T. E. Vasudevan Dialogues: Bhavanikkutty
|S. P. Pillai, Adoor Bhasi, Bahadoor, Manavalan Joseph, Sreenarayana Pillai, Kaduvakulam Antony, J. A. R. Anand, Meenakumari, Khadeeja (actress)|Khadeeja, C. R. Lakshmi, Devichandrika, Abhayam
|-
|Apaswarangal Romance film|Romance, Musical film|Musical, Tragedy Sreekumaran Thampi Prem Nazir, Sharada (actress)|Sharada, Thikkurissy Sukumaran Nair, Sukumari, G. K. Pillai (actor)|G. K. Pillai, Nellikkodu Bhaskaran, T. R. Omana, Master Sridhar, Vahab Kashmiri, Rajeswari, Kuttan Pillai, Prathapachandran
|}

==Plots==

===First segment (Nagarathinte Mukhangal)===
The short film showed the tragic fate of children left alone at home while the parents enjoyed themselves at late-night parties and meetings. One such couple, Sheela and Ummer, leave their daughter at home with the servant and go for a party . The naughty girl plays with the telephone, dials numbers and interacts with the person at the other end of the line. One such call results in trouble. The call was to a house which happened to be the venue of a murder. The girl says over the phone that she knew what was happening there. The murderer (Kottayam Chellappan) locates the number, kidnaps the girl and attempts to kill her. But the intervention of the police saves the girls life.

===Second segment (Penninte Prapancham)===
The characters in the film were the original film stars themselves. Adoor Bhasi, Manavalan Joseph and Bahadoor are trained to drive a car by S. P. Pillai, their ashan or teacher. They encounter hilarious situations with the women they meet. All the students and their teacher go into deep sleep. They wake up after 50 years to find that men have lost dominance; women, they find, have become all powerful in the world.

===Third segment (Apaswarangal)===
The film told the story of a tragic love affair between a blind girl and a street singer. Seetha (Sharada (actress)|Sharada), the daughter of a coolie in a colony is in love with Babu (Prem Nazir), a street singer. A city wastrels evil eye falls on Seetha and Babu is beaten up severely by his goons. A famous dancer happens to listen to Babus singing. Babu goes on to accept the patronage of the dancer, separating him from Seetha. The colony is hit by an outbreak of smallpox. Except for Seetha and a small boy all the others die. Seetha and the boy leave in search of Babu. She hears Babus voice from a theatre. Babu rushes to meet his lover. Seetha who is elated falls into his arms and dies.

==Cast==
 
*Prem Nazir
*Sheela Sharada
*Sukumari
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Manavalan Joseph
*T. R. Omana Baby
*Prathapachandran
*Abhayam
*Baby Rajani
*Bahadoor
*CR Lakshmi
*Devi Chandrika GK Pillai
*JAR Anand
*K. P. Ummer
*Kaduvakulam Antony
*Kashmiri Khadeeja
*Kottayam Chellappan
*Kuttan Pillai
*Master Sridhar Meena
*Nellikode Bhaskaran
*Rajeshwari
*S. P. Pillai
*Usha
*Wahab
 

==Production==
Veteran actor T .S. Muthiah made his directorial debut with Chithramela. He also produced the film under the banner of Sree Movies. Director M. Krishnan Nair is credited for giving technical assistance in direction. The idea for the short film Penninte Prapancham was by film producer T. E. Vasudevan. He, inturn, was inspired by a Laurel and Hardy film. 
 hyperlink format for storytelling came out. Some film pundits consider these films also as portmanteau films.

===Soundtrack===
The films music was provided by G. Devarajan. There are eight songs in the film, all of them included in the third and longest segment Apaswarangal. Lyrics were penned by Sreekumaran Thampi who also penned the script for Apaswarangal. All the songs became super hits. Seven solos by K. J. Yesudas, and a duet with S. Janaki, are considered gems in Malayalam film music.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aakaashadeepame || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 2 || Apaswarangal || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 3 || Chellacherukiliye || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 4 || Kannuneer kaayalile || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 5 || Madam Pottichirikkunna || K. J. Yesudas, S Janaki || Sreekumaran Thampi ||
|-
| 6 || Neeyevide Nin Nizhalevide || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 7 || Neeyoru Minnalaay || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 8 || Paaduvaan Moham || K. J. Yesudas || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  
*   at the Malayalam Movie Database

 
 
 
 
 
 
 
 