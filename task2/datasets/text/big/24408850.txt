Mossane
 
{{Infobox film
| name           = Mossane
| image          = 
| caption        = 
| director       = Safi Faye
| producer       = 
| writer         = Safi Faye
| starring       = Magou Seck
| music          = Yandé Codou Sène
| cinematography = Jürgen Jürges
| editing        = Andrée Davanture
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Senegal
| language       = Wolof
| budget         = 
| gross          = 
}}

Mossane is a 1996 Senegalese drama film directed by Safi Faye. It was screened in the Un Certain Regard section at the 1996 Cannes Film Festival.    Unlike some of Fayes earlier films which use a documentary style, Mossane is purely fictional. {{Citation
  | last = Ellerson
  | first = Beti
  | author-link = 
  | title = Safi Fayes Gaze: The Evolution of an African Woman’s Cinema
  | newspaper = Reference Guide to African Women in Cinema
  | pages = 
  | year = 
  | date = 
  | url = http://www.africanwomenincinema.org/AFWC/Safi_Faye_Gaze.html
  | accessdate =2009-12-19
}}
 

==Plot== Serer village, beloved by many including her own brother and Fara, a poor university student. Although she has long been promised in marriage to the wealthy Diogaye, Mossane defies her parents wishes and falls in love with Fara. On her wedding day, she refuses to marry Diogaye and tragedy ensues. {{Cite web
  | last = Brennan
  | first = Sandra
  | authorlink = 
  | coauthors = 
  | title = Mossane > Overview
  | work = 
  | publisher = Allmovie
  | date = 
  | url = http://www.allmovie.com/work/mossane-136308
  | format = 
  | doi = 
  | accessdate = 2009-12-19}}
  {{Cite book
  | last = Thackway
  | first = Melissa
  | authorlink = 
  | coauthors = 
  | title = Africa shoots back: alternative perspectives in Sub-Saharan Francophone African Film
  | publisher = James Currey
  | year = 2003
  | location = 
  | pages = 155
  | url = http://books.google.co.uk/books?id=__PzjrAIUtYC
  | doi = 
  | id = 
  | isbn =0-85255-576-8 }}
 

==Cast==
* Abou Camara as Oncle Baak
* Moussa Cissé Mbaye Diagne
* Alpha Diouf as Ngor
* Alioune Konaré as Fara
* Daouda Lam
* Ibou NDong
* Isseu Niang as Mere Mingue Diouf
* Magou Seck as Mossane
* Medoune Seck
* Guèye Seynabou
* Moustapha Yade as Samba

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 