The Indian Wars Refought
{{Infobox film
| name           = The Indian Wars Refought
| image          = File:Ad_for_1914_silent_film_The_Indian_Wars_Refought.jpg
| alt            = 
| caption        = Ad for film
| film name      = 
| director       = Theodore Wharton
| producer       = 
| writer         =  Charles King
| story          = 
| based on       =  William F. Nelson Appleton Charles King
| narrator       = 
| music          = 
| cinematography = D. T. Hargan
| editing        =  Essanay Film Mfg. Co.
| distributor    = State Rights 
| released       = August 1914
| runtime        = 5 reels
| country        = United States Silent
| budget         = 
| gross          = 
}}
 William F. Nelson Appleton Charles King, Essanay Film Mfg. Company. The film was released in August 1914, but according to modern sources, it only played in Denver and New York because of pressure from the government, which disapproved of its content because it showed the Indians in a somewhat favorable light.       It is now considered a lost film. 

According to news sources from 1917, the original film was titled Wars of Civilization, but other alternate titles for the feature include: The Last Indian Battles, From the Warpath to the Peace Pipe, The Wars for Civilization in America, Buffalo Bills Indian Wars and Indian War Pictures. 

==Synopsis== Sioux Indians. Chief Big Foot. The feature also depicted Indian war dances, burning of camps and tepees, horse rustling and scalping.  The end of the picture included scenes of Indian children attending modern schools and Indian farmers bringing in their crops. 

==Cast== William F. Cody Nelson Appleton Miles       
*Jesse M. Lee Frank D. Baldwin        Marion P. Maus Charles King
*H. G. Sickles        Short Bull
*Dewey Beard

==Production and background== Secretary of War  Lindley M. Secretary of Franklin K. 12th U. S. Cavalry and Lane authorized the participation of over 1,000 Sioux Indians. Lieutenant General Nelson Appleton Miles was hired as a technical consultant to make sure that the re-enactments were as accurate as possible, and was a cast member as well. Colonel H. G. Sickles and Charles King recreated their parts in the original battles of Wounded Knee and Warbonnet Creek, respectively. The film was shot at the sites of the original battles between September 1913 and November 1913 in the Bad Lands of South Dakota and the Black Hills of Wyoming. On February 27, 1914, the film was screened for Secretary Lane and other members of Woodrow Wilsons cabinet. After Codys death in 1917, footage from the film was used in The Adventures of Buffalo Bill, a tribute to the late Cody. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 