Romantic Heaven
{{Infobox film
| name           = Romantic Heaven
| image          = File:Romantic_Heaven_poster.jpg
| director       = Jang Jin
| producer       = Kang Woo-suk
| writer         = Jang Jin Kim Ji-won 
| music          = Lee Byung-woo
| cinematography = Kim Jun-young
| editing        = 
| distributor    = Cinema Service
| released       =  
| runtime        = 117 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =    . Box Office Mojo. Retrieved 2014-05-28. 
}}

Romantic Heaven ( ) is a 2011 South Korean melodrama about fate, love, loss, and redemption.  

Though the premise is sentimental, dealing with a variety of characters and their relationships in both life and the afterlife, it is very much in line with writer-director Jang Jins previous works, combining elements of several different genres, including romance, comedy, drama, ghost and even police thriller into an eccentric, playful and imaginative film.  

==Plot== bone marrow transplant if she is to have any hope of surviving. With great difficulty, doctors identify a potential donor, but then the man goes into flight after being accused of murder. Hoping to find him, Mimi becomes acquainted with the police detectives assigned to his case. Part two, "Wife," concerns a lawyer named Min-gyu who has recently lost his spouse. Amidst his grief, he is distracted by the fact that he cant find a bag that she had brought with her to the hospital, and which contained her personal diary. In the meantime, he is visited by an ex-convict who has a score to settle. Part three, "Girl," focuses on Ji-wook, a taxi driver whose grandfather is on the verge of death. One day his grandmother tells him that for all of his life, her husband has been unable to forget a young woman he met in his youth. It is in part four, "Romantic Heaven," that the various threads are brought together and ultimately resolved. As fate would have it, their counterparts are gazing down upon their loved ones from heaven, dealing with their own version of remorse and regret.     

==Cast==
*Kim Su-ro - Song Min-gyu
*Kim Dong-wook - Dong Ji-wook  Kim Ji-won - Choi Mimi
*Lee Soon-jae - old man (God) 
*Shim Eun-kyung - Kim Boon as a girl
*Im Won-hee - Detective Kim 
*Kim Won-hae - Detective Park 
*Lee Han-wi - Peter, the secretary
*Jeon Yang-ja - grandma
*Kim Dong-joo - Mimis mother
*Lee Moon-soo - Ha-yeons father
*Kim Byung-ok - manhole man
*Kim Jun-bae - Jang Heo-soo
*Lee Yong-yi - Kim Boon as an old woman
*Kim Jae-gun - old man
*Lee Chul-min - Eung-shik
*Lee Eun-woo - Yoon-joo
*Jung Gyu-soo - Ji-wooks doctor
*Lee Jae-yong   - Mimis doctor
*Kim Il-woong - Lee Kang-shik
*Seo Joo-ae - Park Joon-hee
*Lee Sang-hoon - deputy director of taxi company
*Kong Ho-seok - judge
*Yoo Sun - Yeom Kyeong-ja (cameo)
*Han Jae-suk - stalker (cameo)
*Kim Mu-yeol - Dong Chi-sung (cameo)

==References==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 
 
 