Vashyam
{{Infobox film 
| name           = Vashyam
| image          =
| caption        =
| director       = Suresh Unnithan
| producer       =
| writer         =
| screenplay     =
| starring       = Abhilasha Harish Kumar
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Sree Bhuvaneswari Movie Arts
| distributor    = Sree Bhuvaneswari Movie Arts
| released       =  
| country        = India Malayalam
}}
 1991 Cinema Indian Malayalam Malayalam film, directed by Suresh Unnithan. The film stars Abhilasha and Harish Kumar in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Abhilasha
*Harish Kumar

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maanasa Yamunayile || K. J. Yesudas || Poovachal Khader || 
|-
| 2 || Oru Poomaarithan Kuliril || K. J. Yesudas, KS Chithra || Poovachal Khader || 
|-
| 3 || Raavinmohamnjan || KS Chithra || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 