Idhuthanda Sattam
{{Infobox film
| name           = Idhuthanda Sattam
| image          = 
| image_size     =
| caption        = 
| director       = Senthilnathan
| producer       = Mylai R.V. Gurupadam
| writer         = Senthilnathan A. Varunan  (dialogues) 
| starring       =  
| music          = Sangeetha Rajan
| cinematography = M. Kesavan
| editing        = J. Elango
| distributor    =
| studio         = G.R.P. Films
| released       =  
| runtime        = 110 minutes
| country        = India
| language       = Tamil
}}
 1992 Tamil Tamil crime Rekha and Aamani in lead roles. The film had musical score by Sangeetha Rajan and was released on 29 May 1992.  

==Plot== SP in Mansoor Ali Khan), Dharma (K. Natraj) and Soori (Justin) to punish them. Amudha is intrigued by Selvaraj and he tells her his tragic past.
 deepavali festival, they all witnessed a murder committed by the rowdy Gurusami. Gurusami sent his henchmen to kill Selvarajs family. During the confrontation, Lakshmi and Venugopal were murdered, while Selvaraj was seriously injured and his baby spared. Gurusami was later sentenced to the death penalty but he managed to be released thanks to his henchmen.

Selvaraj is now determined to punish them.

==Cast==
*R. Sarathkumar as Selvaraj Rekha as Lakshmi
*Aamani (credited as Meenakshi) as Amudha
*Goundamani as Karuppan Senthil
*Mylai R.V. Gurupadam as Gurusami Mansoor Ali Khan as Kaali
*K. Natraj as Dharma
*Justin as Soori
*Senthilnathan as Ponnusamy
*Typist Gopu
*V. S. Raghavan as Venugopal
*V. Gopalakrishnan as Commissioner
*Idichapuli Selvaraj as Amudhas father
*Suryakanth
*Sharmili
*Dharani
*Kamala Kamesh
*Surekha
*Shanthi
*K. S. Jayalakshmi as Gurusamis father
*Baby Pushpambika as Meena (child)
*Baby Jennifer as Meena (baby)

==Soundtrack==

{{Infobox Album |  
| Name        = Idhuthanda Sattam
| Type        = soundtrack
| Artist      = Sangeetha Rajan
| Cover       = 
| Released    = 1992
| Recorded    = 1992 Feature film soundtrack |
| Length      = 10:45
| Label       = 
| Producer    = Sangeetha Rajan
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Sangeetha Rajan. The soundtrack, released in 1992, features 3 tracks with lyrics written by Muthulingam and Piraisoodan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Azhagana Poonthottam || Jayachandran, P. Susheela || 3:53
|- 2 || Kuruvi Kunju || K. S. Chithra || 3:16
|- 3 || Kuruvi Kunju || P. Susheela || 3:36
|}

==Reception==
RSP of The New Indian Express gave the film a positive review citing "the dialogue is crisp and meaningful, Senthinathans direction noteworthy. Sangeetha Rajans music is average. R. Sarathkumar flexes his muscles well. Rekha is quite satisfactory in a small role". 

==References==
 

 
 
 
 