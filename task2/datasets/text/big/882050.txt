Mannequin (1987 film)
{{Infobox film
| name           = Mannequin
| image          = Mannequin movie poster.jpg
| image_size     = 200px
| alt            = Mannequin theatrical release poster
| caption        = Theatrical release poster Michael Gottlieb
| producer       = Art Levinson Edward Rugoff executive Joseph Farrell
| writer         = Michael Gottlieb Edward Rugoff
| starring       = Andrew McCarthy Kim Cattrall Estelle Getty James Spader Meshach Taylor  
| music          = Sylvester Levay
| cinematography = Tim Suhrstedt
| editing        = Richard Halsey Frank E. Jimenez
| studio         = Gladden Entertainment MGM (2004, DVD)
| released       =  
| runtime        = 90 minutes   
| country        = United States
| language       = English
| budget         = $7.9 million 
| gross          = $42,721,196 
}} romantic comedy Michael Gottlieb, original music score was composed by Sylvester Levay. The film tells about a chronically underemployed artist named Jonathan Switcher (played by Andrew McCarthy) who gets a job as a department-store window dresser and falls in love with a mannequin (played by Kim Cattrall)—the attraction being that she comes to life on occasion, but only for him.

Mannequin received a nomination for an Academy Award for Best Original Song for its main title tune, "Nothings Gonna Stop Us Now" by Starship (band)|Starship.  The song reached #1 in the Billboard Hot 100|Billboard Hot 100 on April 4, 1987, and #1 on the UK Singles Chart for four weeks the following month.

In 1991, a sequel to the film called   was released.

==Plot==
In Ancient Egypt, Ema "Emmy" Heshire (Kim Cattrall) hides in a pyramid from her mother, who wants her daughter to marry against Emmys will. Emmy prays for the gods to get her out of the mess and to find her true love. The gods answer her prayer by making her disappear.

Philadelphia, 1987; young would-be artist Jonathan Switcher (Andrew McCarthy), takes a number of odd jobs. The first job, where he assembles a beautiful, perfect mannequin, portends the rest of the movie and is representative of his efforts. In each of these jobs, Jonathan painstakingly expresses his artistic self; but each of these early employers dismisses him for taking too much time or deviating from a set pattern.

One night, Jonathan is driving in the rain when he passes the Prince & Company department store and recognizes his "perfect" mannequin in a display window. He declares that she is the first work hes ever done that made him feel like an artist. The next morning he manages to save the owner, Claire Timkin (Estelle Getty), from being hit by a falling sign. The grateful Claire hires Jonathan, much to the chagrin of Vice President Richards (James Spader), who assigns Jonathan to be a stock boy. In his spare time, Jonathan hits it off with flamboyant window dresser Hollywood Montrose (Meshach Taylor). That night, Hollywood and Jonathan construct a window display starring Jonathans mannequin. They have a run-in with the stores night security chief, Captain Felix Maxwell (G. W. Bailey). When Jonathan is alone, the mannequin he is obsessed with comes to life as Emmy.

To the surprise of his detractors, Jonathans window-dressing for Prince & Company attracts large audiences. Jonathans arrogant ex-girlfriend Roxie (Carole Davis) and president B.J. Wert (Steve Vinovich), both of rival department store Illustra, learn about it as well. It is revealed that Richards is a corporate spy for Wert. At their board meeting, Richards wants to fire Jonathan, who is ostensibly showing off with the window displays. In contrast, the board members promote Jonathan to visual merchandising.

Emmy and Jonathans relationship snowballs over the following week. Every night, she helps him to create window displays which dazzle everyone at Prince & Company. As a result, Claire promotes Jonathan to a vice presidency.

As the window designs are bringing a tremendous amount of customers and profit for Prince & Company, people from Illustra plan to steal Emmy—not knowing that she is alive—and put her on display at Illustra. The next day, Roxie offers Jonathan work at Illustra, but is refused as he and Emmy are in love.

Maxwell and Richards break into Prince & Company and search for Emmy. The next morning, Hollywood and Jonathan discover Emmy and other mannequins missing. Jonathan suspects Illustra and dashes there, where he confronts Wert, who is dismissive regarding the stolen property. Roxie storms out of the office, swearing that Jonathan will never see Emmy ever again. Jonathan chases Roxie while being pursued by a dozen security guards. Hollywood bombards the guards with water from a fire hose as Roxie loads Emmy along with the other mannequins into a trash compactor. Jonathan charges up the trash compactors conveyor belt to rescue Emmy. She comes to life in his hands.
 breaking and entering, conspiracy, kidnapping Emmy, and grand theft. Wert, Richards and Felix are arrested instead and hauled away. Wert fires Roxie as he is being dragged out.

Emmy and Jonathan are married in the shop window of Prince & Company, with Claire as a bridesmaid, and with Hollywood as best man.

==Cast==
* Andrew McCarthy as Jonathan Switcher
* Kim Cattrall as Ema "Emmy" Hasure
* Estelle Getty as Mrs. Claire Timkin
* James Spader as Mr. Richards
* G. W. Bailey as Captain Felix Maxwell
* Meshach Taylor as Hollywood Montrose
* Carole Davis as Roxie Shield
* Steve Vinovich as B.J. Wert
* Christopher Maher as Armand

==Production==
===Development=== Michael Gottlieb, One Touch of Venus. 

The film was made based on the marketing principles of noted Hollywood market researcher Joseph Farrell, who served as an executive producer. The film was specifically designed to appeal to target demographics. McCarthy, though not a star, was cast after tests of his films showed that he strongly appealed to girls, the target audience.   

===Filming=== John Wanamakers in Philadelphia. (Now Macys Center City.) The store was given the me Prince and Company for the film. 

Additional scenes were filmed in the formal gardens behind The Hotel Hershey. Scenes taking place at the fictitious department store Illustra were filmed at the Boscovs department store (previously an E. J. Korvette that was taken over by Boscovs in 1985 after the Korvettes chain went out of business) in the former Camp Hill Mall (now Camp Hill Shopping Center) near Harrisburg, Pennsylvania.
 by PAUL WILLISTEIN, The Morning Call] accessed 17 April 2015 

Prior to the start of filming, Cattrell spent six weeks posing for a Santa Monica sculptor, who captured her likeness. Six mannequins, each with a different expression, were made. 

Cattrall later recalled, "Theres no way to play a mannequin except if you want to sit there as a dummy... I did a lot of body-building because I wanted to be as streamlined as possible. I wanted to match the mannequins as closely as I could." 

The actress later said that doing the film made her feel "grown up":
 Ive become more of a leading lady instead of, like, the girl... All the other movies that Ive done I played the girl, and the plot was around the guy. Ive never had anybody to do special lighting for me, or find out what clothes look good on me, or what camera angles are best for me... In this movie, I learned a lot from it. Its almost like learning old Hollywood techniques... Ive always been sort of a tomboy. I feel great being a girl, wearing a dress.  
Interior filming at Wanamakers took about three weeks, with shooting usually beginning around 9 pm and going until 6 am the next day.

"I had one day off," recalled Cattrall. "I went out to the Amish country. It was beautiful. I flew my sister in from Montreal. We took a buggy ride." 

==Reception==
The film had its premiere in Philadelphia.
===Box office=== Over the Top. 

===Critical response=== Siskel & Ebert and The Movies. In his print review, Roger Ebert awarded it a half star, deeming it "dead" and full of clichés.   

Rita Kempley of The Washington Post called the film "made by, for, and about dummies."  Janet Maslin of The New York Times puts the blame on the writer/director; "as co-written and directed by Michael Gottlieb, Mannequin is a state-of-the-art showcase of perfunctory technique." 

===Reception in Philadelphia=== Philadelphia Magazine, in a nod to the movies use of Philadelphia as a setting, after panning the film itself declared, "The message of Mannequin, clumsy as it is, is that the greatest place and time in recorded history is 1980s Philadelphia...Truly, this is the most uplifting film ever made about the city."   

==Sequel==
In 1991, a partial sequel called   was released and was directed by Stewart Raffill. The film was dubbed as "one of the worst follow-ups ever made." 

==Remake== Govinda and Karisma Kapoor.
 laser display hologram" as opposed to a mannequin.    However, no further details were made public about its development. 

==DVD==
Mannequin was released to DVD by MGM Home Entertainment on October 7th, 2004 in a widescreen Region 1 DVD, and was later re-released to DVD on Jan. 11, 2011 in a new double bill edition with Mannequin Two: On the Move as the second disc.

==References==
 

==External links==
 
*  
*  
*  
*  
*  at Snakkle.com
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 