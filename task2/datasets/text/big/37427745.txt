Hansel & Gretel (2013 film)
 
{{Infobox film name           = Hansel & Gretel image          = Hanselandgretel.jpg caption        = Promotional poster director       = Anthony C. Ferrante producer       =   screenplay     = Jose Prendes story          =  based on       =   (uncredited) starring       =   music          = Chris Ridenhour cinematography = Ben Demaree editing        =  studio         = The Asylum distributor    = The Asylum released       =   }} runtime        =  country        = United States language       = English budget         = $135,000   gross          =
}}

Hansel & Gretel is a 2013  .

== Plot ==
Teen siblings Hansel and Gretel Grimm are abducted by an elderly recluse named Lilith and witness the horrors of her home in the woods. Lilith and her moronic sons are involved in a conspiracy to fatten up children before she eats them and the local population to serve in meat pies in her local shop "the Gingerbread House". The plot revolves around their attempts to escape the clutches of this old age pensioner, and her mute sons.

== Cast ==
* Dee Wallace as Lilith
* Brent Lydic as Hansel Grimm
* Stephanie Greco as Gretel Grimm
* Steve Hanks as Brandon Grimm
* Clark Perry as Kevin
* M. Steven Felty as Sheriff Woody Meckes
* Adrian Bustamante as Deputy Gerry Carter
* Jasper Cole as John
* Sara E. Fletcher as Jane

== Production ==
Director Anthony C. Ferrante personally asked Dee Wallace to join the production. Wallace was drawn to her character because it was more than just a stereotypical witch. Wallace found the shooting grueling; besides issues with the budget and time limitations, temperatures rose to 100 degrees Fahrenheit, and there was no air conditioning.   Wallace performed her own stunts. 

== Release ==
The film was released  , which it preceded by five days. 

== Reception ==
Scott Foy of Dread Central rated it 3/5 stars and called it "a perfectly okay horror movie" after seeing a misleading pull quote of his comments in a previous article used as ad copy.   Mark L. Miller of AICN wrote that the film has "a few worthwhile and creative scenes of grue and gore" but frequently descends into tedious torture porn.   Dave Pace of Fangoria rated it 2/4 stars and called it "a good evening of entertainment" that, outside of one shocking and imaginative hallucinatory scene, lacks ambition.   Rod Lott of the Oklahoma Gazette wrote that it is "still technically a bad movie, but leagues above the labels usual level of dreck." 

== References ==
 

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 