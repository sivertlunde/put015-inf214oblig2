Ernest Saves Christmas
{{Infobox film
| name           = Ernest Saves Christmas
| image          = Ernest Saves Christmas Poster.jpg
| image_size     = 190px
| caption        = Theatrical release poster
| director       = John R. Cherry III
| producer       = Joseph Akerman
| screenplay     = Ed Turner B. Kline
| story          = Ed Turner
| starring       = Jim Varney
| music          = Mark Snow
| cinematography = Peter Stein
| editing        = Ian D. Thomas Silver Screen Emshell Producers Buena Vista Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $6 million (estimate)
| gross          = $28.2 million
}}

Ernest Saves Christmas is a 1988 Christmas comedy film directed by John R. Cherry III and starring Jim Varney. It is the third film to feature the character Ernest P. Worrell, and chronicles Ernests attempt to find a replacement for an aging Santa Claus. Unlike other "Ernest" movies, it does not have an antagonistic character.

==Plot synopsis==
A man who claims to be Santa Claus (Douglas Seale) arrives at the Orlando International Airport on December 23.
 taxi driver. He takes a passenger to the airport, but speeds and the passenger falls out of the taxi. Ernest later picks up Santa Claus, who tells Ernest that he is on his way to inform a local celebrity named Joe Carruthers (Oliver Clark) that he has been chosen to be the new Santa Claus. Joe had previously hosted a childrens TV program named "Uncle Joeys Treehouse" in the Orlando area similar to Mister Rogers Neighborhood with emphasis on manners and integrity with the catchphrase "They never get old. They always stay new. Those three little words, Please and Thank You." It got cancelled three weeks before and Joe must settle for a new job reading stories to children.

While they are driving, a runaway teenage girl (Noelle Parker) calling herself Harmony Starr joins Ernest and Santa in the cab. When they get to their destination, Santa possesses no legal currency (only play money), so in his giving Christmas spirit, Ernest lets him ride for free. The decision ultimately gets Ernest fired. As a dejected Ernest leaves the taxi garage, his former boss throws out Santas sack, which it turns out Santa had left behind.
 agent Marty point of view). Ernest discovers the magic power of the sack, and realizes that the owner really is Santa Claus. He and Harmony immediately set off to find Santa and return it.

On Christmas Eve, having learned of Santas imprisonment, Ernest poses as Astor Clementh, an employee of the governor and Harmony as the governors niece Mindy, and they help Santa escape from jail by convincing the police chief that Santa believing that he is Santa Claus is "infectious insanity" and he must be taken to solitary confinement. Ernest disguises himself as an Apopka snake rancher (Lloyd Worrell from Knowhutimean? Hey Vern, Its My Family Album) who sneaks Santa into a movie studio and speaks to a security guard about delivering the snakes to people who direct horror films. Meanwhile, Marty presses Joe to move on from his childrens show career, shave his beard, and instead land a part in a movie as a father and family man, since he is good at working with kids; however, it turns out to be a horror film titled Christmas Slay about an alien which terrorizes a bunch of children on Christmas Eve; this offends Santa so deeply that he punches the director in the eye.  Joe accepts the part but feels uncomfortable swearing in front of the kids on set.

Harmony is fascinated by the power of the sack, and hopes to find gifts of monetary value inside. She steals it, replacing it with one filled with feathers, and attempts to run away yet again to Miami. Santa tracks down Joe at his home, but has nothing to show except the fake sack. Joe politely but firmly declines the job.

Ernest heads to the airport to pick up the sleigh, reindeer, and two of Santas elves before they hit rush hour traffic. But their truck is disabled and Ernest decides to drive the sleigh to the Childrens Museum. His inexperience, though, leads him and the elves on a wild, out-of-control ride through Orlando. Meanwhile, Joe is overcome by conscience when the director refuses to tone down any of the profanity and violence in the film. He sees the haphazardly-flying sleigh from a distance, and realizing that Santa was telling the truth all along, is overcome with joy and runs off to find him.

Joe finds Santa at the Childrens Museum and accepts the job. Harmony returns with the real sack, apologizing to Santa (who reveals that her real name is Pamela Trenton.) The sleigh arrives just in time and Joe asks Ernest (saddened that the adventure is over) to drive for the first night, while Pamela (who decided to go home) hitches a ride as an honorary elf.

The film ends with the words, "Merry Christmas to all, and to all a good night!," followed by a sleigh dash that spells, KnoWhutImean?, one of Ernests catchphrases. Meanwhile, Chuck and Bobby were looking at the letter "E" for what it means. Suddenly, you see the Easter Bunnys ears when they come out of the cargo box, and Chuck rolls his eyes and squeals like he is screaming for help.

==Cast==
*Jim Varney as Ernest P. Worrell, Astor Clementh, Auntie Nelda, The Snake Guy
*Douglas Seale as Santa Claus (a.k.a. Seth Applegate)
*Oliver Clark as Joe Carruthers, the new Santa Claus
*Noelle Parker as Pamela Trenton, a.k.a. Harmony Starr, Mindy
*Gailard Sartain as Chuck (Storage Agent)
*Bill Byrge as Bobby (Storage Agent)
*Billie Bird as Mary Morrissey
*Key Howard as Immigration Agent
*Jack Swanson as Businessman
*Buddy Douglas as Pyramus the Elf
*Patty Maloney as Thisbe the Elf
*Barry Brazell as Cab Passenger
*George Kaplan as Mr. Dillis
*Robert Lesser as Marty Brock
*Zachary Bowden as Boy in the Train Station

==Filming locations==
*This was the first major feature production filmed almost entirely in Orlando, Florida at the then-unfinished Disneys Hollywood Studios|Disney-MGM Studios. Verns house was actually a façade located on Residential Street at the park, and was part of the Studio Backlot Tour until it was demolished in 2002. Epcot Center Church Street Orange Avenue Bee Line Orlando AMTRAK station. Scenes that take place in the movie studio and its hallways were shot at the facilities of the local FOX affiliate WOFL which in the mid-1980s had its own custom promo featuring the Ernest character. A small number of scenes were filmed in Nashville, Tennessee|Nashville. 

==Reception==

The movie was not a critical success, but remains to this day a cult classic, especially among Ernest fans. Seales performance is now regarded as one of the best interpretations of Santa Claus in movie history.    

===Box office===
In the opening weekend the film opened at #2 at the box office and grossed $5,710,734 from 1,634 theaters. Its final domestic grossing was $28,202,109. 

==References==
 

==External links==
 
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 