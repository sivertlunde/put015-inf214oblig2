Theirs Is the Glory
 
 
{{Infobox film
| name           = Theirs is the Glory
| image          = Theirsistheglory.jpg
| caption        = Detailing the battle plans in Theirs is the Glory
| director       = Brian Desmond Hurst
| producer       = Leonard Castleton Knight
| writer         = Louis Golding and Terence Young
| starring       = 
| music          = Guy Warrack
| cinematography = C.M. Pennington-Richards
| distributor    = Gaumont British
| released       = 14 October 1946
| runtime        = 82 min.
| country        = United Kingdom English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1946 British World War A Bridge Too Far depicts the operation as a whole and includes the American, British and Polish Airborne forces, while Theirs is the Glory focuses solely on the British forces, and their fight at Oosterbeek and Arnhem.

The film was directed by Ulsterman Brian Desmond Hurst, who was himself a veteran of the First World War, having survived Gallipoli where he had served with the Royal Irish Rifles. Hurst was an accomplished film director having been mentored by John Ford in Hollywood and directing more than 30 films including Malta Story, Scrooge (1951 film)|Scrooge and Tom Browns Schooldays. Hurst was also Irelands most prolific film director of the 20th Century. The producer was Leonard Castleton Knight, Head of Gaumont British News.  The script was written primarily by Louis Golding but honed by Hursts protege Terence Young (who subsequently went on to direct the early Bond films). Young had been with 30 Corp and the Irish Guards seeking to relieve Arnhem during the battle and hence the authenticity of the eventual story-line. The veterans who starred in the film also actively collaborated on the script. 

==Production==
Using the original locations of the battle, the film featured veterans who were actual participants in the battle. The film was jointly produced by the J. Arthur Rank Organisation and the Army Film and Photographic Unit (AFPU). 

Weaving original footage from the battle with re-enactments shot on location at Oosterbeek and Arnhem, the film was shot a year after the battle had ravaged the Dutch streets. As well as veterans, the film also features local people like Father Dyker (a Dutch civilian priest who conducts the service in the movie) and Kate ter Horst (who reads a psalm to the wounded men in the cellar) re-enacting their roles and what they did for the airborne troops during the battle.

Though no credits appear before or after the film, over 200 veterans appeared as actors including Majors CFH "Freddie" Gough and Richard "Dickie" Lonsdale, Lieutenant Hugh Ashmore, Sergeants Jack Bateman and John Daley, Corporal Pearce and Privates Tommy Scullion, Peter Holt, David Parker, George ‘Titch’ Preston, Frank ‘Butch’ Dixon, and war correspondents Stanley Maxted and Alan Wood. Each veteran was paid £3 per day by the Rank Organisation.

The narrator notes the soldiers as "just ordinary men". "But when you next watch the movie look closely at the faces of the men and especially at their eyes in the many close shots that Brian Desmond Hurst arranged. When you look into the eyes you will start to gain a little bit of the experience that those ordinary men went through". Theirs is the Glory - 65th Anniversary of the filming of the movie, Ministory number 106, author Allan Esler Smith, published by Friends of the Airborne Museum Oosterbeek, November 2010 

The film may have been the "Help For Heroes of its day". Hursts biographer, Allan Esler Smith in "Theirs is the Glory - 65th Anniversary of the filming of the movie" explains that "The popularity of Theirs is the Glory allowed Arthur Rank, the head of the Rank Organisation, to fulfill his pledge to help the Airborne Forces Security Fund. Collections, raffles and parades rode the wave of enthusiasm that swept the United Kingdom following its premier. The Earl Mountbatten subsequently received a cheque for £50,000 for the Airborne Forces Security Fund from Arthur Rank".

Brian Desmond Hurst said, "The film is my favourite because of the wonderful experience of working with soldiers, and because it is a true documentary reconstruction of the event. I say without modesty it is one of the best war films ever made". 

==Comparison with A Bridge Too Far==
The two films were compared in the October 2010 magazine Against All Odds and the comparison is stark and revealing, A Bridge Too Far ...is a slow moving epic, well worth a viewing with some authentic scenes, but is unconvincing in its portrayal of the battle of Oosterbeek... Theirs is the Glory is the only feature film currently released that accurately portrays the events at Oosterbeek in atmospheric and chronological terms, despite its jerky portrayal of events.  This is a film to watch.". 

==Articles and books==
Revisiting Theirs is the Glory by Allan Esler Smith, published by Robert Sigmond Publishing for the  , 21 September 2012.

Pegasus The Journal of the Parachute Regiment and Airborne Forces. An analysis and review of the making of Theirs is the Glory and running to over 20,000 words and published over 4 parts. Winter 2012 Part 1: Theirs is the Glory; the epic film of the Airborne Forces at Arnhem starring the veterans of the battle.  2012 Yearbook Part 2: The veterans tribute to every man that fought at Arnhem. Summer 2013 Part 3: Arnhem the greatest drama ever told. Winter 2013 Part 4: Roll of Honour: Arnhem veterans- the film stars without credit. All by Allan Esler Smith.

==References==
 

== External links ==
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 