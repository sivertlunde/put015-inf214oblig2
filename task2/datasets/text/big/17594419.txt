Green Dolphin Street
 
{{Infobox film
| name           = Green Dolphin Street
| image          = Green Dolphin Street.jpg
| caption        =
| director       = Victor Saville Carey Wilson
| based on       =   Carey Wilson Richard Hart
| music          = Bronislaw Kaper
| cinematography = George J. Folsey George White
| distributor    = Metro-Goldwyn-Mayer
| released       = 5 November 1947
| runtime        = 142 minutes
| country        = United States
| language       = English
| budget         = $4,391,000  .  Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 400 
| gross          = $7,173,000 
}}
 historic drama film released by Metro-Goldwyn-Mayer and based on the novel by Elizabeth Goudge.

==Plot summary== Richard Hart). Having settled in New Zealand, William writes a letter to the family proposing marriage to one of the sisters. Because he is drunk, he mistakenly writes the name "Marianne" instead of "Marguerite" It is actually Marguerite that he loves and wants to marry.  Marianne, believing he wants to marry her, decides to set off for New Zealand to be with her intended.

==Cast==
* Lana Turner as Marianne Patourel
* Van Heflin as Timothy Haslam
* Donna Reed as Marguerite Patourel Richard Hart as William Ozanne
* Frank Morgan as Dr. Edmond Ozanne
* Edmund Gwenn as Octavius Patourel
* Dame May Whitty as Mother Superior
* Reginald Owen as Captain OHara
* Gladys Cooper as Sophie Patourel
* Moyna Macgill as Mrs. Metivier
* Linda Christian as Hine-Moa
* Bernie Gozier as Jacky-Poto
* Patrick Aherne as Kapua-Manga
* Al Kikume as A Maori
* Edith Leslie as Sister Angelique
* Ramsay Ames as Corinne
* Gigi Perreau as Veronica Douglas Walton as Sir Charles Maloney

==Production background== Richard Hart, Carey Wilson.

Turner and Heflin reprised their roles in a Lux Radio Theatre version of Green Dolphin Street on 19 September 1949.

Hart and Heflin, who played romantic rivals in Green Dolphin Street, were similarly cast in B.F.s Daughter (1948). Hart made only four feature films before his death at an early age, two of them co-starring Heflin.

==Reception==
The film was one of the most popular movies at the British box office in 1948  and MGMs most popular movie of 1947. It earned $4,304,000 in the US and Canada and $2,869,000 elsewhere, but because of its high cost only recorded a profit of $339,000.  

==Awards== Sound Recording (Douglas Shearer) and Special Effects.   

==Theme song== title song, Green Dolphin Street" (often recorded as "On Green Dolphin Street"), went on to become a jazz standard. The song has been recorded by Bill Evans, Eric Dolphy, The Modern Jazz Quartet, Tony Bennett, Miles Davis, Ahmad Jamal, John Coltrane, Agnieszka Hekiert, *Dick and Kiz Harp, and Grant Green among others.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 