I Am Michael
{{Infobox film
| name           = I Am Michael
| image          = 
| caption        =  Justin Kelly
| producer       = {{Plainlist|
*James Franco
*Vince Jolivette
*Michael Mendelsohn
*Joel Michaely
*Scott Reed
*Ron Singer
}}
| writer         = Justin Kelly
| based on       =  
| starring       = {{Plainlist|
*James Franco
*Zachary Quinto
*Emma Roberts
*Charlie Carver
}}
| music          = {{Plainlist|
*Jake Shears
*Tim K
}}
| cinematography = 
| editing        = 
| studio         = {{Plainlist|
*RabbitBandini Productions
*Patriot Pictures, LLC.
}}
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $2.5 million 
| gross          = 
}}
 Justin Kelly. Filming ran in New York City from August 11 to August 30, 2014.

== Plot ==
The controversial true story depicts a gay activist Michael Glatze who rejects his homosexuality and becomes a Christian pastor.

== Cast ==
* James Franco as Michael Glatze,  a gay activist who renounces homosexuality.
* Zachary Quinto as Bennett,  an architect and former boyfriend of Michael.
* Emma Roberts  as Rebekah Fuller, a young Christian woman who Michael falls in love with after his conversion.
* Avan Jogia as Nico,  a gay Buddhist Michael meets and briefly falls for.
* Charlie Carver as Tyler,  a college student who gets involved in a relationship with Michael and Bennett.
* Daryl Hannah as Deborah,  who runs a Colorado Buddhist retreat where Michael goes to find solace.
* Devon Graye as Cory
* Leven Rambin as Catherine
* Jenna Leigh Green

== Production ==
On April 1, 2014, James Franco was added to the film to play the gay activist Michael Glatze, who renounced his homosexuality.    Justin Kelly was set to direct the film Michael based on Benoit Denizet-Lewis New York Times Magazine article "My Ex-Gay Friend", while Gus Van Sant would executive produce the film.  On July 14, Zachary Quinto, Emma Roberts, and Chris Zylka joined the cast of the film. Quinto would play Bennett, the former boyfriend of Francos character, Roberts would play his girlfriend, and Zylka would play another past love interest.   

Director Justin Kelly said that "this isnt just a story about an ex-gay, its actually a very relatable story about the power of belief and the desire to belong".  On August 14, Charlie Carver joined the film to play Tyler, who gets involved in a relationship with Michael and Bennett and is heartbroken when Michael rejects his homosexuality.    On August 15, Avan Jogia joined the film as Nico, a young man whom Michael meets and falls for along his journey.    On August 18, Lesley Ann Warren joined the films cast to play Quintos characters mother.    On August 25, Daryl Hannah joined the film to play Deborah, who runs a Colorado Buddhist retreat where Michael goes to find solace.   

=== Filming === Ocean Avenue East Rockaway.  The production wrapped on August 31, 2014 in Baldwin, New York.

==Release==
The film was released at the 2015 Sundance Film Festival, and also screened in the Panorama section of the 65th Berlin International Film Festival.    The film will be the opening night film of the Frameline Film Festival on June 18, 2015 at the Castro Theatre in San Francisco. 

==Reception==
The film drew mostly mixed to mildly positive reviews.  Writing for Variety, Peter Debruge wrote, "An unusually nuanced James Franco carries this complex portrait of a man who came out and fought to encourage other LGBT youth before converting to conservative Christianity," but said that the film was too dry and kept the title character too distant and veiled in its attempt to be impartial.   Boyd van Hoeij wrote in The Hollywood Reporter, "Thankfully, the screenplay doesn’t portray the story in simple terms of good or evil, but that doesn’t mean that there’s quite enough nuance or insight to constantly elevate the material above the level of a well-made-but-TV-ready biopic."   Indiewires Eric Kohn gave the film a B+ grade. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 