The Adventure of Sudsakorn
{{Infobox film
| name = The Adventure of Sudsakorn
| image = Sudsakornposter.jpg
| caption = Poster
| director = Payut Ngaokrachang
| producer = 
| writer = Payut Ngaokrachang Sunthorn Phu (main character)
| starring =
| music = 
| cinematography =
| editing =
| distributor = 
| released = April 13, 1979 (Thailand)
| runtime = 82 min.
| country = Thailand Thai
| budget = 
}} animated fantasy Songkran Day, April 13, 1979.  Since then, it has occasionally been seen at film festivals around the world but has not been made available for international audiences on DVD or video.

The story is based on Phra Aphai Mani, a 30,000-line epic written by Thailands best-known poet, Sunthorn Phu. In 2006, the story was adapted into a Thai live-action fantasy film, The Legend of Sudsakorn.

==Plot==
Sudsakorn, the son of a mermaid and a minstrel prince, fights on different occasions, an elephant, shark, and dragon horse, and encounters in his meanderings a king, a hermit, a yogi, a magic wand, and ghosts.

==Background==
Production started in 1976 and was plagued with shortages of capital, personnel and equipment. For the first six months, the crew had 100 workers, but by the second year their numbers were reduced to nine.

"I made a lot of my equipment from pieces I got from junk of World War II military surplus," Payut told writer John A. Lent. "Id find a screw here, a crank there, etc. I used a combat camera and adapted it. I pulled together pieces of wood, aluminum, whatever I could find."

The intense, detailed work on Sudsakorn impaired Payuts eyesight. "I did all the key drawings myself, even the layout and design ... I was almost blind from doing that film and now I wear contacts. My right eye is long, my left is short, crooked because of all that detailed work."

==See also==
*List of animated feature films
*List of films based on poems
*Khan Kluay (Thailands first computer-animated film)

==References==
* Lent, John A. (April 1997).  . Animation World Magazine, Issue 2.1.
* Danutra, Pattara and Himes, Robert (January 1, 2004).  ,  .

==External links==
* 
* 

 
 
 
 
 
 
 