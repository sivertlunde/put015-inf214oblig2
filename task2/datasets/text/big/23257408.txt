Murder C.O.D.
{{Infobox film
| name           = Murder C.O.D.
| image          = 
| image_size     = 
| caption        = 
| director       = Alan Metzger
| producer       = Harel Goldstein (line producer) Perry Lafferty (executive producer) May Quigley (associate producer) Fred Whitehead (producer)
| writer         = Barbara Paul (novel Kill Fee) Andrew Peter Marin (teleplay)
| narrator       = 
| starring       = Patrick Duffy
| music          = Fred Karlin
| cinematography = Bernd Heinl
| editing        = Pamela Malouf
| distributor    = NBC
| released       =  
| runtime        = 100 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 American TV-film directed by Alan Metzger starring Patrick Duffy and William Devane based on the Barbara Paul novel Kill Fee.

The movie was filmed in Portland, Oregon in May 1990, including on the South Park Blocks. 

== Plot summary ==
Police officer Steve Murtaugh is blackmailed for having an extramarital affair. Moving with his wife from Chicago to Portland Oregon, the blackmailer follows.

== Cast ==
*Patrick Duffy as Steve Murtaugh
*Chelsea Field as Ellie
*Alex Hyde-White as Corbin
*Harris Laskaway as Jerry Walsh
*Janet Margolin as Maye Walsh
*Allan Miller as Leon Walsh Charles Robinson as Lt. Silk
*Mariette Hartley as Sally Kramer
*William Devane as Alex Brandt

==References==
 

 

 
 
 
 
 
 
 
 
 


 
 