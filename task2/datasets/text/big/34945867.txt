Dobol Trobol: Lets Get Redi 2 Rambol!
{{Infobox film
| name             = Dobol Trobol: Lets Get Redi 2 Rambol!
| image            = 
| caption          = 
| director         =  Tony Y. Reyes
| producer         = Olga Bautista Mhalouh Crisologo Elmer Cruz Erdie Macaraig
| writer           = R.J. Nuevas Tina Velasco Mon Roco
| starring         = Dolphy Vic Sotto Carmi Martin Jose Manalo Wally Bayola Riza Santos
| cinematography   = Gary Gardoce
| editing          = Kelly Cruz
| distributor      = APT Entertainment M-Zet Productions RVQ Productions
| released         =  
| runtime          = 
| country          = Philippines
| language         =  
| budget           =
| gross            = P90,507,251
}}
 Filipino comedy film starring Dolphy and Vic Sotto. The film was a box office success, grossing P90.5 million.

==Plot==
The movie starts at a resort where Mac works as a chef,with Nemo as sous chef.They were serving foreigners,including some Americans and a Japanese tourist,played by Ya Chang.When pickpockets attacks the customers,Mac uses his pots,spatula and pans to inflict damage on the crooks.Meanwhile,Arthur,along with a gang,tried to target Macs friend and the resort owner,but Arthur relented,repulsed the gang members and rescued Mr.Toribio.In turn,he was given a calling card for a job.
 molar was pulled also.

All things change when he saw Macs daughter,Boni arrives at the resort.He fell in love at her but sot shocked when he found our that Boni is Macs daughter.He tried to advance Boni,to no avail,including staling Macs flowers (courtesy of Nikki),meeting up with her,and most of all,via a paper plane,but got intercepted by Mac,and the paper plane became a Japanese Zero plane,firing bullets before crashing down.When Turo reads it,it was full of swear words,just as Mac goes out of the terrace,with Japanese tie in the forehead,a katana in right hand and a Japanese flag in the left,acting like a cross betweena Japanese samurai and a Japanese soldier,while shouting "Tora,Tora,Turo!Turo,Bakero!Kamikaze-ne!Banzai Nippon!Turo Pugot Uro!Turo Pugot Uro!"("Tora,Tora Turo!Turo,Fool!Kamikaze!Long Live Japan!").Turo runs away scared and Mac scolds her daughter,saying "kiri kiri ne!chorva chorva ne!"and ends with saying "irrashaimase!"("thank you very much!") while bowing.

Macs problematic past is revealed when Boni wants her daddy to attend her birthday along with her mother,a rich person.She wants to stop Macs love,being a chef and focus on her company as owner,much to her disagreement and ends up leaving the house to help Toribios resort.He was affected on his job that the couple that wants to invest at the resort became pissed and cancelled the investment.In order to help him,Turo,Mr.Toribio,Nemo and Bogart tricked Mac by telling them they have a catering service in Manila,but in Bonis birthday.Meanwhile in a convenience store,a confrontation ensued when 2 perverted men tried to harass 2 customers (Pia Guanio and Zsa Zsa Padilla).They defend the women and thanks them,Mac pulling an anti-asthma medicated nebulizer.Mac was shocked when he learned when he arrived at his own house.He was confronted by her wife,for not being a husband and a boss on the family.There is a chef,named Paco,an overly perfectionist,grammar Nazi gay chef from France.He is the head chef,while Nemo,Bogart and Arthur became assistant,cook and server,respectively.Problem arose when Paco was accidentally knocked out by a rolling pin,replacing him with Mac by force.The party continued and the chef was introduced,Mac.His wife and daughter became happy and his wife praises him.After the end of the party,Mac stayed at his home,while Arthur,Nemo and Bogart return to the resort.

Mac saw Turo packing his clothes,fulfilling his promise to Mac that he will leave the resort if he reconcile with his family,and to end his feelings to her daughter,but Boni disagrees and states that she loves Arthur.The couples hugged each other,as Nemo,knocks Bogarts head.

==Cast==
*Dolphy† as Macario/Mac
*Vic Sotto as Arthur Calaycay/Turo
*Carmi Martin as Gabriela
*Jose Manalo as Nemo
*Wally Bayola as Bogart
*Riza Santos as Boni
*BJ Forbes as Moo/Nikki
*Pocholo Montes as Mr. Toribio
*Fritz Ynfante as Chef Paco
*Sugar Mercado as Information Officer
*Ricky Davao as guest/investor
*Dexter Doria as guest/investor
*Ya Chang as guest
*Daiana Menezes as Vivian
*Epi Quizon as guest
*EB Babes as cameos
*Pia Guanio as convenience store customer
*Zsa Zsa Padilla as convenience store customer

===Cameo===
*Zsa Zsa Padilla
*Pia Guanio

==Reception==

===Box office===
The film was a box office success. The film grossed P44.6 million on its opening weekend. The film grossed a total of P90.5 million on its entire theatrical run. 

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2009
| rowspan="1" align="left"| GMMSF Box-Office Entertainment Awards 
| align="left"| Comedy Box-Office King/s 
| align="center"| Dolphy and Vic Sotto
|  
|}

==References==
 

 
 
 
 