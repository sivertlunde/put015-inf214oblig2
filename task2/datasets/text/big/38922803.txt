Chhayamoy
 
{{Infobox film
| name           = Chhayamoy
| image          = Chhayamoy 2013 Bengali film poster.jpg
| image_size     = 260px
| border         = 
| alt            =
| caption        = 
| director       = Haranath Chakraborty
| producer       = 
| writer         = Sirsendu Mukhopadhyay
| screenplay     = 
| story          = Chhayamoy
| based on       =  
| narrator       =  See below
| music          = Debojyoti Mishra
| cinematography = Souvik Basu
| editing        = Rabiranjan Moitra
| studio         = Rosevalley films
| distributor    = 
| released       =   
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}} Bengali film directed by Haranath Chakraborty and based on Shirshendu Mukhopadhyays novel with same title. This was a childrens film with supernatural elements in it. Debajyoti Mishra composed the music of the film.         

== Plot ==
Indrajit is a UK based scholar. While working to preserve historical documents he find a parchment from which he learns about treasures hidden in an old palace in Simulgarh, a village of West Bengal. Indrajit comes to the village, finds out the treasure, but, a local goon Gagan Sapui accuses him of robbery, beats him up and expels him out of the village. Sapui wants to melt those coins to make new jewellery. After being beaten up, Indrajit goes to forest near the village where he meets Chhayamoy, a benevolent ghost. After listening the incidence from Indrajit, Chhayamoy decides to teach him a lesson.   

== Cast ==
* Gaurav Chakraborty as Indrajit
* Sabyasachi Chakrabarty as Chhayamoy/Chandro Kumar
* Dipankar Dey as Gagan Sapui
* Paran Bandopadhyay as Thakurda (Grand Father)
* Adhiraj Ganguly as Alankar
* Shantilal Mukherjee as Kapalik Kali
* Debesh Roychowdhury as Alankars Father

== See also ==
* Ashchorjyo Prodeep

== References ==
 
 
 
 


 