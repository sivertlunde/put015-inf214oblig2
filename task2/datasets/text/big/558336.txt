Garden State (film)
{{Infobox film
| name           = Garden State
| image          = Garden State Poster.jpg
| caption        = Theatrical release poster
| director       = Zach Braff
| producer       = Gary Gilbert Dan Halsted Pam Abdy Richard Klubeck
| writer         = Zach Braff
| starring       = Zach Braff Ian Holm Method Man Natalie Portman Peter Sarsgaard 
| music          = Alexi Murdoch Chad Fischer
| cinematography = Lawrence Sher
| editing        = Myron I. Kerstein Camelot Pictures Jersey Films Double Features Films
| distributor    = Fox Searchlight Pictures  (North America)  Miramax Films  (International) 
| released       =      
| runtime        = 112 minutes 
| country        = United States
| language       = English
| budget         = $2.5 million
| gross          = $35,825,316
}}
Garden State is a 2004 comedy-drama film written and directed by Zach Braff and starring Natalie Portman, Peter Sarsgaard, Ian Holm and Braff himself. The film centers on Andrew Largeman (Braff), a 26-year-old actor/waiter who returns to his hometown in New Jersey after his mother dies. Braff based the film on his real life experiences.

It was filmed in April and May 2003 and released on July 28, 2004. New Jersey was the main setting and primary shooting location.  . Rotten Tomatoes. Retrieved December 18, 2013. 
 a soundtrack for which Braff, who picked the music himself, won a Grammy Award.

==Plot==
Andrew Largeman (Braff) wakes up from a dream—in which he apathetically sits on a crashing plane—to a telephone message from his father (Holm), telling Andrew that he needs to return home because his mother has died.
 Alex Burns), who invite him to a party that night. At home, his father gets Andrew a doctors appointment for headaches hes been having.
 ecstasy but remains detached. 
 pathological liar. lithium and other mood stabilizers, as well as antidepressants, for his entire adult life, but has recently stopped taking them. He also says that his father, who is his psychiatry|psychiatrist, put him on the medication. Andrew finds Sam outside the office and offers her a ride home. Sam invites him into her house, and he meets her mother, who inadvertently reveals that Sam has epilepsy. Andrew tells Sam of his mothers death, and Sam tearfully eulogizes her hamster. After returning home, Andrews father confronts him and is insistent that they have a talk before Andrew leaves.

Later, Andrew and Jesse sit in the cemetery as Mark digs another grave. Andrew observes Mark stealing jewelry from the corpse he is burying. Andrew then returns to Sams house, and the two spend the rest of the day together, joining his friends later at Jesses mansion. Andrew tells her that when he was nine years old he pushed his mother in frustration, knocking her over a broken dishwasher in an accident that left her paraplegia|paraplegic; he says that his father blames him for his wifes paralysis and put him on his medications to "curb the anger" he supposedly harbors. Sam listens, and Andrew then admits his feelings for her.
 Newark where Mark talks to a man named Albert (Denis OHare), who is employed in keeping intruders out of the quarry. The three visitors discuss the reasons for which Albert and his wife choose to live in the quarry. Albert explains that living there and exploring the quarry is "doing something thats completely unique, thats never been done before," mirroring an earlier speech by Sam. Finally, Albert explains that what actually matters is living with his family. Andrew is inspired by the conversation, and outside in the rain, he climbs atop a derelict crane and screams into the quarry, joined by Sam and Mark. He and Sam then share a kiss.

When Mark and Andrew look at the gift later on, it turns out to be Andrews mothers favorite pendant, one of the items Mark stole from her grave, sold, and subsequently located. Andrew eventually talks with his father, and states that he was not to blame for his mothers accident and that he will live the rest of his life without medications. He forgives his father and says he wants to build a better relationship with him.

The morning after, Andrew says his goodbyes to Sam at the airport, while she begs him not to leave. He acknowledges that she has changed his life but that he still has to fix his personal problems before continuing the relationship. Andrew boards the flight, and Sam is left crying in a telephone booth. Andrew then returns, saying that he doesnt want to waste any more of his life without Sam. He wonders what to do next, and the two then kiss.

==Cast==
*Zach Braff as Andrew Largeman, a clinical depression|depressed, heavily medicated, struggling young actor who waits tables for a Vietnamese restaurant. When he was nine years old, he accidentally paralyzed his mother by pushing her over a dishwasher door. He hasnt cried or felt any significant emotions for several years, mainly as a result of the medication hes been supplied with by his estranged father. compulsive liar, who openly admits her casual deception and frequently ponders what makes her do it. She works at a law office, and lives with her equally peculiar mother and her adopted African sibling, Titembay.
*Peter Sarsgaard as Mark, an old school friend of Andrew, now working as a grave-digger. He still lives with his mother and smokes marijuana, frequently attending wild parties; he also makes money by stealing jewelry from the people he buries and exploiting loopholes in store return policies. lithium and other medications to "control" Andrews emotions in a futile attempt to bring happiness to his family.
*Jean Smart as Carol, Marks mother, a recovering alcoholic, who sees a wealth of potential in her son.
*Armando Riesco as Jesse, Another school friend of Mark and Andrew who has made a fortune and bought a mansion on money he has earned from inventing a silent alternative to Velcro fabric.
* s "Three Times a Lady" at her sister-in-laws funeral.
*Method Man as Diego, a bellhop at a luxury hotel who hosts peeping sessions of various hotel rooms. Alex Burns as Dave: Another old school friend who now works as a grave-digger with Mark.
*Ron Liebman as Dr. Cohen, a neurologist whom Andrew visits at the beginning of the film.
*Denis OHare in a cameo appearance  as Albert: One of the "guardians of the abyss." canon Klingon was used).
*Michael Weston as Kenny the Cop, a former classmate of Andrews from high school, he is now employed as a police officer in their New Jersey neighborhood.
*Ann Dowd as Olivia
*Ato Essandoh as Titembay, Sams adopted brother

==Production==
Garden State was Braffs feature directing and writing debut. The title of the film was originally intended to be Larges Ark, in reference to Braffs character (note that Albert mentions his own ark in the movie), but he changed it because no one understood what it meant.    The title alludes both to the nickname for New Jersey, and to lines from Andrew Marvells poem "The Garden" ("Such was that happy garden-state/While man there walked without a mate").
 Tenafly and Wallington as well as New York City and Los Angeles.

Braff has cited such films as Harold and Maude, Woody Allen films (specifically Annie Hall and Manhattan (film)|Manhattan), and the films of Alexander Payne as influences on Garden State.    Parallels have also been drawn between Braffs film and Ted Demmes Beautiful Girls (film)|Beautiful Girls (1996). Braff wrote the script during his college years when Beautiful Girls was in theatres, and his first choice for the love interest was Natalie Portman, who played a young adolescent with uncommon insight in Demmes film.  In some ways Portmans role in "Beautiful Girls" could be seen as the girl who would grow into the young woman in "Garden State."

The film is partly autobiographical, depicting Braffs own emotions while he was writing the screenplay. He described that "When I wrote Garden State, I was completely depressed, waiting tables and lonesome as Ive ever been in my life. The script was a way for me to articulate what I was feeling; alone, isolated, a dime a dozen and homesick for a place that didnt even exist."   

==Music==
 
The music that accompanied the film was hand-picked by Zach Braff. Commenting on the selections, Braff said that "Essentially, I made a mix CD with all of the music that I felt was scoring my life at the time I was writing the screenplay."    Braff used many artists he used in other works.
 Best Compilation Soundtrack Album for a Motion Picture, Television or Other Visual Media. The films trailer won an award for Best Music at the Golden Trailer Awards. The Broadcast Film Critics Association nominated it for Best Soundtrack.

==Themes==
The protagonists father has been "protecting" him from his own feelings with pills, namely lithium carbonate, which are seen "as the symbolic soul-destroying enemy".  Zach Braff describes the themes of the movie as "love, for lack of a better term. And its a movie about awakening. Its a movie about taking action. Its a movie about how life is short, go for it now. My character says, Im 26 years old, and Ive spent my whole life waiting for something else to start. Now I realize that this is all there is, and Im going to try to live my life like that".  "I have this theory that your body goes through puberty in its teens, and the mind goes through puberty in your twenties," he says. "  is dealing with issues that you are going through all the time going into your thirties. Hes lost and lonesome, which is something I definitely felt in my twenties". 

==Reception== USD $5 Peter Rice said of the film, "Having enjoyed the film immensely, we look forward to working with Miramax to bring Garden State to audiences worldwide."    From March until mid July, it screened at other various film festivals until it received a limited release on July 28 in North America. It became only the fourth non-documentary feature to top the chart that year, as calculated by per screen average, since Memorial Day weekend.      Stephen Gilula, president of distribution at Fox Searchlight, attributed the films gradual success to word of mouth and a publicity tour by Braff leading up to the films theatrical debut. Gilula said, "Zach   had a cross-country tour, and we   word of mouth screenings, where we had to turn people away. Zach did Q&As following  ."  From late 2004 through mid-2005, Garden State was shown at more festivals and was released in over 30 countries. In limited release, the film earned about $26.7 million in North American box office, and a total $35.8 million worldwide. 

Garden State has an 86% "Fresh" rating on Rotten Tomatoes.  Empire (film magazine)|Empire placed Garden State at number 393 on their list of the 500 Greatest Films Of All Time.  In 2014, Jon Dolan of Rolling Stone referred to Garden State as the film "that helped make Hollywood safe for indie pop". 

===Awards===
In addition to being a nominee for the Grand Jury prize at the 2004 Sundance Film Festival, Braff received Best New Director from the Chicago Film Critics Association, the Florida Film Critics Circles Pauline Kael Breakout Award, Best Debut Director award from the National Board of Review of Motion Pictures, Breakout of the Year from the Phoenix Film Critics Society. and Hollywood Breakthrough Director of the Year Award at the Hollywood Film Festival.

==Home media release==
After its limited release in theaters, the film gained more popularity during its DVD release on December 28, 2004, which includes commentaries, deleted scenes, and featurettes. It was first released on Blu-ray in the UK on February 19, 2012. 

The Blu-ray version of the movie was released in the U.S. on March 4, 2014 by Fox. 

==References==
{{Reflist|30em|refs=

   

}}

==External links==
 
*  
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 