Welcome to Dongmakgol
{{Infobox film
| name           = Welcome to Dongmakgol
| image          = Welcome to Dongmakgol poster.jpg
| caption        = Film poster
| film name      = {{Film name
| hangul         =      
| rr             = Welkeom tu dongmakgol
| mr             = Werk‘ŏm t‘u dongmakgol}}
| director       = Park Kwang-hyun
| writer         = Jang Jin Park Kwang-hyun Kim Joong
| producer       = Jang Jin Lee Eun-ha Ji Sang-yong
| cinematography = Choi Sang-ho
| editing        = Steve M. Choe
| starring       = Jung Jae-young Shin Ha-kyun Kang Hye-jung Im Ha-ryong Seo Jae-kyung Ryu Deok-hwan Steve Taschler
| music          = Joe Hisaishi
| distributor    = Showbox (South Korea) Subway Cinema (USA)
| released       =  
| runtime        = 133 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =   
}}
Welcome to Dongmakgol ( ) is a 2005 South Korean film. Based on the same-titled long-running stage play by filmmaker/playwright Jang Jin,   Park Kwang-hyuns debut film was a commercial and critical success. 

The story is set in Korea during the Korean War in 1950. Soldiers from both the North and South, as well as an American pilot, find themselves in a secluded village, its residents largely unaware of the outside world, including the war. 
 South Koreas foreign language it ranked as twentieth).

The film was released in the United Kingdom under the alternate title Battle Ground 625. 

==Plot==
A United States Naval Aviator|U.S. Navy pilot, Neil Smith (Steve Taschler), is caught in a mysterious storm of butterflies and crash-lands his plane whilst flying over a remote part of Korea. He is found by local villagers who nurse him back to health. In the small village of Dongmakgol, time appears to stand still. They have no knowledge of modern technology, such as firearms, explosives, motor vehicles and aircraft. All villagers are unaware of the massive conflict within Korea.

Meanwhile, not far from the village, a platoon of North Korean and South Korean soldiers have an encounter, and the ensuing gunfight leaves most of the North Koreans dead.  The surviving soldiers from the North manage to escape through a mountain passage. The North Korean soldiers, Rhee Soo-hwa (Jung Jae-young), Jang Young-hee (Im Ha-ryong), and Seo Taek-gi (Ryu Deok-hwan) are found by an odd girl Yeo-il (Kang Hye-jung) who acts crazy. She leads them to the village, where to their astonishment, they find two South Korean soldiers Pyo Hyun-chul (Shin Ha-kyun) and Moon Sang-sang (Seo Jae-kyung). The South Korean soldiers, who have both deserted their units, had also been led to the village which is housing the injured U.S. Navy pilot, Smith, by a different villager.

The unexpected encounter causes an armed standoff that lasts for several days.  The villagers have no idea what the stir is about, and wonder why the two sides are standing there pointing those "sticks" at each other. The confrontation ends only when a soldier holding a grenade is worn by fatigue and accidentally drops it. Another soldier heroically throws himself onto the grenade, but it does not explode. He discards the "dud" over his shoulder in contempt, and it rolls into the village storehouse and blows up the villages stockpile of corn for the winter. The remnants fall down from the sky surrealistically as popcorn.

The two groups of Korean soldiers and Smith now have to face the fact that their quarrel condemned the village to starvation in the following winter. They help the villagers in the fields to make up for the damage they have caused, and even work together to kill one of the wild boars that trouble the village. Tensions between the two groups of Korean soldiers gradually lessen, though members of both sides are haunted by the memory of terrible things they have experienced during the war.

While this is happening, Allied commanders, who have lost several other planes in the area, are preparing a rescue team to recover Smith, whom they mistakenly believe has been captured by enemy units and is being held at a hidden base. The plan: when the rescue team finds and recovers Smith, a bomber unit is to fly in and destroy the anti-aircraft guns they presume are sited in the village, which means that the innocent villagers are now in grave peril.

The rescue team, led by their commander (David Joseph Anselmo), drops in by parachute at night, suffering heavy casualties from the rough terrain. They enter the village, and under the assumption it is a cover for an enemy base, begin roughing up toward the villagers. Despite the efforts of the villagers to conceal the Korean soldiers by disguising them as villagers, a firefight breaks out in which all the members of the rescue team but one are killed and Yeo-il is fatally wounded by a bullet. The only survivor of the rescue team, the Korean translator, is hit over the head by Smith and is captured by the villagers.

Through the translator, the people in the village find out about the bombing plan. The North and South Korean soldiers realize there is no time for Smith to make it back to his base to stop the bombing. The only possible way to save the village, they decide, is to create a decoy "enemy base" using equipment from the rescue team parachute drop, so that the bombing unit will attack them instead of the village.

Smith is sent back along with the surviving rescue party member so that he can tell the Americans that there is nothing in the village to bomb, in case they decide to send more bombers. Meanwhile, the decoy is successful, and the remaining North and South Korean soldiers die smiling while a barrage of bombs explode around them. The village is saved, but at the cost of the lives of the former enemies who had later become friends.

==Production==
After being impressed with Park Kwang-hyuns 2002 short film My Nike, Film It Suda CEO Jang Jin gave him a new project: to direct an adaptation of one of his successful stage plays Welcome to Dongmakgol. The final script was the result of 18 months of brainstorming between Jang, Park, and visual supervisor Kim Joong.   
 CGI took much longer than expected, and the budget skyrocketed to  , putting Jangs company in trouble: for a small production company like Film It Suda failing with this film would have been catastrophic. 

Park had been a big fan of Japanese animation director Hayao Miyazakis work since he watched Future Boy Conan as a child. And one of Miyazakis most important collaborators was composer Joe Hisaishi. Park liked Hisaishi so much he wrote the script thinking about his music, visualizing the scenes in his mind while listening to his past work. During pre-production, producer Lee Eun-ha asked Park who the best music director for the project would be, and he didnt hesitate a moment, saying "Joe Hisaishi."  Lee then wrote a very heartfelt letter to Hisaishi, explaining their situation and translating the script in Japanese for him.    Hisaishi accepted the proposal,  later stating that he was moved by the enthusiasm and sincerity in the letter, choosing Welcome to Dongmakgol as his first ever Korean film. 

==Awards and nominations==
{| class="wikitable"
|-
! Year !! Group !! Category !! Recipient !! Result
|- 2005 || rowspan=9| Blue Dragon Film Awards || Best Film || Welcome to Dongmakgol ||  
|-
| Best Supporting Actor || Im Ha-ryong ||  
|-
| Best Supporting Actress || Kang Hye-jung ||  
|-
| Best Screenplay|| Jang Jin Park Kwang-hyun Kim Joong ||  
|-
| Best Art Direction || Lee Joon-seung ||  
|-
| Best Music || Joe Hisaishi ||  
|-
| Best Visual Effects || Jo Yi-seok ||  
|-
| Best New Director || Park Kwang-hyun ||  
|-
| Audience Choice (Most Popular Film) || Welcome to Dongmakgol ||  
|-
| rowspan=12|  Korean Film Awards  || Best Film || Welcome to Dongmakgol ||  
|-
| Best Director || Park Kwang-hyun ||  
|-
| Best Actor || Jung Jae-young ||  
|-
| Best Supporting Actor || Im Ha-ryong ||  
|-
| Best Supporting Actress || Kang Hye-jung ||  
|-
| Best Screenplay|| Jang Jin Park Kwang-hyun Kim Joong ||  
|-
| Best Cinematography || Choi Sang-ho ||  
|-
| Best Editing || Steve M. Choe ||  
|-
| Best Music || Joe Hisaishi ||  
|-
| Best Visual Effects || Jo Yi-seok ||  
|-
| Best New Director || Park Kwang-hyun ||  
|-
| Best New Actor || Ryu Deok-hwan ||  
|-
|  Directors Cut Awards || Best Actor || Jung Jae-young ||  
|- 2006 ||  Baeksang Arts Awards || Best New Director || Park Kwang-hyun ||  
|-
| rowspan=8| Grand Bell Awards || Best Film || Welcome to Dongmakgol ||  
|-
| Best Supporting Actor || Im Ha-ryong ||  
|-
| Best Supporting Actress || Kang Hye-jung ||  
|-
| Best Screenplay|| Jang Jin Park Kwang-hyun Kim Joong ||  
|-
| Best Music || Joe Hisaishi ||  
|-
| Best Visual Effects || Jo Yi-seok ||  
|-
| Best Sound || Blue Cap ||  
|-
| Best New Director || Park Kwang-hyun ||  
|}

==References==
 

==External links==
*    
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 