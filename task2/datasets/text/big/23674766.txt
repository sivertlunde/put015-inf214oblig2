Coma Girl: The State of Grace
 
Coma Girl: The State of Grace is a 2005 comedy film written and directed by Dina Jacobsen and produced by Lisa Renée.

== Plot ==

Grace Anderson never knew how to feel good and hated to feel bad, so chose to feel nothing.  Since her parents died, twenty years ago, she has been living with her perfect older sister, Mandy.  When Mandy decides that she wants to move to Norway, Grace has to take care of herself for the first time.  The process of finding a new flat leads Grace to question her job, her life choices and her very existence.  Her life then seems to spiral out of control as she quits her job of the last 15 years, that of a data processor, and attempts to find a career with purpose.

Helping her in her quest for a better, more waking life, is her friend, Megan, and numerous vox-pop interjections by well-meaning advisers.  Gradually serendipity enters her life through the very act of her questioning and she finds a new career as a stand-up comedienne, a new flatmate and eventually love.  Her life starts to make sense after all when she accepts the call to grace.

== Influence ==
In Greek mythology there is a great tradition of the hero and the monomyth|heros journey. The beginning of that journey, the pre-journey, is the necessary growth from child-hero to hero. It has three stages: abandonment, exposure and danger. By experiencing these stages and passing the various rites they entail, the child-hero grows up and embarks on their heros quest.

Grace has never grown up. She drags her feet through life, never becoming more than a partial adult, always shrinking from the demands of total responsibility. Grace finally stops blaming others for her state, thinks about someone else for a change and opens herself up to the call to grace, the call to adulthood.

 

 
 
 


 