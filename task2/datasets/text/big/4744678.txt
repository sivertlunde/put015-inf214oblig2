Raising Victor Vargas
{{Infobox film
| name = Raising Victor Vargas
| image = Raising Victor Vargas.jpg
| director = Peter Sollett
| producer = Peter Sollett Robin OHara Scott Macaulay Alain de la Mata
| writer = Peter Sollett Eva Vives
| starring = Victor Rasuk Judy Marte Melonie Diaz Silvestre Rasuk Kevin Rivera Altagracia Guzman Krystal Rodriguez Brad Jones Roy Nathanson
| cinematography = Tim Orr
| editing = Myron I. Kerstein Fireworks
| released =  

| runtime = 88 minutes
| country = United States
| language = English
| budget = $800,000 (estimated)
 }}
Raising Victor Vargas is a 2002 film directed by Peter Sollett, written by Sollett and Eva Vives. The film follows Victor, a Lower East Side teenager, as he deals with his eccentric family, including his strict grandmother, his bratty sister, and a younger brother who completely idolizes him. Along the way he tries to win the affections of Judy, who is very careful and calculating when it comes to how she deals with men. In a subplot, we also see Judys friend Melonie in her own romantic adventure.

The film was screened in the Un Certain Regard section at the 2002 Cannes Film Festival.   

==Plot==
Victor is a teenager growing up in the Lower East Side of New York. He is a cocky young man, very sure of himself in his love life. He lives in a small apartment with his strict grandmother, bratty sister Vicki, and his younger brother Nino, who is just coming into his own sexuality and looks up to his girl-crazy brother highly.

At the beginning of the film, Victor is found in the bedroom of Fat Donna, a girl that many in the neighborhood consider overweight. Word quickly spreads throughout the community amongst his friends, although Victor continuously denies it happened. As this is a huge threat to his reputation, he sets his sights on the beautiful girl of the neighborhood, Judy.

Judy is a good-looking young woman who is continuously hit-on by men in her neighborhood, 
which makes her very careful in who she chooses in terms of her love life. When Victor comes on to her, she lies, telling him she has a boyfriend. When Victor finds out this isnt true, he enlists the help of Judys little brother Carlos, on the condition that Victor introduce him to Vicki, whom he has a crush on. Judy ultimately says yes to Victors advances, believing he will serve as a repellent toward the many men that hit on her.
 sleeping together, and Melonie reveals to Harold the real reasons why Judy agreed to go out with Victor. Harold tells Victor, who goes to confront Judy. When Victor invites her over to dinner at his house, she believes hes doing so to impress his family and better his reputation. Ultimately, they decide to stay together, with Victor saying that he invited her to see his family to see who he really is.

==Production notes==
*Filmed in Super 16 mm.
*Filmed on location with non-professional actors in all of the roles.
*The films working title was "Long Way Home".
*This feature film evolved from Peter Solletts 2000 short film entitled Five Feet High and Rising, which featured many of the same cast members as Raising Victor Vargas.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 