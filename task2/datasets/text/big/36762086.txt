Znaki na drodze
{{Infobox film
| name           = Znaki na drodze
| image          = 
| caption        = 
| director       = Andrzej Piotrowski
| producer       = 
| writer         = Andrzej Piotrowski Andrzej Twerdochlib
| starring       = Tadeusz Janczar
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Poland
| language       = Polish
| budget         = 
}}

Znaki na drodze is a 1970 Polish drama film directed by Andrzej Piotrowski. The film won the Golden Leopard at the Locarno International Film Festival.   

==Cast==
* Tadeusz Janczar as Michal Biel
* Galina Polskikh as Jadwiga
* Leon Niemczyk as Paslawski
* Leszek Drogosz as Stefan Jaksonek
* Bolesław Abart as Driver Sosin
* Arkadiusz Bazak as Marian
* Ewa Ciepiela as Helena
* Janusz Klosinski as Mechanic Franciszek Wasko
* Ryszard Kotys as Mechanic Bulaga
* Zygmunt Malanowicz as Lieutenant
* Jan Peszek as Driver Bakalarzewicz
* Jerzy Block as Porter Kurek

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 