The Flying Squad (1929 film)
{{Infobox film
| name           = The Flying Squad
| image          =
| caption        =
| director       = Arthur Maude
| producer       = S.W. Smith
| writer         = Edgar Wallace (novel)   Kathleen Hayden  
| starring       = John Longden   Donald Calthrop   Wyndham Standing   Henry Vibart
| music          =
| cinematography =  
| editing        = 
| studio         = British Lion Film Corporation
| distributor    = Warner Brothers
| released       = January 1929
| runtime        = 7,572 feet 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
}} silent crime The Flying 1932 and The Flying Squad (1940 film)|1940.

==Cast==
* John Longden as Inspector John Bradley 
* Donald Calthrop as Sederman 
* Wyndham Standing as Mark McGill 
* Henry Vibart as Tiser 
* Laurence Ireland as Ronnie Perryman 
* Dorothy Bartlam as Ann Parryman 
* John Nedgnol as Li Joseph 
* Eugenie Prescott as Mrs. Schifan 
* Carol Reed as Offender 
* Bryan Edgar Wallace as Offender 
* Joan Playfair  
 
==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 