Burning Palms (film)
 
{{Infobox film
| name           = Burning Palms
| image          = Burning Palms Poster.jpg
| caption        = Theatrical release poster
| director       = Christopher B. Landon
| writer         = Christopher B. Landon
| producer       = Oren Segal Steven Prince Jason Hewitt  Tyler Thompson Vince Morella  Naz Jafri
| starring       = Dylan McDermott Shannen Doherty Zoe Saldana Lake Bell Nick Stahl Paz Vega
| music          = Mike Desmond  
| cinematography = Seamus Tierney
| editing        = Gregory Plotkin
| distributor    = Films In Motion
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $5,000,000 - $5,800,000
| gross          = $3,271 
}}

Burning Palms is a 2010 satirical thriller film based on Los Angeles stereotypes told through five intertwining storylines. The film is the directorial debut of screenwriter Christopher B. Landon.        

==Premise==
The film explores satires of Angeleno stereotypes, which are told through five interlacing stories.   The five intertwining segments are based on popular stereotypes of West Hollywood, Santa Monica, Sherman Oaks, Westwood and Holmby Hills.  Each of the characters in the film confronts taboos and an uncertain, often darkly humorous, fate.  Producer Oren Segal likens the film to "a John Waters version of Short Cuts ,  a 1993 drama film directed by Robert Altman.

==Plot==
; The Green Eyed Monster
Dedra Davenport (Rosamund Pike) meets the 15-year-old daughter (Emily Meade) of her fiancé (Dylan McDermott) for the very first time. However, she is soon disturbed by how unhealthy and bordering on incestuous the relationship between the two is.

; This Little Piggy Robert Hoffman). Soon after she begins to slowly lose her mind when she cannot seem to get rid of an odd smell from her finger.

; Buyer’s Remorse
A rich and well-recognized West Hollywood gay couple (Peter Macdissi and Anson Mount) decide to adopt a seven-year-old African girl (Tiara McKinney). They prove to be mentally unprepared for the challenges and risks involved in parenthood, especially since she is a decided mute who refuses to speak to them.

; Kangaroo Court
A nanny (Lake Bell) for a group of neglected children comes to find some dangerous secrets about the live-in staff at their mansion home.

; Maneater
An unidentified man (Nick Stahl) breaks into the apartment of meek woman Sarah Cotton (Zoe Saldana), and rapes her. Sometime later she finds the mans wallet and is able to track him down and approach him with an unusual request.

==Cast==
* Zoe Saldana as Sarah Cotton
* Jamie Chung as Ginny Bai
* Dylan McDermott as Dennis Marx
* Rosamund Pike as Dedra Davenport
* Lake Bell as Maryjane
* Nick Stahl as Robert Kane
* Paz Vega as Blanca Juarez
* Adriana Barraza as Luisa Alvarez
* Shannen Doherty as Dr. Shelly Robert Hoffman as Chad Bower
* Peter Macdissi as Geri
* Anson Mount as Tom
* Emily Meade as Chloe Marx
* Jon Polito as The Pharmacist
* Victor Webster as Paulo
* Dimitri Diatchenko as Bob

==Production==
Palms was scripted by Christopher B. Landon, who also wrote the 2007 thriller Disturbia (film)|Disturbia. Palms will also mark Landons directorial debut. 
 Robert Hoffman, Tom Wright. 

The film was shot in Los Angeles, California and Baton Rouge, Louisiana.  Oren Segal, Steven Prince and Jason Hewitt produced the film, and Tyler Thompson, Vince Morella and Naz Jafri were executive producers. 

==Reception==

===Critical response===
To date, the critical reception for the film has been largely negative. One critic described the film as being "one of the most offensive movies I’ve seen in the past decade", going on to say:

 

Andrew Schenker of Slant (magazine)|Slant magazine gave the film just half of one star out of a possible four. Writing of the film:

 

Gabe Callahan, of Poptimal.com, pointed out his theories as to why the film was such a disappointment:

{{cquote|Burning Palms has a lack of focus climatically as each story limps to their anti-climatic reveals.… For Landon’s directorial debut, Burning Palms is a movie that threatens to leave no taboo unexplored but the film seemed scared and hesitant of its own subject matter, and never takes the plunge into the deep end of dark and satirical film-making.

In this way the movie fails. The shocking parts are predictable, the funny parts fall flat, and the satire is non-existent. That is the core of the problem with Burning Palms: there is a fine line between satire and stereotypes. The film doesnt seem aware of this line as it tries its hardest to make a point about stereotypes and taboos. It fails miserably at making this point and just ends up being misogynistic and naïve. I would have loved to have been shocked and disturbed. Instead I had to settle for disappointed. }}

The film currently only holds a 33% rating on Metacritic,  and a 29% "Rotten" rating on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  
*  
*   at Metacritic

 
 
 
 
 
 
 
 