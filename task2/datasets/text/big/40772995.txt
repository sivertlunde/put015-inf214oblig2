The House of Seven Corpses
{{Infobox film
| name           = The House of Seven Corpses 
| image          = File:The House of Seven Corpses - Poster.jpg
| caption        = 1974 theatrical poster
| director       = Paul Harrison
| producer       = {{plainlist|
* Paul Lewis
* Paul Harrison
}}
| writer         = {{plainlist|
* Paul Harrison
* Thomas J. Kelly
}}
| starring       = {{plainlist| John Ireland
* Faith Domergue
* John Carradine
}}
| music          = Bob Emenegger
| cinematography = Don Jones
| editing        = 
| studio         = {{plainlist|
* Television Corporation of America
* International Amusement
}}
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} John Ireland, Faith Domergue and John Carradine.

==Synopsis==
A director courts disaster by filming his horror movie in a real haunted house.

==Cast== John Ireland as Eric Hartman
* Faith Domergue as Gayle Dorian
* John Carradine as Edgar Price
* Carole Wells as Anne
* Charles Macaulay as Christopher Millan
* Jerry Strickler as David
* Ron Foreman as Ron
* Larry Record as Tommy
* Charles Bail as Jonathon Anthony Beal/Theodore Beal
* Lucy Doheny as Suzanne Beal
* Jo Anne Mower as Allison Beal
* Ron Garcia as Charles Beal
* Jeff Alexander as Russell Beal
* Wells Bond as The Ghoul

==Production==
It was filmed at the Utah Governors Mansion in Salt Lake City.   

==Release==
Severin Films released the film on DVD and Blu-Ray in 2013. 

==Reception==
Writing in The Zombie Movie Encyclopedia, academic  , Glenn Kay called the concept better suited to an anthology film.   Bloody Disgusting rated it 1.5/5 stars and wrote that though it is "only frightening in the first few minutes".   Stuart Galbraith of DVD Talk rated it 2/5 stars and called it "cheap and derivative but hard to entirely dislike".   Daryl Loomis of DVD Verdict wrote, "While there are things to enjoy about The House of Seven Corpses, it is completely forgettable, mostly because its patently unscary." 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 