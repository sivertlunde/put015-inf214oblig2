The Rift (2012 film)
 
{{Infobox film
| name           = The Rift
| director      = Robert Kouba
| producer       = Robert Kouba, Robert Kouba Senior, Colleen Keane
| writer         = Robert Kouba, Tasha Lowe-Newsome, Yuell Newsome, Jonathan Peace
| starring       = Eileen Grubba, James DiStefano, Ralph Guzzo, Carlotta Montanari, Quinn Butterfield, George Benedict, Brian Bookbinder, Robert Kouba, Kevin Kloepfer
| music          = David Mason, Robert Kouba Senior
| cinematography = Sebastian Cepeda
| studio         = Vantis Pictures
| distributor    =
| released       =  
| runtime        = 26 minutes
| country        = United States Switzerland
| language       = English
| gross          =
}} science fiction thriller short film directed by Robert Kouba and written by Robert Kouba, Tasha Lowe-Newsome, Yuell Newsome, and Jonathan Peace.

==Plot==
In 1982 a Russian physicist discovered something extraordinary was behind the strange radar anomalies he was recording. Something dark, which will eventually alter the path of mankind. Before he could tell anyone, he mysteriously vanished.

Many years later budding physics student Dean Hollister and his discredited physics teacher have become obsessed with the same mystery. What had been a rare phenomenon is suddenly happening all over the world. The day it happens is a regular night shift for Dean at his mother’s diner. Black rifts appear in the sky. Behind those rifts, something is moving. It’s watching us.

==Cast==
*Eileen Grubba as Gloria Hollister
*James DiStefano as White Tie
*Ralph Guzzo as Dr. Gordon The Woman
*Quinn Butterfield as Dean Hollister
*George Benedict as Roland
*Brian Bookbinder as Clive
*Robert Kouba Senior as Ivan Petrenko Karkarov Russian Soldier

== External links ==
*  
*  

 
 
 
 
 


 