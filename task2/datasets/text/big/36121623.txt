The Lion and the Mouse (1919 film)
{{infobox film
| name           = The Lion and the Mouse
| image          = The Lion and the Mouse (1919) - 1.jpg
| imagesize      =
| caption        = Still with Alice Joyce
| director       = Tom Terriss
| producer       = Vitagraph Company of America Albert E. Smith
| writer         = Charles Klein (play) Edward J. Montagne (scenario)
| starring       = Alice Joyce
| music          =
| cinematography = Joseph Shelderfer
| editing        =
| distributor    = Vitagraph Company of America
| released       = February 16, 1919
| runtime        = 60 minutes; 6 reels
| country        = United States
| language       = Silent (English intertitles)
}} lost 1919 American silent drama film produced and released by the Vitagraph Company of America. It was directed by Tom Terriss and based on the famous Charles Klein play. Alice Joyce starred in the film.   

==Plot==
As described in a film magazine,  John Burkett Ryder (Randolf), "the richest man in the world," seeks to discredit a judicial decision which works against his financial interests by discrediting its author, Judge Rossmore (Hallam), and has impeachment charges initiated against the judge in Congress. Shirley Rossmore (Joyce), the judges daughter, learns of her fathers trouble and returns from Paris, where she has won success as an author. She is loved by Jefferson Ryder (Nagel), son of the magnet. Determined to force the millionaires hand, she publishes The American Octopus under a pseudonym with a main character based upon Burkett. He is attracted by the book and brings its author Shirley, whom he knows as Sarah Green, into his home to write his biography. She uses this opportunity as the chance to obtain two letters that will clear her fathers name. John aids her in obtaining the documents, but is discovered and denounced as a thief. Shirley cannot allow the man she loves so branded, so she reveals her identity. The millionaire "lion" had already been won over by the charm of the "mouse," so there is a happy resolution.

==Cast==
*Alice Joyce - Shirley Rossmore
*Conrad Nagel - Jefferson Ryder
*Anders Randolf - John Burkett Ryder
*Henry Hallam - Judge Rossmore
*William T. Carleton - Sen. Roberts (*as W. T. Carlton)
*Mona Kingsley - Kate Roberts
*Jane Jennings - Mrs. Ryder
*W. H. Burton - Judge Scott
*Templar Saxe - Fitzroy Bagley (*as Templer Saxe)
*Mary Carr - Eudoxia

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 
 


 