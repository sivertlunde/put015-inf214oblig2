Scent of Mystery
{{Infobox film
| name           = Scent of Mystery
| caption        = A film poster bearing the films new title: Holiday in Spain
| image	         = Scent of Mystery FilmPoster.jpeg
| director       = Jack Cardiff
| producer       = Mike Todd, Jr.
| writer         = Gerald Kersh Kelley Roos (novel Ghost of a Chance)
| starring       = Denholm Elliott Peter Lorre Elizabeth Taylor
| music          = Harold Adamson Mario Nascimbene Jordan Ramin
| cinematography = John von Kotze
| editing        = James E. Newcom
| distributor    =
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         =
}} 1960 mystery Around the World in Eighty Days.

==Plot== American heiress, taxi driver, smoking pipe, for example.

The screenplay was adapted from the 1947 novel Ghost of a Chance by Kelley Roos, the pen name of husband and wife mystery writers Audrey Kelley and William Roos. The novel was set in locations in New York City. Kelley Roos also wrote a 1959 paperback novelization of the screenplay, reset in Spain. 

==Smell-O-Vision== New York Times writer Richard Nason believed it was a major advance in filmmaking. As such, expectations for the film were great.   
 Los Angeles, and Chicago. Unfortunately, the mechanism did not work properly. According to Variety (magazine)|Variety, aromas were released with a distracting hissing noise and audience members in the balcony complained that the scents reached them several seconds after the action was shown on the screen. In other parts of the theater, the odors were too faint, causing audience members to sniff loudly in an attempt to catch the scent. 

* Technical adjustments by the manufacturers of Smell-O-Vision solved these problems, but by then it was too late. Negative reviews, in conjunction with word of mouth, caused the film to fail miserably. Comedian Henny Youngman quipped, "I didnt understand the picture. I had a cold." {{Citation
  | last = Kirsner
  | first = Scott
  | title = Inventing the Movies
  | publisher = Createspace
  | pages = 45–46
  | isbn = 978-1-4382-0999-9 The Bell Jar, which was also his last film.

* The film was eventually retitled as Holiday in Spain and re-released, sans odors. However, as The Daily Telegraph described it, "the film acquired a baffling, almost surreal quality, since there was no reason why, for example, a loaf of bread should be lifted from the oven and thrust into the camera for what seemed to be an unconscionably long time." 

* Scent of Mystery was aired once on television by MTV and syndicated on local TV stations in the 1980s, in conjunction with a convenience store promotion that offered scratch and sniff cards that viewers were to use to recreate the theater experience.

==Soundtrack== score composed Eddie Fisher.

Blu Ray is coming of Holiday in Spain in November 2014. Lots of extras included.

 

==See also==
*Polyester (film)|Polyester

==References==
 

==External links==
* , producers of the CD soundtrack
*  

 

 
 
 
 