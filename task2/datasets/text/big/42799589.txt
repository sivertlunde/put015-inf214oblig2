Annabelle's Affairs
{{Infobox film
| name = Annabelles Affairs
| image =
| image_size =
| caption =
| director = Alfred L. Werker William Fox   William Goetz Leon Gordon   Harlan Thompson     
| narrator = Sam Hardy
| music = George Lipschultz 
| cinematography = Charles G. Clarke 
| editing = Margaret Clancey     
| studio =  Fox Film Corporation 
| distributor =    Fox Film Corporation 
| released = June 14, 1931
| runtime = 76 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Annabelles Affairs is a 1931 American romantic comedy film directed by Alfred L. Werker and starring Victor McLaglen, Jeanette MacDonald and Roland Young. The film is based on the play Good Gracious Annabelle by Clare Kummer. It is the only one of MacDonalds films to be considered lost film|lost. It was well received by critics, but did not perform well at the box office.  

==Plot==
After they are separated shortly after their marriage, Annabelle doesnt really know what her husband looks like. When they meet later she finds herself falling in love with him, without realizing that they are already married.

==Cast==
*   Victor McLaglen as John Rawson / Hefly Jack 
* Jeanette MacDonald as Annabelle Leigh  
* Roland Young as Roland Wimbleton  Sam Hardy as James Ludgate  
* William Collier Sr. as Wickham  
* Sally Blane as Dora 
* Joyce Compton as Mabel 
* Ruth Warren as Lottie  
* George Beranger as Archie   Walter Walker as Walter J. Gosling  
* Hank Mann as Summers  
* Jed Prouty as Bolson  
* Louise Beavers as Ruby 
* Wilbur Mack as Vance, assistant hotel manager

==See also==
*List of lost films

==References==
 

==Bibliography==
* Turk, Edward Baron. Hollywood Diva: A Biography of Jeanette MacDonald. University of California Press, 1998.

==External links==
* 

 

 
 
 
 
 
 
 
 

 