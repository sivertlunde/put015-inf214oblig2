Kin Kiesse
{{Infobox film
| name           = Kin Kiesse
| image          = 
| caption        = 
| director       = Mwezé Ngangura
| producer       = OZRT Antenne 2 Ministère français des Relations extérieures
| writer         = 
| starring       = Chéri Samba
| distributor    = 
| released       = 1982
| runtime        = 28 minutes
| country        = Democratic Republic of the Congo
| language       = 
| budget         = 
| gross          = 
| screenplay     = 
| cinematography = 
| editing        = 
| music          = 
}}

Kin Kiesse is a 1982 documentary film about "Kin" (Kinshasa), the capital of Zaire, and the capital of paradoxes and excesses, commentated on by one of its naïf artists, the painter Chéri Samba. We discover the "Kin" of night clubs, high buildings, bicycle-taxis, shoe shiners and hairdressers, the "Kin" of poor neighborhoods, but, above all, the “Kin” of music, where all the genres rub elbows, from beer party brass bands to the rumba to traditional dances, without leaving out the in-fashion bands of the time.

According to the films director, Mweze Ngangura, Chéri Samba was instrumental in the making of the film, convincing the French Ministry of Co-operation, France 2 and Congolese television that Ngangura could make a film on Kinshasa.   

== Awards ==
* Fespaco 1983

== References ==
 
 

 
 
 
 
 
 
 
 
 
 

 
 