I chartopaichtra
 

{{Infobox film
| name           = I chartopaichtra Η χαρτοπαίχτρα 
| image          = 
| image_size     = 
| caption        = 
| director       = Giannis Dalianidis
| producer       = 
| writer         = Giannis Dalianidis
| starring       = Rena Vlachopoulou Lambros Konstantaras Kostas Voutsas Hloi Liaskou Lili Papagianni Sapfo Notara Yorgos Vrasivanopoulos Periklis Hristoforidis Nassos Kedrakas Angelos Mavropoulos Nikos Feramas Nikitas Platis Yannis Bertos
| music          = Mimis Flessas
| cinematography = 
| photographer   = Vassilis Vassiliadis
| editing        = 
| distributor    = Finos Films 1964
| runtime        = 94 min
| country        = Greece Greek
| cine.gr_id     = 600501
}}
 1964 Greece|Greek black-and-white film starring Rena Vlachopoulou and Lambro Konstantaras.  The film made 534,086 tickets in Athens.

==Plot==

The main plot heads around a lady (Rena Vlahopoulou) for playing cards and her trying with his man (Lambros Konstantaras) for making it sensibly.

==Other==
*Photographer: Nikos Dimopoulos
*Operator: Vassilis Vassiliadis
*Music: Minis Flessas
*Presenter: Petros Lykas
*Scenic producer: Markos Zervas
*Make-up: Nikos Xepapadakos
*Audio: Giannis Fisher
*Style: Markos Zervas
*Dancers: Fotis Metaxopoulos, Viky Tzinieri
*Associate producer: Markos Zervas
*Machine audio: G. Mihaloudis

==See also==
*List of Greek films

==External links==
* 
 
 
 
 
 
 


 
 