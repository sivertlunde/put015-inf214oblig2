Saaheb
{{Infobox Film
| name           = Saaheb
| image          = Saaheb poster.jpg
| caption        = Poster
| director       = Anil Ganguly
| producer       = Rajani Ganguly
| writer         = Madan Joshi (dialogue)
| screenplay     = Sachin Bhowmik
| story          = Ranjan Roy
| narrator       = Shilpa Pednekar
| starring       = Anil Kapoor, Amrita Singh, Raakhee, Utpal Dutt, Deven Verma Anjaan (lyrics)
| cinematography = P. Subedar
| editing        = Waman Bhosle Guru Dutt Shirali
| released       =  
| runtime        = 150 minutes
| country        =   India Hindi
}}
 Vishnuvardhan playing Shankar and in Telugu as Vijetha with Chiranjeevi.

Anil Kapoor plays Saaheb, the youngest son of the family. He is focused on football, his passion. Due to this, everyone in the family constantly tells him he is no good. However when circumstances demand, he gives up his kidney to fund his sisters marriage, at the cost of his football career.

The movie has catchy songs like - "Yaar Bina Chain Kahan Re" and "Kya Khabar Kya Pata" - which remain popular to this day.

==Cast==
*Anil Kapoor &ndash; Sunil Sharma Saaheb
*Amrita Singh &ndash; Natasha Nikki
*Raakhee &ndash; Sujata Sharma, Saahebs eldest sister-in-law
*Utpal Dutt &ndash; Badri Prasad Sharma, Saahebs Father
*Biswajeet &ndash; Eldest Son
*Vijay Arora &ndash; Kamal Sharma, Second Son
*Rajnibala &ndash; Kamals Wife
*Dilip Dhawan &ndash; Third Son
*Sudha Chopra &ndash; Geeta Sharma Bultie
*Deven Verma &ndash; Pareshan, Nikkis Uncle Satyen Kappu &ndash; Nikkis father
*A.K. Hangal &ndash; Doctor
*Pinchoo Kapoor &ndash; Mr. S.P. Sinha
*Suresh Chatwal &ndash; Mr. Somnath, Saahebs Football Coach
*Nandita Thakur &ndash; Shanti
*Jankidas &ndash; Jandubalm
*Viju Khote
*Dinesh Hingoo
*Nitin Sethi
*Shashi Ranjan
*Master Kunal Gandhi
*Master Vijay Ganguly
*Roopali Ganguly (child artist)

==Plot==
Mr. Sharma (Utpal Dutt) lives with his four sons (Biswajeet, Vijay Arora, Dilip Dhawan) and a daughter, Geeta or Butlie. Only the youngest son, Sunil (Anil Kapoor) and Geeta are unmarried. Mr. Sharma arranges his daughters marriage with a man who will be re-locating to London, U.K. The family requires Rs.50,000/- for the expenses, and will even go to the extent of selling their house. Sunil or Saaheb as he is referred to, is the black sheep of the family - unemployed & uneducated - only interested in playing football. None of the elder brothers have any means of coming up with money for the expenses, and two of them even scheme with their respective wives to take over the house. Mr. Sharma attempts to raise the money, in vain. Sunil then makes the ultimate sacrifice for the marriage of his sister, and brings the money to his dad. His dad believes that Sunil has stolen the money, and Sunil is forced to lie about where he got the money from. The family only finds out after the marriage ceremony is over, and do not hesitate to blame him. It is then Sujata (Raakhee) reveals the secret of how the money came to Sunils possession.

==Crew==
*Director &ndash; Anil Ganguly
*Story &ndash; Ranjan Roy
*Screenplay &ndash; Sachin Bhowmik
*Dialogue &ndash; Madan Joshi
*Producer &ndash; Rajani Ganguly, R. M. Valia, Rupa K. Gandhi
*Production Company &ndash; R. V. Films
*Presenter &ndash; Shilpa Pednekar
*Cinematographer &ndash; P. Subedar
*Editor &ndash; Waman B. Bhosle, Guru Dutt Shirali
*Art Director &ndash; Bijoy Ghosh
*Costume Designer &ndash; Jasbir Valia
*Costume and Wardrobe &ndash; Ashok Bhonsle, Dilip Bhonsle
*Action Director &ndash; Manek Irani
*Choreographer &ndash; Kamal, Sujatha
*Music Assistant &ndash; Anil Mohile, Arun Paudwal, Bansari Lahiri, Surendra Singh Sodhi
*Music Director &ndash; Bappi Lahiri Anjaan
*Playback Singers &ndash; Asha Bhosle, Bappi Lahiri, S. Janaki, Kishore Kumar

==Soundtrack==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     = Anjaan
| all_music       = Bappi Lahiri

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Chalte Chalte Leharon Ke Saath
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Kishore Kumar
| length1         = 7:30

| title2          = Jawan Hai Dil Jawan Hain Hum
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = S. Janaki
| length2         = 4:40

| title3          = Kya Khabar Kya Pata
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Kishore Kumar, Chorus
| length3         = 6:35

| title4          = Tuku Tuku Pyar Karoongi
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Asha Bhosle
| length4         = 4:30

| title5          = Yaar Bina Chain Kahan Re
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Bappi Lahiri, S. Janaki
| length5         = 4:40

| title6          = Tuku Tuku Pyar Karoongi
| note6           = Sad
| writer6         =
| lyrics6         =
| music6          =
| extra6          = Asha Bhosle
| length6         = 1:00
}}

== External links ==
*  

 
 
 
 