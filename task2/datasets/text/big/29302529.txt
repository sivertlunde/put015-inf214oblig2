Ingrid – Die Geschichte eines Fotomodells
{{Infobox film
| name           = Ingrid - Die Geschichte eines Fotomodells
| image          = 
| caption        = 
| director       = Géza von Radványi
| producer       = Allemande
| writer         = Gerda Corbett Joachim Wedekind
| starring       = Johanna Matz Louis de Funès
| music          = Hans-Martin Majewski
| cinematography = 
| editing        = 
| distributor    = 
| released       = 21 January 1955 (France)
| runtime        = 110 minutes
| country        = Germany
| awards         = 
| language       = German
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 German Comedy comedy film from 1955, directed by Géza von Radványi, written by Gerda Corbett, starring Johanna Matz and Louis de Funès. 

== Cast ==
* Johanna Matz : Ingrid
* Louis de Funès : DArrigio, fashion designer
* Paul Hubschmid : Robert, journalist
* Erni Mangold : Ingrids sister « Hanne »
* Joseph Offenbach : Mr Moga
* Paul-Edwin Roth : Walter, press photographer
* Franz Schafheithin : headmisstress of a school for models
* Alice Treff : the female director Jens Andersen
* Horst Becker
* Isolde Bräuner : a model
* Elly Brugmer : Ingrids aunt
* Mickael Burk
* Marion Carr
* Gerda Corbett
* Georg Edert
* Hans Friedrich
* Harry Gondi
* Linda Geiser

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 


 