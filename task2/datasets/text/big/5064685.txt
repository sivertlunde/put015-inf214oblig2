Vasool Raja MBBS
 
 

{{Infobox film
| name            = Vasool Raja MBBS
| image           = Vasool_Raja_MBBS_Poster.jpg Saran
| writer          = Vidhu Vinod Chopra Rajkumar Hirani Crazy Mohan   (dialogues )  Prabhu Sneha Sneha Prakash Malavika Nithin Sathya
| producer        = Gemini Film Circuit Bharadwaj
| cinematography  = A. Venkatesh (cinematographer)|A. Venkatesh
| editing         = Suresh Urs
| studio          = Gemini Film Circuit
| distributor     = Raaj Kamal Films International
| released        = 12 August 2004
| runtime         = 160 minutes
| country         = India
| language        = Tamil
| gross          =  
}}
 Tamil comedy Malavika and Karunas. The films music was composed by Bharadwaj (music director)|Bharadwaj.

==Plot==
Rajaraman, nicknamed "Vasool Raja" (Kamal Haasan) is a small time don in Chennai, making a living by collecting money from people who refuse or dilly-dally in paying their debts to others, with the help of his right hand man Vaddi (Prabhu Ganesan). Given that his father had wished him to be a doctor, he creates the faux Venkataraman Charitable Hospital (named after his father) and pretends to live in accordance with this wish whenever his father (Nagesh (actor)|Nagesh) and mother Kasturi (Rohini Hattangadi) visit him in Chennai.

One year, however, Rajas plan goes awry when his father meets an old acquaintance, Dr. Vishwanathan (Prakash Raj) and the two men decide to get Raja  married to Vishwanathans daughter, Dr. Janaki "Paapu" (Sneha (actress)|Sneha). At this point,the truth about Raja is revealed. Vishwanathan insults Rajas parents and calls them "fools" for being ignorant of Rajas real life. Rajas father and mother, aghast and heartbroken, leave for their village.

Raja, in grief and despair, decides that the only way to redeem himself and to gain revenge for the humiliation suffered by his father at the hands of the spiteful Vishwanathan is to become a doctor. He decides to go to a medical college to obtain an Bachelors of Medicine and Surgery#India|M.B.B.S. degree, the graduate medical degree in South Asia.

With the help of Vaddi and others, Raja gains admission to the SLR Institute of Medical Sciences by threatening Dr. Margabandhu (Crazy Mohan), where he again encounters Dr. Vishwanathan who is the dean. His success there becomes dependent upon the (coerced) help of faculty member Dr. Margabandhu (Crazy Mohan). While Rajas skills as a medical doctor are minimal, he transforms those around him with the "Kattipudi Vaithiyam" ("hugging therapy")&nbsp;— a method of comfort taught to Raja by his mother&nbsp;— and the compassion he shows towards those in need. Despite the schools emphasis on mechanical, Cartesian, impersonal, often bureaucratic relationships between doctors and patients, Raja constantly seeks to impose a more empathetic, almost holistic, regimen. To this end, he defies all convention by treating a brain-dead man as if the man were able to perceive and understand normally; intimidating Dr. Kalidas (Chitra Lakshman) into admitting and treating a suicide patient(Nithin Sathya) interacts on familiar but autocratic terms with patients; humiliates school bullies; effusively thanks a hitherto-underappreciated cleaner; helping a terminally ill cancer patient Zakir (Jayasurya); and encourages the patients themselves to make changes in their lives, so that they do not need pharmaceuticals or surgery.

Dr. Vishwanathan, who perceives all this as symptoms of Chaos (cosmogony)|chaos, is unable to prevent it from expanding and gaining ground at his college. He becomes increasingly irritable, almost to the point of insanity. Repeatedly, this near-dementia is shown when he receives unwelcome tidings and he begins laughing in a way that implies that he has gone mad. This behavior is explained early on as an attempt to practice laughter therapy, an attempt that seems to have backfired&nbsp;— Vishwanathans laughing serves more to convey his anger than diffuse it. Meanwhile, his daughter becomes increasingly fond of Raja, who in his turn becomes unreservedly infatuated with her. Some comedy appears here, because Raja is unaware that Dr. Janaki and his childhood friend "Paapu" are one and the same; an ignorance that Janaki hilariously exploits. Vishwanathan tries several times to expel Raja but is often thwarted by Rajas wit or the affection with which the others at the college regard Raja, having gained superior self-esteem by his methods.
 guilt for not being able to help Zakir gets the better of him. In the moments immediately following Rajas departure, the bed ridden man miraculously awakens from his vegetative state; at this point Janaki gives a heartfelt speech wherein she criticizes her father for having banished Raja, saying that to do so is to banish hope, compassion, love, and happiness, etc. from the college. Vishwanathan eventually realizes his folly.

Raja later marries Dr. Janaki, learning for the first time that she is "Paapu".

==Cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Kamal Haasan || Rajaraman (Raja/Vasool Raja)
|- Prabhu || Vatti
|- Sneha || Janaki Vishwanathan (Paapu)
|-
| Prakash Raj || Vishwanathan
|-
| Crazy Mohan || Margabandhu/Mark
|-
| Jayasurya || Zakir
|-
| Nagesh || Sriman Venkatraman, Rajas father
|-
| Rohini Hattangadi || Kasturi, Rajas mother
|-
| Karunas || Amit
|- Malavika || Priya
|-
| Nithin Sathya || Neelakandan
|-
| Ragasya || Special Appearance in the song "Chirukki Chirukki"
|}

==Production== Shankar Dada Sneha for the film. Saran hoped to sign on director K. Balachandar to play Kamal Haasans father in the film, with the veteran director being both Kamal Haasans and Sarans film industry mentor. However Balachandar was reluctant to act and the team finalised Girish Karnad for the role, before later replacing him with veteran actor Nagesh. 

==Soundtrack== Saran for another time and lyrics by Vairamuthu. The film has 6 songs. The audio was launched on July 2004. 

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| No. || Song || Singers || Length
|-
| 1|| "Kaddu Thiranthae" || Hariharan (singer)|Hariharan, Sadhana Sargam|| 5.24
|-
| 2|| "Kallakapovathu Yaaru" || Kamal Haasan, Sathyan (singer)|Sathyan|| 4.37
|-
| 3|| "Love Pannuda" || Kamal Haasan || 5.13
|-
| 4|| "Pathukullae Number" || Kay Kay, Shreya Ghosal || 5.19
|-
| 5|| "Sakalakala Doctor Doctor" || Bharathwaj || 4.31
|-
| 6|| "Seena Thana (Siruchi)" || Grace || 4.38
|}

==Box office==
The film was released in about 285 screens worldwide to generally positive reception and box-office success collecting around 400&nbsp;million.  According to certain reports, 10 million tickets were sold worldwide. 

==Controversy==
The film faced controversy as the petition filed by Tamil Nadu Medical Council president K R Balasubramanian stated that the films title ridiculed the medical profession and tarnished the image of the medical fraternity. Moreover, the film title is a mockery on the medical profession which would lower dignity of the medical fraternity in eyes of public. 

==References==
 

 

 
 
 
 
 
 
 
 
 