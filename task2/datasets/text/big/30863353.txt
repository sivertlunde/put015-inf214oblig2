Mumsy, Nanny, Sonny and Girly
 
 
{{Infobox film
| name = Mumsy, Nanny, Sonny, and Girly
| image = Girly 1970 Poster 1.jpg
| caption = British original poster
| image size =
| director = Freddie Francis
| producer = Ronald J. Kahn
| writer = Brian Comport Michael Bryant Ursula Howells Vanessa Howard Howard Trevor Pat Heywood
| music = Bernard Ebbinghouse
| cinematography = David Muir
| editing = Tristam Cones
| distributor = Cinerama Releasing Corporation
| released = 12 February 1970
| runtime = 101 min
| language = English
| budget =
}}

Mumsy, Nanny, Sonny, and Girly, released as Girly in North America, is the name of a 1970 British horror-comedy. The film originated as a dream project for renowned cinematographer-turned-director Freddie Francis, who wanted the opportunity to direct a film over which he had complete creative control, instead of working on assignment from a studio (as was the case with his previous directorial efforts). Francis teamed with writer Brian Comport to build the movie around Oakley Court, which Francis had used for exterior shots in previous films. The script was based on a two-act play by Maisie Mosco entitled Happy Family, which was later adapted into a novella by screenwriter Brian Comport as "Mumsy, Nanny, Sonny, and Girly". Though the film fared poorly in British cinemas, it enjoyed a brief but successful run in North America before going on to achieve status as a cult film. 

==Plot==

Four individuals live in a secluded  ), Nanny (the nanny, Pat Heywood), Sonny (the son, played by Howard Trevor), and Girly (the daughter, Vanessa Howard). The Game is built around a set of strictly enforced yet ill-defined rules, the principal one of which is "Rule No. 1: Play the Game."

As a part of The Game, the teenaged Sonny and Girly regularly venture to more populated areas, where the pair use Girly to lure men back to the manor house. Once there, the men are dressed like schoolboys and forcibly indoctrinated into The Game, assuming the roles of "New Friends." Those who refuse are "sent to the Angels"—a euphemism for being ritualistically murdered in scenarios built around playground games, which Sonny routinely records on a 16mm movie camera so that the family can later enjoy the resultant snuff film.
 Michael Bryant) hungover man that he murdered the woman after a night of heavy drinking, and convince him to return to the manor with them. The prostitute—rechristened "New Friend"—is outfitted in schoolboy clothes and subjected to an indeterminate period of torment "playing the game," during which he is repeatedly presented with his clients body as a reminder that the family has incriminating information about him.

After Mumsy makes sexual overtures to New Friend one evening, he gets the idea to turn the family against itself. New Friends plot succeeds, as he creates sexual jealousy between the women after first sleeping with Mumsy and then Girly. Sonny, left out of the sexual politics, petitions to have New Friend "sent to the angels;" in a moment of panic, Girly bludgeons him to death with an antique mirror. Chastising Girly for creating a mess, Mumsy dismisses Sonny as "naughty" and orders a visibly shaken New Friend to bury Sonny beneath a drained fountain on the manor grounds, which is already populated by makeshift gravestones bearing the numerical identities assigned to dispatched "friends."

Nanny, jealous that she is the only female member of the household left out of New Friends attentions, attempts to murder Mumsy with acid-tipped needles, but the attempt fails when it is inadvertently interrupted by New Friend. Girly, realising that Nanny has set her sights on New Friend, hacks Nanny to death with an axe and cooks her head for use in baked goods.

Rather than turn on one another, Mumsy and Girly declare a truce, deciding to "share" New Friend by alternating which days of the week each woman will be permitted to have sex with him. The two women agree, though ponder what will happen should either of them ever become bored with New Friend, with Mumsy declaring it as an inevitability. Overhearing the womens conversation, New Friend retrieves—and hides—Nannys acid tipped needles before settling into Mumsys room, smiling.

==Production==

The film began as a dream project for Freddie Francis, a renowned cinematographer who had made the transition to directing at the beginning of the 1960s. Though he had numerous directorial credits to his name, each of these had come to him on commission from a studio, and Francis had long dreamed of making a film over which he had complete creative control. Over the course of his career, Francis had shot several exterior scenes for films at Oakley Court, but long lamented the fact that neither he nor any other director had ever had the opportunity to film inside the building; in putting together his project, Francis decided that his film would be set in and around Oakley Court, with the script tailored to the buildings unique landscaping and architecture.

Having never written a film himself, Francis hired writer Brian Comport to craft a screenplay, with the only condition being that the story had to be built around Oakley Court. Trying to come up with ideas, Francis and Comport attended the production of an off west end play entitled "The Happy Family," written by Maisie Mosco, then a radio playwright for the BBC. The play—itself influenced by Shirley Jacksons We Have Always Lived in the Castle and Tennessee Williams Baby Doll —concerned a woman recently forced to undergo a hysterectomy by her abusive husband, who shortly thereafter left her for a younger woman. Having gone insane, the woman—redubbed "Mumsy"—forces her two children and her maid to join her in an elaborate role-playing game in which young societal drop-outs are welcomed into the family as new "children" for Mumsy. Both men thought that the play—which was overtly sexual and dealt explicitly with incest, lesbianism, and sadomasochism—was "terrible," but agreed that it was an excellent tipping-off point for a story that would take place at Oakley Court.  Little of the plays storyline would survive into Comports script, beyond the names of the principal characters and the basic premise of an isolated family engaging in a deadly role-playing game.
 Hammer Horror films whom Francis knew from his time working for the studio. Howard Trevor, who played Sonny, had only a single prior screen credit, on an episode of the anthology series ITV Playhouse; Girly would prove to be his only film role. Vanessa Howard, who had also gotten her start on ITV, was a relative newcomer who had starred in four films prior to Girly; she performed so well that it was decided in post production that she would become the centerpiece of the films marketing campaign, with the intention of turning the film into a star vehicle for her.

==Reception==
 mods and Swinging London. Media watchdogs latched onto a scene in the opening minutes of the film in which Girly suggestively sucks Sonnys finger after accepting a piece of candy from him. The scene was the result of Comports having toyed with the idea of carrying over incest themes from the play, in which Sonny and Girly are explicitly engaged in a sexual relationship. Comport ultimately decided against this, deciding it was more thematically appropriate to the film to imply incest but never confirm it. Though the scene is the only implication of incest in the film, it came to be the films definitive moment in contemporary media reviews. 
 West End premier. 

In an attempt to recoup losses, the film was rebranded for release in the United States, where exploitation films were enjoying moderate success. Retitled simply Girly, the films advertising campaign was retooled to be built exclusively around Vanessa Howard, removing all of her costars from the films posters. The film performed surprisingly well, even achieving a positive writeup in Variety (magazine)|Variety.

==Legacy==

The films failure at the British box office led to Vanessa Howards decision to retire from acting in 1972; at the time of her decision, she was unaware of the films success in America, and remained uninformed for some time.  Despite the films financial failure, Francis maintained for the rest of his life that it had been his best work, and his personal favourite of all the films he made.
 Salvation Films announced that they had obtained the rights to release Girly on DVD. The release entered development hell, with Salvation promising the films upcoming release on its website for the next three years. In the interim, Freddie Francis died, eliminating hopes of a potential directors commentary. Salvation ultimately sold their rights to Scorpion Releasing, who recorded an interview with writer Brian Comport and obtained an old radio interview with Francis regarding the film to be included as special features. Vanessa Howard, having learned of the films cult status, agreed to record a commentary for the DVD; however, Howard was terminally ill at the time, and ultimately proved too weak to participate. The DVD was released on 30 March 2010, with remastered audio and video. Vanessa Howard died in October 2010, seven months after the films release.

In 2012, Tightrope Theatre of Portland, OR, re-adapted the screenplay back into stage format and mounted what is purported to be the world premiere stage production of "The Happy Family". Production dates were 11 May through 9 June 2012, at Tightrope Theatres performance space in southeast Portland. The adaptation was done by Elizabeth Klinger, and the production was directed by James Peck. The cast included award winning actors, including Jamie Rea, Rebecca Teran, David Cole, Elizabeth Klinger and Zachary Rouse. The productions stage manager was Lizz Esch Brown.

A complete set of English Hearing Impaired subtitles for the movie, to accompany the recently released (but unsubtitled) DVD, was placed in the public domain in February 2013.

==Influence==
 The Shining, for a scene in which Nanny chases one of the "friends" with an axe, hacking through the panel of a door and exposing her face to the rooms occupant. The scene predates the infamous "Heres Johnny" sequence in The Shining by over a decade.  

==References==
 
*  
* {{Citation
  | title = The Overlook Film Encyclopedia
  | url = http://books.google.com/books?id=3-ehQgAACAAJ&dq=overlook+film+encyclopedia
  | editor-last = Hardy
  | editor-first = Phil
  | editor-link = Phil Hardy (journalist)
  | volume = 3
  | publisher = Overlook Press
  | year = 1995
  | isbn=0-87951-624-0 }}

== External links ==
*  
*   SDH subtitle set (for use with commercially released DVD)

 

 
 
 
 
 
 
 