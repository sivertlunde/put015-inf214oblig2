Destricted
 
{{Infobox film
| name = Destricted
| image	= Destricted FilmPoster.jpeg
| image_size = 220px
| border = 
| alt = 
| caption = Poster Sam Taylor-Wood Tunga
| producer = Mel Agace Fredrik Carlström Mark Fletcher Andrew Hale Andrew Herwitz Igor Kecman Jelena Mitrovic Sigurjon Sighvatsson Neville Wakefield
| writer = Matthew Barney Richard Prince Sam Taylor-Wood
| starring = #Cast|see below
| music = Jonathan Beblar Andrew Hale Matmos Richard Prince
| cinematography = Aleksander Ilic  Seamus McGarvey Peter Strietmann Eric Voake
| editing = Alex Blatt Marco Brambilla Akiko Iwakawa-Grieve Michael D. Thomson
| studio = Offhollywood Digital
| distributor = Revolver Entertainment 
| released =  
| runtime = 112 minutes   129 minutes  
| country = United Kingdom United States
| language = English
}} drama film series that explores the line where art and pornography intersect. The UK and US film releases had overlapping but different film lineups.
 Sam Taylor-Woods Death Valley.

The US version (2010) runs at 129 minutes and includes eight short films: Marilyn Minters Green Pink Caviar, Barneys Hoist, Cecily Browns Four Letter Heaven, Clarks Impaled, Noés We Fuck Alone, Princes House Call, Sante DOrazios Scratch This, and Tunga (artist)|Tungas Cooking.

==Plot==
Destricted can be described as seven short art-house porn films:
* Impaled (Larry Clark) - A casting for a porn film, but not with the insecure women often displayed, instead with insecure young men. (37min 28s).
* Balkan Erotic Epic (Marina Abramović) - An erotic comedy about myths from the Balkan around the sexual organs. (13 min 04s).
* House Call (Richard Prince) - A vintage sex scene recontextualized with edits and music. (12min 29s).
* Sync (Marco Brambilla) - Consists of very fast cuts from different porn films. (2min 15s).
* Hoist (Matthew Barney) - A juxtaposition of sexuality and industrial machinery. (14min 37s).
* Death Valley - A man masturbates in the desert. (8min 25s).
* We Fuck Alone (Gaspar Noé) - A man and a woman masturbate to the same porn film in different rooms. (23min 31s).

==Cast==
;Impaled
* Daniel as himself August as herself
* Jasmine Byrne as herself
* Destiny Deville (uncredited) as herself
* Dillan Lauren as herself
* Sativa Rose as herself
* Angela Stone as herself
* Nancy Vee as herself

;House Call
* Kora Reed as The Patient
* John Saint John as The Doctor

;Hoist
* Vincente Pinho Neto as Blooming Greenman

;Death Valley
* Chris Raines

;We Fuck Alone
* Shirin Barthel
* Richard Blondel
* Manuel Ferrara Katsumi

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 