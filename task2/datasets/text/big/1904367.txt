The Nasty Girl
 
{{Infobox film
| name           = Das schreckliche Mädchen
| image          = DasSchrecklicheMädchen.jpg
| director       = Michael Verhoeven
| producer       = Michael Senftleben
| writer         = Michael Verhoeven
| starring       = Lena Stolze Hans-Reinhard Müller Monika Baumgartner
| music          = Lydie Auvray Billy Gorlt Mike Herting Elmar Schloter
| cinematography = Axel de Roche
| editing        = Barbara Hennings Miramax (USA)
| released       =  
| runtime        = 94 minutes
| country        =  
| language       = German
}} West German drama film based on the true story of Anna Rosmus from Passau, Bavaria. The original German title loosely translates as "The Terrible Girl."

==Plot== German high school student, Sonja (Lena Stolze as a fictionalized version of Anna Rosmus) wins an essay contest and goes on a trip to Paris.

Martin Wegmus begins teaching physics at Sonjas school and one of Sonjas classmates falls in love with him.  Almost by luck, Mr. Wegmus and Sonja kiss. The teacher promises to return for her. 
 Nazi party long before it came to power. As she digs further, local authorities stonewall her efforts.  Sonja persists and learns that there had been eight concentration camps in the area and that all the Jews were forced out of the town and had their property confiscated.

Sonja marries Martin, and the townsfolk think Sonja has dropped the issue of Nazi involvement. Sonja bears two daughters and studies history at the University. She resumes her research into the towns Nazi past, and wins court cases granting her access to archives. She still has to employ trickery to get the information she wants, however.  In response, her towns hostility grows from verbal abuse, to death threats to physical assaults as they attempt to silence her with increasing desperation, but nothing deters her.

Meanwhile, her husband feels emasculated as hes forced to take care of the children.  The family survives a bomb from the angry townsfolk, but Sonja keeps up her research.

At the end, the townspeople change their tune, even putting a bust of Sonja at the town hall.  Sonja sees this as a means to silence her and rejects the honor.

==Cast==
* Lena Stolze as Sonja
* Hans-Reinhard Müller as Juckenack
* Monika Baumgartner as Sonjas mother
* Elisabeth Bertram as Sonjas grandma
* Michael Gahr as Paul Rosenberger
* Robert Giggenbach as Martin
* Fred Stillkrauth as Sonjas uncle
* Barbara Gallauner as Miss Juckenack
* Udo Thomer as Archivist Schulz

==Awards== BAFTA Award for Best Film not in the English Language
* Nominated: Golden Globe Award for Best Foreign Language Film
*   (Michael Verhoeven)    New York Film Critics Circle Awards for Best Foreign Language Film

==Award nominations==
* Academy Award for Best Foreign Language Film
* Golden Bear, Berlin Film Festival

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 