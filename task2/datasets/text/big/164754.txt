Lilo & Stitch
 
{{Infobox film
| name           = Lilo & Stitch
| image          = LiloandStitchmovieposter.jpg
| caption        = Theatrical preview poster
| director       =  
| producer       = Clark Spencer
| writer         =  
| based on       = 
| starring       = {{plainlist|
* Chris Sanders
* Daveigh Chase
* Tia Carrere
* David Ogden Stiers
* Kevin McDonald
* Ving Rhames
* Kevin Michael Richardson
* Jason Scott Lee
* Zoe Caldwell }} 
| music          = Alan Silvestri
| editing        = Darren T. Holmes Walt Disney Feature Animation Buena Vista Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $80 million   
| gross          = $273.1 million 
}}
 adventure comedy-drama Walt Disney Walt Disney Chris Sanders Walt Disney Hollywood Studios Hayao Miyazakis film, Spirited Away, which also starred Daveigh Chase and David Ogden Stiers.
 Walt Disney Television Animation.

==Plot==
 
 Kaua i, but is knocked unconscious by a passing truck, and is taken to an animal shelter.

On Kaua i, Nani is struggling to care for her rambunctious younger sister, Lilo. A social worker named Cobra Bubbles expresses concern that Nani may not be able to adequately care for Lilo. Seeking a change, Nani decides to allow Lilo to adopt a dog and they go to the animal shelter, where Lilo immediately takes a keen interest in Experiment 626, who is impersonating a dog in order to escape. Lilo names 626 "Stitch (Lilo & Stitch)|Stitch" and shows him around the island. As Nani attempts to find a good job, Lilo tries educating Stitch about Elvis Presley, whom she calls a "model citizen". However, Stitchs antics, which include foiling Jumba and Pleakleys attempts to capture him, ruin Nanis chances of getting a job.
 wipe out, and Stitch to unintentionally pull Lilo down with him. Although everyone gets safely to shore, Cobra witnesses the event and tells Nani that even though she means well, Lilos best interests mean she must be placed with a foster family.  After Stitch sees how much trouble he has caused, he leaves. Meanwhile, the Councilwoman relieves Jumba and Pleakley of their assignment, which frees Jumba to pursue Stitch using less covert methods.

The next morning, David tells Nani of a job opportunity, which Nani rushes to pursue. Stitch, hiding in the nearby woods, encounters Jumba, who chases Stitch back to Lilos house. The two fight, destroying the house in the process as Nani returns and Cobra arrives to collect Lilo. As Nani and Cobra argue, Lilo runs away and encounters Stitch, who reveals his true form just as both are captured by Captain Gantu, who has been given the task of recovering Stitch. Stitch escapes before the ship takes off and is confronted by Nani. Before he can explain everything, Jumba and Pleakley capture Stitch themselves. Nani demands they rescue Lilo, but Jumba insists they only came for Stitch. As Nani breaks down over losing her sister, Stitch tells Nani about  ohana, a term for "family" he learned from Lilo, and convinces Jumba to help rescue Lilo. As Jumba, Pleakley and Nani give chase in Jumbas spaceship, Stitch drives a tanker truck full of fuel into a lava flow and uses the exploding tank to launch himself into Gantus cockpit, where he distracts Gantu enough to crash-land the ship and rescue Lilo.

Back on land, the Grand Councilwoman appears and takes Stitch into custody and also retires Gantu, but when Stitch explains that he has found a family in Nani and Lilo, she realizes he has become a civilized creature. The Councilwoman before leaving, decrees that Stitch will be exiled on Earth and entrusted into the care of Lilo and Nani, and asks Cobra to keep an eye on them. Together, they rebuild the house, having Jamba and Peakley become members of Lilo and Stitchs family as well.

==Cast==
  Christopher Michael Stitch
* Daveigh Chase as Lilo Pelekai
* Tia Carrere as Nani Pelekai
* David Ogden Stiers as Dr. Jumba Jookiba
* Kevin McDonald as Agent Pleakley
* Ving Rhames as Cobra Bubbles
* Kevin Michael Richardson as Captain Gantu
* Zoe Caldwell as Grand Councilwoman
* Jason Scott Lee as David Kawena
* Liliana Mumy as Mertle Edmonds
* Kunewa Mook as Moses Puloki
* Amy Hill as Mrs. Hasagawa
* Susan Hegarty as Rescue Lady
* Frank Welker as Pudge, Frog and Duck (uncredited)

==Production==

===Development===
  Chris Sanders, Kaua i Hawaiian islands before.  In Sanders words:

 i was kind of a big leap. But that choice went to color the entire movie, and rewrite the story for us.}}

===Writing===
Dean DeBlois, who had co-written Mulan (1998) with Sanders, was brought on to co-write and co-direct Lilo & Stitch, while Disney executive Clark Spencer was assigned to produce. Unlike several previous and concurrent Disney Feature Animation productions, the Lilo & Stitch pre-production team remained relatively small and isolated from upper management until the film went into full production.    The character and set designs were based upon Chris Sanders personal artistic style. 
 extended families. This concept of  ohana became an important part of the movie. DeBlois recalls:

 ohana, a sense of family that extends far beyond your immediate relatives. That idea so influenced the story that it became the foundation theme, the thing that causes Stitch to evolve despite what he was created to do, which is destroy.}}
 economic downturn, with the islands serene beauty. The actors voicing the films young adults Nani and David, Tia Carrere, a native of Honolulu, and Jason Scott Lee, who was raised in Hawaii, assisted with rewriting the Hawaiian characters dialogue in the proper colloquial dialect and adding Hawaiian slang.

One innovative and unique aspect of the film was its strong focus on the relationship between two sisters, Lilo and Nani. Making the relationship between sisters into a major plot element is very rare in American animated films.    

===Design and animation=== Snow White and the Seven Dwarfs (1937) and Dumbo (1941), the techique had been largely abandoned by the mid-1940s in favor of less complicated media such as gouache. Sanders preferred that watercolors be used for Lilo Pelekai|Lilo to evoke both the bright look of a storybook and the art direction of Dumbo, requiring the background artists to be trained in working with the medium.  The character designs were based around Sanders personal drawing style, eschewing the traditional Disney house style.  The films extraterrestrial elements, such as the spaceships, were designed to resemble marine life, such as whales and crabs. 

===Marketing=== The Little Back in Black."

In the United Kingdom, Lilo & Stitch trailers and TV ads featured a cover of Elvis song "Suspicious Minds", performed by Gareth Gates, who became famous on the UK TV program Pop Idol.

As a promotional campaign, comics of Lilo & Stitch were run in   rendering the comics non-canonical, but is notable to the series as introducing Experiment 625, Reuben, who was made a main character in the subsequent movies and TV series.

The comic series has been released as a collective volume titled Comic Zone Volume 1: Lilo & Stitch.
 Tutti Frutti" performed by Little Richard replacing Hound Dog.

===Deleted scenes=== Stitch was Jumba was one of his former cronies sent after Stitch by the Intergalactic Council to capture him.  Test audience response to early versions of the film resulted in the change of Stitch and Jumbas relationship to that of creation and creator, respectively. 
 CGI model of the 747 with that of Jumbas spaceship, with only a few shots in the sequence fully re-animated. 

Another scene that was deleted was one of Lilos attempts to make Stitch into a model citizen by notifying tourists on the beach about the tsunami warning sirens, which she knows are being tested that day.

The original version of Jumba attacking Stitch in Lilos home was found to be too violent by test audiences, and was revised to make it more comedic.

There was also a scene in which Lilo introduces Stitch to Pudge the fish, which ultimately leads to the fishs death. Lilo then takes Pudges body to the same graveyard where her parents were buried, and thus Stitch learns the consequences of his actions and gains a better understanding of mortality.

There was a scene where Nani brings Lilo pizza and then Lilo tells herself a bedtime story about a friendly and stinky bear named Toaster. This was replaced with the scene where Lilo and Nani talk about being family because test audiences had mistaken Nani for Lilos mother.

==Release==

===Box office=== Minority Report. In its second week it fell to #3, again behind the Steven Spielberg film at #2. The film raked in $145,794,338 in the United States and Canada, and $127,349,813 internationally, finishing with $273,144,151 in the world. 

===Critical reception===
Lilo & Stitch received highly positive reviews from critics. The films success at the box office and on home video formats led to a Lilo & Stitch franchise, with three direct-to-video sequels and a television series spanning two seasons. The film has received 145 critical reviews on the site Rotten Tomatoes, 124 Fresh and 21 Rotten, giving it a positive total rating of 86%. The sites consensus reads "Edgier than traditional Disney fare, Lilo and Stitch explores issues of family while providing a fun and charming story."  The film has also earned a score of 73 on Metacritic. 
Roger Ebert of the Chicago Sun-Times gave the film 3.5 stars out of 4 and wrote "Its one of the most charming feature-length cartoons of recent years -- funny, sassy, startling, original and with six songs by Elvis".

Peter M. Nichols states that through the character of  , and Treasure Planet. 

===Home video===
The film was released on VHS and DVD on December 3, 2002.    In 2003, a 2-disc DVD version was announced alongside special edition DVDs of  , but a release in the US suffered from continuous delays.

On March 24, 2009, Disney re-released the DVD, which they dubbed a 2-Disc Big Wave Edition.    This set includes most of the bonus features from the original DVD and adds an audio commentary, a two-hour making-of documentary, more deleted scenes including the original climax with the plane hijacking, a number of behind-the-scenes featurettes, and some games. 

The film was released on  . 

==Soundtrack==
{{Infobox album
| Name        = Lilo & Stitch
| Type        = Soundtrack
| Artist      = Various artists
| Cover       = Lilo & Stitch (soundtrack album - cover art).jpg
| Released    = June 11, 2002
| Recorded    = 2001-2002
| Genre       = Rock, country rock, Pop
| Length      = 34:47 Walt Disney
| Producer    = Chris Montan  (executive) 
| Music       = Alan Silvestri
| Chronology  = Lilo & Stitch music
| Last album  =
| This album  = Lilo & Stitch (2002)
| Next album  =   (2006) Misc = {{Singles
 | Name            = Lilo & Stitch
 | Type         = soundtrack
 | Single 1      = Cant Help Falling in Love#A-Teens version|Cant Help Falling in Love
 | Single 1 date = October 29, 2002
}}}}

{{Album ratings
| rev1      = AllMusic
| rev1Score =   
}}
 Audio CD and Compact Cassette.

===Track listing===
{{Track listing
| extra_column    = Performer
| title1   = Hawaiian Roller Coaster Ride Mark Keali i Ho omalu, The Kamehameha Schools Childrens Chorus
| length1  = 3:28 Stuck on You
| extra2   = Elvis Presley
| length2  = 2:25
| title3   = Burning Love
| extra3   = Wynonna
| length3  = 3:10
| title4   = Suspicious Minds
| extra4   = Elvis Presley
| length4  = 3:23
| title5   = Heartbreak Hotel
| extra5   = Elvis Presley
| length5  = 2:13
| title6   = (Youre the) Devil in Disguise
| extra6   =  Elvis Presley
| length6  = 2:30
| title7   = He Mele No Lilo Mark Keali i Ho omalu, The Kamehameha Schools Childrens Chorus
| length7  = 2:28 Hound Dog
| extra8   = Elvis Presley
| length8  = 2:27
| title9   = Cant Help Falling in Love
| extra9   = A*Teens
| length9  = 3:07
| title10  = Stitch to the Rescue (score)
| extra10   = Alan Silvestri
| length10 = 5:57
| title11  = You Can Never Belong (score)
| extra11   = Alan Silvestri
| length11 = 3:56
| title12  = Im Lost (score)
| extra12   = Alan Silvestri
| length12 = 4:43
}}

===Charts===
{| class="wikitable sortable"
|-
!Chart (2002)
!Peak  position
|- US Billboard 200|Billboard 200 11
|- US Billboard 200|Billboard Top Soundtracks 1
|}

==Sequels==
 
On August 26, 2003, Disney released a direct-to-video sequel,  . This series ran for 65 episodes between September 20, 2003 and July 29, 2006. The series carried on where the film left off and charted Lilos efforts to capture and re-home Jumbas remaining experiments. This series almost ended with TV movie Leroy & Stitch, which was released on June 27, 2006.

On August 30, 2005,  , the "official" direct-to-video sequel to the film, was released. In this film (set between the first and second films), Stitch has a glitch because his molecules were never fully charged (this is contrary to an alternate opening, "Stitchs trial", which was seen on the DVD release of Lilo & Stitch). Lilo wants to win the May Day hula contest like her Mom did in the 1970s, but Stitch continues to have outbursts. Lilo gets increasingly mad at Stitch as his glitch causes more problems for her and ruins her chances of winning the competition. She thinks Stitch is not cooperating properly, until she finds out that Stitch is dying.

In March 2008, Disney announced a reimagined version of Lilo & Stitch, titled Stitch!, aimed at the Japanese market. The show, which began in October 2008, features a Japanese girl named Yuna (formerly referred to as Hanako) in place of Lilo, and is set on a fictional island in Okinawa Prefecture instead of Hawaii. The series is produced by the Japanese animation house Madhouse LTD.

==Video games==
There were two official games released to coincide with the film,   for the  ".

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 