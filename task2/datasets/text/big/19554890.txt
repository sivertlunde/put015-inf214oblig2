The Thaw (film)
{{Infobox film
| name           = The Thaw
| image          =
| caption        =
| director       = Mark A. Lewis
| producer       = Trent Carlson Rob Neilson Mary Anne Waterhouse
| writer         = Mark A. Lewis Michael Lewis
| starring       = Val Kilmer Martha MacIsaac Kyle Schmid Steph Song Aaron Ashmore
| music          =
| cinematography =
| editing        =
| studio         = Anagram Pictures Inc.
| distributor    =
| released       =  
| runtime        =
| country        = United States Canada
| language       = English
| budget         =
}} science fiction horror/thriller film directed by Mark A. Lewis starring Val Kilmer and Martha MacIsaac.

==Plot==
 
The film begins with a video documentary by Dr. David Kruipen, a research scientist in the Canadian Arctic. This is followed by a flashback to when David, his assistant Jane and two other researchers tranquilize a polar bear, then discover the frozen remains of a wooly mammoth.

They transport the polar bear to their research station. David calls his daughter Evelyn and pleads with her to visit the research station. Their relationship has been strained since the recent death of her mother. Meanwhile, a group of students, Ling, Freddie and Adam, are selected to join Davids research team; Evelyn decides to come.

Days later, Davids research team begins feeling the effects of a strange illness, and David tells helicopter pilot Bart not to bring his daughter Evelyn to the station. However, Evelyn insists, Bart ignores Davids orders, and brings her to the station along with the students. In the meantime, Jane shoots and apparently kills David and another researcher. The students discover the body of the polar bear, and Bart is bitten by a bug while touching it.

Evelyn is awakened in the middle of the night by an ATV approaching the station. When she goes to investigate, Jane falls from the helicopter. Jane has destroyed the helicopters control panel, eliminating any immediate chance of escape. Ling wakes up with many bug bites on her face and torso, and Jane dies in Evelyns arms. Realizing Ling is sick, Freddie calls in a helicopter to rescue Ling.

Attempting to find David, Evelyn and Adam discover eggs in the brain of the mammoth. Assuming her father has been infected, Evelyn and Adam deduce that something has made the group sick. Evelyn decides to cancel the rescue helicopter and quarantine the group until a better-prepared team can rescue them. Freddie, discovering he too is infected, goes berserk and destroys the radio.

The bite on Barts arm is infected and he has Adam and Evelyn amputate his arm at the elbow. The group decides to destroy the facility and wait things out in the helicopter. Ling is attacked by bugs who have made their way in through the ventilation system. Bart discovers that the amputation was useless as his upper arm is now showing signs of infection; he opts to stay behind with Ling. They deliberately overdose on morphine and fall asleep as the bugs swarm over them.

Freddie comes running out, refusing to be checked for infection, then turns on Evelyn and David. As he is about to shoot Evelyn, he is shot from behind by David. David insists they destroy the research station. Evelyn finds a video David recorded and discovers that David has intentionally infected himself, preparing to set the bugs loose to teach humanity a lesson about global warmings effects.

As a rescue helicopter comes to pick up David, Evelyn and Adam are locked up by David, but they escape. Adam tries to hang on to a helicopter skid but falls to the ground. As the helicopter starts flying away, Evelyn shoots at it, causing it to crash into a building previously doused in gasoline. David and the helicopter crew die in the resulting fire; Evelyn finds Adam, who dies in her arms.

The following day another rescue team arrives, finding the destruction and Evelyn, the sole survivor. Later, as a radio station airs information based on Evelyns reports, a hunter calls his dog away from a dead bird the dog has been eating. Bugs emerge from the birds body. The closing scene shows the hunters truck heading towards a large city.

==Cast==
*Martha MacIsaac as Evelyn, Dr. Krupiens daughter    
*Aaron Ashmore as Adam, a student, Evelyns love interest 
*Kyle Schmid as Freddie, a student with a severe phobia of insects  
*Steph Song as Ling, a smart, assertive student who is trying to dispel rumors of her promiscuity 
*Val Kilmer as Dr. Kruipen, the leader of the expedition 
*Viv Leacock as Bart, as a helicopter pilot & Dr. Kruipens friend.
*Anne Marie Loder as Jane (as Anne Marie Deluise) 
*John Callander as Edward, Dr. Kruipens so-worker
*Lamech Kadloo as Nuti, Dr. Kruipens so-worker
*Sebastian Gacki as Chad, Evelyns friend
*Alejandro Rae as Rob, Barts so-worker

==Reception==
The film has received generally poor reviews. At Rotten Tomatoes, only 29 out of 100 people had positive reviews.

==See also==
*"Ice (The X-Files)|Ice", the eighth episode of The X-Files, also dealing with a deadly parasite unleashed on an Arctic research station.
*Series 1 of "Fortitude_(TV_series)|Fortitude", the Sky Atlantic thriller, dealt with a similar theme of deadly, prehistoric bugs being released from the remains of a frozen mammoth.

==References==
 

==External links==
*  

 
 
 
 
 
 