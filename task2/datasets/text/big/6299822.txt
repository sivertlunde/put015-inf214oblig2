American Hardcore (film)
{{Infobox film
| name=American Hardcore
| image= American hardcore ver2.jpg
| caption = The movie poster for American Hardcore.
| writer=Steven Blush
| starring= Various musicians, entertainment executives, etc.
| director=Paul Rachman
| music=
| distributor=Sony Pictures
| released=September 22, 2006
| runtime=
| language=English
| movie_series=
| awards=
| producer=Steven Blush Paul Rachman
| budget=}}
 Black Flag, The Minutemen, SS Decontrol, and others. It was released on DVD by Sony Pictures Home Entertainment on February 20, 2007.

==Plot==
This documentary film addresses the birth and evolution of hardcore punk rock from 1978 to 1986 (although the packaging says 1980-1986). The documentary boasts extensive underground footage shot during the height of the hardcore movement.  It features exclusive interviews with early hardcore punk music artists from bands such as Black Flag, Minor Threat, Bad Brains, and many more.

==Production==
The film was shot and edited for 5 years with many clips of 80s hardcore bands being sent in by the bands themselves. Some of the footage was shot by director Paul Rachman in the 80s with the most notable piece of footage being the final show of Negative FX which spawned a riot after the power was cut mid-song.

Many of the interviews were actually done in both Paul Rachmans and Steven Blushs apartments in different areas to make it seem like they were done in different locations.
 Wasted Youth covered in blood was used for the book cover.

==Soundtrack==
{{Infobox album  
| Name        = American Hardcore OST
| Type        = studio
| Artist      = Various artists
| Cover       = Americanhardcoresoundtrack.jpg
| Released    = 10 October 2006
| Recorded    = 
| Genre       = Hardcore punk Thrashcore
| Length      = 37:01 SST
| Producer    = 
}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =   
| noprose = yes
}} Nervous Breakdown Black Flag  Middle Class 
3.   Pay To Cum - Bad Brains 
4.   Fucked Up Ronnie - D.O.A. (band)|D.O.A. 
5.   Red Tape - Circle Jerks 
6.   Filler - Minor Threat  MDC  Untouchables 
9.   Kill a Commie - Gang Green 
10. Boston Not L.A. - The Freeze 
11. Straight Jacket - Jerrys Kids (band)|Jerrys Kids  SSD  Void  Scream 
15. Enemy for Life - YDI 
16. Runnin Around - Dirty Rotten Imbeciles|D.R.I. 
17. Dont Tread On Me - Cro-Mags 
18. Friend or Foe - Negative Approach  Articles of Faith 
20. Think For Me - Die Kreuzen  7 Seconds 
22. Brickwall - Big Boys 
23. I Was a Teenage Fuckup - Really Red 
24. I Hate Children - The Adolescents 
25. My Minds Diseased - Battalion of Saints  Flipper 
27. Victim In Pain - Agnostic Front

==Interviewees==
Interviewees include: Slapshot
*Phil Anselmo formerly of Pantera and Superjoint Ritual Brian Baker of Bad Religion, formerly of Minor Threat and Dag Nasty
*Dicky Barrett of Mighty Mighty Bosstones, formerly of Impact Unit and The Cheapskates Articles of Faith
*Dave Brockie of Death Piggy (which would later form into the band GWAR), formerly of X-Cops (band)|X-Cops, and the Dave Brockie Experience
*Brandon Cruz of Dr. Know (band), formerly of the Dead Kennedys
*Bob Cenci of Gang Green and Jerrys Kids (band)|Jerrys Kids Black Flag roadie and formerly of Nig-Heist Mike Dean of Corrosion of Conformity MDC
*Chris Doherty of Gang Green, formerly of Jerrys Kids (band)|Jerrys Kids
*Harley Flanagan founding member of the Cro-Mags Flea of Fear
*Flipper Flipper members Bruce Loose, Ted Falconi and Steve DePace Black Flag
*Jack Grisham of T.S.O.L.
*Brett Gurewitz of Bad Religion, owner of punk rock record label Epitaph Records
*H.R., Dr. Know (guitarist)|Dr. Know and Darryl Jenifer of Bad Brains
*Greg Hetson of Bad Religion and the Circle Jerks John Joseph of the Cro-Mags
*Joe Keithley of D.O.A. (band)|D.O.A. Terror
*Alec The Faith
*Ian Mackaye of Fugazi, formerly of The Teen Idles and Minor Threat
*Paul Mahern of Zero Boys Flipper  Black Flag
*Reed Mullin of Corrosion of Conformity
*Rev. Hank Peirce former roadie for Slapshot (band)|Slapshot, Corrosion of Conformity and Uniform Choice Black Flag Black Flag and State of Alert 7 Seconds Down by Law, formerly of DYS (band)|D.Y.S., ALL (band)|ALL, and Dag Nasty The Misfits
*Dave "Springa" Spring formerly of SS Decontrol
*Vinnie Stigma of Agnostic Front Minutemen and Firehose
*Perry Webb of Culturcide Warzone and Murphys Law (band)|Murphys Law
*Mike Patton of Middle Class, Elysian Fields, Trostky Icepick and Jeff Atta of Middle Class

==References==
 

==External links==
* 
* 
*  at Metacritic.com
* 
* 
* 
* 

 
 
 
 
 
 