H-2 Worker
 

{{Infobox film
| name           = H-2 Worker
| image          = 
| image_size     =
| caption        =
| director       = Stephanie Black
| producer       = Stephanie Black
| writer         = 
| narrator       =
| starring       = 
| music          = Various, including "H-2 Worka" by Mutabaruka
| cinematography = Maryse Alberti
| editing        = John Mullen
| studio         = 
| distributor    = New Video
| released       =  
| runtime        = 67 min
| country        = United States
| language       = English
| gross          = 
| preceded_by    =
| followed_by    =
}} Belle Glade, Clewiston, Florida|Clewiston, and Okeelanta, Florida as well as Jamaica and includes cane fields and worker camps (Ritta Village, Prewitt Village) owned by US Sugar Corporation and the Okeelanta Corporation.

The cane harvesters were brought in to perform the autumn harvest of sugar cane under the H-2A Visa program.  The Jamaicans replaced earlier generations of Bahamian seasonal workers who in turn replaced migrant labor recruited from the Cotton Belt (region) in the first half of the 20th century.  A documentary short that accompanies the DVD version of the film states that human labor was abandoned for mechanical harvesters in 1992.

The film features interviews with a United States Department of Labor official, a Florida Sugar Cane League official, Jamaican Prime Minister Michael Manley, local merchants, and a dozen or so field workers.  It also includes footage of César Chávez, US Representative Thomas Downey, and US Senator Bill Bradley.

==References==
 

==External links==
* 

 
{{succession box
| title= 
| years=1990
| before=For All Mankind American Dream}}
 

 
 
 
 
 
 
 
 

 