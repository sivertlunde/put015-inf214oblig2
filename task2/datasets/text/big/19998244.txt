Sathi (2002 film)
 
{{Infobox film
| name = Sathi
| image =Sathi.jpg
| image_size     =
| caption        = The DVD cover film Sathi.
| director = Haranath Chakraborty
| writer = Jeet Priyanka Trivedi Ranjit Mullick
| producer = Shree Venkatesh Films
| distributor =
| cinematography =
| editing =
| released = 14 June 2002
| country        = India
| budget         =
| gross          =
| followed_by    =
| runtime = Bengali
| music = S. P. Venkatesh
| website        =
}}
 Bengali movie Tamil film Thulladha Manamum Thullum. Singer S. P. Balasubramanyam sang all of the songs.

==Plot==

The movie visualises the sacrifice made by the protagonist for his love. Bijoy (Jeet (actor)|Jeet) is an aspiring singer with lots of talent, who resides in a village of Burdwan district. He has a caring and loving mother and a cute and helpful brother. He comes to Calcutta on the call of a musical tycoon but when he finally arrives at his home Bijoy is shocked to find his mentor dead. He takes refugee with the alcoholic Keshtoda (Ranjit Mullick) a person with a heart of gold. He architects a group of peers and all of them begin to work for an insurance company. In the meanwhile Bijoy views Sonali (Priyanka Trivedi) who stays in the same locality with her grandmother. Sonali knows Bijoy in a peculiar way. She has never seen him, but when Bijoy chants his numbers, she hears them utmost interest and becomes his ardent admirer. But whenever Bijoy and Sonali physically meet each other something or the other mishaps occurs which make Sonali misunderstand. Thus Sonali has a different picture of Bijoy in her sub-consciousness. She does not realize that one she admirers is the same person whom she hates. Things come to standstill, when Bijoy accidentally pushes Sonali from the stairs of her college and she becomes blind. When Bijoy comes to know about this, he is shattered. Then begins his series of sacrifices and love, he helps the financially strained Sonali and her grandmother by giving their rent. He brings the duo at their mess when they are expelled by their landlord and finally he sells the precious ring (Bijoys mothers gift) to allocate the fees and cost required for Sonalis eye treatment. Even when his mother expires he keeps it a secret to everyone and takes Sonali to the eye surgeon. There he knows that Sonali can regain her eyesight if she undergoes an acute and complex operation. To acquire the cost of  , Bijoy secretly wards off to Vishakapatnam with the desting to sale one of his kidneys. Bijoy sacrifices his passion to become a vocalist and secretly supports Sonali to fulfill her dream of becoming a singer, while Sonali gets success in her first stint at the recording studios, Bijoy is arrested as a terrorist on the platform of Vizag station. Thus with Bijoys money Sonali regains eyesight and becomes a famous singer while Bijoy is sentenced to 5 years rigorous imprisonment. The day Bijoy returns after completing his sentence turns out to be the same day when Sonali was attending her musical concert. Bijoy after arriving at Howrah station eyes the hoardings of having huge cutouts of Sonali. He reaches the spot but when he tries to contact Sonali and explain to her that he is her Bijoy, he is brutally hammered by the security. Keshtoda and Bijoys friend arrive at the juncture. They explain to Sonali the entire truth. Sonali realises that the one she has neglected for ever is her long lost love. They reunite amidst a jubilant crowd.

==Cast== Jeet .... Bijoy
* Priyanka Trivedi .... Sonali
* Ranjit Mullick .... Keshtoda
* Anamika Saha Rajesh Sharma
* Kanchan Mullick as Bijoys Friend
* Pushpita Mukherjee

==Crew==
* Producer(s): Shree Venkatesh Films
* Director: Haranath Chakraborty Plot section
<!--*Production Design:
* Dialogue:
* Lyrics:
* Editing :
* Cinematography:-->

==Music==
{{Listen
 | filename    = O Bondhu Tumi Shunte Ki Pao.ogg
 | title       = O Bondhu Tumi Shunte Ki Pao sample music
 | description = A 1 minute 3 seconds sample of the most popular song of the film "O Bondhu Tumi Shunte Ki Pao".
 | format      = Ogg
 | pos = right
}}
* "Ei Bhalobasa Tomakae Pete Chay" - S. P. Venkatesh
* "Bolbo Tomay Ajke Aami"- S. P. Venkatesh
* "Pora Bansi Dak Diye Jay" - S. P. Venkatesh
* "Ei Gaan Moner Khataate" - S. P. Venkatesh
* "O Bondhu Tumi"- S. P. Venkatesh

==Critical reception==
The movie received mixed reviews.   

==References==
 

==External links==
*  

 
 

 

 
 
 
 
 
 