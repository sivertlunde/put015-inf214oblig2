The Figurine
{{Infobox film
| name           = The Figurine: Araromire
| image          = Thefigurine.jpg
| alt            =
| caption        = Theatrical poster
| director       = Kunle Afolayan
| producer       = Golden Effects 
| writer         = Kemi Adesoye 
| narrator       = Lagbaja Funlola Aofiyebi-Raimi Tosin Sido
| music          = Eilam Hoffman Wale Waves
| cinematography = Yinka Edward
| editing        =  
| studio         = Golden Effects Studios   Jungle FilmWorks
| distributor    = Golden Effects Pictures
| released       =  
| runtime        = 122 minutes
| country        = Nigeria
| language       =  
| budget         = Nigerian naira|₦50   - 70 million 
| gross          = ₦30,000,000 (domestic gross)    
}} suspense thriller film written by Kemi Adesoye, produced and directed by Kunle Afolayan, who also stars in the film as one of the main protagonists. It also stars Ramsey Nouah and Omoni Oboli.

The movie narrates the story of two friends who finds a mystical sculpture in an abandoned shrine in the forest while serving at a National Youth Service Corps camp, and one of them decides to take the artwork home. Unknown to them, the sculpture is from the goddess Araromire which bestows seven years of good luck on anyone who encounters it, and after the seven years have expired, seven years of bad luck follow. The lives of the two friends begin to change for good, as they become successful and wealthy businessmen. However, after seven years, things start to change for the worse.  

The original idea for a thriller film came long ago from Kunle Afolayan himself and Jovi Babs and it was to be titled Shrine. The script took nine months to be finalized and the development stage took five years. The film was shot in Lagos and Osun State for three months and received sponsorship from companies such as MicCom Golf Resort, GSK, Omatek Computers, MTN, IRS Airlines, and Cinekraft. It also had media partners like HiTV and others. There were no professional stunts on the ground, so most of the stunts were performed by Kunle Afolayan on set.

The film was first released at the 2009  , detailing scholarly analysis of events in the film was released. The book was also welcomed with positive reviews.   The Figurine has also been used as a subject of educational studies in the Arts department of some higher institutions.      

==Plot==
The film opens in Araromire in 1908: There is an old folktale about a goddess Araromire who asked a priest to invoke her spirit into a figurine carved out of the bark of a cursed tree. When villagers of Araromire touched the figurine, Araromire would grant them wealth and prosperity in all their endeavours. This good fortune however unfortunately lasted for just seven years, after which everything deteriorated and became worse than it was seven years ago. The climax of the disaster was when the priest who invoked her spirit was found dead at the river. The villagers become infuriated and invaded the Araromire shrine; the shrine is burned down and that ended the evil of Araromire.

Lagos 2001: Femi (Ramsey Nouah) is about to go for his National Youth Service Corps|NYSC; He has been posted to Araromire – at this time, his father (David J. Oserwe) has been very ill (it is later revealed in the movie that he was dying of cancer).
 Dean of his faculty to get his clearance for NYSC. The Dean recognizes him as an irresponsible student who rarely attended his lectures. He further tells him that he used to date Mona who is on the contrary: a brilliant girl. The Dean asks him where he has been posted to; "Araromire", he replied. The Dean picks interest and brings out a book. He starts to narrate the folk story about the village and the goddess it was named after. However, Sola fails to listen to rest of the tale (the disaster which follows the seven years) as he politely leaves the deans office with disinterest.

Femi and Mona (Omoni Oboli) meet at the car park - She has been posted to Araromire as well; it is revealed here that they were once close friends. They both board a bus headed for Araromire. While in the bus, Mona is asleep and Femi brings out a ring; he stares at the ring and cuddles it while he stares at sleeping Mona. He is also seen cuddling the ring later at the camp.
 endurance trek, Femi becomes drained and he stops in the woods to use his inhaler as he is an asthmatic patient. Sola also waits behind to help him out. While in the jungle, they begin to hear a strange sound which always comes up during camo parade. They both start to trace the sound and later discover a strange object on top of a tree that is emitting the sound. Rain starts to fall and they both find shelter in a mud house. Sola uses his lighter and starts to explore the place. The rain has stopped and Femi moves out of the building to get fresh air. Sola finds a figurine in an enclosed in a dusty wooden cradle. On the last day of Parade, Mona informs Femi that Sola proposed to her and she accepted. She also stated that shes carrying his baby. Femi seems jealous and tries to discourage Sola from getting married, but Sola declines. Sola brings out the figurine he found in the mud house and drops it on Femis lap. Femi gets back from the NYSC only to see his father hale and hearty. He is also selected to go for a special training at his work place within just six months of his appointment. Sola gets a job as well and marries Mona.

Seven years later, Femi arrives from abroad and attends Solas get-together. Mona (who is pregnant again) introduces Femi to Linda (Funlola Aofiyebi), a fashion designer. Throughout the party, Femi pays no attention to her and keeps staring at Mona whom he is apparently still in love with. Lara (Tosin Sido), Femis younger sister, now lives with Solas family as Mona is helping her with her thesis. However, The Araromire figurine is still kept at Solas. Femis asthma has been cured miraculously as he smoked at the party.

Mona gets to know about the Araromire legend and she is worried; she begins to make conclusions and starts doubting the real source of the sudden wealth of the family. She also deduces that it may be the reason why her marriage with Sola has been so flawless, despite Sola having been a rascal and a Casanova back in school. She asks Lara to throw the figurine away but it returns into the house mysteriously. She then decides to throw it into the lagoon herself, she loses her pregnancy on her way to the lagoon. While dancing at the club with Linda, Femi gets an asthma attack again after so many years. Sola also starts having an affair with Ngozi again after so many years of being faithful to Mona, it is later discovered that he is having an affair with Lara as well and he also had an affair with Linda. Femi arrives at his apartment one day and sees his father dead in his crafts workshop. Both Femi and Sola eventually lose their jobs. Femi raises the topic of the figurine and suggests that they return it to the shrine; though Sola still doesnt believe the figurine is responsible for all the misfortune, he agrees. Junior (Tobe Oboli), Solas first son, also dies by falling off from the building while trying to reach out to his mother — his father was having a big argument with his mum and the figurine was burnt. However, the figurine returns again. Then, Sola and Femi set out to return the figurine.

Mona, now in depression, sends Lara out of her house believing she let her son die (She was supposed to be with him at the time Junior had the accident). Laras box opens up while shes struggling with Linda who is throwing her things out and also pushing her out of the compound. Lots of Figurines; similar to the one in Solas study start to fall out of Laras box. Apparently, she has been responsible for the return of the figurine each time it was thrown out! Shes called in and she starts to make confessions revealing that She is being controlled by her brother, Femi. Femi and Sola arrive at the Araromire Shrine to return the figurine; rain starts to fall and Femi is nowhere to be found. While Sola is busy looking for Femi, Femi kills him by hitting him with a log of wood. Femi returns to Lagos and he is confronted by Linda, whom Lara has already confessed to. Femi confirms the truth by revealing many things which proves that the whole Araromire good luck and bad luck was all a charade orchestrated by him in order to kill Sola and get Mona all to himself. He terms the losing of their jobs, his fathers death and his asthma coincidence. He kills Linda afterwards and dumps her body in the lagoon. On his way back, he has his asthma attack and commands Lara to bring him his inhaler, but she refuses; instead, she calls the Rapid Response Squad to report her brothers killings. Femi struggles to get upstairs. He reaches out to the body of Mona, he removes Solas ring from her finger and replaces it with his ring (which he was always staring at earlier). The Rapid Response Squad arrives and declares Femi dead.

Linda, Femi and Mona are later seen alive and the movie ends with the caption What do you believe?   

==Cast==
 
* Ramsey Nouah as Femi, an unpredictable nerd who falls in love with his friend - Mona, this feeling of love overtakes him until he becomes irrational and Violent. He orchestrates a plan to win his love back by making the Araromire myth a reality. Nouah has expressed love for his role in this film and the film as whole on several occasions,  He stated that the movie had everything Nollywood lacked in storytelling at the time and he was glad the movie was coming out of Nollywood. He stated in an interview that The Figurine was the only movie hes acted that hes willing to keep to show his kids when they are grown.    
* Omoni Oboli as Mona, a close friend to Femi who is oblivious of the fact that Femi is desperately in love with her as shes in love with Sola, who is also Femis close friend, but he however knows that Femi is interested in Mona. Mona eventually gets married to Sola after getting pregnant for him. Oboli stated that she likes Kunle Afolayan as a director and she wishes to learn a lot from him as shes hoping to become a director herself. She also stated that it was stressful on set as she was shooting with her real son (Tobe Oboli), so she had to take care of her son and shield him from likes of Kunle and Ramsey who would want to talk about "adult stuff" on set. According to her, the scene where she was dragged down the stairs was the most difficult as it was shot over and over and she got a lot of bruises as a result.   
* Kunle Afolayan as Sola, an unserious Casanova who finds a mystical, Araromire while serving as a youth corps and takes it home. Despite his wayward ways and terrible grades, he gets a job, marries Mona, gets rich and raises a wonderful family until things start to turn around after seven years. Afolayan, who was also the director of the film expressed that it is very stressful to act in ones film. Art Director Pat Nebo stated "Kunle showed heavy maturity when it mattered most; cos he had to act, then go back to direct". 
* Funlola Aofiyebi as Linda, an outgoing fashion designer who is a little desperate to get married because of her age. She ends up with Femi who is still in love with Mona.
* Tosin Sido as Lara, Femis younger sister whom hed use to carry out his plans. He gives her lots of Figurines crafted by their Father who is a Sculptor to scare Sola and Mona into breaking up. This is Tosin Sidos first film role. She was one of the actresses auditioned to play Femis younger sister and she got the role.  western museums.
* Kate Adepegba as Ngozi, one of the Older Ladies Sola used to date; he still cheats on Mona with her after marriage.. Dean in school
* Wale Adebayo as Camp Commandant
* Ombo Gogo Ombo as Araromire Priest

The narration at the beginning of the movie was done by the highlife singer Lagbaja. Pat Nebo, the art director, played the marriage registrar at Sola and Monas wedding ceremony. Yomi Fash Lanso played Femis Doctor in the movie. Jide Kosoko played an antagonistic Chairman and featured in two scenes in the film. The make-up artist on set, Lola Maja also played the doctor at the scene where Junior was taken to the hospital. She was also the Doctor that came to give Mona her medication, making her feature in a total of two scenes.

==Production==

===Development===
The Figurines funding came up when lots of questions were being asked about Nollywood, about the level of mediocrity and lack of good structure in the industry. Before The Figurine, only few movies were fit to be shown in theatres as Nollywood was a home video-dominated Industry. 
Kunle Afolayan threw up a challenge to do something different which would be acceptable globally. The original idea for a thriller movie came long ago from Kunle Afolayan himself and Jovi Babs and it was to be titled Shrine. In 2006, Kunle Afolayan met a scriptwriter, Kemi Adesoye, and she was signed to write the script for the movie. A first draft script written by Kunle Afolayan was given to her. After going through the script, she said to the director "Its funny, not a thriller". The script had to be scrapped and another four different versions of a thriller movie were later written with the title Shrine.     Care had to be taken when writing the script so as not insult peoples beliefs; according to the Art Director, Pat Nebo, "living in Nigeria is a war between our culture and our love for independence in different ways".   The script for the movie took nine months to be finalized and the development stage took five years. Within these periods, Kunle Afolayan shot a movie titled Irapada; another spiritual-themed thriller which he said was just an "experiment".     When he saw the success of Irapada, he then revisited The Figurine. 

According to Kunle, all his life savings were put into the movie.  Meanwhile, the movie got sponsorship from a number of brands and corporate organizations which include MicCom Golf Resort, GSK, Omatek Computers and so on. After completion of the film however, other companies came on board like MTN, IRS Airlines, Cinekraft and Media partners like HiTV and many others. 

===Design of Araromire sculpture===
 
In designing the sculpture of the figurine Araromire, the sculptor stated that, as he understood the film, the sculpture should have a connection with African history and at the same time have expression of humanity like breasts (milk). A jewel is also held by the figurine which indicates some kind of magical powers. And above all, the figure is naked, because life itself is naked. 

===Filming===
The film was shot in Lagos and Ada, Osun State for three months.  The tourist attraction in Osun was shown in the movie by filming at the water falls, hills and the forests.  MicCom Golf and Hotel Resort provided free accommodation for the cast and crew while they shot in Osun State, some scenes in the film were shot at the resort as well.  The equipment used for the movie were gotten from Jungle Film Works; the only place in Nigeria at the time to get all the equipments needed for big budget movies. Jungle film Works eventually came on board as associate producers for the film.

There were no professional stunts on ground, so most of the stunts were performed by Kunle Afolayan including a scene where the Priest was floating on the surface of the River; Kunle Afolayan was under the water lifting Ombo up. Other stunts were also performed by Mayowa Adebayo.

For the two Shrine scenes where Rain was needed, the crew were in a Village in Osun state so they had to contact the fire fighters from the city to help out. At the first day of shoot, there was not enough pressure for the water to create the needed effect; the water eventually got finished and the shoot was cancelled for that day and was shot all over again the next day.

There was a lot of makeup and special effects used on Ombo (the Priest); He is not very old, so apart from body paintings, skin bags had to be applied on his face, long grey hairs had to be attached to his eyebrows to make him look weird and fake contacts were added to his eyes as well.

 
According to the script, the house Sola and Mona live is supposed to be a Lagoon House. It took three weeks; Kunle and the Location Manager, Biodun Apantaku visited all Houses in Lagos Island, but no one would release his house for a film shoot. The set later had to be created: the Set Designer had to erect a structure and create the necessary parts of the house (Sitting room, Kitchen and bedroom). However, sets for a couple of scenes were also created; for example, the mud house where the figurine was found was also erected. 

On a day of shoot, the crew were trying to move all the equipments to the Island by boat, including the generator: The boat capsized and shooting was shut down for over a week. When they resumed, smaller generators were gotten. As a result of the heavy lights they were using; One of the generators developed faults and everything that was brought blew up: the battery, the charger got burnt and Shoot had to be cancelled for days again. 

===Music and soundtrack===
{{Infobox album
| Name        = The Figurine (Araromire) Soundtrack
| Type        = Soundtrack
| Artist      = Wale Waves
| Cover       = Araromire OST.jpg
| Released    = September 2013
| Genre       = Film soundtrack
| Length      = 21:50
| Label       = Cisum Entertainment
| Producer    = Netto   George Nathaniel
| Last album =
| This album =
| Next album =
}}

{{Album ratings
| rev1 = Nollywood Reinvented
| rev1Score =   
|noprose=yes}}

The song "Araromire" by Wale Waves is one of the most anticipated and highly acclaimed film soundtracks of the 2000s and Wale Waves has been constantly praised for his beautiful and powerful vocals on the song.      The soundtrack was sang by Wale Waves, produced by Netto and George Nathaniel under Cisum Entertainment music label.       Africalypso is a song by Lagbaja from Africano...the mother of groove album of 2005 and was produced by Motherlan Music label.  Additional music was scored by Eilam Hoffman, who also did the music editing for the film. 

====Track listing====
{{track listing
| headline =
| extra_column = Singer(s)
| music_credits = no
| total_length = 21:50

| title1 = Araromire
| extra1 = Wale Waves
| length1 = 02:53

| title2 = We are together
| extra2 = George Nathanie
| length2 = 02:00

| title3 = Africalypso
| extra3 = Lagbaja
| length3 = 07:07

| title4 = Awade Wasiu Ayinde Barrister
| length4 = 09:50
}}

==Release==

===Pre-release and promotions===
According to Kunle Afolayan, it was important to reach out to the audience and one good way was through social networking sites like Facebook, Twitter and YouTube. While the film was in production, photographs of scenes and locations were posted on the group page set up for The Figurine. He also stated that another important strategy was word of mouth from which the movie benefited a lot.   

A teaser trailer was released during the early months of 2009  and the movie sparked anticipations among movie fans.  The first official trailer for the movie was released on 10 August 2009  and a Cinema promo was also released to the public on October 1, 2009  

===Premiere===
The movie premiered in film festivals including The London African Film Festival, New York African Film Festival, Tokyo African Film Festival and Rotterdam International Film Festival.
Golden Effects Pictures and Silverbird distributed the movie in Nigeria.

It premiered in Lagos on 2 October 2009 and afterwards screened across Africa, Europe, America and Asia.  Releases were also subtitled in French with the title La Figurine  as well as Spanish with the title Figurilla  for the respective audiences.

==Reception==

===Critical reception===
The Figurine was critically acclaimed. It was praised for its top-notch cinematography, props, and for being the movie to finally break the jinx of mediocrity in Nollywood and for leading the revolutionary birth of what has been termed New-Nollywood.          praised Afolayans story telling power and stated: "Kunle Afolayan’s The Figurine makes a strong point of seeking to change cinema culture in Nigeria and through this movie he wants viewers to think about the film - not just see it. Viewers of The Figurine: Araromire will leave the cinema neither happy nor sad. They will leave filled with questions, thus fulfilling what Afolayan set out to do: make us think".    Sakari Maatta of Elitisti, a   Commented "Afolayan deserves credit for a superb and almost flawless production. The musical score is well produced, the lighting does justice to many of Lola Maja-Okojevoh’s artistic creations, the special effects add to the magical feel and all in all, it is a great achievement not just for Nollywood but for the African cinematic industry as a whole. For raising the cinematographic bar to record heights and putting our culture in the limelight, The Figurine is one ‘reference’ movie that will be spoken about years to come".  Femi Owolabi gave it 3 out of 4 stars and states: "The film’s cultural relevance is so profound and its status in the social sphere is remarkable. The historical essence is highly applaudable too" "your eyes may never forgive you if you don’t get to see this exceptionally brilliant movie".  Ayo Stephens commented that "the major themes of fate and faith touched in this movie are very weighty ones and the writer does a very good job in leaving the audience to choose their sides. The Figurine is a must see, it would start up a discussion in you before you join the one going on around you".   

Nollywood Reinvented commended Kunle Afolayans show of ingenuity by producing, directing and starring in a single film, and the film still ends up turning out good: "Being able to direct, produce and then come and act was something I previously regarded as impossible for any movie but hey! Kunle just defies all laws of nature it seems."  She also commended Funlola Aofiyebis acting in the film stating: "Funlola as the comic relief; Amazing acting! It was never at one point over acting or underacting is what I loved the most about her performance."  Itua Ewoigbokhan commended the use of Yoruba and English languages interchangeably to give realistic feel to the film: "The Yoruba language spoken sometimes in the movie helped create a certain level of realism. The characters spoke Yoruba to themselves in private and reverted to the English language for more formal conversations as regular people would in real life. This level of detail would ensure a wider audience watches The Figurine". 

===Box office===
The Figurine was a success at the Nigerian box office. Due to the presence of few cinemas in the country as of 2009, the films total gross was approximated at around 30 million Naira,  after eight weeks of theatrical run.  However, the movie was able to recoup its investment through international screenings. The movie was also screened in rented halls in Nigerian cities and schools. 

===Awards===
The Figurine received ten nominations at the 6th Africa Movie Academy Awards and won five awards including the award for The Best Picture. It was the film with most wins at the ceremony. In October 2014, The Figurine topped Nolly Silver Screen Magazines list of "The 10 Best Nollywood Films Ever Made. 

====Complete list of awards====
{| class="wikitable"
|-
!  Award !! Year of Award!! Category !! Recipient/Nominee !! Result !! Notes !! Ref
|- 6th Africa Movie Academy Awards 2010
| Best Picture
| Kunle Afolayan, Ramsey Nouah
|  
|
| rowspan=10| 
|- Best Director
| Kunle Afolayan
|  
| lost to Shirley Frimpong-Manso for Perfect Picture
|- Best Performance by an Actor in a leading Role
| Ramsey Nouah
|  
|
|- Best Performance by an Actress in a leading Role
| Funlola Aofiyebi-Raimi
|  
| lost to Tapiwa Gwaza for Seasons of a life
|-
| Best Performance by a Child Actor
| Tobe Oboli
|  
| lost to Teddy Onyago and Bill Oloo for Togetherness Supreme
|- Heart of Africa
| Kunle Afolayan
|  
|
|-
| Achievement in Art Direction
| Pat Nebo
|  
| lost to Fulani
|- Achievement in Visual Effects
| Obiora Okafor
|  
|
|- Best Original Soundtrack
| Wale Waves, Eilam Hoffman
|  
| lost to A Sting in a Tale
|- Achievement in Cinematography
| Yinka Edward
|  
|
|- 6th Screen Screen Nation Awards 2011
| Favourite West African film
| Kunle Afolayan
|  
| lost to The Mirror Boy
| rowspan=2|   
|-
| Favourite West African Actor
| Ramsey Nouah
|  
| lost to Majid Michel
|}

===Film analysis and themes===
The story in The Figurine though seemed like a typical African tale, is in fact entirely fiction. The Village depicted, Araromire doesnt really exist (in tale, and in real life) and there is no goddess called "Araromire" in Nigeria.     The theme music; though most believe is in Yoruba language, it isnt, and the chant is totally meaningless.  The film has been described to be "characterized by a strong spiritual theme spiced with a bit of romance".     Sakari Maatta comments that the film reflects the number of everyday topics such as friendship, love and loyalty, role of the family and, ultimately, betrayal and death.  The lingering rhetoric question on the conclusion of the film remains: "is The Figurine a story of a love triangle, friendship, loyalty, betrayal? Or is it about the lingering power of curses, whether or not we believe in fate?"  

The goddess, Araromire has often been described as "neither good nor evil", since anyone who comes in contact with her will succeed for seven years then suffer for another seven years that follows it.  As it is seen at the beginning of the film, the priest who invoked the spirit of Araromire drowns in the river after seven years of coming in contact with her: "Araromire becomes wrathful, unleashing terror and destruction on whoever has her in custody. It is unfortunate that even the priest couldnt escape the adverse side of the deity", Femi Owolabi comments.  Itua Otaigbe Ewoigbokhan of Africa Magic describes the tale of Araromire as a modified version of Josephs interpretation of Pharaohs dream about Egypt from the Bible. 

In the present day of The Figurine world, Mona is still disturbed after getting to know about the folktale of the statue her husband has placed in their home;  Sakari Maatta interprets this to mean that even though such old myths and beliefs have not been given too much weight in the films society, they still live in the subconscious of the people.  Ayo Stephens cited that Nigerian culture is sold to the audience throughout the film, giving an example of the scene where Junior (Tobe Oboli) (aged 7) is instructed by her mother, Mona to prostrate and greet his father both in Yoruba and Urhobo language|Urhobo, which are respectively his fathers and mothers dialects. 

The conclusion of the film which is left undecided has been subject to different interpretations. Most two popular interpretations are: a first possible scenario which explains that maybe the Araromire is really a powerful and real goddess. Using the phrase "who the gods want to destroy they first make mad", Itua Otaigbe Ewoigbokhan explained that Femi may have been possessed by the goddess to get so irrationally in love with Mona, leading to rage which results in the doom of him and the other parties. This means Araromire uses Femis love as a tool to accomplish its mission. A second possible scenario explains that Araromire might really be a myth. Judging that Femi bribes officials to be posted to Araromire in order to be with Sola and Mona, its also possible he has placed the Araromire in the Shrine and cunningly leads Sola to the place during trek. This is a very possible scenario, since the folktale in fact says that the Araromire has been "destroyed" by the Villagers. A resolution to this scenario will render the loss of jobs, Solas kids accident as coincidence. And that Femis Asthma and eyesight mystery, and his fathers cancer mystery can be somehow explained scientifically.   However, many analysts hold the conclusion that Araromire is responsible for the occurrences in the film. It has been derived that even though Femi is the orchestrator of the events, theres a glaring dominance of the goddess’ powers.  

Anuli Agina of Pan-African University in his analysis of the film strongly believes that the film itself drives the viewers to believe in Araromire’s powers. He cited instances such as; the fear evoked by Femis father when Femi mentioned its name, the lecturers curiosity. It affirms that there are several strands in the storytelling that points to the presence of the goddess in the lives of the three friends. He mentioned instances such; sounds heard on the parade ground, which mysteriously led Sola and Femi closer to the Sculpture; the heavy rain when: the Villagers burnt Araromire’s shrine, the sculpture was discovered by the two friends and when both friends to returned it; the cure of Femis health problems and his father’s; the quick rise to success of Sola, without a change of his reckless attitude; the loss of Solas son, Monas miscarriage and so on. He concludes his analysis by stating: "With deliberate or inadvertent camera movements, the film compels the belief that Araromire the goddess is not only powerful, but also present in the lives of those who touch her. There is a conflict of opposing forces, but clearly, one is the more powerful or the film director chose to make it so. The only incident that discredits Araromire’s powers is that Femi does not have the woman of his dreams". He says even though Afolayan skillfully presents two options, he has made one very less plausible. 

==Home media== VOD platforms.   The film was reissued on 15 December 2014 in a special edition collection DVD tagged "Kunle Afolayan’s Collection". The DVD package also consists Irapada (2006) and Phone Swap (2012), which are Afolayans other films.   

==Legacy==

===Educational study===
The Figurine has often been used as a subject of study, assignments and educational subjects in institutions. Afolayan in an interview declared that he had personally granted over twenty interviews to students who were writing their thesis on The Figurine in their various schools. He has also been invited by school lecturer talk to students about the film. 

===Adaptation===
 
A book detailing the analysis of events in The Figurine, titled Auteuring Nollywood: Critical Perspectives on The Figurine was released on 31 July 2014.  The book is the first book in the history of Nigerian Cinema to be devoted to the work of a single Nigerian film director and it contains scholarly essays, which explores "the thematic focus and cinematic style employed in The Figurine".     It also contains interviews with the cast and crew of the film, insights into the African and Nigerian film industry and the trends in the New Nigerian Cinema.      The idea to make a book based on the film had been around even before the release of the film.  

Contributors to the 455-page book include; Sola Osofisan, Dele Layiwola, Chukwuma Okoye, Jane Thorburn, Matthew H. Brown, Gideon Tanimonure, A.G.A Bello, Foluke Ogunleye and Hyginus Ekwuazi. Foreword is written by Jonathan Haynes, Afterword by Onookome Okome and Adeshina Afolayan edited the book. 

The launch of the book had several notable political and film stakeholders in attendance,  together with some entertainment people.  Since its release, the book has been getting rave reviews from critics, educators and film scholars.   The director of the original film has also expressed how honoured and impressed he is about the book. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 