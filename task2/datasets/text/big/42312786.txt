Romeo Ranjha
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Romeo Ranjha
| image          = 
| caption        = Romeo Ranjha film poster
| director       = Navaniat Singh
| producer       = Gunbir Singh Sidhu & Manmord Sidhu
| writer         = Dheeraj Rattan 
| starring       = Jazzy B   Garry Sandhu    Monica Bedi   Parul Gulati   Rana Ranbir   Yograj Singh   Rana Jung Bahadur   Sangha Jagir
| music          = Jatinder Shah
| cinematography = 
| editing        = Manish More
| studio         = White Hill Productions & Basic Brothers Productions
| distributor    = Surya Basic Brothers
| released       =  
| runtime        = 
| country        = India
| language       = Punjabi
| budget         =  
| gross          = 
}} Indian action comedy film Starring Jazzy B & Garry Sandhu written by Dheeraj Ratan, directed by Navaniat Singh, who also directed Singh vs Kaur.  Romeo Ranjha is produced by Gunbir Singh Sidhu & Manmord Sidhu with Hansraj Railhan as Co-Producer. The movie was released on 16 May 2014. 

==Cast==

* Jazzy B as Romeo 
* Garry Sandhu as Ranjha
* Monica Bedi  
* Parul Gulati as Preet
* Rana Ranbir
* Yograj Singh 
* Rana Jung Bahadur
* Sangha Jagir

==Crew==

* Producers - Gunbir Singh Sidhu & Manmord Sidhu
* Co Produced By - Hansraj Railhan
* Director By Navaniat Singh
* Story & Screen Play - Dheeraj Ratan 
* Action Director.  Allan Amin
* DOP.  Harmeet Singh
* Music Director.  Jatinder Shah
* Choreographer.  Mehul Gadani
* Editor – Manish More
* Photographer - Vikram Kohli

==Awards==

PTC Punjabi Film Awards 2015

Lost
*Best Supporting Actress - Monica Bedi

==References==
 

==External links==
*  

 
 
 
 
 
 


 