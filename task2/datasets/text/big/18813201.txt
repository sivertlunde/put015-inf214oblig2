The Farm: Angola, USA
{{Infobox Film
| name           = The Farm: Angola, USA
| image          = Poster of the movie The Farm- Angola, USA.jpg
| image_size     = 
| caption        = 
| director       = Liz Garbus Wilbert Rideau Jonathan Stack
| producer       = Liz Garbus Jonathan Stack Bob Harris
| narrator       = Bernard Addison
| starring       = 
| music          = 
| cinematography = Sam Henriques Bob Perrin
| editing        = Mona Davis Mary Manhardt
| distributor    = 
| released       = 1998
| runtime        = 88 minutes
| country        =    English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Farm: Angola, USA is a 1998 award-winning documentary set in Americas infamous maximum security prison in Louisiana State Penitentiary|Angola, Louisiana. It was produced by Jonathan Stack and Liz Garbus and directed by Stack, Garbus, and Wilbert Rideau.
 Best Documentary Feature.   

A follow-up film,  , which examines the lives of the surviving prison inmates 10 years after the original film, was released in 2009.

{| class="wikitable"
|+ Inmates profiled
|-
! Inmate !! Year imprisoned !! Sentence !! Conviction
|-
| John A. Brown, Jr. || 1986 || Death || First-degree murder and robbery
|-
| George Crawford || 1997 || || First-degree murder
|-
| Wilbert Rideau || 1961 || || Murder and kidnapping
|-
| Vincent Simmons || 1977 || 100 years || Double rape
|-
| Eugene Bishop Tannehill || 1959 || Life || Murder
|-
| Logan Bones Theriot || 1960 || || Murder of wife
|-
| Ashanti Witherspoon || 1972 || 75 years || Armed robbery
|}
==Awards==
* Sundance Film Festival - Grand Jury Prize, 1998
* Academy Awards - Best Documentary Feature: Nominee, 1999
* Doubletake Documentary Film Festival - Audience Award, 1998
* Emmy Awards - 1999
**Outstanding Achievement in Non-Fiction Programming - Cinematography
**Outstanding Achievement in Non-Fiction Programming - Picture Editing
**Outstanding Achievement in Non-Fiction Programming - Sound Editing: Nominee
**Outstanding Non-Fiction Special: Nominee
* Santa Barbara International Film Festival - Best Documentary, 2000
* Florida Film Festival - Grand Jury Prize, 1998
* San Francisco Film Festival - Golden Gate Award, 1998
* New York Film Critics Circle - Best Non-Fiction Film, 1998
* National Society of Film Critics - Best Non-Fiction Film, 1999
* Los Angeles Film Critics Association - Best Documentary Film, 1999
* Taos Talking Pictures Film Festival - Taos Land Grant Award: Nominee, 1998
* Thurgood Marshall Award, 1999
* Satellite Awards - Best Documentary: Nominee - 1998

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 
 
 
 
 