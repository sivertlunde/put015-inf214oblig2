Sher Khan (1981 film)
 
 
{{Infobox film
| name           = Sher Khan
| image          = 
| alt            = 
| caption        = Younis Malik
| director       = Younis Malik 
| producer       = Anwar Kemal Pasha
| writer         = Nasir Adeeb Anjuman Mustafa Aliya Adeeb Bahar Talish Habib
| music          = Wajahat Attre
| cinematography = Ravi Yadav
| editing        = 
| studio         = Kemal Films
| distributor    = Kemal Films
| released       =August 2, 1981
| runtime        = 
| country        = Pakistan
| language       = Punjabi
| budget         = 
| gross          = 
}} Anjuman and Mustafa Qureshi. It is the highest grossing film in the history of Pakistani cinema. 

==Cast==
*Anjuman
*Sultan Rahi
*Mustafa Qureshi
*Aliya
*Nazli
*Bahar
*Nanha
*Adeeb
*Changezi
*Iqbal Durrani
*Altaf Khan
*Seema
*Ladla
*Talish
*Rehan
*Habib
*Iqbal Hassan

==Reception==
It created a record for a Punjabi film. When released it was the highest grossing film in the history of Pakistan. It ran for three and a half years in various cinemas. Anjuman got her breakthrough from this mega hit action and musical film. Anjuman-Sultan Rahi era started, they appeared in a record number of 117 films together. (Their last film was Madam Rani 1995). Iqbal Hassan performed extremely well in the title role and till his death was a major star due to the success of this film.

This film completed continuously 172 weeks at Naghma Cinema and 35 weeks at Mehfil cinema Lahore and combined 444 weeks in its first run. This film celebrated Solo Golden Jubilee in Rawalpindi and Multan and solo silver jubilees in Gujranwala, Sialkot, and Faislabad. This film celebrated Diamond Jubilee in Karachi and Golden Jubilee in Lahore at its second run. This film also celebrated Silver Jubilees in Lahore and Hydrabad at its third run. (Thanks to Mr. Waseem Khawar, Saudi Arabia)

==Soundtrack==
The music of the film is by famous musician Wajahat Attre. The lyrics are penned by Waris Ludhinavi and Khawaja Pervaiz. Madam Noor Jehan and musician Wajahat Attre became the box offices demand after three mega hit musical films in the same year, the others being Chan Veryam and Sala Sahib.

===Songs===
* Main vi badnaam Sayyan, too vi badnaam en... 	Madam Noor Jehan
* Chhad ke Mela tur chalia en, ujri Duniya haye... 	Masood Rana
* Jhanjhria pehna do, Bindia vi chamka do... 	Madam Noor Jehan
* Toon ain Mahi chhaila, te main aan teri Laila 	Madam Noor Jehan
* Main charhi Chubare ishq de aj laike tera naa 	Madam Noor Jehan
* Too je mere hamesha kol rahwen... 	Madam Noor Jehan

==References==
 

 
 
 

 