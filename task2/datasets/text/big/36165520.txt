The Big Bust Out
{{Infobox film
| name           = The Big Bust Out
| image          =
| image_size     =
| caption        =
| director       = Ernst R. von Theumer (as Richard Jackson)
| producer       = Sergio Garrone
| writer         = Sergio Garrone Ernst R. von Theumer
| based on       = Tony Kendall William Berger Gordon Mitchell Vonetta McGee
| music          = Elsio Mancuso Berto Pisano (as Burt Rexon)
| cinematography = Umberto Galeassi (as Robert Galeasi)
| editing        = Cesare Bianchini Barbara Pokras
| distributor    = New World Pictures (US)
| released       =  
| runtime        = 95 minutes (Italy) 75 minutes (US)
| country        = Italy West Germany English
| budget         =
| gross          =
}}
The Big Bust Out is the US title of an Italian women in prison film, The Crucified Girls of San Ramon (Lo Monaca... per tre Carogne e Sette Peccatrici). The US rights were bought by Roger Corman for New World Pictures, who cut out 20 minutes for the US release. Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 16 

Roger Corman later said the film was not very good but he managed to make money from it. 

==Synopsis==
It tells the story of seven beautiful women who escape from a high-security prison. After the breakout, Rebecca (Karen Carter) and her fellow escapees find they are in more trouble than they had imagined as they become the victim of a man involved in white slavery, Miller Drake (Gordon Mitchell). 
==Cast== Tony Kendall ... Jeff 
*Monica Teuber ... Sister Maria  William Berger... Bob Shaw 
*Gordon Mitchell ... El Kadir 
*Vonetta McGee... Nada 
*Christin Thorn ... Lolita (as Christiane Thorn) 
*Margaret Rose Keil ... Gail 
*Nuccia Cardinali ... Inga (as Karen Carter) 
*Linda Fox ... Claire (as Patrizia Barbot) 
*Felicita Fanny ... Carmen 
*Ivana Novak ... Liane 
*Herb Andress... Little Ivan 

==References==
 

==External links==
*  at IMDB
*  at Allmovie

 
 
 
 
 
 
 
 
 
 