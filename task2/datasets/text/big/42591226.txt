Cafundó (film)
{{Infobox film
| name           = Cafundó
| image          = Cafundó Film Poster.jpg
| caption        = Theatrical release poster
| director       = Paulo Betti   Clóvis Bueno
| producer       = Paulo Betti Virginia W. Moraes R.A. Gennaro
| writer         = Clóvis Bueno
| starring       = Lázaro Ramos   Leona Cavalli   Leandro Firmino da Hora   Beto Guiz    Ernani Moraes
| music          = André Abujamra
| cinematography = José Roberto Eliezer
| editing        = Sérgio Mekler
| studio         = Prole de Adão 
| distributor    = Laz Audiovisual
| released       =  
| runtime        = 102 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = R$185,707 
}}
Cafundó is 2005 Brazilian historical drama film written and directed by Paulo Betti and Clóvis Bueno and starring Lázaro Ramos. 

==Background==
Cafundó is a biopic of  , the source of João de Camargos original spiritual inspiration, located in todays Salto de Pirapora.

==Plot== possessed White white prostitute visions and Kardecist spiritism.

==Cast==
 
*Lázaro Ramos as  João de Camargo
*Leona Cavalli as Rosário
*Leandro Firmino da Hora as Cirino
*Alexandre Rodrigues as Natalino
*Ernani Moraes as Coronel João Justino
*Luís Melo as Monsignor João Soares
*Renato Consorte as Minister
*Francisco Cuoco as Bishop
*Abrahão Farc as Judge
 

==Production== Antonina were chosen as filming locations. 

==Reception==
The film won five gold Kikitos at the Gramado Film Festival, in the categories of best actor (Lázaro Ramos), best art direction, best cinematography and the special jury prize. It was also awarded as best film and best cinematography, at ParatyCine.   

==See also==
* Slavery in Brazil
* Umbanda

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 