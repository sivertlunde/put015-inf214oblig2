Soegija
 
 
 
{{Infobox film
| name = Soegija
|image=POSTERFILMSOEGIJA.jpg
|caption=Promotional poster
| director = Garin Nugroho
| producer = Y.I. Iswarahadi SJ, Djaduk Ferianto, Murti Hadi Wijayanto SJ, Tri Giovanni
| writer = Armantono, Garin Nugroho
| starring = Nirwan Dewanto, Annie Hertami, Wouter Zweers, Wouter Braaf, Nobuyuki Suzuki, Olga Lydia, Margono , Butet Kartaredjasa, Hengky Solaiman, Andrea Reva, Rukman Rosadi, Eko cockscomb
| music = Djaduk Ferianto
| cinematography = Garin Nugroho
| editing = Garin Nugroho
| studio = Studio Audio Visual Puskat Yogyakarta
| released =  
| runtime = 115 minutes
| country = Indonesia Indonesian  Javanese  Dutch  Japanese
| budget = Rp 12 Billion    - Surabaya Post  (about $1,270,000)
}} epic movie history drama national hero Albertus Soegijapranata. The film, starring actors with a diversity of cultural backgrounds, is launched in Indonesia on 7 June 2012. With a budget of around Rp 12 billion ($ 1,2 million), the film became the most expensive movie directed by Garin Nugroho so far.

The film was produced with the format that takes the story from the diary of the National Heroes character Monsignor|Mgr. Soegijapranata by taking the background Indonesian National Revolution and the establishment of the United States of Indonesia during the period 1940 - 1949. The film was directed by veteran director Garin Nugroho and shoot within the regional background of Yogyakarta and Semarang. Soegija isn’t a regular war movie with lots of fighting scenes, but describes the humanity behind the war. It shows how different people from Indonesia, Japan and the Netherlands, civil and military, were dealing with everyday life. Why those people made the choices they made, because of the different backgrounds they all came from.

==Synopsis==
Though raising the universal aspect of humanity rather than emphasize the religious aspects, this film is about   Society of Jesus|SJ, from his inauguration until the end of Indonesias independence war (1940–1949). This turbulent decade marked by the end of 350 years of Dutch occupation, entry and commencement of Japanese occupation of Indonesia, the Proclamation of Indonesian Independence, and the return of the Netherlands who tried to get Indonesia back as part of their Dutch Empire, which led to the Indonesian National Revolution. Soegija wrote all these events in his diary reflections, and also its participation in relieving the suffering of people in the midst of the chaos of war. He tried to play a role at all levels, local politics, national and international. (For his participation, President Sukarno awarded him with the title of National Hero of Indonesia.)
The film also shows the background and story of the Indonesian nurse Mariyem, Dutch soldier Robert, Dutch war photographer Hendrick and Japanese colonel Nobuzuki,  in their own struggle during one of the heaviest periods of Indonesian history.

==Cast==
* Nirwan Dewanto as Albertus Soegijapranata
* Andrea Reva as Lingling
* Annisa Hertami Kusumastuti as Mariyem
* Butet Kertarajasa as Koster Toegimin
* Eko cockscomb as Suwito
* Henky Solaiman as Linglings grandfather
* Landung Simatupang as Mr. Ward
* Margono as Pak Besut
* Marwoto as a Seller of Herbs
* Muhammad Abbe as Maryono
* Nobuyuki Suzuki as Nobuzuki
* Olga Lydia as Linglings mother
* Rukman Rosadi as Lantip
* Wouter Braaf as Hendrick
* Wouter Zweers as Robert

== References ==
 

==External links==
*  
*  

 

{{Persondata  
| NAME              = Garin Nugroho
| ALTERNATIVE NAMES = 
| SHORT DESCRIPTION = Indonesian director
| DATE OF BIRTH     = 6 June 1961
| PLACE OF BIRTH    = Yogyakarta (city)
| DATE OF DEATH     =
| PLACE OF DEATH    =
}}
 
 
 
 
 
 