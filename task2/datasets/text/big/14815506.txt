Zeder
{{Infobox film
| name           = Zeder
| image          = Zeder (DVD cover).jpg
| caption        = 
| director       = Pupi Avati
| producer       = {{plainlist|
* Antonio Avati
* Gianni Minervini
}}
| writer         = {{plainlist|
* Antonio Avati
* Pupi Avati
* Maurizio Costanzo
}}
| starring       = Gabriele Lavia
| music          = Riz Ortolani
| cinematography = Franco Delli Colli
| editing        = Amedeo Salfa
| distributor    = 20th Century-Fox (Italy)
| released       =  
| runtime        = 98 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
Zeder is a 1983 Italian horror film directed by Pupi Avati, starring Gabriele Lavia. The story is about a young novelists discovery of the writings of a late scientist who had found a means of reviving the dead. 

==Plot==
In 1956, Gabriella (Veronica Moriconi)—a girl with apparent psychic powers—is brought to the enormous house of Dr. Meyer (Cesare Barbetti) in Chartres, France. Meyer intends to conduct an experiment testing her abilities. He takes her into his basement, where the youngster abruptly falls to her knees and begins digging into the dirt. "This is where youre hiding, isnt it?" Meyer yells. He rushes upstairs for help from his assistants and leaves Gabriella by herself. She is attacked by something unseen and is taken to the hospital. In the basement, further digging reveals a rotted corpse, with a nearby wallet identifying the dead man as Paolo Zeder. Dr. Meyer realizes that the earth in which Zeder was buried was a K-Zone.

In present day Bologna, Stefano (Gabriele Lavia), a novelist, is given an old typewriter as a birthday present by his wife Alessandra (Anne Canovas). One night, after Alessandra has gone to bed, he discovers a series of typed letters on the typewriters ribbon. As he reads the ribbon, he finds it is an essay written by scientist Paolo Zeder discussing the existence of K-Zones—areas where death ceases to exist. According to Zeders essay, bodies buried in these zones can return from the dead. 

Stefano investigates the message left on the typewriter ribbon. He encounters people who make it clear that they do not appreciate any questions relating to Paolo Zeder or K-Zones. However, the resistance he experiences intrigues him even more. He becomes obsessed with getting answers to the mystery and temporarily abandons his wife. His investigation leads him to a huge old property, ostensibly abandoned but protected by electrified fences. A nearby service station attendant tells Stefano that French investors are building a gigantic hotel on the property, but no work is ever seen being done. 

Stefano sneaks onto the property, and discovers surveillance equipment with numerous monitors showing the dead face of a man buried in a coffin. Stefano watches the monitors. The dead man is Don Luigi Costa, an ex-priest who abandoned his vows after contracting an incurable disease. Continuing Zeders work, Costa had himself buried on the property, a suspected K-Zone, and his rebirth from the dead is caught on camera. Stefano manages to elude the conspirators involved in the experimental work, but finds his wife has been murdered by them. At night, Stefano buries her in a K-Zone located on the abandoned property. She revives and approaches her husband. In the darkness, Stefano begins screaming horribly.

==Cast==
* Gabriele Lavia as Stefano
* Anne Canovas as Alexandra
* Paola Tanziani as Gabriella Goodman
* Cesare Barbetti as Dr. Meyer
* Bob Tonelli as Mr. Big
* Ferdinando Orlandi as Giovine 
* Enea Ferrario as Mirko
* John Stacy as Professor Chesi
* Alessandro Partexano as Police Lt. Silvestre
* Marcello Tusco as Doctor Melis
* Aldo Sassi as Don Luigi Costa
* Veronica Moriconi as Young Gabriella
* Enrico Ardizzone as Benni
* Maria Teresa Tofano as Anna
* Andrea Montuschi as Inspector Bouffet
* Adolfo Belletti as Don Emilio
* Paolo Bacchi as Mr. Bigs Secretary
* Imelde Marani as Nurse

==Release==
The film was released theatrically in the United States by Motion Picture Marketing in 1984.  

In the mid-1980s, the film was briefly released on home video by Lightning Video in the U.S. under the title Revenge of the Dead.

The film was released on DVD in the United States by Image Entertainment.  This version is currently out of print.

==Reception==
Writing in The Zombie Movie Encyclopedia, academic  , said that the film "stands out a little by actually trying to generate suspense and not completely repulsing its audience". 

== External links ==
*  
*  

==References==
 

 
 
 
 
 
 
 
 
 
 