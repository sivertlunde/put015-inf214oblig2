Youth (2015 film)
{{Infobox film
| name           = Youth
| image          =Youth movie poster.jpg
| caption        =Italian poster
| director       = Paolo Sorrentino
| producer       = Nicola Giuliano Francesca Cima Carlotta Calori
| writer         = Paolo Sorrentino
| starring       = Michael Caine Harvey Keitel Paul Dano Rachel Weisz Jane Fonda David Lang
| cinematography = Luca Bigazzi
| distributor    =
| released       =  
| runtime        =
| country        = Italy
| language       = English
| budget         =
}}

Youth ( ) is an upcoming Italian film written and directed by Paolo Sorrentino. Starring Michael Caine, it is the directors second English-language film and the follow-up to his Academy Award-winning film The Great Beauty (2013).    The cast also includes Rachel Weisz, Paul Dano, Jane Fonda and Harvey Keitel. Youth is set to premiere at the 2015 Cannes Film Festival, where it will compete for the Palme dOr.   

==Plot==
A retired orchestra conductor is on holiday with his daughter and her friend in the Alps when he receives an invitation from Queen Elizabeth II to perform for Prince Philips birthday.

==Production==
Principle photography for the film began in Switzerland in May 2014.   Some scenes were also shot in Rome. 
 party anthem and unofficial national anthem, but it is not known whether this is referenced in the film. 

==References==
 

==External links==
* 

 

 
 
 
 
 

 