Lizzie (film)
{{Infobox film
| name           = Lizzie
| image          = Lizzie1957MovieAd.jpg
| caption        = Movie Ad
| writer         = Novel:  
| starring       = Eleanor Parker Richard Boone Joan Blondell
| director       = Hugo Haas Jerry Bresler
| music          = Leith Stevens
| cinematography = Paul Ivano
| editing        = Leon Barsha
| studio         = Bryna Productions
| distributor    = Metro-Goldwyn-Mayer
| released       = April 4, 1957
| runtime        = 81 minutes
| country        = United States English
| budget         =$361,000  .  gross = $555,000 
|}}
Lizzie is a 1957 drama film directed by Hugo Haas. The movie is based on the novel The Birds Nest by Shirley Jackson and stars Eleanor Parker, Richard Boone and Joan Blondell. The popular songs "Its Not for Me to Say" and "Warm and Tender" were written for this film, and performed by Johnny Mathis, who played a piano player/singer in the film. (Both songs were subsequently included in Mathis fifth album, Johnnys Greatest Hits). The film was produced by MGM Studios.

==Plot==
Elizabeth has recurring headaches and is plagued with  -like Lizzie, and the kind, well-adjusted Beth, the woman she always should have been. It is up to Dr. Wright to help Elizabeth to become Beth completely.

==Cast==
* Eleanor Parker &ndash; Elizabeth Richmond
* Richard Boone &ndash; Dr. Neal Wright
* Joan Blondell &ndash; Aunt Morgan
* Hugo Haas &ndash; Walter Brenner
* Ric Roman &ndash; Johnny Valenzo Dorothy Arnold &ndash; Elizabeths mother
* Marion Ross &ndash; Ruth Seaton
* Johnny Mathis &ndash; Piano Singer
==Reception==
According to MGM records the film earned $280,000 in the US and Canada and $275,000 elsewhere, resulting in a loss of $154,000. 
==References==
 
==External links==
* 

 
 
 
 
 
 
 
 
 
 