The Depraved (1957 film)
{{Infobox film
| name           = The Depraved 
| image          = "The_Depraved"_(1957).jpg
| image_size     =
| caption        =
| director       = Paul Dickson
| producer       = Edward J. Danziger Harry Lee Danziger
| writer         = Brian Clemens
| narrator       =
| starring       =Anne Heywood Robert Arden 
| music          = Albert Elms James Wilson	(as Jimmy Wilson)
| editing        = Vera Campbell
| studio         = 
| distributor    = 
| released       = 1957
| runtime        = 70 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
The Depraved is a 1957 British crime film.   

==Plot==
Laura is married to drunken, abusive Tom, and begins an affair with U.S. army officer, Dave. She persuades the Captain to help her murder her husband, contriving his death to look like the result of Toms drunk driving. However, a nosy policeman, Inspector Flynn, becomes suspicious, and soon the lovers crafty scheme becomes their own nightmare.

==Cast==
*Laura Wilton - Anne Heywood	
*Dave Dillon - Robert Arden	
*Major Kellaway - Carroll Levis	
*Tom Wilton - Basil Dignam	
*Inspector Flynn - Denis Shaw	 Robert Ayres	
*Kaufmann - Garry Thorne (as Gary Thorne)	
*Barman - Hal Osmond	
*Sergeant U.S. Army - Gil Winfield

==Critical reception==
TV Guide wrote, "the title promises something more lurid than another B murder-mystery with an American leading man, but thats all this is."  

==References==
 

==External links==
*  at IMDB

 
 
 
 


 