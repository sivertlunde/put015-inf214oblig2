Lonesome Luke, Messenger
 
{{Infobox film
| name           = Lonesome Luke, Messenger
| image          =
| image size     =
| caption        =
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| narrator       =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English English intertitles
| budget         =
}}
 1917 short short comedy film starring Harold Lloyd.   

==Cast==
* Harold Lloyd - Lonesome Luke
* Bebe Daniels
* Snub Pollard
* Gilbert Pratt
* Gus Leonard
* Fred C. Newmeyer
* Billy Fay
* Nina Speight
* Bud Jamison Charles Stevenson
* Dorothea Wolbert
* May Ballard - (as Mabel Ballard)
* Evelyn Page
* W.L. Adams
* Sammy Brooks

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 