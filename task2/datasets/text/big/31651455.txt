Our Russian Front
{{Infobox film
| name           = Our Russian Front
| image          = Our Russian Friont 1942 lobby card.jpg
| alt            = 
| caption        = Lobby card
| director       = Joris Ivens Lewis Milestone
| producer       = Joris Ivens Lewis Milestone
| writer         = Elliot Paul
| narrator       = Walter Huston
| starring       = 
| music          = 
| cinematography = Roman Karmen Ivan Belyakov Dmitri Rymarev Arkadi Shafran Mark Troyanovsky Vladimir Yeshurin
| editing        = Marcel Craven Albert Nalpas Russian War Relief Inc. Artkino Pictures
| released       =  
| runtime        = 45 minutes
| country        =  
| language       = English
| budget         = 
| gross          =
}}

Our Russian Front is a 1942 American documentary film directed by Joris Ivens and Lewis Milestone, and narrated by Walter Huston to promote support for the Soviet Unions war effort.  {{cite book
|last=Joseph R. Millichap
|title=Lewis Milestone
|publisher=Twayne Publishers
|year=1981
|series=Twaynes filmmakers series
|pages=108, 109, 115
|isbn=0-8057-9281-3
|url=http://books.google.com/?id=sy8eAAAAMAAJ&dq=%22Our+Russian+Front%22%2C+1942+film&q=%22Our+Russian+Front%22}}  {{cite book
|last=Michael J. Strada, Harold R. Troper
|title=Friend or foe?: Russians in American film and foreign policy, 1933-1991
|publisher=Scarecrow Press
|year=1997
|pages=46
|isbn=0-8108-3245-3}} 

==Film==
In production before America entered World War II, the film was completed several weeks after the Japanese attack on Pearl Harbor, having gone through frantic last minute updates to ensure it meeting its February 1942 release date. {{cite book
|last=Eugene P. Walz
|title=Flashback: people and institutions in Canadian film history
|publisher=Mediatexte Publications
|year=1986
|series=Volume 2 of Canadian film studies
|pages=38, 40, 52
|isbn=0-9691771-1-9
|url=http://books.google.com/?id=Xo0aAQAAIAAJ&dq=%22Our+Russian+Front%22%2C+1942+film&q=%22Our+Russian+Front%22}}   Joris Ivens anticipated that editing might take a week, but stated that Hollywood   "fiddled with it for two months and unrecognizably altered the original version." {{cite book
|last=Hans Schoots
|title=Living dangerously: a biography of Joris Ivens
|publisher=Amsterdam University Press
|year=2000
|pages=168
|isbn=90-5356-433-0
|url=http://books.google.com/?id=JlcfbzDx9HEC&pg=PA168&dq=%22Our+Russian+Front%22,+1942+film#v=onepage&q=%22Our%20Russian%20Front%22%2C%201942%20film&f=false}}   Walter Huston narrates a World War II documentary intended to bolster United States support for the USSRs war efforts.  Created using front line footage taken by Russian battlefield cameramen, {{cite book
|last=Robert Mann
|title=Complete idiots guide to the Cold War |publisher=Penguin
|location=Russian Films
|year=2002
|pages=148
|isbn=0-02-864246-5
|url=http://books.google.com/?id=TeSwjppr4zcC&pg=PA148&dq=%22Our+Russian+Front%22,+1942+film#v=onepage&q=%22Our%20Russian%20Front%22%2C%201942%20film&f=false}}  and archive footage of Averell Harriman, Joseph Stalin, and Semyon Timoshenko, the film was edited in the US.  Upon release, the film screened for more than 20 hours a day and broke all previous box office records at the Rialto Theater in Times Square. 

==Reception==
The New York Times reports that "the greatest battle in history" was assembled by Lewis Milestone and Joris Ivens into a "tersely contemporary document".  They note that it did not rank favorably when compared to "great documentaries" because its commentary was uninspired, and it attempted to crowd too much within a timeframe of 40 minutes, resulting in it being only "a synoptic account of the Russian war effort".  They granted that as a record of the Russian peoples struggle, "it is a heartening account," and communicated the "urgency of this urgent moment."  The reviewer noted that the film was a picture of total war, from the bayonets and shells of the front lines, to the efforts of the peasants and laborers and scientists struggling to support the war effort, sharing that in such circumstances, there are "no noncombatants in this war." {{cite news
|url=http://movies.nytimes.com/movie/review?res=9C04EFDE1439E33BBC4A52DFB4668389659EDE
|title=review: Our Russian Front (1941)
|last=T.S.
|date=February 12, 1942
|work=The New York Times
|accessdate=2 May 2011}} 
 Artkino Pictures Russia Relief Organization, and The Battle of Russia, directed in 1943 by Anatole Litvak and Frank Capra as part of the Why We Fight series. {{cite book
|last=Harlow Robinson
|title=Russians in Hollywood, Hollywoods Russians: biography of an image
|publisher=UPNE
|year=2007
|pages=116, 117, 118
|isbn=1-55553-686-7
|url=http://books.google.com/?id=i2hxJrlSE0AC&pg=PA116&dq=%22Our+Russian+Front%22,+1942+film#v=onepage&q=%22Our%20Russian%20Front%22&f=false}} 

==References==
 

==External links==
*   at the Internet Movie Database

 
 

 
 
 
 
 
 
 
 
 
 