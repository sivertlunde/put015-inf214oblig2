Banjaran
 
{{Infobox film
| name           = Banjaran
| image          = Banjaran.jpg
| caption        = DVD Cover
| director       = Harmesh Malhotra
| producer       = Om Prakash Mittal, Ram Singh Pran Kulbhushan Kharbanda Gulshan Grover Anjana Mumtaz
| music          = Laxmikant-Pyarelal
| released       = 8 November 1991
| country        = India
| language       = Hindi
| Lyricist       = Anand Bakshi
}}

Banjaran (बंजारन) is a Bollywood films of 1991 Indian Bollywood Hindi-language film. It was directed by Harmesh Malhotra. Rishi Kapoor, and Sridevi in the main lead roles and had Pran (actor)|Pran, Kulbhushan Kharbanda and Gulshan Grover in supporting roles. Laxmikant Pyarelal were the music directors. This film was released on 8 November 1991.

==Cast==
*Rishi Kapoor .... Kumar Singh Sesodia / Suraj
*Sridevi .... Reshma / Devi
*Kulbhushan Kharbanda .... Rana Udaybhan Singh Sesodia
*Gulshan Grover ... Shakti Singh Pran .... Thakur Baba Neelam ....Neha Singh
*Raza Murad .... Thakur Ranjit Singh
*Sudhir Pandey .... Sardar Malik
*Anjana Mumtaz .... Mrs. Singh Sesodia
*Viju Khote .... Mrs. Singh Sesodia
*Sharat Saxena ....Bujangji Maharaj
*Rakesh Bedi ....Sarju
*Pankaj Dheer ....Girja
*Mahavir Shah ....Thakur Mahavir Singh
*Ram Mohan 
*Renu Arya 
*Madhu Malhotra
*Rajan Haksar 
*Birbal 
*Renu Arya 
*Madhu Malhotra

==Soundtrack==

* Badli Hai Na Badlegi - Lata Mangeshkar
* Desh Badalte Hain - Anuradha Paudwal, Mohd. Aziz & Sukhwinder Singh
* Mere Dil Ki Galiyon Mein - Alka Yagnik & Suresh Wadkar
* Tere Mere Pyar Ki - Anuradha Paudwal and Mohd Aziz
* Teri Banjaran - Alka Yagnik
* Ye Jeevan Jitni Bar Mile -  Alka Yagnik & Mohd. Aziz

 
 
 
 


 