Freaks
 
 
{{Infobox film
| name = Freaks
| image = FreaksPoster.jpg
| alt = 
| caption = Theatrical release poster
| director = Tod Browning
| producer = Tod Browning
| screenplay = Tod Robbins
| based on =  
| starring = Wallace Ford Leila Hyams Olga Baclanova Roscoe Ates
| cinematography = Merritt B. Gerstad
| editing = Basil Wrangell
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 90 minutes   64 minutes    
| country = United States
| language = English German French
| budget = $316,000 Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p 152 
}} carnival sideshow performers and had real deformities. The original version was considered too shocking to be released, and no longer exists. Directed and produced by Tod Browning, whose career never recovered from it, Freaks has been described as standing alone in a subgenre of one.    

At 16, Browning had left his well-to-do family to join a traveling circus; he drew on his personal experiences for Freaks. Because of his success as the director of Dracula (1931 English-language film)|Dracula, he was given a considerable leeway for a major studios first horror film; this and the fact he was working in Pre-Code Hollywood enabled a unique production. In the film, the physically deformed "freaks" are inherently trusting and honorable people, while the real monsters are two of the "normal" members of the circus who conspire to murder one of the performers to obtain his large inheritance.

==Plot==
The film opens with a sideshow barker drawing customers to visit the sideshow. A woman looks into a box to view a hidden occupant and screams. The barker explains that the horror in the box was once a beautiful and talented trapeze artist. The central story is of this conniving trapeze artist Cleopatra, who seduces and marries sideshow midget Hans after learning of his large inheritance. Cleopatra conspires with circus strongman Hercules to kill Hans and inherit his wealth. At their wedding reception, Cleopatra begins poisoning Hans wine. Oblivious, the other "freaks" announce that they accept Cleopatra in spite of her being a "normal" outsider; they hold an initiation ceremony in which they pass a massive goblet of wine around the table while chanting, "We accept her, we accept her. One of us, one of us. Gooba-gobble, gooba-gobble". The ceremony frightens the drunken Cleopatra, who accidentally reveals that she has been having an affair with Hercules. She mocks the freaks, tosses the wine in their faces, and drives them away. The humiliated Hans realizes that hes been played for a fool and rejects Cleopatras attempts to apologize, but then he falls ill from the poison.
 castrating him; tarred and feathered. She is the opening scenes cause for alarm.
 happier ending, Hans is living a millionaires life in a mansion. Venus and her clown boyfriend Phroso visit, bringing Frieda, to whom Hans had been engaged before meeting Cleopatra. Hans refuses to see them, but they force their way past his servant. Frieda assures Hans that she knows he tried to stop the others from exacting revenge. Phroso and Venus leave, and Frieda comforts Hans when he starts to cry.

===Subplots===
Spliced throughout the main narrative are a variety of "slice of life" segments detailing the lives of the sideshow performers. Bearded Woman, The Stork Woman.
* Violet, a  . 
* In the middle of a conversation, The Human Torso lights his own cigarette, using only his mouth. In the original scene, he also rolls the cigarette. Frances OConnor, the armless wonder, demonstrates how she performs everyday activities using her feet, in the place of arms

==Cast==
 
* Wallace Ford as Phroso
* Leila Hyams as Venus
* Olga Baclanova as Cleopatra
* Roscoe Ates as Roscoe (as Rosco Ates)
* Henry Victor as Hercules Harry Earles as Hans Daisy Earles as Frieda
* Rose Dione as Madame Tetrallini
* Daisy and Violet Hilton as the Siamese twins
* Schlitzie as himself Half Woman-Half Man
* Johnny Eck as Half Boy Frances OConnor Armless girl Peter Robinson as Human skeleton Olga Roderick as Bearded lady Koo Koo as herself
* Prince Randian as The Living Torso
* Martha Morris as Angelenos armless wife
* Elvira Snow as Pinhead Pip
* Jenny Lee Snow as Pinhead Zip Elizabeth Green as Bird Girl Sword Swallower
* Angelo Rossitto as Angeleno
* Edward Brophy and Matt McHugh as  the Rollo Brothers.
 

==Production==
 
MGM had purchased the rights to Robbins short story, Spurs, in the 1920s at Brownings urging. In June 1931, MGM production supervisor, Irving Thalberg, offered Browning the opportunity to direct Arsène Lupin (1932 film)|Arsène Lupin with John Barrymore. Browning declined, preferring to develop Freaks, a project he had started as early as 1927. Screenwriters Willis Goldbeck and Elliott Clawson were assigned to the project at Brownings request. Leon Gordon, Edgar Allan Woolf, Al Boasberg and an uncredited Charles MacArthur would also contribute to the script. The script was shaped over five months. Little of the original story was retained beyond the marriage between a midget and an average-sized woman and their wedding feast. Victor McLaglen was considered for the role of Hercules, whilst Myrna Loy was initially slated to star as Cleopatra, with Jean Harlow as Venus. Ultimately, Thalberg decided not to cast any major stars in the picture.
 Peter Robinson Olga Roderick Frances OConnor microcephalics who appear in the film (and are referred to as "pinheads") were Zip and Pip (Elvira and Jenny Lee Snow) and Schlitzie, a male named Simon Metz who wore a dress mainly due to Urinary incontinence|incontinence, a disputed claim. Also featured were the intersex Josephine Joseph, with her left-right divided gender; Johnny Eck, the legless man; the completely limbless Prince Randian (also known as The Human Torso, and mis-credited as "Rardion"); Elizabeth Green the Stork Woman; and Koo-Koo the Bird Girl, who had Virchow-Seckel syndrome or bird-headed dwarfism, and is most remembered for the scene wherein she dances on the table.
 carnival barker premiere at Fox Criterion in Los Angeles on February 20, 1932. 

==Reception== Lon Chaney and for directing Bela Lugosi in Dracula (1931 English-language film)|Dracula (1931), had trouble finding work afterward, and this effectively brought his career to an early close. Because the film was thought to be overly exploitative, it was banned in the United Kingdom for 30 years. 

A number of contemporary reviews were not only highly critical of the film, but expressed outrage and revulsion. Harrisons Reports wrote that "Any one who considers this entertainment should be placed in the pathological ward in some hospital."    In The Kansas City Star, John C. Moffitt wrote, "There is no excuse for this picture. It took a weak mind to produce it and it takes a strong stomach to look at it."    The Hollywood Reporter called it an "outrageous onslaught upon the feelings, the senses, the brains and the stomachs of an audience." 

Variety (magazine)|Variety also published a negative review, writing that the film was "sumptuously produced, admirably directed, and no cost was spared, but Metro heads failed to realize that even with a different sort of offering the story is still important. Here the story is not sufficiently strong to get and hold the interest, partly because interest cannot easily be gained for too fantastic a romance." The review went on to state that the story "does not thrill and at the same time does not please, since it is impossible for the normal man or woman to sympathize with the aspiring midget. And only in such a case will the story appeal." 

Not all reviews were as harsh. The New York Times called it "excellent at times and horrible, in the strict meaning of the word, at others" as well as "a picture not to be easily forgotten."  The New York Herald Tribune wrote that it was "obviously an unhealthy and generally disagreeable work," but that "in some strange way, the picture is not only exciting, but even occasionally touching." 

 , pictures of them might as well be shown in the Rialto Theatre (New York City)|Rialto. They may hereafter even be regarded in the flesh with a new dread bordering on respect." 
 Bravo TVs list of the 100 Scariest Movie Moments.
 film review aggregator website Rotten Tomatoes, Freaks holds a 93% "fresh" rating based on 45 reviews; the general consensus states: "Time has been kind to this horror legend: Freaks manages to frighten, shock, and even touch viewers in ways that contemporary viewers missed." 

;American Film Institute lists
* AFIs 100 Years...100 Movies - nominated
* AFIs 100 Years...100 Thrills - nominated
* AFIs 100 Years...100 Movie Quotes:
** "Gooba-gobble, gooba-gobble. We accept her. One of us, one of us." - nominated

==Cultural influence== freaks versus hippies.
* The Ramones song "Pinhead (Song)|Pinhead" was inspired by a screening of the film by the band.
* In the final episode of  , two characters made to resemble Pip and Zip enter the QuickStop and interact with Dante and Randal. 25th season of The Simpsons, "Treehouse of Horror XXIV".
* South Park episode "Butters Very Own Episode" spoofed the film. The Dreamers, The Player, The Wolf of Wall Street also reference the famous "One of us" chant from the film.

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 