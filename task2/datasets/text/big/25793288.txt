Sherlock Holmes: A Game of Shadows
 
 
 
{{Infobox film
| name = Sherlock Holmes: A Game of Shadows
| image = Sherlock Holmes2Poster.jpg
| image_size = 215px
| alt =  
| caption = Theatrical release poster
| director = Guy Ritchie Lionel Wigram Susan Downey Dan Lin
| writer = Michele Mulroney Kieran Mulroney
| based on =  
| starring = Robert Downey, Jr. Jude Law Noomi Rapace Jared Harris Rachel McAdams  Eddie Marsan
| music = Hans Zimmer
| cinematography = {{plainlist|
* Philippe Rousselot
* Gavin Free }}
| editing = James Herbert
| studio = Village Roadshow Pictures Silver Pictures Wigram Productions Warner Bros. Pictures 
| released =  
| runtime = 129 minutes  
| country = United Kingdom United States
| language = English
| budget = $125 million 
| gross = $545.4 million   Box Office Mojo 
}}
 action mystery Lionel Wigram, Sherlock Holmes, based on the titular character created by Sir Arthur Conan Doyle. The screenplay was written by Michele Mulroney and Kieran Mulroney. Robert Downey, Jr. and Jude Law reprise their roles as Sherlock Holmes and Dr. Watson|Dr. John Watson and were joined by Noomi Rapace as Simza and Jared Harris as Professor Moriarty.

Holmes and Watson join forces to outwit and bring down their most cunning adversary, Professor James Moriarty. Although influenced by Doyles short story "The Final Problem", the film follows an original story and is not a strict adaptation. 

On its release the film, received generally positive reviews from critics,  and was commercially successful with a worldwide gross of over $545 million. 

==Plot==
In 1891, Irene Adler delivers a package to Herr Dr. Hoffmanstahl; payment for a letter he was to deliver. Hoffmanstahl opens the package, triggering a hidden bomb that is prevented from detonating by the intervention of Sherlock Holmes. Holmes takes the letter while Adler and Hoffmanstahl escape. Holmes finds Hoffmanstahl assassinated moments later. Adler meets with Professor Moriarty to explain the events, but Moriarty poisons and kills her — deeming her position compromised by her love for Holmes.
 Mycroft at Watsons bachelor party, Holmes meets with Gypsy fortune-teller Simza, the intended recipient of the letter he took from Adler, sent by her brother Rene. Holmes defeats an assassin sent to kill Simza, but she flees before Holmes can interrogate her. After the wedding of Watson and Mary Morstan, Holmes meets Moriarty for the first time. Expressing his respect for Holmes tenacity, Moriarty informs Holmes that he murdered Adler and will kill Watson and Mary if Holmes interference continues. Holmes vows to bring him down.

Moriartys men attack Watson and Mary on a train to their honeymoon. Holmes, having followed the pair to protect them, throws Mary from the train into a river below where she is rescued by Mycroft. After defeating Moriartys men, Holmes and Watson travel to Paris to locate Simza. When she is found, Holmes tells Simza that she has been targeted because Rene is working for Moriarty, and may have told her about his plans. Simza takes the pair to the headquarters of an anarchist group to which she and Rene had formerly belonged. They learn that the anarchists have been forced to plant bombs for Moriarty.

The trio follows Holmes deduction that the bomb is in the Paris Opera. However, Holmes realizes too late that he has been tricked and that the bomb is in a nearby hotel; the bomb kills a number of assembled businessmen. Holmes discovers that the bomb was a cover for the assassination of Alfred Meinhard, one of the attendees, by Moriartys henchman, Sebastian Moran. Meinhards death grants Moriarty ownership of Meinhards arms factory in Germany. Holmes, Watson and Simza travel there, following clues in Renes letters.

At the factory, Moriarty captures, interrogates and tortures Holmes while Watson battles Moran. Holmes spells out Moriartys plot, revealing that the Professor secretly acquired shares in multiple war profiteering companies, and intends to instigate a world war to make himself a fortune. Meanwhile, Watson uses the cannon he had been hiding behind to destroy the watchtower in which Moran is concealed. The structure collapses into the warehouse where Moriarty is holding Holmes captive. Watson, Simza, and an injured Holmes reunite and escape aboard a moving train. Holmes deduces that Moriartys final target will be a peace summit in Switzerland, creating an international incident.

At the summit, Holmes reveals that Rene is the assassin and that he is disguised as one of the ambassadors, having been given radical reconstructive surgery by Hoffmanstahl. Holmes and Moriarty, who is also in attendance, retreat outside to discuss their competing plans over a game of chess. Watson and Simza find Rene and stop his assassination attempt, but Rene is killed by Moran.

Outside, Holmes reveals that he previously replaced Moriartys personal diary that contained all his plans and financing with a duplicate. The original was sent to Mary in London, who decrypted the code using a book that Holmes had noticed in Moriartys office during their first meeting. Mary passes the information to Inspector Lestrade who seizes Moriartys assets, financially crippling him. Holmes and Moriarty anticipate an impending physical confrontation, and both realise Moriarty will win due to Holmes injured shoulder. Holmes instead grapples Moriarty and forces them both over the balcony and into the Reichenbach Falls below. The pair are presumed dead.

Following Holmes funeral, Watson and Mary prepare to have their belated honeymoon when Watson receives a package containing a breathing device of Mycrofts that Holmes had noticed before the summit. Contemplating that Holmes may still be alive, Watson leaves his office to find the delivery man. Holmes, having concealed himself in Watsons office, reads Watsons memoirs on the typewriter and adds a question mark after the words "The End". 

==Cast==
 
* Robert Downey, Jr. as Sherlock Holmes   
* Jude Law as Dr. Watson|Dr. John Watson  Professor James Moriarty    
* Noomi Rapace as Madame Simza Heron {{cite news |url=http://spinoff.comicbookresources.com/2010/11/23/a-first-look-at-sherlock-holmes-2-plus-photos-from-six-other-wb-films |title=Noomi Rapace to play Sim the Gypsy in ‘Sherlock Holmes 2′ |last=Melrose
|first=Kevin |work=Spin Off Online |date=23 November 2010 |accessdate=28 November 2010}} 
* Stephen Fry as Mycroft Holmes  
* Kelly Reilly as Mary Watson (née Morstan)   
* Rachel McAdams as Irene Adler   
* Eddie Marsan as Inspector Lestrade  Paul Anderson as Sebastian Moran 
* Geraldine James as Mrs. Hudson 
* Thierry Neuvic as Claude Ravache
* Fatima Adoum  as a Manouche
* Wolf Kahler as Herr Dr. Hoffmanstahl
* Affif Ben Badra as Tamas Morato
 

==Production== Sherlock Holmes, a sequel was fast-tracked by Warner Bros. with director Guy Ritchie dropping out of an adaptation of Lobo (DC Comics)|Lobo and Robert Downey, Jr. leaving Cowboys & Aliens.    It was unclear if Rachel McAdams would appear in the film. McAdams said, "If I do, it wont be a very big thing. Its not a lead part".  Warner Bros. later confirmed to Entertainment Weekly that McAdams would play a part in the sequel. 

The film, then under the running title of Sherlock Holmes 2, was reported to be influenced by Conan Doyles "The Final Problem".    While the film took place a year after the events of the first film,  Sherlock Holmes: A Game of Shadows was intended to be a stand-alone film that did not require knowledge of the previous movie. 
 PS Waverley Victoria Bridge, Royal Naval College. 

In early February 2011, principal photography moved for two days to Strasbourg, France. Shooting took place on, around, and inside Strasbourg Cathedral. The scene was said to be the opening scene of the film, as it covered an assassination/bombing in a German-speaking town. 
 The Historic Dockyard Chatham. The White Cliffs of Dover can also be seen in the background of the scene where Watson and Sherlock travel by boat to France. 

The slow-motion work in the film was done by Gavin Free, an English filmmaker who works for Rooster Teeth Productions and is known for his web series, The Slow Mo Guys.

===Music===
 
The score was composed by  , Wolfgang Amadeus Mozart, Ennio Morricone and Franz Schubert.

==Distribution==
The film was released on 16 December 2011 in Canada, Russia, the United States, the United Kingdom, and Mexico; on 25 December 2011 in most other countries; and on 5 January 2012 in Australia, Poland and Spain. 

===Home media===
Sherlock Holmes: A Game of Shadows was released on DVD and Blu-ray on 12 June 2012 for Region 1   and 14 May 2012 for Region 2   and Region 4. 

==Reception==
  in January 2012 at the films French premiere in Paris.]]

===Box office===
Sherlock Holmes: A Game of Shadows earned $186,848,418 in North America as well as $357,000,000 in other territories for a worldwide total of $543,848,418.  It is the 12th highest-grossing film of 2011 worldwide. 

In North America, it topped the box office on its opening day with $14.6 million,     down from the opening-day gross of the previous film ($24.6 million).  During the weekend, it grossed $39.6 million, leading the box office but earning much less than the opening weekend of its predecessor ($62.3 million).    By the end of its theatrical run, it became the 9th highest-grossing film of 2011 in the US. 

Outside North America, the film earned $14.6 million on its opening weekend, finishing in third place.  It topped the overseas box office during three consecutive weekends in January 2012.    It eventually surpassed its predecessors foreign total ($315.0 million). In the UK, Ireland and Malta, its highest-grossing market after North America, the film achieved a first-place opening of £3.83 million  ($5.95 million),  over a three-day period, compared to the £3.08 million earned in two days by the original film.  It earned $42.2 million in total. Following in largest totals were Russia and the CIS ($28.4 million) and Italy ($24.5 million). 

===Critical response===
The film received moderately positive reviews from critics. Review aggregator  , which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 48 based on 38 reviews. 

  felt that the film "aims lower than its predecessors modest ambition, and still misses the mark." 

===Accolades===
{| class="wikitable sortable" width="99%"
|- style="background:#ccc; text-align:center;"
! colspan="6" style="background: LightSteelBlue;" | List of awards and nominations
|- style="background:#ccc; text-align:center;"
! Year
! Award
! Category
! Recipient(s) and nominee(s)
! Result
! class="unsortable" |  }}
|-
|rowspan="5"| 2012 Saturn Awards Best Action/Adventure Film
| Sherlock Holmes: A Game of Shadows
| 
| rowspan="2" style="text-align:center;"|   
|- Best Costume
| Jenny Beavan
| 
|- Teen Choice Awards
|  
| Sherlock Holmes: A Game of Shadows
| 
| rowspan="3" style="text-align:center;"|   
|-
|  
| Robert Downey, Jr.
| 
|-
|  
| Noomi Rapace
| 
|}

==Sequel==
Warner Bros. Pictures announced in October 2011 that the first draft for Sherlock Holmes 3 is being produced with screenwriter Drew Pearce
writing the script;    he was replaced by Justin Haythe.  Jude Law commented on the project in late 2013, "We had a meeting earlier this year, the three of us, and I think its being written now. Warner Bros. have still got to agree to pay for it... I think they want to!"  He also said of the delay in getting into production, "I think Warner Bros. wants it, and theres a lot of want from us as a team. We want it to be better than the other two. We want to make sure its smarter and cleverer, but in the same realm. Its a slow process. Were all busy. So getting us together to try to nail that has taken a little bit longer than we had hoped... I hate celebrating anything Ive done, but Im so proud of those films. I think it was Joel Silver who said, Take it out of that dusty room and put it on the street." 

In October 2014, Susan Downey stated that a third film was in development: "Theres an idea, theres an outline, there is not a script yet. Trust me, the studio would love there to be a script. But our feeling is, we gotta get it right." When asked whether the film would realistically be out within the "next few years", she expressed confidence that it would be, saying, "Yeah. At a certain point its going to be too long— weve waited too long. Were working as fast and responsibly as we can to get a great script." 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 