In the Dark Half
{{Infobox film
| image          = 
| alt            = 
| caption        = 
| director       = Alastair Siddons
| producer       = Margaret Matheson
| screenplay     = Lucy Catherine
| starring       = {{plainlist|
* Tony Curran
* Lyndsey Marshal
* Jessica Barden
}} Dan Jones
| cinematography = Neus Ollé-Soronellas 
| editing        = Paul Carlin
| studio         = {{plainlist|
* BBC Films
* Cinema Six
* Matador Pictures
* Regent Capital
}}
| distributor    = {{plainlist|
* Verve Pictures
* ContentFilm International
}}
| released       =   |df=y}}
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = $464,000 (£300,000)   
| gross          = 
}}
In the Dark Half is a 2012 British drama film directed by Alastair Siddons and starring Tony Curran, Lyndsey Marshal, and Jessica Barden.  Barden plays a teenaged girl who becomes obsessed with her neighbor, played by Curran, and his grief over losing his son.  It received mixed reviews.

== Plot summary ==
Marie, a 15-year-old girl, lives with Kathy, her mother, and occasionally babysits for her next door neighbor, Filthy, a poacher who is widely rumored to have murdered his wife.  Marie and Kathy have been drifting apart, and there are unresolved issues between them.  Kathys behavior has been erratic, and Marie believes that her mother plans to leave her.  When Filthys young son, Sean, spontaneously dies of apparently natural causes while Marie is babysitting him, Marie becomes obsessed with her neighbor and begins to think that his stories about spirits in the hills may be true.  Filthy, devastated by the loss of his son, reacts furiously when Marie loots his traps and breaks into his house to steal Seans favorite toy.  When Marie states that she needs these items in order to appease the spirits and contact his dead son, Filthy initially dismisses the myths as idle stories he told Sean.  However, Filthy eventually comes to believe Marie and commits suicide to be with son, whom he believes to be lonely.  Before he dies, Filthy urges Marie to return to her own father, and Marie realises that she has repressed her mothers suicide; instead of living with her mother, she has been working through her grief and denial while ignoring her father.  Having come to terms with this loss, Marie reunites with her father.

== Cast ==
* Jessica Barden as Marie
* Alfie Hepper as Sean
* Tony Curran as Filthy
* Lyndsey Marshal as Kathy
* Georgia Henshaw as Michelle
* Simon Armstrong as Steve

== Production ==
In the Dark Half was shot in Bristol, England.  Producer Matheson was drawn to the script because of the quality and opportunity to work in a new genre, and director Siddons was inspired by European horror films. 

== Release ==
In the Dark Half premiered at Raindance Film Festival on 8 October 2011.   The United Kingdom theatrical premiere was on 10 August 2012. 

== Reception == Time Out London, Dave Calhoun rated the film 3/5 stars and described it as a "mysterious, thoughtful experiment".   Kim Newman of Empire (film magazine)|Empire rated it 2/5 stars and criticized the films pacing.   Josh Winning of Total Film rated the film 3/5 stars and compared it to The Sixth Sense. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 