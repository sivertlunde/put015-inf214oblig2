Gemini (play)
{{Infobox play
| name = Gemini
| image = GeminiPoster.jpg
| image_size = 200
| caption = 
| writer = Albert Innaurato
| chorus =
| characters = 
| mute =
| setting = South Philadelphia
| date of premiere = December 8, 1976
| place = Playwrights Horizons New York City
| original language = English
| series = 
| subject = 
| genre = Comedy
| web = 
}}

Gemini is a play by Albert Innaurato.

==Plot== WASP Hastings siblings; Judith (who seeks romance with Francis) and Randy (the object of Francis unexpressed affection), who have arrived unexpectedly, much to their friends dismay.  All are dysfunctional to varying degrees, and the interactions among them provide the play with its comic and dramatic moments.

==Production history== Carol Potter (Judith Hastings), and Robert Picardo (Francis Geminiani). Critical response encouraged the producers to transfer the play to Broadway theatre|Broadway. 
 Little Theatre and closed on September 6, 1981 after 1,819 performances. 

A 1980 screen adaptation, written and directed by Richard Benner, was entitled Happy Birthday, Gemini. The cast included Madeline Kahn, Rita Moreno, Alan Rosenberg, David Marshall Grant, and Sarah Holcomb.

A 1999 off-Broadway Second Stage Theatre revival closed after 14 performances. 

==Awards and nominations==
;1977 Off-Broadway
*Obie Award
:Anne DeSalvo - performance - winner
:Danny Aiello- performance - winner
:Albert Innaurato - playwrighting - winner

;1977 Broadway
Drama Desk Award for Outstanding New American Play - nomination

==References==
 

==External links==
*  
*  
*  
*  
*  }}
* 


 
 
 
 
 
 
 
 
 
 
 
 