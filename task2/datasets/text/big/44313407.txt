The Road to Babel
{{Infobox film
| name =   The Road to Babel
| image =
| image_size =
| caption =
| director = Jerónimo Mihura
| producer = 
| writer = José Luis Sáenz de Heredia    Jerónimo Mihura
| narrator =
| starring = Alfredo Mayo   Guillermina Grin   Fernando Fernán Gómez    Mary Lamar
| music = Manuel Parada  
| cinematography = Cecilio Paniagua  
| editing = Julio Peña 
| studio = Chapalo Films 
| distributor = 
| released = 19 February 1945  
| runtime = 77 minutes
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}}
The Road to Babel (Spanish:El camino de Babel) is a 1945 Spanish comedy film directed by Jerónimo Mihura and starring Alfredo Mayo, Guillermina Grin and Fernando Fernán Gómez. Its style was close to that of a screwball comedy. 

==Cast==
*   Alfredo Mayo as César Jiménez  
* Guillermina Grin as Laura  
* Fernando Fernán Gómez as Marcelino Pastor  
* Mary Lamar as Elena  
* Miguel del Castillo as Arturo 
* Tita Gracia as Encarnación 
* Nicolás D. Perchicot as Doctor Gamíndez 
* María Brú as Doña Luisa 
* Antonio Riquelme as Bruno 
* María Isbert as Cloti 
* José Calle as Padre de César 
* Julia Lajos as Enriqueta, mujer de Arturo 
* Manolo Morán as Brandolet 
* Juana Mansó  
* César de Nueda  
* José Luis Ozores 

== References ==
 
 
==Bibliography==
* Labanyi, Jo & Pavlović, Tatjana. A Companion to Spanish Cinema. John Wiley & Sons, 2012.

== External links ==
* 

 

 
 
 
 
 
 
 

 