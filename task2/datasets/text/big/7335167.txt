Parampara (1993 film)
{{Infobox film
| name           = Parampara
| image          =Parampara_(film).jpg
| director       = Yash Chopra
| producer       = Firoz A. Nadiadwala
| story          = Honey Irani
| screenplay     = Aditya Chopra Honey Irani
| starring       = Aamir Khan Sunil Dutt Vinod Khanna Ashwini Bhave Ramya Krishna Saif Ali Khan
| music          = Shiv-Hari
| editing        = Keshav Naidu
| cinematography = Manmohan Singh (director)
| distributor    = A G Films
| released       =  
| country        = India
| language       = Hindi
}}

Parampara ( ) is a 1993 Bollywood movie starring Aamir Khan, Sunil Dutt, Vinod Khanna, Ashwini Bhave, Ramya Krishna, Saif Ali Khan, Raveena Tandon, Neelam Kothari and Anupam Kher. The film is produced by Firoz A. Nadiadwala and directed by Yash Chopra. Aditya Chopra wrote the screenplay for the film. The film was Saif Ali Khans debut.The film was panned by the critics for its weak storyline, weak music and bad direction by Yash Chopra. It was a commercial flop. The movie was shot in the famous Indian boarding school, Mayo College. 

==Plot==

The wealthy Thakur Bhavani Singh (Sunil Dutt) lives in a remote region of India where he shares an intense rivalry with a local gypsy clan led by Gora Shankar (Anupam Kher). Parampara (tradition) dictates that differences are resolved with a single bullet pistol duel at the top of a hill. Years earlier, Bhavani Singh fought a duel with Gora Shankars father and killed him.

Bhavanis son Prithvi (Vinod Khanna) returns from London and strikes up a friendship with Shankar and his clanmates, much to the dismay of his father. As Prithvi gets closer he falls in love with Shankars sister Tara (Ramya Krishna). His father however has arranged Prithvis marriage with the daughter of a fellow upper-class acquaintance. Prithvi is unable to make up his mind and ends up defiantly marrying Tara against his fathers wishes - and subsequently marrying the girl of his fathers choosing, Rajeshwari (Ashwini Bhave).

Later, Bhavani Singh discovers Tara has given birth to Prithvis son and to his fury, orders his men to attack the gypsy colony and to kill Tara, her son and anyone who comes in the way. The Thakurs men set the camps ablaze, killing Tara but not her son, Ranvir. Shankar, who also survived the attack storms in to kill Bhavani Singh in revenge but he is arrested and imprisoned. Prithvi, tells his father that he will never acknowledge his presence again and despite living in the same house, his father will never hear his voice. Rajeshwari then presents him with the rescued Ranvir, earning the respect and love of her husband. Rajeshwari gives birth to a son Pratap, whom Bhavani Singh recognises as his true grandson. Both Ranvir and Pratap are brought up in the same household by Prithvi and Rajeshwari but both boys eventually realise that there is a difference between the two.

Gora Shankar is released from prison and returns to challenge Bhavani Singh to a pistol duel, but before the old man can respond, Prithvi intervenes and says the duel should be fought among equals and that Shankar should be duelling him, not his father. Shankar, who still sees Prithvi as his friend, reluctantly accepts and the duel is set for the next day.

Prithvi silently takes his fathers blessing and heads to the hill alone. At the duel, both Shankar and Prithvi take their pistols and begin to pace away from each other. As the shot to turn and fire is heard, both men turn and aim - but only Shankar fires, shooting Prithvi in the chest. He runs immediately to Prithvi who reveals he never loaded his gun. As Prithvi dies, he tells Shankar to take Ranvir far away from his father, hoping no more blood will be shed in petty rivalry. At Prithvis funeral, both Bhavani Singh and Gora Shankar draw their swords and challenge each other but are stopped by Rajeshwari. Shankar takes Ranvir and leaves the region for good.

Years later, both of Prithvis sons meet in college as strangers, not knowing the identity of the other. Firstly rivals, the two become close friends but after Pratap (Saif Ali Khan) recognises Gora Shankar at Ranvirs (Aamir Khan) house, the two quickly realise that their past is linked but after years of being influenced by their respective guardians, they share a hatred of one anothers families. Ranvir blames Bhavani Singh for killing his mother, while Pratap blames Shankar for killing his father.

Despite the efforts of Shankar and Rajeshwari to end the fighting which will only lead to tragedy, Ranvir ends up challenging his grandfather Bhanvi Singh to a duel. Pratap, echoing the words of his father years ago, states that the duel should be fought among equals and with that the challenge is set.

The next day Pratap arrives with Rajeshwari and Ranvir with Shankar. Just as the battle is about to start, Bhavani shows up on his horse to watch from a distance. The two brothers turn their back to each other and start to pace away. As they do this their grandfather watches them and begins to see visions of his son Prithvi as he looks at both Ranvir and Pratap. The signal to turn and shoot is given and both men turn and fire instinctively - only to see their grandfather between them having taken both their bullets. As Bhavani stumbles to the ground, he cries out for Prithvi, revealing the years of torment of losing his son. The brothers rush to the fallen old man who in his last moments tells them of his regret and hopes that with his death there is no further bloodshed.

After the funeral, Ranvir and Pratap are about to head their separate ways but stop to embrace each other as brothers for the first time.

== Cast ==
* Sunil Dutt as Thakur Bhavani Singh
* Vinod Khanna as Prithvi Singh
* Aamir Khan as Ranbir
* Saif Ali Khan as Pratap
* Ashwini Bhave as Rajeshwari
* Ramya Krishna as Tara
* Raveena Tandon as Vijaya
* Neelam Kothari as Sapna
* Anupam Kher as Shankar
* Vikas Anand as a Qawal Singer
* Mukesh Rishi as Thakur Bhavani Singhs Man

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aadhi Raat Ko"
| Amit Kumar, Lata Mangeshkar
|-
| 2
| "Hum Banjare Dil Nahi Dete"
| Lata Mangeshkar
|-
| 3
| "Tu Saawan Mein Pyaas Piya"
| Lata Mangeshkar
|-
| 4
| "Phoolon Ke Is Shehar Mein"
| Abhijeet Bhattacharya|Abhijeet, Lata Mangeshkar
|-
| 5
| "Mere Sathiya"
| Abhijeet, Lata Mangeshkar
|-
| 6
| "Nawjawanon Ka Zamana Hai"
| Kavita Krishnamurthy
|}

==References==

 

== External links ==
*  

 
 
 
 