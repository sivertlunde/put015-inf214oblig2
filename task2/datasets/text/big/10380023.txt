Population 436
{{multiple issues|
 
 
}}

{{Infobox Film
| name           = Population 436
| image_size     = 
| image	=	Population 436 FilmPoster.jpeg
| caption        = 
| director       = Michelle MacLaren
| producer       = Gavin Polone
| writer         = Michael Kingston
| starring       = Jeremy Sisto Fred Durst Charlotte Sullivan Peter Outerbridge
| music          = Glenn Buhr
| cinematography = Thomas Burstyn
| editing        = Gabriel Wrye
| distributor    = Sony Pictures Home Entertainment
| released       = September 5, 2006
| runtime        = 92 minutes
| country        = Canada United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 2006 mystery film|mystery-horror film starring Jeremy Sisto, Fred Durst, Peter Outerbridge, and Charlotte Sullivan.

==Synopsis==

Steve Kady (Jeremy Sisto), a US Census Bureau researcher is sent to the remote and seemingly idyllic village of Rockwell Falls to interview residents concerning the population. On the way to Rockwell Falls he is distracted by a woman falling off a horse and his vehicle hits a pothole and bursts two tires. He is eventually picked up by Bobby Caine (Fred Durst), the Sheriffs Deputy, who drives him into Rockwell Falls and helps him find a place to stay.

During his stay, Kady notices a number of increasingly strange things about the town, people acting awkward, and strange. People make vague allusions to the fever, and several residents treat him as though he were not just a visitor, but has moved to Rockwell Falls permanently. His research reveals that the towns population has remained at exactly 436 for over 100 years. People who try to leave Rockwell Falls seem to meet with bizarre and deadly accidents, or just vanish, which the residents believe to be the work of God. Kady also begins to have eerie dreams about a truck, a cross and a doll.

Kady becomes romantically involved with Courtney Lovett (Charlotte Sullivan), a local woman and the daughter of his host, much to the chagrin of Caine, who is also in love with her. He also befriends Amanda, a young girl whose father was killed trying to escape from the town and who is being held at the clinic of Dr Greaver, the town doctor, on the pretext of treating her for schizophrenia. Courtney and Amanda both express a desire to leave the town, but are afraid of the consequences of trying.

After stumbling upon some books on Biblical numerology, Kady realizes that the townspeople attach a mystical importance to the number 436 and are willing to go to extreme lengths to keep the population at exactly that number, including executing surplus residents. Anyone who expresses a desire to leave is treated for the fever by Dr. Greaver with electroshock therapy or, in extreme cases, frontal lobotomy. It gradually becomes apparent to Kady that the residents of Rockwell Falls have no intention of allowing him to leave.

After witnessing the execution of a seemingly willing woman at a town feast, Kady becomes hysterical, and is taken to the clinic to be treated for the fever. He escapes from the clinic, and is sheltered by a sympathetic resident who reluctantly helps him plan his escape. After setting fire to the town garage as a diversion, Kady rescues Amanda from the clinic, but is forced to leave Courtney behind after discovering that she has been lobotomized by Dr. Greaver. As Kady and Amanda flee the town in a stolen tow-truck, a rainstorm is brewing and, after a lightning strike, the cross from his dream appears in the truck hanging from the mirror. It is followed by the doll from his dream appearing on the dashboard with the next lightning strike. While he is distracted by these, the truck veers into the path of an oncoming semi-trailer truck, killing them both.

The film ends with one of Kadys co-workers (Christian Potenza), who has come in search of him, being picked up by a police officer after his car hits the same pothole that Kadys did, blowing his tires, echoing the beginning of the film.

===Alternate ending===
In the Alternate ending, Kady and Amanda survive, as Kady swerves to avoid the truck at the last possible moment, avoiding their fatal accident.

==Cultural references== The Village, Robin Hardys The Wicker Man as being a secluded small town stuck in a completely different era from the surrounding world, as in Brigadoon both in custom and in dress, in addition to sharing some major plotlines with The Stepford Wives.
 digit summing).  In Population 436, special meanings are also attributed to the individual numbers 4, 3 and 6.

==External links==
* 

 
 
 
 