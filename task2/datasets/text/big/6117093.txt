Mad About Mambo
{{Infobox film
| name           = Mad About Mambo
| image          = Mad About Mambo Poster.jpg
| caption        = Theatrical release poster
| director       = John Forte
| producer       = David P. Kelly Gabriel Byrne  
| writer         = John Forte William Ash Keri Russell Maclean Stewart
| music          = Richard Hartley
| cinematography = Ashley Rowe
| editing        = David Martin
| studio         = Gramercy Pictures Phoenix Pictures
| distributor    = USA Films
| released       =  
| runtime        = 92 minutes
| country        = Ireland/United Kingdom
| language       = English, Portuguese 
| budget         =
| gross          = $65,283   
}}
 William Ash, Brian Cox.

==Plot summary==
A boy obsessed with football finds his life changing dramatically once he adds a little Samba. Danny (Ash) plays on the football team at the all-boys Catholic school he attends in Belfast. Dannys three best friends, who also play on the team, all have different ambitions for their lives. Mickey (Maclean Stewart) wants to be a fashion designer so he can get rich and date supermodels. Gary (Russell Smith) wants to become a magician so he can get rich and meet beautiful women (and presumably saw them in half). And Spike (Joe Rea) likes to beat people up, so he wants to become a mercenary and do it for a living. But Danny dreams of making football his life.

The players Danny most admires are South Americans, such as Pele and Carlos Riga, who he feels have a special rhythm and flexibility. Wanting to add some of these qualities to his own game, Danny has an idea: hell take Samba lessons, in the hope that dancing like a South American will help him play like a South American. To the surprise of himself and his friends, Danny turns out to be a pretty good Latin dancer and finds himself smitten with a student in his dance class, Lucy (Russell). However, Lucy happens to have a boyfriend, who is a fierce competitor on one of Dannys rival teams.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 