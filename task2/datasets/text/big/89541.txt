A Better Tomorrow
 
 
 
{{Infobox film name = A Better Tomorrow image = ABetterTomorrowPosterHK.jpg caption = Hong Kong film poster simplified    = 英雄本色
| traditional = 英雄本色
| pinyin = Yīng Xióng Běn Sè
| jyutping       = Jing1 hung4 bun2 sik1 }} director = John Woo producer = Tsui Hark John Woo writer = Chan Hing-Ka Leung Suk-Wah John Woo starring = Ti Lung Leslie Cheung Chow Yun-fat music = Joseph Koo editing = David Wu cinematography = Wing-Hung Wong studio = Cinema City Company Limited Film Workshop distributor = Cinema City & Films Co. released =   runtime = 95 min. country = Hong Kong language = English   budget =
}} international scale.

Although it was produced with a tight budget and was relatively unknown until it went on screen (due to virtually no advertising), it broke  , a prequel directed by Tsui Hark.
Out of the various movies it inspired across nations, a very popular Hindi remake was   starring Sanjay Dutt.

Although Ti Lung was the films lead actor, co-star Chow Yun-fats breakout performance out-shined him, solidifying the latters status as one of the top superstars in the Hong Kong film industry. Chows character "Mark Gor" was imitated by many fans even decades after the films release. {{cite web|url=http://news.singtao.ca/toronto/2013-06-01/city1370076020d4524086.html|title=
周潤發憑《英雄本色》 奠定香港影壇地位|accessdate=10 November 2014|publisher=Sing Tao Daily}} 

==Plot==
Sung Tse-Ho (Ti Lung) works for the Triad (underground society)|Triad, whose principal operation is printing and distributing counterfeit US bank notes. Ho is a respected member of the organization and is entrusted the most important transactions. Mark Lee (Chow Yun-Fat), another high-ranking member of the group,    is his best friend and partner in crime.

Ho is close to his younger brother, Kit (Leslie Cheung), who is training to become a police officer. Ho keeps his criminal life secret from his brother and encourages Kits career choice. Hos father is aware of Hos criminal activities and appeals to him to go straight. Ho agrees, deciding that his next deal in Taiwan will be his last one before leaving the Triad. Shing (Waise Lee), a new member, is sent along as an apprentice. The deal turns out to be a trap by the Taiwanese gang. A shootout ensues in which Ho and Shing flee, pursued by local law enforcement. 

Meanwhile, a gang member attempts to kidnap Hos father to ensure Hos silence if he is caught by police; in the ensuing fight also involving Kit and his girlfriend, Hos father is killed. Just before dying, he pleads with Kit to forgive his brother. Ho eventually surrenders to the police in order to buy time for Shing to escape. After learning of Hos capture, Mark finds and kills the Taiwanese gang leader and his bodyguards. However, Marks leg is shot in the gunfight, leaving him crippled.

Ho is released from prison three years later. Remorseful and determined to start a new life, he finds work as a driver for a taxi company, run by another ex-con. Ho spots Mark during one of his shifts; in contrast to Marks letters, he realizes that Mark has been reduced to an errand boy for Shing (who is the new leader of the Triad).  During an emotional reunion, Mark asks Ho to take revenge on Shing and reclaim their positions in the organization, but Ho refuses.

Ho seeks Kit out and attempts to reconcile with his brother (who is now a police officer), but is disowned by Kit, who sees Ho as a criminal who is responsible for their fathers death. Additionally, Kit is resentful that his familial tie to Ho is preventing him from advancement in the department.  In an effort to prove himself to his superiors and further distance himself from his brothers criminal past, Kit becomes obsessed with bringing down Shings criminal group, despite Hos warnings to stay away from the dangerous case.

Shing finds Ho and presses him to come back to his organization, offering to reinstate Mark if he returns. Ho flatly refuses. Consequently, Shing begins harassing Ho in order to get him to return, including luring Kit into a trap and injuring Kit, attacking Hos co-workers, and having Mark beaten severely. Ho is dismayed but is still hesitant to take action, but an impassioned speech by Mark finally convinces Ho to join Mark in taking revenge on Shing. 

Mark steals incriminating evidence from the counterfeiting business and wins a shootout with gang members, with Ho arriving to aid Marks escape. The film then reveals that it was Shing who betrayed Ho three years ago in Taiwan. Ho and Mark use the evidence to ransom Shing in exchange for money and an escape boat. However, Ho ensures that the evidence is passed to Kit to hand to the police. Using Shing as a hostage, Ho and Mark take the money to a pier, where Shings men await. There, Ho persuades Mark to escape by himself in the boat.

After Mark leaves, Kit arrives on the scene intending to make an arrest where he is captured by Shings men. A deal is made to exchange Shing with Kit, but the trade explodes into a wild shootout. Ho and Kit are wounded, but Mark returns with guns blazing out of loyalty to Ho. After Ho, Kit and Mark kill many of Shings men, Mark berates Kit, telling him that Hos actions had atoned for whatever wrongdoings he had done in the past. Mark is in turn killed by Shing.

As the police approach, Shing mocks Ho (who has run out of ammunition), stating that he will surrender, but his money and power will ensure his swift release. Kit, finally seeing eye to eye with his brother, hands Ho a revolver, with which Ho kills Shing. Immediately afterwards, Ho handcuffs himself to Kit, expressing his desire for redemption and his admiration that Kit always walked the right path. The film ends with the reconciled brothers walking together towards the gathered crowd of police.

==Cast==
* Ti Lung as Sung Tse-Ho
* Leslie Cheung as Sung Tse-Kit
* Chow Yun-fat as Mark Lee
* Emily Chu as Jackie, the girlfriend of Kit
* Waise Lee as Shing
* Shing Fui-On as Shings right-hand man
* Kenneth Tsang as Ken, the leader of the taxi company Ho joins
* Tien Feng, as the father of Ho and Kit
* John Woo, the film director, plays the bespectacled Taiwanese police chief
* Tsui Hark as a music judge (cameo)

== Theme song ==
The films theme song is "In the Sentimental Past" (當年情), performed by lead Leslie Cheung, composed and arranged by Joseph Koo and written by Wong Jim.

==Box office==
A Better Tomorrow grossed $34,651,324 HKD at the Hong Kong box office, ensuring that sequels and imitators would not be far behind. 

==Musical references==
*During the nightclub scene, the song being played in the background (幾許風雨, Gei2 heoi2 fung1 jyu5) is the Cantonese version of a classic South Korean song called Hee Na Ree(희나리) sung originally by Goo Chang-mo( ) in 1985. The Cantonese version in the movie was sung by Roman Tam, considered the "godfather" of the musical genre Cantopop.
* In the scene where Kit rushes Jackie to a music recital, the violinist playing before Jackie plays the theme song of the movie.
* Also heard in the soundtrack is "Sparrowfall 1", a track from Brian Enos 1978 album, Music for Films.
*The film also contains "Birdys Theme" (from the film Birdy (film)|Birdy) by Peter Gabriel incorporated into the soundtrack. Tomorrow will be Better (明天会更好/明天會更好), written by Lo Ta-yu.  This is likely the origin of the films English title.

==Film references==
* Chow Yun-fats entrance to the restaurant before the shoot-out is John Woos homage to Mean Streets.
* Woos film was partially inspired by the 1967 Lung Kong film   list of the Top 100 Chinese Films.
* The scene in which Mark Lee tells the story of being forced to drink urine is apparently based on a real incident involving Chow Yun-fat and director Ringo Lam, according to Bey Logan on the DVD commentary. This scene was recreated in Woos Bullet in the Head.

==Cultural impact== dusters in trench coats are called "Mark Gor Lau" (literally, Brother Marks coat).
* The Wu-Tang Clan has a song named after the film on their 1997 album Wu-Tang Forever. The Wu-Tang Clan also has plans to release their 20th anniversary album which will share its name with the film. Spike and Vicious in the episode List of Cowboy Bebop episodes|"The Real Folk Blues (Part 2)" which parallels the final shoot out in "A Better Tomorrow 2".
*The character Mr. Chang from the Black Lagoon is closely patterned after Chows character Mark in both visual design and characterization. 
*Chow wore Alain Delon sunglasses in the movie.  After the movie, Hong Kong was sold out of Alain Delons sunglasses.  French star Alain Delon sent Chow a personal thank you note. Empire Magazine named it #20 in a poll of the 20 Greatest Gangster Movies Youve Never Seen* (*Probably)
* In 1994   (meaning: The Fire) that starred Sanjay Dutt, Atul Agnihotri, Aditya Pancholi and Shakti Kapoor in the lead.
*In September 2010, prolific Korean filmmaker Song Hae-Sung released A Better Tomorrow (2010 film)|Mujeogja (Invincible) which was an official Korean language remake of John Woos A Better Tomorrow.  It opened to positive response at the Korean box-office.  John Woo and Terence Chang also serve as Executive Producers for Mujeogja; which was a joint production between South Korea, Japan and China.

==Remake Anthology==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;" A Better Tomorrow (2010) (Korean cinema|Korean) 
|-
| John Woo || Sanjay Dutt || Joo Jin-mo
|- Kim Ji-yung
|}

==See also==
* A Better Tomorrow 2
* A Better Tomorrow 3
*  

* Hong Kong in films
* Heroic bloodshed
* List of Hong Kong films

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 