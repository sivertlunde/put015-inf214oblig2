Live Free or Die (2006 film)
 
{{Infobox film
|  name = Live Free or Die
| image = Live free or diemp.jpg
| caption = Live Free or Die one sheet
| director = Gregg Kavet Andy Robin
| writer = Gregg Kavet Andy Robin Paul Schneider Ebon Moss-Bachrach Judah Friedlander Michael Rapaport Kevin Dunn Zooey Deschanel
| runtime = 89 minutes
| country = United States
| language = English
}} Paul Schneider, Zooey Deschanel, Michael Rapaport, Judah Friedlander, Kevin Dunn, and Ebon Moss-Bachrach.  It was written and directed by former Seinfeld writers Gregg Kavet and Andy Robin.

==Plot==
A clueless, aspiring criminal named John "Rugged" Rudgate (Stanford) spends his days forging rebate coupons and selling speakers out the back of his van. One day, Rugged runs into an old acquaintance, the dim-witted Jeff Lagrand (Schneider), who recently returned home to help his cynical sister run the storage facility that they inherited from their father. When Rugged tries to force his way into the Lagrand family business, things go terribly wrong—and the situation gets even more complicated when an emotionally unstable cop begins investigating.

==Reception==
The film-review aggregate website Rotten Tomatoes gave the film a 56% approval rating.  Film critic Frank Lovece of Film Journal International praised Aaron Stanford as "the young Steve Buscemi" and wrote that despite the films "lack of visual click, Live Free or Die manages to be poignant without even being maudlin" and that "none of the movies flaws negate its many remarkable little performances and casually insightful script. 

==See also==
*Live Free or Die

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 