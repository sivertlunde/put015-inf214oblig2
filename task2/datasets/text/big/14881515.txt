Master of Thunder
{{Infobox film
| name = Master of Thunder
| image =
| caption = Master of Thunder cover
| director = Kenji Tanigaki
| producer = Shigeyuki Yasumura, Masato Onishi, Nana Kurata
| writer = Kenji Tanigaki, Mao Aoki
| starring = Sonny Chiba, Yasuaki Kurata, Ayumi Kinoshita
| music = Koji Kikkawa, Tokusatsu
| cinematography = Ryo Serizawa
| editing = Daisuke Yoshimoto
| distributor = Amuse Soft Entertainment
| released =  
| runtime = 92 minutes
| country = Japan
| language = Japanese
| budget = 
}} Japanese martial-arts film starring Sonny Chiba.

==Plot==
For 1400 years, Spiritual Guardians have watched over the mountains of Japan and defeated the evil spirits there. The nearby Kikyo Temple is rumored to have been the home of these legendary Guardians known as the "Blue Seven Dragons." Only two survivors of the long battle between good and evil remain, the martial monks Santoku (Yasuaki Kurata) & Genryu (Sonny Chiba). Now the fate of the world must be decided once and for all in a final ferocious battle between the force of good and evil.

==External links==
*  
*    

 
 
 
 
 


 
 