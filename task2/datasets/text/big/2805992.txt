Barsaat (2005 film)
 
{{Infobox film
| name           = Barsaat: A Sublime Love Story
| image          = Barsaat.jpg
| caption        = Theatrical release poster
| director       = Suneel Darshan
| producer       = Suneel Darshan
| writer         = Suneel Darshan Robin Bhatt Shyam Goel Rumi Jaffery K.K. Singh
| starring       = Bobby Deol Priyanka Chopra Bipasha Basu
| music          = Nadeem-Shravan
| cinematography = W.B. Rao
| editing        = Sanjay Sankla
| studio         = 
| distributor    = Shree Krishna International
| released       =  
| runtime        = 144 minutes
| country        = India
| language       = Hindi
| budget         = Rs. 12 crores
}} Sweet Home Alabama.

==Plot==
Arav (Bobby Deol) is an ambitious young Indian whose dream is to design cars. He travels to the United States seeking greener pastures, where he meets the beautiful Anna (Bipasha Basu). Anna instantly goes head over heels for Arav, but Arav remains focused on his career. Over time, he mellows and the two fall in love. Moreover, Aravs ambitions go on the upswing as the chairman of the BMW company in the U.S. (Shakti Kapoor) gives him a job as a designer. 

Coincidentally, the BMW chairman happens to be Annas grandfather, and he announces the impending nuptials of Arav and Anna.

==Cast==
* Bobby Deol as Arav Kapoor
* Priyanka Chopra as Kajal
* Bipasha Basu as Anna Virvani
* Shakti Kapoor as Mr. Virvani (Annas grandfather)
* Manmeet Singh as Maninder Singh (Aravs friend)
* Farida Jalal as Kajals grandmother
* Sharat Saxena as Lawyer
* Gajendra Chauhan as Aravs father
* Beena Banerjee as Aravs mother
* Vivek Shauq as Shammi
* Supriya Pilgaonkar as Supriya (Aravs sister-in-law)
* Mahesh Thakur as Dr. Pranav Kapoor
* Vivek Vaswani as Garage owner
* Delnaaz Paul as Dolly Bindra
* Palak Jain as child Kajal

==Reception==
The film received mixed to negative reviews from critics. Taran Adarsh of Bollywood Hungama gave 1/5 and said Barsaat is like watching a 1970 film. Overall the film was rated 5/10. It was declared below average at the box office.  

==Box office==
At the box office, Barsaat had a good initial opening due to the curiosity surrounding the two damsels. Barsaats raw box Office net collections was Rs. 6.67 crores and total box office gross was Rs. 9.44 crores. It did well in overseas (UK) with a total gross of £454,660

==Soundtrack==
The Soundtrack Of The Movie Is Composed By The Music Duo Nadeem Shravan.The Song Lyrics Planned By Sameer. All songs were very popular in 2005 with the song "Barsaat Ke Din Aaye" being chart busters. Singer Kumar Sanu, Alka Yagnik give their voices for the song. Other singers who have sung for the album include Abhijeet Bhattacharya|Abhijeet, Udit Narayan, Alisha Chinai,  Sonu Nigam, Kailash Kher, Ishq Bector & Priyanka Chopra. The album was voted at No 9 in the all time music sales chart.

{| class="wikitable"
! No !! Title !! Singer(s)
|-
| 1
| "Saajan Saajan Saajan"
| Kailash Kher, Alka Yagnik, Priyanka Chopra
|-
| 2
| "Shaadi Jo Kiya" 
| Alisha Chinai, Ishq Bector
|-
| 3
| "Maine Tumse Pyaar Bahut Kiya"
| Alka Yagnik
|-
| 4
| "Aaja Aaja" 	
| Alka Yagnik
|-
| 5
| "Chori Chori"
| Udit Narayan, Alka Yagnik, Sapna Awasthi
|-
| 6
| "Mushkil"
| Abhijeet Bhattacharya|Abhijeet, Alka Yagnik
|-
| 7
| "Pyaar Aaya"
| Sonu Nigam, Alka Yagnik
|-
| 8
| "Barsaat Ke Din Aaye"
| Kumar Sanu, Alka Yagnik
|}

==References==
 

==External links==
*  

 
 
 
 
 