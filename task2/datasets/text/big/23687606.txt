I figli di nessuno
{{Infobox film
| name           =I figli di nessuno
| image          =I figli di nessuno.jpg
| image_size     =
| caption        =
| director       = Raffaello Matarazzo
| producer       =
| writer         =
| narrator       =
| starring       =
| music          = 	Salvatore Allegra
| cinematography =
| editing       =Mario Serandrei
| distributor    =
| released       = 1951
| runtime        =
| country        = Italy Italian
| budget         =
}}
 1951 Italy|Italian drama film.

==Cast==
* Amedeo Nazzari: Guido Canali
* Yvonne Sanson: Luisa Fanti / Suor Addolorata
* Françoise Rosay: Contessa Canali
* Folco Lulli: Anselmo Vannini
* Enrica Dyrell: Elena
* Teresa Franchini: Marta
* Gualtiero Tumiati: Padre Demetrio
* Alberto Farnese: Poldo
* Aristide Baghetti: Bernardo Fanti, padre di Luisa
* Enrico Glori: Rinaldi
* Olga Solbelli: Madre Superiora
* Nino Marchesini: il dottore
* Rita Livesi: la suora
* Giulio Tomasini: Antonio
* Enrico Olivieri: Bruno
* Rosalia Randazzo: Alda
* Felice Minotti: custode
* Loris Gizzi: direttore del collegio

==External links==
*  

 
 
 
 
 
 

 