Hideaways
 
{{Infobox film
| name           = Hideaways
| image          = Hideaways (film).jpg
| caption        = 
| director       = Agnès Merlet
| producer       = Marc Missonnier Olivier Delbosc Jean-Luc Ormieres Nick Murphy
| narrator       = Rachel Hurd-Wood
| starring       = Rachel Hurd-Wood Harry Treadaway James Wilson Thomas Sangster Susan Lynch
| music          = 
| cinematography = Tim Fleming 
| production designer = Didier NAERT
| editing        = 
| studio         = Ardmore Studios Wild Bunch
| released       =  
| runtime        = 
| country        = Ireland France Sweden
| language       = English
| budget         = €6.6 million 
| gross          = 
}} Nick Murphy. It stars Rachel Hurd-Wood and Harry Treadaway. The film is a French/Irish/Swedish co-production.

==Plot==
James Furlong is the last in a long line of Furlongs who were each blessed or cursed with a strange ability. His grandfather, Charlie went temporarily blind when he thought about sex. His father, Philip could turn off anything electrical when he was frightened. From the moment of his violent birth, involving the death of his mother, his life seems ill-fated, but it is unclear what kind of ability, if any, he possesses. Growing up in rural Ireland, his grandmother tells him about the strange quirks of his ancestors and the boy begins to experiment on himself, longing to discover some extraordinary, hidden power. But instead, his experiments lead to the death of his familys livestock, followed swiftly by the loss of Philip and his beloved grandmother, Charlotte. By the time he is ten years old, James is the sole survivor of the Furlong family. 

James is sent to St. Judes reformatory, but does not adjust easily to life there. Having been home-schooled on the farm, he is not equipped with the necessary social skills or abilities on the sports-field. He is bullied by the other boys, especially Kevin and Stephen. His only friend is Liam, the only one who shows him any kindness. His powers then kill the hurling Coachs vicious dog Tinkerbell. The principal, Mrs Moore suspects Kevin, because of his smug attitude. Kevin and Stephen attack James, for getting Kevin into trouble. A mysterious illness then sweeps through the reformatory, killing everyone except James and Liam, who flees and suffers only lung problems. In the chaos that ensues, he finally begins to comprehend the dark, destructive nature of his powers, and at the first opportunity he goes into hiding. 

Years later, he is discovered by a young woman, living out a lonely existence completely isolated from society. Mae (named after actress Mae West) has run away from hospital where she is being treated for cancer and comes across Jamess cottage deep in the woods by chance. Initially, he is reluctant to have anything to do with her, fearful of harming her, but Mae has already resigned herself to her fate and is afraid of very little – certainly not of James with his gentle nature, and childlike innocence. Even when she learns his story, she is unafraid, and urges him to leave the cottage and his lonely life behind. Mae wants to savour all the time she has left in the world and hates to see someone so loving and compassionate isolated from it and denying himself everything that life has to offer. 

They soon fall in love, but Mae returns to the hospital soon after. James visits her, where they consummate their love. Soon after, James bumps into Liam, the sole survivor from the reformatory. In the final climactic scenes, Liam confronts James with the destruction of his past, and kills him with a scissors, before running away. James strange powers had cured everyone in the hospital and stopped Maes death. The film ends six years later with Mae telling their six-year-old daughter, Diana (named after singer Diana Ross) about James.

==Cast==
* Rachel Hurd-Wood as Mae-West OMara
* Harry Treadaway as James Furlong
* James Wilson as James Furlong (10)
* Thomas Sangster as Liam
* Susan Lynch as Mrs OMara
* Stuart Graham as Sergeant
* Aaron Monaghan as Philip Furlong
* Diarmuid ODwyer as Liam (11)
* Craig Connolly as Kevin
* Calem Martin as Stephen
* Chris Johnston as Bully
* Shane Curry as Bully 3 Kate OToole as Mrs. Moore
* Bern Deegan as Charlie Furlong (32)
* Mairead Reynolds as Charlotte Furlong (28)
* Holly Gregg as Laura
* Niamh Shaw as Night Nurse
* Tom Collins as Mr. Boyle
* Brianna O Driscoll as Diana-Ross Furlong-OMara
* Callum Maloney as Philip Furlong (5)
* Joe Dermody as Coach Hanley

==Production==
Principal photography began on 10 May 2010 in Ireland.    Filming took place on location in Wicklow and County Meath|Meath. Filming was scheduled to last two months,  but completion of principal photography was delayed as a result of Hurd-Wood becoming ill.  Filming locations included Ballygarth Castle in Julianstown, Meath and Shanganagh Castle in Shankill, Dublin. The film was originally called The Last Furlong but was changed to Hideaways. It premiered in the 2011 Tribeca Film Festival.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 