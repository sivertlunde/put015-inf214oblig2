Louise Wimmer
{{Infobox film
| name     = Louise Wimmer
| image    = 
| director = Cyril Mennegun
| producer = Bruno Nahon
| writer = Cyril Mennegun   Anne-Louise Trividic (collaboration)  	
| starring = Corinne Masiero
| cinematography = Thomas Letellier
| music = 
| country = France
| language = French
| distributor = Haut et Court
| runtime = 80 minutes
| released =  	
}}
Louise Wimmer is a 2012 French drama film directed by Cyril Mennegun. 

==Plot==
Louise Wimmer barely scrapes a living and has to resort to living in her constantly malfunctioning car. Eventually an efficient social security officer jump starts her life with the prospect of a new home.

== Cast ==
* Corinne Masiero as Louise Wimmer
* Jérôme Kircher as Didier
* Anne Benoit as Nicole
* Marie Kremer as Séverine
* Jean-Marc Roulot as Paul
* Frédéric Gorny as le manager de lhôtel
* Maud Wyler as Jessica Wimmer

==References==
 

== External links ==
* 

 

 
 
 
 
 

 
 