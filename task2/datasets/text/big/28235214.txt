Eccezzziunale... veramente
 
{{Infobox film
| name           = Eccezzziunale... veramente
| image          = Eccezzziunale... veramente.jpg
| caption        = Film poster
| director       = Carlo Vanzina
| producer       = Claudio Bonivento Alessandro Fracassi
| writer         = Carlo Vanzina Diego Abatantuono Enrico Vanzina
| starring       = Diego Abatantuono
| music          = Detto Mariano
| cinematography = Alberto Spagnoli
| editing        = Raimondo Crociani
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Eccezzziunale... veramente  is a 1982 Italian comedy film directed by Carlo Vanzina. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Plot==
Diego Abatantuono portrays three different parts as a tifoso (supporter) of the three main football teams of Italy: A.C. Milan, Internazionale and Juventus F.C.

The three characters are: Donato Cavallo (A.C. Milan supporter), the Ras (boss) of the hooligans of the southern stand in San Siro stadium; Franco Alfano, Inter supporter who believes he won the football lottery, but its just a prank made by his friends; Tirzan, Juventus supporter, is a truck driver from Apulia, ho decided to follow his beloved team in Belgium for a Champions League game.

==Cast==

* Diego Abatantuono: Donato Cavallo / Franco Alfano / Felice La Pezza, aka "Tirzan"
* Massimo Boldi: Massimo
* Teo Teocoli: Teo
* Stefania Sandrelli: Loredana
* Yorgo Voyagis: "The Slavic"
* Ugo Conti: Ugo
* Anna Melato: Wife of Franco
* Gianfranco Barra: Inspector Patanè
* Clara Colosimo: Francos mother-in-law
* Renzo Ozzano: Parisian Inspector
* Renato DAmore: Sandrino 
* Franco Caracciolo: Hitchhiker 
* Enio Drovandi: Policeman 
* Guido Nicheli: Man at bar
* Franca Scagnetti:Wife of La Monica

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 