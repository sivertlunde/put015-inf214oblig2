Thaayi Saheba
{{Infobox film
| name           = Thaayi Saheba
| image          = 
| caption        = 
| director       = Girish Kasaravalli
| producer       = 
| writer         = 
| screenplay     = Girish Kasaravalli
| story          = 
| based on       =   Jayamala
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
Thaayi Saheba ( ) is a Kannada language film released in 1997 directed by Girish Kasaravalli.

==Plot summary==

The film is based on the story of a Brahmin family during the pre independence and post independence periods of India. The protagonist Narmada Thayi is the second wife of Appa Sahib, a freedom fighter. The childless couple decide to adopt the child of Venkobanna, a close relative. Venkobanna has other plans in mind when he gives away his child in adoption. He calculates that his son will inherit the familys money and property. In the meanwhile, Appa Sahib also has an illicit relationship with Chandri and a daughter is born to them. Narmada Thayi is a patient woman and although aware of her husbands character, she supports him and the household. After the independence, Appa Sahib supports the governments view that the laborers who till the land own them. Venkobanna is angered to see this long awaited plan of his crumble due to the utopian views of App Sahib. After a brief illness, Appa Sahib leaves the house one day with a group of people to protest for the sake of the farmers never to return again. Months after this, Narmada learns that her husband has been imprisoned by the government. But no other detail regardinhg Appa Sahib comes to light. Narmada Thayi is the only one capable of handling the household. She seeks the help of Venkobanna to help her handle monetary issues. Meanwhile their adopted son comes of age. The story takes a surprise and unexpected turn when Thayi Saheba discovers that her adopted son is in love with Appa Sahibs mistresss daughter, who legally is his sister. She tries in vain to convince her son not to go against social ethics. With Chandri, Narmada Thayi sets out to find her husband. Together they travel to many prisons, but Appa Sahib is never found. Thayi Saheba must either save the son, who seeks freedom to marry Chandris daughter or save herself because if she supports the marriage, she could be jailed. She tries to cancel the adoption, but no such thing as adoption cancellation existed then. She also tries to see if somebody can adopt Chandris daughter, but adopting a girl child was never allowed. Finally, Thayi Saheba tells her son that he is free to do whatever he wants. She gets ready to face the serious consequence of the marriage. The film ends with Thayi Saheba waiting on the steps of her house and an angered Venkobanna arriving with the police and announcing their arrival.

==Interpretations==
It is difficult to say what Thaayi Saheba was all about. It was evidently a film that narrated the transition in the Indian society from the pre independence to the post independence periods. We can easily skip the transition part and look at the film as a straight narrative about the life of Narmada Thayi. The film also narrates the social conditions of women during that period. Thayi Saheba can definitely be viewed as a sum total of all these interpretations. We witness the social and political transition of the Indian Society through the life of the protagonist.

==Highlights==
*Thaayi Saheba was a very well made film. The plot subject was highly complex and handling such an intricate theme and adapting it to the celluloid was a difficult task. Jayamalas performance as Thaayi Saheba was widely appreciated.
*The climax was yet another feature of the film that left a lasting impression on the viewer. The picture of Narmada Thayi sitting on the porch of her house prepared to face the police is unforgettable. As Venkobanna announces the arrival of the police, the camera just moves up giving us a final glimpse of the protagonist and then the light fades out.

==Awards and screenings== National Film Awards, India, 1998: Best Film - Girish Kasaravalli Special Jury Award - Jayamala Best Costume Design - Vaishali Kasaravalli Best Art Direction - Mr.Ramesh Desai.

Mysore Sandal Gold Awards 
*Best Film
*Best Cinematograper - Ramachandra
*Best Actress - Jayamala

Filmfare Awards
*The film won three filmfare awards in the Kannada film category.

Screenings
*Vancouver film festival.

==References==
 

==External links==
* 

 
 

 
 
 
 
 