Thalapathi
{{Infobox film
| name           = Thalapathi
| image          = Thalapathi_poster.jpg
| caption        = Official poster
| director       = Mani Ratnam
| writer         = Mani Ratnam
| starring       =  
| producer       = G. Venkateswaran
| music          = Ilaiyaraaja
| studio         = G. V. Films
| distributor    = G. V. Films
| cinematography = Santosh Sivan
| editing        = Suresh Urs
| released       =  
| runtime        = 157 minutes
| country        = India
| language       = Tamil
| budget         =
| gross          =
}}
 Tamil crime Telugu and Hindi as Dalapathi. The film, being set in a contemporary milieu at the time of its release, emerged as a critical and commercial success during its theatrical run.  The film was one among the Deepavali releases of 1991.

==Plot==
Surya (Rajinikanth), the abandoned child of fourteen-year-old unwed mother Kalyani (Srividya) is brought up by a generous lady living in the slums. He grows up to become a fearless kind-hearted slum king who fights the injustice that takes place daily. These qualities lead him to cross swords with the local don Devarajan (Deva) (Mammootty). Devas friend Ramana is fatally beaten up by Surya, and he threatens Surya that he will find himself in a dangerous situation if the friend was laid to rest. His friend passes away, and Surya gets placed in prison. However, when Deva discovers Ramanas offense, he decides to get Surya released from jail.

Surya is surprised to see Deva send his own man to get him out from jail, and there he tells Surya, Nyayamnu onnu irrukku (there is such a thing called justice). From that moment, Deva surrenders his ego and offers his hand of friendship to Surya. Surya pledges that he has nothing else but his life to give his new friend. From then on Deva makes Surya his "Thalapathi" (commander). Meanwhile Surya falls for a local girl Subbulakshmi (Shobana). They soon fall in love and their relationship continues. Surya and Deva virtually rule the town with their muscle and their sense of justice. In the eyes of the government, they are anti-social elements. Arjun (Arvind Swamy), posted as the new district collector, plans to arrest both so that he can end their brutal activities. But his opportunities are in vain.

At this time, Deva and his wife go to Subbulakshmis house asking for her betrothal to Surya. But her father refuses stating he cannot marry his daughter to a person who doesnt know about his parents. Instead, Subbulakshmi is married to Arjun. Surya eventually marries another girl (Bhanupriya) who was widowed by him earlier (her husband was Ramana). Surya, still troubled by the guilt, prefers to remain as a guard to the widow and her daughter.

A struggle breaks out between the collector and Deva/Surya during which Surya learns that Arjuns mother is his mother, too. Surya promises his mother that he will not harm Arjun but refuses to leave Deva, whose friendship is more valuable to him than his relationship with his brother. Deva is stunned to learn that collector and Surya are brothers and admires Surya for not parting with him in spite of discovering his identity. During the process of surrendering to the collector, assuring him completely stopping their activities, Deva is shot by his enemy. Surya, in a fit of rage, brutally kills Kalivardhan (Amrish Puri) who was the cause for Devas death and surrenders to the police after performing the final rites of his friend. But the police withdraw the case due to lack of evidence and witnesses. The collector goes to another place on a new posting accompanied by his wife. His mother then changes her mind to stay with her elder son, Surya.

== Cast ==
 
* Rajinikanth as Surya
* Mammootty as Devaraj
* Arvind Swamy as Arjun Geetha as Selvi
* Bhanupriya as Padma
* Shobana as Subhalakshmi
* Srividya as Kalyani
* Jaishankar as Arjuns father
* Amrish Puri as Kalivardhan
* Nagesh as Panthulu
* Charuhasan as Sreenivasan Kitty as Police Commissioner Sundaram
* Charle
* Nirmalamma as Suryas foster mother
* Sonu Walia in a special appearance in the song "Rakkamma Kaiya Thattu"
* Fathima
* P. L. Narayana
* Kiruba
* Pradeep Shakti as Kalivardhans right hand
* Manoj K. Jayan as Manoharan
* Tarun Kumar as a petty thief
* Baby Vichitra as Azhagi
* Anand Krishnamoorthi
* Vincent Roy as Shanmugam
* Sakthivel as Sub-inspector Palanivel
* Jaya Prahasam as police officer
* Ravi
* Kaushik
* Thalapathy Dinesh as Ramana
 

==Production==
 
  from the Indian epic Mahabharata, who Ratnam considers "one of the best characters in the Mahabharata".  Mani Ratnam wanted to present a realistic Rajini, which he saw in 16 Vayathiniley and Mullum Malaram minus all his style elements.  Rajini recalled that he had tough time while shooting for the film as "He   was from a different school of film making and asked me to feel emotions even when taking part in a fight scene". 

Mammootys character Deva was the equivalent of Duryodhana, while Rajinikanths played Surya, the equivalent of Karna. Shobana played the equivalent of Draupadi, while Arvind Swamy and Srividya played Arjuna and Kunti respectively.  Actor Kreshna was chosen to play the younger version of Rajinikanths character, although the character was later scrapped because it affected the films length.  Cinematography was handled by Santosh Sivan,   the film being his first with Ratnam.  Ratnam chose to shoot the beginning sequence in black and white instead of colour, because according to him, "Black and white gives the sense of this being a prologue without us having to define it as a prologue."  He has also refused to state who was the father of the protagonist, citing that the film "consciously avoids the who and the how of the underage girls first love. It was the child, the son of Surya, who formed the story".  Aravind Swamy made his acting debut with this film.  Malayalam actor Manoj K. Jayan was recruited to play an important character after Mani was impressed by his performance in Malayalam film Perumthachan thus made his acting debut in Tamil cinema. 

The songs "Rakkamma" and "Sundari" were filmed at Rayagopura, Melkote and Chennakesava Temple, Somanathapura in Karnataka respectively.  

==Soundtrack==
{{Infobox album
|  Name        = Thalapathi: The Original Motion Picture Soundtrack
|  Type        =  Soundtrack
|  Artist      = Ilaiyaraaja
|  Cover       = 
|  Background  = Gainsboro
|  Released    =  1991
|  Genre       = Soundtrack
|  Length      =  32:30
|  Label       = Lahari Music
|  Producer    =  Ilaiyaraaja
|  Reviews     =
|  Last album  = Anjali (film)|Anjali (1990)
|  This album  = Thalapathi (1991)
|  Next album  = Mannan (1992)
}}
 Agent Vinod, and Lahari took "legal action" against the producer of the film Saif Ali Khan, who used the song without permission.   The recording for the song "Sundari" had taken place in Mumbai with R. D. Burmans orchestra. When Raja gave them the notes they were so taken in by composition that all the musicians put their hands together in awe. 

;Original version (Tamil)
{{tracklist
| collapsed       = yes
| all_music       = Ilaiyaraaja Vaali
| extra_column    = Artist(s)
| title1          = Yamunai Aatrile
| extra1          = Mitali Banerjee Bhawmik
| length1         = 1:22
| title2          = Rakkama Kaiya Thattu
| extra2          = S. P. Balasubrahmanyam, Swarnalatha
| length2         = 7:10
| title3          = Sundari Kannal
| extra3          = S. P. Balasubrahmanyam, S. Janaki
| length3         = 7:14
| title4          = Kaattukuyilu
| extra4          = S. P. Balasubrahmanyam, Yesudas
| length4         = 5:32
| title5          = Putham Puthu Poo
| extra5          = Yesudas, S. Janaki
| length5         = 5:00
| title6          = Chinna Thayaval
| extra6          = S. Janaki
| length6         = 3:23
| title7          = Margazhithan Chorus
| length7         = 2:39
}}

;Track listing (Hindi) 
{{tracklist
| collapsed       = yes
| all_music       = Ilaiyaraaja
| all_lyrics      = P. K. Mishra
| extra_column    = Artist(s)
| title1          = Yamuna Kinare
| extra1          = Sadhana Sargam
| length1         = 1:22
| title2          = Jaaneman Aaja Aaja
| extra2          = Kumar Sanu, Sadhana Sargam
| length2         = 7:10
| title3          = Sundari Yeh Jeevan Tera
| extra3          = Suresh Wadkar, Sadhana Sargam
| length3         = 7:14
| title4          = In Aankhon Ka Tu Tara
| extra4          = Kavitha Krishnamurthy
| length4         = 5:32
| title5          = In Aankhon Ka Tu Tara - II
| extra5          = Kavitha Krishnamurthy
| length5         = 3:23
| title6          = Aayi Holi
| extra6          = Udit Narayan
| length6         = 2:39
}}

;Track listing (Telugu) 
{{tracklist
| collapsed       = yes
| all_music       = Ilaiyaraaja
| all_lyrics      =
| extra_column    = Artist(s)
| title1          = Yamuna Thatilo (sad)
| extra1          = Swarnalatha
| length1         = 1:22
| title2          = Chilakamma Chitikeyanga
| extra2          = S. P. Balasubrahmanyam, K.S. Chitra
| length2         = 7:10
| title3          = Sundari Nuvve
| extra3          = S. P. Balasubrahmanyam, K. S. Chitra
| length3         = 7:14
| title4          = Singarala
| extra4          = S. P. Balasubrahmanyam, K. J. Yesudas
| length4         = 5:32
| title5          = Mudda Banthi Puvvulo
| extra5          =  Mano (singer)|Mano, Swarnalatha
| length5         = 5:00
| title6          = Ada Janmaku
| extra6          = P. Susheela
| length6         = 3:23
| title7          = Yamuna Thatilo Chorus
| length7         = 1:47
}}

;Track listing (Malayalam)
{{tracklist
| collapsed       = yes
| all_music       = Ilaiyaraaja Vaali
| extra_column    = Artist(s)
| title1          = Yamunayaattile"(Sad)
| extra1          = K. S. Chithra
| length1         = 1:22
| title2          = Raakkamma
| extra2          = M. G. Sreekumar, Swarnalatha
| length2         = 7:10
| title3          = Sundari Kannal
| extra3          = S. P. Balasubrahmanyam, S. Janaki
| length3         = 7:14
| title4          = Kattukuyilin Manasinullin
| extra4          = M. G. Sreekumar, KG Markose
| length4         = 5:32
| title5          = Putham Puthu Poo
| extra5          = Yesudas, S. Janaki
| length5         = 5:00
| title6          = Kannippaal Nilaa Vettam
| extra6          = K. S. Chitra
| length6         = 3:23
| title7          = Seethaakalyaana Vaibhogame Chorus
| length7         = 1:47
}}

==Release==

===Reception===
Reviews for Thalapathi have been positive. On 8 November 1991, The Hindu said, "Moving his pieces with the acumen of an international grandmaster, the director sets a hot pace".  IndiaGlitz said "Manirathnams excellent direction, and Ilayaraja supplementing with spellbound music makes this movie a definite watch."  Behindwoods said, "In Dalapathi,   although good ultimately prevails over evil, the journey is portrayed with the obstacles and difficulties. That is the greatest plus for the film." Rajinis performance was praised stating that it "is another moviewhich brought out the serious and highly skilled actor in Rajini.   Film historian Randor Guy called it a "superbly crafted movie", saying "Even though Rajinikanth is the main protagonist, the movie is entirely the film-makers."   

===Awards===
;39th Filmfare Awards South Best Director – Tamil – Mani Ratnam Best Music Director – Tamil – Ilaiyaraaja

==Remake== Dil Se. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-
! Year
! Film
! Language
! Cast
! Director
|-
| 2003
|Annavru (2003 film)|Annavru Kannada cinema|Kannada
| Ambareesh, Darshan (actor)|Darshan, Suhasini Maniratnam|Suhasini, Kaniha
| Om Prakash Rao
|}

==Legacy== Raja Rani (2013) cites Thalapathi was the main inspiration for him to consider cinema as a career.  Soundarya Rajini said that Rajinis hairstyle in Kochadaiiyaan (2014) was inspired by his appearance in Thalapathi. 

==In Popular Culture==
In a comedy scene from Suriyan (1992), Ramasamy (Goundamani) sings "Rakkamma" while walking through forest.  In Guru En Aalu (2009), Azhagappan (Vivek) who is in a drag imitates Shobanas portion from the song. 

==References==

===Footnotes===
 

===Bibliography===
 
*  
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 