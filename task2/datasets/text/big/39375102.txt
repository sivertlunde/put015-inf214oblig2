Two Sinners
{{Infobox film
| name           = Two Sinners
| image          = 
| image_size     = 
| caption        = 
| director       = Arthur Lubin
| producer       = Trem Carr
| writer         = Warwick Deeping Jefferson Parker
| narrator       = 
| starring       = Otto Kruger Martha Sleeper Minna Gombell Ferdinand Munier Cora Sue Collins Margaret Seddon Harrington Reynolds
| music          = 
| cinematography = Harry Neumann
| editing        = Jack Ogilvie
| studio = Monogram Pictures
| distributor    = Republic Pictures
| released       = 1935
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Two Sinners is a 1935 film directed by Arthur Lubin. 

==Production==
The working title of Two Sinners was Two Black Sheep, the title of the 1933 Warwick Deeping novel on which it was based. 

==Release==
Two Sinners was released as a second feature in some U.S. theaters alongside the Kay Francis vehicle The Goose and the Gander. 

==References==
 

==External links==
*  at IMDB

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 