Back to the Sea
 

{{Infobox film
| name           = Back to the Sea
| image          = 
| alt            = 
| caption        = 
| director       = Thom Lu
| producer       = Calvin Yao
| writer         = Thom Lu James D. Mortellaro Calvin Yao
| starring       = Yuri Lowenthal Kath Soucie Tom Kenny Mark Hamill Tim Curry Christian Slater
| music          = Gordon McGhie
| cinematography = Ono Yao
| editing        = Thom Lu
| studio         = Glory & Dream Entertainment Funimation
| distributor    = IndustryWorks Pictures Viva Pictures
| released       =  
| runtime        = 96 minutes
| country        = China
| language       = Chinese English
| budget         =
| gross          = 
}} Chinese 3D 3D traditional traditional animated family comedy film. It was released on January 27, 2012 in Canada and November 16, 2012 in the United States.

==Plot==
Flying fish Kevin and his family and friends live happy and carefree life in the depths of Atlantic. But Kevin has a cherished dream - to get to Barbados island, to the flying fish kingdom. Thirst for adventures leads the hero to the Rock of Fame where a family treasure - a huge pearl is hidden. Kevin gets captured by fishers and then finds himself in an aquarium of a popular Chinese restaurant. It seems, that his fate is decided, but suddenly a small boy comes to the aid to Kevin.
==Cast==
*Yuri Lowenthal as Kevin
*Kath Soucie as Shaobao
*Tom Kenny as Ben
*Mark Hamill as Bunker
*Matthew Yang King as Dabao
*Tim Curry as Eric 
*Christian Slater as Jack 
*Tara Strong as Sammy 
*James Sie as Cook Liu
*Annie Mumolo as Danny
*Nolan North as Farley

==Release== 3D and regular "2D" formats on January 27, 2012. It got a limited release in the United States on November 16, 2012, distributed by Viva Pictures.

==Music==
The films score is composed by Gordon McGhie. This film features the popular song What a Wonderful World from Louis Armstrong.

==References==
 

==External links==
* 
* 
* 
*  on Behind the Voice Actors

 
English-language films
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 