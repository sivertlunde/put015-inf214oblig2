Jennifer on My Mind
{{Infobox film
| name           = Jennifer on My Mind
| image          = Jennifer on My Mind poster.jpg
| caption        = theatrical poster
| director       = Noel Black
| producer       = Bernard Schwartz
| screenplay     = Erich Segal
| based on       = Heir&nbsp;by  
| starring       = Michael Brandon Tippy Walker Steve Vinovich Chuck McCann Peter Bonerz Renée Taylor Lou Gilbert Robert DeNiro Bruce Kornbluth Michael McClanathan Barry Bostwick Jeff Conaway
| music          = Stephen J. Lawrence
| cinematography = Andrew Laszlo
| editing        = John W. Wheeler
| studio         = Bernard Schwartz Productions Joseph M. Schenck Productions    
| distributor    = United Artists
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Jennifer on My Mind  is a 1971 American black comedy film based on the 1968 novel Heir by Roger L. Simon. It was directed by Noel Black from a screenplay by Erich Segal and stars Michael Brandon and Tippy Walker, as well as Robert De Niro in a minor role.
 addiction following the explosion of recreational drug use in the 1960s. 

==Plot== hippie girlfriend overdose in his living room. Fearing that he might be arrested for her death, he decides to dump her body, but not before his sister Selma (Taylor) and her psychologist boyfriend Sergei (Bonerz) -- whom Marcus detests -- pay him a visit. They notice his strange behavior and suspect that Marcus has gotten himself into trouble. They search his apartment, but to no avail.
 flashback shows Oyster Bay, where they briefly date until Jennifer again cuts all ties.

Heartbroken, Marcus leaves for Vienna, but returns to court Jennifer on her birthday, and is shocked to find out that she is injecting heroin with two minstrels (Bostwick and Conaway). Marcus scares away the two guys and attempts to save Jennifer as she, under influence, jumps from her rooftop.

Fearing that she might become a heroin addict, Marcus convinces Jennifer to return to Venice, but she grows bored, expressing her desire to return to New York. As he visits the ghetto, she leaves a note explaining that she cannot be with him. Marcus moves to New Jersey to clear his mind. One day, Jennifer suddenly shows up on his doorstep, telling Marcus that she has traveled the world and still has not found her luck in life. She asks to stay at his place for a while. 

As he cooks her dinner, Jennifer injects herself with heroin and becomes manic. She begs Marcus to inject her once more, but this leads to a fatal overdose. Advised by his friend Ornstein (Vinovich), Marcus puts her body in his car trunk and plans on dumping her body in the river, but he is almost caught by a man helping him with his flat tire and by three Hells Angels bullying him. While being chased by some joyriders, his car crashes and catches fire. Marcus watches it burn with Jennifers body still in the trunk.

==Cast==
*Michael Brandon as Marcus Rottner
*Tippy Walker as Jennifer DaSilva
*Steve Vinovich as Sigmund Ornstein
*Lou Gilbert as Grandpa Max Rottner
*Chuck McCann as Good Samaritan
*Peter Bonerz as Sergei
*Renée Taylor as Selma Rottner
*Bruce Kornbluth as Larry Dolci
*Robert DeNiro as Mardigian, Cab Driver
*Michael McClanathan as Hells Angel #1
*Allan F. Nicholls as Hells Angel #2
*Ralph J. Pinto as Hells Angel #3
*Barry Bostwick as Minstrel #1
*Jeff Conaway as Minstrel #2
*Kim Hunter as Jennifers Mother (scenes deleted)
*Nick Lapadula as Motorcycle Cop
*Leib Lensky as Cantor at Synagogue
*Ketti Prosdocimo as Little Girl at Synagogue
*Lino Turchetto as Singing Gondolier
*Rehn Scofield as Hearse Passenger
*Alfredo Michelangeli as Hotel Concierge

==Production==
A Los Angeles Times reviewer speculated that either Simon or Segal was inspired by the true 1966 case of Robert Friede, a twenty-five-year-old Annenberg publishing heir, and Celeste Crenshaw, a drug-addicted, nineteen-year-old socialite whose corpse was found in Friedes car. Friede was the son of Evelyn Annenberg Hall and her first husband, Kenneth Friede.  Crenshaw and Friedes story is told in the book Turned On: The Friede-Crenshaw Case (1967) by Dick Schaap. 

Based on the 1968 novel by Simon, the films working title was Heir.    The rights of the novel were initially acquired by Anthony Spinner and Barry Shear in October 1968.  In June 1969, it was announced that Herb Jaffe of United Artists was set to produce the film in collaboration with Joseph M. Schenck Enterprises. 

Actress Kim Hunter filmed some scenes as the lead womans mother, but they were cut following a "disastrous preview" in San Francisco, which prompted the films makers to edit it several times.  The film marked the feature  debuts for Bostwick and Conaway.  It was shot on location in New York City, Venice and New Jersey. 

==Reception== Pretty Poison, this is a potpourri of disarming satire, black comedy and poignancy that creates a strangely haunting aura. Michael Brandon, as Marcus, is charmingly boyish and natural. Tippy Walker, as Jennifer, comes across with a bitchy ethereal allure." 

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 