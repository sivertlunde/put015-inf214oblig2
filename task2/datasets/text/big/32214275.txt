Ang Pinakamagandang Hayop sa Balat ng Lupa
{{Infobox film
| name           = Ang Pinakamagandang Hayop sa Balat ng Lupa
| image          = Ang Pinakamagandang Hayop sa Balat ng Lupa 1974.jpg
| image_size     = 200
| caption        = 
| director       = 
| producer       = 
| writer         = glecilyn roldan and Rafael Ma. Guerrero
| narrator       = 
| starring       = Gloria Diaz   Vic Vargas   and Elizabeth Oropesa    (See Cast below) 
| music          = 
| cinematography =  
| editing        =   
| distributor    = Gemini Films International
| released       = 1974
| runtime        = 
| country        = Philippines Tagalog
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 
Ang Pinakamagandang Hayop sa Balat ng Lupa (although "The Most Beautiful Animal in the World" is the official translation, the literal meaning of the title is "The Most Beautiful Animal on the Surface of the Earth") is a 1974 Tagalog-language film from the Philippines.  The story was written by Celso Ad. Castillo.  The screenplay was written by Rafael Ma. Guerrero.  The film starred Filipino actors Gloria Diaz (1969 Miss Universe title holder), Vic Vargas, and Elizabeth Oropesa.  The film was produced by Gemini Films International and was shot in Sicogon Island of Carles, Iloilo in the Visayas region of the Philippines.   

==Plot==
One stormy night, in a sea side village, a childless couple finds a mysterious beautiful woman in the beach without any consciousness, the next day the girl wakes up and introduces herself as Isabel. She narrated that she is an orphan and that the night before, she escaped from her uncle who tried to rape her. The couple took pity on her and let her stay in the quiet sea side town. The moment she steps outside, people immediately took notice of her beauty and allure. Soon enough men took notice of her. They start to shun the women in their lives, even the closest of friends are willing to kill each other just to have her. With these turn of events, Isabel realized that she could use her beauty for her own gain. One by one, she manipulated the men in town to kill the man who sexually assaulted her, even if the that man is their close friend. Because of what she did, she earned the ire of all the women in town. The only man that sees her true colors is Simon but ironically he is also drawn to her. Among all the men in town, Isabel truly loves Simon but Simon is already betrothed to a local girl, Saling, but even though Isabel knows this, she still seduced him to her arms until he himself, starts to abandon reason. Saling is furious of her and planned to attack Isabel with the towns women. They attacked her and left her half dead, but what they did only made the men in town pity Isabel. The men decided to leave the women in their lives. The womens families became destroyed because of her. Isabel is like the tempest that brought her. She single-handedly destroyed the peaceful town. The devotion of the people are now to her. With Simon hypnotized to Isabels spell and with no chance of getting him back, Saling committed suicide in the sea. Because of this, Salings mother lost her sanity. Even the town idiot killed his adoptive parent because he forbid him to see Isabel since he sees her as a bad influence.  Even her adoptive mother killed her adoptive father because he gave himself to her. The close friends sued each other because of jealousy that their former friends are close to the Isabel. Meanwhile, favors are continually given to her, that even the church are losing its congregation because they are now drawn to her as moth to flame, yet she still shows no remorse as if everything is normal. With these events, Simon finally sees that Isabel is like Eve, who brought evil unto the peaceful world or like Pandora, who opened chest full of death. He decided to leave and to lead a new life far from the town and escape the poison that is Isabel. But Isabel deems everything worthless without Simon. She hopes that she can lure him to him as she did with the others, she mistook the sound in Simons house as him. When she enters and she sees the town idiot who became more insane because of her. He is carrying a bag full of dynamite that Simons friends once used for illegal fishing. The dynamites exploded with the town idiot and Isabel inside. It turns out heaven itself interceded to rid of the earth of an animal such as her.

==1996  version==
The 1996 remake of Ang Pinakamagandang Hayop sa Balat ng Lupa was also written by Celso Ad. Castillo and was produced by Royal Era and Viva Films.  The 1996 film version starred Ruffa Gutierrez, Dindi Gallardo, and Dick Israel.   

==References==
 

==External links==
* , theme song for the film Ang Pinakamagandang Hayop sa Balat ng Lupa

 
 
 
 
 
 
 