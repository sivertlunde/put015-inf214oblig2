Yes or No?
{{Infobox film
| name           = Yes or No ?
| image          = Yes or No (1920) - Ad 1.jpg
| caption        = Ad for film
| director       = Roy William Neill
| producer       = Norma Talmadge Joseph Schenck
| writer         = Mary Murillo
| based on       = Yes or No by Arthur Goodrich
| starring       = Norma Talmadge Lowell Sherman Gladden James
| music          =
| cinematography = Ernest Haller
| editing        =
| distributor    = First National
| released       = June 28, 1920
| runtime        = 72 minutes
| country        = United States
| language       = Silent (English intertitles)
}} silent drama film directed by Roy William Neill and starring Norma Talmadge. It is based on the 1917 Broadway play Yes or No by Arthur Goodrich.  Talmadge and Joe Schenck produced the picture and released it through First National Exhibitors.  

It is preserved at the Library of Congress. 

==Cast==
*Norma Talmadge- Margaret Vane/Minnie Berry Frederick Burton - Donald Vane
*Lowell Sherman- Paul Derreck
*Lionel Adams - Dr. Malloy
*Rockliffe Fellowes - Jack Berry
*Natalie Talmadge - Emma Martin
*Edward Brophy - Tom Martin (as Edward S. Brophy)
*Dudley Clements - Horace Hooker
*Gladden James - Ted Leach

==References==
 

==External links==
*  
* 
* 

 

 
 
 
 
 
 
 


 