Sister-in-Law's Wet Thighs
{{Infobox film
| name = Sister-in-Laws Wet Thighs
| image = Sister-in-Laws Wet Thighs.jpg
| image_size = 
| caption = Theatrical poster for Sister-in-Laws Wet Thighs (2001)
| director = Tarō Araki 
| producer = 
| writer = Tadashi Naitō
| narrator = 
| starring = Ayumi Tokitō Moe Sakura Sachiko Maeno
| music = 
| cinematography = 前井一作
| editing = Shōji Sakai
| studio = Taro Pro
| distributor = OP Eiga
| released = July 3, 2001
| runtime = 60 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 2001 Japanese pink film directed by Tarō Araki. It won the award for Fourth Best Film at the Pink Grand Prix ceremony and Araki was awarded Best Director. 

==Synopsis==
A female adult video director in Tokyo awakens from a dream in which she was a landlady at a Onsen|hot-springs inn. Later, while scouting for a film location, she finds the inn she saw in her dream. 

==Cast==
* Ayumi Tokitō ( )   
* Moe Sakura ( )
* Sachiko Maeno ( )
* Masahiro Nishikawa ( )
* Jōmonjin ( )
* Takasama Kidachi ( )
* Kenichi Irizuki ( )

==Availability==
Tarō Araki filmed Sister-in-Laws Wet Thighs for his own Taro Pro production studio, and it was released theatrically in Japan by OP Eiga on July 3, 2001.  It was released to home video on February 22, 2002 as  .    OP Eiga gave Sister-in-Laws Wet Thighs a second theatrical release on October 21, 2004 under the title  . 

==Bibliography==
*  
*  
*  
*  

==Notes==
 

 
 
 
 
 

 
 

 
 
 
 
 
 


 
 