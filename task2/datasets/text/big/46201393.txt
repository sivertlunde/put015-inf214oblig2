Fresno (2015 film)
{{Infobox film
| name           = Fresno
| image          = 
| alt            = 
| caption        = 
| director       = Jamie Babbit
| producer       = {{plainlist|
* Andrea Sperling
* Stephanie Meurer
}}
| writer         = Karey Dornetto
| starring       = {{plainlist|
* Judy Greer
* Natasha Lyonne
* Aubrey Plaza
* Clea DuVall
}}
| music          = Nathan Matthew David
| cinematography = Jeffrey Waldron
| editing        = Suzanne Spangler production companies = {{plainlist|
* Gamechanger Films
* Leeden Media
* Lakeview Productions
* TALU Productions
* Andrea Sperling Productions
}}
| distributor    =
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
Fresno is a 2015 dark comedy that was directed by Jamie Babbit.  The film had its world premiere on 14 March 2015 at South by Southwest and stars Natasha Lyonne and Judy Greer as two sisters that find themselves in trouble after Greer accidentally kills someone. 

==Synopsis== housekeeper working at a local hotel in Fresno, California. Her sister Shannon (Judy Greer) is a sex addict that was recently forced to go into rehab after her addiction caused her to lose her job. As she now needs gainful employment, Shannon allows Martha to talk her into working at the hotel as well. However soon after she begins work Shannon accidentally kills one of the hotels guests and convinces Martha to help her dispose of the body. This is easier said than done, as their attempts result in pet cemetery owners Ruby (Allison Tolman) and Gerald (Fred Armisen) blackmailing them for hush money. Without any other options, Martha and Shannon must now commit a series of robberies to come up with the money.

==Cast==
* Judy Greer as Shannon
* Aubrey Plaza
* Clea DuVall
* Natasha Lyonne as Martha
* Ron Livingston as Edwin
* Allison Tolman as Ruby
* Fred Armisen as Gerald
* Molly Shannon as Margaret
* Jessica St. Clair as Kristen Malcolm Barrett as Eric
* Maria Olsen as Softball Player
* Michael Hitchcock as David
* William R Phillips as Scott
* Teresa R. Parker as Bartender (as Teresa Parker)
* John Roohinian as Noah

==Development and production==
While writing the script Dornetto partially drew upon her own past relationship with her sister, wondering, "What if I still lived in my same hometown and we were in this sort of a meshed relationship, like a co-dependent relationship?".    From there she began to add the films fictional elements such as Shannons sex addiction.  Filming was initially intended to take place in Cleveland, but due to budget issues Dornetto and Babbit had to shift the films setting to Fresno, a location they chose because it seemed like it was a city people wanted to get away from.  In August 2014, it was announced that Greer and Lyonne would portray the two central characters of Fresno, marking the second time Lyonne has worked with Babbit.  The two actresses stated that part of the reason that they chose to act in the film was because their characters were atypical to how they were traditionally cast, as Lyonne is usually cast as a "trainwreck" while Greer was usually the "levelheaded" character.     The actors were encouraged to engage in improvisation, though they stayed close to the script.  Influences included Welcome to the Dollhouse, Bottle Rocket, and Bridesmaids (2011 film)|Bridesmaids.   Shooting took place in Los Angeles. 

==Reception==
In a mixed review, Crave Online wrote that it "is full of gleeful raunchiness and a registered sex offender or two, but not much actual comedy."  The Austin Chronicle wrote, "This heavy comedy, scripted by Karey Dornetto, delivers its expected yield of snappy and emotionally charged levity from a charismatic cast that also features Fred Armisen, Molly Shannon, and Aubrey Plaza in supporting roles. But as the storys centerpiece, Greers character ultimately gives the audience too little to root for. Insufferable and unrepentant until far too late, we dont feel conflicted like we should when her lone true ally finally says enough is enough. And although its hard to pry our eyes away from such a cool cast, by that time the feeling is mutual."  Variety (magazine)|Variety was also mixed in their opinion, as they felt that the work would likely not gather the cult following that Babbits 1999 film But I’m a Cheerleader received and that Fresno was overall "a mean-spirited farce whose strenuous bad taste seldom translates into actual laughs."   Slant Magazine wrote, "Brightly lit and cheerfully acted, Jamie Babbits Fresno pushes its not-so-funny premise almost to the breaking point, sacrificing character development on the altar of comedy along the way." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 