S&M Hunter
{{Infobox film
| name           = S&M Hunter
| image          = S&M Hunter.jpg
| image_size     =
| caption        = Theatrical poster 
| director       = Shuji Kataoka
| producer       = Daisuke Asakura
| writer         = Shuji Kataoka
| narrator       = 
| starring       = Shiro Shimomoto   Hiromi Saotome    Yutaka Ikejima   Ayu Kiyokawa
| music          = Hiroshi Akutagawa
| cinematography = Toshio Shimura
| editing        = Shōji Sakai
| studio         = Kokuei
| distributor    = Shintōhō (domestic)   Troma Entertainment (international) 
| released       = 1986-02 (Japan)
| runtime        = 59 minutes
| country        = Japan Japanese
| budget         = 
| gross          = 
}}
 comedy pink film directed by Shuji Kataoka and starring Shiro Shimomoto, Hiromi Saotome and Yutaka Ikejima. 

==Synopsis==
S&M Hunter ( ) is a super genius nawashi whose specialty is tying up women in order to tame them. When the sukeban gang The Bombers kidnap a man to use as their personal sex slave, his boyfriend asks S&M Hunter for help. S&M Hunter accepts the mission to defeat the gang. 

==Cast==
*Shiro Shimomoto: S&M Hunter
*Hiromi Saotome: Meg
*Yutaka Ikejima: Dungeon Master
*Ayu Kiyokawa: Machiko (Machi in the US release)
*Bunmei Tobayama: Saeki (Joe in the US release)
*Naomi Sugishita: Maria
*Akira Fukuda: Wataru (Jack in the US release)
*Mie Mogami: gang member
*Utako Sarashina: gang member

==Availability==
Shuji Kataoka filmed S&M Hunter for Kokuei and it was released theatrically in Japan by Shintōhō in February 1986.    The U.S premiere was September 2008 in Austin Fantastic Fest.  The second appearance of this film in the U.S. is at San Francisco Independent Film Festival. {{cite web |url=http://www.sfbg.com/entry.php?entry_id=7991&catid=110&volume_id=398&issue_id=417&volume_num=43&issue_num=19|title=
Hot pink|accessdate=2009-03-10|publisher= }}  Region 0 DVD by US-based company Pinkeiga has been released since January 14, 2009 in USA.         The film is also available as a digital download or streaming file on the   as of 2012.  

==Reviews==
*  
*  
*  
*  
*  
*  
*  
*  

==References==
 
*  

==External links==
*  
*  

 
 
 
 
 
 
 