Waking Ned
 
 
 
{{Infobox film
| name           = Waking Ned  
| image          = Waking Ned Devine.jpg
| image_size     =
| alt            =
| caption        = UK DVD cover Kirk Jones Richard Holmes Glynis Murray
| writer         = Kirk Jones David Kelly Fionnula Flanagan
| music          = Shaun Davey
| cinematography = Henry Braham
| editing        = Alan Strachan
| studio         = Canal+ Tomboy Films
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 91 minutes
| country        = United Kingdom France
| language       = English Latin Gaelic
| budget         = 
| gross          = 
}} Kirk Jones. David Kelly, Screen Actors Guild award for his role as Michael OSullivan. The film is set in Ireland, but was filmed on location in the Isle of Man. It was produced by Canal+ and the British studio Tomboy Films and distributed by the American company Fox Searchlight Pictures.

==Plot== David Kelly), Irish National Lotterys Lotto game, they, along with Jackies wife Annie (Fionnula Flanagan), plot to discover the identity of the winner. They obtain a list of lottery customers from Mrs. Kennedy (Maura OMalley) at the post office, and invite the potential winners to a chicken dinner, where they attempt to get the winner to reveal his or herself. After everyone has left and they are no closer to an answer, Annie realizes that one person did not come to the dinner, so Jackie pays a late-night visit to the only absentee: the reclusive Ned Devine (Jimmy Keogh). He finds Ned in his home in front of the TV, still holding the ticket in his hand, a smile on his face and dead from shock. That same night, Jackie has a dream that the deceased Ned wants to share the winnings with his friends, as he has no family to claim the ticket. Jackie wakes up after the dream, and before dawn, he and Michael return to Neds house to gather Neds personal information so they can claim the winnings for themselves.

Elsewhere in the village, Maggie OToole (Susan Lynch) continues to spurn the romantic interests of her old flame, "Pig" Finn (James Nesbitt), a local pig farmer. Finn is convinced they belong together, as he thinks he is the father of her son Maurice, but she cannot abide him due to his ever present odour of pigs. Finn has a rival in Pat Mulligan (Fintan McKeown), also hoping to marry Maggie.

Jackie and Michael call the National Lottery to make the claim, prompting a claim inspector to be sent. The inspector, Mr. Kelly, arrives to find Jackie on the beach and asks him for directions to Neds cottage. Jackie delays Kelly by taking him on a circuitous route while Michael races to the cottage on a motorcycle, completely naked, and breaks in so he can answer the door as Ned. After discovering that the lottery winnings are far greater than they anticipated (totaling nearly Irish Pound|IR£7 million), Jackie and Michael are forced to involve the entire village in fooling Mr. Kelly. All the villagers sign their name to a pact to participate in the ruse except one – the local curmudgeon, Lizzie Quinn (Eileen Dromey), who threatens to report the fraud in order to receive a ten-percent reward, and attempts to blackmail Jackie for £1 million of the winnings. Jackie agrees, but does not actually intend to pay her more than her fair share.

The villagers go to great lengths to fool the inspector, even pretending Neds funeral is a service for Michael when the inspector wanders into the church. As the inspector leaves, satisfied that the claim is legitimate, and the villagers celebrate their winnings at the local pub, Quinn makes her way to the nearest working phone, a phone box outside the village on the edge of a cliff, and phones the lottery office. Before she can report the fraud, however, the departing claim inspector sneezes while driving past her and loses control of his car, forcing an oncoming van to crash into the phone box, sending it plummeting off the cliff and crashing to the ground below with Quinn still inside.

At the celebration, Jackie spots Maggie, who is content that Finn is going to give up pig farming to marry her now that he can afford to. Jackie approves, adding that Maurice needs a father in his life. "More than seven million pounds?" she asks, nodding to her son. She then tells him "Ned does have a family, Jackie", implying that Ned was Maurices father. Jackie urges her to claim the entire fortune, but Maggie is sure that Maurice needs a father more and the villagers need the money. 

Finally, Jackie, Michael, Maurice, and several other villagers stand on a hill and raise their glasses to Ned, toasting him for his gift to the village.

==Production==
 
The film was shot on the Isle of Man, with the village of Cregneash standing in for the fictional Irish village of Tulaigh Mhór.

==Reception==
===Box office===
Waking Ned grossed £911,901 in the UK in its opening weekend  and £2.16 million internationally , for a grand total of £3.45 million worldwide. 

===Critical response===
Review aggregator Rotten Tomatoes gives the film a score of 83% based on 59 reviews. 

===Accolades===
  BAFTA Award for Most Promising Newcomer. The film was nominated for and won several other awards including the Screen Actors Guild, Satellite Awards, and the National Board of Review.

===Influence===
Waking Ned inspired a Bollywood blockbuster Malamaal Weekly directed by Priyadarshan. 

==References==
 

==Further reading==
* Waking Ned Devine: An Original Screenplay by Kirk Jones (1999) ScreenPress Books,

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 