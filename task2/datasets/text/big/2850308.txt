B*A*P*S
 
 
{{Infobox film
 | name        = B*A*P*S
 | image       = BAPsFilmPoster.jpg
  | writer      = Troy Beyer
 | starring    = Halle Berry Martin Landau Robert Townsend
 | producer    = Jay Stern
 | distributor = New Line Cinema
 | released    = March 28, 1997
 | runtime     = 91 minutes
 | country     = United States
 | language    = English
 | music       =
 | awards      =
 | budget      = $10,000,000 
 | gross       = $7,338,279 
|}} Robert Townsend, and starring Halle Berry and Martin Landau. The movie includes actors such as Bernie Mac, Natalie Reid, and Ian Richardson. In the 1998 Acapulco Black Film Festival, Halle Berry was nominated for best actress for her work in the film.

==Plot==
 Georgia (a suburb of Atlanta). Their dream is to open the worlds first combination hair salon and soul food restaurant.
 audition for a music video in Los Angeles. Through an unusual turn of events, they end up assisting a Beverly Hills butler and caring for an aging millionaire who welcomes them into his mansion. They become Black American Princesses (BAPs) "livin large and takin charge!" amongst the rich and famous.

They soon find themselves in the middle of a plot to milk the millionaire of his fortune.

==Cast==
*Halle Berry&nbsp;– Nisi
*Martin Landau&nbsp;– Mr. William Blakemore
*Ian Richardson&nbsp;– Manley
*Natalie Desselle-Reid&nbsp;– Mickey
*Troy Beyer&nbsp;– Tracy Shaw
*Luigi Amodeo&nbsp;– Antonio
*Jonathan Fried&nbsp;– Isaac
*Pierre Edwards&nbsp;– Ali Anthony Johnson&nbsp;– James
*Bernie Mac&nbsp;– Mr. Johnson
*Faizon Love&nbsp;– Tiger J
*Rudy Ray Moore&nbsp;– Nate

==Awards and nominations== 1998 Acapulco Black Film Festival
*Best Actress&nbsp;– Halle Berry (nominated)

==Reception==

The reception was overwhelmingly negative. It has a 13% fresh rating on Rotten Tomatoes. Roger Ebert gave the film a rare no-stars rating, calling it "jaw-droppingly bad." 

==References==
 

 
==External links==
* 
* 

 

 
 
 
 
 
 
 

 