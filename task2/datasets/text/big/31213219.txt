Black Snow (film)
 
{{Infobox film
| name           = Black Snow
| image          = Black Snow (film).jpg
| caption        =  Xie Fei
| producer       = Yongxin Li
| writer         = Liu Heng
| starring       = Cai Hongxiang
| music          = 
| cinematography = Xiao Feng
| editing        =  Beijing Youth Film Studio    
| distributor    = Second Run DVD (UK)
| released       =  
| runtime        = 107 minutes
| country        = China
| language       = Mandarin
| budget         = 
}}
 Xie Fei. Twelve Terrestrial Branches come round again. 
==Plot summary== Cultural Revolution, Li Huiquan, is released from labor camp. He arrives back in his native Beijing to find that he has no family or prospects or friends, just his underworld contacts trying to drag him back into a life of crime.        His attempts to make good are continually thwarted. His street stall selling clothes puts him on the fringe of the black market, and he soon gets lured back into his old neighbourhood gangs. His disenchanted comrades include a nightclub chanteuse as well as an escaped convict.    The films lurking handheld camera visually presents realistic footage of a man destined for the past from which he left behind.   

==Cast==
* Cai Hongxiang as Cui Yongli
* Jiang Wen as Li Huiquan, nickname Quanzi
* Lin Cheng as Zhau Yaqiu    
* Yue Hong
* Meng Jin
* Liu Xiaoning 
* Liang Tian
* Fang Zige
* Yue Hong   

==Reception==
===Awards=== Berlin International 1990
**Fei Xie Silver Berlin Bear (won)
**Fei Xie Golden Berlin Bear (nominated) 1990
**Best Film (won) tied with Jue zhan zhi hou (1991) 

==DVD release==
In 2010, Black Snow was released as a DVD  in a new High-Definition transfer and a booklet featuring essay by author and academic Professor Shaoyi Sun.  It includes a 32 minute interview with Xie Fei, in which the director discusses a variety of topics from his training in cinema, to the Cultural Revolution and his approach to teaching filmmaking.   

==References==
 

==External links==
* 
*  - Official DVD release company

 
 
 
 
 
 
 