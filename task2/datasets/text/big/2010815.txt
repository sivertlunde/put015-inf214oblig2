How to Make a Monster (2001 film)
 
{{Infobox film
| name           = How to Make a Monster
| image          = How to Make a Monster DVD.jpg
| caption        = DVD cover for How to Make a Monster George Huang
| producer       = Lou Arkoff Colleen Camp Stan Winston George Huang
| narrator       =
| starring       = Clea DuVall Steven Culp Tyler Mane Jason Marsden Karim Prince
| music          = David Reynolds
| cinematography = Steven Finestone
| editing        = Daniel Cahn Kristina Trirogoff
| distributor    = Columbia TriStar
| released       =  
| runtime        = 91 mins
| country        = United States English
| budget         =
| gross          =
}} Creature Features series of film remakes produced by Stan Winston. Julie Strain made a cameo appearance in the film as herself. How To Make A Monster debuted on October 14, 2001 on Cinemax. In 2005, it was nominated for a Hollywood Makeup Artist Award and Hair Stylist Guild Award.

==Plot== final boss.

Sol inserts his new AI chip into the mainframe and begins to play the game. As he advances to the games third level, the motion capture suit connected to the computer network comes alive. Simultaneously, the suit attacks Sol in real life while an in-game monster drags Sols video game character away. The following morning, Bug and Hardcore discover that Sol has been killed and the backup CD has been stolen. Hardcore attempts to review the security camera tapes with his PDA, but accidentally touches the motion capture suit, which has merged itself with Sols dead body. The suit attacks Hardcore; although he initially manages to fight it off, he is later killed. The suit then merges itself with Hardcores muscular body and takes all of his weapons, giving it a striking resemblance to the monster in the game. Bug theorizes that the lighting strike combined with Sols powerful AI chip has caused the suit to believe the real world is part of the video game. Bug, Laura and Drummond decide to pull the computers plug, although doing so will likely erase all of the games data.

While Bug tries to figure out which set of wires to pull, Drummond is attacked by the monster in Hardcores body. With Bugs help, Drummond manages to escape, but the security system malfunctions, seals all exits, and leaves Laura, Bug, and Drummond in near darkness. Bug travels through ceiling shafts and falls into the kitchen area, where he is attacked by the monster. As the monster draws closer, Bug exposes a gas line and lights his lighter, killing both himself and the monster. The monster returns to the motion-capture suit and attacks Drummond, but Laura saves him by virtually fighting the in-game monster. Laura later tries to beat the game but becomes frustrated and hysterical. Drummond suggests that she try a virtual reality headset, promising to stay with her while she fights. However, in the midst of the game, Laura realizes Drummond has left her. The monster appears and, in the real world, Laura escapes to the kitchen. There she finds a PDA displaying a video of Drummond stealing the games backup CD the night of Sols death; had Drummond not stolen it, the CD would have allowed the programmers to reverse compile the game and shut down the monster. Laura finds and confronts Drummond at gun point, forcing him to drop the CD. Drummond makes a speech stating that everyone is ultimately a monster. Laura shoots him in the knee and allows the monster to kill him. Laura then dons the VR headset and gloves, acquires a sword, and simultaneously battles the monster in both the real and virtual world. In the real world, she lures the suit toward a fish tank and electrocutes it with the water inside. She then stabs the monster with her sword, finally killing the monster.
 CEO of the company, which is renamed Wheeler Software.

==Cast==
*Clea DuVall as Laura Wheeler, the kind, 24-year-old intern of Clayton Software. Eventually, Laura kills the monster and becomes a ruthless CEO.
*Steven Culp as Peter S. Drummond, a hardened businessman at Clayton Software. He is revealed to have stolen the copy of the evil monster for the game.
*Tyler Mane as Hardcore, the muscular developer who is responsible for designing the games weapons for motion capture sessions.
*Jason Marsden as Bug, the developer who creates the sound and music for the game.
*Karim Prince as Sol, the developer who programs the games artificial intelligence.
*Julie Strain as Herself. Strain makes a cameo appearance in the film when she arrives for a motion-capture session.
*James Sullivan as the Monster, who is brought to life through a motion capture suit as a result of a lightning strike.
*Colleen Camp as Faye Clayton, the head of the computer software company Clayton Software.
*Danny Masterson as Jeremy, the abusive boyfriend of Laura Wheeler.

==DVD release==
How to Make a Monster was released on DVD on June 11, 2002. The film is presented in anamorphic widescreen and its audio is presented in 5.1 surround sound in both English and French. Extra features include a "making-of" featurette, photo galleries of drawings and behind-the-scenes images, and theatrical trailers for other Columbia TriStar horror films. The DVD also includes DVD-ROM content for personal computers.

==Further reading==
Staiger, Michael. "Evilution - Die Bestie aus dem Cyberspace". In "film-dienst" (Germany), Vol. 55, Iss. 15, 16 July 2002, Pg. 32

==External links==
*  
*   at Allmovie
*  
*   

 

 
 
 
 
 
 
 
 
 
 
 
 