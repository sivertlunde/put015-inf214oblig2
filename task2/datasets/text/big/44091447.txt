Aval Oru Devaalayam
{{Infobox film 
| name           = Aval Oru Devaalayam
| image          =
| caption        =
| director       = AB Raj
| producer       = RS Sreenivasan
| writer         = VP Sarathy Cochin Haneefa (dialogues)
| screenplay     = VP Sarathy
| starring       = Prem Nazir Jayan Sheela Jayabharathi
| music          = M. K. Arjunan
| cinematography = PB Mani
| editing        = BS Mani
| studio         = Sree Sai Productions
| distributor    = Sree Sai Productions
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by AB Raj and produced by RS Sreenivasan. The film stars Prem Nazir, Jayan, Sheela and Jayabharathi in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Prem Nazir
*Jayan
*Sheela
*Jayabharathi
*Jose Prakash
*Manavalan Joseph
*Sreelatha Namboothiri
*Cochin Haneefa
*Maniyanpilla Raju
*Prathapachandran
*Bahadoor GK Pillai
*Mallika Sukumaran
*PR Varalekshmi
*Paravoor Bharathan
*Poojappura Ravi
*T. P. Madhavan
*Vanchiyoor Radha
*Vijay Shankar
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Bharanikkavu Sivakumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aashaane Namukku Thodangam (Maniyan Chettikku) || CO Anto, Zero Babu || Bharanikkavu Sivakumar || 
|-
| 2 || Bhoomithan Pushpaabharanam || K. J. Yesudas || Bharanikkavu Sivakumar || 
|-
| 3 || Dukhathin Mezhuthiri || Jency, LR Anjali || Bharanikkavu Sivakumar || 
|-
| 4 || Naarayanakkili || P Susheela, Chorus, Jency || Bharanikkavu Sivakumar || 
|-
| 5 || Njanoru Shakthi || P Susheela || Bharanikkavu Sivakumar || 
|-
| 6 || Pandu Pandoru Chithira || P Susheela || Bharanikkavu Sivakumar || 
|}

==References==
 

==External links==
*  

 
 
 

 