Wallander – Tjuven
{{Infobox film
| name           = Wallander – Tjuven
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Stephan Apelgren
| producer       = Malte Forssell
| writer         = Lars Lundström
| narrator       = 
| starring       = Krister Henriksson Lena Endre Sverrir Gudnason Nina Zanjani
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Wallander – Tjuven is a 2009 film about the Swedish police detective Kurt Wallander directed by Stephan Apelgren.

== Synopsis ==
Some homes are burgled and a vigilante group is formed. Soon Wallander is convinced that a double murder has occurred, although no bodies have been found.

== Cast ==
*Krister Henriksson as Kurt Wallander
*Lena Endre as Katarina Ahlsell
*Sverrir Gudnason as Pontus
*Nina Zanjani as Isabelle
*Fredrik Gunnarsson as Svartman
*Mats Bergman as Nyberg
*Douglas Johansson as Martinsson
*Stina Ekblad as Karin
*Henny Åman as Hanna, Katarinas daughter
*Jacob Ericksson as Olle
*Shanti Roney as Ralf
*Kalle Westerdahl as Peter
*Karin Lithman as Anne
*Kola Krauze as Jarek
*Vera Veljovic-Jovanovic as Maja
*Harald Leander as Journalist

== References ==
* 

== External links ==
* 

 

 
 
 
 
 


 
 