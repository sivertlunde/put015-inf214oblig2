Horror Stories 2
{{Infobox film
| name           = Horror Stories 2
| image          = File:Horror_Stories_2_poster.jpg
| director       = Min Kyu-dong   Kim Sung-ho   Kim Hwi   Jung Bum-sik
| producer       = Min Jin-su   Kim Won-guk   Yoo Jae-hyeon
| writer         = Min Kyu-dong   Kim Sung-ho   Kim Hwi   Jung Bum-sik Kim Ji-won
| music          = Jeong Yong-jin   Yeon Ri-mok   Na Yun-sik   Lee Jin-hui
| cinematography = Kim Hyeong-ju   Lee Jae-hyeok   Kim Young-min   Jung Seong-wook
| editing        = Son Yeon-ji   Eom Yun-ju   Shin Min-kyung   Kim Hyeong-ju
| studio         = Daisy-Cinergy Entertainment Soo Film
| distributor    = Lotte Entertainment
| released       =  
| runtime        = 95 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| admissions     = 
| gross          = 
| film name      = {{Film name
| hangul         = 무서운 이야기 2 
| hanja          = 
| rr             = Mu-seo-un Iyagi 2 
| mr             = }}
}}
 omnibus film made up of four episodes by four South Korean directors.  It screened at the Puchon International Fantastic Film Festival and Sitges Film Festival in 2013,  and won the Silver Raven prize in the International Competition at the 2014 Brussels International Fantastic Film Festival. 

Min Kyu-dongs 444 is set against the backdrop of the warehouse of an insurance company where a woman with the ability to communicate with the dead delves into fraudulent insurance claim cases. Kim Sung-hos The Cliff focuses on two friends who go hiking in the woods then get trapped at the edge of a cliff. Kim Hwis The Accident centers on three depressed girls who go on a road trip after they fail the teachers certification exam, but a car accident turns their trip into a nightmare. Jung Bum-shiks The Escape is about a male trainee teacher who gets locked in the doorway to hell.  
 Horror Stories, a film with a similar format which was released in 2012. 

==Stories==

===444===
*Plot: Se-young, an outsider at an insurance company, has the extraordinary ability to communicate with dead people. Manager Park, Se-youngs boss, assuming that she has some kind of special power, decides to test it. One very late night when everyone has gone home, Mr. Park brings Se-young to a storage room full of case reports. Among them, Mr. Park picks three doubtful cases. He asks Se-young to tell him exactly what happened. Se-young, who senses some dark aura around him, tells the story of each case. Every time Se-young finishes each case, the dark aura gets darker and bigger and Se-young tries to warn Mr. Park about it. 
*Directed by Min Kyu-dong
*Lee Se-young ... Se-young
*Park Sung-woong ... Manager Park

===The Cliff===
*The Cliff ( ) (Running time: 23 minutes)
*Plot: Two friends luckily survive after falling from a cliff, but end up on a protruding rock just below. While waiting to be rescued, their friendship soon breaks apart because of one chocolate candy bar. To survive, one of them has to die. (Adapted from Oh Seong-daes popular webtoon The Cliff.)    
*Directed by Kim Sung-ho
*Sung Joon ... Dong-wook 
*Lee Soo-hyuk ... Sung-kyun
*Noh Kang-min ... young boy in apartment playground

===The Accident===
*The Accident ( ) (Running time: 23 minutes)
*Plot: Three young women fail their teacher certification examination. To cheer themselves up, the friends set out on a road trip into the mountains. But they get into an accident and their car breaks down. Despite their injuries, they decide to walk toward a dim light coming from a mountain. 
*Directed by Kim Hwi
*Baek Jin-hee ... Kang Ji-eun
*Kim Seul-gie ... Yoon Mi-ra
*Jung In-sun .... Gil Sun-joo
*Kim Gi-cheon ... resident of mountain cabin

===The Escape===
*The Escape ( ) (Running time: 30 minutes)
*Plot: Byeong-shin is a trainee teacher who gets humiliated by his students on his first day at school. After meeting Tan-hee, a high school girl obsessed with black magic, he imitates one of her spells. He finds himself locked in the doorway to hell.
*Directed by Jung Bum-sik
*Go Kyung-pyo ... Go Byeong-shin Kim Ji-won ... Sa Tan-hee
*Im Won-hee ... teacher Kim Ye-won ... Byeong-shins girlfriend

==References==
 

==External links==
*    
*    
*    
*  
*  
*  

 
 
 
 
 
 
 