Twin Rivers (film)
 
Twin Rivers is a 2007 South Australian feature film directed and written by Matthew Holmes and co-written by South Australian author Meredith Resce and Peter Court. It stars Darren Holmes, Matthew Holmes, Robyn Dickinson, Joshua Jaeger, Hakan Magill and Jonathan Western.

Set in the Depression era|Depression-ravaged Australian outback of 1939 Australia, two brothers Thomas Norton (Matthew Holmes) and William Norton (Darren Holmes) embark on a 500 mile journey on foot across south-eastern Australia to Melbourne. When their finances are stolen by a fellow traveler, the two brothers find themselves working in the township of Riverton. New friendships and romance challenge the brothers loyalty to each other and their original plans, which leads to a bitter separation.
 DeReel Independent Film Festival and the 2009 South Australian Screen Awards.

== Plot ==
 Broken Hill swaggies and angora rabbit fur trade when they arrive. The older brother Thomas is the driving force behind the journey, while William is a passive follower. However, their money is stolen by a fellow traveler called Bertie (Jonathan Western), who disappears quickly after. In searching for him, they discover a sick man on the road, Mr. Carmody (Oscar Peters) and reluctantly decide to help him to the nearest town rather than keep looking for Bertie.

Arriving at a small township called Riverton, they are immediately embraced by the sick mans estranged family, including a young girl called Alice (Robyn Dickinson) who takes a special interest in the younger brother, William. They find work at a local sheep station run by the kindly Robert Locchel (Hakan Magill). William forms a special bond with one of the workers at the station, Jack (Joshua Jaeger) whose larrikan ways and outlook on life appeal to William immediately. Thomas soon becomes restless and wants to continue on the journey, especially when he sees his younger brother forming a romantic attachment to Alice. The brothers become increasingly distant and short-tempered with each other, which leads to a violent quarrel. Thomas leaves the sheep station and William decides to stay on as one of the workers.

On the road, Thomas comes across Bertie in one of the towns and ambushes his camp one night, beating him severely and trying to recover their stolen money. He quickly discovers that Bertie has since spent it all and Thomas regrets the incident. he becomes increasingly more ill on the journey due the weather and lack of clean water. Meanwhile, William saves his friend Jack from a suicide attempt after he learned of the death of a former lover he got pregnant. William decides to restore his relationship with his brother and leaves Riverton. He reunites with Thomas on the road, but finds him wracked with fever. William does his best to get him help, but Thomas dies one night and William buries him in the bush. Grief-stricken, he returns to Riverton to be with Alice and his friends and start a life of his own.

== Cast ==
Darren Holmes as William Norton.

Matthew Holmes as Thomas Norton.

Robyn Dickinson as Alice Carmody.

Joshua Jaeger as Jack Humphries.

Hakan Magill as Robert Locchel.

Jonathan Western as Bertie.

Oscar Peters as Mr. Carmody.

Meredith Resce as Mrs. Carmody.

David Anderson as Frank.

Stewart Lindsay as Percy.

Ross Morgan as Ron.

Edwin Hodgeman as Voice of Old William

== Production ==
 Depression and was inspired by the true life accounts of Matthew Holmes great-grandmother Alice Norton. The script later expanded into a 60 min feature when South Australian author Meredith Resce joined the project as a co-writer and Associate Producer. Shooting on weekends and holidays, the shooting of Twin Rivers lasted for four years.  All cast and crew were volunteers, which contributed to its long production period. It had no financial backing and all costs were paid for by its director, Matthew Holmes.  During this time, the script was constantly rewritten and many scenes had to re-filmed when the plot changed quite dramatically from its original concept. By 2005, Twin Rivers had grown into a 2-hour feature film.

The project went into hiatus as it looked for financial backing or a distribution deal to complete the editing, sound and music. Both private investors and Australian film funding bodies turned down the project on several occasions. Between 2005 and 2006, director Matthew Holmes edited the film at his own costs. The unfinished project caught the attention of South Australian director Rolf De Heer (Bad Boy Bubby, Ten Canoes, The Tracker) and his long-time sound collaborator James Currie. Both men committed to lending assistance to the project in the form of equipment, advice and time. The film was completed in May 2007.

The documentary An Independent Epic - The Making of Twin Rivers was produced by the filmmakers in 2009, which chronicles the journey of making the film from beginning to end.

== Locations ==
 Mallee Regions Melrose a small township at the foot of Mount Remarkable, part of the Lower Flinders Ranges. Because it was a modern, functioning sheep station, the production had to carefully frame every shot to not reveal modern buildings and machinery in the backgrounds. Derelict remains of the original farmhouse were dressed up by the crew to look like shearers quarters. Many locals in the Melrose area offered properties, shearing sheds and buildings to the production that still looked faithful to the 1930s era.
 One Tree Hill and Blackwood. Interiors were filmed in various places throughout Adelaide, including the production studios at Anifex Pty Ltd, where the interior of the workers quarters were constructed.

The historical village Old Tailem Town outside Murray Bridge was chosen to represent the township of Riverton and also provided many interiors, such as shops and pubs.

== Trivia ==

The two brothers Thomas and William are played by real-life brothers. Thomas and William are the names of Matthew Holmes great-grandfather and grandfather.

The film was shot on standard definition 4:3 DVCAM format, which proved a challenge to picture quality during post-production when all televisions became High Definition and changed to the 16:9 format. The films entire soundtrack had to replaced in post-production, including all dialogue, sound effects and atmospheres. Only about 2% of original live sound appears in the film.

Over its six year production, director Matthew Holmes spent approximately A$20,000 of his own money on the project. It is estimated that the films budget wouldve been closer to A$1.5 million if all the volunteer cast and crew were paid industry award rates. Most of the extras in the film costumed themselves.

The script for Twin Rivers went through approximately 25 rewrites. Originally, the character of Thomas did not die and the character of Jack did not exist in the first few drafts. It wasnt until actor Joshua Jaegers performance so impressed the director that the expanded the character of Jack to a pivotal role. Due to the constantly shifting script, several scenes were re-shot - some up to three times.

The films original voice-over was spoken by a young William Norton. It was during the last phase of post-production that Rolf De Heer suggested a rewrite that changed the voice-over to William as an old man.

== Release ==
 Capri Cinema in Adelaide. Upon completion, it still did not have a distributor. Twin Rivers finally received distribution in early 2009 through Ovation. 

Critical reception
 The Advertiser called it “a wonderful, involving and truly Australian story... richly detailed and emotional. A beautiful and handsome production which captures atmosphere and characters of the period.”

Empire Magazine gave the Twin Rivers a four-star review, citing that it boasted "superior performances from an entirely amateur cast and a stunningly beautiful Australian landscapes that are almost a character in themselves. An independent epic that oozes more authenticity than most period films produced these days."

 
 