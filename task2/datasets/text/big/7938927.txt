The Hoose-Gow
 
{{Infobox film
| name           = The Hoose-Gow
| image          = Hoosegoetitlecard29.jpg
| caption        =
| director       = James Parrott
| producer       = Hal Roach
| writer         =
| starring       = Stan Laurel Oliver Hardy
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 20 07"
| language       = English
| country        = United States
| budget         =
}} short film starring Laurel and Hardy, directed by James Parrott and produced by Hal Roach.

==Synopsis== inmates at a prison after apparently taking part in a hold-up raid, a raid they tell a prison officer they were only watching. After attempting to escape and giving themselves up after the officer shoots them in the seats of their pants they are sent to dig ditches with other convicts on work detail. After sitting down to eat at the officers table and chopping down a tree being used as a watchtower, they accidentally puncture the radiator in the prison inspectors car with a pickaxe and then stupidly try to patch it with rice on the advice of another convict. The rice boils up and spews out after the engine is started. This starts a rice-throwing melee with the visiting governor and his party.

==Opening title==
Neither Mr. Laurel nor Mr. Hardy had any thoughts of doing wrong. As a matter of fact, they had no thoughts of any kind.

==Cast==
*Stan Laurel - Stan
*Oliver Hardy - Ollie
*Tiny Sandford - Warden
*Jimmy Finlayson - Judge Charlie Hall - Treetop Lookout
*Leo Willis - Leo
*Ellinor Vanderveer - Party guest
*Retta Palmer - Party guest
*Sam Lufkin - Prison camp officer
*Leo Sulky - Prison guard
*Dick Sutherland - Cook
*Eddie Dunn - Prisoner
*Chet Brandenburg - Prisoner
*Ed Brandenburg - Prisoner
*Baldwin Cooke - Prisoner
*Charles Dorety - Prisoner
*Ham Kinsey - Prisoner
*Tiny Ward - Prisoner
*Blackie Whiteford - Prisoner 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 

 