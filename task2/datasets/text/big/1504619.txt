Home Fries (film)
 
{{Infobox film
| name           = Home Fries
| image          = Home fries poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| alt            = 
| director       = Dean Parisot Mark Johnson Lawrence Kasdan Barry Levinson Charles Newirth
| writer         = Vince Gilligan
| starring       = Drew Barrymore Luke Wilson Catherine OHara Shelley Duvall Jake Busey
| music          = Rachel Portman
| cinematography = Jerzy Zielinski
| editing        = Nicholas C. Smith
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States English
| budget         = $15 million   
| gross          = $10,513,979 (US) 
}} Taylor and Bastrop, Texas. 

==Plot== Chris Ellis) orders a milkshake at the drive-thru. At the window, he tells the attendant, Sally Jackson (Drew Barrymore), that his wife knows about their affair. She asks him if he has also told his wife about her pregnancy. On his way home, he encounters a ferocious wind. It turns out to be an attack helicopter, which runs him off the road. In a panic, he flees through the woods and drops his heart medication. At an outdoor chapel, he sits on a bench as the helicopter hovers in front of him. The pilot, Angus Montier (Jake Busey), shoots at the ground near him despite the protests of his copilot and brother, Dorian (Luke Wilson). The shots scare him enough to cause a fatal heart attack.

Throughout their attack, Dorian and Angus can hear the chatter of Sally and her Burger-Matic coworkers. Likewise, they can hear the helicopter pilots on their headsets. The next day, the police inform Beatrice Lever (Catherine OHara) that Henry has died. She appears shocked and crestfallen, when Dorian and Angus arrive. It quickly becomes clear that she encouraged her sons to scare him to death. She is also furious about his affair, and wants revenge on his mistress. Angus and Dorian are worried that the people they heard on the radio might have overheard enough to connect them to his death. They quickly deduce that Burger-Matic is the only location close enough to have been on the same frequency. Angus goads Dorian into getting a job there to ensure that no one is wise to their crime.
 National Guard. She asks him to accompany her to lamaze class, since she doesnt have a partner. Eventually, he takes her to the base to see the helicopter that he flies. As she sits in the cockpit, she tells him about Henry.

Knowing that his mother is still furious about Henrys affair and that Angus would hurt Sally if he knew her identity, Dorian frantically tries to keep the truth from his family. When Angus discovers Sallys identity, Beatrice visits her under the pretense of making amends. Dorian is terrified of what Angus might do out of a misplaced loyalty to their crazy mother. Sure enough, he arrives at Sallys house in the attack helicopter. She, Dorian, and Beatrice escape in a truck. He eventually forces them to stop on the road. Beatrice pretends to be unaware of what is going on and leaves the truck. Dorian gradually convinces Angus to stop his attack.

The stress of the chase triggers Sallys labor, and Dorian drives her to the hospital. After she has a boy, he talks to him. He struggles to explain how they are related, and he tells him that he is lucky to have the best mother in the world.

==Cast==
* Drew Barrymore as Sally Jackson
* Catherine OHara as Beatrice Lever
* Luke Wilson as Dorian Montier
* Jake Busey as Angus Montier
* Shelley Duvall as Mrs. Jackson
* Kim Robillard as Billy
* Daryl Mitchell as Roy
* Lanny Flaherty as Red Jackson Chris Ellis as Henry Lever
* Blue Deckert as Sheriff
* Mark Walters as Deputy
* Shane Steiner as Soldier in Jeep
* Theresa Merritt as Mrs. Vaughan
* Jill Parker-Jones as Lamaze Instructor
* Morgana Shaw as Lucy Garland

==Critical reception==
The films received negative reviews from critics. It currently holds a 31% rating on Rotten Tomatoes.

Film critic Roger Ebert gave the film a mixed review, writing, "Home Fries is not a great movie, and as much as I finally enjoyed it, Im not sure its worth seeing two times just to get into the rhythm. More character and less plot might have been a good idea. But the actors are tickled by their characters and have fun with them, and so I did, too." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 