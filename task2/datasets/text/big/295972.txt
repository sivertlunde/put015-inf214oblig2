Thirty Two Short Films About Glenn Gould
{{Infobox film
| name           = Thirty Two Short Films About Glenn Gould
| image          = Thirty two short films about glenn gould poster.jpg
| producer       = Michael Allder Niv Fichman Barbara Willis Sweete Larry Weinstein
| caption        = Original Canadian theatrical release poster
| director       = François Girard
| writer         = François Girard Don McKellar
| starring       = Colm Feore Derek Keurvorst Katya Ladan
| music          =
| cinematography =
| editing        =
| distributor    = The Samuel Goldwyn Company
| released       =  
| runtime        = 98 min.
| language       = English/French
| budget         =
| gross          = $1,567,543 (USA)
}}
Thirty Two Short Films About Glenn Gould is a 1993 film about the pianist Glenn Gould played by Colm Feore. The films screenplay was written by François Girard (who also directed) and Don McKellar.

The film does not present a single narrative, rather a series of thirty-one short films. These include documentaries (five interviews with people who knew him), re-creations of scenes from Goulds life, and various odd items (such as "Gould Meets McLaren", in which animated spheres reminiscent of those in Norman McLarens animations move to Goulds music). The segments range in length from six minutes to less than one minute.

According to Girard: "As Gould was such a complex character, the biggest problem was to find a way to look at his work and deal with his visions. The film is built of fragments, each one trying to capture an aspect of Gould. There is no way of putting Gould in one box. The film gives the viewer 32 impressions of him. I didnt want to reduce him to one dimension." 

The soundtrack consists almost entirely of piano recordings by Gould. A notable exception is the overture to  , and the Well-Tempered Clavier, as well as others which are less so.
 Toronto International Film Festival.

The structure and style of the The Simpsons episode "22 Short Films About Springfield" (first aired April 14, 1996), is inspired by this film. It was also used in an Animaniacs short entitled "Ten Short Films About Wakko Warner".

==Segments==

# Aria
# Lake Simcoe
# Forty-Five Seconds and a Chair 
# Bruno Monsaingeon: musician and collaborator
# Gould Meets Gould: text by Glenn Gould
# Hamburg
# Variation in c minor
# Practice
# The L.A. Concert
# CD318
# Yehudi Menuhin: violinist
# Passion According to Gould
# Opus 1: a composition by Glenn Gould
# Crossed Paths
# Truck Stop
# The Idea of North: a radio documentary by Glenn Gould
# Solitude
# Questions with No Answers
# A Letter
# Gould Meets McLaren: animation by Norman McLaren
# The Tip
# Personal Ad
# Pills
# Margaret Pacsu: friend
# Diary of One Day
# Motel Wawa
# Forty-Nine
# Jessie Greig: cousin
# Leaving
# Voyager
# Aria
# End Credits

==References==
{{Reflist
| colwidth = 30em
| refs     =

 
{{cite web
| url         = http://www.academy.ca/hist/history.cfm?stitle=Thirty-Two+Short+Films+About+Glenn+Gould&awyear=0&winonly=0&awards=0&rtype=2&curstep=4&submit.x=39&submit.y=6
| title       = Canadas Awards Database
| first       = 
| last        = 
| author      = Genie Award
| date        = 
| month       = 
| year        = 
| work        =   
| publisher   = Canadian Academy of Recording Arts and Sciences
| format      = 
| archiveurl  = 
| archivedate = 
| accessdate  = 18 December 2011
| quote       = 
| postscript  = . 
}}
 

 
{{cite book 
|title=The Film journal
|url=http://books.google.com/books?id=AvzrAAAAMAAJ
|accessdate=29 March 2011
|date=1 January 1994
|publisher=Pubsun Corp.
|page=130
}}
 

}}

==External links==
* 
*  Script - Dialogue Transcript

 

 
 
 
 
 
 
 
 