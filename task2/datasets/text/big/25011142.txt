Bloodbrothers (1978 film)
{{Infobox film
| name           = Bloodbrothers
| image          = Bloodbrothers (1978 film).jpg
| image_size     = 
| caption        = 
| director       = Robert Mulligan Walter Newman
| based on       =   Stephen J. Friedman Kenneth McMillan
| music          = Elmer Bernstein
| cinematography = Robert L. Surtees
| editing        = Sheldon Kahn
| distributor    = Warner Bros. 
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $4 million
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}   novel of Richard Price. Best Adapted Screenplay.

==Plot==
Set in a working class community, it tells the story of the De Coco family, a family of construction workers. Louis is the head of the family with three sons, but one, Stoney (Richard Gere) wants to be a teacher, not a hardhat. Then he accepts a job as a recreational assistant at a childrens ward. Immediately, bitter divisions begin to surface.

==Cast==
* Paul Sorvino as Louis "Chubby" De Coco
* Tony Lo Bianco as Tommy De Coco
* Richard Gere as Thomas "Stony" De Coco
* Lelia Goldoni as Marie De Coco
* Yvonne Wilder as Phyllis De Coco
* Marilu Henner as Annette Kenneth McMillan as Banion
* Floyd Levine as Dr. Harris
* Kim Milford as Butler
* Michael Hershewe as Albert "Tiger" De Coco
* Lila Teigh as Jackies Mother
* Kristine DeBell as Cheri
* Robert Englund as Mott
* Gloria LeRoy as Sylvia
* Damu King as Chili Mac
* Paulene Myers as Mrs. Pitt
* Danny Aiello as Artie
* Raymond Singer as Jackie Bruce French as Paulie
* Peter Iacangelo as Malfie Eddie Jones as Blackie
* E. Brian Dean as Brian (credited as Brian Dean)
* Randy Jurgensen as Randy
* Ron McLarty as Mac
* David Berman as Dave
* Robert Costanzo as Vic (credited as Bob Costanzo)
* Edwin Owens as Stan (credited as Ed Owens)
* Tom Signorelli as Sig
* Kennedy Gordy as Tyrone
* Jeffrey Jacquet as Derek

==Response==
The movie opened to positive reviews, and though it would be forgotten about in later years, it was liked for the ensemble cast. As one of the De Coco sons, Richard Gere was especially praised. The film also introduced Marilu Henner, who would later star on the TV show Taxi (TV series)|Taxi.

Ryan McDonald of Shameless Self Expression said: "This 1978 Robert Mulligan tale about a seriously dysfunctional Italian-American family is too broadly played, stereotyped, and overly familiar... This is all very shouty and somewhat overbearing stuff for a story that isn’t all that memorable to begin with. ... Tony Lo Bianco and especially an unrestrained Lelia Goldoni are the worst offenders. Lo Bianco, often typecast as (an) Italian-American hood, gives us a stereotype of Italian-American machismo, misogyny, occasional brutality, and just general hamminess. Occasionally there seems to be a real character in there, but largely it’s just too much of a ‘performance’...  But at least he has his moments, which cannot be said for the ghastly Goldoni, whose shrieking, mugging, wailing ... coupled with a pathetic, basically psychotic character derail the film.  ... The most enjoyable work comes from Paul Sorvino, Marilu Henner, and Kenneth McMillan..."  

==Awards== Walter Newman) Walter Newman)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 