Meetin' WA
{{Infobox film
| name           = Meetin WA
| image_size     =
| image	         =
| caption        =
| director       = Jean-Luc Godard
| producer       =
| writer         =
| starring       = Woody Allen Jean-Luc Godard
| music          =
| cinematography = Pierre Binggeli
| editing        = Jean-Luc Godard
| distributor    =
| released       = 1986
| runtime        = 26 min.
| country        = France English
| budget         =
}}

 
Meetin WA is a 1986 short film by Jean-Luc Godard. In the film, he interviews his "old friend" Woody Allen.

==Description==
The film mainly consists of an interview of Woody Allen by Godard, with the help of an off screen interpreter. In a prologue, Godard can be seen from the rear in silhouette standing at a window looking out over Central Park in New York City, while on the sound track a Gershwin tune previously used by Allen in Manhattan_(film)|Manhattan can be heard. The interview proper is presented as a series of fragments that frequently obfuscate the subject of the conversation, though it is evident that much of the conversation is about Allens film Hannah and Her Sisters, which had just been released. The conversation fragments are separated by various still images and by intertitles; the text comments on the conversation, often in a punning way characteristic of Godard. The filmmaker, for whom intertitles are a frequent device, asks Allen about his own use of intertitles in Hannah and Her Sisters. Allen observes that, while for Godard the device is filmic, for Allen it is literary, and Allen goes on to say more about the literary origins of Hannah and Her Sisters. 

Godard then asks Allen about the influence of television on his work, saying that Allens shots of New York City buildings in Hannah and Her Sisters seem to owe something to the way television portrays things. Finding the question obtuse, Allen instead rhapsodizes on his youthful experience of the cinema. In an epilogue, Godard is seen rifling through ephemera and a stack of books on a table. Suddenly, declaring that "the meeting is finished," he slams the stack of books down on the table, slowing down the gesture in post production, so that the slam of the books hitting the table has a low, ominous tone. The final intertitle, displayed longer than the others, declares: "Meetings Over."

== Background ==
The film was made as a substitute for the traditional press conference with the director following the premiere of Hannah and Her Sisters at the Cannes Film Festival. According to Tom Luddy, who helped arrange the interview, the impetus was to keep Allen involved in Godards King_Lear_(1987_film)|King Lear project while Godard was stalled for lack of ideas. Knowledge of Godards life and work illuminate what might otherwise be an opaque encounter as one in which Godard was disappointed by his interview subject. For example, Allen describes being influenced by novels when writing Hannah and Her Sisters and asserts that script writing is the most important phase of filmmaking for him. This is at diametrically opposed to Godards philosophy and method of filmmaking, where the work is in the process of being created across all stages of filmmaking, particularly editing, and the primacy is on images, not text. Godard also has a deep disdain of television, and Allens lack of understanding of Godards line of questioning on the subject of televisions corruption of the cinema appears to have been the final nail in the coffin that led to the desultory ending of this short film.

== References ==
* 
* 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 