Rush to Judgment
{{Infobox book
 
| name             = Rush to Judgment
| image            =  
| caption    =  Mark Lane
| title_orig       = 
| translator       = 
| illustrator      = 
| cover_artist     = 
| country          = United Kingdom
| language         = 
| series           = 
| subject          = Assassination of John F. Kennedy
| genre            = 
| publisher        = The Bodley Head
| pub_date         = August 1966
| english_pub_date = 
| media_type       = Print (hardcover)
| pages            = 478 pp
| isbn             = 
| oclc             = 4215197
| dewey            = 
| congress         = E842.9 .L3 1966a
| preceded_by      = 
| followed_by      = 
}} Mark Lane. conspiracy to assassinate John F. Kennedy.   The books introduction is by Hugh Trevor-Roper, Regius Professor of History (Oxford).
 hardcover book to confront the findings of the Warren Commission.   

Sid Moody accused Lane of selectively quoting witness to support his conclusions:    
 Thomas Erskines King George III in 1800.

According to Alex Raskin of the Los Angeles Times, "Rush to Judgment opened the floodgate for John F. Kennedy assassination conspiracy theories|  conspiracy theories".   

==Documentary==
In 1967, a documentary film based on Lanes book, Rush to Judgment, about the John F. Kennedy assassination, was directed by Emile de Antonio and hosted by Lane.   It is a black and white film, 122 minutes long. It has been shown on BBC TV as part of the much longer (300 minutes) film entitled The Death of Kennedy. Included are several video clips showing how Dealey Plaza existed in 1963 and 1966, clips of Lee Harvey Oswald, Dallas Chief of Police Jesse Curry, Dallas District Attorney Henry Wade, Jack Ruby, and his defense attorney Melvin Belli.

Some of the assassination witnesses who present their observations on-camera include Abraham Zapruder, James Tague, Charles Brehm, Mary Moorman, Jean Hill, Lee Bowers, Sam Holland, James Simmons, Richard Dodd, Jessie Price, Orville Nix, Patrick Dean, Napoleon Daniels, Nancy Hamilton, Joseph Johnson, Roy Jones, and Cecil McWatters.

==See also==
*John F. Kennedy assassination conspiracy theories

==References==
 

 

 
 
 
 
 
 
 
 