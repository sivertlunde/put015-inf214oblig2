Ash Wednesday (1958 film)
 
{{Infobox film
| name           = Ash Wednesday
| image          = Miércoles de ceniza.jpg
| caption        = DVD cover
| director       = Roberto Gavaldón
| producer       = Gregorio Walerstein
| writer         = Julio Alejandro Luis G. Basurto Roberto Gavaldón
| starring       = María Félix
| music          = 
| cinematography = Agustín Martínez Solares
| editing        = Rafael Ceballos
| distributor    = 
| released       =  
| runtime        = 112 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}

Ash Wednesday ( ) is a 1958 Mexican drama film directed by Roberto Gavaldón. It was entered into the 8th Berlin International Film Festival.   

==Plot==
Victoria (María Félix) was attacked in her youth by a Catholic priest. Years later she is a famous prostitute and hates everything related to the Catholic Church. She falls in love with a priest (Arturo de Córdova), a hidden priest during the Cristero War.

==Cast==
* María Félix as Victoria Rivas
* Arturo de Córdova as Dr. Federico Lamadrid
* Víctor Junco as José Antonio
* Rodolfo Landa as El Violador Andrea Palma as Rosa, amiga de Victoria
* María Rivas (actor)|María Rivas as Silvia
* María Teresa Rivas as Elvira
* David Reynoso as Enrique, coronel Carlos Fernández as Carlos
* Enrique García Álvarez as Padre Gonzalez
* Luis Aragón as General cristero
* Consuelo Guerrero de Luna as Mujer del burdel
* Arturo Soto Rangel as Notario
* Cuco Sánchez as Soldado cantante Arturo Castro Bigotón  as Borracho
* Miguel Suárez as Conspirador

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 