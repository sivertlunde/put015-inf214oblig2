Méfiez-vous des blondes
{{Infobox film
| name           = Méfiez-vous des blondes
| image          =
| caption        = 
| director       = André Hunebelle
| producer       = 
| writer         = 
| based on       = 
| screenplay     = 
| starring       = Raymond Rouleau Martine Carol Claude Farell
| music          =
| cinematography = 
| editing        = 
| studio         = 
| distributor    =
| released       =  
| runtime        = 107 minutes
| country        = France French
| budget         =
}} French drama film directed by André Hunebelle and starring Raymond Rouleau, Martine Carol and Claude Farell.  It is part of the trilogy of films featuring reporter Georges Masse. It was preceded by Mission in Tangier in 1949 and followed by Massacre in Lace (1952). Masse investigates the death of a woman who had been trying to gain information of drug-smuggling organisations in the Far East.

==Cast==
* Raymond Rouleau ...  Georges Masse
* Martine Carol ...  Olga Schneider
* Claude Farell ...  Suzanne Wilson
* Yves Vincent ...  Luigi Costelli
* Bernard La Jarrige ...  Petit Louis
* Henri Crémieux ...  M. Dubois
* Pierre Destailles ...  Lionel, le voyageur
* Espanita Cortez ...  La danseuse espagnole
* Monique Darbaud ...  La danseuse de be bop

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 