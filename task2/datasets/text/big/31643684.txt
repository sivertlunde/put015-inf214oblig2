Kalabha Mazha
 
{{Infobox film
| name           = Kalabha Mazha
| image          = Kalabhamazha.jpg
| caption        =
| alt            =
| director       = P Sukumenon
| producer       = Suku Nair
| writer         =
| starring       = Krisha  Radhika
| music          = Rajeev ONV
| cinematography = M. J. Radhakrishnan
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Kalabha Mazha (  ) is a 2011 Malayalam film directed by P Sukumenon, starring Krisha and Radhika in the lead roles.

==Synopsis==
Kalabha Mazha tells the friendly relationship between a Hindu and a Muslim family. Madhava Menon (Thilakan) who worked in the All India Radio always respects other religions and works for the upliftment of  the society. Malavika (Devika) is the eldest daughter of Madhava Menon. She is betrothed to Unnikrishnan (Krishna). She is more than a sister to her two younger siblings, since their mother had died when they were quite young. Menon, is regretful that he hasnt been able to save much money for his children, and the family finds it hard, to make both ends meet, as days pass by. There is a Muslim family staying nearby, headed by Kunjali (Mamukkoya). His son-in-law, on a job in the Middle East, had been missing for about six long years. Malavikas sister, falls for a film director, and soon realizes that her decision was wrong. Her brother on the other hand, takes up a job at a local bank, and finds himself having transformed into a goon. Later, Malavika bumps into her sisters absconding lover, and almost meets with the same fate in his hands.

==Cast==
* Krishna as Unnikrishnan
* Devika as Malavika
* Thilakan as Madhava Menon
* Mamukkoya as Kunjali
* Cochin Haneefa
* Jagathy Sreekumar
* Indrans
* Saina Krishna
* Kalaranjini
* Sarat
* Mohan Kartha
* Hanna Yazir
* Baby Gandhika

==References==
*  
*  
*  

 
 
 
 
 
 

 
 