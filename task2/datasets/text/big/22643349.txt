The Black Tulip (1921 film)
 
{{Infobox film
| name           = The Black Tulip
| image          = 
| caption        =  Frank Richardson
| producer       = 
| writer         = Alexandre Dumas père
| starring       = 
| music          =
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 72 minutes
| country        = Netherlands
| language       = Silent
| budget         = 
}}
The Black Tulip ( ) is a 1921 Dutch silent adventure film directed by Maurits Binger.

==Cast== Gerald McCarthy - Cornelis van Baerle
* Zoe Palmer - Rosa, daughter of the jailer
* Eduard Verkade - Cornelis de Witt
* Dio Huysmans - Johan de Witt
* Coen Hissink - Jailer / Gryphus
* Harry Waghalter - Isaac Boxtel
* August Van den Hoeck - Tichelaer, the barber from Piershil
* Frank Dane - Prins van Oranje
* Lau Ezerman - Willem
* Wilhelmina van den Hoeck - Wife of Cornelis de Witt
* Josephine Homann-Niehorster
* Carl Tobi
* Fred Homann
* Betty Doxat-Pratt

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 