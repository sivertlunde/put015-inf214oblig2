The Prodigal
{{Infobox film
| name           = The Prodigal
| image          = Prodigal-Lobby2-1-.jpeg
| image_size     =
| alt            =
| caption        = Theatrical release lobby card
| director       = Richard Thorpe
| producer       = Charles Schnee
| writer         = Maurice Zimm
| narrator       =
| starring       = Lana Turner Edmund Purdom  Louis Calhern Joseph Wiseman
| music          = Bronislau Kaper
| cinematography = Joseph Ruttenberg
| editing        =
| studio         = MGM
| distributor    = MGM
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         =$2,783,000  . 
| gross          = $4,143,000 
}} 1955 epic Biblical epic film made by MGM starring Lana Turner. It was directed by Richard Thorpe and produced by Charles Schnee. 

The Maurice Zimm screenplay was adapted by Joseph Breen, Jr. and Samuel James Larsen from the New Testament story of the selfish son who leaves his family in search of riches. The music score was by Bronislau Kaper, with cinematography by Joseph Ruttenberg. 
 James Mitchell, Joseph Wiseman, Cecil Kellaway and Walter Hampden. The dancer Taina Elg made her film debut.

The Prodigal was satirized in Mad (magazine)|Mad #26 (November 1955) as "The Prodigious".

==Plot summary== parable of the prodigal son, from the Biblical New Testament Gospels, although considerable liberties are taken with the source material, chief among them being the addition of a female lead in the form of the priestess of Astarte, Samarra (Turner). Micah (Purdom), a young Hebrew farm boy, is dissatisfied with the rural lifestyle of his family, demands that his father give him his inheritance, and journeys to the cosmopolitan city of Damascus, Syria|Damascus. There he meets the pagan priestess Samarra, who seduces him into losing his inheritance to the local High Priest and betraying his religious faith. Enduring a number of difficulties, Micah finally realizes where he belongs and returns home to his father, who forgives Michael all of his sins and orders a lavish celebration of his return.

==Cast==
* Lana Turner as Samarra
* Edmund Purdom as Michael
* Louis Calhern as Nahreeb
* Audrey Dalton as Ruth James Mitchell as Asham
* Neville Brand as Rhakim
* Walter Hampden as Eli
* Taina Elg as Elissa
* Joseph Wiseman as Carmish
* John Dehner as Joram
* Cecil Kellaway as the Governor
==Reception==
According to MGM records the film earned $2,153,000 in the US and Canada and $1,990,000 elsewhere, resulting in a loss of $771,000. 
==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 