Chance or Coincidence
{{Infobox film
| name = Chance or Coincidence
| image =
| image_size =
| alt = 
| caption =
| director = Claude Lelouch
| producer = André Picard Marie-Christine Lezzi Suzanne Dussault Gabriela Chavira-Gélin Sule Soysal Faruk Aksoy
| writer = Claude Lelouch
| starring = Alessandra Martines Pierre Arditi Marc Hollogne Laurent Hilaire Geoffrey Holder
| music = Claude Bolling Francis Lai
| cinematography = Pierre-William Glenn
| editing = Hélène de Luze
| studio = 
| distributor = Les films 13
| released =  	
| runtime = 120 minutes
| country = Canada France
| language = English, Italian, French
| budget =
| gross =
}}
Hasards ou coïncidences is a French film directed by Claude Lelouch, released in 1998 in film|1998.

==Starring==
* Alessandra Martines : Myriam Lini
* Pierre Arditi : Pierre Turi
* Marc Hollogne : Marc Deschamps
* Véronique Moreau : Catherine Desvilles Patrick Labbé : Michel Bonhomme
* Laurent Hilaire : Laurent
* Geoffrey Holder : Gerry
* Charles Gérard : Lhomme sur le bateau
* David La Haye : Le voleur

==Awards==
* Nominated for César Award for Best Music Written for a Film
* Best Actress at Chicago Film Festival for Alessandra Martines

 
 

 
 
 

 