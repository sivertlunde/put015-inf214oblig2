Ben & Sam
 
{{Infobox film
| name           = Ben & Sam
| image          = 
| alt            =  
| caption        = Ben & Sam
| director       = Mark Shandii Bacolod
| producer       = Jazmin Trinidad Tecson Sean Lim Tessa Aquino
| writer         = Archie Del Mundo
| starring       = Ray An Dulay Jess Mendoza Micah Munoz Tara Cabaero Ana Abad Santos
| music          = Christian Banawa Herrick Ortiz
| cinematography = Rain Yamson II
| editing        = Bebs Gohetia
| studio         = 
| distributor    = Imaginative Media Production
| released       =  
| runtime        = 96 minutes
| country        = Philippines
| language       = Filipino
| budget         = 
| gross          = 
}}

Ben & Sam  is a 2010 Filipino indie film, directed by Mark Shandii Bacolod  and starring Ray An Dulay, Jess Mendoza, Micah Munoz, Ana Abad Santos and Tara Cabaero. The movie is about two campus kings who are in love.  The film, which premiered at the 2010 Queer Love Film Festival on February 17, 2010,  was written by Archie Del Mundo. 

==Plot==
Ben is haunted by painful memories of his abusive father, and is suffering even more at present due to his increasingly eccentric mother. He is eventually toughened by this condition. Sam, meanwhile, is still grieving the murder of his activist boyfriend. Both boys turn to recreational activities to forget their painful precedents; Ben becomes involved in basketball, while Sam takes up dancing.
 liaison eventually blossoms into something deeper. 

==Special screening== 2010 Cinemalaya Independent Film Festival  as part of the July midnight shows.

==Cast==
* Ray An Dulay as Ben
* Jess Mendoza as Sam
* Micah Munoz as George
* Ana Abad Santos as Arlene
* Tara Cabaero as Sugar
* Simon Ibarra as Mr. Quirino
* Malouh Crisologo as Dean
* Angeli Bayani as Prof Castro
* Jerri Barrios as Coach Mario

==References==
 

==External links==
*  

 
 
 
 