The Bangville Police
{{Infobox film
| name           = The Bangville Police
| image          =
| caption        =
| director       = Henry Lehrman
| producer       =
| writer         =
| starring       = Fred Mace Mabel Normand The Keystone Cops
| cinematography =
| editing        =
| distributor    = Mutual Film
| released       =  
| runtime        = 8 minutes
| country        = United States
| language       = Silent English intertitles
}}

The Bangville Police (also known as Bangville Police) is a 1913 comedy short starring Fred Mace, Mabel Normand and the Keystone Cops (Mace, Raymond Hatton, Edgar Kennedy, Ford Sterling, and Al St. John).     The film, notable for being regarded as the seminal Keystone Cops short, was directed by Henry Lehrman. The first Keystone Cops comedy was Hoffmeyers Legacy (1912).

==Plot==
A farmer and his daughter (Mabel Normand) are in the barn. She is saying she wishes the cow would have a calf. Left alone in the  house she hears strangers in the barn and calls the police. She barricades herself in. Her parents return and have to break the door down. She thinks it is the robbers. Meanwhile a smokey car brings the police (the Keystone Cops). After misunderstandings are resolved they find a new-born calf in the barn.

==Cast==
 
* Fred Mace ... Police Chief
* Mabel Normand ... Farm Girl
* Nick Cogley ... Father
* Dot Farley ... Mother
* Betty Schade
* Raymond Hatton ... Policeman
* Edgar Kennedy ... Policeman
* Hank Mann ... Policeman
* Ford Sterling ... Policeman
* Al St. John ... Policeman
 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 