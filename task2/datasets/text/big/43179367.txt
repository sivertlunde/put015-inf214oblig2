Kannan En Kadhalan
 

{{Infobox film
| name           = KANNAN EN KADHALAN
| image          = 
| caption        = 
| director       = P. Neelakantan
| producer       = R.M.Veerappan           
| writer         = A.S.Pragassam R.M.Veerappan Vidwan Lakshamanan & Na.Pandhu ranghan
| starring       = M. G. Ramachandran Jayalalitha Vanisree S. A. Asokan Muthuraman Cho
| music          = M. S. Viswanathan
| cinematography = V.Ramamoorthy
| editing        = C.P.Djambhulingham
| studio         = Sathiya Studios
| distributor    = La Sathiya Movies
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Indian Tamil Tamil  directed by Pa.Neelakandhan, starring M. G. Ramachandran in the lead role and Jayalalitha with Vanisree, S. A. Asokan, Cho and among others.

==Plot==

Kannan (MGR) and Maruthi (Vanisree) is in love.
 
Maliga (Jayalalitha) loves secretly for a long time Kannan.

Kannan always considered Maliga as her sister.

Here is the dilemma of Maliga !

But one day, arrives a terrible accident which everything is going to change everything...

==Cast==
{| class="wikitable" width="72%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || as "Piano" Kannan
|-
| J. Jayalalitha || as Maliga
|-
| Vanisree || as Maruthi
|-
| R. Muthuraman || as The engineer Sundharam
|-
| S. A. Ashokan || as The doctor Singh
|-
| Cho || as Pathapi / as Sabhabathi
|-
| Thengai Srinivasan || as Sheshastri Iyer
|-
| J. P. Chandrababu (Guest-Star) || as Chandran
|-
| O. A. K. Devar || as Singaram
|-
| Tiruchi Selandhar Rajan || as The Captain Gopal
|- Justin || as a goon in the cabaret
|-
| Usilamani || as The false astrologer
|-
| Gandhimadi || as Neelambigai Ammu
|-
| Rama Prabha || as Geeridja
|-
|}

The casting is established according to the original order of the credits of opening of the movie, except those not mentioned

==Around the movie==

A sentimental drama, a love triangle in "Douglas Sirk" style.

The soundtrack of M.S.V. is magnificent !

This movie looks like another MGR, in this case, ORU THAAI MAKKAL, 1971.

The character was played by the actress Jayalalidha, the dancer Maliga is reached by the Münchausen syndrome. She has all the characteristics of it.

 
 
 
 


 