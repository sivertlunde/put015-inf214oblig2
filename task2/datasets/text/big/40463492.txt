Kuma (film)
{{Infobox film
| name           = Kuma
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Umut Dag
| producer       = Veit Heiduschka Michael Katz
| screenplay     = Petra Ladinigg
| story          = Umut Dag
| based on       =  
| narrator       = 
| starring       = 
| music          = Iva Zabkar
| cinematography = Carsten Thiele
| editing        = Claudia Linzer
| studio         = Wega Film
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Austria
| language       = German Turkish
| budget         = 
| gross          = 
}} Turkish immigrant family living in Vienna.

==Cast==
*Nihal G. Koldas as Fatma
*Begüm Akkaya as Ayse
*Vedat Erincin as Mustafa
*Murathan Muslu as Hasan
*Alev Imak as Kezvan
*Aliye Esra Salebci as Gülsen

==Reception==
Kuma has won several international awards including the Special Audience Prize at the 2012 Lecce Festival of European Cinema and the Golden Starfish Award at the 2012 Hamptons International Film Festival. At the 2012 Philadelphia Film Festival Begüm Akkaya won Honorable Mention in the category of Best Actress. The film was nominated for Best Debut Film at the 62nd Berlin International Film Festival. 
 The Telegraph, Tim Robey awarded Kuma the same rating and described it as a "vigorous and engrossing debut". 

==References==
{{Reflist|refs=
 {{Citation
  | last = 
  | first = 
  | author-link = 
  | title = News
  | work = Official Kuma website
  | publisher = 
  | date = 
  | url = http://www.kuma-film.com/en/news
  | accessdate =6 September 2013
}}
 
 {{Citation
  | last = Bradshaw
  | first = Peter
  | author-link = Peter Bradshaw
  | title = Kuma — review
  | work = The Guardian
  | publisher = Guardian News and Media
  | date = 15 August 2013
  | url = http://www.theguardian.com/film/2013/aug/15/kuma-review
  | accessdate = 6 September 2013
}} 
 {{Citation
  | last = Robey
  | first = Tim
  | title = Kuma, review The Telegraph
  | publisher = Telegraph Media Group
  | date = 15 August 2013
  | url = http://www.telegraph.co.uk/culture/film/filmreviews/10244850/Kuma-review.html
  | accessdate =6 September 2013
}}
 
}}

==External links==
* 
* 

 
 
 
 