Todo un hombre (1943 film)
{{Infobox film
| name           = Todo un hombre
| image          = Todo un hombre.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Pierre Chenal
| producer       = 
| writer         = 
| screenplay     =Ulyses Petit de Murat, Homero Manzi, Miguel Mineo
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          =Lucio Demare, Juan Ehlert
| cinematography =Bob Roberts 
| editing        = Gori Muñoz and Carlos Rinaldi 
| studio         =Artistas Argentinos Asociados
| distributor    = 
| runtime=94 minutes
| released       =    
| country        = Argentina Spanish
| budget         =
}}
 1943 Argentina|Argentine romantic drama film directed by Pierre Chenal on his Latin film debut, and starring Francisco Petrone
and Amelia Bence.  Critically acclaimed, the film was compared by critics in Argentina to Jean Vigos LAtalante.  At the 1944 Argentine Film Critics Association Awards, Petrone won the Silver Condor Award for Best Actor for his performance in the film.   

==Plot==
Francisco Petrone is a tough, hard-working independent river man, who finds it difficult to communicate and express his true feeling to his young wife (Amelia Bence). The two travel up a winding river, and tension between the two escalates. 

==Cast==
*Francisco Petrone
*Amelia Bence
*Nicolás Fregues
*Florindo Ferrario
*Guillermo Battaglia
*Ana Arneodo
*Tilda Thamar
*Renée Sutil
*Leticia Scuri
*Carlos Belluci

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 