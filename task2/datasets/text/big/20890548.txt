The Incredible Sarah
 
{{Infobox film
| name           = The Incredible Sarah
| image          = Poster of the movie The Incredible Sarah.jpg
| caption        = Film poster
| director       = Richard Fleischer
| producer       = Helen M. Strauss Douglas Twiddy
| writer         = Ruth Wolff
| starring       = Glenda Jackson
| music          = Elmer Bernstein
| cinematography = Christopher Challis
| editing        = John Jympson
| distributor    = Cinema International Corp. Anchor Bay Entertainment
| released       =  
| runtime        = 106 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}

The Incredible Sarah is a 1976 British drama film directed by Richard Fleischer and starring Glenda Jackson.  It presents dramatization of the acting career of Sarah Bernhardt.

==Cast==
* Glenda Jackson - Sarah Bernhardt Daniel Massey - Victorien Sardou
* Yvonne Mitchell - Mamselle
* Douglas Wilmer - Montigny
* David Langton - Duc De Morny Simon Williams - Henri de Ligne
* John Castle - Damala
* Edward Judd - Jarrett
* Rosemarie Dunham - Mrs. Bernhardt
* Peter Sallis - Thierry
* Bridget Armstrong - Marie Margaret Courtenay - Madame Nathalie
* Maxwell Shaw - Fadinard
* Patrick Newell - Major Neil McCarthy - Sergeant

==Awards== Best Costume Best Art Direction (Elliot Scott, Norman Reynolds). Glenda Jackson was also nominated for a Golden Globe for Best Actress.   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 