Six Bridges to Cross
{{Infobox film
| name           = Six Bridges to Cross
| image          = Six Bridges to Cross.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Joseph Pevney
| producer       = Aaron Rosenberg
| writer         =
| screenplay     = Sydney Boehm
| story          = 
| based on       = Joseph F. Dinneens "They Stole $25,000,000 - And Got Away with It"
| starring       = 
| music          = Frank Skinner and Herman Stein (both uncredited)
| cinematography = William H. Daniels
| editing        = Russell F. Schoengarth	
| studio         = Universal-International
| distributor    = 
| released       = January 29, 1955
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1.8 million (US)  
}} 1955 American crime caper film directed by Joseph Pevney of Universal-International.  The film starred Tony Curtis, George Nader, Julie Adams, Jay C. Flippen and Sal Mineo on his screen debut.    Six Bridges to Cross is based upon the famous 1950 Great Brinks Robbery of Boston, Massachusetts in which the thieves made off with roughly $2.5 million.    

==Plot==
Jerry Florea (Tony Curtis) is planning a heist. The story begins with the events which led a young Florea (Sal Mineo) to become a crook. One day he is shot during a robbery and as a result an ameniable policeman and his wife take him under their wing.    As a young man he deludes them, and pretends to no longer have criminal intent and even gets a job at the Brinks. They are unaware he is preparing to rob the establishment. It is only after he and his gang pull off the heist that Florea reconsiders his actions and attempts to make amends for the crime.

==Cast==
*Tony Curtis ...  Jerry Florea
*George Nader ... Edward Gallagher
*Julie Adams ... Ellen Gallagher
*Jay C. Flippen ... Vincent Concannon
*Sal Mineo ... Jerry as a boy
*Jan Merlin ... Andy Norris
*Richard Castle ... Skids Radzevich William Murphy ... Red Flanagan
*Kendall Clark ... Mr. Sanborn
*Don Keefer ... Mr. Sherman
*Harry Bartell ... Father Bonelli
*Tito Vuolo ... Angie

==Production==
The screenplay for the film was written by Sydney Boehm, based on Joseph F. Dinneens They Stole $25,000,000 - And Got Away with It. The film was shot on location in Boston.    
 Jeff Chandler was to play the lead but refused and was put on suspension by Universal. Jeff Chandler Suspended at U-I
Los Angeles Times (1923-Current File)   21 May 1954: A6.  
 Jeff Chandler, recording it on December 2, 1954.          The overall score was composed by Frank Skinner and Herman Stein but they went uncredited in the film for their contributions.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 