Misfire: The Rise and Fall of the Shooting Gallery
{{Infobox film name           = Misfire: The Rise and Fall of the Shooting Gallery  image          = caption        = created by   = Whitney Ransick director       = Whitney Ransick producer       = Gil Gilbert Bob Gosse Whitney Ransick
|exec. producer = writer         =  starring       = Robin Tunney Ray Angelic Miguel Arteta music          = Sam Bisbee Dave Thomas Junior editing        = Gil Gilbert studio         = TSG distributor    =  released       =   (limited) runtime        = 78 minutes country        = United States language       = English budget         = gross          = 
}} distributor The Shooting Gallery, directed by Whitney Ransick. The film had its world premiere on 11 October 2014 at the Hamptons International Film Festival.  

==Synopsis== Laws of Gravity. Ransick looks at the company starting with their start in the early nineties to their crash in later years.

==Cast==
 Listed alphabetically 
 
*Ray Angelic
*Miguel Arteta
*Mark Chandler Bailey
*Eamonn Bowles
*Peter Broderick
*Maggie Carino Ganias
*Jamie Chvotkin
*Barry Cole
*Edie Falco
*Hampton Fancher
*George Feaster
*F.X. Feeney
*James Foley
*Elizabeth Garnder
*Steven Gaydos
*Nick Gomez
*Bob Gosse
*Matthew Harrison
*Ted Hope
*J. Christian Ingvordsen

 
*William Jennings
*Eli Kabilio
*Jason Kliot
*Danny Leiner
*Tim Blake Nelson
*Amy Nicken
*Amos Poe
*Whitney Ransick
*Brandon Rosser
*Larry Russo
*James Scahmus
*John Sloss
*Holly Sorenson
*Paul Speaker
*Michael Spiller
*Morgan Spurlock
*Henry Thomas
*Lina Todd
*Adam Trese
*Robin Tunney
*Christine Vachon
*Chris Walsh
*Boaz Yakin
 

==Reception==
  and Grolsch Film Works were more mixed in their reviews and The Hollywood Reporter commented that "Gosse comes across sympathetically, and the film captures the shock of the companys 2001 collapse. But the "rise and fall" chronology is thinner than it should be, leaving us to marvel at the train wreck without exposing anything new about its causes."  

==References==
 

==External links==
*  
* 

 
 
 