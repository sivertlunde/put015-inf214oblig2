Funny People
{{Infobox film
| name           = Funny People
| image          = PosterFunnyPeople.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Judd Apatow
| producer       = Judd Apatow Clayton Townsend Barry Mendel
| writer         = Judd Apatow RZA  Michael Andrews
| cinematography = Janusz Kamiński
| editing        = Brent White Craig Alpert
| studio         = Relativity Media Apatow Productions Happy Madison|Mr. Madison 23 Productions
| distributor    = Universal Pictures  (United States)  Columbia Pictures  (International) 
| released       =   
| runtime        = 146 minutes  
| country        = United States
| language       = English
| budget         = $75 million 
| gross          = $71.6 million 
}}
Funny People is a 2009 American comedy-drama|comedy-drama film written, produced and directed by Judd Apatow, and starring Adam Sandler, Seth Rogen, and Leslie Mann. The film was released on July 31, 2009 in North America, and on August 28, 2009 in the United Kingdom. It is Apatows longest film, with its running time being 146 minutes. The film was co-produced by Apatow Productions and Happy Madison|Mr. Madison 23 Productions, a subsidiary of Sandlers company Happy Madison. Universal Pictures and Columbia Pictures co-financed the film and the former also served as a worldwide distributor.    The film received generally positive reviews, with critics praising the script, the directing and the performances of Sandler and Rogen, although criticism was directed towards the films excessive runtime. The supporting cast features Eric Bana, Jason Schwartzman, Jonah Hill and Aubrey Plaza.

==Plot==
George Simmons (Adam Sandler) is a middle aged former stand-up comedian turned movie star. Despite his millions he is sad and lonely and most of his recent film work is low-brow and dumb. He is diagnosed with acute myeloid leukemia and offered an experimental treatment that has only an eight-percent chance of therapeutic response. Believing he is about to die, he returns to his roots to do stand-up comedy.

Ira Wright (Seth Rogen) is an aspiring stand-up comedian in his twenties who shares an apartment with his two best friends, Mark and Leo (Jason Schwartzman and Jonah Hill). George meets Ira at a small comedy club and hires him as his assistant. Ira becomes one of Georges only close relationships. The two travel around the country, often meeting with real life comedians who play themselves and talk about the business of comedy.

George reconnects with his ex-fiancée, Laura (Leslie Mann) who is currently married to Clarke (Eric Bana). George′s physician tells him that the leukemia is in Remission (medicine)|remission. George decides he wants Laura back. Laura invites George and Ira to her house in Marin County while her husband is away on business. George and Ira spend quality time with Laura and her two young daughters. George and Laura sneak off to have sex, but Clarke returns home and there is a huge argument. 

The plot of the movie now concerns who Laura will choose, her current husband Clarke who cheats on her, or her former husband George (who also cheated). Ira is not always on Georges side in the love triangle, so when it doesnt go Georges way in the end, he fires Ira.

Ira returns to his old food-service job. After some time has passed, George attends Iras stand-up act and sees that his old assistant has become a far more confident performer. The next day, George finds Ira at work and they reconnect as friends, telling each other jokes as equals.

==Cast==
 
 
* Adam Sandler as George Simmons
* Seth Rogen as Ira Wright
* Leslie Mann as Laura
* Eric Bana as Clarke
* Jason Schwartzman as Mark Taylor Jackson
* Jonah Hill as Leo Koenig
* Aubrey Plaza as Daisy Danby RZA as Chuck
* Maude Apatow as Mable
* Iris Apatow as Ingrid
* Aziz Ansari as Randy Springs
* Torsten Voges as Dr. Lars
* Allan Wasserman as Dr. Stevens
* Steve Bannos as Deli Manager
  Tom Anderson, George Wallace, and Andy Dick made cameo appearances as themselves in the roles of Georges fellow comedians.  Rapper Eminem,  comedian Ray Romano, musician James Taylor,  MADtv member Nicole Parker,  and newcomer Bo Burnham  also appeared in small roles. Undeclared alum Carla Gallo had a cameo in the film as a character on Yo Teach!, the television show within the film that Mark stars in,  while Justin Long and Apatow regular Ken Jeong have cameos in the film as characters in movies for which George is famous.  Owen Wilson and Elizabeth Banks are featured on posters for fake movies in which George starred.  Bryan Batt makes an appearance as Georges agent. Musicians Jon Brion, Sebastian Steinberg, and James Gadson appear in the film as members of Georges jam band. Comedians Rod Man, Budd Friedman, Monty Hoffman, Mark Schiff, Orny Adams, Al Lubel, and Jerry Minor appear as themselves. Comedienne/producer/writer Carol Leifer appears as herself.

==Production==
Judd Apatow had expressed his desire to make a stand-up comedian mentor film loosely based on his own early experiences as a struggling performer. He could not come up with an interesting idea, however, since most of his mentors were kind to him. He then thought of making a film about a mentor facing a life crisis, and decided to cast his former roommate Adam Sandler after seeing him in Reign Over Me. They discussed making the film almost two years prior to production.   

Apatow had cast Sandler, Seth Rogen, and Leslie Mann as the three leads in March 2008.  Eric Bana, Jonah Hill, and Jason Schwartzman were cast in June 2008 when the title of the film was announced. When asked about the decision to cast Bana, Apatow said that both he and Rogen are fans of his films; Rogen additionally commented they cast him as the husband because he was someone who would be considered an intimidating presence to both Sandler and Rogen.  Bana mentioned that he decided to play the character with his native Australian accent so he would be more comfortable improvising.  Apatow and Manns daughters, Maude and Iris Apatow, play the young girls in the film. Both Apatow and Mann state that this casting choice allowed for more natural dialogue for the children, but the girls have not been allowed to actually see the film 

Academy Award-winning cinematographer Janusz Kamiński handled the cinematography for the film. Apatow had Sandler, Rogen, and Hill write their own material for routines. Apatow filmed them performing their routines in front of live audiences, using six cameras to capture their performances and audience reactions. Apatow filmed their entire performances, although only five to ten minutes of stand-up footage appear in the film. Hill admitted his performance was not well-received because he had never done stand-up before. Additionally, Apatow filmed scenes from Sandlers characters fictional filmography, as well as scenes from Schwartzmans characters fictional television show Yo Teach!, for the film to add realism. 

Apatow used an old video of Sandler, from when the two were roommates, in which Sandler makes prank phone calls, and features a young Ben Stiller.

==Marketing== teaser poster Orpheum Theater in Los Angeles to film a scene for the movie. The event was open to the general public and featured acts by Adam Sandler, Seth Rogen, Aziz Ansari, Sarah Silverman, David Spade, and Patton Oswalt, with Sandler, Rogen, and Ansari performing as their characters in the film. The first theatrical trailer for the film was released February 20, 2009 on the Internet, with a shortened version first appearing in theaters with I Love You, Man.

A website for a fictional television show-within-a-film was created on NBC.com.    The sitcom, Yo Teach!, "stars" character Mark Taylor Jackson (Jason Schwartzman), a C-list actor portraying a young teacher with a class of failing students, and includes a cameo by internet celebrity Bo Burnham.   

A website for Aziz Ansaris character Randy Springs was created, along with a documentary of the character on Funny or Die|FunnyOrDie.com. The documentary was directed by Jason Woliner.

Comedy Central aired a special, "Inside Funny People" on July 20, documenting the making of the film and showing clips of the stand-up. The channel also aired "Funny People: Live" on July 24, which is a live broadcast stand-up of Sandler, Rogen and Hill as part of the films promotion.

==Release==

===Critical reception===
Funny People received positive reviews from critics.  On  , gave the film a score of 60 out of 100, based on 36 critics, indicating "mixed or average reviews".    
    
Jeffrey Wells from Hollywood Elsewhere received feedback from sources who had seen a test screening, with one source calling it "really funny, a really sweet movie, a lot of veracity...really a brilliant film", comparing it to the works of James L. Brooks. 
    
Roger Ebert of the Chicago Sun-Times awarded the film 3½ stars of four, calling it "a real movie. That means carefully written dialogue and carefully placed supporting performances &mdash; and its about something. It could have easily been a formula film...but George Simmons learns and changes during his ordeal, and we empathize." It is the highest rating Ebert ever gave an Adam Sandler film, tied with his review for Punch-Drunk Love.  Peter Travers of Rolling Stone also praised the film, writing, "Apatow scores by crafting the film equivalent of a stand-up routine that encompasses the joy, pain, anger, loneliness and aching doubt that go into making an audience laugh."  Kyle Smith of the New York Post wrote that the film was "one of the most absorbing films of the year."    
     Michael Phillips of the Chicago Tribune gave the film one of its mixed reviews, complaining of the films two-and-a-half-hour running time: "Funny People is...an attempt by Apatow to reconcile the huge success he has become with the up-and-comer he once was. The results run an increasingly exasperating 2½ hours.". 

Manohla Dargis of the New York Times complains the film is "irritatingly self-satisfied" and describes the film as "nice" ... "but nice can be murder on comedy and drama alike". {{cite news | url = http://movies.nytimes.com/2009/07/31/movies/31funny.html | title = 
Funny People (2009) | author = Manohla Dargis | date = July 30, 2009 | work = New York Times }} 

Gene Shalit of NBCs The Today Show disliked the film greatly, stated that its "A smirk of faithful characters that are making a vanity movie about themselves that keeps not ending for 2 1/2 unendurable hours. Director Judd Apatow wrote the script and its vulgar, in fact its ineffable, because without the letter F, he would have no script." 

===Box office===
Funny People was commercially released on July 31, 2009 in the United States and Canada. It was distributed to 3,007 theaters, and grossed $8.63 million on its opening day.  At the end of its opening weekend, the film had grossed $23.44 million. Funny People, which cost an estimated $75 million to produce, made about $71 million worldwide in theatres.    In comparison, Apatows previous directorial effort, Knocked Up, cost $33 million to produce and made over $219 million in gross receipts, while Sandlers last three movies had all made over $100 million. 

===Home media===
Funny People was released on DVD and Blu-ray Disc|Blu-ray in the USA on November 24, 2009. There is a one-disc "Unrated & Theatrical" cut and a two-disc "Unrated Edition". The Unrated cut of the film runs at 153 minutes. It was released in the United Kingdom on January 18, 2010, again, on DVD and Blu-ray.   

==Soundtrack==
{{Infobox album  
| Name        = Funny People: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       = 
| Released    = July 28, 2009
| Recorded    =
| Genre       = Soundtrack
| Length      =
| Label       = Concord Records
| Producer    =}}
{{Album ratings
| rev1      = PopMatters| rev1Score =     |noprose=yes
}}
Funny People: Original Motion Picture Soundtrack was released on July 28, 2009.
# "Great Day" by Paul McCartney (2:08) Coconut Records (2:26) Strange Sensation (4:19)
# "Carolina in My Mind" (Live) by James Taylor (4:58)
# "Keep Me in Your Heart" by Warren Zevon (3:27) Real Love" by Adam Sandler (4:56)
# "We (Early Take)" by Neil Diamond (4:11)
# "Jesus, Etc." (Live Summer 08) by Wilco feat. Andrew Bird (4:01)
# "George Simmons Soon Will Be Gone" by Adam Sandler (2:15)
# "I Am Young" by Coconut Records (3:07)
# "Memory (Cats song)|Memory" by Maude Apatow & Larry Goldings (3:53)
# "Numb as a Statue" by Warren Zevon (4:07)
# "Photograph (Ringo Starr song)|Photograph" by Ringo Starr (3:58)
# "Watching the Wheels" (Acoustic Demo) by John Lennon (3:06)

===Bonus tracks on iTunes release===
#   "Secret O Life (Live)" by James Taylor (3:55)
# "Photograph" (Live) by Adam Sandler (2:55)
# "Everybody Knows This Is Nowhere" by Adam Sandler (4:02)
# "Nighttiming" by Coconut Records (2:48)

The film also features "Joanna (Kool & the Gang song)|Joanna" by Kool & The Gang, "Three Little Birds" by Bob Marley, "Diamond Dave" by The Bird and the Bee, "Man in the Box" by Alice in Chains, "(Ive Had) The Time of My Life" by Bill Medley & Jennifer Warnes, "Walk Like an Egyptian" by The Bangles, "In Private" by Paul McCartney, "Cat Song" by Tomoko Kataoka and "Give Me Love (Give Me Peace on Earth)" by George Harrison.

The Blu-ray and 2-Disc DVD also includes Sandler performing The English Beats "Save It for Later."

Additional songs used in the films trailers are "We Will Become Silhouettes" by The Postal Service, "My Friend" by Dr. Dog, and "Nothingsevergonnastandinmyway (Again)" by Wilco.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 