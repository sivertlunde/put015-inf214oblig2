El Retorno del Hombre Lobo
{{Infobox film
| name           = El Retorno del Hombre Lobo
| director       = Jacinto Molina
| producer       = Modesto Pérez Redondo
| writer         = Jacinto Molina
| starring       = Paul Naschy Silvia Aguilar Azucena Hernández Julia Saly
| music          =
| cinematography =
| editing        = Pedro del Rey
| distributor    = Federico de la Cruz Martín  (Spain, theatrical) , Film Concept Group  (USA, theatrical) 
| released       =  
| runtime        = 92 min
| country        = Spain Spanish
| budget         =
}} 1981 Spanish horror film that is the ninth in a long series about the werewolf Count Waldemar Daninsky, played by Paul Naschy. In the United States, the film was released theatrically and on VHS as The Craving in 1985, and recently on DVD and Blu-ray as Night of the Werewolf.  
This was by far Naschys favorite Hombre Lobo film, according to interviews. Naschy considered it a remake of his 1970 classic La Noche de Walpurgis. The film had a much larger budget than previous Naschy werewolf productions. 
Naschy followed this up with a sequel, The Beast and the Magic Sword.


==Synopsis==
Waldemar Daninsky is sentenced to be executed along with Elizabeth Bathory and a number of witches. Since it is nearly impossible to truly kill him, he is left in a kind of living death, with a silver dagger through his heart and an iron mask to keep him from biting. Centuries later, the dagger is removed and Daninsky returns to activity, fighting against a revived Elizabeth Bathory and her demonic manservant.

==External links==
* 
* 

 

 
 
 
 
 
 
 


 
 