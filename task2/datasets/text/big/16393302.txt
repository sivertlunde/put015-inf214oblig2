Death's Requiem
 
 
{{Infobox film
| name = Death’s Requiem
| image =
| caption =
| director = Marc Furmie
| producer = Tim Maddocks
| executive producer =
| writer = Marc Furmie Ji Hao Lou
| starring = Jai Koutrae Raoul Agapis Darren Hawkins
| narrator =
| music =
| cinematography = Carl Robertson
| audio post = Stephen Hope David Glasser
| editing =
| production company = Maddfilms
| released = 2007
| runtime =
| country = Australia
| language = English
| preceded_by =
| followed_by =
}}
Death’s Requiem is an Australian 2007 short film. It tells of Nathan Chapel, a terminally ill comic book artist with an obsession for death. But When the Grim Reaper himself pays Nathan comes to life to haunt him, Nathans greatest fears are realized.
 Short Film.

==Synopsis==
Nathan Chapel is a terminally ill comic book artist who possesses a lifelong obsession with death and a voyeuristic preoccupation with his self-destructive neighbor Sarah. When Death himself, comes to life to haunt him, Nathan must confront his greatest fears, leading him to an act of self-sacrifice.

== External links ==
*  

 
 
 
 


 
 