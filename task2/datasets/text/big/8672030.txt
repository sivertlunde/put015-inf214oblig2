Who Killed Mary What's 'Er Name?
{{Infobox Film
 | name        = Who Killed Mary Whats Er Name?
 | image       = Who Killed Mary Whats Er Name.jpg
 | writer      = John OToole
 | starring    = Red Buttons
 | director    = Ernest Pintoff
 | producer    = George Manasse
 | distributor = Cannon Film Distributors
 | released    = November 12, 1971
 | runtime     = 90 minutes English
 | budget      = 
 | music       = Gary McFarland
 | awards      = None
 | tagline     = In the shadowy world of seedy hotels and street gangs a murder is committed, the question is...
|}}

Who Killed Mary Whats Er Name? (also known as Death of a Hooker) is a 1971 film starring comedian and actor Red Buttons and featuring Conrad Bain, Sylvia Miles, Alice Playten and Sam Waterston. Ernest Pintoff was the director, and Gary McFarland wrote the soundtrack music.

The plot centers around the murder of a prostitute in a crime-ridden low-income city neighborhood. Dismayed by the general indifference the police and neighbors show toward the murder, a resident who knew the prostitute sets out to do his own investigation of the case. 
 PG despite the fact that it dealt with topics including prostitution and murder.

Since its original theatrical release, the film was only released on VHS (distributed by Video Gems in 1982), and is now out-of-print.

==See also==

*List of films featuring diabetes

==References==
 

==External links==
*  
*  

 
 
 
 
 

 