Bananaz
 
{{multiple issues|
 
 
}}
 
{{Infobox film
| name           = Bananaz
| image          =
| director       = Ceri Levy
| writer         = Ceri Levy  Jamie Hewlett
| starring       = Damon Albarn  Jamie Hewlett
| producer       = Ceri Levy   Rachel Connors
| music          = Gorillaz  Spacemonkeyz
| cinematography = Screen International
| distributor    = 
| released       = 7 February 2008 (Berlin Film Festival)
| runtime        = 92 minutes
| language       = English
}}
Bananaz is a 2008 British documentary film directed by Ceri Levy about Damon Albarn and Jamie Hewletts alternative rock virtual band Gorillaz.

==Information==
During the years from 2000 to 2006, the director Ceri Levy filmed the creators of the Gorillaz behind the scenes, from the very first drawings and animations, to the music and the musicians, as well as the faces behind the voices of the characters and other content such as interviews with various Gorillaz collaborators and backstage footage of live concert performances. This documentary features 92 minutes of behind the scenes footage of the creators making the visuals and recording the music for the Gorillaz albums Gorillaz (album)|Gorillaz and Demon Days that was recording over the course of six years. 

==2008 screenings==
*  Bananaz premiered to the news media on 7 February at the Berlin Film Festival and to the rest of the world on 9 February. The documentary was shown in the festival until 15 February Paramount Theater.
*  The Indie Lisboa Film Festival held in Lisbon, Portugal twice screened the film on 24 April and 2 May. 
*  The documentary was shown at the Edinburgh Film Festival that takes places in Edinburgh, United Kingdom in late June. 

==DVD releases==
On 20 April 2009, it was released on PAL/Region 0 DVD format.

==References==
 

==External links==
*   (Review from Movie Digs)
*   (Review from Filmschoolrejects.com)
*   (Review from tunaflix)
*   (Review from Reuters)
*   at the Internet Movie Database

 

 
 
 
 

 