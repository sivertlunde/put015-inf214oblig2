The Story of Alexander Graham Bell
 
{{Infobox film
| name           = The Story of Alexander Graham Bell
| caption        =
| image	         = The Story of Alexander Graham Bell FilmPoster.jpeg
| director       = Irving Cummings
| producer       = Darryl F. Zanuck (producer) Kenneth Macgowan (associate producer)
| writer         = Ray Harris
| screenplay     = Lamar Trotti Boris Ingster Milton Sperling
| starring       = Don Ameche Loretta Young Henry Fonda
| music          = Ernst Toch
| cinematography = Leon Shamroy
| editing        = Walter A. Thompson
| distributor    = 20th Century Fox
| released       =    
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
}}

The Story of Alexander Graham Bell is a somewhat fictionalized 1939 biographical film of the famous inventor. It was filmed in black-and-white and released by Twentieth Century-Fox. The film stars Don Ameche as Bell and Loretta Young as Mabel, his wife, who contracted scarlet fever at an early age and became deaf. 

==Synopsis==
The first half of the film concentrates on the heros romantic, financial, and scientific struggles, starting in 1873. Most scenes are set in Boston and vicinity; a few late scenes are in London.

Henry Fonda is notable in a supporting role as the “Mr. Watson” who hears the first words ever spoken over the telephone. In a pivotal scene, Bell (Don Ameche), while working on the telephone, accidentally spills acid onto his lap and shouts in pain, “Mr. Watson, come here! I want you!”. Watson, barely able to contain his own excitement, rushes into the room and stammers out the news that he heard Bell calling out to him over the telephone receiver.&nbsp; Bell has Watson repeat his own words to him to confirm it, and the two men begin hopping around the room, with Watson yelling out a war whoop.
 manned flight, under his wifes adoring gaze.

==Cast==
 
* Don Ameche as Alexander Graham Bell
* Loretta Young as Mabel Gardiner Hubbard|Mrs. Mabel Hubbard Bell
* Henry Fonda as Thomas A. Watson 
* Charles Coburn as Gardiner Greene Hubbard 
* Gene Lockhart as Thomas Sanders 
* Spring Byington as Mrs. Hubbard 
* Sally Blane as Gertrude Hubbard 
* Polly Ann Young as Grace Hubbard 
* Georgiana Young as Berta Hubbard 
* Bobs Watson as George Sanders  Russell Hicks as Mr. Barrows  Paul Stanton as Chauncey Smith 
* Jonathan Hale as President of Western Union  Harry Davenport as Judge Rider 
* Beryl Mercer as Queen Victoria  Elizabeth Patterson as Mrs. Mac Gregor 
* Charles Trowbridge as George Pollard 
* Jan Duggan as Mrs. Winthrop 
* Claire Du Brey as Landlady  Harry Tyler as Joe Eliot 
* Ralph Remley as DArcy - Singer 
* Zeffie Tilbury as Mrs. Sanders
 

== References ==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 