Gutterballs (film)
{{Infobox film
| name           = Gutterballs
| image          = GutterBalls2008.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Balls-Out Uncut Edition DVD released by Plotdigger Films
| film name      = 
| director       = Ryan Nicholson
| producer       = Roy Nicholson   Michelle Grady   Ryan Nicholson
| writer         = Ryan Nicholson
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Dan Ellis   Nathan Witte   Mihola Terzic   Alastair Gamble   Candice Lewald
| music          = Gianni Rossi   Patrick Coble
| cinematography = Mark Atkins
| editing        = Lars Simkins
| studio         = Plotdigger Films
| distributor    = Plotdigger Films
| released       =  
| runtime        = 96 minutes
| country        = Canada
| language       = English
| budget         = $250,000
| gross          = 
}}

Gutterballs is a 2008 horror film written and directed by Ryan Nicholson.

== Plot ==
:Plot is way too long, spoilers are certainly included
On January 1, 2008 at the Xcalibur Bowling Centre, a disco-themed bowling alley in Surrey, British Columbia|Surrey, British Columbia, Egerton, the janitor, has allowed two groups of teenagers to bowl against one another after hours. The "prep", Steve, is still bitter after his best friend Jamie, the "jock", had sex with Lisa, a girl with whom Steve was smitten and even took to their senior prom. After Steve and his friends Joey, Patrick, and A.J. harass the "Transvestite|tranny", Sam, they are chastised by Jamie. This leads to a brawl between Steves team and Jamies. Lisa breaks up the fight by dropping a bowling ball on Steves foot just as Steve is about to kick Jamie again. Egerton, brandishing a shotgun, tells the teenagers to leave and they can continue their tournament the following night.

Lisa, having forgot her purse in the arcade, returns to find Steve and his friends waiting for her. Steve proceeds to violently rape Lisa while the others watch. Egerton is oblivious to Lisas cries for help, as he is downstairs cleaning up the mess the teenagers made. After he is finished raping Lisa, Steve leaves the room. While Steve is gone, A.J. anally rapes Lisa on the pool table; then its Joey who abuses her. Patrick, who believed the three were only going to "scare" Lisa, refuses to participate in the rape. Steve returns with a bowling pin and prepares to insert it into Lisa before Patrick intervenes. Steve retorts by giving Patrick the pin and ordering him to do it himself. Patrick refuses to do so at first but complies when Steve threatens to do the same to him. In the end Patrick penetrates Lisa with the bowling pin as she screams in pain. The four leave Lisa on the pool table, naked and barely conscious.
 69 position skull and crossbones.

The game continues and Sam goes to the bathroom so she can "freshen up". The person who murdered Dave and Julia is hiding in one of the stalls when Sam enters the girls restroom and Sam, frightened by the noise coming from the stall, approaches it and is pulled inside. She tries to bargain with the killer, but the killer murders her by forcing a bowling pin down her throat. The killer lifts Sams skirt up and, using the switchblade Sam tried to defend herself with, castrates Sam lengthwise. Later, Ben and Cindy leave to have sex in a storage room upstairs, but Ben leaves Cindy alone to buy a condom from the vending machine in the bathroom. He dies when the killer hits him over the head with a bowling pin and gouges his eyes out with a sharpened one. The killer finds Cindy upstairs and strangles her to death with a pair of bowling sneakers. Downstairs, Hannah searches for Julia but dies when the killer crushes her head with two bowling balls. Joey goes back behind the pin setters to fix his teams after noticing they were broken. A.J. notices the strikes appearing next to BBK on the scoreboard and questions Egerton about it, who assures him that it is still a glitch. Egerton sends A.J. to an area currently being remodeled with an "out of order" sign to put on an automatic ball polisher that Joey broke earlier. A.J. turns the ball polisher on and finds it already fixed but gets killed when the killer uses it to wax his face off.

Steve, noticing that something is amiss, leaves to find Joey behind the pin setters. Joey lies dead on the ground, headless. Steve is frightened by the killer, who bludgeons him with a bowling pin before sodomizing him with another sharpened pin. As Steve tries to crawl away, the killer smashes his head in with the bowling pin. Meanwhile, Sarah and Jamie continue their game. Jamie reaches into the ball return slot to find his bowling ball, instead discovering Joeys severed head. The two try desperately to escape the bowling alley before coming face to face with the killer. They run and hide in the basement, where they find their friends bodies. The killer appears with a shotgun and removes the bowling bag to reveal Egertons face. Sarah discovers that Jamie knew about the murders of Steve, Joey, and A.J. but not the others. Lisa appears, dressed up as well, revealing that she murdered Steve. Jamie explains that Lisa told her father, who turns out to be Egerton, about the rape and Egerton let Jamie in on the plan. When asked why he killed their friends too, Egerton explains that they didnt intervene when Lisa reentered the bowling alley and are therefore just as guilty. Another person dressed like the killer, as Egerton goes on to explain, Another killer (possibly the real "BBK") enters the basement and removes the bowling bag to reveal Patricks face. Patrick explains that he confessed his involvement to Egerton and wanted to contact the police, but Egerton let him in on the plan instead and he was the person who murdered Joey and A.J., supposedly to make it up to her. Lisa is furious that her father allowed him to live so Egerton betrays Patrick and slits his throat with the switchblade he got from Sam. Jamie, realizing hes out of control, wrestles Egerton to the ground and, after a scuffle, blows his head clean off with the shotgun. Lisa cradles her fathers body before grabbing the switchblade and trying to attack Jamie. Sarah grabs the shotgun and kills Lisa.

With the three "BBK"s dead, Sarah shoots the locks off of the emergency doors and the two survivors escape the bowling alley. Traumatized and, above all, upset that Jamie did nothing to prevent the murders, Sarah turns the gun on him, pumps it saying: . The screen cuts to black as a gunshot is heard, indicating that Sarah murdered Jamie and is on the run.

== Cast ==

* Alastair Gamble as Steve
* Mihola Terzic as Sarah
* Nathan Witte as Jamie
* Wade Gibb as Joey
* Candice Lewald as Lisa
* Jeremy Beland as Ben
* Trevor Gemma as Patrick
* Nathan Dashwood as A.J.
* Scott Alonzo as Dave
* Jimmy Blais as Sam
* Danielle Munro as Julia
* Stephanie Schacter as Cindy
* Saraphina Bardeaux as Hannah
* Dan Ellis as Egerton

== Release ==

The Balls-Out Uncut Edition was released by Plotdigger Films on DVD on April 29, 2008. The film was re-released (with an altered soundtrack) by TLA Releasing the following year on January 27, 2009.

The Pin-Etration Edition, a cut containing hardcore inserts and limited to 69 copies, was available for purchase on the Plotdigger Films website in 2011.  
 uses of documentary of the same name, about the history and usage of the word) and first in total usage for a scripted motion picture.    
 MPAA Rating Board. The film, however, was released unrated when he was told that almost twenty minutes would have to be cut to avoid an NC-17 rating. 

== Reception ==

Johnny Butane of Dread Central said that while the music was fitting and the gore was impressive, the film suffered from atrociously bad acting, and "hits all the wrong notes, even if its intentions are in the right place".   

In a review for   thrills loaded with gore and nudity set in a kitschy 80s bowling alley".   Gutterballs was described as "one of the weirdest movies Ive seen" by Kurt Dahlke of DVD Talk, who gave it a two and a half out of a possible five, and concluded that it was "a damn fine sexually explicit, graphically violent, morally repugnant, willfully bad B movie" that refreshingly took itself seriously, despite the over-the-top subject matter.  

== Sequel ==
A sequel, subtitled Balls Deep, is in development, and will be directed by Ryan Nicholson, and written by Jordan Boos.   The film stars Aidan Dee, Momona Komagata and Kirsty Elizabeth in the leading roles.  The film premiered in May 2015 as part of the Texas Frightmare Weekend. 

== References ==

 

== External links ==

*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 