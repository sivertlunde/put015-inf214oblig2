The Paper Wedding
{{Infobox film
| name           = The Paper Wedding
| image          = 
| image size     = 
| caption        = 
| director       = Michel Brault
| producer       = 
| writer         = Jefferson Lewis
| starring       = Geneviève Bujold
| music          = 
| cinematography = Sylvain Brault
| editing        = Jacques Gagné
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Canada
| language       = French
| budget         = 
}}
 Canadian film directed by Michel Brault, screenplay by Jefferson Lewis. It was entered into the 40th Berlin International Film Festival.   

==Plot==
The action is taking place in Montréal, Québec.

Claire is a teacher and lives alone. Her lover, Milosh, is married and their relationship is strained. Claires sister, Annie, a lawyer, has a problem. The visa of her client Pablo, a political refugee from Chile illegally working as a dishwasher in a restaurant, is about to expire. She asks Claire to marry him so he can remain in Canada. Claire relunctantly agrees. Before the modest civil ceremony can be concluded, immigration agents arrive, but everyone escapes. 

Claires mother is thrilled to arrange a big church wedding and reception instead. Afterwards, Claire and Pablo go their separate ways. But soon, immigration agents are back knowing whats going on and the two are forced to live together. There, Claire notices that Pablo (who has fallen for her)has nightmares. When they get to know each other, Pablo tells Claire that he was a tortured political prisoner, among other things. 

In case they are questioned by the Canadian officers, Claire and Pablo try to make up a story about how they met. Pablos romantic story touches Claire, and in time, she finds she has fallen for him as well.

==Cast==
* Claire - Geneviève Bujold
* Annie - Dorothée Berryman 
* Pablo - Manuel Aranguiz
* Mother - Monique Lepage 
* Milosh - Teo Spychalski

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 