Uuno Turhapuro muuttaa maalle
{{Infobox film
| name          = Uuno Turhapuro muuttaa maalle
| image         =
| caption       =
| director      = Ere Kokkonen
| writer        = Ere Kokkonen
| starring      = Vesa-Matti Loiri, Marjatta Raita, Tapio Hämäläinen, Olavi Ahonen
| producer      = Spede Pasanen
| distributor   = Filmituotanto Spede Pasanen Ky
| released      = 1986
| runtime       = 1h 45min
| language      = Finnish
}} Finnish film, the 11th film in the Uuno Turhapuro film series.

The film attracted a total of 556,519 viewers. It is the fourth most successful Uuno Turhapuro films. It was filmed in twenty days, from 22 May to 17 June 1986. The countryside scenes were filmed in Heinola, around the Viikinäinen vacation resort. The scene where Tuura visits a psychiatrist was filmed in Spede Pasanens office in the MTV studios.

The film shows Spedes inventions "alarm clock", "cross between a Mercedes-Benz and a Volkswagen Beetle", and "boiling machine", which were previously shown in Spedes films X-Paroni and Pähkähullu Suomi. The other inventions were designed by Ere Kokkonen.
 Parliament and Minister Paavo Väyrynen, who recites Runebergs poem Maaherra wearing a traditional Finnish national costume.
 Uuno Turhapuro muuttaa maalle was made for the Commodore 64, based on the film. The game was developed by Pasi Hytönen and published by AmerSoft, part of the Amer group. It is probably the first licensed video game based on a Finnish film.

==Cast==
{|class="wikitable"
|-
|Vesa-Matti Loiri Uuno Turhapuro
|- Marjatta Raita Elisabeth Turhapuro
|- Tapio Hämäläinen Councillor Tuura
|- Marita Nordberg
|Mrs. Tuura
|- Olavi Ahonen Hugo Turhapuro, Uunos father
|- Simo Salminen
|Sörsselssön
|- Spede Pasanen
|Härski Hartikainen
|- Helge Herala Teacher Happonen
|- Marja Korhonen
|Mrs. Happonen
|- Elli Castrén Unelma Säleikkö, secretary of Councillor Tuura
|- Mirjam Himberg Shopkeeper Rentukka
|- Jyrki Kovaleff Bank manager
|- Jukka Sipilä Agrarian councillor
|- Juhani Kumpulainen
|Heiveröinen, manager of gas station
|- Johanna Raunio
|Greta, female detective
|- Risto Aaltonen Psychiatrist
|-
|Yrjö Parjanne Minister of Traffic Reenpää
|- Paavo Väyrynen Himself
|-
|}

==Plot==
Uunos father-in-law, Councillor Tuura, is losing his nerves again. He decides to make a complete change in his life, and moves to the country together with his wife and daughter Elisabeth without telling Uuno about it. Uuno, on the other hand, has decided to try to salvage his marriage by spending a week away from home, without telling his wife where he is. When Uuno returns with flowers, his wife has moved away. Uuno finds a notice left by Elisabeth in the refrigerator, although the notice was written by Uunos father-in-law. Uuno hears that Härski Hartikainen and Sörsselssön plan to close down the car service station and go to emergency service work. Uuno joins them.
 taxi for three hundred kilometres to Helsinki to meet the Minister of Traffic and hears that the minister is on vacation in the same village where Tuura bought the mansion from. Tuura returns to the countryside and meets the minister at his summer cabin. The minister takes Tuuras business up for discussion at the Council of Traffic. At the same time, Uuno and Elisabeth go to visit Uunos father Hugo, who has all kinds of inventions at his home. Hugo is secretly brewing moonshine and fears that the sheriff will hear of it. Elisabeth thinks she has still more to learn from the men in the Turhapuro family. After the countryside work, the Turhapuro men get to their favourite pastime. Uuno also visits his old teacher.
 markka from Tuura to turn around the heads of the Council of Traffic. After a successful summer event, the Council of Traffic meets, to vote for the road to pass through Tuuras lands at the direction of the school teacher. The minister draws a line on the map. Tuura is disappointed about the issue, until Uunos father arrives and is ready to buy the mansion. Tuura laughs, but Hugo has enough money for the mansion and so they go to sign a deed. Uuno arrives to tell that he held a ruler and his thumb so that the road made a bend around Tuuras mansion. Tuura goes to follow Uunos father, who has gone to the farmhands home.

==External links==
*  
*  

 

 
 
 
 