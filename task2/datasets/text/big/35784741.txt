Yankee Buccaneer
{{Infobox film
| name           = Yankee Buccaneer
| image          =
| caption        =
| director       = Frederick de Cordova
| producer       = Howard Christie
| writer         = Charles K. Peck
| narrator       = Jeff Chandler Scott Brady Suzan Ball
| music          =
| cinematography = Russell Metty
| editing        =
| studio         = Universal International
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}} Jeff Chandler as a US Navy officer fighting pirates.

==Plot== Commander David Porter (Jeff Chandler) and his men receive orders via David Farragut|Lt. David Farragut (Scott Brady) that they are to fight piracy. They go undercover as pirates.

==Production==
The film was shot on sets left over from Against All Flags (1952). 

==Cast== Jeff Chandler Commander David Porter
*Scott Brady as David Farragut|Lt. David Farragut
*Suzan Ball as Countess Margarita La Raguna
*Joseph Calleia as Count Domingo Del Prado
*George Mathews as Link
*Rodolfo Acosta as Poulini
*David Janssen as Beckett
*Joseph Vitale as Scarjack
*Michael Ansara as Lt. Romero
*James Parnell as Redell
*Jay Silverheels as Lead warrior
*Ian Murray as Seaman

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 
 
 
 
 


 