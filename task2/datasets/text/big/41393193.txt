Savat Mazi Ladki
{{Infobox film
| name           = Savat Mazi Ladki
| image          =  
| image_size     =
| caption        = 
| director       = Smita Talwalkar
| producer       = Smita Talwalkar
| story          = 
| narrator       = 
| starring       = Varsha Usgaonkar Mohan Joshi Prashant Damle Neena Kulkarni
| music          = 
| cinematography = 
| editing        = 
| distributor    = Everest Entertainment Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
}}

Savat Mazi Ladki is a Marathi film released on August 30, 1993. Directed and produced by Smita Talwalkar. The film stars Varsha Usgaonkar, Mohan Joshi, Prashant Damle. 

The films is based on a time-tested universal truth that man is cursed with lure while women is blessed with control.

==Plot==
Dr. Madhu Hirve and Mrs. Seema Hirve is a happy couple. Seema is a responsible, clever and mature house-wife and Madhu is a well-known doctor.

Dr. Dinesh Kirtikar is his assistant anesthetist, who is a bachelor with a great sense of humor. One day Dr. Bina joins as Dr. Madhus assistant in his hospital. During her tenure Dr. Madhu is attracted towards her while Dr. Dinesh is also trying his luck with Dr. Bina.  Eventually Dr. Bina gives in to Dr. Madhus advances.

After realizing her husbands progress in his love-affair, Seema cooks up a plan. She gets Dr. Bina home as her husbands second wife. She showers love and affection on Dr. Bina while teaching her husband a lesson.

==Cast==
* Varsha Usgaonkar
* Mohan Joshi
* Prashant Damle
* Neena Kulkarni
* Ramesh Bhatkar
* Amita Khopkar

==Crew==
*Director & Producer - Smita Talwalkar

== References ==
 
 

== External links ==
*  
*  

 
 
 


 