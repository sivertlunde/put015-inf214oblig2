Saloon Bar
{{Infobox film
| name           = Saloon Bar
| image          =Saloon Bar.jpg
| image_size     =
| caption        =
| director       = Walter Forde
| producer       = Culley Forde (producer) Frank Harvey (play) Angus MacPhail (writer)
| narrator       =
| starring       = See below
| music          = Ernest Irving
| cinematography = Ronald Neame
| editing        = Ray Pitt
| distributor    =
| released       =
| runtime        = 99 minutes
| country        = UK
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 British thriller film directed by Walter Forde. It was made by Ealing Studios and its style has led to comparisons with the later Ealing Comedies, unlike other wartime Ealing films which are different in tone. 

== Plot summary ==
An amateur detective tries to clear an innocent man of a crime before the date of his execution. 

== Cast ==
*Gordon Harker as Joe Harris
*Elizabeth Allan as Queenie
*Mervyn Johns as Wickers
*Joyce Barbour as Sally
*Anna Konstam as Ivy
*Cyril Raymond as Harry Small
*Judy Campbell as Doris
*Al Millen as Fred
*Norman Pierce as Bill Hoskins
*Alec Clunes as Eddie Graves
*Mavis Villiers as Joan
*Felix Aylmer as Mayor
*O. B. Clarence as Sir Archibald
*Aubrey Dexter as Major
*Helena Pickard as Mrs Small
*Manning Whiley as Evangelist
*Laurence Kitchin as Peter
*Roddy Hughes as Doctor
*Gordon James as Jim
*Annie Esmond as Mrs. Truscott
*Eliot Makeham as Meek Man
*Roddy McDowall as Boy

==References==
 

;Bibliography
* Murphy, Robert. Realism and Tinsel: Cinema and Society in Britain 1939-48. Routledge, 1992.

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 
 