Karma (1933 film)
 
 
{{Infobox film
| name           = Karma
| image          =
| image_size     =
| caption        =
| director       = J.L. Freer Hunt
| producer       =
| writer         = Rupert Downing
| story          = Dewan Sharar
| narrator       =
| starring       =  
| music          = Ernst Broadhurst
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 68 mins. 
| country        =  
| language       = English, Hindi
| budget         =
}} bilingual film starring Devika Rani and Himanshu Rai. The film was directed by J.L. Freer Hunt and was a joint production among India, Germany and United Kingdom. Karma featured a four-minute kissing scene between the lead actors—Devika Rani and Rai—the longest in an Indian film.

==Plot==
The story is about a princess (played by Devika Rani) who falls in love with a neighbouring prince much to the disapproval of the latters father.

==Production== Nobel laureate Rabindranath Tagore was professionally associated with Rai even before the two married in 1929.  Impressed by her talent, Rai decided to cast her in the film alongside him. Abraham Sofaer was cast in a pivotal role as a "Holy Man".   The screenplay was co-written by Rai and Freer Hunt.  The music was composed by German composer Ernst Broadhurst. Devika Rani had recorded a song in the film including the Hindi version.   

Karma, an "Indo-German-British" collaboration, was released two years after the Alam Ara (1931), the first Indian talkie. Karma was made targeting the international audience.  The film was entirely shot in India while the post-production process was carried out in Stoll Studios, London.  The film was the first talkie produced by Rai. 

==Release==
 

The film initially premiered in London in May 1933. Devika Ranis performance was lauded by the critics in London. However, when the film was released later in Hindi as Naagan Ki Raagini, it failed to impress the Indian audience.       
 India to feature an on-screen kiss.   The four-minute long scene between Devika Rani and Rai, her husband in real life, is also known to being the longest such scene in Indian cinema.   Upon release, the film became controversial in the then "orthodox India" for featuring a kissing scene. 

==Legacy==
Though largely ignored in India during its release, Karma is considered a landmark in Indian cinema due to its unprecedented kissing scene. In 2012, The Times of India described it as the "first Indian talkie with English dialogue which set all London talking". 

==Notes==
 

==References==
 
* 
* 
* 
* 
* 
 

==External links==
 

 
 
 
 
 