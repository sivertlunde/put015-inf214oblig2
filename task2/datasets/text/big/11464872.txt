Kiss the Bride (2002 film)
{{Infobox film
| name        = Kiss the Bride
| image       = KisstheBride2002.JPG
| caption     = DVD cover
| writer      = Vanessa Parise
| starring    = Amanda Detmer Sean Patrick Flanery Brooke Langton Monet Mazur Alyssa Milano
| director    = Vanessa Parise
| producer    = Vanessa Parise Marco Derhy Jordan Gertner
| distributor = Metro-Goldwyn-Mayer
| released    =  
| runtime     = 89 minutes
| language    = English
| budget      = 
}} Italian family, directed by Vanessa Parise.

==Plot==
The story centers around a traditional Italian-American family with four daughters with completely different personalities. Danni, one of the sisters, is about to be the first of the sisters to walk down the aisle. That is, if her three sisters wont stop it. Niki, Chrissy and Toni return home for the long overdue family reunion, which ultimately turns into a contest of who can one-up the other. Niki brings her Jewish boyfriend along, the lesbian Toni is accompanied by her biker girlfriend Amy, and Chrissy, who is too busy for a boyfriend, brings her brand new Porsche. The sisters reek of over-achievement and insecurity and subconsciously long for the approval and love of their domineering father.

The film takes place in Westerly, Rhode Island, the hometown of director/actor/writer/producer Vanessa Parise.

==Cast==
*Amanda Detmer - Danisa Danni Sposato
*Brooke Langton - Nikoleta Niki Sposato
*Monet Mazur - Antonia Toni Sposato
*Vanessa Parise - Christina Chrissy Sposato

*Sean Patrick Flanery - Tom Terranova
*Johnathon Schaech - Geoffrey Geoff Brancato
*Alyssa Milano - Amy Kayne
*Johnny Whitworth - Marty Weinberg
*Talia Shire - Irena Sposato
*Burt Young - Santo Sposato

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 