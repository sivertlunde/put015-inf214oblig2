Uncovered: The War on Iraq
{{Infobox film
| name           = Uncovered: The War on Iraq
| image          = Uncovered- The War on Iraq poster.jpg
| image_size     = 185px
| caption        = Film poster
| director       = Robert Greenwald
| producer       = Philippe Diaz Robert Greenwald Kathryn McArdle Devin Smith
| writer         =
| narrator       =
| starring       = Jim Ervin Mars Lasar
| cinematography = Richard Ray Perez
| editing        = Chris M. Gordon Kimberly Ray
| distributor    = Cinema Libre Studio
| released       =  
| runtime        = 87 min
| country        = United States
| language       = English
| budget         =
| gross          =
}}


Uncovered: The War on Iraq is a 2004 documentary film directed by Robert Greenwald that builds the case that the George W. Bush administration intentionally deceived the American people in order to justify going to war in Iraq in 2003.        

==Synopsis==

The film is divided into seven sections.     

Part 1: The Experts consists of more than two dozen experts establishing “impeccable” non-partisan credentials.       These include former State Department, CIA, Pentagon and military officials; weapons inspectors and ambassadors; journalists and politicians.            

Part 2: Terrorism presents the thesis that the September 11, 2001 attacks provided a false pretext for the Iraq War.  This section argues that the George W Bush administration falsely claimed there was a link between Saddam Hussein and Osama bin Laden in order to convince Americans that we needed to go to war in Iraq.      

Part 3: Informers deals with the administration’s trust in unreliable, self-serving sources for intelligence regarding Iraq’s military capabilities, including the fiction of WMDs.    

Part 4: Sixteen Words exposes George W. Bush’s fake claim that Iraq had taken delivery of weapons-grade yellow-cake uranium from Niger.    The film goes on to discuss how diplomat Joseph C. Wilson leaked information that refuted this story. In retaliation, the Bush administration “outed” Wilson’s wife Valerie Plame as a covert CIA operative, costing her her job.  

Part 5: War in Iraq documents how, as the US-led invasion of Iraq began, the administration was forced to backpedal on their assertions regarding Saddam Hussein’s arsenal and intentions.  In order to justify continuing the war, they resorted to the claim that the Iraqi people were clamoring for the US and allies to free them from dictator Saddam Hussein.   

Part 6: The Cost of War tallies the exorbitant amount of US taxpayer money that had been spent on the war up to the date of production. This section takes  special note of the overblown contracts granted to large US corporate friends of the Bush administration.  

Part 7: Neocons wraps up the argument that the citizens of the US were intentionally and illegally conned into the war by a group of powerful neo-conservatives who had been looking for an excuse to invade Iraq long before the September 11 attacks.   

==Production Style==

The filmmakers intersperse clips from news shows that originally aired during the build-up to the war, and then debunk the points made in those clips with expert sound bites.         

The information presented in Uncovered was not new to its audience,     but reviews said it was organized in  a useful way     and built an “airtight” argument. 

==Reception==

Uncovered was described as “sober and meticulous” by the New York Times,  and “convincing and well-organized” by the Washington Post. 

It has been compared to Michael Moore’s Fahrenheit 9-11, which shares the same basic thesis about the reasons for the war, but is presented in a more emotional style.   

The film’s critics decry its production values, saying that it is overly reliant on talking heads, and that it plays like television,  or even radio. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 