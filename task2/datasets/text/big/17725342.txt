The Million Ryo Pot
{{Infobox film
| name           = The Million Ryo Pot 丹下左膳余話 百萬両の壺
| image          = Tange sazen yowa Hyakuman ryo no tsubo poster 2.jpg
| caption        = Theatrical release poster
| director       = Sadao Yamanaka
| producer       = Nikkatsu
| writer         = 
| narrator       = 
| starring       = Denjirō Ōkōchi Kiyozo
| music          = 
| cinematography = 
| editing        = 
| distributor    = Nikkatsu
| released       = 15 June 1935
| runtime        = 92 minutes
| country        = Japan
| language       = Japanese
| budget         =  
}} Japanese film comedy directed by Sadao Yamanaka and starring Denjirō Ōkōchi.

==Cast==
*  
* Kiyozo
* Kunitaro Sawamura
* Reisaburo Yamamoto
* Minoru Takase
* Ranko Hanai: Ogino

==Reception==
Mark Schilling of The Japan Times noted that the film was "universally considered the best of all the Tange Sazen lot." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 