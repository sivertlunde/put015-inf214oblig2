Stella Maris (1918 film)
{{Infobox film
| name           = Stella Maris
| image          = File:Stella Maris lobby card.jpg
| image size     = 
| caption        = Lobby card
| director       = Marshall Neilan 
| producer       = 
| screenplay     = Frances Marion
| based on       =  
| starring       = Mary Pickford Ida Waterman Herbert Standing
| cinematography = Walter Stradling 
| studio         = Mary Pickford Film Corporation
| distributor    = Famous Players-Lasky/Artcraft
| released       =  
| runtime        = 80 mins.
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          =
}}
 1918 American silent drama film directed by Marshall Neilan, written by Frances Marion and based on William John Lockes 1913 novel of the same name. The film stars Mary Pickford in dual roles as the title character and an orphan servant.
 remade in 1925, with Mary Philbin in the title role.

==Plot==
Stella Maris (Mary Pickford) was born paralyzed and is unable to walk. Her wealthy parents try to prevent her from being exposed to all the bad that is happening in the world. She is not allowed to leave her room in a London mansion and is bound to her bed. Her door even has a sign on it which says: "All unhappiness and world wisdom leave outside. Those without smiles need not enter."   Stella has no idea a war is going on in the world and that there are poor and hungry people.

John Risca (Conway Tearle) is a well-known journalist and a friend of the family. He has been unhappily married to Louise for six years now and frequently visits Stella. John wants Stella to think he is perfect and lies about being unmarried. Louise, meanwhile, wants a servant in her house and hires orphan Unity Blake (also Mary Pickford). Unity is uneducated and has been deprived and mistreated for her entire life. This resulted in her being afraid of everyone.

One night, a drunk Louise gives Unity the order to get some groceries from the supermarket. Unity does as told and on her way back, the food is stolen by kids. She returns to the home only to get beaten up by an outraged Louise. Unity is severely hurt and Louise gets arrested. It is announced she will have to serve three years in prison. John is kinder to Unity and adopts her. Unity is very grateful and falls in love with him. John himself is only interested in Stella. John wishes Unity to be raised at the Blounts residence, but they dont want her. They prevent her from meeting Stella, fearing Stella will notice there are suffering people in the world. They finally convince John to raise Unity at Aunt Gladys house.

In order to make John fall in love with her, Unity starts to educate herself. Meanwhile, Stella gets an operation and is able to walk after three years. She meets John and they fall in love. One day she decides to give John a surprise visit. Louise, who has just been released from jail, opens the door and tells Stella the truth about her marriage. Stella is heartbroken upon learning that he lied to her about his marriage. Feeling betrayed she tells John to leave her alone and refuses to talk to her family upon seeing how much sadness and pain are in the world.

Meanwhile, Unity uses one of Johns suits and pretends he is asking her to marry him. When he comes home heartbroken over losing Stella, she tries to busy herself with work. As she hears Aunt Gladys concerns about Johns inability to be free to love Stella while Louise lives, Unity realizes she and John can never be a couple. She writes him a note which she thanks him for showing her kindness and says he should get together with Stella. Unity secretly grabs a gun from a gun collection and settles the score by killing Louise for the pain she inflicted on her, Stella and John. She next kills herself, making the police think it was a revenge murder as her troubled history is well known even to them. Aunt Gladys convinces Stellas parents to give John another chance and not think badly about Unity for she helped free him from his abusive wife. John is reunited with Stella and they marry.

==Cast==
* Mary Pickford - Miss Stella Maris/Unity Blake
* Ida Waterman - Lady Eleanor Blount, aka Aunt Julia
* Herbert Standing - Sir Oliver Blount
* Conway Tearle - John Risca
* Marcia Manon - Louise Risca
* Josephine Crowell - Aunt Gladys Linden
* Teddy the Dog - The Mack Sennett Dog (uncredited)
* Lou Conley - The Nurse (uncredited)
* Gustav von Seyffertitz - The Surgeon (uncredited)

==Preservation status==
Stella Maris still exists with copies preserved at the Mary Pickford Institute for Film Education and the Library of Congress. 

==DVD release==
Stella Maris was released on Region 0 DVD by Milestone Film & Video on April 18, 2000. 

==References==
 

==External links==
* 

 


 
 
 
 
 
 
 
 
 
 