Hello Down There
{{Infobox film
| name           = Hello Down There  (also known as "Sub-a-Dub-Dub")
| image          = Hello down there.jpg
| caption        =  Jack Arnold  Ricou Browning
| producer       = George Sherman  Ivan Tors
| writer         = Art Arthur  John McGreevey  Frank Telford  Ivan Tors 
| starring       = Tony Randall  Janet Leigh  Jim Backus  Ken Berry  Roddy McDowall  Charlotte Rae
| music          = Jeff Barry
| cinematography = Clifford H. Poland Jr.
| editing        = Erwin Dumbrille Ivan Tors Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States English
| budget         = 
}}
 musical comedy Jack Arnold and Ricou Browning and produced by George Sherman and Ivan Tors from a screenplay by John McGreevey and Frank Telford. It starred Tony Randall and Janet Leigh. The film was reissued in 1974 under the title Sub-a-Dub-Dub.

==Plot==
Fred Miller (Tony Randall) must prove that his new design for an underwater home is viable by convincing his family to live in it for thirty days. His son and daughter (Gary Tigerman & Kay Cole) are members of an emerging pop rock band (Richard Dreyfuss & Lou Wagner) whom they invite to live with them during the experiment. Their temporary home, which Miller dubs the "Green Onion," is 90 feet below the surface of the ocean and is filled with super-modern appliances and amenities for house-wife Vivian (Janet Leigh) all designed by Miller, and a hole in the floor providing direct access to the sea.
 seal named Gladys and a pair of dolphins (Duke and Duchess) which stay close at hand and fend off unwanted sharks. They are confronted by many obstacles including a rival designer (Ken Berry) from Undersea Development Inc. who begins to cause problems for the inhabitants of the "Green Onion". 
 Navy is alerted by the sounds of the music coming from the sea. As naval fleets swarm in to investigate what must surely be a Communist plot, the movie abruptly ends.

==Cast==
* Tony Randall as Fred Miller
* Janet Leigh as Vivian Miller
* Jim Backus as T.R. Hollister
* Ken Berry as Mel Cheever
* Roddy McDowall as Nate Ashbury
* Charlotte Rae as Myrtle Ruth
* Richard Dreyfuss as Harold Webster
* Kay Cole as Lorrie Miller
* Gary Tigerman as Tommie Miller
* Lou Wagner as Marvin Webster Bruce Gordon as Adm. Sheridan
* Frank Schuller as Alan Briggs
* Arnold Stang as Jonah
* Harvey Lembeck as Sonarman
* Merv Griffin as Himself
* Henny Backus as Mrs. Webster
* Pat Henning as Reilly
* Jay Laskay as Philo
* Bud Hoey as Mr. Webster
* Charles Martin as Chief Petty Officer
* Frank Logan as Captain
* Andy Jarrell as Radioman
* Lora Kaye as Secretary

==Notes==
 

==References==
*Weiler, A. H., " ", (1969-06-26). The New York Times. Retrieved 2009-03-19.

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 