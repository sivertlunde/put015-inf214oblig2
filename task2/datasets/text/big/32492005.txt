3 Musketeers (film)
{{Infobox film
| name           = 3 Musketeers
| image          = 3musketeersdvd.jpg
| alt            =  
| caption        = DVD Cover
| director       = Cole McKay
| producer       =  
| writer         = Edward DeRuiter
| screenplay     = 
| story          = 
| based on       = The Three Musketeers by Alexandre Dumas (uncredited)
| starring       =  
| music          = Chris Ridenhour
| cinematography = Ben Demaree
| editing        = 
| studio         = 
| distributor    = The Asylum
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $300,000 
| gross          = 
}} action film The Three Musketeers. The film was released on DVD and Blu-ray disc on October 25, 2011.   

Unlike other adaptations of The Three Musketeers, this film is a modern take on the original story.

== Plot ==
Set in the near-future USA, when junior NSA officer Alexandra DArtagnan uncovers a plot by a corrupt general to assassinate the President of the United States to instigate a coup to take over the government in order to install a militarist regime, she enlists the help of three infamous international spies to stop the threat.

==Cast==
*Heather Hemmens as Alexandra DArtagnan XIN as Athos Keith Allan as Porthos
*Michele Boyd as Aramis
*David Chokachi as Lewis
*Darren Thomas as Rockford
*Alan Rachins as Treville
*Simon Rhee as a Commander
*Andy Clemence as President King
*Edward DeRuiter as Jenkins

==References==
 

==External links==
*   at The Asylum
*  

 
 

 
 
 
 
 
 
 
 
 
 


 