True Heart Susie
 
{{Infobox film
| name           = True Heart Susie
| image          = Trueheartsusie1919movieposter.jpg
| caption        = Film poster
| director       = D. W. Griffith
| producer       = D. W. Griffith
| writer         = Marian Fremont
| starring       = Lillian Gish
| cinematography = G. W. Bitzer
| editing        = James Smith
| released       =  
| runtime        = 86 minutes 
| country        = United States 
| language       = Silent (English intertitles)
| studio         = D.W. Griffith Productions
| distributor    = Artcraft Pictures Corporation  
| budget         = 
}}
 
True Heart Susie is a 1919 American drama film directed by D. W. Griffith and starring Lillian Gish. A print of the film survives in the film archive of the British Film Institute.    The film has seen several VHS releases as well as a DVD issue.

==Plot==
As described in a film magazine,    "True Heart Susie" (Gish) lives with her aunt (OConnor) and loves stupid William Jenkins (Harron). Her love is so great that she sacrifices the family cow, a pet of hers, and other farm produce so that he can go to college, but the benefaction is a secret one, and he finishes his theological studies without suspecting that she aided him. He has impressed her that she must dress as plainly as possible, and she is so attired when she goes with him for a "Soft drink|sody" on his triumphant return from college, but his eyes wonder to girls giving a more attractive expression of themselves. After he becomes a minister, he cruelly consults Susie about the policy of taking a wife, and almost breaks her heart when he weds gay Bettina "Betty" Hopkins (Seymour), expecting his bride to adopt herself to his colorless life. The young wife fails to satisfy her husband with her cooking, with William finding the dishes Susie makes more to his taste. He begins to regret his marriage, and so does his wife, who escapes the monotony of her marriage by attending a dance at a neighboring house. After she loses her key and gets caught in the rain on the way home, Betty appeals to Susie, who shields her from the consequences as far as the minister is concerned. However, Bettys fright and her soaking bring on a fatal sickness, and it is after her death that her husband learns of her escapade. Although he swears never to marry again, he finds that True Heart Susie has given the one opportunity of his life, and he returns to her with the offering of his hand in marriage.

==Cast==
* Lillian Gish as True Heart Susie
* Robert Harron as William Jenkins
* Clarine Seymour as Bettina Hopkins
* Kate Bruce as Bettinas aunt Raymond Cannon as Sporty Malone
* Carol Dempster as Bettinas friend
* George Fawcett as The Stranger
* Wilbur Higby as Williams father
* Loyola OConnor as Susies aunt

==See also==
* D. W. Griffith filmography
* Lillian Gish filmography
* The House That Shadows Built (1931 promotional film by Paramount)

==References==
 

==External links==
 
* 
* 
*  available for free download at  

 

 
 
 
 
 
 
 
 