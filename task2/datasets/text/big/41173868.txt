Under the Southern Cross (1929 film)
 
 
{{Infobox film
| name = Under the Southern Cross
| image =
| caption =
| director = Lew Collins
| producer = Lew Collins
| writer =
| starring =
| music = Bathie Stuart
| cinematography =
| editing = Hugh Hoffmann
| distributor =
| released =  
| runtime = 5579 ft US silent 5606 then 6279 ft UK 6642 ft US sound  English
| budget =
| gross =
}} White Island, which has an active volcano.
 Down on the Farm, Hei Tiki and On the Friendly Road) which lay claim to be the first "New Zealand talkie", although dubious as the sound was added to the 1930 release in the United States.
 Under the Southern Cross, directed by Gustav Pauli.

==Plot==
In pre-European New Zealand there are two hostile Māori tribes. The chief of one tribe proposes to marry his daughter Miro into the other tribe, the Waiti. But a contest, The Challenge of the Spear, must be held, with the victor to marry Miro. Rangi, a vicious warrior wins by trickery. Miro is by tapu forbidden from seeing her true love Patiti. But Patiti rows across the lake to see her nightly, until the suspicious Rangi finds them. In a deadly struggle on the edge of the volcano, Patiti forces Rangi into the volcano. War resumes, but love brings a compromise and Miro and Patiti marry.

==Cast==
*Patiti Warbrick as Patiti
*Witarina Mitchell as Miro

==References==
*New Zealand Film 1912-1996 by Helen Martin & Sam Edwards p42 (1997, Oxford University Press, Auckland) ISBN 019 558336 1

==External links==
*  
*  

==References==
 

 
 
 
 
 
 
 
 
 