The Picture of Dorian Gray (1918 film)
 
{{Infobox film
| name           = The Picture of Dorian Gray
| image          = 
| caption        = 
| director       = Alfréd Deésy
| producer       = 
| writer         = József Pakots Oscar Wilde (novel)
| starring       = Norbert Dán Béla Lugosi
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Hungary
| language       = Silent
| budget         = 
| gross          = 
}} of the same name.

==Cast==
* Norbert Dán as Dorian Gray
* Béla Lugosi as Lord Henry Wotton (as Arisztid Olt)
* Lajos Gellért as Jim (as Viktor Kurd)
* Annie Góth as Princess Marborough
* Ella Hollán
* Richard Kornay as The prince
* Ila Lóth as Sibyl Vane
* Gusztáv Turán as Basil Hallward
* Camilla von Hollay as Hetti (as Kamilla Hollay)

==See also==
* Béla Lugosi filmography
* Adaptations of The Picture of Dorian Gray

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 