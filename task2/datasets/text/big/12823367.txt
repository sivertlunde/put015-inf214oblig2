Art Heist
{{Infobox film
| name           = Art Heist
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Bryan Goeres
| producer       = Manual Corbi
| writer         = Diane Fine Evan Spiliotopoulos
| narrator       = 
| starring       = Ellen Pompeo William Baldwin Abel Folk Sean Murray
| cinematography = Jacques Haitkin
| editing        = 
| studio         = 
| distributor    = 
| released       = July 13, 2004
| runtime        = 98 minutes
| country        = Spain United States
| language       = English Spanish Russian Catalan
| budget         = 30 million
| gross          = $130-150 million
| preceded by    = 
| followed by    = 
}} Spanish actor Abel Folk. Written by Diane Fine and Evan Spiliotopoulos, produced by Manual Corbi, and released on July 13, 2004. http://www.imdb.com/title/tt0375560/ 

==Plot summary== MNAC art museum in Barcelona|Barcelona, Spain. The museum has many security features that are problematic for the thieves, but because of their skill they are able to circumvent the security and steal the famous El Greco Christ Carrying the Cross on display courtesy of Victor Boyd (Ed Lauter.) 

After the first heist, Sandra Walker (Ellen Pompeo), is called by her boss Victor Boyd and sent to Spain to ensure his El Greco is returned. Victor Boyd, the owner of the stolen El Greco, is a very rich and powerful businessman who Sandra works for as an art consultant. She is called in to find the stolen El Greco because she persuaded Victor to display it at the MNAC art museum, from which it was ultimately stolen.
 
While getting ready for her trip to Spain to track down the missing El Greco, the audience is given some insight into Sandra’s life: She is separated from an NYPD detective, Bruce (William Baldwin), and they have a young daughter together, Allison (Madison Goeres.) The couple appears to be going through a bit of a rough patch, but still seem to love each other. Sandra is able to convince her husband to watch their daughter while she’s away. Bruce is skeptical of the whole situation and worried about Sandra’s well-being.

Upon her arrival at the crime scene in Barcelona, Sandra is reunited with an old colleague of hers, Daniel (Abel Folk), who was brought in to consult on the investigation. It quickly becomes apparent these two have a deep history and there are many secret feelings. The two of them make a great team and quickly narrow down the list of suspects to the ruthless Russian mobster Dimitri Maximov (Simon Andreu.) Dimitri is portrayed as a questionable individual who seems to be scheming some sort of master plan as the art heists continue. This plays right into Sandra and Daniel’s suspicion that he is the culprit. 
Another El Greco is stolen during an auction that both Sandra and Daniel as well as all potential suspect attend.  While in the bathroom, Sandra observes one thief escaping. After alerting Daniel, they pursue the suspect through Barcelona but crash into a restaurant.
After waking up in the hospital, Bruce and their daughter arrive from New York. Soon after, Sandra’s life is threatened in a park where she was together with her daughter and husband. The attacker demands that she leave Spain. However, Bruce chases them away and follows them on a motorbike. Although he manages to catch up with one attacker, they eventually flee after knocking him down. However, he managed to see the face of one of them as well as ripping a piece of his clothing which gave him a clue to a night club.
When Maximovs yacht gets blown up with all the paintings, the original theory seems confirmed. Several clues point towards Sandra as a main accomplice. However, Sandra’s partner in the investigation, Daniel, who had been nothing but helpful and affectionate turns out to be the villain in a dramatic ending scene. 
After Bruce and Sandra discover the paintings in Daniels studio, Daniel kidnaps their daughter and sets up a meeting at a closed amusement park overlooking the sea where he arrives by helicopter with the last surviving thief (the others having been killed by him to keep his fee to himself).
Daniel, in his capacity as an art professor, had been using his students to steal the art under orders from Sandra’s boss, Victor Boyd. Victor planned to frame Sandra by depositing large amounts of money in her bank accounts to make it appear as though she sold security information to the robbers. As the final scene unfolds and all of this information is revealed to the audience, we ask “Why?” As it turns out, Daniel was frustrated with his persistent failures as an artist while other artists around him flourished. He participated in this long and carefully thought-out scheme for his own revenge and Victors promise to help him become famous.
At the last moment, Bruce comes to the rescue of his estranged wife and daughter, only to be shot by Victor after subduing Daniel. Although it appeared as though Daniel and Victor were going to make a dramatic helicopter getaway, after Victor explained that he planned to kill Sandra and her daughter, Daniel had a change of heart and tried to protect them from Victor; only to be shot. Right before his death, Daniel passed Bruce his gun and with it Bruce was able to kill Victor. The movie ends with Sandras family reunited.

==Cast==
William Baldwin as Bruce Walker

William Baldwin plays a New York Police officer who has recently been separated from his wife, played by Ellen Pompeo. His character is determined to discover who framed his wife and bring his family back to New York.
Ellen Pompeo as Sandra Walker

Ellen Pompeo plays character Sandra Walker, an art agent from New York who is summoned to Barcelona, Spain by Victor Boyd in order to recover his stolen El Greco. She discovers she has been framed by those who are the closest to her and must discover the truth in order to protect herself and her family.

Abel Folk as Daniel

Abel Folk plays an old boyfriend of Sandra who appears to be helping her in the case to find who stole the missing painting. Throughout the film he turns from friend to foe and had a large role in the setup to frame Sandra.

Simon Andreu as Dimitri Maximov

Simon Andreu plays Dimitri Maximov, the head of the Russian mafia who is the prime suspect of the painting robbery.

Ed Lauter as Victor Boyd

Ed Lauter plays art collector Victor Boyd who summons Sandra Walker to Barcelona, Spain in order to find his missing painting.

==Art pieces stolen throughout movie==
‘Christ Carrying the Cross’  – Painted by the famous El Greco (1541 – 7 April 1614), this oil painting of Christ during a time of suffering was completed in the year 1595 in Spain. In it, Greco seeks to portray the haunting immediacy and resonant with pathos. Christ’s willingness to be sacrificed shows his devotion to mankind and can be seen through his gentle grip of the cross as seen here.

‘St. John the Evangelist’  – Painted by the famous El Greco (1541 – 7 April 1614), this oil painting produced in Spain was completed around the year 1604. One of Christ’s disciples, St. John the Evangelist  is depicted here as a handsome young individual at a time of his following to Christ. In his hand is a cup of poison to resemble a liquid acquired from a dragon to symbolize ‘evil emerging’. His appearance is said to lead viewers to believe that he is a person who has the power to work miracles. This piece of art can be viewed at the (Aaron and B Lima Shickman Old Masters Galleries.) 
 Samson and Lucas Cranach (1472 – 1553), this oil painting displayed on hard wood is one of many versions of this style of painting. In it, Philistine Delilah is cutting the hair of Samson, which displays the sapping of his strength. You can see Samson is laying his head on Delilah’s lap motionless due to his weakness. This painting is to warn men of the pitfalls of love and how it could drain one of their powers. It also shows how women are conniving in their ways to draw in men.
 Old Man’  - Painted by the famous El Greco (1541 – 7 April 1614), this oil painting produced in Spain was completed around the year 1604. This Spanish portrait of this old man during the Late Renaissance resembles the stress this time put on many people. Having painted various styles of Christ and other religious figures, Greco this time portrays what seems to be himself as this old man. It is not proven that this is a portrait of Greco, but the characteristics in the face and style of his cloths give good indication that this is a self-portrait of the artist himself. You can find this painting in Metropolitan Museum of Art, New York City.

Coronation of the Virgin’  - Painted by the famous El Greco (1541 – 7 April 1614), this oil painting produced in Spain was completed around the year 1591. Scholars have presumed that Greco created the first version of this elaborate painting which displays Christ, Virgin Mary, and God-the-Father. The dove above them symbolizes peace, which through the sacrifice of Christ himself, was what the world would receive in return for his actions. This painting can be found in the Museo del Prado, Madrid.

==Production== Barcelona province of Catalonia.   

==Music==
1. “Introitus Requiem” http://www.youtube.com/watch?v=h4GBSO4fXIw
https://www.google.com/search?q=Introitus+Requiem&ie=utf-8&oe=utf-8&aq=t&rls=org.mozilla:en-US:official&client=firefox-a  by Wolfgang Amadeus Mozart, published by Selected Sounds (ASCAP), courtesy of Associated Production Music, LLC.

2. “Another Face” written by Joan Rectoret & Sonia Sanchez, performed by FLAM&CO.

==Reception==
Art Heist has been widely criticized for having a lackluster plot and weak dialogue by users and critics alike on IMDb, Rotten Tomatoes, and Netflix. Ratings range from 2.6/5 stars on Rotten Tomatoes,  with one critic review each for fresh and rotten, to 4.2/10 on IMDb  with overwhelmingly negative reviews from critics. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 