Austin Powers in Goldmember
 
 
{{Infobox film name   = Austin Powers in Goldmember image  = Austin Powers in Goldmember.jpg caption = Theatrical release poster director = Jay Roach producer = {{Plainlist|
* Jan Blenkin
* John S. Lyons
* Eric McLeod
* Demi Moore
* Mike Myers}} screenplay = {{Plainlist|
* Mike Myers
* Michael McCullers}} based on =   starring = {{Plainlist| 
* Mike Myers
* Beyoncé Knowles
* Seth Green
* Michael York
* Robert Wagner
* Mindy Sterling
* Verne Troyer
* Michael Caine}}
| music = George S. Clinton
| cinematography = Peter Deming
| editing = {{Plainlist|
* Greg Hayden
* Jon Poll}}
| studio = {{Plainlist|
* Gratitude International
*Team Todd
*Moving Pictures}} distributor = New Line Cinema released =   runtime = 94 minutes country = United States language = English budget = $63 million gross = $296.7 million}} Michael York, Verne Troyer, Michael Caine, Mindy Sterling and Fred Savage. There are a number of cameo appearances including Steven Spielberg, Kevin Spacey, Britney Spears, Quincy Jones, Tom Cruise, Danny DeVito, Katie Couric, Gwyneth Paltrow, John Travolta, Nathan Lane, and The Osbournes.
 film within the film in the opening. Austin Powers is featured in a bio-pic called Austinpussy (a parody of the James Bond film Octopussy) directed by Steven Spielberg and starring Tom Cruise as Austin Powers, Gwyneth Paltrow as Dixie Normous, Kevin Spacey as Dr. Evil, Danny DeVito as Mini-Me, and John Travolta as Goldmember.
 You Only The Spy Live and The Man with the Golden Gun and GoldenEye. The film grossed $296.6 million at the box office internationally.

==Plot== the well Austin Powers and the British Secret Service attack and arrest Dr. Evil. Austin is knighted for his services, but is disappointed when his father, the famous super-spy Nigel Powers, fails to attend the event. At a party to celebrate his knighthood he sings a song with the band Ming Tea; later he meets two Japanese twins named Fook Mi and Fook Yu and is about to have a threesome with them when Basil Exposition informs Austin that his father has been kidnapped, the only clue being that the crew of his yacht have had their genitalia painted gold.
 mole named Fat Bastard, sumo wrestler. After a humorous fight between Austin and Fat Bastard, Foxxy arrests Fat Bastard who tells them that a Japanese business man, Mr. Roboto, is working on a device for Dr. Evil and Goldmember.

Austin and Foxxy later meet with Mr. Roboto, who pleads ignorance about Nigels whereabouts. Unconvinced, Austin and Foxxy infiltrate Robotos factory where the command unit for the tractor beam is being loaded in Goldmembers car, and Roboto hands Goldmember a golden key needed to activate the beam. Foxxy confronts Goldmember while Austin attempts to free Nigel, but Goldmember escapes with the command unit and flees to Dr. Evils sub. Unable to settle their differences, Nigel and Austin part ways when they disagree on how to deal with the situation. Meanwhile, Dr. Evils son,  . Dr. Evil replaces Mini-Me with Scott as his favored son; the rejected Mini-Me defects and joins Austin.
 adapted into a film by Steven Spielberg, starring Tom Cruise as Austin, Kevin Spacey as Dr. Evil, Danny DeVito as Mini-Me, and John Travolta as Goldmember. Austin, Foxxy, Dr. Evil, Mini-Me and Nigel are in the audience of a Hollywood theater watching the film. Upon exiting the theater they bump into Fat Bastard, now thinner but flabby, thanks to the Subway diet. As Austin and Foxxy kiss, in Dr. Evils Hollywood lair, Scott – now completely bald, dressed like and laughing in a manner similar to his father – declares he will get his revenge on Austin and begins dancing like the singer Michael Jackson.

==Cast==
  Austin Powers Fat Bastard
** Aaron Himelstein as young Austin Powers Josh Zuckerman as young Dr. Evil
* Beyoncé|Beyoncé Knowles as Foxxy Cleopatra  Michael York as Basil Exposition
** Eddie Adams as young Basil Exposition
* Michael Caine as Nigel Powers 
** Scott Aukerman as young Nigel Powers  Number Two
** Rob Lowe as Middle Number Two
** Evan Farmer as Young Number Two
* Seth Green as Scott Evil
* Verne Troyer as Mini-Me
* Mindy Sterling as Frau Farbissina Number Three / Mole
* Masi Oka as The Japanese Copyright Guy
* Diane Mizota as Fook Mi
* Carrie Ann Inaba as Fook Yu
* Nobu Matsuhisa as Mr. Roboto
* Neil Mullarkey as Physician
* Tiny Lister as Prisoner
* Jim Piddock as Headmaster
* Esther Scott as Judge
* Leyna Nguyen as Anchorwoman
* Jeannette Charles as Queen Elizabeth II
* Brian Tee as Japanese Pedestrian  Radar Operator Johnson Ritter Michael McDonald as Royal Guard
* Donna DErrico as Female Vendor
* Fred Stoller as Melon Guy
* Greg Grunberg as the shirtless fan with the letter "T" (Gregs brother Brad Grunberg is the fan with the "A")
* Kinga Philipps as Mrs. Powers
* Kevin Stea as Assistant Director of "Austinpussy" / Dancer
* Anna-Marie Goddard, Nina Kaczorowski, and Nikki Ziering as henchwomen
* Ming Tea as Themselves Rachel Roberts as The Model
* Susanna Hoffs as Gillian Shagwell
* Matthew Sweet as Sid Belvedere Christopher Ward as Trevor Algberth
* Nathan Lane as Mysterious Disco Man
* Katie Couric as Prison Guard
* Kristen Johnston as Dancer at Austins pad

;Cameos
* Tom Cruise as himself as Austin Powers (during Austinpussy)
* Danny DeVito as himself as Mini-Me (during Austinpussy)
* Gwyneth Paltrow as herself as Dixie Normous (during Austinpussy)
* Kevin Spacey as himself as Dr. Evil (during Austinpussy)
* Steven Spielberg as himself
* Quincy Jones as himself
* John Travolta as himself as Goldmember (during the Austinpussy ending)
* Britney Spears as herself as Fembot ("Boys (Britney Spears song)|Boys" music video)
* Ozzy Osbourne as himself
* Sharon Osbourne as herself
* Kelly Osbourne as herself
* Jack Osbourne as himself
* Willie Nelson as himself
* Burt Bacharach (during the credits) as himself
* Brad Pitt (uncredited)
 

==Production==
===Title concerns===
The title of the film, Goldmember, led to legal action being taken by  . 

===Characters=== Austin Powers Fat Bastard Foxy Brown Christie Love Michael York, reprising the role of Basil Exposition, and Verne Troyer in his second appearance as Mini-Me. The film also introduced a new character named Number 3 (a.k.a. the Mole), who is portrayed by former child star Fred Savage. Clint Howard plays a radar operator in all three films. Michael Caine guest stars as Austins father, Nigel; this role was inspired by The Ipcress File, a 1965 film starring Caine.   
 Michael McDonald (the Virtucon guard who got run over by a steamroller in "International Man of Mystery" and a NATO soldier in "The Spy Who Shagged Me") appears as the royal guard.

====Goldmember====
Johan van der Smut, better known as Goldmember, is a fictional villain played by Myers (John Travolta played the character in a cameo at the end of the film). The name was inspired by the James Bond villain Auric Goldfinger. Goldmembers Dutch origins and character traits were, according to Myers, inspired by an episode of the HBO TV series Real Sex featuring a Dutchman who operated a "sex barn" north of Rotterdam. The mans distinct forms of expression caught Myers attention while he was writing. 

==Release==
===Box office===
Austin Powers in Goldmember took in Pound sterling|£5,585,978 in the United Kingdom on its opening weekend. In the United States, it broke the opening weekend record for a spoof movie, surpassing the previous Austin Powers film. The film grossed US$73 million on its opening weekend, and grossed a total of $213 million in the United States, according to Box Office Mojo.

===Critical reaction===
The film received mixed reviews from critics, earning a score of 54% on Rotten Tomatoes. 
 Best Villain The Ring, Best Comedic Performance, making it the first time he won the award, having previously lost twice for the first two films.

===Awards===
 
{| class="wikitable" border="1" align="center" style="font-size: 90%;"
|-
! Award
! Category
! Name
! Outcome
|-
| 2003 MTV Video Music Awards || Best Video From a Film || "Boys (The Co-Ed Remix)" by Britney Spears  (feat. Pharrell)  ||  
|- BMI Film & Television Awards || BMI Film Music Award || George S. Clinton ||  
|-
| rowspan="2"| Black Reel Awards of 2003 || Best Breakthrough Performance || Beyoncé|Beyoncé Knowles ||  
|-
| Best Song || Beyoncé Knowles, Work It Out (Beyoncé Knowles song)|"Work It Out" ||  
|-
| rowspan="2"| Canadian Comedy Awards || Film – Pretty Funny Male Performance || Mike Myers ||  
|-
| Film – Pretty Funny Writing || Mike Myers ||  
|- Empire Awards 2003 || Best Actor || Mike Myers ||  
|-
| Scene of the Year || The opening sequence ||  
|- Best Costume Design || Deena Appel ||  
|- Best Original Song || "Work It Out" ||  
|-
| Best Overall DVD || ||  
|-
| rowspan="2"| Hollywood Makeup Artist and Hair Stylist Guild Awards || Best Character Hair Styling – Feature || Candy L. Walken, Jeri Baker, Susan V. Kalinowski ||  
|-
| Best Period Hair Styling – Feature || Candy L. Walken, Jeri Baker, Susan V. Kalinowski ||  
|-
| rowspan="4"| 2003 Kids Choice Awards || Favorite Movie || ||  
|-
| Favorite Female Butt Kicker || Beyoncé Knowles ||  
|-
| Favorite Male Movie Star || Mike Myers ||  
|-
| Favorite Fart in a Movie || ||  
|- Best Comedic Performance || Mike Myers ||  
|- Best Villain || Mike Myers ||  
|-
| Best Female Breakthrough Performance || Beyoncé Knowles || 
|- 29th Saturn Best Costume || Deena Appel ||  
|-
| rowspan="2"| 2003 Teen Choice Awards || Choice Movie Actor – Comedy || Mike Myers ||  
|-
| Choice Movie Breakout Star – Female || Beyoncé Knowles ||  
|}

==Soundtrack==
{{Infobox album
| Name = Austin Powers in Goldmember
| Type = soundtrack
| Artist = Various artists
| Cover = Album-APIG.jpg
| Background = gainsboro
| Released = July 16, 2002
| Recorded = rock
| Length = 62:30 Warner Bros.
| Producer =
| Misc = {{Extra chronology Austin Powers series
| Type = soundtrack
| Last album =   (1999)
| This album = Austin Powers in Goldmember (2002)
| Next album = 
}}}}
{{Album ratings
| rev1 = AllMusic
| rev1Score =   
| noprose = yes
}}
 Sing a Song" by Earth, Wind & Fire, "Get Down Tonight", "(Shake, Shake, Shake) Shake Your Booty", and "Thats the Way (I Like It)", all by KC and the Sunshine Band.

"Sing a Song" is not listed in the credits but is sung by Beyoncé at the beginning.

# "Stay (Black Stone Cherry song)|Stay" - The Rolling Stones Work It Out" – Beyoncé Miss You"  (Dr. Dre Remix 2002)  – The Rolling Stones
# "Boys (Britney Spears song)|Boys"  (Co-Ed Remix)  – Britney Spears  (feat. Pharrell of N*E*R*D) 
# "Helluva Life" - Smash Mouth and They Might Be Giants
# "Groove Me" – Angie Stone Devin
# Shining Star" – Earth, Wind & Fire Devin and Solange Knowles|Solange) 
# "Aint No Mystery" – Smash Mouth Evil Woman" – Soul Hooligan  (feat. Diana King) 
# "1975" – Paul Oakenfold  (which samples "A Fifth of Beethoven" by Walter Murphy) 
# "Hard Knock Life (Ghetto Anthem)"  (Dr. Evil Remix)  – Dr. Evil Austin Powers) 
# "Goodnight Kiss" - Earth, Wind & Fire Bad Boys" Inner Circle Alfie (Whats It All About, Austin?)" – Susanna Hoffs
# "Soul Bossa Nova" – Quincy Jones Beyond the Sea" -  Bobby Darin

==See also==
* Story within a story
* Outline of James Bond

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 