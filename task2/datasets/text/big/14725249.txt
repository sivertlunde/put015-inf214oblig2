Into the Sun (1992 film)
{{Infobox film
| name           = Into the Sun
| image          = Into the Sun poster.jpg
| caption        = 
| director       = Fritz Kiersch
| producer       = Kevin M. Kallberg Oliver G. Hess Jim Begg Mark Amin John Brancato Michael Ferris
| starring       = Michael Paré Anthony Michael Hall Deborah Moore Randy Miller
| cinematography = Steve Grass
| editing        = Barry Zetlin
| distributor    = Trimark Pictures
| released       =    
| runtime        = 101 min USA
| English
| budget         = 
}} 

Into the Sun is a 1992 action comedy film starring Michael Paré and Anthony Michael Hall.   

== Synopsis ==
Paul Watkins (Michael Paré) is an American pilot stationed in the Middle East, who is taken away from his normal duties of protecting the skies to show an actor everything he needs to know about being a pilot in the Air Force. The actor, Tom Slade (Anthony Michael Hall), is about to start filming an Air Force movie and wants to method acting|"get the feeling" for the part. When Watkins takes Slade for a ride in an F-16 fighter, some other American jets are attacked and the pair comes to the rescue. However, it does not go according to the plan.

== Cast ==
* Anthony Michael Hall - Tom Slade
* Michael Paré - Capt. Paul Watkins
* Deborah Moore - Maj. Goode (as Deborah Maria Moore)
* Terry Kiser - Mitchell Burton
* Brian Haley - Lt. DeCarlo
* Michael St. Gerard - Lt. Wolf
* Linden Ashby - Dragon
* Melissa Moore - Female Sergeant

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 


 