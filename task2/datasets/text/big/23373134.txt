Le tartuffe
{{Infobox film
| name           = Le tartuffe
| image          = 
| caption        = 
| director       = Gérard Depardieu
| producer       = Margaret Ménégoz
| writer         = Molière
| starring       = Gérard Depardieu
| music          = 
| cinematography = Pascal Marti
| editing        = Hélène Viard
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = France
| language       = French
| budget         = 
}}

Le tartuffe is a 1984 French comedy film directed by and starring Gérard Depardieu. It was screened in the Un Certain Regard section at the 1984 Cannes Film Festival.   

==Cast==
* Gérard Depardieu - Tartuffe
* François Périer - Orgon
* Yveline Ailhaud - Dorine
* Paule Annen - Madame Pernelle
* Paul Bru - Un exempts
* Elisabeth Depardieu - Elmire
* Dominique Ducos - Flipote
* Noureddine El Ati - Laurent
* Bernard Freyd - Cleante
* Hélène Lapiower - Marianne
* Jean-Marc Roulot - Valère
* Jean Schmitt - Monsieur Loyal
* André Wilms - Damis

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 