Yahşi Batı
{{Infobox film
| name           = Yahşi Batı
| image          = Yahsi-bati-afis.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Ömer Faruk Sorak
| producer       = Murat Akdilek Cem Yılmaz
| writer         = Cem Yılmaz
| narrator       =
| starring       = Cem Yılmaz Ozan Güven Demet Evgar Özkan Uğur Zafer Algöz
| cinematography = Mirsad Herović
| editing        = 
| distributor    = UIP
| released       =  
| runtime        = 119 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = $13,733,563
| preceded_by    = 
| followed_by    =
}}

Yahşi Batı is a 2010 Turkish comedy film, directed by Ömer Faruk Sorak, which stars Cem Yılmaz as special agents tasked by the Sultan of the Ottoman Empire to deliver a diamond gift to the American president. The film, which went on nationwide general release across Turkey on  , was one of highest-grossing Turkish films of 2010. The title Yahşi Batı ("Mild West") is a play on the term Vahşi Batı ("Wild West").

==Plot==
Two special agents, Aziz and Lemi, are tasked by the Sultan of the Ottoman Empire with delivering a diamond as a gift to the American president. As they ride on a stagecoach across the American wild west, they are robbed of the diamond by bandits, leaving them stranded without any money. A tough cowgirl, Susanne Van Dyke (a character like Calamity Jane) joins them on their quest.

==Cast==
{| width="70%" |
| valign="top" width=50% |
*Cem Yılmaz as Aziz Vefa
*Ozan Güven as Lemi Galip
*Demet Evgar as Susanne Van Dyke
*Özkan Uğur as Chief Red Rocks
*Zafer Algöz as Sheriff Lloyd
*Kaan Öztop as Chuck
*Yılmaz Köksal as Sheriff Çeko
*Ferdi Sancar as Johnnie
| valign="top" width=50% |
*Dilek Çelebi as Betty
*İştar Gökseven as Garry 
*Mehmet Polat as Johnny Lesh 
*Süleyman Turan as Sheriff Murphy 
*Tuncay Özinel as Josh Brokeback
*Mazlum Çimen as Wanted Sheriff
*Tevfik Yapıcı as President James Abram Garfield
|}

==Release==
The film opened in 693 screens across Turkey on   at number one in the Turkish box office chart with an opening weekend gross of $13,706,319.   

The film later opened in one screen in the UK with a first weekend gross of £557.   

==Reception==

===Box Office===
The film was number one at the Turkish box office for two weeks running and has made a total gross of $13,733,563. 

== See also ==
* 2010 in film
* Turkish films of 2010

==References==
 

==External links==
*   for the film (Turkish)
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 
 