This Is What Love In Action Looks Like
 

{{Infobox film
| name           = This Is What Love In Action Looks Like
| image          = File:Love_In_Action_Documentary_Cover.jpg
| image size     =
| alt            =
| caption        = Theatrical release cover
| director       = Morgan Jon Fox
| music          = Jonsi Alan Hayes Glorie
| studio         = Sawed-Off Collaboratory Productions Live From Memphis
| distributor    = TLA Releasing
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = Unknown
}}

 
This Is What Love In Action Looks Like is an American documentary film directed by Morgan Jon Fox about a Memphis teenager who was sent to a controversial Christian program after telling his parents that he was gay. The film was released in 2012, and is distributed by TLA Releasing.

==Synopsis==
In June 2005, a 16-year-old Tennessee boy, Zach Stark, posted a blog entry on his MySpace page, part of which included the following:

 Somewhat recently, as many of you know, I told my parents I was gay... Well today, my mother, father, and I had a very long "talk" in my room where they let me know I am to apply for a fundamentalist christian program for gays. They tell me that there is something psychologically wrong with me, and they "raised me wrong." Im a big screw up to them, who isnt on the path God wants me to be on. So Im sitting here in tears, joing   the rest of those kids who complain about their parents on blogs - and I cant help it.  

Zachs parents were sending him to a controversial ex-gay program called Love In Action in Memphis. After friends of Zach, as well as several Memphis bloggers and activists became aware of his blog, an impassioned, nationwide protest began in support of the teen.  As daily protests were staged outside the Love In Action campus, the controversy quickly became international news as the world counted down the days until the teens release.  The programs director, John Smid believed that being gay was a choice, and that he himself was an ex-gay. The protestors outside, joined by former Love In Action clients, believed that the program was harmful. While Zach was still in the program, his father, Joe Stark went on the Christian Broadcasting Network defending his decision.  

The film covers a six year span, and features in depth interviews with Zach, protestors, former Love In Action clients, as well as the organizations Executive director, John Smid.

==Premier and Release==
This Is What Love In Action Looks Like premiered in June 2011 at Frameline, the 35th Annual San Francisco International Lesbian and Gay Film Festival.  A hit on the film festival circuit, it won Best Documentary awards at Indie Memphis,  and Birmingham Shout. It was also honored with the Derek Oyston CHE Film Award 2012 at the 26th BFI London Lesbian and Gay Film Festival.  Fox, and Love In Actions director John Smid appeared on the CNN to promote the film the morning of its release on DVD and Digital platforms. {{cite web|title=New documentary examines a controversial gay rehab program for teens|url=http://startingpoint.blogs.cnn.com/2012/05/08/new-documentary-examines-a-controversial-gay-rehab-program-for-teens/
|work=CNN.com}} 

==Soundtrack==

{| class="wikitable"
|-
! Title !! Written by !! Performed by !! Label
|-
| Boy Lilikoi, Tornado (instrumental), Sinking Friendships (instrumental), Around Us (instrumental) || Jon Thor Birgisson || Jon Thor Birgisson (as Jonsi) || XL Recording/Universal Music Publishing
|-
| Lazy Day, Gunshot City, Full Circle, Water Drops || Jason Paxton || Glorie || Makeshift Music
|-
| Softy 1, Softy 2, Softy 3 || W.C. Bevan || W.C. Bevan || W.C. Bevan
|-
| Baby Brother (Instrumental) || Brad Postlethwaite || Brad Postlethwaite || Makeshift Music
|- Hood || Domino Records

|-
| Featherlands, Transcend, Precedent, Transition, Resolution || Alan Hayes || Alan Hayes || The House of Hayes
|}

==Follow Up==
On April 13, 2012 a follow up based on the events in the documentary was featured in a story on This American Life titled Own Worst Enemy.  In the podcast the films director, Morgan Jon Fox and Love In Actions now former director, John Smid talk about meeting as enemies, and becoming friends over time.

==References==
 

 

 