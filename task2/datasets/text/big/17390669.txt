Night of the Running Man
{{Infobox film name           = Night of the Running Man image          = Nightoftherunningman.jpg caption        = director       = Mark L. Lester producer  George W. Perkins writer         = Lee Wells based on       =   starring  John Glover Peter Iacangelo Judith Chapman Todd Susman Wayne Newton cinematography = Mark Irwin editing        = David Berlatsky music          = Christopher Franke distributor    = Trimark Pictures released       =   runtime        = 93 minutes country        = United States language       = English budget         = gross          =
}}
 crime Thriller thriller directed HBO before being released direct-to-video. 

== Plot ==
Jerry Logan, a cab driver in Las Vegas, picks up a nervous passenger who offers him $100 to get to the airport as quickly as possible.  Unknown to Jerry, the man has stolen $1 million from the Al Chambers, a mob-connected casino owner.  Chambers himself skimmed the money from mob boss August Gurino.  Knowing that Gurino will kill him if he notices the missing money, Chambers sends assassins to recover it.  Although they successfully kill the thief, Logan escapes with the cash.  Desperate, Chambers hires an expensive hitman, David Eckhart, who tracks Logan to his house.  Logan escapes with the money and flees the city on a train.  Before he leaves, he confides in a friendly waitress, and Eckhart intimidates her into revealing Logans destination.  Eckhart kills the waitress and follows Logan.

In Salt Lake City, Eckhart captures but loses Logan at the airport as he boards a flight for Los Angeles.  Eckhart contacts his associate, Derek Mills, who pays off all the cab drivers at the Los Angeles airport.  With no other choices available, Logan chooses Mills cab.  Mills kidnaps him at gunpoint and, at his house, burns Logans feet in boiling water to make escape impossible.  However, Mills underestimates Logan, who is able to overpower him and flee with the money.  When Logan collapses unconscious on the street, he is taken to the hospital, where he meets nurse Chris Altman.  Altman treats his wounds and covers for him when Eckhart comes looking.  Mills, who is in the same hospital, overhears that Altman has taken Logan to a hotel for safety, and he alerts Eckhart.

Although Logan encourages Altman to leave, Eckhart bursts in before she can.  The two escape together and head for Altmans house after depositing the money in a safety deposit box.  At her house, Logan and Altman have sex.  When Logan wakes in the morning, Eckhart is waiting for him in the kitchen.  Logan proposes a deal: he will split the money with Eckhart, and, in return, Eckhart lets both him and Altman survive.  Eckhart accepts the deal and leaves for Las Vegas, where he kills Chambers and his wife on the orders of Gurino.  When Eckhart returns to Los Angeles, Logan attempts to ambush him but fails.  Frustrated by Logans reluctance to produce the money, Eckhart orders Mills to beat Altman.  Logan reveals that he has hidden it in the basement, and Eckhart kills Mills when Mills offers to retrieve it.

Eckhart and Logan proceed to the basement, where Logan is able to disarm Eckhart, who only becomes more excited by the heightened tension.  Although Eckhart briefly considers killing Altman in order to flush out Logan, he instead chooses to engage Logan in hand-to-hand combat.  Armed with a knife, Eckhart stalks Logan in the dark, taunting him as he scores hits.  Although Logan is able to briefly surprise Eckhart, Eckhart easily overpowers him and goes for a killing blow.  Before Eckhart can finish him, Logan strikes Eckhart with a wooden plank.  Both men are surprised when the plank turns out to have a nail in it, which kills Eckhart.  Now more careful, Logan and Altman leave the country under assumed names and pay cash for their tickets.

== Cast ==
{| class="wikitable"
|-
! style="background-color:gray;" | Actor
! style="background-color:lightgrey;" | Role
|-
| Andrew McCarthy || Jerry Logan
|-
| Scott Glenn  || David Eckhart
|-
| Janet Gunn || Chris Altman
|- John Glover || Derek Mills
|-
| Peter Iacangelo || Al Chambers
|-
| Judith Chapman || Roz Chambers
|-
| Todd Susman || Meyer Weiss
|-
| Wayne Newton || August Gurino
|-
| Kim Lankford || Waitress
|}

== Soundtrack == score was released by Super Tracks. 

Track listing
# American World Pictures Logo - 0:16
# Opening Credits - 3:26
# Taxi Ride - 2:59
# Killer Love - 2:07
# Neck Break/Total Cash - 1:44
# Trailer Break In - 3:19
# Train Chase/Hoover Dam - 3:15
# Couple Argument - 2:03
# Airport Escape - 4:34
# Mills House - 1:39
# Torture - 4:15
# Hospital Escape - 2:22
# Continental Chase (Pts 1 & 2) - 3:55
# Love Scene - 1:48
# Killer in the House - 2:16
# Back to Vegas - 3:02
# Surprise/Girl Beating/End Fight - 7:28
# Explosion/The End - 3:14
# Natashas Theme - 2:08

== Reception == Tarantino territory" for director Lester. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 