The Captive City (1952 film)
 

{{Infobox film
| name           = The Captive City
| image          = Poster of the movie The Captive City.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Wise
| producer       = Theron Warth
| screenplay     = Alvin M. Josephy Karl Kamb
| story          = Alvin M. Josephy
| narrator       = 
| starring       = John Forsythe Joan Camden
| music          = Jerome Moross
| cinematography = Lee Garmes
| editing        = Robert Swink
| studio         = Aspen Productions
| distributor    = United Artists
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Captive City is a 1952 film noir, directed by Robert Wise.  The screenplay is based on real life experiences of Time (magazine)|Time magazine reporter Alvin M. Josephy, Jr., who co-wrote the script. 

==Plot==
As newspaper editor Jim Austin prepares his testimony before the Committee, the story flashes back to the events which led to his testifying.

Mob boss Murray Sirak has the entire police force of Austins small town under his thumb. Sirak takes his orders from an unseen "untouchable" Mister Big.

Austin is driven to investigate corruption after Clyde Nelson, a local private detective, working on an apparently harmless divorce case, discovers the existence of a big-time gambling syndicate operating with the consent of the city fathers, the local police, and the respectable elements of the community. Nelson is killed in a hit-and-run which appears to be an accident. Austin thinks otherwise because he is harassed by police when he looks into the PIs death.

==Cast==
* John Forsythe as Jim Austin
* Joan Camden as Marge Austin
* Harold J. Kennedy as Don Carey
* Marjorie Crossland as Mrs. Sirak
* Victor Sutherland as Murray Sirak
* Ray Teal as Chief Gillette
* Martin Milner as Phil Harding
* Geraldine Hall as Mrs. Nelson
* Hal K. Dawson as Clyde Nelson
* Ian Wolfe as Rev. Nash

==Background== The Turning Point (1952).

==References==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 