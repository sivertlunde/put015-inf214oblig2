Scream of Stone
{{Infobox film
| name           = Scream of Stone
| image_size     =
| image	         = Scream of Stone FilmPoster.jpeg
| caption        =
| director       = Werner Herzog
| narrator       =
| producer       = Walter Saxer Henri Lange Richard Sadler
| writer         = Hans-Ulrich Klenner Walter Saxer Robert Geoffrion
| starring       = Vittorio Mezzogiorno Stefan Glowacz Mathilda May Donald Sutherland Brad Dourif Al Waxman Chavela Vargas Hans Kammerlander Volker Prechtel
| music          =
| cinematography = Rainer Klausmann
| editing        = Suzanne Baron
| distributor    =
| released       = 1991
| runtime        = 105 minutes
| country        = Germany France Canada English
| budget         =
| preceded_by    =
| followed_by    =
}}
 1991 film directed by Werner Herzog about a climbing expedition on Cerro Torre. The film was shot on location at Cerro Torre, with several scenes filmed close to the summit.

The script was written principally by longtime Herzog production manager Walter Saxer, based on an idea from mountaineer Reinhold Messner, who Herzog had worked with in his documentary The Dark Glow of the Mountains. Herzog, who usually writes his own screenplays, believes that the script was weak, especially the dialogue, and says that he does not consider Scream of Stone to be his film. 
 {{cite book
  | last = Herzog
  | first = Werner
  | title = Herzog on Herzog
  | publisher = Faber and Faber
  | year = 2001
  | isbn = 0-571-20708-1 }} 
The movie has elements drawn from the history of the supposed first conquest of the summit of Cerro Torre in 1959, by the Italian climber Cesare Maestri and his partner, the Austrian Toni Egger, who died during the descent.

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 

 