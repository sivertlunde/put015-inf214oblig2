Theekuchi
{{Infobox film
| name = Theekuchi
| image =
| caption =
| director = A. L. Raja
| producer = Star Movie Makers
| writer =  Santhanam Ashish Vidyarthi
| music = Srikanth Deva
| cinematography = 
| editing =
| distributor =
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}}
Theekuchi (English: Matchstick) is a Tamil action film directed by A. L. Raja. The film stars Jai Varma, new comer Mythri in lead roles whilst supporting cast like Ashish Vidyarthi, Vadivelu and Bhanupriya. The music was composed by Srikanth Deva. The film was released in March 14, 2008 to negative reviews.  

==Plot==
Sakthi (Jaivarma) is on a mission to take revenge on those who usurped government land and killed his mother (Bhanupriya) who wanted to construct a school there. Instead a local illicit arrack seller and rowdy Pasupathy Pandian (Ashish Vidhyarti) with the help of the Education Minister (Kadhal Dhandapani) builds a self-financing private engineering college and strikes gold.

Pasupathy becomes rich and powerful by giving admissions by getting capitation fees. The poor students who don’t pay up either disappear or commit suicide. Sakthi and his friends including the heroine (Mythriya), daughter of Pasupathy fight back against this injustice and nefarious activities going on in the campus. How they ultimately triumph and justice prevails is what the film is about

==Cast==
*Jai Varma as Sakthi
*Menakai
*Vadivelu Santhanam
*Bhanupriya
*Ashish Vidyarthi as Pasupathi Pandian
*Thambi Ramaiah
*Shobha
*V. M. C. Haneefa Pandi
*Kalidas
*Vasu Vikram
*Kadhal Dhandapani
*Suman Shetty

==Production==
Theekuchi, is produced by G A Lucas for Star Movie Makers. A. L. Rajan who earlier directed Parthiban starrer Ninaikkatha Naalilai was selected as director. Jai Varma, brother of Disco Shanthi who earlier appeared in Azhagiya Theeye was selected to play the lead role.  

The film was launched in 2005 and the shooting was commenced at locations in Madurai, Tirupparakundram, Kodaikanal, Nagercoil and Kanyakumari among other places. The film faced controversy when actresses from Kerala complained that producer did not pay salary for them.  The filming was finished in 2006 and was finalised to release in 2007 but failed to meet deadline and it had a low key release in 2008.

==References==
 

 
 
 
 
 
 