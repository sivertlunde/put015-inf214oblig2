Indigo Children (film)
{{Infobox film
| name           = Indigo Children
| image          = Indigo Children.jpg
| border         = yes
| caption        = 
| director       = Eric Chaney
| producer       = Eric Chaney
| writer         = Eric Chaney
| starring       =  
| music          = Jesse Lee Herdman
| cinematography = Jay Hufford
| editing        = Eric Chaney
| studio         = 
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Indigo Children is a 2012 American drama film about the romance between two teenagers based on the concept that some children possess special faculties. This concept is known as Indigo Children. Directed by Eric Chaney, this film was inspired by the first girl he ever loved. 

==Plot==
A mysterious girl pursues a young man in her new small town home. Coinciding deaths and a disappearance create a common thread between them as they struggle with young love and loss over the course of one summer. 

==Cast==
*Robert Olsen as Mark
*Isabelle McNally as Christina
*Christine Donlon as Jenny
 

==Awards==
The film won the Best of Show, feature film at the Accolade Competition 2012. 

==References==
 

==External links==
*  
*   at Swikat

 
 
 
 
 


 