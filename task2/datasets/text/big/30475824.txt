Sathi Murali
{{Infobox film
| name           = Sathi Murali
| image          = SathiMurali.jpg
| image_size     = 150px
| caption        = A still from Sathi Murali
| director       = T. C. Vadivelu Naicker
| writer         =
| starring       = M. K. Radha, M. R. Santhanalakshmi, T. R. Mahalingam (actor)|T. R. Mahalingam, Nagercoil K. Mahadevan, L. Narayana Rao,  Kali N. Ratnam, P. G. Venkatesan, "Buffoon" Sankara Iyer, "Joker" Ramudu, S. Varalakshmi, T. A. Madhuram, P. S. Gnanam
| producer       = Modern Theatres
| distributor    =
| music          =
| cinematography =
| editing        =
| released       = 1940
| runtime        = Tamil
}}

Sathi Murali was a 1940 Tamil-language film directed by Vadivelu Naicker. It starred M. K. Radha, M. R. Santhanalakshmi and T. R. Mahalingam (actor)|T. R. Mahalingam in the lead roles.

== Production ==

During the late 1930s and the 1940s, there were a lot of movies with social messages such as Thyagabhoomi. Sathi Murali targeted caste-based discrimination in society. Sathi Murali was the second film for singer and actor T. R. Mahalingam who later became one of the leading stars of Tamil cinema.

== Plot ==

The movie was based on the story of a low-caste boy named Seenu who falls in love with a high-caste woman named Murali. After facing a lot of hardships, their love finally succeeds.

== Cast ==
* M. K. Radha as Seenu
* T. R. Mahalingam (actor)|T. R. Mahalingam as the young Seenu
* M. R. Santhanalakshmi as Murali.

==References==
*  

==External links==
*  

 
 
 
 


 