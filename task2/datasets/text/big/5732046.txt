The End (1998 film)
 
The End is a 1998 skateboarding film from Birdhouse Skateboards. It was made under the direction of Jamie Mosberg, who also produced and edited it. The original movie was released in 1998, but the movie was re-released in 2001.

==Plot==

There is no real plot that runs throughout this Documentary film|documentary. Instead, each skater was placed into their segment to showcase their skills. Some of the skaters also incorporated a short story into their segments. Some of these story lines include McCrank looking for a job, Berra being killed by an unknown attacker, Reynolds hanging out with a monkey, Kirchart and Kleins rampaging mayhem in the city and Lasek taking down Hawk so that Lasek can become the number one skateboarder.

==Cast==
(in order of appearance)
*Rick McCrank
*Willy Santos Andrew Reynolds
*Brian Sumner - Am montage
*Ali Cairns - Am montage
*Jeff Lenoce - Am montage Steve Berra
*Heath Kirchart
*Jeremy Klein
*Tony Hawk
*Bucky Lasek

===Featuring===
Janine Lindemulder and Kobe Tai - Appearance in Jeremy Klein and Heath Kircharts dream sequence.

==Special Edition==
In 2001, a special edition of The End became available for VHS and DVD. The DVD came with many special features such as a "making of" featurette, photo gallery, remastered audio, additional and old footage, board gallery and audio commentary with the option of adding in picture-in-picture with The End and the filmed commentary session with the Birdhouse team. The VHS version however only included the "making of" featurette.

==References==
 

==External links==
* 
* 

 
 
 