The Hollywood Knights
{{Infobox film
| name           = The Hollywood Knights
| image          = hollywood_knights_movie_poster.jpg
| image_size     = 225px
| caption        = Theatrical release poster
| director       = Floyd Mutrux
| producer       = William Tennant Richard Lederer
| writer         = Floyd Mutrux Richard Lederer William Tennant
| starring       = Robert Wuhl Tony Danza Fran Drescher Michelle Pfeiffer Stuart Pankin   Debra Feuer
| cinematography = William A. Fraker
| editing        = Stanford C. Allen Scott Conrad
| distributor    = Columbia Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States English
| budget         = $2.5 million (est.)
| gross          = $10,000,000 (domestic)
}}

The Hollywood Knights (1980) is an American motion picture comedy written and directed by Floyd Mutrux    depicting the crass and mischievous antics and practical jokes of the remaining members of a 1950s-era car club turned social fraternity in and around Beverly Hills and Hollywood in 1965.  The cast, led by Robert Wuhl as the fraternitys charismatic leader Newbomb Turk, features Tony Danza  and a young Michelle Pfeiffer  as high school sweethearts as well as Fran Drescher  and Stuart Pankin in supporting roles.

==Plot==
On Halloween night in 1965, a group of high school pranksters &ndash; the Hollywood Knights &ndash; are enraged by the Beverly Hills Residents Associations success in arranging for the shutdown and demolition of their favorite hangout, Tubbys Drive-In diner, which is to be replaced by an office building. In response, they launch a sustained and comically vengeful campaign against the principals of the association and two bumbling local police officers charged with keeping the "The Knights" in check during their last night in Beverly Hills. The ensuing antics include, among other things, a sexual encounter involving premature ejaculation, a punch bowl being spiked with urine, an initiation ceremony involving four pledges who are left in Watts wearing nothing but the car tires they are left to carry, a cheerleader who forgets to put on her underwear before performing at a pep rally, several impromptu drag races, and the lead character of Newbomb Turk (Robert Wuhl) wearing a majordomo outfit and singing a version of Volare (song)|Volare accompanied by the sounds of flatulence. Mooning also plays a prominent role in the film: one of the advertising slogans exploited the recent Apollo space program by touting that The Hollywood Knights was the first movie to moon a man on the land. During a mooning incident in the films final scene, the character Dudley Laywicker, becomes absolutely all eyes, transfixed by the bare buttocks. So much so, that he takes his glasses off for a better look.

==Cast==
  
*Robert Wuhl as Newbomb Turk
*Tony Danza as Duke
*Fran Drescher as Sally
*Leigh French as Jacqueline Freedman  
*Randy Gornel as Wheatly
*Gary Graham as Jimmy Shine
*Sandy Helberg as Officer Clark
*James Jeter as Smitty
 
*Stuart Pankin as Dudley Laywicker
*P.R. Paul as Simpson
*Michelle Pfeiffer as Suzie Q
*Gailard Sartain as Bimbeau
*Julius Averitt as Earle
*Richard Schaal as Nevans
*Steve Ballard as Mechanic
*Carol Ann Williams as Jane
*Debra Feuer as Cheetah
 

==Production==
The filming location for the "Tubbys Drive-In" scenes was an old A&W Root Beer location that had closed at 7310 North Van Nuys Blvd, in Los Angeles, California.

Robert Wuhl, Tony Danza and Stuart Pankin all played teenage characters, although Wuhl and Danza were both in their late twenties, and Pankin was 33 years old. 
 Columbia DVD Robert Evans, also preferred Pfeiffer, but the eventual director, James Bridges, refused to cast anyone but Debra Winger in the part. 

==Legacy==
Widely considered to be an inferior rip-off of American Graffiti,  today the film is primarily notable for the début performances of many well-known actors.
 boxer Tony Banta on the television sitcom Taxi (TV series)|Taxi since 1978, but he had not appeared in a feature film prior to this production. Outstanding Writing 63rd and 64th Academy Awards ceremonies.
*The movie built a cult-like following during the 1980s due to repeated late-night airings on HBO.
*The Columbus, Ohio band New Bomb Turks took their name from the films protagonist.
*Melt Bar and Grilled in Lakewood, Ohio named a sandwich The Newbomb Turkey Club in honor of the films protagonist.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 