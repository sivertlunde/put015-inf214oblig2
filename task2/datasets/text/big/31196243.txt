Golden Madonna
{{Infobox film
| name           = Golden Madonna
| image          = 
| image_size     = 
| caption        = 
| director       = Ladislao Vajda John Stafford Akos Tolnay
| writer         = Akos Tolnay
| story          = 
| based on       =
| narrator       = 
| starring       = Phyllis Calvert Tullio Carminati Michael Rennie
| music          = 
| cinematography = 
| editing        = 
| studio         = Independent Film Producers
| distributor    = 
| released       =    
| runtime        = 88 minutes
| country        = Italy United Kingdom
| language       = English Italian
| budget         = 
| gross          = 
}} National Portrait Gallery.

==Plot==
A young British woman who owns a villa in Italy offends the village she lives in by throwing away a painting that they consider lucky. To redeem herself she goes out in search to try to recover it.

==Cast==
* Phyllis Calvert as Patricia Chandler
* Tullio Carminati as Signor Migone
* Michael Rennie as Mike Christie David Greene as Johnny Lester
* Aldo Silvani as Don Vincenzo
* Pippo Bonucci as Pippo
* Francesca Biondi as Maria
* Franco Coop as Esposito
* Claudio Ermelli as Antonio

==References==
 

==External links==
* , with extensive notes
* 

 

 
 
 
 
 
 
 
 
 
 


 