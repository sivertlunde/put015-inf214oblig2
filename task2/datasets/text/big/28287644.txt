Jail Busters
{{Infobox film
| name           = Jail Busters
| image          =
| image_size     =
| caption        =
| director       = William Beaudine
| producer       = Ben Schwalb
| writer         = Edward Bernds (screenplay) Elwood Ulman (screenplay)
| starring       = Leo Gorcey Huntz Hall David Gorcey Bernard Gorcey Bennie Bartlett
| music          = Marlin Skiles
| cinematography = Carl E. Guthrie William Austin Allied Artists Pictures Allied Artists Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Jail Busters is a 1955 film starring the comedy team of The Bowery Boys.  The film was released on September 18, 1955 by Allied Artists and is the thirty-ninth film in the series.

==Plot==
Chuck gets a job working for a newspaper. When promoted, he has to go under cover in the state prison to dig up some information on some of the inmates. When one of the inmates beats Chuck up and lands him in the hospital, its up to Slip, Sach, and Butch to finish Chucks job. Slip decides that he and the boys should commit a crime so that they can be sent to jail. Another reporter, Cy Bowman, agrees to inform the penitentiary that the boys are actually working under cover for the newspaper once they get arrested. However, Bowman does not actually keep his promise. Now the trio are forced to spend what they think is a short sentence in jail. While there, the boys dig up some information on Percival P. Lannigan and some other inmates who have been living it up in jail, unknown to the warden. Lannigan soon gets word that Slip and his pals are under cover for Chuck (whom Lannigan had beaten up earlier), and intends to have Chuck and Louie killed.  The boys eventually expose the inmates scam and turn them over to the warden, who pardons them and arranges for their release.

==Production==
Jail Busters was filmed under the working title of Doing Time  and is the only film in the series with no females in the cast.

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terence Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*David Gorcey as Charles Chuck Anderson (Credited as David Condon)
*Bennie Bartlett as Butch Williams

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski
*Barton MacLane as Captain Jenkins, head guard Anthony Caruso as Percival P. Lannigan
*Percy Helton as Warden B.W. Oswald
*Lyle Talbot as Cy Bowman Michael Ross as Big Greenie Joahn Harmon as Tomcyk
*Murray Alper as Gus

===Cast notes===
After the death of Bernard Gorcey, just seven days before this film was released, Percy Helton, who played the warden in this film, was one of the choices for taking the place of Gorceys character, Louie Dumbrowski. Two years later he would play the character of Mike Clancy, a character similar to Louie, in the film Spook Chasers.

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Four" on August 26, 2014.

==References==
 

==External links==
*  
*  

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Spy Chasers 1955
| after=Dig That Uranium 1956}}
 

 
 

 
 
 
 
 
 
 