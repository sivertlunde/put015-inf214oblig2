Jism (2006 film)
 

{{Infobox Film
| name           = Jism
| image          = Jism (2006).JPG
| image_size     = 
| caption        = DVD cover
| director       = Rasheed Dogar
| producer       = Riaz Gujjar and Shahzard Gujjar
| writer         = Salim Murad
| photography  	 = Ejaz Samrat, Govinda and Tanvir Bholy
| lyrics         = Ahmad Anees
| starring       = Shamil Khan, Roofi Anaam|
| music          = Ali Afzal 
| cinematography = Hanif Bhatti
| editing        = Basharat Ali
| released       = Eid-ul-Azha, (1426 hijri) Friday, January 11, 2006 (Pakistan)
| runtime        = 
| country        = Pakistan
| language       = Urdu
| budget         = 
| domestic gross = 
| preceded_by    = 
| followed_by    =
}}

Jism is a Pakistani Urdu film directed by Rasheed Dogar. Singers are Ameer Ali, Asma Lata, Saima Akhtar and Sana Khan. The film was banned in Pakistan because of inappropriate scenes shown.

==Plot==
The story basically centers around Zeeshan (Shamil Khan) who is a boy. His friend who leads an un-cultural life has an influence on him. Zeeshan falls in love with a pretty girl, then later he has news that his uncle is near to death. He rushes home to find out that his last request was to marry his daughter Kieren (who he does not love), he promises to marry in panic despite not loving her. After his uncles death he denies to marry Kieren, but his mum convinces him, not knowing what an impact this act will cause.

Kieren is happy to marry him, but he is not! On the marriage day he reveals his feelings for her and also tells her that he is in love with someone else. He also says that we will continue this marriage for the sake of my mother and uncle but not have sexual intercourse, she nods in agreement as she is very innocent. His real love is angry and upset once they meet. She also says she still has feelings for him despite his actions. They both fall in each others arms and fall into love.

Later in the film Zeeshan goes to his love again, but finds she has another person to love, which is his un-cultured friend. He comes home late from a club, his innocent wife Kieren questions him and soon a fight occurs, his wife is determined to fight for his love. Zeeshans suspicious wife founds out he is trying to dodge her continuously while he is at his lovers house trying to encourage her for his love again. She said for this you will have to divorce your wife, he agrees. Kieren (who is still his wife) finds out the affair and confronts him, he says fine lets have a divorce, but Kiren had something else planned. She goes cunningly to his lovers house she pledges her not to spoil her life by stealing her husband, the two of them have a fight. After this Kieren tries to sell herself to her husband. Zeeshans friend also loves the same lover and sets him up for him to go to jail. While his in jail he attempts to force Zeeshans lover to marry him but she denies, so he murders her in a struggle. Once Zeeshan comes out from jail he starts to love Kieren, but his so-called friend is in his way yet again. Could Zeeshan and Keiren stop him?

==Film business==
Over all the film was an average, 8 weeks at Imperial Cinema and combined 36 Weeks in Lahore.

==Songs==
* 01. Tu Mera Bun Ke
* 02. Phoolien Kay Saath
* 03. O Shirabi
* 04. Jism Jalta Tera
* 05. Dildar Mujha Karta Hai Pyar
* 06. Jism Jalta Mera

==Cast==
* Shamil Khan
* Roofi Anaam
* Fakhar Imam

== External links ==
*  

 
 
 