Tahader Katha
{{Infobox film name = Tahader Katha image = Tahader Katha.jpg caption = director = Buddhadev Dasgupta producer = writer = Buddhadev Dasgupta (screenplay) Kamal Kumar Majumdar (story) narrator = starring = Mithun Chakraborty Anashua Mujumdar  Dipankar De Subrata Nandy Deboshri Bhattacharya Ashok Mukherjee music = cinematography = Venu
|editing = distributor = released =   runtime = 180 mins country = India language = Bengali
|budget =
}}
 1992 award directed by National Film Awards for Best Actor for the film, while the film won the National Film Award for Best Feature Film in Bengali.

==Plot==
 British officer. Shibnath spent a part of his term in the prison asylum.

In the journey back home, Shibnath is accompanied by one of his comrades, Bipin (played by Dipankar De)—now a successful businessman and an aspiring politician. Shibnath experiences the aftermath of Partition of India (India was partitioned in Republic of India and Pakistan in 1947, at the time of independence), with his own family becoming refugees, and his old village now belonging to a separate nation. Once cutting a formidable figure as a virile and courageous freedom fighter crusading for a united and independent Bengal to drive away the British, Shibnath now stands in stark contrast: a fragile, fragmented shell of his former self as he awkwardly hobbles along an unpaved road through the countryside, stopping frequently along the way to relieve himself in the woods, unable to control even his own bodily functions (undoubtedly the autonomic legacy resulting from years of physical torture and inhumane treatment that he sustained while in police custody).

Shibnaths wife, Hemangini, urges him to make ally with Bipin who is willing to capitalize on Shibnaths legendary reputation for patriotism, by asking to accompany him in electoral campaigns. In exchange, Bipin is ready to arrange Shibnath the job of a school master. However, Shibnath remains disillusioned and mystified by the life that now lies before him away from his beloved—and irretrievably lost—homeland. Unable to abandon his crushed idealism and put his devastated past behind him, he withdraws further away from family and former colleagues, retreating into the tenuous company of his own fractured and haunted memories.

==Cast==

*Mithun Chakraborty
*Anashua Mujumdar
*Dipankar De
*Subrata Nandy
*Deboshri Bhattacharya
*Ashok Mukherjee

==External links==
*  
*  at FilmReference

 

 
 
 
 
 
 
 

 