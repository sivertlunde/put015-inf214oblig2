I Love You, Apple, I Love You, Orange
 
{{Infobox film
| name           = I Love You, Apple, I Love You, Orange
| image          = ILYAILYO Poster Wiki.jpg
| alt            = 
| caption        = DVD cover
| director       = Horam Kim
| producer       = Horam Kim Lori Anne Smithey Tae Il Kim	
| writer         = Horam Kim
| starring       = Lori Anne Smithey Horam Kim Stefan Griswold Victoria Blake
| music          = David R. Lorentz  Jamie Cooper (additional music)
| cinematography = Horam Kim
| editing        = Horam Kim
| studio         =
| distributor    = 
| released       =   |
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} romantic comedy-drama film by Horam Kim. It stars Kim, Lori Anne Smithey, Stefan Griswold, and Victoria Blake. The film combines live action with stop motion animation   and is considered part of the mumblecore movement. 

The film premiered in New York and screened at various film festivals in the United States.  In 2014 it became available on DVD via Amazon.com and for instant streaming on Indieflix.

==Synopsis==
Maggie (Smithey) lives alone and spends her days writing lists and talking to fruits and vegetables that have come to life. The arrival of Martin (Kim), via a blind date set up by Maggies mother (Blake), sparks a romantic relationship that threatens the routine and quiet order of Maggies daily life as she labors to keep Martin from discovering her inner world.

==Cast==
*Lori Anne Smithey as Maggie
*Horam Kim as Martin
*Stefan Griswold as Orange
*Victoria Blake as Maggies mother

==Production== The Sweet Hereafter, Punch-Drunk Love, and Brief Encounter. Real foods were used as stop-animated characters. The same ham was used for the entirety of the movie. By the end of filming, the ham was more than a year and a half old. After six months, the ham smelled awful and made handling it for animating purposes difficult. 

==Critical Reception== The Stranger called the movie, "a slow, scary, and surprising romance, anchored by a tough, vanity-free performance by Lori Anne Smithey."  And critic Sean Kilpatrick lauded it in Hobart (magazine)|Hobart, calling the film, "a little poem." 

I Love You, Apple, I Love You, Orange won the Bullet Award at the 2013 Columbia Gorge International Film Festival.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 