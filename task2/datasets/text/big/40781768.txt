If I Were for Real
 
{{Infobox film
| name           = If I Were for Real
| image          = If i were for real video cover.jpg
| caption        = Video cover art
| director       = Wang Toon
| producer       = 
| based on       = the play by Nikolai Gogol
| screenplay     = Chang Yung-hsiang ( )
| starring       = Kuan-Chen Hu Alan Tam
| music          = 
| cinematography = Lin Hung-chung ( )
| editing        = 
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Taiwan
| language       = Mandarin
| budget         = 
}}

If I Were for Real ( ) is a 1981 Taiwanese  ,     Best Actor (Alan Tam), and Best Adapted Screenplay (Chang Yung-hsiang;  ).    The remaining Golden Horse nominations were the Best Cinematography (Lin Hung-chung;  ) and the Best Art Direction (Wang Tung;  ). 

==Cast==
* Kuan-Chen Hu (as Nancy Hu Guan Zhen)
* Alan Tam as The Impostor

==Music==
Jiaru wo shi zhen de (If I Were for Real) is a 1981 album by Teresa Teng, released internationally, including in Hong Kong, by Polydor Records, and in Taiwan by Kolin Records ( ). In the Kolin edition, sides one and two of the Polydor edition were reversed, and one song is omitted.

;Polydor edition (international standard)
{{track listing |headline=Side one |lyrics_credits=yes |music_credits=yes |extra_column=Chinese title

|title1=If the Dream Was for Real |extra1=  jiaru meng er shi zhen de |lyrics1=Zhuang Nu ( ) |music1=Chen Hsin-yi ( ), aka Hsin-yi ( ) |note1=Sub-theme song of the film

|title2=Lake of Feeling |extra2=  qing hu |music2=Shigaraki Jyunzou ( ) |lyrics2=Zhuang Nu |note2=Japanese:  

|title3=Breeze |extra3=  jingfeng |lyrics3=Zhuang Nu |music3=Hsin-yi

|title4=Why Wont You Say |extra4=  ni wei hebu shuo |lyrics4=Sun Yi ( ) |music4=Tang Ni ( )

|title5=Camellia flower |extra5=  shanchahua |lyrics5=Zhuang Nu |music5=Endō Minoru ( ) |note5=Japanese:  , sung in 1978 by Mieko Makimura ( )

|title6=Me and Autumn |extra6=  wo yu qiufeng |lyrics6=Lin Huang-kuen ( ) |music6=Endō Minoru
}}

{{track listing |headline=Side two |lyrics_credits=yes |music_credits=yes |extra_column=Chinese title

|title7=If I Were for Real |extra7=  jiaru wo shi zhen de  |lyrics7=Zhuang Nu |music7=Hsin-yi |note7=Main theme song for the film

|title8=Leaving |extra8=  bieli |lyrics8=Lin Huang-kuen |music8=Tsutomu Sasaki ( )

|title9=Going to Distant Places |extra9=  yao qu yaoyuan de defang |lyrics9=Lin Huang-kuen |music9=Wang Luobin

|title10=Love the Rain |extra10=  ai yu |lyrics10=Sun Yi |music10=Tang Ni

|title11=Clouds Returning on Sunset |extra11=  caixia huilai ba |music11=Andy ( ) |lyrics11=Zhuang Nu

|title12=Dreams |extra12=  meng |music12=Kei Ogura |lyrics12=Zhuang Nu
}}

 
;Side one
#"If I Were for Real"
#"Leaving"
#"Going to Distant Places"
#"Love the Rain"
#"Clouds Returning on Sunset"
#"Dreams"
;Side two
#"If..." ( )
#: Alternative title for "If the Dream Was for Real"
#"Lake of Feeling"
#"Why Wont You Say"
#"Camellia Flower"
#"Me and Autumn"
 

==See also==
* List of submissions to the 54th Academy Awards for Best Foreign Language Film
* List of Taiwanese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 

 
 