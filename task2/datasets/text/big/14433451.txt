Hell's House
{{Infobox film
| name           = Hells House
| image          = HellsHouse.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Howard Higgin
| producer       = B. F. Zeidman  
| writer         = Paul Gangelin B. Harrison Orkow
| narrator       =  Pat OBrien
| music          = 
| cinematography = Alan G. Siegler 
| editing        = Edward Schroeder   
| studio         = B.F. Zeidman Productions Ltd.
| distributor    = Capitol Film Exchange
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Prohibition era, is based on a story by Higgin.

==Plot==
 When orphaned Jimmy Mason is taken in by his Aunt Emma and Uncle Henry, he meets their boarder Matt Kelly, who impresses the young man with his boastful swagger and alleged political connections, although in reality hes a rum-running|bootlegger. 

The boys life is disrupted when, as one of Kellys hired hands, he refuses to identify his boss during a police raid and is sentenced to three years of hard labor in reform school, where he befriends a sickly boy named Shorty, who eventually is sent to solitary confinement.

When Jimmy realizes his new pal is seriously ill and desperately needs medical attention, he escapes and goes to Kelly and Kellys girl friend, Peggy Gardner, for help. Peggy contacts newspaper columnist Frank Gebhardt, who is anxious to expose the conditions at the state industrial school. 

The authorities find Jimmy at Gebhardts office, but before they can apprehend him Kelly admits his involvement in the bootlegging operation and the boy is set free. He discovers Shorty has died, victimized by a corrupt system.

==Cast (in credits order)==
*Bette Davis as Peggy Gardner Pat OBrien as Matt Kelly
*Junior Durkin as Jimmy Mason
*Frank Coghlan Jr. as Shorty
*Emma Dunn as Emma Clark
*Charley Grapewin as Henry Clark
*Morgan Wallace as Frank Gebhardt
*Hooper Atchley as Captain Of The Guard
*Wallis Clark as Judge Robinson
*James A. Marcus as Superintendent Charles Thompson

==Production== The Man Who Played God. 

==Critical reception==
In his review in the New York Times, Mordaunt Hall observed, "The attempt to pillory reform schools . . . is hardly adult in its attack, but it has a few moderately interesting interludes . . . The direction of this film is old-fashioned. Pat OBrien . . . gives a forced performance. Young Durkins playing is sincere and likewise that of Bette Davis as Peggy." 

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 