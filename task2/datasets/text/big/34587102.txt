Proof of the Man
{{Infobox film
| name           = Proof of the Man
| image          =
| caption        =
| director       = Junya Satō
| producer       = Haruki Kadokawa
| writer         = Zenzō Matsuyama
| starring       = Yūsaku Matsuda
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Japan Japanese
| budget         =
}}
  is a Japanese film from 1977 starring George Kennedy and Yūsaku Matsuda and directed by Junya Satō. It was produced by Haruki Kadokawa.

==Plot==
A young black man (Joe Yamanaka) from New York receives a sum of money. He buys new clothes and takes a flight to Japan. After he arrives, he is found fatally stabbed in a lift in a Tokyo hotel at the same time as a fashion show by designer Kyōko Yasugi (Mariko Okada) is being held. The police department, including Munesue (Yūsaku Matsuda) and his partner (Hajime Hana), come to investigate. The only clue is the dying mans last words "straw hat". At the same time, a woman having an extramarital affair, Naomi (Bunjaku Han), is accidentally run over by Yasugis son (Kōichi Iwaki). He and his girlfriend dump her body in the sea, but drops his watch at the scene. He is haunted by his actions and confesses to his mother, Kyōko, who suggests he flees to New York with his girlfriend.

Munesue starts to suspect that Kyōko knows more than she is letting on. He travels to New York to find out more about the dead man. There he is partnered with an American detective, played by George Kennedy, who seems to be the same man who killed Munesues father. Munesue finds that the young man is the son of a black American soldier and a Japanese woman. He also finds Yasugis son, who deliberately provokes George Kennedy into shooting him dead. Munesue returns to Japan and begins to suspect Kyōko. He travels to a resort and discovers that Kyōko was a prostitute in the years after the war. Finally he has enough evidence and confronts Kyōko that the black man was her son, and she killed him to protect her reputation. Kyōko commits suicide. In America, George Kennedy goes looking for the black mans father and finds he is dead. Then George Kennedy is stabbed and dies.

==Cast==
*George Kennedy - Shuftan
*Yūsaku Matsuda - Munesue
*Mariko Okada - Kyōko Yasugi
*Bunjaku Han - Naomi (woman run over)
*Joe Yamanaka - Johnny Hayward
*Janet Hatta
*Toshiro Mifune - Yasugis husband
*Robert Earl Jones - Willy Hayward, Johnnys father
*Broderick Crawford - Shuftans superior in the police force
*Kōichi Iwaki - Yasugis son
*Hiroyuki Nagato - Naomis husband
*Hajime Hana - partner
*Junzaburō Ban - onsen owner

==Music==

The theme song, entitled Ningen no Shōmei no Tēma, with the line "Mama, do you remember" was a chart hit for Joe Yamanaka, selling 517,000 copies, and reaching number 2 on the Oricon chart in Japan.  It was also a hit in other Asian countries.  In Chinese speaking countries the song is called Old Straw Hat, taken from lyrics in the song. 

==References==
 

==External links==
* 

 

 
 
 
 