Purab Aur Paschim
{{Infobox film
| name           = Purab Aur Paschim
| image          = Purab_Aur_Paschim_Still.jpg
| image_size     =
| caption        = Manoj Kumar
| director       = Manoj Kumar
| producer       = Manoj Kumar
| writer         = Shashi Goswami Manoj Kumar Pran Nirupa Roy Prem Chopra Vinod Khanna
| music          = Kalyanji-Anandji
| cinematography = V.N. Reddy
| editing        = B.S. Glaad
| studio         = Vishal International Productions
| distributor    = Ultra Distributors
| released       = 
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
}} Pran and Prem Chopra in the lead roles. The music is by Kalyanji Anandji. The 2007 film Namastey London is inspired from this film.

This was the second film by Manoj Kumar where he stars as Bharat (first being Upkar) and his fourth film on patriotism (Shaheed (1965 film)|Shaheed was his first, to be followed by Roti Kapda Aur Makaan and Kranti.)

==Synopsis==
In 1942 British India, Harnam (Pran (actor)|Pran) betrays a freedom fighter, and as a result is rewarded, but the freedom fighter is killed, leaving his wife, Ganga (Kamini Kaushal) and family devastated and destitute. 
Years later, after the Indian Independence in 1947, the freedom fighters son, Bharat (Manoj Kumar), has grown up and goes to London for higher studies. On his arrival in Britain, He meets his fathers college friend, Sharma (Madan Puri) with his westernized wife, Rita (Shammi) and daughter, Preeti (Saira Banu) and the hippie son, Shankar (Rajendra Nath). 
Preeti has long blonde hair, wears mini-dresses, smokes and drinks and has no idea of Indian values till she meets Bharat. He is, of course, shocked to see that many Indians in London are ashamed of their roots and even changed their names to sound Western. Or others who long for their country, but stay in the UK for economic reasons. Like Sharma with his stack of KL Saigal records. 
He takes it upon himself to try and change their way of thinking while both Bharat and Preeti fall in love with each other. Later on, with his mothers and Guruji (Ashok Kumar)s approval, he promised to marry Preeti. 
 
Preeti is impressed by Bharats idealism and wants to marry him, but doesnt want to live in India. Bharat wants her to come to India and see what its like before she rejects it. Of course, the purity of India redeems her and she gives up smoking, drinking and minis to adopt the traditional lifestyle.

==Cast==
* Manoj Kumar as Bharat
* Saira Banu as Preeti
* Ashok Kumar as Bharats Guru
* Kamini Kaushal as Ganga, Bharats Mother Pran as Harnam
* Vinod Khanna as Shyamu
* Nirupa Roy as Kaushalya
* Bharathi Vishnuvardhan as Gopi
* Prem Chopra as Omkar / OP
* Madan Puri as Sharma
* Shammi as Rita, Sharmas wife
* Rajendra Nath as Shankar

==Release==
===Reception===

Purab aur Paschim received generally positive reviews. Deepa Gahlot of Bollywood Hungama wrote "By linking the story to the freedom struggle, Manoj Kumar was saying that freeing India from British rule is not enough if Indians do not feel proud of their Indianness. Manoj Kumar shot in London at the height of the hippie phase and caught both the beauty and ugliness of the English landscape. However, his simplistic view of the West was greed, lust and depravity, while India stood for love, honour and piety. Amazingly the idea has endured, and in even in Aditya Chopras cult hit Dilwale Dulhania Le Jayenge, Indian boy (Shah Rukh Khan) does not touch the Indian girl (Kajol) though he claims a chain of foreign girlfriends, and neither does he want to marry her without her fathers consent. Then as now, Indian culture is represented with a lot of colour, rituals, song and dance. In 2007, Akshay Kumar sells the same version of India to the London gal Katrina Kaif -- and a line in the film pays tribute to the original, when he tells her that if her boyfriends Uncle and their associates want to learn more about India, hed give her a DVD of Purab Aur Paschim which she should give to them."   

==Soundtrack==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Notes
|-
| "Dulhan Chali"
| Mahendra Kapoor
| 
|-
|"Koi Jab Tumhara Hriday" Mukesh (singer)
|
|-
|"Om Jai Jagdish Hare" Mahendra Kapoor, Brij Bushan, Shyama Chittar
|
|-
|"Purva Suhani Aayi Re"
| Lata Mangeshkar Mahendra Kapoor and Manhar Udhas
|
|-
|*Raghupati Raghava" Mahendra Kapoor and Manhar Udhas
|
|-
|"Twinkle Twinkle Little Star" Asha Bhosle and Mahendra Kapoor Picturized on Saira Banu and Manoj Kumar in a giant puppet show.
|-
|"Bharat Ka Rahnewala" Mahendra Kapoor In party where Madan Puri ask Manoj Kumar to sing something about Country India
|}

==References==
 

== External links ==
*  

 
 
 
 