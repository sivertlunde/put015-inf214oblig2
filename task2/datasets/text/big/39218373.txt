Geet (1944 film)
{{Infobox film
| name           = Geet
| image          = 
| caption        = 
| director       = S. U. Sunny 
| producer       = Abdul Rashid Kardar
| writer         =
| starring       = Shahu Modak Nirmala Devi
| music          = Naushad
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1944
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Geet or The Song is a 1944 Bollywood film.        Geet was directed by S. U. Sunny and produced by Abdul Rashid Kardar.  The film starred Shahu Modak, Nirmala Devi, Aamir Ali, Shakir Ali and Chandabai.    The music for the film was composed by Naushad with lyrics by D. N. Madhok.   

==Cast==
*Shahu Modak,
*Nirmala Devi
*Aamir Ali
*Shakir Ali 
*Chandabai
*Zahid Ali

==Soundtrack==

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| " Main Akeli Raja Aa Jaa "
| Nirmala Devi
|-
| 2
| " Bedard Zamana "
| Shahu Modak, Nirmala Devi
|-
| 3
| " Hua Savera Preetam Mera "
| Amirbai Karnataki
|-
| 4
| " Preet Kabhi Mat Karna Bete "
| Alauddin Naveed
|-
| 5
| " O Aag Laga Ke Bhaag Gaye "
| Shahu Modak
|-
| 6
| " Jaao Ji Jaao "
| Nirmala Devi
|-
| 7
| " Dil Bujh Hi Gaya "
| Zohrabai Ambalewali
|-
| 8
| " Kab Aag Mohabbat Ki "
| Shahu Modak
|-
| 9
| “Main Toh Piya Ki Joganiya Hoon”
| Nirmala Devi
|-
| 10
| “Chand Khila Tare Muskaye”
| Nirmala Devi
|-
| 11 
| “Bedard Zamana”
| Shahu Modak
|}



==References==
 

==External links==
*  

 
 
 
 


 