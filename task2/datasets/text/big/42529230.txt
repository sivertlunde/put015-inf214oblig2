Naanu Nanna Hendthiru
{{Multiple issues|
  }}

{{Infobox film
| name = Naanu Nanna Hendthiru
| image = 
| caption =
| director = V. S. Reddy
| writer = K. Bhagyaraj Prema
| producer = Rockline Venkatesh
| music = V. Ravichandran
| cinematography = G. S. V. Seetharam
| editing = Shyam
| studio = Rockline Productions
| released =  
| runtime = 166 minutes
| language = Kannada
| country = India
| budget =
}}
 comedy drama drama film Prema among others.  Ravichandran has scored the music as well for the film.

The film written by K. Bhagyaraj, was a remake of Tamil blockbuster film Thaikulame Thaikulame and was also remade in Telugu as Intlo Illalu Vantintlo Priyuralu.

== Cast ==
* V. Ravichandran 
* Soundarya  Prema
* Doddanna
* Mandya Ramesh
* Srinivasa Murthy
* Kashi
* Bank Janardhan
* Jyothi
* Shobha Raghavendra

== Soundtrack ==
The music was composed by V. Ravichandran and lyrics for the soundtrack penned by K. Kalyan. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| collapsed = no
| title1 =  Baare Chinna
| extra1 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics1 = 
| length1 = 
| title2 = Nannavalu Nakkare
| extra2 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics2 =
| length2 = 
| title3 = Naanu Nanna Hendthiru
| extra3 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics3 =
| length3 = 
| title4 = Ready 123
| extra4 = Gurukiran, L. N. Shastry
| lyrics4 = 
| length4 = 
| title5 = Ee Bhumige
| extra5 = L. N. Shastry, K. S. Chithra, Suma Shastry
| lyrics5 = 
| length5 = 
| title6 = Ee Jagave Namadu
| extra6 = L. N. Shastry, K. S. Chithra
| lyrics6 = 
| length6 = 
|}}

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 

 