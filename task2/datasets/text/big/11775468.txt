Thoda Pyaar Thoda Magic
 
 
{{Infobox film
| name = Thoda Pyaar Thoda Magic
| image = Thoda Pyaar Thoda Magic Movie Poster.jpg
| caption = Movie Poster for Thoda Pyaar Thoda Magic
| director = Kunal Kohli
| producer = Aditya Chopra Kunal Kohli
| writer = Kunal Kohli
| starring = Saif Ali Khan Rani Mukerji Rishi Kapoor Ameesha Patel
| music = Shankar-Ehsaan-Loy
| cinematography = Sudeep Chatterjee
| editing = Amitabh Shukla
| studio = Kunal Kohli Productions
| distributor = Yash Raj Films
| released =  
| runtime = 137 minutes
| country = India
| language = Hindi
| budget =
| gross =  
}} Hindi film with Saif Ali Khan and Rani Mukerji in lead roles, and Rishi Kapoor and Ameesha Patel in special appearances. The child artists include Akshat Chopra, Shriya Sharma, Rachit Sidana and Ayushi Burman. Directed, written and co-produced by Kunal Kohli, the film is produced and distributed by Yash Chopra and Aditya Chopra under Yash Raj Films.

== Synopsis ==
Ranbeer Talwar (Saif Ali Khan), a leading industrialist in India, is lonely because he has lost everyone who is dear to him. Due to distraction, he mistakenly kills a husband and wife in a car accident. The judge sentences him to look after their four children, and to do so without ill-treating them. The children resent Ranbeer for his role in their parents death and desire revenge, whereas he is unprepared to live with them, making both him and the children miserable in this situation.

One day, the children pray to God (Rishi Kapoor) for help. God discusses the matter with his fairylike angels and decides to send Geeta (Rani Mukerji), his favourite and the most whimsical of the angels, to unite Ranbeer with the children. God warns her not to use her ability to alter reality during her time on earth. Geeta ignores Gods warning and uses this power often, providing comic relief. She appoints herself as the childrens governess and places the children in awe of her by using her transformative powers. Soon, she ingratiates herself with the children with her supportive affection and positiveness. The children begin to cherish her and depend on her while Ranbeer remains perplexed and overwhelmed by her dominance of his affairs. He soon becomes enamored of her, despite the interference by his girlfriend Malaika (Ameesha Patel).

Ranbeers relationship with Malaika ends when the children spoil her party. Ranbeer eventually begins getting involved with the childrens lives after Geeta tells him that he just needs to see their dreams, and see what troubles them. As Ranbeer and the children slowly become friends, the children forsake their desire for revenge and treat him as an older brother. Geeta is called back to heaven as she has succeeded in uniting Ranbeer and the children. Upset, Ranbeer and the children go to a church to pray that she return to them. After God talks with Geeta, he concedes to her wishes and changes Geeta into a human, allowing her to return to the children. Geeta marries Ranbeer and gives birth to a daughter, who inherits the ability to alter reality, despite Geetas loss of her own power. Nevertheless, they all live happily ever after.

===Plot Similarity=== The Sound Mary Poppins and Nanny McPhee, which also features a governess as its crucial character. The character of Ranbeer Talwar is based on King Uncle.

==Production==

===Casting===
Kunal Kohli considered Aamir Khan and Kajol for the lead roles, but both Aamir and Kajol were busy with their home productions, Jaane Tu Ya Jaane Na (2008) and U, Me Aur Hum (2008) respectively.

===Development===
In September 2007, the film began shooting in Delhi with some portions also being shot in Alibaug, Bangkok and Los Angeles.  The film completed its shooting on 3 February 2008. 

Rani Mukerji sports one layered outfit designed by Manish Malhotra throughout the film while some layers have been taken off according to the requirements of each scene. Kunal Kohli initially asked Ameesha Patel to lose slight weight for her role in the film. Kohli and Malhotra spent hours deciding her look in the film. She dons a yellow bikini in the film in the hit song lazy lamhe.

The movie opened in Canada on 27 June 2008.  

== Cast ==
* Saif Ali Khan as Ranbeer Talwar
* Rani Mukerji as Geeta
* Akshat Chopra as Vashisht Walia
* Shriya Sharma as Aditi Walia
* Rachit Sidana as Iqbal Walia
* Ayushi Burman as Avantika Walia
* Rishi Kapoor as God (Special appearance)
* Ameesha Patel as Malaika (Special appearance)
* Sharat Saxena as Judge
* Razak Khan as Pappu
* Mahesh Thakur as Lawyer
* Tarana Raja as Kapoor
* Danish Aslam as Kid Ranbeers Teacher
* Biren Patel as Yada
* Cameron Pearson as Steve
* Tigerlily Perry as Michelle
* Steven Schneider as American Boy

==Box office==
According to Box Office India, Thoda Pyaar Thoda Magic had a poor opening and altogether grossed  . It was declared a Flop at the Box Office. 

== Reviews and reception ==
The film opened to a decent collections at the box office and picked up during the first weekend. It could not sustain business for long, however, due to the release of two big movies, Lovestory 2050 and Jaane Tu Ya Jaane Na the following weekend. Noyon Jyoti Parasara of AOL India gave the movie 3 out of 5 saying, "The story brings out quite a few emotions. And this is helped by its simplicity".  One India Entertainment said that, "It takes you back to the light-hearted, feel-good cinema made by directors of calibre like Hrishikesh Mukherjee and Basu Chatterjee." 

==Music==
{{Infobox album
| Name = Thoda Pyaar Thoda Magic
| Type = soundtrack
| Artist = Shankar-Ehsaan-Loy
| Cover = TPTM CD.jpg
| Released =  
| Recorded =
| Genre = Film soundtrack
| Length = 29:16
| Label = YRF Music
| Producer = Shankar-Ehsaan-Loy
| Last album  = Taare Zameen Par (2007)
| This album  = Thoda Pyaar Thoda Magic (2008)
| Next album  = Rock On!! (2008)
}}

The music of the film is composed by Shankar-Ehsaan-Loy with lyrics by Prasoon Joshi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s) !! Duration !! Notes
|-
| "Pyaar Ke Liye"
| Shankar Mahadevan
| 5:07
| Picturised on Saif Ali Khan as a young boy
|-
| "Nihaal Ho Gayi"
| Shankar Mahadevan
| 4:11
| Picturised on Saif Ali Khan & Rani Mukerji
|-
| "Bulbula"
| Shankar Mahadevan & Sunidhi Chauhan
| 3:21
| Picturised on Rani Mukerji & Rishi Kapoor
|-
| "Lazy Lamhe"
| Anusha Mani
| 5:12
| Picturised on Ameesha Patel, Saif Ali Khan & Rani Mukerji
|-
| "Beetey Kal Se"
| Shreya Ghoshal & Sneha Suresh
| 4:56
| Picturised on Rani Mukerji & the child artistes
|-
| "Nihaal Ho Gayi" (Remix) - DJ Aqeel
| Shankar Mahadevan
| 3:51
| Picturised on Saif Ali Khan & Rani Mukerji
|-
| "Lazy Lamhe" (Remix) - DJ Aqeel
| Anusha Mani
| 5:58
| Picturised on Ameesha Patel & Saif Ali Khan
|}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 