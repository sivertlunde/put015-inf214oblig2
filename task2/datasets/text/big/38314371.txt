Bilangin ang Bituin sa Langit
{{Infobox film
| name      = Bilangin ang Bituin sa Langit (Count the Number of Stars in the Sky)
| image_size   =
| image	= Bilangin ang mga Bituin sa Langit poster.jpg	
| alt      =
| caption    = Official POster of Bilangin ang mga Bituin sa Langit
| director    = Elwood Perez
| producer    = Lily Monteverde
| writer     = 
| screenplay   = Jake Cocadiz, Jigs Recto	
| story     = 
| based on    =
| starring    = Nora Aunor Tirso Cruz III
| music     = Lutgardo Labad
| cinematography = Ricardo Jacinto
| editing    = George Jarlego	 	
 | studio     = 
| distributor  = Regal Films
| released    = August 17, 1989 
| runtime    =
| country    =   Filipino
| budget     = 
| gross     = 
}}

Bilangin ang Bituin sa Langit is a 1989 drama it is a story of a melodramatic and panoramic portrait of the rise and fall of a poor, hard-working, and determined barrio lass and her lifetime stormy relationship with a childhood sweetheart. Legendary Nora Aunor and Tirso Cruz III are magnificent in dual roles, in a love-hate affair that spans generations, from their high school days in the province, to their twilight years in the big city. 

Bilangin ang Bituin sa Langit is the Guy & Pip love teams award-winning movie. Nora won Best Actress for this film in Urian, Famas, and FAP while Tirso won Best Actor in Star, Famas, and FAP.

==Synopsis==
Noli (Nora Aunor), is an intelligent girl but a poor farmers daughter. She dreams of going to college to become a doctor. To realize her dreams, her father, Damian, mortgages his farm to a rich land owner, Dona Martina (Gloria Romero). But the old woman and his son, Anselmo (Tirso Cruz III), look down on Nolis family and sneer at her ambitions. Perseverance pays off and Noli is able to finish college and claims back their property from Dona Martina. Noli and Anselmo fall in love with each other. But Dona Martina resents the relationship. Noli gets pregnant, does not tell Anselmo and instead marries Arturo despite the objections of his sister, Margarita. Then Arturo commits suicide when he finds out that the child is not his. Years pass and Nolis daughter, Maggie, grows up to be a beautiful but rebellious young lady with her a mothers looks but with exact opposite character. She falls in love with Jun, who turns out to be Anselmos son. Things soon become complicated. 

==Cast==
*Nora Aunor ... Doña Magnolia Dela Cruz vda. de Zulueta/Maggie Zulueta
*Tirso Cruz III ... Dr. Anselmo Santos/Anselmo Santos Jr. Gloria Romero ... Doña Martina Santos Miguel Rodriguez ... Señor Arturo Zulueta
*Ana Margarita Gonzales ... Margot Zulueta
*Perla Bautista ... Cedes Santos
*Beverly Salviejo ... Nolis Bestfriend
*Rolando Tinio ... Principal
*Manjo del Mundo ... Nolis Brother

==Review==
*This is the quintessential Tagalog movie and a loving tribute to the "Golden Age of Philippine Cinema."  - Mario A. Hernando, Malaya Newspaper, 1989

*Aunor is a prime mover here. It’s a relief to see her come out of semi-retirement in fine form, still eons away from the calculated angst of Sharon Cuneta. Aunor as a heroine is in a class by herself. No one, not even Vilma Santos, can quite strike the chord this Superstar, with her chocolate skin and sad eyes, does in the Filipino audience—for all her years in the movies and on TV, she has retained an admirably disciplined lack of guile as an actress.  Her character—the brown-skinned peasant triumphing over the mestizo aristocrat—is laden with cliches, some of which Aunor has lived in real life.  But Aunor was never one for predictability. Just when you expect her to resort to hysterical drivel, you gaze into her eyes, the eyes of an old woman, and feel a chill run down your spine. - Melissa G. Contreras, “Guy and Pip Grow Up,” Chronicle Weekend Guide, August 19, 1989

==Awards and Recognition==

{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Group
! width="25%"| Category
! width="25%"| Nominee
! width="10%"| Result
|-
| rowspan="25" align="center"| 1990
| rowspan="5" align="left"| FAMAS Filipino Academy of Movie Arts and Sciences Awards
| align="left"| Best Picture
| align="center"| 
|  
|-
| align="left"| Best Director
| align="center"| Elwood Perez
|  
|-
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Best Actor
| align="center"| Tirso Cruz III
|  
|-
| align="left"| Best Supporting Actress Gloria Romero
|  
|-
| rowspan="10" align="left"| Gawad Urian Awards (Manunuri ng Pelikulang Pilipino)
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Best Music
| align="center"| Lutgardo Labad
|  
|-
| align="left"| Best Production Design
| align="center"| Raymond Bajarias Ray Maliuanag Gerry Pascual Freddie Valencia 
|  
|-
| align="left"| Best Actor
| align="center"| Tirso Cruzz III
|  
|-
| align="left"| Best Director
| align="center"| Elwood Prez
|  
|-
| align="left"| Best Picture
| align="center"|  
|  
|-
| align="left"| Best in Cinematography
| align="center"| Ricardo Jacinto 
|  
|-
| align="left"| Best Editing
| align="center"| George Jarlego 
|  
|-
| align="left"| Best Screenplay 
| align="center"| Jake Cocadiz 
|  
|-
| align="left"| Best Sound
| align="center"| Joe Climaco  
|  
|-
| rowspan="6" align="left"| Film Academy of the Philippines (Luna Awards)
| align="left"| Best Picture
| align="center"| 
|  
|-
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Best Actor
| align="center"| Tirso Cruz III
|  
|-
| align="left"| Best Director
| align="center"| Elwood Perez 
|  
|-
| align="left"| Best Original Screenplay
| align="center"| Jake Cocadiz Jigs Recto 
|  
|-
| align="left"| Best Production Design
| align="center"| Raymond Bajarias Ray Maliuanag Gerry Pascual Freddie Valencia 
|  
|-
| rowspan="3" align="left"| Star Awards for Movies (Philippine Movie Press Club)
| align="left"| Best Actor
| align="center"| Tirso Cruz III
|  
|-
| align="left"| Production Designer of the Year
| align="center"| Ray Maliuanag
|  
|-
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="left"| Catholic Mass Media Awards
| align="left"| Best Actress
| align="center"| Nora Aunor
|  
|}

==References==
 

== External links ==
*  

 
 
 
 
 