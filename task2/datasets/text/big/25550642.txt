The Man Who Came Back (2008 film)
{{Infobox film
| name = The Man Who Came Back
| image = The-man-who-came-back-20081224115500285-000.jpg
| caption = DVD cover
| director = Glen Pitre
| producer = Michelle Benoit Stephen Bowen Sam Cable Chuck Walker
| writer = Glen Pitre Chuck Walker
| starring = Eric Braeden Billy Zane Sean Young George Kennedy Armand Assante
| music = Phil Marshall
| cinematography = Stoeps Langensteiner
| editing = Matthew Booth Simon Carmody
| distributor = Anchor Bay Entertainment Grindstone films
| released =  
| runtime = 112 minutes
| country = United States
| language = English
| budget =
| gross =
}}
The Man Who Came Back is a 2008 western film directed by Glen Pitre. It stars Eric Braeden, Billy Zane, George Kennedy, and Armand Assante.

==Plot==
The Man Who Came Back is based on the Thibodaux massacre, the second bloodiest labor strike in U.S. history.

Following the American Civil War, recently freed slaves on a Louisiana plantation are worked relentlessly but paid minimum wages. Sometimes paid only in scrip, worthless money accepted only at the plantations overpriced store, the workers become financially in debt. This creates an inescapable situation for these free men looking to gain real freedom.

In an attempt to better their lives, the workers strike. This leads to massive retaliation by the most powerful men in the town, including the sheriff (Armand Assante), the preacher (Al Hayter), power-hungry Billy Duke (James Patrick Stuart) and his vigilante group of thugs.

White overseer Reese Paxton (Eric Braeden) steps up to demand justice for his workers. Dukes rage turns on Paxton and his family. Despite assistance from a Yankee attorney (Billy Zane), Paxton is convicted by Billys father, the corrupt Judge Duke (George Kennedy).

After being sent to prison, beaten within inches of his life, and having endured merciless emotional torture, Paxton "comes back" to seek revenge.

==Cast==
* Eric Braeden as Reese Paxton
* Billy Zane as Ezra
* George Kennedy as Judge Duke
* Armand Assante as Amos
* Sean Young as Kate
* Carol Alt as Angelique Paxton
* James Patrick Stuart as Billy Duke
* Ken Norton as Grandpa
* Peter Jason as The Warden
* Jennifer ODell as a prostitute

==External links==
*  

 
 
 
 
 

 