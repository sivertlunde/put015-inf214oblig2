David & Fatima
{{Infobox film
| name           = David & Fatima
| image          = David and fatima.jpg
| caption        = Theatrical release poster
| director       = Alain Zaloum
| producer       = Tammi Sutton
| writer         = Kari Bian Alain Zaloum Patrick Krauss Randala
| starring       = Cameron Van Hoy Danielle Pollack Martin Landau Tony Curtis
| music          = Michael J. Lloyd
| cinematography = Neil Lisk
| editing        = Richard Francis-Bruce Kimberly Generous White
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Palestinian woman Israeli man from Jerusalem who fall in love. The film is a retelling of William Shakespeares Romeo & Juliet, and was directed by Alain Zaloum, and stars Cameron Van Hoy, Danielle Pollack, Merik Tadros, Anthony Batarse, Ismail Kanater, Sasha Knopf, John Bryant Davila, Ben Kermode, Allan Kolman Tony Curtis and Martin Landau. This was the last fictional movie Tony Curtis starred in.

The film encourages Arab Israeli peace.

==Development==
Kari Bian, the executive producer and one of the writers, Savitz, Masha. " " ( ).   living in  . Wednesday July 2, 2008. Retrieved on February 26, 2015.  Alain Zaloum, the director and the other writer,  is a Cairo-born  . Retrieved on February 27, 2015.  Richard Francis-Bruce did editing work. 

==Production==
The film was almost completely shot in Los Angeles,  and shooting took place for five weeks.  Tim Worman, the art director, developed areas to appear like the films settings. Some exterior shots were actually made in Israel. Dialect coaches trained the actors. In addition the actors read history texts about the conflict region. The films budget was $600,000 ($  adjusted for inflation). 

==Cast==
Americans portrayed almost all of the major characters. Goldstein, Gary. " " ( ). Los Angeles Times. June 30, 2008. Retrieved on February 26, 2015. 
* David Isaac - Cameron Van Hoy
** Alain Zaloum stated that he modeled David after himself as he is married to a woman with a different religion.  Van Hoy directly applied for the acting role instead of using an agent.  Van Hoy was in New York City during the September 11, 2001 attacks, and according to him he used that experience while portraying David. 
* Fatima Aziz - Danielle Pollack
** Pollack, a Jewish woman who originates from New York,  is not an Arab. Seth Frantzman of the  . December 22, 2009.  . Retrieved on February 26, 2015.   Her role as Fatima was her first professional film job.  Despite her inexperience, Van Hoy gave the filmmakers the suggestion of using Pollack;  Pollack and Van Hoy first met each other as students at the Fiorello H. LaGuardia High School.  Pollack stated that through working on the film, she "got to see both sides clearly" and that she learned "there are two sides of the story".  As part of her research she put on a hijab and went shopping at a supermarket to absorb how others around her reacted. 
* Rabbi Schmulic - Martin Landau
* Benny Isaac - Allan Kolman
* Ishmael Aziz - Anthony Batarse
* Aiida Aziz - Yareli Arizmendi
* Sarah Isaac (Davids Mother) - Colette Kilroy
** Kilroy originates from Malibu 
* Mr. Schwartz - Tony Curtis
* Hassan Faraj - Merik Tadros
* Tami Isaac - Sascha Knopf
* Avi Weinstein - Ben Kermode
* IDF Recruitment Officer - Michael Yavnielli
* Christian Priest - Joey Naber
* Imam - Ismail Kanater
* IDF Soldier - John Bryant Davila

==Accuracy==
Frantzman wrote that the films depiction of the Israeli Defense Force (IDF) was "accurate", "gritty", and "sometimes unflattering".  He added that Beit Hanina an Arab doctors house in real life would be more luxurious than the one the film portrays; in addition Frantzman stated that in Jerusalem he had never encountered a bellydancing restaurant like one portrayed in the film and he did not believe such a restaurant existed. 

==Release==
A screening at the Laemmle 4-Plex Theater in Santa Monica, California was scheduled to run until July 25, 2008.  On September 12, 2008, the film premiered in Beverly Hills, California.  The filmmakers intended to distribute the film throughout the United States and in Israel.  There are subtitles available in Arabic, Hebrew, and Persian. 

==Reception==
During the Napa Sonoma Wine Country Film Festival the film received the Robert and Margrit Mondavi Award for Peace and Cultural Understanding. 

Frantzman wrote that because ordinary Israelis prefer American films and the upper class prefers "self-critical" films, David & Fatima "received almost no attention in Israel". 

Gary Goldstein of the Los Angeles Times criticized the "somewhat ersatz quality" that he says originates from the casting of Americans who made "a jumble of imprecise accents that makes one long for native speech and English subtitles." 

Frantzman himself concluded "David and Fatima presents an honest story, but one that also doesnt work in the end." 

==References==
 

==Further reading==
* Binenfeld, Molly. " " ( ). Jewish Journal. July 23, 2008.

==External links==
*  
* " " 60 Second Trailer in Persian - Internet Archive
 

 
 
 
 
 
 
 
 
 
 
 

 