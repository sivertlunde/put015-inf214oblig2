Revenge of the Nerds II: Nerds in Paradise
 
{{Infobox film
| name = Revenge of the Nerds II:  Nerds in Paradise
| image = Revenge Of The Nerds 2.jpg
| caption = Theatrical release poster
| alt = Nerds at the beach, a massive wave in the background
| director = Joe Roth
| producer = Ted Field Robert W. Cort Peter Bart Steve Marshall
| starring = Robert Carradine Anthony Edwards Curtis Armstrong Larry B. Scott Timothy Busfield
| music = Gerald V. Casale Mark Mothersbaugh Charles Correll
| editing = Richard Chew
| studio = Interscope Communications
| distributor = 20th Century Fox
| released =  
| runtime = 98 minutes
| country = United States
| language = English
| budget = $10 million
| gross = $30,063,289
}}
 comedy Revenge of the Nerds. Its cast featured most of the main actors from the earlier film, including Robert Carradine, Anthony Edwards (in a smaller role), Curtis Armstrong, Larry B. Scott, Timothy Busfield, Donald Gibb, and Andrew Cassese. This film also provided an early starring role for Courtney Thorne-Smith. Other cast members include Bradley Whitford, Ed Lauter, and Barry Sobel.

One of the movie trailers parodies the trailer of   where they pan the inside of a house, then the phone rings, and the person sitting in the chair facing away from the camera picks up, then the person turns to reveal its Heather ORourkes character, Carol-Ann, who says "Theyre Back." In the Nerds version, Robert Carradines character, Lewis, turns and says "Were Back!" and does his infamous nerd laugh.

==Plot==
The Lambda Lambda Lambda from Adams College are packing their suitcases to get ready for a national fraternity convention in Fort Lauderdale, Florida. Just before heading to the airport, Lewis (Robert Carradine) best friend Gilbert (Anthony Edwards) explains to him that he feels stupid that he cannot come, due to breaking his leg (during a chess match).

After the group arrive at Fort Lauderdale and head to the Royal Flamingo Hotel, a trainee named Sunny (Courtney Thorne-Smith) informs Lewis that their reservation is canceled and has been given to the Alpha Betas. The Royal Flamingo manager (Ed Lauter) explains to Sunny that they do not want nerds staying in this hotel. The Tri-Lambs also meet Stewart (Barry Sobel), a geeky bellboy at the hotel who is also friends with Sunny. Poindexter finds another hotel called the Hotel Coral Essex, located in a shady neighborhood, and makes their reservation there. Roger (Bradley Whitford), the president of the local chapter of Alpha Betas (and chairman of the regional conference) is seen planning with his fellow jocks to get rid of the Tri-Lambs by any means necessary. Furthermore, Ogre (Donald Gibb) is revealed to be at the conference, scheming with Roger and the Alphas.

The Lambdas arrive at the Florida swamplands hoping to attend the United Fraternity pre-conference barbecue. They take the wrong way and see a group of Seminole Indians capture an Indian maiden and throw her into a flame (an act concocted by the Alphas to humiliate the Tri-Lambs into leaving the conference). They are captured and forced to strip to their underwear, until Poindexter (Timothy Busfield) tests their ethnicity by yelling "Bite my crank" at them in Seminole, to which no one responds. Theyre chased off by Ogre, and forced to make their way across town back to their hotel in their underwear.

The next day, Roger announces a new bylaw to be voted upon by the conference; "Proposition 15", a rule that would require physical as well as academic standards to be met by all members of the conference. Before Lewis can make any argument against the proposition, he is co-opted by a wet-nightie contest happening poolside. The Lambdas decide to beat the Alphas at their own game and throw a party at the Hotel Coral Essex. The neon sign outside the hotel is deliberately broken (so that it reads "HOTel cORAL esSEX"), which draws people for miles. The Tri-Lambs (along with Stewart) perform outside of the Hotel Coral Essex, winning over the crowd, and Prop 15 is voted down the next morning. At this time, Roger, seemingly sincere, expresses a desire to make peace once and for all with the Tri-Lambs, and to that end he proposes a bylaw stating that any fraternity found to be guilty of a crime will be expelled from the national conference and their charter revoked. The Lambdas, satisfied that the Alphas now cannot attack them without being expelled from the conference, accept their offer of friendship and their luxurious hotel suite at the Royal Flamingo. Roger encourages Sunny to grab a couple of girls and take the Tri-Lambs to the beach in his car. While on the beach Sunny and Lewis get to know and like each other. Unfortunately, Roger has reported his car stolen, and the Lambdas are arrested.

Stewart and Sunny bail the Tri-Lambs out of jail. Sunny apologizes and tries to explain that she knew nothing of Rogers plan, but Lewis doesnt believe her. Before she can explain, The Alpha Betas kidnap the Tri-Lambs along with Sunny and dump them on an uninhabited island. Disgusted by Rogers actions, Sunny jumps overboard. When they realize Ogre cannot be trusted to keep his mouth shut, they also throw him into the water, despite the fact that he cannot swim. Upon seeing Ogre drowning, Wormser jumps into the ocean and rescues him, though he is seemingly ungrateful. As the group sleeps Lewis has a dream about Gilbert, who explains to him that all is not lost and implores Lewis not to give up. He also points out that Lewis is acting like a jerk to Sunny, who is stranded with him on this island by choice. This helps give Lewis the confidence he needs to apologize to Sunny for his behavior and to try and find a way off the island.

The next morning The Tri-Lambs, using their combined intelligence and ingenuity, find a Cuban military officers hidden cache of military supplies, including an amphibious vehicle. Meanwhile at the conference, Roger is presiding over the vote to expel the Tri-Lambs, when, decked out in military gear, they literally crash the conference, driving right through the conference room wall, then chase the attendees out to the pool. Roger is unfazed and tries to continue to have the Lambdas voted out for stealing his car, until Sunny reveals that Roger set them up and kidnapped them. Roger states that it doesnt matter since he will always be cool, popular, and good-looking and the Lambdas will always be weird, different, and pathetic, and that theres nothing they can do or say about it. Lewis takes a moment and quietly agrees with Roger, stating that theres nothing he can say, but that there is "something Ive gotta do about it." and punches Roger in the jaw, knocking him into the pool.

Back at Adams College, in an induction ceremony, Ogre is sworn in to Lambda Lambda Lambda as the newest member.

==Cast==
* Robert Carradine as Lewis Skolnick
* Anthony Edwards as Gilbert Lowe
* Curtis Armstrong as Dudley "Booger" Dawson
* Larry B. Scott as Lamar Latrelle
* Timothy Busfield as Arnold Poindexter
* Andrew Cassese as Harold Wormser
* Courtney Thorne-Smith as Sunny Carstairs
* Bradley Whitford as Roger Latimer
* Barry Sobel as Stewart
* James Cromwell as Mr. Skolnick
* Ed Lauter as Buzz
* James Hong as Snotty
* Donald Gibb as Fred "Ogre" Palowakski Tom Hodges as Tiny
* Jack Gilpin as Mr. Comstock
* Michael Fitzgerald Potroast

This is the only Nerds film in which Betty does not appear (although a picture of her can be seen at the beginning of the film). Stan Gable, Takashi, and U.N. Jefferson do not appear.  This is the last Nerds movie to feature Poindexter.  Although he is shown on the DVD cover of the fourth film, he is not seen nor mentioned in it.  This is also the last Nerds movie to include Anthony Edwards as Gilbert and Andrew Cassese as Wormser.  They are played by other actors in the third film and stay offscreen in the fourth and final film.

== Reception ==

The film received a mostly negative reaction from critics.   It currently holds an 8% rating on Rotten Tomatoes based on 25 reviews with the critical consensus "It reunites most of the original cast and rounds them up for a trip to Fort Lauderdale for spring break, but Revenge of the Nerds II: Nerds in Paradise forgets to pack enough jokes or compelling characters to make it through its 89-minute running time."

===Box office===

The movie was a box office success. 

== References ==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 