Under the Volcano (film)
{{Infobox film
| name = Under the Volcano 
| image =Under the volcano, film poster.jpg
| image_size = 175px
| caption =
| director = John Huston Danny Huston (opening titles) 
| producer = Michael Fitzgerald  Moritz Borman  Wieland Schulz-Keil
| writer = Guy Gallo
| starring = {{Plainlist|
* Albert Finney
* Jacqueline Bisset
* Anthony Andrews
}}
| music = Alex North
| cinematography =Gabriel Figueroa
| editing =
| distributor = Universal Pictures (USA) 20th Century Fox (non-USA)
| released =  
| runtime = 112 minutes
| country = United States
| language = English
| budget =
| gross = $2,556,800
}}
 Best Actor in a Leading Role (Albert Finney) and Best Music, Original Score (Alex North).
 the semi-autobiographical 1947 novel by English writer Malcolm Lowry.
 British consul in the small Mexican town of Quauhnahuac (recognizably Cuernavaca) on the Day of the Dead in 1938.

==Cast==
*Albert Finney ...  Geoffrey Firmin 
*Jacqueline Bisset ...  Yvonne Firmin 
*Anthony Andrews ...  Hugh Firmin 
*Ignacio López Tarso ...  Dr. Vigil
*Katy Jurado ...  Señora Gregoria 
*James Villiers ...  Brit 
*Dawson Bray ...  Quincey 
*Carlos Riquelme ...  Bustamante 
*Jim McCarthy ...  Gringo 
*José René Ruiz ...  Dwarf
*Eleazar García, Jr. ...  Chief of Gardens 
*Salvador Sánchez ...  Chief of Stockyards
*Sergio Calderón ...  Chief of Municipality
*Araceli Ladewuen Castelun ...  María 
*Emilio Fernández ...  Diosdado

==Reception==
The film was entered into the 1984 Cannes Film Festival.   

Reviewing in The New York Times, Janet Maslin had much praise for Finneys performance:
:Drunkenness, so often represented on the screen by overacting of the most sodden sort, becomes the occasion for a performance of extraordinary delicacy from Albert Finney, who brilliantly captures the Consuls pathos, his fragility and his stature. Alcoholism is the central device in Mr. Lowrys partially autobiographical novel. (The author, like the Consul, was capable of drinking shaving lotion when nothing more potable was at hand.) Yet the Consuls drinking is astonishingly fine-tuned, affording him a protective filter while also allowing for moments of keen, unexpected lucidity. Mr. Finney conveys this beautifully, with the many and varied nuances for which Guy Gallos screenplay allows. For instance, when the exquisite Yvonne (played elegantly and movingly by Jacqueline Bisset) reappears in Cuernavaca one morning, she finds her ex-husband in a cantina, still wearing his evening clothes. He turns to gaze at her for a moment, pauses briefly, and then continues talking as if nothing had happened. Seconds later, he turns again and looks at Yvonne more closely, still not certain whether or not this is a hallucination. It takes a long while for the fact of Yvonnes return to penetrate the different layers of the Consuls inebriated consciousness, and Mr. Finney delineates the process with grace and precision, stage by stage. 

==Awards==
The film was enthusiastically received.
*Nominated - Best Actor Academy Award (Albert Finney)
*Nominated - Best Original Score Academy Award (Alex North)
*Nominated - Palme dOr Cannes Film Festival
*Nominated - Best Actor Golden Globe (Albert Finney)
*Nominated - Best Supporting Actress Golden Globe (Jacqueline Bisset)
*Winner - Best Actor, London Film Critics Circle (Albert Finney)
*Winner - Best Actor, Los Angeles Film Critics Association (Albert Finney; tied with F. Murray Abraham in Amadeus)
*Selected - Top 10 Films of the Year - National Board of Review
*Nominated - Best Actor, New York Film Critics Circle (Albert Finney)

==Related documentary films==
Hustons drama has sometimes been shown in tandem with an earlier documentary film:   (1976) is a National Film Board of Canada feature-length documentary produced by Donald Brittain and Robert A. Duncan and directed by Brittain and John Kramer. It opens with the inquest into Lowrys "death by misadventure," and then moves back in time to trace the writers life. Selections from Lowrys novel are read by Richard Burton amid images shot in Mexico, the United States, Canada and England.

There are two documentaries about the making of the Huston film: Gary Conklins 56-minute Notes from Under the Volcano and the 82-minute Observations Under the Volcano, directed by Christian Blackwood.

==See also==
* List of American films of 1984

==References==
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 