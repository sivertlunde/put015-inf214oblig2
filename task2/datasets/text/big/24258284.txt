Viswa Mohini
{{Infobox film
| name           = Viswa Mohini
| image          = Viswamohini 1940.JPG
| image_size     =
| caption        =
| director       = Y. V. Rao
| producer       =
| writer         = Balijepalli Lakshmikanta Kavi Y. V. Rao
| narrator       =
| starring       = Chittor V. Nagaiah Y. V. Rao Bezawada Rajarathnam Pushpavalli T. Suryanarayana Doraiswamy Coconada Rajarathnam P. Ganga Rathnam Rangaswamy Lalitha Devi  Sampurna
| music          = Ogirala Ramachandra Rao
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 1940
| runtime        = 195 minutes
| country        = India Telugu
| budget         =
}}

Viswa Mohini ( , romance  film directed by Y. V. Rao. The film written by Balijepalli Lakshmikanta Kavi is touted to be the first film on the Indian motion picture world.   

==Plot==
Purushottam (Y.V. Rao) embezzles money from a firm of brokers owned by Padmanabham in order to get his son Mohan Rao married to Hemalatha, daughter of the millionaire Visalakshamma (B Rajaratnam). The now impoverished Padmanabham partially gets his own revenge when his daughter Sushila (Lalitha Devi) becomes a film star Viswa Mohini. 

The film producer Pashupati, brother of Visalakshamma, introduces her. The star Viswa Mohini falls in love with Mohan Rao and her father agrees to their marriage provided Mohan can find a job. He pretends to have done so and the two get married. Hemalatha offers money to Viswa Mohini to go away and free Mohan, which, in an emotional scene, she refuses to do.

==References==
 

 
 
 
 

 