Menschen im Sturm
 
 
Menschen im Sturm (Eng.: People in the Storm) is a 1941 German film   directed by Fritz Peter Buch. It was anti-Serbian propaganda and part of a concerted propaganda push against Serbs, attempting to split them from the Croats. 

==Synopsis==
Vera witnesses the persecution of ethnic Germans in Yugoslavia, which awakens her ethnic consciousness. Her cosmopolitan friend Alexander is arrested. Vera flirts with the Serbian commander to allow Volksdeutsche to escape to the border. When arrested, she proudly affirms that she helped her countrymen and, in an escape attempt, is shot, to die happy and heroic.

==Motifs==
The film reprises many of the same motifs as Heimkehr, in an anti-Serbian rather than anti-Polish context. 

A Croat aids the Germans, stating that all Croats should be friendly, and is murdered by Serbs for it, reflecting a widespread cliche of the friendly Croat. 

==References==
 

== External links ==
* 

 
 
 
 
 


 