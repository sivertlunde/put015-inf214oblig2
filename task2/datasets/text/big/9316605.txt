Yo-Yo Girl Cop
{{Infobox Film
| name           = Yo-yo Girl Cop
| image          = Sukeban_Deka_2006.png
| caption        = Japanese theatrical poster
| director       = Kenta Fukasaku
| producer       = Tatsuya Kunimatsu
| writer         = Shoichi Maruyama
| starring       = Aya Matsuura v-u-den Riki Takeuchi Shunsuke Kubozuka
| music          = Goro Yasukawa
| cinematography = Takashi Komatsu
| editing        = Chieko Suzaki
| distributor    = Toei Company
| released       =    
| runtime        = 98 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}} Sukeban Deka: directed by Kenta Fukasaku.
 Yuki Saito, who played the role of Saki in the first live-action television series, appears here as Sakis mother. The movie was released on September 30, 2006 in Japan  and in the United States on July 17, 2007  by Magnolia Pictures  as Yo-Yo Girl Cop.

==Cast==
*Aya Matsuura as  
*v-u-den
**Rika Ishikawa as  
**Erika Miyoshi as  
**Yui Okada as   Yuki Saito as Sakis Mother
*Hiroyuki Nagato as  
*Shunsuke Kubozuka as  
*Riki Takeuchi as  
*Masai Ōtani, another Hello! Project member makes a cameo appearance in the film.
*Tak Sakaguchi as a member of the Enola Gay gang

==Music==
The movies theme song, "Thanks!" is by the Hello! Project group GAM (group)|GAM, consisting of Aya Matsuura and Miki Fujimoto. The song "Shinkirō Romance", also by GAM, was used an insert song.

==U.K. release==
Yo-Yo Girl Cop was licensed for U.K. release by 4Digital Asia, a sublabel of 4Digital Media,  formerly Ilc Entertainment. The new sub-label was launched in 2008 to fill the gap in the U.K. for "Asia Extreme" titles created by the demise of label Tartan.  It was released on DVD on September 22. 

==Miscellaneous== softcore V-Cinema release  , directed by Daigo Udagawa and starring AV idol Mihiro. In addition to its similar name, the cover artwork is a near-reproduction of Yo-yo Girl Cops original poster. The DVD was released in Japan in November 2006 and in the United States with English subtitles by Cinema Epoch in November 2008.   

==References and footnotes==
 

==External links==
*    
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 