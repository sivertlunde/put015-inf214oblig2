To Fili Tis Zois
{{Infobox film
| name           = To Fili Tis Zois
| image          = To_fili_tis_zois(film).jpg
| caption        =
| director       = Nick Zapatinas
| writer         = Nick Zapatinas
| narrator       =
| starring       = Cathrine Papoutsaki, Laertis Malkotsis, Zeta Douka
| music          = Soumka
| distributor    = Village Productions, BLACK ORANGE S.A.
| released       =  
| runtime        = 103 minutes
| country        = Greece
| language       = Greek
}}
To Fili Tis Zois ( ) is a 2007 film directed by Nick Zapatina.This film is known for its soundtrack, written and produced by Nick Zapatina (see To Fili Tis Zois (soundtrack)).

==Plot==
Paschalis (Laertis Malkotsis) is a 30 year old agronomist, about to marry his beloved Anthoula on the island of Milos. He accidentally embarks on the boat to Sifnos, where he meets Zoi (Catherine Papoutsaki), a beautiful but strange photographer who goes to the island for her own purposes. Things will become even more difficult for Paschalis when a strike of ship captains may cost him his marriage!  But things change when together with Zoi he meets a couple (Zeta Douka & Themos Anastasiadis) in Sifnos, who try to help him come together with his future wife in time for the marriage. But many unexpected situations lead to funny incidents and a lot of things are revealed about the four heroes.

==Cast==
*Cathrine Papoutsaki
*Laertis Malkotsis
*Zeta Douka
*Themos Anastasiadis
*John Zouganelis
*Sakis Boulas
*Cathrine Matziou
*Chris Tripodis

==Home media==
On Friday, 7 of March 2008, it was released on DVD. It contains English subtitles and Greek subtitles for the hearing impaired. It also contains a bonus feature introducing the landscapes and hotels of the island Sifnos, Greece.

==Box office==
The film made had an estimated number of more than 250,000 cinema ticket sales in its 4th week of release, all in Greek cinemas alone.

==See also==
*To Fili Tis Zois (soundtrack)
*To Fili Tis Zois (Elena Paparizou song)

==External links==
*  

 
 
 
 