The Ten-Year Lunch
{{Infobox film
| name           = The Ten-Year Lunch
| image          = The Ten-Year Lunch.jpg
| writer         = Peter Foges Mary Jo Kaplan
| starring       = 
| director       = Aviva Slesin
| producer       = Aviva Slesin
| distributor    = 
| released       =  
| runtime        = 56 minutes
| country        = United States
| language       = English
| budget         = 
}}
The Ten-Year Lunch: The Wit and Legend of the Algonquin Round Table is a 1987 American documentary film about the Algonquin Round Table, a floating group of writers and actors in the "Roaring Twenties" in New York City, which included great names such as Dorothy Parker, Robert Benchley, George S. Kaufman, Edna Ferber, Marc Connelly, Harold Ross and Harpo Marx.  It was produced and directed by Aviva Slesin and narrated by Heywood Hale Broun.

The title refers to the fact that the members of the Round Table met over lunch at the Algonquin Hotel from 1919 until roughly 1929. The film shows how the group drifted apart once the 1920s ended, as Hollywood beckoned for some and as they grew older.

In 1987, the film won Academy Award for Best Documentary Feature.    

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 