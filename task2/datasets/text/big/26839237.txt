Napoleon Blown-Aparte
{{Infobox Hollywood cartoon|
| cartoon_name = Napoleon Blown-Aparte
| series = The Inspector
| image = Napoleon Blown Aparte title.jpg
| caption =
| director = Gerry Chiniquy John W. Dunn
| animator = Manny Perez Don Williams Bob Matz Warren Batchelder Norm McCabe George Grandpré
| voice_actor = Pat Harrington, Jr. Larry Storch
| musician = William Lava
| producer = David DePatie Friz Freleng
| distributor = United Artists
| release_date = February 2, 1966
| color_process = DeLuxe
| runtime = 6 05" English
| preceded_by = Reaux, Reaux, Reaux Your Boat
| followed_by = Cirrhosis of the Louvre
}}
 short in the The Inspector|Inspector series of made for television cartoons. A total of 34 entries were produced between 1965 and 1969.

==Plot==
The Inspector protects the Commissioner when the Mad Bomber is out for revenge on him for sending him to prison. Despite this, the Commissioner ends up being the pawn of the Mad Bombers tricks.

==Production notes==
An alternate version of the Inspector theme "A Shot in the Dark" in heard during the credits. The music cue during the scene at the Commissioners country house is Camille Saint-Saënss "The Carnival of the Animals".

The Pink Panther Show contained a laugh track when the Inspector cartoons were broadcast on NBC-TV. When MGM released the first 17 Inspector cartoons on DVD in 2008, the theatrical versions were, for the most part, utilized. However, four television prints appeared on the DVD set, Napoleon Blown-Aparte being one of them. 
==See also== List of The Pink Panther cartoons

==References==
 

==External links==
*  
*  
* 


 
 
 

 

 
 
 
 


 