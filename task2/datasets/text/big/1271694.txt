Polyester (film)
 
{{Infobox film
| name          = Polyester
| image         = Polyester ver1.jpg
| alt           = 
| caption       = Theatrical release poster
| director      = John Waters
| producer      = {{Plainlist|
* Robert Shaye
* John Waters Michael White}}
| writer        = John Waters
| starring      = {{Plainlist| Divine
* Tab Hunter
* David Samson
* Edith Massey
* Mink Stole}}
| music         = {{Plainlist|
* Debbie Harry
* Michael Kamen
* Chris Stein}}
| cinematography= David Insley
| editing       = Charles Roggero Dreamland
| distributor   = New Line Cinema
| released      =  
| runtime       = 86 minutes  
| country       = United States
| language      = English
| budget        = $300,000 
}}
Polyester is a 1981 American black comedy film directed, produced, and written by John Waters, and starring Divine (actor)|Divine, Tab Hunter, Edith Massey, and Mink Stole. It was filmed in Waters native Baltimore, Maryland, and features a gimmick called "Odorama", whereby viewers could smell what they saw on screen through scratch and sniff cards.
 satirical look Religious Right.

==Plot==
The life of fat housewife Francine Fishpaw (Divine (actor)|Divine) is crumbling around her in her middle-class suburban Baltimore home. Her husband, Elmer (David Samson), is a polyester-clad lout who owns an X rating|X-rated theater, causing anti-pornography protesters to picket the Fishpaws house. She also states that "all the neighborhood women spit at me" whenever she is at the shopping mall. Francines children are Lu-Lu (Mary Garlington), her spoiled, slutty daughter, and Dexter (Ken King), her Juvenile delinquent|delinquent, Inhalant abuse|glue-sniffing son who derives illicit pleasure from stomping on womens feet. Also adding to Francines troubles is her snobby, class-conscious, cocaine-snorting mother, La Rue (Joni Ruth White), who robs Francine blind and only cares about her "valuable shopping time."

Francine seeks solace in her best friend, Cuddles Kovinsky (Edith Massey), an independently wealthy, simple-minded woman and the worlds oldest debutante. Cuddles was once the Fishpaws housekeeper, but she inherited a large sum of money from a very affluent family that she used to work for, and who has befriended Francine. This infuriates La Rue, who admonishes Francine, "She was a scrub-woman. Give her... car fare...a ham at Easter, but for Gods sake, dont hang around with her!" Cuddles tries to cheer Francine with "seize-the-day" Bromide (language)|bromides, to no avail.

Francine discovers that her husband is having an affair with his secretary, Sandra Sullivan ( ), and she tells her mother, "Im having an abortion, and I cant wait!"; and after Dexter is arrested at a supermarket for stomping on a womans foot, the media reveal that hes the "Baltimore foot stomper".
 home for unwed mothers. Meanwhile, on Halloween evening, La Rue is shot by Bo-Bo and his friend, who have come to trash the Fishpaw house. La Rue manages to retrieve the gun and shoots Bo-Bo dead. Lu-Lu comes home from the unwed mothers home and, upon discovering her dead boyfriend, tries to commit suicide by sticking her head in the oven. Francine comes home, sees her daughters suicide attempt, and faints. After this, Francines life begins to change. Dexter is released from jail, completely rehabilitated. Lu-Lu suffers a miscarriage from her suicide attempt and sees the error of her ways, turning from a high-school harlot to an artistic flower child who enthuses, "Look, mother, Ive discovered macramé!". Francine finally summons the strength to tell off La Rue. A beacon of light arrives in the form of lounge-suit-wearing, Corvette-driving Todd Tomorrow (Tab Hunter), lifting Francines spirits. Todd proposes marriage to an elated Francine, who accepts.

However, it is soon revealed that Todd is romantically involved with La Rue and they are conspiring to embezzle Francines divorce settlement and drive her insane. Meanwhile, Elmer and Sandra break into the house to kill Francine, but are felled by Dexter and Lu-Lu. Dexter steps on Sandras foot, causing her to accidentally shoot Elmer; Lu-Lu uses her macramé to strangle Sandra. Cuddles and her German chauffeur/fiancé Heintz (Hans Kramm) arrive and run over La Rue and Todd.

The film concludes with a happy ending for Francine, her children, and Cuddles and Heintz.

==Cast==
  Divine as Francine Fishpaw
* Tab Hunter as Todd Tomorrow
* David Samson as Elmer Fishpaw
* Edith Massey as Cuddles Kovinsky
* Mink Stole as Sandra Sullivan
* Ken King as Dexter Fishpaw
* Mary Garlington as Lu-Lu Fishpaw
* Joni Ruth White as La Rue
* Stiv Bators as Bo-Bo Belsinger
* Hans Kramm as Heintz
* Susan Lowe as Mall victim
* Cookie Mueller as Betty Lalinski
* George Hulse as Principal Kirk
* Mary Vivian Pearce and Sharon Niesp as Nuns
* Jean Hill as Gospel bus hijacker
* George Figgs as Abortion picketer
 

==Production==
 ]] Divine and Edith Massey, received top billing in this film. Dreamlander perennials Mink Stole, Mary Vivian Pearce, Cookie Mueller, Sharon Niesp, Marina Melin, Susan Lowe, and Jean Hill played small roles in Polyester. While their parts are integral to the plot, they are much smaller compared to their earlier roles.
 R rating (his previous films were all unrated or rated X). The film was set in a middle-class suburb of Baltimore instead of its slums and bohemian neighborhoods (the setting of Waters earlier films).

===Music===
These are the only songs known to be in the film: 
# "Polyester" by Tab Hunter – words and music by Chris Stein and Debbie Harry
# "Be My Daddys Baby (Lu-Lus Theme)" by Michael Kamen – words and music by Harry and Kamen
# "The Best Thing" by Bill Murray – words and music by Harry and Kamen

==="Womens pictures"=== exploitative genre of film that was popular from the 1950–60s and typically featured bored, unfulfilled, or otherwise troubled women, usually middle-aged suburban Housewife|housewives, finding release or escape through the arrival of a handsome younger man. "Womens pictures" were typically hackneyed B-movies, but Waters specifically styled Polyester after the work of the director Douglas Sirk, making use of similar lighting and editing techniques, even using film equipment and movie-making techniques from Sirks era. 

===Odorama===
  3D glasses. When a number flashed on the screen, viewers were to scratch and sniff the appropriate spot. Smells included the scent of flowers, pizza, glue, gas, grass, and feces. For the first DVD release of the film the smell of glue was changed due to, as Waters states, "political correctness".  The gimmick was advertised with the tag "Itll blow your nose!" 

After being prompted to scratch and sniff the bouquet of flowers, a quick swap was made substituting old ratty sneakers, resulting in a joke on the audience.

The ten smells were, 1. Roses, 2. Flatulence, 3. Model Airplane Glue, 4. Pizza, 5. Gasoline, 6. Skunk, 7. Natural Gas, 8. New Car Smell, 9. Dirty Shoes, and 10. Air Freshener.

A video release omits the numbers flashing onscreen as well as the opening introduction explaining Odorama. This version, created by Lorimar-Telepictures, was shown on cable TV in the United States.
 Independent Film Channel released reproduction Odorama cards for John Waters film festivals.

In the commentary track on the films 2004 DVD release, Waters expressed his delight at having the films audiences actually "pay to smell shit".

Producers of Rugrats Go Wild (Paramount Pictures|Paramount) used the Odorama name and logo in 2003, somewhat upsetting Waters when he learned that New Line Cinema had let the copyright lapse.  

The 2011 film   uses a scratch and sniff card now called "Aromascope", which is advertised as providing the fourth dimension in its "4D" format.

The film was re-screened by Midnight Movies at the Edinburgh International Film Festival in June 2011. The Odorama cards were recreated by Midnight Movies, Little Joe Magazine, and The Aroma Company to allow viewers to interact with the film as originally intended.

==Critical response==
Polyester received some good reviews from the mainstream press. Said Janet Maslin of The New York Times:
 

The film currently holds an 89% "fresh" rating on Rotten Tomatoes. 

==In popular culture==
Samples from the film were used in the song "Frontier Psychiatrist" by The Avalanches. The opening of the song samples a conversation between Francine Fishpaw and Principal Kirk that starts with the latter asking "Is Dexter ill today?", and continues as follows:
:"Francine Fishpaw: No, Mr. Kirk, Dexters in school!
:School Principal: Im afraid hes not, Mrs. Fishpaw.  Dexters truancy problem is way out of hand!  The Baltimore County School Board has decided to expel Dexter from the entire school system. expulsion is not the answer?
:School Principal: Im afraid expulsion is the only answer. It is the opinion of the entire staff that Dexter is criminally insane... "

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 