The White Angel (1936 film)
{{Infobox film
| name           = The White Angel
| image          = The-white-angel-1936.jpg
| caption        = movie poster
| director       = William Dieterle
| producer       =
| writer         = Mordaunt Shairp
| narrator       = 
| starring       = Kay Francis
| music          = 
| cinematography =
| editing        =
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $506,000   accessed 16 March 2014  gross = $1,416,000 
| preceded_by    = 
| followed_by    = 
}}
 historical drama film directed by William Dieterle and starring Kay Francis.  The film depicts Florence Nightingales pioneering work in nursing during the Crimean War.

==Plot== Victorian England, Florence Nightingale (Kay Francis) decides to become a nurse, puzzling her upper class family. She travels to Germany to the only nursing school. The training is arduous, but she endures and graduates. When she returns home, however, no one is willing to employ her.
 Ian Hunter), a reporter for The Times, she is permitted to recruit some nurses and lead them to Scutari in Turkey to tend the wounded. 

There, however, she is bitterly opposed by Dr. Hunt (Donald Crisp), who is in charge of the hospital. She remains undaunted, and soon wins the love of her patients. Each night, she passes through miles of the wards, carrying a lamp, so she can satisfy herself that her patients have all they need. Her tireless efforts greatly reduce the mortality rate. Her fame is spread by the newspapers, and Henry Wadsworth Longfellow writes a poem in her honor.
 Billy Mauch), a drummer boy she herself nursed back from the brink of death.

While she is only partially recovered, she is surprised when Sister Colomba shows up. The nun informs her that Dr. Hunt replaced her with Ella Stephens, a flighty socialite Nightingale had already rejected as a nurse. Under Stephens lax and uncaring leadership, conditions had greatly worsened. Nightingale returns to Scutari and sets things straight.

After the war ends, she returns home to England. By this time, even Dr. Hunt has reconsidered his opinion of her work, but his superior, Undersecretary of War Bullock (Montagu Love), remains steadfast in his opposition. Bullock tries to turn Queen Victoria against Nightingale, but the monarch instead shows her approval by presenting Nightingale with a broach.

==Cast==
* Kay Francis as Florence Nightingale Ian Hunter as Fuller Donald Woods as Charles Cooper
* Nigel Bruce as Doctor West
* Donald Crisp as Doctor Hunt
* Henry ONeill as Doctor Scott, a strong supporter of Nightingale Billy Mauch as Tommy
* Charles Croker-King as Mr. Nightingale
* Phoebe Foster as Elizabeth Herbert George Curzon as Sidney Herbert
* Georgia Caine as Mrs. Nightingale
* Ara Gerald as Ella Stephens
* Halliwell Hobbes as Lord Raglan
* Eily Malyon as Sister Colomba
* Montagu Love as Bullock
* Ferdinand Munier as Alexis Soyer, a cook who follows Nightingale to Scutari
* Lillian Kemble-Cooper as Parthenope "Parthe" Nightingale (as Lillian Cooper)
* Egon Brecher as Pastor Fliedner
* Tempe Pigott as Mrs. Waters Barbara Leonard as Minna
* Frank Conroy as Mr. Le Froy

==Reception==
According to Warner Bros records the film earned $886,000 in the US and Canada and $530,000 elsewhere. 
==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 