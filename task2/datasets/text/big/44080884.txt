Brihonnola
{{Infobox film
| name           = Brihonnola
| image          = 
| caption        = 
| director       = Murad Parvez
| producer       = Film Hawkar
| starring       = Ferdous Ahmed Sohana Saba Azad Abul Kalam Intekhab Dinar
| screenplay     = Murad Parvez
| story          = Murad Parvez
| music          = Emon Saha  
| based on       = 
| editing        = 
| cinematography = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 130 minutes
| country        = Bangladesh
| language       = Bangla
}}

Brihonnola is a 2014 Bangladeshi Film Feature film directed, produced and story written by Murad Parvez  based on religion crysis. The film stars Ferdous Ahmed, Sohana Saba, Azad Abul Kalam,  Jhuna Choudhury, Intekhab Dinar. Brihonnola is a tale of the goodness inherent in human nature prevailing over religious prejudice and racial dogmatism.

The film released on 19 September,    2014.

== Plot ==

Mohanpur is an ancient village, not unlike many such villages of our country, still deprived of the basic amenities of modern times. There is a local health centre minus a permanent doctor(Azad Abul Kalam) as no one is willing to work and live in a village. There is also a primary school with no more than a handful of pupils and the few who do go to school hardly ever attend college afterwards. The population of the village is just average, neither too dense nor too sparse. One it was a predominantly Hindu locality, but in course of time the proportion of Hindus has declined to the level of Muslims and is continuing to decline further. However, any conflict or clash between the adherents of the two faiths is yet unheard of in this village. All villagers live in an atmosphere of friendly tolerance.Everything has been going well for ages, but at one point an ancient banyan tree becomes the focal point of dispute between the two sections of the village community. Situated on public land, this tree has been witness to the passage of time. It is said that once this plot of land used to be the property of a Hindu zamindar, but at present it belongs to the government.

The villagers do not have many possessions, except for a few things. The Muslims have a mosque, the Hindus a temple. There is one community health centre and an ancient railway station. One day the Hindus suddenly began to think: “Wish we could have another temple!” The Muslims also thought: “If we could not have another mosque, why not an Eidgah at least?” And both sides wanted their temple or mosque to be erected on the empty public land. But the tree on the plot stood in the way.

The only resident doctor in the village, elderly homeopath Araj Ali(Azad Abul Kalam), had been applying to the authorities for the last five years for a good doctor for the health centre. Finally his application bore fruit, a qualified doctor was sent from the district town, and on the same day the village carpenter Tulsi’s(Intekhab Dinar) goat died on the public land. Hindus and Muslims were shocked by the incident, they suspected the tree on the plot was cursed. Rumour was rife in the village that the old banyan tree was the cause of the animal’s death. To prevent the recurrence of such incidents the vicinity of the tree was declared inaccessible to all human beings, their cattle and other domestic pets. But at one time children used to play hide-and-seek around that banyan tree on the public land, on scorching hot days cowherds dozed in its soothing shade, peasants working in the fields cooled their perspiring bodies under its leafy branches while eating the lunch brought by their wives or daughters. But in a few days time the cursed tree claimed its second victim, the carpenter Tulsi’s old mother(Dilara Zaman) who went gathering dry leaves under it. Tulsi’s beautiful wife Durgarani(Sohana Saba) told everyone that tree was indeed the killer of her mother-in-law.

The newly qualified doctor Abir Ahmed at the village health centre examined the dead woman and said that she had died of a heart attack.
The Hindus refused to accept his verdict. Overnight the tree turned into a deity. Illiterate and superstitious Hindus began to worship the tree from a safe distance, the over-zealous among them even dared to decorate the trunk and branches of the tree with red pennants. The Brahmins began to proclaim that the tree deity could only be appeased by performing puja under it and offering human sacrifice.

The time of Durga Puja, the biggest Hindu festival, drew near. Muslims too prepared to rejoice in the festivities of their Hindu neighbours. The image of goddess Durga was being constructed in the temple. Arati – ritualistic worship with oil lamps and burning cense accompanied by loud ringing of bells and beating of gongs – was performed every evening in the temple till late, although worshippers were careful not to begin it until the Muslims had ended their namaz. This was a familiar daily scenario in Mohanpur.

Mohanpur has a small marketplace consisting of only a handful of shops. Protul Nag is the night watchman, appointed by Gour Biswas, the head of the temple committee. It is said that he had murdered his wife. Protul patrols the market area every night, shouting “Beware, all honest people!” although his own honesty is subject to question. One night a thief broke into a shop and tried to escape carrying a sackful of rice. But he ran into Protul and dropping the sack ran off to hide under the banyan tree on the public land. There were heavy rains and thunderstorm that night and the next morning the thief was found lying stone dead under the tree. After examining the corpse the qualified doctor Abir Ahmed said the man had been murdered.

The Hindus refused to accept the doctor’s opinion. They said: “The deity is demanding human sacrifice. Such incidents will not stop until the deity’s demand is met.” The veteran homeopath Araj Ali asked: “If the man has indeed been killed by that tree, why is there a vicious bruise on his head?” But no one paid any heed to his question.

Tulsi was dumbfounded with grief at his mother’s death. He had the lurking suspicion that the members of the temple committee might have 
persuaded his wife Durga to poison the goat. Were they responsible for his mother’s death as well? His mother and Durga had never been on very good terms with each other, so he could not help suspecting his wife. So he withdrew into a shell and simply stopped talking. Meanwhile, a clandestine relationship developed between Durga and the new doctor Abir. This gave rise to covert whisperings in the village.

The homeopath Araj Ali repeatedly sent applications to the district town praying for the interference of the authorities to resolve this situation. One day a Police Inspector arrived to make enquiries into the questions involving the banyan tree. Both Hindus and Muslims gathered at the spot. The Inspector carefully examined the tree. Araj Ali’s letters had already appraised the Inspector of the dirty politics behind the incidents taking place in the village. An elderly Hindu villager came forward and said: “Inspector Sir, this tree is not a mere tree, it’s a deity. It’s demanding more blood.” The Inspector replied: “So you’re advocating human sacrifice here! You want a temple where you can worship. But was there ever a temple at this spot?” An old Muslim gentleman shouted: “There has never been a temple here. Why is it necessary to preserve this tree? Let’s cut it down!” These heated arguments created considerable excitement among the crowd and led to skirmishes. Finally, the police managed to disperse the crowd by wielding sticks and the Inspector even had to fire a few rounds. The police force left without taking a decision.

The next day a bagful of money reached the Inspector. The authorities shook their hands off the matter. And the mosque committee discovered that their agitation was about to fizzle out since their community had suffered no loss of life or property. Meanwhile, the temple committee arranged to guard the tree lest someone tried to destroy it. In spite of that one night some men wearing caps managed to tear off the red ribbons hung from its branches. Coming to know of this Araj Ali rushed there and re-fastened the red ribbons,
two village communities.

The same night the dead body of Araj Ali was found at the bottom of the tree. The fingers of one of his lifeless hands was clutching a cap. The mosque committee placed the blame of Araj Ali’s death squarely on the temple committee while the latter accused the former of the same crime. The two sides began taking out processions in turn protesting against this heinous act and demanding justice.

Apprehending that the situation was quickly going out of control, the doctor Abir Ahmed petitioned the higher authorities. The government responded with strict orders that neither side was to erect any kind of structure in the vicinity of the disputed tree. Hindus and Muslims then joined hands to drive the Doctor Abir out of the village. They chose Tulsi’s beautiful wife Durga as the instrument for making their plan work.

Their plan was successful. Answering the coquettish call of Durga, the doctor came to see her behind her house at the dead of night. He was caught red-handed by vigilant villagers. The same night both Hindus and Muslims gathered in the courtyard of Haji Sahib where the doctor was put on trial. Their verdict was that Doctor Abir must leave the village by the first light of dawn. Progressive ideology was thus defeated by dirty politics.

But will this politics of religious dogmatism, of vested interests ever come to an end? Or is it to continue as long as the human race lasts? But that banyan tree is still standing on that same public land, and politics is still being played around it.

O Brihonnola Tree, may you survive for ever, because you too have the right to life.

== Cast ==

* Ferdous Ahmed as Doctor Abir Ali
* Sohana Saba as Durga rani
* Azad Abul Kalam as Aroz Ali
* Intekhab Dinar  as Tulsi
* Dilara Zaman as Tulsis Mother
* Jhuna Choudhury as Imam
* K.S. Firoz as Head of Mosque comity
* Abdullah Rana as Head of Temple comity
* Kohinur as Protul Nag
* Uttam Guho as Subodh
* Enamul Huq as Rahim
* Shamim Al Mahmun as Police Inspector
* Khalekujjaman as Monsur Ali
* Manosh Bondopaddhaya as Shoshibhushon
* Ana Putul as Shikha 
* S M Mohasin as Nokul Biswas
* MD Faruk as Thakur
* Prithuraj as Arun
* Mukitul Kabir 
* Monjur Hossain as Modon
* Mahmood Alam as thief 
* Robiul Mahmood Young as Najmul Master
* Mohsin Shamim as Jamsed
* Shahana Shumi as thiefs wife
* Shahadat Hossain Sagaor as Government Officer

== Production ==

=== Director Note===

Noted Iranian film director Asgar Farhadi remarked on one occasion that raising questions had become more important in the contemporary world than giving or seeking answers.
While making my film Brihonnola, I too often pondered over this – Which is more important? The Question or the Answer? So many questions keep rising every day, most of which have no answer. Perhaps there is no need for that either.
I too have tried to raise a few questions in Brihonnola and have also tried to provide a simple solution to them, a solution that I felt would be appropriate.
The story of Brihonnola is a very familiar one. Sometimes it is like old wine in new bottle, sometimes the reverse – new wine in old bottle.
Grabbing a tree, grabbing land, grabbing the sky, air, water, forests, even human faith, religion, labour have become a great game of usurpation, continuing unabated from the beginning of creation.
But is this grabbing game to continue for ever?
Again a question.
Brihonnola poses a thousand questions – regarding religions, regarding artificial barriers imposed between human beings. And the answer? There is none yet.
Who is the real hero – the demigod Rama, or the demon Ravana? Lakshman Sen, or Ikhtiar Uddin Muhammad bin Bakhtiar Khilji?
In my childhood I used listen to the tale of Bognshi-Tonghsi told by my father. Once a village headman was going to the mosque to perform the early morning namaz. On the way he was grabbed by two ghosts who carried him off to their lair. They wanted the headman to resolve a huge dispute that had erupted there.
The dispute was over an iron rod and the ghosts, divided into two opposing sides, were engaged in a bitter quarrel to establish its true 
nature. One side claimed that the rod was a Bongshi while the other side declared that it was nothing but a Tongshi.
The headman was in a big dilemma. He was at a loss to decide which side 

he should take because taking any side would mean putting his life at risk from the other side. Finally he hit upon a clever plan. He took the rod in his hands, pretended to examine it carefully from one end to the other and then gave his verdict: It is not a Bongshi, neither is it a Tongshi. It is only a rod made of iron. The ghosts were highly pleased with this verdict because it meant that neither side was wrong. They profusely thanked the headman and carried him back to the mosque just as the enchanting sound of azaan was floating out in the morning air.

=== Casting ===

=== Filming ===

==Soundtrack==



===Track listing===

{{track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| music_credits = yes
| total_length =  
| title1 = Megh Esheche, Rod Hesheche
| extra1 = Kona, Rituraj Sen
| lyrics1 = Kobir Bokul
| music1 = Emon Saha
| length1 = 4:34
| title2 = Ontor Amar Pure Ongar
| extra2 = Rituraj sen
| lyrics2 = Kobir Bokul
| music2 = Emon Saha
| length2 = 4:31
| title3 = Priyo Tomar Kisher Oviman
| extra3 = Debolina Sur
| lyrics3 = Debolina Sur
| music3 = Emon Saha
| length3 = 3:24
| title4 = Megh Esechhe, Rod Hesheche 
| extra4 = Kona
| lyrics4 = Kobir Bokul
| music4 = Emon Saha
| length4 = 4:34
| title5 = Megh Esechhe, Rod Hesheche 
| extra5 = Rituraj Sen
| lyrics4 = Kobir Bokul
| music4 = Emon Saha
| length4 = 4:34

}}

==References==
 

==External links==
*  