Footloose (2011 film)
{{Infobox film
| name           = Footloose
| image          = Footloose2011Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Craig Brewer
| producer       = {{plainlist|
* Craig Zadan
* Neil Meron
* Dylan Sellers
* Brad Weston
}}
| screenplay     = {{plainlist|
* Ronny Brewer
* Dean Pitchford
}}
| story          = Dean Pitchford
| based on       =  
| starring       = {{plainlist|
* Kenny Wormald
* Julianne Hough
* Andie MacDowell
* Dennis Quaid
}} 
| music          = {{plainlist|
* Deborah Lurie
* Blake Shelton
}}
| cinematography = Amy Vincent
| editing        = Billy Fox
| studio         = {{plainlist|
* Spyglass Entertainment
* MTV Films
* Dylan Sellers
* Zadan/Meron
* Weston Pictures
}}
| distributor    = Paramount Pictures
| released       =  
| runtime        = 113 minutes 
| country        = United States 
| language       = English
| budget         = $24&nbsp;million 
| gross          = $63.5 million  
}} 1984 film of the same name and stars Kenny Wormald, Julianne Hough, Andie MacDowell, and Dennis Quaid. The film follows a young man who moves from Boston to a small southern town and protests the towns ban against dancing.

Filming took place from September to November 2010 in Georgia (U.S. state)|Georgia. It was released in Australia and New Zealand on October 6, 2011, and in North America on October 14, 2011. It grossed $15.5&nbsp;million in its opening weekend and $63&nbsp;million worldwide. It was met with generally positive reaction from critics.

==Plot==
After a long night of partying, Bobby Moore and four of his friends drive over a bridge and, not paying attention, crash into a truck. They are all killed instantly. Bobbys father, Shaw Moore (Dennis Quaid), the reverend of the church of the small Southern United States town of Bomont, Georgia, persuades the city council to pass numerous paternalistic laws, including a ban on all unsupervised dancing and music within the city limits as well as a by-law curfew. The city council agrees, prohibiting the music and the dancing. 

Three years later, Ren McCormack (Kenny Wormald), a teenager raised in Boston, moves to Bomont to live with his uncle, aunt, and cousins after his mothers painful death from leukemia. Soon after arriving, Ren makes friends with Willard Hewitt (Miles Teller), a fellow senior at Bomont High School, and learns from him about the ban on dancing and music. He soon begins to become attracted to Moores rebellious daughter Ariel (Julianne Hough), who is dating dirt-track driver Chuck Cranston (Patrick Flueger), whose father owns the local race-track. After an insult from Chuck, Ren ends up in a game involving buses, and despite his inability to drive a bus, he wins. Reverend Moore distrusts Ren, preventing Ariel from ever seeing him again, even blaming Ariels rebellious attitude on him. Ren and his classmates want to do away with the law and have a senior prom.

After a while, Ariel begins to fall in love with Ren and breaks up with Chuck, telling him shes sick of him treating her like dirt. In result, he insults her by calling her a slut and knocks her down when she tries to hit him. He tries to drive away, but she starts to demolish his truck with a pipe, resulting in Chuck assaulting her, bruising her face. After hearing of Ariels beaten condition, Moore meets up with Ariel and his wife, Vi (Andie MacDowell), at the church and immediately thinks Ren had assaulted his daughter. When he declares he wants Ren in handcuffs, Ariel tells him that he cant blame everything on Ren just like he did with Bobby, who died in the car crash. She goes on to say how Bobby spent his entire life trying to make him proud, such as getting good grades and always going to church every Sunday, but he was never good enough for him, because he made one mistake. Now no one remembers the good things about Bobby, only the accident and ban on music and dancing his death caused. After she bitterly reveals to not being a virgin, Moore begs for her to not talk like that in church, prompting Ariel to sarcastically ask if Moore will pass another law, and how it didnt stop her and Chuck from having sex. Enraged, Moore slaps her across the face in retaliation, prompting Ariel to angrily and tearfully say, "Well, lets go get that guy that blackened my eye, cause we dont hit girls in Bomont, do we, Daddy?" and angrily storm out of the church. Moore tries to apologize, but Vi goes after Ariel telling Moore, "Youve done enough." Vi is supportive of the movement to allow dancing and explains to Moore he cannot be everyones father, and that he is hardly being a good father to Ariel. She also says that dancing and music are not the problem.

Ren goes before the city council and reads several Bible verses, given to him by Ariel, that describe how in ancient times people would dance to rejoice, exercise, celebrate, and/or worship, hoping to lift the dancing ban. Meanwhile, Ren also teaches Willard how to dance. The city council votes against him. Seemingly beaten, Ren returns to daily life. One day while working he is approached by his boss Andy who hatches a plan to have the dance in the city of Basin instead. When Ren responds that he wants the dance in Bomont, Andy informs him that his cotton mill is just outside Bomonts city limits. Once the plans are set, Ren goes to see Moore knowing that Moore still has enough influence to pressure the parents not to let their teenagers come. Ren tells Moore that even though they denied the motion to dismiss the law, they cannot stop the teenagers from having the first senior prom, which has always been denied. He then asks him respectfully if he can take Ariel. Moore, after some thought, lets Ariel go and makes amends with his wife and daughter, better understanding things. 

On Sunday, Shaw asks his congregation to pray for the high school students putting on the prom. Not long after Ren and Ariel arrive at the prom, Chuck and several of his friends ride up, intent on beating up Ren and Willard for an earlier fight in the movie. However, Ren and Willard fend them off along with Rusty and Ariels help. Ren then flings some confetti into a shredding machine and yells, "Lets dance!" The movie ends with everyone dancing in the barn to the song from the opening credits, "Footloose (song)|Footloose".

==Cast==
 
* Kenny Wormald as Ren McCormack
* Julianne Hough as Ariel Moore Reverend Shaw Moore
* Andie MacDowell as Vi Moore
* Miles Teller as Willard Hewitt
* SerDarius Blain as Woody
* Ziah Colon as Rusty Patrick John Flueger as Chuck Cranston Ray McKinnon as Uncle Wes Warnicker
* Kim Dickens as Aunt Lulu Warnicker
 

==Production==
===Development===
In October 2008, Kenny Ortega was announced as director but left the project a year later after differences with Paramount and the production budget.       Peter Sollett was also hired to write the script. Dylan Sellers, Neil Meron and Craig Zadan served as producer; Zadan having produced the original Footloose (1984 film)|Footloose.  In 2010, Craig Brewer came on to re-write the script after Crawford and Ortega left the project and also served as director.  The writer of the original film, Dean Pitchford, also co-wrote the screenplay.  Amy Vincent served as cinematographer. 

===Casting=== Thomas Dekker was a "top candidate" for the role but on June 22, 2010,  Entertainment Weekly reported that Kenny Wormald had secured the lead role as McCormack. 
 Dancing with the Stars ballroom-dance professional Julianne Hough was cast as Ariel, Dennis Quaid as Reverend Shaw Moore, and Miles Teller as Willard Hewitt.  On August 24, 2010, Andie MacDowell joined the cast as Quaids wife.    During an interview on The Howard Stern Show, Kevin Bacon said he declined a cameo appearance in the film as he did not like the role.  The role was playing Ren McCormacks deadbeat dad. Though Bacon passed on the role, he gave Brewer his blessing. 

===Filming=== wrapped two Senoia on October 1.   
 Franklin also in October.  The home and church seen in the film were filmed in downtown Acworth, Georgia|Acworth. Production used the sanctuary of the Acworth Presbyterian Church and the house of the Mayor, Tommy Allegood. 

===Music=== original films Southern grit Bang Your heavy metal band Quiet Riot and "Lets Hear It for the Boy (song)|Lets Hear It for the Boy" by Deniece Williams.

{{Track listing
| collapsed       = yes
| extra_column = Artist
| total_length = 44:14
| writing_credits = yes Footloose
| writer1         = Kenny Loggins Dean Pitchford
| extra1          = Blake Shelton
| length1         = 3:39
| title2          = Where The River Goes (Grammy-nominated)
| writer2         = Zac Brown  Wyatt Durrette Drew Pearson Anne Preven
| extra2          = Zac Brown
| length2         = 3:39
| title3          = Little Lovin
| writer3         = Elisabeth Marius Angelo Petraglia
| extra3          = Lissie
| length3         = 4:30
| title4          = Holding Out for a Hero
| writer4         = Dean Pitchford Jim Steinman
| extra4          = Ella Mae Bowen
| length4         = 5:21
| title5          = Lets Hear It for the Boy (song)|Lets Hear It for the Boy Tom Snow
| extra5          = Jana Kramer
| length5         = 4:10
| title6          = So Sorry Mama
| writer6         = Whitney Duncan Gordie Sampson John Shanks
| extra6          = Whitney Duncan
| length6         = 3:43
| title7          = Fake I.D. John Rich John Shanks
| extra7          = Big & Rich  
| length7         = 3:21
| title8          = Almost Paradise
| writer8         = Eric Carmen Dean Pitchford
| extra8          = Victoria Justice and Hunter Hayes
| length8         = 3:37
| title9          = Walkin Blues 
| writer9         = R.L. Burnside
| extra9          = Cee Lo Green  
| length9         = 3:48
| title10          = Magic in My Home
| writer10         = Jason Freeman
| extra10          = Jason Freeman   
| length10         = 3:13
| title11          = Suicide Eyes
| writer11         = Michael Hobby Jaren Johnston William Satcher
| extra11          = A Thousand Horses
| length11         = 3:00
| title12          = Dance the Night Away
| writer12         = Lavell Crump Christopher Goodman Dean Pitchford Rhashida Stafford Bill Wolfer
| extra12          = David Banner
| length12         = 4:13

}}

====Chart performance====
{| class="wikitable"
! Chart (2011)
! Peak position
|- Australian ARIA Albums Chart   ARIA Report 1151. Retrieved 2012-05-19  56
|- Billboard |accessdate=October 6, 2011}} 
| align="center"| 21
|-
| US Billboard 200 
| align="center"| 14
|-
| US Billboard Top Country Albums 
| align="center"| 4
|-
| US Billboard Top Soundtracks 
| align="center"| 1
|}

==Release and promotion==
The film was originally scheduled for release in North America on April 1, 2011, but was moved to October 14, 2011.   Footloose was released in Australia and New Zealand on October 6, 2011.   
 HSN partnered for a 24-hour promotion on October 12, 2011. They sold clothing inspired by the film, such as womens red boots, denim, footwear and nail polish brands created by Vince Camuto and Steve Madden.  To promote the film, Paramount sent the cast and director on a promotional tour in over a dozen cities.   
 Dancing with the Stars. The episode featured film stars Kenny Wormald and Julianne Hough—a former champion on the show—dancing to the songs "Holding Out for a Hero" and "Footloose (song)|Footloose" from the films soundtrack with Blake Shelton performing the song live. {{cite web|url=http://www.tvguide.com/tvshows/dancing-with-the-stars-2011/episode-11-season-13/dancing-with-the-stars/191470|title=Dancing with the Stars Episode Recap: Tuesday, Oct. 11, 2011 Season 13, Episode 11 CMT advertised and promoted the film. 

==Reception==
===Box office=== Kansas City, Step Up (2006, $20.7&nbsp;million), but it performed around the same as Step Up 3D (2010, $15.8&nbsp;million) and You Got Served (2004, $16.1&nbsp;million).  The 1984 Footloose (1984 film)|Footloose opened to $20&nbsp;million when adjusted for ticket price inflation.    In its second weekend the film held well,  with a drop of 34&nbsp;percent. It placed third and grossed an estimated $10.4&nbsp;million. 
 New Zealand.  Footloose has grossed $51.1&nbsp;million in the United States and Canada, and $10.9&nbsp;million in other countries, for a worldwide total of $62&nbsp;million.    The 1984 Footloose grossed over $80&nbsp;million worldwide.  

{| class="wikitable" style="width:99%;"
|-
! rowspan="2" style="text-align:center;"| Release date (United States)
! rowspan="2" style="text-align:center;"| Budget 
! colspan="3" style="text-align:center;"| Box office revenue 
|-
! style="text-align:center;"| United States/Canada
! style="text-align:center;"| Other markets
! style="text-align:center;"| Worldwide
|- October 14, 2011
| style="text-align:center;"| $24,000,000
|$51,162,383
|$10,898,547
|$62,060,930
|}

===Critical reviews===
The film received generally positive reviews from critics. Review aggregator   infuses his Footloose remake with toe-tapping energy and manages to keep the story fresh for a new generation." Metacritic, which assigns a weighted average score out of 100 to reviews from mainstream critics, gives the film a score of 58 based on 35 reviews.  CinemaScore polls reported that the average grade moviegoers gave the film was an "A" on an A plus to F scale.  

Lisa Schwarzbaum of Entertainment Weekly gave the film an A minus. She praised the performance of Wormald, commenting that he "handily owns the role for a new audience" and closed her review saying, "Guardians of the 80s flame will approve of the productions sincere respect for the original; church still matters, and so do Ariels red cowboy boots."  Roger Ebert of the Chicago Sun-Times gave a negative review of the film, rating it  one and half stars out of four. He criticized the film for being so close to the 1984 original "sometimes song for song and word-for-word". He thought poorly of Wormalds performance, saying, "Hes got the Kevin Bacon role but not the Kevin Bacon charisma." Ebert closed his review expressing, "This new Footloose is a film without wit, humor or purpose. It sets up the town elders as old farts who hate rock n roll. Does it have a clue that the Rev. Moore and all the other city council members are young enough that they grew up on rock n roll? The films message is: A bad movie, if faithfully remade, will produce another bad movie." 

Variety (magazine)|Variety s Rob Nelson also wrote that the film failed to distinguished itself from the original and denounces Wormald and Houghs acting performances saying, "When the music stops, young Hough is saddled, like her co-star, with the impossible task of making 27-year-old verbiage sound fresh." Nelson wrote that Brewers musical staging is "subtly less theatrical than Herbert Ross|Ross, but it hardly constitutes a reinvention" and that Brewers film comes across as "slightly milder" than Ross, such as with Ariels abuse by former boyfriend being toned down for 2011.  Todd McCarthy of The Hollywood Reporter disapproved of how the dance numbers and action sequences were staged, shot and cut, saying, "The visual clumsiness does not disguise that Wormald (a professional dancer since extreme youth), especially, but the others too, are very good dancers. But the compositions vary randomly between close-ups, awkward medium shots and general coverage that cuts together with no cumulative dynamic power."  Orlando Sentinel s Roger Moore gave the film two and half out of four stars. 
 High School Step Up. For Wormalds performance he said, "He has energy but no real magnetism, and while he may be in possession of what are technically known as "moves", his dancing lacks sensuality and a sense of release." Scott gave Miles Teller a good review saying that he "has a natural charisma that is both comic and kind of sexy". He described the music in the remake as "better and more eclectic than the original, with some blues, country and vintage metal mixed in with the peppy dance tunes". 

===Home media===
Paramount Home Entertainment released Footloose on DVD and Blu-ray on March 6, 2012. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*   The Numbers

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 