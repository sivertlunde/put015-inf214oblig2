Lucky Luciano (film)
{{Infobox film
| name           = Lucky Luciano
| image	         = Lucky Luciano FilmPoster.jpeg
| caption        = 
| director       = Francesco Rosi
| writer         = Francesco Rosi Tonino Guerra et al
| narrator       = 
| starring       = Gian Maria Volonté
| music          = 
| cinematography = 
| editing        = 
| studio         = Harbor Productions Les Films de la Boétie Vides Cinematografica
| distributor    = AVCO Embassy Pictures (1974, original, USA) EMI (UK) Studio Canal (current) 
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Lucky Luciano is a 1973 American crime drama film directed by Francesco Rosi. It stars Gian Maria Volonté and Vincent Gardenia.  It was shown as part of the Cannes Classics section of the 2013 Cannes Film Festival. 

==Cast==
*Gian Maria Volonté as Lucky Luciano
*Vincent Gardenia as Colonel Charles Poletti
*Silverio Blasi as Italian Captain
*Charles Cioffi as Vito Genovese
*Larry Gates as Judge Herlands
*Karin Petersen as Igea Lissoni
*Edmond OBrien as Commissioner Harry J. Anslinger
*Charles Siragusa as himself
*Rod Steiger as Gene Giannini

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 