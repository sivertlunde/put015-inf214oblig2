Vasudha
{{Infobox film
| name = Vasudha
| image =
| image_size =
| caption =
| director = UV Babu
| producer = S N M Pictures
| writer = VPB Nair
| screenplay = Innocent Rekha Rekha Rajan P Dev Maniyanpilla Raju
| music = Perumbavoor G. Raveendranath
| cinematography =
| editing =
| studio = SNM Pictures
| distributor = SNM Pictures
| released =  
| country = India Malayalam
}}
  1992 Cinema Indian Malayalam Malayalam film, directed by UV Babu and produced by S N M Pictures. The film stars Innocent (actor)|Innocent, Rekha (South Indian actress)|Rekha, Rajan P Dev and Maniyanpilla Raju in lead roles. The film had musical score by Perumbavoor G. Raveendranath.   
 
==Cast==
   Innocent  Rekha 
*Rajan P Dev 
*Maniyanpilla Raju 
*Charuhasan 
*Kakka Ravi  Kanaka  Saikumar 
*Silk Smitha 
 

==Soundtrack==
The music was composed by Perumbavoor G. Raveendranath. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF"  
! No. !! Song !! Singers !! Lyrics !! Length (m:ss)
|- 
| 1 || Alakkozhinja Neramundo || M Jayachandran, Ranjini Menon || S Ramesan Nair || 
|- 
| 2 || Padmanabha Paahi || KS Chithra || S Ramesan Nair || 
|- 
| 3 || Thaazhampoo || KS Chithra, M Jayachandran || S Ramesan Nair || 
|- 
| 4 || Vasudhe || M Jayachandran || S Ramesan Nair || 
|- 
| 5 || Vrindaavana Geetham || KS Chithra || S Ramesan Nair || 
|}

==References==
 

==External links==
*  

 
 
 


 