Adamant (film)
{{Infobox film
| name           = Adamant
| image          = ADAMANT.png
| alt            = Adamant
| caption        = 
| director       = Giacomo Mantovani
| producer       = Giacomo Mantovani, Cristina Isoli
| writer         = Giacomo Mantovani
| music          = Michael Denny
| starring       = Fergal Philips, Rebecca Kiser, Alberto Silvestrini, David Caron, David France
| studio         = Avant-garde Pictures
| runtime        = 2 minutes
| country        = United Kingdom
| language       = English
}}
Adamant is a short film created by Giacomo Mantovani in 2011. The production was completed in 2 weeks by a cast and crew of 10 people, on location in the central London.  The short garnered its first award  at the 242 Movie TV   contest "Reason Wine",  which has been promoted by Ente Mostra Vini - Enoteca Italiana. The contests main goal was the objective to realize cinematographic short films, of max 2 minute length, to promote the culture of the wine "Made in Italy" and the "drinking responsibly" for the young generations.

==Basics==
The short film has been clearly named Adamant to better represent the Italian wine as precious and historical. In fact, the concept is well represented in the dreamlike sequence made by shapes in backlight, that characterizes the film.

==Plot==
London. A couple of young lovers marks with a glass of good Italian wine one of the most important moments of their lives.

==Cast and crew==
*Director/Writer/Producer: Giacomo Mantovani
*Co-producer: Cristina Isoli 
*Assistant Director: Omer Kula
*Cast: Fergal Philips, Rebecca Kiser, Alberto Silvestrini, David Caron, David France

== References ==
 

== External links ==
*  
*  
*  

 
 