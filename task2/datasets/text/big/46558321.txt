The Last Penny
{{Infobox film
| name = The Last Penny
| image =
| image_size =
| caption =
| director = André Cayatte
| producer = Raymond Borderie
| writer =  Louis Chavance   André Cayatte
| narrator =
| starring = Ginette Leclerc   Gilbert Gil   Fernand Charpin
| music =   Jacques Dupont   
| cinematography = Charles Bauer     
| editing =     
| studio = Continental Films 
| distributor = Films Sonores Tobis
| released = 23 January 1946 
| runtime = 90 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Last Penny (French:Le dernier sou) is a 1946 French drama film directed by André Cayatte and starring Ginette Leclerc, Gilbert Gil and Fernand Charpin. A secretary tries to save her friends company from being bankrupted by unscrupulous figures. It was one of three films Leclerc appeared in for the collaborationist Continental Films, which she believed led to her arrest by the authorities following the Libertation.  Although it was made during the Second World War the film was not released until March 1946.

==Cast==
* Jacques Berlioz as Le président du tribunal 
* Fernand Charpin as Colon  
* Guy Decomble 
* Gabrielle Fontan as Mme. Durban  
* Annie France as Jacqueline  
* Gilbert Gil as Pierre Durban  
* René Génin as Perrin  
* Ginette Leclerc as Marcelle Levasseur  
* Noël Roquevert as Stéfani  
* Zélie Yzelle

== References ==
 

== Bibliography ==
* Mayne, Judith. Le Corbeau. University of Illinois Press, 2007.

== External links ==
*  

 
 
 
 
 
 

 

 