Sthanarthi Saramma
{{Infobox film 
| name           = Sthanarthi Saramma
| image          =
| caption        =
| director       = KS Sethumadhavan
| producer       = TE Vasudevan
| writer         = Muttathu Varkey S. L. Puram Sadanandan (dialogues)
| screenplay     = KS Sethumadhavan
| starring       = Prem Nazir Sheela Adoor Bhasi Muthukulam Raghavan Pillai
| music          = LPR Varma
| cinematography =
| editing        = TR Sreenivasalu
| studio         = Jaya Maruthi
| distributor    = Jaya Maruthi
| released       =  
| country        = India Malayalam
}}
 1966 Cinema Indian Malayalam Malayalam film,  directed by KS Sethumadhavan and produced by TE Vasudevan. The film stars Prem Nazir, Sheela, Adoor Bhasi and Muthukulam Raghavan Pillai in lead roles. The film had musical score by LPR Varma.   

==Cast==
 
*Prem Nazir
*Sheela
*Adoor Bhasi
*Muthukulam Raghavan Pillai
*Sankaradi
*T. R. Omana
*Prathapachandran GK Pillai
*Joseph Chacko
*Kunjandi Meena
*Nellikode Bhaskaran
*Panjabi
*Pankajavalli
 

==Soundtrack==
The music was composed by LPR Varma and lyrics was written by Vayalar Ramavarma.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Akkarappachayile || K. J. Yesudas, P. Leela || Vayalar Ramavarma || 
|-
| 2 || Akkarappachayile(F) || S Janaki || Vayalar Ramavarma || 
|-
| 3 || Kaaveri Theerathu || Renuka || Vayalar Ramavarma || 
|-
| 4 || Kaduvappetti || Adoor Bhasi || Vayalar Ramavarma || 
|-
| 5 || Kuruvippetti || Adoor Bhasi || Vayalar Ramavarma || 
|-
| 6 || Tharivalakilukile ||  || Vayalar Ramavarma || 
|-
| 7 || Thottupoy || Chorus, Uthaman || Vayalar Ramavarma || 
|-
| 8 || Yarusalemin Naadha || P. Leela, Chorus || Vayalar Ramavarma || 
|-
| 9 || Zindabaad Zindabaad || Adoor Bhasi || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 


 