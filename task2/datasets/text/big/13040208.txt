Journey to Lasta
 
Journey to Lasta is a 2004 dramatic film written and directed by Wondwossen D. Dikran.

{{Infobox Film
| name           = Journey to Lasta
| image          =
| image_size     = 
| caption        = Movie poster for Journey to Lasta

| Associate Producer = Milena Gashaw
| director       = Wondwossen D. Dikran
| producer       = Jake Deptula
| writer         = Wondwossen D. Dikran
| starring       = Tsegaye B. Selassie 

Kirubel Assefa 

Teferi Assefa     

Johnny Ashenafi      

Jessica Beshir  
    
Tsehaie Kidane      

Abbey Lessanu 

Scott F. Evans 

Ras Michael
| music          = Lasta Sound, Gizzae, Wayna, Upground, Quinto Sol, Marcus I, Chakra, Admas
| cinematography = Yasu Tanida
| editing        = Gabrielle Thorbourn
| Produced By	 = Reel Image Pictures
| Executive Producer = Nebiyou Essayas
| distributor    = Vanguard Cinema
| released       = October 29, 2004
| runtime        = 125 min.
| country        = United States
| language       = English/Amharic
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

== Plot ==
Inspired by a true story, the journey begins when three childhood friends from Ethiopia reunite at various stages in their mundane lives as struggling musicians. Together, they embark upon a musical mission to bring modern Ethiopian music to the world. Hardships, triumphs, and rude awakenings ultimately test their faith, friendship, and honor while struggling to make their music a success. Filled with pulsating rhythms, and a colorful cast of characters, Journey to Lasta brings an inspiring story of the "underdog" and vividly paints an honest and vibrant portrayal of the immigrant experience in America.

==Cast==
*Tsegaye B. Selassie - Tsegaye
*Kirubel Assefa - Kirubel
*Teferi Assefa - Teferi
*Johnny Ashenafi - Binyam Mekuria
*Jessica Beshir - Selam
*Tsehaie Kidane - Addis
*Abbey Lessanu - Mimi
*Scott F. Evans - Omar Gibson
*Ras Michael - as Himself

==External links==
*  

 
 
 
 
 
 

 
 