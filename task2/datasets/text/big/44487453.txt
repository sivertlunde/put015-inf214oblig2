Feng Shui 2
{{Infobox film
| name = Feng Shui 2
| image =  
| caption = Theatrical Movie Poster
| director = Chito Roño Kristina Bernadette C. Aquino
| writer = Roy C. Iglesias Chito S. Roño
| starring = Kris Aquino   Coco Martin   Cherry Pie Picache    Carmi Martin    Ian Veneracion    Joonee Gamboa
| music = 
| cinematography = 
| editing = 
| distributor = Star Cinema
| studio = Star Cinema   K Productions
| released =  
| runtime = 1 hour and 40 minutes
| country = Philippines
| language = Filipino language|Filipino, English
| budget = Estimate ₱20 million
| gross =  ₱235.0 million
}}
 first film in 2004. Kris Aquino reprise her role as Joy Ramirez and Coco Martin as Lester Anonuevo, the new owner of the cursed bagua. The Film co-stars Cherry Pie Picache, Carmi Martin, Ian Veneracion, and Joonee Gamboa. The film focuses on Lester (Martin) who, upon getting the cursed bagua, starts to get all the luck and prosperity he could get in his life, but with deadly consequences. This film is Star Cinemas official entry to the 2014 Metro Manila Film Festival.   

It is also the first Filipino movie to be rendered in 4D and it will be exclusively be shown at the XD Theater of SM Mall of Asia under a partnership of SM Lifestyle Entertainment and Star Cinema. 

==Plot==
 first film Feng Shui, the cursed bagua went into possession of Lester (Martin), a jack-of-all-trades. He is willing to do every single job offered to him, even dirty and illegal works. But after the arrival of the cursed bagua in his life, everything is about to change. But soon, he realizes that all the luck he is getting has deadly consequences.
On the other hand, the previous owners of the bagua are being drawn together by the curse. Its as if their luck has back fired against them.
Can Joy (Aquino) save them from the deadly curse of Lotus Feet , even if the curse has spawned another force – Lotus Feets niece – who is also set to take more souls and start a whole new cycle of terror?

==Cast==
*Kris Aquino as Joy Ramirez
*Coco Martin as Lester Anonuevo
*Cherry Pie Picache as Lily Mendoza
*Carmi Martin as Ruby
*Ian Veneracion as Douglas
*Beauty Gonzalez as Ellen
*Rez Cortez as Robert
*Ian De Leon as Jack
*Martin Escudero as Moy
*Joonee Gamboa as Hsui Liao
*Raikko Mateo as Mio
*Rosita Lim as Mei Lien/Lotus Feet
*Elizabeth Chua
*Jenine Desiderio
*Diana Zubiri
*Manuel Chua

==Production==
The idea of a sequel was created after Coco Martin approached Director Chito Rono and Kris Aquino about the first film. Being a fan himself of the original movie in 2004, Coco Martin asked Aquino and Rono if they would be interested on a sequel, and that he would be glad to join them. Rono and Aquino decided that after ten years, it was the right time for the sequel.

The shooting of the film was set to commence by late August of this year. With the first cut of the film, Joys character (Kris Aquino) was not present for most of the time in the story. Because of this, the producers asked them to do re-shoots to give her additional screen time.
The first official trailer of the film was attached to the Star Cinema romantic comedy film Past Tense, which was released on November 22. 

==Reception==
The film was met with high expectations by fans since the first movie was a cult classic, considered as the scariest Filipino horror film ever.
The film received mixed to positive response from critics, citing the outstanding performances of Coco Martin and Cherry Pie Picache but lacking the originality of the first movie. They also applauded Kris Aquinos performances, saying it was a great improvement from her past horror films.
Audiences received the film more positively, citing it as a worthy sequel to the original hit.

==Awards and Nominations==

! Year
! Organization
! Category
! Result

|-
| 2014
| 40th Metro Manila Film Festival || Best Festival Actor (Coco Martin) ||  
|}

==Box-office==
The film became the second-highest grossing film in the recently launched 2014 Metro Manila Film Festival. It now holds the title as the highest opening gross for a Filipino horror film in history.

The film has earned a total of ₱235.0 million at the box-office.

==See also==
*List of ghost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 