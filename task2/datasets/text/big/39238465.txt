Furious 7
 
 
 
{{Infobox film
| name           = Furious 7
| image          = Furious 7 poster.jpg
| caption        = Theatrical release poster
| director       = James Wan
| producer       = {{Plain list |
* Neal H. Moritz
* Vin Diesel
* Michael Fottrell
}} Chris Morgan
| based on       =  
| starring       = {{Plain list |
* Vin Diesel
* Paul Walker
* Dwayne Johnson
* Michelle Rodriguez
* Jordana Brewster
* Tyrese Gibson Chris "Ludacris" Bridges
* Kurt Russell
* Jason Statham
 
}} Brian Tyler
| cinematography = {{Plain list |
* Stephen F. Windon
* Marc Spicer
}}
| editing        = {{Plain list |
* Christian Wagner
* Leigh Folsim Boyd
* Dylan Highsmith
* Kirk M. Morri
}}
| studio         = {{Plain list |
* Original Film
* One Race Films
* Media Rights Capital China Film Group
}} Universal Pictures
| released       =  
| runtime        = 137 minutes 
| country        = United States
| language       = English
| budget         = $190 million         
| gross          = $1.429 billion   
}}
 action film Chris Morgan. Chris "Ludacris" Bridges, Kurt Russell, and Jason Statham. Furious 7 follows Dominic Toretto (Diesel), Brian OConner (Walker), and the rest of their team, who have returned to the United States to live normal lives after securing amnesties for their past crimes in Fast & Furious 6 (2013), until Deckard Shaw (Statham), a rogue special forces assassin seeking to avenge his comatose younger brother, puts them in danger once again.

With the previous three installments being set between   (2006), Furious 7 is the first film in the franchise to take place after Tokyo Drift. The film also marks the final film appearance of Walker, who died in a single-vehicle accident on November 30, 2013, with filming only half-completed. After Walkers death, filming was delayed for script rewrites and his brothers, Caleb and Cody, were used as stand-ins to complete his remaining scenes.
 3D internationally, fourth highest-grossing film of all time. An eighth film is scheduled to be released on April 14, 2017.

==Plot==
  DSS office to extract profiles of Doms crew. After revealing his identity, Shaw engages Hobbs in a fight, and escapes when he detonates a bomb that severely injures Hobbs. Dom later learns from his sister Mia that she is pregnant again and convinces her to tell Brian. However, a bomb, disguised in a package sent from Tokyo, explodes and destroys the Toretto house just seconds after Han, a member of their team, is killed by Shaw in Tokyo. Dom later visits Hobbs in hospital, where he learns that Shaw is a rogue special forces assassin seeking to avenge his brother. Dom then travels to Tokyo to claim Hans body, where he meets and races Sean Boswell, a friend of Hans, who gives him personal items found at Hans crash site.

Back at Hans funeral in Los Angeles, Dom notices a car observing and chases after the vehicle, driven by Shaw. Both prepare to fight, but Shaw slips away when a covert ops team arrives, led by Frank Petty, who refers to himself as "Mr Nobody". Petty says that he will assist Dom in stopping Shaw if he helps him obtain the Gods Eye, a computer program that uses digital devices to track specific people, and save its creator, a hacker named Ramsey, from a mercenary named Jakande. Dom, Brian, Letty, Roman Pearce and Tej Parker then airdrop their cars over the Caucasus Mountains, ambush Jakandes convoy and rescue Ramsey. The team then heads to Abu Dhabi, where a billionaire has acquired the flash drive containing the Gods Eye, and manages to steal it. With the Gods Eye, the team manages to track down Shaw, who is waiting at a remote factory. Dom, Brian, Petty and his team attempt to capture Shaw, but are ambushed by Jakande and his militants, and they are forced to flee while Jakande obtains the Gods Eye. At his own request, Petty is then left to be evacuated by helicopter. Left with no other choice, the team decides to return to Los Angeles to fight Shaw, Jakande and his men on their home turf.
 stealth attack helicopter and unmanned aerial vehicle|drone, Ramsey attempts to hack into the Gods Eye while sharing her mobile between their vehicles. Hobbs, seeing the team in trouble, breaks out of hospital and destroys the drone. Ramsey then successfully completes the hack, regains control of the Gods Eye and shuts it down. Meanwhile, Dom and Shaw engage in a one-on-one brawl on a parking garage, before Jakande intervenes and attacks them both, and Shaw is apprehended when part of the parking garage collapses on him. Dom then launches his vehicle at Jakandes helicopter with him in it, successfully managing to toss a bag of grenades on board, before injuring himself when his car lands and crashes. Hobbs then shoots the bag of grenades from ground level, destroying the helicopter and killing Jakande. When Dom remains unconscious, the team fears that he is dead. As Letty cradles Doms body in her arms, she reveals that she has regained her memories, and remembers their wedding. Dom regains consciousness soon after, remarking, "Its about time".

Later, Shaw is taken into custody by Hobbs and locked away in a secret, high-security prison. Meanwhile, at a beach, Brian and Mia play with their son while Dom, Letty, Roman and Tej observe, appreciating their happiness and acknowledging that Brian is better off retired with his family. Dom silently leaves, and Brian catches up to him at a stop sign. As Dom remembers the times that he has had with Brian, they bid each other farewell and drive off in separate directions.

==Cast==
 
 
* Vin Diesel as Dominic Toretto, a former criminal and professional street racer, who has retired and settled down with his wife, Letty.
 FBI agent-turned-criminal and professional street racer. He is married to Doms sister, Mia, with whom he has a son, Jack.
 DSS agent who allied with Dom and his team after their outings in Europe and Rio de Janeiro. It is later revealed that he is also a father, to a young girl. Johnson initially said that if Universal pursued the accelerated development of a seventh film with a summer start date, he would be unable to participate due to scheduling conflicts with filming on Hercules (2014 film)|Hercules.  However, as production for the film commenced in September, he confirmed his return for the film, as Hercules completed production in time for him to film a significant part. 
 Fast & Furious.

* Jordana Brewster as Mia Toretto-OConner, Doms younger sister and a former member of his team. She is married to Brian, with whom she has a son, Jack.

* Tyrese Gibson as Roman Pearce, a member of Doms team and a childhood friend of Brian.
 Chris "Ludacris" Bridges as Tej Parker, a mechanic and technician, and a friend of Brian and Romans from Miami.

* Kurt Russell as Frank Petty    (also known as "Mr Nobody"), the leader of a covert ops team who agrees to help Dom stop Shaw if he can help him prevent a mercenary from obtaining a computer program called the Gods Eye.

* Jason Statham as Deckard Shaw, a rogue special forces assassin seeking to avenge his comatose younger brother after his demise at the hands of Dom and his team in Spain.

* Nathalie Emmanuel as Megan Ramsey, a British computer hacker and the creator of the Gods Eye, who allies with Dom and his team after being saved from a mercenary and helps them to regain control of her program.

* Djimon Hounsou as Mose Jakande, a mercenary who allies with Shaw and uses the Gods Eye to track its creator and use her to track down his enemies.

* Tony Jaa as Kiet, a member of Jakandes team who possesses great agility, athleticism and fighting prowess. Thai martial arts actor Jaa was confirmed to have joined the cast in August 2013, making his Hollywood debut.    
 UFC championship rematch against Miesha Tate. Her participation in the film was similar to that of Gina Carano making the transition from mixed martial arts fighting to acting, following Caranos involvement in Fast & Furious 6. 

* Lucas Black as Sean Boswell, a professional street racer and drifter based in Tokyo, who meets Dom when he travels to Tokyo to claim the body of Han Seoul-Oh, a mutual friend of theirs killed by Shaw. In September, it was confirmed that Black had signed on to reprise his role as Boswell for Furious 7 and two more installments.      

* Elsa Pataky as Elena Neves, a DSS agent and former Rio police officer, who moved to the United States to become Hobbs new partner at the DSS.

* Ali Fazal as Zafar, a friend of Ramsey to whom she sent the Gods Eye for safekeeping. It is Fazals first appearance in an American film. He described his role as a cameo. {{cite web|url=http://gulfnews.com/life-style/celebrity/desi-news/bollywood/ali-fazal-on-his-furious-7-cameo-1.1483016|title=Ali Fazal on his ‘Furious 7’ cameo|author=Manjusha Radhakrishnan,
Senior Reporter|work=gulfnews.com}} 

* John Brotherton as Sheppard, Pettys right-hand man. Brotherton was also used as an acting double for Walker following his death.   
 Bachata singer Romeo Santos also makes a cameo appearance. 

==Production==

===Development===
On October 21, 2011, the Los Angeles Times reported that Universal was considering filming two sequels—  (2006).  On December 20, 2011, Diesel stated that Fast Six would be split into two parts, with writing for the two films occurring simultaneously. On the decision, Diesel said: 

 We have to pay off this story, we have to service all of these character relationships, and when we started mapping all that out it just went beyond 110 pages&nbsp;...&nbsp;The studio said, You cant fit all that story in one damn movie!  

However, in an interview on February 15, 2012, actor Dwayne Johnson stated that the two intended sequels would no longer be filmed simultaneously because of weather issues in filming locations, and that production on Fast Seven would only begin after the completion of Fast Six. 

In April the following year, during completion of post-production on the retitled Fast & Furious 6, director Justin Lin announced that he would not return to direct a seventh film, as the studio wanted to produce the film on an accelerated schedule for release in summer 2014. This would have required Lin to begin pre-production on the sequel while performing post-production on Fast & Furious 6, which he considered would affect the quality of the final product. Despite the usual two-year gap between the previous installments, Universal chose to pursue a sequel quicker due to having fewer reliable franchises than its competitor studios.  However, subsequent interviews with Lin have suggested that the sixth installment was always intended to be the final entry directed by him. 
 Chris Morgan returning to write the script, his fifth in the series. On April 16, 2013, Vin Diesel announced that the sequel would be released on July 11, 2014.  In May 2013, Diesel said that the sequel would feature Los Angeles, Tokyo and the Middle East as locations.  

===Filming===
 
Principal photography began in early September 2013 in Atlanta, Georgia (U.S. state)|Georgia, with a casting call issued.   Abu Dhabi was also a filming location,  as the production crew chose it over Dubai; they benefitted from the Emirates 30% rebate scheme.    Pikes Peak Highway in Colorado was closed in September to film some driving sequences. 
 Oakland Cemetery,  with extras needed for the scene being "hot, hip and trendy cool types of all ethnicities between the ages of 18 and 45".  On the evening of the 19th, Lucas Black joined the production  for his sole scene with Vin Diesel, in an Atlanta parking garage; separate scenes with Paul Walker also shot in the same location on the same night,  including one half of a phone conversation between his character and Jordana Brewsters. The day after, Diesel posted a picture from the night shoot with Black on his public Facebook page. 

On October 24, over a month into the films production, Dwayne Johnson tweeted he had started shooting for the film after wrapping up on Hercules (2014 film)|Hercules.  Five days later, Vin Diesel posted the first photo of Johnson on the set, in the hospital scene. 

On November 30, 2013, while on a break for the Thanksgiving holiday, Walker, who portrayed Brian OConner, died in a single-vehicle accident.   The next day, Universal announced that production would continue after a delay that would allow the filmmakers to rework the film.  On December 4, 2013, Universal Pictures put production on hold indefinitely.  Wan later confirmed that the film had not been cancelled.    On December 22, 2013, Diesel posted on his Facebook page that the film would be released on April 10, 2015.    On February 27, 2014, The Hollywood Reporter reported that filming would resume on April 1, and that the cast and crew had headed to Atlanta to prepare for about eight more weeks of shooting.  Principal photography ended on July 10, 2014.   

===Stunts===
   was used in the film to carry the vehicles that would drop from 12,000 feet high, above the Sonoran Desert, making cars plummet at a speed of about 130 to 140 miles per hour.  ]]
 CGI because dry run BRS parachutes green screen to reproduce their tumble through the sky.    The last part of the scene, which shows the cars hitting the road was shot separately. To get that right, the team set up a pully system that had cars six to ten feet above the ground. When they were dropped from the cranes, the stuntmen who were sitting in the drivers seats raced their engines at about 35 to 40 miles per hour and slid to the ground at full speed. Those cranes were then later removed from the film with computers.   Razatos admits that the air drop sequence was "all real" and that it would be "hard to top."  
 Georgia which provides tax breaks for film productions, and then theyd add woods in the background later in post production to which Razatos denied saying, "the audience is going to know   and arent going to feel good about it."  Shooting finally took place in Colorado. 

A total of 340 cars were used in the film,  and more than 230 cars were destroyed in the making of the film including several black Mercedes-Benz, a Ford Crown Victoria and a Mitsubishi Montero.    The mountain-highway chase scene on Colorados Monarch Pass proved to be the most damaging sequence with over 40 vehicles being destroyed.   Only 10 percent of the action sequences in the film were computer-generated, and even then, much of the CGI was employed simply to erase the wires and other contraptions that were used to film real cars and drivers or to add a background.  It took more than 3,500 Man-hour|man-days to complete the various stunts of the film.  For safety reasons, stunt coordinator, Joel Kramer said that he doesnt let his drivers go above 50 miles per hour. 

===Redevelopment of Walkers character===
 
 Daily News The Mill, noted that the redevelopment was similar to that of Oliver Reeds when his character had to be redeveloped the same way as a result of his death during filming of Gladiator (2000 film)|Gladiator. 

==Music==
  musical score Brian Tyler, who scored the third, fourth, and fifth installments of the series.    "Theres an emotional component to Fast & Furious 7 that is unique," said Tyler about his experience scoring. "I think people are really going to be amazed by it."  A soundtrack album to the film was released by Atlantic Records on March 17, 2015. 

Songs featured in the film include: Go Hard or Go Home" (Wiz Khalifa & Iggy Azalea)  Ride Out" YG & Rich Homie Quan)  See You Again" (Wiz Khalifa and Charlie Puth)
* "My Angel" (Prince Royce) Get Low" (Dillon Francis and DJ Snake)
* "Ay Vamos" (J Balvin feat. Nicky Jam and French Montana)

==Release==
The film, which began principal photography in September 2013, was originally designed as a Summer  2014 release. It was put on hold following the fatal car crash that claimed Paul Walkers life on November 30, 2013. The production resumed in April 2014.

In October 2014, Universal revealed that the film was officially titled Furious 7,  and that the debut trailer would be released during an interactive fan event over social media. In the days leading up to the event, seven-second, behind-the-scenes videos were released, titled "7 Seconds of 7".    On February 1, 2015, a new trailer featuring all-new footage debuted during Super Bowl XLIX.

The film was originally scheduled for release on April 10, 2015, but it was announced that the films release date had been brought forward a week to April 3, 2015. The official announcement in change of date was made in July 2014.  Fast and Furious 7 premiered at the   on March 16, 2015.    It was release in China on April 12, 2015. On March 27, 2015, a free standalone expansion for the video game Forza Horizon 2, titled Forza Horizon 2 Presents Fast & Furious, was released to help promote the film. 

===Piracy=== pricing concerns, soaring internet costs and Censorship in India|censorship. 

==Reception==

===Box office===
  in  , which opened at Scotiabank Theatre in Toronto in December 2014. ]]
 4th highest-grossing film of all-time,  the 2015 in film|highest-grossing film of 2015,  the highest-grossing film in The Fast and the Furious|The Fast and the Furious franchise (achieving the milestone in just twelve days),   the highest-grossing Universal Pictures film,  and the twentieth film in cinematic history to cross the $1 billion mark.
 Universal Pictures Jurassic Park). The Avengers, Avatar (2009 film)|Avatar and Harry Potter and the Deathly Hallows – Part 2 (all 19 days). 

====United States and Canada==== thirty fifth highest-grossing film,  the List of 2015 box office number-one films in the United States|highest-grossing 2015 film, 
and the fourth highest-grossing film distributed by Universal.  Predictions for the opening weekend of Furious 7 in the United States and Canada were continuously revised upwards, starting from $115 million to $150 million.   It opened on Friday, April 3, 2015, across 4,004 theaters, including 365 IMAX theaters, which made it Universals widest opening release ever (breaking   ($56.8 million) and Iron Man 3 ($53 million).  {{cite web|url=http://www.forbes.com/sites/scottmendelson/2015/05/02/box-office-avengers-2-assembles-mere-84-46m-friday/|title=
Avengers 2 Box Office: Age Of Ultron Assembles Mere $84M Friday|author=Scott Mendelson|work= ), the second biggest pre-summer opening ever, behind  ). {{cite web|url=http://www.forbes.com/sites/scottmendelson/2015/04/05/box-office-furious-7-races-to-384m-worldwide-bow/|title=
Box Office: Furious 7 Races To Near-Record $384M Worldwide Bow|author=Scott Mendelson|publisher=Forbes|date=April 5, 2015|accessdate=April 6, 2015}}  It is Universals fastest film to reach the $200 million mark, doing so in eight days (a record previously held by Despicable Me 2 with 11 days).  
 The Hunger Games in March 2012. 

====Other markets====
Outside North America, the film became the third highest-grossing film,   
the highest-grossing Universal distributed film,  and the highest-grossing 2015 film.  On April 26, 2015, it became the third film in cinematic history to earn over $1 billion overseas, following   ($260.4 million), in all which it reached first place at the box office        {{cite web|url=http://www.boxofficemojo.com/intl/weekend/opening/|title=OVERSEAS TOTAL
ALL TIME OPENINGS|work=  in its fourth weekend.   

The film was a massive box office hit in China. It opened there on April 12 and set an all-time midnight run record with $8.05 million (breaking   s $3.38–$3.5 million record)   and an opening day record with $68.8 million. {{cite web|url=http://deadline.com/2015/04/international-box-office-furious-7-china-record-longest-ride-insurgent-paul-blart-1201408563/|title=‘Furious 7′ Box Office Hits $801.5M Global; ‘Paul Blart 2′, ‘Longest Ride’ New Overseas second highest-grossing film ever in China, behind Transformers 4. {{cite web|url=http://deadline.com/2015/04/international-box-office-furious-7-child-44-cinderella-dragonball-z-1201412508/|title=‘Furious 7′ Drives To $1.12B Global To Bust Records in China; ‘Dragon Ball’ Trumps ‘F7′ In Japan – Intl B.O. Update
|author=Nancy Tartaglione and Anita Busch|work=Deadline.com|publisher=(Penske Media Corporation)|date=April 20, 2015|accessdate=April 21, 2015}}   {{cite web|url=http://www.boxofficemojo.com/intl/china/opening/|title=CHINA
ALL TIME OPENINGS|publisher=Box Office Mojo|accessdate=April 29, 2015}}  In just 15 days, it surpassed Transformers 4 to become the highest-grossing film in China with $323 million,  surpassing its gross in Canada and the United States  and became the first (and only) film in China to make more than 2 billion renminbi.  While many other American films that reaped similar high grosses in China have included elements designed to appeal to the Chinese audience, Furious 7 did not. Its success has been credited to China Film Group Corporation, the state-owned film distributor, which had invested considerably in the film, reportedly taking a 10% stake.   

The largest openings outside North America and China occurred in Mexico ($21.5 million), the UK, Ireland and Malta ($18.7 million), Germany ($15.9 million), Russia and the CIS ($15.9 million), Brazil ($11.4 million), France ($11.4 million), Australia ($11.3 million), Taiwan ($10.3 million), Argentina ($9.3 millon), Korea ($8.9 million), Italy ($8.2 million), Malaysia ($7.3 million), Spain ($6.3 million), Venezuela ($6 million), Thailand ($6 million), and Colombia ($5.2 million).   In the UAE, where parts of the film was shot, it opened with $4.8 million.  Its $11.7 million opening in India is the biggest for a Hollywood title ever (previously held by   ($7.6 million).  and   ($7.4 million).  It became the highest-grossing film of all time in Argentina, Colombia, Ecuador, Indonesia, Malaysia, South Africa, the UAE, Uruguay, Trinidad and Vietnam and Universal Pictures highest-grossing film of all time in 29 countries including Argentina, China, Ecuador, Egypt, India, Indonesia, Lebanon, Malaysia, Mexico, Paraguay, Peru, Thailand, Turkey, UAE and Vietnam.   In total earnings, the largest countries after the U.S. and Canada are China ($375 million), the UK, Ireland and Malta ($54.3 million), Mexico ($50.5 million), Brazil ($43 million), Germany ($37.4 million) and Russia and the CIS ($32.2 million).   

===Critical response===
The Los Angeles Times reported that reviews for Furious 7 have been "generally positive" with critics praising the films action sequences and its poignant tribute to Walker.  The review aggregator website Rotten Tomatoes reported an 82% approval rating, based on 194 reviews, with an average rating of 6.7/10. The sites consensus reads, "Serving up a fresh round of over-the-top thrills while adding unexpected dramatic heft, Furious 7 keeps the franchise moving in more ways than one."  On Metacritic, which assigns a normalized rating, the film has a score of 67 out of 100, based on 44 critics, indicating "generally favorable reviews".  In CinemaScore polls conducted during the opening weekend, cinema audiences gave Furious 7 an average grade of A on an A+ to F scale. 
 2015 SXSW Film Festival on March 16, 2015. Ramin Setoodeh of Variety (magazine)|Variety noted that fans started lining up outside four hours before the film was scheduled to start. The film closed with a tribute to Walker, which left many in the theater "holding back tears".  Critic Dave Palmer gave the film 7/10, saying, "Furious 7 is the type of movie Michael Bay has spent his entire career trying to make: filled with shots of scantily clad women, fast cars, and clever one liners". 

A.O. Scott of The New York Times gave the film two and a half stars out of five and said, "Furious 7 extends its predecessors’ inclusive, stereotype-resistant ethic. Compared to almost any other large-scale, big-studio enterprise, the Furious brand practices a slick, no-big-deal multiculturalism, and nods to both feminism and domestic traditionalism. 

John DeFore of The Hollywood Reporter criticized the film however, describing it as "stupidly diverting", saying the running time was "overinflated"; he compared watching the film to a morbid game, in addition to criticizing the screenplay. 

==Sequel==
Regarding a possible sequel, Vin Diesel said, "I was trying to keep it close to the vest throughout the release. Paul Walker used to say that   was guaranteed. And in some ways, when your brother guarantees something, you sometimes feel like you have to make sure it comes to pass... so if fate has it, then you’ll get this when you hear about it.   was for Paul,   is from Paul." 

Diesel further hinted at an eighth film on Jimmy Kimmel Live! when he stated that Kurt Russells character had been introduced in a role that would span multiple films. He also stated that the possible sequel could take place in New York.  Neither a director nor a writer have been finalized for an eighth film. Neal H. Moritz later stated, "  is going to have to be something enticing for all of us... it has to be as good as or better  ."  At the 2015 CinemaCon in Las Vegas, Vin Diesel announced the film for an April 14, 2017 release date.  

==See also==
* List of films featuring drones
* List of films featuring surveillance

==References==
{{Reflist|30em|refs=
   

   

   

   

   

   

   

   

   

   

   
}}

==External links==
* 
* 
* 
* 	
* 	
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 