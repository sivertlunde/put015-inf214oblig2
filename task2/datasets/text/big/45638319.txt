Avathara Purushan
{{Infobox film
| name           = Avathara Purushan
| image          =
| image_size     =
| caption        =
| director       = Pandiyan Arivali
| producer       = A. Jagannanthan Kasthuri Ananth
| writer         = Pandiyan Arivali
| starring       =  
| music          = Sirpy
| cinematography = Sekar V. Joseph
| editing        = Gowri &nbsp;— Parry
| distributor    =
| studio         = Jagannathan Productions
| released       =  
| runtime        = 120 minutes
| country        = India
| language       = Tamil
}}
 1996 Tamil Tamil drama Anand and Sivaranjani in Deva and was released on 28 June 1996.   

==Plot==

Anand (Anand (actor)|Anand) and Vaishali (Sivaranjani (actress)|Sivaranjani) are college students. Anand falls in love with Vaishali at first sight. When Anand reveals her his love, Vaishali rejects it and she ridicules him. Since that day, he follows her everywhere to tease her. She then complains to the police. Soon, Vaishali becomes pregnant and hides the fathers name to her parents. Feeling ashamed about her pregnancy, her family moves to Ooty.

Vaishali finally reveals to her parents that she was raped by a stranger. After giving birth, she drops the baby in front of the rapists house. Anand (Ranjith (actor)|Ranjith) didnt understand anything. He was also a student and was the other Anands worst ennemy. Anand (Ranjith) was arrested mistakenly and beaten by the police for teasing Vaishali.

Anand (Ranjith) takes care of the baby. Anand (Ranjith) thinks that the rapist is Anand (Anand). Both Anand fight among themselves but they turn out to be innocents in this affair. In the meantime, Vaishali befriends with Raja (Veerapandiyan). What transpires later forms the crux of the story.

==Cast==

  Ranjith as Anand Anand as Anand Sivaranjani as Vaishali
*Goundamani as pickpocket Periyasamy Senthil
*Vivek Vivek
*Veerapandiyan as Raja
*Thalaivasal Vijay as Vaishalis father
*Kavitha as Vaishalis mother
*Ganeshkar
*Dhamu
*Mayilsamy
*Idichapuli Selvaraj
*Karuppu Subbaiah
*Tirupur Ramasamy
*Jayamani as Karuppu
*Chelladurai
*Joker Thulasi
*Kovai Senthil
*Bonda Mani
*K. B. Kumar
*Mathi
*Master Vijay
*Baby Raani
*Yogeshwari
*Susila
*Nirmala
*Eashwari
*Anupama
*Indhu
 

==Soundtrack==

{{Infobox album |  
| Name        = Avathara Purushan
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack
| Length      = 21:03
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1996, features 5 tracks with lyrics written by the director Vaali (poet)|Vaali.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Mano || 4:05
|- 2 || Chinamanai Chinamani || Mano || 4:23
|- 3 || Agaya Panthalile || S. P. Balasubrahmanyam, S. P. Sailaja || 3:18
|- 4 || Mathithala || Swarnalatha || 4:19
|- 5 || Unnai Ninachi || S. P. Balasubrahmanyam, K. S. Chithra || 4:58
|}

==References==
 

 
 
 
 
 