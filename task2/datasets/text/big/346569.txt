The Discreet Charm of the Bourgeoisie
{{Infobox film
| name           = The Discreet Charm of the Bourgeoisie
| image          = Discreet-charm-poster.jpg
| caption        = Theatrical release poster
| director       = Luis Buñuel
| producer       = Serge Silberman
| writer         = {{plainlist|
*Luis Buñuel
*Jean-Claude Carrière
}}
| starring       = {{plainlist|
*Fernando Rey
*Paul Frankeur
*Delphine Seyrig
*Stéphane Audran
*Bulle Ogier
*Jean-Pierre Cassel
}}
| music          = Edmond Richard
| editing        = Hélène Plemiannikov
| distributor    = 20th Century Fox
| released       =  
| runtime        = 102 minutes
| country        = {{plainlist|
*France
*Italy
*Spain
}}
| language       = French
| budget         = $800,000
}} surrealist film directed by Luis Buñuel  and written by Jean-Claude Carrière in collaboration with the director.    The film was made in France and is mainly in French, with some dialogue in Spanish.
 Best Original Screenplay.   

==Plot== terrorist from the fictitious Republic of Miranda. The films world is not logical: the bizarre events are accepted by the characters, even if they are impossible or contradictory. 

The film begins with a bourgeois couple, the Thévenots (Frankeur and Seyrig), accompanying M. Thévenots colleague Rafael Acosta (Rey) and Mme. Thévenots sister Florence (Ogier), to the house of the Sénéchals, the hosts of a dinner party. Once they arrive, Alice Sénéchal (Audran) is surprised to see them and explains that she expected them the following evening and has no dinner prepared. The would-be guests invite Mme Sénéchal to join them for dinner at a nearby inn. Finally arriving at the inn, the party find it locked. They knock and are invited in, despite the waitress seeming reluctance and an ominous mention of "new management". Inside, there are no diners (despite disconcertingly cheap prices) and the sound of wailing voices from an adjoining room. It is learned that the manager died a few hours earlier and his former employees are holding vigil over his corpse, awaiting the coroner. The party hurriedly leave. 

Two days later, the bourgeois friends attempt to have lunch at the Sénéchals, but he (Cassel) and his wife escape to the garden to have sex instead of joining them. One of the bourgeois friends takes this as a sign that perhaps the Sénéchals are aware the police are coming (fearing the discovery of the mens involvement in cocaine trafficking) and were leaving to avoid arrest. The party leaves again in panic.

Then the women visit a tea house, which turns out to have run out of all beverages—tea, coffee, milk, and herbal tea, although it finally turns out that they do have water. While they are waiting, a soldier tells them about his childhood and how, after the death of his mother, his education was taken over by his cold-hearted father. The soldiers mother (as a ghost) informs him that the man is not his real father, but in fact killed the soldiers father during a duel over his mother. Following his ghost mothers request, the soldier poisons and kills the culprit.

When the Sénéchals return from their garden after sneaking off to make love, their friends are gone but they meet a  bishop who had arrived shortly after.  He greets them in their gardeners clothing, and they angrily throw him out.  When he returns in his bishops robes, they embrace him with deference, exposing their prejudice, snobbery, and hypocrisy.  The bishop asks to work for them as their gardener.  He explains to them about his childhood—about how his parents were murdered by arsenic poisoning, and the culprit was never apprehended. Later on in the film, he goes to bless a dying man, but when it turns out that the man had killed the bishops parents, he first blesses him, then fires a shotgun, killing the man—thus closing the circle of hypocrisy.

Various other aborted dinners ensue, with interruptions including the arrival of a group of French army officers who join the dinner, or the revelation that a French colonels dining room is in fact a stage set in a theatrical performance, during a dream sequence. Ghosts make frequent appearances in what seemed to be disconcerting dream sequences.  

Buñuel plays tricks on his characters, luring them toward fine dinners that they expect, and then repeatedly frustrating them in inventive ways.  They bristle, and politely express their outrage, but they never stop trying;  they relentlessly expect and pursue all that they desire, as though it were their natural right to have others serve and pamper them.  He exposes their sense of entitlement, their hypocrisy, and their corruption.  In the dream sequences, he explores their intense fears—not just of public humiliation, but of being caught by police, and mowed down by guns.  At least one characters dream sequence is later revealed to be nested, or embedded, in another characters dream sequence.  As the dreams-within-dreams unfold, it appears that Buñuel is also playing tricks on his audience, as we try to make sense of the story.  

A recurring scene throughout the film, wherein the six people are walking silently and purposefully on a long, isolated country road toward a mysterious destination, is also in the final sequence.

==Cast==
{| 
| 

* Fernando Rey as Rafael Acosta, ambassador of the Republic of Miranda
* Paul Frankeur as M. Thévenot
* Delphine Seyrig as Simone Thévenot
* Bulle Ogier as Florence, Mme. Thévenots sister
* Stéphane Audran as Alice Sénéchal
* Jean-Pierre Cassel as Henri Sénéchal
* Michel Piccoli as Minister
* Georges Douking as Gardener
* Milena Vukotic as Ines, a maid of the Sénéchals
* Maria Gabriella Maione as Guerilla
|
* Claude Piéplu as Colonel
* Marguerite Muni as Peasant woman
* Pierre Maguelon as Brigadier sanglant Commissaire Delecluze
* Julien Bertheau as Mgr Dufour, the bishop Muni as Peasant
* Pierre Maguelon as Police Sergeant
* Bernard Musson as Waiter in the tea room
* Robert Le Béal as Tailor
|}

==Production==

===Pre-production===
After having announced that Tristana would be his last film 
because he felt he was repeating himself, Buñuel met with screenwriter Jean-Claude Carrière and discussed the topic of repetition. Shortly afterwards he met with film producer Serge Silberman, who told him an anecdote about having forgotten about a dinner party and being surprised to find six hungry friends show up at his front door. Buñuel was suddenly inspired and Silberman agreed to give him a $2,000 advance to write a new script with Carrière, combining Silbermans anecdote with the idea of repetition. Buñuel and Carrière wrote the first draft in three weeks and finished the fifth draft by the Summer of 1971, with the title originally being Bourgeois Enchantment. Silberman was finally able to raise the money for the film in April 1972 and Buñuel began pre-production.  Wakeman, pp. 88-89. 

Buñuel cast many actors whom he had worked with in the past, such as Fernando Rey and Michel Piccoli, and catered their roles to their personalities. He had more difficulty casting the female leads and allowed actresses Delphine Seyrig and Stéphane Audran to choose which parts they would like to play, before changing the script to better suit the actresses. Jean-Pierre Cassel auditioned for his role and was surprised when Buñuel cast him after simply glancing at him once. 

===Filming and editing===
Filming began on May 15, 1972, and lasted for two months with an $800,000 budget. In his usual shooting style, Buñuel shot few takes and often edited the film in camera and during production. Buñuel and Silberman had a long running and humorous argument as to whether Buñuel took one day or one and a half days to edit his films. Baxter, p. 301.  On the advice of Silberman, Buñuel used video playback monitors on the set for the first time in his career, resulting in a vastly different style than any of his previous films, including zooms and travelling shots instead of his usual close-ups and static camera framing.  It also resulted in Buñuel being more comfortable on set, and in limiting his already minimal direction to technical and physical instructions. This frustrated Cassel, who had never worked with Buñuel before, until Rey explained that this was Buñuels usual style and that since they were playing aristocrats their movements and physical appearance were more important than their inner motivation. Buñuel once joked that whenever he needed an extra scene he simply filmed one of his own dreams. The Discreet Charm of the Bourgeoisie includes three of Buñuels recurring dreams: a dream of being on stage and forgetting his lines, a dream of meeting his dead cousin in the street and following him into a house full of cobwebs, and a dream of waking up to see his dead parents staring at him. 

==Reception==
The film was a box office hit in both Europe and the US, and critically praised.  Robert Benayoun said that it was "perhaps   most direct and most public film". Wakeman, p. 89.  Vincent Canby wrote in his 1972 review of the film, “In addition to being extraordinarily funny and perfectly acted, The Discreet Charm moves with the breathtaking speed and self-assurance that only a man of Buñuel’s experience can achieve without resorting to awkward ellipsis.”  Buñuel later said that he was disappointed with the analysis that most film critics made of the film.  He also disliked the films promotional poster, depicting a pair of lips with legs and a derby hat. 

Buñuel and Silberman travelled to the US in late 1972 to promote the film. Buñuel did not attend his own press screening in Los Angeles and told a reporter at Newsweek that his favorite characters in the film were the cockroaches. While visiting LA, Buñuel, Carrière and Silberman were invited to a lunch party by Buñuels old friend George Cukor, and other guests included Alfred Hitchcock, Billy Wilder, George Stevens, Rouben Mamoulian, John Ford, William Wyler, Robert Mulligan and Robert Wise Baxter, p. 302.  (resulting in a famous photograph of the directors together, other than an ailing Ford). Fritz Lang was unable to attend, but Buñuel visited him the following day and received an autographed photo from Lang, one of his favorite directors. 

==Awards==
Sensing that he had a special film, Silberman decided not to wait until May to premiere it at the Cannes Film Festival and instead released it in the fall of 1972 specifically to make it eligible for the Academy Award for Best Foreign Language Film. Buñuel was famously indifferent to awards and jokingly told a reporter that he had already paid $25,000 in order to win the Oscar. The Discreet Charm of the Bourgeoisie did win the Oscar for Best Foreign Language Film and Silberman accepted on Buñuels behalf at the ceremony. At the Academys request, Buñuel posed for a photograph while holding the Oscar, but while wearing a wig and oversized sunglasses. 

==Adaptations== The Exterminating Angel.   

==See also==
* Bourgeois personality
* List of submissions to the 45th Academy Awards for Best Foreign Language Film
* List of French submissions for the Academy Award for Best Foreign Language Film

==References==
{{reflist|refs=

   

}}

==Further reading==
* 

==External links==
*  
*  
*   at Rotten Tomatoes
*  
* Roger Eberts review of  
* 

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 