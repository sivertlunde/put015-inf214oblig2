Looking Forward (1910 film)
 
{{Infobox film
| name           = Looking Forward
| image          = Looking Forward 1910.jpg
| caption        = A film still showing the marriage of Jack and the female mayor
| director       =
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = Motion Picture Distributing and Sales Company
| released       =  
| runtime        =
| country        = United States English inter-titles
}}
 silent short short drama produced by the Thanhouser Company. Adapted from James Oliver Curwoods short story of the same name, the film follows a young chemist named Jack Goodwin. He discovers a chemical compound that puts a person into a state of sleep for a determined period of time and decides to test it upon himself. The first test is a success and Jack makes arrangements for his sleep of a hundred years, in a state similar to suspended animation. When he awakes in 2010 into a world ruled by women, he woos the female mayor. Jack joins a society to campaign for mens rights. The society ends up before the female mayor who jails all of them, save for Jack who she proposes to. Jack accepts on the condition that men are given back their rights and she accepts. The cast and production credits of the film are not known, but Theodore Marston was not the director. The film was released on December 20, 1910. The film is presumed lost film|lost.

== Plot ==
The film focuses on Jack Goodwin, young chemistry student who has discovered a novel compound that allows a person to be put in a state of sleep for any length of time. The compound has the effect of causing a person to enter similar to suspended animation, with no ill-effects or bodily changes. Jack tests the compound on himself for a period of a week and desires to test it for a century. In preparation for this, he obtains a safety deposit vault and provides instructions to be presented before the mayor in in a hundred years. Jacks experiment works and he awakes in the year 2010 to a very different future. Transportation is has been radically changed to pneumatic tubes allowing him to be transported to the mayor. The woman mayor takes an interest win him and invites her over to her home. In the intervening years, the world has become ruled by women and Jack is now out of place, but the two fall in love. Her father is an advocate for mens rights and Jack joins there society. The group soon appears before the mayor and she sends all of them to jail through the pneumatic tube, except for Jack. She proposes to him, but Jack only consents if the rights of men would be granted. The mayor is true to her word and signs a decree to give the men their liberty. During the ceremony, she attempts to lead Jack to the altar, but Jack shows her that the man must lead. When the bridal veil is places on Jack, he gives places it on his new wife. 

== Cast ==
*Frank H. Crane  William Russell 

== Production ==
The scenario is adapted from James Oliver Curwoods short story of the same name.  The writer of the scenario is unknown, but it was most likely Lloyd Lonergan. He was an experienced newspaperman employed by The New York Evening World while writing scripts for the Thanhouser productions.  The film director is unknown, but it may have been Barry ONeil or Lucius J. Henderson. Sometimes the directional credit is given to Theodore Marston. The apparent origin of this error is from the American Film-Index 1908–1915. Film historian Q. David Bowers consulted one of the co-authors of the book, Gunnar Lundquist, and confirmed that the credit of Marston was in error.    Theodore Marston worked with Pathé, Kinemacolor, Vitagraph and other companies, but there is no record of Marston working with Thanhouser.  This error has persisted in several works including The Complete Index to Literary Sources in Film.    Cameramen employed by the company during this era included Blair Smith, Carl Louis Gregory, and Alfred H. Moses, Jr. though none are specifically credited.  The role of the cameraman was uncredited in 1910 productions.   
 George Middleton, Grace Moore, John W. Noble, Anna Rosemond, Mrs. George Walters. 

Musical accompaniment for the silent films were not provided by the studios, and the Thanhouser productions were no exception. The musical program for the screenings were decided and played by the individual accompanists. At times, musical accompaniments were shared in trade journals, but for Looking Forward a dispute serves to provide one musical credit provided by Mrs. Buttery of Pennsylvania. In responding to an editorial in The Moving Picture World, Mrs. Buttery stated that "John Took Me Home to See His Mother" was played during film at some unstated point. 

==Release and reception ==
The single reel comedy, approximately 1,000 feet long, was released on December 20, 1910.    The New York Dramatic Mirror stated, "Here is a rather exaggerated farce with a good many laughs in it. It has for its basis recent cartoons, but has original treatment. The male members of the cast seemed to enjoy their roles, perhaps for the reason that it is a mans picture; the laugh seems on the ladies."  A more contemporary analysis of the film was covered in a paper by Eric Dewberry, who credits this film as Thanhousers first film portraying suffragists.     }} Dewberry writes that the film presents the idea of a woman mayor, something which was not farfetched for women to find to be humorous. Despite Jacks lack of power in the new era which he awakes to, he managed to win the heart of a powerful woman and easily restore the rights of men. Taken one way, the film seems to advance the idea that a women of power are controlled by their hearts. Dewing writes, "On the one hand it mocks many female suffrage "fighting tactics," as suffragists, in an attempt to not appear too extremist and alienable, exploited conservative ideas of feminine virtue in order to assert their citizenship and reform desires. In the film, the man assumes effeminate passions to woo the Mayor. On the other hand, those sympathetic to the cause can see this as proof that these tactics can work in politics. The film also toys with the fears men harbored concerning the loss of power over females in public spaces, a threat to masculinity and manhood."  The film is also believed to have been the first adaptation of Curwoods work. 

==Notes==
 

== References ==
 

 
 
 
 
 
 
 
 