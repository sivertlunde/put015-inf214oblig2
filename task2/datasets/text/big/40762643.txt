Maggie (film)
{{Infobox film
| name           = Maggie
| image          = Maggie (film) POSTER.jpg
| alt            = A older man and his zombie daughter stand in front of a stormy sky
| caption        = 
| director       = Henry Hobson
| producer       = {{plainlist|
* Colin Bates
* Joey Tufaro
* Matthew Baer
* Trevor Kaufman
* Arnold Schwarzenegger
}}
| writer         = John Scott 3
| starring       = {{plainlist|
* Arnold Schwarzenegger
* Abigail Breslin
* Joely Richardson
}}
| music          =  David Wingo
| cinematography = Lukas Ettlin
| editing        = Jane Rizzo
| studio         = {{plainlist|
* Grindstone Entertainment Group
* Gold Star Films
* Silver Lining Media Group
* Inferno Entertainment
* Lotus Entertainment
* Silver Reel
}}
| distributor    = {{plainlist|
* Roadside Attractions
* Lionsgate Films
* Lotus Entertainment
}}
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $4.5 million 
| gross          = 
}} independent horror film  directed by Henry Hobson, written by John Scott 3, and starring Arnold Schwarzenegger, Abigail Breslin and Joely Richardson. The film is a dramatic departure for Schwarzenegger, who is more known for his action hero roles.
 VOD release on May 8, 2015.

==Plot==
A teenage girl in the Midwest becomes infected by an outbreak of a disease that slowly turns the infected into cannibalistic zombies.  During her transformation, her loving father stays by her side.

==Cast==
* Arnold Schwarzenegger as Wade Vogel
* Abigail Breslin as Maggie 
* Joely Richardson as Caroline
* Aiden Flowers as Bobby
* Carsen Flowers as Molly
* J.D. Evermore as Holt
* Raeden Greer as Allie
* Ashley Hudson as Frightened Woman
* Rachel Whitman Groves as Bonnie

==Production== Black List of "most liked scripts".  Chloë Grace Moretz was initially attached to the project but dropped out due to scheduling conflicts.   Filming started on September 23, 2013  in New Orleans, Louisiana. 

==Reception==
Maggie received mixed to positive reviews, with critics praising Schwarzeneggers and Breslins performances.      

Christopher Bourne of Twitch Film, reviewing from the 2015 Tribeca Film Festival, states that Schwarzenegger "delivers one of his finest and most effective performances here, the most dialed back and restrained hes ever been. He brings surprising nuance and depth to his role as a father who desperately fights to preserve his last moments with his daughter, who suffers considerable agony at his inability to reverse or ameliorate Maggies inevitable decline", and adds that his dramatic turn "suggests his capability for a wider range of roles as he approaches his seventies and is less able to take on the physical demands of the action hero roles that he has been known for." 

Perri Nemiroff of Collider.com, who gave the film a mixed review, notes that Schwarzenegger "won’t ever completely shake the super tough exterior, but he certainly comes across as a very relatable, loving father here", and that Breslin "gets a nice assist from the eerily natural zombie transformation makeup, but it’s her ability to move between selling Maggie as a strong young woman trying to keep it together for her family and also being downright terrified that makes the performance especially heart wrenching." However, she ultimately states that the film "is just one big missed opportunity.   Hobson’s certainly got potential, but his determination to highlight the characters’ misery through dim visuals and super sad faces winds up completely sucking the life out of the concept."  

Drew McWeeny of Hitfix.com said in his review of "Maggie": 

"For someone who has been making films as long as he has, there is a surprisingly short list of films where I would argue that Arnold Schwarzenegger gives genuinely good performances.

As a movie star, he doesnt really have to give good performances. Thats one of the things that makes being a movie star so weird. There are legitimate legendary movie stars who have never given what I would call a good performance, but who do their jobs perfectly well. Being a movie star is far more about having a particular personality that you bring to every role. Most of the most famous Schwarzenegger films, hes just playing variations on himself. Even though I adore films like "Conan The Barbarian" and the first two "Terminator" films, I think his work in them is good because the directors of those films knew exactly what they wanted out of Schwarzenegger, and they practically build the films around him.

Since his comeback after serving as the governor of California, Ive liked the choices Schwarzeneggers been making. Sure, "Escape Plan" is incredibly silly, but I think both "Sabotage" and "The Last Ride" are pretty solid action films, and theyve both made smart use of the new variable in Schwarzeneggers bag of tricks, his age. Now, with "Maggie," we finally have a real performance to judge as Arnold is stripped of pretty much everything hes ever been able to rely on in his work. The result is a close-up study of his limitations as an actor, and a genuinely sad piece of work that he handles well.

As a film, "Maggie" feels slight. Set after the rise of the "necro-ambulist" virus, the film tells the story of Wade, a father (Schwarzenegger) who finds his daughter Maggie (Abigail Breslin) after she has run away. Unfortunately, at some point, she was attacked and bitten by someone with the virus, so by the time he finds her, its too late. Shes been infected, and doctors inform Wade that she will turn at some point in the next few weeks, and when that happens, his options are limited. He can turn her in to the government quarantine, he can give her the same "cocktail" drug they administer in the quarantine, or he can find a way to kill her quick. There is no good option, and from the beginning of the film, its just a slow sad countdown to that moment where Maggie isnt Maggie anymore.

The script, written by the oddly named John Scott 3, does its best to offer up a human view of what life would be like in an age of zombies. This isnt the apocalypse. Instead, its just a long sad glimpse at a father who has an impossible choice ahead of him, and the struggle to make peace with that decision. Much of the film is carried by the close-ups from director Henry Hobson, making his jump to directing after a long career as a guy who designed main title sequences. The film feels very tiny, and intentionally so. This isnt a horror film at all, which is an odd thing to say when youre talking about a movie with zombies in it.

Breslin does solid work as a girl who has to grapple with the idea that she will not be able to enjoy the joys of adulthood, and who knows full well what her suffering is going to do to her father. Joely Richardson is fine as Wades second wife Caroline, but shes out of the movie before shes got much to do. Ultimately, this is largely a two-person movie. There are some cops who check in, some local kids who take Maggie out for her final night as a teenager, and another couple of local undead, but it all comes back to Wade and Maggie in the end. Breslin has the most work to do here, the heaviest lifting, and she finds several grace notes in the role, particularly on a last outing with her friends.

I honestly wish I liked the film more, but at this point, weve seen a whole lot of zombie movies, and while this one tries to find a new way into the idea, it doesnt do anything particularly new. Theres one scene where, after Wade kills two zombies he encounters in the woods, he is forced to deal with a surviving family member face to face and watch her grieve over them. Its a moment we never see in the horror movie version of this story. Little by little, the film keeps tightening its focus until it is simply Wade, alone in the house with his daughter, waiting for her to become something he no longer recognizes. If the entire function of the film is to create a framework in which we see Schwarzenegger cry, then, sure, mission accomplished. But "Maggie" eventually disappoints. Even back at the beginning of  the modern version of this genre, George Romero made movies that were positively laden with subtext. A film like this almost feels like the filmmakers believe they are too "good" to make a real horror film, and as a result, "Maggie" feels toothless, no matter how pronounced its taste for flesh."


On Metacritic, it has a score of 57 based on seven reviews, indicated mixed or average reviews. On Rotten Tomatoes, it has a "fresh" score of 62% based on thirteen reviews.

==See also==
* Arnold Schwarzenegger filmography

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 