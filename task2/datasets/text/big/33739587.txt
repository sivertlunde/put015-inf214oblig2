The Old Curiosity Shop (1921 film)
{{Infobox film
| name           = The Old Curiosity Shop
| image          =
| caption        =
| director       = Thomas Bentley George Pearson
| writer         = Charles Dickens (novel)   J.A. Atkinson
| starring       = Mabel Poulton William Lugg Hugh E. Wright   Pino Conti
| music          = 
| cinematography = 
| editing        = 
| studio         = Welsh-Pearson
| distributor    = Jury Films
| released       = April 1921
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British silent silent drama The Old Curiosity Shop in 1934.

==Cast==
* Mabel Poulton as Little Nell 
* William Lugg as Grandfather 
* Hugh E. Wright as Tom Codlin 
* Pino Conti as Daniel Quilp 
* Bryan Powley as Single Gentleman 
* Barry Livesey as Tom Scott 
* Cecil Bevan as Sampson Brass 
* Beatie Olna Travers as Sally Brass 
* Minnie Rayner as Mrs. Jarley 
* Dennis Harvey as Short Trotters 
* Dezma du May as Mrs. Quilp 
* Colin Craig as Dick Swiveller 
* Fairy Emlyn as Mr. Marton 
* A. Harding Steerman as Mr. Marton

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 