Queen of the Mountain
{{Infobox Film 
| name           = Queen of the Mountain 
| image          =
| caption        =  
| director       = Martha Goell Lubell 
| producer       = Martha Goell Lubell
| writer         = Sharon Mulally Carol Rosenbaum 
| music          = Sumi Tonooka 
| cinematography = Peter Brownscombe
| editing        = Sharon Mulally
| distribution   = Women Make Movies
| runtime        = 56 min.  USA
| English 
}}
 documentary about Theresa Goell, a middleaged woman who, in 1947, left her husband and son to dig beneath the Sanctuary of Nemrud Dagh. Theresa was fascinated by this shrine to King Antiochus I Theos of Commagene, which had been neglected by previous archaeologists.

Queen of the Mountain tells her story through archival footage, family photographs, oral histories, commentary from Theresas friends and her own letters. The New York Times said it offered a "strong, rich narrative with visuals to match."

==Summary==

Theresa’s career as an archaeologist was full of obstacles.  She was a woman working in a man’s field, and as a young adult she was also diagnosed with otosclerosis, which meant she would slowly go stone deaf. According to the documentary, it was Theresa’s “bull dog tenacity” that drove her to the Middle East in hopes of discovering buried treasures and ancient secrets.

“People usually walk around a waterfall—I would walk down a waterfall.  People usually walk over a bridge—I walked under a bridge,” says Theresa, describing her rambunctious childhood.

Theresa’s father expected his daughter to marry and live a respectable life as a wife and mother.  He arranged a marriage for her that she complied with, but soon after, in 1926, while studying at Cambridge, her interest in classical architecture blossomed into a love of archeology.  Unhappily married, she divorced her husband, left her son and moved to Jerusalem to study ancient ruins.  
  draftsman in the Brooklyn naval yard.  Her presence was enough to make heads turn.  “I was the only woman among 1,200 men,” she says.  There was no toilet system for woman so she had to make the long walk across the site to use the Red Cross station.  Frustrated, but with a sense of humor, Theresa says, “they thought I was causing a labor problem because every time I walked to the Red Cross station every one of the 1,200 men stopped to whistle at me.”
 classical and oriental traditions. As a result, Classicalists thought it was too oriental and Orientalists thought it too classical.

Theresa proved revolutionary in a number of ways.  She recognized the significance of ancient ruins that had been neglected, she insisted her team use modern technological advances; and, perhaps most importantly, she helped pave the way for future female archaeologists.

==Reception==
The New York Times wrote,  Tess Goell American heroine that seemed to exist only in 1930s movies, played by
Katharine Hepburn or Rosalind Russell. They were women bravely striding into what was largely
believed to be a mans world — flying planes, battling city hall, working in formerly all-male offices or
newsrooms. Goell strode into archaeology, a divorced, hearing-impaired Jewish woman amid Muslims
in southern Turkey. {{Citation
  | last =Gates
  | first =Anita
  | title = Examining the Life of Tess Goell, a Pioneering Archaeologist
  | newspaper = New York Times
  | date = March 25, 2006
  | url = http://www.wmm.com/filmcatalog/press/Queen_NYT.pdf}} }}

==Notes==
 

==See also== Jews of Iran
*Polas March
*Marions Triumph
*My Yiddish Momme McCoy

==References==
*{{cite web
  | title = Queen of the Mountain
  | publisher = Women Making Movies
  | year =2005
  | url =http://extendedplay.org/?p=56 }}

*{{Citation
  | last =Gates
  | first =Anita
  | title = Examining the Life of Tess Goell, a Pioneering Archaeologist
  | newspaper = New York Times
  | date = March 25, 2006
  | url = http://www.wmm.com/filmcatalog/press/Queen_NYT.pdf}}

==External links==
*   Women Make Movies

 

 
 
 
 
 
 
 
 