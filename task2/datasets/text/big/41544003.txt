Sole Survivor (2013 film)
{{Infobox film
| name           = Sole Survivor
| image          = 
| alt            = 
| caption        =
| director       = Ky Dickens
| producer       = Susan Aurinko  Ky Dickens Alexis Jaworski
| writer         = Ky Dickens
| narrator       =
| music          = Frank Rapp
| cinematography = Tom Clayton
| editing        = Anna Patel
| studio         = 
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $380,000
| gross          =
}}
Sole Survivor is a 2013 CNN Films documentary film by director Ky Dickens.       

==Cast of survivors==
* Bahia Bakari, Yemenia Flight 626
* Cecelia Cichan, Northwest Airlines Flight 255
* George Lamson Jr., Galaxy Airlines Flight 203
* James Polehinke, Comair Flight 5191

==See also==
* List of sole survivors of airline accidents or incidents

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 

 
 