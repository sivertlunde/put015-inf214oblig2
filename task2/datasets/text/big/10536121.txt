Someone Else (film)
{{Infobox film
| name           = Someone Else
| image          = Someone Else Poster.jpg
| size           = 
| caption        = Promotional Poster
| director       = Col Spector
| producer       = Radha Chakraborty
| writer         = Col Spector Radha Chakraborty	
| starring       = Stephen Mangan Lara Belmont Susan Lynch Chris Coghill Shaun Dingwall John Henshaw
| cinematography = Trevor Forrest	
| editing        = Matthew McKinnon	 	
| studio         = RSA Films
| distributor    = IFC Films  Soda Pictures 
| released       =  
| runtime        = 78 min
| country        = United Kingdom
| language       = English
| budget         = £160,000 
}}
Someone Else is a 2006 British Comedy-drama independent film. The film was directed by Col Spector and written by Spector along with Radha Chakraborty.   The film had its world premiere at the Edinburgh International Film Festival on 20 August 2006 and released on 7 September, 2007 in UK.    

==Plot==

Photographer David (Stephen Mangan) and his longtime girlfriend Lisa (Susan Lynch) are all set to celebrate their third year together with a whirlwind trip to Venice. Though exuberant Lisa cant wait for the date to arrive, doubtful David seems to have become somewhat hesitant about the impending vacation.

When David chats-up a loopy local named Nina (Lara Belmont) in the park, the sparks between the pair are quickly doused when David begins to feel guilty about distancing himself from Lisa. Determined to make thing right, David confesses to his brief fling and gets kicked to the curb for his noble effort. To add insult to injury, Nina admits that shes already moved on and isnt really interested in David anymore.

When a local dating agency sends Lisas photo to Davids slacker friend Matt (Christopher Coghill), David sets out to win back his embittered ex by any means necessary.

==Cast==
*Stephen Mangan as David
*Susan Lynch as Lisa
*Lara Belmont as Nina
*Chris Coghill as Matt
*Shaun Dingwall as Michael
*John Henshaw as Paul

==Production==
Mangan talking about the film said that " I think it’s a reasonably common thing for both men and women. Just as you’re on the point of settling down with someone, you do have a bit of a panic It can be scary to commit to someone and shut off all your other options. I’m sure that many people have a last minute panic, get cold feet and do something rash, only later to regret it. I don’t think the character of David is a bad person, but I think the point is that he didn’t really know what he wanted."   

Filming took place in 18 days at London, England. 

==Reception==
Someone Else received mixed reviews and holds a 50% rating on Rotten Tomatoes.  Laura Bushell of BBC Online gave the film positive review and praised the script "Someone Else is the top end of indie filmmaking, with a polished script and even shinier cinematography from Trevor Forrest, but its low-key enough not to verge onto Richard Curtis territory."  David Jenkins of Total Film gave 4 out of 5 stars and said "Performances and script are both first-rate, but what really rivets is how scarily /familiar/ it all feels, from the listless rebound romances to the forced insularity of post-breakup bachelorhood. Superb stuff."  Tim Evans of Sky Movies praised the director "Debut writer-director Col Spector draws out richly detailed performances, particularly from Mangan, and the script is bursting with all-too-familiar break-up clichés that can only have been drawn from bitter experience." 
 Time Out criticized the director and said that "Spector, whos a fluid, straightforward director, resists the crutch of music until the final scene when he goes and blows it all by playing something stupid like... a Gary Barlow song." 

==Awards==

  
 
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|-
| rowspan="2"| 2007
| Karlovy Vary International Film Festival
| Variety Critics’ Choice
| Col Spector
|      
|-
| Panorama of European Cinema in Athens
| Best European Film of the European Section
| Col Spector
|   
|}
 

==Notes== New Years Eve; Stephen Mangan portrayed David in both films while different actors played Matt, Michael and Paul.  

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 