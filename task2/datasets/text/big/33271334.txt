Vithagan
{{Infobox film
| name = Vithagan
| image =
| caption =
| director = R. Parthiban
| writer = R. Parthiban
| starring = R. Parthiban Poorna
| producer = Manickam Narayanan
| music = Joshua Sridhar
| cinematography = M. S. Prabhu Anthony
| studio = Seventh Channel Entertainment
| released =   
| runtime =
| country = India
| language = Tamil
| budget =
}}
 Tamil action film directed by R. Parthiban that features himself in the lead along with Poorna. The film, which has been in making since 2008, released on 18 November 2011 as per schedule. Vithagan, which also happens to be the 50th film of Parthipan, has him in the role of a police officer, who is brainy as well as brawny. With an innovative tagline With the gun, Vithagan has Poorna as heroine and music has been composed by Joshua Sridhar of Kadhal fame. The film opened to extremely negative reviews and bombed at the box office.

==Cast==
*R. Parthiban as Rowdhran Poorna (Shamna kasim) as Mercy
*Milind Soman
*Vincent Asokan
*L. Raja
*Raaki Parthiban (Special appearance)

==Production==
Two years after his last venture Pachchak Kuthira bombed at the box office, Parthiban commenced his next directorial titled as Vithagan in 2008, while also playing the lead role in the film - that of an intelligent assistant commissioner who is "crooked and cunning at the same time".     He added that he would have two different looks in the film.  Poorna was selected to portray the female lead as a Christian girl named Mercy.  Hindi character actor Milind Soman, who was last seen in Paiyaa was finalized for the main antagonist role.  In October 2010, reports claimed that director-producer Gautham Menon would provide his voice for Milind Soman.  Parthibans son Radhakrishnan (Raaki) would appear in a one-minute cameo role during a song sequence.   Vadivelu was supposedly considered for a comedy role in the film,  which did not materialuze eventually. While Joshua Sridhar was roped in to compose the films score, M. S. Prabhu was signed as the cinematographer and Nalini Sriram as the costume designer. A bungalow set was constructed in an 18-storey building in Chennai as well as a helipad where the climax fight sequence was to be filmed.  Parts of the film were shot in Vienna, Austria and the Czech Republic. 

==Critical Reception==
Rohit Ramachandran of nowrunning rated it 1.5/5 stating that "The experience feels like chewing on coarse, indigestible cud. 
Pavithra Srinivasan of rediff rated it 1.5/5 stating that "Vithgan is a silly movie." 

==References==
 

==External links==
* 

 

 
 
 
 