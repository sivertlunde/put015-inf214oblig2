The Countess (film)
{{Infobox film
| name           = The Countess
| image          = Countess Poster.jpg
| caption        = Theatrical release poster
| image_size     = 250px
| director       = Julie Delpy
| producer       = Andro Steinborn Christopher Tuffin Julie Delpy Matthew E. Chausse
| writer         = Julie Delpy
| starring       = Julie Delpy Daniel Brühl William Hurt Anamaria Marinca
| music          = Julie Delpy
| cinematography = Martin Ruhe Andrew Bird
| studio         = X Filme International Social Capital Films EMC Filmproduktion Fanes Film The Steel Company Tempête Sous un Crâne X-Filme Creative Pool
| distributor    = X-Verleih Bac Films
| released       =  
| runtime        = 98 min.
| country        = France Germany
| language       = English
| budget         = 
| gross          = 
}} German drama drama historical film written and directed by Julie Delpy. It stars Delpy, Daniel Brühl and William Hurt. It is based on the life of Elizabeth Báthory.

The film is the third directorial effort by Delpy, who has said of the project that "it sounds like a gothic   but its more a drama. Its more focusing on the psychology of human beings when theyre given power". 

==Plot== Hungarian general Franz Nádasdy, with whom she has three children. After Nádasdys return from the Ottoman-Hungarian Wars, he succumbs to a disease he contracted abroad and dies.
 Matthias II. Matthias consents reluctantly due to his considerable debt to the Countess. At a ball, she meets Count György Thurzós son, István, and falls in love with him. After a night together, István is forced by his father to end the relationship and marry the daughter of a wealthy merchant in Denmark. Erzsébet believes that age difference is to blame for the failure of the relationship. After an incident in which she is splashed with blood after striking a female servant, Erzsébet starts to believe that bathing in the blood of virgin girls can help her to reach eternal youth and beauty. To this end, her staff capture and brutally kill peasant girls to obtain their blood.
 aristocratic girls that the authorities begin an investigation. Count Thurzó is asked to investigate the incidents and he thus sends István, now a count himself, to visit Erzsébet. István is reluctant to believe the allegations and is seduced once more by the countess. Only when he and one of his companions discover evidence of her crime do they arrest her. During the trial, Erzsébet is found guilty and, due to her noble origin, she is sentenced to spend the rest of her life walled into her room in Čachtice Castle in total isolation. Erzsébets staff is also found guilty, but unlike her they are put to death. All of her estate is awarded to the Count Thurzó with the exception of Čachtice, which is given to her children.

Driven by desperation after being walled in, Erzsébet Báthory commits suicide. She is then buried without a coffin in a humble grave, with no funeral ceremony. The film casts doubt on the sentence, suggesting that much of the happenings have been manipulated by Count Thurzó.

==Cast==
* Julie Delpy as Countess Erzsébet Báthory
* William Hurt as György Thurzó
* Daniel Brühl as István Thurzó
* Adriana Altaras as Aunt Klara Báthory
* Charly Hübner as Ferenc Nadasdy
* Anamaria Marinca as the witch Anna Darvulia
*   as Dominic Vizakna
*   as Miklos
* Rolf Kanies as Count Krajevo
* Jesse Inman as King Matthias
*   as Anna Báthory
* Frederick Lau as Janos
* Henriette Confurius as Kayla

==Release==
The film premiered on   at the 59th Berlin International Film Festival and was shown at the Cannes Film Festival 2010. 

==See also==
* Elizabeth Báthory in popular culture

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 