Rio 2
 
{{Infobox film name           = Rio 2 image          = Rio 2 Poster.JPG border         = yes alt            = A group of blue birds sitting on the head of a crocodile, that is submerged in a river caption        = Theatrical release poster director       = Carlos Saldanha producer       = Bruce Anderson  John C. Donkin screenplay     = {{Plain list |
*Don Rhymer
*Carlos Kotkin
*Jenny Bicks
*Yoni Brenner
}} story          = Carlos Saldanha based on       = {{Plain list | 
*Characters by 
*Carlos Saldanha 
*Earl Richey Jones 
*Todd Jones
}} starring       = {{Plain list | 
*Jesse Eisenberg
*Anne Hathaway
*Leslie Mann
*Bruno Mars
*Jemaine Clement
*George Lopez
*Jamie Foxx
*will.i.am  
}} music  John Powell cinematography = Renato Falcão editing        = Harry Hitner studio         = Blue Sky Studios 20th Century Fox Animation distributor    = 20th Century Fox released       =   runtime        = 101 minutes   country        = United States  language       = English  Portuguese budget         = $103 million  gross          = $500.2 million   
}} 3D Computer-animated musical Adventure Rio de Janeiro, where the first film was set and Rio 2 begins, though most of its plot occurs in the Amazon rainforest.

Featuring the returning voices of Jesse Eisenberg, Anne Hathaway, will.i.am, Jamie Foxx, George Lopez, Tracy Morgan, Jemaine Clement, Leslie Mann, Rodrigo Santoro, and Jake T. Austin, the film was released internationally on March 20, 2014,  and on April 11, 2014,  in American theaters. Rio 2 was Don Rhymers final film after he died on November 28, 2012. Despite receiving mixed reviews, the film was a box office success.

==Plot== in the ornithologist husband, Amazon and eventually discover a quick-flying spixs macaw that loses one of its feathers. When word gets out about this through television, Jewel believes that they should go to the Amazon to help find the blue macaws. While the kids are ecstatic, Blu is uncertain, but is pressured into going along. Rafael, Nico and Pedro decide to come along. Luiz attempts to follow, but fails. Blu brings a fanny pack full of supplies, one of which he uses mostly is a Global Positioning System|GPS, much to Jewels displeasure.

Meanwhile, the leader of a group that is in a line of illegal logging named Big Boss, discovers Linda and Tulios expedition to find the macaws and orders his henchmen to hunt them down to avoid disruptions to their work. Also, Blu and Jewels old nemesis, Nigel the cockatoo, has survived the plane crash from the first film, but is now unable to fly and is working as a fortune teller/con artist. When he sees Blu and his family flying overhead of him, he immediately decides to seek revenge on them. He enlists two minions to help him in his plans; a silent anteater named Charlie and a poison dart frog named Gabi, the latter of which is in love with Nigel. Blu and his family use a boat to get to the jungle (with Nigels first plan of revenge being inadvertently foiled by Charlie), and when they arrive, they find nothing in sight. However, they are eventually taken to a flock of blue macaws that are hiding in a secret paradise land. There, they meet Jewels stern long lost father, Eduardo, his older sister Mimi, and Jewels childhood friend, Roberto. Eduardo seems unimpressed with Blus domesticated human behavior.

While searching for the macaws, Linda and Tulio are eventually trapped by the loggers. Meanwhile, Blu does his best to fit in with the flock, as his family and friends are doing, although the flock (especially Eduardo) are against humans and all things human. Meanwhile, a disguised Nigel plans to kill Blu at the new Carnival show after landing in an audition hosted by Rafael, Nico, Pedro, and Carla. When Blu tries to pick a Brazilian nut for Jewel, he accidentally tries to get it in the territory of the Spix Macaws enemies, the Scarlet macaws, led by the hostile Felipe. Blu inadvertently causes war between the two tribes for food when he accidentally hits Felipe with a twig. The war turns out to be just like football (soccer), and Blu accidentally costs the flock the food when he sends the fruit ball into his own teams goal.

Blu visits Tulio and Lindas site, where he discovers that it has been majorly disturbed. After discovering the loggers are destroying the jungle, Blu sends Roberto (who followed Blu) to warn the flock as he saves Linda and Tulio. Blu persuades the macaws to defend their homes, and they easily outmatch the loggers with help from the Scarlet macaws and the other animals. Big Boss tries to blow up the trees as a back-up plan, but Blu steals the lit dynamite. Nigel goes after Blu, and reveals himself as they are falling down when he tugs on the dynamite. After the dynamite goes off, Blu and Nigel engage in a battle while tangled in vines. Gabi and Charlie try to help Nigel by shooting Blu with a dart that has Gabis poison on it, but it accidentally hits Nigel, who gives a Shakespearean death speech before seemingly dying. Gabi tries to commit suicide by drinking her own poison and the pair are seemingly dead. However, Bia reveals that Gabi isnt poisonous at all (she was lied to by her parents that she was). Nigel tries to attack Blu one last time, but Gabi showers Nigel with affection against his will. Meanwhile, Big Boss is eaten alive by a boa constrictor.

With the flock now under Linda and Tulios protection, Blu and Jewel decide to live in the Amazon with their kids and friends, though still agreeing to visit Rio in the summer. Meanwhile, Nigel and Gabi are captured by Tulio to observe their unnatural partnership and are both taken back to Rio, Luiz finally arrives in the Amazon after hitching a ride with Kipo, and Charlie joins the birds party.

==Cast==
 .]]
*Jesse Eisenberg as  Blu, a male Spixs macaw from Moose Lake and Jewels mate   
*Anne Hathaway as Jewel, a female Spixs macaw from Rio de Janeiro and Blus mate 
*Leslie Mann as Linda Gunderson, an American woman who adopted Blu for 15 years. She is Tulios wife 
*Rodrigo Santoro as Tulio Monteiro, a Brazilian ornithologist and Lindas husband 
*Andy García  as Eduardo, Jewels father 
*Rachel Crow as Carla, Blu and Jewels music-loving, older daughter     
*Amandla Stenberg  as Bia, Blu and Jewels intelligent, younger daughter 
*Pierce Gagnon  as Tiago, Blu and Jewels youngest, mischievous, and only son 
*George Lopez as Rafael, a romantic Toco toucan fond of the Rio Carnival, Evas mate 
*will.i.am as Pedro, a rapping Red-crested cardinal 
*Jamie Foxx as Nico, Pedros close friend. He is a Yellow canary with a bottlecap hat 
*Jemaine Clement as Nigel, an evil sulphur-crested cockatoo who seeks revenge on Blu for crippling his ability to fly 
*Bruno Mars  as Roberto, Jewels suave childhood friend 
*Rita Moreno as Mimi, Eduardos older sister  
*Kristin Chenoweth  as Gabi, a poison dart frog and Nigels sidekick 
*Jake T. Austin as Fernando, Linda and Tulios adopted son 
*Tracy Morgan as Luiz, a bulldog and a chainsaw expert with a drooling condition 
*Bebel Gilberto as Eva, a Keel-billed toucan and Rafaels mate 
*Natalie Morales as a news anchor   
*Janelle Monáe as Dr. Monae, a veterinarian   
*Philip Lawrence as Felipe, a male Scarlet macaw and the hostile leader of a tribe who has a rivalry and territorial dispute with the Spixs macaws 
*Miguel Ferrer as Big Boss, the head of the illegal logging activity
*Jeffrey Garcia as Kipo, a Roseate spoonbill
*Kate Micucci as Tiny
*Randy Thom as Logging Foreman

==Production==
On January 25, 2012, while speaking to the   reported that Jesse Eisenberg had signed up to reprise his role as Blu,  and Anne Hathaway had also signed on to reprise her role as Jewel.  In October 2012, Variety (magazine)|Variety stated that Carlos Saldanha had officially signed a five-year deal with 20th Century Fox that allows him to helm live-action and/or animated films, with the sequel being part of that contractual agreement. 
 trailer was released online worldwide, and attached with Epic (2013 film)|Epic.  Entertainer Bruno Mars joined the cast as Roberto after director Carlos Saldanha caught his performance on Saturday Night Live. During production, Mars offered his own personal touches that better shaped his characters physical appearance, personality, and voice. 

==Release==
 , who voices Tulio, and the soundtracks producer Sérgio Mendes at the films press event.]]
The film was released to international theaters on March 20, 2014.  The films premiere was held in Miami, Florida on March 20, 2014.  The film was released in the United States on April 11, 2014. 

===Marketing=== John Powell—the films natural hometown of Rio de Janeiro, Brazil used the film as a tie-in promotion for the 2014 New Years Eve celebration at Copacabana Beach. 

Three of four Angry Birds Rio episodes — all visually tied to Rio 2 — have been released.  The first, "Rocket Rumble", was released in December 2013,  the second, "High Dive", in February 2014,  and the third, "Blossom River", in April 2014.  In April 2014, Kohls began selling Blu, Gabi, and Luiz plush toys as a part of their Kohl’s Cares merchandise program. 

===Home media===
Rio 2 was released on Blu-ray (2D and 3D) and DVD on July 15, 2014.  The Target exclusive comes with a Blu plush toy.  A limited sing-along edition of the film was released on Blu-ray and DVD on November 4, 2014.  

==Reception==

===Critical response===
Rio 2 received mixed reviews from critics. Review aggregation website  , which assigns a normalized rating out of 100 top reviews from mainstream critics, calculated a score of 49 out of 100 based on 34 reviews, which indicates mixed or average reviews. 
 Time Out gave the film three out of five stars, saying "There are problems here ... but the characterisation is feisty and memorable, the song-and-dance sequences intricate and colourful, and itll charm the socks off little people."  Kyle Smith of the New York Post gave the film two and a half stars out of four, saying "Rio 2 is not what I would call Amazon prime, but its got enough silly songs and daffy critters to keep the little ones happy."  Claudia Puig of USA Today gave the film two out of four stars, saying "Rio 2 teems with colorful animated splendor and elaborate musical numbers, but its rambling, hectic, if good-hearted, story is for the birds."  Richard Corliss of Time (magazine)|Time gave the film a positive review, saying "Even when its coarse and calculating, this is an eager entertainment machine that will keep the kids satisfied. Just dont tell them that the Rio movies are musical comedies about an avian genocide." 

Elizabeth Weitzman of the New York Daily News gave the film three out of five stars, saying "Were grading on a sliding scale here. But if Rio 2 is hardly Pixar quality, its certainly better than the average animated sequel."  Peter Hartlaub of the San Francisco Chronicle gave the film two out of four stars, saying "Its like the last Hobbit movie - so much time passes between side plots that you have to jog the memory when a minor character appears again. Whos that toucan again? Is he a bad guy?"  Bill Goodykoontz of The Arizona Republic gave the film three out of four stars, saying "An agreeable song-and-dance movie, a laugh here, a laugh there, pleasant but overly busy, for seemingly no real reason other than to throw a few more set pieces at the wall to see what sticks."  Jessica Herndon of the Associated Press gave the film three out of four stars, saying "With so much going on, its a wonder this kids movie is only five minutes longer than the original. But for the music and brilliantly picturesque look, its worth the 3-D ticket."  Stephanie Merry of The Washington Post gave the film two out of four stars, saying "All in all, though, the movie feels at once too busy and too derivative. Thats no easy feat, but its also one sequel-makers probably shouldnt aspire to."  Bruce Demara of the Toronto Star gave the film two and a half stars out of four, saying "Those who enjoyed the adventures of Blu and Jewel and company in the first Rio are going to find the sequel an equally pleasing diversion." 

Tom Russo of The Boston Globe gave the film two out of four stars, saying "The story flows, but not always freely, thanks to its manufactured feel."  Jeannette Catsoulis of The New York Times gave the film a negative review, saying "The cinematic equivalent of attack by kaleidoscope, Rio 2 sucks you in and whirls you around before spitting you out, exhausted."  Betsy Sharkey of the Los Angeles Times gave the film a negative review, saying "Wonderfully animated and well-voiced, Rio 2 is nevertheless too much. Too much plot, too many issues, too many characters."  Bill Zwecker of the Chicago Sun-Times gave the film three out of four stars, saying "Its as good as the first one and sure to please both the kiddies and adults with its two-tiered humor."  Tirdad Derakhshani of The Philadelphia Inquirer gave the film two out of four stars, saying "Itll keep the kids content for a couple of hours, though its likely to bore the grown-ups."  Liam Lacey of The Globe and Mail gave the film three out of four stars, saying "Rio 2 (like Fox’s Ice Age series) relies on derivative plotting and slapstick visual gags, in contrast to Pixar’s more cerebral originality. Where the film excels though, in an even more pronounced way than the first film, is in the choreographed animation for the musical numbers."  Alonso Duralde of The Wrap gave the film a negative review, saying "The musical moments, on the whole, stand out as the highlights of the film; Rio 2 becomes watchable when the flat characters shut up and sing." 

Rafer Guzman of   gave the film a B+, saying "Like its peppy predecessor, Rio 2 doesnt look or sound like other animated licenses to print money. That alone is reason enough to appreciate it."  Kevin McFarland of The A.V. Club gave the film a C, saying "Like the first film, Rio 2 is almost oppressively bright, bombarding the screen with flashes of saturated rainforest colors and even a bird version of soccer (timed a bit too perfectly to the 2014 World Cup in Brazil)."  Mike McCahill of The Guardian gave the film two out of five stars, saying "Its hard to ascribe much art or wit to a franchise that retains the services of will.i.am as comic relief – and a thoroughly inorganic talent-show subplot feels like another attempt to groom youngsters for life in the Cowell jungle."  Robbie Collin of The Daily Telegraph gave the film two out of five stars, saying "This jumbled sequel, which was also directed by Carlos Saldanha, loses most of what made the first film such an infectious entertainment."  Eric Henderson of Slant Magazine gave the film one out of five stars, saying "Though there isnt a fruit-flavored hue that isnt jammed into every single corner of screen space in Rio 2, the movie has less actual nutritional value than 10 bowls of crushed Froot Loops dust. 20th Century Foxs sequel to the already dubious 2011 film would seem far too endlessly hyperventilating and self-stimulating a way to keep kids from barreling toward a spaz attack on a Saturday afternoon." 

===Box office===
Rio 2 grossed $131,538,435 in North America, and $368,188,435 in other territories, for a worldwide total of $500,188,435, surpassing its predecessor.  In North America, the film earned $12 million on its opening day,  and opened to number two in its first weekend, with $39,327,869, behind  .  In its second weekend, the film dropped to number three, grossing an additional $22,159,742.  In its third weekend, the film dropped to number three, grossing $13,881,457.  In its fourth weekend, the film dropped to number five, grossing $7,711,952.  Twentieth Century Fox Home Entertainment donated $100,000 to WWF to support conservation efforts in the Amazon. 

===Accolades===
{| class="wikitable" style="width:95%;"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Category
! Recipients and nominees
! Result
|- British Academy Childrens Awards 
| BAFTA Kids Vote - Film in 2014
|
| 
|- 42nd Annie Annie Awards     Outstanding Achievement, Character Design in an Animated Feature Production Sang Jun Lee, Jason Sadler, José Manuel Fernandez Oli
| 
|- Outstanding Achievement, Storyboarding in an Animated Feature Production John Hurst
| 
|- Outstanding Achievement, Storyboarding in an Animated Feature Production Rodrigo Perez-Castro
| 
|- Outstanding Achievement, Voice Acting in an Animated Feature Production Andy Garcia as the voice of Eduardo
| 
|-
| 2015 Kids Choice Awards|Kids Choice Awards 
| Favorite Animated Movie
|
| 
|- 41st Peoples Choice Awards|Peoples Choice Awards  Favorite Family Movie
|
| 
|- Visual Effects Visual Effects Society Awards  Outstanding Animation in an Animated Feature Motion Picture Carlos Saldanha, Bruce Anderson, John C. Donkin, Kirk Garfield
| 
|- Outstanding Animated Character in an Animated Feature Motion Picture
|Gabi—Jason Sadler, Ignacio Barrios, Drew Winey, Diana Diriwaechter
| 
|- 19th Satellite Satellite Awards  Satellite Award Best Original Song
|"What is Love"—Janelle Monáe
| 
|- 18th Hollywood Hollywood Film Awards    Hollywood Song Award
|"What is Love"—Janelle Monáe
| 
|}

==Music==

===Soundtrack===
{{Infobox album
| Name       = Rio 2: Music from the Motion Picture
| Type       = Soundtrack
| Artist     = Various Artists
| Cover      =
| Released   =  
| Recorded   =
| Genre      = Pop music|Pop, Latin music|Latin, alternative hip hop
| Length     =  
| Label      = Atlantic Records John Powell
| Reviews    =
| Chronology = Blue Sky Studios film soundtrack
| Last album = Epic (2013 film)|Epic (2013)
| This album = Rio 2 (2014)
| Next album = 
| Misc = {{Singles
| Name = Rio 2: Music from the Motion Picture
| Type = Soundtrack
| single 1 = What Is Love
| single 1 date = March 4, 2014
}}
}}

A soundtrack for the film was released on March 25, 2014, by Atlantic Records.     It was promoted by the single "What Is Love", performed by Janelle Monáe. 

;Track listing
{{Track listing
| extra_column = Performers
| total_length = 39:07
| title1 = What Is Love
| extra1 = Janelle Monáe
| length1 = 3:31
| title2 = Rio Rio
| note2 = featuring B.o.B
| extra2 = Ester Dean
| length2 = 2:41
| title3 = Beautiful Creatures
| extra3 = Barbatuques, Andy García, and Rita Moreno
| length3 = 2:07
| title4 = Welcome Back
| extra4 = Bruno Mars
| length4 = 1:08
| title5 = Ô Vida
| extra5 = Carlinhos Brown and Nina De Freitas
| length5 = 1:47
| title6 = Its a Jungle Out Here
| note6 = featuring Uakti (band)|Uakti; Brazilian Version
| extra6 = Philip Lawrence
| length6 = 3:59
| title7 = Dont Go Away
| note7 = featuring Uakti
| extra7 = Anne Hathaway and Flavia Maia
| length7 = 2:38
| title8 = Batucada Familia
| extra8 = Carlinhos Brown, Siedah Garrett, Jamie Foxx, Rachel Crow, Amy Heidemann, Andy García, and Rita Moreno
| length8 = 2:42
| title9 = Poisonous Love
| extra9 = Kristin Chenoweth and Jemaine Clement
| length9 = 3:30
| title10 = I Will Survive
| extra10 = Jemaine Clement and Kristin Chenoweth
| length10 = 1:51
| title11 = Bola Viva
| extra11 = Carlinhos Brown
| length11 = 3:22
| title12 = Favo De Mel 
| extra12 = Milton Nascimento
| length12 = 3:08
| title13 = Its a Jungle Out Here
| extra13 = Philip Lawrence
| length13 = 4:00
| title14 = What Is Love
| extra14 = Janelle Monáe, Anne Hathaway, Jesse Eisenberg, Jamie Foxx, and Carlinhos Brown
| length14 = 2:43
}}

===Charts===
{| class="wikitable sortable"
|-
! Chart (2014)
! Peak position
|-
| US Billboard 200|Billboard 200  124
|-
| US Soundtracks  4
|}

===Score===
{{Infobox album
| Name       = Rio 2
| Type       = film John Powell
| Cover      = 
| Released   = April 8, 2014
| Recorded   = 2014 Score
| Length     = 
| Label      = Sony Classical
| Producer   =  John Powell film scores
| Last album =   (2012)
| This album = Rio 2 (2014)
| Next album = How to Train Your Dragon 2 (2014)
}} John Powells original score was released on April 8, 2014 by Sony Classical. 

;Track listing
{{Track listing
| total_length  = 55:10 John Powell, except as noted
| title1        = 20th Century Fox Fanfare (Samba Version) Alfred Newman
| length1       = 0:24
| title2        = Batucada People
| length2       = 1:35
| title3        = Over the Falls
| note3         = featuring Milton Nascimento
| length3       = 3:39
| title4        = Breakfast in Rio
| length4       = 3:08
| title5        = Fireworks on the Roof Uakti
| length5       = 1:27
| title6        = Traveling Family
| length6       = 1:59
| title7        = Sideshow Freaks Uakti
| length7       = 3:08
| title8        = Stalking the Ferry
| length8       = 2:06
| title9        = River Boat on the Loggers Uakti
| length9       = 2:59
| title10       = Escorted to the Clan Uakti and Barbatuques
| length10      = 5:40
| title11       = Up Carlas Monkey Uakti
| length11      = 2:15
| title12       = Spider Invite Uakti and Barbatuques
| length12      = 2:46
| title13       = Humans Are Longer Than They Told Me Uakti
| length13      = 2:23
| title14       = Tongue-apult to Blus Nightmare
| length14      = 2:08
| title15       = Red Bullies Uakti
| length15      = 3:19
| title16       = Tantrums Lead to Explosions Uakti
| length16      = 3:42
| title17       = Lollipops Are Bad for Your Teeth Uakti and Barbatuques
| length17      = 3:55
| title18       = Battle for the Heart of the Forest
| length18      = 4:45
| title19       = Romeo and Juliets Unfortunate Demise Uakti and Barbatuques
| length19      = 3:52
}}

==Sequel==
Director Carlos Saldanha has kept the possibility for a Rio 3 open. He has stated, "Of course, I have a lot of stories to tell, so were   prepare for it." 

==References==
 

==External links==
 
 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 