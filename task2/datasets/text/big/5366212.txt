Fan Chan
{{Infobox film
| name           = Fan Chan (My Girl)
| image          = Fan-chan1.jpg
| caption        = Thai movie poster
| director       =  Vitcha Gojiew,  Songyos Sugmakanan,  Nithiwat Tharathorn,  Witthaya Thongyooyong,  Anusorn Trisirikasem,  Komgrit Triwimol 
| producer       = 
| writer         = 
| starring       = Charlie Trairat, Focus Jirakul
| music          = 
| cinematography = 
| editing        =  GMM Pictures
| released       = October 3, 2003
| runtime        =  Thailand
| Thai
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 2003 Cinema Thai romantic film offering a nostalgic look back at the childhood friendship of a boy and girl growing up in a small town in Thailand in the 1980s. It was the debut film by six young screenwriter-film director|directors, Vitcha Gojiew, Songyos Sugmakanan, Nithiwat Tharathorn, Witthaya Thongyooyong, Anusorn Trisirikasem and Komgrit Triwimol. With a soundtrack that featured Thai pop music of the era, Fan Chan was the top domestic film at the Thailand box office in 2003, earning 140 million baht.

==Plot==

Jeab, a young man working in Bangkok, receives word that his best friend from childhood, Noi-Naa is to be married. While driving back to his hometown, the memories of his friendship with her come flooding back, and their story is told in a Flashback (literary technique)|flashback.

Jeab and Noi-Naa live in a small city in Thailand. Their fathers are rival barbers, with shops situated next to each other, with only a sweet shop to separate them. Jeabs father favors efficiency and uses an electric trimmer. Noi-Naas father, meanwhile, has a more contemplative, artistic approach, and uses scissors. Jeab notes that the results of both methods seem to be the same.

 
The school holiday is ended. Jeab is notorious for oversleeping, so that each day he misses the school bus and must be driven part way by his father on a motorcycle. By taking a shortcut, Jeab and his father are able to catch up to the bus, but only just in time.
 bully named Chinese fantasy characters, while the girls plan to play "house".

Because Jeab must cross a busy street to play with the boys, and he fears getting hit by a car, he stays to play with the girls, which makes him the target of much taunting by Jack and the other boys.

Then, one day, Jack and his friends are playing soccer against a rival neighborhood gang. They are one player short. Jeab happens to be hanging around, and hes asked to join the game, proving his abilities.

He earns the trust of Jacks gang, and passes various tests in order to join. But the one thing he must do is sever his ties to Noi-Naa. Jeab does so, quite literally, by cutting a rubber band|rubber-band jump rope, which Noi-Naa is skilled at playing with.

From that moment on, Noi-Naa refuses to talk to Jeab. Then, one day, Jeab gets word that Noi-Naa is moving away. And, of course, on the day she is to leave, Jeab oversleeps and misses the chance to say his final goodbye to Noi-Naa. Jeab then gets Jack and his friends to commandeer a delivery motorcycle and pursue Noi-Naa and her family in their moving truck. But the motorcycle breaks down, and the truck rolls out of sight. Jeab is to never see Noi-Naa again ... until her wedding.

==Cast==
*Charlie Trairat as Jeab 
*Focus Jirakul as Noi-Naa 
*Charwin Jitsomboon as Jeab (adult) 
*Wongsakorn Rassamitat as Jeabs father 
*Arnudsara Jantarangsri as Jeabs mother
*Nipawan Taveepornsawan as Noi-Naas mother 
*Prem Tinsulanonda as Himself (on television)
*Aphichan Chaleumchainuwong as Dtee
*Preecha Chanapai as Noi-Naas father
*Anyarit Pitakkul as Boy 
*Yok Teeranitayatarn as Manoj
*Chaleumpol Tikumpornteerawong as Jack
*Thana Vichayasuranan as Prik

==Trivia== 2005 film directed by one of the six Fan Chan directors, Komgrit Triwimol. Indonesian and featuring the countrys pop music of the era.

==External links==
*  

 
 
 
 
 
 
 