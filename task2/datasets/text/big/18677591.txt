Il medico dei pazzi
{{Infobox film
| name           = Il medico dei pazzi
| image          = Il medico dei pazzi1.jpg
| caption        = Film poster
| director       = Mario Mattoli
| producer       = Alfredo De Laurentiis Carlo Ponti
| writer         = Ruggero Maccari Mario Mattoli Totò Eduardo Scarpetta Vincenzo Talarico (from a play written by Eduardo Scarpetta)
| starring       = Totò
| music          = Pippo Barzizza
| cinematography = Riccardo Pallottini
| editing        = 
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Il medico dei pazzi is a 1954 Italian comedy film directed by Mario Mattoli and starring Totò.

==Cast==
* Totò as Felice Sciosciammocca
* Franca Marzi as La moglie di Cristaldi
* Aldo Giuffrè as Ciccillo
* Vittoria Crispo as Amalia
* Carlo Ninchi as Lattore
* Tecla Scarano as La moglie di Felice
* Nerio Bernardi as Il colonello
* Giacomo Furia as Michele
* Nora Ricci as La figlia di Amalia
* Anna Campori as Una signora
* Mario Castellani as Cristaldi

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 