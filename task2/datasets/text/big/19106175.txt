Inn for Trouble
{{Infobox film
| name           = Inn for Trouble
| image          = "Inn_for_Trouble"_(1960).jpeg
| director       = C. M. Pennington-Richards
| writer         = Fred Robinson
| producer       = Ted Lloyd Charles Hawtrey Philip Green Eric Cross
| editing        = Tom Simpson
| studio        = Hyams and Lloyd Productions (as Film Locations)
| distributor    = Eros Films (UK)
| released       = February 1960
| runtime        = 90 min
| country        = United Kingdom English
| budget         =
}} Trouble House Inn in Gloucestershire near Tetbury.

The Larkin family take over a run-down country public house, "The Earl of Osbourne", but their efforts to rejuvenate business are impeded by the long-standing tradition of free beer being distributed by the local Earl. The film is notable for the final credited appearance of Graham Moffatt in the role of Jumbo. He is best remembered as Albert, the fat boy in Gainsborough Studios famous Will Hay films of the late thirties. He left acting to become a pub landlord, dying of a heart attack in 1965 at the age of 45.  (His final uncredited appearance was in "80,000 Suspects" in 1963.)

==Cast==
* Peggy Mount as Ada Larkin
* David Kossoff as Alf Larkin
* Leslie Phillips as John Belcher
* Glyn Owen as Lord Bill Osborne
* Yvonne Monlaur as Yvette Dupres
* A. E. Matthews as Sir Hector Gore-Blandish
* Ronan OCasey as Jeff Rogers
* Shaun ORiordan as Eddie Larkin
* Alan Wheatley as Harold Gaskin
* Willoughby Goddard as Sergeant Saunders
* Alan Rolfe as Ted
* Gerald Campion as George Stanley Unwin as Farmer
* Irene Handl as Lily
* Graham Moffatt as Jumbo Gudge Charles Hawtrey as Silas Withering
* Esma Cannon as Dolly
* Edward Malin as Old Charlie
* Barbara Mitchell as Hetty Prout
* Graham Stark as Charlie Frank Williams as Percy Pirbright

==External links==
*  

 

 
 
 
 
 


 
 