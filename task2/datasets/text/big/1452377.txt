Cry-Baby
 
 
{{Infobox film
| name           = Cry-Baby
| image          = Crybabyposter.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = John Waters
| producer       = Rachel Talalay
| writer         = John Waters
| starring       = Johnny Depp Amy Locane Polly Bergen Susan Tyrrell Iggy Pop Ricki Lake Traci Lords Patrick Williams
| cinematography = David Insley
| editing        = Janice Hampton
| studio         = Imagine Entertainment Universal Pictures
| released       =  
| runtime        = 85 minutes   91 minutes  
| country        = United States
| language       = English
| budget         = $12 million 
| gross          = $8,266,343 
}}
 teen Musical musical romantic David Nelson, and Patricia Hearst.
 delinquents that refer to themselves as "drapes" and their interaction with the rest of the town and its other subculture, the "squares", in 1950s Baltimore, Maryland. "Cry-Baby" Walker, a drape, and Allison, a square, create upheaval and turmoil in their little town of Baltimore by breaking the subculture taboos and falling in love. The film shows what the young couple has to overcome to be together and how their actions affect the rest of the town.
 Enchanted Forest amusement park in Ellicott City, Maryland. Others take place in the historic towns of Reisterstown, Jessup, Milford Mill, and Sykesville, Maryland.
 cult classic Broadway Cry-Baby musical of the same name which was nominated for four Tony Awards.

==Plot==
In 1954 Baltimore, Maryland|Baltimore, Wade "Cry-Baby" Walker is the leader of a gang of "Greaser (subculture)|Drapes", which includes his sister Pepper, a teenage mother, Mona "Hatchet Face" Malnorowski, who is facially disfigured, Wanda Woodward, whos constantly embarrassed by her yuppie parents, and Milton Hackett, Hatchet Faces devoted boyfriend. His ability to shed a single tear drives all the girls wild. One day after school, he is approached by Allison Vernon-Williams, a pretty girl tired of being a "square", and the two fall in love. ("Women in Cadillacs", "Gee!"),  That same day, Cry-Baby approaches the "square" part of town to a talent show ("Sh-Boom", "A Teenage Prayer") at the recreation center where Allisons grandmother hosts events, and introduces himself to her, who is skeptical of his motives. Cry-Baby invites Allison to a party at Turkey Point, a local hangout spot for the drapes.

Despite her grandmothers skepticism, Allison accompanies Cry-Baby to Turkey Point and sings with the drapes ("King Cry-Baby"). As Cry-Baby and Allison tell each other about their orphan lives (Cry-Babys father was sent to the electric chair after being the "Alphabet Bomber" - a killer who bombed places in alphabetical order while his mother tried to stop him but got killed as a result; Allisons parents always took separate flights to avoid orphaning her if they crashed, but one day both their planes went down), Allisons jealous square boyfriend, Baldwin, starts a riot. Cry-Baby is blamed for the fight and sent to a Prison|penitentiary, outraging all his friends and even Allisons grandmother, who is impressed by Cry-Babys posture, manners, and musical talent. ("Teardrops Are Falling") 

As Lenora Frigid, a loose girl with a crush on Cry-Baby but constantly rejected by him, claims to be pregnant with his child, Allison feels betrayed and returns to Baldwin and the squares, though her grandmother advises her against rushing into a decision. ("Doin Time Bein Young") Meanwhile, in the penitentiary, Cry-Baby gets a teardrop tattoo. He tells the tattoo artist, fellow drape Dupree (Robert Tyree): "Ive been hurt all my life, but real tears wash away. This ones for Allison, and I want it to last forever!".
 chicken race. ("High School Hellcats") Cry-Baby wins, as Baldwin chickens out, and is reunited with Allison.

The film ends with all watching the chicken race crying a single tear, all except for Allison and Cry-Baby, who has finally let go of the past, enabling him to cry from both eyes.

==Cast==
 
* Johnny Depp as Wade "Cry-Baby" Walker
** James Intveld as Cry-Babys singing voice
* Amy Locane as Allison Vernon-Williams
** Rachel Sweet as Allisons singing voice
* Polly Bergen as Mrs. Vernon-Williams
* Susan Tyrrell as Ramona Rickettes
* Iggy Pop as Belvedere Rickettes
* Ricki Lake as Pepper Walker
* Traci Lords as Wanda Woodward
* Kim McGuire as Mona "Hatchet-Face" Malnorowski
* Darren E. Burrows as Milton Hackett
* Kim Webb as Lenora Frigid
* Stephen Mailer as Baldwin
* Alan J. Wendl as Toe-Joe Jackson
* Jonathan Benya and Jessica Raskin as Snare-Drum and Susie Q, Peppers son and daughter
* Troy Donahue and Mink Stole as Mr. and Mrs. Malnorowski, Hatchet-Faces parents, who sell cigarettes to high school students
* Joe Dallesandro and Joey Heatherton as Mr. and Mrs. Hackett, Miltons overzealous, religious parents
* Robert Marbury as Peppers angelic boyfriend David Nelson and Patricia Hearst as Mr. and Mrs. Woodward, Wandas happy-go-lucky parents who remain completely oblivious to their daughters activities
* Robert Tyree as Dupree, a faithful Drape who joins Cry-Baby in the slammer
* Robert Walsh as Judge, the judge who sentences Cry-Baby to the reformatory; he has a crush on Mrs. Vernon-Williams
* Willem Dafoe as Hateful guard
* Brad Baker as Mean Policeman
 

==Musical numbers==
# "Women in Cadillacs by Doc Starkes and The Night Riders" *(sung along)* - Cry-Baby
# "Gee! by The Crows" *(sung along)* - Cry-Baby, Hatchet-Face, Milton, Pepper, Wanda 
# "Sh-Boom" – Baldwin, the Whiffles
# "A Teenage Prayer" – Allison
# "King Cry-Baby" – Cry-Baby, Allison, Hatchet-Face, Milton, Pepper, Wanda
# "Teardrops Are Falling" – Cry-Baby, Dupree, Prisoners
# "Doin Time for Bein Young" – Cry-Baby, Prisoners
# "The Naughty Lady of Shady Lane" (Directors cut) – Baldwin, the Whiffles
# "Mr. Sandman" – Allison, Baldwin, the Whiffles
# "Please, Mr. Jailer" – Allison, Cry-Baby, Company, Prisoners
# "Chicken" (deleted scene) – Baldwin, the Whiffles
# "High School Hellcats" – Cry-Baby, Allison, Pepper, Company

==Release==
The film was screened out of competition at the 1990 Cannes Film Festival.   

===Critical reception===
The film currently holds a 71% "Fresh" rating on Rotten Tomatoes based on 55 reviews, with the consensus "John Waters musical ode to the teen rebel genre is infectious and gleefully camp, providing star Johnny Depp with the perfect vehicle in which to lampoon his pin-up image."  Roger Ebert of The Chicago Sun-Times awarded the film 3 out of 4 stars. 

===Box office===
Cry-Baby opened on April 6, 1990 in 1,229 North American cinemas — an unprecedented number for a John Waters film. In its opening weekend, it grossed a soft $3,004,905 ($2,445 per screen) and grossed $8,266,343 by the end of its theatrical run,    making it a box office flop from its $12 million budget.   

==Musical adaptation==
 
Cry-Baby is the second of Waters films to be adapted for the stage as a musical comedy (following Hairspray (musical)|Hairspray).

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 