Killed the Family and Went to the Movies (1969 film)
{{ infobox film
| name           = Killed the Family and Went to the Movies
| image          = 
| director       = Júlio Bressane
| producer       = 
| writer         = Júlio Bressane
| starring       = Renata Sorrah Vanda Lacerda Paulo Padilha Rodolfo Arena Carlos Eduardo Dolabella Márcia Rodrigues
| cinematography = 
| editing        = 
| distributor    =  1969
| music          = 
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}} Brazilian film directed by Júlio Bressane and released in 1969 in film|1969.

This is quite an innovative movie, in which the protagonist – after doing what the title says – watches four short sketches of other movies with varied plots, including one about rape. It seems that the film is intended to be a harsh (but indirect) critique of sensationalist newspapers (the films title is taken from mock news headlines), banalisation of violence and sexual exploitation. One of the possible explanations for the plot is to criticise torturers who killed students but still went home in peace.
 remake was made in 1991. This version, also written by Bressane but directed by Neville de Almeida, was more polished visually (in colour) and had a nice musical score, but suffered from bad acting. It added an interesting trick in that the film starts with loud music and without any credits; these only appear at the end, after fake newspaper headlines show the films name.

== External links ==
*  

 
 

 
 
 
 