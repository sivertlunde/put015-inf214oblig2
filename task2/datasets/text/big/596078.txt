Good Boy!
 

{{Infobox film
| name = Good Boy!
| image = Good boy poster.jpg
| caption = Theatrical release poster John Hoffman
| producer = Lisa Henson Kristine Belson John Hoffman
| starring = Liam Aiken Matthew Broderick
| music = Mark Mothersbaugh
| cinematography = James Glennon
| editing = Craig Herring
| studio  = Jim Henson Pictures
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 87 minutes
| country = United States English
| budget = $18 million
| gross = $45,312,217
}}

Good Boy! is a 2003 comedy film, directed by John Robert Hoffman and produced by Jim Henson Pictures, released by Metro-Goldwyn-Mayer, and starring 7 talking dogs. The film stars Liam Aiken as Owen Baker, as well as the voices of Matthew Broderick, Delta Burke, Donald Faison, Brittany Murphy, Carl Reiner, Vanessa Redgrave, and Cheech Marin as the abundant dog characters in the movie. The film was based on the book Dogs from Outer Space by Zeke Richardson. John Hoffman and Richardson collaborated on the screen story, while Hoffman wrote the screenplay.

==Plot==
Owen Baker (Liam Aiken) is a 12-year-old who has been working as the neighborhood dog-walker so he can earn the privilege of getting a dog of his own. Owens hard work pays off when his parents, Mr. and Mrs. Baker (Molly Shannon and Kevin Nealon), let Owen adopt a scruffy Border Terrier that he names Hubble (voice of Matthew Broderick). 
Owen has little time to make lasting friends, so he hopes Hubble will be his best friend.

Owen does have a friend named Connie Flemming (Brittany Moldowan), a girl his age who lives in the neighborhood. But that wont be for long if Owens parents continue their trend of buying and selling houses. Owen and Hubble get more than they bargained for when Owen wakes up one morning to discover that he can understand every word Hubble says—including the ominous phrase: "Take me to your leaders."

Owen learns that dogs came to Earth thousands of years ago to colonize and dominate the planet. Hubble, who is really named Canid 3942, has been sent by the powerful Greater Dane (voice of Vanessa Redgrave) on a mission from the Dog Star Sirius 7 to make sure dogs have fulfilled this destiny.
 Boxer Wilson (voice of Donald Faison), nervous Italian Greyhound Nelly (voice of Brittany Murphy) and gassy Bernese Mountain Dog Shep (voice of Carl Reiner).

Despite the best efforts of Owen and this rag-tag group of neighborhood dogs to convince Hubble that everything is fine with Earths dogs, Hubble soon discovers the awful truth about Earth dogs: "Youre all pets!" Things get worse when Hubble learns that the Greater Dane is headed for Earth to do her own inspection. If things dont look right, all dogs on Earth will be recalled to Sirius 7.
 Chinese Crested henchman (voice of Cheech Marin). Owen, Hubble, Connie, and their canine pals set out to whip the other dogs into shape so that they can pass muster.

==Cast==
* Liam Aiken as Owen Baker
* Kevin Nealon as Mr. Baker
* Molly Shannon as Mrs. Baker
* Brittany Moldowan as Connie Flemming
* Hunter Elliott as Frankie
* Mikhael Speidel as Fred
* Patti Allan as Ms. Ryan
* Benjamin Ratner as Wilsons Dad
* Peter Flemming as Wilsons Other Dad
* George Touliatos as Mr. Leone
* D. Harlan Cutshall as Mr. Fleming
* Brenda M. Crichlow as Mrs. Fleming
* Paul Vogt as Dog Catcher

===Voices===
* Matthew Broderick as Hubble Boxer
* Delta Burke as Barbara Ann, a poodle
* Carl Reiner as Shep
* Brittany Murphy as Nelly 
* Vanessa Redgrave as The Greater Dane
* Cheech Marin as The Greater Danes Henchman

==Production==

===Special effects===
The bulk of the digital effects in Good Boy! involved digitally altering the facial features of the dogs so that in the film, they appear to be talking or expressing a different emotion (sometimes called CG muzzle replacement). These effects were handled by Rainmaker Studios. 

==Reception==
The films domestic total gross was around $37 million, with a worldwide gross of around $45 million.  Good Boy! received mixed reviews from critics, earning a 45% rating at Rotten Tomatoes.

Roger Ebert of the Chicago Sun-Times disliked the way Good Boy! was handled. "Sometimes it works to show their lips moving (it certainly did in "Babe (film)|Babe"), but in "Good Boy!" the jaw movements are so mechanical it doesnt look like speech, it looks like a film loop. Look at "Babe" again and youll appreciate the superior way in which the head movements and body language of the animals supplement their speech." 
 United Paramount Network, The Philadelphia Inquirer, The Seattle Times, and Jeanne Wolfs Hollywood) praised the film. There was also a BBC-animated short film from 2002 that accompanied the entire theatrical release. It is called Hamilton Mattress, and after the release, it was on Cartoon Network. Afterwards, in 2004, it was released on DVD and VHS.

===Home media===
Good Boy! was released on DVD and VHS in 2004.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 