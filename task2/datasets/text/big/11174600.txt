Dil Ke Jharoke Main
 
{{Infobox Film
| name           = Dil Ke Jharoke Main
| image          = Dil Ke Jharoke Main.jpg
| image_size     = 
| caption        = Dvd cover
| director       = Ashim Bhattacharya
| producer       = Ronnie Screwvala Zarine Mehta
| writer         = 
| narrator       = 
| starring       = Manisha Koirala Vikas Bhalla Mamik Parveen Dastur Kulbhushan Kharbanda
| music          = Bappi Lahiri
| cinematography = 
| editing        = 
| distributor    = United Motion Pictures
| released       = 10 April 1997
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1997 Bollywood musical romance film directed by Ashim Bhattacharya.

==Plot== heart on their arms and hope to remember each other for the rest of their lives. Years later, they unknowingly meet each other, and this time Suman is married to Vijays brother, Prakash, while Vijay is married to a rich and wealthy U.S. returned woman named Rita. Unfortunately, Prakash and Suman meet with an accident shortly after their marriage, but both recover. But Vijays marriage with Rita is on the rocks due to incompatibility, with Rita moving out of Vijays life. It is then Advocate Suresh, Sumans brother, comes across evidence that leads him to conclude that the couple was swapped by unknown person(s).

== Cast ==
* Vikas Bhalla   ... Vijay Rai
* Manisha Koirala   ... Suman
* Mamik Harmeet   ... Prakash Rai
* Satish Kaushik   ... Mac / Mohan Pahariya
* Kiran Kumar   ... Heera Pratap]
* Kulbhushan Kharbanda   ... Mahendrapratap Rai
* Aparajita
* Chandrashekhar   ... Doctor
* Poonam Dasgupta
* Pervin Dastur   ... Rita Prakash / Rita Rai
* Baby Gazala   ... Munni
* Satyendra Kapoor   ... Satya Pratap
* Ram Mohan   ... College Principal
* Anjana Mumtaz   ... Mrs. Mahendrapratap Rai
* Amita Nangia   ... Julie (Macs wife)
* Sudhir Pandey   ... Surendra Prakash (Ritas dad)
* Shashi Puri   ... Advocate Suresh
* Sanjivini
* Shivraj   ... Kashi (Surendras butler)

==Music==
{{Infobox album |  
 Name = Dil Ke Jharoke Main
| Type = Soundtrack
| Artist = Bappi Lahiri
| Cover =  1997
| Recorded =  Feature film soundtrack
| Length = 
| Label =  Venus Records & Tapes
| Producer = Bappi Lahiri
| Reviews =

| Last album = Amaanat  (1994)
| This album = Dil Ke Jharoke Main  (1997)
| Next album = Ganga Ki Kasam  (1998) 
}}
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Aao Re" 
| Udit Narayan, Vinod Rathod, Alka Yagnik
| 07:53
|-
| 2
| "Dil Ke Jharoke Main"
| Kumar Sanu, Alka Yagnik
| 06:23
|-
| 3
| "Rassi Utte Tangaya"
| Alka Yagnik
| 06:53
|-
| 4
| "Shahe Dilbara"
| Kumar Sanu, Kavita Krishnamurthy
| 06:00
|-
| 5
| "Jugnuon Se Jagmag"
| Udit Narayan
| 07:31
|-
| 6
| "Shama Ne Jalaaya Ho (Sad Song)"
| Kumar Sanu
| 07:43
|}

==References==
*   at movietalkies.com

==External links==
*  

 
 
 


 