Prem Geet
{{Infobox film
| name           = Prem Geet
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sudesh Issar
| producer       = Pawan Kumar Lakhan Sinha
| writer         = Shashi Bhushan	 
| screenplay     = Shashi Bhushan
| story          = 
| based on       =  
| narrator       = 
| starring       = Raj Babbar Anita Raj
| music          = Jagjit Singh
| cinematography = K.R. Murthy
| editing        = Gurudutt Shiral Waman B. Bhosle	
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Sudesh Issar. It stars Raj Babbar and Anita Raj in pivotal roles.

==Plot== 
Akash and Shikha make an impressive creative team as they work together in a dance troupe, and a romantic relationship soon flourishes between them. But when it transpires that Shikha only has a few months to live, and shes pregnant, some dramatic scenes unravel as the movie travels nears its climax

==Cast==
* Raj Babbar...Akash Bhardwaj
* Anita Raj...Shikha
* Rajni Sharma...Asha 
* Showkar Janaki...Mrs. Laxmi Bhardwaj (as Sowcar Janki) 
* Madan Puri...Mr. Bhardwaj 
* Shashi Ranjan...Dr. Amar 
* Komal Soni   
* Sajjan...Hiralal 
* Shammi...Gynecologist 
* Rajkishore Rana...Professor (as Raj Kishore) 
* Viju Khote...Deepak Agarwal 
* Vandana Rane   
* Yasmin  (as Yasmeen) 
* Zubeida  (as Zubeda) 
* Chandramohan  (as Chandermohan) 
* Kumar Gautam   
* Ashok Saxena   
* Sonika Gill  (as Sonika) 
* Ram Avtar ...  Patient (uncredited) 
* Gulshan Grover ...  Rangeela (uncredited)

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dekh Lo Awaz Dekar"
| Anuradha Paudwal
|-
| 2
| "Dilber Jani"
| Asha Bhosle
|-
| 3
| "Dulhe Raja"
| Asha Bhosle
|-
| 4
| "Khwabon Ko"
| Suresh Wadkar
|-
| 5
| "Aao Mil Jaye"
| Suresh Wadkar, Anuradha Paudwal
|-
| 6
| "Kitne Ahsan Kiye Tumne Humpe"
| Asha Bhosle
|-
| 7
| "Hothon Se Chhu Lo Tum"
| jagjit singh
|}

==External links==
* 

 
 
 

 