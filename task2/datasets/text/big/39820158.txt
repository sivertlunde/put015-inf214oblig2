The Picasso Summer
{{Infobox film
| name           = The Picasso Summer
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Serge Bourguignon Robert Sallin
| producer       = Bruce Campbell Wes Herschensohn
| writer         = Ray Bradbury (alt Douglas Spaulding) Edwin Boyd
| based on       = 
| screenplay     = 
| narrator       = 
| starring       = Albert Finney Yvette Mimieux Luis Miguel Dominguín
| music          = Michel Legrand
| cinematography = Vilmos Zsigmond
| editing        = William Paul Dornisch
| studio         = 
| distributor    = Warner Brothers
| released       =  
| runtime        = 90 min.
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}
 drama directed by Serge Bourguignon and Robert Sallin, starring Albert Finney and Yvette Mimieux. The screenplay was written by Ray Bradbury (using the pseudonym of Douglas Spaulding)   based upon his short story, In a Season of Calm Weather.  

Future Academy Award winner Vilmos Zsigmond was the cinematographer. There were two directors. Serge Bourguignon was the original director whose rough cut was rejected by Warner Brothers. Another director, Robert Sallin, was hired to re-shoot some scenes and to do the changed ending. Even with the reworked scenes, the movie was never released to theaters in the United States. It was sold for distribution to TV networks and stations with Salin receiving the directors credit.

==Plot summary==

George Smith (Albert Finney) is a middle age San Francisco architect in mid-life crisis and dissatisfied with the extent of his professional career, as his latest project is nothing more than a warehouse. After he and his wife Alice (Yvette Mimieux) attend a large party they go home and reassess their lives. George remembers how he used to admire Pablo Picasso so much when he was in college and suddenly proposes to his wife that they go to Europe and find Picasso. As they travel through numerous places in France and Spain they meet a number of interesting people but Georges obsessive quest begins to wear on Alice. They never actually find Picasso, and in a typical Hollywood ending, they walk off into the sunset after reaffirming their love for each other.

===Differences between the original story and the movie===

While the basic plot is the same, the movie introduces several elements to stretch the short story into a feature length movie. One change is the animated sequences where Picassos paintings "come alive" to show the vibrant nature of his work. George and Alice become separated and Alice happens to meet a painter who is now blind. Another addition involves Georges encounter with a bull fighter. A number of Picassos works deal with bull fighting and a long sequence shows the bull fighter slowly goring a bull to death, as a way to explain Picassos fascination with the subject.

The biggest change is the ending. The original story ends in the typical Bradbury style with a bittersweet twist, as George does meet Picasso, but is left with only a memory that  he realizes will eventually fade away.

==Cast==
* Albert Finney as George Smith
* Yvette Mimieux as Alice Smith
* Luis Miguel Dominguín as Himself (the bull fighter) Theo Marcuse as The Host
* Jim Connell as The Artist Sopwith Camel as The Band at the Party

==References==
 	

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 