One Fine Day (film)
{{Infobox film
| name           = One Fine Day
| image          = One Fine Day (1996 film) poster.jpg
| caption        = Theatrical release poster Michael Hoffman
| producer       = Lynda Obst
| writer         = Terrel Seltzer Ellen Simon
| starring       = Michelle Pfeiffer George Clooney Alex D. Linz Mae Whitman
| music          = James Newton Howard
| cinematography = Oliver Stapleton
| editing        = Garth Craven
| studio         = Lynda Obst Productions Via Rosa Productions 
| distributor    = 20th Century Fox
| released       =  
| gross          = $97,529,550	 
| runtime        = 108 minutes
| country        = United States
| language       = English
}} Michael Hoffman, One Fine Day" by The Chiffons.

Michelle Pfeiffer served as an executive producer on this film,  which was made in association with her company Via Rosa Productions. 
 Best Original Song ("For the First Time").

==Plot==
Melanie Parker (Michelle Pfeiffer) is an architect and divorced single mother to son, Sammy (Alex D. Linz). Her day gets off to a bad start when she is late to drop him off at school, due to the forgetfulness of fellow single parent Jack Taylor (George Clooney), a New York Daily News reporter whose daughter, Maggie (Mae Whitman), is thrust into his care that morning by his ex-wife.

The children arrive just a moment too late to go on a school field trip (a Circle Line boat cruise). Their parents are forced to accept that, on top of hectically busy schedules, they must work together that day to supervise each others child. In their confusion of sharing a taxi, they accidentally switch cellphones, causing each of them, all morning, to receive calls intended for the other one, which they then have to relay to the right person.  

Melanie must make an architectural design presentation to an important client. Jack has to find a source for a scoop on the New York mayors mob connections. Sammy causes havoc at Melanies office with toy cars, causing her to trip and break her scale model display. 

In frustration, Melanie takes Sammy to a daycare center (which is having a "Superhero Day"), where she coincidentally comes across Jack trying to convince Maggie to stay and behave herself. They create impromptu costumes for the children, utilizing his imagination and her resourcefulness. She takes her model to a shop to get quickly repaired. 

Having left for a meeting, Melanie panics when she receives a phone call from Sammy about another child having a psychedelic drug. She phones Jack in desperation and asks him to pick up both kids. He agrees, on the condition that she take over their care at 3:15 while he chases down a potential news source.

While in Melanies care, Maggie goes missing from a store, and wanders some distance down a crowded midtown sidewalk. Melanie breaks down in despair at the police station, files a missing child report, and then goes to a mayoral press conference to find Jack. He is notified by the police that Maggie has been found, and makes it to the press conference just barely in time to confront the mayor with his scoop about corruption. He had earlier tracked down its source, just as she was leaving a beauty salon in a limousine.

Although they have been antagonistic, Melanie and Jack work together to get both Sammy and Maggie, by taxi, to a soccer game. She insists that she will have time first to do her presentation to the new clients, despite him protesting that it will make them late for the game. She begins her pitch over drinks at the 21 Club lounge, but upon seeing Sammy in high spirits, she realizes that she cares more about him than her job. Bravely insisting that she must leave immediately to be with him, she fully expects to be fired, yet the clients are impressed.

At the game, Melanie meets her ex-husband, Eddie, who admits that he lied to Sammy and that he will be going on tour as a drummer with Bruce Springsteen. That evening, Jack wants a reason to visit Melanies apartment, so he takes Maggie to buy goldfish to replace the ones that were eaten earlier in the day by a cat. 

At Melanies apartment, the children watch TV while she and Jack share an awkward first kiss. She goes to the bathroom to freshen up; when she returns, an exhausted Jack is asleep on the sofa. She joins him and they fall asleep together.

==Cast==
* Michelle Pfeiffer as Melanie Parker
* Alex D. Linz as Sammy Parker
* George Clooney as Jack Taylor
* Mae Whitman as Maggie Taylor
* Charles Durning as Lew
* Jon Robin Baitz as Mr. Yates Jr.
* Ellen Greene as Mrs. Elaine Lieberman
* Joe Grifasi as Manny Feldstein
* Pete Hamill as Frank Burroughs
* Anna Maria Horsford as Evelyn
* Gregory Jbara as Freddy Sheila Kelley as Kristen
* Barry Kivel as Mr. Yates Sr.
* Robert Klein as Dr. Martin
* George N. Martin as Mr. Smith Leland
* Michael Massee as Eddie Parker
* Amanda Peet as Celia
* Holland Taylor as Rita
* Rachel York as Liza
* Marianne Muellerleile as Ruta
* Sidney Armus as Mayor Sidney Aikens

==Production==
Clooneys character did not exist in the scripts original draft. Producer Lynda Obst explained the change: "We were being incredible sexists. There are plenty of divorced, single working fathers going through the exact same thing." The studios initially wanted Kevin Costner or Tom Cruise to portray Jack Taylor but they passed and Clooney ultimately received the part. One Fine Day was filmed in 44 Manhattan locations. 

==Reception==
One Fine Day, as of August 18, 2003, holds a rating of 47% on Rotten Tomatoes, indicating a mixed critical reception.  It was considered a commercial disappointment by Twentieth Century Fox. 

Janet Maslin in the New York Times wrote:  "A 50s romp with a few glaring 90s touches (dueling cellular phones, frazzled single parents), One Fine Day makes for sunny, pleasant fluff. Both stars are enjoyably breezy, and theres enough chemistry to deflect attention from the storys endless contrivances... hes   such a natural as a movie star that he hardly needs false flattery. Ms. Pfeiffer, meanwhile, shows a flair for physical comedy."

Roger Ebert in the Chicago Sun-Times wrote:  "Cinema is the history of boys photographing girls. Or so Jean-Luc Godard is claimed to have said. I thought of his words while watching One Fine Day, an uninspired formula movie with another fine performance by Michelle Pfeiffer. She does everything in this movie that a much better movie would have required from her, but the screenplay lets her down... Pfeiffer looks, acts and sounds wonderful throughout all of this, and George Clooney is perfectly serviceable as a romantic lead, sort of a Mel Gibson lite. I liked them. I wanted them to get together. I wanted them to live happily ever after. The sooner the better."

Rita Kempley in the Washington Post wrote:  "Director Michael Hoffman, whose idiosyncratic portfolio includes the period comedy Restoration and the spoof Soapdish, sets a mellow pace and alternates old-fashioned split screen with crosscutting to enliven the many phone scenes. If the stars dont click, of course, nothing else matters. Happily, Pfeiffer and Clooney, now officially a movie star, not only click, they send off sparks."

Kenneth Turan in the Los Angeles Times wrote:  "One Fine Day is fortunate in its casting. Not only does it have Michelle Pfeiffer, whose gift for this kind of business was visible as far back as Married to the Mob and The Fabulous Baker Boys, but it marks the emergence of George Clooney as a major romantic star... Still, despite feeling like its moments have been micro-managed for maximum audience response, One Fine Day often passes for a pleasant diversion. But with actors so suited to each other, its too bad the film didnt give them more original material to work with."

Edward Guthmann in the San Francisco Chronicle wrote:  "Weve seen it before, but Pfeiffer and Clooney do everything in their power to make it seem fresh and delightful. Thats ultimately not enough, and even though the stars have some chemistry and Pfeiffer delivers her usual spotless performance, One Fine Day never manages to be more than a harmless, forgettable time-filler."

Rob Nelson in the Boston Phoenix wrote:  "Privilege and coincidence have always been central to screwball comedy, but the speed of crosstown travel here rivals Die Hard 3 for plausibility. And its these convenient shortcuts that waylay the film from examining the condition it purports to critique: that is, the 90s compulsion to drive at full throttle."

==Awards and nominations== Grammy Award for Best Song Written for a Motion Picture or Television.   

Michelle Pfeiffer won a Blockbuster Entertainment Award for Favorite Actress – Comedy/Romance, and was nominated for a Kids Choice Award for Favorite Movie Actress. 

Mae Whitman and Alex D. Linz were both nominated for Young Artist Awards in the categories of Best Performance in a Feature Film – Actress/Actor Age Ten or Under. Whitman won. 

==Soundtrack==
{{Infobox album | 
| Name        = One Fine Day: Music From The Motion Picture
| Type        = soundtrack
| Artist      = Various artists
| Cover       = 
| Released    = December 10, 1996
| Recorded    =
| Genre       = soundtrack
| Length      =
| Label       = Sony Records
| Producer    = Chronology   =
| Last album  =  
| This album  =
| Next album  =
}}
{{Album ratings
| rev1 = Allmusic
| rev1Score =    
| noprose = yes
}}
One Fine Day: Music From The Motion Picture is the soundtrack for the film. The album peaked at # 57 on The Billboard 200 in 1997.

===Track listing=== One Fine Day" – Natalie Merchant
# "The Boy from New York City" – The Ad Libs For the First Time" – Kenny Loggins Mama Said" – The Shirelles Someone Like You" – Shawn Colvin
# "Loves Funny That Way" – Tina Arena
# "Have I Told You Lately" – Van Morrison The Glory of Love" – Keb Mo
# "What a Diffrence a Day Made" – Tony Bennett
# "Isnt It Romantic?" – Ella Fitzgerald
# "This Guys in Love with You" – Harry Connick, Jr. Just like You" – Keb Mo One Fine Day" – The Chiffons
# "Suite From One Fine Day" – James Newton Howard

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 