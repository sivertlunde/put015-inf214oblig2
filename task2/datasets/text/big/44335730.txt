Ikkareyanente Manasam
{{Infobox film 
| name           = Ikkareyanente Manasam
| image          =  
| caption        = 
| director       = K. K. Haridas
| producer       = 
| writer         = Gireesh Puthenchery VC Ashok (dialogues)
| screenplay     = VC Ashok Mohini Jagathy Sreekumar Kalabhavan Mani
| music          = S. P. Venkatesh
| cinematography = Rajeev Madhavan
| editing        = G Murali
| studio         = Sree Ganapathy Movies
| distributor    = Sree Ganapathy Movies
| released       =  
| country        = India Malayalam
}}
 1997 Cinema Indian Malayalam Malayalam film, directed by K. K. Haridas. The film stars Prem Kumar, Mohini (Tamil actress)|Mohini, Jagathy Sreekumar and Kalabhavan Mani in lead roles. The film had musical score by S. P. Venkatesh.   

==Cast==
 
*Prem Kumar as Kuttiraman Mohini as Sumathi
*Jagathy Sreekumar as Pankajakshis Husband
*Kalabhavan Mani as Uthaman
*A. C. Zainuddin as Sumathis Uncle
*Rajan P Dev as Adv. Sreedharan
*Vijayakumar Chithra as Pankajakshi
*Harishree Ashokan as Thankappan
*Indrans as Bhargavan
*Kuthiravattam Pappu as Sumathis Uncle
*Mala Aravindan as Padminis Husband
*Zeenath as Padmini
*Machan Varghese as Pokkar
*Spadikam George as Pattalam Raghavan Usha as Malathi
 

==Soundtrack==
The music was composed by S. P. Venkatesh and lyrics was written by Gireesh Puthenchery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ilamaavin || KS Chithra || Gireesh Puthenchery || 
|-
| 2 || Kannilkkannil || MG Sreekumar, Chorus || Gireesh Puthenchery || 
|-
| 3 || Pallimukkile || Biju Narayanan, Mano || Gireesh Puthenchery || 
|}

==References==
 

==External links==
*  

 
 
 

 