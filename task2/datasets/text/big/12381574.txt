New Frontier (film)
:Not The New Frontier
{{Infobox film
| name           = New Frontier 
| image          = NewFrontier1939.jpg
| caption        = Theatrical poster
| director       = George Sherman
| producer       = William A. Berke
| writer         = Betty Burbridge   Luci Ward Jennifer Jones
| music          = 
| cinematography = Reggie Lanning
| editing        = Tony Martinelli
| distributor    = 
| released       =  
| runtime        = 57 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 Ray "Crash" Jennifer Jones. Three Mesquiteers Western B-movies The Searchers, True Grit. Jennifer Jones billed as Phylis Isley. The director was George Sherman.

==Cast==
* John Wayne as Stony Brooke
* Ray Corrigan as Tucson Smith
* Raymond Hatton as Rusty Joslin Jennifer Jones as Celia Braddock (billed as Phylis Isley)
* Eddy Waller as Major Steven Braddock
* Sammy McKim as Stevie Braddock
* LeRoy Mason as M.C. Gilbert
* Harrison Greene as William Proctor
* Wilbur Mack as Mr. Dodge
* Reginald Barlow as Judge Bill Lawson
* Burr Caruth as Dr. William "Doc" Hall Dave OBrien as Jason Braddock
* Hal Price as Sheriff Jack Ingram as Henchman Harmon
* Bud Osborne as Dickson

==See also==
* John Wayne filmography

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 