Paran Jai Jaliya Re
 
{{Infobox film
| name = Paran Jai Jaliya Re
| image =Poran Jai Jolia Re.jpg
| caption = Poster of Movie Paran Jai Jaliya Re
| director =Ravi Kinnagi
| writer = Dev Subhashree Ganguly
| producer =Shree Venkatesh Films
| distributor =
| cinematography =
| editing =
| released =  
| country        = India
| runtime     =155 minutes Bengali
| music = Jeet Ganguly
| budget         = 
| gross          = 
}} Bengali film Dev and Subhashree Ganguly.  The plot is lifted from 2007 film Namastey London.  Vipul shah filed a case against producers and they gave penalsty of lumsum 150&nbsp;million.The Film is a All Time Blockbuster.

==Plot== Bengali lad who falls head-on in love with Subhasree and their marriage is invariably finalized since Devs father is an old friend of Subhasrees Dad as well. However, although Subhasree evidently likes Dev, she hatches a plan to delay and finally, annul the engagement by implementing her brothers plan of bringing Dev to London and breaking up with him abroad. Poran Jai Jolia Re is the story of how Dev manages to win his love over all adversities in a foreign land.

Aritra Dutta Banik leaves his mark as Devs cousin, making the viewers laugh at almost every syllable. Dev shows prowess in his acting skills while Subhasree appears to have honed her acting skills post the success of Challenge (2009 film)|Challenge.  Also impressive are Biswajit as Subhasrees father and Tota Roy Choudhury as Shubhasrees brother.

The plot, which is a lift from 2007 Hindi film Namastey London by Vipul Shah, has certain Bengali nuances added that make it feel Bengali. However, it remains a mystery why the director wants to pass off Malaysia as London. Perhaps he had taken his inspiration a bit seriously.

==Cast== Dev – Raj
* Subhasree Ganguly- Anna
* Rahul Chatterjee- Vivek (Guest Appearance)
* Tota Roy Chowdhury- Sid
* Biswajit Chakraborty- Annas Father
* Labony Sarkar- Annas Mother
* Aritra Dutta Banik – রাজের মামাতো ভাই

==Controversy==
The producers of the movie were sued soon after the release of the film, following the revelation that the plot was lifted from 2007 film Namastey London. Producer Vipul Shah brought plagiarism charges against the film and the film was banned from screening twice, once by a lower court and later by Calcutta High Court. However, after the High Court decision, the producers came to a truce after Shree Venkatesh Films agreed to pay a "fine" of Seventy-five Lakh Rupees. This out of court settlement can be considered hefty since the making of the film took about 20&nbsp;million Rupees. Moreover, the settlement require the movie to display a title card stating Based on Namastey London, a film by Vipul Amrutlal Shah. 

==Critical reception==
The film got mostly negative critical reception, but viewer response was unprecedented since it earned the biggest opening ever in Bengali film history. Even though taken off the screens for a while, the film sustained on people memory thanks to the attractive cast and the negative publicity and started running to packed houses as soon as screenings restarted.
 Challenge and the biggest hit was the title track sung by Jeet Ganguly himself.

==References==
 

==External links==
* 
* 

 
 

 

 
 
 
 