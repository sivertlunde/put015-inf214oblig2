The Student Nurses
{{Infobox film
| name           = The Student Nurses
| image          =
| caption        =
| director       = Stephanie Rothman
| producer       = Charles S. Swartz Stephanie Rothman Roger Corman (uncredited) associate Paul Rapp
| writer         = Don Spencer
| based on       = original story by Stephanie Rothman Charles S. Swartz
| starring       = Brioni Farrell Elaine Giftos Barbara Leigh
| music          = Clancy B. Glass III
| cinematography = Stevan Larner
| editing        = Stephen Judson
| distributor    = New World Pictures
| released       =  
| runtime        = 89 mins
| country        = United States English
| budget         = $120,000 Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 18 
| gross          = over $1 million 
}}
The Student Nurses is a 1970 American film directed by Stephanie Rothman. It was the second film from New World Pictures and the first in the popular "nurses" cycle of exploitation movies. It has since become a cult film. 

==Plot==
Four young women all share a house together as they study to be nurses. Phred falls for a sexy doctor, Jim, despite accidentally sleeping with Jims roommate. Free-spirited Priscilla, who doesnt wear a bra, has an affair with a drug-selling biker who gets her pregnant and leaves her, causing her to seek an abortion. Sharon forms a relationship with a terminally ill patient. Lynn sets up a free clinic with a Hispanic revolutionary, Victor Charlie.
 Army Nurse goes on the run; Lynn decides to go with him. Phred breaks up with Jim but she and Priscilla agree to remain friends. The four friends graduate together.

==Cast==
*Elaine Giftos as Sharon
*Karen Carlson as Phred
*Brioni Farrell as Lynn
*Barbara Leigh as Priscilla
*Reni Santoni as Victor Charlie
*Richard Rust as Les
*Lawrence P. Casey as Jim Caspar
*Darrell Larson as Greg
*Paul Camen as Mark
*Richard Stahl as Dr. Warshaw
*Scottie MacGregor as Miss Boswell
*Pepe Serna as Luis
*John Pearce as Patient
*Mario Aniov as Ralpho
*Ron Gans as Psychiatrist

==Production==
The film was the second made for Roger Cormans new production and distribution company, New World Pictures following Angels Die Hard. The idea for the movie came from distributor Larry Woolner, who was involved in the establishment of New World; he suggested to Corman that he make a film about sexy nurses. Corman said the film:
 Took shape out of a formula I had been working on for some time: contemporary dramas with a liberal to left wing viewpoint and some R-rated sex and humour. But they were not to be comedies. I frankly doubt the left-wing bent, or message, was crucial to the success of the films we would do. But it was important to the filmmakers and me that we have something to say within the films... I insisted each (nurse) had to work out her problems without relying on a boyfriend. Roger Corman & Jim Jerome, How I Made a Hundred Movies in Hollywood and Never lost a Dime, Muller, 1990 p 181  
Corman allocated the project to the husband and wife team of Stephanie Rothman and Charles S. Swartz, who had worked on a number of different projects for Corman, including Its a Bikini World and Gas-s-s-s. Rothman would direct, and she and her husband would produce and provide the original story. Rothman:
 We made it while Roger was out of the country, directing a film of his own, Von Richthofen and Brown (1971), so we were free to develop the story of the nurses as we wished, as long as there was enough nudity and violence distributed throughout it. Please notice, I did not say sex, I said nudity. This freedom, once I paid my debt to the requirements of the genre, allowed me to address what interested me... political and social conflicts and the changes they produce. It allowed me to have a dramatized discussion about issues that were then being ignored in big-budget major studio films: for example, a discussion about the economic problems of poor Mexican immigrants... and their unhappy, restive children; and a discussion about a woman’s right to have a safe and legal abortion when, at the time, abortion was still illegal in America. I have always wondered why the major studios were not making films about these topics. What kind of constraints were at work on them? My guess is that is was nothing but the over-privileged lives, limited curiosity and narrow minds of the men, and in those days they were always men, who decided which films would be made.    
Rothman says when she started work on the movie she did not consider it an "exploitation film".
 I first learned the term when it was used in a review of The Student Nurses. I thought I was making low-budget genre films. But whatever I was making was called, it irritated me that many contemporary films, regardless of their budgets, were dishonest about everything from sexual politics to social conflict. Since I didnt know how long I would be able to work making any kind of film, I decided to say what I wanted while I had the chance, instead of playing it safe... The women in my films are independent in thought and action. I think this stands out because of the limited, usually subordinate roles, that were written for them in that era. It was indeed my intention to change that in my films. I wanted to create – as in the real world I wanted to see – a more equal and just balance of power between the sexes. That is why in some scenes the men are nude as well as the women, which definitely was not the convention then. But I wanted the womens independence to extend far beyond that issue to a life filled with meaning and purpose beyond marriage. Some women lived such lives in the Seventies, but it was a more novel idea in life and films than it is today.   accessed 25 Feb 2015  
It was the first big part for most of the cast, although Elaine Giftos had been in Gas-s-s-s. Barbara Leigh later wrote in her memoirs about her casting that, "Stephanie saw an innocence about me, which she liked. When she interviewed me, she asked me to expose my breasts, so she could see if they were worth photographing.   accessed 25 Feb 2015 

Rothman said the nudity was required because:
 The reason audiences came to see these low-budget films without stars was because they delivered scenes that you could not see in major studio films or more supposedly ambitious independent American films... My struggle was to try to dramatically justify such scenes and to make them transgressive, but not repulsive. I tried to control this through the style in which I shot scenes... Comedy was another method of control I used. I have always enjoyed writing and directing comedy– I was, in fact, more comfortable working in a comic idiom than a dramatic one–and so I also used comedy to modulate a scene’s tone."  

Barbara Leigh says her most memorable scene was when her character was nude on the beach, taking orange juice laced with acid from her lover (Richard Rust). When the scene was shot, Rust gave her orange juice laced with real acid. "I was very stoned on camera," she wrote. 

==Release==
===Critical===
The Los Angeles Times said the film "seems little more than a belated pilot film for The Interns with overheared loins and under-thunk narrative" but which is "never quite as bad as it threatens to be. A taste prevails, holds it in check and manages to make individual characters and relationships intermittently believable."  The Student Nurses on Citywide Screens
Mahoney, John C. Los Angeles Times (1923-Current File)   20 Nov 1970: h24.  

Daily Variety wrote that the movie "is a good contemporary dual-bill item about the varying romantic experiences of four novice nurses. The acting level is fair at best, which drags down what otherwise is a well-crafted film... Stephanie Rothmans physical direction is excellent... an exploitation item to be sure, but beyond those angles, general audiences will find a surprising depth. 

In later years, Pam Cook wrote that "viewed today, Student Nurses is a remarkable achievement. It is striking not only for its high production values, but also for its sophisticated discourse on 1970s politics - neither of which would necessarily be expected from an exploitation film. Also unexpected are the drug-induced fantasy sequences, which are more in line with the European art cinema than the rough and ready codes of exploitation. Rothmans inclusion of a relatively graphic abortion sequence, cut against scenes of one of the nurses having casual sex, still has a powerful impact." 
===Box Office===
The film was an enormous box office success, which Rothman admits took her by surprise, especially when men came to see it "in droves. It was a mystery to me. Only later did I discover that the nurse is a very erotic figure in most male fantasies. There was a wide market out there for films about women and a very responsive audience, not just men. Student Nurses was not a film designed exclusively for men. Many women came to see it." Tony Williams, Feminism, Fantasy and Violence: An Interview with Stephanie Rothman, Journal of Popular Film & Television 9. 2 (Summer 1981): 87. 

The movie helped establish New World Pictures as a production company and kicked off a cycle of similar films about young women having adventures while doing a familiar job:
*nurses - Private Duty Nurses (1971), Night Call Nurses (1972), The Young Nurses (1973), Candy Stripe Nurses (1974);
*teachers - The Student Teachers (1973), Summer School Teachers (1974);
*stewardesses - Fly Me (1973);
*models - Cover Girl Models (1975); Hollywood Boulevard (1976).
Rothman says Corman wanted to make a sequel but she was not interested.
 I had said all I wanted to about student nurses, and so he hired a series of male directors to make the sequels. I never watched them, so I cannot say if they contained any feminist ideas. But the lesson Roger derived from my film’s success was that you could make exploitation films whose narratives included contentious social issues, including feminism, and he consequently encouraged his directors to do it.  

==References==
 

==External links==
*  at IMDB
*  at Film Fanatic
 
 

 
 
 
 
 
 
 
 