Care Bears: Share Bear Shines
{{Infobox film
| name           = Care Bears: Share Bear Shines
| image          = Care Bears Share Bear Shines.jpg
| caption        = Australian (Region 4) DVD cover
| alt            = A purple bear with two lollipops on her stomach is riding upon a star. Beneath her, five others are standing in different poses on an icy surface.
| director       = Davis Doi
| producer       = Robert Winthrop Thomas Hart
| starring       = Tracey Moore Anna Cummer
| music          = Carl Johnson
| cinematography =
| editing        =
| studio         = SD Entertainment
| distributor    = 
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}}
Care Bears: Share Bear Shines is a 2010 computer animated, direct-to-DVD film featuring the Care Bear Power Team.  The film was produced by  .

==Cast==
* Tracey Moore - Share Bear
* Anna Cummer - Princess Starglo
* Tabitha St. Germain - Cheer Bear Ashleigh Ball - Oopsy Bear
* Ian James Corlett - Funshine Bear
* Scott McNeil - Grumpy Bear

==Plot==
The sunshine in Care-a-Lot is at stake when Princess Starglow, the princess of the stars, decides to shut down all of star related things forever.

==Release==
Share Bear Shines premiered on DVD in Australia on March 10, 2010 through Magna Pacific  In the United States, the film was released to select theaters on November 6, as part of Kidtoon Films matinee program,  and was released on DVD through Lionsgate in September 2011. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 