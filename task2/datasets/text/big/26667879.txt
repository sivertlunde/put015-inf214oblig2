Stella Does Tricks
{{Infobox Film
| name = Stella Does Tricks
| image = Stella_Does_Tricks_VideoCover.png
| caption =
| director = Coky Giedroyc
| producer = Adam Barker
| writer = A. L. Kennedy
| starring = Kelly Macdonald, James Bolam
| music = Nick Bicat
| cinematography = Barry Ackroyd
| editing = Budge Tremlett
| distributor = Strand Releasing
| released = 1996
| runtime = 97 minutes
| country = United Kingdom English
| followed_by =
| budget =
| gross =
}}
Stella Does Tricks is a 1996 film about a young Glaswegian girl Stella, played by Kelly Macdonald, working as a prostitute in London.

The film was the first feature film directed by   as a "bleak, perceptive portrait of the prostitute as a young girl torn between the need for genuine love and a career of sexual exploitation". van Gelder, Lawrence (2001) Stella Does Tricks in The New York Times Film Reviews 1999-2000, Routledge, ISBN 978-0-415-93696-5, p. 230-231 

Despite the film centering around the lives of female prostitutes, the only nudity in the film is male nudity. Leach, Jim (2004) British Film, Cambridge University Press, ISBN 978-0-521-65419-7, p. 142 

The screenplay was written by the novelist   collaborator Barry Ackroyd. McFarlane, Brian & Slide, Anthony (2003) The Encyclopedia of British Film, Methuen, ISBN 978-0-413-77301-2, p. 2, 251 

==Plot==
Stella is one of a number of young prostitutes working for the pimp Mr. Peters in London, having run away from her Glasgow home where she was sexually abused by her father, a stand-up comedian.   She tries to get away from Peters and becomes involved with Eddie, a heroin addict, before taking her revenge on Peters and her father.

==Cast==
*Stella - Kelly Macdonald
*Mr. Peters - James Bolam
*Eddie - Hans Matheson
*McGuire - Ewan Stewart
*Fitz - Andy Serkis

== External links ==
*  

==References==
 

 
 