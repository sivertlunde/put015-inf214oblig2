Burroughs: The Movie
 
{{Infobox film
| name           = Burroughs: The Movie
| image          = Burroughs_The_Movie_poster.jpeg
| caption        = Theatrical release poster
| director       = Howard Brookner
| producer       = Howard Brookner
| starring       = William S. Burroughs
| released       = 1983
| runtime        = 90 minutes
| country        = USA United Kingdom English
}}

Burroughs: The Movie is a 1983 documentary film directed by Howard Brookner about the beat generation writer William S. Burroughs.   

==Synopsis==
Burroughs: The Movie is the first and only documentary to be made about and with the full participation of writer William S. Burroughs. In a collaboration between Burroughs and director Howard Brookner the film explores Burroughs’ life story along with many of his contemporaries including Allen Ginsberg, Brion Gysin, Francis Bacon, Herbert Huncke, Patti Smith, Terry Southern, and Lauren Hutton.

Brookner managed to obtain 5 years of unparalleled access and enthusiastic participation from William S Burroughs. As a result Burroughs: The Movie documents Burroughs’ long, controversial and productive life in great detail. The film travels from the American Midwest to North Africa, through defining moments of his wildly unconventional life, including several personal tragedies, ultimately charting the development of Burroughs’ unique literary style.

==Production==
Howard Brookner began shooting the film in 1978 as his senior thesis at NYU; with Burroughs’ cooperation it subsequently expanded into a feature completed 5 years later in 1983. The film was shot by Tom DiCillo and the sound was recorded by Jim Jarmusch, fellow NYU classmates and both very close friends of Brookner’s.

Towards the end of shooting BBC Arena approached Howard Brookner and provided funds to complete the film. As a result the film was screened as part of the BBC Arena television series.
 Bloodhounds of Broadway.

==Cast==
* William S. Burroughs
* William S. Burroughs Jr
* Mortimer Burroughs
* Allen Ginsberg
* James Grauerholz
* Lauren Hutton
* Patti Smith
* Terry Southern
* Jackie Curtis
* Brion Gysin
* John Giorno
* Francis Bacon
* Herbert Huncke

==Release==
The film premiered at the 1983 New York Film Festival.
In Britain the film was released as part of the BBC Arena television series. It screened in both 1983 and was repeated following Burroughs’ death in 1997.

==Reception==
Burroughs: The Movie was released to generally positive reviews and was reviewed by some of the most influential film critics in the United States.

Janet Maslin of the New York Times said of the film: “The quality of discovery about Burroughs was very much the director’s doing, and Mr. Brookner demonstrates an unusual degree of liveliness and curiosity in exploring his subject.” 

Roger Ebert wrote “Burroughs is a documentary portrait of a man who was willing to try everything, and who has so far survived everything. The one thing you miss in the film is the sound of laughter.”   

==Restoration==
In 2012 Howard Brookner’s archive was discovered in a number of locations in both the United States and Europe.

The collection included 16mm film, reel to reel and mag sound, cassette tapes, 8mm film, VHS, video 8, and personal documents including letters, photos and writings. Some of the material contained never before seen out-takes of Burroughs: The Movie, including those with Andy Warhol, Patti Smith, Brian Jones and Brion Gysin.

On November 28, 2012 Howard Brookner’s nephew    in Bloomington, Indiana on February 6, a day after William S. Burroughs birthday. Aaron Brookner had a Q & A panel after the movie, and was also filming for his upcoming documentary entitled Uncle Howard.

The re-release of Burroughs: The Movie is due to coincide with celebrations of William S. Burroughs’ one-hundredth birthday, in the year 2014.   

==References==
 

==External links==
*  
*  
*   at  

 

 
 
 
 
 