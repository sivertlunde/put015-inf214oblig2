Thanatomorphose
{{Infobox film
| name           = Thanatomorphose
| image          = File:ThanatomorphoseFilmPoster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Éric Falardeau
| producer       = 
| writer         = Éric Falardeau
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Kayden Rose, Émile Beaudry, Eryka Cantieri, Roch-Denis Gagnon 
| music          = 
| cinematography = Benoît Lemire
| editing        = Benoît Lemire
| studio         = Black Flag Pictures, ThanatoFilms
| distributor    = Bounty Films
| released       =  
| runtime        = 100 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Thanatomorphose is a 2012 Canadian horror film directed by Éric Falardeau and his directorial debut.  It was first released on October 2, 2012, in Spain and has been shown at several film festivals since then, including the Fantasia Film Festival.  Thanatomorphose stars Kayden Rose as a young woman that finds her body slowly rotting from a mysterious ailment. 

==Synopsis==
The film follows Laura (Kayden Rose), a young woman that is incredibly unhappy with her life. Her career as an artist is going nowhere and shes trapped in an abusive relationship with her boyfriend. After one night of incredibly rough sex, Laura discovers several bruises on her body that begin to spread. She initially pays them no more heed than she would the bruises her boyfriend otherwise gives her, but as time passes the bruises begin to spread over her entire body. As her body begins to decay at an ever increasing rate, Laura begins to isolate herself and experience dreams filled with decay.

==Cast==
* Kayden Rose as Laura
* David Tousignant as Antoine
* Émile Beaudry as Julian
* Karine Picardas Anne
* Roch-Denis Gagnon as Stephan
* Eryka Cantieri as Marie
* Pat Lemaire
* Simon Laperrière

==Reception==
Critical reception for Thanatomorphose has been mixed,  and since its release the film has been compared to Contracted (film)|Contracted, a similarly plotted film that released in 2013.  Aint It Cool News gave a mixed review that praised the films special effects but warning that the film would not appeal to all audiences due to its content.  Fearnet gave a similar review, saying that it was "not the kind of horror film Id want to watch every week – and I may even find it difficult to recommend – but Id be lying if I said Thanatomorphose didnt fascinate, aggravate, and impress me at the same time."  Dread Central panned the movie, giving it one and a half blades and criticizing it as "all grue, little substance". 

===Awards===
*Best Movie Award, XXX Festival de Cine de Terror de Molins de Rei (2012)
*Best Special Effects Award, A Night of Horror International Film Festival (2012) 
*Best Film, Best Director, Best Actress, Most Repulsive Flick Awards, Housecore Horror Film Festival (2013) 
*Best Horror Film, The Phillip K. Dick Film Festival (2013) 
*Best Special Effects Award, Horrorant Film Festival FRIGHT NIGHTS (2014)

==See also==
* Contracted (film)|Contracted

==References==
 

==External links==
*  
*  

 
 
 
 
 
 