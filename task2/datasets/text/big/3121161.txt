Goodbye Uncle Tom
{{Infobox film
| name           = Goodbye Uncle Tom
| image          = Addio Zio Tom.jpg
| image_size     = 
| caption        = Japanese DVD art
| director       = Gualtiero Jacopetti  Franco Prosperi
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = Riz Ortolani
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1971 
| runtime        = 123 minutes (American version)   136 minutes (Italian version)
| country        = 
| language       = 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Goodbye Uncle Tom ( ) is a   or docudrama because of its fantasy framing device of the directors travelling back in time combined with the re-staging of historical events.

== Production ==
 extras as they required, and even a nightly dinner with Duvalier himself.  Hundreds of Haitian extras participated in the films various depictions of the cruel treatment of slaves, as well as white actors portraying historical characters (including Harriet Beecher Stowe).

== Different versions ==
 
The Directors cut|directors cut of Addio Zio Tom draws parallels between the horrors of slavery and the rise of the Black Power Movement, represented by Eldridge Cleaver, LeRoi Jones, Stokely Carmichael, and a few others.  The film ends with an unidentified mans fantasy re-enactment of William Styrons The Confessions of Nat Turner. This man imagines Nat Turners revolt in the present, including the brutal murder of the whites around him, who replace the figures Turner talks about in Styrons novel as the unidentified reader speculates about Turners motivations and ultimate efficacy in changing the conditions he rebelled against.  American distributors felt that such scenes were too incendiary, and forced Jacopetti and Prosperi to remove more than thirteen minutes of footage explicitly concerned with racial politics for American and other Anglophone audiences.

== Reception and criticism ==

The film has frequently been criticized as racist, despite directors Jacopetti and Prosperis claims to the contrary. In Roger Eberts 1972 review of the shorter American version, he asserts that the directors have, "Made the most disgusting, contemptuous insult to decency ever to masquerade as a documentary."   He goes on to call the film "Cruel exploitation," suggesting that the directors degraded the black actors playing slaves by having them enact the extremely dehumanizing situations the film depicts. Critic Pauline Kael called the film “the most specific and rabid incitement to race war,”   a view shared by white nationalist and former Ku Klux Klan leader David Duke, who claimed the film was a Jewish conspiracy to incite blacks to violence against whites. 

The directors denied charges of racism; in the 2003 documentary Godfathers of Mondo they specifically note that one of their intentions in making Addio Zio Tom was to "make a new film that would be clearly anti-racist" in response to criticism by Ebert and others over perceived racism in their previous film Africa Addio. 

The film was considered to have been a critical and commercial failure, and is currently out of print from Blue Underground. 

== Soundtrack ==

The film was scored by Italian composer Riz Ortolani and is notable for the theme "Oh My Love," sung by Katyna Ranieri, which would later be used in the soundtrack to the 2011 film Drive (2011 film)|Drive. Ortolani also collaborated with directors Jacopetti and Prosperi on their previous films, Mondo Cane, and Africa Addio.

==See also==
*List of films featuring slavery

==References==
 

== External links ==
*  
*   at The Internet Movie Database

 

 
 
 
 
 
 
 
 
 
 