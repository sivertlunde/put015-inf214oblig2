The Twist (film)
{{Infobox film
| name           = The Twist
| image          = The Twist film.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = Claude Chabrol
| producer       = Alexander Salkind Ilya Salkind Pierre Spengler  
| screenplay     = Claude Chabrol Ennio de Concini Norman Enfield
| based on       =  
| narrator       = 
| starring       = Bruce Dern Stéphane Audran Ann-Margret
| music          = Manuel De Sica
| cinematography = Jean Rabier
| editing        = Monique Fardoulis Central Cinema Company Film (CCC) Gloria
| distributor    = 
| released       = 23 June 1976
| runtime        = 107 minutes
| country        = France Italy West Germany
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Twist is a 1976 film co-written and directed by Claude Chabrol. Its title in French is Folies bourgeoises (literally - bourgeois madness).

==Plot==
The film follows a bored, upper class group of Parisians who embark on a series of affairs with each other.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Bruce Dern || William Brandels  
|-
| Stéphane Audran ||  Claire Brandels    
|-
| Sydne Rome ||  Nathalie  
|-
| Jean-Pierre Cassel || Jacques Lavolet, léditeur   
|-
| Ann-Margret || Charlie Minerva
|-
| Maria Schell || Gretel     
|-
| Sybil Danning || La secrétaire
|-
| Charles Aznavour ||  Dr. Lartigue
|-
| Curd Jürgens || Le bijoutier
|-
| Tomás Milián || Le détective
|-
| Claude Chabrol || Le client chez léditeur (uncredited)       
|}

==Critical reception==
Glenn Davidson of Turner Classic Movies:
 

Michael Barrett of PopMatters:
 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 