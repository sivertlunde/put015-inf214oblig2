They're a Weird Mob (film)
 
 
{{Infobox film
| name           = Theyre a Weird Mob
| image          = WeirdMob.jpg
| image_size     = 215px
| caption        = DVD cover
| director       = Michael Powell
| producer       = Michael Powell
| writer         = Emeric Pressburger (as Richard Imrie) novel by John OGrady
| starring       = Walter Chiari Chips Rafferty Clare Dunne
| music          = Score: Alan Boustead    Arthur Grant
| editing        = Gerald Turney-Smith
| studio = Williamson-Powell International Films
| distributor    = British Empire Films (Aust) Rank Organisation (UK)
| released       =   (premiere-Sydney)   (Aus.-general) October 1966 (UK)
| runtime        = 112 minutes
| country        = Australia
| language       = English
| budget         =   (est.)
| gross          = $2,000,000 (Australia by 1968) Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p238 
}} novel of John OGrady Michael Powell and Emeric Pressburger.

==Plot==
Nino Culotta is an Italian Post war migrant arrivals, Australia|immigrant, newly arrived in Australia. He expected to work for his cousin as a sports writer for an Italian magazine. However on arrival in Sydney Nino discovers that the cousin has abandoned the magazine, leaving a substantial debt to Kay Kelly. Nino declares that he will get a job and pay back the debt.
 Australian slang culture of the 1960s. Nino endeavours to understand the aspirational values and social rituals of everyday urban Australians, and assimilate. A romantic attraction builds between Nino and Kay despite her frosty exterior and her conservative Irish fathers dislike of Italians. 

A tone of mild racism exists in the film between Anglo-Saxon/Anglo-Irish characters such as Kay Kellys dad Harry (Chips Rafferty) and Nino. Harry says he doesnt like writers, brickies or  . Nino is all three. But this is undermined when Nino, sitting in the Kelly house notices a picture of the pope on the wall. Nino says "If Im a dago, then hes a dago". Realising the impossibility of referring to the pope by that derogatory term, Harry gives in.

==Cast==
 
*Walter Chiari as Nino Culotta
*Clare Dunne as Kay Kelly
*Chips Rafferty as Harry Kelly
*Alida Chelli as Giuliana
*Ed Devereaux as Joe Kennedy
*Slim DeGrey as Pat
*John Meillon as Dennis
*Charles Little as Jimmy
*Anne Haddy as Barmaid Jack Allen as Fat Man in Bar
*Red Moore as texture man
*Ray Hartley as newsboy
*Tony Bonner as lifesaver
*Alan Lander as Charlie
*Keith Peterson as drunk man on ferry
*Muriel Steinbeck as Mrs Keith
*Gloria Dawn as Mrs Chapman
*Jeanie Drynan as Betty
*Gita Rivera as Maria 
*Judith Arthy as Dixie
*Doreen Warburton as Edie
*Barry Creyton as hotel clerk
*Graham Kennedy as himself (cameo)
*Robert McDarra as hotel manager
*Noel Brophy
*Jacki Weaver 
 

===Cast notes=== John OGrady, the author of the novel, makes a cameo appearance as the grey-bearded drinker in the pub in the opening sequence of the film. 
*Alida Chelli was the girlfriend of Walter Chiari, but almost did not get the part because she was thought to be too glamorous and might have upstaged Clare Dunne. 

==Production==
Theyre a Weird Mob was optioned in 1959 by Gregory Peck but he could not come up with a workable screenplay. Michael Powell first read the novel in London in 1960 and wanted to turn it into a film but Peck had the rights. Powell obtained them three years later and brought in his long-time collaborator Emeric Pressburger, who wrote the screenplay under the pseudonym "Richard Imrie."   

Walter Chiari had previously visited Australia during the filming of On the Beach (1959), which starred his then-girlfriend Ava Gardner. Clare Dunne was working as a weather girl when cast in the female lead. 

The film started filming in October 1965 was shot at a number of locations in the area of Sydney:  Bondi Beach Circular Quay (where the ferry comes ashore) Clark Island (the beach party) Hunter Street Elizabeth Street in the central business district Martin Place (where Graham Kennedy asks Nino for directions)
*Manly Beach Neutral Bay (final scene shot at 9 Wallaringa Ave, Neutral Bay) The Royal Life Saving Society. The stars footprints were set in concrete slabs in the pathway.   
*Punchbowl railway station, where Nino is picked up by Joe prior to his first day at work has changed over the years. In a previous configuration it was possible to park a vehicle virtually at the bottom of the northern steps. Balgowlah Heights The place where Nino & Kay want to build their home is referred to in the "making of " documentary as Grotto Point. Balgowlah Heights is on Dobroyd Head on the north side of the entrance to Middle Harbour.
 Australian film industry, which led to the Australian "New Wave" films of the 1970s. 

==Box office==
Theyre a Weird Mob grossed $2,417,000 at the box office in Australia,  which is equivalent to $26,127,770 in 2009 dollars.

In 1968 John McCallum wrote that of the $2 million the film had then earned, only $400,000 had been returned to the filmmakers. 

A behind-the-scenes documentary was shot called  The Story of the Making of Theyre a Weird Mob . 

==DVD==
The film has been released on Region 4 DVD by Roadshow. The DVD includes a TV special, "The Story of Making the Film Theyre a Weird Mob" as well as a picture gallery, theatrical trailer and optional subtitles.

The film has been released on Region 2 DVD by Opening in the Les films de ma vie series. The DVD has fixed French subtitles for the original English soundtrack.

==See also==
*Cinema of Australia

==References==
 

==External links==
*  
*  
*   at the  
*  
*   at Australian Screen Online
*  at Oz Movies
* 

 

 
 
 
 
 
 
 
 
 
 
 