Luke's Newsie Knockout
 
{{Infobox film
| name           = Lukes Newsie Knockout
| image          =
| caption        =
| director       = Hal Roach
| producer       = Hal Roach
| writer         =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        =
| country        = United States Silent English intertitles
| budget         =
}}
 short comedy film starring Harold Lloyd.   

==Cast==
* Harold Lloyd as Luke
* Bebe Daniels
* Snub Pollard Charles Stevenson (as Charles E. Stevenson)
* Billy Fay
* Fred C. Newmeyer
* Sammy Brooks
* Harry Todd
* Bud Jamison
* Margaret Joslin (as Mrs. Harry Todd)
* Earl Mohan
* Leon Leonhardt
* Harvey L. Kinney
* Ray Thompson
* Hilda Limbeck
* Sidney Fiske
* Eva Thatcher
* H.L. OConnor
* Estelle Short (as Estella Short)

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 