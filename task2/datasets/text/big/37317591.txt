Yak: The Giant King
{{Infobox film
| name           = Yak: The Giant King
| image          = YAK The Giant King.jpg
| caption        = Thai theatrical release poster
| director       = Prapas Cholsaranont Chaiporn Panichrutiwong
| producer       = Prapas Cholsaranont
| writer         = Prapas Cholsaranont
| story          = Kreadtisuk Udomnak
| starring       = suntisuk pormmasiri Kreadtisuk Udomnak Boribroon Junrieng Weranut Tippayamontol Pawanrat Naksuriya Caninap Sirisawut Udom Tarpanich
| music          = Jakkrapat Iamnoon
| cinematography = Manuel Sicilia
| editing        = Smith Timsawat Punlop Sinjaroen Prapas Cholsaranont  Workpoint Picture
| distributor    = Sahamongkol Film International Workpoint Picture Eagle Entertainment (DVD)
| released       =  
| runtime        = 96 minutes
| country        = Thailand
| language       = Thai
| budget         = 100,000,000 Baht การะเกด. เจาะเฟรม. สยามรัฐสัปดาห์วิจารณ์. ปีที่ 60 ฉบับที่ 4. วันศุกร์ที่ 12 - วันพฤหัสบดีที่ 18 ตุลาคม พ.ศ. 2555. ISSN 0125-0787. หน้า 82 
| gross          = $1,668,124
}} 2012 Thai 3D computer-animated Workpoint Picture. Thai version of the Ramayana, with robots for the main characters.  
November 29, 2012  Literary reputation in Asia for the last 6 years and budget over 100 million baht. It was released on 4 October 2012

==Plot==
A re-interpretation of Ramayana, the Thai animation film tells the story of a giant robot, Na Kiew, whos left wandering in a barren wasteland after a great war. Na Kiew meets Jao Phuek, a puny tin robot whos lost his memory and is now stuck with his new big friend. Together they set out across the desert populated by metal scavengers, to look for Ram, the creator of all robots.

==Cast==
* suntisuk pormmasiri 
* Kreadtisuk Udomnak 
* Boribroon Junrieng
* Weranut Tippayamontol
* Pawanrat Naksuriya 
* Caninap Sirisawut 
* Udom Tarpanich
* Bawriboon Chanreuang
* Santisuk Promsiri
* Udom Taephanit
* Kerttisak Udomnak 
* Chris Wegoda
===Characters===
* Kumbhakarna
* Na Khiaw
* Ravana
* Brooks
* Phuak
* Hanuman
* Guard
* Father
* French Construction Worker
* Mexican Worker

==Awards==
The Best Directors are Prapas Cholsaranont and Chaiporn Panichrutiwong, The Best Score is Jakkrapat Iamnoon, The Best Editors are Smith Timsawat, Punlop Sinjaroen and Prapas Cholsaranont, The Best Art Direction is Chaiporn Panichrutiwong, The Best Sound is made by Richard Hocks and The Best Original Song is "Kerd Ma Pen Phuen Ther" performed by Apiwat Eurthavornsuk.

==Movie and Storyline==
The makers for the film in Sound Department are Sergei Groshev, Richard Hocks, Jérémy Rodeschini and Warren Santiago. The makers for the film in Animation Department are Watcharadon Chanachina and Suwajana Noochniyom Janajina and the makers for the film in Editorial Department and Other Crew are Steve Calalang and Chris Wegoda. The Music for the film was composed by Jakkrapat Iamnoon, The film is directed by Prapas Cholsaranont	and it stars Bawriboon Chanreuang, Santisuk Promsiri, Udom Taephanit, Kerttisak Udomnak and Chris Wegoda. A re-interpretation of Ramayana, the Thai animation film tells the story of a giant robot, Na Kiew, whos left wandering in a barren wasteland after a great war. Na Kiew meets Jao Phuek, a puny tin robot whos lost his memory and is now stuck with his new big friend. Together they set out across the desert populated by metal scavengers, to look for Ram, the creator of all robots. The storyline for the film is written by Anonymous. The film is produced by Work Point Entertainment and distributed by Sahamongkol Film International and Eagle Entertainment. Also known names for the film are called The Giant King and Yak.

==Release==
The film was theatrically released on 2012 and 2013 in Thailand and Russia in cinemas and in Australia and New Zealand at the DVD Premieres.

==Theme song==
* 
**Artist: ROOM 39
**Composition: Stamp Interview. เซนชู อนิเมแมกกาซีน. Vol. 58. September 2012. TS Interprint. หน้า 86-88 

==Home media==
Yak: The Giant King was released on DVD by Eagle Entertainment on December 28, 2013.

==References==
 

==External links==
* Official website
* Yak: The Giant King at the Internet Movie Database
* Yak: The Giant King at Rotten Tomatoes

 
 
 
 
 
 
 


 
 