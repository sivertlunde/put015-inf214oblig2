Cracking Up (film)
{{Infobox film
| name = Cracking Up
| image = smorgasbordbelgian.jpg
| caption = Theatrical Poster for the Belgian Release (Under its original title)
| director = Jerry Lewis Bill Richmond
| starring = Jerry Lewis Herb Edelman Zane Buzby
| producer = Peter Nelson Arnold Orgolini
| distributor = Warner Brothers
| released =  
| runtime = 83 minutes
| language = English
| country = United States gross = 553,259 admissions (France)   at Box Office Story 
}}
Cracking Up is a comedy film directed by and starring Jerry Lewis. Originally titled Smorgasbord, it was filmed in 1981-82 and only received limited distribution in the United States. It is the last film to be directed by Lewis to date.
 Bill Richmond, The Patsy.

==Plot==
Warren Nefron (Jerry Lewis) is a klutz who cannot do anything right.  He tells his psychiatrist, Dr. Pletchick (Herb Edelman), his problems.  Through a series of flashbacks the viewer sees Nefrons life story.

Warren is such a failure that even his many attempts to commit suicide fail.  Eventually the psychiatrist uses hypnosis to cure Warren.  However, although Warren is now cured, the psychiatrist has experienced transference and now has all of Nefrons problems.

The story ends with Warren and a young woman attending a film called Smorgasbord.

==Production==
It includes many cameos, including Sammy Davis Jr., Dick Butkus, and Milton Berle.  In December 1982, after filming completed, Lewis underwent coronary artery bypass surgery|triple-bypass heart surgery at the Desert Springs Hospital, Las Vegas NV.

==Release==
The film was released theatrically in some European countries, notably France (where it was released on April 13, 1983 by Warner Bros.), Belgium and Italy. It was previewed in the U.S. in Wichita, Kansas (Lewis previous film had done well there ) under its original title Smorgasbord but was not subsequently released in the U.S., going  directly to cable and on videocassette.  In May 1985 it was given a two-day run at New Yorks Thalia Theater, again under its original title, and then played a smattering of revival houses, art cinemas, and film festivals.

==Home media==
Warner Archive released the film on made-to-order DVD in the United States on May 18, 2010.

==Cast==
* Jerry Lewis as Warren
* Herb Edelman as the Psychiatrist
* Zane Buzby as the Waitress
* Milton Berle as the Lady (voiced by Ruta Lee)
* Foster Brooks as the Pilot
* Dick Butkus as Anti-Smoking Enforcer
* Francine York as Marie
* Sammy Davis, Jr. as Himself
==References==
 
==External links==
* 

 

 
 
 
 
 


 