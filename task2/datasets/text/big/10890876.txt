Aadmi Sadak Ka
{{Infobox film
| name           = Aadmi Sadak Ka
| image          = Aadmi Sadak Ka.jpg
| image_size     = 
| caption        = 
| director       = Devendra Goel
| producer       = Devendra Goel
| writer         = Mushtaq Jalili
| narrator       =  Vikram Zaheera Deven Verma Ravi
| cinematography = Keki Mistry
| editing        = R.V. Shrikhande
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1977 Bollywood film. Produced  and directed by Devendra Goel it stars Shatrughan Sinha, Vikram, Zaheera, Deven Verma, Asit Sen and Agha. The films music is by Ravi (music director)|Ravi.

==Plot==

The Nath family consists of Retired Commissioner Upendra, his wife, Savitri; elder son, Madan, who is married to Maya, and they have a son, Ashoo; a second son - Surendra, who is married to Kamla, and they have a daughter, Pinky; a third unmarried college-going son, Chander; and a school-going daughter, Namrata. They are a happy family, and everyone rejoices when Chander completes his M.A.. There is more to rejoice when Chander introduces them to the woman he loves, Vandana, the only daughter of wealthy Mr. Tandon. The marriage is arranged with a ceremony, attended by the family friend, Abdul, who home-delivers groceries.

However, before the marriage could be sealed, Upendra receives news that he has lost his Court case, and passes away. The wedding is canceled, and the lives of the entire Nath family changes thereafter, with Kamla and Maya taking over the household, reducing Savitri to the status of an unwanted guest, while Namrata is asked to become the servant and unable to complete her education, and Chander, who rebels, is asked to leave. Vandanas dad comes to know of their plight and refuses to permit his daughter to get married in the Nath family. Chander moves in with Abdul, gets a job as a waiter, then he is promoted as Manager, and finally as partner and co-owner of the Francis Hotel. Unable to get medication, Savitri passes away, forcing Namrata to move in with Abdul as well. Madan and Surendra also find success when they buy their own hotel and name it Gulmohar Hotel. With Chander on one hand, and his two siblings on the other - they are now poised to compete with each other - not knowing who will win in this cut-throat competition - as they set out to destroy each other.

==Cast==
*Shatrughan Sinha ...  Abdul Vikram ...  Chandramohan Chander U. Nath
*Zaheera ...  Vandana Tandon
*Sujit Kumar ...  Madanmohan Madan U. Nath
*Deven Verma ...  Surendramohan Suren U. Nath Asit Sen ...  Kundanlal David Abraham... Rustom Agha ...  Bus driver - Gulmohar Hotel
*Joginder Shelly Mumtaz Begum ...  Abduls mom
*Padma Chavan ...  Kamla S. Nath
*Sumati Gupte ...  Savitri U. Nath
*Gajanan Jagirdar...  Retd. Commissioner Upendra Nath
*Pallavi Joshi...  Pinky S. Nath (as Pallavi)
*Ritu Kamal ...  Maya M. Nath
*Pinchoo Kapoor ...  Tandon

==Soundtrack==
The music was written by Ravi and the lyrics were written by Verma Malik.
#"Aaj Mere Yaar Ki Shaadi Hai" (Ravi, Mohammed Rafi, Deven Varma)  
#"Bura Na Mano Yaar Dosti Yaari Men" (Anuradha Paudwal, Mohammed Rafi, Deven Varma)
#"Basti Basti Nagri Nagri" (Asha Bhosle, Mohammed Rafi)
#"Aadmi Sadak Ka" (Mohammed Rafi) 
#"Aaj Mere Yaar Ki Shaadi Hai (Revival)" (Mohammed Rafi, Deven Varma)

== External links ==
*  

 
 
 