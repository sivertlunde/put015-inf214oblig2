Shadows of the Night (film)
 
{{Infobox film
| name           = Shadows of the Night
| image          = 
| alt            = 
| caption        = 
| director       = D. Ross Lederman
| producer       = 
| screenplay     = Robert E. Hopkins D. Ross Lederman 
| story          = Ted Shane 
| starring       = Flash the Dog Lawrence Gray Louise Lorraine Warner Richmond Tom Dugan
| music          =
| cinematography = Max Fabian
| editing        = Dan Sharits 	
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 drama silent film directed by D. Ross Lederman and written by Robert E. Hopkins and D. Ross Lederman. The film stars Flash the Dog, Lawrence Gray, Louise Lorraine, Warner Richmond and Tom Dugan. The film was released on October 26, 1928, by Metro-Goldwyn-Mayer.  

== Cast ==
*Flash the Dog as Flash
*Lawrence Gray as Jimmy Sherwood
*Louise Lorraine as Molly
*Warner Richmond as	Feagan
*Tom Dugan as Connelly
*Alphonse Ethier as OFlaherty
*Polly Moran as Entertainer

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 