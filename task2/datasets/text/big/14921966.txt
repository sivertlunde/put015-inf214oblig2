The Right to Remain Silent
{{Infobox film
| name           = The Right To Remain Silent
| image          = Righttoremainsilent.jpg|
| caption        = The Right To Remain Silent VHS Cover|
| director       = Hubert C. de la Bouillerie|
| producer       =
| writer         = Mark Fauser   Brent Briscoe
| starring       = Robert Loggia   Lea Thompson
| distributor    = |
| released       = 1996|
| runtime        = 97 min.|
| country        =  |
| language       = English language|English|
|
}} play by 1996 starring Robert Loggia and Lea Thompson.

==Synopsis==
It was supposed to be just a normal night at the police station for rookie cop Christine Paley (Lea Thompson|Thompson). This is a report of about eight different types of arrests which can happen in a normal month. Lt. Mike Brosloe (Robert Loggia|Loggia) leads her through one of the most unusual first shifts.

==Main cast==
*Robert Loggia as Mike Brosloe
*Lea Thompson as Christine Paley
*LL Cool J as Charles Red Taylor
*Amanda Plummer as Paulina Marcos
*Christopher Lloyd as Johnny Benjamin
*Laura San Giacomo as Nicole Savita
*Fisher Stevens as Dale Myerson
*Judge Reinhold as Buford Lowry
*Patrick Dempsey as Tom Harris
*Carl Reiner as Norman Friedler

==Awards==
Nominated for 2 Cable Ace Awards, 1 win.

==External links==
* 
*  at Markfauser.com

 
 
 
 
 
 


 