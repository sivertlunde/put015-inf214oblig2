Le Fils de Gascogne
Le Fils de Gascogne (Gascognes Son) is a French film directed by Pascal Aubier from a scenario by Patrick Modiano and Pascal Aubier, released on 8 May 1996 in film|1996.

The adventures of Harvey, guide of a group of Georgian singers in Paris, and Dinara their interpreter, are used as a framework to provide a fantastic and nostalgic evocation of the cinema of the 1960s.

==Synopsis==
Harvey, a timid provincial young mans, accommodate a troop of  , my mother is half Russian, half Jewish”), and acts as their guide in Paris.

In a restaurant,a customer joins the singers and claims to recognize Harvey as the son of a script writer named Gascogne, who died twenty-five years earlier in an automobile accident. Marco, one of his friends, tries to profit from this situation, made all the easier as Harvey was born from an unknown father. He tries to find the reels of a film which Gascogne made just before his death in order to sell them to a collector.

During this search, he introduces a group of 1960s stars to the two young people, despite the initial scepticism of Harvey and the growing mistrust of Dinara.

==Cast==
* Grégoire Colin (Harvey) Dinara Droukarova (Dinara)
* Jean-Claude Dreyfus (Marco)
* László Szabó (actor)|László Szabó (the restaurant client)
* Pascal Bonitzer (Hiblen, the authorised representative)
;As themselves
* Yves Afonso
* Anémone
* Stéphane Audran
* Jean Benguigui
* Jean-Claude Brialy
* Claude Chabrol
* Michel Deville
* Stéphane Freiss
* Otar Iosseliani
* Bernadette Lafont
* Valérie Lalonde
* Richard Leacock
* Patrice Leconte
* Macha Meril
* Bulle Ogier
* Marie-France Pisier
* Jean Rouch
* Alexandra Stewart
* Marina Vlady
* etc.

==External links==
 

 
 
 


 