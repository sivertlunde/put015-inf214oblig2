Small Hotel
{{Infobox film
| name           = Small Hotel
| image          =
| caption        =  David Macdonald
| producer       = Robert Hall
| writer         = Wilfred Eades Rex Frost (play)
| starring       = Gordon Harker Marie Lohr Janet Munro
| music          = 
| cinematography = Norman Warwick
| editing        = Seymour Logie    Associated British-Pathé
| released       = October 1957
| runtime        = 59 min
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    =
}}
 1957 Cinema British comedy film directed by David MacDonald. 

==Plot==
A headwaiter in a country hotel works various scams after he is told that he is to be replaced by a young woman.

==Cast==
*Gordon Harker.....Albert
*Marie Lohr.....Mrs. Samson-Fox John Loder.....Mr. Finch
*Irene Handl.....Mrs. Gammon
*Janet Munro.....Effie
*Billie Whitelaw.....Caroline Mallet
*Ruth Trouncer.....Sheila Francis Matthews.....Alan Pryor
*Frederick Schiller.....Foreigner
*Derek Blomfield.....Roland

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 