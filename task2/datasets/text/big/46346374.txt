The House of the Arrow (1940 film)
{{Infobox film
| name = The House of the Arrow 
| image =
| image_size =
| caption =
| director = Harold French
| producer = Walter C. Mycroft
| writer =  A.E.W. Mason (novel)   Doreen Montgomery    
| narrator = Diana Churchill   Belle Chrystall   Peter Murray-Hill
| music = Harry Acres
| cinematography = Walter J. Harvey
| editing = Edward B. Jarvis 
| studio = Associated British Picture Corporation 
| distributor = ABPC 
| released = 26 October 1940 
| runtime = 66 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Diana Churchill The House of the Arrow featuring the French detective Inspector Hanaud.

==Cast==
* Kenneth Kent as Inspector Hanaud   Diana Churchill as Betty Harlowe  
* Belle Chrystall as Ann Upcott  
* Peter Murray-Hill as Jim Frobisher  
* Clifford Evans as Maurice Thevenet  
* Louise Hampton as Mme. Harlow  
* Catherine Lacey as Francine Rollard  
* Aubrey Dexter as Giradot 
* James Harcourt as Boris Raviart 
* Ivor Barnard as Jean Cladel 
* Athene Seyler 

==References==
 

==Bibliography==
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 