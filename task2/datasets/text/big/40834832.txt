Battle of the Damned
 
{{Infobox film
| name           = Battle of the Damned
| image          = Battle of the Damned (2013) Movie poster.png
| caption        = Theatrical poster 
| director       = Christopher Hatton
| producer       = Ehud Bleiberg Christopher Hatton Leon Tong
| writer         = Christopher Hatton
| starring       = {{plainlist|
* Dolph Lundgren
* Melanie Zanetti
* Matt Doran David Field
}}
| music          = {{plainlist|
* Joe Ng Ting
* Si Hao
}}
| cinematography = Roger Chingirian
| editing        = Danny Rafic
| studio         = Bleiberg Entertainment
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
}} David Field.     It is the sequel to the 2011s Robotropolis. Following a deadly viral outbreak, private soldier Max Gatling (Lundgren) leads a handful of survivors and a team of robots in a fight against an army of the dead.

Lundgren said of the film, "This time Im up against virus-infected human zombies, Eaters as we call them. Max is sent into an infected, abandoned city to find the daughter of a rich industrialist. He gets more than he bargained for. I wish I wouldve asked for more money, in Maxs own words. Fortunately I enlist some kick-ass, run-away robots to help me in the battle". 

==Plot==
 
A group of mercenaries led by Maj. Max Gatling are fleeing from a failed rescue attempt, having been hired by a rich industrialist to enter a zombie-infested city to rescue his daughter Jude. At the extraction point, Gatling tells Smiley that he is staying to find Jude and complete the mission. After searching through the city and killing many zombies on the way, he locates Jude and tells her that he is going to get her out of the city, and she takes him to other survivors. There, he is introduced to the survivors including group leader Duke, Reese, Elvis, Lynn, and Anna. Jude is revealed to be in a relationship with Reese and pregnant with his child. The next day when Gatling, Jude, and the others go out to retrieve gasoline from a gas station, Gatling attempts to get Jude out of the city by force when she refuses to go without the others. However, he is stopped by the others and left for dead. After discovering that Jude is pregnant, Reese rushes to Gatlings aid so that Jude can be taken out of the city. At this point it is revealed that not only is Judes father some rich industrialist, but he is also the one who caused the zombie pandemic. While returning to the stronghold to get Jude, Gatling and Reese discover a band of non-programmed robots. Gatling reprograms the robots and proceeds to the stronghold. When it is revealed that the city is to be firebombed, Gatling and all of the survivors make preparations to escape.

On the day of the escape, Gatling, Jude, the survivors, and the robots shoot down and kill many zombies along the way. When taking defensive positions while being attacked at a junkyard, Duke ditches them all and hides away. The robots also malfunction and one begins to attack Gatling. Gatling destroys the robot, and the rest of them reboot. Gatling, Jude, Reese, and the remaining survivors escape the junkyard in a car being pushed by the remaining robots. Duke is left behind and eaten by zombies. Gatling and the others then arrive at a car parking complex and go underground while the robots hold back the zombies. Once the Zombies start swarming the car park, Gatling and the others struggle to fight them off. Gatling finds that one of the robots is still functioning and he sends it to find Jude.

As the firebombing starts and reaches the complex, Gatling, Jude, and Reese jump into a partly flooded level of the complex to escape the fire. Once the firebombing ceases, Jude and Reese leave the complex, and all of the city is shown to be entirely destroyed. They meet up with Gatling, who reveals to Reese that a part of his mission is not only to take Jude home, but to kill anybody that can whistle-blow on the outbreak. But knowing that Jude and Reese are expecting, Gatling decides to let Reese live and the three of them then leave together with the last surviving robot who Max asks to get him a coffee.

==Cast==
* Dolph Lundgren as Major Max Gatling
* Melanie Zanetti as Jude
* Matt Doran as Reese David Field as Duke
* Jen Kuo Sung as Elvis
* Lydia Look as Lynn
* Oda Maria as Anna
* Jeff Pruitt as Smiley
* Kerry Wong as Dean
* Esteban Cueto as Hernandez
* Broadus Mattison as Broadus
* Timothy Cooper as Robot (Voice)

==Release==
The film was released on December 26, 2013, in the United Kingdom    and February 18, 2014 in the United States. 

==Reception== The Walking Dead, Battle of the Damned takes the genre back to its grass roots of horror." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 