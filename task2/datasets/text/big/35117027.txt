Illusions by Julie Dash
{{Infobox film
| name           = Illusions
| image          =
| image_size     =
| caption        = Opening Scene
| director       = Julie Dash
| producer       = Julie Dash
| writer         = Julie Dash Roseann Katon Ned Bellamy Jack Radar
| music          = Chick Webb and his orchestra
| cinematography = Ahmed El Maanouni
| editing        =
| distributor    = Black Filmmaker Foundation, Women Make Movies, and Third World Newsreel (New York); Circles-Women’s Films and Video Distribution (London); Le Soliel O (Paris). 
| released       =  
| runtime        =
| country        = United States English
| running time   = 34 minutes
| film stock     = Black and white
| followed_by    =
}}
Illusions is a 1982 film written and directed by Julie Dash. The short film depicts the life of an African American woman passing as a white woman working in the film industry during the 1940s. It calls attention to the lack of African Americans in the film industry during that era. Taylor, Clyde. "Illusions Introduction." Freedomways 23.4 (1983): 184.  Klotman, Phyllis. Screenplays of African American Experience. Script and background information on Illusions.  Gibson-Hudson, Gloria. "Ties that Bind: Cinematic Representations by Black Women Filmmakers", Black Women Film and Video Artists. Ed. Jacqueline Bobo. London:Routledge, 1998:3-19. 

==Plot==
According to film scholar and critic Clyde Taylor,  “Dashs film plays inventively on themes of cultural, sexual and racial domination.”   The film is set in Hollywood in 1942, a time when the role of the film industry was to create an illusion for the audience to believe in. This illusion was based on the creation of American history in films; what is seen on screen is usually what they want you to believe and not actually the truth.  Made during a time of heavy war propaganda, Hollywood created its own version of America and its freedoms.

Illusions follows a young woman working at National Studios in Hollywood, very rare during that time. As the film progresses and we see this woman, Mignon Dupree, create the illusion of talent among white film stars while a young African American girl, Esther Jeeter, actually sings the part for the film. Dubbing of voices was not uncommon during those times but the use of dubbing voices using African American stars for a white performer and audience only shows the great divide between the races during that time.

Over the course of the film Mignon talks a lot about the illusion of Hollywood and the different levels of society that it creates in film and in real life.  It is hinted throughout the movie, like when Esther tells Mignon, “Oh dont worry....they cant tell like we can,” that there is a secret that she is hiding.  It is not until the final scene when the Lieutenant opens her mail to find a picture of Mignons African American boyfriend that it is clear to the audience that she has been passing as a white woman and is actually an African American woman, just like the young girl Esther Jeeter that they use at their whim.

Dashs film hints on more illusions than just Miss Duprees.  Not only has Mignon created her own illusion but she is also a direct participant in Hollywoods. Mignon not only works in a field of illusions, working on sound dubbing in Hollywood and directly contributing to the industrys absence of African Americans in film but also the absence of African American woman in film and the work industry.  Mignon is given a certain amount of power in her office but was still below her male counterparts, like the Lieutenant.  When she is discovered to not be a white woman, she is placed at a lower standard and it is certain that she would be let go if anyone else had found out. Mignon has created not only opportunities as an African American but as a woman.

Dash has done the same in similar fashion by being one of the first African American women filmmakers to make it in the industry. Her work efforts have created illusions not only for the characters on screen but her audiences too.  She has introduced many themes such as gender, sexuality and race; that other filmmakers have not touched before.

The film ends with Miss Dupree thinking to herself that she will get just as far as the white men and women that surround her.  She will prove to everyone that race has nothing to do with ability and should not be a limiting factor.   S. V. Hartman and Jasmine Griffin, “Are You as Colored as That Negro?: The Politics of Being Seen in Julie Dash’s Illusions", Black American Literature Forum, 1991, p. 366.  

==Cast==
* Lonette McKee as Mignon Dupree
* Rosanne Katon as Esther Jeeter
* Ned Bellamy as the Lieutenant Bedsford
* Jack Radar as C.J. Forrester  

==Productions== Ella Fitzgeralds voice was used as the voice of Esther Jeeter in the film as her voice was dubbed as the voice of a white woman. Julie Dash wanted Lonette McKee to play Mignon Dupree from the beginning and was not able to offer her anything for her work but Lonette loved the script and participated in the film anyway. 

==Critical reception== LA Film Rebellion that arose out of UCLA, Dash develops stories that gives representation to the African American community and creates films that are not a far cry from reality. Dash addresses the discrimination and hardships that African Americans have endured.

Illusions, a period piece set in the 1940s, confronts Hollywood discrimination during the World War II era that subjected people of color and varying race. Anne Christine D’Adesky, a critic from The Guardian wrote that Illusions beats with a strong feminist heart: in the film, Mignon Dupree (protagonist) learns to reject the Hollywood model but also to create her own…” (September 7, 1983). Marcia Pally from the Village Voice applauds that Illusions “Cleverly uses film itself as a metaphor for the myths fostered by whites and men about Blacks and women”. Critics and authors S.W. Hartman and Jasmine Griffin concur by stating, Illusions explores questions of race, representation, and gender in Hollywood cinema-in particular, the absence of ‘meaningful’ and ‘realistic’ images of our lives”. The Black American Cinema Society awarded the film in 1985. Illusions also was nominated in 1988 for a Cable ACE Award in Art Direction and was the season opener of "Likely Stories", The Learning Channels series focusing on independent film. The Black Filmmakers Foundation awarded Illusions with the 1989 Jurys prize for "Best Film of the Decade". 

==Distribution==
Illusions is available to rent through the Black Filmmakers Foundation, Women Make Movies, and Third World Newsreel (New York); Circles-Women’s Films and Video Distribution (London); Le Soliel O (Paris). 

Running Time is 34 minutes, 16&nbsp;mm, black and white.

== References ==
 

== External links ==
*  
*  
* http://voices.cla.umn.edu/artistpages/dashJulie.php
* http://www.filmreference.com/encyclopedia/Independent-Film-Road-Movies/Race-and-Ethnicity-RECEPTION-SPECTATORSHIP-AND-OPPOSITIONAL-CINEMAS.html

 
 
 
 
 
 
 
 
 