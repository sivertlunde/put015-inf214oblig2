Crook's Tour
{{Infobox Film
| name           = Crooks Tour
| image          =  John Baxter 
| writer         = Barbara K. Emary Max Kester John Watt Charles Oliver
| producer       = John Corfield
| music          = Kennedy Russell
| cinematography = James Wilson
| editing        = Michael C. Chorlton
| distributor    = Anglo-Amalgamated|Anglo-Amalgamated Film Distributors  
| released       = US May 24, 1941 
| runtime        = 80 min.
| language       = English
}} John Baxter featuring Charters and Caldicott. It is adapted from a BBC radio serials of the same name.

==Plot==
Charters (Basil Radford) and Caldicott (Naunton Wayne) are touring the Middle East. After visiting Saudi Arabia they find themselves in Baghdad where they are mistaken by a group of German spies for the messengers who are to carry a song record by beautiful singer La Palermo (Greta Gynt) which contains secret instructions of the German Intelligence. Realizing their error, the German spies follow Charters and Caldicott to Istanbul and Budapest, trying to eliminate them and retrieve the record.

==Cast==
* Basil Radford as Charters
* Naunton Wayne as Caldicott
* Greta Gynt as La Palermo
* Abraham Sofaer as Ali Charles Oliver as Sheik Gordon McLeod as Rossenger
* Bernard Rebel as Klacken
* Cyril Gardiner as K.7.
* Morris Harvey as Waiter
* Noel Hood as Edith Charters
* Leo de Pokorny as Hotel Manager

==Availability== The Lady Criterion Collection DVD and Blu-ray.

==External links==
*  
*  
*  

 

 
 
 
 
 
 


 
 