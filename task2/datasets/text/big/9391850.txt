Betrayal from the East
{{Infobox Film
| name           = Betrayal from the East
| image_size     = 
| image	         = Betrayal from the East FilmPoster.jpeg
| caption        = 
| director       = William A. Berke
| producer       = 
| writer         = Kenneth Gamet Aubrey Wisberg
| based on       =  
| narrator       = 
| starring       = Lee Tracy Nancy Kelly
| music          = Roy Webb
| cinematography = Russell Metty
| editing        = Duncan Mansfield
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =   
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Betrayal from the East is a 1945 spy film starring  .

==Cast==
*Lee Tracy as Eddie Carter
*Nancy Kelly as Peggy Harrison
*Richard Loo as Lieutenant Commander Miyazaki, alias Tani
*Regis Toomey as Agent Posing as "Sergeant Jimmy Scott"
*Abner Biberman as Yamato
*Philip Ahn	as Kato
*Addison Richards as Captain Bates, G-2
*Bruce Edwards as Purdy, G-2 Agent
*Hugh Ho Chang as Mr. Araki (billed as Hugh Hoo)
*Victor Sen Yung as Omaya (billed as Sen Young)
*Roland Varno as Kurt Guenther
*Louis Jean Heydt as Jack Marsden
*Jason Robards, Sr.	as Charlie Hildebrand (billed as Jason Robards) Drew Pearson as Himself

== See also ==
*Across the Pacific, a similar 1942 film starring Humphrey Bogart

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 


 