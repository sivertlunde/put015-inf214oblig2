White Elephant (1984 film)
{{Infobox film
  | name = White Elephant: The Battle of the African Ghosts
  | image = WhiteElephantGhosts.jpg
  | caption = VHS cover for White Elephant: The Battle of the African Ghosts
  | director = Werner Grusch
  | producer =
  | writer = Ashley Pharoah
  | starring = Peter Firth Abi Adatsi Kwabena Holm Charles Annan Samuel Amoah
  | music =
  | cinematography =
  | editing =
  | distributor = Troma Entertainment
  | released = 1984
  | runtime = 90 minutes English
  | budget =
  | preceded by =
  | followed by =
  }}
 British comedy comedy drama film directed by Werner Grusch and starring Peter Firth, Peter Sarpong and Nana Seowg.  The plot involves a ruthless white man determined to break through thousands of years of tradition to sell microchips in Africa. A young British businessman goes to Ghana to modernise a furniture factory by introducing computers. Unbeknownst to him, the spirits of African tradition are already working against him and are determined to make him pay. He encounters fierce resistance from the Ghanaians, and this eventually leads him to better appreciate their culture.

==Main cast==
* Peter Firth ...  Peter Davidson
* Peter Sarpong ...  Bishop of Kumasi
* Nana Seowg ...  High Priestess
* Ejissu Jasantua ...  Fetish Priest
* Frederick Lawluwi ...  Reverend in Anloga
* A.N.K. Mensah ...  Herbalist in Anloga
* Asugebe ...  Patron Ghost of Ejissu Fetish Priest School
* Jasantua ...  Patron Ghost of Ejissu Fetish Priest School
* Nana Abiri
* Klevor Abo
* Abi Adatsi
* Owusu Akyeaw
* Samuel Amoah
* Charles Annan
* Otchere Darko
* Toni Darko
* Kwabena Holm
* Sarfo Opoku

==Distribution==
The film was briefly distributed by Troma Entertainment.

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 