The Scuttlers
{{infobox film
| name           = The Scuttlers
| image          =The Scuttlers (1920) - Farnum & Saunders.jpg
| imagesize      =190px
| caption        =Film still with Farnum and Saunders
| director       = J. Gordon Edwards William Fox
| writer         = Paul Sloane (scenario)
| based on        =  
| starring       = William Farnum Jackie Saunders
| cinematography = John W. Boyle
| editing        =
| distributor    = Fox Film Corporation
| released       = December 12, 1920
| runtime        = 6 reels
| country        = United States Silent (English intertitles)
}} lost 1920 American silent drama film produced and distributed by the Fox Film Corporation and directed by J. Gordon Edwards. William Farnum and Jackie Saunders star in this adventure. 

==Cast==
*William Farnum - Jim Landers
*Jackie Saunders - Laura Machen
*Herschell Mayall - Captain Machen
*G. Raymond Nye - Erickson
*Arthur Millett - Linda Quist
*Harry Spingler - George Pitts 
*Manuel R. Ojeda - Raymond Caldara
*Earl Crain - Don Enrico Ruiz (billed as Erle Crane)
*Kewpie Morgan - The Cook
*Claire de Lorez - Senorita Juanita Bonneller
*Al Fremont - Rosen

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 