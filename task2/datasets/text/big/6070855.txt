Scooby-Doo! Pirates Ahoy!
{{Infobox film
| name           = Scooby-Doo! Pirates Ahoy!
| image          = Scooby-Doo! Pirates Ahoy! cover.jpg
| caption        = US DVD Cover
| director       = Chuck Sheetz
| producer       = Margaret M. Dean Chuck Sheetz
| writer         = Margaret M. Dean Jed Elinoff Scott Thomas Freddy Rodriguez Ron Perlman Tim Conway Edie McClurg Kathy Najimy Arsenio Hall Dan Castellaneta
| music          = Steven Argila
| cinematography = John Powell
| editing        = Susan Edmunson
| distributor    = Warner Bros. Home Video
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         =
}}

Scooby-Doo! Pirates Ahoy! is the tenth of a series of direct-to-video animated films based upon the Scooby-Doo Saturday morning cartoons. It was released on September 19, 2006,  and it was produced by Warner Bros. Animation, though it featured a logo for and copyright to Hanna-Barbera|Hanna-Barbera Cartoons at the end. It features the Mystery, Inc. gang travelling to the Bermuda Triangle on a creepy eerie cruise, with ghosts, pirates, and monsters.

==Plot==
Fred is treating the Mystery Inc. gang to share in his birthday present: a mystery cruise, compliments of his parents Skip and Peggy. While preparing for the ship launch, Scooby and Shaggy have a creepy encounter with a sinister-looking cloaked man, whom the duo are sure is up to no good. The gang meet the hyper Cruise Director, Sunny St. Cloud, and the captain, Captain Crothers. The Captain says they are bound for the Bermuda Triangle, and St. Cloud promises some creepy intrigue. A montage of sloppy "mysteries" that are staged by St. Cloud and Captain Crothers follow, which the gang solves with ease. This peeves the other guests, who have no mysteries to solve now. Soon they rescue a man lost at sea, who tells of an encounter with ghost pirates. The gang naturally assumes that this is a setup to another mystery. He is taken below deck as a man in a jetpack appears from the sky and lands on deck. He turns out to be Biff Wellington, an English billionaire who is known to be fun-loving yet eccentric. He plans to stay on the ship as well.

That night, the gang attends a costume party dinner. The creepy cloaked man appears on stage, who turns out to be Mister Mysterio, a famous hypnotist. Shaggy and Scooby are picked from the audience to demonstrate his powers, but they prove immune to his hypnotism: yet the audience falls under the trance. Mysterio dispels the hypnotism and disappears in a puff of smoke. The creepy fog then engulfs the cruise ship. The ghost pirates wreak havoc, chasing the cruise guests, who all mysteriously disappear. Skip and Peggy are kidnapped, and the pirates retreat back to the galleon and leave (after destroying the cruise ship). At this time, the gang realizes it is a real mystery, and they and Garcia are the only ones left aboard the damaged ship.

With his help, they follow the glowing trail left behind by the galleon and arrive in a secret harbor. There, they find Garcias old ship, and then are captured by the ghost pirates, who take them aboard their galleon. The pirates are looking to find a meteor of mystic power, which fell into the triangle ages ago and could be pinpointed using Garcias map. The gang is tied to the mast along with Skip, but there are no other cruise guests around.

The galleon enters the heart of the triangle, and begins to see past ghosts from the triangle: The USS Cyclops, Flight 19, even a sea serpent. Amidst this, the gang manages to escape and explore below deck. They find a lot of modern equipment that was  used to project the ghostly images just witnessed. The ship enters an odd ring of rocks, and pulls up the meteor, which glows golden yellow.

The gang then engineer a trap, which fails. The pirates then attempt to re-capture the gang, leading to a trademark Scooby-Doo chase sequence, in which the entire pirate crew are captured by Scooby and Shaggy. Captain Skunkbeard is revealed to be Wellington, and Wally is revealed to be Mysterio. The rest of the pirates turn out to be the cruise guests (including the Captain, St. Cloud, and Peggy), shipmates of Garcia, and past conquests of the pirates. Wellington explains that Mysterio convinced him that he was the reincarnation of a pirate years ago, and could use the meteor to teleport back in time. The crew were just hypnosis victims under Mysterios power.  Mysterios motive was that the meteor itself was pure gold and he was going to steal it to make himself rich. Mysterio, still free, attempts to take the meteor for himself, but is stopped by Scooby.

At this time, a fierce storm hits, and the gang deduces the forces of the triangle want the meteor back, so they drop it back in the water, and some steering by Fred narrowly gets the galleon out of the ring as it crumbles into the sea. The cruise guests use the galleon as a large party boat as they sail back to Miami to drop off the villains to the authorities.

==Cast== Norville "Shaggy" Rogers Fred Jones, Scooby-Doo (character)|Scooby-Doo
* Mindy Cohn as Velma Dinkley
* Grey DeLisle as Daphne Blake
* Ron Perlman as Captain Skunkbeard/Biff Wellington Freddy Rodriguez as Rupert Garcia
* Tim Conway as Skip Jones
* Edie McClurg as Peggy Jones/Sea Salt Sally
* Kathy Najimy as Sunny St. Cloud
* Arsenio Hall as Captain Crothers
* Dan Castellaneta as Woodenleg Wally/Mr. Mysterio

==Follow-up Film==
Chill Out, Scooby-Doo!, released in 2007. A direct sequel to Pirates Ahoy!.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 