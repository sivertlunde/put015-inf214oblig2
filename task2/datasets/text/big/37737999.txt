Navakoti Narayana
{{Infobox film
| name           = Navakoti Narayana
| image          = Navakoti Narayana DVD cover.jpg
| image size     =
| caption        = Film DVD cover
| director       = S. K. Ananthachari
| producer       = D. Ramanaidu|D. R. Naidu
| writer         = 
| screenplay     = Narendra Babu S. K. Ananthachari
| narrator       = Rajkumar Sowcar Janaki Dikki Madhava Rao H. R. Shastri
| music          = Shivaprasad
| cinematography = S. V. Srikanth
| editing        = S. P. N. Krishna Harinarayanaiah
| distributor    = 
| studio         = 
| released       = 1964
| runtime        = 163 minutes
| country        = India
| language       = Kannada
}}
 Kannada film Rajkumar and Sowcar Janaki in lead roles. The film is based on the life of Purandara Dasa, a prominent composer of Carnatic music who lived from 1484-1564. In the film, Rajkumar plays the role of Purandara Dasa. The music of the film was composed by Shivaprasad. 

==Cast== Rajkumar as Purandara Dasa
* Sowcar Janaki as Saraswati, wife of Purandara Dasa
* Dikki Madhava Rao
* H.R. Shastri
* Mala
* Radhika
* Kemparaj

==Soundtrack==
{{Infobox album
| Name = Navakoti Narayana
| Type = Soundtrack
| Artist = Shivaprasad
| Cover = 
| Released =  1964 |
| Recorded = Feature film soundtrack
| Length = 
| Lyrics = 
| Label = Sa Re Ga Ma 
| Producer = 
| Chronology =
| Last album =
| This album =
| Next album =
}}

The music of the film was composed by Shivaprasad.

Tracklist   
{| class="wikitable"
|-
! # !! Title !! Singer(s) !! Length
|-
| 1 || "Madhukara Vruththi" || P. Leela, P. B. Sreenivas, M. Balamuralikrishna || 4:25
|-
| 2 || "Bangaravidabare" || P. Leela, M. Balamuralikrishna || 3:36
|-
| 3 || "Padumanabha" || Subbanarasimhayya || 2:47
|-
| 4 || "Dasarendare" || P. B. Sreenivas || 0:39
|-
| 5 || "Aalaya" || M. Balamuralikrishna || 1:02
|-
| 6 || "Aadadella Olithe || M. Balamuralikrishna || 2:59
|-
| 7 || "Kannare Kande" || M. Balamuralikrishna || 3:20
|-
| 8 || "Ranga Bara" || S. Janaki, M. Balamuralikrishna || 3:35
|-
| 9 || "Indina Dinave" || M. Balamuralikrishna || 3:31
|-
| 10 || "Manavajanma" || M. Balamuralikrishna || 2:46
|-
| 11 || "Achchuthananda" || M. Balamuralikrishna || 2:03
|- Chorus || 1:58
|}

==References==
 

==External links==
*  

 
 
 
 