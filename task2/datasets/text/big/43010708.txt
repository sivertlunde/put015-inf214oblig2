Dearest (2014 film)
{{Infobox film
| name           = Dearest
| image          = Dearest 2014 film poster.jpg
| caption        = Theatrical poster
| film name = {{Film name|traditional= 親愛的 simplified = 亲爱的 pinyin = qīn ài de jyutping = }}
| director       = Peter Chan
| producer       = Peter Chan Hui Yuet-chun
| editing        = 
| writer         = Zhang Ji
| starring       = Zhao Wei Huang Bo Tong Dawei Hao Lei Zhang Yi Zhang Yuqi
| music          = Leon Ko
| cinematography = 
| studio         = We Pictures Limited China Vision Media Group Stellar Mega Films Stellar Mega Pictures Enlight Pictures Real Thing Media HB Studio PULIN Production Limited
| distributor    = We Entertainment
| released       =  
| runtime        = 130 minutes
| country        = China Hong Kong
| language       = Mandarin
| gross          = US$54,630,000 
}}

Dearest is a 2014 Chinese-language film directed by Peter Chan on kidnapping in China, based on a true story,  starring Zhao Wei, Huang Bo, Tong Dawei, Hao Lei, Zhang Yi and Zhang Yuqi. It was screened in the Special Presentations section of the 2014 Toronto International Film Festival.   

==Plot==
Following years of unrelenting search, Tian Wenjun (Huang Bo) and ex-wife Lu Xiaojuan (Hao Lei) finally locate their abducted son in a remote village. After the boy was violently taken away from the village, the abductors widow Li Hongqin (Zhao Wei) — the boys foster mother — also loses her foster daughter to a state-owned orphanage in Shenzhen. Heartbroken, Li goes on a lone but determined journey to get her daughter back.

==Theme songs== The Unwritten Law.
* "Mei Yi Ci" (每一次; "Every Time") sang by Huang. It was originally sang by Zhang Hongsheng as an insert song in the 1990 TV series Kewang.
* "Yinxing de Chibang" (隐形的翅膀; "Invisible Wings") sang by Huang and parents of missing children. It was originally sang by Angela Chang in her 2006 album Pandora (album)|Pandora.

==Cast==
* Zhao Wei
* Huang Bo
* Tong Dawei
* Hao Lei
* Zhang Yi
* Zhang Yuqi
* Zhang Guoqiang
* Zhu Dongxu
* Yi Qing
* Wang Zhifei

==Production==
Dearest was filmed in Shenzhen, Guangzhou and Chengde. 

Portraying a rural mother, Zhao Wei spoke the Lower Yangtze Mandarin dialect (the predominant dialect in her hometown of Wuhu) rather than Standard Mandarin in the film. 

== Accolades ==
{| class="wikitable" width="80%"
|+List of awards and nominations
! | Award
! | Category
! | Nominee
! | Result
|- 
| The 58th London Film Festival
| Best Film
| 
|  
|-
| The 51st Golden Horse Awards
| Best Actress
| Zhao Wei
|  
|-
| The 10th Chinese American Film Festival
| Best Actress
| Zhao Wei
|  
|- The 15th Huading Awards
| Best Actress
| Zhao Wei
|  
|-
| Best Actor
| Huang Bo
|  
|- The 21st Hong Kong Film Critics Society Awards
| Best Picture
| 
|  
|-
| Best Director
| Peter Chan
|  
|-
| Best Screenplay
| Zhang Ji
|  
|-
| Best Actress
| Zhao Wei
|  
|- The 34th Hong Kong Film Awards
| Best Picture
| 
|  
|-
| Best Director
| Peter Chan
|  
|-
| Best Screenplay
| Zhang Ji
|  
|-
| Best Actor
| Huang Bo
|  
|-
| Best Actress
| Zhao Wei
|  
|-
| The 4th Chinese Cinephilia Society Awards
| Best Actress
| Zhao Wei
|  
|-
| The 9th Asian Film Awards
| Best Actress
| Zhao Wei
|  
|- The 5th China Youth Film Magazine Poll
| Top Ten Pictures
| 
|  
|-
| Best Actress
| Zhao Wei
|  
|-
| Best Actress
| Hao Lei
|  
|- The 5th Douban.com Movie Awards Best Actress
| Zhao Wei
|  
|-
| Hao Lei
|  
|-
| The 6th China Film Directors Guild Awards
| Special Jury Prize
| 
|  
|-
|}

==See also==
* Lost and Love – another film dealing with child kidnapping in China

==References==
 

==External links==
*  
*  
*   - the story that the film is based on

 

 
 
 
 
 
 
 
 
 