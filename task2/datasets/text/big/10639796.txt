Aadmi Aur Insaan
{{Infobox film
| name = Aadmi Aur Insaan
| image = AadmiAurInsaan.jpg
| image_size = 
| caption = 
| director = Yash Chopra
| producer = B. R. Chopra 
| writer = Akhtar-Ul-Iman
| narrator = 
| starring = Dharmendra Saira Banu Feroz Khan  Ravi
| cinematography = 
| editing = 
| distributor = 
| released = 1969
| runtime = 
| country = India
| language = Hindi
| budget = 
}} Johnny Walker, Ajit Khan|Ajit, Anwar, Iftekhar, Kamini Kaushal and Nazima. The films music is by Ravi (music director)|Ravi.

This is the only movie in which Yash Chopra and Dharmendra have worked together. From Russia with Love.

==Plot==
A wealthy and independent industrialist, JK alias Jai Kishan befriends a young man from a middle-class background by the name of Munish Mehra, assists him financially to go abroad and get necessary qualifications as an engineer, and then hires him on one of his construction projects. Jai meets with and falls in love with Meena Khanna, who is the daughter of a government official. Shortly after this, Jai finds out that Munish is also attracted to Meena, and is enraged. He subsequently fires Munish, and ensures that he will not get hired anywhere else. After several vain attempts to obtain employment, Munish gets hired by the government, and is assigned to investigate the collapse of a railway bridge. Munish finds out that Jai was directly responsible for this mishap as he used sub-standard materials. Munish now has the tools to avenge his humiliation at the hands of Jai, and accordingly informs his employer. But when the time comes for Munish to make a full report, he declines, and the government has no alternative but to prosecute him in a court of law for taking bribes and destroying his report. And the main witness appearing against him is none other than Jai.

==Cast==
* Dharmendra	 ...	Munish Mehra
* Saira Banu	 ...	Meena Khanna
* Feroz Khan	 ...	Jai Kishan / J.K. Mumtaz	 ...	Rita Johnny Walker	 ...	Gulam Rasool
* Randhawa	 ...	Bhehlwaan Ajit	 ...	Kundanlal / Sher Singh
* Uma Dutt	 ...	Sher Singh Anwar Hussain	 ...	Gupta
* Iftekhar	 ...	Saxena
* Gajanan Jagirdar	 ...	Judge
* Kamini Kaushal	 ...	Mrs. Khanna (Meenas mom) Poonam Sinha (Komal)	 ...	Renu Mehra
* Manmohan Krishna	 ...	Mr. Khanna (Meenas father)
* Roopesh Kumar	 ...	Abdul Rashid / Rashu
* Nana Palsikar	 ...	Justice B.N. Desai
* Madan Puri	 ...	Sabharwal
* Jagdish Raj	 ...	Balwa (drunkard)
* Achala Sachdev	 ...	Mrs. Mehra (Munishs mom)
* Surekha		
* Moolchand	 ...	Worker / Seller (uncredited)
* Keshav Rana	 ...	Rana (uncredited)
* Ravikant	 ...	Mirza (uncredited)

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Neele Parbaton Ki Dhara"
| Mahendra Kapoor, Asha Bhosle
|-
| 2
| "Bacha Le Ae Maula Ae Ram"
| Mohammed Rafi
|-
| 3
| "Ijaazat Ho"
| Mahendra Kapoor, Lata Mangeshkar
|-
| 4
| "Itni Jaldi Na Karo"
| Asha Bhosle
|-
| 5
| "Jaage Ga Insaan Zaman Dekhega"
| Mahendra Kapoor
|-
| 6
| "O Yara Dildara"
| Mahendra Kapoor, Balbir, Joginder
|-
| 7
| "Zindagi Ittefaq Hai (Duet)"
| Mahendra Kapoor, Asha Bhosle
|-
| 8
| "Zindagi Ittefaq Hai (Solo)"
| Asha Bhosle
|-
| 9
| "Zindagi Ke Rang Kai Re Saathi"
| Lata Mangeshkar
|}

==Awards==
* 1970 Filmfare Best Supporting Actor Award for Feroz Khan

== External links ==
*  

 
 
 
 