Vacancy 2: The First Cut
   
{{Infobox film name = Vacancy 2: The First Cut  image = vac2cov.jpg caption = DVD cover director = Eric Bross producer = Hal Lieberman writer = Mark L. Smith starring = Agnes Bruckner Trevor Wright Arjay Smith David Moscow Brian Klugman Beau Billingslea Nelson Lee Gwendoline Yeo music = Jerome Dillon cinematography = Horacio Marquínez editing = Angela M. Catanzaro distributor = Sony Pictures Home Entertainment released =   runtime = 86 minutes country =   language = English
|budget = $5 million
}} slasher directed by Eric Bross, starring Agnes Bruckner, Trevor Wright, David Moscow, and Gwendoline Yeo. It is the prequel to 2007s Vacancy (film)|Vacancy.

==Plot==
Set three years before the first film, the action takes place in and around the Meadow View Inn, a small roadside motel in rural North Carolina. It opens with newlyweds driving and the bride wants to have sex in the car, which makes the groom stop. They are then stopped by a man, whose property they are on. The manager, Gordon (David Moscow), oversees an operation of using cameras within the motel to videotape the newlyweds having oral sex. Later, a serial killer (Scott G. Anderson), who checks into the motel as Smith brutally murders a woman he brought with him in one of the motel rooms. The horrified staff see the whole crime on videotape and then subdue the man. They, however, do not know what to do with him as they will endanger their operation if they call the police. Smith manages to convince Gordon and his staff to hire him to torture and murder the motel guests on videotape and then sell them as snuff films. 

The trucker confirms that this is indeed a functional market and Gordon agrees under the conditions that he is the leader of the operation. Smith mockingly agrees. A couple of days later, a young couple, Jessica (Agnes Bruckner), Caleb (Trevor Wright), and their friend Tanner (Arjay Smith) check into the Meadow View Inn for a nights rest, unaware of the inns sick-minded employees and their nefarious intentions. As the killers begin to terrorize the hotel guests, it quickly becomes apparent that Smith is a loose-cannon, with no intention of following Gordons lead. He fashions a crude mask out of wire and proceeds to murder two of the hotels three guests. They capture and stab Caleb in the stomach until he dies, while Jessica and Tanner hide in the woods and then to a nearby house. It is revealed that the man who stopped the newlyweds lives in the house with his wife. It is revealed his name is Otis. They let Jessica and Tanner in. They tell the couple about the killers, but Otis does not believe them. Smith and the others come in, telling the man that Jessica and Tanner stole from them, which Otis believes. But his wife does not, as she takes the phone and is about to call the police. Quickly, Smith shoots Otis then his wife and runs after Jessica and Tanner.

They catch both and torture Tanner first. Smith calls the others off, since he wants to kill Jessica himself. The others are watching on tape what Smith is going to do. But one of them does not want to kill anymore and Gordon says to let it play out then they will call the cops and blame the whole thing on Smith. Smith leaves the room to check on Gordon and the others. Jessica found a way to untie herself. Smith comes back and is about to kill Jessica. Jessica manages to stab him in the jaw, but Smith survives. Jessica escapes and Gordon and the others run to see what happened to Smith. Jessica hides and finds dead bodies. She then finds Caleb dead and Tanner, dying while choking on his blood. Meanwhile, Smith (who took care of the wound) and the others desperately search for Jessica. One of the staff go to the place where Jessica hid, but she hides under Calebs body and pushes it upward, killing the employee (since it had low ceiling with nails sticking out of it). Jessica takes his gun and runs to the truck and rummages through the glove box, then she goes underwater in the lake. Gordon goes to the lake and searches for her.

He finds a sweater sticking out of the water, thinking it is her. Jessica, underwater, pulls out the gun out of the water. Gordon turns around to see it as Jessica pulls the trigger, killing him. Smith hears the gunshot and runs to the lake. Jessica gets out quickly and hides in Smiths trailer. He finds her and tries to stab her, but she burns him and runs. The next day, the cops arrive and Jessica tells the story. But they are skeptical of her story because the cameras and bodies have disappeared. At another hotel (it is disputed as to whether or not this is the Pinewood Hotel seen in the first film), a badly scarred Smith informs the trucker that he will have the motel up and running in a few weeks just as soon as the cameras are set. Smith gives the trucker the very first snuff film that features one of the guests he murdered at the Meadow View Inn. He tells the trucker that he will make more copies as soon as he can.

==Cast==
*Agnes Bruckner as Jessica
*Trevor Wright as Caleb
*Arjay Smith as Tanner
*David Moscow as Gordon
*Brian Klugman as Reece
*Beau Billingslea as Otis
*Nelson Lee as Groom
*Gwendoline Yeo as Bride
*Scott G. Anderson as Smith

==Soundtrack==
64 minutes of music was written, performed, recorded, mixed, and mastered by Jerome Dillon (Nearly, Nine Inch Nails) with engineer Ryan Kull, in his home studio for the soundtrack.

==Release==
It was released direct-to-video|direct-to-DVD on January 20, 2009. Reviews were average and mediocre. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 