Sheitan
 
{{Infobox film name        = Sheitan image       = Sheitan.jpg caption     = director    = Kim Chapiron producer    = writer      = Christian Chapiron Kim Chapiron starring    = Vincent Cassel Olivier Bartélémy Roxane Mesquida Nico Le Phat Tan Leïla Bekhti Ladj Ly Monica Bellucci distributor = 120 Films released    = 1 February 2006 country  France
|runtime     = 94 min. ratings      = 16+ (France)  language  French
}} French Horror film|horror/erotic comedy film. It was directed by first time director Kim Chapiron, and written by Kim and Christian Chapiron. It stars and was co-produced by Vincent Cassel. His wife Monica Bellucci also makes a cameo appearance in the film.

==Plot==
On Christmas Eve, a group of friends are led by a girl (Roxane Mesquida) they met at the Styxx Club, in Paris, to her house in the country. There, they are introduced to the eccentric housekeeper Joseph who has something sinister planned for them.

==Cast==
*Vincent Cassel as Joseph / Marie
*Olivier Barthelemy as Bart
*Nico Le Phat Tan as Thaï
*Ladj Ly as Ladj
*Roxane Mesquida as Eve
*Leïla Bekhti as Yasmine
*Julie-Marie Parmentier as Jeanne

==Ratings and Controversy==
When released, the movie was given an "Interdit aux moins de 16 ans" (16+) rating in France due to its inconsistent yet occasionally shocking scenes involving sex (with full nudity), violence, with some sudden shocking gruesome and disturbing images, particularly during the final 10 minutes. The movie was not rated by the MPAA, but would have most likely have been a strong R, perhaps even an NC-17. It was released as "unrated" instead.

==External links==
*  
*  
*  
*  

 
 
 
 
 
 


 
 