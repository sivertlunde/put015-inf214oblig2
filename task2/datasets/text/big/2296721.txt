Cobra Verde
 
{{Infobox film
| name           = Cobra Verde
| image          = Cobra Verde poster.jpg
| caption        = Theatrical release poster
| director       = Werner Herzog
| producer       = Lucki Stipetić
| screenplay     = Werner Herzog
| based on       =  
| starring       = Klaus Kinski José Lewgoy Popol Vuh
| cinematography = Viktor Růžička
| editing        = Maximiliane Mainka
| distributor    = Werner Herzog Filmproduktion
| released       = December 3, 1987 
| runtime        = 111 min.
| country        = West Germany German
| budget         = 
}} German drama slave trader. It was filmed on location in Brazil, Colombia and Ghana.

Klaus Kinski died four years after the release of Cobra Verde, and the film would stand as the last of his collaborations with director Werner Herzog.

==Plot==
Francisco Manoel da Silva (Klaus Kinski) is a debauched Brazilian rancher who reluctantly goes to work at a gold mining company after his ranch is ruined by drought. When he discovers that he is being financially exploited, he murders his boss and goes on the lam to pursue a career as an outlaw. He becomes the notorious Cobra Verde (Green Snake), the most vicious bandit of the sertão.

In his travels, da Silva encounters and subdues an escaped slave, an act that impresses wealthy sugar baron Don Octávio Coutinho (José Lewgoy). Don Coutinho, unaware that he is dealing with the legendary bandit, hires da Silva to oversee the slaves on his sugar plantation. When da Silva subsequently impregnates all three of the Dons daughters, the sugar baron is furious, but the situation becomes even more complicated when he discovers that da Silva is none other than the infamous Cobra Verde.
 Bossa Ahadee of Dahomey (played by His Honor the Omanhene Nana Agyefi Kwame II of Nsein, a village north of the city of Axim, Ghana).
 army of native women, and leads them on a raid to successfully overthrow King Bossa.

Against all expectations, the slave trade is successfully maintained under the new King, thanks to da Silvas resourcefulness. However, da Silva eventually falls out of favor with the new King, and discovers that in the meantime the Portuguese have outlawed slavery and seized his assets, and the English have placed a price on his head. Despite the adversity, da Silva is glad that finally a change has come. The exhausted bandit tries desperately to take a boat to water, but despite his best efforts, he is unable to accomplish the task. He collapses next to the ship as the tide slowly laps in. The film ends with the hauntingly symbolic image of an African man stricken with polio walking along the shore, and a group of young native women laughingly chant over the credits.

==Production==
The film was shot in Ghana, Brazil and Colombia. Herzog showed Kinski photographs of the places where he would like to work. Kinski was interested in some landscapes in Colombia, but Herzog did not agree. However, Kinski made the trip with a group of friends to some remote places that fascinated him: the foothills of the Sierra Nevada de Santa Marta and the Cape of the Sailing, on the peninsula of La Guajira, Colombia. Herzog finally decided on Villa de Leyva and Valle del Cauca, in the South American country. Kinski said then: "Herzog does not know that I give life to the dead scenery".

The film was based upon Bruce Chatwins 1980 novel The Viceroy of Ouidah, which was itself based on the Brazilian slave trader Francisco Félix de Sousa and his role in helping King Ghezo overthrow his brother Adandozan as King of Dahomey with the help of Ghezos Dahomey Amazons.

==Tension between Herzog and Kinski==
Cobra Verde was the last film that Werner Herzog would make with Klaus Kinski. Their now-legendary personality conflict peaked during the film. The films production was especially affected by Kinskis fiery outbursts. The cast and crew were continually plagued by Kinskis wrath, most famously culminating in the films original cinematographer Thomas Mauch walking out on the project after a perpetual torrent of verbal abuse from Kinski. Herzog was forced to replace Mauch with Viktor Růžička. 

Herzogs opinions of Kinski are deeply explored in his 1999 documentary retrospective, My Best Fiend, where he examines their unique friendship, the associated hatred, and the legacy that both qualities were responsible for. The filming of Cobra Verde and relationship of Herzog and Kinski was also the subject of a 1987 Swiss documentary film, Location Africa.

==References==
 

==External links==
*  
*  
*   

 

 
 
 
 
 
 
 