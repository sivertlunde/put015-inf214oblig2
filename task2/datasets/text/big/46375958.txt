Arson for Hire
{{Infobox film
| name           = Arson for Hire
| image          = Arson for Hire.jpg
| image_size     =


| caption        = Theatrical poster.
| director       = Thor L. Brooks
| producer       = William F. Broidy
| writer         = Tom Hubbard based on = 
| narrator       = Steve Bodie Lyn Thomas Tom Hubbard Jason Johnson
| music          = 
| cinematography = William Margulies
| editing        = Herbert R. Hoffman studio          = William F. Broidy Productionsi
| distributor    = Allied Artists Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         =
}} Steve Brodie, Lyn Thomas and Tom Hubbard, who also wrote.
Arson squad investigator Johnny Broderick and his partner Ben Howard, investigate a warehouse fire, and find evidence of arson.

==Plot==
Actress Keely Harris inherits a warehouse from her father that is burned in a fire. Her fathers business partner informs her that he and her father had planned the fire and demands half the insurance money. Meanwhile, arson investigator Johnny Broderick and assistant Ben Howard suspect the fire wasnt an accident.

==Cast== Steve Brodie	...	Arson Squad Insp. John Johnny Broderick
* Lyn Thomas	...	Keely Harris
* Tom Hubbard      ...	Ben Howard, Brodericks Assistant
* Jason Johnson	        ...	William Yarbo
* Frank J. Scannell		...	Pop Bergen (as Frank Scanell)
* Wendy Wilde		...	Marilyn Marilee Bergen
* John Frederick		...	Clete, the Photographer (as John Merrick)
* Corinne Cole		...	Cindy, the Secretary (as Lari Laine)
* Antony Carbone		...	Foxy Gilbert
* Lyn Osborn	...	Jim, the Fireman
* Robert Riordan		...	Fire Chief Boswell Walter Reed		...	Chief Hollister
* Reed Howes		...	Barney, the Bartender
* Lester Dorr		...	Cab Dispatcher Frank Richards		...	Man Making Phone Calls

==Release==
The film was released as a double feature with The Giant Behemoth.
"Crummy actioner   pretty horrendous when the stock footage of fires isnt onscreen." tvguide.com 
==Tabonga==
The "Tabonga" suit from the film From Hell It Came appears in a warehouse. The characters even have a gunfight in front of it. Apparently, the warehouse used in the scene was Allied Artists Pictures own. The scene can be viewed on YouTube. 

==References==
 

==External links==
*  at  

 
 
 
 


 