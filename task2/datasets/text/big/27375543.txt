Trigun: Badlands Rumble
{{Infobox film
| name                 = Trigun: Badlands Rumble
| image                = Trigun - Badlands Rumble poster.jpg
| caption              = Theatrical release poster
| director             = Satoshi Nishimura
| producer             = {{Plain list|
* Shigeru Kitayama
* Yukiko Koike
* Yoshiyuki Fudetani
* Tsuneo Takechi
}}
| screenplay           = Yasuko Kobayashi
| story                = {{Plain list|
* Yasuhiro Nightow
* Satoshi Nishimura
}}
| based on             = {{Plain list|
*  
*  
}}
| starring             = {{Plain list|
* Masaya Onosaka
* Shō Hayami
* Hiromi Tsuru
* Satsuki Yukino
* Maaya Sakamoto
* Tsutomu Isobe
* Bin Shimada
* Fumihiko Tachiki
* Nobuo Tobita
* Taiten Kusunoki
* Kikuko Inoue
}}
| music                = Tsuneo Imahori
| cinematography       = Shinichi Igarashi
| editing              = Satoshi Terauchi Madhouse
| distributor          = KlockWorx
| released             =  
| runtime              = 90 minutes
| country              = Japan
| language             = Japanese
| budget               = 
| gross                = Japanese yen|¥72 million   
}}
Trigun: Badlands Rumble is a 2010 Japanese animated action comedy film by Madhouse (company)|Madhouse. {{cite web
| url = http://www.newsshopper.co.uk/news/8952942.BEXLEY__BAM_festival_returns_bigger_and_better/ 
| title = Bexleys anime and manga festival promises new films and lots more
| date = April 4, 2011 
| archiveurl = http://www.webcitation.org/5y6JvRHul 
| archivedate = 2011-04-20 
| deadurl = no
}}  It is based on the Trigun manga, written by Yasuhiro Nightow.

==Plot==
 
The movie opens with a bank robbery orchestrated by the titanic, towering thief known as List of Trigun characters#Gasback|Gasback. As a thief, he only pulls off the most difficult heists that offer the biggest rewards. Gasbacks trio of henchmen, however, are tired from the increasing danger and expense of each successive job and decide to seize the winnings from his latest heist at Macca City, kill Gasback and retire to a life of luxury. When Cain, the leader of the trio goes to fire the killing shot, his gun is mysteriously knocked out of his hand...

Vash the Stampede appears from underneath a counter where he had been cowering and immediately throws the thieves off guard with his odd behavior and complaints about ruining his doughnuts. Gasback attempts to shoot down his former companions as they run away, but Vash manages to throw his aim off and no one is killed. Police begin to circle the building just as it is revealed that Vash is the legendary "Humanoid Typhoon." Gasback is confused why he would want to try to save even robbers and Vash responds in his usual way telling him that its better to be alive. Gasback ultimately escapes the police by using a barrage of explosives for cover, one of which damages the citys power Plant.

20 years later the movie picks up with Gasbacks former henchmen who had been living luxuriously on the money they had stolen. Gasback has taken revenge on one of them, destroying his business and property and leaving him to seek refuge with Cain Kepler. Cain used his portion of the robbery money to repair the citys Plant that was damaged by Gasbacks escape and became Mayor. His vanity culminates in a huge rotating statue in his image built in the center of the city. Fearing Gasback will try to steal it, he insures the statue for 5 billion double-dollars prompting Milly Thompson and Meryl Stryfe to travel to Macca City to run risk prevention.

Vash travels to the city aboard a sand steamer and intervenes in an escalating brawl with some thugs harassing a young woman named List of Trigun characters#Amelia|Amelia. She is appreciative of Vashs assistance, but spurns his over-the-top advances (and rather humorously has a physical allergic reaction to men touching her). Amelia is actually a bounty hunter who has traveled to Macca City in anticipation of Gasbacks appearance. Over the years the thief has accrued a 300 million double-dollar bounty and Amelia is only one of a great number of bounty hunters gathering to take a shot at bringing the criminal down.
Meanwhile, Gasback continues planning his revenge and is ambushed by police who open fire on the bar he is playing poker in and level it to the ground. Gasback is shielded from the gunfire by his bodyguard who turns out to be Nicholas D. Wolfwood. Gasback discovered the priest dying of thirst in the middle of the desert (in a nod to Wolfwoods first appearance in the anime) near a water vending machine that wouldnt accept his crumpled bills. Feeling indebted to the thief, Wolfwood agrees to act as his bodyguard, but not actively participate in the robbery against Cain.

Vash continues trying to unsuccessfully court Amelia and they end up at dinner with Milly and Meryl. A brawl breaks out and afterward Vash carries the inebriated Amelia back to their hotel. That night Gasback attacks and destroys a factory owned by the second henchmen at a nearby town, leaving him to seek refuge with Cain and word spreads that the rumored attack on Macca City was a hoax. The bounty hunters, including Amelia, begin to leave the city early the next morning just as Gasback arrives. The bounty hunters, most of them nursing hangovers from drinking the night before, offer little resistance and soon Gasback fights his way to Cains doorstep where he is halted by Vash. Wolfwood orders Vash to let Gasback pass and opens fire, distracting him just long enough for the thief to get inside the mansion. With his contract complete, Wolfwood puts his gun away allowing Vash to chase Gasback inside.

Gasback confronts Cain just as Vash and Amelia arrive. Vash hopes to defuse the situation while Amelia aims to kill Gasback. Amelia learns that Vash was the one who allowed Gasback to escape from the botched robbery 20 years ago. During Vashs interruption Cain manages to escape and once again Vash allows Gasback to leave despite Amelias protests. She tells Vash that because he saved Gasback 20 years ago countless lives were affected by his robberies including her own life and her mothers who suffered because of the thief. She swears to chase him down and take revenge.

Elsewhere in the city Gasback confronts Cain once again and explains to his betrayer that hes not going to kill him, but instead steal everything that belongs to him. Cain had believed the target of the theft was his statue, but in fact the object is far more valuable. Gasback sets off a string of explosions which separate the citys Plant from its cradle and the massive light-bulb like Plant rolls through the town past dumbstruck citizens straight to the citys main gates where a special getaway vehicle secures the Plant and drives away. Amelia immediately takes a vehicle and chases after Gasback and she is followed by Vash and Wolfwood. She catches up to Gasback and starts shooting coming dangerously close to hitting the Plant. Vash and Wolfwood pull up between them and Vash tries to calm Amelia down, but he is shot by one of Gasbacks henchmen and falls into dry quicksand. Wolfwood leaps in after him to save him, but only manages to pull up Vashs sunglasses. They take the sad news back to Milly and Meryl in the now darkened city and Amelia starts to think that shes no better than Gasback.

The following day Amelia enlists Wolfwood (wearing Vash’s sunglasses in tribute) to help track down Gasback and reclaim the Plant. They attack the thief’s caravan, but are quickly outmaneuvered. Just as one of Gasbacks men moves to shoot Amelia, the gun is shot out of his hand by Vash, who reappears without a scratch. The bullet that struck him lodged in an extremely tough piece of smoked meat he was carrying in his coat pocket (and he barely avoided drowning in the quicksand pit due to Milly and Meryl nearly falling in the same pit and inadvertently pulling him out). Gasback then challenges him to a duel and Vash handily defeats him with a shot to the leg and shoulder. Gasback then activates a secret energy-based weapon, but Amelia steps forward and reveals a strange mechanical glove she wears which counters the weapon. Gasback recognizes the glove immediately as something he made for his wife, Amelias mother. Amelia explains she was born shortly after Gasback left home to commit another robbery. The thief left his wife well-provisioned, but rival thieves came and stole almost everything they owned and none of their neighbors would help the family of the notorious criminal Gasback and not even doctors would help when Amelias mother lay dying. Amelia holds the gun on him, but ultimately decides to spare his life having adopted Vashs outlook. A disheveled Cain arrives on the scene with a gaudy missile bearing his face which Wolfwood dispatches with a single shot from his Cross Punisher.

The closing credits show scenes where the town is restored. Vash and Wolfwood are walking when they are greeted by Meryl and Milly, who go off to report on the events. Gasback, his henchmen and his former cohorts including Caine have been taken into custody; as the police caravan travels, Amelia is seen following them from a distance. Vash and Wolfwood discuss the events of the last few days, including the circumstance that if Vash had not saved Gasback 20 years ago, Amelia would not have been born. A newspaper page blows into Vashs face. Vash reads it and declares they are heading in a new direction. Wolfwood asks what is going on and learns from the paper that the Dodongo Brothers have escaped prison. Wolfwood interprets Vash’s silent detachment as a sign that the legendary outlaw has something to do with the Brothers as well.

==Development== NEO includes Masao Maruyama, Madhouses founder and series planner. In the article he revealed that the studio has been working on a Trigun movie that would be released in "a couple of years". The November 2005 issue of Anime Insider also confirmed this news.

In May 2007, Nightow confirmed at the Anime Central Convention that the Trigun movie was in the early stages of pre-production with a near-final script, although he did not divulge any plot information.

In February 2008, more details about the Trigun movie emerged on the cover of volume 14 of the Trigun Maximum manga, announcing that the movie was scheduled for 2009.    In October 2009, however, the movies official website announced a new Japanese premiere set for spring 2010.    The story of the movie, as depicted from the cover, was going to be about "Vash vs. Wolfwood", the two main characters of the manga.   

==Release==
Trigun: Badlands Rumble opening debut was at #14 on 10 screens. 

The film was shown to an American audience first at the Sakura-Con 2010 in Seattle, Washington on Friday, April 2, 2010. Following the showing, the director held a 15-minute Q&A session before the movie, explaining the reasons it was subbed, not dubbed, and why it was premiered first at the convention, also explaining the new characters. The movie was shown again on Saturday and Sunday according to the schedule.     FUNimation Entertainment announced at Anime Expo along with film producer Shigeru Kitayama that they had licensed the film and plan on releasing it theatrically in the United States.    The films dubbed version premiered at Otakon on Saturday, July 30, 2011 followed by a Q&A session with voice actor Johnny Yong Bosch. 

The film made its US television premiere on Saturday, December 28, 2013 on Adult Swims Toonami block. 

==Cast==

{| class="wikitable sortable"
|-
! Character !! Japanese audio !! English dub
|-
| Vash the Stampede || Masaya Onosaka || Johnny Yong Bosch
|- Show Hayami || Brad Hawkins
|-
| Meryl Stryfe || Hiromi Tsuru || Luci Christian
|-
| Milly Thompson || Satsuki Yukino || Trina Nishimura
|- Amelia Ann McFly || Maaya Sakamoto || Colleen Clinkenbeard
|- Gasback Gallon Getaway || Tsutomu Isobe || John Swasey
|- Caine Flaccano Kent Williams
|- Mechio Tante Ogorare || Fumihiko Tachiki || Newton Pittman
|- Dorino Ohre Tadiski || Nobuo Tobita || Sonny Strait
|- Shane B. Goodman || Taiten Kusunoki || Robert McCollum
|-
|Amelia’s Mother || Kikuko Inoue || Lydia Mackay
|- Young Amelia || Maaya Sakamoto || Cherami Leigh
|}

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 

 