Special Education (film)
 
  
{{Infobox film
| name           = Специјално васпитање
| image          = Special Education (1977 YU film).jpg
| caption        = Special Education Goran Marković
| producer       = 
| screenplay     = Miroslav Smić
| based on       = 
| starring       = Slavko Štimac Ljubiša Samardžić Branislav Lečić
| cinematography = 
| editing        = Goran Marković
| studio         = 
| distributor    = 
| released       = 1977
| runtime        = 110 minutes
| country        = Yugoslavia
| language       = Serbian
| budget         = 
| gross          = 
}}
Special Education ( , whom this was first feature film. Scenario is written by both Goran Marković and Miroslav Simić.
 Big Golden Arena, for his role of militiaman Cane, as for featured male episodic role.  In the same year he was awarded also by Golden Ducat on Mannheim Film Festival, earlier notable film festival.

Special Education was the most popular and best selling Markovićs film, screened in more than fifty countries. Some folks believe that this Markovićs debuting work gave him an opportunity to make more quality films in the future. In this movie have debuted few afterwards more notorious actors: Branislav Lečić and Aleksandar Berček. Branislav Lečić, debuting as Fikret Hadžiabdić, has started his acting career. His acting career is rich of roles that dont have a respect to law.

== Roles ==
{| class="wikitable"
! Actor !! Role
|-
| Slavko Štimac || Pera Antić "Trta"
|-
| Bekim Fehmiu || Žarko Munižaba
|-
| Aleksandar Berček || Ljupče Milovanović
|-
| Ljubiša Samardžić || police officer Cane
|- Trustee Correctional Facility
|-
| Cvijeta Mesić || Psychologist Kosara
|-
| Branislav Lečić || Fikret Hadžiabdić
|-
| Ratko Tankosić || "Sarma"
|-
| Hasim Ugljanin || "Sirće"
|-
| Olivera Ježina || Mira
|-
| Rade Marković || The Judge 
|-
| Jovan Janićijević Burduš || repairman Hranislav
|-
| Slobodan Aligrudić || Police commander
|-
| Radmila Savićević || neighbor 
|-
| Milka Lukić || Rosa, Canes wife
|-
| Josif Tatić || Educator Stojanović
|-
| Radmila Guteša || Social worker 
|-
| Vladan Živković || Driver of cold storage
|-
| Mirjana Blašković || Peras mom
|-
| Ljubomir Ćipranić || Ilija 
|-
| Predrag Milinković || Second drunker 
|-
| Petros Lupos || Third drunker 
|-
| Voja Brajović || as Himself
|}

== References ==
 

== External links ==
* 

 
 
 