Species (film)
{{Infobox film
| name = Species
| image = Speciesver3.jpg
| caption = Theatrical release poster
| director = Roger Donaldson
| producer = Frank Mancuso, Jr. Dennis Feldman
| writer = Dennis Feldman
| starring = Ben Kingsley Michael Madsen Alfred Molina Forest Whitaker Marg Helgenberger Natasha Henstridge
| music = Christopher Young
| cinematography = Andrzej Bartkowiak
| editing = Conrad Buff
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 108 minutes
| country = United States
| language = English Portuguese
| budget = $35 million {{cite web|title=Species - Box Office Data, DVD Sales, Movie News, Cast Information
|url=http://www.the-numbers.com/movies/1995/0SPEC.php |publisher=The Numbers |accessdate=30 June 2013}} 
| gross = $113,374,103 
}} science fiction alien seductress before she successfully mates with a human male.

The film produced one theatrical   in 2007, which stands as a stand-alone film, not as an official follow-up to the previous three films.

==Plot summary==
During the SETI program, Earths scientists send out transmissions (shown to be the Arecibo message) with information about Earth and its inhabitants, DNA structure, etc., in hopes of finding life beyond Earth. They then receive transmissions from an alien source on how to create endless fuel effortlessly. Therefore, the scientists assume that this is a friendly alien species. From a second alien transmission, the scientists receive information about an alien DNA along with instructions on how to splice it with human DNA. A government team led by Xavier Fitch (Ben Kingsley) goes forward with the genetic experiment attempting to induce a female, because a female would have "more docile and controllable" traits. One of the hundred experimental ova produces a girl named Sil, who looks like a normal human but develops into a 12-year old in 3 months.

Sils violent outbursts during sleep make the scientists consider her a threat. They try to kill her using cyanide gas but she breaks out of her containment cell and escapes. The government assembles a team composed of anthropologist Dr. Stephen Arden (Alfred Molina), molecular biologist Dr. Laura Baker (Marg Helgenberger), empath Dan Smithson (Forest Whitaker) and mercenary Preston "Press" Lennox (Michael Madsen) to track and destroy Sil. Sil matures rapidly into an adult (Natasha Henstridge) in her early twenties and makes her way to Los Angeles. This makes tracking her extremely difficult. She is incredibly strong and intelligent with amazing regenerative powers. The scientists fear she may mate with human males and produce offspring that could eliminate the human race. Sil lacks inhibitions when it comes to killing people who get in her way and wants to produce offspring as soon as possible. She frequently morphs into her alien form, a bipedal creature with tentacles on her shoulders and back.

Sil tries first to mate with a man she meets at a night club, but after sensing that he is diabetic, rejects him. Unsatisfied, he then tries to forcibly initiate sex, prompting her to kill him by puncturing his skull with her tongue. She then tries to mate with a man she meets after a car accident. They swim in the mans pool where Sil forces the man to open his swimming trunks in order to mate, but the man refuses. This is interrupted by Press and Laura. She kills the man and flees naked into a forest without being seen by the team. She pretends to be a rape victim, and then proceeds to kidnap a woman. She fakes her death by crashing the womans car into a high voltage transformer during a high-speed chase.

After cutting and dyeing her hair, she takes an attraction to Press. Arden, who is upset at not having found himself a woman walks into his room to find Sil waiting in there. She has intercourse with Arden; then kills him when he realizes who she is. The rest of the team then follow her into the sewers where Fitch is subsequently killed. The area where Sil and her offspring are destroyed. Press uses a grenade launcher on Sil, blowing her head off. The trio leaves the area. The last scene shows a rat chewing on one of Sils severed tentacles; it starts to mutate into a vicious beast and attacks another rat.

==Cast==
*Ben Kingsley as Xavier Fitch
*Michael Madsen as Preston "Press" Lennox
*Alfred Molina as Dr. Stephen Arden
*Forest Whitaker as Dan Smithson
*Natasha Henstridge as Sil Michelle Williams as Young Sil
*Marg Helgenberger as Dr. Laura Baker
*Whip Hubley as John Carey

==Production==

===Writing and development===
   as he worked on another film about an    Feldman started to think that it was "unsophisticated for any alien culture to come here in what Id describe as a big tin can". Thus in turn he considered that the possibility of extraterrestrial contact was through information. "The Making of Species: The Origin", Species Definitive Edition DVD disk 2  Then he detailed that a message would contain instructions from across the void to build something that would talk to men, and Feldman thought that "this wouldnt be a robot; it would be Wetware (brain)|wetware. It would also want to use our DNA to make sure it could live in our environment, whatever this creature was. It knew the genes that were surviving here were the ones that would tell it what form to take and how to survive here."  Given mankind had already sent to space transmissions "giving out directions" such as the Arecibo message, Feldman thought that "maybe we shouldnt be so freely broadcasting where we live to life forms that might prey upon us",  as "in nature, one species would not want a predator to know where it hides" and  the human race has been an apex predator for so long "that weve lost sight of the fact that were a species, like any other. Maybe we shouldnt be so freely broadcasting where we live to lifeforms that might prey upon us."   

From this emerged a film treatment called "The Message".  The original script had a more police procedural approach, with the alien being created by a "bathtub geneticist" who had just had his project aborted by the government, and a biologist who had worked on the project getting along with a police officer to seek the creature. Eventually Feldman thought this had some credibility issues and instead switch the protagonists to a government team. After coining the name Sil, Feldman initially thought of forming an acronym, but eventually chose only the three-letter names after learning about the codons of the genetic code which are represented in three letters. Sil would originally emerge from "a DNA sequence that took control of ours", and constantly mutate as she used the junk DNA to access "all the defenses of the entire animal kingdom that we evolved through — including ones that had never developed, plus ones we dont know about that have become extinct."  Among the research Feldman did for the script included going to sessions of UCLAs  Center for the Study of Evolution and the Origin of Life (CSEOL), talking to SETI scientists, and visiting the Salk Institute for Biological Studies to talk with researchers of the Human Genome Project.  "The Message", was offered to several studios but passed up.   

Eventually in 1993 Feldman reworked his ideas into a spec script.  This was eventually sent to producer Frank Mancuso, Jr., who had hired Feldman to adapt Sidney Kirkpatricks A Cast of Killers.  The producer got attracted to the creative possibilites as the film offered "the challenge of walking that fine line between believability and pushing something as far as it can go."  Metro-Goldwyn-Mayer got interested on the project, and while Feldman had some initial disagreements on the budget, after considering other studios he signed with MGM feeling "they were the best people to make the movie."   In turn the now retitled Species attracted director Roger Donaldson, who was attracted to its blend of science fiction and thriller. The script underwent eight different drafts, written over an eight-month period, before Donaldson was content that flaws in the storys logic had been corrected. At one point another writer, Larry Gross, tried his hand with the script, but ultimately all the work was done by Feldman.  Feldman would remain as a co-producer. While the initial Species script would suggest a love triangle between Sil, Press and Lennox, eventually dissatisfaction by the crew led to changes to the ending, now featuring Sil having a baby that would immediately prove dangerous. 

===Design=== creatures in Steve Johnson and his company XFX, which had already worked with Gigers designs in Poltergeist II. Giger envisioned more stages of Sils transformation, with the film only employing the last, where she is "transparent outside and black inside—like a glass body but with carbon inside" - with XFX doing the translucent skin based on what they had done for the aliens of The Abyss. Sils alien form had both full-body animatronics with replaceable arms, heads and torsos, and a body suit.  Richard Edlunds Boss Film Studios was hired for over 50 shots of computer-generated imagery, which included one of the earliest forms of motion capture effects. Using a two-foot high electric puppet that had sensors translating its movements to a digital Sil, Boss Films managed to achieve in one day what would have once taken as much as three weeks with practical effects. 

Giger was unhappy with some elements he found excessively similar from other movies, particularly the Alien franchise. At a point he sent a fax to Mancuso finding five similarities: a "Chestburster" (as Sil giving birth echoed the infant Alien breaking out of its hosts chest), the creature having a punching tongue (which in Sils case, Giger at first wanted a tongue composed of barbed hooks), a cocoon, the use of flame throwers, and having Giger as the creature designer. A great point of contention was the ending, which Giger considered derivative from the climaxes from both  , and the designer felt that horror films frequently held some final confrontation with fire, "an oldfashioned Middle-Age weapon, like the way they used to burn witches." Giger even sent some ideas for the climax to the producers, with them accepting to have Sils ultimate death occurring by headshot. 

===Filming=== Silver Lake, Pacific Palisades, Biltmore Hotel. Pantages Theater, Brigham City was part of Sils escape. Other locations included the  Santa Monica Pier and the Arecibo telescope in Puerto Rico. The most complex sets involved the sewer complex and a tar-filled granite cavern where the ending occurs. Donaldson wanted a maze quality for the sewers, which had traces of realism&mdash;tree roots breaking through from the ceiling&mdash;and artistic licenses&mdash;in the words of  production designer John Muto, "ours be wider and taller than real sewers, and real sewers dont come equipped with walkways, but the trick was to make it seem claustrophic and real." The underground tunnels were built out of structural steel, metal rod, plaster and concrete to endure the fire effects, and had its design based on the La Brea Tar Pits, with Muto describing them as  "just the sort of place in which a creature from another planet might feel at home."   

==Influence and themes==
Given Sil grows rapidly and kills humans with ease, at a certain point Dr. Laura Baker even speculates if she was sent as a biological weapon by a species that thought humans were like an intergalactic weed. Feldman declared that this was a theme that he wanted to explore more in the script, as it discussed mankinds place in the universe and how other civilization would see us - "maybe were not a potential threat, maybe a competitor, maybe a resource". He also declared that more could be said about Sils existentialism doubts, as she does not know her origins or purpose, and only follows the instinct to mate and perpetuate the species. 

Writing for the Journal of Popular Film & Television, Susan George detailed a study of " rocreation and creation in recent science fiction films," which examined the "representation of procreation and creation in light of the portrayal of technology, scientists, and gender in three science fiction films of the 1990s."    Writing about Species, George compares the character of Fitch to "an updated Dr. Frankenstein," and explores the development of Sils maternal aspirations, which convert the character into an "archaic mother" figure similar to the xenomorph creature in the Alien series, both of which are portrayed "only in a negative light."  George further states that a recurring theme in science fiction films is a typical response to "this kind of powerful female sexuality and alien-ness" in that "the feminine monster must die as Sil does at the end of Species."  Feldman himself considered that an underlying theme regarded "a female arriving and seeking to find a superior mate". 

A five-year investigation into accounts of the chupacabra, a well known cryptid, which was detailed in Benjamin Radfords Tracking the Chupacabra, revealed that the original sighting report of the creature in Puerto Rico by Madeline Tolentino may have been inspired by the creature design of the character Sil in alien form.    According to Virginia Fugarino of Memorial University of Newfoundland writing for the Journal of Folklore Research, Radford "explores the similarities between   report and the alien creature central to the film," and hypothesized that " , which she did see before her sighting, influenced what she believes she saw of the chupacabra."   

==Adaptations==
Yvonne Navarro wrote a novelization based on the original screenplay, with extensive help from Feldman as he read Navarros manuscript and both would communicate with each other to expand the novels content.  The book gives several in depth details about the characters not seen in the film, such as Sils ability to visualize odors and determine harmful substances from edible items by the color. Gas appears black, food appears pink, and an unhealthy potential mate appears to give off green fumes. Other character details include Presss background in tracking down AWOL soldiers as well as the process of decoding the alien signal. Although no clues are given as to its origin, it is mentioned that the message was somehow routed through several black holes to mask its point of origin. 

  released a World of Species sourcebook for its Masterbook role-playing game  system. {{cite book|title=
The World of Species|last=Woodruff|first=Teeuwynn |authorlink=Teeuwynn Woodruff|publisher=	West End Games|year=1995 |isbn=0874313643}} 

==Reception==
 
Species received mixed reviews. It currently holds an approval rating of 35% at   gave the film 2 and a half out of 4 stars, stating "as long as you dont stop to think about whats going on, Species is capable of offering its share of cheap thrills, with a laugh or two thrown in as well". 

==Legacy==
The first sequel to Species, Species II, was released theatrically in 1998. Navarro later authored the novelization for Species II which followed the films original screenplay with added scenes.    Further sequels were released direct-to-video, Species III (2004) and the SyFy|Sci-Fi Channel co-production Species - The Awakening (2007).

==See also==
*List of films featuring diabetes
*Alien (film)|Alien
*Genocyber

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
   
 
 
 
 
 
 
 