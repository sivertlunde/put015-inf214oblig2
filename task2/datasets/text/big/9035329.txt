Aakrosh (1998 film)
{{Infobox film
| name           = Aakrosh:Cyclone of Anger
| image          = Aakrosh.jpg
| caption        = DVD cover
| director       = Lateef Binny
| producer       = Ramesh J. Sharma
| writer         = Sanjay Kumar
| starring       = Sunil Shetty Shilpa Shetty
| music          = Anand Raj Anand
| cinematography = Sanjay Malvankar
| editing        = Kuldip Mehan
| distributor    =
| released       =  
| runtime        = 155 mins
| country        = India
| language       = Hindi
| budget         =
}}
Aakrosh ( ) is a 1998 Bollywood action crime film directed by Lateef Binny starring Sunil Shetty and Shilpa Shetty. The FIlm was a Success at the Box Office

==Plot==
Due to his activities, Anjali Gujral separates from Mahendra Pratap Gujral, and marries Dr. Malhotra. She does bring up her son, Dev from her first marriage, who grows up to become a Police Officer. Years later, on the tracks of Gujral, Dev, and his close colleague, Komal come across Suraj Singh, and find out that he is indeed Gujral. At this point Gujral decides to play on Devs emotions by reminding of his past affection for him, and blaming his entire criminal career on a politician, Rajvansh Shashtri. Dev must now decide to go after Shastri, or disbelieve his father altogether.

==Cast==
*Sunil Shetty ....  Officer Dev Malhotra
*Shilpa Shetty ....  Komal
*Girish Karnad ....  Rajwansh Shashtri
*Suresh Oberoi ....  Mahendra Pratap Gujral/Suraj Singh
*Mohan Joshi ....  Colonel/Chavan
*Johnny Lever ....  Gopi
*Navin Nischol ....  Dr. Malhotra
*Anjana Mumtaz ....  Anjali Malhotra
*Kulbhushan Kharbanda ....  Vikram Dutt
*Suresh Bhagwat ....  Thief
*Ranjeet ....  Sairas
*Shiva Rindani ....  Goonga/Girdhari

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Hello Hello Bolke"
| Abhijeet Bhattacharya|Abhijeet, Kavita Krishnamurthy
|-
| 2
| "Madhosh Ho Gaya Main"
| Abhijeet, Asha Bhosle
|-
| 3
| "Picnic Mein Ho Gaya"
| Udit Narayan, Aditya Narayan, Kavita Krishnamurthy
|-
| 4
| "Tadke Tadke"
| Udit Narayan, Abhijeet
|-
| 5
| "Ujla Ujla"
| Hema Sardesai
|-
| 6
| "Yeh Ladki Hai"
| Udit Narayan, Kavita Krishnamurthy
|}

==External links==
*  

 
 
 
 
 


 
 