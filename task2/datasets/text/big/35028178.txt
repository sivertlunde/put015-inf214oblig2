Fast Charlie... the Moonbeam Rider
{{Infobox film
| name           = Fast Charlie... the Moonbeam Rider
| image          = Fast_Charlie..._the_Moonbeam_Rider.jpg
| caption        = 1979 Promotional poster
| director       = Steve Carver
| producer       = Roger Corman Saul Krugman
| writer         = Ed Spielman Howard Friedlander
| screenplay     = Michael Gleason Stu Phillips
| starring       = David Carradine Brenda Vaccaro L.Q. Jones R.G. Armstrong Terry Kiser Jesse Vint Noble Willingham Ralph James Bill Bartman David Hayward II
| cinematography = William Birch
| editing        = Eric Orner Tony Redman
| studio         = Universal Pictures National Broadcasting Company (NBC)
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $444,334.00 
| Formats        = 
}}

Fast Charlie... the Moonbeam Rider is a 1979 comedy film starring David Carradine and Brenda Vaccaro and directed by Steve Carver. 

==Plot==
During the 1920s, a World War I deserter competes in the first transcontinental motorcycle race from St. Louis, Missouri to San Francisco, California.

==Cast==
David Carradine ... Charlie Swattle 
Brenda Vaccaro ... Grace Wolf 
L.Q. Jones ... Floyd 
R.G. Armstrong ... Al Barber 
Terry Kiser ... Lester Neal 
Jesse Vint ... Calvin Hawk 
Noble Willingham ... Pop Bauer 
Ralph James ... Bill Bartman 
Bill Bartman ... Young Man 
David Hayward II ... Cannonball McCall  
Whit Clay ... Wesley Wolf 
Jack Hunsucker ... Mechanic 

Stunts 
Mike Bast 
Bud Ekins 
David R. Ellis 
Gene Hartline 
Fred Hice 
R.A. Rondell 
Jesse Thomas 
Michael M. Vendrell 
Robert Winters

==References==
 

==External links==
* 
* 

 

 


 