Ego (film)
 
{{Infobox film
| name           = Ego
| image          = 
| caption        = 
| director       = S.Sakthivel
| producer       = V.Periyasamy Ravichandran
| writer         = S.Sakthivel
| starring       = Velu  Anaswara Kumar  Bala Saravanan
| music          = Kash Villainz
| lyrics         = Viveka
| cinematography = A.V.Vasanth
| editing        = M.Bala
| studio         = V.P.Stillz
| distributor    = Vendhar Movies
| released       =  
| runtime        = 116 Minutes
| country        = India
| language       = Tamil
| budget         = 1 Crore
| gross          = 3 Crore
}} Tamil romantic comedy film directed by S. Sakthivel and produced by V.P.Stillz - V.Periyasamy Ravichandran. The film features debutant Velu, Anaswara Kumar and comedian actor Balasaravanan.Ego was released on December 6, 2013 and distributed by Vendhar Movies. This film received mixed reviews from critics.

== Cast ==
*Velu as Eswar
*Anaswara Kumar as Gomathi
*Bala Saravanan as Bala

Soundtrack

Maatikitten

==References==
 
* http://www.nowrunning.com/movie/11418/tamil/eswar-gomathy-ego/cast.and.crew.htm
* http://movies.sulekha.com/tamil/ego/reviews/91225.htm

 
 
 
 


 