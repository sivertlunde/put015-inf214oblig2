Diggers (film)
{{Infobox film
| name           = Diggers
| image          = Diggers.jpg
| caption        = Promotional poster
| director       = Katherine Dieckmann
| producer       = Mark Cuban Todd Wagner David Wain 
| writer         = Ken Marino  Josh Hamilton   Ken Marino   Sarah Paulson   Maura Tierney
| music          = David Mansfield
| cinematography = Michael McDonough
| editing        = Sabine Hoffmann   Malcolm Jamieson
| distributor    = 
| released       =      
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $28 million
| gross          = $24,019,048 
}} South Shore of Long Island, New York, as clam diggers in 1976. Their fathers were clam diggers as well as their grandfathers before them. They must cope with and learn to face the changing times in both their personal lives and their neighborhood.

The movie was written by actor Ken Marino, who also stars.

==Plot==
A coming-of-age story about four working-class friends growing up on Long Island, New York, as clam diggers. Their fathers were clam diggers as well as their grandfathers before them.

==Cast==
*Paul Rudd... Hunt
*Lauren Ambrose... Zoey
*Ron Eldard... Jack Josh Hamilton... Cons
*Ken Marino... Lozo
*Sarah Paulson... Julie
*Maura Tierney... Gina

== Release ==
Diggers was released on April 27, 2007 and premiered on HDNet on April 27, and was released on DVD on May 1. It was released much in the same way as the 2005 film Bubble (film)|Bubble.

==External links==
*  
*  

 
 
 
 
 


 