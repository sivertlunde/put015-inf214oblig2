Lobster Man from Mars
{{Infobox Film
| name           = Lobster Man From Mars
| image          = lobster_man_from_mars_poster.jpg
| caption        = Theatrical release poster
| director       = Stanley Sheff
| producer       = Steven S. Greene Eyan Rimmon Bob Greenberg (idea) Bob Greenberg (screenplay)
| starring       = Tony Curtis Dean Jacobsen Patrick Macnee Deborah Foreman Anthony Hickox S.D. Nemeth Mindy Kennedy Phil Proctor Bobby Pickett Tommy Sledge Billy Barty
| music          = Sasha Matson
| cinematography = Gerry Lively John Peterson
| distributor    = Electric Pictures
| released       =  
| runtime        = 82 mins.
| country        = US
| language       = English
| awards         = 
| budget         = $980,000 (estimated)
| gross          = Unknown
}}
Lobster Man From Mars is a 1989 comedy film directed by Stanley Sheff and starring Tony Curtis. The film is a spoof of B-movie sci-fi films from the 1950s. It had its world premiere at the Sundance Film Festival in 1989.

==Plot==
 
   The Producers by Mel Brooks.

Inside Shelldrakes private screening room the "film within the film" begins. They watch the weird plot unfold: Mars suffers from a severe air leakage. The King of Mars (Bobby Pickett of "Monster Mash" fame) commands the dreaded Lobster Man and his assistant Mombo (a gorilla wearing a space helmet, in a nod to Robot Monster) to pilot his flying saucer to Earth then steal its air. Once landed, the Lobster Man wastes no time transforming hapless victims into smoking skeletons.

On a lonely road, John and Mary, a young and innocent couple (Deborah Foreman and Anthony Hickox) discovers the hiding place of the flying saucer in a dark and mysterious cave. They attempt to warn the authorities but are ignored.  Successfully contacting Professor Plocostomos (Patrick Macnee), a plan is created to lure the Lobster Man to Mr. Throckmortons (Billy Barty) Haunted House that just happens to be surrounded by boiling hot springs. 
 Fred Holliday) and his troops. The house is shelled and destroyed, the Lobster Man flees to his cave, taking Mary with him.

She manages to escape, but the Lobster Man follows. A wild chase ensues, but Professor Plocostomos uses the hot engine coolant from his overheated vehicle to drench Mombo causing his foamy demise. The chase concludes in Yellowstone Park where the dreaded Lobster Man is tricked into walking into the Old Faithful Geyser and a steamy end.

The screening is over. Shelldrake cannot believe his good fortune to witness such a bad movie with potential to lose every cent invested in its distribution and promotion. He buys the production on the spot, but once in release it becomes a big hit and makes a huge profit sending Shelldrake straight to tax prison, with Stevie taking his place as the studios new boy wonder.

==Cast==
* Tony Curtis as J.P. Shelldrake
* Patrick Macnee as Professor Plocostomos
* Deborah Foreman as Mary
* Billy Barty as Mr. Throckmorton
* Anthony Hickox as John Fred Holliday as Colonel Ankrum
* Dr. Demento as The Narrator
* S.D. Nemeth as Dreaded Lobster Man
* Tommy Sledge as Himself
* M.G. Kelly as Dick Strange
* Phil Proctor as Lou
* Bobby Pickett as King of Mars / Astrologer Steve Peterson as The Butler
* William Ackerman as Gas Station Attendant
* Tim Haldeman as Marvin
* Jim Bentley as Rufus Skip Young as Mr. Zip
* Dean Jacobson as Stevie Horowitz
* Mindy Kennedy as Tammy
* Stanley Sheff as Space Bat / Brainex (voices)

==Awards and nominations==
Sundance Film Festival
*1989: Grand Jury Prize &ndash; (nominated)

== See also ==
* Robot Monster

==External links ==
*  
*  
*  

 
 
 
 
 
 
 