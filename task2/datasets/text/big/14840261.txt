Man's Enemy
{{Infobox film
| name           = Mans Enemy
| image          =
| caption        =
| director       = Frank Powell
| producer       =
| writer         = Eric Hudson Charles H. Langdon Frank E. Woods
| starring       = Lillian Gish
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 30 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 short drama film directed by  Frank Powell and starring Lillian Gish. Prints of the film survive in the film archives of the Library of Congress and the Museum of Modern Art.   

==Cast==
* Lillian Gish
* Franklin Ritchie
* Vivian Prescott
* Henry B. Walthall
* Dorothy Gish

==See also==
* Lillian Gish filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 