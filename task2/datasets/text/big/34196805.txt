Life Returns
{{Infobox film
| name           = Life Returns
| image_size     = 
| image	=	Life Returns FilmPoster.jpeg
| caption        =  James P. Hogan
| producer       = Lou L. Ostrow (producer) James P. Mary McCarthy
| narrator       = 
| starring       = See below
| music          = Clifford Vaughan Oliver Wallace
| cinematography = Robert H. Planck
| editing        = Harry Marker
| studio         = 
| distributor    = Universal Pictures
| released       = 1935
| runtime        = 63 minutes 60 minutes (Canada, Ontario)
| country        = USA
| language       = English
| budget = $48,000 Michael Brunas, John Brunas & Tom Weaver, Universal Horrors: The Studios Classic Films, 1931-46, McFarland, 1990 p103 
}}
 James P. Hogan and released by Universal Pictures.

== Plot ==
A doctor who is convinced that the dead can be brought back to life gets the chance to prove his theory on a dog that has recently died.

== Cast ==
*Onslow Stevens as Dr. John Kendrick
*George P. Breakston as Danny Kendrick Lois Wilson as Dr. Louise Stone
*Valerie Hobson as 	Mrs. Kendrick Stanley Fields as Dog Catcher
*Frank Reicher as Dr. James
*Richard Carle as A.K. Arnold
*Dean Benton as Intern
*Lois January as Nurse
*Richard Quine as Mickey
*Maidel Turner as Mrs. Vandergriff
*George MacQuarrie as Judge
*Otis Harlan as Dr. Henderson
*Robert E. Cornish as Himself
*Mario Margutti as Cornishs Assistant William Black as Cornishs Assistant
*Ralph Colmar as Cornishs Assistant
*Roderic Krider as Cornishs Assistant

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 

 