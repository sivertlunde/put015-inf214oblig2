Tommy (1975 film)
 
 
 
{{Infobox film
| name = Tommy
| image = Tommy film poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Ken Russell
| producer = {{Plainlist|
* Ken Russell
* Robert Stigwood}}
| screenplay = Ken Russell
| based on = Tommy (album)|Tommy by The Who
| starring = {{Plainlist|
* Ann-Margret
* Oliver Reed
* Roger Daltrey
* Elton John
* Eric Clapton
* John Entwistle
* Keith Moon
* Paul Nicholas
* Jack Nicholson
* Robert Powell Arthur Brown
* Pete Townshend
* Tina Turner
* The Who}}
| narrator = Pete Townshend
| music = The Who
| cinematography = {{Plainlist|
* Dick Bush
* Ronnie Taylor}}
| editing = Stuart Baird RSO
| distributor = Columbia Pictures
| released =  
| runtime = 108 minutes  
| country = United Kingdom
| language = English
| budget = $5 million 
| gross = $34.3 million 
}}
{{listen
|filename=Pinball Wizard (John) sample.ogg
|title=Pinball Wizard
|description=Elton Johns "Pinball Wizard" from Tommy
|filetype2=Ogg}} musical fantasy fantasy film Arthur Brown, and Jack Nicholson.
 Golden Globe Award for her performance, and was also nominated for the Academy Award for Best Actress. Pete Townshend was also nominated for an Oscar for his work in scoring and adapting the music for the film. The film was shown at the 1975 Cannes Film Festival, but was not entered into the main competition.    In 1975 the film won the award for Rock Movie of the Year in the First Annual Rock Music Awards. 

==Plot==
: (Note: This narrative is given song by song. For formal song playlist and list of song credits, see Tommy (soundtrack).)

;Prologue - 1945
The film begins as a sun lowers behind the horizon as a man stands atop a mountain, followed by several romantic experiences between the man, Royal Air Force Group Captain Walker (Robert Powell), and his wife, Nora (Ann-Margret), among the intimacy of nature. He has been drafted in the military and leaves Nora to fight in the war as a bomber pilot.

;Captain Walker/Its a Boy first day of peace."

;Bernies Holiday Camp Holiday Camp" and starts a relationship with him. Tommy, still only a boy now, hopes to one day own his own holiday camp as he lives on without a real father.

;1951/What about the Boy? original album Broadway play, however, it is Captain Walker who kills his wifes lover.) Tommy is then told that, "you didnt hear it, you didnt see it, you wont say nothing to no-one".

;Amazing Journey
Tommy goes into shock and becomes non-responsive, leading people to believe that he is deaf, muteness|dumb, and Blindness|blind—while Tommy stares at his reflection, his mind internally going on adventures with his dead father.

;Christmas
Nora is distraught at Tommys condition while Frank finds it to be a nuisance; this comes to a head at the Christmas party—where they fear for his soul.

;Eyesight to the Blind Arthur Brown as the priest). Nora tries to get Tommy to try some of the curing methods, but Tommy breaks the Marilyn Monroe statue.

;The Acid Queen
Frank then brings Tommy to the "Acid Queen" (Tina Turner), a prostitute dealing in LSD, who sends Tommy on a wild trip that ultimately fails to awaken him. The experience also causes the Acid Queen to go crazy.

;Do You Think Its Alright? (1) sadistic cousin, Kevin (Paul Nicholas).

;Cousin Kevin
Kevin beats and tortures Tommy, but eventually grows bored of this activity because Tommy cannot respond.

;Do You Think Its Alright? (2)
Frank and Nora go for a night out and leave Tommy with his Uncle Ernie (Keith Moon), a filthy, alcoholic child molester.

;Fiddle About molests Tommy, having at last found a child he can abuse without being caught as Tommy does not know what is happening. The plan backfires when Frank and Nora return home and Frank finds Ernie in bed with Tommy. Ernie is caught in the act.

;Do You Think Its Alright? (3) id appears in the mirror and guides him to a junkyard.

;Sparks
There, he finds a pinball machine, and spends the rest of the night playing on it, until the police bring Frank and Nora to him.

;Extra, Extra, Extra
Becoming an expert at pinball since he cannot see or hear any distractions, Tommy wins game after game, gaining the admiration of local players and making his parents wealthy.

;Pinball Wizard
Tommy now faces the local champ (Elton John) at a televised pinball championship, featuring The Who performing (sans Daltrey) as the champs backing band. To the champs amazement, Tommy keeps out-scoring him. The match ends with the hysterical crowd storming the stage, the band smashing their instruments, and the defeated champ (dressed in Doc Marten boots that are several feet high), falling into the hands of a booing audience who carry him out of the hall. Tommy is hailed as the new champ.

;Champagne
Nora revels in the familys new-found wealth, but sees it as worthless because Tommy is still disabled. Deeply upset, she throws her champagne bottle at a television and hallucinates floods of champagne, beans, laundry detergent, and chocolate pouring from the broken screen.

;Theres a Doctor
Frank announces that he has found a doctor who can help people like Tommy, so they see him the next day.

;Go to the Mirror
The doctor (Jack Nicholson) confirms that Tommys problems are psychosomatic and not physical; Nora watches Tommy staring at the mirror and wonders, "What is happening in his head?"

;Tommy, Can You Hear Me?
On the way home, Nora tries to get Tommys attention, but with no avail.

;Smash the Mirror!
Amazed that Tommy can see himself but not her, Nora pushes him into the mirror, shattering it.

;Im Free messianistic vision enlightenment by playing pinball.

;Mother and Son messianistic vision—saying, "Pinball ... is far beyond a game beyond your wildest dreams. Those who love me have a higher path to follow now," and then "baptism|baptizing" her.

;Miracle Cure
With a "T" topped with a sphere (a pinball) as his symbol, Tommy starts holding rallies and lectures, with Uncle Ernie selling merchandise.

;Sally Simpson
Sally Simpson (Victoria Russell), a reverends young daughter obsessed with Tommy, begs her parents to let her go to one of his sermons. Furious when they deny her permission, she spends all afternoon getting ready and sneaks out of her house to the sermon, which takes the form of a wild concert set to gospel music. Sally sits at the front row and as the police desperately try to control all the screaming girls, Sally pushes past onto the edge of the stage, attempting to touch Tommy. Frank, sitting behind Tommy onstage, kicks her away. Sally gashes her face on a chair and the ambulance men carry her out. She grows up to marry a green-skinned, guitar playing rock star who is a cross between a cowboy and Frankensteins monster (Gary Rich). Her parents are distraught that their daughter has become a groupie. Sally forevermore carries a horrific scar streaked across her cheek to remember Tommy by.

;Sensation
In just a year, Tommy declares himself a "sensation" and begins to have a profound impact on people whenever he nears them, including motorcycle gangs and slot-machine gambling Teddy Boys. As Tommy flies above them in a hang-glider, his mere presence converts them and many others to a new life according to his beliefs, as opposed to their previous wicked ways.

;Welcome
At Tommys invitation, masses of people begin to gather at his house, seeking spiritual fulfillment. However, the house is not big enough to accommodate the massive population, so Tommy decides to open a holiday camp, his lifelong wish from the beginning of the movie.

;T.V. Studio Tommy camp" in every major city in the world.

;Tommys Holiday Camp
Crowds flock to Tommys "holiday camp". They arrive by the bus-load, finding Uncle Ernie to welcome them. Sitting atop a motorised church organ that doubles as a cash register, Uncle Ernie sings of the joys of the camp while also selling Tommy merchandise to the crowds.

;Were Not Gonna Take It
A frustrated crowd begins clamouring for Tommy to bring them enlightenment. Tommy begins preaching, bans drinking and smoking, and has each follower wear a headgear that blinds, deafens, and silences them and then they are taken to a pinball machine. The followers do not find Tommys enlightenment, start rioting, destroying the machines, and set fire to the camp. They retreat at the sound of a police siren. Frank and Nora die in the attack.

;See Me, Feel Me/Listening To You
Tommy, only mildly injured, flees as flames engulf the camp, and "rejects" the "messianism|religion" he has built around himself. As Tommy escapes, he arrives at the same place in the wilderness in the beginning of the film where his parents spent a romantic day together (presumably the day he was conceived). In the final shot, Tommy greets a rising sun, and a new dawn. 

==Cast==
 
* Ann-Margret as Nora Walker
* Oliver Reed as "Uncle" Frank Hobbs
* Roger Daltrey as Tommy Walker
** Barry Winch as young Tommy
* Elton John as The Pinball Wizard
* Tina Turner as The Acid Queen
* Eric Clapton as The Preacher
* John Entwistle as Himself
* Keith Moon as Uncle Ernie/Himself
* Paul Nicholas as Cousin Kevin
* Jack Nicholson as The Specialist
* Robert Powell as Group Captain Walker Arthur Brown as The Priest
* Pete Townshend as The Narrator/Himself
* Victoria Russell as Sally Simpson
* Ben Aris as Rev. A. Simpson V. C.
* Mary Holland as Mrs. Simpson
* Ken Russell as Cripple
 

==Production== commentary for the 2004 DVD release of the film, Ken Russell stated that the opening and closing outdoor scenes were shot in the Borrowdale valley of the English Lake District, near his own home, the same area that he had used to double for Bavaria in his earlier film Mahler (film)|Mahler, in which Robert Powell had starred. Much of the film was shot on locations around Portsmouth, including the scene near the end of the film featuring the giant pinballs, which were in fact obsolete buoys found in a British Navy yard, which were simply sprayed silver and filmed in situ. The Bernies Holiday Camp ballroom sequence was shot inside the Gaiety Theatre on South Parade Pier. Exterior shots were filmed at Hilsea Lido.    The Sally Simpson interior sequence was filmed in the Wesley Hall in Fratton Road Portsmouth. The exterior intro sequence to the scene however shows Sally Simpson buying a badge and entering South Parade Pier.
 Kings Theatre in Southsea,  others on Portsdown Hill, which overlooks Portsmouth and local churches were used. All Saints in Commercial Road was used for the Sally Simpson Wedding scene whilst the meeting in the same sequence was filmed at the Wesley Hall in Fratton Road. The Eyesight to the Blind sequence was filmed at St Andrews Church in Henderson Road in Southsea.The other church featured was Warblington Church near Havant in Hampshire.
 The Three Musketeers, and the dress worn by the Queen in the Rex ad is that worn by Geraldine Chaplin in the earlier film. 

Elton John initially turned down the role of the Pinball Wizard and among those considered to replace him was David Essex, who recorded a test audio version of the "Pinball Wizard" song. However, producer Robert Stigwood held out until John agreed to take the part, reportedly on condition that he could keep the gigantic Dr. Martens boots he wore in the scene. Russell also recalled that Townshend initially balked at Russells wish to have The&nbsp;Who performing behind Elton in the sequence (they did not perform the audio here), and also objected to wearing the pound-note suits (which were in fact stitched together from novelty pound-note teatowels).  On The Whos involvement with the film, members Daltrey played the title character, Moon played, in essence, a dual role as both Uncle Ernie and as himself along with Entwistle and Townshend lip-synching on their respective instruments in the "Eyesight to the Blind" and "Pinball Wizard" segments.

==Quintaphonic Sound== QS from DBX noise reduction on the magnetic tracks.

Unlike the usual multiple small surround speakers used in cinemas, the Quintaphonic system specified just two rear speakers, but of the same type as those used at the front.

One problem that arose was that by the 1970s the four-track magnetic sound system was largely moribund. Only a few theatres were equipped with the necessary magnetic playback heads etc. and of those that did in many cases it was not in working order. So in addition to installing the extra electronics and rear speakers John Mosely and his team had to repair and align the basic magnetic playback equipment. So each theatre that showed Tommy using the Quintaphonic system had to be specially prepared to take the film. In this respect there is a similarity between Tommy and Walt Disneys Fantasia (1940 film)|Fantasia, for which a special sound system (Fantasound) had been devised and required each theatre that showed it in the original release to be specially prepared. Also, like Fantasound, Quintaphonic Sound was never used again.

Tommy was later released with mono, conventional four-track magnetic and Dolby Stereo soundtracks.

==Changes from album==
 
The film version of Tommy differs in numerous ways from the original 1969 album. The primary change is the period, which is moved forward to the post-World War II era, while the original album takes place just after World War I. As a result, the song "1921" (alternatively named "You Didnt Hear It") is renamed "1951"  and the opening line "got a feelin 21 is gonna be a good year" changes to "got a feelin 51 is gonna be a good year". The historical change allowed Russell to use more contemporary images and settings.

In the album, Group Captain Walker is shot down in wartime, never knowing his unborn son. In the film, the wifes lover (Reed) kills Walker in front of Tommy, heightening the psychological trauma and allowing Captain Walker to be in Tommys secret fantasies.
 The Wall), On a Clear Day You Can See Forever appeared only in the now-lost roadshow version) – perform the songs in character instead of The Who, with the exception of Daltrey as Tommy and where Townshend sings narration in place of recitative.
 soundtrack album.

Major differences between the 1969 and 1975 versions:
* The film opens with a new instrumental, "Prologue - 1945" (partly based on the 1969 "Overture"), which accompanies the opening sequences of Captain Walkers romance and disappearance.
* "Its A Boy" is separated from "Overture" and becomes the medley "Captain Walker/Its A Boy"; in the film this medley narrates the aftermath of Walkers disappearance, the end of the war and the birth of Tommy.
* A new song, "Bernies Holiday Camp", which follows "Captain Walker/Its A Boy", portrays Tommys childhood and his mothers romance with Hobbs (Oliver Reed). The song also features the melody from, and even foreshadows, "Tommys Holiday Camp". climaxes with the return of Tommys father and his killing.
* "The Amazing Journey" (shortened to three minutes) has almost completely different lyrics, and the "guide" from the album is depicted as Tommys dead father. Arthur Brown is cast as the character The Priest in the film, and sings a verse in the song but is not featured on the soundtrack. According to Russells DVD audio commentary, the concept of people literally worshipping celebrities (in this case Marilyn Monroe) and several other elements in the film were adapted from his pre-existing treatment for a film about false religions, which he had developed prior to Tommy but for which he had never been able to secure financial backing.
* The running order of "The Acid Queen" and "Cousin Kevin" is reversed.
* "Underture" is removed but parts from it have been re-arranged as "Sparks".
* "The Acid Queen", "Cousin Kevin", "Fiddle About", and "Sparks", linked by three renditions of "Do You Think Its Alright?", form an extended sequence depicting Tommys inner journey and his trials.
* A three-minute version of the "Sparks" theme (with, then new, synthesiser orchestration) precedes "Extra, Extra, Extra" and "Pinball Wizard". In the film it is used behind the sequence of the dazed Tommy wandering into a junkyard and discovering a pinball machine. The music on the film soundtrack (for this and many other songs) is heavily edited, however, and is a noticeably different mix from the version on the soundtrack album.
* A new linking theme, "Extra, Extra, Extra", narrates Tommys rise to fame and introduces the battle with the pinball champ. It is set to the tune of "Miracle Cure" and precedes "Pinball Wizard".
* "Pinball Wizard" has extra lyrics and movements. It features guitar and keyboard solos (the guitars are only readily discernible on the soundtrack album), and an outro with a riff reminiscent of the Whos first single, "I Cant Explain".
* A new song, "Champagne", which follows "Pinball Wizard", covers the sequence of Tommys stardom and wealth and his parents greed. Like many other songs, it features Tommy singing "See Me, Feel Me" interludes: this is the first song with Daltrey singing for Tommy. In the film (but not on the soundtrack), the song is introduced by a mock TV commercial—reminiscent of the Whos early years when they made jingles.
* "Go to the Mirror" is shortened, not featuring the elements of "Listening To You", nor the phrase "Go to the mirror, boy." psychedelic sequence depicting Tommys reawakening.
* "Im Free" is followed by a new song, "Mother and Son", which depicts Tommys rejection of materialism and his vision for a new faith based around pinball. cassette and CD versions, LP version.)
* "Sally Simpson" is re-arranged with a Bo Diddley beat and in the film is preceded by "Miracle Cure"—which features an extra verse.
* In "Sally Simpson", the album version mentions her fathers Rolls-Royce (car)|Rolls-Royce as blue, but the film changes the lyrics to black (the Rolls-Royce in the film is also black).
* In "Sally Simpson", the album version describes Tommy giving a lesson. In the film, Tommy gives a lesson, and the lyrics are changed to the words of the lesson.
* In the album version of "Sally Simpson", the title character jumps on the stage and brushes Tommys cheek, but in the movie she is kicked off the stage before she can get close to Tommy.
* A new linking piece, "T.V. Studio", is used between "Welcome" and "Tommys Holiday Camp".
* The 1969 albums closing track "Were Not Going To take It" is split into two pieces, "Were Not Gonna take It" and medley "See Me Feel Me/Listening To You"; Note: "See Me, Feel Me/Listening To You" is the name given in the song book for the soundtrack album; the soundtrack album itself, counter-intuitively, calls it "Listening To You/See Me, Feel Me".  this covers the climactic film sequences of Tommys fall from grace and his final redemption.
* The CD reissue of the soundtrack album opens with a newer, previously unreleased version of "Overture From Tommy", which was not included either in the film or on the original soundtrack LP. Although the track is listed in the CDs song credits as being performed by The Who, it is actually a Pete Townshend solo number with him playing all the instruments (as with "Prologue - 1945" and other tracks) – neither John Entwistle nor Keith Moon appear on it, as they do on all other selections on the soundtrack credited to "The Who," regardless of whether Roger Daltrey performs as vocalist.

==Soundtrack==
 
;Sales chart performance
;Album
{| class="wikitable"
|-
!Year
!Chart
!Position
|- 1975
|Billboard (magazine)|Billboard Pop Albums 2 
|- 1975
|UK Chart Albums 21 
|}

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 