Here Comes the Groom (1934 film)
{{Infobox film
| name           = Here Comes the Groom
| image          = 
| alt            = 
| caption        =
| director       = Edward Sedgwick
| producer       = Charles R. Rogers
| screenplay     = Richard Flournoy Casey Robinson Neil Hamilton Patricia Ellis Isabel Jewell Lawrence Gray Sidney Toler
| music          = Herman Hand 
| cinematography = Henry Sharp Milton R. Krasner 
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Neil Hamilton, Patricia Ellis, Isabel Jewell, Lawrence Gray and Sidney Toler. The film was released on June 22, 1934, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Jack Haley as Mike Scanlon
*Mary Boland as Mrs. Widden Neil Hamilton as Jim
*Patricia Ellis as Patricia Randolph
*Isabel Jewell as Angy
*Lawrence Gray as Marvin Hale
*Sidney Toler as Detective Weaver
*E. H. Calvert as George Randolph
*James P. Burtis as 1st Cop
*Ward Bond as Second Cop James Farley as Third Cop
*Fred Toones as Porter
*Arthur Treacher as Butler Ernie Adams as 1st Gunman
*Eddie Sturgis as 2nd Gunman

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 