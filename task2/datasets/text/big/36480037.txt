The China Plate
{{Infobox Hollywood cartoon
| name              = The China Plate
| series            = Silly Symphonies
| image             =
| image size        = 
| alt               = 
| caption           = 
| director          = Wilfred Jackson
| producer          = 
| story artist      = 
| narrator          = 
| voice actor       =   
| musician          = Frank Churchill Jack Cutting Dick Lundy David Hand Jack King
| layout artist     = 
| background artist = Carlos Manriquez  Emil Flohri Walt Disney Productions
| distributor       = Columbia Pictures
| released          = May 25, 1931
| color process     = Black and white
| runtime           = 7 minutes
| country           = United States
| language          = English
| preceded by       = Mother Goose Melodies
| followed by       = The Busy Beavers
}}

The China Plate is a 1931 Silly Symphonies animated film.

==Plot==
The short is based on the Willow pattern legend, with some major differences, including a dragon. In the short, there is an oriental scene (Willow pattern) on a China plate that comes to life. Once it comes to life, it then tells the story of two young lovers who are disturbed by not only an overweight Emperor but also a dragon that not only eats the emperor but also breathes fire.   

==References==
 

 

 
 
 
 
 
 
 
 


 