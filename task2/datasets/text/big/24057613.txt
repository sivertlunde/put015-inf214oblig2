Libera me (1993 film)
{{Infobox film
| name           = Libera me
| image          = Libera me (film).jpg
| caption        = Film poster
| director       = Alain Cavalier
| producer       = 
| writer         = Alain Cavalier Bernard Crombey Andree Fresco
| starring       = Louis Becker
| music          = 
| cinematography = Patrick Blossier
| editing        = Marie-Pomme Carteret
| distributor    = 
| released       = 17 November 1993
| runtime        = 75 minutes
| country        = France
| language       = 
| budget         = 
}}

Libera me is a 1993 French experimental film directed by Alain Cavalier. It was entered into the 1993 Cannes Film Festival.   

==Cast==
* Louis Becker
* Catherine Caron
* Paul Chevillard
* Annick Concha as Mother
* Pierre Concha as Father
* François Cristophe
* Cécile Haas as Helpers girlfriend
* Michel Labelle as Butcher
* Thierry Labelle as Older son
* Jean Monot
* Michel Quenneville as Photographer
* Claire Séguin as Blonde woman
* Philippe Tardif as Helper
* Christophe Turrier as Younger son

==References==
 

==External links==
* 

 
 
 
 
 
 
 