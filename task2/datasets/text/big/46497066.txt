The Man of the Hour
{{Infobox film
| name = The Man of the Hour
| image =
| image_size =
| caption =
| director = Julien Duvivier
| producer =  Julien Duvivier     José Marquis
| writer =  Julien Duvivier      Charles Spaak    Charles Vildrac
| narrator =
| starring = Maurice Chevalier   Elvire Popesco   Josette Day   André Alerme
| music = Jean Wiener   
| cinematography = Roger Hubert   
| editing =     Marthe Poncin 
| studio = Les Films Marquis
| distributor = Distribution Parisienne de Films 
| released = 26 February 1937
| runtime = 93 minutes
| country = France French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Man of the Hour (French:Lhomme du jour) is a 1937 French musical film directed by Julien Duvivier and starring Maurice Chevalier, Elvire Popesco and Josette Day.  The films sets were designed by the art director Jacques Krauss. An ordinary man saves a great actresses life by giving blood, and she in turns decides to promote him as a singing star.

==Partial cast==
*  Maurice Chevalier as Alfred Boulard / Himself  
* Elvire Popesco as Mona Thalia  
* Josette Day as Suzanne Petit  
* Raymond Aimos as Le vieux cabot 
* André Alerme as François-Théophile Cormier de la Creuse  
* Marguerite Deval as Mme Christoforo  
* Marcelle Géniat as Alphonsine Boulard - la mère dAlfred  
* Paulette Élambert as La petite soeur de Suzanne  
* Robert Lynen as Milot - lapprenti électricien  
* Marcelle Praince as Mme Pinchon - une pensionnaire  
* Robert Pizani as Le poète efféminé  
* Pierre Sergeol as La Breloque  
* Charlotte Barbier-Krauss as Mme Legal - la patronne de la pension de famille  
* Simone Deguyse as La petite femme à la terrasse du café  
* Renée Devillers as Fanny Couvrain - la fille aux fleurs  
* Fernand Fabre as Le peintre à la réception  
* Jeanne Fusier-Gir as La chanteuse à laudition  
* Germaine Michel as La bouchère 
* Missia as La dame du monde à la réception

== References ==
 

== Bibliography ==
* Lucy Mazdon & Catherine Wheatley. Je T Aime... Moi Non Plus: Franco-British Cinematic Relations. Berghahn Books, 2010.

== External links ==
*  

 

 
 
 
 
 
 

 

 