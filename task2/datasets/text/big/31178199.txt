Vellinakshatram (1949 film)
 
 
{{Infobox film
| name           = Vellinakshatram
| image          = Vellinakshthram-1949.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Felix J. Beyse
| producer       = 
| story          = 
| screenplay     = 
| starring       = 
| music          = 
| cinematography = 
| editing        =
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}}

Vellinakshatram is a 1949 Malayalam film directed by Felix J. Beyse.  It was the first film produced at the Udaya Studios.  Its music is composed by B. A. Chidambaranath, who debuted with this film.  The film stars Gayaka Peethambaram, Ambujan, Kuttanadu Ramakrishna Pillai, Miss Kumari, Lalitha, Alleppey Vincent, Kandiyoor Parameshwaran Pillai, Cherayi Ambujam and Baby Girija.

Not even a single frame from the footage of this film is available now. What is left of this film is the songs book.  The film was a miserable failure at the box office too. 

==References==
 

==External links==
*  
*   at the Malayalam Movie Database
*http://www.malayalachalachithram.com/movie.php?i=7

 
 
 


 