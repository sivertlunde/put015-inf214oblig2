Thirayum Theeravum
{{Infobox film 
| name           = Thirayum Theeravum
| image          =
| caption        =
| director       = K. G. Rajasekharan
| producer       =
| writer         =
| screenplay     =
| starring       = Prem Nazir Jayabharathi Aranmula Ponnamma Jayaprabha
| music          = G. Devarajan
| cinematography =
| editing        = K Sankunni
| studio         = KC Productions
| distributor    = KC Productions
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film,  directed by K. G. Rajasekharan. The film stars Prem Nazir, Jayabharathi, Aranmula Ponnamma and Jayaprabha in lead roles. The film had musical score by G. Devarajan.  
==Cast==
*Prem Nazir
*Jayabharathi
*Aranmula Ponnamma
*Jayaprabha
*MG Soman Ravikumar

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Gaaname manojnja sooname || K. J. Yesudas, P. Madhuri || Yusufali Kechery || 
|-
| 2 || Leelaathilakam Nananju || K. J. Yesudas, Vani Jairam, Chorus || Yusufali Kechery || 
|-
| 3 || Thedum mizhikale || Vani Jairam || Yusufali Kechery || 
|-
| 4 || Vassantha Chandralekhe || K. J. Yesudas, P. Madhuri || Yusufali Kechery || 
|}

==References==
 

==External links==

 
 
 

 