The Counselor
 
 
{{Infobox film
| name           = The Counselor
| image          = The Counselor Poster.jpg
| image_size     = 220px
| border         = yes
| alt            =
| caption        = Theatrical release poster
| director       = Ridley Scott
| producer       = {{Plain list |
*Ridley Scott Nick Wechsler
*Steve Schwartz
*Paula Mae Schwartz
}}
| writer         = Cormac McCarthy
| starring       = {{Plain list |
*Michael Fassbender
*Penélope Cruz
*Cameron Diaz
*Javier Bardem
*Brad Pitt
 
}}
| music          = Daniel Pemberton
| cinematography = Dariusz Wolski
| editing        = Pietro Scalia
| production companies= {{Plain list |
*Scott Free Productions Nick Wechsler Productions
*Chockstone Pictures
*Fox 2000 Pictures
}}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 118 minutes  
| country        = {{Plain list |
*United Kingdom
*United States
}}
| language       = English
| budget         = $25 million 
| gross          = $71 million 
}}
 spelled The thriller film Ciudad Juarez, Mexico / Texas border area—as well as Penélope Cruz, Cameron Diaz, Javier Bardem and Brad Pitt. The film deals with themes of greed, death, the primal instincts of humans and their consequences.
 Morelia Film Festival and also played the Cork Film Festival. The London premiere was held on October 3, 2013 in Leicester Square. The New York City premiere was held on October 9, 2013. The film has received mixed reviews from critics and was theatrically released on October 25, 2013. It is dedicated to Scotts brother, Tony Scott|Tony, who died in 2012.

==Plot==
A man, known only as "The Counselor" (Michael Fassbender), and his girlfriend Laura (Penélope Cruz) are lying in bed and speaking to one another in an increasingly suggestive and erotic manner. Meanwhile, somewhere in Mexico, cocaine is packaged in barrels and concealed in a sewage truck, driven across the border and stored at a sewage treatment plant.

After the Counselor goes to Amsterdam to meet with a diamond dealer (Bruno Ganz) to purchase an expensive engagement ring for Laura, he proposes and she accepts. He has expensive tastes, driving a Bentley and wearing elegant suits. At a party thrown by Reiner (Javier Bardem) and girlfriend Malkina (Cameron Diaz), the Counselor discusses a nightclub he and Reiner intend to run, as well as the Counselors interest in an upcoming drug deal, which would be his first.

The Counselor meets with Westray (Brad Pitt), a business associate of Reiners. He hears of the deals four-thousand percent return rate, but Westray warns The Counselor about becoming involved in such a deal, saying that Mexican cartels are merciless. Just before this, as the Counselor begins to fully resolve himself in the drug deal, Reiner describes an execution device called "the bolito" which gradually strangles/decapitates the victim. Reiner later describes how disturbed he was from an incident involving him, Malkinas vulva and his cars windshield which reminded him of the fish in aquariums that suck on the glass walls.
 biker and cartel member known as "The Green Hornet" recently arrested for speeding. The Counselor agrees to bail him out of jail.

Malkina, a ruthless criminal herself, employs "The Wireman" (Sam Spruell) to steal the drugs. He does this by decapitating the biker with a wire stretched across the highway. After collecting the component that will allow the sewage truck to start, The Wireman drives to the sewage treatment plant, where he steals the truck containing the cocaine.

Learning of this incident, Westray meets with The Counselor to notify him that The Green Hornet is dead and that the cocaine has been stolen, bleakly intoning The Counselors culpability. Westray says he is leaving town immediately and suggests The Counselor do the same. Westray explains that the cartels ruthlessness extends to creating "snuff films" where murder victims are filmed on camera. The Counselor makes an urgent call to Laura, arranging to meet her in another state, where he will explain.

The cartel has learned that The Counselor bailed out The Green Hornet, which appears as suspect timing and fully blameworthy for the punitive purposes of the cartel. In Texas, two cartel members pretending to be police officers pull over The Wireman and his accomplice. The accomplice shoots and kills one of the imposter "police officers" and wounds the other. The wounded cartel member manages to kill the accomplice and The Wireman, also gunning down an innocent driver who comes upon the scene.

Reiner is accidentally killed by cartel members while they are attempting to capture him. The cartel then kidnaps Laura. In a last-ditch effort, The Counselor contacts Jefe (Rubén Blades), a high-ranking cartel member, for suggestions on what to do next. Jefe philosophically advises The Counselor to live with the choices he made long beforehand.

The Counselor goes to Mexico, hoping to find and rescue Laura there. A package is slipped under the door of his hotel room and in it he finds a DVD with "Hola!" written on it, breaking down at the awareness that the disk likely contains a Snuff film of Laura sent by the cartel. The next shot shows Lauras body being dumped into a landfill.

The cartel is unable to track Westray or Malkina. Malkinas failed effort to steal the drugs means that she is out of the money she wants. She tracks Westray to London where she hires a woman (Natalie Dormer) to seduce him and steal his bank codes. She then hires accomplices to steal Westrays laptop, and in the process they kill him with the "bolito" device that Reiner had previously described to The Counselor. The movie ends with Malkina meeting her banker (Goran Višnjić) at a restaurant, coolly explaining how she wants her accounts to be handled and discussing how exciting it is to see a predator elegantly catch its prey, then ordering lunch.

==Literary themes==
Writing for Serpent Box on October 28, 2013, Vincent Carrella identified the Spanish poet Antonio Machado as the source of the poetic verses used by the cartel kingpin, The Jefe, when speaking to The Counselor. In the second half of the film, The Jefe recites directly from the poem to The Counselor, “Caminante, no hay camino. Se hace camino al andar,” which translates in its original context as: wanderer, there is no road, the road is made by walking. This passage is taken from Machados poem Campos de Castilla with Machados reflections upon the prospects of his own life after learning of his wife being diagnosed with terminal tuberculosis from which she would die within a year. The Jefe uses the poem to inform The Counselor of his own impending demise. In the film, The Jefe concludes by telling The Counselor, “You are the world you have created. And when you cease to exist, that world you have created will also cease to exist.” 

==Cast==
* Michael Fassbender as The Counselor, a lawyer who delves into the dark world of trafficking through his business partner Reiner. He was married previously and is now engaged to Laura. His name is never revealed. Reiner had propositioned The Counselor two years prior to get involved in the drug business, but he originally said no.
* Cameron Diaz as Malkina. The name Malkina comes from Grimalkin meaning "an evil looking female cat." She is a pathological liar and a Psychopathy#Sociopathy|sociopath, an immigrant who is now living the high-life after escaping a sordid past as an exotic dancer. Although Malkina does come from a background of little means (she never knew her parents), she is smart and calculating. She hides her psychological scars through her appearance, wearing large jewelery and extravagant clothes to draw attention to herself. Malkina has a cheetah print tattoo and owns two pet cheetahs (Raoul and Silvia). She is the girlfriend of Reiner, and together they are a powerful couple in the underground drug world. She is always aware of what is going on around her, and in control at all times. She is also more talented in gathering intelligence and locating people than the Mexican Drug Cartel even though the Drug Cartel is formed of an Elite Ex-Military Army Unit of highly trained and dangerous individuals.
* Javier Bardem as Reiner, a charismatic entrepreneur by day and an underground drug kingpin by night. Reiner is the boyfriend of Malkina and they live an extravagant life together. His relationship with Malkina is volatile yet they are similar in nature. Reiner knows he is not the smartest of men and deep down fears this will eventually lead to his demise. He likes to live dangerously and carefree. Jeremy Renner and Bradley Cooper were considered for the role before Bardem was cast.    
* Penélope Cruz as Laura, a naïve and generally positive, religious woman who values tradition. She is in a long-distance relationship with The Counselor and they soon become engaged. She senses that she may not know what The Counselor does while she is away yet does not let this get in the way of their relationship. Her failure to see the evil in those around her could cost her everything.  Her real-life husband Javier Bardem also appears in the film, marking their first film together (although they do not share scenes together) since Vicky Cristina Barcelona.
* Brad Pitt as Westray, a womanizing, charismatic middleman and a friend of Reiners who meets with The Counselor to develop the deal. Westray understands the drug world better than Reiner but his vices for women and alcohol could prove to be a fatal flaw.
* Rosie Perez as Ruth, a prison convict. Through court-appointment she is represented by The Counselor for murder charges.
* Richard Cabral as The Young Man/The Green Hornet; Ruths son and a dangerous, high-ranking member of the cartel with a target on his back.
* Natalie Dormer as The Blonde, a young woman hired by Malkina to help her carry out her plans by seducing Westray.
* Édgar Ramírez as The Priest, whom Malkina goes to speak with.
* Bruno Ganz as The Diamond Dealer, who sells Lauras engagement ring to The Counselor.
* Rubén Blades as Jefe, a senior member of the cartel.
* Goran Višnjić as Michael, a banker friend of Malkinas she meets with to discuss her future plans.
* Toby Kebbell as Tony, an old client of The Counselors.
* Emma Rigby as Tonys Girlfriend.
* John Leguizamo as Randy, a cartel member in Chicago from Colombia.
* Dean Norris as The Buyer, a man who buys drugs from Randy in Chicago.
* Donna Air as The Chauffeur.
*   as Abogado Hernandez, a lawyer The Counselor goes to see for help.
* Sam Spruell as Jaime "The Wireman", a criminal hired by Malkina.
* Richard Brake as The Wiremans accomplice.
* Alex Hafner as Highway Patrolman.
* Velibor Topic as Sedan Man.

==Production==

===Pre-production=== Nick Wechsler, film adaptation of McCarthys novel The Road.    On January 31, it was reported that Ridley Scott was currently considering several directorial projects, but that there was a strong possibility that The Counselor would be his next film and his follow-up to Prometheus (2012 film)|Prometheus.    On February 9, it was confirmed that Scott would direct.    Scott also became a producer. Cormac McCarthy, Mark Huffam, Michael Schaefer and Michael Costigan are executive producers.

===Filming===
Principal photography began on July 27, 2012 in London. The film was also shot in Spain and the United States.    On August 20, 2012, Scott halted production of the film due to his brother Tony Scott|Tonys death. He canceled that weeks shoot in order to travel to Los Angeles to be with his brothers family.  Scott returned to London to resume production on September 3. 

The film was dedicated to the memory of Tony Scott, who had taken his own life during production, and Matt Baker, the second assistant director on the film who had since died.  Only the Baker dedication is seen onscreen, however.

The first teaser trailer was released June 25, 2013.

===Design===
Costume designer Janty Yates collaborated with Giorgio Armani on the film as a part of a new partnership between Armani and 20th Century Fox that also extends into retail and digital initiatives.  Armani was enlisted to create the wardrobes for the characters portrayed by Michael Fassbender and Penélope Cruz.  In addition to Armani, designer Paula Thomas also contributed to the films wardrobe by dressing Cameron Diazs character, Malkina, with roughly 15 different outfits.  "  I read the script that I realized why   called upon me," said Thomas. "  character has a lot of elements of a Thomas Wylde woman.   bold, edgy, modern. She’s about wanting to be seen, as opposed to blending into the background."   

For Javier Bardems character, Yates applied a widely colorful wardrobe that was mostly made up of pieces of Versace.  As for Bardems hair, the idea was the actors own and inspired by film producer Brian Grazers hairdo. 

===Music===
The film score to The Counselor was composed by Daniel Pemberton.  Pemberton recorded the score with a full orchestra at Abbey Road Studios in addition to integrating home-recorded guitar noises and textures.  "Ridley responds really well to interesting and unusual sounds," explained Pemberton on the composer-director relationship. "So as a composer who likes making unusual sounds, that’s exciting. It was daunting but he was great to work with and up for experimenting.   He made the process a lot less scary than it should have been." 
A soundtrack album was released digitally on October 22 and in physical forms on November 11, 2013 by Milan Records. 

==Reception==

===Box office===
Preliminary reports had The Counselor tracking for an $8.6–$13 million debut in North America.   The film opened to $3.2 million in 3,336 locations on Friday and opened at #4 in the box office with just a cumulative $7,842,930 over the weekend. 

===Critical response===
The film received mixed reviews. It currently holds a 34% approval rating on review aggregate website   the film has a score of 48 based on 42 reviews, considered to be "mixed or average reviews".  It received a very negative grade of a "D" from market-research firm CinemaScore. 

Todd McCarthy of The Hollywood Reporter gave the film a negative review, calling it "not a very likable or gratifying film," adding that "one is left with a very bleak ending and an only slightly less depressing sense of the waste of a lot of fine talent both behind and in front of the camera."  Mark Kermode listed it as number two on his Ten Worst Films of 2013.  The Los Angeles Times Kenneth Turan stated, "As cold, precise and soulless as the diamonds that figure briefly in its plot, The Counselor is an extremely unpleasant piece of business."  Peter Debruge of Variety (magazine)|Variety criticized Cormac McCarthys script, saying that his "first original script is nearly all dialogue, but its a lousy story, ineptly constructed and rendered far too difficult to follow." 
 Shakespearean consequences of one man’s irrevocable act of avarice" and called it "a bloody great time."  In addition, Manohla Dargis of The New York Times gave the film a rave review, stating that "Mr. McCarthy appears to have never read a screenwriting manual in his life   Thats a compliment."  Danny Leigh of the BBC film review programme Film 2013 praised the film, saying that "the real star is the script. What this film really is is a Cormac McCarthy audiobook with visuals by Ridley Scott. Its black as night, engrossing and masterful." He also acclaimed the performances, particularly Diazs, and said, with regard to the negative reviews, "Movie history is littered with films that we all sneered at and we all laughed at and we all thought were terrible and the critics hated them and no-one went to see them, and then 40 years later they fetch up on programmes like this with everyone saying what a masterpiece!" 
 Point Blank and the screenplay to the work of David Mamet, Harold Pinter, and Quentin Tarantino. Foundas writes, "  is bold and thrilling in ways that mainstream American movies rarely are, and its rejection suggests what little appetite there is for real daring at the multiplex nowadays." 

==Awards==
{| class="wikitable"
|-
! Year !! Group !! Award !! Result !! Notes
|-
| 2014 || London Critics Circle Film Awards  || British Actor of the Year||   ||  Michael Fassbender
|-
| 2014 || MTV Movie Awards || Best WTF Moment||   ||  Cameron Diaz
|}

==References==
 

==External links==
*  
*  
*   at Rotten Tomatoes
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 