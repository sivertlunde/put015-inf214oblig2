Sheba (film)
{{Infobox film
| name           = Sheba
| image          =
| caption        =
| director       = Cecil M. Hepworth 
| producer       = 
| writer         = Rita (novel)   Blanche McIntosh 
| starring       = Alma Taylor   Gerald Ames   James Carew 
| cinematography = 
| editing        = 
| studio         = Hepworth Films
| distributor    = Butchers Film Service
| released       = October 1919
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama film directed by  Cecil M. Hepworth and starring Alma Taylor, Gerald Ames and James Carew.  The film is notable for an early appearance of Ronald Colman in a small part.

==Cast==
* Alma Taylor as Sheba Ormatroyd  
* Gerald Ames as Paul Meredith  
* James Carew as Levison  
* Lionelle Howard as Count Pharamend  
* Eileen Dennes as Bessie Saxton 
* Mary Dibley as Rhoda Meredith  
* Diana Carey as Mrs. Ormatroyd  
* Eric Barker as Rex Ormatroyd 
* Jacky Craine as Baby Paul  
* Ronald Colman as Bit part  

==References==
 

==Bibliography==
* Goble, Alan. The Complete Index to Literary Sources in Film. Walter de Gruyter, 1999.

==External links==
* 

 
 
 
 
 
 
 

 