El Gezeira
 
{{Infobox film
| name           = El Gezeira
| image          = El Gezeira film.jpg
| image_size     = 225px
| border         = 
| alt            = 
| caption        = 
| film name      = El Gezeira (The Island)
| director       = Sherif Arafa
| producer       = Mohamed Hassan Ramzy
| writer         = Mohamed Diab
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Ahmed El Sakka Mahmoud Yacine Hend Sabri Khaled Elsawy
| music          = Omar Khairat
| cinematography = 
| editing        = 
| studio         = 
| distributor    = ElNasr Films
| released       =  
| runtime        = 8 Months
| country        = Egypt
| language       = ِArabic
| budget         = 16,000,000 L.E
| gross          = 22,000,000 L.E
}}

El Gezeira ("The Island",  ) is a 2007 action-thriller Egyptian film, starring Ahmed El Sakka, Mahmoud Yacine, Hend Sabri and Khaled Elsawy and directed by Sherif Arafa. In upper Egypt, a gang war for leadership leads to a massacre, killing the mother and wife of the young gang boss and most of his clan.  He avenges them, and controls the whole island for trade in weapons and drugs, and makes illegal deals with the police.

==Plot==
A film about a community of Upper Egypt residents living in El Gezira (the Island). They have their own set of rules, ethics and traditions. But they also plant drugs and buy arms from Sudan. The officer in charge of the region turns a blind eye to these happenings, and in the beginning, the government takes no heed of the Island. At the start of the film, we witness the death of the old Kabir el Gezira (the islands ruler), leaving the land to his son Mansour. The first half follows Mansour as he takes control of his land and must deal with a band of other drug lords who are greedy to take control of the island. The second half of the film deals with the political conflict. It follows the governments side of things (which finally decides to take action) at the same time as Mansours side, as the two react to each others threats and the conflict escalates

==Cast==
*Ahmed El Sakka as Mansour
*Mahmoud Yacine as The old boss
*Khaled El Sawy as Roshdy
*Mahmoud Abdel-Moghni as Tarek
*Hend Sabri	 as Karima
*Bassem Samra as Hassan
*Asser Yassin as Mahmoud Nagih
*Ashraf Meslehi as Hemdan
*Zeina as Faiqa, Mansours wife
*Abdel Rahman Abou Zahra as Police General Fouad

== Awards and honors ==
Egypts 2009 Academy Awards official submission to Foreign-Language Film category

==References==
Notes
 
 

==External links==
*  
* 
*  
 

 
 
 
 


 