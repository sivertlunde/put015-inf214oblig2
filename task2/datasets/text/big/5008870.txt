Madhumati
 
 
 
{{Infobox film
| name           = Madhumati
| image          = Madhumati.jpg
| caption        = Poster
| director       = Bimal Roy
| producer       = Bimal Roy
| screenplay         = Ritwik Ghatak Rajinder Singh Bedi  
| story = Ritwik Ghatak Johnny Walker Pran
| Shailendra  
| cinematography = Dilip Gupta
| editing        = Hrishikesh Mukherjee
| distributor    =
| studio         = Bimal Roy Productions
| released       =  
| runtime        = 179 minutes
| country        = India
| language       = Hindi
| budget         =
}} Hindi drama Pran and Johnny Walker. gothic film noir feel National Film Best Feature Best Film, Best Director, Best Music Best Female Best Dialogue, Best Art Best Cinematographer.

Janam Janam, the 1988 Hindi film starring Rishi Kapoor is a remake of Madhumati. In 2007, Farah Khan and Shahrukh Khan made a new film, Om Shanti Om, that uses some plot elements from Madhumati, but did not credit the earlier film.

==Plot==
On a stormy night, Devendra (Dilip Kumar), an engineer, drives down a hill road with his friend to fetch his wife and child from the railway station. A landslide blocks their path and the friends take shelter in an old mansion off the road. Devendra finds the house uncannily familiar. In the large front room, he finds an old portrait which he recognises. His friend and the old caretaker join him, and Devendra, amidst flashes of memory from another life, sits down to tell his story while the storm rages outside.
 Johnny Walker) hides then takes Anands body to the hospital.

Anands life is saved but his mind wanders. One day, he meets a girl who looks exactly like Madhumati. She says she is Madhavi but Anand refuses to believe her and is beaten up by her companions when he tries to plead with her. Madhavi finds a sketch of Madhumati and realises he was speaking the truth. She takes the sketch and learns his story. Meanwhile, Anand is haunted by the spirit of Madhumati who tells him that Ugra Narayan is her killer. He appeals to Madhavi to pose as Madhumati before Ugranarayan and help him get a confession out of him and she agrees.

Returning to Ugranarayans palace, Anand begs permission to do a portrait of him. Next evening, with a storm brewing outside, Anand paints Ugra Narayan. At the stroke of eight, Ugra Narayan sees the pretend-Madhumati in front of him. Shaken, he confesses the truth. The police waiting outside the room come and take him away. Anand suddenly realises that the questions Madhavi asked Ugra Narayan were things she could not have known, as even Anand did not know, such as where Madhumati was buried. Madhavi just smiles and moves towards the stairs. The real Madhavi, dressed as Madhumati, then rushes into the room. She is late, for the car failed on the way. Anand realises it was Madhumatis ghost he had seen, not Madhavi. He runs up to the terrace, where the ghost beckons to him. Madhumati had fallen from the same terrace, trying to escape Ugra Narayan. Anand follows the ghost, falling to his own death.

Devendras story is over. But he says "I finally did have Madhumati as my wife. In this life&nbsp;– Radha." Just then, news arrives that the train in which his wife was travelling has met with an accident. The road is cleared, and they rush to the station. Devendras wife, Radha (Vyjayantimala) appears, unhurt with her baby.

==Cast==
* Dilip Kumar as Anand / Devendra
* Vyjayanthimala as Madhumati / Madhavi / Radha  Johnny Walker as Charandas  Pran as Raja Ugra Narayan  Jayant as Pawan Raja
* Ramayan Tiwari as Bir Singh 
* Ranjit Sood   
* Shivji Bhai  
* Tarun Bose as Devendras colleague

==Production== a novel, received much critical acclaim and a National Film Award for Best Feature Film in Hindi.  However, the film didnt receive much commercial success jeopardising Bimal Roy Productions, thus Roy needed a commercial success to survive.   

The film was written on a story written by noted Bengali filmmaker Ritwik Ghatak. When Ghatak shared the story with Roy, he immediately liked it and started developing the film as a mainstream commercial venture complete with song-and-dance formula, stepping away from usual themes of social realism as seen his previous films, Do Bigha Zamin (1953), Biraj Bahu (1954) and Devdas (1955).  It is re-incarnation, melodrama and commercial got him some criticism.  Ghatak went on to write the screenplay, while Rajinder Singh Bedi wrote in dialogues.  It launched in front of the Karlovy Vary International Film Festival Theatre in Czechoslovakia.    Dilip Kumar faced the camera, while Soviet actress Tatjana Konjuchova, switched on the camera. Polish actress Barbara Polonska acted as Clapper loader.  Madhumati is the first Indian film to be launched abroad. 

Unlike other noir film, shot indoors, Roy decided to shoot the film outdoors, that too at a hill station. The film had six-week schedule in Ranikhet, Nainital,    some scenes of the films were shot in Ghorakhal, near Nainital.  In those days there were no monitors, so when the negatives were subsequently developed, it was found that the most of the footage was foggy. Since a reshoot in far away Uttarakhand wasnt possible, sets were created at a location near Vaitarna Dam, Igatpuri, near Nashik.  Art direction team, led by Sudhendu Roy, created fake pine trees, which were planted to matched up the film to the location in Nainital. In fact, a particular scene where Dilip Kumar looks for Vyjayanthimala in the woods was actually shot in Igatpuri.  Subsequently, a large part was also shot in Aarey Milk Colony, a small forested area in Mumbai. The foggy effect was recreated by employing gas bombs. 

However, due to its extensive outdoor shooting the film went over budget by Rs. 8–1&nbsp;million, this added to the troubles of Bimal Roy Productions. Thus a film preview and lunch was organised for the film distributors. Roy told them, about the financial crunch and also that he had decided to forego Rs, 70,000 of his own fee as director to make up for the loss. Thereafter, all the distributors pitched in with money, and made up for the deficit. 

==Release and reception== highest grossing Hindi film of the year. 

==Awards and nominations==
Madhumati was the record holder for the most awards (9) received by a film at the Filmfare Awards for 37 years until the release of Dilwale Dulhania Le Jayenge which won ten awards. The film had multiple screenings since its release screened; 10th Bite The Mango Film Festival (2004), 4th Pune International Film Festival (2006) and Toronto Film Festival (2011).   
{| class="wikitable" 
|-
! Ceremony
! Award
! Category
! Nominee
! Outcome
! Note
! Ref.
|- 31st Academy Awards Academy Award List of Best Foreign Language Film Bimal Roy
|  Second film submitted by India
|rowspan="14"|   
|- National Film National Film Awards 6th National Film Awards    National Film Best Feature Film in Hindi
|rowspan="3"   Initially known as Presidents Silver Medal for Best Feature Film in Hindi
|- Filmfare Awards 6th Filmfare Awards Filmfare Award Best Film Received on behalf of Bimal Roy Productions
|- Filmfare Award Best Director
|
|- Filmfare Award Best Actor Dilip Kumar
|rowspan="2"  
|
|- Filmfare Award Best Actress Vyjayanthimala
|Vyjayanthimala won Best Actress Award for Sadhna
|- Filmfare Award Best Supporting Actor Johnny Walker Johnny Walker
|rowspan="3"  
|
|- Filmfare Award Best Music Director Salil Choudhury
|
|- Filmfare Award Best Female Playback Singer Lata Mangeshkar For "Aaja Re Pardesi" First winner of this category Given as single category for both male and female singers
|- Filmfare Award Best Story Ritwik Ghatak
| 
|
|- Filmfare Award Best Dialogue Rajinder Singh Bedi
|rowspan="4"   First winner of this category
|- Filmfare Award Best Art Direction Sudhendu Roy
|
|- Filmfare Award Best Cinematographer Dilip Gupta
|Black-and-white category
|- Filmfare Award Best Editing Hrishikesh Mukherjee
|
|}

==Music==
{{Track listing Shailendra
|all_music=Salil Choudhury 
| extra_column = Singer(s)
| title1 = Aaja Re Pardesi | extra1 = Lata Mangeshkar | length1 = 04:26
| title2 = Chadh Gayo Papi Bichhua | extra2 = Lata Mangeshkar, Manna Dey | length2 = 05:23
| title3 = Dil Tadap Tadap Ke | extra3 = Mukesh (singer)|Mukesh, Lata Mangeshkar | length3 = 03:27
| title4 = Ghadi Ghadi Mora Dil Dhadke | extra4 = Lata Mangeshkar | length4 = 03:11
| title5 = Hai Bichhua Hai Re Hai | extra5 = Lata Mangeshkar | length5 = 
| title6 = Ham Haal-e-Dil Sunaenge | extra6 = Mubarak Begum | length6 = 03:26
| title7 = Jungle Mein Mor Naacha | extra7 = Mohammad Rafi | length7 =  Ghulam Mohammad | length8 = 
| title9 = Suhana Safar Aur Yeh Mausam | extra9 = Mukesh | length9 = 03:44
| title10 = Tan Jale Man Jalta Rahe | extra10 = Dwijen Mukherjee | length10 = 
| title11 = Toote Huye Khwabon Ne | extra11 = Mohammad Rafi | length11 = 
| title12 = Zulmi Sang Aankh Ladi | extra12 = Lata Mangeshkar | length12 = 
}}

According to film and music expert Rajesh Subramanian Bimal Roy had opted for S D Burman as the composer of Madhumati but the veteran music director voluntary suggested composer Salil Chaudharys name adding he would do a more apt job.

==Influence==
Madhumati  went on to become the source of inspiration for many later works dealing with the theme of reincarnation in   Yuga Purusha (1989), the  , 7 August 2008.    
 reincarnation as a main theme include:

*Mooga Manasulu (1963)
*Milan (1967 film)|Milan (1967)
*Mehbooba (1976)
*Karz (film)|Karz (1980)
*Kudrat (1981)
*Janam Janam (1988)
*Suryavanshi (film)|Suryavanshi (1992)
*Prem Shakti (1994)
*Karan Arjun (1995)
*Hamesha (1997)
*Mr Ya Miss (2005)
*Valley of Flowers (2006)
*Love Story 2050 (2008)

Reincarnation has also appeared as a main theme in the following Indian soap operas and television serials:

*Banoo Main Teri Dulhann
*Kaajjal
*Kumkum (TV series)|Kumkum

==Golden Jubilee==
Marking the celebrating its golden jubilee anniversary of the film, Bimal Roy foundation headed by Roys daughter Rinki Bhattacharya, hosted a screening of Madhumati at the Globus Cinema in Mumbai on 11 April 2008. The occasion also saw the reunion of films cast, including Vyjayantimala. Madhumati which released in 1958 with music by none other than the legendary Salil Chaudhury and lyrics by Shailendra (lyricist)|Shailendra, was one of Bimal Roys greatest commercial successes.     Subsequently, his daughter published a book on the making of the film, Bimal Roys Madhumati: Untold Stories from Behind the Scenes.   

==See also==
* List of submissions to the 31st Academy Awards for Best Foreign Language Film
* List of Indian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==Bibliography==
*  
* 

==External links==
*  
*   at Upperstall.com
*   at Rediff

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 