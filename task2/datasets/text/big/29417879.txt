Xenoa
{{Infobox film
| name           = Xenoa
| image          = Xenoa poster.jpg
| alt            = Poster shows a black background. In the foreground are the films characters. The films name is at the bottom.
| caption        = Theatrical release poster
| director       = Sean Lim
| producer       = Ruben Dela Cruz Melinda Lim Robert Lim Sean Lim Elma Macdonald-Taylor
| screenplay     = 
| story          = Maya Cruzada	 Ali Co Sean Lim
| based on       = 
| starring       = Isabel Granada Paolo Ballesteros Rafael Nanquil Geryk Genasky Aguas Clementine Poblete Ronnie Felipe Martinez
| studio         = Asia Pacific College School of Multimedia Arts Visual Camp Xion Films
| distributor    = Xion Films
| released       =  
| runtime        = 
| country        = Philippines
| language       = Tagalog, Filipino
| music          = Arnold Buena Allan Feliciano
| cinematography = Ruben Dela Cruz	
| editing        = Gerard Cadlum Ariel Javines
| gross          = 
| budget         = 
}} fantasy Filipino indie film, directed by Sean Lim. The film was released to Philippine theaters on August 22, 2007. It stars Isabel Granada, Paolo Ballesteros and Rafael Nanquil. The film gives a glimpse of the power struggle for the planet Xenoa. When the ruler of the triple-star system, Queen La’ian (Clem Poblete) gives birth to the heirs of the Xenoan throne, she decides to protect them from the scheming General Norak (Ronnie Martinez) by sending the triplets – Eli, Zeus and Drix (played by Granada, Ballesteros and Nanquil respectively) – to faraway Earth. 

The film was released under the tagline "Three siblings. Two worlds. One empire." 

==Plot==
The ruler of Xenoa, Queen Laian, gives birth to the heirs of her throne: Eli, Zeus, and Drix. She decides to protect her triplets from the scheming leader of rival world Zephyr, General Norak, by sending her children to faraway Earth. Two decades later, the lives of the three estranged siblings intersect in the most unexpected way. Everything they know and love is suddenly threatened when General Norak zeroes in on them from light years away. The spiteful alien wants unconditional, uncontested power over Xenoa, but he is unable to achieve this as long as the true heirs to the throne are still alive. He is so hellbent on ruling Xenoa that he does not think twice about turning sibling against sibling in his quest for power. 

==Cast==
 
* Isabel Granada as Eli
* Paolo Ballesteros as Zeus
* Rafael Nanquil as Drix
* Geryk Genasky Aguas as Dennis
* Clementine Poblete as Laian / Lilia
* Ronnie Felipe Martinez as Norak
* Lesley Leveriza as Amanda
* Marq Dollentes as Alien actor
* Richard Turner  as Bartender
* Sophia Castañeda as Bar customer
* Michael Poblete as Chevrolet driver
* Apollo Abraham as Drixs foster father
* Hiyasmin Neri as Jane
* Katsuji Kikuchi as John Yan
* Monette Rosello as Land lady
* Alan Marasigan as Mr. Legazpi
* Lily Chu as Martha
* Daniel Magisa as Mayor Villaruel
* P.J. Lanot as Mike

==Production==

===Development===
Sean Lim, director and writer of the film, admitted that Xenoa is his first film, although he has co-directed a Big Foot production shot in Hong Kong. He tells that the film is about hybrid human beings from another planet called "Xenoa". Lim admits to having a longtime crush on Isabel Granada. According to him, he specifically cast Isabel for the role because of her almond-shaped eyes, making her perfect for the role of a hybrid human named Eli. Her character is described as a naïve yet undeniably gorgeous woman. She is trusting and generous to a fault. Eli grows up in an orphanage house managed by nuns.  Granada was supposed to play as the Queen. When director Lim saw her, he re-cast her as one of the triplets since she was too young to play the queen. Xenoa is considered to be Granadas comeback film, since her last movie was still in 2001 which was Halik ng Sirena. 

==Release==
Xenoa’s world premiere happened on August 14, 2007 at 7 p.m. at SM Megamall Cinema 1, Ortigas Center. Regular showing dates were from August 22 up to 28, 2007 at all SM Cinemas nationwide. 

==Soundtrack==
{{Infobox single
| Name = Collide
| Cover = 
| Caption =  Nina
| Nina Featuring the Hits of Barry Manilow
| Released = August 2007
| Format = 
| Recorded = 2007
| Genre = Pop music|Pop, contemporary R&B|R&B
| Length = 5:51
| Label = Warner Music
| Writer = Edwin Marollano
| Producer = Neil Gregorio I Cant Make You Love Me"  (2007)
| This single = "Collide"  (2007) Somewhere Down the Road"  (2007)
}}
 Nina Featuring the Hits of Barry Manilow. It was also nominated for Best Song Written for Movie/TV/Stage Play on the 2008 Awit Awards. 
 digital download through iTunes. " ". itunes.apple.com Retrieved 2010-10-31 

===Music video===
 
The music video for "Collide" was also directed by the films director, Sean Lim. The video starts with Nina, shown lying on a beatle car. She is then seen singing to the song in close-up, with black background and night lights. Later, she is seen in a bedroom terrace and scenes from the movie start to appear. The scene goes back to the beatle car, where a bright light suddenly struck her face. By the last chorus, Nina is seen wearing a black gown, while floating in the air (with the use of special effects). 

==References==
 

==External links==
* 

 
 
 
 
 
 
 