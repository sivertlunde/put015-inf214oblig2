Le Saint prend l'affût
{{Infobox film
| name           = Le Saint prend laffût
| image          =
| caption        =
| director       = Christian-Jaque
| producer       = Intermondia Films, Medusa Produzione
| writer         = Jean Ferry Leslie Charteris
| starring       = Jean Marais Raffaella Carrà
| music          = Gérard Calvi Pierre Petit
| editing        =
| distributor    =
| released       = 26 October 1966 (France)
| runtime        = 90 minutes
| country        = France
| awards         =
| language       = French
| budget         =
}}
 adventure Drama drama film from 1966. It was directed by Christian-Jaque, written by Jean Ferry, starring Jean Marais and Raffaella Carrà.  The film was known under the title The Saint Lies in Wait (USA),   (Italy),   (West Germany),   (Portugal).  During the making of the film, the stunt arranger Gil Delamare was killed. 

== Cast ==
*  
* Raffaella Carrà : Mrs Anita Pavone
* Jess Hahn : Hoppy Uniatz
* Jean Yanne : Mueller-Strasse
* Danièle Evenou : Sophie Chartier
* Henri Virlojeux : Oscar Chartier
* Dario Moreno

== References ==
 

== External links ==
*  
*   at Films de France

 
 

 
 
 
 
 
 
 
 

 