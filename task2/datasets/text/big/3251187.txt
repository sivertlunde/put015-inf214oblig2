Four Horsemen of the Apocalypse (film)
{{Infobox film
| name = The 4 Horsemen of the Apocalypse
| image = Four Horsemen of the Apocalypse.jpg
| caption = Film poster by Reynold Brown 
| director = Vincente Minnelli
| producer = Julian Blaustein associate Olallo Rubio Jr. John Gay  Robert Ardrey
| narrator = 
| starring =  Glenn Ford Paul Henreid Ingrid Thulin Charles Boyer Lee J. Cobb 
| music = André Previn
| cinematography = Milton R. Krasner
| editing = Ben Lewis Adrienne Fazan
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 153 minutes
| country = United States
| language = English
| budget = $7,174,000    gross = $4,100,000 
}}

:For the 1921 film version, see The Four Horsemen of the Apocalypse (film). novel by Vicente Blasco Ibáñez, which had been filmed in 1921 with Rudolph Valentino. Unlike that film, this was a critical and commercial disaster, which contributed greatly to the financial problems of MGM.

It was directed by Vincente Minnelli and starred Glenn Ford, Ingrid Thulin, Charles Boyer, Lee J. Cobb, Paul Lukas, Yvette Mimieux, Karlheinz Böhm, and Paul Henreid.

==Story==
In 1936, Madariga is an 80 year old patriarch of a large Argentinian cattle ranch. He has two grandsons - Julio, son of the French Marcelo, and Heinrich, son of the German Karl.

Heinrich returns home from studying in German to reveal he has become a Nazi. Madariaga slaps Heinrich and predicts that the Four Horsemen of the Apocalypse (Conquest, War, Pestilence, and Death) will soon devastate the earth; he runs outside into a storm with visions of the four horsemen and then dies in Julios arms.

In 1938 Julio goes to Paris with Marcelo and befriends the anti-Nazi Etienne Laurier. Julio falls in love with Lauriers wife, Marguerite, and then becomes her lover when Laurier is sent to a concentration camp.

Julios sister Chi Chi is active in the French resistance and Julio becomes gradually involved as well. Laurier is released from prison a broken man, and Marguerite leaves Julio to care for him.

Eventually both Laurier and Chi Chi are killed by the Germans. Julio risks his life helping British bombers destroy the Nazi headquarters in Normandy. He encounters Heinrich just as the bombs fall on them, killing them both.

==Cast==
* Glenn Ford: Julio Desnoyers
* Ingrid Thulin: Marguerite Laurier
* Charles Boyer: Marcelo Desnoyers
* Yvette Mimieux: Chi-Chi
* Lee J. Cobb: Madariaga
* Paul Henreid: Etienne Laurier
* Karlheinz Böhm: Heinrich von Hartrott
* Paul Lukas: Karl von Hartrott
* Nestor Paiva: Miguel
*Harriet MacGibbon: Doña Luisa Desnoyers
*Kathryn Givney: Elena von Hartrott
*Marcel Hillaire: Armand Dibie
*George Dolenz: General von Kleig
*Stephen Bekassy: Colonel Kleinsdorf
*Nestor Paiva: Miguel
*Albert Rémy: François
*Richard Franchot: Franz von Hartrott Brian Avery: Gustav von Hartrott

==Production==

===Development===
The silent film rights to the original story had been purchased by Metro in 1918 for $190,000. There had been discussions by MGM about remaking the film before the American copyright expired in 1946. 

The following year MGM producer Sam Marx announced the studio may remake the film as a vehicle for Ricardo Montalban and if they did the story would be updated to World War Two. U-I TO FILM NOVEL OF CIRCUS CAREER: More Than $200,000 Reported as Price for Gus the Great, Forthcoming Duncan Book
By THOMAS F. BRADYSpecial to THE NEW YORK TIMES.. New York Times (1923-Current file)   16 June 1947: 25. 
 a remake of Ben-Hur, which looked like it was going to be a big success, and were looking for other old MGM properties to remake. They obtained the necessary rights and announced they would make the movie in June 1958. M-G-M TO REMAKE A SILENT CLASSIC: Lists The Four Horsemen as 59 Super Venture -- Passport, Novel, Bought
By THOMAS M. PRYORSpecial to The New York Times.. New York Times (1923-Current file)   18 June 1958: 40.  Julian Blaustein was assigned as producer. BRANDO COMPANY PLANS FIVE FILMS: Pennebaker, Inc., Will Start 2 Productions in August -- Preminger Signs Writer
By THOMAS M. PRYORSpecial to The New York Times.. New York Times (1923-Current file)   11 July 1958: 15. 

===Scripting===
Blaustein announced the story would be updated from World War I to World War II:
 The driving force of the book, is of love among men instead of hatred. I dont think it can be said often enough that such love is indispensable for all of us if we are to have any future. If a motion picture can dramatise such a theme entertainingly then the motion picture may make a small contribution to peace in the world. It certainly impresses me as being worth the try... The Paris of the occupation, the births of the resistance movements have never been thoroughly explored on the screen to my mind. Im not interested in trying to recreate the shooting war. Thats almost too difficult to realistically do on the screen today. What I want to put on screen is the atmosphere, so that when you sit in the theatre you will feel the hope and frustration of people struggling against invasion and may realize no man is an island. HOLLYWOOD SCENE: Jerry Wald Presents His Treasurers Report -- Blausteins Horsemen
By THOMAS M PRYORHOLLYWOOD.. New York Times (1923-Current file)   27 July 1958: X5.  
Robert Ardrey wrote the initial script. The movie was, along with a remake of Cimarron (1960 film)|Cimarron, going to be one of MGMs big films for 1960.  HOLLYWOOD VISTA: Extensive Production Slate Planned For 1960-1961 by Metros Chiefs
By MURRAY SCHUMACHHOLLYWOOD.. New York Times (1923-Current file)   20 Dec 1959: X7.  COAST FILM FETE GAINING STATURE: San Francisco Event Draws Top Movies From Abroad -- Logan Makes Plans
By RICHARD NASON. New York Times (1923-Current file)   10 Oct 1959: 12. 

MGM allocated a budget of $4 million and Vincente Minnelli to direct. Minnelli says he had doubts about relocating the time period and wanted it set back in World War I, but the studio were insistent.  Filming was pushed back due to the actors strike in 1960.

Minnelli later claimed he was "drafted" into making the movie, and was rushed into production before he was ready because MGM had a start date.  However he did manage to get head of production Sol Siegel to arrange for the script to be rewritten in order to reflect the German Occupation of Paris. Because Robert Ardrey was busy, MGM hired John Gay to do rewrites working off an outline prepared by Minnelli which outlined the weaknesses as he saw them.

"Gay proved to be an enormous help," wrote Minnelli later. "The script - with the dreadful World War II setting - took shape. But I never justified the updating in my mind." Minnelli p 340 

Pre-production commenced in Paris. Minnelli wrote he flew back to the US and tried to talk the studio into changing the time period once again but they refused. "I began to believe I was the victim of a studio set up," he wrote. 

===Casting=== George Hamilton, and Maximilian Schell. M-G-M TO REMAKE FOUR HORSEMEN: 4-Million Production to Begin Filming in France in Fall -- Actor, 20, May Get Lead
New York Times (1923-Current file)   03 Mar 1960: 24. 

Vincente Minnelli says he wanted Alain Delon for the starring role, and met up with the young actor in Rome, but the producers did not feel he was sufficiently well known at the time.  In June 1960 it was announced that Glenn Ford, who had a long relationship with MGM and had recently signed a new contract with the studio, would play the lead role. 

Minnelli later reflected, "There I was, stuck with a story I didnt want to do, with a leading actor who lacked the brashness and impulsiveness I associated with his part. I wanted new challenges but I didnt think theyd be that challenging." 
 Claude Dauphin Wild Strawberries, stepped in. Of Local Origin
New York Times (1923-Current file)   13 Sep 1960: 40.  The studio wanted Horst Buchholz to play the young German son but he was unable to do it due to his commitment to make Fanny (1961 film)|Fanny (1961), so Karl Boehm was hired instead. 

Ford was paired with an older actress, Ingrid Thulin, making both main roles much older than the book and 1921 film characters, giving more credibility to their relationship than a May–December romance would have. Although Thulin spoke English well, she was dubbed by Angela Lansbury.

===Shooting===
Minnelli later wrote that as he was unhappy with the story he decided to make the film at least as "stunning visually as I cold make it. The flaws in the story might be overlooked. Some of my previous pictures hadnt held much hope in the beginning, but theyd been saved because Id had some leeway in the writing. But I didnt have this freedom on Four Horsemen. It would be interesting to see what could be accomplished 

Minnelli decided to make the Four Horsemen an integral part of the story, which would be designed by Tony Duquette as a set of andirons riding the sky, parallel to the main action. He used red as "a dominating color, culminating in a read gel over the newsreels, which would be shown in a documentary way to point up the devastation of the war and the insensitivity of the principal actors in taking scant notice of it." 

Filming started in Paris on 17 October. "HORSEMEN, BOUNTY STARS GLITTER IN MGM HEAVEN
Scheuer, Philip K. Los Angeles Times (1923-Current File)   19 Feb 1961: l3. 

It proved difficult, in part due to riots due to the situation in Algeria and because of local reluctance to recreate scenes from the Occupation. It was decided to film the bulk of the movie in Hollywood instead. WAR FILM HALTED BY PARIS REALITY: Four Horsemen Back in U.S. When Student Riots End Plans for Mock Ones
By MURRAY SCHUMACHSpecial to The New York Times.. New York Times (1923-Current file)   20 Dec 1960: 44. 

One of the most famous scenes of the 1921 movie involved Rudolph Valentino dancing the dance. It was decided not to have a tango scene in the new movie. HOLLYWOOD TWIST: New Time, War, People in the Remake Of Four Horsemen of Apocalypse
By MURRAY SCHUMACH. New York Times (1923-Current file)   01 Jan 1961: X5. 

Ingrid Thulin later reflected on filming:
 It was an interesting experience. I could not conform to their standards of beauty. I tried... After the first few rushes it was obvious that it   would turn out badly. Yet they went right on. Perhaps they couldnt convince themselves that all that money would end in disaster. I really did want to be as beautiful as they wanted. It was terribly difficult. Then I worked very hard to dub the dialogue but they kept changing lines to things I couldnt pronounce. So they had to dub in another voice. INTONATIONS FROM A SILENT SWEDE: SWEDENS INGRID THULIN REFLECTS
By EUGENE ARCHER. New York Times (1923-Current file)   16 Feb 1964: X7.   Come Fly with Me and The Wonderful World of the Brothers Grimm.

===Post Production===
The movie spent a considerable amount of time in post production, causing its budget to increase further. This, combined with the massive cost over-runs of Lady L (which had been postponed) and the remake of Mutiny on the Bounty led to the resignation of MGMs head of production, Sol C. Siegel. SOL SIEGEL TO QUIT AS M-G-M OFFICIAL: Production Chief Will Return to Independent Producing No Successor Named Mutiny Exceeded Budget Of Local Origin
By MURRAY SCHUMACH Special to The New York Times.. New York Times (1923-Current file)   05 Jan 1962: 35. 

==Reception==
The film had its world premiere in February 1962. FOUR HORSEMEN HAS PARIS PREMIERE
Los Angeles Times (1923-Current File)   09 Feb 1962: C11. 

===Box Office===
MGM had become aware by April that the film was not going to be able to recoup its cost and started writing off the losses. DECLINE IN PROFIT SHOWN BY M-G-M: Quarter Earnings at 15c a Share, Against $1.78 in Like 6l Period GEORGIA-PACIFIC A.V. ROE CANADA COMPANIES ISSUE EARNING FIGURES OTHER COMPANY REPORTS
New York Times (1923-Current file)   16 Apr 1962: 56.   Ultimately the movie earned $1,600,000 in the US and Canada and $2,500,000 overseas, incurring an overall loss of $5,853,000. 

This, along with the failure of Mutiny on the Bounty and The Wonderful World of the Brothers Grimm, lead to MGM president Joseph Vogel resigning.

===Critical=== 1921 version, which propelled Rudolph Valentino to superstardom. Ford, with many films behind him, was not the unknown that Valentino was when he appeared in the 1921 film.   Ford, 46 years old, also had the disadvantage of trying to reprise a role that Valentino had played when he was 26. Critics also considered Ford severely miscast as a Latin love who, in their minds, should have been played by someone a lot younger .

The Los Angeles Times said the filmmakers "have pulled it off. The new "Four Horsemen of the Apocalypse" restores the pleasure there can he in seeing a good story well told on the screen." 4 Horsemen of Apocalypse Ride Again
Scheuer, Philip K. Los Angeles Times (1923-Current File)   18 Feb 1962: A3. 
 The Damned, The Conformist and The Garden of the Finzi Continis. 

==Soundtrack== Alan and Marilyn Bergman later adapted and wrote lyrics to. The resulting song, "More In Love With You," was recorded by Barbra Streisand for The Movie Album (2003).

== References ==
 
*Minnelli, Vincent & Harold Acre, I Remember It Well, Samuel French 1975

==External links==
*  
*  at TCMDB
*  at New York Times
 

 
 
 
 
 
 
 
 
 
 
 
 