Black Out (1970 film)
{{Infobox film
| name           = Black Out
| image          = 
| caption        = 
| director       = Jean-Louis Roy
| producer       = 
| writer         = Jean-Louis Roy Patricia Moraz
| starring       = Marcel Merminod
| music          = 
| cinematography = Roger Bimpage
| editing        = 
| distributor    = 
| released       = 1970
| runtime        = 92 minutes
| country        = Switzerland
| language       = French
| budget         = 
}}

Black Out is a 1970 Swiss film directed by Jean-Louis Roy. It was entered into the 20th Berlin International Film Festival.   

==Cast==
* Marcel Merminod - Émile Blummer
* Lucie Avenay - Élise Blummer
* Marcel Imhoff - Le pasteur
* Georges Wod - Le capitaine Schnertz
* Robert Bachofner - Le petit garçon
* Michel Breton - Le vendeur

==References==
 

==External links==
* 

 
 
 
 