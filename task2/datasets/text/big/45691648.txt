Hera Pheri 3
{{Infobox film
| name           = Hera Pheri 3
| image          = 
| caption        = 
| director       = Neeraj Vora
| producer       = Feroz Nadiadwala
| writer         = Neeraj Vora
| starring       = Sunil Shetty John Abraham Neha Sharma Abhishek Bachchan Esha Gupta Nargis Fakhri Paresh Rawal Jaya Bachchan
| music          = Anu Malik
| cinematography = 
| editing        = 
| studio         = Base Industries Group
| distributor    = 
| released       = Scheduled for 18 December 2015
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} Hera Pheri John Abraham.  Actor Abhishek Bachchan is a new addition in this film with John Abraham.  The film is scheduled to release on 18 December 2015. 

==Cast==
*Sunil Shetty as Shyam
*John Abraham as Raju Khabri
*Abhishek Bachchan as Raju Duplicate
*Paresh Rawal as Baburao Ganpat Rao Apte
*Neha Sharma
*Kader Khan

==Production==

After a long delay production of Hera Pheri 3 has begun in 2014. Sunil Shetty and Paresh Rawal will reprise their roles from previous films as Shyam and Baburao respectively. Reportedly Akshay Kumar has been replaced with actor John Abraham in this film.    Actors Abhishek Bachchan and Kader Khan are new additions in this film.  

Shooting has begun from 9 April 2015 in Dubai.   

==References==
 



 
 
 
 
 