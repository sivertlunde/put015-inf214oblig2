Zara Si Zindagi
{{Infobox film
| name           = Zara Si Zindagi
| image          = ZaraSiZindagifilm.jpg
| caption        = LP Vinyl Records Cover
| director       = K. Balachander
| producer       =
| writer         = Mazhar Khan Shriram Lagoo|Dr. Shriram Lagoo Karan Razdan Arjun Chakraborty Laxmikant - Pyarelal
| cinematography =
| editing        =
| distributor    =
| released       = 7 January 1983
| runtime        =
| country        = India Hindi
| budget         =
| gross          =
}}
 1983 Bollywood|Hindi-language Indian feature directed by Mazhar Khan, Shriram Lagoo|Dr. Shriram Lagoo and Karan Razdan. 

It is a remake of the Tamil hit Varumayin Niram Sivappu (1980), also directed by K. Balachander with Kamal Haasan and Sridevi in the lead roles.

==Plot==

Kamal Haasan reprises his role as Rakesh, a graduate looking for work in the capital, sharing a small home with two other unemployed youth played by Amit (Karan Razdan) and Tilak (Arjun Chakraborty). Rakesh falls in love with a struggling actress Kusum (Anita Raj), whose life is likewise wretched having to take care of an old and ailing grandmother and a paralysed father (Nilu Phule).

==Cast==

*Kamal Haasan
*Anita Raj
*Nilu Phule Mazhar Khan
*Shriram Lagoo|Dr. Shriram Lagoo
*Karan Razdan
*Arjun Chakraborty

==Soundtrack== 
{{Infobox album   
| Name        =  Zara Si Zindagi
| Type        =  soundtrack Laxmikant and Pyarelal
| Cover       =  
| Caption     = 
| Released    =     
| Recorded    =  
| Genre       =  
| Length      =  
| Language    =  Hindi
| Label       =  
| Producer    =  
| Reviews     =  
| Compiler    =  
| Misc        =  
}}
* "Zamaane Mein Mohabbat Phir Huyi Badnaam" - S. P. Balasubrahmanyam
* "Sargam Ke Phoolon Se Geeton Ki Bane Maala" - S. P. Balasubrahmanyam, Asha Bhosle
* "Ghar Se School" - Kishore Kumar
* "Jis Maut Aaya Hai" - K. J. Yesudas|K.J. Yesudas.

==References== 
 

==External links==
*   

 

 
 
 
 
 
 

 