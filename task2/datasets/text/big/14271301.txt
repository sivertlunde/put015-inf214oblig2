Thennavan
{{Infobox film
| name = Thennavan
| image =
| caption =
| director = M. Nandakumaran
| producer = L. K. Sudhish
| writer = M. Nandakumaran R. N. R. Manohar Urvasi Vivek Vivek V. Ravichandran
| music = Yuvan Shankar Raja
| cinematography = A. Ramesh Kumar
| editing = B. S. Vasu - Saleem
| studio = Captain Cine Creations
| distributor =
| released =  
| runtime = 146 minutes
| country = India
| language = Tamil
}} Indian Tamil Tamil action film written and directed by newcomer M. Nandakumaran and produced by Vijayakanth, who played the title role Thennavan IAS,  in-charge as the Chief Election Commissioner and fight against corrupted politicians.The musical score by Yuvan Shankar Raja.The film released on 15 August 2003, coinciding with Independence Day (India)|Indias Independence Day.

==Cast and crew==

===Cast===
* Vijayakanth as Thennavan
* Kiran Rathod as Divya
* Nassar as Ilanthiraiyan Urvasi as Pushpalatha Ravichandran
* Vivek as Dada Mani Ponnambalam
* Sriman Mansoor Ali Khan Raj Kapoor Thyagu
* Thalaivasal Vijay
* R. N. R. Manohar as rowdy who gets killed by Vivek

===Crew===
* Story, Screenplay, Direction: M. Nandakumaran
* Production: Vijayakanth, L. K. Sudhish
* Music: Yuvan Shankar Raja
* Dialogue: R. N. R. Manohar
* Cinematography: A. Ramesh Kumar
* Editing: B. S. Vasu - Saleem
* Art direction: J. K.
* Fights: Rocky Rajesh
* Choreography: Sivashankar, Kala, Brindha & Kalyan
* Costumes: Premalatha Vijayakanth
* Stills: Satheeshkumar - Sureshvaasan
* Lyrics: Pa. Vijay, Na. Muthukumar, Snehan & Muthu Vijayan

==Soundtrack==
{{Infobox album|  
| Name = Thennavan
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Thennavan cd cover.jpg
| Released =  7 August 2003 (India)
| Recorded = 2002 Feature film soundtrack
| Length =
| Label = Five Star Audio
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Pudhiya Geethai (2003)
| This album  = Thennavan (2003)
| Next album  = Kurumbu (2003)
}}

The music was composed by Yuvan Shankar Raja, who, for the first time, worked for a Vijayakanth film. The soundtrack, which was released on 7 August 2003, features 6 tracks with lyrics by Pa. Vijay, Na. Muthukumar, Snehan and Muthu Vijayan. Interestingly, another soundtrack of Yuvan Shankar Raja, Kurumbu, was released on the same day.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Lyricsist
|- 1 || "Vatta Vatta" || Unni Menon, Padmalatha || 4:12 || Muthu Vijayan
|- 2 || "Vinodhane" ||  
|- 3 || "Dhesa Kaatre" ||  
|- 4 || "Englishil Paadinaa" ||   
|- 5 || "Vinodhane - II" ||   
|- 6 || "Adi Saamy" ||  
|}

==Production==
The film is directed by debutant M. Nandakumar who has worked with directors like P. Vasu and Rajasekhar. RNR Manoharan, dialogue writer of this film appeared in a small role as a gangster who arrives to kill Vivek instead gets killed by him.

Shooting for the film commenced in Chennai, and some fight scenes were picturised on top of highrise buildings like the Y.M.C.A and the Devi Theatre, and on a set erected at the Prasad Studios. The hero and the unit at Prasad Studios had a surprise visitor in Cricketer Brian Lara. Lara had come for a friendly go-Kart competition, and visited the sets to watch the shooting. Some scenes between the lead pair were picturised at the old Mahabalipuram road, while a song was picturised at sets erected at the AVM and Prasad Studios. Choreographed by Kala, the dance number had about a hundred dancers dancing around the lead pair. The song picturised at five sets, and shot with three cameras simultaneously, reportedly cost about Rs.35 lakhs to picturise it. Yet another song was picturised with the lead pair at locations in and around Pollachi. Choreographed by Brinda, it took ten days to complete the picturisation.

The fight scene between the hero and the jailor was filmed at Prasad Studios, where a lavish set of a jail was erected at a cost of about Rs.25 lakhs. While most of the shooting took place in Chennai and Pollachi, the unit also shot at locations in Jodhpur, Orissa and Vishakapattinam. For a single scene of a political meeting, about 5000 junior artistes were hired. 

==Reception==
Hindu wrote:"Thennavan would have been a tailor made story for Vijayakanth but for the flawed script, clichéd dialogue and unappealing caricatures. Somewhere down the line everyone from the director and the hero seem to have lost interest". 

==References==
 

 
 
 
 