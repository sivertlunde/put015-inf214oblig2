The Apache Kid (film)
{{Infobox Hollywood cartoon|
| cartoon_name = The Apache Kid
| series = Krazy Kat
| image = Krazyandgirlfriend.jpg
| caption = Krazy and the spaniel
| director = Ben Harrison Manny Gould
| story_artist = George Herriman
| animator = Ben Harrison Manny Gould
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| studio = Winkler Pictures
| distributor = Columbia Pictures
| release_date = October 9, 1930
| color_process = Black and white
| runtime = 6:37 English
| The Bandmaster
| followed_by = Lambs Will Gamble
}}

The Apache Kid is a cartoon short distributed by Columbia Pictures and features Krazy Kat. The film is the characters 149th film.

==Plot== Apache dance. Suddenly, a tiger came by and grabs the spaniel, taking her faraway. Though the kidnapper flees in a horse, Krazy still chooses to run after on foot.

Krazy follows the tigers trail into a sewer. But little did he know that the tiger and the spaniel are in a secret lair, and the entrance to that place is already shut. Desperately wanting to be released and see her boyfriend again, the spaniel weeps in the couch. In this, the tiger decides to cheer her up with a song and dance. While the captor is trying to entertain, Krazy finds an opening to the secret lair and goes in. Krazy finally confronts the tiger, and the two guys decided to settle things in a knife battle. Following a number of exchanges, the tiger is knocked cold. Krazy and the spaniel are reunited.

==Spaniel girlfriend==
Krazys spaniel girlfriend makes one of her earliest appearances in this short, therefore becoming one of the primary cast not originating from the comic strip. She would then replace Ignatz Mouse as Krazys supporting character, and even appearing in the title cards from 1930 to 1933.

==Availability==
*Columbia Cartoon Collection: Volume 1. {{cite web
|url=http://theshortsdepartment.webs.com/columbiacartoons.htm
|title=The Columbia Cartoons
|accessdate=2012-06-17
|publisher=the shorts development
}} 

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 


 