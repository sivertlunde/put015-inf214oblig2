Cat's Eye (1997 film)
{{Infobox film
| name           = Cats Eye
| image          = Cats eye 1997 movie DVD cover.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = Kaizo Hayashi
| producer       = 
| writer         = 
| starring       = Yuki Uchida Norika Fujiwara Kane Kosugi
| music          = 
| cinematography = 
| editing        = 
| studio         = Fuji Television
| distributor    = Toho
| released       =  
| runtime        = 92 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
::Not to be confused with Stephen Kings Cats Eye (1985 film)|Cats Eye.
 1997 cinema Japanese film directed by  Kaizo Hayashi and starring Yuki Uchida and Norika Fujiwara. It is a live-action movie adaptation of the famous manga and anime TV series Cats Eye (manga)|Cats Eye, not to be confused with a previous 1988 live-action TV movie adaptation.

==Cast==
* Yuki Uchida as  Ai Kisugi 
* Norika Fujiwara as  Rui Kisugi 
* Izumi Inamori as  Hitomi Kisugi
* Kenta Harada as  Toshio Utsumi 
* Naoko Yamazaki
* Kane Kosugi
* Wenli Jiang   
* Akaji Maro   
* Shirō Sano
* Akira Terao

==Merchandise==

===Video===
A behind-the-scenes video was created for the movie, called Cats Eye Secret.

===Photobook===
 
A companion photobook was produced by Wani Books. It contains photographs of the stars in character.

==External links==
*  

 
 
 
 
 


 

 