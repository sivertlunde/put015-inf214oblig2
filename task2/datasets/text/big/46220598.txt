When the Earth Trembled
{{Infobox film
| title          = When the Earth Trembled
| image          = 
| caption        = 
| director       = Barry ONeil
| producer       = Siegmund Lubin
| writer         = Edwin Barbour
| narrator       = 
| starring       = Ethel Clayton Harry Myers
| music          = 
| cinematography = 
| editing        = 
| studio         = Lubin Manufacturing Company
| distributor    = General Film Company
| released       = November 1913 (US)
| runtime        = 43 min.
| country        = United States
| language       = Silent film English intertitles
| budget         = 
| gross          = 
}}
When the Earth Trembled (1913 in film|1913) is a silent film starring Ethel Clayton and Harry Myers. 

==Plot==
A mother (Clayton) with two young children survives the 1906 San Francisco earthquake.

==Cast==
*Harry Myers ... Paul Girard Jr.
*Ethel Clayton ... 	Dora Sims
*Richard Morris ... 	Richard
*Mrs. George W. Walters  ... Coffee Mary
*Bartley McCullum  ... 	Paul Girard Sr.
*Mary Powers ... Doras Little Girl
*Lathan Miegzler ... Doras Little Boy
*Peter Lang ... John Pearce

==Preservation status==
In 2015, the film was restored by EyeMuseum in the Netherlands, with material from EyeMuseum, the British Film Institute, and the Museum of Modern Art. The restored print was premiered on 28 March 2015 at EyeMuseum. 

On 29 May 2015, the San Francisco Silent Film Festival will present the film at the Castro Theatre in San Francisco. 

==References==
 

==See also==
*List of rediscovered films

==External links==
* 
* 
* 

 
 