Midnight Madness (film)
 
{{Infobox film
| name = Midnight Madness
| image = MidnightMadness.jpg
| caption = Poster for Midnight Madness
| director = Michael Nankin David Wechter 
| producer = Ron W. Miller
| writer = Michael Nankin David Wechter David Naughton Michael J. Fox Stephen Furst Maggie Roswell
| music =Julius Wechter
| cinematography = Frank V. Phillips
| editing = Norman R. Palmer Jack Sekely
| studio = Walt Disney Productions Buena Vista Distribution
| released =  
| runtime = 112 min
| country = United States
| language = English
| gross = $2.9 million
}} Walt Disney David Naughton, Stephen Furst and Maggie Roswell. The film is about a group of college students who participate in an all night puzzle solving race. It is Michael J. Foxs film debut.

==Synopsis==
Leon (Alan Solomon), a genius, summons five college students to his apartment and challenges them to participate in his latest game creation: The Great All-Nighter. He tells them about his game and instructs them to form teams. At first, the leaders refuse to play. However, rivalries between them lead all five to change their minds by the games start time.

The game works like this:

# Teams are given an initial clue to solve. 
# When the clue is solved, it will lead to a location.
# At that location they must find and solve another clue leading to another location and eventually the end.

Leon, as "game master," keeps track of the teams locations with a giant map, and various radio equipment. The teams are supposed to call and check in at each clue (though many of the teams end up skipping at least one location).

Starting at sundown, the five teams meet and are given envelopes with the first clue. They travel around Los Angeles, deciphering new clues in various locations, including the Griffith Observatory, a piano museum, the Pabst Blue Ribbon brewery, a restaurant, a mini golf course, the Los Angeles International Airport, and a video arcade. The first team to reach the final destination, a room in the Westin Bonaventure Hotel, wins the game.

The movie focuses mainly on the struggle between the yellow and blue teams. The adventures of the other three teams are subplots, as well as the situation at Leons apartment ("Game Control"). Here, along with his female assistants Candy and Sunshine (Debi Richter and Kirsten Baker), Leon monitors the progress of the game. Already unpopular with his landlord|landlady, Mrs. Grimhaus (Irene Tedrow), for the amount of noise he makes, Leon faces eviction if any of the other tenants complain. Several of them do show up to complain, but as Leon explains the mechanics of the game to them, they become fascinated with it and help run it, much to the annoyance of Grimhaus.

The game culminates in a race-to-the-finish at the Westin Bonaventure Hotel, followed by a huge party.

===Teams===
Teams are made up of characters who are broad stereotypes. They wear matching sweatshirts, and ride in vehicles that also match their team color.

*The members of the Yellow Team are all friendly and kind; they play fair and are the main heroes of the film. The yellow team are led by the protagonist Adam (Naughton). Partway through the game, they up an additional member, Adams troubled younger brother Scott (Michael J. Fox, in his first movie), who acts out to get Adams attention. They also force the shy Flynch (Joel Kenney), whom Adam has been counseling, to play the game rather than allow him to go on a date with an ugly girl. Also on the team are Laura (Debra Clinger), Adams love interest, and Marvin (David Damas), another friend of Adams. The team vehicle, owned by Marvin, is often referred to as a Jeep but it is actually a Toyota Land Cruiser.
*The members of the Blue Team are all selfish, rude, and unattractive individuals who cheat at every opportunity. They are led by overweight snob Harold (Stephen Furst), who is intensely jealous of the popular Adam. Melio (played by future Hollywood director Andy Tennant) purposely instigates fights between Harold and his girlfriend Lucille (Patricia Alice Albrecht), who puts Harold on a diet just before the game starts. "Blade" (Sal Lopez), a Mexican-American who is constantly brandishing his switchblade knife, never speaks. An additional member, Barf (Brian Frishman), is apparently mentally challenged.  The team vehicle is a Chevy van equipped with a computer that can solve clues; however, this device is destroyed early on when Harold hides a stash of marshmallows in the circuitry. jocks from the schools football team. They are led by Lavitas (Brad Wilkin); the others are nicknamed "Blaylak" (Dirk Blocker), "Armpit" (Curt Ayers), "Cudzo" (Trevor Henley) and "Gerber" (Keny Long). Their antagonism drives both the Red and White teams into playing. The team vehicle is a Volkswagen Beetle named the "Meat Wagon."
*The Red Team is made up of four members of an unpopular sorority, led by Donna (Maggie Roswell) and Berle (Robyn Petty) who are feminists. The other two members are a set of frequently giggling, overweight twins (Betsy Lynn and Carol Gwynn Thompson), and many of the jokes involving the red team come at their expense. The Red teams vehicle is a Datsun pickup truck, which is eventually destroyed by the Green team.
*The White Team is made up of debate team nerds, led by Wesley (Eddie Deezen). The White Team rides matching Puch mopeds, which they eventually share with the Red team after their vehicle is destroyed.

==Production notes==
Paul Reubens (better known as Pee-wees Playhouse|Pee-Wee Herman) has a small part as the "Pinball City Proprietor." Other cameos include John Fiedler as Wally Thorpe, one of the other tenants, and Marvin Kaplan as the Bonaventure Desk Clerk.

The Star Fire game in the video arcade that provides the clue to the final destination was an actual arcade game of the period. The game play was real; however a special open cabinet for a standing player had been created for the movie, since the real game cabinet was an enclosed cockpit in which the player was seated.

The movie was novelized in a 1980 paperback, Midnight Madness, by Tom Wright (Ace, 1980) ISBN 0-441-52985-2

Credited as Michael Fox, this was Michael J. Foxs first theatrical movie.

David Naughton, the main star, is seen drinking a Dr. Pepper soft drink.  He appeared in many Dr. Pepper commercials in the 70s.

A reference to Walt Disney Productions when the white team ends their search for a clue at the Mickey Mouse star on the Hollywood Walk of Fame.

==Release and reception==
Midnight Madness was rated MPAA film rating system|PG&mdash;only the second film from the Disney company to receive anything other than a "G" (the first was The Black Hole). Though produced by Disney, the companys name did not appear on the credits.
 cult following HBO cable network. After a 2001 DVD release from Anchor Bay Entertainment, Midnight Madness was re-released in 2004 by Disney DVD with the "Walt Disney Pictures Presents" logo&mdash;the first time that Disney has officially associated itself with the film.

==Legacy== Alternate Reality Games (ARG).  Among some of the more popular recreations are:

*Midnight Madness (Hot Springs, Arkansas) - Played every December
*Midnight Madness (Austin, Texas) - The Austin game is played biannually and was created by several Austin transplants including, two veterans from the Hot Springs game.
*  (Brevard County, Florida) - Played on a regular basis, with multiple games being held each year.
*Midnight Madness VT (Greater Burlington, VT) - Runs multiple games per year. midnightmadnessvt on Facebook. The Game - a non-stop 24-48 hour puzzle solving race that is currently active in the San Francisco Bay Area and the Seattle Area
*Mikes Hunt, a 24-hour game played by the members of the Rutgers University Glee Club, has a heavy clue-solving component, with the clues leading to the development of a storyline in which the players become involved.
*Get-a-Clue (Atlanta, Georgia) - Played annually by members of the Georgia Tech Yellow Jacket Marching Band and friends. Interactive and "nerdy" clues centering on a theme/storyline lead participants around the city and nearby counties.

===In popular culture===
*Rap duo Heltah Skeltah sampled the films theme for their song of the same name.
*The   "Fagabeefe". In the second, the chant of "Meat Machine" is reenacted.

==See also==
*Its a Mad Mad Mad Mad World
*The Last of Sheila
*Scavenger Hunt
*Million Dollar Mystery Rat Race

==External links==
*  
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 