Hollow Date
{{Infobox film
| name           = Hollow Date
| image          = HollowDateTuschinskiPosterFilm.jpg
| caption        =
| director       = Alexander Tuschinski
| producer       = Alexander Tuschinski
| writer         = Alexander Tuschinski
| starring       = Alexander Tuschinski Julia Csatary
| music          =
| cinematography = Matthias Kirste
| editing        = Alexander Tuschinski
| distributor    =
| released       =  
| runtime        = 3 minutes (at 24fps)
| country        = Germany
| language       = German
| budget         =
}}
 independent German short film directed by Alexander Tuschinski. It had its premiere at Cambridge Film Festival in September 2012.  It was screened at international film-festivals, winning an award at Berlin Independent Film Festival,  and was officially released online in July 2014. 

== Plot ==
The film demonstrates typical smalltalk among students in a stylized white room, showing the main protagonists growing frustration with the superficial talk in a comedic way. 

== Production ==
The film is edited from a scene from the movie Break-Up  The scene was originally supposed to be the beginning of a theatre-play which Alexander Tuschinski started writing in 2011, before deciding to rather incorporate it into Break-Up. 

== Reception ==

 
 

== Screenings / Awards ==
The film was screened at the following festivals:
* Berlin Independent Film Festival 2013: Best Short <5 min .  
* Cambridge Film Festival 2012: Official Selection,  in competition for Audience Award. 
* Hollywood Reel Independent Film Festival 2012: Official Selection. 
* Shärt International Comedy Film Festival 2014: Official Selection 
* Independent Days Karlsruhe 2013: Official Selection. 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 
 