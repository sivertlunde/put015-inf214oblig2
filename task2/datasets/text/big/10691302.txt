His Prehistoric Past
 
{{Infobox film
| name           = His Prehistoric Past
| image          = His prehistoric past.jpg
| caption        = A scene from the film
| director       = Charlie Chaplin
| producer       = Mack Sennett
| writer         = Charlie Chaplin
| starring       = Charlie Chaplin Mack Swain Sydney Chaplin Frank D. Williams
| distributor    = Keystone Studios
| released       =  
| runtime        = 21 minutes, 49 seconds (two reels)
| language       = Silent film English
| country        = United States
}}
 
  short silent silent comedy film, written and directed by Charlie Chaplin, featuring a Chaplin in a stone-age kingdom trying to usurp the crown of King Low-Brow to win the affections of the kings favorite wife. The film was Chaplins last at Keystone Studios.

==Synopsis==
Set in the stone age, King Low-Brow rules the land and a harem of wives. When Charlie arrives in this land (where every man has one thousand wives), he falls in love with the Kings favorite wife. When the King falls over a cliff, he is presumed dead and Charlie crowns himself King. The King, however, is not dead and comes back and bashes Charlie over the head with a rock. It turns out it was a dream and a police man bashed Charlie over the head with his club because he was sleeping in the park.

==Cast==
* Charlie Chaplin as Weak-Chin
* Mack Swain as King Low-Brow
* Fritz Schade as Ku-Ku aka Cleo, Medicine Man
* Cecile Arnold as Cavewoman
* Al St. John as Caveman
* Sydney Chaplin as Policeman
* Gene Marsh as Sum-Babee, Low-Brows Favorite Water Maiden

== References ==
 

== External links ==
* 
* 
*   on YouTube

 

 
 
 
 
 
 
 
 
 
 
 
 


 