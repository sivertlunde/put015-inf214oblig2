Dubai (2005 film)
{{Infobox Film | name = Dubai
  | image = DubaiCover.gif
  | image_size =
  | caption = Dubai DVD Cover
  | director = Rory B. Quintos
  | producer = Tess V. Fuentes Charo Santos-Concio Malou N. Santos
  | writer = Ricardo Lee Ricardo Lee Shaira Mella Salvador John Paul Abellera
  | starring = Aga Muhlach John Lloyd Cruz Claudine Barretto
  | cinematography = Charlie Peralta
  | editing = Marya Ignacio
  | distributor = Star Cinema
  | released = 28 September 2005
  | gross = P115,200,000 ($9,359,539.20) English
  |}} Filipino drama film shot in the United Arab Emirates, telling the story of three Overseas Filipino Workers who unexpectedly become connected by friendship and love. The film stars Aga Muhlach and John Lloyd Cruz, who play brothers, and Claudine Barretto, who plays the woman with whom the two men fall in love at the same time. The film was released by Star Cinema, ABS-CBN Film Productions.

==Plot summary==
Raffy and Andrew were orphaned as kids and had only each other to depend on. Raffy has spent the last nine years of his life working in Dubai. His ultimate goal is to fulfill a lifelong dream: to eventually move to Canada with his younger brother, Andrew. The Alvarez brothers are finally united when Andrew goes to Dubai.

In Dubai, Andrew meets Faye, one of Raffys many girlfriends. They hit it off well in spite of their age difference. She becomes his guide, comfort and lover. Seeing the two together, Raffy realizes that he still and really loves Faye. When Andrew discovers that Raffy still loves Faye, conflict arises between the brothers, almost severing the ties that bind them. In the end, what they choose and achieve are not as planned, but their experiences in Dubai lead to new beginnings in their lives.

==Cast and characters==

===Main cast===
*Aga Muhlach as Raffy
*John Lloyd Cruz as Andrew
*Claudine Barretto as Faye

===Supporting cast===
*Michael de Mesa
*Pokwang
*Dimples Romana
*Ana Capri
*Mico Aytona
*John Manalo
*Phoemela Baranda
*Mymy Davao
*  Quay Evano
*Jinkee Buzon-Castillo

==Soundtrack==
*"Ikaw Lamang"
*:Composed by Ogie Alcasid
*:Performed By Gary Valenciano

==See also==
* Filipinos in the United Arab Emirates

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 
 