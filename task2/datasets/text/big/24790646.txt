Touch of Death (1988 film)
{{Infobox film
| name = Touch of Death
| image = Touchofdeath.jpg
| caption = Shriek Show DVD Cover
| director = Lucio Fulci
| producer = Antonio Lucidi Luigi Nannerini
| writer = Lucio Fulci
| starring = Brett Halsey 	Ria De Simone Al Cliver
| cinematography = Silvano Tessicini
| music = Carlo Maria Cordio
| editing = Alberto Moriani
| distributor = 
| released = 1988
| runtime = 
| country = Italy
| awards = Italian
| budget = 
| preceded_by = 
| followed_by = 
}}
Touch of Death, also known as When Alice Broke the Mirror (in original Italian, Quando Alice ruppe lo specchio) is a 1988 Italian horror film directed by Lucio Fulci. 

==Plot==
Lester Parson (Brett Halsey) is a cannibal psychopath who regularly abducts and mutilates women, eating certain cuts and disposing the rest in his back yard to his horde of pigs. He converces schizophrenically with himself via tape recordings of his own voice. He is also being hounded by Randy (Al Cliver), a shady loan shark whom he owes money to after accruing bad gambling debts.

Lester picks up a certain Maggie MacDonald (Sasha Darwin), an obnoxious, hysterical, mustached, sexually frustrated alcoholic, whom he invites over at his house for dinner. His attempt to poison her is thwarted because she is already so drunk when she arrives at his place that she spills her glass of wine onto the floor. On the next attempt, she giddily mixed up his glass of wine with hers. He finally loses patience on the third try when she swallows the poisoned glassful, only to vomit before it can take effect. As Maggie excuses herself to the bathroom to clean up, he attacks her with a wooden stick. Her scalp splits apart and she runs screaming from the bathroom with blood streaming down her face. He chases her down the corridor bashing her head repeatedly, causing skin to rip away from her face, and a single bloodshot eyeball to roll out of her right eye socket onto the floor. Playing dead for a few seconds, Maggie rises when Parsons back is turned and makes another mad dash for the front door, but is rounded up and punched unconscious. Furious and exhausted, the killer shoves her head into a oven and switches it on, leaving her slumped with her flesh slowly melting off her face. Dead at last, Parson shoves Maggies body into the trunk of his Mercedes. But he has to chop off the corpses feet to get the body to fit right.

When Parson ditches the body at a construction site, he is observed by a local tramp (Marco Di Stefano), who proceeds to attempt to blackmail Parson. Not to be deterred, he follows the derelict as he leaves. Catching up with him on a long stretch of country road, Parson puts his foot on the gas peddle and pursues the terrified man, eventually crunching the vehicle under him. The cars wheels roll backwards over the mangled body. The next day, when Parson sees on the TV that the tramp survived long enough to give the police a description of his attacker. Parson decides to change his image by shaving off his beard and wearing contact lenses in place of his eyeglasses.

Parsons next victim is Alice Shogun (Ria De Simone), another crazed middle-aged woman who sings opera during sex. The weary killer strangles her to death with one of her stockings. Placing the body in the front seat of his car, Parson drives away, only to get pulled over by a motorcycle policeman and gets a speeding ticket. But the policeman does not notice that the woman with Parson is dead.

Taking Alices jewelry with him, he tries pawning it only to discover that is all fake. He tries to meet a horse fixer at a local racing stables for a tip about putting a bet on a horse, but the person never shows up. When more TV announcements give further descriptions of the mysterious killer, Parson is forced to chance his image again by dying the color of his black hair into brown, and wearing horn-rimmed, tinted eyeglasses.

Sitting morosely at home, Parson responds to an unlikely invitation to "come on over" from Virginia (Zora Ulla Kesler), a similarly bored, lonely, wealthy, but far younger woman than his previous victims, when she dials his phone number by accident. However, the otherwise desirable Virginia is revealed to have a large and attractive blemish on her upper lip. Even though she seems eager for intimacy, he is repulsed by her ugly scar. After Parson rings up another bad gambling debt to Randy, he decides to kill Virginia to steal whatever money and jewelry she has on her and flee the country.

The following evening, Parson meets Virginia in her apartment suite for dinner. When he is about to kill her, she shoots him the chest after discovering the truth about him after seeing another TV broadcast of the latest description of the mysterious lady killer. Mortally wounded, Parson crawls away and ends up in the building garage where he converses with his other self, a shadow on the wall, and eventually dies as it converges with him.

==Critical response==
Though many fans are divided, generally "Touch of Death" is regarded as a better latter era Fulci film.  The overdone black humor touches, unconvincing gore effects and baffling ending does turn off some fans. 

==Releases==
Synapse Films originally passed on releasing "Touch of Death" on DVD when only video sources could be obtained as no original elements or prints could be located. This means the original elements either no longer exist or are temporarily lost. Despite only having video elements to work with, Media-Blasters subsidiary Shriek Show picked up the rights to release this film on DVD.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 