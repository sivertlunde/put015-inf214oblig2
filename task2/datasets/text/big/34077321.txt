The Book of Zombie
{{Infobox film
| name           = The Book of Zombie
| image          = TheBookOfZombie2010Poster.jpg
| alt            =  
| caption        = Film poster
| director       = {{plainlist|
* Scott Kragelund
* Paul Cranefield
* Erik Van Sant
}}
| producer       = {{plainlist|
* Scott Kragelund
* Paul Cranefield
* Erik Van Sant
}}
| writer         = Erik Van Sant
| starring       = {{plainlist|
* Brian Ibsen
* Larisa Peters
* Andrew Loviska
* Paul Cantu
* Bill Johns
* Adrienne MacIain
* Andy Evans
* Adam Gehrke
* Elissa Dowling
}}
| music          = Adam Gehrke
| cinematography = Dave Preston
| editing        = 
| studio         = DMZ Entertainment
| distributor    = Left Films
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         = $15,000 
| gross          = 
}}
The Book of Zombie is a 2010 American independent horror film written by Erik Van Sant and directed by Scott Kragelund, Paul Cranefield and Erik Van Sant.  Brian Ibsen, Larisa Peters, Andrew Loviska, Paul Cantu, Bill Johns, Adrienne MacIain, Andy Evans, Adam Gehrke, and Elissa Dowling star as survivors of a Mormon-themed zombie attack.

==Plot==
On Halloween in a small, sleepy town in Utah, David Driscoll and Jenny King, who are involved in a relationship gone cold, try to settle down for a romantic evening, but they are visited by two Mormon door-to-door salesmen, who seem to suffer from a small affliction. David shrugs them off with a few bad jokes and tries to resume his evening plans when the Mormons ring again – this time, as blood-thirsty Zombie (fictional)|zombies, along with several others who begin to overrun their neighborhood. After escaping their door-callers when one of them recoils from a soda thrown in his face, David decides to see an acquaintance, a convenience shop and bar owner named Harry Linderman, who owns a double-barrel shotgun. They reach the shop, only to meet a youth named Darwin Nedry and his substance-sniffing friend Charlie Cooper, who tell them that Linderman left with his gun after a phone call reached him from his medieval-themed bar, The Drunken Dragon.

Not knowing how to retrieve their little daughter Charlotte, who is staying out of the house this night, without a weapon, David and Jenny decide to stay in the store for the time being. Soon, however, an armed, gritty hunter named Boothe Gardener stages a dramatic entrance into the store and stays for the night as well, following his own close brush with the zombified Mormon population in which his friend Charlie was killed. Owing to the incident at his house door, David concludes that the zombies are vulnerable to things which are taboo to Mormons, including caffeine. When a zombie breaks into the store, Gardener is mortally wounded after he tries to fight it off using non-caffeinated soda. Darwin destroys the zombie with caffeinated soda.

Forced to evacuate the store, David and company make for the Drunken Dragon but draw dozens of zombies. In the nick of time, they are led inside by armor-clad bartender Piper McKenzie, who tells them that Linderman was zombified during a zombie riot in the bar and is now kept locked up in a side room because she cannot bring herself to finish him. After discovering that Darwin keeps several explosive prank devices in his backpack, David devises a plan to reach Pipers car and get to his daughter by letting Linderman out, strapping the explosives and their scant soda supplies onto his back and using him as an involuntary suicide bomber to take out the zombies encroaching on the bar. The plan quickly goes awry when Linderman bites Darwin just after he is released, dooming him, before the youngster blasts his head with his own shotgun. Faced with no other choice, Darwin assumes the role of the bomber as a last heroic act, but he turns before he can light the fuse. Piper lights the explosives with a crossbow and fire arrow, but the blast proves too weak to take out all the zombies; before the door can be re-barricaded, the undead break into the bar and devour Piper alive, while Charlie faints.

Retreating into the main taproom, David and Jenny fight and destroy the undead, but by retreating too far, Jenny is caught by a zombie breaking through a window and mortally wounded. After promising Jenny that he will find their daughter, David hugs her until she dies, decapitates her to prevent reanimation, and then kills the remaining zombies in a berserk rage. As morning dawns, David staggers off, filled with revenge, to finish the undead, with an unsure Charlie in tow.

The film ends in some institution where a file labelled The Utah Project containing information about mutation warfare is presented to a group of boardroom-type people, followed by a file detailing an upcoming project ominously named The Vatican Solution. As the file courier exits the boardroom, the camera backtracks his path to the reception of an office building, where a receptionist answers a phone call with the words, "Thank you for calling Church of Scientology. How may we help you?"

==Cast==
* Brian Ibsen as David Driscoll
* Larisa Peters as Jenny King
* Andrew Loviska as Darwin Nedry
* Paul Cantu as Charlie Cooper
* Bill Johns as Boothe Gardener
* Adrienne MacIain as Piper McKenzie
* Andy Evans as Brother Smith
* Adam Gehrke as Brother Joseph
* Elissa Dowling as Henrietta

==Production==
The majority of the production was shot during the summer of 2007 and finished in 2009.   The filmmakers did not single out Mormonism for any particular reason. The concept came from a joke during a brainstorming session, and one of the producers decided to develop it. Major influences include George A. Romero, Troma Entertainment, and Edgar Wright. 

==Release==
The premiere of The Book of Zombie was on June 11, 2011, at Seattles True Independent Film Festival.      In October 2012, it was featured in the collection of zombie shorts Ultimate Zombie Feast, which was released in the UK. 

==Reception==
HorrorNews.net called The Book of Zombie "one kick ass zombie film".   Gareth Jones of Dread Central called the film "consistently funny and very entertaining".   In a mixed review, academic Peter Dendle wrote that "not every gag in the horror-comedy hits home, but the boldness of the movies premise goes a long way." 

===Awards===
The film won the audience award at Seattles True Independent Film Festival. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 