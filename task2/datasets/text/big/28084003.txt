Bed & Breakfast (2010 film)
{{Infobox film
| name           = Bed & Breakfast
| image          = Bed and Breakfast.jpg
| caption        = Promotional poster
| director       = Márcio Garcia
| producer       = Fabio Golombek Theodore Melfi Uri Singer Márcio Garcia
| writer         = Leland Douglas John Savage Julia Duffy Eric Roberts Rodrigo Lombardi Marcos Pasquim
| music          = Old Man River
| cinematography = Craig Kief
| editing        = Hovig Menakian
| studio         = BB Film Productions
| distributor    = FJ Productions
| released       =  
| runtime        = 93 minutes
| country        = United States Brazil
| language       = English and Portuguese
| budget         = 
| gross          = 
}} 
Bed & Breakfast is a 2010 romantic comedy directed by Márcio Garcia and written by Leland Douglas. Bed & Breakfast is produced by B.B. Film Productions and F.J. Productions, Inc.

==Cast==
* Eric Roberts as Mr. Hopewell
* Dean Cain as Jake  John Savage as Mr. Harvey 
* Julia Duffy as Mrs. Harvey  Kimberly Quinn as Amanda 
* Bill Engvall as Pete Sullivan 
* Juliana Paes as Ana 
* Meredith Bishop as Celeste 
* Jamie Anderson as Caroline 
* Calvin Jung as Mr. Okata 
* Rodrigo Lombardi as Bartolomeu 
* Julian Stone as Victor 
* Emily Nelson as Hank 
* Zilah Mendoza as Maria 
* Lydia Look as Ms. Okata 
* Luiza Valdetaro as Babita 
* Marcos Pasquim as Gustavo 
* Priscila Marinho as Flavia 
* Kimberly Dollar as Waiter 
* Débora Lamm as Gabriela 
* Daniel Ávila as Paulo 
* Murilo Elbas as Thug One 
* Júnior Lisboa as Thug Two 
* Jesse Jake Golden as Girls One

==External links==
*  
*  

 
 
 
 
 
 
 
 


 