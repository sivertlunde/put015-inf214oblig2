Commotion on the Ocean
{{Infobox Film |
  | name           = Commotion on the Ocean|
  | image          = CommotionontheOceanTITLE.jpg |
  | caption        =  |
  | director       = Jules White Edward Bernds (stock footage) | Felix Adler Elwood Ullman (stock footage) | Larry Fine Charles C. Wilson|
  | cinematography = Ray Cory Henry Freulich (stock footage) | Harold White Henry Demond (stock footage) |
  | producer       = Jules White Hugh McCollum (stock footage) |
  | distributor    = Columbia Pictures |
  | released       = November 8, 1956 (United States|U.S.) |
  | runtime        = 16 34" |
  | country        = United States
  | language       = English
}}
Commotion on the Ocean is the 174th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges play janitors who work at a newspaper office, begging to be given a chance to become reporters. The managing editor (Charles C. Wilson) promises to think about it over dinner. The phone rings while he is out and Moe answers. The person on the other end is one of the bosss reporters, Smitty (Emil Sitka), who relays a scoop to Moe that some important documents have been stolen by foreign spies. Coincidentally, the spy with the microfilmed documents, Mr. Borscht (Gene Roth) lives next door to the Stooges. He and the boys wind up as stowaways on an ocean liner. Stranded on a freighter on the high seas, and sustained by eating salami, the boys eventually overtake Borscht, recover the microfilm, and are thrilled with their newspaper scoop.

==Production notes==
Commotion on the Ocean is a remake of 1949s Dunked in the Deep, using ample stock footage. In addition, the newspaper room scenes were borrowed from 1948s Crime on Their Hands.  Commotion on the Ocean was the last of four shorts filmed in the wake of Shemp Howards death using earlier footage and a stand-in.

The films plot device of hiding microfilm in watermelons is an allusion to an actual event that occurred in 1948.   documents, which Chambers had concealed in a hollowed-out pumpkin on his Maryland farm.  

==="Fake Shemp"===
  article]]
  Hot Stuff, Scheming Schemers and Commotion on the Ocean), Columbia utilized supporting actor Joe Palma to be Shemps double. Even though the last four shorts were remakes of earlier Shemp efforts, Palmas services were needed to link what few new scenes were filmed to the older stock footage. Solomon, Jon. (2002) The Complete Three Stooges: The Official Filmography and Three Stooges Companion, p. 481; Comedy III Productions, Inc., ISBN 0-9711868-0-4 

For Commotion on the Ocean, Palma appears in one new shot during the newspaper office scene. After Larry says, "Oh, I know Smitty: Under the spreading chestnut tree, the village smitty stands," Moe slaps him. Palma gets involved in the slapstick exchange and shields himself in defense, obstructing his face. 

All other new footage consists of Moe and Larry working as a duo, often discussing Shemps absence aloud:
*Moe: "I wonder what became of that Shemp?"
*Larry: "You know he went up on deck to scout for some food." 
  (left) in Commotion on the Ocean]]

This new footage was shot on January 17, 1956, only six weeks after Shemps death and one day after the previous film, Scheming Schemers. 

==Quotes==
*Larry: "You can take my word for it; when it comes to fish, Im a common-sewer!"

==References==
 

== External links ==
*  
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 