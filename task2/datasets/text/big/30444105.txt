Flowers of Reverie
{{Infobox film
| name           = Flowers of Reverie
| image          = 
| caption        = 
| director       = László Lugossy
| producer       = 
| writer         = László Lugossy István Kardos
| starring       = György Cserhalmi
| music          = 
| cinematography = Elemér Ragályi
| editing        = László Lugossy
| distributor    = 
| released       = 1984
| runtime        = 102 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}
 Silver Bear - Special Jury Prize.   

==Cast==
* György Cserhalmi as Majláth Ferenc, huszárfõhadnagy
* Grazyna Szapolowska as Mária, Majláth felesége
* Jirí Adamíra as Heinrich nagybácsi
* Boguslaw Linda as Tarnóczy Kornél
* Péter Malcsiner as Tarnóczy Miklós
* Lajos Öze as Ezredes (as Õze Lajos)
* Angéla Császár as Mária magyar hangja (voice)
* Tibor Kristóf as Heinrich magyar hangja (voice)
* Sándor Szakácsi as Kornél magyar hangja (voice)
* Vilmos Kun as Börtönigazgató
* Mátyás Usztics as Rendõrparancsnok
* Kati Marton as Dada (as Marton Katalin)
* Frigyes Hollósi as Head Physician of Madhouse
* Árpád Csernák as Eszelõs katona
* Iván Dengyel as Magyar õrnagy

==References==
 

==External links==
* 

 
 
 
 
 
 
 