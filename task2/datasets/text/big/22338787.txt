The Reward
The Michael Barrett and starring Max von Sydow, Yvette Mimieux and Efrem Zimbalist Jr.. 

==Plot==
El Paso crop duster Scott Svenson accidentally flies his plane into a water tower across the Mexico border. By chance he spots a friend, Frank Bryant, in a car with a woman.

Svenson then notices Bryants face on a $50,000 reward poster. Since he must pay for the damage to the tower, Svenson offers to assist a local law enforcement official, Capt. Carbajal, in tracking down and capturing Bryant, whereupon they would split the reward.

A posse is formed that includes Sgt. Lopez and two other men, Joaquin and young Luis, who dreams of becoming a bullfighter. Bryant and the woman, Sylvia, are tracked down, but Lopez now wants a percentage of the reward for his efforts. And as soon as Joaquin makes a decision to help Bryant and the woman escape, Lopez kills both Bryant and Joaquin.

Luis tries to round up the remaining posses horses, but dies in the attempt. Carbajal then is stricken with malaria and turns seriously ill. There is little left to do for Svenson and the woman except try to get back to town safely on foot.

==Cast==
* Max von Sydow as Svenson
* Efrem Zimbalist, Jr. as Bryant
* Yvette Mimieux as Sylvia
* Gilbert Roland as Capt. Carbajal
* Emilio Fernandez as Sgt. Lopez
* Henry Silva as Joaquin

==Production==
The film was made for $2,685,000. 

==References==
 

 
 
 
 
 
 
 


 