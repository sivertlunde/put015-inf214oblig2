The War of the Worlds (1953 film)
 
 
 
{{Infobox film
| name            = The War of the Worlds
| image           = Film poster The War of the Worlds 1953.jpg
| alt             =  Theatrical release poster
| director        = Byron Haskin
| producer        = George Pal
| screenplay      = Barré Lyndon
| based on        =  
| starring        = Gene Barry Ann Robinson
| narrator        = Sir Cedric Hardwicke
| music           = Leith Stevens George Barnes
| editing         = Everett Douglas
| studio          = Paramount Pictures
| distributor     = Paramount Pictures
| released        =  
| runtime         = 85 minutes  
| country         = United States
| language        = English
| budget          = $2 million
| gross           = $2,000,000   
}} American Technicolor science fiction film from Paramount Pictures, produced by George Pal, directed by Byron Haskin, and starring Gene Barry and Ann Robinson. 
 novel of the same name, and is the first of five feature film adaptations of his famous 1898 novel concerning an invasion of Earth from the planet Mars. Large meteorite-spaceships come crashing down all over the Earth, disgorging Manta Ray-shaped Martian war machines armed with fearsome heat-ray and "skeleton" beam energy weapons; they slowly begin the rout of humanity wherever they move.

==Plot==
In early 1950s southern California, Doctor Clayton Forrester (War of the Worlds)|Dr. Clayton Forrester, a scientist who had worked on the Manhattan Project, is fishing with colleagues when a large object crashes near the town of Linda Rosa. At the impact site, he meets Sylvia Van Buren and her uncle, Pastor Matthew Collins. Van Buren was told that the meteorite came down at a low angle, while Forrester observes it appears far lighter than normal for its massive size. His Geiger counter also detects it is slightly radioactive, but the object is still too hot to examine closely. Unable to account for these anomalies, Forrester is intrigued and decides to wait in town overnight for the object to cool down.
 Martian Tripod war machine. Three men who remained behind as night guards at the crash site approach, waving a white flag, but the cobra-head vaporizes them with a heat-ray; it also damages a nearby electrical tower, knocking out the power to Linda Rosa. Dr. Forrester notices that his and other peoples watches have stopped running, having become magnetized; he then observes the sheriffs compass now points away from magnetic north and towards the meteorite crash site. Forrester and the sheriff go to investigate and are attacked by the Martian heat-ray; both survive to raise the alarm. 
 the Marines force field. The Martians then use their advanced weapons to send the military force into full retreat. Military leaders of the Sixth United States Army later gather in Los Angeles to brief reporters and formulate a counterattack defense plan, as well as prepare for an evacuation of major cities in the path of the Martians.
 Pacific Tech in Los Angeles. From the blood sample and the electronic eyes optics, the scientists make deductions about Martian eyesight and physiology, in particular that the creatures are physically weak and have anemic blood. 

  atomic bomb on the war machines, but it cannot penetrate their force fields; the Martians continue to advance and the government orders an immediate evacuation of Los Angeles. The Pacific Tech scientists estimate the Earth can be conquered in just six days. Widespread panic among the populace scatters the Pacific Tech group as they leave; a mob stops and then steals their trucks and wrecks their equipment, and in the chaos Forrester and Van Buren become separated. 
 God in His wisdom had put upon this Earth" have saved mankind from extinction.

==Cast==
 
* Gene Barry as Doctor Clayton Forrester (War of the Worlds)|Dr. Clayton Forrester
* Ann Robinson as Sylvia van Buren
* Les Tremayne as General Mann Bob Cornthwaite as Dr. Pryor
* Sandro Giglio as Dr. Bilderbeck Lewis Martin as Pastor Dr. Matthew Collins
* Housely Stevenson Jr. as General Manns aide
* Paul Frees as Radio reporter Bill Phipps as Wash Perry
* Vernon Rich as Colonel Ralph Heffner Henry Brandon as Cop at crash site
* Jack Kruschen as Salvatore
* Sir Cedric Hardwicke as Voice of the narrator Martian
* Gertrude W. Hoffmann (uncredited) as Elderly news vendor
 

==Production==
The War of the Worlds opens with a black-and-white prologue featuring newsreel war footage and a voice-over describing the destructive technological advancements of Earthly warfare from World War I through World War II. The image then smash cuts to vivid Technicolor and the dramatic opening title card and credits which follow. The story then begins with a series of color matte paintings by astronomical artist Chesley Bonestell depicting the planets of our Solar System (all except Venus). A narrator (Sir Cedric Hardwicke) offers a tour of the hostile environment of each world, eventually explaining to the audience why the Martians find our lush, green and blue Earth the only world worthy of their scrutiny and coming invasion.  Rubin 1977, pp. 4–16, 34–47.  

This is the first of two adaptations of Wells classic science fiction filmed by George Pal; it is considered to be one of the great science fiction films of the 1950s.  
 montage of destruction to show the worldwide invasion, with armies of all nations joining together to fight the invaders. Rubin 1977, pp. 4–16, 34–47. 

Dr. Forresters and the other scientists "Pacific Tech" ("Pacific Institute of Technology") has since been referenced in other films and television episodes whenever directors/writers/producers needed to depict a science-oriented California university without using a specific institutions name. Warren 1982, pp. 151–163.  
 Corona was El Sereno were also used in the film.  
 commentary track of the Special Collectors DVD Edition of War of the Worlds, Robinson and Barry point out that the cartoon character Woody Woodpecker is seen in a tree top, center screen, when the first large Martian meteorite-ship crashes through the sky near the beginning of the film. Woodys creator Walter Lantz and George Pal were close friends. Pal tried to always include the Woody character out of friendship and good luck in his productions, noted Joe Adamson years later, "Walter had been close friends with Pal ever since Pal had left Europe in advance of the war and arrived in Hollywood." Adamson, Joe.     

===Differences from the Wells novel===	
As noted by Caroline Blake,  the film is very different from the original novel in its attitude toward religion, as reflected especially in the depiction of clergymen as characters. "The staunchly secularist Wells depicted a cowardly and thoroughly uninspiring Curate, whom the narrator regards with disgust, with which the reader is invited to concur. In the film there is instead the sympathetic and heroic Pastor Collins who dies a martyrs death. And then the films final scene in the church, strongly emphasizing the Divine nature of Humanitys deliverance, has no parallel in the original book."

Pals adaptation has many other notable differences from H. G. Wells novel. The closest resemblance is probably that of the antagonists. The films aliens are indeed Martians, and invade Earth for the same reasons as those stated in the novel (the state of Mars suggests that it is in the final stages of being able to support life, leading to the Martians decision to make Earth their new home). They land in the same way, by crashing to the Earth. However, the novels spacecraft are large cylinder-shaped projectiles fired from the Martian surface from some kind of cannon, instead of the films meteorite-spaceships; but the Martians emerge from their craft in the same way, by unscrewing a large, round hatch. They appear to have no use for humans in the film. In the novel, however, the invaders are observed "feeding" on humans by fatally transfusing their captives blood supply directly into Martian bodies by using pipettes; there is also a later speculation about the Martians eventually using trained human slaves to hunt down all remaining survivors after they have conquered Earth. In the film the Martians do not bring their fast-growing red weed with them, but they are defeated by Earth microorganisms, as observed in the novel. However, they die from the effects of the microorganisms within three days of the landing of the first meteorite-ship; in the novel the Martians die within about three weeks of their invasion of England. 
	 	
The Martians themselves bear no physical resemblance to the novels Martians, who are described as bear-sized, roundish creatures with brown bodies, "merely heads", with quivering beak-like, V-shaped mouths dripping saliva; they have sixteen whip-like tentacles in two groupings of eight arranged on each side of their mouths and two large "luminous, disk-like eyes". Due to budget constraints, their film counterparts are short, reddish-brown creatures with two long, thin arms with three long, thin fingers with suction cup tips. The Martians "head", if it can be called that, is a broad "face" at the top-front of its broad shouldered upper torso, the only apparent feature of which is a single large eye with three distinctly colored lenses (red, blue, and green). The Martians lower extremities, whatever they may be, are never shown. (Some speculative designs for the creature suggest the idea of three thin legs resembling their fingers, while others show them as bipeds with short, stubby legs with three-toed feet.)  
	 	
The films Martian war machines do actually have more of a resemblance than they may seem at first glance. The novels machines are 10-story tall tripods and carry the heat-ray projector on an articulated arm connected to the front of the war machines main body. The films machines are shaped like Manta rays, with a bulbous, elongated green window at the front, through which the Martians observe their surroundings. On top of the machine is the cobra head-like heat-ray attached to a long, narrow, goose-neck extension. They can be mistaken for flying machines, but Forrester states that they are lifted by "invisible legs"; in one scene, when the first machine emerges, you can see faint traces of three energy legs beneath and three sparking traces where the three energy shafts touch the burning ground. Therefore, technically speaking, the films war machines are indeed tripods, though they are never given that designation. Whereas the novels war machines had no protection against British army and navy cannon fire, the films war machines have a force field surrounding them; this invisible shield is described by Forrester as a "protective blister". 
	  	
The Martian weaponry is also partially unchanged. The heat-ray has the very same effect as that of the novel. However, the novels heat-ray mechanism is briefly described as just a rounded hump when first seen in silhouette rising above the landing craters rim; it fires an invisible energy beam in a wide arc while still in the pit made by the first Martian cylinder after it crash-lands. The films heat-ray projector when first seen is shaped like a cobras head and has a single, red pulsing "eye", which likely acts like a targeting telescope for the Martians inside their Manta ray-like war machine. The novel describes another weapon, the "black smoke" used to kill all life; the war machines fire canisters  containing a black smoke-powder through a bazooka-like tube accessory. When dispersed, this black powder is lethal to all life forms who breathe it. This weapon is replaced in the film by a Martian "skeleton beam", green pulsing bursts fired from the wingtips of the Manta-Ray machines; these bursts break apart the sub-atomic bonds that hold matter together on anything they touch. These skeleton beams are used off screen to obliterate several French cities. 
	  	
The plot of the film is very different from the novel, which tells the story of a 19th-century writer (with additional narration in later chapters by his medical student younger brother), who journeys through Victorian London and its southwastern suburbs while the Martians attack, eventually being reunited with his wife; the films protagonist is a California scientist who falls in love with a former college student after the Martian invasion begins. However, certain points of the films plot are similar to the novel, from the crash-landing of the Martian meteorite-ships to their eventual defeat by Earths microorganisms. Forrester also experiences similar events like the books narrator: an ordeal in a destroyed house, observing an actual Martian up close, and eventually reuniting with his love interest at the end of the story. The film is given more of a Cold War theme with its use of the Atomic Bomb against the enemy and the mass-destruction that such a global war would inflict on mankind. 

===Music=== When Worlds Collide. 

===Special effects=== Oscar for its special effects and was later selected for inclusion in the National Film Registry of the Library of Congress. 
 
An effort was made to avoid the stereotypical flying saucer look of  , also directed by Byron Haskin; that film prop was later reported melted down as part of a scrap copper recycling drive.  (The model the late Forrest Ackerman had in his massive, now dispersed Los Angeles science fiction collection was a replica made using the Robinson Crusoe on Mars blueprints; it was constructed by friends Paul and Larry Brooks.)
 The Outer Limits, particularly in the episode "The Children of Spider County". 

The machines also fired a pulsing green ray (referred to as a skeleton beam) from their wingtips, generating a distinctive sound, also disintegrating their targets, notably people; this second weapon is a replacement for the  , accompanying the launch of photon torpedos. Another prominent sound effect was a chattering, synthesized echo, perhaps representing some kind of Martian sonar; it can be described as sounding like hissing electronic rattlesnakes. 

When the large Marine force opens fire on the Martians with everything in their heavy arsenal, each Martian machine protected by an impenetrable   and with a hemispherical top. This effect was accomplished by the use of simple matte paintings on clear glass, which were then photographed and combined with other effects, then optically printed together during post-production. 

The disintegration effect took 144 separate matte paintings to create. The sound effects of the war machines heat rays firing were created by mixing the sound of three electric guitars being recorded backwards. The Martians scream in the farmhouse ruins was created by mixing the sound of a microphone scraping along dry ice being combined with a womans recorded scream and then reverse-played for the sound effect mix. 
 tripods of Wells novel. It was eventually decided to make the Martian machines appear to float in the air on three invisible legs. To show their existence, subtle special effects downward lights were to be added directly under the moving war machines; however, in the final film, these only appear when one of the first machines can be seen rising from the Martians landing site. It proved too difficult to mark out the invisible legs when smoke and other effects had to be seen beneath the machines, and the effect used to create them also created a major fire hazard. In all of the subsequent scenes, however, the three invisible leg beams create small, sparking fires where they touch the ground. 

==Reception==
===Release=== premiere on general theatrical release until the autumn of that year.  The film was both a critical and box office success. It accrued $2,000,000 in distributors domestic (U.S. and Canada) rentals, making it the years biggest science fiction film hit. Gebert 1996    

===Critical reaction===
The New York Times review of The War of the Worlds by Armond White, noted, "  an imaginatively conceived, professionally turned adventure, which makes excellent use of Technicolor, special effects by a crew of experts, and impressively drawn backgrounds ... Director Byron Haskin, working from a tight script by Barré Lyndon, has made this excursion suspenseful, fast and, on occasion, properly chilling."  "Brog" in Variety (magazine)|Variety felt, "  a socko science-fiction feature, as fearsome as a film as was the Orson Welles 1938 radio interpretation...what starring honors there are go strictly to the special effects, which create an atmosphere of soul-chilling apprehension so effectively   audiences will actually take alarm at the danger posed in the picture.  It cant be recommended for the weak-hearted, but to the many who delight in an occasional good scare, its socko entertainment of hackle-raising quality." "Brog".Variety, April 6, 1953.  
 Special Effects Film Editing Sound Recording respectively.   oscars.org. Retrieved: January 11, 2015. 

The War of the Worlds still receives high acclaim from critics: At the review aggregate website Rotten Tomatoes, the film has an 85% rating based on 27 critics; the consensus states: "Though its dated in spots, The War of the Worlds retains an unnerving power, updating H. G. Wells classic sci-fi tale to the Cold War era and featuring some of the best special effects of any 1950s film." 

==Legacy==
The War of the Worlds was deemed culturally, historically, or aesthetically significant in 2011 by the United States Library of Congress and was selected for preservation in the National Film Registry.  The Registry noted the films release during the early years of the Cold War and how it used "the apocalyptic paranoia of the atomic age."   Library of Congress, December 28, 2011. Retrieved: January 11, 2015.  The Registry also cited the films special effects, which at its release were called "soul-chilling, hackle-raising, and not for the faint of heart." 

;American Film Institute lists
* AFIs 100 Years...100 Movies – Nominated 
* AFIs 100 Years...100 Thrills – Nominated 
* AFIs 100 Years...100 Heroes and Villains:
** Martians – #27 Villains
* AFIs 10 Top 10 – Nominated Science Fiction Film 
 
The 1988 War of the Worlds (TV series)|War of the Worlds TV series is a sequel to the Pal film; Ann Robinson reprises her role as Sylvia Van Buren in three episodes. Robinson also reprises her role in two other films, first as Dr. Van Buren in 1988s Midnight Movie Massacre and then as Dr. Sylvia Van Buren in 2005s The Naked Monster. 
 
In   is replaced with a nuclear armed cruise missile launched by a Northrop Grumman B-2 Spirit|B-2 Spirit bomber (a direct descendant of the Northrop YB-49 bomber used in the original) and Captain Hiller being based in El Toro, California, which Dr. Forrester mentions as being the home of the Marines, which make the first assault on the invading Martians in the 1953 film. 
 adaptation as H. G. Wells War of the Worlds has direct references to the Pal version: Inside the Martians mouth, they have three tongues that closely resemble the three Martian fingers seen in the Pal film. The Asylum film also includes scenes of power outages after the aliens arrival via meteorite-ships. As in the Pal film, refugees are seen hiding in the mountains instead of hiding underground, as in the Wells novel. 
 
Steven Spielbergs 2005 War of the Worlds (2005 film)|adaptation, although an adaptation of the original Wells novel, does feature several references to the original film: Gene Barry and Ann Robinson have cameo appearances near the end, and the invading aliens have three-fingered hands but are depicted as reptile-like, three-legged walking tripods. There is also a long, snake-like alien camera probe deployed by the invaders in much the same manner as in the 1953 film. Desowitz, Bill.   VFXWorld, July 7, 2005. Retrieved: January 12, 2015. 

Mystery Science Theater 3000 named one of its lead characters, the mad scientist  Doctor Clayton Forrester (MST3K)|Dr. Clayton Forrester, as a homage to the 1953 film.   Academy of Television Arts & Sciences. Retrieved: January 12, 2015. 

==References==

Notes
 

Citations
 

Bibliography
 
* Aberly, Rachel and Volker Engel. The Making of Independence Day. New York: HarperPaperbacks, 1996. ISBN 0-06-105359-7.
* Adamson, Joe.   New York: G.P. Putnams Sons, 1985. ISBN 978-0-39913-096-0.
* Booker, M. Keith. Historical Dictionary of Science Fiction Cinema. Metuchen, New Jersey: Scarecrow Press, Inc., 2010. ISBN 978-0-8108-5570-0
* Gebert, Michael. The Encyclopedia of Movie Awards, New York: St. Martins Press, 1996. ISBN 0-668-05308-9.
* Hickman, Gail Morgan. The Films of George Pal. New York: A. S. Barnes and Company, 1977. ISBN 0-498-01960-8.
* Parish, James Robert and Michael R. Pitts. The Great Science Fiction Pictures. Jefferson, North Carolina: McFarland & Company, 1977. ISBN 0-8108-1029-8.
* Rubin, Steve. "The War of the Worlds." Cinefantastique magazine, Volume 5, No. 4 1977.
* Strick, Philip. Science Fiction Movies. London: Octopus Books Limited, 1976. ISBN 0-7064-0470-X.
* Bill Warren (film historian and critic)|Warren, Bill. Keep Watching The Skies Vol I: 1950–1957. Jefferson, North Carolina: McFarland & Company, 1982. ISBN 0-89950-032-3.
* Willis, Don, ed. Varietys Complete Science Fiction Reviews. New York: Garland Publishing, Inc., 1985. ISBN 0-8240-6263-9.
 

==External links==
 
*  
*  
*  
*  
*  
*   at site dedicated to all things War Of The Worlds
*  
*   on Lux Radio Theater: February 8, 1955. Adaptation of 1953 film.
*   at Angry Alien Productions
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 