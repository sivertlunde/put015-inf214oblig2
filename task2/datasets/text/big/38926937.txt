Drinking Buddies
 
{{Infobox film
| name           = Drinking Buddies
| image          = Drinking Buddies poster.jpg
| caption        = Theatrical release poster
| alt            = Two men and two women, sitting on the floor, shoulder to shoulder. 
| director       = Joe Swanberg
| producer       = {{Plain list | 
* Andrea Roa
* Joe Swanberg
* Alicia Van Couvering
* Paul Bernon
* Sam Slater
}}
| writer         = Joe Swanberg
| starring       = {{Plain list | 
* Olivia Wilde 
* Jake Johnson 
* Anna Kendrick 
* Ron Livingston
}}
| music          = Dominic Lewis Ben Richardson
| editing        = Joe Swanberg
| studio         = Burn Later Productions
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 90 minutes   
| country        = United States
| language       = English
| budget         = $1 million   
| gross          = $343,341   
}}

Drinking Buddies is a 2013 American film written and directed by Joe Swanberg, and starring Olivia Wilde, Jake Johnson, Anna Kendrick and Ron Livingston. The film is about two co-workers at a craft brewery in Chicago.

The film premiered at the 2013 South by Southwest Film Festival,    and also screened within Maryland Film Festival 2013.

==Plot==
Kate ( ), and Luke is with Jill (Anna Kendrick). Jill presses Luke to see if he is yet ready to talk about marriage, which he promises he will be some time soon.

Luke is busy brewing beer and Kate handles phone calls to set up the anniversary party for the brewery. During lunch they agree to go out for drinks after dinner with their co-workers from the brewery. While all are drinking at the bar, Lukes fiancee, Jill, arrives. After the bar, Kate rides her bike over to the apartment of her socially awkward, but extremely nice boyfriend where he gives her dinner and a copy of Rabbit, Run. Later, at the brewery party, Chris invites Luke and Jill to join them for a trip to his familys cottage. 

During the trip Jill and Chris go for a long hike in the woods, where they end up kissing. Luke and Kate spend the whole trip drinking and staying up late alone together, including a bonfire on the beach. Kate invites Luke to go skinny dipping before stripping off her clothes and running into the lake.

After the group returns home, Chris decides he needs to talk to Kate and the two break up. Newly single Kate insists the whole brewery crew go out the next evening for drinks, to celebrate her singleness, and that night she ends up sleeping with a co-worker, Dave. When Luke hears about this he is angry at both Dave and Kate, and spends the whole day pissed off, snapping at both of them, but eventually apologizes for his behavior. Jill decides to go away for a week with some friends from college. During this trip, Luke and Kate go out for dinner and fall asleep together on the couch. Luke agrees to help Kate clean out her old apartment and move the next day, and at the end of the day Luke falls asleep on the bed. Kate joins him.

During the next morning, Luke invites Kate to dinner to celebrate the move, but then cuts his hand while helping her move the couch. Kate is squeamish about the blood and unhelpful. An impatient driver, blocked by the moving van, yells at them and starts a fight with Luke. After Luke ends up with a fat lip, Kate calls Dave and another co-worker to help finish the move. Dave invites Kate to go out for drinks, and she tells Luke that she wants to go, rather than going to dinner. Luke is annoyed, and an argument ensues.

Returning home, Luke finds Jill has returned home early, and she is crying. She confesses to kissing Chris while at the cottage, tells Luke how guilty she feels about what happened, and that she really loves him and wants to marry him. Luke forgives Jill and lets her know that he still loves her and wants to marry her. The next day at work Kate and Luke awkwardly interact and then eventually end up sitting together at lunch. They each offer one another food items before they crack a smile, and drink a beer.

==Cast==
* Olivia Wilde as Kate
* Anna Kendrick as Jill
* Jake Johnson as Luke
* Ron Livingston as Chris
* Ti West as Dave
* Jason Sudeikis as Gene Dentler (credited as Gene Dentler playing "Himself")

==Production== brewing set-up as a birthday present.   According to Swanberg,  "I wanted to do something about Craft Beer   and set in the Craft Beer world but also I was inspired by Bob & Carol & Ted & Alice, a Paul Mazursky movie. Just to tell a complicated adult   that was funny, that managed to remain funny even though it was getting into serious, interesting things."  

The dialog was improvisation|improvised.  Instead of a script, the actors received outlines which covered the major plot points and were told each day what had to happen in that days scenes.  Relying heavily on improvisation is a key feature of the Mumblecore film movement. He said, "knowing that the structure was already pretty heavily in place, it was about letting the actors own their characters, and have a big say in the clothes that they wore, and in the interactions that they have with each other." He added: "The improv was used to mainly make the middle of the movie more complicated, and less predictable that a typical romantic comedy would be." Swanberg also stated "They need to be listening to each other and reacting honestly and I need to be paying really close attention because there’s not a script to fall back on. The goal of doing it that way is to keep everybody engaged and create situations that feel fun and natural".   

Filming took place in Chicago, Illinois in July 2012.  The film was shot in an actual brewery, called Revolution Brewing|Revolution, where one of the female brewers named Kate was the basis for Wildes character.  The actors actually drank real beer during the filming and even did real work for the brewing company. 

The film concludes with an open ending; "its hard for me, knowing how uncertain the world is, to put a certain, definite ending on a movie. I feel like Im hopefully hinting that theres a resolution without it being cemented down, or hammering you over the head with it."   
 Richard Swift, as well as Plants & Animals, Here We Go Magic, Phèdre, and other additional artists.

==Reception==
Drinking Buddies received positive reviews from critics. Rotten Tomatoes gives the film a score of 83%, based on 108 reviews, with an average rating of 6.9/10. The sites consensus reads, "Smart, funny, and powered by fine performances from Olivia Wilde and Jake Johnson, Drinking Buddies offers a bittersweet slice of observational comedy."  At Metacritic gives the films a rating of 71 out of 100, based on 32 critics, indicating "general favorable reviews". 

A. O. Scott of The New York Times remarked "Mr. Swanberg’s camera weaves through bodies at rest, at work and at the bar in no particular hurry, and his script captures the idioms of men and women who are equally inclined to waste words and to say very little. But the busy tedium of their lives is given shape and direction by the skill of the cast and by the precision of the director’s eye, ear and editing instincts." 

Upon the films UK release, Peter Bradshaw gave the film three stars out of five ( ), calling it "a lo-fi photography|lo-fi relationship drama that is interesting, if unevenly presented...an intriguing and distinctive story, soberly told"; according to Bradshaw, "Swanberg interestingly shows how booze loosens them up, lowers their inhibitions, and yet blurs their emotional reactions, in the process weirdly muting and endlessly deferring the sexual drama." 
Quentin Tarantino named it one of the top 10 movies of the year so far in October 2013. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 