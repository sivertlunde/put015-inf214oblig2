Virus (1980 film)
 
{{Infobox film
| name           = Virus
| image          = Fukkatsu-no-hi.jpg
| caption        = Japanese-language poster
| director       = Kinji Fukasaku
| writer         = Kinji Fukasaku Kōji Takada Gregory Knapp Sakyo Komatsu (novel)
| starring       = Masao Kusakari Sonny Chiba Glenn Ford Chuck Connors
| producer       = Haruki Kadokawa Akira Suzuki
| music          = Kentaro Haneda Janis Ian Teo Macero
| cinematography = Daisaku Kimura
| distributor    = Toho
| released       = Jun 26, 1980
| budget         = Japanese yen|¥3,351,000,000 (approx at 1980s rates) USDollar sign|$16,000,000
| runtime        = 156 min.
| country        = Japan English / Japanese
|}}
 1980 Apocalyptic science fiction novel written by Sakyo Komatsu. It starred Masao Kusakari, George Kennedy, Robert Vaughn, Chuck Connors, Olivia Hussey, Edward James Olmos, Ken Ogata, Glenn Ford and Sonny Chiba. The film is notable for being the most expensive Japanese film ever made at the time.

==Plot summary== East German scientist, Dr. Krause, and a group of Americans. It is revealed that MM88 is a deadly virus, created accidentally by an American geneticist, that amplifies the potency of any other virus or bacterium it comes into contact with. The Americans recover the MM88, which was stolen from a lab in the US the year before, but the virus is released, creating a pandemic initially known as the "Italian Flu".

Within seven months, virtually all the worlds population has died off. But the virus is inactive at temperatures below -10 degrees Celsius, and the polar winter has spared the 855 men and eight women stationed in Antarctica. The British nuclear submarine HMS Nereid joins the scientists after sinking a Soviet submarine whose infected crew attempt to make landfall near Palmer Station.

However, just as the group begins to repopulate their new home, it is revealed that an earthquake will activate the Automated Reaction System (ARS) and launch the United States nuclear arsenal.

The Soviets have their own version of the ARS that will fire off their weapons in return, including one targeted at Palmer Station. Yoshizumi and Major Carter embark aboard the Nereid on a mission to shut down the ARS, protected from MM88 by an experimental vaccine.

The submarine arrives at Washington, D.C. and Yoshizumi and Carter make a rush for the ARS command bunker. They reach the room too late and all but a few of the survivors (who left aboard an icebreaker at the same time as the Nereid) perish in the nuclear exchange. Over the course of years Yoshizumi walks back towards Antarctica. Upon reaching Tierra del Fuego in 1988,  he encounters the survivors from the icebreaker.

==Cast==
* Masao Kusakari - Yoshizumi
* Sonny Chiba- Dr. Yamauchi
* Isao Natsuyagi - Dr. Nakanishi
* Glenn Ford - President Richardson 
* Robert Vaughn - Senator Barkley 
* Stuart Gillard - Dr. Meyer
* George Touliatos - Colonel Rankin 
* Henry Silva - General Garland 
* George Kennedy - Admiral Conway
* Chris Wiggins - Dr. Borodinov
* Edward James Olmos - Captain Lopez
* Cec Linder - Dr. Latour
* Chuck Connors - Captain McCloud
* Bo Svenson - Major Carter 
* Olivia Hussey - Marit

==Background/production==
Haruki Kadokawa the producer of the film was the heir to Kadokawa Shoten, major publishing empire in Japan. After his father died in 1975 he decided to create a cinema branch of the company and began producing many films in the late 1970s including Inugamike no ichizoku (1976) and Ningen no shōmei (1977) a murder mystery with a worldwide release. In 1978 production on Fukkatsu no Hi started. Kadokawa wanted the film to be a big breakthrough in the international market so he tried to insure its success by casting some notable foreign stars and doing major international promotion (under the international title Virus). He also funded the largest budget of any Japanese film ever made at the time to solidify the movie as worthy of an international release. 
 CNS Simpson HMCS Okanagan Halifax (doubling for a riverside area in Washington D.C.), Tokyo, Toronto, and Machu Picchu.
 Lindblad Explorer, struck a reef while transporting a production unit to Antarctica for location shooting and almost sank. The passengers were rescued by Chilean naval vessels. The Lindblad Explorer was returned to service but was ultimately lost in Antarctic waters in 2007.

==Reception==
The film was a box office bomb despite its enormous budget. Although it may have had some special showings in the United States and elsewhere, it did not receive a general release. It was sold directly to pay television and edited down to a 108-minute version. This cut left out much of the love story between Yoshizumi and Marit, and nearly all the Japanese scenes including Yoshizumis entire trek back towards Antarctica. The cut left out nearly all character development for Yoshizumi as well. It also starts off with the meeting in East Germany between Doctor Krause and Rankins agents, while the Nereids arrival in Tokyo Bay was moved to much later in the film. Some versions even end with the nuclear explosions, leaving the movie with a quite pessimistic outcome as opposed to the Japanese versions optimistic ending. {{cite web|title=Virus (aka Day of Resurrection, Fukkatsu no hi, 1980)
|publisher=Götterdämmerung|author=Branislav L. Slantchev|year=2004|url=http://www.gotterdammerung.org/film/reviews/v/virus.html}} 

==DVDs==
In 2002, Kadokawa Shoten released the movie in a special limited-edition Region 2 DVD set, which contained the original 156-minute cut and the shorter 108-minute version, plus extra materials.  While the DVD in question does not have English subtitles, an English-subtitled version of the full cut was later released in 2006 as part of a compilation set called the Sonny Chiba Action Pack.  Previous copies of the film released in North America often featured the 108-minute cut, partly because of the multinational cast that had several American actors.

==See also==

* Outbreak (film)|Outbreak
* 12 Monkeys
* Contagion (film)|Contagion

==References==
 

==External links==
*     at the Japanese Movie Database
* 
*  (full length edit) and ( )

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 