Witless Protection
{{Infobox film
| name = Witless Protection
| image = Witless protection.jpg
| director = Charles Robert Carner
| producer = J.P. Williams Alan C. Blomquist
| screenplay  = Charles Robert Carner
| story        = Charles Robert Carner Alan C. Blomquist
| starring = Larry the Cable Guy Ivana Miličević Joe Mantegna Jenny McCarthy Lisa Lampanelli
| music = Eric Allaman
| cinematography = Michael Goi, ASC
| editing = Marc Leif
| studio   = Samwilla Shaler Entertainment Parallel Entertainment
| distributor = Lionsgate
| released =  
| runtime = 99 minutes
| country = United States
| language = English
| budget = $7,500,000
| gross = $4,161,625 (US and Canada) 
}} Sugar Grove, Glen Ellyn, Vernon Hills, Westmont and Yorkville, Illinois|Yorkville.  The film was released in theatres on February 22, 2008 and was released for DVD on June 10, 2008.

==Plot summary==
The story centers on a small-town sheriff who witnesses what he believes is a kidnapping and rushes to rescue a woman. The kidnappers turn out to be FBI agents assigned to protect her and deliver her to a big Enron-type corruption trial in Chicago but are later found to be on the take and are villains who are bent on killing her.

==Cast==
*Daniel Lawrence Whitney (Larry the Cable Guy) as Deputy Larry Stalder
*Jenny McCarthy as Connie
*Joe Mantegna as Dr. Rondog "Doc" Savage
*Yaphet Kotto as Ricardo Bodi (alias Alonzo Mosley)
*Ivana Miličević as Madeleine Richard Bull as Sheriff Smoot
*Eric Roberts as Wilford Duvall
*Peter Stormare as Arthur Grimsley
*J David Moeller as Elmer
*Sean Bridgers as Norm

==Reception==
This film received near unanimously negative reviews from critics. As of December 19, 2010, the review website Rotten Tomatoes reported that 3% of critics gave the film positive reviews, based on 29 reviews.  Metacritic reported the film had an average score of 17 out of 100, based on 6 reviews. 

Witless Protection was nominated for three   (who lost to Mike Myers for The Love Guru), Worst Supporting Actress for Jenny McCarthy (who lost to Paris Hilton for Repo! The Genetic Opera) and Worst Screen Couple for Larry the Cable Guy and Jenny McCarthy (they lost to Paris Hilton and either Christine Lakin or Joel David Moore for The Hottie and the Nottie).

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 