The Atlantis Interceptors
{{Infobox film 
| name           = The Atlantis Interceptors
| image          = PredatoridiAtlantide.jpg
| caption        = 
| director       = Ruggero Deodato
| producer       = Maurizio Amati
| writer         = Tito Carpi Vincenzo Mannino Christopher Connelly Gioia Scola
| music          = Guido De Angelis Maurizio De Angelis
| cinematography = Roberto DEttorre Piazzoli Sergio DOffizi
| editing        = Vincenzo Tomassi
| distributor    = Regal Films
| released       = November 25, 1983 (Italy)
| runtime        = 92 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 Christopher Connelly and directed by Ruggero Deodato.

The film was heavily influenced by George Millers 1979 vision of outback hell, Mad Max, and its sequel, Mad Max 2: The Road Warrior, and John Carpenters dystopian view of a future New York, Escape From New York (1980) and was only one of seemingly countless imitations, such as Bronx Warriors, Rome, The New Gladiators, and The New Barbarians. Very much action-oriented, the films often featured heavy doses of violence and brutality.

Two Vietnam-vets and several scientists face an extraordinary battle for survival against descendants of Atlantis original race, when the Lost Continent emerges in the Caribbean following radioactive leakage from a sunken Russian nuclear submarine. Calling themselves "Interceptors", the murderous Atlanteans set about reclaiming the world by killing everyone and destroying everything in sight. It is up to Mike, Washington and Dr. Cathy Rollins to uncover the secret behind their existence and use it against them in order to stop the interceptors apocalyptic rampage.

The Atlantis Interceptors, aka, Raiders of Atlantis, was released in 1982/83 in the UK on videotape, but is still unavailable on DVD as a mainstream release. The version available in a recent Grindhouse collection in the US is poorly transferred from VHS and in full screen.

==Cast==
{| class="wikitable"
! Actor
! Role
|-  Christopher Connelly || Mike Ross
|-
| Gioia Scola || Dr. Cathy Rollins 
|- Tony King || Mohammed/Washington
|-
| Stefano Mingardo || Klaus Nemnez
|-
| Ivan Rassimov || Bill Cook
|-
| Giancarlo Prati || Frank 
|-
| Bruce Baron || Crystal Skull
|- George Hilton || Professor Peter Saunders
|-
| Mike Monty || George 
|-
| Michele Soavi || James
|-
| Adriana Giuffrè ||
|-
| Maurizio Fardo || Larry Stoddard 
|-
| Lewis E. Ciannelli || Oil Rig Commander 
|-
| John Vasallo || Manuel
|-
| James Demby ||
|-
| Gudrun Schmeissner ||
|-
| Benedetta Fantoli ||
|-
| Gianni Franco ||
|-
| Ruggero Deodato || Oil Rig Assistant 
|-
| Rocco Lerro || Raider in Helicopter 
|-
| Benito Pacifico || Thug at Mansion 
|-
| Angelo Ragusa || Motorcycle Raider w/headband
|}

==References==
* Wingrove, David. Science Fiction Film Source Book (Longman Group Limited, 1985)
*  

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 