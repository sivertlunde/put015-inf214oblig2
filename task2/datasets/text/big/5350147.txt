It Seemed Like a Good Idea at the Time
 
{{Infobox Film |
  name=It Seemed Like a Good Idea at the Time |
  image= It Seemed Like a Good Idea at the Time.jpg|
  caption = The movie cover for It Seemed Like a Good Idea at the Time.| John Trent |
  starring=Anthony Newley   Stefanie Powers   John Candy | John Trent | William McCauley | 	 
  distributor=Gemstone Entertainment |
  released=September 11, 1975 |
  runtime=90 min. |
  language=English |
  movie_series=|
  awards=| David Perlmutter |
  budget= |}}

It Seemed Like a Good Idea at the Time is a Canadian comedy film that was released in 1975.  It was one of John Candys earlier films before he came to the attention of the mainstream. He plays the minor role of an investigating police officer. However, for marketing/publicity reasons, Candy appears on the cover of the DVD release.

==Plot==
Sweeney (Anthony Newley) is a playwright on a career decline. He spends much of his time wheedling money and beer out of his artistic friend Moriarty (Isaac Hayes). One of his few highlights is weekly sex with his ex-wife Georgina (Stefanie Powers). She is remarried to a rich but vile construction developer (Henry Ramer), but Sweeney and Georgina are still in love. 

Sweeneys escapades end with a fake kidnapping scam. This gets the attention of two inept police officers, played by John Candy and Lawrence Dane. These two end up dressed as garbage men in the chase scene finale; everything winds up with a happy ending.

== External links ==
*  
*  

 
 
 
 
 



 