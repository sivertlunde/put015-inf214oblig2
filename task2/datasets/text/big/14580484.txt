Dynamite (film)
{{Infobox film
| name           = Dynamite
| image          = Dynamite_1929_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille
| screenplay     = {{Plainlist|
* Jeanie MacPherson
* John Howard Lawson
* Gladys Unger
}}
| starring       = {{Plainlist|
* Conrad Nagel
* Kay Johnson
* Charles Bickford
* Julia Faye
}}
| music          = 
| cinematography = J. Peverell Marley
| editing        = Anne Bauchens
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 129 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Dynamite is a 1929 American drama film produced and directed by Cecil B. DeMille and starring Conrad Nagel, Kay Johnson, Charles Bickford, and Julia Faye.    Written by Jeanie MacPherson, John Howard Lawson, and Gladys Unger, the film is about a convicted murderer scheduled to be executed, whom a socialite marries simply to satisfy a condition of her grandfathers will. Mitchell Leisen was nominated for the Academy Award for Best Art Direction.   

==Plot==
Coal miner Hagon Derk (Charles Bickford) is sentenced to hang for murder. His only concern is for his young sister Katie (Muriel McCormac), who will be left all alone. Frivolous socialite Cynthia Crothers (Kay Johnson) has her own troubles. By the terms of her grandfathers will, if she is not married by her twenty-third birthday (only a month away), she will not inherit his millions and will be left penniless. She is "engaged" to Roger Towne (Conrad Nagel), but he is married to Marcia (Julia Faye). Marcia has her own lover, Marco (Joel McCrea), and is willing to grant Roger a divorce ... for the right price. The two women haggle behind Rogers back and settle on $100,000.
 confesses before dying, and Hagon is released.

Hagon goes to see his stunned wife. When her friends show up to party the night away, he eavesdrops on a private conversation and learns of the bargain between Cynthia and Marcia. He shows Roger the $25,000 check in Marcias possession: the down payment. Roger tears up the check and tells Cynthia they are through if she pays for him. Hagon leaves in disgust after flinging his $10,000 at her.

When Cynthia is informed that she must actually be living with her husband on her birthday, she drives to his mining town. He refuses to go back to her palatial apartment, so she persuades him to let her stay with him. He agrees on condition that she cook and clean, just like a real wife, and locks up her fancy car in his tool shed. Her first attempt at preparing a meal is a dismal failure. Katie kindly helps out and keeps it a secret from Hagon, but Cynthia confesses on her own. Hagon tells her it is the first honest thing he has seen her do.

The next day, while shopping at the local store, Cynthia buys a gift for a young boy. His mother objects, but the child runs away with his present and is hurt in a traffic accident. The doctor says that only a brain specialist in the city can save him, but the boy only has hours to live. Cynthia breaks into the tool shed, speeds away in her car and returns with the specialist. The child is saved.

Hagon returns from work to find the door of his tool shed demolished and learns that Cynthia withdrew $2000 from the bank (to pay the specialist). He assumes that she got tired of his way of life and went to see Roger. When Hagon demands an explanation, Cynthia is too disheartened to reply. She telephones Roger to come for her. However, the childs mother tells Hagon what Cynthia has done.

When Roger shows up, he insists on seeing Hagon before leaving. They go down into the bowels of the mine to find him. A cave-in traps the trio with only fifteen minutes worth of air. Hagon finally confesses he loves Cynthia. Then he realizes there is a way out. He quickly packs a stick of dynamite into a wall; there is another chamber on the other side with enough air to sustain them until they can be rescued. However, without a fuse cap, someone will have to strike the dynamite with a sledgehammer to set it off. After arguing, the two men toss a coin for the privilege. Roger "wins", but Hagon wrestles the sledgehammer away from him. After Cynthia whispers something to Roger, he tells Hagon that Cynthia has something to say to him. When Hagon goes to Cynthia, she confesses she loves him. With the two safely out of the way, Roger sets off the dynamite and is blown to pieces.

==Cast==
 
 
* Conrad Nagel as Roger Towne
* Kay Johnson as Cynthia Crothers
* Charles Bickford as Hagon Derk
* Julia Faye as Marcia Towne
* Muriel McCormac as Katie Derk
* Joel McCrea as Marco
* Robert Edeson as First Wise Fool
* William Holden as Second Wise Fool
* Henry Stockbridge as Third Wise Fool
* Leslie Fenton as Young "Vulture" Firing Gun
* Barton Hepburn as Young "Vulture" Confessing Crime
* Ernest Hilliard as Good Mixer
* June Nash as Good Mixer
* Judith Barrett as Good Mixer
* Neely Edwards as Good Mixer
 
* Marjorie Zier as Good Mixer
* Rita La Roy as Good Mixer
* Tyler Brooke as The Life of the Party
* Clarence Burton as Police Officer Jim Farley as Death Row Police Officer
* Robert T. Haines as The Judge
* Douglas Scott as Bobby Smith
* Jane Keckley as Bobbys Mother
* Blanche Craig as Neighbor (Mrs. Johnson)
* Mary Gordon as Neighbor at Store
* Ynez Seabury as Neighbor (Mrs. Johnsons Daughter)
* Scott Kolk as Radio Announcer
* Fred Walton as Doctor Rawlins
* Wade Boteler as Mine Foreman (uncredited)
* Randolph Scott as Coal Miner (uncredited)    

==Production== Guinn Big Boy Williams, Dean Jagger and Randolph Scott; Carmelita Geraghty, Katherine Crawford, Ann Cornwall, Merna Kennedy, Leila Hyams, Dorothy Burgess and Sally Blane. His final selections, Charles Bickford and Kay Johnson, were primarily known for their stage work.  Leisen tried to interest DeMille in up-and-coming Carole Lombard for Johnsons role; http://www.tcm.com/thismonth/article.jsp?cid=72490&mainArticleId=72479  allegedly, she can be glimpsed in the surviving versions of the film.

Filming of Dynamite began on January 29, 1929, and lasted until April 30. Scenes for the silent version were shot beginning on May 28 and ending on June 5.  Charles Bickford would later describe the script as "a mess of corn with terrible dialogue." 

Dorothy Parker, who was living in Los Angeles at the time, was commandeered to pen the lyrics for an original song for Dynamite. Her third try, titled "How Am I To Know" and set to music by Jack King, was accepted and featured in the films prison sequence. 

==Reception==
The New York Times reviewer Mordaunt Hall had mixed feelings about DeMilles first talkie, calling it "an astonishing mixture, with artificiality vying with realism and comedy hanging on the heels of grim melodrama."    "Even in the work of the performers, there are moments when they are human beings and then, at times, they become nothing more than Mr. De Milles puppets", "behaving strangely and conversing in movie epigrams".  Nonetheless, Hall approved of the efforts of Johnson ("an accomplished actress") and Bickford ("a splendid performance"), though he could not say the same of Nagel ("does not act up to his usual standard"). 

==References==
 

==External links==
*  
*   at Tollymovies
*  
*  

 

 
 
 
 
 
 
 
 
 
 