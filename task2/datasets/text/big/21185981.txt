Arupusu monogatari Yasei
 
{{Infobox film
| name           = Arupusu monogatari Yasei
| image          =
| caption        = Japanese movie poster
| director       = Tsutomu Sawamura
| producer       = Oizumi Eiga (太泉映画)
| writer         = Kaneto Shindō
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = 1950 
| runtime        = ? min.
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
| preceded_by    =
| followed_by    =
| imdb_id        =  
|
}} Japanese film directed by Tsutomu Sawamura.

==Cast==
* Setsuko Hara
* Hajime Izu (伊豆肇)
* Kuniko Miyake (三宅邦子)
* Sakae Ozawa (小沢栄)

==See also==
* There is a different film with similar name:   (アルプス物語　わたしのアンネット, Arupusu Monogatari Watashi no Annetto), 1983 anime TV series.

==References==
 

==External links==

 
 
 
 
 


 