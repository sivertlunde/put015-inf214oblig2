Alive in Joburg
{{Infobox film
| name           = Alive in Joburg
| image          = Alive in Joburg.jpg
| director       = Neill Blomkamp
| producer       = Sharlto Copley Simon Hansen
| executive producer =  Carlo Trulli
| writer         = Neill Blomkamp
| narrator       = 
| starring       = Sharlto Copley Jason Cope Dawie Ackermann
| music          = Clinton Shorter Drazen Bosnjak
| cinematography = Trevor Cawood Ozan Biron
| editing        = 
| studio         = Spy Films
| distributor    = Spy Films
| released       =  
| runtime        = 5 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Alive in Joburg is a 2006 science fiction short film directed by Neill Blomkamp, produced by Simon Hansen, Sharlto Copley and Carlo Trulli as Executive Producer of Spy Films in Canada.  It runs approximately six minutes long and was filmed in Johannesburg, South Africa with soundtrack featuring composer sound designer Drazen Bosnjaks "Harmonic Code". The film explores themes of apartheid and is noted for its visual effects as well as its documentary film|documentary-style imagery. Blomkamps 2009 feature film District 9, starring Copley, expands themes and elements from this short film.

== Plot and themes == extraterrestrial refugees, whose large spaceships (estimated to be nearly one kilometre in length) can be seen hovering above the city. When the visitors arrived, the human population was enamored with, among other aspects, the aliens advanced "bio-suits", and supposedly welcomed them with open arms. However, later, the aliens began moving into other areas of the city, committing crimes in order to survive, and frequently clashing with the police. Playing as a documentary, the film continues, complete with interviews and footage taken from handheld cameras, highlighting the growing tension between the civilian population and the visitors, especially once the ships began to steal electricity and other resources from the city.
 black population, white populations.

All of the interview statements which do not explicitly mention extraterrestrials were taken from authentic interviews with many South Africans who had been asked their opinions of Zimbabwean refugees. 

== The aliens ==
The alien species in Alive in Joburg are never named, speak in an undefined language, and are frequently referred to simply as "them" or "the aliens".  One citizen referred to them as "the poleepkwa".  In their biosuits, they resembled bipedal, humanoid robots.  Outside of their suits, their most obvious non-human features are a lack of hair and ears, and protruding tentacles where a humans mouth would be. In the film, the area where one would expect eyes to be is pixelated, though in a later scene an alien with unpixelated eyes is shown.

One scene early in the film, shown as television news footage, shows an alien wearing a mecha-suit fending off an attack by two police officers by throwing vehicles at them.

== Adaptation == Best Picture.

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 