City of Joy (film)
{{Infobox film
  | name = City of Joy
  | image =City of Joy (movie poster).jpg
  | caption = Movie Poster
  | director = Roland Joffé
  | producer = Jake Eberts Roland Joffé
  | writer = Mark Medoff
  | starring = Patrick Swayze Pauline Collins Om Puri Shabana Azmi Art Malik
  | music = Ennio Morricone
  | cinematography = 
  | studio  = Lightmotive Allied Filmmakers
  | editing = Gerry Hambling |
  | distributor = TriStar Pictures (USA) Majestic Films International (intl sales) Warner Bros. (some European countries)
  | released = April 15, 1992
  | country = France United Kingdom
  | runtime = 132 minutes
  | language = English
  | budget = $27 million
  | gross = $14,683,921
  | preceded_by =
  | followed_by = 
  }} novel of the same name by Dominique Lapierre, which looks at poverty in then-modern India, specifically life in the slums. The film stars Patrick Swayze, Om Puri and Shabana Azmi.

==Plot==
Hasari Pal (Om Puri) is a rural farmer who moves to Calcutta with his wife (Shabana Azmi)  and three children in search of a better life. The Pals do not get off to a very good start: They are cheated out of their rent money and thrown out on the streets, and its difficult for Hasari to find a job to support them. But the determined family refuses to give up and eventually finds its place in the poverty-stricken city.

Meanwhile, on the other end of Calcutta, Max Lowe (Patrick Swayze), a doctor disillusioned by an easy job in a Houston hospital, has arrived in search of spiritual enlightenment after the loss of a patient there. However, he encounters misfortune as soon as he arrives. After being tricked by a young prostitute, he is roughed up by thugs and left bleeding in the street without his documents and valuable possessions.

Hasari comes to Maxs aid and takes the injured doctor to the "City of Joy," a slum area populated with lepers and poor people that becomes the Pals new home and the Americans home away from home. Max spends a lot of time in the neighborhood, but he does not want to become too involved with the residents because he is afraid of becoming emotionally attached to them. He soon, however, is coaxed into helping his new-found friends by a strong-willed Irish woman (Pauline Collins), who runs the local clinic.

Eventually, Max begins to fit in with his fellow slum-dwellers and become more optimistic. There are many around him whose lives are much worse, but they look on each day with a hope that gives new strength to the depressed doctor.

==Cast==
* Patrick Swayze as Max Lowe
* Om Puri as  Hazari Pal
* Pauline Collins as Joan Bethel
* Vishal Slathia as Joey Barton
* Shabana Azmi as  Kamla H. Pal
* Ayesha Dharker as  Amrita H. Pal
* Santu Chowdhury as Shambu H. Pal
* Imran Badsah Khan as Manooj H. Pal
* Shyamanand Jalan as Mr. Ghatak, the godfather
* Anjan Dutt as Dr Sunil
* Art Malik as Ashok Ghatak
* Nabil Shaban as Anouar
* Debatosh Ghosh as Ram Chande
* Sunita Sengupta as Purmina
* Loveleen Mishra as Shanta
* Pavan Malhotra as Ashish

==Reception== The Killing Fields), the film was not a box office success, even on its modest budget;  According to the Internet Movie Database and Box Office Mojo, the film grossed $14.7 million in the United States.   Critically, the film received mixed reviews, with a 57% rotten rating on review aggregate Rotten Tomatoes. 

==See also==

* White savior narrative in film

== References ==
 

== External links ==
*  
*   at Rotten Tomatoes
*   at Box Office Mojo

 

 
 
 
 
 
 
 
 
 
 
 
 