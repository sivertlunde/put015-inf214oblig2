Lease of Life
{{Infobox film
| name           = Lease of Life
| image          = Leaseoflifeposter.jpg
| caption        = UK release poster
| director       = Charles Frend
| producer       = Jack Rix
| writer         = Eric Ambler (from novel by Pat Jenkins)
| starring       = Robert Donat Kay Walsh Adrienne Corri Denholm Elliott
| music          = Alan Rawsthorne
| cinematography = Douglas Slocombe
| editing        = Peter Tanner
| distributor    = Ealing Studios
| released       =  
| runtime        = 94 min.
| country        = United Kingdom
| language       = English
| budget         =
}} 1955 British Academy Film Awards.   In common with a number of other Ealing films of the era, Lease of Life focuses on a specific English milieu &ndash; in this case a Yorkshire village and its nearby cathedral city &ndash; and examines the nuances, quirks and foibles of its day-to-day life.  The film is unique in the Ealing canon in having religion as its dominant theme.

==Plot==
William Thorne (Robert Donat) is the vicar of the village of Hinton St. John, living with wife Vera (Kay Walsh) and daughter Susan (Adrienne Corri), an exceptionally gifted pianist.  Although the focus of the local community, the Thornes live a life of having to struggle and scrimp to make ends meet financially.  Vera is a typical clergy wife, having to sublimate her own needs and desires to the exigencies of her husbands career, as a result tending to live life vicariously through her daughter, whose musical gifts she is determined must not be wasted.

On discovering that he has less than a year to live, Thorne reevaluates his own life and his parishioners and he finds himself happier than before, as he now feels able to speak completely honestly about his beliefs and does his best to demonstrate to his parishioners that religion is not a matter of unthinking adherence to a fixed set of rules, but of freedom to act according to ones conscience.  However some of his pronouncements are willfully misunderstood and deemed provocative and controversial.  There also remains the worry about how to secure the necessary funds to pay for Susans tuition at a music college, and fate happens to put temptation in the way.

==Cast==
* Robert Donat as Rev. William Thorne
* Kay Walsh as Vera Thorne
* Adrienne Corri as Susan Thorne
* Denholm Elliott as Martin Blake
* Walter Fitzgerald as The Dean
* Reginald Beckwith as Foley
* Cyril Raymond as Headmaster
* Vida Hope as Mrs. Sproatley
* Beckett Bould as Mr. Sproatley
* Jean Anderson as Miss Calthorp
* Russell Waters as Russell Alan Webb as Dr. Pembury
* Richard Wattis as Solicitor
* Richard Leech as Carter
* Frederick Piper as Jeweller Mark Daly as Spooner Frank Atkinson as Verger

==Location filming== Lund (Hinton St. John) in the East Riding of Yorkshire.

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 