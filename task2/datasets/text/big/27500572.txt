Mad Youth
{{Infobox film
| name           = Mad Youth
| image_size     =
| image	=	Mad Youth FilmPoster.jpeg
| caption        = original film poster
| director       = Melville Shyer
| producer       = Willis Kent (uncredited)
| writer         = Willis Kent (uncredited)
| narrator       =
| starring       = Betty Compson Mary Ainslee
| music          =
| cinematography = Harvey Gould Marcel Le Picard
| editing        = Robert Jahns
| distributor    = Willis Kent Productions
| released       =
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Mad Youth is a 1940 American film directed by Melville Shyer. The film is also known as Girls of the Underworld (American reissue title).

== Plot summary ==
Divorceé Marian Morgan (Compson) hires a male escort Count DeHoven (Willy Castello), who has an affair with her teenage daughter, Mary (Ainslee).

After her wild parties, with jitterbugs and strip poker, Helens grandmother (Fealy), locks her out of the house, and she runs away to marry a man she met through correspondence. 

Marian gets fed up with her daughter and her friends. She laments that she never got to be young, and free, like they are; and, tells her daughter to go live with her Father, for a while. Mary doesnt get along with his new wife; so, she decides to go visit Helen, after getting a letter, from her.

The "Count" is furious with Marian, for letting her daughter traipse across the country, without knowing who she is with; and, warns her that mail-order marriage scams can be one of the worst traps there is. Together they track down an address, and he hurries to try to save Mary and Helen (Atkinson).

The girls have been imprisoned in a prostitution and white slavery ring, in a big old mansion. It was all a ruse; and, Helen was beaten until she gave in, and wrote to send for her friend. 

Time is running out. It looks like theres going to be a fight, if the “Count” is going to save Mary, and marry her, before she disappears into the underworld, forever.  

== Cast ==
*Mary Ainslee as Marian Morgan
*Betty Compson as Lucy Morgan
*Willy Castello as Count DeHoven
*Betty Atkinson as Helen Johnson Tommy Wonder as Harry
*Lorelei Readoux as Beth
*Margaret Fealy as Helens Grandmother Donald Kerr as Taxi Driver
*Ray Hirsch as Jitter Bug
*Patti Lacey as Jitter Bug
*Eugene Taylor as Jitter Bug
*Aileen Morris as Jitter Bug
*Maxine Taylor as Jitter Bug
*Pearl Tolson as Jitter Bug

== Soundtrack ==
*"Id Rather Be a Bum on Broadway Than an Angel in the Sky"

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 