Outlaws of the Desert
{{Infobox film
| name           = Outlaws of the Desert
| image          = Outlaws of the Desert poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Howard Bretherton
| producer       = Harry Sherman 
| screenplay     = J. Benton Cheney Bernard McConville William Boyd Brad King Duncan Renaldo Jean Phillips Forrest Stanley Nina Guilbert
| music          = John Leipold
| cinematography = Russell Harlan
| editing        = Carroll Lewis 
| studio         = Harry Sherman Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film William Boyd, Brad King, Duncan Renaldo, Jean Phillips, Forrest Stanley and Nina Guilbert. The film was released on November 1, 1941, by Paramount Pictures.  

==Plot==
 

== Cast ==		 William Boyd as Hopalong Cassidy
*Andy Clyde as California Carlson Brad King as Johnny Nelson
*Duncan Renaldo as Sheik Suleiman
*Jean Phillips as Susan Grant
*Forrest Stanley as Charles Grant
*Nina Guilbert as Mrs. Jane Grant
*Luli Deste as Marie Karitza
*Alberto Morin as Nicki Karitza 
*George J. Lewis as Yussuf
*Jean Del Val as Faran El Kader
*Jamiel Hasson as Ali
*Mickey Eissa as Salim 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 