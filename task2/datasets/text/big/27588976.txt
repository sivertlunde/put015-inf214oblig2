I Am Number Four (film)
{{Infobox film
| name           = I Am Number Four 
| image          = I Am Number Four Poster.jpg
| caption        = Theatrical release poster
| director       = D. J. Caruso
| producer       = Michael Bay
| writer         = 
| screenplay     = Alfred Gough Miles Millar Marti Noxon
| story          = 
| based on       =  
| starring       =  Alex Pettyfer Timothy Olyphant Teresa Palmer Dianna Agron Callan McAuliffe Kevin Durand
| music          = Trevor Rabin
| cinematography = Guillermo Navarro
| editing        = Jim Page Vince Filippone DreamWorks Pictures Bay Films Walt Disney Studios Motion Pictures 
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $50 million   
| gross          = $149.9 million 
}} teen action action science fiction film thriller film, directed by D. J. Caruso, starring Alex Pettyfer, Timothy Olyphant, Teresa Palmer, Dianna Agron and Callan McAuliffe. The screenplay by Alfred Gough, Miles Millar and Marti Noxon is based on the novel I Am Number Four by Pittacus Lore.

Produced by Michael Bay, I Am Number Four was the first film production from DreamWorks Pictures to be distributed by Touchstone Pictures, as part of the studios distribution deal with Walt Disney Studios Motion Pictures.  The Hollywood Reporter estimated the budget to be between $50 and $60 million. The film was released in both conventional and IMAX theatres on February 18, 2011. 

==Plot==
 
 
John Smith (Alex Pettyfer) is an alien from the planet Lorien. He was sent to Earth as a child with eight others to escape the invading Mogadorians, who destroyed Lorien. Here, John is protected by a Cepan, or guardian, Henri (Timothy Olyphant). Together, they live in a beach-side bungalow in Florida.

The Mogadorians, led by the Commander (Kevin Durand), learn about the nine children and come to Earth to find them. The Loric Garde can only be killed in sequence; Number One through Number Nine. Three of them have already been killed. John becomes aware that he is next while swimming in the ocean with a girl. His leg begins to glow with a sharp pain and he sees a vision underwater of Number Three, warning him of the coming danger. The girl and the other people on the beach, who recorded the event happening in the ocean, run from John. When the video and pictures surface on the internet, Henri deletes them all, gathers up his and Johns belongings, and decides to move to an old farm in Paradise, Ohio. Anything not needed is thrown into a fire and the rest is packed into the back of their car, but not before a small lizard is able to crawl in along with their belongings.

The day John and Henri arrive in Paradise is a rainy day and they begin settling in to their new home. As night falls, the lizard that had made its way in to Henris truck crawls out and in to a bush, where it transforms in to a dog. That night, Henri and John hear a loud noise outside and, upon investigation, find the dog, who is then brought inside by John. He decides to name the dog Bernie Kosar (after Cleveland Browns quarterback Bernie Kosar). The following morning, due to Johns insistence, Henri allows John to go to the local high school instead of being home schooled.

His first day there, John goes to the office to receive his class schedule. While waiting, he sees a girl being scolded by a teacher in the principals office. She had taken pictures of the teacher picking his nose and posted them online. After John is given his schedule and the girl is let off by the principal, she is asked by the secretary to guide John to his locker and first class. In this time, John falls for the girl, who is an amateur photographer named Sarah Hart (Dianna Agron). As Sarah walks away, a guy walks up to John with a couple of his friends and introduces himself as Mark James (Jake Abel). As Mark walks away, John watches him slam another boy in to the lockers and throws the boys skateboard away. John retrieves and gives back the board. Later that day at lunch, John befriends conspiracy theorist Sam Goode (Callan McAuliffe), who is the boy who was previously pushed in to the lockers, when one of Marks friends throws a football at Sams head, knocking him to the ground. John helps Sam up and throws the ball back with enough force to knock Marks friend to the ground. After this, John notices Sarah taking an interest in him, as shes taking pictures of him from a distance.

That night at home, John discovers a website run by Sarah. It contains pictures of many of the people of Paradise, including a section dedicated to him, which he finds just moments before it is erased by Henri. At the same time back in Florida, the Mogadorians discover the burnt wreck of Henri and Johns old home and determine that John was not killed in the fire.

The next day, while John is being bothered by Mark during a film in one of his classes, Johns hands begin to glow and burn brightly, similarly to the glow on his leg in the ocean, and he sweats profusely. He runs from the classroom and in to a janitorial closet, where he soaks his hands in water to calm the burning. Henri, alerted by a Loric box that hes kept, comes to John and explains that the glowing is the awakening of Johns "legacies". As these powers grow, John will become much more powerful but, in order to maintain a low profile, Henri forbids John from using these powers. John disobeys, sneaks out of his house, and goes to a nearby forest where he finds he is able to throw  large rocks at supersonic speeds, run faster than he ever could before, and survive long falls with ease.

Later that night, John goes out in to the town to walk around for a bit and runs in to Sarah. He confronts her about the pictures that he found on her website and she admits to being an avid photographer. As theyre walking together, they see Sam across the street being yelled at as he walks towards a car. Sarah reveals that its actually his step-father. They finish walking the rest of the way to Sarahs house, where she invites him in for dinner with her family. Her parents, though a bit nosy, seem to like John and suggest that he and Sarah go to the carnival together. After dinner, John and Sarah head up to her room. She shows John a small bit of her collection of her favorite photos that shes taken. John finds a scrapbook of personal photos that Sarahs taken of herself along with things shes written but she takes it back before he can read too much of it. Bernie Kosar shows up outside of Sarahs house, barking loudly, and he gets up to leave. Sarah gives him a camera of her own for him to keep and they nearly kiss but John leaves quickly because of Bernie Kosars constant barking.

The following day at school, Sam asks about the lights in class but John passes it off as a prank with some flashlights. As Sam is warning John to stay away from Sarah, their lockers explode with pink paint placed by Mark. Johns hands begin to glow again but hes able to hide it by clenching his fists, as he narrowly avoids getting into a fight with Mark. While John and Sam are cleaning up in the bathroom, Sam tells John about how he and his father used to go looking for aliens, though his father suddenly disappeared one day.

During the Spring Fair, Sarah explains to John that she and Mark used to date. She talked about how she had "refused to be his personal cheerleader" and instead wanted to be a photographer. Mark had told her that she was being snobby and convinced the entire school that she was crazy. John and Sarah go on a haunted wagon ride through the woods. When they get off, Marks friends tackle John and begin to beat him while one brings Sarah to Mark. John, however, he uses his legacies to fend them off and rescue Sarah. Sam witnesses Johns use of his legacies and John reveals his true origins to Sam. The next day, Marks father, the local sheriff, interrogates Henri on Johns whereabouts when his son and his friends were attacked, where John says he wasnt paying any attention to Mark, since he was with a girl.

Henri tells John that too many people are suspicious of them, so they have to leave. John refuses because he doesnt want to leave Sarah. Meanwhile, the Mogadorians continue searching for John, while being trailed by another of the Garde, Number Six (Teresa Palmer) who is also trying to locate Number Four. Number Sixs guardian was killed, and she realizes that the remaining six of the Loric Garde will have to team up and fight against the Mogadorians. She knows Number Three is dead and that Number Four is being hunted.

The Mogadorians eventually locate John and manipulate two conspiracy theorists into capturing Henri. When John and Sam go to rescue him, they are attacked, but manage to fend the Mogadorians off. Just before escaping, John and Sam grab Henris knife and a Loric artifact; a blue rock that acts as the first half of a tracking device that locates other Loric children. Knowing who Sam is, Henri reveals that his father was an ally that was helping them. As John, Henri, and Sam begin to drive away in Sams truck, Henri is stabbed through the chest while trying to protect John from a Mogadorian who jumped on the hood of the truck. John throws the Mogadorian from the truck with his telekinesis and they drive away. Henri later dies in Johns arms after telling him to find the remaining children, as their combined powers would be enough to stop the Mogadorians. Sam reveals to John that he has another rock, very similar to the one found with the conspiracy theorists, that his father had found while searching for aliens in Mexico.

While Sam searches for it in his house, John tries to say goodbye to Sarah at a party. Mark sees John and calls his father, who corners John and Sarah on the roof of the house that the party is being thrown at. As Sarah stands up, she slips from the roof. John saves Sarah from a fall with his telekinesis, revealing his powers in the process, and they escape to their high school.

Meanwhile, the Commander arrives in Paradise in a convoy of trucks. He confronts Mark and his father. After injuring the sheriff, the Commander forces Mark to lead him to where John is hiding. Mark takes him to the school, which he knows is Sarahs hideout.
 Chimera that can shapeshift and was sent by Johns biological parents to protect him. John and Number Six fight the Mogadorians using their powers; Number Six uses her invisibility powers and John blocks energy attacks with telekinesis. Meanwhile, Bernie Kosar, now in his true chimera form, fights one of the Mogadorians monsters in the schools showers. While hes able to defeat the monster, he appears to be mortally wounded, as he lies down, bloodied and panting, in the water. Eventually, having ended up on the schools football stadium, John and Number Six defeat all of the Mogadorians, including the Commander, who dies in a large explosion triggered by John igniting the Commanders bandolier of ammunition. John survives this explosion when Number Six stands between him in the blast, revealing her ability to resist fire.

The following day, Number Six unites Johns and Sams blue rocks and discover the location of the other four surviving Garde. John allows Sam to come with them with the hope of one day finding Sams father. They set off to find the others so they can all protect Earth from the Mogadorians. They leave behind Sarah and a repentant Mark, who lies to his father about Johns whereabouts and returns the box left to John by his dad that was in police evidence. Before leaving, John promises to return to Paradise one day and kisses Sarah one last time. Just before getting in to Sams truck, Bernie Kosar, with a visibly injured paw, limps up to John. Besides his paw, he seems to be alright.

John, Sam, and Bernie Kosar leave together in Sams truck, being led by Number Six on her motorcycle to where ever the next of the Loric Garde may be.

==Cast==
  John Smith/Number 4/Daniel Jones
* Timothy Olyphant as Henri Number Six Sarah Hart Sam Goode
* Kevin Durand as Mogadorian Commander Mark James
* Jeff Hochendoner as Sheriff James
* Greg Townley as Number Three
* Reuben Langdon as Threes Guardian
* Judith Hoag as Sarahs Mom Brian Howe as Frank Charles Carroll as Sams Stepdad
* Ken Beck as Jackson
* Tucker Albrizzi as Tuck
* Emily Wickersham as Nicole
* Patrick Sebes as Kevin
* Andy Owen as Bret
* L. Derek Leonidoff as Mr. Berhman
* Garrett M. Brown as Mr. Simms
* Megan Follows as Supermarket Cashier
* Sabrina de Matteo as Physics Teacher
* Cooper Thornton as Sarahs Dad
* Jack Walz as Sarahs Brother
* Bill Laing as Demented Farmer
* Beau Mirchoff as Drew
* Cody Johns as Kern
*Monica Pimentel as lizard
 

==Production==

===Development===
The movie was produced by DreamWorks and Reliance Entertainment.Film producer and director Michael Bay brought the manuscript of the teen book I Am Number Four to Stacey Snider and Steven Spielberg at DreamWorks. A bidding war developed for the film rights between DreamWorks and  J.J. Abrams, with DreamWorks winning the rights in June 2009, with the intention of having Bay produce and possibly direct the project.        Twilight saga film franchise, with at least six more installments planned by the books publisher. 
  Presidents Day weekend.   
 David Valdes executive produced the film.  Steven Spielberg contributed to the films characters, but did  not take a credit on the film.  It was the first DreamWorks film to be released by Disneys Touchstone Pictures label, as part of the 30-picture distribution deal between DreamWorks and Walt Disney Studios Motion Pictures.  The film was also the first release for DreamWorks after the studios financial restructuring in 2008. 

===Casting===
In March 2010, Alex Pettyfer was in talks to play the title character in the film, Number Four.  It was later confirmed that the 21-year-old British actor would play the lead.  Sharlto Copley was going to star as Henri, Number Fours guardian and mentor, but had to drop out due to press obligations with his film The A-Team. Copley was replaced by Timothy Olyphant.  Kevin Durand plays the villain of the film, Commander, the Mogadorian who leads the hunt for the Lorics on Earth. 
 Fox television series Glee (TV series)|Glee, won the role. She plays Sarah Hart, a girl who used to date a high school football player, but falls for Number Four and keeps his secret.  Jake Abel plays the football player, Mark James, an antagonist in the film.  Teresa Palmer plays the other Loric, Number Six, and 16-year-old Australian actor Callan McAuliffe plays Sam Goode, Number Fours best friend. 

===Filming===
Filming began on May 17, 2010, using 20 locations all within the Pittsburgh metropolitan area.     DreamWorks selected the area primarily due to tax incentives from the Pennsylvania Film Production Tax Credit.  The film studio also had a positive experience shooting Shes Out of My League in Pittsburgh in 2008. The production was scheduled to last 12 to 13 weeks. 
 West Deer; Port Vue, North Park, Hyde Park were also used as locations.      The setting of the films fictional town of Paradise, Ohio is Vandergrift, Pennsylvania, where filming took place from June to July 2010.  Producers chose Vandergrift as the "hero town" of the film because of its unique look and curved streets, laid out by Frederick Law Olmsted, the designer of New York Citys Central Park. 
 Franklin Regional Murrysville was Monroeville for Lancaster Township.  Additional filming took place in the Florida Keys in the beginning of the film in Big Pine Key, Florida as well as the spanning of the drive over the bridge showcases the keys 7 mile bridge. 

===Post-production=== Yes guitarist Trevor Rabin. 

==Release==
A teaser trailer for the film was issued in late September 2010,  and a full length trailer premiered on December 8.    Advertisements ran in Seventeen (magazine)|Seventeen and Teen Vogue magazines, Disney released a promotional iPhone app in January 2011.  Disney also developed a lot of Internet content to target a teen audience with their marketing campaign.   A cast tour, in association with American retailer Hot Topic, and cast media appearances were scheduled to lead up to the release of the film.   

I Am Number Four premiered at the Village Theatre in Los Angeles on February 9, 2011. The film was released in theaters on February 18, 2011, and was also released in the IMAX format.    

=== Home media===
The film was released by   or Windows Media Player. All versions include bloopers and the "Becoming Number 6" feature, while the 1-disc Blu-ray and 3-disc Blu-ray, DVD, and "Digital Copy" combo pack versions additionally include six deleted scenes with an introduction from the director, D.J. Caruso.       In its first three weeks of release, 316,081 Blu-ray units were sold in the US, bringing in $7,693,808.  As of October 2, 2011, the standard DVD of I am Number Four has sold 767,692 copies in the United States, generating $12,571,326, and thus bringing the total gross to $166,247,931. 

==Reception==

===Box office===
The film earned $55,100,437 at the North American box office and an estimated $90,882,360 overseas, for a worldwide total of $145,982,797.    It topped the worldwide box office on its second weekend (February 25–27, 2011) with $28,086,805.  

The film opened at number two in the USA and Canada with a gross of $19,449,893. In its second weekend it dropped 43.4%, earning $11,016,126.  The only other market where it has grossed over $10 million is China. It began in third place with $3.4 million, but had an increase of 91% in its second week, topping the box office with $6.4 million. In its third week, it decreased by 21% to $5.0 million. As of March 27, 2011, it has grossed $17,328,244. 

=== Critical response    ===
The film received generally negative reviews from critics.

Empire Magazine gave the film 3 out of 5 stars and said, "If you can make it through the bland schmaltz of the first half youll be rewarded with a spectacular blast of sustained action and the promise of even better to come. This could be the start of something great." {{cite web
| url= http://www.empireonline.com/reviews/reviewcomplete.asp?DVDID=118753
|title= I Am Number Four Review
| publisher = Bauer Consumer Media
| work = Empire Magazine
| accessdate= 11 November 2011
}} 

==Soundtrack==
 
#Radioactive - Kings of Leon
#Tighten Up - The Black Keys 
#Rolling In The Deep - Adele  Rockwell 
#Shelter - The xx 
#Soldier On - The Temper Trap 
#Invented - Jimmy Eat World 
#Curfew - Beck 
#As Shes Walking Away - Zac Brown Band (ft. Alan Jackson)  Civil Twilight

==Sequel==
In 2011, screenwriter Marti Noxon told collider.com that plans for an imminent sequel were shelved due to the disappointing performance of the first installment at the box office. {{cite news 
| url=http://www.contactmusic.com/news/i-am-number-four-sequel-shelved_1219214
| title=Marti Noxon - I Am Number Four Sequel Shelved
| publisher= contactmusic.com
| date=16 February 2011
| accessdate=29 August 2014
}}  {{cite news 
| url=http://www.contactmusic.com/news/i-am-number-four-sequel-shelved_1219214
| title=10 Recently Cancelled Sequels : Not Coming Soon!
| publisher= contactmusic.com
| date=2 December 2012
| accessdate=29 August 2014
}} 

In 2013, however, when director D.J. Caruso was asked if there are any possibilities that The Power of Six will get a movie adaption, he replied: "There’s been some talk in the past couple of months about trying to do something because there is this audience appetite out there  . Most of the people on Twitter that contact me from all over the world ask, “Where’s the next movie?” I think DreamWorks is getting those too so it’ll be interesting. I don’t know if I’d be involved, but I know they’re talking about it."  {{cite news 
| first=Jamie
| last=Philbrick
| url=http://www.iamrogue.com/news/interviews/item/9516-iar-exclusive-interview-director-dj-caruso-talks-standing-up-preacher-invertigo-and-a-possible-i-am-number-four-sequel.html
| title=IAR EXCLUSIVE INTERVIEW: DIRECTOR D.J. CARUSO TALKS STANDING UP, PREACHER, INVERTIGO AND A POSSIBLE I AM NUMBER FOUR SEQUEL
| publisher= iamrogue.com
| date=18 August 2013
| accessdate=29 August 2014
}} 

In 2015, in an interview, James Frey, the author of the series, said that he would like to see more movies, and is hoping for more to be made.

==See Also==
*Cyborg 009

== References ==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 