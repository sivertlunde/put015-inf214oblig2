Hero Wanted
{{Infobox film
| name = Hero Wanted
| image = Hero-wanted-movie.jpg
| caption = DVD cover
| director = Brian Smrz
| producer = {{plainlist|
* Danny Lerner
* Johnny Martin
* David E. Ornston
* Richard Salvatore
* John Thompson
* Les Weldon
}}
| writer = {{plainlist|
* Chad Law
* Evan Law
}}
| starring = {{plainlist|
* Cuba Gooding Jr.
* Ray Liotta
* Kim Coates
* Norman Reedus
* Jean Smart
* Christa Campbell
* Ben Cross Tommy Flanagan
* Sammi Hanratty
}}
| music = Kenneth Burgomaster
| cinematography = Larry Blanford
| editing = Tim Anderson Nu Image Films
| distributor = Sony Pictures Home Entertainment
| released =  
| runtime = 94 minutes
| country = United States
| language = English
| budget = $7 million  
}} thriller film directed by Brian Smrz in his directorial debut, and starring Cuba Gooding Jr., Ray Liotta, Kim Coates, and Norman Reedus. The film was released on direct-to-video|direct-to-DVD in the United States on April 29, 2008.

==Plot==
Liam Case (Cuba Gooding Jr.) is a garbage man whose life hasnt quite turned out the way he expected it would. In order to impress the girl of his dreams, Liam plans an elaborate heist that will culminate with him jumping in to save the day at the last minute. When the day of the heist arrives, however, the plan takes an unexpected turn and Liam winds up in the hospital. Upon learning that a mysterious killer has slain the criminal that left both himself and the bank teller for dead during the chaos of the robbery, Liam realizes that the associates of the murdered thief wont stop until they have avenged the death of their fallen partner in crime.

==Cast==
* Cuba Gooding Jr. as Liam Case
* Ray Liotta as Detective Terry Subcott
* Kim Coates as Skinner McGraw
* Norman Reedus as Swain
* Jean Smart as Melanie McQueen
* Christa Campbell as Kayla McQueen
* Ben Cross as Cosmo Jackson Tommy Flanagan as Derek
* Gary Cairns II as Gill
* Steven Kozlowski as Lynch McGraw
* Sammi Hanratty as Marley Singer
* Paul Sampson as Gordy McGraw
* Todd Jensen as Detective Wallace MacTee

==Production==
Shooting took place in Sofia, Bulgaria in 32 days on April 3 and May 5, 2007. 

==Home media== Region 1 in the United States on April 29, 2008, and also Region 2 in the United Kingdom on 16 June 2008, it was distributed by Sony Pictures Home Entertainment. It is included a downloadable Digital Copy (PC and PSP) version of the disc with studio-imposed restrictions (and was one of the first DVDs to offer this now commonplace feature). 

==Reception==
David Johnson of DVD Verdict wrote, "Its violent and over-the-top, but Hero Wanted is ultimately a slick, hollow affair." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 