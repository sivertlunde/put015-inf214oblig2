The Sailor Takes a Wife
{{Infobox film
| name           = The Sailor Takes a Wife
| image_size     =
| image	         = 
| caption        =
| director       = Richard Whorf
| producer       = Edwin H. Knopf
| writer         = Anne Morrison Chapin Whitfield Cook
| based on       =   Robert Walker June Allyson
| music          =
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 
| country        = United States English
| budget         = $1,012,000  . 
| gross          = $2,559,000 
| preceded_by    =
| followed_by    =
}} Robert Walker and June Allyson. During World War II, a sailor in New York City who is about to be shipped out to Europe marries a woman he has just met. Then he unexpectedly receives a medical discharge.

==Reception==
According to MGM records the movie earned $2,269,000 in the US and Canada and $290,000 elsewhere, making a profit of $683,000. 

==Cast==
*Robert Walker as John Hill
*June Allyson as Mary Hill
*Hume Cronyn as Freddie Potts
*Audrey Totter as Lisa Borescu
*Eddie "Rochester" Anderson as Harry
*Reginald Owen as Mr. Amboy
*Gerald Oliver Smith as Gerald

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 


 