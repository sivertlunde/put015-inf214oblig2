El retrato de la peste
{{multiple issues|
 
 
}}

{{Infobox film
| name = El Retrato de la Peste
| image =
| caption =
| director = Lucila Las Heras
| producer = Ann Marie Flemming Gabriela Avigliano Lucila Las Heras Matías A. Gamio
| writer =
| starring =
| music = Pablo Borghi
| editing = Alberto Ponce
| distributor =
| released = 2008
| runtime = 10 minutes
| country = Argentina
| language =
| budget =
}}
El Retrato de la Peste ( ) is a 2008 stop motion-animated short film written, directed and animated by Lucila Las Heras.

==Plot==
Away in a tower, somewhere in Europe during the Middle Ages, Benjamin, a young painting apprentice, lives happily with his teacher, devoting his life to art. But the arrival of a mysterious plague threatens all they have.

==Awards==
* 2008 - Mejor Cortometraje de Animación (Best Animation Short Film) 3er. Festival Pizza, Birra y Cortos (Gálvez, Santa Fé, Argentina)
* 2008 - Mejor Cortometraje de Animación (Best Animation Short Film) AV Al Extremo Cortos (Río Gallegos, Santa Cruz, Argentina)
* 2009 - Mejor Cortometraje (Best Short Film) II Festival Gualeguaychú Suma Cine (Gualeguaychú, Entre Ríos, Argentina)

==External links==
*  

 
 
 
 
 
 


 
 