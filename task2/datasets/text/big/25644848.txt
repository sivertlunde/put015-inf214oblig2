Sarraounia (film)
{{Infobox film
| name           = Sarraounia
| image          = SarraouniaFilm.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Med Hondo
| producer       = Med Hondo
| writer         = Med Hondo Abdoulaye Mamani
| narrator       = 
| starring       = Aï Keïta Abdoulaye Cissé Issouf Compaore
| cinematography = Guy Famechon
| editing        = Marie-Thérèse Boiché
| studio         = Les Films Soleil O
| distributor    = 
| released       =   (France)
| runtime        = 120 minutes
| country        = Burkina Faso Mauritania France
| language       = Dioula language|Dioula, Fula language|Fula, French
| budget         = USD|$3,000,000
| gross          = 
| preceded by    = 
| followed by    = 
}} historical drama film written and directed by Med Hondo. It is based on a novel of the same name by Nigerien author Abdoulaye Mamani, {{Cite book
  | last = Ukadike
  | first = Nwachukwu Frank
  | authorlink = Frank Ukadike
  | coauthors = 
  | title = Black African Cinema
  | publisher = University of California Press
  | year = 1994
  | location = 
  | pages = 290–294
  | url = http://books.google.co.uk/books?id=INotDiiMhL0C
  | doi = 
  | id = 
  | isbn =0-520-07748-2 }} Hausa people) queen Sarraounia and the advancing French Colonial Forces of the Voulet-Chanoine Mission in 1899.  Sarraounia was one of few African tribal leaders that resisted the advances of French expansionists Paul Voulet and Julien Chanoine. The film won the first prize at the Panafrican Film and Television Festival of Ouagadougou (FESPACO) and was critically well received.

==Synopsis==
The film takes place in Niger and the surrounding region of the Sahel. {{Cite book
  | last = Cham
  | first = Mbye
  | authorlink =
  | editor-last= Pfaff
  | editor-first=Françoise
  | editor-link=Françoise Pfaff
  | title = Focus on African films
  | chapter= Film and History in Africa: A Critical Survey of Current Trends and Tendencies
  | publisher = Indiana University Press
  | year = 2004
  | location = 
  | pages = 64–66
  | url = http://books.google.co.uk/books?id=kjqh7O99pv0C
  | doi = 
  | id = 
  | isbn =0-253-21668-0 }}
  It begins with the initiation and establishment as queen of the Aznas of a young girl.  The young queen, Sarraounia, becomes an accomplished warrior when she defends her tribe from an enemy tribe. {{Citation
  | last = TR
  | first = 
  | title = Sarraounia Review Time Out
  | date = 
  | url = http://www.timeout.com/film/newyork/reviews/74012/Sarraounia.html
  | accessdate =2010-01-01
}}
  Accomplished in archery and herbalism, she is a renowned witchcraft|sorceress. {{Citation
  | last = Fujiwara
  | first = Chris
  | author-link = Chris Fujiwara
  | title = African Dream&nbsp;— The films of Med Hondo at the HFA The Boston Phoenix
  | date = 2000-04-27
  | url = http://www.bostonphoenix.com/archive/movies/00/04/27/AFRICAN_PERSPECTIVES_MED_H.html
  | accessdate =2010-01-05
}}
  Meanwhile, French colonialists Paul Voulet and Julien Chanoine set out to conquer new lands for the French colonial empire. As they advance across the land they rape women and leave burning villages in their wake. {{Cite book
  | last = Thackway
  | first = Melissa
  | authorlink = 
  | coauthors = 
  | title = Africa shoots back: alternative perspectives in Sub-Saharan Francophone
  | publisher = James Currey Publishers
  | year = 2003
  | pages = 95–96
  | url = http://books.google.co.uk/books?id=__PzjrAIUtYC
  | isbn = 0-85255-576-8}}
 

==Cast==
*Aï Keïta as Sarraounia
*Jean-Roger Milo as Capitaine Voulet
*Féodor Atkine as Chanoine
*Didier Sauvegrain as Doctor Henric
*Roger Miremont as Lieutenant Joalland
*Luc-Antoine Diquéro as Lt. Pallier
*Jean-Pierre Castaldi as Sergeant Boutel

==Production==
When Nigerien author Abdoulaye Mamani first published his novel Sarraounia (novel)|Sarraounia, he gave a copy to his friend Med Hondo who decided to put aside all other projects to adapt it into a film. {{cite book
|last= Ukadike
|first= Nwachukwu Frank
|author-link=Nwachukwu Frank Ukadike Conversations with Filmmakers
|publisher=University of Minnesota Press
|year= 2002
|pages=57–72
|chapter= Med Hondo (Mauritania)
|isbn=0-8166-4004-1
|chapter-url=http://books.google.co.uk/books?id=_CMKoMS-0KkC
}}  As well as using the book for reference, Hondo conducted research with Mamani, interviewing older Nigerien people and accessing material in the national archives. 

Hondo cast Aï Keïta after witnessing a confrontation between Keïta and a family member. Although he initially had her in mind for a small role in the film, he cast her as Sarraounia following the first casting session. {{Citation
  | last = 
  | first = 
  | author-link = 
  | last2 = 
  | first2 = 
  | author2-link = 
  | title = Aï Keita-Yara Interview
  | newspaper = Sisters of the Screen: Women of Africa on Film Video and Television
  | pages = 
  | year = 2000
  | url = http://www.africanwomenincinema.org/AFWC/Keita.html
  | accessdate =2010-01-01
}}
  This was her first acting job and she has since performed in films including Les Etrangers (The Foreigners) and SIDA dans la Cite (AIDS in the City), as well as in sitcoms. {{Citation
  | last = Quist Arcton
  | first = Ofeibea
  | author-link = 
  | title = Ai Keita Yara&nbsp;— From Queen to Soothsayer, From Celluloid to Video
  | newspaper = AllAfrica.com
  | pages = 
  | year = 
  | date = 2001-02-26
  | url = http://allafrica.com/stories/200102260050.html
  | accessdate =2010-01-01
}} 

The film was shot in 1986 in Burkina Faso. {{cite book
|last= Pfaff
|first= Françoise
|author-link=Françoise Pfaff
|editor-last= Harrow
|editor-first=Kenneth W.
|editor-link=
|title=With open eyes: women and African cinema Rodopi
|year= 1997
|pages=151–158
|chapter= Interview with Med Hondo
|isbn=90-420-0143-7
|url=http://books.google.co.uk/books?id=IouwFbVrjPUC
}}
  It cost USD|$3,000,000 to make, which was raised over seven years by Burkina Faso|Burkinabé financiers and Hondos own production company.  

==Reception==
The film won the First Prize (Étalon de Yennenga) at the 1987 Panafrican Film and Television Festival of Ouagadougou (FESPACO). {{Cite book
  | last = Diawara
  | first = Manthia
  | authorlink = 
  | coauthors = 
  | title = African cinema: politics & culture
  | publisher = Indiana University Press
  | year = 1992
  | location = 
  | pages = 152
  | url = http://books.google.co.uk/books?id=lIjrlnC0bmsC
  | doi = 
  | id = 
  | isbn =0-253-20707-X }} Time Out called it "superbly crafted and expansive". 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 