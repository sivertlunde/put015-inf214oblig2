For Sale (2013 film)
{{Infobox film
| name           = For Sale
| image          = 
| caption        = 
| alt            =
| director       = Sathish Ananthapuri
| producer       = Anto Kadavelil
| story          = Sathish Ananthapuri Mukesh Saikumar Saikumar  Vijayaraghavan 
| music          = Titus Mathew
| editing        = Ranjit Touchriver
| distributor    = Kadavelil Films
| released       =  
| country        = India
| language       = Malayalam
}}
For Sale is a Malayalam film from Kadavelil films directed by Satish Ananthapuri and produced by Anto Kadavelil. The film portrays the story of todays modeling and advertisement industry.The film deals with various subjects of social relevance.The influence of Parents lifestyle in the character formation of children, ill effects of alcohol in society, importance of family which everyone forgets in search fortunes etc. are depicted in it.   

==Soundtrack==
The soundtrack of the film is composed by Titus Mathew. The album features 4 songs which has been written by Vayalar Sarath Chandra Varma, Anto KADAVELIL and Geetha Thottam.

{{Track listing
| extra_column = Singer(s)
| music_credits = yes
| lyrics_credits = yes
| title1 = For Sale | extra1 = Kavitha Mohan,  Joslee Lonelydoggy | music1 = Titus Mathew | lyrics1 = Anto Kadavelil
| title2 = Ente Mizhi | extra2 = Thulasi Yatheendran, Sachin warrier| music2 = Titus Mathew | lyrics2 = Vayalar Sarat Chandra Varma
| title3 = Nee Varumo | extra3 = Manu Varghese | music3 = Titus Mathew | lyrics3 = Geetha Thottam
| title4 = Come to these clouds | extra4 = Kavitha Mohan | music4 = Titus Mathew | lyrics4 = Anto Kadavelil
}}

== Cast == Mukesh
* SaiKumar
* Vijaya Raghavan
* Kadhal Sandhya Aishwarya
* Sathaar
* Bheeman Raghu
* Kochu Preman
* Indrans
* Narayanankutty Abi
* Kottayam Nazir Nakshatra
* Shruthi Bala
* Sona Maria
* Lintu Thomas
* Valsala Menon
* Manoj Rana
* B C Roy
* Mahalakshmi

==References==
 

 
 
 


 