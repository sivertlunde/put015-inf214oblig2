Special Needs (film)
Special Needs is a dark comedy/satire about three reality TV producers creating show about people with physical and mental disabilities. The film was written, produced, and directed by Isaak James, who also starred. 

==Critical Reaction==
DVD Talk rated it "highly recommended", calling it "an instant candidate for independent comedy of the year ".  Variety called it "a rip-roaring good time" that succeeded with a subject that could have been disastrous.  Blogcritics found it "tastelessly hilarious".   French site Horreur.com was a bit more measured in its praise, suggesting a mixture of laughter and gritting of teeth as an appropriate reaction but awarded a rating of 5/6.   

== Release ==
It was an official selection at the Calgary International Film Festival and Westwood International Film Festival.  It was released on DVD by Troma. 

==References==
 

== External links ==
*  at Internet Movie Database
*  – at the Troma Entertainment movie database
*  at Official Movie Website

 
 
 
 


 