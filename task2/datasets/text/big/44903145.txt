The Winning of Sally Temple
{{Infobox film
| name           = The Winning of Sally Temple
| image          = 
| alt            = 
| caption        =
| director       = George Melford
| producer       = Jesse L. Lasky
| screenplay     = Rupert Sargent Holland Harvey F. Thew  Walter Long Horace B. Carpenter William Elmer Paul Weigel
| music          = 
| cinematography = Percy Hilburn 
| editing        = 
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent Walter Long, Horace B. Carpenter, William Elmer and Paul Weigel. The film was released on February 19, 1917, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Fannie Ward as Sally Temple
*Jack Dean as Lord Romsey Walter Long as Duke of Chatto
*Horace B. Carpenter as Oliver Pipe
*William Elmer as Tom Jellitt
*Paul Weigel as Talbot
*Henry Woodward as Lord Verney
*Harry Jay Smith as Lord Dorset
*Eugene Pallette as Sir John Gorham
*Florence Smythe as Kate Temple
*John McKinnon as Gregory
*Vola Vale as Lady Pamela Vauclain

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 