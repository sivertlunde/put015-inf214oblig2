Daughter of the Dragon
{{Infobox Film
| name           = Daughter of the Dragon
| image_size     = 
| image	         = Poster - Daughter of the Dragon 01.jpg
| caption        = Theatrical release poster
| director       = Lloyd Corrigan
| producer       = 
| writer         = Sax Rohmer (characters) Lloyd Corrigan Monte M. Katterjohn Sidney Buchman (dialogue)
| starring       = Anna May Wong Warner Oland Sessue Hayakawa Bramwell Fletcher
| music          = 
| cinematography = 
| editing        = 
| distributor    = Paramount Pictures
| released       = September 5, 1931
| runtime        = 70 or 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Daughter of the Dragon is a 1931 American film directed by Lloyd Corrigan, released by Paramount Pictures, and starring Anna May Wong as Princess Ling Moy, Sessue Hayakawa as Ah Kee, and Warner Oland as Dr. Fu Manchu (for his third and final feature appearance in the role, excluding a gag cameo in Paramount on Parade). The film was made to capitalize on Sax Rohmers then current book, The Daughter of Fu Manchu, which Paramount did not own rights to adapt.

==Plot==
Princess Ling Moy lives next door to Dr. Fu Manchu, and is romantically involved with Ah Kee, a secret agent determined to thwart Fu Manchu. It is revealed that Fu Manchu is Ling Moys father.

==Cast==
*Anna May Wong as Princess Ling Moy
*Warner Oland as Dr. Fu Manchu
*Sessue Hayakawa as Ah Kee
*Bramwell Fletcher as Ronald Petrie
*Frances Dade as Joan Marshall
*Holmes Herbert as Sir John Petrie
*Lawrence Grant as Sir Basil Courtney
*Harold Minjir as Rogers
*Nicholas Soussanin as Morloff
*E. Alyn Warren as Lu Chung

==See also==
*The House That Shadows Built (1931 promotional film by Paramount with excerpt of this film)

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 