Pra Frente, Brasil
{{Infobox film
| name           = Pra Frente, Brasil
| image          = Pra frente Brasil.jpg
| caption        = Theatrical release poster
| director       = Roberto Farias
| producer       = Roberto Farias
| writer         = Roberto Farias
| based on       =  
| starring       = Reginaldo Faria Antônio Fagundes Natália do Valle Elizabeth Savalla
| music          = Egberto Gismonti
| cinematography = Dib Lutfi
| editing        = Mauro Farias Roberto Farias
| studio         = Embrafilme Produções Cinematográficas R.F. Farias Ltda.
| distributor    = Embrafilme
| released       =  
| runtime        = 105 minutes
| country        = Brazil
| language       = Portuguese
}} military dictatorship during the 1970 FIFA World Cup.

==Plot==
The film is set on mid 1970, when the military regimes "economic miracle" and the victory of the Brazilian national football team on the FIFA World Cup serves as a distraction for the persecution of opposition leaders by the political police of the dictatorship.

Under this context, Jofre Godoi da Fonseca, an alienated middle class man, is mistaken for Sarmento, a political activist he met at an airport prior to his assassination. He is then arrested and brutally tortured by a paramilitary group of vigilantes sponsored by influential businessmen to hunt down people deemed "subversive" by the regime.
 law enforcement agents, Mariana, leader of a left-wing resistance group and Miguels former girlfriend, helps them. Their efforts proved to be useless after Jofre is killed on a failed escape attempt.

==Cast==
* Reginaldo Faria as Jofre Godoi
* Antônio Fagundes as Miguel Godoi
* Natália do Valle as Marta Godoi
* Elizabeth Savalla as Mariana
* Carlos Zara as Dr. Barreto
* Cláudio Marzo as Sarmento

==Awards and nominations==
;33rd Berlin International Film Festival
* Golden Bear &ndash; Roberto Farias (nominated)   
* C.I.C.A.E. Award &ndash; Roberto Farias (won)
* OCIC Award &ndash; Roberto Farias (won)

;10th Gramado Film Festival
* Best Film &ndash; Roberto Farias (won)
* Best Editing &ndash; Roberto Farias, Maurício Farias (won)

;17th Silver Daisy Awards
* Best Feature Film &ndash; Roberto Farias (won)

;9th Festival de Cine Iberoamericano de Huelva
* Critics Choice Award 

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 