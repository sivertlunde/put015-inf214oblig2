Bed Dance
{{Infobox film
| name = Bed Dance
| image = Bed Dance (film).jpg
| image_size = 
| caption = Theatrical poster for Bed Dance (1967)
| director = Toshio Okuwaki ( ) 
| producer = 
| writer = 
| narrator = 
| starring = Setsu Shimizu Naomi Tani Machiko Sakyō
| music = 
| cinematography = 
| editing = 
| studio = 
| distributor = World Eiga
| released = 1967
| runtime = 66 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1967 Japanese pink film directed by Toshio Okuwaki for World Eiga. One of the directors most praised films, it features an early appearance by Naomi Tani.

==Synopsis==
The husband of a young wife discovers she has a promiscuous background as a bar hostess. Feeling that his honor has been slighted, he refuses to have intimate relations with her. The woman flees her old-fashioned husband for solace in the arms of a former lover.      

==Cast==
* Setsu Shimizu ( )  
* Michiko Sakyō ( )
* Naomi Tani

==Critical appraisal==
In their Japanese Cinema Encyclopedia: The Sex Films, Thomas and Yuko Mihara Weisser judge Bed Dance to be one of director Toshio Okuwakis better efforts. The films cast is noted as one of the strong points of Bed Dance, particularly the appearance early in her career of Naomi Tani. The poorly thought-out script and conflicting themes, however, are said to be weaknesses of the film.   

The films description at the time of its 2003 showing at the Yamagata International Documentary Film Festival noted a surprisingly Freudian touch to the films plot.  Allmovie also writes that Bed Dance is one of Okuwakis better films, and that, besides erotic interest, its storyline provides a glimpse of cultural and sexual mores at a time of rapid change. 

==Director Toshio Okuwaki==
Director Toshio Okuwaki is generally not well known today.  Besides his marriage to Tamaki Katori, the "Pink Princess" of the first wave of Pink films,  Okuwaki is today best remembered for Naked Pursuit (1968), one of the early pink films which has been lost in Japan, but was preserved in the U.S. due to its distribution through Harry Novaks Box Office International Pictures.  1967, the year of Bed Dances release, was the high-point of Okuwakis career. Allmovie notes that Okuwakis career went into decline after making Bed Dance, Onna No Aji and Climax, all from 1967.     The Weissers agree, writing that after 1967 Okuwaki became a "notorious studio hack" who directed a "parade of interchangeable sex flicks."  A three-film retrospective of Okuwakis work with Tamaki Katori was a feature at the 2003 Yamagata International Documentary Film Festival.    Bed Dance was shown at the same festival, paired with an early work by Mamoru Watanabe. 

==Bibliography==
*  
*  
*  
*  
*  

==Notes==
 

 
 
 
 