The Connected Baby
{{Infobox film 
| name           = the connected baby 
| image          = The_Connected_Baby_DVD_Cover.jpg 
| image_size     = 
| border         = 
| alt            = Connected Baby Cover 
| caption        = DVD cover 
| co-producer       = Dr Suzanne Zeedyk 
| co-producer       = Jonathan Robertson 
| writer         = 
| screenplay     = 
| story          = 
| based on       =   
| narrator       = Dr Suzanne Zeedyk 
| starring       = 
| music          =   
| cinematography = 
| editing & camera     = Jonathan Robertson 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 75 mins 
| country        = United Kingdom 
| language       = English 
| budget         = 
| gross          = 
}} developmental psychologist Dundee Contemporary Arts centre in Scotland.  Funded by the British Psychological Society, the film explores research showing that infants are born with active social abilities, already able to communicate and connect emotionally with others.  This view of infants contrasts with traditional claims that they are born as passive ‘blank canvases’, who acquire communicative capacities as they develop. 

==Synopsis==
The film provides visual footage of contemporary research findings that babies are born with innate social abilities and are able to engage in emotional exchanges by responding to other peoples body rhythms and vocal tones and movements. It features footage of parents, grandparents, and siblings interacting with infants, as well as commentary from infant researchers in the United Kingdom. 

==Chapters==
Each of these chapters analyses a specific interaction between the baby and another person, during which the babys communicative abilities are illuminated.  A trailer of the film is publicly available. 
* "the dance of connection", 
* "the dance of the nappy", 
* "the dance of the air", 
* "the dance of the mirror" 

==History==
In 2011, the film featured as one of the Renfrewshire-based events in the Scottish Mental Health Arts & Film Festival   and also received showings in New York   and Albuquerque, USA.  In 2012, showings were held at events based within the Scottish Parliament   and the Northern Irish Assembly at Stormont.  The film also toured venues in Northumberland, England in July 2012,  as organized by the local governmental authority. All of these events grew out of a desire by associated organisations to highlight the scientific links that have been established between early life experiences and later life outcomes, in domains such as health, education, imprisonment, and sustaining relationships.  The film has since been cited in academic publications. 

==References==
 

==External links==
*  
*   at the Internet Movie Database

 
 
 