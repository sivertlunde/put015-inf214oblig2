A Dash of Courage
 
{{Infobox film
| name           = A Dash of Courage thumb
| caption        = Still from film featuring William Mason, Harry Gribbon, Wallace Beery and Raymond Griffith
| director       = Charley Chase
| producer       = Mack Sennett (Keystone Studios)
| writer         = 
| starring       = Gloria Swanson Wallace Beery
| cinematography = Paul Garnett
| editing        = 
| studio         = 
| distributor    = Triangle Film Corporation
| released       =  
| runtime        = 20 minutes; 2 reels
| country        = United States
| language       = Silent with English intertitles
| budget         = 
| gross          = 
}} silent comedy film directed by Charley Chase, starring Gloria Swanson,    and featuring Wallace Beery, to whom she was briefly married.

==Synopsis== A band of crooks, headed by Harry Gribbon, are on a train when they learn of a telegram sent to a fellow passenger, who is a police commissioner. The wire identifies him as official collector for the Old Cops Home. A little chloroform does for him and when the train pulls out of his destination he is still on board while Gribbon is posing as the commissioner-collector. Great preparations have been made to receive the distinguished visitor. The only drawback to the welcome is the sour music dispensed by the police band. The musicians are sent upstairs in the police station to practice some more, and here they are found by Gribbons associates. A quart of chloroform poured while they are asleep the crooks exchange clothing with them.

During the interruption Gribbon has begun his collecting by attempting to rob the safe of the richest man in town. Woodward telephones for the police and the crooks respond. They suggest that he wait until the culprit has the money, which can be used as evidence. Gribbon is then arrested by his own men. Woodward suspicious after a long wait, again telephones to the station and is answered by the real police, who have revived. Their pursuit is complicated by the fact that they are wearing the clothes which belong to the pursued. A combination of thrills and laughs brings the picture to a close.|Moving Picture World, May 27, 1916 }}

==Cast==
* Harry Gribbon as Police Commissioner
* Guy Woodward
* Gloria Swanson
* Bobby Vernon
* Frank Opperman
* Wallace Beery as Police chief
* Hank Mann
* Nick Cogley Billy Mason (as William Mason)
* Al Kaufman
* Jack Mintz William Jefferson
* Joan Havez
* Charley Chase
* Raymond Griffith as Policeman (as Ray Griffith) Jack Henderson

==Reviews==
Louis Reeves Harrisons review in The Moving Picture World (1916) said that Harry Gribbons performance offered "much that is new in characterization and in his personality, and his support is active enough to make the little play move with snap from start to finish".  Thomas C. Kennedy said in his review for Motography (1916) that "one finds many really humorous moments, and the picture tells a story that is always interesting; in fact, a story with such continuity as this one has is quite unusual in farce of the Keystone type". Kennedy praised the actors performances, highlighting Gribbons leading role saying, he "wins the acting honors". 
==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 