Katha (2009 film)
 
 
{{Infobox film
| name = Katha
| image =
| alt =  
| caption =
| director = Srinivas Raga
| producer = Urmila Gunnam
| writer = Gangaraju Gunnam
| screenplay =
| story =
| starring = Adith Arun Genelia DSouza Prakash Raj  Aamir Tameem
| music = S. K. Balachandran
| cinematography =
| editing =
| studio =
| distributor = Just Yellow Media
| released =  
| runtime =
| country = India
| language = Telugu
| budget =
| gross =
}}

Katha is a 2009 Telugu language|Telugu-language thriller film directed by Srinivas Raga. The film stars Genelia DSouza in the lead and is produced by Urmila Gunnam. 

==Plot==
Chitra (Genelia) is a loner who comes to Araku seeking employment as a teacher in a kids school. Krishna (Arun) is an aspiring director. Krishna comes to Araku to do a trial shoot and complete the script of his debut film. Theres a Circle Inspector (Prakash Raj), who has a wife & younger kids. Chitra spent 1 year in a mental asylum because she witnessed her family getting killed by more than 1 evil guy. Chitra & Krishna become good friends. While Chitras peeping through her binoculars, she notices a murder where she could clearly see the victims face. The victims a female. Chitra reports it to the police, but they find no evidence of murder. Chitra doubts that her past mental conditions catching up again. Eventually, they find out that the female victim existed. They have a disc that shows Circle Inspector & the Female Victim together in a store. The female victim was none other than Circle Inspectors dead girlfriends daughter. Circle Inspector couldnt bring his daughter to Araku because of his reputation. Its revealed that Circle Inspector lured his daughter to Araku because he wanted to kill her. Although Chitra witnessed the murder, Circle Inspector tried to make it look like that Chitras past mental condition made her hallucinate the murder. Circle Inspectors irritated because Chitra refused to believe that the murder was just her hallucination. Chitra & Krishna try to take the disc & escape, but Circle Inspector tries to kill Krishna. Its hilarious because Krishna injured Circle Inspectors foot. Circle Inspectors holding Krishna at gunpoint, but Chitra refuses to give the disc to Circle Inspector. Chitra slaps Circle Inspector, who decides to let go of Krishnas shirt. Half fortunately & Half unfortunately, Circle Inspector commits suicide. Chitra doesnt believe that Circle Inspector was going to kill her & Krishna because he already framed a dead officer for the female victims murder. Chitra & Krishna take the disc as evidence to the police. The film ends with the union of Chitra & Krishna.

==Cast==
* Adith Arun as Krishna
* Genelia DSouza as Chitra Singh
* Prakash Raj
* Aamir Tameem as Raghu

==References==
 

 
 
 

 