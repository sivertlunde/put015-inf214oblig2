West of Suez
{{Infobox film
| name           = West of Suez
| image          = "West_of_Suez"_(1957).jpg
| image_size     = 
| caption        = U.S. theatrical poster
| director       = Arthur Crabtree Richard Gordon
| screenplay     = Norman Hudis based on        = an original story by Charles F. Vetter (as Lance Hargreaves) and Norman Hudis
| narrator       = 
| starring       = Keefe Brasselle
| music          = Wilfred Burns	(uncredited)
| cinematography = Walter J. Harvey (as James Harvey) Peter Mayhew
| studio         = Amalgamated Productions
| distributor    =  Astral (UK)
| released       = 1957
| runtime        = 74 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| website        = 
| amg_id         = 
}} British drama film directed by  Arthur Crabtree and starring Keefe Brasselle, Kay Callard and Karel Stepanek.  An adventurer is hired to assassinate the leader of an Arab movement advocating peace, but is unable to complete his mission.

==Cast==
* Keefe Brasselle as Brett Manders
* Kay Callard as Pat
* Karel Stepanek as Langford
* Ursula Howells as Eileen
* Bruce Seton as Major Osborne
* Richard Shaw as Cross
* Harry Fowler as Tommy
* Sheldon Lawrence as Jeff
* Alex Gallier as Ibrahim Sayed
* Maya Koumani as Men Hassa

==Production== Richard Gordon had to replace him with Arthur Crabtree. Tom Weaver, The Horror Hits of Richard Gordon, Bear Manor Media 2011 p 18 

==Critical reception==
TV Guide called it an "okay suspense story with a dull romantic subplot."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 