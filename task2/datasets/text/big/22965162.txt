Shyness Machine Girl
{{Infobox Film
| name = Shyness Machine Girl
| image = Shyness Machine Girl.jpg
| caption = DVD cover 
| director       = Noboru Iguchi
| producer       = Yoshinori Chiba Yoko Hayama Kazuto Morita
| writer         = Noboru Iguchi
| narrator       = 
| starring       = Noriko Kijima Yūya Ishikawa Demo Tanaka Kentarō Kishi Hiroaki Murakami Yukihide Benny Takatoshi Naoi Masahiro Aoki
| music          = Kou Nakagawa
| cinematography = Yoshihiro Nishimura
| editing        = Takeshi Wada
| distributor    = Fever Dreams
| released       =  
| runtime        = 22 minutes
| country        = Japan Japanese
| budget         = 
| gross          = 
}}
  was released January 23, 2009 in Japan as part of the Japanese DVD release for The Machine Girl. It is a brief side-story or gaiden to The Machine Girl rather than a direct sequel.   This version stars gravure idol Noriko Kijima as Yoshie, who has not only a machine-gun arm but another gun which extends from her lower anatomy. 

==Synopsis==
Amis friend, Yoshie, was murdered and desecrated by the Kimura Gang. She was saved and, like Ami in the previous film, received modifications from the same mechanics. Remembering her past, Yoshie decides to avenge herself and Ami.

==Cast==
* Noriko Kijima as Yoshie
* Yūya Ishikawa as Suguru Sugihara
* Demo Tanaka as Kaneko/Kimura gang member
* Kentarō Kishi as Yōsuke Fujii
* Hiroaki Murakami
* Yukihide Benny  (as Yukihide Benii)
* Takatoshi Naoi
* Masahiro Aoki
* Minase Yashiro as Ami Hyūga (flashback sequences) (archive footage) Asami as Miki Sugihara (as Asami Sugiura)
* Kentarō Shimazu as Ryūji Kimura/Kimura gang boss

==References==
 

==External links==
*   at Spopro.net, via Wayback Machine
*  

 
 
 
 
 
 
 


 
 