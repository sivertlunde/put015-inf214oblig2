Superman (1978 film)
{{Infobox film
| name                 = Superman
| image                = Superman ver1.jpg
| caption              = Theatrical release poster by Bob Peak
| director             = Richard Donner
| producer             = Pierre Spengler
| screenplay           = {{Plain list|
* Mario Puzo David Newman
* Leslie Newman
* Robert Benton
}}
| story                = Mario Puzo
| based on             =  
| starring             = {{Plain list|
* Marlon Brando
* Gene Hackman
* Christopher Reeve
* Ned Beatty
* Jackie Cooper
* Glenn Ford
* Trevor Howard
* Margot Kidder
* Valerie Perrine
* Maria Schell
* Terence Stamp
* Phyllis Thaxter
* Susannah York
}}
| music                = John Williams
| cinematography       = Geoffrey Unsworth
| editing              = {{Plain list|
* Stuart Baird
* Michael Ellis
}}
| production companies = {{Plain list|
* Film Export A.G.
* Dovemead Limited
* International Film Productions
}}
| distributor          = {{Plain list|
* Warner Bros.  }}
* Columbia Pictures|Columbia-EMI-Warner  }}
}}
| released             =  
| runtime              = 143 minutes 
| country              = {{Plain list|
* United Kingdom
* United States 
}}
| language             = English
| budget               = $55 million   
| gross                = $300.2 million 
}}
 same name Krypton and Metropolis and develops a romance with Lois Lane, while battling the villainous Lex Luthor.
 David and Leslie Newman, and Robert Benton) were associated with the project before Donner was hired to direct. Tom Mankiewicz was drafted in to rewrite the script and was given a "creative consultant" credit. It was decided to film both Superman and Superman II simultaneously, with principal photography beginning in March 1977 and ending in October 1978. Tensions rose between Donner and the producers, and a decision was made to stop filming the sequel—of which 75 percent had already been completed—and finish the first film. 
 Best Film Best Music Best Sound Mixing, and received a Special Achievement Academy Award for Visual Effects. Groundbreaking in its use of special effects and science fiction/fantasy storytelling, the films legacy presaged the mainstream popularity of Hollywoods superhero film franchises.

==Plot==
On the planet Krypton (comics)|Krypton, using evidence provided by scientist Jor-El, the Ruling Council sentences three attempted insurrectionists, General Zod, Ursa (comics)|Ursa, and Non (DC Comics)|Non, to "eternal living death" in the Phantom Zone. Despite his eminence, Jor-El is unable to convince the Council of his belief that Krypton will soon be destroyed when its sun explodes. To save his infant son, Superman|Kal-El, Jor-El launches a spacecraft containing him toward Earth, a distant planet with a suitable atmosphere, and where Kal-Els dense molecular structure will give him superhuman powers. Shortly after the launch, Krypton is destroyed.
 Clark after Marthas maiden name.
 romantic attraction to coworker Lois Lane, but she sees him as awkward and unsophisticated.

Lois becomes involved in a helicopter accident where conventional means of rescue are impossible, requiring Clark to use his powers in public for the first time to save her. During the same night, he rescues a little girls cat from a tree in Brooklyn Heights, thwarts a jewel thief attempting to scale the Solow Building with suction cups, intercedes in a police chase where many of the robbers escape via cabin cruiser (which Superman deposits on Wall Street), and even finds time to rescue Air Force One after a flameout, making the mysterious "caped wonder" a celebrity. He visits Lois at home, takes her for a flight over the city, and allows her to interview him for an article in which she names him "Superman."

Meanwhile, criminal genius Lex Luthor has developed a cunning plan to make a fortune in real estate by buying large amounts of barren desert land and then diverting a nuclear missile test flight to the San Andreas Fault. It will sink California and leave Luthors desert as the new West Coast of the United States, greatly increasing its value. After his incompetent henchman, Otis, accidentally redirects the first rocket to the wrong place following his first delay on the convoy, Luthors girlfriend, Eve Teschmacher, successfully changes the course of a second missile while the military is distracted by his roadblock.

Knowing Superman could stop his plan, Luthor lures him to his underground lair and exposes him to Kryptonite. As he weakens, Luthor taunts him by revealing that the first missile is a decoy, headed east towards Hackensack, New Jersey, knowing that even Superman cant stop both impacts. Teschmacher is horrified because her mother lives in Hackensack, but Luthor does not care and leaves Superman to a slow death. Knowing his reputation for keeping his word, Teschmacher rescues him on the condition that he will deal with the New Jersey missile first. After diverting the east-bound one into outer space, he witnesses the west-bound one detonating in the San Andreas Fault. He is able to mitigate the effects of the nuclear explosion, getting rid of the pollution from the fall-out and shoring up the crumbling Earth, but the aftershocks are still devastating.

While Superman is busy saving others, Loiss car falls into a crevice that opens due to an aftershock. It quickly fills with dirt and debris and she suffocates to death. Angered at being unable to save her, Superman ignores Jor-Els warning not to interfere with human history, preferring to heed Jonathans advice that he must be on Earth for "a reason". He travels back in time (by flying around the Earth at near-relativistic speed) in order to save Lois, altering history so that her car is never caught in the aftershock. He then delivers Luthor and Otis to prison and flies into the sunrise for further adventures.

==Cast==
*  . He has a theory about the planet exploding, though the Council refuses to listen. He dies as the planet explodes but successfully sends his infant son to Earth as a means to help the innocent. Brando sued the Salkinds and Warner Bros. for $50 million because he felt cheated out of his share of the box office profits.  This stopped Brandos footage from being used in Richard Lesters version of Superman II. 

* Gene Hackman as Lex Luthor: An evil scientific genius armed with vast resources who would prove to be Supermans nemesis. It is he who discovers Supermans weakness and hatches an evil plan that puts millions of people in danger.

*   at the Daily Planet as mild-mannered newspaper reporter Clark Kent. Reeve was picked from over 200 actors who auditioned for the role.

* Ned Beatty as Otis: Lex Luthors bumbling henchman.

*   was originally cast, but dropped out shortly before filming because of heart disease. Cooper, who originally auditioned for Otis, was subsequently cast. 

*   that changes Clarks outlook on his duty to others.

* Trevor Howard as the First Elder: Head of the Kryptonian Council, who does not believe Jor-Els claim that Krypton is doomed to its own destruction. He threatens Jor-El, "Any attempt by you to create a climate of fear and panic amongst the populace must be deemed by us an act of insurrection."

*  , who becomes a romantic interest to Clark Kent. Over 100 actresses were considered for the role. Margot Kidder (suggested by Stalmaster), Anne Archer, Susan Blakely, Lesley Ann Warren, Deborah Raffin and Stockard Channing screen tested from March through May 1977. The final decision was between Channing and Kidder, with the latter winning the role.  

* Jack OHalloran as Non (DC Comics)|Non: Large and mute, the third of the Kryptonian villains who are sentenced to be isolated in the Phantom Zone.

*  . She shows a romantic interest in Superman, implied by her fixing her hair before she makes her presence known to him, and then by kissing him before she saves his life.

* Maria Schell as Vond-Ah: Like Jor-El, a top Kryptonian scientist; but she too is not swayed by Jor-Els theories.

*  .

*  s mother-in-law. 

* Susannah York as Lara (comics)|Lara: Supermans biological mother on Krypton. She, after learning of Kryptons fate, has apprehensions about sending her infant son to a strange planet alone.

*   (Diane Sherry). Following the death of his adoptive father, he travels to the Arctic to discover his Kryptonian heritage (All of Easts dialogue in the film is dubbed over by Christopher Reeve). 

* Marc McClure as Jimmy Olsen: A teenage photographer at the Daily Planet. Jeff East, who portrayed the teenage Clark Kent, originally auditioned for this role. 

* Sarah Douglas as Ursa (comics)|Ursa: General Zods second in command and consort, sentenced to the Phantom Zone for her unethical scientific experiments.

* Harry Andrews as the Second Elder: Council member, who compels Jor-El to be reasonable about plans to save Krypton.

  and Terri Hatcher on Smallville.

Larry Hagman and Rex Reed also cameo; Hagman plays an army major in charge of a convoy that is transporting one of the missiles, while Reed plays himself as he meets Lois and Clark outside the Daily Planet headquarters. A then unknown John Ratzenberger briefly appears as a missile control technician. Two unknowns also have cameos, Edward Finneran and Tim Hussey, who were the teenage boys who won "The Great Superman Movie Contest", appearing as football players (in gray) during the scene with Clark as the equipment manager of the Smallville football team. David Petrou, the author of the making-of book about the film also appears briefly in that scene.

==Production==

===Development===
Ilya Salkind had first conceived the idea for a Superman film in late 1973.    In November 1974, after a long difficult process with    The filmmakers felt it was best to film Superman and     .  

Ilya wanted to hire Steven Spielberg to direct, but Alexander was skeptical, feeling it was best to "wait until   big fish opens". Jaws (film)|Jaws was very successful, prompting the producers to offer Spielberg the position, but by then Spielberg had already committed to Close Encounters of the Third Kind.  Guy Hamilton was hired as director, while Puzo delivered his 500-page script for Superman and Superman II in July 1975.  Jax-Ur appeared as one of General Zods henchman, with Clark Kent written as a television reporter. Dustin Hoffman, who was previously considered for Superman, turned down Lex Luthor.  
 Oscar winner David Newman The Late Leslie was brought in to help her husband finish writing duties.  George MacDonald Fraser was later hired to do some work on the script but he says he did little. George MacDonald Fraser, The Lights On at Signpost, HarperCollins 2002 p55-65 
 camp tone, including a cameo appearance by Telly Savalas as his Kojak character. The scripts for Superman and Superman II were now at over 400-pages combined.       Pre-production started at Cinecittà Studios in Rome, with sets starting construction and flying tests being unsuccessfully experimented. "In Italy," producer Ilya Salkind remembered, "we lost about $2 million  ."  Marlon Brando found out he couldnt film in Italy because of a warrant out for his arrest, a sexual obscenity charge from Last Tango in Paris. Production moved to England in late-1976, but Hamilton could not join because he was a tax exile. 
 crest resembling a different letter, justifying the S on Supermans costume.  The Writers Guild of America refused to credit Mankiewicz for his rewrites, so Donner gave him a creative consultant credit, much to the annoyance of the Guild. 

====Casting of Superman====
It was initially decided to first sign an A-list actor for Superman before Richard Donner was hired as director. Robert Redford was offered a large sum, but felt he was too famous. Burt Reynolds also turned down the role, while Sylvester Stallone was interested, but nothing ever came of it. Paul Newman was offered his choice of roles as Superman, Lex Luthor or Jor-El for $4 million, turning down all three roles. 
 casting director Lynn Stalmaster first suggested Christopher Reeve, but Donner and the producers felt he was too young and skinny.   Over 200 unknown actors auditioned for Superman. 

Olympic champion Bruce Jenner had auditioned for the title role.  Patrick Wayne was cast, but dropped out when his father John Wayne was diagnosed with stomach cancer. 

Both Neil Diamond and Arnold Schwarzenegger lobbied hard for the role, but were ignored. James Caan, James Brolin, Lyle Waggoner, Christopher Walken, Nick Nolte, Jon Voight, and Perry King were approached.   Kris Kristofferson and Charles Bronson were also considered for the title role. 

James Caan said he was offered the part but turned it down. "I just couldnt wear that suit." James Caans career hitting tough times
Siskel, Gene. Chicago Tribune (1963-Current file)   27 Nov 1977: e6. 
 dentist was screen tested.  
 teenage Clark overdubbed by Reeve. "I was not happy about it because the producers never told me what they had in mind," East commented. "It was done without my permission but it turned out to be okay. Chris did a good job but it caused tension between us. We resolved our issues with each other years later."  East also tore several thigh muscles when performing the stunt of racing alongside the train. He applied 3 to 4 hours of prosthetic makeup daily to facially resemble Reeve.   

===Filming=== television rights.  
 Brooklyn Heights The Three The Four Musketeers, was then brought in as a temporary co-producer to mediate the relationship between Donner and the Salkinds,  who by now were refusing to talk to each other.  With his relationship with Spengler, Donner remarked, "At one time if Id seen him, I would have killed him." 

Lester was offered producing credit but refused, going uncredited for his work.  Salkind felt that bringing a second director onto the set meant there would be someone ready in the event that Donner couldnt fulfill his directing duties. "Being there all the time meant he   could take over," Salkind admitted. "  couldnt make up his mind on stuff."  On Lester, Donner reflected, "Hed been suing the Salkinds for his money on Three and Four Musketeers, which hed never gotten. He won a lot of his lawsuits, but each time he sued the Salkinds in one country, theyd move to another, from Costa Rica to Panama to Switzerland. When I was hired, Lester told me, Dont do it. Dont work for them. I was told not to, but I did it. Now Im telling you not to, but youll probably do it and end up telling the next guy. Lester came in as a go-between. I didnt trust Lester, and I told him. He said, Believe me, Im only doing it because theyre paying me the money that they owe me from the lawsuit. Ill never come onto your set unless you ask me; Ill never go to your dailies. If I can help you in any way, call me." 

It was decided to stop shooting Superman II and focus on finishing Superman. Donner had already completed 75% of the sequel.  The filmmakers took a risk: if Superman was a   where it was fully restored.

Donner commented, "I decided if Superman is a success, theyre going to do a sequel. If it aint a success, a cliffhanger aint gonna bring them to see Superman II." 

===Effects===
{{multiple image
 | align     = right
 | direction = vertical
 | header    =
 | header_align = left/right/center
 | header_background =
 | footer    =
 | footer_align = left/right/center
 | footer_background =
 | width     =
 | image1    = Sprmnmovie.jpg
 | width1    = 240
 | caption1  = Publicity still emulating screen shot.
 | image2    = Superman movie vid cap2.JPG
 | width2    = 240
 | caption2  = Actual screen shot for comparison. Suit has greenish hue, for use with blue-screen effects.
}}
 digital age. Young Clark football punt blue screen made it transparency (optics)|transparent. The Magic Behind The Cape, 2001, Warner Home Video 

The first test for the flying sequences involved simply catapulting a crash test dummy out of a cannon. Another technique had a remote control cast of Superman flying around. Both were discarded due to lack of movement. High quality, realistic-looking animation was tried, with speed trails added to make the effect more convincing.

As detailed in the Superman: The Movie DVD special effects documentary The Magic Behind The Cape, presented by optical effects supervisor Roy Field, in the end, three techniques were used to achieve the flying effects.

For landings and take-offs, wire flying riggings were devised and used. On location, these were suspended from tower cranes, whereas in the studio elaborate rigs were suspended from the studio ceilings. Some of the wire-flying work was quite audacious considering computer controlled rigs were not then available&nbsp;— the penultimate shot where Superman flies out of the prison yard for example. Although stuntmen were used, Reeve did much of the work himself, and was suspended as high as 50&nbsp;ft in the air. Counterweights and pulleys were typically used to achieve flying movement rather than electronic or motorized devices. The thin wires used to suspend Reeve were typically removed from the film in post-production using rotoscope techniques, although this wasnt necessary in all shots (in certain lighting conditions or when Superman is very distant in the frame the wires were more or less imperceptible).
 matte area and the matted image&nbsp;— in the case Superman&nbsp;— dont exactly match-up) and the slightly unconvincing impression of movement achieved through the use of zoom lenses is characteristic of these shots.

Where the shot is tracking with Superman as he flies (such as in the Superman and Lois Metropolis flying sequence) front projection was used. This involved photographing the actors suspended in front of a background image dimly projected from the front on to a special screen made by 3M that would reflect light back at many times the original intensity directly in to a combined camera/projector. The result was a very clear and intense photographic reproduction of both the actors and the background plate, with far less image deterioration or lighting problems than occur with rear projection.

A technique was developed that combined the front projection effect with specially designed zoom lenses.  The illusion of movement was created by zooming in on Reeve while making the front projected image appear to recede. For scenes where Superman interacts with other people or objects while in flight, Reeve and actors were put in a variety of rigging equipment with careful lighting and photography.  This also led to the creation of the Zoptic system. 

The highly reflective costumes worn by the Kryptonians are made of the same 3M material used for the front projection screens and were the result of an accident during Superman flying tests. "We noticed the material lit up on its own," Donner explained. "We tore the material into tiny pieces and glued it on the costumes, designing a front projection effect for each camera. There was a little light on each camera, and it would project into a mirror, bounce out in front of the lens, hit the costume,   millions of little glass heads would light up and bring the image back into the camera." 

===Music===
Jerry Goldsmith, who scored Donners The Omen, was originally set to compose Superman. Portions of Goldsmiths work from Capricorn One were used in Supermans teaser trailer. He dropped out over scheduling conflicts and John Williams was hired. Williams conducted the London Symphony Orchestra to record the soundtrack.  The music was one of the last pieces to come into place. Williams liked that the film did not take itself too seriously and that it had a theatrical camp feel to it. Ilya Salkind, Pierre Spengler, DVD audio commentary, 2006, Warner Home Video  Kidder was supposed to sing "Can You Read My Mind?," the lyrics to which were written by Leslie Bricusse, but Donner disliked it and changed it to a composition accompanied by a voiceover.  Maureen McGovern eventually recorded the single, "Can You Read My Mind?," in 1979, although the song did not appear on the film soundtrack.  It became a mid-chart hit on the Billboard Hot 100|Billboard Hot 100 that year (#52), and peaked at number five on the U.S. Adult Contemporary chart, as well as making lesser appearances on the corresponding Canadian charts.  It also was a very minor hit on the U.S. Country chart, reaching #93.

====2000 Rhino Complete Album====
{{Tracklist
| collapsed = yes
| headline  = Disc One
| title1    = Prelude and Main Title March*
| length1   = 5:29
| title2    = The Planet Krypton*
| length2   = 6:39
| title3    = Destruction of Krypton*
| length3   = 7:52
| title4    = Star Ship Escapes†
| length4   = 2:21
| title5    = The Trip to Earth
| length5   = 2:28
| title6    = Growing Up*
| length6   = 2:34
| title7    = Death of Jonathan Kent†
| length7   = 3:27
| title8    = Leaving Home
| length8   = 4:49
| title9    = The Fortress of Solitude*
| length9   = 9:17
| title10   = Welcome to Metropolis†
| length10  = 2:11
| title11   = Lex Luthors Lair*
| length11  = 4:48
| title12   = The Big Rescue†
| length12  = 5:55
| title13   = Super Crime Fighter*
| length13  = 3:20
| title14   = Super Rescues*
| length14  = 2:14
| title15   = Luthors Luau (Source)†
| length15  = 2:48
| title16   = The Planet Krypton (Alternate)*
| length16  = 4:24
| title17   = Main Title March (Alternate)
| length17  = 4:38
}}

==Themes==
 
  campy approach. Richard Donner, Tom Mankiewicz, DVD audio commentary, 2001, Warner Home Video 

In each of the three acts, the mythic status of Superman is enhanced by events that recall the heros journey (or monomyth) as described by Joseph Campbell. Each act has a discernible cycle of "call" and journey. The journey is from Krypton to Earth in the first act, from Smallville to the Fortress of Solitude in the second act, and then from Metropolis to the whole world in the third act. 
 Christian references to discuss the themes of Superman.   Mankiewicz deliberately fostered analogies with Jor-El (El (god)|God) and Superman|Kal-El (Jesus).  Donner is somewhat skeptical of Mankiewicz actions, joking "I got enough death threats because of that." 
 Biblical comparisons. the casting Jonathan and the Virgin Mary. 
 the fall, resurrection and battle with evil. Another vision was that of The Creation of Adam. 

The Christian imagery in the Reeve films has provoked comment on the Jewish origins of Superman. Rabbi  ."       Ironically, it is also in the Reeve films that Clark Kents persona has the greatest resemblance to Woody Allen, though his conscious model was  .  
 His romance altering human history, time traveling to save her from dying. Superman instead takes the advice of Jonathan Kent, his father on Earth. 

==Release==
Superman was originally scheduled to be released in June 1978, the 40th anniversary of Action Comics 1 which first introduced Superman, but the problems during filming pushed the film back by six months. Editor Stuart Baird reflected, "Filming was finished in October 1978 and it is a miracle we had the film released two months later. Big-budgeted films today tend to take six to eight months."  Donner, for his part, wished that hed "had another six months; I would have perfected a lot of things. But at some point, youve gotta turn the picture over."  Warner Bros. spent $7 million to market and promote the film.   
 Kennedy Center in Washington, D.C. on December 10, 1978. Three days later, on 13 December, it had a European Royal Charity Premiere at the Empire, Leicester Square in London in the presence of HM Queen Elizabeth II and Prince Andrew. It went on to gross $134.21 million in North America and $166 million internationally, totaling $300.21 million worldwide.  Superman was the second 1978 in film|highest-grossing film of 1978 (behind only Grease (film)|Grease), and became the sixth-highest grossing film of all time after its theatrical run. It was also Warner Bros. most successful film at the time.  Based on 53 reviews collected by Rotten Tomatoes, 93% of reviewers enjoyed Superman, with the consensus "Superman deftly blends humor and gravitas, taking advantage of the perfectly cast Reeve to craft a loving, nostalgic tribute to an American pop culture icon."  By comparison, Metacritic collected an average score of 88, resulting in "universal acclaim", based on 12 reviews.  The film was widely regarded as one of top 10 films of 1978.     Superman creators Jerry Siegel and Joe Shuster gave a positive reaction.  Shuster was "delighted to see Superman on the screen. I got chills. Chris Reeve has just the right touch of humor. He really is Superman". 

  believed "theres no doubt that its a flawed movie, but its one of the most wonderfully entertaining flawed movies made during the 1970s. Its exactly what comic book fans hoped it would be. Perhaps most heartening of all, however, is the message at the end of the credits announcing the impending arrival of Superman II."  Harry Knowles is a longtime fan of the film, but was critical of elements that didnt represent the Superman stories as seen in the comics.  Dave Kehr felt "the tone, style, and point of view change almost from shot to shot. This is the definitive corporate film. It is best when it takes itself seriously, worst when it takes the easy way out in giggly camp, When Lex Luthor enters the action, Hackman plays the arch-villain like a hairdresser left over from a TV skit."  Neal Gabler similarly felt that the film focused too much on shallow comedy. He also argued that the film should have adhered more to the spirit of Mario Puzos original script, and referred to the first three Superman films collectively as "simply puffed-up TV episodes". 

===Beyond theatrical release===
The Salkinds prepared a three-hour-plus version for worldwide television reincorporating some 45 minutes of footage and music deleted from the theatrical cut, and specially prepared so that networks and stations can re-edit their own version at their discretion. American Broadcasting Company aired the broadcast television debut of Superman in 1982, with a majority of the unused footage. A syndicated version of the film aired in local television stations in Los Angeles, California and Washington, D.C. in the 1990s included two additional scenes never seen before, in addition to what had been previously reinstated.    When Michael Thau and Warner Home Video started working on a film restoration in 2000, some of the extra footage was not added because of poor visual effects. Thau felt "the pace of the films storyline would be adversely affected. This included timing problems with John Williams musical score. The cut of the movie shown on TV was put together to make the movie longer when shown on TV because ABC paid per minute to air the movie. The special edition cut is designed for the best viewing experience in the true spirit of movie making."  There was a special test screening of the Special Edition in 2001 in Austin, Texas, on March 23 with plans for a possible wider theatrical release later that year, which did not occur.  In May 2001, Warner Home Video released the special edition on DVD.  Director Donner also assisted, working slightly over a year on the project. The release included making-of documentaries directed by Thau and eight minutes of restored footage. 

Thau explained, "I worked on   release  and Blu-ray Disc|Blu-ray.  Also available (with other films) is the nine-disc "Christopher Reeve Superman Collection"  and the 14-disc "Superman Ultimate Collectors Edition". 

===Broadcast television version details===
When the rights to the first Superman film reverted to the Salkinds in 1981 in television|1981, it was their intention to prepare a television cut longer than what was released theatrically, for the aforementioned reasons. The so-called "Salkind International Extended Cut", which ended up running 3 hours, 8 minutes, was shown internationally on television, and it is from this cut that later domestic TV versions were derived.

The first network American television broadcast of Superman: The Movie took place in February   was falling from the helicopter (the picture froze, creating a cliffhanger-type of ending for part one ). The next evening, there was naturally a recap before the film continued.  This expanded version was repeated in November of the same year,  only this time, shown in one night.  The next two ABC showings after that were the original theatrical version. Apparently, in their contract with ABC, the Salkinds were able to get money for every minute of footage shown on TV. So as a result, they crammed in as much footage as possible for the TV networks in order to maximize their revenues. During production of the film, Alexander and Ilya had been relegated to having to sell more and more of their rights back to Warner Bros. in exchange for financial help. Director Richard Donner was not consulted  on any of the extended versions. However, due to a clause in his contract, Donners name remains in the credits.

Also as previously mentioned, some 40 minutes of footage were reinstated for the initial ABC-TV telecasts of the film.  Among the highlighted moments:

*A subplot of an Executioner (a Kryptonian security officer) being sent by the council to hunt down and capture Jor-El (while the beginning of the scene is shown in the 2000 directors cut restoration, the "payoff"   is not in the latter version).

*When Superman is trying to get to Lex Luthors underground hideout, he is subjected to machine gun fire, a giant blow torch, and is frozen in ice. A tiny fraction of this footage was used in the theatrical version Superman II (directed by Richard Lester), in the scene where Supermans powers are stripped away by the molecule chamber in the Fortress of Solitude.

*Lex Luthor plays the piano in several scenes; when he has Otis drop Eve Teschmacher into the lions den at the end he is singing "You Must Have Been a Beautiful Baby".

*After Superman saves Lois at the end and flies off, hes seen rescuing Eve Teschmacher from the lions den, in which Lex had previously dropped her; he pointedly notes for Luthor to hear, "By the way, Miss Teschmacher, your mother sends her love," a reference to Luthors boast about Hackensack, NJ.
 father seen in the train. Also in the ABC version, #Cast|Otis walk down the street is longer.

*Nearly all of John Williams score is restored (some of which was dialed out of the theatrical cut).

However, at least one noticeable removal occurred: the recording of "Rock Around the Clock" by Bill Haley & His Comets, heard in the original film in the minutes before the death of Glenn Fords character, is replaced with a generic piece of instrumental music in the ABC cut.
 1988 (following a play-out run on pay cable  ) TV stations were offered the extended cut or the theatrical cut. The stations that showed the extended cut  edited the second half to squeeze in commercials and What happened yesterday flashbacks.
 1994 (following Los Angeles WJLA Channel 7, an ABC affiliate out of Washington, D.C. aired the "Salkind International Extended Cut" on Saturday, July 27, 1994. Part one aired from 9:30 p.m. to 11:30 p.m. before breaking for 30 minutes of news. Part two was then aired from 12:00 a.m. to 2:00 a.m.

The extended version of Superman: The Movie has never been broadcast in the United Kingdom. The first showing of the theatrical version on UK television appeared on 4 January 1983 on ITV (TV network)|ITV. In 1985 in television|1985, Irelands RTE television aired the extended versions of Superman: The Movie and Superman II in one night. The films ran from roughly 3:00 until 9:00 including the odd commercial and a break for the 6:00 news.
 mono sound mix done as by the time the extended cut was prepared in 1981, stereo was not available in television broadcasts. Eight of the 45 minutes of extended scenes that were used in the later 2000 directors cut restoration were taken from restored elements.

There are various extended TV versions each broadcast in various countries. Most of these are in pan and scan, as they were made in the 1980s, when films were not letterboxed to preserve the theatrical aspect ratio on old TVs. None of the various extended versions have ever been made available officially on home video/DVD, although they have been widely bootlegged. 

==Legacy== Best Film Best Music Best Sound John Barry and cinematographer Geoffrey Unsworth had not been recognized by the Academy of Motion Picture Arts and Sciences|Academy. 
 Best Newcomer, Best Science greatest film 100 Years... 100 Cheers list, but didnt make it past the ballot.  In 2009, Entertainment Weekly ranked Superman 3rd on their list of The All-Time Coolest Heroes in Pop Culture. 

With the films success, it was immediately decided to finish   (1987), were produced.   also was released in 2006. 

The films final sequence, which features Superman flying high above the Earth at sunrise, and breaking the fourth wall to smile briefly at the camera, featured at the end of every Superman film starring Reeve, and was re-shot with Brandon Routh for Superman Returns.
 Man of Steel in 2013.

Because Superman went into production prior to the releases of   (May 1977) and Close Encounters of the Third Kind (November 1977), some observers credit the three films collectively for launching the reemergence of a large market for science fiction films  in the 1980s. This is certainly the view of Superman producer Ilya Salkind and some who have interviewed him,   as well as of film production assistant Brad Lohan.  Other observers of film history tend to credit the resurgence of science fiction films simply to the Lucas and Spielberg productions, and see Superman as the first of the new cycle of films launched by the first two.  Ilya Salkind denies any connection between Superman—which began filming in March 1977—and the other films, stating that "I did not know about Star Wars; Star Wars did not know about Superman; Close Encounters did not know about Superman. It really was completely independent&nbsp;— nobody knew anything about anybody."  Superman also established the superhero film genre as viable outside the world of Saturday matinee serials, although it was a decade before the comparable success of the Batman series and two decades before that of X-Men and Spider-Man. 

Superman: The Movie inspired The Kinks 1979 song "(Wish I Could Fly Like) Superman", when the bands main songwriter, Ray Davies, watched the film in late 1978.   

American Film Institute Lists
*AFIs 100 Years...100 Heroes & Villains:
**Superman – #26 Hero
**Lex Luthor&nbsp;— Nominated Villain
*AFIs 100 Years...100 Songs:
**Can You Read My Mind&nbsp;— Nominated
*AFIs 100 Years...100 Movie Quotes:
**"Im here to fight for truth, justice, and the American way." – Nominated
*AFIs 100 Years of Film Scores – Nominated
*AFIs 100 Years...100 Cheers – Nominated
*AFIs 10 Top 10 – Nominated Fantasy Film
 Last Son" comic book story arc debuted in Action Comics, co-written by Superman director Richard Donner.

===Comic book continuity=== DC Comics universe:
* the crystallinity|crystalline-based technology of the planet Krypton

* Superman logo|Supermans "S" logo originates as the El family crest
* Ursa and Non—characters created specifically for the film—are imprisoned in the Phantom Zone with General Zod. 
* A computer-generated simulacrum of Jor-El survives in the Fortress of Solitude to advise his adult son Kal-El. 
* Clark Kent commences his public superhero career as the adult Superman, rather than the teenage Superboy (Kal-El)|Superboy. 
* Lois Lane first meets Superman when he rescues her as she falls from a disabled helicopter in Metropolis.   #3 (January 2010) 
* In one version of this encounter, the characters greet each other using the same dialogue from this scene in the film (Superman: "Easy, Miss, Ive got you."  Lois: "Youve got me ... whos got you?"). 
* Lois is the one who first names the hero "Superman". 
* Jonathan Kent dies of a heart attack, but Martha survives as his widow. 
* Although she is an excellent reporter, Lois frequently misspells words. 

==References==
 

;Cited works
* 

==External links==
 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
 
 
 
 
 
 
 
 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 