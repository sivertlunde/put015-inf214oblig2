Missile X – Geheimauftrag Neutronenbombe
{{Infobox film
| name           = Missile X - Geheimauftrag Neutronenbombe
| image          = 
| image_size     = 
| caption        = 
| director       = Leslie H. Martinson
| producer       = Ika Panajotovic
| writer         = Clarke Reynolds Elio Romano
| narrator       = 
| starring       = Peter Graves Curd Jürgens Michael Dante John Carradine
| music          = Alberto Baldan
| cinematography = Claudio Catozzo
| editing        = Antonio Jimeno
| distributor    =  
| released       = 1978
| runtime        = 97 min.
| country        = United States West Germany Spain Italy Iran
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Missile X – Geheimauftrag Neutronenbombe is a 1978 German/Italian/Spanish  , Iran in 1978 before the Iranian Revolution overthrew Irans Shah. Many locations in Iran, including the US Embassy in Tehran before the Iranian Hostage Crisis, appear in the film. It has been branded by some as an exploitation film due to the political nature of Iran before the 1979 Revolution.

==Plot==

The story concerns an experimental nuclear cruise missile which is stolen from a Soviet military site in the USSR. An international terrorist group, under the command of a European power-crazed man known only as the Baron is responsible. The Baron plots to use the stolen Soviet missile to destroy an international peace conference in one week located on an island in the Persian Gulf. When the U.S. counsul to Iran is murdered by the Barons henchmen, Alec Franklin, an US intelligence agent, is order to travel to Iran to take over as counsul as well as investigate the murder. Upon arrival in Tehran, Alec is followed by two of the Barons henchmen whom attempt to kill him, but Alec manages to escape.

Alec then travels from Tehran to Abadan where he meets Kronstein, a Russian intelligence agent who is in Iran searching for leads to locate the missing cruise missile, which leads to Alec and Konstantine joining forces along with Galina, another Russian agent, and Leila, an undercover Iranian policewoman, to investigate the Baron in order to find the location to where the cruise missile is being kept before it is used to start World War III.

==Cast==
 
*Peter Graves as Alec Franklin
*Curd Jürgens as The Baron
*Karin Schubert as Galina Fedorovna
*Michael Dante as Konstanine Senyonov
*Carmen G. Cervera as Nina
*Pouri Baneai as Leila
*Robert Avard Miller as Stetson
*Michael Tietz as Tony
*Mel Novak as Mendosa
*John Carradine as Professor Nikolaeff
 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 