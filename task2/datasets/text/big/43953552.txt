Bhagavan (1986 film)
{{Infobox film
| name           = Bhagavan
| image          =
| caption        = Baby
| producer       = KG Mohan Vijayan Pappanamkodu Lakshmanan (dialogues)
| screenplay     = Pappanamkodu Lakshmanan Menaka Madhuri Madhuri Meena Ganesh Premji
| music          = M. S. Viswanathan
| cinematography = Somendu Roy
| editing        = G Murali
| studio         = Trident Arts
| distributor    = Trident Arts
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, Baby and produced by KG Mohan. The film stars Menaka (actress)|Menaka, Madhuri (Tamil actress)|Madhuri, Meena Ganesh and Premji in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast== Menaka
*Madhuri Madhuri
*Meena Ganesh
*Premji Ramu
*Saleema
*Shanavas
*Sreenath

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Mailanchikkaram || S Janaki, Chorus || Poovachal Khader || 
|-
| 2 || Malarmaari madhumaari || Vani Jairam, Krishnachandran || Poovachal Khader || 
|-
| 3 || Vigrahamalla njaan ||  || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 