Sirf (film)
 
{{Infobox film name = Sirf image = Sirf.jpg caption = Dvd cover starring = Kay Kay Menon Manisha Koirala Ranvir Shorey Sonali Kulkarni director = Rajaatesh Nayar writer = Rajaatesh Nayar Shashikant Verma producer = Sanjay Kotadia Kanu Patel cinematography = Baba Azmi production designer = distributor = Eros International released =   runtime = language = Hindi
|music = Sohail Sen Shibani Kashyap| awards = budget =
}} China Gate, Pukar (2000 film)|Pukar and Lajja (film)|Lajja.

The movie revolves around four couples starring Manisha Koirala, Ranvir Shorey, Kay Kay Menon, Sonali Kulkarni, Parvin Dabbas, Rituparna Sengupta in the main lead.

==Plot==
Mumbai, the commercial capital of India, is a city that never sleeps. Rapidly changing at a feverish pace with every passing second, it is always on the move and so are the people living in it. In this story, we take an insight into the lives of the people living in this city and how the pace of the city affects their relationships and how they deal with it. No matter what happens life has to go on.

This is also a story of four couples and their lives in a metropolitan city. They belong to different social and financial strata and every one has got its own problems to deal with. What one couple has, the other doesnt? Each couple looks at the other couple and wishes that if only they had what the other couple does, their life would be a bed of roses, be it on a monetary level or on moral terms. What seems to be a major and almost insoluble problem for the first couple is just a petty issue for the second. Similarly, second couples serious problem is not looked upon as a problem at all by the first and likewise so, with the other two couples. In a pursuit to achieve what they lack and in a desperate search of that one thing that would solve all their problems, they find themselves lost in the ever moving crowd, where no one has time for themselves, leave aside the cares of the world.

In due course of time, all four couples come together with a distant vision of that one event which would end all their problems and set them free. Is it reality or just a figment of their vivid imagination?

==Characters==
Ankur Khanna and Nauheed Cyrusi as Rahul and Shalu:

Rahul and Shalu belong to the lower middle income group. For them, the biggest and most difficult task on earth is getting married and starting a new life together. What good is love without money?

Kay Kay Menon and Manisha Koirla as Gaurav and Devika:

Gaurav and Devika belong to the cream of high society. They have everything one could wish for. Still they are not happy as their relationship lacks something, which even they fail to figure out. What
good is money without love?

Parvin Dabbas and Rituparna Sengupta as Amit and Suchita:

Amit and Suchita are an upper middle class couple with no financial problems. Amit is at a good position in an advertising agency while Suchita is a housewife. They are a loving couple. But what good is love without faith?

Ranvir Shorey and Sonali Kulkarni as Akash and Namita:

Akash and Namita live in a suburb at a distance from their offices. They love each other and but are not willing to understand each other’s limitations. What good is love without understanding?

== Cast ==
* Kay Kay Menon as Gaurav 
* Manisha Koirala as Devika
* Ranvir Shorey as Akash
* Sonali Kulkarni as Namita
* Parvin Dabbas as Amit
* Rituparna Sengupta as Suchita
* Ankur Khanna as Rahul
* Nauheed Cyrusi as Shalu
* Kanisha Nayyar as Kittu

==Crew==
*Distributor: Eros International
*Production House: Seven Eagle Production
*Director: Rajaatesh Nayar
*Screenplay: Rajaatesh Nayyar & Shashikant Verma
*Cinematography: Baba Azmi
*Music Director: Sohail Sen & Shibani Kashyap
*Background Score: Sohail Sen

==Soundtrack==
{{Infobox album 
| Name        = Sirf
| Type        = Soundtrack
| Artist      = Sohail Sen and Shibani Kashyap
| Cover       =
| Released    = 2008
| Recorded    = 2005–2006  Feature film soundtrack
| Length      =  
| Label       = Sony Music
| Producer    = Sohail Sen
| Chronology  = Sohail Sen
| Last album  = 
| This album  = Sirf (2008)
| Next album  = Whats Your Raashee? (soundtrack)|Whats Your Raashee? (2009)
| Misc           = {{Extra chronology
| Artist         = Shibani Kashyap
| Type           = soundtrack
|  Last album  = Waisa Bhi Hota Hai Part II (2003)
|  This album  = Sirf (2008)
|  Next album  =Main Osama}}
}}
{{Album ratings
| rev1 = Bollywood Hungama
| rev1Score =   
}}
The soundtrack of the film is composed by Sohail Sen and Shibani Kashyap. The lyrics are penned by Mehboob and Vipul Saini. The album contains six original tracks and one reprise track.   

{{tracklist
| headline = Track listing
| extra_column = Artist(s)
| total_length = 41:45

| title1 = Pehla Woh Pyaar
| extra1 = Kunal Ganjawala
| length1 = 5:01
 
| title2 = Life Peeche Peeche
| extra2 = Shibani Kashyap
| length2 = 3:41

| title3 = Ghar Tera Ghar Mera
| extra3 = Shaan (singer)|Shaan, Shreya Ghoshal 
| length3 = 5:25

| title4 = Zindagi Ki Kahani
| extra4 = Kunal Ganjawala
| length4 = 5:37

| title5 = Tujhpe Fida
| extra5 =  Tarannum, Sohail Sen
| length5 = 5:43

| title6 = Khel Jo Khele
| extra6 = Vinod Rathod
| length6 = 5:12

| title7 = Mumbai Nagariya 
| extra7 = Krishnakumar Kunnath|KK, Tarannum
| length7 = 5:33

| title8 = Zindagi Ki Kahani
| extra8 = Kunal Ganjawala, Pamela Jain
| length8 = 5:33
| note8 = II
}}

==References==
 

==External links==
*  

 
 
 
 