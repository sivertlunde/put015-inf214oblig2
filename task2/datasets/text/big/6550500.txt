Shyamol Chhaya
{{Infobox film
| name           = Shyamol Chhaya
| image          = ShyamolChhaya.jpg
| image_size     = 200px
| caption        = Shyamol Chhaya films DVD Cover
| director       = Humayun Ahmed
| producer       =Impress Tlefilm Ltd.
| writer         = Humayun Ahmed
| narrator       =  Riaz Shadhin Khasru Faruk
| music          = oksud Jamil Mintu Anwar Hossain
| editing        = Atiqur Rahman Mollick
| distributor    = Impress Tlefilm Ltd.
| released       = 16 December 2004
| runtime        = 110 Min.
| country        = Bangladesh Bengali
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Shyamol Chhaya also ( ) is a Bangladeshi Bengali language Film. Released was 16 December 2004. Written and directed by Humayun Ahmed. Stars Shaon, Riaz (Actor)|Riaz, Shadhin Khasru, Faruk and many more. It reflects a story of the Bangladesh War of Independence, 1971.

Shyamol Chhaya has been nominated to compete in Best Foreign Language Film category in the 78th Oscar, 2006. Announced by Bangladesh Federation of Film Societies (BFFS). 

==Synopsis==
Its a story of boat journey. Monsoon, 1971 of helpless people was going towards the liberated zone. A horrific situation was prevailing in Bangladesh. Pakistan military had unleashed genocide in the country. Heroic sons of the soil started a war of liberation- Passengers of the boat were terribly frightened but they had dreams of war of Shaymol Chhaya - the land of people of peace. Would they be able to reach Shaymol Chhaya?

==Cast==
*Humayun Faridi as
*Shimul as
*Shaon as Riaz as
*Swadhin as (Shadhin)
*Syed Akhtar Ali as
*Tania Ahmed as
*Ahmed Rubel as
*Ejazul Islam as (Ejajul Islam)
*Faruque Ahmed
*Shamima Nazneen
*Jesmine Parvez as (Jesmin Parvez

==Crew==
* Producer: Impress Telefilm Ltd.
* Story: Humayun Ahmed
* Screenplay: Humayun Ahmed
* Director: Humayun Ahmed
* Cinematography: Anowar Hossain
* Editing: Atiqur Rahman Mollick
* Music: Moksud Jamil Mintu
* Sound: Mafizil Haque
* Set: Mohiuddin Faruque, Ezazul Islam and Abdul Quader

==Technical details==
* Format: 35 MM (Color)
* Running Time: 110 Minutes Bengali
* English
* Country of Origin: Bangladesh
* Date of Theatrical Release: 16 December 2004
* Year of the Product: 2004
* Technical Support: Bangladesh Film Development Corporation (BFDC)

==Participation at the festivals==
* 6th Bangladesh Film Festival 2005. London
* 11th Kolkata Film Festival 2005. India
* International Palm Spring Film Festival 2006. USA
* The 32nd Seattle International Film Festival 2006. USA
* FIBOFEST Film Festival, Prague, 2006. Czech Republic 
* East End Film Festival 2006. London
* East London Film Festival 2006. London
* South Asian Film Festival 2009. GOA, India.
* Film Festival Featuring Liberation War Kolkata 2010. India 

==Awards and achievement==

===6th Bangladesh Film Festival===
* Winner Best Contemporary Film - 2005. London

===Bachosach Film Awards===
* Winner Best supporting Actress - Tania Ahmed

==International achievement==
Shyamol Chhaya has been nominated to compete in Best Foreign Language Film category in the 78th Oscar, 2006. Announced by Bangladesh Federation of Film Societies (BFFS).
 78th Oscar Awards, 2006.

==Home media==
Shyamol Chhaya films VCD and DVD copy rights take was G-Series.

==References==
 

==External links==
* 
* 
*  at The  

 
 
 
 
 
 
 
 
 