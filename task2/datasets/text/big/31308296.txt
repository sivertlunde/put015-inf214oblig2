Lady in Danger
 
{{Infobox film
| name           = Lady in Danger
| image          = "Lady_in_Danger".jpg
| image_size     = 
| caption        = Original Trade Ad Poster
| director       = Tom Walls
| producer       = Michael Balcon
| writer         = Marjorie Gaffney Ben Travers 
| narrator       = 
| starring       = Tom Walls Yvonne Arnaud
| music          = Jack Beaver
| cinematography = Philip Tannura
| editing        = Helen Lewis
| studio         = Gaumont British
| distributor    = Gaumont British Distributors (UK)
| released       = 27 November 1934 (London) (UK)
| runtime        = 68 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British comedy comedy thriller film directed by Tom Walls and starring Walls, Yvonne Arnaud and Anne Grey.  The screenplay was by Ben Travers.

==Plot==
British businessman Richard Dexter (Walls) is persuaded to escort the queen of a revolution-torn European country to the safety of England via his aeroplane. Once there his relations with the Queen (Yvonne Arnaud) are farcically misconstrued, when his fiancée Lydia (Anne Gray) arrives unannounced. After many adventures, the King (Hugh Wakefield), who has fled to Paris, is reunited with his wife. 

==Cast==
* Tom Walls - Richard Dexter
* Yvonne Arnaud - Queen of Ardenberg
* Anne Grey - Lydia
* Leon M. Lion - Dittling
* Hugh Wakefield - King
* Marie Lohr - Lady Brockley
* Alfred Drayton - Quill
* Leonora Corbett - Marcelle
* O.B. Clarence -Nelson
* Cecil Parker - Piker
* Harold Warrender - Clive
* Hubert Harben - Matterby
* Charles Lefeaux - Hotel Manager
* Dorothy Galbraith - Mrs. Quill
* Jane Cornell - Shop Assistant
* Mervyn Johns - Reporter

==Critical reception==
TV Guide described the film  as "a vague comedy that refuses to commit itself to a romance between the leads" ; while Allmovie called it an "airy comedy-melodrama...the farcical possibilities of Lady in Danger are played to the hilt, and the rest is good semi-clean fun."   

==References==
 

==Bibliography==
* Sutton, David R. A chorus of raspberries: British film comedy 1929-1939. University of Exeter Press, 2000.

==External links==
* 

 

 
 
 
 
 
 


 