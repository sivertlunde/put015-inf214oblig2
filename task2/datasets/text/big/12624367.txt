Sweet Liberty
{{Infobox film
| name           = Sweet Liberty
| image          = Sweetlibertyposter.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Alan Alda
| producer       = Martin Bregman
| writer         = Alan Alda
| narrator       =
| starring       = {{plainlist|
* Alan Alda
* Michael Caine
* Michelle Pfeiffer
* Bob Hoskins
* Lise Hilboldt
* Lillian Gish
}}
| music          = Bruce Broughton
| cinematography = Frank Tidy
| editing        = Michael Economou	
| distributor    = Universal Pictures 1986
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $14,205,021
| preceded by    =
| followed by    =
}}

Sweet Liberty (1986) is an American comedy film written and directed by Alan Alda, and starring Alda in the lead role, alongside Michael Caine and Michelle Pfeiffer, with support from Bob Hoskins, Lois Chiles, Lise Hilboldt, Lillian Gish, and Larry Shue.

It was the next-to-last film for Gish, whose first appearance on screen came in 1912.

==Plot==
College history professor Michael Burgess (Alan Alda) is about to have his fact-based historical novel about The American Revolution turned into a Hollywood motion picture being filmed in the North Carolina town where he lives. 
 Method actress Faith Healy (Michelle Pfeiffer).

The excitement of having show-business people in town is short-lived when Michael becomes increasingly exasperated seeing his novel get mauled beyond all recognition by a low-brow scriptwriter (Bob Hoskins) and a condescending director (Saul Rubinek). They want a Hollywood version of history, complete with rebellion against authority, violence, nudity and a total distortion of the truth.
 character she is portraying in the film.

Gretchen turns the tables, becoming receptive to the advances of Elliott James. The married actor is a swordsman in many ways, not only flirting with Gretchen and the Mayors wife (Lois Chiles) but humiliating Michael repeatedly in bouts of fencing. Elliotts wife (Linda Thorson) also turns up, complicating matters further.

Faith turns out to not be what she seems to be, merely behaving the way she does to get into character. Michael becomes fed up with all the Hollywood tomfoolery. When the men from a local Revolutionary War reenactor company who were supposed to participate in a scene are subject to bullying and mockery from the films crew, Michael persuades them to get back at their tormentors. He ends up deliberately sabotaging his own film.

The locals cause explosions during a horribly inaccurate recreation of the Battle of Cowpens. Michael throws the arrogant directors own words back at him, that he is providing: (1.) Rebellion against authority, by Michael and the reenactors refusal to do as ordered in battle; (2.) Violence, by blowing up a house before the director is ready, and (3.) Nudity, when all the men celebrate their onscreen victory by prancing around naked. 

By the time the films premiere is held in town, everything is pretty much back to normal for Michael, who comes to the premiere with Gretchen, who is pregnant. Michael can only respond with a strained look when he gets asked by a Hollywood correspondent how it feels to see history come alive.

==Cast==
* Alan Alda as Michael Burgess
* Michael Caine as Elliott James
* Michelle Pfeiffer as Faith Healy
* Bob Hoskins as Stanley Gould
* Lise Hilboldt as Gretchen Carlsen
* Lillian Gish as Cecelia Burgess
* Saul Rubinek as Bo Hodges
* Lois Chiles as Leslie
* Linda Thorson as Grace James

==Critical reception==
Sweet Liberty has a rating of 75% on Rotten Tomatoes, based on 12 critics reviews. 

The general consensus of critics was that the film lacked the satirical bite that might have been expected from a story about the Hollywood movie industry. Vincent Canby in the New York Times called it a "mildly satiric comedy so toothless it wouldnt even offend a mogul as sensitive and publicly pious as Louis B. Mayer," and sympathised with the actors as "severely limited by material that doesnt go anywhere."     
 Time Out described the film as "nearly as dull as it sounds, intermittently enlivened only by Hoskins and Caine, the latter effortlessly amusing as the productions leading man."  Variety (magazine)|Variety wrote that "comedic potential is too rarely realized." 
Roger Ebert in the Chicago Sun-Times thought the film tried to "juggle a lot of characters all at once" and lamented that there was "more material than there was time to deal with it."   

The majority of critical praise was reserved for the lead actors. Michael Caine was described variously as an "excellent comic actor,"  "the kind of charming cad you can never really hate for too long,"    and "such an accomplished actor that all he has to do is behave with self-assured grace."  Michelle Pfeiffer was also highly acclaimed for her dual role of "wonderfully subtle touches,"  described as getting "a chance to show that she has the potential to be a first-rate comedienne,"  and as the actress who "neatly tucks the movie into her bodice and saunters off with it." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 