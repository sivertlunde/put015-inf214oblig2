BattleGround: 21 Days on the Empire's Edge
{{Infobox Film | name =BattleGround: 21 Days on the Empires Edge
 | image =
 | caption = Stephen Marshall
 | producer = Lisa Kawamoto Hsu Jeff Hull Ian Inaba Bob Jason Robert Kravitz Anthony Lappé Joshua Shore Stephen Marshall
 | starring =Sgt. Robert Hollis Rana al Aiouby Farhan al Bayati Hesham Barbary Raed Jarrar Col. Fred Rudesheim Lt. Col. Nate Sassaman May Ying Welsh
 | music =Soulsavers
 | cinematography =Stephen Marshall
 | editing = Leo Cullen Stephen Marshall
 | distributor = Guerrilla News Network Artists/Media Cooperation (Co.Op)
 | released = October 14, 2004
 | runtime = 82 minutes
 | country = United States
 | language = English
}}
BattleGround: 21 Days on the Empires Edge was released in 2004, and received the Silver Hugo Award for documentaries at the 2004 Chicago International Film Festival.  It aired on Showtime and was released on DVD by Home Vision. The film follows the story of Frank al-Bayati, a former Shiite guerrilla traveling back to Iraq for the first time since the 1991 uprising against Saddam Hussein. Al-Bayati was wounded, captured, tortured and then escaped. He spent more than a year in a Saudi Arabian refugee camp before being repatriated to the U.S. Lappé and Marshall follow al-Bayati as he tracks down his family members and capture the emotional reunions. Al-Bayatis optimism for what he calls "liberated Iraq" is countered by the reality the filmmakers find on the ground. A growing insurgency is creating more enemies than it is killing. With candid interviews with top American commanders, the filmmakers capture the U.S. militarys inability to grasp the nature of their enemy. In addition, Lappé and Marshall bring a Geiger counter and conduct their own radiation tests on Iraqi armor that has been hit by American shells. They find evidence of the use of depleted uranium, the controversial radioactive metal used in some American munitions.

The film was directed by Stephen Marshall, and produced by Anthony Lappé and Lisa Hsu.

==See also==
*Meeting Resistance

==References==
 

==External links==
*   
*  
*  

 
 
 
 
 
 
 
 


 