Keep the Aspidistra Flying (film)
{{Infobox film name            = Keep the Aspidistra Flying image           =  caption         =  director        = Robert Bierman producer        = Peter Shaw writer          = Alan Plater George Orwell (novel) starring        = Richard E. Grant Helena Bonham Carter music           = Mike Batt cinematography  = Giles Nuttgens editing  Bill Wright distributor     = BBC Films First Independent Films Lions Gate Films released        = November 21, 1997 runtime         = 101 minutes country         = United Kingdom language        = English budget          = gross           = $373,830 
|}}
 the eponymous novel by George Orwell. The screenplay was written by Alan Plater and was produced by Peter Shaw. The film stars Richard E. Grant as Gordon Comstock and Helena Bonham Carter as Rosemary.

==Plot==
Gordon Comstock (Grant), is a successful copywriter at a flourishing advertising firm in 1930s London. His girlfriend and co-worker, Rosemary (Bonham Carter), fears he may never settle down with her when he suddenly disavows his money-based lifestyle and quits his job for the artistic satisfaction of writing poetry.

==Cast==
* Richard E. Grant as Gordon Comstock
* Helena Bonham Carter as Rosemary
* Julian Wadham as Ravelston Jim Carter as Erskine
* Harriet Walter as Julia Comstock
* Lesley Vickerage as Hermione
* Barbara Leigh-Hunt as Mrs. Wisbeach (credited as Barbara Leigh Hunt) Liz Smith as Mrs. Meakin
* Dorothy Atkinson as Dora John Clegg as McKechnie

==References==
 

==External links==
* 
 
 
 
 