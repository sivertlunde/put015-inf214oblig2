Right Yaaa Wrong
{{Infobox film
| name = Right Yaaa Wrong
| image = RightYaaWrong.jpg
| alt =  
| caption = Theatrical release poster
| director = Neeraj Pathak
| producer = Neeraj Pathak Krishan Choudhery Sanjay Chauhan
| story = Neeraj Pathak
| starring = Sunny Deol Irrfan Khan Konkona Sen Sharma Isha Koppikar Aryan Vaid
| music = Monty Sharma
| cinematography = Ravi Walia
| editing = Ashfaque Makrani
| studio =
| distributor = Ikkon Pictures Pvt. Ltd. Mukta Arts Ltd.
| released =  
| runtime =
| country = India
| language = Hindi
| budget =
| gross =
}}
Right Yaaa Wrong is a 2010 Bollywood film directed by Neeraj Pathak, starring Sunny Deol and Irrfan Khan in the lead roles. The film is based on the lives of two  police officers of India, who later become strong rivals. It also stars Isha Koppikar, Konkona Sen Sharma, Aryan Vaid and Govind Namdeo. This film was shot in Film City and released on 12 March 2010, under the banner of Eros Labs|Eros.  The film has almost similar plot points to the 1995 Hollywood film Above Suspicion (1995 film)   

==Plot==
Right Yaaa Wrong is a story of two cops, Ajay (Sunny Deol) and Vinay (Irrfan Khan), where an intense rivalry leads them on a battle for supremacy.

Ajay and Vinay are best friends. Ajays wife (Isha Koppikar) is found brutally murdered. Ajay is the prime suspect and Vinay is handed the case. Unsure what to do, either help his best friend or achieve a higher position in his job, Vinay goes against Ajay. By this time, Radhika (Konkana Sen Sharma), Vinays younger sister, becomes Ajays lawyer, and is Ajays only support.

The investigation transpires into a mystery locked with secrets. Behind the secrets lies an astonishing discovery. Ripples begin with wits and mind games. Every time hatching a clue, Vinay tries to beat Ajay, but falls back when Ajay has a reply to all of his questions. This is the story of a strong friendship, which is broken by leadership and forgotten. After all Vinay has done as a detective, the truth is finally revealed. However, did Ajay really murder his own wife? Was Vinay true all along, yet why did no one believe him? And will Vinay prove his point... or once again be friends with Ajay? 

==Cast==
* Sunny Deol as ACP Ajay Shridhar
* Irrfan Khan as Inspector Vinay Patnaik
* Eesha Koppikhar as AnshitaAnshu A. Sridhar
* Aryan Vaid as Boris
* Konkona Sen Sharma as Radhika Patnaik
* Arav Chowdhary as Sanjay Sridhar
* Deepal Shaw as Inspector Shalini
* Govind Namdeo as a Public Prosecutor Nigam
* Master Ali Haji as Yash A. Sridhar
* Suhasini Mulay as a Judge
* Kamlesh Sawant as Sawant
* Ali Khan as Mr. Dacosta
* Parikshat Sahni as a Doctor
* Nilanjana Bhattacharya
* Surendra Rajan as a Security Guard
* Shilpi Sharma in a Special Appearance
* Vijay Patkar as Police Inspector
*Satish Kashyap as Police Inspector

==Critical reception== IBN have panned the movie,  while Nikhat Khazmi of Times of India and Taran Adarsh of DNA rated it above average.    The film received an overall rating of 5/10.  As of 28 March 2010, it was the surprise hit of the year even though IPL was going on.

==References==
 
As per Box Office India the film was a Disaster . It grossed only 3.12 Core. The First Week occupancy was only 25% . 

==External links==
*  
*   - ReviewGang

 
 
 