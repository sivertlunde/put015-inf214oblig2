Terry and the Pirates (serial)
{{Infobox film
| name           = Terry and the Pirates
| image          = Terry_and_the_Pirates-1.jpg
| image_size     =
| caption        =
| director       = James W. Horne
| producer       = Larry Darmour George Morgan Terry and the Pirates created by Milton Caniff
| narrator       = Granville Owen Joyce Bryant Allen Jung Victor DeCamp Sheila Darcy Dick Curtis
| music          = Lee Zahler
| cinematography = James S. Brown, Jr. Earl Turner
| distributor    = Columbia Pictures
| released       =  
| runtime        = 15 chapters (300 min)
| country        = United States English
| budget         =
| gross          =
}} film serial Terry and the Pirates created by Milton Caniff. In his biography, Meanwhile..., Caniff stated that he hated the serial for changing so much of his comic strip, and that "I saw the first chapter and walked out screaming."

==Plot==
Young explorer Terry Lee and his grown-up sidekick, Pat Ryan, arrive in the Asian jungles in search of Terrys father, Dr. Herbert Lee.  The elder Lee is an archaeologist and leader of a scientific expedition seeking evidence of a lost civilization. Soon Terry discovers his father has been kidnapped by an armed pirate gang known as the Tiger Men. The gang is led by the evil Master Fang, a local warlord who controls half of the natives and holds the white settlers in fear.  Fang is seeking the riches hidden beneath the Sacred Temple of Mara. Terry meets the Dragon Lady, who is determined her kingdom shall not be invaded. Attacked by Fang, his henchman Stanton and the Tiger Men, Terry and Pat try valiantly to locate the missing Dr. Lee, uncover the secrets of the lost civilization, and recover the hidden treasure of Mara. After joining forces with Connie, Normandie and the Dragon Lady, the heroes have myriad varied adventures in the inhospitable environment.

==Cast==
* William Tracy as Terry Lee Granville Owen as Pat Ryan 
* Joyce Bryant as Normandie Drake
* Allen Jung as Connie  Victor DeCamp as Big Stoop, magician
* Sheila DArcy as Dragon Lady 
* Dick Curtis as Fang 
* J. Paul Jones as Dr. Lee
* Forrest Taylor as Mr. Drake Jack Ingram as Stanton Charles King as Blackie, henchman
* Duke York as Leopard Man
* Jack Perrin as Mr. Harris

==Chapter titles==
# Into the Great Unknown
# The Fang Strikes
# The Mountain of Death
# The Dragon Queen Threatens
# At the Mercy of the Mob
# The Scroll of Wealth
# Angry Waters
# The Tomb of Peril
# Jungle Hurricane
# Too Many Enemies
# Walls of Doom
# No Escape
# The Fatal Mistake
# Pyre of Death
# The Secret of the Temple
 Source: {{cite book
 | last = Cline
 | first = William C
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc
 | isbn = 0-7864-0471-X
 | pages = 
 | chapter = Filmography
 }} 

==See also== List of film serials by year
*List of film serials by studio

==References==
 

==External links==
*  
*  
*   at Cinefania.com

 
{{succession box  Columbia Serial Serial  The Shadow (1940)
| years=Terry and the Pirates (1940) Deadwood Dick (1940)}}
 

 
 

 
 
 
 
 
 
 
 