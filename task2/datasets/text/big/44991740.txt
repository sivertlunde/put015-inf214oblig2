Sri Kanyaka Parameshwari Kathe
{{Infobox film 
| name           = Sri Kanyaka Parameshwari Kathe
| image          =  
| caption        = 
| director       = Hunsur Krishnamurthy
| producer       = Hunsur Krishnamurthy
| writer         = 
| screenplay     = Hunsur Krishnamurthy Rajkumar B. M. Venkatesh Nagendra Rao H. R. Shastry
| music          = Rajan-Nagendra
| cinematography = D V Rajaram
| editing        = P N Murthy
| studio         = Evergreen Productions
| distributor    = Evergreen Productions
| released       =  
| country        = India Kannada
}}
 1966 Cinema Indian Kannada Kannada film, directed by Hunsur Krishnamurthy and produced by Hunsur Krishnamurthy. The film stars Rajkumar (actor)|Rajkumar, B. M. Venkatesh, Nagendra Rao and H. R. Shastry in lead roles. The film had musical score by Rajan-Nagendra.  

==Cast==
  Rajkumar
*B. M. Venkatesh
*Nagendra Rao
*H. R. Shastry Narasimharaju
*Subbanna
*Vasu
*Dinesh
*Shankar
*Dwarakish
*K. S. Ashwath
*H. K. Shastry
*Basappa
*Master Basavaraju
*Guptha
*Ranganath
*Raghu
*Pandari Bai
*Kalpana
*B. Jayashree
*Ramadevi
*Sharada
*Rama
*Indira
 

==Soundtrack==
The music was composed by Rajan-Nagendra. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Nintalli Avalu Kulitalli Avalu || PB. Srinivas || Hunsur Krishna Murthy || 03.01
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 


 