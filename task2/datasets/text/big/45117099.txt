Dashavathara (film)
{{Infobox film 
| name           = Dashavathara
| image          =  
| caption        = 
| director       = P. G. Mohan
| producer       = B. S. Ranga
| writer         = 
| screenplay     = G. V. Iyer Rajkumar Udaykumar Narasimharaju
| music          = G. K. Venkatesh
| cinematography = B Dorairaj
| editing        = P G Mohan Devendranath
| studio         = Vikram Productions
| distributor    = Vikram Productions
| released       =  
| runtime        = 175 min
| country        = India Kannada
}}
 1960 Cinema Indian Kannada Kannada film, Narasimharaju in lead roles. The film had musical score by G. K. Venkatesh. 

==Cast==
  Rajkumar
*Udaykumar
*Rajashankar Narasimharaju
*K. S. Ashwath
*H. R. Shastry
*Eshwarappa
*Veerabhadrappa
*H. K. Shastry
*A. V. Subba Rao
*M. Bhagavathar
*Srikanth
*Kashinath
*Varadaraj
*Kuppuraj
*Rathnakar
*Rajendrakrishna
*Ganapathi Bhat
*Keshavamurthy
*R. Srinivasan
*Girimaji Leelavathi
*Adavani Lakshmidevi
*Rajasree
*B. Jayashree
*Saroja
*B. Jaya (actress)|B. Jaya
*Papamma
*Baby Suma
*Lakshmikantham
*Mala
 

==Soundtrack==
The music was composed by GK. Venkatesh. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Vaidehi Yenadalo || PB. Srinivas || GV. Iyer || 04.18
|}

==References==
 

==External links==
*  

 
 
 
 


 