Road to Victory (documentary)
 
 
{{Infobox film
| name           = Road to Victory: Milestones in the Struggle for Liberty
| image          = 
| image size     = 
| caption        = 
| director       = Ken G. Hall
| producer       = Ken G. Hall
| writer         = Tom Gurr
| based on       = 
| starring       = Harry Dearth
| music          = 
| cinematography = 
| editing        = 
| distributor    = RKO (Australia) 
| studio  = Cinesound Productions Department of Information
| released       = 17 January 1941   
| runtime        = 10 mins
| country        = Australia
| language       = English
| budget         = 
}}
Road to Victory: Milestones in the Struggle for Liberty is a 1941 Australian short documentary directed by Ken G. Hall.

It was made for the Department of Information in 1940.  It was the first Australian propaganda film for the war, closely followed by Australia Has Wings. 

==Plot==
The activities of Australia and Britain, in peace and war. 

==Release==
The film was released in 20 first run theatres in January 1941. 

The Adelaide Advertiser said that "this film is well prepared from material made available by many of the newsreel producers and, other film companies, and is a very interesting collection of the best photos taken in the past year or so. You will recognise much of it, you will find it all the more interesting because of the sequence and narrative. 

The Hobart Mercury wrote:
 The inherent drama of the many newsreel shots Is heightened by the excellently spoken commentary, and the film inspires a justifiable pride in the growing strength of the British Empire in its struggle against the totalitarian States. "The Road to Victory" shows Australia and Britain at peace, and compares this peace with the marching feet In other lands. Then war-and the film presents many thrilling pictures of air and sea battles. Several not-able British victories in the past 12 months are depicted. The film is expertly edited and is the first of a series of Australian short subjects to be Issued under the aegis of the Department of Information.  

==References==
 

==External links==
*  at IMDB
*  at National Film and Sound Archive
 

 
 
 
 
 
 
 
 