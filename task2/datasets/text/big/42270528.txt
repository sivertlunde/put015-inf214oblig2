De Pernas pro Ar 2
{{Infobox film
| name           = De Pernas pro Ar 2
| image          = De Pernas pro Ar 2 Poster.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Roberto Santucci
| producer       = 
| writer         = Mariza Leão
| starring       = Ingrid Guimarães  Bruno Garcia  Maria Paula  Eriberto Leão  Denise Weinberg  Cristina Pereira   Christine Fernandes
| music          = 
| cinematography = Nonato Estrela
| editing        = 
| studio         = Downtown Filmes  Globo Filmes
| distributor    = Paris Filmes
| released       =  
| runtime        = 115 minutes
| country        = Brazil
| language       = Portuguese  English
| budget         = BRL|R$ 6 million 
| gross          = $25,402,893 
}}

De Pernas pro Ar 2 is a 2012 Brazilian comedy film, directed by Roberto Santucci and written by Mariza Leão. It is a sequel of the 2010 film De Pernas pro Ar, starring Ingrid Guimarães, Bruno Garcia, Maria Paula, Eriberto Leão, Denise Weinberg, Cristina Pereira and Christine Fernandes. 

It was the first film broadcast via satellite to movie theaters in Brazil. On December 20, 2012 the film was broadcast simultaneously in two special sessions for special guests: one at the Cine Carioca in Rio de Janeiro, and another at the Cine Roxy, in Santos, São Paulo|Santos. 

==Plot==
Alice (Ingrid Guimarães) becomes a successful businesswoman, without leaving aside the sexual pleasure, she also continues to work much more. Shes quite busy due to the opening of the first branch of her sex shop in New York, alongside partner Marcela (Maria Paula). Her big goal is to bring to America an unreleased erotic product, which causes her to be quite stressed.

During the celebration for the 100th SexDelícia store in Brazil, Alice has an outbreak due to overwork. She is hospitalized at a spa operated by the rigid Regina (Alice Borges), where she meets several people who seek to control their obsessions and anxieties.

== Cast ==
 
* Ingrid Guimarães as Alice Segretto

* Bruno Garcia as João

* Maria Paula Fidalgo as Marcela

* Tatá Werneck as Juliana Tavares

* Eriberto Leão as Ricardo

* Denise Weinberg as Marion

* Cristina Pereira as Rosa

* Christine Fernandes as Vitória Prattes

* Eduardo Melo as Paulinho

* Alice Borges as Regina

* Luís Miranda as Mano Love

* Pia Manfroni as Valéria

* Wagner Santisteban as Leozinho

* Rodrigo SantAnna as Garçom Geraldo

* Edmilson Barros as Peão

* Dudu Sandroni as Dr. Rafael
 

== References ==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 