All Stars (2013 film)
{{Infobox film
| name           = All Stars
| image          =  
| film name      = 
| director       = Ben Gregor
| producer       = 
| writer         = Paul Gerstenberger
| screenplay     = 
| story          = 
| based on       =  
| starring       = Theo Stevenson Akai Osei-Mansfield Ashley Jensen Fleur Houdijk Dominic Herman-Day Amelia Clarkson
| narrator       = 
| music          = Simon Woodgate
| cinematography = Ben Wheeler
| editing        = Jono Griffith
| studio         = Vertigo Films
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
All Stars is a 2013 British film that was directed by Ben Gregor.  The movie was released in the United Kingdom on May 3, 2013, and stars Theo Stevenson as one of two children trying to save their youth centre.  

==Synopsis==
When their youth centre is threatened with closure, six children decide to come together and put on a dance show. Each has their own reason for performing aside from saving the youth centre. Jaden (Akai Osei-Mansfield) loves to dance but is forbidden to do so by his scholastically motivated parents. Ethan (Theo Stevenson) hopes to impress the girl of his dreams. Amy (Fleur Houdijk) holds a secret crush on Ethan and hopes to use this as an outlet for the emotions she has from taking care of her comatose father. Theyre helped along by Brian (Gamal Toseafa),  Tim (Dominic Herman-Day), and Rebecca (Amelia Clarkson), and they all hope to manage to save the centre and make their hopes come true.

==Cast==
*Theo Stevenson as Ethan Akai Osei-Mansfield as Jaden
*Ashley Jensen as Gina
*Fleur Houdijk as Amy
*Dominic Herman-Day as Tim
*Amelia Clarkson as Rebecca
*Gamal Toseafa as Brian
*Hanae Atkins as Lucy
*Summer Davies as Sadie
*Kimberley Walsh as Trish
*Kieran Lai as Kurt
*Ashley Walters as Mark
*Javine Hylton as Kelly
*Kevin Bishop as Andy
*Mark Heap as Simon Tarrington

==Reception==
Critical reception for All Stars has been predominantly negative and the movie holds a rating of 54% "rotten" on Rotten Tomatoes, based on 13 reviews.  Den of Geek panned the film and wrote "Very young kids might find some enjoyment in All Stars, but its corny, light-entertainment cinema at its most irritating." 

==References==
 

==External links==
*  
*  

 
 
 
 