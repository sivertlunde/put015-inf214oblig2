An Innocent Adventuress
{{Infobox film
| name           = An Innocent Adventuress
| image          = An Innocent Adventuress (1919) - Ad 1.jpg
| alt            = 
| caption        = Ad for film
| director       = Robert G. Vignola
| producer       = Jesse L. Lasky
| screenplay     = Clara Genevieve Kennedy 
| starring       = Vivian Martin Lloyd Hughes Edythe Chapman Gertrude Norman Jane Wolfe Tom Bates
| music          = 
| cinematography = Frank E. Garbutt 	
| editor         =	
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Comedy comedy film directed by Robert G. Vignola and written by Clara Genevieve Kennedy. The film stars Vivian Martin, Lloyd Hughes, Edythe Chapman, Gertrude Norman, Jane Wolfe and Tom Bates. The film was released on June 8, 1919, by Paramount Pictures.  

==Plot==
 

==Cast==
*Vivian Martin as Lindy
*Lloyd Hughes as Dick Ross
*Edythe Chapman as Aunt Heppy
*Gertrude Norman as Mrs. Cribbley
*Jane Wolfe as Mrs. Bates 
*Tom Bates as Chilowee Bill 
*Hal Clements as Doc Brogan
*Jim Farley as Brogans Accomplice
*Spottiswoode Aitken as Meekton 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 