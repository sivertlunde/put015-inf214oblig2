Hridayame Sakshi
{{Infobox film 
| name           = Hridayame Sakshi
| image          =
| caption        =
| director       = IV Sasi
| producer       =
| writer         =
| screenplay     = Sharada K. P. Ummer Sankaradi
| music          = M. S. Viswanathan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by IV Sasi . The film stars Prem Nazir, Sharada (actress)|Sharada, K. P. Ummer and Sankaradi in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
*Prem Nazir as Murali Sharada as Kamala
*K. P. Ummer as Vasudevan
*Sankaradi as Kurup
*Bahadoor as Narayanan

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ezhu Nirangalilethu Manoharam || Ambili, KP Brahmanandan || Sreekumaran Thampi || 
|-
| 2 || Manassillengil || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 3 || Manassupole Jeevitham || P Susheela, Chorus || Sreekumaran Thampi || 
|-
| 4 || Pirinju Povukayo || Ambili, Chorus, Saibaba || Sreekumaran Thampi || 
|-
| 5 || Vasanthame Nee Vannu || S Janaki || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 