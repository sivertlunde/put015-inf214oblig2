The Vampire Who Admires Me
 
 
{{Infobox film
| name = The Vampire Who Admires Me
| image = The Vampire Who Admires Me poster.jpg
| caption = 
| producer = Wong Jing
| director = Cub Chin
| writer = Wong Jing
| starring = Samuel Pang   Roger Kwok   JJ Jia   Natalie Meng Yao   Ankie Beilke   Maggie Li   Winnie Leung   Tanya Ng
| editing = Li Kar Wing
| cinematography = Ng King-Man
| studio = Mega-Vision Pictures (MVP)   See Movie   Wong Jings Workshop Ltd.
| distributor = Mega-Vision Pictures (MVP) 
| released =  
| runtime = 93 minutes
| country = Hong Kong
| language = Cantonese
| budget = 
}}
The Vampire Who Admires Me ( ) is a 2008 Hong Kong mystery film directed by Cub Chin starring Samuel Pang and Roger Kwok and actresses JJ Jia Maggie Li, Natalie Meng, model Ankie Beilke, Winnie Leung, and model Tanya Ng.

==Cast==
* Samuel Pang as King
* Roger Kwok as Wayne Sa
* JJ Jia as Macy
* Natalie Meng Yao as Chelsea
* Ankie Beilke as BiBi
* Maggie Li as Apple
* Winnie Leung as Kimchi
* Tanya Ng as Susie
* Jo Kuk as Madam Sarah Chui Sam Lee as Roman
* Ben Cheung as Victor
* Angel Ho as Jill
* Siu Fei as Mann Ha Yu as Uncle Faye
* Wong Tin-Lam as Uncle Pine
* Lee Fung as Macys grandmother
* Si Ming as Jills mother
* Wong Wai-Tong as Vampire King #1
* Cheung Wing-Cheung as Vampire King #2
* Wong Chun-Sing

== External links ==
*  
*   at the Hong Kong Movie Database
*   at the Hong Kong Cinemagic
*  
*  

 
 
 
 
 
 


 
 