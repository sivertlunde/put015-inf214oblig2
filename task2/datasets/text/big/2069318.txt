Bon Voyage, Charlie Brown (and Don't Come Back!!)
 
{{Infobox film
| name           = Bon Voyage, Charlie Brown (and Dont Come Back!!)
| image          = Bon Voyage, Charlie Brown (and Dont Come Back!!).jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Bill Meléndez Phil Roman
| producer       = Bill Meléndez Lee Mendelson
| writer         = Charles M. Schulz
| starring       = Arrin Skelley  Daniel Anderson  Patricia Patts  Casey Carlson  Michelle Muller  Annalisa Bortolin  Pascal de Barolet  Roseline Rubens  Bill Meléndez
| music          = Ed Bogas Judy Munsen
| editing        = Roger Donley Chuck McCann Lee Mendelson Bill Meléndez Productions United Feature Syndicate
| distributor    = Paramount Pictures
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2,013,193 
}} animated comedy-drama I and World War II|II. It also use the same voice cast that worked on the 1979 Peanuts television special Youre the Greatest, Charlie Brown.

Paramount Home Entertainment released this film on VHS in 1995, and will release it to DVD in October 2015. 

==Plot==
  Woodstock are Lucy warning him to remain in Europe ("...and dont come back!")
 a prior Victoria railway station and rejoin the kids, where upon they take a train from London to Dover, where they briefly admire the White Cliffs of Dover before boarding a hovercraft to cross the English Channel. They travel to France and drive to the farmhouse where Peppermint Patty and Marcie will be staying with Pierre. He is a good host who appreciates Marcie making an effort to learn his native tongue, but Peppermint Patty experiences culture shock in having to help out with the farm chores, French cuisine, as well as having to wear a school uniform to the local school, whereas Marcie reminds her that when one is in another land, one must respect their ways. When Marcie says that Linus and Charlie Brown are staying at the Château du Mal Voisin, Pierre shudders, as it has a bad reputation in the village, then goes on to reveal that there must be some mistake, as it is owned by an antisocial Baron who never puts up guests, especially not Americans. Shortly after the boys drop off the girls at Pierres house, a thunderstorm breaks and hits the village. When they arrive at the château, it apparently seems unoccupied. Linus and Charlie Brown are perplexed as to why someone would invite them all this way then not be at home to greet them. With the rain beating down, they are forced to take cover in an empty stable and use their luggage as makeshift sleeping bags. The boys ask Snoopy and Woodstock to guard them, but they head down to the pub to drink a few pints of root beer.

The next morning, Charlie Brown and Linus find that breakfast and warm blankets have been provided by an unknown benefactor, further puzzling them. When the kids meet with each other at school, Charlie Brown produces the letter from Violet, whom Pierre states that she is the niece of the Baron who owns the château. In a reversal of the earlier scene, the four kids are introduced to the French school by Pierres teacher, who says they will have bilingual classes to help their American exchange students. Charlie Brown is made to sit next to Peppermint Patty, resulting in some humorous exchanges. That evening, the boys return to see their bedding reset and an evening meal laid out, but still no people. With Snoopy once again enjoying himself at the pub, Linus suggests he and Charlie Brown sleep in shifts to catch the mysterious benefactor. During Charlie Browns watch, rooms in the château light up, but he fails to notice as he is so sleepy. The Baron and Violet appear at the front door, with him stating that he is going to the pub for a bit and that he does not want her to let the boys inside, but she protests that they must be hospitable and that their family owes a debt to the Brown family. After he drives away, Linus awakens to see that the blankets have been fixed, new food has been put out, and Charlie Brown has slacked on his watch by going back to sleep. Linus tells himself he will get to the bottom of this, and sneaks into the château, which is dark except for a candlelight, which he follows to the attic to corner Violet, who admits she is the unseen hostess and says that her invitation was a serious mistake. She explains that her grandmother had told her the story of a wartime love affair that she had with Silas Brown, Charlie Browns grandfather, who was an infantryman stationed in France during World War II, and stayed at the château. She presents Linus with Silas satchel, proving her story, as well as a photograph of her family posing with him, whom Linus remarks looks like a full-grown version of Charlie Brown. When Silas received his marching orders, he promised to write letters which he did for many years. However, after he married an American woman, they eventually stopped coming. Violets grandmother moved on, though she never forgot him. At the pub, Snoopy and Woodstock are playing foosball and overhears the Baron confide to the bartender that he knows about Charlie Brown and Linus and has tolerated them long enough, planning to do terrible things to them should they fail to leave by morning. He returns home to Violets surprise, and in her rush to get Linus out of the room, she accidentally knocks down a candle which spreads a fire throughout the attic. Linus takes her towards a pair of casement windows, throws them open, and shouts to Charlie Brown for help. He wakes up and runs to the pub to call Snoopy and Woodstock. They rush off to the château and Charlie Brown continues running to the farmhouse to awaken Peppermint Patty, Marcie, and Pierre. The girls follow him towards the château, while Pierre calls the fire department before following them.

When Snoopy and Woodstock arrive at the château, it is engulfed in flames, while the Baron begs for someone to rescue Violet. Snoopy immediately heads to a shed and brings out an old fashioned fire hose, while Linus throws his blanket down to Charlie Brown, Peppermint Patty, Marcie, and Pierre which they use to catch Violet when she jumps from the window. Snoopy moves a barrel of water under Linus who also jumps to safety, and later barks out instructions to work the hose, which is manned by Charlie Brown and Pierre, but the intense water pressure spins him around, splashing Charlie Brown. Woodstock later pops out of the hose with a violin and begins playing along to the dramatic scene. Snoopy finally wrestles the hose and gets it to work, a fire truck with three firemen arrive, and the fire is finally extinguished. Thankful that the gang has saved the château and more importantly, Violet, the Baron promises that he will never be unfriendly again. Charlie Brown learns the full truth behind the mysterious letter he had received: many years ago, a friend of Violets family informed her he located the Browns when he took a tour of the United States and stopped to have his hair cut, and the barber was Silas son, a.k.a. Charlie Browns father. When Violets school participated in the foreign exchange program, she wrote the letter to Charlie Brown inviting him to stay at the Chateau. As the kids prepare to leave the château and return to America, Violet gives Silas satchel to Charlie Brown. The film ends when the kids say their goodbyes to Pierre and Violette and get into their beat-up rental car.

== Behind the Scenes ==
*The scene at the French school where Peppermint Patty is made to sit next to Charlie Brown was based on a 1970s Peanuts strip, where Peppermint Pattys school burned down and was merged with Charlie Browns. As a result of the overcrowding, desks were taken out and students were made to sit at tables next to each other. Peppermint Patty sits next to Charlie Brown, where she bosses him about his work, then snores in class.
*During the credits, photographs of the crew are shown along with montages of the flags of France, United Kingdom and United States, the three nations the film is set in.
*Many people mistake parts of the films musical score as a slower version of the James Bond theme, most notably the scene when Charlie Brown, Linus, Woodstock and Snoopy are looking for a dry shelter upon arriving at Mal Voisan. This is not the case, as that theme is Charlie Browns theme in minor key, which shares the string sub-melody of the Bond theme by coincidence.
*This is the first Peanuts presentation in which adults are heard speaking in a normal voice (as opposed to the customary trombone wah-wah sound), as well as being seen.

==Cast==
* Arrin Skelley as Charlie Brown
* Daniel Anderson as Linus van Pelt
* Patricia Patts as Peppermint Patty
* Casey Carlson as Marcie
* Annalisa Bortolin as Sally Brown
* Michelle Muller as Lucy van Pelt Woodstock
* Pascale De Barolet as Pierre
* Roseline Rubens as Violet Honfleur, Patty
* Scott Beach as Waiter, Baron, Driver, Tennis Announcer, English Voice, American Male
* Mel Blanc (uncredited) as assorted vocal effects

==Music Crew==
* Music by: Ed Bogas and Judy Munsen
* Additional Music by: Doug Goodwin

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 