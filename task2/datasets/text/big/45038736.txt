Kudure Mukha
{{Infobox film 
| name           = Kudure Mukha
| image          =  
| caption        = 
| director       = Y. R. Swamy
| producer       = 
| writer         = Sri Bhagavathi Productions
| screenplay     = 
| starring       = Anant Nag Aarathi Ambarish Sundar Krishna Urs
| music          = T. G. Lingappa
| cinematography = R Chittibabu
| editing        = P Bhakthavathsalam
| studio         = Sri Bhagavathi Productions
| distributor    = Sri Bhagavathi Productions
| released       =  
| country        = India Kannada
}}
 1978 Cinema Indian Kannada Kannada film, directed by Y. R. Swamy. The film stars Anant Nag, Aarathi, Ambarish and Sundar Krishna Urs in lead roles. The film had musical score by T. G. Lingappa.  

==Cast==
 
*Anant Nag
*Aarathi
*Ambarish
*Sundar Krishna Urs
*Vaman
*M. S. Sathya
*Jr Shetty
*Shantharam
*Bheemaraj
*N. S. Rao
*Helam
*Jayamalini
*Mamatha Shenoy
*Jayakala
*Baby Rekha
 

==Soundtrack==
The music was composed by TG. Lingappa. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Indhe Savi Samaya || Vani Jayaram || R. N. Jayagopal || 03.18
|- Janaki || Vijaya Narasimha || 03.19
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 


 