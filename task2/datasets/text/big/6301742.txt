Sunburn (film)
Sunburn American detective fiction|detective-comedy film directed by Richard C. Sarafian and starring Farrah Fawcett, Charles Grodin and Art Carney.  It is based on the novel The Bind by Stanley Ellin.

==Plot==
A private eye named Dekker goes to Acapulco to investigate a murder and hires the beautiful Ellie Morgan to pretend to be his wife.

==Cast==
*Farrah Fawcett as Ellie
*Charles Grodin as Dekker
*Art Carney as Marcus
*Joan Collins as Nera
*William Daniels as Crawford
*Seymour Cassel as Dobbs
*John Hillerman as Webb
*Eleanor Parker as Mrs. Thoren
*Keenan Wynn as Elmes
*Jack Kruschen as Gela
*Alejandro Rey as Fons

==Music== Wings song "With a Little Luck".

==External links==
* 

 

 

 
 
 
 
 
 
 
 
 