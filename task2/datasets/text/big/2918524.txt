Secret Admirer
 
{{Infobox film
| name = Secret Admirer
| image = secret_admirer.jpg
| caption = Theatrical release poster
| director = David Greenwalt
| producer = Steve Roth
| writer = {{plainlist|
* David Greenwalt
* Jim Kouf
}}
| starring = {{Plainlist|
* C. Thomas Howell
* Lori Loughlin
* Kelly Preston
* Fred Ward Dee Wallace Stone
* Leigh Taylor-Young
* Cliff DeYoung
}}
| music = Jan Hammer
| cinematography = Victor J. Kemper
| editing = Dennis Virkler
| distributor = Orion Pictures
| released =  
| runtime = 100 minutes
| country = United States
| language = English
| budget =
| gross = $8.6 million (US) 
}} teen romantic comedy film written and directed by David Greenwalt in his feature film directorial debut, and starring C. Thomas Howell, Lori Loughlin, Kelly Preston and Fred Ward. The original music score was composed by Jan Hammer. The film was produced at the height of the teen sex comedy cinema craze in the mid-1980s.

== Plot ==
Michael Ryan is a high school student who receives an anonymous love letter. Michael is obsessed with Deborah Ann Fimple, the class beauty, and his best friend, Roger, convinces him that the letter is from her. However, he is totally oblivious that his friend Toni Williams is in love with him. Michael writes Deborah Ann an anonymous love letter in return, and asks Toni to give it to her. Toni realizes the letter is poorly written and unromantic (since Michael had copied words from greeting cards), so she rewrites it. Elizabeth Fimple, Deborah Anns mother, discovers the letter. Her jealous police officer husband, Lou Fimple, sees her reading it. He steals the letter, and believes that his wife is having an affair. He suspects his neighbor (and bridge partner) George Ryan. George also mistakenly reads the letter because Lous wife is his night school teacher and it somehow ends up in his book. When George asks her about it he assumes she wants to have an affair with him despite the fact his wife and she are friends. Meanwhile Lou shows the letter to Georges wife, Connie, and proposes that they expose the adulterers. Receiving no response from Deborah Ann, Michael writes a second letter, which Toni again rewrites.

Michael experiences a series of wacky adventures with his friends throughout the summer.  After Toni arranges a meeting between the two; He tells Deborah Ann that he wrote the love letters, and she finally agrees to a real date during which they are almost caught by Debbies jock college "quasi boyfriend" Steve but Toni intervenes by pretending to seduce him and later ditches him. After a short while Michael realizes Deb is snobby and shallow, not like he expected her to be; they break up after his birthday party when he realizes he cant sleep with her as she intends for his birthday present. Eventually, Lou and Connie cannot control themselves at a bridge party: Lou assaults George, and Connie breaks down in front of her friends. When Lou confronts his wife about the letter, Deborah Ann overhears him reading the words and tearfully accosts her father for reading her private mail. Debbie runs to her rooms and cries, while both sets of parents make up after the misunderstanding with the letters. Michael also blasts his parents for reading his letter and invading his privacy.

Just as the fall semester is about to start, Michael (by comparing Debbies letters to Tonis handwriting) realizes that Toni wrote the original love letter. He races to her home but is told that she has left for a study abroad program aboard a ship that will keep her away for a full year. Michael rushes to the dockyard after a brief scuffle with Steve, screaming his love for Toni. After shouting her love for him as the ship continues to sail away he dives into the water, but cannot reach the ship. Toni dives into the water, too. The lovers embrace in the water and kiss.

== Cast ==
* C. Thomas Howell as Michael Ryan
* Kelly Preston as Deborah Ann Fimple
* Lori Loughlin as Toni Williams
* Fred Ward as Lt. Lou Fimple Dee Wallace Stone as Connie Ryan
* Cliff DeYoung as George Ryan
* Leigh Taylor-Young as Elizabeth Fimple
* Casey Siemaszko as Roger Despard
* Scott McGinnis as Steve Powers
* Corey Haim as Jeff Ryan
* Courtney Gains as Doug
* Janet Carroll as Tonis Mother

== Reception == Time Out London compared it negatively to French sex farces and said that it offers no insights.   TV Guide rated it 1/5 stars and wrote, "This cross between theatrical farce and teen sex comedy is a moronic package that liberally insults the intelligence of both its viewing audience and the hapless adult actors locked into career low points."   Reviewing the film retrospectively, Sarah D. Bunting of Slant Magazine called it "relatively good", which she defines as "not unwatchable" in terms of teen films. 

In 1985, the Los Angeles Times asked a group of teens to judge their interest in a series of released films.  After seeing preview and press materials, the teens rated Secret Admirer C on an A to F range; their opinion was divided over whether they wanted to see it or not. 

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 