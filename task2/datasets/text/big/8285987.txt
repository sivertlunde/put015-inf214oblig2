Juno and the Paycock (film)
 
 
 
{{Infobox film
| name           = Juno and the Paycock
| image          = Juno and the Paycock.jpg
| caption        = Original film poster
| screenplay     = Alfred Hitchcock
| story          = Sean OCasey Alma Reville Edward Chapman Sidney Morgan Sara Allgood
| director       = Alfred Hitchcock
| producer       = John Maxwell
| distributor    = Wardour Films (UK) Harold Auten (US)
| released       = 1930 (UK) 29 June 1930 (US)
| runtime        = 85 min.
| language       = English
| music          =
| cinematography = Jack E. Cox
| editing        = Emile de Ruelle
| budget         =
}} Edward Chapman and Sara Allgood. 
 play by Sean OCasey. 

==Plot== Edward Chapman) lives in a two-room tenement flat with his wife Juno (Sara Allgood) and children Mary (Kathleen ORegan) and Johnny (John Laurie). Juno has dubbed her husband "the Paycock" because she thinks him as useful and vain as a peacock. Juno works while the Captain loafs around the flat when not drinking up the familys meager finances at the neighbourhood pub.

Daughter Mary has a job but is on strike against the victimisation of a co-worker. Son Johnny has become a semi invalid after losing an arm and severely injuring his hip in a fight with the Black and Tans during the Irish War of Independence. Although Johnny has taken the Anti-Treaty side during the continuing Irish Civil War, he has recently turned in a fellow Irish Republican Army (IRA) member to the Irish Free State police who subsequently kill him. The Paycock tells his friend Joxer (Sidney Morgan) of his disgust at the informer, unaware that his son was responsible. The IRA suspect Johnny and order him to report to them for questioning; he refuses, protesting that his wounds show he has done his bit for Ireland.
 Dave Morris) but dumps him for Charlie Bentham (John Longden) who whisks her away after telling Marys family the Captain is to receive an inheritance. The elated Captain borrows money against the (as yet un-received) inheritance and spends it freely on new furniture and a Victrola. Family friends are invited to an impromptu party at the once shabby tenement.

The Captain soon learns the inheritance has been lost because Bentham made an error in drafting the will. The Captain keeps the bad news a secret until creditors show up. Even Joxer turns on the Captain and gleefully spreads the news of the nonexistent inheritance to creditors. The furniture store repossesses the furniture. The tailor demands money for new clothes. Pub owner Mrs. Madigan’ (Maire ONeill) takes the Victrola to cover the Captains bar tab.

The worst is yet to come, however. Mary reveals that she has shamed the family by becoming pregnant by Charles, who has disappeared after his blunder was discovered. Her former fiancé Jerry proclaims his love for Mary and offers to marry her back until he learns of her pregnancy. While his parents are absent dealing with the situation, Johnny is taken from the flat by the IRA and his body is later found riddled with bullets. Realizing that their family has been destroyed, Mary declares, "Its true. There is no God." Although completely shattered, Juno shushes her daughter, saying that they will need both Christ and the Blessed Virgin to deal with their grief. Alone, however, she laments her sons fate before the religious statues in the familys empty tenement, deciding that Boyle will remain useless and leaves with Mary.

==Cast==
*Barry Fitzgerald as The Orator
*Maire ONeill as Maisie Madigan Edward Chapman as Captain Boyle
*Sidney Morgan as "Joxer" Daly
*Sara Allgood as Mrs Boyle, "Juno"
*John Laurie as Johnny Boyle Dave Morris as Jerry Devine
*Kathleen ORegan as Mary Boyle
*John Longden as Charles Bentham
*Dennis Wyndham as The Mobiliser
*Fred Schwartz as Mr Kelly

==Production== The Birds.

Sara Allgood reprised her role as Juno from the play. Barry Fitzgerald made his film debut.

The tailor Mr Kelly, who repossesses Captain Boyles new clothes (bought on hire-purchase), is portrayed by a Jewish actor and given a strong Germanic accent, although there is no indication in the original play that the character (there called Nugent) is anything other than an Irish Gentile. It has been alleged that this plays up to the stereotype of Jews as alien usurers. 

==Copyright status==
The following two views are opposed to each other over the copyright of this work.
#Copyright has already expired, according to the Copyright, Designs and Patents Act 1988, because it was issued over 70 years ago. Directive harmonising the term of copyright protection.

In the US, there is a contractor who releases public domain DVDs based on the former view.

On the other hand, Canal+ which claims the present copyright asserts copyright continuation based on the latter view.

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 