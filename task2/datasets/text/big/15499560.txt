Quicksand (2002 film)
{{Infobox film
| name           = Quicksand
| image          =
| caption        =
| director       = Sam Firstenberg
| producer       = Frank DeMartini Rafael Primorac
| writer         = Steve Schoenberg Ruben Gordon
| starring       = Michael Dudikoff Brooke Theiss Richard Kind Dan Hedaya
| music          = Curt Harpel
| cinematography = Sameer Reddy
| editing        = Alison Learned
| distributor    = Quantum Entertainment New Concorde (USA)  Cinema Club (UK)
| released       =  
| runtime        = 92 minutes
| country        = United States India
| language       = English
| budget         =
| gross          =
}}
Quicksand is a 2002 action film that was released direct-to-video in March 2003 after a short initial run in theatres in Los Angeles in March 2002. The film stars Michael Dudikoff, Brooke Theiss, Richard Kind and Dan Hedaya. In United Kingdom the film was released on DVD in August 2003. The film is directed by Sam Firstenberg.

==Plot==
When military psychiatrist Bill Turner (Michael Dudikoff) falls for a generals daughter, a dark conspiracy threatens to swallow up everyone involved. Turners relationship with his new patient, Marine sergeant Randi Stewart (Brooke Theiss), begins to reach well beyond the typical doctor/patient bond, as he soon discovers that she is involved in a far-stretching political conspiracy but cant tell if she is the victim or the perpetrator. Randis brother, Gordon Stewart is running for political office when their father General Stewart (Dan Hedaya) is murdered and Randi becomes the main suspect. When Turners feelings toward Randi grow he has to deal with her being a possible murderer. But Turner would more than anything not find out, which leads him getting sucked deeper and deeper into the "Quicksand".

==External links==
*  
*  

 
 
 
 
 


 