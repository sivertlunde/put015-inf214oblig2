Ghar Parivar
{{Infobox film
| name           = Ghar Parivar
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Mohanji Prasad
| producer       = B. K. Parivar
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Rajesh Khanna Rishi Kapoor Moushumi Chatterjee Meenakshi Seshadri
| music          = Kalyanji Anandji
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =    
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}

Ghar Parivar is a 1991 Indian Bollywood film directed by Mohanji Prasad and produced by B. K. Parivar. It stars Rajesh Khanna, Rishi Kapoor, Moushumi Chatterjee and Meenakshi Seshadri. 

==Cast==
* Rajesh Khanna
* Rishi Kapoor
* Moushumi Chatterjee
* Meenakshi Seshadri
* Prem Chopra
* Kader Khan

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Beech Bajariya Ladi Najariya"
| Kumar Sanu
|-
| 2
| "Samay Bada Balwan Hai"
| Nitin Mukesh
|-
| 3
| "Kurte Ka Kya Hai"
| Amit Kumar, Udit Narayan, Sadhana Sargam, Sonali Bajpayee
|-
| 4
| "Road Romeo"
| Kumar Sanu, Satyanarayan Rao
|-
| 5
| "Nazar Jo Hamse Churayega"
| Alka Yagnik
|-
| 6
| "Rooth Raswanti"
| Sadhana Sargam
|-
| 7
| "Rajaji Dam Khanse"
| Alka Yagnik
|}

==References==
 

==External links==
* 

 
 
 
 


 