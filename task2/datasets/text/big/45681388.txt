Do Unto Others (film)
{{Infobox film
| name           = Do Unto Others 
| image          =
| caption        =
| director       = Bert Haldane 
| producer       = 
| writer         = H. Grenville-Taylor
| starring       = Thomas H. MacDonald   Peggy Richards   Patrick J. Noonan
| cinematography = 
| editing        = 
| studio         = Barker Films
| distributor    = Barker Films
| released       = July 1915
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama film directed by Bert Haldane and starring Thomas H. MacDonald, Peggy Richards and Patrick J. Noonan.

==Cast==
*  Thomas H. MacDonald as Curley  
* Peggy Richards as Renee  
* Patrick J. Noonan as Steve  
* Pippin Barker as Curley as a Child  
* Connie Barnes as Renee as a Child  Willie Harris as Steve as a Child

==References==
 

==Bibliography==
* Low, Rachael. The History of British Film, Volume III: 1914-1918. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 
 

 