Adiyum Andamum
 

{{Infobox film
| name           = Adiyum Andhamum
| image          = 
| alt            =  
| caption        = 
| director       = Kaushik
| producer       = RSR SCREENZ and N.K. Kraft pvt ltd
| story          = Kaushik
| screenplay     = Kaushik
| starring       = Ajay Mitali Agarwal Kavitha Srinivasan
| music          = L.V. Ganeshan
| cinematography = DS. Vaasan
| editing        = M. Ramesh Bharathi
| released       =  
| language       = Tamil
}} Produced by RSR SCREENZ and N.K. Kraft pvt ltd the film’s cinematography is by DS. Vaasan, Music by L.V. Ganesan, Art direction by A. Amaran, Sound effects by D. Narayanan and editing by M. Ramesh Bharathi.  This film is loosely based on Shutter Island by Martin Scorsese.

== Plot==
The protagonist of this story is Karan a psychiatrist. Orphaned at a very young age he was brought up by his maternal uncle. Though not very rich Karan’s uncle ensured that Karan got the best of education and became a doctor. Karan for his part turns out to become a psychiatrist. Karan is a psychiatrist with a difference. He believes in the human touch. As circumstances take him on a professional trip to a reputed medical institute where he takes charge as lecturer cum psychiatrist. The minute he lands in Ooty he is at ease and enjoys every moment at his new destination. As he settles in the new place he feels at home and ease. 
Soon he gets used to a new routine in his life but just as he thinks that life cant be more perfect he starts experiencing certain disturbances. He starts seeing things in his room which defy the human mind. At first he doesn’t take it seriously but soon they intensify in their hostility with each new day. Soon the disturbances are more terrifying than he can imagine.
Doctor Anbarasu is the founder cum dean cum MD of the institute. Soft spoken but a man who will get what he wants. In him Karan sees a father figure in the beginning which slowly changes with each growing day. As he sees a new side to this person Karan is made to wonder about the true character of the dean.
Shalini is a reporter who hosts a reality show in a reputed channel. She arrives at the institute to do a cover story. The first encounter between Karan and Shalini promotes a kind of respect for Karan in Shalini. This respect slowly blossoms into something else. As their paths meet regularly, Karan feels he can confide in Shalini about his disturbances and his sudden trepidation about the dean. Curiosity kindles the reporter in Shalini and she along with Karan embarks on an investigative journey to find out what really is going on in the institute. The going is tough as there are man made hindrances in their path. At one point it is clear to them that no one is telling the truth and everyone is hiding the truth.

Soon both Karan and Shalini feel that they are being followed and that at any point of time something disastrous is going to happen. As time runs out for Shalini to leave the place they have that much less time to find out the truth about the place and the true face of Dr Anbarasu.As they stumble upon the truth it is a shocker. Sometimes what you see need not be the reality at all.

=== CAST ===
* Ajay
* Mitali Agarwal
* Kavita Srinivasan
* Ramanathan
* Siddarth
* Venkat
* Yuvan swang

{| class="wikitable"
|-
! Song !! Lyricsit !! Singers
|-
| Penne penne || Kaushik || Aalap Raju
|-
| Mayam seidhayo || Kaushik || Chinmayee
|-
| Ararirao || Na.Muthukumar || Naresh Iyer
|-
| Adiyum Andamum title track || Kaushik || L.V. Ganeshan
|-
| Penne remix ||Kaushik|| Lv. Muthu, Kaushik
|-
| Pain of love (Instrumental)
 ||  || 
|}

==References==
 

 
 