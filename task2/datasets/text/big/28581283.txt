The Kitchen Toto
{{Infobox film
| name           = The Kitchen Toto
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Harry Hook
| producer       = Ann Skinner
| writer         = Harry Hook
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Edwin Mahinda Bob Peck Phyllis Logan
| music          = John E. Keane 
| cinematography = Roger Deakins
| editing        = Tom Priestley
| studio         = British Screen Productions Channel Four Films Skreba Films
| distributor    = Cannon Films
| released       = 1988
| runtime        = 96 min
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British drama film directed by Harry Hook and starring Edwin Mahinda, Bob Peck and Phyllis Logan. 

==Plot==
In Kenya 1950, a British policeman takes a murdered black priests son to live with him at his home as a houseboy.

==Cast==
* Edwin Mahinda ...  Mwangi
* Bob Peck ...  John Graham
* Phyllis Logan ...  Janet Graham Nicholas Charles ...  Mugo
* Ronald Pirie ...  Edward Graham Robert Urquhart ...  D.C. McKinnon Kirsten Hughes ...  Mary McKinnon
* Edward Judd ...  Dick Luis
* Nathan Dambuza Mdledle ...  Mzee, Mwangis Father
* Ann Wanjuga ...  Mwangis Mother
* Job Seda ...  Kamau
* Leo Wringer ...  Sergeant Stephen

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 