Waterfront (1939 film)
{{infobox film
| name           = Waterfront
| image          =
| imagesize      =
| caption        =
| director       = Terry O. Morse
| producer       = Bryan Foy Hal B. Wallis Jack L. Warner
| writer         = Kenyon Nicholson(play) Lee Katz(writer) Arthur Ripley(writer) Fred Niblo, Jr. Don Ryan
| starring       = Gloria Dickson Dennis Morgan
| music          = Heinz Roemheld
| cinematography = James Van Trees
| editing        = Louis Hesse
| distributor    = Warner Brothers
| released       = July 15, 1939
| runtime        = 59 minutes
| country        = USA
| language       = English
}}
Waterfront is a 1939 film produced and released by Warner Brothers and starring Dennis Morgan. It was directed by Terry OMorse from a play, Blind Spot, by Kenyon Nicholson. It is preserved at the Library of Congress.   

==Cast==
*Gloria Dickson - Ann Stacey
*Dennis Morgan - James Jim Dolan Marie Wilson - Ruby Waters
*Sheila Bromley - Marie Cordell
*Larry Williams - Frankie Donahue
*Aldrich Bowker - Father Dunn
*Frank Faylen - Skids Riley
*Ward Bond - Mart Hendler Arthur Gardner - Dan Dolan George Lloyd - Joe Becker

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 


 