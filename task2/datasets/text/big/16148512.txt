Voltaire (film)
{{Infobox film
| name           = Voltaire
| image          = Voltaire_Arliss.jpg
| image_size     = 210px
| caption        =
| director       = John G. Adolfi
| producer       = Paul Green Maude T. Howell
| starring       = George Arliss Doris Kenyon Margaret Lindsay Alan Mowbray Reginald Owen
| music          = Bernhard Kaun Milan Roder (both uncredited)
| cinematography = Tony Gaudio
| editing        = Owen Marks
| distributor    = Warner Bros.
| released       = 5 August 1933
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} French writer and philosopher.

==Plot==
In pre-Revolutionary France, Voltaire champions the oppressed commoners and tries to warn King Louis XV (Reginald Owen) about the growing unrest among his subjects. The writer has a powerful ally in Madame Pompadour (Doris Kenyon), Louis mistress, but the Count de Sarnac (Alan Mowbray) opposes him for his own ends.

When Voltaire pleads for the life of Calas, unjustly accused of treason, Louis is inclined to pardon the man, but Sarnac persuades him that it would be a sign of weakness, and Calas is swiftly executed. As a reward, Sarnac gains the wealthy mans estates.  Voltaire invites Calas daughter and rightful heiress, Nanette (Margaret Lindsay), to shelter in his home.

Meanwhile, Sarnac tries to persuade the King that Voltaire is a traitor, citing his well-known friendship with Frederick the Great and claiming that it is he who is betraying French secrets to the Prussian ruler. Louis is not entirely convinced, but does banish Voltaire from his royal court at Palace of Versailles|Versailles.   

As a result, Madame Pompadour becomes reluctant to aid Voltaire further, until he arranges it so that she can overhear from Sarnacs own lips his ambition to replace her as Louis paramount adviser. Then, she persuades the King to allow Voltaire to stage a new play at Versailles. 

The production is a thinly disguised portrayal of Calas execution and the aftermath transposed to an exotic setting. Voltaire hopes to open the Kings eyes to his danger. Voltaire recruits Nanette to portray the part of herself. The King is sympathetic to the theatrical Nanettes plight, not recognizing himself as her despised oppressor until Sarnac points it out. Then Louis orders the play stopped before the explanatory final scene and orders that Voltaire be sent to the Bastille. However, hearing of a rich present given to Sarnac by Frederick, Voltaire unmasks the count as the real traitor. Sarnac is arrested, and Nanettes estates are restored to her.  

==Cast==
*George Arliss as Voltaire Madame Pompadour
*Margaret Lindsay as Nanette Calas
*Alan Mowbray as the Count de Sarnac King Louis XV
*Theodore Newton as Francois, a friend of Nanette
*Gordon Westcott as the Captain
*David Torrence as Dr. Tronchin, Voltaires doctor
*Murray Kinnell as Emile
*Doris Lloyd as Madame Clarion

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 