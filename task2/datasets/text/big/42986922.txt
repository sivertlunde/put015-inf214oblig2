Lazer Team
{{Infobox film
| image          = Lazer Team film poster.jpg
| alt            = 
| caption        = Teaser poster
| director       = Matt Hullum
| producer       = {{Plain list |
* Burnie Burns
* Doreen Copeland
* Matt Hullum
}}
| writer          = {{Plain list |
* Burnie Burns
* Chris Demarais
* Josh Flanagan
* Matt Hullum
}}
| story           = Burnie Burns
| starring       = {{Plain list | 
* Burnie Burns
* Gavin Free  Michael Jones
* Colton Dunn 
* Allie DeBerry
* Alan Ritchson
 
}}
| music          = 
| cinematography = Philip Roy
| editing        = David James Ward Rooster Teeth Productions Fullscreen Films
| released       =   }}
| runtime        = 
| country        = United States
| language       = English
| budget         = $2.4 million (crowdfunding total)   
| gross          = 
}}
 science fiction Michael Jones, Colton Dunn, Allie DeBerry, and Alan Ritchson.     It is set to be released in 2015.      

==Plot==  
 

==Cast==
*Burnie Burns as Hagan, a member of Lazer Team 
*Gavin Free as Woody, a member of Lazer Team  Michael Jones as Zach, a member of Lazer Team 
*Colton Dunn as Herman, a member of Lazer Team 
*Allie DeBerry as Mindy, Hagans daughter 
*Alan Ritchson as Adam, the Champion of the Earth 

==Production==
===Development===
The concept for Rooster Teeths first live action feature was in development as early as 2010.    It was first announced at Halo Fest during PAX Prime 2011.  In February 2014, Burns confirmed the company was going to launch a crowdfunding campaign for the film as a way to offer more support options while gaining publicity.  Burns stated they were drawing inspirations from, "a lot of the sci-fi classics that weve grown up with," but were, "not making a parody and we’re not making a send-up- we’re making our own movie."  Freddie Wong was a consultant in shaping their campaign.   
 highest funded film campaign on Indiegogo with over $2.4 million collected.  On the final day of the campaign, in celebration of becoming the highest-funded Indiegogo film, Rooster Teeth released a special $5 perk for a single fan to be cast in a walk-on role. So many people selected within 20 seconds of the perk going live that it slowed the sites ticketing system and allowed 535 people to purchase it. Burns confirmed they will be used for a crowd scene in the film. 

Burns explained during the campaign that as it makes more money, the films budget scales up. "The initial budget for talent was based on making the movie on the bare minimum with us throwing in the remainder of the expected budget. For instance, that meant using talent almost exclusively from in-house. As the budget grows, so do our opportunities to approach all kinds of talent. The same applies to Visual FX, quality of props and costuming, lighting, crew, etc."  Hullum corroborated, stating that since the campaign started theyve been contacted by multiple distribution companies, media companies and acting agencies.   
 RTX 2014.  Additional casting for extras was held during shooting. 

===Filming===
Principal photography began on October 14, 2014.  Filming took place over 40 days in Austin and New Mexico.   During the first week of production, filming took place at the Austin National Guard Armory, as well as the University of Texas at Austin.  Filming wrapped on December 13.  Reshoots started in late February 2015.

== Release ==
In January 2015 parent company Fullscreen announced that Lazer Team would be among the first titles in its newly launched feature film division.  Rooster Teeth stated they are platform-agnostic but are aiming for a theatrical release and announced it will hold an international premiere in Australia after a local fan, William OMalley, asked the Rooster Teeth panel on the subject at PAX Australia 2014. Australia was chosen as it was the next highest country to donate in the crowd funding campaign, behind the USA and slightly ahead of Canada.   The first teaser was released to the public on February 20, followed by a full trailer release during SXSW on March 16th.      

==References==
 

==External links==
*  on YouTube
* 

 

 
 
 