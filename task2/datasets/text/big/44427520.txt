Desha: The Leader
 
{{Infobox film name            = Desha: The Leader image           = DeshaTheLeader2014Poster.jpeg caption         = Theatrical release poster director        = Saikat Nasir producer        = Abdul Aziz screenplay      = Abdullah Zahir Babu  story           = Saikat Nasir starring        = {{Plainlist|
* Mahiya Mahi
* Shipan Mit
* Tariq Anam Khan}} music           = Shafiq Tuhin
                   Kishor cinematography  = Sahil Rony   Saiful Shaheen editing         = Tawhid Hossain Chowdhury studio          = Jaaz Multimedia distributor     = Jaaz Multimedia released        =   runtime         = country         = Bangladesh language        = Bengali budget          = ৳ 15 million gross           = ৳ 7.5 million   
}} Bangladeshi political thriller film directed produced by Jaaz Multimedia. The film was scripted and directed by Saikat Nasir and features a cast that includes Shipan Mit, Mahiya Mahi, and Tariq Anam Khan.  Desha: The Leader is a story about a political reality show that turns severe. 
The film is released on 26 December 2014. The film received favorable response from both Critics and Audiences. 

==Plot==
Channel 99 arranges a reality show, to select the next leader by SMS voting of people all over the world. The elected leader will stay on the persons house for two days, who will vote for the maximum times by SMS.

==Cast==
* Shipan Mit as Desha
* Mahiya Mahi as The Presenter
* Tariq Anam Khan as Hasan Hyder
* Manjurul Karim
* Tiger Robi
* Shimul Khan

==Production==

===Release===
On 10 November 2014, The first teaser of Desha: The Leader was introduced, Beauty Conglomerate Fair and Lovely was partnered up with the film as a commercial partner. 

==Reception==

===Critical response===
Upon Release, the film has received mostly Positive Reviews from the Critics. According to Dhallywood24, The film is a ray of light in the dark  and DhallywoodWorld described the film as Entertaining and Inspiring.  BMDB has rated the film 8/10. 

===Box office===
The film opened in Bangladesh on Friday, 26 December 2014 at 12:00 p.m across 71 Theaters. It earned ৳2.3 million on Opening Day with around 90% Occupancy across Single Screens and 95% on Multiplexes. The second day collection slightly dropped to ৳2.1 million. However, the film saw a steady increase on its third day and Already got almost 60% advanced ticket sale for 1 January.  

==Soundtracks==

{{Infobox album  
| Name        = Desha: The Leader
| Type        = soundtrack
| Artist      = Dilshad Nahar Kona, Kishore Das, James (musician)|James, A I Raju
| Cover       = Desha - The leader film cover.jpg
| Alt         = 
| Caption     = 
| Released    =  
| Recorded    = 2014
| Genre       = 
| Length      = 
| Director    = 
| Producer    = 
}}

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    =
| title1          = "Koto Ronger Hatchani"
| length1         = 3:48
| extra1          = Kishore Das, Dilshad Nahar Kona

| title2          = "Asche Desha Asche"
| length2         = 3:15 James

| title3          = "Eki Maya"
| length3         = 2:23
| extra3          = A I Raju

| title4          = "Bangla Maaer Gaan"
| length4         = 3:16
| extra4          = Shafiq Tuhin
}}


==References==
 

 
 
 
 
 
 
 
 