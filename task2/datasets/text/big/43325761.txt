Seeman (film)
{{Infobox film
| name           = Seeman
| image          = 
| caption        =  Raj Kapoor
| producer       = M. G. Balu
| screenplay     = 
| story          = Karthik Sukanya_(actress)|Sukanya
| music          = Ilaiyaraaja
| cinematography = 
| editing        = 
| studio         = M. G. Pictures Ltd.
| distributor    = M. G. Pictures Ltd.
| released       = 15 April 1994
| country        = India
| runtime        = 
| language       = Tamil
}}
 1994 Cinema Indian Tamil Tamil drama Raj Kapoor. Karthik and Sukanya in lead roles. The film, had musical score by Ilaiyaraaja. After the success of Chinna Jameen, Karthik and Rajkapoor joint hand for this venture. Karthik and Salim Ghouse were lauded for their performances

==Cast== Karthik
*Sukanya_(actress)|Sukanya
*Jaiganesh
*Goundamani
*Senthil
*Salim Ghouse Manorama
*Silk Smitha Sindhu
*Vadivukkarasi

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet) Vaali || 05:01
|-
| 2 || Manu Koduthu || S. Janaki || 05:32
|-
| 3 || Naatukottai Chettiyar || Mano (singer) |Mano, Sunandha || 04:57
|- Mano || 05:08
|- Mano || 05:47
|}

==References==
 

==External links==
*   at oneindia entertainment

 
 
 
 
 


 