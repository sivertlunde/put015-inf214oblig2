Life Just Is
 
 
 
{{Infobox film
| name           = Life Just Is
| image          = Life Just Is Poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Life Just Is Poster, designed by Soledad del Real
| director       = Alex Barrett
| producer       = Tom Stuart
| writer         = Alex Barrett
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist |
* Will De Meo
* Jack Gordan
* Nathaniel Martello-White
* Fiona Ryan
* Jayne Wisener Paul Nicholls
}}
| music          = 
| cinematography = Yosuke Kato
| editing        = Murat Kebir
| studio         = {{Plainlist |
* Asalva
* Patchwork Productions
* Stanley Road Productions
}}
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Paul Nicholls.

==Plot==
Life Just Is tells the story of Pete, Tom, Claire, David, and Jay who are university graduates having trouble making the move into adult life. Amongst the hanging out and their daily routines simmers Petes desire to find a spiritual answer to lifes meaning, Jays desperate need not to get hurt again, and Tom and Claires ever increasing mutual attraction.

==Cast==
* Will De Meo as David
* Jack Gordon as Pete
* Nathaniel Martello-White as Tom
* Fiona Ryan as Claire
* Jayne Wisener as Jay Paul Nicholls as Bobby
* Rachel Bright as Anna
* Jason Croot as Walahfrid
* Vanessa Govinden as Michelle
* Andrew Hawley as Nick
* Lachlan McCall as Lawrence
* Joshua Osei as Vince
* Niall Phillips as Ollie
* Alix Wilton Regan as Zoe
* Gillian Wisener as Beth

==Themes==
Through its examination of the lives of a group of young adults in todays society, Life Just Is explores a number of contemporary themes concerning life after University and finding ones place in life. More specifically, the film looks at the existential clash between desire and fear, and the ways in which the latter prevents people from achieving the former. The film also examines attitudes towards religion and the standoff between religious ideas and modern values.

==Soundtrack==
Life Just Is features no original score and no non-diegetic music on its soundtrack, instead containing only music listened to by the characters themselves. In addition to the tracks featured in the film, there are also several moments when the characters sing songs. In one scene, the character David performs "  and "Moving Along" by Plan A.
 Animal Kingdom.

==Marketing==
Since the very early stages of production, the team behind the film have been posting comprehensive updates on the projects blog,  with the intention of being as open and as informative as possible about their creative process and experiences. In the run up to the shoot, the documenting expanded to include video updates posted on both the films blog and its YouTube channel.  During the shoot, actress Jayne Wisener created a series of popular video diaries.

After the shoot, the production launched an international competition to design their poster. An overwhelming success,  the competition (aimed predominately at students) attracted over 100 entries from 23 different institutions based in eight countries spread over four continents. The entries were simultaneously shortlisted by a panel of industry experts and a public vote   run on the films Facebook page.  On 28 February 2011 Soledad del Real was announced as the winner, as decided by the films director and producers.   All of the entries were later collated into an ebook given away for free on the films website. 

From September 2009 to May 2011, the team behind the film also ran a popular networking and screening night entitled Lifes Just Networking at The Duchess pub in Battersea, London.  They continue to run occasional events under their Lifes Just Events banner. 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 