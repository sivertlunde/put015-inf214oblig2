Your Very Good Health
{{Infobox film
| name           = Your Very Good Health
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = John Halas Joy Batchelor
| producer       = John Halas Joy Batchelor
| writer         = John Halas Joy Batchelor
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Matyas Seiber
| cinematography = 
| editing        = 
| studio         = Halas & Batchelor
| distributor    = 
| released       = 1948
| runtime        = 8 min 34 sec
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 animated public information film about the foundation of the National Health Service (NHS).    It explains how people would be affected by the National Health Service Act introduced under Clement Attlee.

==Synopsis== National Health Service Act. The narrator lists other public health services in existence at the time, and mentions problems with healthcare in Britain, such as the inconsistent coverage provided by hospitals and the limitations of the current insurance scheme for low-paid workers. Responding to Charleys objections, the film depicts hypothetical scenarios involving Charley falling off his bicycle and his wife becoming ill, in order to illustrate the benefits of the new national health system. The previously doubtful Charley is convinced, and proceeds to convince his neighbour George of the advantages of the new NHS.

==Production== postwar Labour Ministry of Health. 

==References==
 

==External links==
*   at BFI Screenonline
*   at The National Archives

 
 
 
 


 