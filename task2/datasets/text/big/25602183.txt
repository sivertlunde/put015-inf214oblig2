Pittsburgh (2006 film)
{{Infobox film
| name         = Pittsburgh 
| image        = Pittsburgh dvd.png Chris Bradley Kyle LaBrache
| producer     = Bernie Cahill Patrick Bradley Ryan Magnussen
| starring     = Jeff Goldblum Illeana Douglas Ed Begley, Jr. Moby
| music        = David Gregory Byrne Roar Prosperity Pictures
| released     =  
| runtime      = 84 minutes
| country      = United States
| language     = English
}} green card summer Regional regional theatre Transylvania 6-5000, Illeana Douglas (who directed Goldblum in her own film Supermarket), Moby (as Douglass boyfriend), Conan OBrien and Craig Kilborn (hosting Goldblum on their respective shows), Alanis Morissette, and Tom Cavanagh.

Though some of the primary events of the story took place (for example, Goldblum did perform The Music Man with the Pittsburgh Civic Light Opera at the Benedum Center, though that performance was staged solely for the film) "Pittsburgh" was, with a few improvisational flourishes, totally scripted. Many of the deleted scenes included on the DVD release of the film are far more overtly comic than those the filmmakers chose to include in the finished product and make the fictional nature of the project more obvious.

==References==
 
 
* Conan Obrien as Himself.

==External links==
* 

 
 
 
 
 
 


 