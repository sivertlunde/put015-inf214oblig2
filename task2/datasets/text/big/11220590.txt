The War on Democracy
 
{{Infobox film name       = The War on Democracy image      = The War on Democracy, Poster.jpg director   = Christopher Martin John Pilger writer     = John Pilger producer   = Wayne Young
|distributor= Lions Gate studio     = Youngheart Entertainment Granada Productions budget     = released   = 15 June 2007 (United Kingdom) runtime    = 96 min. language   = English
}}

The War on Democracy is a 2007 documentary film directed by the British filmmakers Christopher Martin and John Pilger, who also wrote the narration.  Focusing on the political states of nations in Latin America, the film is a rebuke of both the United States intervention in foreign countries domestic politics and its "War on Terrorism". The film was first released in the United Kingdom on June 15, 2007.

== Production ==
The film was produced over a two-year period. Carl Deal, chief archivist on the Michael Moore films Fahrenheit 9/11 and Bowling for Columbine, provided the archive footage used in the film. It is mastered in high-definition video. 

== Distribution ==
The War on Democracy was screened at both the 2007 Cannes Film Festival and the Galway Film Festival. The film was sold to distributors Lionsgate for distribution in the U.K. and Hopscotch distribution in Australia and New Zealand.  Pre-release screening took place at two Fopp locations on June 12, 2007, including one that was followed by a question and answer session with co-director John Pilger. 

== Soundtrack ==
*Original Music - Jesper Mattsson and Makoto Sakamoto A Change Is Gonna Come" - Sam Cooke
*"(Something Inside) So Strong" - Labi Siffre 
*"La Soga" - Ali Primera
*"Jääkärien Marssi OP. 91A" - Jean Sibelius
*"Plegaria a un Labrador" - Víctor Jara

==Reception==
Peter Bradshaw wrote: 
 

==Box office==
The War on Democracy grossed $199,500 at the box office in Australia. 

== See also == The Revolution Will Not Be Televised, documentary filmed from within the Chavez camp during the failed coup of 2002. South of the Border, documentary by Oliver Stone
*Bolivarian revolution
*Hugo Chávez
*Economy of Venezuela

== References ==
 

==External links==
*  , Complete documentary online, at  
*  , BBC News World 
*  , Empire (magazine)|Empire
*  , Narconews|NarcoNews
*  

 

 
 
 
 
 
 
 
 