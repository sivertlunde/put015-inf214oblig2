Joseph (film)
{{Infobox film
| name           = Joseph
| image          = 
| alt            =  
| caption        =  Roger Young
| producer       = {{Plainlist|
*Luca Bernabei
*Laura Fattori}}
| writer         = {{Plainlist|
*James Carrington
*Lionel Chetwynd}}
| starring       = {{Plainlist|
*Ben Kingsley
*Paul Mercurio
*Martin Landau
*Lesley Ann Warren}}
| music          = {{Plainlist|
*Marco Frisina
*Ennio Morricone}}
| cinematography = Raffaele Mertes
| editing        = Benjamin A. Weissman
| studio         =  TNT
| released       =  
| runtime        = 
| country        = 
| language       = English
| budget         = 
| gross          = 
}} Joseph from TNT and was filmed in Morocco.

== Plot ==
Joseph, a Hebrew, is an Egyptian slave to Potiphar, chief of Pharaohs palace guard.  When Joseph is placed under the charge of Ednan, Potiphars overseer, Ednan torments Joseph for his refusal to show deference for the Egyptian god Amun.  But Joseph eventually earns Ednans respect when he reveals that he knows how to read, and Ednan starts relying more and more on Joseph.  

Joseph is eventually put in charge of Potiphars home, but his wife begins to desire Joseph and repeatedly, but unsuccessfully, tries to seduce him.  She ultimately becomes so angry at Joseph that she falsely accuses him of raping her and demands his execution.  But Potiphar, knowing Josephs trustworthiness—and his wifes infidelity—has his doubts.  Potiphar speaks to Joseph alone and demands that Joseph give him some reason to trust him.  

Joseph gives Potiphar his lifes story from his youth:  Joseph is a son of Jacob (a.k.a. Israel) and the first of two sons to his mother Rachel, who later died giving birth to his younger full-brother Benjamin.  Joseph also had a half-sister, Dinah, ten half-brothers.  Six of these half-brothers came through his step-mother, (who was also his aunt), Leah, Rachels older sister.  Jacob and his extended family make a pilgrimage to a fertile plane near the Hivite town of Shechem, where they are visited by the citys king, Hamor, along with his counselor and his son.  

While Jacob and Hamor make arrangements to pay taxes for the land, Hamors son, also named Shechem, notices Dinah and takes a liking to her.  As they leave, Hamor invites Jacob and his family to come to a wedding celebration, and asks that Dinah accompany them.  After Hamor leaves, Simeon, one of Josephs half-brothers, disliking the way Shechem looked at Dinah, suggests they resist Hamor by force, but Jacob rebukes Simeons irascibility.  The night of the wedding celebration, Shechem catches Dinah and rapes her.  The next day Hamor and Shechem come to talk to Jacob about making amends by having Shechem marry Dinah.  Jacob replies that Dinah can only marry a man who is of her faith.  After consideration Hamor agrees that all the Shechemite men will convert to Jacobs faith—and enter into the covenant via circumcision.  While Jacob is satisfied, Simeon seeks revenge against Shechem.  While the Shechemites are recovering from their circumcisions, the brothers and other members of the tribe attack the fortified village, setting many of their buildings ablaze and killing many Shechemites including Shechem.  Jacob is furious with his sons ruthless actions and announces that now that they have become a stench to the people of the land, they must leave the area.  During this ordeal of moving again, at Beth-El, Rachel gives birth to Benjamin, but dies soon afterward in exhaustion from the journey and the childbirth.  

Nine years later, Reuben, Josephs oldest half-brother, desires Bilhah, one of Jacobs women.  Zilpah, one of Leahs handmaids, sees the couple together and tells Jacob, who angrily declares to Reuben that authority over the family will fall to Joseph instead.  This adds even more fuel to Reubens (and the other brothers) jealous anger toward Joseph.  For reasons such as these, Joseph has seen the destruction infidelity can bring, and would never do such a thing, he tells Potiphar.

Nevertheless, the story continues that while the brothers are out at work in the fields, they decide to kill and eat one of their own lambs, an act expressly forbidden by Jacob.  The brothers only taunt Joseph mercilessly after he reminds them of Jacobs rule.  They later try to tell Jacob that the lamb was killed by a wild animal; Jacob, a lifelong shepherd, is not fooled and he berates his sons for disobeying him.  The brothers blame Joseph for Jacobs anger and make reference to betrayal and spying in regard to Joseph.  

Jacob is so pleased with Joseph that he gives him a beautiful coat, which increases the jealousy among the brothers.  Further exacerbating their hatred of Joseph is his frequent interpretations of his dreams that one day his brothers and his father will fall prostrate before him.  Spurred on by Simeon, the brothers discuss killing Joseph, but Reuben says he will not have the blood of his brother on his hands.  Instead, the brothers throw Joseph into a dry well pit. They then sell Joseph into slavery to a group of Ishmaelite traders.  Back at the encampment, the sons then show Jacob the coat (bloodied by the brothers) and tell him that Joseph has been killed by a wild animal.  Jacob is overcome with grief.  

After listening to Josephs life story, Potiphar asks him to tell him exactly what happened with his wife.  Joseph tells Potiphar of his wifes attempts at seduction and his refusal to accede to her demands.  Potiphar then calls in the household.  He announces that Joseph will go to Pharaohs prison; Potiphars wife complains that he has humiliated her, but he merely responds that one humiliation deserves another.  

Seven years later Joseph, now in charge of many of the prisoners, has earned a reputation as a talented interpreter of dreams.  He makes two interpretations of the dreams of the Royal cupbearer and the Royal baker, both imprisoned on suspicion of theft.  He says the cupbearers dream means that he will soon be forgiven and returned to his post, whereas the bakers dream means that Pharaoh will have him executed.  Both of these interpretations later come to pass just as Joseph said they would.  

In Canaan, Judah, another of Josephs half-brothers, had left Jacobs encampment because of Jacobs unabated grief over Joseph.  During his self-imposed exile Judah has sex with a prostitute (whose face is partially hidden from him).  Not having enough money, Judah gives the woman his staff and seal as assurance that he will pay her the following day, but when Judah returns the woman, the staff and his seal are all gone.  When Judah later returns to Jacobs camp he seeks advice on how to deal with his daughter-in-law, Tamar; Tamar married Judahs firstborn son who died before they could have children; his second son suffered the same fate, and Judah is afraid to let her marry his third son for fear that he too will die.  But Tamar, twice widowed and not remarried, is now pregnant and refuses to reveal the fathers identity.  Jacob reminds Judah of the Israelite custom which decrees Tamar must be put to death, so when Judah goes to see Tamar she returns Judahs staff and seal; Judah shockingly realizes that Tamar was masquerading as the prostitute Judah had sex with, making Judah himself the father of her child.  With this revelation, Judah spares Tamars life.

Back in Egypt, Pharaoh himself has two disturbing dreams:  His first dream involved seven fat cows being swallowed by seven sickly cows, the second dream is a similar one involving seven full ears of corn consumed by seven withered ears.  After unsuccessfully asking his vizier and his staff what the dreams mean, the cupbearer and Potiphar inform Pharaoh that Joseph can interpret his dreams, so Pharaoh summons Joseph from prison. Josephs interpretation is that the seven fat cows and the seven full ears of corn mean that there will be seven years of great plenty; the seven sickly cows and the seven thin ears of corn signify that after the seven year abundance will follow seven years of extreme famine, which could take many lives.

At first Pharaoh, calling Josephs interpretations "madness", has him thrown back in prison, but that night Pharaoh is again plagued by the same dreams.  Realizing the dreams are too important to ignore, Pharaoh again summons Joseph from prison and asks his advice about what can be done to avoid the deaths.  Joseph suggests that Pharaoh appoint a steward to have all the farmers give one-fifth of their crop to Pharaoh for storage for the coming famine.  After some consideration, Pharaoh decides that no one, including his own advisers, has the ability to carry out Josephs plan except for Joseph himself, so Pharaoh appoints Joseph governor over all of Egypt, second only to Pharaoh himself.  He then gives Joseph a new name: Zaphenath-Paneah (translated in the film as "the savior"), and also gives Joseph a wife, Asenath, confidant to Pharaohs wife and daughter of the high priest of On.  Potiphar, who now serves Joseph, gives Ednan into Josephs charge as his assistant.  
 Manasseh and Ephraim.  With the onslaught of famine, times are difficult at best, especially in Canaan.  Someone in Jacobs camp tells of the abundance of grain in Egypt, so Jacob sends all of his sons (except Benjamin) to Egypt.  On their arrival, Joseph recognizes his brothers (though they dont recognize him) and, because he notices they havent changed in the years since they sold him into slavery, accuses them of spying and has them thrown into prison.  Three days later Joseph gives them a chance to prove their innocence, by bringing Benjamin with them when they come back to Egypt.  To ensure their return, Joseph sends Simeon back to prison.  Initially, Jacob is firmly against the idea; Benjamin is Jacobs last link to the late Rachel, whom Jacob loved more than Leah.  But when their food runs out, Jacob is forced to admit theres no other choice, so he reluctantly allows Benjamin to go to Egypt with his half-brothers.  

On their way back home the second time, Josephs palace guards arrest Benjamin for theft (one of Josephs silver cups was planted in Benjamins grain sack) and they all return to Egypt.  Joseph, still not recognized by his brothers, declares that Benjamin will stay in Egypt but the rest can leave.  Simeon and most of the others overpower the guards by grabbing their spears, swearing that they would rather die than leave Benjamin behind (and be subject to Jacobs wrath).  Joseph confronts them about their actions; that they are only now willing to put their lives on the line for another brother, but not for the first one (meaning himself).  Joseph then tearfully reveals his true identity to all of them.  Benjamin immediately embraces Joseph, but the others, overwhelmed by shock and shame, drop their spears.  Joseph embraces each of them in turn, assuring them all of his love and forgiveness, especially Simeon, who tearfully admits that he is most to blame for Joseph having been sold into slavery more than 20 years earlier.  As he embraces each of them, Joseph explains that God used their evil intentions for their ultimate good, preparing Joseph for his current position so that he can help and provide for his extended family.

Joseph sends his brothers home to bring Jacob and the entire settlement back to Egypt where Joseph can guarantee that they will be well provided for during the remainder of the famine.  Jacob and his extended family arrive in Egypt where he is emotionally reunited with Joseph and meets his children for the first time.

== Cast ==

* Ben Kingsley - Potiphar Joseph
* Martin Landau - Jacob
* Lesley Ann Warren - Zuleika (legendary)|Potiphars Wife
* Alice Krige - Rachel
* Dominique Sanda - Leah
* Warren Clarke - Ednan
* Monica Bellucci - Pharaohs Wife Pharaoh
* Valeria Cavalli - Asenath Kelly Miller Tamar
* Gloria Carlin - Bilhah Reuben
* Simeon
* Colin Bruce - Levi Judah
* Dan
* Rodolfo Corsato - Naphtali Gad
* Silvestre Tobias - Asher
* Diego Wallraff - Issachar
* Michael Zimmermann - Zebulun
* Jamie Glover - Benjamin
* Rinaldo Rocco - Young Joseph (17 yrs)
* Timur Yusef - Young Joseph (8 yrs)
* Paloma Baeza - Dinah
* Brett Warren - Young Benjamin (9 yrs)
* Anna Mazzotti - Zilpa
* Andrew Clover - Shechem
* Arthur Brauss - Hamor
* Eric P. Caspar - Bera
* Anton Alexander - Hirah
* Milton Johns - Cupbearer
* Renato Scarpa - Baker
* Peter Eyre - Vizir
* Timothy Bateson - Priest Ishmaelite
* Josh Maguire Manasseh
* Gabriel Thomson - Ephraim
* Oliver Cotton - Architect
* Anna Zapparoli - Mistress

== Crew ==
 Roger Young
* Teleplay by: Lionel Chetwynd
** based on the Novel by: James Carrington
* Produced by: Lorenzo Minoli and Gerald Rafshoon
* Music by: Marco Frisina and Ennio Morricone AIC
* ACE
* Costumes by: Enrico Sabbatini

== Awards ==

* Emmy Award
** Outstanding Miniseries
* Emmy Award-Nominations
** Art Direction
** Casting
** Sound Editing
** Supporting Actor: Ben Kingsley

* Writers Guild of America-Nomination - Lionel Chetwynd

== See also ==
*List of historical drama films
*List of films featuring slavery

== External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 