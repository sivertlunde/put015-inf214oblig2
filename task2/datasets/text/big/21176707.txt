My Wife's Murder
{{Infobox film
| name       = My Wifes Murder
| image      = My_Wifes_Murder.jpg
| written by = Atul Sabharwal
| director   = Jiji Philip
| producer   = Anil Kapoor Film Company Ram Gopal Varma
| music      = Amar Mohile  
| starring   = Anil Kapoor Suchitra Krishnamurti Boman Irani Nandana Sen Rajesh Tandon
| released   = 19 August 2005
| runtime    = 103 min
| country    = India
| language   = hindi
}} Hindi crime crime thriller directed by Jijy Philip. The cast includes Anil Kapoor, Suchitra Krishnamurti, Boman Irani and Nandana Sen. The film is a remake of  Telugu film, Madhyanam Hathya directed by Ram Gopal Varma himself.

== Plot ==
Police Inspector Tejpal Randhawa (Boman Irani) is assigned to investigate the case of the dead woman whose body is recovered from a small pond. Insp. Tejpal checks if this matter can be linked with a missing persons report filed by Ravi Patwardhan (Anil Kapoor) and his father-in-law. The dead woman is identified as Sheela (Suchitra Krishnamoorthi), Ravis wife. According to Ravi, Sheela had left their home to go to visit her parents. When she had not arrived at their house 24 hours later, he himself had gone to their house, and on not being able to locate her, had accompanied his father-in-law to the nearest police station and filed a report as she is missing. Insp. Tejpal concludes that Sheela was waylaid on her way to her parents house by person(s) unknown, beaten, and her body was left in the pond. But this case puzzles him, as there was no apparent motive for unknown person(s) to waylay her, as no money was taken, and her body does not show any signs of sexual molestation. Taking these facts into consideration, Insp. Tejpal starts to suspect Ravi. Ravis assistant (Nandana Sen) tries to help Ravi. However, her involvement makes the matter worse. The movie then takes the audience through thrilling tale of how Ravi makes out of this matter and truth behind Sheelas murder.

== Cast ==
* Anil Kapoor as Ravi Patwardhan: He is a man whose wife is murdered and he has to make it out of the matter proving himself innocent.
* Suchitra Krishnamoorthi as Sheela: Ravis wife who gets murdered.
* Boman Irani as Inspector Tejpal Randhawa: He investigates the case of Sheelas murder.
* Nandana Sen: Ravis assistant who tries to help Ravi, however, unwillingly gets too much involved making the matter worse for her, her boyfriend and their relationship.
* Rajesh Tandon as Nandana Sens boyfriend.

== References ==
 

==External links==
* 

 

 
 
 
 
 