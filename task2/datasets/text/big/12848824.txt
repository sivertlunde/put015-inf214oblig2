Bingo Crosbyana
{{Infobox Hollywood cartoon
|cartoon_name=Bingo Crosbyana
|series=Merrie Melodies
|image=bingocrosbyana.jpg
|caption=Title card
|director=Friz Freleng|I. Freleng
|story_artist=
|animator=Cal Dalton Sandy Walker
|layout_artist=
|background_artist=
|voice_actor=Billy Bletcher
|musician=Norman Spencer
|producer=Leon Schlesinger
|distributor=Warner Bros.
|release_date=May 30, 1936
|color_process=Technicolor
|runtime=7 min (one reel)
|movie_language=English
}}
Bingo Crosbyana is a 1936 Merrie Melodies cartoon short directed by Friz Freleng, and notable for its title song, composed by Sanford Green and with lyrics by Irving Kahal.

==Plot==
The plot revolves around a group of fun-loving anthropomorphic insects which have taken over a kitchen. The female insects promptly become enthralled by a crooning show-off of a fly, Bingo Crosbyana, "the crooning hit of all Havana", a caricature of the singer Bing Crosby, who incurs the jealous resentment of their boyfriends. Bingo, however, literally shows his colors (yellow) when a spider invades the kitchen, leaving Bingo to cower in fear inside a roll of wax paper. After the boyfriends team up to trap the spider on a sheet of fly paper, Bingo emerges from the wax paper roll and attempts to resume his braggadocio, only to be put in his place decisively.

==Trivia== Let It Be Me) which Bing Crosby initiated lawsuits to suppress because they portrayed him in what Crosby considered a defamatory light. In this case, he objected to his portrayal as a vainglorious coward and to the imitation of his voice. Cohen (2004), p. 39-40 

According to The Hollywood Reporter, Paramount Pictures was suing Warner Bros. alongside Crosby. Their law firm wanted the distribution of the film to cease. Apparently they lost their case. 
 Cohen (2004), p. 39-40 

==Sources==
*  

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 


 