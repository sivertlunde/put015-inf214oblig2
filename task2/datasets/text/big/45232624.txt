Bad Parents
{{Infobox film
| name           = Bad Parents
| image          = File:Bad Parents 1446 (8056810858).jpg
| alt            =
| caption        = Janeane Garofalo in publicity photo
| director       = Caytha Jentis
| producer       = {{plainlist|
* Dorothy Fucito
* Caytha Jentis
}}
| writer         = Caytha Jentis
| based on       =  
| starring       = {{plainlist|
* Janeane Garofalo
* Christopher Titus
* Michael Boatman
* Cheri Oteri
* Kristen Johnston
* Rebecca Budig
}}
| music          = James Harrell
| cinematography = Anthony Savini
| editing        = Verne Mattson
| studio         = Bad Parents
| distributor    = Gaiam Vivendi Entertainment
| released       =   }}
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Bad Parents is a 2012 comedy film written and directed by Caytha Jentis and starring Janeane Garofalo. The movie humorously showcases New Jersey soccer moms becoming obsessed with their childrens role in the sport.  The supporting cast features Christopher Titus and Cheri Oteri. 

==Cast==
* Janeane Garofalo as Kathy
* Christopher Titus as Nick
* Michael Boatman as Gary
* Cheri Oteri as Melissa
* Kristen Johnston as Tracy
* Rebecca Budig as Allison

==Production== Morris County talent.   Jentis based the film on her own experiences. 

==Release==
Bad Parents premiered on October 3, 2012, at the Montclair Film Festival  and later played at Ridgewood, New Jersey, on February 23, 2014. 

==External links==
*  
*  

== References ==
 

 
 
 
 
 
 
 
 


 