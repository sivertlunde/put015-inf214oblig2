Upbeat in Music
{{Infobox film|
  name           = Upbeat in Music |
  starring       = Marian Anderson Tex Beneke Benny Goodmans Orchestra  Perry Como Tommy Dorsey Duke Ellington Glenn Miller  Benny Goodman  The Pied Pipers  Frank Sinatra Paul Whiteman |
  released       =  | 
  runtime        = 17 minutes |
  language       = English |
  country        = United States |
}}

Upbeat in Music is a 1943 film short produced as part of The March of Time series distributed theatrically by 20th Century Fox.  The film has significant history as being early film appearances of Frank Sinatra and Perry Como. The film also features footage of Glenn Miller in uniform as a Captain in the U.S. Air Force leading the U.S. Army Training Command Band.

==References==
 

==External links==
* 

 
 
 
 
 
 

 