House of Bamboo
{{Infobox film
| name           = House of Bamboo
| image          = Houseofbambooposterart.jpg
| size           =
| alt            =
| caption        = Theatrical release poster
| producer       = Buddy Adler
| director       = Samuel Fuller
| screenplay     = Harry Kleiner  Samuel Fuller Shirley Yamaguchi Cameron Mitchell
| music          = Leigh Harline
| cinematography = Joseph MacDonald James B. Clark
| distributor    = 20th Century Fox
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $1,380,000 
| gross          = $1.7 million (US)  
}}
House of Bamboo is a 1955 American film noir shot in CinemaScope and DeLuxe Color. It was directed and co-written by Samuel Fuller. 

The film is a loose remake of The Street with No Name (1948), using the same screenwriter (Harry Kleiner) and cinematographer (Joseph MacDonald).

==Plot==
In 1954, a military train guarded by American soldiers and Japanese police is attacked as it travels between Kyoto and Tokyo. During the raid, which is carried out with great precision, an American sergeant is killed, and the trains cargo of guns and ammunition is stolen. The crime is investigated by Capt. Hanson, an American, and Japanese police inspector Kita, who, five weeks later, are concerned when a thief named Webber is shot with some of the stolen bullets. As Webber lies dying in a Tokyo hospital, he is questioned by Hanson and Kita, and although Webber was left for dead by his gang during a thwarted robbery, he refuses to implicate his cohorts, who presumably are responsible for the earlier crime. Webber, who is also an American, does reveal, however, that he is secretly married to a Japanese woman named Mariko. Among Webbers possessions is a letter from an American named Eddie Spanier, who wants to join Webber in Japan after his release from a U.S. prison.

Three weeks later, Eddie arrives in Tokyo and finds Mariko, who is initially afraid that he is one of the men responsible for her husbands death. Eddie gains Marikos trust with a photograph of himself and Webber, then warns her to keep quiet about her marriage so that she will not be in danger from Webbers killers. Later, Eddie goes to a pachinko parlor, in which patrons gamble on intricate machines similar to pinball machines. There, Eddie attempts to sell "protection" to the manager, but when he tries the same tactic at another such parlor, he is beaten and warned to leave by racketeer Sandy Dawson and his henchmen, Griff, Charlie, Willy and Phil. Intrigued by Eddies presence in Japan, Sandy arranges for him to be arrested, and Sandys secret informer, who is connected to the police department, obtains Eddies rap sheet. Convinced of Eddies aptitude for crime, Sandy invites him to join his gang, which consists of former American servicemen who have been dishonorably discharged. After his acceptance into the gang, Eddie secretly meets with Kita and Hanson, for whom he is working undercover. Needing help from someone he can trust, Eddie asks Mariko to live with him as his "kimono girl," although he does not reveal his identity as a military police investigator. Hoping to discover who killed her husband, Mariko resides with Eddie despite being ostracized by her neighbors, who do not know that her relationship with the foreigner is platonic. As time passes, Sandy grows to trust Eddie, although Eddie is shocked during a robbery when a wounded gang member is killed by Griff to prevent him from talking. Eddie is also wounded, but Sandy makes an exception to his rule of killing fallen men and saves him.

Eddie finally informs the worried Mariko that his real name is Sgt. Kenner, and that he is investigating Sandy. Meanwhile, Griff, Sandys "ichiban" or "number one boy", becomes jealous of Sandys reliance upon Eddie, and Sandy relieves the hot-headed Griff of his duties. The next day, Mariko, who has fallen in love with Eddie, notifies Kita and Hanson about a planned robbery, but Sandys informant, reporter Ceram, warns him that the police are poised to capture him. After the robbery is aborted, Sandy kills Griff, whom he mistakenly assumes tipped off the police. Ceram informs Sandy of his mistake, and Sandy retaliates by setting Eddie up to be killed by the Japanese police during a robbery of a pearl broker. When the plan fails, Sandy is chased by the police up to a rooftop amusement park, but after an intense gunfight, Eddie succeeds in shooting and killing Sandy. Later, wearing his military uniform, Eddie walks with Mariko in a Tokyo park.

==Cast==
* Robert Ryan as Sandy Dawson
* Robert Stack as Eddie Kenner Shirley Yamaguchi as Mariko Cameron Mitchell as Griff
* Brad Dexter as Capt. Hanson
* Sessue Hayakawa as Inspector Kitz
* DeForest Kelley as Charlie
* Biff Elliot as Webber
* Sandro Giglio as Ceram
* Elko Hanabusa as Japanese Screaming Woman

==Background==
The narration at the films beginning tells the viewer that the film was photographed entirely on location in Tokyo, Yokohama, and the Japanese countryside. At movies end, an  acknowledgments credit thanks "the Military Police of the U.S. Army Forces Far East and the Eighth Army, as well as the Government of Japan and the Tokyo Metropolitan Police Department" for their cooperation with the films production.

==Reception==

===Critical response===
The staff of Variety (magazine)|Variety magazine wrote of the film, "Novelty of scene and a warm, believable performance by Japanese star Shirley Yamaguchi are two of the better values in the production. Had story treatment and direction been on the same level of excellence, House would have been an all round good show. Pictorially, the film is beautiful to see; the talks mostly in the terse, tough idiom of yesteryear mob pix." 
 Battle Hymn) with the philosophical inquiry of the best noirs." 

Interestingly, for many years after its initial release, the film was only seen on TV in pan-and-scan prints, leading people to believe that DeForest Kelley has a small role near the end of the film. When Fox finally struck a new 35mm CinemaScope print for a film festival in the 1990s, viewers were surprised to see that Kelley was in the film all the way through—he was just always off to one side and thus had been panned out of the frame.

===References in other films=== Minority Report.

==References==
 

==External links==
*  
*  
*  
*  
*   film trailer at YouTube

 

 
 
 
 
 
 
 
 
 
 
 