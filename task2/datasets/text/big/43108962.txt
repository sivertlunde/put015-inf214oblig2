Grahan (2014 film)
{{Infobox film
| name           = Grahan
| image          = Poster of Grahan.jpg
| caption        = Promotional poster of Grahan
| director       = Tilak Sarma
| producer       = M.Choudhury
| writer         = Bilkis Mazumdar
| starring       = Diganta Hazarika, Farhina Choudhary, Babi Baruah
| music          = Rideep Das
| cinematography = Rajiv Saikia
| released       =  
| editing        = Rajiv Saikia
| banner         = BMC Media Production, Guwahati
| distributor    = 
| Editing_Studio = Round The Globe
| re-recording   = Studio K7
| country        = India
| language       = Assamese
| budget         = 
| box office     = 




}}
Grahan ( ) is an  Assamese language romantic drama film directed by Tilak Sarma and produced by M.Choudhury under the banner of BMC Media Production, Guwahati. It stars Diganta Hazarika, Farhina Choudhary in the lead roles.The film will release in on 27 June, 2014.The story, written by Bilkis Mazumdar, is based on love and friendship as seen through the eyes of the younger generation. The music is composed by Rideep Das. There are 5 songs in the movie, which also includes an item song. Nil and Parbin Sultana have performed in the item number. The songs have been sung by Mahalakshmi Iyer, Rideep Das and Poly Saikia. The song Phoole xulae’s lyric has been written by famous poet Hiren Bhattyacharya. Mituparna Chakrabarty is the Choreographer and Rajiv Saikia is the cinematographer of the movie.


== Cast ==
* Diganta Hazarika
* Farhina Choudhury
* Babi Baruah
* Pankaj Pujari
* Rajpoot Saikia
* Farhan Choudhury
* Tanveer Mazumder
* Neil Das
* Parbin Sultana

==External links==
*  
*  
*  
*  
*   from rupaliparda.com

 
 
 


 