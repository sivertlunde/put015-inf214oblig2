The Changeling (1980 film)
 
{{Infobox film
| name           = The Changeling
| image          = Changeling ver1.jpg
| director       = Peter Medak
| producer       = Joel B. Michaels Garth H. Drabinsky Russell Hunter William Gray Diana Maddox
| starring       = George C. Scott Trish Van Devere Melvyn Douglas John Colicos Jean Marsh Helen Burns Madeleine Sherwood Rick Wilkins
| cinematography = John Coquillon Lou Lombardo (sup)
| distributor    = Associated Film Distributors
| released       = March 28, 1980 (U.S. & Canada)
| runtime        = 107 minutes English
| country        = Canada   United States
| awards         =
| budget         = $CAD7,600,000 gross = $5.3 million Richard Nowell, Blood Money: A History of the First Teen Slasher Film Cycle Continuum, 2011 p 260 
}} Russell Hunter said he experienced while he was living in the Henry Treat Rogers Mansion of Denver, Colorado.  

==Plot== Washington state upstate New York. In suburban Seattle, John rents a large, old and eerie Victorian architecture|Victorian-era mansion and begins piecing his life back together.
 United States senator, Joseph Carmichael.

John subsequently discovers that the real Joseph Carmichael was murdered by his father, Richard. Joseph was a crippled, sickly child, and in the event of his death before his 21st birthday, the family fortune (which he inherited from his late mother) would pass to charity. Desperate to keep control of the fortune, Josephs father drowned young Joseph in the bathtub, secretly replaced him with a healthy orphan, and took him to Europe in the guise of seeking a treatment or cure. He returned several years later with the impostor, now grown and "cured" of his illness, and continued as if nothing had happened.

Now, the ghost of the real Joseph haunts the house, making great efforts to persuade John to investigate his murder, and give him some form of justice. Johns investigation leads him to a property that was once owned by the Carmichael family, and after convincing Mrs. Gray (the owner of the property and whose daughter has seen Josephs ghost), John discovers the skeletal remains of Joseph in a well, as well as his birth medal.

After refusing to hand the medal over to the police, John attempts to speak to Senator Carmichael as he is about to depart by plane but is restrained by police. It is then revealed that the Senator has an identical medal to the one John found. The Senator then sends a policeman, Captain Dewitt to Johns home in an attempt to retrieve the medal, John refuses and when Dewitt leaves to obtain a search warrant, his vehicle "mysteriously" crashes, killing him.

After hearing of Dewitts death, the Senator finally agrees to listen to Johns story and meets with him. John reveals the entire story to the Senator, that his father murdered his natural son and replaced him with a changeling, which is the Senator. The Senator refuses to believe the story and angrily berates John for accusing his father, whom he claims was a "loving man", of murder. John then leaves the Senator Josephs medal, files and the only copy of the seance recording and apologizes. The Senator threatens John that there will be consequences if he has told anyone else of his story.

Meanwhile, Johns realtor and friend, Claire, goes to the house alone in an attempt to find John and is chased by Josephs wheelchair until she falls down the stairs. Meanwhile the house begins to shake and rumble. John arrives, and escorts Claire outside, and then goes back inside to try and stop the ghost of Joseph. A strong wind causes John to fall from the second story, but he survives. Joseph then lights the house on fire.

Meanwhile, back at the Senators home, he is observing the two birth medallions, and throws Josephs away while placing his own on a portrait of his father. Suddenly, the picture and the desk start shaking violently and an illusion of the Senator is transported to the house and begins to climb the stairs which then crumble. John sees the Senators illusion walking up the main staircase, and narrowly escapes being crushed by a chandelier. Meanwhile the Senators illusion then climbs the stairs to Josephs attic room where he witnesses how his father murdered the real Joseph by drowning him. Meanwhile, back at the mansion, the Senator himself suffers a heart attack and dies as Josephs attic room explodes. John and Claire arrive and see the Senators body being hoisted away. The ambulance then passes the Carmichael mansion which is now completely engulfed in flames.

The next morning, at the ruins of the mansion, Josephs burnt wheelchair is seen sitting upright and his music box then opens and begins playing a lullaby, possibly signifying that the justice he wanted has finally been served.

== Cast ==
*George C. Scott as John Russell, composer 
*Melvyn Douglas as Sen. Joseph Carmichael
*Trish Van Devere as Claire Norman
*Eric Christmas as Albert Harmon
*John Colicos as De Witt
*Jean Marsh as Joanna Russell
*Roberta Maxwell as Eva Lingstrom
*Barry Morse as Doctor Pemberton
* Terence Kelly as Sgt. Durban

==Production== University of Hotel Europe in Vancouver. The scenes at the senators home were filmed at what was then Royal Roads Military College (now Royal Roads University) in Victoria, British Columbia|Victoria, British Columbia. Interior scenes of the mansion where Scotts character lives were a set, as were the exterior scenes; the house was a giant mock-up.

==Awards and recognition== Genie Award for Best Canadian Film. It also won the following Genie Awards:
*Best Foreign Actor&nbsp;- George C. Scott   
*Best Foreign Actress&nbsp;- Trish Van Devere  William Gray and Diana Maddox 
*Best Art Design&nbsp;- Trevor Williams 
*Best Cinematography&nbsp;- John Coquillon 
*Best Sound&nbsp;- Joe Grimaldi, Austin Grimaldi, Dino Pigat, Karl Scherer 
*Best Sound Editing&nbsp;- Patrick Drummond, Dennis Drummond, Robert Grieve 

This film was #54 on Bravo (US TV channel)|Bravos 100 Scariest Movie Moments. 
Director Martin Scorsese placed The Changeling on his list of the 11 scariest horror films of all time. 

==Soundtrack==
The Soundtrack to The Changeling was released by Percepto Records on CD on December 21, 2001 and was limited to 1,000 copies.  On April 13, 2007, Percepto released a 2-CD "Deluxe Edition" of the soundtrack, which was also limited to 1,000 copies and has subsequently been sold out. 

 
 
;Regular Edition Track Listing
# Main Title 2:31
# The First Look 1:46
# First Chill 1:31
# Music Box Theme for Piano
# Country Ride 1:04
# Bathtub Reflections 3:03
# Secret Door 3:31 
# The Attic 2:45 
# Music Box Theme 1:45
# The Ball 3:15 
# The Seance 7:31 
# The Killing 3:42 
# Carmichael Reflects / On the Floor 2:18
# Face On the Bedroom Floor 1:59
# Chain Reaction 3:46
# The Doors 1:10
# Mirror, Mirror On The Wall 1:11 
# The Attic Calls Clair 3:52 
# Resolution 5:53
# End Title 3:10
# The Seance - Alternate Version (bonus) 7:09
# Carmichaels Demise (bonus) 3:43 
# Piano Solos (bonus) 1:37
# Alternate End Title (bonus) 2:31
 
;Deluxe Edition Track Listing
Disc 1
# Main Titles 2:33
# Piano Source :57
# Arrival At The House 1:48
# Piano Source 1:11
# Piano Source :13
# First Chill 1:33
# The Door Opens By Itself :21
# Music Box Theme For Piano 2:06
# Country Ride 1:06
# Bathtub Reflections 3:05
# Finding the Secret Door 3:33
# Up Into The Attic 2:47
# Music Box Theme 1:47
# The Wheelchair :25
# Microfilm Research / Cemetery 1:30
# Ball Over The Bridge / Its Back! 3:17
# The Seance / Talk To Us! 7:14
# Murder Flashback 3:43
# Wheelchair / Carmichael Tower 1:00
# Carmichael Reflects :34
# The House On The Lake 1:56
# Breaking Into The House :54
# Face On The Bedroom Floor 2:01
# The Chain Appears In The Dirt 3:47
# All The Doors Shut 1:12
# Mirror Mirror (Vision Of Death) 1:13
# Russell Goes To See Carmichael 2:02
# The Attic Calls Clair 3:53
# The Big Finale / Resolution 5:55
# Music Box / End Credits 3:13

Disc 2
# The Seance - Alternate Version 7:11
# Carmichaels Demise (Unused Cue) 3:45
# Alternate End Title 2:31
# Unknown Cue 1:51
# Unused String Quartet (V1) :48
# Unused String Quartet (V2) 1:17
# Solo Celeste :47
 

==See also==
* List of ghost films
*The Changeling 2 an unofficial sequel by Lamberto Bava

== References ==
 

==External links==
*  
*   
*  

 
 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 