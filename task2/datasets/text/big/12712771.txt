A Gentleman of Paris (1927 film)
{{Infobox film
| name           = A Gentleman of Paris
| image          = File:Glass_lantern_advertising_slide_for_1927_silent_film_A_Gentleman_of_Paris.jpg
| alt            =  
| caption        = Glass lantern advertising slide
| director       = Harry dAbbadie dArrast
| producer       = 
| writer         = Benjamin Glazer
| screenplay     = Chandler Sprague
| story          = 
| based on       =  
| starring       = Adolphe Menjou Arlette Marchal Nicholas Soussanin
| music          = 
| cinematography = Harold Rosson
| editing        = 
| studio         = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States Silent Herman J. Mankiewicz (Intertitles)
| budget         = 
| gross          = 
}}
A Gentleman of Paris is a 1927 comedy silent film based on the novel Bellamay the Magnificent by Roy Horniman. The film was directed by Harry dAbbadie dArrast and stars Adolphe Menjou, Arlette Marchal, Nicholas Soussanin, Lawrence Grant and William B. Davidson. The feature has been preserved and was released on DVD in 2010. The movie was also the basis for the 1928 film A Certain Young Man.  

==Plot summary==
Marquis de Marignan is a brazen womanizer who spends most of his life escaping the wrath of husbands he has angered.  Joseph, his faithful valet frequently rescues Marignan from disaster. But when Joseph finds out that his boss has been sleeping with his wife, he plots a scheme to publicly humiliate Marquis by exposing him as a card cheat. The ruse works, but Marignan manages to have the last laugh by faking his own suicide and returning to haunt Joseph into confessing his scheme.
==Cast==
*Adolphe Menjou - Marquis de Marignan
*Shirley OHara - Jacqueline
*Arlette Marchal - Yvonne Dufour
*Ivy Harris - Henriette
*Nicholas Soussanin - Joseph Talineau
*Lawrence Grant - General Baron de Latour
*William B. Davidson - Henri Dufour
*Lorraine MacLean - Cloakroom Girl

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 
 
 
 
 


 