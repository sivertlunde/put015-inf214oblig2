Coffee: Ani Barach Kahi
{{Infobox film
| name           = Coffee Ani Barach Kahi
| image          = File:Coffee Ani Barach Kahi promotional poster.jpg
| alt            = Coffee Ani Barach Kahi promotional poster
| caption        = 
| film name      =  
| director       = Prakash Kunte
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = Vaibbhav Tatwawdi   Prarthana Behere   Neha Mahajan   Bhushan Pradhan   Suyash Tilak
| narrator       =  
| music          = Aditya Bedekar 
| cinematography = 
| editing        = 
| studio         =  
| distributor    =  
| released       =   
| runtime        = 
| country        = 
| language       = Marathi
| budget         = 
| gross          =  
}}
 Marathi language movie released in 2015 directed and produced by Prakash Kunte.

== Cast ==
* Vaibbhav Tatwawdi as Nishad
* Prarthana Behere as Jaai
* Neha Mahajan as Abha
* Bhushan Pradhan as Anish
* Suyash Tilak as Nikhil 

== Release ==
Coffee Ani Barach Kahi was released on 3rd April 2015. 

=== Reception ===
The movie generally open to positive reviews with critics praising all lead actors. Ganesh Matkari  of Pune Mirror rated with film with three and half star saying, "Coffee has the potential to be appreciated by audience of all ages, and I hope the film finds it".  Mihir Bhanage of The Times of India rated the film with three star by appreciating the performances, treatment and music. 

== Music ==
Soundtracks for Coffee Ani Barach Kahi is composed by debut music director Aditya Bedekar.
 
{{tracklist
| headline        = 
| extra_column    = Artist(s)
| total_length    = 
| note         = 
| title1          = Rang He Nave Nave
| extra1          = Sasha Tirupati
| length1         = 2:48
| note1         = 
| lyrics1= Yogesh Damle
| title2          = Tu Astis Tar
| extra2          = Pt. Sanjeev Abhyankar
| length2         = 1:52
| note2         = 
| lyrics2 = Mangesh Padgaonkar 
}}

== References ==
 

== External links ==
*  

 
 
 


 