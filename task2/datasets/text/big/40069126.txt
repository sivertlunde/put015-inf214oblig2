Riders of the Range (1923 film)
{{Infobox film
| name = Riders of the Range
| caption =
| image	= 
| imagesize      
| story = Courtney Ryley Cooper
| screenplay = 
| director = Otis B. Thayer
| studio         =  Art-O-Graf
| starring = Edmund Cobb, Dolly Dale, Helen Hayes
| music =
| producer       = Roy M. Langdon
| cinematography = W. E. Smith
| editing =
| distributor = Truart Film Corp
| released = February 15, 1923
| runtime = 
| language =
| country = United States
| budget =
}}
Riders of the Range  is a 1923 western silent film presented by Clifford S. Efelt and directed by Otis B. Thayer and starring Edmund Cobb, Dolly Dale, Helen Hayes and Frank Gallagher. The film was shot in  Colorado by Thayers Art-O-Graf film company.  
"Genre and Hollywood" By Stephen Neale, Psychology Press, 2000, page 25  It was a Roy M. Langdon Production. The film was released on VHS by Grapevine Video.

==Plot==
A growing number of cattle raids prompts the cattlemen to call on their cattlemens association president Martin Lethbridge to investigate. Sheep Ranchers are suspected, who led by Gregg Randall blame the cattlemen for increased casualties among the sheep herds. Letherbridge falls in love with Randalls daughter, Dolly, and eventually exposes Blunt Vanier as the cause of the conflict.

==Cast==

*Edmund Cobb as Martin Lethbridge
*Frank Gallagher as Blunt Vanier 
*Clare Hatton as Gregg Randall  
*Roy Langdon as Bob Randall  
*Harry Ascher as Red Morriss  
*E. Glendower as Soapweed Harris  
*B. Bonaventure as Roddy, the sheriff  
*Levi Simpson as Wagner  
*Dolly Dale as Dolly  
*Helen M. Hayes as Inez  
*Mae Dean as Neil Barclay  
*Ann Drew as Mary Smithson

==Crew==
*Otis B. Thayer Managing Director
*Vernon L. Walker Head Camerman
*H. Haller Murphy Cameraman
* William E. Smith Photography

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 
 