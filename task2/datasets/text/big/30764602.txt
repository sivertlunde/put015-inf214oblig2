The Magic Flame
{{Infobox film
| name = The Magic Flame
| image =File:Magic Flame lobby card.jpg
| caption =Lobby card Henry King
| producer = Samuel Goldwyn
| writer = Rudolph Lothar (play) George Marion, Jr. (titles) June Mathis (continuity) Bess Meredyth (writer) Nellie Revell (titles)
| starring = Ronald Colman Vilma Bánky
| music = Sigmund Spaeth George Barnes
| editing =
| studio = Samuel Goldwyn Productions
| distributor = United Artists
| released =  
| runtime = English
| country = United States
| budget =
}} Henry King, George Barnes Best Cinematography for his work in The Magic Flame, The Devil Dancer and Sadie Thompson.   The film promoted itself as the Romeo and Juliet of the circus upon its release.

This is now considered to be a lost film.  The first five reels are rumored to exist at the George Eastman House, though this is disputed.  

== Cast ==
* Ronald Colman as Tito the Clown
* Vilma Bánky as Bianca, the acrobat
* Agostino Borgato as The Ringmaster
* Gustav von Seyffertitz as The Chancellor Harvey Clark as The Aide Shirley Palmer as The Wife
* Cosmo Kyrle Bellew as The Husband George Davis as The Utility Man
* André Cheron (actor)|André Cheron as The Manager
* Vadim Uraneff as  The Visitor
* Meurnier-Surcouf as Sword Swallower
* Paoli as Weight Thrower
* William Bakewell
* Austen Jewell

==See also==
*List of lost films
*List of incomplete or partially lost films

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 