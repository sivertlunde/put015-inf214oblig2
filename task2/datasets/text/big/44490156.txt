Silent Barricade
 
{{Infobox film
| name           = Silent Barricade
| image          = 
| caption        = 
| director       = Otakar Vávra
| producer       = 
| writer         = Jan Drda Otakar Vávra
| starring       = Jaroslav Prucha
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

Silent Barricade ( ) is a 1949 Czechoslovak drama film directed by Otakar Vávra.   

==Cast==
* Jaroslav Prucha as Hosek (as J. Prucha)
* Barbara Drapinska as Halina (as B. Drapinská)
* Jaroslav Marvan as Strázník Brucek (as J. Mavran)
* Vladimír Smeral as Kroupa (as V. Smeral)
* Marie Vásová as Nedvedová (as M. Vásová)
* Robert Vrchota as Cetar (as R. Vrchota)
* Jirí Plachý as Uhlír (as J. Plachý)
* Jaromír Spal as Pruvodcí (as J. Spal)
* Jaroslav Zrotal as Ridic (as J. Zrotal)
* Eva Karelová as Tramvajacka (as E. Karelová)
* Jaroslava Panenková as Hosková (as J. Panenková)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 