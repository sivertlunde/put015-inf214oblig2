Thirst (2010 film)
{{Infobox film
| name           = Thirst
| image          = Thirst 2010 Poster.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Jeffrey Lando
| producer       = {{Plainlist|
* Jeffrey Lando
* Wendy McKernan
}}
| writer         = {{Plainlist|
* Kurt Volkan
* Joel Newman
}}
| starring       = {{Plainlist|
* Lacey Chabert
* Tygh Runyan
* Mercedes McNab
* Brandon Quinn
}}
| music          = Christopher Nickel
| cinematography = Jeffrey Lando
| editing        = Jeffrey Lando
| studio         = Insight Film Studios
| distributor    = First Look International
| released       =  
| runtime        = 91 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
 thriller film directed by Jeffrey Lando and starring Lacey Chabert, Tygh Runyan, Mercedes McNab, and Brandon Quinn.

==Plot==
Four young friends—fashion photographer Brian and Tyson, Brians wife Noelle, and fashion model "Atheria" Jennifer—travel deep into the desert for a photography session, but get into a car accident and end up stranded in the desert.

Brian separates from the party to search for a way out of the desert, while Atheria begins to show signs that she is suffering from both a concussion and a subdural hematoma—both easily spotted by Noelle, who is a medical student—as a result of a head injury sustained in the crash. Eventually, Noelle decides that she has to perform an emergency procedure to relieve Atherias life-threatening intracranial pressure, but she is too late, and Atheria dies. In desperation, Tyson torches the car (which is hopelessly stuck in a ravine) as a signal fire.

Brian returns and explains that there is no sign of civilization in any direction, and thus no one to see the signal fire. After spending roughly 24 hours camped out by the car, Brian, Noelle, and Tyson decide that there is no hope of rescue, and realize that they have to hike out of the desert. Tyson advocates walking the 80 mile road that led them to their present location, since they at least know where it leads, but Noelle argues that there might be water in the mountains, which are closer. Noelle wins the argument after Brian recalls reading on a map that there is a town just west of the mountains—the only form of civilization around for many, many miles.

Heat and thirst wear away at the three friends as they trek through the desert for days on end. Initially, Tyson seems to be the most resourceful of the three; he knows enough to nourish himself by drinking blood from a rattlesnake. However, as Brian, Noelle, and Tyson grow increasingly desperate for water, Tyson foolishly tries to alleviate his thirst first by eating a cactus—which only makes him more thirsty—and then by drinking a pool of water that Noelle warns him is poisonous. Tyson dies in the middle of the night as a result.

The next night, Brian and Noelle seek shelter in a cave, where they realize that they are dying of thirst. Motivated by Noelles revelation that she is pregnant, Brian slits his own throat so that Noelle can save her own life by drinking his blood. Noelle at first refuses, but when it becomes clear that she cannot prevent Brian from bleeding to death, she drinks.

The following day, Noelle continues to hike alone, and despairs when she happens upon the crash site, and realizes she has traveled in a big circle. In a moment of despair she remembers that Atheria mentioned breast implants. She desperately searches her friends grave, which has been picked clean by wolves, and finds one intact. She then uses it to set up a saline I.V. Afterwards, she continues to walk until she sees a car that, presumably, will come to her rescue. As she collapses from exhaustion, it begins to rain.

==Cast==
* Lacey Chabert as Noelle
* Tygh Runyan as Bryan
* Mercedes McNab as Jennifer a.k.a. Atheria
* Brandon Quinn as Tyson
* Maureen William as Waitress
* Mavourneen Varcoe-Ryan as Flakey Woman

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 