Passion (1982 film)
{{Infobox film
| name           = Passion
| image          = Passion-1982-poster.jpg
| image_size     = 
| caption        = 
| director       = Jean-Luc Godard
| producer       = Armand Barbault Catherine Lapoujade Martine Marignac
| writer         = Jean-Luc Godard Jean-Claude Carrière  (uncredited)
| narrator       = 
| starring       = Isabelle Huppert Jerzy Radziwilowicz Hanna Schygulla Michel Piccoli László Szabó (actor)|László Szabó
| music          = 
| cinematography = Raoul Coutard
| editing        = Jean-Luc Godard
| distributor    = Parafrance Films
| released       = 26 May 1982
| runtime        = 88 minutes
| country        = Switzerland / France
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Passion is a 1982 film by Jean-Luc Godard, and the second feature film made during his return to relatively mainstream filmmaking in the 1980s, sometimes referred to as the Second Wave. Like most of Godards work from this period, Passion is shot in color with a 1.37 aspect ratio. Cinematographer Raoul Coutard won the Technical Grand Prize for cinematography at the 1982 Cannes Film Festival.    The film had 207,294 Admissions in France. http://www.jpbox-office.com/fichfilm.php?id=6694 

== Plot ==
Jerzy is a Polish filmmaker working at a big studio in Switzerland shooting a series of tableaux vivants for a feature film. His producer Lászlo is impatient because there is no apparent story to this film and Jerzy keeps canceling the shoot, repeatedly citing difficulties with the lighting. In the process of making his film, Jerzy has gotten involved with two local women: Isabelle, an earnest young factory worker with a stutter, and Hanna, the worldly German owner of the hotel where the crew is staying. Hanna is married to Michel, an arrogant man with a chronic cough who owns the factory where Isabelle works.

Isabelle is fired from her job and attempts to organize her fellow workers to strike – not for her sake, but for their own. The film crew is meanwhile recruiting factory workers as extras for the series of tableau that Jerzy is shooting. Jerzy continues to search for the right lighting in the studio and to try to manage an increasingly unruly group of extras. At the same time he is trying to continue his relationship with Hanna, with whom he has shot some test footage that the two review together while discussing the intersection of love and work. Jerzy is also taken with Isabelle, who also wants to merge love and work. She tries to get Jerzy involved with her cause and to make meaningful connections with the film crew, asking them why films never show people working.
 Solidarity events and his family back in Poland. Resolving to finish his project by other means, Jerzy leaves for Poland with neither Isabelle nor Hanna but with a waitress from the hotel. Isabelle and Hanna connect with each other and also decide to go to Poland.

== Cast ==
*Jerzy Radziwilowicz - Jerzy
*Isabelle Huppert - Isabelle
*Hanna Schygulla - Hanna
*Michel Piccoli - Michel Boulard
*László Szabó (actor)|László Szabó - Laszlo
*Jean-François Stévenin - Le machino
*Patrick Bonnel - Bonnel
*Sophie Lucachevski - Script-girl

== Background ==
Shooting began in November 1981 close to the  ; the last time they had worked together was on Weekend (1967 film)|Week-end (1967), which is usually considered the end of the French New Wave.

== Themes ==

=== Love and Work ===
The narrative part of the film is about love and work in general. In the film, the two notions get mixed up. Love and work are not separated anymore as Hanna is Jerzy’s lover but works with him for his film at the same time. On the other hand Jerzy is living in Hanna’s hotel but having an affair with her at the same time. The process of film making as a kind of work is paralleled with Isabelle’s work in the factory. At one point, Isabelle even claims that the gestures of love and work are the same.

=== Light and Dark ===
The opposition of light and dark first appears in the Night Watch, which is the first of the tableaux vivants represented in the film, in the context of light in a painting. Later on during the Ingres episode, the two women Isabelle and Hanna are described by Jerzy as the opposition of night and day. According to him, Hanna is like the day, as she is opened towards everything; Isabelle is like the night because she is more difficult to reach. The filmstudio is the place, which stands for artificial light whilst most of the places outside of the filmstudio stand for natural light. The lighting of the tableaux vivants can never reach the qualities of the natural light outside and therefore remains artificial. This may be the reason, why Jerzy is never satisfied with the light.

== Tableaux Vivants ==
The practice of staging a painting in real life with actors belongs to the tradition of the tableau vivant. In Passion, the tableaux vivants are intercut with and inform the films narrative. For example, the unrest of the workers and the relationships between Jerzy, Hanna and Isabelle are reflected in the paintings of Goya or Ingres/Delacroix. The tableaux also reflect the films themes, as in the case of Night Watch, with its opposition of light/dark. Each tableaux vivant has a particular piece of classical music that accompanies it.

=== Rembrandt (The Night Watch) ===

     Night Watch, Hollywood Cinema, which will lead to the final failure of the production of the film.

=== Goya ===

  
The  , background and   The middle ground with the love letter would be a symbol for communication and the link between the foreground and the background at the same time. On the narrative plot level, there are insertitions of shots showing Isabelle sleeping in her apartment. These scenes serve necessarily as a counterpart to the scenes in the filmstudio, as the world outside is present during almost every Sequence. The only exception is the Delacroix Sequence, where all the action takes place in the studio.

=== Ingres ===

 
In the Ingres Sequence, the The Valpinçon Bather and presumably The Turkish Bath are represented loosely as they are interrupted by shots in the hotel and in the factory. The topic of the paintings of the tableaux vivants indicate on the one hand that the theme of love and sexual desire which was symbolically presented in La Maja Desnuda of the Goya Sequence is taken up here.  On the other hand, the Ingres Sequence is about the notions of love and work which are put into relation through the tableaus and the interrupting scenes of the hotels and the factory. A very important point in the Ingres Sequence is when Jerzy and Bonnel are having a conversation in the dark filmstudio. Bonnel asks Jerzy what “the right light” means for him and Jerzy tries to make him understand by darkening the studio. Bonnel finally understands then, that the question about the light is linked to Jerzy’s relation to the two women. He compares Isabelle to the night and Hanna to the day. For Jerzy the main conflict would be to find the right balance between the two.

=== Delacroix ===

The Entry of the Crusaders in Constantinople and Jacob’s Fight with the Angel were the models for the sequence of the Tableaus Vivants by Eugène Delacroix. Like in the Goya Sequence, the Tableaus are not fixed. The horse-riders in the Entry of the Crusaders are mobile and are another symbol of fight or aggression. In Jacob’s fight with the Angel, the director Jerzy gets himself a part of the tableau vivant as he is actually fighting with a comedian disguised as an angel and thus performing Jacobs part from the original painting. Connected to the narrative side of the film, this means that Jerzy is getting more and more involved into the problems concerning the production of his film. This could mean that he is so much involved in the problems concerning the production that he gets a part of his own film. 

=== El Greco ===
 montage technique. Pie Jesu, serves to link the two distinct places and is a hint to Isabelle’s virginity, as Isabelle is compared to Maria and to the religious theme of the tableau vivant. Isabelle’s virginity points to the scene of the tableau vivant in the studio, as the original painting is called Assumption of the Virgin. However there are two interruptions of the music when there is a cut and Isabelle is sitting on Jerzy’s bed in his hotel room murmuring the text of the Pie Jesu. 
According to the principle of the montage, the movements and actions in the two different settings correspond to each other so that when one action is performed in one shot, the action is carried on in the shot afterwards. When Isabelle finally arrives at Jerzy’s room, the camera makes a circle upstairs around the tableau vivant and is very close to the comedians.

=== Watteau ===

 
The Embarkation for Cythera is the only painting of the Watteau sequence and the last of the tableaux vivants. It exists only in fragments, because when it is performed, it is already clear that the main persons who were responsible for the film won’t participate in its production anymore and thus the whole film is falling apart. The camera is very far away from the action, like in an establishing shot. The Embarkation for Cythera is the only tableau vivant which is performed outside and therefore the only tableau vivant which is executed under the conditions of natural light. Hanna, one of the women with whom director Jerzy was in love with, is wandering through the tableau. On her way she encounters two couples. The first couple is a tableau of the two pilgrims from the center of the original painting where the spectators view is likely to attach first.
The second couple she meets is to the right of the central couple in the original painting. Furthermore, the sailing boat of the Berlin version of the painting is displayed in the background of the scene.

==See also==
* Isabelle Huppert filmography

==References==
 

== Bibliography ==

* Barck, Joanna. Hin zum Film – Zurück zu den Bildern. Bielefeld: Transcript Verlag, 2008.
* 
* Dalle Vacche, Angela. Cinema and Painting- How Art is used in Film. London: Athlone Verlag, 1996.
* 
* Heeling, Jennifer. Malerei und Film – Intermedialität. Saarbrücken: VDM Verlag, 2009. 
* 
* Paech, Joachim. Film, Fernsehen, Video und die Künste. Strategien der Intermedialität. Stuttgart: Verlag J.B. Metzler, 1994. 
* 
* Paech, Joachim, Passion oder die Einbildungen des Jean-Luc Godard. Frankfurt am Main: Deutsches Filmmuseum, 1989.
* 
* Schönenbach, Richard. Bildende Kunst im Spielfilm. München: Scaneg Verlag, 2000.
*

== External links ==
*  

 

 
 
 
 
 
 

 