Dekh Tamasha Dekh
{{Infobox film
| name =Dekh Tamasha Dekh
| image = Dekh Tamasha Dekh.Jpg
| caption = DVD Cover
| director = Feroz Abbas Khan
| producer = Feroz Abbas Khan Sunil Lulla
| writer =  Shafaat Khan
| starring = Satish Kaushik Tanvi Azmi Vinay Jain
| cinematography = Hemant Chaturvedi
| editing = A. Sreekar Prasad
| released =  
| runtime = 110 minutes
| country = India
| language = Hindi
| budget =  
 
}}

Dekh Tamasha Dekh is an Indian social and political satire film about a true incident, it revolves around the search for the religious identity of a poor man crushed under the weight of a politicians hoarding. The film explores an Impossible India where bizarre is normal. It has been released on 18 April 2014.
 

==Plot==
Inspired by a true incident, the film starts off when an underprivileged man gets crushed under the weight of a politicians (Satish Kaushik). The film gets into the mood immediately after the death of this man. Be it the judge trying to get into the details of the mans death, be it the lawyers of the parties arguing the case, be it the sparking off of the communal riots. Since the deceased, who was a Hindu by birth, but had got converted to Islam, his death gives rise to a religious spark between two religious factions "the Hindus and Muslims" who want his body to be burnt and buried, respectively. 

==Cast==
*Satish Kaushik ...MUTHASETH
*Tanvi Azmi...FATIMA
*Vinay Jain...VISHWASRAO
*Sharad Ponkshe...BAWDERKAR
*Ganesh Yadav...INSPECTOR SAWANT
*Santosh Juvekar...BADSHAH
*Apoorva Arora...SHABBO
*Alok Rajwade...PRASHANT
*Satish Alekar...Professor SHASTRI
*Jaywant Wadkar...SATTAR
*Dhiresh Joshi...KULKARNI 
 

==Reception== IANS gave it 4 out of 5, "By using the twin missiles of satire and irony, he brings into a play a kind of pinned-down provocativeness into the plot whereby the characters become real and representational simultaneously." and added,"To record the dirt on the wall and the blood on the floor with such clarity and honesty is not within the creative powers of every filmmaker."  Shubhra Gupta of Indian Express, who gave 3 out of 5 stars, explained, "Khan’s film gets into theatrical territory every once in a while, but there is no denying its terrifying power. He pulls no punches, and paints extremism from both sides equally black." and suggested, "This is an important film, and I do hope it gets seen widely, timely and topical as it is in the time of Muzzafarnagar, misguided mullahs and modified bhakts." 

==References==
 

==External links==
*  

 