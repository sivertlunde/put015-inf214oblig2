Les Scélérats
{{Infobox film
| name           = Les Scélérats
| image          = Les Scelerats poster.jpg
| image_size     = 
| caption        = Original film poster
| director       = Robert Hossein
| producer       = 
| writer         = Frédéric Dard Robert Hossein (adaptation) 
| narrator       = 
| starring       = Michèle Morgan  Robert Hossein Olivier Hussenot  Jacqueline Morane
| music          = André Hossein 
| cinematography = Jacques Robin 
| editing        = Louisette Hautecoeur 
| studio         = Les Films Marceau-Cocinor  Les Productions Francis Lopez 
| distributor    = Cocinor 
| released       = 9 May 1960
| runtime        = 90 min
| country        = French
| language       = France
| budget         = 
| gross          = 
}}
Les Scélérats (English title: The Wretches) is a 1960 French drama film directed by Robert Hossein who co-stars with Michèle Morgan, Olivier Hussenot and Jacqueline Morane. It is also known as "The Blackguards". {{cite web|url=http://en.unifrance.org/movie/4028/the-blackguards|title=The Blackguards
|publisher=unifrance.org|accessdate=2013-07-21}} 

==Plot==
The film tells the story of an American couple (coming to in Paris after having lost their only child) and their maid.

==Principal cast==
*Michèle Morgan as  Thelma Rooland 
*Robert Hossein as  Jess Rooland 
*Olivier Hussenot as  Arthur Martin 
*Jacqueline Morane as  Adeline Martin

==References==
 

==External links==
* 
* 
* 

 
 
 


 