Lady with a Past
{{Infobox film
| name           = Lady with a Past
| image_size     =
| image	         = Lady with a Past FilmPoster.jpeg
| caption        =
| director       = Edward H. Griffith E. J. Babille (assistant)
| producer       = Charles R. Rogers
| writer         = Harriet Henry (novel) Horace Jackson
| narrator       =
| starring       = Constance Bennett Ben Lyon David Manners
| music          =
| cinematography =
| editing        = Charles Craft
| studio         = RKO Pathė Pictures, Inc. Charles R. Rogers
| distributor    = RKO Pathė Pictures, Inc.
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = $541,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p39 
| gross          = $595,000 
}}
Lady with a Past is a 1932 romantic comedy film starring Constance Bennett as a shy and very proper young lady who decides to invent a scandalous past for herself to spice up her life. It is based on the novel of the same name by Harriet Henry.

==Cast==
*Constance Bennett as Venice Muir
*Ben Lyon as Guy Bryson
*David Manners as Donnie Wainwright
*Don Alvarado as Carlos Santiagos
*Albert Conti as Rene, the Viscomte de la Thernardier
*Merna Kennedy as Ann Duryea
*Astrid Allwyn as Lola Goadby
*Don Dillaway as Jerry
*Blanche Friderici as Nora (as Blanche Frederici) John Roche as Carl Howe
*Cornelius Keefe as Spaulding
*Nella Walker as Aunt Emma

 

Cast notes:
* Thomas A. Curran the early American silent film star plays an uncredited bit part.

==Reception==
According to RKO records the film lost $140,000. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 

 