Padma Nadir Majhi
 
{{Infobox film
| name           = Padma Nadir Majhi পদ্মা নদীর মাঝি
| image          = Padma Nadir Majhi.jpg
| caption        = DVD cover for Padma Nadir Majhi.
| director       = Goutom Ghosh
| writer         = Manik Bandopadhay  (Novel) Goutom Ghosh (screenplay)  Rupa Gangooly  Rabi Ghosh  Humayun Faridi 
| producer       = West Bengal Film Development Corporation NFDC 
| cinematography = Goutom Ghosh
| country        = India Bangladesh
| editing        =Moloy Banerjee 
| released       = 16 May 1993 (India)
| runtime        = 126 minutes Bengali
| music          = Goutom Ghosh  Alauddin Ali
}}
 Bangladesh joint production feature film directed by Goutom Ghosh from the novel of the same name, Manik Bandopadhyays Padma Nadir Majhi.

==Plot==
 
 
Hossian Miya (  and offer them a better life there. It is apparent that Hossian Miya has a flourishing business there, because he has recently purchased a huge boat because of expanding business. He doesnt care if the people who populate it are Hindu or Muslim. It is 1947, just before the partition of India, and the Hindu fisherman Kuber briefly accepts an offer by Hossain to ferry some of the communitys cargo from the island. He would be fishing, except that the fish he usually catches have been driven away by a big storm. 

In the process of getting the cargo, he gets to see what the colony is like and, even though he is fully aware of the gripes of a former colony member and the limitations of it, comes to share some of the utopian vision. When Kuber returns to his home and a variety of unfortunate events make it in his best interest to leave, he knows just where to go. 

However, people who return from Moynadeep after working there has a different tale to narrate. Rasu (Sunil Mukherjee) and Aminuddin (Rabi Ghosh) say that Moynadeep is infested with lions and tigers, and there is forest all around. Rasu has fled from Moynadeep deserting his wife and children there. Aminuddin, too, refuses to go back to Moynadeep again. Kuber has a wife and daughter and was recently blessed with a child. His wife is beset with a leg deformity, and his daughter is grown up and engaged to a local man. Kuber goes to pay a visit to his relatives from his wifes side. On return, he brings along his sister-in-law Kapila (Roopa Ganguly) with a ton of small kids. Kapila was married but is now estranged from her husband. A sneaky amorous relationship develops between Kubir and Kapila. Staying with a deformed wife and coupled with amorous gestures from his alluring sister-in-law Kubir succumbs to her wishes of passion play. However, Kapilas husband re-appears and takes her away with him. 

One day a storm rises in the Padma River and leaves behind a trail of destruction. Kuber tells his wife "Padda amago joto daay abar totoi loy" (It is true Padda gives us a lot, but in return it also takes a lot from us). The storm wreaks havoc in the village. Hussian Miya offers to lend a helping hand to the villagers. He does so, but in return he takes their thumb impressions as proof of his help so that he can use it later to exploit them. Meanwhile Kubers daughters marriage breaks down. Rasu proposes to marry her. Even though Rasu was close to Kuber, he refuses to accept the match because of the age difference between them. Rasu becomes furious and threatens to ruin Kuber. He succeeds in his endeavor. Kuber is fabricated in a theft case, and police is now trying to nab him. He flees to Hussain Miya for help. Hossain Miya tells he can absolve him of the theft charge, but in return Kubir must go to Moynadeep. Kuber agrees to the offer. Kuber goes to Moynadeep. A new life unfolds for him there. These concluding sequences from the film add up to a compelling climax.

==Cast==
* Raisul Islam Asad as Kuber
* Champa as Mala
* Utpal Dutt as Hosen Rupa Gangooly as Kopila
* Rabi Ghosh as Aminuddin
* Humayun Faridi
* Sunil Mukhopadhyay
* Bimal Deb
* Ajijiul Hakim
* Aminil Hak Chowdhury
* Mukti Anwar
* Rasul Islam
* Tandra Islam
* Mamata Shankar

==Awards==
*National Film Award for Best Feature Film in Bengali (1994)
*National Film Award for Second Best Feature Film (1994)
*National Film Award for Best Direction-Goutom Ghosh (1994)

==Awards(Bangladesh)==
*Bangladesh National Film Award for Best Film (Producer Habibur Rahman Khan) (1993)
*Bangladesh National Film Award for Best Actor-Raisul Islam Asad (1993)
*Bangladesh National Film Award for Best Actress-Champpa (1993)
*National Film Award for Best Art Direction-Mohiuddin Faruque (1993)
*National Film Award for Best Makeup-Mohammed Alauddin (1993)

==References==
 

==External links==
* 
* 
*  at calcuttatube.com
*  at msn.com
*  at hollywood.com
*  yahoo.com

 
 

 
 
 
 
 