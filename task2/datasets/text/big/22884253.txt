A Saloon Wet with Beautiful Women
{{Infobox film
| name = A Saloon Wet with Beautiful Women
| image = A Saloon Wet with Beautiful Women.jpg
| image_size = 
| caption = Theatrical poster for A Saloon Wet with Beautiful Women (2002)
| director = Tatsurō Kashihara 
| producer = Minoru Kunizawa
| writer = Tatsuro Kashihara
| narrator = 
| starring = Koharu Yamazaki
| music = 
| cinematography = Takuya Hasegawa
| editing = 
| distributor = OP Eiga
| released = July 1, 2002
| runtime = 60 min.
| country = Japan Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 2002 Japanese pink film directed by Tatsurō Kashihara. It was chosen as Best Film of the year at the Pink Grand Prix ceremony.    Screenwriter Tatsurō Kashiharas directorial debut, he was awarded at the Pink Grand Prix for his screenplay and as Best New Director.  The film also earned Koharu Yamazaki the Best Actress award. 

==Synopsis==
An erotic romance. 

==Cast==
* Koharu Yamazaki ( ): Utako
* Misaki Wakamiya ( ): Miyoko
* Yui Mamiya ( ): Kanako
* Yasushi Takemoto ( ): Ikuo
* Tsutomu Ōide (大出勉): Keita
* Masayoshi Nogami ( ): Master

==Availability==
The film was released on DVD in Japan under the same title as its theatrical release on March 26, 2004. 

==Bibliography==
*  
*  
*  
*  

==Notes==
 

 
 
 
 
 

 

 
 
 
 
 
 
 
 


 
 