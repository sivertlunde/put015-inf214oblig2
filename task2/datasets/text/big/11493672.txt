The Last Man (2006 film)
{{Infobox film
| image          =The-last-man.jpg
| caption        =Poster of the movie
| name           =Le dernier homme - اطلال
| writer         =Ghassan Salhab
| starring       =Carlos Chahine   Raia Haidar   Faek Homaissi   Raymond Hosni   Aouni Kawas   May Sahab
| director       =Ghassan Salhab
| released       =2006
| runtime        =101 minutes
| country        =Lebanon
| music          =Cynthia Zaven
| cinematography =Jacques Bouquin
| language       =Arabic
| producer       =Marie Balducchi
| distributor    =Agat Films & Cie   Djinn House Productions
|
}}

Le dernier homme (  (Atlal)   film by the Lebanese director Ghassan Salhab.

==Synopsis==
Khalil (Carlos Chahine), a doctor who works at an hospital, is strangely linked to victims of a serial killer who leaves them without blood.

==Cast and characters==
*Carlos Chahine as Khalil
* Faek Homaissi as Dr. Labib

==Trivia==
*The film was presented in Cannes 2007 during the Tous les cinemas du monde section.
*Carlos Chahine won the Best actor prize for his role in this movie during Singapore International Film Festival 2007.

==External links==
*  

 
 
 
 
 
 


 
 