Fearless Tiger
{{Infobox Film  |
name     = Fearless Tiger|
image          = Fearless tiger movie poster.jpg|
size           = 220|
caption        = Movie poster|
director       = Ron Hulme|
writer         = Ron Hulme J. Steven Maunder (created by)  Jalal Merhi (created by)|
starring       = Jalal Merhi  Bolo Yeung Monika Schnarre Jamie Farr  Lazar Rockwood|
producer         = Jalal Merhi Enrique Jim Gutierra-Ross (associate producer)  Dale Hildebrand (line producer) J. Stephen Maunder (associate producer)|
music         = Varouje|
cinematography = Mark Willis|
studio = Film One|
distributor    = |
released   = March 1991 (American Film Market)
runtime        = 88 min.|
language = English |
}} Canadian martial arts movie, directed by Ron Hulme.

==Plot==

Jalal Merhi, Bolo Yeung and Monika Schnarre star in the tale of a martial artist (Merhi) who studies under an old master (Yeung) in the hopes of avenging his brothers death at the hands of a drug ring.

==Release==

The movie was released in Canada in 1991, under the Black Pearls name, but was not released in the United States until 1994, where it was released directly to video by Imperial Entertainment under the Fearless Tiger name. In Canada, the movie flopped at the box office. The movie has been released on DVD in the UK, and in the United States as a double feature with Death Machines from EastWest Entertainment. A new special edition DVD is in the works, but as of April 4, 2011, nothing else has been announced.

==External links==
*  
*  

 
 
 
 
 
 


 
 