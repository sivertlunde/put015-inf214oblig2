Rowd
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Rowd
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Gautam Baruah
| producer       = Mousumi Bordoloi
| screenplay     = Ashim Krishna Rupam Dutta Chirantan Mahanta
| story          = Gautam Baruah
| narrator       = 
| starring       = Utpal Das Diganta Hazarika Moonmi Phukan Monali Bordoloi
| music          = Jatin Sarmah
| cinematography = Bishwabhushan Mahapatra Bipul Das Pradip Doimary
| editing        = 
| studio         = M.B. Films
| distributor    = 145 minutes
| released       =  
| runtime        = 
| country        = India
| language       = Assamese
| budget         = 
| gross          = 
}}
 Assamese Romantic romantic film directed by Gautam Baruah, with a screenplay by Ashim Krishna, Rupam Dutta, and Chirantan Mahanta, and produced by Mousumi Bordoloi. It stars Utpal Das, Diganta Hazarika, Moonmi Phukan, and Monali Bordoloi in the lead roles. It was the debut film for both the director and actress Monali.

==Cast==
* Utpal Das as Ankit
* Diganta Hazarika as Angshuman
* Moonmi Phukan as Emon
* Monali Bordoloi as Pooja
* Bhagawat Pritam as Rudra Pratap Chaliha
* Biju Phukan
* Jayanta Bhagawati
* Padmaraag Goswami
* Hiranya Deka

==External links==
* 
* 

 
 


 