Soldaterkammerater på bjørnetjeneste
{{Infobox film
| name           = Soldaterkammerater på bjørnetjeneste
| image          = 
| caption        = 
| director       = Carl Ottosen
| producer       = Henrik Sandberg
| writer         = Carl Ottosen
| starring       = Preben Kaas
| music          = Sven Gyldmark
| cinematography = Henning Bendtsen
| editing        = Birthe Frost
| distributor    = 
| released       = 15 December 1968
| runtime        = 92 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Soldaterkammerater på bjørnetjeneste is a 1968 Danish comedy film directed by Carl Ottosen and starring Preben Kaas.

==Cast==
* Preben Kaas - Private 12
* Paul Hagen - Private 13
* Willy Rathnov - Private 14
* Poul Bundgaard - Private 15
* Louis Miehe-Renard - Private 16
* Carl Ottosen - 1st Sergeant Vældegaard
* Dirch Passer - Guard Commander 419
* Nat Russell - Little Nat
* Karl Stegger - the Colonel
* Anja Owe - The Colonels Granddaughter
* Ove Sprogøe - Colonel
* Bent Vejlby - Lieutenant Petersen
* Esper Hagen - Guard
* Mei-Mei - Leader of Childrens camp
* Ole Monty - The Circus Director
* Else Petersen - The Chef / Miss Petersen
* Yvonne Ekmann - Leader of Childrens camp
* Morten Grunwald - Doctor Bjørn Bille
* Tine Blichmann - Nurse Karen

==External links==
* 

 
 
 
 
 
 


 
 