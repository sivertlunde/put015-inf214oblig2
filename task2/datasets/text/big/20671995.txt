South Kensington (film)
{{Infobox film
| name = South Kensington
| image = South Kensington (film).jpg
| caption =
| director = Carlo Vanzina
| writer =
| starring =
| music = Flavio Ibba
| cinematography =
| editing =
| producer =
| distributor =
| released =  
| country = Italy United Kingdom
| language = English
}} British romantic comedy film directed by Carlo Vanzina and starring Elle Macpherson, Rupert Everett and Sienna Miller, in her first screen role.  It takes its name from the London neighborhood of South Kensington.

==Cast==
*Rupert Everett: Nick Brett
*Elle Macpherson: Camilla Fox
*Judith Godrèche: Susanna
*Enrico Brignano: Francesco
*Giampaolo Morelli: Antonio
*Naike Rivelli: Ilaria
*Jean-Claude Brialy: Ferdinando 
*Sienna Miller: Sharon
*Nunzia Schiano: Madre di Antonio
*Max Pisu: Massimo
*Vic Tablian: Mobarack 
*Eleonora Benfatto: Fabiana

==References==
 
 

 
 
 
 
 
 


 
 