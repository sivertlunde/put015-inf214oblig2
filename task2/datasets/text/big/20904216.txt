Taxi Taxie
 
{{Infobox film
| name           = Taxi Taxie
| image          = 
| image_size     = 
| caption        = 
| director       = Irshad
| producer       = 
| writer         =
| narrator       = 
| starring       =
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 Bollywood adventure film comedy directed by Irshad.
 
In 1976, Martin Scorsese came up with Taxi Driver, a movie that went on to be viewed as one of the best films ever. Starring Robert De Niro, the movie garnered tremendous critical acclaim and emerged as a cult classic. Over the years many directors have been inspired by its style and the way it went about constructing a psychological study of a man leading a life shrouded by disturbing elements, or rather deconstructing the psychological state of disturbed man finding it difficult to come to terms with his environs.

Closer home, a man called Irshaad made a movie titled Taxi Taxie, casting Amol Palekar in the lead as a taxi driver operating in the metropolis of Mumbai. This movie that came out in 1977 seems to be clearly inspired by the English classic that released a year before. The director/writer has been influenced by the idea of his famous contemporary from the west, and has created a completely different movie, completely different narrative of his own, which is quite unique.

Amol Palekar, quite fondly given the moniker ‘Hero’ by his fellow Mumbai Taxiwallahs, is an honest and diligent worker who spends most of his time on the streets of Mumbai, helping various people move around the city. However, he himself finds it difficult to navigate through certain cobwebs in his mind, which keep pushing him back to his past. His choice of being a taxi driver, despite being well qualified academically, comes as an enigma to his old college friend who he runs into one day. He justifies it by voicing the joys of being constantly with various passengers, who he treats like fellow companions in the journey of life. The profession, thus, is his way of parrying off any chances of falling prey to loneliness. There is also a more romantic reason of him wanting to search for an old flame who he hopes would run into him, if luck is on his side. Thus, all the time his eyes keep searching for that familiar face while he drives through the good and bad wonders of the city.

As a part of his routine, Hero chances into a hapless (and kind) prostitute more than once, and forges a friendly relationship with her. He also meets a struggling playback singer who somehow reminds him of his flame who he wants to reconnect with quite badly. There is also a kind Christian landlady with whom he shares a lovely bond. It is through these relationships that Hero’s thoughts and conflicts are brought to the fore.

The city of Mumbai plays a crucial role in binding the narrative. The recent Aamir Khan film Talaash shares some other similarities with this thirty-five-year-old forgotten work. People who have been to Mumbai will find it interesting to see all their favourite places from the town being given prominence in the film (without pushing the main context to the background). The film appears technically sound in spite of the apparent low budget.

==Cast==
*Amol Palekar	... 	Dev / Hero
*Zaheera	... 	Jyoti Sharma
*Reena Roy	... 	Neelam
*Rama Vij	... 	Prostitute
*Jalal Agha	... 	RV
*Asha Bhosle	... 	Singer
*Aruna Irani	... 	Zarina
*Mehmood Jr.		(as Mehmood Junior)
*Lalita Pawar	... 	Mrs. DSa
*Tun Tun	... 	Mother of 9 children (as Tuntun)
*Dinesh Hingoo	... 	passenger in taxi

Banner : Radha Mohan Arts
Produced By Mrs. Pari A. V. Mohan
Directed by Irshad
Music By Hemant Bhosle
Lyrics By Majrooh Sultanpuri

Songs

1. JEEVAN MEIN HUM SAFAR - Kishore Kumar & Rama Vij

2. HUMEIN TO AAJ ISI BAAT PAR - Asha Bhosle

3. LAYI KAHAN HAI ZINDAGI - Lata Mangeshkar, Asha Bhosle & Chorus

4. BHAWRON KI GUN GUN - Asha Bhosle

5. Title Song (Instrumental)

==External links==
*  

 
 
 