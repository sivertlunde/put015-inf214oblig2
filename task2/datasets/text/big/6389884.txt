Rhapsody (film)
{{Infobox film
| name           = Rhapsody
| image          = Rhapsody1c.jpeg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Charles Vidor
| producer       = Lawrence Weingarten
| writer         = {{Plainlist|
* Ruth Goetz
* Augustus Goetz
* Fay Kanin
* Michael Kanin
}}
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{Plainlist|
* Elizabeth Taylor
* Vittorio Gassman
* John Ericson
* Louis Calhern
}}
| music          = Johnny Green  (director) 
| cinematography = Robert H. Planck
| editing        = John D. Dunning MGM
| distributor    = MGM
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $1,979,000  . 
| gross          = $3,292,000 
}}
Rhapsody is a 1954 American musical romance drama film directed by Charles Vidor and starring Elizabeth Taylor, Vittorio Gassman, John Ericson, and Louis Calhern.

==Plot summary==
Based on the novel Maurice Guest by Henry Handel Richardson, the film is about a wealthy and beautiful woman who follows the man she loves and hopes to marry to Zurich where he studies violin at a conservatory.

There she meets a piano student who falls madly in love with her. She must then choose between this man who loves her more than his music, and the violinist who loves his music more than anything else.    Rhapsody features music by Franz Liszt, Sergei Rachmaninov, Pyotr Ilyich Tchaikovsky, Felix Mendelssohn, and Claude Debussy.

==Cast==
* Elizabeth Taylor as Louise Durant
* Vittorio Gassman as Paul Bronte
* John Ericson as James Guest
* Louis Calhern as Nicholas Durant
* Michael Chekhov as Prof. Schuman
* Barbara Bates as Effie Cahill
* Richard Hageman as Bruno Fürst
* Richard Lupino as Otto Krafft
* Celia Lovsky as Frau Sigerlist
* Stuart Whitman as Dove
* Madge Blake as Mrs. Cahill
* Jack Raine as Edmund Streller
* Birgit Nielsen as Madeleine
* Jacqueline Duval as Yvonne
* Norma Nevens as Student Pianist   

==Production==
The novel Maurice Guest was originally published in 1908.  

Rhapsody was filmed on location in Florhofgasse, Zürich (the street scenes) and Pontresina, Kanton Graubünden in Switzerland.   

==Reception==
According to MGM records the film earned $1,291,000 in the US and Canada and $2,001,000 overseas, resulting in a loss of $217,000. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 