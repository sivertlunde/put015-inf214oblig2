Lost Children (2005 film)
{{Infobox film
| name           = Lost Children
| image          = Lost children-Ali Samadi 2005.gif
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Ali Samadi Ahadi and Oliver Stoltz
| producer       = Oliver Stoltz
| writer         = Ali Samadi Ahadi and Oliver Stoltz
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = Ali N. Askin
| cinematography =  
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 97 min   
| country        =  English
| budget         = 
| gross          = 
}}
Lost Children is a 2005 documentary film by Ali Samadi Ahadi and Oliver Stoltz about Military use of children in civil war in northern Uganda.    

The film premiered in February 2005 at the Berlin International Film Festival, where it received the third place at PANORAMA Audience Award. The film was awarded in October 2005, the UNICEF Prize and the Youth Award and ran for more than two dozen international film festivals. Lost Children was nominated for Deutschen Kamerapreis Köln Award in 2005 for Best Documentary and Deutscher Filmpreis in 2006 in the documentary category.

== References ==
 

== External links ==
*   Official website
*  
*  , Brooklyn Film Festival

 
 
 
 
 
 


 