Frantic (film)
{{Infobox film
| name = Frantic
| image = Frantic (movie poster).jpg
| image_size = 197px
| alt = 
| caption = Theatrical release film poster
| director = Roman Polanski
| producer = Tim Hampton Thom Mount
| writer = Roman Polanski Gérard Brach Robert Towne  (uncredited)  Jeff Gross  (uncredited) 
| starring = Harrison Ford Betty Buckley John Mahoney Emmanuelle Seigner|
| music = Ennio Morricone
| cinematography = Witold Sobociński
| editing = Sam OSteen
| distributor = Warner Bros.
| released =  
| runtime = 119 minutes
| country = United States France
| language = English French
| budget = $20 million
| gross = $17,637,950 (USA) 
}}

:For 1958 French film aka Frantic in USA, see Elevator to the Gallows
 mystery Thriller thriller film directed by Roman Polanski and starring Harrison Ford and Emmanuelle Seigner. The theme was written, arranged and performed by Simply Red.

The French locations and Ennio Morricones musical score create much of the films atmosphere. Grace Jones recording of "Ive Seen That Face Before (Libertango)", a cover version of Ástor Piazzollas Libertango, as well as "Chicago Song" of David Sanborn is heard at key moments in the film.

== Plot ==
Dr. Richard Walker is a surgeon visiting Paris with his wife Sondra for a medical conference. At their hotel, she is unable to unlock her suitcase, and Walker determines that she has picked up the wrong one at the airport. While Walker is taking a shower, his wife mysteriously disappears from their hotel room.

Still jet-lagged, he searches for her in the hotel with the help of a polite but mostly indifferent staff and then wanders outside to search himself. A vagrant overhears him in a café and says he saw Walkers wife being forced into a car. Walker is skeptical until he finds his wifes ID bracelet on the cobblestones. He contacts the Paris police and the US embassy, but their responses are bureaucratic and there is little hope anyone will look for her.

As Walker carries on the search himself (with input from a very sympathetic but wary desk clerk at the hotel), he stumbles onto a murder scene and then encounters the streetwise young Michelle, who had mistakenly picked up his wifes suitcase at the airport. It transpires that Michelle is a career smuggler but does not know for whom she is working. She reluctantly helps Walker in his increasingly frantic attempt to learn what was in the switched suitcase and to trade whatever it is for the return of his wife.

It turns out that hidden within a small replica of the Statue of Liberty is a krytron, a small electronic switch used in the detonators of nuclear devices. The film ends with a confrontation beside the River Seine where Walkers wife is released. However, a firefight ensues between the Arab and Israeli agents. During the crossfire, the Arab agents are killed but Michelle is also shot and dies with Walker and Sondra at her side. Angry and upset, Walker throws the krytron into the river while the helpless Israeli agents look at him. Soon after, the Walkers leave Paris.

== Cast ==
* Harrison Ford as Dr. Richard Walker
* Emmanuelle Seigner as Michelle
* Betty Buckley as Sondra Walker
* John Mahoney as Williams (U.S. Embassy Official)
* Jimmie Ray Weeks as Shaap
* Yorgo Voyagis as The Kidnapper
* David Huddleston as Peter
* Alexandra Stewart as Edie
* Gérard Klein as Gaillard
* Dominique Pinon as Wino

== Production == Le Grand 9th arrondissement. The hotels lobby also appeared in the film. {{cite book
 | last1 = Sandford | first1 = Christopher
 | title = Polanski
 | location = London
 | publisher = Random House
 | pages = 368–369
 | date = 2007
| isbn = 9781844138791 }}  Lady Liberty scenes. 

== Release and reception ==
Frantic was released in the UK on 16 February 1988, with a release of 26 February in the USA and a 30 March release in France.  The film was a disappointment at the box office with a domestic gross of $17,637,950, failing to recoup its production budget. However the film was more successful in other countries such as France where it received 1,293,721 admissions. 

As opposed to its commercial failure, Frantic was a critical success. Review aggregator Rotten Tomatoes reported that 78% of critics gave positive reviews based on a sample of 40 reviews with an average rating of 6.4/10. 
 Siskel & Ebert and The Movies.  Pat Collins of WWOR-TV called it "Polanskis best film ever." 
Desson Howe of the Washington Post called the movie "vintage Polanski", with its relentless paranoia, irony, diffident strangers and nutty cameos. 
British film magazine Empire (film magazine)|Empire rated the movie three out five, calling it Polanskis most satisfying film since Chinatown (1974 film)|Chinatown, and one of the best traditional thrillers to come down the pike in quite some time. 
Roger Ebert in his review gave the movie three stars, saying "to watch the opening sequences of Frantic is to be reminded of Polanski’s talent. Here is one of the few modern masters of the thriller and the film noir. Frantic is a reminder of how absorbing a good thriller can be."
 

== References ==
 

== External links ==
*  
*  
*  
*  
*   at Yahoo! Movies

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 