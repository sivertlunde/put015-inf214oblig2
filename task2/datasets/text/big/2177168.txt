Chain of Fools (film)
{{Infobox film
| name           = Chain of Fools
| image          =
| image_size     =
| caption        =
| director       = Pontus Löwenhielm Patrick von Krusenstjerna
| producer       = Charles Block Tony Lord Steven Reuther
| writer         = Bix Skahill
| starring       = Steve Zahn Salma Hayek David Cross Elijah Wood Jeff Goldblum David Hughes John Murphy
| cinematography = Frederick Elmes
| editing        = Harvey Rosenstock
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 96 min.
| country        = United States
| language       = English
| budget         = $20,000,000 (estimate)
}}
Chain of Fools (also known by its production title Shiny New Enemies) is a 2000 heist comedy film about a hapless barber named Kresk (Steve Zahn).

==Plot==

Thomas Kresk is a loser—he isnt good at his job, hes been tossed out of his home, and his wife just dumped him for the marriage counselor. Now hes depressed, and contemplating suicide. And yes, things get worse: a criminal named Avnet (Jeff Goldblum) has stolen three priceless coins, and decided to blackmail Bollingsworth (Tom Wilkinson), his billionaire partner in crime. After Kresk overhears this, he almost gets shot—and Avnet ends up impaled on a pair of barbers scissors. 

Now Kresk is in a considerably nastier situation, so he steals the gun and the coins. But things take a sharp turn when he hires a hit man named Mikey (Elijah Wood), and discovers that the hit man is only seventeen and emotionally traumatized by his parents suicide. And that Kresk is falling for the cop/Playboy model Sgt. Meredith Kolko (Salma Hayek), and that his nephew Scottie (Devin Drewitz) has now swallowed the coins. Now Kresk is in over his head, and has to deal with the strange and sometimes dangerous people around him.

==Cast==
* Steve Zahn as Kresk
* Salma Hayek as Kolko
* Jeff Goldblum as Avnet
* David Cross as Andy
* Elijah Wood as Mikey
* Tom Wilkinson as Bollingsworth
* David Hyde Pierce as Mr. Kerne
* Kevin Corrigan as Paulie
* Orlando Jones as Miss Cocoa
* Lara Flynn Boyle as Karen
* John Cassini as Henchman
* Michael Rapaport as Hitman
* Craig Ferguson as Melander Stevens
* Myndy Crist as Jeannie

==Crew== Swedish production team consisting of five directors and two producers.

The stunt coordinator was David Jacox.

==External links==
*  
*  
* 
* 

 
 
 
 
 
 


 