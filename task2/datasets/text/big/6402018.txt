Blood of a Champion
{{Infobox film
| name= Blood of a Champion
| image= Blood of a champion.jpg
| caption = The movie poster for Blood of a Champion.
| writer=Lawrence Page
| starring=Bokeem Woodbine Deborah Cox
| director=Lawrence Page
| music=RZA
| distributor=Clanage
| released= 
| runtime=
| language=English
| producer=Lawrence Page
| budget=
}}
Blood of a Champion is a 2005 film that was released directly to DVD.  The film was written, produced, and directed by Lawrence Page.  It stars Bokeem Woodbine and R&B star Deborah Cox.

==Plot==
Shadow (Woodbine), an ex-con, finds it very difficult adjusting to many things after he has spent 10 years in prison.  He has trouble adjusting to society, himself, and his wife Sharon (Cox).  Yet, before Shadow went to prison he was a promising boxer and when he returns to society he is offered a shot at the ring again only this time it is in the underground rings of street fighting.  With this offer, Shadow believes his luck is changing and he takes the offer and enters the ring again.  Shadow eventually learns that this type of fighting is controlled by ruthless businessmen and the stakes invested in it are very, very high.  Shadow soon discovers that leaving the underground world of street fighting is going to be his greater challenge, even greater than him adjusting to life out of prison.

==Cast==
*Bokeem Woodbine as Shadow
*Deborah Cox as Sharon
*George T. Odom as Shadows father
*Mamie Louise Anderson as Shadows Mother
*Michael Ciminera as Prison Guard
*Teri Denine as Detective
*DJ Envy as Shadows enemy friend
*Eshaya Draper (credited as Eshiya Fitz) as Young Shadow
*Brooke Forbes as Lawyer
*Shyheim Franklin
*Arthur French as Old man at gym
*Shalonne Lee as Call Girl
*Frank McCarthy as Executioner
*Shawand McKenzie as Referee
*Paula McLaughlin as Nurse Paula
*DJ Megatron as Shadows old friend

== External links ==
*  

 
 
 
 

 