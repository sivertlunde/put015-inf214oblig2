Dark Tourist
{{Infobox film
| name           = Dark Tourist
| image          = Dark tourist dvd cover.jpeg
| alt            =
| caption        = Movie poster
| director       = Suri Krishnamma
| producer       = {{plainlist|
* Zachery Ty Bryan
* Adam Targum
* Suzanne DeLaurentiis
* Michael Cudlitz
* Frank John Hughes
}}
| writer         = Frank John Hughes
| starring       = {{plainlist|
* Michael Cudlitz
* Melanie Griffith
* Pruitt Taylor Vince
}}
| music          = Austin Wintory
| cinematography = Ricardo Jacques Gale
| editing        = Justin Guerrieri
| studio         = Vision Entertainment Group
| distributor    = Phase 4 Films
| released       =   }}
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Dark Tourist (also The Grief Tourist) is a 2012 American psychological thriller film directed by Suri Krishnamma, written by Frank John Hughes, and starring Michael Cudlitz, Melanie Griffith, and Pruitt Taylor Vince.  Cudlitz plays a bigoted security guard who engages in dark tourism.  It premiered at Filmfest München on July 3, 2012, and Phase 4 Films released it theatrically on August 23, 2013.

== Plot ==
Jim, a quiet and solitary man, explains in voice-over narration his hatred of society and his employers, though he enjoys his work as security guard, as it allows him long periods of solitude.  On his vacation time, he engages in dark tourism, the visitation of murder sites.  His current subject is Carl Marznap, a mass murderer and arsonist.  He leaves Yonkers, New York, to visit Carls home in California.  His neighbor at the cheap motel in which he stays, Iris, turns out to be a prostitute.  Nervous, Jim fumbles in conversation with her, then states in voice over how much he hates prostitutes.  At a local restaurant, he meets Betsy, a friendly waitress.  Jim introduces himself to her as Carl and falsely claims that his sister has been diagnosed with cancer.  Betsy and Jim draw closer over their difficult lives, and she invites him to an Alcoholics Anonymous meeting.

On his tour of Marznaps home and crime scenes, Jim becomes increasingly disturbed by hallucinations and vague flashbacks.  Eventually, Jim hallucinates Marznap himself, who urges him to teach the world what it feels like to be a victim.  Marznap, who was tortured and gang-raped while in juvenile detention, explains that he sought revenge by burning down a church, as he blamed those people for doing nothing to help him while his father abused him.  After the Alcoholics Anonymous meeting, Jim and Betsy bond further, and Betsy invites him to dinner at her house.  Betsy offers him marijuana, and he reluctantly accepts.  When she tries to kiss him, he becomes agitated and demands that they have sex without any affection.  When she asks him to slow down, he berates her and leaves.  Marznap encourages him to visit Iris.  Iris, who is a trans woman, has anal sex with Jim, and he flashbacks to a childhood gang-rape.

Jim and Carl discuss their shared experiences, and Jim expresses his belief that he is broken beyond repair.  The next day, at the restaurant, Betsy apologizes to Jim, and he calls her disgusting.  She runs away crying, and Jim returns to his motel.  Jim breaks into Iris room, scares off her client, and beats her savagely.  After he berates Iris, God, and the kids who raped him, he takes Iris to Marznaps house, where he strangles her.  Marznap and Jim then discuss what to do next; Marznap counsels him to commit suicide, and Jim slices his own throat open.  In an epilogue, the police reveal that Jim has killed six trans woman prostitutes, and a young man visits Jims house as part of his own dark tourism.  Around a hole in the wall in Jims house, a message reads, "From this void, no one returns!"

== Cast ==
* Michael Cudlitz as Jim Tahna
* Melanie Griffith as Betsy
* Pruitt Taylor Vince as Carl Marznap
* Suzanne Quast as Iris
* Brad Bufanda as Manny

== Production ==
Production was originally to take place in New Orleans but moved to Los Angeles after a money dispute.   Cudlitz and Griffith were drawn to the film because of the writing. 

== Release ==
Dark Tourist premiered at Filmfest München on July 3, 2012.   It received a limited release in the United States on August 23, 2013.   It was released on Blu Ray, DVD in the United States on February 25, 2014. 

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 38% of eight surveyed critics gave the film a positive review; the average rating was 4.6/10.   Metacritic rated the film 40/100.   Geoff Berkshire of Variety (magazine)|Variety called it "a well-acted but rote and ultimately repellent character study".   Karsten Kastelan of The Hollywood Reporter wrote that the film is unrelentingly dark, which makes it too uncomfortable to enjoy.   Nicolas Rapold of The New York Times wrote that the films mood is weakened by Vinces and Griffiths appearances, and the post-credits scene is "too clever by half".   Annlee Ellingson of the Los Angeles Times criticized the films pacing and called the climax "a grotesque, exploitive mess".   Ernest Hardy of The Village Voice wrote that the film "veer  hard into cliché" but the actors give good performances.   Gareth Jones of Dread Central rated it 1.5/5 stars and wrote, "What should be a deep and wounding trip to the edge of sanity remains far too understated and impenetrable for its own good."   Becki Hawkes of Daily Dead rated it 4/5 stars and wrote, "Theres an intense evocation of dread throughout: a crawling, fetid sense of evil, which some viewers may simply find too repellent to watch." 

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 