Jeg har elsket og levet
{{Infobox film
| name           = Jeg har el sket og levet
| image          = Jegharelsketoglevet.jpg
| image_size     =
| caption        = Poster
| director       = George Schnéevoigt 
| producer       = 
| writer         = Fleming Lynge 
| narrator       = 
| starring       = Erling Schroeder 
| music          = C.E.F. Weyse  
| cinematography = Valdemar Christensen Poul Eibye    
| editing        = Valdemar Christensen  Carl H. Petersen     
| distributor    = Nordisk Film
| released       = 17 December 1940 
| runtime        = 95 minutes
| country        = Denmark Danish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1940 Denmark|Danish family film directed by George Schnéevoigt. The film stars Erling Schroeder  and Johannes Meyer. 

==Cast==
*Erling Schroeder as C.E.F. Weyse 
*Ellen Gottschalch as Henriette Frisch 
*Johannes Meyer as Tutein 
*Mary Alice Therp as Lucia Tutein 
*Bjarne Forchhammer as Frederich Tutein 
*Tove Bang  as Pauline Tutein 
*Inger Stender as Emilie Tutein 
*Edith Oldrup Pedersen as Julie Tutein 
*Aksel Schiøtz as Herman Kramer 
*Angelo Bruun as Wilhelm Nolthenius 
*Edvin Tiemroth as Adam Oehlenschläger 
*Børge Munch Petersen as Knud Lyhne Rahbek 
*Karen Lykkehus as Kamma Rahbek 
*Hans Kurt as du Puy 
*Carl Viggo Meincke  as Kofoed 
*Asmund Rostrup as Søren Hertz 
*Thorkil Lauritzen as Buntzen 
*Victor Montell as Tørring  Kristian as Hans Søstersøn 
*Carl Madsen as En Kusk

==External links==
*  

 
 
 
 
 
 
 
 