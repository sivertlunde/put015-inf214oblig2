Ente Gramam
{{Infobox film
| name           = Ente Gramam
| image          =
| caption        =
| director       = Sreemoolanagaram Vijayan
| producer       =
| writer         =
| screenplay     = Sharada Sreelatha Namboothiri MG Soman Vidhubala
| music          = Vidyadharan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, directed by Sreemoolanagaram Vijayan. The film stars Sharada (actress)|Sharada, Sreelatha Namboothiri, MG Soman and Vidhubala in lead roles. The film had musical score by Vidyadharan.   

==Cast== Sharada
*Sreelatha Namboothiri
*MG Soman
*Vidhubala

==Soundtrack==
The music was composed by Vidyadharan and lyrics was written by Sreemoolanagaram Vijayan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kalppaanthakaalatholam || K. J. Yesudas || Sreemoolanagaram Vijayan || 
|-
| 2 || Maninaagathanmare || K. J. Yesudas, Ambili || Sreemoolanagaram Vijayan || 
|-
| 3 || Pathaayam polathe || CO Anto, PR Bhaskaran || Sreemoolanagaram Vijayan || 
|-
| 4 || Veenaa paanini || Vani Jairam || Sreemoolanagaram Vijayan || 
|}

==References==
 

==External links==
*  

 
 
 

 