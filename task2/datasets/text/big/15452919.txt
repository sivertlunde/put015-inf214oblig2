Carny (1980 film)
{{Infobox Film
| name           = Carny
| image_size     = 
| image	=	Carny FilmPoster.jpeg
| caption        = 
| director       = Robert Kaylor
| producer       = Robbie Robertson Thomas Baum
| starring       = Gary Busey Jodie Foster Robbie Robertson
| music          = Alex North
| cinematography = Harry Stradling, Jr.
| editing        = Stuart H. Pappé
| studio         = Lorimar Film Entertainment
| distributor    = United Artists
| released       = June 13, 1980
| runtime        = 107 minutes
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Carny is a 1980 drama film about a waitress who joins a traveling carnival. It stars Gary Busey, Jodie Foster, and Robbie Robertson. It also includes an early role for Fred Ward.

==Plot== Kenneth McMillan) negotiating deals with local officials and representatives of the local underworld to keep the show open. 

What it takes to keep the show open varies from town to town. In one town, its making good on a city officials losses gambling on the midway and giving a city councilor a pile of free passes to the carnival. In another, its compromising to allow the strippers to work,  but keeping the freak show closed. In a third, it involves providing an underworld bosss thug with a girl he fancies. He also works to maintain harmony among the carnies. Patch is good at his job of patching together the deals that keep the carnival rolling and keeping the peace on the lot, but never likes being played for a fool. 

At one stand Donna (Jodie Foster), a wayward bisexual 18-year-old bored with small town life, strikes up a friendship with Frankie and at his invitation follows the carny onto the carnival circuit. Patch is less than happy with her presence, and would like her out of the picture. To get Patch off her back, she takes a job with the cooch show as a "side girl," a backup dancer who does not actually take her clothes off. Patch plants the suggestion with Delno, the carny who runs the girlie show, that Donna wants to "work strong" -- be a stripper, in other words. When she is thrust onstage, she freezes and a brawl ensues. Afterwards, she covers with Heavy to keep Patch out of it, taking the blame for Patchs setting her up to fail herself.

Frankie gets her a job in the string joint, one of the midway games of chance, under the tutelage of Gerta (Meg Foster). Coached in how the game works, Donna is a big success, learning how to con the marks and get their money without giving herself (which is what the rubes really want) to them. After one successful scamming, she winds up in bed with Patch and the two of them are caught in flagrante by Frankie, which puts a strain on his relationships with Patch and with Donna.

While this is going on, a con run by Nails (Theodore Wilson) on Skeets, the local crime bosss main enforcer, goes badly wrong. Uspet at losing their money, the local underworlds muscle boys wreck the Bozo Joint and kill On-Your-Mark (Elisha Cook), a carny who has been "with it" for more than fifty years and was planning to retire at the end of the season. Crime boss Marvin Dill (Bill McKinney) comes after the carnival intending to extort more money than they have already paid him. However, the carnies have had enough of his shaking them down. To avenge On-Your-Marks death and get Dill off their backs, Patch, Frankie, Donna and Heavy run a scam on Mr. Dill involving the apparent beheading of Skeets. (They dont actually kill him, of course, but the effect used to terrify Dill and make him believe they did is simple and horrifyingly effective.)

The movie ends with the Great American Carnival continuing on its way, with Donna her own woman rather than Frankies girlfriend, Frankie and the Patch reconciled, and the implication that in a season or two Heavy will retire and Patch will be the one running the show.

==Cast==
*Gary Busey as Frankie
*Jodie Foster as Donna
*Robbie Robertson as Patch
*Meg Foster as Gerta Kenneth McMillan as Heavy St. John
*Elisha Cook, Jr. as On-Your-Mark
*Tim Thomerson as Doubles
*Theodore Wilson as Nails (as Teddy Wilson)
*John Lehne as Skeet
*Bill McKinney as Marvin Dill
*Bert Remsen as Delno Baptiste
*Woodrow Parfrey as W. C. Hannon
*Alan Braunstein as Willie Mae
*Tina Andrews as Sugaree
*Craig Wasson as Mickey
*Fred Ward as Jamie

==Reception==
Roger Ebert of the Chicago Sun-Times said, "Carny is bursting with more information about American carnivals than it can contain, surrounding a plot too thin to support it. ...Inside this movie is a documentary struggling to get out." 

==References==
 
==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 

 