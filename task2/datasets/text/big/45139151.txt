Valmiki (1963 film)
{{Infobox film 
| name           = Valmiki
| image          =  
| caption        = 
| director       = C. S. Rao
| producer       = S K Habeebulla
| writer         = 
| screenplay     = C. S. Rao Rajkumar Kantha Narasimharaju Rajanala Kaleswara Rao Ghantasala
| cinematography = M A Rehaman
| editing        = K Govindaswamy
| studio         = Jupiter Pictures
| distributor    = Jupiter Pictures
| released       =  
| runtime        = 138 min
| country        = India Kannada
}}
 1963 Cinema Indian Kannada Kannada film, Narasimharaju and Rajanala Kaleswara Rao in lead roles. The film had musical score by Ghantasala (singer)|Ghantasala.  

==Cast==
  Rajkumar
*Kantha Rao Narasimharaju
*Rajanala Kaleswara Rao
*Raghuramaiah
*Mikkilineni
*Srikanth
*A. V. Subba Rao
*Dr Shivaramakrishnaiah
*Dharmaraj
*Jagga Rao
*Koteshwar Rao
*Sampath
*Basha
*Shando Krishna
*Raja Sulochana Leelavathi
*Lakshmidevi
*Sharada
*Shakunthala
*B. Jaya (actress)|B. Jaya
*Padmini
*Shashikala
*Pramila
*Kousalya
 

==Soundtrack==
The music was composed by Ghantasala Venkateswara Rao. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Sri Ramayana Kaavya Katha || Ghantasala Venkateswara Rao || Samudrala || 13.26
|- Susheela || Samudrala || 03.50
|}

==References==
 

==External links==
*  
*  

 
 
 


 