The Autograph Hound
{{Infobox Hollywood cartoon
|cartoon_name=The Autograph Hound
|series=Donald Duck 
|image=The Autograph Hound Theatrical Poster.PNG
|caption=Theatrical release poster Jack King
|story_artist=  Ed Dunn Ed Love Ward Kimball Paul Allen Rex Cox Seamus Culhane Nick De Tolly (assistant)
|voice_actor=Clarence Nash Billy Bletcher
|musician=
|producer=Walt Disney
|distributor=RKO Radio Pictures
|studio=Walt Disney Productions
|release_date= 
|color_process=Technicolor
|runtime=7:55
|movie_language=English}}
The Autograph Hound is a 1939 American Donald Duck cartoon which features Donald Duck as an autograph hunter in Hollywood. Many celebrities from the 1930s are featured. This is the first cartoon where Donald Duck is featured in his blue sailor hat.

==Plot==
Donald Duck tries to enter a Hollywood studio so he can search for celebrities willing to sign their autograph. A security guard at the gate prevents him from entering the building. Donald manages to sneak inside by climbing on the limousine with Greta Garbo so that it seems hes riding along with her. The police officer discovers hes been fooled and chases Donald, who enters a room with the name "Mickey Rooney" on it. Inside, Mickey Rooney is dressing up in front of the mirror, when Donald asks him for his autograph. Rooney writes his name in Donalds book and makes it disappear and reappear with a magic trick. Donald, who is not amused, tries to impress Rooney by doing a similar trick with an egg. The egg is however obviously hidden under Donalds hat and Rooney who is aware of this, crushes it, laughing loudly. Donald gets extremely angry and starts waving his fists, while Rooney manages to put a violin in Donalds hands and starts dancing an Irish jig Donald is playing. When Donald discovers he has been tricked for the third time he throws the violin at Rooney. Rooney ducks and the instrument lands in the face of the security guard.

Alarmed, Donald runs away and hides under a bell-jar carried by actor Henry Armetta. When the guard discovers Donalds hiding place the duck runs to another film set full with ice. There he meets Sonja Henie and asks her for an autograph. Henie signs her name by skating it in the ice, so that Donald has to carry it with him. While walking in a desert setting Donald discovers the ice has melted. He notices a tent with the silhouettes of three belly dancing Arabic women, who turn out to be the Ritz Brothers. Excited, he asks them for their autographs, but behaving like screwballs they jump on Donald and sign their group name on his buttocks. An enraged Donald throws a paint can at their heads, but it hits the face of the guard instead.
 Joe E. Edward Arnold, Katharine Hepburn, Eddie Cantor, Slim Summerville, Lionel Barrymore, Bette Davis, Groucho Marx, Harpo Marx, Mischa Auer, Joan Crawford and Charles Boyer). When the police officer asks Donald to sign his autograph book and offers him his pen, Donald squirts ink in the policemans face. While the ink drips from the officers face and writes Donalds name on his chest, Donald laughs hysterically.

==Cultural references==
* Quite a few of the caricatures featured at the end of the cartoon also appeared in an earlier Disney short: Mother Goose Goes Hollywood (1938)
* Donald Duck imitated Sonja Henie in an earlier cartoon, The Hockey Champ (1939).
* Bette Davis is featured in her role as Jezebel (film)|Jezebel (1938), Lionel Barrymore appears as Dr. Gillespie from the Dr. Kildare film series and Charles Boyer is dressed as Napoleon Bonaparte in reference to his film Conquest (1937 film)|Conquest (1937).
* The Road to Mandalay was the working title of the first "Road to..." series, Road to Singapore, with Bob Hope and Bing Crosby.

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 