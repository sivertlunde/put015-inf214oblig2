Act of Reprisal
{{Infobox film
| name           = Act of Reprisal
| image          = 
| caption        = 
| director       = Erricos Andreou Robert Tronson
| producer       = Wilbur Stark
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Ina Balin Jeremy Brett Kostas Kazakos Yorgos Moutsios
| music          = 
| cinematography = Basil Maros
| editing        = Barry Malkin
| studio         = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Act of Reprisal is a 1964 American drama film directed by Erricos Andreou and Robert Tronson and starring Ina Balin, Jeremy Brett and Giannis Voglis.  The film depicts a romance set against the backdrop of Cypriot attempts to gain independence from Britain in the 1950s. It was also known as Antekdhikissi.

==Cast==
* Ina Balin - Maria 
* Jeremy Brett - Harvey Freeman, British Military Officer 
* Kostas Kazakos 
* Yorgos Moutsios 
* Dimitris Nikolaidis 
* Dimos Starenios   
* Nikos Tsachiridis   
* Giannis Voglis

==References==
*List of American films of 1964
 

==External links==
* 

 
 
 
 
 
 
 