The Bodyguard (1992 film)
 
{{Infobox film
| name           = The Bodyguard
| image          = The Bodyguard 1992 Film Poster.jpg
| alt            = 
| caption        = Theatrical release poster Mick Jackson
| producer       = {{Plainlist|
* Kevin Costner
* Lawrence Kasdan Jim Wilson}}
| writer         = Lawrence Kasdan
| starring       = {{Plainlist|
* Kevin Costner
* Whitney Houston}}
| music          = Alan Silvestri Andrew Dunn
| editing        = {{Plainlist|
* Donn Cambern
* Richard A. Harris}}
| studio         = {{Plainlist| Tig Productions Kasdan Pictures}} Warner Bros. Pictures
| released       =  
| runtime        = 129 minutes  
| country        = United States
| language       = English
| budget         = $25 million 
| gross          = $411 million 
}} romantic Thriller thriller film Secret Service Agent-turned-bodyguard who is hired to protect Houstons character, a music star, from an unknown stalker. Kasdan wrote the film in the mid 1970s, originally as a vehicle for Steve McQueen and Diana Ross.   

The film was Houstons acting debut and was the second-highest-grossing film worldwide in 1992, making $411 million worldwide, despite mixed to negative reviews from critics. The   became the List of best-selling albums|best-selling soundtrack of all time, selling more than 45 million copies worldwide.   

The film implicitly takes place in Los Angeles in late 1994 or early 1995, as it culminates at the then-future 67th Academy Awards.

==Plot== Secret Service when Reagan was shot. Bill eventually finds Farmer, who is now a highly successful private bodyguard, mostly protecting corporate VIPs. However, Frank is reluctant to accept the offer to guard Rachel as he sees her as a spoiled diva who is oblivious to the threats against her life.

Franks misgivings are confirmed when he enters Rachels estate too easily. And when he is finally introduced to the superstar, Rachel looks upon Frank as paranoid and his extensive protection techniques as intrusive. Rachels existing bodyguard Tony (Starr) resents Franks presence. But at a concert where Rachel is performing a riot breaks out, and Frank rescues her from danger. As a result, the two develop a closer relationship. Frank tries to remain professional, but the two sleep together. However, recognizing that this compromises his ability to protect his client, Frank breaks off their affair. Hurt, Rachel begins to defy Franks painstaking security measures. She even goes as far as attempting to sleep with his former Secret Service colleague Greg Portman (Arana). But after she has personal contact with her stalker via a threatening phone call, Rachel realizes that she must put her trust in Frank ahead of her own desire for personal gratification. She finally recognizes the seriousness of the situation and cancels the rest of her tour.

Frank, Rachel, Rachels driver Henry, Rachels son Fletcher (Nixon), and her sister Nicki (Richards) then travel to Franks fathers home (which is a large, lakefront log cabin in the mountains). Franks father, Herb (Waite), is a semi-retired attorney who welcomes the group to his home. Nicki later attempts to seduce Frank and becomes angry when her advances are refused. The next day, Fletcher is almost killed when a bomb explodes inside the boat he rode moments before. After finding footprints around the cabin and sabotaged automobiles, Frank realizes that Rachels stalker has followed them.

After securing the house for the night,  Frank plans to leave with Rachel and company in the morning, then learns that Rachels obsessive stalker and the person trying to kill her are not the same person. Angry and drunk, Nicki admits that during a drug-induced fit of jealousy she hired a hitman to kill Rachel, but that the letters from the stalker came before that. After Fletcher was almost killed, Nicki realized the hitman would kill anyone to get to his target, including her beloved nephew. However, she cannot call it off because she does not know the killers identity. Abruptly, the hitman breaks into the house and shoots Nicki dead. Frank, who is armed with a semi-automatic pistol, ensures that his father has secured the rest of the group on the second floor. Upon tracking the killer and then pursuing him into the woods, Frank shoots but misses, allowing the former to escape capture. Frank learns the next day, from his Secret Service contacts, that they have apprehended the stalker and were interviewing him when Nicki was shot.

Frank and Rachel attend Nickis funeral and then the Academy Awards ceremony. A host of backstage technical issues hamper Franks efforts to monitor the proceedings closely. During the actual show, Rachel freezes and runs offstage, angry at Frank for embarrassing her with overprotective measures. However, Rachel returns to the audience and is present when announced as the winner for Best Actress. As she comes toward the stage to accept the award, the hitman is revealed to be Portman. Frank notices Portman pointing a gun disguised as a camera at Rachel. As Portman prepares for the fatal shot, Frank runs on stage and leaps in front of Rachel, intercepting the shot. Once regaining his balance, he shoots Portman through his camera-gun, killing him. Frank is left wounded and Rachel calls for help—all the while urging him to stay with her.

Frank recovers from the shooting and goes to say goodbye to Rachel at the airport. After the plane starts to taxi, Rachel suddenly jumps out and runs to Frank for one last passionate kiss. The two reluctantly let go. Rachel boards the plane, and Frank leaves for his next protection detail.

==Cast==
* Kevin Costner as Frank Farmer
* Whitney Houston as Rachel Marron
* Gary Kemp as Sy Spector
* Bill Cobbs as Bill Devaney
* Ralph Waite as Herb Farmer
* Tomas Arana as Greg Portman
* Michele Lamar Richards as Nicki Marron Mike Starr as Tony Scipelli
* Christopher Birt as Henry Adams
* DeVaughn Nixon as Fletcher Marron
* Gerry Bamman as Ray Court
* Joe Urla as Minella

==Background== Steve McQueen and Diana Ross in the leads, but negotiations fell through as McQueen refused to be billed second to Ross.  It was proposed again in 1979,   starring Ryan ONeal and Ross again in the leads. The project fell through due to irreconcilable differences in the relationship between the two stars. Costner stated that he based Frank Farmer on Steve McQueen; even cutting his hair like McQueen (who had died 12 years earlier).

Olivia Newton-John, Pat Benatar, Madonna (entertainer)|Madonna, Cher, Joan Jett, Deborah Harry, Liza Minnelli, Janet Jackson, Donna Summer, Terri Nunn, Kim Carnes, and Dolly Parton were all at some point considered for the role of Rachel Marron. 

The 1920s Beverly Hills mansion and grounds featured in the film once belonged to William Randolph Hearst; also featured in The Godfather (1972).

==Reception==

===Critical reception=== Worst Picture, but did not win any awards.  Owen Gleiberman, of Entertainment Weekly reviewed the film negatively, stating, "To say that Houston and Costner fail to strike sparks would be putting it mildly." He added, "  the movie gives us these two self-contained celebrity icons working hard to look as if they want each other. Its like watching two statues attempting to mate."  However, other critics praised the film, such as Roger Ebert, who gave the film three out of four stars, remarking "The movie does contain a love story, but its the kind of guarded passion that grows between two people who spend a lot of time keeping their priorities straight." 
 Run to Oscar for Best Original Album of the Year for its soundtrack album of the same name.  Golden Screen Award and an Award of the Japanese Academy. All of which are listed on IMDBs website.
 John Wilsons book The Official Razzie Movie Guide as one of The 100 Most Enjoyably Bad Movies Ever Made. 

===Box office===
In the United States, The Bodyguard opened on November 25, 1992 in 1,717 theaters; it grossed $16.6 million in its opening weekend, ranking third.  The film was in theaters for thirteen non-consecutive weeks, ultimately grossing $121.9 million domestically, and $410.9 million worldwide. It was the seventh highest-grossing film of 1992 in North America, and the second highest-grossing film of 1992, worldwide.  At the time, the film became one of the 100 all-time highest grossing films worldwide, though it has since fallen out.  . Box Office Mojo (1993-04-27). Retrieved on 2011-04-05. 

==Soundtrack==
 
The Bodyguard: Original Soundtrack Album became the best-selling soundtrack of all time. It has been certified diamond in the United States (sales of at least ten million) with shipments of over 17 million copies.  Worldwide, the sales are over 45 million copies.  In addition, Houstons cover of "I Will Always Love You" sold 12 million units worldwide.   
 I Will Run to Queen of the Night".

In 2013, La-La Land Records released a limited edition CD (3500 units) of Alan Silvestris original score. 

==Parodies==
* The Simpsons&nbsp;– in the episode "Mayored to the Mob", Homer Simpson receives bodyguard training from an instructor singing "I Will Always Love You" after graduating his class. Also in the episode, Mark Hamill gets carried by Homer in a fashion which parodies the way Costner carries Houston in the film.
* 30 Rock&nbsp;– in the episode "Hard Ball", Tracy Jordan is rescued from a mob by his entourage; his character sang "I Will Always Love You" during the scene.
* Bulletproof (1996 film)|Bulletproof&nbsp;– a scene in the film parodies The Bodyguard with Adam Sandlers character singing "I Will Always Love You" and remarking that Damon Wayanss character can always be his bodyguard. Myra Brandish says she was taught "Never let them out of your sight. Never let your guard down. Never fall in love", the tag line from the film.
* In Living Color&nbsp;- Kim Wayans plays Grace Jones as the Whitney Houston character and Jim Carrey plays the Kevin Costner character, complete with bad hair. The scene when Kevin Costner carries Whitney Houston off-stage from the original movie is reversed with Grace Jones carrying Frank Farmer off while singing "I Will Always Love You".
* American Dad!&nbsp;- In the episode "Lincoln Lover", Stan Smith creates a play which depicts the relationship between Abraham Lincoln and his bodyguard. At the end of the play, the song "I Will Always Love You" starts playing.
* Joey (TV series)|Joey&nbsp;- In the episode "Joey and the Beard", Joey Tribbiani is told, "You know the rules: Never let her out of your sight, never let your guard down, and never fall in love." He quickly recognizes this as the films tagline.

==Sequel and remake== remake that will update the story to reflect the world of the Internet; in which sites such as Twitter, Google Maps and countless other sites make access to celebrities easier than ever. The remake will be penned by Jeremiah Friedman and Nick Palmer. Singer Rihanna was once sought after to play the role of Rachel. 

On March 10, 2011, Rihanna ruled out playing the part saying, "Absolutely not. I hate it when singers do singing movies all the time, because you can never look at them as anybody else. I want to play a character. My whole life is playing Rihanna; being a singer wont be a stretch for me." 
 Princess Diana and he "had been talking about doing Bodyguard 2. I told her I would take care of her just the same way that I took care of Whitney." He said the script was delivered to him on August 31, 1997 – the day before Princess Diana died of injuries she sustained in a car crash in Paris at the age of 36.

==Musical adaptation==
  Run to Queen of the Night". Several of Houstons other songs are used as well, including "Im Your Baby Tonight (song)|Im Your Baby Tonight", "How Will I Know", and "I Wanna Dance with Somebody (Who Loves Me)". Tony Award-winning actress and singer Heather Headley was cast to play Rachel, the role played in the film by Houston. MOBO Award-winning and BRIT-nominated singer Beverley Knight took over Rachel in September 2013 through to May 2014; she was replaced by Alexandra Burke in June 2014.

==References==
 

==External links==
*  
*  
*  
*  

 
  
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 