Pattathu Raani
{{Infobox film
| name           = Pattathu Raani
| image          = 
| image_size     =
| caption        = 
| director       = Manivasagam
| producer       = Rajeswari Manivasagam P.S. Mani
| writer         = Manivasagam K. C. Thangam  (dialogues) 
| starring       =   Deva
| cinematography = R. H. Ashok
| editing        = L. Kesavan
| distributor    =
| studio         = Raja Pushpa Pictures
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Tamil
}}
 1992 Tamil Tamil comedy Vijayakumar and Deva and was released on 14 August 1992.  

==Plot==

Jalagandeswaran (Goundamani) and Rayappan (Senthil (actor)|Senthil) are brothers and house owners of a small colony. The tenants refuse to pay up their rents, knowing that the houses were built on State land. Rayappan has the idea to rent a vacant house to Usha (Gouthami), a beautiful company manageress. Usha is married to a middle-aged man Sundaram (Vijayakumar (actor)|Vijayakumar). The couple move into their new house and befriend with the neighbours. The married neighbours Jalagandeswaran, Viswanathan (Janagaraj), Ganesan (Delhi Ganesh) and Coimbatore (Manivasagam) fall in love with the beautiful Usha. They begin to woo the young woman. What transpires later forms the crux of the story.

==Cast==
 Vijayakumar as Sundaram
*Gouthami as Usha Manorama as Muniamma
*Goundamani as Jalagandeswaran
*Janagaraj as Viswanathan Senthil as Rayappan
*Delhi Ganesh as Ganesan
*Manivasagam as Coimbatore
*Sethu Vinayagam
*Vijayaraj as Nagaraj
*Vadivukkarasi as Yamuna
*Kovai Sarala as Savitri
*Unni Mary (credited as Deepa)
*Mahesh as Ramakrishnan
*Ravali (credited as Mythili) as Shanthi
*Idichapuli Selvaraj
*Karuppu Subbiah
*Vellai Subbiah
*Thidir Kannaiah
*Pasi Sathya
*Vasuki
*R. Sundarrajan in a guest appearance

==Soundtrack==

{{Infobox Album |  
| Name        = Pattathu Raani
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1992
| Recorded    = 1992 Feature film soundtrack |
| Length      = 24:30
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1992, features 5 tracks with lyrics written by Kalidasan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Ada Thotta || Mano (singer)|Mano, K. S. Chithra  || 4:37
|- 2 || Devathi || K. S. Chithra || 6:20
|- 3 || Muthu Muthu || S. Janaki || 4:28
|- 4 || Pennaga Piranthore || Mano, S. Janaki || 4:41
|- 5 || Sound Kodu || Krishnachandran, Mano, Radhika || 4:24
|}

==Reception==
K. Vijayan of New Straits Times gave the film a mixed review citing "See it with your wife and you probably enjoy it yourself" and described the film as "comedy loses steam midway". 

==References==
 

 
 
 
 
 