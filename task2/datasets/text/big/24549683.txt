Vesham
{{Infobox Film
| name           = Vesham
| image          = Vesham.png
| director       = V M Vinu (director)|V. M. Vinu
| producer       = Swargachithra Appachan
| writer         = T. A. Razzaq assisted by Manoj R Innocent Saikumar Sai Kumar Indrajith Mohini Mohini Gopika
| music          = S.A. Rajkumar
                   Kaithapram Damodaran Namboothiri   (Lyrics)  
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =
| awards         =
}}

Vesham (  directed by V M Vinu (director)|V. M. Vinu in which Mammootty plays the lead role Appu, a businessman. The film is highly regarded as the best family entertainer of the year. The film was Mammoottys third consecutive commercial success after Kaazhcha and Black (2004 film)|Black.

==Plot==

The movie narrates the story of Appu, an elder brother who sacrifices his life for the younger one, an often repeated theme seen in Malayalam movies.

Mammootty plays the lead role Appu, M.D. of Leela Group of companies, a self-made businessman. Appu is a workaholic but a family man. He is married to Ashwathi (Mohini (actress)|Mohini). Appus father Pappettan (Innocent (actor)|Innocent) had a humble beginning as a porter.
 Sai Kumar), the M.D. of Bharath Motors, who wants to destroy Leela Group. Sivan tries to influence Hari through his secretary Veni (Sindhu Menon) and her lover Deepak (Riyaz Khan). Hari has already been creating problems at the office as well as at home but Appu tolerates it. The story takes a turn with the accidental death of Deepak which was actually committed by Hari. In order to save his brothers life, Appu confesses to the murder. Rest of the movie deals with Sivans attempt to take over Leela Group and how Appu takes revenge as well as the agony associated with it.

==Cast==
*Mammootty as Appu Innocent as Pappan (Father of Appu) Sai Kumar as Sivan Indrajith as Hari (Younger brother of Appu) Mohini as Ashwathi (Appus wife)
*Gopika as Revathy (Haris wife)
*Cochin Haneefa as brother-in-law of Appu & Hari
*Jagathy Sreekumar as Ganapathi
*Sindhu Menon as Veni
*T. P. Madhavan
*Riyaz Khan as Deepak Augustine

==External links==
*  

 
 
 
 

 