Shade (film)
{{Infobox film|
  name = Shade |
  image = Shade03.jpg|
  caption = Theatrical release poster | Chris Hammond  Ted Hartley  David Schnepp |
  director = Damian Nieman |
  writer = Damian Nieman |
  starring = Stuart Townsend Gabriel Byrne  Thandie Newton  Jamie Foxx  Melanie Griffith  Sylvester Stallone |
  music = James Johnzen | 	 
  studio = DEJ Productions  RKO Pictures |
  distributor = Dimension Films |
  released = June 21, 2003  |
  runtime = 101 minutes |
  country = United States| English |
  preceded_by = |
  followed_by =  |
  awards = |
  budget = $10 million {{cite web
  | last = 
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Shade
  | work = 
  | publisher = Box Office Mojo
  | date = 
  | url = http://www.boxofficemojo.com/movies/?id=shade.htm
  | doi = 
  | accessdate = 2011-07-25}} |
  gross = $459,098  {{cite web
  | last = 
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Shade (2004) International Box Office
  | work = 
  | publisher = Box Office Mojo
  | date = 
  | url = http://www.boxofficemojo.com/movies/?page=intl&id=shade.htm
  | doi = 
  | accessdate = 2011-07-25}} |
|}} grifters who attempt to set up a legendary card sharp nicknamed "The Dean".

==Plot==
 
Many years ago, a young Dean is playing in a mob-run illegal underground poker game. The game is raided by robbers who order everyone to put up their hands. The Dean reluctantly does so, revealing the card hes muck (gambling)|concealed. Incensed, a fellow player grabs a gun and starts shooting, setting off a firefight that kills everyone in the club except the Dean and one of the mobsters. The two men point their guns at each others heads.
 Las Vegas blackjack dealer. Working with a small crew, he switches out the contents of a six deck shoe and they take the casino for $40,000.

As Vernon and Charlie wait for Larry at another bar, Scarne (Bo Hopkins), a corrupt cop, shakes them down and empties Vernons wallet. Vernon saves the bulk of his bankroll because its stashed in his boot. Larry arrives shortly after Scarne leaves and after a demonstration of Vernons card-handling skills agrees to team with them. The plan is for Larry to take down large pots on Vernons crooked deals.
 bondage scene. After hes bound, Tiffany lets in a surgical team to harvest his kidney for black market transplant.

Charlie and Larry head into a house for the game; Vernon is already there. Larry gets impatient with the slow action and, on his own deal, gets over $100,000 in the pot heads up with four 10s. He loses to four Jacks. Unfortunately for him, the money he bet doesnt belong to him. It belongs to a mobster named Malini (Patrick Bauchau), who sends two of his enforcers, Marlo (Roger Guenveur Smith) and Nate (B-Real), to retrieve him. They take him to the house, which has been stripped bare. Everyone at the game was in on the con. The enforcers take Larry to an airport and under cover of the jet noise, kill him.

Returning to the scene from many years ago, as Charlie tells the story. Dean and the mobster agree to cut cards for the money. The mobster cuts a King and Dean cuts the Ace of spades. The mobster goes for his gun but Dean gets to his first, shooting the mobster and splashing the Ace with his blood. The three speculate that the story is an urban legend.

At Eves upscale bar and restaurant, the Dean (Sylvester Stallone) and Eve (Griffith), his former partner, reminisce. The Dean is considering retiring. Intercut with this scene, Charlie, Vernon and Tiffany talk about taking the Dean down at a game with a $250,000 buy-in and total stakes of at least $2,000,000. Following this conversation and Vernons departure, its revealed that Vernon and Tiffany had been lovers until Vernon left and Tiffany slept with Charlie.

The next day, Malinis enforcers track down Charlie to a restaurant and Marlo demands the return of Marinis money. Charlie agrees to pay back $100,000 but instead Nate pulls a gun and opens fire. In the ensuing firefight Nate kills the bartender and Tiffany arrives in time to kill Nate, but not Marlo, who escapes. Charlie, Vernon and Tiffany run away and prior to the game they hide out at the Magic Castle. There they encounter The Professor (Hal Holbrook), Vernons former mentor before Charlie lured him into a life on the grift. The Professor counsels him to be careful, noting that hes known most of the best mechanics but hes never known a retired one.

Meanwhile, Scarne, a vice squad detective, intrudes into the murder scene, where an unnamed detective has been assigned to the murders at the restaurant. The witness descriptions are of two men and a woman leaving the scene and he realizes that the three are involved.

Later that night the three arrive at the Hollywood Roosevelt Hotel for the game, where they discover that the host is the gangster Malini. They meet the other players, including the Dean. One by one, Vernon and the Dean bust the other players until theyre heads up and they decide to play five card stud for the rest of the game. Eve arrives, and they take a break. Eve and the Dean talk privately about the difficulty hes having trying to bust Vernon. The three grifters talk amongst themselves about the trouble Vernon is having getting the Dean to bite when Vernon makes plays at him and Tiffany speculates that the cards are marked. The three grab some cards and head to another room, where Vernon discovers that the Dean is using a "juice deck," a deck marked in such a way as to be readable when ones eyes are unfocused. They return to the game prepared to use the deck against the Dean.

In the climactic final hand of the game, Vernon mucks a card and deals the hand. He deals the Dean two pair, Kings and Queens (with one King in the hole), and himself a pair of Jacks with a 7 in the hole for the Dean to see. The Dean goes all in, leaving Vernon $50,000 short. Charlie and Tiffany make up the shortfall and Vernon calls the bet. Before the cards are turned up, Scarne arrives at the hotel and Marlo enters the room. Identifying the three as the team who took off Larry, Malinis muscle pull their guns and Tiffany pulls hers, then Scarne enters with his gun drawn. The Dean insists that the hand be completed and Vernon swaps out his hole card 7 for a third Jack, which would beat the two pair hed dealt the Dean. The room is stunned when the Dean turns up a third Queen to take the hand and a $2,000,000 pot. Malini, dismissing Larry as an "idiot," tells the three they can leave but advises them to stay out of the Los Angeles rackets. Scarne departs. The Dean and Eve leave with his winnings, but not before the Dean offers Vernon a last piece of advice: "Always re-check your hole cards. Hell, I was mucking cards before you were born."

Charlie splits up the partnership with Vernon and (with Marlos revelation that he was tipped off by Tiffany about their shaking down Larry) also with Tiffany.

Vernon is sitting alone in a diner. The Dean, Eve and Scarne enter. The game with The Dean was all an elaborate set up by the four of them to take off Charlie and Tiffany. They split the take and part, the Dean and Eve leaving together. The Dean pauses to flip the blood-stained Ace of spades to Vernon.

==Soundtrack==
 
The Shade soundtrack features three original works, "Penumbra," "Moon Rocks" and "Red Reflections", composed and recorded by jazz composer and flugelhornist Dmitri Matheny.

==Critical reception==
The film currently holds a 67% "fresh" rating at Rotten Tomatoes. {{cite web
  | last = 
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Shade
  | work = 
  | publisher = Rotten Tomatoes
  | date = 
  | url = http://www.rottentomatoes.com/m/shade/
  | format = 
  | doi = 
  | accessdate = 2008-05-11}} 

Shade was open for only five weeks in six theaters, and it grossed $459,098 in worldwide ticket sales.   It is available on DVD from Warner Home Video. 

The movie was produced by RKO Pictures in 2003 and released in the USA on 21 June 2003 at the CineVegas International Film Festival.  It began its limited release theatrical run on 7 May 2004. {{cite web
  | last = 
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Shade
  | work = 
  | publisher = Internet Movie Database
  | date = 
  | url = http://www.imdb.com/title/tt0323939/releaseinfo
  | format = 
  | doi = 
  | accessdate = 2008-05-11}} 

==Trivia== Charlie Miller, Ed Marlo, Max Malini, Dai Vernon ("The Professor"), Larry Jennings, Arthur Buckley, Nate Leipzig, John Scarne and Jacob Daley.
 Earl Nelson. Nieman also had Earl Nelson, R. Paul Wilson and Lee Asher teach Sylvester Stallone and Stuart Townsend how to perform their card tricks for the film.

The Expert at the Card Table from S. W. Erdnase (E. S. Andrews) book is also shown throughout the movie.

==Cast==
* Stuart Townsend as Vernon
* Gabriel Byrne as Charlie Miller
* Thandie Newton as Tiffany
* Jamie Foxx as Larry Jennings
* Sylvester Stallone as The Dean
* Melanie Griffith as Eve
* Bo Hopkins as Scarne
* Patrick Bauchau as Max Malini
* Roger Guenveur Smith as Marlo
* B-Real as Nate
* Jason Cerbone as Young Dean Stevens
* Glenn Plummer as Gas Station Attendant
* Dina Merrill as Dina
* Hal Holbrook as The Professor

==Notes==
 

==See also==
*Dai Vernon

== External links ==
* 
* 
* 

 
 
 
 
 
 
 
 