Krishnan Marriage Story
{{Infobox film
| name           = Krishnan Marriage Story
| image          = Krishnan marriage story poster.jpg
| caption        = Theatrical poster
| director       = Nuthan Umesh
| producer       = R. Vijaya Kumar
| writer         = 
| starring       = Ajay Rao Nidhi Subbaiah Jai Jagadish
| music          = Sridhar V. Sambhram
| cinematography = Shekar Chandra
| editing        = Sri Crazymindz
| studio         = Crazymindz
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         =
| gross          = 
}}
Krishnan Marriage Story is an Indian Kannada-language romantic drama film written and directed by debutant Nuthan Umesh, starring Ajay Rao and Nidhi Subbaiah in the lead roles. Vijaya Kumar is the producer and Sridhar V. Sambhram is the music director of the film. The story revolves around the main lead character who plays an advertising executive and his large joint family. 
 

==Cast==
* Ajay Rao
* Nidhi Subbaiah
* Jai Jagadish
* Vinaya Prasad
* Balaraj
* Jayashree
* Sri Crazymindz

== Soundtrack ==

{| class="wikitable"
|-
! SL.No
! Song
! Artist
|-
| 1
| "Nidde Bandilla"  
| Mika Singh, Nanditha, Chaitra H. G., Anuradha Bhat
|-
| 2
| "Ee Sanje"
| Sonu Nigam, Anuradha Bhat
|-
| 3
| "Ayyoo Raama Raama"
| Harsha Sadananda, Akansha Badami, Apoorva Sridhar 
|-
| 4
| "Parijathada"
| Lakshmi Manmohan, Rajesh Krishnan
|-
| 5
| "My Heart Is Beating"
| Santosh Venki
|-
| 6
| "Ee Janmavu"
| Sonu Nigam, Shreya Ghoshal
|}

== References ==
 

 
 
 
 
 


 