Showdown at the Cotton Mill
 
 
{{Infobox film
| name           = Showdown at the Cotton Mill
| film name =  
| image          = 
| alt            = 
| caption        = 
| director       = Wu Ma
| producer       = Lau Wai-Ban
| writer         = Kuang Ni
| starring       = Chi Kuan Chun|Kuan-Chun Chi Tao-liang Tan Peng Chang
| music          = Frankie Chan
| cinematography = Chen Hay-Lock
| editing        = Peter Cheung
| studio         = Long Year Film Company Production
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = Taiwan / Hong Kong
| language       = Mandarin
| budget         = 
}} Hong Kong martial arts action film directed by Wu Ma. The film was believed to be lost until Rarescope uncovered the film in a Taiwanese film vault. It is a sequel to the 1976 film The Shaolin Avengers, which was co-directed by Wu Ma and Chang Cheh and stars Chi Kuan Chun|Kuan-Chun Chi as Hu Hui-Chien.

==Plot== Canton at Shaolin master Hu Hui-Chien. This leads to the classic northern kicks and southern fist duel.

==Cast==
* Chi Kuan Chun|Kuan-Chun Chi as Hu Hui-Chien
* Tao-liang Tan as Master Kao Chin Chung
* Peng Chang as Uy Hsing-Hung
* Ching Kuo Chung as Tung Chien-Chin
* Wan Fei
* Keung Li
* Mao Shan

==External links==
*  
*  

 
 
 
 
 
 
 
 


 
 