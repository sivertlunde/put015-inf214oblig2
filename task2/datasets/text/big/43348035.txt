Animosity (film)
{{Infobox film
| name           = Animosity
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Brendan Steere
| producer       = Brandon Taylor Roy Frumkes
| writer         = Brendan Steere
| screenplay     = 
| story          = 
| based on       =  
| starring       = Tracy Willet  Marcin Paluch  Thea McCartan  Tom Martin  Alyssa Kempinski  Stephan Goldbach   Michelle Jones
| narrator       = 
| music          = Geoff Gersh
| cinematography = Jesse Gouldsbury
| editing        = Steve Burgess
| studio         = Laika Come Home
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Animosity is a 2013 horror thriller film written and directed by Brendan Steere.  It had its world premiere on May 13, 2013 and stars Tracy Willet and Marcin Paluch as two newlyweds that discover a sinister presence in the woods. 

==Synopsis==
Mike (Marcin Paluch) and Carrie (Tracy Willet) Bonner are newly wed and have decided to move into a house set deep into a quiet and secluded forest. Carrie is unnerved by a series of strange encounters with others living in the area, encounters that her husband dismisses as nothing to worry about. Shes horrified when she witnesses a violent event and becomes convinced that the area is home to sinister supernatural powers.

==Cast==
*Marcin Paluch as Mike Bonner
*Tracy Willet as Carrie Bonner
*Thea McCartan as Lauren
*Tom Martin as Carl Hampton
*Stephan Goldbach as Tom
*Alyssa Kempinski as Nicole
*Matt Ziegel as Joe
*Michelle Jones as Erin
*Rob ORourke as Boy

==Reception== Cold in July.  Bloody Disgusting was mixed and gave the movie two skulls, stating that the movie could very well improve with repeated viewings and that they would write a new review if this was the case, but that they left the theater "feeling worn out and unsatisfied." 

===Awards===
* Dreamer Award for Best Horror Film at the Buffalo Dreams Fantastic Film Festival (2013, won)   
* Dreamer Award for Best Actress at the Buffalo Dreams Fantastic Film Festival (2013, won - Tracy Willet) 

==References==
 

==External links==
*  
*  
*  

 
 