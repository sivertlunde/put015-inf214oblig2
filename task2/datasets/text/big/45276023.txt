Mass E Bhat
{{Infobox film
| name           = Mass E Bhat
| image	         = Mass E Bhat.jpg
| image size     = 
| caption        = 
| director       = Hannan Majid Richard York
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = 
| music          = John Pandit Lou Beckett Rowen Perkins   MASS - E -  BHAT 
| cinematography = 
| editing        = Hugh Williams
| studio         = Rainbow Collective Channel 4 BRITDOC Foundation
| distributor    = 
| released       =  
| runtime        = 72 minutes 
| country        = United Kingdom Bengali
| budget         = 
| gross          = 
}}
 British documentary film directed by Hannan Majid and Richard York. The film weaves together the stories of children living and working in Bangladesh.

==Summary==
Set in Bangladesh, the documentary follows 20-year-old Nasir, a social worker in the slums, who moved from a rural village to the city. He reflects and recounts on his childhood working in rubbish dumps and sweatshops from the age of eight, how he grew up, and achieved his dream of an getting an education and respect within his community.

As social worker, he wanders the alleyways of Dhakas Korrail slum searching for working children to try to convince to enrol in school for a better future. As Nasir recounts his life, the documentary also features several children, parents and employers, who mirror his past.

==Development==
In September 2009, Mass E Bhat entered the Good Pitch event at Amnesty International|Amnestys East London Auditorium, organised by Channel 4s BRITDOC Foundation and Sundance Institute.       The film subsequently received production funding from the Channel 4 BRITDOC Foundation.   

After finishing editing and post-production on the film, an outreach campaign was launched to raise awareness and improve treatment for children like those it featured in the film. 

==Release== Rix Mix.       In October of the same year, it was screened at Elefest.   

==Reception==
Frontline Club London said, "Mass E Bhat is a portrait of a developing nation through the eyes of its children."    Rich Mix called the film an "exploration of what it means to grow up in Bangladesh... A film full of fascinating, moving stories, this is a portrait of a nation in flux, the emotional impact of which is raised by a brilliant score from John Pandit from Asian Dub Foundation."   

Oliver Zarandi of East End Review said of the film, "The filmmakers see Mass E Bhat as a way of reaching out to cinema goers and raising awareness while passing on the skills of documentary making to another generation."  Tarannum Nibir of Paraa said of the film, "kids can express themselves, share their lifesyles and also can show anything they want in a film."   

==Awards and nominations==
{| class="wikitable sortable"
! Year
! Award
! Category
! Result
|-
| rowspan="2"| 2014
| rowspan="2"| East End Film Festival
| Best Documentary Award
|  
|-
| Best Soundtrack Award
|  
|-
|}

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 
 

 