Burn After Reading
 
{{Infobox film
| name           = Burn After Reading
| image          = Burn_After_Reading.jpg
| image_size     = 215px
| caption        = Theatrical release poster Joel Coen Ethan Coen
| producer       = Joel Coen Ethan Coen
| writer         = Joel Coen Ethan Coen
| starring       = George Clooney Frances McDormand John Malkovich Tilda Swinton Richard Jenkins Brad Pitt
| music          = Carter Burwell
| cinematography = Emmanuel Lubezki
| editing        = Joel Coen Ethan Coen  (as Roderick Jaynes) 
| studio         = Relativity Media StudioCanal Working Title Films
| distributor    = Focus Features
| released       =  
| runtime        = 96 minutes
| country        = United States United Kingdom France
| language       = English
| budget         = $37 million
| gross          = $161,128,228
}} Joel and 2008 Venice Film Festival.  It was released in the United States on September 12, 2008, and in the United Kingdom on October 17, 2008.

==Plot== CIA Intelligence analyst and resolves to write a memoir about his life and career. When his pediatrician wife Katie (Tilda Swinton) finds out, she sees it as a justifiable opportunity to file for divorce and continue her adulterous affair unimpeded. Taking her lawyers advice, she copies financial records and several other files from her husbands computer onto a CD. These files contain a rambling but meaningless diatribe by Cox on purported CIA activities. 
 Russian embassy, offering more information in return for monetary compensation. With no other data to give them, Linda persuades Chad to sneak into the Coxs home to get more files from their computer.
 Treasury Department employee and U.S. Marshal, Harry is coincidentally also secretly seeing Linda. Harry finds Chad, whom Linda sent to find more files on Osbourne, hiding in a wardrobe in the Coxs home, panics, and fatally shoots Chad in the head.

Two days later at the CIA headquarters, Palmer Smith (David Rasche), Osbournes former superior, and his director (J. K. Simmons) learn that information from Osbourne has been given to the Russian Embassy. The two men are perplexed, given Osbournes low-level security clearance, the material delivered to the Russians is of no importance to anyone, and the apparent motive of all involved parties remain unknown. Smith is told to maintain observation until the situation "makes sense". Harry, burdened by keeping the day priors events secret, gets into an argument with Katie and decides to leave the house. On his way out, he spots a man (Kevin Sussman) who has been trailing him for the past several days. After tackling him to the ground, Harry finds out that the man is a private detective hired by his wife Sandra "Sandy" (Elizabeth Marvel) to gather evidence for impending divorce proceedings.  Sandy is shown to be having an extramarital liaison of her own. Harry is devastated and goes to see an agitated Linda, who confides in Harry that Chad is missing. Harry agrees to help find Chad.

The next morning, Harry and Linda meet in a park, and Linda provides him with more information about Chads disappearance. When Linda mentions the name "Osbourne Cox", Harry figures out that Chad is the man he shot. He panics, realizes that there are strange men in the park (most likely the CIA people trailing Linda) and flees, assuming Linda is a spy. Linda then turns to Ted Treffon (Richard Jenkins), the kindhearted manager of Hardbodies, who has unrequited feelings for her and has been critical of Linda and Chads scheming thus far. Believing the Russians have kidnapped Chad, he agrees to go to the Coxs home to search Osbournes computer. Unemployed and having spent the past several days living on a small boat, Osbourne becomes unhinged when he finds out that Katie has emptied his bank accounts and, no longer having keys, decides to break into the house to get some of his personal belongings. Finding Ted in the basement, Osbourne initially takes him to be Katies lover. He soon realizes Teds affiliation with Linda and what he refers to as the "league of morons" he feels that he has been struggling against his whole life and fires a gunshot at Ted, wounding him. Ted manages to get out of the house, only to be fatally attacked by a hatchet-wielding Osbourne.

At CIA headquarters a few days later, Palmer and his director try to understand what exactly happened. It is revealed that while trying to board a flight to Venezuela, Harry was detained because his name was on a hot list and that the CIA are holding Linda, who is promising to "play ball" and "sit on it" if they will pay for her plastic surgery. A CIA agent shot Osbourne during his assault on Ted and the bullet has put Osbourne in a coma. The director instructs Palmer to let Harry fly to Venezuela (as that country has no extradition treaty with the US and therefore will not send him back) and pay for Lindas surgery. The director and Palmer conclude that despite their oversight and the unusual events that have unfolded, there appears to be no lesson for the agency to have learned, be it moral, espionage, or otherwise. "I guess we learned not to do it again," the director concludes (despite not knowing exactly what they did), and closes his file.

Meanwhile, staff at the Russian embassy have dismissed the contents of Cox ramblings in the files from his computer - arguably the one factor that started off the entire chain of events - as "drivel".

==Cast==
  
* George Clooney - Harry Pfarrer
* Frances McDormand - Linda Litzke
* John Malkovich - Osbourne Cox
* Tilda Swinton - Katie Cox
* Brad Pitt - Chad Feldheimer
* Richard Jenkins - Ted Treffon
* J. K. Simmons - CIA superior
* Elizabeth Marvel - Sandra "Sandy" Pfarrer
* David Rasche - Palmer Smith
* Jeffrey DeMunn - Cosmetic surgeon
* Kevin Sussman - Surveillance Olek Krupa - Krapotkin
* Dermot Mulroney - Star of Coming Up Daisy
 

==Production==
Working Title Films produced the film for Focus Features, which also has worldwide distribution rights.   
 Sleepy Hollow Taiko drums. Joel Coen said they wanted the score to be "something big and bombastic, something important sounding but absolutely meaningless."   
 The Man Who Wasnt There.    Ethan Coen compared Burn After Reading to the Allen Drury political novel Advise and Consent and called it "our version of a Tony Scott/Jason Bourne kind of movie, without the explosions."    Joel Coen said they intended to create a spy movie because "we hadnt done one before,"    but he feels the final result was more of a character-driven movie than a spy story. Joel also said Burn After Reading was not meant to be a comment or satire on Washington D.C. 

Parts of the Burn screenplay were written while the Coens were also writing their adaptation of No Country for Old Men.  The Coens created characters with actors George Clooney, Brad Pitt, Frances McDormand, John Malkovich and Richard Jenkins in mind for the parts, and the script derived from the brothers desire to include them in a "fun story."    Ethan Coen said Pitts character was partially inspired by a botched hair coloring job from a commercial the actor filmed.    Tilda Swinton, who was cast later than the other actors, was the only major actor whose character was not written specifically for her. The Coens struggled to develop a common filming schedule among the A-list cast. 

Production Weekly, an online entertainment industry magazine, falsely reported in October 2006 that Burn After Reading was a loose adaptation of Burn Before Reading: Presidents, CIA Directors, and Secret Intelligence, a memoir by former Director of Central Intelligence|U.S. Director of Central Intelligence Stansfield Turner.  Although both stories involve the Central Intelligence Agency and derive their titles from the top secret classification term, the Coen brothers script has nothing to do with the Turner book; nevertheless, the rumor was not clarified until a Los Angeles Times article more than one year later. 

  in August 2008.   

The Coen brothers said idiocy was a major central theme of Burn After Reading; Joel said he and his brother have "a long history of writing parts for idiotic characters"  and described Clooney and Pitts characters as "dueling idiots."  Burn After Reading is the third Coen brothers film for Clooney (O Brother, Where Art Thou? and Intolerable Cruelty), who acknowledged that he usually plays a fool in their movies: "Ive done three films with them and they call it my trilogy of idiots."  Joel said after the last scene was shot, "George said: OK, Ive played my last idiot! So I guess he wont be working with us again."   

Pitt, who plays a particularly unintelligent character, said of his role, "After reading the part, which they said was hand-written for myself, I was not sure if I should be flattered or insulted."  Pitt also said when he was shown the script, he told the Coens he did not know how to play the part because the character was such an idiot: "There was a pause, and then Joel goes...Youll be fine."   
 affair with Michael Clayton, prompting Clooney to say to Swinton at the end of a shoot, "Well, maybe one day well get to make a film together when we say one nice thing to each other."  Swinton said of the dynamic, "Im very happy to shout at him on screen. Its great fun." 

Swinton described Burn After Reading as "a kind of monster caper movie,"  and said of the characters, "All of us are monsters – like, true monsters. Its ridiculous."  She also said, "I think there is something random at the heart of this one. On the one hand, it really is bleak and scary. On the other, it is really funny. ... Its the whatever-ness of it. You feel that at any minute of any day in any town, this could happen."  Malkovich said of the characters, "No one in this film is very good. Theyre either slightly emotional or mentally defective. Quirky, self-aggrandizing, scheming."  Pitt said the cast did little ad-libbing because the script was so tightly written and wove so many overlapping stories together.  Veteran actor Richard Jenkins said the Coen brothers asked him if he could lose weight for his role as the gym manager, to which Jenkins jokingly replied, "Im a 60-year-old man, not Brad Pitt. My body isnt going to change." 

Joel Coen said the sex machine built by Clooneys character was inspired by a machine he once saw a key grip build, and by another machine he saw in the Museum of Sex in New York City. 

==Reception==

===Critical reception=== No Country for Old Men."    Pulver, who also gave Burn After Reading four out of five stars, said it "may also go down as arguably the Coens happiest engagement with the demands of the Hollywood A-list."  Pulver said Brad Pitt had some of the funniest moments and that compared to the other Coen brothers movies, Burn After Reading most resembles Intolerable Cruelty.  The Hollywood Reporter reviewer Kirk Honeycutt complimented the actors for making fun of their screen personae, and said the Coen brothers "have taken some of cinemas top and most expensive actors and chucked them into Looney Tunes roles in a thriller."    Honeycutt also said "it takes awhile to adjust to the rhythms and subversive humor of Burn because this is really an anti-spy thriller in which nothing is at stake, no one acts with intelligence and everything ends badly." 

Todd McCarthy, of Variety (magazine)|Variety magazine, wrote a strongly negative review of Burn After Reading, which he said "tries to mate sex farce with a satire of a paranoid political thriller, with arch and ungainly results."    McCarthy said the talented cast was forced to act like cartoon characters, described Carter Burwells score as "uncustomarily overbearing"  and said the dialogue is "dialed up to an almost grotesquely exaggerated extent, making for a film that feels misjudged from the opening scene and thereafter only occasionally hits the right note." Time film critic Richard Corliss said he did not understand what the Coen brothers were attempting with the film, and after describing the plot, wrote, "I have the sinking feeling Ive made Burn After Reading sound funnier than it is. The movies glacial affectlessness, its remove from all these subpar schemers, left me cold and perplexed."    Corliss complimented Richard Jenkins and J.K. Simmons for their brief supporting roles.  David Denby of The New Yorker said the movie had several funny scenes, but they "are stifled by a farce plot so bleak and unfunny that it freezes your responses after about forty-five minutes."    Denby also criticized the pattern of violence in the movie, in which innocent people die quickly and the guilty go unpunished. "These people dont mean much to  ; its hardly a surprise that they dont mean much to us, either. ... Even black comedy requires that the filmmakers love someone, and the mock cruelties in Burn After Reading come off as a case of terminal misanthropy." 

Leah Rozen, of People (magazine)|People magazine, said the characters "unrelenting dumbness and dim-witted behavior is at first amusing and enjoyable but eventually grows wearing."    But Rozen said the performances are a redeeming factor, especially that of Pitt, who she described as a standout who "manages simultaneously to be delightfully broad and smartly nuanced." 

Le Monde noticed its "particularly bitter image of the U.S. The alliance of political incompetence (the CIA), the cult of appearance (the gym club) and vulgar stupidity (everyone) is the target of a settling of scores" where the comedy "sprouts from a well of bitterness." 

==Accolades==
The film was nominated at the 2009  |accessdate=January 11, 2009|archiveurl=http://web.archive.org/web/20090227063722/http://www.metacritic.com/film/awards/2008/toptens.shtml|archivedate=
February 27, 2009}} 

===Box office===
In its opening weekend, the film grossed $19.1&nbsp;million in 2,651 theaters in the United States and Canada, ranking number one at the box office.  As of July, 2009, it has grossed $60.3&nbsp;million in the United States and Canada and $100.8&nbsp;million in other countries adding up to $161.1&nbsp;million worldwide gross. 

==Home media==
Burn After Reading was released on DVD and Blu-ray Disc on December 21, 2008, on Region 1. The Region 2 version was released on February 9, 2009.

==Notes==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*   at Working Title Films

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 