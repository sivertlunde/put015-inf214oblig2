A Happy Family
{{Infobox Hollywood cartoon|
| cartoon_name = A Happy Family
| series = Krazy Kat
| image = 
| caption = 
| director =
| story_artist = Ben Harrison
| animator = Manny Gould Allen Rose Harry Love
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| studio = 
| distributor = Columbia Pictures
| release_date = September 27, 1935
| color_process = Black and white
| runtime = 6:41 English
| preceded_by = Garden Gaities
| followed_by = Kannibal Kapers
}}

A Happy Family is a short animated film by Columbia Pictures, and part of the Krazy Kat series.

==Plot==
Krazy and his spaniel girlfriend are in a house looking at an album of Krazys relatives. Momentarily, Krazy receives a letter from an uncle of his who is coming to visit as well as bringing in some relatives. Several moments later, the uncles car arrives. Although the car is the size of a four-seater, hundreds of cats come out and fill in Krazys house. Finally, Krazys uncle shows up and courteously greets him.

At first, things appear okay at home when the older cats are just having a meal. But Krazy becomes dismayed when the kittens do some roughhousing. Krazy tries to intervene but the kittens troublesome antics keep him at bay.

Eventually Krazys home is pretty much ruined. Krazy is forced to take shelter in a dog house. It is unknown on what happened to the spaniel.

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 

 