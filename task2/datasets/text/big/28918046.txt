Goliath and the Barbarians
{{Infobox film
| name           = Goliath and the Barbarians
| image          = GoliathAndBarbarians-poster.jpg
| image_size     = 220
| caption        = Film poster by Reynold Brown
| director       = Carlo Campogalliani
| producer       = 
| writer         = 
| based on = 
| narrator       =
| starring       = 
| music          = Carlo Innocenzi
| cinematography =  	Bitto Albertini
| editing        = Franco Fraticelli
| studio = 
| distributor    = American International Pictures (USA)
| released       = 1959
| runtime        = 
| country        = Italy English
| budget         = 
| gross = $1.6 million (North America) 
| preceded by    =
| followed by    =
}} peplum loosely Lombard Lombards#Invasion invasion of Italian peplums English translation renames some of the characters (for example: "Emiliano" becomes "Goliath" in the English version).

==Plot==
Set in the 6th century, it follows the start of the Barbarian invasions and deals with one group that attacks a village and destroys anything that is there. One man, Emiliano, is left, and he swears revenge and wages a one man war against the evil tribes. He also is helped by the survivors and  Landa, the daughter of the tribal leader.

==Cast==
* Steve Reeves as "Emiliano"
* Chelo Alonso as "Landa"
* Bruce Cabot as "Alboin"
* Giulia Rubini as "Lidia"
* Arturo Dominici as "Svevo"
* Livio Lorenzon as "Igor"
* Andrea Checchi as "Delfo"

==US Release== Sign of the Gladiator "Rental Potentials of 1960", Variety, 4 January 1961 p 47. Please note figures are rentals as opposed to total gross. 

AIP announced plans to make a follow up called Goliath and the Dragon from a script by Lou Rusoff with Debra Paget but this fell through and they ended up buying an Italian film called The Vengeance of Hercules and simply renaming it Goliath and the Dragon. 

===DVD Releases=== Goliath and the Vampires.

==See also==
* list of historical drama films
* Late Antiquity

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 