Brown Sugar (1931 film)
{{Infobox film
| name           = Brown Sugar
| director       = Leslie S. Hiscott
| producer       = Julius Hagen
| writer         = Arthur Lever   Cyril Twyford
| starring       = Constance Carpenter   Francis Lister   Alan Anyesworth   Helen Haye
| music          =  Ernest Palmer
| editing        = 
| distributor    = Twickenham Film Studios
| released       =  
| runtime        = 70 minutes
| country        = United Kingdom
| language       = English
}}
Brown Sugar is a 1931 British romance film directed by Leslie S. Hiscott and starring Constance Carpenter, Francis Lister, Alan Aynesworth and Helen Haye.  It was largely filmed at Twickenham Studios in west London.

==Cast==
* Constance Carpenter as Lady Stella Sloane  
* Francis Lister as Lord Sloane  
* Alan Aynesworth as Lord Knightsbridge  
* Helen Haye as Lady Knightsbridge  
* Cecily Byrne as Lady Honoria Nesbitt  
* Eva Moore as Mrs. Cunningham  
* Chili Bouchier as Ninon de Veaux  
* Gerald Rawlinson as Archie Wentworth  
* Alfred Drayton as Edmondson  
* Wallace Geoffrey as Crawbie Carruthers  

==References==
 

== External links ==
*  

 
 
 
 
 
 
 

 