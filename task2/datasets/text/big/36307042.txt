Man of Tai Chi
 
 
{{Infobox film
| name           = Man of Tai Chi
| image          = Man of Tai Chi.jpg
| alt            = 
| caption        = China poster
| director       = Keanu Reeves
| producer       = Lemore Syvan Daxing Zhang
| writer         = Michael G. Cooney
| starring       = Keanu Reeves Tiger Chen Iko Uwais Karen Mok Simon Yam
| music          = Chan Kwong-wing
| cinematography = Elliot Davis
| editing        = Derek Hui Village Roadshow Pictures Asia Universal Studios
| distributor    = United States:   Australia/New Zealand/Singapore:  
| released       =  
| runtime        = 105 minutes
| country        = China United States
| language       = Mandarin Cantonese English 
| budget         = United States dollar|US$25 million
| gross          = US$5,421,632 
}}
Man of Tai Chi is a 2013 Chinese-American martial arts film starring Keanu Reeves and Tiger Chen.    The film is Reevess directorial debut. Man of Tai Chi is a multilingual narrative, partly inspired by the life of Reeves friend, stuntman Tiger Chen. The film also features Indonesian The Raid star and Pencak Silat martial artist, Iko Uwais.

==Plot==
Tiger Chen (Tiger Chen Linhu) is the sole student of his elderly masters Ling Kong Tai Chi style. Whilst Tiger excels in the physical aspects of his training, Master Yang struggles to instill in Tiger the philosophical aspects, and fears for his character. Tiger harbors a determined ambition to prove the martial effectiveness of the style, as he competes in the local Wulin (武林) contest. In between, HKPD officer Sun-Jing Shi (Karen Mok) has been leading an investigation into Security System Alliance (SSA), a private security firm owned by the mysterious Donaka Mark (Keanu Reeves). Suspected of hosting an illegal fighting operation, Sun-Jing turned one of Donakas fighters into a mole, but he was discovered and personally killed by Donaka. Despite the setbacks, Sun-Jing wants to continue investigating, but without any evidence, Superintendent Wong (Simon Yam) had the case closed. Believing the evidence would eventually come through, Sun-Jing secretly continued investigating Donaka and things would change with Tigers involvement.

Short of a fighter, Donaka searches for his next great fighter and took notice of Tiger during the Wulin Competition. Seeing Tai Chi as something different and noticed the innocence within Tigers eyes; he sent an invitation to Tiger for a job opportunity at SSA. Tired of his menial courier job, he took a chance and was flown over to Hong Kong for the job interview. However, once he arrived at the interview room, he was attacked and he subdued his opponent; it was a test of his combat ability. Donaka welcomed him and offered him great financial rewards for joining his underground fighting ring; Tiger refused as it would compromise his honour by fighting for money. However, Donaka had other means of persuasion.

The following day, investigators had arrived and had the temple declared structurally unsafe; Master Yang was to be evicted and the temple demolished for new real estate development. Unclear of the legal system, Tiger sought help from Qing Sha (Ye Qing), a paralegal friend and love interest to Tiger. With Qing-Shas help, she found a means to save the temple: historic preservation and government protection. However, even if the law prevails on their side, the temples dilapidated status required emergency repairs within a months time and a lot of money; Tiger relented to Donakas offer and fought to preserve the temple.

Tiger excelled in his combat challenges. With every battle, he won large sums of money, but also evolved and enhanced his combat abilities with every match. His path allowed him to repair the Ling Kong Temple, quit his courier job, lavish his parents with presents, quickly rise in the Wulin Competition, and he seemed to have a budding romantic relationship with Qing Sha. However, with every battle, the challenges became more brutal and darkened Tiger. Master Yang wasnt a fool and noticed a change in his combat abilities and warned him of the dangers of not changing his ways. Unfortunately, Tiger didnt see a problem with his situation and continued to enjoy life; little did he realized Donaka had corrupted him.

In a recent two against one fight, Tiger won the battle, but developed a ruthless attitude and brought that mentality into the Wulin Competition. Breaking the rules of friendly and honourable competition, he viciously injured his opponent, which resulted in him being disqualified from the competition. Master Yang witnessed what happened on TV and was shocked at Tigers ruthlessness. When Tiger came to train with Master Yang, they had an aggressive fight rather than a respectful spar. Despite his advanced age, Master Yang held his own and was forced to use his internal chi energies to palm-strike Tiger, repelling him away. Still loving his student, he continued to tell Tiger to return to the path of peace, but Tiger rejected his masters good intentions and left him saddened. Things worsened when he found out his petition to gain historical protection for his temple was rejected. A disappointed Qing-Sha revealed Tigers vicious actions from the Wulin Competition swayed her directors decision. Enraged, he demanded the director to explain herself and she reasoned that his actions dont reflect the claimed honour and nobility of his temples philosophy; she favoured new real estate development instead.

Still raging from what happened, Tiger demanded a fight and Donaka had Tiger face the killer mercenary, Uri Romanov. Using his rage, he quickly defeated Uri and could have killed him, but he refused; Donaka appeared in a black mask to finish off Uri. Tiger had had enough of underworld combat and wanted to leave, but Donaka laughed at Tiger thinking he had a choice of leaving. Realizing hes trapped, Tiger decided to reach out to the police. However, unknown to Tiger, Donaka had every aspect of Tigers life recorded and Gong (Donakas camera man) reported on Tigers plan to defect; Donaka had a contingency plan.

Now "secretly" working with the police, Donaka set up a private tournament for a death match. Sun-Jing secretly trailed Tigers escort to reach the secret tournament, but her car was violently knocked off the road by a garbage truck. Sun-Jing survived her attack, contacted her squad for assistance and discovered Superintendent Wong commanded communications to prevent anyone from investigating particular frequencies that may lead to Donaka; Wong works for Donaka. Several hours before the tournament, a brief video revealed the private life of Tiger, shamefully revealing how exposed his life is and how Donaka manipulated him into underground combat, corrupting him as a person. Enraged, Tiger tried to attack Donaka, but was subdued by a taser. It was then Donaka revealed his vision for Tiger: exposing his life as he slowly corrupted and made Tiger a killer before a live audience.

In the death match, Tiger was to face Gilang Sanjaya (Iko Uwais); now fully awakened to his reality, Tiger refused to fight, but challenged Donaka for a match instead. The match was over after HKPD stormed the compound and arrested everyone, including Wong; Donaka escaped capture. Back at home, Tiger was visiting the temple when he came across Donaka. They had their own private death match as Donaka declared that Tiger owed him a life and told Tiger to show him his true nature. Tiger was losing at first, but he re-embraced his Tai Chi training but it still wasnt enough to gain an upper hand. However, Donaka forced Tigers hand as he pulled out a knife and stabbed Tiger in the stomach. It was then that Tiger used his internal powers to repel Donaka. Suffering from internal damage, Donaka told Tiger he was pleased to know Tiger did have it in him to be a killer after all, and then Donaka died on the temple grounds from the injuries inflicted by Tiger. Ironically, after long hand to hand combat, Tiger won by applying short distanced technique.

Some time later, Tiger repaired his relationship with his master and Sun-Jing has been promoted to Superintendent. Tiger reached out to both Qing Shas law firm and the real estate developers and all three parties reached a compromised agreement: the temple will be part of a historical cultural village. Not only will the government protect the village, but now tourists can come and visit, to learn more about the 600 year history of the Ling Kong Temple. Finally putting that chapter of his life away, Tiger tells Qing-Sha that he intends to return to the city and open his own Tai Chi school, to continue on the legacy of the Ling Kong Tai Chi.

==Cast==
* Tiger Chen as Tiger Chen Linhu
* Keanu Reeves as Donaka Mark
* Karen Mok as Sun Jing Shi
* Simon Yam as Superintendent Wong
* Ye Qing as Qing Sha
* Yu Hai as Master Yang Sam Lee as Tak Ming
* Michael Tong as Policeman Yuan
* Iko Uwais as Gilang Sanjaya
* Silvio Simac as Uri Romanov

==Production==
 
Pre-production began in 2008 with years-long script refinements. During the films five years of scripting and production, Reeves acted in several B movies with lead roles as Henry in 2010s Henrys Crime and John in 2012s Generation Um.... When the project eventually moved into the production phase, principal photography occurred on mainland China and Hong Kong.

The film originally planned to use a camera rig dubbed the "Cam-Fu". Due to technical limitations, handheld cameras were used to film the fight scenes.

==Release== premiered in Cannes Film Festival.  It was also scheduled to be shown at the 2013 Toronto International Film Festival.    It became available for purchase on 27 September  via the iTunes Store (VOD) video on demand, and had its theatrical release in the US on November 1. 

==Reception==
 
The work was awarded  in Beijing and praised by recognized action film director John Woo. It has a rating of 70% on Rotten Tomatoes. 

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 