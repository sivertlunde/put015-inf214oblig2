Open Tee Bioscope
{{Infobox film
| name           = Open Tee Bioscope
| image          = Open tee bioscope poster.jpg
| alt            =  
| caption        = Poster
| director       = Anindya Chatterjee
| producer       = Shoojit Sircar
| writer         = Anindya Chatterjee
| starring       = Rajatava Dutta  Kaushik Sen  Paran Bandopadhyay   Sudipta Chakraborty   Aparajita Auddy   sohini sarkar  Biswanath Basu
| music          = Upal Sengupta
| studio         =
| distributor    =
| released       =  
| runtime        = 2:17
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}
 Bengali film released on January 15, 2015, directed by director Anindya Chatterjee, who is the well known vocalist of the band Chandrabindoo (band)|Chandrabindu.  The film is a coming of age story of an adolescent boy and his friends. 

==Cast==
* Riddhi Sen as Fowara 
* Rwitobroto Mukherjee as Kochua 
* Dhee Majumder as Charan 
* Rajarshi Nag as Gopa 
* Surangana Bandopadhyay as Titir 
* Rajatava Dutta  as Gopeshwar 
* Sudipta Chakraborty as Fowaras mother, Boishakhi
* Kaushik Sen  as Mahim Halder
* Paran Bandopadhyay as Noton Da
* Sohini Sarkar as Iraboti 
* Ambarish Bhattacharya as Herombo
* Aparajita Auddy as Gopeshwars wife
* Biswanath Basu as Pulish
* Ritwick Chakraborty as grown-up Fowara          

==Plot==

The film steers through comedy, drama, and emotions of the middle class community of 1990s Kolkata, and carefully captures the essence of North Kolkata, and the quintessential spirit of North Kolkata’s para football. There are some funny dialogues in this movie you will love to hear it. Though It will be difficult to understand the actual meaning for you if you cannot get the sarcasm.
e.g.:  "-Whats your name?(Tor naam ki?)"   -"Fowara(Water Fountain)"   -"Jol pore?(Does water stream down from it?)"   Director has depicted very well in this manner. You may recall your childhood days after watching this movie.

==Music==
The soundtrack is composed by Upal Sengupta. The song "Bondhu chol" and the Cycle Theme are composed by guest music director Shantanu Moitra.

{{Track listing
| headline        = Tracklist
| extra_column    = Singer(s)
| total_length    = 
| all_music       = 
| all_lyrics      = 
| title1          = Le Le Babu 
| extra1          = Upal Sengupta
| length1         = 
| title2          = Tor Jonnyo
| extra2          = Prosen and Mou
| length2         = 
| title3          = Pagla Khabi Ki  
| extra3          = Prosen
| length3         = 
| title4          = Hey Shokha  
| extra4          = Surangana 
| length4         = 
| title5          =   Maa  
| extra5          = Upal Sengupta
| length5         = 
| title6          = Bondhu Chol
| extra6          = Anupam Roy 
| length6         = 
| title7          = Cycle Theme 
| extra7          = Anindya Chatterjee
| length7         = 
}}

==References==
 

==External links==
*  

 
 
 
 

 
.....