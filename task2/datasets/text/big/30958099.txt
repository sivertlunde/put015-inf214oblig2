Real Pests
{{Infobox film
| name           = Real Pests
| image          = 
| image_size     = 
| caption        = 
| director       = Jože Bevc
| producer       = 
| writer         = Jože Bevc
| starring       = Bert Sotlar Dare Valič Majda Potokar Jože Horvat Milada Kalezič  Boris Cavazza Radko Polič Andrej Prevc Bogdan Sajovic
| music          = Janez Gregorc
| cinematography = Ivan Marinček
| editing        = Darinka Peršin
| distributor    = Viba film
| released       = 1977
| runtime        = 96 minutes
| country        = Yugoslavia
| language       = Slovene
| budget         = 
}}

Real Pests ( ) is a 1977 Slovenian comedy film by the writer and director Jože Bevc, starring  Bert Sotlar, Dare Valič and Majda Potokar.  The film was one of the most successful films in Slovenia, holding the domestic box office record for almost 15 years. 

== Plot ==
Real Pests is a light-hearted comedy about a widower Štebe (Bert Sotlar), who works as a bus driver and lives with his five adolescent sons and an elderly maid Rozi (Majda Potokar) in  . Everybody boards her bus and they drive off.

== Cast ==
* Bert Sotlar - father Štebe
* Dare Valič - Bine
* Majda Potokar - Rozi
* Jože Horvat - Brane
* Milada Kalezič - Meri
* Boris Cavazza - Toni
* Radko Polič - Boris
* Andrej Prevc - Janez
* Bogdan Sajovic - Muki

== References ==
 

== External links ==
*  

 
 
 
 


 