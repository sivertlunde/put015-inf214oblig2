Carnival (1921 film)
{{Infobox film
| name           = Carnival
| image          =
| caption        =
| director       = Harley Knoles 
| producer       =  Adrian Johnson
| starring       = Matheson Lang Ivor Novello Hilda Bayley   Clifford Grey
| music          =
| cinematography = Philip Hatkin
| editing        = 
| studio         = Alliance Film Corporation
| distributor    = Alliance Film Corporation
| released       = 26 June 1921 (US) 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         = 
| preceded_by    =
| followed_by    =
}} British silent silent drama film directed by Harley Knoles and starring Matheson Lang, Ivor Novello and Hilda Bayley.  During a production of Shakespeares Othello in Venice an Italian actor suspects his wife of having an affair and plans to murder her on stage. It was based on a stage play of the year before of which Matheson Lang was one of the writers.  The film was a popular success, and was re-released the following year. It was remade as a sound film Carnival (1931 film)|Carnival in 1931 directed by Herbert Wilcox.

==Cast==
* Matheson Lang - Sylvio Steno
* Ivor Novello - Count Andrea
* Hilda Bayley - Simonetta, Silvios wife
* Clifford Grey - Lelio, Simonettas brother
* Victor McLaglen - Baron
* Florence Hunter - Nino, Silvios son
* Maria de Bernaldo - Ottavia, Silvios sister

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 
 


 