Our Grand Despair
{{Infobox film
| name           = Our Grand Despair
| image          = Bizim Büyük Çaresizliğimiz (2011 film) poster art.jpg
| caption        = 
| director       = Seyfi Teoman
| producer       = Yamac Okur Nadir Operli
| writer         = Seyfi Teoman Baris Bicakci
| starring       = Ilker Aksum
| music          = 
| cinematography = Birgit Gudjonsdottir
| editing        = Cicek Kahraman
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
}}

Our Grand Despair ( ) is a 2011 Turkish drama film, directed by Seyfi Teoman, about two flatmates who reluctantly take in their friends traumatised sister.  The film premiered in competition at the 61st Berlin International Film Festival.      

==Cast==
* Ilker Aksum as Ender
* Fatih Al as Cetin
* Gunes Sayin as Nihal
* Taner Birsel as Murat - Cetins brother
* Baki Davrak as Fikret - Nihals brother
* Mehmet Ali Nuroglu as Bora - Nihals boyfriend

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 