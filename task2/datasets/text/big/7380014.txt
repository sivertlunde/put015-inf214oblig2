The Naked Maja
:For the painting by Francisco de Goya whose title is often translated as "The Naked Maja", see  .
{{Infobox film
| name           =  The Naked Maja
| image          = Naked Maja.jpeg
| image_size     = 225px
| caption        = Theatrical Film Poster
| director       = Henry Koster
| producer       = Silvio Clementelli  Goffredo Lombardo
| writer         = Norman Corwin  Giorgio Prosperi   Albert Lewin  Oscar Saul (story)   Talbot Jennings (story)
| starring       = Ava Gardner     Anthony Franciosa     Amedeo Nazzari    Gino Cervi    Lea Padovani    Massimo Serato
| music          = Angelo Lavagnino
| cinematography = Giuseppe Rotunno
| editing        = Mario Serandrei
| distributor    = United Artists MGM (foreign)
| released       =  
| runtime        = 111 minutes
| country        = United States Italy France
| awards         = English
| budget         = $913,000  .  gross           = $1 million (outside US/Canada) 
}} American co-production historical biographical film recounting Duchess of Alba was directed by Henry Koster, and produced by Silvio Clementelli and Goffredo Lombardo. The screenplay was by Norman Corwin, Giorgio Prosperi and Albert Lewin based on a story by Oscar Saul and Talbot Jennings. The music score was by Angelo Lavagnino and the cinematography by Giuseppe Rotunno.

The film stars Ava Gardner and Anthony Franciosa with Amedeo Nazzari, Gino Cervi, Lea Padovani, Massimo Serato and a largely Italian cast. It is based on a novel by Noel Gerson
==Box Office==
According to MGM records the film earned $1 million at the box office outside the US and Canada resulting in a loss to the studio of $513,000. 
==References==
 
==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 