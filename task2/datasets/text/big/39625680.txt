In for Treatment
{{Infobox film
| name           = In for Treatment
| image          = 
| caption        = 
| director       = Marja Kok Erik van Zuylen
| producer       = 
| writer         = 
| starring       = Joop Admiraal
| music          = 
| cinematography = Robby Müller
| editing        = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}}
 Best Foreign Language Film at the 53rd Academy Awards, but was not accepted as a nominee. 

==Cast==
* Joop Admiraal as De Bruin
* Frank Groothof as Frank
* Marja Kok as Mrs. De Waal
* Hans Man in t Veld as Dr. Hageman
* Daria Mohr as Anja Vonk
* Shireen Strooker as nurse
* Gerard Thoolen as male nurse
* Herman Vinck as Franks father
* Helmert Woudenberg as Mr. De Waal
* Olga Zuiderhoek as nurse

==See also==
* List of submissions to the 53rd Academy Awards for Best Foreign Language Film
* List of Dutch submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 