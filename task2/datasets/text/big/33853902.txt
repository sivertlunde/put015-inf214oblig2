Rakhwale
{{Infobox film
 | name = Rakhwale
 | image = Rakhwale94.jpg
 | caption = DVD Cover
 | director = Sudharshan Lal
 | producer = Kishan Meharchandani
 | writer = 
 | dialogue = 
 | starring = Aatish Devgan Yogeeta Singh Harsha Kader Khan Mukesh Khanna Alok Nath
 | music = Sonik Omi
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = September 09, 1994
 | runtime = 145 min.
 | language = Hindi
 | country = India Rs 3 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1994 Hindi Indian feature directed by Sudharshan Lal, starring Aatish Devgan, Yogeeta Singh and Harsha. The film has a galaxy of stars Dharmendra, Mithun Chakraborty, Govinda and Raj Babbar in special appearances.

==Plot==
Rakhwale is an action/ revenge film.

==Cast==
*Dharmendra
*Mithun Chakraborty
*Govinda
*Raj Babbar
*Kader Khan
*Mukesh Khanna
*Alok Nath
*Dara Singh
*Raza Murad
*Sudhir Pandey
*Firoz Irani
*Yogeeta Singh
*Aatish Devgan
*Harsha

==References==
*http://www.induna.com/1000002672-productdetails/
*http://ibosnetwork.com/asp/filmbodetails.asp?id=Rakhwale

 
 
 

 