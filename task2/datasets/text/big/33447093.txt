Mrs Peppercorn's Magical Reading Room
{{Infobox film
| name           = Mrs Peppercorns Magical Reading Room
| image          = PeppercornPoster.jpg
| alt            =  
| caption        = 
| director       = Mike Le Han
| producer       = Mike Le Han
| writer         = Helen Le Han and Mike Le Han
| narrator       = Penny Ryder
| starring       = Jane Cox, Stephen Boyes, Margaret Jackman, Kerry Downing, Kris Sleater and introducing Emily Coggin
| music          = Kevin Kliesch
| cinematography = Stephen Murphy (DOP)
| editing        = Mark Talbot-Butler
| studio         = 
| distributor    = 
| released       =   
| runtime        = 24 Minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Mrs Peppercorns Magical Reading Room is a 24 minute Fantasy/Magical short film directed by Mike Le Han. In November 2010 the official trailer was released and entered into the International Movie Trailer Festival and won Best Trailer for an Un-Produced Movie. In March 2011 the film premiered at Bafta       in London, then in August of the same year was requested by Disney Animation Studios to screen at their Burbank facility in Los Angeles. Three days later Mrs Peppercorn won grand prize of Best Short Film at the prestigious HollyShorts International Film Festival.   In December 2011 Mrs Peppercorn won Best Short Fiction Film at the Olypmia International Film Festival in Greece.

The short film was created as a promo for Le Hans up and coming feature trilogy titled PEPPERCORN.

==Awards==

{| class="wikitable"
|- bgcolor="#CCCCCC"
! Year !! Festival !! Title !! Award !! Result
|-
| 2010  
| International Movie Trailer Festival
| Trailer for Mrs Peppercorn
| Best trailer for an un-produced movie 
| Winner
|-
| 2011
| HollyShorts Film Festival
| Mrs Peppercorns Magical Reading Room
| Best short film
| Winner
|-
| 2011
| Olympia International Film Festival - Greece
| Mrs Peppercorns Magical Reading Room
| Best Short Fiction Film - Childrens Jury Award
| Winner
|}

==References==
 

==External links==
*  
*  

 
 
 
 