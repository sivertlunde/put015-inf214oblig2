The Chinese Ring
{{Infobox film
| name           = The Chinese Ring
| image          = Poster of the movie The Chinese Ring.jpg
| image_size     = 190px
| caption        = 
| director       = William Beaudine
| producer       = James S. Burkett
| writer         = Earl Derr Biggers (character) Scott Darling (screenplay)
| narrator       = 
| starring       = 
| music          = Edward J. Kay
| cinematography = William A. Sickner
| editing        = Richard V. Heermance Ace Herman
| distributor    = 
| studio         = Monogram Pictures
| released       =  
| runtime        = 68 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 
The Chinese Ring is a 1947 American film directed by William Beaudine.

The film is also known as Charlie Chan in the Chinese Ring (American poster title) and The Red Hornet.  This is the first to feature Roland Winters as Charlie Chan. It is a remake of the 1939 Monogram Pictures film Mr. Wong in Chinatown with an identical story line and very little alterations of secondary characters.

==Plot==
Charlie Chan (Roland Winters) is a private investigator living in San Francisco. One day a Chinese princess comes to visit him in his home. She has just arrived from the East by boat. The princess manages to give Chan’s butler Birmingham Brown (Mantan Moreland) an artifact - an ancient heirloom ring - before she is shot an killed by a poisoned arrow through the window. She leaves a note behind, with the name ”Captain K”. Chan calls for the police to investigate this murder. Bill Davidson (Warren Douglas) with the SFPD comes to Chan, but his friend, the reporter Peggy Cartwright (Louise Currie), also arrives uninvited to the scene. 
By examining the ring, it turns out the princess’ name was Mei Ling (Barbara Jean Wong). She arrived in San Francisco weeks earlier with two men, Captain Kong (Philip Ahn) and Captain Kelso (Thayer Roberts), to try and acquire fighter planes to fight off an enemy back home. For this purpose she had brought a substantial amount of money, $1,000,000, with her on the journey. A search for the money ensues, but it soon turns out there are more people looking for the lost money. Peggy helps out in the hunt, and she meets the princess’ maid, Lillie Mae (Chabing), and a deaf-mute boy living in the princess apartment. When Chan comes to talk to the maid, he finds her murdered in the apartment, and the deaf-mute boy manages to tell him that a man came to visit the apartment shortly before the maid was found dead. 
Chan continues his search for the money, visiting the banker in charge of handling the princess’ assets abroad, Armstrong (Byron Foulger). While they are talking, Kong and Kelso, eager to get their share of the money, break in and kidnap both men and hold them for ransom. They are taken to the Chinese cargo vessel, but Birmingham manages to track them to the ship. Together with Chan’s son Tommy (Victor Sen Yung), they manage to free Chan and Armstrong, and when Bill and Peggy arrive to the ship with the police, Kong and Kelso are captured and arrested. Chan discovers that Armstrong is guilty of the princess’ murder, and of stealing her money. Armstrong also killed the maid to cover his tracks. 

== Cast ==
*Roland Winters as Charlie Chan
*Victor Sen Yung as Tommy Chan
*Warren Douglas as Police Sgt. Bill Davidson
*Mantan Moreland as Birmingham Brown
*Louise Currie as Peggy Cartwright
*Philip Ahn as Captain Kong
*Byron Foulger as Armstrong
*Thayer Roberts as Captain James J. Kelso
*Barbara Jean Wong as Princess Mei Ling
*Chabing as Lillie Mae Wong
*George Spaulding as Dr. Hickey

== External links ==
* 
* 

==References==
 

 
 

 
 
 
 
 
 
 
 
 
 
 