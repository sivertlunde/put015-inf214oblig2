The Walker
 
{{Infobox film
| name           = The Walker
| image          = The Walker poster.jpg
| caption        = Theatrical poster
| director       = Paul Schrader
| producer       = Deepak Nayar
| writer         = Paul Schrader
| narrator       = 
| starring       = Woody Harrelson Kristin Scott Thomas Lauren Bacall Ned Beatty Lily Tomlin Willem Dafoe Moritz Bleibtreu  Mary Beth Hurt
| music          = Anne Dudley
| cinematography = Chris Seager
| editing        = Julian Rodd
| distributor    = Kintop Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States United Kingdom
| language       = English
| budget         = 
| gross          = 
}} independent production and is the latest installment in Schraders night workers series of films, starting with Taxi Driver in 1976, followed by American Gigolo in 1980 and Light Sleeper in 1992.

== Synopsis ==
Carter Page III (Harrelson), a middle-aged gay man in Washington, D. C., is a "male escort|walker", a single man who escorts other mens wives to social events so the husbands do not have to. One of the women he escorts, Lynn Lockner (Scott Thomas), is the wife of a United States senator and is carrying on an affair with a lobbyist. When she finds the lobbyist murdered, she embroils Carter in an investigation that leads to the highest levels of the federal government.

== Cast ==
* Woody Harrelson as Carter Page III
* Kristin Scott Thomas as Lynn Lockner
* Lauren Bacall as Natalie Van Miter
* Ned Beatty as Jack Delorean
* Moritz Bleibtreu as Emek Yoglu
* Mary Beth Hurt as Chrissie Morgan
* Lily Tomlin as Abigail Delorean
* Willem Dafoe as Senator Larry Lockner William Hope as Mungo Tenant

== Production ==
Schrader completed the script for The Walker in 2002. Initially the film was to be a direct sequel to American Gigolo, with Julian Kaye (played by Richard Gere) as the lead character. The director originally wanted Kevin Kline to play the lead. {{cite news 
  | title = The American gigolo comes out
  | work = The Advocate
  | page = 20
  | date = 2002-09-17
  | url = http://www.accessmylibrary.com/coms2/summary_0286-25961515_ITM
  | accessdate = 2009-05-20}} 

== Critical reception == Cambridge film festivals. The Walker was released direct-to-DVD but played in an independent film theater for two weeks in Dorris, California. The film received mixed reviews from critics. As of December 7, 2007, the review aggregator Rotten Tomatoes reported that 51 percent of critics gave the film positive reviews, based on 39 reviews.  On Metacritic, the film had an average score of 55 out of 100, based on seven reviews. 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 