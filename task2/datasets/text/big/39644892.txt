The Land of Smiles (1930 film)
{{Infobox film
| name           = The Land of Smiles 
| image          = 
| image_size     = 
| caption        = 
| director       = Max Reichmann 
| producer       =
| writer         = Ludwig Herzer (libretto)   Fritz Löhner-Beda (libretto)   Viktor Léon   Anton Kuh   Hans Jacoby   Curt J. Braun   Léo Lasko
| narrator       = 
| starring       = Richard Tauber   Mara Loseff   Hans Mierendorff   Bruno Kastner
| music          = Franz Lehár (operetta)   Paul Dessau
| editing        = Geza Pollatschik
| cinematography = Reimar Kuntze 
| studio         = Richard Tauber Tonfilm
| distributor    = Bavaria Film 
| released       = 17 November 1930
| runtime        = 102 minutes
| country        = Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German operetta film directed by Max Reichmann and starring Richard Tauber, Mara Loseff and Hans Mierendorff. It is an adaptation of the operetta The Land of Smiles composed by Franz Lehár.  Lehár himself appeared in the film in a small role.
 The Land of Smiles starring Jan Kiepura.

==Cast==
* Richard Tauber as Ein exotischer Fürst / Prinz Sou Chong in der Operette 
* Mara Loseff as Liesa  
* Hans Mierendorff as Der Gesandte - ihr Vater 
* Bruno Kastner as Gustl 
* Karl Platen as Eine alte Exzellenz / ein alter Chinese in der Operette 
* Margit Suchy as Liesa in der Operette 
* Hella Kürty as Mi in der Operette 
* Willy Stettner as Gustl in der Operette 
* Max Schreck as Der Hundertjährige in der Operette 
* Georg John as Tschang in der Operette 
* Franz Lehár as Der Kapellmeister

==References==
 

==Bibliography==
* Traubner, Richard. Operetta: A Theatrical History. Routledge, 2003.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 