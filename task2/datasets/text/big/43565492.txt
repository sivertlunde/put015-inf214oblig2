Superstition (1982 film)
{{Infobox film
| name           = Superstition
| image          = Superstition 1982 poster.jpg
| caption        = Theatrical poster
| director       =  James W. Roberson
| producer       = Ed Carlin
| writer         = Galen Thompson
| narrator       =
| starring       = James Houghton Albert Salmi Lynn Carlin
| music          = David Gibney
| cinematography = Leon Blank
| editing        = Al Rabinowitz
| distributor    = Almi Pictures Carolco Pictures StudioCanal
| released       =  
| runtime        =  85 minutes
| country        = United States Canada
| language       = English
| budget         = 
| gross          =
}}

Superstition (also known as The Witch)  is a 1982 Canadian-American horror film directed by James W. Roberson and starring James Houghton, Albert Salmi, and Lynn Carlin. The plot follows a family who move into a house that was one the site of a witchs execution.

==Plot==
Two young men are murdered in an abandoned house after playing a prank on a young couple outside in their car.

Inspector Sturgess pays a visit to Reverend David Thompson, a new clergyman assigned to the parish who is taking over for an elderly minister named Reverend Maier. Sturgess complains about the house, which is on property owned by the church. The two boys murdered there have brought the safety concerns of the house to the forefront, as well as the small body of water on the property, known as Black Pond. Sturgis says local teens sometimes go there to skinny dip at midnight, and there have been several drownings. He and Maier go to the house and meet Arlen, the animalistic caretaker of the property grounds, who lives in a small bungalow with his aging mother. The police suspect Arlen of the two murders but are unable to establish a case against him. Sturgess tells his partner, Hollister, to follow Arlen, who goes off running in the direction of the pond. Hollister investigates the dock where Arlen was standing, and is suddenly pulled under the water by a clawed hand that emerges from the pond. When Hollister does not return, Sturgess holds Arlen and has the pond dragged. Nobody is able to turn up his body, and Thompson announces hes going to have the pond completely drained. Arlen reacts violently, breaking free from the police and running off into the woods. Maier and Thompson talk to his mother, Elvira, who tells them her son serves a secret "mistress", something the two men write off as fantasy. Back at the house, Thompson meets a friendly young girl named Mary who seems to appear out of nowhere and vanishes just the same way. Before the ministers can bless the house, a freak accident occurs when the blade of a table saw being used suddenly flies off the machine and embeds itself into Rev. Maier, maintaining its rotation until it has sawed completely through his body.

Reverend George, his wife Melinda, and their three adolescent children: Ann, Sheryl, and Justin, move into the house. On the day of their move-in, one of the renovators is killed by the black-clawed figure when he goes off into the basement alone to turn off the power in the houses old elevator. His disappearance goes unnoticed by the rest of the crew, and they leave without him. Justin Leahy, the youngest of Reverend Leahys three children, hears the commotion in the basement and investigates, when suddenly the little girl Mary appears and introduces herself. She runs off before Justin can find out who she is. Justin excitedly tells the others that they have their own "pool"...the pond. They all go down to the pond to relax, where Ann jumps in to swim, but she suddenly starts screaming. When they drag her out of the pond, she has a severed human hand clamped around her ankle, presumably that of Detective Hollister. As Ann lays in her bed, languishing from shock, Melinda tells George they simply cannot move out of the house.

Sturges still thinks Arlen is responsible for the murders, but Elvira insists that Arlen is just protecting his "mistress", cleaning up her leavings and guarding them against her. She is dismissed as insane. Meanwhile, after Justin finds George drinking alcohol in the basement of the house, he returns there on his own and is killed by the witch when he discovers the body she left in the elevator shaft. A search is mounted for him, but Sturgess tells the rest of the Leahy family to stay out of it and leave it to him and his men, along with Thompson. Ann has a nightmare of the family being slaughtered. 

Elvira tells Thompson that the antique crucifix he has recovered from the pond was keeping the witch dormant until nightfall. Now that the cross has been removed, she can move about in the early hours of the morning. Thompson remains unconvinced, and is fixated on finding Arlen. Later, Thompson discovers a book in the church archives documenting events of the Inquisition. It seems a woman was executed for being a witch in that location in 1692. Elondra Sharack is accused of witchcraft by a nine-year-old girl named Mary. As Elondra is shackled on a plank, she affirms Satan as her master and insists she will take revenge on Reverend Pike, who condemns her to death by drowning. The moment they drown her, the church erupts in flames and her laughter is heard everywhere. The flashback establishes that Elondra continued to haunt the area after her death, resulting in numerous deaths and disappearances. Left alone is Reverend Pike, whom she murders by crushing him in a wine press.

Sturgess and his men discover a secret room off the houses basement, where Arlen has been hiding. They also find the body of the elevator repairman. The police take Arlen away in a squad car, while Sturgess stays behind to investigate the secret room, where he is killed by the witch. Melinda gets restless with waiting for some news about Justin, and she ventures downstairs. The witch emerges from the basement, trapping Melinda in the kitchen and hurling her around the room until she is bloodied and beaten. The girls are terrified by her screams and George attempts to intervene. Thompson returns, armed with the knowledge of the witch and still carrying the crucifix, which seems to undo her evil magic momentarily by allowing him to enter the supernaturally locked doors of the house. They are able to enter the kitchen, but Melinda is nowhere to be found. Thompson tells George to go back upstairs, where he barricades himself in his bedroom and discovers Melindas dead body in their bed. The witch appears in his window and causes his death when a mirror explodes and slashes his face and throat. Thompson tries to save him but is too late.

He attempts to get the girls out of the house but Sheryl runs off on her own into the attic, where the witch attacks her and gruesomely murders her by nailing her head to the floor with a large spike. Thompson sends Ann running from the house and gives her the keys to his van, telling her to save herself. When he goes into the attic to rescue Sheryl, he finds her body and is attacked by the witch. The crucifix keeps her from touching him, and before she can kill him, she vanishes. Thompson goes outside and realizes the van is still there, and he discovers Anns dead body in the van. Despondent, he takes cans of gasoline down to the pond and purifies it with fire. Mary appears to him and reveals herself to be simply another form of the witch; Thompson drives the crucifix through her heart and she falls backwards into the pond. The disturbance seems over, but before Thompson can leave, Elondras hand erupts from the pond and pulls him to his death under the water.

==Cast==
*James Houghton as Reverend David Thompson
*Albert Salmi as Inspector Sturgess
*Lynn Carlin as Melinda Leahy
*Larry Pennell as George Leahy
*Jacquelyn Hyde as Elvira Sharack
*Robert Symonds as Pike
*Heidi Bohay as Ann Leahy
*Maylo McCaslin as Sheryl Leahy
*Carole Goldman as Elondra Stacy Keach Sr. as Reverend Maier
*Kim Marie as Mary
*Billy Jayne as Justin Leahy
*Johnny Doran as Charlie
*Bennett Liss as Arty
*Joshua Cadman as Arlen

==Production== Silver Lake section of Los Angeles, California.  The historic Garbutt House, a twenty-room mansion overlooking the lake, was used as the primary filming location.

==References==
 

 
 
 
 