Afghan Knights
{{Infobox film
| name           =Afghan Knights 
| image          = 
| caption        = 
| screenplay     = Brendan K. Hogan Christine Stringer
| based on       = 
| starring       = Steve Bacic Michael Madsen Colin Lawrence Steven Cree Molison Chris Kramer Pete Antico Vince Murdocco Francesco Quinn Gary Stretch
| director       = Allan Harmon
| producer       = 
| cinematography = 
| music          = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  July 31, 2007
| runtime        = 132 minutes
| country        = USA/Can
| language       = English
| budget         = $1,500,000
| gross          = 
}}

 Afghan Knights is a 2007 war film/horror film based on an original story by Christine Stringer. Navy SEAL called Pepper (Steven Bacic) who suffers with a bad conscience because he has left a comrade behind in Afghanistan and eventually decides to go back in order to try to rescue the man after all. He assembles a bunch of soldiers and goes for it. {{Cite web|url= http://movies.yahoo.com/movie/1809425828/info
|title= Haunted by the fact that he left a man behind in Afghanistan, an ex Navy SEAL puts together a special task force to endure one last mission to save his comrade. 
|accessdate=2011-06-16}} 

In the course of action they come across a mysterious cave system and are confronted by hostile ghosts. {{Cite web|url= http://www.flixster.com/movie/afghan-knights
|title= U.S. military experts engage in a paranormal battle for survival while attempting to rescue a fellow soldier |accessdate=2011-06-16}}  This turns the war movie into a horror film. {{Cite web|url= http://www.trashcity.org/BLITZ/BLIT1801.HTM
|title= Then, however, the team are trapped in a cave system, and the film suddenly becomes a horror-film|accessdate=2011-06-16}} 

==References==
 

==External links==
*  
*  
*  
*  
*  
*

==See also==
*United States Navy SEALs in popular culture

 
 
 
 
 
 
 
 
 
 
 