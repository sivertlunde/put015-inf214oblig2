The Key (1961 film)
{{Infobox Film
| name = The Key
| image = Thekey2.jpg
| caption = The boy being hailed a hero after winning the golden key
| director = Lev Atamanov
| producer = 
| writer = Mikhail Volpin
| starring = Anna Komoleva Georgiy Vitsin V. Tumanova L. Glushchenko Rina Zelyonaya Sergey Martinson
| music = Lev Solin
| cinematography = 
| editing = 
| distributor = 
| released = 1961
| runtime = 57 minutes
| country = Soviet Union
| language = Russian
| budget = 
}}
The Key ( ;  . It was produced at the Soyuzmultfilm studio in Moscow.

The critic S. V. Asenin about the animated film: "The director L. Atamanov and the screenwriter M. Volpin used a fantastic form to mention (1961) burning questions of education in the animated film "Key". With original skill and a step they enclosed the new, modern contents in traditional fantastic images. What is the happiness as to reach it, what key its treasured door - this subject equally interesting to adults and children opens, they devoted the animated film."

==Plot==
The film is a cautionary tale about the need to develop good work habits, personal responsibility and conscience in life.

Three fairies visit the parents of a newborn boy and give them a red ball of string which they say will give him a happy life.  They also speed up his growth and tell the parents that he will grow by "hours, not days". The boys grandfather comes to visit and tries to give the boy working tools for later in life, but the gifts are refused by the parents and fairies, who tell him that the boy is already promised happiness and will have no need to work.  They magically turn the grandfather into a sheet of paper and send him off to his apartment.  The grandfather, not liking the idea of his grandson having ready-made happiness, breaks out of his magical cage and decides to seek help.

He visits a friend of his; a scientist who works in a large laboratory with a collection of amazing inventions such as human-like robots, including one which can compose poetry.  He asks the scientists robots what will become of a person who is born into ready-made happiness.  Most of them tell him that he will become disagreeable, lazy and arrogant, but the poetry-composing robot tells him that it would be terrible to take happiness away from a child. Unsure of what to do, the grandfather decides to visit the Land of Happiness and see for himself what it is like.  

The scientist synthesizes a copy of the magical red ball by using a few fragments that were left on the grandfathers hand after he held it while he was visiting his grandchild.  The grandfather uses the scientists teleportation machine to teleport to the gates of the Land of Happiness, and uses his red ball to gain entrance.  The many fairies who are in charge of this land lead him on a tour.  The ground is sticky with candy and the rivers run milk.  School consists of the "summer holidays", "fall holidays", "winter holidays" and "spring holidays".  At old age, the entire day consists of sitting on rocking chairs.

Disgusted by what he sees, the grandfather runs away, pursued by the fairies who want him to come back.  He manages to escape and runs back into the lab.  After telling the scientist what he saw, and that he doesnt want his grandchild to live in such a place, they decide to put a special lock on the gates of happiness.  They send one of the scientists small robots to trick the gatekeeper into letting him put it on.  The gatekeeper tries to open the lock but is unsuccessful.

Meanwhile, the grandchild, who grew up spoiled because of always getting his way, decides to leave his parents.  He takes the ball of happiness and arrives at the gates, whereupon the gatekeeper tells him that he cannot enter.  However, she tells him that if he goes to the "Kingdom of Quick Feats and Easy Victories" around the corner, and passes the tests, he will receive a golden key which will open any lock including, presumably, this one.  The boy goes to the kingdom, where hes met at the gates by a four-headed dragon.  While the four heads of the dragon try to reach consensus on whether or not to let him in, the boy sneaks in and fulfills all of the ridiculously easy tests, whereupon he is hailed a hero and given the golden key.  

When he comes back to the gate, he finds that the key doesnt work and begins to cry.  The small robot who put on the lock tells him that the lock can only be opened by a key which he makes with his own hands, so the boy goes to his grandfather to learn how to make a key.  

His grandfather teaches him how to properly use tools, and the boy slowly learns.  Finally, he creates his own key and leaves his grandfather, though reluctantly.  Meanwhile, the scientists poetry-composing robot decides to run away and show off his talent to the world.  Instead, he ends up drunk, and after coming back to the laboratory hits and maims the small robot which put the lock on the door to happiness.  The scientist takes the robot to the grandfathers apartment and asks him to fix her, but the grandfathers eyes are not good enough.  Just then, the grandchild comes back and says that he wants to stay with the grandfather.  Hes able to fix the little robot.

They conclude that happiness is "when a person knows much, has many skills, and gives it all away to others".  Upon realizing this, the child throws the red ball out the window.

==Creators==
{| class="wikitable"
|-
! !! English !! Russian
|-
| Director-producer
| Lev Atamanov
| Лев Атаманов
|-
| Scenario 
| Mikhail Volpin
| Михаил Вольпин
|-
| Art Directors
| Leonid Shvartsman Aleksandr Vinokurov
| Леонид Шварцман Александр Винокуров
|-
| Artists 
| Dmitriy Anpilov Valentin Karavayev
| Дмитрий Анпилов Валентин Караваев
|-
| Animators
| Fyodor Khitruk Renata Merenkova L. Popov Tatyana Taranovich Yelizaveta Komova Anatoliy Petrov Yelena Khludova Galina Zolotovskaya Konstantin Chikin Mariya Motruk H. Karavayeva Viktor Shevkov Lidiya Reztsova Marina Voskanyants Aleksandr Davydov Galina Barinova I. Beryozin Kirill Malyantovich Igor Podgorskiy
| Фёдор Хитрук Рената Миренкова Л. Попов Татьяна Таранович Елизавета Комова Анатолий Петров Елена Хлудова Галина Золотовская Константин Чикин Мария Мотрук Г. Караваева Виктор Шевков Лидия Резцова Марина Восканьянц Александр Давыдов Галина Баринова И. Берёзин Кирилл Малянтович Игорь Подгорский
|-
| Camera Operator
| Mikhail Druyan
| Михаил Друян
|-
| Executive Producer 
| Fyodor Ivanov
| Фёдор Иванов
|-
| Composer
| Lev Solin
| Лев Солин
|-
| Sound Operator 
| Nikolai Prilutskiy
| Николай Прилуцкий
|-
| Script Editor
| Raisa Frichinskaya
| Раиса Фричинская
|-
| Puppets & Decorations
| Dmitriy Anpilov Pyotr Korobayev
| Дмитрий Анпилов Пётр Коробаев
|-
| Voice Actors  Anna Komoleva Georgiy Vitsin (father) V. Tumanova L. Glushchenko Rina Zelyonaya (fairy Giatsinta) Tatyana Peltser(fairy Tyulpina) Georgiy Tusuzov (Zmey Gorynychs head) Gennagiy Dudnik Sergey Martinson Vera Orlova (mother) Vladimir Gribkov Yelena Ponsova (fairy guardian) Vladimir Lepko (Zmey Gorynychs Head) Larisa Pashkova
|Анна Комолова Георгий Вицин (отец) В. Туманова Л. Глущенко Рина Зелёная (фея Гиацинта) Татьяна Пельтцер (фея Тюльпина) Георгий Тусузов (голова Змея Горыныча) Геннадий Дудник Сергей Мартинсон Вера Орлова (мама) Владимир Грибков Елена Понсова (Фея-сторожиха) Владимир Лепко (Голова Змея Горыныча) Лариса Пашкова
|}

==Video==
This animated film was released on VHS by the Krupnyy Plan and Soyuz Video companies in the 1990th years in PAL system.

This animated film was released on DVD by the Krupnyy Plan company. Digital restoration of the image and a sound wasnt used. Except it, on a disk the animated film "It Was I Who Drew the Little Man" also contained.

*Sound — the Russian Dolby Digital 1.0 Mono;
*Regional code — 0 (All);
*The image — Standart   (1,33:1);
*Color — PAL;
*Packing — the Collection edition;
*The distributor — "Krupnyy Plan".

==See also==
* History of Russian animation
* List of animated feature-length films

==External links==
*  at Animator.ru (English and Russian)
* 
*   
 
 
 
 
 
 
 