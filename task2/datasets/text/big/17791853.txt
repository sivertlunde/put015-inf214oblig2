Trouble with Sex
{{Infobox film
| name           = Trouble with Sex
| image          = Trouble with Sex DVD cover.jpg
| caption        = Australian DVD cover
| director       = Fintan Connolly
| producer       = Fiona Bergin
| writer         = Fintan Connolly Catriona McGowan
| narrator       =  Eamon Morrissey
| music          = Niall Byrne
| cinematography = Owen McPolin
| editing        = Sarah Armstrong Ray Roantree
| studio         = A Bord Scannán na hÉireann / Irish Film Board Presentation A Fubar Film
| distributor    = Eclipse Pictures
| released       =  
| runtime        = 89 minutes
| country        = Ireland
| language       = English
| budget         =  
| preceded by    = 
| followed by    = 
}} Irish drama Irish Film and Television Award for Best Actress in a Leading Role for her performance.

== Plot ==
Dublin. Michelle Kelly is an ambitious, successful lawyer. Her relationship with her doctor boyfriend Ivan is on the rocks. Her mother Rosie has succumbed to premature dementia and lives in a home. Michelle visits her regularly.

Conor Flynn manages a quiet, traditional pub in the city, owned by his father. Conor takes care of the Old Man, a hardened drinker.  He has no girlfriend and has withdrawn somewhat from life. Joe, a barman who has been there since his father’s time works alongside him running the pub, Flynn’s.

After a furious row with Ivan at a party, a visibly upset Michelle bursts into Flynn’s pub after hours. Her friend, Kathy comes to take her home but not before Conor’s interest is piqued. The morning after, a chastened and hungover Michelle returns to the pub to collect her phone. There is a definite spark of attraction between them. Michelle begins to frequent the pub.

They go dancing with Joe and Kathy. She invites them all back to her apartment. Conor and Michelle kiss for the first time but they are interrupted. Conor asks her out on a date. They return to the apartment, alone this time. They kiss but unexpectedly he leaves. Michelle is intrigued. She asks him to accompany her to a dinner with her work colleagues. Conor invites them all back to a music session in the pub. Michelle gets up and belts out a song. They go back to her apartment where they have sex for the first time. They become lovers and have sex at every opportunity. Their affair intensifies.

Ivan turns up at Michelle’s apartment and they argue. He leaves but there is clearly still unfinished business between them. Michelle takes Conor away for the weekend to the country. They have an idyllic time together. On their last night Conor confesses that he is falling in love with her.  Michelle is taken aback and urges caution. They return to the city.

Back in the city things start to normalise.  Michelle is promoted and buys herself a sports car. The differences in their lives become more apparent. Conor suspects her feelings for him are cooling. After she stands him up they have a furious row. Things are said. Emotions spill over and she walks out.

They make up.  After a night out they return to the apartment.  They are interrupted by the arrival of Ivan, who lets himself into the apartment. A serious argument develops.  Ivan is forced to leave. As Michelle hadn’t told Conor anything about Ivan he is angry and confused.  Sensing betrayal, Conor freaks out and storms off. Michelle is upset.

Just as Michelle had started to really fall for him Conor retreats  Hurt and angry he rejects her when she comes to the pub to apologise. Their affair ends. Conor’s father dies. He decides to sell up. Life is moving on for them both. They pass each other on the street.  Conor walks by Michelle pointedly ignoring her. Then he turns back and calls to her. They embrace. Reunited.

== Tagline ==
Trouble with sex is you can fall in love

== Cast ==
* Aidan Gillen as Conor
* Renée Weldon as Michelle
* Gerard Mannix Flynn as Joe Eamon Morrissey as Old Man
* Declan Conlon as Ivan
* Susan Fitzgerald as Rosie
* Sinead Keenan as Kathy
* Darragh Kelly as Fred
* Irina Björklund as Eva
* Tomas O’Suilleabhain as Kelvin
* Conor Lambert as Taximan

== Reception ==
The film was premiered at the Dublin International Film Festival on February 19, 2005. Tara Brady in Hot Press wrote “The film makes for sweet likeable drama and Renee Weldon is remarkable as a spiky, sensual, triple-vodka swilling anti-Bridget Jones. You go girl. Chardonnay and propriety be damned.”   Pat Gillett in Irish Star Sunday said that the film “is probably the most explicit Irish movie released with regard to the beast with two backs.”   Esther McCarthy in Sunday World called it a “provocative and entertaining movie about the modern dating game”. 

In his review for The Irish Times, Michael Dwyer said ”in her first leading role, Weldon sparkles with screen presence, belts out a fine version of Crowded House’s Fall at Your Feet, and comfortably holds her own with the subtly expressive Gillen, a Tony nominee on Broadway and one of Ireland’s finest actors. Connolly propels the narrative with the keen sense of pacing and atmosphere he brought to Flick.” 

In an article in The Sunday Times subtitled Irish Film Grows Up, Gerry McCarthy concludes “Flawed, though it is, Trouble With Sex is proof that a film can be commercially viable without being loud or juvenile. Cinema, driven by special effects, has moved away from naturalism. Connolly shows that catching a reflection of contemporary life remains a valid aspiration for a film-maker. The mirror may be clouded in places, but we still need to see our reflection from time to time.” 

== Awards ==
Renée Weldon won the Best Actress in a Leading Role for Trouble With Sex at the 2005 Irish Film & Television Awards. The film was nominated in 8 categories, including Best Irish Film, Best Director, Best Director of Photography, Best Original Score, Best Production Design, Best Costume Design and Best Editing.

== Release ==
The film received a domestic theatrical release opening in cinemas on 6 May 2005.

== Home media ==
Xtra-vision produced the Region 2 DVD that came out in September 2005. Arkles Entertainment released a further Region 2 DVD in August 2009. 

== References ==
 

==External links==
*  
* Trouble With Sex trailer  
* Watch Trouble With Sex  
* Fintan Connolly Interview with Tanya Warren “Trouble With Sex in cinemas”  
* Renée Weldon Interview with Tara Brady “Lets Talk About Sex”  
*  
*  

 
 
 
 