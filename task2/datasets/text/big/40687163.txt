A beszélő köntös
{{Infobox film
 | name             = A beszélő köntös
| director         = Géza Radványi
| based on          =  
 | writer = Károly Nóti, Miklós Asztalos, Géza Palásthy, Ágoston Pacséry
| music            = Vincze Ottó
 | cinematographer        = Barnabás Hegyi
| imdb            = 0033385
 | PORT.hu         = 37244
}}

A beszélő köntös is a Hungarian film that was shot partially in colour and directed by Géza Radványi. It was produced in 1941. It was based on Kálmán Mikszáth’s novel on the same title. It was the first Hungarian film, where outdoor scenes were made with Agfacolor colour technology. 

==Caracteres==
* Pál Jávor (actor)|Pál Jávor (Mihály Lestyák Junior)
* Ferenc Kiss (Mihály Lestyák)
* Maria von Tasnady|Mária Tasnády Fekete (Cinna)
* Gyula Csortos (Pasha of Buda)
* Tivadar Bilicsi (Putnoky)
* Béla Mihályffi (Ágoston)
* Lehotay Árpád (Mihály Szűcs Mihály, judge)
* József Bihari (Pintyő)
* Sándor Tompa (Máté Puszta) Mehmed II)
* Erzsi Orsolya (Sára, Gipsy lady)
* József Juhász (Bey Olaj)
* Piri Vaszary (Mrs Fábián)

==References==
 

==Sources==
*  
*  

 
 
 

 