Married in Canada
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Married in Canada
| image          = 
| alt            =  
| caption        = Theatrical release poster
| director       = Arianne Robinson
| producer       = Arianne Robinson
| writer         = 
| screenplay     = 
| story          = 
| starring       = 
| music          = Edgardo Moreno
| cinematography = 
| editing        = Josephine Boxwell
| studio         = 
| distributor    = 
| released       =  
| runtime        = 60 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Married in Canada is a Canadian documentary about human rights and cross-border same-sex marriage. The documentary is produced and directed by Arianne Robinson, a Toronto-based filmmaker, her first feature-length documentary.

==Synopsis==
Married in Canada follows seven American couples, 3 gay male and 4 lesbian couples, deciding to benefit from Canadian legalized marriage laws to get married in Toronto, while they are prohibited from doing so in the United States in their resident states. The couples and their families illustrate why overcoming the obstacles to legal nuptials is worthwhile, despite the reality that once back home south of the border, the newlyweds will remain merely married in Canada as their marital status will be unrecognized. The marrying couples also candidly discuss differences in attitude between Americans and Canadians based on their experiences in Canada including human rights, homophobia, openness of society and acceptance of the other. The film also takes an inside view on a Canadian agency catering for cross-border same-sex marriage packages to the United States and other countries.

==Screenings==
The world premiere of the film was May 9, 2010, during 2010 Spirit Quest Film Festival. It also showed at the DocMiami International Film Festival.
   
Television premiere was on April 30, 2011 on Canadian gay OUTtv network.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 
 