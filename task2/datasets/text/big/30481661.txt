The Chaperone (film)
{{Infobox film
| name           = The Chaperone
| image          = The Chaperone poster.jpg
| alt            =  
| caption        = Official film poster
| director       = Stephen Herek
| producer       = {{Plainlist | 
* David Calloway
* Nancy Hirami
* Todd Lewis
}}
| screenplay     = S.J. Roth
| starring       = {{Plainlist | Paul "Triple H" Levesque 
* Yeardley Smith 
* Ariel Winter 
* Kevin Corrigan 
* José Zúñiga  Kevin Rankin 
* Enrico Colantoni 
* Annabeth Gish 
* Israel Broussard
}} Jim Johnston
| cinematography = Kenneth Zunder
| editing        = Michel Aller
| studio         = WWE Studios
| distributor    = Samuel Goldwyn Films
| released       =  
| runtime        = 103 minutes
| country        =  
| language       = English
| budget         = $3 million  
| gross          = $14,400  
}} Kevin Rankin, Enrico Colantoni, and Israel Boussard. 

==Plot==
Ray Bradstone (Triple H) is the best wheel man in the business, but he is determined to go straight and be the best parent he can be to his daughter, Sally (Winter), and make amends with his ex-wife, Lynne (Gish). As Ray struggles to find honest work, his old bank-robbing crew, led by Phillip Larue (Corrigan), offers him one last job. He agrees at first, but changes his mind at the last second leaving the crew without a driver. Ray decides instead to serve as a chaperone for Sallys school field trip. When the robbery goes awry, Larue blames Ray and chases the school bus all the way to New Orleans. Ray must deal with Larue, while supervising Sallys class on what becomes one of the craziest school trips ever.

 

==Cast== Paul "Triple H" Levesque as Ray
* Ariel Winter as Sally
* Kevin Corrigan as Larue
* Jose Zuniga as Carlos
* Annabeth Gish as Lynne
* Yeardley Smith as Miss Miller Kevin Rankin as Goldy
* Enrico Colantoni as Dr. Etman
* Ashley Taylor as Meredith
* Israel Broussard as Josh
* Darren OHare as Augie
* Lucy Webb as Dr. Marjorie Jake Walker as Ted
* Cullen Foster Chaffin as Simon
* Taylor Faye Ruffin as Brenda
* Camille Bourgeois as Bill
* Nick Gomez as Nick The Bus Driver
* Alec Rayme as Kevin

==Reception==
  
The Chaperone met with negative reviews from critics. Review aggregation website Rotten Tomatoes gives the film a score of 30% based on reviews from 17 critics.  
Metacritic gives a score of 33/100 based on reviews from 11 critics. 

Eric Kohn of Indiewire graded the film a C-, saying that it had flat direction and a mediocre script and said "As a vehicle for WWE champ Paul "Triple H" Levesque, its haplessly stuck on cruise control."  Nick Schager of Slant Magazine gave it half-a-star out of four, criticizing the script, direction and characters saying that "this hulk-with-a-heart-of-gold fable embraces banalities with a vigor matched only by its lack of imagination." 

==Awards==
{| class="wikitable" rowspan=5; style="text-align: center; background:#ffffff;"
|-
!Award !! Category !! Recipient(s) !! Result !! Ref.
|- Young Artist Young Artist Best Performance Ariel Winter|| ||   
|}

==References==
  

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 