Fury (1947 film)
{{Infobox film
| name = Fury 
| image =
| image_size =
| caption =
| director = Goffredo Alessandrini
| producer =  Fabio Franchini 
| writer =  Giovanni Verga (novel) Vittorio Nino Novarese    Goffredo Alessandrini 
| narrator =
| starring = Isa Pola   Rossano Brazzi   Gino Cervi   Adriana Benetti
| music = Franco Casavola  
| cinematography = Piero Portalupi  
| editing = Giuseppe Fatigati   Fernando Tropea    
| studio = A.G.I.C. 
| distributor = A.G.I.C. 
| released = 2 May 1947 
| runtime = 94 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} La lupa, it is a melodrama set in the horsebreeding community. 

==Cast==
* Isa Pola as Clara  
* Rossano Brazzi as Antonio 
* Gino Cervi as Oreste  
*   Adriana Benetti as Marietta 
* Checco Durante as Postman 
* Paolo Ferrara as Doctor 
* Camillo Pilotto as Priest 
* Cesare Polacco as Lawyer 
* Umberto Spadaro as Rocco 
* Bella Starace Sainati as Priests Sister 
* Attilio Torelli
* Pina Piovani
* Armando Guarnieri

== References ==
 

== Bibliography ==
* Dick, Bernard F. Hal Wallis: Producer to the Stars. University Press of Kentucky, 2004.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 