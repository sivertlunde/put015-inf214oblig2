Family (2006 film)
 
{{Infobox film
| name           = Family 
| image          = Familytiesofblood2.jpg
| caption        = Theatrical release poster
| director       = Rajkumar Santoshi
| producer       = Keshu Ramsay
| story          = Shaktimaan Talwar Ashok Raut
| screenplay     = Rajat Arora Shridhar Raghavan
| writer         = Tigmanshu Dhulia Rajkumar Santoshi
| starring       = Akshay Kumar Amitabh Bachchan Bhoomika Chawla Aryeman Ramsay
| music          = Ram Sampath
| cinematography = Ashok Mehta
| editing        = Shyam Salgonkar
| studio         = Amitabh Bachchan Corporation BIG Pictures
| released       = 12 January 2006
| runtime        = 154 minutes
| country        = India Hindi
| awards         =
| budget         =
}}
Family (also known as Family – Ties of Blood) is a 2006 Bollywood crime drama film directed by Rajkumar Santoshi. The film features Amitabh Bachchan, Akshay Kumar, Bhoomika Chawla and Aryeman Ramsay in lead roles. It released on 12 January 2006, and received mixed response from critics, however bombed at the box office.

== Plot ==
The story is based on the concept of karma and bad deeds.

Viren Sahai (Amitabh Bachchan), is an underworld don who rules over India. His son Abhir (Sushant Singh) is spoilt and believes he can do anything due to his fathers power. When Virens enemy Khan (Kader Khan) attacks Abhir, Viren demands revenge. Viren receives full proof information that Khan will be hidden at an local cinema theater on an particular date. The plot then moves onto the life of a simple cook, Shekhar Bhatia (Akshay Kumar) who is married to Dr. Kavita (Bhumika Chawla). Shekhar lives with his parents, wife and his younger brother Aryan (Aryeman Ramsay). When Aryan runs away from home, Shekhar searches the whole city to find him. During his search, he witnesses Viren attacking the cinema theater in which Khan is hidden, and decides to help the innocent people in the cinema.

Whilst helping the innocent, Shekhar finds Khans nephew fatally wounded and tries to take him to the hospital. Viren witnesses this and mistakenly shoots Shekhar instead of Khans nephew. Soon enough, Viren realizes his mistake and brutally murders Khans nephew and flees the theater. Wounded Shekhar tries to return home however is accidentally hit by Virens car, leading to his death. Aryan returns home and realizes Shekhar has been murdered. Together, Aryan and his group of friends form a gang and kidnap Virens family including Abhir. However, Abhir escapes and reaches to his father.  Aryan tries not to do harm to any of his family member as he knows how precious they are, though Virens wife suddenly dies when Abhir accidentally shoots her which was targeted for Aryan. Viren believes Aryan is responsible for this, Aryan calls Viren to meet him at the same place where Viren killed Shekhar and also the police.But the police decide to help Viren, Aryans plan fails.

Afterwards a changed Viren decides to end it all & surrender to the police.But the police comes out with the intention of killing him on behest of his own son Abhir.But Viren escapes by killing them. Khan enters, As Viren murders Khan, Abhir tries to shoot his father but instead Viren kills Abhir. Aryan leaves the scene, and as Viren demands Aryan to kill him now, Aryan ends the film on a note on karma, and explains his punishment is not death, but life as he has nothing to live for. In the end, Aryan becomes a cook like his late brother and him and his friends start to work in the same canteen Shekhar used to work in.

== Cast ==
* Akshay Kumar as Shekhar Bhatia
* Amitabh Bachchan as Viren Sahai
* Bhoomika Chawla as Dr. Kavita
* Aryeman Ramsay as Aryan
* Sunil Grover as Aryans friend
* Nawazuddin Siddiqui as Aryans friend
* Sushant Singh as Abhir
* Kader Khan as Kalim Khan
* Gulshan Grover as Babubhai Bichhoo
* Raza Murad as Syed
* Shernaz Patel as Sharada
* Viju Khote as Hyder Chacha Rahul Singh

==Soundtrack==
{{Infobox album 
| Name        = Family: Ties Of Blood
| Type        = soundtrack
| Artist      = Ram Sampath
| Cover       =
| Released    = 
| Recorded    = Studio Satya  (Mumbai)  Feature film soundtrack
| Length      = 
| Label       = T-Series
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

The films score and songs were composed by Ram Sampath and the lyrics were penned by Sameer (lyricist)|Sameer. The official soundtrack contains seven songs and two reprise versions. The song "Janam Janam" performed by Various Artists, was not used in the film.

=== Track listing ===
{{Track listing
| extra_column = Performer(s)
| title1 = Family Theme | extra1 = Prakash| length1 = 2:37
| title2 = Janam Janam | extra2 = Shailendra , Suhail Kaul , Arnab Chakraborty , Somya Rao , Madhushri
 | length2 = 4:31 Suhail Kaul | length3 = 2:59
| title4 = Lori | extra4 = Sona Mohapatra | length4 = 3:32 Suhail Kaul, Ernie | length5 = 3:32 Suhail Kaul, Ernie | length6 = 3:06
| title7 = Qatra Qatra | extra7 = Arnab Chakrabarty, Sowmya Raoh | length7 = 3:06
| title8 = Qatra Qatra - 2 | extra8 = Arnab Chakrabarty, Sowmya Raoh | length8 = 3:09
| title9 = Quick Byte | note9 = Acoustic | extra9 = Arnab Chakrabarty | length9 = 1:50
}}

== External links ==
* 

 

 
 
 
 
 
 
 
 