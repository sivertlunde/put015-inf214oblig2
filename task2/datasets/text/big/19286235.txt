Paasam
{{Infobox film
| name           = Paasam
| image          = 
| director       = T. R. Ramanna
| producer       = T. R. Ramanna
| writer         = Thuraiyoor K. Murthy
| story          = Thuraiyoor K. Murthy
| starring       = M. G. Ramachandran M. R. Radha B. Saroja Devi T. R. Rajakumari S. A. Ashokan T. R. Ramachandran
| music          = Viswanathan–Ramamoorthy
| cinematography = M. A. Rehman
| editing        = R. Rajagopal
| studio         = R. R. Pictures
| distributor    = R. R. Pictures
| released       =   
| runtime        = 158 mins
| country        =   India Tamil
}}

Paasam is a Tamil language directed by T. R. Ramanna.  The film features M. G. Ramachandran, M. R. Radha, B. Saroja Devi and T. R. Rajakumari in lead roles. 
The film, produced by R. R. Pictures, had musical score by Viswanathan–Ramamoorthy and was released on 31 August 1962. 
The film only ran for 80 days and flopped at box-office. 

The character of MGR dies at the end.

==Cast==
* M. G. Ramachandran
* M. R. Radha
* B. Saroja Devi
* T. R. Rajakumari
* Sheela Devi
* Kalyan Kumar
* S. A. Ashokan
* T. R. Ramachandran
* S. N. Lakshmi
* C. K. Saraswathi   Lakshmi Rajyam

==Crew==
*Producer: T. R. Ramanna
*Production Company: R. R. Pictures
*Director: T. R. Ramanna
*Music: Viswanathan–Ramamoorthy
*Lyrics: Kannadasan
*Story: Thuraiyoor K. Murthy
*Dialogues: Thuraiyoor K. Murthy
*Art Direction: Selvaraj
*Editing: R. Rajagopal
*Choreography: B. Hiralal
*Cinematography: M. A. Rehman
*Stunt: Shyam Sundar
*Songs Recording & Re-Recording: T. S. Rangasamy
*Dance: None

==Soundtrack==
The music composed by Viswanathan–Ramamoorthy. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:41
|-
| 2 || Pal Vannam Paruvam || P. B. Sreenivas, P. Susheela || 03:48
|-
| 3 || Maalaiyum Iravum || P. B. Sreenivas, S. Janaki || 03:26
|-
| 4 || Therathu Silaiyethu || P. Susheela || 03:16
|-
| 5 || Ulagam Pirandhadhu Enakkaga || T. M. Soundararajan || 03:41
|-
| 6 || Uravu Solla Oruvarindri || P. Susheela || 04:29
|-
| 7 || Vengaikku Kurivaithu || Sirkazhi Govindarajan || 04:04
|}

==References==
 

==External links==
 

 
 
 
 
 


 