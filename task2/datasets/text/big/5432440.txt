The Three Little Pigs (film)
 
{{Infobox Hollywood cartoon
| cartoon name      =The Three Little Pigs
| series            = Silly Symphonies
| image             = Three Little Pigs poster.jpg
| image size        = 
| alt               = 
| caption           = 
| director          = Burt Gillett
| producer          = Walt Disney
| story artist      = 
| narrator          = 
| voice actor       = Pinto Colvig Billy Bletcher Mary Moder Dorothy Compton
| musician          = Carl W. Stalling Frank Churchill Fred Moore Dick Lundy Norm Ferguson
| layout artist     = 
| background artist =  Walt Disney Productions
| distributor       = United Artists
| release date      = May 27, 1933
| color process     = Technicolor
| runtime           = 8 min
| country           = United States
| language          = English
| preceded by       = Birds in the Spring
| followed by       = Three Little Pigs
}}
 animated short fairy tale Best Animated Short Film. The short  cost $22,000 and grossed $250,000.    In 1994, it was voted #11 of the 50 Greatest Cartoons of all time by members of the animation field. In 2007, The Three Little Pigs was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot== fiddle and Wolf comes through your door!" Fifer and Fiddler ignore him and continue to play, singing the now famous song "Whos Afraid of the Big Bad Wolf?".

As they are singing, the Big Bad Wolf really comes by, and blows Fifers house down (except for the roof) with little resistance. Fifer manages to escape and hides at Fiddlers house. The wolf pretends to give up and go home, but returns disguised as an innocent sheep. The pigs see through the disguise ("Not by the hair of our chinny-chin-chin! You cant fool us with that old sheep skin!"), whereupon the Wolf blows Fiddlers house down (except for the door). The two pigs manage to escape and hide at Practicals house. The Wolf arrives disguised as a Jewish peddler/Fuller Brush man to trick the pigs into letting him in, but fails. The Wolf then tries to blow down the strong brick house (losing his clothing in the process), but is unable. Finally, he attempts to enter the house through the chimney, but smart Practical Pig takes off the lid of a boiling pot filled with water (to which he adds turpentine) under the chimney, and the Wolf falls right into it. Shrieking in pain, the Wolf runs away frantically, while the pigs sing "Whos Afraid of the Big Bad Wolf?" again. Practical then plays a trick by knocking on his piano, causing his brothers to think the Wolf has returned and hide under Practicals bed.

==Reaction and legacy==
The movie was phenomenally successful with audiences of the day, so much that theaters ran the cartoon for months after its debut, to great financial response.  A number of theaters added hand-drawn "beards" to the movie posters for the cartoon as a way of indicating how long its theatrical run lasted.   The cartoon is still considered to be the most successful animated short ever made,  and remained on top of animation until Disney was able to boost Mickeys popularity further by making him a top merchandise icon by the end of 1934. 

Animator Chuck Jones said,  "That was the first time that anybody ever brought characters to life  . They were three characters who looked alike and acted differently". (Other animation historians, particularly admirers of  s who would be dedicated to working on a "story development" phase of the production pipeline. 
 the further "Three Pigs" cartoons was seen as a factor in Walt Disneys decision not to rest on his laurels, but instead to continue to move forward with risk-taking projects, such as the multiplane camera and the first feature-length animated movie. Disneys slogan, often repeated over the years, was "you cant top pigs with pigs." 

==Song== Nazis began expanding the boundaries of Germany in the years preceding World War II, the song was used to represent the complacency of the Western world in allowing Adolf Hitler to make considerable acquisitions of territory without going to war, and was notably used in Disney animations for the Canadian war effort.

The song was further used as the inspiration for the title of the 1963 play Whos Afraid of Virginia Woolf?

==Censorship==
One sequence in the cartoon which showed the Big Bad Wolf dressed as a Jewish peddler was excised from the film after its release  and was re-animated so the Wolf would be a Fuller Brush Man, albeit one with a Yiddish accent. A nose, glasses and beard disguise also remained. Airings on American television have edited this further by using the Fuller Brush Man footage and redubbing the Wolfs voice so that he does not sound stereotypically Jewish. When the film was released on home video, the scene was further edited: the topical Fuller Brush Man line "Im the Fuller Brush Man...Im giving a free sample!" was changed to the incongruous "Im the Fuller Brush Man - Im working my way through college" for this and all subsequent home video releases.

==Home video==
In the United States, the short was first released on VHS, Betamax and Laserdisc in 1984 as part of its "Cartoon Classics" Home Video series. It came out on VHS in the UK in spring 1996 as part of the Disney Storybook Favourites series. It made its  DVD debut on December 4, 2001, included in the Silly Symphonies set of the Walt Disney Treasures line, with the PAL release retaining the Jewish peddler animation. It was later included in Walt Disneys Timeless Tales, Vol. 1, released August 16, 2005 (featuring the edited version in the US Silly Symphonies set), which also featured The Pied Piper (1933), The Grasshopper and the Ants (1934), The Tortoise and the Hare (1935) and The Prince and the Pauper (1990).
In those other countries to whom the original 1933 cartoon was first released with original soundtracks in both English and other foreign languages, the uncensored images — with original 1933 soundtracks in both English and other foreign languages — are still issued by the Disney corporation in home-release videos.

==Sequels==
  drag Wolf and barely evade capture. He proceeds in running ahead of them to the residence of the old woman. The Wolf places her in a closet and then awaits her granddaughter to arrive. The young girl soon does, but also enters the closet with the assistance of her grandmother. Then Fiddler and Fifer Pig alert their brother to the situation. Practical arrives and soon manages to send the Wolf running by placing hot coals and popcorn into his trousers. The short contained several gags but at the time failed to repeat the commercial success of the original. Modern audiences have found it entertaining enough but still inferior to its predecessor.

In 1936, a third cartoon starring the three little pigs and the Big Bad Wolf followed, with a theme more towards The Boy Who Cried Wolf. This short was entitled The Three Little Wolves and it was so called because it introduced the Big Bad Wolfs three pup sons, all of whom just as eager for a taste of the pigs as their father.

One more cartoon short featuring the characters, The Practical Pig, was released in 1939, right at the end of the Silly Symphonies run.

In 1941, much of the film was edited into The Thrifty Pig, which was distributed by the National Film Board of Canada. Here, Practical Pig builds his house out of Canadian war bonds, and the Big Bad Wolf representing Nazi Germany is unable to blow his house down.

A new character, Lil Bad Wolf, the son of the Big Bad Wolf, was introduced in subsequent Disney comic books.  He was a constant vexation to his father, the Big Bad Wolf, because the little son was not actually bad.  His favorite playmates, in fact, were the Three Pigs.

There were subsequent sequels made for the Disney TV series Mickey Mouse Works as well.

==Warner Bros. cartoons== The Three Little Bops, featuring the pigs as a jazz band, who refused to let the inept trumpet-playing wolf join until after he died and went to Hell, whereupon his playing markedly improved.  Both of these cartoons were directed by ex-Disney animator Friz Freleng. The third film was The Windblown Hare, featuring Bugs Bunny, and directed by Robert McKimson. In "Windblown", Bugs is conned into first buying the straw house, which the wolf blows down, and then the sticks house, which the wolf also blows down. After these incidents, Bugs decides to help the wolf and get revenge on all three pigs, who are now at the brick house.

==In popular culture==
The pigs and the Big Bad Wolf also appear at the Walt Disney Parks and Resorts as meetable characters.

The three little pigs were featured in  .

A miniature set of the pigs’ homes is featured in the Storybook Land Canal Boats attraction in Disneyland Park (Anaheim).

A poster in the queue area for the Magic Kingdom attraction Mickeys PhilharMagic features the three little pigs and the Big Bad Wolf as The Wolf Gang Trio.

Fiddler Pig, Fifer Pig, and Zeke the Wolf appeared in Who Framed Roger Rabbit.
 Disney California Adventure is named Fiddler, Fifer & Practical Cafe in homage to the pigs. The shop is decorated with a motif of fifes, fiddles and pianos.

The pigs also appear in the video game  , as scrapped versions of themselves.

==See Also==
*List of Disney animated films based on fairy tales

==References==
 

== External links ==
*  
*  
*  
*   in the Encyclopedia of Disney Animated Shorts

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 