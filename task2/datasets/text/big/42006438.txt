Dhruva (film)
{{Infobox film|
| name = Dhruva
| image = 
| caption =
| director = M. S. Ramesh
| writer = M. S. Ramesh   R. Rajashekar Darshan  Sherin   Om Puri 
| producer = G. D. Suresh Gowda   P. S. Srinivas Murthy
| music = Gurukiran
| cinematography = Seenu
| editing = Shyam
| studio = Sri Seethabhairaveshwara Productions
| released = September 13, 2002
| runtime = 149 min
| language = Kannada
| country =  India
| budgeBold textt =
}}
 action film Darshan and Sherin in the lead roles whilst Om Puri plays an important role. 

The film featured original score and soundtrack composed by Gurukiran.

== Cast == Darshan 
* Sherin 
* Om Puri 
* Avinash
* Datthatreya
* Anantha Velu Sumithra
* Bullet Prakash
* Sadhu Kokila
* P. N. Satya

== Soundtrack ==
The music was composed by Gurukiran for Ashwini Audio label. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Cheluvina Chakori
| extra1 = Unnikrishnan, K. S. Chithra
| lyrics1 = V. Nagendra Prasad
| length1 = 
| title2 = O Vidhiye
| extra2 = Unnikrishnan Kaviraj
| length2 = 
| title3 = Haadiyali Beediyali
| extra3 = Hemanth
| lyrics3 = Sriranga
| length3 = 
| title4 = Meetaru Meetaru
| extra4 = Gurukiran
| lyrics4 = V. Nagendra Prasad
| length4 = 
| title5 = Dina Heege Ninne
| extra5 = Kumar Sanu, K. S. Chithra
| lyrics5 = V. Nagendra Prasad
| length5 = 
}}

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 

 