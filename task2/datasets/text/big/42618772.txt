Zatoichi's Vengeance
{{Infobox film
| name = Zatoichis Vengeance
| image =
| film name      =  {{Infobox name module
| kanji          = 座頭市の歌が聞える
| romaji         = Zatōichi no uta ga kikoeru
}}
| director = Tokuzo Tanaka
| producer = Ikuo Kubodera
| writer = Hajime Takaiwa
| based on       =  
| starring = Shintaro Katsu Jun Hamamura Mayumi Ogawa Kei Sato
| music = Akira Ifukube
| cinematography = Kazuo Miyagawa
| editing        = Kanji Suganuma
| studio         = Daiei Studios
| distributor    =
| released =  
| runtime = 83 minutes
| country = Japan
| language = Japanese
}} Daiei Motion Picture Company (later acquired by Kadokawa Pictures).

Zatoichis Vengeance is the thirteenth episode in the 26-part film series devoted to the character of Zatoichi.

==Plot==
  thunder drum festival. The town is under the domination of a Yakuza boss who extorts from the people.

==Cast==
* Shintaro Katsu as Zatoichi
* Shigeru Amachi as Kurobe
* Jun Hamamura as blind priest
* Gen Kimura as Tamekichi
* Koichi Mizuhara as Joshuya
* Mayumi Ogawa as Ocho/Oshino
* Kei Sato as Boss Gonzo 

==Reception==

===Critical response===
J. Doyle Wallis, in a review for DVD Talk, wrote that "Zatoichis Vengeance displays one of the most interesting aspects of Zatoichi as a character."   

==References==
 

==External links==
* 
*  
* 
* " by Thomas Raven for freakengine (August 2011)
*  review by D. Trull for Lard Biscuit Enterprises 
*  review by Alec Kubas-Meyer for Unseen Films (15 February 2014)
*  by Mark Pollard for Kung Fu Cinema

 

 
 
 
 
 
 
 
 
 
 


 