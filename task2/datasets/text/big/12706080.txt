Along Came Jones (film)
{{Infobox film
| name           = Along Came Jones
| image          = Along Came Jones.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Stuart Heisler
| producer       = Gary Cooper Walter Thompson (assoc. producer)
| writer         = Nunnally Johnson Alan Le May (novel)
| narrator       =
| starring       = Gary Cooper Loretta Young Dan Duryea William Demarest
| music          = Arthur Lange
| cinematography = Milton R. Krasner
| editing        = Thomas Neff RKO Radio Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 
  ]] Western comedy film starring Gary Cooper, Loretta Young, William Demarest, and Dan Duryea, in which Cooper mercilessly spoofs his own slow-talking cowboy persona.  The movie was adapted by Nunnally Johnson from the novel Useless Cowboy by Alan Le May, and directed by Stuart Heisler.
 Along Came Jones" written by Leiber and Stoller; songwriter Mike Stoller had studied orchestration under Arthur Lange, the composer of the films score.
 Iverson Movie The Lives of a Bengal Lancer (1935) and other productions. Cooper had a Western town built at the movie ranch for Along Came Jones, which was then used in many other productions during the next 10-plus years and became a fixture in B-Westerns in particular.

==Plot==
Easygoing Melody Jones (Gary Cooper) and his friend George Fury (William Demarest) wander into a town. Jones is mistaken for a wanted bandit named Monte Jarrad (Dan Duryea), which causes him no end of trouble. Meanwhile, the real Jarrad is hiding out in the home of his girl, Cherry de Longpre (Loretta Young). At first, she tries to use the newcomer to distract the townsfolk, but as she gets to know Jones, her feelings start to change.

==Cast==
* Gary Cooper as Melody Jones
* Loretta Young as Cherry de Longpre
* William Demarest as George Fury
* Dan Duryea as Monte Jarrad
* Frank Sully as Avery de Longpre  
* Don Costello as Leo Gledhill  
* Walter Sande as Ira Waggoner   Russell Simpson as Pop de Longpre  
* Arthur Loft as Sheriff  
* Willard Robertson as Luke Packard  
* Ray Teal as Kriendler  
* Lane Chandler as Boone

== References ==
 

==External links==
 
* 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 