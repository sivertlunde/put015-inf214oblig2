Kalavaram
{{Infobox film
| name           = Kalavaram
| image          = 
| alt            =  
| caption        = 
| director       = Ramesh Selvan
| producer       = T. R. Ravichandran
| writer         = 
| starring       = Sathyaraj Ajay Reddy Yasir
| music          = F. S. Faizal
| cinematography = P. Chandran
| editing        = A. Marish
| studio         = Universal Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}}
Kalavaram ( ) is a 2014 Tamil film directed by Ramesh Selvan and produced by Ravichandran. The film features veteran actors Sathyaraj and Tanikella Bharani in the leading roles, while an ensemble cast of newcomers play other pivotal roles. The film, which had been in production since 2010, released on 14 January 2014. 

==Cast==
 
* Sathyaraj as Vetriselvan
* Tanikella Bharani as Adi Moolam
* Ajay Reddy
* Yasir
* Ragavendar
* Kutti Ajay
* Lavanya
* Harini 
* Inbanila 
* Riya
* Sujibala
* Mayilsamy  Raj Kapoor
 

==Production==
Ramesh Selvan, director of the films Ulavuthurai (1998) and Jananam (2004), announced that he would be making a comeback in early 2010, revealing that he was working a project titled Kalavaram with Sathyaraj in the lead role. He noted that he had taken an extended break as his previous film, Runway with Prashanth, had run into financial problems. Kalavaram was revealed to be based on a true story, that of the biggest riot that has ever taken place in Tamil Nadu, with Sathyaraj playing a deputy police commissioner who heads an investigation commission.   The soundtrack of the film was released at an event in Chennai in September 2011, with composer Faizal noting that there were only two songs in the film. 

The release of the film was pushed back several times, leaving the project which began in 2010, in development hell. Plans to release the film in August 2013 were also unsuccessful due to a high number of other films seeking theatres for release.  Around the same period, Ramesh directed Thalaivan which he opted out due to health problems and the film was also involved in a legal tussle with the makers of the Vijay (actor)|Vijay-starrer Thalaivaa, accusing them of failing to live up to their commitments. Ramesh revealed that the producer of Thalaivaa, Chandraprakash, asked him to delay the release of Kalavaram to get more theatre space for his film. In return, Chandraprakash reportedly told Ramesh that he would purchase and distribute his film under his Sri Mishri Productions banner. Ramesh then alleged that the producer backed out of his commitments. 

==Release==
The film finally released on 14 January 2014 coinciding with the Tamil festival of Thai Pongal. It opened to mixed reviews, with a critic from The Hindu noting the film has a "messy plot" and that it "never becomes anything more than a generic action movie, with generally underwhelming performances and staging".  A critic from the New Indian Express noted the film is "interesting in parts" and "a more coherent screenplay and consistency in treatment could have made it an engaging fare." 

== References ==
 

 
 