Chandavalliya Thota
{{Infobox film|
| name = Chandavalliya Thota
| image = 
| caption =
| director = T. V. Singh Thakur
| writer = T. R. Subba Rao
| based on =   Rajkumar  Jayanthi   Rajasree
| producer = Pals & Company
| music = T. G. Lingappa
| cinematography = B. Dorairaj
| editing = Venkatram   Raghupathy
| studio = Pals & Co.
| released = 1964
| runtime = 145 minutes
| language = Kannada
| country = India
}}
 Jayanthi in lead roles. The film won many laurels upon release including the National Film Award for Best Feature Film in Kannada for its Gandhian theme of dealing with the poverty in rural Indian villages. 

==Cast== Rajkumar 
* Udaykumar  Jayanthi
* Rajasree Balakrishna
* Raghavendra Rao
* Advani Lakshmi Devi
* Jayashree
* Sharadamma
* Shanthamma

==Soundtrack==
The music was composed by T. G. Lingappa with lyrics by R. N. Jayagopal and Ta Ra Su. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Oh Nanna Bandhavare
| extra1 = Nageswara Rao Pantulu
| lyrics1 = Ta Ra Su
| length1 = 
| title2 = Ondaguva Mundaguva
| extra2 = K. J. Yesudas, L. R. Eswari
| lyrics2 = R. N. Jayagopal
| length2 = 
| title3 = Suma Baleya Premada
| extra3 = L. R. Eswari
| lyrics3 = R. N. Jayagopal
| length3 = 
| title4 = Balli Hange
| extra4 = P. B. Srinivas, S. Janaki
| lyrics4 = R. N. Jayagopal
| length4 = 
| title5 = Ee Neethi Ee Nyaya
| extra5 = P. B. Srinivas
| lyrics5 = R. N. Jayagopal
| length5 = 
| title6 = Namma Maneye Nandana
| extra6 = S. Janaki
| lyrics6 = Ta Ra Su
| length6 = 
}}

==Awards==
* National Film Award for Best Feature Film in Kannada - 1964
; This film screened at IFFI 1992 Kannada cinema Retrospect.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 


 