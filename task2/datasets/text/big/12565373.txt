Watchers II
{{Infobox film
| name    = Watchers II
| image    = Watchers 2.jpg
| image_size   = 
| caption   = 
| director   = Thierry Notz
| producer   = Roger Corman
| writer    = Screenplay:  
| narrator   = 
| starring   = Timothy Marlowe Marc Singer Tracy Scoggins Jonathan Farwell Irene Miracle
| music    = Rick Conrad
| cinematography = Edward J. Pei
| editing    = Diane Fingado Adam Wolfe
| distributor    = 
| released    =  
| runtime    = 101 mins.
| country   = United States
| language   = English
| budget   = 
| gross    = 
}}

Watchers II is the 1990 sequel to the horror film Watchers (film)|Watchers.  Starring Marc Singer and Tracy Scoggins, the film is loosely based on the novel Watchers (novel)|Watchers by Dean Koontz. It was released on August 16, 1990.

The films writers were credited under a pseudonym as they were not members of the Writers Guild at the time. Chris Nashawaty, Crab Monsters, Teenage Cavemen and Candy Stripe Nurses - Roger Corman: King of the B Movie, Abrams, 2013 p 220  

==Plot== genetically altered Marine Corps animal psychologist from the top secret laboratory where the genetic manipulation occurred. Together with this trusted psychologist (Tracy Scoggins), they attempt to elude the murderous monster and the gory trail of death closing in on them.

==Release==
The sequel did not receive the benefit of a theatrical release that the original did. Instead, the film went straight-to-video via VHS and laser-disc.   The film remained out-of-print for many years until September 23, 2003, when both this film and the original were released on a double feature DVD by Artisan Entertainment, although the release was eventually discontinued.  As of December 21, 2009, Lions Gate has not announced any plans to re-release both movies on DVD.

==Critical reception==
The film has largely received negative reviews, although Watchers II has often been noted as the closest to Koontzs book, out of all four film adaptions. Scott Weinberg of eFilmCritic.com gave 2.5 out of 5, and stated "Watchers 2 is an improvement of Empire Strikes Back proportions over its predecessor for one distinct reason: Part 2 doesnt have Corey Haim from Watchers in it. The plot is, not at all surprisingly, exactly the same as in the original Watchers, but none of the plot stuff really matters all that much. Whats most interesting about Watchers 2 ("most" being a relative term) is that it actually seems to follow much more closely to the Dean Koontz source material than did the original. Its still a painly chintzy and woodenly delivered little affair, but if you happen to be a fan of the novel, Part 2 is the one youd probably dig the "most"."  Walter Chaw of Film Freak Central described the film as "A joyless exercise."  

J. P. Harris, in his 2001 book Time Capsule: Reviews of Horror, Science Fiction and Fantasy Films and TV Shows from 1987-1991, stated "While a marked improvement over "Watchers," "Watchers II" is hardly more faithful to the wonderful novel; "improvements" by scripter Dominic include several plot flaws. But the kernel of both films and book are still there. The monster has an awful rubbery suit by Dean Jones and William Star Jones, which is ineffective even as a shadow. The film gives Singer an opportunity to be laughably macho. As in the book, the best scenes are of the dog, played by golden retriever Dakai, using a computer and other displays of intelligence." 

==References==
 
==External links==
* 
* 

 
 

 
 
 
 
 
 
 