Song of Love (1947 film)
{{Infobox film
| name           = Song of Love
| image          = Poster of the movie Song of Love.jpg
| image size     =
| producer       = Clarence Brown
| director       = Clarence Brown
| writer         = Ivan Tors Irma von Cube Allen Vincent Robert Ardrey Robert Walker
| music          = Robert Schumann Johannes Brahms Franz Liszt
| cinematography = Harry Stradling Sr.
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 119 min.
| language       = English
| budget = $2,696,000  . 
| gross = $2,737,000 
}} Robert Walker, and Leo G. Carroll, directed by Clarence Brown and released by Metro-Goldwyn-Mayer. 

Hepburn plays Clara Wieck, Henreid plays Robert Schumann, Walker plays Johannes Brahms, and Henry Daniell plays Franz Liszt. The screenplay was co-authored by Ivan Tors, Irma von Cube, Allen Vincent, and Robert Ardrey, based on a play by Bernard Schubert and Mario Silva.

Hepburn trained intensively with a pianist so that she could be filmed playing the piano.    When Henreid is playing piano, the hands of  .  
==Reception==
The film earned $1,469,000 in the US and Canada and $1,268,000 elsewhere resulting in a loss of $1,091,000. 
==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 