The Photographer (film)
 
 
{{Infobox film
| name = The Photographer
| image = ThePhotographer2000.jpg
| caption = DVD cover for The Photographer
| director = Jeremy Stein Chris Moore Jeremy Stein
| writer = Jeremy Stein
| screenplay =
| story =
| based on =  
| starring = Reg Rogers Anthony Michael Hall Maggie Gyllenhaal
| music = Andrew Hollander
| cinematography = Vanja Cernjul
| editing = Sylvia Waliga
| studio =
| distributor =
| released =  
| runtime = 91 minutes
| country = United States
| language = English
| budget =
| gross =
}}

The Photographer is a 2000 comedy film directed by Jeremy Stein. It revolves around a photographer who has a single evening to find ten magical photographs, or else he stands to lose everything that is important to him.

==Plot==
A year after becoming the toast of New York Citys art scene, photographer Max Martin has lost his ability to take a decent picture. On the night before his make-or-break gallery opening, surrounded by the trappings of success but devoid of inspiration, Max embarks on a bizarre trek through the city in search of ten mysterious photographs that could save his career. Accompanied by an unlikely crew of strangers he meets along the way, Max trips through a modern-day Oz, and rediscovers the easily forgotten value of seeing magic reflected in everyday life. 

==Cast==
* Reg Rogers - Max Martin
* Tina Holmes - Amy
* Anthony Michael Hall - Greg
* Marisa Berenson - Julie Morris
* Kristen Wilson - Schuyler
* Fenton Lawless - Bartender Richard Bright - Drunk in Bar
* Tom Noonan - Butler
* Josh Stamberg - Peter Morgan
* Alex Draper - Nelson Stanley John Heard - Marcello
* Rob Campbell - Romeo
* Michael Shannon - Maurice
* Maggie Gyllenhaal - Mira
* Leslie Lyles - Zora
* Chris Bauer - Paul Siobhan Fallon - Crazy Lady
* Missy Yager - Judy
* Miles Chapin - Steve
* Mary Alice - Violet
* Ritchie Coster - Attacker #1

==References==
 

==External links==
*  
*  

 
 
 
 
 
 