Bayalu Daari
{{Multiple issues|
  }}

{{Infobox film|
| name = Bayalu Daari
| image = 
| caption =
| director = Dorai Bhagwan
| writer = Bharathi Sutha (novel) Kalpana  Ashok
| producer = Dorai Bhagwan
| music = Rajan-Nagendra
| cinematography = R. Chittibabu
| editing = P. Bhaktavatsalam
| studio = Anupam Movies
| released = 1977
| runtime = 139 min
| language = Kannada
| country =  India
| budgeBold textt =
}}
 Kalpana and K. S. Ashwath in lead roles.

The film was a musical blockbuster with all the songs composed by Rajan-Nagendra considered evergreen hits.

==Cast==
* Ananth Nag as Gopu Kalpana as Chandra
* K. S. Ashwath 
* Jayalakshmi
* Advani Lakshmi Devi Balakrishna
* "Upasane" Seetharam

==Soundtrack==
The music was composed by Rajan-Nagendra with lyrics by Chi. Udaya Shankar.  All the 3 songs composed for the film were received extremely well and considered as evergreen songs.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| music_credits = no
| collapsed = no
| title1 =  Kanasalu Neene Manasalu Neene
| extra1 = S. P. Balasubramanyam, Vani Jayaram
| music1 = 
| length1 = 03:33
| title2 = Elliruve Manava Kaaduva
| extra2 = S. P. Balasubramanyam
| music2 = 
| length2 = 04: 31
| title3 = Baanallu Neene Bhuviyallu Neene
| extra3 = S. Janaki
| music3 =
| length3 = 04:26
}}

==Awards==
* Karnataka State Film Award for Best Supporting Actress - Jayalakshmi

==References==
 

==External links==
*  
*  

 
 
 
 
 
 


 