Hong Kong 1941
 
 
{{Infobox film
| name           = Hong Kong 1941
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Po-Chih Leong
| producer       = John Sham
| writer         = Chan Koon-Chung Sammo Hung Hong Kong Cinemagic:   
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Chow Yun-fat Cecilia Yip Alex Man Shih Kien Wu Ma
| music          = 
| cinematography = Brian Lai
| editing        = Peter Cheung
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 min
| country        = Hong Kong
| language       = 
| budget         = 
| gross          = HK$ 7.223 M. 
}} 1984 Cinema Hong Kong film, directed by Po-Chih Leong and written by Sammo Hung.

==Synopsis==
The film takes places shortly before and during the Japanese occupation of Hong Kong during World War II. The story of three young friends focuses on their sufferings as Hong Kong falls under oppressive occupation.

Years later, a woman narrates her personal story of the Japanese takeover of Hong Kong in 1941. Shes Nam; young, attractive, daughter of a wealthy rice merchant, and prey to painful, disabling seizures. Her boyhood friend is Coolie Keung, whose family used to have wealth; hes now impoverished, a tough kid, a leader, in love with her. Into the mix steps Fay, cool and resourceful, an actor from the north, intent on getting to Gold Mountain in the US or Australia. They form a trio, but the day they are to leave Hong Kong, the invasion stops them. Fay must rescue Keung from collaborators, Nam falls in love with Fay, and danger awaits their next attempt to escape

==Cast and roles==
* Chow Yun-fat - Yip Kim Fei
* Cecilia Yip - Han Yuk Nam
* Alex Man - Wong Hak Keung
* Shih Kien - Ha Chung-Sun, Nams father
* Wu Ma - Chairman Liu Yan-Mau Paul Chun - Fa Wing
* Ku Feng - Shui / Shiu
* Stuart Ong - General Kanezawa
* Billy Lau - Factory Foreman
* Angela Yu Chien - Feis Aunt
* Yung Sai-Kit - General Kanezawa
* Chu Tau - Lius men
* Kam Kong Chow - Lius man
* Chin Kar-lok - extra
* Pang Yun-Cheung - extra
* Lee Chi Kit - extra
* Leong Po Chih

==References==
 

==External links==
*  
*  

 
 
 
 
 

 