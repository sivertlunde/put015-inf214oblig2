The Expendables (2010 film)
 
{{Infobox film
| name = The Expendables
| image = Expendablesposter.jpg
| caption = Theatrical release poster
| alt = Nine armed men dressed in black standing shoulder to shoulder, Sylvester Stallone front and center.
| director = Sylvester Stallone
| producer =  
| screenplay =  
| story = David Callaham
| starring =    Brian Tyler
| editing =  
| cinematography = Jeffrey L. Kimball
| studio =   Lionsgate
| released =  
| country = United States
| runtime = 103 minutes 
| language = English
| budget = $80 million 
| gross = $274.5 million   
}} ensemble action Johnny Mnemonic, and Steve Austins last theatrical release film until 2013s Grown Ups 2.
 CIA officer James Munroe. It pays tribute to the blockbuster action films of the late 1980s and early 1990s. It was distributed by Lionsgate Films|Lionsgate.

The Expendables received mixed reviews , praising the action scenes, but criticizing the lack of story. However, it was commercially successful, opening at number one at the box office in the United States,  the United Kingdom, {{cite web
| url = http://www.metro.co.uk/film/839187-the-expendables-takes-no-1-uk-box-office-spot A sequel was released on August 17, 2012. 

==Plot== Somali pirates. The team consists of leader Barney Ross, blades specialist Lee Christmas, martial artist Yin Yang, Gunner Jensen, weapons specialist Hale Caesar, and demolitions expert Toll Road. Jensen instigates a firefight, causing casualties for the pirates. He then tries to hang a pirate, but Yang stops him when Ross and the team discourage it. Ross reluctantly discharges him from the team. Later, Christmas is upset to discover his girlfriend, Lacy, has left him for another man.
 CIA officer waterboarded for information by Munroe. Meanwhile, Lacy has been physically abused by her new man, so Christmas beats him and his friends, revealing what he does for a living. Ross and the group discover that Church is a CIA operative and the real target is Munroe, who has gone rogue and joined forces with Garza to keep the drug money that funds the CIA to himself, but the CIA cannot afford a mission to kill one of their own directly because of bad publicity. Ross meets tattoo expert and friend Tool to express his feelings. Tool makes a confession about letting a woman commit suicide instead of saving her. Ross is then motivated to go back for Sandra alone, but Yang accompanies him. Jensen and hired men pursue them on the road, ending in an abandoned warehouse, where Yang and Jensen fight a second time. Ross shoots Jensen when he attempts to impale Yang on a pipe. Jensen makes amends and gives the layout of Garzas palace. Ross boards the plane with Yang and finds the rest of the team waiting. 

They then infiltrate Garzas compound. Thinking Munroe hired the team to kill him, Garza has his soldiers faces painted, preparing them for a fight. The team plants explosives throughout the site but Ross, while saving Sandra, is captured by Munroes henchmen. The team saves him and kills the Brit, but is pinned down by Garzas men as Paine wrestles Ross. Caesar fights back and Paine escapes. Garza finally stands up to Munroe, ordering him out and returning his money. Instead, as Garza rallies his men against the Americans, Munroe kills him and escapes with Paine and Sandra. Garzas men open fire against the team, who fight their way through, detonating the explosives and destroying the compound. Toll kills Paine by burning him alive while Ross and Caesar manage to destroy the helicopter before Munroe can escape. Ross and Christmas catch up to Munroe, killing him and saving Sandra. Later, Ross gives his mission reward to Sandra to restore Vilena.
 bullseye from outside the building.

==Cast==
 .]]
: See List of characters from The Expendables film series

* Sylvester Stallone as Barney Ross
* Jason Statham as Lee Christmas
* Jet Li as Yin Yang
* Dolph Lundgren as Gunner Jensen
* Eric Roberts as James Munroe
* Randy Couture as Toll Road Steve Austin as Dan Paine
* David Zayas as General Garza
* Giselle Itié as Sandra
* Charisma Carpenter as Lacy 
* Gary Daniels as Lawrence Sparks / The Brit
* Terry Crews as Hale Caesar
* Mickey Rourke as Tool
* Bruce Willis as Mr. Church
* Arnold Schwarzenegger as Trench Mauser

==Production==
===Development===
In mid-2005, writer David Callaham submitted the first draft of a mercenary-inspired action film titled "Barrow" to Warner Bros., as part of his "blind commitment" deal with the studio at the time. Callaham revised the script two more times and submitted his third final revision in early 2006. Having expressed interest in doing an ensemble film, Sylvester Stallone reviewed Callahams third/final revised draft of Barrow and used it as a "starting point" for The Expendables. 

===Casting===
Jean-Claude Van Damme was personally offered a role by Stallone, but turned it down because he felt there was no substance or development to the character.  South Central." {{Cite news
| date = June 28, 2010
| author = Michael Cieply
| url = http://www.nytimes.com/2010/06/29/movies/29expendables.html
| title = The Return of the Action Flick All-Stars
| newspaper = New York Times
}}
  At the premiere of the film, Stallone claimed to have been speaking to Van Damme over the phone and had said, "I told you!", to which Van Damme concurred and expressed his regret over not participating.  Van Damme would later appear as the main antagonist, Jean Vilain, in the films sequel.
 Demolition Man tax issues, and not able to leave the United States without the courts approval. It was later rewritten for Forest Whitaker.    NFL player Terry Crews. 

Steven Seagal was asked to make a cameo appearance, but turned down the offer due to negative experiences with producer Avi Lerner. {{cite web
| date = June 7, 2009
| author = Clint Morris
| title = Exclusive : Expendables Under Siege
| url = http://www.moviehole.net/200919409-exclusive-expendables-under-siege
}}
 

Robert De Niro, Al Pacino, Ben Kingsley, and Ray Liotta were all considered for the role of James Munroe before Stallones The Specialist co-star Eric Roberts was eventually cast in the role.
 Demolition Man co-star Sandra Bullock was rumored to have a role in the film, but revealed that she did not even know about the project. Despite the news, she did express interest in working in another action film and would have liked to appear in the film, depending on the storyline. {{cite web
| date = July 3, 2009
| author = Alex Fletcher
| title = Bullock denies Expendables rumour
| url = http://www.digitalspy.com/movies/a163290/bullock-denies-expendables-rumour.html
}}
 
 ensemble acting at the moment". {{cite web
| url = http://www.chud.com/articles/articles/18638/1/KURT-RUSSELL-IS-NOT-EXPENDABLE/Page1.html
| title = Kurt Russell is Not Expendable
}}
  Cop Out. Willis casting as Mr. Church was confirmed by August 2009, as was the fact that he would appear in a scene with both Stallone and Schwarzenegger. {{cite web
| date = 2009-08-17
| author =
| url= http://moviesblog.mtv.com/2009/08/17/bruce-willis-will-join-sylvester-stallone-and-arnold-schwarzenegger-on-screen-in-the-expendables/
| title= Bruce Willis Will Join Sylvester Stallone And Arnold Schwarzenegger On Screen In ‘The Expendables’
| publisher = Viacom
| work = Moviesblog.mtv.com
| accessdate = 2010-03-16
}} 

===Filming===
Film production began on March 3, 2009, with a budget of $82&nbsp;million.   
Filming commenced 25 days later in   used for filming is a Grumman HU-16 Albatross and the ship used as a setting in the opening scene was a Russian SA-15 (ship)|SA-15 class Arctic cargo ship Igarka.

In Summer 2010, Brazilian company   released a statement saying it was still owed more than US$2&nbsp;million for its work on the film. {{Cite news
| date = 2 August 2010
| author = Tom Phillips  
| title = Sylvester Stallone pursued Brazilians debts
| url = http://www.guardian.co.uk/film/2010/aug/02/sylvester-stallone-pursued-brazilians-debts
| newspaper = The Guardian
| location=London
}}
 

==Release==
The film had an original scheduled release date set at April 23, 2010, but was later pushed back four months until August 13, to extend production time.  On March 17, 2010, the official international poster for the film was released. {{cite web
| url = http://www.denofgeek.com/movies/441783/official_poster_for_the_expendables.html
| title = Official Poster for The Expendables
}}
 

A promo trailer (aimed at industry professionals) was leaked online in August 2009.  {{cite web
| url = http://rutube.ru/tracks/2490609.html
| title = The Expendables Trailer
}}  
 
Sometime in October, nearly two months after the promo trailer was leaked, it was officially released online.
The promo trailer was edited by Stallone and it was shown at the Venice Film Festival. On April 1, 2010, the official theatrical trailer for the film was released. The film had its red carpet Hollywood premiere on August 3, 2010. The grand premiere of the film was held at the Planet Hollywood Resort and Casino on the Las Vegas Strip in Paradise, Nevada on August 10, 2010.

===Home media===
The theatrical cut of The Expendables was released on DVD/Blu-ray Disc on November 23, 2010. The Blu-ray Disc is a 3-disc combo pack. 

An Extended Directors Cut of the film was meant to be out for an early 2011 DVD/Blu-ray Disc release, but was first released on cable television instead. The Extended Directors Cut was released on Blu-ray Disc on December 13, 2011.   A 90 minute documentary called Inferno: The Making of The Expendables was released exclusively to the theatrical cuts Blu-ray release.

====Extended Directors Cut==== Epix on Sinners Prayer" by Sully Erna in the new opening credits.

==Reception==
===Box office===
The film made its US debut at 3,270 theaters with approximately 4,300 screens, which earned it the #10 spot on the list of the "Biggest Independent Releases of All Time" at Box Office Mojo {{cite web
| title = Widest Independent Releases at the Box Office:
| url = http://www.boxofficemojo.com/alltime/widestindie.htm
| publisher= Box Office Mojo
| accessdate = 2010-08-17
}}
  and the #16 spot on their list of top opening weekends for August. {{cite web
| title = Top opening weekends in August at the Box Office: url = http://boxofficemojo.com/alltime/weekends/month/?mo=08&p=.htm publisher = Box Office Mojo
| accessdate = 2010-08-22
}}
  It earned $34.8&nbsp;million in its opening weekend and took the #1 position in the U.S. box office. {{cite web
| date = August 16, 2010
| first = Brandon | last = Gray
| title = Expendables Pump Up, Eat Pray Love Pigs Out, Scott Pilgrim Powers Down
| url = http://www.boxofficemojo.com/news/?id=2889&p=.htm
| publisher= Box Office Mojo
| accessdate = 2010-08-17
}}  The A-Team. 

Brandon Gray of Box Office Mojo stated that the film "took a commanding lead in its debut", compared to competing films Eat Pray Love and Scott Pilgrim vs. the World. {{cite web
| date = August 14, 2010
| first = Brandon | last = Gray
| title = Friday Report: Expendables Rock It on Opening Day
| url = http://boxofficemojo.com/news/?id=2888&p=.htm
| publisher = Box Office Mojo
| accessdate = 2010-08-17
}}
 
Ben Fritz of the Los Angeles Times stated that the "over-the-top shoot-em-up" opened to a "very strong" reception. As well, he described it as "a crowd-pleaser even if critics didnt take to it."    Research by Lionsgate found that between 38% and 40% of the films viewers were female. The results were unexpected, for a film thought to have limited appeal to female filmgoers.  {{cite news
| date = August 23, 2010
| title = Company Town: The Expendables still No. 1 at box office
| url = http://www.latimes.com/entertainment/news/la-et-boxoffice-20100823,0,2665260.story
| accessdate = 2010-08-23
| work=Los Angeles Times
| first=John
| last=Horn
}}
 

The Expendables remained at the top position in the U.S. box office during its second weekend, earning a weekend total of nearly $17&nbsp;million. {{cite news
| date = August 23, 2010
| title = Expendables Endures at No. 1 with $17 Million
| url = http://www.washingtontimes.com/news/2010/aug/23/expendables-endures-at-no-1-with-17-million/?page=all
| work = The Washington Times
| agency = Associated Press
| accessdate = August 24, 2013
}}
 

As of December 17, 2010, the film has made $103,068,524 in the U.S. and $171,401,870 in the international box office, bringing its worldwide gross to $274,470,394.    

=== Critical response ===
 
The film review aggregator Rotten Tomatoes shows that 41% of critics gave the film a positive review based upon reviews by 192 critics.    normalized rating mean score of 45, based on 35 reviews.    CinemaScore polls, however, reflect solid audience approval with a B+ average grade. 

Some reviews praised the film highly. The Hollywood Reporter stated that "the body count is high and the personalities click in this old-school testosterone fest," {{cite news
|url= http://www.hollywoodreporter.com/hr/film-reviews/the-expendables-film-review-1004107460.story
|work=The Hollywood Reporter
|title= The Expendables – Film Review
| last =Linden | first = Sheri
|date=August 4, 2010
|accessdate=August 10, 2010
}}   Dirty Dozen meets Inglourious Basterds—and then some…" 
Richard Corliss of Time (magazine)|Time added that "what you will find is both familiar in its contours and unique in its casting." 
Peter Paras of E! Online said that the film is "peppered with funny dialogue, epic brawls and supersize explosions," and that "The Expendables is the adrenaline shot the summer of 2010 needs,"  and the Boston Globe stated that the film is "a lot of unholy fun".  Other film review sites and magazines supplied decent feedback such as Empire Magazines Genevieve Harrison who delivered the film a 3/5 stars rating and remarked that the film was "More The Wild Geese than The Wild Bunch, The Expendables is not a wasted opportunity, but more one not fully exploited. For action fans raised on Commando and Cobra, the ensemble cast and ’80s-style violence will be pure wish-fulfilment — but even they could have wished for something better." 

Some highly negative reviews appeared. In The New York Post, Lou Lumenick labeled it "the brain-dead male equivalent of Sex and the City 2",  and in The New Yorker, Anthony Lane called it "breathtakingly sleazy in its lack of imagination". 
Peter Travers, writing for Rolling Stone, said, "Stallone forgets to include non-spazzy direction, a coherent plot, dialogue that actors can speak without cringing, stunts that dont fizzle, blood that isnt digital and an animating spirit that might convince us to give a damn." 
Claudia Puig, writing the review for USA Today, summed the film up as a "sadistic mess of a movie". 

Mickey Rourkes performance was given special recognition by some critics. In the Chicago Tribune, Michael Phillips said, "Rourke delivers a monologue about his time in Bosnia, and the conviction the actor brings to the occasion throws the movie completely out of whack. Whats actual acting doing in a movie like this?" {{cite news
| date =August 12, 2010
| author =
| title = 3 words about The Expendables: Not enough fun
| url = http://www.chicagotribune.com/entertainment/movies/sc-mov-0810-expendables-20100812,0,156384.column
| work=Chicago Tribune
| first=Michael
| last=Phillips
}}
 
Mick LaSalle of the San Francisco Chronicle praised Rourke for the same scene, stating, "Hes amazing…a great actor." {{cite news
| date =August 13, 2010
| author =
| title = Review: Expendables a routine action film
| url = http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2010/08/12/MVLF1ERS4E.DTL
| work=The San Francisco Chronicle
| first=Mick
| last=LaSalle
}}
 

===Accolades===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- Golden Raspberry Award Golden Raspberry Worst Director Sylvester Stallone
| 
|- Saturn Award Saturn Award Best Action/Adventure Film
| 
|-
|}

==Music==
 
{{Infobox album  
| Name        = The Expendables: Original Motion Picture Soundtrack
| Type        = film Brian Tyler
| Cover       =
| Border      = yes
| Alt         =
| Released    = August 10, 2010
| Recorded    =
| Genre       =
| Length      = 71:41
| Label       = Lionsgate
| Producer    =
| Last album  = Skyline (2010)
| This album  = The Expendables (2010)
| Next album  =
}}

{{Album ratings
| rev1      = Allmusic
| rev1Score =   
}}
 Brian Tyler announced on his official website that he had been hired to write original music for the film.  Tyler previously worked with Stallone on Rambo (2008 film)|Rambo in 2008.
 Sinners Prayer"; Stallone liked it and wanted to use it in the film. However, during the films post-production, the scene that "Sinners Prayer" was originally meant to be used in was reworked and the song was taken off the film and its soundtrack. The American hard rock band Shinedown contributed a brand new track, "Diamond Eyes (Boom-Lay Boom-Lay Boom)", recorded specifically for the film, but the song does not appear in the film nor its official soundtrack. The song was used in the theatrical trailer and the finished piece was released on June 15, 2010. Both songs were finally used for the Extended Directors Cut. {{cite web |url= http://www.atlanticrecords.com/news/article/?articleId=8a0af812274dc9b00127b9dcbc3a48b0 title = Shinedowns Unreleased Song Featured In The Expendables Movie Trailer! publisher = Atlantic Records date = 2010-04-01 accessdate = 2010-04-02
}} 
One of the alternate trailers uses the song "Paradise City" by Guns N Roses. 
The song "The Boys Are Back in Town" by Thin Lizzy played in TV spots and is played over the credits. 

The score for the film was released on August 10. The tracklists have been revealed. {{cite web
| url = http://soundtrackgeek.com/2010/07/14/soundtrack-preview-the-expendables-by-brian-tyler/
| title = The Expendables by Brian Tyler
}}
 
{{Track listing
| collapsed       = no
| headline        = The Expendables: Original Motion Picture Soundtrack
| all_music       = Brian Tyler
| title1          = The Expendables
| length1         = 3:22
| title2          = Aerial
| length2         = 2:58
| title3          = Ravens and Skulls
| length3         = 4:49
| title4          = Lee and Lacy
| length4         = 2:15
| title5          = Massive
| length5         = 3:24
| title6          = The Gulf of Aden
| length6         = 6:56
| title7          = Lifeline
| length7         = 4:29
| title8          = Confession
| length8         = 2:56
| title9          = Royal Rumble
| length9         = 3:41
| title10         = Scanning the Enemy
| length10        = 3:47
| title11         = The Contact
| length11        = 1:31
| title12         = Surveillance
| length12        = 3:27
| title13         = Warriors
| length13        = 3:49
| title14         = Trinity
| length14        = 4:19
| title15         = Waterboard
| length15        = 3:01
| title16         = Losing His Mind
| length16        = 2:37
| title17         = Take Your Money
| length17        = 2:41
| title18         = Giant with a Shotgun
| length18        = 3:57
| title19         = Time to Leave
| length19        = 1:55
| title20         = Mayhem and Finale
| length20        = 5:47
}}

==Sequel==
  The Expendables franchise. The film went on to out-gross the first film with $312.5 million and received a more positive critical response.

==References==
 

==External links==
 
 
*  
*  
*  
*  
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 