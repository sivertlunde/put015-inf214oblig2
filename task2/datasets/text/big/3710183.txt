Stander (film)
{{Infobox film
 | name = Stander
| image = Stander ver2.jpg
| caption = Theatrical poster
| director = Bronwen Hughes
| producer = Hillard Elkins Martin Katz Chris Roland Julia Verdin
| writer = Bima Stagg
| starring = Thomas Jane Deborah Kara Unger Ashley Taylor David OHara Dexter Fletcher Marius Weyers David Holmes
| cinematography = Jess Hall
| editing = Robert Ivison
| studio = Grosvenor Park Productions Seven Arts Pictures Stander Productions The Imaginarium ZenHQ
| distributor = Newmarket Films
| released =  
| runtime = 116 minutes
| country = South Africa
| language = English
| budget = 
| gross = $31,651
| preceded_by =
| followed_by = 
}}
 2003 biographical Captain André Stander, a South African police officer turned bank robber, starring Thomas Jane who initially turned down the role.    The filmmakers were able to talk to Allan Heyl, one of Andre Standers accomplices who was still in prison; Cor van Deventer, his police partner; and the warden of the prison where Andre was incarcerated.  

==Plot==
Andre Stander is an officer with the South African Police, newly married with a reputation as the youngest captain on the force, he and his partner are assigned along with other officers to riot duty in the wake of the Soweto uprising. In the chaos of one of the riots in Tembisa, Stander shoots a young, unarmed protester, which deeply affects him and causes him to become disillusioned towards the Apartheid system. One day on his lunch break Stander decides to spontaneously walk in and rob a bank, he thoroughly enjoys the rush and decides to embark on a spree of robberies, even responding to one in official capacity as an officer. In the wake of these robberies, Cor Van Deventer, Standers partner, leads a team assigned to take down the new bank robber. Eventually being able to see through Standers disguises, Deventers team finally makes the arrest, Andre Stander is stripped of his position and sentenced to 32 years in prison.

While in prison Stander meets two other men, Lee McCall and Allan Heyl, whom he quickly fosters a friendship with. The trio have grand plans of what they will do when they get out, even saying that when they do they will come back for each other. After a year or so in prison Stander and McCall go to play a rugby game with other prisoners, during the game they feign serious injury and are taken to the infirmary, where they knock the doctor unconscious and relieve the guards of their weapons. Shortly after their escape Stander and McCall return for Heyl, the three introduce themselves to each other as their new assumed names and proceed to rob a few banks, purchase a high-priced safehouse, and steal a yellow Porsche 911 Targa.

As the robberies continue, the risks that come with it increase exponentially, as the so-called "Stander Gang" is being relentlessly pursued by the police task force under none other than Cor Van Deventer. After a gunshop hold-up that left a women shot as well as able to identify the gang, McCall dropping money on the way out of a bank, and McCalls unexplained shooting spree at another bank that lead to a police chase, the gang soon sees that their luck is running out as they become increasingly more reckless. Deciding it would be best to cut their losses and settle down Stander comes up with a plan to rob the exchange office at the airport and leave South Africa, using a combination of flight schedules and disguises to come up with the best plan. Hours before the robbery is to take place Stander returns to Tembisa to make his final peace with the father of the protester he killed, and is instead beaten with a club by the boys father. As McCall becomes infuriated with the fact Stander didnt come to pull off the robbery, he and Heyl see on the news that if they were to have gone to the airport a large amount of police would have arrested or killed them leading Heyl to say "Even when hes wrong, hes right."

1984, the gang begins to organize their exit strategy when Stander goes off to Fort Lauderdale, Florida to purchase a boat and Heyl plans to go to Greece, however McCalls plans are cut short when a squad of police surround the safehouse. While driving to see McCall, Heyl tells Stander a story about his relationship with a black women, she had become pregnant (not by Heyl) and the two were living together, when police saw this they beat her to the point of miscarriage. Heyl thanks Stander for all he has done to help him and McCall get their revenge on the system, and how the last six months had been the time of him and his friends lives. Meanwhile back at the safehouse McCall scrambles for an escape, but realizing there is no way out he decides to grab two pistols and begin shooting at police, Stander and Heyl pull up just in time to see McCall gunned down by police. As they drive from the scene Stander and Deventer lock eyes, a police chase ensues and the Porsche is severely damaged, leading Stander and Heyl to steal another vehicle and drive off into the distance. Heyl and Stander part ways to go off and escape South Africa, Stander goes to the airport and is followed by numerous police where he is forced to show identification. Deventer frantically rushes to see if it is Stander, but stops when he finds out that it was a false alarm (due to Standers use of a fake passport) and Stander is allowed to leave.

Finally arriving in Fort Lauderdale, Stander is unable to remain inactive for long when he hotwires a Mercury Cougar and runs a red light in front of police. Leading them on a short chase, Stander exits his vehicle and begins to disobey the officers orders, prompting the officers partner to grab a shotgun and threaten Stander with it. Stander disarms the partner only to be shot by the officer multiple times.

==Cast==
*Thomas Jane as Andre Stander
*Deborah Kara Unger as Bekkie Stander
*Ashley Taylor as Cor van Deventer
*David OHara as Allan Heyl
*Dexter Fletcher as Lee McCall
* Ron Smerczak as Cop

==Reception==
Stander received mostly positive reviews from critics. Movie magazine Empire gave it four stars out of five saying "a star turn that shifts Jane up a notch or two and a career best performance". Nev Pierce of the BBC gave it four stars as well. It is rated Fresh at 72% on the Rotten Tomatoes website. 

==Awards and nominations (2005)==
* The film was nominated for a Genie Award for Best Achievement in Direction. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 