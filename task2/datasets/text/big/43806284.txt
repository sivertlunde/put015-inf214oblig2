Punjab Mail (film)
{{Infobox film
| name           = Punjab Mail
| image          = Punjab Mail 1939 film.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Homi Wadia
| producer       = Wadia Movietone
| writer         = JBH Wadia
| screenplay     = Homi Wadia
| story          = JBH Wadia
| based on       = 
| narrator       = 
| starring       = Fearless Nadia John Cawas Boman Shroff Sardar Mansoor
| music          = Madhavlal Damodar Master 
| cinematography = 
| editing        = 
| studio         = Wadia Movietone
| distributor    = Wadia Movietone
| released       = 1939
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1939 action adventure Hindi film directed by Homi Wadia for Wadia Movietone.    The score was provided by Madhavlal Damodar Master,  and stars Fearless Nadia, John Cawas, Sayani Atish, Sardar Mansoor, Boman Shroff and Sarita Devi.  The film once again had Nadia playing the avenging female with mask and whip astride on a horse dispensing justice and beating up the villains.

==Cast==
* Fearless Nadia
* John Cawas
* Sayani Atish
* Sarita Devi
* Boman Shroff
* Sardar Mansoor
* Master Chhotu
* Shahzadi
* Nazira
* Master Mohammed

==Production==
The Wadia brother films normally starred Boman Shroff in the Douglas Fairbanks Sr. style roles, but with Punjab Mail Boman Shroff was replaced by John Cawas as a leading man though Shroff continued to act in character roles for them.    The action films Nadia starred in like Hunterwali (1935), Miss Frontier Mail (1936) and Punjab Mail (1939) had patriotic implications and showed her fighting against persecution and prejudice with all the films turning out to be box-office successes.    The train "metaphor" likening the female protagonist to a speedy train and its "thrilling settings" was continually used in the Wadia Brothers films with titles like Toofan Mail (1932), Miss Frontier Mail (1936), Toofan Express (1938) and Punjab Mail` (1939).   

==Music==
The music director was Madhavlal Damodar Master with lyrics written by Pandit Gyan Chandra. The singers were Sarita Devi, Sardar Mansoor and Master Mohammed.. 
===songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Chale Mori Naiya Nadi Kinare Prem Duare
| Sarita Devi, Sardar Mansoor
|-
| 2
| Is Khaadi Mein Desh Azaadi
| Sarita Devi, Master Mohammed
|-
| 3
| Joban Barse Jiya Mora Tarse
| Sardar Mansoor
|-
| 4
| Qaid Mein Aaye Nand Dulare
| Sarita Devi, Sardar Mansoor
|}


==References==
 

==External links==
* 

 


 
 
 
 
 
 
 