The September Sessions
{{Infobox film
| name           = The September Sessions
| image          =
| caption        =
| writer         =
| starring       = Kelly Slater Shane Dorian Rob Machado Ross Williams Jack Johnson Emmett Malloy Universal Music Studio 411
| released       =  
| runtime        = 40 minutes (original 2002) 60 minutes (special edition 2004)
| language       = English
| cinematography = Chris Malloy (art director) Art Brewer (Still Photographer)
| editing        = Erik Thaler (technical) Jack Johnson Brushfire Records
| budget         =
}} documentary surf Jack Johnson. Often called September Sessions, it is the second of The Moonshine Conspiracy film series.
 Thicker Than Mentawais of Hollow Trees in September, 1999.  On the first morning, they awoke and stared at perfectly flawless waves.  During the next several days, when Jack wasnt surfing, Jack captured on film the rebirth of Kelly.  Jack wrote several songs along the way including "F-Stop Blues".  However, unbeknownst to Jack, this film captures Johnson before his own rise to international stardom both as a filmmaker and as a musician.   on Jack Johnson music    on Woodshed Films website    on amazon.com    on amazon.com    on IMDb.com 

The September Sessions was the result of this historically important piece of surfing, filmmaking, and music.

==Cast==
In alphabetical order:
* Shane Dorian
* Luke Egan
* Brad Gerlach
* Rob Machado
* Kelly Slater
* Ross Williams

==Plot==
Kelly Slater, Shane Dorian, Rob Machado, and others take a freesurf trip to the coast of Sumatra, where they find themselves surfing beautiful waves, and lose the urgency they have come to live with being professionals.

Also includes commentary by the surfers, and some fun antics including nosewalks, bodysurfing, river surfing and music.

==Soundtrack==
Released on December 17, 2002, The September Sessions soundtrack, which is scored by Johnson, are 10 tracks by various performers including  , and Johnson himself.  Jack performs his own version of Jimmy Buffetts classic A Pirate Looks at Forty.  Recorded before Johnsons first album Brushfire Fairytales, these tracks predate Johnsons rise to worldwide stardom.   on Brushfire Records    on amazon.com 

===Track listing===
# " )
# "What Would You Rather Do" - 2:44 (The September Sessions Band)
# "Greys Groove" - 5:19 (DJ Greyboy)
# "Willow Tree" - 3:27 (G. Love & Special Sauce)
# "Super Bowl Sundae" - 4:40 (Ozomatli)
# "Ganges A Go-Go" - 1:58 (Bombay the Hard Way)
# "Thug Style" - 2:27 (The September Sessions Band)
# "Meaningless Conversation" - 4:42 (Princes of Babylon)
# "Piglets Lament" - 5:06 (Beat Down Sound) Jack Johnson)

==Special Edition==
Released in 2004, The September Sessions Special Edition Set included the soundtrack CD and a bonus DVD.  The bonus DVD includes extra lefts, extra rights, The Making of September Sessions, and audio tracks with commentary by Kelly and Jack plus Brad and Rob.

==Awards and honors==
Jack Johnson received the Adobe Highlight Award for September Sessions   at the 2000 ESPN Film Festival.   on Artist Direct 

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 