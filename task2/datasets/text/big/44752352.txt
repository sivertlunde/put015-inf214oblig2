Mupperum Deviyar
{{Infobox film
| name           = Mupperum Deviyar
| image          =
| caption        =
| director       = K. Shankar
| producer       = M. Sarojini Devi
| writer         = K. P. Arivanandham (dialogues)
| screenplay     = K. Shankar
| story          = K. P. Arivanandham Lakshmi Sujatha Sujatha Prabhu Prabhu Ambika Ambika
| music          = M. S. Viswanathan
| cinematography = M. C. Sekar
| editing        = K. Shankar V. Devan
| studio         = Ammu Creations
| distributor    = Ammu Creations
| released       =  
| runtime        = 136 minutes
| country        = India
| language       = Tamil language|Tamil}}
 1987 Cinema Indian Tamil Tamil film,  directed by K. Shankar and produced by M. Sarojini Devi. The film stars Prabhu (actor)|Prabhu, Ambika (actress)|Ambika, K. R. Vijaya and M. N. Nambiar in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
 
*K. R. Vijaya Lakshmi
*Sujatha Sujatha
*Prabhu Prabhu
*Ambika Ambika
*M. N. Nambiar
*Delhi Ganesh
*Sangili Murugan
*Senthil
*Jaiganesh
*Venniradai Moorthy
*Oru Viral Krishna Rao
*LIC Narasimhan
*Heran Ramasamy
*Sangili Murugan
*Kokila
*Master Sridhar
*Sreekanth
 

==Soundtrack==
The music was composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Sapparum Kalviyum ||  ||  || 06.14
|-
| 2 || Arivil Aathavan Naan ||  ||  || 05.32
|-
| 3 || Thirumaal Azhaga ||  ||  || 04.41
|-
| 4 || Niril Oru Pournamani ||  ||  || 04.52
|-
| 5 || Moontru Deyivam ||  ||  || 05.07
|-
| 6 || Kaali Purapattaal ||  ||  || 08.29
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 


 