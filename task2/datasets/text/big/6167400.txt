Twist (film)
{{Infobox Film
| name           = Twist
| image          = Twist Film.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Jacob Tierney
| producer       = Victoria Hirst Dan Lyon Kevin Tierney
| writer         = Jacob Tierney
| based on       =  
| starring       = Nick Stahl Joshua Close
| music          = Ron Proulx
| cinematography = Gerald Packer
| editing        = Mitch Lackie
| distributor    = Christal Films   Strand Releasing
| released       =  
| runtime        = 97 minutes
| country        = Canada
| language       = English
| budget         = $350,000
| gross          = $47,370
}}
Twist is a 2003 Canadian drama film and a retelling of Charles Dickens classic, Oliver Twist.

==Plot==
The film is similar in some ways to  Seth Michael Donskys 1996 film Twisted (1996 film)|Twisted made prior to Tierneys film.
 Dodger (Nick Stahl). The prosaically beautiful Oliver (Joshua Close) falls into the hands of down-and-out young men. Dodge takes Oliver under his wing and instructs him in the unforgiving arts of drug abuse and prostitution. Oliver develops a crush on Dodge and views him as his boyfriend, complicating their friendship. Dodge does not reciprocate his feelings, and reacts angrily to Olivers kisses and other signs of affection. As Olivers innocence dissolves, both young men confront their demons, and ultimately it is Dodge who finds he cannot escape his past. Dodge is found by his abusive brother around the same time the young mens caretaker commits suicide, sending Dodge into a violent rage at the films conclusion.

==Cast== Dodge
* Oliver
* Gary Farmer as Fagin Nancy
* Tygh Runyan as David
* Stephen McHattie as Benjamin/The Senator
* Moti Yona as Charley
* Brigid Tierney as Betsy
* Andre Noble as Adam
* Maxwell McCabe-Lokos as Noah
* Josh Holliday as Morris
* Dave Graham as Buck
* Mike Lobel as Bully

==Reception==
Twist currently holds a 16% Fresh rating on Rotten Tomatoes, with the consensus "Despite of   its contemporary setting and some strong performances, this is a bland retelling of Oliver Twist." 

==See also==
* Oliver Twist
* Twisted (1997 film)|Twisted, a 1997 Seth Michael Donsky similarly conceived film.
* Sugar (2004 film)|Sugar, a 2004 film with a similar plot.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 