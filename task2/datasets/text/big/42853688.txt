Kikaider Reboot
{{Infobox film
| name           = Kikaider Reboot
| film name      = {{Film name
 | kanji = キカイダー REBOOT
 | romaji = Kikaidā Ribūto}}
| image          = Kikaider Reboot.jpg
| caption        = Theatrical poster
| director       = Ten Shimoyama
| producer       = Shinichiro Shirakura Shinichiro Inoue
| writer         = Kento Shimoyama
| starring       = Jingi Irie Aimi Satsukawa Kazushige Nagashima
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =   (Japan)
| runtime        = 110 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
 Japanese science Kikaider franchise created by Shotaro Ishinomori.   

==Plot==
In the near future, the Japanese government develops the ARK Project, lead by Dr. Nobuhiko Kohmyoji, to create androids to help public citizens. Two prototypes are constructed: one by Dr. Kohmyoji, Jiro/Kikaider, an android built with an experimental Conscience Circuit, and another by Professor Gilbert Kanzaki, Mari, a combat-based android. 

Dr. Kohmyoji mysteriously dies and his children, Mitsuko and Masaru are suddenly targeted by Japans Defense Minister. Jiro comes to their aide and vows to protected them, a final request from Dr. Kohmyoji. Masaru becomes fond of Jiro but Mitsuko becomes wary of him. Mari eventually finds Jiro and nearly destroys him but spares him after Mitsuko and Masaru agree to go with her without resistance. A data chip is then retrieved from within Masarus body, containing all of Dr. Kohmyojis research. The Defense Minister then turns the ARK Project into the DARK Project (Developing Advanced Research by Kohmyoji) and forces Professor Gil to complete his work. 

Jiro is then attacked by another android named "Hakaider", who is revealed to be Professor Gil, having surgically placed his brain within the android body. Hakaider proceeds to destroy the ARK Project facility where he is counter-attacked by Jiro. Mitsuko tries to stop him from fighting but Jiro finally realizes his own free-will and chooses to protect by fighting. Jiro manages to defeat Hakaider but at the cost of his life. Mitsuko vows to one day rebuild him. 

In a post-credit scene, the Defense Minister, having escaped the demise of the facility with Mari, confirms the official start of the true android project.

==Cast==
* / :  
* :  
* :  
* :  
* :  
* / :  
* :  
* :  
* :  
* :  

==Production==
At the end of   (2013), a post-credits scene features the sound of a heartbeat and Kikaider is shown with the narrator stating that all the heroes that were featured in the movie "arent the only superheroes of Earth". The 30th episode of Kamen Rider Gaim serves as a prequel to the events of the movie. 

Toei officially announced the film on January 30, 2014 in association with Kadokawa Shoten.  The screenplay reportedly took 2 years to complete. 

The films theme song is a re-recording of the original "Go Go Kikaider" titled  .

==Release==
The film was released nationwide in Japan on May 24, 2014. On October 10, 2014, Kikaider Reboot premiered in Honolulu, due to the popularity of the original Android Kikaider in the 1970s and to celebrate the 40th anniversary of Kikaider in Hawaii. 

==References==
 

==External links==
*   
*  

 

 
 