Straight On till Morning (film)
 
 
{{Infobox film
| name           = Straight On till Morning
| image          =
| image_size     =
| caption        =
| director       = Peter Collinson
| producer       = Michael Carreras John Peacock
| narrator       =
| starring       = Rita Tushingham Shane Briant James Bolam
| studio         = Hammer Studios
| distributor    = EMI
| released       = 9 July 1972
| runtime        = 96 minutes
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British thriller Peter Collinson and starring Rita Tushingham, Shane Briant, James Bolam, Katya Wyeth and John Clive.   A reserved young woman finds herself attracted to a handsome stranger, unaware of his psychotic tendencies. It was made by Hammer Studios.

Leonard Maltins Movie Guide describes the film as an "offbeat thriller, but not terribly effective",  while Halliwells Film Guide describes the film as an "unattractive suspenser, wildly directed," 

==Cast==
* Rita Tushingham - Brenda Thompson
* Shane Briant - Peter
* James Bolam - Joey
* Katya Wyeth - Caroline
* Annie Ross - Liza Tom Bell - Jimmy Lindsay
* Claire Kelly - Margo
* Harold Berens - Mr Harris
* John Clive - Newsagent
* Lola Willard - Customer

==Plot==

Brenda is a plain young woman who lives at home with her mother in Liverpool and enjoys writing fairy tales for children.  One day she tells her mother that she is leaving home and moving to London in order to find a father for her yet unborn baby.

Arriving in London, she has some of her belongings knocked out of her hands by an attractive young man (Peter) who doesn’t give her a second look.  She ends up living in a grubby bedsit before finding a job at a fashionable boutique (run by Jimmy Lindsay), and taking a spare room in a flat owned by one of her co-workers, Caroline specifically as she hopes to meet a man at one of Caroline’s many parties.

Peter, appears to live with an older, alcoholic woman.  He invites her upstairs.  She goes into his room and, unseen, starts screaming.

After discovering Caroline in bed with another co-worker that she was interested in (Joey), Brenda runs crying out of the flat and onto the streets.  She comes across a scruffy dog, and sees its owner looking for it.  Recognising the young man who had previously bumped into her (Peter), she picks up the dog and runs home with it.  Unbeknownst to her, Peter sees her do so.

Back at the flat, Brenda washes the dog, and pretties it up with a bow.  She then uses the address on the dog’s collar to take it back to Peter.  He lets her in, and is nice to her, but upsets her when he insists she tell him why she took the dog.  She finally admits that it was because she wanted to meet him, and have a baby with him.  He suggests that she moves in with him.

While she has gone to get her belongings, Peter kills his dog with a utility knife, ostensibly because it is now pretty.  Brenda lies to Caroline, telling her that she is moving in with her mother who is ill.

After Brenda returns, Peter starts to buy her baby gifts.  Meanwhile Brenda’s mother is worried about not hearing from her daughter, and calls her last known number, which is Caroline’s flat.  She then visits Caroline, before going to the police.  Caroline looks through Brenda’s old room and finds the dog’s broken lead, which has Peter’s address on it.

After Brenda has gone out shopping, Caroline arrives at Peter’s house, and eventually they go to bed.  Peter then murders her with the utility knife, again because she is too beautiful.

When Brenda returns, Peter is alone in the house (the implication being that Caroline has been buried in the garden).  Brenda has had a total makeover – hair, clothes, make-up – in order to look beautiful for Peter.  He makes it very clear that he loves her exactly the way she was.  He tells a fairy story which is interspersed with flashbacks implying that he has killed multiple women.  Brenda and Peter go to bed.

A few days later Peter reads in the paper that there are missing person reports out on both Brenda and Caroline, and when he gets home he forbids Brenda to go out.
Brenda gives signs that she might now be pregnant, and they both seem happy.  Peter tells Brenda he has a surprise for her and takes her up to his bedroom, where he plays her a tape of him killing the dog and Caroline.  Brenda becomes hysterical.

Later Peter sits alone in his house.  There is no sign of Brenda.

==References==
 

 
 

 
 
 
 
 
 
 
 
 
 
 