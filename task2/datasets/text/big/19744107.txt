Han, hun og Hamlet
 
{{Infobox film
| name           = Han, hun og Hamlet
| image          = 
| caption        = 
| director       = Lau Lauritzen Sr.
| producer       = 
| writer         = Lau Lauritzen Jr. Alice OFredericks A. V. Olsen
| starring       = Marguerite Viby
| music          = 
| cinematography = Carlo Bentsen Lau Lauritzen Sr.
| editing        = 
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Han, hun og Hamlet is a 1932 Danish comedy film directed by Lau Lauritzen Sr.

==Cast==
* Marguerite Viby - Eva
* Hans W. Petersen - Evas kæreste
* Olga Svendsen - Institutsbestyrerinde
* Christian Arhoff - Teaterdirektøren
* Erna Schrøder - Skuespillerinde
* Jørgen Lund - Skipper
* Einar Juhl - Regissør Arthur Jensen - Sømand
* Christian Schrøder - Evas far
* Willy Bille
* Johannes Andresen - Sømand Henry Nielsen - Sømand
* Alex Suhr - Fordrukken sømand
* Carl Schenstrøm - Fyrtaarnet
* Harald Madsen - Bivognen
* Solveig Sundborg
* Tove Wallenstrøm - Pupil at the girls boarding school
* Aage Bendixen - Short sailor
* Poul Reichhardt - Sømand i hængekøje (uncredited)

==External links==
* 

 
 
 
 
 
 
 


 
 