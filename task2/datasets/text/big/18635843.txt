The Forgotten Refugees
{{Infobox Film
| name = The Forgotten Refugees
| image = BuyFR-1-.jpg
| image_size =
| caption =
| director = Michael Grynszpan
| producer = The David Project, IsraTV
| writer =
| narrator =
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released = 2005
| runtime = 49 minutes
| country =
| language = English
| budget =
}}

The Forgotten Refugees is a 2005 documentary film produced by The David Project and IsraTV that recounts the rich history of the ancient Jewish communities of the Middle East and North Africa and their rapid demise in the face of persecution in the decades following the creation of the modern State of Israel in 1948. The film was directed by Michael Grynszpan with Ralph Avi Goldwasser serving as the executive producer.

==Summary== forced exodus of Middle Eastern and North African Jewish communities in the second half of the 20th century. Using extensive testimony of refugees from Egypt, Yemen, Libya, Iraq, and Morocco, the film weaves personal stories with dramatic archival footage of rescue missions, historic images of exodus and resettlement, and analyses by contemporary scholars to tell the story of how and why the Jewish population in the Middle East and North Africa declined from one million in 1945 to several thousand today.

==Awards==
The Forgotten Refugees won the "Best Documentary Film" at the 2007 Marbella Film Festival. 

In 2006, the film won "Best Featured Documentary" at the Warsaw Jewish Film Festival. 

==Interviews==
Personalities interviewed in the film include:
* Irwin Cotler, Canadian member of parliament and international human rights lawyer
* Mordechai Ben-Porat, Iraqi Jew and organizer of the mass exodus of Iraqi Jews between 1949 and 1951
* Gina Waldman, Libyan Jew and current head of JIMENA (Jews Indigenous to the Middle East and North Africa)
* Raphael Israeli, Moroccan Jew and Israeli academic

==See also==
*The David Project Center for Jewish Leadership
*Jewish exodus from Arab lands
*Jewish refugees
*Islam and antisemitism
*Arabs and antisemitism

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 


 