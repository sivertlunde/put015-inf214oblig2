Stuck (2007 film)
{{Infobox film
| name           = Stuck
| image          = Stuckposter07.jpg
| caption        = Theatrical release poster
| director       = Stuart Gordon
| producer       = Jay Firestone Ken Gord Stuart Gordon Robert Katz
| screenplay     = John Strysik
| story          = Stuart Gordon
| starring       = Mena Suvari Stephen Rea
| music          = Bobby Johnston
| cinematography = Denis Maloney
| editing        = Andy Horvitch Amicus Entertainment Prodigy Pictures Grana Pictures
| distributor    = THINKFilm Image Entertainment
| released       =  
| runtime        = 85 minutes
| country        = Canada United States United Kingdom
| language       = English
| budget         = 
| gross          = $146,154 
}} true story. The film premiered on May 21, 2007 at the Cannes Film Market. It was later adapted in Bollywood as Accident on Hill Road starring Celina Jaitley in Mena Suvaris role.

==Plot== Ecstasy before drive home taxi to work, leaving him to die slowly in her garage. When Tom realizes that, he painfully tries to escape.
 burning both Toms corpse and the car.

Rashid returns to the garage, and prepares to shoot the apparently unconscious Tom with a revolver. But Tom, secretly awake, manages to attack Rashid before stabbing him through the eye with a concealed pen, so killing him. During the brawl, a shot is discharged from the gun, prompting Brandi to investigate. Tom tries to start the car, but Brandi enters the garage before he is able to do so. Tom knocks Brandi over and limps out of the garage towards the street, taking Rashids gun with him.

Brandi pursues Tom, wielding a hammer from the garage. Tom pleads with Brandi to stay back, not wanting to fire on her, but she subdues him with the hammer. She drags Tom back to the garage and begins dousing the room with gasoline, panicking. She hopes that the police will conclude that Tom broke in, attacked Rashid and accidentally became engulfed in flames while trying to burn the garage down. However, Tom is able to sneak back into the car, start it and drive forwards, pinning Brandi against the back of the garage.

Brandi begs for help, while Tom steps out of the car and lights a match. He asks why she didnt help him, and Brandi says she doesnt know, and continues to plead for her life. Tom decides to spare Brandi and extinguishes the match. Brandi suddenly produces Rashids revolver and fires on Tom, who ducks beside the car and a stray bullet ignites the gasoline, setting Brandi alight. As she screams in pain, burning to death, Tom struggles to the front of the garage and is able to open the door. Tom stumbles out onto the driveway and collapses as neighbours surround him, alerted by the fire. Tom is helped away and turns to look back at the burning garage, where Brandis screaming has subsided.

==Cast==
* Mena Suvari as Brandi Boski
* Stephen Rea as Thomas "Tom" Bardo
* Russell Hornsby as Rashid
* Rukiya Bernard as Tanya
* Carolyn Purdy-Gordon as Petersen
* Lionel Mark Smith as Sam
* Wayne Robson as Mr. Binckley
* R.D. Reid as Manager
* Patrick McKenna as Joe Lieber
* John Dunsworth as Cabbie

==Production==
The film marks the first production under the newly reformed Amicus Productions.  It was filmed in Saint John, New Brunswick, Canada.

==Release==
The film premiered May 21, 2007 at the Cannes Film Market. It was also shown at the Toronto International Film Festival, Atlantic Film Festival, Edmonton International Film Festival, Wisconsin Film Festival, Philadelphia Film Festival, and RiverRun International Film Festival. Stuck opened in limited release in the United States on May 30, 2008. 

==Reception==
Stuck received generally favorable reviews from critics. The review aggregator Rotten Tomatoes reported that 72% of critics gave the film positive reviews, based on 82 reviews.  Metacritic reported the film had an average score of 61 out of 100, based on 25 reviews. 
 Rea does quite a bit with a role that keeps him face down and bleeding like a stuck pig for most of the movie, but this is definitely Mena Suvari|Suvaris show." 

Robert Wilonsky of the The Village Voice said "Stuck is both darkly comic and disgusting; the name alone reduces the crime to a sick joke."  Joe Leydon of Variety (magazine)|Variety said "Stuck is ingeniously nasty and often shockingly funny as it incrementally worsens a very bad situation, then provides a potent payoff..." Leydon called it a "darkly comical farce" and said it could generate a cult following through a "carefully calibrated theatrical rollout, especially if it generates want-to-see buzz in key regions of the blogosphere." Leydon called the script "crafty" and the director Stuart Gordon "establishes a heightened-reality tone of bleak hilarity early on." Leydon said the film "overall has the look and feel of a tawdry B-movie. Whether thats due to budgetary limitations or artistic inspiration, it serves the material well." 

J.R. Jones of the Chicago Reader said "As the title of this splatter comedy by writer-director Stuart Gordon (Re-Animator) indicates,  s like a bug stuck to her windshield, and thats about the level of humanity and insight one can expect here." 

The film appeared on some critics top ten lists of the best films of 2008. Nathan Rabin of The A.V. Club named it the 7th best film of 2008,  and Scott Tobias of The A.V. Club named it the 10th best film of 2008.     

In The New American Crime Film, scholar Matthew Sorrento describes the film as using "widespread media fodder to create a tale of isolated torment that is, like Edmond, minimalistic and absurdist  moves beneath moral judgment to
depict the humanity of those caught in tough spots." 

==Box office==
The film opened in limited release in the United States on May 30, 2008 and grossed $8,844 in 2 theaters.    It was shown in as many as 16 theaters as of June 6, 2008. 

The film grossed $146,154 worldwide &mdash; $67,505 in the United States and $78,649 in other territories.   

==Awards and nominations==
Director Stuart Gordon won the Silver Raven award at the Brussels International Festival of Fantasy Film and also the Staff Prize for Narrative Feature at the San Francisco Indiefest for the film. 

==Inspiration==
 
Stuck was based on the true story of Chante Jawan Mallard, a Texas woman who received 50 years in prison for the murder of a homeless man, Gregory Biggs. She hit Biggs, allegedly while driving impaired, and he became lodged in her windshield after the collision. She drove home, parked her car in the garage with Biggs still lodged, alive and conscious, in the windshield. While she did go out to apologize to Biggs, she failed to call for any assistance and allowed him to die. Mallard and two accomplices then took the body to a local park, where it was discovered. She was arrested four months later after bragging about the incident at a party. The Fort Worth Medical Examiner and other experts all testified at the trial that basic first responder care would have saved Biggs life.

== See also == The Hitch-hiker, in which a similar vehicular homicide was portrayed where the zombie-like victim followed the driver all the way back to her garage.

==References==
 

==External links==
*   at Cinefantastique Online
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 