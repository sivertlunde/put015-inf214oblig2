Out of the Depths
:for the 1945 film see Out of the Depths (1945 film)
{{Infobox film
| name = Out of the Depths
| caption =
| image	=
| director = Otis B. Thayer, Frank Reicher
| story = Robert Ames Bennett
| screenplay =
| adaptation = Art-O-Graf
| starring =
| music =
| cinematography =
| editing =
| distributor = Pioneer Pictures
| released =  
| runtime =
| language =
| country = United States
| budget =
}}
Out of the Depths  is a 1921 western silent film based on a book by author Robert Ames Bennet and directed by Otis B. Thayer and Frank Reicher, starring Edmund Cobb and Violet Mersereau. The film was shot in Denver, Colorado by Thayers Art-O-Graf film company.  

==Plot==
Two engineers developing irrigation systems for desert land fall for the same girl. One of the men tries unsuccessfully to murder the other man, who eventually is identified as the girls long lost brother.

==Cast==
*Edmund Cobb
*Violet Mersereau

==Crew==
*Otis B. Thayer Managing Director
*Vernon L. Walker Head Camerman
*H. Haller Murphy Cameraman

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 
 