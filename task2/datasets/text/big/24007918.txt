Night Song (film)
{{Infobox film
| name           = Night Song
| image          = Night Song 1948 poster.jpg
| image_size     = 300px
| caption        = 1948 half-height US Theatrical Poster
| director       = John Cromwell
| writer         = Jack Gargan
| narrator       = 
| starring       = Dana Andrews Merle Oberon Ethel Barrymore
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 102 min 
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
 American film John Cromwell and starring Dana Andrews, Merle Oberon and Ethel Barrymore. 

==Plot==
San Francisco wealthy socialite Cathy Mallory is entranced by the music of a nightclub pianist, Dan Evans, who is blind. He is bitter and resents a potential lady bountifuls attempt to become his patron saint.

Next time shes at the club, bandleader Chick Morgan informs her that Dan has quit. Cathy arranges to meet him on a public beach as if by coincidence and introduces herself as Mary Willey, a woman of limited means.They strike up a relationship and Dan explains how he lost his sight from another drivers car crash.

Going to great lengths to continue the ruse, she and longtime companion Mrs. Willey rent an inexpensive apartment. Dan is persuaded to resume writing a piano concerto. Cathy sponsors a $5,000 prize for a contest without telling him, confident Dans music will win. It does and will be performed at Carnegie Hall by the famed pianist Artur Rubinstein.

Dan uses the money to undergo an operation in New York that restores his vision. At the contest, he discovers Cathy provided the prize money but still has no idea that she is also Mary as well.

==Cast==
* Dana Andrews as Dan Evans
* Merle Oberon as Cathy Mallory / Mary Willey
* Ethel Barrymore as Mrs. Willey
* Hoagy Carmichael as Chick Morgan
* Artur Rubinstein as Himself
* Eugene Ormandy as Himself

==Production==
* The working titles of this film were Counterpoint and Memory of Love. 
* RKO borrowed Dana Andrews from Samuel Goldwyns company for the picture.
* Scenes were shot in San Francisco, Trancas Beach and Lake Arrowhead, CA, and in various locations in New York City. 
* Andrews reprised his role in a May 29, 1950 Lux Radio Theatre broadcast, co-starring Joan Fontaine.

==References==
The film recorded a loss of $1,040,000. Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p46 

==References==
 

 

 
 
 
 
 
 


 