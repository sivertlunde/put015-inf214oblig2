Beloved World
{{Infobox film
| name = Beloved World
| image =
| image_size =
| caption =
| director = Emil Burri
| producer =  Curt Prickler 
| writer =  Peter Francke    Emil Burri
| narrator =
| starring = Brigitte Horney   Willy Fritsch   Paul Dahlke
| music = Lothar Brühne   
| cinematography = Franz Weihmayr   
| editing =  Lena Neumann      
| studio =   Bavaria Film 
| distributor = Deutsche Filmvertriebs 
| released =30 October 1942 
| runtime = 89 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Beloved World (German:Geliebte Welt) is a 1942 German romantic comedy film directed by Emil Burri and starring Brigitte Horney, Willy Fritsch and Paul Dahlke. 

==Cast==
*   Brigitte Horney as Karin Ranke  
*Willy Fritsch as Generaldirektor Dr. Blohm  
*Paul Dahlke as Professor Strickbach  
*Else von Möllendorff as Rosi Hübner - Sekretärin 
* Mady Rahl as Beate Kästner  
* Walter Janssen as Direktor Ullmer  
* Elisabeth Markus as Frau Ullmer  
* Hedwig Wangel as Frau Pilz - Haushälterin bei Dr. Blohm  
* Margarete Haagen as Frau Kramer  
* Gustav Waldau as Direktor Steinkopf  
* Karl Blühm as Ingenieur Becker  Klaus Pohl as Bürovorsteher Dohle  
* Trude Haefelin 
*Harry Hardt 
* Erich Dunskus  Adolf Fischer  
* Arthur Wiesner
* Michael von Newlinsky

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 