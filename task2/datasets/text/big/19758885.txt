Good Bad Boys
 
{{Infobox film
| name           = Good Bad Boys
| image          =
| caption        = Edward Cahn
| producer       = Jack Chertok Richard Goldstone for MGM
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Jackson Rose
| editing        = Leon Bourgeau MGM
| released       =  
| runtime        = 10 53"
| country        = United States
| language       = English
| budget         =
}}
 short comedy Edward Cahn.  It was the 192nd Our Gang short (193rd episode, 104th talking short, 105th talking episode, and 24th MGM produced episode) that was released.

==Plot==
Slicker steals an orange from a fruit stand, and Alfalfa is wrongfully accused and punished for it. An angry Alfalfa decides to get even with his parents by embarking upon a life of crime. To that end, he enlists the other kids as his "mob." Hoping to deflect his pals from this drastic action, Spanky McFarland decides to teach the gang a lesson. He tricks the kids into thinking theyre burglarizing a house, when in fact theyre merely helping their neighbor, Mrs. Wilson, clean out her junk. Things take an unexpected turn when a real-life fugitive from justice chooses the gangs clubhouse as his hideout, with the cops hot on his heels. Assuming the police are after them, Alfalfa and the gang confess to their "crime," not knowing what the real crime committed by the real criminal was. The next morning they are arraigned and Spanky comes in with Mrs. Wilson to explain what had really happened. Meanwhile, Slicker is being arraigned with his mother for what seems to be an unrelated crime. For Alfalfa, Spanky, and the rest of the gang, everything is solved.   

==Cast==
===The Gang=== Mickey Gubitosi (later known as Robert Blake) as Mickey
* George McFarland as Spanky
* Carl Switzer as Alfalfa
* Billie Thomas as Buckwheat
* Leonard Landy as Leonard

===Additional cast===
* Freddie Walburn as Slicker
* Hugh Beaumont as Judges assistant Barbara Bedford as Alfalfas mother
* Margaret Bert as Slickers mother
* Byron Foulger as Store owner, Mr. Stephens
* George Lessey as Judge Kincaid Al Hill as Burglar
* Liela McIntyre as Mrs. Wilson
* Roger Moore as Court Official (as Joe Young) William Newell as Alfalfas father
* Harry Strang as Banker
* Emmett Vogan as Police officer

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 