Leap of Faith (film)
{{Infobox Film
| name           = Leap of Faith
| image          = Leap of faith film.jpg
| caption        = Leap of Faith original theatrical poster Richard Pearce
| producer       = Michael Manheim   David V. Picker   (Gospel songs and choir produced by George Duke and choir master Edwin Hawkins.)
| writer         = Janus Cercone
| starring       = Steve Martin Debra Winger Lolita Davidovich Liam Neeson Lukas Haas
| music          = Cliff Eidelman
| cinematography = Matthew F. Leonetti Mark Warner Don Zimmerman
| distributor    = Paramount Pictures
| released       = December 18, 1992
| runtime        = 108 minutes
| country        = United States
| awards         =
| language       = English
| gross          = $23,369,283
| preceded_by    =
| followed_by    =
}} 1992 Cinema American Comedy-drama|dramedy Richard Pearce and starring Steve Martin, Debra Winger, Lolita Davidovich, Liam Neeson and Lukas Haas. The film is about Jonas Nightengale, a fraudulent Christian faith healer who uses his revival meetings, in Rustwater, Kansas, to bilk believers out of their money.

==Plot summary== faith healer, loosely based on televangelist Peter Popoff, who makes a living traveling across America holding revival meetings and conducting miracles. He is helped by his friend and manager Jane Larson (Debra Winger) and an entourage of fellow con artists.

Their bus breaks down in Rustwater, Kansas, a town with a 27 percent unemployment rate that is also suffering a drought and in desperate need of rain to save its crops. While waiting for spare parts, Jonas decides to hold revival meetings just outside of town in an effort to make some money while the truck is being repaired. Early on Jonas meets Marva, a waitress in a local café. She rebuffs his advances, but he keeps trying.

Local sheriff Will Braverman (Liam Neeson) is skeptical and tries to prevent his townspeople from being conned out of what little money they do have. After seeing the smoke and noise of the first show and the counting of money by the team on Jonas tour bus, he decides to investigate his past. He learns that Jonas, claiming to have been born in a humble log cabin in the Appalacian Mountains, is in fact Jack Newton, a native of New York City. Between the age of 15 and 18 he lived a life of crime, including petty theft and possession of marijuana. Braverman publicly shares this information with the townspeople who have gathered for another tent revival. Jonas walks off the stage at first, but then returns and successfully spins it, leaving the crowd more energized than ever, much to Bravermans dismay.

Jonas also gives back the money he collected earlier, plus secretly an additional £60, leaving believers to describe it as a miracle.

Throughout all of this is a subplot involving Jane (the manager) and Braverman (the sheriff), who find themselves falling for each other. She becomes enchanted by Bravermans simple farm life and his penchant for butterflies. However, after Bravermans public disclosure of Jonas past, Jane appears to break off their budding relationship. It doesnt last however - they meet again and at some point Jane confesses to Braverman that she is tired of manipulating people. He makes it clear he would like a permanent relationship with her if she will stay.

Meanwhile, Jonas has begged Marva to tell him why she wont date him. Marva points to her brother Boyd who walks with crutches following the death of their parents in an auto accident. Marva says that doctors cant find anything wrong so she took him to a faith healer, but the man blamed it on Boyd himself. Thus Marva detests faith healers. Their conversation ends but this implies that Boyds disability may be psychosomatic.

During one of their revivals, a huge crucifix is used as a prop. The next morning it is found to have its eyes opened, despite being guarded all night. A shocked Jonas, in front of all the television cameras, proclaims it to be a miracle.

Boyd comes to believe that Jonas can make him walk again. He goes to the revival and implores Jonas to heal him. Jonas finishes the show while pretending not to notice the boy, but is compelled to return to the stage after the crowd begins to chant "one more".

Jonas spins the expected failure to heal Boyd by blaming Braverman, who is present, saying that if a failure occurs, it will be due to Bravermans skepticism. Boyd walks to the open-eyed crucifix and touches the feet of Jesus Christ. He drops his crutches and begins to walk unassisted. The awed crowd sweeps the stage, but after the show, an enraged Jonas rails to Jane that he was conned and that Boyd upstaged him. Jane doesnt believe it was a con. The production crew are thrilled with all the money which came in as a result of the boy being healed and want Boyd to join the show to "testify". A clearly annoyed Jonas says, "fine" and he stalks off the bus. Jane follows him out and argues with Jonas saying, "try dealing with this" and leaves to go be with Braverman.

After the revival, Jonas enters to the empty, darkened tent and mocks the crucifix and the teachings of Christ. Boyd walks in while hes talking. Boyd thanks Jonas for healing him, but Jonas insists angrily that he did nothing. Boyd says it doesnt matter - he still got the job done. Jonas accuses Boyd of being a better con artist than he himself is. Boyd merely says that he wants to join Jonas on the road, saying, "Im handy, I can do a lot of things". Jonas agrees to meet Boyd the following morning, implying Boyd can come. Then Boyds sister Marva arrives. She sends him out of the tent saying that people are looking for him. She thanks Jonas, who tells her that he will not be meeting her brother the next morning. He asks her to tell Boyd that "just because someone doesnt meet you doesnt mean he doesnt care."  (There was a set up earlier in the movie where Jane defended Jonas by telling Braverman the story of a 5 year old Jonas waiting for 4 days for his mother to return, and believing for many years after he went to live in an orphanage that one day she would).

Jonas leaves the tent and sees the crowd that has gathered outside of his tent, many of them praying, others sleeping in groups, and others feeding the crowd that has gathered. He packs a bag and leaves alone under a cover of darkness, leaving behind his silver-sequined jacket with an envelope for Jane containing a ring of his that she had long admired. Braverman and Jane drive to Jonas hotel room and find him gone.

Jonas hitches a ride with a truck driver bound for Pensacola, Florida. When asked by the driver if hes in some kind of trouble, Jonas says, "no, in fact, maybe for the first time in my life."  As they continue to ride along, a third miracle happens: the prayed-for, long-awaited rain has finally come.

As he laughs heartily, Jonass final words as he rides into the sunrise are "Thank you Jesus" - which indicates that for the first time he may have come to terms with the Christianity he had only pretended to preach.

==Selected cast==
* Steve Martin — Jonas Nightengale
* Debra Winger — Jane Larson
* Lolita Davidovich — Marva
* Liam Neeson — Sheriff Will Braverman
* Lukas Haas — Boyd
* Albertina Walker - Lucille
* Meat Loaf — Hoover
* Philip Seymour Hoffman — Matt
* M. C. Gainey — Tiny
* La Chanze — Georgette
* Delores Hall — Ornella
* Phyllis Somerville - Dolores  Troy Evans - Officer Lowell Dade

==Production==
The movie was filmed in Groom, Texas, and Tulia, Texas, though parts of the movie were filmed in Plainview, Texas, where the town water tower still has the fictional town mascot painted on the side. Martin became the films leading actor after Michael Keaton quit the production.   

==Musical adaptation== Broadway by bringing the film to the stage as a musical. The score was written by Alan Menken with lyrics by Glenn Slater. Hackford chose Leap of Faith even though he had previously been offered the opportunity to bring his film Ray to the stage. "What got me here was Alan Menkens score and how it so beautifully fit the book," Hackford said.   According to an article in the January 20, 2010 New York Post, Hackford is no longer the director of this project. 
 Broadway at the St. James Theatre on April 3, 2012, and opened on April 26, 2012 before closing after only 20 performances.  Direction was by Christopher Ashley, choreography by Sergio Trujillo, a revised book by Warren Leight, with a cast featuring Raúl Esparza as Jonas Nightengale, and Jessica Phillips as Marla. {{cite web
  | last = Jones
  | first = Kenneth
  | authorlink = 
  | coauthors = 
  | title = Leap of Faiths Broadway Launch Will Be April 3; Jessica Phillips, Raul Esparza, Kendra Kassebaum Star
  | work = Playbill
  | publisher = 
  | date = January 12, 2012
  | url = http://www.playbill.com/news/article/158551-Leap-of-Faiths-Broadway-Launch-Will-Be-April-3-Jessica-Phillips-Raul-Esparza-Kendra-Kassebaum-Star
  | format = 
  | doi = 
  | accessdate = January 13, 2012}} 

==References==
 

==External links==
*  
*  
*  December 18, 1992
*  by Roger Ebert
*  The Observer
*  BBC 9 Dec 06
* , which formed the basis for this film.
*  by Paul Kurtz
*  by Pat Reeder

 

 
 
 
 
 
 