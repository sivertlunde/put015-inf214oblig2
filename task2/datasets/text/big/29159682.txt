Over the Hill to the Poorhouse
{{Infobox film
| name           = Over the Hill to the Poorhouse
| image          = Over the Hill to the Poorhouse (1920).jpg
| caption        = Film still with Carr and her children
| director       = Harry Millarde William Fox
| writer         = Paul H. Sloane (scenario)
| based on       =  
| starring       = Mary Carr
| music          = Edgar Allen Maurie Rubens Lou Klein (lyrics)
| cinematography = Hal Sintzenich George Schneiderman
| editing        =
| distributor    = Fox Film Corporation
| released       = September 17, 1920
| runtime        = 11 reels 
| country        = United States Silent (English intertitles)
| gross          = $3.0 million 
}} 1920 American silent film about a woman who has a lot of children, and who never gets the chance to enjoy life. The film starred actress Mary Carr and almost all of her real-life children.

The film was directed by Harry Millarde, released by Fox Film Corporation, and was a box office success in 1920. The film was remade as Over the Hill (1931), starring Mae Marsh and as Tears of a Mother (1937). The 1920 silent film is preserved at Bois dArcy in France.  

==Cast==
*Mary Carr - Ma Benton James Sheridan - Child Isaac (billed Sheridan Tansey)
*Noel Tearle - Adult Isaac Stephen Carr - Child Thomas William Welsh
*Jerry Devine - Child John Johnnie Walker - Adult John (billed John Wallker)
*James Sheldon - Child Charles
*Wallace Ray - Adult Charles
*Rosemary Carr - Child Rebecca
*Phyllis Diller - Adult Rebecca (*this Phyllis Diller not the TV Phyllis Diller|comedian)
*Maybeth Carr - Child Susan
*Louella Carr - Adult Susan
*Vivienne Osborne - Isabella Strong
*Dorothy Allen - Agulitia
*Edna Murphy - Lucy
*Joseph Donohoe - Undetermined role
*John T. Dwyer - Adult Thomas

==References==
 

==External links==
 
* 
* 
* 
*  (courtesy Zoverhill)

 
 
 
 
 
 
 
 


 