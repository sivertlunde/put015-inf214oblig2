Lilly Turner
{{Infobox film
| name           = Lilly Turner
| image_size     =
| image	         = Lilly Turner FilmPoster.jpeg
| caption        =
| director       = William A. Wellman
| producer       = Hal B. Wallis (uncredited)
| writer         = Phillip Dunning (play) George Abbott (play) Gene Markey Kathryn Scola
| starring       = Ruth Chatterton George Brent Frank McHugh
| studio         = First National Pictures
| distributor    = First National Pictures The Vitaphone Corporation
| released       =  
| runtime        = 65 minutes
| country        = United States
}}
 1933 Pre-Code melodrama about a woman who marries a Bigamy|bigamist, then a drunk, and falls in love with another man, all while working at a carnival. It was based on the 1932 play of the same name by Phillip Dunning and George Abbott.

==Cast==
*Ruth Chatterton as Lilly Turner
*George Brent as Bob Chandler
*Frank McHugh as Dave Dixon
*Guy Kibbee as Doc Peter McGill
*Robert Barrat as Fritz
*Ruth Donnelly as Edna Yokum
*Marjorie Gateson as Mrs. McGill
*Gordon Westcott as Rex Durkee
*Arthur Vinton as Sam Waxman Grant Mitchell as Dr. Hawley
*Margaret Seddon as Mrs. Turner
*Mae Busch as Hazel

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 


 