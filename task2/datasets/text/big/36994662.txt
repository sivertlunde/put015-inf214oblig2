The Company She Keeps
{{Infobox film
| name           = The Company She Keeps
| image          = John Cromwell
| producer       = John Houseman
| writer         = Ketti Frings
| starring       = Lizabeth Scott Jane Greer
| music          =
| cinematography =
| editing        =
| distributor    = RKO Radio Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Company She Keeps is a 1951 film drama starring Lizabeth Scott, Jane Greer, and Dennis OKeefe.
 John Cromwell, whose film the previous year, Caged, also concerned a woman sent to prison.

This was Jeff Bridges first film.  He was only a year old at the time of the films release (he was born on  ).
==Plot==
Released from prison after serving two years on a check-forging charge, Mildred Lynch changes her name to Diane Stuart and moves to Los Angeles.

Parole officer Joan Willburn finds her a job at a hospital. Diane repays her by stealing Joans boyfriend, Larry Collins, after he comes to the hospital to visit a patient.

Diane hides the relationship from Joan and hides her past from Larry. Once she finally finds out, Joan graciously accepts the new relationship but warns Diane that to get married, she must first seek approval from the parole board, which will be under a legal obligation to contact Larry.

Despite all the help Joan has been, Diane accuses her of trying to sabotage her romance and also her parole, after Diane is arrested for a drug theft at the hospital for which ex-convict Tilly Thompson is responsible. She runs away until Larry lets her know that, thanks to Joan, the charges have been dismissed.

==Cast==
* Lizabeth Scott as Joan
* Jane Greer as Diane
* Dennis OKeefe as Larry
* Fay Baker as Tilly
* Irene Tedrow as Mrs. Seeley

==Reception==
The film recorded a loss of $315,000. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982 p256 

==References==
 
==External links==
*  at IMDB

 

 
 
 