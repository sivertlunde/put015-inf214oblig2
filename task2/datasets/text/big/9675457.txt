Shakalaka Boom Boom
 
 
{{Infobox film
| name = Shakalaka Boom Boom
| image = ShakalakaBoomBoom.jpg
| image_size =
| caption = Movie poster for Shakalaka Boom Boom
| director = Suneel Dharshan
| producer = Suneel Dharshan
| writer = Anurag Kashyap
| starring = Bobby Deol Upen Patel Kangna Ranaut Celina Jaitley language = Hindi
| overseas gross = 
| gross = 6 crores
| budget =
| awards =
}}
 thriller drama Anurag Kashyap. The film stars Bobby Deol, Upen Patel, Celina Jaitley and Kangna Ranaut in the lead. It released on 6 April 2007.
 Bobby Deols role as a first-time villain. The movie worked for the musical fans.

==Plot summary==
Shakalaka Boom Boom follows the tale of a jealous, selfish and greedy music artist, AJ (Bobby Deol). AJ is one of the finest music artists in the industry, and is currently under a stop since he cant think of a new project. AJ is in love with the hot and sexy Ruhi (Kangna Ranaut) and hopes to tell her how he feels. However, a wannabe singer, Reggie (Upen Patel) appears who falls in love with Ruhi and woos her before AJ can. 
Therefore, AJ swears to destroy Reggies career, and hence comes into Reggies life as his friend. Getting him drunk, getting him smoking, is all that AJ has been doing to Reggie, and Reggie even loses control and passes out. One day, AJ finds out all Reggies secrets, and gets him so drunk that he has liver-fail. Reggie in the state of dying, AJ takes all his music-notes and beats, and flees from the place. Then Ruhi shows up and takes him to the hospital. He is placed into the operation section duet to the failure of his liver, and then onwards, Ruhi plans to destroy AJs career just like he did to Reggie.

Though, Ruhi does not know that AJ isnt alone, he also has his hidden agenda with Reggies ex-girlfriend Sheena (Celina Jaitley) who is now a bigshot due to AJ. AJ and Sheena together publish Reggies music at their own, and it goes onto becoming a big hit. At the music-signing, Ruhi gets her gun out, though it doesnt seem to work. She seems thats it, though Karma has a different plan in mind. Due to her gun not working, she leaves and as she leaves, a disco ball randomly falls on top of AJs head. He is placed into the hospital, and the doctor declares him as he is now deaf, and cant hear any music anymore. The ending shows him going to hell, and Reggie waking up to a better life, as he and Ruhi have now proved that the music is really his.

== Cast ==
* Bobby Deol as A.J.
* Upen Patel as Reggie Kapoor
* Kangna Ranaut as Ruhi 
* Celina Jaitley as Sheena
* Asrani as Yogra
* Dalip Tahil
* Anupam Kher
* Govind Namdeo as Guruji
* Vivek Vaswani

==Box office==
Because it had a controlled budget the film didnt do too badly but only grossed about 4.5 crores in the first week and its total net gross was 6 crores altogether. Boxoffice-india declared the film a flop.

==Reception==

===Critical reception=== DNA gave movie a one and half stars and wrote in his review, "Its simple — sexual innuendo, potshots at popular films, bad mimicry, foreign locations, a generous dose of overacting, an item song and a gora villain. And as he magnificently presents the climax of the film. Spare us the comedy, please?" Nikhat Kazmi of Times of India said, "This ones definitely not for the fastidious, choosy viewer but for those who dont mind losing it for a bit, Shakalaka Boom Boom works like an average Bollywood musical. Performance-wise, its one big circus with the guys hogging most of the limelight. The girls &mdash; Kangna and Celina &mdash; are mere confetti" and gave it 3 out of 5 stars. Taran Adarsh also gave it 3 out of 5 stars, saying "Its a well-crafted entertainer and lives up to the expectations of its target audience &mdash; the youth. At the box-office, its business at the multiplexes will help it generate good revenue, making it a profitable proposition for its investors."

== External links ==
*  

 
 
 