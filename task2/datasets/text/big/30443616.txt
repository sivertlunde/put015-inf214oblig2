Seburi monogatari
{{Infobox film
| name           = Seburi monogatari
| image          = 
| image size     = 
| caption        = 
| director       = Sadao Nakajima
| producer       = Tatsuo Honda Masao Sato
| writer         = Sadao Nakajima
| starring       = Kenichi Hagiwara
| music          = 
| cinematography = Fuminori Minami
| editing        = 
| distributor    = 
| released       = 11 May 1985
| runtime        = 125 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
 1985 cinema Japanese drama film directed by Sadao Nakajima. It was entered into the 35th Berlin International Film Festival.   

==Cast==
* Kenichi Hagiwara as Hajime Kinoshita
* Yumiko Fujita as Kuni
* Michiko Kohno as Hide
* Takashi Naitô as Kazuo
* Ai Saotome as Hana
* Eiko Nagashima as Mitsu
* Taiji Tonoyama as Kamezo
* Ken Mitsuishi as Jiro
* Hideo Murota as Kuzushiri
* Rei Okamoto as Tome
* Asao Uchida as Ayutachi
* Nenji Kobayashi as Army
* Yoshio Ichikawa as Imasuke

==References==
 

==External links==
* 

 
 
 
 
 
 
 