Fool's Gold (2008 film)
{{Infobox film
| name           = Fools Gold
| image          = Fools gold 08.jpg
| director       = Andy Tennant
| producer       = Donald De Line
| writer         = Andy Tennant John Claflin Daniel Zelman
| starring       = Matthew McConaughey Kate Hudson Alexis Dziena Ewen Bremner Ray Winstone Donald Sutherland 
| music          = George Fenton
| cinematography = Don Burgess
| editing        = Troy Takaki Tracey Wadmore-Smith
| distributor    = Warner Bros.
| released       =  
| runtime        = 113 minutes
| country        = United States Australia
| language       = English
| budget         = $70 million
| gross          = $111,231,041
}} Warner Bros. Pictures about a recently divorced couple who rekindle their romantic life while searching for a lost treasure. The film was directed by Andy Tennant and reunites the How to Lose a Guy in 10 Days stars Matthew McConaughey and Kate Hudson.

==Plot== steward on Kevin Hart) and Finns mentor, Moe Fitch (Ray Winstone), are intent on finding the treasure first.
 pontoon as the plane takes off. As Bigg Bunny attempts to shoot Finn, Tess kicks Bigg Bunny out of the plane, sending him into the ocean. Bigg Bunnys henchman is then taken prisoner by Moe, after Moe has been shot in the leg with a speargun.

Finn and Tess are reunited and find the treasure together. Tess is shown to be pregnant. Eventually Finn, Tess, Nigel, Gemma, Moe and all those who contributed in finding the treasure open a museum to display the find.

==Cast==
* Matthew McConaughey as Ben Finn Finnegan
* Kate Hudson as Tess Finnegan
* Donald Sutherland as Nigel Honeycutt
* Alexis Dziena as Gemma Honeycutt
* Ray Winstone as Moe Fitch Kevin Hart as Bigg Bunny Deenz
* Ewen Bremner as Alfonz
* Brian Hooks as Curtis
* Malcolm-Jamal Warner as Cordell
* Michael Mulheren as Eddie
* Adam LeFevre as Gary
* Rohan Nichol as Stefan David Roberts as Cyrus
* Roger Sciberras as Andras

==Production== Port Douglas. Gold Coast, Hamilton Island, Lizard Island, Airlie Beach, Hervey Bay.  Scenes were also filmed at Batt Reef, where Steve Irwin died from a stingray barb in 2006.   
 python in the backyard of his house in Port Douglas. McConaughey said, "There were other days like the day we went out diving and swam with a dugong, which was very cool." 

Two crew members were stung by Irukandji jellyfish during filming, so some of the water scenes were shot in the Caribbean because the actors were so frightened. 

At the time of filming, The Precious Gem luxury motor yacht in the film was called the Keri Lee and has subsequently been renamed "Penny Mae".  It was designed by yacht architect Ward Setzer of Setzer Design Group and originally named Status Quo. It is privately owned and operated by Lee Group Charters. 

The yachts tender was a 2007    . 

===2011 lawsuit===
Warner Brothers Entertainment, Inc., was sued in 2011 by Canadian novelist Lou Boudreau, in Canadian court, alleging copyright infringement by Tennant and two other men over the authorship of the script.    Warner Brothers did not comment on the matter. 

==Release==

===Box office===
Fools Gold was released on February 8, 2008, in the North America and grossed $21.5 million in 3,125 theaters its opening weekend, ranking #1 at the box office.   , the film grossed over $110.5 million worldwide &mdash; $70.2 million in the North America and $40.3 million in other territories.   

===Critical response===
The film received overwhelmingly negative reviews from critics.  , the review aggregator Rotten Tomatoes reported that 11% of critics gave the film positive reviews, based on 143 reviews.  Metacritic reported the film had an average score of 29 out of 100, based on 28 reviews. 
 National Treasure     and Romancing the Stone.    Some critics referred to the film as "tedious"      and "listless."   

Peter Travers of Rolling Stone gave the film zero stars out of four and said "Paris Hiltons appalling The Hottie and the Nottie is "marginally better." Travers wrote "I defy any 2008 comedy to be as stupid, slack and sexless" as Fools Gold.   

Lou Lumenick of the New York Post gave the film one star out of four and called it "excruciatingly lame." Lumenick said "Its all basically an excuse to show off the scenery", including McConaugheys abs.   

Carrie Rickey of The Philadelphia Inquirer gave the film one and a half stars out of four and said it "plays like a Three Stooges movie with scuba gear.", but that "a Three Stooges movie is enlightened next to this one." Rickey described McConaughey as "perennially shirtless" and Hudson as "peculiarly mirthless."   

Pete Vonder Haar of Film Threat gave the film one and a half stars and said "the resolution is never in doubt, the villains are comedic rather than menacing, and no one involved seems to care one way or the other that their names are attached to this indifferent mess." Vonder Haar said McConaughey plays Finn "as Sahara (2005 film)|Saharas Dirk Pitt minus the SEAL training and a few million brain cells." and asked "Does McConaughey have some codicil in his contract stipulating he must spend at least 51% of a movie shirtless?"   

Sid Smith of the Chicago Tribune gave it two stars out of four and said the characters "are comic book clichés." Smith said "the outcome is predictable" and "The wasted talents include Sutherland, affecting a hokey British accent, and hatchet-faced Ewen Bremner."   
 The Deep, only without the comedy." Lowry said the tropic scenery was well-shot but said "theres not much chemistry" between McConaughey and Hudson.   

Carina Chocano of the Los Angeles Times called it a "cheesy, familiar bore" and said it "feels at times like a third-rate Bond movie set to a Jimmy Buffett album." Chocano said "Hudson is the best thing about the movie. She has a likable, grounded presence and sharp comic timing."   

Nathan Rabin of The A.V. Club gave the film a "C+" and called it "the kind of thing people watch because its the in-flight movie." Rabin called the repeated mentions of Finns sexual prowess "a delightfully unnecessary move." Rabin said the film "outstays its welcome by a good 20 minutes" and called it "extravagantly stupid", but that the films strengths were the "photogenic locales, obscenely beautiful stars, a laid-back soundtrack" and an unwillingness to take itself seriously.   

Lou Lumenick said the ending was "surprisingly bloody"  and Brian Lowry said the ending is "a little more violent than necessary" and "a bit grittier than it should be tonally, as if weve detoured into a different movie." 

Simon Braund of Empire (magazine)|Empire magazine gave the film one star out of five and called it "Absolute tosh. A ridiculous, unerringly tedious plot is weighed down by listless performances from a cast who clearly wished they were somewhere else, despite the sumptuous location."   
 My Best Friends Girl). 

===Home media===
Fools Gold was released on DVD and Blu-ray Disc|Blu-ray discs on June 17, 2008.  About 1,225,904 DVD units have been sold, acquiring revenue of $20,502,574. This does not include Blu-ray sales. It was presented in anamorphic widescreen with an English-language 5.1 digital surround soundtrack. The extras for the DVD include Flirting with Adventure McConaughey-Hudson chemistry featurette, and a gag reel.  Fools Gold was released on R4 Australian DVD on June 5, 2008. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 