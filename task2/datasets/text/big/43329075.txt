Happiness Is the Main Thing
{{Infobox film
| name = Happiness is the Main Thing
| image =
| image_size =
| caption =
| director = Theo Lingen
| producer = Ernst Rechenmacher   Heinz Rühmann Walter Forster  Rudo Ritter
| narrator =
| starring = Heinz Rühmann   Hertha Feiler   Ida Wüst     Hans Leibelt
| music = Werner Bochmann   
| cinematography = Oskar Schnirch   
| editing =  Hilde Grebner      
| studio = Bavaria Film 
| distributor = Bavaria Film
| released =3 April 1941 
| runtime = 92 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Happiness is the Main Thing (German:Hauptsache glücklich) is a 1941 German comedy film directed by Theo Lingen and starring Heinz Rühmann, Hertha Feiler and Ida Wüst.  Rühmann plays a recently married employee of a large company.

==Cast==
*  Heinz Rühmann as Axel Roth  
* Hertha Feiler as Uschi Roth  
* Ida Wüst as Frau Lind  
* Hans Leibelt as Generaldirektor Arndt 
* Arthur Wiesner as Standesbeamter  
* Jane Tilden as Liselotte / Daisy  
* Fritz Odemar as Generaldirektor Zimmermann 
* Max Gülstorff as Bürovorsteher Binder 
* Hilde Wagener as  Frau Bertyn  
* Arthur Schröder as Rechtsanwalt Mohrig  
* Annemarie Holtz as Betty Arndt  
* Karl Etlinger as Juwelier 
* Ernst G. Schiffner as Steuerbeamter  
* Hans Paetsch as Unverheiratetet Kollege  
* Hilde Sessak
* Theo Shall
* Hans Zesch-Ballot

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 