Lonely Boy (film)
{{Infobox film
| name           = Lonely Boy |image_size=
| image          = LonelyBoy.jpg
| caption        = VHS cover
| director       = Wolf Koenig Roman Kroitor
| producer       = Roman Kroitor
| writer         = 
| narrator       = 
| starring       = Paul Anka
| music          = 
| cinematography = 
| editing        = John Spotton
| distributor    = National Film Board of Canada (NFB)
| released       = 1962
| runtime        = 27 min.
| country        = Canada English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Lonely Boy," which he performs to screaming fans in the film. This short documentary is also unique for its use of hand-held cameras to record intimate backstage moments.

Co-directed by Roman Kroitor and Wolf Koenig, this National Film Board of Canada production won a Canadian Film Award as top film of the year and was nominated at the BAFTA Awards for its best short film prize.

== Influence ==
Lonely Boy proved to be of substantial influence on the  .   

== References ==

 

==External links==
* 
* 
* 
* 

 
 
 
 

 

 

 
 
 
 
 
 
 
 
 
 
 


 
 