Sati (film)
{{Infobox film 
| name           = Sati
| image          = Satishabana.jpg 
| image_size     =
| caption        =DVD cover story           = Kamal Kumar Majumdar screenplay      =Aparna Sen Arun Banerjee
| starring       = Shabana Azmi producer        =NFDC
| director       = Aparna Sen  music           = Chidananda Das Gupta Chandan Raichaudhri
|editing= Shaktipada Roy
| distributor    =  cinematography   = Ashok Mehta
| released       =    	
| country        = India
| language       = Bengali
| budget         =                    
| runtime        =140 minutes
}}
 Bengali film 1989 written and directed by  Aparna Sen. Based on a story by Kamal Kumar Majumdar,  the film is about mute orphan girl who is married to a Banyan tree because her horoscope suggests that she would be a Sati (practice)|sati, and her husband would die. The film had Shabana Azmi and Arun Banerjee in lead roles. 
 Bengali cinema to explore gender issues and feminist perspective.       


== Synopsis == 
The young Brahmin girl (Shabana Azmi) in this story has a disastrous horoscope. In an Indian village in 1828, this can be a real handicap. The fact that she is mute only compounds her difficulties. Her horoscope predicts that she will become a widow at an early age. If this turns out as predicted, in addition to being bad luck for her prospective husbands, it is bad luck for her, as she will, according to the customs of the time, have to commit suttee, Sati (practice)|sati. That means she will have to be burned alive on her husbands funeral pyre. To avoid this fate, her family has hit upon the appealing strategem of having her marry a banyan tree. 

==Cast==
* Shabana Azmi as ... Uma
*  Arun Banerjee
* Kali Bannerjee
* Pradip Mukherjee
* Anndam Ganguli 
* Shakuntala Barua

== References ==

 
==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 