Kiko and the Honey Bears
 
{{Infobox Hollywood cartoon|
| cartoon_name = Kiko and the Honey Bears
| series = Kiko the Kangaroo
| image = 
| caption = 
| director = Mannie Davis George Gordon
| story_artist = Joseph Barbera
| animator = 
| voice_actor = 
| musician = 
| producer = Paul Terry
| studio = Terrytoons
| distributor = 20th Century Fox
| release_date = August 21, 1936
| color_process = Black and white
| runtime = 5:48 English
| preceded_by = Farmer Al Falfas Prize Package
| followed_by = Kiko Foils the Fox
}}

Kiko and the Honey Bears is a short animated film created at Terrytoons, and distributed by 20th Century Fox. It is the second film to feature Kiko the Kangaroo as well as the characters first solo film.

==Plot==
When a mother bear gets burdened by the pesky antics of her cubs, she places a sign in front of her house to hire a caretaker. Immediately Kiko, who is passing by, reads the ad and takes the job.

Kiko plays with the cubs in the woods. The ways he assists them include being their diving board and seesaw. After such a play, the kangaroo takes a nap under a tree. Moments later, a hunter and pack of hounds come to the woods. The cubs attempt to run and hide from their pursuers. When the hunter fires a gun, Kiko is awakened and very surprised. Kiko brawls with the hunter and the hounds, knocking the pursuers out of the scene. He then takes the cubs and leaves.

Kiko returns to the home of the cubs, much to the delight of the mother bear. Singing in the melody of Kikos theme song, the cubs tell their mother how Kiko saved them in woods.

==External links==

*  at the Big Cartoon Database

 
 
 
 
 
 

 