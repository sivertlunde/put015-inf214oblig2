Scandal Makers
{{Infobox film
| name           = Speed Scandal 
| image          = Speedy Scandal film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| hanja          =  스캔들
| rr             = Gwasok Seukaendeul
| mr             = Kwasok Sŭkaendŭl}}
| director       = Kang Hyeong-cheol
| producer       = Ahn Byeong-ki Sin Hye-yeon
| writer         = Kang Hyeong-cheol
| starring       = Cha Tae-hyun Park Bo-young
| music          = Kim Jun-seok
| cinematography = Kim Jun-young
| editing        = Nam Na-yeong
| distributor    = Lotte Entertainment
| released       =  
| runtime        = 108 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =    . Box Office Mojo. Retrieved 4 March 2012. 
}} 2008 South highest grossing Korean film of the year.

==Plot==
Former teen idol Nam Hyeon-soo (Cha Tae-hyun) who thinks he is pretty popular is now in his thirties and working as a radio DJ. A young woman named Hwang Jeong-nam (Park Bo-young) sent stories about her being a single mother to the radio station Hyeon-soo worked at, telling him she is going to meet her father. When he finds out that he is the father of Jeong-nam when she comes to his apartment with her son Ki-Dong (Wang Seok-hyeon). He also finds out that Jeong-nam was his first loves name and the other Jeong-nams real name is Jae-in. Jae-in and her son, Ki-dong. Not believing it, he goes through a DNA blood test,and the results say that theyre related. Jae-in wants to perform on stage singing, but Hyeon-soo thinks this will reveal his scandal with whats happening to Jae-In, Ki Dong, and himself. Ki-dongs dad Park Sang-yoon (Im Ji-kyu) finds Jae-in, when she started getting popular with her singing. They meet and chat, before Sang-yoon thought Jae-in was in a relationship with Hyeon-soo. Later at Jae-ins performance, Ki-dong went missing, which brought Hyeon-soo to realize that he really did care for his daughter and grandson. He gave up some of his job to be with his family and started to really love his daughter and grandson and cared for them.

==Cast==
*Cha Tae-hyun as Nam Hyeon-soo
*Park Bo-young as Hwang Jeong-nam
*Wang Seok-hyeon as Hwang Ki-dong
*Im Ji-kyu as Park Sang-yoon, Jeong-nams first love
*Hwang Woo-seul-hye as Jo Mo, kindergarten teacher
*Im Seung-dae as Bong Pil-joong, entertainment reporter
*Jeong Won-joong as Bureau director
*Kim Ki-bang as Director
*Park Yeong-seo as Assistant director
*Seong Ji-roo as Lee Chang-hoon (cameo)

==Release== topped the The Good, The Chaser with roughly 5 million tickets sold. Admissions as of 1 June 2008.  , HanCinema. Retrieved on 28 September 2008. 

==Awards and nominations==
2009 Max Movie Awards
*Best New Actress - Park Bo-young

2009 Korea Junior Star Awards
*Grand Prize (Film category) - Park Bo-young
 2009 Baeksang Arts Awards
*Best New Actress - Park Bo-young
*Best Screenplay - Kang Hyeong-cheol
*Most Popular Actress - Park Bo-young
*Nomination - Best Film
*Nomination - Best New Director - Kang Hyeong-cheol

2009 Udine Far East Film Festival
*Audience Award (2nd place)

2009 Shanghai International Film Festival
*Best Film (Asian New Talent Award)

2009 Grand Bell Awards
*Popularity Award - Park Bo-young
*Nomination - Best New Actress - Park Bo-young
*Nomination - Best Editing - Nam Na-yeong
*Nomination - Best Music - Kim Jun-seok
*Nomination - Best Lighting - Lee Seong-jae 

2009 Korean Association of Film Critics Awards
*Best Director - Kang Hyeong-cheol
*Best New Actress - Park Bo-young
 2009 Blue Dragon Film Awards
*Best New Actress - Park Bo-young
*Best New Director - Kang Hyeong-cheol
*Nomination - Best Music - Kim Jun-seok

2009 Korean Culture and Entertainment Awards
*Best New Actress (Film) - Park Bo-young

2009 University Film Festival of Korea Awards
*Best New Actress - Park Bo-young

2009 Golden Cinematography Awards
*Best New Actress - Park Bo-young
*Best New Cinematographer - Kim Jun-young

2009 Directors Cut Awards
*Best New Actress - Park Bo-young

==References==
 

==External links==
*   
* 
* 
* 
* 
* 
* 

 
 
 