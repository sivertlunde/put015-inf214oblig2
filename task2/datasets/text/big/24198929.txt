Dangerous Crossing
{{Infobox film
| name           = Dangerous Crossing
| image          = Dangerous Crossing poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Joseph M. Newman
| producer       = Robert Bassler
| screenplay     = Leo Townsend
| based on       =  
| narrator       =
| starring       = Jeanne Crain Michael Rennie
| music          = Lionel Newman
| cinematography = Joseph LaShelle
| editing        = William H. Reynolds
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = $500,000
| gross          =
}} mystery film, directed by Joseph M. Newman and starring Jeanne Crain and Michael Rennie, based on the 1943 play Cabin B-13 by John Dickson Carr. 

==Plot== Mary Anderson), and second officer Jim Logan (Max Showalter). When she talks to the captain (Willis Bouchey), he notices that Ruth isnt even wearing a wedding ring, and the crew begins to suggest that she is mentally unbalanced.

That night, John calls Ruth with a cryptic warning not to trust anyone. A divorcee traveling solo (Marjorie Hoshelle) and the stewardess take an interest in Ruth. And Dr. Manning (Michael Rennie) spends time with her, assuming a clinical demeanor and getting her to open up about the recent death of her father, a wealthy steel executive.

Ruth decides to put on an act and agree that shes been foolish, but mysterious things continue to happen. Ruth and Dr. Manning get closer, and a man who walks with a cane seems to stalk her.

Then the stewardess is revealed as conspiring with someone (by phone) to make Ruth seem unstable. Dr. Manning confronts Ruth over the fact that her marriage was either secret or non-existent. She explains that John wanted it to be quick and quiet and talks about an uncle who might scheme to get her inheritance.

John calls again and asks to meet Ruth on deck but runs into the fog when he hears others approach. When Ruth makes a scene after dinner, the captain demands that she be locked in her cabin.

Then John is revealed to be Barlowe, the third mate, under Dr. Mannings care all along for a claimed illness. When he learns Ruth has been locked in, he asks the stewardess to enable her escape. When they meet again, John attempts to throw Ruth overboard (mentioning money as a motive) but is stopped by Dr. Manning, who has followed her. It is John who goes overboard in the fight.

Later, Dr. Manning comforts Ruth, and the captain apologizes and explains that the stewardess confessed.

==Cast==
* Jeanne Crain as Ruth Stanton Bowman
* Michael Rennie as Dr. Paul Manning
* Max Showalter as Jim Logan (as Casey Adams)
* Carl Betz as John Bowman Mary Anderson as Anna Quinn
* Marjorie Hoshelle as Kay Prentiss
* Willis Bouchey as Capt. Peters
* Yvonne Peattie as Miss Bridges

==Reception==

===Critical response===
The film critic for The New York Times gave a lukewarm review, writing, "Although it maintains an eerie quality and suspense through the first half of its footage, Dangerous Crossing, which arrived at the Globe yesterday, is only a mildly engrossing adventure ... While sound effects, background music and shipboard sets lend a peculiar fascination to the melodrama, the acting of the cast adds little tautness to the proceedings. As the beleaguered heiress Jeanne Crain is beautiful but not entirely convincing in the role ... Dangerous Crossing, in effect, is intriguing only part of the way. Thereafter, it is a commonplace trip. 

==References==
 

 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 