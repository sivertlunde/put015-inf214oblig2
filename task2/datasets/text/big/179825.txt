Top Gun
 
{{Infobox film
| name           = Top Gun
| image          = Top Gun Movie.jpg
| caption        = Theatrical release poster
| director       = Tony Scott
| producer       = Don Simpson Jerry Bruckheimer
| writer         = Jim Cash Jack Epps, Jr.
| starring       = {{plainlist|
* Tom Cruise
* Kelly McGillis
* Val Kilmer
* Anthony Edwards
* Tom Skerritt
}} 
| music          = Harold Faltermeyer
| cinematography = Jeffrey L. Kimball
| editing        = Chris Lebenzon Billy Weber Bruckheimer
| distributor    = Paramount Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $356.8 million 
}}
 action drama film directed by Tony Scott, and produced by Don Simpson and Jerry Bruckheimer, in association with Paramount Pictures. The screenplay was written by Jim Cash and Jack Epps, Jr., and was inspired by the article "Top Guns" written by Ehud Yonay for California (magazine)|California magazine.
 Lieutenant Pete Fighter Weapons Miramar in San Diego.

==Plot== John Stockwell) CAG Tom Fighter Weapons School, known as "Top Gun", at NAS Miramar.
 Phantom was approaches a woman named Charlotte "Charlie" Blackwood (Kelly McGillis), who he later learns is a civilian contractor with a Doctor of Philosophy|Ph.D. in astrophysics serving as a Top Gun instructor.
 Lieutenant Commander Commander Mike "Viper" Metcalf (Tom Skerritt). Maverick continues to pursue Charlie and becomes a rival to top student Lieutenant Tom "Iceman" Kazanski (Val Kilmer), who considers Mavericks methods dangerous and unsafe. Although outwardly critical of Mavericks tactics, Charlie eventually admits that she admires his flying but was critical because she was afraid for her credibility. They begin a romantic relationship.

During one flight, Maverick goes one-on-one with Viper. Although Maverick matches the older pilot move for move, Viper lasts long enough for Jester to maneuver around and "shoot" Maverick down, demonstrating the value of teamwork over individual ability.
 jet wash flat spin from which he cannot recover, forcing both Maverick and Goose to ejection seat|eject. Goose ejects directly into the jettisoned aircraft canopy, which breaks his neck, killing him.
 board of inquiry clears Maverick of responsibility, he feels guilty for Gooses death, losing his aggressiveness when flying. Charlie and others attempt to console him, but Maverick considers leaving the Navy. Unsure of his future, he seeks Vipers advice. Viper reveals that he served with Mavericks father and discloses classified details over his last mission, explaining how Duke stayed in the fight after his Phantom was hit and saved three planes before he died. Information about the dogfight was classified to avoid revealing that the American planes were not where they should have been.
 catapult failure and nearly retreats after encountering circumstances similar to those that caused Gooses death. Upon rejoining Iceman, they shoot down four MiGs and force the other two to flee, and return to the Enterprise, where the two men, with newfound respect for each other, finally become friends. Offered any assignment he chooses, Maverick decides to return to Top Gun as an instructor. Later, he tosses Gooses dogtags into the ocean, finally freeing himself of guilt over Gooses death.

Sitting alone in a restaurant in downtown San Diego, Maverick hears "Youve Lost That Lovin Feelin" playing on the jukebox and recalls meeting Charlie. She reveals that she is in the bar and the two reunite.

==Cast==
* Tom Cruise as LT Pete "Maverick" Mitchell
* Kelly McGillis as Charlotte "Charlie" Blackwood
* Val Kilmer as LT Tom "Iceman" Kazanski
* Anthony Edwards as LTJG Nick "Goose" Bradshaw
* Tom Skerritt as CDR Mike "Viper" Metcalf
* Michael Ironside as LCDR Rick "Jester" Heatherly John Stockwell as LT Bill "Cougar" Cortell
* Barry Tubb as LTJG Leonard "Wolfman" Wolfe
* Rick Rossovich as LTJG Ron "Slider" Kerner
* Tim Robbins as LTJG Samuel "Merlin" Wells
* Clarence Gilyard, Jr. as LTJG Marcus "Sundown" Williams
* Whip Hubley as LT Rick "Hollywood" Neven
* James Tolkan as CDR Tom "Stinger" Jordan
* Meg Ryan as Carole Bradshaw
* Adrian Pasdar as LT Charles "Chipper" Piper
* Duke Stroud as Air Boss CDR Johnson
* Linda Rae Jurgens as Mary Metcalf

==Production==

===Background===
The primary inspiration for the film was the article "Top Guns", by Ehud Yonay, from the May 1983 issue of California magazine, which also featured aerial photography by then-Lieutenant Commander Charles "Heater" Heatley.    The article detailed the fighter pilots at the Miramar Naval Air Station, located in San Diego, self-nicknamed as "Fightertown USA". Numerous screenwriters allegedly turned down the project.  Bruckheimer and Simpson went on to hire Jim Cash and Jack Epps, Jr., to write the first draft. The research methods, by Epps, included an attendance at several declassified Top Gun classes at Miramar and gaining experience by being flown in an F-14. The first draft failed to impress Bruckheimer and Simpson, and is considered to be very different from the final product in numerous ways. 
 LT Pete "Maverick" Mitchell (that went to Tom Cruise) because he felt the films pro-military stance went against his politics. 
 Christine "Legs" Fox, a civilian mathematician employed by the Center for Naval Analyses as a specialist in Maritime Air Superiority (MAS), developing tactics for aircraft carrier defense.     Rear Admiral Pete "Viper" Pettigrew, a former Navy aviator, Vietnam War veteran, and Top Gun instructor served as a technical advisor on the film, and also made a cameo appearance in the film as a colleague of Charlies.
 Randy "Duke" Cunningham claimed to have been the inspiration for Pete Mitchell, although the films producers have denied that the character was based on any specific Naval aviator. 

===Filming===
  USS Enterprise (CVN-65), showing aircraft from F-14 squadrons VF-114 Aardvarks and VF-213 Black Lions.  The majority of the carrier flight deck shots were of normal aircraft operations and the film crew had to take what they could get, save for the occasional flyby which the film crew would request. During filming, director Tony Scott wanted to shoot aircraft landing and taking off, back-lit by the sun. During one particular filming sequence, the ships commanding officer changed the ships course, thus changing the light. When Scott asked if they could continue on their previous course and speed, he was informed by the commander that it cost $25,000 to turn the ship, and to continue on course. Scott wrote the carriers captain a $25,000 check so that the ship could be turned and he could continue shooting for another five minutes. 
 NAS Fallon, in Nevada, using ground-mounted cameras. Air-to-air shots were filmed using a Learjet. Grumman, manufacturer of the F-14, was commissioned by Paramount Pictures to create camera pods to be placed upon the aircraft that could be pointed toward either the front or rear of the aircraft providing outside shots at high altitude.  
 flat spin, Pitts S-2 Carlsbad on September 16, 1985. Neither Scholls body nor his aircraft were recovered, leaving the official cause of the accident unknown.  Top Gun was dedicated to the memory of Art Scholl. 

===Music===
  Danger Zone". Berlin recorded the song "Take My Breath Away", which would later win numerous awards, sending the band to international acclaim. After the release of Logginss single "Danger Zone", sales of the album exploded, selling 7 million in the United States alone. On the re-release of the soundtrack in 2000, two songs that had been omitted from the original album (and had been released many years before the film was made), "Great Balls of Fire" by Jerry Lee Lewis and "Youve Lost That Lovin Feelin" by The Righteous Brothers, were added. The soundtrack also includes "Top Gun Anthem|Top Gun Anthem" and "Memories" by Steve Stevens/Faltermeyer and Faltermeyer. However, no soundtrack release to date has included the full Faltermeyer score. 
 Toto was originally meant to record "Danger Zone", and had also written and recorded a song "Only You" for the soundtrack. However, there was a dispute between Totos lawyers and the producers of the film, paving the way for Loggins to record "Danger Zone" and "Only You" being omitted from the film entirely. 

==Release==

===Home media===
In addition to its box office success, Top Gun went on to break further records in the then still-developing home video market. Backed by a massive $8 million marketing campaign including a Top Gun-themed Diet Pepsi commercial,  the advance demand was such that the film became the best-selling videocassette in the industrys history on pre-orders alone. It was also one of the first video cassette releases in the $20 price range.    Top Guns home video success was again reflected by strong DVD sales, which were furthered by a special-edition release in 2004. Bomber jacket sales increased and Ray-Ban Aviator sunglasses jumped 40%, due to their use by characters in the film.    The film also had boosted Navy recruitment. The Navy had recruitment booths in some theaters to attract enthusiastic patrons. 

===IMAX 3D re-release===
Top Gun was re-released in IMAX 3D on February 8, 2013, for six days.    A four-minute preview of the conversion, featuring the "Danger Zone" flight sequence, was screened at the 2012 International Broadcasting Convention in Amsterdam, Netherlands.    Subsequently, the film was released in Blu-ray 3D on February 19, 2013. 

==Reception==

===Box office===
The film opened in the United States in 1,028 theaters on May 16, 1986. It quickly became a success and was the highest grossing film of 1986. It was number one on its first weekend with a $8,193,052 gross, and went on to a total domestic figure of $176,786,701. Internationally it took in an estimated $177,030,000 for a worldwide box office total of $353,816,701.   

===Critical response===
Upon the films original release, critical response was mixed. Review aggregator Rotten Tomatoes reports that 54% of 48 critics have given the film a positive review, with a rating average of 5.8 out of 10 and the critical consensus states: "Though it features some of the most memorable and electrifying aerial footage shot with an expert eye for action, Top Gun offers too little for non-adolescent viewers to chew on when its characters arent in the air". 

Roger Ebert of the Chicago Sun-Times gave the film 2.5 out of 4 stars, pointing out that "Movies like Top Gun are hard to review because the good parts are so good and the bad parts are so relentless. The dogfights are absolutely the best since Clint Eastwoods electrifying aerial scenes in Firefox (film)|Firefox. But look out for the scenes where the people talk to one another." 

===Awards===
The film was nominated for and won many awards, most prominently for its sound and effects. The film won the following awards:

{| class="wikitable"
!Year
!Award
!Category – Recipient(s)
|-
| 1987
| ASCAP Film and Television Music Awards
| Most Performed Songs from Motion Pictures – Giorgio Moroder and Tom Whitlock for the song "Take My Breath Away".
|-
| 1987
| Academy Awards
| Best Music, Original Song – Giorgio Moroder (music) and Tom Whitlock (lyrics) for the song "Take My Breath Away".
|-
| 1986
| Apex Scroll Awards
| Achievement in Sound Effects
|-
| 1987 BRIT Awards
| Best Soundtrack
|-
| 1987
| Golden Globe Awards
| Best Original Song – Motion Picture – Giorgio Moroder (music) and Tom Whitlock (lyrics) for the song "Take My Breath Away".
|-
| 1987 Golden Screen Award
|
|-
| 1987
| Grammy Awards Best Pop Instrumental Performance (Orchestra, Group or Soloist) – Harold Faltermeyer and Steve Stevens for "Top Gun Anthem|Top Gun Anthem".
|- 1987
| Motion Picture Sound Editors
| Golden Reel Award for Best Sound Editing
|-
| Best Sound Editing – Sound Effects
|-
| 1987
| Peoples Choice Awards
| Favorite Motion Picture
|-
| 1988
| Award of the Japanese Academy
| Best Foreign Language Film
|-
|}

The film was nominated for the following awards:

* Academy Awards (1987)   
** Best Effects, Sound Effects Editing – Cecelia Hall and George Watters II
** Best Film Editing – Billy Weber and Chris Lebenzon Kevin OConnell, Rick Kline and William B. Kaplan
** Best Music, Original Song Giorgio Moroder (music), Tom Whitlock (lyrics)
* Apex Scroll Awards (1986)
** Actress in a Supporting Role – Meg Ryan
** Film Editing – Billy Weber and Chris Lebenzon
** Best Original Song – Motion Picture – Giorgio Moroder (music) and Tom Whitlock (lyrics) for the song "Take My Breath Away".
** Best Picture – Don Simpson, Jerry Bruckheimer
** Achievement in Compilation Soundtrack
** Achievement in Sound
* Golden Globe Awards (1987)
** Best Original Score – Motion Picture – Harold Faltermeyer
* Award of the Japanese Academy (1988)
** Best Foreign Language Film
* Fennecus Awards (1986)
** Achievement in Compilation Soundtrack
** Best Original Song – Motion Picture – Giorgio Moroder (music) and Tom Whitlock (lyrics) for the song "Take My Breath Away".
** Film Editing – Billy Weber and Chris Lebenzon
** Achievement in Sound
** Achievement in Sound Effects

===Effect on military recruiting=== John Davis claimed that Top Gun was a recruiting video for the Navy, that people saw the movie and said, "Wow! I want to be a pilot." After the films release, the US Navy stated that the number of young men who joined wanting to be Naval Aviators went up by 500 percent. 

Paramount Pictures offered to place a 90-second Navy recruiting advertisement at the beginning of the videocassette for Top Gun, in exchange for $1 million in credit towards their debt to the Navy for production assistance. An internal memo to the Pentagon from an advertising agency rejected the offer, noting that "Both movies are already wonderful recruiting tools for the military, particularly the Navy, and to add a recruiting commercial onto the head of what is already a two-hour recruiting commercial is redundant."   

==Legacy==
Since its initial release, the film has made many top film lists and has been the subject of comedic interpretation. In 2008, the film was ranked at number 455 in Empire (film magazine)|Empire s list of the 500 greatest films of all time.  Yahoo! Movies ranked Top Gun #19 on their list of greatest action films of all-time.  The film has been nominated multiple times for various AFI lists, ranking only once. In 2005, the line "I feel the need... the need for speed!" was ranked 94 on AFIs 100 Years...100 Movie Quotes list.

;American Film Institute lists
*AFIs 100 Years...100 Thrills – Nominated 
*AFIs 100 Years...100 Heroes and Villains:
**Lt. Pete "Maverick" Mitchell – Nominated Hero 
*AFIs 100 Years...100 Songs:
**"Take My Breath Away" – Nominated 
*AFIs 100 Years...100 Movie Quotes:
**"I feel the need — the need for speed." – #94
*AFIs 100 Years...100 Cheers – Nominated 

The 1991 film Hot Shots! was a comedy spoof of Top Gun.
 homoerotic subtext examined in a monologue performed by Quentin Tarantino in the 1994 film Sleep with Me.   http://www.cracked.com/funny-1906-quentin-tarantino-movies/ Cracked Magazine.com 
"Tarantino on Pop Culture (AKA If Cracked.com Was Written on Cocaine)"  

Top Gun is one of many war and action films, especially those by Jerry Bruckheimer, parodied in the 2004 comedy  . 

In the 2011 season opener, the Saturday Night Live crew did a sketch for the Top Gun 25th Anniversary DVD, featuring "never-before-seen screen tests". The SNL cast parodied Tony Danza, Alan Alda, Paula Abdul, Sinbad (entertainer)|Sinbad, and others.  The show had previously spoofed Top Gun in a 2000 episode hosted by Val Kilmer, doing a sketch in which Iceman had become a commercial airline pilot. 

The 2013 computer-animated film Planes (film)|Planes pays homage to Top Gun, with Val Kilmer and Anthony Edwards as voice cast.

==Sequel== New York magazine reported that Paramount Pictures had made offers to Jerry Bruckheimer and Tony Scott to make a sequel to Top Gun. Christopher McQuarrie had also received an offer to write the sequels screenplay, which was rumored to have Cruises character Maverick in a smaller role.  When asked about his idea for a new Top Gun film, Scott replied, "This world fascinated me, because its so different from what it was originally. But I dont want to do a remake. I dont want to do a reinvention. I want to do a new movie." 

In December 2011, Tom Cruise stated that the sequel was in the works, and that he was in talks to reprise his role.  In March 2012, it was revealed by Tom Burbage, Lockheed Martin F-35 program manager, that the F-35 Lightning II will be used and to be flown by Maverick as a test pilot in the sequel. 

On August 17, 2012, director Tony Scott and Tom Cruise met to scout locations in Fallon, Nevada for the sequel which was set to go into production in 2013 and expected for release in 2014. Jerry Bruckheimer would again produce and Peter Craig was in charge of writing script which is said to be nearly finished. According to reports, the plot of the movie would focus on the role of drones in modern aerial warfare. 

Since Scotts suicide, the sequels future has remained in question. However, producer Jerry Bruckheimer has stated he "hasnt given up" on the sequel as all parties, including Tom Cruise and Val Kilmer, are still interested in making the film. Mentioning he has wanted to make a sequel happen since the film came out 27 years ago, Bruckheimer confirmed the interest is still strong. "Its just figuring out how to do it," Bruckheimer said, "which I think we have a good handle on, and losing Tony slowed us down. But hopefully, we can pick up speed again." 

In June 2013, Bruckheimer stated that: "For 30 years weve been trying to make a sequel and were not going to stop. We still want to do it with Tom   and Paramount are still interested in making it. What Tom tells me is that no matter where he goes in the world, people refer to him as Maverick. Its something he is excited about so as long as he keeps his enthusiasm hopefully well get it made." 

On September 8, 2014, Paramount and Skydance revealed being in negotiations to have Justin Marks write the screenplay. 

==Video games==
 
Top Gun also spawned a number of  , Commodore 64, ZX Spectrum, Amstrad CPC and Nintendo Entertainment System (NES) (with an equivalent version for Nintendos "VS." arcade cabinets). In the game, the player pilots an F-14 Tomcat fighter, and has to complete four missions. A sequel, Top Gun: The Second Mission, was released for the NES three years later.

Another game, Top Gun: Fire at Will, was released in 1996 for the PC and later for the   in 2001 and was ported to the  , PC, and PlayStation 3.

Mobile game publisher Hands-On Mobile (formerly known as Mforma) have published three mobile games based around Top Gun.  The first two were top-down scrolling arcade shooters. The third game takes a different approach as a third-person perspective game, similar to Segas After Burner games. 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 