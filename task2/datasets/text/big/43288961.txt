Pappa e ciccia
{{Infobox film
| name           =Pappa e ciccia
| image          = Pappa e ciccia.jpg
| caption        =  
 | director =Neri Parenti
 | writer =  Leonardo Benvenuti Piero De Bernardi Franco Marotta Laura Toscano  Paolo Villaggio Gianni Manganelli  Neri Parenti
 | starring =   Paolo Villaggio   Lino Banfi  
 | music =  Bruno Zambrini 
 | cinematography = Alberto Spagnoli	
 | editing =   Sergio Montanari
 | released =  
 | language = Italian  
| released =  1982 
 | country = Italy 
| runtime = 98 min
}}
 1983 Italian comedy film directed by Neri Parenti.       

==Plot==
First segment: Nicola Calore, an Apulian mason who emigrated in Switzerland, faces the visit of his  niece Rosina who believes that he has become wealthy.  
Second segment: the misadventures that occurred to two surveyors during their stay in a holiday village in Kenya.

==Cast==

*Paolo Villaggio: Surveyor / Female Nurse
*Lino Banfi: Nicola Calore / Cpt. Tombale
*Milly Carlucci: Rosina Calore / Claudia  
*Jacques Herlin: Herr Schmidt
*Pippo Santonastaso: Guido Colzi
*Marina Confalone: Antonia 
*Antonio Allocca: Pino 
*Roberto Della Casa: Goffredo

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 