Crazy Kutumba
 

{{Infobox film
| name = Crazy Kutumba
| image = 
| caption =
| director =  B Ramamurthy
| writer = Atul Kale   Sudesh Manjrekar
| starring = Ramesh Aravind  Ananth Nag
| producer = Ravi Joshi
| music = Ricky Kej   Mysore Ananthaswamy
| cinematography = K. Mallikarjun   Ramesh Appi
| editing = Narahalli Jnanesh
| studio = Luv Kush Productions
| released = February 12, 2010
| runtime = 125 minutes
| language = Kannada
| country = India
| budget =
}}
 comedy film directed by B. Ramamurthy and produced by Ravi Joshi for Luv Kush Productions banner. The story is a remake of Marathi film De Dhakka (2008) which was inspired from the Hollywood film, Little Miss Sunshine (2006). 

The film features Ramesh Aravind and Ananth Nag in leading roles with Sanathini, Dhanya Rao, Jai Jagadish, Bank Janardhan and other theater artistes from North Karnataka region playing supporting roles.

== Cast ==
* Ramesh Aravind as Shankar Patil
* Ananth Nag as Mallanna
* Sanathini as Sumathi
* Chindodi Veershankar
* M. S. Umesh
* Rajinikanth
* Jai Jagadish
* Karibasavaiah
* Bank Janardhan
* G. Bharathkumar
* Baby Dhanya Rao as Gowri

==Plot==
A family travels from a village to Bangalore, as one of its members is to participate in a dance contest. En route, the conflicts and travails of the family come to the fore in a humorous manner.

== Soundtrack ==
The music was composed by Ricky Kej for Junglee music company. One song "Amma Naanu" is composed by Mysore Ananthaswamy.  The soundtrack includes popular folk poems written by acclaimed poets such as Kuvempu and K. S. Narasimhaswamy.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Amma Naanu ( )
| extra1 = Shreya Ghoshal
| lyrics1 = H. S. Venkateshmurthy
| length1 = 05:40
| title2 = Hendathi Obbalu
| extra2 = Rajesh Krishnan
| lyrics2 = K. S. Narasimhaswamy
| length2 = 03:41
| title3 = Nade Mundhe
| extra3 = Avinash Chebbi, Avinash Bharadwaj
| lyrics3 = Kuvempu
| length3 = 04:36
| title4 = Naanagiddare Shrimantha
| extra4 = Rajesh Krishnan
| lyrics4 = Jayanth Kaikini
| length4 = 04:27
| title5 = Banthu Banthu
| extra5 = Avinash Chebbi
| lyrics5 = Avinash Chebbi
| length5 = 01:21
| title6 = Chori Chori
| extra6 = M. D. Pallavi Arun
| lyrics6 = Avinash Chebbi
| length6 = 01:50
| title7 = Nee Badalaadare
| extra7 = B. Jayashree, M. D. Pallavi Arun, Avinash Chebbi, Rajesh Krishnan, Badri Prasad, L. N. Shastry
| lyrics7 = Avinash Chebbi
| length7 = 03:59
| title8 = Main Chulbuli Hoon
| extra8 = Avinash Chebbi, Sinchan Dixit
| lyrics8 = Avinash Chebbi
| length8 = 01:30
}}

==Review==
Upon release, the film met with favorable critical reviews for its content and character portrayals. Rediff.com reviewed with 3 stars saying the film is meant to be watched with entire family.  Times of India reviewed saying the film is endearing to the family audience.

==See also==
* De Dhakka
* Little Miss Sunshine

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 

 