Suspect (1960 film)
{{Infobox film
| name           = Suspect
| image          =
| image_size     =
| caption        =
| director       = John Boulting Roy Boulting
| producer       = John Boulting Roy Boulting
| writer         = Nigel Balchin
| narrator       =
| starring       = Tony Britton  Virginia Maskell Ian Bannen
| distributor    = British Lion Film Corporation
| released       = 1960
| runtime        = 81 minutes
| country        = United Kingdom
| language       = English
| budget         =
}} 1960 Cinema British thriller film directed by Roy Boulting and John Boulting. It starred Tony Britton, Virginia Maskell, Ian Bannen, Peter Cushing and Donald Pleasence. A young scientists pioneering work and his acquaintance with subversive anti-government groups attract the attention of the authorities. 

==Cast==
* Tony Britton&nbsp;&mdash; Doctor Robert Marriott
* Virginia Maskell&nbsp;&mdash; Doctor Lucy Bryne
* Ian Bannen&nbsp;&mdash; Ian Andrews
* Kenneth Griffith&nbsp;&mdash; Doctor Shole
* Peter Cushing&nbsp;&mdash; Professor Sewell
* Thorley Walters&nbsp;&mdash; Prince
* Donald Pleasence&nbsp;&mdash; Parsons
* Spike Milligan&nbsp;&mdash; Arthur
* Raymond Huntley&nbsp;&mdash; Sir George Gatting, Minister of Defence
* Sam Kydd&nbsp;&mdash; Slater
* Antony Booth&nbsp;&mdash; Parkin
* Basil Dignam&nbsp;&mdash; Doctor Childs
* Brian Oulton&nbsp;&mdash; Director

==References==
 

==External links==
* 

 

 
 
 
 


 
 