Off-Balance
{{Infobox film
| name           = Off-Balance
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Shin Togashi
| producer       = 
| writer         = 
| screenplay     = Shiori Kazama
| story          = Naoko Uozumi (novel)
| based on       = 
| narrator       = 
| starring       = Megumi Hatachiya Fumiyo Kohinata Yu Hatano
| music          = Masahiro Kawasaki
| cinematography = 
| editing        = 
| studio         = 
| distributor    = JVC Media Box
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
  is a 2001 Japanese film directed by Shin Togashi based on the novel Hi-baransu by Naoko Uozumi which won the author the Kodansha Award for New Writers of Childrens Literature.  

==Cast==
* Megumi Hatachiya as Chiaki
* Fumiyo Kohinata as Kiku-chan
* Yu Hatano as Mizue
* Momoka Nakamura as Yukari
* Shūji Kashiwabara as Heath
* Tomato as Yuri-chan
* Kumiko Tsuchiya as Homeroom Teacher
* Ryūshi Mizukami as Video Store Clerk
* Masayo Umezawa as Mizues Mother
* Noriko Hayami as Masayoshis Wife
* Mieko Harada as Chiakis Mother

==Awards==
23rd Yokohama Film Festival  
*Won: Best New Director - Shin Togashi
*10th Best Film

==References==
 

==External links==
*  
*  

 

 
 
 
 


 