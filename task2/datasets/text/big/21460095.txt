The Rabbit Man
{{Infobox film
| name           = Kaninmannen
| image          = Kaninmannen poster.JPG
| caption        = Film poster Stig Larsson
| producer       = Peter Kropenin Stig Larsson
| starring       = Leif Andrée Börje Ahlstedt Stina Ekblad
| music          = Dror Feiler
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
 Stig Larsson, about a rapist whose father unknowingly investigates his crimes for a commercial television programme. The film sparked controversy because of its nude scenes and difficult subject, handled with an unconventional narrative style.

The title refers to a nickname given by newspapers to a real rapist Larsson read about in the 1970s, who lured young girls into his apartment by telling them he was keeping rabbits there that he wanted to save from being exposed to animal testing.
 Best Actor for his part as the rapists father at the 26th Guldbagge Awards.   

==Selected cast==
* Leif Andrée - Hans Nääs
* Börje Ahlstedt - Bengt Nääs
* Stina Ekblad - Lollo
* Eva Engström - Maud
* Björn Gedda - Gunnar Dahlgren
* Erika Ullenius - Carla
* Krister Henriksson - Alexandersson
* Johan Rabaeus - Thommy

==References==
 

==External links==
*  

 
 
 
 
 
 
 