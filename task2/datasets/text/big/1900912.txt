Pulgasari
{{Infobox film name            = Pulgasari image           = Pulgasari-poster.jpg caption         = Japanese theatrical poster director        = Shin Sang-ok Chong Gon Jo producer        = Kim Jong-il Shin Sang-ok writer          = Kim Se Ryun starring        = Chang Son Hui Ham Gi Sop Jong-uk Ri Gwon Ri Gyong-ae Yu music           = So Jong Gon cinematography  = Cho Myong Hyon Pak Sung Ho Kenichi Eguchi editing         = Kim Ryon Sun|  studio          = Korean Film Studio distributor     = Korean Film Studio released        =   runtime         = 95 min country         = North Korea Japan language        = Korean budget          =
| film name      = {{Film name hangul          =   hanja           =   rr              = Bulgasari mr              = Pulgasari context         = north}}
}} North Korean North Korean intelligence on the orders of Kim Jong-il, son of the then-ruling Kim Il-sung. Backrow Banter,   

Kim was a lifelong admirer of the director, as well as Godzilla and other Kaiju films. He kidnapped the former director and his wife, famous actress Choi Eun-hee, with the specific purpose of making fantasy/propaganda films for the North Korean government.  Kim Jong-il also produced Pulgasari and all the films that Sang-ok made before he and Choi managed to escape from their minders while on tour in Austria. 
 special effects.  Kenpachiro Satsuma – the stunt performer who played Godzilla from 1984 to 1995 – portrayed Pulgasari, and when the Godzilla (1998 film)|Godzilla reimagining was released in Japan in 1998, he was quoted as saying he preferred Pulgasari to Zilla (Toho)|Emmerichs "Godzilla". 

The film was eventually released direct to video in the United States in 2001 by A.D. Vision

== Synopsis == Goryeo Dynasty, a king controls the land with an iron fist, subjecting the peasantry to misery and starvation. An old blacksmith who was sent to prison creates a tiny figurine of a monster by making a doll of rice. When it comes into contact with the blood of the blacksmiths daughter, the creature springs to life, becoming a giant metal-eating monster named Pulgasari.

The evil King becomes aware that there is a rebellion being planned in the country, which he intends to crush, but he runs into Pulgasari, who fights with the peasant army to overthrow the corrupt monarchy.

== Background ==
The film is based around a legendary creature called the Pulgasari.  The original story was set in the city of Songdo (now Kaesong, North Korea).

== Legacy ==
Pulgasari has gained some popularity over the years because of the shocking story of Shin Sang-ok and Choi Eun-hees kidnapping and strange captivity as the director and leading actress - the latter one excluding this film - of a total of seven films, for which the couple was simultaneously commissioned and forced to do by North Koreas government.  Jonathan Ross stated that the film is intended to be a propaganda metaphor for the effects of unchecked capitalism and the power of the collective.   

== See also ==
* Culture of North Korea
* List of North Korean films
* List of films set in or about North Korea
* Propaganda in North Korea

== References ==
 

== External links ==
*  
*   BBC
*  

 

 
 
 
 
 
 
 
 
 
 
 