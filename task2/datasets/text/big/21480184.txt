Girl and the River
 
{{Infobox film
| name           = Girl and the River
| image          = 
| caption        = 
| director       = François Villiers
| producer       = Claude Clert
| writer         = Alain Allioux Jean Giono
| starring       = Pascale Audret
| music          = 
| cinematography = Paul Soulignac
| editing        = Edouard Berne
| distributor    = 
| released       =  
| runtime        = 96 minutes
| country        = France
| language       = French
| budget         = 
}}
Girl and the River ( ) is a 1958 French drama film directed by François Villiers. It was entered into the 1958 Cannes Film Festival.     

==Cast==
* Henri Arius - The uncle from Cavaillon
* Pascale Audret - Hortense
* Odette Barencey - Joséphine
* Charles Blavette - Loncle Simon
* Jean Clarens - Le notaire
* Andrée Debar - The cousin from Rochebrune
* Hubert de Lapparent - Elie, a peasant
* Hélène Gerber - La femme dElie
* Jean Giono - Le récitant (voice)
* Harry-Max - Le juge de paix
* Germaine Kerjean - The aunt from Rochebrune
* Robert Lombard - Le cousin de Rochebrune
* Milly Mathis - The aunt from Cavaillon
* Pierre Moncorbier - Loncle vigneron
* Jean Panisse - The butcher
* Maurice Sarfati - The cousin from Cavaillon
* Dany Saval
* Jean-Marie Serreau - Le Jéhovah de Perthuis
* Madeleine Sylvain - La bouchère
* Arlette Thomas - La femme de Dabisse
* Jean Toscane

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 