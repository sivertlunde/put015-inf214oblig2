Agustín's Newspaper
 

{{Infobox film
| name           = Agustins Newspaper
| image          = 
| alt            = 
| caption        =
| director       = Ignacio Agüero
| producer       = Ignacio Agüero Fernando Villagrán
| writer         = Ignacio Agüero Fernando Villagrán
| starring       =
| music          = Giorgio Varas   Cristián López
| cinematography = Gabriel Díaz   Ricardo Lorca
| editing        =
| studio         =Ignacio Aguero & Asociados   Amazonía films
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Chile
| language       = Spanish
| budget         = 
| gross          = 
}}
Agustins Newspaper (  Chilean documentary, directed by Ignacio Agüero, a Chilean film director.

==Plot==
Journalism students start an investigation about the editorial line of El Mercurio S.A.P., a media corporation owned by Agustín Edwards Eastman that publishes Chilean daily newspapers El Mercurio, La Segunda, and Las Últimas Noticias.
 land reform military government, arrival of democracy in Chile. Overall, it presents a strong indictment of the newspaper and its editor,  which are accused of public opinion manipulation, especially with disinformation campaigns.

==Chapters==
*I. El Mercurio Lies   
*II. Foreign Help Is on the Way The 119
*IV. Crime of Passion
*V. A Guide for Society
*VI. Slanders and Libels
*VII. Epilogue

==Awards== Altazor Award in 2009 for best director in a documentary film. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 