Mickey's Garden
 
{{Infobox Hollywood cartoon cartoon name=Mickeys Garden
|series=Mickey Mouse
|director=Wilfred Jackson    producer          =Walt Disney story artist      = narrator          = voice actor       =Walt Disney Pinto Colvig musician          =Leigh Harline animator          =Art Babbitt Frenchy DeTremaudan Dick Huemer layout artist=Ollie Johnston background artist= Walt Disney Productions
|distributor=United Artists release date=  color process=Technicolor
|runtime=8 minutes 49 seconds
|country=United States
|language=English preceded by=Mickeys Kangaroo followed by=Mickeys Fire Brigade
}} Walt Disney Productions and released by United Artists. The film was the second Mickey Mouse cartoon shot in Technicolor (after The Band Concert) and is also the first color appearance of Pluto (Disney)|Pluto. The cartoon is also the first color cartoon where Mickey speaks (though he only does so at the end when Pluto starts licking him). The films plot centers on Mickey Mouse trying to rid his garden of insects, but they keep coming back. When he accidentally gets hit with his own bug spray, he begins seeing a warped reality.   

==Plot==
Mickeys garden is being invaded by insects. Mickey, in an attempt to get rid of them, uses insecticide, but it eventually runs out, and the insects are able to continue eating. Mickey goes back and attempts to make some more insecticide.

The insects return and resume eating all the food in the garden. Mickey comes back and tries to poison the bugs, but the pump is blocked and Mickey tries to unblock it. At the same time, Pluto is chasing a bug. It bites him on the nose and he yelps in pain. When it teases him from atop a flower, he lunges at it and gets his head stuck inside a pumpkin. He runs about it with the pumpkin on his head and bumps into Mickey, making him get hit by his own bug spray, after which he begins seeing hallucinations.

Mickey finds himself in an imaginary world where he, his house, and Pluto have shrunk and the bugs are giants. The bugs proceed to drink the insecticide, but instead of killing them it gives them a taste for revenge and they start chasing Mickey and Pluto (who has gotten loose from the now-giant pumpkin). After encounters with a caterpillar and a beetle, Mickey and Pluto escape up a flower but are stung by a bee. Pluto is thrown into the air and then gets swallowed by a hiccuping firefly. Mickey lands on a leaf but is shaken off by a drunken grasshopper. He lands in a tomato with a worm, which he proceeds to ride and then wrestle.

Eventually Mickey wakes up and discovers, much to his delight, that the worm he was wrestling was really his hose. Pluto manages to break free from the pumpkin and catapults it onto Mickey by accident, and starts licking him.

==Releases==
* 1935 - theatrical release 
* 1988 - Mickeys 60th Birthday
* 2001 - Mickey Mouse in Living Color: Cartoon No. 2

==References==
 

== External links ==
*  
* 

 

 
 
 
 
 
 
 