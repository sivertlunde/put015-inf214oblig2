Vai que Dá Certo
{{Infobox film
| name           = Vai que Dá Certo
| image          = Vai que Dá Certo Poster.jpg
| caption        =
| director       = Mauricio Farias	
| producer       = 
| writer         = 	 
| starring       = Gregório Duvivier Bruno Mazzeo Danton Mello Natália Lage Fábio Porchat
| music          = 
| cinematography = 
| editing        = 
| studio         = Globo Filmes  Fraiha Produções
| distributor    = Imagem Filmes
| released       =  
| runtime        = 87 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          =
}}
Vai que Dá Certo is a 2013 Brazilian comedy film directed by Mauricio Farias. It was shot in Paulínia and Campinas, cities in the state of São Paulo. 

==Plot==
The film follows the reunion of five young adult friends who share the frustration of not having achieved the success they wanted in their lives. The possibility to recover the time they lost arises through a tempting and risky proposal: the assault of a carrier of values. The supposedly perfect crime that promised to transform their trajectories fulfills its purpose, but not exactly as planned. 

==Cast==
* Danton Mello as Rodrigo
* Fábio Porchat as Amaral
* Lúcio Mauro Filho as Danilo
* Natália Lage as Jaqueline
* Gregório Duvivier as Vaguinho
* Bruno Mazzeo as Paulo
* Lúcio Mauro as Seu Altamiro
* Felipe Abib as Tonico
* Roney Facchini as Durval
* Sérgio Guizé   

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 
 