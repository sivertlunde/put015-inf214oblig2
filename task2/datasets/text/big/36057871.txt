Jai Hind (film)
{{Infobox film
| name           = Jai Hind 
| image          = 
| image_size     = 
| caption        =  Arjun
| producer       = S. Chain Raaj Jain Arjun
| starring       =   Vidyasagar
| cinematography = K. S. Selvaraj
| editing        = P. Sai Suresh
| studio         = Shri Mishri Entreprises
| distributor    = Shri Mishri Entreprises
| released       = 20 May 1994
| runtime        = 150 minutes
| country        = India Tamil
| budget         =
}}
 1994 Tamil Tamil action Arjun and Vidyasagar and was released on 20 May 1994. Jai Hind achieved significantly positive reviews and became a major blockbuster (entertainment)|blockbuster.   The prequel titled Jai Hind 2 was released in 2014.

==Plot==
Bharath (Arjun Sarja|Arjun), a police officer, is in love with a beautiful woman Priya (Ranjitha). A terrorist group attacks the state of Tamil Nadu and they kill the current Chief Minister (Kalyan Kumar) and Sriram (Devan (actor)|Devan), Bharaths brother.

Bharath decides to go with some prisoners and his lover in the island where the terrorist group are hidden. Whether he will be able to prevent their attacks or will he become a victim himself forms the crux of the story. 

==Cast==
  Arjun as Bharath
*Ranjitha as Priya
*Goundamani as Kottaisamy Manorama as Bharaths mother Senthil as  Pulikutty / Poonaikutty / Paayuson / Saarayakadai Saathappan / Sangili Karuppan
*Major Sundarrajan as Ravichandran
*Charuhasan
*Kalyan Kumar as Chief Minister Rajesh as Seenivasan Devan as Sriram, Bharaths brother
*Vimalraj as the Terrorist leader
*Pandari Bai as Maria Devi Chandrasekhar as Sekhar
*Chokkalinga Bhagavathar
*Bayilvan Ranganathan
*Idichapuli Selvaraj
*Gowtham as Gowtham Vaishnavi as Susila
*Kamala Kamesh
*Baby Vichitra
*Kavithasri as an item number
*Raju Sundaram in a Special appearance
 

==Soundtrack==
{{Infobox Album |  
  Name        = Jai Hind |
  Type        = soundtrack | Vidyasagar |
  Cover       = |
  Released    = 1994 |
  Recorded    = 1994 | Feature film soundtrack |
  Length      = 21:59|
  Label       = Vega Music Bayshore| Vidyasagar |  
  Reviews     = |
}}
 Vidyasagar while lyrics written by Vairamuthu. The soundtrack, released in 1994, featuring 5 tracks was well-received by the public. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration 
|-  1 || Bodhai Yeri Pocchu || Suresh Peters || 4:29
|- 2 || Kanna En Selai || S. Janaki, S. P. Balasubrahmanyam || 4:53
|- 3 || Mano || 3:22
|- 4 || Vidyasagar || 4:49
|- 5 || Thayin Manikodi || S. P. Balasubrahmanyam || 4:26
|}

==References==

 

 
 
 
 
 