Hvězda jede na jih
 
 
{{Infobox film
| name           = Hvězda jede na jih
| image          = 
| alt            = 
| caption        = 
| director       = Oldrich Lipský
| producer       = 
| writer         = Oldrich Lipský Vladimír Skutina
| starring       = Gordana Miletic
| music          = 
| cinematography = Ferdinand Pecenka
| editing        = Miroslav Hájek
| studio         = 
| distributor    = 
| released       = 1958 (filmed) 15 May 1964 (released)
| runtime        = 98 minutes
| country        = Yugoslavia Czechoslovakia
| language       = Czech Serbo-Croatian
| budget         = 
| gross          = 
}}

Hvězda jede na jih is a 1958 Czechoslovak-Yugoslavian comedy film directed by Oldrich Lipský.

==Cast==
* Gordana Miletic - Sona Klánová - singer
* Rudolf Hrusínský - Conductor
* Joza Gregorin - Driver Dargo
* Barbara Polomska - Lída
* Ludmila Píchová - Camper Nademlejnská Rudolf Deyl - Alfréd Necásek
* Stella Zázvorková - Camper Petioká
* Milos Kopecký - Soustek - tourist guide Eman Fiala - Camper Strouhal
* Jaroslav Stercl - Drummer Pistelák
* Rudolf Cortés - Rudy Bárta - singer
* Karel Effa - Egon Zejda - guitarist
* Vladimír Mensík - Clarinetist Vostrák
* Lubomír Lipský - Trombonist Holpuch
* Josef Hlinomaz - Trumpetist Bríza

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 