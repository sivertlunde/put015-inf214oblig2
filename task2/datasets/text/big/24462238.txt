Lights of Old Broadway
{{Infobox film
| name           = Lights of Old Broadway
| image          =
| caption        =
| director       = Monta Bell
| producer       = Cosmopolitan Productions
| writer         = Laurence Eyre (play The Merry Wives of Gotham) Carey Wilson (adaptation & scenario) Joseph Farnham (intertitles)
| starring       = Marion Davies Conrad Nagel
| cinematography = Ira H. Morgan
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 minutes
| country        = United States Silent English intertitles
| budget         =
}}

Lights of Old Broadway (1925 in film|1925) is a drama film directed by Monta Bell, produced by William Randolph Hearsts Cosmopolitan Productions, and released by Metro-Goldwyn-Mayer. The film stars Marion Davies and Conrad Nagel, and is an adaptation of the play The Merry Wives of Gotham by Laurence Eyre (USA).  The film has color sequences using Film tinting|tinting, Technicolor#Process 2|Technicolor, and the Handschiegl color process. 

The play was produced on Broadway at Henry Millers Theatre from January 16, 1924 to April 1924. Davies part was played on the stage by actress Mary Ellis.  

==Plot==
Marion Davies plays twins separated at birth: Anne becomes a society girl in New York and Fely becomes an Irish poor girl turning to the musical world. Dirk (Conrad Nagel), Annes half brother, falls in love with the poor sister, with the added inconvenient that Annes family owns the slum in which the Irish families live.

==Cast==
* Marion Davies - Fely / Anne 
* Conrad Nagel - Dirk de Rhonde 
* Frank Currier - Lambert de Rhonde 
* George K. Arthur - Andy  Charles McHugh - Shamus OTandy 
* Eleanor Lawson - Mrs. OTandy

==See also==
*List of early color feature films

==References==
 

==External links==
*  
* 
* 
* 

 
 
 
 
 
 
 
 


 