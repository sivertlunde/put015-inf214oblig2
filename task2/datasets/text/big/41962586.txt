Some Came Running (film)
{{Infobox film
| name           = Some Came Running 
| image          = Poster of the movie Some Came Running.jpg
| image_size     = 
| caption        = 
| director       = Vincente Minnelli
| producer       = Sol C. Siegel
| based on       =   John Patrick Arthur Sheekman
| narrator       =  Arthur Kennedy Nancy Gates
| music          = Elmer Bernstein
| cinematography = William H. Daniels
| editing        = Adrienne Fazan
| distributor    = Metro-Goldwyn-Mayer (1958, original) Warner Bros. (2008, DVD) 1958
| runtime        = 137 min.
| country        = U.S.A. English
| budget         = $3,151,000  . 
| gross          = $6,295,000 
}}
 1958 in American film directed by Vincente Minnelli and starring Frank Sinatra, Dean Martin, and Shirley MacLaine.
 James Jones Arthur Kennedy and Nancy Gates.

MacLaine garnered her first Academy Award nomination, which she credited to Sinatra for his insistence on changing the films ending.

==Plot==
Dave Hirsh (Frank Sinatra) is a cynical Army veteran who winds up in his hometown of Parkman after being put on a bus in Chicago while intoxicated. Ginny Moorehead (Shirley MacLaine), a woman of seemingly loose morals and poor education, was invited by Dave in his drunken state, but gives her money to return to Chicago because it was a mistake.
 Arthur Kennedy), placed him in a charity boarding school, and is still embittered. Frank has since married well, inherited a jewelry business from the father of his wife Agnes (Leora Dana), and made their social status his highest priority. Daves return threatens this, so Frank makes a fruitless stab at arranging respectability, introducing him to his friend Professor French (Larry Gates) and his daughter Gwen (Martha Hyer), a schoolteacher.

Dave moves in different circles than his brother would prefer, however. He befriends Bama Dillert ( ), and falls in love with Gwen. Despite his somewhat notorious reputation, Dave is basically a good, honest man, well aware of his own shortcomings. His cynicism is often a mask to hide the pain of rejection.

Though Ginny is not his social or intellectual match, he eventually sees the basic good in her and responds to her unconditional love. In the end, Ginny, stalked by her former boyfriend (a Chicago hoodlum), proves the depth of her love for Dave by taking a fatal bullet for him.

==Cast==
  
* Frank Sinatra as Dave Hirsh
* Dean Martin as Bama Dillert
* Shirley MacLaine as Ginny Moorehead
* Martha Hyer as Gwen French Arthur Kennedy as Frank Hirsh
* Nancy Gates as Edith Barclay
 
* Leora Dana as Agnes Hirsh
* Betty Lou Keim as Dawn Hirsh
* Larry Gates as Professor Robert Haven French
* Steve Peck as Raymond Lanchak
* Connie Gilchrist as Jane Barclay
* Ned Wever as Smitty
 

==Reception==
Released to critical plaudits, Some Came Running was praised both nationally and internationally on release, with Sinatra garnering some of the strongest notices of his career. Variety (magazine)|Variety noted that "Sinatra gives a top performance, sardonic and compassionate, full of touches both instinctive and technical. It is not easy, either, to play a man dying of a chronic illness and do it with grace and humor, and this Martin does without faltering."

Although the film was popular – according to MGM records it earned $4,245,000 in the US and Canada and $2,050,000 elsewhere – its high cost meant MGM recorded a loss of $207,000.  It was the 10th highest-earning film of 1958. 
 Academy Awards==
;Nominations    Best Actress: Shirley MacLaine Best Actor in a Supporting Role: Arthur Kennedy Best Actress in a Supporting Role: Martha Hyer
*  ; Lyrics by Sammy Cahn
*  

==Legacy==
Martin Scorsese included a clip from the film for his A Personal Journey with Martin Scorsese Through American Movies; the films final carnival scene remains for Scorsese one of the best and most expressive uses of CinemaScope.

In his book Who the Hells in It, director Peter Bogdanovich writes extensively about Some Came Running. He later filmed a short segment for Turner Classic Movies on its influence on cinema.
 Flubber the robot Weebo uses a clip from the film as a shout-out for why she wont sabotage the professors relationship anymore.

==DVD==
Some Came Running was released to DVD by Warner Home Video on May 13, 2008 as a Region 1 widescreen DVD and also on the same date as part of the 5-disc box set Frank Sinatra: The Golden Years with Some Came Running as the fourth disc.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 