Far from Heaven
 
{{Infobox film
| name = Far From Heaven
| image = Far from heaven.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Todd Haynes
| producer = Jody Allen Christine Vachon
| writer = Todd Haynes
| starring = Julianne Moore Dennis Quaid Dennis Haysbert Patricia Clarkson Viola Davis
| music = Elmer Bernstein
| cinematography = Edward Lachman James Lyons TF1 Cinema Section Eight Vulcan Productions
| distributor = Focus Features
| released =  
| runtime = 107 minutes  
| country = United States
| language = English
| budget = $13.5 million 
| gross = $29,027,914   
}}
Far from Heaven is a 2002 American drama film written and directed by Todd Haynes and starring Julianne Moore, Dennis Quaid, Dennis Haysbert, and Patricia Clarkson.
 Imitation of Life), dealing with complex contemporary issues such as Race (classification of human beings)|race, gender roles, sexual orientation and Social class|class.

==Plot==
In 1957 suburban Connecticut, Cathy Whitaker, appears to be the perfect wife, mother, and homemaker. Cathy is married to Frank, a successful executive at Magnatech, a company selling television advertising. One evening Cathy receives a phone call from the local police who are holding her husband. He says its all a mix up but they wont let him leave alone. Frank has in fact been exploring the underground world of gay bars in Hartford, Connecticut. One day, Cathy spies an unknown black man walking through her yard. He turns out to be Raymond Deagan, the son of Cathys late gardener.

Frank often finds himself forced to stay late at the office, swamped with work. One night when Frank is working late, Cathy decides to bring his dinner to him at the office. She walks in on him passionately kissing another man. Frank confesses having had "problems" as a young man, and agrees to sign up for conversion therapy. However, his relationship with Cathy is irreparably strained, and he turns to Alcoholism|alcohol.

Cathy runs into Raymond at a local art show, and initiates a discussion with him about modern painting, to the consternation of a few onlookers. One night, after a party, Frank attempts to make love to Cathy. He is unable to become aroused and strikes Cathy when she tries to console him.

Cathy decides to spend a day with Raymond. They go to a bar in the black neighborhood in which she is the only white person present. Raymond toasts her with a drink saying "Heres to being the only one".
They are sighted together by one of Cathys neighbors, who immediately tells everyone. The town is soon ablaze with gossip about the two of them. This becomes evident when Cathy attends a ballet performance by her young daughter and the mothers of the other girls prevent them from socializing with Cathys daughter. Cathys husband is also furious. Cathy goes to find Raymond to tell them that their friendship isnt "plausible".

Over Christmas and New Years, Cathy goes on a vacation with her husband to Miami to take their minds off of things. At the hotel, Frank has another sexual encounter with a young man.

Back in Hartford, three white boys taunt and assault Raymonds daughter, Sarah. Frank tells Cathy that he has found a man who loves him and wants to be with him and seeks a divorce from Cathy. When Cathy eventually finds out that the victim of the attack was Raymonds daughter, Sarah, she goes to the Deagan home to find them packing up and moving to Baltimore. At one point when he addresses her as "Mrs. Whitaker", she begs him to call him Cathy. She suggests they can be together now that she is to be single. Raymond declines, saying "Ive learned my lesson about mixing the two worlds." In the final scene, Cathy goes to the train station to see Raymond off and say her silent goodbye to him, waving to him as the train moves out of the station.

==Cast==
* Julianne Moore as Cathy Whitaker
* Dennis Quaid as Frank Whitaker
* Dennis Haysbert as Raymond Deagan
* Patricia Clarkson as Eleanor Fine
* Viola Davis as Sybil
* James Rebhorn as Dr. Bowman
* Michael Gaston as Stan Fine
* Celia Weston as Mona Lauder
* Barbara Garrick as Doreen
* Bette Henritze as Mrs. Leacock
* June Squibb as Elderly Woman
* Ryan Ward as David Whitaker
* Lindsay Andretta as Janice Whitaker
* Jordan Puryear as Sarah Deagan
* J.B. Adams as Morris Farnsworth
* Olivia Birkelund as Nancy 

Haynes wrote the script envisioning Moore and James Gandolfini as Cathy and Frank, respectively. While Moore joined the project immediately, Gandolfini was unavailable due to The Sopranos. Haynes next choice, Russell Crowe, believed that the role was too small, and Jeff Bridges wanted too much money. Vachon, Christine and Austin Bunn.   Simon and Schuster, 2006. 

==Style==
According to the DVD Audio commentary|directors commentary Far from Heaven is made in the style of many 1950s films, notably those of Douglas Sirk. Haynes created color palettes for every scene in the film and was careful and particular in his choices. Haynes emphasizes experience with color in such scenes as one in which Cathy, Eleanor, and their friends are all dressed in reds, oranges, yellows, browns, and greens. Haynes also plays with the color green, using it to light forbidden and mysterious scenes. He employs this effect both in the scene in which Frank visits a gay bar and when Cathy goes to the restaurant in a predominantly black neighborhood. Far from Heaven DVD commentary track 
 foley to make more prominent the sound of rustling clothes and loud footsteps, a sound technique that was used more in 1950s-era film. 

In the commentary, Haynes notes that he was also influenced by  .  As in Fassbinders film, in Far from Heaven Haynes portrays feelings of alienation and awkwardness. For example, instead of cutting to the next scene, Haynes sometimes lingers on a character for a few seconds longer than comfortable to the viewer, the same technique used by Fassbinder. 

Another feature is when Cathy drives her car through town. Rather than filming inside the car as it actually moves, the car is filmed still with artificial backgrounds seen through the windows, reminiscent of older films. On the DVD commentary, Haynes states that one of these scenes re-uses the artificial background first used in a scene from All That Heaven Allows. 

==Reception==
The film received positive critical reviews. It holds a 87% "Fresh" rating on Rotten Tomatoes, based on 210 reviews, with an average rating of 8.1/10. The critical consensus states that "An exquisitely designed and performed melodrama, Far From Heaven earns its viewers tears with sincerity and intelligence." 
The film was nominated for several  , considers the film to be a companion piece to Haynes 1995 Safe (1995 film)|Safe as both use "the same talented actress to explore suburban alienation in comparably gargantuan consumerist surroundings." 

==Awards and nominations==
The film did extraordinarily well in the Village Voice Film Poll|Village Voice s Film Critics Poll of 2002, where Far from Heaven won for best picture, Moore for best lead performance and Haynes for best director and best original screenplay. Lachmans work in Far from Heaven also won best cinematography by a wide margin while Quaid, Clarkson, and Haysbert were all recognized for their supporting performances, placing second, fourth and ninth, respectively. 

{| class="wikitable sortable"
|- Year
!align="left"|Ceremony Category
!align="left"|Recipients Result
|- 2002
|rowspan="7"|7th 7th Satellite Awards Best Film - Drama
|Far From Heaven
| 
|- Best Director Todd Haynes
| 
|- Best Actress - Drama Julianne Moore
| 
|- Best Supporting Actor - Drama Dennis Haysbert
| 
|- Dennis Quaid
| 
|- Best Screenplay - Original Todd Haynes
| 
|- Best Cinematography Edward Lachman
| 
|- 8th Critics Choice Awards Best Actress Julianne Moore
| 
|- 9th Screen Actors Guild Awards Best Female Actor in a Leading Role Julianne Moore
| 
|- Best Male Actor in a Supporting Role Dennis Quaid
| 
|- 18th Independent Spirit Awards Best Feature
|Far From Heaven
| 
|- Best Director Todd Haynes
| 
|- Best Female Lead Julianne Moore
| 
|- Best Supporting Male Dennis Quaid
| 
|- Best Cinematography Edward Lachman
| 
|- 60th Golden Globe Awards Best Actress - Drama Julianne Moore
| 
|- Best Supporting Actor Dennis Quaid
| 
|- Best Screenplay Todd Haynes
| 
|- Best Original Score Elmer Bernstein
| 
|- 75th Academy Awards Best Actress Julianne Moore
| 
|- Best Original Screenplay Todd Haynes
| 
|- Best Cinematography Edward Lachman
| 
|- Best Original Score Elmer Bernstein
| 
|- 2002 Los Angeles Film Critics Association Awards Best Picture
|Far From Heaven
| 
|- Best Director Todd Haynes
| 
|- Best Actress Julianne Moore
| 
|- Best Cinematography Edward Lachman
| 
|- Best Music Score Elmer Bernstein
| 
|- Best Production Design Mark Friedberg
| 
|- 2002 National Society of Film Critics Awards Best Supporting Actress Patricia Clarkson
| 
|- Best Cinematography Edward Lachman
| 
|- 2002 New York Film Critics Circle Awards Best Film
|Far From Heaven
| 
|- Best Director Todd Haynes
| 
|- Best Actress Julianne Moore
| 
|- Best Supporting Actor Dennis Quaid
| 
|- Best Supporting Actress Patricia Clarkson
| 
|- Best Cinematography Edward Lachman
| 
|- Boston Society of Film Critics Awards 2002 Best Actress Julianne Moore
| 
|- Best Cinematography Edward Lachman
| 
|- Chicago Film Critics Association Awards 2002 Best Film
|Far From Heaven
| 
|- Best Director Todd Haynes
| 
|- Best Actress Julianne Moore
| 
|- Best Supporting Actor Dennis Quaid
| 
|- Best Cinematography Edward Lachman
| 
|- Best Original Score Elmer Bernstein
| 
|-
|rowspan="2"|Dallas–Fort Worth Film Critics Association Awards 2002 Best Actress Julianne Moore
| 
|- Best Cinematography Edward Lachman
| 
|- Florida Film Critics Circle Awards 2002 Best Actress Julianne Moore
| 
|- Best Cinematography Edward Lachman
| 
|- Kansas City Film Critics Circle Awards 2002 Best Actress Julianne Moore
| 
|- National Board of Review Awards 2002 Best Actress Julianne Moore
| 
|- Online Film Critics Society Awards 2002 Best Picture
|Far From Heaven
| 
|- Best Director Todd Haynes
| 
|- Best Actress Julianne Moore
| 
|- Best Supporting Actor Dennis Quaid
| 
|- Best Original Screenplay Todd Haynes
| 
|- Best Cinematography Edward Lachman
| 
|- Best Original Score Elmer Bernstein
| 
|- Best Art Direction Peter Rogness Ellen Christiansen
| 
|- Best Costume Design Sandy Powell Sandy Powell
| 
|- Phoenix Film Critics Society Awards 2002 Best Film
|Far From Heaven
| 
|- Best Director Todd Haynes
| 
|- Best Actress Julianne Moore
| 
|- Best Supporting Actor Dennis Quaid
| 
|- Best Screenplay - Original Todd Haynes
| 
|- Best Cinematography Edward Lachman
| 
|- Best Original Score Elmer Bernstein
| 
|- Best Production Design Peter Rogness Ellen Christiansen
| 
|- Best Costume Design Sandy Powell Sandy Powell
| 
|- San Diego Film Critics Society Awards 2002 Best Film
|Far From Heaven
| 
|- Best Actress Julianne Moore
| 
|- San Francisco Film Critics Circle Awards 2002 Best Director Todd Haynes
| 
|- Southeastern Film Critics Association Awards 2002 Best Actress Julianne Moore
| 
|- Best Screenplay - Original Todd Haynes
| 
|- Toronto Film Critics Association Awards 2002 Best Director Todd Haynes
| 
|- Best Actress Julianne Moore
| 
|- Best Supporting Actor Dennis Quaid
| 
|- Vancouver Film Critics Circle Awards 2002 Best Actress Julianne Moore
| 
|- Washington D.C. Area Film Critics Association Awards 2002 Best Actress Julianne Moore
| 
|- Best Supporting Actor Dennis Haysbert
| 
|- Writers Guild of America Awards 2002 Best Original Screenplay Todd Haynes
| 
|}

==Soundtrack==
Far from Heaven was the last film scored by Elmer Bernstein. The albums runtime is 42 minutes and 53 seconds.

#"Autumn in Connecticut" – 3:08
#"Mother Love" – 0:42
#"Evening Rest" – 1:52
#"Walking Through Town" – 1:49
#"Proof" – 1:01
#"The F Word" – 1:11
#"Party" – 0:55
#"Hit" – 2:42
#"Crying" – 1:11
#"Turning Point" – 4:46
#"Cathy and Raymond Dance" – 2:02
#"Disapproval" – 1:00
#"Walk Away" – 2:34
#"Orlando" – 0:56
#"Back to Basics" – 1:47
#"Stones" – 1:44
#"Revelation and Decision" – 4:21
#"Remembrance" – 1:56
#"More Pain" – 4:04
#"Transition" – 0:55
#"Beginnings" – 2:17

==Musical adaptation==
Theatrical songwriting team Scott Frankel and Michael Korie are currently working with Richard Greenberg on an Off Broadway-bound musical adaptation. The musical opened at Playwrights Horizons in Spring of 2013. Kelli OHara starred in the central role. 

==See also==
* Queer Cinema
* Douglas Sirk

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 