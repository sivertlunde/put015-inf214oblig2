Mysterious Intruder
{{Infobox film
| name           = Mysterious Intruder
| image          = Mysterious Intruder poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = William Castle Rudolph C. Flothow Eric Taylor
| story          = Eric Taylor
| narrator       = Otto Forrest
| starring       = Richard Dix Barton MacLane Nina Vale
| music          = George Duning
| cinematography = Philip Tannura
| editing        = Dwight Caldwell
| studio         = Larry Darmour Productions
| distributor    = Columbia Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} noir based on the radio drama The Whistler. Directed by William Castle, the production features Richard Dix, Barton MacLane and Nina Vale.   It is the fifth of Columbia Pictures eight "The Whistler#Films|Whistler" films produced in the 1940s, the first seven starring Dix.

==Plot==
Edward Stillwell, the aged proprietor of a music store, hires private detective Don Gale (Dix) to find Elora Lund, a then 14-year-old who vanished seven years ago at the time her mother died. Stillwell can only pay $100, but hints mysteriously that finding Lund could make Gale a rich man.

A young woman claiming to be Elora Lund shows up at Stillwells shop, supposedly in answer to his newspaper advertisement. Stillwell tells her that her mother gave him some "odds and ends" to sell; he discovered something very valuable among them, but refuses to give her any details until he telephones Gale. Meanwhile, Harry Pontos sneaks into the basement and finds a package marked as belonging to Lund. He grabs it, then stabs Stillwell to death and kidnaps "Elora Lund". Gale informs the reporters at the murder scene that the woman is not Lund. The news results in her release unharmed.

Gale goes to see her; her real name is Freda Hanson, and she is Gales accomplice. From clues that Hanson is able to provide, Gale retraces her steps and finds the house occupied by Pontos. Finding Pontos drunk and passed out, he looks around, but just then, Police Detectives Taggart and Burns bang on the door. Pontos awakens, grabs a gun and a shootout ensues. Gale sneaks away, but is seen by a neighbor and loses a shoe in the process.

Taggart and Burns question Gale, inform him that Pontos is dead and return his shoe. When he refuses to cooperate, they arrest him, but release him a little later (hoping he will lead them somewhere). He goes to see Hanson and finally gets her to admit that what she is after is worth $200,000. When there is a knock on the door, he hides and finds a newspaper clipping indicating that a magnate has offered $100,000 each for two wax cylinder recordings legendary Swedish singer Jenny Lind made shortly before her death.

Meanwhile, the real Elora Lund goes to the police. Taggert and Burns send her to see Gale to try to find out what he knows. She remembers the recordings; Gale offers to secure them for 25% of their value. He persuades her to stay with his associate Rose Deming while he does so.

James Summers, the manager of the apartment building in which Hanson lives, finds her strangled body in her closet. The police figure Gale is guilty, as he was seen leaving her apartment around the time of the murder. Joan Hill, Gales secretary, warns him he is a wanted man. 

He heads to Stillwells place, finding his neighbor and friend, Mr. Brown, dead too. Down in the basement, he sees Summers and an accomplice. They have found the recordings. Summers offers to cut Gale in, but Gale does not like what happened to Summers other partners: Hanson and Pontos. A gunfight breaks out. Gale shoots Summers, grabs the recordings and flees. He telephones police headquarters and announces he has the recordings. When he hears someone coming after him, he fires. He is shot and killed ... by the police. Taggart notes that one of the shots has shattered the recordings.

==Cast==
* Richard Dix as Don Gale
* Barton MacLane as Detective Taggart
* Nina Vale as Joan Hill
* Regis Toomey as James Summers
* Helen Mowery as Freda Hanson
* Mike Mazurki as Harry Pontos
* Pamela Blake as Elora Lund Charles Lane as Detective Burns
* Paul E. Burns as Edward Stillwell
* Kathleen Howard as Rose Deming
* Harlan Briggs as Mr. Brown

==Reception==
Film critic Dennis Schwartz liked the film and wrote a positive film review, "This was the fifth episode in Columbia Pictures "The Whistler" series, and is one of the better ones. William Castle (The Tingler/Strait-Jacket/Shanks (film)|Shanks) directs this low-budget black-and-white enjoyable minor film noir, that comes with a choice narration by the disguised Whistler (Otto Forrest) ...  It has a good performance by Richard Dix as the unscrupulous private detective and a plausible surprise ending." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 