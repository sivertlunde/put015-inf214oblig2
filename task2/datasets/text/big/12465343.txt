Seven Sinners (1940 film)
 
{{Infobox film
| name           = Seven Sinners
| image          = Poster of the movie Seven Sinners.jpg
| caption        = Film poster
| director       = Tay Garnett
| producer       = Joe Pasternak John Meehan Harry Tugend László Vadnay
| starring       = Marlene Dietrich John Wayne
| music          = 
| cinematography = Rudolph Maté
| editing        = Ted J. Kent
| distributor    = Universal Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $760,000 (estimated)
}}

Seven Sinners (UK title Cafe of the Seven Sinners) is a 1940 American adventure film starring Marlene Dietrich and John Wayne in the first of three films they made together.    The film was produced by Universal Pictures in black and white.

==Plot==
The film spotlights the controversial life of torch singer Bijou Blanche (Dietrich), who has been kicked off one South Seas island after another. She is accompanied by naval deserter Edward Patrick Little Ned Finnegan (Broderick Crawford) and magician/pickpocket Sasha Mencken (Mischa Auer). Eventually, she meets a handsome, young naval officer, Lt. Dan Brent (Wayne), and the two fall in love. When Brent vows to marry Bijou, his commander and others plead with him to leave her.

==Cast==
* Marlene Dietrich as Bijou Blanche
* John Wayne as Lt. Dan Brent
* Albert Dekker as Dr. Martin
* Broderick Crawford as Edward Patrick Little Ned Finnegan
* Anna Lee as Dorothy Henderson
* Mischa Auer as Sasha Mencken
* Billy Gilbert as Tony
* Richard Carle as District Officer
* Samuel S. Hinds as Gov. Harvey Henderson
* Oskar Homolka as Antro  Reginald Denny as Capt. Church
* Vince Barnett as Bartender
* Herbert Rawlinson as First Mate James Craig as Ensign
* William Bakewell as Ens. Judson

==See also==
* John Wayne filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 