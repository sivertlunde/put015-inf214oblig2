Sol Madrid
{{Infobox Film
| name           = Sol Madrid
| image          = Solmadpos.jpg
| image_size     = 
| caption        = Original film poster
| director       = Brian G. Hutton
| writer         = 
| narrator       = 
| starring       = David McCallum Telly Savalas
| music          = Lalo Schifrin
| cinematography = 
| editing        = 
| studio          = Hall Bartlett Pictures Inc.
| distributor    = MGM
| released       = February 7, 1968
| runtime        = 90 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Sol Madrid is a 1968 film directed by Brian G. Hutton and filmed in Acapulco. It was released in the UK as The Heroin Gang.

==Plot==
Half a million dollars is stolen from the Mafia by small-time crook Harry Mitchell, who splits it with girlfriend Stacey Woodward and takes off for Acapulco.

The mob sends hit man Dano Villanova to deal with Harry and get the money back. Sol Madrid, an undercover narc, is out to find Harry first, hoping to persuade him to testify against organized crime in court.

Stacey happens to be Villanovas former girlfriend. Things get complicated in Mexico, where a heroin dealer named Dietrich is engaged in criminal activity while Mexican law official Jalisco is on the case. Before she can flee on a yacht, Stacey is taken captive by Villanova and shot up with dope until shes turned into an addict.

Harry is caught and killed.  Jalisco isnt what he seems to be, so Madrid not only must deal with him, but with Villanova and Dietrich as well.

==Cast==
*David McCallum  ...  Sol Madrid  
*Stella Stevens  ...  Stacey Woodward  
*Telly Savalas  ...  Emil Dietrich  
*Ricardo Montalban  ...  Jalisco  
*Rip Torn  ...  Dano Villanova  
*Pat Hingle  ...  Harry Mitchell  
*Paul Lukas  ...  Capo Riccione  
*Michael Ansara  ...  Capt. Ortega  
*Michael Conrad  ...  Scarpi  
==Soundtrack==
{{Infobox album |  
| Name        = Sol Madrid
| Type        = Soundtrack
| Artist      = Lalo Schifrin
| Cover       = Sol Madrid (soundtrack).jpg
| Released    = 1968
| Recorded    = November 9 & 10, 1967  MGM Scoring Stage, Culver City, California
| Genre       = Film score
| Length      = 28:22 MGM  SE 4541 ST 
| Producer    = Jesse Kaye
| Chronology  = Lalo Schifrin Cool Hand Luke (1967)
| This album  = Sol Madrid (1967) The Fox (1968)
}}

The   in 2010 as part of the 5 CD  .  
===Track listing===
All compositions by Lalo Schifrin
# "Sol Madrid (Main Theme)" - 2:00   
# "Fiesta" - 1:35   
# "Staceys Bolero" - 2:36   
# "The Burning Candle" - 2:25   
# "Adagietto" - 2:50   
# "Sol Madrid (Main Theme)" - 1:55   
# "The Golden Trip" - 2:35   
# "Charanga" - 2:20   
# "El Patio" - 2:25   
# "Villanovas Villa" - 2:10   
# "Bolero #2" - 2:07   
# "Villanovas Chase" - 2:07  

===Personnel=== conductor
*Laurindo Almeida - guitar
*Unnamed Orchestra conducted by Robert Armbruster
*George del Barrio - orchestration
==References==
 
==External links==
* 

 

 
 
 
 
 