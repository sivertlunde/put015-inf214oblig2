PVC-1
{{Infobox film
| name           = PVC-1
| image          = Film PVC 1.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Spiros Stathoulopoulos
| producer       = 
| writer         = Spiros Stathoulopoulos   Dwight Istanbulian
| starring       = Daniel Páez   Mérida Urquía   Alberto Zornoza   Hugo Pereira 
| music          = 
| cinematography = Spiros Stathoulopoulos 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        =  85 minutes
| country        = Colombia
| language       = Spanish
| budget         = 
| gross          = 
}}
PVC-1 is a 2007 Colombian drama film directed by Spiros Stathoulopoulos. The plot was inspired by a true story about a pipe bomb improvised explosive device (IED) that was placed around the neck of an extortion victim. This directorial debut premiered at the Cannes International Film Festival in 2007 as an official selection of the Directors Fortnight.  The 84-minute film was shot in one single continuous take without cuts using a Glidecam Smooth Shooter and a Glidecam 2000 Pro camera stabilization system. The film won numerous awards at the International Thessaloniki Film Festival. 

==Plot==
The film, set in Colombia, opens with a gang in a car, carefully looking after a certain package. The gang is led by a violent and volatile man who is only known as Benjamin. A few moments later, the gang arrives at a farm and storm the house, taking the family hostage. Benjamin then says that there is money in the house, and that the family will give it to them if they want to remain unharmed. However, Simon, the man of the house, says that they are wrong and that they are a really poor family who have never had much money. A furious Benjamin holds Simon at gunpoint, while one of his henchmen begins taking measures of all the family members necks, he realizes that the only person who has the "desired" measures is Simons wife, Ofelia. The man then takes out the package seen at the beginning of the film and opens it, revealing 2 pieces of PVC pipes and other instruments; he places the pieces around Ofelias neck and straps them together, forming a "collar bomb". Benjamin then orders the family to lay down on their stomachs, the family complies and, a few minutes later, realize the gang is gone.
 pesos to remove the collar, and at the same time warning them not to tamper with the device or go to the police.

Since the family has no money, they decide to ask help to the police anyway. They use a cellphone to call Ofelias sister so that she in turn can call the police.

The national guard and the Colombian army reacts quickly and arranges a meeting with Ofelia so that they can remove the device. However, the meeting point is far away, and so the family asks a neighbor (who is also unaware of what the collar was) to drive them to the meeting point. He initially complies but then tells them to get off the car after realizing the device is actually a bomb.

The family are forced to walk, but luckily they are given a ride by a couple of men who were pushing a wagon along the train rails. 

Soon after, the device begins making a beeping noise, which terrorizes the family.

They eventually reach the meeting point and meet with Jairo, an officer from the national guards  bomb squad, who begins examining the device. He knows the procedure to remove it, but he lacks the proper tools and is forced to improvise with a kitchen knife, a candle, and a lighter.

The process takes a long time, since the knife is not very sharp and the candle takes a lot to heat it. The army eventually arrives and sets around a perimeter, and a lot of passersby start gathering around the perimeter to watch.

Ofelia, who was initially relatively calm, is now visibly distraught and has a breakdown after Jairo tells her to "excuse her for a second" to which she replies "I have no spare seconds".

Jairo eventually manages to remove a piece from the device, and finds the explosive component. 
He attempts to remove it but realizes its more complicated than that when the device releases toxic fumes from a chemical detonator that was hidden inside.
Jairo barely manages to avoid the fumes. Ofelia however, doesnt and she goes into respiratory arrest, but is helped by the paramedics on site and manages to breathe normally again. Jairo successfully removes the chemical detonator, he reports it to his CO, who congratulates him and reassures Ofelia that she will be fine, Ofelia takes his word and starts calming down. As the CO begins walking to the safe zone, the camera turns black, and an explosion is heard, Ofelias family rush to the site and realize that despite all efforts, the bomb has detonated, killing Ofelia. Jairo can be seen lying on the floor with severe burns on his arm (although it is unknown if he is dead or just injured) the camera then runs towards Ofelias youngest daughter, who is watching In horror from a distance, the film ends with the girl crying and asking "Why God why?". Jairos fate, as well as the gangs, is left unknown.

==References==
 

==External links==
*  
*  

 
 
 
 