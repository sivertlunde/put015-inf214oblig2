Jaathakam
{{Infobox film
| name           = Jaathakam
| image          =
| caption        =
| director       = Suresh Unnithan
| producer       =
| writer         =
| screenplay     = Shari Thilakan Kaviyoor Ponnamma
| music          = R. Somashekharan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1989 Cinema Indian Malayalam Malayalam film,  directed by  Suresh Unnithan. The film stars Jayaram, Shari (actress)|Shari, Thilakan and Kaviyoor Ponnamma in lead roles. The film had musical score by R. Somashekharan.   

==Plot Summary==

(Contains some spoilers)

The story revolves a just-married couple. The Husbands first wife had passed away in an accident. His family now relies heavily on their astrologer for an auspicious match and believe in him completely.
The Wifes family doesnt take astrology so seriously.

After the wedding, the wife confesses her horoscope (Jaathakam) given prior to the wedding was probably incorrect. The Husbands family verifies it and finds out she her horoscope doesnt match with her husbands. They then try to dissolve the marriage - mentally torturing the new wife and tearing her husband between his family and his wife in the process.

The wifes brother (an army officer) comes in and tries to take control of the situation. It is then that the story takes a complete twist leading to a fast-paced later half.

==Cast==
*Jayaram as Madhavanunni Shari as Shyamala
*Thilakan as Appukuttan Nair
*Kaviyoor Ponnamma as Janaki Sithara as Malini Madhu as Ramachandran Nair
*Jagathy Sreekumar as Narayana Panikkar Innocent
*Shammi Thilakan as Chendakkaran
*Sukumaran as Somasekharan Nair
*Premji as Vaidyan

==References==
 

==External links==
*  

 
 
 


 