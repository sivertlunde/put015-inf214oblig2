Anchors Aweigh (film)
{{Infobox film
| name =Anchors Aweigh 
| image          =Anchors_aweigh.jpg
| caption        =original poster
| director       = George Sidney
| producer       = Joe Pasternak
| writer         = Natalie Marcin (story) Isobel Lennart
| starring       = Frank Sinatra Kathryn Grayson Gene Kelly
| music          = Georgie Stoll (musical direction) Axel Stordahl (orchestrations) Calvin Jackson (incidental music)
| cinematography = Charles P. Boyle
| editing        = Adrienne Fazan
| studio         = Metro-Goldwyn-Mayer
| distributor    = Turner Entertainment  (current) 
| released       =  
| runtime        = 143 minutes
| country        = United States
| language       = English
| budget         = $2,580,000  . 
| gross = $7,475,000  
}}

Anchors Aweigh is a 1945 American Technicolor musical comedy film directed by George Sidney and starring Frank Sinatra, Kathryn Grayson, and  Gene Kelly, in which two sailors go on a four-day shore leave in Hollywood, accompanied by music and song, meet an aspiring young singer and try to help her get an audition at Metro-Goldwyn-Mayer. In addition to a live-action Kelly dancing with Jerry Mouse the cartoon mouse, the movie also features José Iturbi, Pamela Britton, Dean Stockwell, and Sharon McManus. 

==Plot==
Joe Brady and Clarence Doolittle are Navy sailors who have a four-day leave in Hollywood. Joe has his heart set on spending time with his girl, the unseen Lola. Clarence, the shy choir boy turned sailor, asks Joe to teach him how to get girls. Donald, a little boy who wants to join the navy, is found wandering around the boulevard by a cop, who takes him to the police station. Clarence and Joe end up being picked up by the cops to help convince Donald to go home. After the two sailors wait at home and entertain Donald, Donalds Aunt Susie arrives. Clarence is smitten with her from the beginning. Susan goes on to tell them that she has been trying to find work in music, and longs to perform with José Iturbi. Trying to make Susan impressed with Clarence, Joe tells her that Clarence is a personal friend of Iturbi, and that he has arranged an audition for Susan with him. That night, they go out to a cafe, where Clarence meets a girl from Brooklyn, and they hit it off. The next day, Joe visits Donalds school, and tells the kids the story of how he got his medal, and how he brought happiness to a lonesome king (played by Jerry Mouse of Tom and Jerry), and joy to the forest animals of the kingdom.

Meanwhile, Clarence has been trying to get into Metro-Goldwyn-Mayer Studios to find Iturbi with no luck. After many failed attempts to find Iturbi, all hope is lost for Joe and Clarence, who want to come clean with Susan and tell her there was no audition. On Clarence and Joes last day of leave, Susan runs into Iturbi at a café, who has no idea of the audition. Susan begins to call Joe, whom by now she has fallen in love with, to yell at him. Iturbi stops her and agrees to get her a screen test, which turns out to be very successful. The movie ends as Iturbi conducts the choir in singing "Anchors Aweigh", and Joe, Susan, Clarence and the girl from Brooklyn kiss.

==Cast==
*Frank Sinatra – Seaman First Class Clarence "Brooklyn" Doolittle
*Kathryn Grayson – Susan Abbott
*Gene Kelly – Petty Officer Second Class|Gunners Mate Second Class Joseph "Joe" Brady
*José Iturbi – Himself
*Dean Stockwell – Donald Martin
*Billy Gilbert- Café Manager
*Pamela Britton – The Girl From Brooklyn  Leon Ames – Admirals aide reads citation for medals
*Grady Sutton – Bertram Kraler
*Sara Berner – Jerry Mouse (in Tom and Jerry cartoon sequence)

==Songs==
* "Main Title" – MGM Studio and Orchestra
* "Anchors Aweigh" – MGM Studio and Orchestra and Jose Iturbi
* "We Hate to Leave" – Gene Kelly & Frank Sinatra
* "Brahmss Lullaby" – Frank Sinatra (Sang to Donald to come back again with Joe)
* "I Begged Her" – Gene Kelly & Frank Sinatra
* "If You Knew Susie" – Frank Sinatra & Gene Kelly Jealousy – Kathryn Grayson
* "What Makes the Sunset" – Frank Sinatra
* "(All of a Sudden) My Heart Sings" – Kathryn Grayson
* "The Donkey Serenade" – Jose Iturbi
* "The King Who Couldnt Sing and Dance" – Gene Kelly
* "The Worry Song" – Gene Kelly & Sara Berner (as Jerry Mouse from the cartoons "Tom and Jerry")
* "The Charm of You" – Frank Sinatra (featuring a rare appearance of Guitarist Benito Mayorga with the orchestra)
* "Las Chiapanecas" – Gene Kelly & Sharon McManus
* "Hungarian Rhapsody No. 2|Liszts Hungarian Rhapsody No. 2" – Jose Iturbi
* "I Fall in Love Too Easily" – Frank Sinatra
* "La cumparsita" – Gene Kelly (Music by Francisco Mayorga and "The Guadalajara Trio")
* "Waltz Serenade" – Kathryn Grayson - Pyotr Ilyich Tchaikovsky Anchors Aweigh (Reprise)" – Dean Stockwell Anchors Aweigh (Reprise 2)" – MGM Studio and Orchestra Chorus (THE END)

==Background== Take Me On the Town, both in 1949. The production tried to mix some of the more successful story elements and set-pieces from earlier MGM musical hits, such as Meet Me in St. Louis.
 Academy Award Best Music, Best Actor Best Cinematography, Best Music, Best Picture.
  Roy Disney Bob Thomass book on Roy Disney, the studio was in debt after World War II and they were focusing on trying to get their own films out on time. According to Roy, they had no business making cartoons for other people. 
 Serenade for title tune. Many of the memorable scenes in this film were later featured in the Thats Entertainment! tributes to MGM.

==Reception==
According to MGM records the film earned $4,498,000 in the US and Canada and $2,977,000 elsewhere, resulting in a profit of $2,123,000. 

==Awards and nominations==
* Winner: Academy Award, Original Music Score, Georgie Stoll (although the contributions of one of the first black composers and pianists in the MGM music department, Calvin Jackson, went uncredited,  this was not uncommon even for white studio musicians)
* Nominated: Academy Award, Best Picture
* Nominated: Academy Award, Best Actor, Gene Kelly
* Nominated: Academy Award, Best Cinematography (Color)
* Nominated: Best Song, "I Fall In Love Too Easily" (Words and Music by Sammy Cahn and Jule Styne, Sung by Frank Sinatra)

==In popular culture==
* Paula Abdul was inspired by the scene of Kelly dancing with Jerry to create the video for her song "Opposites Attract", where she dances with an animated cat.
* Several Tom & Jerry shorts have bits from The Worry Song scene played in their score. Mainly when its Jerrys moment.
* Family Guy episode "Road to Rupert" reworked the Jerry Mouse dancing scene replacing Jerry with Stewie Griffin, though Jerrys reflection on the floor is still visible.

==References==
 

==Further reading==
* 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 