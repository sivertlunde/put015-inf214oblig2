The Adventures of Peg o' the Ring
 
{{Infobox film
| name           = The Adventures of Peg o the Ring
| image          = The Adventures of Peg o the Ring.jpg
| caption        = Theatrical poster to The Adventures of Peg o the Ring Francis Ford Jacques Jaccard
| producer       =
| writer         = Joe Brandy Grace Cunard Francis Ford
| cinematography =
| editing        = Universal Film Manufacturing Co.
| released       =  
| runtime        = 300 minutes (15 episodes)
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 drama film Francis Ford and Jacques Jaccard. It is now considered to be lost film|lost.   

==Cast==
* Grace Cunard - Peg Francis Ford - Dr. Lund, Junior
* Mark Fenton - Dr. Lund, Senior (as Marc Fenton)
* G. Raymond Nye
* Peter Gerald - Flip the Clown (as Pete Gerald)
* Jean Hathaway - Mrs. Lund
* Charles Munn
* Irving Lippner - Marcus, the Hindoo Jack Duffy
* John Ford - Lunds Accomplice (as Jack Ford)
* Lionel Bradshaw
* Eddie Polo - (scenes deleted)
* Ruth Stonehouse - (scenes deleted; but stills of her in the film survive)

==Chapter titles==
 
# The Leopards Mark
# A Strange Inheritance
# In The Lions Den
# The Circus Mongrels
# The House of Mystery
# The Cry For Help or Cry of The Ring
# The Wreck
# Outwitted
# The Leap
# In the Hands of The Enemy
# The Stampede
# On The High Seas
# The Clown Act
# The Will
# Retribution

==See also==
* List of film serials
* List of film serials by studio
* List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 