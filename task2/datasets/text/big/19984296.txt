Swapner Feriwala
{{Infobox film
| name = Swapner Feriwala
| image =Swapner Feriwala.jpg
| director = Subrata Sen
| writer = Subrata Sen
| starring = Subrata Dutta Ferdous Ahmed Nilanjana Sharma
| producer = Arya Bhattacharya
| distributor =
| cinematography = Shirsha Roy
| editing = Raviranjan Maitra
| released = 6 June 2002
| runtime =  119 min. Bengali
| music = Debojyoti Mishra
}} Bengali film. Directed by Subrata Sen, it featured Subrata Dutta, Ferdous Ahmed, Nilanjana Sharma. The film is Nilanjana Sharmas  cinematic debut. 
{{cite web
|url=http://www.telegraphindia.com/1060317/asp/etc/story_5969146.asp
|title=I am not selling sex
|publisher=www.telegraphindia.com
|date= 17 March 2006
|accessdate=2008-10-29
|last=
|first=
}}
  
{{cite web
|url=http://www.arjoe.com/sferi/press_indiatoday.shtml
|title=Review in India Today
|publisher=www.arjoe.com
|accessdate=2008-10-29
|last=
|first=
}}
  
{{cite web
|url=http://www.arjoe.com/sferi/press_ht1.shtml
|title=Review in Hidustan Times
|publisher=www.arjoe.com
|accessdate=2008-10-29
|last=
|first=
}}
 

== Plot ==
Swapner Feriwala is a soothing fantasy in the pursuit of the last magical charms in our modern, urban lives. Its a symbolic story of youthful and vigorous rebellion against age-old orthodoxy and religious fanaticism and a clarion call to set everyone free of blind prejudices. 

Saritsekhar Roychowdhury, the old descendant of a renowned family, gets obsessed with the attainment of spiritual emancipation and brings a "godman" (Dipankar De) to the house to help him find the true way to freedom.  The latter him the confidence of everyone in the family through his Tantra tricks and promises the childless couple a child within a year. 

Som (Subrat Dutta) an iconoclast magician and his friend, a fellow magician named Ferdoup Siddhartha, have devoted their lives to uncover the magic rackets fooling people with petty magic tricks. Som is an enigma, while Siddhartha believes the magic to pervade the beauty of nature, the vastness of the universe, the beautiful movements of togetherness and entire human existence.  He wants only to drink the elixir of life and insists Som stop hankering after an elusive goal. The evil godman plots to seize the Roychowdhury house from Sanitsekhar and lay his hands on young, pretty Turni (Nilanjana Sharma), the only granddaughter of Saritsekhar, who lost her parents at an early age. 

Som and Sidhartha befriend Turni and warn her about the upcoming danger in her life.  The godman reduces the woman in his aid and entrusts upon her the duty to keep an eye on Turni and to lure her closer to him. Turni escapes their evil plans with the help of the magician duo at first but soon she feels to be at her wits end when her grandfather donates his house to the godman to build an ashram there. She could not settle elsewhere like her two uncles, being financially dependent on her grandfather. The satanic duo threatens Turni to follow their instructions to stay at the ashram and finally traps her and gets her intoxicated. As the sinister godman pounces upon her, the magician duo comes at her rescue and brings her to consciousness. The godmen fly away while the trio set on a journey.

By then, Turni has fallen for Som while Siddhartha loves her with his whole heart. When she reveals her feeling to Som, he resists her and leaves to follow his destiny. Siddhartha consoles the bereaved Turni and promises to stay with her forever. She recognises her true love in Siddhartha with the help of the magical hourglass, given by her mother and together they start a new journey.

== Cast ==
* Subrat Dutta as  Shome
* Ferdous Ahmed as Siddhartha
* Nilanjana Sharma as Turni
* Dipankar Dey as Godman (as Dipankar De)
* Baisakhi Marjit as Godmans lady assistant
* Haradhan Bannerjee as Turnis grandfather
* Gautami Bhattacharya as Turnis aunt
* Bodhisattva Mazumdar as Turnis uncle

== Awards ==
* BFJA Awards  (2003)

* Most Promising Actress Nilanjana Sharma

* Official selection at Karlovy Vary Film Festival 
{{cite web
|url=http://www.gomolo.in/Movie/MovieAwards.aspx?mid=15217
|title=Swapner Feriwala(2002)
|publisher=www.gomolo.in
|accessdate=2008-10-29
|last=
|first=
}}
 

== References ==
 

== External links ==
*  

 

 
 
 
 