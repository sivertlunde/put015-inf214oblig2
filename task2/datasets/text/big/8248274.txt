Bread and Chocolate
{{Infobox film
| name = Bread and Chocolate (Pane e cioccolata)
| image = Breadandchocolate.jpg
| caption = DVD cover
| director = Franco Brusati
| producer = Maurizio Lodi-Fe Turi Vasile
| writer = Franco Brusati Jaja Fiastri Nino Manfredi
| narrator =
| starring = Nino Manfredi Johnny Dorelli Anna Karina
| music = Daniele Patucchi
| cinematography = Luciano Tovoli
| editing = Mario Morra
| distributor = Cinema International Corporation|Cinéma International Corporation
| released = 1974 (Italy, France) July 14, 1978 (USA)
| runtime = 111 minutes {{Citation
  | last = Canby
  | first = Vincent
  | author-link = 
  | title = Movie Review&nbsp;— Pane e Cioccolata (1973)
  | newspaper = The New York Times
  | pages = 
  | year = 
  | date = July 14, 1978
  | url = http://movies.nytimes.com/movie/review?res=9A01E6D61E31E632A25757C1A9619C946990D6CF
  | accessdate =December 24, 2009
}} 
| country = Italy
| language = Italian, German, English
| budget =
}}
Bread and Chocolate ( ) is a 1974 Italian comedy-drama film directed by Franco Brusati. This film chronicles the misadventures of an Italian immigrant to Switzerland and is representative of the commedia allitaliana film genre.

==Plot== guest worker" Greek woman. Then he befriends an Italian industrialist, relocated to Switzerland because of financial problems. The industrialist takes him under his wing, only to commit suicide when he squanders his last savings. Nino is constrained to find shelter with a group of clandestine Neapolitans living in a chicken coop, together with the same chickens they tend to in order to survive. Captivated by the idyllic vision of a group of young blonde, Swiss youths, he decides to dye his hair and pass himself off as a local. In a bar, when rooting for the Italian national football team during its transmission, he is found out after celebrating a goal scored by Fabio Capello. He is arrested and deported. He embarks on a train and finds himself in a cabin filled with returning Italian guest workers. Amid the songs of "sun" and "sea", he is seen having second thoughts. He gets off at the first stop: better life as an illegal immigrant than a life of misery.

==Cast==
* Nino Manfredi as Nino Garofalo
* Johnny Dorelli as Italian Industrialist
* Anna Karina as Elena
* Paolo Turco as Gianni
* Ugo DAlessio as Old Man
* Tano Cimarosa as Giacomo
* Gianfranco Barra as The Turk
* Giorgio Cerioni as Police Inspector
* Francesco DAdda as Rudiger
* Geoffrey Copleston as Boegli
* Federico Scrobogna as Grigory
* Max Delys as Renzo
* Umberto Raho as Maitre
* Nelide Giammarco as The Blonde
* Manfred Freyberger as Lo svizzero sportivo

==Awards==
* The film won several international awards including the Silver Bear at the 24th Berlin International Film Festival in 1974.   
* New York Film Critics Circle Award for Best Foreign Language Film 1978

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 