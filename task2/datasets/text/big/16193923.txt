Armin (film)
{{Infobox Film
| name           = Armin
| caption        = 
| image	=	Armin FilmPoster.jpeg
| director       = Ognjen Sviličić
| producer       = Marcelo Busse Mirko Galić Markus Halberschmit Ademir Kenović Damir Terešak
| writer         = Ognjen Sviličić
| starring       = Emir Hadžihafizbegović Armin Omerović
| music          = Michael Bauer
| cinematography = Stanko Herceg Vedran Šamanović
| editing        = Vjeran Pavlinić
| distributor    = 
| released       = 9 February 2007 (Berlin Film Festival)
| runtime        = 82 minutes
| country        = Croatia English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Armin is a Croatian/Bosnian drama film directed by Ognjen Sviličić that premiered at the 2007 Berlin Film Festival.

==Plot== Bosnia to audition in epileptic seizure. As they get ready to head back to Bosnia and Herzegovina|Bosnia, the film crew makes an unexpected offer, but when Ibro refuses, Armin at last realizes how much his father really loves him.

==Awards==
Armin was selected to be Croatias submission for the Best Foreign Language Film at the 80th Academy Awards . Karlovy Vary Palm Springs Durban Film Festival. It received the following awards:
 2007 Durban International Film Festival
**Best Actor (Emir Hadžihafizbegović) 2007 Festróia - Tróia International Film Festival
**Silver Dolphin for Best Screenplay (Ognjen Sviličić) 2007 Karlovy Vary International Film Festival
**East of West Award 2007 Pula Film Festival
**Golden Arena for Best Actor in a Leading Role (Emir Hadžihafizbegović)
**Golden Arena for Best Screenplay (Ognjen Sviličić) 2008 Palm Springs International Film Festival
**FIPRESCI Award for Best Foreign Language Film of 2007

==External links==
* 
* 
* 
*  
* 

 

 
 
 
 
 
 
 
 
 
 


 