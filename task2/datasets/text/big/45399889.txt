Angela (1955 film)
 
{{Infobox film
| name           = Angela
| image          = File:Angela_(1955)_film_poster.jpg
| image size     =
| caption        = Theatrical poster
| director       = Dennis OKeefe
| producer       = Steven Pallos Augusto Fantechi
| writer         = Dennis OKeefe
| narrator       =
| starring       = Dennis OKeefe Mara Lane Rossano Brazzi Arnoldo Foà
| music          = Mario Nascimbene
| cinematography = Leonida Barboni 
| editing        = Giancarlo Cappelli Twentieth Century-Fox Film Corporation
| released       = 1954 (Italy) 1955 (United States)
| runtime        = 81 min.
| country        = United States Italy English Italian Italian
| budget         =
}} Italian film film noir type of Angela has been described as femme fatale with elements of betrayal and obsession. 

==Plot== American G.I. heart attack in her apartment. OKeefe places the body in the trunk of the wrong car and watches helplessly as Lane drives away with it. O’Keefe then goes about trying to fix the situation, but a police inspector (Arnoldo Foà) and Lane’s sadistic husband (Rossano Brazzi) have ulterior motives in store for him.  

==Cast==
*Dennis OKeefe - Steve Catlett 
*Mara Lane - Angela Towne 
*Rossano Brazzi - Nino
*Arnoldo Foà - Captain Ambrosi
*Galeazzo Benti - Gustavo Venturi
*Enzo Fiermonte - Sergeant Collins
*Nino Crisman - Bertolati
*Giovanni Fostini - Tony
*Francesco Tensi - Dr. Robini
*Maria Teresa Paliani - Beautician
*Gorella Gori - Nurse
*Aldo Pini - Doorkeeper

==Reviews and reception== Hal Erickson reviewed the film for AllMovie and opined that it "lacks the pacing and punch necessary to sustain audience empathy", but OKeefe still "knows how to frame a scene and get the most out of his largely unknown cast".  The Internet Movie Database rates the film a 5.4/10, based on user reviews.  Michael Keaney wrote in his book, Film noir guide, that "O’Keefe is okay as the American patsy, as is Italian romantic lead Brazzi as Lane’s sadistic husband, but the all-too-familiar plot doesn’t make the grade". 

==See also==
*List of film noir titles

==References==
 

==External links==
* 

 
 
 
 
 
 
 