Buddha Mountain
{{Infobox film
| name           = Buddha Mountain
| image          = Buddha Mountain.jpg
| border         = yes
| caption        = Li Yu
| producer       =
| writer         = 
| starring       = Sylvia Chang Fan Bingbing Chen Bolin
| music          = Peyman Yazdanian
| cinematography = Zeng Jian
| editing        = Zeng Jian, Karl Riedl
| distributor    = Golden Scene
| studio         = Laurel Films Huaxing Real Estate
| released       =  
| runtime        = 104 minutes
| country        = China
| language       = Mandarin
}} Li Yu and starring Sylvia Chang, Fan Bingbing and Chen Bolin. It was produced by Laurel Films, a small independent production company owned by Fang Li and based in Beijing. Laurel Films also produced Li Yus previous film Lost in Beijing.

This film chronicles the lives of three youths who have no intention of sitting exams and getting into universities and a retired Chinese opera singer who is mourning the death of her son. The film explores themes of teenage confusion, angst, and rebellion and the impermanence of life.

== Plot ==
When singing on a pub stage, Nan Feng (Fan Bingbing) knocks a man unconscious. Nan Feng, her boyfriend Ding Bo (Chen Bolin), and another friend eat and drink by the roadside.
Nan Feng, Ding Bo, Fatso and Teacher Chang are eating the supper. They are waiting for a train home at Buddha mountain railway station. When they back home, they put 2800 RMB in the suitcase.

The repaired car driven by Ding Bo stopped in front of them when they are eating by the road. Then they get in the car and stopped at a destroyed building. By the flashback scenes, we know this building was destroyed by the 2008 Sichuan earthquake. They took a picture helped by a monk in front of the destroyed Buddha temple.

Nan Feng, Ding Bo, Fatso and Teacher Chang go to the destroyed Buddha temple. They make some repairing and painting, and hang a bell. In the night, they talk to each other. The monk says his master’s real body is in the temple. Teacher Chang says she has done all she has to do. The next morning Nan Feng and his friends seek Teacher Chang and they find Teacher Chang is on the opposite cliff top. Nan Feng looks down to the cliff to see the train passing by, when she raises her head, she finds Teacher Chang is disappeared. Finally, they know Teacher Chang jumped off the cliff and died.

==Awards==
{| class="wikitable"
|-
! Year !! Group !! Award !!  Result
|- 2010
| Tokyo International 23rd Tokyo International Film Festival  
| Best Actress – Fan Bingbing 
|  
|-
| Best Artistic Contribution – Li Yu 
|  
|- 2011
| Casa Asia Film Week   
| Best Film 
|  
|- 18th Beijing College Student Film Festival  
| Best Actress – Fan Bingbing
|  
|}

==Alternate versions==
This films release in China consisted of a version different from the version seen at Tokyo International Film Festival. The deleted content include the forced demolitions and the beginning scenes (Sylvia Changs role originally, was at the beginning of the film. She had an appearance during a scene in the Beijing Opera Troupe but her position in the troupe is replaced by another actor, so she was frustrated when the three youngsters first meet her. This clip is deleted so the role of teacher chang could be more complete). Director Li Yu stated that the deletion was not the request of  the State Administration of Radio, Film, and Television, instead, it was simply to make the whole movie rhythm better and overall narrative smoother. 

== Reception ==
The film received generally positive reviews from critics. The film was a financial success, with a domestic gross of more than 70 million RMB.   The Hollywood Reporter criticized this film could easily have been a rote, melodramatic weeper but is saved from that fate by some astute writing, strong performances and an almost utter dearth of expected devices and although there are jumps in the growth of the characters, its hard to find serious fault when the film has such an intense veracity otherwise.  The variety (Magazine)|Variety wrote younger thesps also impress, particularly Fan, who makes Nan Feng both childlike and fearsome. Chens Ding Bo is less detailed, but scenes with the characters father (producer/co-writer Fang Li) give the thesp opportunity to explore greater emotional depths. 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 