Four of a Kind (film)
{{Infobox film
| name = Four of a Kind
| image = Fourofakind.png
| caption = Theatrical release poster
| director = Fiona Cochrane
| producer = Fiona Cochrane Leverne McDonnell
| writer = Helen Collins
| starring = Leverne McDonnell Gail Watson Nina Landis Louise Siversen Ben Steel
| music = Joe Camilleri The Black Sorrows
| cinematography = Zbigniew Friedrich
| editing = Zbigniew Friedrich
| released =  
| country = Australia
| language = English
}} 
Four of a Kind is the debut feature film for director Fiona Cochrane. It was completed in 2008 and released in 2009. 

It is based on the stage play Disclosure (play)|Disclosure by Helen Collins as presented at La Mama Theatre (Melbourne) during the 2006 Melbourne Fringe Festival.  It was shot on location in Melbourne, Australia.

Synopsis: Lies. Betrayal. Blackmail. Murder.

Four different women, each with a well-hidden secret they are coaxed, tricked or forced into revealing. 
Through a veil of lies all four flirt with the truth as they experience betrayal, ambition, loneliness, pain and anger.  
But the lies they tell themselves might be the ones that hurt the most.

" … beneath its deceptively simple surface  lies an emotionally lacerating psychological whodunit of unusual complexity" - Paul Harris, Film Buffs Forecast

Not far off being a hidden Australian gem that you should race out to- Ben McEachen, Empire Magazine

‘Tingling with adult tension!’ - John Flaus, The Melbourne Review

==Reception==
Film reviews include:
*  
 
*    
*  
*  

==References==
 

==External links==
* Trailer:  http://www.youtube.com/watch?v=KwViMWp1YFI&feature=channel&list=UL
*  
*  
*  
*  
*  

 
 
 

 