Tirza
 
{{Infobox film
| name           = Tirza
| image          = Tirza.jpg
| caption        = Film poster
| director       = Rudolf van den Berg
| producer       = Jeroen Koolbergen San Fu Maltha
| writer         = Rudolf van den Berg (script) Arnon Grunberg (book)
| starring       = Sylvia Hoeks
| music          = 
| cinematography = Gábor Szabó
| editing        = Job ter Burg
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}} Best Foreign Language Film at the 83rd Academy Awards    but it didnt make the final shortlist.   

==Cast==
* Gijs Scholten van Aschat as Jörgen
* Sylvia Hoeks as Tirza
* Johanna ter Steege as Alma
* Abbey Hoes as Ibi
* Titia Hoogendoorn as Ester
* Nasrdin Dchar as Choukri
* Keitumetse Matlabo as Kaisa

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Dutch submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 