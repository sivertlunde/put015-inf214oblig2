The Gate of Sun
 
{{Infobox film
| name           = The Gate of Sun
| image          = 
| caption        = 
| director       = Yousry Nasrallah
| producer       = Humbert Balsan
| writer         = Yousry Nasrallah Elias Khoury Muhammad Suwaid
| starring       = Hiam Abbass
| music          = 
| cinematography = Samir Bahzan
| editing        = Luc Barnier
| distributor    = 
| released       =  
| runtime        = 278 minutes
| country        = Egypt France
| language       = Arabic
| budget         = 
}}

The Gate of Sun ( , Transliteration|translit.&nbsp;Bab el shams,  ) is a 2004 French-Egyptian war film directed by Yousry Nasrallah. It was screened out of competition at the 2004 Cannes Film Festival.   

==Cast==
* Hiam Abbass - Um Youness
* Fady Abou-Samra - Dr. Amgad
* Hussein Abou Seada - Colonel Mehdi
* Mohamed Akil - Responsible OLP
* Ahmad Al Ahmad - Adnan
* Vivianne Antonios - Epouse de Sameh
* Muhtaseb Aref - Sheikh Ibrahim
* Gérard Avedissian - Barman
* Antoine Balabane - Georges
* Béatrice Dalle - Catherine
* Darina El Joundi - Femme fantôme
* Maher Essam - Selim
* Hanane Hajj Ali - Zeinab
* Mohamed Hedaki - Abou Essaf
* Talal Jordy - Tortionnaire
* Bassel Khayyat - Khaleel
* Ragaa Kotrosh - Om Soliman
* Kassem Melho - Ostaz Youssef
* Orwa Nyrabia - Youness
* Hala Omran - Shams
* Nadira Omran - Um Hasan
* Bassem Samra - Interrogateur
* Wissam Smayra - Himself
* Rim Turkhi - Nahila

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 