Jackson County Jail (film)
{{Infobox film
| name           = Jackson County Jail
| image          = Film_Poster_for_Jackson_County_Jail.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Michael Miller
| producer       = Jeff Begun Roger Corman  (executive producer) 
| writer         = Donald E. Stewart
| narrator       =
| starring       = Tommy Lee Jones Yvette Mimieux Lisa Copeland
| music          = L. Loren Newkirk
| cinematography = Bruce Logan
| editing        = Caroline Biggerstaff
| studio         = New World Pictures
| distributor    = United Artists
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $500,000 Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 100-102 
| gross          = $2.3 million 
| website        =
| amg_id         =
}}
Jackson County Jail is an American exploitation action thriller film from 1976 directed by Michael Miller, starring Tommy Lee Jones, Yvette Mimieux and Lisa Copeland.

==Plot==
Dinah Hunter (Mimieux) works for a Los Angeles advertising agency and has been involved in a two-year romantic relationship. After she catches the man being unfaithful to her, Dinah quits her job and drives to New York for a fresh start.

On a rural highway in the West, she picks up two hitchhikers. They rob and assault her, stealing her purse and car. A law enforcement officer then places Dinah herself under arrest when she can produce no identification. He locks her in a cell of the Jackson County jail, where one of the guards brutally beats and rapes her.

Dinah kills her jailer and escapes with the help of Coley, a thief who likes to hijack trucks. A bond forms between them as Dinah and Coley flee the law, even though he could be a deranged killer as well. They make a stand together in a final shootout with police.

==Cast==
* Yvette Mimieux as Dinah Hunter
* Tommy Lee Jones as Coley Blake
* Severn Darden as Sheriff
* Robert Carradine as Bobby Ray

==Production notes== Walking Tall; 1974s The Texas Chain Saw Massacre; 1975s Macon County Line and 1976s The Town That Dreaded Sundown) when most, if not all, of what was portrayed on screen was outright fiction.

==Response==
The film has become a cult movie. In 1996, it was selected by film director Quentin Tarantino for the first Quentin Tarantino Film Festival in Austin, Texas. 

Film critic Danny Peary lists the film as one of his "Must See" Films in his Guide for the Film Fanatic (1986). 

== References ==
 

==External links==
* 

 
 
 

 