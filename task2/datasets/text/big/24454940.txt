Geedka nolosha
{{Infobox Film
| name           = Geedka nolosha
| image          = 
| caption        = 
| director       = Abdulkadir Ahmed Said
| producer       = Jubba Enterprises/Somali Film Agency
| writer         = Abdulkadir Ahmed Said
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1988 
| runtime        = 23 min.
| country        =   Turin Film Festival Somali
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Somali writer Torino International Festival of Young Cinema.

==Plot==
In a small town, a nomad cuts down a tree he needs to use. However, he accidentally chops down the wrong tree, felling the tree that sustains life itself. When the latter tree falls, all life disappears. The nomad then finds himself in the middle of a desert that he does not recognize, and he tries to work his way out of it. During this particular quest, he comes across a small tree that is in the process of dying and opts instead to save this one. Association, p.407 

==Other==
Geedka nolosha was shot on 16 mm film. 

==Notes==
 

==References==
*{{cite book
  | last = Association des trois mondes
  | first = 
  | authorlink = 
  | coauthors = 
  | title = Cinémas dAfrique
  | publisher = KARTHALA Editions
  | year = 2000
  | location = 
  | pages = 
  | url = http://books.google.ca/books?id=61Ol3PLhYhsC
  | doi = 
  | id = 
  | isbn = 2-84586-060-9}}

==External links==
* 

 
 
 
 
 


 