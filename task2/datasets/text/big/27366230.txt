Vendetta (1995 film)
{{Infobox film
| name           = Vendetta 
| image          = 
| image_size     = 
| caption        = 
| director       = Mikael Håfström
| producer = Ingemar Leijonborg Hans Lönnerheden
| writer         = Thomas Borgström Lars Bill Lundholm
| based on       =  
| narrator       = 
| starring       = Stefan Sauk Francis Shaw
| cinematography = Erling Thurmann-Andersen 
| editing        = Sofia Lindgren
| studio         =  SF International Sales
| released       = February 10, 1995
| runtime        = 135 min.
| country        = Sweden
| language       = English
| budget         = 
| gross          = 
}} Carl Hamilton and Pasquale Anselmo as his adversary. 

==Plot==
Carl Hamilton gets assigned to go to Sicily in order to free two Swedish citizens who are kept captured by a local villain named Don Tommaso. In the beginning Hamilton acts merely as a negotiator but when he cannot meet Don Tommasos expectations the situation escalates. Hamilton has to prove he can stand up against Don Tommaso.

==Cast== Carl Hamilton
*Pasquale Anselmo as Enrico
*Carlo Barsotti as Don Giovanni
*Renato Carpentieri as Alda
*Leonardo De Carmine as Roberto

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 

 