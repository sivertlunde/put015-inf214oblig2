Kuch Tum Kaho Kuch Hum Kahein
{{Infobox film
| name           = Kuchh Tum Kaho Kuchh Hum Kahein
| image          = KTKKHK.jpg
| image size     =
| director       = K. Ravishankar 
| producer       = D. Ramanaidu 
| writer         = Dheenaraj
| narrator       = 
| starring       = Fardeen Khan Farida Jalal Govind Namdeo Mukesh Tiwari
| music          = Anu Malik
| cinematography = Venkat R. Prasad
| editing        = Bhanodaya
| studio         = Suresh Productions 
| distributor    = Dream Merchants Enterprise
| released       = June 28, 2002
| runtime        = 
| country        = India
| language       = Hindi
}}
 romance drama film directed by Ravi Sharma Shankar. The film stars Fardeen Khan opposite debutant Richa Pallod. It released worldwide on 28 June 2002, and opened to a mixed response from critics and public.  The story is based on two lovers who are unable to start an relationship because of their familys feud and ancient grudge. The storyline is a remake of the Telugu movie Kalisundam Raa, and features similarities with William Shakespeares classical story, Romeo and Juliet.

==Plot summary==
Vishnupratap Singh (Vikram Gokhale) is celebrating his 60th birthday in his village with his wife (Farida Jalal). At the behest of his wife, Vishnu Pratap calls his estranged sons family from Bombay for the event. Their grandson, Abhay (Fardeen Khan), arrives with his mother and sister to the village, and is initially greeted with animosity. However, he shows a kind and caring side that wins the affections of his family members. He also starts to become friendly with Mangala (Richa Pallod), and the two start playing pranks and teasing each other, which eventually leads to them falling in love. Abhay ends up getting into a fight with the family of Virendra Pratap (Govind Namdeo), specifically his nephew, Rudra Pratap (Sharad Kapoor). Vishnu Pratap slaps Abhay afterwards for his actions, even though he was defending himself against Rudra.

Abhay finds out Rudras anger towards his family is a deep-rooted family feud. Virendra Pratap and Vishnu Pratap are brother-in-laws who were once very close friends after Vishnu married his sister to Virendra. 25 years before, they decided that Vishnu Prataps son, Abhays father, Indra, would marry Virendra Prataps niece. However, Indra was in love with Abhays mother, and ran away on the day of the wedding to avoid the marriage. The bride, despondent over her humiliation, committed suicide that day by setting herself on fire. Virendra Prataps brother and son-in-law died that day, as a result of the fight that broke out at the wedding. Vishnu Pratap blames his son for the discourse within the family, and shunned him and his children, as a result. Abhay, hearing this, resolves to reunite the two families in some way. Rudra, however, is determined to kill Abhay and his family to avenge the deaths of his sister and father.

Abhay eventually succeeds in reconciling the two families, and decides to ask his grandfather for permission to marry Mangala. However, Mangalas marriage is arranged to a member of Virendra Prataps family in order to reunite the families permanently. Abhay sacrifices his love for Mangala in order to let the families come back together. However, Mangala refuses to agree, and runs away to the station, where Abhay tries to stop her from leaving for the sake of their family. The families eventually find out about Mangala running away, and Rudra goes after her, intending to kill Abhay. The two get into a fight, until Rudra stabs Abhay. At that point, Vishnu Pratap and Virendra Pratap show up with the family after finding out about Abhay and Mangalas love, and realizing the mistake theyve made. Rudra also comes to realize his mistake, and he reunites Abhay and Mangala, as the families become one and reconcile.

== Cast ==

*Fardeen Khan as Abhay Indra Vishnupratap Singh
*Vikram Gokhale as Vishnu Pratap Singh
*Richa Pallod as Mangla
*Farida Jalal as Mangla Dadi
*Sanjai Mishra

==Box-Office==
It didnt do well at the box office and declared an Average though to do well 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! Title !! Singer(s)
|-
| Aa Ra Raa Ra Raa
| Fardeen Khan, Sonu Nigam
|-
| Chudi Chudi 
| Shaan (singer)|Shaan, Preeti & Pinky, Prashant
|-
| Hua Salaam Dil Ka
| Udit Narayan, Alka Yagnik
|-
| Jab Se Dekha Tumko
| Kumar Sanu, Alka Yagnik
|-
| Kuch Tum Kaho Kuch Hum Kahein Hariharan
|-
| Tu Hi Hai
| Shaan, Sunidhi Chauhan
|-
| Yeh Kya Majboori
| Krishnakumar Kunnath|KK, Prashant
|}

==References==
 

== External links ==
* 

 

 
 
 
 