Master and Commander: The Far Side of the World
{{Infobox film name = Master and Commander: The Far Side of the World image = Master and Commander-The Far Side of the World poster.png caption = Theatrical release poster screenplay = Peter Weir John Collee based on =   starring = Billy Boyd James DArcy Edward Woodall director = Peter Weir producer = Samuel Goldwyn, Jr. Peter Weir Duncan Henderson production companies Miramax Films Universal Pictures Samuel Goldwyn Films distributor = 20th Century Fox  released =   runtime = 138 minutes country = United States language = English cinematography = Russell Boyd editing = Lee Smith music = Iva Davies Christopher Gordon Richard Tognetti budget = $150 million  gross = $212 million
}} epic historical Miramax Films, Universal Pictures and Samuel Goldwyn Films. The films plot and characters are adapted from three novels in author Patrick OBrians Aubrey–Maturin series, which includes 20 completed novels of Jack Aubreys naval career.

At the  .

==Plot summary== HMS HMS beat to quarters. As Captain Aubrey rushes on deck the ship is ambushed by Acheron, a ship twice as large as the British warship.
 British whaling fleet at will. He orders pursuit of Acheron, rather than returning to port for repairs. Acheron again ambushes Surprise, but Aubrey slips away in the night by using a clever decoy.

Following the privateer south, Surprise rounds Cape Horn and heads to the Galápagos Islands, where Aubrey is sure Acheron will prey on Britains whaling fleet. The ships doctor, Maturin, is interested in the Galápagos wildlife|islands fauna and flora; Aubrey promises his friend several days exploration time. When Surprise reaches the Galápagos they recover the survivors of a whaling ship, the Albatross, destroyed by Acheron. Realizing the ship is close, Aubrey hastily pursues the privateer. Maturin accuses Aubrey of going back on his word, and of following Acheron more out of pride rather than duty, something which Aubrey flatly denies, although not without acknowledging that he has exceeded his orders in pursuit of the privateer. Superstitious crewmen begin to whisper that the incompetent Hollom is a Jonah who is responsible for the ships misfortunes; he responds by killing himself.
 phasmid (phasmatodea|stick insect)—Aubrey disguises Surprise as a whaling ship under the name Syren; he hopes the French would move in close to capture the valuable ship rather than destroy it. The Acheron falls for the disguise and is disabled after a nearly one-sided gun duel. Aubrey leads boarding parties across the wreckage, engaging in fierce hand-to-hand combat with the French crew before the ship is captured. Looking for the Acherons captain, Aubrey is directed to the sickbay, where a French doctor by the name of De Vigny tells him the captain is dead and offers Aubrey the commanders sword.

Acheron and Surprise are repaired; while Surprise will remain in the Galápagos, the captured Acheron is to be taken to Valparaíso under the command of Captain Pullings. As Acheron sails away, Maturin mentions that Acherons doctor had died months ago. Realizing the French captain is still alive and has deceived him by pretending to be the ships doctor, Aubrey gives the order to beat to quarters and escort Acheron to Valparaíso. Maturin is again denied the chance to explore the Galápagos. Aubrey notes that since the bird Maturin seeks is flightless, "its not going anywhere", and the two friends play a selection of Luigi Boccherini as the crew prepares to follow the Acheron once again.

==Cast==

 
 
Officers Captain Jack Aubrey – Russell Crowe
*Stephen Maturin|Dr. Stephen Maturin – Paul Bettany Lieutenant / Acting Captain Thomas Pullings – James DArcy Lieutenant William Mowett &ndash; Edward Woodall Captain Howard, Royal Marines &ndash; Chris Larkin
*Midshipman Lord William Blakeney &ndash; Max Pirkis
*Midshipman Boyle &ndash; Jack Randall Lieutenant Peter Myles Calamy &ndash; Max Benitz
*Midshipman Hollom &ndash; Lee Ingleby
*Midshipman Williamson &ndash; Richard Pates
*John Allen, Sailing Master &ndash; Robert Pugh
Seamen
*Mr. Higgins, Surgeons Mate &ndash; Richard McCabe
*Mr. Hollar, Boatswain &ndash; Ian Mercer
*Mr. Lamb, Carpenter &ndash; Tony Dolan
*Preserved Killick, Captains Steward &ndash; David Threlfall Barrett Bonden, Billy Boyd
*Joseph Nagle, Carpenters Mate &ndash; Bryan Dick Joseph Morgan
*Joe Plaice, Able Seaman &ndash; George Innes
*Michael Doudle, Able Seaman &ndash; William Mannering Patrick Gallagher
*Nehemiah Slade, Able Seaman &ndash; Alex Palmer
*Mr. Hogg, Whaler &ndash; Mark Lewis Jones Loblolly Boy &ndash; John DeSantis
*Black Bill, Stewards Mate &ndash; Ousmane Thiam
*Young Sponge &ndash; Konstantine Kurelias
*Captain of the Acheron &ndash; Thierry Segall
*Private Trollope &ndash; Aidan Black
*Jemmy Ducks &ndash; Sebastian Grubb
 

==Allusion to real events and people==

Although in the original book by Patrick OBrian the "prey" ship was the American USS Norfolk, these episodes are probably inspired  by the capture of the East Indiaman Stanhope by the great naval strategist Don Blas de Lezo. 

==Production==

===Development=== USS Constitution Desolation Island, although the Acheron replaced the Dutch Seventy-four (ship)|74-gun warship Waakzaamheid, the Surprise replaced the Leopard, and in the book it is Aubrey who is being pursued around the Cape of Good Hope. HMS Surprise.  Other incidents in the film come from other books in OBrians series. HMS Rose), HMS Surprise modern replica of James Cook|Cooks HM Bark Endeavour|Endeavour rounding Cape Horn. All of the actors were given a thorough grounding in the naval life of the period in order to make their performances as authentic as possible. The ships boats used in the film were Russian Naval six- and four-oared yawls supplied by Central Coast Charters and Boat Base Monterey.  Their faithful 18th century appearance complemented the historic accuracy of the rebuilt "Rose", whose own boat, the "Thorn" could be used only in the Brazilian scene. The on-location shots of the Galapagos were unique for a feature film as normally only documentaries are filmed on the islands.

===Sound===
Sound designer Richard King earned Master and Commander an Oscar for its sound effects by going to great lengths to record realistic sounds, particularly for the battle scenes and the storm scenes.     King and director Peter Weir began by spending months reading the  Patrick OBrian novels in search of descriptions of the sounds that would have been heard on board the ship&mdash;for example, the "screeching bellow" of cannon fire and the "deep howl" of a cannonball passing overhead. 

King worked with the films Lead Historical Consultant Gordon Laco, who located collectors in Michigan who owned a 24-pounder and a 12-pounder cannon. King, Laco, and two assistants went to Michigan and recorded the sounds of the cannon firing at a nearby National Guard base. They placed microphones near the cannon to get the "crack" of the cannon fire, and also about   downrange to record the "shrieking" of the chain shot as it passed overhead. They also recorded the sounds of bar shot and grape shot passing overhead, and later mixed the sounds of all three types of shot for the battle scenes.

For the sounds of the shot hitting the ships, they set up wooden targets at the artillery range and blasted them with the cannon, but found the sonic results underwhelming. Instead, they returned to Los Angeles and there recorded sounds of wooden barrels being destroyed. King sometimes added the "crack" of a rifle shot to punctuate the sound of a cannonball hitting a ships hull. 

For the sound of wind in the storm as the ship rounds Cape Horn, King devised a wooden frame rigged with one thousand feet of line and set it in the back of a pickup truck. By driving the truck at   into a   wind, and modulating the wind with barbecue and refrigerator grills, King was able to create a range of sounds, from "shrieking" to "whistling" to "sighing," simulating the sounds of wind passing through the ships rigging.

===Music=== Icehouse traveled to Los Angeles to record the soundtrack to the film with Christopher Gordon and Richard Tognetti. Together, they won the 2004 APRA/AGSC Screen Music Award in the "Best Soundtrack Album" category. Suites for Violin Concerto No. 3; the third (Adagio) movement of Arcangelo Corelli|Corellis Christmas Concerto (Concerto grosso in G minor, Op.&nbsp;6, No.&nbsp;8); and a recurring rendition of Ralph Vaughan Williamss Fantasia on a Theme of Thomas Tallis.  The music played on cello before the end is Luigi Boccherinis String Quintet (Quintettino) for 2 violins, viola & 2 cellos in C major ("Musica notturna delle strade di Madrid"), G.&nbsp;324 Op.&nbsp;30. The two arrangements of this cue contained in the CD differ significantly from the one heard in the movie.

The song sung in the wardroom is "Dont Forget Your Old Shipmates." The tunes sung and played by the crew on deck at night are "OSullivans March", "Spanish Ladies" and "The British Tars" ("The shipwrecked tar"), which was set to tune of "Bonnie Ship the Diamond" and called "Raging Sea/Bonnie Ship the Diamond" on the soundtrack.

==Reception==

===Critical response===
Master and Commander was critically well received. 85% of 204 reviews tallied by the aggregate web site Rotten Tomatoes gave the film an overall positive rating, and the film has a "certified fresh" rating.    Roger Ebert gave the movie 4 stars out of 4, saying that "it achieves the epic without losing sight of the human". 

Christopher Hitchens finds "the summa of OBrians genius was the invention of Dr. Stephen Maturin. He is the ships gifted surgeon, but he is also a scientist, an espionage agent for the Admiralty, a man of part Irish and part Catalan birth—and a revolutionary. He joins the British side, having earlier fought against it, because of his hatred for Bonapartes betrayal of the principles of 1789—principles that are perfectly obscure to bluff Capt. Jack Aubrey. Any cinematic adaptation of OBrian must stand or fall by its success in representing this figure. On this the film doesnt even fall, let alone stand. It skips the whole project." He finds the actions scenes more inspirational: "In one respect the action lives up to its fictional and actual inspiration. This was the age of Bligh and Cook and of voyages of discovery as well as conquest, and when HMS Surprise makes landfall in the Galapagos Islands we get a beautifully filmed sequence about how the dawn of scientific enlightenment might have felt." 

===Release===
The movie opened #2 in the first weekend of North American release, November 14&ndash;16, 2003, grossing $25,105,990. It dropped to the #4 position in the second weekend and #6 in the third, and finished the domestic run with $93,926,386 in gross receipts. Outside the U.S. and Canada the movie grossed $116,550,000, doing best in Italy (at $15,111,841) with an overall worldwide total of $212,011,111.    As of December 2010, this puts the film at #397 on the all-time worldwide gross ranking (unadjusted for inflation). 

===Awards===
 

76th Academy Awards:    Best Cinematography, Russell Boyd Best Sound Richard King Best Picture Best Director, Peter Weir Best Art Direction Best Costume Design Best Film Editing Best Makeup Best Sound Mixing Best Visual Effects

AFI Top 10 Epics (longlisted)

===Sequel===
Director Peter Weir, asked in 2005 if he would do a sequel, stated he thought it "most unlikely", and after disclaiming internet rumors to the contrary, stated "I think that while it did well...ish at the box office, it didnt generate that monstrous, rapid income that provokes a sequel."   In 2007 the film was included on a list of "13 Failed Attempts To Start Film Franchises" by   at Fox and let him know your thoughts". 

==References==
 
<!--
 
 
-->

==Bibliography==
* 

==See also== Commander as a naval rank

==Notes==
 

==External links==
 
 
*  
*   which explores the films connections to the Aubrey Maturin series
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 