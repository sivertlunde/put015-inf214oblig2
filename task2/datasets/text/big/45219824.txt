Jari Bidda Jana
{{Infobox film 
| name           = Jari Bidda Jana
| image          =  
| caption        = 
| director       = Y. R. Swamy
| producer       = J. Chandulal Jain B. Deepak Chand Naveenchand Shah A. Krishnamurthy
| story          = H. S. Tharanala
| writer         = Chi. Udaya Shankar (dialogues)
| screenplay     = Y. R. Swamy Jayanthi Ashok Ashok Rekha Rao
| music          = T. G. Lingappa
| cinematography = R. Madhusudan
| editing        = R. Rajan
| studio         = Thirupathi Jain Films
| distributor    = Thirupathi Jain Films
| released       =  
| runtime        = 134 min
| country        = India Kannada
}}
 1980 Cinema Indian Kannada Kannada film, Ashok and Rekha Rao in lead roles. The film had musical score by T. G. Lingappa.  

==Cast==
 
*Lokesh Jayanthi
*Ashok Ashok
*Rekha Rao
*Srinivasa Murthy
*Pramila Joshai
*Thoogudeepa Srinivas
*Musuri Krishnamurthy
*Thyagaraj Urs
*Mallika
*Master Rajesh
*Baby Rekha
*Ranadheer
*Bhoja
*Vedavyasa Rao
*Muniyappa
*Mahadevappa
*Sumathi
*Comedian Guggu in Guest Appearance
*Rajanand in Guest Appearance
*Uma Shivakumar in Guest Appearance
 

==Soundtrack==
The music was composed by TG. Lingappa. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Chi. Udaya Shankar || 04.19
|-
| 2 || Kogileyu Haaduthidhe || Vani Jayaram || Chi. Udaya Shankar || 04.03
|-
| 3 || Halliya Thotadi || Vani Jayaram, S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.34
|-
| 4 || Nanage Nanade Nyaya || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 04.29
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 

 