BoOzy' OS and the Cristal Gem
{{Infobox film
| name           = BoOzy OS and the Cristal Gem
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Julien Rocca-Darcin
| producer       = Julien Rocca-Darcin
| writer         = Julien Rocca-Darcin
| screenplay     = Julien Rocca-Darcin
| story          = Julien Rocca-Darcin
| based on       =  
| starring       = Julien Rocca-Darcin Georges Colazzo
| music          = Julien Rocca-Darcin
| editing        = Julien Rocca-Darcin
| studio         = CreaSyn Studio
| distributor    = KAJA Films
| released       =  
| runtime        = 5 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
BoOzy OS and the Cristal Gem (also known as The Cristal Gem)    ( , also known as La Gemme de Cristal) is a 2013 French animated cartoon based on the French animated television series BoOzy OS, and directed, written, scored by and starring its creator Julien Rocca-Darcin. It was produced by CreaSyn Studio.

The film was first released at Annecy International Animated Film Festival on April 14, 2013. It was presented during the Dailymotion, Cartoon Network Studios and Annecy international event "+ de courts ! Online animation film contest by Annecy", and arrived first.   

The film title  refers to the Annecy award internationally named "Cristal dAnnecy".

==Plot==
BoOzy OS, a bodybuilder caveman, is seeking the "Cristal" of Annecy.

==Themes== Sonic The Doctor Robotnik Beauty and the Beast. The Cristal Gem also heavily parodies most of Julien Rocca-Darcins movies, such as Cow Hard and Stop-and-Cop.

==Cast==
* Julien Rocca-Darcin as BoOzy OS / Mari OS / OSmic The Hedgeh OS / SkoOlet OS / Rob OSmic / PoOlet OS (voice)
* Georges Colazzo as Victor Chai (voice)

==References==
 

==External link==
*  

 
 
 
 
 
 
 

 
 