Nurse Cavell
 
{{Infobox film
  | name     = Nurse Cavell
  | image    = 
  | caption  = 
  | director = W. J. Lincoln		
  | producer = W. J. Lincoln
  | writer   = W. J. Lincoln 
  | starring = Margaret Linden
  | music    = 
  | cinematography = Maurice Bertel
  | editing  = 
  | studio = Cinema Films Ltd 
  | distributor = 
  | studio = J.C. Williamson Ltd
  | released = 21 February 1916 
  | runtime  = over 2,000 feet   
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

Nurse Cavell is a 1916 Australian feature-length film directed by W. J. Lincoln about the execution of Edith Cavell during World War I. It was also known as Edith Cavell.

It is considered a lost film.

==Plot==
In an English garden, a Belgian officer meets a ward of an old clergyman friend of Edith Clavell.

Eventually Clavell is executed for spying.

==Cast==
*Margaret Linden as Edith Cavell 
*Arthur Styan as Captain Karl
*Agnes Keogh as Nita
*Stewart Garner as Captain Devereaux
*Fred Camborne

==Production==
The movie was independently produced at the J.C. Williamson Ltd (film company)|J.C. Williamson Ltd studios in Melbourne.   It was shot 16–19 February 1916. Marsden, Ralph. The Old Tin Shed in Exhibition Street: The J.C. Williamson Studio, Melbournes Forgotten Film Factory  . Metro Magazine: Media & Education Magazine, No. 157, 2008: 144-153. Availability:   ISSN: 0312-2654.  . 

W. J. Lincoln later claimed the film took one week from starting to write the story until screening:
 In this relatively short space of time the producer had to prepare the scenarios, assemble his company, arrange for locations, scenery, costumes, etc., and direct the picture. It was not until the Wednesday that a start was actually made with the camera, and on the following Saturday afternoon the film was ready for the various mechanical processes necessary to complete its preparation.  

==Reception==
The Daily Herald called the film "an attraction of exceptional merit... the story is faithfully portrayed". 

The Melbourne Winner wrote that:
 In view of such hurried work, it is a tribute to all concerned that so good a picture was turned out. The story is interesting, and the photography, with the exception of one or two sections, excellent. The cast which interprets Mr Lincolns story is more than equal to the demands made upon it... Miss Margaret Lindens Nurse Cavell is a sympathetic study, although her face is marred somewhat by the heavy make-up used about the eyes. Miss Agnes Keogh, as Nita Devereux, has a fine screen presence, and acts in a convincingly natural manner.... Mr Arthur Styan... appears to advantage as Lieut. Karl; Mr Fred Kehoe as General von Bissing generally does well; Mr Stewart Garner looks capably alert as Captain Devereux, the Belgian attache, and Frank Cullinan as the priest supports Nurse Cavell in her hour of trial with befitting solemnity.  
The film appears to have been reasonably successful at the box office as a sequel was being prepared within two weeks of its release. 

==Legal Action== John Gavin, who took legal action against Lincoln.  This led to the film having to be advertised with the disclaimer "patrons are reminded that this picture is in no way connected with any other bearing the same name." 
 La Revanche (1916).    

==References==
 

==External links==
*  
*   at National Film and Sound Archive
*  at AustLit
 

 
 
 
 


 