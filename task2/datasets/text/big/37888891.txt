Gopala Gopala (1996 film)
 
{{Infobox film
| name           = Gopala Gopala
| image          = 
| image_size     =
| caption        = 
| director       = Pandiarajan
| producer       = M. Kajamydeen V. Gnanavelu Jayaprakash|V. Jaya Prakash
| writer         = Pandiarajan
| starring       =   Deva
| cinematography = K. Nithya
| editing        = V. Rajagopal S. Govindaswamy
| distributor    =
| studio         = Roja Combines
| released       =  
| runtime        = 155 minutes
| country        = India
| language       = Tamil
}}
 1996 Tamil Tamil comedy Kushboo in lead roles with a musical score by Deva (music director)|Deva. The film was named after a song from the film Kadhalan. 

The film ran for 100 days.  The film was remade in Malayalam as Mister Butler with Dileep (actor)|Dileep.

==Plot==

Gopalakrishnan (Pandiarajan), a clever cook from Coimbatore, moves to an apartment in Chennai. He becomes quickly popular and appreciated among the apartments women, however their husbands get jealous of Gopalakrishnan. Meanwhile, he falls in love with Usha (Kushboo Sundar|Kushboo). He decides to marry her with the support of his neighbours. After the marriage, Usha has just learned that Gopalakrishnan was already married. Gopalakrishnan got married with a girl (Sindhu) but she was already pregnant and she eloped with her lover that very evening. Usha is upset and refuses to live with him. Later, Gopalakrishnan finds a forsaken baby (by his ex-wife) in a dustbin and leaves the baby to an orphanage. Gopalakrishnans father (Manivannan), an army officer, comes to live with his son and he helps his son to win back Ushas heart.

==Cast==

*Pandiarajan as Gopalakrishnan and Ushas grandfather Kushboo as Usha
*Manivannan as Gopalakrishnans father Janagaraj as Kuzhanthaivel Chithra as Meenakshi, Kuzhanthaivels wife
*Venniradai Moorthy as Sundaram
*Jyothi Meena as Annakili, Sundarams wife
*Charle as Chettiar
*Kovai Sarala as Valliammai, Chettiars wife
*Malaysia Vasudevan as Ushas father Jayanthi as Ushas mother
*Sindhu Anju as Fathima
*Loose Mohan
*Kollangudi Karuppayee as Ushas grandmother
*Pandu as Chinna
*Sempuli Jegan
*Kumari Muthu
*Jaguar Thangam as Gold
*Pandian (guest appearance)

==Soundtrack==

{{Infobox album |  
| Name        = Gopala Gopala
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack |
| Length      = 22:08
| Label       = Lahari Music Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1996, features 5 tracks with lyrics written by Vairamuthu.  

{| class="wikitable"
|- align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Deva || 4:11
|- 2 || Kanna Nee Varuvai || Anuradha Sriram, Ishrath, Meera Krishnan || 5:23
|- 3 || Mano || 4:18
|- 4 || Thenkasi Mamanukku || Shahul Hameed, Swarnalatha || 3:50
|- 5 || Yuddhathil || S. P. Balasubrahmanyam, K. S. Chithra || 4:26
|}

==References==
 

 

 
 
 
 
 
 
 
 