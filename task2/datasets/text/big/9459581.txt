Trouble Along the Way
{{Infobox film
| name           = Trouble Along the Way
| image          = TroubleAlongtheWay.jpg
| image_size     =
| caption        =
| director       = Michael Curtiz
| producer       = Melville Shavelson Jack Rose Melvill Shavelson James Edward Grant (uncredited)
| narrator       =
| starring       = John Wayne Donna Reed Charles Coburn
| music          = Max Steiner
| cinematography = Archie Stout
| editing        = Owen Marks
| distributor    = Warner Brothers
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2.45 million (US) 
}}
 1953 film starring John Wayne and Donna Reed, with a supporting cast including Charles Coburn and Marie Windsor. The movie was directed by Michael Curtiz, director of Casablanca (film)|Casablanca. The black-and-white comedy was released by Warner Bros. with an aspect ratio of 1.37:1.

==Synopsis==
Small, obscure St. Anthony’s college, run by the Catholic church, is in financial straits and about to be closed. To save it, and himself from forced retirement, elderly rector Father Burke (Charles Coburn) hires a down and out former big time football coach, Steve Williams (John Wayne), in hopes of building a lucrative sports program. First turning down the job, Williams later accepts it when he learns that his former wife, Anne (Marie Windsor), now remarried, complained to Social Services that he is an unfit father, and plans to sue for custody of his 11 year old daughter, Carole (Sherry Jackson). Anne’s actual aim is not to get Carole, in whom she has no interest, but rather to pressure Steve into rekindling an affair with her. Social Services worker Alice Singleton (Donna Reed), coldly prejudiced against Steve because she suffered from a relationship with her father similar to that between Steve and Carole, is preparing a report in Anne’s favor. Steve, in the meanwhile, attempts to charm Alice and win her over. Desperate to have the football program pay off, Father Burke uses his clerical connections to schedule St. Anthony’s against high profile Catholic colleges--Villanova, Notre Dame, etc.--in the upcoming season. Faced with physically inadequate players, Steve, unbeknownst to Father Burke, uses chicanery to enroll beefy star athletes as freshmen, giving St. Anthony’s a winning team. Father Burke subsequently learns of Steve’s dishonest methods and, after chiding him, disbands the sports program, knowing this would cause St. Anthony’s to close. Alice submits a report unfavorable to Steve, but subsequently, in her testimony in the court custody hearing, repudiates it after having recognized her bias and the mothers lack of honest affection for Carole. Alice also speaks against Steve, stating he isnt a properly responsible parent, and under questioning reveals she is in love with him. The judge halts proceedings, placing Carole in custody of the State, and assigning her a new case worker, until matters can be sorted out. In a surprise move, the church agrees to continue funding St. Anthonys without the football program. Even so, Burke resigns as rector believing that he had been behaving selfishly to unnecessarily prolong his position. Before leaving, though, he reinstates Steve as coach and forgives him his unscrupulous behavior because it was for love of his child. The film ends with Carole, accompanied by Alice, walking away from Steve, with the implied understanding that Steve and Alice would later marry and the three would be together as a family.


==Cast==
* John Wayne as Steve Aloysius Williams
* Donna Reed as Alice Singleton
* Charles Coburn as Father Burke
* Tom Tully as Father Malone
* Sherry Jackson as Carole Williams
* Marie Windsor as Anne McCormick
* Tom Helmore as Harold McCormick
* Dabbs Greer as Father Mahoney Leif Erickson as Father Provincial
* Douglas Spencer as Procurator
* Lester Matthews as Cardinal OShea
* Chuck Connors as Stan Schwegler

James Dean appears as an uncredited extra in the film, during a scene in the college chapel.

==Production==
Portions of the film were shot at Pomona College and Loyola Marymount University, and various Los Angeles high schools. Max Steiner provided the music.   

==Reception==
The New York Times gave it a favorable review, citing "spirited and contemporary" dialogue. 

Saying that Wayne was "completely at home" in the role, Variety also found the lines, "a principal factor" in carrying the film.  Craig Butler found the film predictable yet heart warming. 

==See also==
* John Wayne filmography

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 