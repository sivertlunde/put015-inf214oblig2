Lightning Bug (film)
{{Infobox film
| name           = Lightning Bug
| image          = LightningBug.jpg
| caption        =  Robert Hall
| producer       = Lisa Waugh
| writer         = Robert Hall Kevin Gage Ashley Laurence
| music          = Jason M. Hall
| cinematography = Brandon Trost
| editing        = Joshua Charson
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 97 minutes
| country        =  
| language       = English
| budget         = 
}}
Lightning Bug is a 2004  . 

It is a fictionalized account of Halls own childhood and entry into special effects makeup for film and television. It was executive produced by Laura Prepon as a way to expand her image as an actress beyond her role on That 70s Show.

It was filmed on location Fairview, Alabama.

==Plot==

Single mother Jenny Graves (Ashley Laurence) decides to restart her deadend life by moving out of Detroit and taking her two sons Green (Bret Harrison) and Jay (Lucas Till) to small rural town in Alabama. Green is fan of horror films, more specifically the makeup effects used to bring them to life. He meets a pair of affable locals, Tony Bennet and Billy Martin (Jonathan Spencer and George Faughnan).

However, his mothers penchant for getting involved with the wrong type of men brings a very human monster into his life, Earl Knight (Kevin Gage). 

Taking some horror films back to the video store, he meets Angevin Duvet (Laura Prepon) who shares both his interest in the horror genre and fish-out-of-water status in the small town. Smart, funny and a sexy Goth girl he is instantly smitten. However, there are hints that there are some troubling aspects to her past. 

Green approaches the local business man, Tightwiler (Bob Penny), who runs a yearly haunted house and by startling him with one of his creations nabs the job of creating this years haunted house. With his share of the ticket sales, he and Angevin can move to Hollywood to pursue their dreams. However, this puts him on a direct collision course with Angenvins mother, a deeply religious woman involved with local Holy Calling of the Southern Saints church.

==Cast==
*Ashley Laurence - Jenny Graves
*Bret Harrison - Green Graves
*Laura Prepon - Angevin Duvet Kevin Gage - Earl Knight
*Lucas Till - Jay Graves
*Jamie Avera - Deputy Hollis

==External links==
*  
*  

 
 
 
 
 
 


 