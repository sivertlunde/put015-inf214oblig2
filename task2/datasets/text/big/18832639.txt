The Romanovs: An Imperial Family
{{Infobox Film name = The Romanovs: An Imperial Family image = Romanovs-A Crowned Family.jpg caption = DVD cover director = Gleb Panfilov producer = Vladimir Bychkov writer = Gleb Panfilov Ivan Panfilov Inna Churikova starring = Olga Vasilyeva Vladimir Grachyov Aleksandr Filippenko music = Vadim Bibergan cinematography = Mikhail Agranovich editing = Enzo Meniconi distributor = Studio Vera  (Russia)   released = July 19, 2000 runtime = 135 minutes language = Russian country = Russia budget = $ 18 million  
}}
The Romanovs: An Imperial Family ( ) is a 2000 Russian film about the last days of Tsar Nicholas II and his family. The Russian title implies both the Imperial Crown of Russia and the crown of thorns associated with martyrs. The film premiered at the 22nd annual Moscow Film Festival.

== Plot == Alexandra are woken up by their son, Alexei who has a high fever, as well as Olga, the eldest of the five children. The next morning, Nicholas leaves to Stavka, at the war front. Meanwhile in Petrograd, people are starting to revolt. A street is bombed and many are killed. The Russian Revolution has started. Nicholas arrives at Stavka and he is told that his son Alexei, a hemophiliac, will not live to the age of 16. Back in Petrograd, Alexandra is told about the chaos in the city. At the Imperial Train, Nicholas is given documents requesting his abdication. He signs and is no longer Emperor of All the Russias. He had decided to give the throne to his son Alexei, but due to his bad health, he decides to give the throne to his brother Michael, who does not accept it. Russia is left without an emperor. 

At the Palace, Alexandra is told that she and her husband are no longer Emperor and Empress and that they are now under house arrest. Alexandra asks M. Gilliard, the French tutor, to tell her son the news. Nicholas, no longer tsar, returns to his family at the Alexander Palace. The next day, Nicholas, Alexandra, the two older Grand Duchesses, and Alexei are introduced to Alexander Kerensky of the Provisional Government. Olga does not seem to like him; Tatiana says that he does not respect the ex-tsar and tsarina because they are private citizens. Alexandra is hurt and Tatiana tells her that she’s getting bald. Olga reassures her mother that it is normal that she and her sisters are losing their hair because they have been sick with measles. Alexandra decides to shave her children’s heads. One night Alexandra wakes up screaming. She tells Nicholas that she had a dream in which Grigori Rasputin showed her a vision of the future. 
 Michael Alexandrovich before they leave. They are transferred to Tobolsk, a village in Siberia, to live in the Governor’s Mansion under house arrest. The Grand Duchesses continue their everyday lives. They play the piano while Alexei draws ships. One day, Olga is playing the piano when a guard downstairs is playing another song on the mandolin, his name is Andrei Denisov. Olga starts playing the same song that Denisov is playing. They all start dancing as he keeps playing the mandolin. The Imperial Family and their former servants live a simple life. Their everyday lives consist of cutting wood, snow sledding, and family dinners. One day, Olga and Denisov fall in love with each other. With another revolution comes another change in the Russian Government. In November 1917, the Bolsheviks who form the first Soviet Republic overthrow the Provisional Government. The family is photographed and new, more aggressive guards are brought in. Olga Nikolaevna is sick once again and needs a blood transfusion. Denisov is the donor as Alexandra cannot do it. Olga recovers and she and Nicholas play the piano together. The new Soviet government decides to transfer the Imperial Family to a new location. Yakovlev, a commissar of the Ural Soviet, visits the Imperial Family in Tobolsk. He talks to Alexei and asks the maid Demidova to check is temperature. Because of Alexei’s bad health, only Nicholas, Alexandra, Maria, and the servants leave to Ekaterinburg in April 1918; Alexei, Olga, Tatiana, Anastasia, and M. Gilliard are left behind. A few weeks later they also are transferred to the Ipatiev House in Ekaterinburg. After they are reunited, the commandant, Yurovsky, allows the family to have a church service. The next day, a priest comes to give a mass for the family. In the following days, the kitchen boy, Sednev is let go to be with his uncle. 

On the night of July 16, 1918, the Ural Soviet receives an order to execute the Imperial Family. While everyone sleeps, Yurovsky and the other guards are planning the execution. Shortly after midnight on July 17, the guards awaken Dr. Botkin. He is told that everybody is to come to the cellar for safety, as the White Army is getting closer. The family and their servants are led downstairs into a cellar. The guards bring in two chairs for Alexei and Alexandra; everybody else stands. Yurovsky tells everyone that there are rumors that the family is dead and that they will take a picture to put an end to it.  He arranges them into position, but then suddenly reads to Nicholas an execution order. Nicholas is shocked, Alexandra and her daughters cross themselves. The guards draw their guns and shoot. Not everyone is dead immediately, so they massacre the survivors individually. As they clear the bodies, the guards discover diamonds hidden in the family’s clothes. The film then switches to the canonization of Tsar Nicholas II and his family. This is the last scene before the end of the film.

== Cast == Tsar Nicholas II Tsarina Alexandra Olga Nikolaevna Tatiana Nikolaevna Olga Vasilyeva Maria Nikolaevna Anastasia Nikolaevna
* Vladimir Grachyov as Alexei Nikolaevich, Tsarevich of Russia
* Aleksandr Filippenko as Lenin

== Production ==
* The photographs Nicholas is looking at while he sits on the toilet in the Ipatiev house are actual photographs of the real Imperial family.
* All actors portraying the grand duchesses Olga, Tatiana, Maria, Anastasia and Alexei had their hair shaved for the movie. In real life the daughters and heir were indeed sick and had their heads shaved.  Although, their hair never grows back despite the fact that they still lived for about 13 months.
* British actress Lynda Bellingham (Empress Alexandra) had her lines dubbed into Russian by writer Inna Churikova.
* Outside and inside shots were actually taken at the Alexander Palace, the official home of Tsar Nicholas II and his family. Inside shots included the Tsars study. Although the rooms of the Grand Duchesses were recreated from water colors and photographs.
* The dresses the Grand Duchesses wear as they dance around the room at Tobolsk are replicas of the actual dresses worn in the 1914 formal photo-shoot.
* When the family is resting by the lake, while they travel to Toblosk, Nicholas lets photographs fall from his shirt. The photographs are of Mathilde Kschessinska, Nicholas first lover before he married Alexandra.
* The Governors house in Tobolsk still exists and is now a museum. Church of Blood.

== Accolades ==
* (2000) “Golden Aries” award for Best Production design (Alexander Boim, Vladimir Gudilin, Anatoly Panfilov)
* (2001) International Moscow Human Rights Film Festival “Stalker”. Directors Guild Award (Gleb Panfilov)
* (2003) St. Petersburg Governor’s prize for cinematic score (Vadim Bibergan)

== See also ==
* List of films about the Romanovs

== References ==
 

== External links ==
*    
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 