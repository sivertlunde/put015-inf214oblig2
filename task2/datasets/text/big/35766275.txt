The Prince Who Was a Thief
{{Infobox film
| name           = The Prince Who Was a Thief
| image_size     =
| image          =
| caption        =
| director       = Rudolph Mate
| producer       = Leonard Goldstein
| writer         = Gerald Drayson Adams Aeneas MacKenzie
| based on       = story by Theodore Dreiser
| narrator       =
| starring       = Tony Curtis Piper Laurie Everett Sloane
| music          = Hans J. Salter
| cinematography = Irving Glassberg
| editing        = Edward Curtiss
| studio         = Universal International
| distributor    =
| released       =
| runtime        =
| country        =
| language       = English
| budget         =
| gross          = $1,475,000 (US rentals) 
}}
The Prince Who Was a Thief is a 1951 swashbuckler film starring Tony Curtis. It was his first film as a star.

==Plot==
An assassin (Everett Sloane) is sent to kill a baby prince but cannot go through with it. He decides to raise the child as his own, and he grows up to be a thief (Tony Curtis).

==Cast==
*Tony Curtis as Julna/Hussein
*Piper Laurie as Tina
*Everett Sloane as Yussef
*Jeff Corey as Mokar
*Betty Garde as Mirza
*Marvin Miller as Hakar
*Peggie Castle as Princess Yasmin
*Donald Randolph as Mustapha
*Nita Bieber as Cahuena
*Milada Mladova as Dancer
*Hayden Rorke as Basra
*Midge Ware as Sari

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 

 
 
 
 
 
 
 
 

 