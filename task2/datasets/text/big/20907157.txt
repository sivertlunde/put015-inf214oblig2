All Through the Night (film)
{{Infobox film
| name           = All Through the Night
| image          = All_through_the_night_poster.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = Vincent Sherman
| producer       = Hal B. Wallis Jerry Wald
| story          = Leo Rosten Leonard Spigelgass Edwin Gilbert
| starring       = Humphrey Bogart Conrad Veidt Kaaren Verne
| music          =   (music) Johnny Mercer (lyrics)
| cinematography = Sidney Hickox
| editing        = Rudi Fehr
| studio         = Warner Bros. Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States English
| budget         = $600,000 Frankel, Mark.   on TCM.com 
| gross          =
}}
 thriller film released by Warner Brothers in 1941, starring Humphrey Bogart, Conrad Veidt and Kaaren Verne, and featuring many of the Warner Bros. company of character actors.  It was directed by Vincent Sherman.

==Plot==
An elderly baker named Miller (Ludwig Stossel) is murdered by a sinister stranger (Peter Lorre). A trail leads on to a nightclub singer, Leda Hamilton (Kaaren Verne) who reveals that she and Miller have been in thrall to an organization of Nazi fifth columnists led by Ebbing (Conrad Veidt). She is helped by a well-meaning sports promoter, Alfred "Gloves" Donahue (Humphrey Bogart), who himself is suspected of murdering a nightclub owner (Edward Brophy), and has to track down those responsible to prove his innocence.

==Cast==
  
* Humphrey Bogart as Alfred "Gloves" Donahue
* Conrad Veidt as Ebbing
* Kaaren Verne as Leda Hamilton
* Jane Darwell as Mrs. Donahue
* Frank McHugh as Barney
* Peter Lorre as Pepi
* Judith Anderson as Madame
* William Demarest as Sunshine
* Jackie Gleason as Starchy
 
* Phil Silvers as Waiter
* Barton MacLane as Marty Callahan
* Edward Brophy as Joe Denning
* Martin Kosleck as Steindorf
* Jean Ames as Annabelle
* Ludwig Stossel as Mr. Miller
* Irene Seidner as Mrs. Miller James Burke as Forbes
 

Cast notes
*Phil Silvers and Jackie Gleason owe their presence in the film to the direct intervention of Warner Bros. studio head Jack Warner, who personally phoned director Vincent Sherman to ensure that they would be added to the cast.
*Kaaren Verne and Peter Lorre married in 1945, and divorced in 1950.

==Production==
Producer Hal Wallis made All Through the Night as a "companion piece" to his earlier anti-Nazi melodrama, Underground (1941 film)|Underground, despite the poor box office of the prior film. 

Humphrey Bogart was not the first person considered for the lead in the film: it was originally supposed to be played by    

The scene in which Bogart and William Demarest confuse a room full of Nazi sympathizers with doubletalk was not part of the original script, but was invented by director Sherman, who filmed it despite the objections of producer Wallis. Wallis ordered it removed from the film, but Sherman left a small segment of it in, and when preview audiences reacted positively to it, Wallis backed down and told Sherman to put the entire scene back in. 

==See also==
*America First Committee

==References==
Notes
 

==External links==
* 
* 

 

 

 
 
 
 
 
 
 
 
 