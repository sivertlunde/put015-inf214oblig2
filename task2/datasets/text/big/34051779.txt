The Cheat (1923 film)
{{infobox film
| name           = The Cheat
| image          =
| imagesize      =
| caption        =
| director       = George Fitzmaurice
| producer       = Adolph Zukor Jesse L. Lasky George Fitzmaurice
| writer         = Hector Turnbull (scenario) Jeanie MacPherson (scenario) Ouida Bergere (adaptation) Jack Holt
| music          =
| cinematography = Arthur C. Miller
| editing        =
| distributor    = Paramount Pictures
| released       =   reels (2232.05 meters)
| country        = United States Silent (English intertitles)
}}
The Cheat (1923) is a silent film produced by Famous Players-Lasky and distributed by Paramount Pictures, and is a remake of Cecil B. DeMilles 1915 hit feature using the same script by Hector Turnbull and Jeanie MacPherson. This version stars Pola Negri and was directed by George Fitzmaurice.   

==Cast==
*Pola Negri - Carmelita De Cordoba Jack Holt - Dudley Drake
*Charles de Rochefort - Claude Mace a/k/a Prince Rao-Singh (as Charles De Roche)
*Dorothy Cumming - Lucy Hodge
*  - Jack Hodge
*Charles A. Stevenson - Horace Drake
 
*Helen Dunbar - Duenna
*Richard Wayne - Defense Attorney
*Guy Oliver - District Attorney
*Edward Kimball - Judge
*Charles Farrell - Bit part

==Preservation status==
This film is now considered a lost film. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 