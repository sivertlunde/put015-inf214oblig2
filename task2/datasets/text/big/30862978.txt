Sainik
 
{{Infobox film
| name           = Sainik
| image          =
| image size     = 
| director       = Sikandar Bhati
| producer       = Jimmy Nirula
| writer         = Madan Joshi   Dharam Veer Ram
| starring       = Akshay Kumar Ashwini Bhave Ronit Roy Farheen Laxmikant Berde Anupam Kher Alok Nath Ranjeet Harish Patel Navneet Nishan
| music          = Nadeem-Shravan Sameer
| cinematography = Kishore Kapadia
| editing        = Nand Kumar
| distributor    = Manish Arts
| released       = 10 September 1993
| runtime        = 
| country        = India
| language       = Hindi
}}

Sainik is an Indian film directed by Sikander Bharti and released in 1993. It stars Akshay Kumar, Ashwini Bhave and Farheen.

==Plot==
This movie is about an army officer named Suraj Dutt (Akshay Kumar ) who is the son of Yashpal Dutt (Anupam Kher). Suraj goes to meet his sister Minnie in at a womens college before he falls in love with Alka (Ashwini Bhave). They get married soon but Suraj is called for a year-long mission. While he is away a message arrives that Suraj has been killed. Minni, Alka & Yashpal each learn about this but dont tell each other as they feel each otherwill die upon hearing this . Meanwhile, Minni is about to wed Vijay(Ronit Roy) but on the day of her wedding, she is kidnapped. 
The movie ends, surprising the audience with the reappearance of Suraj, (who was alive all this while!) who comes and saves Mini, and everybody is reunited.

Akshays acting was appreciated by all in this movie.

==Cast==
* Akshay Kumar ... Lieutenant Suraj Dutt
* Ashwini Bhave ... Alka Suraj Dutt
* Farheen ... Minnie Dutt
* Ranjeet ... Ghajraj Chaudhary (as Ranjit)
* Guddi Maruti ... Guddi
* Anupam Kher ... Retd. Colonel Yashpal Dutt
* Satish Shah ... Alkas dad
* Ronit Roy ... Vijay Ghai

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track # !! Title !! Singer(s)
|-
| 1
| "Kitni Hasrat Hai Hamein" 	
| Kumar Sanu, Sadhana Sargam
|- 
| 2
| "Meri Wafayen Yaad Karoge"
| Kumar Sanu, Asha Bhosle
|- 
| 3
| "Jaam Woh Hai" 
| Kumar Sanu
|- 
| 4
| "Humko Hua Hai Pyar" 	
| Vinod Rathod, Alka Yagnik
|- 
| 5
| "Babul Ka Ghar"
| Kumar Sanu, Alka Yagnik
|-
| 6
| "Hai Meri Saason Mein" 
| Suhasini
|}

==External links==
*  

 
 
 
 

 