Gadibidi Ganda
{{Infobox film
| name           = Gadibidi Ganda
| image          = 
| image_size     = 
| caption        =
| director       = V. S. Reddy
| producer       = K. Krishna Mohana Rao
| writer         = 
| screenplay     = 
| narrator       =  Ravichandran Ramya Roja    Jaggesh
| music          = Hamsalekha
| lyrics         = 
| cinematography = Jayaram
| editing        = Kotagiri Venkateswara Rao
| distributor    = 
| released       = 1993
| runtime        = 153 minutes
| country        = India
| language       = Kannada
| budget         = 
}}
 Roja amongst others. Music and lyrics was composed by Hamsalekha. The film was produced by K. Krishna Mohana Rao. It was a remake of Telugu movie Allari Mogudu, starring Mohan Babu.

==Synopsis==
Gopal is a village boy with a melodious voice who comes to the city in search of a job. Mohana a young and vibrant girl helps him to get a break in a good music company. In this process Mohana falls in love with him and forces him to get married to her. On the other hand Gopal returns to his village and gets married to his childhood love Nilambari. How would he juggle between both his wives makes the comedy climax of the story. 

==Cast== Ravichandran as Gopal
* Ramya Krishnan as Mohana Roja as Neelambari Nagesh 
* Jaggesh as Gopals friend
* Doddanna

==Songs==
{| class=wikitable sortable
|-
! # !! Title !! Singer(s) 
|-
|  1 || "Pancharangi Putta" || S. P. Balasubrahmanyam, K. S. Chithra
|-
|  2 || "Bidde Bidde" || S. P. Balasubrahmanyam, K. S. Chithra
|-
|  3 || "Muddadendide Mallige" || S. P. Balasubrahmanyam, K. S. Chithra
|-
|  4 || "Neenu Neene" || S. P. Balasubrahmanyam
|-
|  5 || "Bum Chik Bum"  ||  K. S. Chithra
|-
|  6 || "Gadibidi Ganda Neenu" || S. P. Balasubrahmanyam, K. S. Chithra
|}

==Reception==
The film and music composed by Hamsalekha was well received and the audio sales hit a record high.

==References==
 

==External links==
*  

 
 
 
 
 
 


 