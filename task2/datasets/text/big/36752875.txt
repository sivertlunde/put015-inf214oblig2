Hoovu Hannu
{{Infobox film
| name           = Hoovu Hannu
| image          = 
| caption        = 
| director       = S. V. Rajendra Singh Babu
| producer       = Jai Jagadish R. Dushyanth Singh Thriveni
| screenplay     = S. V. Rajendra Singh Babu
| based on       =   Lakshmi  Baby Shamili   Rajesh Gundu Rao
| narrator       = 
| music          = Hamsalekha
| cinematography = V. K. Murthy
| editing        = Suresh Urs
| studio         = Vaibhava Lakshmi Productions
| distributor    = 
| released       =  
| runtime        = 151 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Baby Shamili in the lead roles. It was directed by Rajendra Singh Babu and produced by Vaibhava Lakshmi Productions.  The music and lyrics were written and composed by Hamsalekha. The film was a musical hit and won many awards including the Karnataka State Film Awards and Filmfare Awards South.

Veteran cinematographer V. K. Murthy worked in this film and won many accolades for his work. 

==Cast== Lakshmi
* Baby Shamili
* Shankar Ashwath
* Rajesh Gundu Rao
* Bharathi

==Plot==
The story revolves around a woman played by Lakshmi who undergoes pain in every step she takes and is forced in prostitution racket to earn a livelihood. Her grand daughter, played by Shamili, comes to her rescue in the latter half of the film.

==Soundtrack==
{{Infobox album
| Name        = Hoovu Hannu
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = Kannada film Hoovu Hannu album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1993
| Recorded    =  Feature film soundtrack
| Length      = 32:22
| Label       = Lahari Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Hamsalekha composed the music for the soundtracks also writing their lyrics. The album consists of seven soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 32:22
| lyrics_credits = yes
| title1 = Manju Manju
| lyrics1 = Hamsalekha
| extra1 = K. S. Chithra|Chithra, Rajesh Krishnan
| length1 = 4:20
| title2 = Thaaye Thaaye
| lyrics2 = Hamsalekha Rajkumar
| length2 = 1:25
| title3 = Ningi Ningi
| lyrics3 = Hamsalekha
| extra3 = C. Aswath
| length3 = 5:01
| title4 = Ramana
| lyrics4 = Hamsalekha
| extra4 = Chithra
| length4 = 5:41
| title5 = Thaaye Thaaye (Female Version)
| lyrics5 = Hamsalekha
| extra5 = Chithra
| length5 = 5:45
| title6 = A Aa E Ee
| lyrics6 = Hamsalekha
| extra6 = Vijayalakshmi, S. P. Balasubramanyam
| length6 = 4:25
| title7 = Thaaye Thaaye (Male Version)
| lyrics7 = Hamsalekha
| extra7 = Rajkumar
| length7 = 5:45
}}

==Awards==
* Karnataka State Film Award for Best Director - Rajendra Singh Babu Lakshmi
* Filmfare Award for Best Director - Kannada
* Filmfare Award for Best Actress - Kannada

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 

 