Life Show
{{Infobox film
| name           = Life Show
| image          = Life_Show_DVD.jpg
| image_size     = 
| caption        = 
| director       = Huo Jianqi
| producer       = Jin Zhongqiang Han Sanping Yang Buting Tong Gang
| writer         =  
| narrator       =  Tao Hong Tao Zeru
| music          = Zhang Xiaofeng
| cinematography = Ming Su
| editing        = Kong Jinglei
| distributor    = International:   
| released       =  : August 28, 2002
| runtime        = 105 min. China
| Mandarin Chinese
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
 Chinese film Tao Hong, whose busy life dealing with family and business is nevertheless a lonely one. Her life takes a turn, however, when one of her long-time customers, played by Tao Zeru, shows a romantic interest in her. The film was a co-production between the China Film Group and the Beijing Film Studio. It was based on a novel by Chi Li and was adapted by Si Wu.   

Life Show won the Golden Goblet at the 2002 Shanghai International Film Festival, as well as awards for best actress and best cinematography.  It received a North American premiere  at the Montreal World Film Festival on August 28, 2002. 

== Plot == Tao Hong), who operates a duck neck stall in night market in an unnamed city in central China (the film was shot in Chongqing ). Though Shuangyang keeps her cool, her life is filled with complications. Her brother drug-addicted Jiujiu has been committed to a sanitarium Her assistant, Mei, who has fallen in love with Jiujiu, attempts suicide. Her sister in law, Xiaojin, always leaves Shuangyangs nephew for Shuangyang to take care of. At the same time, Shuangyang is trying to regain her familys home, which was given to a neighbor during the Cultural Revolution. Perhaps worst of all, Shuangyang is at risk of losing her restaurant.

Into this morass, Shuangyang starts to notice one of her customers, Zhou Xiongzhou, a middle-aged businessman who has been frequenting her restaurant for a year. Despite concerns, she decides to take the risk with Xiongzhou and the two begin an unassuming romance.

== Cast == Tao Hong as Lai Shuangyang, the owner of a small restaurant on a night market street specializing in the central Chinese specialty of duck necks. Yang Yi as Mei, Lai Shuangyangs assistant, who is in love with Shuangyangs younger brother, Jiujiu.
* Tao Zeru as Zhuo Xiongzhou, a middle aged customer at Lai Shuangyangs restaurant who begins to show a romantic interest in Shuangyang. 
* Pan Yueming as Xiaojin, the wife of Shuangyangs older brother, Shuangyuan. 
* Luo Deyuan as Zhang, a local bureaucrat that deals with Shuangyang in her efforts to regain the home of her family that was lost during the Cultural Revolution.

== DVD release ==
Life Show was released for North American markets on region 1 DVD by Razor Digital Entertainment on October 11, 2005. The DVD included subtitles for English, as well as both simplified and traditional Chinese characters.

== References ==
 

== External links ==
* 
* 
*  at the Chinese Movie Database

 

 
 
 
 
 
 
 