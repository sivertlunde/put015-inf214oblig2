Déficit
 
{{Infobox Film |
| name = Déficit
| image = 
| director = Gael García Bernal
| producer = Gael García Bernal Pablo Cruz Diego Luna
| starring = Gael García Bernal Luz Cipriota Tenoch Huerta Mejía Camila Sodi
| music = 
| cinematography = 
| editing = 
| distributor = 
| released = May 21, 2007
| runtime = 79 minutes
| country = Mexico Spanish
| budget = 
| gross = 
}} 2007 Cinema Mexican feature film, the debut of Gael García Bernal as a film director|director. It was written by Kyzza Terrazas and debuted at the Cannes Film Festival on May 21, 2007.

In the film the lives of the rich Cristobal and his sister are contrasted with those of the servants on the estate. However, the riches are from illegal sources it would seem, and his father is on the run in Europe. In particular it is made clear that his childhood friend Adan, is now firmly kept in his place as the family gardener, and is no longer treated as a friend.

==Plot== Indian servants. They drive through a group of protesters without ever really wondering what they are protesting about. His hippie sister, Elisa, has already invaded the place with her drug-addled friends hailing from places such as Brazil and Japan. The two groups seem disparate but its nothing that cant be resolved through sharing alcohol and marijuana. Already at the house is a family who work as the groundskeepers and Adan, also a twenty-something who has grown up with them but works for them, creating an uncomfortable atmosphere as Cristobal barks out orders.
 Argentine girl football match in the garden, Cris purposely fouls Adan, showing his bitterness.
 ecstasy trip, wanders into the garden and finds Adan sitting in a tree on the perimeter, as he had returned to observe their antics after he had already left. She kisses him, but then begins to Drug overdose|overdose. She calls for Cristobal and he takes her into the house and blames Adan for what has happened. Cristobel starts a fight with Adan but is overpowered, and in frustration calls Adan an Indio, as a derogatory, deeply hurting Adan and reinforcing their differences. Elisa insists that he should not take her to a hospital. He finds she has been given three pills by one of the boys and the guests are asked to leave. Mafer then turns up, furious that he has been flirting with other women while she was trying to get to the party (Cris having ignored her phone call asking to come and pick her up). Cris receives a mobile phone call, which he can see is from his father, but he doesnt answer it and bursts into tears. The day saw him rejected from Harvard, lose his girlfriend (and Dolores) and his sister overdose on ecstasy, with the rift from his parents firmly entrenched and the differences between Adan perceived

==Cast==
* Gael García Bernal as Cristobal
* Luz Cipriota as Dolores
* Tenoch Huerta Mejía as Adan
* Camila Sodi as Elisa

==External links==
* 

 
 
 
 
 
 
 
 
 

 