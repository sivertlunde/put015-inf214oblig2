Rising from Ashes
{{multiple issues|
 
 
}}
 
Rising From Ashes is an independent film about the development of a national cycling team in Rwanda, a country still affected by 1994 Rwandan Genocide where an estimated 500,000–1,000,000 Rwandans were killed.  

==Plot== Jacques “Jock” Boyer moves to Rwanda in 2006 to help a group of struggling genocide survivors working to form a national cycling team. The team is composed of children left orphaned and traumatized by the genocide a decade earlier. Over the course of the story, both Boyer and the team "rise from the ashes" of their pasts with the help of their new achievements.
 illiterate and malnourished, living without water, electricity or healthcare, and most are recovering from the psychological effects of the 1994 genocide. Eventually, "Team Rwanda" is viewed as a symbol of hope for Rwanda, ambassadors for the recovering country. In subsequent years, the team expands its vision and develops a model of caring for athletes. In 2012, the team begins developing the first all-African team to match up to the Tour de France after one of the riders qualifies for the 2012 Summer Olympics.

==Reviews==
Daphne Howland of The Village Voice called Rising From Ashes “a remarkable documentary. It’s not just about a cycling team; it’s a testament to what happens when human beings care for one another.” 

“The film is crisp and economical,” said Frank Schneck of The Hollywood Reporter. “The film … avoids extraneous melodramatics, which, after all, are hardly necessary in a tale that already contains such inherently powerful drama.” 

The review aggregation website Rotten Tomatoes reports that 79% of critics gave the film a positive review based on 19 reviews, with an average score of 5.9/10. 

==Production==
Rising From Ashes was produced by two partnering non-profit organizations, Gratis 7 Media Group and Project Rwanda, over the course of six years. Forest Whitaker narrated the film, which was produced with $800,000 that came exclusively from donors. Since its release in 2012, the film has won awards at more than a dozen film festivals worldwide. 

==References==
 

== External links==
* 

 
 
 
 
 