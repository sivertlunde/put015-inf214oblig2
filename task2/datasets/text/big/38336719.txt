The Romany
 
 
{{Infobox film
| name           = The Romany
| image          =
| caption        =
| director       = Floyd Martin Thornton  George Pearson
| writer         = Eliot Stannard
| starring       = Victor McLaglen   Irene Norman   Harvey Braban   Peggy Hathaway
| music          = 
| cinematography = Percy Strong
| editing        = 
| studio         = Welsh-Pearson
| distributor    = Jury Films
| released       = January 1923
| runtime        = 5, 700 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}
 silent adventure film directed by Floyd Martin Thornton and starring Victor McLaglen, Irene Norman and Harvey Braban.

==Cast==
* Victor McLaglen as The Chief 
* Irene Norman as Valia 
* Harvey Braban as Andrew MacDonald 
* Peggy Hathaway as Flora 
* Malcolm Tod as Robbie 
* Ida Fane as Zilla 
* Hugh E. Wright  
* Harry Agar Lyons   
* N. Watt-Phillips

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 