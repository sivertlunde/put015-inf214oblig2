For Those I Loved
{{Infobox film
| name           = For Those I Loved
| image          = Au nom de tous les miens (1983).jpg
| director       = Robert Enrico
| producer       = Pierre David Robert Enrico
| writer         = Max Gallo Michael York Helen Hughes Wolfgang Müller Dennis OConnor Laurent Lopez Boris Bergman
| music          = Maurice Jarre
| cinematography = François Catonné
| studio         = Les Productions Mutuelles Ltée Producteurs Associés TF1 Films Production
| distributor    = Cinema International Corporation (CIC) Les Films René Malo
| released       = 7 June 1990 in USA, 9 November 1983 in France, 12 October 1983 in Canada
| runtime        = 145 min
| country        = Canada France Hungary French
}}
 Michael York, about a Polish Jewish Holocaust survivor who emigrated to the USA in 1946. It was directed by Robert Enrico for the Les Productions Mutuelles Ltée.

== Plot == Martin Gray. Reform Jews, where he lived with his family in Warsaw Ghetto after the Nazi invasion of Poland. The character supports his family with black-market supplies and joins the Resistance. He is deported to the Treblinka camp, where he manages to survive and then escape. Afterwards he joins the partisan forces and then the Red Army, taking part in the capture of Berlin.

After the war he left the Red Army and went in search of his grandmother, the sole survivor of his family. He found his grandmother in New York and emigrated to United States|America. He became a successful businessman there. Then he married Dina, with whom he had four children. After the birth of their first child, the protagonist moved with his family back to France. There in 1970 his wife and children tragically lost their lives in a forest fire. In 1976 he married again and had three more children. He started a foundation to teach others about his experiences.
 Captain Wacław Kopisto. Nowiny Rzeszowskie,   Nr 163. "Kim jest Martin Gray?" (Who is Martin Gray) Polish daily newspaper Nowiny Rzeszowskie, 2 August 1990; interview with Captain Wacław Kopista (sic).   

==See also==
 

==External links==
*  
*  
*    

==Discography==

The CD soundtrack composed by Maurice Jarre is available on Music Box Records label ( ).

 
 
 
 