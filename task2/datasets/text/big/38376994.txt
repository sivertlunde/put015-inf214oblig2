Forget Us Not
{{Infobox film
| name = Forget Us Not
| image = Forget Us Not theatrical release poster.jpg
| caption = Official poster
| director = Heather Connell
| producer = Heather Connell
| writer = Heather Connell, Lane Shadgett
| starring = Ron Perlman
| music = Sherene Strausberg
| cinematography = Arthur Lee
| editing = Jason Rosenblatt
| studio = Displaced Yankee Productions
| distributor = 
| released =  
| runtime = 
| country = United States
| language = English
| budget =
}}
Forget Us Not is a 2013 feature-length documentary film by Heather Connell, which follows the stories of some of the 5 million non-Jewish Holocaust survivors including artist Ceija Stojka and is narrated by actor Ron Perlman. The documentary was released on the festival circuit in August 2013 and has won eight awards to date including Feature Documentary and Editing Awards Of Merit from Accolade Film Competition, Helping Hand International Humanitarian Award from the Rhode Island International Film Festival,    Best of Festival at Vancouvers Columbia Gorge International Film Festival,  Mark Of Distinction Film at the New York Independent Film Festival and Best Narration and World Peace Impact Award from the Artisan World Peace Hamptons Film Festival    and Feature Documentary Audience Award at the First Glance International Film Festival Los Angeles.

==Synopsis==
Forget Us Not is a look at the persecution and death of the 5 million non Jewish victims of the World War II Holocaust and the lives of those who survived including:

Robert Wagemann, born with a physical disability, finds himself targeted twice. Nearly euthanized for being imperfect, he is snuck out of the hospital by his mother and spends the war hiding with his parents who have arrest warrants out against them for being Jehovahs Witnesses.
 Roma girl, sees her father arrested and dragged off to Dachau, where he later dies. Shortly after, she is rounded up with the rest of her family and shipped to Auschwitz.   
 Polish Catholic teen is separated from the rest of her family and sent to a concentration camp, where she survives each brutal day by sewing uniforms for her Nazi captors before being sent on a 900 mile death march.

Natalia Orloff, a Ukraine child, is marched with her family to the Polish border before being put on a train and sent to a work camp where sickness and starvation dominates her life and memories. Through personal stories of survivors and original historical footage, these lesser known voices are brought to life.

==References==
 

==External links==
* 
* 
* 

 
 
 
 


 