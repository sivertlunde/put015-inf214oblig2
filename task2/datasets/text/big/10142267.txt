Adam Had Four Sons
{{Infobox film
| name           = Adam Had Four Sons
| image          = Adam Had Four Sons.jpg
| image size     =
| caption        = Italian poster for the film.
| director       = Gregory Ratoff
| producer       = Robert E. Sherwood William Hurlbut Michael Blankfort
| narrator       =
| starring       = Ingrid Bergman Warner Baxter Susan Hayward Fay Wray
| music          = W. Franke Harling Rudy Schrager
| cinematography = J. Peverell Marley
| editing        = Francis D. Lyon
| distributor    = Columbia Pictures
| released       = February 18, 1941
| runtime        = 81 min.
| country        = United States English
| budget         = $488,000 est.
| gross          =
| preceded by    =
| followed by    =
}}
Adam Had Four Sons is a 1941 drama and romance film, starring Ingrid Bergman, Warner Baxter, and Susan Hayward.

==Plot==
Adam Stoddard (Baxter) is a wealthy, easy-going family patriarch who falls on hard times after the death of his wife and a stock market crash that wipes out his wealth. Recently arrived governess Emilie (Bergman) works to keep the family together. But with the loss of Adams fortune the boys are sent off to boarding school, their schooling paid for by a wealthy cousin. Emilie must return to France until Adam can afford to repurchase the family estate and recall her to look after it. Reversing his fortunes takes Adam several years. By then, the three older boys are fighting in World War II. Then, just as the family is getting back to its former way of life...

One son returns with his new wife, Hester (Hayward), who turns out be a conniving, evil woman wanting to rule the roost. She schemes to rid the home of Emilie, and seduces another son, Jack, while her husband is away at war. Emilie discovers the affair, but keeps quiet to preserve Adams happiness and the brothers reputation, pretending to Adam that she was the one involved with Jack.

Ultimately, all is discovered, and Hester receives her comeuppance and is evicted from the home. Emilie and Adam become engaged, and all ends happily.

==Cast==
*Ingrid Bergman — Emilie Gallatin
*Warner Baxter — Adam Stoddard
*Susan Hayward — Hester Stoddard
*Fay Wray — Molly Stoddard
*Richard Denning — Jack Stoddard
*Johnny Downs — David Stoddard Robert Shaw — Christopher Stoddard
*Charles Lind — Phillip Stoddard
*Helen Westley — Cousin Phillipa
*June Lockhart — Vance
*Pietro Sosso — Otto

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 


 