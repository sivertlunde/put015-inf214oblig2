Vicky Cristina Barcelona
{{Infobox film
| name = Vicky Cristina Barcelona
| image = Vicky cristina barcelona.jpg
| caption = Theatrical release poster
| director = Woody Allen
| producer = Letty Aronson Jaume Roures Stephen Tenenbaum Gareth Wiley
| writer = Woody Allen
| narrator = Christopher Evan Welch
| starring = Javier Bardem Penélope Cruz Scarlett Johansson Rebecca Hall Patricia Clarkson Kevin Dunn Chris Messina 
| cinematography = Javier Aguirresarobe
| editing = Alisa Lepselter Wild Bunch The Weinstein Company
| distributor =   website 
| released =  
| runtime = 97 minutes
| country = Spain United States
| language = English Spanish
| budget = $15 million   
| gross = $96,409,300 
}}

Vicky Cristina Barcelona is a 2008 romantic comedy-drama film written and directed by Woody Allen. The plot centers on two American women, Vicky (Rebecca Hall) and Cristina (Scarlett Johansson), who spend a summer in Barcelona where they meet an artist, Juan Antonio (Javier Bardem), who is attracted to both of them while still enamored of his mentally and emotionally unstable ex-wife María Elena (Penélope Cruz). The film was shot in Spain in Barcelona, Avilés and Oviedo, and was Allens fourth consecutive film shot outside of the United States.
 Best Motion Picture – Musical or Comedy. Cruz won both the Academy Award and BAFTA Award for Best Actress in a Supporting Role. Altogether, the film won 25 out of 56 nominations.

==Plot==

Vicky (Rebecca Hall) and Cristina (Scarlett Johansson) visit Barcelona for the summer, staying with Vickys distant relative Judy (Patricia Clarkson) and her husband Mark (Kevin Dunn). While the two are great friends, Vicky is practical and traditional in her approach to love and commitment and is engaged to the reliable Doug (Chris Messina), whereas Cristina imagines herself to be a nonconformist, spontaneous but unsure of what she wants from life or love.

At an art exhibition, Cristina is intrigued by artist Juan Antonio (Javier Bardem), who Judy says has suffered a violent relationship with his ex-wife. Later, he brazenly approaches the two women to invite them to join him right away for the weekend in the city of Oviedo, in a small plane he flies himself, for sight-seeing, fine eating and drinking, and hopefully, love-making. Cristina is won over by the offer almost at once, but Vicky is unimpressed and reluctant; she however eventually decides to accompany her friend anyway, mainly to watch over her.

At the end of their first day, Vicky refuses to join Juan Antonio in his hotel room, citing her fidelity to Doug, but Cristina accepts his invitation immediately.  Before the love-making starts, Cristina suddenly falls ill with digestive complaints, and is put to bed, with food poisoning.  Vicky and Juan Antonio proceed to spend the weekend together alone while they wait for Cristina to recuperate. Vicky gradually changes her opinion of Juan Antonio as he tells her about his tumultuous relationship with his former wife, María Elena (Penélope Cruz). Vicky accompanies him to visit his father, an old poet, and then becomes deeply moved by a Spanish guitar performance later that evening. She finally succumbs to Juan Antonios advances as they walk through a grove of trees in the dark. The next day, with Cristina recovered, the three of them fly back to Barcelona.

Feeling guilty, Vicky does not mention the incident to Cristina, and the two begin to grow apart. Vicky starts throwing herself into her studies while Cristina and Juan Antonio take up a relationship.  Cristina then moves in with Juan Antonio and begins to discover more about his past. After learning that María Elena attempted to kill herself, Juan Antonio brings her to his home, where Cristina already lives. After some defiance, the two women grow fond of each other. Cristina realizes that the ex-spouses are still in love, and María Elena suggests that Cristina may be the element that can give balance and stability to their relationship. All three become romantically involved with one another.

In the meantime, Vicky is joined in Spain by an enthusiastic Doug and the two get married. When Cristina describes her new life with Juan Antonio to Vicky, Vicky becomes secretly jealous, and after a few other awkward moments, she realizes she is unsatisfied in her married life and is still attracted to Juan Antonio. Learning that Judy is similarly unhappy in her marriage, she confides to her, and Judy, who sees her younger self in Vicky, decides to bring Juan Antonio and Vicky together. Meanwhile, Cristina becomes restless and at some point decides to leave Juan Antonio and María Elena; without her, their relationship quickly falls apart again.

As the summer winds to a close, Judy arranges for Juan Antonio and Vicky to meet at a party. Juan Antonio begs Vicky to meet him again privately before leaving Spain, which she finally accepts, lying to Doug in the process. At his home, Juan Antonio seduces and wins Vicky over again, but they are interrupted by María Elena who bursts in with a gun, firing wildly as Juan Antonio tries to calm her. Vicky gets shot in the hand in the process, and leaves, shouting they are insane and she could never live like this. She confesses the entire story to Cristina, who never realized how Vicky felt about Juan Antonio, and wishes she could have helped her. Doug, Vicky and Cristina return to America; Doug never learns what truly happened, Vicky goes back to her married life, and Cristina is back where she started, unsure of what she wants.

==Cast==
* Javier Bardem as Juan Antonio Gonzalo
* Penélope Cruz as María Elena
* Scarlett Johansson as Cristina
* Rebecca Hall as Vicky
* Chris Messina as Doug
* Patricia Clarkson as Judy Nash
* Kevin Dunn as Mark Nash
* Pablo Schreiber as Ben
* Carrie Preston as Sally
* Zak Orth as Adam
* Julio Perillán as Charles
* Christopher Evan Welch as Narrator
 Spanish actor Joan Pera, who dubbed Allens voice in his previous films, makes a cameo appearance. 

==Production==

In 2007, controversy arose in   and the Generalitat de Catalunya (Government of Catalonia) half a million, or ten percent of the films budget. 
 The Prestige.

The movie featured several paintings by the Catalan artist Agustí Puig. Puig was even commissioned to create a painting for the movie, the result being La Protectora II, though it was not used on set. 

The film was shot on location in Barcelona (Park Güell, Sagrada Família, Casa Milà) and Oviedo. 

==Critical reception==

===Box office===
 , the film grossed $96 million worldwide; in relation to its $15&nbsp;million budget,  it is one of Allens most profitable films. 

===Reviews===
Vicky Cristina Barcelona garnered the best reviews Allen received since his Academy Award|Oscar-nominated 2005 film Match Point (also starring Johansson), particularly for Cruzs performance, earning a rating of 81% on Rotten Tomatoes.   

Scott Tobias wrote in the   gave the film 4 out of 5, and wrote "within Allens recent output, Vicky Cristina is a highlight. See it for beautiful locales, an ambivalent look at human relationships and a clutch of great performances, especially from Cruz." 

===Top ten lists===
 
The film appeared on many critics top-ten lists of the best films of 2008.   

  David Denby, The New Yorker 
* 5th – Ray Bennett, The Hollywood Reporter 
* 5th – Robert Mondello, NPR 
* 7th – Joe Morgenstern, The Wall Street Journal 
* 7th – Keith Phipps, The A.V. Club 
* 7th – Kyle Smith, New York Post 
* 7th – Steve Rea, The Philadelphia Inquirer 
* 8th – Mick LaSalle, San Francisco Chronicle 
* 9th – Carrie Rickey, The Philadelphia Inquirer 
* 10th – Michael Sragow, The Baltimore Sun 
 

==Awards and nominations==
 
The film won the Golden Globe Award for Best Motion Picture – Musical or Comedy.  Woody Allen won his first Independent Spirit Award for Best Screenplay and was nominated for the Writers Guild of America Award for Best Original Screenplay.
 Academy of Motion Picture Arts and Sciences, the British Academy of Film and Television Arts, the Independent Spirit Awards, the Boston Society of Film Critics, the Goya Awards, the Kansas City Film Critics Circle, the Los Angeles Film Critics Association, the National Board of Review, the New York Film Critics Circle, and the Southeastern Film Critics Association. 

==References==
 

==External links==
 
* 
* 
* 
* 
*   at The Numbers
*   at Movieloci.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 