Snow in August
{{Infobox film
| name           = Snow in August
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Richard Friedenberg
| producer       =  
| writer         = Richard Friedenberg (teleplay)
| screenplay     = 
| story          = 
| based on       =   
| starring       =  
| music          = Peter Himmelman
| cinematography = Serge Ladouceur
| editing        = Tatiana S. Riegel
| studio         = 
| distributor    = 
| genre			= Drama
| released       =  
| runtime        = 104 minutes
| country        = United States and Canada
| language       = English
}}
 New York Times best selling novel by Pete Hamill.    Featuring a young boy from an Irish Catholic neighborhood of Brooklyn in 1947, the film follows the unlikely friendship that evolves between 11 year old Michael and a Czechoslovakian Rabbi that stirs up the wrath of a local Irish gang from Michaels neighborhood.    After a tragic turn of events, Michael seeks the help of an ancient Jewish text to summon a protector in his time of need.    The film is a made-for-television movie that was distributed by Showtime Networks in 2001, and later released on DVD in 2003.    

==Plot==
In Brooklyn in the summer of 1947, 11 year old Michael Devlin (Peter Tambakis) witnesses a brutal beating of a Jewish shopkeeper by a local Irish street gang called the Falcons. Scared to go to the police, Michael tells his widowed mother (Lolita Davidovich) who only consoles him by telling him God will judge the guilty. With a heavy feeling of guilt still following the young boy, Michael reaches out to Rabbi Judah Hirsch (Stephen Rea) who had fled Czechoslovakia during World War II. An unlikely friendship evolves between the two characters as they bond over baseball and alluring tales of Jewish folklore like the book of Kabbalah which has the power to create miracles such as snow in August. As a fatherless child, Michael eventually grows to see the Rabbi as close friend and father figure-- a relationship that ends up costing him his friends and brings unwanted attention from Frankie McMarthy (Adam MacDonald ) and the rest of the Falcon gang. The Falcons attack Rabbi Hirsch and later threaten Michael and his mother, demonstrating their outright disapproval of the relationship between a Jewish Rabbi and a Catholic family. Determined to finally have the courage to stand up against the gang, Michael goes to Rabbi Hirsch in the hospital to learn more about an ancient Jewish text and the ritual for summoning the Golem to help him serve justice and protect his loved ones.    

==Cast==

* Peter Tambakis as Michael Devlin
* Stephen Rea as Rabbi Judah Hirsch 
* Lolita Davidovich as Kate Devlin
* Adam MacDonald as Frankie McCarthy

==Reception== People Magazine said the film including a good cast and a heart warming set of values.    

The film received three Emmy nominations including Outstanding Direction in a Childrens Special for Richard Friedenberg and Outstanding Performer in a Childrens Special for Stephen Rea.   Finally, the film was nominated for Outstanding Childrens Special in which all the producers were credited including producers Jake Eberts, Lenny Young, Daniel Ostroff, Nancy Cooperstein, and Tani Cohen.   

==References==
 