Dong (film)
{{Infobox film
| name           = Dong
| image          = Dong_Poster.jpg
| caption        = 
| director       = Jia Zhangke
| producer       = Yu Lik-wai Zhu Jiong Chow Keung Dan Bo
| writer         = 
| starring       = Liu Xiaodong
| music          = Lim Giong
| cinematography = Yu Lik-wai Jia Zhangke Chow ChiSang Tian Li
| editing        = Kong Jinglei Zhang Jia
| distributor    = Xstream Pictures
| released       =  
| runtime        = 66 minutes
| country        = China Hong Kong Mandarin Sichuanese Sichuanese Thai Thai
| budget         = 
}} Still Life, which was released concurrently although Dong was reputedly conceived of first.     The film, which runs a relatively short 66 minutes, follows the artist and actor Liu Xiaodong as he invites Jia to film him while he paints a group of laborers near the Three Gorges Dam (also the subject of Still Life) and later a group of women in Bangkok. The film was produced and distributed by Jias own production company, Xstream Pictures, based out of Hong Kong and Beijing.
 Venice International Film Festival as part of its "Horizons" Program, and as part of the 2006 Toronto International Film Festivals "Real-to-Reel" Program. 
 HD digital video.

== Relationship with Still Life ==
Filmed at the same time as Jias fiction film, Still Life, Dong also shares the same setting (the Three Gorges area of central China) and in certain instances, the same shots. Han Sanming, one of the leads in Still Life, also appears (in character) within Dong as do other characters from that film. 
 Still Life, which would go on to win the 2006 Golden Lion at the Venice Film Festival.

In contrast, Dong generated far less publicity, prompting one critic to deride it as a "minor addition" to Jia Zhangkes canon. 

== See also ==
 Still Life - Companion film
* Three Gorges Dam

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 


 
 