When Johnny Comes Marching Home (film)
{{Infobox film
| name           = When Johnny Comes Marching Home
| image          = When Johnny Comes Marching Home.jpg
| image_size     =
| caption        =
| director       = Charles Lamont
| producer       = Bernard W. Burton
| writer         = Dorothy Bennett Oscar Brodney Allan Jones Jane Frazee Gloria Jean Donald OConnor Peggy Ryan
| music          = Ted Cain
| cinematography = George Robinson
| editing        = Charles Maynard
| distributor    = Universal Pictures
| released       =  
| runtime        = 74 minutes
| country        = United States English
| budget         =
| gross          =
}} 1942 musical Allan Jones song with the same title.

==Plot==
Johnny Kovacs (Jones) is a war hero who comes back home for a ten-day leave. Pursued by a woman (Shelton) who considers herself his fiance, he works with his superior officers to hide during his leave. He adopts the name Johnny ORourke, and finds a room at a theatrical boarding house. He becomes friends with some other boarders (including OConnor, Ryan and Gloria Jean) and falls in love with a woman (Frazee). His friends overhear him talking to his officer on the phone. They misinterpret the conversation and conclude that he is a deserter. They push him into giving himself up and returning to duty. Confused by their behavior at first, he figures out what they are up to, and plays along. It all works out in the end. The closing song in the picture is a rousing patriotic number sung directly to the audience by the main players in the film.

==Cast== Allan Jones as Johnny Kovacs aka Johnny ORourke
*Jane Frazee as Joyce Benton
*Gloria Jean as Marilyn Benton
*Donald OConnor as Frankie Flanagan
*Peggy Ryan as Dusty
*Richard Davies as Lt. Tommy Bridges
*Clyde Fillmore as Hamilton Wellman
*Marla Shelton as Diana Wellman
*Olin Howland as Trullers
*Emma Dunn as Ma (Norah) Flanagan
*Four Step Brothers as Themselves
*Phil Spitalny as Himself

==Soundtrack==
*My Little Dream Girl
**Written by A. Friedland and L. Wolfe Gilbert
**Sung by Allan Jones

*You and the Night and Music
**Written by Howard Dietz and Arthur Schwartz
**Sung by Allan Janes and Jane Frazee

*Romance
**Written by Edgar Leslie and Walter Donaldson
**Sung by Allan Jones and Gloria Jean  

*Say It With Dancing
**Written by Don Raye and Gene de Paul
**Sung by Gloria Jean, Peggy Ryan, and Donald OConnor

*This Is It
**Written by Don Raye and Gene de Paul
**Sung by Allan Jones and Jane Frazee 

*Green Eyes
**Written by Adolfo Utera and Nilo Menendez
**Sung by Gloria Jean 

*One Of Us Has Gotta Go
**Written by Inez James and Buddy Pepper
**Sung by Gloria Jean, Peggy Ryan, and Donald OConnor

*This Is Worth Fighting For
**Performed by the (credited) Phil Spitalny All-Girl Orchestra with Evelyn and Her Magic Violin 

*The Yanks Are Coming
**Written by Harry Seymour
**Performed by Allan Jones, Jane Frazee, Gloria Jean, Donald OConnor, Peggy Ryan with the Phil Spaltany All-Girl Orchestra

*We Must Be Vigilant
**Written by Edgar Leslie and Joseph Burke
**Performed by the Phil Spaltany All-Girl Orchestra with Evelyn and Her Magic Violin

*Red Safarin

*Jazz Etude

==Reception==
The New York Times called Universal Pictures "miraculous" for "bringing this star-studded entertainment in under budget and within a 73-minute running time". 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 