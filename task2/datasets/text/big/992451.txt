After the Sunset
{{Infobox film
| name           = After the Sunset
| image          = AfterTheSunsetPoster.jpg
| caption        = International poster
| director       = Brett Ratner
| producer       = Beau Flynn Jay Stern Tripp Vinson
| screenplay     = Paul Zbyszewski Craig Rosenberg
| story          = Paul Zbyszewski
| starring       = Pierce Brosnan Salma Hayek Woody Harrelson Don Cheadle
| music          = Lalo Schifrin
| cinematography = Dante Spinotti Mark Helfrich
| studio         = 
| distributor    = New Line Cinema
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $59 million
| gross          = $61,347,797
}} FBI agent Stan Lloyd, played by Woody Harrelson. The film was directed by Brett Ratner and shot in the The Bahamas|Bahamas.

==Plot==
Master thief Max Burdett (Pierce Brosnan) and his beautiful accomplice, Lola Cirillo (Salma Hayek), steal the second of three famous diamonds, known as the Napoleon diamonds, from FBI Agent Stanley P. Lloyd (Woody Harrelson). But Lloyd shoots Max before passing out from being gassed by the thieves. Max survives and tells Lola to get the diamond. She does, leaving in its place the one-dollar bill that she had received as a tip for washing the agents windshield (while in disguise). Max and Lola then fly to Paradise Island in The Bahamas. 

Burdett unwittingly turns the tables and befriends the frustrated detective Lloyd, showing him the pleasures that Paradise Island has to offer, even paying for the most expensive suite, the bridge suite, for as long as Lloyd is there. Lloyd, out of his element, adapts quickly to the easy-going Caribbean lifestyle. 

Burdett, still wanting the diamond for himself, pretends to work with Mooré, and gives him a fake plan as to how he would steal the diamond (which he earlier related to Stan). Stan however has teamed up with local police constable Sophie (Naomie Harris) to catch Burdett, and tails him to Junkanoo, a local parade, where Max loses him, warning that he shouldnt tail so closely before he is hit in the face by a tuba player swinging his tuba by Maxs request. Lola kicks Max out after he breaks his promise to spend their first sunset on her new deck she had been working on and after she finds out he lied about writing his vows to her. Max is forced to bunk with Stan, and they share their thoughts about each others lives. The next morning, the authorities and Sophie discover them, revealing that Stans FBI license is suspended.

The next day, Max is met by Stan while celebrating, who tells him he was never drunk the night Burdett had to bunk with him, and details how he let Max do all the work while he later recovered the diamond. Max concedes that his nemesis has won this time, and is simply happy to live out his life with and watch sunsets with Lola. However, he has fun with Stan when he tries to leave by remote controlling his car again, promising Lola it is the last time.

==Cast==
* Pierce Brosnan as Max Burdett
* Salma Hayek as Lola Cirillo
* Woody Harrelson as Stan Lloyd
* Don Cheadle as Henri Mooré
* Naomie Harris as Sophie
* Rex Linn as Agent Kowalski, a fellow FBI agent.
* Mykelti Williamson as Agent Stafford
* Troy Garity as Luc
* Obba Babatundé as Zacharias Michael Bowen as FBI Driver
* Russell Hornsby as Jean-Paul, Moorés bodyguard
* Mark Moses as Lakers FBI Agent
* Chris Penn as Rowdy Fan
* Joel McKinnon Miller as Wendell
* Alan Dale as Security Chief
* Noémie Lenoir as Moorés Girl
* John Michael Higgins as Hotel Manager (uncredited)

The film also features several cameos, including Gary Payton, Karl Malone, Chris Penn, Phil Jackson, Jeff Garlin, Dyan Cannon, Edward Norton and Shaquille ONeal as themselves.

==Production==
Paul Zbyszewskis original screenplay for After the Sunset was discovered by producers Beau Flynn and Tripp Vinson, both known for producing movies such as Tigerland (2000) and Requiem for a Dream (2000). The script was purchased by New Line Cinema and the producers hired Australian screenwriter Craig Rosenberg to create a re-write. Both the studio and the producers agreed that their first choice for the role of master thief Max Burdett was Pierce Brosnan.  Salma Hayek, Academy Awards|Oscar-nominated for her role in Frida (2002), was the next to join the cast. 
 John Stockwell but Stockwell dropped out due to creative differences.  Talking about joining the movie, Ratner said: "I love caper films. There are so many great films in this genre, but what makes After the Sunset different is that its a heist movie that has a combination of great relationships, heart and comedy."

Chris Tucker and Jackie Chan were both offered to film cameos as American police officers (a nod to the Rush Hour (film series)|Rush Hour series which Brett Ratner also directed) but turned them down.

With the two  s casting marked a third collaboration with Ratner, following The Family Man (2000) and Rush Hour 2 (2001). The role of Sophie, the Bahamian cop, was the next role to be cast. British actress Naomie Harris landed the role. 

With the majority of the script set on an island in the Caribbean, the filmmakers decided to shoot in The Bahamas, Basing their production out of Kerzner Internationals Atlantis resort in Nassau, Bahamas|Nassau, cast and crew flew in from Los Angeles, Miami and New York City to commence filming.

==Reception== normalized rating system, the film earned a rating of 38/100 based on 32 reviews.   

==Box office==
The film opened at #3 in the North American box office, earning $11,100,392 in its opening weekend, with its widest release in 2,819 theaters. It grossed $28,331,233 domestically and $33,016,564 in international markets, adding up to a worldwide gross of $61,347,797.   

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 