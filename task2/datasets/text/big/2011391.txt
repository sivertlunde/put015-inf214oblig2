The Big Trail
{{Infobox film
| name           = The Big Trail
| image          = BigTrail.jpg
| caption        = Movie poster for The Big Trail
| director       = Raoul Walsh
| producer       = Winfield R. Sheehan
| writer         = {{plainlist|
*Hal G. Evarts (story)
*Marie Boyle
*Jack Peabody
*Florence Postal
*Fred Sersen
}}
| starring       = {{plainlist|
*John Wayne
*Marguerite Churchill
*Tyrone Power, Sr.
*El Brendel
}}
| music          = {{plainlist| Arthur Kay
*Reginald Hazeltine Bassett
*Peter Brunelli
*Alfred R Dalby
*Jack Virgil
}}
| cinematography = {{plainlist|
*Lucien Andriot
*Arthur Edeson
}} Jack Dennis
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 122 min. 70&nbsp;mm version, 108 min. 35&nbsp;mm version
| country        = United States
| language       = English
| budget         = $1,250,000 
}}
 American West starring John Wayne in his first leading role and directed by Raoul Walsh.
 Grandeur wide-screen process used in filming." 

==Pre-production==
 
After the film was given the  , despite the fact that Wayne had done more than 30 movies by the time it was produced.)  Filming began on-location just outside Santa Fe, New Mexico in April 1930, which was unheard of at the time and quickly became very costly to the studio. 

==Production==
The film was shot in an early widescreen process using 70&nbsp;mm film called "70 mm Grandeur film," which was first used in the film "Fox Movietone Follies of 1929."   The scene of the wagon train drive across the country was pioneering in its use of camera work and the depth and view of the epic landscape.  An effort was made to lend authenticity to the movie, with the wagons drawn by oxen instead of horses - they were lowered by ropes down canyons when necessary for certain shots in narrow valleys.  Tyrone Powers characters clothing looks realistically grimy, and even the food supplies the immigrants carried with them in their wagons were thoroughly researched. Locations in five states, starting from New Mexico to California, were used to film the caravans   trek. 
 heart attack.) Also during production, John Wayne fell sick from dysentery and was nearly replaced. 

==Release in theaters and the effect on future widescreen viewings==
After shooting, the film was previewed to select audiences and generally released on October 1930. The movie quickly became a box office bomb because it was released as a widescreen film during a time when theatres would not change over their standard screens due to the huge cost, and mainly because the Great Depression left so many exhibitors almost financially ruined and the film barely made its huge investment back.  It would be almost 20 years before the concept of widescreen films would be put back into general production.

After completion of the film, Wayne found stardom only in low-budget serials and features (mostly Poverty Row Western (genre)|westerns); it would take nine years and the film Stagecoach (1939 film)|Stagecoach to return Wayne to mainstream prominence.   
 extra in films directed scout dressed in buckskins similar to Waynes outfit.

== Cast ==
 
* John Wayne as Breck Coleman
* Marguerite Churchill as Ruth Cameron
* El Brendel as Gus, a comical Swede
* Tully Marshall as Zeke, Colemans sidekick
* Tyrone Power, Sr. as Red Flack, wagon boss
* David Rollins as Dave "Davey" Cameron Frederick Burton as Pa Bascom
* Russ Powell as Windy Bill (uncredited) 
* Ian Keith as Bill Thorpe, Louisiana gambler Charles Stevens as Lopez, Flacks henchman (Geronimo|Geronimos Grandson)
* Louise Carver as Guss mother-in-law
* John Big Tree as Indian Chief (uncredited)
* DeWitt Jennings as Boat Captain Hollister (uncredited)
* Ward Bond as Sid Bascom
* Marjorie Leet as Mildred Riggs (uncredited)
* Dodo Newton as Abigail Vance (uncredited)  
* Jack Peabody as Bill Gillis (uncredited) 
* William V. Mong as Wellmore (uncredited) 
* Marcia Harris as Mrs. Riggs (uncredited) 
* Emslie Emerson as Sairey (uncredited)  Chief Cochises Grandson)
* Iron Eyes Cody as Indian (uncredited)
* Alphonse Ethier as Marshal (uncredited) 
* Victor Adamson as Wagon Train Man (uncredited)
* Don Coleman as Wrangler (uncredited) 
* Andy Shuford as Ohio Mans Son (uncredited) 
* Helen Parrish as Honey Girl Cameron (uncredited)  Marilyn Harris as Pioneer Girl (uncredited)  Jack Curtis as Pioneer (uncredited)
* Jack Padjan as Pioneer (uncredited) 
* Pete Morrison as Wrangler (uncredited) 
* Gertrude Van Lent as Sister from Missouri (uncredited)
* Lucille Van Lent as Sister from Missouri (uncredited) 
* Robert Parrish as Pioneer Boy (uncredited) 
* Apache Bill Russell (uncredited)
* Marion Lessing (uncredited)   
* Dannie Mac Grant (uncredited) 
* Frank Rainboth as Ohio Man (uncredited) 
 

==Plot== Charles Stevens). Prairie schooners would follow Wellmores ox-drawn train of Conestoga Wagons, as the first major group of settlers to move west on the Oregon Trail. The action takes place between 1837 and 1845. Ruth tells Honey Girl that there are 26 stars in the flag, dating the movie between Jan., 1837 and March 1845.  This is historically accurate, as the first major wave of settlers on the Oregon Trail was in 1843, although the details were completely different.

Coleman finds love with young Ruth Cameron (Marguerite Churchill), whom hed kissed accidentally, mistaking her for somebody else. Unwilling to accept her attraction toward him, Ruth gets rather close to a gambler acquaintance of Flacks, Thorpe (Ian Keith), who joined the trail after being caught gambling. Coleman and Flack have to lead the settlers west, while Flack does everything he can to have Coleman killed before he finds any proof of what hed done. The three villains main reason for going west is to avoid the hangmans noose for previous crimes, and all three receive frontier justice instead. The settlers trail ends in the Willamette Valley of Oregon, where Coleman and Ruth finally settle down together amidst giant redwoods.

==Preservation and re-release==
The film was mostly forgotten to history until the early 1980s, when the Museum of Modern Art in New York City, which housed the 65mm nitrate original camera negative, wanted to preserve the film but found that it was too shrunken and fragile to be copied due to lack of real preservation, and that no film lab would touch it.

They sought out and found Karl Malkames, a cinematographer and specialist and pioneer in film reproduction, restoration, and preservation. He immediately designed and built a special printer to handle the careful frame-by-frame reproduction of the negative to a 35mm anamorphic CinemaScope fine grain master to duplicate the original 70mm process.  The printer copied at a speed of one frame a second, leading to the films year-long process in preserving its original look. 

==The two versions==
 
Beyond the format difference, the 70mm and 35mm versions vary substantially from each other. They were shot by different cameras, and footage for each format was edited separately in the cutting room. Some scenes were shot simultaneously by both cameras, the only difference being the angle (with the better angle usually given to the 70mm camera). Some scenes were shot first by one camera, and then retaken with the other camera.  The 70mm cameras could not focus well up close, so their shots were mainly panoramas with very few close-ups. The 35mm cameras could move in and focus at short distances.  Thus, scenes in the 70mm version might show two characters talking to each other in the same take, making greater use of the widescreen frame, while the 35mm version would have close-up shots cutting back and forth between the two characters.

In the editing of the films, some scenes were edited out for one version but allowed to remain in the other. The 35mm version was edited to be shorter, so many scenes in the 70mm version are not found in the 35mm. However, there are a few scenes in the 35mm version not found in the 70mm. The 35mm version was 108 minutes, but the 70mm was longer at 122 minutes.

The 70mm version has been released on VHS as well as DVD in its original widescreen format, but it was also   TV screen, despite the availability of the 35mm version which is closer to that format.

==Home video releases==
The 70mm version was finally seen on cable television in the late 1990s. A radically different 35mm version had been released to VHS and DVD previously for several years. 

A two-disc restored DVD was released in the US on May 13, 2008 featuring the 35mm and the 70mm version. A Blu-ray edition featuring the 70mm version was released in September 2012. 

==Trivia==
According to the Nov. 12, 1930 issue of the Idaho Falls Post, this movie was once set to be titled "The Oregon Trail". The change, as stated, was made in response to the requests from nearby residents of Jackson, WY, where the bulk of the movie was filmed. 

John Waynes first movie role. Raoul Walsh was having trouble casting the movie when he saw Wayne taking furniture off a truck. Wayne worked for the studio in the prop department. 

This was Marion Morrisons first starring role and the producers didnt like his name. They told Raoul Walsh (the director) Morrison needed a new name. Several names were suggested, one of them from Walsh, who had just read about American Revolutionary War general Mad Anthony Wayne. The studio took the hint, added the first name John and the rest was history. 

This was his only talking film of Tyrone Power Sr., father of Tyrone Power. He died in 1931. 

Reportedly this film debuted at a running time of 158 minutes. However, this is unconfirmed as of May 2008. 

Incredibly, six different versions of this film were shot simultaneously. (1) a 70mm version in the Grandeur process for exhibition in the biggest movie palaces; (2) a standard 35mm version for general release; (3) a 35mm alternate French language version La piste des géants (1931) (4) a 35 mm alternate Spanish language version La gran jornada (1931),(5) a 35 mm alternate German language version Die große Fahrt (1931)and (6) a 35mm alternate Italian language version Il grande sentiero (1931). The four alternate language versions were shot with (mostly) different casts. 

This film was shot in both a format known as Fox Grandeur, a 70mm wide screen film process developed by the Fox Film Corporation and used commercially on a small scale in 1929-31, as well as a standard Academy ratio format. Both versions survive, and differ significantly in composition, staging and editing. Grandeur was a forerunner of the Todd-AO 70mm system which was introduced in 1955.) Grandeur was one of a number of wide screen processes which were developed by the major Hollywood studios alongside sound in the late 1920s and early 1930s. A combination of the Great Depression and the costs of converting thousands of cinemas to sound prevented the successful introduction of any of these projection systems on a commercial scale. 
 
Gary Cooper was originally offered the role of Breck Coleman and wanted it, but he was under contract to Paramount Pictures, which refused to loan him out. The role was eventually given to John Wayne.

== Foreign language versions == dubbing the dialogue.  There were at least four foreign-language versions made of The Big Trail, all filmed in 35mm, 1.20:1 ratio and using different casts and different character names:

* French: La Piste des géants (1931 in film|1931), directed by Pierre Couderc, starring Gaston Glass (Pierre Calmine), Jeanne Helbling (Denise Vernon), Margot Rousseroy (Yvette), Raoul Paoli (Flack), Louis Mercier (Lopez).   

* German: Die Große Fahrt (1931), directed by Lewis Seiler and Raoul Walsh, starring Theo Shall (Bill Coleman), Marion Lessing (Ruth Winter), Ullrich Haupt (Thorpe), Arnold Korff (Peter), Anders Van Haden (Bull Flack), Peter Erkelenz (Fichte), Paul Panzer (Lopez).  
 Snow White and the Seven Dwarfs (1937 in film|1937). 
 Jorge Lewis title character in the Spanish-language version of Dracula (Spanish Version)|Dracula (1931 in film|1931).  (Orena), Charles Stevens Stevens plays the same part in both the English and Spanish versions of The Big Trail.  (Lopez).  

==See also==
* John Wayne filmography
* List of American films of 1930

==Further reading==
* Elyes, Allen.  John Wayne. South Brunswick, N.J.: A.S. Barnes and Co., 1979. ISBN 0-498-02487-3.

==Notes==
 

== References ==
{{reflist|refs=
   
}}

== External links ==
*  , David Coles, 2001
*  , Arthur Edeson, A.S.C., American Cinematographer, September 1930.
*  
*  
*  
*   at Rotten Tomatoes

 

 
 
 
 
 
 
 
 
 