That Night in Rio
{{Infobox film
| name           = That Night in Rio
| image          = That Night in Rio.jpg
| image_size     =
| caption        =
| director       = Irving Cummings
| producer       = Darryl F. Zanuck
| based on       = The Red Cat (play)
| screenplay     = George Seaton Bess Meredyth Hal Long Samuel Hoffenstein Jessie Ernst Hans Adler (play)
| narrator       =
| starring       = Don Ameche Alice Faye Carmen Miranda
| music          = Mack Gordon Harry Warren
| cinematography = Ray Rennahan Leon Shamroy
| editing        = Walter Thompson
| distributor    = 20th Century Fox
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 musical comedy Hans Adler. Others are Folies Bergère de Paris (1935) and On the Riviera (1951).

The original songs for the film were written by the musical partnership of  ".

==Plot==
Larry Martin (Don Ameche) is an American entertainer in the Casino Samba in Rio de Janeiro. He has a skit in his show, making fun of the womanizing Baron Manuel Duarte (also Ameche). On one particular evening, the Baron and his wife, Baroness Cecilia Duarte (Alice Faye) come to see Larrys impersonation. To the surprise of the couple, the act is amazingly realistic. Backstage, the Baron meets Larrys girlfriend, Carmen (Carmen Miranda), and invites her to a party he is going to hold. Carmen declines.

Later in the evening, Larry meets Cecilia and is attracted to her singing and her beauty. He does an impersonation of the Baron for her. But the real Baron receives a telegram that his airline is in danger because a contract is not being renewed and he has already purchased 51% of the stock. Needing money to repay the bank he borrowed it from, he flies down to Buenos Aires.

Larry is hired to play the Baron to confuse his rival, Machado (J. Carrol Naish), but at the stock market, he buys the remainder of the airline stock. That evening, at the party, Larry is hired again to play the Baron. He does not want the Baroness to know, but Cecilia is informed without his knowing. He sweeps her off her feet and they stay close to each other for the remainder of the evening.

Meanwhile, Carmen is furious to discover that Larry is at the party and decides to go there as well, where she discovers that he is impersonating the Baron. To make matter worse, the real Baron returns to his house, confusing all involved. Machado corners Larry instead and talks to him in French, which Larry cant understand. After the party, the Baron discovers that Cecilia was flirting with Larry for the evening and tries to play the joke on her. She, however, inadvertently turns the tables on him.

To get back at his wife, the next morning, the Baron calls and tells Cecilia that his plane has just landed. Cecilia is scared that she has been unfaithful to Manuel but Larry later tells her the truth. At the office, Machado gives the Baron a payment of $32 million for his airline, the topic of his conversation with Larry. The Baron heads home but Cecilia tries one more time to get back at him by pretending to make violent love to Larry. It turns out to be the Baron and all is soon resolved in the end.

===Cast===
 
* Don Ameche as Larry Martin / Baron Manuel Duarte
* Alice Faye as Baroness Cecilia Duarte
* Carmen Miranda as Carmen, Larrys girlfriend
* S. Z. Sakall as Arthur Penna
* J. Carrol Naish as Machado, Manuel Duartes business rival
* Curt Bois as Felicio Salles
* Leonid Kinskey as Monsieur Pierre Dufond
* Frank Puglia as Pedro, Manuels valet
* Lillian Porter as Luiza, Cecilias maid
* Maria Montez as Inez 

=== Production ===
The working titles of this film were A Latin from Manhattan, Rings on Her Fingers, They Met in Rio and The Road to Rio. A November 15, 1940 HR news item noted that Twentieth Century-Fox had to change the title from The Road to Rio because of a conflict with Paramount, which wanted them not to release their picture until six months after the release of Paramounts Road to Zanzibar in order to avoid any confusion over the similar titles. This film marked the sixth and final teaming of Faye and Don Ameche, and the first film in which Carmen Miranda played a character with a name other than her own. According to modern sources, Faye and Ameche recorded and filmed the song "Chica, Chica, Boom Chic" as a dance number, but only the sequence of Miranda and Ameche singing the song is in the released picture.
 PCA filed noted that the picture was rejected for distribution in Ireland, although no reason was given. 

=== Soundtracks ===
*Chica Chica Boom Chic — Don Ameche and Carmen Miranda
*The Baron Is in Conference — Mary Ann Hyde, Vivian Mason, Barbara Lynn, Jean ODonnell
*The Conference — Don Ameche
*They Met in Rio (A Midnight Serenade) - Don Ameche (in Portuguese) and Alice Faye (in English)
*Cai Cai — Carmen Miranda
*I, Yi, Yi, Yi, Yi (I Like You Very Much) — Carmen Miranda
*Boa Noite (Good-Night) — Don Ameche, Alice Faye and Carmen Miranda 

==Release==
 
The film was released in theaters on April 11, 1941.

=== Critical reception ===
In his review for the newspaper Chicago Reader, Dave Kehr said "Shrieking Technicolor and Carmen Miranda are the main attractions in this routine Fox musical from 1941." 
 Variety wrote that “  Ameche is very capable in a dual role, and Miss   Faye is eye-appealing but it’s the tempestuous Miranda who really gets away 
to a flying start from the first sequence.”  The Los Angeles Herald-Examiner described Miranda as “outfitted in smart, barbaric colors, waving articulate hips and rollicking through the most fun of her Hollywood career.” 
 Daily Mirror, said that "Apparently exotic Carmen Mirandas lips are as fascinating as her hands." 

In his review of the film,   in the cast, That Night in Rio departs but little from the stock musical comedy formula, which inevitably sacrifices originality (...) In fact, the only departure of even slightly revolutionary degree is the employment of Don Ameche in dual roles, instead of his customary single one—and that, in the eyes of some people (including ourself), is hardly a step in the right direction". 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 