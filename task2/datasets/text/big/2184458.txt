Hustle & Flow
{{Infobox film
| name           = Hustle & Flow
| image          = Hustle and flow.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Craig Brewer
| producer       = Stephanie Allain John Singleton  
| writer         = Craig Brewer
| starring       = Terrence Howard Anthony Anderson Taryn Manning Taraji P. Henson DJ Qualls
| music          = Scott Bomar
| cinematography = Amy Vincent Billy Fox
| studio         = MTV Films New Deal Entertainment
| distributor    = Paramount Classics
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $2.8 million
| gross          = $23,563,727
}} independent drama Memphis hustler and pimp who faces his aspiration to become a Rapping|rapper. The movie is dedicated to Sun Records founder Sam Phillips.
 Best Actor.

==Plot== keyboard and reacquainting himself with an old friend from school, Key (Anthony Anderson), who has become a sound technician, DJay decides to try his hand at making hip hop songs.

Key and his sound-mixer friend Shelby (DJ Qualls) help DJay put together several "flow" songs in which he expresses the frustrations of a small-time hustler struggling to survive. DJay quickly proves to have a real talent for lyrics, and his first fixed-length song, done at the urging of these friends, appears to have a decent chance of becoming a hit and getting local radio play.

The group experiences many setbacks throughout the creative process. DJay must hustle those around him in order to procure proper equipment and recording time, and Keys relationship with his wife becomes strained. DJay throws out one of his prostitutes, Lexus, for ridiculing his art. DJays pregnant prostitute, Shug (Taraji P. Henson), joins in the creative process, singing Hook (music)|hooks, and the group eventually records several fixed-length tracks, including "Whoop That Trick" and their primary single "Its Hard Out Here for a Pimp". After their first recording, DJay begins to show a romantic interest in Shug.
 Chris "Ludacris" Bridges), a successful Memphis rapper, will be returning to the neighborhood for a Fourth of July party. DJay gains admittance to the party under the pretext of providing marijuana, with the intention of giving Skinny Black his demo tape. Black is dismissive at first, but after a long night of reminiscing DJay successfully persuades him into taking the tape.

Before leaving the party, however, DJay discovers that the drunken Black has destroyed his tape, leaving it in the toilet. When DJay confronts Skinny Black, Black laughs at the idea of touring with DJay and insults him. In a fit of rage, DJay begins beating Skinny Black. During the confrontation Skinny Black manages to break the chain around DJays neck, which was a gift from Shug.
 pistol whips him. DJay aims the gun at Black but suddenly comes to the realization of what he has done. While he attempts to resuscitate the unconscious Black, a member of Blacks crew enters the bathroom and quickly pulls out his gun, which results in DJay shooting him in the arm. DJay then resorts to using the wounded man as a human shield to make his escape.

DJay arrives home to find the police waiting for him. DJay turns himself in and tells Nola to keep his writing pad, with his rap lyrics. He tells her that "she is in charge" of getting his songs on local radio stations. While Shug tearfully watches DJay being led away in handcuffs, one of Blacks entourage (juicy j) takes advantage of his defenseless position to deliver a sucker punch. DJay  is charged for assault and possession of a firearm and is sentenced to 11 months in prison.
 DJs into playing his songs, which have become local hits. Key says he and Nola want to discuss his future plans.  The film ends as we see a duo of prison guards who have their own rap group asking DJay to listen to their demo, much as DJay had approached Skinny Black. Djay accepts their tape and responds with: "You know what they say, everybody gotta have a dream".

==Cast==
* Terrence Howard as DJay, a small-time hustler who is dissatisfied with his unglamorous life and learns to express himself. He is also Shugs love interest.
* Anthony Anderson as Key, a modestly successful audio technician with a passion for music.
* Taryn Manning as Nola, a levelheaded prostitute of DJays.
* Taraji P. Henson as Shug, a friendly prostitute of DJays who is pregnant with an unknown clients child. She is also DJays love interest.
* DJ Qualls as Shelby, a friend of Keys and an amateur musician.
* Ludacris as Skinny Black, a successful Memphis rapper who has forgotten his roots.
* Paula Jai Parker as Lexus, an arrogant and rude prostitute and part-time stripper with a young child.
* Elise Neal as Yevette, Keys strait-laced wife.
* Isaac Hayes as Arnel, a bar owner who is sympathetic to DJays aspirations.
* Juicy J as Tigga, a local rapper.
* Haystak as Mickey, the strip club DJ.

==Production==
  typecast as a "pimp" archetype. However, after recognizing the complexity and depth of the character, he reversed his earlier decision and took on the role.

As concepts of both hustle and flow are unique to African American culture, it turned out to be nearly impossible to find proper translations for international release of the film. For example, the Russian translation of the title means "The fuss and the torrent".

The film experienced many years of near-misses and outright rejection from major studios and potential financiers before finally being backed by its longtime supporter John Singleton. In the DVD extras Singleton says that he decided at last to put up the money himself because he was exasperated at his friends not getting what their film deserved.

==Critical reception==

The film received positive reviews overall. Rotten Tomatoes gives the film an 82% "fresh" rating.  Metacritic gives the film a rating of 68/100 based on 37 reviews. 
The Boston Globe said, "Some will find it chicly inspired, recalling blaxploitations heyday with its grimy urban realism. Some will rightly find it corny, absurd, and an insultingly limited presentation of options for the most disenfranchised African-Americans." 

According to Entertainment Weekly, "The home-studio recording sequences in Hustle & Flow are funky, rowdy, and indelible. Brewer gives us the pleasure of watching characters create music from the ground up." 

==Awards and nominations==
;Academy Awards
* Best Actor in a Leading Role: Terrence Howard (Nominated) Jordan Houston, Cedric Coleman, Paul Beauregard for "Its Hard Out Here for a Pimp" (Winner)

;Austin Film Critics Four Brothers, Get Rich Lackawanna Blues Their Eyes Were Watching God 

;Black Movie Awards
* Outstanding Actor in a Motion Picture: Terrence Howard (Winner)
* Outstanding Performance by an Actress in a Supporting Role: Taraji P. Henson (Winner)
* Outstanding Performance by an Actor in a Supporting Role: Anthony Anderson (Winner)
* Outstanding Motion Picture: (Nominated)

;Black Reel Awards
* Best Actor: Terrence Howard (Winner)
* Best Supporting Actress: Taraji P. Henson (Winner)
* Best Original Soundtrack: (Winner)
* Best Film: (Nominated)
* Best Supporting Actor: Anthony Anderson (Nominated)
* Best Ensemble: Nominated

;Broadcast Film Critics Association Awards
* Best Actor: Terrence Howard (Nominated)
* Best Song: Terrence Howard for "Hustle & Flow" (Winner)

;Chicago Film Critics
* Best Actor: Terrence Howard (Nominated)

;Florida Film Critics Get Rich or Die Tryin 

;Golden Globes
* Best Actor in a Drama Motion Picture: Terrence Howard (Nominated)

;Gotham Awards
* Breakthrough Award: Terrence Howard (Nominated)

;Image Awards
* Outstanding Motion Picture (Nominated)
* Outstanding Actor in a Motion Picture: Terrence Howard (Nominated)
* Outstanding Supporting Actress in a Motion Picture:
** Taraji P. Henson (Nominated)
** Elise Neal (Nominated)
* Outstanding Supporting Actor in a Motion Picture: Anthony Anderson (Nominated)

;MTV Movie Awards
* Best Breakthrough Performance: Taraji P. Henson (Nominated)
* Best Kiss: Terrence Howard and Taraji P. Henson (Nominated)
* Best Performance: Terrence Howard (Nominated)

;Screen Actors Guild Awards
*Outstanding Performance by a Cast in a Motion Picture

;Sundance Film Festival
* Audience Award, Dramatic: Craig Brewer (Winner)
* Excellence in Cinematography Award, Dramatic: Amelia Vincent (Winner)

;Teen Choice Awards Crash  Crash 
* Choice Summer Movie (Nominated)

==Soundtrack==
  Grand Hustle and Atlantic Records. The album centers on Southern hip hop.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 