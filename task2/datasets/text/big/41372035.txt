Echchil Iravugal
{{Infobox film
| name = Echchil Iravugal
| image = EchchilIravugal.JPG
| caption = LP Vinyl Records Cover
| director = Prof. A.S.Pragasam
| writer = Roopa Prathap Prathap Pothan Raveendran Vanitha Krishnachandran
| producer = 
| music = Illayaraja
| cinematography = Balu Mahendra
| editor =
| released = 1982
| runtime = Tamil
| budget =
}}
 1982 Cinema Indian feature directed by Prathap Pothan, Raveendran and Vanitha Krishnachandran in lead roles.   

==Cast==
 Roopa
*Prathap Prathap Pothan Raveendran
*Vanitha Krishnachandran

==Soundtrack ==
{{Infobox album Name     = Echchil Iravugal Type     = film Cover    = EchchAudio.jpg Released =  Artist   = Illayaraja Genre  Feature film soundtrack Length   = 24:48 Label    = Polydor
}}

The music composed by Ilaiyaraaja.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics 
|-
| 1 || Ezhla Melakku || S. P. Balasubrahmanyam|| Vairamuthu
|-
| 2 || Poothu Nikkudhu   || Jency Anthony, Malaysia Vasudevan || Kannadasan
|-
| 3 || Kadar Karayil || K. J. Yesudas & Chorus || Kannadasan
|-
| 4 || Poo Melae Veesum  || K. J. Yesudas, Vani Jairam || Vallapan
|}

==References==
 

==External links==

 
 
 
 
 


 