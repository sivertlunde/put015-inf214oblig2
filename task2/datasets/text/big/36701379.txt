Dwando
{{Infobox film
| name           = Dwando
| image          = Dwando poster.png
| image_size     = 200px
| border         = 
| alt            = Poster of the film
| caption        =  Suman Ghosh
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Soumitra Chatterjee Ananya Chatterjee
| music          = Mayookh Bhaumik
| cinematography = Barun Mukherjee
| editing        = Sujay Duttaroy
| studio         = Suman Ghosh Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film. Suman Ghosh.       According to Telegraph review, the film is inspired by Krzysztof Kieslowski’s Decalogue II.    This was Suman Ghoshs second film after Podokkhep.   

== Theme ==
The film which the director has called a "serious film" is a psychological drama that deals with dilemma of a woman. In directors own words–I have always felt that a film should be something more than just a slice of life. In Ananya’s ethical dilemma we get the larger picture of how we can mould our behavioural pattern when some extraordinary events happen. 

== Plot ==
Sudipta is married for 10 years to Anik but the couple dont have any child and the marriage has turned cold. When Anik is away for an office trip, Sudipta gets attracted towards a young NRI Rana. Anik comes back and suspects something wrong. 
It is found Anik is suffering from Brain Tumour and he is admitted to hospital for treatment under surgeon Dr Ashok Mukherjee. 
Anik’s physical condition gets worse. At this time Sudipta discovers she is pregnant. Sudipta, on one hand loves her husband Anik and on the other hand loves Rana and wants to give birth to the child, faces a moral dilemma. 
At a stormy night Sudipta goes to Dr. Ashok Mukherjee to know the health condition (chances of living) of Anik on which her decision will depend. 
Her dilemma is ultimately resolved by Dr. Mukherjee letting love triumph over everything else. 

== Credits ==

=== Cast ===
* Soumitra Chatterjee as Dr. Mukherjee
* Ananya Chatterjee as Sudipta
* Kaushik Sen as Anik
* Samrat Chakrabarti as Rana

=== Crew ===
* Director: Suman Ghosh
* Production: Suman Ghosh Productions
* Cinematographer: Barun Mukherjee
* Music: Mayookh Bhaumik
* Chief assistant director: Avijit Chaudhuri
* Sound designer: Partha Barman
* Art director: Tanmoy Chakraborty
* Editor: Sujay Duttaroy
* Costumes and Production stylist: Anandi Ghosh
* Production Controller: Saubhik Das

== Soundtrack ==
{{Track listing
|extra_column=Singer(s)
|lyrics_credits=no

| all_writing     = 
| all_lyrics      = 
| all_music       = Mayookh Bhaumik 

| writing_credits = 
| lyrics_credits  = 
| music_credits   =

| title1          = Teri Surat
| note1           = 
| writer1         = 
| lyrics1         = 
| music1          = 
| extra1          = Joy D and Tritha

| title2          = Bare Bare
| note2           = 
| writer2         = 
| lyrics2         = 
| music2          = 
| extra2          = Rupam

| title3          = Ashru Bhora
| note3           = 
| writer3         = 
| lyrics3         = 
| music3          = 
| extra3          = Rezwana

| title4          = Bare Bare
| extra4          = Tirtha
}}

==See also ==
* Waarish
* Nobel Chor

== References ==
 

== External links ==
*  
*  

 
 