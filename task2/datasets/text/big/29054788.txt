West Is West (2010 film)
{{Infobox film
| name           = West Is West!
| image          = West Is West.jpg
| caption        =Canadian release poster
| director       = Andy DeEmmony
| producer       = Leslee Udwin
| writer         = Ayub Khan-Din
| starring       = Aqib Khan Om Puri Linda Bassett Robert Pugh Raj Bhansali
| music          = Rob Lane Shankar Ehsaan Loy
| cinematography = Peter Robertson Stephen OConnell
| studio         = BBC Films Assassin Films
| distributor    =
| released       =  
| runtime        = 103 minutes
| country        = United Kingdom
| language       = English
| budget         = £2.5 million 
| gross          = £4.9 million   }} British comedy-drama East Is East. It stars Aqib Khan, Om Puri, Linda Bassett, Ila Arun and Jimi Mistry, is written by Ayub Khan-Din, directed by Andy DeEmmony, and produced by Leslee Udwin for Assassin Films and BBC Films.
 Salford family is being planned.  , retrieved 2010-10-24  The first US showing was on 2 November 2010 at the South Asian International Film Festival, followed by several other US festivals. While it was released in Canada on 25 March 2011, it never received a US release.

==Plot== Paki bastard," George decides to take him to Pakistan to meet his extended family and show him that life there is better, though Ella openly disapproves.

On arriving in Pakistan, George and Sajid are greeted by a few of Georges relatives including Tavnir, a lazy man who often tries to swindle George—and is his son-in-law through his marriage to one of Georges daughters with his first wife Basheera. On the family farm, George is reunited with Basheera and their daughters, whom he had abandoned thirty years before, and hands out gifts to all, then tells them that he has come for a month to find Maneer a wife, but he soon discovers that no family will offer their daughter in marriage as they fear that Maneer will leave his wife for an English woman as his father George did when he left Basheera for Ella. Furious, George blames his family in England, but Maneer reminds him that he himself is at fault, and Basheera is also angry with George for abandoning her when she needed him.

When Tavnir explains to Sajid that he will not tolerate any trouble, he is abruptly told to "fuck off." Sajid is taken to the local school for enrolment, where he meets spiritual teacher Pir Naseem and local boy Zaid who Sajid loathes at first and refuses to enrol. Zaid, who can speak basic English language|English, advises him, and the two soon become friends. Zaid teaches him Pakistani ways and Pir Naseem promises George that he will discipline his son when he misbehaves. Sajid gradually appreciates his culture and new surroundings which pleases George, except that he is slightly jealous of the bond that develops between his son and Pir Naseem. Eventually Sajid meets Rochdale-born Pakistani woman Neelam, who bears a striking resemblance to Maneers favourite singer Nana Mouskouri; like Maneer she is also looking for a spouse, and with her approval Sajid plans a meeting between the pair.

Meanwhile Ella, who soon discovers that George has withdrawn the family savings, travels to Pakistan with her best friend Annie in tow, and is furious when she finds that her husband is building a house for his family. She also plans to take Sajid back to England with her, and is shocked when her son refuses to leave. During her stay there, Ella fights with Basheera and her daughters, and refuses to give them access to the new house, but upon realising how alike they are the two put their differences aside. Maneer and Neelam soon marry, and George for the first time in years begins to appreciate Ella as a wife who stood by him during hard times. The film ends with George and his England-based family returning home and Sajid finally proud of his Asian background, whilst Georges chippy now serves Pakistani-style kebabs.

==Cast==
* Om Puri as George
* Aqib Khan as Sajid
* Linda Bassett as Ella Lesley Nicol as Annie
* Jimi Mistry as Tariq
* Emil Marwa as Maneer
* Vijay Raaz as Tanvir
* Vanessa Hehir as Esther
* Robert Pugh as Headmaster Jordan
* Ila Arun as Basheera
* Zita Sattar as Neelam
* Nadim Sawalha as Pir Naseem
* Raj Bhansali as Zaid

Om Puri, Linda Bassett, Leslie Nicol, Jimi Mistry, and Emil Marwa are the only actors from the original movie who reprise their roles in West is West. Zita Sattar who plays Neelam was Meena, George and Ellas only daughter, in the original stage production of East is East. 

==Soundtrack==
{{Infobox album
| Name = West is West
| Type = Soundtrack
| Artist = Shankar–Ehsaan–Loy
| Cover = 
| Released =   Feature film soundtrack
| Label = Decca Records
| Producer = Universal Music Group
| Reviews = Patiala House  (2011)
| This album = West is West  (2011)
| Next album = Game (2011 film)|Game  (2011)
}}
The original score and songs for the film are composed by Shankar–Ehsaan–Loy  and arranged by Rob Lane.  The songs were recorded in Purple Rain studio, in Mumbai. Rob Lane joined Shankar–Ehsaan–Loy to compose music for the film. They used several Indian classical instruments like Jaltarang, Santoor, Flute  and Sarangi. The renowned Jaltarang player Milind Tulnkar was roped in to play the instrument for ten to twelve sequences. 

{{tracklist
| headline        = Tracks
| extra_column    = Artist(s)
| music_credits   = yes
| title1          = Alright Alright Alright
| extra1          = Mungo Jerry
| music1          = Jacques Dutronc Jacques Lanzmann Joe Strange
| length1         = 2:47
| title2          = O Meri Maina
| extra2          = Manna Dey, Usha Mangeshkar
| music2          = Shankar-Jaikishan
| length2         = 4:03
| title3          = Arriving in Pakistan
| extra3          = 
| music3          = Shankar–Ehsaan–Loy, Rob Lane
| length3         = 2:08
| title4          = George Ploughs The Field
| extra4          = 
| music4          = Shankar–Ehsaan–Loy, Rob Lane
| length4         = 1:58
| title5          = Over And Over
| extra5          = Nana Mouskouri
| music5          = Nana Mouskouri
| length5         = 2:58
| title6          = Piya Tu Ab To Aaja
| extra6          = Asha Bhosle
| music6          = R.D. Burman
| length6         = 5:21
| title7          = Checking Out The Girls
| extra7          = 
| music7          = Shankar–Ehsaan–Loy, Rob Lane
| length7         = 2:00
| title8          =  Kaala Doria
| extra8          = Sanjeev Kumar, Shankar Mahadevan
| music8          = Shankar–Ehsaan–Loy
| length8         = 2:21
| title9          = Mere Sayaan
| extra9          = Shankar Mahadevan
| music9          = Shankar–Ehsaan–Loy
| length9         = 2:17
| title10         = Toomba
| extra10         = Sain Zahoor
| music10         = Sain Zahoor (Coke Studio)
| length10        = 2:15
| title11         = Rooftop Chase
| extra11         = 
| music11         = Shankar–Ehsaan–Loy, Rob Lane
| length11        = 2:21
| title12         = Basheeras Pain
| extra12         = 
| music12         = Shankar–Ehsaan–Loy, Rob Lane
| length12        = 3:38
| title13         = Main Ho Gaee Dildar Ki
| extra13         = Nahid Akhtar
| music13         = Kamal Ahmad
| length13        = 5:06
| title14         = Mellow Yellow
| extra14         = Donovan
| music14         = Donovan
| length14        = 3:38
| title15         = Numaishaan Mohobbatan Di
| extra15         = Sain Zahoor
| music15         = Shankar–Ehsaan–Loy
| length15        = 1:06
| title16         = Waterfight
| extra16         = 
| music16         = Shankar–Ehsaan–Loy, Rob Lane
| length16        = 3:07
| title17         = The Final Farewell
| extra17         = 
| music17         = Shankar–Ehsaan–Loy, Rob Lane
| length17        = 3:25
| title18         = Aik Alif
| extra18         = Noori
| music18         = Sain Zahoor (Coke Studio)
| length18        = 3:07
}}

==Critical Reception==
The film received mixed reviews, scoring 68% on Rotten Tomatoes.  Several reviewers noted that while the film still had the sense of humour of East is East, it did not reach the level attained in the original.   In his review for the Globe and Mail, Rick Groen noted the "sudden leaps into unabashed melodrama", saying that "When they fail, the hurlyburly gets annoying. But when they succeed, the result can be genuinely touching."   
 Daily Bhaskar review described the music as "soothing" and "situational".   

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 