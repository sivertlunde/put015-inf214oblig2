It Was Always So Nice With You
{{Infobox film
| name = It Was Always So Nice With You
| image =
| image_size =
| caption = Hans Wolff
| producer =  
| writer =  Hans Rameau    Hans Wolff
| narrator =
| starring = Heinz Drache   Georg Thomalla   Ingrid Stenn   Grethe Weiser
| music = Georg Haentzschel  
| cinematography = Hans Schneeberger 
| editing =  Hermann Leitner       
| studio = Fono Film  
| distributor = Allianz Filmverleih
| released = 16 March 1954 
| runtime = 90 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Hans Wolff and starring Heinz Drache, Georg Thomalla and Ingrid Stenn.  The Swedish actress Zarah Leander who had been the leading German film star during the Nazi era, plays a self-referential role as a maturing film star. The film was intended as a tribute to the composer  Theo Mackeben with many of his hit songs sung by Leander and Kirsten Heiberg, another star of the 1940s.

==Cast==
*  Heinz Drache as Peter Martens  
* Georg Thomalla as Karl Holler  
* Ingrid Stenn as Elisabeth  
* Grethe Weiser as Aunt Martha  
* Albrecht Schoenhals as Publisher Conrads  
* Carsta Löck as Secretary  
* Willy Maertens as Father Hannemann  
* Helmuth Rudolph as 2nd Film Director 
* Robert Kersten as Filmstar  
* Willi Forst as Film Director  
* Kirsten Heiberg as Cabarre Singer  
* Margot Hielscher as Revue Star 
* Zarah Leander as Filmstar  
* Sonja Ziemann as Ballett Dancer  
* Grete Sellier as Dancer  
* Willy Dirtl as Dancer  
* Erwin Bredow as Dancer  
* Arthur Bankmann as Dancer 
* Helmut Ketels as Dancer  
* I.P. Schaich as Dancer  
* Klaus Zimmermann as Dancer 
* Eddi Arent as Peters Begleiter in Hafenbar

== References ==
 

== Bibliography ==
* Anne Commire & Deborah Klezmer. Women in World History: A Biographical Encyclopedia, Volume 9. Yorkin Publications, 1999. 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 