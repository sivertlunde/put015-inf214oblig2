Hip-Hop: Beyond Beats and Rhymes
{{Infobox Film name           = Hip-Hop: Beyond Beats and Rhymes image          = File:Beyond Beats and Rhymes.jpg caption        =  director       = Byron Hurt producer       = Byron Hurt and Sabrina Schmidt Gordon writer         = Byron Hurt starring       = {{Plainlist|
*Busta Rhymes 
*Chuck D
*Clipse
*Doug E. Fresh
*Fat Joe
*Jadakiss
*M-1 (rapper)|M-1
*Mos Def
*Talib Kweli
}} cinematography = Bill Winters editing        = Sabrina Schmidt Gordon distributor    =  released       =  : February 20, 2007 runtime        = 56 minutes country        = United States language       = English budget         = 
}} PBS Emmy|Emmy-winning documentary series, Independent Lens.

==Interviews== Sarah Jones are also interviewed.

==Noted moments==
Of the one hour documentary, media outlets largely focused on three specific interviews:
 Tip Drill," in which he is seen sliding a credit card down the back side of a woman. In response, the rapper canceled plans to hold the bone marrow drive at the school.  

An interview with Rapper Busta Rhymes in which the rapper walked out when confronted with a question about homophobia in the rap community. Rhymes is quoted as saying: "I cant partake in that conversation," followed by, "With all due respect, I aint trying to offend nobody. . . What I represent culturally doesnt condone   whatsoever." When asked if the hip-hop culture would ever accept a homosexual rapper, Busta Rhymes then exited the interview. 

To reveal the effect of the commodification of women in hip-hop, Hurt interviewed concertgoers at the BET Spring Fling in Daytona, FL. Hurt was appalled by the actions of black youth at the concert, who were indiscreetly touching and taking videos of women. One young man that was interviewed commented that "Look how they dress” to justify the actions of the men at the event. In this segment of the documentary, Byron claims that the objectification of women in hip-hop lyrics and music videos has taught young men to view women as sex objects for their own personal pleasure. 

Many media outlets focused on the interview with activist and rapper,   is the cancer of black manhood in the world, because they have one-dimensionalized and commodified us into being a one-trick image. Were   throwing money at the camera and flashing jewelry at the camera that could give a town in Africa water for a year."  The rapper also stated a link existed between the sales of hip-hop music to young white Americans, and the amount of pressure on black artists to create more of that content: sex and violence.     

==References==
 

==External links== PBS 
*  
*  

 
 
 
 
 