The Eternal City (1923 film)
:For the 1915 film version of the Hall Caine novel, see The Eternal City (1915 film).
{{infobox film
| name           = The Eternal City
| image          = The Eternal City - 1923 poster.jpg
| caption        = 1923 theatrical poster
| director       = George Fitzmaurice
| producer       = Samuel Goldwyn
| based on       =  
| writer         = Ouida Bergere (scenario)
| starring       = Lionel Barrymore Bert Lytell Barbara La Marr
| music          =
| cinematography = Arthur C. Miller
| editing        =
| studio         = Samuel Goldwyn Productions Associated First National
| released       =   reels 7,929 feet
| country        = United States Silent English intertitles
}}
The Eternal City (1923) is a silent film directed by George Fitzmaurice, from a script by Ouida Bergère based on a Hall Caine novel, starring Barbara La Marr, Lionel Barrymore and Bert Lytell.
 Associated First The Eternal City (1915) starring Pauline Frederick. This film is the second filming of the 1902 play starring Viola Allen which was also based on Caines novel.  

Fitzmaurice was able to film King Victor Emmanuel III and Benito Mussolini reviewing Italian troops. This film is now a lost film. 

==Cast==
*Barbara La Marr - Donna Roma
*Bert Lytell - David Rossi
*Lionel Barrymore - Baron Bonelli Richard Bennett - Bruno
*Montagu Love - Minghelli
*Ronald Colman -
*Betty Bronson - page boy
*Joan Bennett - page boy
*Benito Mussolini - himself
*King Victor Emmanuel - himself

==See also==
*Lionel Barrymore filmography
*List of lost films

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 