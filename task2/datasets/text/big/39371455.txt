Red Scorpion 2
{{Infobox film
| name         = Red Scorpion 2
| image        = Red Scorpion 2 American VHS Cover 1994.jpeg
| image_size   = 
| caption      = American VHS cover of "Red Scorpion 2".
| genre        = Action
| runtime      = 95&nbsp;minutes
| creator      =  Michael Kennedy
| producer     = Robert K. MacLean Jack Abramoff Robert Abramoff Dale A. Andrews George G. Braunstein Mary Eilts Michele White
| writer       = Troy Bolotnick Barry Victor Straw Weisman Arne Olsen (characters) John Savage Jennifer Rubin
| editing      = Gary Zubeck
| music        = George Blondheim
| studio       = Abramoff Production Shapiro-Glickenhaus Entertainment
| distributor  = Home Box Office (HBO) (USA TV) MCA/Universal Home Video (USA USA VHS/laserdisc)
| budget       = $8,000,000 
| country      = United States Canada
| language     = English
| network      = 
| released     = May 1994 (France)   (America)
}} John Savage Jennifer Rubin. Michael Kennedy. The film is a sequel to the 1988 film Red Scorpion, starring Dolph Lundgren, although the story is largely unrelated to the first installment.

==Background== Duncan Fraser as Mr. Benjamin, George Touliatos as Gregori and Michael Ironside as Col. West.  Marc Singer was the original choice for the role of the films nemesis Andrew Kendrick, whilst Gary Daniels and David Bradley were considered for the role of leading hero character Nick Stone.

The first version of the screenplay for this film was written by Dennis Hackin. A second different version, set in Malaysia, was written by Clay Walker in 1990. Neither of these screenplays were used. http://www.imdb.com/title/tt0110954/trivia?ref_=tt_trv_trv  The version set in Malaysia was seemingly due to go ahead and in the New Straits Times of June 18, 1992, it was reported that the script was being written by Vigil and was scheduled to be shot in Europe in November.  In the same issue it had confusingly stated that McColm was "in Malaysia to promote his latest movie, Red Scorpion 2: The Spear of Destiny. In this movie, McColm takes the lead as Kurt Hawkins, a US Special Forces staff sergeant." 

The film had a working title of Red Scorpion 2: Spear of Destiny, which was used on the films laserdisc release exclusively. http://www.imdb.com/title/tt0110954/releaseinfo?ref_=tt_dt_dt  It was filmed in Britannia Beach, British Columbia, Canada, and Vancouver, British Columbia, Canada.  The two producers Jack and Robert Abramoff of Abramoff Production, had written the original Red Scorpion film, whilst Jack Abramoff would also produce it. 

The film first aired on the Home Box Office (HBO) network on April 27, 1995, before being distributed by MCA/Universal Home Video on VHS/laserdisc on February 6, 1996. http://www.imdb.com/title/tt0110954/companycredits?ref_=tt_dt_co  The film was released theatrically in Japan, India and Italy.  The film debuted in France during May 1994 at the Cannes Film Market, before being released within Japan, USA and South Korea respectively during 1995, and later in Italy during 1997. Like in the USA, the video release of the film in France was also in 1996. 

Red Scorpion 2 has never been released on DVD in America, but only on VHS and laserdisc. In the UK, the film was released on 1 October 1999 via Cinema Club on VHS.  Additionally, Cinema Club released a double feature, which consisted of both films Red Scorpion and Red Scorpion 2.  In 2005, a DVD version was released in Italy, under the title Missione Senza Nome, which translates to Mission Nameless.  In Canada, it was released in 2000 via Alliance Atlantis Video, in Greece during 1997 via New Star/Telefilm Hellas.  Although most VHS releases had similar artwork to one another, the laserdisc version featured completely different artwork. 

==Plot==
Uneducated, disaffected white youths known as skinheads are attacking racial minorities in the US. A government agent investigating the incidents traces them to a wealthy, extreme right-wing, neo-Nazi businessman who is secretly bankrolling the skinheads to carry out his wishes, allowing him to keep his hands clean as he preaches a non-violent message to his wide following.    Nick Stone (McColm) is an agent with the National Security Agency who wants to retire after his last job going undercover as a weapons dealer resulted in the death of a fellow agent. He meets up with his boss Col. West to tell him about his retirement, but West offers him one last mission. To take down Andrew Kendrick (Savage), a charismatic and dangerous white supremacist and his Neo-Nazi militia group hellbent on taking over the United States with his sleeper cells. Nick often works alone but West tells him that he will be working with a team composed of Commander Sam Guinness (Rubin), Vince DAngelo (Ben Paul-Victor), a womanizing computer expert from Hollywood, Billy Ryan (Michael Covert), a redneck sharpshooter, Joe Nakamura and Winston Mad Dog Powell, (Real Andrews), a disgraced Detroit cop. Forming an elite group of soldiers, each with their own set of skills, the groups first assignment is to infiltrate a warehouse owned by Kendrick which may be housing "The Spear of Destiny", an ancient spear used in the crucifixion of Jesus and believed by Kendrick to give him ultimate power in his goal. The team storm the warehouse. The team however dont work together despite constant orders from Commander Guinness which results in Nakamuras death and the spear not being recovered. Despite the failed mission, Col. Wests boss Colonel Gregori proposes to take the team to his cabin in the pacific Northwest. Under his guidance and training, the team become better and Gregori names them The Red Scorpions.

A second infiltration is planned in which Winston enters one of Kendricks gatherings and Ryan, who establishes himself there as a violent racist redneck from the south. Impressed, Kendrick offers Ryan a chance to work for him. Billy is taken to the groups own heavily guarded camp where Kendrick is training an army. However, after he sleeps with Kendricks female assistant Donna (Kaiser), the game is up and Ryan gets thrown in a dungeon. Stone and Guinness then infiltrate the camp, who are about to launch a nationwide campaign of violent action unless they are stopped in time. The pair pose as a couple interested in investing within the group, however they are soon discovered, although they manage to successfully lead their team of high specialized assassins and technicians on the raid of the covert American hate group. Stone is later tortured by Hans (Kulich) who later fights him in a battle to the death. Guinness manages to kill Donna and help Winston and DAngelo infiltrate the camp and in the chaos, the team take out the Neo-Nazis. Kendick dies when his lair where he planned to launch his sleepers explodes and he slowly burns to death. The team are then picked up by helicopter after killing Hans whom they viciously gun. In the films closing scene, DAngelo, noticing that the pilot is female, compliments her on what lovely eyes she has.    

==Cast==
* Matt McColm as Nick Stone John Savage as Andrew Kendrick Jennifer Rubin as Sam Guinness
* Paul Ben-Victor as Vince DAngelo
* Michael Covert as Billy Ryan
* Réal Andrews as Winston Mad Dog Powell Duncan Fraser as Mr. Benjamin
* George Touliatos as Gregori
* Michael Ironside as Col. West
* Vladimir Kulich as Hans
* Suki Kaiser as Donna
* Jerry Wasserman as Steinberg
* Tong Lung as Joe
* Anthony Stamboulieh as Abdu
* Samantha Schubert as Renee
* Gabriel Hogan as uncredited
* Rich Priske as Bad guy (uncredited)

==Reception==
Iotis Erlewine of Allmovie gave the film one out of five stars. 

In the 2004 book DVD & Video Guide 2005, published by Ballantine Books and written by Mick Martin and Marsha Porter, the authors stated "The action scenes are well-handled."  In the 2006 book VideoHounds Golden Movie Retriever, published by and written by Jim Craddock, the author stated "Another "Dirty Dozen" rip-off finds your average ethnically mixed good guys brought together by a government agency to be heroic." 

On February 02, 1996, Los Angeles Times staff writer Susan King spoke of the film, calling it a "glum thriller". 

==References==
 

==External links==
*  

 
 
 
 
 

 