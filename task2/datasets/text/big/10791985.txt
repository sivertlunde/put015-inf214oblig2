Garjanai
{{Infobox film|
| name = Garjanai
| image = Garjanai.jpg
| caption = Poster
| director = C. V. Rajendran 
| writer = Panju Arunachalam Madhavi Geetha Geetha Jaishankar M. N. Nambiar Major Sundarrajan Jayamalini
| producer = Ram Kishen
| music = Ilaiyaraaja
| cinematography = Jayanan Vincent
| editing = R. G. Gopu
| studio = Hem Nag Films
| distributor = Hem Nag Films
| released = 6 August 1981
| runtime =
| country = India Tamil
| budget =
}}

Garjanai is a 1981 Tamil film is directed by C. V. Rajendran. It has Madhavi as Rajinis love interest and Geetha as his sister. Music was scored by Ilaiyaraaja. The film was first made in Malayalam, with Jayan. After his death during the filming of Kolilakkam, the film was re-shot with Rajinikanth; simultaneously in Malayalam and Tamil owing to Rajins huge fan base in South India.  The movie did not fare well at the box office and was a failure.

==Cast==
*Rajinikanth Madhavi
*Geetha Geetha
*Jaishankar
*M. N. Nambiar
*Major Sundarrajan
*Jayamalini
*Thengai Srinivasan
*V. K. Ramasamy (actor)|V. K. Ramasamy

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Enna Sugamana || Malaysia Vasudevan, Uma Ramanan || Panju Arunachalam || 4:41
|- Kannadasan || 4:27
|- Vani Jayaram || 4:09
|-
| 4 || Vanthathu Nallathu || S. P. Balasubrahmanyam, S. Janaki || 4:46
|-
| 5 || Varuvai Anbe || T. K. S. Kalaivanan, S. Janaki || Panju Arunachalam || 5:04
|}

==References==
 

 

 
 
 
 
 
 


 