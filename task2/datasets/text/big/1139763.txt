The Hunger (1983 film)
 
 
 
{{Infobox film
| name = The Hunger
| image = The Hunger film poster.jpg
| image_size = 215px
| alt =
| caption = Theatrical release poster
| director = Tony Scott Richard Shepherd
| screenplay = Ivan Davis
| based on =  
| starring = {{plainlist|
* Catherine Deneuve
* David Bowie
* Susan Sarandon
* Cliff De Young
}}
| music = Denny Jaeger Michel Rubini
| cinematography = Stephen Goldblatt
| editing = Pamela Power
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 96 minutes
| country = United Kingdom
| language = English
| budget =
| gross = $10,190,512
}} eponymous novel by Whitley Strieber, with a screenplay by Ivan Davis and Michael Thomas.

The film was screened, out of competition at the 1983 Cannes Film Festival.   

==Plot== immortal vampire, promising specially chosen humans eternal life as her vampire lovers. As the film begins, her vampire companion is John (David Bowie), a talented cellist whom she married in 18th century France. The film opens in a night club in New York, to a live performance from Bauhaus (band)|Bauhaus. There they connect with another young couple who are brought home and fed upon by slashing their throats with a bladed Ankh pendant. The bodies are disposed of by an incinerator in the basement of John and Miriams elegant New York townhouse, where they pose as a wealthy couple who teach classical music. The only student shown is a young tomboy violinist named Alice Cavender (Beth Ehlers).

Periodically killing and feeding upon human victims, allows Miriam and John to possess eternal youth or what the latter was led to believe. Approximately 200 years after his turning, John begins suffering insomnia and ages rapidly in only a few days. Realizing that Miriam knew that this would happen and that her promise of "forever and ever" was only partially true: he will have eternal life but not eternal youth. Feeling betrayed, he seeks out the help of Dr. Sarah Roberts (Susan Sarandon), a gerontologist, alongside her boyfriend Tom (Cliff De Young), who specialises in studying the effects of rapid ageing in primates, hoping she will be able to help reverse his accelerating decrepitude. Sarah assumes that John is a hypochondriac or mentally unbalanced and ignores his pleas for help. As John leaves the clinic in a rage, Sarah is horrified to see how rapidly he is ageing. John rebuffs her once she tries to help him.

After returning from the clinic, Alice drops by unexpectedly to say that she cannot attend the next days lesson. While inside, she believes John to be his own father due to the accelerated ageing. In a last attempt to regain his youth, John murders and feeds upon Alice, whom Miriam was grooming to be her next consort when she came of age, to no avail. As Johns ageing advances, he begs Miriam to kill him and release him from the agony of his decrepit body. Weeping, Miriam tells him that there is no release. After John collapses in the basement, Miriam carries him into the attic full of coffins and places him in one, then asks the surrounding ones to "be kind to him tonight." Like John now, Miriams former vampire lovers are doomed to suffer an eternal living death, helplessly moaning and trapped in their coffins. Later, a police official comes to the Blaylock residence, looking for the missing Alice. Miriam feigns ignorance and claims that her husband is in Switzerland.

Sarah comes looking for John at his home but only finds Miriam. With the two women feeling an attraction towards each other, Miriam acts upon this as she now feels alone after losing her lover and the young girl she was grooming. (In a memorable scene during a piano adaptation of The Flower Duet, Sarah says: "Are you making a pass at me, Mrs. Blaylock?" Miriam softly replies: "Miriam, please.") The two have a sexual encounter during which, without Sarah being fully aware of it, Miriam bites her arm and a blood exchange occurs in which some of Miriams blood enters Sarahs body. Miriam attempts to initiate Sarah in the necessities of life as a vampire but Sarah is repulsed by the thought of subsisting on human blood.

Sarah returns home and goes out to dinner with Tom, who becomes argumentative about her 3 hour disappearance at the Blaylock residence, of which she is strangely quiet. The next day at the lab, the team investigates Sarahs blood by Toms authority and reveal she has some kind of infection that is taking over. Confused, Sarah returns to the Blaylock residence to confront Miriam about her sudden changes. Still reeling from the effects of her vampiric transformation, Sarah allows Miriam to put her to bed in a guest room.

Shortly afterwards, Tom arrives on Miriams doorstep, trying to find Sarah. Miriam informs him that Sarah is in the upstairs bedroom. Sarah, starving and desperate, tries to resist the urge to kill Tom but gives in to temptation, when Tom refuses to leave her side. Sarah then joins Miriam by the piano and Miriam assures her that she will soon forget what she was and come to love Miriam. As the two kiss, Sarah drives Miriams ankh-knife into her own throat, attempting to kill herself as she forcibly holds her mouth over Miriams mouth, forcing Miriam to ingest her blood, possibly working on a hunch regarding the "blood borne metabolic ageing disease" and "host" relationship she was told about affecting her blood. Miriam carries Sarah upstairs, intending to place her with her other boxed lovers. A rumbling occurs and the mummies of Miriams previous lovers emerge from their coffins, driving her over the edge of the balcony. As she rapidly ages, the mummies fall and become dust, ostensibly providing the trapped souls with release.
 the Barbican), in the company of an attractive young man and woman and is serenely admiring the gorgeous view as dusk falls. From a draped coffin in a storage room, Miriam repeatedly screams Sarahs name (an overlay of the audio from earlier in the film).

===Rewritten ending===
The final scene of Sarah on the balcony was added at the studios behest, with a view to leaving the film open-ended and allowing for possible sequels.  , DVD audio commentary, 2004, Warner Bros. 

==Cast==
* Catherine Deneuve as Miriam Blaylock
* David Bowie as John Blaylock
* Susan Sarandon as Dr. Sarah Roberts
* Cliff De Young as Tom Haver
* Beth Ehlers as Alice Cavender
* Dan Hedaya as Lieutenant Allegrezza
* Suzanne Bertish as Phyllis James Aubrey as Ron
* John Stephen Hill as Young disco man
* Ann Magnuson as Young disco woman
* Shane Rimmer as Arthur Jelinek
* Bessie Love as Lillybelle
* John Pankow as 1st phone booth youth
* Willem Dafoe as 2nd phone booth youth Bauhaus as Disco Group

==Music== Flash Gordon Queen and The Snowman (1982).
 Piano Trio in E flat. Ralph recorded the Gigue from Bachs Violin Partita in E and Raphael the Prelude to Bachs solo cello sonata in G, to which Bowie mimed. I was persuaded to appear in one scene as a pianist, for which I wrote a Dolphin Square Blues. Tony wanted to add a synthesizer score and I introduced him to Hans Zimmer, then working at The Snake Ranch Studio in Fulham but Tony eventually used a score by Michel Rubini and Denny Jaeger with electronics by David Lawson. It is hard however to exactly separate these elements." 

===Movements===
# (On camera) "Bela Lugosis Dead" – Bauhaus, specially arranged, recorded and produced for the film by Howard Blake: Opening club scene shot in Londons Heaven.
# Score (David Lawson) – The first killing.
# Excerpt Piano Trio in E flat major by Franz Schubert (1797–1828), 2nd movement arranged by Howard Blake, played by Ralph Holmes, Raphael Wallfisch, and Howard Shelley – After the first killing, cut to ext. New York. (The Schubert first movement is slightly arranged in each case in one way because at the request of the director the trills are not played – Director Scott felt they gave it an unnecessarily period connotation).
# (On camera) Excerpt Piano Trio in E flat major by Franz Schubert, 2nd movement, Andante con moto. Excerpt arranged by Blake, played by Holmes, Wallfisch, and Shelley: Miriam and John in late 18th century costume mime to the track. J S Bach (1685–1750) played by Wallfisch: Continuation of the 18th century scene. Piano Trio in B flat major by Schubert, 2nd movement arr. by Howard Blake, played by Holmes, Wallfisch, and Shelley: Alice, the young violin pupil, John and Miriam mime to the track; John starts to age.
# Excerpt Sonata for piano in B flat major D960 slow movement, arranged and played by Howard Blake: The ageing Bowie returns to Miriams house.
# Score (Michel Rubini/Denny Jaeger with electronics by David Lawson): Ageing ape scene.
# Score (Lawson): Changing room scene.
# Excerpt from Funtime – Iggy Pop, David Bowie (publishers: James Osterberg Music/Bewlay Bros SARL/Fleur Music): Roller-skating scene.
# Lalo (1823–1892) excerpt from Romance from piano trio in C minor 0p.7 – Play the Lalo for me Alice plays an excerpt of the solo violin part (unaccompanied).
# Excerpt Sonata for piano in B flat major D960 slow movement, arranged and played by Howard Blake: John now really old and Miriam in the music room.
# "Miserere" – Gregorio Allegri (1582–1652): A recording of the famous papal piece for high treble and chorus: Used throughout the attic death scene.
# Score (Rubini/Jaeger with electronics by Lawson): The detective arrives.
# "Le Gibet" – Maurice Ravel (1875–1937): This piano piece starts with Miriam in bed with her new partner and later she is seen miming to it. Credited on the film only as published by Durand it comes from the piano suite Gaspard de la nuit by Ravel and conjures up the image of a man hanging from a gallows to the sound of a tolling bell. A recording was made at Advision Studios by Howard Blake (piano)16 September 1982.
# "Lakmé" by   is the opera of Leo Delibes, 1836–1891). Use of this song was suggested to Scott by Blake, who himself improvised the piano track mimed by Miriam. As the seduction unfolds the improvisation reaches a cadence and merges seamlessly into an authentic rendering of the operatic duet sung by Elaine Barry and Judith Rees (sopranos) with The Sinfonia of London conducted by HB, recorded at CTS Studios Wembley 18 October 1983. (This recording was later used as a section of underscore in the Tony Scott/Quentin Tarantino film True Romance, 1993).
# "Dolphin Square Blues" (Blake): Miriam is having lunch at Dolphin Square Restaurant with her new lover. In the background, HB can be seen and heard playing his own blues on piano. Below two (sometimes) naked girls swim and dive in the pool.
# Score (Lawson) – Sara coughing.
# Score (Rubini/Jaeger with electronics by Lawson): Sara picks up a male victim.
# Score (Rubini/Jaeger with electronics by Lawson): Tom, friend of Sara, arrives at the house.
# "Le Gibet" (Ravel): Miriam is again seen miming to this piece. Credited on the film only as published by Durand it comes from the piano suite Gaspard de la nuit by Ravel and conjures up the image of a man hanging from a gallows to the sound of a tolling bell. A recording was made at Advision Studios by Howard Blake (piano) 16 September 1982.
# Score (Rubini/Jaeger with electronics by Lawson): A killing.
# Score (Rubini/Jaeger with electronics by Lawson): The denouement in the attic.
# Same excerpt (see above) from Schuberts Piano Trio in E flat major, arranged by Blake, played by Holmes, Wallfisch, and Shelley: Final sequence—the selling of the house; Sara taking over the role of Miriam, looking across London. The music continues through the credits.

==Reception==
Bowie was excited to work on the film but was concerned about the final product. He said "I must say, theres nothing that looks like it on the market. But Im a bit worried that its just perversely bloody at some points."   

The Hunger was not particularly well-received upon its release and was attacked by many critics for being heavy on atmosphere and visuals but slow on pace and plot. Roger Ebert, of the Chicago Sun-Times, described the film as "an agonizingly bad vampire movie".  Camille Paglia writes that The Hunger comes close to being a masterpiece of a "classy genre of vampire film" but that it is "ruined by horrendous errors, as when the regal Catherine Deneuve is made to crawl around on all fours, slavering over cut throats."  Elaine Showalter calls The Hunger a "post-modernist vampire film" that "casts vampirism in  bisexual terms, drawing on the tradition of the lesbian vampire...Contemporary and stylish,   is also disquieting in its suggestion that men and women in the 1980s have the same desires, the same appetites, and the same needs for power, money, and sex." 
 Bauhaus song TV series of the same name. The Hunger holds a 46% rating on Rotten Tomatoes based on 28 reviews. 

==Remake== remake of the film,  with the screenplay written by Whitley Strieber. 

==See also==
*  , in which Sarandon talks candidly about The Hungers lesbian seduction scene.

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 