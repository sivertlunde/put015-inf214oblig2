February 14 (film)
 
{{Infobox Film
| name           = February 14
| image          =
| image_size     =
| caption        =
| director       = S. P. Hosimin
| producer       = Salem Chandrasekharan
| writer         = S. P. Hosimin
| narrator       =
| starring       = Bharath Renuka Menon
| music          = Bharathwaj
| cinematography = R. Rathnavelu
| editing        = Suresh Urs
| studio         = Sri Saravanaa Creations
| distributor    =
| released       = July 22, 2005
| runtime        = 140 mins
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
}}
February 14 is a 2005 Indian Tamil romance film directed by debutant S. P. Hosimin. Produced by Sri Saravanaa  Creations, the film stars Bharath and Renuka Menon, whilst its soundtrack was composed by Bharathwaj.

==Plot==

Shiva (Bharath) enters into St.Peters college, Bangalore and meets Pooja (Renuka Menon), who was born and brought up in the United States and has come to India to stay with her grandparents while completing her college education. Shiva falls in love with her but soon realises that their characters are totally different. Pooja feels alone, like a fish out of water in India and she wants to return to the US. So Shiva thinks about a  plan to get her to stay and he expresses his love to her by creating a fictitious character MR X but never reveals about the character till the end.  Pooja(Renuka Menon) who got impressed by MR X refuses to see him in the later stage but understand and accepts shivas true love.

==Cast==
*Bharath as Shiva/Mr. X
*Renuka Menon as Pooja
*Vadivelu
*Sathyan Sivakumar Santhanam
*Suman Shetty
*Pyramid Natarajan
*Raviprakash
*Ajmal Ameer
*Mayilsamy
*Ragasya (Special appearance)

==Soundtrack==
{{Infobox album
| Name = February 14
| Longtype =
| Type = Soundtrack Bharadwaj
| Cover =
| Released = 2005 Feature film soundtrack Tamil
| Bharadwaj
| Label = Star Music Classic Audio
}}

The music was composed by Bharadwaj (music director)|Bharadwaj.
#Laila Majnu - Karthik (singer)|Karthik, Sadhana Sargam, written by Na. Muthukumar
#Aantha Avasthai - Shreya Ghoshal, written by Kabilan 
#Nanba Nanba - Srinivas, Karthik, Janani Bharadwaj, Bharathwaj, written by Pa. Vijay
#Un Peyarennada - Anuradha Sriram, written by S. P. Hosimin
#Idhu Kathala - Haricharan, written by S. P. Hosimin
#Ennachu Enakku - Sri Krishna, written by S. P. Hosimin

==External links==
* 

 
 
 
 
 
 
 


 
 