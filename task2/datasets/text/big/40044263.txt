She Devil (1957 film)
{{Infobox film
| name           = She Devil
| image          = 1957 she devil small poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster Kurt Neumann
| producer       = Kurt Neumann
| screenplay     = Kurt Neumann Carroll Young
| based on       =  
| narrator       = Jack Kelly Albert Dekker
| music          = Paul Sawtell Bert Shefter
| cinematography = Karl Struss
| editing        = Carl Pierson Regal Films
| distributor    = 20th Century Fox
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} horror film Kurt Neumann, Jack Kelly and Albert Dekker. 

==Plot==
Dr. Dan Scott has developed a serum that cures the ills of animals, although it did alter the color of a leopard used in one experiment. Eager to try it on a human being, despite his mentor Dr. Richard Bachs many concerns, Scott finds a consenting patient in Kyra Zelas, a woman with a meek personality who is dying of tuberculosis.

The serum seems to cure her instantly. It also dramatically affects her personality, Kyra shows a flash of temper, then jumps out of a car and runs into a shop, where she steals a dress and disguises her identity by willing her hair color to change from brunette to blonde.

Scott falls in love with her. At a party, however, Kyra seduces a guest, Barton Kendall, and when his wife Evelyn objects, Kyra disguises herself again and murders her. Then she marries Kendall, but behaves monstrously toward him. The doctors use a ploy that leaves Kyra in an unconscious state, then perform surgery to reverse the serums effect, which also restores Kyras terminal disease.

==Cast==
* Mari Blanchard as Kyra Zelas Jack Kelly as Dr. Dan Scott
* Albert Dekker as Dr. Richard Bach John Archer as Barton Kendall
* Fay Baker as Evelyn Kendall
* Blossom Rock as Hannah, the housekeeper
* Paul Cavanagh as Sugar Daddy
* George Baxter as Store Manager
* Helen Jay as Blond Nurse
* Joan Bradshaw as Redhead
* X Brands as First Doctor
* Tod Griffin as Intern

==Reception==

===Critical response=== RKO stock noir Angel Angel Face Paint By Numbers atrocity." 

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD/Blue-ray review at DVD Beaver (includes images)
*  

 
 
 
 
 
 
 
 
 
 