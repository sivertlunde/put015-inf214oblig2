Sharky's Machine (film)
{{Infobox film
| name           = Sharkys Machine
| image          = Sharkys machine ver3.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Burt Reynolds
| producer       = Hank Moonjean
| writer         = William Diehl Gerald Di Pego
| narrator       =
| starring       = {{plainlist|
* Burt Reynolds
* Vittorio Gassman
* Brian Keith
* Charles Durning
* Earl Holliman
* Bernie Casey
* Henry Silva
* Darryl Hickman
* Richard Libertini
* Rachel Ward
}}
| music          =
| cinematography = William A. Fraker
| editing        = William D. Gordean Dennis Virkler
| distributor    = Orion Pictures Warner Bros.
| released       =  
| runtime        = 122 min
| country        = United States English
| budget         =
| gross          = $35,856,053
}} 1981 Film|motion picture directed by Burt Reynolds, who stars in the title role. The movie is an adaptation of William Diehls first novel Sharkys Machine (1978), with a screenplay by Gerald Di Pego.

Diehl, who was age 50 when he wrote the novel, saw the movie shot on location in and around his hometown of Atlanta, Georgia (U.S. state)|Georgia. Its cast included Burt Reynolds, Vittorio Gassman, Brian Keith, Charles Durning, Earl Holliman, Rachel Ward, Bernie Casey, Henry Silva, and Richard Libertini.

The romantic sub-plot of this movies storyline has been likened to that of the classic film noir Laura (1944 film). 

It was one of the most successful box-office releases of a film directed by Reynolds.

==Plot==
  MARTA bus only after the wounding of the bus driver. In the aftermath, Sharky is demoted to vice-squad, which is considered the least desirable assignment in the police department.

In the depths of the vice-squad division, led by Friscoe (Charles Durning), the arrest of small-time hooker Mabel results in the accidental discovery of a high-class prostitution ring that includes a beautiful escort named Dominoe (Rachel Ward) who charges $1,000 a night. Sharky and his new partners begin a surveillance of her apartment and discover that Dominoe is having a relationship with Hotchkins (Earl Holliman), a candidate running for governor.

With a team of downtrodden fellow investigators that includes veteran Papa (Brian Keith), Arch (Bernie Casey) and surveillance man Nosh (Richard Libertini), referred to by Friscoe sarcastically as Sharkys "Machine politics|machine," he sets out to find where the trail leads. During one of the stakeouts, a mysterious crime kingpin known as Victor (Vittorio Gassman) comes to Dominoes apartment. He has been controlling her life since she was a young girl, but now she wants out. Victor agrees but forces her to have sex with him one last time.

The next day, Sharky witnesses (what appears to be) Dominoe blown away by a shotgun blast through her front door, killing her and disfiguring her face beyond recognition. Sharky has privately been developing feelings for her while viewing through binoculars and listening to her bugged conversations. The man who shot her, known as Billy Score, is a drug addict and Victors brother. He answers to Victor, as does Hotchkins, who is in love with Dominoe but remains a powerless political stooge under Victors rule.

Dominoe suddenly turns up to Sharkys surprise, and is told that her friend Tiffany used her apartment and is the one who was mistakenly shot by Billy Score (Henry Silva). Dominoe is convinced that if Victor wants her dead, she is going to be dead, but reluctantly leaves with Sharky to be hidden away at his childhood home in the West End neighborhood. Meanwhile, Nosh informs Sharky that most of the surveillance tapes have disappeared from the police station, leaving both of them wondering if the investigation has been compromised. Nosh is then confronted by Billy Score, who kills him offscreen.

Sharky confronts Victor at his penthouse apartment in the Westin Peachtree Plaza and vows to bring him to justice. Victor smugly knows that Dominoe is dead and cannot testify against him, but is stunned to be told by Sharky that she is still alive.

While attempting to find Nosh at his home, two men spring an attack on Sharky and he is knocked cold. He awakens on a boat, where he is held captive and tortured by Smiley, who turns out to be working for Victor. Smiley informs him of the killing of Sharkys old narcotics division boss JoJo (Joseph Mascolo) (who was run over by a car), and reveals that Nosh is dead as well. He cuts off two of Sharkys fingers while demanding to know where Dominoe can be found. Sharky attacks and shoots Smiley, and he manages to escape. Later, Sharky turns up with Dominoe at a Hotchkins political rally, to the candidates considerable shock. Hotchkins is placed under arrest, and Victor finds out about it on the evening newscasts.

Billy Score, in an agitated state, shoots and kills Victor. Almost immediately, Sharky and other police officers arrive at Victors penthouse in an attempt to catch Billy. He is pursued through the upper floors of the Westin, where like a ghostly apparition he appears and disappears, killing Papa and seriously wounding Arch. Billy ultimately is gunned down by Sharky, crashing through a window and plummeting to his death nearly 700 feet below. In the end, Sharky returns to his childhood home, where Dominoe is now living with him.

==Cast==
* Burt Reynolds as Sharky
* Charles Durning as Friscoe
* Vittorio Gassman as Victor
* Brian Keith as Papa
* Bernie Casey as Arch
* Rachel Ward as Dominoe
* Darryl Hickman as Smiley
* Earl Holliman as Hotchkins
* Henry Silva as Billy Score
* Richard Libertini as Nosh
* John Fiedler as Twigs
* Hari Rhodes as Highball
* Joseph Mascolo as JoJo
* Carol Locatell as Mabel

==Production==
Reynolds said he was attracted to the film because it was similar to Laura (1944) his favourite movie. He talked to John Boorman about directing but it was too soon after Excalibur. Boorman suggested Reynolds direct himself. Burt Reynolds: --Getting Behind the Camera Burt Reynolds Behind The Lens
By STEPHEN FARBER. New York Times (1923-Current file)   20 Dec 1981: D17.  
 Hyatt Regency Hotel (doubling for the Westin Peachtree Plaza) still holds up as the highest free-fall stunt ever performed from a building for a commercially-released film. The stuntman was the legendary Dar Robinson. Despite it being a record-setting fall, only the briefest moment of the beginning of the fall is used in the movie. The bulk of the fall from the skyscraper as shown on film is clearly of a dummy. Street Life" Jackie Brown (1997). (Crawford is given the only credit on the song title.) Strangely and perhaps for time constraints the beautiful and haunting one and a half minute opening of the song heard over most of the opening credits is omitted from the soundtrack and has never been released. 

As was standard for the time, little of Severinsens score is included on the album with many of his contributions being heavily edited for the album tracks and several, like his version of "My Funny Valentine" being omitted altogether. 

The soundtrack album has been re-released after more than thirty years on the Varèse Sarabande label. 
 

==Music From The Original Soundtrack==

TRACKS: 
# Street Life - Randy Crawford; 
# Dope Bust - Flora Purim and Buddy De Franco;  Route 66 - The Manhattan Transfer
# My Funny Valentine - Chet Baker; 
# High Energy - Doc Severinsen; 
# Love Theme From Sharkys Machine - Sarah Vaughan;  Joe Williams; 
# My Funny Valentine - Julie London; 
# Sexercise - Doc Severinsen; 
# Lets Keep Dancing - Peggy Lee; 
# Sharkys Theme - Eddie Harris; 
# Before You - Sarah Vaughan and Joe Williams.

==Remake==

A remake of the film is in production, directed by Phil Joanou and produced by Mark Wahlberg. {{cite web
|url=http://movies.about.com/od/moviesinproduction/a/sharkys040606.htm
|title=The Burt Reynolds Movie Sharkys Machine Gets the Remake Treatment
|author=Rebecca Murray
|work=About.com
}} 

==References==
 

==External links==
* 
*  
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 