Donald's Cousin Gus
{{Infobox Hollywood cartoon
|cartoon_name=Donalds Cousin Gus
|series=Donald Duck
|image=Donalds Cousin Gus.jpg
|caption=Theatrical release poster Jack King
|story_artist=Jack Hannah Carl Barks Dick Lundy
|voice_actor=Clarence Nash (Donald Duck) Pinto Colvig (Barking Hot Dog)
|musician=
|producer=Walt Disney
|studio=Walt Disney Productions
|distributor=RKO Radio Pictures
|release_date=May 3, 1939 (USA)
|color_process=Technicolor
|runtime=7 minutes
|preceded_by=The Hockey Champ (1939)
|followed_by=Beach Picnic (1939) English
}}

Donalds Cousin Gus is a 1939 Walt Disney cartoon in which Donald Duck is visited by his gluttonous cousin, Gus Goose, who proceeds to eat Donald out of house and home. It was released on May 3, 1939.
 televised in the United States, airing as part of NBCs "first night" of sponsored programming on May 3, 1939. 
 Jack King, Don Towsley. The story was created by Jack Hannah and Carl Barks.

==Availability==

===VHS===
*In the United States on Cartoon Classics: Limited Gold Editions: Donald
*In Germany on Donald Duck Geht in die Luft, Drei Caballeros im Sambafieber, Goofy und Pluto Total Verrückt, and Mit Mir Nicht
*In France on Disney Parade 3
*In Italy on Paperino, Sono Io ...  Paperino, Cartoons Disney 6

===Laserdisc and DVD===
The short can be found on the Japanese laserdiscs Hello Donald, Donald: Limited Gold Edition, and Disney Cartoon Festival 3. It can also be found on   DVD, which was released on May 18, 2004.

==Television airings==
It was later reaired on television on: The Ink and Paint Club, episode 21, Goin to the Birds
*Mickeys Mouse Tracks, episode 77
*Walt Disney anthology series|Disneyland, episode The Plausible Impossible

==References==
 

==External links==
* 
* 
 
 
 
 
 
 

 
 