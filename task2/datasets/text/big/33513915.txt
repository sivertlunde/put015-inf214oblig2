Fires of Fate (1932 film)
 
 
{{Infobox film
| name           = Fires of Fate
| image          =
| image_size     =
| caption        =
| director       = Norman Walker 
| producer       = Norman Walker
| writer         = Arthur Conan Doyle (novel)   Dion Titheradge
| narrator       =
| starring       = Lester Matthews   Kathleen ORegan   Dorothy Bartlam  Jean Cadell
| music          = Idris Lewis
| cinematography = Claude Friese-Greene
| editing        = Bert Bates
| studio         = British International Pictures
| distributor    = Wardour Films 
| released       = September 1932
| runtime        = 74 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
  Norman Walker and starring Lester Matthews, Kathleen ORegan and Dorothy Bartlam.  It was adapted from the play Fires of Fate by Arthur Conan Doyle which was in turn based on his 1898 novel The Tragedy of the Korosko.

==Cast==
* Lester Matthews as Lt. Col Egerton 
* Kathleen ORegan as Nora Belmont 
* Dorothy Bartlam as Kay Byrne 
* Jean Cadell as Miss Byrne 
* Donald Calthrop as Sir William Royden 
* Hubert Harben as Rev. Mark Royden 
* Clifford Heatherley sa Abdullah 
* Arthur Chesney as Mr. Braddell 
* Jack Raine as Filbert Frayne 
* Garry Marsh as Captain Archer

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 