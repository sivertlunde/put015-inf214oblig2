Lady for a Day
{{Infobox film
| name           =  Lady for a Day
| image          =  Poster_lady_336.jpg
| alt            = 
| caption        = 
| director       = Frank Capra
| producer       = Harry Cohn
| writer         = Robert Riskin
| story          = Damon Runyon
| starring       = May Robson Warren William Guy Kibbee Howard Jackson Joseph Walker
| editing        = Gene Havlick
| studio         = 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $300,000
| gross          =
}}
 Best Director Best Picture.

==Plot== Spanish convent since she was an infant. Louise has been led to believe her mother is a society matron named Mrs. E. Worthington Manville who lives at the Hotel Marberry. Annie discovers her charade is in danger of being uncovered when she learns Louise is sailing to New York with her fiancé Carlos and his father, Count Romero.

Among Annies patrons are Dave the Dude, a gambling gangster who believes her apples bring him good luck, and his henchman Happy McGuire. Annies friends ask Dave to rent her an apartment at the Marberry and, although he initially declines, he has a change of heart and arranges for her to live in the lap of luxury in a palatial residence belonging to a friend. His girlfriend, nightclub owner Missouri Martin, helps transform Annie from a dowdy street peddler to an elegant dowager. Dave arranges for pool hustler Henry D. Blake to pose as Annies husband, the dignified Judge Manville.

At the pier, Annie tearfully reunites with Louise. When three society reporters become suspicious about Mrs. E. Worthington Manville, of whom they can find no public records, they are kidnapped by members of Daves gang, and their disappearance leads the local newspapers to accuse the police department of incompetence.

A few days later, Blake – in the role of Judge Manville – announces he is planning a gala reception for Louise, Carlos, and Count Romero before they return to Spain, and he enlists Daves guys and Missouris dolls to pose as Annies society friends. On the night of the reception, the police – certain Dave is responsible for the missing reporters – surround Missouris club, where the gang has assembled for a final rehearsal. Dave calls Blake to advise him of their predicament, and Annie decides to confess everything to Count Romero. But fate – in the form of a sympathetic mayor and governor and their entourages – unexpectedly steps in and allows Annie to maintain her charade and keep Louise from learning the truth.

==Cast==
* May Robson as Apple Annie
* Warren William as Dave the Dude
* Guy Kibbee as Henry G. Blake
* Glenda Farrell as Missouri Martin
* Ned Sparks as Happy McGuire
* Jean Parker as Louise
* Barry Norton as Carlos
* Walter Connolly as Count Romero
* Nat Pendleton as Shakespeare
* Halliwell Hobbes as Rodney Kents Butler
* Hobart Bosworth as Governor
* Robert Emmett OConnor as Inspector

==Production==
Damon Runyons short story Madame La Gimp was published in the October 1929 issue of Cosmopolitan (magazine)|Cosmopolitan. Columbia Pictures purchased the screen rights in September 1932, and the studio scheduled the production to begin the following May, although director Frank Capra had misgivings about the project. He reminded studio head Harry Cohn he was "spending three hundred thousand dollars on a picture in which the heroine is seventy years old," to which Cohn responded, "All I know is the things got a wallop. Go ahead." Robert Riskin was assigned to develop the story for the screen and wrote four drafts, submitting the last on May 6, 1933, three days before principal photography began. Aside from some minor revisions made during production, this final script was filmed intact. Riskins version deviated from the original Runyon story primarily in that it linked its central character and a number of plot developments to millions of Americans who were suffering from an economic crisis as a result of the onset of the Great Depression. Runyon was pleased with the changes and later said, "Lady for a Day was no more my picture than Little Miss Marker, which, like the former picture, was almost entirely the result of the genius of the scenario writers and the director who worked on it."  McBride, Joseph, Frank Capra: The Catastrophe of Success. New York: Simon & Schuster 1992. ISBN 0-671-73494-6, pp. 288-292, 294-296, 298-302, 309-310 
 Robert Montgomery, MGM refused extras who would add color to the film.  One week before filming began, Capra offered the role of Apple Annie to 75-year-old May Robson, most of whose career had been spent performing on stage. In later years, Capra thought the fact she and most of the supporting players were unfamiliar to movie audiences helped the public accept them as the down-on-their-luck characters they were meant to be. 

Just prior to the first preview in Hollywood in early July 1933, the films title was changed from Madame La Gimp to Beggars Holiday, then changed again before the film premiered at Radio City Music Hall on September 7. It went into general release on September 13 and within a very short time earned $600,000, twice its budget and a substantial sum for the period. According to the contract he had negotiated prior to making the film, Capra received 10% of the net profits.  The films success prompted the 1934 sequel Lady by Choice, directed by David Burton and starring Carole Lombard. 

In the early 1950s, the original negative was lost while being transferred from one film lab to another for preservation work. For a period of time the only existing copy was a 35mm print owned by Capra, until he made a duplicate negative from it and donated a newly minted print to the Library of Congress. Columbia later sold the rights to the story to United Artists for $200,000, and Capra remade the film as Pocketful of Miracles with Bette Davis and Glenn Ford in 1961. The director claimed to prefer the remake to the original, although most critics and, in later years film historians and movie buffs, disagreed with his assessment. 
 Richard Maltby Jr; the team of John Kander and Fred Ebb have all worked on unfinished and unrealized adaptations. 

==Critical reception==
Mordaunt Hall of the New York Times called it "a merry tale with touches of sentiment, a picture which evoked laughter and tears from an audience at the first showing." He added, "Its plausibility may be open to argument, but its entertainment value is not to be denied. It has aspects of J.M. Barrie|Barries The Old Lady Shows Her Medals and also more than a mere suggestion of George Bernard Shaw|Shaws Pygmalion (play)|Pygmalion, set forth, as might be anticipated, in a more popular vein."  

Variety (magazine)|Variety said the film "asks the spectator to believe in the improbable. Its Hans Christian Andersen stuff written by a hard-boiled journalist and transferred to the screen by trick-wise Hollywoodites. While not stinting a full measure of credit to director Frank Capra, it seems as if the spotlight of recognition ought to play rather strongly on scriptwriter Robert Riskin."  

Channel 4 calls it "wonderfully improbable and charming" and, although "not a bona fide Capra classic," it is "cracking fun all the same."  

==Awards and nominations== Best Actress Morning Glory, Little Women.

Will Rogers presented the Academy Award for Best Director, and when he opened the envelope he simply announced, "Come up and get it, Frank!" Capra, certain he was the winner, ran to the podium to collect his Oscar, only to discover Rogers had meant Frank Lloyd, who won for Cavalcade, instead. Possibly to downplay Capras gaffe, Rogers then called third nominee George Cukor to join the two Franks on stage. 

==Home media==
Image Entertainment released the film on Region 1 DVD on October 23, 2001, and on Blu-ray on March 20, 2012. Both editions include commentary by Frank Capra, Jr., as well as his brief introduction to the 2001 restoration work. The Blu-ray edition additionally incorporates about four and a half minutes of lost footage, including a key scene where Dave, Blake and McGuire are planning the reception. 

==References==
 

==External links==
*  
*  
*  
*  on Lux Radio Theater: May 1, 1939

 

 
 
 
 
 
 
 
 
 
 
 
 