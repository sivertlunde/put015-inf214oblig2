Bhageeratha (film)
 
{{Infobox film|
 name = Bhageeradha |
 image = Bhageeratha Film DVD.jpg |
 imdb_id = |
 writer = Rasool Ellore |
 starring = Ravi Teja Shriya Saran Prakash Raj |
 director = Rasool Ellore |
 producer = Mallidi Satyanarayana Reddy|
 distributor = |
 released = 13 October 2005 |
 runtime = 161 minutes |
 language = Telugu |
 country = India | Chakri |
 awards = |
 budget = |
}} Telugu movie directed by Rasool Ellore. The film stars Ravi Teja and Shriya Saran in the lead roles. The film was dubbed in Hindi as The Return of Sikander.

==Plot==
Krishna Lanka is an island in Godavari districts. The only way of commuting to that island is by boats. Lots of people die due to bad weather conditions while they travel. Krishna Lankas President Bullabbai (Vijay Kumar) wants to have a bridge to that island which costs several crores. He asks his IAS friend Venkata Ratnam (Prakash Raj) to make his dream come true. Even 20 years after Venkata Ratnam leaving the village, there is no news about the bridge. Bullebbai sends his son Chandu (Ravi Teja) to Hyderabad to meet Venkata Ratnam and learn the progress. After coming to Hyderabad, Chandu realizes that Venkata Ratnam is very money-minded and is least bothered about the bridge. The rest of the story is all about how Chandu makes Venkata Ratnam get the bridge approved by the government.

==Cast==
  was selected as lead heroine marking her first collaboration with Ravi Teja.]]
* Ravi Teja...Chandu 
* Shriya Saran...Shweta
* Vijayakumar... Bullabbai
* Prakash Raj...Venkata Ratnam 
* Brahmanandam...Hotel manager
* Jeeva (Telugu actor)|Jeeva...Yadhanna
* Nassar Sunil

==Reception==
It received mixed to negative reviews. Despite the negative reviews, the film did well and was an average at the box office.
==External links==
*  

 
 
 
 


 