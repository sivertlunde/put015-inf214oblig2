Altid ballade
{{Infobox film
| name = Altid ballade
| image = Altid ballade (Nothing but trouble) 1955 Gabriel Axel poster.jpg
| caption = Film poster
| director = Gabriel Axel
| writer = Leck Fischer   
| starring = Sigrid Horne-Rasmussen
| music = Sven Gyldmark
| cinematography = Poul Pedersen
| editing = Carsten Dahl
| producer = Nordisk Films Kompagni
| distributor = Nordisk Film
| released =  
| runtime = 88 minutes   
| awards = 
| country = Denmark
| language = Danish
| budget =
}}
Altid ballade (English: Nothing but trouble ) is a 1955 Danish drama film directed by Gabriel Axel which focuses on a working class family.    The film was a remake of Edith Carlmars 1954 film Aldri annet enn bråk,    and Axels début as a film director. 

Sigrid Horne-Rasmussen received a Bodil Award for Best Actress in a Leading Role for her role as Helga Nielsen. 

==Cast==
* Sigrid Horne-Rasmussen
*  
* Asbjørn Andersen
* Jørn Jeppesen
* Kai Holm
* Kirsten Passer Annie Birgit Hansen
* Karen Lykkehus
* Valsø Holm
* Birgit Sadolin 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 