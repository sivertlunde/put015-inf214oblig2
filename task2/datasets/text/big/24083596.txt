Desperate Remedies (film)
 
 
{{Infobox film
| name = Desperate Remedies
| caption = Film poster
| image = Desperate Remedies FilmPoster.jpeg Peter Wells
| producer = Trishia Downie
| writer = Stewart Main Peter Wells
| starring = Jennifer Ward-Lealand 
Kevin Smith 
Lisa Chappell 
Cliff Curtis
| music =
| cinematography = Leon Narbey
| editing = David Coulson
| distributor =
| released =  
| runtime = 93 minutes
| country = New Zealand
| language = English
| budget =
}} Peter Wells. It was screened in the Un Certain Regard section at the 1993 Cannes Film Festival.   

==Plot==
Set in "Hope, New Britannia",  an overwrought nineteenth century New Zealand community seemingly on the edge of destruction, Dorothea Brooke (Ward-Lealand) is a shopkeeper and dress designer with a troubled past. She strives in vain to keep her feckless, opium-addicted sister Rose out of the clutches of her former lover, Fraser (Curtis). She is in a lesbian relationship with Anne Cooper (Chappell) but tempted by newcomer and former radical Lawrence Hayes (Smith). MP and war profiteer William Poyser (Hurst) wants her business and property to shore up his tottering career, through marriage. Dorothea, Anne and Lawrence become enmeshed in a tortured triangle, resolved when Lawrence agrees to marry Rose for convenience and to get her away from Fraser. Rose dies ravaged from her opium addiction, although Lawrence and Fraser have fought a battle on board a vessel as passengers and both are missing, presumed dead. Dorothea has been coerced into accepting Poyser in marriage.

Two years pass and Fraser returns, as does Lawrence. At an opera, a mise-en-abyme version of Frasers sudden death is replayed- but the veiled female assailant turns out to be Anne, not Dorothea. Realising that Anne is her true love, Dorothea leaves Poyser to his fate, given the exposure of his financial mismanagement and gambling debts. Lawrence sees them off at the dock as they depart Hope, and Dorothea and Anne are last seen together at the vessels helm, embracing and arm in arm.

==Cast==
* Jennifer Ward-Lealand as Dorothea Brook Kevin Smith as Lawrence Hayes
* Lisa Chappell as Anne Cooper
* Cliff Curtis as Fraser
* Michael Hurst as Willam Poyser
* Kiri Mills as Rose
* Bridget Armstrong as Mary Anne
* Timothy Raby as Mr. Weedle
* Helen Steemson as Gnits
* Geeling Ng as Su Lim (as Geeling Ching)

==Production==
The film was shot on a relatively modest budget of $2 million. As a money saving device it was mostly shot in a shed on the Auckland harbour. To further save on costs the cast members each played multiple roles. By utilising Fuji stock for its excessive colour the film was able to make the most of it costuming and art direction to create visuals that far exceeded what was to be expected from its budget.   

==Release and reception==
Desperate Remedies premiered at the Miami Film Festival to wide acclaim. It appeared at many festivals including Cannes Film Festival, Kiev International Film Festival and the Turin Lesbian and Gay Film Festival. The film picked up many awards including Best Design and Best Film at Kiev and the Audience Prize in Turin. It was also well received in New Zealand, winning awards for Best Cinematography, Best Design and Best Costume Design at the 1994 New Zealand Film and Television Awards.   

The film also garnered critical acclaim in the USA and United Kingdom with many high profile publications giving it positive reviews. In the US The New York Times described the film as "gorgeous but silly" while the Los Angeles Times noted that "Wells and Main never stumble, and their film emerges as a glittery triumph". In the UK the film received glowing reviews from NME who described it as "gloriously overblown, shamelessly camp and utterly wonderful" while The Sunday Times reviewed it as "Deliciously Dotty".   

==References==
 

==External links==
* 
*  
*  at New Zealand Feature Film Database
 
 
 
 
 
 
 
 