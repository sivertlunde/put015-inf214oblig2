Tales from the Organ Trade
{{Infobox film
| name            = Tales From the Organ Trade
| image           = 
| image_size   = 250px
| alt                =

| director       = Ric Esther Bienstock
| producer     = Ric Esther Bienstock Felix Golubev Simcha Jacobovici
| editing        = Steve Weslak
| narrator      = David Cronenberg

| music        = John Welsman
| cinematography = Frank Vilaca csc Michael Ellis csc Kenneth Ng

| studio         = Associated Producers Ltd.
| released       =  
| runtime        = 82 minutes
| country        = Canada
| language       = English Writer = Ric Esther Bienstock}}

Tales From the Organ Trade is a 2013 Canadian documentary film written and directed by Emmy Award-winning  filmmaker Ric Esther Bienstock which was created in association with HBO Documentary Films, Shaw Media and Canal D.  The film examines the shadowy world of black market organ trafficking. The film is narrated by David Cronenberg. 

== Reaction ==
The film has been positively received by critics. Macleans calls the film "a serious, superb and essential documentary that cuts through the sensationalism and hysteria surrounding its subject. It’s one of the most impressive, and incisive, works of investigative journalism I’ve seen onscreen in a long time. It’s also a virtuosic feat of story-telling."  and continues to say that Ric Esther Ric Esther Bienstock|Bienstocks  film  "is a nuanced, analytical portrait of the fierce ethical dilemmas on both sides of the issue, which Bienstock distills into cautious advocacy for a sensible solution." 

== Festivals ==
*Documentary Edge Festival 2013  (World Premiere)
*Hot Docs Canadian International Documentary Festival 2013   (North American Premiere)  
*MIFF 2013  
*SouthSide Film Festival 2013  
*San Antonio Film Festival 2013  
*Indianapolis International Film Festival 2013 
*Dokufest International Documentary and Short Film Festival 2013 
*The White Sands International Film Festival 2013 
*DocUtah Southern Utah International Documentary Film Festival 2013 
*Fantastic Fest 2013 
*Film North - Huntsville International Film Festival 2013 
*Calgary International Film Festival 2013  
*Zurich Film Festival 2013  
*Raindance Film Festival 2013  
*Kansas International Film Festival 2013  
*Tenerife International Film Festival 2013  
*Hot Springs Documentary Film Festival 2013  
*Orlando Film Festival 2013 
*Mumbai International Film Festival 2013  
*Margaret Mead Film Festival 2013  
*St. Johns International Womens Film Festival 2013  
*Bergen International Film Festival 2013  
*Iowa Independent Film Festival 2013  
*Indie Memphis Film Festival 2013  
*Virginia Film Festival 2013  
*Ojai Film Festival 2013  
*Stockholm International Film Festival 2013
*St. Louis International Film Festival 2013  
*Raindance Berlin Film Festival 2013  
*Bahamas International Film Festival 2013 
*Nevada International Film Festival 2013 
*Anchorage International Film Festival 2013 
*Domestic Arrivals - Festival of Canadian Film 2014 
*Beloit International Film Festival 2014  
*One World Film Festival 2014  
*Portland Oregon Womens Film Festival 2014  
*Los Angeles Womens International Film Festival 2014
*Docudays UA International Documentary Human Rights Film Festival 2014  
*New Haven International Film Festival 2014  
*Womens International Film & Arts Festival 2014  
*Belgrade Documentary & Short Film Festival 2014  
*12th Human Rights Film Festival - Donostia San Sebastian 
*International Human Rights Film Festival Albania
*Document International Human Rights Documentary Film Festival

== Awards ==

{| class="wikitable sortable" style="font-size: 95%;"
|-
! Award
! Institution
! Year
! Category
! Recipients and nominees
! Result
|-
| Emmy Award  
| Emmy Award
| 2014
| Outstanding Investigative Journalism—Long-Form
| -
|  
|-
| Emmy Award  
| Emmy Award
| 2014
| Outstanding Writing
| Ric Esther Bienstock
|  
|-
| British Documentary Award  
| The Grierson Trust
| 2014
| Best Documentary on Current Affairs
| -
|  
|-
| Edward R. Murrow Award  
| Overseas Press Club of America
| 2014
| Best TV Interpretation or Documentary on International Affairs
| -
|  
|-
| Jack R. Howard Award  
| Scripps Howard Foundation
| 2014
| Television/Cable In-Depth National and International Coverage
| -
|  
|-
| Norman Bethune Award for Excellence in International Health Reporting - Print, Broadcast Award 
| Canadian Medical Association Media Awards for Health Reporting
| 2014
| -
| -
|  
|-
| Global Awareness Award 
| WorldMediaFestival
| 2014
| -
| -
|  
|-
| Intermedia-globe Gold Award 
| WorldMediaFestival
| 2014
| Documentary
| -
|  
|-
| Golden Eagle 
| CINE
| 2014
| Televised News Division – Investigative Reporting
| -
|  
|-
| Golden Sheaf 
| Yorkton Film Festival
| 2014
| Documentary Social/Political
| -
|  
|-
| Silver Hugo 
| Chicago International Film Festival Television Awards
| 2014
| Documentary: Social/Political
| -
|  
|-
| Silver Screen 
| US International Film & Video Festival
| 2014
| Documentary Programs: Social Issues
| -
|  
|-
|}

== Festival Awards ==

{| class="wikitable sortable" style="font-size: 95%;"
|-
! Festival
! Award
! Year
! Category
! Recipients and nominees
! Result
|-
| Documentary Edge Festival 
| -
| 2013
| Best Sign of the Times
| -
|  
|-
| DocUtah Southern Utah Documentary Film Festival 
| Raven Award
| 2013
| Best Film
| -
|  
|-
| DocUtah Southern Utah Documentary Film Festival 
| Raven Award
| 2013
| Best Director
| Ric Esther Bienstock
|  
|-
| Tenerife International Film Festival 
| -
| 2013
| Best Feature Documentary
| -
|  
|-
| Nevada International Film Festival 
| -
| 2013
| Special Jury Prize
| -
|  
|-
| 12th Human Rights Film Festival - Donostia San Sebastian 
| Amnesty International Award
| 2014
| -
| -
|  
|-
| Mexico International Film Festival 
| Golden Palm Award
| 2014
| -
| -
|  
|-
|}

== References ==
 

== External links ==
* 
* 

 
 
 
 
 