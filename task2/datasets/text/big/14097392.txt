Year One (film)
{{Infobox film
| name           = Year One| image          = Year one.jpg
| caption        = Theatrical release poster
| director       = Harold Ramis
| producer       = Harold Ramis Judd Apatow Clayton Townsend
| screenplay     = Harold Ramis Gene Stupnitsky Lee Eisenberg
| story          = Harold Ramis
| starring       = Jack Black Michael Cera Olivia Wilde Juno Temple Christopher Mintz-Plasse David Cross Theodore Shapiro
| cinematography = Alar Kivilo
| editing        = Craig Herring Steve Welch
| studio         = The Apatow Company
| distributor    = Columbia Pictures
| released       =  
| runtime        = 100 minutes  
| country        = United States
| language       = English
| budget         = $60 million
| gross          = $62.3 million  
}}
Year One is a 2009 comedy film directed by Harold Ramis, and produced by Judd Apatow. The film stars Jack Black and Michael Cera, and features Christopher Mintz-Plasse and Hank Azaria. The film was released in North America on June 19, 2009 by Columbia Pictures. The film would be Ramis last as an actor, writer and director before his death in 2014.

==Plot==
Zed (Jack Black) is a hunter and Oh (Michael Cera) is a gatherer. After being informed that Zed ate from the Tree of Knowledge of Good and Evil, the shaman (Bill Hader) and Marlak (Matthew Willig) banish him from the tribe. After Zed burns down the village by accident Oh decides to go with Zed on his journey to discover all the world has to offer. Along the way, they encounter Cain and Abel (David Cross and Paul Rudd). Cain kills Abel and informs Zed and Oh that they must escape with him or else be accused of killing Abel.
 Sodomites attack and take the slaves prisoner, though Zed and Oh escape and hide in the desert, watching the Sodomites. 

The next morning, Zed and Oh discover that the Sodomites have left with the slaves. They head off to save the slaves. They come to a mountain and find Abraham (Hank Azaria) about to kill his son Isaac (Christopher Mintz-Plasse). Zed stops them, claiming that the Lord sent him to do so. Abraham takes them to his Hebrew village and tells them about the cities of Sodom and Gomorrah.
 
Zed and Oh head off for Sodom after Abraham decides to circumcise them. As they arrive in Sodom, they are captured. Cain, now a Sodomite soldier, saves them from being sodomized, calling them "his brothers." The two recalled that they were sold by Cain as slaves and Cain apologizes and offers them food. While being given a tour of the city by Cain, Zed and Oh are offered by Cain to become guards. Shortly after they become guards and are patrolling the city they see the princess Inanna (Olivia Wilde), who is fasting because she feels guilty that most of the city is starving. That night, at a party, Zed is invited by the princess to talk with her. 

Inside the palace, Zed sees Maya and Eema serving as slaves, while Oh is forced to follow the very effeminate high priest around the palace. Zed meets Princess Inanna and she asks him to enter the Holy of Holies and tell her what it is like, thinking that Zed is the "Chosen One." Inside the temple, Zed encounters Oh, who is hiding from the high priest (Oliver Platt). There, they get into a heated argument and are then imprisoned for going inside the temple. The two are sentenced to be stoned to death but Zed convinces the Sodomites to have mercy, so they are instead sentenced to hard labor until they die from work. The king then announces that he will be sacrificing his daughter and two virgins (Maya and Eema) as a gift to the gods. 
 lay with each other inside the palace, which not only consummates their relationship, but also means that Eema cannot be sacrificed. They then come out to help Zed fight the soldiers (including Cain). The crowd kills all the leaders and proclaim Zed as the Leader being the "Chosen One". Zed turns this down, letting Inanna rule, and instead becomes an explorer with Maya. Oh becomes the leader of the village where the whole adventure started. The two say their goodbyes and head their separate ways.

==Cast==
* Jack Black as Zed, son of Zero
* Michael Cera as Oh, son of Ooh
* Olivia Wilde as Princess Inanna 
* June Diane Raphael as Maya
* David Cross as Cain
* Gabriel Sunday as Seth
* Paul Rudd as Abel  (uncredited)
* Juno Temple as Eema, daughter of Zero and sister of Zed Matthew J. Willig as Marlak
* Horatio Sanz as Enmebaragesi
* Oliver Platt as High Priest
* Christopher Mintz-Plasse as Isaac
* Eden Riegel as Lilith
* Hank Azaria as Abraham
* Bill Hader as The Shaman Sargon
* Harold Ramis as Adam
* Rhoda Griffis as Eve
* Xander Berkeley as The King
* Gia Carides as The Queen
* Paul Scheer as Bricklayer
* David Pasquesi as Prime Minister
* Kyle Gass as Zaftig the Eunuch
* Marshall Manesh as Slave Trader
* Bryan Massey as New Guard

==Music== Theodore Shapiro, who recorded his score with contemporary band elements, and a 75-piece ensemble of the Hollywood Studio Symphony at the Sony Scoring Stage.   

==MPAA rating==
The film was originally rated R, "for some sexual content and language" by the   

==Marketing==
A commercial for this film aired during Super Bowl XLIII; soon after, a clip from the film surfaced on YouTube and the first trailer was attached to I Love You, Man.

==Reception==
===Critical reaction===
The film received negative reviews from critics.  , which assigns a weighted average score  out of 100 top reviews from mainstream critics, the film has an average score of 34% based on 28 reviews.  

Michael Phillips of the Chicago Tribune gave the film a "fair-good" review of 2.5/4 stars stating that "Year One wont join his list of essential comedies, the ones Ramis helped create as writer, director, performer or combination thereof."  Claudia Puig of USA Today gave the film 2/4 stars complaining that the film "is scattershot and silly, squandering its potential by relying on juvenile bawdy humor".  Roger Ebert of the Chicago Sun-Times gave the film 1 star out of 4, stating that the film "is a dreary experience, and all the ending accomplishes is to bring it to a close".  Kyle Smith of the New York Post gave the film 1.5 stars out of 5 saying it was "mediocre at best." 

===Box office===
Year One opened at #4 at the US box office in its opening weekend.  The film eventually grossed $43,337,279 in US ticket sales, and a further $16,899,152 worldwide, for a total of $62,357,900. 

==References==
 

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 