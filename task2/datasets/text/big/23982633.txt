Harry Brown (film)
{{Infobox film
| name           = Harry Brown
| image          = Harry Brown poster.jpg 
| caption        = Theatrical release poster Daniel Barber
| producer       = Matthew Vaughn Kris Thykier Matthew Brown Keith Bell Gary Young Ben Drew David Bradley Jack OConnell Sean Harris Ruth Barrett Pete Tong Paul Rogers
| cinematography = Martin Ruhe Joe Walker Marv Partners UK Film Council HanWay Films Prescience Framestore Features Lionsgate UK
| released       =  
| runtime        = 103 minutes
| country        = United Kingdom
| language       = English
| budget         = $7.3 million 
| gross          = $10.3 million 
}} vigilante thriller Daniel Barber Jack OConnell, and Liam Cunningham. The story follows Harry Brown, a widowed Royal Marines veteran, who had served in Northern Ireland during The Troubles, living on a London housing estate that is rapidly descending into youth crime; Harry fights fire with fire after a friend is murdered.
 Plan B Chase & Lionsgate UK on 11 November 2009; the film was released in the United States by Samuel Goldwyn Films and Destination Films on 30 April 2010. The film was mainly filmed on and around the mostly abandoned Heygate Estate in Walworth, London;  which was due to be demolished in late 2010. And the subway filming at Marks Gate, East London. Peter Walker  , The Guardian, 3 September 2010 

==Plot== David Bradley). Drugs are dealt openly in the pub. When the hospital phones to tell him that his wife, Kath, is dying, Harry is too late to see her one last time because he is too scared to take the quicker underpass route, where a gang holds court. His wife is laid to rest next to the grave of their thirteen-year-old daughter, Rachel, who died in 1973.
 Ben Drew) Jack OConnell) are arrested for the murder, but released due to lack of evidence. After Lens funeral, Harry is held at knife-point by Dean, who intends to rob him. Harry stabs Dean with his own knife in self-defence during a brief struggle. Frampton visits Harry again the following morning and informs him that because Len was killed with his own bayonet, any charges could be reduced to manslaughter on the basis of self-defence.
 cannabis and overdosing girl pornographic films. When Harry suggests calling an ambulance for the girl, Stretch threatens Harry, and he kills the dealers in retaliation, before burning down their den and leaving with the girl and some firearms in a stolen Land Rover.

After leaving the girl outside a hospital with a large sum of money taken from her abusers, Harry follows Marky, to find him being sexually abused by Troy Martindale, a heroin kingpin. Harry shoots and kills Martindale and captures Marky, whom he tortures into revealing some mobile phone camera footage of Lens murder, proving the gang did not kill Len in self-defence, but had already disarmed him before killing him with his own knife. Harry uses Marky to bait Noel and Carl into a gunfight in the underpass. Carl is killed by Harry, and Marky is killed in the crossfire between Harry and Noel. After Marky is killed, Noel flees the underpass, with Harry in pursuit, before Harry collapses due to an emphysema attack.

Frampton has worked out that Harry is the likely killer. However, convinced that the recent gang deaths are instead related to a gang war, Police Superintendent Childs (Iain Glen) arranges her transfer to an anti-identity-theft unit and orders a major arrest operation on the estate, which then results in a massive riot. Harry discharges himself from hospital to pursue Noel. Driving onto the estate to stop him, Frampton and Hicock are involved in a car crash in which Hicock is severely injured. Harry rescues them and takes them to the nearby pub, where Frampton warns Harry that Sid Rourke (Liam Cunningham), the landlord, is actually Noels uncle. Harry discovers that Sid has been hiding Noel, but his guard drops due to his emphysema, allowing Sid to take the gun and reveal that he plans on killing Harry and the officers to protect Noel and also appears to be the gangs real leader. Frampton tries to call for backup, only to be stopped midway by Noel, who begins to strangle her after Sid suffocates an unconscious Hicock. After Harry draws a revolver and shoots Noel, Sid shoots and wounds Harry, only to be shot by police snipers, who have picked up Framptons interrupted call.

At a press conference held after the riot, Superintendent Childs announces that Hicock is to be awarded a posthumous Queens Gallantry Medal and Frampton is to be commended for their work and sacrifice, and denies any evidence of vigilante involvement in the entire case, saying that any such suggestions are unhelpful. Frampton leaves, evidently scarred by her experiences. The final scene is of a recovered Harry walking towards the underpass, now free and safe to walk through.

==Cast==
 
* Michael Caine as Harold "Harry" Brown
* Emily Mortimer as Detective Inspector Alice Frampton
* Charlie Creed Miles as Detective Sergeant Terence "Terry" Hicock David Bradley as Leonard "Len" Attwell Ben Drew as Noel Winters
* Sean Harris as Stretch Jack OConnell as Marky
* Jamie Downey as Carl
* Lee Oakes as Dean Saunders
* Joseph Gilgun as Kenneth "Kenny" Soames
* Liam Cunningham as Sidney "Sid" Rourke
* Iain Glen as Superintendent Childs
* Klariza Clayton as Sharon "Shaz" Thompson
* Liz Daniels as Katherine "Kath" Brown
* Orla ORourke as Nurse # 2

==Reception==
Harry Brown was met with mixed to positive reviews. Review aggregation website Rotten Tomatoes reports that 66% of critics have given the film a positive review based on 113 reviews, with an average score of 6.1/10,  critical consensus being, "Its lurid violence may put off some viewers, but Harry Brown is a vigilante thriller that carries an emotional as well as a physical punch, thanks to a gripping performance from Michael Caine in the title role."  Empire (magazine)|Empire gave the film four stars out of five, The Sunday Times awarded it one; GQ magazine gave the film five stars out of five, calling it "truly awesome". The News of the World gave the film four out of five, The Daily Mail said "finally a film that really matters...Brilliant", and Shortlist called it "the best British film of the year".
 Death Wish A Nightmare on Elm Street. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 