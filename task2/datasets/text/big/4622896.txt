Krull (film)
 
 
{{Infobox film
| name           = Krull
| image          = Krull.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Peter Yates
| producer       = Ron Silverman
| writer         = Stanford Sherman
| starring       = {{Plainlist|
* Ken Marshall
* Lysette Anthony
* Freddie Jones
* Francesca Annis
}}
| music          = James Horner
| cinematography = Peter Suschitzky
| editing        = Ray Lovejoy
| studio         = Barclays Mercantile Industrial Finance
| distributor    = Columbia Pictures
| released       =  
| runtime        = 121 min.
| country        = United Kingdom United States
| language       = English
| budget         = US$45–50 million
| gross          = $16,519,463
}}

Krull is a 1983 British-American heroic fantasy-science fiction film directed by Peter Yates and starring Ken Marshall and Lysette Anthony. It was produced by Ron Silverman and released by Columbia Pictures.

The film includes early screen roles for actors Liam Neeson and Robbie Coltrane.

== Plot ==
 
A narrator describes a prophecy regarding "a girl of ancient name that shall become queen, that she shall choose a king, and that together they shall rule their world, and that their son shall rule the galaxy".

The planet Krull is invaded by an entity known as "The Beast" and his army of "Slayers", who travel the galaxy in a mountain-like spaceship called the Black Fortress. In a ceremony involving exchanging a handful of flame between the newlyweds, Prince Colwyn and Princess Lyssa attempt to marry and form an alliance between their rival kingdoms in the hope that their combined forces can defeat the Beasts army. A prophecy foretells that Lyssa will bear a child destined to rule the galaxy. The Beast has his Slayers attack the wedding before it is concluded. The castle of Lyssas father, King Eirig, is destroyed and the Kings are murdered. The Kingdoms armies are devastated and the princess is kidnapped.

Colwyn, the only survivor, is found and nursed by Ynyr, the Old One. Colwyn sets out to rescue Lyssa, but Ynyr tells him in order to defeat the Beast he must find the "Glaive", an ancient, magical, five-pointed throwing weapon with retractable blades which Colwyn retrieves from a high mountain cave. He learns from Ynyr that he must also track down the Black Fortress, which teleports to a new location every day at sunrise. As they travel Colwyn and Ynyr are joined by an accident-prone magician, Ergo "the Magnificent", and a band of nine fugitives led by Torquil. Colwyn enlists the convicts aid and in return offers them their freedom as their reward. Torquils group of thieves, fighters, bandits and brawlers include Kegan, Rhun, Oswyn, Bardolph, Menno, Darro, Nennog, and Quain, who each has minor, notable roles in the story. The cyclops Rell, who possesses the ability to know when his time of death arrives, also joins the group after first following them from a distance.

To help them find the Black Fortress, Colwyns small army travels to the home of the blind Emerald Seer, and his young apprentice Titch. The Emerald seer uses his emerald crystal to try and view where the fortress will rise but the beasts hand rises out of nowhere and crushes the crystal before the group can discover the location. The Emerald Seer tells the group that the only place where the beasts power cannot overcome his vision is a place in the swamp. Ynyr persuades the Emerald Seer to go to the swamp with them.

The group is attacked by Slayers as soon as they reach the swamp, killing Darro and Menno. The Beast has the Seer killed before he can reveal the location of the Fortress. While the group rests up in a forest near a village, Kegan goes to the small town and gets Merith, one of his wives, to bring food and to cook for their tired group. While they are eating and resting, Meriths assistant, under the remote command of the Beast, tries to first tempt and later, when the temptation fails, kill Colwyn. He escapes the assassination, which causes the assistant to reveal she is jealous of Colwyn and Lyssas love. The Beast, enraged by her inability to kill her target, kills her instead.

Ynyr does not camp with the others though as he momentarily leaves to journey to the Widow of the Web for assistance in finding the Black Fortress. The Widow of the Web is actually an enchantress, also named Lyssa, who loved Ynyr long ago and was exiled to the lair of the Crystal Spider for murdering their only child. The Widow reveals where the Beasts Fortress will be at sunrise. She also gives Ynyr the sand from the enchanted hourglass that kept the Crystal Spider from attacking her and will keep Ynyr alive as long as he can continue holding the grains in his hand. Ynyr quickly flees the web as the Crystal Spider attacks the Widows prison and begins to destroy it. Ynyr returns to the group to reveal the news before he loses the last of the sand, which kills him.

Colwyn and his followers use captured Fire Mares—the only transport fast enough to reach the teleporting fortress in the same day—to reach the Black Fortress, but are attacked by Slayers, who kill Rhun. Upon finding a way in, Rell uses his tremendous strength to hold open the huge doors long enough for the others to enter. While this happens, additional Slayers attack, killing Quain. After everyone else enters, the entrance closes, crushing Rell. The group then attempts to cross a bridge, but as they make their way across, Nennog is killed by a Slayer.

Shortly afterward, as they make their way through the Fortress, the group is further reduced when Kegan sacrifices his life to save Torquil. To make matters worse, Ergo and Titch get separated from the others. When the Slayers try to kill Titch, Ergo magically transforms into a tiger and kills the Slayers, injuring himself while saving Titchs life.

Colwyn, Torquil, Bardolph, and Oswyn reach a spacious area with no apparent exits, containing a large dome. As Colwyn attempts to open a portion of the dome with the Glaive to find either another passageway and/or Lyssa, Ergo and Titch, Torquil, Bardolph, and Oswyn attempt to find another entrance to the curved structure by walking around its circumference. When one is not immediately located, they are surprised when Bardolph falls through a wall facing the dome. Torquil and Oswyn go after him and the three bandits find themselves caught in a trap with slowly closing walls studded with huge spikes. In an attempt to reach a fallen dagger, Bardolph loses his life.

Meanwhile, upon opening a section of the dome, Colwyn finds Lyssa and confronts the Beast, injuring it with the Glaive. However, he is unable to recover the Glaive from the Beasts body. With nothing to defend themselves against the Beasts counterattack, Lyssa realizes that together, their ability to project flame can finish the Beast. They quickly finish the wedding ritual and the two use the flame to slay the Beast. Its death frees Torquil and Oswyn from the spike room and the two soon find their way to Colwyn and Lyssa, who retrieve Ergo and Titch as they all make their way out of the crumbling Fortress. The survivors watch as the Black Fortress crumbles and is sucked up into the sky. Colwyn names Torquil as Lord Marshal, which Torquil accepts.

As the surviving heroes depart across a field, the narrator (Ynyr) repeats the same prophecy from the beginning, implying that the queen and her chosen king mentioned there (whose son shall rule the galaxy) are Lyssa and Colwyn respectively.

== Cast ==
 
* Ken Marshall as Colwyn; favours a sword and the Glaive
* Lysette Anthony as Princess Lyssa
* Freddie Jones as Ynyr, the Old One
* David Battley as Ergo the Magnificent, "short in stature, tall in power, narrow of purpose and wide of vision"; favours magic
* Bernard Bresslaw as Rell the Cyclops (credited as Cyclops); favours a large trident
* Alun Armstrong as Torquil, leader of the bandits; favours an axe
* Liam Neeson as Kegan, a bandit and polygamist; favours an axe
* Robbie Coltrane as Rhun, a bandit; favours a spear
* Dicken Ashworth as Bardolph, a bandit; favours daggers
* Todd Carty as Oswyn, a bandit; favours a bō staff
* Bronco McLoughlin as Nennog, a bandit; favours a net
* Gerard Naprous as Quain, a bandit; favours a bow and arrows
* Andy Bradford as Darro, a bandit; favours a whip
* Bill Weston as Menno, a bandit; favours a whip John Welsh as The Emerald Seer
* Graham McGrath as Titch, the Seers young apprentice
* Trevor Martin as the voice of the Beast
* Francesca Annis as The Widow of the Web
* Tony Church as King Turold, Colwyns father
* Bernard Archard as King Eirig, Lyssas father
* Clare McIntyre as Merith, one of Kegans many wives
* Belinda Mayne as Vella, Meriths assistant
 

The voice of Princess Lyssa was re-dubbed by American actress Lindsay Crouse as the producers wanted the Princess to have a more mature sounding voice. Krull DVD Cast and Crew Commentary 

== Production ==
The film was one of the most expensive of its time. Twenty-three sets were built for the film, covering 10 sound stages at Pinewood Studios, London. Other filming locations were Lanzarote in the Canary Islands and Cortina dAmpezzo, Campo Imperatore, Italy. 

The dub for the death screams of the Slayers and the demise of the Emerald Seer imposter was taken from the Mahar shrieks in  s that travel so fast they leave a trail of flame and can effectively fly, are played by Clydesdale horses. 

Despite persistent rumours that the film was meant to tie-in with the game Dungeons & Dragons, Gary Gygax stated that "To the best of my knowledge and belief the producres   of Krull never approached TSR for a license to enable their film to use the D&D game IP." 

== Music ==
The film score, which was composed by James Horner and performed by The London Symphony Orchestra and the Ambrosian Singers was released on Southern Cross Records, and has been re-released on CD in various releases by labels. It has been commended as part of the composers best early efforts before his more famous post-1990 era works.   

The score features traditional swashbuckling fanfares, an overtly rapturous love theme and other musical elements that were characteristic of fantasy/adventure films of the 1980s, along with incorporating avant-garde techniques with string instruments to represent some of the monstrous creatures in the story. Additionally, to accompany the main antagonists, the Beast and its army of Slayers, Horner utilised The Planets|Holst-like rhythms and groaning and moaning vocals from the choir. Also of note is a recurring "siren call" performed by female voices that starts and bookends the score, and appears numerous times in the story to represent the legacy of the ancient world of Krull. 

Horners score is reminiscent of earlier works, particularly  . Some pieces of the music were reused for the area atmosphere nearby:  —at Disneyland Paris 

The score has been released numerous times on album. The first was a 45-minute condensed edition, which was released by Southern Cross Records in 1987, featuring most of the major action cues, three renditions of the love theme and the music from the End Credits. However, music from the Main Title sequence was omitted. Southern Cross Records later released Special Editions in 1992 and 1994 (the latter a Gold disc) with a running time of over 78 minutes, expanding on all of the previously released tracks, featuring the Main Title music and other action cues.

In 1998, SuperTracks released the complete recorded score in a two-CD set with extensive liner notes by David Hirsch in a notoriously designed booklet,  however, this release, along with the 1992 and 1994 releases, have become rare and very expensive collectible items. In 2010, La-La Land Records re-issued the SuperTracks album, with two bonus cues and new liner notes by Jeff Bond in a limited edition of 3,000 copies, which sold out within less than a year. 

== Reception ==
Janet Maslin, reviewing Krull for The New York Times found the film to be "a gentle, pensive sci-fi adventure film that winds up a little too moody and melancholy for the Star Wars set," praising director Yates for "giving the film poise and sophistication, as well as a distinctly British air," as well as "bring  understatement and dimension to the material."  Baird Searles described Krull as "an unpretentious movie . . . with a lot of good things going for it," noting the film as "very beautiful, in fact, a neglected quality in these days when it seems to have been forgotten that film is a visual medium." 

Krull currently holds a 33% rating on Rotten Tomatoes based on 24 reviews.  The film made over $16.5 million in the U.S.,  failing to bring back its reported budget of over $45–50 million. However, it has gained a cult following over the years since its release. 


===Awards===
* Nominee Best Fantasy Film - Saturn Awards
*Nominee Best Music (James Horner) - Saturn Awards
*Nominee Best Costumes (Anthony Mendleson) - Saturn Awards
*Nominee Grand Prize (Peter Yates) - Avoriaz Fantastic Film Festival
 Stinkers Bad Movie Award for Worst Picture. 

== Legacy ==

=== Adaptations === Marvel Super Special No. 28 with behind-the-scenes material from the film,  and as a two-issue limited series. 

=== Video games ===
 
In 1983, several games were developed with the Krull license:
* A Parker Brothers board game and card game
* An arcade game by Gottlieb|D. Gottlieb & Co., who also designed a Krull pinball game that was never put into production.
* A console game originally planned for the Atari 5200, but changed to the Atari 2600 because of poor sales of the former system.

=== Cultural References ===
* The Krull glaive was spoofed in South Park, season 11, episode 5 ("Fantastic Easter Special").
* In the American Dad episode "All About Steve" Snot holds up a fictional magazine which reads "500 reasons why Krull is better than sex!"
* Sean Phillips, the reclusive central character of John Darnielles 2014 novel Wolf In White Van, buys an ex-rental VHS copy of the film and spends four pages musing on the films story and themes. 
* The Cyclops and fire mares from Krull are emulated in Gentlemen Broncos.
* The Krull glaive makes an appearance by intermittently floating up out of the lava in the tunnels preceding the Onyxia boss encounter in the MMO video game World of Warcraft.
* In the 2007 movie The Air I Breathe, Brendan Frasers character (Pleasure) refers to Rell, the Cyclops, as both of them share the ability to see into the future - though significantly different for each.

== Home media == Starz and Netflix until June 2012. Mill Creek Entertainment, through a license from Sony, released Krull on Blu-ray for the first time on September 30th, 2014.

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 