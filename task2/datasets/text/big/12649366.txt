The Chocolate War (film)
{{Infobox Film
| name           = The Chocolate War
| image          = Chocolate war post.jpg
| caption        = Theatrical release poster
| director       = Keith Gordon
| producer       = Saul Zaentz Jonathan D. Krane
| writer         = The Chocolate War|Novel Robert Cormier Screenplay Keith Gordon John Glover Ilan Mitchell-Smith Doug Hutchison Tom Richmond
| editing        = Jeff Wishengrad
| distributor    = MCEG Sterling (theatrical), Metro-Goldwyn-Mayer (2007, DVD)
| released       = November 18, 1988
| runtime        = 104 min.
| country        = United States
| language       = English
| budget         = $500,000
| gross          = $303,624
}} novel of John Glover, Ilan Mitchell-Smith, Wallace Langham, and Doug Hutchison. Jonathan D. Krane produced it after seeing Static, a short film Gordon wrote.

== Plot ==
The film offers a portrait of the hierarchical structure of a Catholic school, both formal and informal. New student Jerry Renault (Ilan Mitchell-Smith) must submit to the bizarre rituals of his peers and the expectations of the schools administration by selling chocolates as a fundraiser.  A secret student society, The Vigils, assigns Jerry the task of refusing to sell chocolates, drawing the ire of the schools interim headmaster, Brother Leon.  However, after Jerrys assignment is over, he continues to refuse, resulting in torment, bullying, and alienation from his peers.

== Cast == John Glover as Brother Leon
* Ilan Mitchell-Smith as Jerry Renault
* Doug Hutchison as Obie Jameson
* Wallace Langham as Archie Costello
* Corey Gunnestad as Roland "Goober" Goubert Brent Fraser as Emile Janza
* Robert Davenport as Brian Cochran
* Jenny Wright as Lisa
* Bud Cort as Brother Jacques
* Adam Baldwin as Bill Carter
* Ethan Sandler as David Caroni

== Production ==
* Doug Hutchison, who portrayed 18-year-old Obie Jameson, was 27 when this movie was filming. Kate Bushs "Running Up that Hill" was substituted.

== Differences from the novel ==
While the film generally adheres to the plot of the novel, the ending of the film contains significant changes that diverge from the novels plot and themes.  These changes have commonly been seen as negative, and have been strongly criticized for compromising the messages of the novel and attempting to force a more uplifting "Hollywood ending" to the story.
* In the novel, Jerry Renault must box Emile Janza, the school bully.  In the film, Janzas place is taken by Archie after he draws a black marble, as opposed to the book, where he draws two white marbles.
* Jerry wins the boxing match in the film, pummeling Archie and winning the praise of his classmates, much to his own chagrin, as he has now played into the Vigils manipulations. In the novel, Jerry is beaten to semi-consciousness by Janza, and taken to the hospital, having lost the war.
* Archie maintains his control of the Vigils in the novel, whereas in the film, he is physically beaten by Jerry and ultimately replaced by Obie, who, as Assigner, creates less thoughtful, more simplistic assignments.

== Release ==
On a $500,000 budget, the film grossed a mere $303,624 and is considered a box office flop.

The film was released on DVD on April 17, 2007. The special features consist of:
* Audio commentary by director Keith Gordon
* Interview with director Keith Gordon

== External links ==
 
*  
*  

 
 
 
 
 
 
 
 
 
 