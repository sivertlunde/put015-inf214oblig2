Comedy of Power
 
{{Infobox film
| name           = Comedy of Power
| image          = 
| caption        = 
| director       = Claude Chabrol
| producer       = Françoise Galfré
| writer         = Claude Chabrol Odile Barski
| starring       = Isabelle Huppert
| music          = 
| cinematography = Eduardo Serra
| editing        = Monique Fardoulis
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = France
| language       = French
| budget         = 
}}

Comedy of Power ( ) is a 2006 French drama film directed by Claude Chabrol and starring Isabelle Huppert.    The French title, which sums up the whole story, means “drunk with power”.

==Plot==
Humeau, head of a major state-owned company, is arrested for abuse of funds and interrogated by the implacable prosecutor, Jeanne Charmant-Killman. As she uncovers more and more hidden corruption, implicating politicians and foreign powers, both subtle and brutal methods are used to stop her. Intoxicated with the power of rooting out evil, her private life disintegrates and after an apparent suicide attempt her unhappy husband ends up in intensive care. At the hospital she sees Humeau, another man whose health is broken by his ordeal. The audience is left to wonder how many more victims she will amass or whether she will herself succumb to the increasing pressure.

==Cast==
* Isabelle Huppert - Jeanne Charmant-Killman
* François Berléand - Michel Humeau
* Patrick Bruel - Jacques Sibaud
* Marilyne Canto - Erika
* Robin Renucci - Philippe Charmant-Killman
* Thomas Chabrol - Félix
* Jean-François Balmer - Boldi Pierre Vernier - President Martino
* Jacques Boudet - Descarts
* Philippe Duclos - Jean-Baptiste Holéo
* Roger Dumas - René Lange John Arnold - Man in Jaguar #1
* Jean-Christophe Bouvet - Me Parlebas
* Jacques Bouanich - Prison Guard
* Benoît Charpentier - Economics Journalist

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 