Moscow, Belgium
Moscow, 2008 Belgian film directed by Christophe Van Rompaey and written by Jean-Claude Van Rijckeghem and Pat van Beirs. 
 Moscou is Flemish city of Ghent.

==Plot==
Mathilda or "Matty", (Barbara Sarafian) is a bitter, 41-year old, postal worker in Gent, Belgium.  She finds herself drifting through life waiting for her husband Werner (Johan Helengbergh) to decide if he wants to leave his current 22-year-old lover Gail, and return to Matty and their three kids.  

One day after shopping at the supermarket, Matty backs into a yellow highway tractor in the parking lot.  The owner of the truck is 29-year-old Johnny (Jurgen Delnaet).  They have an explosive argument over whose fault it is.  Johnny rips into Matty and tries to put her in her place but Matty gets the best of him and earns his respect.  The police show up and they file a report.  Johnny memorizes Mattys number from the police report and pursues her.  He calls several times and one day shows up at her apartment offering to fix the trunk of her car.  At first, she acts as though shes annoyed, but she secretly feels amused.  After he fixes her car, she invites him to join her family for dinner.  They have a good time and after, he proposes they go out for a drink.  Unwillingly, Matty accepts assuming it will be harmless.  However, during the date, he relentlessly tries to persuade her to spend the night with him.  She eventually gives in.  

The night they share together rejuvenates her.  When Werner learns about Mattys affair with Johnny, he asks a friend at the police station to run a background check.  They find out that he did time for an incident where he brutally attacked his wife and put her in the hospital for two weeks.  One night at dinner, Mattys teenage daughter Vera asks Johnny if the charges were true.  He shamefacedly says yes.  Vera leaves the table disgusted and Matty tries to break things off with Johnny but he explains he had had too much to drink that night and after his ex-wife admitted she had been having an affair for 3 months, he went into a rage.  Matty tries but she cant resist his charm and things continue as though nothing happened.  

Jealous that he might lose Matty, Werner tries to get her back.  The three end up having dinner together.  Johnny and Werner have an argument at the table and both leave while Matty hides out in the laundry room.  She realizes that Werner is the one making her unhappy, and makes the decision to try having a relationship with Johnny.  While they are out celebrating her decision, Johnny and Matty run into Johnnys yuppie ex-wife and her lawyer lover.  At this point, Johnny has had a few too many drinks and is sloshed.  All four end up in a heated argument and Johnny ends up throwing a beer keg at the lawyers car and shattering the windshield.  Matty is disgusted at Johnnys lack of restraint and leaves.  She tries to rekindle her romance with Werner but theres no passion.  A while later, Vera invites Matty to the karaoke bar, where they run into Johnny.  He tries to serenade her but she is again disgusted and leaves but later goes back to try and find him before he departs for Italy.

== Cast ==
* Barbara Sarafian as Matty
* Jurgen Delnaet as Johnny
* Johan Heldenbergh as Werner
* Anemone Valcke as Vera
* Sofia Ferri as Fien
* Julian Borsani as Peter Bob De Moor as Jacques
* Jits Van Belle as Nicky
* Griet van Damme as Nathalie
* Camille Friant as Iris

==Awards and nominations== Bermuda Film Festival
**Won: Best Narrative Feature (Christophe Van Rompaey)

*Cannes Film Festival
**Won: ACID Award (Christophe Van Rompaey)
**Won: Grand Golden Rail (Van Rompaey)
**Won: SACD Screenwriting Award (Jean-Claude Van Rijckeghem and Pat van Beirs)
 Denver Film Festival
**Won: Best Film (Van Rompaey)

*European Film Awards
**Nominated: Best Composer (Tuur Florizoone)
 Minsk Film Festival
**Won: Best Actress (Barbara Sarafian)

*São Paulo International Film Festival|São Paulo Film Festival
**Nominated: International Jury Award (Van Rompaey)

*World Soundtrack Awards
**Won: Public Choice Award (Florizoone)
**Nominated: Discovery of the Year (Florizoone)

*Zurich Film Festival
**Won: New Talent Award (Van Rompaey)

== External links ==
*  

 
 
 
 