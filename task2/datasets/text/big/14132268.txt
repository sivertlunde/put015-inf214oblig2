Beethoven Lives Upstairs
{{Infobox film
| name           = Beethoven Lives Upstairs
| image          = Beethoven Lives Upstairs.jpg
| image size     =
| caption        = David Devine David Devine  Richard Mozer   Terence Robinson
| writer         = Heather Conkie Neil Munro   Fiona Reid   Paul Soles
| music          =
| cinematography = David Perrault
| editing        = Rik Morden
| distributor    =
| released       = 1992
| runtime        = 51 min.
| country        = Canada English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 1992 HBO TV movie David Devine. Neil Munro), a boarder in the boys parents house. The film was shot in Prague in the Czech Republic and has been broadcast in over 110 countries in numerous languages and has sold over one million DVDs and is used extensively in U.S. and Canadian elementary and middle school music classrooms.  

The film went on to win the Primetime Emmy Award for Outstanding Childrens Program in 1993, was nominated for numerous Gemini Awards, and was also admitted to the Permanent Collection in the Paley Center for Media in New York City.

==Cast== Neil Munro as Ludwig van Beethoven
*Illya Woloshyn as Christoph
*Fiona Reid as Mother
*Paul Soles as Mr. Schindler
*Albert Schultz as Uncle Kurt
*Sheila McCarthy as Sophie the maid

==External links==
* 
 
 
 
 
 


 
 