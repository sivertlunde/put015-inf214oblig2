The Pay-Off
{{Infobox film
| name           = The Pay-Off
| image          = DVD cover of The Pay-Off.jpg
| image_size     =
| caption        = DVD cover
| director       = Lowell Sherman Henry Hobart
| writer         = Jane Murfin John B. Hymer Samuel Shipman
| based on       =  
| narrator       =
| starring       = Lowell Sherman Marian Nixon Hugh Trevor
| music          =
| cinematography = J. Roy Hunt
| editing        = Rose Smith
| distributor    = RKO Radio Pictures
| released       =   }}
| runtime        = 65 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

The Pay-Off is a 1930 American melodrama film directed by Lowell Sherman, who also starred in the film along with Marian Nixon and Hugh Trevor. The screenplay was adapted by Jane Murfin, along with Samuel Shipman and John B. Hymer, based on Shipman and Hymers 1927 play, Crime.

== Plot summary ==
Gene Fenmore is a suave gentleman, safely ensconced in the upper crust of society.  Unbeknownst to his society acquaintances, he is also the leader of the largest mob in the city.  But it is a mob with scruples, thanks to Fenmore, and they only prey on dishonest businessmen, and never shoot or kill anyone.  However, there is a power struggle developing between Fenmore and his number two man, Rocky, who disagrees with the moral constraints put on the gang by Fenmore. The tension between the two men is exacerbated by the fact Rocky has taken Fenmores girlfriend, Dot away from him.

As he starts to exert his influence on the other gang members, Rocky holds up a young engaged couple, Annabelle and Tommy, of their last few dollars for the fun of it. When Fenmore hears of the robbery, he gives them back their money and takes them under his wing, offering the both of them jobs on the legitimate side of his business.  When Rocky sees that Fenmore has taken a liking to the couple, he develops a plan to use them in order to take over Fenmores gang. Even though Fenmore has given explicit orders not to involve the couple in the illicit activities of the gang, Rocky takes them along when he goes to hold up a jewelry store.  When the robbery goes wrong, Rocky ends up shooting and killing the owner of the store, after which he frames Tommy and Annabelle for the crime.  

In order to set things right, Fenmore orchestrates a confrontation with the police, wherein he confesses to the jewelry store robbery, and in the ensuing melee, Rocky is killed.  As he is led off in handcuffs, Fenmore turns to the police officer and says, "If it wasnt for men like me, they wouldnt need men like you."

== Cast ==
*Lowell Sherman as Gene Fenmore
*Marian Nixon as Annabelle
*Hugh Trevor as Rocky
*William Janney as Tommy
*Helene Millard as Dot
*George F. Marion as Mouse
*Walter McGrail as Emory
*Robert McWade as Frank
*Alan Roscoe as District Attorney
*Lita Chevret as Margy
*Bert Moorhouse as Spat

(Cast list as per AFI database) 

== Notes == Eltinge 42nd Street Theatre (currently the Empire Theatre) in New York City from February to August 1927. 

In 1938, RKO would remake the film, under the title, Law of the Underworld.   
 public domain in the USA due to the copyright claimants failure to renew the copyright registration in the 28th year after publication. 

==References==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 