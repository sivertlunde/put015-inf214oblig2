Decalogue I
{{Infobox film
| name = Decalogue I
| image = Decalogue_jeden.jpg
| caption =
| director = Krzysztof Kieślowski
| producer = Ryszard Chutkovski Yoram Globus
| writer = Krzysztof Kieślowski Krzysztof Piesiewicz
| starring = Henryk Baranowski Wojciech Klata Maja Komorowska
| music = Zbigniew Preisner
| cinematography = Wieslaw Zdort
| editing = Ewa Smal
| distributor = Polish Television
| released =  
| runtime  = 53 minutes
| country = Poland
| language = Polish
| budget = $10,000
| gross = 
}} The Decalogue by Polish director Krzysztof Kieślowski, possibly connected to the first and second imperatives of the Ten Commandments: "I am the Lord your God; you shall have no other gods before me" and "Thou shalt not make unto thee any graven image."

A university professor (Henryk Baranowski) trains his young son in the use of reason and the scientific method, but is confronted with the unpredictability of fate. Reason is defied with tragic results.  

==Plot==
 ) and Paweł (Wojciech Klata)]]
Krzysztof lives alone with Paweł, his 12-year old and highly intelligent son, and introduces him to the world of personal computers. They have several PCs in their flat and do many experiments with programming such as opening/closing the doors or turning on/off the tap water with help of the PC.  

One cold winter morning Paweł asks his father to give him a physics problem to solve. Krzysztof does so and Paweł solves it very quickly on his computer. Later Paweł becomes depressed at the sight of a stray dead dog and begins to wonder about deeper things in life. When asked about the nature of death, his father gives him a very objective (but cold) explanation of death as the ending of all vital functions. 

A TV crew comes to Pawełs school and makes several shots of Paweł and his schoolmates for their reportage on the poor quality of milk in public schools. Paweł creates a program that allows him to know what his mother is doing (she appears to be living on a different continent, since the program also calculates time zone differences), but the computer is unable to answer as to what she dreams.  Irene, however, Pawełs aunt, answers easily: she dreams of Paweł. Paweł also believes that his fathers computer must know his mothers dreams. Paweł talks with his aunt about what his father said about death; she elaborates on that and talks about the soul and religion. Irene and Krzysztof discuss Pawełs decision to take religious courses at the local church.
 whether it would hold him. After filling in the data into the computer, the PC says that the expected ice would hold three times Pawełs weight. Krzysztof even goes to the lake and corroborates that the ice is strong enough to hold him.

Paweł finds a pair of skates in a drawer meant to be a Christmas gift and wants to try them on the lake not far away from their home. His father gives his permission. The next day, Krzysztof hears firemens sirens going off and people rushing to the lake. Later the mother of a classmate of Pawełs comes distraught to Krzysztof; the English lesson where Paweł was supposed to be was cancelled due to illness of the teacher and the ice in the lake broke. Krzysztof remains calm at first and refuses to believe that the ice could have broken, since his calculations clearly indicated that the ice would not break. After searching all around the neighborhood he gets confirmation from one of Pawełs friends that he was skating at the time of the accident. 

 
Irene and Krzysztof are desperate when the rescue services fish several corpses out of the frozen water (Paweł is not specifically shown to be among them, but Krzysztof and Irenes reaction seems to indicate that he is). Krzysztof returns home to find that his computer has turned on by itself again indicating that it is ready. Krzysztof later enters a provisional church, destroying a small altar to the Black Madonna of Częstochowa, which appears to be crying.     The film ends as Krzysztof, in tears, sinks to his knees and tries to cross himself with holy water on his forehead, but the water is frozen.

==Cast==
*Henryk Baranowski - Krzysztof: an atheist, professor of linguistics at the university. 
*Wojciech Klata - Paweł: Krzysztofs 12 year old son.
*Maja Komorowska - Irena: Krzysztofs sister, she deeply believes in God and does not understand her brother much but they respect each other and have in common their love for little Paweł. 
*Artur Barciś - man in the sheepskin
*Maria Gladkowska - English teacher
*Ewa Kania - Ewa Jezierska
*Aleksandra Kisielewska - woman
*Aleksandra Majsiuk - Ola
*Magda Sroga-Mikołajczyk - journalist

===Extras===
*Anna Smal-Romańska 
*Maciej Sławiński,
*Piotr Wyrzykowski
*Bożena Wróbel

==References==
 

 

 
 
 
 
 