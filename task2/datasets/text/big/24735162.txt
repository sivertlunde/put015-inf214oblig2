Clément (film)
 
{{Infobox film
| name           = Clément
| image          = Clément (film).jpg
| caption        = Film poster
| director       = Emmanuelle Bercot
| producer       = Philippe Martin Frédéric Niedermayer
| writer         = Emmanuelle Bercot
| starring       = Olivier Guéritée
| music          = 
| cinematography = Crystel Fournier
| editing        = Julien Leloup
| distributor    = 
| released       =  
| runtime        = 139 minutes
| country        = France
| language       = French
| budget         = 
}}

Clément is a 2001 French drama film directed by Emmanuelle Bercot. It was screened in the Un Certain Regard section at the 2001 Cannes Film Festival.   

==Cast==
* Olivier Guéritée - Clément
* Emmanuelle Bercot - Marion
* Kevin Goffette - Benoît
* Rémi Martin - Franck
* Lou Castel - François
* Catherine Vinatier - Aurore
* Jocelyn Quivrin - Mathieu
* David Saada - Maurice
* Eric Chadi - Julien
* Yves Verhoeven - Patrick
* Nicolas Buchoux - Barman discothèque
* Fiona Casalta - Mathilde
* Joël Curtz - Copain Clément
* Cyril Descours - Copain Clément
* Catherine Guillot - Mère Clément
* Aurélie Lepley - Copain Clément
* Damien Moratti - Copain Clément
* Eddy Okba - Copain Clément
* José Mambolongo Togbe - Videur discothèque
* Adrian Touati - Copain Clément

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 