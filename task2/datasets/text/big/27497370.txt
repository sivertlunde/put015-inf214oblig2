The Apprentice/L'Apprenti
{{Infobox film name           = The Apprentice  image          =  image_size     =  caption        =  producer       = Ches Yetman	  director       = Richard Condie writer         =  starring       =  music          =  cinematography =  editing        =  distributor    = National Film Board of Canada released       = 1991 runtime        =  country        =   Canada language       = imdb           = 
}}
The Apprentice/LApprenti is a 1991 animated short by Richard Condie, produced in Winnipeg by Ches Yetman for the National Film Board of Canada.

A more enigmatic work than Condies popular short The Big Snit, The Apprentice/LApprenti is a series of animated blackout sketches, telling the story of a medieval jester and his young apprentice. The "dialogue" is supplied by gargling noises, sampled on a computer.   

The film received a Blizzard Award for Best Animation at the 1993 Manitoba Motion Picture Industry Association Film & Video Awards.   

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 