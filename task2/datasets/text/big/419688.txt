Lady Liberty (film)
{{Infobox film
| name           = Lady Liberty
| image          = Lady Liberty (film).jpg
| image_size     = 
| caption        = 
| director       = Mario Monicelli
| producer       = Carlo Ponti
| writer         = Leonard Melfi Suso Cecchi DAmico Don Carlos Dunaway
| narrator       = 
| starring       = Sophia Loren William Devane Gigi Proietti Susan Sarandon Danny DeVito
| music     =Ron (singer)|Ron-Lucio Dalla
| cinematography    =Alfio Contini
| editing    =Ruggero Mastroianni
| distributor    = United Artists
| released       = December 23, 1971 (Italy)
June 7, 1972 (USA)
| runtime        = 97 minutes
| country        = Italy   France
| language       = English
| budget         = 
}}
Lady Liberty (Italian: La mortadella) is a 1971 Italian-French comedy film directed by Mario Monicelli and starring Sophia Loren, William Devane, Gigi Proietti, Susan Sarandon, Danny DeVito and Edward Herrmann in his film debut. 

==Plot summary==
Maddalena Ciarrapico arrives in New York City from Italy to get married and bringing with her a gift of mortadella (large Italian pork sausage) from her co-workers from the sausage factory where she used to work, for her fiancee.   But she is refused permission to bring the mortadella  into the country because of the ban on meat that may contain food-borne diseases.   An indignant Maddalena refuses to hand the sausage over, staying in the customs office at the airport, sparking a diplomatic incident in which she attracts widespread sympathy and support.

==Reception==
The New York Times was scathing of the film, observing "Probably no other woman has so triumphantly survived as many rotten movies in such a short space of time as Sophia Loren". Although "the farcical premise is promising" it was "a comedy that manages to be both too serious and not serious enough and that, at no point matches the level of the humor and intelligence of its principal performance". It also questioned "the grindingly bleak New York settings in which so much of the film is set.". 

==Cast==
* Sophia Loren - Maddalena Ciarrapico
* William Devane - Jock Fenner
* Gigi Proietti - Michael Bruni
* Beeson Carroll - Dominic David Doyle - OHenry
* Danny DeVito - Fred Mancuso
* Susan Sarandon - Sally

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 

 
 