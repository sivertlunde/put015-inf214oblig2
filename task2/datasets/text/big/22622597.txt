Mulleya Mulleya
{{Infobox film
| name           = Mulleya Mulleya
| image          = Mulleya Mulleya.jpg
| caption        = Theatrical poster for Mulleya Mulleya (1984)
| film name      = {{Film name
 | hangul         =        
 | hanja          =  
 | rr             = Yeo-in Janhoksa Mulle-ya Mulle-ya
 | mr             = Yŏin Janhoksa Mulleya Mulleya}}
| director       = Lee Doo-yong 
| producer       = Jeong Woong-ki
| writer         = Im Choong
| starring       = Won Mi-kyung Shin Il-ryong
| music          = Jeong Yoon-joo
| cinematography = Lee Seong-choon
| editing        = Lee Kyung-ja
| studio         = HanRim Films Co., Ltd.
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} Best Foreign Language Film at the 57th Academy Awards, but was not accepted as a nominee. 

==Plot==
A historical drama about the life of a widow. 15th century life was sometimes cruel to Korean women and this story depicts a lot of the injustices that could occur as happening to Kil-Rye, the heroine. 

==Cast==
* Won Mi-kyung: Gillye 
* Shin Il-ryong: Yun-bo
* Moon Jung-suk: Mother
* Choe Sung-kwan: Father
* Park Min-ho
* Choe Seong-ho
* Moon Mi-bong
* Yang Chun
* Hyun Kill-soo
* Choe Jeong-won

==Critical reception==
Comment on contemporary reactions to this film: There was a great deal of controversy in the Korean media when this film came out, saying that this work did not represent Korea well. Some objected to the shade of the lead actress skin, saying that she looked too dark for a Korean. Whiteness of skin was and is still considered important among many in Korea.

Others scoffed that it was unlikely that all of the injustices depicted could have happened to one woman. However, it is a vivid and visually spectacular depiction of the struggles many women went through in that time period.

The film also polarized audiences and stirred controversy when it was shown at the East-West Center and the Fifth International Film Festival in Honolulu.   

==See also==
* List of submissions to the 57th Academy Awards for Best Foreign Language Film
* List of South Korean submissions for the Academy Award for Best Foreign Language Film

==References==

===Notes===
 

===Bibliography===
*  
*  
*  

==External links==
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 


 