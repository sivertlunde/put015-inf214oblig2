Forbidden Planet
 
{{Infobox film
| name           = Forbidden Planet
| image          = Forbiddenplanetposter.jpg Theatrical release poster
| director       = Fred M. Wilcox
| producer       = Nicholas Nayfack
| screenplay     = Cyril Hume
| story          = {{plainlist|
* Irving Block
* Allen Adler }}
| narrator       = Les Tremayne
| starring       = {{plainlist|
* Walter Pidgeon
* Anne Francis
* Leslie Nielsen
* Warren Stevens Jack Kelly
* Robby the Robot }}
| music          = Louis and Bebe Barron
| cinematography = George J. Folsey
| editing        = Ferris Webster
| studio         = Metro-Goldwyn-Mayer Corp.
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 minutes   Internet Movie Database. Retrieved: January 16, 2015. 
| country        = United States
| language       = English
| budget         = $1,968,000 "The Eddie Mannix Ledger." Margaret Herrick Library, Center for Motion Picture Study, Los Angeles. Retrieved: January 16, 2015. 
| gross          = $2,765,000 
}}
 The Tempest. analogues to the play.  There is also a reference to one section of Carl Jung|Jungs theory on the collective unconscious. 

Forbidden Planet was filmed in   (Carnegie Mellon University). Retrieved: January 16, 2015.   

The film was entered into the Library of Congress National Film Registry in 2013, being deemed "culturally, historically, or aesthetically significant."   Washington Post, December 18, 2013. Retrieved: January 16, 2015. 

==Plot==
 
 
In the 23rd century, to discover the fate of an expedition sent 20 years earlier, starship C-57D reaches the distant planet Altair IV. Dr. Edward Morbius (Walter Pidgeon), one of the expeditions scientists, contacts the starship. He states no assistance is required, warning the Earth ship away, saying he cannot guarantee their safety. Commander John Adams (Leslie Nielsen) insists on landing.

On arrival, Adams, Lieutenant Jerry Farman (Jack Kelly), and Lieutenant "Doc" Ostrow  (Warren Stevens) are met by Robby the Robot, who transports them to Morbiuss home. There, Morbius says that an unknown "planetary force" killed nearly everyone and finally vaporized their starship, Bellerophon, as the survivors tried to lift it off. Only Morbius, his wife (who later died of natural causes), and their daughter Altaira (Anne Francis) were somehow immune. Morbius now fears the C-57-D crew are in danger. Altaira, having only known her father, becomes attracted to several of the Earth men, and a love triangle forms between Adams, her, and Farman.

The next night, equipment aboard the starship is sabotaged by an invisible intruder. Adams and Ostrow confront Morbius the following morning. They learn he has been studying the Krell, a highly advanced native race that mysteriously died out suddenly 200,000 years before. In a Krell laboratory Morbius shows Adams and Ostrow a device he calls a "plastic educator", capable of measuring and enhancing intellectual capacity. When Morbius first used this machine, he barely survived but discovered his intellect had been permanently doubled, enabling him, along with information from a stored Krell library, to build Robby and the other "technological marvels" in his home. Morbius then takes them on a tour of a vast cube-shaped underground Krell machine complex, 20 miles (30&nbsp;km) on a side, still functioning and powered by 9,200 thermonuclear reactors. Afterwards, Adams demands that the Krells knowledge be turned over for Earth supervision. Morbius refuses, citing the potential danger that Krell technology poses if it were to fall into human hands prematurely.
 force field fence deployed around the starship. This proves useless when the intruder returns undetected, murdering Chief Engineer Quinn (Richard Anderson). The C-57-Ds crew later discover it is invisible, only becoming semi-visible as a large creature outlined within the fences energized force field. Their energy weapons have no effect, and it kills Farman and two other crewmen. Morbius, who has fallen asleep in the Krell lab, is startled awake by screams from Altaira; at exactly the same instant, the roaring creature suddenly vanishes.

Later, while Adams confronts Morbius at his home, Ostrow sneaks away to use the Krell educator; as Morbius had warned, he is fatally injured. Ostrow explains to Adams that the Great Machine was built to materialize anything the Krell could imagine, projecting matter anywhere on the planet. With his dying breath, he also says the Krell forgot one thing: "Monsters from the id, ego and super-ego|Id". Adams asserts that Morbius subconscious mind, enhanced by the "plastic educator", can utilize the Great Machine, recreating the Id monster that killed the original expedition and attacked the C-57-Ds crew. Morbius refuses to accept this conclusion.

After Altaira declares her love for Adams in defiance of her fathers wishes, Robby detects the creature approaching. Morbius commands the robot to kill it, but Robby knows it is a manifestation of Morbius; his programming to never harm humans conflicts with Morbius command, shutting Robby down. The creature melts through the indestructible Krell metal doors of the laboratory where Adams, Altaira, and Morbius have now taken refuge. Morbius finally accepts the truth: the creature is an extension of his own mind, "his evil self". He then confronts the creature as it enters, but he is fatally injured. As Morbius dies, he has Adams unknowingly initiate an irreversible chain reaction within the Great Machine. He then warns that Adams and Altaira must be 100 million miles away within 24 hours. 

At a safe distance in deep space, Adams, Altaira, Robby, and the surviving crew witness the destruction of Altair IV on the starships main viewplate. Adams comforts Altaira on the loss of her Father as the C-57-D sets course for Earth.

==Cast==
{{multiple image
|direction=vertical
|width=190
|image1=Forbidden Planet.jpg|caption1=Near the ship, First Officer Lt. Jerry Farman converses with Dr Morbius daughter, Altaira.
|image2=Forbidden Planet 2.jpg|caption2=The crew sets up Quinns Jury rig|jury-rigged communications circuits (Ostrow in the middle, Adams and Quinn on the right).
}}
 
* Walter Pidgeon as Dr. Edward Morbius
* Anne Francis as Altaira "Alta" Morbius
* Leslie Nielsen as Commander John J. Adams
* Robby the Robot as Himself
* Warren Stevens as Lt. "Doc" Ostrow Jack Kelly as Lt. Jerry Farman
* Richard Anderson as Chief Quinn
* Earl Holliman as Cook George Wallace as Steve
* Bob Dix as Grey
* Jimmy Thompson as Youngerford
* James Drury as Strong
* Harry Harvey, Jr. as Randall
* Roger McGee as Lindstrom
* Peter Miller as Moran Morgan Jones as Nichols
* Richard Grant as Silvers
* Frankie Darro, the stuntman inside Robby the Robot (uncredited) Marvin Miller, voice of Robby the Robot (uncredited)
* Les Tremayne as the Narrator (uncredited)
* James Best as a C-57-D crewman (uncredited)
* William Boyett as a C-57-D crewman  (uncredited)
 

==Production==
{{multiple image
|direction=vertical
|align=left
|width=300
|image1=FPcapSaucer.jpg|caption1=United Planets Cruiser C-57-D lands on Altairs 4th planet.
|image2=Forbidden Planet Id Monster1.JPG|caption2=Id Monster&nbsp;– a plaster cast of its footprint; the invisible creature outlined by force field and blaster rays.
}}

The screenplay by Irving Block and  . An Earth expedition headed by John Grant was sent to the planet to retrieve Dr. Adams and his daughter Dorianne, who had been stranded there for twenty years. From then on, its plot is roughly the same as that of the completed film, though Grant is able to rescue both Adams and his daughter and escape the invisible monster stalking them. 
 film sets Culver City film lot and were designed by Cedric Gibbons and Arthur Longeran. The film was shot entirely indoors, with all the Altair IV exterior scenes simulated using sets, visual effects, and matte paintings.
 cyclorama featuring the desert landscape of Altair IV; this one set took up all of the available space in one of the Culver City sound stages. Principal photography took place from April 18 to late May 1955.  

 

Later, C-57-D models, special effects shots, and the full-size set details were reused in several different episodes of the television series The Twilight Zone (1959 TV series)|The Twilight Zone, which were filmed by CBS at the same MGM studio location in Culver City.

At a cost of roughly $125,000, Robby the Robot was very expensive for a single film prop at this time.      Both the electrically controlled passenger vehicle driven by Robby and the truck/tractor-crane off-loaded from the starship were also constructed specially for this film. Robby the Robot later starred in the science fiction film The Invisible Boy and appeared in many TV series and films that followed; like the C-57-D, Robby (and his passenger vehicle) appeared in various episodes of CBS The Twilight Zone, usually slightly modified for each appearance.
 animated sequences Leo the Lion, seen at the very beginning of Forbidden Planet and the studios other films of the era.

==Reception==
Forbidden Planet was first released across the U. S. on April 1, 1956 in CinemaScope, Metrocolor, and in some theaters, stereophonic sound, either by the magnetic or Perspecta processes.  The Hollywood premiere was held at Graumans Chinese Theatre, and Robby the Robot was on display in the lobby. Forbidden Planet ran every day at Graumans Theater through the following September. 

According to MGM records the film initially earned $1,530,000 in the US and Canada  and $1,235,000 elsewhere resulting in a loss of $210,000. 

Forbidden Planet was re-released to movie theaters during 1972 as one of MGMs "Kiddie Matinee" features; it was missing about six minutes of film footage cut to ensure it received a "G" rating from the Motion Picture Association of America.  Later video releases carry a "G" rating, though they are all the original theatrical version.

===Home media===
Forbidden Planet was first released in the   next released the film on DVD in 1999 (MGMs catalog of films had been sold in 1988 to AOL-Time Warner by Turner Entertainment and MGM/UA). Warners release offered both cropped and widescreen picture formats on the same disc.

 For the films 50th anniversary, the Ultimate Collectors Edition was released on November 28, 2006 in an over-sized red metal box, using the original movie poster for its wraparound cover. Both DVD and high definition   edition of Forbidden Planet was released on September 7, 2010.

==Novelization== first person narrations by Dr. Ostrow, Commander Adams, and Dr. Morbius.  The novel delves further into the mysteries of the vanished Krell and Morbius relationship to them. In the novel he repeatedly exposes himself to the Krells manifestation machine, which (as suggested in the film) boosts his brain power far beyond normal human intelligence. Unfortunately, Morbius retains enough of his imperfect human nature to be afflicted with hubris and a contempt for humanity. Not recognizing his own base primitive drives and limitations proves to be Morbius downfall, as it had for the extinct Krell. While not stated explicitly in the film (although the basis for a deleted scene first included as an extra with the Criterion Collections LaserDisc set and included with both the later 50th anniversary DVD and current Blu-ray releases), the novelization compared Altairas ability to tame the tiger (until her sexual awakening with Commander Adams) to the medieval myth of a unicorn being tamable only by a virgin.

The novel also raises an issue never included in the film: when Dr. Ostrow dissects one of the dead Earth-type animals, he discovers that its internal structure precludes it from ever having been alive in the normal biological sense. The tiger, deer, and monkey are all conscious creations by Dr. Morbius as companions ("pets") for his daughter and only outwardly resemble their Earth counterparts. Since the Krells Great Machine can project matter "in any form" it has the power to create life. Thus, the Krells self-destruction can be interpreted by the reader as a cosmic punishment for misappropriating the life-creating power of the universe. This is why Commander Adams says in his speech to Altaira "... we are, after all, not God."

The machine creations of the novel, however, can be said to break some canons established in the film. The Great Machine operated in real time and could not create lifeforms that were independent of its operators immediate will. Thus, Morbius would be tasked with re-imaging those animals any time they were needed, and there is no suggestion anywhere in the novel of this happening. Hence, the more plausible statement offered within the film: the tiger, the deer, and the monkey were the descendants of specimens brought to Altair IV from Earth.

Upon its publication in 1956, Anthony Boucher dismissed the novelization as "an abysmally banal job of hackwork." 

==Soundtrack== electronic film score; their soundtrack preceded the invention of the Moog synthesizer by eight years (1964).

Using ideas and procedures from the book,   and electrical engineer Norbert Wiener, Louis Barron constructed his own electronic circuits that he used to generate the scores "bleeps, blurps, whirs, whines, throbs, hums, and screeches".  Most of these sounds were generated using an electronic circuit called a "ring modulator". After recording the basic sounds, the Barrons further manipulated the sounds by adding other effects, such as reverberation and delay, and reversing or changing the speeds of certain sounds.   MovieGrooves. Retrieved: January 16, 2015. 
 David Rose single of Culver City during March 1956. His main title theme had been discarded when Rose, who had originally been hired to compose the musical score in 1955, was discharged from the project by Dore Schary sometime between Christmas 1955 and New Year’s Day.  The films original theatrical trailer contains snippets of Roses score, the tapes of which Rose reportedly later destroyed. 
 LP album for the films 20th anniversary; it was on their very own Planet Records label (later changed to Small Planet Records and distributed by GNP Crescendo Records). The LP was premiered at MidAmeriCon, the 34th World Science Fiction Convention, held in Kansas City, MO over the 1976 Labor Day weekend, as part of a 20th Anniversary celebration of Forbidden Planet held at that Worldcon; the Barrons were there promoting their albums first release, signing all the copies sold at the convention. They also introduced the first of three packed-house screenings that showed an MGM 35mm fine grain vault print in original CinemaScope and stereophonic sound. A decade later, in 1986, their soundtrack was released on a music CD for the films 30th Anniversary, with a six-page color booklet containing images from Forbidden Planet, plus liner notes from the composers, Louis and Bebe Barron, and Bill Malone. 

A tribute to the films soundtrack was performed live in concert by Jack Dangers, available on disc one of the album Forbidden Planet Explored.

===Track list===
The following is a list of compositions on the CD: 

# Main Titles (Overture)
# Deceleration
# Once Around Altair
# The Landing
# Flurry Of Dust&nbsp;– A Robot Approaches
# A Shangri-La In The Desert / Garden With Cuddly Tiger
# Graveyard&nbsp;– A Night With Two Moons
# "Robby, Make Me A Gown"
# An Invisible Monster Approaches
# Robby Arranges Flowers, Zaps Monkey
# Love At The Swimming Hole
# Morbius Study
# Ancient Krell Music
# The Mind Booster&nbsp;– Creation Of Matter
# Krell Shuttle Ride And Power Station
# Giant Footprints In The Sand
# "Nothing Like This Claw Found In Nature!"
# Robby, The Cook, And 60 Gallons Of Booze
# Battle With The Invisible Monster
# "Come Back To Earth With Me"
# The Monster Pursues&nbsp;– Morbius Is Overcome
# The Homecoming
# Overture (Reprise)  

==In popular culture==
In the authorized biography of Star Trek creator Gene Roddenberry, Roddenberry notes that Forbidden Planet "was one of   inspirations for Star Trek." Alexander 1996   

The robot B9 and set of the spaceship Jupiter 2 in the TV series Lost in Space were inspired by Forbidden Planet and designed by Robert Kinoshita, creator of Robby the Robot.
 
Elements of the Doctor Who serial Planet of Evil were consciously based on the 1956 film. 

Forbidden Planet is named alongside several other classic science fiction films in the opening song "Science Fiction Double Feature" from The Rocky Horror Picture Show.

The U. K. musical  . Retrieved: January 16, 2015. 

A scene from the science fiction TV series Babylon 5, set on the Epsilon III Great Machine bridge, strongly resembles the Krells great machine. While this was not the intent of the shows producer, the special effects crew, tasked with creating the imagery, stated that the Krells machine was a definite influence on their Epsilon III designs. 

In Strata (novel)|Strata, an early Terry Pratchett novel, Silver, a bear-like alien – mentions portraying the Id Monster in a remake of Forbidden Planet.

In Gremlins (1984), during the scene where the father, Randall Peltzer, is phoning home from the inventors conference, Robbie the Robot can be seen walking in the background and speaking some of his lines from Forbidden Planet.

In a scene from Atlantis, the Lost Continent various props from Forbidden Planet are to be seen. The gauges from the Krell Lab, and also parts of the great machine are used to form a force field around the ship.

In a scene in the 1999 animated movie, The Iron Giant, while Hogarth salutes himself in the mirror before going off at night to look for the giant, a poster for Forbidden Planet can be seen in the reflection of said mirror.
 

==Accolades==
The film appeared on two American Film Institute Lists.
* AFIs 100 Years of Film Scores&nbsp;– Nominated 
* AFIs 10 Top 10&nbsp;– Nominated Science Fiction Film 

==Possible remake== limbo or industry turnaround.

==References==
Notes
 

Citations
 

Bibliography
 
* Alexander, David. Star Trek" Creator: Authorized Biography of Gene Roddenberry. London: Boxtree, 1996. ISBN 0-7522-0368-1.
* Booker, M. Keith. Historical Dictionary of Science Fiction Cinema. Metuchen, New Jersey: Scarecrow Press, Inc., 2010. ISBN 978-0-8108-5570-0.
* Lev, Peter. Transforming the Screen, 1950–1959. History of the American Cinema 7. Oakland, California: University of California Press, 2006. ISBN 0-520-24966-6.
* Miller, Scott.   Sex, Drugs, Rock & Roll, and Musical Theatre. Boston: Northeastern University,  2011. ISBN 978-1-5555-3743-2.
* Prock, Stephan.   Journal of the Society for American Music, 8.3 (2014): 371-400. 
* Ring, Robert C. Sci-Fi Movie Freak.  Iola, Wisconsin: Krause Publications, a division of F+W Media, 2011. ISBN 978-1-4402-2862-9.
* Stuart, W. J. Forbidden Planet (A Novel), New York: Farrar, Strauss and Cudahy, 1956.
* Wierzbicki, James. Louis and Bebe Barrons Forbidden Planet: A Film Score Guide. Metuchen, New Jersey: Scarecrow Press, 2005. ISBN 0-8108-5670-0
* Wilson, Robert Frank. Shakespeare in Hollywood, 1929–1956. Madison, New Jersey: Fairleigh Dickinson University Press, 2000. ISBN 0-8386-3832-5.
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  
* "  in  
*  
* " 

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 