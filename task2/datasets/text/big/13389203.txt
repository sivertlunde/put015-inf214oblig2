A Misappropriated Turkey
 
{{Infobox film
| name           = A Misappropriated Turkey
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         = Edward Acker Charles West
| music          =
| cinematography = G. W. Bitzer
| editing        =
| distributor    =
| released       =  
| runtime        = 17 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}

A Misappropriated Turkey is a 1913 American drama film directed by  D. W. Griffith.

==Cast== Charles West - The Striker
* Claire McDowell - Mrs. Fallon
* Edna Foster - The Strikers Son Harry Carey - The Bartender John T. Dillon - Union Member
* Frank Evans - Union Member
* Robert Harron - Union Member
* Walter P. Lewis - Union Member
* Charles Hill Mailes - Union Member Joseph McDermott - Union Member/In Bar
* W. Chrystie Miller - Union Member
* Jack Pickford - On Street

==See also==
* Harry Carey filmography
* D. W. Griffith filmography

==External links==
* 

 
 
 
 
 
 
 
 
 

 