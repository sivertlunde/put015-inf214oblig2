Killers (2014 film)
 
{{Infobox film
| name           = Killers
| image          =  
| caption       = Killers movie poster on 2014 Sundance Film Festival
| director       = Mo Brothers
| producer       = Yoshinori Chiba Shinjiro Nishimura Takuji Ushiyama Kimo Stamboel Timo Tjahjanto
| executive producer
| writer         = Takuji Ushiyama Timo Tjahjanto
| starring       = Kazuki Kitamura Oka Antara Rin Takanashi Luna Maya
| music          = Fajar Yuskemal Aria Prayogi
| cinematography = Gunnar Nimpuno
| editing        = Arifin Marhan Japri
| studio         = Nikkatsu Guerilla Merah Films Damn Inc. Media Prima Productions PT. Merantau Films Million Pictures Holy Bastards
| distributor  = XYZ Films
| released       =  
| runtime        = 137 minutes
| country        = Japan Indonesia
| language       = Japanese Indonesian English
}}
Killers ( , "Kirazu") is a 2014 Japanese-Indonesian psychological thriller film directed by Indonesian director duo The Mo Brothers. This film marks the first collaboration in the thriller genre between Japan and Indonesia. The story was written by Takuji Ushiyama with Timo Tjahjanto of The Mo Brothers.

==Plot==
Nomura Shuhei is a young, charismatic, and charming Japanese executive based in Tokyo who has a dark side nobody knows. He kills people and has been recording the action in videos. Nomura uploads videos of his killing on the Internet in order to be seen by everyone.

On the other side of the earth is Bayu Aditya, an ambitious Jakarta-based journalist. His obsession to uncover a story of a politician named Dharma makes his marriage and career fall apart.

In a desperate condition, Bayu sees one of Nomuras videos and begins to raise questions about Nomuras life. The unreasonable acts inspired Bayu to find out Nomuras reason for killing. This eventually leads him to follow the dark path to become a serial killer himself. Bayu begins to form his own killer-persona and tries to kill in the name of justice, record his act, and anonymously upload the video on the internet, similar to Nomuras modus operandi.

Unexpectedly, they are eventually connected by the Internet, Nomura is intrigued when he saw Bayus video and responds. The psychological bond between them grows and becomes more complicated when Nomura decides to meet Bayu personally. 

==Cast==
* Kazuki Kitamura as Nomura Shuhei, the Japanese sociopath
* Oka Antara as Bayu Aditya, an ambitious Indonesian Journalist
* Rin Takanashi as Hisae Kawahara, a florist who becomes a friend of Nomura
* Luna Maya as Dina Aditya, Bayus wife
* Ray Sahetapy as Dharma
* Ersya Aurelia as Elly Aditya
* Epy Kusnandar as Robert
* Mei Kurokawa as Midori
* Denden as Jyukais father
* Motoki Fukami as Officer Muroi
* Tara Basro as Dewi
* Dimas Argobie as Dharmas son

==Release==
Killers premiered at   by Tiberius Film GmbH, France by Wild Side Films, Turkey by Calinos Films, Hong Kong by Sundream Motion Pictures, United Kingdom by Lionsgate, North America by Well Go USA, and Australia & New Zealand by Vendetta Films. 

There are few differences between the Indonesian theatrical version with the Japanese and International theatrical version. The Indonesian version has been edited, with a mild violence and nudity scene being removed or softened due to the graphic nature. The Japanese and International theatrical version contains scenes omitted from the Indonesian version. Well Go USA released the film in a limited screening in January 23, 2015  and on DVD & Blu-Ray on April 7, 2015. 

==Production notes==
In a behind-the-scenes interview on the official movie video channel  , The Mo Brothers (Kimo Stamboel & Timo Tjahjanto) revealed that the original movie title had been Killer Clowns. The concept for the story line involved two masked serial killers from Japan and Indonesia, and their competition to become the best killer representing their countries.

==Award & achievement==
* Official Selection of Sundance Film Festival 2014 

==References==
 

==External links==
*  
*  
*   
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 