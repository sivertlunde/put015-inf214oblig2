Chaitanya (film)
{{Infobox film
| name           = Chaitanya
| image          = Chaitanya.jpg
| caption        =
| writer         = Singeetam Srinivasa Rao  
| story          = Prathap K. Pothan
| screenplay     = Prathap K. Pothan 
| producer       = Sathyam Babu
| director       = Prathap K. Pothan
| starring       = Akkineni Nagarjuna   Gautami 
| music          = Ilaiyaraaja
| cinematography = Rajiv Menon
| editing        = B. Lenin   V. T. Vijayan
| studio         = Sri Tirumalesa Productions
| released       =  
| runtime        = 130 minutes
| country        =   India
| language       = Telugu
| budget         =
| gross          =
| preceded_by    = 
| followed_by    = 
}}
 Telugu road movie, produced by Sathyam Babu on Sri Tirumalesa Productions banner, written and directed by Prathap K. Pothan. Starring Akkineni Nagarjuna, Gautami in lead roles and music composed by Ilaiyaraaja. The film recorded flop at box-office.   
 
==Plot==
Chaitanya, (Akkineni Nagarjuna) is the chief mechanic for the upcoming Roots Challenge 1991 Automotive rally race – which starts via the route from Chennai coast to Goa coast, a route which is noted for illegal drug trade and arms trafficking. Retired Major Harischandra Prasad (Girish Karnad) is the chief guest for the rally, his daughter Padmini (Gautami) also competes for the Roots Challenge Trophy.

Chaitanyas journalist friend Sudhakar (Chinna) witnesses the murder of the city D.I.G. in a park. The D.I.G.s murder happens to have been planned by a high profile unknown criminal. Later, Sudhakars murdered by a gang near his garage because he witnessed the murder of the D.I.G. Chaitanya finds himself beaten by the gang after he comes to the assistance of Sudhakar. When he regains his senses he finds himself with a revolver in his hand and in police custody, arrested and charged with the homicide of the friend he tried to assist. With evidence pointing toward his guilt, he has virtually no defense and may well spend the rest of his life in prison or be hanged. He manages to escape from the prison with the help of Chaitanyas garage assistant, Golconda (a drunkard), through the journalists friend (Joseph) in a water tanker. Disguised as the journalists friend, the high profile criminals henchman is hidden in the tanker.

The police commissioner (Kota Srinivasa Rao), who is a close associate of Harischandra Prasad, attempts to solve the mysterious murder of D.I.G. and the journalist.

The criminals henchman plans to blast the water tanker and kill Chaitanya who flees from Srungavarapukota prison in the same water tanker. But, after a battle with the henchman inside the water tanker, Chaitanya escapes and jumps from the water tanker into a lake and the henchman dies in the water tanker explosion. Police send Chaitanyas case to CBI as they come to the conclusion that it was Chaitanya who was killed in the explosion.

Chaitanya gets to participate in the one halt race by joining at a mile stone on the route, in the guise of the garage assistants race car. He finds out that the photographs of D.I.G.s murder caught by his journalist friend are hidden in one of the three cars of the rally. In another twist, Padmini happens to drive the car in which 1 photograph was hidden.

How Chaitanya unweaves the mystery behind all this crime and how Padmini & Chaitanya fall in love in the process and identify the high profile criminal adds to the rest of the plot and suspense in the film. The D.I.G. was killed by three people. According to the pictures, the guy in the brown coat is the police commissioner, who claims to be the mafia boss aka  . Police commissioner forces Harischandra & Padmini to join the mafia. Chaitanya manages to reach the commissioners safehouse, where its revealed that Harischandras the high profile criminal. Commissioner didnt know that someone else was the mafia boss. Harischandra kills the commissioner. When Chaitanya shows up, he realizes that Padminis father is the high profile criminal who is responsible for illegal drug trade, arms trafficking, DIGs murder, Sudhakars murder, & ordered his gang to frame Chaitanya.

==Cast==
 
*Akkineni Nagarjuna as Chaitanya
*Gautami as Padmini
*Girish Karnad as Harischandra Prasad
*Kota Srinivasa Rao as Commissioner KJ Prabhakar
*Raghuvaran as Raana
*Babu Antony as Cobra
*Silk Smitha as Smitha Chinna as Journalist Sudhakar
*Nizhalgal Ravi as Smuggler
*Chinni Jayanth as Smuggler
*Ravi Teja as Racer
*Suthi Velu as Detective
*Raavi Kondala Rao as Rules Rally Company MD 
*P. J. Sarma 
*Vijayachander as DIG Ram Mohan Rao
*Bhimeswara Rao as Jailor
*KK Sarma  as Police Officer
*Telephone Satyanarayana  as Doordharshan Reporter 
 

==Soundtrack==
{{Infobox album
| Name        = Chaitanya
| Tagline     = 
| Type        = film
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1991
| Recorded    = 
| Genre       = Soundtrack
| Length      = 24:35
| Label       = Echo Music
| Producer    = 
| Reviews     =
| Last album  = Nirnayam (1991 film)|Nirnayam   (1991) 
| This album  = Chaitanya   (1991)
| Next album  = Surya IPS   (1991)
}}

Music composed by Ilaiyaraaja. Lyrics written by Veturi Sundararama Murthy. All songs are hit tracks. Music released on ECHO Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 24:35
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Oho Laila SP Balu
| length1 = 4:56

| title2  = Kanne Lady
| extra2  = S. P. Sailaja
| length2 = 4:57

| title3  = Paapa Eddu SP Balu
| length3 = 4:51

| title4  = Sweety SP Balu,S. Janaki
| length4 = 4:55

| title5  = Vayase Tholi SP Balu,S. Janaki
| length5 = 4:53
}}

==References==
 

==External links==
*  

 
 
 
 
 
 