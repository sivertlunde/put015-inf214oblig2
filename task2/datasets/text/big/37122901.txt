City of Bad Men
{{Infobox film
| name           = City of Bad Men
| image          = City of Bad Men trailer 1.jpg
| caption        = Dale Robertson and Jeanne Crain in film scene
| director       = Harmon Jones
| producer       =
| writer         = George W. George George F. Slavin
| based on       =
| starring       = Dale Robertson Jeanne Crain Richard Boone Lloyd Bridges
| music          =
| cinematography = Charles G. Clarke
| editing        = George A. Gittens
| studio         = 20th Century-Fox
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $740,000. 
| gross          =
}} Western film starring Jeanne Crain, Dale Robertson and Richard Boone.

==Plot==
A heavyweight championship fight between James J. Corbett and Bob Fitzsimmons is coming to Carson City, Nevada at exactly the same time outlaw Brett Stanton and brother Gar return to town.

Away for six years, Brett has alienated his former love, Linda Culligan, who is now involved with Jim London, the fights promoter. Londons sister, Cynthia Castle, tries to attract Bretts interest, but hes only got eyes for Linda.

Because the bout could sell as much as $100,000 in tickets, law-breakers like Johnny Ringo are also milling around, keeping Sheriff Bill Gifford on his toes. He ends up asking Brett to be a deputy, just for the week of the fight, not knowing Bretts actually scheming to rob the proceeds himself.

A fight-day decision by Linda to end her engagement to Jim changes the plans of Brett, who decides to go straight. Alas, his brother Gar betrays him to Ringo, who goes through with the daring robbery. Brett has to exchange gunfire with both Ringo and Gar, and when hes successful, Linda returns to his arms.

==Cast==
*Jeanne Crain as Linda Culligan
*Dale Robertson as Brett Stanton
*Richard Boone as Johnny Ringo
*Lloyd Bridges as Gar Stanton
*Carole Mathews as Cynthia Castle
*Carl Betz as Deputy Phil Ryan
*Whitfield Connor as Jim London
*Hugh Sanders as  Sheriff Bill Gifford
*Rodolfo Acosta as Mendoza
*Pascual García Peña as Pig Harry Carter as Jack
*Robert Adler as Barney

==Home video==
City of Bad Men was released in 2014 as a "publish on demand" DVD in the United States. 

==In Popular Culture== USA network series Graceland (TV series)|Graceland, the main character Mike (who is an undercover FBI agent) has a conversation with one of this targets, the ruthless Nigerian crime lord "Bello" (played by actor Gbenga Akinnagbe), where at one point the film is mentioned after Mike quotes a line from the film. Bello admits hes impressed with this knowledge of the film and admits hes a fan. Bello even shows Mike a copy of the film on DVD in a video store later on in the episode.

==References==
 

==Further reading==
*  A rare, recent review of the film by a blogger whos working on a book about 1950s western films.

==External links==
 
*  at IMDB
*  at TCMDB

 
 
 
 
 
 


 