We Are Together
{{Infobox Film
| name           =We Are Together
| image          =We Are Together (film).jpg
| image_size     = 
| caption        =  Paul Taylor
| producer       = 
| writer         = Slindile Moya, Paul Taylor
| starring       =
| music          = Dario Marianelli
| cinematography = 
| editing        = Masahiro Hirakubo, Ollie Huddleston
| distributor    = 
| released       = 2006
| runtime        = 83 min.
| country        =  Zulu
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 British documentary film about the orphanage "Agape" in South Africa. Children, who live here, lost their parents to AIDS. The film is full of songs, both native and English. Eventually the children get a chance to visit New York and raise money by singing for the public.

The film has won eight awards. 

== Cast ==
*Lorraine Bracco as Herself
*Alicia Keys as Herself
*Mbali as Herself
*Mthobisi Moya as Himself
*Nonkululeko Moya as Herself
*Sifiso Moya as Himself
*Slindile Moya as Herself
*Swaphiwe Moya as Herself
*Grandma Zodwa Mqadi as Herself
*Paul Simon as Himself
*Kanye West as Himself

== Awards ==
*Standard Life Audience Award, Edinburgh International Film Festival 2007
*Cadillac Audience Award, Tribeca Film Festival 2007
*Special Jury Prize, Tribeca Film Festival 2007
*Audience Award, Amnesty International Film Festival 2007
*All Rights Award, Amnesty International Film Festival 2007
*Special Jury Prize, One World International Film Festival 2007
*Audience Award, IDFA Amsterdam 2006
*First Appearance Award, IDFA Amsterdam 2006

== References ==
 
 

== External links ==
*   (archive)
*  
*  
*  

 
 
 
 
 
 
 
 
 


 