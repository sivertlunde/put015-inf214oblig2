Chicken and Duck Talk
 
 
{{Infobox film
| name           = Chicken and Duck Talk
| image          = 
| caption        = 
| director       = Clifton Ko
| producer       = Ronny Yu Michael Hui (executive)
| story          = Jo Ma James Yuen
| screenplay     = Michael Hui Clifton Ko
| starring       = Michael Hui Ricky Hui Sylvia Chang Lowell Lo Lawrence Ng
| music          = Richard Yuen
| cinematography = Derek Wan
| editing        = Wong Yee Shun
| distributor    = Huis Film Production Co.
| released       =  
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
| film name = {{Film name| traditional    = 雞同鴨講
| simplified     = 鸡同鸭讲}}
}} 1988 film co-written by Michael Hui, who also starred.  It was directed and co-written by Clifton Ko.  The film deals with the conflict that ensues between the proprietor of a BBQ duck restaurant that is in trouble for health violations and the fast food chicken restaurant that opens across the street.  The film was the highest-grossing Hong Kong film released in 1988.  Huis screenplay and performance won him several awards, including a special award given by the American Film Institute in 1989.

The film also includes a celebrity cameo by Sam Hui (Michaels younger brother), as the master of ceremonies at the grand opening of Dannys Chicken, and the screen debut of Gloria Yip in a brief appearance as Huis sons school friend.  Sam Hui (playing himself) speaks the title phrase to cut his speech short.

The title is a Chinese idiom for people not understanding each other.  

==Plot==
Hui Kei (Michael Hui) is the struggling proprietor of a BBQ duck restaurant in Hong Kong. At the beginning of the film, Hui is visited by a health inspector, who found a cockroach in his soup and other unsanitary conditions, and threatens to sue the restaurant. Unfortunately, Huis staff stops him from leaving with the soup sample after a slipup from the inspector. While the restaurant is typically operated on ground floor, Hui lives upstairs and awaits a visit from his aristocratic mother-in-law (Pak Yan), who is convinced that her daughter, Ah Kuen (Sylvia Chang), has married a good-for-nothing husband who cares for no one.

Later, Hui unknowingly faces new trouble when a genius businessman, Danny Poon (Lawrence Ng) opens what is to be the first of a chain of Dannys Chicken fast food fried chicken restaurants across the street from him. Not long after, Dannys Chicken becomes the talk across Hong Kong, attracting young customers and families and causing Huis business to plummet. When Hui has a frank discussion meeting with his fellow staff, in regards to his business and its working conditions, one of his employees, Cuttlefish (Ricky Hui) gets fed up with Huis abuse and guilt-tripping about his dead father and struggling mother, and plans to work for Dannys Chicken. However, Cuttlefish discovers while being trained over at Dannys Chicken that Poons regimen was insane, strict, and humiliating, where much of his job involves appearing in a chicken suit on the street, much to Huis disgust. Huis mother-in-law offers to invest in Huis dwindling business, to which Hui vehemently rejects. After his son tells him that his best friend, Judy (Gloria Yip), likes its food and its atmosphere, Hui disguises himself as an Indian woman to infiltrate his rivals restaurant and tries Dannys Chicken. Hui is convinced that it is only the packaging that people like, but his disguise is quickly undone by Poon himself.

Hui transforms his new restaurant to match Dannys Chickens "packaging" atmosphere, from tabletops to nightclub singing, with little success. Hui also begins parading the street in a makeshift duck costume, leading to a physical fight with Cuttlefish in his chicken suit that nearly gets Hui and Cuttlefish arrested. Huis business was attracting no customers at this point, and even his most regular of customers, a Buddhist nun who frequently orders vegetarian noodles with no lard finds Dannys Chicken accommodating to her needs. After a series of rat infestations, Cuttlefishs reluctant return, notices of closure, and an attempted buyout from Poon, Hui resorted to his mother-in-laws help to renovate his restaurant completely before she leaves. Business finally began to boom for Hui once again, while Dannys Chicken slowly dwindles to the point of a disastrous attempt to market BBQ ducks. In a final act of desperation, Poon resorts to arson to end the competition, nearly killing himself before Hui and his family and staff eventually rescues Poon and sends Poon to the hospital for his oxygen surgery. Several times throughout the film, Hui talks about getting AIDS through sharing food.

==Reception==
A retrospective review in 2005 rated the film as "justly famous... well-made, it was a fresh look at ordinary urban Hong Kongers, and it’s funny." 

==Cast==
* Michael Hui
* Ricky Hui
* Sylvia Chang
* Lowell Lo
* Lawrence Ng
* Pak Yan
* Sam Hui
* Gloria Yip

 
 

==References==
 

==External links==
* 

 
 
 
 