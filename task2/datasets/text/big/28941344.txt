The Big Game (1942 film)
{{Infobox film
| name = The Big Game 
| image = 
| image_size =
| caption =
| director = Robert A. Stemmle
| producer = 
| writer = Toni Huppertz   Richard Kirn   Robert A. Stemmle
| narrator =
| starring = René Deltgen   Gustav Knuth   Heinz Engelmann
| music = Michael Jary  
| cinematography = Erich Rossel   Bruno Stephan
| editing = Ludolf Grisebach   Frieda Zillich
| studio =  Bavaria Film
| distributor = Deutsche Filmvertriebs 
| released =  10 July 1942
| runtime = 85 minutes
| country = Germany German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} German sports film directed by Robert A. Stemmle and starring René Deltgen, Gustav Knuth and Heinz Engelmann. It featured famous German footballers of the era. National coach Sepp Herberger arranged for many German international footballers to be recalled from fighting in the Second World War, obstensibly to improve the quality of the film, but actually to try to protect them from the horrors of war. 

Some sequences of the film are in Agfacolor. The sets were designed by art directors Gerbert Hochreiter and Walter Schlick.

==Partial cast==
* René Deltgen ...  Mittelstürmer Werner Fehling
* Gustav Knuth ...  Trainer Karl Wildbrandt
* Heinz Engelmann ...  Torwart Jupp Jäger
* Maria Andergast ...  Annemarie Sand
* Karl Schönböck ...  Fotograf Richter
* Josef Sieber ...  Vereins-Senior Vater Gabler
* Ruth Lommel ...  Verkäuferin Flora Tietze
* Wolfgang Staudte ...  Fritz Eysoldt
* Lucy Millowitsch ...  Frau Eysoldt
* Hans Appel ...  Player
* Fritz Balogh ...  Player
* Bauernfeind ...  Player
* Gerta Böttcher ...  Ilse Klebusch
* Tino Carocci ...  Otto Wendland
* Karl Ludwig Diehl ...  SS-Mann
* Erich Dunskus ...  Zuschauer beim Elfmeterschießen
* Hermann Eppenhoff ...  Player
* Adolf Fischer ...  Heini Gabler
* Albert Florath ...  Pförtner
* Ulla Gauglitz ...  Edith Schladebach
* Rudi Gellesch ...  Player
* Erich Goede ...  Player
* Graf ...  Player
* Paul Henckels ...  SA-Ordner
* Sepp Herberger ...  Zuschauer des Fußballspiels
* Hoffmann ...  Player
* Lucie Höflich ...  Wirtin Frau Klebusch
* Hilde Jansen ...  Grete Gabler
* Kästner ...  Player
* Herbert Klatt ...  Bruno Mielke

==Bibliography==
* Hesse-Lichtenbeger, Ulrich. Tor!: The Story of German Football. WSC Books, 2003.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 