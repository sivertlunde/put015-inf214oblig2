The Love of Siam
 
{{Infobox film
| name = The Love of Siam
| image = love_of_siam_poster.jpg
| image_size = 215px
| alt = 
| caption = Thai theatrical release poster
| director = Chookiat Sakveerakul
| producer = Prachya Pinkaew Sukanya Vongsthapat
| writer = Chookiat Sakveerakul
| starring = Witwisit Hiranyawongkul Mario Maurer
| music = Kitti Kuremanee
| cinematography = Chitti Urnorakankij
| editing = Lee Chatametikool Chukiat Sakweerakul
| studio = Baa-Ram-Ewe Sahamongkol Film International
| released =   
| runtime = 150 minutes 170 minutes (Directors Cut) 186 minutes (Full Length Version)
| country = Thailand
| language = Thai
| budget = ฿17,000,000
| gross  = ฿44,959,100 ($1,405,711)  . Boxofficemojo. Retrieved 2011-12-18. 
}} gay romance between two teenage boys.

The film was released in Thailand on November 22, 2007. The fact that the gay storyline was not apparent from the films promotional material initially caused controversy, but the film was received with critical acclaim and proved financially successful. It dominated Thailands 2007 film awards season, winning the Best Picture category in all major events,

==Plot==
Ten-year old Mew and Tong are neighbors. Mew is a soft-featured but stubborn child, while Tong is a more masculine, energetic boy who lives with his parents and sister, a Roman Catholic ethnic Chinese family. Tong wants to befriend Mew, but the quiet boy and his outgoing neighbor are not initially close. At school, effeminate Mew is teased by several other students and harassed until Tong steps in to defend him. Tong receives injuries and they then begin a friendship. Mew plays on his late grandpas piano and is joined by his grandma, who begins to play an old Chinese song. Mew asks his grandma why she liked this song and his grandma responds that it was played for her by his grandpa. She explains that one day, Mew will understand the meaning of the song.

Tongs family goes on vacation to Chiang Mai and his older sister, Tang, begs her mother to allow her to remain with friends several days longer. Tong buys Mew a present, deciding to give it to Mew piece by piece in a game similar to a treasure hunt, a tradition in his family. One by one, Mew finds all of the pieces except for the last one which is hidden in a tree. The tree is cut down just as Mew is about to retrieve it leaving the present Tong bought for Mew incomplete. Tong is disappointed at their misfortune, but Mew remains grateful for Tongs efforts.

Tongs parents are unable to contact Tang in Chiang Mai, and go there to look for her, fearing she may be lost in the mountains. Tong, devastated that his sister is missing, cries and Mew tries to comfort him.  Tongs parents are unable to find Tang, and the family decides to move to Bangkok. After giving Mew some parting words, Tong looks back to find Mew wiping his eyes with his sleeve as Tongs familys car drives away.

Six years pass. Tongs father Korn is a severe alcoholic, due to his guilt for losing his daughter. Tong has a pretty—but uptight—girlfriend, Donut. Tong and Mew are reunited during their senior year of high school at Siam Square. The musically talented Mew is the lead singer of a boy band called August. The meeting stirs up old feelings that Mew has harbored since boyhood: his love for Tong.
 alcoholic depression. Grace one at the dinner table. Amazingly to Sunee, June seems to already know much of the familys past.  June passes it off as her creative imagination.
 unrequited crush from an obsessive neighbour girl, Ying, who is trying to use a voodoo doll and other tricks to make Mew like her. Unfortunately for her, Mew is more interested in his boyhood friend Tong, who has become his inspiration for writing the new love song. Aod and Mews bandmates are impressed with Mews composition.

As part of the deception with "Tang," a backyard party is held in honor of her return, and Mews band August provides the entertainment. Singing the new love song for the first time in public, Mews eyes lock intensely with Tongs. After the party, when everyone has left, the two boys share a prolonged kiss. Unseen, Sunee accidentally witnesses their kiss. The next day, she firmly commands Mew to stay away from her son. When Tong finds out that his mother has interfered, an argument ensues, but her actions succeeded in creating a rift between the teens. Mew is heartbroken and loses his musical inspiration, so he quits the band.
 liver condition which sends him to a hospital.  While in the hospital, June questions the effectiveness of the "Tang" ruse, noting that Korn has not reduced his alcohol consumption.  She leaves, and does not answer when Korn asks her whether she will return for Christmas.  After she leaves, Korn starts eating more and begins takes his liver medication.

At Christmas time, as Tong and his mother are decorating their Christmas tree, he shows her how controlling she is. At this time Tong is hesitant about whether to pick a boy or a girl ornament to put on the tree and expresses this frustration by telling his mother that he doesnt want to make a mistake like last time and wind up having her upset with him again. Upon hearing this his mother takes and holds out the ornaments, each in one hand, and tells Tong to "choose what you think is best for yourself." With that Tong takes and places the boy ornament on the tree while exchanging a smile with his mom. June has saved money and heads off in a bus to Chiang Mai. It is not clear whether June and Tang were the same person.

Tong goes to Siam Square for a date with Donut. Mew has rejoined the band, and they are playing nearby. Tong abandons Donut, telling her they are no longer together. He then rushes to see Mew sing and is guided there by Ying, who has accepted the fact that Mew loves Tong. After the performance, Tong gives Mew his Christmas gift, the missing nose from the wooden doll that Tong gave him when they were children. Tong then tells Mew, "I cant be your boyfriend, but that doesnt mean I dont love you."

The film ends with Mew putting the missing nose back to the wooden puppet, saying "thank you" and crying quietly.

==Cast==
 
* Witwisit Hiranyawongkul as Mew
* Mario Maurer as Tong
* Kanya Rattanapetch as Ying
* Chanidapa Pongsilpipat (Aticha Pongsilpipat) as Donut
* Chermarn Boonyasak as Tang/June
* Sinjai Plengpanich as Sunee, Tongs mother
* Songsit Rungnopakunsri as Korn, Tongs father
* Jirayu La-ongmanee as young Tong
* Artit Niyomkul as young Mew

==Reception==

===Marketing controversy and audience response===
  gay aspect of the love story was controversial.

Thai-language web boards were posted with messages of support, as well as accusations by moviegoers that they were misled into watching "a gay movie." 

Writer/director Chookiat Sakveerakul admitted the film was marketed on the film posters and in the films previews as a straight romance because he wanted it to reach a wider audience. 

"The movie is not all about gay characters, we are not focusing on gay issues, we are not saying, lets come out of the closet, so obviously, we dont want the movie to have a gay label," he said in an interview. 

But the director confirmed the mixed reaction of audiences. "I went incognito to a movie theater and observed the audience. I didnt expect such a strong reaction. Maybe I was just too optimistic that homophobia in Thai society had subsided." 

===Commercial performance=== Thai cinemas on November 22, 2007, opening on 146 screens. It was the No. 1 film at the Thai box office that weekend, topping the previous No. 1 film, Beowulf (2007 film)|Beowulf.  It slipped to No. 2 the following weekend, unseated by the comedy film, Ponglang Amazing Theater.  In the third week of release, it had dropped to No. 5, with to-date box office takings of US$1,198,637.  It has grossed a total of US$1,305,125 to date.

The film was officially released in Taiwan on September 19, 2008, Japan on May 20, 2009, and in Singapore on July 16, 2009. It reached No. 12 at the Taiwan box office in opening week. 

===Critical response===
The Love of Siam was received with critical acclaim upon its release.

Bangkok Post film critic Kong Rithdee called the film "groundbreaking", in terms of being the first Thai film "to discuss teenagers sexuality with frankness". He praised the mature, realistic family drama aspects of the film, as well as the solid performances, particularly by Sinjai Plengpanich as the mother Sunee. 

Another Bangkok Post commentator, Nattakorn Devakula, said the film contained important lessons for Thai society. "The point that the film attempts to teach viewers – and a largely conservative Thai society – is that love is an evolved form of emotional attachment that transcends sexual attraction of the physical form." 
 The Nation called the film "brilliantly conceived". 

A few critics found fault with the film, among them Gregoire Glachant of BK magazine, who commented that "The Love of Siam isnt a very well shot movie. Chookiats camera only records his dull play with equally dull angles and light as it wanders from homes to schools, to recording studio, and to Siam Square without sense of purpose or directions." 

The movie also reached a rating of 8.0 (out of 10) on the Internet Movie Database. 

===Accolades===
 
The Love of Siam dominated Thailands 2007 film awards season, winning the Best Picture category in all major national film award events, including the Thailand National Film Association Awards, Starpics Magazines Starpics Awards, the Bangkok Critics Assembly Awards, Star Entertainment Awards, and Kom Chad Luek Newspapers Kom Chad Luek Awards.  Awards won by the film include the following:

{|
|style="vertical-align:top"|Starpics Awards
*Best Picture
*Best Director (Chookiat Sakveerakul)
*Best Actor (Mario Maurer)
*Best Actress (Sinjai Plengpanich)
*Best Supporting Actor (Songsit Rungnopakunsri)
*Best Screenplay (Chookiat Sakveerakul)
*Best Cinematography (Chitti Urnorakankij)
*Best Original Score (Kitti Kuremanee)
*Popular Film.

Kom Chad Luek Awards
*Best Picture
*Best Actress (Sinjai Plengpanich)

Thailand National Film Association Awards
*Best Picture
*Best Director (Chookiat Sakveerakul)
*Best Supporting Actress (Chermarn Boonyasak)
|style="vertical-align:top"|Bangkok Critics Assembly Awards
*Best Picture
*Best Director (Chookiat Sakveerakul)
*Best Actress (Sinjai Plengpanich)
*Best Supporting Actress (Chermarn Boonyasak)
*Best Screenplay (Chookiat Sakveerakul)
*Best Original Score (Kitti Kuremanee)

Star Entertainment Awards
*Best Picture
*Best Director (Chookiat Sakveerakul)
*Best Actress (Sinjai Plengpanich)
*Best Supporting Actress (Chermarn Boonyasak)
*Best Screenplay (Chookiat Sakveerakul)
*Best Original Song

Osaka Asian Film Festival 2009
*Audience Award  
|}

The film was also nominated for Best Supporting Actor (Mario Maurer) and Best Composer (Kitti Kuremanee) categories in the Asian Film Awards at the Hong Kong International Film Festival, but did not win. 

In October 2008, Mario Maurer won the Best Actor award in Southeast Asian film category at the 10th Cinemanila International Film Festival. 
 submission to the 81st Academy Awards. 

==Production==
 
The film was first shot on December 26, 2006,  taking advantage of the Christmas lights and decorations of Siam Square and the surrounding area. 
 Thai films in many respects. First, at 150 minutes, the film is markedly longer than most other Thai films, and second it is a drama film, which is rare in the Thai industry, which mainly produces horror, comedy, action, and (heterosexual) teen romance films. Director Sakveerakul said he felt the longer running time was needed to more fully develop all the characters and the story. He received full backing for this decision from producer Prachya Pinkaew and the production company, Sahamongkol Film International.  "They liked the first cut, which was even longer, so I didnt need to convince them that much. I feel that every minute of the movie is important, and Im glad the audience will be able to see it in full," Chookiat said in an interview before the films release. Rithdee, Kong. November 16, 2007. "Romancing the Square", Bangkok Post, Real Time, Page R1 (print edition; online version available only to subscribers after seven days).  A nearly three-hour "directors cut" was released in January 2008 exclusively at the House RCA cinema, and it played for several weeks of sold-out shows.

The film was a departure for Chookiat, who had previously directed the horror film, Pisaj and the psychological thriller, 13 Beloved.
 Thai films with gay characters, gay men are coarsely depicted as transgenders or transvestites with exaggerated effeminacy.

The young actors portraying Mew and Tong both had difficulties with the kissing scene.  Mario Maurer, who portrayed Tong, was nervous about the role. "Ive never kissed a man and kissing is not something you do every day," he said in an interview. "My father said it was just a job and not to think about it too much." 

Witwisit Hiranyawongkul, who portrays Mew, accepted the role because it was challenging and because he was interested in working with the director, who was a senior classmate at Montfort College in Chiang Mai. 

==Soundtrack==
{{Infobox album  
| Name        = The Love of Siam Original Soundtrack
| Type        = soundtrack
| Artist      = Various Artists
| Cover       = Love_of_Siam_soundtrack.jpg
| Cover size  = 215px
| Released    =  
| Recorded    =  Thai pop
| Length      = 45:20
| Label       = Sahamongkol Film International
| Producer    = Chookiat Sakveerakul
| Reviews     = 
}}
An original soundtrack album was released on November 12, 2007, ahead of the films release. The two-disc package features a CD with music tracks by Chookiat Sakveerakul, Witwisit Hiranyawongkul, the August band, Passakorn Wiroonsup and Flure, and a VCD. The album proved popular, and had sold out of many shops in the weeks after its release.    "Gun Lae Gun" spent seven weeks at number one on Seed 97.5 FMs charts. 

Tagline: "Just ask yourself who you think of when you are listening to love song."

;Disc 1 (CD)
# "Gun lae gun" ("กันและกัน") – performed by Suweera Boonrod (Flure) – 4:34
# "Ticket (Day Trip)" – performed by Chookiat Sakveerakul & August Band – 3:34
# "Roo suek barng mhai" ("รู้สึกบ้างไหม") (Live) – performed by Witwisit Hiranyawongkul – 4:16
# "Pieng ter" ("เพียงเธอ") – performed by Witwisit Hiranyawongkul – 4:12
# "Gun lae gun" ("กันและกัน")  (Acoustic version) – performed by Chookiat Sakveerakul – 6:20
# "Kuen aun pen niran" ("คืนอันเป็นนิรันดร์") – performed by Passakorn Wiroonsup – 3:07
# "Gun lae gun" ("กันและกัน")  (Live) – performed by Witwisit Hiranyawongkul – 6:02
# "Roo suek barng mhai" ("รู้สึกบ้างไหม ") (Original Mix) – performed by Witwisit Hiranyawongkul – 5:00
# "Pieng ter" ("เพียงเธอ") (Demo) – performed by Chookiat Sakveerakul – 4:23
# "Ticket (Night Trip)" (Bonus Track)  – Instrumental  – 3:50

;Disc 2 (VCD)
# The Love of Siam trailer
# "Gun lae gun" music video
# Introducing the August band
# "Gun lae gun" behind-the-scenes music video

==Home media==
The standard DVD was released on February 19, 2008 in anamorphic widescreen format with Thai Dolby Digital 5.1 and Dolby 2.0 Surround audio tracks and a commentary by the director. &nbsp; The DVD includes music videos, trailer and a photo gallery.

The three-disc directors cut DVD was released on April 9, 2008. Discs 1 and 2 contain the 173 minutes directors cut of the film. Disc 3 includes a trailer, a film documentary, deleted scenes, a "Making Of", character introductions, a live concert, an interview with the songwriter and the complete theatrical version with an on-screen commentary by the director.

In addition, an audio CD, a wooden doll, postcards, the letter and a note of Gun lae gun are featured in a limited DVD Boxset. 

The Love of Siam was released as a Collectors Edition 3-disc set in Taiwan on January 21, 2009. This edition duplicates the Thai three-disc directors cut DVD and adds an exclusive extra of the directors and stars reception in Taiwan. This is the only DVD of the directors cut that has English subtitles on both the film and the extras, except the Making Of which doesnt have any subtitles. 

All Thailand editions mentioned above are now out of print. A budget-price one disc version  was released in 2009,  accompanied by a two disc version of the directors cut. 

The Love of Siam (theatrical version) was released in the US on October 13, 2009.  The only extra is the trailer. This is the only DVD of the theatrical version that has English subtitles. 

==See also==
* List of lesbian, gay, bisexual, or transgender-related films by storyline

==References==
 

==External links==
 
*  
*  
*  
*   at SiamZone  

 
 
 
 
 
 
 
 
 
 
 