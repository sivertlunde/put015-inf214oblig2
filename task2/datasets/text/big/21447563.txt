Faustina (1957 film)
{{Infobox film
| name           = Faustina
| image          = 
| caption        = 
| director       = José Luis Sáenz de Heredia
| producer       = Eduardo de la Fuente José Luis Sáenz de Heredia
| writer         = Johann Wolfgang Goethe José Luis Sáenz de Heredia
| starring       = María Félix
| music          = 
| cinematography = Alfredo Fraile
| editing        = Julio Peña
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Faustina is a 1957 Spanish comedy film directed by José Luis Sáenz de Heredia. It was entered into the 1957 Cannes Film Festival.   

==Cast==
* María Félix as Faustina
* Fernando Fernán Gómez as Mogon
* Conrado San Martín as Capitán Batler
* Fernando Rey as Valentín
* Elisa Montés as Elena
* José Isbert as Cura
* Juan de Landa as Mefistófeles
* Tony Leblanc as Novio
* Tomás Blanco (actor)|Tomás Blanco as Dueño del cabaret
* Xan das Bolas as Limpio
* Santiago Ontañón as Don Fernando
* Carlos Martínez Campos
* Margot Prieto
* Rafael Bardem as Jurado (as Rafael Barden)
* Francisco Bernal as Empleado

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 