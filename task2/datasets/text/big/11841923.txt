Sound of the Sea
{{Infobox film
| name           = Sound of the Sea
| image          = Son de Mar.jpg
| caption        = Movie poster
| director       = Juan José Bigas Luna
| producer       = Andrés Vicente Gómez
| writer         = Manuel Vicent Rafael Azcona
| narrator       =
| starring       = Jordi Mollà Leonor Watling Eduard Fernández
| music          = Piano Magic
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 95 mins
| country        = Spain Spanish
| budget         =
}}
 Spanish drama drama / erotic film directed by Juan José Bigas Luna based on the novel of the same title by Manuel Vicent.
 Ulises (Jordi Mollà), who comes to a fishing village to teach literature at a local high school. During his stay he falls in love with Martina (Leonor Watling), the daughter of his landlord. Sierra (Eduard Fernández), a rich businessman, also falls in love with her and fruitlessly tries to win her heart.

==Plot== Valencia to teach literature. He rents a room at a local hostel and immediately falls in love with Martina, the landlords daughter as he sees her hanging up her clothes on the clothesline. A local businessman by the name of Sierra is also in love with her but she resists his advances. Martina invites Ulises to see the Son de Mar, a yacht on which a film had been made years before, when she was 13 years old. Her dream is to purchase it when she becomes  rich.
 Dido and Aeneas. The quote she is most infatuated with is:
:From the depths of the calm sea, two serpents surface. Above the waves their crista and chest emerge, the rest of their bodies under the surface. One of them holds me and suffocates me with the double ring of her love. And I try to escape from the knot her body makes.

Enchanted by his romantic quotations, amidst the scenery surrounding Valencia, she falls in love with Ulises and gets pregnant. They marry and have a son, Abel. One day Sierra invites the couple to a party, at which Ulises feels attracted to a woman in a red dress. The next day, he goes out on his boat to catch a tuna for Martina. During a storm, his boat is found, but he disappears. Presumed dead, a funeral service is held for him.

Finding herself alone with a very young child, Martina marries Sierra and lives the rich lifestyle she always dreamed of. Five years after he vanished, Ulises calls her and tells her: "It has taken all my travels to discover that I cannot live without you." Unable to resist, she again falls in love with him. Sierra finds out she is cheating on their marriage and takes drastic measures to punish the lovers. He sabotages the Son de Mar, which they use to escape. In the middle of the ocean, the ship sinks and the lovers find peace and eternal love in death.

==Awards==
*Butaca Awards 2001, nominated best Catalan Film Actor (Millor actor català de cinema)
*Goya Awards 2002, nominated best Screenplay - Adapted (Mejor Guión Adaptado)
*Sant Jordi Awards 2002, won best Spanish Actor (Mejor Actor Español), Eduard Fernández
*Verona Love Screens Film Festival 2002, nominated best film, Bigas Luna

==External links==
*  
*  
*  

 

 
 
 
 
 