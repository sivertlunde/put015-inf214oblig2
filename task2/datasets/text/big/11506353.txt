The Friendly Ghost
{{Infobox Hollywood cartoon|
| cartoon_name = The Friendly Ghost
| series = Noveltoon
| image = friendlyghost.png
| caption = Title card
| director = I. Sparber
| story_artist = Story adaptation:   Seymour Reit
| animator = Nick Tafuri John Walworth Tom Golden
| voice_actor = Narration:   (Bonnie/Johnny/Bonnie and Johnnys Mother) Cecil H. Roy (Casper) Jackson Beck (Landlord)
| musician = Winston Sharples
| studio = Famous Studios
| distributor = Paramount Pictures
| release_date = November 16, 1945
| color_process = Technicolor
| runtime =
| movie_language = English
| followed_by = Theres Good Boos To-Night (1948)
}}

The Friendly Ghost is a cartoon released by Paramount Pictures on 16 November 1945 as part of its Noveltoons series of animated short movies. It is the first cartoon to feature the character Casper the Friendly Ghost.

==Plot==

Casper is seen reading the book How to Win Friends, a real book by Dale Carnegie. Every night at midnight his brothers and sisters scare people, but Casper doesnt want to scare people, so he stays home instead.  Casper decides to make friends with the living. While his family is off scaring people, Casper bids his pet cat goodbye and leaves home.

The next morning he meets a rooster to whom he says hello but the rooster retreats. Casper next meets a mole. At first the mole is happy to befriend him but when he puts on eyeglasses, he sees that Casper is a ghost and jumps back in his hole. Casper later meets a mouse and cat who resemble Herman and Katnip and who flee into the barn upon seeing him. Casper then sees a flock of hens who fly away and splatter eggs on him.

Casper thinks that he made a mistake leaving home.  When he hears a train whistle he decides to kill himself by having the train run over him, apparently forgetting that he is already dead. After the train passes over Casper without harming him, he begins crying.  Casper is approached by a boy and girl named Johnny and Bonnie who want to play with him, which makes Casper very happy.

After a game of ball and jump rope, Bonnie and Johnny introduce Casper to their mother, who screams and tells Casper to leave. Casper picks up his sack and is about to go through the door when a landlord opens it. The landlord orders Casper to tell the mother he has come for a mortgage payment, but when he realizes that Casper is a ghost, he runs off in fright, telling Casper to keep the mortgage (because he doesnt want to have a "haunted" house on the market).

Despondent, Casper decides to go back home to his own family. He is about to leave when the mother picks him up with a smile on her face, accepting him for saving her and the children from being forced to leave. The movie concludes with the mother seeing Casper (now wearing schoolboy clothes), Bonnie, and Johnny off to school together. 

==External links==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 

 