Limbo (2004 film)
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Limbo
| image          = Limbo2004.JPG
| director       = Thomas Ikimi
| producer       = Scott Brock John Kim
| Writer         = Thomas Ikimi
| starring       = Christopher M. Russo Joe Holt Etya Dudko
| music          = Andrew David Daniels
| cinematography = Jon Miller 
| released       = 2004
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $9,000}}

Limbo is a 2004 American science fiction drama film about a man caught in a time loop and the effects on him.

==Plot==
Adam Moses is a lawyer with a problem.  He is caught in a repeating loop of time that lasts for 1 hour. At the end of the hour, time resets. Everything he was wearing when he was caught in the loop is back on him.

An interesting note is that his position in Space does not change. If he was across town when time resets, he does not end up back where he was when he became caught in the time loop. In theory he could travel as far as he could in the 1 hour and just keep going when time resets.

He feels that he is alone until he starts meeting others that are also caught in this loop.

The movie keeps track of his "Cycles" or time loops. When he is telling the story it is Cycle 8729 and we see another loop begin at the end of the movie.

Living that many loops, his sanity begins to fray. He begins to question his faith.

==Cast==
*Christopher M. Russo as Adam Moses
*Joe Holt as The Great Laslow
*George Morafetis as Tony Tachinardi
*Etya Dudko as Rebecca

==Themes==
There are numerous religious elements and references to Limbo where Adam appears to be - caught between Heaven and Hell.  Neither alive nor dead.

==Movie poster==
"The Circle Never Ends" with a picture of a snake biting its tail.  Within the circle are the words "Virtue, Sin, Right, Wrong, Good, Evil"

==External links==
*  

 
 
 
 