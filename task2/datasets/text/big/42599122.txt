When Monaliza Smiled
 

{{Infobox film
| name           = When Monaliza Smiled
| image          = When Monaliza Smiled.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Fadi G.Haddad
| producer       = Nadine Toukan Nadia Eliwat
| writer         = Fadi G.Haddad
| starring       = Tahani Salim Shady Khalaf Haifa Al-Agha Nadera Omran Fuad Shomali Haider Kfouf Suha Najjar
| music          = Najati Al Suloh
| cinematography = Samer Nimri
| editing        = Fadi G.Haddad
| studio         = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Jordan
| language       = Arabic
| budget         = $170,000
| gross          = 
}}
When Monaliza Smiled ( ; Lamma Dehket Monaliza) is a 2012 Jordanian romantic comedy film directed by Fadi Hadadd, produced by Nadia Eliwat and Nadine Toukan and starring Tahani Salim and Shady Khalaf with Haifa Al Agha, Nadera Omran, Fouad Al Shomali, Haidar Kfouf, and Suha Najjar. Its plot is set in Amman, Jordan and the film is now is available online and on digital platforms. The film talks about how a Jordanian young lady named Monaliza, who sees no reason to smile, gets a job as an office girl and falls in love with cheerful Hamdi who is an Egyptian immigrant and is the “tea guy” of the office. Hamdi, who is the only person able to make Monaliza smile, shows Monaliza the world in an amazingly new and different perspective.

==Plot==
When Monaliza Smiled is a romantic comedy film that takes place in Amman, Jordan and tells the story of Monaliza (Tahani Salim) who was named after a postcard of Leonardo da Vinci’s Mona Lisa. In contrast to the “interesting” story behind her name, Monaliza lived an unpleasant upbringing, which made her grow her anti-social attitude and most importantly an extreme inability to draw a smile. Now, 37 years old, and after 17 years of waiting for her turn to come in the long queue of applicants, Monaliza finally gets a governmental job at the Directorate of Archiving and makes a decision that it’s time for her to go out into the world. This makes Afaf (Haifa Al Agha), her older unmarried agoraphobic sister who wants to keep Monaliza by her side, very unhappy. At the same time their outgoing neighbour, Rodaina(Suha Najjar), is trying to marry Monaliza off to her sleazy, older and still unwed brother, Suhail(Haidar Kfouf). Monaliza, fed up by Afaf constantly controlling her life, She tricks her into thinking that she is actually considering the proposal. In the office Monaliza meets Nayfeh (Nadera Omran), a bossy, prejudiced, disagreeable woman who keeps picking on everybody around and especially Hamdi (Shady Khalaf), the cheerful Egyptian cafeteria guy, who keeps trying to loosen her up with his coffee and jokes. Monaliza who’s been living in an emotional ice cube for a long time, gradually opens up to Hamdi’s charm and little by little lets him into her closed world and start to learn the joy of living. When Hamdi finally manages to draw a smile on Monaliza’s face, things start getting complicated; Hamdi’s work permit is expiring and he’ll soon become “illegal”. On the other hand, Monaliza’s trick backfires and suddenly everyone in the neighbourhood think that she is officially engaged to Suhail. Being socially inexperienced, and not wanting to lose the battle with Afaf, Monaliza doesn’t know what to do about that neither she tells Hamdi about it. Until one day, after a big fight with Nayfeh in the office, Hamdi impulsively decided to show up at Monaliza’s neighbourhood to meet her sister and propose, only to hear that Monaliza is already “engaged” to the grocer, Suhail. Feeling shocked and betrayed, Hamdi decides to go back to Egypt immediately and Monaliza thinks she must have lost him forever. But when Rodaina realizes the heartbreak, being a dedicated lover, she encourages Monaliza to run after him and work things out. Monaliza’s story overlaps with a bunch of other characters’ who surround her; the story of her sister Afaf and what brought her to become agoraphobic, and the story of Rodaina, who is still waiting for her husband who disappeared in Iraq several years ago, and the story of Nayfeh at the office and her inexcusable bitterness.

==Cast==
* Tahani Salim as Monaliza
* Shady Khalaf as Hamdi
* Haifa Al Agha as Afaf
* Nadira Omran as Nayfeh
* Suha Najjar as Rudaina
* Fouad Al Shomali as Abu Sarah
* Haidar Kfouf as Suhail
* Fadi Arida as Asfour
* Nabil Koni as Maurice
* Ibrahim Abbas as Hassan

==Production==
The film was produced with the support of the Royal Film Commission (RFC) in Jordan/Education Feature Film program, the Landmark Hotel Amman, Ultra Water, Royal Jordanian Airlines, Jordan Tourism Board, the SAE institute in Jordan, and others. The film was shot in August 2011 in Amman, Jordan and is 95 minutes long. The filming took 30 days and it was concluded with a budget of US $170,000. The crew include:
*Executive Producer: Nadine Toukan 
*Producer: Nadia Eliewat 
*Line Producer: Rawan Babia
*Director of Photography: Samer Nimri
*Production Designer: Amjad Al Rasheed
*Art Director: Rand Abdelnour
*Music Composition & Arrangement: Dr. Najati Al Suloh
: Featuring: Maestro Aziz Maraka
*Sound Supervisor: Falah Hannoun
*Sound Designer: Shirin Kamal
*Editor: Fadi G.Haddad
*Casting Director: Nabil Koni
*Location Manager: Waleed Laham

==Marketing==
The film Lamma Dehket Monaliza was a self distributed film and therefore most of its marketing took place on social media websites such as Facebook and Twitter.

==Film festivals and commercial releases==
The films first theatrical release was in Jordan in September 2012. 8 months after that, the film made is 2nd theatrical release in Morocco in June 2013. During this time, the film was shown in many festivals all around the world. These film festivals include:
* International premier at Dubai International Film Festival (DIFF) – December 2012
*European premier at Arab Womens Film Festival – Den Haag, Netherlands - March 2013
*French premier at the IMAGIMA Arab Film Festival - Paris, France - July 2013
*Malmo Arab Film Festival - Sweden - September 2013
*HUMAN SCREEN Film Festival, Tunisia - September 2013
*Oran Arab Film Festival - Algeria - September 2013
* DC Palestinian Film & Arts Festival - Washington, D.C. - October 2013
*Festival du Film Arabe de Fameck - France - October 2013
*Arab Film Festival - Los Angeles, Berkeley and San Diego -  October - November 2013
*The Franco-Arab Film Festival - France - November 2013
*Muscat International Film Festival – March 2014

===Accolades===
*Oran Arab Film Festival in Algeria in 2013:
:*Journalist award (won)
:*Best Actress- Tahani Salim (won)

== References ==
 
#Jordanian cinema on the rise. BroadcastPro. Retrieved April 13, 2014.
#  Retrieved April 14, 2014.
#Arab Observer. Retrieved April 14, 2014.
#Andfaraway.net . Retrieved April 14, 2014.

== External links ==
*  
*  

 
 
 
 