There Goes the Neighborhood (film)
{{Infobox film
| name           = There Goes the Neighborhood (a.k.a. "Paydirt")
| image = There Goes the Neighborhood film poster.jpg
| size = 
| caption        = Theatrical release poster Bill Phillips
| producer       = Stephen J. Friedman Bill Phillips
| narrator       = 
| starring       = Jeff Daniels Catherine OHara Hector Elizondo  Rhea Perlman David Bell Walt Lloyd
| editing        = Sharyn L. Ross
| distributor    = Kings Road Entertainment Paramount Pictures
| released       = October 30, 1992
| runtime        = 88 minutes
| country        =   English
| budget         = 
| gross          = 
| preceded_by    =
| followed_by    = 
|}}

There Goes the Neighborhood, released as Paydirt in most foreign countries, is a 1992 comedy film. The film tells a story of a dying prisoner who whispers the location of his loot to the facilitys psychologist Willis Embry (Jeff Daniels) who heads to the New Jersey suburbs to find it.

==Synopsis==
  Cherry Hill and dig down seven feet in the basement of a house on Pleasant Street, number 7322.  Unbeknown to them however, Lyle is listening in the cell next-door, though he fails to hear the house number correctly, putting down 7320 instead.

Meanwhile, over at number 7322, things are not going well.  Jessie Lodge and her husband Albert are in the middle of a rather messy divorce, and Alberts way of dividing things in half involves taking a chainsaw to a piano.  Across the road neighbours Jeffrey Babbit and Lydia Nunn are supposed to be writing their latest best-selling book, but spend most of their time spying on their neighbours with binoculars from their front window.  They are also spying on Norman and Peedi Rutledge, who live at number 7320.
 jailbreak is being made from prison, with former armed robber Marvin helping Lyle and Harry escape from the joint.  They disguise themselves as landscape gardeners.  Marvin has left behind a surprise for Willis at his flat, a tripwire attached to a bomb.  Laden with heavy groceries, Willis drops them on the floor and notices a tin rolling across the floor to the wire, just managing to jump clear from the building in time which is engulfed by a huge explosion.  The neighbors and police think he is dead, despite there being no sign of a body.

Next morning Norman Rutledge is about to leave for work in his car when a landscape gardeners van blocks his exit, and Lyle, Harry and Marvin emerge to take him back inside, where Normans wife Peedi is also taken hostage.  The couple are questioned about the floor in their basement and the three circles and a triangle that should mark the spot of the treasure.  Circles or no circles, the crooks decide to dig in the basement anyway.  Meanwhile, Embry arrives next door at number 7322 and poses as a furnace engineer to get inside Jessie Lodges house and scout the basement for the digging spot. When she realises he isnt an engineer however, she throws him out.
 pneumatic drill makes a terrible racket.  He suggests using a lawnmower to mask the noise.  Marvin eventually agrees, and tells Lyle to get outside and mow.  Indeed, Jeffrey and Lydia have already noticed the van, and are wondering how the Rutledges can afford a gardener.  Jeffrey is also wondering how Lydia managed to phone Marty Rollins without looking his phone number up.

Over at the Rutledges house, Harry and Peedi are starting to get on quite well and the former is very much in love with her.  Peedi for her part is suitably charmed by the attention.  Her marriage to Norman it seems has been a sham for some time, and they havent shared a bed together for many years.  Norman meanwhile is making yet more advice about moving the van, suggesting that the neighbours need to see that it is "business as usual".  Willis meanwhile is still trying to get into Jessies house and breaks into the basement.  Just as she is about to call the police, Willis reveals the story about the $8.5 million, and says that hell share his half with her 50/50.  She agrees, they find the spot and start to dig.

Night falls, and Peedi and Norman spend the night handcuffed together and later admits to Harry that she has never spent the night tied up before.  They are both becoming attracted to each other, as are Willis and Jessie.  Harry asks what "Peedi" is short for, and she reveals it stands for Penelope Diane.  A suspicious Jeffrey meanwhile phones up Marty Rollins and accuses him of having an affair with his wife.

Next morning, Lydias daughter Swan is out collecting her paperboy money.  She is suspicious not only of Lyle, still doing the gardening at number 7320, but also of Jessie and Willis at number 7322 as she looks into the window in the basement and sees them digging a mysterious hole.  Jessie is also having problems keeping Marty at bay, as he continues to try and bring possible buyers to the house, while her estranged husband Albert also shows up again, though Willis pretends to be her lover and sends him packing.  Later that evening, Marvin hears a noise from next door and sends Lyle to investigate.  He notices Willis next door, and the trio of criminals soon realise that they are at the wrong house.

Norman uses this event to his advantage and offers his help.  He moves a jukebox from the wall and opens a door into a secret room full of contraband and weapons.  He proposes a deal - they join forces and launch an assault next door to take the money by force.  Lyle is not happy, but Marvin agrees, and Norman takes charge.  After a failed attempt at taking the house by force when they are repelled by a crossbow, Norman orders that their existing hole be turned into a tunnel and be extended next door.  After buying some more time, Jessie and Willis meanwhile, continue digging.  Not long after Lyle and Harry break through in their tunnel but come up instead in Jessies swimming pool, with the water coming back through into the basement.  Peedi, still handcuffed to a chair, is released by Harry with a hacksaw before the room becomes completely flooded.  Norman is nowhere to be seen.  A suspicious Albert has already called in the police, and Marvin and Lyle attempt to do a runner from the front door with Lyle disguised as a woman.
 scuba gear.  As events move towards their climax, Harry gives himself up to the police, while Lyle and Marvins disguise is blown and they are arrested.  Willis and Jessie find the money, but first Norman and then Marty try to steal it from them.  In the end it is young Swan who knocks Marty out and the trio escape out the front door.  The money is found to be wet, gone to mush and therefore completely useless. Jessie and Willis though have not broken any laws and are free to go. Just as Marty is dragged away, Jeffrey pulls a gun of his own, and accuses him of sleeping with Lydia.  She says that she knew his phone number because it was on the for sale board, so Jeffrey too joins the ranks of the arrested, although Marty and Lydia exchange a sly nod and a wink as he is led into the paddy wagon.  Even Norman doesnt make a clean getaway, as he attempts to escape on young Swans bicycle.  Peedi calls him the biggest crook of all, and instead gives Handsome Harry a kiss as he is led away and promises to write to him.

Later, as they sit together in the hole in their basement and realise their true love for each other, Willis and Jessie notice a large bag of hidden gold coins.  The film ends as Jessie and Willis drive off into the sun together to fulfil his bargain to Trick and give half of the money to the mysterious Louise.

==Main cast==
 skim bank, buried under the basement of a house at 7322 Pleasant Street, Cherry Hill.
*   with her estranged husband Albert.  She lives at 7322 Pleasant Street, Cherry Hill and strikes an agreement with Willis Embry to find the money hidden under her basement.
*   in a hidden secret room in his basement and is probably the biggest crook of them all. A hostage at first, along with his wife Peedi, he later takes over as leader from Marvin and concocts his own plan to recover the money from next-doors basement.
* Rhea Perlman as Lydia Nunn : Lydia Nunn and husband Jeffrey Babitt are successful authors, who when they are not trying to write their next book are the nosiest neighbours in the world and are never without a pair of binoculars, looking out their window at the comings and goings in the neighborhood.
* Judith Ivey as Peedi Rutledge : Peedi (or Penelope Diane) is the long-suffering wife of Norman Rutledge.  Whatever magic there had been in their marriage many years ago has long since gone.  She strikes up a friendship with Handsome Harry.
*  .
* Jonathan Banks as Handsome Harry : Harry shares a cell with Lyle in prison.  He is the most sympathetic criminal character and falls in love with Peedi Rutledge when they hold her and Norman hostage as they attempt to dig for the money.
*  .
*   issues, but is nowhere near as psychotic as Marvin.  Willis Embry blames his problems on trying to prove that he is harder than his brother.  It is Lyle who overhears the conversation between Willis and Trick about the $8.5million, although he mishears the address and takes his fellow convicts to 7320 Pleasant Street (the home of the Rutledges), not number 7322.
*   who is trying to sell Jessies house.  He hasnt sold a house in three years, and is receiving help as a Sexaholics Anonymous|sexaholic.
*   at a piano and dividing it equally in two.  The divorce is not going well.
*  .  She and her stepfather Jeffrey Babitt share an uneasy relationship. William Morgan Sheppard as Trick Bissell

==Critical Reaction==

"There Goes the Neighborhood" was never a commercial success.  In fact, according to movie stats website www.the-numbers.com, the total US box office gross was a mere $11,000.   This may account for its release in foreign territories under a different title, though this would have had to have been slightly changed in the UK at least as the word "Neighborhood" is spelt in  " all going on general release during the month.  It is perhaps not surprising therefore that "There Goes the Neighborhood" came and went from US cinemas quite quickly.

As of 1 December 2007, some 349 individual users of the IMDB website had given this film a weighted average vote of 5.2.  Of those reviewers who expressed a general opinion on the film, most are in broad agreement that although it has some funny moments, the assembled cast is capable of better things, though co-star Hector Elizondo is usually singled out in particular for special praise with some of the funniest lines in the movie.

==Availability==
 Polish edition Carlton label) and a Dutch version titled as "The Hidden Fortune"
,  it has not yet been made officially available on this format in the USA (region 1) or the UK (region 2) under any of its two main English-language titles.  The region 0 discs that are available are 4:3 full-frame only, not widescreen.

==References==
 

== External links ==
*  

 
 
 
 
 
 