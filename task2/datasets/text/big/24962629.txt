Project Shadowchaser IV
{{Infobox film|
| name = Project Shadowchaser IV
| image =
| caption =
| director = Mark Roper
| writer = Boaz Davidson Danny Lerner B.J. Nelson Mark Roper
| producer = Danny Dimbort (executive producer) Avi Lerner (executive producer) Danny Lerner (producer) Trevor Short (executive producer) Yossi Wein (producer) Brian OShaughnessy
| cinematography = Rod Stewart
| editing = Alain Jakubowicz
| music = Robert O. Ragland
| distributor = Nu Image
| released =  
| budget =
| runtime = 99 min.
| language = English
| country = United States
}}

Project Shadowchaser IV, also known as Shadowchaser IV, Shadowchaser: The Gates Of Time, Orions Key and Alien Chaser, is a 1996 science fiction film by director Mark Roper. It is the fourth and final installment in the Project Shadowchaser film series.

==Synopsis==
After two archaeologists discover an ancient alien artifact in Africa, they must run for their lives from both the unstoppable guardian and protector that awakens as a result, and their greedy, madman employer, both of whom want the artifact.

==DVD release==
The DVD was released in 1999 by Image Entertainment under license from A-Pix Entertainment. The DVD has now been discontinued and as of March 18, 2010, no plans have been made to release a new DVD of the film. Coincidentally, Image Entertainment released a DVD double feature of Project Shadowchaser II and Project Shadowchaser III 8 years later.

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 