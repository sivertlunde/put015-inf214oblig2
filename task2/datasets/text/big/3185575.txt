The Color Purple (film)
{{Infobox film
| name           = The Color Purple
| image          = The Color Purple poster.jpg
| caption        = Theatrical release poster by John Alvin
| director       = Steven Spielberg Kathleen Kennedy Frank Marshall
| screenplay     = Menno Meyjes
| based on       =  
| starring = {{Plainlist|
* Danny Glover
* Adolph Caesar
* Margaret Avery
* Rae Dawn Chong
* Whoopi Goldberg
}} 
| music          = Quincy Jones
| cinematography = Allen Daviau Michael Kahn
| studio         = Amblin Entertainment Warner Bros. Pictures
| released       =  
| runtime        = 154 minutes
| landscaping    = Classic Landscapes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $142 million
}} period drama novel of blockbusters for which he had become famous. It is also the first feature-length film in Spielbergs entire career for which John Williams has not composed the music. The film starred Danny Glover, Desreta Jackson, Margaret Avery, Oprah Winfrey (in her film debut), Adolph Caesar, Rae Dawn Chong, and introduced Whoopi Goldberg as Celie Harris.
 Anson and Union counties in North Carolina,    the film tells the story of a young African American girl named Celie Harris and shows the problems African American women faced during the early 1900s, including poverty, racism, and sexism. Celie is transformed as she finds her self-worth through the help of two strong female companions. 

==Plot==
Taking place in the Southern United States between 1909 and 1936, the movie tells the life of a poor African American woman named Celie Harris (Whoopi Goldberg) whose abuse begins when she is young. By the time she is 14, she has already had two children by her father Alphonso "James" Harris (Leonard Jackson). He takes them away from her at childbirth and forces the young Celie (Desreta Jackson) to marry a wealthy young local widower Albert Johnson, known to her only as "Mister" (Danny Glover), who treats her like a slave. Albert makes her clean up his disorderly household and take care of his unruly children. Albert beats and rapes her often, intimidating Celie into submission and near silence. Celies sister Nettie (Akosua Busia) comes to live with them, and there is a brief period of happiness as the sisters spend time together and Nettie begins to teach Celie how to read. This is short-lived; after Nettie refuses Alberts predatory affections once too often, he kicks her out. Before being run off by Albert, Nettie promises to write to Celie saying, "Nothing but death can keep me from it!".

Alberts old flame, jazz singer Shug Avery (Margaret Avery), for whom Albert has carried a torch for many years, comes to live with him and Celie. Delirious with sickness, Shug initially declares Celie as "ugly" on their first meeting. Despite this, they eventually become close friends and Shug helps Celie raise her self-confidence. Shug and Celie enter into a sexual relationship (more pronounced in the book, but only hinted at in the film).  Celie also finds strength in Sofia (Oprah Winfrey), who marries Alberts son Harpo (Willard E. Pugh). Sofia has suffered abuse from the men in her family, but unlike Celie, she refuses to tolerate it. This high-spiritedness proves to be her downfall, as a rude remark to the town mayors wife and a retaliatory punch to the mayor himself ends with Sofia beaten and jailed.

Meanwhile, Nettie has been living with missionaries in Africa and writing frequently to Celie - however, Celie is unaware of the correspondence, as Albert has confiscated the letters, forbidding Celie to touch the mailbox, telling Celie that she will never hear from her sister again. During a visit from Shug and her new husband Grady, Celie and Shug discover many years worth of Netties correspondence. Reconnecting with her sister and the assurance that she is still alive helps give Celie the strength to stand up to Albert. She prepares to slit his throat while shaving him, but is physically stopped by Shug.

During a subsequent family dinner, Sofia is shown to be prematurely aged and permanently disfigured due to the severe beatings she received in jail, and demoralized into an almost catatonic state. At the dinner, Celie finally asserts herself, excoriating Albert and his father. Shug informs Albert that she and Grady are leaving, and that Celie is coming with them. Harpos girlfriend Squeak (Rae Dawn Chong) declares she is going with them as well. Despite Alberts attempts to verbally abuse Celie into submission, she stands up to him by mentioning that he kept Nettie away from her because Nettie was the only one who really loved her. Seeing Celie stand up for herself, Sofia returns to her normal self, laughing hysterically at a dumbfounded and embarrassed Albert. She also warns Celie not to follow in her own footsteps as Celie holds a knife to Alberts throat.  It is at this point Celie curses Albert saying, "Until you do right by me, everything you think about gonna fail". As Shug, Grady, Squeak, and Celie go quickly to the car, Albert readies to beat Celie, but she stands up on the sideboards of Gradys car and curses Albert by raising her hand in his face with a determined stare. Dumbfounded, Albert backs away and the car drives off.

In Tennessee, Celie opens a haberdashery, making and selling one-size-fits-all slacks for men and women. Upon the death of her father, she learns that he was, in fact, her stepfather, and that she has inherited her childhood home, the farm, and a shop from her real father. She opens her second slacks shop in town, naming it Miss Celies Folks Pants, while Harpo and Sofia reconcile. Meanwhile, Albert is feeling the effects of Celies words. His fields and home languish into almost nonexistence as he slips into alcohol-fueled idleness, spending most of his time at Harpos Juke joint. At one point, his father is seen suggesting that he find a new wife, but Albert casually grabs his father by the arm and turns him off his property. Years of guilt finally catch up with Albert, with the knowledge that he has been a horrible person most of his life, especially to Celie. In a sudden act of kindness unknown to her, Albert takes all the money he has saved over the years, goes to the immigration office, and arranges for Nettie, her husband, and Celies two children and daughter-in-law to come back to America from Africa. Celies children, Adam and Olivia, are reunited with her at Celies farm. Albert looks on from a distance, and Shug smiles at him because he finally did the right thing. Nettie and Celie play their childhood clapping game as the sun sets.

==Cast==
 
* Whoopi Goldberg as Celie Harris Johnson
* Danny Glover as Albert Johnson ("Mister")
* Oprah Winfrey as Sofia
* Margaret Avery as Shug Avery
* Akosua Busia as Nettie Harris
* Adolph Caesar as Old Mister Willard Pugh as Harpo Johnson
* Rae Dawn Chong as Squeak
* Laurence Fishburne as Swain
* Grand Bush as Randy
* Dana Ivey as Miss Millie
* Leon Rippy as Store Clerk
* Bennet Guillory as Grady
* James Tillis as Henry "Buster" Broadnax
* Desreta Jackson as Young Celie Harris Leonard Jackson as Alphonso "Pa" Harris
* Howard Starr as Young Harpo Johnson
* Lelo Masamba as Olivia Johnson
* Táta Vega serves as "The singing voice" of Shug Avery
 

==Reception==

=== Critical response ===
The Color Purple received mostly positive reviews. Rotten Tomatoes gives the film a score of 88% based on reviews from 26 critics, with an average score of 6.9/10. The sites consensus is that the film is "a sentimental tale that reveals great emotional truths in American history." {{cite web
| url= http://www.rottentomatoes.com/m/color_purple/
| title= The Color Purple (1985)
| publisher= Flixster
| work = Rotten Tomatoes
| accessdate= 2011-12-08
}}
 

Roger Ebert of the Chicago Sun-Times awarded the film four stars, calling it "the years best film." He also praised Whoopi Goldberg, calling her role "one of the most amazing debut performances in movie history" and predicting she would win the Academy Award for best actress. (She was nominated but did not win.) Ebert wrote of The Color Purple:
 

Eberts long-time television collaborator, Gene Siskel of the Chicago Tribune, praised the film as "triumphantly emotional and brave," calling it Spielbergs "successful attempt to enlarge his reputation as a director of youthful entertainments." Siskel wrote that The Color Purple was "a plea for respect for black women." Although acknowledging that the film was a period drama, he praised its "... incredibly strong stand against the way black men treat black women. Cruel is too kind a word to describe their behavior. The principal black men in The Color Purple use their women — both wives and daughters — as sexual chattel." 

New York Times film critic Janet Maslin noted the films divergence from Walkers book, but made the case that this shift works: 
 

Variety (magazine)|Variety found the film over-sentimental, writing, "there are some great scenes and great performances in The Color Purple, but it is not a great film. Steven Spielbergs turn at serious film-making is marred in more than one place by overblown production that threatens to drown in its own emotions." 

In addition, some critics alleged that the movie stereotyped black people in general  and black men in particular,  pointing to the fact that Spielberg, a white man, had directed a predominantly African American story.   

Filmmaker Oliver Stone defended The Color Purple as "an excellent movie, and it was an attempt to deal with an issue that had been overlooked, and it wouldnt have been done if it hadnt been Spielberg. And its not like everyone says, that he ruined the book. Thats horseshit. Nobody was going to do the book. He made the book live again." 

=== Box office ===
The Color Purple was a success at the box office, staying in U.S. theaters for 21 weeks,  and grossing over $142 million worldwide.  In terms of box office income, it ranked as the #1 rated PG-13 movie released in 1985, and #4 overall.   Box Office Mojo. Accessed Dec. 9, 2011. 

=== Awards ===

   }}
 Best Picture, Best Actress Best Supporting The Turning Point for the most Oscar nominations without a single win.   
;Academy Awards nominations: Best Picture Best Actress in a Leading Role - Whoopi Goldberg  Best Actress in a Supporting Role - Margaret Avery 
* Best Actress in a Supporting Role - Oprah Winfrey Best Writing, Screenplay Based on Material from Another Medium Best Cinematography - Allen Daviau  Best Art Direction – Set Decoration - J. Michael Riva, Bo Welch, Linda DeScenna Best Costume Design - Aggie Guerard Rodgers  Best Makeup Ken Chase  Best Music, Original Score Best Original Song ("Miss Celies Blues (Sister)")
 Golden Globes, Best Picture Best Director Best Supporting Best Actress (Drama).

Menno Meyjes was nominated for the BAFTA Award for Best Adapted Screenplay.

Spielberg received the Directors Guild of America Award for Best Motion Picture Director, his first.

The film was shown at the 1986 Cannes Film Festival as a non-competing title.   

==Popular culture==
* Dialogue from the film was sampled in the 2012 mixtape   by Mykki Blanco. 

==See also==
* List of American films of 1985 musical version of the novel.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 