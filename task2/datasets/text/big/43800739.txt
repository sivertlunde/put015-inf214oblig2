Law of the Underworld
{{Infobox film
| name           = Law of the Underworld
| image          = Law of the Underworld poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lew Landers
| producer       = Robert Sisk 
| screenplay     = Bert Granet Edmund L. Hartmann
| story          = John B. Hymer Samuel Shipman
| based on       =   Anne Shirley Eduardo Ciannelli Walter Abel Richard Bond
| music          = Nathaniel Shilkret Roy Webb
| cinematography = Nicholas Musuraca
| editing        = Ted Cheesman
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Anne Shirley, Eduardo Ciannelli, Walter Abel and Richard Bond. The film was released on May 6, 1938, by RKO Pictures.  

==Plot==
 

== Cast ==
*Chester Morris as Gene Fillmore Anne Shirley as Annabelle Porter
*Eduardo Ciannelli as Rocky
*Walter Abel as Warren Rogers
*Richard Bond as Tommy Brown Lee Patrick as Dorothy Palmer Paul Guilfoyle as Batsy
*Frank M. Thomas as Police Captain Gargan
*Eddie Acuff as Bill
*Vinton Hayworth as Eddie 
*Jack Carson as Johnny Paul Stanton as Barton 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 