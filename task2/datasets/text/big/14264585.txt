The Kiss in the Tunnel
{{Infobox film
| name           = The Kiss in the Tunnel
| image          = TheKissintheTunnel.jpg
| image_size     =
| caption        = Screencap from the film George Albert Smith
| producer       = George Albert Smith
| writer         =
| starring       = Laura Bayley George Albert Smith
| cinematography = George Albert Smith
| editing        =
| distributor    =
| released       =  
| runtime        = 1 min 3 secs
| country        = United Kingdom Silent
| budget         =
}}
 1899 film British Short short  silent comedy George Albert Smith, showing a couple sharing a brief kiss as their train passes through a tunnel, which is said to mark the beginnings of narrative editing. The director, according to Michael Brooke of BFI Screenonline, "felt that some extra spice was called for," in the then-popular phantom ride genre, which featured shots taken from the front of a moving train, "and devised a shot showing a brief, almost furtive moment of passion between two passengers, taking advantage of the brief onset of darkness."  Just this middle shot was offered by The Warwick Trading Company to exhibitors, who were advised, "to splice it into train footage that they almost certainly would own from previous programmes."      

Screenonline reviewer Michael Brooke points out that "the lighting here is totally unrealistic - we can see everything thats going on," and, "no attempt has been made at realism in the setting - the "carriage is very obviously a painted flat that has been decorated with various props: luggage, parasols and so on, though the camera has been made to sway from side to side to create the illusion of movement." Nonetheless, according to David Fisher, this three-shot film (as recontsructed by the BFI) is, "beginnings of narrative editing."  

The film was remade under the same title by Bamforth and Company the same year, although they, according to Michael Brooke of BFI Screenonline, "adopted a rather less stylised and noticeably more passionate approach to the brief encounter of the title." 

The film combined two popular features of early cinema: phantom rides and kissing scenes. Smiths work was imitated by others including James Banforth (in The Kiss in the Tunnel, 1899), S. Lubin (Love in a Railroad Train, 1902) and Edwin S. Porter (What Happened in the Tunnel, 1903). The success of the film led to the Warwick Trading Company offering the shot of the couple kissing as a standalone item to film exhibitors, which they could then insert into footage they already had of trains entering and exiting a tunnel.   

==References==
 

== External links ==
*  
*   on YouTube

 
 
 
 
 
 
 


 
 