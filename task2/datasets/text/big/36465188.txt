El Gringo
<!--  
 
-- Write the article first.   -->
 
{{Infobox film
| name           = El Gringo
| image          = El Gringo film poster.png
| caption        = Theatrical release poster Eduardo Rodríguez
| producer       =   Jonathan Stokes 
| starring       =  
| music          =Luis Ascanio
| cinematography = Yaron Levy
| editing        =  
| studio         = After Dark Films Silver Pictures
| distributor    = After Dark Films G2 Pictures Tanweer Films
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English Spanish
| budget         =   7 million 
| gross          =
}} Eduardo Rodríguez, Jonathan Stokes, and starring Scott Adkins, Christian Slater and Yvette Yates.

==Plot==
Following an ambush in which he is wounded, and his undercover DEA partners are killed, The Man escapes into Mexico with a case holding two million dollars, and arrives in the dusty town of El Fronteras. He faces danger from the local sheriff and his thugs, a local drug cartel, his checkered past and his former DEA boss.

==Partial cast==
* Scott Adkins as The Man
* Yvette Yates as Anna
* Christian Slater as Lieutenant West
* Israel Islas as Culebra
* Erando González as Chief Espinoza
* Sofía Sisniega as Flaca
* Valentin Ganev as Deputy Chief Logan
* Darren Shahlavi

==Production==
The screenplay by Jonathan Stokes was purchased by After Dark Films in 2011 for Joel Silver to executive produce.    

The film was shot in Bulgaria and Louisiana at an estimated cost of  7 million.   

==Release== MPAA "R" rating.  As part of the "After Dark Action" bundle, the film showed for one week in ten cities,  and was simultaneously released for video on demand. {{
cite web|title=After Dark Action releases trailer, poster and stills for El Gringo |date=April 27, 2012|publisher=After Dark Films|work=afterdarkaction.com|url=http://afterdarkaction.com/news/after-dark-action-releases-trailer-0}}  {{
cite web|title=FAQ |date=April 27, 2012|publisher=After Dark Films|work=afterdarkaction.com|url=http://afterdarkaction.com/faq}}  

==Reception==
The film received mildly warm reviews. Variety described it as "an undeniable exercise in third-hand coolness, with nods to spaghetti Westerns and 70s drive-in actioners, El Gringo is diverting enough", continuing, "willfully over-the-top action and character types are fun if never quite as giddily distinctive as hoped for."  The Los Angeles Times summarized, "not bad exactly, but its not especially notable either."  IndieWire noted that the films "colorful character    dont really get much to do to emphasize their identities amidst the action", adding, "El Gringo gets bogged down in overly-plotty nonsense, but the fight choreography and shootouts are fast-paced and inventive, allowing the film to come alive in spite of its time-wasting peripherals", giving the film a "B-". 

==References==
 

==External links==
*  
*  
*   at TheActionElite.com

 
 

 
 
 
 
 
 
 
 
  
 
 