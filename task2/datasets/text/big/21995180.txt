Trail of the Mounties
{{Infobox film
| name           = Trail of the Mounties
| image          = 
| alt            = 
| caption        = 
| director       = Howard Bretherton
| producer       = Carl K. Hittleman
| screenplay     = Betty Burbridge
| story          = {{Plainlist|
* Carl K. Hittleman 
* Harold Kline
}}
| starring       = {{Plainlist|
* Russell Hayden
* Jennifer Holt
* Emmett Lynn
}}
| music          = Albert Glasser
| cinematography = Benjamin H. Kline
| editing        = Paul Landres
| studio         = Bali Pictures Inc.
| distributor    = Screen Guild Productions
| released       =  
| runtime        = 45 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film directed by Howard Bretherton and starring Russell Hayden, Jennifer Holt, and Emmett Lynn. Written by Betty Burbridge, Carl K. Hittleman, and Harold Kline, the film is about a Royal Canadian Mounted Policeman who is seeking to thwart a gang of fur thieves led by his twin brother Johnny.

==Cast==
* Russell Hayden as Sandy/ Johnny Sanderson
* Jennifer Holt as Kathie McBain
* Emmett Lynn as Gumdrop Terry Frost as Nick
* Harry Cording as Hawkins
* Charles Bedell as Maurice
* Zon Murray as Jacques

==External links==
*  

 
 
 
 
 
 
 
 


 