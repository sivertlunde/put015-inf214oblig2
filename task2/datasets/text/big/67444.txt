Half Past Dead
 
{{Infobox film
| name = Half Past Dead
| image = Half Past Dead.jpg
| caption = Theatrical release poster
| director = Don Michael Paul
| producer = Elie Samaha Steven Seagal Andrew Stevens
| writer = Don Michael Paul Michael "Bear" Talliferro
| music = Tyler Bates
| cinematography = Michael Slovis
| editing = Vanick Moradian
| studio = Franchise Pictures
| distributor = Screen Gems
| released =  
| runtime = 98 min.
| country = United States
| language = English
| budget = $25,000,000 
| gross = $19,233,280 
}}

Half Past Dead is a 2002 American action film written and directed by Don Michael Paul in his directorial debut, and produced by Steven Seagal, who also starred in the lead role.    The film co-stars Morris Chestnut, Ja Rule and Nia Peeples. The film was released in the United States on November 15, 2002. This was Steven Seagals last theatrical release film in many countries until 2010s Machete (film)|Machete.
 FBI agent has to stop him. Distribution and copyrights are held by Columbia Pictures.

==Plot==
In San Francisco, Sasha Petrosevitch (Steven Seagal) is a Russian car thief whos brought in by criminal Nick Frazier (Ja Rule) to work for crime boss Sonny Eckvall (Richard Bremmer), who apparently shot and killed Sashas wife. After some time, FBI Special Agent Ellen "E. Z." Williams (Claudia Christian) and her team show up to nail Nick, but things go bad, and Sasha gets shot.

After eight months of recovery following his brief bout of being clinically dead from the shooting, Sasha is incarcerated along with Nick in the newly reopened Alcatraz prison. Run by the charismatic warden, Juan Ruiz "El Fuego" Escarzaga (Tony Plana), the place is known for its new state of the art death chamber where the condemned can choose from five different ways to die: lethal injection, gas chamber, hanging, firing squad, or electric chair.

Lester McKenna (Bruce Weitz), is the first death row prisoner brought to the new Alcatraz and also the first prisoner scheduled to be executed. An older man, he stole $200,000,000 worth of gold bricks in a heist that resulted in five deaths, and hid the loot at an unknown location. Federal Bureau of Prisons head Frank Hubbard (Stephen J. Cannell) and Supreme Court Justice June McPherson (Linda Thorson) have arrived to witness the execution, which is a result of June sentencing Lester.

But shes not the only one interested in Lester. A small but well equipped team of terrorists who call themselves the "49ers" have parachuted onto the Alcatraz island, and gained control of it. Led by 49er One, a.k.a. Hubbards assistant Donny Johnson (Morris Chestnut), and 49er Six (Nia Peeples), the team finds Lester, and they want him to give up the location of his hidden stash of gold. When Lester will not tell them, Donny shoots a nearby priest (Eva-Maria Schönecker), and threatens to kill others if the information is not delivered.

Donnys plan is disrupted, however, when Sascha decides to step in and fight back. It turns out that Sasha is actually an undercover FBI agent who has been trying to use Nick to get to Sonny Eckvall. When Sasha rescues Lester, the 49ers strap June to the electric chair and threaten to kill her, all while Ellen and her team prepare a rescue plan from the mainland.

With the help of Nick and some of the other inmates such as Twitch (Kurupt) and Little Joe (Michael "Bear" Taliferro), Sasha sets out to rescue June and bring Donny down, before Alcatraz becomes everyones final resting place.

==Cast==
* Steven Seagal as Sasha Petrosevitch
* Morris Chestnut as 49er One / Donald Robert Johnson
* Ja Rule as Nicolas "Nick" Frazier
* Nia Peeples as 49er Six
* Tony Plana as Warden Juan Ruiz "El Fuego" Escarzaga
* Kurupt as Twitch Michael "Bear" Talliferro as Little Joe
* Claudia Christian as Special Agent Ellen "E. Z." Williams
* Linda Thorson as Judge Jane McPherson
* Bruce Weitz as Lester McKenna
* Michael McGrady as Guard Damon J. Kestner
* Richard Bremmer as Sony Eckvall
* Hannes Jaenicke as Agent Hartmann
* Matt Battaglia as 49er Three
* Wiliam T. Bowers as Alcatraz Guard

==Reception==
The film has received negative reviews from most film critics. It ranked 19th in Rotten Tomatoess 100 worst films of the 2000s and had a 3% fresh rating, with critic Roger Ebert stating: "Half Past Dead is like an alarm that goes off with nobody in the room. It does its job, stops, and nobody cares."

==Awards and Nominations==
*2003 Golden Raspberry Awards
**Nomination: Worst Actor (Steven Seagal) - lost to Roberto Benigni for Pinocchio (2002 film)|Pinocchio

==Sequel==
Half Past Dead 2 was released direct-to-video in May 2007, though it did not star Steven Seagal or Ja Rule. The starring characters were Twitch (Kurupt) and Burke (Bill Goldberg).

==Home media== Region 1 Columbia TriStar Home Entertainment.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 