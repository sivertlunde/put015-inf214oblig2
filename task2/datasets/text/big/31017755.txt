Megamind: The Button of Doom
{{Infobox film
| name           = Megamind: The Button of Doom
| image          = Megamind The Button of Doom dvd cover.jpg
| caption        = DVD cover
| director       = Simon J. Smith 
| producer       = Mark Swift Denise Cascino
| screenplay     = Alan Schoolcraft Brent Simons
| starring       = Will Ferrell David Cross
| studio         = DreamWorks Animation Pacific Data Images|PDI/DreamWorks
| music          = Hans Zimmer Lorne Balfe
| editing         = Michelle Belforte Hauser
| distributor    = Paramount Pictures
| released       =  
| runtime        = 16 minutes
| country        = United States
| language       = English
}} Nickelodeon on February 26, 2011.

==Plot==
For their first day on the job as the new protectors of Metro City after defeating the deranged villain Tighten, Megamind and Minion are selling off their gadgets from their evil lair to the citizens for good purposes (such as the Boot Wheel of Death being sold to a soccer mom, who intends to use it for practice with her soccer team and the Flamethrower being sold to a chef, who intends to use it for cooking his meals). All of their items get sold (except for the death ray which Megamind used to "kill" Metro Man in the original film). As for the De-Gun (Megaminds weapon of choice used for a number of different destructive functions) it gets sold to a boy named Damien, who accidentally uses it (in its dehydration mode) on his mother and turns her into a cube. Megamind tells Damien to soak the cube in water to get her back to normal, but unknown to Megamind, Damien decides not to do it and instead revels in his freedom from his mother. After the auction is over, Megamind reveals a suit he created that contains all of Metro Mans powers, which he will wear to protect Metro City. 

Minion finds a seemingly harmless button that had also not had been sold. Not knowing what the button does, Megamind pushes the button, and it reveals a giant robot called the Mega-Megamind, which Megamind put his evil self in. The robot thinks that Megamind is Metro Man, and starts attacking him. Megamind fights the robot with his new powers, but is defeated. He and Minion hide in the Invisible Car. Megamind decides that they should live in the car from then on, because he programmed the robot to not stop until the hero (and his assistant) is destroyed. Minion reveals that he still kept the Spiderbot, a robot that Megamind wanted to throw away, suggesting that Megamind should try to be a hero his way rather than try to do the job the way Metro Man did it. Megamind plans to lure the Mega-Megamind to a spot where the Death Ray shoots (the old observatory in which the real Metro Man was lured to in the original movie). However, Minion realizes that he cannot activate the Death Ray because the control buttons were broken. He then tries to find the Death Ray remote control, but Megamind does not remember what the remote looks like, so Minion tries all the remotes. Megamind, trying to hide, eventually is found by the Mega-Megamind (because Minion accidentally played rock music on the Spiderbot). Minion finally finds the correct remote and activates the Death Ray. Megamind and the Spiderbot get off the robot, using the robots wrist-mounted shooter just before the robot explodes. 

The duo then recover the De-Gun from Damien, who not only has refused to re-hydrate his mother but also used it on the parents of several other children, who are all now having a party to celebrate no longer having to do as they are told. Megamind then re-hydrates the parents with a glass of water to ensure that the children will face punishment for their actions, much to the childrens dismay. The short ends with Megamind and Minion seeing a signal in the sky (an obvious spoof of the Bat-Signal) and riding off in the Invisible Car as they are called to action.

==Cast==
* Will Ferrell - Megamind and Mega-Megamind
* David Cross - Minion, Megaminds assistant
*Michelle Belforte Hauser - Concerned Mother
*Jordan Alexander Hauser - Damien
*Kevin N. Bailey - Kevin
*Dante James Hauser - Nigel
*Declan James Swift - Peter
*Fintan Thomas Swift - Barney

==Home media==
The film was released on DVD and Blu-ray on February 25, 2011. 

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 