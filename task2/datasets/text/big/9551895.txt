The Railway Children (1970 film)
 
 
{{Infobox film
| name           = The Railway Children
| image          = TheRailwayChildren.jpg
| caption        = DVD cover
| director       = Lionel Jeffries Robert Lynn
| writer         = Lionel Jeffries
| narrator       = Jenny Agutter
| starring       = Jenny Agutter Gary Warren Sally Thomsett Dinah Sheridan Bernard Cribbins William Mervyn  Ann Lancaster Johnny Douglas
| cinematography = Arthur Ibbetson
| editing        = Teddy Darvas EMI Elstree
| distributor    = EMI Films#MGM-EMI|MGM-EMI (UK) Universal Pictures (USA)
| released       =  
| runtime        = 110 minutes
| country        = United Kingdom
| language       = English
| budget         = £500,000 
}} novel of the same name by E. Nesbit. The film was directed by Lionel Jeffries, and stars Dinah Sheridan, Jenny Agutter (who had earlier featured in the successful BBCs 1968 dramatisation of the novel), Sally Thomsett and Bernard Cribbins in leading roles. The film was released to cinemas in the United Kingdom on 21 December 1970.

The film rights were bought by Lionel Jeffries. It was his directorial debut, and he was also responsible for writing the screenplay for the film. The Railway Children turned out to be a critical success, both at the time of its release and in later years. It has gone on to gain a place in several surveys of the greatest films ever made, including surveys conducted by the British Film Institute and Total Film magazine.

==Plot==
The story follows the adventures of the Waterbury children, who are forced to move with their mother (Dinah Sheridan) from a luxurious Edwardian villa in the London suburbs to "Three Chimneys", a house near the fictional Great Northern and Southern Railway in Yorkshire, as their father (Iain Cuthbertson), who works at the Foreign Office, has been imprisoned as a result of being wrongly accused of selling state secrets to the Russians.

The three children, Roberta (Bobbie) (Jenny Agutter), Phyllis (Sally Thomsett) and Peter (Gary Warren), find amusement in watching the trains on the nearby railway line and waving to the passengers. They become friendly with Albert Perks (Bernard Cribbins), the station porter, and with the Old Gentleman who regularly takes the 9:15 down train. Meanwhile, to earn money to survive during her husbands absence, Mother writes and sells stories to magazines.
 paper chase, Bobbie eventually discovers the truth of her fathers absence and appeals to the Old Gentleman for his help. He is eventually able to help prove their fathers innocence, and the family is reunited.

==Cast==
* Dinah Sheridan as Mrs. Waterbury ("mother")
* Bernard Cribbins as Albert Perks
* William Mervyn as Old Gentleman
* Iain Cuthbertson as Charles Waterbury ("father")
* Jenny Agutter as Roberta Bobbie Waterbury
* Sally Thomsett as Phyllis Waterbury
* Gary Warren as Peter Waterbury 
* Peter Bromilow as Doctor Forrest 
* Ann Lancaster as Ruth 
* Gordon Whiting as Shapanski
* Beatrix Mackey as Aunt Emma 
* Deddie Davies as Mrs. Nell Perks  David Lodge as Band Leader
* Christopher Witty as Jim 
* Brenda Cowling as Mrs. Hilda Viney 
* Paddy Ward as Cart Man 
* Erik Chitty as Photographer  Sally James as Maid 
* Dominic Allan as CID Man  
* Andy Wainwright as Desk Seargent 
* Amelia Bayntun as Cook (uncredited)
* Bob Cryer as guard of train returning father (uncredited)

==End credits==
The entire cast break the fourth wall and perform a curtain call as the credits roll. The camera moves slowly along a railway track towards a train which is decked in flags, in front of which all of the cast are assembled, waving and cheering to the camera.

At the start of the credit sequence, a voice can be heard shouting "Thank you, Mr Forbes" as an acknowledgement to Bryan Forbes, who put up a security for the film to be completed.   Meanwhile, Jenny Agutter holds up a small slate on which "The End" is written in chalk and says "Goodbye" as the credits conclude.

==Production==
 
Sally Thomsett was cast as the 11-year-old Phyllis, despite being 20 years old at the time. Her contract forbade her to reveal her true age during the making of the film and she was also not allowed to be seen smoking or drinking during the shoot.  17-year-old Jenny Agutter played her older sister, Roberta, and Gary Warren played their brother, Peter. Dinah Sheridan was cast as Mother, and Bernard Cribbins as Perks the porter.

===Filming locations===
Lionel Jeffries used the Keighley and Worth Valley Railway, and its station at Oakworth railway station|Oakworth, as the backdrop for the film, referring to it as the "Great Northern and Southern Railway".  At the time of filming there were still very few heritage railways in Britain, and only the KWVR could provide a tunnel, which is important in a number of scenes. The tunnel is a lot shorter in reality than it appears in the film, for which a temporary extension to the tunnel was made using canvas covers. 
 5775 (L89), 52044 (Preserved 4744 (69523/1744), Stroudley livery North Eastern Great Northern Great Central Railway at Loughborough. 5775 and 957 are still on the Keighley & Worth Valley Railway. As of 2014, 4744 is the only locomotive that remains operational as 957, 5775 and 67 are on static display at the Keighley and Worth Valley Railway, National Railway Museum Shildon and the Middleton Railway respectively.
 Metropolitan and London and North Eastern railways. Although different carriages appeared in different liveries, the dominant one is white and maroon, which is reminiscent of the livery of the Caledonian Railway. The most important carriage in the film, the Old Gentlemans Saloon, was a North Eastern Railway Directors Saloon, that has been used since in the stage production of the book. It and all the other carriages seen in the film are still at the KWVR, but tend to be used at special events only, due to their age.
 paper chase scene, as well as the one in which the children wave the girls petticoats in the air to warn the train about a landslide. The landslide sequence itself was filmed in a cutting on the Oakworth side of Mytholmes Tunnel and the fields of long grass where the children waved to the trains are situated on the Haworth side of the tunnel. A leaflet, "The Railway Children Walks", is available from KWVR railway stations or the Haworth Tourist Information Centre. 

==Release==
The film was the ninth most popular movie at the British box office in 1971  and recouped its cost in that country alone. It was one of the few financial successes of Bryan Forbes regime at EMI Films. 

===Critical reception===
Since its release, the film has received universally good reviews; it holds a 100% rating on Rotten Tomatoes, based on 8 reviews.

===Home media===
A 40th anniversary Blu-ray and DVD was released on 5 May 2010 with a new digitally remastered print. It includes new interviews with Sally Thomsett, Jenny Agutter and Bernard Cribbins. The planned commentary by director Lionel Jeffries was not completed due to his death in February 2010.

===Awards and nominations=== Best Supporting Best Newcomer David Bradley. Johnny Douglas was also nominated for the Anthony Asquith Award for Film Music but the award was won by Burt Bacharach for his film score for Butch Cassidy and the Sundance Kid. 

===Merchandise===
Hornby Railways produced a 00 Gauge train set of the train from the film. It had a London, Midland and Scottish Railway 0-6-0 tank shunting locomotive in GN&SR livery with Synchrosmoke, two period coaches, an oval of track and a station. It was very popular with children,  and it is highly sought after today as a collectors item.

In 2010, to coincide with the 40th anniversary, a book was brought out called The Making of the Railway Children by Jim Shipley - a former volunteer station master of Oakworth Station. It detailed events that took place during filming and interviews from local people associated with it.
In November 2012, a second updated version was printed with added information, in particular about Gary Warren who disappeared in the mid 70s after retiring from acting. He had been tracked down by a member of the Official Catweazle fanclub and the author had permission to write a more updated version of what had happened to him.

==Legacy== Top 100 Men in Black and Ghostbusters.
On 28 March 2010 the Bradford International Film Festival concluded with a new restoration of The Railway Children film with the 40th anniversary digital premiere. 

Jenny Agutter also starred in a The Railway Children (TV film)|made-for-TV remake of The Railway Children in 2000 in the role of Mother.  Much of the publicity for the 2000 film focused on Agutters involvement in both films made a generation apart.

==References==
 

==External links==
*  
*  
*  
*   - Daily Telegraph obituary
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 