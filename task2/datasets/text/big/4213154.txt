The White Countess
{{Infobox film
| name           = The White Countess
| image          = White countess.jpg
| caption        = Original poster James Ivory
| producer       = Ismail Merchant
| writer         = Kazuo Ishiguro
| starring       = Ralph Fiennes Natasha Richardson Vanessa Redgrave Hiroyuki Sanada Lynn Redgrave Allan Corduner Madeleine Potter Richard Robbins
| cinematography = Christopher Doyle
| editing        = John David Allen
| distributor    = Sony Pictures Classics
| released       = December 21, 2005
| runtime        = 135 minutes
| country        = United Kingdom United States China
| awards         =
| language       = English
| budget         =
| gross          = $4,092,682 (worldwide)
}} James Ivory. The screenplay by Kazuo Ishiguro focuses on a disparate group of displaced persons attempting to survive in Shanghai in the late 1930s.

==Plot== Bolshevik Revolution in Russia, Countess Sofia Belinskaya is working as a taxi dancer in a seedy Shanghai bar to support her family of White émigrés, including her daughter Katya, her mother-in-law Olga, her sister-in-law Grushenka, and an aunt and uncle by marriage, Princess Vera and Prince Peter. Despite the fact employment is scarce and her meagre income is the familys only source of revenue, Sofias once-aristocratic relatives scorn her for her choice of profession and insist she keep it a secret from her child.

Sofia eventually meets Todd Jackson, a former official of the U.S. State Department who recently lost his wife and children in separate terrorist bombings. The bombing that killed his child also left him blind. Using his substantial winnings from a well-placed bet at the racetrack, he opens an elegant nightclub catering to the cosmopolitan upper class and invites Sofia to work for him as his primary hostess, an offer she accepts, and in honor of her he calls the club "The White Countess." As time passes, the two begin to fall in love, but neither acts on their feelings until the political climate around them slowly disintegrates, leading to the outbreak of the Second Sino-Japanese War and a mass exodus from the besieged city.

==Cast==
*Ralph Fiennes as Todd Jackson
*Natasha Richardson as Countess Sofia Belinskaya
*Hiroyuki Sanada as Mr. Matsuda
*Lynn Redgrave as Olga Belinskaya
*Vanessa Redgrave as Princess Vera Belinskaya
*Madeleine Potter as Grushenka
*Madeleine Daly as Katya
*Lee Pace as Crane
*Allan Corduner as Samuel Feinstein John Wood as Prince Peter Belinski

==Production==
In The Making of The White Countess, a bonus feature on the DVD release of the film, production designer Andrew Sanders discusses the difficulties he had recreating 1930s Shanghai in a city where most pre-war remnants are surrounded by modern skyscrapers and neon lights. Many of the sets had to be constructed on soundstages. Also impeding him were restrictions on imports levied by the Chinese government, forcing him to make do with whatever materials he could find within the country.

The film proved to be the last for producer Ismail Merchant, who died shortly after principal photography was completed.  

The film premiered at the Savannah Film Festival in Savannah, Georgia and was shown at the Two River Film Festival in Monmouth County, New Jersey before going into limited release in the US. It opened on ten screens and earned $46,348 on its opening weekend, ranking #34 among all films in release. It eventually grossed $1,669,971 in the US and $2,422,711 in foreign markets for a total worldwide box office of $4,092,682. 

==Critical reception== The English Patient is a racing, dramatic pulse. Its sedate tone is simply too refined for the story it has to tell. Mr. Ishiguros prim, anemic screenplay is so lacking in drive and emotional gravitas that the actors are left with only scraps of lean dramatic meat to tear into."  

Roger Ebert of the Chicago Sun-Times stated, "Fiennes and Richardson make this film work with the quiet strangeness of their performances" and then observed, "I saw my first Merchant and Ivory film, Shakespeare Wallah, in 1965 ... Sometimes they have made great films, sometimes flawed ones, even bad ones, but never shabby or unworthy ones. Here is one that is good to better, poignant, patient, moving."  

Mick LaSalle of the San Francisco Chronicle said of the film, "Measured and meticulous, with small patches of narrative awkwardness that are more than compensated for by rich performances, its an appropriate finish to the 40-year partnership: a typical, above-average Merchant-Ivory film ... The movie has a slow start, but Ivory is laying in foundations for later ... Long before the climax, which is magnificent, the movie has us completely believing in the characters and their histories and marveling at their extraordinary circumstances. This is Merchant-Ivorys kind of showmanship, the unflashy adult variety of movie magic that they made their hallmark."  
 Chekovian sight of so many Richardson-Redgraves lamenting their circumstances in heavily Russian-accented English and pining for Hong Kong, where their former social glory will be restored, makes you wonder if theyd have been better off in a stage production of Three and a Half Sisters: The Twilight Years ... The White Countess takes place in a fascinating time and place, rife with conflict and turmoil. But to watch Fiennes float (and Richardson trudge) through it all, absorbed in themselves and their own private misery, is to wish theyd started falling earlier, if only to knock some sense into them."  

Peter Travers of Rolling Stone rated the film three out of four stars and commented, "The convoluted screenplay ... makes it hard for director James Ivory to maintain an emotional through-line. But Richardson ... finds the storys grieving heart. Fiennes is her match in soulful artistry. As the last film from the legendary team of Ivory and producer Ismail Merchant ... The White Countess is a stirring tribute to Merchant, a true builder of dreams in an industry now sorely bereft of his unique spirit."   

Justin Chang of Variety (magazine)|Variety stated, "The threads come together ever so slowly in The White Countess ... This final production from the team of James Ivory and the late Ismail Merchant is itself adrift in more ways than one, with a literate but meandering script ... that withholds emotional payoffs to an almost perverse degree. Name cast and typically tasteful presentation should spark biz among sophisticated older viewers, though likely a fraction of what the Merchant Ivory pedigree used to command theatrically."  

==Awards and nominations== John Bright was nominated for the Satellite Award for Best Costume Design, and Michael Barry, Martin Czembor, Ludovic Hénault, and Robert Hein were nominated for the Satellite Award for Best Sound.

==References==
 

==External links==
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 