Linda Lovelace for President
{{Infobox film
| name           = Linda Lovelace for President
| image          = LindaLovelaceForPresident.jpg
| image_size     = 
| caption        = Promotional publicity poster 
| director       = Claudio Guzman David Winters Charles Stroud
| writer         = Jack S. Margolis
| narrator       = 
| starring       = Linda Lovelace Micky Dolenz Scatman Crothers Joe E. Ross Vaughn Meader Chuck McCann
| music          = Bic Mac & The Truckers
| release        = 
| cinematography = Robert Birchall
| editing        = Richard Greer
| released       = March 1, 1975 (USA)
| runtime        = 95 minutes United States of America
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| distributor    = General Film Corp.
}}
 1975 David David Winters Deep Throat.

==Plot==

A committee of independent U.S. political party leaders have gathered to join forces and select a candidate for the upcoming presidential election.  One of the committee members flippantly suggests nominating Linda Lovelace.  The committee approaches the porn star, who agrees to be the flag bearer of the newly formed Upright Party.  Lovelace’s campaign takes her on a cross-country tour, where she meets voters in stops ranging from crowded big cities to isolated rural towns.  Lovelace’s popularity, however, threatens the Washington, D.C., establishment, and her political rivals dispatch a hit man known as The Assassinator to bring a fatal end to the Lovelace campaign. 

==Production==
 David Winters David Winters to establish the star’s crossover appeal with mainstream moviegoers. Winters came up with the idea for the film after observing the strong positive reaction that college students exhibited towards Linda Lovelace during her speeches at various college campuses.    
 The Monkees,  as a near-sighted bus driver, Scatman Crothers as a pool hall hustler, Joe E. Ross as a political operative, Vaughn Meader as a preacher who lusts after Lovelace, and Chuck McCann as The Assassinator.  However, much of the film played up Lovelaces starring role in Deep Throat with jokey reminders of the X-rated films oral sex subject matter (i.e., the slogan for the Lovelace campaign is "A vote for Linda is a blow for democracy"). 
 David Winters, was one of the film’s producers.   
  

Part of the film was shot on the campus of the University of Kansas in Lawrence, Kansas,  and at Swope Park in Kansas City, Missouri. 

==Reception==
Linda Lovelace for President was theatrically released in X-rated, R-rated and PG-rated versions.   None of these versions was commercially successful.  Linda Lovelace made no further films after this production.

Over the years, bootleg versions of Linda Lovelace for President were released on home video.  A commercial DVD version was scheduled for release in August 2008 on the Dark Sky/MPI label. 

==References==
 

==External links==
* 
 

 
 
 