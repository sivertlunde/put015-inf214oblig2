Kevin Hart: Let Me Explain
{{infobox film
| name           = Kevin Hart: Let Me Explain
| image          = Kevin Hart Let Me Explain.jpg
| border         = 
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Leslie Small
| producer       = Jeff Clanagan Blake W. Morrison
| starring       = Kevin Hart
| music          = Kennard Ramsey
| cinematography = Larry Blanford
| editing        = Spencer Averick CodeBlack Films Hartbeat Productions
| distributor    = Summit Entertainment
| released       =   
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = $2.5 million   
| gross          = $32,318,051 
}}
Kevin Hart: Let Me Explain is a 2013 American Stand-up comedy|stand-up comedy film featuring Kevin Harts 2012 performance at Madison Square Garden. It was released in theaters on July 3, 2013. The film was produced by Hartbeat Productions and distributed by Summit Entertainment.

==Plot==
Kevin is throwing a celebration party, but guests tell him that he has changed since his divorce. He tries to explain himself, but they will not let him, so he decides to do a show at Madison Square Garden letting him explain, which sold out with 30,000 people. The film also shows performances of his sold-out shows from all around the world.

==Reception==

===Critical response===
Kevin Hart: Let Me Explain has received positive reviews, with many critics praising Harts stand-up outlook and compared the film to Eddie Murphy Raw.  Rotten Tomatoes gives the film a score of 63% based on reviews from 40 critics. The sites consensus is: "Its ironically rather short on standup from Mr. Hart himself, but structural problems aside, Kevin Hart: Let Me Explain offers another example of why hes achieved mainstream success on his own terms." 

==Home media==
Kevin Hart: Let Me Explain was released on DVD and Blu-ray on October 15, 2013. which was also the date of the second season premiere of Real Husbands of Hollywood, a comedy-improv reality television parody on BET created by Hart.

==Soundtrack==
Erick Sermon, Snoop Dogg, Method Man, and featuring RL performed a song "Let Me Explain", based on the film. The music video is available on the HartBeat Productions YouTube channel and also available on iTunes.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 