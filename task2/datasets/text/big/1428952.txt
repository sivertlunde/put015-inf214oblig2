Performance (film)
 
 
 
{{Infobox film
| name           = Performance
| image          = Performance poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Donald Cammell Nicolas Roeg
| producer       = Sanford Lieberson
| writer         = Donald Cammell
| starring       = James Fox Mick Jagger
| music          = Jack Nitzsche Jagger/Richards
| cinematography = Nicolas Roeg
| editing        = Antony Gibbs Brian Smedley-Aston Frank Mazzola  
| studio         = Goodtimes Enterprises
| distributor    = Warner Bros.
| released       =  
| runtime        = 105 minutes
| country        = United Kingdom
| language       = English
| budget         = £750,000
}}
Performance is a 1970  . The film was produced in 1968 but not released until 1970.

==Plot==
Chas (James Fox) is a member of an east London gang led by Harry Flowers (Johnny Shannon); his speciality is intimidation through violence as he collects pay-offs for Flowers. He is very good at his job and has a reputation for liking it. His sexual liaisons are casual and rough. When Flowers decides to take over a betting shop, owned by Joey Maddocks (Anthony Valentine), he forbids Chas to get involved, as he feels Chass complicated personal history with Maddocks (which is at least partly gay) may lead to trouble. Chas is angry about this and later humiliates Maddocks, who retaliates by wrecking Chass apartment and attacking Chas. Chas shoots him, packs a suitcase and runs from the scene. When Flowers makes it clear that he has no intention of offering protection to Chas but instead wants him eliminated, Chas decides to head for the countryside to hide but instead winds up hiding out in London, requesting a trusted friend to help him get out of the country.

He assumes a new name, Johnny Dean, and appears at the house of Turner (Mick Jagger), makes a clumsy attempt to ingratiate himself with Pherber (Anita Pallenberg), one of the female inhabitants and moves in. Turner is a reclusive, eccentric former rock star who has "lost his demon" and who lives there with his female friends Pherber and Lucy (Michele Breton), with whom he enjoys a non-possessive and bi-sexual ménage à trois. At first, Chas is contemptuous of Turner and Turner attempts to return the rent paid in advance but they start influencing each other. Chas also enjoys intimate moments with Pherber, during which he shows his bisexual tendencies. Pherber and Turner understand his conflict and want to understand what makes him function so well within his world. To speed up the process they make him take hallucinogenic drugs (Amanita muscaria). After that evening Chas opens up, he begins a caring relationship with Lucy, implying that he outgrew the psychological boundaries he was stuck in, due to having to function as a stereotypically masculine man within a gangster world.

At the end of the film, Turner is shot by Chas and Pherber is last seen hiding in a cupboard. Chas seems to agree to be welcomed back to his former boss Harry Flowers by Rosie (Stanley Meadows), another Flowers thug; we understand that they are going to kill him. As the car drives off, the face we see through the window is ambiguous – it could be Chas or it could be Turner.

As the last of the credits roll the car, breasting a hill in the middle distance, fades out of existence leaving an empty road where it should have been.

==Cast==
* James Fox as Chas/Johnny Dean
* Mick Jagger as Turner
* Anita Pallenberg as Pherber
* Michèle Breton as Lucy
* Ann Sidney as Dana
* John Bindon as Moody
* Stanley Meadows as Rosebloom
* Allan Cuthbertson as The Lawyer
* Anthony Morton as Dennis
* Johnny Shannon as Harry Flowers
* Anthony Valentine as Joey Maddocks
* Kenneth Colley as Tony Farrell
* John Sterland as The Chauffeur
* Laraine Wickens as Lorraine Billy Murray as Thug 1

==Production== swinging 60s romp. At one stage, Cammells friend Marlon Brando (with whom he later collaborated on the posthumously published novel Fan Tan) was to play the gangster role which became "Chas". At that stage the story involved an American gangster hiding out in London. James Fox, previously cast in rather upper crust roles, eventually took the place of Brando and spent several months in South London among the criminal underworld researching his role. 
 Argentinian writer A Hard Days Night (1964). Instead, Cammell and Roeg delivered a dark, experimental film which included graphic depictions of violence, sex and drug use.

It was intended that the Rolling Stones would write the soundtrack but due to the complicated nature of the various relationships on and off-screen, this never happened. It was widely rumoured that Anita Pallenberg, then in a relationship with Keith Richards and Mick Jagger played out sexual scenes in the film for real (out-takes of these scenes apparently won a prize at an Amsterdam adult film festival). When Keith Richards heard the rumours, he apparently took to sitting in his car outside the house where the film was being shot. Needless to say, this didnt do much for the Jagger-Richards musical chemistry and the soundtrack came together from a number of sources.

The film has gained notoriety due to the difficulties it faced in getting on screen. The films content was a surprise to the studio. It has been reported that during a test screening, one Warner executives wife vomited in shock.  Nic Roeg notes in   wanted the negative to be destroyed.
 ICA on 18 October 1997, incorporating a talk by film theorists (including Colin MacCabe, who went on to write a guide to the film), a screening of the uncut UK edition and finally a question and answer session. Those in attendance included James Fox (and family), Pallenberg, set designer Christopher Gibbs and Cammells brother, who introduced part of a video interview with Donald Cammell, shot just before his death. Mick Jagger was originally to appear but was committed to the Rolling Stones Bridges to Babylon Tour.

The Region 1 DVD was released on 13 February 2007 and elsewhere soon after. Although the film has undergone significant restoration, one famous line of dialogue – Jaggers "Heres to old England!" heard during the Memo from Turner sequence – has been removed. This is because at this crucial stage of the film (the music sequence) one of the stereo channels has been used on both channels. Other music and sound effects are also missing from this scene on the DVD release (some drums, the throbbing sound as Turner plugs a lead into his music generator and the shrieking sound at the climax of his fluorescent light tube dance). These sounds, the dialogue and the music are all audible on other releases of the film. The voices of Harry Flowers and the young maid in Turners mansion, have been restored to the voices of the original actors. When the film was first released in the United States and also on the VHS releases, their voices were dubbed because the studio had feared that Americans would find their Cockney accents difficult to understand.

==Critical reputation== Time Out magazine "all-time greats" poll of critics and directors.  After Cammells death in 1996, the films reputation grew still further. It is often cited as a classic of British cinema.

In the September–October 2009 issue of Film Comment, Mick Jaggers Turner was voted the best performance by a musician in a film. 
 Mark Cousins says: "Performance was not only the greatest seventies film about identity, if any movie in the whole Story of Film should be compulsory viewing for film makers, maybe this is it."

It holds a 84% rating on Rotten Tomatoes, based on 31 reviews.

==Influence==
Several aspects of the film were novel and it foreshadowed MTV type music videos (particularly the Memo from Turner sequence in which Jagger sings) and many popular films of the 1990s and 2000s.
* The gangster aspect of Performance has been imitated by many popular directors such as Quentin Tarantino, Guy Ritchie and Jonathan Glazer.
* Performance pushed boundaries by featuring explicit sex scenes and drugs, which have been rumoured to be real instead of simulated. Although Andy Warhols (and other underground filmmakers) films had featured such behaviour before Performance, it was unprecedented that they appeared in a studio production. Coil (on Loves Secret Domain) also includes dialogue from the film.
* Happy Mondays second album, Bummed, features several songs inspired by the film, including "Moving in With, "Performance" and "Mad Cyril". "Mad Cyril" is explicitly inspired by the film and includes the following dialogue from the film:
** "I like that, turn it up"
** "It was Mad Cyril!"
** "What about the detector vans, they be right with you?"
** "Weve been courteous"
** "Put the frighteners on the flash little twerp"
** "Lets have a look, lets have a look, excuse me, but... Come in!–take it off, take it off"
** "Its a right pisshole, long hair, beatniks, druggers, freeloaders, tsk, freeloaders"
** "I need a bohemian atmosphere"
* Also inspired by the movie were the 79 Mod Revival act, Secret Affair whose East End following known as The Glory Boys were based on the South London gangsters portrayed in the film. Glory Boys was also the title of their first album.
* In keeping with the intellectual bent of Jaggers character, legendary Argentine writer Jorge Luis Borges is quoted numerous times during the film. His photograph appears in the brief montage which follows Turners shooting.
* Beat the Devil, the BMW promo film directed by Tony Scott and starring James Brown, Gary Oldman and Clive Owen contains at least two references to Performance. At one point Owens character says "I know a thing or two about performing" – a quote from Chas. The Devil, played by Oldman, dances with a fluorescent tube, just as Turner does in Performance. In the earlier Tony Scott film True Romance, Gary Oldman (as Drexl) is seen swinging a lampshade back and forth in front of someone, as Turner does during the "Memo from Turner" sequence.
* Cult film director Harmony Korine was possibly inspired directly by Performance in casting James Fox and Anita Pallenberg as an impersonating couple (the Pope and the Queen) in his film Mister Lonely. The Charlatans Tim Burgess adopting the Mick Jagger slick-backed hair look. Also, in the video for "Jesus Hairdo", we see him dancing with a neon strip light lifted from the film.
* Writer-director Paul Schrader has often cited Performance as one of his favourite films. In a 2007 article for Film Comment, he describes the films influence.
*  , makes several references to Performance in its second issue, "Paint it Black", prominently featuring Mick Jaggers Turner character. The plot of the issue is about The Stones in the Park concert that took place after the death of Brian Jones and shows just how Turner "lost his demon."

==Soundtrack==
 
The soundtrack album was released by Warner Bros. Records on 19 September 1970. The album features Mick Jagger, Ry Cooder, Randy Newman, The Last Poets, Buffy Sainte-Marie and Merry Clayton.

==See also==
* List of films featuring hallucinogens

==Further reading==
* {{cite book
| last          = MacCabe
| first         = Colin
| authorlink    = Colin MacCabe
| title         = Performance
| url           = http://books.google.ca/books/about/Performance.html?id=GMNZAAAAMAAJ&redir_esc=y
| accessdate    = 30 April 2012 BFI Film Classics
| date          = 1 July 1998 BFI Publishing
| location      = London
| isbn          = 0-85170-670-3
}}
* {{cite book
| last          = Brown
| first         = Mick
| authorlink    = Mick Brown (journalist)
| title         = Mick Brown on Performance
| series        = Bloomsbury Movie Guides
| date          = 20 November 1999
| publisher     = Bloomsbury USA
| isbn          = 1-58234-043-9
}}
* {{cite book
| last          = Buck
| first         = Paul
| title         = Performance: The Biography of a 60s Masterpiece
| date          = 10 September 2012
| publisher     = Omnibus Press
| isbn          = 1-84938-700-1}}

==References==
 

Bibliography
* Ali Catterall and Simon Wells, Your Face Here: British Cult Movies Since The Sixties (Fourth Estate, 2001) ISBN 0-00-714554-3

==External links==
*  
*  
*  
*  
*  
*   at  
*   by Colin MacCabe (book)

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 
 