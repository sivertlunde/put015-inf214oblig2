Aksharathettu
 

{{Infobox film
| name           = Aksharathettu
| image          = 
| image size     = 
| caption        = 
| director       = I. V. Sasi
| producer       =
| writer         = 
| narrator       =  Urvasi Sudha Sudha Jagathy Mukesh Lizy Lizy Janardhanan Janardhanan Jagannatha Varma Kaviyoor Ponnamma Shyam
| editing        = 
| released       = 1989
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Aksharathettu is a 1989 Malayalam film directed by I. V. Sasi, starring Suresh Gopi, Mukesh (actor)|Mukesh, Jagathy Sreekumar, and Urvashi (actress)|Urvasi. The movie is a remake of the Hollywood movie Fatal Attraction, starring Michael Douglas and Glenn Close.

==Plot==

The film features the story of three bank employees and their wives. Bank manager Prakash (Suresh Gopi) is an ideal husband, while his wife Sumati (Urvashi (actress)|Urvasi) is emotional and complaining. They have a perfect family with a smart boy, a terraced house and a maruti car. Accountant James (Mukesh (actor)|Mukesh) is a womanizer and his wife Elsi (Lizy (actress)|Lizy) is fed up with his activities. Cashier Gowthaman (Jagathy Sreekumar) is struggling to adjust with his North Indian, Hindi-speaking wife Ambika as he has to take care of all the household chores and look after their baby. There is a tangential storyline involving a suspicious husband named Thankappan (Kuthiravattam Pappu) and Radha, a servant in Prakashs home.

Then enters Renuka Menon (Sudha (actress)|Sudha), a beautiful widow who arrives at the bank to open an account and gets charmed by the handsome Prakash. Sumati leaves for her parents (Jagannatha Varma and Kaviyoor Ponnamma) house to celebrate her fathers birthday while Prakash is busy with a visit by the regional manager (Janardhanan (actor)|Janardhanan) at the bank.

The rest of the story is Prakashs struggle to be faithful to his wife.

==Cast==

*Suresh Gopi Urvasi
*Sudha Sudha
*Jagathy Sreekumar Mukesh
*Lizy Lizy
*Janardhanan Janardhanan
*Jagannatha Varma
*Kaviyoor Ponnamma

==Soundtrack==

The film contains a hit song, "Hridayam Kondezhuthiya Kavitha", composed by Shyam (composer)|Shyam.

==External links==
*  

 
 
 


 