Into the Dark (film)
{{Infobox film
| name           = Into the Dark
| image          = Film poster, Into the Dark.jpg
| alt            = 
| caption        = Promotional poster
| director       = Mark Edwin Robinson
| producer       = David C. Robinson Christine Holder Mark Holder Danny Roth
| writer         = Mark Edwin Robinson
| starring       = Mischa Barton Ryan Eggold Leah Pipes Jaz Martin
| music          = Jesse Voccia
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Sony Pictures Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $5 million
| gross          = 
}} same name. On 14 May 2012, Epic Pictures Group released an international trailer for the film.  The film premiered at the Hollywood Film Festival on 20 October 2012, where it was nominated for the Best Feature Film award.   The film was released theatrically in the United States on 11 October 2013. 

==Plot==
Sophia Monet is a young woman battling depression after losing both her parents in the last six months. She becomes increasingly isolated and convinces herself that shell never see her parents again, dismissing any notion of an afterlife. She is drawn out of this flux when she meets a new love interest, Adam. After Adams mysterious disappearance, Sophia becomes determined to track him down. Her search leads to an eerie apartment building, where passing the threshold means leaving the living and entering the realm of the dead.  

==Cast==
*Mischa Barton  as Sophia Monet
*Ryan Eggold as Adam Hunt
*Jaz Martin as Sam
*Leah Pipes as Astrid
*John Rubinstein as Dr. Thomas
*Melinda Y. Cohen as Penny
*Frank Ashmore as Mr. Carter
*Jim Tooey as Officer Mason
*Kris Wheeler as Officer Darby
*Talon Reid as Tortured soul
*Zack Tiegen as Tom
*Jessee Foudray as Funeral Goer with Baby

==Release==
In May 2012, Screen Daily reported that Epic Pictures Group made several deals that licenses the film for overseas distribution. The film will be released by Tiberius Film in Germany, Eagle in the Middle East and EuroFilms for Peru, Ecuador and Bolivia. 

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 