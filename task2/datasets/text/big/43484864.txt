The Pyramid (film)
 
{{Infobox film
| name           = The Pyramid
| image          = The Pyramid (film).jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Grégory Levasseur
| producer       = Alexandre Aja Mark Canton Chady Eli Mattar Scott C. Silver 
| writer         = Daniel Meersand Nick Simon James Buckley Daniel Amerman
| music          = Nima Fakhrara 
| cinematography =
| editing        = Scott C. Silver 
| studio         = Silvatar Media Fox International Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 89 minutes  
| country        = United States
| language       = English
| budget         = $6.5 million
| gross          = $14.3 million   
}} found footage James Buckley and Daniel Amerman.

The film was released on December 5, 2014, by 20th Century Fox and was widely panned by critics.

==Plot== Egyptian protests in 2013. An archaeological team discover a vast pyramid buried under the Egyptian desert; a pyramid that has three sides and not four like the pyramids of Giza, Egypt. Using satellite technology they determine the pyramid to be 600 feet deep. A tunnel that leads into the apex of the pyramid is discovered and upon opening, releases toxic air which poisons a worker. Shortly afterwards the team is ordered to leave the site because of the uprising in Giza. The team, composed of father and daughter Miles and Nora Holden, argue over leaving the dig site because they are uncertain of when they can return. Eventually they agree on sending in a remote controlled robot to survey the first few rooms and document the pyramid. Shorty, the robot, enters the pyramid and after examining a small portion of the structure is attacked by an unknown creature and goes offline. After Shortys destruction by unknown means, they make their way inside to recover it. They rapidly become lost, and a section of floor collapses beneath them, wounding and trapping 
Zahir (Amir K) pinning his leg to the ground by fallen debris. While attempting to climb back up, Sunni (Nicola) is scratched across the face by an unseen creature and falls. Leaving Zahir behind to find another way out, they hear him scream, and return to find only a bloody trail leading up the wall.

They are pursued through a narrow tunnel by creatures revealed to be cats that have survived for nearly thousands of years by cannibalism. When a soldier, Shadid, finds and rescues them, he is then pulled back into the tunnel by an unseen and powerful force. Shortly after escaping a sand trap, Sunni is pushed into a spike pit and fed on by the scavenger cat beasts, dying shortly after her fatal wounds. After finding a burial chamber and speculating about an escape route, Dr. Miles Holden (OHare) has his heart torn out from behind. His daughter, Nora, and the cameraman Fitzie (Buckley) flee, though Fitzie shortly returns to discover Anubis weighing Miles heart against Maat to determine entry to the afterlife. Miles is deemed unworthy and crumbles to dust.  It is afterwards they learn that the pyramid was constructed to imprison Anubis, who was unforgiving and merciless in his goal to reunite with his father/creator, Osiris.  Finding a journal from a Freemason explorer, an occupant who had previously discovered the pyramid years ago, they find a way out, but are chased after by Anubis, and Fitzie is discovered and killed. Nora is temporarily captured and soon to be judged by Anubis, but manages to escape with the help of the scavenger cats, and passes out near the pyramids exit. She wakes to discover a child above her, before Anubis lunges at them both.

== Cast ==
*Ashley Hinshaw as Dr. Nora Holden
*Denis OHare as Dr. Miles Holden James Buckley as Terry "Fitzie" Fitsimmons
*Daniel Amerman as Luke
*Amir K as Michael Zahir
*Joseph Beddelem as Taxi driver
*Garsha Arristos as Egyptian Worker
*Christa Nicola as Sunni Marsh
*Omar Benbrahim as Chubby Intern
*Philip Shelley as The Provost

== Release ==
On July 7, 2014, 20th Century Fox picked up the distribution rights to the film, and was set the for a release date for December 5, 2014.  The release date was dropped and went into a direct-to-video premiere on May 5, 2015 on DVD and Blu-ray & on video on demand on April 17, 2015. 

=== Box office ===
;North America
Released on December 5, 2014, in 589 theatres,    the film underperformed, earning only $1.3 million.  Although investors originally expected the film to make more than $20 million domestically, it made less than $2.8 million.  

;Other territories
Outside North America, the film was released on the same day in 18 markets including the U.K., Russia and Vietnam.  It earned $3.8 million from 16 international markets. 

==Reception==
 
The Pyramid received extremely negative reviews from critics, with Moviefone and CraveOnline both naming it one of the worst films of 2014.   On Rotten Tomatoes, the film has a rating of 9%, based on 34 reviews, with an average rating of 2.7/10. The sites consensus reads "Poorly lit and thinly writ, The Pyramid houses little more than clunky dialogue, amateurish acting, and dusty found-footage scares." 

Lelsie Felperin of The Hollywood Reporter called it "a stinker in every sense."  Drew Taylor of The Playlist wrote, "The Pyramid is cursed. Only, instead of an ancient evil, its just plagued by inept filmmaking."  Alonso Duralde of The Wrap said, "The ultimate moral of The Pyramid, and of almost every other film where an underground mummy or scarab or tomb wreaks havoc, is that some things were never meant to be uncovered. Some movies, too." 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 