Woman of Fire
{{Infobox film name           = Woman of Fire image          = Woman of Fire.jpg caption        = Poster for Woman of Fire (1971) director       = Kim Ki-young  producer       = Jung Jin-woo writer         = Kim Ki-young starring       = Namkoong Won Youn Yuh-jung music          = Han Sang-ki cinematography = Jung Il-sung editing        = Kim Hee-su distributor    = Woo Jin Films Co., Ltd. released       =   runtime        = 98 minutes country        = South Korea language       = Korean budget         =  gross          = 
| film name =   hanja          =   rr             = Hwanyeo mr             = Hwanyŏ context        = }}
}} 1971 South Korean film directed by Kim Ki-young. This was the second film in Kims Housemaid trilogy followed by Woman of Fire 82.

==Synopsis== The Housemaid (1960 in film|1960). The lives of a composer and his wife, who live on a chicken farm, are thrown into turmoil when a femme fatale joins their household. 

==Cast==
* Namkoong Won... Dong-shik 
* Jeon Gye-hyeon... Jeong-suk
* Youn Yuh-jung... Myeong-ja
* Choi Moo-ryong... Detective
* Kim Ju-mi-hye
* O Yeong-a... Hye-ok
* Hwang Baek
* Chu Seok-yang... Ki-ja
* Lee Hoon  
* Lee Ji-yeon

==Awards==
* Festival de Cine de Sitges 
**Special Mention Best Actress (Youn Yuh-jung)
* Blue Dragon Film Awards 
** Best Director (Kim Ki-young)

==Notes==
 

==Bibliography==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 