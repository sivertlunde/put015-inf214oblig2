The Wonders of Aladdin
{{Infobox film
| name           = The Wonders of Aladdin
| image          = The Wonders of Aladdin.jpg
| alt            = 
| director       = Mario Bava Henry Levin
| producer       = Joseph E. Levine Massimo Patrizi
| writer         = Luther Davis Franco Prosperi Silvano Reina Duccio Tessari Marco Vicario
| story          = Stefano Strucchi
| starring       = Donald OConnor Noëlle Adam Vittorio De Sica Aldo Fabrizi Michèle Mercier
| music          = Angelo Francesco Lavagnino
| cinematography = Tonino Delli Colli
| editing        = Gene Ruggiero
| studio         = Compagnie Cinématographique de France Embassy Pictures Corporation Lux Film
| distributor    = Metro-Goldwyn-Mayer
| released       = 31 October 1961  (Italy)  13 December 1961  (United States)  21 February 1962  (France) 
| runtime        = 100 min.
| country        = France Italy United States
| language       = English ITL 157,000,000  (Italy) 
}}

The Wonders of Aladdin (  fantasy film directed by Mario Bava and Henry Levin and presented by Joseph E. Levine for Metro-Goldwyn-Mayer|MGM. The film stars Donald OConnor as the title character. At this time, OConnor was reaching the decline of his career after many years of fame.

== Cast ==

* Donald OConnor as Aladdin
* Nöelle Adam as Djalma
* Vittorio De Sica as Geni
* Aldo Fabrizi as Sultan
* Michele Mercier as Princess Zaiha
* Milton Reid as Omar
* Terence Hill as Prince Moluk (credited as Mario Girotti)
* Fausto Tozzi as Grand Vizier
* Marco Tulli as Fakir

== Critical reception ==

AllRovi|Allmovie, while not particular favorable toward the film, called it "a fun movie". 

According to MGM records it made a loss of $276,000.  . 

== References ==

 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 

 