Southside 1-1000
{{Infobox film
| name           = Southside 1-1000
| image          = Southside 1-1000 movie poster.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Boris Ingster
| producer       = {{plainlist|
* Frank King
* Maurice King
}}
| screenplay     = Boris Ingster Leo Townsend
| story          = {{plainlist|
* Bert C. Brown
* Milton M. Raison
}}
| narrator       = Gerald Mohr
| starring       = {{plainlist|
* Don DeFore
* Andrea King
* George Tobias
}}
| music          = Paul Sawtell
| cinematography = Russell Harlan
| editing        = Christian Nyby
| studio         = King Brothers Productions
| distributor    = Allied Artists Pictures
| released       =   
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Southside 1-1000 is a 1950 semidocumentary-style film noir directed by Boris Ingster and featuring Don DeFore, Andrea King, George Tobias and Gerald Mohr as the off-screen narrator. 

==Plot==
Based on a true story, the US secret service goes after a gang of counterfeiters, whose engraver (Morris Ankrum) has secretly constructed his plates while in prison. A federal agent (Don DeFore) poses as the counterfeiters contact man in order to purchase enough bills to incriminate the gang.

==Cast==
* Don DeFore as John Riggs/Nick Starnes
* Andrea King as Nora Craig
* George Tobias as Reggie
* Barry Kelley as Bill Evans
* Morris Ankrum as Eugene Deane
* Robert Osterloh as Albert
* Charles Cane as Harris
* Kippee Valez as Singer
* Joe Turkel as Frankie John Harmon as Nimble Willie
* G. Pat Collins as Hugh B. Pringle - Treasury Agent
* Douglas Spencer as Prison Chaplain
* Joan Miller as Mrs. Clara Evans William Forrest as Prison Warden

==Production==
The final fight-to-the-death scene was filmed aboard Los Angeles "Angels Flight", a cable-car service hanging 40 feet above the ground.   

==Reception==

Film critic Craig Butler of Allmovie wrote, "Southside 1-1000 is a good pseudo-noir film told in pseudodocumentary fashion, but it also must register as a bit of a disappointment. Its functional and all the parts fit together smoothly, making it run like a fairly well-oiled machine -- but it lacks real spark. Given director Boris Ingsters impressive work on the seminal Stranger on the Third Floor, one expects something a bit more unusual or off the beaten path -- or at least distinctive. Instead, Southside looks like it could have been the work of any competent director."   The New York Times wrote, "In the cinemas library of routine gangster fiction, Southside 1-1000 merits a comfortable middle-class rating being neither especially exciting nor particularly dull."   Michael Barrett of PopMatters rated it 4/10 stars and called it "an unnecessary and forgettable entry in the genre". 

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images)
*  

 
 
 
 
 
 
 
 