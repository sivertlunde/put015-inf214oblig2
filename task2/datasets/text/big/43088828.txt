Captain Wronski
{{Infobox film
| name = Captain Wronski 
| image =
| image_size =
| caption =
| director = Ulrich Erfurth
| producer = Willie Hoffmann-Andersen 
| writer =  Axel Eggebrecht   Michael Graf Saltikow
| narrator =
| starring = Willy Birgel   Elisabeth Flickenschildt   Antje Weisgerber   Ilse Steppat
| music = Norbert Schultze 
| cinematography = Igor Oberberg 
| editing =  Hermann Ludwig    
| studio = Apollo-Film   Deutsche London-Film 
| distributor = Deutsche Film Hansa
| released = 11 October 1954  
| runtime = 99 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Captain Wronski (German:Rittmeister Wronski) is a 1954 West German thriller film directed by Ulrich Erfurth and starring Willy Birgel, Elisabeth Flickenschildt and Antje Weisgerber. A Polish officer works undercover in 1930s Berlin to discover Nazi Germanys plans against his homeland. The casting of Birgel in the title role referenced his best-known performance during the Nazi era when he had played another Rittmeister in Riding for Germany (1941). 

==Cast==
* Willy Birgel as Rittmeister Wronski  
* Elisabeth Flickenschildt as Jadwiga, seine Schwester 
* Antje Weisgerber as Illse von Jagstfeld  
* Ilse Steppat as Leonore Cronberg  
* Irene von Meyendorff as Liane von Templin  Paul Hartmann as Oberst Ranke  
* Claus Holm as Dornbusch  
* Olga Tschechowa as Frau von Eichhoff 
* Axel Monjé as Major Momenbek  
* Volker von Collande as Major Kegel 
* Rudolf Forster as Oberst Maty   Ernst Schroder as Stepan  
* Marina Ried as Susi im RWM  
* Ernst Stahl-Nachbaur as Ein deutscher Abwehrgeneral  
* Rolf von Nauckhoff as SS-Unterführer  
* Hilde Körber as Gefangenenaufseherin 
* Margarete Schön as Gefängnisbeamtin  
* Charles Regnier as Vorsitzender Volksgerichtshof  
*Harald Holberg 
*Alexa von Porembsky 
*Paul Heidemann 
*Erich Dunskus 
*Hans Stiebner 
*Ingrid Lutz
*Karl Ludwig Schreiber 
*Walther Süssenguth

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 


 