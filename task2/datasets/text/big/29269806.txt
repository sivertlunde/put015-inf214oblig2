The Singing Cop (film)
{{Infobox film
| name = The Singing Cop
| image =
| image_size =
| caption =
| director = Arthur B. Woods
| producer = Irving Asher Brock Williams
| starring = Keith Falkner Chili Bouchier
| music = Benjamin Frankel
| cinematography = Basil Emmott
| editing = Warner Brothers-First First National Productions
| released = January 1938
| runtime = 78 minutes
| country = United Kingdom
| language = English
}} 1938 British musical comedy spy drama, directed by Arthur B. Woods and starring singer Keith Falkner and Chili Bouchier. The film was a quota quickie production, based on a short story by Kenneth Leslie-Smith. The Singing Cop is now classed as a lost film. 

==Plot==
A temperamental opera diva arouses official suspicion that she is a spy, secretly gathering classified information to pass to enemy agents. A policeman who happens to be a talented amateur singer is sent undercover to join the opera company and try to find out whether there is any substance to the allegations. Once there, an immediate attraction springs up between the policeman and a female member of the company. But the diva also sets her sights on him and, used to getting what she wants, becomes the bitter rival-in-love of the other singer. The policeman lets his lady friend into his confidence, and the pair set about sleuthing. They finally prove that all the suspicions were justified and the diva is indeed a foreign agent.

==Cast==
* Keith Falkner as Jack Richards
* Chili Bouchier as Kit Fitzwillow
* Marta Labarr as Maria Santova
* Ivy St. Helier as Sonia Kassona
* Athole Stewart as Sir Algernon Fitzwillow
* Bobbie Comber as Bombosa
* Glen Alyn as Bunty Waring
* George Galleon as Drips Foster-Hanley
* Ian McLean as Zabisti
* Vera Bogetti as Rosa
* Frederick Burtwell as Dickie
* Robert Rendel as Sir Treves Hallam
* Brian Buchel as Pemberton
* Derek Gorst as Captain Farquhar

==References==
 

== External links ==
*  
*   at BFI Film & TV Database

 

 
 
 
 
 
 
 


 