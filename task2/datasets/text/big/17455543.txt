Hunger (2008 film)
 
 
{{Infobox film
| name           = Hunger
| image          = Hunger2008Poster.jpg
| alt            = 
| caption        = UK release poster Steve McQueen
| producer       = Laura Hastings-Smith Robin Gutch
| writer         = Enda Walsh Steve McQueen
| starring       = Michael Fassbender Liam Cunningham  David Holmes
| cinematography = Sean Bobbitt Joe Walker
| studio         = Film4 Productions Channel 4 Northern Ireland Screen Broadcasting Commission of Ireland Wales Creative IP Fund
| distributor    = Icon Film Distribution
| released       =  
| runtime        = 96 minutes  
| country        = Ireland
| language       = English 
| budget         =
| gross          = £1,707,277   
}} historical drama Steve McQueen and starring Michael Fassbender, Liam Cunningham, and Liam McMahon, about the 1981 Irish hunger strike. It was written by Enda Walsh and McQueen.
 Grand Prix BAFTA nominations, 2009 Irish Film and Television Awards|IFTAs, winning six at the event. 
 volunteer and Brendan "The Maze Prison in the period leading up to the hunger strike and Sands death.

==Plot==
Prison officer Raymond Lohan prepares to leave work; he cleans his bloodied knuckles, checks his car for bombs, puts his uniform on, and ignores his comrades.
 IRA prisoner, arrives; he is categorised as a  "non-conforming prisoner" for his refusal to wear the prison uniform. He is sent to his cell naked with only a blanket. His cellmate, Gerry, has smeared the walls with faeces from floor to ceiling. The two men get to know each other and we see them living within the cell. Gerrys girlfriend sneaks a radio in by wrapping it and keeping it in her vagina.

Prison officers forcibly and violently remove the prisoners from their cells and beat them before pinning them down to cut their long hair and beards, grown as part of a no-wash protest. The prisoners resist, Sands spitting into Lohans face, who responds by punching him in the face and then swings again, only to miss and punch the wall, causing his knuckles to bleed. He cuts Sands hair and beard; the men throw him in the bath tub and scrub him clean before hauling him away again. Lohan is then seen smoking a cigarette, as in the opening scenes, his hand bloodied.

Later, the prisoners are taken out of their cells and given second-hand civilian clothing. The guards are seen snickering as they are handed to the prisoners who respond, after Sands initial action, by tearing up the clothes and wrecking their cells. For the next interaction with the prisoners, a large number of riot officers are seen coming into the prison on a truck. They line up and beat their batons against their shields and scream to scare the prisoners, who are hauled from their cells, then thrown in between the lines of riot police where they are beaten with the batons by at least 10 men at one time. Lohan and several of his colleagues then probe first their anuses and then their mouths, using the same pair of latex gloves for each man. One prisoner head-butts a guard and is beaten brutally by a riot officer. One of the riot officers is seen crying while his colleagues, on the other side of the wall, brutally beat the prisoners with their batons.

Lohan visits his catatonic mother in a retirement home. He is shot in the back of the head by an IRA assassin and dies slumped onto his mothers lap.
 Donegal where he and his friends found a foal by a stream that had cut itself on the rocks and broken its back legs. Sands drowned the foal and tells the priest, although he got into trouble, he knew he had done the right thing by ending its suffering. He then says he knows what he is doing and what it will do to him, but he says he will not stand by and do nothing. The rest of the film shows Sands well into his hunger strike, with weeping sores all over his body, kidney failure, low blood pressure, stomach ulcers, and the inability to stand on his own by the end. In the last days, while Sands lies in a bath, a larger orderly comes in to give his usual orderly a break. The larger orderly sits next to the tub and shows Sands his knuckles, which are tattooed with the letters "Ulster Defence Association|UDA". Sands tries to stand on his own and eventually does so with all his strength, staring defiantly at the UDA orderly who refuses to help him up, but then he crumbles in a heap on the floor with no strength left to stand. The orderly carries him to his room. Sands parents stay for the final days, his mother being at his side when Sands dies, 66 days after beginning the strike.
 MP for Fermanagh and South Tyrone while he was on strike. Nine other men died with him during the seven-month strike before it was called off. 16 prison officers were murdered by paramilitaries throughout the protests depicted in the film. Shortly afterwards, the British government conceded in one form or another virtually all of the prisoners demands despite never officially granting political status.

==Cast==
 
* Michael Fassbender as Bobby Sands
** Ciaran Flynn as 12-year-old Bobby
* Liam Cunningham as Father Dominic Moran
* Liam McMahon as Gerry Campbell Stuart Graham as Raymond Lohan
* Brian Milligan as Davey Gillen
* Laine Megaw as Mrs Lohan
* Karen Hassan as Gerrys girlfriend
* Frank McCusker as the Governor
* Lalor Roddy as William
* Helen Madden as Mrs Sands
* Des McAleer as Mr Sands
* Geoff Gatt as Bearded man
* Rory Mullen as Priest
* Ben Peel as Riot Prison Officer Stephen Graves
* Helena Bereen as Raymonds mother
* Paddy Jenkins as Hitman
* Billy Clarke as Chief Medical Officer
* B.J. Hogg as Loyalist orderly
* Aaron Goldring as Young Bobbys friend
 

==Production==
  Broadcast Commission of Ireland, Channel 4, Film4 Productions, and the Wales Creative IP Fund.
 Liam Cunningham tries to talk Bobby Sands out of his protest. In it, the camera remains in the same position for the duration of the shot. To prepare for the scene, Cunningham moved into Michael Fassbenders apartment for a time while they practised the scene between twelve and fifteen times a day. 

==Release== premiered at Cannes Film Un Certain Toronto International Film Festival on 6 September,  the New York Film Festival on 27 September,   and the Chicago International Film Festival on 19 October 2008. 

The film was released in the United Kingdom and Ireland on 31 October 2008.

===Critical reception===
   
Hunger received widespread critical acclaim by critics, audiences and at festivals all over the world. The film has a rating of 90% on Rotten tomatoes based on 115 reviews with an average score of 7.8 out of 10. The consensus states "Unflinching, uncompromising, vivid and vital, Steve McQueens challenging debut is not for the faint hearted, but its still a richly rewarding retelling of troubled times."  Metacritic, another review aggregator, assigned the film a weighted average score of 82 (out of 100) based on 25 reviews from mainstream critics, considered to be "universal acclaim". It is currently among the sites highest-rated films. 

  of The Guardian scored the piece a maximum five stars, writing, "There is an avoidance of affect and a repudiation of the traditional liberal-lenient gestures of dialogue, dramatic consensus and narrative resolution. This is a powerful, provocative piece of work, which leaves a zero-degree burn on the retina." while praising McQueens work, "Hunger shows that McQueen is a real film-maker and his background in art has meant a fierce concentration on image, an unflinching attention to what things looked like, moment by moment." 

Lauren Wissot of Slant Magazine spoke profoundly of the film, "Hunger, with all its visual, sonic and editing elements flowing together in harmony like a five-star, six-course meal, exemplifies the phrase  ." and that, "McQueens film is a nuanced masterpiece that never flaunts its artistry, but uses it humbly to serve the all-important story."  Nigel Andrews of the Financial Times heralded the film and its director, Steve McQueen, "McQueen understands the first principle of cinema. On either side of its middle section, where the very wordiness stands ironic witness to the ultimate impossibility to explain, Hunger has the power and hieratic integrity of silent cinema."  Matthew De Abaitua of Film4 scored the film five out of five stars, "Intense, disturbing and powerful mix of vision and detail: a recreation of a terrible time combined with a vivid and distinctive artistic sensibility. Truly powerful filmmaking."  
 

  highlighted "Hunger may be criticized for being willfully arty, or for reducing a complex political situation to a broadly allegorical vision of martyrdom, but its never less than visually stunning." 

Critic J. Hoberman of The Village Voice noted praise calling the film, "A superbly balanced piece of work" and a "compelling drama thats also a formalist triumph." Hoberman went on to say, "Ive seen Hunger three times, and with each screening, the spectacle of violence, suffering, and pain becomes more awful and more awe-inspiring."  Lisa Schwarzbaum of Entertainment Weekly gave Hunger an A- on an A+ to F scale and stated, "For your art-house pleasure and discomfort, heres one of the most talked-about film-festival triumphs of 2008, a disturbingly avid re-creation of the last six weeks in the life and slow, self-imposed wasting of Irish hunger striker Bobby Sands." 

Writing for The New York Post, Kyle Smith praised McQueens film, "Regardless of politics, one must grant McQueens substantial gifts, which bring to mind Paul Greengrass in another Northern Ireland film, Bloody Sunday."  Liam Lacey of The Globe and Mail gave the film a maximum of four out of four stars, "Hunger -- the disturbing, provocative, brilliant feature debut from British director Steve McQueen -- does for modern film what Caravaggio did to Renaissance painting."  Reyhan Harmanci of The San Francisco Chronicle wrote, "Its horrific. But Hunger displays uncommon intelligence and visual panache, transcending the goal of making the situation seem real. It feels more than real. Its art." in addition to praising McQueens work by stating "Steve McQueen is a well-known visual artist turned feature film director who makes you wish more moviemakers went to art school."  Ann Hornaday of The Washington Post was very praising of the film, writing, "McQueen has taken the raw materials of filmmaking and committed an act of great art." and calling the piece "An artistic masterpiece."  

Wendy Ide of The Times praised McQueens directing and screenwriting talents by stating, "Ultimately, the one thing that cant be questioned is McQueens bold and unflinching talent." 

===Accolades===
  
The film appeared on some critics top ten lists of the best films of 2008. Andrea Gronvall of Chicago Reader named it the 3rd best film of 2008,  and Scott Foundas of LA Weekly named it the 3rd best film of 2008 (along with Che (2008 film)|Che).   

Hunger was voted the best film of 2008 by the British film magazine Sight & Sound,  and that year McQueen received the Discovery Award and $10,000 at the 33rd annual Toronto film festival.  It also won in the best film category at the 2009 Evening Standard British Film Awards.  The film also was named the "Best Film of 2009" by the Toronto Film Critics Association Awards; it shared the award with Quentin Tarantinos Inglourious Basterds. Director McQueen won the Carl Forman BAFTA Award for "Special Achievement by a British Director, Writer or Producer for their First Feature Film". 

==References==
 

==External links==
*  
*  
*  
*   – New York Film Festival press conference with Steve McQueen on Hunger
*   – Ronan Bennetts memoir and film review, The Guardian
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 