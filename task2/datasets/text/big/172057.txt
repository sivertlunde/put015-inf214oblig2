The Divine Lady
{{Infobox film
| name           = The Divine Lady
| image          = The-Divine-Lady-1929.jpg
| caption        = Film poster
| director       = Frank Lloyd
| producer       = Frank Lloyd Walter Morosco Richard A. Rowland
| writer         = Forrest Halsey Harry Carr (intertitles)
| based on       =  
| starring       = Corinne Griffith Victor Varconi H.B. Warner Ian Keith
| music          = Cecil Copping
| cinematography = John F. Seitz Hugh Bennett
| studio         = First National Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 99 minutes 
| country        = United States
| language       = English
| budget         =
I| Ingress       = samey21
}} Nathaniel (Nat) Shilkret. The song became a popular hit in 1929 and was recorded by numerous artists such as Nat Shilkret, Frank Munn, Ben Selvin (as The Cavaliers), Smith Ballew, Adrian Schubert, Sam Lanin, and Bob Haring.

The film was adapted by  .  It was directed by Frank Lloyd.
 Best Actress Best Cinematography. 

This is also the only film to ever win Best Director without a Best Picture nomination. Though one year earlier, Two Arabian Knights won for Academy Award for Best Director of a Comedy Picture, without being nominated for Best Picture.

==Plot==
In the late eighteenth century, Lady Hamilton has had a somewhat turbulent relationship with the British people, especially the aristocracy. Born Emma Hart from a very humble background (she being the daughter of a cook), she was seen as being vulgar by the rich, but equally captivating for her beauty. In a move to protect his inheritance, Honorable Charles Greville, Emmas then lover and her mothers employer, sent Emma to Naples under false pretenses to live with his uncle, Sir William Hamilton, where she would study to become a lady. Surprisingly to Greville whose deception Emma would eventually discover, Emma ended up becoming Hamiltons wife in a marriage of convenience. But it is Emmas eventual relationship with Horatio Nelson of the British navy that would cause the largest issue. A move by Lady Hamilton helped Nelsons armada defeat Napoleons fleet in naval battles, which Nelson would have ultimately lost without Lady Hamiltons help. Beyond the dangers of war, Lady Hamilton and Nelsons relationship is ultimately threatened by the court of public opinion as both are married to other people.

==Cast==
*Corinne Griffith - Emma Hart
*Victor Varconi - Horatio Nelson
*H.B. Warner - Sir William Hamilton
*Ian Keith - Honorable Charles Greville
*Marie Dressler - Mrs. Hart
*Montagu Love - Captain Hardy
*William Conklin - Romney
*Dorothy Cumming - Queen Maria Carolina

==Preservation==
The film still survives intact along with its Vitaphone soundtrack. This film was a joint preservation project of the UCLA Film and Television Archive and the Museum of Modern Art Department of Film in cooperation with the Czechoslovak Film Archive. It was restored in conjunction with the project American Moviemakers: The Dawn of Sound. 

==Home Media==
In 2009, the film was released on manufactured-on-demand DVD by the Warner Archive Collection.

==References==
 

== External links ==
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 
 
 
 