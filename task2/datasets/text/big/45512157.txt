Leon of the Table D'hote
 
{{Infobox film
| name           = Leon of the Table Dhote
| image          = 
| caption        = A surviving film still
| director       =
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = Motion Picture Distributing and Sales Company
| released       =  
| runtime        =
| country        = United States English inter-titles
}}
 silent short short comedy produced by the Thanhouser Company. The film follows Leon, a waiter at a table dhote restaurant who is in love with Rosa, a French cashier at the same restaurant. Leon goes on vacation and poses as a foreign noble, attracts the interest of Violet Hopes mother as a suitable candidate to marry her daughter. While at the beach, Leon is knocked over by a breaking wave and Violet rescues him, earning Leons gratitude. Rosa arrives after tracking Leon down and forces him to confess and return to the restaurant. Violets mother then allows her daughter to marry the man of her choice. No cast or staff credits are known for the production. The film was released on October 4, 1910, and was met with praise by the reviewer of the The New York Dramatic Mirror. The film is presumed lost film|lost.

== Plot ==
Though the film is presumed   restaurant and makes desperate love to the fat French cashier, Rosa. Off on his vacation, Leon decides to pose as a foreign nobleman. At a seaside hotel where he stops, he becomes all the rage. One of the guests at the same hotel is a beautiful young heiress, a Violet Hope, whose designing mother at once conceives the idea of marrying her off to the supposed count. The plan does not meet with Violets approval, she is already in love with a native born. One day while bathing in the surf, Violet, who is an expert swimmer, comes to the assistance of the bogus count, who has been knocked over by a breaker, and delivers him safely into the hands of the lifeguard. The count at once lays his life and fortune at Violets feet, greatly to that young ladys disgust and her mothers delight. In the meantime Rosa becomes acquainted with the doings of her absent lover and traces him to the beach. Finding him in the water, and beyond her reach, Rosa also dons a bathing suit and after a chase through the waves, captures Leon and forces him to confess that he has been sailing under false colors. He was led back to the restaurant by the triumphant Rosa, while Violet obtains her mothers consent to wed the man of her choice." 

== Production == George Middleton, Grace Moore, John W. Noble, Anna Rosemond, Mrs. George Walters. 

== Release and reception ==
The single reel comedy, approximately 1,000 feet long, was released on October 4, 1910.    The film had a wide national release, with theaters showing the film in South Dakota,  Indiana,  Oklahoma,  Pennsylvania,  Missouri,  Washington (state)|Washington,  Kansas,  Maryland,  and New Hampshire. 

The film review by Walton of The Moving Picture News and The Moving Picture World bear an unusual expression about laying siege to an heiress that is used in both summaries. Bowers says both reviews were probably adapted from a "canned" review supplied by Thanhouser.  Though it is uncertain if the review by The Moving Picture World which was issued on October 15 was picked up and reused by the The Moving Picture News for the November 5 edition.  The New York Dramatic Mirror reviewer offered praise for the production in its summary, "The Thanhouser players get considerable humor out of this comedy which is quite cleverly constructed and well acted. Leon is a waiter in a table dhote restaurant. He bids his fat sweetheart, the restaurant cashier, good-bye and goes on his vacation, stopping at a seaside resort where he poses as a foreign count, captivates the ladies, and finds a rich mamma who wants to marry him to her daughter. The engagement gets into the papers, his fat sweetheart in town reads the news and takes the next train to the seaside hotel, where she makes short work of Leon, leading him off by the ear, to the great delight of the rich girl and her American lover. Amusing by-play is worked in, and the film pleases."  An advertisement for the Jewel Theatre in Winfield Daily Free Press claimed it to be better than Home Made Mince Pie. 

== References ==
 

 
 
 
 
 
 
 
 