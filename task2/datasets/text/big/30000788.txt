The Lady (1925 film)
{{infobox film
| name           = The Lady
| image          =Norma Talmadge and Wallace MacDonald in The Lady 01.jpg
| caption        =Norma Talmadge and Wallace MacDonald
| director       = Frank Borzage
| producer       =  Norma Talmadge  Joseph Schenck
| writer         =  Martin Brown (play)  Frances Marion (scenario)
| starring       =  Wallace MacDonald  Norma Talmadge
| music          =
| cinematography = Tony Gaudio
| editing        =
| distributor    = First National Pictures
| released       =  January 26, 1925 (New York City premiere)  February 8, 1925 (nationwide)
| runtime        = 8 reels; 7,357 feet
| country        = United States
| language       = Silent film (English-language intertitles)
}}
 silent drama film starring Norma Talmadge and directed by Frank Borzage.

Talmadges own production company produced the film with distribution by First National Pictures.

The film survives in the U.S. Library of Congress with reel 2 missing. The remaining elements of the film have severe beginning stages of nitrate decomposition making much of the film hard to follow. 

==Plot==
 

==Origins==
Like many of Talmadges silents of the 1920s The Lady is derived from a stage play. The play, The Lady, ran on Broadway from December 4, 1923 to February 1924 at Charles Frohmans Empire Theatre. It was produced by  A. H. Woods. Mary Nash played Talmadges part of Polly Pearl and Elizabeth Risdon played Fanny Le Clare which in the film was played by Doris Lloyd. Also in the cast was child actor Junior Durkin soon to find bigger fame in films. 

==Cast==
 
 
*Norma Talmadge as Polly Pearl
*Brandon Hurst as St. Aubyns Sr
*Wallace MacDonald as Leonard St. Aubyns
*Paulette Duval as Madame Adrienne Catellier
*Emily Fitzroy as Madame Blanche Johnny Fox (billed as John Fox Jr.) as Freckles
*Alfred Goulding as Tom Robinson
*George Hackathorne as Leonard Cairns
 
*John Herdman as John Cairns
*Ed Hubbell (billed as Edwin Hubbell) as London Boy
*Doris Lloyd as Fannie St. Clair Walter Long as Blackie
*Miles McCarthy as Mr. Graves
*Marc McDermott as Mr. Wendover
*Margaret Seddon as Mrs. Cairns
 

==See also==
*1925 in film

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 


 