Yolki
{{Infobox film
| name     = Yolki
| image    = Yolki2010.jpg
| caption  = Russian poster
| director = Timur Bekmambetov Yaroslav Chevazhevskiy Ignas Jonynas Dmitriy Kiselev Aleksandr Voytinskiy
| producer = Timur Bekmambetov Iva Stromilova Sergey Yahontov
| writer = Dmitriy Aleynikov Andrey Kureychik Oleg Malovichko Givi Shavgulidze
| starring = Alina Bulynko   Sergey Pokhodaev
| cinematography = Sergey Trofimov
| editing = Andrey Mesnyankin
| music = 
| country = Russia
| language = Russian
| runtime = 90 minutes
| released = 13 December 2010 (Kazakhstan) 16 December 2010 (Russia)
}}

Yolki ( ), also known as Six Degrees of Celebration, is a 2010 Russian comedy film directed by Timur Bekmambetov. Two sequels have been released, including Yolki 2 in 2011 and Yolki 3 in 2013. The films in the series represent a Russian tradition of the "New Years Movie" where films that take place during the holiday season tap into the vein of hope, optimism, and possibility associated with New Years in the Russian culture. Other examples include The Irony of Fate and The Irony of Fate 2 (also directed by Bekmambetov). Typically such films are released in December, just before the start of the holidays in Russia. 

== Sequels ==
The film has had 2 sequels: Yolki 2 in 2011 and Yolki 3 in 2013 with most of the cast reprising their roles, as well as a prequel called Yolki 1914 in 2014.

== Cast ==
* Alina Bulynko – Varvara
* Sergey Pokhodaev – Vova
* Ivan Urgant – Boris
* Sergey Svetlakov – Evgeniy
* Elena Plaksina – Olya
* Vera Brezhneva – Vera Brezhneva
* Nikita Presnyakov – Pasha Bondarev – taksist (cab driver)
* Boris Khvoshnyanskiy – Fyodor
* Artur Smolyaninov – Aleksey
* Sergey Garmash – Valeriy Sinitsyn – kapitan militsii (police captain)
* Ekaterina Vilkova – Alina

== External links ==
* 

 

 
 
 
 

 
 