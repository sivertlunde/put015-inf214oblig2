White Pongo
{{Infobox film
| name           = White Pongo
| image          = White Pongo.jpg
| image_size     =
| caption        =
| director       = Sam Newfield
| producer       = Sigmund Neufeld
| writer         = Raymond L. Schrock (story)
| narrator       =
| starring       = See below
| music          =
| cinematography = Jack Greenhalgh
| editing        = Holbrook N. Todd
| distributor    =
| released       = 10 October 1945
| runtime        = 71 minutes 10 minutes (US short version)
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

White Pongo is a 1945 American film directed by Sam Newfield. The film is also known as Adventure Unlimited in the United Kingdom.

== Plot summary ==
In the jungles of the Belgian Congo, a group of natives are dancing around a great fire with a human sacrifice named Gunderson, They are attacked by an albino gorilla called The White Pongo.  During the attack an elderly scientist who lives with the tribe frees Gunderson and gives him his deceased colleagues diary that contains his findings on the white gorilla.
 missing link. Rhodesian Secret Secret Serviceman. The group battle the jungle and one another. In the climax, the White Pongo, who has been stalking the group, kidnaps one of the Safari members and duels with a regular gorilla. The rest of the safari hear the battle and rescue their comrade. The White Pongo comes out victorious over his rival but is wounded by the safari and taken with them to the boat back to London.

== Cast == Richard Fraser as Geoffrey Bishop
*Maris Wrixon as Pamela Bragdon
*Lionel Royce as Peter Van Dorn
*Al Eben as Hans Kroegert Gordon Richards as Sir Harry Bragdon
*Michael Dyne as Clive Carswell George Lloyd as Baxter
*Larry Steers as Doctor Kent
*Milton Kibbee as Gunderson
*Egon Brecher as Dr. Gerig
*Joel Fluellen as Mumbo Jumbo

Uncredited:  Ray "Crash" Corrigan as White Pongo.  Corrigan, a Western actor, was an experienced "gorilla man," and played a similar role earlier that year in "The White Gorilla," where he starred both as jungle explorer and as the gorilla.

== Soundtrack ==
  Leo Erdody was the musical director.

== External links ==
* 
* 

 

 
 
 
 
 
 
 

 