The Abandoned (1955 film)
{{Infobox film
| name           = Gli Sbandati (The Abandoned)
| image          = GliSbandati.jpg
| image_size     =
| caption        =
| director       = Francesco Maselli
| producer       = Antonio Pellizari
| writer         = Eriprando Visconti (story and writer), Francesco Maselli (writer), Ageo Savioli (writer) Antonio De Mario Girotti
| music          = Giovanni Fusco
| cinematography = Gianni Di Venanzo
| editing        = Antonietta Zita
| distributor    =
| released       =  
| runtime        = 102 minutes
| country        = Italy
| language       = Italian
| budget         =
| gross          =
}} Italian film Gli Sbandati, set during the aftermath of the Allied invasion of Italy in 1943 during World War II. The film entered the 1955 Venice Film Festival, where it received a special mention.   It is the directorial debut of  Francesco Maselli. 
 100 Italian films to be saved. Massimo Bertarelli, Il cinema italiano in 100 film: i 100 film da salvare, Gremese Editore, 2004, ISBN 88-8440-340-5.
   

==Plot==
 

==Cast==
*Lucia Bosé - Lucia
*Isa Miranda - Contessa Luisa
*Jean-Pierre Mocky - Andrea
*Goliarda Sapienza - Lucias aunt Antonio De Teffè - Carlo
*Leonardo Botta - Ferruccio
*Marco Guglielmi - Scattered soldiers officer
*Giuliano Montaldo - Scattered soldier from Tuscany
*Ivy Nicholson - Andreas girlfriend
*Fernando Birri - Scattered soldiers lieutenant
*Franco Lantieri - Scattered soldier from Veneto
*Giulio Paradisi
*Joop van Hulzen Mario Girotti Manfred Freidbager
*Bianca Maria Ferrari
*Dori Ghezzi

==References==
 

==External links==
* 

 
 
 
 
 


 