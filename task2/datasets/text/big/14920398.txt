The Wayward Bus (film)
{{Infobox Film
| name           = The Wayward Bus
| image          = Waywardbusmovieposter.gif
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Victor Vicas
| producer       = Charles Brackett
| screenplay     = Ivan Moffat
| based on       =  
| writer         = Ivan Moffat
| narrator       = 
| starring       = Jayne Mansfield Joan Collins Dan Dailey Rick Jason
| music          = Leigh Harline 
| cinematography = Charles G. Clarke
| editing        = Louis R. Loeffler
| distributor    = 20th Century Fox
| released       =   
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $1,465,000 
| gross          = $1,750,000 (US rentals) 
}} 1957 drama drama film 1947 novel of the same name by John Steinbeck.

==Plot==
Alice Chicoy (Collins) is the wife of driver Johnny (Jason). He owns a small and rundown little bus that makes side trips. Alice is the owner of a little restaurant and likes liquor a bit too much.

Unhappy with what has become of her life, she decides to "surprise" her husband mid-way through his bus trip. Among the passengers, Camille Oakes (Mansfield) is a shamed burlesque dancer on the way to a well-paying job in San Juan. Camille gets caught up in a flirtation with traveling salesman Ernest Horton (Dailey).

Most of the story takes place on the charter bus. Slowly making their way through a treacherous California mountain region, the passengers undergo a variety of life-altering experiences. The journey has its most profound effects upon the iconoclastic salesman and the lonely stripper.

==Cast==
* Jayne Mansfield as Camille Oakes
* Joan Collins as Alice Chicoy
* Dan Dailey as Ernest Horton
* Rick Jason as Johnny Chicoy
* Betty Lou Keim as Norma, the counter girl
* Dolores Michaels as Mildred Pritchard
* Larry Keating as Elliott Pritchard
* Robert Bray as Morse

==Background== Bus Stop (starring Marilyn Monroe) film adaptation but instead ended up crafting the Steinbeck novel into what one commentator called "the kind of lowbrow schlock the novel had satirized". 
 Will Success Broadway hit that ran from October 1955 to May 1956 and starred Mansfield as Rita Marlowe and featuring Orson Bean. Waiting for Rock Hunter to be available for filming, Fox kept Mansfield busy. Wayward Bus was made shortly after Girl Cant Help It to keep Mansfields name in the publics eye. In The Wayward Bus, however, Mansfield would play Camille Oakes seriously, rather than as the light-headed Jerri Jordan of The Girl Cant Help It.

Joanne Woodward was intended to play "Mildred Pritchard", but Woodward dropped out to star in The Three Faces of Eve, and the role went to Dolores Michaels, her first acting role.      United Press International said in a review of the film that Michaels "torrid" scene, a seduction scene in a hayloft where she makes a pass at the bus driver (Rick Jason), "manages to steal the sexiest scene in the picture," over better known actresses Jayne Mansfield and Joan Collins and said Hollywood had not had a scene like it since Jane Russell in The Outlaw.  Director Victor Vicas shot two versions, an "A" scene and a "B" scene because of the censors. 
 12 Angry Men.   

The film was never officially released on DVD  because Steinbecks widow bought back the rights  and refuses to let it be released because Steinbeck was greatly displeased with the final result. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 