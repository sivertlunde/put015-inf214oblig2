Tuna Wranglers
 
Tuna Wranglers (2007) is a documentary film produced by the makers of Deadliest Catch. It follows the exploits of the southern bluefin tuna industry in South Australia as it captures wild fish and transports them to grow-out pens where the fish are fattened up for sale to the Japanese market. It features footage of fishermen diving into tuna pens to wrestle sharks in order to protect their multi-million dollar stock.  The film was directed and produced by Mark Strickson for television and has also been released on DVD  in several regions. Critics of the film noted the omission of the southern bluefin tunas conservation status and the incongruous casting of an American narrator given the exclusively Australian cast and story.  The film was produced by NHNZ for the Discovery Channel. A year after its release, the film was acknowledged by the southern bluefin tuna industry as having helped raise its public profile and boosted related tourism activity in the town of Port Lincoln. 

== Setting ==
The film follows fishermen from the Stehr Group as they journey from the fishing town of Port Lincoln on Eyre Peninsula along the continental shelf to the north-west and into the Great Australian Bight. A brief stop-over in the town of Streaky Bay also features, but most of the story transpires at sea in the Southern Ocean.

== Versions ==
The duration of the Region 4 DVD release is 90 minutes  and features alternative cover art to the Region 2 release, which runs 120 minutes. 

== Origins == National Geographic channel. It was directed and produced by Mike Bhana and included topside footage from Motion Pacific.  The film received awards at a number of film festivals in 2003 and 2004.  Unlike Tuna Wranglers, it was not released on DVD.

== References ==
 

 
 
 