Accident (2009 film)
 
 
 
{{Infobox film
| name           = Accident 
| image          = Accident (2009 film) poster.jpg
| caption        = Promotional poster
| producer       = Johnnie To Soi Cheang 
| writer         = Szeto Kam-Yuen Nicholl Tang
| starring       = Louis Koo Richie Jen Michelle Ye
| music          = Xavier Jamaux
| cinematography = Fung Yuen-Man
| editing        = David M. Richardson Media Asia Films Milkyway Image Media Asia Distribution 
| released       =  
| runtime        = 86 minutes
| country        = Hong Kong
| language       = Cantonese
}} thriller directed Soi Cheang, produced by Johnnie To and starring Louis Koo, and Richie Jen. Accident competed at the 66th Venice International Film Festival, and was released theatrically in Hong Kong on 17 September 2009.

The film was released on DVD and Blu-ray Disc in Region 1 by Shout! Factory in 2012.

== Plot ==

In the opening scene, a man is killed in what appears to be an accident. However, it is revealed that the man was actually murdered, with the supposed accident actually orchestrated by a group of killers led by the Brain (Louis Koo). The group specializes in killing their victims using elaborate schemes that mask the murders as if they were accidents. Uncle (Stanley Fung), one of the team members, accidentally leaves a cigarette butt at the crime scene, and it is found by the Brain. After an argument, Uncle vows to quit smoking afterwards.

The team then gets a job from a son that wants to kill his own father, likely for insurance money. They plan an accident to kill their target via electrocution, but the plan requires for it be to raining at a specific time. The team tries to execute their plan many times but fail due to the rain falling later than necessary. During one run, Uncle smokes a cigarette and begins to show signs of Alzheimers disease, forgetting that he made a vow to stop smoking. However, the rain falls on time, and the team decides to go forth with their plan. Uncle forgets to play his part in the murder, but the murder is successfully executed nonetheless.

Unexpectedly, after the murder, a bus loses control and almost runs over the Brain. The Brain manages to get out of the buss path, but the bus then runs over Fatty (Lam Suet), another member of the team, killing him on the scene. The Brain takes Fattys belongings, with Fatty asking the Brain whether it really was an accident before he dies. The Brain then returns home to find police officers investigating his flat, which was broken into.

The Brain becomes paranoid and is convinced that everything was a setup. He trails the son to an insurance company the next day. The Brain sees the son arguing with an insurance agent (Richie Jen) over what appears to be money, leading the Brain to suspect that the insurance agent was behind the bus accident. The Brain rents a flat on the floor directly below the insurance agents flat, and begins to perform secret surveillance on the agent.

One day, the Brain trails the client to find the Woman (Michelle Ye) picking up the payment for the job at a parking garage. However, only Fatty and the Brain should have known about the pickup. The Woman explains to the Brain that Fatty had told her about the client, but the Brain, suspecting her betrayal, kills her and takes the money. After he leaves, as he is walking on the sidewalk, the client (the son) falls from the roof of the same building and is dead. The Brain develops further suspicion of the insurance agent, who he can see talking on the phone and later hear him talking about the client on the phone in his flat.

In a later date, the Brain receives a call from Uncle for help. However, the Brain is too late and finds Uncle severely injured, apparently dropping two stories from a building near the crime scene in the opening scene. The Brain decides to take revenge against the insurance agent and devises a scheme to kill the agent by reflecting the suns rays to blind a car to run over the agent in what would appear to be an accident.

However, in the middle of the plans execution, there is a solar eclipse, blocking the sunlight. At this time, the Brain receives a call from Uncle in the hospital, who tells the Brain that Fattys death really was an accident. Uncle had accidentally dropped a bag of toy balls that night, which led to the loss of control of the bus.

Realizing he has made a mistake and that the agent is innocent, the Brain runs and tries to cover the windshield from reflecting light. The agent sees and recognizes the Brain as he is running, but the Brain is too late, and a blinded driver runs over the agents wife, killing her.

On a later date, as the Brain is leaving the rented flat, he is confronted and stabbed by the insurance agent in the staircase, who asks the Brain why he had to harm them. The Brain looks at the sky and thinks about his own wife before he dies.

== Cast ==
* Louis Koo as Ho Kwok-Fai, the Brain
* Richie Jen as Chan Fong-chow
* Michelle Ye as the Woman
* Lam Suet as Fatty
* Stanley Fung as Uncle
* Monica Mok as the Brains wife

==Awards and nominations==
29th Hong Kong Film Awards
* Nominated: Best Screenplay (Szeto Kam Yuen, Nicholl Tang and Milkyway Creative Team)
* Nominated: Best Supporting Actor (Feng Tsui Fan)
* Won: Best Supporting Actress (Michelle Ye)
* Nominated: Best Film Editing (David Richardson)

== Reception ==
The film opened to lukewarm reviews.

Perry Lam, of Muse (Hong Kong magazine)|Muse magazine, writes, "the movie dutifully fulfills the rules of the genre but offers few incidental pleasures." 

== References ==
 

== External links ==
*   (English version)
*   MediaAsia (English version)
*  
*   of Accident at Yahoo! Movies plus the trailer, images and making-of videos.

 
 

 
 
 
 
 
 
 
 
 