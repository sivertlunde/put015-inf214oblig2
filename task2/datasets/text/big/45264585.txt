Daddy (2015 film)
{{Infobox film
| name           = Daddy
| image          = 
| caption        = 
| director       = Gerald McCullouch
| producer       = 
| writer         = Dan Via
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
}}

Daddy is an American comedy-drama film, slated for release in 2015.  The directorial debut of Gerald McCullouch, the film is based on the play by Dan Via.

The film stars McCullouch and Via as Colin McCormack and Stewart Wisniewski, two gay men in their late 40s. Longtime friends whose relationship has taken on many of the emotional undercurrents and routines of a non-sexual marriage,  their bond is tested when Colin begins dating a younger man (Jaime Cepero). 

The films cast also includes Brooke Anne Smith, Jay Jackson, Tamlyn Tomita, Scott Henry, John Rubinstein, Mackenzie Astin, Richard Riehle and Leslie Easterbrook.

==Background==
McCullouch and Via starred in the original stage production of Daddy, which was staged in New York City  and Los Angeles  in 2010. The film adaptation was funded in part by a Kickstarter campaign in 2013. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 

 