Badge 373
{{Infobox film
| name           = Badge 373
| image          = Badge 373 poster.jpg
| image_size     =
| caption        = theatrical poster Howard Koch
| producer       = Howard Koch Jim Di Gangi
| writer         = Pete Hamill
| starring       = Robert Duvall Verna Bloom Henry Darrow Eddie Egan
| music          = J.J. Jackson (singer)|J.J. Jackson
| cinematography = Arthur J. Ornitz John Woodcock 
| art director   = Philip Rosenberg
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       = 25 July 1973 (NYC)
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  $1,100,000 (US/ Canada rentals) 
}} crime thriller Howard Koch, and stars Robert Duvall as Ryan, with Verna Bloom, Henry Darrow and Eddie Egan himself as a police lieutenant.

The film was not successful, either at the box office or with the critics. 

==Plot==
Eddie Ryan (Robert Duvall), a tough, no-nonsense, abrasive and racist Irish NYPD cop, has to turn in his badge after scuffling with a Puerto Rican suspect who then falls to his death from a rooftop, but that doesnt stop him from heading out on a one-man crusade to find out who killed his partner of three years, Gigi Caputo (Louis Cosentino), all the while neglecting his new live-in girlfriend, Maureen (Verna Bloom).  Ryans search leads him to Puerto Rican drug kingpin Sweet Willie (Henry Darrow), and a shipment of guns for Puerto Rican Puerto Rican independence movement|independenistas.

==Cast==
*Robert Duvall as Eddie Ryan
*Verna Bloom as Maureen
*Henry Darrow as William Salazar a.k.a. Sweet William
*Eddie Egan as Lt. Scanlon
*Felipe Luciano as Ruben 
*Tina Cristiani as Mrs. Caputo
*Marina Durell as Rita Garcia
*Chico Martinez as Frankie Diaz
*Jose Duval as Ferrer
*Louis Cosentino as Gigi Caputo
*Luis Avalos as Chico
*Nubia Olivero as Mrs. Diaz
*Sam Schacht as Assistant D.A.
*Edward F. Carey as The Commissioner
*"Big" Lee as Junkie in casino
*Duane Morris as Gay in casino
*John Marriott as Superintendent
*Joe Veiga as Manuel (Botica   Proprietor) 
*Mark Tendler as Harbor Lights bouncer
*Robert Weil as Hans 
*Rose Ann Scamardella as Herself
*Pete Hamill as Reporter
*Larry Appelbaum as Copo at toll booth John McCurry as Bus driver
*Bob Farley as Patrolman
*Tracey Walter as Delivery boy
*John Scanlon, Jimmy Archer, Ric Mancini, Mike ODowd as Tugboat crew
*Robert Miano, Pompie Pomposello, Hector Troy as Sweet William  s hoods
*Miguel Alejandro, Harry Collazo, Damian Colon as "Ruben  s gang
*Johnny Pachero & his Orchestra as Band at Carorrojeno  s
*Orestes Matacena as Drug Dealer

Cast notes:
* Journalist Pete Hamill, who wrote the screenplay, has a bit part as a reporter named Pete, in the sequence of Gigis wake, while WABC-TV anchorwoman Rose Ann Scamardella, later the inspiration for Gilda Radners Saturday Night Live character "Roseanne Roseannadanna", plays herself in a cameo appearance. Dominican salsa salsa bandleader Johnny Pachero & his Orchestra makes a cameo appearance in the opening nightclub sequence. Stafford, Jeff   

==Production== 42nd Street. FDR Drive, with the United Nations headquarters visible.  

On August 10, 1973, Paramount Pictures rejected a demand by the Puerto Rican Action Coalition to withdraw the film for what the coalition called the movies racism. 

==Crew== Howard Koch
*Producer: Howard Koch
*Screenplay: Pete Hamill
*Music composed and conducted by: J.J. Jackson (singer)|J.J. Jackson
*Director of photography: Arthur J. Ornitz John Woodcock
*Associate producer: Lawrence Appelbaum
*Assistant to the producer: Irwin Yablans
*Inspired by the exploits of: Eddie Egan
*Art director: Philip Rosenberg
*Costumes: Frank Thompson
*Second unit director: Michael Moore 
*Production manager: Jim DiGangi 
*1st assistant director: Michael P. Petrone
*2nd assistant directors: Robert Grand, Gerrold   T. Brandt, Jr.
*Technical advisor: Eddie Egan
*Script supervisor: Roberta Hodes
*Casting: Bernie Styles
*Set decorator: Edward Stewart
*Set dresser: Gary Brink
*Hair stylist: Vern Caruso
*Property master: Al Griswold
*Costumer: George Newman
*Special effects: Conrad Brink
*Sound recording: Dennis Maitland
*Re-recording: John Wilkinson

==Critical response==
The critical reaction to Badge 373 was generally negative.  In the New York Times, Roger Greenspan pointed out the biases of the film: "All of the evil is perpetrated by Puerto Ricans, either innocent but violent revolutionaries who run around shouting Puerto Rico Libre! or the uninnocent but equally violent nonrevolutionaries who manipulate them. Against such forces, Eddie the hard-nosed cop has only the instincts of his personal bigotry to guide him. And invariably the instincts of his personal bigotry turn out to be right. ...  nless you care to hate Puerto Ricans (or Irish cops) I dont see how the movie can have anything for you".   

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 