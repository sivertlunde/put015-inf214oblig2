Tammy (film)
 
{{Infobox film
| name           = Tammy
| image          = Tammy poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Ben Falcone
| producer       = Melissa McCarthy Will Ferrell Adam McKay
| writer         = Ben Falcone Melissa McCarthy
| starring       = Melissa McCarthy Susan Sarandon Allison Janney Gary Cole Dan Aykroyd Kathy Bates Michael Andrews
| cinematography = Russ T. Alsobrook
| editing        = Michael L. Sale
| studio         = New Line Cinema Gary Sanchez Productions On the Day Productions
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 96 minutes  
| country        = United States
| language       = English
| budget         = $20 million 
| gross          = $100.4 million  
}}
Tammy is a 2014 American comedy film directed by Ben Falcone and produced, co-written by, and starring Melissa McCarthy as the title character. The film also stars Susan Sarandon, Allison Janney, Dan Aykroyd, Kathy Bates, and Falcone himself. The film was released on July 2, 2014.  

The film received negative reviews from critics, but was a box office success grossing over $100 million.

==Plot==
In Murphysboro, Illinois, Tammy strikes a deer on the way to her job at Topper Jacks (a fictional fast food restaurant), causing significant damage to her vehicle. Once she arrives at Topper Jacks, her supervisor, Keith, fires her for repeatedly showing up late for her shift. Upon leaving, Tammy indignantly causes a scene by contaminating or stealing food. After her car dies on the way home on Illinois Route 13, she eventually arrives to find her husband, Greg, eating a romantic meal with their neighbor, Missi. Upset, Tammy leaves and walks two doors down to her parents house.

She tells her mother, Deb, about her plans to leave and takes her grandmother Pearls car. Pearl requests to come along. Tammy initially refuses but ultimately agrees when Pearl proves that she has a large sum of cash. Tammy has beer with Pearl, and the next morning they wake up near a park where Pearl convinces Tammy not to go back home. Pearl wants to go to Niagara Falls with Tammy since she hadnt gone as a child. Along the way the two stop in a bar in Louisville, Kentucky, Tammy meets Earl and his son Bobby, and Earl hooks up with Pearl. Tammy and Bobby begin to make a love connection as Pearl and Earl drunkenly make out in the car. Bobby gives Tammy his number to call him so he can pick up Earl. Back at the hotel, Tammy is forced to sleep outside. The next morning, Bobby picks up Earl, and the two leave. Tammy, infuriated with Pearl, leaves her, but returns after feeling guilty. Tammy and Pearl are arrested after Pearl gets caught buying a case of beer for underaged teenagers as well as shoplifting a pint of whiskey for herself. Tammy is released, but Pearl stays at the jail for possessing illegal prescription drugs.

To bail out Pearl, Tammy robs a Topper Jacks, where she converses with employees Becky and Larry. Finally having obtained the money, she rushes to the prison to bail her out, but Bobby has already bailed out Pearl. With the help of Pearls wealthy lesbian cousin Lenore (who made her fortune off of a small chain of pet supply stores), they destroy the car to hide the evidence from the robbery. The two then stay at the home of Lenore and her wife, Susanne. At a 4th of July party thrown at the house, Pearl gets drunk and humiliates Tammy by making rude comments about her weight and appearance in front of all the guests. After Tammy runs off to the dock on the lake by the house, Lenore follows her to both comfort her and offer her some tough love, telling her that she has always been complaining about her life, but has never done anything about it. She tells Tammy that if she wants to make things better for herself she needs to work hard to make it happen.

Later in the morning, Tammy brings coffee to Pearl, who is presumably asleep outside. After repeatedly trying to wake her, Pearl does not wake up and Tammy assumes she is dead. She, Lenore, and Susanne grieve Pearls death, but Pearl suddenly awakens, much to everyones shock. Pearl was actually unconscious due to the large amount of alcohol she drank the previous night. Tammy is relieved, and she tearfully demands Pearl to get help for her drinking problem. The ambulance arrives and takes Pearl to the hospital. The police arrive as well, and Tammy is arrested.

Tammy is released from prison 38 days later, and her father, Don, picks her up. He offers to kill Greg for her, though she declines. Returning home, Tammy finds that Greg and Missi have packed Tammys belongings. She and Greg agree to an amicable divorce. She walks down the street to her parents place and finds out that Pearl is now living in Brookview Retirement Home. Tammy goes to Brookview to break her out, but Pearl is actually happy there. She has been attending Alcoholics Anonymous meetings at the home, and she is dating one of the men there. However, they do still take a trip to Niagara Falls.

At Niagara Falls, Bobby surprises Tammy there and they kiss. Tammy tells him about her choice to move to Louisville to get a fresh start in life and get closer to him.

==Cast==
 
* Melissa McCarthy as Tammy Banks, the title character.
* Susan Sarandon as Pearl Baldwin, Tammys grandmother. Pearl has diabetes, so Sarandon wore prosthetic ankles to reflect the symptoms.   
* Kathy Bates as Lenore, Pearls cousin
* Allison Janney as Deb, Tammys mother and Pearls daughter
* Dan Aykroyd as Don, Tammys father
* Mark Duplass as Bobby, Earls son
* Gary Cole as Earl, Pearls new love interest
* Nat Faxon as Greg Banks, Tammys husband
* Toni Collette as Missi Jenkins, Greg and Tammys neighbor, and Gregs love interest
* Sandra Oh as Susanne, Lenores wife
* Ben Falcone as Keith Morgan, Tammys boss
* Sarah Baker as Becky, Tammys friend
* Rich Williams as Larry
 

==Production==
Principal photography began May 3, 2013 in Wilmington, North Carolina. Other filming locations include the surrounding areas of Shallotte, North Carolina and Boiling Spring Lakes, North Carolina. Also brief footage was filmed in Louisville, Kentucky  and Niagara Falls, New York.  

==Release==

===Marketing===
The first official full-length trailer of the film was released on May 6, 2014.  On June 3, three posters for the film were released.  On June 16, the UK trailer for the film was revealed. 

The film was released on July 2, 2014.      

==Reception==

===Critical reception===
Tammy received generally negative reviews from critics. The film currently has a 23% rating on  , the film has a score of 39 out of 100, based on 36 reviews, indicating "generally unfavorable reviews". 

Ian Buckwalter of NPR gave the film a mixed review saying "Tammy never quite manages to find that balance between the sweet and the smartass the way Bridesmaids did, nor does the mismatched buddy dynamic between McCarthy and Sarandon ever approach the success of The Heat. But eventually the film does manage to find its own awkward way, with enough effective and less desperate jokes to smooth things over after the rocky start. Its a shakier debut of McCarthy and Falcones efforts behind the camera than one might have hoped for, but if Tammy can turn things around, surely they can too." 

===Box office===
The film grossed $6.2 million on its opening day, and $21.6 million in its opening weekend, finishing second place at the box office. Grossing $100,375,432 against a $20 million budget, Tammy has become a box office hit.   

===Accolades===
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient
! Result
|- 2014
! Palm Springs International Film Festival
| Directors to Watch
| Ben Falcone
|  
|- Teen Choice Awards
| Choice Summer Movie Star
| rowspan="3"| Melissa McCarthy
|  
|- 2015
! 41st Peoples Choice Awards|Peoples Choice Awards
| Favorite Comedic Movie Actress
|  
|- 35th Golden Razzie Awards
| Worst Actress
|  
|-
| Worst Supporting Actress
| Susan Sarandon
|  
|- GLAAD Media Awards
| Outstanding Film - Wide Release
| 
|  
|-
! Dorian Awards
| Campy Flick of the Year
| 
|  
|}

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 