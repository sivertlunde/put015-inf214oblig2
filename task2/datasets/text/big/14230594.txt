Jayamkondaan
{{Infobox film
| name =Jayam Kondaan
| image = Jayamkondaan.jpg Kannan
| starring = Vinay Rai, Bhavana Balachandran|Bhavana,  Lekha Washington,  Kishore (actor)|Kishore,  Saranya Mohan, Vasundhara  Vivek (actor)|Vivek, Santhanam (actor)|Santhanam, Cochin Hanifa Kannan
| producer = T. G. Thiyagarajan
| cinematography= Balasubramaniem
| editing = V. T. Vijayan
| distributor = Satya Jyothi Movies
| released =  
| runtime =
| country = India
| language = Tamil Vidyasagar
}} Kannan and Vivek and Kishore also appear.

The film revolves around an NRI Civil Engineer , who returns to Chennai from London to set up his own business.  He feels that if one has to reach his goal in life it is better to win over your enemies and take them along with you. He finds out that his late father had another family, and that his step sister wants to steal his home. After clashes with his sister, he discovers another tragedy involving someone who wants to make a vendetta with the family. The film follows the sibling relationship and the threat from the avenger.

The film opened to worldwide audiences after several delays on 29 August 2008 to generally positive reviews.      

==Plot==
Arjun (Vinay Rai) is an IT professional who lived in London for years, and has now come down to Chennai to start his own business. His circle of friends like Krishna (Krishna), Gopal (Vivek (actor)|Vivek) and their wives, make him happy. Arjuns philosophy is to be cool and never lose ones temper, under any circumstances. He believes that in order to reach ones goals in life that it is better to win over your enemies and take them along.

However Arjun is shocked when he learns that his late father had second family, and his half sister Brinda (Lekha Washington) is now trying to sell his family house in Madurai, a large lakeside villa with a mountain backdrop. She needs the money to study at MIT where she has received a scholarship. They lock horns over the property and end up in Madurai, where a red chilly dealer Durai Raj (Nizhalgal Ravi) is staying in their house with his daughter Annapoorni aka Molaka (Bhavana Balachandran|Bhavana). Arjun pretends that Poorni is his childhood sweetheart, wins her over, and then manages to get her to vacate the house.

Arjun gets entangled with a local troublemaker Guna (Kishore), which leads to an accident where his wife Poonkodi (Vasundhara) is killed. Now Guna is looking to avenge his wifes death as he follows Arjun to Chennai. The rest of the film depicts Arjun taking the responsibilities of a big brother, win over Brinda, and deal with Guna, who is determined to kill him.

==Cast==
 , in her debut film, as picturized in a promotional still.]] IT professional returning from London.
*Bhavana as Annapurani. The daughter of the chilli vendor Durai Raj, who lives in Arjuns ancestral house. Illegitimate daughter of Arjuns father, who wants to sell the ancestral house. Kishore as Guna. A rowdy in Madurai who begins to clash with Arjun .
*Vasundhara as Poonkodai. The village wife of Guna. Vivek as Gopal. The friend of Arjun. Santhanam as Bhavani. A chilli broker in Madurai.
*Annee Malavika Avinash as Chandrika. A French teacher, mother of Brinda.
*Nizhalgal Ravi as Durai Raj. A chilli vendor who lives in Arjuns family house. Also as the father of Annapurani.
*Saranya Mohan as Archana. Sister of Annapurani. Krishna
*Devan Ekambaram aarthi
*Kamala Krishnaswamy as Apartment President

==Reception== Bhavana has Santhanam were Vivek who at times you feel speaks more dialogues than necessary. Praise is also heaped on Athisaya as the small town girl who gets enamored by the rowdy, describing her as a "revelation", while Kishore "fits the bill as the bad guy". 

In unison, Rediff.com also praise the film as a "nice blend of the cinematic and logic". 
The reviewer describes Vinay Rai, "as the protagonist is very comfortable in his role" and thathe has "expressive eyes, emotes well, and makes sure his audience isnt disappointed. However Lekha Washington is clearly the surprise package", echoing Sify.coms views. However it claims that Bhavana, with her "soulful eyes and acting talent, could have done with a meatier role".  The film has risen above the clichés as per the review of The Hindu and the reviewer has praised Vinay for choosing this film as his second project".   

Critics claimed that Kannan deserves credit for a "good job" on his story and screenplay. 
Vidayasagar’s music was described as is "so-so", there is a feeling that you have heard these tunes before with the "picturisation is nowhere near the high standards set up by the director’s guru (Mani Ratnam).  Rediff claims that Balasubramaniams camera makes sure the "viewers arent treated to bizarre angles" and that V. T. Vijayans editing is "slick and smooth". 

==Soundtrack==
{{Infobox album
| Name       = Jayam Kondaan
| Type       = soundtrack Vidyasagar
| Cover      = 
| Released   = 2008
| Recorded   = 
| Genres     = World Music
| Length     = 
| Label      = sony music Vidyasagar
| Last album = Abhiyum Naanum (2008)
| This album = Jeyam Kondaan (2008)
| Next album = Mulla (film)|Mulla (2008)
}} Vidyasagar with Vaali and Pa. Vijay. The audio of the film released worldwide on 3 June 2008, two months prior to the release. The soundtrack was successfully received with praise.      

{| class="wikitable"
|-
! Song title !! Singers !! Length !! Description
|- Karthik || 5:25 ||   A montage/background number.
|- Bhavana
|-
| Naan Varaindhu Vaitha ||  .  
|-
| Ore Or Naal en ||  . Actress Suja performs an item number.
|-
| Sutrivarum Boomi ||   and Saranya Mohan in the localities of Madurai.
|- Tippu || 2:39 ||  A montage song which features the successes of Arjun and Bhavani.
|-
|}

==References==
 

==External links==
*  
*  

 
 
 
 