The Last Bandit
{{Infobox film
| name =  The Last Bandit
| image =
| image_size =
| caption =
| director = Joseph Kane 
| producer = Joseph Kane 
| writer = Thames Williamson       Luci Ward    Jack Natteford
| narrator = Bill Elliott   Lorna Gray   Forrest Tucker     Andy Devine
| music = R. Dale Butts  
| cinematography = Jack A. Marta  Arthur Roberts 
| studio = Republic Pictures
| distributor = Republic Pictures
| released = February 25, 1949
| runtime = 80 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Bill Elliott, The Great Train Robbery  with a larger budget and using the studios Trucolor process. The film was remade again in 1952 as South Pacific Trail.

==Synopsis== railroad detective, is implicated in an attempt by his former outlaw colleagues to rob a series of gold shipments. 

==Partial cast== Bill Elliott as Frank Norris / Frank Plummer 
* Lorna Gray as Kate Foley / Kate Sampson  
* Forrest Tucker as Jim Plummer 
* Andy Devine as Casey Brown   Jack Holt as Mort Pemberton 
* Minna Gombell as Winnie McPhail 
* Grant Withers as Ed Bagley  
* Virginia Brissac as Kates Mother  
* Louis Faust as Hank Morse  
* Stanley Andrews as Jeff Baldwin 
* Martin Garralaga as Patrick Moreno  
* Joseph Crehan as Local No. 44 Engineer   Charles Middleton as Blindfolded Circuit Rider

==References==
 

==Bibliography==
* Fetrow, Alan G. Feature Films, 1940-1949: a United States Filmography. McFarland, 1994. 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 