The Story of a Poor Young Man (1920 film)
{{Infobox film
| name = The Story of a Poor Young Man 
| image =
| caption =
| director = Amleto Palermi
| producer =
| writer = Octave Feuillet  (novel)   Amleto Palermi
| starring = Luigi Serventi   Pina Menichelli   Antonio Gandusio
| music = 
| cinematography = Antonio Cufaro 
| editing =
| studio = Rinascimento Film  UCI
| released = October 1920
| runtime = 86 minutes
| country = Italy Italian intertitles
| budget =
| gross =
}}
The Story of a Poor Young Man (Italian:Il romanzo di un giovane povero) is a 1920 Italian silent drama film directed by Amleto Palermi and starring Luigi Serventi, Pina Menichelli and Antonio Gandusio.  It was one of numerous film adaptations of Octave Feuillets novel of the same name.

==Cast==
*  Luigi Serventi as Massimo 
* Pina Menichelli as Margherita 
* Gemma De Sanctis     
* Antonio Gandusio  
* Giuseppe Piemontesi    
* Gustavo Salvini

== References ==
 

==Bibliography==
* Angela Dalle Vacche. Diva: Defiance and Passion in Early Italian Cinema. University of Texas Press, 2008.

== External links ==
*  

 
 
 
 
 
 
 
 

 

 