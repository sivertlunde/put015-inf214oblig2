Second Nature (2009 film)
{{Infobox film
| name           = Second Nature
| director       =  
| producer       =  
| starring       =  
| music          =  
| distributor    = Sector 9 Skateboards
| released       =  
| runtime        = 22 minutes
| country        = United States
| language       = English
| budget         = 
}}
Second Nature  is a short film about skateboarding created for Sector 9 Skateboards. 

==Plot summary==
In an exploration of the abstract and the extreme, Second Nature is an examination of the natural boundaries of the human body. Noah Sakamoto, Patrick Rizzo and J.M. Duran star as the test subjects as they wield skateboards and blue suits to race down the roads of the High Sierras in California.

==Awards and honors== Sonoma International Film Festival  and Best Adventure Sport Film at the 5 Point Film Festival. 

==References==
 

 
 
 
 


 
 