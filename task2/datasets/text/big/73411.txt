Cops (film)
{{Infobox film name            = Cops image           = Keaton_Cops_1922.jpg caption         = Theatrical poster for Cops (1922) director        = Edward F. Cline   Buster Keaton  producer        = Joseph M. Schenck writer          = Edward F. Cline   Buster Keaton  starring        = Buster Keaton   Virginia Fox   Joe Roberts   Edward F. Cline   Steve Murphy music           = cinematography  =  editing         = Elgin Lessley distributor  First National Pictures Inc. released        =   runtime         = 18 minutes language  English (original intertitles) country         = United Sttates budget          =
}}
 1922 comedy short silent film about a young man (Buster Keaton) who accidentally gets on the bad side of the entire Los Angeles Police Department during a parade, and is chased all over town.  It was written and directed by Edward F. Cline and Keaton.

== Background and plot ==
This very Kafka-esque film was filmed during the rape-and-murder trial of Keatons friend and mentor Fatty Arbuckle, a circumstance that may have influenced the shorts tone of hopeless ensnarement.   Even though the central characters intentions are good, he cannot win, no matter how inventively he tries. He gets into various scraps with police officers throughout the film. Eventually, he unwittingly throws a bomb into a police parade and ends up being chased by a horde of cops.

At the end of the film, Keatons character locks up the cops in the police station. However, the girl he is trying to woo disapproves of his behavior and gives him the cold shoulder. Therefore, he unlocks the police station and is immediately pulled in by the cops. The film ends with the title "The End" written on a tombstone with Keatons pork pie hat propped on it. 

One of Keatons most iconic and brilliantly-constructed short films, Cops was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in their National Film Registry in 1997.

==Cast==
* Buster Keaton - The Young Man
* Joe Roberts - Police Chief
* Virginia Fox - Mayors Daughter
* Edward F. Cline - Hobo
* Steve Murphy - Conman selling furniture (uncredited)

==See also==
* Buster Keaton filmography
* List of United States comedy films
* 1922 in film

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 