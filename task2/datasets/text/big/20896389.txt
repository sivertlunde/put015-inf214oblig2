Six Weeks
 
{{Infobox film
| name = Six Weeks
| image = sixweeksmovie.jpg
| caption = theatrical poster
| director = Tony Bill
| producer = Peter Guber Jon Peters
| screenplay = David Seltzer
| based on = Six Weeks by Fred Mustard Stewart
| starring = {{plainlist|
* Dudley Moore
* Mary Tyler Moore
* Katherine Healy
}}
| music = Dudley Moore
| cinematography =
| editing =
| distributor = Universal Pictures
| released =  
| runtime =
| country = United States
| language = English
| budget = $11,000,000
| gross = $6,700,000
}}

Six Weeks is a 1982 drama film, directed by Tony Bill and based on a novel by Fred Mustard Stewart. It stars Dudley Moore and Mary Tyler Moore.

Co-star Katherine Healy was a professional figure skater and a ballerina, both talents demonstrated by her character in the film. Golden Globe nominated actress and ballet dancer Anne Ditchburn choreographed her dance scenes, as well as appeared as an assistant choreographer on camera.

==Plot==

Charlotte Dreyfus, a wealthy cosmetic tycoon and her 12-year-old daughter Nicole, whos dying from leukemia, strike up a sentimental friendship with a California politician, Patrick Dalton. Nicole has decided to abandon all further treatments for the disease because of the treatments side effects.
 Tchaikovsky ballet, she suddenly collapses and dies in her mothers arms, having achieved her lifelong dream.

In the final scene, Charlotte gets on a plane to Paris and is seen alone. Patrick writes to her imploring her to keep in touch. 
There is no reply.

==Reception==

The film was nominated for two Golden Globe Awards, one for Dudley Moore for Best Actor in a Dramatic Motion Picture and one for Katherine Healy as Best New Female Star of the Year. However, Mary Tyler Moores performance earned a Razzie Award nomination for Worst Actress. Roger Ebert later named it one of the worst films of 1982.  Gene Siskel however, liked the film, praising the performances from the leads and its go-for-broke sentiment.

==Cast==
* Dudley Moore as Patrick Dalton
* Mary Tyler Moore as Charlotte Dreyfus
* Katherine Healy as Nicole
* Shannon Wilcox as Peg Dalton

==References==
 
* .
* .
* .

==External links==
* 

 

 
 
 
 
 
 
 
 