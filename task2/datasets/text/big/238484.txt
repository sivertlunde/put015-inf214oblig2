Tillie's Punctured Romance (1914 film)
 
{{Infobox film
| name           = Tillies Punctured Romance
| image          = Tille.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Re-release poster with different billing
| director       = Mack Sennett
| producer       = Mack Sennett
| writer         = Hampton Del Ruth Craig Hutchinson Mack Sennett
| based on       =   Charles Bennett The Keystone Cops Charley Chase (uncredited) 
| music          =  Frank D. Williams (uncredited)
| editing        = 
| studio         = Keystone Film Company
| distributor    = Mutual Film
| released       =  
| runtime        = 74 mins. 82 mins. (2003 restoration)
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          =
}} silent comedy Keystone Film Company and the only such film to feature Chaplin.
 Tramp character in this movie.

==Plot==
 
Chaplin portrays a womanizing city man who meets Tillie (Dressler) in the country after a fight with his girlfriend (Normand). When he sees that Tillies father has a very large bankroll for his workers, he persuades her to elope with him. In the city, he meets the woman he was seeing already, and tries to work around the complication to steal Tillies money. He gets Tillie drunk in a restaurant and asks her to let him hold the pocketbook. Since she is drunk, she agrees, and he escapes with his old girlfriend and the money.

Later that day, they see a picture show entitled "A Thiefs Fate", which illustrates their thievery in the form of a morality play. They both feel guilty and leave the theater. While sitting on a park bench, a paperboy asks him to buy a newspaper. He does so, and reads the story about Tillies Uncle Banks, a millionaire who died while on a mountain-climbing expedition. Tillie is named sole heir and inherits three million dollars. The man leaves his girlfriend on the park bench and runs to the restaurant, where Tillie is now forced to work to support herself, as she is too embarrassed to go home. He begs her to take him back and marries her. Although she is skeptical at first, she believes that he truly loves her. They move into the uncles mansion and throw a big party, which ends horribly when Tillie finds her husband with his old girlfriend, smuggled into the house and working as one of their maids.

The uncle is found on a mountaintop, alive after all. He goes back to his mansion, in disarray after Tillie instigated a gunfight (a direct result of the husband smuggling the old girlfriend into the house) which, luckily, did not harm anyone. Uncle Banks insists that Tillie be arrested for the damage she has caused to his house. The three run from the cops all the way to a dock, where a car "bumps" Tillie into the water. She flails about, hoping to be rescued. She is eventually pulled to safety, and both Tillie and the mans girlfriend realize that they are too good for him. He leaves, and the two girls become friends.

==Cast==
 
 
 
 
 
* Marie Dressler ... Tillie Banks, Country Girl
* Mabel Normand .. Mabel, Charlies Girl Friend
* Charles Chaplin ... Charlie, City Slicker
* Mack Swain ... John Banks, Tillies Father Charles Bennett ... Uncle Banks, Tillies millionaire uncle
* Chester Conklin ... Mr. Whoozis
* Phyllis Allen ... Prison Matron/Restaurant patron (uncredited)
* Billie Bennett ... Maid/Party Guest (uncredited)
* Joe Bordeaux ... Policeman (uncredited)
* Glen Cavender ... First Pianist in Restaurant (uncredited)
* Charley Chase ... Detective in Movie Theater (uncredited)
* Dixie Chene ... Guest (uncredited)
* Nick Cogley ... Keystone Cop Desk Sergeant (uncredited)
* Alice Davenport ... Guest (uncredited)
* Minta Durfee ... Crooks Girlfriend in "A Thiefs Fate" (uncredited)
* Ted Edwards ... Waiter (uncredited) Billy Gilbert ... Policeman (uncredited)
* Gordon Griffith ... Newsboy (uncredited)
* William Hauber ... Servant/Cop (uncredited)
* Fred Hibbard ... Servant (uncredited)
* Alice Howell ...Guest (uncredited)
* Edgar Kennedy ... Restaurant Owner/Butler (uncredited)
* Grover Ligon ... Keystone Cop (uncredited)
* Wallace MacDonald ... Keystone Cop (uncredited)
* Hank Mann ... Keystone Cop (uncredited)
* Gene Marsh ... Maid/Waitress (uncredited)
* Harry McCoy ... Second Pianist in Restaurant/Pianist in Theater/Servant
* Gordon Griffith ...  Kid (uncredited) Charles Murray ... Detective in A Thiefs Fate (uncredited)
* Frank Opperman ... Rev. D. Simpson (uncredited)
* Fritz Schade ... Waiter/Diner (uncredited)
* Al St. John ... Keystone Cop (uncredited)
* Slim Summerville ... Keystone Cop (uncredited)
* A. Edward Sutherland ... Keystone Cop (uncredited)
* Morgan Wallace ... Thief in the Movie within the Movie (uncredited)

==Production==
The film was based on the Broadway play Tillies Nightmare,  which Dressler had great success in, on Broadway, and on tour in the United States, from 1910 to 1912.  Dressler would revive the play with her own touring company.
 Little Tramp". Although it is usually assumed that his performance in this film predated his crafting of the Tramp persona, Chaplin had already appeared in more than 30 shorts as the Tramp by the time Tillies Punctured Romance was released.

The comedy in the film is largely slapstick: people frequently kick each other or trip each other; four men unsuccessfully attempt to help Tillie up when she falls; Tillie, taken to the police station, has a police officer wave his finger in her face, and she bites it.

Milton Berle always claimed that he played the five-year-old paperboy in the film,  but the role was actually portrayed by Gordon Griffith.

This is one of only two films (the other is Making a Living, another pre-Tramp film) in which both Chaplin and the Keystone Cops appear.

==Reception==
The film was released on December 21, 1914.  The movie was popular enough to continue to be exhibited into 1915. 

==Sequels==
Dressler appeared as Tillie in three more movies, Tillies Tomato Surprise (1915), Tillie Wakes Up (1917) and The Scrub Lady (1917) aka Tillie the Scrub Lady. In Tillie Wakes Up the Tillie character has a different last name, which could be explained by the fact that her character is married.

===The 1928 film===
Another comedy called Tillies Punctured Romance (1928 film)|Tillies Punctured Romance was released in 1928 starring W. C. Fields as a circus ringmaster. Although often erroneously cited as a remake, the later movie actually bears no resemblance to the 1914 film aside from the shared title. Chester Conklin and Mack Swain appear in both movies, however.

==International versions==
*  Spain, 1916
*  Denmark, 1917
*  Sweden, 1920
*  Finland, 1924

==See also==
* List of United States comedy films
* Charlie Chaplin filmography

==References==
;Citations
 

;Bibliography
*  }}
*  
*  |accessdate=2015-03-17}}

==External links==
 
*  
*  
*  
*   on YouTube

 
 
 
 
 
 
 
 
 
 
 
 
 