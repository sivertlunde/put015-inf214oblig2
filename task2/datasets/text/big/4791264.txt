Zazie dans le Métro
 
{{Infobox film
| name           = Zazie dans le Métro
| image          = Zazie-dans-le-metro-poster.jpg
| alt            = Film poster
| caption        = Film poster
| director       = Louis Malle
| producer       = Louis Malle
| writer         = Louis Malle Jean-Paul Rappeneau
| based on       =  
| starring       = Catherine Demongeot Philippe Noiret
| music          = Fiorenzo Carpi André Pontin
| cinematography = Henri Raichi
| editing        = Kenout Peltier
| studio         = 
| distributor    = Astor Pictures  (US) 
| released       =  
| runtime        = 89 minutes
| country        = France Italy
| language       = French
| budget         = 
| gross          = 
}}
 novel by Raymond Queneau.

==Plot== female impersonator) for two days, while her mother spends some time with her lover. Zazie manages to evade her uncles custody, however, and, Paris Métro|métro strike notwithstanding, sets out to explore the city on her own.

In the film, Zazie is younger than in the book (in which her mother is worried about Zazies virginity). 

==Cast==
* Catherine Demongeot as Zazie 
* Philippe Noiret as Uncle Gabriel 
* Hubert Deschamps as Turandot 
* Carla Marlier as Albertine 
* Annie Fratellini as Mado 
* Vittorio Caprioli as Trouscaillon 
* Jacques Dufilho as Ferdinand Grédoux 
* Yvonne Clech as Madame Mouaque 
* Odette Piquet as Zazies mother 
* Nicolas Bataille as Fédor 
* Antoine Roblot as Charles 
* Marc Doelnitz as M. Coquetti

==Reception==
Decades after its release, Jonathan Rosenbaum wrote that it was "arguably Louis Malle’s best work...A rather sharp, albeit soulless, film, packed with ideas and glitter and certainly worth a look." 

==Home video==
A digitally restored version of the film was released on DVD and Blu-ray by The Criterion Collection in June 2011. 

==References==
 

==External links==
*  
*  
*  
*  by Ginette Vincendeau
* 

 
 

 
 
 
 
 
 
 
 


 
 