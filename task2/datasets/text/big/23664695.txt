Pierre and Djemila
{{Infobox film
| name           = Pierre and Djemila
| image          = Pierre and Djemila.jpg
| caption        = Film poster
| director       = Gérard Blain
| producer       = Philippe Diaz
| writer         = Gérard Blain Mohamed Bouchibi Michel Marmin
| narrator       = 
| starring       = Jean-Pierre André Nadja Reski
| music          = 
| cinematography = Emmanuel Machuel
| editing        = Catherine-Alice Deiller
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = France
| language       = French
| budget         = 
}}

Pierre and Djemila ( ) is a 1987 French drama film directed by Gérard Blain. It was entered into the 1987 Cannes Film Festival.   

==Cast== Abdelkader - Djaffar
* Djedjigua Ait-Hamouda - Aicha
* Jean-Pierre André - Pierre Landry
* Jacques Brunet - Pere De Pierre
* Fatia Cheeba - Houria
* Fatiha Cheriguene - Mere de Djemila
* Francine Debaisieux - Mere De Pierre
* Séverine Debaisieux - Carole
* Abdelkader Djerouni - Imam
* Lakhdar Kasri - Lakhdar
* Svetlana Novak - Professeur De Mathematiques
* Nadja Reski - Djemila Khodja
* Salah Teskouk - Pere de Djemila

==References==
 

==External links==
* 

 
 
 
 
 
 