Sx Tape
 
{{Infobox film
| name           = Sx_Tape
| image          = File:Sx tape film poster 2013.jpg
| alt            = 
| caption        = Theatrical release poster Bernard Rose
| producer       = {{Plain list |
*Sebastian Aloi
*Ricardo DAmato
*Eric Reese
*Steven Schneider
*Chuck Simon
}}
| writer         = Eric Reese
| starring       = {{Plain list |
*Caitlyn Folley Ian Duncan
*Chris Coy Diana Garcia
}}
| music          = Nigel Holland
| cinematography = Bernard Rose
| editing        = Bernard Rose
| studio         = {{Plain list |
*Aeroplano
*La.Lune Entertainment
}}
| distributor    = {{Plain list |
*Well Go USA Entertainment  
*S Pictures  
}}
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} found footage Bernard Rose. Ian Duncan as a young couple that decides to film a sex tape in an abandoned hospital, only to discover that it is not as abandoned as they believed it to be. 

==Synopsis== Ian Duncan) is dead and that two others are missing. The movie then cuts to Adam shooting film of Jill with the intention of turning it into a movie about her and her paintings. He jokingly asks to film a sex tape and despite Jill initially declining, he then films the two of them trying to have sex in various locations. During a drive home Adam points out The Vergerus Institute for Troubled Women, an abandoned hospital rumored to have given various women abortions. He idly mentions that hed like to hold a party there and have Jill display her paintings, which prompts Jill to push the two into exploring the hospital. The two are nearly caught by a passing policeman, which prompts them to stay in the hospital and explore until theyre sure hes left. During this time Adam talks Jill into letting him tie her down to an operating table, after which he pretends to leave her. While hes gone a female ghost attacks and possesses Jill. When Adam returns Jill shows no outward sign of her state and agrees to a sexual encounter with Adam, during which she suffers a nose bleed. 

They decide to leave but find that their car has been towed. Jill manages to persuade her friend Ellie (Diana Garcia) to pick them up, but Ellies boyfriend Bobby (Chris Coy) talks Jill into going back inside. This elicits jealousy from Adam, who believes that Jill is only doing this because shes attracted to Bobby. Once inside Adam keeps trying to get everyone to leave and nearly comes to blows with Bobby, who keeps flirting with Jill. Bobby threatens Adam with a gun and wanders off with Jill. Ellie persuades Adam to wait behind while she goes to find the others, knowing that Adam would likely spark a lethal fight. After waiting a while, Adam goes looking for them and eventually finds a hysterical Jill standing near blood spatters and gun shells. Jill vaguely tells Adam that she had a fight with Ellie after she caught Bobby kissing her. They continue to wander around the hospital looking for the exit and the pair finds the hospitals medical record archive, where Jill is engrossed by the records of Nicolette, a lobotomized patient that had exhibited aggression and suicidal tendencies. 

Adam pushes Jill to leave, which provokes her to anger. As they continue trying to find the exit they come across the ghost of Nicolette, who systematically terrorizes them. Jill tries to call the police, only for the dispatcher to angrily tell her to go to the office and get away from Adam. Jill takes off and Adam follows her until they get to the office, where Jill gets trapped inside. He breaks down the door and finds Jill holding a videotape. Adam tries to take it from her, at which point Jill runs away. He tries to find her by looking at the hospitals still working security system, which shows Jill having a threesome with Ellie and Bobby before shooting them both to death. Adam then physically locates Jill in a bathroom where shes acting erratic and demands to perform oral sex on him before shooting him to death. The movie then shows footage of Nicolette getting anesthetized and it is implied that she is raped before getting lobotomized. The film ends with a man filming Jill giving him oral sex, which ends with her biting his penis off.

==Cast==
*Caitlyn Folley as Jill Ian Duncan as Adam
*Chris Coy as Bobby Diana Garcia as Ellie
*Julie Marcus as Colette McLeod Daniel Faraldo as Doctor
*Eric Neil Gutierrez as Detective

==Reception==
Critical reception for Sx Tape has been predominantly negative.    Fangoria and SciFiNow both expressed disappointment that the movie did not build more upon its premise,  as SciFiNow commented that "the idea of a horror film attacking the exploitative nature of sex tapes and beyond has potential. However, there’s no clear indication that this premise was given any real thought beyond comparing this exploitation to the institutional abuse suffered by women in mental hospitals, a comparison that is made with all the subtlety of a head-butt."  Twitch Film wrote an extremely negative review for Sx Tape, stating that "SX_Tape is a masterclass in how not to structure your film, found footage or otherwise."  The AV Club and The Hollywood Reporter were slightly more positive in their reviews but overall panned the movie,  as The Hollywood Reporter felt that while the movie was at times "gripping and claustrophobic", Sx Tape "does not have the gory excess and high body count to impress hardcore horror fans, but it also lacks the sly wit, deeper subtext and formal originality which might have earned it broader mainstream appeal." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 