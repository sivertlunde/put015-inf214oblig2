Safari (1940 film)
  American adventure film directed by Edward H. Griffith and starring Douglas Fairbanks Jr., Madeleine Carroll and Tullio Carminati.  An ambitious young woman goes on a safari hunt with a millionaire in the hope of convincing him to marry her, but falls in love with the chief hunter instead.

==Cast==
* Douglas Fairbanks Jr. - Jim Logan 
* Madeleine Carroll - Linda Stewart 
* Tullio Carminati - Baron de Courtland 
* Muriel Angelus - Fay Thorne 
* Lynne Overman - Jock McPhail 
* Frederick Vogeding - Captain on Yacht 
* Clinton Rosemond - Mike 
* Thomas Louden - Doctor Phillips 
* Fred Godoy - Steward  Jack Carr - Wemba
* Billy Gilbert - Mondehare 
* Hans von Morhart - Head Quartermaster 
* Darby Jones - Admiral Henry Rowland - Steersman 


==References==
 

==External links==
* 

 
 
 
 
 
 


 