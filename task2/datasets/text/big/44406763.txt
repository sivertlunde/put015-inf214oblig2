Sherni

{{Infobox film
| name           = Sherni
| image          = Sherni.jpg
| image_size     =
| caption        = DVD Cover
| director       = Harmesh Malhotra 
| producer       = Pawan Kumar Pran Shatrughan Sinha
| music          = Kalyanji Anandji
| cinematography = Srinivas Mahapatra	
| released       = 13 May 1988
| country        = India
| language       = Hindi 
}}
 Hindi film produced by Pawan Kumar and directed by Harmesh Malhotra. The film stars Sridevi and Shatrughan Sinha in lead roles along with Pran (actor)|Pran, Ranjeet, Master Rinku, Tej Sapru & Lalita Pawar. The music is composed by Kalyanji Anandji.

== Cast ==

*Sridevi as Durga Pran as Durgas dad
*Ram Mohan as Durgas gang-member
*Shatrughan Sinha as Inspector Rajan
*Sudhir as Inspector S.K. Sinha
*Master Rinku as Durgas younger brother
*Ranjeet as Vinodpal Singh
*Tej Sapru as Tejaa
*Lalita Pawar 
*Kader Khan as Thakur Dharampal Singh
*Jagdeep as Hajaam
*Birbal as Havaldar Baburam

== Soundtrack ==

{{Track listing
| headline        = Songs
| extra_column    = Playback

| title1          = Ek Rupaiya Doge
| extra1          = Anuradha Paudwal

| title2          = Gadi Gadi Chunar Sarkane Lage
| extra2          = Alka Yagnik, Sadhana Sargam  

| title3          =  Koi Mard Mila Na Aisa
| extra3          = Various  

| title4          = Mushkil Hai Mushkil
| extra4          = Various

| title5          = Tere Aane Se
| extra5          = Sadhana Sargam  
}}
                                                                                   
==External links==
* 

 


 