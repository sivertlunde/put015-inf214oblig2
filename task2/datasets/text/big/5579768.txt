Bad Company (1972 film)
{{Infobox film
| name           = Bad Company
| image          = Bad company poster.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Robert Benton
| producer       = Stanley R. Jaffe David Newman
| narrator       = Barry Brown Jeff Bridges
| music          = Harvey Schmidt
| cinematography = Gordon Willis
| editing        = Ron Kalish Ralph Rosenblum
| distributor    = Paramount Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States English
| budget         =
}} David Newman. Barry Brown the draft during the American Civil War to seek their fortune and freedom on the unforgiving American frontier. 

This acid western attempts in many ways to demythologize the American West in its portrayal of young men forced by circumstance and drawn by romanticized accounts to forge new lives for themselves on the wrong side of the law.  Their initial eagerness to be outlaws soon abates, however, when the boys are confronted with the realities of preying on others in a nation ravaged by war and exploitation.

==Plot== Barry Brown) despite his mothers protest. She explains that she has already lost one son to the war. When the soldiers leave, Drew emerges from his hiding place. His parents give him $100 and urge him to go West, giving him their picture and his brothers watch as mementos.

In St. Joseph, Missouri, Drew is approached by Jake Rumsey (Jeff Bridges) who pistol-whips him and takes his money in an alley. Jake runs a gang of petty thieves who steal purses and rob children of their pocket change. While Drew is recovering at a ministers house, Jake arrives to return the purse that one of his gang stole from the ministers wife, hoping to collect a reward. Once inside, he purloins various household items until Drew sees him and attacks, demanding his money back. After a long struggle, Jake finally bests Drew and convinces him that he has no choice but to join his gang, as the Army will catch him if he tries to board a wagon train as is his plan. 
 John Savage), Arthur Simms (Jerry Houser), and the ten-year-old Boog Bookin (Joshua Hill Lewis). Loney demands that Drew demonstrate his worth by committing a robbery and bringing in some money. Drew agrees and claims to have robbed a hardware store, when in fact he simply took $12 from his boot where he is hiding his parents money.

The gang heads West, hoping to improve their fortunes. At night, Drew reads to everyone from Jane Eyre. When they spy a rabbit, all six of them shoot at it, barely managing to kill it. Jake orders Boog to clean the rabbit, but Boog declines. Jake is stunned to realize that no one in the gang knows how to clean the rabbit. He demonstrates how to do it, but his barely contained disgust reveals that he is skinning his first rabbit. The next day, a settler and his wife are returning from the West, where they went bust. The settler offers his wife to all six boys for $10. Drew declines, citing his morals. 
 Geoffrey Lewis), come upon the boys while they are still asleep. During the robbery, Jake aims his gun at Big Joe, but doesnt have the nerve to fire. Flat broke, the gang tries to mount a string of unsuccessful robberies, which eventually grow tragic as Boog is shot while he runs with a pie from a window sill. 

The gang finally disintegrates for good when the Logan brothers rob Jake and Drew, taking his brothers watch and the horses. Left with only a mule, Jake and Drew wander aimlessly. Eventually, they come across the Logan brothers corpses hanging from a tree. Big Joes gang has killed them, and as Jake and Drew bury their bodies, Hobbs leads the thugs to attack them, despite Big Joes warning that they would bungle the job. Sure enough, Jake and Drew manage to kill all four thugs, and as Drew leans over Hobbs to retrieve his watch, he reveals a hole in his boot. Jake sees a $10 bill through the hole and realizes that Drew had lied about robbing the hardware store. He pistol-whips Drew again and takes the money.

When Drew awakens, he wanders alone, swearing that hell kill Jake if he ever sees him again. Seeing smoke on the horizon, he investigates, only to find that it was the result of a burning barn, set afire during a raid by Big Joe. Before he is hanged for taking part in the raid, one of Joes men confirms to Drew that Jake has joined up with the gang. Drew joins the posse in order to get his revenge on Jake. 

The posse captures Big Joes gang, and as Drew guards Jake, he realizes how guilty he is by lying about his money. Jake offers to split $1,000 that the gang has buried. Drew helps him escape during the night, but after a few days of riding, he realizes that there is no buried money. Jake assumes that Drew will kill him, but Drew swears instead to stick with Jake until he has repaid every cent that he owes Drew. 

In the final scene, as they approach a town, Jake asks Drew, "So howd that Jane Eyre turn out in the end?" Drew replies, "Fine. Just fine." The two boys walk into the Wells Fargo and rob it.

==Cast==
* Jeff Bridges as Jake Rumsey Barry Brown as Drew Dixon Jim Davis as Marshal
* David Huddleston as Big Joe John Savage as Loney
* Jerry Houser as Arthur Simms
* Damon Cofer as Jim Bob Logan
* Joshua Hill Lewis as Boog Bookin Geoffrey Lewis as Hobbs
* Raymond Guth as Jackson
* Ed Lauter as Orin
* John Quade as Nolan
* Jean Allison as Mrs. Dixon
* Ned Wertimer as Mr. Dixon
* Charles Tyner as Egg Farmer
* Ted Gehring as Zeb
* Claudia Bryar as Mrs. Clum
* John Boyd as Prisoner

==Critical reception==
Film critic Roger Ebert liked the film and wrote, "The movie is built as a series of more-or-less self-contained episodes, and the episodes that work are worth the effort. But we get the feeling the movie doesnt know where its headed and the last scene (one of those freeze-frames thats supposed to crystallize a significant moment for us) left me suspended in midair. If there were ever a movie that just plain stopped, instead of arriving at a conclusion, this is it. Still, there were some good moments along the trail. 
 Bonnie and The Late Show/Nadine (1987 film)|Nadine), who again teams with the writer Newman. I thought this was the best film Benton ever directed, including his Oscar winning Kramer vs. Kramer, and he disappointed me by never coming close again to being as subversive and hard-hitting as he was in this film. The ironic comical Western prides itself in taking on the Horatio Alger myth by debunking the belief that going West would lead adventurers to the land of plenty, as it bitterly points out the harsh realities of life and that taking to the road will more than likely mean meeting with bad company and hardship rather than good company and fortune." 

The film was shot on location in Kansas USA.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 