Way of the Dragon
 
 
 
{{Infobox film
| name           = Way of the Dragon
| image          = Way-of-the-dragon-poster.jpg
| caption        = Hong Kong film poster
| film name      = {{Film name| traditional = 猛龍過江
| simplified = 猛龙过江
| pinyin         = Měng Lóng Guò Jiāng
| jyutping       = Maang5 Lung5 Gwo3 Gong1}}
| director       = Bruce Lee
| producer       = {{plainlist|
* Raymond Chow
* Bruce Lee
}}
| writer = Bruce Lee
| starring       = {{plainlist|
* Bruce Lee
* Nora Miao
* Chuck Norris
* Hwang In-Shik
* Robert Wall Paul Wei
* Wang Chung-hsin
}}
| music          = Joseph Koo
| cinematography = Tadashi Nishimoto
| editing        = Peter Cheung
| studio         = {{plainlist|
* Concord Production Inc.
* Golden Harvest Company
}}
| distributor    = {{plainlist| Golden Harvest
* Bryanston Pictures
* Fortune Star Media (Worldwide)
}}
| released       =  
| runtime        = 99 minutes
| country        = Hong Kong
| language       = Mandarin Cantonese English
| budget         = HK$130,000  
| gross          = US$85 million 
}} martial arts action comedy film written, produced and directed by Bruce Lee, who also stars in the lead role. This was Bruce Lees only completed directorial effort. The film co-stars Nora Miao, Chuck Norris, Robert Wall and Hwang In-Shik. The film was released in Hong Kong on 30 December 1972.

==Plot==
In Rome, Chen Ching-hua and her uncle Wang experience trouble with their restaurant from a mob boss who wants their property.  When Chen refuses to sell, the mob boss sends gangsters to scare away customers.  Appealing to an uncle in Hong Kong, Chen receives help in the form of Tang Lung, a martial artist.  Disappointed, Chen asks how he can help her, but Tang confidently assures her that he is capable.  At the restaurant, Tang learns that the staff have begun to learn karate, much to the annoyance of Quen, an employee who favors Chinese martial arts.  Tang tells Quen that he should be open-minded and incorporate any moves that work.  Before Tang can demonstrate his style to them, customers arrive, and the staff change clothes.

Before long, gangsters appear at the restaurant and chase off the customers while Tang is in the bathroom.  Angered by Tangs poor timing, the staff question his skill and the usefulness of his style.  Later, the gangsters return to harass more customers.  Before the staff can engage the gangsters, Wang asks all involved to take their fighting outside.  The staff engage the thugs, only to be beaten.  However, Tang single-handedly defeats the thugs, and the staff abandon their training to study under him.  Wang warns them that the gangsters will now seek revenge and that this victory could make the situation worse.  Tang vows to protect the restaurant.  Chen and Tang grow closer, and she takes him on a tour of Rome, though Tang is unimpressed.

Ho, the mafia bosss consigliere, returns with armed thugs and takes the restaurant staff hostage while they wait for Chen and Tang to return.  Ho gives Tang a ticket back to Hong Kong; when his men escort Tang outside, Tang quickly disarms and overpowers them.  When reinforcements from the restaurant arrive to subdue him, he defeats them using a pair of nunchaku.  Tang warns Ho not to return, and the  thugs leave the restaurant.  The staff celebrate their victory, though Wang again urges them to focus more on business than fighting.  Meanwhile, the mafia boss threatens to send an assassin to kill Tang unless he leaves by Chinese New Year, and Wang urges Chen to convince Tang to leave.

When Tang refuses to abandon the restaurant, the assassin targets him from a nearby rooftop.  On edge from nearby fireworks, Chen and Tang survive the attempt, and Tang races off to confront the assassin, whom he tricks into wasting his ammunition.  When he returns to the apartment, he finds Chen gone.  Assuming that Ho has kidnapped her, Tang arrives at the mafia boss headquarters and leads the restaurant staff in a battle, which they win.  Tang issues a final warning to the mafia boss, which Ho translates.  The staff again celebrate, but a telegram for Tang cuts this short when they learn that he has been summoned back to Hong Kong.  Tang assures them that he will not leave without seeing the situation resolved.

Ho hires two martial artists to challenge Tang, Japanese and American karate practitioners who initially refuse to work together.  When the mafia boss indicates that money is no issue, Ho also recruits the Americans sensei, a world-class martial artist named Colt.  Ho leads the restaurant staff to an isolated spot under the pretense of a truce, where the two hired martial artists attack them, and Ho lures Tang away to fight Colt at the Colosseum.  After they defeat the martial artists, Wang betrays and kills two of the staff, as he wants to sell the restaurant to the mafia boss and return to Hong Kong.

In a brutal fight, Tang injures and then disables Colt.  When Colt refuses an offer of mercy, Tang kills him.  The mob boss arrives at the original location as Tang and Ho return, and the boss kills both Ho and Wang.  The police arrive and arrest him as he attempts to also kill Tang.  As Tang leaves for Hong Kong, Quen tells Chen that Tang is a loner who will never settle down.

==Cast==
* Bruce Lee as Tang Lung
* Nora Miao as Chen Ching-hua
* Chuck Norris as Colt 
* Robert Wall as Fred (Bob)
* Hwang In-Shik as Japanese martial artist Paul Wei as Ho
* Wang Chung-hsin as Uncle Wang
* Tony Liu as Tony
* Unicorn Chan as Jimmy
* Tommy Chen as Tommy
* Chin Ti as Quen
* John T. Benn   as mafia boss
* John Derbyshire appears in the film briefly as one of the mafia thugs. 

==Production== Golden Harvest founder, Raymond Chow.   

==Reception==
The film set a new box office record in Hong Kong.  It ranked #95 in Empire (magazine)|Empire magazines list "The 100 Best Films of World Cinema" in 2010. 

==References==
 

==External links==
*  
*  
*   at the Hong Kong Movie DataBase
*  
*  
*   – slideshow by Life (magazine)|Life magazine

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 