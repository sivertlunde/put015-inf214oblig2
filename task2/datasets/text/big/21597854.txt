Kami (2008 film)
 
 
{{Infobox film
| name = Kami
| image = KAMI the Movie poster Khai.jpg
| caption = Theatrical release poster
| director = Effendee Mazlan  Fariza Azlina Isahak
| writer = Fariza Azlina Isahak
| starring = Liyana Jasmay Syarul Ezani Mohamed Ezzuddeen Nas Muammar Zar Ani Zayanah Ibrahim Juliana Sophie Evans Zahiril Adzim
| producer = Lina Tan
| editing  =
| distributor = Grand Brilliance
| budget = RM 1.4 million
| gross = RM 1,245,000
| released =  
| runtime = 100 minutes
| country = Malaysia
| language = Malay
}}

Kami is a 2008 Malaysian drama film directed by Effendee Mazlan and Fariza Azlina Isahak. It was first released on 1 October 2008. The story revolves around the ups and downs of the life of five teenagers and how they cope with their surroundings.

==Plot==
Based loosely from the TV series of the same name, the film tells about a group of teenagers and their miseries in life and how they cope with the dramatic experiences they are facing as they undergo their everyday life.

==Cast==
* Liyana Jasmay as Lynn
* Syarul Ezani Mohamed Ezzuddeen as Ali
* Juliana Evans as Sofie
* Nas Muammar Zar as Abu
* Ani Zayanah Ibrahim as Adii
* Zahiril Adzim as Boy
* Rozie Rashid as Lynns mother Mustapha Kamal as Alis father
* Norish Karman as Alis mother
* Susan Manen as Sofies mother
* Fathil Dani as Abus father
* Dian P. Ramlee as Abus mother

==Production==
According to Lee Mee Fung, the Executive Director of Red Films Sdn Bhd, the film which is targeted at teenagers is about "a commercial film which revolves around teenagers conflicts and their reactions in determining their future."

The catch phrase of this film is "Hidup ini memang palat, tapi esok masih ada." The word "palat" has been featured in the promotion materials that are included in KAMI: Vol II soundtrack, as well as in the official website. However, the word has been eliminated from the further promotion materials as well as from the dialogues in the film since it was considered as a vulgar word.
 Hotlink and Monkey Bone Records.

==Release==
Kami was released to 43 cinemas across Malaysia on the first day of Hari Raya, and had a tight competition with Senario The Movie: Episode 1 which was released at the same day. Although it was predicted that it would not be able to compete with Senario The Movie,    it went to raise RM1.4 million in the first two weeks of screening and it managed to avoid the possibility of massive loss. 

==Critical reception==
Initial critical reception to Kami was generally positive reviews.

Izuan Shah from Gua.com said that "the premise of this film is very simple and interesting to grasp the attention of modern teenagers", giving the movie a rating of 8 out of 10. His compliment went to Nas Muammar who "commanded the character Abu convincingly with his natural talent" as well as Zahiril "who has the potential of a newcomer". 

Nurulhisham Abdul Latiff from Berita Harian complimented all the casts for their successful performance, especially Liyana Jasmay, Nas Muammar and Zahiril. However, she further commented that "the recording style of this film is unsuitable for some audience" but "it was a fine job afterall. 

==Soundtrack==
# Meet Uncle Hussain - Pari-pari di Bawah Angin
# Lailas Lounge - Mawar Khayalan
# Harmacy - Riuh
# Dragon Red - K.A.M.I (Kerana Aku Masih Ingat)
# Couple - Hey Now!
# Meor - Hari Hari Autistik
# Twickheads - Guris
# Telebury - Stars Belong to Us
# Love Me Butch - This is Our Year
# Sharidir - Kamu
# Bittersweet - Going Into the Black Hole
# Koffin Kanser - Berong
# The Aggrobeats - We Got Soul
# They Will Kill Us All - Secret Episodes Butterfingers - 1000 Tahun

==References==
 

==External links==
*  
*  

 
 
 
 