Highlander (film)
 
{{Infobox film
| name           = Highlander
| image          = Highlander 1 poster.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Original style-A poster 
| director       = Russell Mulcahy 
| producer       = {{plainlist|
* Peter S. Davis
* William N. Panzer
}}
| screenplay     = {{plainlist|
* Gregory Widen
* Peter Bellwood Larry Ferguson
}}
| story          = Gregory Widen
| starring = {{Plainlist|
* Christopher Lambert
* Roxanne Hart
* Clancy Brown
* Sean Connery
}}
| music          = {{plainlist| Queen
* Michael Kamen
}}
| cinematography = Gerry Fisher
| editing        = Peter Honess
| studio         = {{plainlist| Cannon Films  Highlander Productions Limited
}}
| distributor    = {{plainlist|
* EMI Films  
* 20th Century Fox  
}}
| released       =  
| runtime        = 110 minutes 
| country        = {{plainlist|
* United Kingdom
* United States
}}
| language       = English
| budget         = $19 million Andrew Yule, Hollywood a Go-Go: The True Story of the Cannon Film Empire, Sphere Books, 1987 p138 
| gross          = $12.9 million   
}} American Cult cult Fantasy fantasy film immortal warriors, depicted through interwoven past and present day storylines.

Despite having enjoyed little success in its initial U.S. release, the  . The films tagline, "There can be only one", has carried on throughout the franchise, as have the songs provided for the film by Queen (band)|Queen.

==Plot==
The opening narration establishes the story of the immortals fighting to the death for centuries. In 1985, Connor MacLeod, also known as the Highlander, is in New York City. In an arena parking garage, Connor is confronted by fellow immortal Iman Fasil, and decapitates him, upon which an energy surge destroys several cars around him. Police swarm the exit to the garage, and arrest Connor.

The story then goes back in time to the 16th century Scottish Highlands, presumably the village of Glenfinnan on the shores of Loch Shiel. Connor and his clan, the Clan MacLeod, prepare for battle.  Back in 1985, the police release Connor, as they have failed to get any information from him. One of the detectives involved, Brenda C. Wyatt, is an expert in metallurgy and recognises Fasils sword as an extremely rare Toledo Salamanca broadsword. Later, Connor returns to the garage to retrieve his own sword but sees Brenda is looking at the crime scene. She finds metal shards embedded in a concrete column and saves them for analysis.
 the Kurgan assists the clan Fraser against the Clan MacLeod in exchange for the sole rights to Connor. In the resulting confrontation the Kurgan stabs Connor but is then driven off by the MacLeod clansmen. The wounded Connor is taken back to the village, and everyone assumes he will die. When he makes a remarkable overnight recovery, the village is convinced that it is the work of the devil. The villagers attempt to have Connor executed, but he is instead exiled by the clan leader.

In 1985, Connor lives under the alias Russell Nash and is a wealthy antiquities dealer. Meanwhile, the Kurgan takes up residence at a seedy motel. Connor tails Brenda to a bar, but she leaves quickly. She then tails Connor, but the Kurgan attacks them both. The fight is interrupted by a police helicopter, and everybody flees. Brenda analyzes the metal fragments and discovers them to be from a Japanese katana, dated about 600 B.C. but made with advanced technology for the era. She makes a date with Connor in an attempt to entrap him. Connor recognises the ruse, tells her to stop digging, and leaves.

In the past, Connor lives an idyllic life with his wife Heather. One day, the mysterious Juan Sánchez Villa-Lobos Ramírez appears and begins training Connor in sword fighting. He explains that they both belong to a group of immortals, who constantly fight one another but can only be killed by complete decapitation. When one immortal decapitates another, the winner receives a transfer of power called "the quickening". Eventually, all the immortals must do battle until there is only one left alive; the last survivor will receive "the Prize". Ramirez tells Connor that the Kurgan, by that time the strongest of the immortals, must not win the Prize, or mankind will enter a dark age. One night, while Connor is away, the Kurgan attacks and decapitates Ramirez.

In Central Park, Connor meets a long-time friend, fellow immortal Sunda Kastagir. They talk about the impending gathering of immortals which precedes the final battle, and they joke about old times. Brenda has by now discovered that Connor has been alive for centuries, living under false identities, faking his death every few decades, signing his assets over to children who had died at birth, and assuming their identities. She confronts Connor, who demonstrates his immortality. After this revelation, Brenda and Connor become lovers, although Connor is reluctant, following a flashback in which Connors wife, Heather, dies of old age in his arms. Ramirez has earlier explained that immortals cannot have children and should not get romantically involved. Ramirez reveals his Japanese katana was made specifically for him by the father of his third wife.

The Kurgan finds Kastagir, killing him and a man who witnessed his beheading. When the Kurgan finds out about Connors relationship with Brenda, he kidnaps her to draw Connor out. After a climactic battle at the Silvercup Studios in Queens, Connor defeats and beheads the Kurgan. He receives the Prize, which manifests itself as a massive quickening. Now mortal and capable of having children, Connor returns to Scotland with Brenda. Connor now has awareness of peoples thoughts around the world, and uses this to encourage cooperation and peace.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-  Connor MacLeod / Russell Nash
|-
| Sean Connery || Juan Sánchez Villa-Lobos Ramírez
|- The Kurgan / Victor Kruger
|-
| Roxanne Hart || Brenda Wyatt
|- Heather MacDonald
|-
| Alan North || Lieutenant Frank Moran
|-
| Jon Polito || Detective Walter Bedsoe
|-
| Sheila Gish || Rachel Ellenstein
|-
| Hugh Quarshie || Sunda Kastagir
|-
| Christopher Malcolm || Kirk Matunas
|- Peter Diamond || Iman Fasil
|-
| Billy Hartman || Dougal MacLeod
|-
| James Cosmo || Angus MacLeod
|-
| Corinne Russell || Candy
|-
| Celia Imrie || Kate MacLeod
|}

==Production and development==

===Conception===
 
Gregory Widen wrote the script for Highlander, then titled Shadow Clan, as a class assignment while he was an undergraduate in the screenwriting program at UCLA. Widen sold the script for US$200,000.

According to William Panzer, joint producer with Peter S. Davis of the Highlander franchise:
 

Widen also used Ridley Scotts 1977 film The Duellists as inspiration for his story.

Widens original draft of the script differed significantly from the movie version. The initial story of the film was darker and more violent. Connor is born in 1408 rather than 1518. He lives with his mother and father. Heather doesnt exist; Connor is promised to a girl named Mara, who rejects him when she learns hes immortal. Connor leaves his village instead of being banished. His alias is Richard Tupin and his weapon is a custom broadsword. Ramirez is a Spaniard born in 1100 instead of an ancient Egyptian born more than two thousand years earlier. The Kurgan is known as the Knight, using the alias Carl William Smith. He is not a savage, but a cold-blooded killer. Brenda is Brenna Cartwright.

Other elements were changed during the rewrite. Initially, immortals could have children; in the draft Connor is said to have had 37. In a flashback in the first draft, Connor attends the funeral of one of his sons. His wife (in her 70s) and his two sons, who are in their mid 50s, see him revealed as an immortal. Also, there are no quickenings in the first draft. When an immortal kills another, nothing special occurs. Nor is there mention of a "prize". When Connor finally kills the Knight, he feels a sharp burning pain. The viewer is then not told if he remains immortal.

===Filming===
  Filming began in April 1985 and ended August 30, 1985.   It took place in Scotland, England, and New York City. 

Director Russell Mulcahy filmed it using music video techniques including fast cutting and pacy music. 

Director of photography Arthur Smith actually filmed the scene in which fish fall out of MacLeods kilt, but Lamberts kilt proved to be too short. Smith said, "I stuck part of a drain pipe above Chriss kilt out of camera range, and fed live trout down the tube." Smith also had difficulties shooting MacLeod meeting the Kurgan. It was raining that day and the crew had to use umbrellas and hair dryers to prevent water from hitting the camera lenses and appearing on the film. Smith also remembered that Lambert, who was near-sighted, "kept forgetting to take off his glasses as he came over the hill on his horse." 
 Greg Gagne, The Tonga Kid. 

The scene where the MacLeod clan sets off to battle is supposed to take place "in the village of Glenfinnan, on the shore of Loch Shiel" in the Lochaber area, but was actually filmed at Eilean Donan Castle, which is in the same general area but is really on the shore of Loch Duich, a sea loch near Kyle of Lochalsh and the Isle of Skye.

According to the DVD commentary, the films climax was originally intended to take place on top of the Statue of Liberty. Then it was changed to an amusement park and finally changed to the rooftop of the Silvercup Studios building.  The opening sequence was originally intended to take place during a National Hockey League game, but the NHL refused because the film crew intended to emphasize the violence of the match. 

The scene in the alley where the Kurgan beheads Kastagir and then stabs the ex-Marine, followed by an explosion, was filmed in an alley in England even though it was set in New York. The director was reluctant to set off the explosion in the alley because the windows were full of Victorian glass, but he was given permission to do so because that particular site was going to be destroyed in a few months anyway. 

The opening voice-over by Connery has an echo effect because it was recorded in the bathroom of his Spanish villa (where he had been working with a voice coach in order to perfect the Spanish accent he used in the film). It was played for the producers over the phone and they approved of it because they could not discern the quality of the recording that way. Feature-length DVD commentary by director Russell Mulcahy and producers William N. Panzer and Peter S. Davis. Located on the 10th Anniversary Highlander Directors Cut Region 1 DVD by Republic Pictures. 

==Soundtrack== A Kind of Magic" and "Princes of the Universe" (the latter also being used for the Highlander television series title sequence).  Queen wrote many of the songs specifically to match the mood of the scenes when the songs play, notably Brian Mays "Who Wants to Live Forever", concerning the doomed love of Connor and his wife Heather.
 One Year Theme from The Works.

The 1995 CD Highlander: The Original Scores includes five cues from Kamens Highlander score (along with six cues from Stewart Copelands Highlander II score, and four cues from J. Peter Robinsons Highlander III score). Furthermore, a rearrangement of an excerpt from Kamens score (specifically, the beginning of the track "The Quickening") was used as the theme music for New Line Cinemas logo indent in the late 1990s and early 2000s.

==Alternate releases==

===Lost scenes===
 
A number of scenes were lost in a fire. They included:
* A duel sequence that introduced an Asian immortal named Yung Dol Kim was cut from the film. The footage for the scene, along with certain other deleted scenes, was later destroyed by fire, although a few stills from the sequence, some in colour and others in black and white, survived. 
* Connor, Kastagir and Bedsoe partying at a bar. The scene expanded more on Kastagir and Connors relationship and revealed that they met during the American Revolutionary War.
* One scene in which Connor shows Brenda his katana after the sex scene.

===Proposed duel in the ending===
 
In the scene following Connor beheading the Kurgan, Mulcahy had originally envisioned an animated dragon with the Kurgans battle helmet emerging from his decapitated body and challenging Connor again. Only after Connor had defeated this ghost-dragon would he have received the final Quickening and subsequent Prize. This idea was eventually cut due to budget constraints.

===Alternate versions===
The European version of the film contained scenes not found in the American version. The directors cut is based upon this version, and it runs eight minutes longer than the US version. 

The additional scenes include: 
* MacLeod having a short flashback about his first battle in Scotland during the wrestling match
* A longer fight scene between Connor and Fasil, mainly Fasil doing backflips through the garage
* A scene showing Connors first love, Kate, bringing him flowers before he goes to battle
* A flashback to World War II that further develops the character of Rachel Ellenstein
* Longer sex scene between Connor and Brenda
* A scene where the Kurgan can be seen in the background trailing MacLeod and Brenda at the zoo
* Much longer fight scene between MacLeod and the Kurgan at the end of the movie

There are several changes in dialogue from the theatrical version:
* Whooshing sounds whenever one Immortal senses another
* When Connor and Ramirez jump into the water during training, Ramirez (in the theatrical version) shouts, "MacLeod, this is the Quickening!"
* When Connor is talking about the 1783 bottle of wine (in the theatrical version), after he says, "Brandy, bottled in 1783", Brendas head can be seen moving but she speaks no dialogue. In the new release, she says, "Wow, thats old."
* After Connor wins the Prize and is being comforted by Brenda (in the theatrical version), he looks up and says, "I want to go home." This is missing in the new release.

The new release is also missing a short scene of Detective Bedsoe spilling coffee on himself while staking out Brendas apartment.   

The French theatrical version of Highlander is mainly the same version as the U.S. theatrical. It includes the World War II flashback but it removes the interior shot of Detective Bedsoe in his car while on a stakeout. This has been issued on 2-disc and 3-disc DVD sets in France with French dialogue only. 

==Release and reception==
Upon initial U.S. release, it was not well-received, but it gained wide and persistent popularity in Europe and on other markets, as well as on home video. It has since become a  , and various other spin-offs. 

The film grossed $2.4 million on its opening weekend and ended with $5.9 million in the US.  Internationally, the film grossed $12.9 million. 

Highlander holds a 67% approval rating on Rotten Tomatoes based on 30 reviews.   Metacritic rated it 24/100 based on seven reviews. 

Danél Griffin of Film as Art awarded the film four stars (out of four), saying: "The key to Highlanders success is in its approach to its subject matter. What could have been a premise that breathes cliché is given a fresh approach due to Mulcahys unique directing style and a  written script.   Highlander is certainly a classic film that will continue to be cherished and watched as the world of movie making continues to grow and change. It is a triumphant example of the art of cinema, and watching it reminds us all of why we like going to the movies in the first place."  Christopher Null of FilmCritic.com gave the film four and a half stars out of five, writing: "Highlander has no equal among sword-and-sorcery flicks."  Null later called Highlander "the greatest action film ever made," saying that it features "awesome swordfights, an awesome score, and a time-bending plotline that only a philistine could dislike". 

Matt Ford of the BBC gave the film three stars out of five, writing: "From the moody, rain-soaked, noir-ish streets of late 20th century America to the wild open spaces of medieval Scotland, Mulcahy plunders movie history to set off his visceral fight scenes with suitably rugged locations.   What the film loses through ham acting, weak narrative, and pompous macho posturing it more than compensates with in sheer fiery bravado, pace, and larger than life action."  Dean Winkelspecht of DVD Town also gave Highlander three stars out of five, writing: "The films slow pace and dated look will turn away many a new viewer   However, there is a certain appeal to the film that brings back many for a second or third helping. I have learned to appreciate the film over the years,   the films story is unique and entertaining." 

Also giving the film three stars out of five, Adam Tyner of DVD Talk wrote, "The screenplay spots a number of intelligent, creative ideas, and I find the very concept of displacing the sword-and-sorcery genre to then-modern-day New York City to be fairly inventive. The dialogue and performances dont quite match many of the films concepts, though. The tone seems somewhat uneven, as if Highlander is unsure if it wants to be seen as a straight adventure epic or if its a campy action flick."  IGN, awarding Highlander a score of 8 out of 10, wrote: "This 80s classic has a lot going for it. The hardcore MTV manner in which it was filmed is common these days, but was groundbreaking then. This movie features some of the best scene transitions committed to celluloid.   To this is added some fun performances by Connery and especially Clancy Brown." 

 ." 

===Home video===
The video was a hit in the United States.  The theatrical release of Highlander II: The Quickening in 1991 significantly increased the rental activity on Highlander even though the sequel was not a box-office success.  Highlander was first released to DVD in the United States in 1997, in a "10th Anniversary Edition" Directors Cut that contained the international uncut version of the film.    A "15th Anniversary" edition was released in Australia in 2001, which also contained the International cut of the film. 
 CD containing three Queen songs from the film) and a standard edition, both of which contain the International uncut version.  On the June 17, 2009 French distributor StudioCanal issued the film on Blu-ray  with identical releases following in Germany,  UK,  Holland, Australia and Japan.  The U.S. directors cut is currently available on DVD in North America from Lionsgate under license from the films current owner, StudioCanal. 20th Century Fox, the theatrical distributor, remains the television rights holder.

===Novelization===
A novelization of the film was written by   known as "The Bedouin," whom he eventually kills. The novel also reveals how the Kurgan gets his customized broadsword and his battle with an Immortal Mongol before meeting MacLeod in 1536.
The novel also introduces an alternate scene showing Conner and Kastagir meeting in the Subway before meeting at the Bridge. One really interesting thing about the novel is that it portrays Conner and Kastagirs relationship very differently from in the film. Here they are just simply two Immortals who are simply not enemies and are just passive friends who can talk about anything without fighting.

The novel also reveals how Heather came to find out about Conners Immortality from Ramirez, the ending of the book is also expanded by revealing that Conner went back to his Antiques store to say his final goodbye to Rachel before leaving for Scotland. Once he and Brenda arrive in Scotland, they tour for two months, and then open an antique shop in Camden Alley. On one occasion, he returns to the Scottish Uplands alone and stares at the remnants of his home with Heather. There is no croft there, but he finds a few stones from the fallen tor and locates the burial place of Ramirez and Heather. He finds two timbers and fashions a crude cross, telling Heather that she would like Brenda. "She is much like you.".

==Remake== Iron Man writers Art Marcum and Matt Holloway were writing the script, but Summit Entertainment turned to Melissa Rosenberg to write it instead, with release scheduled for 2011.    In September 2009, Fast & Furious director Justin Lin was announced as director of the film,  while Neal H. Moritz was slated to co-produce. However in August, Lin dropped out of the film due to commitments to other projects  and was replaced by 28 Weeks Later director Juan Carlos Fresnadillo. In June 2012, Ryan Reynolds was announced to play the lead role of Connor MacLeod.  Due to creative differences, Fresnadillo left in November 2012  and Reynolds also departed in June 2013. 

In October 2013, Cedric Nicolas-Troyan, visual effects supervisor and second unit director on Snow White and the Huntsman, was hired to direct the film, which will be his directorial debut. Filming is set to begin in 2014 using the original script by Art Marcum and Matt Holloway.  In February 2015, it was announced that Dave Bautista would play The Kurgan.  

==See also==
* Scottish Clan
* Scottish Highlands

==References==
 

==External links==
 
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 