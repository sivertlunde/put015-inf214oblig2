Punching the Clown
{{Infobox film
| name           = Punching the Clown
| director       = Gregori Viens Henry Phillips
| written by     = Gregori Viens, Henry Phillps Dave Klein, Eric Klein 
| cinematography = Ian Campbell
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
}} Henry Phillips as a semi-fictionalized version of himself. 

==Plot==
Henry (Phillips) is a struggling singer-songwriter comic in Los Angeles who suddenly finds success and then is subjected to the cruelty of show business.

==Production==
The film includes both scripted scenes as well as portions of actual live performances by Phillips. 

==Reception==
Punching the Clown has been received positively by critics and currently holds an 86% fresh rating on Rotten Tomatoes.  The film also won the Audience Award at the 2009 Slamdance Film Festival. 

==External links==
*  
*  

== References ==
 

 
 
 
 
 

 