Pokémon: The Rise of Darkrai
{{Infobox film
| name             = Pokémon: The Rise of Darkrai
| image            = The Rise of Darkrai 2.JPG
| caption          = North American DVD cover
| film name      = {{film name
| kanji          = 劇場版ポケットモンスター ダイヤモンド&パール ディアルガVSパルキアVSダークライ
| romaji         = Gekijōban Poketto Monsutā Daiyamondo Pāru Diaruga tai Parukia tai Dākurai
| translation    = Pocket Monsters Diamond & Pearl the Movie: Dialga vs. Palkia vs. Darkrai
}}
| director         = Kunihiko Yuyama
| producer         = Mikihiko Fukazawa Takemoto Mori Junya Okamoto Choji Yoshikawa
| writer           = Hideki Sonoda
| narrator         = Rodger Parsons Emily Jenness Bill Rogers Billy Beach   Michele Knotz   Khristine Hvam   Rich McNanna   Scott Williams   Rodger Parsons
| music            = Shinji Miyazaki
| cinematography   = Takaya Mizutani
| editing          = Toshio Henmi
| studio           = OLM, Inc.
| distributor      = Toho (Japan)   Viz Media (USA)   Network DVD (UK)
| released         =    
| runtime          = 90 minutes
| country          = Japan
| language         = Japanese  
| budget           = 
| gross            = $42,496,749 
}}
Pokémon: The Rise of Darkrai, originally released in Japan as   is a 2007 Japanese anime film directed by  , as well as Dialga and Palkia, are the featured Pokémon. 
 Chris Thompson. The song was replaced by "Ill Always Remember You" by Kristen Price (who would also sing the theme for the next season), and another song called "Living in the Shadow" followed afterwards.

Unlike other previous Pokémon films, Pokémon: The Rise of Darkrai share plot elements with the next two films making this the first film of what is said is the Diamond & Pearl trilogy, followed by   and ending in  .

== Plot ==
In the prologue, a scientist named Tonio is reading a diary which belonged to a famous architect named Godey, his great-grandfather, indicating that two Pokémon between their dimension are fighting, and that the battle could lead to chaos. Those written future events are occurring at that same moment as Palkia, who can distort space and Dialga, who rules over time are brawling, which causes the hourglass in Tonios lab to fall and shatter.
 falls for her and his Croagunk Jabs him. Alice is a tour guide as well as a music student who plays the leaf whistle. On their way, the battle between the dimensions continues and causes a massive air current shockwave. Alice then offers a tour of the town.

Later on, Alice brings them to a garden in which many Pokémon reside in peace and where Alice played at as a little girl. Ash, Brock, and Dawn let all their Pokémon free and they run off to do their own things, which then later goes from happiness to chaos. Once the Pokémon begin to quarrel, along with the garden Pokémon present, Alice calms them down playing a soothing song using the leaf whistle, later on revealing that she had learned the song from her grandmother (who appears deceased). Shortly after a Gallade hops right into the scene. Alice indicates that Gallade senses something wrong and thus, the Gallade requests Ash and the rest of them to follow him. The trio recalls their Pokémon and they pursuit the Gallade, ending up at a small temple with some of its pillars damaged. A man named Baron Alberto walks out of the tunnel, claiming that was the work of Darkrai. The name Darkrai puzzles Ash and Dawn, but Brock mentions that Darkrai is a Pokémon who causes nightmares. Everyone hears a noise from a bush nearby and Baron orders his Lickilicky to fire a Hyper Beam attack into the bush. However, the victim was Tonio who screams and limps out of the bush. Alice quickly hurries to Tonios pain.
 she likes him. Tonio blushes and tries not to deny anything. Not too long after, Darkrai appears in the garden. Baron orders Lickilicky to attack the Dark Pokémon, which misses while Darkrai fires a Dark Void at his enemy. Barons Lickilicky dodges the attack and the orb of dark impales Ash, instantly knocking him out to sleep and subsequently, trapping him in a nightmare. The nightmare shows Palkia moving up to Ash and then goes to attack him. This delivers Ash to a part of the garden. After learning the dream is Darkrais doing and Darkrai appears in front of him, Ash reaches for a Poké Ball and tosses it, but it vanishes into thin air. Then, Darkrai disappears to reveal Pikachu, who is about to get devoured by a vacuum hole. Ash dives into the hole to rescue Pikachu. Having failed, he and his Pikachu drop into a neverending fall. He is then awakened by Pikachus Thunderbolt at the Pokémon Center. At night, Tonio tries to find more information in which he succeeds. He reads that Darkrai had opened his heart to Alices grandmother years ago and remains in the garden since then, with Godey witnessing a little bit on the side as she plays the leaf whistle. Meanwhile, Baron meets three TV people who are actually Team Rocket who decide to film Baron as he takes on Darkrai.

The next morning, Ash, Brock, and Dawn visit the Space-Time Towers in town, meeting Tonio there with some news. They climb to the top to be introduced to a massive instrument which plays music by the hour Using the instrument, Dawn plays a special song through the towers bells (The "Shop Theme" from the Legend of Zelda franchise.) As Ash and the rest of the group leave  the Space-Time Towers, a hole blasts out in the middle of the sky just above the towers which then cools off. A massive shockwave has purged through the entire town and Tonio decides to study the sudden outburst.  Afterwards, Darkrai reappears in the courtyard while Ash and company (with Baron joining along) engage in combat with it. Darkrai is outraged and fires multiple loads of Dark Void orbs, placing many Pokémon in the town square into nightmares.  Back at Tonios lab after some intensive research, he finds that the sudden outburst was when the legendary Palkia had entered their dimension, but he did not know why. Ash and Baron chase Darkrai into a dark alley where the Dark Pokémon scores a direct hit on Barons Lickilicky, causing it to doze off. Just then, Baron is turned into a Lickilicky himself (due to a combination of the distorting of space and Lickilicky dreaming that he turned into his master). With that occurring, floating ghost-like images of the sleeping Pokémon floated all around town, some passing through walls, causing the situation to grow quite strange.

After all the Pokémon are at the Pokémon center, Alamos Town is completely covered in a massive fog which can not be cleared off and causes anything which attempts to leave the town to be reversibly entering the town, resulting in no possible escape. Baron still believes that it was still Darkrai who had caused all this. All the trainers follow Baron to confront Darkrai, but Ash, Brock, and Dawn do not go along listening to Tonio, who doesnt believe Darkrai is up to this. He believes Darkrai is good since he had saved Alice in their childhood. It is revealed that the two of them were playing in the garden when Alice tripped off the cliff and fell. Tonio ran to her (knowing that he would fail) but a silhouette of Darkrai zipped out and caught Alice just before disappearing. Hearing that, the trio now agree that it was not Darkrai who caused the havoc. Tonio explains that the distortion caused from the spiritual images of the sleeping Pokémon was caused by an multidimensional disturbance from when Palkia entered the realm. Later that evening, Tonio finds Palkia resting between the towers. Darkrai, who was fighting Baron, instead tries to attack the resting Palkia. Ash then realizes that was what Darkrai was trying to tell him in his nightmare, that the two legendary Pokémon are waging war.
 Spanish and that is the name of the song she plays on the leaf whistle. Tonio indicates that "Oración is the song that could soothe even the fiercest rage". They realize that they must play Oración on the towers musical instrument to stop Dialga and Palkia from their feud.
 
After finding the music disc, Ash, Dawn, Alice and Tonio begin to head up the tower by balloon as Brock helps Nurse Joy evacuate the townspeople. On their way up, Dialga fires its Roar of Time while Palkia stands in between the balloon and Dialga. Palkia dodges the attack, leaving the beam to crossfire with the balloon. Darkrai instantly comes to the rescue and takes the massive blow from Dialga. However, in the process Palkia and Dialga fly by the balloon, causing heavy damage, and leaving Ash and Dawn to travel up by foot which will slow them down. Dialga and Palkia continue their confrontation and now they are going to deal the final blow. Dialga is charging his Roar of Time while Palkia charges his Spacial Rend. When both the legendary Pokémon discharge their attacks, Darkrai crosses the attacks line and forms a sphere around him to block the attacks, engulfing and immobilizing Dialga and Palkia in the process. Baron, who had already turned back to himself due to his lickylicky awakening, then finally believes Darkrai is not the foe. As Ash and Dawn make their way up the tower, they watch as Darkrais sphere gets weaker and eventually shrinks, causing Dialga and Palkia to break free. The dragons, fed up with the Dark Pokémon intervening with their battle, both shoot Darkrai from both sides. Having almost no armor or the energy left, he bravely takes the destructive blow and disintegrates as he floats up into the air. Everyone looks on with the fear that now that Darkrai had perished, they too would suffer the same fate in the next moment. Tonio then mentions that if the dragons signature moves collide once more, their dimension will be completely destroyed.

Ash and Dawn then end up running to the musical instrument, only to find that the electrical power source is down (as the dimension itself was disintegrating). Ashs Pikachu uses Thunderbolt fully and Dawns Pachirisu uses Discharge to get the machine running. The song plays loud throughout the small section of whats left of the town. During the song, the towers start to glow. Each of the six towers (the whole structure is composed of three space and time towers) begin to unveil all their hidden features added by Godey (i.e. blooming flowers and curve-like structures out of the towers). Then, the towers begin to form gigantic golden wings. Those wings fold down and actually restore the lost part of town. The song successfully calms the dragons down, Palkias wound heals, Dialga flies away and Palkia restores everything back, returning Alamos Town to how it originally was along with the townspeople and the Pokémon living in the garden.

Later, the band are mourning for the loss of Darkrai, but thank him for his efforts. Ash and Dawn begin to cry, both believing they would probably never see Darkrai again. As they walk away, Ash stops as he sees a shadow on the cliffside. He along with everyone else looks back at the cliff. Later on the Space-Time Towers, they find Darkrai standing heroically on top. Everyone is relieved that he is safe and Alice cuddles next to Tonio as they look onto Darkrai who then glares with a lit up eye, as the film quickly fades.
 the next adventure that Ash, Dawn and Brock will face.

== Setting ==
The Rise of Darkrai is set in an area based on Barcelona, Spain.    The staff visited this area in September 2006 to form a basis for the movie’s setting. Yuyama travelled there with screenwriter Hideki Sonoda and composer Shinji Miyazaki. Places in The Rise of Darkrai are inspired by places in Spain - the Space-Time Tower and Oración featured are based on the uncompleted Sagrada Família and the Park Güell, respectively, in Barcelona. The name of the architect behind the Space-Time Tower, Godey, and the name of his descendant, Tonio, bear homage to the name of Sagrada Famílias architect, Antoni Gaudí.

== Cast ==

 
 
 
{| class="wikitable sortable"
|-
!Character
!Voice Actor (Japanese)
!Voice Actor (English)
|- Ash Ketchum|Ash/Satoshi || Rica Matsumoto || Sarah Natochenny
|-
|Dawn/Hikari || Megumi Toyoguchi || Emily Bauer
|- Pikachu  || colspan="3" style="text-align: center"| Ikue Ōtani
|- Bill Rogers
|-
|Jessie/Musashi || Megumi Hayashibara || Michele Knotz
|-
|James/Kojirō || Shin-ichiro Miki || rowspan="2" | Jimmy Zoppi
|-
|Meowth/Nyarth || Inuko Inuyama
|- Darkrai || Bill Rogers
|- Tonio || Kōji Yamamoto (child - Daisuke Sakaguchi) || Rich McNanna
|- Alice || Rosa Kato || Khristine Hvam
|- Baron Alberto || Kōichi Yamadera || Sean Reyes
|-
|Kai/Dai || Ryūji Akiyama || Ax Norman
|-
|Maury/Katsumi || Hiroshi Yamamoto || Joshua Swanson
|-
|Torterra/Dodaitose || Hiroyuki Baba || Jimmy Zoppi
|-
|Allegra/Maki || Shoko Nakagawa || Elisabeth Morinelli (a/k/a Elisabeth Williams)
|- Alicia || Chiharu Suzaka|| Kayzie Rogers
|- Narrator || Unshö Ishizuka || Rodger Parsons
|}

== Development and Release ==
As with all Pokémon films, it was announced in Japan after the ending   in the United States; the next two films were distributed by   by the DVD distribution company Network DVD, who specialises in distributing old British TV shows. Oddly enough future Pokémon movies in the UK were released by Universal, which distributed this film in Mainland Europe on DVD. The UK version has since gone out of print, with the possibility of perhaps being re-released by Universal or another distributor in the future.

== Box office and reception==
The Rise of Darkrai topped the Japanese box office charts in the first three days of its release. With a revenue of Japanese yen|¥1.13 billion (United States dollar|$9.26 million) from 1,074,000 viewers, the movie performed better than its predecessor Pokémon Ranger and the Temple of the Sea.    The Rise of Darkrai eventually earned a franchise record of 5.02 billion yen (US$47 million). The Anime News Network gave the film a B-. 

== References ==
 

== External links ==
 
*  
*  
*  
*   at Bulbapedia

 

 
 
 
 
 
 
 