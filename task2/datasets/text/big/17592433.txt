China Cry
{{Infobox film
| name           = China Cry
| image          = China cry cover.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = James F. Collier
| producer       = Don L. Parker
| screenplay     = James F. Collier
| based on       =  
| starring       = Julia Nickson France Nuyen James Shigeta
| music          = Joel Hirschhorn & Al Kasha David Worth
| editing        = Duane Hartzell & Ruby Yang
| studio         = Parakletus, TBN Films
| distributor    = 
| released       =  
| runtime        = 101 min
| country        = USA
| language       = English
| budget         = 
| gross          =   
}}
 Maoist regime brings hardship and misery to her family. She is arrested by authorities, and she believes that only Jesus Christ must have saved her when she survived a firing squad. She is taken to a labour camp while pregnant, but survives to take her children and family to freedom. The film was directed by James F. Collier, and is an example of positive Asian characters in a Christian-themed film.

== External links ==
 

 
 
 
 
 
 