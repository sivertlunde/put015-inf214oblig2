La puerta de no retorno
 

{{Infobox film
| name           = La puerta de no retorno
| image          = LaPuertaDeNoRetorno2011Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Santiago A. Zannou 
| producer       = Shankara Films Dokia Films 
| writer         = 
| screenplay     = Santiago A. Zannou Jaume Martí 
| story          = 
| starring       = 
| music          = Wolfrank Zannou
| cinematography = Albert Pascual 
| editing        = Jaume Martí 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 72
| country        = Spain
| language       = 
| budget         = 
| gross          = 
}}
La puerta de no retorno  is a 2010 film.

== Synopsis ==
Santiago A. Zannou accompanies his father, Alphonse, to his homeland, Benin, 40 years after he left it, to face his fears and his lies. On this journey of redemption, Alphonse will not just seek reconciliation with his only living sister, but also forgiveness from his ancestors, in the hopes of finally laying the hurt of the past to rest.

== References ==
 
*  

 
 
 


 