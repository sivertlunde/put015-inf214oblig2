Inside the Law
{{Infobox film
| name           = Inside the Law
| image          = InsideTheLaw1942Poster.jpg
| image_size     =
| caption        = Film Poster
| director       = Hamilton MacFadden
| producer       = Dixon R. Harwin (producer)
| writer         = Jack Natteford (writer)
| narrator       =
| starring       = See below
| music          = David Chudnow  (uncredited)
| cinematography = Arthur Martinelli
| editing        = Carl Pierson
| distributor    = Producers Releasing Corporation
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}

Inside the Law is a 1942 American film directed by Hamilton MacFadden. It is also known as Rogues in Clover.

== Cast ==
*Wallace Ford as Billy
*Frank Sully as Jim Burke
*Harry Holman as Judge Mortimer Gibbs
*Luana Walters as Dora Mason
*Lafe McKee as Pop Cobb
*Barton Hepburn as Paul Kane
*Danny Duncan as Peter Clifford
*Earle Hodgins as Police Chief
*Rose Plumer as Mom Cobb
*Robert Frazer as Bank Official

== Soundtrack ==
 

== External links ==
* 
* 

==References==
 

 
 
 
 
 
 
 
 
 