Lives Worth Living
{{Infobox film
| name           = Lives Worth Living
| image          =
| alt            =  
| caption        = yes
| director       = Eric Neudel
| producer       = Alison Gilkey
| starring       =
| music          = John Kusiak & P. Andrew Willis
| cinematography = Eric Neudel
| editing        = Bernice Schneider
| studio         =
| distributor    = Storyline Motion Pictures & Independent Television Service (ITVS)
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}} PBS through Independent Television Service|ITVS, as part of the Independent Lens series.  The film is the first television chronicle  
of the history of the American disability rights movement from the post-World War II era until the passage of the Americans with Disabilities Act (ADA) in 1990.   

==Background==
The disability rights movement is a social empowerment movement wherein people with disabilities fight against discrimination and demand equal access and equal opportunity to everything society has to offer, including employment, housing, transportation, telecommunications and state and local government services. 

==Synopsis== civil rights activists a decade earlier, including marches, protests, and civil disobedience.
 deaf president be appointed instead.  People with disabilities formed cross-disability coalitions to demand access to all the things that nondisabled people take for granted, including public transportation, accessible housing, public accommodations, and jobs.  All these efforts culminated in the passage of the Americans with Disabilities Act by Congress, and the ADAs signing by President George H. W. Bush on July 26, 1990. 

==References==
 

==Further reading==
* Bagenstos, Samuel. Law and the Contradictions of the Disability Rights Movement (Yale University Press, 2009). ISBN 978-0-300-12449-1
* Fleischer, Doris Zames and Zames, Frieda. The Disability Rights Movement: From Charity to Confrontation (Temple University Press,  2nd Edition, 2011). ISBN 1-4399-0743-9
* Longmore, Paul, K. and Umansky, Laurie, editors, The New Disability History: American Perspectives (New York University Press, 2001). ISBN 978-0-8147-8564-5
* Pelka, Fred. The ABC Clio Companion to the Disability Rights Movement (ABC-Clio, 1997).  ISBN 978-0-87436-834-5
* Pelka, Fred. What We Have Done: An Oral History of the Disability Rights Movement (University of Massachusetts Press, 2012). ISBN 978-1-55849-919-5.
* Shapiro, Joseph P. No Pity: People with Disabilities Forging a New Civil Rights Movement (Times Books, 1993). ISBN 978-0-8129-2412-1
* Stroman, Duane. The Disability Rights Movement: From Deinstitutionalization to Self-Determination (University Press of America, 2003). ISBN 978-0-7618-2480-0

== External links ==
* 
* 

 
 