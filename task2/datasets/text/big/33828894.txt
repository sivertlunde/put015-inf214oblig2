The Lost Bridegroom
{{infobox film
| name           = The Lost Bridegroom
| image          = The Lost Bridegroom 1916 newspaper - scene.jpg
| imagesize      =
| caption        = Scene from the film, as published in a contemporary newspaper. James Kirkwood
| producer       = Adolph Zukor (Famous Players)
| writer         = Willard Mack (short story: The Man Who Was Lost)
| starring       = John Barrymore
| music          =
| cinematography = H. Lyman Broenig
| editing        =
| distributor    = Paramount Pictures
| released       = March 20, 1916
| runtime        = 5 reels; (50 minutes)
| country        = United States Silent (English intertitles)
}} James Kirkwood lost silent film today.   
 binges during the making of this film, and spent a large amount of time downing drinks in a saloon. These delays caused the production to drag (thus costs go up), so Kirkwood went down to the saloon and motioned to Barrymore to either return to work or that he would sling the actor over his shoulders and carry him back to the set to finish filming.

==Cast==
*John Barrymore - Bertie Joyce
*Katherine Corri Harris - Dorothy Hardin (*as Katherine Harris Barrymore)
*Ida Darling - Mrs. Amelia Hardin
*June Dale - Madge McQuirk
*Hardee Kirkland - Black McQuirk
*Eddie Sturgis - Slim Denny (*Edward Sturgis) John T. Dillon - Crook
*Tammany Young - Crook
*Fred Williams -

unbilled James Kirkwood
*William Sherwood

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 


 