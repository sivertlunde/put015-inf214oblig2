Running Water (film)
{{Infobox film
| name           = Running Water
| image          =
| caption        =
| director       = Maurice Elvey
| producer       = 
| writer         = A.E.W. Mason (novel)   Kinchen Wood 
| starring       = Madge Stuart   Lawford Davidson   Julian Royce 
| cinematography = 
| editing        = 
| studio         = Stoll Pictures 
| distributor    = Stoll Pictures
| released       = September 1922 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} Running Water. 

==Cast==
*    Madge Stuart as Sylvia Skinner  
* Lawford Davidson as Capt. Hilary Cheyne  
* Julian Royce as Garrett Skinner  
* A. Bromley Davenport as Capt. Barstow  
* Irene Rooke as Mrs. Thesiger   George Turner as Wallie Hine  
* E. Lewis Waller as Archie Parminter   George Harrington as Michel

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 