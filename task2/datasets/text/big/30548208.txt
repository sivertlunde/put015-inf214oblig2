The Iron Lady (film)
 
 
{{Infobox film
| name           = The Iron Lady
| image          = Iron lady film poster.jpg
| image_size     = 220px
| alt            = 
| caption        = British cinema poster
| director       = Phyllida Lloyd
| producer       = Damian Jones
| writer         = Abi Morgan
| starring       = Meryl Streep Jim Broadbent Iain Glen Olivia Colman Anthony Head Richard E. Grant
| music          = Thomas Newman 
| cinematography = Elliott Davis
| editing        = Justin Wright Film4 UK Film Council Yuk Films Canal+ CinéCinéma Classic|CinéCinéma Goldcrest Films DJ Films
| distributor    = 20th Century Fox (U.K.) The Weinstein Company (U.S.A.) Icon Productions (Australia)

| runtime        = 104 minutes  
| country        = United Kingdom France
| language       = English
| released       =      
| budget         = £8.2 million 
| gross          = $115 million   
}} cabinet member and eventual deputy, Geoffrey Howe, is portrayed by Anthony Head. 
 Best Actress Oscar nomination for her portrayal and ultimately won the award, 29 years after her first win. She also earned her third Golden Globe Award for Best Actress – Motion Picture Drama award (her eighth Golden Globe Award win overall), and her second BAFTA Award for Best Actress in a Leading Role. The film also won the Academy Award for Best Makeup and the BAFTA Award for Best Makeup and Hair.

==Plot== Mark lives Carol is at times strained.
 House of Conservative Party and eventual victory, and her voice coaching and image change.
 1979 general Ian Gilmour, Francis Pym, Jim Prior), bombing in Grand Hotel retake the ARA General Belgrano and Britains subsequent victory in the Falklands War, her friendship with President of the United States|U.S. President Ronald Reagan and emergence as a world figure, and the economic boom of the late 1980s.

By 1990, Thatcher is shown as an imperious but aging figure, ranting aggressively at her cabinet, refusing to accept that the Community Charge (the "Poll Tax") is regarded as unjust, even while it is causing riots, and fiercely opposed to European Integration. Her deputy Geoffrey Howe resigns after being humiliated by her in a cabinet meeting, Heseltine challenges her for the party leadership and her loss of support from her cabinet colleagues leaves her little choice but to reluctantly resign as Prime Minister after 11 years in office. A teary-eyed Margaret exits 10 Downing Street for the last time as Prime Minister with Denis comforting her. She is shown as still disheartened about it almost twenty years later.

Eventually, Thatcher is shown packing up her late husbands belongings, and telling him its time for him to go. Deniss ghost leaves her as she cries that she actually is not yet ready to lose him, to which he replies "Youre going to be fine on your own... you always have been" before leaving forever. She is finally shown in her kitchen, alone, contentedly washing a teacup (a wifely role she had told Denis she would never accept), having finally overcome her grief.

==Cast==
 
*Meryl Streep as Margaret Thatcher
**Alexandra Roach as young Margaret 
*Jim Broadbent as Denis Thatcher 
**Harry Lloyd as young Denis
*Iain Glen as Alfred Roberts
*Olivia Colman as Carol Thatcher
*Anthony Head as Geoffrey Howe
*Nicholas Farrell as Airey Neave
*Richard E. Grant as Michael Heseltine Susan Brown as June - Margaret Thatchers live-in carer
*Martin Wimbush as Mark Carlisle
*Paul Bentley as Douglas Hurd
*Robin Kermode as John Major
*John Sessions as Edward Heath
*Roger Allam as Gordon Reece Reg Prentice
*Michael Pennington as Michael Foot Angus Wright as John Nott Francis Pym Jim Prior Ian Gilmour Nicholas Jones as Admiral Henry Leach John Fieldhouse Matthew Marsh as Alexander Haig archive footage) as himself
**Reginald Green  as Ronald Reagan 
 

==Production==
Filming began in the UK on 31 December 2010, and the film was released in late 2011.
 House of MPs in neogothic Manchester Town Hall, which is often used as a location double for films which feature the Houses of Parliament because of its architectural similarity. 

Streep said: "The prospect of exploring the swathe cut through history by this remarkable woman is a daunting and exciting challenge. I am trying to approach the role with as much zeal, fervour and attention to detail as the real Lady Thatcher possesses  I can only hope my stamina will begin to approach her own." 

==Release==

===Historical inaccuracies===
It is suggested in the film that Thatcher had said goodbye to her friend Airey Neave only a few moments before his assassination, and had to be held back from the scene by security officers.  In fact, she was not in Westminster at the time of his death and was informed of it while carrying out official duties elsewhere.

The film does not portray any other female MPs in Parliament of the United Kingdom|Parliament. In fact, during Thatcher’s time in Parliament, the total number of female MPs was between 19 and 41. 

The Labour Party leader Michael Foot is depicted as a critic of the decision to send a task force to the Falkland Islands, and Thatcher is shown admonishing him in the wake of Britains victory over Argentina. In fact, Foot supported the decision to send a task force, something for which Thatcher expressed her appreciation. 

===Critical reception===
The Iron Lady received mixed reviews from critics, with unanimous praise for Streeps performance. On  , the film has a score of 54 out of 100, based on 41 critics, indicating "mixed or average reviews". 

The films depiction of Thatcher has been criticized by her children,   |date=9 February 2011 |accessdate=9 February 2011}}  Karen Sue Smith of America (Jesuit magazine)|America wrote that "by combining the Baroness’s real roles of wife, mother and leader, the film’s portrait of her does what many purported "lives of great men" fail to do—namely, show the person in context, in the quotidian." 

  gave the film two stars out of four, praising Streeps performance but lamenting that "shes all dressed up with nowhere to go" in a film that cannot decide what it wants to say about Thatcher: " ew people were neutral in their feelings about  , except the makers of this picture".  . rogerebert.com. Retrieved 8 February 2012. 

Film review blog Movie Metropolis praised Streeps performance but criticized the lack of depth given to the rest of the story, which seemed to only focus on the glory days of Thatchers reign.   

Despite the films mixed reviews, Streeps performance in the title role garnered much critical acclaim. Kevin Maher of  . Retrieved 16 November 2011.  David Gritten in   of the   of Time (magazine)|Time named Streeps performance one of the Top 10 Movie Performances of 2011. 

Streeps portrayal ultimately won her the Academy Award for Best Actress (her 17th nomination and third award overall), as well as several other awards, including the BAFTA Award for Best Actress in a Leading Role, and the Golden Globe Award for Best Actress – Motion Picture Drama.   The film also won the Academy Award for Best Makeup.

As of March 2012, Thatcher herself had not watched the film, nor had her children. 

===Box office===
The film grossed $30 million in the USA, plus $84 million outside the USA, for a combined gross of $114 million. 

===Soundtrack===
 
#"Soldiers of the Queen"
#"MT"
#"Grocers Daughter"
#"Grand Hotel"
#"Swing Parliament" Shall We Dance?"
#"Denis"
#"The Great in Great Britain"
#"Airey Neave"
#"Discord and Harmony"
#"The Twins"
#"Nation of Shopkeepers"
#"Fiscal Responsibility"
#"Crisis of Confidence"
#"Community Charge"
#"Casta Diva"
#"The Difficult Decisions"
#"Exclusion Zone"
#"Statecraft"
#"Steady the Buffs" Prelude No. 1 in C Major, BWV 846" (Johann Sebastian Bach)
 

The trailer for the film features  s theme tune for the science-fiction film Moon (film)|Moon. 

Not included on the soundtrack album or listings although credited among the eight songs at the end of the film is "Im in Love with Margaret Thatcher|(Im In Love With) Margaret Thatcher" by Burnley punk band Notsensibles, which was re-released as a single due to the publicity. The song appears seventy-five minutes into the film, as part of the Falklands War victory celebrations.

==Awards and nominations==
 
{| class="wikitable"
|- align="center" Awards and Nominations
|- align="center"
! Award
! Category
! Nominee
! Result
|- 84th Academy Awards   The Full List |work=The Guardian |date=27 February 2012 |accessdate=27 March 2012}}     Best Actress
| Meryl Streep
| 
|- Academy Award Best Makeup
| Mark Coulier and J. Roy Helland
| 
|- 1st AACTA Australian Academy of Cinema and Television Arts Best International Actress
| Meryl Streep
| 
|- BAFTA Awards   Best Leading Actress
| Meryl Streep
| 
|- Best Original Screenplay
| Abi Morgan
| 
|- Best Supporting Actor
| Jim Broadbent
| 
|- Best Makeup and Hair
| Marese Langan, Mark Coulier and J. Roy Helland
| 
|- Boston Society of Film Critics Best Actress
| Meryl Streep
| 
|- Broadcast Film Critics Association Awards
| Best Actress
| Meryl Streep
| 
|-
| Best Make-up
| Marese Langan
| 
|- Central Ohio Film Critics Association
| Best Actress
| Meryl Streep
| 
|- Chicago Film Critics Association Awards
| Best Actress
| Meryl Streep
| 
|-
|Dallas-Fort Worth Film Critics Association Awards
| Best Actress
| Meryl Streep
| 
|- Denver Film Critics Society Best Actress
| Meryl Streep
| 
|- 69th Golden Golden Globe Awards Golden Globe Best Actress – Motion Picture Drama
| Meryl Streep
| 
|- Irish Film and Television Awards Best International Actress
| Meryl Streep
| 
|-
| Best Costume Design
| Consolata Boyle
| 
|- London Critics Circle Film Awards
| Best Actress
| Meryl Streep
| 
|-
| British Actress of the Year
| Olivia Colman
| 
|- National Society of Film Critics Awards
| Best Actress
| Meryl Streep
| 
|- 2011 New York Film Critics Circle Awards Best Actress Meryl Streep
| 
|- New York Film Critics Online Awards 2011 Best Actress
| Meryl Streep
| 
|- Online Film Critics Society Awards Best Actress
| Meryl Streep
| 
|- Phoenix Film Critics Society Awards Best Actress
| Meryl Streep
| 
|- Satellite Awards Best Actress – Motion Picture
| Meryl Streep
| 
|- 18th Screen Screen Actors Guild Awards Screen Actors Female Actor in a Leading Role
| Meryl Streep
| 
|- Southeastern Film Critics Association Awards
| Best Actress 
| Meryl Streep
| 
|- Toronto Film Critics Association Awards
| Best Actress 
| Meryl Streep
| 
|- Washington D.C. Area Film Critics Association Best Actress
| Meryl Streep
| 
|}

==Home media==
The Iron Lady was released on DVD in the United States and the United Kingdom on 30 April 2012. The special features in the DVD include Making The Iron Lady, Bonus Featurettes, Recreating the Young Margaret Thatcher, Battle in the House of Commons, Costume Design: Pearls and Power Suits, Denis: The Man Behind the Woman. 

==See also==
 
 
* 2013 in film List of 20th Century Fox films 
* List of biographical films
* List of British films of 2013
* List of films set in London
* List of French films of 2013
 
 
 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 