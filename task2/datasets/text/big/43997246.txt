Aniyatha Valakkal
{{Infobox film
| name = Aniyatha Valakkal
| image =
| image_size =
| caption =
| director = Balachandra Menon
| producer = NP Abu
| writer = Balachandra Menon
| screenplay = Balachandra Menon
| starring = Sukumaran MG Soman Venu Nagavally Sankaradi
| music = A. T. Ummer
| cinematography = Vipin Das
| editing = G Venkittaraman
| studio = Priya Films
| distributor = Priya Films
| released =  
| country = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, directed by Balachandra Menon and produced by NP Abu. The film stars Sukumaran, MG Soman, Venu Nagavally and Sankaradi in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
  
*Sukumaran
*MG Soman 
*Venu Nagavally 
*Sankaradi
*Jagathy Sreekumar  Ambika 
*Shobha  Shubha
*K. P. A. C. Azeez
*T. R. Omana
*Alleppey Ashraf
*Sreelatha Namboothiri Santhakumari
 

==Soundtrack==
The music was composed by A. T. Ummer. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Janaki || Bichu Thirumala || 
|-  Janaki || Bichu Thirumala || 
|- 
| 3 || Padinjaaru chaayunna || K. J. Yesudas, Vani Jairam || Bichu Thirumala || 
|- 
| 4 || Piriyunna Kaivazhikal || K. J. Yesudas || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 


 