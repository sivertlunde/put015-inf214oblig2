Supahpapalicious
{{Infobox film
| name           = Supahpapalicious
| image          = SupahpapaliciousPoster.JPG
| caption        = Theatrical movie poster
| director       = Gilbert Perez
| producer       = Kara Kintanar Chinno B. Marquez Marizel V. Samson
| writer         = Jullian Abuda
| starring       = Vhong Navarro Valerie Concepcion Makisig Morales
| music          =
| cinematography =
| editing        =
| distributor    = Star Cinema
| released       = 22 March 2008
| runtime        =
| country        = Philippines
| awards         =
| language       = Filipino / Tagalog, English
| budget         =
| gross          = PHP 67 Million
| preceded_by    =
| followed_by    =
}}

SupahPapalicious is a 2008 comedy film from Star Cinema starring Vhong Navarro, Valerie Concepcion and Makisig Morales.

==Plot==
The spurned lover Adonis (Vhong Navarro) has to win over Athenas only son, Atong (Makisig Morales), to court his single mom, Athena (Valerie Concepcion). Atong decides the only kind of man good enough for his mother is a man with a big family. Adonis help from his friends to work in special effects to transformed into his pretending imaginary family in disguise: a spunky Lola and a wacky Black American to be able to win Athena’s heart to convince Atong that Adonis is good enough to win the love of his life. 

==Cast and characters==
* Vhong Navarro as Adonis/Dodong/Tita Barbara/Lola Paulina/Apl
* Valerie Concepcion as Athena/Ateng
* Makisig Morales as Atong
* Pokwang as Nymfa/Inday
* Baron Geisler as Sir Juno
*Julius Empoy Marquez as Hercules
*Mura as Macho
*Marvin Raymundo as Eman Loloco/Dodong KitKat as Diosa
*Harold Billy Cells (cameo)

==References==
 

==See also==
* Star Cinema
 

 
 
 
 
 
 


 
 