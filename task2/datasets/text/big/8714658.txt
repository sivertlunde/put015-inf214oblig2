Guide (film)
 
{{Infobox film
| name           = Guide
| image          = Guide 1965 film poster.jpg
| caption        = Film poster Vijay Anand
| producer       = Dev Anand
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| starring       = 
| narrator       = Dev Anand Waheeda Rehman Leela Chitnis
| music          = S. D. Burman
| cinematography = Fali Mistry
| editing        = Vijay Anand Babu Sheikh
| studio         = 
| distributor    = 
| released       =  
| runtime        = 183 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Romantic Drama Vijay Anand, Indian film industry. 
 Time Magazine listed it at Number Four on its list of Best Bollywood Classics. 

A 120-minute U.S. version was written by Pearl S. Buck, and directed and  produced by Tad Danielewski,  The film was screened at the 2007 Cannes Film Festival, 42 years after its release.   

==Plot==
 
The movie starts with Raju (Dev Anand) being released from jail. Raju was a freelance guide, who earned his living by taking tourists to historic sites. One day, a wealthy and aging archaeologist, Marco (Kishore Sahu) comes to the city with his young wife Rosie (Waheeda Rehman), the daughter of a courtesan. Marco wants to do some research on the caves outside the city and hires Raju as his guide.

While Marco devotes himself to the discovery of the cave, Raju takes Rosie on a tour and appreciates her dancing ability and innocence. He learns about Rosies background as a daughter of a prostitute and how Rosie has achieved respectability as the wife of Marco but at a terrible cost. She had to give up her passion of dancing since it was unacceptable to Marco. Meanwhile, Rosie tries to commit suicide by consuming poison. Marco, upon learning of the incident, returns from the caves to see Rosie and is furious with Rosie after seeing her alive. He tells her that her act of committing suicide was a drama, otherwise she would have consumed more sleeping pills so that she could really have died. Upon returning to the caves which were discovered, Rosie learns that Marco is spending time and enjoying the company of a native tribal girl. She is enraged at Marco and both indulge in a serious heated discussion, which concludes with Rosie leaving the caves, and she once again wants to end her life.

Raju calms her down by saying that committing suicide is a sin, and that she should live to pursue her dream. She finally says good-bye to the relation of being the wife of Marco. Now she needs support and a home. Raju gives her shelter. Rosie is considered a prostitute by Rajus community (as classical dancing traditionally was prostitutes work at royal courts), which leads to many problems, including his mother and her brother insisting that Rosie be kicked out. Raju refuses and his mother leaves him. His friend and driver also falls out with him over Rosie. Raju loses his business and the entire town turns against him. Undeterred by these setbacks, Raju helps Rosie embark on a singing and dancing career and Rosie becomes a star. As she rises as a star, Raju becomes dissolute — gambling and drinking. Marco comes back on the scene. Trying to win Rosie back, he brings flowers and has his agent ask Rosie to release some jewelry which is in a safe deposit box. Raju, a bit jealous, does not want Marco to have any contact with Rosie and forges Rosies name on the release of the jewels. Meanwhile, Rosie and Raju drift apart due to Rosies incomprehensible behaviour when she tortures Raju by not obliging him a caring hug even and asks him to leave her room else she says she will have to go out. Before this, they also had a discussion about how a man should live when Rosie remembers Marco and tells Raju that Marco was probably correct when he used to say that a man should not live on a womans earnings.

Raju retorts by saying that she is under a misunderstanding that she has become a star on her own and it was only because of Rajus efforts that she became famous. Later, Rosie learns of the forgery release. Raju is convicted of forgery, resulting in a two-year sentence. Rosie does not understand why Raju indulged in forgery, when he could have easily asked her for money. It was not money, it was the loving fascination for Rosie which urged Raju not to reveal Marcos visit to Rosie so that she doesnt remember him again and to eliminate the probability of Rosie and Marcos togetherness, if at all, there was any little chance. On the day of his release, his mother and Rosie come to pick him up but they are told that he was released six months ago because of his good behaviour.

Meanwhile, upon his release Raju wanders alone. Despair, poverty, rags, hunger, loneliness engulf him until he finds a wandering group of sadhus (holy men) with whom he spends a night at a derelict temple in a small town.Raju impresses the woman with the logic in taking a husband and she submits, which convinces Bhola that Raju is a swami (holy man). Impressed by this, Bhola spreads the news through the village. Raju is taken as a holy man by the village. Raju assumes the role of village holy man (Swami Ji) and engages in skirmishes with the local pandits.And drama started here.
Due to drought Raju was forced to fast for 12 days so that it rains. Meanwhile his mother, friend and Rosie unite with him and patch things up. In the end it rains but Raju dies.

==Cast==
* Dev Anand as Raju
* Waheeda Rehman as Rosie Marco / Miss Nalini
* Leela Chitnis as Rajus Mother
* Kishore Sahu as Marco
* Gajanan Jagirdar as Bhola Anwar Hussain as Gaffoor
* Ulhas as Rajus Maternal Uncle Rashid Khan  as Joseph
* Krishan Dhawan as Inspector Girdhari
* Ram Avtar as Pandit
* Narbada Shankar as Shastri
* Nazir Kashmiri as villager

==Production== 1962 Berlin Film Festival. Somebody suggested The Guide. Dev Anand purchased the book and read it at one go. He called up Pearl who invited him to the United States to discuss the project. With their approval, he called up R. K. Narayan and procured the rights to the book. {{Cite book
 | last= Anand
 | first= Dev
 | authorlink= Dev Anand
 | title= Romancing with Life - an autobiography
 | publisher= Penguin Viking
 | year= 2007
 | pages=182–184
 | isbn= 0-670-08124-8}}
 
 Chetan Anand Vijay Anand who stepped in, as the film proved a landmark for him.    

The song, Aaj phir jeene ki tamanna hai which was picturised on Waheeda Rehman was shot in the Chittorgarh Fort in Rajasthan.  The climax of the film was shot in Limdi town, 90&nbsp;km from Ahmedabad as it has Bhugaro river which flows only during the monsoon. Chetan Anand was a classmate of erstwhile royals of Limbi, Janaksinhji of Jhala family at Doon School in Dehradun, taught English at Limbi, in 1941.    Towards the end of the film, theres a scene where a foreign journalist arrives to interview Raju (Devs character in the movie) after he becomes an ascetic. Dev wanted a young, good-looking foreigner for the role. So, he asked an associate to get one in five hours! The associate rushed to Ahmedabad. Walking down a road, he spotted a tall, well built foreigner. He went up to him and bluntly asked: ‘Do you have a good-looking wife?’ He glared at him. Realizing his blunder, he clarified that the unit were looking for a foreigner to feature in an Indian movie. The couple agreed, and he drove them back to Limdi for the shoot. 

==Music==
{{Infobox album  
| Name        = Guide
| Type        = Soundtrack
| Artist      = Sachin Dev Burman
| Cover       =
| Released    = 1965 (India)
| Recorded    = 1964
| Genre       = Film soundtrack|
| Length      = 38:01
| Label       = The Gramophone Company of India (Private) Limited  
| Producer    = Sachin Dev Burman
| Last album  = Ziddi (1964 film)|Ziddi (1964)
| This album  = Guide  (1965)
| Next album  = Teen Devian  (1965)        
|}} Shailendra and they were sung by Mohammed Rafi, Lata Mangeshkar, Kishore Kumar, Manna Dey and Sachin Dev Burman. The soundtrack was listed by Planet Bollywood as number 11 on their list of 100 Greatest Bollywood Soundtracks. 

{| border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! Song !! Singer(s) !! Picturised on
|-
| "Aaj Phir Jeene Ki Tamanna"
| Lata Mangeshkar
| Dev Anand & Waheeda Rehman
|-
| "Din Dhal Jaaye"
| Mohammed Rafi
| Dev Anand & Waheeda Rehman
|-
| "Gaata Rahe Mera Dil"
| Kishore Kumar & Lata Mangeshkar
| Dev Anand & Waheeda Rehman
|-
| "Kya Se Kya Ho Gaya"
| Mohammed Rafi
| Dev Anand & Waheeda Rehman
|-
| "Piya Tose Naina Laage Re"
| Lata Mangeshkar
| Waheeda Rehman
|-
| "Saiyaan Beimaan"
| Lata Mangeshkar
| Dev Anand & Waheeda Rehman
|-
| "Tere Mere Sapne"
| Mohammed Rafi
| Dev Anand & Waheeda Rehman
|-
| "Wahan Kaun Hai Tera"
| Sachin Dev Burman
| Dev Anand
|-
| "He Ram Hamare Ramchandra"
| Manna Dey & Chorus
| Dev Anand
|-
| "Allah Megh De Paani De"
| Sachin Dev Burman
| Dev Anand
|}

==Awards== Best Foreign Language Film at the 38th Academy Awards, but was not accepted as a nominee.  Guide was also first film to win all four of the major awards (Best Movie, Best Director, Best Actor and Best Actress) at the Filmfare Awards.

{| class="wikitable" 
|-
! Ceremony
! Award
! Category
! Nominee
! Outcome
! Note
|- 38th Academy Awards Academy Award List of Best Foreign Language Film Dev Anand
|  Eighth film submitted by India
|- National Film National Film Awards 13th National Film Awards Certificate of Merit for the Third Best Feature Film
|rowspan="5"  
|
|- Filmfare Awards 14th Filmfare Awards Filmfare Award Best Film Received on behalf of Navketan Films
|- Filmfare Award Best Director Vijay Anand Vijay Anand
|
|- Filmfare Award Best Actor Dev Anand
|
|- Filmfare Award Best Actress Waheeda Rehman
|
|- Filmfare Award Best Music Director
|S.D. Burman
|rowspan="2"  
|
|- Filmfare Award Best Female Playback Singer Lata Mangeshkar For "Aaj Phir Jeene Ki Tamana Hai"
|-
|
|
|- Filmfare Award Best Dialogue Vijay Anand Vijay Anand
|
|- Filmfare Award Best Cinematographer Fali Mistry Color category
|}

==Differences from the novel==
* In the film, Raju meets a celebrity death surrounded by his near and dear ones and the media with rain ending the drought in the village. However, in the novel, this event is ambiguous with an unclear ending about his death or the end of the drought.
* In the novel, Raju is shown to woo Rosie, but in the movie, Rosie is already unhappy with her marriage. Upon seeing her husband with another woman, she leaves him and goes to Raju.

==See also==
* List of submissions to the 38th Academy Awards for Best Foreign Language Film
* List of Indian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==Bibliography==
*  

==External links==
*  

 
 
 
 

 
 
 
 
 
 
 
 