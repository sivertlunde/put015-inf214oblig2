Tu ne tueras point
Tu ne tueras point (Thou Shalt Not Kill), also known as Lobjecteur, is a 1961 French feature film directed by Claude Autant-Lara, written by Jean Aurenche and Pierre Bost, and starring Laurent Terzieff and Horst Frank. Actress Suzanne Flon won the Best Actress award at the 1961 Venice Film Festival for her role in the film. 

==Plot==
The film is set in France at the end of World War II.  It is about a conscientious objector (Laurent Terzieff), imprisoned and on hunger strike, because of his opposition to war. He finds himself in jail with a German priest who had killed a French Resistance fighter. This set-up allows Autant-Lara to explore ideas about morality, obedience, and religion.

==Production and release==
Autant-Lara had trouble obtaining funding for the film, in part because of its anti-war message at the time of Frances involvement in the Algerian War. He invested his own money in the film, and received support from Yugoslavia. The film was shown at the 1961 Venice Film Festival, but the French refused to show it in their pavilion so it was shown by the Yugoslavs. The film was then blocked from release in France for 2 years. 

==Reception==
The film was badly received by the French press of its time. Bernard Dort, in one of the few positive reviews, noted that the film was more a study of its two main characters than an anti-war tirade.  

AllMovie gives it 3/5, criticising Autant-Laras "cut-and-dried directorial techniques". 

==Awards==
Suzanne Flon, playing the conscientious objectors mother, won the Volpi Cup at the 1961 Venice Film Festival for best actress. The film was nominated for Best Film at the 1963 British Academy Film Awards. 

==References==
 

==External links==
* 
* 

 

 
 
 