Keroro Gunso the Super Movie: Creation! Ultimate Keroro, Wonder Space-Time Island
{{Infobox film
| name           = Keroro Gunso the Super Movie 5: Creation! Ultimate Keroro, Wonder Space-Time Island
| image          =
| caption        = 
| director       = 
| producer       = 
| writer         = Mine Yoshizaki (original manga)
| starring       = Kumiko Watanabe   Houko Kuwashima   Yūtarō Honjō   Tamaki Matsumoto
| music          = 
| cinematography =  Sunrise
| editing        = 
| distributor    = 
| released       =  
| runtime        =
| country        = Japan
| language       = Japanese
| gross          = $2,560,000
}} Keroro Gunso. 

The film was released in theatres on February 27, 2010 in Japan.  It was produced by Sunrise (company)|Sunrise,the studio behind the TV anime. 

==Plot==
Fuyuki is seen walking around an empty black space. But what he does see is a burning ship crashing from the sky and when he turns around the Keroro platoon (minus Keroro) is frozen in stone. When Keroro is located he is blown up and Fuyuki wakes up to find Alisa Southerncross who gives him a statue that looks like a Keronian. She leaves before the house starts to shake and is flooded with water from one of Kululus failed experiments. The after affect is the platoon cleaning up all the water damage. Fuyuki shows Keroro the statue and the island where it was originated from and convinces Keroro to go with him while Keroro makes plans of building a second base there. They fly to the island at night while back at the Hinata house Kululu who is sharing a tent with Giroro is informing him on Aku Aku the being on Easter Island, the island their friends are heading to. While flying a beam of energy hits the space bike that Fuyuki and Keroro are riding and it sends them in separate directions. Fuyuki wakes up and meets Lo and Rana who are trying to cook the sgt. They both have a joyful reunion and learn about Kanini. Giroro and Kululu find out about Fuyuki and Keroro sneaking out to the island and decide to get Dororo and Tamama because Aku Aku has been making trouble and they might be endanger.

==Reception==
The film debuted in the Japanese box office sixth, earning US$662,651 on 144 screens.  It was the 13th most watched anime film of the first of half of 2010 in Japan. 

==References==
 

==External links==
*    
*  

 

 
 
 


 