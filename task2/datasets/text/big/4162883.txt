The Creation of the Humanoids
 

{{Infobox Film
| name           = The Creation of the Humanoids 
| caption        = 
| image	=	The Creation of the Humanoids FilmPoster.jpeg
| director       = Wesley Barry (billed as Wesley E. Barry)
| producer       = Wesley Barry Edward J. Kay
| writer         = Jay Simms
| starring       = Don Megowan Erica Elliot Frances McCann Don Doolittle George Milan Dudley Manlove
| music          = Edward J. Kay
| cinematography = Hal Mohr
| editing        = Ace Herman (as Leonard W. Herman)
| distributor    = Emerson Film Enterprises
| released       =  
| runtime        = 84 minutes
| country        = United States
| awards         = 
| language       = English
| budget         = 
}}

The Creation of the Humanoids is a 1962 US science fiction film release, directed by Wesley Barry and starring Don Megowan, Erica Elliot, Frances McCann, Don Doolittle and Dudley Manlove. The film is not based on the plot of Jack Williamsons novel The Humanoids, to which it bears little resemblance, but on an original story and screenplay written by Jay Simms. 
 post nuclear war society, blue-skinned, silver-eyed Humanoid robot|human-like robots have become a common sight as the surviving population suffers from a decreasing birth rate and has grown dependent on their assistance. A fanatical organization tries to prevent the robots from becoming too human, fearing that they will take over. Meanwhile, a scientist experiments with creating human replicas that have genuine emotions and memories.

==Plot==
 nuclear war that destroyed 92 percent of humanity. Lingering radiation has caused the birth rate to fall below replacement level and the population continues to decline. A robotic labor force maintains a high standard of living for the survivors and the humanoids of the title are an advanced type of robot created to directly serve and otherwise work closely with human beings. These humanoids are built with artificial, ultra-logical personalities and they appear human except for their blue-gray "synthe-skin", metallic eyes and lack of hair. The humanoids periodically visit recharging stations they call "temples" where they also exchange all information acquired since their last visit with a central computer they call "the father-mother".

A quasi-racist human organization named The Order of Flesh and Blood is opposed to the humanoids, which the members disparagingly refer to as "clickers". The Order believes the humanoids are planning to take over the world and are a threat to the very survival of the human race. The Order does not stop at illegal violent actions, including bombings. At one meeting, its members are alarmed to learn of the existence of a humanoid which has been made externally indistinguishable from a human and which has killed a man. They demand that all existing humanoids be disassembled or downgraded to a strictly utilitarian machine-like form.
 thalamic Organ transplantation|transplant", which transfers the memories and personality of a recently deceased human into a robotic replica of that person. The human-humanoid hybrids that result awake from the process unaware of their own transformation, although their human personalities are shut off between 4 and 5 A.M., when they report back to the humanoids at the robot temple. As Dr. Raven describes the operation, "We draw off everything that makes a man peculiar to himself. His learning, his memory: these, inter-reacting, constitute his personality, his philosophy, capability and attitude. The human brain is merely the vault in which the man is stored." With the help of Dr. Raven, the humanoids are secretly replacing humans who recently died with these replicas.

One of the leaders of the Order of Flesh and Blood, Captain Kenneth Cragis (Megowan), meets Maxine, and although she is opposed to the Order they both fall in love. In the end they discover that they, too, are advanced humanoid replicas with the minds of deceased persons. Ironically, the "real" Maxine had died in a bomb attack which the Order intended to harm only robots. Dr. Raven, a once-human replica himself, explains to Cragis and Maxine that not only are they practically immortal in their new forms, they can also be the first humanoids upgraded to the highest possible level: after an alteration, they will be able to reproduce. Then he tells the viewers, "Of course the operation was a success... or you wouldnt be here."

== Cast ==
*Don Megowan as Capt. Kenneth Cragis
*Erica Elliott as Maxine Megan
*Don Doolittle as Dr. Raven
*George Milan as Acto, a clicker
*Dudley Manlove as Lagan, a clicker
*Frances McCann as Esme Cragis Milos
*David Cross as Pax, a clicker
*Malcolm Smith (actor) as Court
*Richard Vath as Mark, a clicker
*Reid Hammond as Hart, Chairman of Surveillance Committee
*Pat Bradley (actor) as Dr. Moffitt
*William Hunter (actor) as Ward, Surveillance Committee Member
*Gil Frye as Orus, a clicker
*Alton Tabor as Kellys Duplicate
*Paul Sheriff as Policeman

==Production==
The Creation of the Humanoids is normally dated to 1962, the year of its general release, but one screening in 1961 is documented by an advertising flyer and the film itself displays a 1960 copyright date (MCMLX in Roman numerals), indicating that it was a complete film before the end of that year. Short items in contemporary trade publications indicate that it was being filmed in the summer of 1960 under a working title variously reported as This Time Around or This Time Tomorrow.   The Academy of Motion Picture Arts and Sciences lists August 1960 as the completion date.  Producer-director, former child star and Hollywood area native Wesley Barrys Genie Productions was located in Hollywood,  and the cast and crew credits are populated by Hollywood personnel, but no information about the actual filming location and other specific details has yet come to light.
 Confederate Army caps. Yet the producers opted for the added expense of filming in color at a time when black-and-white was still being used for many major-studio productions and was readily accepted by audiences, and they obtained the services of two top-tier behind-the-camera talents, albeit in the twilights of their careers. 

Cinematographer Hal Mohr had a very extensive Hollywood career and two Academy Awards to his credit. Mohr used lighting and camera angles to make the best of the sets and add some visual interest to the long, actionless talking-head scenes that make up nearly all of the film. He sometimes used classic Hollywood "glamor lighting" techniques when photographing the normal-looking "human" characters, giving some scenes a degree of visual polish seldom seen in a low-budget exploitation film. 
 Jack Pierce scleral contact optometrist who pioneered the use of contact lenses to change actors eye color and is credited in the film for "special eye effects". At that time, scleral lenses were made of a hard plastic, wearing them was far from comfortable to unaccustomed users, and they had to be removed frequently to allow the eyes to "breathe". Pierce had used similar silvery lenses in 1957 for brief close-ups in The Brain from Planet Arous. Most of the considerable time and effort it took to apply the rest of the humanoid makeup was spent on hiding the actors hair, which it would have been unthinkable to expect them to actually shave off for a few days work in a low-budget film. Latex rubber "bald wigs" were glued on, eyebrows were stuck down flat, then putty was carefully applied to cover rough textures and blend in tell-tale edges. Finally, the actors heads were painted all over with blue-gray greasepaint and they were given rubber gloves of the same color. 
 score consists of electronically generated sounds and wordless female vocalizing that suggests the Theremin music often used in science fiction films of the 1950s (e.g., The Day the Earth Stood Still and It Came from Outer Space). The credit appearing in the film is "Electronic Harmonics by I.F.M." The Internet Movie Database lists producer Edward J. Kay as the composer, though this information is nowhere verified.

==Release==
A general theatrical release through Emerson Film Enterprises was launched with an official opening in Los Angeles on 3 July 1962. An advertising campaign was begun, including the broadcast of short TV spots. Unlike most film posters of the time, which were printed by a four-color process that allowed a full range of eye-catching colors to be used, the posters for this film were two-color printings limited to black, white, red, grays, pinks and browns. The use of a two-color poster for a small independent black-and-white film was not uncommon, but it was unusual and unhelpful when one of the selling points of the film being advertised was the fact that it was in color. 

Makeup artist Jack Pierce participated in the 1962 publicity campaign by giving interviews and making up a Los Angeles TV movie host as a humanoid, complete with special contact lenses, during a live broadcast. Progress in the application of the makeup was televised during commercial breaks in the unrelated film being shown. The live segments were saved on videotape and rebroadcast several times in the following days. 
 Beta and VHS videocassettes by Monterey Home Video in 1985 and was later available from Something Weird Video. The first licensed DVD release came in 2006 on a double-feature disc from Dark Sky Films.
 aspect ratios most commonly used for 35&nbsp;mm projection in the US in 1962. The earlier videocassette releases are not Pan and scan|pan-and-scan versions of a widescreen image, but simply unmatted full-frame 1.33:1, revealing areas at the top and bottom of the image not normally seen in a theater.

==Reception== Invasion of the Animal People at the Fox shape to a sock $10,000",  "sock" being a favorable term in that publications show-business jargon and $10,000 the box office receipts at the Fox theater.

After its 1964 television debut, references to more than its title began to appear. In 1965, Susan Sontag briefly mentioned some story details in her essay on science fiction films entitled The Imagination of Disaster. 

==Later critical opinion==
"Slow, stagy cheapie" – Leonard Maltin. Leonard Maltins 2008 Movie Guide, Signet/New American Library, New York, 2007. 

"This interesting film...is badly let down by Simms over-talkative script." – The Aurum Film Encyclopedia – Science Fiction. Phil Hardy (ed.), The Aurum Film Encyclopedia – Science Fiction, Aurum Press, London, 1991. 

"Incredible little film" – Michael Weldon, The Psychotronic Encyclopedia of Film. Michael Weldon: The Psychotronic Encyclopedia of Film, Plexus, London 1989. 

"...a highly underrated gem of considerable worth   a perfect illustration of how science-fiction should work as a literature of ideas rather than of special effects." – Richard Scheib, Moria – The Science Fiction, Horror and Fantasy Film Review. 

"Yes, it is ham-handedly, painfully un-subtle, but making a film with this message in the early 1960s, with the storms of the civil rights movement still raging, required considerable courage on the part of the filmmakers." – Erick Harper, DVD Verdict. 

"Undeniably sophisticated as science fiction, The Creation of the Humanoids is one weird movie." – Glenn Erickson, DVD Savant. 

==In popular culture==
The Creation of the Humanoids is often said to be "Andy Warhols favorite film".    The original source for this claim appears to be a 1964 art review of new Warhol paintings that begins with a short description of the film and states that the protagonists climactic discovery is "the happy ending of what Andy Warhol calls the best movie he has ever seen." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 