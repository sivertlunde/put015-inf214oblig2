Fast Food (film)
{{Infobox Film
| name           = Fast Food
| image          = Fast_food_xlg film.jpg
| image_size     = 
| caption        = 
| director       = Michael A. Simpson
| producer       = Jerry Silva Michael A. Simpson Stan Wakefield Phil Walden
| writer         = Clark Brandon Lanny Horn Scott B. Sowers Jim Basile
| starring       = Jim Varney Traci Lords Michael J. Pollard Blake Clark Pamela Springsteen
| music          = Iris Gillon
| cinematography = Bill Mills
| editing        = John David Allen
| studio         = Varney Productions
| distributor    = Double Helix Films
| released       = April 28, 1989
| runtime        = 90 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Fast Food is a 1989 American comedy film starring Jim Varney, Traci Lords, Michael J. Pollard, Blake Clark and Pamela Springsteen.   

==Plot== secret sauce that makes people go crazy.
 rated PG-13.

==Cast==
*Jim Varney ... Bob Bundy
*Traci Lords ... Dixie Love
*Michael J. Pollard ... Bud
*Blake Clark ... E.G. McCormick
*Pamela Springsteen ... Mary Beth Bensen
*Randi Layne ...  Alexandra Lowell  Kevin McCarthy ...  Judge Reinholte 
*J. Don Ferguson ...  Dean Witler 
*Terry Hobbs ...  Donald Frump III 
*Amy Bryson ...  Sheryl 
*Kathleen Webster ...  Wendy 
*Julie Ridley ...  Dr. Duran 
*Paige Conner ...  Tracy 
*Tom Key ...  Jack Skinner 

==References==
 

==External links==
*  

 
 
 
 
 


 