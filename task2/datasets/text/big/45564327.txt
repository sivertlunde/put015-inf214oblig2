Men Without Honour
{{Infobox film
| name =  Men Without Honour
| image =
| image_size =
| caption =
| director = Widgey R. Newman Bernard Smith   Widgey R. Newman George A. Alexander George
| narrator = Ian Fleming Howard Douglas   Charles Paton   Grace Arnold
| music =  John Miller
| editing = Violet Burdon
| studio = Smith & Newman Productions 
| distributor = Equity British Films
| released = March 1939
| runtime = 59 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Ian Fleming, Howard Douglas B film designed to go on the bottom-half of a double bill. 

==Synopsis==
A disgraced lawyer ends up working for a gang of share-pushers. He becomes outraged when he discovers that they are scamming his sons prospective father-in-law and eventually unmasks the villains with the help of the police.

==Cast== Ian Fleming as Frank Hardy   Howard Douglas as Fane  
* W.T. Hodge as Vigor  
* Charles Paton as Rev. Fanshawe  
* Grace Arnold as Mrs. Hardy  
* Alastair Hunter as John Hardy  
* Edith Clinton as Enid Fanshawe  Charles Courtney as Field  Tony Melrose as Commissioner 
* George Dewhurst as Inspector Smith 
* Rex Alderman as Barclay

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 