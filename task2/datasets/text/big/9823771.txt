Death Wish V: The Face of Death
 
{{Infobox film
| name           = Death Wish V: The Face of Death 
| image          = DeathWish5.jpg 
| image_size     = 
| caption        = Theatrical release poster
| director       = Allan A. Goldstein
| producer       = Damian Lee
| screenplay     = Allan A. Goldstein
| story          = Michael Colleary Allan A. Goldstein
| based on       = characters created by Brian Garfield
| starring       = Charles Bronson Lesley-Anne Down Michael Parks Robert Joy Saul Rubinek
| music          = Terry Plumeri
| cinematography = Curtis Petersen
| editing        = Patrick Rand Lionsgate (VHS/DVD Release)   MGM Home Entertainment/Sony Pictures Entertainment (Australian DVD Distributors)   Europa Carat (Brazil VHS)1994  
| released       =   (USA)
| runtime        = 95 mins.
| country        = United States
| language       = English
| budget         = $5,000,000 (estimated)
| gross          = $1,702,394 (USA)
}}
 action crime Death Wish series. It features Charles Bronson, who reprises his role as Paul Kersey in his final theatrical starring role. In the film, Kersey battles mobsters as he tries to protect his girlfriend (played by Lesley-Anne Down) from her ex-husband, mob boss Tommy OShea (Michael Parks).
 Cannon Films bankruptcy. It was shot in Toronto and helmed by Canadian director Allan Goldstein. Steve Carver was originally going to direct until the producers planned to cut the budget.

==Plot==
 
Paul Kersey (Charles Bronson) returns to New York City under the name Paul Stewart. He is invited by girlfriend Olivia Regent (Lesley-Anne Down) to a fashion show. Backstage, mobster Tommy O’Shea (Michael Parks) and his goons muscle in on the action. Tommy then threatens Olivia, who is his ex-wife and mother to their daughter Chelsea (Erica Lancaster).

Olivia later informs Paul of her ex-husbands behaviour after he finds bruises on her hand. Paul proceeds to confront him, but Tommy points a gun at Pauls face. The confrontation ends with the arrival of Chelsea.

NYPD officers Tony Hoyle (Saul Rubinek) and Hector Vasquez (Miguel Sandoval) visit Stewarts home. Paul Stewart is revealed to be Paul Kersey in the Witness Protection Programme. He informs them about Tommy O’Shea. Hoyle says they have been trying to nab Tommy for years, and he wants Olivia to testify.

That night at a restaurant, Paul proposes to Olivia, who accepts. Olivia excuses herself to go to the ladies room and is attacked by Tommy’s associate, Freddie "Flakes" Garrity (Robert Joy), who in an attempt to prevent her testimony bashes her head on a mirror, causing permanent disfigurement to her face.

Freddie escapes, although Paul gets a look at him. At the hospital, where Paul is told Olivia will need reconstructive surgery, he talks with Lt. Mickey King (Kenneth Welsh), who has been working on the O’Shea case for 16 years. Paul is angry but is warned by King not to pick up his old habits and to let the police handle it, but Paul tells him that the law works and that it sometimes doesnt.

Tommy escalates the violence, killing several people including female police officer Janice Omori (Lisa Inoue). Paul and Olivia are attacked by Freddie and his friends, who end up shooting Olivia in the back and killing her as the couple tries to escape. She becomes the fourth of Pauls five love interests in the series to be murdered by gang members. Paul jumps from the roof of his apartment, where he lands in a dumpster, and is retrieved by the police.

Tommy is "cleared" of involvement in his ex-wife’s death and seeks custody of their daughter. This sets the stage for a fight between Paul and Tommy, one that leaves Paul unconscious when he assaults Tommy. He decides to return to his vigilante ways and is later assisted by Hoyle, who learns his department has been corrupted by Tommy. Paul poisons one of Tommys men, Chuck, with a cannoli. He then kills Freddie by blowing him up with a remote-controlled soccer ball. Tommy finds out from an informant that Paul is the vigilante and will be going after him for killing Olivia. The informant, Hector Vasquez of the NYPD, tries to kill Kersey himself, but Paul gets the upper hand and kills him. Fellow officer Hoyle arrives and finds out Tommy wants both him and Kersey dead. Hoyle comments on Pauls style - "No judge. No jury. No appeals. No deals." - and tells Kersey he must never see him again, and Paul agrees.

Tommy hires three thugs, Frankie (Scott Spidel), Mickey (Tim MacMenamin), and Angel (Sandro Limotta), to take care of Paul, using Chelsea as bait. Paul rescues her. Lt. King is wounded by Tommy. Paul kills Sal, one of Tommys men, by shooting him into a large paper shredder, cutting him into pieces. Paul picks up an empty beer bottle, smashes it, and cuts  Tommys face in retaliation for what he did to Olivia. Armed with a shotgun, Paul corners Tommy.

Tommy tries to persuade Paul to spare him by saying "Whatever you want, whatever you need, its yours." Paul quickly responds with "I dont need anything, but you need a bath." Paul then finally manages to kill Tommy by knocking him into the pool of acid, where he disintegrates until theres nothing left of him. King thanks Paul for saving his life. He rejoins Chelsea, calling out to the injured King, "Hey Lieutenant, if you need any help, give me a call" while Paul walks toward the lightened hallway.

==Cast==
* Charles Bronson as Paul Kersey
* Lesley-Anne Down as Olivia Regent
* Michael Parks as Tommy OShea
* Robert Joy as Freddie "Flakes" Garrity
* Saul Rubinek as Tony Hoyle
* Kenneth Welsh as King
* Erica Lancaster as Chelsea
* Chuck Shamata as Sal Paconi
* Kevin Lund as Chuck Paconi
* Melissa Illes as Runway Model
* Jefferson Mappin as Albert

==Production== Cannon Films. Chapter 11 bankruptcy and its financial records came under investigation by the U.S. Securities and Exchange Commission. Co-owners Menahem Golan and Yoram Globus also had a personal falling out during the collapse of their company. Talbot (2006), p. 75-103  Golan soon launched his own company, 21st Century Film Corporation. Talbot (2006), p. 75-103  The films of the new company tended to have small budgets and performed poorly at the box office. Meanwhile the Death Wish films continued to enjoy popularity in the video and television market. Golan came up with the idea of a fifth Death Wish  film to serve as a much-needed hit for the company. Talbot (2006), p. 103-119 

Financing to start the film production was secured through a loan from the Lewis Horwitz Organization. Golan still owned an unused screenplay for a Death Wish film, submitted in the late 1980s by J. Lee Thompson and Gail Morgan Hickman. Talbot (2006), p. 75-103   He decided against using it, since it would be too costly to produce. Instead, he hired Michael Colleary to write a new script. 

Golan initially reserved directorial duties for himself. His preoccupation with directing   (1987) may have led Golan to count him out.  Golan then hired Steve Carver for the job, an experienced director in the action film genre. Carver recalled discussing with Bronson over the depiction of Paul Kersey. Bronson wanted the character to become more sympathetic and less violent. Carver and screenwriter Stephen Peters started co-operating in revising the script. 
 dual citizenship as a Canadian and American. Carver believes that it was Goldsteins Canadian ties which influenced the decision. Goldstein himself was surprised, since he specialized in drama films. Death Wish V was his first action film.  He tried familiarizing himself with the film series by watching the previous entries for the first time.  He soon started revising the script. He attempted to insert humor and black comedy elements. 

The film was shot in Toronto. For tax purposes, several of the roles had to be filled with Canadian actors. Among them were Robert Joy, Saul Rubinek, Kenneth Welsh, and teenager Erica Lancaster.  The previous films of the series were mostly shot on location, but the fifth film was mainly shot in a studio. All the scenes involving the dress factory were shot in a studio. 
 Crime and Punishment (2002) in Russia. 

Ami Artzi is credited as the executive producer of the film. He was the president of the 21st Century Film Corporation, working directly under Golan.  Damian Lee functioned as the line producer of the film. 

The film was the first screen credit for screenwriter   (2001). 

The film was the only one in its series which did not include a rape scene. Flashes of bare breasts in a non-violent context were its only instances of nudity. 

The film was partially financed through an advanced payment by   (1987). 

Golan planned to continue the film series without Bronson and announced the upcoming film Death Wish 6: The New Vigilante. But 21st Century Film Corporation went bankrupt and the film project was cancelled.  Bronson subsequently appeared in the television film A Family of Cops (1995) and its two sequels. 

==Reception==
Death Wish V received mainly negative reviews from critics and was a box office failure. Many fans of the previous four Death Wish films considered this sequel the poorest of the series.  
The film currently holds a two star rating (4.2/10) on IMDb.

==Sources==
*  
*  

== References ==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 