Reasonable Doubt (2014 film)
{{Infobox film
| name           = Reasonable Doubt
| image          = File:Reasonable Doubt Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Peter Howitt (as Peter P. Croudins)
| producer       = Frank Buchs Fredrik Malmberg Silvio Muraglia David Valleau Daniel Wagner
| writer         = Peter A. Dowling
| starring       = Dominic Cooper Samuel L. Jackson
| music          = James Jandrisch
| cinematography = Brian Pearson
| editing        = Richard Schwadel Eagle Vision Paradox Entertainment South Creek Pictures Voltage Pictures Grindstone Entertainment Group Lionsgate Films
| released       = January 17, 2014
| runtime        = 91 minutes
| country        = Canada
| language       = English
| budget         = $8 million
| gross          = 
}}

Reasonable Doubt (also known as The Good Samaritan) is a 2014 Canadian crime thriller film directed by Peter Howitt and written by Peter A. Dowling. The film stars Samuel L. Jackson, Dominic Cooper, Erin Karpluk, Gloria Reuben and Ryan Robbins.

==Plot==
When up-and-coming District Attorney Mitch Brockden (Dominic Cooper) commits a fatal hit-and-run, he feels compelled to manipulate the case to acquit the accused criminal (Samuel L. Jackson) who was found with the body and blamed for the crime. Following the trial, Mitchs worst fears come true when he realizes that his actions freed a guilty man, and he soon finds himself on the hunt for the killer before more victims pile up.

==Cast==
* Dominic Cooper as Mitch Brockden
* Samuel L. Jackson    as Clinton Davis
* Erin Karpluk as Rachel Brockden
* Gloria Reuben as Detective Blake Kanon 
* Ryan Robbins as Jimmy Logan Dylan Taylor as A.D.A Stuart Wilson
* Philippe Brenninkmeyer as DA Jones
* John B. Lowe as Judge Mckenna
* Kelly Wolfman as Dr. Brown

==Production==
The production of the film began on November 19, 2012 and shot in Winnipeg, Canada.  It was also shot in Chicago and shot over twenty seven days. 

==Release==
In May 2013, Lionsgate Films picked up the rights of distribution in US and Voltage Pictures will distribute the film internationally. 

==Reception==
Reasonable Doubt received negative reviews. The film holds a 13% rating on Rotten Tomatoes, with an average rating of 3.2/10, based on eight reviews. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 