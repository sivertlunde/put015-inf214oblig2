Main, Meri Patni Aur Woh
{{Infobox film
| name           = Main, Meri Patni Aur Woh
| image          = Main,_Meri_Patni_Aur_Woh.jpg
| caption        = DVD cover
| director       = Chandan Arora
| producer       = Ronnie Screwvala
| starring       = Rajpal Yadav Rituparna Sengupta Varun Badola Kay Kay Menon
| music          = 
| cinematography = 
| editing        = 
| distributor    =
| released       =  
| runtime        = 129 mins
| language       = Hindi
| budget         = 
}}
 Hindi romantic comedy 
film, directed by Chandan Arora, starring Rajpal Yadav, Rituparna Sengupta and Kay Kay Menon as the leading actors. The film released on October 7, 2005.

==Plot==
The movie is about a short yet qualified middle aged man named Mithilesh played by Rajpal Yadav and his insecurities about his height and personality when he gets married to Veena played by Rituparna Sengupta.  As his married life progresses with his loving and devoted wife, his insecurities grow. He becomes jealous about his friend Saleem, played by Varun Badola, Veenas childhood friend Akash, played by Kay Kay Menon and others who he claims have an infatuation with his wife.  Slowly, he suspects a change in his wifes behavior and starts believing she wants to divorce him. Being a loving husband he confesses his fear to Veena and frees her from all wifely duties. But the hidden truth, revealed at the very end, ultimately brings the couple closer.

==Cast==
Rajpal Yadav... Mithilesh Chhotey Babu Shukla 
Rituparna Sengupta... Veena Tiwari / Veena M. Shukla 
Vinod Nagpal... Advocate Kishorilal Mishra 
Varun Badola... Saleem 
Kay Kay Menon... Akash 
Naseeruddin Shah... Voice Over
Abdul basir... Abdul basir

==Critical Reception==
The movie got 8/10 on Rotten Tomatoes   and 7.3/10 on IMDB.  It garnered positive reviews from both critics  and audiences  for the non-clichéd plot, subtle cinematography and Rajpals realistic portrayal of an insecure and self-conscious person.

==References==
 

==External links==
*  
*  

 
 
 
 
 


 
 