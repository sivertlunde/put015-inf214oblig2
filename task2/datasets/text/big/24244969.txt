Night Beat (1947 film)
 
 
{{Infobox Film
| name           = Night Beat
| image          = "Night_Beat"_(1947).jpg
| image_size     = 
| caption        = Song sheet tie-in with Christine Norden
| director       = Harold Huth
| producer       = Harold Huth Guy Morgan   T. J. Morrison
| narrator       =  Ronald Howard  Christine Norden
| music           = Benjamin Frankel
| cinematography  = Václav Vích
| editing         =  Grace Garland
| studio          = Harold Huth Productions British Lion Films
| distributor    = British Lion Film Corporation (UK)
| released       = 15 January 1948 (UK)
| runtime        = 91 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross = £153,438 (UK) 
}}

Night Beat is a 1947 British crime drama film directed by   when trying to adjust to civilian life."   

==Cast==
*Anne Crawford: Julie Kendall
*Maxwell Reed: Felix Fenton Ronald Howard: Andy Kendall
*Christine Norden: Jackie
*Hector Ross: Don Brady Fed Groves: PC Kendall
*Sid James: Nixon (as Sidney James)
*Nicholas Stuart: Rocky
*Frederick Leister: Magistrate
*Michael Medwin: Rocky

==Critical reception==
*The Radio Times wrote, "a relishably bad British crime drama set in an unreal Soho underworld of spivs and nightclubs. Its a compendium of clichés...Benjamin Frankels score is better than the film deserves."  
*Allmovie wrote, "though its starts out strong, Night Beat metamorphoses into standard melodramatics towards the end."  
*Britmovie wrote, "fast-paced British crime melodrama...The two lead actors are particularly wooden and it’s left to the supporting cast to add some lowlife colour; particularly Maxwell Reed’s smug villain, Christine Norden as the vampish blonde, Sid James piano playing snout and a brief appearance by Michael Medwin as an indignant petty crook."  

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 
 