Blue (2009 film)
 
 

{{Infobox film
| name                  = Blue
| image                 = Blue_movie_poster.jpg
| caption               = Theatrical release poster
| director              = Anthony DSouza
| producer              = Dhillin Mehta
| writer                = Mayur Puri (dialogue)
| story                 = Anthony DSouza
| screenplay            = Anthony DSouza
| dialogue              = Mayur Puri
| starring              = Akshay Kumar Sanjay Dutt  Lara Dutta Zayed Khan Katrina Kaif
| music                 = A. R. Rahman
| cinematography        = Laxman Utekar
| editing               = Shyam Salgaonkar
| distributor           = Shree Ashtavinayak Cine Vision Ltd
| released              =  
| runtime               = 119 minutes
| country               = India Hindi
| budget                =   http://www.bollywoodmantra.com/news/top-5-big-budget-films-of-bollywood/7119 
| gross                 =      (domestic gross) 
}}
 action thriller thriller film Into the Blue (2005), Blue failed to recover its high budget from the box office. 

==Plot==
The story follows three men on an underwater treasure hunt. In the Bahamas, Aarav (Akshay Kumar) runs a successful fishing company under the title of Blue. He works with Sagar (Sanjay Dutt) who works as a diver for Aaravs fishing company. Sagar is in a relationship with Mona (Lara Dutta) and wants to propose to her soon. Sagar is under a financial crisis, and to help him, Aarav advises Sagar to retrieve the lost treasure of a sunken ship called Lady in Blue. However, Sagar refuses to go to the ship due to an unknown reason.

Meanwhile, Sagars younger brother Sam (Zayed Khan) is in Bangkok. Sam is a bike rider, and meets Nikki (Katrina Kaif), a female biker who works for an underworld gangster named Gulshan (Rahul Dev). Sam is challenged by Gulshan to a bike race, and after Sam wins, he asks Nikki out for dinner. Before leaving, Gulshan asks Sam to work for him, but Sam refuses unless theres a large sum of money involved.

During Sams date with Nikki, Nikki explains to Sam that the job Gulshan proposed would pay with $50,000. Gulshan hires Sam to work for him, but Sam is set up by Gulshan in failing to deliver a package costing Gulshan the $50 million. Sam must either pay his money back or die. Sam does not have the money so he explains to Nikki, whom he now loves, that he is going on holiday to the Bahamas to visit his brother Sagar and doesnt think Gulshan will find him there. Sam arrives at Aaravs company and Sagar welcomes him happily. In the Bahamas, Sam receives a call from Gulshan on Nikkis phone, explaining that he has killed Nikki, and knows where Sam is.

Soon enough, Gulshan and his crew arrive in the Bahamas and surround Sam. Aarav enters and beats everyone up, saving Sams life. For now. Sam tells Aarav what he did wrong, and Aarav decides he needs to tell Sagar about the situation. When Sagar learns how much trouble Sam is in, Sam tries to convince Sagar to retrieve the treasure from Lady in Blue. Sagar refuses. The next day, Gulshan bombs Sagars home, and kidnaps Mona, with a ransom of $50 million or Sam for her return. When Sagar tells Sam and Aarav, Sam decides that they need to go and rescue Mona as they already killed Nikki and will kill Mona too. But Aarav speaks up and says they dont know how far Gulshan is connected and they may all die, meaning the only option is to find the treasure, pay off Gulshan and use the rest of the money to live comfortably. Sagar is still hesitant and it is revealed that Sagar has a haunted and shocking past with Lady in Blue. Years ago, Sagar had to abandon his father at the ship to save his own life. However, after Aarav convinces Sagar that the treasure will save the lives of both Mona and Sam, Sagar finally agrees. The three of them bring the treasure up and find Gulshan sitting on the top of his ship that him and his gang have steered up next to the ship belonging to Sam, Sagar, and Aarav. He tells Sam and Sagar that Aarav hired Gulshan to do all of this. Soon there is a fight between Sagar and Aarav, and Aarav jumps into the ocean. Mona, Sam, and Sagar are forced to leave him there, and Sagar agrees never to come back to the Lady in Blue, after losing two kin there. His father and friend, Aarav.

Three months later Sagar gets a call from Aarav. Talking to them, Aarav reveals that he survived his leap into the ocean, and tells them that his grandfather was the traitorous captain of the ship, Lady in Blue. Aarav never admitted to Sagar who his grandfather was, because he didnt want to dishonor his family. Aarav explains that finding the treasure of the Lady in Blue would help bring honor back to his family name, and he befriended Sagar, knowing he was the only one who could take him to the treasure. Throughout the phone call, a womans arm is seen around Aarav, and later, the woman asks to speak to Sam. It is revealed that the woman is Nikki, and she is Aaravs wife. She asks Sam to forgive her for deceiving him, but it isnt revealed if Sam said yes or no. However, while Sam answers there is a smile on Nikkis face. The film ends with Aarav and Nikki cuddling on one of Aaravs boats, and the end credits begin.

==Cast==
* Zayed Khan as Sameer: Sagars brother and a biker racer
* Sanjay Dutt as Sagar: Aaravs closest friend and employee.
* Lara Dutta as Mona: a marine enthusiast, Sagars love.
* Akshay Kumar as Aarav: Owner of Blue Shipping and Fisheries
* Katrina Kaif in a special appearance as Nikki: the biker chick and Aaravs wife.
* Rahul Dev as Gulshan: Gangster from Bangkok
* Kabir Bedi as Jagat Malhotra: Captain of the sunken ship Lady in Blue.
* Kylie Minogue – Guest Appearance in the song "Chiggy Wiggy".

== Music ==
{{Infobox album
 |  
 | Name = Blue
 | Type = Soundtrack
 | Artist = A. R. Rahman
 | Cover =
 | Released =  8 September 2009
 | Recorded = Panchathan Record Inn A.M. Studios Feature film soundtrack
 | Length = 34:22
 | Label = T-Series
 | Producer = A.R. Rahman
 | Last album = Delhi-6 (Soundtrack)|Delhi-6 (2009)
 | This album = Blue (2009) Couples Retreat (2009)
}}

The music of the film was composed by A. R. Rahman with lyrics provided by Abbas Tyrewala, Mayur Puri, Ajit Arora, Raqeeb Alam and Sukhwinder Singh. The music was promoted and digitally distributed by Hungama Digital Media Entertainment Pvt. Ltd.
 Rashid Ali. The Yeri voice for the song was by Kavita Belliga. The theme song for Blue was sung by Blaaze, with backing vocals by Raqeeb Alam, Sonu Kakkar, Jaspreet Jasz, Neha Kakkar and Dilshad Khan. "Rehnuma" was sung by Shreya Ghoshal and Sonu Nigam.  "Yaar Mila Tha" was sung by Udit Narayan and Madhushree with backing vocals by Ujjayinee Roy, Shi Millhouse and Raven Millhouse. This track was not included in the film.

The official traclisting: 
{| class="wikitable"
|-
! Track# !! Song !! Singer(s) !! Lyrics !! Length
|-
| 1
| "Chiggy Wiggy"
| Kylie Minogue, Sonu Nigam, Suzanne DMello
| Abbas Tyrewala
| 5:12
|-
| 2
| "Aaj Dil Gustakh Hai"
| Sukhwinder Singh, Shreya Ghoshal
| Mayur Puri
| 5:29
|-
| 3
| "Fiqrana"
| Vijay Prakash, Shreya Ghoshal
| Ajit Arora
| 5:25
|-
| 4
| "Bhoola Tujhe" Rashid Ali
| Abbas Tyrewala
| 5:26
|-
| 5
| "Blue Theme"
| Blaaze, Raqeeb Alam, Sonu Kakkar, Jaspreet Jasz, Neha Kakkar, Dilshad Khan
| Raqeeb Alam, Punjabi lyrics by Sukhwinder Singh
| 3:55
|-
| 6
| "Rehnuma"
| Sonu Nigam, Shreya Ghoshal
| Abbas Tyrewala
| 4:24
|-
| 7
| "Yaar Mila Tha"
| Udit Narayan, Madhushree
| Abbas Tyrewala
| 4:29
|}

==Album reception==
{{Album ratings rev1 = Planet Bollywood rev1score =    rev2 = Bollywood Hungama rev2score =   
}} 
The soundtrack of the movie has received positive reviews. bollywoodhungama reviewed the music saying "Blue is a good album and has all in it to make a good impression at the music stands. In a way, the album comes at just the right time when there is quite some variety in store this Diwali.  Blue practically mixes up genres.".".   Planetbollywood.com said "Blue does have some fun compositions such as the Blue Theme, Chiggy Wiggy and Yaar Milaa Thaa. And, composition-wise, the rest of the songs are also enjoyable to listen to…however, lyrics, for the most part, are a huge disappointment, and there isn’t consistency in terms of quality throughout the OST. While some songs grab your attention immediately, songs like Aaj Dil Gustaakh Hai will take a very long time to grow on you, and even then, it feels as if you’re forcing yourself to feel excited about some of the songs."  

==Reception==

===Critical reception===
The film received mixed to positive reviews from critics. Anupama Chopra of NDTV called the movie an smartly crafted underwater thriller, criticizing and at the same time praising the performance and the cast and the dialogues.  Taran Adarsh of Bollywood Hungama gave the movie a positive review, praising the star cast and action scenes and gave the film 4 stars out of 5. 

===Box office===
Blue had an excellent opening at the box office, and collected very well in its opening weekend. The film gradually decreased with the occupancy almost non-existent after the first week. Its domestic nett was   394&nbsp;million and Box Office India declared the film as a Below Average.

==Sequel== John Abraham, Neha Oberoi, and Sonal Chauhan.   A. R. Rahman has been approached to compose the music.

==See also==
* List of most expensive non-English-language films

==References==
 

== External links ==
*  

 
 
 
 
 
 
 