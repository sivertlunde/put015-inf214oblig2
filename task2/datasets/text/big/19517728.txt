Lisa Picard Is Famous
{{Infobox film
| name           = Lisa Picard is Famous
| image          = LisaPicardisFamous.jpg
| image size     =
| caption        = Promotional poster
| director       = Griffin Dunne
| producer       = Dolly Hall Mira Sorvino
| writer         = Nat DeWolf Laura Kirk
| narrator       =
| starring       = Laura Kirk
| music          =
| cinematography = William Rexer
| editing        = Nancy Baker
| distributor    =
| released       = May, 2000
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         =
| preceded by    =
| followed by    =
}} 2000 comedy-drama film directed by Griffin Dunne and written by Nat DeWolf & Laura Kirk.  The film stars Kirk, DeWolf, Dunne, Daniel London, and a large number of famous actors in cameos as themselves.

The films story is about a documentary maker who has focused on Lisa Picard as she is on the verge of stardom.  It was screened in the Un Certain Regard section at the 2000 Cannes Film Festival.   

==Cast==
{{columns-list|2|
* Laura Kirk as Lisa Picard
* Nat DeWolf as Tate Kelly
* Griffin Dunne as Andrew
* Daniel London as Boyfriend
*Cameos as themselves:
** Sandra Bullock
** Carrie Fisher
** Melissa Gilbert
** Buck Henry
** Spike Lee
** Penelope Ann Miller
** Charlie Sheen
** Fisher Stevens
** Mira Sorvino
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 