Stella (2008 film)
{{Infobox Film
| name           = Stella
| image          = 
| image_size     = 
| caption        = 
| director       = Sylvie Verheyde WDR
| writer         = Sylvie Verheyde
| narrator       = 
| starring       = Léora Barbara Mélissa Rodrigues Benjamin Biolay Karole Rocher Guillaume Depardieu
| music          = NousDeux the band
| cinematography = 
| editing        = Christel Dewynter
| distributor    =
| studio         = 
| released       =  
| runtime        = 103 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}} 2008 French film directed by Sylvie Verheyde.

== Cast ==
* Léora Barbara - Stella
* Mélissa Rodrigues - Gladys
* Laëtitia Guerard - Geneviève
* Benjamin Biolay - Stellas father
* Karole Rocher - Stellas mother
* Thierry Neuvic - Yvon
* Guillaume Depardieu - Alain-Bernard

==Festivals and awards==
*Official selection at the 2009 Seattle International Film Festival
*The Lina Mangiacapre Award, and also a Christopher D. Smithers Foundation Special Award for education about the disease of alcoholism, at the 65th Venice International Film Festival

==References==
 

==External links==
* 

 
 
 
 


 
 