La Señal (film)
 
{{Infobox film
| name           = La Señal
| image          = La Señal.jpg
| caption        = From left to right: Ricardo Darín, Julieta Díaz and Diego Peretti
| director       = Ricardo Darín Martín Hodara
| producer       = 
| writer         = Eduardo Mignogna
| starring       = Ricardo Darín Diego Peretti Julieta Díaz Andrea Pietra
| music          = César Lerner
| cinematography = Marcelo Camorino
| editing        = Alejandro Carrillo Penovi
| distributor    = 
| released       =  
| runtime        = 95 minutes 
| country        = Argentina
| language       = Spanish
| budget         = 
|}} Argentine film written by Eduardo Mignogna. Mignogna was going to direct the film  as well. However, due to his death one year before the start of production, his friend and frequent collaborator, Ricardo Darín, took up the task. He was aided by Martín Hodara, who had his directorial debut. Darín also starred in the lead role, along with Diego Peretti, Julieta Díaz and Andrea Pietra.

The film is dedicated to Eduardo Mignogna.

== Synopsis ==
The film starts in medias res, with Corvalán hiding in a safehouse and preparing himself for a gunfight. The film then takes a step back to the start, in the form of a Flashback (narrative)|flashback.
The year is 1952: Eva Perón has retired from public life due to poor health, and her final days are being broadcast on the radio. Corvalán, alias "El Pibe" (The Kid, Ricardo Darín) and Santana (Diego Peretti) are two amateur private detectives living off oddjobs and gambling. Corvalán is also in a decaying relationship with Perla (Andrea Pietra). After one unlucky day at the tracks, Corvalán meets a mysterious woman, Gloria (Julieta Díaz), in a café. She passes him her telephone number and leaves. Corvalán tries to contact her, but unsuccessfully so until he is reached by Gloria herself, and presented with an assignment: trail a man, Nicolás Pertureta, all day and report his movements. Although Pertureta proves difficult to follow, Corvalán manages to garner information about Pertureta contacting a number of shady people.

Corvalán recruits his cautious partner, Santana, into the assignment - while keeping his regular tasks in hand. Perla is caught cheating on Corvalán, Pertureta is found strangled with barbed wire, and it is revealed that Gloria is the wife of a major mafia boss, "El Noruego" (The Norwegian), who is being stalked by Dino Capuano, the last surviving member of a family he massacred years ago in Sicily. Because El Noruego suspects a snitch, and Gloria fears for her safety, she confesses to Corvalán that she hired him to give her, and indirectly her husband, proof that the snitch is Pertureta.

Santana wants no business with the feud between El Noruego and Dino Capuano, but Corvalán, moved by Glorias story, decides to help her, and together they go into hiding at a safehouse. It is here that the movie catches up with the start, and Corvalán engages in a gunfight with El Noruegos henchmen. He and Gloria escape after killing one of them, and as Corvalán runs out of bullets, Santana arrives and rescues them.

Upon getting word that El Noruego and his gang have been gunned down by Capuanos men, Gloria manages to convince Corvalán to disappear with her. To do so, Corvalán must first return to El Noruegos place and open the safe that holds savings worth a lifetime. In the movies climax, Corvalán manages to open the safe and reads the "Remington" seal in the safe-box and remembers that days ago his father told him he had a safe-box with a five-digit combination.Corvalán is suddenly interrupted by Glorias chauffeur, who points a gun at him and demands the money. Just as he is about to kill him, he is shot in the back by Gloria as he, in turn, shoots Corvalán. Gloria leaves Corvalán to die, and walks away with the money.

In the last scene, Santana painfully identifies Corvaláns body at the morgue. He mutters "So long, my friend" (in English) and leaves. In the radio, a speaker announces the death of Eva Perón and the national mourning.

==External links==
*  

 
 
 
 
 
 
 
 