National Lampoon's Class Reunion
 

{{Infobox film
| name = National Lampoons Class Reunion
| image = National Lampoons Class Reunion movie poster.jpg
| caption = Class Reunion theatrical poster
| director = Michael Miller
| producer = Matty Simmons John Hughes
| starring = {{plainlist|
* Gerrit Graham
* Fred McCarren
* Miriam Flynn
* Stephen Furst Shelley Smith Michael Lerner
* Chuck Berry
}} Peter Bernstein Mark Goldenberg
| cinematography = Philip Lathrop
| editing = Ann Mills Richard Meyer
| studio = ABC Motion Pictures
| distributor = 20th Century Fox
| released =  
| runtime = 84 minutes
| country = United States
| awards =
| language = English
| budget =
| gross = $10 million
}} National Lampoon National Lampoon Goes To The Movies was filmed in 1981, it was delayed and not released until 1983.

==Synopsis==
Lizzie Borden Highs class of 1972 are getting ready to go through the motions at their ten year reunion, when a deranged alum Walter Baylor, who was driven insane by a horrible sadistic senior-year prank, escapes from the mental institution and decides to crash the party at his high school reunion. Guests start to disappear and are found dead, the other alumni, including the high class snooty yacht salesman Bob Spinnaker, class tease Bunny Packard, and the class zero Gary Nash, spring into action as they try to uncover the culprit and put an end to the nightmare that has become their class reunion. 

==Cast and crew==

;Staff 
*Michael Miller - Director John Hughes - Writer
*Matty Simmons  - Producer

;Cast 
*Gerrit Graham - Bob Spinnaker Michael Lerner - Dr. Robert Young
*Misty Rowe - Cindy Shears
*Blackie Dammett - Walter Baylor
*Fred McCarren - Gary Nash
*Miriam Flynn - Bunny Packard
*Stephen Furst - Hubert Downs
*Mews Small - Iris Augen Shelly Smith - Meredith Modess
*Zane Buzby - Delores Salk
*Jacklyn Zeman - Jane Washborn
*Barry Diamond - Chip Hendrix
*Art Evans - Carl Clapton
*Marla Pennington - Mary Beth McFadden
*Randy Powell - Randy Powell

==Release==
The film was released theatrically in the United States by 20th Century Fox in October 1982. It proved to be a huge disappointment for the company, grossing only $10,054,150 at the box office. 

In 1982, Dell Publishing released a "photo novel" version book, adapted from John Hughes screenplay by Sandra Choron.

The film was released on VHS and laserdisc by ABC Video in 1983. In August 1998, Anchor Bay Entertainment re-released the film on VHS.

In 2000, Anchor Bay Entertainment released the film on DVD. The film was re-released on DVD by MGM in 2005.

National Lampoons Class Reunion grossed $10,054,150 in total for its career. Opening weekend for the movie it made $3,086,525.

==Soundtrack== Peter Bernstein and Mark Goldenberg and includes a live onscreen performance by Chuck Berry, and a small scene where the actors perform the classic 1960 hit Stop!_In_the_Name_of_Love|"Stop! in the Name of Love". The score was edited by Jim Harrison and Lada Productions.   

;Songs performed by:  Peter Bernstein and Mark Goldenberg
* Chuck Berry - "It Wasnt Me", "My Dingaling", and "Festival"

==National Lampoon history==
National Lampoons Class Reunion was the second big-screen movie that was released from the Original National Lampoon Company. The movie had a huge buildup, and viewers expected it to be another hit for the National Lampoon franchise, because it was following National Lampoons Animal House, the companys first big-screen release in 1978. The original company consisted of the writers from the National Lampoon magazine and the some of the cast from the National Lampoon Radio Hour and the stage show National Lampoons Lemmings. 

==Reviews==
National Lampoons Class Reunion suffered negative reviews on release.

;Critics Review
According to T.V. Guide "this is a very unfunny film which was released after the successful National Lampoons Animal House, and which died at the box office, focuses on the 10th reunion of a 1972 high-school graduating class. The plot combines both comedy and horror-slasher elements, a combination that doesnt work. A murderer is killing off class members, who are such dull, dreary creatures no one could blame him. Even Chuck Berry, who makes brief appearance singing a melody of his songs, cant save this one." 
 John Hughes can be blamed for the script, a feeble spoof of a slasher movie. Songs are used to extend running length, but even Chuck Berry seems under par." 

== References ==
 

== External links ==
*   at MGM.com
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 