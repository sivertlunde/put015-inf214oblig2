Von Richthofen and Brown
{{Infobox film
| name           = Von Richthofen and Brown
| image          = Von Richthofen and Brown.jpg
| image_size     = 225px
| caption        = Theatrical release poster
| director       = Roger Corman
| producer       = Gene Corman
| writer         = John William Corrington Joyce Hooper Corrington
| starring       = John Phillip Law Don Stroud
| music          = Hugo Friedhofer Michael Reed Alan Collins
| studio         = The Corman Company
| distributor    = United Artists
| released       =  
| runtime        = 97 minutes
| country        = United States English
| budget         = under $1 million Corman and Jerome 1990, p. 169.  gross = 108,851 admissions (France) 
}}
Von Richthofen and Brown, also known as The Red Baron, is a 1971 war film directed by Roger Corman, and starring John Phillip Law and Don Stroud as the title characters. Although names of real people are used, the story by Joyce Hooper Corrington and John William Corrington makes no claim to be historically accurate, and in fact is largely fictional. Corman 1978, p. 224. 

==Plot== Roy Brown (Don Stroud) who arrives at a British squadron, where the top scoring pilot is a Victoria Cross holder named Lanoe Hawker (Corin Redgrave).

The two pilots are very different; Brown ruffles the feathers of his squadron mates by refusing to drink a toast to Richthofen, while the Baron awards himself silver trophies in honour of his kills and clashes with fellow pilot Hermann Göring (Barry Primus), when Boelcke is killed after a mid-air collision and Richthofen assumes command of the squadron. While Brown becomes moody and depressed by his war service, Richthofen becomes outwardly energized by the war. Outraged by an order to camouflage his squadrons aircraft, he paints them in bright conspicuous colours, claiming that gentlemen should not hide from their enemies.

The toll on both squadrons is highlighted when Richthofen is wounded during an aerial battle and Lanoe Hawker is killed. The war becomes personal for both when Brown and his squadron attack Richthofens airfield, destroying their aircraft on the ground. Revenge comes when Richthofen, with the help of a batch of new fighters from Anthony Fokker (Hurd Hatfield) launches a counterattack on the British airfield. Back at their aerodrome, Richthofen rants at Göring for leaving the formation and strafing medical personnel.  He says:  "Youre an assassin!"   Göring defends himself by saying:  "I make war to win."  Richthofen tells him:  "Get out of my sight!", threatening that if Göring does something similar again, he will personally go to the Kaiser to make sure Goering is shot. 

Richthofens passion for the war fades, becoming dismayed that his squadron is losing so many pilots. He even starts to realize that Germany might lose the war. He refuses a job offer from the government deciding to help fight alongside his men, knowing it will probably lead to his death in combat. 

On April 21, 1918, Richthofen and Brown engage in an aerial duel during which Richthofen receives a fatal wound. He is able to land his aircraft, but soon dies. The Allied pilots congratulate Brown on downing Richthofen.  The pilot who will take over from Richthofen is Göring.

==Cast==
  
*John Phillip Law as Manfred von Richthofen Roy Brown
*Barry Primus as Hermann Göring
*Corin Redgrave as Lanoe Hawker
*Karen Huston as Ilse
*Hurd Hatfield as Anthony Fokker
 
*Stephen McHattie as Werner Voss
*Brian Foley as Lothar von Richthofen
*Robert La Tourneaux as Ernst Udet
*Peter Masterson as Oswald Boelcke David Weston as Murphy Tom Adams as Owen
 

==Production==
 ’s collection in flight over Weston Aerodrome, Ireland (August, 1970)]] 
 -built S.E.5 replica flown by Charles Boddington five seconds before fatal crash on September 15, 1970]] 
 
Roger Corman had been interested in making a film about von Richthofen for a number of years.  In 1965 it was announced he had commissioned a script called The Red Baron from Robert Towne.  Although the story of the two foes who meet in a fateful last flight, was essentially a historical subject, Cormans intention was to treat the subject as an allegory of the modern war machine in conflict with antiquated old world notions of chivalry. 
 Tiger Moths Stampe SV4Cs had also been converted to represent other aircraft, for a total of 12 aircraft available for aerial scenes. 
 Alouette helicopter, along with a Helio Courier, for aerial photography, supported by a number of specialized camera mounts Garrison developed for use on individual aircraft. This allowed footage of actors, such as John Philip Law and Don Stroud "flying" the aircraft. Garrison trained Law and Stroud to the point where they could take off, land a Stampe, and fly basic sequences themselves from the rear seat, filmed with a rear-facing camera. Stunt pilots were used for the more complicated sequences, one such being famed New Age author Richard Bach. Bach wrote about his experiences in a short story entitled "I Shot Down the Red Baron, and So What", which is reproduced in his short story collection A Gift of Wings. 

Corman used a filming schedule that included so-called "Blue Days, Grey Days and Don’t Give a Damn Days" so that the aircraft were used no matter what the weather presented.

On September 15, 1970, Charles Boddington, a veteran of both The Blue Max and Darling Lili, was killed when his S.E.5 spun in during a low-level maneuver over the airfield. The next day, during the last scheduled flight on the shooting schedule, Garrison and Stroud were involved in a low-level sequence across Lake Weston, in a Stampe, when a jackdaw struck Garrison in the face, knocking him unconscious. The aircraft then ran through five powerlines, snap rolled and plunged into the River Liffey inverted. Garrison and Stroud were rescued from the water. Stroud was uninjured, but Garrison required 60 stitches to close a head wound. Both incidents occurring in such a short period resulted in Irish authorities grounding the production. Corman lobbied for restoration of flying and a few days later, was successful. 
 Powerscourt House, a noted stately home in County Wicklow Ireland. Powerscourt had been designed by Richard Cassels, a German architect, and the entrance hall had a Germanic motif, lending a visual connection to a German location. 

Corman felt the pressure of directing a big-budget feature were such that he wanted to take a sabbatical.  Although heavily involved as a producer during the interim, he did not direct another film until Frankenstein Unbound (1990). 

==Reception==
Von Richthofen and Brown received mixed to negative reviews from both viewers and critics, although Roger Greenspun, in his review for The New York Times saw Cormans work as "... an extraordinarily impressive movie by a filmmaker whose career has not always been marked by success, or even noble failure."  Critics also connected Cormans anti-war views with the central characters of the film, seeing the antagonists as representing the modern relentless killing machine versus old world chivalry. Evans 2000, p. 195.  

As an aviation epic, reviewer Leonard Maltin noted, "Aerial work is excellent, its the ground work which crashes." 

==References==
Explanatory notes
 

Citations
 

Bibliography
 
* Bach, Richard. A Gift of Wings. New York: Dell, 1989. ISBN 978-0-44020-432-9.
* Corman, Roger. How I Made A Hundred Movies In Hollywood And Never Lost A Dime. New York: da Capo Press, 1978. ISBN 978-0-30680-874-6.
* Corman, Roger. Roger Corman: Interviews. Jackson, Mississippi: University Press of Mississippi, 2012. ISBN 978-1-61703-165-6.
* Corman, Roger and Jim Jerome. How I Made a Hundred Movies in Hollywood and Never Lost a Dime. London: Muller, 1990. ISBN 978-009174-679-7.
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books, 2000. ISBN 1-57488-263-5.
* Hardwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Hyams, Jay. War Movies. New York: W.H. Smith Publishers, Inc., 1984. ISBN 978-0-8317-9304-3.
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
 

==External links==
 
* 
* 
* 

 

 
 

 
 
 
 
 
 
 
 
 
 
 