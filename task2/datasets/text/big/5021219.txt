Tragedy of a Ridiculous Man
{{Infobox film
| name           = Tragedy of a Ridiculous Man
| image          = Tragedy of a Ridiculous Man poster.jpg
| image_size     = 
| caption        = 
| director       = Bernardo Bertolucci
| producer       = Giovanni Bertolucci
| writer         = Bernardo Bertolucci
| narrator       = 
| starring       = Ugo Tognazzi Anouk Aimée
| music          = Ennio Morricone
| cinematography = 
| editing        = Gabriella Cristiani
| distributor    = Ladd Company, through Warner Bros. (United States|USA)
| released       =  
| runtime        = 116 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}} Best Male Actor Award at the 1981 Cannes Film Festival for his performance.    In his review, Vincent Canby describes the film as, "Bernardo Bertoluccis very good, cerebrally tantalizing new film, Tragedy of a Ridiculous Man, the story of what may or may not be a terrorist kidnapping of the sort that has been making Italian headlines with increasing frequency in recent years." 

==Synopsis==

In a small Italian village, Primo runs a cheese factory, but one morning he witnesses his son
kidnapped by terrorists. As the film progresses, he has two problems: paying the ransom the 
terrorists demand and trying to save his factory which might face bankruptcy. Whats a businessman like him to do?

==Cast==
* Ugo Tognazzi - Primo Spaggiari
* Anouk Aimée - Barbara Spaggiari
* Laura Morante - Laura
* Victor Cavallo - Adelfo
* Olimpia Carlisi - Chiromant
* Vittorio Caprioli - Marshal
* Renato Salvatori - Colonel
* Ricky Tognazzi - Giovanni Spaggiari
* Gianni Migliavacca
* Margherita Chiari - Maid
* Ennio Ferrari
* Gaetano Ferrari - Guard
* Franco Trevisi
* Pietro Longari Ponzoni
* Don Backy - Crossing Keeper

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 