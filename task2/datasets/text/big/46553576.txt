(attn dfct)
 
{{Infobox film
| name           = (attn dfct)
| image          = 
| caption        = 
| image size       =
| director       = Chris Burt
| producer       =
| writer         =
| narrator       =
| starring       =Jack Olson Mike Lemnitzer David Nelson TJ Moran Andrew Liebman Sam Evensen Andrew Ellison Dalton Jones
| music          = James Pants   Wiicca   Yung Bitch   18 Carat Affair   Death Grips   Sukmeqilme   Mono Lisa   Wise Blood
| cinematography =
| editing        = Chris Burt
| distributor    = Independent
| released       = (DVD) August 2013   (Online) September 2013
| runtime        = 15&nbsp;mins.
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Sony VX-1000 and similar digital film cameras.

==Introduction feature==
The introduction features the title in relief with a moving ocean, set on a background of gray light. This is followed by a short montage of a television being shattered and the first skateboarder (Jack Olson) injuring himself.    The audio used is Epilogue by James Pants. 

==Production==
The video took, according to Burts YouTube description, about a year to make. The entire video is split into parts for each skateboarder, as well as a "friends" montage and an ending credits montage. Each part is preceded by a shot with the name of the skateboarder in relief over a moving pattern in a palette that is consistent throughout the video.  Slow motion is utilized for dramatic effect, especially near the end of each part.

==Filming==
Burt only gives his own name for the filming and editing of the whole video. Josh Manoles is credited for creating the DVD cover art.

==Release==
The video was released as a screening event in August 2013 at Cal Surf in Minneapolis, MN.  The video was also made publicly available on YouTube a month later.  It received positive attention from forums such as SkatePerception  and SkateVideoSite  as well as Transworld Skateboarding. 

==References==
 

 
 