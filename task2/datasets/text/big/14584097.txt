Heroes for Sale (film)
{{Infobox film
| name           = Heroes for Sale
| image          = File:Film_Poster_for_Heroes_for_Sale.jpg
| caption        = Theatrical release poster
| producer       = Hal B. Wallis
| director       = William Wellman
| writer         = Robert Lord Wilson Mizner
| starring       = Richard Barthelmess Aline MacMahon Loretta Young
| music          = Bernhard Kaun
| cinematography = James Van Trees
| editing        = Howard Bretherton
| distributor    = Warner Bros. Pictures
| released       = June 17, 1933 TCM print) 
| country        = United States
| language       = English
}}
Heroes for Sale (1933) is a Great Depression|Depression-era film directed by William Wellman, starring Richard Barthelmess, Aline MacMahon, and Loretta Young, and released by Warner Bros. and First National Pictures. Original 76 minute considered lost film|lost, current 71 minutes from Turner Entertainment print.

==Plot==
A veteran of the Great War, Thomas Holmes (Richard Barthelmess), struggles to make his way in civilian life in almost every way imaginable.  In the opening scene of the movie, Tom and his friend are on a mission to gather intelligence by capturing a German soldier.  Toms friend, the bankers son Roger Winston (Gordon Westcott), in terror, refuses to leave the shell hole so Tom volunteers to go alone.

He captures a German but is apparently killed; in fact, he has only been wounded, and the Germans take him to their hospital to recover.  His friend Roger Winston returns to the safety of American lines with the captured German soldier and is rewarded with a medal for it; his feeble efforts to refuse credit are dismissed as modesty, and he comes home a decorated hero. During Toms captivity, German doctors treat his pain with morphine and he becomes addicted to the drug.  After Tom returns from the war, Roger offers him a job at his fathers bank out of shame.
 Grant Mitchell) dies. The new ownership decides to break the deal and automate the laundry, throwing most of its employees out of work, Tom included.

Furious and resentful, the fired employees march on the plant to destroy the machines, as Tom does his best to stop them.  In the riot with police that follows, Ruth is killed trying to find him, and he is arrested as a ringleader of the mob.  Tom is put away for five years in prison; in the meantime, the invention he helped finance continues to sell nationwide, throwing countless other people out of work.  When Tom gets out, it is 1932, the heart of the Depression. Unimaginably rich, he refuses to take the proceeds, which by now amount to over fifty thousand dollars.  Instead, it goes to feed the endless line of hungry and jobless that come seeking a handout at the diner that Pop Dennis and Mary run. When "Red Riots" break out, the local city "Red Squad" arrests Tom and drives him out of town.

Without work, at the mercy of a society in which unemployed men are turned into hobos and every community orders them to keep moving on, Tom finds himself in one hobo shantytown, next to Roger, his old army comrade. Roger Winston, too, has been ruined; his father stole from the bank and when exposure came, killed himself. Roger served time in prison.  Now neither of them has any prospect, any future. The difference is that Tom, in a stirring speech, asserts his faith that America can and will restore itself, that he can lick the Depression.  Still driven on by authorities, with no prospect in sight, he marches ahead, determined that this is not the end. And back at the diner, the line of needy continues to stretch down the street, all of them being fed by the funds he provided, and on the wall a plaque honors him for his gift. The movie closes with his son looking at it and declaring to Mary that when he grows up, he means to be just like his Dad. The message is clear: a hero in war, Tom is a hero still.

Haunting and powerful, "Heroes for Sale" was issued at one of the darkest points in the Depression.  Its views of American society were particularly dark.  Police are there to beat up demonstrators and harass people that they consider dangerous radicals, their squads little better than vigilante gangs. The courts mete out injustice. The bankers are crooks, the honest businessmen outweighed by those who care only for their profits and at the expense of workers. Even the comical radical at the start of the film, having come into money, has become a Social Darwinist, caring nothing for those in need and out only for himself. For audiences expecting a happy ending, the sudden, violent death of Toms wife Ruth comes as an unexpected shock. Where hints are given from the start that Mary is also in love with Tom, and where, in the customary movie formula from later in the 1930s, audiences might expect that they would end up together at the films close, no such reuniting happens. And yet, unlike the 1932 masterpiece, "I Was A Fugitive From A Chain Gang," "Heroes for Sale" shows the shift in mood as the New Deal began. It ends not in despair, but with an expression of hope, not just in Toms speech, but in the picture of those in need being taken care of. Indeed, in expressing his confidence, Tom refers specifically to Franklin Delano Roosevelts inaugural address—which, in a Warner Brothers picture, should not be too surprising: Warner Brothers was friendlier to the New Deal than most of the other big studios, just as its films gave far more attention to the big city milieu and members of the working class.

==Cast==
*Richard Barthelmess as Thomas Holmes
*Aline MacMahon as Mary Dennis
*Loretta Young as Ruth Loring Holmes
*Gordon Westcott as Roger Winston
*Robert Barrat as Max Brinker
*Berton Churchill as Mr. Winston Grant Mitchell as George W. Gibson
*Charles Grapewin as Pa Dennis
*Robert McWade as Dr. Briggs
*G. Pat Collins as Leader of agitators James Murray as Blind soldier

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 