The Amityville Haunting
 
{{Infobox film
| name           = The Amityville Haunting
| image          = The Amityville Haunting (2011).jpg
| alt            =  
| caption        = DVD cover
| director       = Geoff Meed
| producer       = David Michael Latt
| writer         = 
| screenplay     = 
| story          = 
| starring       = 
| music          = David Raiklen
| cinematography = Ben Demaree
| editing        = Cody Peck
| studio         = The Asylum Taut Productions
| distributor    = The Asylum
| released       =   
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Amityville Haunting is a direct to video film released on December 13, 2011. It is the tenth production to be inspired by the 1977 book The Amityville Horror. The film was produced by The Asylum and Taut Productions.
 found footage that documents the horrifying experiences of a family that moved into the infamous haunted house."

Although the poster features the traditional quarter round windows and gambrel roof of 112 Ocean Avenue, they are never seen on screen.

==Plot== CCTV cameras John Matthew, which leads Douglas to wonder if Lori or Tyler told Melanie about the history of the house. As the family grows more fearful of the unexplainable deaths of a close family friend and a neighbor boy who was attracted to Lori, Douglas starts to break down, using religious paraphernalia to rid the house of any spirits that reside within the house. After one month within the house, Lori, Virginia, Douglas, and Tyler Benson all die in various manners. Melanie Benson is the only survivor, as she says that she has plans to stay in the house forever, along with John Matthew. The autopsy reports shown at the end of the film place emphasis on the fact that each victim was under extreme stress at the time of their death.

==Cast==
(all actors are uncredited, as most of the crew)
*Jason Williams as Douglas Benson
*Amy Van Horne as Virginia Benson
*Devin Clark as Tyler Benson
*Nadine Crocker as Lori Benson
*Gracie Largent as Melanie Benson
*Luke Barnett as Ronald DeFeo, Jr./the Ghost
*Tyler Shamy as Greg
*John Kondelik as Brett
*Alexander Rzechowicz as Donny Reddit

==Reception==
The Amityville Haunting received negative reviews from critics. A Horrornews.net writer called it "simply just a bad movie with no offering for viewers whatsoever", criticizing the over-used low-budget scare tricks and its false advertising as "Actual found footage". He also described Jason Williams performance as Doug as "not believable for what its trying to achieve and simply comes off as d*ck with an attitude", but said that "The military freak-out tops the icing by just making it all seem rather silly".  Dread Centrals Foywonder scored it a one out of five, concluding his review with "A part of me almost wonders if the only reason The Amityville Haunting even exists is because someone made a bet that they could dethrone Amityville 3D for the title of worst “Amityville” movie of all time. I don’t know if they succeeded here, but they sure give it a run for its money." 

==References==
 

==External links==
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 