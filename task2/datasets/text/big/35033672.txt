The Birthday Dragon
{{multiple issues|
 
 
}}
{{Infobox television
| name           =The Birthday Dragon
| image          =
| image_size     =
| caption        =
| director       =Hilary Phillips
| producer       =Gerald Tripp
| writer         =Patrick Granleese
| starring       =Tracey Moore, Barry Morse
| music          =
| cinematography =
| editor         =
| distributor    = Lacewood Productions CTV Television Network (Canada) Family Home Entertainment (U.S.) (1993) (video)
| released       = September 11, 1992 (U.S.)
| runtime        =
| country        = United States
| language       = English
| budget         =
}} American television animated film. It is a sequel to the 1989 animated film The Railway Dragon.

==Plot==
Three years on since first meeting the Railway Dragon, Emily manages to summon him to her house one night by shouting tidings. Upon his arrival, Emily reveals to him that she would like him to come to her birthday party and meet her other friends. The dragon, however, is reluctant as he feels that the world is not yet safe for him, despite Emily telling him there are lots of people like her who can make the world safe for dragons.
Furthermore, the dragon is curious as to what a birthday is. Emily shows him through a projector after going through different slides, including a picture of the forest and the zoo, before finding a picture from one of her earlier birthday parties and explaining to the dragon that a birthday is a celebration of the day you were born (although the dragon doesnt know which day is his birthday). Emily tells him about the best part of birthdays, which is receiving presents, before showing him by making him a present (wrapping up one of her old toys, a Noahs Ark shaped clock), and giving it to him to unwrap, much to his delight. When Emily tells him if he comes it will be the best present she could ask for, as well as assuring him that hell be safe, the Dragon happily agrees to come to her party before flying off into the night.

The next morning, the boss of a circus informs two bumbling clowns, Lenny and Clarence, that they have another birthday party to do. Clarence is an overweight man who enjoys clowning, but is quite dumb and easily sidetracked. Lenny, on the other hand, hates it and is constantly infuriated by Clarence while looking for a way to get a better job.
At Emilys party, Emily has made everyone dress up as dragons to help the Railway Dragon blend in, which is successful. Lenny and Clarence perform to the children but both mess up, so Emily, the dragon and the children move onto the games. It all goes well, until a blindfolded child playing Pin the tail on the donkey accidentally stabs the dragon in the butt, causing him to fly up and roar, scaring the children away in the process.
Angry, the Dragon tells Emily is was a mistake coming to her party and flies off back to his Railway tunnel, despite Emily calling for him to come back. This is watched by both Lenny and Clarence, with both realising the dragon is real and Lenny now believing they have a way to get to the top.

The two clowns follow Emily to try and find the Dragons lair, unbeknown to her. However, they lose track of her when she slides down the burrow into the Railway Dragons lair. Although she cannot find the dragon, she tells him that just because things went bad at the party doesnt mean they cant still be friends, and states that she doesnt want a world without dragons before leaving the cave.
In the forest, the other dragons she met on the Day of Tidings appear to her to try and cheer her up, but she sends them away and tells them that they must hide where humans arent looking.
In his cave, the Railway dragon tearfully looks back on the days events before finding the Noahs Ark clock he received from Emily. Upset at how he has hurt Emilys feelings (especially since she cares the most about him), he comes up with a plan to make it up to her by giving her the best present ever.

That night, Emily is woken up and finds various zoo animals in her house and garden before seeing the Dragon outside and hugging him. The dragon reveals he has brought almost all the animals from the zoo (with the exception of some beetles and the crocodile) as her birthday present. Emily tells him it is a wonderful present, but its not safe in the world right now for the animals. The dragon and Emily both agree that they must work till they make it safe again for both the zoo animals and for dragons.  
They are interrupted, however, by Lenny and Clarence (who had hidden outside Emilys house). As the Dragon roars and advances towards them, the two clowns spring their trap and catch the Railway Dragon in a net attached to a circus pickup truck. As they drive off, Emily shouts tidings to call the other dragons. Together, they quickly capture both Lenny and Clarence (putting them in the net) before freeing the Railway Dragon.
While the other dragons play with the two clowns, Emily and the Railway Dragon manage to return all the animals back to the Zoo before the Zoo owner realises theyre missing.

At the Circus, the Boss takes several days wages from Lenny and Clarence for various reasons including their stupid story about the Dragons. As Lenny is about to complain to Clarence, the two look up fearfully as Emily and the Dragon fly overhead, with both clowns agreeing that they didnt see anything.
In the sky, the railway dragon takes Emily for another fun ride before flying back to the railway tunnel with her. He is upset as he has not been able to give her a present to keep, but Emily reassures him that she has a wonderful present to keep; a memory of the days events and also that she and the dragon are still friends.
As the dragon happily states that the day feels like his birthday also, Emily surprises him as the other dragons appear (one holding a large cake) and wish him happy birthday, before both himself and Emily blow out the candles on the cake together.

==Cast==
*Tracey Moore as Emily
*Barry Morse as The Railway Dragon
*Robert Bockstael as Emilys Father/Boss
*Johnathan Cameron as The boy at the party
*Johni Keyworth as Clarence
*John Koensgen as Lenny
*Melissa Willet as The girl at the party

==External links==
* 

 
 
 
 
 
 
 
 
 
 