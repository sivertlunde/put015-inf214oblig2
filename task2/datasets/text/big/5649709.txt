Gulliver's Travels (1939 film)
{{Infobox film
| name = Gullivers Travels
| image = Gulliverstravelsposter.jpg
| caption =
| director = Dave Fleischer
| producer = Max Fleischer Dan Gordon Cal Howard Tedd Pierce Edmond Seward Isadore Sparber
| based on =  
| starring = Pinto Colvig Jack Mercer Sam Parker Jessica Dragonette Lanny Ross  Tedd Pierce
| music = Victor Young Leo Robin (songs) Ralph Rainger (songs) Al Neiburg (songs) Winston Sharples (songs) Sammy Timberg (songs)
| cinematography = Charles Schettler
| studio = Fleischer Studios
| editing =
| distributor = Paramount Pictures
| released =  
| runtime =  76 minutes
| country = United States
| language = English
| budget = $700,000 
}}
  American traditional Snow White Tom Palmer, Grim Natwick, William Henning, Roland Crandall, Thomas Johnson, Robert Leffingwell, Frank Kelling, Winfield Hoskins, and Orestes Calpini. This is Paramount Pictures|Paramounts first feature-length animated film.
 Lilliputian adventures of Gulliver depicted in Jonathan Swifts 18th century novel Gullivers Travels.

==Plot==
On November 5, 1699, Gulliver (voiced by Sam Parker) washes onto Lilliput and Blefuscu|Lilliput, after a shipwreck. While scouting the forest, the town crier Gabby (voiced by Pinto Colvig), finds Gulliver unconscious body and rushes to warn the ruler of Lilliput, King Little (voiced by Jack Mercer). At this time, Little and his friend, King Bombo (voiced by Tedd Pierce) of Blefuscu, are signing a wedding contract, granting their children, Princess Glory of Lilliput (voiced by Jessica Dragonette) to Prince David of Blefuscu (voiced by Lanny Ross), permission to marry. When they argue over which song is to play at the wedding, Bombo declares war.

After failures, Gabby tells King Little of the "giant on the beach" (i.e. Gulliver), and leads a mob to the beach to capture him. There, the Lilliputians tie Gulliver to a wagon on which they convey him to the capital. In the next morning, Gulliver awakens and breaks himself free; but when they see that the invading Blefuscuians are intimidated by his size, the Lilliputians enlist his help against their neighbor, treating him with hospitality and making him a new set of clothes.

King Bombo, who has sent three spies, Sneak, Snoop, and Snitch, into Lilliput, orders them to kill Gulliver; whereupon the spies steal Gullivers flintlock pistol, confiscated by the Lilliputians, and prepare to use it against him. Meanwhile, Gulliver learns of the wars cause from Glory and David, and proposes a new song that combines the two proposed by their fathers.

When the spies assure King Bombo that they can kill Gulliver, Bombo announces by carrier pigeon Twinkletoes, that he will attack at dawn. Gabby intercepts this message and warns the Lilliputians; but is himself captured by the spies, who prepare the pistol. As the Blefuscuian fleet approaches Lilliput, Gulliver ties them together and draws them disarmed to shore. The spies fire at Gulliver from a cliff, but Prince David diverts the shot and falls to his apparent death. Using Davids body to illustrate his point, Gulliver scolds both Lilliput and Blefuscu for fighting; but when they solemnize a truce, reveals that David is unharmed, whereupon David and Glory sing their combined song for everyone to hear. Both thereafter build a new ship for Gulliver, on which he departs.

==Cast==
*Gulliver - Sam Parker
*Gabby - Pinto Colvig
*King Little, Twinkletoes, Sneak, Snoop, and Snitch - Jack Mercer
*King Bombo - Tedd Pierce
*Princess Glory - Jessica Dragonette
*Prince David - Lanny Ross

==Soundtrack==
* "Faithful/Forever" (Music by Ralph Rainger, lyrics by Leo Robin)
* "I Hear a Dream (Come Home Again)" (Music by Ralph Rainger, lyrics by Leo Robin)
* "Were All Together Now" (Music by Ralph Rainger, lyrics by Leo Robin)
* "Bluebirds in the Moonlight (Silly Idea)" (Music by Ralph Rainger, lyrics by Leo Robin)
* "Alls Well" (Music by Ralph Rainger, lyrics by Leo Robin)
* "Its a Hap-Hap-Happy Day" (Written by Sammy Timberg, Al Neiburg and Winston Sharples)

"Alls Well", "Its a Hap-Hap-Happy Day", and "Faithful Forever" all later became standards of Fleischer, and later Famous Studios, cartoon scores. The films song "I Hear a Dream" was also very popular as well. 

==Production== Snow White and the Seven Dwarfs, Paramount agreed to allow the Fleischers to make a feature.  Paramount offered to build the New York City-based Fleischers a new state-of-the-art animation studio in Miami Beach, Florida, away from the union influence which had polarized the Fleischer Studio after a bitter 1937 labor strike. The Fleischers agreed, and began development on Gullivers Travels in spring 1938 as construction began on the Miami studio. The Miami Fleischer Studio opened in fall 1938, and the Fleischer staff moved their production headquarters there. A few individuals, including voice actor Mae Questel, opted to remain in New York and did not follow the Fleischers to Miami.
 Leon Schlesinger Frank Smith and James Culhane, who had all migrated over to the Disney studio. Factions developed between the East and West Coast animators, who were unaccustomed to each others habits. The two sides grew further apart after Howard, Pierce, and the other Hollywood storymen decided to discard the New York regimes storyboards, crafting the films plot over again from scratch.

Rotoscoping, an animation technique originally developed by the Fleischer Studios, was used throughout Gullivers Travels to animate Gulliver. The process involves tracing live-action footage frame-by-frame; Sam Parker, the actor who performed the voice of Gulliver, also modeled as the characters live-action reference. This was in an attempt to differentiate the animation style of Gulliver from the more comical Lilliputians. Popeye the Sailor had originally been planned to "portray" Gulliver, but these plans were scrapped during pre-production.

The voice cast consisted of a variety of performers. The voice of Gabby was provided by Pinto Colvig, who had previously worked at Disneys. Colvig had previously been the voice of Goofy, provided vocal effects for Pluto, was the stern Practical Pig in The Three Little Pigs (1933), and voiced Grumpy and Sleepy in Snow White and the Seven Dwarfs. Jack Mercer, who portrayed King Little of Lilliput, was a story man for Fleischers who lent his voice the gruff Popeye the Sailor. In addition to voicing King Little, Mercer was also the voice behind Bombos spies, Sneak, Snoop, and Snitch. Mercer was a regular voice heard in Fleischer and Famous Studios cartoons, and worked for Paramount until Famous Studios was dissolved. Jessica Dragonette and Lanny Ross were both popular singers of the day, and were hired to sing for Princess Glory and Prince David, respectively. Sam Parker was a radio announcer in the 1930s who won the role of Gulliver in a radio contest. When the Fleischers met Parker, they felt that his appearance was suitable for him to also perform in the live action footage that would be rotoscoped to create Gullivers movement.  Tedd Pierce was a story man hired away from Leon Schlesinger Productions to join Fleischer in their trip to Miami. Pierce, who would occasionally do voices for some of the characters in the cartoons, played King Bombo. Other vocal credits by Pierce include the voice of "Babbit" to Mel Blancs "Catstello", and that of Bertie in Chuck Joness Hubie and Bertie cartoons. 

==Release==
Like Snow White before it, Gulliver was a success at the box office, and led to the production of another Fleischer/Paramount feature,  .

==Home video releases==
Due to the films public domain status, it has been released by many distributors in various home video formats. E1 Entertainment released the film on Blu-ray Disc on March 10, 2009, but received strong criticism for presenting the movie in a stretched and cropped 1.75:1 format, as well as applying heavy noise reduction.   
In March 2014, Thunderbean Animation released a restored version of the film with several Fleischer Studios shorts in a Blu-ray/DVD combo pack titled Fleischer Classics Featuring Gullivers Travels.  

==Awards==
The film was nominated for two Academy Awards:
* Victor Young for Best Music, Original Score
* Ralph Rainger (music) and Leo Robin (lyrics) for Best Music, Original Song for the song "Faithful Forever"

== Popular culture ==
A couple of scenes from the film are briefly seen in two episodes of  : "The Incredible Shrinking Town" and "Flippy".

==Spin-off cartoons==
The film was spun off into two short-lived Fleischer cartoon short series: the Gabby (film series)|Gabby cartoons starring the Pinto Colvig-voiced Lilliputian sidekick of the film, and the Animated Antics cartoons starring Sneak, Snoop and Snitch (the three villains) and Twinkletoes (the carrier pigeon) from the film.

==See also==
* List of animated feature films

==Further reading==
* Michael Barrier (historian)|Barrier, Michael (1999). Hollywood Cartoons: American Animation in Its Golden Age. Oxford: Oxford University Press. ISBN 0-19-516729-5.
* Cabarga, Leslie (1976, updated 1988). The Fleischer Story. Cambridge: Da Capo Books. ISBN 0-306-80313-5.
* Leonard Maltin|Maltin, Leonard (1980, updated 1987). Of Mice and Magic: A History of American Animated Cartoons. New York: Penguin Books. ISBN 0-452-25993-2.

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 