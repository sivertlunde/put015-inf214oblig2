The Firebird (1952 film)
{{Infobox film
| name           = The Firebird
| image          =
| caption        =
| director       = Hasse Ekman
| producer       = Lorens Marmstedt, Terrafilm
| writer         = Hasse Ekman
| narrator       =
| starring       = Tito Gobbi Ellen Rasch Eva Henning Georg Rydeberg
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 99 minutes
| country        = Sweden Swedish
| budget         =
| gross          =
}}
 Italian film The Red Shoes by Powell and Pressburger.

==Plot summary== Royal Opera in Stockholm and falls in love with her. And so a passionate and colourful drama begins.

==Cast==
*Tito Gobbi as Mario Vanni, Italian operasinger
*Ellen Rasch as Linda Corina, prima ballerina 
*Eva Henning as Alice Lund
*Bengt Blomgren as Frank
*Georg Rydeberg as Jascha Sacharowitch
*Åke Falck as Spinky, Vannis impresario 
*Märta Arbin as Franks mother
*Alan Blair as Johan A. Sjöberg, Lindas  doctor 
*Märta Dorff as Seamstress at the Operan 
*Gull Natorp as Tilda, dresser at the Operan 
*Björn Holmgren as John, Dancer 
*Gun Skoogberg as Dancer 
*Maurice Béjart as Alex, Dancer 

==External links==
* 
* 

 

 
 
 
 
 

 