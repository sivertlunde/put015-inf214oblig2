The Death of Mr. Lazarescu
{{Infobox film
| name           = The Death of Mr. Lazarescu
| image          = PosterLazarescu.jpg
| caption        = Cannes Film Festival poster
| writer         =  
| starring       =  
| director       = Cristi Puiu
| producer       =  
| cinematography =  
| editing        = Dana Bunescu
| distributor    = Tartan USA
| released       =  
| runtime        = 153 minutes
| country        = Romania
| language       = Romanian
| budget         = €350,000
| music          = Andreea Paduraru
}}
The Death of Mr. Lazarescu ( ) is a 2005 Romanian dark comedy film by director Cristi Puiu. In the film an old man (Ioan Fiscuteanu) is carried by an ambulance from hospital to hospital all night long, as doctors keep refusing to treat him and send him away.

The Death of Mr. Lazarescu enjoyed immediate critical acclaim, both at film festivals, where it won numerous awards, and after wider release, receiving enthusiastic reviews. However, the film did poorly in international box office. The film is planned to be the first in a series by Puiu called Six Stories from the Outskirts of Bucharest.

==Plot==
Dante Remus Lăzărescu (Ioan Fiscuteanu), a cranky retired engineer, lives alone as a widower with his three cats in a Bucharest apartment. In the grip of extreme pain, Lăzărescu calls for an ambulance, but when none arrives, he asks for his neighbors help. Not having the medicine Lăzărescu wants, the neighbors give him some pills for his nausea. A neighbor reveals that Lăzărescu is a heavy drinker. His neighbor helps Lăzărescu back to his apartment and to bed. They call again for an ambulance. 

When the ambulance finally arrives, the nurse, Mioara (Luminiţa Gheorghiu), dispels the idea that Lăzărescus ulcer surgery over a decade before could cause this pain. While taking a patient history, she suspects that Lăzărescu has colon cancer. After informing his sister, who lives in a different city, that the condition could be serious and she should visit Lăzărescu in the hospital, the nurse decides to get him to a hospital. His sister makes arrangements to come the following day; his only child, a daughter, lives in Toronto.

 
The film follows Lăzărescus journey through the night, as he is carried from one hospital to the next. At the first three hospitals, the doctors, after much delay, reluctantly agree to examine Lăzărescu. Then, although finding that he is gravely ill and needs emergency surgery, each team refuses to admit him and sends him to another hospital. Meanwhile, Lăzărescus condition deteriorates rapidly, his speech is reduced to babbling, and he slowly loses consciousness. The hospitals are jammed with injured passengers from a bus accident, but some doctors appear to reject him out of fatigue or because they do not feel like taking care of a smelly old drunkard. During the night, his only advocate is Mioara, the paramedic who stubbornly stays by him and tries to get him hospitalized and treated, while passively accepting verbal abuse from the doctors who look down on her.

Finally, at the fourth hospital, the doctors admit Lăzărescu. They perform an emergency operation to remove a blood clot in his brain. One of the doctors comments that they have saved him so he can die from an incurable liver neoplasm which they had found.

==Production==
According to   and Cristian Mungiu), accused CNC of directing financing towards the members of its Advising Council, led by Sergiu Nicolaescu, and their protégés.    In 2003 Puiu wrote in a few weeks the synopsis for a six-film cycle he called Six stories from the outskirts of Bucharest (including The Death of Mr. Lăzărescu). He initially planned them as low-budget films, in order to prove that Romanian directors can make films without aid from the CNC. 

Between 2001 and 2003, Cristi Puiu suffered from stress and an exaggerated fear of relatively minor ailments; as a result of his   and a common form of colitis, Puiu became convinced that he had a terminal disease. The resulting fear of dying made him obsessively collect information on diseases and medication, and he spent much time in doctors offices and emergency rooms. This period informed his portrayal of the medical system in this film. 

In addition, the filmmaker knew of a notorious Romanian case in 1997 of Constantin Nica, a 52-year-old man who, after being sent away from several hospitals, was left in the street by the paramedics and died. 

After finishing the synopsis for the six films in Six Stories from the Outskirts of Bucharest, Cristi Puiu showed them to Răzvan Rădulescu, a writer and screenwriter who collaborated with Puiu on writing Stuff and Dough (2001) and Lucian Pintilies Niki and Flo (2003). They started researching The Death of Mr. Lăzărescu by going to various doctors and hospitals, then completed the screenplay.  Puiu and Rădulescu entered the screenplay in the 2004 Script Contest organised by the CNC. However, the CNC refused financing for The Death of Mr. Lăzărescu. Puiu appealed directly to Răzvan Theodorescu, the Minister of Culture, who approved it immediately, overruling the CNC decision. Cristi Puiu: "The Romanian movie industry is losing ground" interview with Cristi Puiu by Otilia Haraga, Bucharest Daily News 16 Dec. 2005 

The filming was accomplished over 39 nights, in November–December 2004. Because the film was finished late in the year, the crew worked hard to complete it in time for 2005 Cannes Film Festival. The film was completed on an overall budget of €350,000.  To produce this film, Cristi Puiu started his own production company, Mandragora Movies|Mandragora, together with his wife and Alexandru Munteanu, the executive producer of The Death of Mr. Lăzărescu. All marketing decisions were left to his partners in the production company, Puiu focusing on the artistic and technical issues. 

Romanian-American pop singer Margareta Paslaru consented to the use of a couple of songs from her repertoire for both the opening and ending credits in the movie: "Cum e oare" (Telling It Like It Really Is) and "Chemarea marii" (The Waves of the Ocean), respectively.

==Reception==
 

===Film critics===
After its 2006 US release, The Death of Mr. Lazarescu rose quickly to critical acclaim, receiving enthusiastic reviews. Rotten Tomatoes, which gathers reviews from a large number of professional film critics, gives the film a 93% fresh rating.  Moreover, in 2007 it appeared on more than 10 "Top Ten films of 2006" lists compiled by professional critics, reaching the first place in J. Hobermans list in the "Village Voice" and Sheri Lindens list in the Hollywood Reporter. 

 , May 12, 2006  and  , April 25, 2006  called it "the great discovery of the last Cannes Film Festival and, in several ways, the most remarkable new movie to open in New York this spring".   in the New York Times called it "a thorny masterpiece"  and Philip French described it as "one of the most harrowing and wholly convincing movies Ive seen for several years". 

Many critics, among which J. Hoberman  and Jay Weissberg,  also remarked the black comedy aspect of the film. Michael Phillips wrote in the Chicago Tribune that the film is "a black comedy, among the blackest",  while Peter Bradshaw called it a "blacker-than-black, deader-than-deadpan comedy" and said that, given the subject, "it seems extraordinary to claim that this film is funny but it is".  

Some reviewers criticised the film for its excessive length. Duane Byrge in the Hollywood Reporter said that "at two hours and 34 minutes, we, seemingly, also endure his agony",  while Kyle Smith in the New York Post wrote that "Its supposed to be about a Kafkaesque experience. Instead, it is a Kafkaesque experience".  Other critics noted the length of the film without criticism: Roger Ebert said that "it is a long night and a long film, but not a slow one"  while Philip Kennicott said "its long, but its also very real and worth every minute". 

===Box office===
The Death of Mr. Lazarescu did not fare well in international boxoffice. The film was released in the US on 26 April 2006 by Tartan Films and it played for 22 weeks, until 28 September. It enjoyed a limited distribution, playing in only five theatres simultaneously at its widest release. The film grossed $80,301 in US and an additional $117,046 in Argentina, Mexico and the UK. 

In its home country, Romania, the film was released earlier, on 22 September 2005. Trying to attract the public to the cinema, the distributor advertised the film emphasizing the comedy aspect. The Death of Mr. Lazarescu was a boxoffice success relative to the domestic market with 28,535 spectators before the end of the year. By the number of spectators it was the most successful Romanian film of 2005 and the 6th most successful Romanian film in 2001–2005. 

Even though the film brought Ion Fiscuteanu worldwide acclaim, unfortunately, it also proved to be his swan song. Sadly and ironically, like the title character he was playing, he died of cancer in 2007.

===Festivals and Awards===
The film was selected in numerous international festival and received more than 20 awards,  among which:
* 2005 Cannes Film Festival – Un Certain Regard Award   
* 2005 Transilvania International Film Festival (Cluj-Napoca) – Audience Award
* 2005 Chicago International Film Festival – Silver Hugo Special Jury Prize
* 2005 Reykjavík International Film Festival – Discovery of the Year Award
* 2005 Copenhagen International Film Festival – Grand Prix du Jury
* 2005 Motovun Film Festival – Propeller of Motovun for Best Film
* 2006 Los Angeles Film Critics Association Awards – Best Supporting Actress in a Motion Picture, Luminiţa Gheorghiu BBC Four World Cinema Award

It also received nominations for Best Director and Best Screenwriter at the 2005 European Film Awards, and for Best Foreign Film at the 2006 Independent Spirit Awards.

==See also==
*Romanian New Wave
*Cinema of Romania

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 