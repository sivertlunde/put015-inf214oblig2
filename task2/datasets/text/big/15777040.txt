Within the Rock
 
{{Infobox Film
| name           = Within the Rock
| image          = 
| caption        = 
| director       = Gary J. Tunnicliffe
| producer       = Robert Patrick/Stanley Isaacs/Scott McGinnis, Barry L. Collier/John Fremes/Barbara Javitz (executive producer), Thomas Carl McGuinness (associate producer), Holly A. Keenan (line producer)
| writer         = Gary J. Tunnicliffe
| starring       = Xander Berkeley, Brian Krause, Duane Whitaker, Michael Zelniker
| music          = Tony Fennell/Rod Gammons
| cinematography = Adam Kane
| editing        = Roderick Davis The Sci Fi Channel/360 Entertainment 1996
| runtime        = 
| country        = USA English
}}
 science fiction slasher Television movie|TV-Movie directed by Gary J. Tunnicliffe produced among others; actor, Robert Patrick and filmed in the USA. It starred Xander Berkeley, Brian Krause, Duane Whitaker and a cameo from former U.S. Marine Corps captain and technical advisor, Dale Dye.

==Cast==
{| class="wikitable"
|-
! Actor / Actress
! Character
|-
| Xander Berkeley
| Ryan
|-
| Brian Krause
| Luke Harrison
|-
| Duane Whitaker
| Potter
|-
| Michael Zelniker
| Archer
|-
| Caroline Barclay
| Dr. Dana Shaw
|-
| Bradford Tatum
| Cody Harrison
|-
| Barbara Patrick
| Samantha Nuke em Rogers
|-
| Calvin Levels
| Banton
|-
| Earl Boen
| Agent Berger
|-
| Brioni Farrell
| Michael Isaacs
|-
| Dale Dye
| General Hurst
|}

==External links==
*  

 
 
 
 
 


 