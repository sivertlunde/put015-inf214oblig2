Kamini (film)
{{Infobox film 
| name           = Kamini
| image          =
| caption        =
| director       = Subair
| producer       = Anvar HH Abdulla Settu
| writer         = PP Subair Sreerangam Vikraman Nair Subair (dialogues)
| screenplay     = Subair Prema T. Raghavan T. S. Muthaiah
| music          = MS Baburaj Ashok Kumar
| editing        = Ramesh
| studio         = Chitrabharathi
| distributor    = Chitrabharathi
| released       =  
| country        = India Malayalam
}}
 1974 Cinema Indian Malayalam Malayalam film, Raghavan and T. S. Muthaiah in lead roles. The film had musical score by MS Baburaj.   

==Cast== Prema
*T. R. Omana Raghavan
*T. S. Muthaiah
*Alleppey Vincent Baby Sumathi
*Bahadoor
*Kuthiravattam Pappu
*Rani Chandra
*Roja Ramani

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Anwar Suber. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aashichakadavil || S Janaki, Anwar Suber || Anwar Suber || 
|-
| 2 || Manmadhanorukkum || K. J. Yesudas || Anwar Suber || 
|-
| 3 || Muralikayoothunna || S Janaki || Anwar Suber || 
|-
| 4 || Venna kondo || K. J. Yesudas || Anwar Suber || 
|}

==References==
 

==External links==
*  

 
 
 

 