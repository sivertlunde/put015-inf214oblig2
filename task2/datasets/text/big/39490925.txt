Cafe Metropole
{{Infobox film
| name           = Cafe Metropole
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Edward H. Griffith
| producer       = Nunnally Johnson
| writer         = Gregory Ratoff
| based on       = 
| screenplay     = Jacques Deval
| narrator       = 
| starring       = Loretta Young Tyrone Power Adolphe Menjou
| music          = David Buttolph Cyril J. Mockridge
| cinematography = Lucien N. Andriot
| editing        = Irene Morra
| distributor    = Twentieth Century Fox Film Corporation
| released       =  
| runtime        = 83 min.
| country        = United States
| language       = English language|English}}
 1937 Drama drama film directed by Edward H. Griffith, starring Loretta Young, Tyrone Power and Adolphe Menjou.

==Plot summary==
 

==Cast==
* Loretta Young as Laura Ridgeway 
* Tyrone Power as Alexis 
* Adolphe Menjou as Monsieur Victor 
* Gregory Ratoff as Paul 
* Charles Winninger as Joseph Ridgeway 
* Helen Westley as Margaret Ridgeway 
* Christian Rub as Maxl Schinner 
* Ferdinand Gottschalk as Monsieur Leon Monnet 
* Georges Renavent as Captain 
* Leonid Kinskey as Artist 
* Paul Porcasi as Police Official 
* André Cheron (actor)|André Cheron as Croupier (as Andre Cheron) 
* George Beranger as Hat Clerk (as Andre Beranger)

==External links==
*  
*  
*  

 
 
 
 
 


 