Moonfleet (1955 film)
{{Infobox film
| name           = Moonfleet
| image          = moonfleet_poster.jpg
| director       = Fritz Lang
| producer       = John Houseman
| writer         = Jan Lustig Margaret Fitts
| based on       =  
| starring       = Stewart Granger George Sanders Joan Greenwood Viveca Lindfors
| music          = Vicente Gómez Miklós Rózsa
| cinematography = Robert H. Planck
| editing        = Albert Akst MGM
| released       =   1960 (France)
| runtime        = 87 minutes
| budget         = $1,955,000 The Eddie Mannix Ledger’, Margaret Herrick Library, Center for Motion Picture Study, Los Angeles 
| gross          = $1,574,000  English
}}
 1955 Eastman Color film filmed in CinemaScope directed by Fritz Lang which was inspired by the novel Moonfleet by J. Meade Falkner,  although significant alterations were made in the characters and plot. 

A gothic melodrama set in England during the eighteenth century, the film is about John Mohune, a young orphan, played by Jon Whiteley, who is sent to the Dorset village of Moonfleet to stay with his mothers former lover, Jeremy Fox. Fox, played by Stewart Granger, is a morally ambiguous character, an elegant gentleman intimately involved with smugglers.   On the run from the law, Mohune and Fox must decipher a coded message in their pursuit of a fabulous diamond hidden long ago.

==Plot summary==
 

==Cast==
* Stewart Granger as Jeremy Fox
* George Sanders as Lord James Ashwood
* Joan Greenwood as Lady Clarista Ashwood
* Viveca Lindfors as Mrs. Minton
* Jon Whiteley as John Mohune
* Liliane Montevecchi as Gypsy
* Melville Cooper as Felix Ratsey
* Sean McClory as Elzevir Block
* Alan Napier as Parson Glennie
* John Hoyt as Magistrate Maskew
* Donna Corcoran as Grace
* Jack Elam as  Damen

==Production==
The movie was shot almost entirely on the MGM backlot, augmented by a few shots of the California coast. During filming, James Dean visited the set; Stewart Granger said his manner was rude and dismissive. 

==Reception==
The film was a critical and financial failure on release. According to MGM records the movie earned $567,000 in the US and Canada and $1,007,000 overseas. It made a loss of $1,203,000. 

The film was released in France in 1960 and had 917,219 admissions.   at Box Office Story 
==References==
 

== External links ==
*  
*  
*   at TCMDB
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 