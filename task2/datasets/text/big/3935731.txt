Samurai I: Musashi Miyamoto
 
{{Infobox film
| name           = Musashi Miyamoto
| image          = Samurai1_mm.jpg
| caption        = The cover of the Criterion Collection DVD.
| director       = Hiroshi Inagaki
| producer       = Kazuo Takimura
| writer         = Hideji Hōjō (play) Hiroshi Inagaki Tokuhei Wakao Eiji Yoshikawa (novel)
| starring       = Toshiro Mifune Rentarō Mikuni
| music          = Ikuma Dan
| cinematography =
| editing        = Toho Studios
| distributor    = Toho Studios
| released       =  
| runtime        = 93 minutes
| rating         =
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
| Followed By    = Samurai II: Duel at Ichijoji Temple
}} Japanese swordsman Miyamoto Musashi. The film won a Special/Honorary Award at the 1955 Academy Awards for outstanding foreign language film.

The other two films in the trilogy are   and  .

== Plot ==

Following the battle of Sekigahara, Takezo (Toshiro Mifune) and his friend Matahachi (Rentarō Mikuni) find themselves on the losing side. Instead of the grand victory and glory Takezo had anticipated, he finds himself a hunted fugitive, having to assist a severely injured Matahachi. The pair seek shelter with a widow and her daughter who unknown to them are connected to local brigands. The brigands soon show up and ask for tribute from what the women have stripped off dead samurai, and Takezo has to fight them off. Both women attempt to seduce Takezo but are rejected. The widow then tells Matahachi that Takezo tried to assault her and convinces him to escort her and her daughter to Kyoto. Matahachi agrees even though he loves (and is betrothed to) Otsu (Kaoru Yachigusa), a woman from his village.

Takezo thinks his friend Matahachi has deserted him, and as he goes home breaks through a road block injuring several of the local lords men manning it, and returns to his village to tell Matahachis family that he is still alive but will not reveal why Matahachi has not returned. Matahachis mother does not believe him and she is arrested for treason along with many members of her clan. There is a village wide search for Takezo, organized by the lord. Even after using his relatives as bait, the villagers cannot catch Takezo. Otsu, meanwhile gets a letter signed by the womans daughter saying that Matahachi has gone off with her and to forget him, which leaves her devastated.

Takezo is finally captured by the Buddhist priest Takuan Sōhō, who tells the lord that he must be allowed to use his methods to control him. The priest believes that he can straighten him out, but Takezo escapes with Otsus help. But he is soon tracked down and Otsu is captured, but he fights his way out. Takezo is then captured again by the priest when he goes to look for Otsu, after finding out that Otsu has been taken to Himeji Castle. He is tricked and locked in a room in the castle for three years.

The end of the film shows Takezo being released and granted his samurai name Musashi Miyamoto. He then leaves to search for enlightenment, leaving two messages for Otsu: "Soon I will be back" and "Forgive me."

== Historical background ==
The film begins in the year 1600 with Tokugawa Ieyasus victory in the battle of Sekigahara which cleared the path to the Shogunate for Tokugawa Ieyasu.  Though it would take three more years to consolidate the position of power over the other clans. The historical Miyamoto Musashi is believed to have fought in this battle.

== Cast ==
* Toshiro Mifune as Miyamoto Musashi aka Takezo
* Rentarō Mikuni as Honiden Matahachi
* Kuroemon Onoe as priest Takuan (Takuan Sōhō)
* Kaoru Yachigusa as Otsu
* Mariko Okada as Akemi
* Mitsuko Mito as Oko 
* Eiko Miyoshi as Osugi, Matahachis mother
* Akihiko Hirata as Seijuro Yoshioka
* Kusuo Abe as Temma Tsujikaze
* Eitaro Ozawa as Terumasa Ikeda
* Akira Tani as Kawarano-Gonroku
* Seijiro Onda as chief official
* Fumito Matsuo as petty official 1
* Masanobu Ôkubo as petty official 2
* Jiro Kumagai as villager  1
* Akira Sera as villager 2
* Yasuhisa Tsutsumi as villager 1
* Yutaka Sada as soldier 1
* Shigeo Kato as soldier 2
* Junichirō Mukai as soldier 3
* Kiyoshi Kamoda as roving warrior 1
* Michio Sakurai as roving warrior 2
* Kyoro Sakurai as roving warrior 3
* Masao Masuda as woodcutter
* Daisuke Katō
* Kanta Kisaragi
* Yoshio Kosugi
* Sōjin Kamiyama
* William Holden as narrator in the original US version (uncredited)

==External links==
* 
*  
*   by Bruce Eder
*   http://www.jmdb.ne.jp/1954/cd002800.htm

 
 
 

 
 
 
 
 
 
 
 
 
 
 