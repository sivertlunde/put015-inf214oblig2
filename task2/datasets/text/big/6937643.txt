Zielen Van Napels
 
{{Infobox film
| name           = Zielen Van Napels
| image          = Zielen Van Napels.jpg
| caption        = 
| director       = Vincent Monnikendam
| producer       = Sherman De Jesus   Cécile van Ejik
| writer         = 
| starring       = 
| music          = 
| cinematography = Melle van Essen
| editing        = Albert Markus
| distributor    = 1More Film
| released       =  
| runtime        = 
| country        = Netherlands
| language       = Italian
| budget         = 
| gross          = 
}}
Zielen Van Napels is a documentary film by director Vincent Monnikendam that was first released in the United States in a special preview at the National Gallery of Art in Washington, D.C. in 2005.
 Neapolitan society and is filmed primarily in local dialects and Italian. English subtitles were available during the opening preview at the NGA.

This documentary has frequented a number of the more renowned film festivals, including the International Film Festival Rotterdam and the 2004 Toronto Film Festival.

== External links ==
*  

 
 
 
 

 