Ragalaipuram
{{Infobox film
| name           = Ragalapuram
| image          = 
| alt            =  
| caption        = 
| director       = Mano
| producer       = Ken Media
| writer         = Mano
| starring       = Karunas Angana Roy Sanjana Singh
| music          = Srikanth Deva
| cinematography = Velraj, R. Mahalingam
| editing        = V. T. Vijayan, T. S. Jai
| studio         = Ken Media
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         =
| gross          =
}} Tamil drama film directed by Mano, which is a remake of the 1991 Malayalam film Aanaval Mothiram. The film features Karunas, Angana Roy and Sanjana Singh in the lead roles, with Kovai Sarala and Delhi Ganesh playing other pivotal characters. The film opened to mixed reviews upon release on October 18, 2013.  

==Cast==
 
* Karunas as Velu
* Angana Roy as Kalyani
* Sanjana Singh as SI Shwetha Bharath Reddy as SI Suresh 
* Delhi Ganesh 
* Manobala as Inspector
* Kovai Sarala
* Singampuli
* M. S. Bhaskar as Vincent Pawan as Burma Kumar
* Uma Padmanabhan as Velus Mother
* Ken Karunas
* Mayilsamy
* O. A. K. Sundar
* Kottaikumar
* Vaiyapuri as Insurance Policy agent
* Shakeela as Kuruvamma (Special Appearance)
 

==Production==
The film was first reported in February 2012, with debutant Mano chosen to direct the remake of the 1991 Malayalam film Aanaval Mothiram with Karunas selected to produce the venture and play the lead role while his son Ken made a cameo appearance in the film.  Angana Roy was selected to make her debut in the film as lead actress, though Vathikuchi ended up being her first release, owing to the films delay.  

==Soundtrack==
Soundtrack of the film was scored by srikanthdeva. Songs received mixed reviews upon release.
* Ragalapuram - VM Mahalingam, Grace Karunas
* Adi Devaloga - Febin Pillai
* Obamavum - Karunas
* Sudamani - Srikanth Deva, Grace
* Obamavum Remix - Karunas

==Release==
The film opened to poor reviews from critics. A critic from the Times of India noted "Ragalaipuram is a textbook example of how to make a lowest common denominator entertainer."  Sify.com wrote that "the Tamil remake is nowhere near the original, as director Mano’s jokes fall flat and the comic scenes are so predictable." 



==References==
 

==External links==
*  

 
 
 
 
 


 