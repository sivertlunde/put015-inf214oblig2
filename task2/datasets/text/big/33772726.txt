A Caixa
 
{{Infobox film
| name           = A Caixa
| image          = ACaixa1994Poster.jpg
| caption        = French poster
| director       = Manoel de Oliveira
| producer       = Paulo Branco
| writer         = Manoel de Oliveira Prista Monteiro (play)
| starring       = Luís Miguel Cintra Glicínia Quartin Ruy de Carvalho 
| music          = 
| cinematography = Mário Barroso
| editing        = Valérie Loiseleux Manoel de Oliveira
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = France Portugal
| language       = Portuguese
| budget         = 
}}

A Caixa is a 1994 Portuguese comedy film directed by Manoel de Oliveira. It was screened in competition at the 1994 Tokyo International Film Festival.

==Cast==
* Luís Miguel Cintra as Blind Man 
* Glicínia Quartin as Old Woman 
* Ruy de Carvalho as Taverner 
* Beatriz Batarda as Daughter 
* Diogo Dória as Friend 
* Isabel Ruth as Saleswoman 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 