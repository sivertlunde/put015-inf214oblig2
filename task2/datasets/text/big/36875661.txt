List of lesbian, gay, bisexual or transgender-related films of 1979
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1979. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==1979==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|-
|Army of Lovers or Revolt of the Perverts|| 1979 ||   ||  || Documentary||
|- Steno ||  || Comedy ||
|-
|Caligula (film)|Caligula|| 1979 ||   ||    || Historical drama||
|-
| || 1979 ||   ||    || Television film, drama||
|-
|Das Ende des Regenbogens|| 1979 ||   ||  || Drama||
|-
|Ernesto (film)|Ernesto|| 1979 ||   ||      || Drama||
|-
|Il était une fois un homosexuel|| 1979 ||   ||  || Adult ||
|-
|A Intrusa|| 1979 ||   ||  || Drama||
|-
|Manhattan (film)|Manhattan|| 1979 ||   ||  || Comedy, Drama||
|-
|Race dEp|| 1979 ||   ||  || Documentary ||
|-
|República dos Assassinos|| 1979 ||   ||  || Drama||
|-
|To Forget Venice|| 1979 ||   ||    || Drama || aka Dimenticare Venezia
|-
| || 1979 ||   ||   || Drama, romance ||  
|-
|We Were One Man|| 1979 ||   ||  || Drama || aka Nous étions un seul homme
|}

 

 
 
 