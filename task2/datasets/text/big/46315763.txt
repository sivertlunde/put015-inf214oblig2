Ticket of Leave (film)
{{Infobox film
| name =  Ticket of Leave 
| image =
| image_size =
| caption =
| director = Michael Hankinson
| producer = Anthony Havelock-Allan 
| writer =  Margaret McDonnell   Vera Allinson   Michael Hankinson
| narrator = George Merritt   Wally Patch
| music = 
| cinematography = Francis Carver 
| editing =  British and Dominions 
| distributor = Paramount British Pictures
| released = January 1936 
| runtime = 69 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} George Merritt. It was made as a quota quickie at Elstree Studios by the British subsidiary of Paramount Pictures.  A woman joins forces with a criminal after he robs her apartment|flat.  The title refers to the ticket of leave given to prisoners when they were released from jail.

==Cast==
*  Dorothy Boyd as Lillian Walters  
* John Clements as Lucky Fisher   George Merritt as Inspector Black  
* Max Kirby as Goodman  
* Wally Patch as Sergeant Knott  
* Enid Lindsey as Edith Groves  
* J. Neil More as Sir Richard Groves  
* Molly Hamley-Clifford as Old Rose

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
 

 
 
 
 
 
 
 
 
 

 