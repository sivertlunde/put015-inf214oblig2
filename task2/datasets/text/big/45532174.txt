Meeting in the Night
{{Infobox film
| name           = Meeting in the Night
| image          = 
| image_size     = 
| caption        = 
| director       = Hasse Ekman
| producer       = Hasse Ekman, Europafilm
| writer         = Hasse Ekman, Torsten Flodén
| starring       = Hasse Ekman Eva Dahlbeck Ulf Palme
| music          = Erik Baumann, Nathan Görling
| released       = 30 September 1946
| runtime        = 86 min
| country        = Sweden Swedish
}}
 Swedish film directed by Hasse Ekman.

==Plot summary==
The journalist Åke Bergström writes a critical article about prisons in Sweden. The magazines editor in chief disagrees with him and removes the article. He also fires Åke, who gets the idea that he should pretend to murder a friend to gain real knowledge about life inside the walls of a prison. What sounds like a clever but mad idea at first, soon turns out to be a real nightmare...

==Cast==
*Hasse Ekman - Åke Bergström
*Eva Dahlbeck - Marit Rylander
*Ulf Palme - Sune Berger
*Tord Bernheim - Svarten Peter Lindgren - Filarn
*Hugo Björne - direktör Rylander
*Elsa Widborg - Argonda
*Eivor Landström - Sonja
*Karin Alexandersson - Lovisa
*Sigge Fürst - Spacklarn
*Gösta Cederlund - Holmstedt, editor in chief 
*Carl Reinholdz - Lången Wiktor "Kulörten" Andersson - Darris
*Josua Bengtson - Eskil
*Charlie Almlöf - Carnival Barker at Gröna Lund
*Artur Rolén - Karlsson
*Tord Stål - Karl-Axel, Second Editor 
*Åke Engfeldt - Telegraph Worker 
*Arne Lindblad - Maitre d at Saltsjöholms hotel 
*Sten Hedlund - Detective 

==External links==
* 

 
 

 
 
 
 
 