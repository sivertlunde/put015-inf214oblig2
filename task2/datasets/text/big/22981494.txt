Put on Ice
 
{{Infobox film
| name           = Put on Ice
| image          = 
| caption        = 
| director       = Bernhard Sinkel
| producer       = Alf Brustellin Bernhard Sinkel Joachim von Vietinghoff
| writer         = Alf Brustellin Bernhard Sinkel
| starring       = Helmut Griem
| music          = 
| cinematography = Dietrich Lohmann
| editing        = Annette Dorn
| distributor    = 
| released       = May, 1980
| runtime        = 88 minutes
| country        = Germany
| language       = German
| budget         = 
}}
Put on Ice ( ) is a 1980 German thriller film directed by Bernhard Sinkel. It was entered into the 1980 Cannes Film Festival.   

==Cast==
* Helmut Griem - Lehrer Brasch
* Martin Benrath - V-Mann Körner
* Ángela Molina - Franziska Schwarz
* Friedhelm Ptok - Herr Sokolowski
* Hans-Günter Martens - Herr Schröder
* Meret Becker - Annaa
* Helga Koehler - Juliane Brasch
* Frank Schendler - Kapuste
* Thomas Kufahl - Schindler
* Peter Lustig - Schulleiter
* Hermann Steza
* Rudolf Unger
* Gerhard von Halem
* Jürgen Bieske
* Monika Hansen
* Michael Brennicke
* Michael Duffek

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 