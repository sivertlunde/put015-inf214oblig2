Propaganda (film)
{{Infobox film name           = Propaganda image          = PropagandaTheatricalPoster.jpg image_size     =  caption        = Theatrical Poster director       = Sinan Çetin producer       = Sinan Çetin Cemil Çetin Susa Kohlstedt writer         = Sinan Çetin Gülin Tokat  narrator       =  starring       = Kemal Sunal Metin Akpınar Meltem Cumbul Rafet El Roman Sinan Çetin music          = Sezen Aksu Nejat Özer Yavuz Çetin Erkan Oğur cinematography = Rebekka Haas editing        =  studio         = Plato Film distributor    = released       =   runtime        = 120 mins country        = Turkey language       = Turkish budget         =  gross          =  preceded_by    =  followed_by    = 
}}

Propaganda is an award-winning 1999 Turkish comedy film written, directed and produced Sinan Çetin.  The film, which is a darkly surreal comedy set in a sleepy village in the southeast Turkey in 1948, starred popular comedy actor Kemal Sunal, who died a year later in 2000. It was shown in competition at the 18th Istanbul International Film Festival and the 4th Shanghai International Film Festival, where it won the Golden Goblet, and went on general release across Turkey on  .

==Production==
The film was shot on location in Hatay Province|Hatay, Turkey.   

==Synopsis==
Based on a true story set in 1948, customs officer Mehti is faced with the duty of formally setting up the border between Turkey and Syria, dividing his hometown. He is unaware of the pain that will eminently unfold, as families, languages, cultures and lovers are both ripped apart and clash head on in a village once united.

==Cast==
* Kemal Sunal as Mehdi
* Metin Akpınar as Rahim
* Meltem Cumbul as Filiz
* Rafet El Roman as Âdem
* Meral Orhonsay as Şahane
* Ali Sunal as Mahmut
* Nazmiye Oral as Azamet
* Müge Oruçkaptan as Nazmiye
* Berfi Dicle as Melek
* Kenan Baydemir as Hamdi
* Nail Kırmızıgül as Kopuk Yaşar
* Turgay Aydın as Cemil
* Cem Safran as Abuzer
* Zaven Çiğdemoğlu as Deli Selami
* Baycan Baybur as Komiser Muavini
* Ayşegül Yurdakul as Köylü Kadın
* Cengiz Deveci as Belediye Başkanı
* Özcan Pehlivan as Asker
* Bahar Uçan as Şadiye
* Sultan Yılmaz as Rahimin Annesi
* Ahmet Tok as Genç Köylü
* Üzeyir Tok as Selcuk
* Turgut Giray as Cemil
* Sinan Çetin as Sheakespeare

==Release==
===Festival Screenings===
* 4th Shanghai International Film Festival (October 22–31, 1999)
* 18th Istanbul International Film Festival

==Reception==
===Awards===
* Won: 4th Shanghai International Film Festival Golden Goblet
* Won: Bogey Award

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 
 