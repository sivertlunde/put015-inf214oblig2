The Sensorium
 
The Sensorium is regarded the worlds first commercial 4D film and was first screened in a Six Flags theme park in Baltimore in 1984.   It was produced in partnership with Landmark Entertainment.  

==Description==
The 4D film included multiple track discrete sound system, bodysonic seats and Scent-a-Vision (a series of smells released in sync with the film). The story told a series of American pastimes around the turn of the twentieth century and was narrated by a brilliant inventor named Phineaas Flagg. The film used the ArriVision over/under 3D film system. The film was only showing at Six Flags Power Plant theme park.    

==See also==
*Sensorama - one of the earliest known prototype of an immersive, multi-sensory technology

==References==
 

 
 
 
 
 
 
 
 

 