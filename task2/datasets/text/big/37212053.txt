The Victim (2012 film)
 
 
{{Infobox film
| name = The Victim
| image = The_Victim_Konkani_Film_Poster.jpg
| caption = Film poster
| director = Milroy Goes
| assistant director = Rohan Austin Goes
| producer = Jerryton A. Dias
| writer = Jerryton A. Dias
| starring = Kunal Malarkar Prajkta Biliye Ragesh B Asthanaa Deepraj Rana Razak Khan Princy Gupta Apoorva Sharma Jitu Jadhav Kevin Demello Bond Braganza Roma Sarita Vogt
| music = Anna Rebelo e Gomes  (songs)  D&A Productions  (background score) 
| cinematography = Julius Dmello, Anthonio Micheal
| editing = Anna Rebelo e Gomes, Tinu
| studio = D&A Productions, Vasco
| 1st AD = Rohan Austin Goes
| released =  
| runtime = 75mins
| country = India
| language = Konkani
| budget = 1.5 crore
| gross = 
}} Konkani Cinema with Bollywood actors that was shot entirely on Canon EOS 7D.  Starring Kunal Malarkar, Prajkta Biliye and Deepraj Rana. Razak Khan does the role of a Bus conductor. Assistant Director Rohan Austin Goes.  The supporting cast includes Deepraj Rana, Ragesh B Asthanaa, Princy Gupta, Prajkta Biliye and Kunal Malarkar. Produced by Jerryton Dias under the banner of Diasons Ventures, the film is set and shot completely in Goa. It premiered at Inox, Goa on 3 August 2012.  the film was screened in Karwar, Karnataka on 14 September 2012 after successful premiere and 2 weeks screening in Goa. 

==Plot==
The film tells the life story of Ashley, character portrayed by Kunal Malarkar. How he is bound to look after two families and fall a victim.

It is said that Marriages are made in Heaven; but the saddest thing that nobody discussed was, that the doubt created in mind can ruin a happy married life. It has been happening in most recent marriages, many a time they fail. This is a story of doubt, trust, responsibilities of a family man. How a doubt can victimized a family and how children suffer. The story revolves around two good friends King Deepraj Rana and Ashley (Kunal Malarkar) where one dies in an accident while accomplishing a task given by his friend Ashley. Even though Ashley is married, he is obliged to give homely support to his friends wife Veronica (Jitu Jadhav) and her daughter.

This leads to a situation beyond repairs with Ashley (Kunal Malarkar) and his wife Sonia (Princy Gupta).
Ashley ends up being Partially paralyzed and Deaf as Sonia decides to take the kids and walk away and when Sonia realizes Ashley is innocent, its a little too late.

The inevitable phase in his life takes a drastic turn where fate leads his personal life beyond repairs.

==Cast==
* Kunal Malarkar as Ashley
* Prajkta Biliye as Sis Melissa
* Deepraj Rana as King
* Razak Khan
* Ragesh B Asthanaa as Lawyer
* Keshav Nadkarni as Lawyer
* Princy Gupta as Sonia
* Apoorva Sharma
* Kevin Demello
* Jitu Jadhav
* Jerryton Dias in guest appearance

==Production and release==
The film has been scripted by Jerryton A. Dias. The film was entirely shot in Goa where the film is set. The editing and post production was entirely done in Goa at D&A Productions.  

==Soundtrack==
Composed by Anna Rebelo e Gomes, the music album comprises six tracks penned and tuned by Jerryton A. Dias with the background music of D&A Productions, Vasco. The music has been released online on CD Baby. The track list includes Peppy Love Song "Kallzam Amchim" by Elston Pereira feat. Hema Sardesai, "Mog" by Natania Baptista, "Visvas" by Veeam Braganza, "Jivit Mojem" by Nephie Rod, "Noxib" and "Sonvsar" by Anthony Fernandes.

==References==
 

==External links==
*  

 
 
 
 
 
 