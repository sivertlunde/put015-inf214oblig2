Blackenstein
 

{{Infobox Film
| name           = Blackenstein
| image          = Black frankenstein.jpg
| caption        = Film poster using alternative title Black Frankenstein
| director       = William A. Levey
| producer       = Frank R. Saletri
| eproducer      = Ted Tetrick
| aproducer      =
| writer         = Frank R. Saletri John Hart Ivory Stone Andrea King Roosevelt Jackson Joe De Sue Nick Bolin Liz Renay Walco Productions
| cinematography = Robert Caramico
| editing        = William A. Levey
| distributor    = Exclusive International
| released       = 1973
| runtime        = 87 minutes
| country        =  
| awards         = English
| budget         = $80,000  
| gross          = $2,000,000  
| preceded_by    =
| followed_by    =
}}
Blackenstein, also known as Black Frankenstein, is a low budget 1973 blaxploitation horror film loosely based on Mary Shelleys Frankenstein. It was made in an attempt to cash in on the success of Blacula, released the previous year by American International Pictures. However, Blackenstein fared poorly in comparison to its predecessor, with most reviews agreeing that the movie was "a totally inept mixture of the worst horror and blaxploitation films". 

==Plot== John Hart) who has recently won a Nobel Peace Prize for "solving the DNA genetic code".

In a tour of Doctor Steins castle-like Los Angeles home, Winifred is introduced to his other patients: the ninety-year-old Eleanor who looks to be only fifty (Andrea King) thanks to Steins treatments, and the bald Bruno (Nick Bolan) whose lower legs have been successfully re-attached via "laser beam fusion" and Steins "DNA solution".

Winifred is startled when she sees one of Brunos legs is tiger-striped, which Doctor Stein attributes to "an unknown RNA problem" which he hopes to correct during the course of treatment. His sinister black assistant Malcomb (Roosevelt Jackson) seems overly interested in her reaction to this sight and in her in general.

Meanwhile, the stoically suffering Eddie is being verbally abused by an obnoxious white orderly (Bob Brophy) at the local Veterans Health Administration|Veterans Hospital. When Doctors Stein and Walker arrive to ask if hed be interested in submitting to experimental limb transplant surgery that could correct his condition, he consents.
 devolve into a primitive brutish state with hairy hands and a Neanderthal brow ridge.
 monster with a squarish afro instead of the usual scars and neck bolts.

Although he lies in a near catatonic state by day, compelled by horrible cannibalistic urges the black suit and turtleneck-clad Eddie secretly leaves the house late each night in search of victims who he dismembers, disembowels and devours zombie-style, always returning in time each morning for his ongoing schedule of DNA injections with his doctors none the wiser.

Two police detectives visit Doctor Stein as the body count starts to rise (their suspicions aroused due to the fact that all the killings took place in the surrounding vicinity and that the abusive hospital orderly was the vengeful Eddies first victim), but Stein is ignorant of the fact that there is now a murderous monster living in his basement laboratory. Winifred however has become suspicious of Malcomb and spends her time in the lab, examining the various solutions used during Eddies surgery.

One night, returning from his usual senseless rampage, Eddie hears screaming coming from Winifreds room. He enters to find Malcomb at her bedside and interrupts the attempted rape. Malcomb grabs a gun and empties it into the unaffected Eddie as Winifred flees.  Eddie strangles Malcomb and then goes on to kill Bruno and Eleanor, the latter aging rapidly as she dies.

Doctor Stein meets Winifred on the stairs, where she tells him Eddie is the monster. Together they down run to the lab.

Winifred busies herself preparing an injection of the DNA solution that she hopes will cure Eddie. When Eddie draws near, he seems moved by her terror and backs away, perhaps dimly remembering that she is his fiancee. Doctor Stein however attacks him from behind, provoking a violent response. After a brief tussle with his creator that ends with Stein being fatally knocked into the high voltage electrical equipment, Eddie leaves the house.
 Canine Corps, Dobermans surround Eddie, knock him to the ground and, with a fittingly macabre irony, viciously tear the monster to pieces in the same way he killed his victims. 

==Cast== John Hart as Dr. Stein
* Ivory Stone as Dr. Winifred Walker
* Joe De Sue as Eddie Turner
* Roosevelt Jackson as Malcomb
* Andrea King as Eleanor
* Nick Bolin as Bruno Stragor
* Karin Lind as Hospital Supervisor
* Yvonne Robinson as Hospital Receptionist
* Liz Renay as Blond Murder Victim

==Frank R. Saletri==
 criminal lawyer cult icon Liz Renay.

Saletri also wrote, produced and directed the never-released Black the Ripper  and wrote the screenplays for two unmade Sherlock Holmes movies, Sherlock Holmes in the Adventures of the Werewolf of the Baskervilles and Sherlock Holmes in the Adventures of the Golden Vampire which was to star Alice Cooper as Dracula. 

In 1982 Saletri was found murdered "gangland style" in his home, a mansion formerly owned by Bela Lugosi.  

==Trivia==

Despite all the talk of DNA and laser surgery, the movies laboratory set uses Kenneth Strickfadens original sparking and zapping electrical equipment from the 1931 Frankenstein (1931 film)|Frankenstein film. 

Except for the soulful songs written and sung by Cardella DiMilo, the musical score consists of stock music taken from classical composers and old horror movies. 

Surprisingly for a blaxploitation movie of this time period and despite its title, Blackenstein features little, if any, overt displays of racism, with even the angry tirade the white orderly directs toward the bedridden Eddie motivated more by bitter jealousy about not being able to join the army than any form of bigotry.

Several sequels were announced by various producers, including The Fall of the House of Blackenstein and Black Frankenstein Meets the White Werewolf,  but were never made. The Return Of Blackenstein is not a sequel but merely a retitled re-release of the original film. 

The Mexican lobby card for Blackenstein is actually "swipe (comics)|swiped" from the American poster for the 1965 Japanese kaiju flick Frankenstein Conquers the World with the title loincloth-clad monster repainted brown. 

==Other Appearances==

Blackenstein has appeared in skits on Saturday Night Live and on MADtv where he was re-dubbed "Funkenstein".

The movie was referenced in the 2003 South Korean film Save the Green Planet!. 

Blackenstein is given a surprisingly serious scholarly examination as an aspect of a larger cultural perspective in Elizabeth Youngs Black Frankenstein: The Making of an American Metaphor published in 2009 as part of the America and the Long 19th Century series from New York University Press. 

==Home Video Release==

Blackenstein was first released on video in the early eighties by Media Home Entertainment  and again on DVD and VHS by Xenon Pictures in 2003.

==See also==

* List of films featuring Frankensteins monster

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 