Maniac Cop 2
{{Infobox film
| name           = Maniac Cop 2
| image          = Maniacop2pos1.jpg
| alt            = 
| caption        = Film poster
| director       = William Lustig
| producer       = Larry Cohen
| writer         = Larry Cohen Michael Lerner Bruce Campbell Laurene Landon Robert ZDar Clarence Williams III Leo Rossi
| music          = Jay Chattaway
| cinematography = James Lemmo
| editing        = David Kern
| studio         = Medusa Pictures   Fadd Enterprises   Overseas FilmGroup   The Movie House Sales Company
| distributor    = Metropolitan Filmexport
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $4,000,000  
| gross          = 
}}
 action horror Michael Lerner  and Bruce Campbell.

== Plot ==

After being impaled by a pipe and plunging into a river at the end of the previous film, the undead Maniac Cop Officer Matthew Cordell acquires a junked police cruiser and continues his killing spree through New York. Finding a convenience store in the middle of a robbery, he kills the clerk; the thief is subsequently killed in a shootout with police. As Cordell stalks the streets, his enemies Officers Jack Forrest and Theresa Mallory are put back on duty by Deputy Commissioner Edward Doyle, who has the two undergo a psychiatric evaluation under Officer Susan Riley. While Jack is content that Cordell is long gone and wants to go on with his life, Theresa is convinced that Cordell is still alive and plotting his revenge.

At a newsstand, Jack is stabbed through the neck by Cordell, which leaves Theresa distraught and prompts her to appear on a talk show to inform the public about Cordell, as the police have kept Cordells supposed return covered up, as Commissioner Doyle was involved in originally framing Cordell and sending him to Sing Sing. While en route to a hotel in a taxi, Theresa is joined by Susan, and the two are attacked by Cordell, who kills the cabbie and forces Susan and Theresa off the road. After handcuffing Susan to the steering wheel of a car and sending her into the busy streets, Cordell kills Theresa by snapping her neck. Gaining control of the car, Susan crashes and is found and given medical attention.

Elsewhere, a stripper named Cheryl is attacked in her apartment by Steven Turkell, who has strangled at least six other exotic dancers. As Turkell brutalizes Cheryl, Cordell arrives, disposes of a pair of officers earlier called by Cheryl, and helps Turkell escape. Grateful for the help, Turkell befriends Cordell and takes him back to his apartment, where Cordell stays for a short while. After Cordell leaves, Turkell goes out to find another victim but is identified at a strip club by Cheryl. He is arrested and placed in a holding cell by Susan and Detective Lieutenant Sean McKinney.

Turkell taunts Susan, telling him Cordell will break him out. Turkells assumption proves correct, as Cordell breaks into the police station and massacres the bulk of the police force in a hail of gunfire. Using Susan as a hostage, Turkell, Cordell, and another criminal named Joseph Blum hijack a prison bus and head to Sing Sing, where Turkell believes Cordell wants to free all the inmates and create an army of criminals. McKinney and Doyle follow, and McKinney convinces Doyle to reopen Cordells case and rebury his casket with full honors on the assumption that this will appease Cordell.

Cordell bluffs his way into the prison using Blums paperwork, and the others kill a guard for his keys. Shortly after entering death row, Cordell is contacted over the prison PA system by Doyle, who admits to Cordell that he was set up and states that his case has been reopened. After hearing Doyles announcement, Cordell abandons Turkell, Blum, and Susan and heads deeper into the prison, where he is attacked with a Molotov cocktail by the three inmates who originally mutilated him. While burning, Cordell kills the three convicts and assaults the other prisoners, only to be attacked by Turkell, who realizes Cordell used him. As Cordell and Turkell fight, the two crash through a wall, fall onto the bus below, and seemingly die when the vehicle explodes.

Sometime later, Cordell is buried with full honors alongside other fallen officers; Susan and McKinney attend his funeral. As Cordells casket is lowered, McKinney throws Cordells badge into the grave, leaves with Susan, and delivers a monologue about how there is a little bit of Cordell in every officer, and that every member of the force needs to rise above becoming a Maniac Cop. Before the credits roll, Cordells hand bursts through the lid of his casket and grabs his badge.

== Cast ==

* Robert Davi as Detective Lieutenant Sean McKinney
* Claudia Christian as Officer Susan Riley Michael Lerner as Deputy Commissioner Edward Doyle
* Bruce Campbell as Officer Jack W. Forrest, Jr.
* Laurene Landon as Officer Theresa Mallory
* Robert ZDar as Officer Matthew Cordell
* Clarence Williams III as Joseph T. Blum
* Leo Rossi as Steven Turkell
* Paula Trickey as Cheryl
* Sam Raimi as Newscaster
* Danny Trejo as Prisoner

== Release ==

Maniac Cop 2 was released direct-to-video in the United States.   

Blue Underground gave the film a limited theatrical release in the United States in October 2013, which was followed by the Blu-ray and DVD release on November 19, 2013. 

== Reception ==
 Cohen specialty."   Ty Burr rated the film C+ and called it a "brutal, stupid" zombie film in which "the style almost redeems the sleaze."   Michael Gingold of Fangoria rated it 3.5/4 stars and called it Lustigs best film.   Anthony Arrigo of Dread Central rated it 4/5 stars and wrote, "Maniac Cop 2 embodies all of the excess that made ‘80s horror sequels so great".   Mike Pereira of Bloody Disgusting rated it 4/5 stars and wrote, "Maniac Cop 2 delivers a fresh experience while all along staying true to what fans dig about the original." 

William Lustig considers Maniac Cop 2 to be his best film, saying, "It was the film   I felt as though myself and my crew were really firing on all cylinders. And I think we made a terrific B-movie", and also thinks its superior to the first Maniac Cop film. 

== References ==

 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 