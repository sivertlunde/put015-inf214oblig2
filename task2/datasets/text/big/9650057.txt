Nenjinile
{{Infobox film
| name = நெஞ்சினிலே Nenjinile
| image = Nenjinile DVD Cover.jpg
| caption = DVD Cover
| director = S. A. Chandrasekhar
| writer = A. C. Jairam (dialogues)
| screenplay = S. A. Chandrasekhar
| story = A. C. Jairam Vijay Eesha Isha Koppikar
| producer = S. A. Chandrasekhar
| editing = B. S. Vasu Saleem
| cinematography = S. D. Vijay Milton
| studio = V. V. Creations
| distributor = Deva
| released = June 25, 1999
| runtime = 139 minutes
| country = India Tamil
| budget = 
}}
 Tamil action Vijay and Isha Koppikar Sriman and Manivannan play supporting roles.  The films music is composed by Deva (music director)|Deva, and the film opened in June 1999 to a mixed response at the box office.

==Plot==
The story starts with Karunakaran (Vijay) leaving his village to go to Mumbai looking for a job to help his sisters wedding. He lives with his sister and there he meets Nisha (Isha Koppikar), who loves him at first sight. He first rejects her but later he starts to love her too. Hes unable to get a job, and through a friend, joins as a hit man for a gangster. This brings him a lot of money, in this way he can help his family. But when his own gang plan to kill Nisha after killing her parents, he tries to protect her, earning their wrath. Whether he can save Nisha or not forms the crux of the story

==Cast==
  Vijay as Karunakaran Isha Koppikar as Nisha
*Manivannan as Mani
*Sonu Sood Rami Reddy
*Devan
*Srividya Sriman
*Nizhalgal Ravi
*Sathyapriya Roja in a special appearance
 

==Production== Vijay recommended Isha Koppikar Roja making a special appearance in the film.  For another song in the film, lyricist Arivumathi was asked to write a song in praise of Vijay – unable to inspire himself to do so, the lyricist later wrote a song around the success of Rajinikanth, hoping it would also adapt to Vijays rise.

During the post-production stages, S. A. Chandrasekhar accused the son of veteran director K. Balachandar of trying to make pirate copies of the film. The allegations prompted Vijay to pull out of a film he had agreed to act in under Balachandars production house. 

==Release==
The film opened to negative reviews, with the critic of Indolink.com claiming the film Chandrasekhar "screwed up the storyline part in a very major way" while mentioning that the only respite was the films music.  The film became a financial failure at the box office and triggered a run of continuous unsuccessful films for Vijay such as Minsara Kanna

==Soundtrack==
{{Infobox album|  
  Name        = Nenjinile
|  Type        = Soundtrack Deva
|  Cover       =
|  Released    = 1999
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       = Five Star Audio
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Nenjinilea (1999)
|  Next album  = 
}}
The soundtrack of the film was composed by Deva (music director)|Deva, was well received by the audience. The lyrics were written by Vaali (poet) |Vaali, Pazhani Bharathi, Ravi Shankar, Kalaikumar, Vijayan, A. C. Jairam.

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Manase Manase
| extra1          = Unnikrishnan, K. S. Chithra 
| length1         = 5:36
| title2          = Anbe Anbe Hariharan
| length2         = 5:27
| title3          = Prime Minister Harini
| length3         = 5:49
| title4          = Thanganiram 
| extra4          = Vijay (actor)|Vijay, Swarnalatha  
| length4         = 5:04
| title5          = Madras Dhost
| extra5          = Krishnaraj, Naveen 
| length5         = 5:39
| title6          = Manasaey
| extra6          = Hariharan (singer)|Hariharan, Sadhana Sargam 
| length6         = 5:34
}}

==References==
 

 
 
 
 