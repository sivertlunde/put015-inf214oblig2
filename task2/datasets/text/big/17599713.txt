The Italian Straw Hat (film)
 Un chapeau de paille dItalie}}

{{Infobox film
| name           = Un chapeau de paille dItalie The Italian Straw Hat
| image          = Un chapeau_de_paille_d_italie.jpg
| image_size     =
| caption        =
| director       = René Clair
| producer       = Alexandre Kamenka
| writer         = Play: Eugène Marin Labiche|Eugène Labiche Marc Michel Screenplay: René Clair
| narrator       =
| starring       = Albert Préjean Olga Tschechowa
| music          =
| cinematography = Maurice Desfassiaux Nikolas Roudakoff
| editing        =
| distributor    = Films Albatros
| released       = (France) 13 January 1928 (USA) 31 August 1931
| runtime        = 105 minutes (19f/s)
| country        = France
| language       = Silent
| budget         =
}}
 1928 French silent film comedy directed by René Clair, and based on the 1851 play Un chapeau de paille dItalie by Eugène Marin Labiche|Eugène Labiche and Marc Michel. 

==Plot==
On the day of Fadinards wedding, his horse eats a ladys hat on a bush at the roadside, while the lady is hidden behind the bush with her lover Lieutenant Tavernier. Because she is married, she cannot return home hatless without being compromised, and Tavernier orders Fadinard to replace the hat with one exactly like it - or else he will wreck his new home. In an elaborate sequence of complications, Fadinard tries to find a hat while keeping to his marriage schedule.

==Cast==
* Albert Préjean as Fadinard
* Geymond Vital as Lieutenant Tavernier
* Olga Tschechowa as Anaïs de Beauperthuis, the compromised wife
* Paul Ollivier as Vésinet, the deaf uncle
* Alex Allin as Félix, the valet
* Jim Gérald as Beauperthuis
* Marise Maia as Hélène, the bride
* Valentine Tessier as a customer in the milliners shop
* Louis Pré, fils, as Bobin, the cousin with one glove
* Alexis Bondireff as the cousin with a crooked tie
* Alice Tissot as his wife
* Yvonneck as Nonancourt, the brides father

==Production==
When René Clair was first offered the job of adapting the farce of Labiche and Michel for the cinema by Alexandre Kamenka, of the Albatros film company, he was unenthusiastic, but he nevertheless demonstrated a sure touch for a fast-moving satirical portrayal of bourgeois style and manners. He updated the original play from 1851 to the setting of the Belle Époque, and an opening title dates it specifically to 1895, the year of the birth of the cinema. In addition to the period settings designed by Lazare Meerson, the story is filmed in a style which recalls the techniques of the earliest cinema films.   Most shots use a fixed camera which does not move with the action; characters walk in and out of shot from the sides. Set-ups are usually in long shot, with the characters backlit to provide contrast with the background; much of the action is built up by details within the shot.  Close-ups are comparatively rare.  (Only in occasional sequences do we see tracking shots, to follow a moving carriage, or rapid cutting, for instance to suggest Fadinards growing panic in a dance scene.) 

The verbal dexterity of the original text is replaced with inventive visual comedy. Each supporting role is characterised by a comic detail which becomes a running joke: the deaf uncle with a blocked-up ear-trumpet; the cousin who has lost one white glove; the brides father whose dress shoes are a size too small; the bride who feels a pin that has dropped down the back of her dress; the cousin whose tie keeps dropping, and his wife whose pince-nez will not stay on her nose. The visual narrative is made largely self-sufficient, and there are comparatively few intertitles throughout the film. 

==Reception==
On its first release, the film was not particularly successful with the French public, and its initial run in Paris lasted for only three weeks. It was however very positively received by the critics, and it proved to be one of the most durable of French silent films.    By transposing the action to the 1890s when the play had found particular success with audiences, the film was seen as a satire of the play itself and of the kind of audience that would have enjoyed it.   

"With this first big success (artistic more than commercial), René Clair, at the age of 30, achieved his maturity, and established, through his meeting with Labiche, his themes and a style." 

==Musical accompaniment==
In 1952 Georges Delerue wrote a music score to accompany a screening of the silent film.  The British composer Benedict Mason also composed an orchestral score for the film, into which he incorporated Jacques Iberts Divertissement which that composer had based on his own incidental music for a performance of the original play by Labiche and Michel in 1929.

For the DVD edition of The Italian Straw Hat by Flicker Alley (2010), an orchestral score compiled by Rodney Sauer and performed by the Monte Alto Motion Picture Orchestra was provided, together with an alternative piano accompaniment by Philip Carli.

==Adaptation==
On May 1, 2005, a two-act ballet, An Italian Straw Hat, to a libretto by Timothy Luginbuhl, was premiered by the National Ballet of Canada. The choreography was created by James Kudelka, the score composed by Michael Torke.

==Alternative titles==
The film is sometimes referred to by the more exact translation of An Italian Straw Hat. It was variously distributed in the United States as The Italian Straw Hat or The Horse Ate the Hat. (Other languages:  ,  ,  ,  ).)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 