Pink Flamingos
 
 
{{Infobox film
| name           = Pink Flamingos
| image          = Pink Flamingos (1972).jpg
| image_size     = 215px
| alt            = An obese drag queen stands center stage, holding a gun as the title is above her, with the tagline "An exercise in poor taste"
| caption        = Theatrical release poster
| director       = John Waters
| producer       = John Waters
| writer         = John Waters
| narrator       = John Waters Divine David Lochary Mink Stole Mary Vivian Pearce Danny Mills Edith Massey
| cinematography = John Waters
| editing        = John Waters Dreamland Saliva Films
| distributor    = Saliva Films New Line Cinema
| released       =  
| runtime        = 92 minutes 107 minutes      
| country        = United States
| language       = English
| budget         = $10,000
}} transgressive black comedy exploitation film written, produced, scored, shot, edited, narrated, and directed by John Waters.    When the film was initially released, it caused a huge degree of controversy due to the wide range of perverse acts performed in explicit detail. It has since become one of the most notorious films ever made and made an underground star of the flamboyant drag queen actor Divine (actor)|Divine. The film co-stars David Lochary, Mary Vivian Pearce, Mink Stole, Danny Mills, Cookie Mueller, and Edith Massey.

Produced on a budget of only $10,000, it was mostly shot on weekends in Phoenix, Maryland, a suburb of Baltimore. After screenings at universities across the U.S. including Harvard College in 1973, the film was distributed theatrically by Saliva Films,    and later by New Line Cinema and became a nationally known film.   
 compact disc), scenes cut from the original release. The re-release was rated MPAA rating system|NC-17 by the Motion Picture Association of America; this edition was later released on DVD.

Like Rocky Horror Picture Show, when shown in a theater it is usually at 11 PM or later.

The film came in at #29 on the list of 50 Films to See Before You Die on a show in the United Kingdom.

Empire Magazine|Empire listed it as the 31st-greatest independent film ever made. 

==Plot== Divine lives trailer on plastic pink tabloid paper, jealous rivals Connie and Raymond Marble set out to destroy her career but come undone in the process.
 exposing himself crushing a looks on through a window. Cookie then informs the Marbles about Babs real identity, her whereabouts, and her family, as well as information about her upcoming birthday party.

The Marbles send a box of human feces to Divine as a birthday present with a card addressing her as "Fatso" and proclaiming themselves "The Filthiest People Alive". Worried her title has been seized, Divine proclaims whoever sent the package must die and her two associates agree. Meanwhile, at the Marbles, Channing dresses up as Connie and Raymond, wearing Connies clothes and imitating their earlier overheard conversations. When the Marbles return home, they catch Channing in the act and respond with outrage, firing him and locking him in a closet until they can return from their tasks and kick him out for good. 
 prolapsed anus eat them.
 emasculate Channing offscreen.

Meanwhile, Connie and Raymond burn Divines beloved trailer to the ground. Upon their return home they are troubled by the behavior of their furniture. Having been "cursed" by being licked by Divine and Crackers, the furniture "rejects" the Marbles when they return home: when they try to sit, the cushions fly up, throwing them to the floor. They then find that Channing has bled to death from his castration and the two girls have escaped. 

After finding the remains of the burned-out trailer, Divine and Crackers return to the Marbles home. They take them hostage at gunpoint and return to the site of the trailer. Divine calls the local tabloid media to witness the Marbles trial and execution, as she proclaims her belief in "filth politics":
 
 tar and homosexual scandal a few years previously. Gerassi, John, with introduction by Peter Boag (1966, reprinted 2001). The Boys of Boise: Furor, Vice and Folly in an American City.. University of Washington Press. ISBN 0-295-98167-9. 
 defecates on takes the feces in her hand and puts them in her mouth, proving, as the narrator states, that she is "not only the filthiest person in the world, but is also the worlds filthiest actress". She gags twice and grins at the camera.

==Cast==
* John Waters (narrator) as Mr. J Divine as Divine / Babs Johnson
* David Lochary as Raymond Marble
* Mink Stole as Connie Marble
* Mary Vivian Pearce as Cotton
* Danny Mills as Crackers
* Edith Massey as Edie
* Cookie Mueller as Cookie
* Channing Wilroy as Channing
* Paul Swift as The Egg Man Susan Walsh as Suzie
* Linda Olgierson as Linda Hitler
* Steve Yeager as Nat Curzan
* George Figgs as Bongo player Chick with a dick

==Production==
Divines friend Bob Adams described the trailer set as a "hippie commune" in Phoenix, Maryland, and noted that they were operating out of a farmhouse that didnt have any hot water. Adams noted however that ultimately Divine and Van Smith decided to start sleeping at Susan Lowes home in the city, and that they would get up before dawn to put on Divines makeup before being driven to the set by Jack Walsh. As Adams related, "Sometimes Divy would have to wait out in full drag for Jack to pull the car around from back, and cars full of these blue-collar types on their way to work would practically mount the pavement from gawking at him." 

Divines mother, Frances, later related that she was surprised that her son was able to endure the "pitiful conditions" of the set, noting his "expensive taste in clothes and furniture and food". 

Waters has stated that  , could be her exact twin, only heavier. Isabel, you inspired us all to a life of cheap exhibitionism, exaggerated sexual desires and a love for all that is trash-ridden in cinema." 

===Music=== soundtrack CD in 1997 on the 25th anniversary release of the film on DVD. 
# "The Swag" – Link Wray and His Ray Men The Centurions Jim Dandy" – LaVern Baker
# "Im Not a Juvenile Delinquent" – Frankie Lymon and the Teenagers The Girl Cant Help It" – Little Richard
# "Joeys Song|Ooh! Look-a-There, Aint She Pretty?" – Bill Haley & His Comets
# "Chicken Grabber" – The Nighthawks
# "Happy, Happy Birthday Baby" – The Tune Weavers
# "Pink Champagne" – The Tyrones
# "Surfin Bird" – The Trashmen The Robins
# "(How Much is) That Doggie in the Window" – Patti Page
 Sixteen Candles", which appeared in the original 1972 cut of the film; for the 1997 reissue, "Sixteen Candles" could not be used in the film or the soundtrack due to copyright problems. The original release had also used a brief excerpt of Igor Stravinskys The Rite of Spring, which was removed for the re-release.  

==Release== premiere in late 1972 at the third Annual Baltimore Film Festival, held on the campus of the University of Baltimore, where it sold out tickets for three successive screenings; the film had aroused particular interest among fans of underground cinema following the success of Multiple Maniacs, which had begun to be screened in cities such as New York, Philadelphia, and San Francisco. 

  in 2009; this building formerly housed the Elgin Theater, where Pink Flamingos was screened as a midnight movie in the early 1970s.]]
Being picked up by the then-small independent company New Line Cinema, Pink Flamingos was later distributed to Ben Barenholtz, the owner of the Elgin Theater in New York City. At the Elgin Theater, Barenholtz had been promoting the midnight movie scene, primarily by screening Alejandro Jodorowskys acid western film El Topo (1970), which had become a "very significant success" in "micro-independent terms". Barenholtz felt that being of an avant-garde nature, Pink Flamingos would fit in well with this crowd, subsequently screening it at midnight on Friday and Saturday nights. 

The film soon gained a cult following of filmgoers who would repeatedly come to the Elgin Theatre to watch it, a group Barenholtz would characterize as initially composing primarily of "downtown gay people, more of the hipper set", but, after a while Barenholtz noted that this group eventually broadened, with the film becoming popular with "working-class kids from New Jersey who would become a little rowdy" too. Many of these cult cinema fans learned all of the lines in the film, and would recite them at the screenings, a phenomenon which would later become associated with another popular midnight movie of the era, The Rocky Horror Picture Show (1975). 

===Ban=== banned in Australia, as well as in some provinces in Canada and Norway. The film was eventually released on VHS in Australia in the late 1980s with a X rating, but distribution of the video has since been discontinued. The 1997 version was cut by the distributor to achieve an R18+ after it was also refused classification. No submissions of the film have been made since, but it has been said that one of the reasons for which it was banned (as a film showing unsimulated sex cannot be rated X in Australia if it also features violence, so the highest a film such as Pink Flamingos could be rated is R18+) would now not apply, given that the depiction of unsimulated sex was passed within the R18+ rating for Romance (1999 film)|Romance in 1999, two years following Pink Flamingos  re-release. 

===Home media=== New Line audio commentaries and deleted scenes as introduced by Waters in the 25th anniversary re-release (#Alternate versions|see below).

===Alternate versions===
* The 25th anniversary re-release version contains a re-recorded music soundtrack, re-mixed for stereo, plus 15 minutes of deleted scenes following the film, introduced by Waters. Certain excerpts of music used in the original, including Igor Stravinskys The Rite of Spring had to be removed and replaced in the re-release, since the music rights had never been cleared for the original release. 
* Because of this films explicit nature, it has been edited for content on many occasions throughout the world. In 1973, the U.S. screened version edited out most of the fellatio scene, which was later restored on the 25th anniversary DVD. Canadian censors recently restored five of the seven scenes that were originally edited in that country. A town on Long Island, New York banned the film altogether.  The Japanese laserdisc version contains a blur superimposed over all displays of pubic hair. Prints also exist that were censored by the Maryland Censor Board. BBFC video regulation requirements) was completely uncut. It was issued by Palace as part of a package of Waters films they had acquired from New Line Cinema. The package included Mondo Trasho (double-billed with Sex Madness), Multiple Maniacs (double-billed with Cocaine Fiends), Desperate Living, and Female Trouble. The 1990 video re-release of Pink Flamingos (which required BBFC approval) was cut by three minutes and four seconds (3:04), the 1997 issue lost two minutes and forty-two seconds (2:42), and the pre-edited 1999 print by two minutes and eight seconds (2:08). Odorama for the first time, using scratch n sniff cards similar to the ones used in Waters later work Polyester.

==Reception==
The film received generally positive reviews, currently holding an 80% "fresh" rating on Rotten Tomatoes based on 40 reviews. 

==Influence==
===Divine=== coprophile but only ate excrement that one time because "it was in the script". 

Divine asked his mother, Frances Milstead, not to watch the film, a wish that she obliged. Several years before his death, Frances asked him if he had really eaten dog excrement in the film, to which he "just looked at me with that twinkle in his blue eyes, laughed, and said Mom, you wouldnt believe what they can do nowadays with trick photography." #Mil01|Milstead, Heffernan and Yeager 2001. p. 61. 

===Cultural influence===
  cult with audience participation similar to The Rocky Horror Picture Show.
* The Funday PawPet Show holds what is called the "Pink Flamingo Challenge", in which the ending to the film is played to the audience while they eat a (preferably chocolate) confection. Videos of the show are forbidden from showing the clip, only the reaction of the audience.
* Theater patrons often received free "Pink Phlegmingo" vomit bags.

Death metal band Skinless sampled portions of the Filth Politics speech for the songs "Merrie Melody" and "Pool of Stool", both on their second album Foreshadowing Our Demise.

===Proposed sequel=== Divine refused to be involved, and in 1984, Edith Massey died. 
 Trouble in Mind. According to his manager, Bernard Jay, "What was, in the early seventies, a mind-blowing exercise in Poor Taste, was now, we both believed, sheer Bad Taste. Divi  felt the public would never accept such an infantile effort in shock tactics some fifteen years later and by people fast approaching middle age." 

==See also==
* Cult film
* Midnight movies
* List of banned films
* Divine Trash
* In Bad Taste

==References==
 

;Bibliography
 
*  
*  
 

==External links==
 
*  
*  
*   at the  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 