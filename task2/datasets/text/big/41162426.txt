The Shiranui Sea
{{Infobox film
| name           = The Shiranui Sea
| image          =
| caption        =
| director       = Noriaki Tsuchimoto
| producer       = Ryūtarō Takagi
| writer         =
| starring       =
| music          =
| cinematography = Kōshirō Ōtsu
| editing        = Noriaki Tsuchimoto Keiko Ichihara
| studio         = Seirinsha Productions
| distributor    =
| released       =  
| runtime        = 153 min
| country        = Japan
| awards         =
| language       = Japanese
| budget         =
| preceded_by    =
| followed_by    =
}} mercury poisoning incident in Minamata, Japan.

==Film content==
Four years after  , Tsuchimotos camera focuses on the everyday lives of the victims of mercury poisoning. Fisherman still knowingly catch and eat the mercury-laden fish caught in the beautiful Shiranui Sea because that is what they have always done and that is how they relate to nature. Some patients who received significant compensation from Chisso, the polluter, may now live in good houses, but without doing work their lives seem somehow empty. The real victims remain the children, who are now getting older and in some cases increasingly conscious of the fact they are different from other children.

==Reception== Makoto Satō called The Shiranui Sea "the ultimate masterpiece" of Tsuchimotos Minamata films;    and the filmmaker John Gianvito selected it as one of the ten best films of all time in the 2012 Sight and Sound poll.   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 