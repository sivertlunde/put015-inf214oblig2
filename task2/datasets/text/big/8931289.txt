The Underneath (film)
 
{{Infobox film
| name           = The Underneath
| image          = UnderneathPoster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Steven Soderbergh
| producer       = John Hardy
| screenplay     = Steven Soderbergh  Daniel Fuchs Don Tracy
| narrator       =
| starring       = Peter Gallagher Alison Elliott
| music          = Cliff Martinez
| cinematography = Elliot Davis
| editing        = Stan Salfas
| distributor    = Gramercy Pictures
| released       =  
| runtime        = 99 min
| country        = United States
| language       = English
| budget         =
| gross          = $536,023 (USA)  
}}
 1995 American 1949 film adaptation. The plot revolves around many themes common to film noir, including romantic intrigue, a botched robbery, and a surprise ending.

==Plot==
Michael Chambers returns home to celebrate his mothers remarriage. Michael had fled his hometown due to gambling indiscretions and had left his wife Rachel to deal with the mess he created. He must now reassimilate into the town, renew his relationships with his family and friends (and enemies) and, most of all, seek out his ex-wife to woo her again.

Michael obtains a job working for his mothers new husband as an armored car driver. He almost seems the perfect prodigal son as he finds his niche back in the community and his way back into his ex-wifes heart. His troubles increase when he and Rachel are caught in the act by her hoodlum boyfriend, Dundee. To get out of this predicament, Michael must concoct a plan to steal a payroll being transported by his armored car company.

==Cast==
* Peter Gallagher ...  Michael Chambers 
* Alison Elliott ...  Rachel 
* William Fichtner ...  Tommy Dundee 
* Adam Trese ...  David Chambers 
* Joe Don Baker ...  Clay Hinkle 
* Paul Dooley ...  Ed Dutton
* Shelley Duvall ...  Nurse 
* Elisabeth Shue ...  Susan Crenshaw 
* Anjanette Comer ...  Mrs. Chambers 
* Cowboy Mouth ... Band at concert
* Dennis Hill ...  Guard Tom 
* Harry Goaz ...  Guard Casey 
* Mark Feltch ...  Guard George 
* Jules Sharp ...  Hinkles Assistant 
* Kenneth D. Harris ...  Mantrap Guard 
* Vincent Gaskins ...  Michaels Partner 
* Cliff Haby ...  Turret Operator 
* Tonie Perensky ...  Ember Waitress 
* Randall Brady ...  Ember Bartender 
* Richard Linklater ...  Ember Doorman 
* Helen Cates ...  Susans Friend 
* Kevin Crutchfield ...  VIP Room Flunky 
* Brad Leland ...  Man Delivering Money  John Martin ...  Justice of the Peace 
* C.K. McFarland ...  BonaFide Delivery Person 
* Rick Perkins ...  TV Delivery Man 
* Paul Wright ...  TV Delivery Man 
* David Jensen ...  Satellite Dish Installer 
* Jordy Hultberg ...  TV Sports Reporter 
* Steve Shearer ...  Detective 
* Fred Ellis ...  Detectives Partner 
* Joe Chrest ...  Mr. Rodman 
* Mike Enright ...  Embers V.I.P. (uncredited) 
* Mark Hanson ...  Bar Patron (uncredited) 
* Matthew Hurley ...  Angry Bar Patron (uncredited) 
* Mike Malone ...  Smoking Guy at Concert (uncredited) 
* Christopher K. Philippo ...  Ember Clubgoer (uncredited) 
* Ryan Wickerham ...  Guy at Concert (uncredited)

==Notes==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 