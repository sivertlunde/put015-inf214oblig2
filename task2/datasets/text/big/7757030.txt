The Great Gatsby (1949 film)
{{Infobox film
| name           = The Great Gatsby
| image          = The-Great-Gatsby-Poster-C10126101.jpeg
| caption        = Original film poster
| director       = Elliott Nugent
| producer       = Richard Maibaum
| writer         = Richard Maibaum   Cyril Hume
| based on       =    Barry Sullivan    Shelley Winters   Howard Da Silva
| music          = Robert Emmett Dolan
| cinematography = John F. Seitz
| editing        = Ellsworth Hoagland
| distributor    = Paramount Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
The Great Gatsby (1949) is a feature film released by Paramount Pictures, directed by Elliott Nugent, and produced by Richard Maibaum, from a screenplay by Richard Maibaum and Cyril Hume. It is based on the novel The Great Gatsby by F. Scott Fitzgerald. The music score was by Robert Emmett Dolan and the cinematography by John F. Seitz. The production was designed by Roland Anderson and Hans Dreier and the costumes by Edith Head.
 Barry Sullivan 1974 version.

==Cast==
* Alan Ladd as Jay Gatsby
* Betty Field as Daisy Buchanan
* Macdonald Carey as Nick Carraway
* Ruth Hussey as Jordan Baker Barry Sullivan as Tom Buchanan
* Howard Da Silva as George Wilson
* Shelley Winters as Myrtle Wilson

==Production notes==
Plans to make the film were announced in 1946, with Ladd, Maibaum and Hume all attached. PAT OBRIEN TO STAR IN THE BIG ANGLE: Crime Drama Was Written by Author of Bombardier-- Gatsby to Be Remade
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   26 Feb 1946: 31.  However, it was pushed back a number of years, reportedly due to censorship concerns. NOTES ABOUT PICTURES AND PEOPLE: New York to Get Another Film Unit -- Ticket Tax Cut Asked -- Addenda
By A.H. WEILER. New York Times (1923-Current file)   26 Oct 1947: X5. 
 Paramount to star as Daisy. Tyrone Power had stipulated that he would star as long as Tierney was cast. Elliott Nugent and producer Maibaum felt Tierneys beauty would be a distraction for Daisy. Tierney was dropped, and Power left the production. 

John Farrow, who had made a number of films with Alan Ladd, was originally meant to direct, but he left the project after a disagreement with Maibaum over casting.  He was replaced by Nugent. NUGENT REPLACES FARROW ON MOVIE: Named by Paramount to Direct The Great Gatsby, Remake of Fitzgerald Novel
By THOMAS F. BRADYSpecial to THE NEW YORK TIMES.. New York Times (1923-Current file)   13 Feb 1948: 26. 
 the 1926 silent version (now considered a lost film because no prints are known to exist). In 2012, a new print of the 1949 film was produced. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 