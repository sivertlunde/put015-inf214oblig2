Nagaram Sagaram
{{Infobox film 
| name           = Nagaram Sagaram
| image          =
| caption        =
| director       = KP Pillai
| producer       = KP Pillai
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi
| starring       = Sukumari KPAC Lalitha Adoor Bhasi Thikkurissi Sukumaran Nair
| music          = G. Devarajan
| cinematography = EN Balakrishnan
| editing        = K Sankunni
| studio         = Thrupthi Films
| distributor    = Thrupthi Films
| released       =  
| country        = India Malayalam
}}
 1974 Cinema Indian Malayalam Malayalam film, directed and produced by KP Pillai . The film stars Sukumari, KPAC Lalitha, Adoor Bhasi and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Sukumari
*KPAC Lalitha
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Sreelatha Namboothiri Raghavan
*Bahadoor Sumithra

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chanchalamizhi || P Jayachandran || Sreekumaran Thampi || 
|-
| 2 || Ente Hridayam || P. Madhuri || Sreekumaran Thampi || 
|-
| 3 || Jeevithamaam Saagarathil || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 4 || Ponnonakkili || Ambili || Sreekumaran Thampi || 
|-
| 5 || Thennalin Chundil || P Jayachandran, P. Madhuri || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 