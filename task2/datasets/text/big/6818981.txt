Bavagaru Bagunnara?
 
{{Infobox film
| name = Bavagaru Bagunnara
| image = Bavagaru Bagunnara.jpg
| caption =
| director = Jayanth C. Paranjee
| producer = Nagendra Babu
| writer = Jayanth C. Paranjee Paruchuri Brothers Rambha Paresh Rachana
| music = Mani Sharma
| cinematography =
| editing =
| distributor = Anjana Productions
| released = April 9, 1998
| runtime =
| language = Telugu
| budget =
| preceded by =
}} Govinda and Urmila Matondkar. 

==Plot==
Raju (Chiranjeevi) runs between New Zealand, where he owns a restaurant, and India to run a home for orphans started in his sisters name. Swapna (Rambha (actress)|Rambha) is a student in New Zealand, staying with her uncle. Once she goes looking for Raju to take him to task for thrashing her friend. On learning that Raju was not at fault, she promptly falls in love with him.

After that the scene shifts to India when Raju goes there to look after the home. There he keeps a pregnant woman, Sandhya (Rachana Banerjee|Rachana), from committing suicide. On learning about her jilted love affair, he decides to help her out. He convinces her that he will act as her husband until the baby is born, whereupon he would leave her, so that she could live with her child peacefully as a deserted wife. With that plan they go to her village. Her father, Rao Bahaddur Rajendra Prasad (Paresh Rawal), after initially refusing, unwillingly gives his nod to the plan under pressure from family members.

The story takes a twist when Swapna comes to India and finds, to her utter shock, Raju as her brother-in-law. Rajus pleadings of innocence fail to convince her. Meanwhile, Raju gets involved in a dispute about the ownership of a lake between their village and a neighboring one. He wins the race that decides its ownership in favor of Sandhyas village. Sandhyas father, happy at the turn of events leading to heightening of the prestige of their village, decides to accept Raju as his son-in-law and decides them married. Now Swapna, who comes to know the truth, is in a turmoil. Meanwhile, Sandhya makes another attempt of suicide, but Raju thwarts it again. In the process he comes to know that she met her lover (Achyut), who was held captive by the neighboring village head (Jayaprakash) and is being forced to marry his daughter. Raju rescues Sandhyas lover and gets them married, giving a happy ending to the movie.

==Cast==
* Chiranjeevi Rambha
* Paresh Rawal
* Rachana Banerjee
* Kota Srinivasa Rao
* Kaikala Satyanarayana Janaki
* Brahmanandam
* Jaya Prakash Reddy
* Srihari
* Deepa Raju

==Notes==
 
inspired by the Hollywood movie "a walk in the clouds"

==External links==
*  
*   at Basthi.com

 

 
 
 
 


 