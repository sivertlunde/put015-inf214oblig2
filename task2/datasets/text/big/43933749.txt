Married but Living Single
 
{{Infobox film
| name               = Married but Living Single
| image              = Married but Living Single poster.jpg
| alt                = 
| caption            = Theatrical release poster
| director           = Tunde Olaoye
| producer           = Kalejaiye Paul
| based on       =  
| screenplay        = Jovi Babs   Tunde Olaoye
| starring       =  
| music          = Sanjo Adegoke
| cinematography = Moruf Fadairo
| editing        = Shola Ayorinde   Seyi-Ola Emmanuel
| studio         = Indelible Mark Media
| distributor   = 
| released       =  
| runtime        = 
| country        = Nigeria
| language       = English
| budget         = ₦30 million (est) 
| gross          = ₦9,900,000   
}}
 Joseph Benjamin, KICC Lagos  and tells the story of Kate (Funke Akindele), a career driven woman whos married to an entrepreneur, Mike (Joseph Benjamin). Mike is diagnosed with lung cancer; Kate has to choose to either take a break from work to be with her husband while he recuperates from his surgery, or stay dedicated to her company which now stands a big chance of winning an important contract with a big telecommunication company.

==Cast==
*Funke Akindele as Kate Joseph Benjamin as Mike
*Joke Silva as Kates boss
*Tina Mba as 
*Kiki Omeili as Titi
*Femi Brainard as 
*Kalejaiye Paul as Patrick
*Adeola Faseyi as 
*Yemi Remi as Lola

==Release==
The official trailer for the film was released on 20 February 2012, along with some behind-the-scenes promotional photos.    It premiered at the Silverbird Galleria, Lagos on 3 June 2012;    the event had Lagos State Deputy Governor: Adejoke Orelope-Adefulire in attendance.  The five cinema halls used for screening at the premiere was reportedly packed.    It was initially released in selected cinemas on 5 June 2012.    It had its overseas premiere at the Greenwich Odeon Cinema, London on 5 October 2012.   It was also screened in Scotland, a non-traditional market. 

==Reception==
===Critical reception===
The film received generally negative to mixed reviews. Adedayo Odulaja of Daily Independent concludes: "While Married but Living Single is a movie that everyone can easily relate to, the character of Mike can be said to have taken too much attention. The happily-ever-after factor in the tale is another that is clearly an exaggerated issue in this movie. Aside those areas that might not sit well with those discerning enough to dwell on them, Married but Living Single is deserving enough of a view".  Augusta Okon of 9aijabooksandmovies comments: "Married but Living Single struggles to bring to life the ideals intended by the author. Although its adaptation is commendable, distinguishing it from those in the sea of original works, the stereotyped storyline, lack lustre performance, and a couple of subplots insults our sensibilities. The movie lacks depth and just hobbles to the finish line, only by the acting dexterity of the two leads. It’s a mishmash, ordinary, wanna be nice movie".  Ada Ude of Connect Nigeria comments: "The satiric plot of this movie touches on all aspects; the family—relationship, trust, neglect, and emotional and physical abuse, as are evident in our daily living. The plot was excellent and they had a good story which every young upwardly mobile couple in present day Nigeria should be conscious about. however, more attention should have been given to the many tiny little details in the movie, for a better execution of a wonderful story. Also, Akindele and Joseph just didn’t have any sparks on screen".  Myne Whitman faults the portrayal of women in the film, noting it as being biased. 

===Box office===
The film opened strongly and continued on average receipts, peaking at number three in West African cinemas.   

===Accolades===
The film was nominated in five categories at the 2012 Best of Nollywood Awards, including "Movie of the Year", "Director of the Year", "Best Lead Actress in an English Film" for Akindele, and "Child actress of the year" for Deola Faseyi; Benjamin won the award for "Best Lead Actor in an English Film". Faseyi was again nominated for "Best Child Actor" at the 2013 Nollywood Movies Awards.

{| class="wikitable sortable" style="width:100%"
|+Complete list of Awards
|-
!  Award !! Category !! Recipients and nominees !! Result
|- Best of Best of Nollywood Magazine   2012 Best of Nollywood Awards 
| Movie of the Year
| Tunde Laoye
|  
|-
| Director of the Year
| Tunde Laoye
|  
|-
| Best Lead Actress in an English Film
| Funke Akindele
|  
|-
| Best Lead Actor in an English Film Joseph Benjamin
|  
|-
| Child actress of the year
| Deola Faseyi
|  
|- Nollywood Movies Network   2013 Nollywood Movies Awards 
| Best Child Actor
| Adeola Faseyi
|  
|}

==Home media== VOD on 26 April 2013 via Distrify.  It was released on DVD on 29 August 2013. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 