City Police (film)
{{Infobox film
| name           = City Police
| image          =
| image_size     =
| caption        =
| director       = Venu Nair
| producer       = Palamuttam Majeed
| writer         = Kaloor Dennis
| screenplay     = Kaloor Dennis Geetha Sukumari PC George
| music          = S. P. Venkatesh
| cinematography = Saloo George
| editing        = K. Sankunni
| studio         = Kaseeba Productions
| distributor    = Kaseeba Productions
| released       =  
| country        = India Malayalam
}}
 1993 Cinema Indian Malayalam Malayalam film,  directed by Venu Nair and produced by Palamuttam Majeed. The film stars Suresh Gopi, Geetha (actress)|Geetha, Sukumari and PC George in lead roles. The film had musical score by S. P. Venkatesh.   

==Cast==
 
*Suresh Gopi as Arun Kumar Geetha as Dr Jessy
*Sukumari as Sajan Mathews mother
*PC George Mahesh as Jayan
*Prathapachandran as D.I.G
*Geetha Vijayan as Maya
*Jagannatha Varma
*KPAC Sunny
*Kakka Ravi as Sajan Mathew
*Mala Aravindan
*Narayanankutty as Narayanankutty
*TS Krishnan as Nandu
*Neena Kurup
 

==Soundtrack==
The music was composed by S. P. Venkatesh.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ennolam Sundariyaarundu || Minmini || Kaithapram ||
|}

==References==
 

==External links==
*  

 
 
 

 