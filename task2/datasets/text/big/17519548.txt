Take Me Out to the Ball Game (film)
{{Infobox film
| name           = Take Me Out to the Ball Game
| image          = Take Me Out To The Ballgame (MGM film).jpg
| image_size     = 
| alt            = 
| caption        = Promotional poster
| director       = Busby Berkeley 
| producer       = Arthur Freed George Wells (screenplay)   Gene Kelly (story)  Stanley Donen (story)
| narrator       = 
| starring       = Frank Sinatra   Esther Williams Gene Kelly Betty Garrett   Jules Munshin
| music          = Adolph Deutsch
| cinematography = George J. Folsey
| editing        = Blanche Sewell
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = $2,025,000  . 
| gross          = $4,344,000 
| preceded_by    = 
| followed_by    = 
}}
 1949 Technicolor musical film starring Frank Sinatra, Esther Williams, and Gene Kelly.  The movie was directed by Busby Berkeley. The title and nominal theme is taken from the unofficial anthem of American baseball, "Take Me Out to the Ball Game".  The movie was released in the United Kingdom as Everybodys Cheering.

==Plot summary==
  as Dennis Ryan]] start the Washington Senators, and later play the Boston Red Sox, Philadelphia Athletics, and Cleveland Indians, all American League teams.  and two of its players, Eddie OBrien (Gene Kelly) and Dennis Ryan (Frank Sinatra), who are also part-time vaudeville|vaudevillians.  

The ball clubs status quo is turned on its head when the team winds up under new ownership, and the distress this causes the team is only increased when the new owner is revealed to be a woman, K.C. (Katherine Catherine) Higgins (  

==Production==
  as K.C. Higgins]]
Esther Williams, a star in swimming-themed musicals, did not enjoy her experience filming with star, story-writer and choreographer Gene Kelly.  In her autobiography, she describes her time on the film as "pure misery", claiming that Kelly and Stanley Donen treated her with contempt and went out of their way to make jokes at her expense.  Williams asserts that Kelly was uncomfortable with the height difference between them, Williams being 510", while Kelly was 57".  

Director   was originally slated to star, but was replaced because of substance abuse problems.   Similarly, Sinatras role of Dennis Ryan was said to have originally been intended for professional baseball manager (and former player) Leo Durocher. 

==Cast==
*Frank Sinatra as Dennis Ryan
*Esther Williams as K.C. Higgins
*Gene Kelly as Eddie OBrien
*Betty Garrett as Shirley Delwyn
*Jules Munshin as Nat Goldberg Edward Arnold as Joe Lorgan Richard Lane as Michael Gilhuly
*Tom Dugan as Slappy Burke

==Musical numbers==
*"Take Me Out to the Ball Game" - Gene Kelly and Frank Sinatra, reprise by Esther Williams
*"Yes, Indeedy" - Gene Kelly and Frank Sinatra
*"OBrien to Ryan to Goldberg" - Gene Kelly, Frank Sinatra and Jules Munshin
*"The Right Girl for Me" - Frank Sinatra
*"Its Fate Baby, Its Fate" - Frank Sinatra and Betty Garrett
*"Strictly U.S.A." - Betty Garrett, Frank Sinatra, Esther Williams and Gene Kelly
*"The Hat My Dear Old Father Wore upon St. Patricks Day" - Gene Kelly

==Reception==
Take Me Out to the Ball Game was a box office success, earning $2,987,000 in the US and Canada and $978,000 overseas, resulting in a profit of $675,000. 

It received modestly positive reviews, although some reviewers felt the cast was better than the material, and the film lacked a "consistent style and pace". 

==Awards== George Wells On the MGM musical comedy, also produced by Arthur Freed, and also starring Gene Kelly, Frank Sinatra, Betty Garrett and Jules Munshin, which was released four months after Take Me Out to the Ball Game.

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 

 