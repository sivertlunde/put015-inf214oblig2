Some People (film)
 Some People}}
{{Infobox film
| name           = Some People
| image          = 
| image_size     = 
| caption        = 
| director       = Clive Donner
| producer = James Archibal
| writer         = 
| narrator       =  Ray Brooks
| music          = Ron Grainer
| cinematography = John Wilcox
| editing        = Fergus McDonell
| distributor    =  
| released       = June 1964
| runtime        = 93 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
 Ray Brooks and is centred on the Duke of Edinburghs Award Scheme. 

==Plot==
A welfare worker (Kenneth More) tries to help a gang of teenagers in Bristol.

==Production==
The film was shot entirely on location in Bristol. More says he agreed to play the real role for nothing apart from his expenses because he had no other offers around the time, and the movie was for a good cause: all proceeds were to go to the Duke of Edinburghs Award Scheme and the National Playing Fields Association. During filming he began an affair with one of the cast, Angela Douglas, who became his wife. 

==Cast==
* Kenneth More as Mr. Smith Ray Brooks as Johnnie
* Anneke Wills as Anne
* David Andrews as Bill
* Angela Douglas as Terry
* David Hemmings as Bert
* Harry H. Corbett as Johnnies Father  Richard Davies as Harper 
* Michael Gwynn as Vicar 
* Cyril Luckham as Magistrate
* Valerie Mountain dubbed Angela Douglas singing voice 

==References==
 

==External links==
* 

 

 
 
 