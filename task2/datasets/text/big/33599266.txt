Love Crime
{{Infobox film
| name           = Love Crime
| image          = Love Crime.jpg
| caption        = 
| director       = Alain Corneau
| producer       = Saïd Ben Saïd Alexander Emmert
| writer         = Natalie Carter Alain Corneau
| starring       = Ludivine Sagnier Kristin Scott Thomas
| music          = 
| cinematography = Yves Angelo
| editing        = 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = France
| language       = French English
| budget         = €6,970,000
| gross          = $3,608,661 
}}
Love Crime ( ) is a 2010 French psychological suspense thriller starring Ludivine Sagnier and Kristin Scott Thomas. It is the last film directed by Alain Corneau, and was released posthumously after the directors death from cancer. 

==Plot==
Isabelle (Sagnier) is a young, ambitious and talented executive working in the Paris office of an unnamed multi-national corporation. Her immediate boss is Christine (Scott Thomas), equally ambitious, who presents Isabelles work as her own to win a promotion to the firms New York office. Tired of being upstaged, and with the aid of a colleague, Daniel (Marquet), Isabelle blindsides Christine with a secret project that the New York office enthusiastically endorses. Christines New York promotion is subsequently withdrawn. Enraged by what she considers an act of disloyalty, Christine begins to psychologically torment Isabelle. Christines lover, Philippe (Mille), works with the firm and is suspected of embezzling funds. Using this information against him, she blackmails Philippe into standing up Isabelle, with whom he is having an affair, leaving her inconsolable. Isabelle is then humiliated at a staff party in which security camera footage of her meltdown is presented to her colleagues.

Relaxing at home after hosting another soiree, Christine is stabbed to death by Isabelle, who plants evidence at the home implicating herself as the killer. With police pursuing the obvious motive that she was taking revenge for her various humiliations at the hands of Christine, Isabelle is arrested and, despite giving all the signs of being in the grips of a nervous breakdown, confesses to, and then is charged with, her murder. Awaiting sentencing in prison, Isabelle withdraws her confession, claiming it was made under duress and while heavily medicated. She begins to pick apart the circumstantial evidence implicating her guilt; scratches on her arm thought to be knife wounds were from a gardening accident, witnesses testify to seeing her at a cinema at the time of the murder and a kitchen knife thought to be the murder weapon is actually located in Isabelles shed. The last piece of evidence is a torn strip of a scarf found at the murder scene, an item originally given to Isabelle by Christine as a gift. When Isabelle is able to produce an identical scarf, completely intact, at her home, she is cleared of the charges and released from prison. Acting on a tip provided by Isabelle herself, police learn of Philippes embezzlement, and a search of his property locates the remaining section of the scarf in his vehicle, planted there by Isabelle as part of an elaborate effort to frame him. He is arrested and charged. Isabelle, now free of both her nemeses, returns triumphantly to her company.

==Release==
The film opened in the U.S. in September 2011 in limited release, and is slowly making its way around the U.S. The dialogue is in both French and English; the English being spoken only when the characters of Christine and Isabelle have to interact or deal with foreign clients who do not speak French.

==Cast==
*Ludivine Sagnier as Isabelle Guérin
*Kristin Scott Thomas as Christine Riviére
*Patrick Mille as Philippe Deschamps
*Guillaume Marquet as Daniel
*Gérald Laroche as Gérard
*Marie Guillard as Claudine (sister)
*Mike Powers as Boss 1
*Matthew Gonder as Boss 2
*Jean-Pierre Leclerc as Gérard
*Stéphane Roquet as Fabien
*Frédéric Venant as Cadre

==Reception==
The film has received a 63% "Fresh" rating, according to the website Rotten Tomatoes. 

==Remake==
 
It was announced in 2012 that Rachel McAdams and Noomi Rapace would play the lead roles in a French-German-Spanish-UK  remake of the film titled Passion (2012 film)|Passion to be directed by Brian De Palma   It will sell at the 2012 Cannes Film Festival.  The film has been selected to compete for the Golden Lion at the 69th Venice International Film Festival.   

==References==
 

==External links==
* 
 

 
 
 
 
 