Una cita de amor
 
{{Infobox film
| name           = Una cita de amor
| image          = 
| image size     = 
| caption        = 
| director       = Emilio Fernández
| producer       = Jorge García Besné
| writer         = Pedro Antonio de Alarcón Mauricio Magdaleno
| starring       = Silvia Pinal
| music          = 
| cinematography = Gabriel Figueroa
| editing        = Gloria Schoemann
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}

Una cita de amor ("A Date of Love") is a 1958 Mexican drama film directed by Emilio Fernández. It was entered into the 8th Berlin International Film Festival.   

==Cast==
* Silvia Pinal as Soledad
* Carlos López Moctezuma as Don Mariano Jaime Fernández as Róman Chávez
* José Elías Moreno as Juez de Acordada
* Agustín Fernández (actor)|Agustín Fernández as Sustituto de juez
* Guillermo Cramer as Ernesto
* Amalia Mendoza as Genoveva
* Arturo Soto Rangel as Sacerdote
* Jorge Treviño - Anunciador
* Rogelio Fernández
* Antonio León Yáñez
* Margarito Luna
* Gregorio Acosta
* Emilio Garibay

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 