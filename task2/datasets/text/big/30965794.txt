Hauling (film)
{{Infobox film
| name = Hauling
| image = Hauling.jpg
| director = Sean Walsh
| producer = Sean Walsh
| released =  
| country = Brazil
| language = Portuguese
}}
Hauling is a 2010 documentary film on poor families who work as recyclers in Sao Paulo, Brazil.

==Plot==
The recycling underworld of Sao Paulo, Brazil is the background to this documentary centering on Claudieni, father to 27 children. In his green VW bus, he visits the Santa Efigenia neighborhood of Sao Paulo to recycle plastic, cardboard, and computers and other electronic equipment. He and his family represent the lower class of people who are discriminated against by other strata of society.

The film will have its U.S. West Coast premiere at the San Francisco Green Film Festival in March 2011. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 