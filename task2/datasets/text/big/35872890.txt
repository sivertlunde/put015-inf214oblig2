La Playa DC
 
{{Infobox film
| name           = La Playa DC
| image          = La Playa DC film poster.jpg
| caption        = Theatrical release poster
| director       = Juan Andrés Arango
| producer       = Jorge Andrés Botero
| writer         = Juan Andrés Arango
| starring       = Luis Carlos Guevara   Jamés Solís  Andrés Murillo
| music          = 
| cinematography = Nicolas Canniccioni
| editing        = Felipe Guerrero  
| distributor    = Cineplex
| released       =  
| runtime        = 90 minutes
| country        = Colombia France Brazil
| language       = Spanish
| budget         = 
}} Best Foreign Language Film at the 86th Academy Awards,       but it was not nominated.

==Cast==
* Luis Carlos Guevara as Tomás
* Jamés Solís as Chaco
* Andrés Murillo as Jairo
==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Colombian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 