Palmy Days
{{Infobox film
| name           = Palmy Days
| image          = Poster of Palmy Days.jpg
| image_size     = 
| caption        = 
| director       = A. Edward Sutherland
| producer       = Samuel Goldwyn
| writer         = Eddie Cantor Morrie Ryskind David Freedman
| starring       = Eddie Cantor Charlotte Greenwood George Raft
| music          = Harry Akst
| cinematography = Gregg Toland
| editing        = Sherman Todd 
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $1,601,000   accessed 19 April 2014  
}}
Palmy Days (1931 in film|1931) is an American musical comedy film written by Eddie Cantor, Morrie Ryskind, and David Freedman, directed by A. Edward Sutherland, and choreographed by Busby Berkeley (who makes a cameo appearance as a fortune teller). The film stars Eddie Cantor. The famed Goldwyn Girls make appearances during elaborate production numbers set in a gymnasium and a bakery ("Glorifying the American Doughnut"). Betty Grable, Paulette Goddard, Virginia Grey, and Toby Wing are among the bevy of chorines.

==Cast (in credits order)==
*Charlotte Greenwood as Helen Martin
*Barbara Weeks as Joan Clark
*Spencer Charters as Mr Clark
*Paul Page as Steve Charles Middleton as Yolando
*George Raft as Joe - Yolandos Henchman
*Harry Woods as Yolandos Henchman
*Eddie Cantor as Eddie Simpson

==Reception==
The film was one of the most popular movies of the year. Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 18 

New York Times movie critic Mordaunt Hall, described Palmy Days as "a more or less funny diatribe" with "two or three inconsequential melodies and a great deal to gaze, including pretty damsels from the Pacific Coast and effectively photographed groups of dancers." 

==Product placement== Underwood Typewriter Continental Baking Company.
 

==See also==
* List of American films of 1931

==References==
 

== External links ==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 