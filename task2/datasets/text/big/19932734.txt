Japanese War Bride
{{Infobox film
| name = Japanese War Bride
| image= Japanese War Bride VideoCover.png
| caption =
| director = King Vidor
| producer = Joseph Bernhard Anson Bond
| writer = Anson Bond (story)  Catherine Turney (screenplay) Shirley Yamaguchi Don Taylor
| music =
| cinematography = Lionel Lindon
| editing = Terry O. Morse
| distributor = 20th Century-Fox
| released =  
| runtime = 91 minutes
| country = United States
| awards =
| language = English
| budget =
}} Shirley Yamaguchi in the title role.

==Synopsis== Don Taylor), Shirley Yamaguchi) was working as a nurse. Back in America, the two face racism and bigotry from their neighbours and family, particularly their sister-in-law, Fran (Marie Windsor).

==Impact and legacy== The Teahouse of the August Moon and the more successful film Sayonara (film)|Sayonara, Japanese War Bride was argued by some scholars to have increased racial tolerance in the United States by openly discussing interracial marriages.   

==Principal cast== Shirley Yamaguchi - Tae Shimizu, a nurse, wife to Jim Sterling Don Taylor GI in the Korean War Cameron Mitchell - Art Sterling, Jims younger brother
*Marie Windsor - Fran Sterling, Arts wife James Bell - Ed Sterling, Jims father
*Louise Lorimer - Harriet Sterling, Jims mother
*Philip Ahn - Eitaro Shimizu, Taes grandfather
*Lane Nakano - Shiro Hasagawa, the Sterlings Japanese-American neighbour
*May Takasugi - Emma Hasagawa, Shiros wife
*Sybil Merritt - Emily Shafer,  a local girl
*Orley Lindgren - Ted Sterling, Jims brother	 George Wallace - Woody Blacker, a friend of Jim Sterling
*Kathleen Mulqueen - Mrs. Milly Shafer, a friend of Harriet Sterling

==External links==
* 
* 
* 

==Notes and references==
 
*"Story of a Japanese War Bride", The New York Times, January 30, 1952.

 

 
 
 
 
 
 
 
 
 
 


 