Ithramathram (1986 film)
{{Infobox film
| name           = Ithramathram
| image          =
| caption        =
| director       = P Chandrakumar
| producer       =
| writer         =
| screenplay     = Urvashi Ratheesh Kalaranjini Maniyanpilla Raju
| music          = Raghu Kumar
| cinematography =
| editing        =
| studio         = Kumarakam Films
| distributor    = Kumarakam Films
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by P Chandrakumar. The film stars Urvashi (actress)|Urvashi, Ratheesh, Kalaranjini and Maniyanpilla Raju in lead roles. The film had musical score by Raghu Kumar.   

==Cast==
*Ratheesh as Rameshan
*Kalaranjini as Anitha
*Maniyanpilla Raju as Rajeevan
*Jalaja as Chithra
*Kuthiravattam Pappu as Pappan
*MG Soman as Colonel Rajasekharan
*Sukumari as Prameela
*Mala Aravindan as Augustine
*Rajkumar Sethupathi as Ravi
*Nanditha Bose as Sharada Kalpana as Rajani Santhakumari as Chithras mother Sadhana as Stella Ramu as Vijayan
*Sankaradi as K. V. Menon
*Shankar Panikkar as Guest
*T. R. Omana as Santhamma Vijayan as Johny

==Soundtrack==
The music was composed by Raghu Kumar and lyrics was written by Vijayan (actor)|Vijayan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vijayan || 
|- Vijayan || 
|}

==References==
 

==External links==
*  

 
 
 

 