A Long Hot Summer (film)
{{Infobox film
| name           = A Long Hot Summer
| director       = Perttu Leppä
| producer       = Johannes Lassila
| writer         = Perttu Leppä
| starring       = 
| music          = 
| cinematography = Jyrki Arnikari
| editing        = Kimmo Taavila
| studio         = Talent House
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Finland
| language       = Finnish
| budget         = €880,000 
| gross          = 
}} Finnish rock comedy film written and directed by Perttu Leppä. It was the most popular youth film of the 1990s in Finland.  The film, set in 1980, tells the story of a fictional rock band called Kalle Päätalo. Due to the films popularity, the band started to tour around the country. 

== Cast ==
* Unto Helo as Paavo "Patu" Taskinen, the manager and lyricist of the band Kalle Päätalo
* Hanna-Mari Karhinen as Noora, the driver of Kalle Päätalo
* Mikko Hakola as Jukka Lajunen, the singer of Kalle Päätalo
* Timo Lavikainen as Kukkonen, the drummer of Kalle Päätalo Konsta Hiekkanen as Jimi, the guitarist of Kalle Päätalo
* Olli Sorjonen as Kakkonen, the bassist of Kalle Päätalo
* Aino Seppo as Patus mother
* Niina Stützle as Sanna, Patus sister
* Toni Wirtanen as Lärssi, the singer of The Vittupäät
* Harri Tuovinen as Stuube, the bassist of The Vittupäät
* Pauli Rantasalmi as Pate, the drummer of The Vittupäät 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 
 