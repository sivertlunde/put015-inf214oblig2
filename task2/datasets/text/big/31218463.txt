Children (2011 film)
{{Infobox film name           = Children image          = Children2011Poster.jpg caption        = Film poster director       = Lee Kyu-man  producer       = Lee Yong-ho Son Kwang-ik Uhm Joo-young  writer         = Lee Hyeon-jin Lee Kyu-man  starring       = Park Yong-woo Ryu Seung-ryong Sung Dong-il  music          = Choi Seung-hyun  cinematography = Ki Se-hoon editing        = Kim Hyeong-ju distributor    = Lotte Entertainment  released       =   runtime        = 132 minutes country        = South Korea language       = Korean budget         =  admissions     =  gross          =   
}} unsolved murder case.  

==Plot==
On March 26, 1991, five boys went to Mount Waryong to catch frogs but never returned. Eleven years later, police discover the bodies of the five children, and television producer Kang Ji-seung tries to solve the mystery.    

==Cast==
*Park Yong-woo as Kang Ji-seung  
*Ryu Seung-ryong as Hwang Woo-hyuk
*Sung Dong-il as Park Kyung-sik
*Sung Ji-ru as Jung-hos father
*Kim Yeo-jin as Jung-hos mother
*Ju Jin-mo as Director Ahn
*Park Byung-eun as Kim Joo-hwan
*Kim Gu-taek as Won-kils father
*Park Mi-hyun as Won-kils mother
*Lee Sang-hee as Yong-duks father
*Seo Ju-hee as Yong-duks mother
*Jo Deok-je as Dong-pils father
*Seo Young-hwa as Dong-pils mother
*Nam Sang-baek as Chul-woos father
*Jeon Guk-hwan as Professor Cha
*Kwak Min-seok as police chief
*Ra Mi-ran as psychic

==Release==
The film was released on February 17, 2011 and netted a total of 1,867,736 admissions nationwide. 

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 
 