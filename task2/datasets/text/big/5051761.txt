Park (film)
Park is the name of an independent comedy-drama film released in 2007 in film|2007. It was produced by Dana Jackson and directed by Kurt Voelker.                    
 Los Angeles park, where ten colorful characters encounter love - and loss - in the course of one day.

It received the Audience Award at the 8th Annual CineVegas Festival where it was premiered in June 2006. After a limited theatrical run in 2007, it was released on DVD on May 20, 2008.

==Cast==
*William Baldwin - Dennis
*Ricki Lake - Peggy
*Cheri Oteri - Claire
*Melanie Lynskey - Sheryl
*Izabella Miko - Krysta
*Anne Dudek - Meredith
*Trent Ford - Nathan
*Maulik Pancholy - Babar
*David Fenner - Ian
*Dagney Kerr - April Anthony Treach Criss - Darnell
*Francesco Quinn - Javier

==Awards==
* Audience Award, CineVegas 2006
* Best Screenplay Award, Fylmz Festival 2007
* Bud Abbot Award - Best Feature Comedy, Garden State Film Festival 2007
* Audience Award, Sonoma Valley Film Festival 2007

==References==
 

==External links==
* 
* 
* 

 
 


 