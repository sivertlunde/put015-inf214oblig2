The Blind Sunflowers (film)
{{Infobox film
| name           = The Blind Sunflowers
| image          = Los Girasoles Ciegos, film poster.jpg
| caption        = 
| director       = José Luis Cuerda
| producer       = Fernando Bovaira José Luis Cuerda Emiliano Otegui
| writer         = José Luis Cuerda Rafael Azcona
| based on       =  
| starring       = Maribel Verdú Javier Cámara Raúl Arévalo Irene Escolar  Martiño Rivas José Ángel Egido Roger Princep 
| music          = Lucio Godoy
| cinematography = Hans Burmann
| editing        = Nacho Ruiz Capillas
| studio         = 
| distributor    = Sogecine
| released       =  
| runtime        = 98 minutes
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = 
}} Rector delays Salvadors access to priesthood for a year. Salvador begins teaching in a school where he meets with Lorenzo, the son of Elena, whom of which Salvador thinks is widowed. This opportunity multiplies with the deacon becoming obsessed with her, abusing her mentally and physically. We realise that Salvador is threatening Elenas family because of his obsession. Wounded and beaten by the circumstances, the characters of the Blind Sunflowers hit the wall of repression, impossible romances and emotional defeats, while we realise Elenas family try to search for a glimpse of hope.

This film was Spains 81st Academy Awards official submission to Foreign Language Film category, but it was not selected.

==Cast==
*Maribel Verdú as Elena
*Javier Cámara as Ricardo
*Raúl Arévalo as Salvador
*Roger Príncep as Lorenzo
*José Ángel Egido as Rector
*Martín Rivas (actor)|Martín Rivas as Lalo

==Accolades==
*Goya Awards
**Won: Best Screenplay &ndash; Adapted (Rafael Azcona and José Luis Cuerda)
**Nominated: Best Actor &ndash; Leading Role (Raúl Arévalo)
**Nominated: Best Actor  &ndash; Supporting Role (José Ángel Egido)
**Nominated: Best Actress &ndash; Leading Role (Maribel Verdú)
**Nominated: Best Cinematography (Hans Burman) 
**Nominated: Best Costume Design (Sonia Grande) 
**Nominated: Best Director (José Luis Cuerda)
**Nominated: Best Editing (Nacho Ruiz Capillas)
**Nominated: Best Film
**Nominated: Best Makeup and Hairstyles (Fermín Galán and Silvie Imbert)
**Nominated: Best New Actor (Martín Rivas)
**Nominated: Best Original Score (Lucio Godoy)
**Nominated: Best Production Design (Baltasar Gallart)
**Nominated: Best Production Supervision (Emiliano Otegui)
**Nominated: Best Sound (Ricardo Steinberg, María Steinberg and Alfonso Raposo)

==External links==
*  
*  

 
 
 
 
 
 
 


 
 