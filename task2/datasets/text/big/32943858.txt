The Sick Stockrider
 
{{Infobox film
  | name     = The Sick Stockrider
  | image    = 
  | caption  = 
  | director = W. J. Lincoln Godfrey Cass		
  | producer = 
  | writer   = W. J. Lincoln 
| based on = original poem by Adam Lindsay Gordon
  | starring = Roy Redgrave Godfrey Cass
  | music    = 
  | cinematography = Maurice Bertel
  | editing  = 
  | studio  = Lincoln-Cass Films
  | released = 18 August 1913 
  | runtime  = 2,000 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}

The Sick Stockrider is a 1913 film directed by W. J. Lincoln based on the poem by Adam Lindsay Gordon. It was the first production from Lincoln-Cass Films and is one of the few Australian silent films to survive in its entirety. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, p41 

==Plot==
The film presents the verses of the poem one by one, separated by illustrated tableaux. It tells the story about a dying stockman.

==Cast==
*Roy Redgrave
*Godfrey Cass as the stock riders mate
*George Bryant as the stockrider 
*Beryl Bryant
*Tom Cannam

==Production==
Adam Lindsay Gordons ballad was first published in 1870, the year of his death. The movie was the first from Lincoln-Cass Films, established in 1913.  It was shot at the companys studio in Elsternwick, Melbourne

==Release==
Screenings were often accompanied by a lecturer who would recite the poem. 

The movie screened to thirty full houses in Victoria. It has been described as "solid and stagey with shaking canvas sets, an exaggerated alcoholic scene and a bull-goring sequence in which an actor tumble turns across an animal all too obviously at rest." Graham Shirley and Brian Adams, Australian Cinema: The First Eighty Years, Currency Press 1989 p 42 

===Remake===
Harry Southwell also announced plans to film the poem but no movie resulted. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive
* 
*  at AustLit
 

 
 
 
 
 
 


 