Madison Square Garden (film)
{{Infobox film
| name           = Madison Square Garden
| image          = 
| alt            = 
| caption        = 
| director       = Harry Joe Brown
| producer       = Charles R. Rogers
| screenplay     = Thomson Burtis Allen Rivkin P.J. Wolfson
| starring       = Jack Oakie Thomas Meighan Marian Nixon William Collier, Sr. ZaSu Pitts Lew Cody William "Stage" Boyd
| music          = Harold Lewis 
| cinematography = Henry Sharp 	
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Madison Square Garden is a 1932 American drama film directed by Harry Joe Brown and written by Thomson Burtis, Allen Rivkin and P.J. Wolfson. The film stars Jack Oakie, Thomas Meighan, Marian Nixon, William Collier, Sr., ZaSu Pitts, Lew Cody and William "Stage" Boyd. The film was released on November 4, 1932, by Paramount Pictures.  
 
==Plot==
 

== Cast ==
*Jack Oakie as Eddie Burke
*Thomas Meighan as Bill Carley
*Marian Nixon as Bee 
*William Collier, Sr. as Doc Williams
*ZaSu Pitts as Florrie
*Lew Cody as Rourke
*William "Stage" Boyd as Sloane
*Warren Hymer as Brassie Randall Robert Elliott as Honest John Miller
*Joyce Compton as Joyce
*Bert Gordon as Izzy
*Noel Francis as Noel
*Sailor Sharkey as Tom Sailor Sharkey 
*Tommy Ryan as Tommy Ryan
*Stanislaus Zbyszko as Wrestler Stanislaus Zbyszko
*Billy Papke as Billy Papke
*Mike Donlin as Referee Mike Donlin
*Tod Sloan as Tod Sloan
*Damon Runyon as Damon Runyon
*Grantland Rice as Grantland Rice
*Jack Lait as Jack Lait 
*Westbrook Pegler as Westbrook Pegler 
*Paul Gallico as Paul Gallico  Jack Johnson as Jack Johnson 
*Ed W. Smith as Ed Smith
*Teddy Hayes as Teddy Hayes
*Lou Magnolia as Lou Magnolia 
*W. C. Robinson as Writer W. C. Robinson

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 