The Lost Tape (film)
 
 
{{Infobox film
| name=The Lost Tape
| image= 
| caption=Theatrical release poster
| director=Rakshit Dahiya
| producer=Ashok Baphna
| starring= Ankuar Panchal   Esha Rajee  Nishantt Tanwar  Monali Sehgal   Shaurya Singh 
| cinematography=Dhaval Ganbote
| editing=Vivek Agrawal
| studio= Baphna Films
| distributor= Manoj Nandwana (Jai Viratra Entertainment Ltd)
| released=   
| country=India
| language=Hindi
}}
The Lost Tape (Hindi: द लोस्ट टेप) is a 2012 Bollywood horror filmdirected by Rakshit Dahiya   found footage style. The films cinematography is done by Dhaval Ganbote, a graduate from Whistling Woods International Institute and is edited by  Vivek Agrawal, an editing Faculty of Whistling Woods International Institute .

==Plot==
5 youngsters went into the woods of Chail, Himachal Pradesh to explore an old urban legend’s story. They were completely unaware of the fact that they were slowly becoming part
of the story. The more they found out, the closer they got to the unexplained reality, of what they
thought was merely a myth. After entering into the woods, they were never seen again. The search went on for 6 months and the case was closed. Until one day, police recovered a digital video recorder, which held the unexplained secret. This film unfolds the mystery of those 5 youngsters’ mysterious disappearance. A tale of Love, friendship, betrayal and fear. Fear of the devil and fear of the Lord himself.

==Cast==
*Ankuar Panchal As Lavish 
*Esha Rajee as Jassi
*Nishantt Tanwar as Rahul 
*Monali Sehgal as Ayesha
*Shaurya Singh as Jai

===Filming===
The shoot took place in the forests of Chail, Himachal Pradesh.

==References==
 
 

==External links==
* 
* 
* 
 

 
 
 
 
 


 