The Fourth War
{{Infobox film
| name           = The Fourth War
| image          =The Fourth War.jpg
| caption        =
| director       = John Frankenheimer Kenneth Ross
| narrator       =
| starring       = Roy Scheider Jürgen Prochnow
| music          = Bill Conti
| cinematography = Gerry Fisher
| editing        =
| distributor    =
| released       =  
| running time   = 91 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
The Fourth War is a 1990 film directed by John Frankenheimer, set in late 1980s Berlin. Its title stems from a famous quote by Albert Einstein: "I cannot predict how the Third World War shall be fought, or with what; I can, however, predict that the Fourth World War shall be waged with sticks and stones."

==Plot==
During the Cold War, two gung-ho army colonels - one American (Roy Scheider), the other Russian (Jurgen Prochnow) - wage their own private war against each other on the German–Czechoslovak border. As the colonels feud gradually escalates to frightening proportions, the threat of full-scale armed conflict between both their commands goes from possible to likely to all-but-imminent.

==Cast==
*Roy Scheider  ...  Colonel Jack Knowles
*Jürgen Prochnow  ...  Colonel Valachev
*Tim Reid  ...  Lieutenant Colonel Clark
*Lara Harris  ...  Elena
*Harry Dean Stanton  ...  General Hackworth
*Dale Dye ... Sergeant Major

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 