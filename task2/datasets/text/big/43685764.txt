Kaaval Dheivam
{{Infobox film
| name           = Kaaval Dheivam
| image          =
| image_size     =
| caption        =
| director       = K. Vijayan
| producer       =
| writer         = Jayakanthan
| screenplay     = Lakshmi Nagesh
| music          = G. Devarajan
| cinematography = Vijayan
| editing        =
| studio         = Ambal Productions
| distributor    = Ambal Productions
| released       =  
| country        = India Tamil
}}
 1969 Cinema Indian Tamil Tamil film, Lakshmi and Nagesh in lead roles. The film had musical score by G. Devarajan.   

==Plot==
Aanaikundram Jail. Superintendent K. Raghavan is a man of integrity and humaneness who views the 500 inmates as 500 books that need to be perused. He stays with his wife Alamu in the bungalow adjoining the penitentiary, and the childless couple treats the prisoners with compassion and love, for they look upon the inmates as the children they never had. Chamundi who is serving a life sentence for killing one of the 2 villains who had fatally molested his teenaged daughter Sivakami, Syed who , Kesavan who ensures that he is arrested ever so often on trivial crimes, for he prefers the comforts of the prison to the uncaring outside world…. With the unusual backdrop of a prison, JK endows each character with an interesting history and subtle idiosyncrasies.

In the nearby Allikkulam village lives Manickam- an honest, hardworking youth who is the leaseholder of Raghavan’s lands. Manickam and Kokila love each other. The scoundrel Marimuthu, an unwelcome suitor of Kokila, sees his dreams of marrying Kokila coming to nought, and schemes with his accomplices to harm Manickam. They spy Manickam and Kokila singing and romancing, and this increases their ire. Marimuthu accosts Manickam near the Aiyanaar statue. When Marimuthu speaks deprecatingly of Manickam’s lineage, Manickam sees red. He plucks the sword from the hands of the Aiyanaar statue and injures Marimuthu. Manickam is arrested and sentenced to 5 years imprisonment. Superintendent Raghavan consoles him and treats him with kindness.

The story then captures the interesting events in the prison. Of particular interest are the sequences that involve Chamundi. Chamundi sees the other man who was responsible for his daughter’s death as an inmate in the prison and manages to hack him to death one night. For this crime, Chamundi is sentenced to death, and the death sentence is carried out. In the meantime, Manickam gets the news that his mother is seriously unwell. He grieves for her and longs to pay her a visit. On his own accord Superintendent Raghavan takes the unprecedented step of permitting Manickam to go to Allikkulam to see his mother, after eliciting a promise from him that he would return to the prison by daybreak. The next day is Raghavan’s last day in service, and Raman Nair arrives to take charge as the new superintendent. Manickam has not yet returned, and Raman Nair refuses to take charge until the headcount tallies with the roster. Raghavan is confident that Manickam will return, and his confidence is not misplaced. Manickam arrives just in the nick of time. Raghavan retires; his honour and reputation untarnished.

==Cast==
*Sivaji Ganesan
*Sowkar Janaki Lakshmi
*Nagesh
*Sivakumar
*S. A. Ashokan
*S. V. Subbiah
*O. A. K. Devar
*V. Gopalakrishnan
*T. S. Balaiah in Guest Appearance
*V. K. Ramasamy (actor)|V. K. Ramasamy in Guest Appearance
*M. N. Nambiar in Guest Appearance
*R. Muthuraman in Guest Appearance

==Production==
Actor Subbiah ventured into film production. Being a voracious reader and an ardent fan of Jayakanthan, he was firm that he would adapt one of JK’s stories for cinema. Besides his liking for Subbiah, another factor that made it difficult for JK to refuse Subbiah’s current request to give one of his stories for making into a movie, was that Subbiah had appointed K. Vijayan as the director, and Vijayan, was of course, JK’s close friend. A railway employee of Ponmalai Station and a fellow comrade, Vijayan had appeared in the lead role in ‘paadhai theriyudhu paar’. Vijayan was more interested in the technical aspects of film making, and would observe keenly cinematographers, editors and directors at work. http://www.dhool.com/sotd2/990.html 

So when JK learned that Vijayan was finally getting a well-deserved break as director through Subbiah, he agreed readily to give them any of his stories for filming. When Subbiah enquired from JK as to the payment JK expected, JK said that he would be happy even if Subbiah would pay him 5 paise. Subbiah replied since JK had mentioned the number 5, he would pay JK Rs.5,000. JK acquiesced with a smile. The story that Subbiah and Vijayan selected was ‘kai vilangu’, a novel that JK had written in Kalki in 1961.  JK agreed to give Subbiah the rights to the story, with the disclaimer that he would not involve himself in writing the screenplay or dialogues. JK himself changed the title to ‘kaaval deivam’. A befitting title, considering the remarkable characterization of Superintendent Raghavan. This, juxtaposed with the Aiyanaar statue, considered to be the guarding deity in the village, which plays a silently evocative role in the story, must have inspired JK to come out with the apposite title of ‘kaaval deivam’. Subbiah enjoyed the goodwill of all in the film fraternity, and many of them came forward to appear in guest appearances in his ‘kaaval deivam’.

The film boasted of stalwarts like Sivaji Ganesan, Muthuraman, T.S. Baliah, Nambiar, Nagesh and V.K. Ramasami who all appeared in well-etched roles. Subbiah played the pivotal role of the benign Superintendent Raghavan; yet in the titles, the humble artiste chose to place his name way below that of Sivakumar (Manickam) and Ashokan (Marimuthu). Sowcar Janaki played the role of the jailor’s understanding wife Alamu, while Lakshmi played the role of Kokila. V. Gopalakrishnan, Sriranjini and G. Sakuntala were the others in the cast. The jail settings, designed by Art-Director B. Nagarajan are realistically designed. 

==References==
 

==External links==
*  
*  

 
 
 
 