The Tortured
 

{{Infobox film
| Name           = The Tortured
| image          = The Tortured.jpg
| caption        = Teaser poster
| director       = Robert Lieberman
| writer         = Marek Posival
| producer       = Mark Burg Oren Koules Carl Mazzocone Marek Posival
| starring       = Chelah Horsdal Jesse Metcalfe Erika Christensen Bill Moseley
| music          = Jeff Rona
| cinematography = Peter F. Woeste
| editing        = Rye Dahlman
| studio         = LightTower Entertainment Twisted Light Productions Twisted Pictures
| distributor    = VVS Films
| released       =  
| country        = Canada United States
| language       = English
}} mystery horror thriller film directed by Robert Lieberman and written by Marek Posival. 

==Plot==
The film tells the story of a couple, Elise Landry (Erika Christensen) and her husband Craig Landry (Jesse Metcalfe), whose life is broken when a serial killer kidnaps, tortures and kills their only son. 

The wife blames her husband, but they both understand each others pain. The trial goes on for the murder of their son and the killer gets 25 years to life. The couple is not satisfied with the verdict. Elise tells Craig to get her a gun so that she can kill the serial killer, but Craig does not agree to it. Elise leaves the house and Craig tries to kill himself while he is alone at home. Later he meets Elise to say that just killing him will not be enough. They make a plan to kidnap him. Craig gets medicines from his workplace and emergency aid kit from an ambulance. They keep an eye on the serial killer when he is taken out of the jail into a police van. Craig and Elise follow them and wait for the police van to stop at a point. 

The police stops to get some coffee. Craig distracts them while Elise is able to spike their coffees. After some time, the police pulls over and Craig is successful in taking away the van with the prisoner. In confusion and panic, Craig hits the van and it rolls over the bridge. Elise is calm to see Craig is all right and the prisoner still alive. Elisa receives a call from the detective informing that the serial killer had taken the police van and had escaped. She is able to sound normal to avoid any doubts. 

They take him to their basement and chain him just like he did to their son. They come and torture him in every way they could think of but then have to deal with the stress of torturing him. They pull themselves together and continue torturing him thinking of their son. However, it is known later that they had abducted, tortured and killed the wrong convict. The van had two convicts, one for the crime of tax evasion and other for the serial murder. The serial killer had escaped while the other convict was tortured by the couple.

Wrinkle: At the end, their torture victim writes an odd note for a tax evader.

==Cast==
* Chelah Horsdal as Liane Strader
* Erika Christensen as Elise
* Jesse Metcalfe as Craig
* Bill Moseley as Kozlowski
* Aaron Pearl as Father
* Viv Leacock as Officer Patterson
* Yee Jee Tsoa as Dr. Scanlan
* Darryl Scheelar as Officer Alvarez
* Thomas Greenwood as Ben
* Peter Abrams as Judge Stanley
* Stephen Millera as Dr. Locke
* Donita Dyer as Mrs. Holden

==Production==
The Twisted Pictures film features Erika Christensen, Bill Moseley, and Jesse Metcalfe in the leads.  It is based on the screenplay, "Act of Redress", by Czech screenwriter, Marek Posival, and was directed by Rob Lieberman. 

The scenes involving the fictional "Paz Fuels" truck stop were shot in Langley, British Columbia (city)|Langley, British Columbia, while the others scenes were filmed in Vancouver, British Columbia.  Mark Burg, Oren Koules, Carl Mazzocone and Marek Posival produced the film for Twisted Light Productions.  Minor roles are played by Zak Santiago, Lynn Colliar, Chelah Horsdal, and Fulvio Cecere. The role of Kozlowski was to originally be played by Charlie Sheen, but Sheen left the project on 30 June 2008. 

==Soundtrack==
The multi-instrumentalist Jeff Rona composed the score. 

==Release== limitedly in VOD in the US.

===Critical reception=== New York Citys Daily News wrote, "There’s really nothing here to recommend, other than bravely stalwart performances from the leads. It is bluntly written, poorly shot and edited, and cruel without being clever." 

==Notes==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 