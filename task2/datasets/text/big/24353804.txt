Justice League: Crisis on Two Earths
{{Infobox film
| name        = Justice League: Crisis on Two Earths
| image       = Justice League-Crisis On Two Earths.jpg
| caption     = DVD cover art
| director    = Lauren Montgomery Sam Liu
| producer    = Bruce Timm Bobbie Page Alan Burnett Sam Register
| writer      = Dwayne McDuffie
| starring    = William Baldwin Mark Harmon Chris Noth Gina Torres James Woods
| studio      = Warner Bros. Animation Warner Premiere DC Comics
| distributor = Warner Home Video
| released    =  
| runtime     = 75 minutes
| country     =  
| language    = English
| budget      = 
| gross       = $7,945,410  
}}
Justice League: Crisis on Two Earths is an original   animated television series and its then forthcoming sequel series Justice League Unlimited. Crisis on Two Earths was reworked from the Worlds Collide script to remove references to the TV series continuity.

The premise of Crisis on Two Earths is borrowed from the 1964   graphic novel, with a heroic  .

==Plot== alternate universe the Joker Crime Syndicate. Power Ring, Johnny Quick and Owlman (comics)|Owlman, but escapes to the Earth of the heroic Justice League by activating a dimensional travel device.
 Green Lantern, Flash and Martian Manhunter - travel to Luthors Earth.
 Slade Wilson.  Wilson releases Ultraman and explains that acceding to the Syndicates demands saves millions of lives. His daughter, Rose, however, regards him as a coward. Martian Manhunter inadvertently reads her mind and explains that as a military man her father actually holds life more dear than others. Martian Manhunter foils an assassination attempt on Rose and the pair fall in love.
 destroy entire nothing he Firestorm and Red Tornado. Superwoman and one of her lieutenants escape with the Quantum Trigger, but are followed by Batman.

Batman tricks and defeats Superwoman, and summons the League. Jonn and Rose bond, and Rose decides to learn the location of the Syndicate base to allow the Justice League to confront them. The League arrive at the Crime Syndicates moonbase with captive Superwoman, and eventually battle the Syndicate. Owlman fights off Batman and takes the QED bomb to Earth-Prime, the original universe that contained the first alternate Earth. Earth-Prime turns out to be uninhabited and lifeless, having suffered a cataclysm that caused it to leave solar orbit. Luthor speculates that a speedster might be able to vibrate and match the temporal vibration of the teleported QED device and open a portal. Batman dissuades the Flash from attempting this and Johnny Quick volunteers and opens a portal.
 looked into the abyss, only Owlman "blinked" when the abyss looked back at them, teleported with the Q.E.D. device to another lifeless, uninhabited Earth covered in ice with an orbital ring that either never coalesced into the Earths Moon, or the shattered remnants of a Moon that passed too close to the Earths Roche limit in that universe. Although Owlman does have a few seconds to abort the countdown and save himself, after realizing that an alternate version of himself would make the opposite choice regardless, Owlman decides not to abort the bombs countdown and states that "it doesnt matter". He is killed when the bomb detonates and destroys that alternate Earth but no others.

Batman returns to discover that the strain of acting as a vibratory conduit has aged Johnny Quick to near death. Before dying, Johnny correctly deduces that Batman had anticipated this and prevented the Flash from trying to save him. Martian Manhunter returns, accompanied by President Wilson and the U.S. Marines, and together they arrest Ultraman, Superwoman, and Power Ring.

President Wilson thanks the heroes, and although Rose asks Martian Manhunter to remain with her, the group return to their dimension. Wonder Woman retains the Invisible plane|still-cloaked plane stolen from Owlman, and Batman and Superman discuss a membership drive, with the five heroes summoned previously greeting the League.

==Cast== Bruce Wayne / Batman
* Mark Harmon as Superman|Kal-El / Clark Kent / Superman
* Chris Noth as Lex Luthor
* Gina Torres as Superwoman    Owlman
* Ultraman     Jonathan Adams as Martian Manhunter|Jonn Jonzz / Martian Manhunter    The Flash (credited), Aquaman (uncredited)  Princess Diana / Wonder Woman President Deathstroke|Slade Wilson
* Freddi Rogers as Rose Wilson The Jester (uncredited) Hal Jordan Power Ring (uncredited)  Captain Super Red Archer (uncredited) Model Citizen (credited), Black Canary (uncredited) Uncle Super Captain Super Jr. (uncredited) Breakdance (credited),    Secret Service Agent (uncredited) Firestorm (credited),  Black Lightning (uncredited) Richard Green Blockbuster
* Andrea Romano as Watchtower Computer (credited),  Reporter (uncredited)

==Soundtrack==
{{Infobox album
| Name = Justice League: Crisis on Two Earths (Soundtrack from the DC Universe Animated Original Movie)
| Type = Film score
| Artist = James L. Venable with Christopher Drake Digital download)
| Cover = Justice League Crisis on Two Earths (soundtrack).jpg
| Released = February 23, 2010
| Length =
| Label = New Line Records
| Reviews =
}}

{{tracklist
| collapsed       = no
| headline        = Track listing
| music_credits   = yes
| title1          = Break In
| length1         = 3:13
| title2          = Finish What the Jester Started / Main Title
| length2         = 3:24
| title3          = Only Surviving Member / Police Station / Of Course Well Help
| length3         = 3:09
| title4          = Headquarters Battle
| length4         = 4:07
| title5          = Battle in the Sky
| length5         = 3:59
| title6          = QED Monologue / Crime Syndicate / Made Men / Flash and Jon  Shipyard Battle
| length6         = 4:53
| title7          = Sup and Lex Fight Jimmy and Ultraman
| length7         = 3:07
| title8          = Owlman Multiverse Monologue / President Office Monologue
| length8         = 2:25
| title9          = Rose Garden and Ultraman Intimidation / Superwoman Toys with Bats / Batman Pissed at Luthor / Sniper Red Archer / Owlman Gets Quantum Trigger
| length9         = 4:31
| title10         = Perimeter Breach Watchtower
| length10        = 5:10
| title11         = Rose and Jon Mindmeld / Owlmans End / Batman Owlman Fight
| length11        = 4:44
| title12         = Moonbase Intro / Is This Just a Little Too Easy / Moonbase Battle
| length12        = 6:25
| title13         = Teleport
| length13        = 3:10
| title14         = Jon Says Goodbye / Johnny Burns Out / Cavalry
| length14        = 3:37
| title15         = Ending / End Credits
| length15        = 4:04
| total_length    = 57:58
}}
 

==Reception==
Justice League: Crisis on Two Earths received positive reviews.  The Worlds Finest stated "...the film ranks up there as one of Dwayne McDuffies better works in the animated DC world and even though it’s reminiscent of stories we’ve seen in animation before, the brilliant work done by Moi, the directing by Sam Liu and Lauren Montgomery, and story make it more than worth watching again." 

==References==
 
 

==External links==
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 