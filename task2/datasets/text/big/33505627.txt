The Tapes
 
 
{{Infobox film
| name           = The Tapes
| image          = the tapes film poster.jpg
| alt            =  
| caption        = Movie Poster
| director       = Lee Alliston Scott Bates
| producer       = 
| writer         = Scott Bates
| starring       = Jason Maza Mark Dusty Miller Lee Alliston
| music          = 
| cinematography = Richard Mahoney
| editing        = 
| studio         = Darkside Pictures 
Pure Film Productions
| distributor    = Exile Media Group
| released       =  
| runtime        = 80 minutes
| country        = United Kingdom
| language       = English
| budget         = £100,000
| gross          = 
}} found footage" filmmaking style. The film concerns three students who run afoul of devil worshipers.

==Plot==
The film concerns Danny and his girlfriend plus friends find out about a local swingers party in the area but instead find a cult who worships Satan. Contained in the film are subliminal messages that only the more discerning viewer will pick up. For instance, when Danny, Nathan and Gemma first enter the dilapidated farm, Nathan finds three tarot cards, the star, the fool and the magician. The film portrays a cult known as the brotherhood of Beelsebub, who appear at first to be rather eccentric villagers in the same vein as seen in the Wicker man.

==Cast==
*Jason Maza as Danny
*Mark Dusty Miller as Worshipper
*Lee Alliston as Farmer
*Mandy Lee Berger as The Barmaid
*Eddie Focarelli as Horse
*Eddie Folcarelli as Worshipper
*Jodie Mooney as Whistable Kid
*Nick Nevern as Dannys brother
*Arnold Oceng as Nathan
*Natasha Sparkes as Gemma
*Tom Waldron as Gemmas Dad

==External links==
* 

 
 
 
 
 
 

 