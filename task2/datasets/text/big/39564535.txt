Madame Makes Her Exit
{{Infobox film
| name           = Madame Makes Her Exit
| image          = 
| image_size     = 
| caption        = 
| director       = Wilhelm Thiele 
| producer       = Charles Delac   Marcel Vandal
| writer         = Paul Armont (play)   Marcel Gerbidon (play)   Franz Schulz   Wilhelm Thiele
| narrator       = 
| starring       = Liane Haid   Hans Brausewetter   Hilde Hildebrand   Ilse Korseck
| music          = Ralph Erwin
| editing        = 
| cinematography = Nicolas Farkas
| studio         = Tobis Film 
| distributor    = Tobis Film 
| released       = 12 January 1932
| runtime        = 85 minutes
| country        = Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German romance romantic comedy film directed by Wilhelm Thiele and starring Liane Haid, Hans Brausewetter, Hilde Hildebrand. It premiered on 12 January 1932.  A separate French language version Amourous Adventure was also released, directed by Thiele with a largely different cast.

==Cast==
* Liane Haid as Madame Vernier 
* Hans Brausewetter as Marcel Douzet 
* Hilde Hildebrand as Eva, the friend
* Ilse Korseck as Georgette, the maid
* Elisabeth Pinajeff as the girlfriend of Verniers 
* Paul Biensfeldt as Marcels helper
* Ernst Dumcke as Herr Venier 
* Karl Etlinger as Marcels father
* Hugo Fischer-Köppe as Gaston 
* Ernst Pröckl as Albert 
* Toni Tetzlaff as Marcels mother
* Albert Préjean

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 

 
 
 
 
 
 
 
 