Kshetram (film)
{{Infobox film
| name           = Kshetram
| image          =
| caption        =
| writer         = Paruchuri Brothers  
| story          = T Venugopal
| screenplay     = T Venugopal 
| producer       = G. Govinda Raju
| director       = T Venugopal Kick Shaam
| cinematography = M. V. Raghu
| editing        = Kotagiri Venkateswara Rao Koti
| studio         = Sri Balaji Movie Makers
| released       =  
| runtime        = 2:24:57
| country        =   India
| language       = Telugu
| budget         =
| preceded_by    = 
| followed_by    = 
}}
 Kick Shaam in the lead roles and music composed by Saluri Koteswara Rao|Koti. The film recorded as flop at box office.

==Plot== Adithya Menon) Kick Shyam) is the son of Viswanadha Rayulu falls in love with Sohini Agrawal (again Priyamani), who is eerily similar to Lakshmi. When Sohini comes into the ancestral house of Chakri, strange supernatural events happen and powerful spirits and athmas come into the picture. What happens to Sohini? Will the ritual be completed? That is the story.

==Cast==
 
* Jagapati Babu as Veera Narasimha Rayalu (Naga Penchalaiah)
* Shaam as Chakri / Chakradeva Rayalu
* Priyamani as Lakshmi (Naga Penchalamma) & Sohini Agarwal
* Kota Srinivasa Rao Adithya Menon as Viswanadha Rayulu
* Tanikella Bharani
* Brahmanandam
* Chalapathi Rao
* Posani Krishna Murali
* Vijayaranga Raju
* Brahmaji
* Rajiv Kanakala
* Jakkie
* Uttej Annapurna
* Siva Parvati Jayalalitha
* Hema
* Surekha Vani
* Manju Bhargavi
* Meena
* Alpathi Lakshmi
 

==Soundtrack==
{{Infobox album
| Name        = Kshetram
| Tagline     = 
| Type        = film Koti
| Cover       = 
| Released    = 2011
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:19
| Label       = Aditya Music Koti
| Reviews     =
| Last album  = Raaj (film)|Raaj   (2011)
| This album  = Kshetram   (2011)
| Next album  = Mugguru   (2011)
}}

Music composed by Saluri Koteswara Rao|Koti. Lyrics written by Suddala Ashok Teja. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 25:19
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Jwalaahobila
| extra1  = 
| length1 = 4:42

| title2  = Chukka Chukka
| extra2  = Dheeraj, Sravana Bhargavi
| length2 = 3:37

| title3  = Dheera Dheera Chitra
| length3 = 4:40

| title4  = Narasimha Raya 
| extra4  = Mano
| length4 = 4:53

| title5  = Rayalavari Abbayi
| extra5  = Karthik (singer)|Karthik, Malavika (singer)|Malavika, Anjana Sowmya
| length5 = 4:11

| title6  = Hey Kala
| extra6  = Karthik,Chitra
| length6 = 3:16
}}

==References==
 

 
 
 


 