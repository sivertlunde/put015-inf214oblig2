Morgan M. Morgansen's Date with Destiny
{{Infobox film
| name           = Morgan M. Morgansens Date with Destiny
| image          = 
| caption        = 
| director       = Joseph Gordon-Levitt
| producer       = Jared Geller
| writer         = Sarah Daly
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Joseph Gordon-Levitt Lexy Hulme Lawrie Brewster Nathan Johnson
| cinematography = Lawrie Brewster
| editing        = Joseph Gordon-Levitt Lawrie Brewster
| studio         = hitRECord
| distributor    = hitRECord
| released       = 
| runtime        = 
| country        = U.S.
| language       = English
| budget         = $4,000 (estimated)
}}

Morgan M. Morgansens Date with Destiny is a short romantic silent film directed, edited by and starring Joseph Gordon-Levitt. It co-stars Lexy Hulme.

It is Gordon-Levitts directing debut. A sequel entitled   was released the same year.

==Plot==
The silent film, except for narration throughout by Gordon-Levitt, follows the first date between Morgan M. Morgansen (Gordon-Levitt) and a young woman named Destiny (Hulme). Backed up by a sour-turned-sentimental waiter - or the "foodpenguin" (Brewster) - and a band of cartoon feline musicians (among others), Morgan ultimately manages to woo Destiny. After meeting with Destiny at a restaurant and ordering a cooked rabbit, Morgan discovers Destinys vegetarianism, and decides to eat only the vegetables around the plate rather than eat the rabbit meat and appall his alarmed date. Appreciative of Morgans sacrifice at dinner for the sake of her comfort, Destiny begins to kiss him in the street, overlooked by the approving foodpenguin. The two rush off to Morgans abode and it is stated that Morgan, lying in bed that night, needed no longer feel alone.

==Cast==
*Joseph Gordon-Levitt as Morgan M. Morgensen / Narrator
*Lexy Hulme as Destiny
*Lawrie Brewster as Food Penguin

==Production==
The short was produced by Gordon-Levitts online production company hitRECord with contributions from artists all over the world. Gordon-Levitt directed and starred in the piece which was written and conceived by Irish writer Sarah Daly.  The visual effects were created by Scotlands Lawrie Brewster  with illustrations from Jenyffer Maria. Nathan Johnson composed the score which was then assembled from the contributions of musicians online.

With homages to influences as varied as Lewis Carroll, Sigmund Freud and Georges Méliès.

==Premiere==
The short film premiered at the 2010 Sundance Film Festival.

==References==
 

==External links==
*  

==See also==
* 

 

 
 
 
 
 
 