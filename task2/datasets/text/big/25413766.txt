The Restless Breed
{{Infobox film
| name           = The Restless Breed
| image          = Restbrpos.jpg
| caption        = Original film poster
| director       = Allan Dwan
| producer       = Edward L. Alperson Richard Einfeld Charles B. Fitzsimmons Ace Herman Steve Fisher Rhys Williams
| music          = Edward L. Alperson Jr.
| cinematography = John W. Boyle 
| production design = Ernst Fegté 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 86 min. 
| country        = United States English
| budget         = 
}}
 1957 western film, directed by Allan Dwan and starring Scott Brady and Anne Bancroft.

==Plot==
1865: Lawyer Mitch Baker is called into an office of the United States Secret Service to be told that his father was murdered in the border town of Mission, Texas. He was betrayed to Newton by an informer whilst on a mission investigating a group of gunrunners called "Newtons Raiders" supplying the forces of Emperor Maximilian I of Mexico with weapons arousing the ire of the United States who wants a Republican Mexico.
 Reverend of the Gospel. Simmons also has a childrens shelter of half breed children that neither their Indian or American fathers and mothers want. The oldest is Angelita who aspires to be a dancer in the local saloon.

Angelita is fascinated, then falls in love with Mitch.  As no one in town know who Mitch is or why he came the other children imagine him an Archangel, especially as Mitch turns the table on several assassination attempts as he waits for Newton to arrive to exact his revenge. Arriving before Newton is Marshal Evans who knew Mitchs father and tells Mitch his father would be ashamed of what he was doing. He also threatens to imprison Mitch and charge him with murder if he kills one more of Newtons men. Angelita and Simons are glad to know Mitchs mission and urge him to let Marshal Evans arrest Newton, but Newton rides in with a gang of riders.

==Cast==

*Scott Brady (Mitch Baker)
*Anne Bancroft (Angelita)
*Jay C. Flippen (Marshal Steve Evans) Jim Davis (Ed Newton) Rhys Williams (Rev. Simmons) 
*Leo Gordon (Cherokee)
*Scott Marlowe (James Allan)
*Eddy Waller (Caesar)
*Harry Cheshire (Mayor Johnson)
*Myron Healey (Sheriff Mike Williams)
*Gerald Milton (Jim Daley - Bartender) 
*Dennis King Jr. (Hotel Clerk)
*James Flavin (Secret Service Chief)
*Clegg Hoyt (Spud)
*Marilyn Winston (Banee)

 

 
 
 
 
 


 