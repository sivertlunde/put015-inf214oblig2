The Squad (film)
 
{{Infobox film
| name           = The Squad
| image          = The Squad (film) film poster.jpg
| caption        = Theatrical release poster
| director       = Jaime Osorio Marquez
| producer       = Federico Durán
| writer         = Tania Cardenas Jaime Osorio Marquez
| starring       = Juan David Restrepo
| music          = 
| cinematography = Alejandro Moreno
| editing        = Felipe Guerrero Sebastián Hernández
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Colombia
| language       = Spanish
| budget         = 
}}

The Squad ( ) is a 2011 Colombian horror film and the directorial debut for Jaime Osorio Marquez.     The film was released in Colombia on 7 October 2011 and follows a squadron of men that are sent to investigate a mountain military base but instead discovers something deadly. 

==Plot==
After a secret military base ceases all communications, an anti-guerrilla commando unit is sent to the mountainous location to discover what exactly happened. The squad expects to discover that the base was attacked and taken over by guerrilla units, but instead find only a lone woman (Daniela Catz) wrapped in chains. Despite some concern that she may have been shackled for a reason as she was hidden away and had black magic items placed all around her, the men release her and begin to try to find out what occurred prior to their arrival. They find the missing soldiers logs, which further suggests that the woman should have remained in captivity. Only now realizing their mistake and with night settling in, the men find that the woman has since disappeared. As paranoia and distrust grows, the squad finds that it may be difficult to survive the night.

==Cast==
* Juan David Restrepo as Ramos
* Andrés Castañeda as Sargento
* Mauricio Navas as Teniente
* Mateo Stevel as Parra
* Daniela Catz as Mujer
* Nelson Camayo as Fiquitiva Andres Torres as Arango
* Juan Pablo Barragan as Ponce
* Julio César Valencia as Robledo
* Alejandro Aguilar as Cortez

==Reception==
Critical reception for The Squad has been mixed.  Flickering Myth and Geeks of Doom both gave lukewarm reviews for the film,  and Geeks of Doom wrote "For some, The Squad will impress with its tense, psychological drama. For others, it will fail to deliver on its interesting premise. It’s a polished, well-crafted thriller, but it just isn’t all that thrilling to me. Regardless, Marquez’s feature film debut signifies a promising future for the Colombian filmmaker."  Bloody Disgusting panned the film overall, stating that the idea had promise and that they would look for future films by Jaime Osorio Marquez’, but that the film did not live up to its full potential and that some ideas, specifically the "witch" character, could have been better utilized. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 