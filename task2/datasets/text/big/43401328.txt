Vasiliki (film)
{{Infobox film
| name           = Vasiliki
| image          =
| caption        =
| director       = Vangelis Serdaris
| producer       =
| writer         = Vangelis Serdaris
| starring       = Paschalis Tsarouhas  Tamila Koulieva  Vasilis Papanikas  Vivian Kontomari 
| music          = Giorgos Tsangaris
| cinematography = Aggelos Viskadourakis
| editing        = Kostas Raftopoulos
| distributor    =
| released       =  
| runtime        = 135 minutes
| country        = Greece
| language       = Greek
| gross          =
}}
Vasiliki ( ) is a Greek film directed by Vangelis Serdaris. It released in 1997 and star Paschalis Tsarouhas and Tamila Koulieva. The film received the Greek Film Critics Association Awards. Also, Paschalis Tsarouhas won the award for best actor in the Cairo International Film Festival and Giorgos Tsangaris won the award for best music in Greek State Film Awards.

==Plot==
The film presents the riotous years immediately after the Greek Civil War. Vasiliki is the wife of a Greek communist guerrilla during Greek Civil War. The local gendarmerie arrests her because brought food to her husband. The chief of gendarmerie charmed by her beauty rapes her. Then, he moved in other city but returned to ask her to follow him (meanwhile her husband had killed in the war). His love for the wife of a communist is the cause to be expelled by the gendarmerie. Thus he moves in the North Greece doing plans for his professional future. After the sudden death of his partner his plans fails. He seeks help from a rich industrialist but the industrialist exploits his ideas for himself. The impediments that he meets make him more and more violent resulting to miss her wife and eventually to reach the crime killing the industrialist. 

==Cast==
*Paschalis Tsarouhas as Leonidas Loufakos
*Tamila Koulieva as Vasiliki
*Vasilis Papanikas as Giagos Hrysomoglou
*Vivian Kontomari as Smaragda
*Haris Gregoropoulos
*Nikos Katis
*Michalis Iatropoulos

==Awards==
{| class="wikitable"
|+ List of awards and nominations   
! Award !! Category !! Recipients and nominees !! Result
|- Cairo International 1998 Cairo Best Actor||Paschalis Tsarouhas|| 
|- Mar del 1998 Mar Best Actor||Paschalis Tsarouhas|| 
|- Greek State 1997 Greek Best Music||Giorgos Tsangaris|| 
|- Greek Film Best Film||Vangelis Serdaris|| 
|}

==References==
 

==External link==
* 

 
 