Sankarabharanam
{{Infobox film
| name           = Sankarabharanam
| image          = sankarabharanam.jpg
| director       = K. Viswanath Jandhyala   (dialogues )  Chandra Mohan
| producer       =Edida Nageswara Rao Aakasam Sriramulu
| released       = 4th February, 1980
| runtime        = 137 min
| cinematography = Balu Mahendra
| editing         = G. G. Krishna Rao
| distributor     =Poornodaya Movie Creations
| country        = India
| language       = Telugu
| music          = KV Mahadevan
}} musical drama film, directed by Kaasinathuni Viswanath|K. Viswanath and produced by Poornodaya Movie Creations.  The soundtrack was composed by K. V. Mahadevan, and remained a chartbuster. The film is listed among CNN-IBNs list of hundred greatest Indian films of all time. 
 National Film Awards including the National Film Award for Best Popular Film Providing Wholesome Entertainment, and five state Nandi Awards.
  The film was premiered at the 8th International Film Festival of India,   the Tashkent Film Festival, and the Moscow International Film Festival held in May 1980.  The film has also won the Prize of the Public at the Besancon Film Festival of France in the year 1981. 

==Plot==
The film begins with an introduction by Viswanath, "Shishurvetti pashurvetti, vetti gaana rasam phanihi (Music is enjoyed equally well by babies, animals and even snakes). We hope you appreciate our effort in bringing you the Jeeva Dhara (Lifestream) of Indian classical music."
 Carnatic singer. He is immersed in sangeeta rasaamruta (Nectar of Music) with his nirantara saadhana (uninterrupted practice). People come in huge numbers to listen to his voice and consider him a great man. He has mastered the raga Sankarabharanam and hence is eponymous with the same. Tulasi, (Manju Bhargavi) is a prostitutes daughter who has great interest in music and dance. She is also an admirer of Sastri and learns music from him when he used to teach his own daughter along the riverside. But her mother wants her to become a prostitute to earn money.

One day a rich client of her mothers rapes Tulasi. He insults Sastri saying that now that he is done with Tulasi, she could go and flirt with Sastri all she wanted. Enraged by the disgrace towards Sastri, her guru, she kills the client. Sastri tries to save her by consulting his lawyer brother (Allu Ramalingaiah), who wins the case in Tulasis favour and her mother is sent to jail.
Then Sastri brings her to his home where other people insult him, as Tulasi is a murderer and daughter of a prostitute, while Sastri is a devout Brahmin. Tulasi moves out of his house as she does not want him to be insulted because of her but hopes to show her gratitude towards him.

Ten years have passed, pop music is now popular in India and Sastri loses his classical music audience. He now lives in a small house with his grown up daughter. When Tulasi comes to know of Sastris plight she tries to help him financially but gives the money through someone else. Tulasi inherits all of her mothers property and utilizes all of it to help him. She also asks her son to go to Sastris home and learn classical music from him. After managing to get an entry to Sastris home, he starts learning Carnatic music from the maestro.
 Chandra Mohan, a Amateur|dilettante, falls in love with Sastris daughter. Although Sastri rejects the alliance at first, he later agrees after learning of the mans interest in classical music. Tulasi then arranges for a concert on the day of Sastris daughters wedding, where Sastri finds his lost audience return to hear his voice. Sastri sings at the concert and halfway through it suffers a heart attack and finds his new disciple, Tulasis son, take over the concert from him. As he watches him with pride, he also sees Tulasi and finds out that the boy is indeed, Tulasis son. After the performance, he symbolically anoints the boy as heir to his music. Tulasi comes to her guru and falls down at his feet. The film ends with death of both Sastri and Tulasi on the stage.

== Cast ==
  
* J. V. Somayajulu  as  Sankarabharanam Sankara Sastry
* Manju Bhargavi  as  Tulasi Chandra Mohan Rajalakshmi  as  Sankara Sastrys daughter Sarada
* Allu Ramalingaiah  as  Lawyer and Sankara Sastrys friend
* Nirmalamma
* Sakshi Ranga Rao
* Dubbing Janaki
* Jith Mohan Mishra
* Baby Tulasi Ram  as  Tulasis son
* Baby Varalakshmi  as  veena artist
* Jhansi  as  Tulasis distant relative
* Bhargahavi
* Nirmalamma
* Pushpa Kumari
* Varalakshmi
* Arja Janardhana Rao
* Edida Sriramprasad
* S. Bheemeswara Rao
* Dhum
* Srigopal
* Ganeswara Rao
 

==Reception==

===Box office performance===
* The film released in only one theatre and opened to empty hall.    But it later turned out to be the biggest hit of 1980 and also one of the legends of Telugu industry owing to the positive feedback from the audience.
* The film had a 216-day run at Royal theatre, Hyderabad, Andhra Pradesh|Hyderabad.  Sruthi Layalu, Swarna Kamalam, Sirivennela, and Swati Kiranam.   
* The original Telugu version was dubbed into Malayalam which released across Kerala with overwhelming response.  
* It was remade in Hindi as Sur Sangam (1986) with Jayaprada. 
* The film was dubbed in Tamil and Malayalam.

After this movie release number of people started learning classical music in Andhra Pradesh. Sankarabharanam songs most sung songs in telugu film song concerts.

===Critical response===
* Film critic Gudipoodi Srihari called it as the best Telugu film he has seen after Mayabazar. 
* On the centenary of Indian cinema in April 2013, Forbes (India)|Forbes included J. V. Somayajulus performance in the film on its list, "25 Greatest Acting Performances of Indian Cinema". 

==Awards==
 
|- 1980
| Kasinathuni Viswanath
| National Film Award for Best Popular Film Providing Wholesome Entertainment - Golden Lotus Award
|  
|-
| K. V. Mahadevan
| National Film Award for Best Music Direction
|  
|-
| S. P. Balasubrahmanyam
| National Film Award for Best Male Playback Singer
|  
|-
| Vani Jayaram
| National Film Award for Best Female Playback Singer
|  
|-
| Kasinathuni Viswanath
| Nandi Award for Best Feature Film - Gold
|  
|-
| S. P. Balasubrahmanyam
| Nandi Award for Best Male Playback Singer
|  
|-
| Vani Jayaram
| Nandi Award for Best Female Playback Singer
|  
|-
| K. V. Mahadevan
| Nandi Award for Best Music Director
|  
|-
| Veturi Sundararama Murthy ("Sankara Naada Sareerapara")
| Nandi Award for Best Lyricist
|  
|-
| J. V. Somayajulu
| Filmfare Award for Best Actor – Telugu
|  
|}

==Soundtrack== Carnatic based, was composed by K.V. Mahadevan.
M. Balamuralikrishna was the original choice for the male playback singer, due to the heavy classical content of the compositions. But K.V. Mahadevan, having faith in the mettle of S. P. Balasubrahmanyam, insisted on him taking up this challenge.

{{Tracklist
| collapsed       =
| headline        = Songs 
| extra_column    = Playback
| all_music       = K. V. Mahadevan
| lyrics_credits  = yes

| title1  = Broche varevaru ra 
| lyrics1 = Mysore Vasudevachar
| extra1  = S. P. Balasubramanyam, Vani Jayaram

| title2  = Dorakunaa Ituvanti Seva
| lyrics2 = Veturi Sundararama Murthy
| extra2  = S.P. Balasubramanyam. Vani Jayaram

| title3  = Manasa Sancharare
| lyrics3 = Sadasiva Brahmendra
| extra3  = S.P. Balasubramanyam, Vani Jayaram

| title4  = Maanikya Veena
| note4   = Poem Mahakavi Kalidasu
| extra4  = S.P. Balasubramanyam

| title5  = Omkaara Naadaanusandhanam
| lyrics5 = Veturi Sundararama Murthy
| extra5  = S. P. Balasubrahmanyam, S. Janaki

| title6  =  Paluke Bangaaramaayena 
| lyrics6 = Bhadrachala Ramadasu
| extra6  = S.P. Balasubramanyam, Vani Jayaram

| title7  = Raagam Taanam Pallavi
| lyrics7 = Veturi Sundararama Murthy
| extra7  = S.P. Balasubramanyam

| title8  = Sankaraa Naadasareeraparaa
| lyrics8 = Veturi Sundararama Murthy
| extra8  = S.P. Balasubramanyam

| title9  = Saamaja Varagamana
| lyrics9 = Veturi Sundararama Murthy
| extra9  = S. Janaki, S.P. Balasubramanyam

| title10  = Ye Teeruga Nanu
| lyrics10 = Bhadrachala Ramadasu
| extra10  = Vani Jayaram
}}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 