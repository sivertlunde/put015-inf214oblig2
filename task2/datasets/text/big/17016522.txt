Beyond the Limits (film)
{{Infobox Film name = Beyond the Limits image = Beyond_the_Limits.jpg image_size = caption = Official DVD Cover director = Olaf Ittenbach producer = Yazid Benfeghoul Olaf Ittenbach Frank Geiger writer = Olaf Ittenbach Thomas Reitmair narrator =  starring = Darren Shahlavi Xenia Seeberg Timo Rose music = Les Gilles cinematography = Stefan Biebl editing = Eckard Zerzawy studio = IMAS Filmproduktion brave new work Filmproduktions distributor = Laser Paradise released = 23 March 2003 runtime = 100 minutes country = Germany language = English German German
|budget =  gross =  followed_by = 
}} thriller film directed by Olaf Ittenbach and stars Xenia Seeberg, Darren Shahlavi and Timo Rose. 

==Plot==
The young reporter interviewed the gravedigger Vivian Frederick, who told her about the recently deceased mobster Robert Downing, this comes in the attempt to steal an ancient relic with occult into a party that ends in a bloodbath. The relic itself has already been provided here for death and terror in the Middle Ages and preserves a terrible secret. 

==Cast==
* Darren Shahlavi as Dennis
* Russell Friedenberg as Tom Brewster
* Hank Stone as James Flynn
* David Creedon as David Deming Joe Cook	as Paul Pattuchi
* Christopher Kriesa as Frederick
* Kimberly Liebe as Vivian
* Natacza Boon as Annabelle Branagh
* James Matthew-Pyecha as Jimmie Levinson
* Simon Newby as Mortimer
* Daryl Jackson	as Robert Downing
* Twin as Courtney
* Saskia Lange as Marianne
* Mehmet Yilmaz	as Christopher
* Matthias Rimpler as Brad
* Jeff Motherhead as Howard
* Marika Elena David as Denise
* Melanie Sigl as Laura
* Eric Sumner as Father
* Karen Ponader as Woman
* Jacek Gluszko	as Soldier
* Erich Amerkamp		
* Andreas Pape as Farmer
* Thomas Reitmair as Rick
* Timo Rose as Knight
* Xenia Seeberg as Clarice
* Matthias Hues as Big Blond Knight
* Robert Seamans as Patrolman

==Prizes==
Olaf Ittenbach won 2003 the Golden Glibb at Weekend of Fear in Nürnberg, Germany for his film. 

==Release==
The film was released on 23 March 2003 as the Direct-to-video project. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 


 
 