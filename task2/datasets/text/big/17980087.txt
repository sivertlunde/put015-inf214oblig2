The Subject Was Roses (film)
{{Infobox film
| name           = The Subject Was Roses
| image          = SubjectWasRosesPoster.JPG
| caption        = Original poster
| director       = Ulu Grosbard Edgar Lansbury
| writer         = Frank D. Gilroy
| starring       = Patricia Neal Jack Albertson Martin Sheen
| music          = Lee Pockriss
| cinematography = Jack Priestley
| editing        = Gerald B. Greenberg
| studio         =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,375,000 (US/ Canada rentals) 
}}
 Pulitzer Prize-winning play of the same title.

The film stars Patricia Neal, Martin Sheen and Jack Albertson. Albertson won an Academy Award for his performance.

== Plot ==
 Bronx home following World War II, Timmy Cleary discovers his middle class parents have drifted apart and constantly quarrel at the least provocation. Once closer to his mother Nettie, the young veteran finds himself bonding with his salesman father John, but he tries to remain neutral when intervening in their disputes.

En route home after a day trip to the familys summer cottage with his father, Timmy purchases a bouquet of roses and suggests John present them to his wife. Nettie is thrilled by his apparent thoughtfulness, and the three spend the evening nightclubbing in Manhattan. When an inebriated John (whose infidelities have by now been referenced in the film) attempts to make love to his wife later that night, Nettie rejects his advances, suggesting he go to "one of his whores", and breaks the vase of flowers, prompting her husband to reveal it really was Timmy who bought them.   

The following morning, while John is at Sunday Mass (liturgy)|Mass, Timmy accuses his mother of trying to make him choose between his parents, and she goes out to allow both of them time to calm down. When she returns, she finds John arguing with their half-drunk son. Realizing the domestic situation is not likely to improve, Timmy announces he is leaving home, a decision his parents grudgingly accept. When he changes his mind, his father insists he stick to his plan, and the three eat breakfast together before he departs.

== Cast ==

*Patricia Neal as Nettie Cleary
*Jack Albertson as John Cleary
*Martin Sheen as Timmy Cleary

== Production ==
 Tony Award Broadway production of Frank D. Gilroys play.  Joining him were original cast members Jack Albertson and Martin Sheen as John and Timmy Cleary; Patricia Neal replaced Irene Dailey in the role of Nettie.

The film was a significant comeback for Neal, who was recovering from a debilitating stroke she had suffered three years earlier and hadnt appeared on screen since In Harms Way in 1965. During filming, the actress was beset with memory problems and physical limitations she struggled to overcome, and in her autobiography As I Am, she described the experience as a career milestone that convinced her she still was a good actress. She also discussed her effort to memorize a five-page monologue she was required to do in one take and her pride at doing so successfully. 

The film was shot on location in New York City,Spring Lake, New Jersey and Belmar New Jersey   .

The soundtrack includes "Who Knows Where the Time Goes?" (written by Sandy Denny) and "Albatross," both performed by Judy Collins (who wrote the later song).

== Critical reception ==
 Philco Playhouse . . . The play has been brought to the screen with flat, fatal fidelity by Mr. Gilroy . . . Quite awkwardly (since it makes you aware of everything else you are not seeing), the one-set play has been opened up with several excursions outside the Bronx apartment. Whats worse, Mr. Grosbard has retained the Broadway pace, which is particularly evident in the performances of Mr. Albertson and Mr. Sheen . . . The tempo of the acting often seems to be outrunning the movie itself . . . Miss Neals presence . . . gives the movie an emotional impact it wouldnt otherwise have . . . She has, in fact, simply too much style and wit for this kind of monosyllabic nonsense."  

Roger Ebert of the Chicago Sun-Times thought Gilroys "extraordinary play . . . has been filmed with the greatest care, but it fails as a movie. It is hard to say exactly why. Theres nothing obviously wrong, but when you walk out you dont feel as if youve been there. Something was missing." He added, "Part of the problem is with the actors, I think . . . Albertson and Sheen . . . talk loudly, their movements are too obvious, they are trying to project . . . Miss Neal, who knows the movies, is better suited to the medium. She holds back, she suggests more than she reveals, and when all three actors are on camera her performance makes the other two look embarrassingly theatrical. And there is where the movie fails."  

Variety (magazine)|Variety said, "The terrific writing, which top-notch performances make more magnificent, displays a wide range of human emotions, without recourse to cheap sensationalism or dialog. Grosbards perceptive direction keeps the bickering and banter from becoming shrill histrionics."  

TV Guide rated the film four stars, citing "the terrific acting, sharp writing, and outstanding direction from Grosbard" and adding, "Never does the emotion explode into oratory, so almost every scene has an underlying tension that continues to bubble."  

== Awards and nominations ==
 The Lion Funny Girl. Daniel Massey in Star! (film)|Star!.

== References ==
 

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 
 
 