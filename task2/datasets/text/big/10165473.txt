Time for Revenge
{{Infobox film
| name           = Tiempo de revancha
| image          = Tiempoderevancha1.jpg
| image_size     = 250px
| caption        = Spanish language theatrical poster
| director       = Adolfo Aristarain
| producer       = Héctor Olivera Luis O. Repetto
| writer         = Adolfo Aristarain
| starring       = Federico Luppi Haydée Padilla Julio De Grazia Ulises Dumont Aldo Barbero Enrique Liporace Arturo Maly Rodolfo Ranni
| music          = Emilio Kauderer
| cinematography = Horacio Maira Eduardo López
| studio         = Aries Cinematográfica Argentina
| distributor    = Aries Cinematográfica Argentina Televicine International
| released       =  
| runtime        = 112 min.
| country        = Argentina
| language       = Spanish
| budget         =
| gross          =
}} crime drama film written and directed by Adolfo Aristarain and starring Federico Luppi, Julio De Grazia, Haydée Padilla and Ulises Dumont. It was produced by Héctor Olivera and Luis O. Repetto. The music was composed by Emilio Kauderer. The film premiered in Argentina on July 30, 1981, and won 10 awards, including the Silver Condor for Best Film and Best Film (ex aequo) in the Montréal World Film Festival. 

Tiempo de revancha is not only considered as a classic in Latin American cinema and a cult film, but also as a powerful allegory that deals directly with National Reorganization Process|Argentinas last civil-military dictatorship, which was still ruling the country when it was released. 

== Synopsis == union organizing demolition worker copper pit. Di Toro was supposed to pretend to be Mute as a consequence of an explosion, and Pedro would corroborate his story. Things dont go as planned and Di Toro loses his life, leaving Pedro to continue the plan on his own, while pretending to be Mute. However, when the company finally agrees to an economical settlement, Pedro refuses to accept, searching justice primarily, and his case goes to the courts. This event changes Pedros life for ever.

== Cast ==
* Federico Luppi ... Pedro Bengoa
* Haydée Padilla ... Amanda Bengoa
* Julio De Grazia ... Larsen
* Ulises Dumont ... Bruno Di Toro
* Jofre Soares ... Aitor
* Aldo Barbero ... Rossi
* Enrique Liporace ... Basile
* Arturo Maly ... García Brown
* Rodolfo Ranni ... Torrens
* Jorge Hacker ... Don Guido Ventura
* Alberto Benegas ... Golo
* Ingrid Pelicori ... Lea Bengoa
* Jorge Chernov ... Jorge
* Cayetano Biondo ... Bautista
* Marcos Woinsky ... Polaco
* Marcela Sotelo
* Lidia Catalano
* Cristina Arocca
* Héctor Calori
* Carlos Verón
* Carlos Trigo
* Osvaldo De la Vega
* Aldo Pastur
* Enrique Latorre
* Jorge Velurtas
* Rafael Casadó
* Enrique Otranto

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 