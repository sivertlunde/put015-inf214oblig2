Arise, My Love
{{Infobox film 
| name           = Arise, My Love
| image          = Arisemylove1940.jpg
| image_size     = 150px
| caption        = Theatrical release poster
| director       = Mitchell Leisen
| producer       = Arthur Hornblow Jr.
| based on       =  
| writer         = Billy Wilder Charles Brackett Jacques Théry
| starring       = Claudette Colbert Ray Milland Dennis OKeefe
| music          = Victor Young
| cinematography = Charles Lang
| editing        = Doane Harrison
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         =
}}  interventionist message, it tells the love story of a pilot and lady journalist who meet in the latter days of the Spanish Civil War and follows them through the early days of World War II.  Colbert once said that Arise, My Love was her personal favorite film of all the ones she had made.    

Arise, My Love is based on the true story of Harold Edward Dahl. During the Spanish Civil War Dahl, who was fighting as a pilot for the Spanish Republican Air Force, was shot down and taken as prisoner of war. Initially sentenced to death, there were some diplomatic movements to free Dahl. His first wife, Edith Rogers, a known singer of impressive beauty, was said to have visited Francisco Franco herself to plead for his life. He remained in prison until 1940 and then returned to the United States. 

==Plot==
American pilot Tom Martin (Ray Milland) is a soldier of fortune who went to Spain to fight in the Spanish Civil War. During the summer of 1939, he is languishing in a prison cell while awaiting execution. Unexpectedly granted a pardon on the morning that he is to face a firing squad, Toms release has been managed by reporter Augusta "Gusto" Nash (Claudette Colbert), who posed as his wife. When the prison governor learns of the deception, the pair has to run for their lives. 
 RAF while Gusto remains in France as a war correspondent. At the fall of Paris, Tom is reunited with Gusto, and both decide to return home to convince Americans that a real danger awaits.

==Quotes==
*Father Jacinto: "This is my first execution."
*Tom Martin: "Dont worry, Father, its mine too."
----
*Tom Martin: "You know, its a funny thing that you of all people should be sitting beside me. Youre precisely my type."
*Augusta Nash: "Mmm-hmm. How long were you in that prison?"
----
*Mr. Phillips: "Gusto Nash, youre fired, as of immediately!"
*Augusta Nash: "Oh, its not true!"
*Mr. Phillips: I know its not true. I just wanted to taste the words. Sheer rapture!"
----
*Mr. Phillips: "Im not happy. Im not happy at all!"

==Cast==
  
* Claudette Colbert as Augusta (Gusto) Nash
* Ray Milland as Tom Martin
* Dennis OKeefe as Joe "Shep" Shepard
* Walter Abel as Mr. Phillips
* Dick Purcell as Pinky OConnor
* George Zucco as Prison Governor
* Frank Puglia as Father Jacinto
 
* Esther Dale as Secretary
* Paul Leyssac as Bresson
* Ann Codee as Mme. Bresson
* Stanley Logan as Col. Tubbs Brown
* Lionel Pape as Lord Kettlebrook
* Aubrey Mather as Achille
* Cliff Nazarro as Botzelberg
 

==Production==
  in Arise, My Love .]]
Filming for Arise, My Love began on June 24, 1940 on the Paramount lot and lasted until mid-August 1940, with the script continuously updated to incorporate actual events, such as the sinking of the SS Athenia and the signing of the  . Dream Lover, composed by Victor Schertzinger, lyrics by Clifford Grey, was sung and hummed by Claudette Colbert. The song was originally introduced in The Love Parade (1929).
  Stinson A trimotor was featured in the film as a Spanish aircraft. Noted aerial coordinator, Paul Mantz flew the aircraft. 

==Reception==
Arise, My Love Bosley Crowther,  film reviewer for The New York Times considered the film a cynical way to exploit the war in Europe. "... it is simply a synthetic picture which attempts to give consequence to a pleasant April-in-Paris romance by involving it in the realities of war—but a war which is patently conceived by some one who has been reading headlines in California. Miss Colbert and Mr. Milland are very charming when tête-a-tête. But, with Europe going up in flames around them, they are, paradoxically, not so hot. Same goes for the film." Crowther, Bosley.   The New York Times, October 17, 1940. 

==Adaptations==
Arise, My Love was adapted as a radio play on the June 8, 1942 episode of Lux Radio Theater with Milland joined by Loretta Young. It was also presented on the June 1, 1946 episode of Academy Award Theater, with Milland reprising his role. 

==Awards and nominations== Best Story Best Music Best Cinematography Best Art Direction (Hans Dreier and Robert Usher).   The New York Times, 2010. Retrieved: October 31, 2014. 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Dick, Bernard F. Claudette Colbert: She Walked in Beauty. Jackson, Mississippi: University Press of Mississippi, 2008, ISBN 978-1-60473-087-6.
* Farmer, James H. Celluloid Wings: The Impact of Movies on Aviation. Blue Ridge Summit, Pennsylvania: Tab Books Inc., 1984. ISBN 978-0-83062-374-7.
* Paris, Michael. From the Wright Brothers to Top Gun: Aviation, Nationalism, and Popular Cinema. Manchester, UK: Manchester University Press, 1995. ISBN 978-0-7190-4074-0.
 

==External links==
*  
*  
*  
*   on   June 1, 1946 
*   by Jeanette MacDonald

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 