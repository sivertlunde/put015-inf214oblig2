Under the Sand
{{Infobox film
| name           = Sous le sable
| image          = Sous Le Sable.jpg
| caption        = Theatrical release poster
| director       = François Ozon
| producer       = Olivier Delbosc Marc Missonnier
| writer         = François Ozon Emmanuèle Bernheim
| starring       = Charlotte Rampling Bruno Cremer
| music          = Philippe Rombi
| cinematography = Antoine Héberlé Jeanne Lapoirie   
| editing        = Laurence Bawedin
| distributor    = Haut et Court
| released       =  
| runtime        = 96 minutes
| country        = France
| language       = French
| budget         = 
}} 2000 France|French drama film directed and written by François Ozon. The film was nominated for three 27th César Awards|César Awards and was critically well received. It stars Charlotte Rampling and Bruno Cremer.

==Plot== swim in the sea while his wife is sunbathing and never returns, vanishing without trace. No body is found and several questions arise. Has he left her, committed suicide, drowned? With no body to mourn, she pretends that he is still alive and present in their apartment. Her life becomes characterized by denial, cloaked in enigmatic complexity and emotional disorientation. When the badly decomposed body is found in the sea and identified from dental records, she refuses to admit it is Jean. She imagines she will see him alive as he was on the beach.

==Cast==
*Charlotte Rampling as Marie Drillon 
*Bruno Cremer as Jean Drillon 
*Jacques Nolot as Vincent 
*Alexandra Stewart as Amanda  Pierre Vernier as  Gérard 
*Andrée Tainsy as  Suzanne 
*Maya Gaugler as German woman 
*Damien Abbou as Chief lifeguard 
*David Portugais as Young lifeguard 
*Pierre Soubestre as  Policeman 
*Agathe Teyssier as  Manager of luxury store 
*Laurence Martin as  Apartment seller 
*Jean-François Lapalus as Paris doctor 
*Laurence Mercier as  Paris doctors secretary 
*Fabienne Luchetti as  Pharmacist

==Production==
 ]]
===Filming=== Landes department including Lit-et-Mixe, Mimizan|Mimizan-Plage beach and at Saint-Julien-en-Born.

==Awards== Best Film Best Director Best Actress César nominations for Ozon and Rampling.

==Comparison ==

The plot of Sous le Sable bears some resemblance to that of the 1990 British romantic comedy Truly, Madly, Deeply.

==Reception==
The Swedish filmmaker Ingmar Bergman admired Sous le sable, claimed that he watched the film several times. 

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 

 
 