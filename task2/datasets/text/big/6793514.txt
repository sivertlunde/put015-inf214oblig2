Fuori Dal Mondo
 
{{Infobox film
| name           = Not of this world
| image          = Fuori_dal_mondo.jpg
| image_size     = 
| caption        = 
| director       = Giuseppe Piccioni
| producer       = Lionello Cerri
| writer         = 
| narrator       = 
| starring       = Margherita Buy Silvio Orlando 
| music          = Ludovico Einaudi
| cinematography = Luca Bigazzi
| editing        = Esmeralda Calabria
| distributor    = 
| released       = 26 March 1999 (Italy) 25 February 2000 (U.S.) 8 March 2002 (Spain)
| runtime        = 100 Min
| country        = Italy Italian
| budget         = 
}}

Not of this World or Fuori Dal Mondo is an Italian drama film directed by Giuseppe Piccioni. The translation for Fuori Dal Mondo is outside from the world.

==Plot==
The movie is about a nun, whose life is upended when she is handed an abandoned infant in a park. She takes the baby to the hospital and proceeds to track down the mother. Along the way she meets the owner of a dry cleaning business, whose sweater was wrapped around the baby.

==Cast==
*Margherita Buy: Sister Caterina
*Silvio Orlando: Ernesto
*Carolina Freschi: Teresa
*Maria Cristina Minerva: Esmeralda
*Giuliana Lojodice: Caterinas Mother

==Soundtrack==
The soundtrack was composed by Ludovico Einaudi.

===Track listing=== 
{{Track listing
| extra_column = Performed by
| title1  = Fuori Dal Mondo
| extra1  = Ludovico Einaudi
| length1 = 
| title2  = Viaggio 2
| extra2  = Ludovico Einaudi
| length2 = 
| title3  = Passaggio
| extra3  = Ludovico Einaudi
| length3 = 
| title4  = Interludio 1
| extra4  = Ludovico Einaudi
| length4 = 
| title5  = Promessa
| extra5  = Ludovico Einaudi
| length5 = 
| title6  = Rockabilly Roadhouse
| extra6  = Billy Roues
| length6 = 
| title7  = Interludio 3" 
| extra7  = Ludovico Einaudi
| length7 = 
| title8  = Fuori Dalla Notte
| extra8  = Ludovico Einaudi
| length8 = 
| title9  = Cadenza
| extra9  = Ludovico Einaudi
| length9 = 
| title10  = Alta Pressione
| extra10 = Ludovico Einaudi
| length10 = 
}}

==External links==
* 

 
 
 
 
 
 
 

 