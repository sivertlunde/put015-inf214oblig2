Il Posto
{{Infobox film
| name           = Il Posto
| image          = Il Posto DVD.jpg
| image_size     = 
| caption        = DVD Cover
| director       = Ermanno Olmi
| producer       = Alberto Soffientini
| writer         = Ettore Lombardo Ermanno Olmi
| narrator       = 
| starring       = Loredana Detto Tullio Kezich Sandro Panseri Mara Revel
| music          = Pier Emilio Bassi
| cinematography = Lamberto Caimi
| editing        = Carla Colombo
| distributor    = Titanus
| released       = Italy: 1961 United States: October 22, 1963
| runtime        = 93 minutes
| country        = Italy Italian
| budget         = 
}} Italian film directed by Ermanno Olmi.   An extension of Italian Neorealism, it explores many of the dehumanizing practices of the corporation from the viewpoint of an Italian adolescent.    The film, shot in Milan, is in part a satire on Italys "economic miracle."

==Plot==
The film tells the story of Domenico, a young man who forgoes the latter part of his education when his family is in need of money.  Applying for a job at a big city corporation, he goes through a bizarre series of exams, physical tests and interviews.  During a brief respite from the tests, he meets Antonietta, a young girl who has similarly forgone her schooling when in need of money to support herself and her mother.  Through the course of this meeting, they have coffee at a local cafe, discussing the issues of their lives and their ambitions.  Becoming attracted to her, they are quickly separated when they land jobs in different departments.  

Meeting with a superior, he is informed that no clerical positions are available, subsequently taking a job as a messenger while awaiting a better position, gradually stripping him of his individuality.  While on an errand for his job, he meets Antonietta again, now working as a typist.  She invites him to a party, which he attends later in the evening.  Arriving at the party alone, he befriends an older couple despite being depressed at the prospect of Antonietta not showing up.  When an older woman asks him to dance, he reluctantly agrees after a few drinks and finds himself having fun, forgetting the sorrows of his job and Antonietta.  Returning to work the following day, he is offered a recently vacated desk of an employee since departed (an aspiring writer, presumed to have killed himself).  Before obtaining the seat, he is moved to the back in a dimly lit corner when the other employees complain of his relative youth in acquiring this prestigious seat.  As the film ends, Domenico has obtained this job for life, committing himself to the desperation of a banal career.

== Cast ==
* Loredana Detto as Antonietta Masetti
* Tullio Kezich as Psychologist
* Sandro Panseri as Domenico Cantoni
* Mara Revel as Old Woman

==Awards==
Wins British Film Institute Awards: Sutherland Trophy, Ermanno Olmi; 1961.
* Venice Film Festival: Italian Film Critics Award, Ermanno Olmi; 1961.
* David di Donatello Awards: David, Best Director, Ermanno Olmi; 1962.

== References ==
 

== External links ==
*  
*  
*   essay at the Criterion Collection by Kent Jones

 

 
 
 
 
 
 
 
 
 
 