Khullam Khulla Pyaar Karen
{{Infobox film
| name        = Khullam Khulla Pyaar Karen
| image       = KKPK_DVD_cover.jpg
| caption     = Khullam Khulla Pyaar Karen DVD cover
| director    = Harmesh Malhotra Anwar Khan Govinda Preity Zinta Prem Chopra Kader Khan Sadashiv Amrapurkar Mohnish Behl Johnny Lever Himani Shivpuri
| producer    = Tutu Sharma
| distributor = Padmini Telemedia Tutu Films
| released    =  
| runtime     = 155 mins
| language    = Hindi
| music       = Anand-Milind
| budget      =
}}
 Govinda and Preity Zinta. Due to complications the film kept on getting delayed therefore it was not released until 2005. 

== Synopsis ==

Damani (Prem Chopra) and Goverdhan (Kader Khan) are bitter enemies but when they get word from their boss Trikal Annaa (Sadashiv Amrapurkar) to stop the rivalry, they are forced to shake hands. They soon decide to get their children married to each other. Goverdhan tells his son Vicky (Mohnish Behl) to go to Surat to meet Damanis daughter Priti (Preity Zinta).

Whilst going to Surat, Vicky offers a lift to Raja (Govinda Ahuja|Govinda) but the car has an accident and falls into the river. Some people manage to rescue Raja but not Vicky. Using this as an advantage and knowing about Vickys purpose, he poses himself as Vicky and makes his way to Surat. He meets Priti and falls in love with her. At first when Priti meets Raja (under the guise of Vicky), she hates him but soon she falls in love with him. Goverdhan soon gets word that Vicky has met Priti and both have agreed to marry.

Then to his shock, the real Vicky comes home and announces that he never made it to Surat and he did not meet Priti. Goverdhan makes his way to Surat to find out who is there posing as his son...

== Cast == Govinda ... Raja
* Preity Zinta ... Priti
* Satish Kaushik ... Sindhi 
* Sadashiv Amrapurkar ... Supremo/Trikal Anna
* Prem Chopra .. Damani
* Kader Khan ... Goverdhan
* Mohnish Behl ... Vicky
* Himani Shivpuri ... Goverdhans wife
* Johnny Lever ... Gangster

==Soundtrack==

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! Title !! Singer(s)
|-
| Khullam Khulla Pyaar Karen 
| Jolly Mukherjee, Sunidhi Chauhan
|-
| Bachalo Bachalo
| Sonu Nigam, Alka Yagnik
|-
| Tere Ishq Mein Pad Gaye Re
| Sonu Nigam, Jaspinder Narula
|-
| Challa Challa
| Sudesh Bhosle
|-
| Bagalwalee Aankh Mare
| Udit Narayan, Jaspinder Narula
|-
| Mare Najar Se Kataree
| Sonu Nigam, Alka Yagnik
|-
| Yeh Ladakee Nahee, Yeh Banaras Kaa Pan Hai
| Sonu Nigam, Bela Sulakhe
|}

== Trivia ==
* The film was Harmesh Malhotras last film before his death.
* Though the movie starred Govinda and Preity Zinta, the film was a box office failure.

== Notes ==
 

== External links ==
*  

 
 
 