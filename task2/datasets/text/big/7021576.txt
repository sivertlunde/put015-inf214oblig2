8 Guys
{{Infobox film
| name           = 8 Guys
| image          = 
| alt            =  
| caption        = 
| director       = Dane Cook
| producer       = Barry Katz Brian Volk-Weiss
| writer         = Dane Cook
| starring       = Alonzo Bodden Jay Davis
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 20 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Robert Kelly, Kathleen Luong, and Wayne Previdi.

==Plot==
Eight men share a tiny studio apartment in an anonymous American city. Dane angrily reminds his roommates that the apartment was meant for only one person, and that their landlord would evict them if he knew how many people were actually living there. None of the roommates will leave voluntarily, so Dane proposes a game to determine who will leave the apartment. The rules of the game are as follows: The first seven roommates to receive phone calls have to move out of the apartment. All incoming calls to the apartment will be screened through the answering machine. The speaker must pronounce a roommates name clearly in order for it to count. In addition to moving out, the eliminated roommate must leave his most prized possession behind. The last remaining roommate wins sole occupancy of the apartment.

==References==
 

== External links ==
*  

 
 
 
 
 
 