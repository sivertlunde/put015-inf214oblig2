The Boy and the Fog
{{Infobox film
| name           = The Boy and the Fog
| image          = 
| image_size     = 
| caption        = 
| director       = Roberto Gavaldón
| producer       = Jesús Grovas
| writer         = Edmundo Báez Roberto Gavaldón Rodolfo Usigli
| starring       = Dolores del Río
| music          = 
| cinematography = Gabriel Figueroa
| editing        = Gloria Schoemann
| distributor    = 
| released       = 24 December, 1953
| runtime        = 111 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}

The Boy and the Fog ( ) is a 1953 Mexican drama film directed by Roberto Gavaldón. It was entered into the 1954 Cannes Film Festival.     

==Cast==
* Dolores del Río
* Alejandro Ciangherotti -  (as Alejandro Ciangherotti hijo)
* Miguel Ángel Ferriz
* Lupe Inclán
* Pedro López Lagar
* Tana Lynn Eduardo Noriega
* Carlos Riquelme

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 