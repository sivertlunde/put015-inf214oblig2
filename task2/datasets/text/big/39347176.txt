Suriya Paarvai
{{Infobox film
| name           = Suriya Paarvai
| image          = 
| image_size     =
| caption        =  Jagan
| producer       = Sreenivas Prasad
| writer         = 
| story          = 
| screenplay     = Jagan
| starring       =  
| music          = S. A. Rajkumar
| cinematography = K. Prasad
| editing        = Sai Prasad Babu Raj
| distributor    =
| studio         = Moturi Creations
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}

Suriya Paarvai is a  .   

==Plot==

Vijay (Arjun Sarja|Arjun) is a hitman who works for Sundaramoorthy (Radha Ravi). He kills anybody for a price, other than women and children and he lives alone in an apartment. Pooja (Pooja), a young girl, studies in a boarding school and comes to her fathers house for the holiday. Between her abusive father, selfish stepmother and an arrogant step-aunt, Pooja feels rather badly but she treasures her lovely little brother Dinesh (Mahanadi Dinesh). Pooja tries to befriend with Vijay but Vijay is not interested. Her father is a drug smuggler. One day, a corrupt police officer Jayanth (Raghuvaran) kills Poojas family including her brother. Vijay decides to accommodate Pooja and Pooja compels him to teach her his skills as a hitman. What transpires later forms the crux of the story.

==Cast==
 Arjun as Vijay
*Meena as Meena
*Raghuvaran as Jayanth Vijayakumar as Narayanan
*Radha Ravi as Sundaramoorthy
*Goundamani as Raj Bharath / Nattamai Senthil as Pichai Perumal / Sundaram
*Chinni Jayanth
*Manjula Vijayakumar as Lakshmi
*Mahanadi Dinesh as Dinesh
*Sindhu as Sindhu
*Alphonsa as Sheela
*S. N. Lakshmi as Lakshmis mother
*Rani
*Kavitha Sree Master Mahendran as Vijay (child)
*Jaya Mani
*K. K. Soundar
*Vimalraj as George
*Aadukalam Naren as Kannan, Jayanths henchman
*Kalaignanam (guest appearance)

==Soundtrack==

{{Infobox Album |  
| Name        = Suriya Paarvai
| Type        = soundtrack
| Artist      = S. A. Rajkumar
| Cover       = 
| Released    = 1999
| Recorded    = 1998 Feature film soundtrack |
| Length      = 30:04
| Label       = 
| Producer    = S. A. Rajkumar
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer S. A. Rajkumar. The soundtrack, released in 1999, features 7 tracks with lyrics written by Pazhani Bharathi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|-  1 || Boom Blast It  || Abinaya || 4:24
|- 2 || Master Mahendran || 4:02
|- 3 || Hey Man || Sunidhi Chauhan || 4:57
|- 4 || Kadhavai Thirakkum  (male)  || P. Unni Krishnan || 3:52
|- 5 || Kadhavai Thirakkum  (female)  ||  K. S. Chithra || 3:52
|- 6 || Panirendu Vayasula || K. S. Chithra || 4:45
|- 7 || Thotathu Pookkal || S. P. Balasubrahmanyam, Abinaya || 4:12
|}

==References==
 

 
 
 
 
 
 