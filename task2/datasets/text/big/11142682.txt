Karishma (1984 film)
{{Infobox film
| name           = Karishmaa
| image          = Karishma84.jpg
| image_size     =
| caption        = LP Vinyl Records Cover
| director       = I. V. Sasi
| producer       = Barkha Roy (producer), I.A. Desai (executive producer)
| writer         = Manivannan Sachin Bhowmik (Dialogues)
| narrator       =
| starring       = Kamal Haasan Reena Roy Tina Munim Danny Denzongpa
| music          = Rahul Dev Burman
| cinematography = Jayanan Vincent
| editing        = K. Narayanan
| distributor    =
| released       =   
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
 1984 Hindi language film starring Kamal Haasan, Reena Roy, Tina Munim, Danny Denzongpa with Jagdeep, Sarika, Raza Murad, Viju Khote, Mac Mohan, Swaroop Sampat, Satyendra Kapoor, Mahesh Anand, and Baby Pinky.
 Tamil film Tik Tik Tik (1982) that also starred Kamal Haasan in the same role. 

==Cast==

*Kamal Haasan
*Reena Roy
*Tina Munim
*Danny Denzongpa
*Jagdeep
*Sarika
*Raza Murad
*Viju Khote
*Mac Mohan
*Swaroop Sampat
*Satyendra Kapoor
*Mahesh Anand
*Baby Pinky.

==Note==
The word "karishma" translates as "miracle", in Hindi.

==References==
 

==External links==
*  

 
 
 
 
 

 