Aces: A Story of the First Air War
 

{{Infobox film
| name           =Aces: A Story of the First Air War 
| image          =Aces (film).jpg
| image size     =150px
| caption        =VHS cover
| director       =Raoul Fox
| producer       =George Pearson
| writer         =George Pearson
| narrator       =Bill McNeil George Pearson
| starring       =
| music          =Larry Crosley
| cinematography =Savas Kalogeras
| editing        =Raoul Fox
| studio         = National Film Board of Canada 
| distributor    = National Film Board of Canada
| released       =   
| runtime        = 92 minutes, 32 seconds
| country        = Canada
| language       = English
| budget         =
| gross          =
}}

Aces: A Story of the First Air War (aka Les As du ciel - Chronique de la première guerre des airs for a French-language version) is a 92-minute 1993 Canadian historical drama film directed by Raoul Fox and produced by the  , 2013. Retrieved: October 9, 2014. 

==Plot== Western Front as an officer in the infantry, he applies to become an aviator and is assigned to a balloon corps as an air observer.

Spotting for gunners is his first task, but the job is dangerous, as balloons draw the attention of anti-aircraft batteries (called "archie") and enemy aircraft. A parachute saves him when his balloon is shot down. His second transfer request to an aircraft squadron is quickly approved, mainly because there is a pressing need for replacements during the "Fokker Scourge" in 1915–1916, when Allied flyers were being shot down in large numbers by superior German Fokker aircraft.

Flying aerial reconnaissance missions in a Royal Aircraft Factory B.E.2|B.E.2C reconnaissance aircraft|reconnaissance/bomber aircraft is just as harrowing. Although the air observers are armed and able to fight back, the crews call themselves "Fokker fodder". With the development of more capable Allied fighters, air superiority is slowly won back. On the ground, armies are no longer able to move without being observed from the air, leading to a blood-stained stalemate.
 aces such as Baron Manfred von Richthofen, the "Red Baron".  After being wounded, a final application to become a pilot is approved. The air war is still considered the realm of "knights of the air" by not only Canadians but also Americans signing up, even before their country enters the war. At the completion of his training in 1918, despite his background as an air observer, grandfather is a neophyte assigned to a fighter squadron at the front. Flying the new S.E.5, he takes part in air combat against seasoned German aces.

With his prospects grim, he beats "the odds" when the Armistice ends the conflict. The Canadian contribution to the air war has been remarkable: 10,000 aviators, including Billy Bishop|"Billy" Bishop and Raymond Collishaw, among the top aces in the war.

His grandson resolves to keep the replica S.E.5 as a tribute to his grandfather and all the others who fought in the first air war.

==Production==
  
Aces: A Story of the First Air War is more of a docudrama than a traditional documentary, weaving a fictional story into a depiction of the air war on the Western Front. Canadian military aviation historians Philip Markham (technical coordinator and historical consultant) and Brereton Greenhous (historical consultant) provided historical context to the fictional account of a Canadian aviator. Group Captain (RCAF Ret’d) A.J. Bauer was the films overall technical consultant.

The film utilized rarely seen archival footage from the National Archives of Canada, the Imperial War Museum, the Audiovisual Archives (The Hague) and the Fokker Aircraft Corporation (Amsterdam). Some aerial scenes were from the film Wings (1927 film)|Wings (1927). Footage was also shot of modern First World War replicas flown by owner-pilot Cole Palen at the Old Rhinebeck Aerodrome, in Red Hook, New York. 

==Reception==
Aces: A Story of the First Air War was primarily made-for-television and after broadcast on a number of different Canadian networks in 1993 was released to the home market on June 4, 1997. The film was made available as a VHS video to schools, libraries, film libraries operated by university and provincial authorities and other interested parties. 

Reviews were generally favourable, with the Video Rating Guide for Libraries noting: "High school students to adults will find   offers an excellent use of documentary technique to capture the mood of World War I. The videos recollection of World War I brings a vivid contrast to current military air technology."  Alex Leites, in a review for the History Journal, called the film "... this hidden gem."  Library Journal stated "The films producers have done a superb job of applying modern standards to rare archival footage."     National Film Board of Canada, 2007. Retrieved: October 9, 2014. 

The New York Times reviewer said, "This video combines rare film footage with interviews to re-create aspects of the real-life battles confronted by  Americas   World War I flying aces." 

==Awards==
Aces: A Story of the First Air War received a number of awards: 
* Peoples Choice Award at the International Aviation Film/Video Festival, August 3–6, 1995, Red Deer, Alberta, Canada.  Spring Valley, California, United States. 
* Bronze Plaque Award (Category: Social Issues) at the International Film and Video Festival, October 25–28, 1994, Columbus, Ohio United States. 

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Dunnigan, James F. How to Make War: A Comprehensive Guide to Modern Warfare in the Twenty-first Century. New York: HarperCollins, 2003. ISBN 978-0-06009-012-8.
 

==External links==
*  

 
 
 
 
 
 
 
 