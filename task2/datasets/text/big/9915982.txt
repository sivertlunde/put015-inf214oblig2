La caída
{{Infobox film
| name           = La caída
| image          =
| image_size     =
| caption        =
| director       = Leopoldo Torre Nilsson
| producer       = Adolfo Cabrera Leopoldo Torre Nilsson
| writer         = Beatriz Guido Leopoldo Torre Nilsson
| starring       = Elsa Daniel
| music          =
| cinematography = Alberto Etchebehere
| editor         = Jorge Gárate
| distributor    =
| released       =  
| runtime        = 84 minutes
| country        = Argentina
| language       = Spanish
| budget         =
}}

La caída is a 1959 Argentine drama film directed by Leopoldo Torre Nilsson. It won the Silver Condor Award for Best Film was entered into the 9th Berlin International Film Festival.   

==Cast==
* Elsa Daniel - Albertina
* Duilio Marzio - José María
* Lautaro Murúa - Uncle Lucas
* Lydia Lamaison - Marta
* Hebe Marbec - Laura
* Oscar Orlegui - Diego
* Carlos López Monet - Gustavo (as Carlos Monet)
* Mariela Reyes
* Mónica Grey
* Nora Singerman Pinky - Delfina
* Óscar Robledo
* Emma Bernal - Tía
* Enrique Kossi
* Miguel Caiazzo
* Mónica Linares

==References==
 

== External links ==
*  
 
 
 
 
 
 
 
 
 