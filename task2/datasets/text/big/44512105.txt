Beg, Borrow or Steal
{{Infobox film
| name           = Beg, Borrow or Steal
| image          = Beg, Borrow or Steal poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Wilhelm Thiele
| producer       = Frederick Stephani
| screenplay     = Leonard Lee Harry Ruskin Marion Parsonnet 
| story          = William C. White John Beal Erik Rhodes
| music          = William Axt
| cinematography = William H. Daniels
| editing        = Conrad A. Nervig
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Beal, Erik Rhodes. The film was released on December 3, 1937, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Frank Morgan as Ingraham Steward
*Florence Rice as Joyce Steward John Beal as Count Bill Cherau
*Janet Beecher as Mrs. Agatha Steward
*Herman Bing as Von Giersdorff Erik Rhodes as Lefevre
*George Givot as Izmanov
*E. E. Clive as Lord Nigel Braemer
*Tom Rutherford as Horace Miller 
*Cora Witherspoon as Mrs. Elizabeth Miller Reginald Denny as Clifton Summitt
*Vladimir Sokoloff as Sascha
*Harlan Briggs as Mr. Virgil Miller

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 