Mavi Boncuk
{{Infobox film
| name           = The Blue Bead
| image          = MaviBoncuk.jpg
| image_size     =
| caption        = Film poster
| director       = Ertem Eğilmez
| producer       = Ertem Eğilmez
| writer         = Zeki Alasya Ertem Eğilmez Sadık Şendil
| narrator       =
| starring       = Emel Sayın Tarık Akan Zeki Alasya Metin Akpınar Münir Özkul Halit Akçatepe Kemal Sunal Adile Naşit
| music          = 
| cinematography = Hüseyin Özşahin
| editing        = Arzu Film
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
| preceded_by    =
| followed_by    =
}}

The Blue Bead ( ) is a 1974 Turkish comedy film, produced, co-written and directed by Ertem Eğilmez, featuring Turkish classical music singer Emel Sayın as a nightclub singer who is kidnapped by a gang of slackers after they are beaten and thrown out of the club where she works. The film, which went on nationwide general release across Turkey on  , is considered a comedy classic in its homeland.

== Cast ==
*Emel Sayın - Emel Sayın
*Tarık Akan - Yakışıklı
*Zeki Alasya - Sweet Kamil
*Metin Akpınar - Süleyman
*Halit Akçatepe - Mıstık
*Münir Özkul - Father Yaşar
*Kemal Sunal - Governor Cafer
*Adile Naşit - Mıstıks mother

== External links ==
*  

 
 
 
 
 


 
 