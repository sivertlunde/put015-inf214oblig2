Atomic Ed and the Black Hole
 
{{Infobox film
| name           = Atomic Ed and the Black Hole
| image          = Atomiced2.jpg
| image size     =
| caption        =
| director       = Ellen Spiro
| producer       = Ellen Spiro 
Karen Bernstein
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = Ellen Spiro
| editing        =Karen Skloss
| distributor    = HBO/Cinemax Documentary
| released       =  
| runtime        = 40 minutes
| country        =
| language       =
| budget         =
| gross          =
}} documentary released in 2001 by filmmaker, Ellen Spiro. The documentary was made for HBOs Cinemax Reel Life Series. Sheila Nevins served as Executive Producer and Lisa Heller served as Supervising Producer.  Karen Bernstein served as Producer.  Laurie Anderson provided her song, Big Science, for the soundtrack.

Ed Grothus (“Atomic Ed”) is a machinist-turned-atomic junk collector who more than 30 years ago quit his job of making atomic bombs and began collecting non-radioactive high-tech nuclear waste discarded from the Los Alamos National Laboratory.

Atomic Ed is the proprietor of the “The Black Hole”, a second-hand shop and, next door, curator of the unofficial museum of the nuclear age.  His collection reveals and preserves the history of government waste that was literally thrown in a trash heap.

==Awards and festival screenings==
*Best Documentary Short, South by Southwest Film Festival, 2001
*Audience Award and Judges Competition First Place Award, Alibi Short Film Fiesta, Albuquerque, 2001
*Melbourne International Film Festival, 2001
*Hot Springs International Film Festival, 2001
*Peace and Justice Filmmakers Award, 2001
*San Francisco Documentary Festival, 2001
*SITE Santa Fe, 2001.

==Articles==
*Halleck, Deedee.  .  2007-4-12.  Retrieved on 2007-6-20.
*MacDonald, Scott.  .  Public Culture Duke University Press. 14.3 (2002) 469-475.   Retrieved on 2007-6-25.

==See also==
*List of films about nuclear issues
*List of books about nuclear issues

==References==
 

==External links==
* 


 

 
 
 
 
 
 
 
 
 
 