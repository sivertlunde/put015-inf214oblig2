A Love in Germany
{{Infobox film
| name           = A Love in Germany
| image          = A Love in Germany FilmPoster.jpeg
| alt            =  
| caption        = US poster
| director       = Andrzej Wajda
| producer       = Artur Brauner ZDF
| writer         = Rolf Hochhuth Agnieszka Holland
| screenplay     = 
| story          = 
| based on       =  
| starring       = Hanna Schygulla Piotr Lysak Armin Mueller-Stahl Ralf Wolter
| music          = Michel Legrand
| cinematography = Igor Luther
| editing        = Halina Prugar-Ketling
| studio         = 
| distributor    = Scotia Triumph Films
| released       =  
| runtime        = 132 minutes
| country        = West Germany France
| language       = German Polish
| budget         = 
| gross          =
}}
Eine Liebe in Deutschland (A Love in Germany) is a 1983 feature film directed by Andrzej Wajda.  
 broadcaster ZDF.

==Synopsis== Polish POW sentenced to death. Paulina is imprisoned for two years. Decades later her son and her grandson visit the still existing little town in order to confront the man who warned Paulina repeatedly but in the end let her go down. The both of them are appalled when they realise he still lives in the very town where the citizens forced him to take action on Paulina and he doesnt hide.

==Background==
The film was made 12 years after Willy Brandt, incubent Chancellor of Germany (Federal Republic of Germany) had signed the Treaty of Warsaw (1970) and moreover had knelt down in Warsaw (a gesture known as Warschauer Kniefall).

==Reception==
  

== References ==
 

==External links==
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 