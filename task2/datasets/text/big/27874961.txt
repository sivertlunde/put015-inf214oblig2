La Rose de Fer
{{infobox film
| name = La Rose de Fer
| image = Rose-fer,_jean_rollin-1972.jpg
| imagesize = 249PX
| caption = Theatrical Poster
| director = Jean Rollin
| producer = Sam Selsky
| writer = Jean Rollin Tristan Corbière Maurice Lemaître
| starring = Françoise Pascal Hugues Quester Nathalie Perrey Mireille Dargent Michel Dalessalle
| music = Pierre Raph
| cinematography =
| editing = Michel Patient
| distributor = Les Films ABC
| released = 12 April 1973
| runtime = 86 mins
| country =  
| language = French
}}

La Rose de Fer (English title: The Iron Rose) is a 1972 film directed by Jean Rollin, it is his first film not to feature vampires, a theme for which he is best known.  It still features all the dream-like qualities associated with his films.

==Plot==
A woman and a man who met at a wedding reception decide to go on a date. They meet again at a railway station and go for a picnic and a bike ride. They come to the outside of a lonely cemetery and the boy invites her to go in together.

Once inside the huge cemetery, the woman becomes anxious. He calms her and persuades her to enter a crypt with him. A strange man watches the couple. The man and the woman make love in the crypt. A clown places some flowers on a nearby grave and leaves. An old woman closes the cemetery gates.

When the couple finally exit the crypt, night has fallen and they cannot find their way out. They begin to panic. They discover a small building; inside are several child-sized coffins with small skeletons inside. The woman becomes moody and exhibits bizarre behaviour, personality changes and hysteria. She locks her lover in the crypt and he suffocates. Dawn finds the woman dancing around the cemetery, and later entering the crypt herself. The old woman reopens the cemetery gates. Finding the crypt open, she seals the door and puts flowers on top of it.

==Releases==

===DVD===

La Rose de Fer was released in France as part of the Jean Rollin collection.

It was released in its original aspect ratio of   on 20 January 2005 in Europe by X-Rated Kult DVD, in the UK on 28 March 2005 by Redemption, and in the US on 25 September 2007 by Redemption.  

===Blu-ray===

La Rose de Fer was released in 2012 by Kino Lorber in a five Blu-ray collection, along with Fascination (1979 film)|Fascination, La Vampire Nue, Le Frisson des Vampires, and Lèvres de Sang.  

==References==
 

==External links==
*  

 

 
 
 
 
 