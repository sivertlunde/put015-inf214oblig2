Love Liza
{{Infobox film
| name           = Love Liza
| image          = Love liza.jpg
| caption        = Movie poster
| director       = Todd Louiso
| producer       = Ruth Charny Chris Hanley Corky OHara Jeffrey Roda Fernando Sulichin
| screenplay     = Gordy Hoffman
| starring       = Philip Seymour Hoffman Kathy Bates Jim ORourke
| cinematography = Lisa Rinzler
| editing        = Ann Stein
| distributor    = Columbia TriStar, Sony Pictures Classics
| released       =  
| runtime        = 90 minutes
| country        = France Germany United States
| language       = English
| budget         = 
}}
Love Liza is a 2002 tragicomedy film directed by Todd Louiso and starring Philip Seymour Hoffman, Kathy Bates, Jack Kehler, Wayne Duvall, Sarah Koskoff and Stephen Tobolowsky.

==Plot summary==

Wilson Joels (Philip Seymour Hoffman) wife Liza (Annie Morgan) has, for an unexplained reason, committed suicide.  Wilson one night discovers a sealed letter from his wife, which he believes to be her suicide note.  In his grief-stricken state and with the added stress of finding the letter, which he cannot bring himself to open and read, he forms an addiction to inhaling gasoline fumes (“huffing”).

In order to hide his addiction from a work colleague, he informs her that the petrol smell in his house is from his hobby of flying remote controlled planes. To try to engage an ever-distant Wilson, she asks her brother-in-law, Denny (Jack Kehler), a radio control (RC) hobbyist, to visit Wilson.  With the knowledge of an impending visit from Denny, Wilson heads down to the local RC hobbyist shop to buy himself a plane. He also stocks up on some RC fuel to fill his plane and feed his addiction. Out of this lie, a very unusual friendship forms between Wilson and an energetic Denny.

His mother-in-law (Kathy Bates) tries her best to help Wilson and deal with her own loss at the same time. She becomes increasingly anxious to know the contents of the letter.

==Cast==
In Order Of Appearance
{| class="wikitable sortable"
! Actor !! Character
|-
| Philip Seymour Hoffman || Wilson Joel
|- JD Walsh || Bern
|-
| Jimmy Raskin || Pad
|-
| Kathy Bates || Mary Ann Bankhead
|-
| Erika Alexander || Brenda
|-
| Sarah Koskoff || Maura
|-
| Mark Hannibal || Waiter with Drink
|-
| Jim Wise || Bland Man
|-
| Trace Turville || Bland Woman
|-
| Wayne Duvall || Gas Station
|-
| Stephen Tobolowsky || Tom Bailey
|-
| Kevin Breznahan || Jim
|-
| Jennifer Keddy || Lynne
|-
| David Lenthal || Hobbytown USA Clerk
|-
| Jack Kehler || Denny
|-
| Pauline Boyd || Zoo
|-
| Ernest Perry Jr || Trucker
|-
| Cullen Douglas || Cashier at Pancake House
|-
| Joanne Pankow || Grandma Clerk
|-
| Dan Klass (as Dan Klaas) || Officer Escort
|- Chris Ellis || Patriot Model Aeronautics Clerk
|-
| George Mills || Pickup Truck Driver
|-
| Julia Lashae || Breakfast Woman
|-
| Don Hood || Good Morning Man
|- John McConnell || High School Principal #1
|-
| Terry Loughlin || High School Principal #2
|-
| Maggie Valentine || Girl on Front Porch
|-
| Teegan Eley || Jenn
|-
| Morgan Jarrett || Stacy
|-
| Daniel Farber || Huffer Boy
|-
| Kelli Garner || Huffer Girl
|-
| Shannon Holt || Angela Ryan from Bailey Federation
|- Ann Morgan || Liza
|}

==Production==

Much of the film was shot in Mobile, Alabama.

==Reception== Vanity Fair noted the way the film "tackled depression and substance abuse in a dark-as-hell comedy that takes on an additional layer of sadness  ".    Rex Reed of the New York Observer praised Hoffmans performance, believing that it could have the same kind of impact on his career as Leaving Las Vegas had for Nicolas Cage. However, A. O. Scott of The New York Times  stated that Hoffmans "omnipresence is something of a mixed blessing", given that the protagonist is "more a state of feeling than a character".   

Love Liza was released in a small number of movie theaters and earned $210,000. 

==References==
 

==External links==
*  
*  
* 

 
 
 
 
 
 
 
 
 
 


 