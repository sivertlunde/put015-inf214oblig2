Manorama Six Feet Under
{{Infobox film
| name           = Manorama Six Feet Under 
| image          = Manorama six feet under poster.jpg
| caption        = Movie poster for Manorama Six Feet Under  Navdeep Singh 
| producer       = Ketan Maroo 
| writer         = Devika Bhagat Navdeep Singh
| dialogues      = Abhinav Singh Kashyap Manoj Taparia 
| starring       = Abhay Deol Raima Sen Gul Panag 
| music          = Jayesh Gandhi Raiomond Mirza 
| cinematography = Arvind Kannabiran 
| editing        = Jabeen Merchant 
| distributor    = Shemaroo Entertainment 
| released       = 21 September 2007 
| runtime        = 138 minutes
| country        = India 
| language       = Hindi
}}
 thriller film Navdeep Singh. The film features Abhay Deol, Raima Sen and  Gul Panag in the lead roles. The film released on 21 September 2007.

The movie is a neo-noir with an amateur detective in a small sleepy town from Rajasthan who finds himself caught in a web of lies, deceit and murder. This movie is based on Chinatown (1974 film)|Chinatown (1974) by Roman Polanski, with the basic premise being similar to the original, despite changes in the representation of characters and some key plot points. In a cinema-noir style, the makers of Manorama Six Feet Under acknowledge the inspiration from the original by playing the sequence where Jack Nicholsons character gets his nose slashed on the main characters (Satyaveer) television.

== Plot ==

The film opens with a narrative about a nondescript town called Lakhot in Rajasthan, India. The narrator is Satyaveer Singh Randhawa (Abhay Deol), a down-on-his-luck public works engineer. He compares Lakhot – dry, desolate and despondent – to the general downturn in his own life. As he returns to his irritable and nagging wife Nimmi and their young son, we learn that Satyaveer has just been implicated in a small bribery scandal at work. Nimmi (Gul Panag) broods over how she wishes she had married a richer fellow. Satyaveer, an aspiring writer whose only novel Manorama sank without a trace, laments about how he had once wished to be famous but is now resigned to a banal and unremarkable existence.

They have an unusual visitor that night. A well dressed, affluent woman presents herself as Mrs. P. P. Rathore, the wife of the Irrigation Minister (and former Maharaja, presumably prior to Independence) P. P. Rathore (Kulbhushan Kharbanda). She says she is a big fan of Satyaveers novel. Captivated by the ingenuity of the detective Raghu, the principal character of the novel, she hopes to secure Satyaveers assistance in applying the same ingenuity to procure photographic evidence of her husbands affair. She pays him an advance and leaves.
Satyaveer accepts the job in spite of Nimmis reproach. He stealthily stakes out Rathores manor. He spots another woman visiting Rathore. Rathore rebukes the woman and turns her away. Satyaveer snaps a few pictures of this exchange and hands over the roll to Mrs. Rathore. He also confides in his brother-in-law and best friend, the loutish but generally well-meaning local cop Brij Mohan (Vinay Pathak). Brij finds this all very fishy and advises Satyaveer to take Nimmi on a short vacation.
Strange events happen to Satyaveer. He finds out that the woman who hired him is not Mrs. Rathore; the real Mrs. Rathore is an invalid. Late one evening, as Satyaveer returns home after drinks with Brij, he spots the same woman running for her life from people who are out to kill her. She earnestly urges him to remember her real name, Manorama (Sarika), and that she is 32 years old. The next days papers report that Manorama committed suicide in connection with her protests against a canal being built through Lakhot. The canal project is sponsored by Rathore.

He decides to investigate further. He finds out that Manorama was connected with a local childrens home and lived with a roommate, Sheetal (Raima Sen). The roommate is confused and scared. She refuses to talk with Satyaveer. Satyaveer is later set upon by thugs; they turn out to be the same men who chased Manorama on the night of her death; and they want to find out what she told him that night. Satyaveer later uncovers that Manoramas death was an accident; she was hit by a truck as she fled her pursuers.

Sheetal calls him. She is still scared but somehow warms to him and asks if she can stay with him for a few days. Seeing as Nimmi has returned to Rohtak (her parents house) for Diwali, Satyaveer agrees. Sheetal moves in for a little while. Around this time, Satyaveer attends a rally function with Rathore in attendance. He follows Rathore and discovers that Rathore receives regular medication from a doctor for an unknown ailment. He also spots the woman who visited him on the night Satyaveer took the photos.

He follows the woman and makes contact with her. (She lives with the doctor.) She turns out to be Sameera Rathore, the illegitimate daughter of Rathore. She was trying to get Rathore to accept her as a daughter. Satyaveer begins piecing the parts together. However, when Satyaveer visits the doctor, he finds that the doctor and Sameera have both been brutally murdered. He runs to the childrens home where he discovers that Sheetal has been dead for quite some time!

Arriving home, he finds that the girl masquerading as Sheetal is neither surprised nor scared. She directs him to meet Rathore. Rathore demands the photos. Satyaveer turns over the photos he had taken. Not surprisingly, Rathore isnt the least bit interested in photos of him having an argument with his biological daughter. It turns out that Nitu (the girl pretending to be Sheetal) is a concubine for Rathore. Rathore now openly threatens him and asks that he hand over those photos. Satyaveer returns home. He scans one of the photos he had taken and suddenly finds a clue in it. This clue leads him to what Manorama had said to him on the night she died. He follows these clues to a small hotel in town, where he discovers a shocking set of photos cached in one of the rooms.

Satyaveer returns to confront Rathore. He presents the shocking evidence he has found. Rathore was a pedophile. The childrens home was his steady supply of (orphaned) children. The real Sheetal and Manorama, who worked at the home, had realized his wicked activities. They were about to blow his cover so he had them silenced. Rathore calmly informs Satyaveer that Satyaveer, smart though he may be, is still a small-town man that may easily be dispensed with. Satyaveer reveals another fact. The doctor and Manorama were siblings. Also, though the doctor knew that Rathore had lung cancer, he kept concealing the fact from him and kept giving him placebos merely to suppress the symptoms of the cancer. The doctor and Manorama merely wanted to keep him alive long enough for him to accept Sameera as his daughter and heir. Then, on Rathores eventual death, the estate would come to Sameera, and thus also the doctor and Manorama. It is now too late. The cancer has reached an advanced stage and Rathore has precious little time left.

The film ends as Satyaveer quietly walks out. He remarks that the only thing that is certain in an unknown world is a known God.

== Cast ==

{| class="wikitable"
|-
! Actor
! Role
|-
| Abhay Deol || Satyaveer
|-
| Raima Sen || Sheetal
|-
| Gul Panag || Nimmi
|-
| Kulbhushan Kharbanda || Minister P.P Rathore
|-
| Sarika || Manorama
|-
| Vinay Pathak || Brijmohan
|-
| Nawazuddin Siddiqui || Local Goon
|-
| Brijendra Kale || Bihari Lal
|}

== Production == Dor (2006). The selection of Raima Sen was because of her versatility and suitability for most roles. Navdeep not only wanted a good script, but also great actors to work with. The character of Manorama went to Sarika because she was convinced of the role.

Since most of the movie was shot in Rajasthan during the winter season, the crew encountered trouble with the prevailing extreme cold conditions. The ensuing fog and rain also gave them trouble during the shoot.

== Box office ==
The film didnt work well commercially but critics appreciated the film and especially excellent written dialogues. Film was given the verdict of below average.

== Score ==
=== Track listing === Richa Sharma
# "Dhundla Jo Sama Bandh" (5:04) - Kailash Kher
# "Tere Sawalon Ke" (5:43) - Roop Kumar Rathod, Mahalakshmi Iyer
# "Woh Bheege Pal" (6:49) - Jayesh Gandhi
# "Woh Bheege Pal" (6:48) - Zubeen Garg
# "Woh Bheege Pal" (Remixed by Akbar Sami) (6:18) - Jayesh Gandhi

==External links==

*  

 
 
 