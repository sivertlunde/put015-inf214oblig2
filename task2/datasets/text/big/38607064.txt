Girlfriend in a Coma (film)
 
 
{{Infobox film
| name           = Girlfriend in a Coma
| image          = Giac poster.JPG
| caption        = Girlfriend in a Coma theatrical poster, Springshot Productions
| director       = Annalisa Piras
| producer       = Annalisa Piras Springshot Productions
| writer         = Annalisa Piras, Bill Emmott
| screenplay     = 
| story          = 
| based on       = 
| narrator       = Bill Emmott
| starring       = 
| music          = 
| cinematography = Patrizio Sacco
| editing        = Francesco Caradonna, Matteo Bini
| studio         = 
| distributor    = Mercury Media
| released       =  
| runtime        = 98 minutes
| country        = United Kingdom
| language       = English/ Italian
| budget         = 
| gross          = 
}}

Girlfriend in a Coma is a documentary about Italian and western decline directed, produced and co-written by Annalisa Piras, journalist and film-maker, co-written and narrated by Bill Emmott, former editor-in-chief of The Economist. It has been lauded as being ground-breaking in its creative combination of animation, interviews and hard facts, and has caused fierce controversy in Italy.

==Background==
The "Girlfriend in a Coma" title is derived from a British musical hit by   in 2012. 

Excerpts from Dante Alighieri’s Divine Comedy, read by Benedict Cumberbatch, are used to illustrate the vices and virtues of Italy, connecting today’s malaise to that of seven centuries earlier and placing it in the context of Italian history and culture. Animation by the London-based artist Phoebe Boswell provides a fil rouge through the film that is light in appearance but dark in nature.

==Plot==

The documentary, using the eyes of an English outsider directed by a passionate, but exiled, Italian insider, exposes the dire situation of Italian politics and the process of economic and social decline the country has suffered during the last two decades, treating the decline as a warning of what might happen elsewhere in the West. The decline has occurred amid a collapse of moral values and the victory of "Mala Italia" over "Buona Italia". Critical of  Silvio Berlusconi, the former Prime Minister, for some of the actions his government undertook or failed to undertake, and for his inappropriate use of his media companies to influence the Italian electorate, the documentary also does not spare criticism of the Left.

Overall, the film identifies "ignavia", the sin of lack of moral courage denounced by Dante Alighieri in his Divine Comedy (1321), as one of the crucial issues behind Italians’ failure to act when faced with their country’s constant decline over the past 20 years.

===Features===
The documentary features interviews with  , activist and filmmaker, Toni Servillo, one of today’s top Italian actors,  John Elkann, Chairman of FIAT, Sergio Marchionne, CEO of FIAT, Carlo Petrini, founder of Slow Food, Roberto Saviano, the author of Gomorrah (book)|Gomorrah, Giovanni Ferrero, CEO of Ferrero, Emma Bonino, former European Commissioner and Italian foreign minister in 2013, Susanna Camusso, trade union leader, Nicola Gratteri, anti-mafia magistrate, but also many unsung heroes from civil society such as Don Giacomo Panizza, a priest who founded the "Progetto Sud" against organised crime in Calabria.

==Production==
It was produced in 2012 by Springshot, an independent production company, in HD in Italy and London during 2011 and 2012, and the duration is 98 minutes.

==Release==
The movie’s premiere was held at the Institute of Contemporary Arts of London, on 26 November 2012.  The attendees included distinguished members of the world of arts, politics, diplomacy and business including John Elkann, Chairman of FIAT, and Vittorio Colao, Chief Executive of Vodafone. It has since been broadcast on BBC Four (300,000 viewers), Sky Italia and La7 in Italy (combined 750,000 viewers), as well as on many other European channels. Since then it has become a viral phenomenon and has been screened along with debates about the state of Italy and the West in public venues in Italy, across Europe and in North America. It is independently distributed on DVD and online.

===Reaction===
Girlfriend in a Coma has been described by the Italian press as a film about how Italy "has been left dying by the roadside" (La Stampa), as "marvellously indigestible" (Il Sole 24 Ore) and that "it freezes the blood in your veins" (LEspresso).

The international press hailed the documentary as a "lettre d’amour désespérée à l’Italie"  (Le Monde), and was described as "an intelligent, imaginatively presented documentary" by Philip French, film critic for The Observer. Fiammetta Rocco, Arts Editor for The Economist, wrote: "Girlfriend in a Coma is a terrific film. Its funny, sad, comic, deadly serious - and shows just how you can put across complex political and economic ideas when you decide to be creative. Like The End of the Line and One Day In Septembe, I think Girlfriend in a Coma really sets a whole new standard for documentary-making."

===Controversy===

Originally scheduled for 13 February 2013 at the MAXXI National Museum of the 21st Century Arts of Rome, Giovanna Melandri, the president of the foundation running the museum, banned the planned Italian premiere from being held there during the electoral campaign for polls on February 25th.
In a widely followed tweet and then an interview with The Guardian, Bill Emmott accused the Italian government of censorship.  Stefano Corradino, director of the website "Articolo21" (a reference to the Italian Constitution article on freedom of thought), launched a petition  asking Giovanna Melandri to reconsider her decision.

More than 30,000 people signed the petition. But the MAXXI decision was not reversed and the film’s premiere in Italy instead took place at the Teatro Eliseo in Rome on 13 February 2013, hosted at short notice by L’Espresso magazine.

The documentary has now become the centre of a social media campaign, connecting together fans, followers and anyone who wants to #wakeitalyup, as well as associations in Italy and around the world that are dedicated to bringing about change and modernisation in Italy.

==See also==
* Economy of Italy
* Politics of Italy
* Silvio Berlusconi
* Policies of Silvio Berlusconi
* Trials and allegations involving Silvio Berlusconi
* Rubygate

==References==
 

==External links==
*  
*  
*  
* 

 
 
 