Les deux orphelines vampires
{{infobox film
| name           = Les deux orphelines vampires 
| image          = Les deux orphelines vampires.jpg  
| imagesize      = 300px
| caption        = Original poster 
| director       = Jean Rollin 
| producer       = 
| writer         = Jean Rollin
| starring       = Alexandra Pic   Isabelle Teboul   Bernard Charnacé   Nathalie Perrey 
| music          = Philippe DAram 
| cinematography = Norbert Marfaing-Sintes
| editing        = Nathalie Perrey 
| distributor    = Les Films ABC   Avia Films  
| released       = 9 July 1997
| runtime        = 103 minutes 
| country        = France  French 
| budget         = ₣3,000,000 
}}
Les deux orphelines vampires (The Two Orphan Vampires) is a 1997 French horror film directed by Jean Rollin.  The film is an adaptation of Rollins novel of the same name.  While Rollin is primarily known for his vampire-themed works,  Les deux orphelines vampires is Rollins first vampire film since his 1979 Fascination (1979 film)|Fascination.

==Plot==
Louise and Henriette are a pair of orphaned sisters, innocent & sightless by day but after sunset, they are deadly Vampires, with an acute vision. Two beautiful girls embarks on a darkly romantic journey through the cities of Paris and New York to seduce victims to quench their thirst for fresh blood and satisfy their perverse carnal desires.

Their only refuge are the cemeteries where they find solitude and piece together the fragmented memories of their past lives.

==Cast==
* Alexandra Pic ... Louise
* Isabelle Teboul ... Henriette
* Bernard Charnacé ... Dr. Dennary
* Nathalie Perrey ... Nun
* Anne Duguël
* Nathalie Karsenty
* Anissa Berkani-Rohmer
* Raymond Audemard
* Tina Aumont ... Ghoul
* Catherine Day
* Camille Delamarre
* Véronique Djaouti ... Venus
* Michel Franck
* Frederique Haymann
* Paulette Jauffre

==Production==
The film was shot on location in Paris and New York.   The same location was used in Rollins 1989 film Perdues dans New York (Lost in New York). 

==Reception==
The film has received mixed reviews.  Online reviewer DVD Resurrections has said the film is "fairly good if one is in the right mood", and gave the film 6.5/10.   Online reviewer Digital Vampiric Discs has said "youll probably find it boring if youre not used to Rollins style, and even if you are youll probably have to be in the mood for this one".  Götterdämmerung has said that it is "an interesting little film that is a bit plodding in places and that requires some patience to decide", and that "nevertheless a rewarding viewing experience". 

==References==
 

==External links==
*  

 

 
 
 
 
 