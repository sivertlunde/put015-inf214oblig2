Hard Boiled (1919 film)
{{Infobox film
| name           = Hard Boiled
| image          = 
| alt            = 
| caption        =
| director       = Victor Schertzinger
| producer       = Thomas H. Ince
| screenplay     = John Lynch R. Cecil Smith 
| starring       = Dorothy Dalton C.W. Mason William Courtright Gertrude Claire Walter Hiers Nona Thomas
| music          = 
| cinematography = John Stumar
| editor         = W. Duncan Mansfield 	
| studio         = Thomas H. Ince Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent film directed by Victor Schertzinger and written by John Lynch and R. Cecil Smith. The film stars Dorothy Dalton, C.W. Mason, William Courtright, Gertrude Claire, Walter Hiers and Nona Thomas. The film was released on February 2, 1919, by Paramount Pictures.  

==Plot==
 

==Cast==
*Dorothy Dalton as Corinne Melrose
*C.W. Mason as Billy Penrose
*William Courtright as Deacon Simpson 
*Gertrude Claire as Aunt Tiny Colvin
*Walter Hiers as Hiram Short
*Nona Thomas as Daisy May

== References ==
 

== External links ==
*  
 
 
 
 
 
 
 
 

 