Rain Clouds over Wushan
{{Infobox film
| name           = Rain Clouds Over Wushan
| image          = Rainclouds Over Wushan.jpg
| caption        = 
| director       = Zhang Ming
| producer       = Han Sanping Huang Yunkai Tian Zhuangzhuang Zhu Wen
| starring       = Zhang Xianming Zhong Ping Wang Wenqiang
| music          = Liu Feng
| cinematography = Ding Jiancheng Zhou Ming Yao Xiaofeng
| editing        = Wang Qiang Zhou Ying
| distributor    = 
| country        = China
| released       =  
| runtime        = 96 minutes
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 巫山云雨
| fanti          = 巫山雲雨
| pinyin         = Wū shān yún yǔ}}
}} Zhu Wen. Wushan on the banks of the Yangtze River during the construction of the Three Gorges Dam. 

The satirical portrayal of rural life is considered part of the sixth generation movement that began in the early 1990s. Zhang, Yingjin & Xiao, Zhiwei (1998).  . Taylor & Francis, p. 276. ISBN 0-415-15168-6. Google Book Search. Retrieved 2008-09-26.  Unlike many other films of the movement, however, Rain Clouds over Wushan was produced with help from the state-run Beijing Film Studio. 

== Plot ==
Mai Qiang (Zhang Xianming) is a lonely thirty-year-old river signalman living in Wushan, on the banks of the Yangtze River. Lonely, he is pushed to meet women by his more socially adept friend (Wang Wenqiang. One day, after getting drunk, he meets a young widow, Chen Qing (Zhong Ping). Chen, works in a small inn destined to be flooded when the Three Gorges Dam is complete and dreams of a better life. Mai, thinking she is the woman he dreams of every night, forces her to have sex but is shamed by his actions when he realizes what he has done the next morning. 

Chen informs the police and Mai is arrested. Chen, however, decides to tell the police that she has consented to the sex, saving Mai and bringing disdain upon herself. A grateful Mai then decides to propose to Chen.

== Reception ==
Since its release in 1996, Rainclouds over Wushan has slowly gained a reputation as a key film in the sixth generation movement of Chinese cinema. It was, for example, part of a retrospective by the Harvard Film Archive as part of its retrospective on the so-called "Urban Generation" in 2001.  It was also screened at other retrospectives throughout the United States, including the UCLA Film Archives 2000 "New Chinese Cinema: Tales of Urban Delight, Alienation and the Margins" retrospective. 
 The Day a Pig Fell Into the Well.  Additionally the film won a FIPRESCI prize at the Torino Film Festival, as well as the Best Feature Film award. 

== References ==
 

== External links ==
* 
* 
*  at the Chinese Movie Database

 
 
 
 
 