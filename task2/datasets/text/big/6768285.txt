Majunu
{{Infobox Film
| name           = Majunu2015
| image          = "Majunu, 2001".jpg Ravichandran
| art director   = Yogamagi
| writer         = Ravichandran Prashanth Rinke Vivek Rati Agnihotri Murali Manohar Priyan
| music          = Harris Jayaraj
| editing        = B. Lenin V. T. Vijayan
| studio    = Cee I TV Ltd
| released       =  
| runtime        = 172 mins
| country        = India Tamil
}}
 romantic action Ravichandran and Prashanth and Vivek appearing in other pivotal roles. The films score and soundtrack were composed by Harris Jayaraj, whilst Priyan handled the cinematography. The film was distributed by London based Tamil language telechannel – Cee I TV. The film was released on 14 December 2001 and it was declared a hit at the box office.  

==Plot==
Vasanth (Prashanth (actor)|Prashanth) is a son of a wealthy MP Gajapathy (Raghuvaran) who hates love and against love marriage. Vasanth lives a fun-filled enjoyable life until he meets a girl (Rinke Khanna) while rushing to save a baby who is about to fall in a railway track while the girl also rushed for the same reason. He develops an attraction for that girl and again meets her in his college and knows her name to be Heena a Tamil speaking Bengali girl. Meanwhile, a trap is set for MP Gajapathy to kill him by explosion in a public gathering. Heena who is on an education trip to Chennai searches for some books in a book shop. Since the book is not available that day she has to come again to the shop and while leaving the shop some sound comes from her bag which she funnily mentions it to be a bomb which on explosion will destroy 20&nbsp;km. The shop-keeper laughs on her fun. But the sound is actually the sound of watch alarm which she kept it inside.

Heena comes again to the shop where the public meeting is conducted. As planned by the terrorists the bomb explodes but Gajapathy escapes with wounds and injuries. The shop-keeper immediately points Heena to be the terrorist keeping in mind about the bomb which she mentioned during her last visit. Also circumstantial evidences are against her. Hence police and Gajapathy men are in search of her but Heena escapes from them. Meanwhile Vasanth dashes one of the terrorist accidentally and finds bombs in his bags. He immediately chases him but the terrorist kills himself by self poisoning when caught by police.

Heena hides in a house which happened to be house of Vasanth. Vasanth finds her from the backyard and takes her to his room. Heena resists the charge against her and claims she is innocent. Vasanth assures that he believes her and it is his duty to send her back to Kolkata. Vasanth helps her by keeping her in his room without the knowledge of his parents. Both develop an attraction for each other but Vasanth is keen to send her home. Heena finds out that he is the son of the man whom she is charged for killing attempt. Vasanth takes her to railway station and sends her to her native.

Vasanth lies to his parents and leaves for Kolkata along with his friend to find his love with the hope of finding her knowing her name alone. He searches her in many colleges and finally finds her. She is happy to see again and takes him to his home and introduces to her mother (Rati Agnihotri) while her brother left before his arrival. Actually her brother (Sonu Sood) is the mastermind in the plot for killing Gajapathy which is not known by his family. He comes to know about the arrival of Vasanth and plans to kill him without knowing he actually saved his sister. Both Vasanth and Heena become intimate which is disliked by her mother. She urges to keep away from him which disappoints Heena. Vasanth expresses his love for her. Heena finds out the real identity of her brother and fears that he might kill Vasanth and so she pretends to reject him. Vasanth is hurt by her rejection but does not back off, hoping to win her love one day. Heena informs Gajapathy about all incidents and makes him to come to Kolkata to take away his son. Gajapathy insists Vasanth to come with him and he unwillingly accepts. Heenas brother comes to know about Vasanth and Gajapathy and chases to kill them. Heena struggles to save them but Vasanth does not co-operate with her as he wants her to accept him. Heena finally confesses her love and tells the reason for her behaviour is the brothers true identity and intentions which shatters Heenas mother. She unites both of them and asks to leave the place. Both get into the running train in the nick of time before Heenas brother catches them and he is arrested by police. Vasanth and Heena are united finally.

==Cast== Prashanth as Vasanth
* Rinke Khanna as Heena
* Raghuvaran as Gajapathy Vivek as Mano
* Sonu Sood
* Rati Agnihotri 
* Vaiyapuri
* S. N. Lakshmi
* Nethra Raghuraman
* Anupama Verma

==Production== Murali Manohar Prashanth to appear in a fourth successive production for his studio, following the successes of their previous collaborations Jeans (film)|Jeans, Kaadhal Kavithai (1998) and Jodi (film)|Jodi (1999).   Director Ravichandran (Tamil film director)|Ravichandran, who had delivered a commercially successful film with Prashanth through Kannedhirey Thondrinal (1998), was subsequently chosen to direct Manohars next production. Hindi actress Rinke Khanna was then signed on to make her Tamil film debut in October 2000 and joined the team soon after, after her mother Dimple Kapadia was impressed with the script.  Actress Rati Agnihotri also signed on to play Rinke Khannas mother in the film, making her first appearance in a Tamil film for twenty years. Models Nethra Raghuraman and Anupama Verma also made special appearances in the film.

The team shot scenes in North India, while Prashanth was injured during the making of the film, delaying it temporarily.  The art director Yogamahi designed a lavish set in a studio for a song picturisatio costing Rs.10 lakh, with a Rajasthani ambience created with 200 people working on it.  The film had completed filming scenes by July 2001 and dubbing works began, preparing for a September release, although it was later postponed. 

==Release==
The film opened in December 2001 to mixed reviews from critics, with Malathi Rangarajan of The Hindu noting "Majunu is a love story in a terrorist ambience. The ambience is undoubtedly different but surely the treatment could have been more gripping."  Another critic wrote "The storyline is just stupid and should have never been. But Ravichandran makes a better director than a story-writer, and the movie is at least sit-throughable".  A further reviewer noted "The listless romance and the amateurish attempts at portraying terrorism here just prove that the same issues that were merged so effortlessly and effectively in Manirathnams classic, Roja (1992), can also be combined to create a film that resides on the other end of the watchable scale." 

==Music== Lara Dutta being the events chief guest.  All lyrics were written by Vairamuthu and Thamarai, while the songs received positive reviews from film critics. 

{{Infobox album|  
 Name = Majunu |
 Type = soundtrack |
 Artist = Harris Jayaraj | Feature film soundtrack |
 Label = Sa Re Ga Ma |
 Released = 5 July 2001 |
 Last album = Rehna Hai Tere Dil Mein (2001) |
 This album = Majunu (2001) |
 Next album = 12B (2001) |
}}

{{tracklist
| headline = Track listing
| extra_column = Singer(s)
| title1 = Gulmohar Malare Anupama
| length1 = 05:26
| title2 = Hari Gori
| extra2 = Tippu (singer)|Tippu, Karthik (singer)|Karthik, Devan Ekambaram, Ganga, Febi Mani
| length2 = 03:00
| title3 = Mudhal Kanave
| extra3 = Harish Raghavendra, Bombay Jayashree
| length3 = 05:11
| title4 = Pada Pada Pattamboochi
| extra4 = Shankar Mahadevan, Kavita Krishnamurthy
| length4 = 06:26
| title5 = Mercury Mele
| extra5 = Devan Ekambaram, Unnikrishnan
| length5 = 05:29
| title6 = Pinju Thendrale
| extra6 = M. G. Sreekumar, Sandhya
| length6 = 05:19
| title7 = Gulmohar Malare (Short Version)
| extra7 = Hariharan, Timmy, Anupama
| length7 = 03:22
}}

==References==
 

 
 
 