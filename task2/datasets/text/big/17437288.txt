A Soap (film)
{{Infobox film
| name = En Soap
| image = EnsoapDVDcover.jpg
| caption = DVD cover
| director = Pernille Fischer Christensen
| producer = Lars Bredo Rahbek
| writer = Kim Fupz Aakeson Pernille Fischer Christensen
| starring = Trine Dyrholm David Dencik
| music =  Magnus Jarlbo Sebastian Oberg
| cinematography = Erik Molberg Hansen
| editing =  Asa Mossberg
| studio = {{Plainlist |
*Nimbus Film
*Zentropa
}}
| released = April 7, 2006
| runtime = 104 minutes
| country = Denmark
| language = Danish
| budget = 
| gross = 
}}
 2006 Denmark|Danish Dogme style. The movie, starring Trine Dyrholm and David Dencik, follows the turbulent relationship between an abrasive beauty clinic owner and a depressed pre-op transgender woman. Made on a budget of 1.5 million dollars, it was the first feature film directed by Christensen.
 Jury Grand Best First Feature Film Award at the 2006 Berlin Film Festival. A Soap  received the Bodil Award for the 2007 Danish Film of the Year.  Dyrholms leading role earned her a third Bodil Award for Best Actress. 

==Selected Cast==
*Trine Dyrholm as Charlotte
*David Dencik as Veronica
*Frank Thiel as Kristian
*Elsebeth Steentoft as Veronicas Mother
*Christian Tafdrup as Costumer
*Pauli Ryberg as Costumer
*Jakob Ulrik Lohmann as One-Night Stand
*Claes Bang as One-Night Stand
*Christian Mosbæk as Narrator (voice)

 
==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 