Peddannayya
{{Infobox film
| name           = Peddannayya
| image          =
| caption        =
| writer         = Paruchuri Brothers  
| screenplay     = Sarath
| producer       = Nandamuri Ramakrishna
| director       = Sarath Roja Indraja Indraja
| Koti
| cinematography = Nandamuri Mohana Krishna
| editing        = Kotagiri Venkateswara Rao Ramakrishna Horticultural Cine Studios
| released       =  
| runtime        = 146 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}  
 Ramakrishna Horticultural Roja and Indraja  in the lead roles and music composed by Saluri Koteswara Rao|Koti. 

==Cast==
 
* Nandamuri Balakrishna as Rama Krishna Prasad & Bhavani Prasad (Duelrole) Roja as Seeta Indraja as Sravani
* Kota Srinivasa Rao as Chenchuramiyah
* Charanraj as Bhaskar Rayudu Srihari as Bhaskar Rayudus Son
* Brahmanandam as Principal Sudhakar as Bhaskar Rayudus Brother-in-law
* Vijaya Rangaraju as Bombula Bal Reddy
* Chalapathi Rao as Broker
* Mannava Balayya|M.Balayya as Heros Father
* Achyuth as Sai Prasad
* Raj Kumar as Durga Prasad
* Raja Ravindra as Ravindra (Bhaskar Rayudus Brother)
* Rajiv Kanakala as College Student
* Prasad Babu as Police Inspector
* Ananth as College Student Annapoorna as Heros Mother
* Subhasri as Neelaveni
* Lathasri as Sai Prasads Wife
* Rajitha
* Krishnaveni
* Alapathi Lakshmi
 

==Soundtrack==
{{Infobox album
| Name        = Peddannayya
| Tagline     = 
| Type        = film Koti
| Cover       = 
| Released    = 1996
| Recorded    = 
| Genre       = Soundtrack
| Length      = 27:53
| Label       = Shiva Musicals Koti
| Reviews     =
| Last album  = Intlo Illalu Vantintlo Priyuralu   (1997)  
| This album  = Peddannayya   (1997)
| Next album  = Muddula Mogudu    (1997)
}}

Music composed by Saluri Koteswara Rao|Koti. All songs are blockbusters. Music released on Shiva Musicals Company.
{|class="wik{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 27:53
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = O Mustafa
| lyrics1 = Veturi Sundararama Murthy SP Balu, Chitra
| length1 = 5:03

| title2  = Nee Andamantha
| lyrics2 = Veturi Sundararama Murthy
| extra2  = SP Balu,Chitra
| length2 = 5:08

| title3  = Chakkilaala Chukka
| lyrics3 = Veturi Sundararama Murthy
| extra3  = SP Balu,Chitra
| length3 = 4:23

| title4  = Kutumbam Annagari
| lyrics4 = C. Narayana Reddy
| extra4  = SP Balu,Chitra
| length4 = 4:33

| title5  = Kalalo Kalyanamala
| lyrics5 = Veturi Sundararama Murthy
| extra5  = SP Balu,Chitra
| length5 = 4:32

| title6  = Chikkindi Chemanthi  
| lyrics6 = Bhuvanachandra 
| extra6  = SP Balu,Chitra
| length6 = 4:03
}}
|}

===Music===
The music of the film was composed by Saluri Koteswara Rao|Koti. 

{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
! No.!!Song!!Lyrics!!Singers
|- Veturi Sundararama Veturi ||S. Chitra
|- 2 || Veturi ||S. P. Balasubrahmanyam, Chitra
|- Veturi ||S. P. Balasubrahmanyam, Chitra
|-
| 4|| "Kutumbam Annagari " ||C. Narayanareddy||S. P. Balasubrahmanyam, Chitra
|- Veturi ||S. P. Balasubrahmanyam, Chitra
|- Veturi ||S. P. Balasubrahmanyam, Chitra
|}

== References ==
 

==External links==
*  

 
 
 

 