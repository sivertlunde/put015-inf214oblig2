The Island President
 
{{Infobox film
| name           = The Island President
| image          = The Island President (film).jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Jon Shenk
| producer       = Richard Berge Bonni Cohen
| writer         = 
| narrator       = 
| starring       = Mohamed Nasheed
| music          = Marco DAmbrosio 	 
| cinematography = Jon Shenk
| editing        = Pedro Kos
| studio         = Actual Films
| distributor    = Samuel Goldwyn Films
| released       =  
| runtime        = 101 minuntes
| country        = USA
| language       = English
| budget         = 
| gross          = 
}}
 rising sea levels resulting from climate change.  Produced by Actual Films, an Academy Award|Oscar- and Emmy-winning American documentary film company based in San Francisco, and directed by Jon Shenk,  the film premiered at the Telluride Film Festival.

The film was funded by groups including the Ford Foundation, the American Corporation for Public Broadcasting, the John D. and Catherine T. MacArthur Foundation, the Atlantic Philanthropies and the Sundance Institute. 

At the 2011 Toronto International Film Festival, The Island President won the Cadillac Peoples Choice Documentary Award.  

US rights to The Island President were acquired by Samuel Goldwyn Films.  The film opened in New York City on March 28, 2012, followed by releases in other US cities, like Los Angeles and San Francisco. 

==Reception==
The Island President received positive reviews from critics. On Rotten Tomatoes, the film has a fresh rating of 98%, based on 45 reviews, with the consensus saying, "An eye-opening and appealing documentary about an earnest politician up against the closed door drama of climate change."  The film has a score of 72 out of 100 at Metacritic, based on 14 reviews.  A.O. Scott of The New York Times described the film as "buoyant and spirited" despite a grim postscript, praising its "spectacular aerial and underwater footage of the Maldives’ beauty" and stating that "It is impossible, while watching it, to root against Mr. Nasheed or to believe that he will fail".  Noel Murray of The A.V. Club gave the film a "B-", praising "the energy and conclusiveness" of its opening sections, but writing that "the movie stalls a bit" during the sections of international diplomacy. 

==References==
 

==External links==
* 
* 
* 
* 
* 
*  at the Movie Review Query Engine

 
 
 
 
 
 
 
 