The Small One
 
{{Infobox film
| name           = The Small One
| image          = Smalloneposter1.jpg
| caption        = Original one-sheet poster for The Small One
| director       = Don Bluth Ron Miller (executive)
| writer         = Vance Gerry Pete Young
| based on       = The Small One by Charles Tazewell  Sean Marshall William Woodson Hal Smith Joe Higgins Gordon Jump Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 26 minutes
| language       = English Richard Rich
}} Richard Rich, Henry Selick, Gary Goldman and John Pomeroy.

The story tells of a young boy, outside Nazareth, who must part with his best friend, an old donkey named Small One. He brings it to Market (place)|market, but no one is in need of a "scrawny donkey", save for a tanning|tanner.

==Plot summary==
Outside of the city of Nazareth, a young boy and his father own four donkeys. Three of these donkeys are young and strong. The fourth donkey, Small One, is old and weak, but the boy loves him anyway. Everyday, the boy and the donkey play together before they go to work, helping the boys father to collect wood.

The boy and his father take the donkeys to work one morning, as they always do. Many times, the boy loads Small One with small sticks, since Small One cant carry heavy loads any more. Small One even has trouble carrying stacks of small sticks and the boy helps to carry them for him.

That evening, the boys father tells the boy that he has to sell Small One. Devastated, but understanding, the boy asks if he can be the one to sell his best friend. The father agrees and tells him that he has to sell him for one piece of silver. That night, the boy comforts Small One and promises to find him a gentle and loving master.

The next morning, the boy takes Small One to the market in Nazareth. Unfortunately, nobody wants an old weak donkey but the tanner, and he only wants to kill Small One to make leather out of his hide. After failing to find another buyer, the boy and his donkey return to the tanners shop. The boy weeps, and Small One, accepting his fate under the tanners knife, tenderly consoles the boy.
 kind man pregnant wife bright star  appears in the sky.

==Characters==
*The Small One: He is a gentle donkey past his prime. Long years of working hard have made him weak. Though he eats as much as the other donkeys, he cannot handle the same loads. His right ear never likes to stay straight up, giving him that "Disney cuteness" rating. He has strong feelings for the Boy and would give his life to help him, as he almost does at the tanners.
*Boy: He is about ten years old, roughly. Although he takes care of all the donkeys, Small One is his favorite. When his father tells him to sell Small One, the boy is devastated. Soon he accepts it. Though he loves the donkey, he accepts what his father tells him. He reassures Small One will have a gentle and loving master and he does.
*Father: The boys father is not seen much at all. He is the one who sends the boy to sell Small One. He loves his son and deep down, he loves Small One too, but he knows they cannot afford to keep a donkey that cannot carry a large load.
*Three Merchants: These three men, one tall and skinny, one medium and skinny, and the last short and round, walk around the marketplace taking money from people, "for the bank". They are the ones who point the boy to the auctioneer.
*Auctioneer: He auctions horses in the market. At first he is angered that the boy brings Small One onto the stage and slightly embarrasses him. However, when the boy tells him what Small One can do, the man changes his mind and is intrigued, especially when the boy tells him that Small One is "good enough to be in a Kings stable". With that, he decides to have some fun. He mocks and almost injures Small One with his weight. But when Small One sees his best friend pushed aside, he regains his strength and throws the man off of his back. The auctioneer chases them away.
*The tanner: He kills animals to make leather from their skins. barn mates. They seem jealous that the boy spends more time with Small One than them.
*Saint Joseph|Joseph: The man who buys The Small One at the end of the film. Though his name is never mentioned, it is hinted that he is in fact Joseph as he states that he needs a donkey to carry his wife to Bethlehem.

==Credits==
*Story Adaptation: Pete Young, Vance Gerry Sean Marshall, Hal Smith, Joe Higgins, Gordon Jump
*Storyboards: Peter Young
*Supervising Animators: John Pomeroy, Gary Goldman, Cliff Nordberg
*Animators: Lorna Pomeroy, Heidi Guedel, Linda Miller, Emily Jiuliano, Jerry Rees, Bill Hajee, Chuck Harvey, Ron Husband, Randy Cartwright, Ed Gombert, Dan Haskett, John Musker
*Effects Animators: Ted C. Kierscey, Dorse A. Lanpher
*Assistant Animation Supervisor: Walt Stanchfield
*Layout: Dan Hansen, Sylvia Roemer
*Backgrounds: Jim Coleman, Daniela Bielecka
*Assistant Animators: Dan Kuenster, Chuck Williams, Henry Selick, Leslie Gorin, Michael Cedeno, Ben Burgess
*Inbetween Artists: Sally Voorheis, Skip Jones, Dave Spafford, Will Finn, David Molina, Diann Landau, Vera Law
*Breakdown Artists: Kevin Wurzer, Kathy Zielinski, Mark Henn
*Animation Camera: Joe Jiuliano, Chuck Warren, Brian LeGrady, Ron Maine, Peter McEvoy
*Cel Painters: Daryl Carstensen, Olga Craig, Carmen Oliver, Janet Bruce, Sarah King, Gretchen Albrecht
*Film and Sound Editing: James Melton
*Music Editor: Evelyn Kennedy
*Production Manager: Don Duckwall Richard Rich
*Songs Writers: Richard Rich, Don Bluth
*Music Composed and Conducted by Robert F. Brunner Ron Miller
*Produced and Directed by Don Bluth

==DVD release==
On September 27, 2005, Disney released this short for the first time on Region 1 DVD on Volume 9 of the Walt Disneys Classic Cartoon Favorites line, titled Classic Holiday Stories. This DVD also featured Mickeys Christmas Carol (1983) and Plutos Christmas Tree (1952).  The DVD is edited in two places:
* The star at the end has been given more lines to look less like a cross.
* The song the three merchants sing has had a lyrics change.  The lyric "We never, never fail when we go to make a sale, we simply cheat a little if we must" was changed to "We never, never fail when we go to make a sale, we just work a little harder if we must." The reason for these edits is not known; but it may have to do with the merchants being "Jewish" stereotypes.

This short is also featured on Walt Disney Animated Classics: Vol. 7: Mickeys Christmas Carol.
It was also released on Region 2 on the DVD Countdown to Christmas.

==International television== ITV
*  Ireland - RTÉ Two

==External links==
*   at the Big Cartoon DataBase
*   at The Encyclopedia of Disney Animated Shorts
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 