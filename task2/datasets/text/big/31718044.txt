Doctor Ahrendt's Decision
{{Infobox film
| name           =Die Entscheidung des Dr. Ahrendt
| image          = 
| image size     =
| caption        =
| director       =Frank Vogel
| producer       =Werner Dau
| writer         = Hasso Grabner
| narrator       =
| starring       =Johannes Arpe
| music          = Gerhard Wohlgemuth	 	
| cinematography = Walter Fehdmer
| editing        = Friedel Welsandt	 	
| studio    = DEFA 
| distributor    = PROGRESS-Film Verleih
| released       = 9 June 1960 
| runtime        = 81 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} East German black-and-white film, directed by Frank Vogel. It was released in List of East German films|1960. 

==Plot==
Dr. Ahrendt developed a new model of an iron smelter. When his invention fails to produce the required results, he begins to doubt himself, and is even considered as a liar by some. However, the workers in the factory are determined to achieve the goals set forth, and together with the scientist they manage to prove that the machine can be used as planned. 

==Cast==
*Johannes Arpe as Dr. Heinrich Ahrendt
*Rudolf Ulrich as Martin Kröger
*Willi Schrade as Andreas Morgner
*Erika Radtke as Gisela Ahrendt
*Josef Stauder as Karl Szepinski
*Fritz Diez as Scholz
*Gisela May as Mrs. Kröger
*Fritz H. Kirchhoff as Dr. Maurer
*Paul Streckfuß	as Kurt
*Werner Lierck as Rudi
*Hans Klering as Franz
*Roman Silberstein as Ede
*Helga Göring as Irma
*Hans Hardt-Hardtloff as Erwin
*Hans Flössel as Musner Siegfried Weiß as Kripphahn

==Production==
The film was one of the so-called "mission films", commissioned in great number by the East German government during the late 1950s and the early 1960s, that were intended to promote distinct political aims.  Dr. Ahrendts Decision was meant to set an ideal model for behavior and attitude for the industrial workers. 

==Reception==
Heinz Kersten wrote that the film resembled Socialist Realist pictures, and was intended to promote the economical aspirations of East Germany, by motivating the populace to work harder.  

Marianne Lange noted that Dr. Ahrendts Decision portrayed the transformation of simple workers to ambitious, skilled labor.  The West German film service regarded it as "boring propaganda". 

==References==
 

==External links==
*   on the IMDb.
*  on PROGRESS website. 

 
 
 
 

 