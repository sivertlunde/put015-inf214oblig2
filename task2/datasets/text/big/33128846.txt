The Axe of Wandsbek (1951 film)
{{Infobox film
| name           =The Axe of Wandsbek
| image          =
| image size     =
| caption        =
| director       = Falk Harnack
| producer       =Kurt Hahne
| writer         = Hans-Robert Bortfeldt, Falk Harnack, Wolfgang Staudte
| narrator       =
| starring       =Erwin Geschonneck Käthe Braun
| music          = Ernst Roters
| cinematography = Robert Baberske
| editing        =Hilde Tegener
| studio    = DEFA
| distributor    = PROGRESS-Film Verleih
| released       = 11 May 1951
| runtime        =111 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

The Axe of Wandsbek ( .

==Plot==
1934, Hamburg. Adolf Hitler is about to visit the city. Hamburgs executioner falls ill, and is unable to deliver the sentence of four communists who are awaiting capital punishment in jail. Fearing that this would spoil Hitlers visit, SS leader Footh offers a local bankrupt butcher, Albert Teetjen, 2,000 Marks in order to carry out the verdict. The broke Teetjen agrees and follows suit. When his neighbors hear of the execution, they shun him. His wife cannot tolerate her husbands deed and puts an end to her life. Eventually, Teetjen also commits suicide.

==Cast==
* Erwin Geschonneck: Albert Teetjen
* Willy A. Kleinau: Hans Peter Footh
* Käthe Braun: Stine Teetjen
* Gefion Helmke: Dr. Käthe Neumeier
* Arthur Schröder: Dr. Koldewey
* Ursula Meißner: Annette Koldewey
* Helmuth Hinzelmann: Colonel Lintze
* Erika Dannhoff: Lene Prestow
* Fritz Wisten: Siegfried Mengers, convict
* Albert Garbe: Otto Merzenich, convict
* Hermann Stövesand: Friedrich Timme, convict
* Gert Karl Schaefer: Willi Schröter, convict

==Production== Arvid was 999th Penal Greek Resistance, decided to film Staudtes work in 1950.  Seán Allan, John Sandford. DEFA: East German cinema, 1946-1992, pp. 68-69. ISBN 978-1-57181-753-2 

==Reception==
The Axe of Wandsbek was viewed by 800,000 people in the first three weeks after its release,  and received positive reviews. 

The East German political establishment and the Soviet representatives in the country disapproved of the film, which they viewed as promoting sympathy to the perpetrators of Nazi atrocities. The SED politburo denounced it, proclaiming that "it did not present the proletariat resistance as heroes, but rather, their executioners."  The film was banned after less than a month, although Zweig himself, who wielded considerable influence as the President of the GDRs Academy of Arts, resisted the move.  Bertolt Brecht offered to write an alternate version, but was rejected.  The Axe of Wandsbek was DEFAs first film to be banned. Stephen Brockmann. A Critical History of German Film. Camden House (2010), p. 221. ISBN 978-1-57113-468-4  This happened soon after the government established the DEFA commission to regulate the studio and provide political control. Dagmar Schittly. Zwischen Regie und Regime. Die Filmpolitik der SED im Spiegel der DEFA-Produktionen, p. 45. ISBN 978-3-86153-262-0  Shortly afterwards, Harnack left for West Germany, abandoning his position as DEFAs artistic director. The studio came under the control of party functionary Sepp Schwab. 

In 1962, the film was allowed to be screened again, in honor of Zweigs 75th birthday. The authorized version was twenty minutes shorter than the original. 

==References==
 

==External links==
*  on the IMDb.

 
 
 
 
 
 
 