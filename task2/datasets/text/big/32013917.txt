Jeene Ki Arzoo
{{Infobox film
 | name = Jeene Ki Arzoo
 | image = JeenekiAarzoo.jpg
 | caption = Promotional Poster Rajasekhar
 | producer = AVM Productions
 | writer = Rama Narayanan|Ram-Rahim
 | dialogue = 
 | starring = Mithun Chakraborty Rati Agnihotri Rakesh Roshan Sujit Kumar Rajendranath
 | music = Bappi Lahiri
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = 4 December 1981
 | runtime = 130 min.
 | language = Hindi Rs 90 Lakhs
 | preceded_by = 
 | followed_by = 
 }}
 1981 Hindi Indian feature directed by Rajasekhar for Telugu film Punnami Naagu which was directed by Rajasekhar himself. 

==Plot==

Jeene Ki Arzoo is an action film starring Mithun and Rakesh Roshan.

==Synopsis==

Before Dying the village priest curses the snake charmer, Raka for killing him and making his son Ravi (Rakesh Roshan) an orphan. Poor Ravi is taken care of by the village landlord who adopts him and makes him the brother of his daughter Poonam. Years pass by and its time for Raka to pay for his sins. His only son Nagraj (Mithun Chakraborty) becomes the victim of the curse where he will be incarnated as a snake, killing anyone who makes contact with him. Things take a dramatic turn with the mysterious death of Poonam. Ravi investigates his sisters death to find out Nagraj is the culprit. Nagraj moonlights as a venomous snake that is out to kill women. Can the dangerous Nagraj be stopped from his killing spree? Will he be freed of the curse?

==Cast==

*Mithun Chakraborty
*Rati Agnihotri
*Rakesh Roshan
*Sujit Kumar
*Rajendranath
*Jagdeep
*Jayamalini

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aadhi Ye Raat Jale"
| Kishore Kumar
|-
| 2
| "Gup Chup Chori Chori"
| SP Balasubramanyam, Lata Mangeshkar
|-
| 3
| "Hath Na Lagana"
| Asha Bhosle
|-
| 4
| "Kachhi Kachhi Ambia"
| Asha Bhosle
|}

==References==
 
* http://www.ibosnetwork.com/asp/filmbodetails.asp?id=Jeene+Ki+Arzoo
* http://www.indimaza.com/watch/161406/jeene-ki-arzoo--full-length-hindi-movie--mithun-chakraborty-and-rakesh-roshan.html

==External links==
*  
 
 
 
 
 