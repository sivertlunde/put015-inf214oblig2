Jai Vikraanta
{{Infobox film
| name=Jai Vikraanta
| image=Jai Vikraanta.jpg
| image_size =180px
| alt=
| caption=Film Poster Sultan Ahmed
| producer=Sultan Ahmed
| story=
| based on=
| screenplay=
| starring=Sanjay Dutt Zeba Bakhtiyar Amrish Puri
| music=Anand-Milind
| cinematography=R.D. Mathur
| editing=
| studio=Sultaan Productions
| distributor=
| released=  
| runtime=
| country=India
| language=Hindi
}}

Jai Vikraanta is a 1995 Bollywood Action film directed and produced by Sultan Ahmed. The film features Sanjay Dutt, Zeba Bakhtiyar and Amrish Puri as main characters.  

==Cast==
*Sanjay Dutt as Vikraanta A. Singh
*Zeba Bakhtiar as Nirmala Nimu
*Amrish Puri as Thakur Jaswant Singh
*Shahbaaz Khan as DIG Sher Ali Khan
*Suresh Oberoi as Raja
*Deepti Naval as Harnams Wife
*Aruna Irani as Sakina
*Saeed Jaffrey as Police Commissioner
*Ranjeet as Inspector Khote
*Mukesh Khanna as Thakur Harnam Singh

==Music==

The music of the film is composed by Anand-Milind and lyrics are penned by Sameer.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-  style="background:#ccc; text-align:center;"
! # !! Title !! Singer(s)
|-
| 1
| "Rishta Tera Mera Sabse (Male)"
| Pankaj Udhas
|-
| 2
| "Gore Gore Gaal Meri Jaan Ke Dusman"
| Poornima
|-
| 3
| "Tere Honton Pe Bansi Shyam Ki"
| Sadhana Sargam, Kavita Krishnamurthy
|-
| 4
| "Dekh Ke Mera Khilta Husn-O-Shabab"
|  Alka Yagnik
|-
| 5
| "Pyar Ikrar Mere Yaar Ho Gaya"
| Kumar Sanu, Alka Yagnik
|-
| 6
| "Rishta Tera Mera (Female)"
|  Sadhana Sargam
|}

==References==
 

==External links==
* 

 
 
 
 

 