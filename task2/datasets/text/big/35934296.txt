Major Wilson's Last Stand
 
 
{{Infobox film name     = Major Wilsons Last Stand image    = Wilsons_last_stand_show.jpg caption  = Photo by Frederic G. Hodsoll depicting the final scene of Savage South Africa, the play on which the film was based director       =  producer       =  screenplay     = Frank E. Fillis   story          =  based on       = Historical events of the Shangani Patrol (1893) starring  Texas Jack Peter Lobengula Francis "Frank" Edward Fillis music          =  cinematography =  editing        =     released       = September 1899 (UK) studio         = Levi, Jones & Company distributor    =  runtime        = 125 ft.    country        = United Kingdom language       = Silent English intertitles
}}
 silent Short short war film based upon the historical accounts of the Shangani Patrol.  The film was adapted from Savage South Africa, a stage show depicting scenes from both the First Matabele War and the Second Matabele War which opened at the Empress Theatre, Earls Courte, on 8 May 1899.   It was taken on the field by Joseph Rosenthal for the Warwick Trading Co., Ltd.  Copies of this short film originally sold for £3.

The film was shown to audiences at the Olympic Theatre in London and at the Refined Concert Company in New Zealand.

==Story==
The studios original description is as follows:
 Major Wilson and his brave men are seen in laager in order to snatch a brief rest after a long forced march. They are suddenly awakened by the shouts of the savages, who surround them on all sides.  The expected reinforcements alas arrived too late. The Major calls upon his men to show the enemy how a handful of British soldiers can play a losing game as well as a winning one.  He bids them to stand shoulder to shoulder, and fight and die for their Queen. The horses are seen to fall, and from the rampart of dead horses, the heroic band fight to the last round of revolver ammunition. The Major, who is the last to fall, crawls to the top of the head of dead men, savages and horses, and makes every one of the few remaining cartridges find its mark until his life is cut short by the thrust of an assegai in the hands of a savage, who attacks him from behind. Before he falls however, he fires his last bullet into the fleeing carcass of the savage, who drops dead. The Major also expires, and death like silence prevails. The most awe-inspiring cinematograph picture ever produced." 

==Cast== Texas Jack as Frederick Russell Burnham, the American Chief of Scouts   
* Peter Lobengula (the son of the real-life Matabele King) as King Lobengula    Major Allan Wilson   
* Cecil William Coleman as Captain Greenless    Zulu predominantly from the Colony of Natal.   

==See also==
* Shangani Patrol (film)|Shangani Patrol (film) – The historical full-length feature film shot on location in Rhodesia and released in 1970.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 