Fright Night (2011 film)
 
{{Infobox film
| name           = Fright Night
| image          = FrightNight2011Poster.jpg
| alt            = 
| caption        = International poster
| director       = Craig Gillespie
| producer       = Michael De Luca Alison R. Rosenzweig
| screenplay     = Marti Noxon Tom Holland
| based on       =  
| starring       = Anton Yelchin Colin Farrell Christopher Mintz-Plasse David Tennant Imogen Poots Toni Collette
| music          = Ramin Djawadi
| cinematography = Javier Aguirresarobe
| editing        = Tatiana S. Riegel DreamWorks Pictures Michael De Luca Productions Walt Disney Studios Motion Pictures 
| released       =  
| runtime        = 106 minutes  
| country        = United States
| language       = English
| budget         = $30 million   
| gross          = $41 million 
}} 3D Horror comedy horror Tom Hollands 1985 film adapted by premiered at The O2 DreamWorks Pictures, Real D 3D. 

==Plot== Jerry Dandrige, their new neighbor. Fed-up and angry with Ed after he claims that Jerry is a vampire, Charley tells him that hes crazy and that he doesnt want to be friends anymore.

While feeling deeply angry, hurt, and upset on his way home, Ed is confronted by Jerry. Jerry claims that he has been watching Ed and has been aware of Ed watching him. After a short fight and chase Jerry seriously considers making Ed a vampire. He then starts to gradually exploit and manipulate Eds weakness of his emotional state with his former friend (a year earlier, Charley abandoned Ed to hang out with a more popular crowd at school) and his social status with his peers. Jerry convinces Ed into believing that his life would be much better if he was a vampire. Ed subsequently succumbs and willingly allows Jerry to bite him. The next day, Charley realizes that Ed is missing and decides to investigate, starting to believe Eds claims when he discovers video recordings of objects moving on their own, with Eds voiceover revealing that he is recording Jerry to prove that his reflection doesnt show up in recordings. As Jerry begins to attack more people throughout the neighborhood, Charley sneaks into Jerrys house and finds out that he keeps his victims in secret rooms. Charley goes to Las Vegas magician Peter Vincent, a supposed expert on vampires. Peter doesnt take him seriously, and kicks him out.

Jerry comes to Charleys house and sets fire to it. Charley, Jane, and his girlfriend, Amy Peterson, barely escape with their lives, fleeing through the desert in their minivan. Jerry catches up with them, but is wounded by Jane with a real estate sign stake. Jane is admitted to a hospital, where Charley is summoned by Peter. Upon arriving at Peters penthouse Ed turns-up. By now Ed has fully been transformed into a vampire and he aids Jerry in attacking Charley, Amy, and Peter. As they fight, Ed lets all of anger out on his opponent and Charley reluctantly kills Ed. Meanwhile Amy injures Jerry with holy water. They then run into a club, where they get separated in the crowd. Amy is kissed, bitten, and possessed by Jerry, who proceeds to take her.
 Saint Michael that will kill Jerry and turn all of his victims back into humans. Charley goes to Jerrys house where Peter decides to join him after all.

They are led into Jerrys basement, where they are attacked by many of Jerrys victims, including Amy. Charley, having outfitted himself in a flame-retardant suit, lights himself on fire in order to burn and disorient Jerry while he tries to stake him. Peter assists him by shooting a hole in the floor above to allow sunlight in which burns Jerry, then tossing Charley the stake which Charley dropped. Charley quickly stabs Jerry in the heart, killing him and returning his victims to their human form. Afterwards, Charleys mother recovers from the hospital and goes to shop for a new house as Charley and Amy make love in Peters penthouse.

==Cast==
 
* Anton Yelchin as Charley Brewster Jerry 
* Christopher Mintz-Plasse as Edward "Evil Ed" Lee
* David Tennant as Peter Vincent
* Imogen Poots as Amy Peterson
* Toni Collette as Jane Brewster
* Dave Franco as Mark
* Reid Ewing as Ben
* Will Denton as Adam
* Sandra Vergara as Ginger
* Lisa Loeb as Victoria Lee
* Grace Phipps as Bee
 

Chris Sarandon, who portrayed Jerry in the original film, makes a cameo appearance as a motorist killed by the vampire (his character is credited as "Jay Dee", after the initials of his original character).

==Production== Las Vegas set as the backdrop on July 26, 2010, and wrapped on October 1, 2010.  Fright Night was produced by DreamWorks Pictures and distributed worldwide by Walt Disney Studios Motion Pictures through the Touchstone Pictures label.

Steven Spielberg provided a great deal of input in the making of the film, such as storyboarding scenes and assistance with editing.   

==Release==
Although the film received a wide release in the United States on August 19, 2011, an advance screening took place at San Diego Comic-Con International on July 22, 2011.   

===Home media===
The film was released by  s song " s with introductions by director Gillespie, a gag reel, an uncensored music video for "No One Believes Me", "Squid Man – Extended & Uncut", "Peter Vincent: Swim Inside My Mind", "The Official How to Make a Funny Vampire Movie Guide" and "Frightful Facts & Terrifying Trivia" bonus features.      

==Sequel== Sean Power, Sacha Parkinson and Chris Waller. The film was released direct to DVD on October 1, 2013.  Though billed as a sequel, the film repeats the plot of the original and remake, with none of the 2011 cast, and no reference made to events in the previous film (for example, the character of Evil Ed, killed in Fright Night, is alive in the "sequel").

==Reaction==
===Critical response===
Fright Night received positive reviews from critics. Review aggregator   polls reported that the average grade moviegoers gave the film was a "B-minus" on an A+ to F scale. 

Robert Koehler of Variety (magazine)|Variety writes, Fright Night has "a cleverly balanced mix of scares and laughs". 

===Box office===
Fright Night opened in number six in the box office. The film grossed $7,714,388 in its opening weekend and finished with a domestic grossing of $18,302,607 and $22,700,000 in other countries, giving a worldwide total of $41,002,607 against its $30 million budget.   

===Accolades===
Fangoria Chainsaw Awards
* Best Wide-Release Film: 2012 
IGN Best of 2011
* Best Horror Movie: 2011 

==See also==
*Vampire film

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 