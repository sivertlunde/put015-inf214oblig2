Through the Olive Trees
{{Infobox film name = Through the Olive Trees image =Through the Olive Trees poster.jpg caption = director = Abbas Kiarostami producer = Abbas Kiarostami writer = Abbas Kiarostami starring = Hossein Rezai Farhad Kheradmand Mohamad Ali Keshavarz music = cinematography = Hossein Jafarian editing = Abbas Kiarostami distributor = Miramax Films released =    runtime = 103 minutes country = Iran language = Persian budget =
}}
 1994 film directed and written by Iranian director Abbas Kiarostami, set in earthquake-ravaged Northern Iran.

It is the final part of Kiarostamis Koker trilogy, and the plot revolves around the production of the second episode, Life, and Nothing More..., which itself was a revisitation of the first film, Where Is the Friends Home?. Like many of Kiarostamis films, it is filmed in a minimalist, naturalistic way, while also being a complex study of the link between art and life, constantly blurring the boundaries between fiction and reality.

== Plot ==
Hossein Rezai plays a local stonemason turned actor who, outside the film set, makes a marriage proposal to his leading lady, a student recently orphaned after the earthquake. The family of the girl finds his offer insulting however, as he is poor and illiterate, and the girl decides to evade him because of this. She continues evading him even when they are filming, as she seems to have trouble grasping the difference between her role and real life. The fictional couple takes part in what would be the filming of Life, and Nothing More....

The situation complicates further as Hossein still pursues the affections of the young actress while the film goes on. The director learns about this and tries to advise Hossein about what to do. He then illustrates their story and where the conflict began. The girl manages to finish the scene while Hossein woos her and then departs by walking as Hossein runs to follow her.

In a memorable scene, at a great distance, the girl finally gives an answer to Hossein and we are left with him running through a green field and back into the olive grove.   The audience is left to wonder what response was given by the girl.

== Reception ==
The film was well-received amongst international cinema critics, especially in France, and was nominated for the Palme dOr at the 1994 Cannes Film Festival.    It won the Espiga de Oro at the 1994 Seminci in Valladolid. In particular, its ambiguous final scene has been widely discussed and celebrated.

Miramax Films had also acquired the United States distribution rights to Through the Olive Trees and the film was given a limited US theatrical release in 1995. However, Miramax Films hasnt released this movie on DVD yet. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 