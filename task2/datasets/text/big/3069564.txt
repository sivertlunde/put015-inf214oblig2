Joy Ride (2001 film)
 
{{Infobox film
| name           = Joy Ride
| image          = Joy Ride Poster.jpg
| caption        = Theatrical release poster
| director       = John Dahl Chris Moore
| writer         = J. J. Abrams Clay Tarver
| starring       = Steve Zahn Paul Walker Leelee Sobieski
| music          = Marco Beltrami
| cinematography = Jeff Jur
| editing        = Eric L. Beason Scott Chestnut Todd E. Miller Glen Scantlebury New Regency Bad Robot Productions LivePlanet
| distributor    = 20th Century Fox
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $23 million
| gross          = $36,642,838
}}
 thriller road road film. The film was written by J. J. Abrams and Clay Tarver and directed by John Dahl and starring Steve Zahn, Paul Walker and Leelee Sobieski.

Although not a strong commercial hit, the film received enthusiastic reviews by critics and it was followed by two direct to video sequels,   and Joy Ride 3.

== Plot == 1971 Chrysler Newport, heading east to Colorado. However, as he is prepared to leave, his mother calls, and asks him to bail out his troublesome older brother, Fuller (Steve Zahn), in Salt Lake City. Much to Lewiss chagrin, Fuller invites himself on the trip.

The brothers stop at a truck stop near the Wyoming border, where Fuller buys a CB radio for $40 merely for the purpose of fun and breaking the tension in the car. He chats with several truckers in a mock hillbilly accent. When one of the truckers gives a brief cryptic speech, Fuller coaxes Lewis into playing a cruel prank on the mysterious driver, who identifies himself as "Rusty Nail" (Ted Levine). Lewis pretends to be an attractive young woman named "Candy Cane" and sets up a meeting with Rusty Nail in the motel where Lewis and Fuller will be spending the night, in Table Rock, Wyoming and tells him to bring a bottle of pink champagne.

Rusty Nail apparently falls for the joke, genuinely believing Lewis to be female. Instead of telling him to go to room 18, which is their room, Lewis then gives him the number of the room next door to his, room 17, which is occupied by a largely built, highly irritable and obnoxious businessman (Kenneth White), who had a brief altercation with Fuller while he was checking in, and racially abused the middle eastern night manager. That cold and rainy night, the brothers are waiting for Rusty Nail to arrive, and they hear him knocking on the next door. They listen, and they hear a brutal attack and the screams of the businessman. In shock, they stay in their room for the rest of the night.

The next morning, the brothers are questioned by the local police and discover that it was the businessman who was brutally attacked and horrifically mutilated, with his jaw ripped off, and found alive nearby but in a coma. Rusty Nail, meanwhile, has easily evaded the authorities, due to his anonymity and also the untraceable nature of CB radio. Lewis admits the prank to the police officers. They are scolded for this, and although they cannot be arrested, since they didnt commit a crime, they are ordered to leave Wyoming the next day. Back on the road, they encounter Rusty Nail on the CB radio again, who demands the whereabouts of "Candy Cane". Lewis tells him about the joke, and Rusty Nail demands an apology, but Fuller simply yells at him abusively and insults him. Rusty Nail then makes a comment that they should fix their right tail light&nbsp;— revealing he has identified which car they are driving.

Horrified, Lewis and Fuller speed off as fast as they can, and eventually find a nearby gas station to fill up. To their horror, an ice truck enters the station. Convinced the driver must be Rusty Nail, they flee in panic, and the ice truck gives chase. They soon discover the driver is actually one Mr. Jones, and is merely trying to return Lewis credit card, which he left behind at the gas station. At that moment, the real Rusty Nail plows through the ice truck. Another chase ensues, and when they reach a dead end, Rusty Nail slowly drives his truck into their vehicle, turning it onto its side, compelling Fuller to apologize and explain the reason for the joke. Rusty Nail accepts the apology and drives away, declaring his actions to be simply a retaliatory joke.

Believing themselves to be safe, the brothers meet up with Venna in Boulder, Colorado, and are introduced to her roommate, Charlotte (Jessica Bowman), and they drive back to Nebraska, where they stop at a motel. While the three spend a night at a motel, Rusty Nail calls, believing they lied about the presence of a girl. He reveals he has kidnapped Charlotte, and is holding her hostage, and orders Lewis and Fuller to go into a crowded Nebraska diner "completely nude", under the threat of killing Charlotte, so that they will understand how it feels to be "the butt of the joke". Next, he orders them to drive to a cornfield just inside the Nebraska border, step out, and walk 100 ft (50 m) ahead of the car. The truck then ploughs through the field, separating all three, and Rusty Nail kidnaps Venna, and destroys their car, though not before instructing them to meet him in Room 17 of a nearby motel, with pink champagne. But he does not specify which motel they must go to, and there are several in the nearby town.

As Fuller and Lewis search frantically, Rusty Nail binds Venna to a chair nailed to the floor with shrink wrap, gags her with sellotape, and rigs a trap using wire attached to the trigger of a shotgun aimed at her head, and the door of the motel room, so that when opened, it will kill her. Eventually, Fuller and Lewis arrive at the right motel, but find room 17 empty. Rusty calls them and indicates that he is in room 18. Fuller sneaks round the back, and hears muffled talking. He spots Venna through a window, but is attacked by Rusty Nail and barely manages to prevent Lewis from killing Venna. Rusty Nail nails Fuller to a nearby fence, and prepares to drive his truck into him. At that moment, the police arrive (called by Rusty Nail himself, reporting three bodies) and after Lewis manages to push Venna out of the way of the shotgun, he saves Fuller, just as the truck crashes through the motel. Inside the truck, they find a bloodied, dead body in the drivers seat implying that Rusty Nail is dead, and the bound and gagged Charlotte on the floor. The three huddle around an ambulance and receive treatment for their injuries. At this point, the body is revealed to be the innocent ice truck driver, Mr Jones. Lewis, Fuller, and Venna listen to the CB radio of the ambulance as Rusty Nail gives another brief cryptic speech, indicating his survival and escape.

The film ends with Rusty Nails catchphrase: "Looking forward to the storm. Keeps everyone inside, washes everything clean".

== Cast ==
* Paul Walker as Lewis Thomas
* Steve Zahn as Fuller Thomas
* Leelee Sobieski as Venna Wilcox
* Jessica Bowman as Charlotte Dawson
* Matthew Kimbrough as Rusty Nail (voiced by Ted Levine) (uncredited)
* Stuart Stone as Danny, Lewis roommate.
* Brian Leckner as Officer Keeney
* Jim Beaver as Sheriff Ritter
* Hugh Dane as Man at Door
* Jay Hernandez as Marine

== Alternate titles ==
The film also goes under numerous other titles in other countries. In Australia, Sweden, Finland, Ireland and some other European countries the film was retitled Roadkill, Never Play with Strangers in Israel and Spain, Radio Killer in Italy, Never talk to strangers in Greece, Road Killer in Japan, and Mortal Frequency in Mexico.

The film went under the working titles of Candy Cane, Highway Horror, Deadly Frequency and Squelch.

== Production notes ==
In his screenplay for Joy Ride, J. J. Abrams was influenced by the first film directed by Steven Spielberg, Duel (1971 film)|Duel, and there are numerous references to it. For example, in one scene, a seemingly maniacal ICE truck pulls up to a gas station that the brothers have pulled up to. In Duel, a likewise maniacal truck pulls up the protagonist, and a small billboard for ICE is visible in the background.
 359 Peterbilt.

== Alternate endings and deleted scenes ==
On the DVD release there is a 29-minute long alternate ending, and 4 other shorter alternate endings. The main one featured Rusty Nail committing suicide with a shotgun and numerous bodies are found by the police in his trailer. One featured Rusty Nail being arrested, another being beaten in a fight with both Thomas brothers, another wherein he is blown up in his truck, and another saw Rusty Nail run over with his own truck. The ending featured in the actual theatrical cut of the film is the only ending in which Rusty Nail lives. There are also numerous deleted scenes.

In the alternate ending where Rusty Nails truck explodes, you can see a water tower behind the truck as it burns. The original intention was to have the truck hit the water tower and have the water come down and put the flames out so that it would be believable if Rusty Nail survived. However, time constraints kept the scene from being filmed. The water tower cost over $100,000.

Leelee Sobieski filmed two different romantic interludes, one with Steve Zahn and one with Paul Walker during the shooting and re-shooting of the film. Both scenes ended up getting cut. This may explain why Venna appears to be romantically interested in both of them.

== Release ==
=== Critical response ===
 
Joy Ride currently has a "Fresh" score of 73% on Rotten Tomatoes. 

=== Box office ===
The film opened at #5 at the U.S. box office raking in $7,347,259 USD in its opening weekend. 

== Sequels ==
The film was followed by two sequels were released on   (2008) and Joy Ride 3 (2014), featuring the character Rusty Nail from the original film, appeared as a character himself and voice.

== References ==
 

== External links ==
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 