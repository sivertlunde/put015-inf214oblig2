The Healer (film)
 
{{Infobox film
| name           = The Healer
| image          =The Healer (film).jpg
| image_size     =
| caption        =
| director       = Reginald Barker
| producer       = Robert Herrick (novel The Healer) James Knox Millen (adaptation) George Waggner (continuity and dialogue)
| narrator       =
| starring       = Ralph Bellamy Karen Morley Mickey Rooney
| music          =
| cinematography = Harry Neumann
| editing        = Jack Ogilvie
| distributor    =
| released       = June 15, 1935
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Healer is a 1935 American film directed by Reginald Barker. The film is also known as Little Pal (American reissue title).

==Plot==
Dr. Holden (Ralph Bellamy) is "The Healer" (the original title) in this 1930s morally uplifting pot-boiler. He is a doctor that has come home to a warm springs to try to heal children from the unnamed crippling disease (polio). He runs a destitute camp for these children, assisted by Evelyn Allen (Karen Morley) who looks upon the Doc as a great man. Jimmy (Mickey Rooney) is a paraplegic kid whom the Doc promises to cure. This little triangle is interrupted by a rich girl Joan Bradshaw (Judith Allen) who cons the good Doc into building a sanitorium for the wealthy with her fathers money. Doc is momentarily swayed, but comes to his senses just as a forest fire threatens his original cabins around the warm spring. His treatment of Jimmy pays off as Jimmy rides a bicycle to save the day. Doc realizes that his true love is Evelyn, not the self-interested Joan.

==Cast==
*Ralph Bellamy as Dr. Holden
*Karen Morley as Evelyn Allen
*Mickey Rooney as Jimmy
*Judith Allen as Joan Bradshaw
*Robert McWade as Mr. Bradshaw
*Bruce Warren as Dr. Thornton
*J. Farrell MacDonald as Applejack
*Vessie Farrell as Martha

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 