Night in the City
{{Infobox film
| image          = 
| caption        = 
| name           = Night in the City
| writer         = 
| starring       = Ruan Lingyu Jin Yan Li Keng Lin Chuchu
| director       = Fei Mu
| cinematography = 
| producer       = 
| studio         = Lianhua Film Company
| runtime        = 65 min
| released       = 1933 China
| Mandarin
| budget         =
|
}}
Night in the City ( ) (also known as City Nights) was a 1933 Chinese silent film.

==Production background==
Night in the City was the directorial debut of Fei Mu (who would later go on to achieve lasting fame throughout the 1930s and 1940s) and starred actress Ruan Lingyu and actor Jin Yan. Highly acclaimed upon its release, particularly by leftist progressives for its social criticisms of city-life,  the film is now considered a lost film.

==Plot==
The film tells the story of a poor family living in Chinas slums. The daughter, played by Ruan Lingyu, is forced to work at the docks. When it is learned that the slums landlord is preparing to demolish the entire tenement, the daughter is forced to give up her body to her landlords son in exchange for a delay in construction. By the end of the film, the daughter, her family, and the landlords son all leave the city for what they hope will be a better life in the country.

==Preservation== Centre Stage. 

==Cast==
* Ruan Lingyu
* Jin Yan

==See also==
*List of lost films

==References==
 

==Bibliography==
* Hjort, Mette, Stanley Kwans center stage, Hong Kong University Press (2007). ISBN 962-209-791-X.
* Hu, Jubin, Projecting a nation: Chinese national cinema before 1949. Hong Kong University Press (2003). ISBN 962-209-610-7.

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 