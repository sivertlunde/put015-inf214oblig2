9 to 5 (film)
 
 
{{Infobox film
| name           = 9 to 5
| image          = 9_to_5_moviep.jpg
| caption        = Cropped image from the Sexist, Egotistical, Lying, Hypocritical Bigot Edition
| director       = Colin Higgins
| producer       = Bruce Gilbert
| writer         = Patricia Resnick Colin Higgins
| starring       = Jane Fonda Lily Tomlin Dolly Parton Dabney Coleman Marian Mercer Colin Higgins Peggy Pope Elizabeth Wilson Charles Fox
| cinematography = Reynaldo Villalobos
| editing        = Pembroke J. Herring
| distributor    = 20th Century Fox
| released       =  
| runtime        = 110&nbsp;minutes
| country        = United States
| language       = English
| budget         = $10 million 
| gross          = $103,290,500 (USA)   
}}

9 to 5 is a 1980 American comedy film written by Patricia Resnick and Colin Higgins, directed by Higgins, and starring Jane Fonda, Lily Tomlin, Dolly Parton, and Dabney Coleman. The film concerns three working women living out their fantasies of getting even with, and their successful overthrow of, the companys autocratic, "sexist, egotistical, lying, hypocritical bigot" boss.
 television series 9 to 5), with new songs written by Parton, opened on Broadway on April 30, 2009.

9 to 5 is number 74 on the American Film Institutes "100 Funniest Movies"  and is rated "82% fresh" on Rotten Tomatoes. 

==Plot==

Judy Bernly (Jane Fonda) is forced to find work after her husband, Dick (Lawrence Pressman), squanders their savings, loses his job and runs off with his secretary. Judy finds employment as a secretary at Consolidated Companies, a very large corporation. The senior office supervisor is the feisty widow, Violet Newstead (Lily Tomlin). Violet shows Judy the place, while warning her about the two higher-ups. The first is the sleazy, selfish Franklin Hart, Jr (Dabney Coleman); the latter is the crisp but equally obnoxious Roz Keith (Elizabeth Wilson), Harts executive assistant and resident snitch. Violet reveals to Judy that Hart is supposedly involved with his buxom secretary, Doralee Rhodes (Dolly Parton); in reality the married Doralee refuses his advances, but Hart nonetheless leads other staff to believe that they are having an affair.

Hart exploits and mistreats his subordinates regularly. He takes credit for Violets efficiency proposals, while refusing to promote her on the basis that "clients would rather deal with men when it comes to figures."  He cruelly yells at and threatens Judy on her first day after an equipment malfunction. He sexually harasses Doralee, and spreads false rumors that they are having an affair, damaging her credibility with coworkers. Finally, Hart casually fires a worker named Maria over an overheard discussion on salaries.

When Violet discovers that another promotion shed been hoping for has instead gone to a man she herself trained, she confronts Hart about his manipulations and sexism. She then references Harts claims of his purported affair with Doralee (which Doralee enters the room just in time to hear) before storming out, announcing that she needs a drink. Doralee, previously unaware of the rumors and now realizing why her coworkers have been cold to her, informs Hart that she keeps a gun in her purse and warns him that, up until she just learned about the rumors, shed been forgiving and forgetting what Hart has previously done, because of the way she was brought up, and if he ever makes another indecent reference about her, she will change him "from a rooster to a hen with one shot".  She also angrily leaves the office, also proclaiming that she needs a drink.

Judy, upset that Maria has just been fired over a trivial infraction, wants to inform Violet and is told that Violet is at a local bar "getting drunk".  She joins Violet and Doralee, and the three women drown their sorrows together before going to Doralees house.  There, the beginning of the their friendship forms over dinner and smoking some marijuana that belongs to Violets son. They fantasize about getting revenge on Mr Hart, with Judy wanting to shoot him execution style and mount his head on her wall, Doralee wanting to rope him like a steer, and Violet wanting to poison him (the fantasy putting her in the role of a cartoon princess).

The following day, a mix-up leads Violet to accidentally spike Harts coffee with rat poison (a nod to her fantasy the previous night). However, before he can drink any of the tainted coffee, Hart accidentally knocks himself unconscious by falling backward from a faulty office chair, hitting his head on a credenza desk. On hearing he has been rushed to the hospital, Violet, realizing her error, panics, thinking its due to the poison. After they arrive at the hospital, the three mistake a dead police witness for their boss. Violet, in a state of panic and desperation, steals the body of the deceased man and stashes it in the trunk of her car, convincing Doralee and Judy to join her, and the three drive off, planning to somehow dispose of the body so that there can be no autopsy. After a car accident, they discover theyve stolen the wrong body, so they smuggle it back into the hospital.

Hart turns up alive the next morning, much to the shock of Violet, Doralee and Judy. During a break in the ladies room, the three speculate on what could have happened and then vow to forget the nights troubles, but Roz, hiding in one of the stalls, overhears them and relates the conversation to Hart. He confronts Doralee with the information he has just learned, and demands that she spend the night at his house or hell have all three of them prosecuted for attempted murder. Hart refuses to believe it was an accident, so the three kidnap him, with Judy firing at him with Doralees gun (much as she had done in her fantasy), and Doralee tying his hands and legs with a phone cord (similarly to what she had done in her fantasy). Unsure of how to prevent Hart from alerting authorities, the three bring him to his architecture|Tudor-style mansion, keeping him prisoner in his bedroom while they decide to try to find something with which to blackmail him so that he wont have them arrested. The ladies discover that hes been selling Consolidated property behind their backs and keeping the profits for himself. To prove the crime, they need to wait several weeks for the accounting documents to arrive. In the meantime, the women fashion a special bondage device to allow Hart to move around, but keep him confined to his home.

Since Doralee is able to forge Harts signature without difficulty, the three women use the occasion of their bosss absence to effect numerous changes around the office, in his name. These include allowing flexible hours, a job-sharing program that allows people to work part-time, and a daycare center in the building (while Maria, the woman whom Hart had fired, is allowed to return). All the while they conceal the true reason for his disappearance. As it turns out, Hart is so feared and/or hated around the office that nobody questions his absence, with the exception of Roz, whom Violet, under the pretext of the company, sends to the "Aspen Language School" to learn French.

One night, Judy discovers a prowler outside Harts home. It turns out to be her ex-husband Dick, whose marriage to Liza lasted only a week. He tries to get Judy to come back to him until he finds Hart bound and gagged in the upstairs bedroom and believes, erroneously, that Judy has gotten into perverted sex games. Judy sends him away, admitting that shes into everything, even smoking pot, and that his departure was the best thing that ever happened to her.

Harts adoring wife (the feeling is clearly one-sided) Missy (Marian Mercer) returns from vacation early, putting the ladies plan in jeopardy. While still pretending to be the womens prisoner, Hart scrambles to replace the property he stole from Consolidated. He then takes Doralees gun and directs the three women back to the office at gunpoint. Hart is appalled by the changes which have been made in his absence, even though all his employees are delighted with them. Before he can have the three women arrested, Hart receives an unexpected visit from Russell Tinsworthy (Sterling Hayden), the Chairman of the Board.

Mr Tinsworthy has arrived to congratulate Hart for increases in productivity, which are due to the changes the three women have instituted in his name. Hart is only too happy to take credit for everything the ladies have done. Tinsworthy is so impressed that he recruits Hart to work at Consolidateds Brazilian operation for the next few years. Hart is not pleased by this development, but is trapped by his lies. Moments after Tinsworthy and a fearful Hart depart, Roz returns from language school and is stunned to discover Violet, Judy and Doralee celebrating in Harts office, and realizing that she now has to report to them.

A post-credits montage reveals the fate of the major characters. Violet was promoted to Vice President. Judy fell in love and married the Xerox representative. Doralee left Consolidated and became a country and western singer. Hart was abducted by a tribe of Amazons in the Brazilian jungle and was never heard from again.

==Cast==
 
* Jane Fonda as Judy Bernly, the new girl who is forced to find work after her husband has an affair with his secretary, a younger woman named Liza. She becomes friends with Violet and Doralee.
* Lily Tomlin as Violet Newstead, a widow with four kids whos been working at the company for twelve years. She is very knowledgeable around the company, and was the one who trained Hart. Despite her knowledge, she is continually passed over for promotions. She is Judys best friend, helped train her, and seems to understand the loneliness Judy feels.
* Dolly Parton as Doralee Rhodes, a secretary who is presumed to be sleeping with Mr. Hart, despite the fact that she has refused his advances. She is also one of Judys best friends in the office.
* Dabney Coleman as Franklin M. Hart Jr., the strict, overly-tight, lying boss and antagonist of the movie who fires people for no reason and also spread the false rumor that Doralee was sleeping with him. He was eventually reassigned to Brazil.
* Sterling Hayden as Russell Tinsworthy, Consolidateds chairman of the board who likes the new office layout that Violet and the others set up.
* Elizabeth Wilson as Roz Keith, Mr. Harts administrative assistant who is constantly eavesdropping. Henry Jones as Mr. Hinkle, Consolidateds president.
* Lawrence Pressman as Dick Bernly, Judys ex-husband.
* Marian Mercer as Missy Hart, Mr. Harts sweet natured wife who is oblivious to the fact that Mr. Hart has a one-sided thing for Doralee.
* Ren Woods as Barbara, one of Judy and Violets co-workers.
* Norma Donaldson as Betty, another co-worker.
* Roxanna Bonilla-Giannini as Maria Delgado, a friend of Judys who got fired, thanks to Rozs snitching to Hart, but was later reinstated by one of the three girls, under Harts name.
* Peggy Pope as Margaret Foster, an alcoholic secretary (sometimes referred to by other characters as "the old lush") whose catchphrase is "atta girl!" Although Judy feels weird around her at first, she warms up to her and the two become friends.
* Richard Stahl as Meade, another vice president at the company, who doesnt like Hart.
* Ray Vitte as Eddie Smith, a man who works in the mail room. When he meets Judy on her first day, he says, "What? How am I going to get out of this mail room prison if they keep hiring people from the outside? Lady, youre gonna hate it here", which turns out to be true.
* Jeffrey Douglas Thomas as Dwayne Rhodes, Doralees supportive husband. A musician, he is often performing at various gigs, thus allowing Doralee a good deal of free time to get into mischief with Judy and Violet. 
 

==Production==
The movie was based on an idea of Jane Fonda, who had recently formed her own production company, IPC. Fonda:
 My ideas for films always come from things that I hear and perceive in my daily life... A very old friend of mine had started an organisation in Boston called Nine To Five, which was an association of women office workers. I heard them talking about their work and they had some great stories. And Ive always been attracted to those 1940s films with three female stars.  
Fonda says the film was at first going to be a drama, but "any way we did it, it seemed too preachy, too much of a feminist line. Id wanted to work with Lily   for some time, and it suddenly occurred to   Bruce and me that we should make it a comedy."    
Patricia Resnick wrote the first draft drama, and Fonda cast herself, Lily Tomlin and Dolly Parton in the leads, the latter in her first film role.   
Then Colin Higgins came on board to direct and rewrite the script. Part of his job was to make room for all three in the script. Higgins says Jane Fonda was a very encouraging producer, who allowed him to push back production while the script was being rewritten. HIGGINS: WRITER-DIRECTOR ON HOT STREAK
Goldstein, Patrick. Los Angeles Times (1923-Current File)   January 24, 1981: b15.  

"Hes a very nice, quiet, low-key guy", said Dolly of Higgins. "I dont know what I would have done if Id had one of those mean directors on my first film." 

Higgins admitted "he expected some tension", from working with three stars, "but they were totally professional, great fun and a joy to work with. I just wish everything would be as easy." 
 Grapes of Salt of the Earth", says Fonda. "We took out a lot of stuff that was filmed, even stuff the director, Colin Higgins, thought worked but which I asked to have taken out. Im just super-sensitive to anything that smacks of the soapbox or lecturing the audience"." 

Fonda says she did a deal of research for the movie, focusing on women who had begun work late in life due to divorce or being widowed.
 What I found was that secretaries know the work they do is important, is skilled, but they also know theyre not treated with respect. They call themselves office wives. They have to put gas in the bosss car, get his coffee, buy the presents for his wife and mistress. So when we came to do the film, we said to Colin (Higgins), ok, what you have to do is write a screenplay which shows you can run an office without a boss, but you cant run an office without the secretaries!  

==Filming locations==
The home of Franklin Hart is located at 10431 Bellagio Road in Bel-Air, Los Angeles. The Consolidated offices were presumably in the Pacific Financial Center located at 800 W 6th Street, at South Flower Street,  Los Angeles, California 90017.

==Theme song==
  Female Country platinum by the RIAA.

At the same time, newcomer Sheena Easton was enjoying her first major hit in the UK with a song also titled 9 to 5 (Sheena Easton song)|"9 to 5". With the success of Partons song, and to avoid confusion, Eastons record company renamed her recording "Morning Train (9 to 5)" for its North American release.

==Television series==
  ABC (1982–83) and in first run syndication (1986–88), featured Partons younger sister, Rachel Dennison, in Partons role, and Rita Moreno and Valerie Curtin took over Tomlin and Fondas roles, respectively. In the second version of the show, Sally Struthers replaced Moreno. A total of 85 episodes were filmed.

==2009 Broadway musical==
  musical stage adaptation of the film.   A private reading of the musical took place on January 19, 2007   Further private presentations were held in New York City in summer 2007.

In early March 2008,   directed. 
 Broadway at the Marquis Theatre in previews on April 7, 2009, and officially on April 30, 2009.  However, due to low ticket sales and gross, the production closed on September 6, 2009. A National Tour began in September 2010.A UK tour has been arranged for 2012.

==Possible sequel==
In the 1980s Universal developed a sequel with Colin Higgins. Tom Mankiewicz worked on it for a while and says while Dolly Parton was enthusiastic, Jane Fonda was not and Higgins heart was not in it. 

In a TV interview broadcast on BBC1 in the UK in 2005, the movies stars Fonda, Tomlin and Dolly Parton all expressed interest in starring in a sequel. Fonda said if the right script was written she would definitely do it, suggesting a suitable name for a 21st-century sequel would be 24/7. Parton suggested they had better hurry up before they reach retirement age. In the DVD commentary, the three reiterate their enthusiasm; Fonda suggests a sequel should cover outsourcing and they agree Frank Hart would have to return as their nemesis.

==American Film Institute lists==
* 2000: AFIs 100 Years... 100 Laughs—#74
* 2004: AFIs 100 Years... 100 Songs—#78 (for 9 to 5 (Dolly Parton song)|9 to 5)

==See also==
* Horrible Bosses, a 2011 comedy film

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 