Promise Her Anything
{{Infobox film
| name           = Promise Her Anything
| image          = PromiseHerAnything.jpg
| image_size     = 
| caption        = Original poster
| director       = Arthur Hiller
| producer       = Stanley Rubin
| writer         = William Peter Blatty Based on a story by Arne Sultan and Marvin Worth
| narrator       =  Bob Cummings
| music          = Lyn Murray
| cinematography = Douglas Slocombe
| editing        = John Shirley
| studio         = Seven Arts Productions
| distributor    = Paramount Pictures
| released       = November 1965 (UK) February 22, 1966 (US)
| runtime        = 98 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1965 Cinema British romance romantic comedy film directed by Arthur Hiller. The screenplay by William Peter Blatty is based on a story by Arne Sultan and Marvin Worth.

==Plot== psychologist Peter Brock, is a better prospect as a new mate.

Although he is an authority on children, Peter actually despises them, so Michelle decides to keep John Thomas a secret for the time being. Unbeknownst to her, Harley is using the baby in his movies. When John Thomas is admitted to Peters clinic for observation, Harley sneaks into his room to complete a film, but his surreptitious activities are captured by a hidden camera recording the babys behavior. Michelle is furious but, when he saves John Thomas from a potentially dangerous situation, she forgives Harley and decides he may be the better choice for a father after all.

==Production==
The film was shot in its entirety at Shepperton Studios in Surrey, England.
 Tom Jones.

==Behind the scenes==
 
While this film was being made, Leslie Caron and Warren Beatty were having an affair, this while she was married to her second husband Peter Hall. It led to a divorce case where Beatty was named as a co-respondent. The divorce of Caron and Hall was granted, with Beatty being ordered to pay the cost. He and Caron never saw each other afterwards. It created word of Beatty being a notorious womanizer, and he lived up to this in later years.

==Cast==
* Warren Beatty as Harley Rummell
* Leslie Caron as Michelle OBrien Bob Cummings as Dr. Peter Brock
* Keenan Wynn as Angelo Carelli
* Hermione Gingold as Mrs. Luce
* Lionel Stander as Sam
* Asa Maynor as Rusty
* Cathleen Nesbitt as Mrs. Brock
* Baby Michael Bradley as John Thomas
* Warren Mitchell as Frank Focus / Panel Moderator
* Sydney Tafler as Panel Participant Michael Kane as Staff Doctor
* Riggs OHara as Glue Sniffer
* Mavis Villiers as Rustys Mother
* Margaret Nolan as Mail-Order Film Girl
* Donald Sutherland as Autograph-Seeking Father (uncredited)

==Critical reception==
Variety (magazine)|Variety called the film "light" and "refreshing" and added, "Well-paced direction of many fine performances, generally sharp scripting and other good production elements add up to a satisfying comedy."  

Time Out New York said, "This dull attempt at an offbeat and sophisticated romantic comedy falls flat on its face, thanks largely to the usual sluggish direction from Arthur Hiller   … a dismal script by William Peter Blatty."  

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 