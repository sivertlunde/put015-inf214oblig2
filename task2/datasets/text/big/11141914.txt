Sankarlal
 {{Infobox film
| name           = Sankarlal
| image          =
| image_size     =
| caption        =
| director       = T.N.Balu
| producer       =
| writer         = T.N.Balu
| narrator       =
| starring       = Kamal Haasan Sridevi Suruli Rajan, Seema (actress)|Seema, Varalakshmi
| music          = Ilaiyaraaja Gangai Amaran
| cinematography = N. K. Viswanathan
| editing        = V. Rajagopal
| studio         = Sri Sham Films
| distributor    = Sri Sham Films
| released       = 15 August 1981
| runtime        =
| country        = India Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Sankarlal is a Tamil language film starring Kamal Haasan in the lead role of the protagonist. This film was initially planned to be directed by T.N.Balu(He is the director for the famous Tamil films Uyarndhavargal and sattam en kayil). Unfortunately T.N.Balu died half-way before completing this film. So Kamal Haasan himself directed and completed this film. This is supposedly his first direction attempt. The movie did average business at the box office.

==Storyline==
Kamal Haasan plays a father-son dual role in this movie which begins with an ordinary man Dharmalingam (the elder Kamal Haasan) vacationing in Ooty with his family where he is framed for murder by a criminal named Chelladurai (R.S Manohar) and is jailed while his wife, son Mohan (the younger Kamal Haasan) and daughter (Seema) all end up split from each other. After several years, the vengeful Dharmalingam returns to confront Chelladurai as the secretive Sankarlal and also desperately is on the lookout for his lost family. Chelladurai has meanwhile kidnapped Hema (Sridevi) for ransom and Mohan is deputed by Hemas father to rescue her. This results in Dharmalingam and Mohan locking horns in their fight against their common enemy Chelladurai.

==Cast==
Kamal Haasan 
Sridevi 
Seema 
R.S Manohar 
Suruli Rajan 
Varalakshmi

==Soundtrack== 
The music was composed by Ilaiyaraaja.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Elankiliyae || S. Janaki|Janaki, S. P. Balasubrahmanyam || Pulamaipithan || 04.33 
|- 
| 2 || Ada Vaada || S. P. Balasubrahmanyam || Pulamaipithan || 04.24 
|- 
| 3 || Kasturi Maan || S. Janaki|Janaki, Malaysia Vasudevan || Pulamaipithan || 04.36 
|-  Janaki || Pulamaipithan || 04.27 
|- 
| 5 || Thedinen || Malaysia Vasudevan || Gangai Amaran || 04.24 
|- 
| 6 || Unmai || Vani Jayaram, Malaysia Vasudevan || Panchu Arunachalam || 04.13 
|}

==References==
 

==External links==
*  

 
 
 
 
 


 