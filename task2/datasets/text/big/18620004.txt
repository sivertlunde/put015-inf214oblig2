La Ghriba
{{Infobox film
| name           = La Ghriba
| image          = Synagogue-djerba-jerb-1-.jpg
| image_size     =
| caption        =
| director       = Wolfgang Lesowsky
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 2001
| runtime        = 45 min.
| country        = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 documentary about Tunisian island of Jerba, where Jews and Muslims live side-by-side peacefully.

==Summary==
According to local tradition, the first Jews settled on Jerba in the wake of the first Temple’s destruction in Jerusalem in 566 BCE. A community with true foundations in the Jewish life of the ancients, island tradition holds that its synagogue -– known as La Ghriba (“the miraculous”) — is built with a stone from the ruins of the First Temple, and houses the oldest known Sefer Torah in its sanctuary.
 Berber neighbors.

Along their valued status as fully Tunisian, Jerba’s Jews maintain a strong pride in their Judaism, and they revel in showing it. The film captures a yearly festival held just outside La Ghriba, commemorating two famous rabbis. The occasion is one of music and dancing, and helps continue the tradition of drawing Jews from around the world to Jerba –- if only for a visit. Such visitors have the reputation of leaving the island with a future husband or wife in town.

And yet, the Jewish community is so utterly suffused with its Tunisian culture, that it is outwardly almost indistinguishable. Repeated shots of the wide variety of Jerba’s residents reveal just how much both Jew and non-Jew in this millennia-old community share similarities in both physical appearance and their shared Arabic and French language.

But they retain individualizing brands of orthodoxy when it comes to their respective religions. Even though both communities share a love for couscous, vegetable dumplings and potato soup, because many of Jerba’s Jews keep strictly kosher, they rarely share meals with Muslim neighbors.

==Resoureces==
* 

==See also==
*Djerba
*Islam and other religions
*Projects working for peace among Arabs and Israelis
Other comperable Jewish films about the Diaspora: Jews of Iran
*Next year in Argentina
*Trip to Jewish Cuba
*In Search of Happiness

==External links==
* 
* 
* 
* 

 
 
 
 

 