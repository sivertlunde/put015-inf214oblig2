Head of a Tyrant
{{Infobox film
| name = Head of a Tyrant
| image = Head-of-a-tyrant-movie-poster-1960-1020253911.jpg
| image_size = 
| caption = 
| director = Fernando Cerchio
| producer = Piero Ghione 
| writer = Friedrich Hebbel  (play)   Guido Malatesta    Damiano Damiani    Gian Paolo Callegari       Fernando Cerchio 
| narrator = 
| starring = Massimo Girotti   Isabelle Corey   Renato Baldini   Yvette Masson
| music = Carlo Savina 
| cinematography = Pier Ludovico Pavoni 
| editing = Gianmaria Messeri 
| studio = Faro Film   C.F.P.C
| distributor = Constantin Film (West Germany)   Universal Pictures (US)
| released = 26 February 1959 
| runtime = 94 minutes
| country = France   Italy Italy
| budget = 
}}
Head of a Tyrant or Judith and Holofernes (Italian:Giuditta e Oloferne) is a 1959 Italian-French historical film directed by Fernando Cerchio and starring Massimo Girotti, Isabelle Corey and Renato Baldini. 
 Judith and Holofernes was also inspired by the tale.

==Cast==
*    Massimo Girotti as Holophernes  
* Isabelle Corey as Judith  
* Renato Baldini as Arbar  
* Yvette Masson as Rispa  
* Gianni Rizzo as Ozia  
* Camillo Pilotto as Belial 
* Lucia Banti as Servant Girl 
* Ricardo Valle as Isaac  
* Leonardo Botta as Gabriele  
* Franco Balducci as Galaad  
* Luigi Tosi as Irasa 
* Gabriele Antonini as Brother 
* Daniela Rocca as Naomi  
* Enzo Doria as Daniel  

==References==
 

==Bibliography==
* Parish, James Robert. Film Directors Guide:Western Europe. Scarecrow Press, 1976.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 