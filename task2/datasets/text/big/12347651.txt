Shadows Run Black
{{Infobox Film
  | name = Shadows Run Black
  | image = ShadowsRunBlackCostner.jpg
  | caption = European DVD cover of Shadows Run Black
  | director = Howard Heard
  | producer = Charles E. Domokos Laurel A. Koernig William J. Kulzer Eric Louzil Julius Metoyer
  | writer = Duke Howard Craig Kusaba
  | starring = Elizabeth Trosper William J. Kulzer Kevin Costner Shea Porter George Engelson
  | music =
  | cinematography = John Sprung
  | editing =  Raúl Dávalos Davide Ganzino 
  | distributor = Troma Entertainment
  | released = 1986 
  | runtime = 91 minutes
  | language = English
  | budget =  
  | preceded_by = 
  | followed_by = 
  }}
 crime thriller thriller directed by Howard Heard and starring Kevin Costner.

The film follows a tough Los Angeles detective as he races against time to discover who is behind a string of brutal serial slayings. Costner plays the films main suspect.

Tagline: Pray he kills you before you scream.

== Critical reception ==

Allmovie called the film "execrable" and an "exploitational slasher film|slice-n-dicer masquerading as a police thriller". 

== References ==

 

==External links==
* 
*  

 
 
 
 
 


 