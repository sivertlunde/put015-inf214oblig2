We're Funny That Way!
{{Infobox television show_name            = Were Funny That Way! image                = caption              = genre                = Documentary film|Documentary/Comedy creator              = David Adkin developer            = starring             = theme_music_composer = opentheme            = endtheme             = country              = Canada language  English
|num_seasons          = 2 num_episodes         = 7 list_episodes        = producer             = executive_producer   = director         = David Adkin location             = Toronto, Ontario camera               = runtime              = 90 minute special (1998) 30 minutes (2007) network              = Bravo (Canada)|Bravo/OUTtv picture_format       = audio_format         = first_aired          = 1998 (special) September 17, 2007 (series) last_aired           = website              =
}}
Were Funny That Way! was an annual comedy festival in Toronto, Ontario, Canada, held from 1997 to 2012. Launched in 1996 by Maggie Cassella,  the festival featured stand-up and sketch comedy shows by LGBT|lesbian, gay, bisexual and transgender comedians.

The inaugural festival was held in April 1997 at Torontos Buddies in Bad Times theatre.

Proceeds from the festival supported the Were Funny That Way Foundation, aimed at donating money to LGBT charities in the smaller towns and cities across Canada. 

The festival was the subject of Were Funny That Way!, a 1998 documentary film by  , February 9, 1999.  which interviewed and featured performance clips of various performers who appeared at the inaugural festival.  A short-run television series also aired in 2007 in conjunction with the festivals 10th anniversary, using the same format to profile comedians appearing at the 2005 and 2006 editions of the event. Jesse Kohl,  . Media in Canada, August 10, 2007. 

The festival had its final edition in 2012. 

==Film== Steve Moore, Christopher Peterson, Bob Smith, John McGivern and The Nellie Olesons. 
 Bravo Canada, HBO and Citytv. 

==Television series==
A spinoff television series, also titled Were Funny That Way!, aired on Canadas OutTV in 2007.  The six-episode series featured highlights from the 2005 and 2006 Were Funny That Way festivals, including performances by Kate Rigg, Dina Martina, Maggie Cassella and Trevor Boris.

==Notes==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 