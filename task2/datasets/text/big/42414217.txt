Getúlio (film)
{{Infobox film
| name           = Getúlio
| image          = Getúlio Film Poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Brazilian theatrical poster
| director       = João Jardim
| producer       = Carla Camurati
| writer         = George Moura
| based on       = 
| starring       = Alexandre Borges Tony Ramos Leonardo Medeiros Fernando Luís Drica Moraes
| music          = Federico Jusid
| cinematography = Walter Carvalho
| editing        = Pedro Bronz
| studio         = Globo Filmes Copacabana Filmes Fogo Azul Filmes Midas Filmes
| distributor    = Europa Filmes  
| released       =   
| runtime        = 100 minutes {{cite web|url=http://globofilmes.globo.com/Getulio/ |title=GETÚLIO
 |publisher=Globo Filmes|accessdate=2014-04-05}} 
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = $2,868,827    biographical drama Brazilian president Getúlio Vargas, based on the final moments of the crisis that led to the death of the then president, during the 19 days before the August 24, 1954.   

The film was released in Brazil on May 1, 2014, coinciding with the Workers Day official holiday – in reference to the labor policies implemented by Getúlio. {{cite web|url=http://rollingstone.uol.com.br/video/tony-ramos-sofre-e-se-irrita-em-trailer-de-igetulioi-filme-s/ |title=Tony Ramos sofre e se irrita em trailer de Getúlio, filme sobre o ex-presidente da República
 |publisher=Rolling Stone|accessdate=2014-04-05}} 

==Plot==
Getúlio Vargas was president during two distinct periods in the history of Brazil: during the Vargas Era and, later, elected by vote, and is considered one of the most remarkable political of Brazil, in the 20th century.   

The plot begins in August 1954, after the attempted murder of Carlos Lacerda (Alexandre Borges), owner of the opposition newspaper, then, president Getúlio has to confront with the increasing instability of his government, in addition to the charges of ordering the murder of his political enemy.   

==Cast==
 
* Tony Ramos as Getúlio Vargas
* Drica Moraes as Alzira Vargas
* Alexandre Borges as Carlos Lacerda
* Leonardo Medeiros as General Caiado
* Fernando Luís as Benjamim Vargas Daniel Dantas as Member of the Opposition
* Murilo Elbas as João Zaratimi 
* Sílvio Matos as General Carneiro de Menezes
* José Raposo as Nero Moura
* Adriano Garib as General Genóbio da Costa
* Thiago Justino as Gregório Fortunato
* Luciano Chirolli as General Tavares
* Marcelo Médici as Lutero Vargas
*Clarisse Abujamra as Clarisse Vargas
 

==Production==
===Development===
With experience in documentary films, the director João Jardim has gathered an extensive material research, including films and rare books, documents and biographies of contemporary politicians, as well as notes of Alzira Vargas, Getúlios daughter. "This allowed a deep dive on the contradictory figure of the political leader, and nationalist dictator," praises Tony Ramos.   

The idea for the film came about, according to the director, shortly after filming the documentary Pro Dia Nascer Feliz about education and schools in Brazil. "What I loved was not talking about Getúlio, but tell a story that echoes still today, which is connected with the formation of our country, with our current moment".   

Jardim revealed that chose Tony Ramos for the role due to his similarities of personality with the character. "Getúlio was reserved and charismatic, I was looking for someone like that, and the differential of Tony was the talent. He does everything brilliantly" admits, emphasizing the originality of the story in his film. "Its a story that completes 60 years and that had never been told," he said.   

===Filming===
Filming took place in the Catete Palace, in Rio de Janeiro, seat of the federal government at the time and where Getúlio killed himself with a gunshot to the chest, in 1954.    

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 