Bedtime Stories (film)
{{Infobox film
| name           = Bedtime Stories
| image          = Bedtime stories.jpg
| caption        = Theatrical release poster
| director       = Adam Shankman Andrew Gunn Jack Giarraputo
| writer         = Matt Lopez Tim Herlihy
| starring       = Adam Sandler Keri Russell Jonathan Morgan Heit Laura Ann Kesling Guy Pearce Russell Brand Richard Griffiths Teresa Palmer Lucy Lawless Courteney Cox
| music          = Rupert Gregson-Williams Michael Barrett
| narrator       = Jonathan Pryce
| editing        = Tom Costain
| studio         = Walt Disney Pictures Happy Madison Productions Gunn Films Offspring Entertainment Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $80 million
| gross          = $212.9 million  
}} American family Andrew Gunn Andrew Gunns company Gunn Films co-produced the film with Walt Disney Pictures.

==Plot==
 
Skeeter Bronson (Adam Sandler) is a hotel handyman who was promised by his father, Marty Bronson (Jonathan Pryce), to be the manager of the family hotel. A mysophobe named Barry Nottingham (Richard Griffiths) agreed to keep that promise when the Bronson family sold their hotel to him—then built a new hotel instead. Thirty years later, when the story begins, Skeeter is the hotels handyman while management is held by Kendall (Guy Pearce). Barrys new hotel, the Sunny Vista Nottingham Hotel, is a hit, but hes got plans to build an even more elaborate hotel, one designed around a theme that hes keeping secret.

Skeeters sister and principal of Webster Elementary School, Wendy (Courteney Cox), asks Skeeter to watch her kids, Bobbi (Laura Ann Kesling) and Patrick (Jonathan Morgan Heit), while she goes out of town. Skeeter does not know his niece and nephew very well, but agrees to watch them. Helping him out during the day is Wendys friend, Jill Hastings (Keri Russell), an elementary teacher who works at the same school as Wendy. That night, putting Bobbi and Patrick to bed, Skeeter gives them a bedtime story|story, one obviously inspired by his own life as an "underappreciated" handyman: a downtrodden squire "Sir Fixalot" rivals the pompous "Sir Buttikiss" in competition for a new job. The kids add their own details such as the king giving Sir Fixalot a chance to prove himself, a mermaid based on Jill, and a downpour of gumballs when Fixalot prevails.

The following day, while fixing Barrys television, Skeeter learns that the new hotels surprise theme will be rock and roll. Barry is shocked to learn from Skeeter that the idea was already used for the Hard Rock Hotel. Barry offers Skeeter a chance to compete with Kendall for a better theme. While driving, Skeeter is suddenly greeted with a shower of gumballs which he does not see is caused by a crashed candy delivery truck. Skeeter concludes that the story had come true and quickly develops a plan.

For the next story, he chooses a Western in which he receives a horse named "Ferrari" from a Native American horse trader (Rob Schneider) for free. The children change the story to have him save a damsel in distress. They claim he should be rewarded with a kiss, only to have a dwarf kick him instead. That night, Skeeter goes out in search of his Ferrari and meets a man (also played by Rob Schneider), who steals his wallet (that only had three dollars and a baseball card). Barrys daughter, Violet Nottingham (Teresa Palmer), hounded by the paparazzi, is rescued by the passing Skeeter. Just as he is about to kiss her, he is kicked by a dwarf. The dwarf then flees to a nearby AMC Gremlin, hops in, and flees. From this point, he determines that it is only the changes made by the children that affect his reality.

The following night, Skeeter tries to sell the kids on the theme ideas contest for the new hotel, but they are more interested in romance and action in their stories. The next story is centered around a Greek gladiator, Skeeticus, who, after impressing the emperor and a stadium of onlookers, attracts the attention of the most beautiful maiden. After a meal in which all the girls who used to pick on him in high school were so impressed by the beautiful maiden he is with, they start randomly singing the "Hokey Pokey." After Skeeticus saves a mans life, a rainstorm sends him and the maiden into a magical cave which has Abraham Lincoln in it. Skeeter loses his patience with the story and upsets the children, telling them that their stories have nothing to do with real life. Unable to get them to continue, the story ends.

The next day, Skeeter learns Violet will not be meeting with him per the story design, but unexpectedly runs into Jill at the beach who invites him to lunch. Recognizing girls at the restaurant from his high school days, Skeeter asks Jill to pretend to be his girlfriend. The girls are plainly impressed and then inexplicably break into the "Hokey Pokey." Walking on the beach with Jill, Skeeter casually saves the life of a man before a sudden rainstorm sends them under the dock. Skeeter realizes that the girl in the stories is Jill, not Violet, and that he is falling in love with her. As they are about to kiss, Skeeter remembers that Abe Lincoln is supposed to appear and moves away. Instead, an American penny (with Lincolns face on it) falls from through the cracks of the dock, completing the story.

For Skeeter and the kids final night together, a space-themed story begins with Skeeters character who battles Kendalls character in anti-gravity. Skeeters character, who speaks in alien gibberish, wins and Skeeter quickly ends the story. Patrick interjects that the story is too predictable and—remembering Skeeters argument against whimsically happy endings—pointless. Instead, Skeeters character is incinerated by a fireball and there ends the story.

Panicking, Skeeter sees/hears signs of fire everywhere. At Barrys luau-themed birthday party, while dodging many fiery hazards, Skeeters tongue is stung by a bee, making him as hard to understand as his character was in the last of the stories. Luckily, Skeeters best friend, Mickey (Russell Brand), can still understand him and offers to translate for him. Kendalls idea is for a hotel with a theme celebrating Broadway musicals—an idea that impresses no one. Barry much prefers Skeeters approach—simply reminding them of how much fun children have when staying at a classy hotel. After winning the competition, Skeeter thinks hes found his happy ending. Instead, Kendall reveals to Skeeter that the new hotel is replacing Jill, Patrick and Bobbis school, which is to be demolished the next day. Stunned at that, Skeeter then panics when he sees Barrys oversized birthday cake. Skeeter douses the candle and Barry with a fire extinguisher. Barry immediately tells Skeeter that hes fired.

Afterwards, Jill, Patrick, and Bobbi discover that the school where they all work and attend is to be knocked down to make way for the new hotel, and they are all upset with Skeeter, refusing to believe that he didnt know about the location. Wendy believes him, but is upset because he taught her children not to believe in happy endings. She confesses that she had always been jealous of his and their fathers ability to believe in made up stories and have fun the way she never did and had secretly hoped that, by leaving her children with him, his fun loving nature would rub off on them. When they attend the demolition to protest, Skeeter is inspired to prevent the school from being demolished—Donna Hynde (Aisha Tyler), one of the girls from his high school days, is a zoning commissioner, and helps find Barry Nottingham an alternative location on the beach in Santa Monica. Skeeter takes Jill on a wild motorcycle ride (during which Skeeter steals back his wallet from the thief (Rob Schneider) who stole it) which ends at the school and manages to stop the countdown of the demolition. As a reward, Skeeter asks Jill for a kiss and she gladly complies.

Sometime later, Skeeter finds Martys Motel (named after his late father) while Kendall and his scheming partner, Aspen (Lucy Lawless), are demoted to Skeeters motel wait staff. In the films conclusion, Marty Bronson narrates that Barry Nottingham overcame his fear of germs to the degree that he left the hotel business to became a school nurse at Webster Elementary School. His daughter, Violet Nottingham, became the new owner of her fathers hotel business and married Mickey, while Skeeter and Jill got married as well and live happily ever after.

==Cast==
* Adam Sandler as Skeeter Bronson, a handyman and the protagonist
** Thomas Hoffman as Young Skeeter
* Keri Russell as Jill Hastings, Wendys friend and Skeeters love interest 
* Guy Pearce as Kendall Duncan, Skeeters rival and the films antagonist 
* Russell Brand as Mickey, a waiter and Skeeters best friend
* Richard Griffiths as Barry Nottingham, the owner of Sunny Vista Nottingham and Skeeters boss
* Teresa Palmer as Violet Nottingham, Barrys daughter and Kendalls girlfriend
* Lucy Lawless as Aspen, Kendalls scheming partner
* Courteney Cox as Wendy Bronson, Skeeters older sister
** Abigail Droeger as Young Wendy
* Jonathan Morgan Heit as Patrick, Skeeters nephew
* Laura Ann Kesling as Bobbi, Skeeters niece
* Jonathan Pryce as Marty Bronson, Skeeter and Wendys father
* Annalise Basso as Tricia Sparks
* Nick Swardson as Engineer
* Aisha Tyler as Donna Hynde, a zoning commissioner who joins forces with Skeeter  to foil Kendalls plot 
* Allen Covert as Ferrari Guy
* Blake Clark as Biker
* Kathryn Joosten as Mrs. Dixon
* Mikey Post as Angry Dwarf
* Rob Schneider as Indian Chief/The Robber  
* Arne Starr as Nottingham Employee/Senator/Cowboy/Spaceman  
* Jonathan Loughran as Party Guest
* Heather Morris as Cat Dancer
* Horse as Red Horse

==Production==
Director Adam Shankman describes Adam Sandlers character as "a sort of Cinderfella character" and adds that "Hes like Han Solo..." 

==Music==
{{Infobox album  
| Name      = Bedtime Stories
| Type      = Film score
| Artist    = Rupert Gregson-Williams
| Cover     = Bedtime Stories Soundtrack.jpeg
| Released  = December 23, 2008
| Genre     = Soundtrack, film score
| Length    = 35:11 Walt Disney
| Reviews   =
}}
 Journey song "Dont Stop Believin" is played during the film and during the end credits.
{{Track listing
| title1          = The Sunny Vista Motel
| length1         = 4:15
| title2          = The Tale of Sir Fixalot
| length2         = 3:04
| title3          = Raining Gumballs
| length3         = 1:16
| title4          = The Fat Mouse
| length4         = 1:54
| title5          = The Wild West Adventure
| length5         = 2:15
| title6          = Rooftop Camp Out
| length6         = 2:34
| title7          = The Legend of Skeetacus
| length7         = 1:57
| title8          = Almost a Kiss
| length8         = 1:53
| title9          = Space Odyssey
| length9         = 3:08
| title10         = Skeeters Pitch
| length10        = 3:17
| title11         = At the Nottingham Broadway Mega Resort
| note11          = Performed by Guy Pearce
| length11        = 1:18
| title12         = Youre Supposed To Be the Good Guy
| length12        = 3:49
| title13         = Motorcycle Rescue
| length13        = 3:26
| title14         = Happily Ever After
| length14        = 1:07
}}

==Reception== Marley & The Curious Case of Benjamin Button. However, during the standard 3-day weekend, it jumped ahead of The Curious Case of Benjamin Button ranking #2 behind Marley & Me with $27.5 million.  As of February 2009, the film had grossed $110,101,975 in the United States and Canada and  $102,772,467 in other countries, totaling $212,874,442 worldwide.

==Home media release==
The film was released on Blu-ray Disc and DVD on April 7, 2009. The DVD was released as a single disc or a two-disc edition including behind-the-scenes featurette. Commercials advertising the discs feature background music recycled from the film Back to the Future Part III. As of November 1, 2009 the DVD has sold 2,835,662 copies generating $49,409,944 in sales revenue. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 