Guardatele ma non toccatele
{{Infobox film
| name           = Guardatele ma non toccatele
| image          =
| caption        = 
| director       = Mario Mattoli
| producer       = Isidoro Broggi Renato Libassi
| writer         = Bruno Baratti Franco Castellano Giuseppe Moccia Ettore Scola
| starring       = Ugo Tognazzi
| music          = 
| cinematography = Riccardo Pallottini
| editing        = Gisa Radicchi Levi
| distributor    = 
| released       = 1959
| runtime        = 90 minutes
| country        = Italy 
| language       = Italian
| budget         = 
}}

Guardatele ma non toccatele is a 1959 Italian comedy film directed by Mario Mattoli and starring Ugo Tognazzi.

==Cast==
* Ugo Tognazzi - Maresciallo La Notte
* Caprice Chantal - Rebecca OConnor
* Johnny Dorelli - Tenente Altieri
* Lynn Shaw - Unausiliara
* Bice Valori - Irma La Notte
* Corrado Pani - Claudio
* Liana Orfei - Unausiliaria
* Edy Vessel
* Raimondo Vianello - Il colonnello
* Fred Buscaglione - Himself
* Bruce Cabot - Il pilota
* Chelo Alonso - Unausiliaria
* Giacomo Furia - Addetto allo spaccio

==External links==
* 

 

 
 
 
 
 
 
 
 