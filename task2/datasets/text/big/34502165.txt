Just Another Day (2008 film)
{{Infobox film
| name           = Just another day
| image          = 
| caption        = 
| director       = Hisham Zreiq
| producer       = 
| writer         = Hisham Zreiq
| starring       = Hisham Suliman   Ziad Awaise
| music          = 
| cinematography = Ossama Bawardi
| editing        = Hisham Zreiq 
| distributor    = 
| released       = 
| runtime        = 12 minutes
| country        = Germany Palestine
| language       = Arabic language|Arabic, German
| budget         = 
| gross          = 
}}
Just Another Day is a short film by the Palestinian visual artist and filmmaker Hisham Zreiq (Zrake). The film deals with post 9/11 status of Arabs in the Western world, via the story of a young Arab man that lives in Europe after the September 11 terror attacks.  The story is told in a day when his city is struck by another terror attack, in a sad chapter of his life, because he is suffering after the woman he loves left him – precisely because he is an Arab. 

The film is full of symbols, and a surrealistic atmosphere resembling Hisham Zreiqs art. 

== Film festivals ==
* 18th Damascus International Film Festival, 2010, Syria (nominated) 
* Salento International Filem Festival, 2010, Italy (nominated) 
* Al Ard Doc Film Festival, 2011, Cagliari, Italy 
* Palestine Film Festival in Madrid, 2010, Spain  
* Chicago Palestine Film Festival, 2009, USA 

==See also==
* Hisham Zreiq
* The Sons of Eilaboun

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 

 