Tada, Kimi o Aishiteru
{{Infobox film
| name           = Tada, Kimi o Aishiteru
| image          = Tada, Kimi wo Aishiteru.jpg
| caption        = Original poster
| director       = Takehiko Shinjo
| producer       = Toshiya Nomura
| writer         = Takuji Ichikawa (novel) Kenji Bando (screenplay)
| starring       = Aoi Miyazaki Hiroshi Tamaki Meisa Kuroki
| cinematography = Mitsuru Komiyama
| editing        = Yoshifumi Fukazawa
| studio         = Avex Entertainment IMJ Entertainment Toei Company
| distributor    = Toei Company 
| released       = June 3, 2006 (Japan) March 16, 2007 (Taiwan)
| runtime        = 116 minutes
| country        = Japan
| language       = Japanese
| gross          = US$5,751,542 
}}
 romance and drama film based on the novel    written by Takuji Ichikawa.    It was also released as a manga. The film was directed by Takehiko Shinjo, and focuses on the relationship that evolves between a photographer named Makoto, and two of his female university classmates, Shizuru and Miyuki.   

==Plot==
Makoto, a freshman on his first day at university, meets a cute girl named Shizuru. Makoto is normally shy around people, but is attracted to Shizuru for her childlike appearance and behavior. Shizuru wants to be with Makoto, so she develops an interest in his hobby of photography. The two spend time together taking photos in a nearby forest. However, Makoto soon develops stronger feelings for another student named Miyuki, who is beautiful and more well developed. Shizuru, seeing this, hints to Makoto that she will grow up to be a beautiful woman, and he will be sorry for not picking her. One day, she tells Makoto that she wants to take a photo of them kissing in the forest as a present for her birthday, which they do.

After this, Shizuru unexpectedly leaves school and is not heard from again for two years. Makoto receives a letter asking him to come to New York to see Shizurus debut photography exhibit. By then Makoto has broken up with Miyuki because he has decided he really loves Shizuru, and will wait for her to return. When he arrives in New York, he is greeted by Miyuki, and finds out that Shizuru was hiding a disease from him, and has died. Apparently when she fell in love with Makoto, and started eating more to "grow up" for him, she accelerated her disease. He goes to the exhibit and sees many photos of himself, and a huge photo of Shizuru all grown up and beautiful, as she had foretold. There is also the photo of the two of them kissing in the forest, with a caption saying that this was her one true love.

==Cast==
*Aoi Miyazaki as Shizuru Satonaka
*Hiroshi Tamaki as Makoto Segawa
*Meisa Kuroki as Miyuki Toyama Misa Uehara as Saki Inoue
*Munetaka Aoki as Ryo Shirohama
*Keisuke Koide as Kyohei Sekiguchi
*Asae Ōnishi as Yuka Yaguchi

==Soundtrack==
The films theme song, "Renai Shashin" (Love Photo), is a ballad sung by Ai Otsuka. The title of the movie was taken from the lyrics of the song.  The single was released by Ai Otsuka on October 25, 2006. In its first week of sales, the single debuted at number 2, being Otsukas highest debut sales of the year with 77,570 copies sold. The song also won the "Best Video From A Film" and "Best Pop Video" awards at the MTV Video Music Awards Japan 2007.

==Reception==
Tada, Kimi o Aishiteru earned US$5,311,676 at the Japanese box office.   

==References==
 

==External links==
*  
* 
* 

 
 
 
 
 
 
 