Annadha Shilpangal
{{Infobox film 
| name           = Annadha Shilpangal
| image          =
| caption        =
| director       = MK Ramu
| producer       = PS Veerappa
| writer         = Venkiteswar S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan
| starring       = Prasad Sankaradi Sreelatha Namboothiri T. R. Omana
| music          = R. K. Shekhar
| cinematography = V Namas
| editing        = SA Murukesh
| studio         = PSV Pictures
| distributor    = PSV Pictures
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by MK Ramu and produced by PS Veerappa. The film stars Prasad, Sankaradi, Sreelatha Namboothiri and T. R. Omana in lead roles. The film had musical score by R. K. Shekhar.   

==Cast==
 
*Prasad
*Sankaradi
*Sreelatha Namboothiri
*T. R. Omana
*William Thomas
*Paul Vengola
*Kalmanam Murali
*Bahadoor
*ML Saraswathi Sudheer
*T Jayasree
*Usharani
*Prem Navas
 

==Soundtrack==
The music was composed by R. K. Shekhar and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Achankovilaattile || S Janaki, P Jayachandran || Sreekumaran Thampi || 
|-
| 2 || Kathaatha Karthika || P Susheela || Sreekumaran Thampi || 
|-
| 3 || Paathividarnnoru || S Janaki || Sreekumaran Thampi || 
|-
| 4 || Sandhyaaraagam Maanjukazhinju || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 5 || Theerthayaathra Thudangi || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 