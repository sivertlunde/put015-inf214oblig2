Broadway Rose (film)
{{Infobox film
| name           = Broadway Rose
| image          = Broadway Rose 1922.jpg
| caption        = Mae Murray and Monte Blue in Broadway Rose
| director       = Robert Z. Leonard
| producer       =
| screenplay     = Edmund Goulding
| story          = Edmund Goulding
| starring       = Mae Murray Monte Blue
| cinematography = Oliver T. Marsh
| studio         = Tiffany Pictures
| distributor    = Metro Pictures
| released       =  
| runtime        = 60 mins.
| country        = United States
| language       = Silent English intertitles
}} 1922 American silent romantic drama film released by Metro Pictures and directed by Robert Z. Leonard. It stars Leonards then-wife Mae Murray and Monte Blue. The film is based on an original story by Edmund Goulding written for star Murray, and was produced by Leonards and Murrays production company Tiffany Pictures.

Prints of the film are housed at the George Eastman House and Gosfilmofond in Moscow.  

==Cast==
* Mae Murray - Rosalie Lawrence
* Monte Blue - Tom Darcy
* Raymond Bloomer - Hugh Thompson
* Ward Crane - Reggie Whitley
* Alma Tell - Barbara Royce Charles Lane - Peter Thompson
* Maude Turner Gordon - Mrs. Peter Thompson
* Jane Jennings - Mrs. Lawrence
* Pauline Dempsey - Maid

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 