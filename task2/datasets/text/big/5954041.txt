The Fairylogue and Radio-Plays
 
{{Infobox film
| name           = The Fairylogue and Radio-Plays
| image          = Fairylogue.jpg
| caption        = A production still displaying the quality of the Radio-Play coloring process.
| director       = Francis Boggs Otis Turner
| producer       = William Selig John B. Shaw, Jr L. Frank Baum
| writer         = L. Frank Baum Frank Burns George E. Wilson Joseph Schrode Burns Wantling Grace Elder
| music          = Nathaniel D. Mann
| cinematography =
| editing        =
| studio         = The Radio Play Company of America
| distributor    = Selig Polyscope Company
| released       =  
| runtime        = 120 minutes (3600 m)
| country        = United States
| awards         =
| language       = English
| budget         =
}}
 live actors, hand-tinted magic lantern slides, and film. Baum himself would appear as if he were giving a lecture, while he interacted with the characters (both on stage and on screen).  Due to financial problems—the show cost more to make than sold-out houses could bring in—the show folded after two months of performances.  It opened in Grand Rapids, Michigan on September 24, 1908.  It later moved to New York City, where it reportedly closed December 16, 1908.  It was scheduled to run through December 31, and ads for it continued to run in The New York Times until then.

==Michael Radio Color==
The films were colored (credited as "illuminations") by Duval Frères of Paris, in a process known as "Radio-Play", and were noted for being the most lifelike hand-tinted imagery of the time.  Baum once claimed in an interview that a "Michael Radio" was a Frenchman who colored the films, though no evidence of such a person, even with the more proper French spelling "Michel", as second-hand reports unsurprisingly revise it,  has been documented.  It did not refer to the contemporary concept of radio (or, for that matter, a radio play), but played on notions of the new and fantastic at the time, similar to the way "high-tech" or sometimes "cyber" would be used later in the century.  The "Fairylogue" part of the title was to liken it to a Travel literature|travelogue, which at the time was a very popular type of documentary film entertainment.

==Original film score== score consisting The Wizard of Oz musical. It debuted four months before Camille Saint-Saënss score for The Assassination of the Duke of Guise, and is therefore the earliest original film score to be documented.

==Adaptation== William Gillespie Ozma was musical three years prior.

==Cast==
 
(listed in the order credited in the program)
* L. Frank Baum:  The Wizard of Oz Man, who will present his very merry, whimsical and really wonderful Fairylogue and Radio-Plays
*  
*  
*  , the Machine Man
*  , whose Brains are Seeds
*  , a Master of Enchantments
* Will Morrison: Tip, a transformation, but a real boy
*  
*  
*  
*   The Yellow Hen: Herself
* Toto (dog)|Toto, Dorothys Dog: Himself
*  , Visitor at the Emerald City
*   , Visitor at the Emerald City
*  , Visitor at the Emerald City
*  , Visitor at the Emerald City
*  , Visitor at the Emerald City
*   , Visitor at the Emerald City
*   of Kansas
*   of Oz
*  , a Sorceress
*   the Witch Gingerbread Man
* Geo. Weatherbee: Mons. Grogande, the Baker who made him The Rubber Bear, a Good Natured Thing
* George E. Wilson: The White Rabbit, Diffident, but not Shy
*   who likes Gingerbread
* Lillian Swartz: Hogo, Mifket who likes Gingerbread
* Minnie Brown: Joko, Mifket who likes Gingerbread
* Daniel Heath: Tertius, an Islander
* Tom Persons: Hopkins, of the Village Fireworks Committee
*  , an Incubator Baby
* Annabel Jephson: The Island Princess
* Mrs. Bostwick: Mme. Grogand, the Bakers Wife  

==Production==
The New York Times included a write-up of the show in a full-page article in a late 1909 issue, over a year after the show had come and gone, probably because they finally had space for it after it was no longer necessary but still of interest.  When the production appeared in New York, the Times listing for it appeared along with the plays, not with the films, drawing attention to the fact that Baum, not to mention the rest of the cast, would be appearing live on stage with the films as a major, though far from the only, component.

The Fairylogue and Radio-Plays was produced by "The Radio-Play Company of America", John B. Shaw, Jr., general manager.  The sets were designed and painted by E. Pollack.  The costumes were designed by Fritz Schultz and Chicago Costuming Co..  Properties and papier-mâché work by Charles van Duzen.  Mechanical effects by Paul Dupont.  Wigs by Hepner.  Shoes by Marshall Field & Co.  Jewels loaned by C.D. Peacock.
 The Wonderful Wizard of Oz and its sequels were derived from the materials of this film, which was disproven with the discovery of that film, which bears little resemblance to the surviving materials of Fairylogue.  Otis Turner is believed to be the director of both film versions of John Dough and the Cherub, both Lost films|lost.  It may be possible that they were one and the same film, but highly unlikely, as Fairylogue was most likely the singular print eventually discarded by the Baum family after its Vinegar syndrome|decomposition.

==See also==
* List of lost films

== References ==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 