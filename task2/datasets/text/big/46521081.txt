Sunken Rocks
{{Infobox film
| name           = Sunken Rocks
| image          =
| caption        =
| director       = Cecil M. Hepworth
| producer       = Cecil M. Hepworth
| writer         = E. Temple Thurston 
| starring       = Alma Taylor   Gerald Ames  James Carew
| cinematography = 
| editing        = 
| studio         = Hepworth Pictures
| distributor    = Butchers Film Service
| released       = August 1919
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}
Sunken Rocks is a 1919 British silent drama film directed by Cecil M. Hepworth and starring Alma Taylor, Gerald Ames and James Carew. 

==Cast==
* Alma Taylor as Evelyn Farrar 
* Gerald Ames as Dr. Purnell 
* James Carew as J.H. Farrar 
* Nigel Playfair as Mr. Gurney 
* John MacAndrews as Tramp 
* Minnie Rayner as Cook  

==References==
 

==Bibliography==
* Palmer, Scott. British Film Actors Credits, 1895-1987. McFarland, 1988. 

==External links==
* 

 
 
 
 
 
 
 

 