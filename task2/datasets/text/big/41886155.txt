Journey to Self
{{Infobox film
| name           = Journey to Self
| image          = Journey_to_Self.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Tope Oshin Ogun
| producer       = Ashionye Michelle Raccah
| starring       =  
| music          = 
| cinematography = Kunle Adejuyigbe Jebo Bature 
| editing        = Kunle Adejuyigbe
| writer         = Ashionye Michelle-Raccah
| studio         = AlleyKat Media
| distributor    = Silverbird Distribution
| released       =  
| runtime        = 
| language       = English
| country        = Nigeria
| budget         =
| gross          =
}}
Journey to Self is a 2012 Nigerian drama film written and produced by Ashionye Michelle Raccah and directed by Tope Oshin-Ogun, starring Nse Ikpe Etim, Ashionye Michelle Raccah, Dakore Akande, Tosin Sido and Katherine Obiang. It received a nomination for the category Achievement In Soundtrack at the 9th Africa Movie Academy Awards.   
The film tells a story of 5 childhood friends who converge at the home of one of them who just died, she leaves a series of letters that explains her struggles through life and eventually leading to their "self discovery".

==Plot==
The film tells a story of 5 childhood friends that converge at the home of the deceased Uche reading a series of letters she left that explains her struggles through life and eventually leading to their self discovery.

Rume (Katherine Obiang) is married to a man, whom she later discovered was a "crossdresser", she filed for divorce after several attempt to make him normal proved abortive. however he pleaded with her to stage infidelity as the reason for the divorce to avoid disgrace and societal outrage from family and friends for his eccentric lifestyle. 

Uche (Tosin Sido) was married off by her dad as the 4th wife to a former business colleague as a remedy for an unpaid debt. As soon as it was revealed that she was barren, she began suffering an emotional breakdown caused by her much older husband and his jealous wives. 

Regina (Ashionye Michelle Raccah) is a full housewife that is being assaulted by her husband, Their 2 kids were also affected by the domestic violence.

Nse (Nse Ikpe Etim) had a son when she was younger and her husband is yet to know about it.  

Alex (Dakore Akande) is an entertainment celebrity that has been very unlucky with men taking advantage of her tenderheartedness.

==Cast==
*Nse Ikpe Etim as Nse
*Ashionye Michelle Raccah as Regina
*Dakore Akande as Alex
*Katherine Obiang as Rume
*Tosin Sido as Uche
*Chris Attoh as Dapo
*Femi Brainard as David
*Femi Jacobs as Uzo
*Adeyemi Okanlawon as Benjamin
*Kalu Ikeagwu as Joe

==Reception==
Journey to Self received mixed to positive reviews from critics with Nollywood Reinvented giving it a 54% rating. The reviewer described the film as not fitting into the typical Nollywood formula. "Just because it’s not what we’re used to doesn’t make it a bad movie. It had a point to convey, it sent that point home and it ended. Simple and short."  Sodas and Popcorn gave it 4 out of 5 rating and states "It has a strong message about self-discovery, being independent, standing up for yourself. The best part is the fact that the movie succeeds in communicating all these lessons effectively."   "I had an eighteen month old baby to raise and a husband who doesnt know whether he wants to be a mummy or a daddy" has been noted as being a memorable line from the movie. 

==Awards==
{| class="wikitable"
|-
! Award !! Category !! Recipient !! Result
|- Africa Movie Academy Awards
| Achievement in Soundtrack
| Tee Y Mix
|  
|- Nollywood Movies Awards
| Best Movie
| Ashionye Michelle Raccah
|  
|-
| Best Director
| Tope Oshin Ogun
|  
|-
| Best Editing
| Kunle Adejuyigbe
|  
|-
| Best Sound Design
| Carl Raccah
|  
|-
| Best Costume Design
| Yolanda Okereke
|  
|-
|-
| Best Makeup
| Toby Ejiro Jeje- Philip & Korede Olowoyo
|  
|- Best of Nollywood Awards
| Best Actress in a supporting role
| Katherine Obiang
|  
|-
| Best Child Actor (male)
| Dozie Onyiriuka
|  
|-
|-
| Movie with the Best Social Message
| Ashionye Michelle Raccah
|  
|- Africa Magic Viewers Choice Awards
| Best Sound Editing
| Carl Raccah
|  
|-
| Best Actress in a Drama
| Nse Ikpe Etim
|  
|-
| rowspan=3 | 2014 Nigeria Entertainment Award 
| Best Movie
| Ashionye Michelle Raccah
|  
|-
| Best Director
| Tope Oshin- Ogun
|  
|-
| Best Actress
| Nse Ikpe Etim
|  
|}

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 