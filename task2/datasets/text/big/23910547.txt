Streets of New York (1939 film)
{{Infobox film
| name           = Streets of New York
| image          = File:Streets of New York poster.jpg
| image_size     =
| caption        = Film poster
| director       = William Nigh
| producer       = Scott R. Dunlap (producer) William T. Lackey (associate producer)
| writer         = Robert Hardy Andrews
| narrator       =
| starring       = See below
| music          = Edward J. Kay
| cinematography = Harry Neumann
| editing        = Russell F. Schoengarth
| distributor    =
| released       = 12 April 1939
| runtime        = 73 minutes
| country        = United States
| language       = English
| studio         = Monogram Pictures
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Streets of New York is a 1939 American film directed by William Nigh.

The film is also known as The Abe Lincoln of Ninth Avenue, and The Abraham Lincoln of the 4th Avenue.

== Plot summary ==
 , Jackie Cooper, and Marjorie Reynolds, in The Abe Lincoln of the 4th Avenue]]
"Jimmy" (Jackie Cooper) and crippled "Gimpy" (Martin Spellman) run the corner newsstand.  Spike (David Durand), a neighbourhood Juvenile delinquency|delinquent, doesn’t like it, on his turf, and does everything he can to get them into trouble, and disrupt their circulation.
 George Irving) tries to help them out. He doesn’t want Jimmy going bad, like his big brother, the racqueteer, Tap (Dick Purcell), while Jimmy is trying to go to school and teach himself to be a lawyer, like his hero, Abraham Lincoln.

Jimmy has to choose between the right thing, and family, when his brother comes to him for help.

== Cast ==
*Jackie Cooper as James Michael Jimmy Keenan
*Martin Spellman as William McKinley Gimpy Smith
*Marjorie Reynolds as Anne Carroll
*Dick Purcell as T.P. Tap Keenan
*George Cleveland as Pop OToole George Irving as Judge Carroll
*Robert Emmett OConnor as Police Officer Burke Sidney Miller as Jiggsy, newsboy
*David Durand as Spike Morgan
*Buddy Pepper as Flatfoot, newsboy

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 


 