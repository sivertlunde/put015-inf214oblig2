Oh, What a Night (1944 film)
{{Infobox film
| name            = Oh, What a Night
| director        = William Beaudine
| writers         = Marian Orth & Paul Girard Smith
| starring        = Edmund Lowe Jean Parker
| distributor     = Monogram Pictures
| released        =  
| runtime         = 72 mins
| language        = English
}}

Oh, What A Night is a 1944 crime drama starring Edmund Lowe & Jean Parker and directed by William Beaudine.

The films story was written by Marian Orth and the screenplay by Paul Girard Smith.

==Plot==
A man is keeping a secret from his young niece: hes an international jewel thief!

==Cast==
* Edmund Lowe as Rand
* Jean Parker as Valerie
* Marjorie Rambeau as Lil Vanderhoven
* Alan Dinehart as Detective Norris
* Pierre Watkin as Tom Gordon
* Ivan Lebedeff as Boris
* Claire Du Brey as Petrie
* Charles F. Miller as Sutton
* Olaf Hytten as Wyndy
* Karin Lang as Sonya
* George J. Lewis as Rocco
* Crane Whitley as Sullivan
* Charles Jordan as Murphy
* Dick Rush as Healy

==External links==
* http://www.imdb.com/title/tt0037145/

 

 
 
 
 
 
 
 


 