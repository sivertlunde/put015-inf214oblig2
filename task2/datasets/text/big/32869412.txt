The Last Summer
 
 
{{Infobox film
| name           = The Last Summer
| image          = 
| caption        = 
| director       = Christo Christov
| producer       = 
| writer         = Yordan Radichkov
| starring       = Grigor Vachkov
| music          = 
| cinematography = Tzvetan Chobanski
| editing        = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = Bulgaria
| language       = Bulgarian
| budget         = 
}}
 Best Foreign Language Film at the 47th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Grigor Vachkov as Ivan Efreytorov
* Dimitar Ikonomov as Dinko
* Bogdan Spasov as Dyadoto
* Vesko Zehirev as Vuychoto
* Lili Metodieva as Maykata
* Daniela Danailova as Karakachankata
* Lyuben Boyadzhiev as Generalat
* Yuli Toshev as Majorat
* Peter Goranov as Partizaninat Dimitar Milanov as Dyavolat

==See also==
* List of submissions to the 47th Academy Awards for Best Foreign Language Film
* List of Bulgarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 