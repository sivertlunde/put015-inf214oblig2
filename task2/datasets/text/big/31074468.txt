Yo... el aventurero
{{Infobox film
| name           = Yo... el aventurero
| image          = Yoelaventurero.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Jaime Salvador
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       =  Antonio Aguilar Rosa de Castilla Ángel Infante Amalia Mendoza Andrés Soler Domingo Soler
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          =
}}
 Antonio Aguilar, Rosa de Castilla, Ángel Infante, Amalia Mendoza, Andrés Soler, and Domingo Soler. The film was shot in Mexiscope, a lens similar to CinemaScope, and it one of the rare color films of the late 1950s.

==Cast==
*Antonio Aguilar as Antonio Ardabín
*Rosa de Castilla as Gloria Cisneros
*Ángel Infante as Gregorio Carriles
*Amalia Mendoza as Amalia 
*Andrés Soler as Guadalupe Cisneros
*Domingo Soler as Manuel Ardabín
*Agustín Isunza as Moncho		
*Joaquín García Vargas as Nacho		
*Armando Soto La Marina as Nicho		
*Paco Michel as Lencho		
*Dolores Tinoco as Petra Eduardo Pérez as Pascual		
*Roberto Meyer as Comandante
*José Luis Fernández (actor)|José Luis Fernández as José Luis	
*Guillermo Calles as Amalias confidant

==External links==
* 

 

 

 