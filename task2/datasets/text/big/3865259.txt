Just Follow Law
{{Infobox film
| name           = Just Follow Law
| image          = JustFollowLawPoster.jpg
| director       = Jack Neo
| producer       = 
| writer         = 
| starring       = Fann Wong Gurmit Singh Moses Lim
| music          = Ocean Butterflies Music Pte Ltd
| cinematography = 
| editing        = 
| studio         = J Team
| distributor    = Golden Village Pictures
| released       = 15 February 2007
| runtime        = 
| country        = Singapore
| language       = Mandarin English Malay Hokkien Tamil
| budget         = Singapore dollar|$ 1.05 million
| preceded_by    = 
| followed_by    = 
}} comedy film directed by Jack Neo, and starring Gurmit Singh of Phua Chu Kang fame, Fann Wong, Moses Lim, Samuel Chong, Lina Ng, David Bala and Steven Woon.

In the film, a blue-collar technician and the events and promotion department director swap souls after a freak accident at a fictional government agency Work Allocation Singapore (WAS). 
It was first released in Singapore on 15 February 2007    and Malaysia and Brunei on 22 March 2007.

== Plot ==

Lim Teng Zui is a WAS technician who is a single father to a young girl named Xiao Mei. He is employed by the director of WAS Events and Promotion department, the prideful Tanya Chew. Tanya stays in a condominium and has a single mother, a blogger whom she has neglected. Lim has drawn little salary, and strives hard to earn more so that he can buy Xiao Mei a piano and original abalone, whereas Tanya constantly tries to improve the terrible image of her department. Both workers are under employment of Alan Lui, the CEO of WAS.

One day, Alan briefs Tanya, Lau Chee Hong, Eric Tan and Uncle Tiong about Minister Seetoh accompanying Chinas Minister of Manpower, Mr. Chen and his delegates will be visiting the department. As such they are to portray the company in the best light as possible. All departments are briefed, but Tanyas arrogance leaves her crew unmotivated to prepare for the day.
 VIP visit, they decide to put the junk outside temporarily. Knowing it still will remain an eyesore, Alan tells Tanya to conceal it some way, who in turn gets her crew to do so. Eventually Lim is left to handle the matter on his own. He manages to build a temporary wall, yet company incompetence in obtaing supplies leaves the wall completed half-heartedly with masking tapes used instead of nails.
 1998 Seat Toledo in his Citroën Berlingo until the vehicles collide, causing both to run off the flyover and fall down to the beach.
 idles around, performing poorly in his/her capacity as a department director.

Due to "Tanya"s negligence, the department grossly overspends and the CEO, together with Chee and Eric, two department heads who had been offended by Tanya earlier, plans to shut down the department. In an attempt to save it from closure, Tanya and Lim plan to put on a successful Job Fair Exhibition so that the CEO would think twice about shutting it down. Tanya and Lim try ways of getting publicity, but red tape and lack of information gets in their way and they would have to wait at least three months to be able to do anything. However, it proceeds smoothly, and Tanya hires a pyrotechnics operator, who is actually a demolition expert. After a series of trials and tribulations, the Job Fair is realized.
 Civil Defense firefighters arrive on the scene to put out the fires, Tanya rescues Lims daughter. Both Lim and Tanya realise the neglect they left on their loved ones when Tanyas mother and Xiao Mei embrace their original bodies.

A board of inquiry looks into the actions during the fire. Later during the 2007 National Creativity Award ceremony, Lim was awarded for his use of masking tapes to save 53 lives and his initiative to invent a fire escape wall. 

Two months later, the statutory board has folded as WAS shuts down with several outcomes on its key members; Uncle Tiong retires because of his heart attack, Eric is sacked and has his pension (CPF) confiscated when their plot is discovered, Chee becomes the first director in history to receive pink slip for skills obsolete and CEO Alan is blacklisted for making too many mistakes, but later establishes the Minced Pork Organisation, only to be dissolved months later.

Lim and Tanya, having going in their separate ways other than working, decide to re-enact the accident in an attempt to switch back to their original bodies. They start their same vehicles and drive off at the same time and place, doing the same collision. As the credits roll, Lim and Tanyas attempts are revealed to be unsuccessful, but they still remain together and get married. Lim and Xiao Mei move in with Tanya, and all now live a happier life.

== Production ==
=== Development and writing ===

The development of Just Follow Law began when Jack Neo pitched the idea of Fann Wong during one of their backstage meetings, though they cant agree regarding the location of the agreement.  It was inspired by then Prime Minister Lee Hsien Loongs speech at the 2006 National Day about the lack of professionalism among Singaporean workers. Most of the workers completes work on time during evenings and their mentality of staying beyond that time would be like doing as a favour. He hopes to use the movie to highlight the bureaucracy inefficiencies in office. 

=== Cast ===

*Fann Wong was cast as Tanya Chew, the WAS E&amp;P director who stays at a condominium and has a mother she cares little for, and as Lim Teng Zui, the blue-collar worker who is a single father of a daughter and as a technician, draws a small salary. 
*Gurmit Singh was cast as Lim Teng Zui, who "swapped bodies" with Tanya Chew later in the film.
*Suhaimi Yusof as Bamboo, Lims colleague and friend Brandon Wong as Blackjack, Lims colleague and friend
*Moses Lim as Eric Tan, WASs penny-pinching director.
*David Bala as Muthu,security guard of WAS.
*Samuel Chong as Alan Lui.
*Steven Woon as Lau Chee Hong.

=== Filming ===
This film began shooting in high-definition video format starting in 1 February 2006 and ending in March 2006.

== Release ==
It had a strong opening during Chinese New Year, earning $421,000 from 35 prints for second place in the chart. 
*Singapore 15 February 2007
*Malaysia 22 March 2007
*Brunei 22 March 2007
*Hong Kong 10 May 2007
*Indonesia  21 August 2007

== Reception ==
At the Golden Horse Awards 2007, Just Follow Law was nominated for Best Original Screenplay and Best Visual Effects, while Gurmit Singh was one of four considered for Best Actor.  He did not win.

== Movie connections ==

Just Follow Law contains many references and homages to films. The soul switching between the leading characters references to that from Freaky Friday and Vice Versa.

During certain sequences, some of the sound effects come from various sources, notably including the 2004 video game Half-Life 2. For example, the Minister falls down through a poorly erected wall during his visit of WAS; Lim Teng Zuis Citroën Berlingo crashes on the ground. Later on, the Ssangyong Istana doors open to reveal the bikini dancers and hunks; the Singapore Civil Defence Forces Dennis Sabre fire engine stops over a VIP lot.

== See also ==
* No U-turn syndrome

== References ==
 

== External links ==
*  
*  
*  
 
 

 
 
 
 
 