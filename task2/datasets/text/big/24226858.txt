Little Big Shot
{{Infobox film
| name           = Little Big Shot
| image          =File:Little Big Shot lobby card.JPG
| image_size     =
| caption        =Lobby card
| director       = Michael Curtiz
| producer       = Samuel Bischoff
| writer         =  
| narrator       = Robert Armstrong Edward Everett Horton
| music          =
| cinematography = Tony Gaudio
| editing        = Jack Killifer
| distributor    = Warner Bros. Pictures
| released       = 7 September 1935 (USA)
| runtime        = 78 minutes (Turner Libaray Print is 72 minutes)
| country        = United States
| language       = English
}}

:For the 1952 British film, see Little Big Shot (1952 film).
 1935 American film directed by Michael Curtiz from Warner Brothers Pictures. 

== Cast == 

* Sybil Jason as Gloria Countess Gibbs
* Glenda Farrell as Jean Robert Armstrong as Steve Craig
* Edward Everett Horton as Mortimer Thompson
* Jack La Rue as Jack Doré 
* Arthur Vinton as Norton Nort Kell
* J. Carrol Naish as Bert
* Edgar Kennedy as Onderdonk
* Addison Richards as Hank Gibbs
* Joe Sawyer as Dorés henchman 
* Emma Dunn as Orphanage matron
* Ward Bond as Kells henchman
* Tammany Young as Ralph Lewis (the rajah)
* Murray Alper as Dorés henchman
* Marc Lawrence as Dorés henchman
* Guy Usher as Lt. Adams
* Mary Foy as Orphanage matron #2
* unbilled players include Howard C. Hickman and Milton Kibbee

== Plot summary ==
 Broadway one step ahead of the police selling phony watches. Broke, they arrange to have dinner with Gibbs, an old friend, thinking he will help them with some money, but Gibbs and his daughter Gloria (Sybil Jason) dont have that much money too and think that Steve and Mortimer will help them with money too. At the time Gibbs is hiding from gangsters. After Steve, Mortimer, Gibbs and Gloria are finished with their dinner in the restaurant Gibbs gets shot outside by the gangsters who was chasing him. Steve and Mortimer hurry to leave the restaurant before they get hurt, but by mistake leave Gibbs daughter Gloria, Steve reminds Mortimer that they forgot about Gloria, but Mortimer doesnt care to go back and get Gloria, but Steve convinces him to. Gloria stays with Steve and Mortimer for a night at their place and Gloria takes Mortimers bed, so Mortimer has to sleep in the toilet. Later on they try to put Gloria in an orphanage, but Steve felt bad for Gloria because she was crying not to go there so he didnt give her to the orphanage. So Steve and Mortimer try to take care of Gloria with the help of Jean (Glenda Farrell), a hat check girl at their hotel they live in. Steve and Mortimer find out that Gloria could sing and dance so they try to make money off of her talents on the street, Gloria also helps them sell their fake watches too, Gloria finds out and she doesnt like the idea, but Steve and Mortimer still do it. Steve goes into a crap game with a gangster, but the gangster refuses to pay Steve the money he gambled, Steve threatens to kill the gangster and later on the gangster is actually found dead and Steve is the suspect for a crime he didnt do. Jean puts Gloria in an orphanage because she thinks that Steve and Mortimer aint responsible enough to take care of Gloria. Another gang kidnaps Gloria to force Steve out of hiding, Steve later on convinces the gangsters to release Gloria, and just about as they are trying to kill him, the police, tipped of by Mortimer and Jean, arrive at the hideout. His name cleared, Steve marries Jean and adopt Gloria, and also open a roadside cafe with Mortimer.   

== Other information ==
Michael Curtiz filmed some scenes of this movie at real orphanages in Hollywood and used real orphans in this movie. Sybil Jason stated that a young Marilyn Monroe was in this movie but no film historian or Marilyn Monroe biographer has confirmed that yet.

==External links==
* 
* 

 

 
 
 
 
 
 