Dragon Ball: Yo! Son Goku and His Friends Return!!
{{Infobox film
| name           = Dragon Ball: Yo! Son Goku and His Friends Return !! 
| image          = 2008 Dragon Ball Short.PNG
| caption        = Japanese poster
| director       = Yoshihiro Ueda
| producer       = 
| screenplay     = Takao Koyama
| story          = Akira Toriyama
| based on       =  
| narrator       = Jōji Yanami
| starring       = See #Cast|Cast
| music          = Shunsuke Kikuchi
| cinematography = Akio Ōnuki
| editing        = Shinichi Fukumitsu
| studio         = Toei Animation
| distributor    = Shueisha
| released       =  
| runtime        = 31 minutes
| country        = Japan
| language       = Japanese
| budget         =
}}
  is a 35-minute  .

== Plot ==
Two years after the defeat of Majin Boo, List of Dragon Ball characters#Mr. Satan|Mr. Satan finally finishes construction on his brand new hotel. Feeling guilty that the hotel is in honor of what the majority of Earth believes to be his defeat of Boo, he invites all of the characters and their families to a banquet. Goku and Vegeta decide to spar, as they have dressed in their usual fighting outfits. Shortly after dinner, two Saiyan spaceships land just outside of town, and a mysterious Saiyan and his wife confront the heroes at the hotel. They are revealed to be Vegetas younger brother, Tarble, who had been deemed too weak to fight in the past and was sent to a distant star before the destruction of Planet Vegeta, and his wife Gure, who is a small, cartoon-like alien. Tarble asks his brother for help in dealing with Abo and Kado, soldiers who are remnants of Freezas army, who are causing havoc on his planet. Goku and Vegeta argue over who will fight the brothers. Even their sons and friends offer to join in, but so many fighting two people seems unfair, so Goku comes up with an idea to solve the issue.
 Goten picks Trunks to pick one that is longer. Pressured by his father, Trunks picks out what appears to be a short radish, but it turns out to be the longest, stretching all the way down to the mountain. This means that Trunks wins and is chosen to fight, and the battle begins. However, Abo and Kado are two opponents and Goku asks Vegeta to let Goten fight with Trunks, but Vegeta says that Trunks is enough. Nevertheless, Goku lets Goten join Trunks, which prompts an angry outburst from Vegeta, who relents after his wife persuades him to agree. When the battle begins, without going Super Saiyan, the children easily defeat the two soldiers. However, when Abo and Kado create doubles of themselves, the boys get beaten easily. Irritated, Vegeta shouts at Trunks to read their ki and says that Trunks has forgotten the basics. When his wife says it is because he never taught Trunks basics, Vegeta says he lacks motivation like her, starting an argument between them which prompts Goku to say that its not the time for a "marital quarrel". With a little help from Gotens brother Gohan, he and Trunks are able to fight and seem to have the upper hand. Everyone except for Goku, Vegeta, and Piccolo go back to the hotel. As the battle is almost over, Abo and Kado fuse into their larger, more powerful form, Aka.

Aka easily devastates Goten and Trunks, slamming them around and tossing them like rag dolls, and everyone comes back. Goten and Trunks try to fuse; while the first is a failure, the second is successful, and they become Gotenks. Gotenks attacks and appear to have defeated Aka. However, while everyone is celebrating, Aka attacks the boys again with a super move after Gotenks taunts him, but Piccolo blocks the attack. Infuriated, Aka destroys most of the hotel while Gotenks protects Tarble and Gure. Seeing Akas threat growing, Goku and Vegeta join the fight, but Goku tricks Vegeta into looking away while he defeats Aka with a final Kamehameha. With the battle over, Mr. Satan mourns the loss of his hotel. His daughter Videl tells him that this serves as a lesson, and he promises to change. Everyone goes back to whats left of the hotel, where their banquet is still waiting. Abo and Kado give up the fight and join them in the feast. The special ends with Goku and Vegeta fighting over pork and sushi, even going Super Saiyan as they argue, much to the amusement of their friends but the embarrassment of their wives.

== Characters ==
      
This special introduces four new characters:   and  , red and blue aliens wearing battle fatigues who later merge into a large, purple alien named   (Goten and Trunks jokingly call him "Abokado", a reference to avocado), a Saiyan named  , Vegetas younger brother who had previously been sent away from planet Vegeta by King Vegeta, and his wife  , a small pink and white robot-like alien from the planet that Tarble was sent to.

== Cast == Goten
* Ryō Horikawa as Vegeta Yamcha
* Mayumi Tanaka as Kuririn Piccolo
* Hiromi Tsuru as Bulma Oolong
* Naoko Watanabe as List of Dragon Ball characters#Chi-Chi|Chi-Chi, List of Dragon Ball characters#Pu.27ar|Puar Trunks
* Hiroshi Masuoka as List of Dragon Ball characters#Kame-Sen.27nin|Kame-Sennin Videl
* Daisuke Gōri as List of Dragon Ball characters#Mr. Satan|Mr. Satan Android 18
* Masakazu Morita as Tarble  
* Kumiko Nishihara as Gure  
* Yūsuke Numata as Abo  
* Kazunari Tanaka as Kado  
* Yasunori Masutani as Aka  
* Jōji Yanami as the narrator

== Development ==
The film was first announced in the April 21st issue of   in the Jump Super Anime Tour and visit ten Japanese cities to celebrate Shōnen Jumps 40th anniversary.  In the June 9th issue of Weekly Shōnen Jump it was announced that the story would be based on an original concept by Dragon Ball creator Akira Toriyama, revisiting the comedic story-telling seen early in the series.  The following August issue of V Jump revealed a small hint of the plot and that at least three new characters would appear. 

== Distribution ==
The site Jumpland began streaming the film to both Japanese and English speaking viewers on their website on November 24, 2008 the Monday following the final stop on the Super Tour and was discontinued on January 31, 2009.  Shortly before ending, it was announced that the film would be made available along with One Piece: Romance Dawn Story and   on a triple feature DVD that would only be available though a mail-in offer exclusive to Japanese residents.  The special was included as a bonus feature on the more widely available, limited edition home release of the 2013 film   on September 13, 2013.

In the February issue of V Jump it was announced that a manga adaptation illustrated by Naho Ōishi was in the works; it was published in the magazines March 21 and April 21, 2009 issues.  It was collected into a single tankōbon volume on September 3, 2010. 

== See also ==
 
*List of Dragon Ball films
 

== References ==
 

== External links ==
 
*    
*    
*  

 

 
 
 
 
 
 
 