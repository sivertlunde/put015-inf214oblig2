Hrabina Cosel
{{Infobox Film
| name           = Hrabina Cosel (Countess Cosel)
| image          = HrabinaCosel1968.jpg
| caption        = Promotional movie poster for the film
| director       = Jerzy Antczak
| producer       =
| writer         = Jozef Ignacy Kraszewski Zdzislaw Skowronski
| starring       = Jadwiga Barańska Mariusz Dmochowski Daniel Olbrychski
| music          = Waldemar Kazanecki
| cinematography = Boguslaw Lambach Jan Janczewski
| editing        = Janina Niedzwiecka
| distributor    = Zespol Filmowy Iluzjon
| released       = 9/6/1968 Poland
| runtime        = 139 min
| country        = Poland Polish
| budget         =
| preceded_by    =
| followed_by    =
| amg        =
| imdb        = 0063104
}}
 1968 Poland|Polish Augustus the Strong, the first of the two Saxon Kings of Poland, at the turn of the eighteenth century. 

==Plot== Augustus the Strong, King of Poland and Elector of Saxony in 1704.

  Against the backdrop of life at Augustus’ court the movie follows the arrival at Dresden of 24 year old Anna Constantia of Brockdorff, who is an inordinately beautiful and scrupulously devout young married woman; her romance with the King and her ‘ten year reign’ as his mistress – a role she consents to in the belief that she has won the king’s love and commitment; and finally, her heartbreak, disillusion and struggle against him. The conflict between Anna and the King arises from the fact that Anna takes seriously not only the relationship, but the written promise of marriage which she manages to secure from him during their courtship. Neither the relationship nor the contract is held in much esteem by Augustus, who replaces her, as he has always done, with another mistress, as soon as she loses for him the charm of novelty. Her valiant refusal to return the marriage promissory note upon request enrages him and delivers her to her miserable fate.

In the opinion of the court, Cosel came to be considered increasingly dangerous when it became known that king Augustus had given her a secret written promise of marriage. Ultimately, she fell from grace with the king when she showed her jealousy after her spies told her about the kings affair with Countess Maria Magalena von Dönhoff of Warsaw. Distraught and feeling desperate to win back kings affection, Cosel departed Dresden to meet the king in Warsaw only to be turned back at the city gates. Augustus, having no more feelings for his former mistress, gave orders to lock her up in the Stolpen fortress. Cosels faith and courage, which replaced her initial despair and anger culminated in her decision to remain in her jail, even after Augustus’ death, thirty-two years before her own, opened the way for her to regain her freedom. As a result, Cosel was imprisoned for forty-nine years, before she  died in 1765, being eighty-five years of age. To the end of her life she preserved traces of her great beauty, by which she became so famous.

==Cast==

* Jadwiga Barańska - Anna Konstancja Cosel, Anna Constantia of Brockdorff
* Mariusz Dmochowski - August II Mocny, Augustus II the Strong
* Stanisław Jasiukiewicz - Rajmund Zaklika, Polish soldier in love with Cosel who tried so rescue her form her imprisonment
* Daniel Olbrychski - King Charles XII of Sweden
* Ignacy Gogolewski - baron Fryderyk Kyan
* Stanisław Milski - hrabia Egon Furstenberg, marszałek dworu
* Henryk Borowski - hrabia Adolf Hoym, mąż hrabiny Cosel
* Władysław Hańcza - general Schulenburg
* Krystyna Chmielewska - Marianna Dönhoff, mistress of the king Augustus II the Strong
* Mieczysław Kalenik - La Haye, porucznik gwardii królewskiej
* Maria Homerska - Krystyna Eberhardyna, kings wife
* Władysław Dewoyno - Holder, szlachcic saski
* Bronisław Pawlik - alchemik Bettger
==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 