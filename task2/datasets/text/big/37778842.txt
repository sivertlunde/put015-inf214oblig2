Purgatory Comics
{{Infobox film name = Purgatory Comics image = Purgatory Comics film theatrical poster USA 2009.jpg director = Ralph Suarez writer = Ralph Suarez starring = Bree Olson Angela Benedict Jonathan Zungre Tim Kelly cinematography = Michael LaVoie editing = Ralph Suarez John Kilgour studio = Warm Milk Productions distributor = GoDigital released =   runtime = 90 minutes country = United States language = English
|budget = $23,000
}}
 American Independent independent comedy film written and directed by Ralph Suarez. It stars Angela Benedict, Jonathan Zungre, and Tim Kelly, and is notable for being the first comedy role for adult film star Bree Olson. It was shot in 2007 and picked up by GoDigital for worldwide digital distribution in 2011.

==Plot==
Cola runs from her adult responsibilities and instead spends her time and talent helping the Purgatory Comics store stay in business. She befriends Evan, the store owner, and Berner, an offbeat customer and comic book fan. The three launch a local marketing effort for an upcoming sale in hopes of drumming up business for the small comic book store.

As Berner hangs flyers at a park, he watches Mia preparing to leave after a racquetball game. She spots him and initiates an awkward conversation that reveals they have known each other for a long time, and while Mia has started college and does volunteer work on the side, Berner has become a slacker after high school. They are joined by Andrew, Mias militant cousin who pulls her away to get to a class they share. As she leaves, Berner gives them a flyer and invites them to visit the store, to which Mia replies "well see."

At the shop, Cola tells Evan she has contacted Peter Whales, a legendary comic book writer who has become a local recluse. Its then revealed via flashback that when Cola was still in college, shed started a relationship with a man named Danny. In the present, her mind still wanders and she thinks of him. Cola and Evan then argue about why comic book stores dont get the business they once had, and the general depiction of women in comics.

Later, Cola finds Berner outside waiting in case Mia arrives. She then sarcastically suggests Berner "clean his pipes" to help him relax. Inside, Evan plays a card game similar to   with his friend X-Ray. As Cola leaves to prepare for her meeting with Peter Whales, Berner grabs an issue of a comic called Zombie Gal and heads to the basement. There he has a fantasy of the title character coming to life and seducing him while a zombie awkwardly watches. Cola returns in a sexy red dress at the same time Berner returns from the basement. She grabs the comic from his hand and throws it on the counter as she pulls him to go with her to meet Peter. They find Peter Whales in a small hotel room but before they can ask about the signing, he tears into them and begins deconstructing Colas life. He accuses her of running from something. This causes Cola to think of how she ran from Danny, which prompts her to call him and ask to meet up.

After X-Ray leaves the comic store, an irritated customer tries to return a comic book while accusing Evan of masturbating into it. After Cola and Berner return, Berner admits that it was the Zombie Gal comic hed used earlier. Evan threatens to permanently ban Berner from the store forever. Evan then goes into his own fantasy in which he returns home to Bree Olson, who offers to sexual roleplay with him and make him feel better. Berner snaps Evan back into reality just as Mia and Andrew enter the store. Mia spends some time with Berner but shoots down his offer for a date saying her responsibilities keep her too busy. Meanwhile, Evan educates Andrew on the artistic merit of comic books.

At the park where they first met, Cola meets up with Danny to talk. The conversation frustrates Danny since Cola was the one who ran away from him after their two years together. They leave peacefully, but Danny asks her to never contact him again unless shes positive she wants to be with him. When Cola gets back to the comic shop, she begins educating Berner on marketing and synergy. Evan discovers that Cola had used his laptop to look for new jobs. Scared that they might lose her, Evan and Berner pressure her to stay with them and avoid returning to her previous lifestyle. Their talk turns aggressive and Evan tells her to leave the store. On her way out, she finds Berner sitting sadly on her car. She reveals to him that her whole life, shes used comic books to escape reality whenever things got too difficult. She also explains how she had quit her marketing job and punched her boss, and then later had an argument with Danny that led to their breakup.

Cola returns to the hotel room of Peter Whales and concedes that everything hed said about her earlier is true. They go to the hotels bar where she admits that every time things go well for her, she runs and hides out of fear. Peter tells her a story of how he couldnt get work after the Comics Code Authority was established, and he had to accept jobs he hated to support his wife until the day she died. He tells Cola that "bravery will always be the driving point of your life," and "you know youve run out of it when suddenly nothing changes anymore." He then leaves, declaring that he wont come help Purgatory Comics during the sale.

That evening, Berner offers to help Mia with her volunteer work so he can spend time with her. Cola returns to Dannys house and tells him shes not afraid to keep him anymore. Three weeks later, Cola visits the big Purgatory Comics sale during her lunch break from a new job. It is revealed that Berner has become a full-time employee and, thanks to Dannys help, a designer for the stores web site. Berner has also begun a relationship with Mia while Andrew has become a comic book fan. Evan and Cola are happy to see each other again. As she leaves with Danny, they spot Peter Whales going into the store to do a surprise signing.

==Cast==
* Angela Benedict as Cola
* Tim Kelly as Evan
* Jonathan Zungre as Berner
* April Simpson as Mia
* Alex Adams as X-Ray
* Bree Olson as Dream Girl
* Bob Socci as Peter Whales
* Atoki Ileka as Andrew
* Vinny Bove as Danny

==Reception==
The film became a major winner at the 2010 Long Island International Film Expo where it won "Best Long Island Feature Film," "Best Actress (Angela Benedict), "Best Supporting Actor (Jonathan Zungre)," and "Best Song (In the Spin by Fox Vulpini)."  The ceremony was hosted by Brian OHalloran (Clerks) and Jackie Martling.  The film was also an Award of Merit winner during the 2009 Accolades Competition in the categories of "Feature Film" and "Casting." 

Online reviews were generally mixed, with one review on Aint It Cool News accusing the film of being tonally similar to Kevin Smith movies such as Clerks. 

==References==
 

==External links==
* 
* 
* 

 