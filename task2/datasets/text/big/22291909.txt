End of a Priest
{{Infobox film
| name           = End of a Priest
| image          = 
| caption        = 
| director       = Evald Schorm
| producer       = 
| writer         = Evald Schorm Josef Škvorecký
| starring       = Vlastimil Brodský
| music          = 
| cinematography = Jaromír Sofr
| editing        = Jiřina Lukešová
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

End of a Priest ( ) is a 1969 Czechoslovak comedy film directed by Evald Schorm. It was entered into the 1969 Cannes Film Festival.   

==Cast==
* Vlastimil Brodský - Verger
* Jan Libíček - Teacher
* Zdena Škvorecká - Anna
* Jana Brejchová - Majka
* Vladimír Valenta - Farmer
* Pavel Bosek
* Gueye Cheick - Black Bishop
* Andrea Cunderlíková
* Libuše Freslová - Mother
* Vladimír Jedenáctik - Tramp
* Václav Kotva
* Marie Landová
* Pavel Landovský
* Jiří Lír
* Josefa Pechlatová - Granny
* Eva Řepíková
* Martin Ruzek - White bishop
* Helena Růžičková - Farmers wife
* Jaroslav Satoranský - Toník

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 