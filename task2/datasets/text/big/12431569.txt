Backfire (1988 film)
{{Infobox film
| name           = Backfire
| image          = Backfireposter1987.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Gilbert Cates
| producer       = Danton Rissner	
| writer         = Larry Brand Rebecca Reynolds
| narrator       =
| starring       = Karen Allen Keith Carradine Jeff Fahey Bernie Casey Dean Paul Martin
| music          = David Shire
| cinematography = Tak Fujimoto
| editing        = Melvin Shapiro	
| distributor    = ITC Entertainment
| released       = June 21, 1988
| runtime        = 90 min
| country        = United States Canada English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

Backfire is a 1988 mystery-thriller film about a murderous love triangle which forms between an affluent Vietnam War veteran, his wife, and another man. The film was directed by Gilbert Cates, and stars Karen Allen, Keith Carradine, and Jeff Fahey.

==Plot==
Mara is married to Vietnam veteran Donny, who has horrible visions and nightmares of his combat experiences. Mara is having an affair with Jake, and the lives of all are disrupted when she meets a mysterious stranger, Reed.

==Cast==
* Keith Carradine as Reed
* Karen Allen as Mara
* Jeff Fahey as Donny
* Dean Paul Martin as Jake
* Bernie Casey as Clinton
* Dinah Manoff as Jill Tyson

==References==
 
 

==External links==
*  

 

 
 
 
 
 
 
 
 

 