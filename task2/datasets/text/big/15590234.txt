The Chicken Chronicles
 

{{Infobox Film
| name             = The Chicken Chronicles
| image            = The Chicken Chronicles.jpg
| caption          = film poster Frank Simon
| producer         = Walter Shenson
| writer           = Paul Diamond 
| starring         = Phil Silvers Ed Lauter Steve Guttenberg
| music            = LeRoy Holmes Ken Lauber
| cinematography   = Matthew F. Leonetti
| editing          = George Folsey Jr.    
| distributor      = AVCO Embassy Pictures
| released         = October 1977
| runtime          = 95 minutes
| language         = English
}}
The Chicken Chronicles is a 1977 teen comedy film, set in 1969 and starring Steve Guttenburg.

==Synopsis==
David Kessler is a high school student who will go to any lengths to impress a pretty cheerleader and lose his virginity, while juggling his job at a chicken joint and trying not to get thrown out of Beverly Hills High - a fate that could get him sent to Vietnam War|Vietnam.

==Cast==
*Phil Silvers - Max Ober
*Ed Lauter - Vice Principal Nastase 
*Steve Guttenberg - David Kessler 
*Lisa Reeves - Margaret Shaffer
*Gino Baffa - Charlie Kessler
*Meridith Baer - Tracy Vogel 
*Branscombe Richmond - Mark 
*Jon Gries - Tom 
*Raven De La Croix - Mrs. Worth

==References==
 

==External links==
* 
*  

 
 
 
 
 


 