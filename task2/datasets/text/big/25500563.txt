The Beast (1974 film)
 
{{Infobox film
| name           = The Beast
| image          = Il bestione.jpg
| image_size     =
| caption        = Official Teaser Poster
| director       = Sergio Corbucci
| producer       = Carlo Ponti
| writer         = Luciano Vincenzoni Sergio Donati
| narrator       = 
| starring       = Giancarlo Giannini Gabriella Giorgelli Michel Constantin
| music          = Guido & Maurizio De Angelis 
| cinematography = Giuseppe Rotunno
| editing        = Eugenio Alabiso
| studio         = Champion PECF
| distributor    = Warner-Columbia Filmverleih
| released       = 20 November 1974
| runtime        = 100 minutes
| country        = Italy France
| language       = Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Italian comedy directed by Sergio Corbucci and stars Gabriella Giorgelli, Michel Constantin and Giancarlo Giannini. 

==Plot==
 
A Lombardy trucker with 20 years of trade on the back, see you foisted as the second driver a Sicilian much younger men, at first the two are struggling to tie: why so much is rough and taciturn. Their big beast a large truck to transport international wares leading them on the streets of Europe, are friendship between both was born between and it rules sincere harmony. The fellows gathered the savings and signing a lot of bills, on the first trip from owners in Germany to load back comes in trouble with a shady individual police. In problems Sandro and Nino become to dare losing the truck, it will nevertheless be safe and sound from bad adventure, the friends goes an unchanged confidence of his future. 

==Cast==
* Giancarlo Giannini as Nino Patrovita
* Michel Constantin as Sandro Colautti
* Giuseppe Maffioli as Super Shell
* Giuliana Calandra as Amalia
* Dalila Di Lazzaro as Magda
* Enzo Fiermonte as Matteo Zaghi
* Jole Fierro as the former wife of Colautti		
* Philippe Hersent as Owner of CISA Car Company
* Carlo Gaddi as Camorrista
* Gabriella Giorgelli as Zoe
* Attilio Duse as Truck driver
* Anna Mazzamauro as Friend of Amalia
* Giorgio Trestini as Andrea
* Imma Piro as Lella Colautti
* Franco Angrisano as Mafioso
* Romano Puppo as Dutch Driver
* Franca Scagnetti as Receptionist
* Sergio Testori as Dutchman

==Release==
The film premiered on 20 November 1974 in Paris and screened in West Germany on 14 February 1975 as Die Cleveren Zwei with an Warner-Columbia Filmverleih distribution. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 