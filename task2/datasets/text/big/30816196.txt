Dil-e-Nadaan
{{Infobox film
| name = Dil-e-Nadaan
| image = Dil-E-Nadaan.jpg
| image_size =
| caption = Dil-E-Nadaan
| director       =   Art Director: Bhaskara Raju
| producer       = S.M. Sundaram
| writer         = C.V. Sridhar Charandas Shok (Dialogues)
| screenplay     = C.V. Sridhar
| story     = C.V. Sridhar Agha  Keshto Mukherjee  Jagdish Raj  Shivraj Khayyam Naqsh Lyallpuri (Lyrics)
| cinematography = R.K.Tiwary
| editing        = Subba Rao 
| distributor    = 
| released       = 
| runtime        =
| country        = India Hindi
| gross          =  2.5 crores
| preceded_by    =
| followed_by    =
| website        =
}} Indian Hindi film directed by C.V. Sridhar, released in Bollywood films of 1982|1982. The film stars Rajesh Khanna in the main lead role as Anand and Shatrughan Sinha as his friend  as the second lead character of the film.  This film was remake of 1978 Tamil film Ilamai Oonjal Aadukirathu, directed by  C.V. Sridhar, where Kamal Hassan and Rajnikanth played the lead male roles. After success of the Tamil version, C.V. Sridhar decided to remake it in Hindi and directed it himself.

==Plot==
Vikram (Shatrughan Sinha) and Anand (Rajesh Khanna) are best friends, brought up by their mom (Dina Pathak). Anand is an orphan who was adopted by the family as a child and he has never been given the impression that he was an outsider. Vikram and Anand are owner and manager respectively, of an advertising agency in Bombay. Both work for the same organization & love the same woman, Asha (Smita Patil).

In the organization, Sheela (Jaya Prada) works as a deputy to Anand and is in love with him, though Anand is unaware of this. Sheela continues to harbour love for Anand even after finding out he loves Asha, whom Sheela is friends with and also lives with. Vikram frequents a bus stop daily on his way to office, where he always sees Asha. He instantly decides to propose to her. Vikram discloses to Anand that he wants to marry a girl but does not reveal her name. Meanwhile, Vikrams mother looks for suitable girls for their marriage and declares that she would marry both her sons together on the same day.

Sheela becomes confused as to whether she should continue to love Anand in spite of knowing that Anand and Asha love each other. One day, Sheela meets Vikram and as they talk, she says things that indicate she feels lonely and has lost her loved one to another. The next day, Sheela apologizes to Vikram and asks for a 10-day leave. She decides to go to her hometown to forget Anand. However, Asha joins her, as she learns that Anand is going there as well on a business trip. On a rainy day, Sheela is alone in the house when Anand arrives to meet Asha. He learns that Asha is attending a friends wedding, but as no train is available for returning, Anand is forced to stay the night. In the midnight, Anand starts feeling cold due to the weather, so he starts drinking. After a few drinks, he starts seeing Sheela as Asha. Despite knowing he is wrong, Sheela and Anand make love.

In the morning, Anand realizes what has happened and writes a letter to Sheela, asking her to keep the incident a secret for life, as he wants marry Asha only. However, Asha comes across the letter first and becomes aware of what happened between Sheela and Anand in her absence. She then decides that, as she respects her friendship with Sheela, she will sacrifice her love for Sheela and make Anand marry her instead.

The rest of the story shows what will happen to Vikram and Anands friendship after this incident. Will Anand marry Sheela? Will Sheela agree to marry Anand? Will Sheela make Asha marry Anand? Will Vikram marry Asha?

==Cast==
*Rajesh Khanna  as  Anand
*Jaya Prada  as  Sheela
*Shatrughan Sinha  as  Vikram
*Smita Patil  as  Asha
*Om Prakash  as  Ashas Father
*Dina Pathak  as  Vikrams Mother
*Shivraj  as  Sheelas Father Agha
*Ashalata
*Jagdish Raj
*Keshto Mukherjee

==Soundtrack==
{{Track listing Khayyam
| all_lyrics   = Naqsh Lyallpuri
| extra_column = Singer(s)
| title1       = Agar Leta Hoon Tera Naam | extra1 = Kishore Kumar
| title2       = Chandni Raat Mein | extra2 = Kishore Kumar, Lata Mangeshkar
| title3       = Dil Tera Hai | extra3 = Asha Bhosle
| title4       = Isse Pehle Ki Yaad Tu Aaye | extra4 = Kishore Kumar
| title5       = Tera Ishq Hai Meri Zindagi | extra5 = Kishore Kumar, Lata Mangeshkar
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 