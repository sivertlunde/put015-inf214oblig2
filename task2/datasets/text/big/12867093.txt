Patlabor 2: The Movie
{{Infobox film
| name           = Patlabor 2: The Movie
| image          = Patlabor2DVD.jpg
| caption        = DVD cover art
| director       = Mamoru Oshii
| writer         = Kazunori Itō
| starring       = Ryūsuke Ōbayashi Yoshiko Sakakibara Naoto Takenaka
| producer       = Mitsuhisa Ishikawa Shin Unozawa Tsuyoshi Hamawatari
| music          = Kenji Kawai
| cinematography = 
| editing        = Shuichi Kakesu
| studio         = Production I.G
| distributor    = Shochiku
| released       =  
| runtime        = 113 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
}} Allied Forces after the end of World War II. 

==Plot==
 
Three years after the events of  ,     Noa Izumi and Asuma Shinohara are now testing new Labors at a facility run by the Metropolitan Police. Isao Ota is a police academy Labor instructor. Mikiyasu Shinshi has since been reassigned as the Tokyo Metropolitan Polices head of General Affairs. Seitaro Sakaki has retired with Shigeo Shiba taking over his position as head of the labor maintenance team with Hiromi Yamazaki, Kiichi Goto and Shinobu Nagumo remaining with the unit as Kanuka Clancy had permanently returned to New York. Most of them had been replaced by fresh labor pilots.
 GSDF forces JASDF was JSDF bases take place as a means of conveying their denial of the bridge attack. Before long, public panic comes as JGSDF-marked gunships attack in several bridges in Tokyo Bay, various communication centers and SV2 headquarters, coupled by the release of a supposed deadly gas after Special Assault Team snipers shoot down an auto-piloted blimp that was responsible for jamming all electronics in the Greater Tokyo Area.
 UN peacekeeping US intervention looming unless the government controls the situation, the team uses an old stretch of the Ginza Line to approach an artificial island Tsuge uses as his hideout. Goto also takes care of things on his end by facilitating the arrest of Shigeki Arakawa, a GSDF intelligence agent who is actually one of Tsuges cohorts. After a fierce fight inside the artificial islands tunnel which results in flooding, the team evacuates the tunnel while Nagumo breaks through to finally arrest Tsuge.

==Cast==
{| class="wikitable"
|-
!  style="width:25%; text-align:center;"| Character
!  style="width:25%; text-align:center;"| Original Japanese
!  style="width:25%; text-align:center;"| English (Manga UK)
!  style="width:25%; text-align:center;"| English (Bandai Visual)
|- Noa Izumi || Miina Tominaga || Briony Glassico || Julie Ann Taylor
|- Asuma Shinohara David Jarvis || Doug Erholtz
|- Kiichi Goto || Ryunosuke Ohbayashi || Peter Marinker || Roger Craig Smith
|- Shinobu Nagumo || Yoshiko Sakakibara || Sharon Holm || Megan Hollingshead
|- Kanuka Clancy || Yō Inoue || Tamsin Hollo || Lisa Enochs
|- Isao Ohta || Michihiro Ikemizu || Martin McDougall || Sam Riegel
|- Mikiyasu Shinshi || Issei Futamata || Ron Lepaz || Joe Ochman
|- Hiromi Yamazaki || Daisuke Gouri || Michael Fitzpatrick || Jason C. Miller
|- Shigeo Shiba Peter Doyle
|- Seitaro Sakaki || Osamu Saka || Blair Fairman || Jamieson Price
|- Jitsuyama || Mahito Tsujimura || Don Fellows || Milton Lawrence
|- Matsui || Tomomichi Nishimura || Mac McDougall || Paul St. Peter
|- Kaihou || Toshihiko Kojima || Julian B. Wilson || Paul St. Peter
|- Fukushima || Shinji Ogawa || Bill Roberts || Bob Papenbrook
|- Kataoka || Kouji Tsujitani || William Dufries || Liam OBrien
|-
| Shigeki Arakawa || Naoto Takenaka || Blair Fairman || Kim Strauss
|-
| Yukihito Tsuge || Jinpachi Nezu || Bob Sherman || Robert Clotworthy
|-
|}

==Production==
Production of Patlabor the Movie 2 started after Kazunori Ito was appointed as the scriptwriter for the upcoming movie back in the early 1990s. Patlabor the Movie 2 Archives Book, page 20.  The original plan called for the same plot used in the original OVA series episode "The SV2s Longest Day", which showed renegade JGSDF soldiers and officers conspiring to undermine and overthrow the Japanese government.  However, it was soon abandoned when Mamoru Oshii told Kazunori Ito that maybe the scope of terrorists causing havoc under the cover of a coup would be a better movie idea. Patlabor the Movie 2 Archives Book, page 20. 
 HEADGEAR to Oshii working on the storyboard. 
 coup détat attempt. 

==Subject==

===Theme background===
  is declared.]]

Most of the prevailing theme in the movie has been cast on the Japanese Self-Defense Forces and on their legality as Japans military force since Article 9 of the Japanese Constitution, created on May 3, 1947 by the Allied Forces, meant that the JSDF is only allowed to defend Japanese territory from hostile invasion and not to be deployed in any manner on foreign soil.  

Kazunori Ito and Mamoru Oshii were opposed to the deployment of JSDF personnel to participate in UNTAC, a UN peace keeping mission in Cambodia .  

===Basis===
At the time of Patlabor 2s release in 1993, a lot of issues had faced the Japanese government in both the domestic and international level. Most of the issues include the end of the Soviet Union, Japans economic prosperity in the 1980s and the 1990s, and outbreak of the Gulf War with the 1990 market crash that left many Japanese devastated.  In an interview with Animage magazine on October 1993, Hayao Miyazaki said that the opening scene of the movie was inspired by the local Cambodian scene, especially with the appearance of what seems to be Angkor Thom since Miyazaki said that it is the case. He also mentions with Mamoru Oshii on the nature of the limitations that JSDF personnel faced in Cambodia, since the JSDF was formed originally not as a sort of military body of the post-WWII Japanese government.   

Several real-life incidents were also mentioned or used for the movie. For instance, references to Viktor Belenkos defection to Japan in 1976 were mentioned by Arakawa himself when he spoke to Goto and Nagumo.    Another incident with the nearly fatal dogfight over Tokyo was supposedly based on an accident in a computer simulation that forced the US military to go onto DEFCON 3 and almost went to nuclear war with the Soviet Union. 
 Allied forces. 

==Release==
It was shown originally in Japanese theaters on August 7, 1993.  Later in 1998, the movie was released again with Dolby Digital 5.1 channel remix and additional music.    

===Home release===
Patlabor 2: The Movie was dubbed and released in 1995 in Australia and the UK by Manga Entertainment and by Manga Entertainment in the USA in 1996. In the mid 00s, Manga Entertainment lost the licenses to Patlabor 1 and 2 in the UK and the USA, but retained the license in Australia, where Manga Entertainment properties are distributed by Madman Entertainment. Bandai Visual later released the film under their North American label, Honneamise, with a new English dub produced by Elastic Media Corporation. 

Aside from Bandai Visual having the license to North America and the UK,   Panorama owns the license for Patlabor 2 to Hong Kong and Macau with a Cantonese dub and subtitles aside from the original Japanese dub. 

In Japan, Bandai Visual/Emotion has released Patlabor 2 on DVD, Blu-ray and on UMD.  Only the double disc DVD/Blu-ray and DVD/HD DVD sets as well as the single disc Blu-ray release has the remade English dub and subtitles.  The Blu-ray also includes a booklet that includes illustrations and screenshots from the movie. 

In Australia and New Zealand, Manga Entertainment UK still holds the rights to Patlabor 1 & 2 and is sub-licensed and distributed by Madman Entertainment on DVD.  Due to this ongoing license and the popularity of Mangas dub of the 2 movies in Australia amongst Anime fans, Madman & Manga were refused the remastered DVD & Blu-ray license from Bandai Visual which contained the new Elastic Media dub. Since Bandai Visual USA is no longer releasing new anime and letting current licenses lapse, the chances of a remastered release in Australia is slim.

Beez Entertainment licensed the film in Europe. Maiden Japan has licensed all three Patlabor films, with the second film to be released on Blu-ray and DVD on July 21, 2015. 

==Reception==
Views on Patlabor the Movie 2 have been very positive. One review from the Anime Cafe states that Patlabor the Movie 2 is an "intellectually stimulating anime", combining the movies mecha concept with the plot similar to a Tom Clancy novel.  Anime World Order states that while the sequel is better than the first in terms of action and drama, some viewers may not understand the political aspects of the film, though its philosophical themes may be easier to understand, especially with the concept of just war and unjust peace.  Japan Heros review of the movie commented well on the side story that the movie showcased the main characters who had to let go of their past and to move on and grow up, with the fact that they had to get back together and face a threat that could make a country ruled by civilian authority or one ruled by fear and paranoia.  Anime News Network has noted the background music used in the movie "favors heavy, pulsing techno beats backed by airy, haunting vocals for the intense scenes and soaring synthesized scores in other places."   
 Mainichi Film Concours award in the category for Best Animated Film. 

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 