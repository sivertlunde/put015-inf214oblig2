Lebanon, PA (film)
{{Infobox film
| name = Lebanon, PA
| image = 
| image_size = 
| border = 
| alt = 
| caption = 
| producer = Jason Contino
| screenplay = Ben Hickernel
| based on = 
| starring =  
| music = Thomas Newman
| cinematography = Jeff Schirmer
| editing = Ben Hickernell 
| studio = Reconstruction Pictures
| distributor = 
| released =   
| runtime = 100 minutes
| country = United States
| language = Arabic, English, Hebrew
| budget = 
| gross = $48,381
}}

Lebanon, PA  is a 2010 American drama film written and directed by Ben Hickernel. The film tells the story of Will, a 35-year-old man from Philadelphia who travels to Lebanon, Pennsylvania to bury his recently deceased father. While visiting he begins to build a relationship with his 17-year-old cousin, CJ, who is recently pregnant. Through this relationship, Will meets and becomes romantically interested in CJs teacher, who is married. After CJ confronts her conflicted father, both Will and she struggle with difficult decisions regarding their futures. The film explores the challenges of bridging the cultural divide in America through the lens of an extended family. 

Matt Pond and Chris Hansen of the band Matt Pond PA composed the score for the film.

== External links ==
*  
*  