Aussie Rules the World
{{Infobox film
| name           = Aussie Rules the World
| image          = Aussie Rules the World.jpg
| caption        =
| director       = Michael Stringer McIntyre
| producer       = Michael Stringer McIntyre
| writer         = Michael Stringer McIntyre Kevin Sheedy Paul Roos Bachar Houli John Longmire David Wenham Tadhg Kennelly Adam Goodes
| cinematography = 
| editing        = 
| distributor    = Second Nature Films
| released       =  
| runtime        = 90 minutes
| country        = Australia
| language       = English
}}
 Paul Roos, Kevin Sheedy, former AFL CEO Andrew Demetriou, current   player Bachar Houli and incumbent Australian of the Year Adam Goodes.

Filmed over a period of four years between 2011 and 2014, it focuses on the sport of Australian rules football and its efforts to spread the game worldwide. The film was released on 22 July 2014. 

==Background== Better Man which looked into the real-life execution case of Van Tuong Nguyen, the film follows the journey of recently retired Sydney Swans premiership player Brett Kirk and his family, who embark on a six-month journey around the world to help spread the game across the globe, amid fears that the sport has been largely ignored in favour of other sports such as association football|soccer, rugby union and rugby league. 
 round 23, 2011 AFL match between   and the  .

==Cast==
{| class="wikitable"
|-
! Actor !! Role
|- Brett Kirk and his family || Themselves
|- Paul Roos Paul Roos || Himself
|- Kevin Sheedy Kevin Sheedy || Himself
|- Bachar Houli || Himself
|- Mike Sheahan || Himself
|- John Longmire || Himself
|- Adam Goodes || Himself
|- Tadhg Kennelly || Himself
|- Andrew Demetriou || Himself
|- Majak Daw || Himself
|}

==See also==
*Year of the Dogs
*List of sports films

==References==
 

==External links==
*  
*  

 
 
 
 
 