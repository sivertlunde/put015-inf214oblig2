Hide and Seek (2007 film)
 
{{Infobox film
| name           = Hide & Seek
| image          =
| caption        =
| director       = Rahyan Carlos
| producer       = Lily Monteverde Roselle Monteverde-Teo Lea Calmerin Manny Valera
| writer         = Gina Marissa Tagasa
| narrator       =
| starring       = Eric Quizon Jean Garcia Alessandra de Rossi Ryan Eigenmann Valeen Montenegro Jennica Garcia Mart Escudero
| music          = Von de Guzman
| cinematography = Eli Balce Luis Quirino
| editing        = C.J. Piol Regal Entertainment, Inc. Regal Multimedia, Inc.
| released       =  
| runtime        =
| country        = Philippines
| language       = Tagalog English
| budget         =
| gross          =
}}

Hide & Seek is a 2007 Philippine horror film directed by Rahyan Carlos.

==Plot==
Oliver Aliciano (Eric Quizon), a college professor, began to leave Manila with his wife Leah (Jean Garcia), his rebellious stepdaughter Nica (Jennica Garcia) and her young brother Uno (Julijo Pisk) after he was blamed on the death of Ella Cabuena (Valeen Montenegro), Nicas best friend. After moving to an abandoned house to stay, ominous visions and hauntings happened there.

At first they didn’t mind the strange apparitions and continue to live like there’s nothing else with them in the house. Later, they find out the real story of the house where there lived before a rich family and the husband had an affair with his maid, Dolor (Alessandra de Rossi) that’s why the husband of Dolor – Gener (Ryan Eigenmann) get jealous and suspected that their daughter and her upcoming baby is not his.

So Gener confronted Dolor as she also admit that he has nothing to suspect because it’s his blood but Gener didn’t listen and as the struggles goes on Gener accidentally killed her daughter with the nail on her head and also kill Dolor using his sharp wedge. Gener also was killed later then. After hearing that story Oliver and Leah find ways to stop the haunting but later they realize that the ghost of Dolor and her daughter only wants to help them but the ghost of Gener wants them killed like what happened to his family.

The ghost of Gener possessed Oliver and made him attacked every member of his family but because of the help of Dolor and her daughter, they stopped the ghost of Gener and free from Oliver’s body. It ended them leaving the house to start a new life back in the city while Oliver is also acquitted from the accused murder and also getting the bond with his step daughter.

==Cast==
*Eric Quizon as Oliver Aliciano
*Jean Garcia as Leah Aliciano
*Alessandra de Rossi as Dolor Buntag
*Ryan Eigenmann as Gener Buntag
*Mart Escudero as Joseph Dionisio
*Jennica Garcia as Nica Aliciano
*Valeen Montenegro as Ella Cabuena
*Rubirubi as Andeng
*Angel Sy as Tala Buntag
*Julijo Pisk as Uno Aliciano

==External links==
* 

 
 
 
 
 
 
 

 
 