Rites of Passage (2012 film)
 
{{Infobox film
| name           = Rites of Passage
| caption        =
| image	=	Rites of Passage FilmPoster.jpeg
| director       = W. Peter Iliff
| producer       = Mark Canton David Hopwood William D. Johnson Jonathan Sachar
| writer         = W. Peter Iliff Rick Halsey
| starring       = Wes Bentley Kate Maberly Ryan Donowho Christian Slater Stephen Dorff
| music          =Elia Cmiral
| cinematography = Alex Nepomniaschy
| editing        = Glenn Garland 
| studio         = Voltage Pictures
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Rites of Passage is a 2012 American thriller film written and directed by W. Peter Iliff. The film stars Wes Bentley, Kate Maberly, Ryan Donowho, Christian Slater and Stephen Dorff.

==Plot==
Nathan, (Ryan Donowho), a studious anthropology student decries his position in a society that fails to provide a ceremony to mark his transition into manhood. Professor Nash (Stephen Dorff), an ethically challenged teacher who openly pursues sexual liaisons with his female students, is intrigued when Nathan tells him his familys unoccupied but functional ranch is the site of an ancient Indian burial ground. Nathan convinces Professor Nash to bring the class out to the ranch, where the plan is to hold a spiritual ceremony in a sweat lodge built over the burial site. Encouraged by Nashs enthusiastic agreement to the idea, Nathan guides his stressed, party-going classmates to the ranch. There they meet his brother, Benny, (Wes Bentley) who is in a psychotic spiral of drug addiction and guilt.  A crystal meth cook at the ranch, Delgado, (Christian Slater) a man with a grudge who has an uneasy friendship with the brother, kills two of the students after he and the brother are insulted. Delgado then begins hunting the rest of the group. During his rampage, Delgado hallucinates a talking, stuffed monkey named Pancho that clings to his shotgun and makes futile attempts to reason with Delgados apparently absent sense of morality.

==Cast==
*Wes Bentley as Benny
*Kate Maberly as Dani
*Ryan Donowho as Nathan
*Christian Slater as Delgado
*Stephen Dorff as Professor Nash
*Briana Evigan as Penelope
*Travis Van Winkle as Hart
*Carly Schroeder as Carly
*Ashley Hinshaw as Sandee
*Guy Burnet as Mojo

==Release==
The film was released, Straight-to-DVD on October 16, 2012. The trailer for the film was released on late 2011, and can be viewed on the official Rites of Passage  , Facebook page, and YouTube.

The film premiered at the Isla Vista Theatre in Isla Vista, Calif. The audience responded enthusiastically, except for about a half dozen Native American activists who appeared at the showing to protest. During the subsequent Q&A, the activists accused the film of being racist, exploitative, a mockery of Chumash rituals, and "cultural prostitution." However, actor Mylo IronBear, a Native American spirit warrior of the Spirit Lake Tribe, who appears as a Chumash in the film, disagreed with the activists. "I am standing behind this film because it’s a film,” IronBear said, adding that Iliff was entitled to his artistic interpretation.
 Chumash tribe stood up after the activists were removed by police and apologized for their behavior, indicating that they did not represent the Chumash Nation.  The Chumash jimsonweed ceremony portrayed in the film was accurately represented according to anthropologist J.P. Harringtons extensive field notes from Chumash informants in the early 1900s. 

==References==
 

==External links==
*  

 
 
 
 
 