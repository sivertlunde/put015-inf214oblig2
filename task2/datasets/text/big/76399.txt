Trouble in Paradise (film)
{{Infobox film
| name           = Trouble in Paradise
| image          = Troubleinparadise1932.JPG
| image_size     = 225px
| caption        = theatrical release poster
| director       = Ernst Lubitsch
| producer       = Ernst Lubitsch
| screenplay     = {{Plainlist|
* Samson Raphaelson
* Grover Jones (adaptation)
* Ernst Lubitsch (uncredited)
}}
| based on       =  
| starring       = {{Plainlist|
* Miriam Hopkins
* Kay Francis
* Herbert Marshall
}}
| music          = {{Plainlist|
* W. Franke Harling (music)
* Leo Robin (lyrics)
}}
| cinematography = Victor Milner
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $519,706   at Kay Francis Films. Accessed 16 March 2014 
| gross          = $475,000 (US/Canada) 
}}

Trouble in Paradise is a 1932 American  . Accessed=August 24, 2012  the film is about a gentleman thief and a lady pickpocket who join forces to con a beautiful perfume company owner.

In 1991, Trouble in Paradise was selected for preservation by the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 

==Plot==
In Venice, Gaston Monescu (Herbert Marshall), a master thief masquerading as a baron, meets Lily (Miriam Hopkins), a beautiful thief and pickpocket also pretending to be of the nobility, and the two fall in love and decide to team up. They leave Venice for Paris, and go to work for the famous perfume manufacturer Madame Mariette Colet (Kay Francis), with the intention of stealing a great sum of money from her safe, which Monescu, as her secretary, arranges to be diverted there. In the course of things, Colet begins to flirt with Monescu, and he begins to have feelings for her.

Unfortunately, the plan develops a hitch when François Filiba (Edward Everett Horton), one of Colets suitors, sees Monescu at a garden party. He is unable to remember where he knows him from, but when another of Colets suitors, The Major (Charles Ruggles), tells Filiba that he once mistook Monescu for a doctor, Filiba suddenly remembers that he knows Monescu from Venice, where the thief robbed him, pretending to be a doctor. Monescu and Lily plan an immediate getaway that night, after they take all the money in the safe.

Colet prepares to leave for a dinner party given by the Major, but cannot decide whether to go or to stay and have sex with Monescu. Eventually she goes, but not before Lily catches on that Monescu has fallen for her rival, and wants to back out of the plan &ndash; so she robs the safe herself after confronting her partner. At the Majors, Filiba tells Colet about Monescu, but she refuses to believe its true. She returns home and suggestively probes Monescu, who admits that the safe has been cleaned out, but claims that he himself took the cash. He also tells her that the manager of her business, Adolph J. Giron (C. Aubrey Smith), who has been suspicious of Monescu all along, has stolen millions of dollars from the firm over the years.

Lily then confronts Colet and Monescu, reporting that it was she who stole the money from the safe.  An argument ensues, in which, eventually, Colet allows the two thieves to leave together. As a parting shot, Monescu steals a necklace from Colet that Lily had her eye on, and, in turn, Lily steals it from him, displaying it to him as the taxi takes them away, hugging each other.

==Cast==
* Miriam Hopkins as Lily
* Kay Francis as Madame Mariette Colet
* Herbert Marshall as Gaston Monescu / Gaston Lavalle
* Charles Ruggles as The Major
* Edward Everett Horton as François Filiba
* C. Aubrey Smith as Adolph J. Giron
* Robert Greig as Jacques, Mariettes butler
* Leonid Kinskey as The Communist

==Production==
Working titles for Trouble in Paradise included "The Honest Finder," "Thieves and Lovers," and "The Golden Widow"; the latter was publicly announced to be the intended release title.    As with all the Lubitsch-Raphaelson collaborations, Lubitsch contributed to the writing and Raphaelson contributed ideas to the directing.  Lubitsch did not receive screen credit for his writing, and   whose memoir was published in 1907, and became the basis for two silent films. 
 Production Code, the film is an example of Pre-Code Hollywood|pre-code cinema containing adult themes and sexual innuendo that was not permitted under the Code. In 1935, when the Production Code was being enforced, the film was not approved for reissue  and was not seen again until 1968.  Paramount was again rejected in 1943, when the studio wanted to make a musical version of the film. 

The Art Deco sets for Trouble in Paradise were designed by the head of Paramounts art department, Hans Dreier, and the gowns were designed by Travis Banton. 

==Reception==
Trouble in Paradise was the film that first had people talking about "the Lubitsch Touch," and it was, in fact, one of the directors favorites.    Critic Dwight Macdonald said of the film that it was "as close to perfection as anything I have ever seen in the movies."  The New York Times named the film as one of the ten best films of 1932. In 1998, Roger Ebert added it to his Great Movies collection.   Wes Anderson and Ralph Fiennes both said the movie was an inspiration for The Grand Budapest Hotel.

==Awards and honors==
Trouble in Paradise was named by the National Board of Review as one of the top ten films of 1932.      

==References==
Notes
 

==External links==
*  
*  
*  
*  
*   by Armond White

 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 