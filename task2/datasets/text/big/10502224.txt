The Monster of Piedras Blancas
  
{{Infobox film | name = The Monster of Piedras Blancas
  | image = Monsterofpiedrasblanas.jpg
  | caption = Poster for the films theatrical release, on a double-feature with Okefenokee 
  | director = Irvin Berwick  
  | producer = Jack Kevan
  | writer = Irvin Berwick John Harmon Pete Dunn Jeanne Carmen
  | cinematography = Philip H. Lathrop
  | editing = George A. Gittens	
  | studio         = Vanwick Productions
  | distributor    = Filmservice Distributors Corporation
  | released =   April 22, 1959   1961 
  | runtime = 71 min 
  | country        = United States
  | language = English  
  | budget = $29000   
    }}
 1959 science fiction/horror film written and directed by Irvin Berwick and starring Jeanne Carmen, Les Tremayne, John Harmon, Don Sullivan, Forrest Lewis, and Pete Dunn. Influenced by The Creature from the Black Lagoon (1954), the film was produced by Jack Kevan, who had supervised the manufacture of the Creature suit at Universal-International, and created the Piedras Blancas monster costume. Kevan employed several of his former Universal associates on the picture including soundman Joe Lapis and prop master Eddie Keys. 

==Plot== 
The setting is the sleepy lighthouse town of Piedras Blancas. Sturges (Harmon) is the lighthouse keeper of the town and is very superstitious and concerned for the safety of his teenage daughter, Lucy (Carmen). He leaves food for a sea monster who lives in a nearby cave. The locals disregard him at first, but they begin to take notice when the bodies of people murdered by the monster are found on the beach. A local scientist identifies a scale as being from a "diplovertebron," a  prehistoric amphibious reptile presumed long extinct.

==Cast==
*Les Tremayne as Dr. Sam Jorgenson
*Forrest Lewis as Constable Matson John Harmon as Sturges, the Lighthouse Keeper
*Frank Arvidson as Kochek, the Storekeeper
*Jeanne Carmen as Lucy
*Don Sullivan as Fred
*Pete Dunn as Eddie (The Monster)
*Joseph La Cava	as Mike
*Wayne Berwick as Little Jimmy

==Production==
Both Jack Kevan and Irvin Berwick toilied in unbilled obscurity as contract employees at Universal-International. Berwick had been an uncredited dialogue director at U-I and at Columbia prior to that, working with the likes of William Castle and Jack Arnold. Kevan in particular chafed under the stewardship of Bud Westmore, the head of the studios makeup department,who seldom allowed employees like Kevan or sculptors Chris Mueller and Millicent Patrick to receive publicity. Berwick and Kevan formed Vanwick Productions and became independent producers. Their first film, The Monster of Piedras Blancas was designed as a patch on U-Is popular Creature from the Black Lagoon, whose iconic monster suit Kevan had helped create. For this movies fictional "diplovertebron", Kevan cut cost and labor time by using existing molds for the feet (cast from those of the Metaluna Mutant from This Island Earth) and the oversized hands (designed originally for The Mole People.) Actor/stuntman Pete Dunn wore the green-hued monster suit in the film, and did double-duty playing the bartender. Universal gave a great deal of unofficial cooperation to the production, since it was going through a period of budget problems. Vanwick received sweetheart deals for production vehicles and equipment, the studios way of helping the many laid-off technicians who found work on the independent film. Top-lined Don Sullivan would appear in a number of other genre films afterward, such as The Giant Gila Monster.  
 Point Conception lighthouse near Lompoc, and the movies "town"  is actually the seaside town of Cayucos, about 30 miles south of the real Piedras Blancas.   

Several scenes broke new ground for onscreen gore, such as the monster making a shock entrance carrying a bloody human head, and a later shot of the same head with a crab crawling across the face. The film was released on a double bill with Okefenokee, a bayou melodrama. Kevan and Berwick made several other B-films, notably The Street Is My Beat, before Kevan left show business to start a cosmetics company. Berwick continued to direct and produce low budget features into the 1980s.    
Parts of the rubber monster suit showed up years later in the TV show Flipper (1964 TV series)|Flipper, in the episode Flippers Monster, which was directed by Ricou Browning, who had performed the Gill Man swimming scenes in Creature From the Black Lagoon.  The War of the Worlds). Irv Berwick supplied an off-screen radio voice for the parody. 

==Reception==
  James Rolfe stated in his review of the 1982 film Q (film)|Q that he hates The Monster of Piedras Blancas because the monster is never seen until the climax. 
Leonard Maltin awarded the film 1  1/2  stars out of 4 calling the film "obvious and amateurish" also criticizing the films sluggish pacing.   
Allmovie gave the film a positive review stating, "a horror movie with a lot of familiar elements but just enough offbeat touches to keep viewers coming back for 50 years or more".  
TV Guide awarded the film 1  1/2  stars out of 4 calling it "A distinctly subpar effort". 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 