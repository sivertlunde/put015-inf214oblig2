Empty Nest (film)
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Empty Nest
| image          = 
| caption        = 
| director       = Daniel Burman
| producer       = Daniel Burman Diego Dubcovsky José María Morales
| writer         = Daniel Burman Oscar Martínez Cecilia Roth Inés Efron Arturo Goetz Jean Pierre Noher Eugenia Capizzano
| music          = Nico Cota Santiago Río Hinckelmann Hugo Colace
| editing        = Alejandro Brodersohn
| studio         = 
| distributor    =
| released       =  
| runtime        = 91 minutes
| country        = Argentina France Italy Spain
| language       = Spanish
| budget         =
| gross          =
}} Argentine drama Oscar Martínez and Cecilia Roth. 

== Plot == son in law Ianib leaves behind a book for him to read and asks for his opinion of it. Leonardo takes some time to actually read the book even though he carries it around for most of the film. When he finally reads the book, he is surprised to find out that it is brilliant. This scene is accompanied with a distinct use of lighting that focuses on Leonardo and his absorption within the novel.
	
The question of anti-Semitism is brought up in the film. Leonardo is questioned by one of his colleagues after a cartoon that he created is looked down upon as an anti-Semitic caricature of the Jewish people. Leonardo responds by saying, “Me, anti-Semitic? My daughter married an Israeli”. Leonardo takes his colleagues remarks quite lightly and soon travels to Israel with his wife to visit their daughter. It is during almost the end of the film that the viewer meets Julia and Ianib. Ianib wins a prestigious award for his novel and this serves as a bonding experience between the father and the Jewish son-in-law. Tensions that were built upon during most of the film are released with the time that Leonardo and Ianib spend together in Ianibs home country. Talk of children comes up during dinner and possible circumcision is mentioned. Ianib and Leonardo find something in common with each other, and while it may not be religion or language, it is their passion for writing. Ianib tells Leonardo, when his memory is questioned by Martha and Julia, “Family stories are always true”. The film closes with shots of the landscapes of Israel and a bonding moment between Martha and Leonardo before playing with chronological order once more, going back in time, to offer a scene with Leonardo and Julia talking after she comes home from a date with Ianib.

== References ==
 

==External links==
*  

 
 
 
 
 