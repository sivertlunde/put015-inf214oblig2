Zakhmi Kunku
{{multiple issues|
 
 
 
}}

{{Infobox film
| name           = Zakhmi Kunku - Marathi Movie
| image          = Zakhmi Kunku.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Ramesh Gangane
| producer       = Dinesh DeshpandeAtul Laulkar
| screenplay     = 
| story          = 
| starring       = Alka Kubal Ashok Shinde Rahul Solapurkar Varsha Usgaonkar Deepjyoti Naik
| music          = Dhananjay Dhumal
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Zakhmi Kunku is a Marathi movie released on 22 November 1995. The movie has been produced by Dinesh Deshpande along with Atul Laulkar and directed by Ramesh Gangane. The movie is a revenge saga of how a lone woman takes revenge of her husband’s death, who was brutally killed for a piece of land. 

== Synopsis ==
Nageshwar Patil is a self-proclaimed king, of a village that he terrorizes, and is feared by all. In the village live a happy couple, Namdev and his wife Durgabai. They are always there for the people who need their help. However, their blissful existence is suddenly brought to a halt when Nageshwar eyes a piece of land owned by Namdev.

Namdev is subsequently killed leaving behind a distraught widow Durgabai. Fear of invoking Nageshwars wrath; the villagers turn a deaf ear to her cries. Alone, devastated and unable to forgive the brutal killing of her husband, Durga now seeks vengeance. Like a wounded tigress, Durga sets out looking for Nageshwar armed with a rifle in one hand as she vows to avenge the killing of her soul-mate.

== Cast ==

The cast includes Alka Kubal, Ashok Shinde, Rahul Solapurkar, Varsha Usgaonkar, Deepjyoti Naik  & others.

==Soundtrack==
The music has been directed by Dhananjay Dhumal.

== References ==
 
 

== External links ==
*  

 
 
 


 