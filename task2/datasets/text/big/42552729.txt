Ondagona Baa
{{Infobox film
| name = Ondagona Baa
| image =
| caption = 
| director = K. R. Udhayashankar
| writer = K. R. Udhayashankar
| based on = Kalisundam Raa (2000)
| producer = Rockline Venkatesh
| starring = V. Ravichandran   Shilpa Shetty   Charan Raj 
| music = Hamsalekha
| cinematography = G. S. V. Seetharam
| editing = Jo Ni Harsha
| studio  = Rockline Productions
| released =  
| runtime = 155 minutes
| language = Kannada  
| country = India
}}
 Telugu blockbuster Venkatesh and Simran in the lead roles. The film also marked the reunion of actor Ravichandran with music composer Hamsalekha who had parted ways due to some differences for many years. 

The film created much hype before release due to the reunion of veterans on the screen after long time. After the release, the film was received with average response at the box-office while the critics appreciated the film for its family based theme and decent music. 

== Cast ==
* V. Ravichandran as Raghu
* Shilpa Shetty as Belli
* J. V. Somayajulu as Raghavayya
* K. R. Vijaya Tara
* Charan Raj
* Doddanna
* Mukhyamantri Chandru
* Bank Janardhan
* Vanitha Vasu
* Pavitra Lokesh
* Pramila Joshai
* Chitra Shenoy
* Tennis Krishna
* Mandeep Roy
* M. N. Lakshmidevi
* Sundar Raj
* Shivaram

== Soundtrack ==
All the songs are composed and written by Hamsalekha. 

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) || Lyrics
|- Nanditha || Hamsalekha
|-
| 2 || "Ond Hejje Naave" || Fayaz || Hamsalekha
|- Nanditha || Hamsalekha
|-
| 4 || "Ragi Mudde Murisi" || S. P. Balasubrahmanyam || Hamsalekha
|-
| 5 || "Lovvisu Nanna" || Rajesh Krishnan, Mahalaxmi Iyer || Hamsalekha
|- Hamsalekha
|-
| 7 || "Ajja Aalad Mara" || S. P. Balasubrahmanyam, K. S. Chithra || Hamsalekha
|-
| 8 || "Preethi Mounavagide" || Madhu Balakrishnan || Hamsalekha
|}

==Notes==
 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 


 

 