Secuestrados
{{Infobox film
| name           = Secuestrados
| image          = Secuestrados2010Poster.jpg
| caption        = Film poster
| director       = Miguel Ángel Vivas
| producer       =  
| writer         =  
| starring       =  
| music          = Sergio Moure
| cinematography = Pedro J. Márquez
| editing        = José Manuel Jiménez
| studio         =  
| distributor    = Filmax
| released       =  
| runtime        = 
| country        = Spain
| language       = Spanish, Albanian
| budget         = €100,000
| gross          =
}}
Secuestrados is a 2010 Spanish psychological thriller directed by Miguel Ángel Vivas and was written by Javier García. 

==Plot==
Jaime, his wife Marta and their daughter Isa have just moved into a new home. Isa has plans to attend a party with a boyfriend while Marta and Jaime are arguing about how to raise their daughter. In the midst of this, three masked Albanians break in. They are from the moving crew and in the film credits are listed as Head Thief, Young Thief, and Strong Thief.

Head Thief takes Jaime to a bank machine several miles away from home, while the other two stay with the mother and daughter. He tells Jaime that if there is any deviation from the plan, or if he tries to alert anyone to his predicament, he will call the house and have the other two kill Marta. Jaime takes the risk, and tries to convince a woman at the bank machine to call the police, but she believes hes trying to rob her, and gives him all her money. This causes Head Thief to call the house and tell Strong Thief to do whatever he wants to Marta. Jaime, hearing Martas screams over the phone, begs Head Thief to give him another chance. Head Thief agrees and takes Jaime to another bank machine further away.

Meanwhile César calls to pick up Isa for their date. Young Thief and Strong Thief pull him into the house, but find that Marta and Isa have fled to the basement and locked themselves in a back room. The men drag César downstairs and threaten to kill him unless the women open the door. When they dont, Strong Thief shoots César, injuring him, and breaks into the room with a sledgehammer, taking the women hostage again.

A security guard comes to the door to say that the neighbours have complained about the television being on too loud; he asks if he may come in and look around. Strong Thief pretends to be Martas husband and, when the guard grows suspicious, slits his throat. Strong Thief then decides to rape Isa. Marta offers herself in Isas place, but he laughs and breaks her arm. Young Thief, disgusted with all that has happened, pulls him off and the two men fight. When Strong Thief tries to rape Isa again, she grabs a statuette and crushes his head. Young Thief manages to talk her out of shooting him with Strong Thiefs gun and leaves.

While this is happening, Jaime and Head Thief are in the car, driving around. The idea is for the clock to turn to the next day, so that Jaime can take out more money from the bank. Instead, Jaime speeds up and runs the car into a telephone pole, knocking himself and Head Thief unconscious. When Jaime comes to, he takes Head Thiefs gun and returns home. He and Young Thief meet each other outside the house, but both are too traumatised and Young Thief escapes.

Jaime and Isa go to the basement and release Marta and César, who have been handcuffed together. Jaime gives the gun to Marta, then calls the police; while he is on the phone, Head Thief returns to the house and kills him with the sledgehammer. Marta tries to shoot Head Thief, but misses. He takes the gun from her and shoots her in the head. César comes up from the basement and Head Thief kills him too. Isa falls to her knees and stares at her mother in shock while he stabs her repeatedly in the stomach.

==Cast==
* Young Thief - Guillermo Barrientos
* Head Thief - Dritan Biba
* Strong Thief - Martijn Kuiper
* Jaime - Fernando Cayo
* Marta - Ana Wagener
* Isa - Manuela Vellés
* César - Xoel Yáñez
* Javier - Luis Iglesia
* Security Guard - Pepo Suevos
* Conductor - Eduardo Torroja
* Cash Register Clerk - Candela Fernández

==Production== Las Rozas.  The film is particularly well known for consisting of only 12 long takes.

==Release==
It premiered in early 2011 in Spain over Filmax.  The film had an UK and US release in spring 2011 as Kidnapped. 

==See also==

*List of films featuring home invasions

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 