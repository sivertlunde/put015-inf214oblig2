Ettore Fieramosca (1938 film)
{{Infobox film
| name = Ettore Fieramosca 
| image =
| image_size =
| caption =
| director = Alessandro Blasetti
| producer = Attilio Fattori   Vincenzo Genesi 
| writer = Massimo DAzeglio (novel)   Vittorio Nino Novarese   Augusto Mazzetti   Cesare Ludovici  Alessandro Blasetti
 | narrator =
| starring = Gino Cervi   Mario Ferrari   Elisa Cegani   Osvaldo Valenti
| music = Alessandro Cicognini 
| cinematography = Mario Albertelli   Václav Vích 
| editing = Ignazio Ferronetti   Alessandro Blasetti
| studio = Nembo Film  ENIC
| released = 29 December 1938 
| runtime = 93 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} novel of the same title by Massimo DAzeglio, based on the life of the 16th century condottiero Ettore Fieramosca. 

==Partial cast==
* Gino Cervi as Ettore Fieramosca 
* Mario Ferrari as Graiano dAsti 
* Elisa Cegani as Giovanna di Morreale 
* Osvaldo Valenti as Guy de la Motte 
* Lamberto Picasso as Prospero Colonna 
* Corrado Racca as Don Diego Garcia de Paredes 
* Clara Calamai as Fulvia 
* Umberto Sacripante as Franciotto 
* Gianni Pons as Il duca di Nemours 
* Carlo Duse as Jacopo, lo scudiero spia di Graiano  Mario Mazza as Fanfulla 
* Andrea Checchi as Gentilino

== References ==
 

== Bibliography ==
* Bondanella, Peter E. Italian Cinema: From Neorealism to the Present. Bloomsbury Publishing, 2001.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 