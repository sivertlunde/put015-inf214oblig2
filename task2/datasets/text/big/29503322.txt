Challenge to Be Free
{{Infobox film
| name           = Challenge to Be Free
| image          = ChallengeToBeFree-PosterArt.jpg
| image_size     = 150px
| caption        = Theatrical poster
| director       = Tay Garnett
| producer       = Chuck D. Keen
| based on       = 
| writer         = Anne Bosworth Chuck D. Keen
| narrator       = John McIntire
| starring       = Mike Mazurki
| music          = Ian Bernard
| cinematography = Chuck D. Keen
| editing        = 
| studio         = 
| distributor    = Pacific International Enterprises
| released       =  
| runtime        = 88 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Albert Johnson, the reputed "Mad Trapper of Rat River". 
 The Mad Trapper (1972),  a British made-for-television production.  A later fictionalized account, Death Hunt (1981), also based on the story of the  RCMP pursuit of  Albert Johnson, was directed by Peter R. Hunt, and starred Charles Bronson, Lee Marvin, Angie Dickinson and Carl Weathers.   IMDb. Retrieved: December 1, 2014. 

==Plot==
In Alaska, Trapper (Mike Mazurki) attempts to live in harmony with nature but is aware that other trappers are using inhumane traps. When he is confronted by rival trappers over his interference with their trap lines, they bring along Sargent (Fritz Ford) the local police officer. Feeling intimidated, Trapper fights back, shooting his way out of his cabin and embarking on a desperate attempt to escape the authorities.

==Cast==
  
* Mike Mazurki as Trapper
* Fritz Ford as Sargent
* Vic Christy as Frenchy
* Jimmy Kane as "Old Tracks"
* Alex Van Bibber as Great Rifleman
* Gordon Yardley as Supply Officer
   
* Bob McKinnon as Buck Dawson
* Roger Reitano as Eli Zane
* Ted Yardley as Officer Cabot
* Brian Russell as Storekeeper
* Connie Yardley as Housewife
* Patty Piper as Indian 
 

==Production==
Challenge to Be Free was filmed mainly on location in Alaska, as the locale of the "Mad Trapper" manhunt was changed from the Yukon to the United States.  As an American production, Johnsons character was changed to, simply, the  "Trapper". The theme song, "Trapper Man" was featured. 

==Reception==
Reviewer Leonard Maltin characterized Challenge to Be Free as being "... Simple, and simple-minded, best for younger viewers." 

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Anderson, Frank W. and Art Downs. The Death of Albert Johnson, Mad Trapper of Rat River. Surrey, British Columbia, Canada: Heritage House, 1986. ISBN 978-1-89438-403-2.
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
* North, Dick. The Mad Trapper of Rat River: A True Story of Canadas Biggest Manhunt. Toronto, Ontario, Canada: Macmillan Company, 1972. ISBN 978-1-59228-771-0.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 