Vennira Iravuggal
 
 
 
{{Infobox film
| name           = Vennira Iravugal-A Romantic Touch R.Perakas
| image          =
| caption        =
| director       = R. Perakas Rajaram
| producer       = R. Perakas Rajaram
| writer         = R. Perakas Rajaram
| starring       = Vikadakavi   Sangeeta Krishnasamy  Psychomantra   David Anthony
| music          = Lawrance Soosai
| cinematography = Mano V. Narayanan
| editing        = Kumarann.A / Tinesh Sarathi Krishnan
| studio         = Shine Entertainment
| distributor    = Shine Entertainment
| released       = 6 March 2014
| runtime        = 100 Minute
| country        = Malaysia
| language       = Tamil
| budget         = RM 1M
| gross          =
}}
 Malaysian Tamil romantic comedy film written, directed and produced by R.Perakas. Starring Vikadakavi in the lead role with Sangeeta Krishnasamy.   The film revolves around a journey of love which begins in Malaysia, continues to Singapore and finally ends in Myanmar with one objective, one mission and vision.
The film were set to released on 6 March 2014 at Malaysia, Singapore and Myanmar.

==Plot==
Vennira Iravuggal (duration: 100 minutes), is a story about a rich, seemingly irresponsible senior student, Ramesh at a university (shot on the campus of Universiti Sains Malaysia in Penang).Ramesh & his sidekick rag junior female students but Ramesh soon begins to have a different attitude to a freshie, Megala. They soon get together to the extent that she begins to have full faith in him in spite of her friends warning her to be careful. Ramesh suddenly vanishes taking Megala’s money by using her bank card & runs off to Myanmar. Megala goes in search of Ramesh, determined to get her money back.

Megala takes a (physical) journey (all the way to an impoverished Indian community in Myanmar). A logline that appears on the poster is:
When are you going to pay me back?
This is her personal objective in the film (one which is also paralled with
Ramesh’s father who’s also concerned with money).

In the process, Megala undergoes a transformation (one that is also bit by bit motivated by her remembrance of the past & her relationship with Ramesh at the university campus). Through her adventures & misadventures in Myanmar, Megala discovers that Ramesh is not all that he has made himself out to be. Another logline for the film is:
She came for money, she got her money but love conquered the money                                   The grammar may be a little loose but we get the message! At the end of the film, she demands the interest for the money owed to her by Ramesh but we know now
that it’s not motivated by love for the money but for Ramesh

==Cast==
* Magen @ Vikadavi as Ramesh@Senior Jagat
* Sangeeta Krishnasamy as Megala
* Psychomantra @ Krishna Kumar Lechmana as Ravi,Rameshs Collagemate
* David Anthony as College Professor
* Aruna as Sangeeta
* Kanchana
* Logan as Megalas Brother
* Janagaraj
* Sumathi
* Steel
* Larmin Andrea
* Mathi Alagan as Rameshs Father
* Ruben
* Dev Raj(hero)

==Soundtrack==
* Indre Pirivam&nbsp;— Charu,Lawrance
* Vaanavil&nbsp;— Andrea Jeremiah
* Kathale&nbsp;— Lawrence Soosai
* Sangi Mangi&nbsp;— Sheezay

==Awards==
* Norway Film Festival
** Official screening
* International Tamil Film Academy
** Special Mention 
The film were selected for a special screening in the Norway Tamil Film Festival.
Vaseeharan Sivalingam, the founder of the Norway Tamil Film Festival, mentioned that Vennira Iravuggal was selected to be screened as it was the first Tamil film to be filmed in Malaysia, Singapore and Myanmar and the screening of the film is likely to attract the interest of the Tamil Diaspora in Europe.
Has won all 5 Categories set at the Malaysian Kalai Ulagam Awards 2015 
Best Movie, Best Director, Best Cinematographer, Best Music Director & Best Actress

==References==
 

==External links==
* 

 

 
 
 
 
 
 