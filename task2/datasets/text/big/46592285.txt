As Long As I Live
{{Infobox film
| name = As Long As I Live
| image =
| image_size =
| caption =
| director = Jacques de Baroncelli
| producer = Carlo Bugiani   Lucien Masson   Alexandre Mnouchkine
| writer =  Pierre Brive   Jacques Companéez   Alex Joffé    Marc-Gilbert Sauvajon    Solange Térac
| narrator =
| starring = Edwige Feuillère   Jacques Berthier   Jean Debucourt
| music = Wal Berg   
| cinematography = Christian Matras     
| editing =     Paula Neurisse 
| studio = Les Films Ariane 
| distributor = Pathé
| released =   21 January 1946 
| runtime = 80 minutes
| country = France   Italy French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
As Long As I Live (French:Tant que je vivrai) is a 1946 French-Italian drama film directed by Jacques de Baroncelli and starring Edwige Feuillère, Jacques Berthier and Jean Debucourt.  The films sets were designed by the art director Guy de Gastyne. A wild-living woman on the run from the police falls in love with a consumptive pavement artist.

==Main cast==
*    Edwige Feuillère as Ariane 
* Jacques Berthier as Bernard Fleuret  
* Jean Debucourt as Jean Marail  
* Marguerite Deval as La marquise  
* Germaine Kerjean as Madame Levallois  
* Georges Lannes as Miguel Brennan  
* Margo Lion as Linfirmière  
* Germaine Michel as Aubergiste  
* Maurice Nasil as Jacquelin  
* Freddy Alberti as Band Leader  
* Pierre Juvenet as Le docteur Monnier

== References ==
 

== Bibliography ==
* Dayna Oscherwitz & MaryEllen Higgins. The A to Z of French Cinema. Scarecrow Press, 2009.

== External links ==
*  

 
 
 
 
 
 
 
 

 