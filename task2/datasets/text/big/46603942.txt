Aisa Yeh Jahaan
 
{{Infobox film
| name           = Aisa Yeh Jahaan
| image          = AisaYehJahaan.jpg
| alt            =
| caption        = Aisa Yeh Jahaan poster
| director       = Biswajeet Bora
| producer       = Maya Kholie
| writer         = Biswajeet Bora,  Ashok Mishra
| starring       =  
| editor         = Suresh Pai
| music          = Palash Sen - Euphoria
| cinematography = Anil Mehta
| editing        = Suresh Pai
| studio         = 
|
| released       =  
|
| country        = India
| language       = Hindi
}}

Aisa Yeh Jahaan is a 2015 India’s First Carbon Neutral Film directed by Biswajeet Bora and produced by by Maya Kholie. The film stars Palash Sen and Ira Dubey in lead roles, with Yashpal Shrama, Tinu Anand, Carol Gracias, Kymsleen Kholie, Prisha Dabbas and Saurabh V Pandey in supporting roles.  The film was release in May 2015.

==Plot==
 
 
The film is set in Mumbai. It depicts the relationship between a husband and wife, the ever growing human detachment with nature and people’s attitude towards the physiology of a child’s growth which is portrayed in a lighthearted yet satirical fashion. 

Rajib, works for a multinational company and is far apart in terms of his nature from his wife, Ananya,  who is a receptionist at a private firm. On the other hand, Kuhi, their three-year-old daughter, and Pakhi, the domestic help of the house, share a relationship based on innocence and pure emotions. Pakhi takes good care of Kuhi when Rajib and Ananya are away at work during the day.

The whole family plans a vacation to their hometown in Assam. There, Kuhi gets to know Nalia Kai, the helper of the house, who introduces her to a new world – a world of trees, birds, greenery, paddy fields and the environment at large. There she realizes the difference between the concrete world of Mumbai and the natural environment of her father’s hometown. She also fears that one day the people of Mumbai would not have air to breathe, as she learns that we get oxygen from trees. This compels her to question the scarcity of trees in Mumbai. 

== Cast ==

Palash Sen  - Mr Rajib Saikia

Ira Dubey  - Mrs Ananya Saikia

Yashpal Sharma - Nalia Kai (Domestic help of Mr. Saikias household)

Tinu Anand - Mr. Majid Khan (Director in the movie)

Carol Gracias  - Club Singer     
  
Kymsleen Kholie - Pakhi (domestic helper)

Prisha Dabbas - Kuhi Saikia (Rajib and Ananya’s child)

Saurabh V Pandey - Ron (Ananya’s cousin brother)

Bishnu Khargharia - Gunakanta Saikia ( Rajibs father)

== Production ==
Aisa Yeh Jahaan is a socio-environmental film that focuses on life and survival. Written and directed by Biswajeet Bora & produced by Maya Kholie under Kholie Entertainment, the shooting of the movie started on May 15, 2014 in Meghalaya and was completed on August 24, 2014.

==Filming==

This film is set majorly in Mumbai and partly in Assam & Meghalaya. Regions such as Meghalaya (Barapani Lake) and Golaghat District of Assam (No 1. Senchowa, Betioni, Bokaghat, Chinatolly & Aborghat) have contributed the most. 

The movie has a cast of 30 actors. The filming was completed in 45 days (including picturization of six songs).

==Supporting Clean Energy==

CERE (Center for Environmental Research and Education) established in 2002 by Dr. (Ms.) Rashnesh N. Pardiwala, an ecologist from University of Edinburgh, and Mrs. Kitayun Rustom, an environmental educationalist, is a pioneer in the field of corporate sustainability and carbon management systems. They help organizations map their carbon footprint, meet international reporting standards, and implement low-cost carbon reduction strategies.

CERE is conducting an audit based on certain parameters to measure the emissions associated with producing this film. Based on the audit, CERE will assist the production team of Aisa Yeh Jahaan to take steps to offset the carbon emitted through the entire production process through a massive afforestation program. Essentially, CERE will be helping the producers of Aisa Yeh Jahaan remove as much carbon dioxide (as trees absorb carbon dioxide) from the atmosphere as they have put in, making the film carbon-neutral. 

==Soundtrack==

Singer and composer, Dr. Palash Sen, the renowned singer from the much-loved Indian rock band Euphoria, has composed the music for Aisa Yeh Jahaan. 

==Promotional and Marketing==

The first look of the film was launched on 26 January 2015. As part of the promotional activities, they started with an official Facebook page  and a Twitter handle.  The official trailer of the film will be releasing on an event on (date) March/April 2015.

Their aim is to plant 500 saplings out of which 330 saplings have been planted. CERE has planted 230 trees in Bhiwandi, Mumbai. They have made an MOU with the villagers stating that they wont destroy the plants at least till the year 2030. 

== References ==
 

== External links ==

 
 
 