Why Me? (1985 film)
 
 
{{Infobox film
| name           = Why Me?
| image          = WhyMe.jpg
| image size     =
| caption        = Film poster
| film name      = {{Film name
| traditional    = 何必有我
| simplified     = 何必有我
| pinyin         = Hé Bì Yǒu Wǒ
| jyutping       = Ho4 Bit1 Jau2 Ngo2 }}
| director       = Kent Cheng Raymond Wong
| screenplay     = Raymond Wong Kent Cheng Philip Cheng
| narrator       =
| starring       = Chow Yun-fat  Kent Cheng Olivia Cheng
| music          = Chris Babida
| cinematography = 
| editing        = Wong Ming-lam Cinema City
| released       =  
| runtime        = 92 minutes
| country        = Hong Kong
| language       = Cantonese
| gross          = HK$7,060,507
| preceded by    =
| followed by    =
}}
Why Me? (何必有我) is a 1985 Hong Kong drama film directed by Kent Cheng.

==Cast==
* Kent Cheng as Fat Cat
* Olivia Cheng as Koko Cheng
* Chow Yun-fat as Mr. Chow
* Chiao Chiao as Fat Cats mother
* Paul Chu as Kokos father
* Tanny Tien as Kokos mother
* Au-yeung Sha-fei as Kokos grandmother	
* Karen Chan as Ms. Chan
* Eric Tsang as Eric
* Shing Fui-On as Jackson Ken
* Jamie Luk as Kens rascal
* Fofo Ma as Kens rascal
* Kara Hui as Woman whose husband gambles
* Lau Kar-wing as Man doing lion dance
* Annette Sam as IQ assessor
* Ng Siu-gong as Policeman at Village Wai
* Peter Lai as Police officer
*Wong Yue as Police office
* Yam Choi-bo as Family Service Depts staff
* Lung Tin-sang as Family Service Depts staff

==External links==
*  

 
 
 
 
 
 
 
 

 