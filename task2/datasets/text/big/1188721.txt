The Fighting Seabees
 
{{Infobox film
| name = The Fighting Seabees
| image = Fighting Seabees 1944.jpg
| caption = theatrical poster
| director = Edward Ludwig
| producer = Albert J. Cohen
| writer = Borden Chase Aeneas MacKenzie
| starring = John Wayne Susan Hayward Dennis OKeefe William Frawley
| music = Walter Scharf Roy Webb William Bradford
| editing = Richard Van Enger
| distributor = Republic Pictures
| released =  
| runtime = 100 minutes
| country = United States
| language = English
| budget = $1.5 million 
| gross =
}}
The Fighting Seabees is a 1944 war film starring John Wayne and Susan Hayward. The picture portrays a heavily fictionalized account of the dilemma that led to the creation of the U.S. Navys "Seabees" in World War II. The supporting cast includes Dennis OKeefe and William Frawley, and the movie was directed by Edward Ludwig.

==Plot==
Wedge Donovan (John Wayne) is a tough construction boss, building airstrips in the Pacific for the U.S. Navy during World War II. He clashes with his liaison officer, Lieutenant Commander Robert Yarrow (Dennis OKeefe), over the fact that his men are not allowed to arm themselves against the Empire of Japan|Japanese. When the enemy lands in force on the island, he finally takes matters into his own hands, leading his men into the fray. This prevents Yarrow from springing a carefully devised trap that would have wiped out the invaders in a murderous machinegun crossfire, with minimal American losses. Instead, many of Donovans men are killed unnecessarily.

As a result of this tragedy, Yarrow finally convinces the US Navy to form Construction Battalions (CBs, or the more familiar "Seabees") with Donovans assistance, despite their mutual romantic interest in war correspondent Constance Chesley (Susan Hayward). Donovan and many of his men enlist and receive formal military training.

The two men are teamed together on yet another island. The Japanese launch a major attack, which the Seabees barely manage to hold off, sometimes using heavy construction machinery such as bulldozers and a clamshell bucket. When word reaches Donovan of another approaching enemy column, there are no sailors left to oppose this new threat. In desperation, he rigs a bulldozer with explosives on its blade, intending to ram it into a petroleum storage tank. The plan works, sending a cascade of burning liquid into the path of the Japanese, who retreat in panic, right into the sights of waiting machine guns.

==Cast==
 
* John Wayne as Lt. Cmdr. Wedge Donovan
* Susan Hayward as Constance Chesley
* Dennis OKeefe as Lt. Cmdr. Robert Yarrow
* William Frawley as Eddie Powers
* Leonid Kinskey as Johnny Novasky
* J.M. Kerrigan as Sawyer Collins
* Grant Withers as Whanger Spreckles
* Paul Fix as Ding Jacobs
* Ben Welden as Yump Lumkin William Forrest as Lt. Tom Kerrick
* Addison Richards as Capt. Joyce
* Jay Norris as Joe Brick
* Duncan Renaldo as Construction worker at party
* Wally Wales as Lt. Cmdr. Hood
 
USN Composite Squadron 68 aerial and rearming sequences, Lt. Raymond Anderson, Flight Leader

==Production==
William Frawley later portrayed "Fred Mertz" in the television series I Love Lucy.

==See also==
* John Wayne filmography

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 


 