White as Snow (film)
{{Infobox film
| name           = White as Snow
| image          = WhiteAsSnowFilmPoster.jpg
| alt            =
| caption        = Theatrical poster
| director       = Selim Güneş
| producer       = Nur Güneş
| writer         = Selim Güneş   Sabahattin Ali
| starring       = Hakan Korkmaz   Sinem İslamoğlu   Gürkan Piri Onurlu   Kaya Akkaya   Ziver Armağan Açıl   Ruhan Odabaş   Sinan Koçal
| music          = Mırcan
| cinematography = Serdar Özdemir
| editing        = Ahmet Can Çakırca   Selim Güneş
| studio         = Agustos Film
| distributor    =
| released       =  
| runtime        = 82 mins
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          =
}}
White as Snow ( ) is a 2010 Turkish drama film written and directed by Selim Güneş based on the short story Ayran by Sabahattin Ali, which tells the story of a 9-year-old boy who must care for his younger siblings by selling hot drinks to weary winter travelers. The film was selected for the 46th Chicago International Film Festival and the 47th International Antalya Golden Orange Film Festival, where it premiered.

== Plot ==
Hasan is a twelve-year-old boy living with his two younger siblings in a mountain village in Turkey’s Eastern Black Sea region, struggling to survive. The family has been in dire poverty since Hasan’s father’s imprisonment. Hasan’s mother has had to take up work in town as a caretaker and Hasan has to sell ayran, the salty yogurt drink, to feed his siblings.

One cold winter day, Hasan goes to the teahouse by the road to sell ayran to travellers. The man who runs the place, Recep, is waiting to hear from his beloved Fatma whose family forced the two lovers apart. An old man, Kadir, is trying to sell his winter pears. A passenger in the approaching bus is on his way to his new post, a place he doesn’t want to go to...

== Release ==

=== Premiere ===
The film premiered in competition at the 47th Antalya "Golden Orange" International Film Festival on  October 12, 2010, at 14:30, in the AKM Aspendos Saloon.

=== Festival screenings ===
* 46th Chicago International Film Festival (October 7–21, 2010)   
* 47th Antalya "Golden Orange" International Film Festival (October 9–14, 2010)

== See also ==
* 2010 in film
* Turkish films of 2010

==References==
 

==External links==
*  
*  

 
 
 
 
 
 