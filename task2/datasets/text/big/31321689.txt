Moner Majhe Tumi
{{Infobox Film
| name           = Moner Majhe Tumi
| image          = Moner Majhe Tumi.jpg
| image_size     = 200px
| caption        = Movies Commercial Poster
| director       = K. Vasu
| producer       = Anandamela Chalochitra Ltd.
| writer         = M. S. Raju Riaz Purnima Purnima Jishu Sengupta Abbas Ullah Biplab Chatterjee Ali Azad Siraj Hayder Shankar Chakraborty Moumita Gupta Arpita Datt Mita Chaterjee Pallab
| music          = Devendranath Chaterjee
| cinematography = M V Ramkrihsna Rokibul Bari Chowdhury
| editing        = Majibur Rahman Dulu Shamrashib Rao Narsima Rao
| distributor    = Anandamela Chalochitra Ltd.
| released       = 2003
| runtime        = 150 Min.
| country        = Bangladesh India Bengali
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
| imdb_id        =
}}
Moner Majhe Tumi also ( ) is a Bangladesh &  India Joint Venture Bengali language movie.The movie is a remake of the Telugu movie Manasantha Nuvve.The movie was top grossing in Bangladesh for (2003).
 

==Cast== Riaz as Benu (Chintu) Purnima as Renu (Anu)
* Jishu Sengupta as Arun
* Abbas Ullah as
* Biplab Chatterjee as
* Ali Azad as
* Siraj Hayder as
* Shankar Chakraborty as
* Moumita Gupta as
* Arpita Datt as
* Mita Chaterjee as
* Pallab as
* Tanu Roy

==Crew==
* Producer: Anandamela Chalochitra Ltd.
* Story: M. S. Raju
* Screenplay: Motiur Rahman Panu
* Director: K. Vasu 
* Script: Motiur Rahman Panu
* Dialogue: Ali Azad
* Cinematography: M V Ramkrihsna and Rokibul Bari Chowdhury
* Editing: Majibur Rahman Dulu, Shamrashib Rao and Narsima Rao
* Music: Devendranath Chaterjee
* Lyrics: Priyo Chaterjee
* Sound: Mafizil Haque
* Distributor: Anandamela Chalochitra Ltd.

==Technical details==
* Format: 35 MM (Color)
* Running Time: 150 Minutes Bengali
* Country of Origin: Bangladesh and India
* Date of Theatrical Release: 2003
* Year of the Product: 2002
* Technical Support: Bangladesh Film Development Corporation (BFDC)

==Music==
{{Infobox album
| Name = Moner Majhe Tumi
| Type = Album
| Artist = Devendranath Chaterjee
| Cover = Moner Mahje Tumi 2.jpg
| Background = Gainsboro |
| Released = 2002 (Bangladesh)
| Genre = Soundtrack/Filmi
| Length =
| Label =
| Producer =ANUPAM
| Reviews =
| Last album = 
| This album = Moner Majhe Tumi (2002)
| Next album = 
|}}

===Soundtrack===
The songs and background score were composed by Devendranath Chatterjee, a veteran Bengali composer. All the songs were superhits. The song Akashe Batase was copied from a Malayalam song titled Kannadikkoodum Kootti.
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Songs !! Singers !! Lyrics !! Notes
 
|- 1
|Akashe Batase Chal Sathi Kavita Subramaniam|Kavita Krishnamurthy and Sadhana Sargam Priyo Chaterjee Kids Song
|- 2
|Akash Chhoa Swapna Asha Sadhana Sargam Shaan
|Priyo Chaterjee
|
|- 3
|Premi O Premi Udit Narayan and Sadhana Sargam Priyo Chaterjee Sad
|- 4
|Chupi Chupi Kichhi Kotha Kavita Subramaniam|Kavita Krishnamurthy Priyo Chaterjee
|
|- 5
|Premi O Premi
| Udit Narayan and Sadhana Sargam
| Priyo Chaterjee Title Song
|- 6
|Pran Kade Hay
| Kumar Sanu
| Priyo Chaterjee
| Part Song
|- 7
|Dukko Jala Kashtote
| Kumar Sanu
| Priyo Chaterjee
| Part Song
|- 8
|Jibine Koto Ritu Ashe Jay
| Kumar Sanu
| Part Song
|-
 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 