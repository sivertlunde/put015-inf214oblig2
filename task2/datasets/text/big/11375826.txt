Kampf um Norwegen – Feldzug 1940
{{Infobox film
| name = Kampf um Norwegen - Feldzug 1940
| image =
| image_size =
| caption =
| director = Martin Rikli Werner Buhre
| producer =
| writer =
| narrator =
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime = 81 minutes
| country = Nazi Germany
| language = German
| budget =
| preceded_by =
| followed_by =
| website =
}} Invasion of Denmark and Norway in the spring of 1940.
 
 

==Summary== Britain to invade the country, and attempts by the British to mine the leads along the Atlantic coast. When the Royal Navy invaded Norwegian waters to attack the German tanker Altmark and release prisoners held there by the Germans, it signalled an escalation of the growing crisis. The British prisoners had been captured by the  German cruiser Admiral Graf Spee during raids on merchant shipping in the Atlantic ocean and Indian ocean in the previous year.
The campaign itself opens with the attempt by the German Navy to force entry up the Oslo Fjord, and initially failed owing to Norwegian heavy guns either side of the fjord where it narrowed in the approach to Oslo itself.

==Preservation status== nitrate copy of the film surfaced on an Internet auction in 2005. The Norwegian college professor and media expert Jostein Saakvitne discovered this, and purchased the copy.   

Saakvitne then contacted the Norwegian Film Institute, and a consignment was entered into. The Film Institute had the film transferred to a video master, and sent the nitrate copy to the National Library nitrate film depository in Mo i Rana. As it became clear that the rights belonged to the Bundesarchiv, the film copy was brought back to Germany.

The movie can today be bought on DVD in Germany and Norway, and is, in any case, available as a free download from the Internet Archive.

==See also== List of films made in the Third Reich
*List of rediscovered films

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 