O Seeta Katha
{{Infobox film
| name           = O Seeta Katha
| image          =
| caption        =
| director       = K. Vishwanath
| producer       = Sharma C. Ashwini Dutt
| writer         = Gollapudi Maruti Rao
| narrator       = Chandra Mohan Shubha Ramaprabha Pandari Bai
| music          = K. V. Mahadevan
| cinematography =
| editing        =
| distributor    =
| released       = 1974
| runtime        =
| country        = India Telugu
| budget         =  |
| preceded_by    =
| followed_by    =
}} Indian feature directed by Malayalam and Tamil languages. Kamal Hassan played the antagonist in Malayalam version (Mattoru Seetha) while Rajnikanth played same role in Tamil version (Moondru Mudichu). 

==Plot==
Seeta (Roja Ramani), a teenage girl, lives with her mother and elder sister (Subha) who runs the house with her harikatha performances. Seeta falls in love with Chandram (Chandramohan), but evil Gopalakrishna (Devadas Kanakala) has an eye on her and hires goons to bash Chandram, who dies on the spot. Seeta marries Madhava Rao (Kantha Rao), father of Gopalakrishna, and makes Gopalakrishna realize his mistakes.

==Cast==
*Kanta Rao Chandra Mohan
*Roja Ramani
*Devadas Kanakala
*Allu Ramalingayya Shubha
*Ramaprabha
*Pandari Bai

==Awards==
;Nandi Awards
*Nandi Award for Best Feature Film (Silver) - 1974

;Filmfare Awards South
* Filmfare Best Director Award (Telugu) - 1975  
* Filmfare Best Film Award (Telugu) - 1975  

==Songs==
* bhaaratanaarii charitamu (harikatha)
** Lyric: Veturi Sundararama Murthy
** Playback: P. Leela

* malle kannaa tellana maa seeta sogasu
** Lyric: C. Narayana Reddy
** Playback: S. P. Balasubrahmanyam, P. Suseela

* puttaDi bomma maa peLLikoDuku
** Playback: S. P. Balasubrahmanyam, P. Suseela

* kallaakapaTam erugani pillalu allari chEstE andam
** Playback: P. Suseela

* ninu kanna katha, mee amma katha vinipinchanaa
** Playback: B. Vasanta, P. Suseela

* chintachiguru pulupani cheekaTanTE nalupani
** Playback: S. P. Balasubrahmanyam

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
! Box Office
|-
| 1975
| Mattoru Seetha
|Malayalam cinema|Malayalam
|Roja Ramani, M. G. Soman, Kamal Haasan
|P. Bhaskaran
| Hit
|-
| 1976
| Moondru Mudichu
|Tamil cinema|Tamil
|Sridevi, Kamal Hassan, Rajnikanth
|K. Balachander
| Super Hit
|-
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 