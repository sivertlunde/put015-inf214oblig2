NWF Kids Pro Wrestling: The Untold Story
{{multiple issues|
 
 
}}

 

{{Infobox film
| name           = NWF Kids Pro Wrestling: The Untold Story
| image          = NWF The Untold Story.jpg
| alt            = 
| caption        = DVD cover
| film name      =  
| director       = Shawn Crossen
| producer       = Shawn Crossen
| writer         = Shawn Crossen
| based on       =  
| starring       = See article  
| narrator       = Travis
| music          = 
| cinematography =  
| editing        = 
| studio         = NWF Films
| distributor    =  
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $18,000
| gross          = 
}}
 documentary that tells the story of a youth based professional wrestling league that existed in the mid-1980s.    It was released in the USA by NWF Films.       

==Plot==
The documentary (84 min) covers the birth and development of a professional style wrestling league from the mid-1980s that was produced by young teens aging from 12 to 16 years of age. The film covers the rise and fall of a unique wrestling experience for both the fans and kids that were involved.  The film includes footage from the original NWF productions as well as current interviews with past NWF participants.

==Cast==
Interviewed for the film were Charley Lane, Michael Ackermann, Matt Kelsey, Chris Hanson, Jason Clauson, John Hoffman, and Leslie Johnson

==Reception==
 

==Awards & nominations==
Since the film was released on DVD in October, 2005, It has received 13 national and international awards.     Among these are: 
*2005, won Best Sports Documentary Award at New York International Independent Film & Video Festival 
*2006, won ScreenCraft Award for Best Feature Documentary at New York International Independent Film & Video Festival 
*2006, won Aegis Award at Aegis Film and Television competition   Accolade Award honorable mention at Accolade Competition 
*2006, won 2 silver and 1 bronze Telly Award   

==References==
 

==External links==
*  

 
 
 
 
 
 

 