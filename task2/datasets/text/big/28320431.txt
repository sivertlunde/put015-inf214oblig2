Dr. Pasupathy
{{Infobox film
| name           = Dr. Pasupathy
| image          = Dr. Pasupathy.jpg
| image_size     = 
| caption        = 
| director       = Shaji Kailas
| producer       = 
| writer         = Ranji Panicker
| narrator       =  Innocent Parvathy Parvathy Rizabawa Johnson
| cinematography = Santosh Sivan
| editing        = L. Bhoominathan
| distributor    = Saga films
| released       = 1990
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Innocent played the lead role.It achieved a cult status in Kerala since its release.

== Plot ==
The comical film introduces us to a small village and its funny characters. The narrator (M. G. Soman in the prologue) informs of its developments and day-to-day activities. Soon, it slowly focus to the problems discussed in the Panchayat office (fights do occur between two factions in a comical way), in which the main problem is the absence of a veterinary doctor. The leaders of the opposition faction, Nanappan (Jagathy Sreekumar) and Uthpalakshan (Pappu), gives a final ultimatum to the Panchayat president Unnikannan Nair (Nedumudi Venu) to resign his post if he cant find a vet within days.

Unnikannan Nairs daughter Ammukutty (Parvathy) falls love with Pappen (Rizabawa). However Unnikannan Nair disapproves due to the rivalry of Pappens friends for The Panchayat President - Nanappan, Uthpalakshan and Society Balan (Jagadish). Also the animosity between Pappans Grandfather (Thikkurisi Sukumaran Nair) and Kunjulakshmi, Unnikannan Nairs mother (Philomina) adds fun to the tale. The romantic angle includes Society Balans romance with U. D. C. Kumari (Kalpana) who is also followed by Balans father and  Unnikannan Nairs associate Parameswara Kurup (Paravoor Bharathan).

While Parameswara Kurup travels to find a vet he encounters an old friend and thief, Bhairavan (Innocent) and encourages him to act. Thus Bhairavan is introduced in the village in his new avatar, Dr. Pasupathy.  Unnikannan Nair soon takes a liking to him and arranges Ammukuttys marriage. Heartbroken, Pappen soon leaves the village for work and finds solace and shelter from a friend who is Police Circle Inspector (Vijayaraghavan).

Soon when Dr. Pasupathys uncle, his associate in fact Velayudhan Kutty (Mammukoya), enters the village to see Bhairavan and joins his scam, its up to Pappens friends to call back Pappen to save Ammukutty.

== Cast == Innocent as Dr. Pasupathi / Bhairavan Parvathy as Ammukutty
* Nedumudi Venu as Unnikannan Nair
* Rizabawa as Pappen
* Jagathy Sreekumar as Nanappan
* Kuthiravattam Pappu as Uthpalakshan
* Jagadish as Society Balan
* Sreeja as Usha
* Paravoor Bharathan as Parameswara Kurup
* Thikkurissy Sukumaran Nair as Pappans Grandfather Kalpana as U. D. C. Kumari
*Meenakumari as Pappans Mother
* K.P.A.C. Lalitha as Chandramathi
* Philomina as Kunjulakshmi
* Mamukkoya as Velayudhan Kutty Vijayaraghavan as Thomas
* N. L. Balakrishnan as Govindan Nair
* Krishnan Kutty Nair as Nanu Nair Zainuddin as Kunjan Nair Kunchan as Gopalan

==External links==
* 

 
 
 
 
 

 