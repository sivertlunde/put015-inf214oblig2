Blackboard Jungle
{{Infobox film
| name = Blackboard Jungle
| image = Blackboardjungle.jpg
| caption = Theatrical release poster
| alt =
| director = Richard Brooks
| producer = Pandro S. Berman
| writer = Richard Brooks
| based on =  
| starring = Glenn Ford Anne Francis Louis Calhern Sidney Poitier
| music = Max C. Freedman, Jimmy DeKnight (song "Rock Around the Clock") (uncredited), Willis Holman (song “Blackboard Jungle”), Jenny Lou Carson (song "Let Me Go, Lover!" (uncredited)
| cinematography = Russell Harlan, ASC
| editing = Ferris Webster
| distributor = Metro-Goldwyn-Mayer (1955, original) Warner Bros. (2005, DVD)
| released =  
| runtime = 101 minutes
| country = United States
| language = English
| budget =$1,168,000  . 
| gross = $8,144,000 
}}
Blackboard Jungle is a 1955 social commentary film about teachers in an inner-city school. It is based on the novel of the same name by Evan Hunter.

==Plot==
Richard Dadier (Glenn Ford) is a new teacher at North Manual High School, an inner-city school where many of the pupils, led by student Gregory Miller (Sidney Poitier), frequently engage in anti-social behavior. Dadier makes various attempts to engage the students interest in education, challenging both the school staff and the pupils. He is subjected to violence as well as duplicitous schemes; he first suspects Miller, but later realizes that Artie West (Vic Morrow) is the perpetrator, and challenges him in a classroom showdown.

==Cultural impact==

===Music and teen culture===
The film marked the rock and roll revolution by featuring Bill Haley & His Comets "Rock Around the Clock",    initially a A-side and B-side|B-side, over the films opening credits (with a lengthy drum solo introduction, unlike the originally released single), as well as in the first scene, in an instrumental version in the middle of the film, and at the close of the movie, establishing that song as an instant classic. The record had been released the previous year, gaining only limited sales. But, popularized by its use in the film, "Rock Around the Clock" reached number one on the Billboard charts, and remained there for eight weeks.

In some theaters, when the film was on first release, the song was not heard at all at the beginning of the film because rock and roll was considered a bad influence. Despite this, other instances of the song were not cut.

The music led to a large teenage audience for the film, and their exuberant response to it sometimes overflowed into violence and vandalism at screenings.    In this sense, the film has been seen as marking the start of a period of visible teenage rebellion in the latter half of the 20th century.

The film marked  a watershed in the United Kingdom and was originally refused a cinema certificate before being passed with heavy cuts. When shown at a South London Cinema in Elephant and Castle in 1956 the teenage Teddy Boy audience began to riot, tearing up seats and dancing in the aisles.  After that, riots took place around the country wherever the film was shown.  In 2007, the Journal of Criminal Justice and Popular Culture published an article that analyzed the films connection to crime theories and juvenile delinquency.  

In March 2005, the 50th anniversary of the release of the film and the subsequent upsurge in popularity of rock and roll, was marked by a series of "Rock Is Fifty" celebrations in Los Angeles and New York City, involving the surviving members of the original Bill Haley & His Comets.  The film was released on DVD in North America on May 10, 2005 by Warner Home Video.

== Cast ==
 
* Glenn Ford as Richard Dadier
* Sidney Poitier as Gregory Miller
* Vic Morrow as Artie West
* Anne Francis as Anne Dadier
* Louis Calhern as Jim Murdock
* Margaret Hayes as Lois Hammond
* John Hoyt as Mr. Warneke
* Richard Kiley as Joshua Edwards
* Emile Meyer as Mr. Halloran
* Warner Anderson as Dr. Bradley
* Basil Ruysdael as Professor A. R. Kraal
* Dan Terranova as Belazi
* Rafael Campos as Pete V. Morales
* Paul Mazursky  as Emmanuel Stoker
* Horace McMahon as Detective Jameel Farah as Santini
 

This was the debut film for Campos, Morrow and Farah, and one of Poitiers earliest. Farah later changed his name to Jamie Farr (Cpl. Klinger from M*A*S*H (TV series)|M*A*S*H TV series.)

==Box Office==
According to MGM records the film earned $5,292,000 in the US and Canada and $2,852,000 elsewhere resulting in a profit of $4,392,000. 
==Awards== Academy Award Nominations: 
 Best Adapted Screenplay (Richard Brooks) Best Cimematography (Russell Harlan) Best Art Direction (Cedric Gibbons, Randall Duell, Edwin B. Willis, Henry Grace) Best Film Editing (Ferris Webster).   

==Honors== Turner Classic Movies (TCM) listed the soundtrack of the movie on its list of the Top 15 Most Influential Movie Soundtracks of all time. TCM described the impact and the influence of the movie:

 

== References ==
 
*   

==External links==
 
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 