Hard Hunted
{{Infobox film
| name           = Hard Hunted
| image          =
| caption        =
| director       = Andy Sidaris
| producer       = Arlene Sidaris
| writer         = Andy Sidaris
| narrator       =
| starring       = Dona Speir Roberta Vasquez Cynthia Brimhall  Bruce Penhall R.J. Moore Richard Lyons Mark Morris Craig Stewart
| distributor    = Malibu Bay Films
| released       =  
| runtime        = 97 minutes
| country        = United States English
| budget         =
}}
Hard Hunted is a 1992 action/adventure film starring Dona Speir, Roberta Vasquez,  Cynthia Brimhall, Bruce Penhall and Geoffrey Moore. It was directed and written by Andy Sidaris.

==The Triple B Series by Andy Sidaris==

*1. Malibu Express (1985)
*2. Hard Ticket to Hawaii  A.K.A.  Hard Ticket to Hawaii|Piège Mortel à Hawaï (1987)
*3. Picasso Trigger (1988)
*4. Savage Beach (1989) Guns (1990) Do or Die  A.K.A.  Do or Die (1991 film)|Girls, Games and Guns (1991)
*7. Hard Hunted (1992)
*8. Fit to Kill (1993)
*9. Enemy Gold (1993)
*10. The Dallas Connection (1994)
*11. Day of the Warrior (1996)
*12.   (1998)

==See also==
*Girls with guns

==External links==
* 
*  

 
 

 
 
 
 
 

 