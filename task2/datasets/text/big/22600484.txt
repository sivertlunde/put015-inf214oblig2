Team Picture
{{Infobox Film
| name           = Team Picture
| image          = Team Picture film poster.jpg
| image_size     = 
| caption        = Team Picture film poster
| director       = Kentucker Audley
| producer       = Brian Takats
| writer         = Kentucker Audley
| narrator       = 
| starring       = Andrew Nenninger Timothy Morton Amanda Harris Chellie Bowman Bill Baker
| music          = Ben Siler Kentucker Audley
| cinematography = Timothy Morton
| editing        = Kentuker Audley
| studio         = Benten Films
| distributor    = Benten Films
| released       = October 10, 2007
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 2007 mumblecore drama film written and directed by filmmaker Kentucker Audley. {{cite web
|url=http://www.filmmakermagazine.com/loadandplay/2008/09/team-picture.php
|title=Team Picture
|last=Guerrasio
|first=Jason
|date=September 7, 2008
|publisher=Filmmaker Magazine
|accessdate=May 29, 2009}}  {{cite web
|url=http://thefanzine.com/articles/film/291/real_escapism-_kentucker_audley_and_team_picture
|title=Real Escapism: Kentucker Audley and Team Picture
|last=Pera
|first=Brian
|date=November 3, 2008
|publisher=The Fanzine relationship with an ambitious girlfriend, his dealings with the familial and societal pressures to go to college, and his considerations of a future as a musician. {{cite news
|url=http://www.commercialappeal.com/news/2007/oct/20/audleys-debut-hits-screen-with-dead-on-accuracy/
|title=Audleys debut hits screen with dead-on accuracy
|last=Beifuss
|first=John
|date=October 20, 2007 
|publisher=Memphis Commercial Appeal|accessdate=2009-05-29}}  {{cite news
|url=http://www.memphisflyer.com/memphis/Content?oid=oid%3A35047
|title=Team Picture: Something Real
|last=Herrington
|first=Chris
|date=October 18, 2007
|publisher=Memphis Flyer
|accessdate=May 29, 2009}}  {{cite news
|url=http://www.commercialappeal.com/news/2007/aug/07/news-in-the-arts-elvis-film-co-stars-gathering/
|title=News in the Arts Elvis film co-stars gathering to recall the King: Local Hero
|last=Beifuss
|first=John
|date=August 7, 2007
|publisher=Memphis Commercial Appeal
|accessdate=May 29, 2009}} 

The filmmaker was named in August 2007 to Filmmaker Magazines annual list of "25 New Faces of Independent Film" and {{cite journal
|title=25 New Faces of Independent Film: Kentucker Audley
|journal=Filmmaker Magazine
|issue=Summer 2007|url=http://www.filmmakermagazine.com/summer2007/25faces.php
|accessdate=May 29, 2009}}  is considered an auteur.

==About the film==
Team Picture is about the life of a young man whose interactions at an ordinary job and with his family are a stark contrast to his bohemian home life with friends.  Fitting into the khaki pants and shirt tucked in demands of a job is an adjustment for a character apt to strum an acoustic guitar in cutoff shorts while wearing a straw hat by the backyard kiddie pool. The movie also deals with his romantic relationship with a girlfriend. 

It had its premiere at the Indie Memphis Film Festival on October 19, 2007,  and was released on DVD by Benten Films on August 26, 2008, {{cite web
|url=http://www.salon.com/ent/movies/btm/feature/2009/01/14/no_see_dvd/
|title=DVDs you should have seen in 2008, but didnt: "The Guatemalan Handshake," "The Free Will" and "Team Picture" |last=O’Hehir
|first=Andrew
|date=January 14, 2009
|publisher=Salon.com|accessdate=May 29, 2000}}  containing director commentary, a new epilogue to the film, a short by Audley and deleted scenes. 

==Critical reception==
Michael Atkinson of Independent Film Channel gave a mixed review of the film, summarizing, "Charming as it is, maybe like Jayasundaras film (The Forsaken Land), "Team Picture" isnt realism but rather a heightened Beckettian void". {{cite web
|url=http://www.ifc.com/news/2008/09/the-forsaken-land-team-picture.php
|title=On DVD: "The Forsaken Land," "Team Picture"
|last=Atkinson
|first=Michael
|publisher=Independent Film Channel
|accessdate=2009-05-29}}   John Beifuss of Memphis Commercial Appeal praised the film, writing that it was "the richest and most assured local feature in the festival", that it "may represent the most promising feature debut for a Memphis filmmaker", and that the "movies characters and situations are so recognizable and distressingly funny that they are likely to unnerve viewers who arent bored by the films lack of overt drama or puzzled by its home-video esthetic".   Conversely, Jennifer Aldoretta of The Technique|Technique panned the film as "mediocre and borderline terrible", noting only that writer/director Audley was "the only one involved who seems like he actually knew what he was doing."   Monika Bartyzel of Cinematical opined that the film, while "not for moviegoers looking for a fast-paced, tightly written story, Team Picture does have some charm as a sort of dead-pan voyeuristic look into modern slackers." {{cite web
|url=http://www.cinematical.com/2008/08/27/dvd-peek-slacker-indifference-in-team-picture/
|title=DVD Peek: Slacker Indifference in Team Picture
|last=Bartyzel 
|first=Monika
|date=Aug 27, 2008 
|publisher=Cinematical
|accessdate=May 20, 2009}}  Noel Megahey of DVD Times notes an autobiographical character to the film, noting that the films lead character is played by the writer-director  himself, and compliments by writing "its simple philosophy of taking time to find enjoyment is a sound one and it’s an honest sentiment that arises naturally out of the characters". {{cite web
|url=http://www.dvdtimes.co.uk/content.php?contentid=68607
|title=Team Picture
|last=Megahey
|first=Noel
|date=August 20, 2008
|publisher=DVD Times
|accessdate=May 29, 2009}}   Nick Dawson of Filmmaker Magazine opined that the film felt intimately real, writing "Nenningers   is scarily familiar, eschewing overly crafted Hollywood patter for the often comical idiosyncrasies of everyday speech.". 

==Synopsis==
Erik (Kentucker Audley) quits his job at his step dads (Greg Gaston) sporting goods store, mutually breaks up with his girlfriend Jessica (Shawna Wheeler) and meets a new girl Sarah (Amanda Harris). At a crossroads he decides to travel with Sarah to Chicago. His roommate (Timothy Morton) stays in Memphis to lounge at a baby pool in the front yard and perform at coffee shops.

==Cast==
*Kentucker Audley as David (as [Andrew Nenninger)
*Timothy Morton as Eric
*Amanda Harris as Sarah
*Bill Baker as Davids dad
*Greg Gaston as Davids step dad
*Chellie Bowman as Hilary
*Shawna Wheeler as Jessica
*Terry Hamilton as Davids mom
*Dana Terle as  Linda
*Cole Weintraub as McTyere

==Additional sources==
* " {{cite news
|url=http://thephoenix.com/Boston/Movies/42570-Notes-from-underground/
|title=Notes from underground: Celebrating independents at the HFA 
|last=Keough
|first=Peter
|date=June 27, 2007
|publisher=Boston Phoenix
|accessdate=May 29, 2009}} 

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 