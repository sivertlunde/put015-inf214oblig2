Double Fattiness
 
 
{{Infobox Film
| name           = Double Fattiness (雙肥臨門)
| director       = David Chiang
| starring       = Lydia Shum   Bill Tung   Eric Tsang   Maggie Cheung
| distributor    = D&B Films
| released       =  
| runtime        = Approx. 1 hour, 20 minutes
| country        =  	 Cantonese
| gross          = HK $12,719,337
}}

Double Fattiness (  film.

==Cast and roles==
* Bill Tung - Mo Jup Shu (武則書)
* Lydia Shum - Siu-Fung / Miss Choi
* Eric Tsang - Mo Dut Go
* Maggie Cheung - Diana
* Dennis Chan - Spiritworld Keeper Paul Chun - Kum Dai Tse James Wong - Orchestra Director
* David Chiang - 2nd Spiritworld Keeper
* Ronald Wong - Tses men
* Yip Wing-Cho - Mr. Lin

==Synopsis== Chinese flatbread restaurant in Hong Kong, where the Mo family worked and make a living. Siu-Fung (Lydia Shum) is the matriarch of the family, protecting them against all external threats from local gangsters, while also serving the familys every needs. However, Siu-Fung suffers from heart problems, and on the eve of her wedding anniversary with her longtime husband, Mo Jup Shu, Siu-Fung died.
 Chinese Wedding Sedan in the afterlife, which Jup Shu and his son, Dut Go, promises. When Siu-Fung reaches the underworld, the wedding sedan arrived just in time to carry Siu-Fung over the Neihe Bridge, where she will officially reach the underworld and be prepared for reincarnation. After proper registrations, Siu-Fung (who is grossly overweight) attempted to cross the Neihe Bridge with the sedan, and the excessive weight caused the bridge to collapse. The sedan carriers, along with the sedan, fell to their death into the Neihe River, while Siu-Fung was saved by the Spiritworld Keeper at the last moment.

Since the Neihe Bridge was destroyed, Siu-Fung cannot cross the bridge, thus creating an anomaly where Siu-Fung is not officially dead, in spiritworld terms. The spiritworld keeper attempted to guide Siu-Fung back to human life, where she can reenter her body, and be returned to life. However, Siu-Fung was a little late, and her husband and son pressed a button to cremate Siu-Fungs body.

 

==Character Naming and References==
In Cantonese, the name "Mo" (武) is homonymic to the term "no/nothing/never" (無), therefore, Mo Jup Shu (武則書) is phonetically similar to the term "無執輸", which means "Will not lose" or "will not be at a disadvantage". Meanwhile, Mo Dut Go (武德高)is phonetically similar to "無得高", which means "will never grow tall".

Paul Chuns character, Kum Dai Tse (金大枝) somewhat resembles the transliterated name of the former President of South Korea, Kim Dae-jung.

Mo Dut Gos purported English name, Charles (as revealed to Diana), is a reference to Charles, Prince of Wales, while Dianas name is a reference to Diana, Princess of Wales.

== External links ==
*  

 
 


 