There Ain't No Justice
{{Infobox film
| name           = There Aint No Justice
| image          = 
| image_size     = 
| caption        = 
| director       = Pen Tennyson
| producer       = Michael Balcon James Curtis   Sergei Nolbandov   Pen Tennyson
| narrator       =  Edward Chapman Michael Wilding
| music          = Ernest Irving
| cinematography = Mutz Greenbaum
| editing        = Ray Pitt
| studio         = Ealing Studios ABFD
| released       = June 1939
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
}} 1939 British sports drama Edward Chapman novel of James Curtis. Martin Knight. 

==Plot summary==
Tommy Mutch (Jimmy Hanley) is a garage mechanic and small time boxer. With his family in financial difficulty he needs to find money in a hurry. As luck would have it he meets boxing manager Sammy Sanders (Edward Chapman). Sammy assures Tommy he can get him lucrative main event bouts.

Tommy is promoted as the next boxing star which is reinforced with a series of convincing wins. However, Tommy discovers that the bouts were fixed by a gambling syndicate. He realises now that he has been set up by his manager and is expected to take a fall.

He has little choice but to go-ahead but needs to come up with a plan. One that will guarantee a financial return for his family while also hitting the syndicates in the pocket.

==Cast==
* Jimmy Hanley as Tommy Mutch 
* Edward Rigby as Pa Mutch 
* Mary Clare as Ma Mutch 
* Phyllis Stanley as Elsie Mutch  Edward Chapman as Sammy Sanders 
* Jill Furse as Connie Fletcher 
* Nan Hopkins as Dot Ducrow 
* Richard Ainley as Billy Frist 
* Gus McNaughton as Alfie Norton 
* Sue Gawthorne as Mrs. Frost 
* Michael Hogarth as Frank Fox  Michael Wilding as Len Charteris 
* Richard Norris as Stan 
* Al Millen as Perce  John Boxer as Mr Short James Knight as Police Constable

==Production== film of the same name. As with that adaptation he found himself having to remove areas of dialogue and story that would not get by the censors of the time. Many of these would be depictions of graphic violence against men rather than the sexual nature of his previous novel. 

This was the first film directed by Pen Tennyson, who had served as Assistant Director to Alfred Hitchcock from 1934. He would go on to direct two further films before being killed during World War II. 

The film features an uncredited role by real life boxer Bombardier Billy Wells|”Bombardier Billy Wells. 
He is best known for being the second gongman at the beginning of many Rank Organisation films, replacing Carl Dane.
 

==Release and reception==

It was released theatrically in the UK with the slogan “Real people, Real problems, a human document”. Due in part to its distinctive realistic portrayal of the boxing world it became a critical success. 
However, the author Graham Greene, having praised the previous years James Curtis adaptation (They Drive by Night), was not convinced. He considered the film to be timid and too refined in its depiction of the subject matter.
 

While not currently available on DVD, it is often shown at film revivals in both the US and UK. It was shown in May 2010 as part of BFI Southbanks “Capital Tales” season. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 