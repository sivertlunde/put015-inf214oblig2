Testimony (1988 film)
{{Infobox film
| name           = Testimony
| image          =
| caption        =
| director       = Tony Palmer
| producer       = Michael Kustow Grahame Jennings
| writer         = David Rudkin
| narrator       =
| starring       = Ben Kingsley
| music          = Dmitri Shostakovich London Philharmonic Orchestra
| cinematography = Nic Knowland
| editing        = Tony Palmer
| distributor    = Digital Classics DVD DVD 2006
| released       = November, 1988
| runtime        = 151 minutes
| country        = United Kingdom English
| budget         =
}} British musical musical drama film directed by Tony Palmer and starring Ben Kingsley, Sherry Baines and Robert Stephens. The film is based on the memoirs of Dmitri Shostakovich (1906&ndash;1975) as dictated in the book Testimony (book)|Testimony (edited by Solomon Volkov, ISBN 0-87910-021-4) and filmed in Panavision. Some consider the book to be a fabrication.

==Awards==
*Winner of the Gold Medal for Best Drama - New York International Film Festival
*Winner of the Fellini Prize - UNESCO
*Winner of the Critics Prize - São Paulo International Film Festival

==Cast==
*Ben Kingsley as Dmitri Shostakovich
*Sherry Baines as Nina Shostakovich
*Magdalen Asquith as Galya Shostakovich Mark Asquith as Maxim Shostakovich Stalin
*Ronald Tukhachevsky
*John Zhdanov
*Robert Robert Reynolds as Brutus
*Vernon Dobtcheff as Gargolovsky
*Colin Hurst as Stalin’s Secretary
*Joyce Grundy as Stalin’s Mother
*Mark Thrippleton as Young Stalin
*Liza Goddard as The English Humanist Glazunov
*Robert Meyerhold
*William Khachaturian
*Murray Melvin as The Film Editor Robert Urquhart as The Journalist
*Christopher Bramwell as Vanya
*Brook Williams as H. G. Wells
*Marita Phillips as Madam Lupinskaya
*Kenneth Holland  as pall bearer / grave digger

==Music==
The London Philharmonic Orchestra
*Leader: David Nolan
*Conductor: Rudolf Barshai

The Golden Age Singers
*Chorus Master: Simon Preston

Soloists: 
*Margaret Fingerhut
*Yuzuko Horigome
*John Shirley-Quirk
*Felicity Palmer
*Howard Shelley
*The Chilingrian Quartet

==See also==
*Stalinism
*Testimony (book)
*Solomon Volkov

==Further reading==
* Volkov, Solomon: Shostakovich and Stalin: The Extraordinary Relationship Between the Great Composer and the Brutal Dictator; Knopf 2004. ISBN 0-375-41082-1
* Fay, Laurel: Shostakovich versus Volkov: Whose Testimony? – The Russian Review, vol. 39 no. 4 October 1980 pp. 484–493.

==External links==
*  

 

 
 
 
 
 
 
 
 