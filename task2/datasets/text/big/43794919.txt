Wanted! Jane Turner
{{Infobox film
| name           = Wanted! Jane Turner
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Edward Killy Ed Donahue (assistant)
| producer       = Lee Marcus Cliff Reid
| writer         = 
| screenplay     = John Twist Edmund L. Hartmann
| story          = John Twist
| based on       =  
| starring       = Lee Tracy Gloria Stuart
| narrator       = 
| music          = 
| cinematography = Robert de Grasse
| editing        = Ted Cheesman RKO Radio Pictures
| distributor    = 
| released       =   |ref2= }}
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Wanted! Jane Turner is a 1936 American crime drama film directed by Edward Killy from a screenplay by Edmund L. Hartmann and John Twist, based on Twists story. Produced by RKO Radio Pictures, it was premiered in New York City on November 27, 1936, with a national release the following week on December 4.  The film stars Lee Tracy and Gloria Stuart, with an extensive supporting cast.

==References==
 

 

 
 


 