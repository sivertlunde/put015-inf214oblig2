Two Memories
 
{{Infobox film
| name           = Two Memories
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = D. W. Griffith
| starring       = Marion Leonard
| music          = 
| cinematography = G. W. Bitzer
| editing        = 
| distributor    = 
| released       =  
| runtime        = 3 minutes (one Reel#Motion picture terminology|reel)
| country        = United States
| language       = Silent
| budget         = 
}}
 silent short short drama film directed by D. W. Griffith. The film marks the onscreen debut of Mary Pickford.   

==Cast==
* Marion Leonard as Marion Francis David Miles as Henry Lawrence
* Mary Pickford as Marions Sister
* Charles Avery as Party Guest
* Clara T. Bracy
* John R. Cumpson as Party Guest
* Robert Harron
* Anita Hendrie as Party Guest
* Arthur V. Johnson as Party Guest
* Florence Lawrence as Party Guest
* Owen Moore as Party Guest
* Anthony OSullivan as Servant
* Lottie Pickford
* Herbert Prior as Party Guest
* Gertrude Robinson as Party Guest
* Mack Sennett as Party Guest

==See also==
* Mary Pickford filmography

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 