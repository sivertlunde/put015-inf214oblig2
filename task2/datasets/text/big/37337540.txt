Great Expectations: The Untold Story
 
{{Infobox film
| name           = Great Expectations: The Untold Story
| image          = 
| image size     =
| caption        = 
| director       = Tim Burstall
| producer       = Tom Burstall Ray Alchin executive Antony I. Ginnane associate Sigrid Thornton
| writer         = Tim Burstall
| based on = novel by Charles Dickens idea by Tom Burstall
| narrator       = John Stanton
| music          = George Dreyfus
| cinematography = Peter Hendry
| editing        = Tony Kavanaugh Lyn Solly John Pryce-Jones
| studio = Hemdale Film Corporation International Film Management
| distributor    = ABC (Australian TV) Filmpac (video)
| released       = 7 February 1987 (Australian TV)
| runtime        = 102 mins (film version) 3 x 2 hours (TV version)
| country        = Australia
| language       = English
| budget         = A $5,970,077 "Australian Productions Top $175 million", Cinema Papers, March 1986 p64 
| gross = 
| preceded by    =
| followed by    =
}}
Great Expectations: The Untold Story is a 1987 Australian film which was made as a feature film and a mini series. Ed. Scott Murray, Australia on the Small Screen 1970-1995, Oxford Uni Press, 1996 p70, 201-202  

It is based on an account of what happened to Magwitch from the novel Great Expectations when he was in Australia. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p187-188 
==Plot==
Magwitch is sentenced to life in New South Wales. He is put on a chain gang run by Solomon Tooth and eventually amasses a fortune.
==Cast== John Stanton as Magwitch
*Sigrid Thornton as Bridget
*Robert Coleby as Compeyson
*Anne Louise Lambert as Estella
*Noel Ferrier as Jaggers
*Ron Haddrick as Lankerton Young Pip Older Pip

==Production==
The film was the idea of Tom Burstall, who made it with his director father Tim. Tim Burstall says in the writing of it he was influenced by a book by Price Waring, Tales of the Old Convict System.   accessed 14 October 2012  It was filmed in Sydney from 9 March to 11 July 1986. 

==References==
 

==External links==
*  at IMDB
*  at TCMDB BFI
 
 
 

 
 
 
 
 

 