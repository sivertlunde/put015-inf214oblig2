Restoration (1995 film)
{{Infobox film
| name           = Restoration
| image          = Restauration1995.jpg
| caption        = DVD cover Michael Hoffman
| producer       = Cary Brokaw Andy Paterson
| writer         = Rupert Walters
| starring       = Robert Downey, Jr. Sam Neill Ian McKellen Hugh Grant David Thewlis Polly Walker Meg Ryan
| music          = James Newton Howard
| cinematography = Oliver Stapleton
| editing        = Garth Craven
| distributor    = Miramax Films
| released       =  
| runtime        = 118 min.
| country        = United States
| language       = English
| budget         = $19 million
| gross          = $4,005,941
}} historical drama Michael Hoffman. novel of the same title by Rose Tremain. It won two Academy Awards.   

==Plot== restoring the banishes him from court back to his life as a physician.

Merivel rejoins his old friend, John Pearce (David Thewlis), who has opened a Quaker Sanatorium|sanitarium. There, Merivel meets Katherine (Meg Ryan), a troubled young woman whose husband walked out on her after their daughter drowned in the river. Merivel and Katherine become lovers. Pearce falls fatally ill with tuberculosis|consumption, and while Merivel is tending to his dying friend, they then discover that Katherine is pregnant with Merivels child, and after the death of Pearce, Merivel and Katherine leave.
 Great Plague has hit. Katherine gives birth to a daughter, Margaret, via Caesarean section, but Katherine dies in the process as there is no way to ward off infection once the body has been cut open. In her dying moments, Merivel promises Katherine that he will care for Margaret, and that he loves Katherine.

As the plague continues to kill the people of London, Merivel feels compelled to do what he can as a physician. He leaves Margaret with a midwife who promises to care for her in his absence, then he goes out into the city, separating the sick from the well, who have all been quarantined together, and does what he can to ease the suffering of the dying. When someone asks for Merivels name, he says he is John Pearce as a tribute to his friend. Under this misnomer and in disguise, Merivel is once again summoned to the palace. The King fears that Celia has contracted the plague, but Merivel soon assures him that she does not have the plague, but rather has a treatable fever and is with child. With this, Merivel realizes the life he has now is more rewarding and fulfilling than the life and loves at the court he left behind.
 city is row boat, unconscious, and is floated by the river current away from the city. When he awakens, he is being cared for by Will Gates back at Bidnold, the estate the King had given to him at his marriage of Celia, and was then taken away. As Merivel recovers from his fall, he cannot recover from his failure to protect his young daughter from harm, when suddenly the King arrives at the house with his entourage. He informs Merivel that he has discovered the doctors true identity, and that he was impressed with the man Merivel had become. With that, the King steps aside to reveal a nurse holding Margaret, Merivels daughter, safely in her arms. For his courage and good work done in treating the victims of the plague, the King once again gives Bidnold to Merivel, stating that this time it will never be taken away. The film ends with Merivel returning to London, to set up a new hospital with help from the King.

==Cast==
* Robert Downey, Jr. as Robert Merivel
* Sam Neill as King Charles II
* David Thewlis as John Pearce
* Polly Walker as Celia Clemence
* Meg Ryan as Katharine
* Ian McKellen as Will Gates
* Hugh Grant as Elias Finn
* Ian McDiarmid as Ambrose
* Mary MacLeod as Midwife
* Mark Letheren as Daniel
* Sandy McDade as Hannah
* Rosalind Bennett as Eleanor
* Willie Ross  as Man with Visible Heart
* Bridie & Hannah Rees-Davies as Twin Babies

==Reception== Best Art Best Costume Design for James Acheson.  The film was also entered into the 46th Berlin International Film Festival.    It currently holds a 69% rating on Rotten Tomatoes from 32 critics.
 AIDS parable and described Ryan as miscast in the role of the troubled Katherine.  In her review for New York Times, Janet Maslin wrote, "Restoration crams in more research and period detail than it can comfortably digest, but its story is not overwhelmed by such overkill". 

Rose Tremain, author of the novel on which the film was based, said of the film that it had a beautiful texture to it. She was however disappointed with the films storytelling, and said the story has no logic and so does not move the audience. The disappointment led her to take up scriptwriting herself.  

==Soundtrack==
{{Infobox album 
| Name        = Restoration: Music From The Miramax Motion Picture Soundtrack 
| Type        = Soundtrack
| Artist      = James Newton Howard
| Cover       =
| Released    = 1996
| Recorded    =
| Genre       = Soundtrack
| Length      = 58:47
| Label       = Milan Records
| Producer    = Michael Mason, Shawn Murphy
| Last album  =
| This album  =
| Next album  =
}}
Composer James Newton Howards main theme is based on the music from The Fairy-Queen by Henry Purcell.

===Track listing===
# "If Loves A Sweet Passion (From the Fairy Queen)" 1:31
# "Main Titles" 2:57
# "Frost Dance In C (From King Arthur)" 1:34
# "A Night With Lulu" 1:21
# "Minuet In G (From Abdelazer)" 0:53
# "Here The Deities Approve (From the Ode welcome to all the Pleasures)" 2:28
# "A Creature Of The New Age" 1:09
# "Overture In D (From the Fairy Queen)" 1:26
# "The Wedding" 1:39
# "Hornpipe In D Minor (From the Fairy Queen)" 1:26
# "Arrival In Bidnold" 1:08
# "The Cabinet Of Curiosities" 2:54
# "The Land Of Mar" 1:11
# "The Lie" 1:19
# "A New Ground (In E Minor)" 0:52
# "Merivel Woos Celia" 2:26
# "Katharine Sleeps" 3:23
# "Taking Bidnold Back" 1:35
# "Muzette 1 In A Minor (From 3e Livre De Pieces De Viole)" 2:56
# "The Right Knowledge" 2:06
# "The Plague" 2:09
# "Katharines Death" 4:37
# "Night Sweats" 3:03
# "Hospital" 2:54
# "Doctor Merivel" 1:50
# "Listening To Celias Heart" 1:39
# "The Fire" 3:18
# "Allegro From Sinfonia (Act II) (From the Indian Queen)" 1:19
# "Your Child I Believe" 1:13
# "Newcastle (Traditional)" 0:38
# "2nd Overture In D (From King Arthur)" 1:27

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 