Vamsanikokkadu
{{Infobox film
| name           = Vamsanikokkadu
| image          =
| caption        =
| writer         = Paruchuri Brothers  
| screenplay     = Sarath
| producer       = Smt Anitha Krishna
| director       = Sarath
| starring       = Nandamuri Balakrishna Ramya Krishna Aamani Koti
| cinematography = V. S. R. Swamy
| editing        = Kotagiri Venkateswara Rao
| studio         = Sridevi Movies
| released       =  
| runtime        = 138 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Koti      

==Cast==
 
*Nandamuri Balakrishna as Raja
*Ramya Krishna as Radha Bai
*Aamani as Sirisha Satyanarayana as Chakrapani
*Kota Srinivasa Rao as Kotilingam
*Brahmanandam as Appaji
*Babu Mohan as Vinayakam Mallikarjuna Rao as Chidambaram
*Tanikella Bharani as Yadagiri
*Rakhee as Giri
*Chalapathi Rao as Manager
*Vijaya Rangaraju as Rowdy
*Mannava Balayya|M.Balaiyah as Ranga Rao
*Raja Ravindra as Murali
*Sanjay Asrani as Bhaskar
*Sivaji Raja as Vishnu
*Kota Shankar Rao as Lawyer
*Bhemiswara Rao as Judge
*Jaya Bhaskar as Lawyer
*Narsingh Yadav as Narsingh
*Jayanthi as Lilavathi Annapurna as Lakshmi 
*Aruna Sri as Nirmala
*Madhurima as Rukmini
*Jayalalita as Ammaji 
*Kalpana Rai 
*Y.Vijaya as Andallu
 

==Soundtrack==
{{Infobox album
| Name        = Vamsanikokkadu
| Tagline     = 
| Type        = film Koti
| Cover       = 
| Released    = 1995
| Recorded    = 
| Genre       = Soundtrack
| Length      = 29:52
| Label       = Supreme Music Koti
| Reviews     =
| Last album  = Subhamasthu   (1995)  
| This album  = Vamsanikokkadu   (1996)
| Next album  = Saradha Bullodu   (1996)
}}

The music was composed by Saluri Koteswara Rao. The soundtrack was released by the Supreme Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 29:52
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Saradaga Samayam  Sirivennela Sitarama Sastry SP Balu
| length1 = 5:07

| title2  = Dandalo Dandamandi 
| lyrics2 = Bhuvanachandra Chitra
| length2 = 5:12

| title3  = Valachi Valachi Vaastayana
| lyrics3 = Veturi Sundararama Murthy
| extra3  = SP Balu,Chitra
| length3 = 4:50

| title4  = Abbadani Soku Thaluku 
| lyrics4 = Veturi Sundararama Murthy
| extra4  = SP Balu,Chitra
| length4 = 5:06

| title5  = Priya Mahashaya 
| lyrics5 = Veturi Sundararama Murthy  
| extra5  = SP Balu,Chitra
| length5 = 4:54

| title6  = Yabba Neevalu Kallu
| lyrics6 = Bhuvanachandra  
| extra6  = SP Balu,Chitra
| length6 = 4:43
}}

==Others==
* VCDs and DVDs were released by VOLGA Videos, Hyderabad.

==References==
 

 
 
 


 