The World Intellectuals
The World Intellectuals is a series by Mahmoud Shoolizadeh, a part of the life and belief of some of the intellectuals in art and literature are shown, The life of Johann Wolfgang von Goethe, the famous German poet, who was very much influenced by the famous Iranian poet Hafez, also, the effective life and word of George Bernard Shaw, the British writer, are among them.
 ]]

A 40-minute episode is about the Great German poet, Johann Wolfgang von Goethe which was broadcast from the main channel of Iran TV and several film festivals. Goethe, writer and poet of late 18th century, who knew five languages, was inspired by Persia and specially Hafez, Iranian poet. Goethe then wrote the “West-Eastern Divan” which was a collection of poems, and he devoted it to Hafez. In this film Goethe’s poetic and passionate life, and also his love and relations with Hafez is shown. Goethe lived for 83 years and he was a great intellectual in literature and has a key role in German Literature and Romanticism. Hafez’s sonnets had a magic influence on Goethe and he used to call Hafez ”A Devine Poet” and he also said he couldn’t resist in front of this Great Phenomenon of the Mankind in poetry (Hafez). It was his love towards Hafez that made one of the most beautiful poetic works: “West-Eastern Divan” which consists of twelve books with subjects such as love and paradise and connection with Persian culture in a unique and beautiful way. Goethe borrowed some of his poem’s contexts and subjects from Hafez’s poems: "The beauty of God is reflected in His beautiful creatures, and at last shows itself in Human’s beauty. And the Human’s spirit that had been separated from his heavenly creator and is in danger of being forgotten, is now searching for the beauty of the God."

==Technical specifications and Film crew==
 

The World Intellectuals
*Betacam sp, 40mins, fiction, Iran, 1990
*Director, Researcher and Script writer: Mahmoud Shoolizadeh,
*Photograph: Mohammad Taheri Jam,
*Voice: behrooz Moavenian,
*Make-up: Farhang Moayeri,
*Art and Dress Design: Mahmoud Shoolizadeh
*Edit: Hossain Maher
*Producer: Mehdi Rajai ( I.R.I.B, Ch1 )

 
 
 

 