Korkoro
 
 
{{Infobox film
| name = Korkoro
| italic title = force
| image = Korkoro (DVD Cover).jpg
| alt = DVD cover for the movie, with a lady in Roma attire in the foreground, her face partially outside the frame, we can see her lips, chin and nose. In the background can be seen three men: one with grey hair, one with long black hair and the last with a hat on. The man with grey hair is shown about to jump. There is also a woman in the background, with not much of her in focus, except that she is wearing purple ethnic clothing.
| caption = American DVD cover
| director = Tony Gatlif
| producer = Tony Gatlif
| writer = Tony Gatlif
| starring = Marc Lavoine Marie-Josée Croze James Thiérrée
| music = Delphine Mantoulet Tony Gatlif
| cinematography = Julien Hirsch
| editing = Monique Darton
| studio = Production Princes France 3 Cinema Rhone-Alpes Cinema
| distributor = UGC Distribution
| released =  
| runtime = 111 minutes
| country = France
| language = French Romani
| budget =
}}

Korkoro ("Freedom" in the Romani language) is a 2009 French drama film written and directed by Tony Gatlif, starring French actors Marc Lavoine, Marie-Josée Croze and James Thiérrée. The films cast were of many nationalities such as Albanian, Kosovar, Georgian, Serbian, French, Norwegian, and nine Romani Gatlif recruited in Transylvania.
 Second World Romani (Gypsy) historian Jacques Sigot, the film was inspired by a Romani who escaped the Nazis with help from French villagers. It depicts the rarely documented subject of Porajmos (the Romani Holocaust).  Other than a band of Romani people, the film has a character based on Yvette Lundy, a French teacher who was active in the French resistance and deported to a concentration camp for forging passports for Romani. Gatlif had intended to make a documentary but the lack of supporting documents led him to present it as a drama.

The film premiered at the Montréal World Film Festival, winning the Grand Prize of the Americas, amongst other awards.  It was released in France as Liberté in February 2010, where it grossed $601,252; revenues from Belgium and the United States brought the total to $627,088.  The films music, composed by Tony Gatlif and Delphine Mantoulet, received a nomination in the Best Music Written for a Film category at the 36th annual César Awards 2011|César Awards.

Korkoro has been described as a "rare cinematic tribute" to those killed in the Porajmos.    In general, it received positive reviews from critics, including praise for having an unusually leisurely pace for a Holocaust film.  Critics regarded it as one of the directors best works, and with Latcho Drom, the "most accessible" of his films. The film is considered to show Romani in a non-stereotypical way, far from their clichéd depictions as musicians.

== Plot == Romani Caravan (travellers)|caravan, an extended family of 20 men, women and children, who decide to adopt him. The Romani start calling Claude, Korkoro, the free one. Fascinated by their nomadic lifestyle, Claude decides to stay with them. 

The caravan sets up camp outside a small wine-growing village, hoping to find seasonal work in the vineyards and a place to sell their wares. The village, as was the trend, is divided into two factions—one welcomes the Romanies, and the other sees them as an intrusion. Théodore Rosier (Marc Lavoine), the village mayor and  veterinarian, and Mademoiselle Lundi (Marie-Josée Croze), a school teacher and clerk in city hall, are two of the friendlier villagers. The Vichy France gendarmerie used the documentation made in the passports of its citizens to monitor their movements for which a threshold was set, along with imprisonment for violations This adversely affected the Romanies. Lundi uses her powers as a clerk, and forges their passports, removing the documentation about their movements.   
 Fascist policy of imprisoning the homeless. Lundi enrolls the children in her school. The freedom-loving Romani recognize that these French are trying to help but struggled with life in a fixed place and the rules of formal education.
 concentration camps. Claude, cared for by Rosier, chooses to go with the Romani.   

== Production ==

=== Background ===
 

During World War II, the Porajmos was the attempt by Nazi Germany, the Independent State of Croatia, Horthys Hungary and their allies to exterminate the Romani people of Europe.    Under Hitler’s rule, both Romanies and Jews were defined as "enemies of the race-based state" by the Nuremberg Laws; the two groups were targeted by similar policies and persecution, culminating in the near annihilation of both populations in Nazi-occupied countries.  Estimates of the death toll of Romanies in World War II range from 220,000 to 1,500,000.   

Because Eastern European Romani communities were less organised than Jewish communities, Porajmos was not well documented. There also existed a trend to downplay the actual figures, according to Ian Hancock, director of the Program of Romani Studies at the University of Texas at Austin.  Tony Gatlif, whose films mostly have Romanies as subjects, had long wanted to make a documentary on this less well-known subject, but the lack of enough documented evidence coupled with the absence of accurate pre-war census figures for the Romanies made it difficult. 
 

=== Development === Montreuil to interview the Romanies there who refused to discuss the subject. Gatlif was also researching the Righteous among the Nations|Justes, the French who attempted to shield the Romanies from persecution.    Following former French President Jacques Chiracs efforts to honour the Justes,    Gatlif came across Yvette Lundy,  a former schoolteacher in Gionges, La Marne, who had been deported for forging documents for the Romanies.    Gatlif came across an anecdote by Jacques Sigot, a historian who has documented the Porajmos,    which would later help with the story.  The anecdote is about a Romani family saved from being sent to the camp at Montreuil-Bellay by a French lawyer who sold them his home for a single French franc|franc. Unable to adjust to a stationary lifestyle, the family took to the streets, which led to their arrest in northern France and eventual incarceration in the Auschwitz concentration camp. 
   Front de Libération Nationale (National Liberation Front). 

Intended to be a documentary, Korkoro became a drama because of  the lack of sufficient supporting documents. Gatlif wrote the initial script in one month; further modifications later followed which made the films style a narrative by the characters Rosier and Lundi. Gatlif used Lundys help to write the scenes related to her,  to which he added his  own experiences with his teacher. The first appearance of the Romanies in the film is inspired by the way the nomadic Romanies showed up in the middle of nowhere after Gatlif had been working on the characterisation for over a year. Another year was spent in developing Taloches character. 

{{multiple image
| align     = right
| direction = vertical
| width     = 120
| image3    =Marc Lavoine.jpg
| alt3      = Face shot of a man with light eyes and close cut hair.
| image2    =Marie-Josée Croze au Défilé Channel, Printemps-Eté 2010.jpg
| alt2      = Mugshot of a woman with blonde hair and grey eyes. She is smiling; we can see her dimples.
| image1    =  James Thierrée.jpg
|alt1=A man is shown seated on a chair, enacting a drama scene, with his hands below his face, and his eyes closed. His clothes are torn and we can see a belly fold on his rather flat and smooth tummy including his navel.
|footer=Principal cast : James Thiérrée (top), Marie-Josée Croze(middle) and Marc Lavoine(bottom) 
|footer_align=center
}}

=== Casting === Romani and Gypsy swing music in six months. 

For Théodore Rosier, Gatlif wanted someone who looked like a typical Frenchman of the time,  with a "voice and face a little like that of Pierre Fresnay, Maurice Ronet, Jacques Charrier or Gérard Philippe", which he found in Marc Lavoine.  Marie-Josée Croze was the obvious choice for Mademoiselle Lise Lundi. Gatlif had envisioned Lundi as being like a "Hitchcock character: fragile, mysterious and strong". 
 Rufus was chosen by Gatlif for the role of Fernand because of his typical French looks. Puri Dai, the grandmother, was played by Raya Bielenberg, a Soviet-born Norwegian artist and 2005 recipient of Oslo City art award, who uses music and dance in an effort to make the Romani culture better known in Norway.  Gatlif found her in Oslo.  The other notable characters in the movie, Darko, Kako, Chavo, Zanko and Tatane were played by Arben Bajraktaraj, Georges Babluani, Ilijir Selimoski, Kevyn Diana and Thomas Baumgartner respectively.  A minor character named Levis was played by then 11-year-old great-grandson of Django Reinhardt, a virtuoso jazz guitarist and composer of Manouche gypsy ethnicity.    The cast included people of many nationalities, Albanian, Kosovar, Georgian, Serbian, French and Norwegian along with the nine Romanies Gatlif found living in extreme poverty in Transylvania.  Arrangements were made for these Romanies to stay in France for the three to four months it took to shoot the film. 

=== Filming ===
The film was shot in Loire, in the Monts du Forez, Rozier-Côtes-dAurec and Saint-Bonnet-le-Château.    The tools used in the movie, which were very similar to the ones employed in 1943, came from Transylvania. The barbed wire fences of the concentration camps are genuine ones built by the Nazis in Romania which can be differentiated from the ones used for cattle by their denser spacing. 

The male actors were asked to grow their hair and moustaches. The actors also had to diet to lose weight to achieve the look of World War II characters.  The costumes had a faded look, a reflection that people of the period owned few clothes, often only two outfits. None of the actors knew the script in advance and were only informed each night before of what they were to do in their daily scenes. The Romanies were not aware of the historic events that were the basis of the movie, and were only told that the story was set in hard times comparable to Nicolae Ceaușescu|Ceaușescus tenure in Romania.  In the scene where the Romanies revolt against the police over the death of Taloche, they were made aware of the characters death only when the scene was being shot, leading to a genuine outpouring of emotions, making their fight with the police appear more real. Gatlif later remarked in an interview that this scene stands for the actual revolt  by the Romanies in Auschwitz on May 16, 1944.   

Thierrée was the only actor allowed to improvise. His characterisation of Taloche was built on spontaneity, and in many instances, Gatlif had no clue how he would act in a scene, such as in  the tap scene in which he plunges into a stairwell. In another scene, in which he dances with war music in the background, Thierrée pretended to make love to the earth like an animal. Gatlif, who had wanted the character to have the ability to  sense forthcoming danger, like animals often do, stated that Thierrée was suitable  for the role because he is very much an animal. The dance scene where Taloche is shown falling from a tree was done without stunt doubles.   
 

=== Music ===
{{Infobox album  
| Name       = Liberté
| Type       = soundtrack
| Longtype   = Korkoro
| Artist     = Various
| Cover      = 
| Alt        = 
| Released   =   
| Recorded   = 
| Genre      = Soundtrack
| Length     = 
| Language   = French
| Label      = Universal France
| Producer   = Tony Gatlif
| Last album = 
| This album = 
| Next album = 
}}
Music plays a very important role in all Gatlifs films, such as   in line with the opening lines of the screenplay, "the barbed wire sings in the wind",  to the oddest tools used to make music, such as the clanking of buckets and wagon wheels. 

The background score was composed by Tony Gatlif and Delphine Mantoulet. The main theme of the songs is the Romani association with France. Despite the sad story, there are cheerful tracks too, with pieces for the waltz, tarantella and Java (dance)|java. The films music plays a prominent role from the opening credits to Catherine Ringers track in the closing credits, "Les Bohemians", a waltz piece written by Gatlif and Mantoulet, which is described as setting the tone for the film.     "Les Bohemians" is the first French song ever featured in a Gatlif movie. Gatlif chose Ringer for the track, inspired by the "blood in the mouth" feel to her voice. The song translates as "Good luck to you all, if anyone worries that we’re gone, tell them we’ve been thrown from the light and the sky, we the lords of this vast universe."  The java dance piece composed by Delphine accompanies a scene where the characters secretly congregate in a barn for dancing, signifying the scenario then when public gatherings were prohibited.  The track "Un Poulailler A La Bastilles", sung by Gatlifs son Valentin Dahmani, plays on the existing stereotype of Romanies as chicken thieves. The film also incorporates sound effects of horses, explosions and a watch mechanism. The soundtrack also has a tune of the "Le Temps des cerises", the revolutionary song of the Paris Commune. The music for the songs version in the movie was composed by Gatlif, using clockwork sounds and banjo. Other soundtrack vocalists included Kalman Urszuj, Sandu Ciorba and Ikola.   
 The Ghost Writer.  Korkoro s soundtrack is said to invoke mixed feelings like good humour, nostalgia and fear, creating a universe parallel to the film. 

== Themes and analysis == Dickensian orphan.   Critics also made comparisons between the state of the Romanies in the film, set during World War II, and their circumstances in the present. 

=== Holocaust elements === Time Out, New York.   Sophie Benamon at LExpress (France)|LExpress observed that Gatlif dealt with the horrors of the Holocaust by hinting at them through symbolism, such as portraying an abandoned child, suggesting imprisoned parents, and a clock with Hebrew markings seen lying abandoned in the middle of the railroad tracks, implying the passage of a train taking Jews to a ghetto.  Jr Glens Heath, writing forSlant Magazine, remarked that Gatlifs characterisation of the incomplete historic archives with which he was presented made the film a very "personal WWII historiography", where the characters "transcend victimisation" rather than mire themselves in melodrama, regarded as a typical Holocaust movie characteristic.  Michael Nordine wrote for Hammer to Nail that this film cannot be compared with Life is Beautiful and other "uplifting tales" with Holocaust themes because of its straightforward portrayal of realistic events. 

=== Gatlifs "Gypsy soul" ===
 
There were many reviews on the way Gatlif has depicted the Romanies, comparing it with their portrayals in his earlier movies. A reviewer in Variety (magazine)|Variety said  that the films central theme lies in its depiction of the "Gypsy soul", which is its unique element, rather than its rather clichéd portrayal of the Holocaust.  Tobias noted that Gatlif has depicted Romanies as "a tight, syncopated unit than a motley collection of individuals", citing a scene where the members of a group are distressed, even though they have escaped from a ghetto, until a missing person rejoins them.  Unlike his earlier films, in which the Romanies were stereotyped as musicians, Korkoro depicts them as possessing many other skills, for example as healers and blacksmiths, and that its portrayal reveals their communitarian side along with their respect for unique individual qualities, stated Odile Tremblay at Le Devoir.   Brian Laffertys (East County Magazine) comments were quite the opposite, complaining that the characterisations were "bland and generic with no unique identity" except for Taloches, which was considered more of an annoyance.   Rachel Saltz at The New York Times attributed Korkoros "unexpectedly leisurely quality" to the Gypsy way of living, their music, colours and bond with nature.  The film also depicts the Romanies aversion to being tied down to one location, observed Harvey Karten at Arizona Reporter,    and some of their customs, such as silencing the sounds of horses hooves with cloth bags, according to The Village Voice s reviewer Nick Schager.  Dan Bennett at North County Times approved of the appropriate costumes used in the movie, making it "a visually stylish and detail-friendly look at the nomadic lifestyle" that prevailed at the time. 

=== Freedom as a theme ===
A few critics suggested freedom as a theme in light of the importance shown by the characters to it. True to its title, which is a Romani word for freedom,  Gatlif used his freedom to direct a tangential, poignant romantic story with the historical documents available to him, unlike other movies with similar themes, remarked Jacques Mandelbaum at Le Monde.  The Village Voice review declared that it is "a magnificent paean to the mad ecstasy of freedom".  The Arizona Reporter review added that, for the Romanies, freedom means "being able to keep in motion, that is, the journey, not the destination, is the reward". It observed the importance the characters give to freedom, citing the scene where Taloche becomes concerned that water is being "held against its will" in the taps, and "releases" it to overflow the sink onto the bathroom floor, and then the stairs, with Taloche blissfully sliding down the stairs as if he were on a Disney ride.  Alexis Campion at Le Journal du Dimanche remarked that Gatlif has refreshingly portrayed the Romanies as "free-spirited" characters and added that this historic film is a tribute to those free souls who take to the streets even today.  The Télérama review was of the opinion that the movie runs out of steam during the scenes depicting historic events, but gains momentum in the forests and on the roads, where its characters passion for freedom, and hence Lavoines and Crozes characters, get sidelined by that of Thierrées, with his St. Vitus dance and Dostoyevsky-like ruminations.  It added that Taloche is the true "incarnation" of freedom.   

=== Mirroring the current times ===
A section of critics wrote on the relevance of the movie to the current times. In an interview, Gatlif stated that he wanted the movie to mirror the current times, adding that the times have not changed much, and that while the political extermination has gone, the psychological and political views of Romanies have not. He criticised the French law that allows wanderers to stay in one place only for 24 hours. He was also critical of the plight of Romanies in Hungary, Romania and Italy.  He went on that the state of the Romanies now in many places, "with the rows of homeless people waiting for a bowl of soup with a tin can on their hands", is not very different from that in the concentration camps.  Gatlif also lashed out against the fact that until 1969, Romanies were required to have their papers stamped at a police station or city hall whenever they arrived at or left a French village.  Bob Hill at Moving Pictures Network remarked that the movie draws parallels to the fact that "we are once again veering toward a culture in which regimes and wealth determine who has the right to live free — and who has no rights at all", and cited present happenings such as the developments in the Middle East, racial wars and inter-country disputes. It added that the movie makes the audience ask themselves if they live in a society that embraces or condemns diversity. 

== Release == Grand Prize of the Americas, Special Grand Prix of the Jury, Best Director, Best Actress, Best Actor, Best Screenplay, Best Artistic Contribution, and Innovation Award.  Alongside the film, Gatlif released a novel under the same name, Liberté, which he co-authored with the French novelist Eric Kannay. The book follows the films script.    
 Ankara International Film Festival placed it in its masters section, along with the works of such noted filmmakers as Werner Herzog, Takeshi Kitano and Ken Loach.    The Washington, DC International Film Festival also had a screening of the movie in 2011. 

== Reception ==

=== Box office === Lorber Films holding its distribution rights.      The film grossed $1,224 over its opening weekend, and ranked 107th at the box office. It made $8,179 in 15 weeks in North American cinemas. This brought the total gross to $627,088.   

=== Critical response === stream of consciousness techniques. Commenting on its tone, he wrote that "Korkoro is a reserved but lasting examination of collectively silent horror".    At LExpress, Sophie Benamon declared that with the films controlled pace, it keeps its viewers breathless and induces emotion.  Alexis Campion at Le Journal du Dimanche stated that the film transcends the stereotypes    while the reviewer at Arizona Reporter noted that some might consider it to be stereotyping the Romanies. 

On Gatlifs direction, Odile Tremblay at Le Monde remarked that he had taken a heavy burden in directing a Holocaust movie along with coupling it with the "poetic effervescence" that the Romanies are known for. He added that this can be regarded the best among his films.  Michael Nordine characterised Gatlifs directorial style as passive and "documentarian", such that it "sometimes verges on emotional distance". "What easily noticeable flourishes he adds tend to be understated", he added.    Eric Hynes at TimeOut lauded Gatlifs work in making a celebration of textures and music of the Romanies out of a melodramatic story.    East County Magazine s negative review summed that Gatlif had too much faith on his audience, "expecting them to take everything at face value".   

  National Public Radio review by Scott Tobias stated that the film weds the exotic culture of the Romanies to the cliched themes of a war film with its characterisation as its means. With a "Schindlerian" Theodore Rosier, a Dickensian Claude, a hinted romance between Rosier and Miss Lundi, and the clown-like Taloche, it added, Gatlif has "weaved a tapestry out of the authentic and the chintzy".  Joe Bendel at The Epoch Times stated that the film avoids simply depicting the Romanies as victims, with its "decidedly unsentimental" central character, Taloche, who is "wild almost to the point of being feral".  Sophie Benamon at LExpress declared that the madness of Taloche is the pivotal element of the film.  In Taloches face, Nick Schager perceived that "the film seethes with full-bodied fury and anguish".    Arizona Reporter s review lauded "Taloches manic antics" as "both the comic center of the film and a representation of the tragedy".  Ronie Scheib (Variety) admired Taloches characterisation, commenting on his acrobatic stunts and close-to-nature persona as gelling well with the films title of "Freedom".  Lavoines and Crozes characters also received a positive mention in the reviews at LExpress and The Epoch Times,     with Sophie Benamon at LExpress calling them "compelling".   

Julian Hirschs cinematography can be regarded as a relief to the eyes from the films gory Holocaust theme, stated Ronnie Scheib (Variety).  Jacques Mandelbaum at Le Devoir added that the beauty of scenes such as the arrival of the Romanies in caravans in their first scene and the gentle music is a stark contrast to the harsh themes of concentration camps and extermination.    Odile Tremblay (Le Monde) made mention of a number of scenes, including the one where Taloche opens a tap to "free" water, and the scene with an abandoned watch implying the ghettos as the best moments of the film.  Nick Schager (The Village Voice) stated that the film scores on its aesthetics in sequences depicting key elements, like the one with trains symbolising the Holocaust, and its detailed depiction of the characters intimate practices, which bring depth to a rather predictable plot.  Brian Lafferty (East County Magazine) criticised Julian Hirsch for making the sequences look dull and gloomy with insufficient lighting.  The tap scene received a special mention in LExpress  review too. 

On its historical aspects,  Alexis Campion (Le Journal du Dimanche) stated that this is the first French film dealing with the Porajmos.  Ronnie Scheib (Variety) lauded the film on its exposure of the French gendarmeries role in the Holocaust, the reviewer added that this is the most "accessible" film of Gatlif after Latcho Drom.    A review in the Independent Catholic News said that the film provides one a chance to remember the forgotten aspects of World War II and to learn more about French prejudice and the persecution of the Gypsies. 

The movie received its highest rating of three stars from the reviewers at Le Journal du Dimanche  and Slant Magazine  while Arizona Reporter gave it a B+ as per its grading system. 

Korkoro is rated at 75% on Rotten Tomatoes.

===Awards===

{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Credits !! Won !! Ref.
|-
|rowspan="3"| 2009
|rowspan="3"| Montreal World Film Festival 
| Grand Prix of the Americas
| Korkoro
| 
| style="text-align:center;"|        
|-
| Audience Award, International
| Korkoro
| 
| style="text-align:center;"|  
|-
| Prize of the Ecumenical Jury – Special Mention
| Korkoro
| 
| style="text-align:center;"|  
|-
|rowspan="4"| 2010 
| Festival international du film dhistoire de Pessac
| Prix du public
| Korkoro
| 
| style="text-align:center;"|  
|-
| MedFilm Festival
| Special Mention
| Korkoro
| 
| style="text-align:center;"|    
|-
|rowspan="2"| The Time for Peace Film and Music Awards
| Best Picture & Directing
| Tony Gatlif John Rabe
| style="text-align:center;"|     
|-
| Founders Choice Picture
| Tony Gatlif
| 
| style="text-align:center;"|      
|-
| 2011
| César Awards 2011|César Awards
| Best Music Written for a Film
| Tony Gatlif,  Delphine Mantoulet The Ghost Writer
| style="text-align:center;"|    
|}

== References ==
 

== External links ==
*    
*  
*  

 
 

 
 
 
 
 
 
 
 