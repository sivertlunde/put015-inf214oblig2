Joke Falls
{{Infobox Film name = Joke Falls image = Kannadamoviejokefalls.jpg director = Ashok Patil producer = Atlanta Nagendra released = 4 December 2004 language = Kannada music = Mano Murthy starring = Neethu
|budget =  1 crores
}}

Jokefalls is a 2004 Indian Kannada-language comedy movie, directed by Ashok Patil and produced by Atlanta Nagendra.    Music was composed by Mano Murthy.
  
 

==General background==
Director Patil devised the basic plot of the story from the Hindi movie Chupke Chupke and reworked it to give it a fresh look. Patil then recruited Ramesh  to act in the film. New comer Neetha was chosen to play the female lead. 
  Deepali who had previously acted in Nanna Preethiya Hudugi was chosen to play the second female lead.  

==Plot==
Botany professor Anant Patil falls in love with Sulkeha and marries her to the dismay of Sulekhas brother in law, Raghav Joshi. Raghav refuses to attend the wedding. But Anant decides to turn the tables on Raghav and teach him a lesson.

==Cast==
* Ramesh - Anant Patil
* Neetha - Sulekha
* Deepali - Vasudha
* Dileep - Sukumar
* Pallavi - Kamala

==Crew==
*Director: Ashok Patil
*Producer: Atlanta Nagendra 
*Music: Manomurthy
*Cinematography: Ashok Kashyap 
*Editing: Shashi Kumar
*Lyrics: K.Kalyan, Nagathihalli Chandrashekar
*Singers: Rajesh, Nandita, Chetan, Kavitha KrishnaMurthy

==Soundtrack==
{{Infobox album  
| Name        = Joke Falls
| Type        = Soundtrack
| Artist      = Mano Murthy
| Cover       =
| Released    = 2004
| Recorded    =
| Genre       = Film Soundtrack
| Length      =
| Label       = 
| Producer    =
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}

The music is scored by Mano Murthy and was big hit. 

{| class="wikitable"
|-
! Song title !! Singers !! 
|- Gandhavathi || Rajesh Krishnan| Rajesh || 
|- Swaraga Shrusthi Modala || Rajesh Krishnan| Rajesh & Chetan Sosca| Chetan || 
|- Nanna Beladingalu Neenu || Kavita Krishnamurthy || 
|- O Prema Tangaliye || Nanditha || 
|- Naguvina Loka Idu || Chetan Sosca| Chetan & Nanditha || 
|}

==Reviews==
Joke falls received positive reviews from critics as well as audiences.  

==Box-Office==
Joke falls was released on 3 December 2004.  The film received a good response, and ran for a hundred days in Bangalore.  
  
 
It was the first Kannada movie to run for twenty five weeks in PVR Cinemas in Bangalore.

The movie also saw screenings in Australia, Canada, and the United States. 
 

== References ==
 

 
 
 
 