Finale (film)
{{Infobox film
| name           = Finale
| image          = Finale film.jpg
| director       = John Michael Elfers
| producer       = Michael Elkin and Marisa Wahl
| writer         = John Michael Elfers
| starring       = Carolyn von Hauck Suthi Picotte Domiziano Arcangeli
| cinematography = Ryan Stevens Harris
| editing        = Edward H. Stanley
| music          = Shawn Clement and James Speight
| studio         = Fire Trial Films
| distributor    = Image Entertainment
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
}}
Finale is a 2009 horror film directed by John Michael Elfers and starring Carolyn von Hauck. Inspired by actual events from the directors life, FINALE is reminiscent of Suspiria, Hellraiser and Rosemarys Baby, shot in super 16mm to capture an authentic 1970s feel, with all practical in-camera effects, borrowed from the lost arts of early film magicians.

The film was released in the United States on May 25, 2010, after premiering at A Night of Horror in Sydney Australia, and Screamfest in the U.S. in 2009.   

==Cast==
* Carolyn von Hauck as Helen Michaels
* Suthi Picotte as Kathryn Michaels

==Awards==
* "Independent Spirit Award" the A Night of Horror film festival of Sydney, Australia   

==Reviews==
The film received positive reviews from LA Weekly,    Arrow in the Head,    and Digital Retribution.   

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 

 