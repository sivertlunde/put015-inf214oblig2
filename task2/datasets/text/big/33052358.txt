Australia's Own
 
 
{{Infobox film
| name           = Australias Own
| image          = 
| image_size     =
| caption        = 
| director       = J. E. Ward
| producer       = J. E. Ward
| writer         = J. E. Ward
| based on       = 
| narrator       =
| starring       = Nellie Romer Garry Gordon
| music          =
| cinematography = J. E. Ward
| editing        = 
| distributor    = 
| released       = 17 December 1918 (preview)  20 January 1919 
| runtime        =
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Australias Own is a 1919 Australian silent film set in New Guinea, with footage shot in the Yule Island area near Port Moresby. It is a lost film.

The title refers to the transfer of New Guinea from being a German possession to an Australian colony. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 88. 

==Plot==
In New Guinea, a young ex-Anzac officer and his girlfriend come into conflict with a German settler, Carl, who is trying to steal the womans right to an oil well.   

==Cast==
*Nellie Romer
*Garry Gordon as Anzac soldier
*J.E. Ward as Carl Hickmann

==Production==
J.E. Ward was a sketch artist on the staff of the Sydney Morning Herald who travelled extensively in Papua, shooting thousands of feet of footage. On the suggestions of Dan Carroll he decided to add some dramatic narrative to his footage, and in mid-1918 shot some scenes with actors Nellie Romer and Garry Gordon on Yule Island. 

Catholic missionaries complained about the filming and territory administrators impounded the footage on the grounds the film might hurt relations with the native population. Ward appealed and the footage was released. 

==Reception==
The film was advertised as "The motion picture sensation that the Government of Papua banned." 

It does not appear to have been a success. However, Ward later released several more documentaries with a Papuan background, including The Quest for the Blue Bird of Paradise (1923) and Death Devils in Paradise (1924), as well as the comedy, Those Terrible Twins (1925). 

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 


 