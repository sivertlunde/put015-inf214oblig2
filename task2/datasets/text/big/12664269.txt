Offside (2006 Swedish film)
{{Infobox film|
 name = Offside |
 director = Mårten Klingberg |
 producer = Christer Nilson |
 writer = Mårten Klingberg Oskar Söderlund |
 starring = Jonas Karlsson Torkel Petersson Göran Ragnerstam Ingvar Hirdwall Brendan Coyle Tina Nordlund |
 distributor = Sandrews |
 released = August 18, 2006 (Sweden) |
 runtime = 99 minutes | Swedish |
}}
 Swedish film from 2006, directed by Mårten Klingberg and starring Jonas Karlsson, Torkel Petersson, Ingvar Hirdwall, Göran Ragnerstam and Brendan Coyle.

==Story==
The film is about a Swedish football team, Stenfors BK, who used to play in Allsvenskan once upon a time, but now plays in one of the lowest divisions. The humiliation is complete. But no more! If Stenfors BK wont start winning and climb up some divisions, the team will be disbanded.

So now, the struggle to survive starts. But its tough, especially because Anders, the team captain, has trouble at home with his wife Åsa.
 Liverpool player Duncan Miller.

But when "Killer Miller" arrives at Stenfors, the team founds out that he is a little more former player than what they had expected...

==Actors==
*Jonas Karlsson as Anders
*Torkel Petersson as Tommy
*Ingvar Hirdwall as Boston
*Göran Ragnerstam as Kent
*Brendan Coyle as Duncan Miller
*Anja Lundqvist as Åsa
*Tina Nordlund as Annika

==Notes==
*Duncan Miller is a fictional character and has never played for Liverpool F.C. or any other football club.
*Tina Nordlund appears in the film as a football player. She was a football player in real life too. She has played many times for the Swedish national team and for Umeå IK. She retired in 2002, because of anorexia nervosa. 

==References==
 

==External links==
*  
*  

 
 
 
 


 
 