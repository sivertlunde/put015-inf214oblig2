Used People
{{Infobox film
| name           = Used People
| image          = Used people poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Beeban Kidron
| producer       = Peggy Rajski
| writer         = Todd Graff
| starring       = Shirley MacLaine Kathy Bates Jessica Tandy Marcello Mastroianni Marcia Gay Harden Sylvia Sidney Joe Pantoliano
| music          = Rachel Portman David Watkin
| editing        = John Tintori
| studio         = Largo Entertainment JVC Entertainment
| distributor    = 20th Century Fox 
| released       = 16 December 1992
| runtime        = 115 min
| country        = USA / Japan
| language       = English
| budget         =
| gross          = $17,957,265
}}
 American romantic Queens circa 1969. 

==Plot synopsis==
Pearl Berman has just returned home from her husband Jacks funeral, her grief disrupted by her many relatives animatedly discussing which parkway offered the best route to the cemetery. Pearls family tackles any and every subject - from body odor to toilets to Tupperware to borscht - as if its worthy of a major debate.
 Italian who years ago met Pearls wayward husband in a bar and convinced him to return to her. He has desired her ever since, and now that Pearl is a widow, Joe feels the time is right to make his move. He invites her for coffee, his first step on the road to seduction.
 her husband; appears as Marilyn Monroe as she serves her surviving son his breakfast; and also impersonates Faye Dunaway and Barbra Streisand among others. And those are just some of the crazy relatives that come as part of the package.

==Cast==
*Shirley MacLaine ..... Pearl Berman 
*Marcello Mastroianni ..... Joe Meledandri 
*Bob Dishy ..... Jack Berman 
*Kathy Bates ..... Bibby Berman 
*Marcia Gay Harden ..... Norma 
*Jessica Tandy ..... Freida 
*Sylvia Sidney ..... Becky Lee Wallace ..... Uncle Harry
*Doris Roberts ..... Aunt Lonnie 
*Joe Pantoliano ..... Frank

==Principal production credits== Lawrence Gordon
*Executive Producers ..... Michael Barnathan, Lloyd Levin Original Music ..... Rachel Portman     David Watkin  
*Choreography ..... Patricia Birch  Production Design ..... Stuart Wurtzel     Art Direction ..... Gregory P. Keen  
*Costume Design ..... Marilyn Vance

==Critical reception==
In her review in the New York Times, Janet Maslin observed, "As directed by Beeban Kidron,   makes an international issue out of an Italian-Jewish courtship. It also slathers the ethnic equivalent of corn onto every sentimental scene."  

Roger Ebert of the Chicago Sun-Times said, "The movie is by turns serious, satirical, bittersweet, maudlin, satirical, romantic and farcical... MacLaine is a pro and survives the material... We care about her enough, indeed, to wonder if meeting the Mastroianni character is really the best thing that could have happened to her. He doesnt often seem like a real human in this movie; hes more like an all-purpose writers device. The odds are against any sane person being able to behave like this man... what the movie could not overcome, for me at least, is the lack of any convincing romantic chemistry between  ."  

Variety (magazine)|Variety stated, "A modern, absurdist sensibility informs the soap opera Used People... which harks back to 50s weepies... MacLaines precise acting is laudatory and balanced by a very sympathetic turn by twinkle-eyed Mastroianni, in his best English-language role by far. The support ensemble is excellent."  
 matzo balls, Jewish mother joke... its rich in character if rather trite in theme and scene."  

In The New Yorker, Michael Sragow observed, "Life goes on — for the audience, seemingly forever — in yet another ethnic comedy-drama ... In the end, love conquers everything from religious differences to mental illness: everything, that is, except the forced eccentricity and bickering stereotypes in Todd Graffs script."  

In his review of the videotape release, Ty Burr of Entertainment Weekly rated the film C- and added, "Everythings in italics in this Jewish Moonstruck... MacLaine honks nasally ... Mastroianni talks in spumoni aphorisms... the score oompahs with grating merriment as characters parade cutesy tics instead of human traits. With stars as gifted as Jessica Tandy, Sylvia Sidney, and Kathy Bates drowning in the tough-talking treacle, its one of those movies that gives New York a worse name than it already has."  

==Accolades==
Marcello Mastroianni was nominated for the Golden Globe Award for Best Actor – Motion Picture Musical or Comedy and Shirley MacLaine was nominated for the Golden Globe Award for Best Actress – Motion Picture Musical or Comedy. The Casting Society of America nominated Mary Colquhoun for the Artios Award for Best Casting for a Dramatic Feature Film. 

==Home video release==
Despite being distributed by 20th Century Fox on its initial release in 1992, the movie was put on DVD on March 22, 2011 by Warner Home Video as part of their Warner Archive Collection.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 