Gaja Gamini
{{Infobox film
| name           = Gaja Gamini गज गामिनी  
| image          = Gaja Gamini .jpg
| image size     =
| border         =
| alt            =
| caption        =
| director       = M. F. Husain
| producer       = Rakesh Nath
| writer         = Kamna Chandra (writer) M. F. Husain
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       = Madhuri Dixit  Shahrukh Khan  Naseeruddin Shah
| music          = Bhupen Hazarika
| cinematography = Ashok Mehta
| editing        = Waman Bhosle Virendra Gharse
| studio         =
| distributor    = Yash Raj Films  
| country        = India
| duration       = 122 minutes
| released       = 1 December 2000 English
| gross          =   1.08 crores  
}}

Gaja Gamini ( ,  }}, English   and is his ode to womanhood and his muse of the time, Madhuri Dixit, who stars as the lead in the film with Shahrukh Khan and Naseeruddin Shah.   The film was a box office disaster. 

==Plot==
The central figure of the film is represented by a mysterious figure called "Gaja Gamini" (Madhuri Dixit), who inspires, arouses, and confuses the common man. "Gaja Gamini" is the inspiration behind Leonardo da Vincis (Naseeruddin Shah) Mona Lisa, Kalidas poem "Shakuntala", and a photojournalist named Shahrukhs (Shah Rukh Khan) photographs. The mysterious "Gaja Gamini" appears as four characters, one of them being Sangeeta, a blind girl from Banaras at the beginning of time, who inspires village women (Farida Jalal, Shilpa Shirodkar, and Shabana Aazmi) to revolt against a male-dominated system and carve a niche for women forever. Another character is Shakuntala, who is the subject of Kalidas poem of the same name. Shakuntala incites jealousy in the women and love in the men around her, charming humans and animals alike in the forests of Kerala. "Gaja Gamini" is also Mona Lisa during the Renaissance, the object of painter Leonardo da Vincis obsession. Finally, Monika, the most confusing sector of the film, is supposed to represent the woman of the New Millennium. Kamdev, the God of Love (Inder Kumar), walks the earth throughout history, attempting to win the love of "Gaja Gamini".

Thrown into this mix is a large black wall, separating two different periods, and confrontations between Science (Ashish Vidyarthi) and Art (Mohan Agashe) at different points in history, showing that the world itself can change, but its original ideas will always be the same. For example, a play by Shakespeare written and performed by actors in the 15th century will still be performed in the 21st century, but with different actors. The confrontations between art and science also bring about the idea that while science is firmly set on believing that which can only be proved, the basis for art is that which can be proved, and an intuitive sense that can be felt. Science uses the brain, while Art uses the brain and the heart. Another facet of the film is a "gathri", a small bundle which a woman carries upon her head, like a burden, with which she must walk forever.  

==Cast==
* Madhuri Dixit – Gaja Gamini/Sangita/Shakuntala/Monika/Mona Lisa
* Tanvi Hegde - Baby Shakuntala
* Shabana Azmi – Premchands Nirmala
* Shahrukh Khan– Shahrukh (special appearance)
* Naseeruddin Shah – Leonardo da Vinci
* Shilpa Shirodkar – Sindhu
* Tej Sapru – Tansen
* Farida Jalal – Noorbibi
* Mohan Agashe – Kalidasa
* Ashish Vidyarthi – Scientist

==Crew==
* Art direction – Sharmishtha Roy
* Costume design – Reza Shareefi
* Choreography – Saroj Khan, Jojo Khan
* Sound – Buta Singh
* Background music – Anuradha Pal

==Reviews==
Critic Taran Adarsh said "The film has a very colourful look, with the visuals being eye-catching. But as a director, Hussain fails to convey his thoughts on celluloid. On the whole, Gaja Gamini is an artistic film which will not be understood by any strata of audience - classes or masses. Business-wise, the film is sure to spell disaster. A waste of precious celluloid". 

==Soundtrack==
{{Infobox album
| Name        = Gaja Gamini
| Type        = Soundtrack
| Artist      = Bhupen Hazarika
| Cover       =
| Released    = 1 January 2000 (India)
| Recorded    =
| Genre       = Film soundtrack|
| Length      =
| Label       = Sa Re Ga Ma|
| Producer    = Bhupen Hazarika|
| Reviews     =
| Last album  =    (2001)
| This album  = Gaja Gamini  (2000)
| Next album  = Darmiyan  (1997)
|}}

Hussain approached A. R. Rahman to compose the music for the film, but due to time constraints, he had to turn down the offer. The music was composed by Bhupen Hazarika.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s) !! Lyricist
|-
| "Deepak Raag"
| Shankar Mahadevan
| Maya Govind
|-
| Do Sadiyon Ke Sangam
| Udit Narayan, Kavita Krishnamurthy
| Javed Akhtar
|-
| Gaja Gamini
| Bhupen Hazarika
| Maya Govind
|-
| Hamara Hansa Gaya Videsh
| Kavita Krishnamurthy
| Maya Govind
|-
| Meri Payal Bole
| Kavita Krishnamurthy
| Maya Govind
|-
| Shloka - Part 1
| Roop Kumar Rathod
| Kalidas
|-
| Shloka - Part 2
| Udit Narayan
| Kalidas
|-
| "Yeh Gathri Taj Ki Tarah"
| Kavita Krishnamurthy
| M. F. Hussain
|-
| "Yeh Gathri Taj Ki Tarah - 2"
| M. F. Hussain
| M. F. Hussain
|-
| "Protest March" Instrumental 
|
|}

==References==
 

==External links==
*  
*   at Bollywood Hungama

 
 
 
 
 