Dr. Socrates
{{Infobox film
| name           = Dr. Socrates
| image          =
| image_size     =
| caption        =
| director       = William Dieterle
| producer       =
| writer         = Robert Lord (screenplay) Mary C. McCall, Jr. (adaptation)
| story          =
| based on       =  
| narrator       =
| starring       = Paul Muni Ann Dvorak Barton MacLane
| music          =
| cinematography =
| editing        =
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Dr. Socrates is a 1935 crime film starring Paul Muni as a doctor forced to treat a wounded gangster, played by Barton MacLane.

==Cast==
*Paul Muni as Lee
*Ann Dvorak as Josephine
*Barton MacLane as Red Bastian
*Robert Barrat as Dr. Ginder John Eldredge as Dr. Burton
*Hobart Cavanaugh as Stevens
*Helen Lowell as Ma Ganson
*Mayo Methot as Muggsy
*Henry ONeill as Greer
*Grace Stafford as Caroline Suggs Samuel Hinds as Dr. McClintick
*June Travis as Dublin
*Raymond Brown as Ben Suggs
*Olin Howland as Bob Catlett
*Joseph Downing as Cinq Laval

==External links==
* 
* 
* 

 

 
 
 
 
 
 


 