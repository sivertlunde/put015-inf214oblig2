After Midnight (1927 film)
{{Infobox film
| name           = After Midnight
| image          =
| caption        = Lobby card
| director       = Monta Bell
| writer         = Joseph Farnham (titles) Lorna Moon
| story          = Monta Bell
| starring       = Norma Shearer Gwen Lee
| cinematography = Percy Hilburn
| editing        = Blanche Sewell
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70&nbsp;minutes
| country        = United States Silent English intertitles
}} silent drama film written and directed by Monta Bell. The film stars Norma Shearer and Gwen Lee. 

==Synopsis==
A story of New Yorks nightlife: Mary (Norma Shearer) is a cabaret hostess with a heart of gold and her sister Maizie (Gwen Lee) is a gold-digger with no heart.

==Cast==
* Norma Shearer - Mary Miller
* Lawrence Gray - Joe Miller
* Gwen Lee - Maizie
* Eddie Sturgis - Red Smith
* Philip Sleeman - Gus Van Gundy

==References==
 

==External links==
*  
*  
*   at Flickr

 
 
 
 
 
 
 
 


 