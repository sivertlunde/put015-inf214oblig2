How to Break Up a Happy Divorce
{{Multiple issues|
 
 
}}

{{Infobox television film
| name           = How to Break Up a Happy Divorce
| caption        = 
| director       = Jerry Paris	
| executive_producer   = Charles Fries
| producer       = Dee Caruso Gerald Gardner Dee Caruso
| starring       = Barbara Eden Hal Linden Peter Bonerz Marcia Rodd

| music          = Nelson Riddle 
| cinematography = Richard A. Kelley
| editing        = Bud Molin Robert Moore
 Charles Fries Productions
| network        = NBC
| first_aired    =  
| runtime        = 74 minutes
| country        =  
| language       = English
| budget         = 
| preceded_by    = 
| followed_by   = 
}} American Television Gerald Gardner and Dee Caruso.      

== Plot ==

Ellen (Barbara Eden) and Carter (Peter Bonerz) used to be married.  But now theyre happily divorced.
So why would anyone want to break up a perfect situation like that?  Well, the truth is that Ellen isnt happy with the arrangement and she uses every trick in the book to get her ex-husband back.  The results are complicated, confusing and downright hilarious in this comedy also starring Hal Linden and Marcia Rodd.  Ellen cant stand the fact that her ex-husband is dating a woman with the body of a pin-up girl and the intelligence of a grapefruit.  So she decides to somehow win him back.  Ellens best friend, Eve (Marcia Rodd), advises her that the only method that will work is to make Carter jealous.  Her first choice of weapons turns out to be Tony (Hal Linden), a handsome man-about-town.

==References==
 

==External links==
* 
* 
* 

 
 
 


 
 