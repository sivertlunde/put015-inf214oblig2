Iru Malargal
{{Infobox film
| name = Iru Malargal
| image = Iru Malargal.jpg 
| caption = Promotional poster
| director = A. C. Tirulokchandar
| producer = Manijeh Cine Productions
| writer = Aarur Das
| story = A. C. Tirulokchandar Padmini K. R. Vijaya Chittor V. Nagaiah Nagesh Manoramma S. A. Ashokan Roja Ramani
| music = M. S. Viswanathan
| cinematography = Thambu
| editing = B. Kandaswamy
| studio = Manijeh Cine Productions
| released = 1 November 1967 
| runtime = 
| country = India
| language = Tamil
| budget = 
| gross = 
}}

Iru Malargal (  romantic drama film directed by A. C. Tirulokchandar. The film stars Sivaji Ganesan, Padmini (actress)|Padmini,  K. R. Vijaya, Chittor V. Nagaiah, Nagesh, Manoramma, Roja Ramani and S. A. Ashokan. The film tells the story of a man who faces upheavals in his life as he is caught between his lady-love and his devoted wife. Released on 1 November 1967, Iru Malargal opened to critical acclaim.

==Plot==
Sundar (Sivaji Ganesan) and Uma (Padmini (actress)|Padmini) are classmates who fight often. Sundar, however, is very much in love with Uma, and when they perform in a dance drama competition at Madurai, and later move on to Kodaikanal, he expresses this love. Uma asks him to climb up a peak so that she will consider him as a suitor. Sundar suffers from Acrophobia and almost falls while climbing, at which point Uma accepts his love.

Sundars cousin Shanti (K. R. Vijaya), who lives with his family and takes care of the entire household, is fond of him and wishes to marry him. Sundars father (V. Nagaiah) also wants this marriage to take place.  When Shanti discovers the love between Sundar and Uma, however, she changes her mind. Sundars father asks Sundar to marry Shanti, but he refuses, revealing that he is in love with Uma. Furious, Sundars father begins looking for another bridegroom for Shanti.

Meanwhile, Uma goes to seek permission to marry Sundar from her brother, an Army major who is her only living relative. She tells Sundar that she will send him a letter on a particular date (October 10). When Umas letter informs him that she has decided to marry another person because she is not willing to go against the wishes of her brother, an emotionally distraught Sundar becomes bedridden. In truth, Umas brother and sister-in-law were killed in a road accident, leaving behind their three children. She decided to renounce her love for Sundar in order to take care of the children, and lied to him so that he would not come after her.

While taking care of the dejected Sundar, Shanti is confronted by her bridegroom who accuses her of having a relationship with Sundar. When Sundar realizes how much his father and his cousin have suffered because of him, he decides to marry Shanti.

Years go by, and Sundar has become a successful businessman living in Kodaikanal with Shanti and their six-year-old daughter Geetha (Roja Ramani). Meanwhile, Uma joins Geethas school and becomes her teacher. When Geetha enthusiastically tells her mother about Uma, Shanti wants Uma to take tuition for Geetha; Uma accepts the request. She is shocked to learn that Geetha is Sundars child when they accidentally meet along the road. Sundar goes to Uma’s home, where they have a conversation about the past. Hearing this, Geetha realizes that her father was once in love with her teacher.

Shanti comes to know of Sundars relationship with Uma. Not wanting to cause Sundar and Shanti to separate, Uma gives a letter to the school principal (Nagesh) asking him to take care of her brothers children if something happens to her, and goes to a cliff to meet Sundar. She asks Sundar, “If I call you, will you come with me leaving everything behind?” Sundar tells her, "I could leave anything except my wife and child." This is the answer Uma wants, having decided that, if Sundar talked about leaving Shanti and Geetha, she would throw herself from the cliff. At the same time, Shanti concludes that Uma and Sundar should be united, and decides to commit suicide, Uma and Sundar stop her, with Uma telling her everything that happened between her and Sundar was in the past. Uma also tells Sundar and Shanti that she doesn’t want to cause problems for them and leaves, going back to a boarding school with her niece and nephew.

==Cast==
* Sivaji Ganesan as Sundar Padmini as Uma
* K. R. Vijaya as Shanti
* Chittor V. Nagaiah as Sundars father
* Nagesh as the Principal
* Manoramma as Principals wife
* S. A. Ashokan as Correspondent
* Roja Ramani as Geetha

==Production==
Shooting for Iru Malargal took place at Kodaikanal and Madurai. The introduction song - a "Dance - Drama competition", picturized on Sivaji Ganesan and Padmini was shot at Madurai, and  other portions of the film were shot at Kodaikanal. 

==Soundtrack== Vaali penned Ventriquilosim   had been used in 1967 itself in the song Maharaja oru Maharani song before it was used on a larger scale in Avargal 10 years later."  Vaalee considers the number Madhavi Ponmayilal as one among his "personal favourites". 

{| class="wikitable"
|-
! Track !! Song !! Singers !! Lyrics
|- Vaali
|-
| 2 || Maharaja Oru Maharani || Sadan, Shoba Chandrasekhar|Shoba, T. M. Soundararajan || Vaali
|-
| 3 || Mannikka Vendugiren || T. M. Soundararajan, P. Susheela || Vaali
|-
| 4 || Mathavi Pon Mayilaal || T. M. Soundararajan || Vaali
|-
| 5 || Velli Mani || P. Susheela || Vaali
|-
| 6 || Annamita Kaigaluku|| P. Susheela || Vaali
|}

==Release==
Iru Malargal was released on November 1, 1967, coinciding with Deepavali day. It was released with another Sivaji Ganesan film Ooty Varai Uravu,  making him the only actor to act in two films released on the same day. 

===Reception=== Padmini carries KRV as Manorama provide Asokan making a brief appearance doing well", while concluding that "Overall, it is a pleasure watching Iru Malargal."    Sulekha.com called it "An excellent story filled tamil movie". 

===Box office===
According to Ganesans autobiography, the film ran for over 100 days in theatres. 

==References==
 

==External links==
*  

 

 
 
 
 
 