Dilli Ka Thug
{{Infobox Film
| name = Dilli Ka Thug
| image = Dilli Ka Thug DVD cover.jpg
| image_size =
| caption = Film DVD cover
| director = S.D. Narang
| producer = S.D. Narang
| writer = S.D. Narang
| screenplay = S.D. Narang
| narrator =
| starring = Kishore Kumar  Nutan  Madan Puri   Iftekhar Ravi
| cinematography =
| editing = Mohan Rathod
| distributor =
| studio = Famous Cine Lab & Studios Ltd.
| released = 1958
| runtime = 132 mins
| country = India
| language = Hindi
| budget =
| preceded_by =
| followed_by =
}}
 Ravi and Shailendra and Majrooh Sultanpuri. The song "Hum to mohabbat karega" was Sultanpuris contribution.

==Plot==

Dili Ka Thug (Trickster of Delhi) is a movie revolving around Kishore (Kumar) who gambles and tricks people to make money. On being confronted by his mother, he decides to find a job and lands one in Mumbai, where he continues wooing Asha (Nutan). Kishores father and his friend were murdered by the notorious Anantram who now wears a mask to hide his true identity and continues manufacturing spurious medicines and also passes as Nutans uncle. The firm employing Kishore is run by Anantram and his two henchmen Sevakram and Bihari. Anantram goes to murder Kishore in the hospital, but drops his monocle which makes Iftekhar suspicious. He manages to take Anantrams fingerprints on Sohan Lals wallet and figures out the truth. What follows is a dramatic air-borne sequence where Anantram is beaten by Kishore and shot by Nutan. Kishore manages to land the aeroplane after the pilots lose consciousness.

==Cast==
* Kishore Kumar
* Nutan
* Madan Puri
* Amar
* Iftekhar
* Krishnakant
* Nirula
* Mirajkar
* Kumar Tripathy

==Track listing== Ravi while Shailendra and S. H. Bihari.
{| class="wikitable"
|-
! # !! Title !! Singer(s)
|-
| 1 || "Yeh Raaten Yeh Mausam" || Kishore Kumar, Asha Bhosle
|-
| 2 || "Cat Mane Billi" || Kishore Kumar, Asha Bhosle
|-
| 3 || "Hum To Mohabbat Karega" || Kishore Kumar
|-
| 4 || "Kisi Ka Dil Lena Ho" || Asha Bhosle
|-
| 5 || "O Babu O Laila Mausam Dekho Chala" || Geeta Dutt
|-
| 6 || "O Bandariya" || Kishore Kumar
|-
| 7 || "Seekh Le Babu Pyar Ka Jadoo" || Asha Bhosle
|-
| 8 || "Yeh Bahar Yeh Sama" || Asha Bhosle
|-
| 9 || "Yeh Raaten Yeh Mausam (Revival)" || Kishore Kumar, Asha Bhosle
|}

==External links==
*  

 
 
 
 
 
 


 
 