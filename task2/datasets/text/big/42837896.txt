The Colleen Bawn (1911 American film)
{{Infobox film
| name           = The Colleen Bawn
| image          = Colleen_Bawn_Wiki.jpg
| image_size     = 
| caption        = J.¨P. McGowan (left) and Sidney Olcott.
| director       = Sidney Olcott
| producer       = 
| writer         = Gene Gauntier
| story          = 
| based on       =  
| narrator       = 
| starring       = Gene Gauntier J. P. McGowan Sidney Olcott
| music          = 
| cinematography = George K. Hollister
| editing        = 
| studio         = Kalem Company
| distributor    = 
| released       =  
| runtime        = Three reels   
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 1860 play of the same name. A secret marriage leads to murder. It and the play are based on the actual 1819 murder of 15-year-old Ellen Scanlan.

Prints of this film survive in the National Archives of Canada, and the George Eastman House Motion Picture Collection has one reel. 

==Cast==
*Gene Gauntier as Eily OConnor, the "Colleen Bawn"
*J.P. McGowan as Hardress Cregan
*Sidney Olcott as Danny Mann
*Jack J. Clark as Myles na Copaleen
*Alice Hollister as Anne Chute Arthur Donaldson as Father Tom
*Robert G. Vignola as Mr. Corrigan
*Agnes Mapes as Mrs. Cregan
*Anna Clark as Sheelah

==Production notes==
* The film was shot in Beaufort, County Kerry, Ireland, during the summer of 1911.
* The Colleen Bawn is the first three reels of the kalem Company.

==References==
 
* Michel Derrien, Aux origines du cinéma irlandais: Sidney Olcott, le premier oeil, TIR 2013. ISBN 978-2-917681-20-6  

==External links==
* 
*  at YouTube
*    website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 