Two Shadows
 
{{Infobox film
| name           = Two Shadows
| image          = Two shadows.jpg
| caption        = Official One-Sheet (artwork by Ryan Graber)
| director       = Greg Cahill
| producer       = Greg Cahill Christen Marquez Arn Chorn-Pond Dara Yem
| writer         = Greg Cahill
| starring       = Sophea Pel Lida Lang Polo Doot
| music          = Ryan Leach
| cinematography = John Matysiak
| editing        = Greg Cahill
| studio         = Rising Falcon Cinema Paradocs Productions
| released       =  
| runtime        = 94 minutes
| country        = United States Cambodia Khmer English
}} Cambodian immigrants in the United States seeking surviving family connections in Cambodia since the fall of 1970s communist party, the Khmer Rouge.     The film is the second collaboration between director Greg Cahill and actress Sophea Pel, following the 2006 short film The Golden Voice about Cambodian singer Ros Serey Sothear.     The film was shot primarily in Cambodia and also in Los Angeles, California.

==Plot==
Cambodian-American hipster wannabe Sovanna opens a cryptic letter from Cambodia claiming that her long-lost brother and sister are still alive.  She travels to her birthplace alone to seek out her two siblings who disappeared during the civil war 20 years earlier. Upon discovering a girl who may or may not be her real sister, Sovanna is ensnared into an increasingly dangerous situation, pitting her in a tug-of-war between her own personal safety, and her compassion for a stranger.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 