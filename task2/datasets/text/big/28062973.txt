Kisses
 
{{Infobox film
| name           = Kisses
| image          = Kisses poster.jpg
| caption        = Theatrical release poster
| alt            =
| director       = Lance Daly
| producer       = Lance Daly Macdara Kelleher Peter Garde Les Kelly
| writer         = Lance Daly Shane Curry
| cinematography = Lance Daly
| editing        = J. Patrick Duffner
| studio         = Fastnet Films
| distributor    = Focus Features
| released       =  
| runtime        = 72 minutes
| country        = Ireland
| language       = English
| budget         = 
| gross          = $30,738 
}}

Kisses is a 2008 Irish drama film directed by Lance Daly. The film is a coming of age drama about two ragamuffin Preadolescence|preadolescents, next door neighbors each from dysfunctional families living in a poor area in the outskirts of Dublin, Ireland, who run away together one Christmas holiday.

==Plot==
 
 
Early in the film we meet Dylan (Shane Curry), approximately 11 years old, sitting on a couch absorbed in a handheld video game and attempting to ignore his fathers (Paul Roe) shouts from the kitchen where he is railing at a non-working toaster. We soon learn that rage is his fathers natural state; roughly kicked out of the house to "go play", Dylan talks to his next door neighbor, Kylie (Kelly ONeill), of approximately the same age, about what a "prick" his father is, and the wise decision of his brother to run away two years prior, to which Kylie observes that at least Dylans father is not in jail like most fathers in the neighborhood, implying that her father is incarcerated. She tells him about the "Sack Man", who shes heard kills kids, but Dylan says that it is just a story, like "Santa and God", used by adults to control kids.

Kylie is sent to "walk" her infant sibling in a stroller. When she returns she sees a motorcycle parked in the driveway of her house and her reaction, her hesitation to go into her house, tells us whoever has arrived makes her apprehensive. Inside she is cajoled by her mother to give her uncle a kiss. Her revulsion is evident. When he is leaving and comes to Kylies bedroom to say goodbye, Kylie hides under her bed. We see there that she has a stash of cash hidden away in a shoe.

The view returns to Dylans house, where his father is screaming at Dylans mother. When he punches her in the face, Dylan gets involved, slamming his handheld videogame into his father face which leaves a shard of plastic embedded in his fathers forehead. Dylan is chased by his father into an upstairs bathroom. With the door being busted in, Kylie comes to the rescue, maneuvering a ladder near the window from Dylans yard which allows him to just escape his fathers clutches. The pair run from the scene. They are next seen by a narrow river, where a dredger is passing; they climb aboard over the protests of the captain (David Bendito), a friendly sort, who gives them a ride to the end of the line, near Dublin and tells them about Bob Dylan—Dylans namesake.
 squat on Gardiner Street. Reaching there, they start knocking on random doors and asking passersby if they know him.  Ultimately following a lead to a Gardiner Street flat, a woman there informs them that he was kicked out for fighting six months earlier. Convinced they will not find his brother, Dylan argues that they have little choice but to go home; Kylie vehemently opposes this and runs off.

The two are reunited later when Dylan observes Kylie run into an alley, pursued by a worker of the place from which she stole food. He joins her in hiding, and they narrowly avoid detection. She adamantly says that she never wants to go home. Kylie reveals that her uncle sexually assaulted her, forcing her to go along with it and gaining her terrified silence by telling her that no one would believe her. Together they find some boxes to sleep on, but a man drives by and tries to convince Kylie to ride with him. She refuses, and the car drives away, only for the man to return and kidnap her.

Dylan gives chase, grabbing hold of the bumper of the car. His Heelys, bought earlier from Kylies savings, allow him to roll along behind the car despite its speed. He screams for help, and the car soon turns into another alley. As the men in the car try to get rid of him, Kylie breaks free of the car, and they both manage to lose their assailants. Realizing that they have feelings for each other, Kylie and Dylan share a passionate kiss. Laughing, they find boxes to sleep on and spend the night on the street.

In the morning, Kylie awakens to find a cold hand beside her, sticking out of the box pile. She reacts with horror, stumbling away and waking Dylan. The remove some boxes to reveal a man, recently dead. The next scene is the two of them in a police car, being returned to their houses. The glamour of living on the streets was obviously lost for them. Their families are waiting outside, concerned. Kylie and Dylan share a moment, staring at each other, and Kylie blows him a kiss before their parents bemusedly yank them away.

==Cast==
* Kelly ONeill as Kylie
* Shane Curry as Dylan
* Paul Roe as Dylans father (Da)
* Neilí Conroy as Dylans mother (Ma)
* Cathy Malone as Kylies mother (Ma)
* David Bendito as the Dredger captain
* Elizabeth Fuh as Gardiner Street tenant
* Stephen Rea as Bob Dylan tribute band singer

==Critical reception==
Kisses was met with critical acclaim among critics, receiving an overall 86% rating from all critics and 100% rating from top critics as of July 16, 2010 at the review aggegrator website, Rotten Tomatoes.  Rex Reed of The New York Observer called it a "poignant little film"    and "a serendipitous journey worth taking",  while Stephen Holden of The New York Times opined that Kisses was a "small slice of Irish kitchen sink realism embellished with fairy-tale fantasy, "Kisses" may strike you as either ingeniously magical or insufferably cute, depending on your taste. But more than the story, which circles back on itself, the natural performances of its young stars, Shane Curry and especially Kelly ONeill, nonprofessional actors, lend the movie a core of integrity."  Praise was not universal however. Kevin Maher of The Times wrote that the film was " ndeniably artful but wildly disingenuous... Like arthouse Roddy Doyle its ambitious and phoney in equal measure." 

==Accolades==
Kisses was nominated for and won multiple awards from various film organizations and festivals. 
It took the Festival Prize for Best Feature Film at the Foyle Film Festival in 2008; won Best Irish Feature Film at the Galway Film Fleadh in 2008;  the IFTA Award at the Irish Film and Television Awards for Best Director for Film and for Best Editing in 2009 and was also nominated there in categories for Best Actress in a Lead Role in a Film, Best Costume Design, Best Film, Best Hair & Make-Up and for Best Script for Film; and director Lance Daly received the Audience Award for the film at the Miami International Film Festival. Kisses was also an official selection at the Toronto International Film Festival, the Telluride Film Festival, the Locarno Film Festival and at the London Film Festival.

==Music==
The music for Kisses was written and performed by Go Blimps Go.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 