About Face (1942 film)
{{Infobox film
| name           = About Face
| image          = 
| alt            = 
| caption        =  Kurt Neumann
| producer       = Hal Roach Fred Guiol
| screenplay     = Eugene Conrad Edward E. Seabrook	
| starring       = William Tracy Joe Sawyer Jean Porter Marjorie Lord Margaret Dumont Veda Ann Borg Joe Cunningham Edward Ward
| cinematography = Paul Ivano
| editing        = Bert Jordan 
| studio         = Hal Roach Studios
| distributor    = United Artists
| released       =  
| runtime        = 43 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Kurt Neumann and written by Eugene Conrad and Edward E. Seabrook. The film stars William Tracy, Joe Sawyer, Jean Porter, Marjorie Lord, Margaret Dumont, Veda Ann Borg and Joe Cunningham. The film was released on April 16, 1942, by United Artists.  

==Plot==
 

== Cast == 
*William Tracy as Sgt. Dorian Dodo Doubleday
*Joe Sawyer as Sgt. William Ames
*Jean Porter as Sally
*Marjorie Lord as Betty Marlowe
*Margaret Dumont as Mrs. Culpepper
*Veda Ann Borg as Daisy, Blonde Hustler
*Joe Cunningham as Col. Gunning Harold Goodwin as Capt. Caldwell
*Frank Faylen as Bartender Jerry
*Dick Wessel as Bartender Charlie Charles Lane as Rental Car Manager 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 