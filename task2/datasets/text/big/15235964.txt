The Thrill Chaser
 
{{Infobox film
| name           = The Thrill Chaser
| image          = 
| caption        = 
| director       = Edward Sedgwick
| producer       = Hoot Gibson Carl Laemmle
| writer         = Richard Schayer Raymond L. Schrock Edward Sedgwick
| starring       = Hoot Gibson
| music          = 
| cinematography = Virgil Miller
| editing        = 
| distributor    = 
| released       =  
| runtime        = 60 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}

The Thrill Chaser is a 1923 American action film directed by Edward Sedgwick and featuring Hoot Gibson.

==Cast==
* Hoot Gibson as Omar K. Jenkins
* James Neill as Sheik Ussan
* Billie Dove as Olala Ussan
* W. E. Lawrence as Prince Ahmed (credited as William E. Lawrence)
* Bob Reeves as Lem Bixley
* Gino Corrado as Rudolph Biggeddo (credited as Gino Gerrado)
* Lloyd Whitlock as Abdul Bey
* King Baggot as Cameo appearance Reginald Denny as Cameo appearance
* Hobart Henley as Cameo appearance
* Norman Kerry as Cameo appearance
* Laura La Plante as Cameo appearance
* Mary Philbin as Cameo appearance
* Edward Sedgwick as Cameo appearance

==See also==
* Hoot Gibson filmography

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 