Gold Rush Daze
{{Infobox Hollywood cartoon
|cartoon_name=Gold Rush Daze
|series=Merrie Melodies
|image=
|caption=
|director=Ben Hardaway Cal Dalton
|story_artist=Melvin Millar
|animator=Gil Turner
|voice_actor=Mel Blanc
|musician=Carl Stalling
|producer=Leon Schlesinger
|distributor=Warner Bros., The Vitaphone Corporation
|release_date=February 25, 1939
|color_process=
|runtime=7:11
|movie_language=English
}}
Gold Rush Daze is a Merrie Melodies cartoon released on February 25, 1939.

==Plot==
A prospector drives to the hills to dig for gold. A local gas station attendant goes on to explain that the prospector is wasting his time: since 1849, he had been chasing gold strikes around the world and never achieving success. The film shows the attendants various stops including the California Gold Rush, the Comstock Lode, and various other short appearances that never (literally) pan out. Yet as the attendant finishes his story, a passerby spreads the news that there has indeed been gold found in the hills, to which the attendant steals the prospectors car to get back in on the gold rush and hands the prospector the gas station.

Included in the film is a short, farcical musical number, “My Sweetheart Needs Gold for Her Deed.”

==External links==
* 
* 

 
 
 
 
 
 
 


 