Drug War (film)
{{Infobox film
| name           = Drug War
| image          = DrugWarfilm.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Drug War poster
| director       = Johnnie To
| producer       = {{plainlist|*Johnnie To
*Wai Ka-Fai}}
| writer         = {{plainlist|*Wai Ka-Fai
*Yau Nai-hoi
*Ryker Chan
*Yu Xi
}}
| starring       = {{plainlist|*Louis Koo
*Sun Honglei Huang Yi
*Wallace Chung
}}
| music          = Xavier Jamaux
| cinematography = Cheng Siu-Keung
| editing        = Allen Leung
| studio         = {{plainlist|
*Beijing Hairun Pictures Milkyway Film Production}} Media Asia Distributions   Variance Films  
| released       =  
| runtime        = 105 minutes
| country        = {{plainlist|*China
*Hong Kong}}   
| language       = Mandarin 
| budget         = 
| gross          = US$24,676,341 
}}

Drug War (Du zhan 毒戰) is a Chinese-Hong Kong crime thriller film directed and produced by Johnnie To. The film stars Sun Honglei as Police Captain Zhang, who partners with a drug lord named Timmy Choi (Louis Koo) after Choi is arrested. To avoid the death penalty, Choi agrees to reveal information about his partners methamphetamine ring. Zhang starts to harbor doubts about Chois honesty as the police begin to take on the drug ring.

The film premiered at the Rome Film Festival on November 15, 2012. It has received positive reviews.
==Plot Summary==
Choi crashes his car into a restaurant and when he revives he is captured by Captain Zhang Lei.  Realizing that he will receive the death penalty for his crimes, he bargains information on his colleagues to survive.  First, he introduces Captain Zhang Lei as "Uncle Bill", to become a supplier to Haha, who owns a port and can distribute drugs to other countries.  Then Captain Zhang Lei poses as Haha to the real "Uncle Bill".  Then they go to Chois factory where they meet the mute brothers.  Then they setup the real Haha and catch him.  However, an attempt to capture the mute brothers at the factor fails, and they escape through a hidden path.  Captain Zhang is livid at Choi for withholding this information at the cost of the lives of several police team members.  Choi pleads for a second chance as "Uncle Bill" is really just a front for seven other powerful hong kong gangsters whom he did not wish to rat out because 2 of them are his relatives: one is his brother, and another is his godfather.  The next day Captain Zhang poses as Haha again and meets with "Uncle Bill".  They negotiate a deal to distribute drugs using his port, meanwhile Choi identifies the 7 gangster bosses.  Later they discuss terms of the deal at a nightclub, where Captain Zhang, posing as Haha, accuses "Uncle Bill" of being a cop and takes him into custody.  The big 7 then confront him in the parking garage, revealing their identities and confirming their business relationship.  The next day, Choi leads the big 7 and their entourage to exit 441 and they arrive at a school.  Choi removes his wires and reveals to the big 7 that he has ratted them out and they are surrounded by cops.  A shootout ensues and nearly everyone is killed except Choi, who escapes in a school bus.  However, he crashes into the escaping mute brothers who believe Choi to have ratted them out and another shootout ensues.  Captain Zhang, wounded, arrives with several other wounded comrades to stop Choi.  They are all killed by Choi, but Captain Zhang is able to handcuff himself to Chois leg before he dies.  Choi is then captured by SWAT reinforcements before he can escape.  The film ends with Choi begging for another chance to live by trading more information on other gangsters before he is killed by lethal injection.
==Cast==
 
*Louis Koo as Choi Tin-ming/Timmy Choi
*Sun Honglei as Captain Zhang Lei Huang Yi as Yang Xiaobei
*Wallace Chung as Guo Weijun
*Gao Yunxiang as Xu Guoxiang
*Li Guangjie as Chen Shixiong Guo Tao as Senior Dumb Li Jing as Junior Dumb
*Lo Hoi-pang as Birdie Eddie Cheung as Su
*Gordon Lam as East Lee
*Michelle Ye as Sal
*Lam Suet as Fatso 

==Production==
The film was billed as  Tos first action film to be entirely shot and set in Mainland China.      To had previously shot the romance film Romancing in Thin Air in China. 

==Style==
Variety (magazine)|Variety wrote that the film does not have the same feeling as Tos similar Hong Kong films such as Election (2005 film)|Election or Sparrow (2008 film)|Sparrow and that the film was also "actually quite light on action."  Film Business Asia noted that "To has modified his style to take account of the Mainlands different look and more spacious geography, as well as appearing to be newly energised by the challenge of what he can get away with." 

==Awards and nominations==
{| class="wikitable" cellpadding="5"
|-
! scope="col" style="width:120px;"|  Organization
! scope="col" style="width:160px;"|  Award category
! scope="col" style="width:180px;"|  Recipients and nominees
! scope="col" style="width:10px;"|  Result
|- Online Film Critics Society Awards 2013 Best Film
|
| 
|- Best Foreign Language Film
|
| 
|- Best Editing Allen Leung
| 
|- San Diego Film Critics Society Best Foreign Language Film
|
| 
|- 14th Chinese Film Media Awards  Best Film
|
| 
|- Best Director  Johnnie To
| 
|}

==Release==
Drug War premiered at the Rome Film Festival on November 15, 2012.     The film was the second "surprise film" from Asia at the festival, with the first being Back to 1942.     The film was released on April 2, 2013 in China and earned $13,070,000 for its opening week, coming in third place at the Chinese box office behind The Chef, the Actor, the Scoundrel and Finding Mr. Right.  The film grossed a total of $23,180,000 in China.    The film opened in Hong Kong on April 18, 2013 and grossed $376,577 in its opening weekend, making it the second highest grossing film at the Hong Kong box office, beaten only by Oblivion (2013 film)|Oblivion.  The film grossed a total of $639,155 in Hong Kong. 

The film continues to maintain popularity and was highlighted in the Masters section of the 2013 San Diego Asian Film Festival.   

Drug War was released on October 15, 2013 on DVD and Blu-ray Disc. 

==Remake==
In 2014, it was announced that there will be a South Korean remake of the film. 

==Reception== average score of 86, based on 19 reviews.  IndieWire gave the film a B rating, stating that the film confirms director Johnnie Tos status as "a first-rate genre filmmaker. That said, the film does not innovate or break any new ground".  Variety (magazine)|Variety praised the film, stating that it was "light on action but so well-scripted and shot, its nonetheless edge-of-your-seat material." 

At the 7th Asian Film Awards, Drug War was nominated for Best Film, Best Screenwriter (Wai Ka-fai, Yau Nai-hoi, Ryker Chan, Yu Xi), and Best Editing (David Richardson, Allen Leung). 

==Notes==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 