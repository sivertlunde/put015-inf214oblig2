Children of Sarajevo
 
{{Infobox film
| name           = Children of Sarajevo
| image          = Djeca (2012 film).jpg
| caption        = Film poster
| director       = Aida Begić
| producer       = Aida Begić
| writer         = Aida Begić
| starring       = Marija Pikić
| music          = 
| cinematography = Erol Zubčević
| editing        = Miralem Zubčević
| distributor    = 
| released       =     
| runtime        = 90 minutes
| country        = Bosnia and Herzegovina Germany France Turkey
| language       = Bosnian
| budget         = 
}}
 Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   

==Cast==
* Marija Pikić as Rahima
* Ismir Gagula as Nedim
* Bojan Navojec as Davor
* Sanela Pepeljak as Vedrana
* Vedran Đekić as Čiza
* Mario Knezović as Dino
* Jasna Beri as Saliha
* Nikola Đuričko as Tarik
* Staša Dukić as Selma
* Aleksandar Seksan as Rizo
* Velibor Topić as Mirsad Melić

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Bosnian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 
 
 
 
 