Tyrannosaur (film)
{{Infobox film
| name = Tyrannosaur
| image = Tyrannosaur poster.jpg
| caption = Tyrannosaur original poster by Dan McCarthy
| director = Paddy Considine
| producer = Diarmid Scrimshaw Mark Herbert
| writer = Paddy Considine
| starring = Peter Mullan Olivia Colman Eddie Marsan Paul Popplewell Sally Carman
| music = 
| cinematography = Erik Wilson
| editing = Pia Di Ciaula
| studio = Warp X Inflammable Films Film4 Productions UK Film Council Screen Yorkshire EM Media Optimum Releasing 
| distributor = StudioCanal UK  (UK)  Strand Releasing  (US) 
| released =   
| runtime = 92 minutes 
| country = United Kingdom
| language = English
| budget = £750,000 
| gross = £396,930 
}} British drama film written and directed by Paddy Considine in his Directorial debut. The film stars Peter Mullan, Olivia Colman, Eddie Marsan, Paul Popplewell and Sally Carman.

==Plot==
One night, unemployed widower Joseph (Peter Mullan) drunkenly kicks his dog Bluey to death. He buries the dog and goes to the post office, where he mocks some Asian workers then throws a rock through the window when they tell him to leave. At the pub, Joseph attacks two young men playing pool who were threatening and taunting him. He then hides in a second-hand shop, where employee Hannah (Olivia Colman) offers to pray for him. That night, Joseph goes to his home and is attacked by the men from the post office. The next morning, Joseph wakes up and goes to the shop, where Hannah looks after him. He begins to berate and insult her, before leaving.
 abusive husband James (Eddie Marsan) urinates on her. The next morning, Joseph apologises and slowly befriends Hannah. One day, she comes to the shop with a black eye and claims to have fallen in the bath. He asks her to pray for his best friend Jack (Robin Butler), who is dying of cancer. A few days later, Jack dies. James goes to the shop and finds them preparing for the funeral, where he silently threatens Hannah and Joseph. Joseph realises that James is beating Hannah and confronts her about the beatings. She constantly denies it.

One night, Hannah gets drunk and James comes to take her home. He hits her and she begins to mock him. She falls asleep and he comes to the bedroom, where he beats and rapes her. Hannah tells Joseph that she is leaving James and asks to stay with him. After a few days, Joseph tells her that she is not safe with him and suggests she leave. Joseph decides to confront James and goes to her house, where he finds she had stabbed James to death, presumably after the rape. He confronts Hannah and admits that he knows about the murder. She breaks down and reveals that James mutilated her reproductive system with a glass bottle and that she just wanted to be a mother.

A year later, Joseph prepares to visit Hannah in jail. It is revealed that his best friend, 6-year-old neighbor Samuel (Samuel Bottomley), was mauled by his mothers boyfriends dog. In retaliation, Joseph beheaded the dog with a machete for which he spent a few months in jail. After leaving jail, he quit drinking. The movie ends with Joseph walking down a path after visiting Hannah.

==Cast==
*Peter Mullan as Joseph
*Olivia Colman as Hannah
*Eddie Marsan as James
*Paul Popplewell as Bod
*Samuel Bottomley as Samuel
*Sian Breckin as Samuels Mum
*Ned Dennehy as Tommy
*Sally Carman as Marie
*Julia Mallam as Drunk Girl
*Natalia Carta as Drunk Girls friend

==Production== National Lottery fund through the UK Film Council. The remainder of the films budget came from Warp X, Inflammable Films, Film4 Productions|Film4, Screen Yorkshire, EM Media and Optimum Releasing (StudioCanal). It depicts an environment similar to what Considine witnessed growing up on a council estate in the Midlands (England)|Midlands, although the film is in no way autobiographical. The films title is a metaphor, the meaning of which is revealed in the film. 

The film is set in an unspecified town in the North of England. Although much of the film was shot on location in residential areas of Leeds and Wakefield, including Seacroft, Cross Gates, Eccup, Harehills and Alwoodley, in the Spring of 2010 and the accents of many of the main characters are drawn from a wide geographical area. The film  makes reference to the fictional Manners Estate as an area in the town where the more wealthy inhabitants reside. Manners Estate is the name of the council estate in the parish of Winshill near Burton-on-Trent where Paddy Considine grew up. 
 James Marsh and Gary Oldman.

==Soundtrack== cover of hit song Paint Your Wagon   
#"This Gun Loves you Back" – Chris Baldwin (written By Paddy Considine & Chris Baldwin) 
#"Truth or Glory" – JJ All Stars 
#"Saturday Night" – JJ All Stars 
#"Psycho Mash" – JJ All Stars 
#"Hi Jack" – Chris Wheat 
#"Sing All Our Cares Away" – Damien Dempsey 
#"We Were Wasted" – The Leisure Society 

Original music composed by Chris Baldwin & Dan Baker

==Reception==
===Box Office===
Tyrannosaur received a limited release in America in 5 theatres and grossed £13,871. The film grossed £383,059 internationally for a total of £396,930, below its £750,000 production budget.  

===Critical response===
Tyrannosaur received positive reviews and currently has a "certified Fresh" score of 83% on  , indicating "Generally favourable reviews".  

Stuart McGurk of GQ magazine called Tyrannosaur "The best British film of the year", whilst Empire (film magazine)|Empire said it was "Riveting, uncompromising, brilliant" and gave it 4/5 stars, as did Total Film, The Guardian, Sunday Mirror, and Evening Standard. The Daily Star Sunday and LoveFilm gave the film 5/5 stars and The Sunday Telegraph dubbed it "One of the most powerful films of 2011."

The American film critic and blogger Jeffrey Wells was so taken by Tyrannosaur after seeing it at the Los Angeles  Film Festival that he started Hollywood Elsewheres Tyrannosaur fundraising campaign with the idea of raising $2,000 to cover the rental of a screening room so that the film could be shown in Hollywood with the hope of gaining recognition. Wells claimed this was the first screening financed by a critic. 

Roger Ebert of the Chicago Sun-Times gave the film 3.5 stars out of 4, calling Peter Mullans performance muscular and unrelenting. He also remarked: "This isnt the kind of movie that even has hope enough to contain a message. There is no message, only the reality of these wounded personalities." 
 We Need to Talk About Kevin.

By 18 December 2011, the film had won 21 awards from 28 nominations worldwide.
 global trending of both Olivia Colman and Tyrannosaur on Twitter. 

==Accolades==
{| class="wikitable" style="font-size:100%;"
|-
! Year !! Group !! Award !!  Result
|- 2011
| Sundance Film Sundance International Film Festival Award
| The World Cinema Award for Directing: Dramatic
|  
|-
| World Cinema Special Jury Prize for Breakout Performance: Peter Mullan
|  
|-
| World Cinema Special Jury Prize for Breakout Performance: Olivia Colman
|  
|-
| Grand Jury Prize for World Cinema – Dramatic
|  
|-
| Nantucket Film Festival Award
| Best Writer/Director
|  
|-
| Munich Film Festival, Germany
| CineVision Award Outstanding Debut Feature
|  
|- Voices Festival of independent European Cinema, Russia
| Voices Festival Prize: Best Film
|  
|-
| Best acting prize: Olivia Colman
|  
|- Dinard British Film Festival, France
| The Golden Hitchcock: Grand Jury Prize/Ciné+ Award
|  
|-
| The Allianz Award: Best Screenplay
|  
|-
| Chicago International Film Festival
| Silver Hugo for Best Actress: Olivia Colman
|  
|-
| Zagreb Film Festival, Croatia
| T-Com Audience Award: Best Film
|  
|-
| Thessaloniki International Film Festival, Greece
| Fischer Audience Award (For a film in the Open Horizons section)
|  
|- Mar del Plata Film Festival, Argentina
| Jury Special Award
|  
|-
| Silver Astor for Best Screenplay
|  
|-
| Argentine Film Critics Association ACCA Award
|  
|-
| 2nd place SIGNIS (World Catholic Association for Communication) Award
|  
|-
| Stockholm Film Festival, Sweden
| Best First Feature
|  
|- British Independent Film Awards
| Best British Independent Film
|  
|-
| Best Director: Paddy Considine
|  
|-
| The Douglas Hickox Award  : Paddy Considine
|  
|-
| Best Actress: Olivia Colman
|  
|-
| Best Actor: Peter Mullan
|  
|-
| Best Supporting Actor: Eddie Marsan
|  
|-
| Best Achievement in Production
|  
|- International Press Academy Satellite Awards
| Best Actress in a Motion Picture: Olivia Colman
|  
|-
| Best Screenplay: Original
|  
|-
| Best First Feature
|  
|- 2012
| Independent Spirit Awards
| Best International Film
|  
|-
| The Guardian First Film Award 2012
| Best First Film
|  
|- London Critics Circle Film Awards
| The Virgin Atlantic Award – Breakthrough British Film-Maker: Paddy Considine
|  
|-
| The Moët & Chandon Award – British Actress of the Year: Olivia Colman
|  
|- War Horse)
|  
|-
| British Academy Film Awards (BAFTA)
| Outstanding debut by a British Writer, Director or Producer (Considine/Scrimshaw)
|  
|- Evening Standard British Film Awards
| Best Film
|  
|-
| Best Actor: Peter Mullan
|  
|-
| Best Actor: Olivia Colman
|  
|-
| Best Screenplay: Paddy Considine
|  
|-
| Kermode Award
| Best Actress: Olivia Colman (Shared with Tilda Swinton)
|  
|- Jameson Empire 2012
| Best British Film
|  
|-
|  Citroën Best Actress Award: Olivia Colman
|  
|- Bucharest International Film Festival (Bucuresti IFF) 2012
| Best Film
|  
|-
|  Critics’ Choice Award
|  
|-
| Transilvania International Film Festival, Romania
| FIPRESCI (International Federation of Film Critics) Award
|  
|}

==References==
 

==External links==
* 
* 
* , review by Mark Deming (3.5/5)

 

 
 
 
 
 
 
 
 
 
 
 
 
 