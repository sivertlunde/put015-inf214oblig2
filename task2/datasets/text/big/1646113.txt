The Sugarland Express
:"The Kenneth Hall.
 
{{Infobox film
| name = The Sugarland Express
| image = The Sugarland Express (movie poster).jpg
| caption= Original film poster
| director = Steven Spielberg David Brown Matthew Robbins 
| story = Steven Spielberg Hal Barwood Matthew Robbins Ben Johnson William Atherton Michael Sacks
| music = John Williams
| cinematography = Vilmos Zsigmond
| editing = Edward M. Abroms Verna Fields Universal Pictures
| released =  
| runtime = 110 minutes
| country = United States
| language = English
| budget = $3 million
| gross = $12.8 million
}} Ben Johnson, William Atherton, and Michael Sacks.
 Sugar Land, San Antonio, Lone Oak, Converse and Del Rio, Texas. 
 The Color Bridge of Spies being the only exceptions).

==Plot==
In May 1969, Lou Jean Poplin (Goldie Hawn) visits her husband Clovis Michael Poplin (William Atherton) to tell him that their son will soon be placed in the care of foster parents.  Even though he is four months away from release from the Beauford H. Jester Prison Farm in Texas, she forces him to escape to assist her in retrieving her child. They hitch a ride from the prison with an elderly couple, but when Texas Department of Public Safety Patrolman Maxwell Slide (Michael Sacks) stops the car, they take the car and run.

When the car crashes, the two felons overpower and kidnap Slide, holding him hostage in a slow-moving caravan, eventually including reporters in news vans and helicopters. The Poplins and their captive travel through Beaumont, Dayton, Houston, Cleveland, Conroe and finally Wheelock, Texas.  By holding Slide hostage, the pair are able to continually gas up their car, get food via the drive-through, and stay at motels.  Eventually, Slide and the pair bond and have mutual respect for one another.
 Ben Johnson). A pair of Texas Rangers shoot and kill Clovis and the Texas Department of Public Safety arrests Lou Jean. Patrolman Slide is found unharmed. Lou Jean spends fifteen months of a five-year prison term in a womens correctional facility.

==Production==
Film characters Lou Jean Poplin and Clovis Michael Poplin are based on the lives of Ila Fae Holiday and Robert Dent, respectively. The character Patrolman Slide is based on Trooper James Kenneth Crone|J. Kenneth Crone.

In real life, Ila Fae Holiday did not break Robert Dent out of prison.  Dent had been released from prison two weeks before the slow-motion car chase began. 

Steven Spielberg persuaded co-producers Richard Zanuck and David Brown to let him make his big-screen directorial debut with this true story. A year later, Spielbergs next project for Zanuck and Brown was 1975s blockbuster hit Jaws (film)|Jaws.

==Cast==
* Goldie Hawn as Lou Jean Poplin Ben Johnson as Captain Harlin Tanner
* Michael Sacks as Patrolman Maxwell Slide
* William Atherton as Clovis Michael Poplin
* Gregory Walcott as Patrolman Ernie Mashburn
* Steve Kanaly as Patrolman Jessup
* Louise Latham as Mrs. Looby

The actual kidnapped patrolman, J. Kenneth Crone, played a small role in the film as a deputy sheriff.

==Reception==
 
The Sugarland Express holds a 92% rating on Rotten Tomatoes with an average score of 7.2 out of 10 from 25 reviews. 

==Awards== Best Screenplay at the 1974 Cannes Film Festival.   

==References==
 

==External links==
*  
*  
*   A Story from the The Tuscaloosa News May 4 1969 about Robert and Ila Dent]

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 