The House on the Edge of the Park
{{Infobox film
| name           = The House on the Edge of the Park
| image          = TheHouseOnTheEdgeOfThePark.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Ruggero Deodato
| producer       = Franco Di Nunzio Franco Palaggi
| writer         = Gianfranco Clerici Vincenzo Mannino
| narrator       =
| starring       = David Hess Annie Belle Christian Borromeo Giovanni Lombardo Radice Marie Claude Joseph
| music          = Riz Ortolani
| cinematography = Sergio DOffizi
| editing        = Vincenzo Tomassi
| distributor    = United Artists (Italy)
| released        English
| budget         = Unknown
}}
 exploitation film Italian director The Last House on the Left (playing a similar character), and Giovanni Lombardo Radice. 

The entire film was shot in only three weeks in September 1979, under a very limited budget.

==Plot==
 
Alex (David Hess) is a darkly sinister thug driving around New York City at night when he spots a young woman (Karoline Mardeck) driving alongside him. He follows the woman to a nearby park where he cuts her off, gets out of his car, and runs into hers where he throws her into the back seat and proceeds to savagely rape her before strangling her to death. He takes her locket as a trophy to another one of his many killings.

Some time later, serial killer/rapist Alex is with his working-class friend Ricky (Giovanni Lombardo Radice), both of whom work in a local mechanic garage which is closing up for the evening. Alex is planning to go boogying at a local disco and Ricky is tagging along. Suddenly, a large Cadillac vehicle pulls into the underground garage which contains a young, well-dressed yuppie couple named Tom (Cristian Borromeo) and his girlfriend Lisa (Annie Belle) where they ask for assistance for their cars motor is making strange sounds. Alex refuses to help them saying that the garage is closed, and continues to refuse assistance even after Tom offers him money. But the slow-witted Ricky decides to help the couple and upon checking under the hood, finds a loose wire on the alternator and fixes it. Grateful, Tom tells Alex and Ricky that they are driving to a friends house in New Jersey for a party, and Alex asks them if he and Ricky could tag along, and Tom agrees. Before closing up the garage for the night, Alex stops by his locker which is filled with various weapons he uses to kill people. Alex picks out just one; a straight-edged razor, before closing up and getting into Tom and Lisas car with Ricky for the drive to New Jersey.

A little later, the four arrive at a large villa situated next to a large, dark, empty park where they are welcomed by the homes owner Gloria (Lorraine de Selle), and are introduced to her friends Glenda (Marie Claude Joseph), and Howard (Gabriele Di Giulio). But minutes after Alex and Rickys arrival, there are signs of tension as it becomes obvious that the rich people are looking for kicks, as are Alex and Ricky. It first starts when Ricky is forced to make a fool of himself when Gloria asks him to do a striptease to some disco music, while he is further humiliated by forced to drink some hard liquor with each move he makes. But Alex stops Ricky before he can take it all off.

While Tom, Howard, and Glenda play a card game of poker with Ricky, which is obviously rigged, Lisa begins sexually teasing Alex with her come-ons, and goes further to invite him to an upstairs bathroom to shower with her, only to push him away. As Alex grows more frustrated and angry, he later sees that the hosts are cheating at playing cards with Ricky. Finally, Alex pulls out the razor he has, and a fight breaks out between him and Howard. Alex throws Howard outside the back door where he endures a vicious beating and thrown into the backyard swimming pool, and further urinated upon by the laughing Alex, who drags Howard back inside and ties him to a piano leg, while he proclaims that he is running the party now.

At this point, the movie descends into an unrelenting catalogue of abuse and humiliation as Alex and Ricky further beat on their hosts-turned-hostages with Alex slashing Toms face with the razor, and beating his face into the poker table. Ricky holds the others at bay with a wine bottle and Alex corners Gloria and sexually fondles her. Lisa runs to an upstairs bedroom where she tries to escape, but Alex catches her and proceeds to rape her, while the movie soundtrack plays the romantic theme song "Sweetly."

When Alex takes Lisa downstairs to rejoin the other hostages, the front doorbell rings. Alex forces Gloria to answer and its a neighbor, Cindy, a local teenage girl. Alex grabs Cindy, while Gloria takes advantage of this to try to escape. Ricky, still holding the broken wine bottle, runs after her and catches up with Gloria on the perimeter of the grounds. Ricky shows Gloria that he means her no harm by throwing away the wine bottle. For no clear reason, Gloria responds to his simple nature by stripping off her clothes and seducing him.

Meanwhile back at the house, Alex cuts Cindys blouse off with the razor while singing. Ricky then returns to the house with Gloria just as Alex forces Cindy to strip off the rest of her clothes, and proceeds to savagely slash her nude body with his razor over and over again. At this point, Ricky makes the astonishing observation that things are getting a little out of hand and attempts to stop Alex. Upset at being "betrayed" by his friend, Alex turns against Ricky and slashes his abdomen wide open, and then breaks down at what a mess he made of Ricky.

Taking advantage of this distraction, the bloodied and battered Tom runs into the nearby study and pulls out a 9mm pistol out from a desk drawer... and the tables are instantly turned. Tom shoots Alex a few times, then kicks him back out the glass back door and into the back yard. Gloria and the other women untie Howard, and the five hosts descend upon the fatally wounded Alex lying on the ground. Tom removes the locket Alex is wearing and reveals the reason for all this: the woman that Alex raped and murdered in the opening scene was Toms sister and Tom wanted revenge, so he and Lisa wanted to lure them to Glorias house so they can kill them and make it look like self-defense. Tom shoots the wounded Alex in the groin, which causes him to fall into the backyard swimming pool after shouting on-screen for at least 15 seconds, in extreme slow-motion. Tom and Lisa take turns shooting Alex, who thrashes weakly in the water, before a final shot to the head by Howard finishes him for good.

Returning to the house, Howard wants to shoot Ricky, but Gloria stops him. Glenda tends to the wounded Cindy. Tom and Lisa go into the study where they talk about this plan of revenge they had and if they went too far for it to work. Tom says that despite some mistakes and inconsistencies, that it worked out for the best, and then picks up the phone to call the police.

==Controversy== BBFC when video nasties" when it was revealed that the uncut version was readily available on UK video. When it was eventually passed by the BBFC in July 2002, it was cut by 11 minutes and 43 secs, with almost all of the rape and violence either replaced or removed entirely. It was resubmitted in 2011 and received an almost uncut release, now only being cut by 42 seconds to the razor slashing.

The USA Media Blasters release is completely uncut at 91 minutes. 

Once during an interview about the making of the film, Ruggero Deodato was asked about his initial thoughts of the script when he first read it. He responded:

"I thought it was too violent. I make violent films, but softer ones. But this film was full of violence, and that made me uncomfortable. When I met David Hess, I thought that with my direction I could make him do anything. But when I first read it, I found it quite disturbing."

In 2006, the BBFC commissioned a group of academics at the University of Wales, Aberystwyth to conduct research into peoples responses to films that include scenes of sexual violence. The House on the Edge of the Park is one of the films included in their remit to examine. 

==Sequel==
On 27 February 2011, the Dread Central movie website exclusively reported that UK production company North Bank Entertainment are teaming up with director Ruggero Deodato and actor Giovanni Lombardo Radice for The House on the Edge of the Park Part II, a direct sequel to the original film.

==See also==

*List of films featuring home invasions

==Footnotes==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 