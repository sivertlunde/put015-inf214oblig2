The Desperate Hours (film)
:For the 1990 remake, see Desperate Hours.

{{Infobox film
| name =The Desperate Hours
| image =The-Desperate-Hours-Poster.jpg
| caption =The Desperate Hours film poster
| director = William Wyler
| producer = William Wyler
| writer =Joseph Hayes
| starring =Humphrey Bogart Fredric March
| music = Gail Kubik, Daniele Amfitheatrof (uncredited)
| cinematography =
| editing = Robert Swink
| distributor = Paramount Pictures
| released = October 5, 1955
| runtime = 112 minutes
| language = English
| budget = $2,388,000 (estimated)
| gross = $2.5 million (US) 
}} novel and play of Joseph Hayes which were loosely based on actual events.

The original Broadway production had actor Paul Newman in the Bogart role but he was passed over for the movie because Bogart was a much bigger star. The character was made older in the script so Bogart could play the part. Bogart said he viewed the story as "Duke Mantee grown up," Mantee having been Bogarts breakthrough movie role in The Petrified Forest. Spencer Tracy was first cast to be in the film with Bogart, but the two friends both insisted on top billing and Tracy eventually withdrew.  The role of Glenn Griffin was Bogarts last as a villain.
 Leave it to Beaver was used for exterior shots of the Hilliards home. In 1956, Joseph Hayes won an Edgar Award from the Mystery Writers of America for Best Motion Picture Screenplay.

== Plot ==

Glenn Griffin (Humphrey Bogart) is the leader of a trio of escaped convicts who invade the Hilliard familys suburban home in Indianapolis and hold four members of the family hostage. There they await the arrival of a package from Griffins girlfriend, which contains funds to aid the three fugitives in their escape.

Police organize a statewide manhunt for the escapees and eventually discover the distraught familys plight.  Griffin menaces and torments the Hilliards and threatens to kill them. Later the unfortunate garbage collector George Patterson (Walter Baldwin) who happens upon the situation after noticing Griffins car in the garage, is murdered in his truck in order to silence him.

Finally the father, Daniel Hilliard (Frederic March), after working with police to play a risky trick on Griffin, ends up throwing Griffin out of the house with Griffins own gun on him. Griffin is subsequently shot to death when he hurls his unloaded gun at a police spotlight and tries to make a break for it.

==Cast==
*Humphrey Bogart as Glenn Griffin 
*Fredric March as Daniel C. Hilliard  Arthur Kennedy as Deputy Sheriff Jesse Bard
*Martha Scott as Eleanor "Ellie" Hilliard  Dewey Martin as Hal Griffin 
*Gig Young as Chuck Wright  Mary Murphy as Cindy Hilliard
*Richard Eyer as Ralphie Hilliard
*Robert Middleton as Simon Kobish
*Walter Baldwin as George Patterson

==Background==
Actual events that took place on September 11 and 12 in 1952, wherein the five members of the Hill family were held hostage for 19 hours, inspired the 1953 Joseph Hayes novel which, in turn, inspired the 1954 play on which the movie was based. The Hill family (formerly of Whitemarsh, Pennsylvania) sued Time Inc.|Time, Inc., because Life (magazine)|Life magazine published an article in the February 1955 issue about the play, describing it as based on the actual events. The article was illustrated by staged photos with actors in the actual home that was the scene of the events, the Hills having moved away, making efforts to discourage publicity. The Hills complaint was that the article falsely described the actual events while claiming it represented the truth. Immediately following the home invasion event, Mr. Hill had told the press the family had not been molested or harmed, and in fact had been treated courteously. The Life magazine article, however, stated that some family members had been assaulted, profanity used, and in other ways - according to a New York appellate court - differed from the account Hill had given. Suing in a New York court, the plaintiffs relied on a New York statute which permitted damages suits for violation of the right of privacy only in instances of use of a persons name or picture for commercial purposes without consent. The statute, however, had been interpreted by the New York courts to make the truth of the publication a defense. The defense for Time, Inc., was that the matter was of general interest and the article had been published in good faith. A jury awarded compensatory and punitive damages, but the state appellate court awarded a new trial at which only compensatory damages could be considered, while sustaining liability. This order was affirmed by the highest state court.
 First Amendment Richard M. Nixon, at that time an attorney in private practice.  The Supreme Court thus made it extremely difficult even for ordinarily private persons to prevail in a defamation or "false light" invasion of privacy case. From the Supreme Court, the case was sent back in 1967, to the New York courts for disposition under this newly announced constitutional standard, probably involving a new trial, or perhaps summary judgment rendered on the basis of affidavits and depositions. 

==Remakes== David Morse. The remake, directed by Michael Cimino, received poor reviews.

The film was also remade in India as the Hindi film 36 Ghante (1974).

== See also ==

* List of films featuring home invasions

==References==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 