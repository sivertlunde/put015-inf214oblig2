Blinkity Blank
  engraved directly Maurice Blackburn along with graphical sounds created by McLaren scratching onto the films optical soundtrack. 

Blinkity Blank features lines, dots and other abstract forms, along with fruits, trees, planets and chickens—the latter featured at length in another McLaren hand-drawn film Hen Hop—which blink in and out of existence, or merge with or modulate other shapes. McLaren also left some frames blank, which he described as “sprinkling on the empty band of time”.   

It received the Short Film Palme dOr at the 1955 Cannes Film Festival,    the first prize for Best Animated Film at the BAFTA Awards as well as a Silver Bear at the Berlin International Film Festival. Blinkity Blank was produced by the National Film Board of Canada.   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 