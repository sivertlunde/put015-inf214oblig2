Miarka (1920 film)
{{Infobox film
| name = Miarka
| image =
| image_size =
| caption =
| director = Louis Mercanton
| producer = Louis Mercanton
| writer =  Jean Richepin  (novel)
| starring = Ivor Novello 
| music = 
| cinematography = 
| editing =     
| studio =Mercanton Films
| distributor = Mercanton Films
| released = 29 October 1920
| runtime = 
| country = France French intertitles
| budget =
| gross =
}} on location turned into a sound film of the same name.

==Cast==
* Desdemona Mazza 
* Jean Mercanton
* Marie Montbazon
* Ivor Novello
* Paul Numa
* Jean Richepin
* Gabrielle Réjane
* Charles Vanel

== References ==
 

==Bibliography==
* John Caughie, Kevin Rockett. The Companion to British and Irish Cinema. Cassell, 1996.
Levine, Alison. Framing the Nation: Documentary Film in Interwar France. A&C Black, 2011.

== External links ==
*  

 

 
 
 
 
 
 
 
 

 