Nanny McPhee and the Big Bang
 
 
{{Infobox film
 | name           = Nanny McPhee and the Big Bang
 | image          = Nanny mcphee and the big bang ver2.jpg
 | caption        = UK theatrical release poster
 | director       = Susanna White
 | producer       = Tim Bevan Eric Fellner Lindsay Doran
 | screenplay     = Emma Thompson
 | based on       =  
 | starring       = {{Plain list |
* Emma Thompson
* Rhys Ifans
* Maggie Gyllenhaal
* Asa Butterfield
* Ewan McGregor
* Maggie Smith
* Bill Bailey
* Katy Brand
}}
 | music          = James Newton Howard
 | cinematography = Mike Eley
 | editing        = Sim Evan-Jones 
 | studio         = {{Plain list |
* StudioCanal
* Relativity Media
* Working Title Films
* Three Strange Angels
}}
 | distributor    = Universal Pictures
 | released       =  
 | runtime        = 109 minutes   
 | country        = United Kingdom
 | language       = English
 | budget         = $35 million   
 | gross          = $93,251,121 
}}
Nanny McPhee and the Big Bang (released in the United States and Canada as Nanny McPhee Returns) is a 2010 British family film. It is a sequel to the 2005 film Nanny McPhee. It was adapted by Emma Thompson from Christianna Brands Nurse Matilda books.  Thompson reprises her role as Nanny McPhee, and the film also stars Maggie Gyllenhaal, Ralph Fiennes, Rhys Ifans, Ewan McGregor, Maggie Smith, Asa Butterfield,    Bill Bailey and Katy Brand.

==Plot==
On a farm during World War II, Mrs Isabel Green (Maggie Gyllenhaal) is driven to her wits end by her hectic life. Between trying to keep the family farm up and running and her job in the village shop, run by the slightly mad Mrs Doherty (Maggie Smith), she also has three boisterous children to look after, Norman (Asa Butterfield), Megsie (Lil Woods) and Vincent (Oscar Steer). All of this she has to do while her husband is away at war. So when her childrens two wealthy cousins, Cyril (Eros Vlahos) and Celia (Rosie Taylor-Ritson) are sent to live on their farm and another war is being fought between the two sets of children, she is in need of a little magic.
 ARP Warden, Mr Doherty (Sam Kelly), warns them all about bombs and how he imagines a pilot might accidentally release his bomb. At the end of the picnic Uncle Phil delivers a telegram saying that Mr Green has been killed in action in the war. Mrs Green believes the telegram, along with everybody else. But Norman says that he can "feel it in his bones" that his father is not dead. He tells this to Cyril, who at first says it is just because he is upset, but then agrees that Norman might be right, so the two boys decide to ask Nanny McPhee to take them to London, where Cyril and Celias father works.

They travel to London with Nanny McPhee and ask Cyrils father Lord Gray (Ralph Fiennes), who is very important in the War Office, what has happened to Mr Green. At first he sneers at Norman when he tells him about his disbelief of his fathers death, but after Cyril angrily blurts out that he knows that his parents are getting a divorce, Lord Gray goes to check what has happened. While he is gone, Cyril tells Norman that he and Celia have been sent away because their parents will be splitting up, and Norman asks where Cyril and Celia will live. When Cyril replies "with Mum I suppose, not that it makes much difference, she only ever really sees us when she wants to show us off", Norman tells Cyril that he and Celia are welcome to go and live on the farm with the Greens. Cyrils father returns and tells Norman that his father is not dead, but M.I.A. - missing in action, and that there has been no record of a telegram being sent to his mother.

The boys then leave, and Norman works out that the letter Uncle Phil had given to his mother was forged. While the boys were at the War Office, Megsie, Celia and Vincent were trying to stop Mrs Green from signing the papers and selling the farm. Just as Mrs Green is about to sign the papers, a German pilot accidentally drops a huge bomb, it shakes everything but does not explode and is left sticking out of the barley field. When Nanny McPhee returns with Norman and Cyril, the children go out for watching Mr Spolding dismantle the bomb, but he falls from the ladder and Megsie takes over, and succeeds with the help of the other children and Nanny McPhees raven, Mr. Edelweiss. After Nanny McPhee helps to harvest the barley, with a little magic, it is revealed that old Mrs Doherty is in fact baby Aggie from the first film about the Brown children (it is also revealed that Nanny McPhee has been staying with her throughout the movie as Nanny McPhee thanks Aggie for having her to stay). As Nanny McPhee walks away from the now happy family, the children and Mrs Green chase after her, only to see that Mr Green in army uniform and with an injured arm, is making his way over to them, he runs to the children and Mrs Green and they all hug.

===Nanny McPhees five lessons===
In the course of the story, Nanny McPhee teaches the children five lessons: (1) to stop fighting; (2) to share nicely; (3) to help each other; (4) to be brave; and (5) to have faith. 

==Cast==
* Emma Thompson as Nanny McPhee, the magical nanny who changes the lives of the Green and Gray children.
* Rhys Ifans as Phil Green - Norman, Megsie, and Vincents uncle who tries to sell the farm because he gambled it away at a casino.
* Maggie Gyllenhaal as Isabel Green (née Carrington) the frazzled mother of Norman, Megsie, and Vincent.
* Asa Butterfield as Norman Green, the eldest of the Green children.
* Lil Woods as Megsie Green, the middle and only girl of the Green children.
* Oscar Steer as Vincent Green, the youngest of the Green children.
* Ewan McGregor as Rory Green, the father of the Greens, away fighting in World War II.
* Eros Vlahos as Cyril Gray - the spoiled cousin of Norman, Megsie, and Vincent. He becomes kinder throughout the film and makes friends with Norman.
* Rosie Taylor-Ritson as Celia Gray - the other spoiled cousin of Norman, Megsie, and Vincent.
* Ralph Fiennes as Lord Gray - Cyril and Celias father, who is very high up in the War Office. first film grown up.
* Sam Kelly as Mr. Algernon Doherty - Mrs. Dohertys husband, who is an ARP (Air Raid Precautions) Warden.
* Sinead Matthews as Miss Topsey - a henchwoman of Mrs. Biggles, the woman who owns the casino at which Phil gambled the farm away.
* Katy Brand as Miss Turvey, the colleague of Miss Topsey.
* Bill Bailey as Farmer MacReadie, the farmer who buys the piglets from the Greens.
* Nonso Anozie as Sergeant Ralph Jeffreys - the guard at the War Office, and a former charge of Nanny McPhee.
* Daniel Mays as Blenkinsop - Cyril and Celias chaffeur.
* Ed Stoppard as Lieutenant Addis, a coworker of Lord Gray.
* Toby Sedgwick as an enemy plane pilot.

==Reception==

===Critical response===
Critical response for the film was positive. Review aggregation website Rotten Tomatoes gave the film a rating of 76% based on 115 reviews, with the sites consenus stating: "Emma Thompsons second labor of love with the Nanny McPhee character actually improves on the first, delivering charming family fare with an excellent cast." 
 Diary of The Karate Kid.

===Box office=== The Blind Side, grossing a total of £16,211,057. In the United States and Canada, it debuted in seventh position with a $8.4 million.    Gross exceeded $27 million. 

==Home media==
The film was released on DVD and Blu-ray Disc|Blu-ray in the UK on 19 June 2010. Nanny McPhee Returns (as the film was renamed for the North American market) was released on DVD and Blu-ray on 6 May 2011.

===Relation to Nurse Matilda===
The film is one of a wave of adaptations of the book trilogy by childrens author Christianna Brand, Nurse Matilda. The books are not directly connected to the film, but most scenes in the film are adapted from the books. Emma Thompson started to write the script based on Brands books in the spring of 2007. 

==Film locations== Senate House.   

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 