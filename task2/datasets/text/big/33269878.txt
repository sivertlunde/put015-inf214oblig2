Saarathi
{{Infobox film
| name           = Saarathee
| image          = Saarathi.jpg
| image size     = 200px
| caption        = Saarathi theatrical release poster
| director       = Dinakar Tugudeep
| producer       = K. V. Satya Prakash
| writer         = Dinakar Tugudeep   Chinthan
| screenplay     = 
| story          = 
| based on       =   Darshan  Sharath Kumar
| music          = V. Harikrishna
| cinematography = K. Krishna Kumar
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 200days
| country        = India
| language       = Kannada
| budget         =  10 Crores 
| gross          =  15+ crores 
}}
Saarathi (English: Charioteer) is a 2011 romantic action   who plays the female lead actress.  V. Harikrishna composed the music for the film and lyricist Nagendra Prasad penned the songs.

Saarathi was released on 30 September 2011 when the actor Darshan was under  , and an additional 20 screens were put up due to its commercial success in the opening weekend.  Finally, Darshan has admitted that the film Sarathi is the Kannada version of The Lion King. 

==Plot==
The story revolves around Raja (Darshan (actor)|Darshan), an Auto rickshaw|auto-rickshaw driver, who falls in love with Rukmini (Deepa Sannidhi). But she has an enemy in Pratap (played by Ajay), a relative who wants to marry her, and takes her to his hometown - Durgakote. Raja follows her to the place and finds that she hails from the family of palegars who rule the place. The story takes a turn when Raja finds that Rukmini is the daughter of his fathers sister. The movie ends with action sequences.

==Production==
Saarathi was initially under the production of Kannada cinema veteran KCN Chandrashekhar, but was later taken up by K. V. Satya Prakash as his first film venture.    As a result of which, the film has been in the making for over 2 years  including 90 days for the shooting and later the film release got delayed owing to actor Darshans arrest by the police following a complaint of domestic violence by his wife Vijayalakshmi.  It was filmed at major South Indian tourist places like Chitradurga, Pondicherry (city)|Pondicherry, Chalakudy (in Kerala) and Hyderabad, India|Hyderabad, the film ran 100days in around 16 theaters

==Reception== Chamundeshwari in water falls is also a noted aspect of the film. 

  and DNA India rated it at 4.5/5.    Bangalore Mirror summarised it by saying that "the real hero of the film is director Dinakar and it is a film to enjoy, being one of the biggest entertainers of the year." 

The movie completed 50 consecutive days in 93 theatres,totally it completed 100days in 16 theaters.
The Movie Released in 147 Theatres.

==Cast== Darshan as Raja, and later, Krishna
* Deepa Sannidhi as "Rukmini" Sarathkumar as Suryanarayana
* Rangayana Raghu
* Sharath Lohitashwa
* Ajay as "Pratap"
* Bullet Prakash
* Dileep
* Kote Prabhakar
* Lokesh Kademani (BGM)

==Sound Track==
The music and Background score was composed by V. Ramakrishna and lyrics for songs were penned by Nagendra Prasad
{{Infobox album  
| Name        = Saarathi
| Type        = Soundtrack
| Artist      = V. Harikrishna
| Cover       = 
| Alt         = 
| Released    = 30/9/2011
| Recorded    = Feature film soundtrack
| Length      = 23:01
| Label       = Aananda Audio
}}

{{Track listing
| total_length   = 23:01
| lyrics_credits = no
| extra_column	 = Singer(s)
| title1	= Kai Mugidhu Yeru 
| lyrics1 	= 
| extra1        = Shankar Mahadevan
| length1       = 5:00
| title2        = Manase Manase
| lyrics2 	= 
| extra2        = Vijay Prakash
| length2       = 4:47
| title3        = Vajra Ballalaraya
| lyrics3       = 
| extra3 	= Kailash Kher
| length3       = 4:43
| title4        = Haago Heege
| extra4        = Darshan (actor)|Darshan, Vaani Harikrishna
| lyrics4 	= 
| length4       = 4:57
| title5        = Kittappa Kittappa
| extra5        = Shamita Malnad, Shankar Mahadevan
| lyrics5 	= 
| length5       = 4:54
}}

== Awards and nominations ==
{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 1st South Indian International Movie Awards SIIMA Award Best Film
|K. V. Sathyaprakash

| 
|}block buster

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 