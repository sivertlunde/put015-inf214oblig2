Malaspina (film)
{{Infobox film
| name = Malaspina 
| image =
| image_size =
| caption =
| director = Armando Fizzarotti
| producer = Roberto Amoroso 
| writer =  Roberto Amoroso 
| narrator =
| starring = Vera Rol   Aldo Bufi Landi   Rino Genovese   Ugo DAlessio
| music = 
| cinematography = Roberto Amoroso 
| editing = 
| studio =   
| distributor = 
| released = 1 May 1947
| runtime = 90 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Malaspina is a 1947 Italian drama film directed by Armando Fizzarotti and starring Vera Rol, Aldo Bufi Landi and Rino Genovese. It is a melodrama, based on a popular song of the same name. Its story of female wrongdoing and ultimate redemption was characteristic of Neapolitan-style cinema. A young woman promises to be faithful to her lover when he goes off to fight in the Second World War. However, in his absence she becomes a prostitute and takes up with a notorious criminal. When her real love returns he kills her new boyfriend. Deeply ashamed of her conduct, she becomes a nun.

The film revived the Naples film-industry, which had largely disappeared during the Fascist era when Italian filmmaking was concentrated in Rome. The film was released in the United States, where it proved popular with Italian-American audiences. 

==Cast==
* Vera Rol as Maria, detta Malaspina  
* Aldo Bufi Landi as Andrea  
* Rino Genovese as Gaetano 
* Ugo DAlessio as Nicola  
* Vittoria Crispo as Teresina  
* Carmelo Capurro    
* Nicola Pouthod
*  Alberto Amato  
* Gino Vittorio

== References ==
 

== Bibliography ==
* Marlow-Mann, Alex. The New Neapolitan Cinema. Edinburgh University Press, 2011.
* Moine, Raphaëlle.  Cinema Genre. John Wiley & Sons, 2009.

== External links ==
*  

 

 
 
 
 
 
 
 
 