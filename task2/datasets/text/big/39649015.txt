Odysseus' Gambit
{{Infobox film
| name           = Odysseus’ Gambit
| image          = Theatrical Poster for Odysseus Gambit, 2012.jpg
| caption        = Theatrical Poster
| director       = Alex Lora Cercos
| producer       = Flavio Alves  Valérie Delpierre
| music          = 
| cinematography = Alex Lora Cercos
| editing        = 
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          = 
}}
 Odysseus Gambit  is a 2011 documentary film, directed by Alex Lora Cercos, about a homeless Cambodian immigrant who maintains his livelihood and sanity by playing chess in the heart of Manhattan. In 2012, Odysseus Gambit was nominated for the Best Short Filmmaking Award at the Sundance Film Festival.  

==Plot==
Saravuth Inn is a charismatic man living in New York’s Union Square. There, this American of Cambodian origin plays chess with people; losers pay him five dollars, enabling him to survive another day. A caring community of sorts has formed around Saravuth. Sometimes fellow players bring him food or hot drinks. Playing guitar also makes Saravuth’s life on the streets more bearable.

==Reception==
 Odysseus’ Gambit entered more than 70 film festivals and received multiple awards, including the prestigious Golden Dragon for Best Short at 52nd Krakow Film Festival and Best Documentary award at the 29th Tehran International Short Film Festival.   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 