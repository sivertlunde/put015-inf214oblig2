Tarnation (film)
{{Infobox film
| name = Tarnation
| image = TarnationPOSTER11.jpg
| caption = DVD cover
| director = Jonathan Caouette
| producer = Stephen Winter Jonathan Caouette
| writer = Jonathan Caouette
| starring = Jonathan Caouette
| music = John Califra Max Avery Lichtenstein
| cinematography = Jonathan Caouette
| editing = Jonathan Caouette Brian A. Kates
| distributor = Wellspring Media
| released =  
| runtime = 88 minutes
| country = United States
| language = English
| budget = $218.32
| gross = $1,162,014  
}} Super 8 footage, VHS videotape, photographs, and answering machine messages to tell the story of his life and his relationship with his mentally ill mother Renee.

Tarnation was initially made for a total budget of $218.32, using free iMovie software on a Macintosh computer|Mac.   As an early supporter, film critic Roger Ebert stated that $400,000 more was eventually spent by the distributor on sound, print, score and music/clip clearances to bring the film to theaters.  The film went on to win the Best Documentary Award from the National Society of Film Critics, the Independent Spirits, the Gotham Awards, as well as the L.A. and London International Film Festivals.

==Content==
Tarnation is an autobiographical documentary focusing on Caouettes early life and adulthood, as well as his mother, Renee LeBlanc, who was treated with electroshock in her youth. With an absent father and a mother who struggled with mental illness, Caouette eventually settled in the Houston area with his grandparents, Adolph and Rosemary Davis, who despite personality quirks provided a supportive family for him.  The film explores Caouettes life as he negotiates his complicated relationship with his mother as her child, friend and ultimately, parental figure while developing his creativity as an actor, writer and director.
 came out as gay at a young age and moved to New York City at age 25, eventually finding a boyfriend named David Sanin Paz. They both live in New York City today. As documented in the film, his mother has lived with them at times and theyve formed an unusual family. A scene early in the movie has an approximately 11-year-old Caouette improvising a monologue as a woman in an abusive relationship. 

==Soundtrack== Hex (Donnette The Cocteau Twins, Dolly Parton, Low (rock band)|Low, Mark Kozelek, Glen Campbell, The Magnetic Fields, Iron and Wine, The Chocolate Watch Band, Mavis Staples, Red House Painters, Marianne Faithfull and many more. Orchestral cues for the film were composed by John Califra.  Max Avery Lichtenstein wrote several original instrumental songs for the film, including the films recurring theme "Tarnation".

The films trailer features orchestral music by John Califra, "Tarnation" by Max Avery Lichtenstein, and the song "Safe As Milk" by the band Hopewell (band)|Hopewell.

No soundtrack compilation album has been released, but a digital E.P. featuring selections of Max Avery Lichtensteins original music for the film was released in 2005 by Tin Drum Recordings.

==History==
Caouette was shaping his material when he sent in an audition tape for John Cameron Mitchells Shortbus. The tape contained the footage of an 11-year-old Jonathan imitating a battered wife. Mitchell was impressed and encouraged him to continue working on the film. He alerted Stephen Winter, then the artistic director of MIX NYC, the New York Lesbian & Gay Experimental Film Festival. Stephen became the producer of Tarnation. A tape found its way to Mitchells friend, Gus Van Sant, who was also deeply moved by the film. Both he and Mitchell signed on as executive producers.   

The November 2003 world premiere of the movie at MIX in was much more abstract in nature, running about two hours. With input from Mitchell, Winter, and co-editor Brian A. Kates, Caouette shot new footage and edited the film down to about 90 minutes for its screening at the 2004 Sundance Film Festival in the Frontier Section.  There it was invited to appear in the 2004 Cannes Film Festival Directors Fortnight.  The filmmakers didnt have the $30,000 to make a film print for the festival but at the last minute respected art house distributor Wellspring picked it up and brought it to Cannes where it garnered great critical acclaim and worldwide distribution. 
 All Tomorrows Parties. 

Walk Away Renee (film)|Walk Away Renee, Jonathans companion film to Tarnation, examines a certain later period involving the filmmaker and his mother. It premiered at Cannes in 2011, and was released internationally in May 2012.

==References==
 

==External links==
* 
* 
* 
* 
*  
* 

 
 
 
 
 
 
 
 
 
 
 