Danielle Steel's A Perfect Stranger
{{Infobox film
| name = Danielle Steels A Perfect Stranger
| image =
| caption =
| writer = Danielle Steel Jan Worthington Patricia Brown Michael Miller
| producer = Douglas S. Cramer Darren Frankel Dennis Hammer
| music = Lee Holdridge Yuri Gorbachow Jeffrey Kimball
| editing = Gordon McClellan
| cinematography = Mike Fash
| distributor = The Cramer Company
| released =  
| runtime = 105 minutes
| country = United States
| language = English
| budget =
| gross =
}} name of Michael Miller, whose most important element is the love triangle which characterizes it. The special effects were created by Brock Jolliffe. The film is set in San Francisco, California  and was released in the USA in 1994.

==Plot==
Robert Urich is a lawyer whos fallen in love with a woman whos married to a dying millionaire and the whole story results in a love triangle.

==Cast==
* Robert Urich -  Alex Hale
* Stacy Haiduk - Raphaella Phillips
* Darren McGavin - John Henry Phillips
* Susan Sullivan - Kaye
* Holly Marie Combs - Amanda Hale
* Marion Ross - Charlotte Brandon
* Ron Gabriel - Fred
* Tamara Gorski - Sarah
* George R. Robertson - Richard Lance Patricia Brown - Mary
* Denise McLeod - Flight Attendant
* Margaret Ozols - German Housekeeper
* Adrian Truss - Publisher
* Natalie Gray - Flight Attendant (uncredited)
* Michal Page - Woman in airport (uncredited)

==External links==
*  

 
 
 
 
 

 