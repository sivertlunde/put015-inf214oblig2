The Love God?
{{Infobox Film   
| name           = The Love God?
| image          = The Love God?.jpg
| caption        = 
| director       = Nat Hiken 
| producer       = Edward Montagne
| writer         = Nat Hiken
| starring       = Don Knotts Anne Francis 
| music          = Vic Mizzy
| cinematography = William Margulies
| editing        = Sam E. Waxman
| distributor    = Universal Pictures
| released       = August 1969  
| runtime        = 101 min.
| country        = USA
| language       = English
| awards         = 
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1969 Universal Pictures feature film starring Don Knotts and film was written and directed by Nat Hiken,    who died after it was shot but before it was released in theaters. The film marks a change of pace for Knotts, who up to then, had appeared in G-rated family comedies. The Love God? was an attempt to integrate Knotts into the type of adult-related films that dominated the late 1960s and early 1970s.

==Plot==
Abner Peacocks beloved bird-watchers magazine, The Peacock, is in financial crisis. Desperate to stay afloat, Abner takes on a new partner, Osborn Tremain (Edmond OBrien), who has an agenda of his own: to publish a sexy gentlemans magazine, which he can only do by taking over Abners, since he has been convicted too often of sending obscene material through the mails.

Before the hapless bird-watcher can stop the Tremains, the first issue sells over 40 million copies and Abner becomes the unwilling spokesman for First Amendment rights. Swept up in adulation, the unwitting playboy quickly begins settling into the swinging bachelor lifestyle.

==Cast and characters==
*Don Knotts – Abner Audubon Peacock IV
*Anne Francis – Lisa LaMonica
*Edmond OBrien – Osborn Tremain James Gregory – Darrell Evans Hughes
*Maureen Arthur – Eleanor Tremain

==References==
 

== External links ==
*  

 
 
 
 
 
 


 