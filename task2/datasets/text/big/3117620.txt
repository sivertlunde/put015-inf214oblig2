Bill Cosby: Himself
{{Infobox film name        = Bill Cosby: Himself image       = Himself cosby.PNG writer      = Bill Cosby starring    = Bill Cosby director    = Bill Cosby producer    = Bill Cosby James B. Herring studio      = Jemmin, Inc. distributor = 20th Century Fox released    =   runtime     = 105 minutes country     = United States language    = English music       = Bill Cosby Stu Gardner awards      = budget      =
}}
Bill Cosby: Himself is a 1983 stand-up comedy film featuring Bill Cosby. Filmed before a live audience at the Hamilton Place Theatre, in Hamilton, Ontario, Cosby gives the audience his views ranging from marriage to parenthood. The film also showcases Cosbys trademark conversational style of stand-up comedy. For most of the performance, Cosby is seated at the center of the stage, only getting up to emphasize a joke.

Many of the comedic routines presented in the film were precursors to Cosbys successful sitcom   was also released on Motown Records.

The film is regarded by many as "the greatest stand-up concert movie ever." 

==Topics==
Nearly all of the movie concerns the trials and tribulations of raising children, frequently illustrated with anecdotes involving his own family. Occasionally he compares these with stories from his childhood. Other topics include grandparents, going to the dentist, and people who drink too much or take drugs.

==See also==
*Bill Cosby 77

==References==
 

==External links==
* 
* 

 
 
 
 
 


 