Paradise Lost 3: Purgatory
{{Infobox film
| name           = Paradise Lost 3: Purgatory
| director       = Joe Berlinger and Bruce Sinofsky
| producer       = 
| writer         = 
| starring       = Jessie Misskelley, Damien Echols and Jason Baldwin (the West Memphis Three)
| music          = Metallica
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Home Box Office (HBO)
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
| image          = Paradise Lost 3 Purgatory poster.png
}}

Paradise Lost 3: Purgatory is a 2011   and  . The three films chronicle the arrest, 18-year imprisonment, and eventual release of Damien Echols, Jason Baldwin, and Jessie Misskelley, otherwise known as the West Memphis Three. The films, directed by Joe Berlinger and Bruce Sinofsky, are considered to play a substantial role in generating publicity, awareness, and support for the innocence of the West Memphis 3.  

It aired on HBO on January 12, 2012. The film was also nominated for a Primetime Emmy Award for Exceptional Merit in Documentary Filmmaking (2012), and for an Academy Award for Best Documentary Feature.

==Description==
 s defense team has hired some of the most renowned forensic scientists to collect DNA and other evidence that had never been tested during the 1994 trials in hopes of getting a new trial. The defense teams and supporters of Echols, Jason Baldwin, and Jessie Misskelley have uncovered new details that occurred during the trial that led to guilty verdicts against them. 

Central are the allegations of jury misconduct with the jury foreman discussing the case with an attorney during the Echols-Baldwin trial and bringing Misskelleys confession into deliberations even though it was not let into evidence. The forensic experts have uncovered DNA and new witnesses that focus suspicion toward Terry Hobbs, the stepfather of one of the murder victims. 

A hair found in the ligature that bound one of the victims is a match to him, he has told several conflicting stories concerning his whereabouts during the time of the murders, and he has a history of violence against his wife and possibly his stepson. While many are convinced he should be considered a suspect, the West Memphis, Arkansas Police Department has only questioned him and to this day does not consider him a suspect. 

Appeals for a new trial based on the new evidence have been denied by the original trial judge. But in November 2010, the Arkansas Supreme Court threw out that ruling and granted an evidentary hearing scheduled for December 2011, to decide if the evidence is enough for a new trial. This brings new hope to the defendants and their supporters that they will finally get the fair trial they never got. 
 plead guilty but can maintain their innocence. They reluctantly accept the deal, after 18 years and 78 days, they walk free from prison.

==Release==
Originally intended to be another installment in which the three men remained in prison, the film was to premiere on the HBO network in November 2011.  The world premiere of the film was announced to occur at the Toronto International Film Festival in September 2011. 
 Best Documentary Feature at the 84th Academy Awards.  Interviews used for the film featuring the newly freed men began shooting the day following their release on August 20.  

The film, in its original form, still made its premiere at the Toronto International Film Festival, while the re-cut version premiered at the New York Film Festival.  The re-cut version premiered on October 10, 2011. The three men, accompanied by their families, attorneys, and supporters, attended the event.

On Tuesday, January 24, 2012, Paradise Lost 3: Purgatory was among five documentary features to be nominated for an Oscar in the 2012 Academy Awards ceremony.   

==References==
 

==External links==
* 
*Paradise Lost 3: Purgatory at HBO - http://www.hbo.com/documentaries/paradise-lost-3-purgatory/index.html

 
 

 
 
 
 
 
 
 