Days of Betrayal
{{Infobox film
| name           = Days of Betrayal
| image          = 
| caption        = 
| director       = Otakar Vávra
| producer       = Ladislav Kalas Gustav Rohan
| writer         = Miloslav Fábera Otakar Vávra
| starring       = Jiří Pleskot
| music          = 
| cinematography = Jaromír Sofr
| editing        = 
| distributor    = 
| released       =  
| runtime        = 227 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}
 Best Foreign Language Film at the 46th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Jiří Pleskot as Dr. Edvard Beneš
* Bohuš Pastorek as Klement Gottwald
* Gunnar Möller as Adolf Hitler
* Jaroslav Radimecký as Neville Chamberlain
* Martin Gregor as Edouard Daladier
* Bořivoj Navrátil as Sergey S. Alexandrovsky
* Otakar Brousek as Bonnet
* Josef Langmiler as Cooper
* Rudolf Krátký as Dr. Paul Schmidt - Hitlers interpreter
* Günter Zschieschow as K.H. Frank Fred Alexander as Josef Goebbels
* Rudolf Jurda as Hermann Göring

==See also==
* List of submissions to the 46th Academy Awards for Best Foreign Language Film
* List of Czechoslovak submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 