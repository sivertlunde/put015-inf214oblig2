Dawn of the Mummy
{{infobox film
| name           = Dawn of the Mummy
| image          = Dawn_of_the_mummy_poster.jpg
| imagesize      = 280px
| caption        = Original poster
| director       = Frank Agrama
| producer       = Frank Agrama
| writer         = Frank Agrama   Ronald Dobrin   Daria Price
| starring       = Brenda King   Barry Sattels   George Peck   John Salvo   Ibrahim Khan   Joan Levy   Ellen Faison   Dianne Beatty
| music          = Shuki Levy
| cinematography = Larry Revene   Sergio Rubini
| editing        = Jonathon Braun
| distributor    = Harmony Gold USA   Goldfrab Distribution 
| released       = December 11, 1981
| runtime        = 93 minutes
| country        = United States   Egypt   Italy
| language       = English
| budget         = $500,000
}}
Dawn of the Mummy is a 1981 Low-budget film|low-budget horror film directed by Frank Agrama, who also served as writer and producer on the film.

==Plot==
 
A photographer and a group of American models travel to Egypt for a fashion shoot.  While shooting in an ancient tomb, their hot lights accidentally revive a mummy and his followers, a band of flesh-eating zombies.

==Cast==
* Brenda King as Lisa
* Barry Sattels as Rick
* George Peck as Bill
* John Salvo as Gary
* Ibrahim Khan as Karib
* Joan Levy as Jenny
* Ellen Faison as Melinda
* Dianne Beatty as Joan
* Ali Gohar as Tarak
* Ahmed Rateb as Omar
* Bahar Saied
* Ali Azab
* Ahamed Labab as Ahamed
* Laila Nasr as High Priestess/Old Hag

==Production==
 
Dawn of the Mummy was filmed in Egypt    with a mostly Italian crew leading many to mistake it as an Italian film.   
 
Unlike tradition  interpretations of mummies in cinema, the films portrayal of the undead is quite unique. In the film the mummies are portrayed as ravenous flesh eaters, similar to the popular portrayal of zombies in which both share many similarities.   

Due to the films surprisingly graphic content the film was subject to several cuts in order to trim down the films more graphic scenes, approximately 1 minute and 32 seconds were cut from the film from 12 different scenes in the film. Some scenes included The Mummy ramming a machete into the character Taraks head, a man being gutted by the mummy, and scenes where the mummys undead servants feast on several characters. 

== Release ==

===Home media ===
Dawn of the Mummy has been released on VHS and DVD format.  The film was released on region 1 by Madacy Entertainment, as a VHS transfer.  The packaging is said to list the films Theatrical Trailer and other Theatrical Trailers, although there is only one on the disc, it is not for the main feature, it is of a 1997 film, Road Ends.  It does however feature an audio commentary track by director Frank Agrama. Brian Lindsey 
 DTS Digital Surround, Dolby Digital 2.0 & 5.1 Surround. The extras include Directors Audio Commentary, Stills Gallery, Production Notes and Trailer. 

==Reception==
 
The film has received mixed to negative reviews from critics.

John Stanley awarded the film 2 / 4 stars stating, "Hashish smokers in a Cairo square are as exciting as it gets in this tale".   
Popcorn Pictures.com gave the film 6/10 stating, "It is an arduous struggle to get past the first half of Dawn of the Mummy but stick with it and you’ll be rewarded with one of the more entertaining zombie flicks of its period: a guilty pleasure of trashy exploitation at it’s finest. If the entire film had been as enjoyable as the last half, you’d be looking at a bonafide classic right here". 

It was awarded a score of 0 / 4 by VideoHounds Golden Movie Retriever which panned the films plot and acting. 

==Remake== Prisoners of Roger Christian and stars Joss Ackland, Carmen Chaplin, Nick Moran and John Rhys-Davies.  The film will release in April 2015 in the United Kingdom on DVD and Blu-ray. 

==References==
 

==External links==
*  

 
 
 
 
 
 