Purushotthama (film)
 
{{Infobox film|
| name = Purushotthama
| image = 
| caption =
| director = M. S. Rajashekar
| writer = T. N. Narasimhan
| starring = Shivrajkumar  Shivaranjini   Madhubala   Srinath
| producer = Madhu Bangarappa
| music = Hamsalekha
| cinematography = V. K. Kannan
| editing = S. Manohar
| studio = Sri Renukamba Combines
| released =  
| runtime = 140 minutes
| language = Kannada
| country = India
| budget =
}} Kannada drama romance drama film directed by M. S. Rajashekar and produced by Madhu Bangarappa. The film features Shivarajkumar, Shivaranjini and Madhubala in the lead roles.  The films soundtrack composed by Hamsalekha proved to be very successful.

== Cast ==
* Shivarajkumar 
* Shivarajini
* Madhubala
* Srinath Balakrishna
* Mukhyamantri Chandru
* Thoogudeepa Srinivas
* Vajramuni Ashok
* Prithviraj
* Sudheer

== Soundtrack ==
The soundtrack of the film was composed by Hamsalekha. 

{{track listing 
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Shiva Shiva Rajkumar
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Madhuvade Neenu
| extra2 = S. P. Balasubrahmanyam & Manjula Gururaj
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Supero Supero Hudugi
| extra3 = S. P. Balasubrahmanyam, K. S. Chithra & Manjula Gururaj
| lyrics3 = Hamsalekha
| length3 = 
| title4 = Kanchana Kanchana
| extra4 = Shivarajkumar & Manjula Gururaj
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Naanu Nimmavanu Rajkumar
| lyrics5 = Hamsalekha
| length5 = 
}}

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 