Enrai
{{Infobox film
| name           = Enrai
| image          = 
| image_size     = 
| caption        = 
| kanji          = 遠雷
| kana           = 
| romaji         = 
| director       = Kichitaro Negishi
| producer       = 
| writer         = Haruiko Arai
| narrator       = 
| starring       = Toshiyuki Nagashima Johnny Okura Eri Ishida
| music          = 
| cinematography = Shouhei Andou
| artdirector    = 
| editing        =  ATG New Century Producers (NCP) Nikkatsu ATG
| released       = October 24, 1981
| runtime        = 135 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}} Japanese film directed by Kichitaro Negishi.

==Synopsis==
Enrai is a low-key study of a farmer, Mitsuo Wada, in 1980s Japan when modernization and urbanization were threatening rural areas. After his father leaves his wife to run off with his mistress, Mitsuo struggles to preserve his livelihood with the help of his mother, grandmother, and his arranged marriage bride, Ayako.

==Cast==
* Toshiyuki Nagashima as Mitsuo Wada
*   as Hirotsugu Nakamori
* Eri Ishida as Ayako Hanamori
*   as Kaede
*   as Mitsuos father
*   as Mitsuos mother
*   as Mitsuos grandmother
* Yumiko Fujita as Chii
* Keizō Kanie as Kaedes husband

==Background==
Enrai is based on the 1980 novel of the same name by Wahei Tatematsu. The film, financed by Art Theatre Guild|ATG, Nikkatsu, and the smaller Nikkatsu-related company New Century Producers, marked director Negishis breakthrough into mainstream film after several Pink film#Second wave (The Nikkatsu Roman Porno era 1971–1982)|Roman porno features for Nikkatsu.   Enrai was released in Japan as a VHS tape by   in July 1998  and subsequently as a DVD in February 2008 by  . 

==Awards and nominations==
3rd Yokohama Film Festival  
* Won: Best Director - Kichitaro Negishi
* Won: Best Screenplay - Haruiko Arai
* Won: Best Cinematography - Shouhei Andou
* Won: Best Actor - Toshiyuki Nagashima

6th Hochi Film Awards 
* Won: Best Film
* Won: Best Actor - Toshiyuki Nagashima
* Won: New Face Award - Eri Ishida

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 


 