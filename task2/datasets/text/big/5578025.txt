Kamen Rider 555: Paradise Lost
{{Infobox film
| name           = Kamen Rider 555: Paradise Lost
| film name      = {{Film name
| kanji          = 劇場版 仮面ライダー555 パラダイス・ロスト
| romaji         = Gekijōban Kamen Raidā Faizu: Paradaisu Rosuto}}
| director       = Ryuta Tasaki
| writer         = Toshiki Inoue
| based on       =  
| producer       =   
| starring       =  
| music          = Hayato Matsuo
| cinematography = Fumio Matsumura
| editing        = Naoki Osada Toei
| Toei Co. Ltd 
| released       =  
| runtime        = 81 minutes
| country        = Japan
| language       = Japanese}}
 , was released during the run of the series, as a   on August 16, 2003. The catchphrases for the movie are   and  .

==Plot== Kamen Rider Psyga.

Meanwhile, in a human refugee camp known as "Paradise," Takumi has been living a different life as a cobbler named Takeshi and shares a home with a girl named Mina. When Yuji, Naoya and Yuka approach the human resistance and notify them of Psygas power, Paradise is suddenly invaded by Riotroopers. During the attack, Masato Kusaka battles Psyga as Kamen Rider Kaxia, but is quickly defeated and killed. Psyga attempts to take the Kaixa Belt, but he is warded off by the three Orphnochs allied with the humans. That night, Takeshi and Mari meet at a masquerade ball, but when Riotroopers ambush the party, Takeshi regains his memories as Takumi and once again becomes Kamen Rider Faiz - who is reunited with Auto Vajin while dispatching the attackers. Takumi, however, is not pleased that Mari had spread rumors of him being mankinds savior while he was away. Furthermore, members of the human resistance suspect he is an Orphnoch himself due to his ability to become Faiz. Mina reveals to Takumi that years ago, her father found him badly wounded from the previous Riotrooper attack and implanted false memories in him. Shortly after giving Takumi back his Faiz Blaster, Mina is fatally shot by resistance member Mizuhara - who steals the Faiz Belt. Yuji kills Mizuhara in self-defense and recovers the Faiz belt before handing it back to Takumi, but the resistance exile Yuji, Naoya and Yuka from the group until they prove themselves they can steal the Emperor Belts.
 Kamen Rider Orga. Orga forces Takumi out of his Kamen Rider form but witnesses him transform into the Wolf Orphnoch, to everyones surprise. Yuji is forced to change into Horse Orphnoch to battle Takumi before both combatants change back into their Kamen Rider modes while Mari is captured again to be fed to the Elasmotherium Orphnoch. Faiz changes into Blaster Form to defeat Orga. When Mari is about to be eaten, Orga sacrifices himself to save her while Faiz uses his Faiz Blaster to destroy the monster. A dying Yuji makes Takumi promise to fulfill his dream of peace between the humans and Orphnochs, while Smart Lady has Murakamis head crushed under executive orders. As the crowd stares at Takumi and Mari in silence, the pair walk out of the arena with Takumi telling Mari they will go anywhere until they find a place to live.

==Production==
Kamen Rider 555: Paradise Lost had scenes filmed at Ajinomoto Stadium. On the ending credits, a large "THANK YOU!" sign ("SEE YOU AGAIN!" in the Directors Cut) is made out of the names of the crowd members in the stadium for the final battle scene.

==Movie-exclusive Kamen Riders==

===Kamen Rider Psyga=== Rose Ophnoch Kyoji Murakami. Created alongside the Orga Gear, the Psyga Gear is regarded as a "perfect" gear, and therefore it can only be worn by Orphnochs. It is not stated in the film which Orphnoch Leo is; in the Super Imaginative Chogokin|S.I.C Hero Saga prequel story Lost World, he is the first Lion Orphnoch (a different character is the second Lion Orphnoch in the film). Psyga appears to be as fast and strong as the existing Gears, nearly matching Kamen Rider Faiz Axel Form for speed and exceeded Kaixa for strength. Because the weapons appear once the armor is formed and are created based on trial data from the previous gears, it possesses much greater functionality and ability than the previous Riders. Psyga also has the Flying Attacker jetpack that he uses to travel with. It also acts as one of Psygas weapons, transforming into a "Blaster Rifle Mode" in which the engines turn into either rapid fire ray guns or discharge a single blast of energy. Psyga is also armed with the Psyga Tonfa Edges, which are used in his "Tonfa Edge EX" Exceed Charge attack. Despite his more advanced Rider gear, the more experienced Faiz hits the Sparkle Cut through the Gear itself and into Leos stomach, destroying the Gear and killing Leo, reducing him to dust.

===Kamen Rider Orga===
In the events of the film, Yuji Kiba is brainwashed by the Smart Brain corporation and is chosen to be the user of the Orga Gear, allowing him to transform into   by inputting the code 000 into the Orga Phone, and then placing it into the Orga Driver belt. The Orga gear was recorded as the fifth Rider Gear created by Smart Brain, after the Faiz and Kaixa Gears were stolen by the humans and the Riotrooper gear had undergone mass-production. Created alongside the Psyga Gear, the Orga Gear is regarded as a "perfect" gear, and therefore it can only be worn by Orphnochs. Kamen Rider Orga has a more ornate appearance than any of the previous Gears, the Rider form having robes made of Soul Foam for a majestic appearance. Orga is armed with the Orga Stlanzer sword which he uses in the Orga Slash Extreme Charge. He is sent to fight Kamen Rider Faiz after Psyga is defeated. Faiz is finally able to defeat Kamen Rider Orga with Blaster Forms Rider Kick, but does not kill him. Instead, Orga dies while trying to hold off the Elasmotherium Orphnoch so Faiz can destroy it.

==Cast==
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  ,  ,  
* :  ,  
* :  
* :  
* :  

==Theme song==
*"Justi&phi;s -Accel Mix-"
**Lyrics: Shoko Fujibayashi
**Composition:  
**Arrangement:   ISSA

 
 

 
 
 
 
 