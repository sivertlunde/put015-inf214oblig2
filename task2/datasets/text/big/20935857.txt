Heatwave (film)
 
 
{{Infobox film name           = Heatwave image          = Heatwave (film).jpg caption        = producer       = Hilary Linstead director       = Phillip Noyce writer         = Mark Stiles Tim Gooding Phillip Noyce Marc Rosenberg starring  Bill Hunter music          = Cameron Allan cinematography = Vincent Monton editing        = John Scott distributor    = Umbrella Entertainment released       =   runtime        = 93 minutes country        = Australia language       = English budget         = AU $1 million  gross          = AU $267,000
}}
Heatwave is a 1982 Australian film directed by Phillip Noyce based on the murder of Juanita Nielsen. It was the second of two films inspired by this story that came out around this time, the first being The Killing of Angel Street (1981).

==Plot summary==
Around Christmas time a heatwave hits Sydney and an architect undertakes a controversial project.

==Cast==
*Judy Davis as Kate Dean
*Richard Moir as Stephen West
*Chris Haywood as Peter Houseman Bill Hunter as Robert Duncan
*John Gregg as Philip Lawson
*John Meillon as Freddy Dwyer

==Production==
The original script was called Kings Cross and was written by Tim Gooding and Mark Stiles. The final script was by Phil Noyce and Mark Rosenberg. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p215  Phil Noyce:
 Heatwave was the story of a working-class Protestant boy who made good. I dont know whether audiences realised that, but we had always assumed that he was a working-class Protestant and that Judy Daviss character was a middle-class Catholic girl. She, in the Catholic saintly tradition, had adopted a social cause - had set herself up as the spokesperson and protector of the working class. He, as a working-class boy, of course, was now forced to confront the moral implications of his own success and how that affected other people. In a way, the religious and ethnic backgrounds of the two characters were just a continuation of the conflicts that we had seen in Newsfront, but Australia had by this stage moved from a principally working-class and upper-class society to a principally middle-class society. Thats captured in the atmosphere of inner Sydney, its buildings and the regulations of law and government.  

==Reception==
Reflecting on the film Noyce said:
 I’d have no doubt shot it differently … told the story differently, today... Maybe that’s because I’m more conservative. I might have made the connections between the conspirators more certain, rather than implied. Heatwave belongs to a different era in Australian cinema, a time when we took a lot risks. I guess that comes with youth – the youth of the director and the youth of that second new wave of filmmakers. It was a time when there was a love affair between audiences and Australian cinema, something which these days is rather on and off.   accessed 17 October 2012  

==Awards== AFI Awards in 1982.

==Box office==
Heatwave grossed $267,000 at the box office in Australia,  which is equivalent to $776,970 in 2009 dollars.

==Home Media==
Heatwave was released on DVD by Umbrella Entertainment in July 2007. The DVD is compatible with all region codes and includes special features such as the theatrical trailer, Umbrella Entertainment trailers, a stills gallery and an interview with Phillip Noyce titled Sweating It Out.   

==See also==
*Cinema of Australia

==References==
 

==Further reading==
* 

==External links==
*  
*  at Oz Movies
 

 
 
 
 
 
 
 