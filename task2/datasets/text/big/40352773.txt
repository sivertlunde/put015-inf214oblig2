Amazing Grace (1992 film)
{{Infobox film
| name           = Amazing Grace 
| image          = 
| alt            = 
| caption        = 
| director       = Amos Guttman
| producer       = Dagan Price
| writer         = Amos Guttman
| starring       = Gal Heuberger, Sharon Alexander, Aki Avni, Rivka Michaeli, Karin Ophir
| music          = Arkadi Duchin
| cinematography = Yoav Kosh, Amnon Zlayet
| editing        = Einat Glaser-Zarhin
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = 
}}
Amazing Grace is a 1992 Israeli film directed by Amos Guttman.   

It is one of the few Israeli films dealing with gay subjects and the only one so far which has dared to deal with AIDS. The film includes a biography details the life of Guttman and it was his last film before he died of AIDS.         

==Awards==
The film won the Wolgin Award at the Jerusalem Film Festival and Best Film award at the Haifa International Film Festival.   

==Plot==
The young Jonathan (Gal Heuberger) leaves his familys home in the Moshav, moves to "the big city" of Tel Aviv, to find an alienated and alone world. He falls in love with Thomas (Sharon Alexander), an HIV patient, and a touching and painful love story develops between the two.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Sharon Alexander || Thomas
|- 
| Aki Avni || Miki, Jonathans room-mate
|- 
| Dvora Bartonov || Thomas Grandmother friend
|-
| Gal Heuberger || Jonathan
|- 
| Rivka Michaeli || Yehudit (Thomas Mother)
|-
| Hinna Rozovska || Thomas Grandmother 
|- 
| Ada-Valery Tal || Neighbour 
|- 
| Dov Navon ||  
|- 
| Karin Ophir || Jonathans little sister
|- 
| Iggy Vaxman ||  
|- 
|}

==References==
 

 

 
 
 
 
 
 