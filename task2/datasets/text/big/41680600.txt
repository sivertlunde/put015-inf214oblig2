Standing Still (film)
{{Infobox film
| name           = Standing Still
| image          = Standing Still 2005 Poster.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Matthew Cole Weiss
| producer       = {{Plainlist|
* Trent Othick
* Matthew Perniciaro
* Jeff Rice
}}
| writer         = {{Plainlist|
* Matthew Perniciaro
* Timm Sharp
}}
| starring       = {{Plainlist|
* Jon Abrahams
* Amy Adams
* Roger Avary
}}
| music          = BC Smith
| cinematography = Robert Brinkmann
| editing        = Andy Blumenthal
| studio         = Insomnia Entertainment
| distributor    = Freestyle Releasing
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $1,700,000
| gross          = 
}}
Standing Still is a 2005 American romantic comedy film directed by Matthew Cole Weiss and starring Jon Abrahams, Amy Adams, and Aaron Stanford. Written by Matthew Perniciaro and Timm Sharp, the film is about a group of lifelong friends who reunite at a wedding and revisit their complicated relationships of the past. The film was Matthew Cole Weiss feature film debut as a director.   

==Cast==
* Jon Abrahams as Pockets
* Amy Adams as Elise
* Roger Avary as Franklin Brauner
* Xander Berkeley as Jonathan
* Ethan Embry as Donovan
* Adam Garcia as Michael
* Lauren German as Jennifer
* Colin Hanks as Quentin
* Melissa Sagemiller as Samantha
* Aaron Stanford as Rich
* Mena Suvari as Lana
* James Van Der Beek as Simon
* Marnette Patterson as Sarah
* Nikki Ziering as Prostitute / Cop
* Dakota Sky as Kid
* Buck Kartalian as Older Man
* James Duval as Store Clerk
* Bret Roberts as Taxi Driver
* Matthew Cole Weiss as Captain
* Michael B. Healey as Blackjack Dealer

==References==
 

==External links==
*  
*  

 

 
 
 

 