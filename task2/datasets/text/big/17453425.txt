Abby in Wonderland
 

{{Infobox film
| name           = Abby in Wonderland
| image          = Abby_in_Wonderland.jpg
| alt            = 
| caption        = DVD coverart
| film name      = 
| director       = Kevin Clash
| producer       =  
| writer         = Christine Ferraro
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          =  
| cinematography = 
| editing        =  
| studio         = Sesame Workshop
| distributor    =  
| released       =  
| runtime        = 45 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Abby in Wonderland is a Sesame Street direct-to-video|direct-to-DVD film that adapts Lewis Carrolls Alice in Wonderland with the Muppets of Sesame Street. Abby Cadabby stars in the "Alice (Alices Adventures in Wonderland)|Alice" role.  Thanksgiving 2008.  It was aired again on December 29, 2008 on local PBS stations.

==Plot==
Abby wants very much to be in her own fairy tale. Elmo takes an interest in Abbys wish and
tells her the story of "Alice in Wonderland". For a second Abby falls asleep and she opens her
eyes to see that she is wearing a blue dress and blouse and Elmo has turned into a rabbit and
is in a great hurry. 

Abby follows Elmo down a tunnel. At the bottom she loses her wand to Elmo, who disappears at
the end of the tunnel. Abby notices there is a small door, but its locked. She finds the key to
the door on a table that just appeared. She unlocks the door, but shes too big to fit through
the little door. With that she closes, locks the door and puts the key back on the table. Then
Abby notices there is a bottle that just appeared. Before Abby can drink it, the bottle tells
her not to drink him, but say things that rhyme with drink. Abby does and she shrinks to the
right size to go through the door, but she recalls she locked it and left the key on the table,
where it is out of her reach. Abby finds a cookie who tells her to say things that rhyme with
eat. Abby says so many rhymes, she grows pretty big, but she manages to balance her size,
grab the key and shrink herself to a small size. Abby takes Bottle and Cookie with her as she
unlocks the door and enters a flowerbed.
 Counterpillar and his partner Little Rose-Ita, Cheshire Cookie Cat appears. He points Abby in the direction of a tea party.
 Mad Hatter, Elmo and Zoe (Sesame Street)|Mousie. To Abbys surprise, the Hatter doesnt serve tea, but letters T in the cups. The Hatter shows Abby his collection of hats. After that Abby asks Elmo for her wand, but with the hole in his pocket, he dropped it somewhere. The Cheshire Cookie Cat appears, guzzles up all the scones and cookies and
scares away Mousie.
 The Grouch of Cards". The king has Abbys wand thinking its a new royal scepter. Abby tries to get her wand back, challenging the king to a game of croquet, but he cheats. Elmo protests at the kings decision to keep the wand. Before his grouchlings can throw out Abby and Elmo, they both use Cookie to grow large. They scare the grouchlings and the king away. Once Abby and Elmo restore themselves to their original size, Abby reclaims her wand. Abby is unable to magic herself back home, but Elmo reminds her shes still dreaming.

Back in Sesame Street Elmo shakes Abby awake. Although Abby sort of got her wish to be in a fairy tale, she thinks shes better off being in a fairy tale when shes a little older. With that Abby and Elmo go off to play.

==Cast== Alice
* Red Rabbit
* Jerry Nelson as Count von Count as the Caterpillar (Alices Adventures in Wonderland)|Counter-pillar
* Caroll Spinney as Oscar the Grouch as the Grouch of Hearts  Cheshire Cookie Cat Zoe as Mousey the Hatter Helper
* Tyler Bunch as Bottle Bert as Tweedle Dum  Rosita as Little Rose Ernie as Tweedle Dee
 John Kennedy, Matt Vogel.

==Reception==
DVD Verdict gave the film a positive review, noting that while the original Alice in Wonderland story "kooky and fanciful, but also a little creepy", the Sesame Street version was sanitized for its target audience by polishing the originals "rough edges" and "removing any real sense of danger." They further noted that there is a place for some scary childrens tales, but not on Sesame Street. While the film is targeted to a very young audience, some of the jokes are for adults. An example given is on how Ernie explains to Bert "that, despite popular misconception," the characters of Tweedledum and Tweedledee are "not actually in this story".  It was noted that while the songs written by Mark Radice are bright and fun, "they dont quite live up to the best tunes Sesame Street has to offer."   It was concluded "There are a lot of bad remakes of Alice in Wonderland. This isnt one of them."   

==References==
 

==External links==
*   at Internet Movie Database
*  


 

 
 
 
 
 
 
 