Half Moon (film)
{{Infobox film
| name = Half Moon
| image = Half Moon poster.jpg
| caption =
| writer = Bahman Ghobadi
| starring = Golshifteh Farahani, Ismail Ghaffari, Allah-Morad Rashtian, Hedye Tehrani
| director = Bahman Ghobadi
| producer = Simon Field, Keith Griffiths, Abbass Ghazali, Bahman Ghobadi, Behrooz Hashemian, Hedye Tehrani
| music = Hossein Alizadeh
| distributor =   (USA) 
| released =  
| runtime = 114 minutes
| country = Iran, Austria, France, Iraq Persian
| budget =
}} directed by Kurdish filmmaker Bahman Ghobadi. Half Moon is a joint production of Iran, Austria, France and Iraq. This movie was commissioned by the New Crowned Hope festival, a celebration of the 250th birthday of Wolfgang Amadeus Mozart, and the story plot has been inspired in part by Mozarts Requiem (Mozart)|Requiem.

==Plot==
Mamo, an old Kurdish musician in the twilight of his life, plans to perform one final concert in Iraqi Kurdistan. The villages elderly warn him that as the moon becomes full, something awful would happen to him and urge him not to proceed with his plan. After several months of trying to overcome the red-tape, he begins a long and dangerous journey along with his sons. Along the way, the group picks up female singer Hesho who resides in a village of 1,334 exiled women singers. This adds to the complications of the trip as Hesho did not have authorization to go into Iraq. Despite all these obstacles, Mamo is determined to  continue with his journey across the border.

==Critical reception==
Critics gave the film generally favorable reviews. As of December 14, 2007, the review aggregator Rotten Tomatoes reported that 100% of critics gave the film positive reviews, based on 23 reviews.  On Metacritic, the film had an average score of 72 out of 100, based on 9 reviews. 

==Awards==
#Peoples choice Award, International Competition, Istanbul International Film Festival, 2007.
#Best Cinematography, San Sebastián International Film Festival, 2006.
#FIPRESCI Prize, San Sebastián International Film Festival, 2006.
#Golden Seashell, San Sebastián International Film Festival, 2006.

==References==
 

==External links==
* 
* 
* 
* 
* , The Boston Globe.
* , Variety (magazine)|Variety.
* , Hollywood Reporter.
* , New Crowned Hope Festival, Vienna, 2006.
*   by Laurie Munslow, close-upfilm.com

 

 
 
 
 
 
 
 
 


 