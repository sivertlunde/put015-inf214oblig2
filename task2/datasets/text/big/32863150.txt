Der Herr im Haus
 Man of the House (disambiguation) Man of the House}}
{{Infobox film
| name           = Der Herr im Haus
| image          =
| image_size     =
| caption        =
| director       = Heinz Helbig
| producer       = Karl Schulz (line producer)
| writer         = Jacob Geis Heinz Helbig
| narrator       =
| starring       = See below
| music          = Leo Leux
| cinematography = Karl Puth
| editing        = Margarete Steinborn
| studio         =
| distributor    =
| released       = 1940
| runtime        = 80 minutes
| country        = Nazi Germany
| language       = German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Der Herr im Haus is a 1940 German film directed by Heinz Helbig.

The film is also known as A Man in the House (Belgian English title) and The Landlord in Australia.

== Plot summary ==
 

== Cast == Hans Moser as Napoleon Bonaparte
*Maria Andergast as Christa Schellenberg
*Elise Aulinger as Haushälterin
*Leo Slezak as Kammersänger Wolfram Schellenberg
*Hermann Brix as Klaus Frank
*Thea Aichbichler as Mrs. Anger Hans Junkermann as Egon von
*Julia Serda as Amalie von
*Rudolf Schündler as Ferdinand von
*Fritz Odemar as Menarek
*Paul Westermeier as Karl
*Friedrich Ulmer as Bongelstedt
*Egon Brosig as Heller
*Karl Skraup as Oberkellner
*Hans Schulz as Sturm
*Ludwig Schmid-Wildy as Lakai
*Heinrich Hauser as Antiquitätenhändler Klaus Pohl as Granseder
*Meta Weber
*Fanny Schreck
*Anita Düwell
*Else Kündinger
*Rudolf Stadler
*Senta Esser
*Kurt Uhlig
*Vladimír Pospísil-Born
*Josef Hagen
*Maria von Krüdener

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 


 
 