Pavangal Pennungal
{{Infobox film 
| name           = Pavangal Pennungal
| image          =
| caption        =
| director       = Kunchacko
| producer       = M Kunchacko
| writer         = P. K. Sarangapani
| screenplay     = P. K. Sarangapani
| starring       = Prem Nazir Adoor Bhasi K. P. Ummer N. Govindankutty
| music          = G. Devarajan
| cinematography = NA Thara
| editing        =
| studio         = Excel Productions
| distributor    = Excel Productions
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed and produced by Kunchacko . The film stars Prem Nazir, Adoor Bhasi, K. P. Ummer and N. Govindankutty in lead roles. The film had musical score by G. Devarajan.    This was Prem Nazirs 300th film and was an average hit.

==Cast==
*Prem Nazir
*Adoor Bhasi
*K. P. Ummer
*N. Govindankutty
*S. P. Pillai
*Ushakumari
*Vijayasree

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aalundelayundu || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 2 || Kunjalle Pinchukunjalle || P Jayachandran, Ambili || Vayalar Ramavarma || 
|-
| 3 || Onnaam Ponnona || K. J. Yesudas, P Susheela, Chorus || Vayalar Ramavarma || 
|-
| 4 || Paavangal Pennungal || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 5 || Pokoo Maraname || P Jayachandran || Vayalar Ramavarma || 
|-
| 6 || Prathimakal || P Jayachandran, P. Madhuri || Vayalar Ramavarma || 
|-
| 7 || Swarnakhanikalude || P Susheela, P. Leela, P. Madhuri || Vayalar Ramavarma || 
|-
| 8 || Thuramukhame || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 