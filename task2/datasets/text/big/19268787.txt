Nor the Moon by Night
 
{{Infobox film
| name           = Nor the Moon by Night
| image          = Ntmbnpos.jpg
| image_size     = 
| caption        = Film poster
| director       = Ken Annakin
| producer       = John Stafford
| writer         = Guy Elmes Joy Packer (novel)   Michael Craig Belinda Lee Patrick McGoohan James Bernard
| cinematography = Harry Waxman
| editing        = 
| distributor    = Rank Organisation
| released       = 1958
| runtime        = 84 minutes
| country        = UK
| language       = English
| budget         =  
| gross          = 
}} 1958 UK|British Michael Craig. It was based on the novel by Joy Packer and filmed in the Kruger National Park South Africa.  The title comes from a passage in the Bible; "The sun shall not smite thee by day, nor the moon by night." Psalm 121:6

The film was released in the United States as Elephant Gun.

==Cast==
*Belinda Lee ...  Alice Lang   Michael Craig  ...  Rusty  
*Patrick McGoohan  ...  Andrew Miller  
*Anna Gaylor  ...  Thea Boryslawski  
*Eric Pohlmann  ...  Boryslawski  
*Pamela Stirling  ...  Mrs. Boryslawski  
*Lionel Ngakane  ...  Nimrod  
*Joan Brickhill  ...  Harriet Carver  
*Ben Heydenrych  ...  Sergeant Van Wyck  
*Alfred Kumalo  ...  Chief  
*Doreen Hlantie  ...  Oasis

 

==External links==
* 
*  at BFI Screenonline

 

 
 
 
 
 
 
 
 
 
 


 