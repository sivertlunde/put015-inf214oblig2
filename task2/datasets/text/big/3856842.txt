Hollywood Canteen
 
  Thanksgiving Day), canteen was allied countries ticket for admission was his Military uniform|uniform, and everything at the canteen was free of charge.

  unions of entertainment industry building renovations.  The Canteen was operated and staffed completely by volunteers from the entertainment industry.  By the time the Canteen opened its doors, over 3000 stars, players, directors, producers, grips, dancers, musicians, singers, writers, technicians, wardrobe attendants, hair stylists, agents, stand-ins, publicists, secretaries, and allied craftsmen of radio and screen had registered as volunteers.
 wait on cook in clean up. One of the highlights for a serviceman was to dance with one of the many female celebrities volunteering at the Canteen. The other highlight was the entertainment provided by some of Hollywoods most popular stars, ranging from radio stars to big bands to novelty acts.  On September 15, 1943, the one millionth guest walked through the door of the Hollywood Canteen. The lucky soldier, Sgt. Carl Bell, received a kiss from Betty Grable and was escorted in by another beautiful star, including Marlene Dietrich.

A Hall of Honor at the Hollywood Canteen had a wall of photos which honored the film actors who served in the military.
 Hollywood Canteen. Starring Joan Leslie and Robert Hutton, the film had scores of stars playing themselves.  It was directed by Delmer Daves, who also wrote the screenplay.  At the time the Canteen closed its doors, it had been host to almost three million servicemen.

==Canteen site today==
Today, the site of the Original Hollywood Canteen is occupied by a parking garage for the office building at 6430 Sunset Boulevard.

==Reuse of the name==
Fifty years after the closure of the Hollywood Canteen, a private business opened under the same name as a private restaurant and night club. It does not cater to military servicemen and women.

==Volunteers==
Among the many celebrities who donated their services at the Hollywood Canteen were:
 
*Bud Abbott and Lou Costello
*June Allyson
*Brian Aherne
*Don Ameche Eddie Rochester Anderson
*The Andrews Sisters
*Dana Andrews
*Eve Arden
*Louis Armstrong
*Jean Arthur
*Fred Astaire
*Mary Astor
*Lauren Bacall
*Lucille Ball
*Tallulah Bankhead
*Theda Bara
*Lynn Bari
*Jess Barker
*Diana Barrymore
*Ethel Barrymore
*Lionel Barrymore
*Count Basie
*Anne Baxter
*Louise Beavers
*Wallace Beery
*William Bendix
*Constance Bennett
*Joan Bennett
*Jack Benny
*Edgar Bergen
*Ingrid Bergman
*Milton Berle Julie Bishop
*Mel Blanc
*Ann Blyth
*Humphrey Bogart
*Ray Bolger
*Beulah Bondi William Boyd
*Charles Boyer
*Clara Bow
*El Brendel
*Walter Brennan
*Fanny Brice Joe E. Brown Les Brown
*Billie Burke
*George Burns & Gracie Allen
*Spring Byington
*James Cagney
*Cab Calloway Rod Cameron
*Eddie Cantor
*Judy Canova
*Kitty Carlisle
*Jack Carson
*Adriana Caselotti
*Charlie Chaplin
*Marguerite Chapman
*Cyd Charisse
*Charles Coburn
*Claudette Colbert Jerry Colonna
*Ronald Colman
*Perry Como
*Chester Conklin
*Gary Cooper
*Joseph Cotten
*Noël Coward James Craig
*Bing Crosby
*Joan Crawford
*George Cukor
*Xavier Cugat
*Cass Daley
*Dorothy Dandridge
*Linda Darnell
*Bette Davis
 
*Doris Day
*Yvonne De Carlo
*Gloria DeHaven
*Dolores Del Rio
*William Demarest
*Olivia de Havilland
*Cecil B. DeMille
*Marlene Dietrich
*Walt Disney
*Jimmy Dorsey
*Tommy Dorsey
*Irene Dunne
*Jimmy Durante
*Deanna Durbin
*Nelson Eddy
*Duke Ellington
*Faye Emerson
*Dale Evans
*Jinx Falkenburg
*Alice Faye
*Louise Fazenda
*Gracie Fields
*Barry Fitzgerald
*Errol Flynn
*Kay Francis
*Jane Frazee
*Joan Fontaine
*Susanna Foster
*Eva Gabor
*Ava Gardner
*Judy Garland
*Greer Garson
*Lillian Gish
*James Gleason
*Betty Grable
*Cary Grant
*Kathryn Grayson
*Sydney Greenstreet
*Paulette Goddard
*Samuel Goldwyn
*Benny Goodman
*Jack Haley Margaret Hamilton
*Phil Harris
*Moss Hart
*Helen Hayes
*Dick Haymes
*Susan Hayward
*Rita Hayworth
*Sonja Henie
*Paul Henreid
*Katharine Hepburn
*Darla Hood
*Bob Hope
*Hedda Hopper
*Lena Horne
*Edward Everett Horton
*Marjorie Hoshelle
*Ruth Hussey
*Betty Hutton
*Frieda Inescort
*Harry James
*Gloria Jean
*Anne Jeffreys
*Allen Jenkins
*Van Johnson
*Al Jolson Jennifer Jones
*Marcia Mae Jones
*Boris Karloff
*Danny Kaye
*Buster Keaton
*Ruby Keeler
*Evelyn Keyes
*Andrea King
*Gene Krupa
*Kay Kyser
*Alan Ladd
*Bert Lahr
 
*Elsa Lanchester
*Angela Lansbury
*Veronica Lake
*Hedy Lamarr
*Dorothy Lamour
*Carole Landis
*Frances Langford
*Charles Laughton
*Peter Lawford
*Gertrude Lawrence
*Peggy Lee
*Pinky Lee
*Mervyn LeRoy
*Vivien Leigh
*Joan Leslie Ted Lewis
*Beatrice Lillie
*Mary Livingston
*June Lockhart
*Anita Loos
*Peter Lorre
*Myrna Loy
*Keye Luke
*Bela Lugosi
*Ida Lupino
*Diana Lynn
*Marie McDonald
*Jeanette MacDonald
*Fred MacMurray
*Irene Manning
*The Marx Brothers
*Herbert Marshall
*Victor Mature
*Elsa Maxwell
*Louis B. Mayer
*Hattie McDaniel
*Roddy McDowall
*Frank McHugh
*Victor McLaglen
*Butterfly McQueen
*Lauritz Melchior
*Adolphe Menjou
*Una Merkel
*Chef Milani
*Ray Milland
*Ann Miller
*Glenn Miller
*Carmen Miranda
*Robert Mitchum
*Maria Montez
*Jackie Moran
*Dennis Morgan Ken Murray
*The Nicholas Brothers
*Ramon Novarro
*Jack Oakie
*Margaret OBrien
*Virginia OBrien
*Donald OConnor
*Maureen OHara
*Oona ONeill
*Maureen OSullivan
*Merle Oberon
*Eugene Pallette
*Eleanor Parker
*Louella Parsons John Payne
*Gregory Peck
*Mary Pickford
*Walter Pidgeon
*Zasu Pitts
*Cole Porter
*Dick Powell
*Eleanor Powell
*Jane Powell
*William Powell
*Anthony Quinn
*George Raft
 
*Claude Rains
*Basil Rathbone
*Martha Raye
*Donna Reed Bill "Bojangles" Robinson
*Edward G. Robinson
*Ginger Rogers
*Roy Rogers
*Cesar Romero
*Mickey Rooney
*Jane Russell
*Rosalind Russell
*Ann Rutherford
*Peggy Ryan
*S.Z. Sakall
*Olga San Juan Ann Savage
*Hazel Scott
*Lizabeth Scott
*Randolph Scott
*Toni Seven
*Norma Shearer
*Ann Sheridan
*Dinah Shore
*Sylvia Sidney
*Phil Silvers
*Ginny Simms
*Frank Sinatra
*Red Skelton
*Alexis Smith
*Kate Smith
*Ann Sothern
*Jo Stafford
*Barbara Stanwyck Craig Stevens
*Leopold Stokowski
*Lewis Stone
*Gloria Swanson
*Elizabeth Taylor
*Shirley Temple
*Danny Thomas
*Gene Tierney
*Lawrence Tibbett
*Martha Tilton
*Claire Trevor
*Sophie Tucker
*Lana Turner
*Spencer Tracy
*Gloria Vanderbilt
*Beryl Wallace
*Nancy Walker
*Bunny Waters
*Ethel Waters
*John Wayne
*Clifton Webb
*Virginia Weidler
*Johnny Weissmuller
*Orson Welles
*Mae West
*Alice White
*Paul Whiteman
*Margaret Whiting
*Esther Williams
*Warren William
*Chill Wills Marie Wilson
*Jackie Wise
*Jane Withers
*Teresa Wright
*Anna May Wong
*Constance Worth
*Jane Wyman
*Keenan Wynn
*Rudy Vallee
*Lupe Vélez
*Loretta Young Robert Young
*Darryl F. Zanuck
*Vera Zorina
 

==References==
 
 

 
 
 
 
 
 
 