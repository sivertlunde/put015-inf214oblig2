A propósito de Sudán
{{Infobox film
| name           = A propósito de Sudán (Journey of Hope)
| image          = 
| caption        = 
| director       = Lidia Peralta, Salah Elmur
| producer       = Producciones Damira
| writer         = 
| starring       = 
| distributor    = 
| released       = 2009
| runtime        = 25 minutes
| country        = Spain Sudan
| language       = 
| budget         = 
| gross          = 
| screenplay     = Lidia Peralta, Salah Elmur
| cinematography = Lidia Peralta, Salah Elmur
| editing        = Wafir Sheikh-Eldin, Mosab Amore, Nasser Mohamed, Lidia Peralta
| music          = Wafir Sheikh-Eldin, Al-haqiba Music
}}

A propósito de Sudán is a 2009 documentary film directed by Lidia Peralta and Salah Elmur.

== Synopsis ==
By means of five 5 minute films, About Sudan submerges us in some of the most deeply rooted activities in daily life. Based primordially on sound and images, we shall become acquainted with the coffee ceremony, see what happens around a 70 m deep well, meet the Nile, Sufism and travel in a peculiar public transport: the rackshaw. Stroke brushes filled with curious facts about a surprising country.

== References ==
 

 
 
 
 
 
 
 
 
 
 


 
 