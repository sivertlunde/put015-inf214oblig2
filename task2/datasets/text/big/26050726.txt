Loha (1987 film)
Loha Mandakini and Amrish Puri. The Film became a box office "hit" surfacing as one of the years highest grossing films. The film became Dharmendras first hit of the year 1987, where he went on to deliver 7 more outright hits and hence, represented one of his best career years as well as an all-time record year for any Hindi film star. The films music were also superhit at the time most notably, "Isa Pir na musa pir, sabse bada hain paisa pir" picturised beautifully on the male leads of the  film.

==Story==
Honest and diligent Police Inspector Amar (Dharmendra) is a middle-clas man living in Bombay. In his attempt to arrest bandit Sher Singh (Amrish Puri), he and Inspector Dayal (Raza Murad) are attacked. Amar survives, but Dayal gets his legs crushed under a truck and becomes wheelchair-bound. Subsequently, Amar arrests local politician, Jagannath Prasad (Kader Khan), who gets released without being charged while Amar gets reprimanded, and decides to resign. Shortly thereafter, Sher Singh hijacks a bus and holds the passengers as hostages in exchange for 25 of his jailed associates. Dayals granddaughter, Seema (Mandakini (actress)|Mandakini), is amongst them, and Dayal asks Amar for assistance. Amar, along with ex-convict, Qasim Ali (Shatrughan Sinha), and Karan (Karan Kapoor), a drug-dealer, does manage to rescue them but differences crop up between the trio and they part ways. Then Amar finds out that Qasim and Karan have masterminded a plan to facilitate the escape of the 25 convicts, and decides to confront them.

==Cast==
*Dharmendra ...  Senior Police Inspector Amar
*Shatrughan Sinha ...  Qasim Ali Barkat Ali Jung Shamsher Bahadur Nawab Topchi
*Amrish Puri ...  Sher Shera Singh
*Karan Kapoor ...  Karan
*Vikas Anand ...  Senior Police Inspector
*Ramesh Goyal ...  Sheras Man in prison
*Jugal Hansraj ...  Hassan Ali
*Goga Kapoor ...  Kundan Singh
*Kader Khan ...  Jagannath Prasad
*Shashi Kiran ...  Dang Praveen Kumar ...  Sheras Man in prison
*Roopesh Kumar ...  Sheras Man in prison Madhavi ...  Anita Mandakini ...  Seema
*Mac Mohan ...  Sheras Man in prison
*Ram Mohan ...  Rahim Chacha
*Moolchand ...  Man with a briefcase
*Raza Murad ...  Dayal
*Yunus Parvez ...  Sheras Man in prison
*Jagdish Raj ...  Police Commissioner
*Tej Sapru ...  Bhima
*Joginder Shelly ...  Sheras Man in prison
*Anjan Srivastav ...  Champaklal Sudhir ...  Sheras Man in prison
*Bhushan Tiwari ...  Sheras Man in prison

==Soundtracks==
* Saat taalo me rakh
* Isa Pir na musa pir, sabse bada hain paisa pir

==External links==
*  

 
 
 
 
 
 
 