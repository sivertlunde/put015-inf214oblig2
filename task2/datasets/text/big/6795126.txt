Nesam
{{Infobox film
| name = Nesam
| image =
| director = K. Subash
| writer = K. Subaash Manivannan   (Dialogues ) 
| starring = Ajith Kumar Maheswari Goundamani Manivannan Senthil
| producer = K. Subaash
| studio = Dhanooja Films
| cinematography = S. Bernard
| editing = Krishnamoorthy Siva Deva
| distributor = D.P.S International Ltd.
| released = January 14, 1997
| runtime = 139 minutes
| country = India Tamil
| budget =  2.7 crore
}} Senthil and Manivannan among others play other pivotal roles in the film, which has music composed by Deva (music director)|Deva. The film released on 14 January 1997 to mixed reviews. 

==Cast==
*Ajith Kumar as Ranga Nathan
*Maheswari
*Goundamani
*Manivannan Senthil
*Visu
*Manobala
*Subhalekha Sudhakar

==Production==
The film became one of the two films of 1997 in which Ajith Kumar and Maheswari were paired together, with the second collaboration being in Ullaasam and also being the second collaboration of Ajith and Subash after Pavithra.

==Release==
The film opened to mixed reviews.   Two years after release, the producers were given a  5 lakh subsidy by Tamil Nadu Chief Minister M. Karunanidhi along with ten other films. 
The film profitable success

==Soundtrack==
Music is composed by Deva. 
*Kundrathile - Sabesh
*Madonna Varuvala - Mano, Swarnalatha
*Natchathira Bangla - Deva, Subha
*O Ranganatha - SPB, Chithra
*Thuli Thuli - Mano

==References==
 

 
 
 
 
 
 


 