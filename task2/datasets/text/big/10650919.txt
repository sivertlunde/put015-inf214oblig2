Hillbillys in a Haunted House
 
{{Infobox Film 
  | name = Hillbillys in a Haunted House
| image          = Hillbillys in a Haunted House 1967 poster.jpg
| image_size     = 300
| caption        = 1967 US Theatrical Poster
  | director = Jean Yarbrough
  | producer = Bernard Woolner
  | writer = Duke Yelton
  | starring = Ferlin Husky Joi Lansing  Lon Chaney Jr.
  | music = Hal Borne
  | cinematography = Vaughn Wilkins
  | editing = Holbrook N. Todd
  | distributor = Woolner Brothers Pictures Inc. 
  | released = May 1967
  | runtime = 88 min. 
  | language = English
  }}
 
Hillbillys in a Haunted House is a 1967 American horror comedy film starring Ferlin Husky and Joi Lansing, and directed by Jean Yarbrough. The film is a sequel to The Las Vegas Hillbillys (1966), with Joi Lansing replacing Mamie Van Doren in the role of "Boots Malone".

==Plot==
Country singers are headed to Nashville. Their car breaks down and they stop overnight at an abandoned house, which turns out to be haunted. A ring of international spies (Lon Chaney, Jr., Basil Rathbone and John Carradine) who live in the house are seeking a top secret formula for rocket fuel.  While it is never revealed for whom they are spying, they carry out their activities under the cover of a supposed haunted house, which comes complete with a gorilla in the basement.

==Background==

* Basil Rathbone starred in one of his last film roles.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 