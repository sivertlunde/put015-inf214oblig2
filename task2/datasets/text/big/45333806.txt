For Horowitz
{{infobox film
| name           = For Horowitz
| image          = 
| director       = Kwon Hyung-jin
| producer       = Cha Seung-jae   Kim Mi-hee
| writer         = Kim Min-sook 
| starring       = Uhm Jung-hwa   Shin Eui-jae   Park Yong-woo
| cinematography = Hwang Dong-guk 
| editing        = Steve M. Choe     Kim Chang-ju
| music          = Lee Byung-woo Sidus FNH
| distributor    = Showbox/Mediaplex
| released       =  
| runtime        = 108 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =  
}}
 2006 South Korean drama film directed by Kwon Hyung-jin and starring Uhm Jung-hwa, Shin Eui-jae and Park Yong-woo.    

==Plot==
Kim Ji-soo is a 30-year-old single woman who once dreamed about becoming a world-class pianist, but ends up opening a small neighborhood piano school on the outskirts of Seoul, where she teaches children to play.

She then meets Yoon Gyung-min, a 7-year-old orphaned boy who starts pestering her for no apparent reason. Gyung-mins parents died in a car accident, and he now lives with his uncaring grandmother. He is a troublemaker, emotionally distant from his peers, and possibly autistic.

When Ji-soo discovers that Gyung-min has untapped musical genius, she becomes determined to turn him into a renowned pianist like Vladimir Horowitz. She does this for her own selfish reasons, because if she gains the reputation of being a great piano teacher, that would lead to her schools success.

She uses music to communicate with her gifted student, ruthlessly giving him intensive piano lessons to prepare him for competitions. But their relationship turns maternal when Ji-soo realizes what the boy really needs is love.

==Cast==
* Uhm Jung-hwa as Kim Ji-soo
* Shin Eui-jae as Yoon Gyung-min
* Park Yong-woo as Shim Kwang-ho
* Choi Seon-ja as Gyung-mins grandmother
* Yoon Ye-ri as Jung-eun
* Jung In-gi as Ji-soos older brother
* Yang Kkot-nim as Ji-soos sister-in-law
* Park Young-seo as Pizza shop employee
* Ko Tae-ho as Bo-ram
* Lee Seung-hyun as Yoo-sik
* Kang Soo-jin as Ye-ri
* Choi Yoon-sun as So-young
* Kim Bo-ra as Min-hee
* Seo Ji-ah as Mi-ra
* Lee Oi-seon as Cleaner Lee
* Kim Jong-min as Taekwondo Kim
* Jo Seok-hyun as Video Park
* Julius Jeongwon Kim as Adult Gyung-min (cameo appearance|cameo) 

==Box office==
For Horowitz was released in South Korea on May 25, 2006. It drew 544,656 admissions during its theatrical run. 

==Awards and nominations==
{| class="wikitable"
|-
! Year
! Award
! Category
! Recipient
! Result
|- 2006
| 27th Blue Dragon Film Awards   
| Best Actress
| Uhm Jung-hwa
|  
|-
| Best New Director
| Kwon Hyung-jin
|  
|-
| Best Music
| Lee Byung-woo
|  
|-
| Best Visual Effects
| Jeong Sang-seong
|  
|- 5th Korean Film Awards 
| Best Actress
| Uhm Jung-hwa
|  
|-
| Best Music
| Lee Byung-woo
|  
|- 2007
| 44th Grand Bell Awards 
| Best Actress
| Uhm Jung-hwa
|  
|-
| Best New Director
| Kwon Hyung-jin
|  
|-
| Best Screenplay
| Kim Min-sook
|  
|-
| Best Editing
| Steve M. Choe, Kim Chang-ju
|  
|-
| Best Music
| Lee Byung-woo
|  
|-
| Best Sound
| Choi Tae-young
|  
|-
|}

==References==
 

==External links==
*   
* 
* 
* 

 
 
 