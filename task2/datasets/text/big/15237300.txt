Libby, Montana (film)
{{Infobox Film | name = Libby, Montana
| image          =
| caption        = 
| director       = Drury Gunn Carr & Doug Hawes-Davis
| producer       = Drury Gunn Carr & Doug Hawes-Davis
| writer         = 
| starring       = 
| music          = Ned Mudd
| cinematography = 
| editing        = Drury Gunn Carr & Doug Hawes-Davis
| distributor    = 
| released       = 
| runtime        = 124 min.
| language       = English
| budget         =  
}}

Libby, Montana is a 2004 documentary film about the biggest case of community-wide exposure to a toxic substance in U.S. history. The film details the story of the iconic, mountainside town of Libby, Montana and the hundreds of residents who have been exposed to asbestos, raising questions of the role of corporate power in American politics. 
 Point of View series in 2007.

==See also==
*Alice - A Fight For Life, a 1982 British television documentary about asbestos exposure.

==References==
 

== External links ==
*   - PBSs site dedicated to the film
*  

 

 
 
 
 
 
 
 
 
 

 