Purple Sea
{{Infobox film
 | name = Purple Sea
 | image = Purple Sea.jpg
 | caption =
 | director = Donatella Maiorca
 | writer =   
 | starring =  
 | music =  Gianna Nannini
 | cinematography =  Roberta Allegrini
 | editing =  
 | language =  Italian
 }} 2009 Italian romance drama film directed by Donatella Maiorca. It is based on the non-fiction novel Minchia di re written by Giacomo Pilati. The film premiered at the 2009 Rome Film Festival.   It was nominated for two Nastro dArgento Awards, for Best Actress (Valeria Solarino) and Best Original Song ("Sogno" by Gianna Nannini). 

== Cast ==

*Valeria Solarino: Angela/Angelo
*Isabella Ragonese: Sara
*Ennio Fantastichini: Salvatore
*Giselda Volodi: Lucia
*Maria Grazia Cucinotta: Agnese
*Marco Foschi: Tommaso
*Lucrezia Lante Della Rovere: Baronessa
*Corrado Fortuna: Ventura

==References==
 

==External links==
* 
 
  
 
 
 
 
 

 
 