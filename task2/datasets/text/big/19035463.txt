Vishwavidhaata
 
{{Infobox Film
| name           = Vishwavidhaata
| image          =
| director       = Farogh Siddique
| producer       = Thirupur Mani Revathi Suresh Menon
| narrator       =  Arjun
| music          = Songs: A. R. Rahman  (Re-used from Pudhiya Mugam)  Background Score Naresh Sharma
| cinematography = 
| editing        = 
| distributor    =  1997
| runtime        = 
| country        =  
| language       = Hindi
| budget         =
| awards         =
}} Arjun and Ashish Vidyarthi. The film is shot in India and Sharjah (city)|Sharjah. It is a remake of a Tamil movie Pudhiya Mugam, written by Revathi and Suresh Menon. The film features songs composed by A. R. Rahman, re-used from Pudhiya Mugam. Rahman was upset with the producers since the music was re-used in the film without his permission. 

==Plot==
Jay Verma (Sharad Kapoor) lives in Bombay. He is an honest, unemployed youth who cannot arrange for his mothers treatment. He happens to meet underworld goons looking for such frustrated people and they force Jay into the world of terrorism. R.B the uncrowned king of terrorrism, orders Jay to be killed when he revolts. Sharjah and undergoes plastic surgery to begin a new life. He returns to India as Ajay Kahnna (Jackie Shroff), meets his ex-lover Radha (Ayesha Jhulka) and marries her. They have a baby boy, Ravi Khanna. Ravis parents want him to become a police officer to do away with traitors, but they themselves end up becoming a target instead. 

{{Infobox album |  
 Name = Vishwa Vidhaata |
 Type = soundtrack |
 Artist = A R Rahman |
 Cover = Vishwavidhaatacover.jpg |
 Released = 1997 (India)|
 Recorded = Panchathan Record Inn, AM Studios | Feature film soundtrack |
 Length =  |
 Label =  Bombino  |
 Producer = A. R. Rahman | 
 Reviews = |
 Last album = Ratchagan (1997) |
 This album = Vishwa Vidhaata (1997) |
 Next album = Kabhi Na Kabhi (1997)|
}}

==Soundtrack==
The film features songs composed by A. R. Rahman, re-used from Pudhiya Mugam. Rahman was upset with the producers since the music was re-used in the film without his permission. 

The lyrics were written by P. K. Mishra and Mehboob for Humdum Pyaara Pyaara.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s) !! Notes
|-
| "Sambo Sambo"
| Kavita Krishnamurthy, Malgudi Subha
| Reused "Sambo Sambo" from Pudhiya Mugam
|-
| "Kal Nahi Tha Woh" Sujatha
| Reused "Netru Illadha Maatram" from Pudhiya Mugam
|-
| "Kal Nahi Tha Woh (Sad)" Hariharan
| Reused "Idhu Thaan Vazhkai" from Pudhiya Mugam
|-
| "Nazron Ke Milne Se"
| Kavita Krishnamurthy
| Reused "Kannukku Mai Azhagu" from Pudhiya Mugam
|-
| "Jaan Tum Ho Meri" Anupama
| Reused "July Madham Vandhaal" from Pudhiya Mugam
|-
| "Kaliyon Se Palkhen Hain"
| Hariharan
| Reused "Kannukku Mai Azhagu" from Pudhiya Mugam
|-
| "Humdum Pyaara Pyaara"
| Udit Narayan, Kavita Krishnamurthy
| Not by A. R. Rahman
|}

==References==
 

==External links==
 

 
 
 
 


 