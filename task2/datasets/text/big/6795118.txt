Pagaivan
{{Infobox film
| name = Pagaivan
| image =
| image_size =
| caption =
| director = Ramesh Krishna
| producer = V. Sundhar
| writer =
| starring = Ajith Kumar Sathyaraj Anjala Zhaveri Deva
| cinematography = D. Shankar
| editing = B. Lenin V. T. Vijayan
| distributor =
| released = 19 August 1997
| runtime = 142 mins
| studio = 
| country =   Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 Vivek who all appear in supporting roles. It released in August 1997.  

==Plot==
Prabhu (Ajith Kumar), despite having all the required qualifications for the job he applied for, fails after he was demanded to pay a considerable amount as a caution deposit. And every time all his chances of getting the job were hanging on the payment of this caution.

Determined to get this job at all costs, Prabu is ready to do anything to obtain this amount as quick as possible. So Prabu decides to kidnap someone so as to demand a ransom to the family of this person. The fate leads him to abduct Uma (Anjala Zhaveri) who happens to be the one and only daughter of Minister Duray Raj (K. S. Ravikumar).

Uma falls in love with Prabu and, before long, Prabu begins to love Uma too. Meanwhile, Uma’s parents hire Vaasu (Sathyaraj), a rowdy who’s ready to do anything for the sake of money, to find their daughter. The rest of the film revolves around if the pair will be found.

==Cast==
*Ajith Kumar as Prabu
*Sathyaraj as Vasu
*Ranjitha
*Anjala Zhaveri as Uma Durairaj
*Nagesh
*K. S. Ravikumar as Durairaj Mansoor Ali Khan
*Babu Antony
*Delhi Ganesh Vivek
*Dhamu

==Release==
The film became a commercially success and subsequently became one of five classical success films starring Ajith Kumar in 1997 after the bigest  success of Kadhal Kottai. 

==References==
 

 
 
 
 
 


 