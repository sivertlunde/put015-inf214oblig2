August Underground's Penance
{{Infobox film
| name           = August Undergrounds Penance
| image          = AUPenance.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Snuff Edition DVD Released by Toetag Pictures - signed by most of the cast
| film name      = 
| director       = Fred Vogel
| producer       = 
| writer         = Fred Vogel   Cristie Whiles
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Fred Vogel   Cristie Whiles
| music          = Rue   The Murder Junkies
| cinematography = 
| editing        = Fred Vogel   Logan Tallman
| studio         = Toetag Pictures
| distributor    = Toetag Pictures
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $5,000 (estimated) August Undergrounds Penance Directors Commentary 
| gross          = 
}}

August Undergrounds Penance is a 2007 horror film written and directed by Fred Vogel, and co-written by Cristie Whiles. It is the sequel to 2003s August Undergrounds Mordum, and the final installment in the August Underground series, which began in 2001.

== Plot ==

August Undergrounds Penance continues the seriess narrative mode of showing the lives of serial killers (now just Peter, and his girlfriend Crusty) through their camera, though this installment abandons the "degraded footage" aspect employed by the first two films, being shot in high-definition.

After killing a man who breaks free of his restraints and tries to escape them, Peter and Crusty visit various locations in town, and enjoy some fireworks at a celebration. Peter is then shown in his basement, taunting a semi-conscious man who has had nails hammered into various parts of his body. Next, Peter and Crusty go on a hike, assault a vagrant they find sleeping under a bridge, and cut a man open so they can pull his gurgling intestines out. The two then attend a party, where Peter does drugs, Crusty flirts with other women, and a live rat is fed to a pet alligator.

Around Christmas, Peter and Crusty break into a familys home. Peter bludgeons the father with a hammer, and suffocates the mother, who he is unsuccessful in trying to rape due to being unable to attain an erection. When the dead couples young daughter stumbles onto the two intruders, Crusty strangles her. While Peter goes to shower, Crusty opens some of the familys presents, then falls asleep beside the corpses of the little girl, and her mother. Later, while watching a band perform, Crusty goes to an empty room with a man, who she has rough anal sex with. Back in the basement, Peter and Crusty torture and murder several people they have imprisoned. Afterward, they take apart a dead deer, and with a friends help, feed pieces of it to a lion. The two then enjoy several recreational activities, like shooting on a makeshift firing range, and racing ATVs.

As Peter beats a man to death with a hammer, Crusty drinks some of the spilled blood, then helps Peter dismember a female body. When Peter wakes up from a nap, he and Crusty get into an argument. Peter is then shown cutting the fetus out of a pregnant woman, an act which causes Crusty to break down. When his attempts at comforting Crusty fail, Peter rapes her, then goes into hysterics himself. Peter gets drunk, and proceeds to take his frustrations out on a woman in the cellar, while screaming insults at Crusty, who is still sobbing hysterically upstairs. When Peter passes out, Crusty throws alcohol and other fluids onto his body, while ranting about how much she hates him.

After beating and tying up a woman, Peter masturbates and smears his semen on her. When Crusty walks in on this, she gets into a physical fight with Peter; he beats her while she screams insults and profanities at him. When Peter falls asleep, Crusty spits on him, and strangles his captive to death. With the woman dead, Crusty breaks down again, and begins begging for forgiveness and rambling about how she "wants out". The film ends with Crusty going to the bathroom and committing suicide via self-asphyxiation.

== Cast ==

* Fred Vogel as Peter Mountain     
* Cristie Whiles as Crusty
* Shelby Vogel as Pregnant Partier
* Jerami Cruise as Partier with Baseball Cap
* Anthony Matthews as Nail-Covered Victim
* Rob Steinbruegge as Intro Victim
* Talia Becker
* Lucious Ridgewood
* Ed Laughlin as Gutted Victim
* Fuctup as Homeless Man
* Bub as The Lion
* Wally
* Brama
* Matt Rizzutto as Crustys Lover
* Autumn Smith
* Art Ettinger
* Uncle Mike as Mike
* Merle Allin as Himself
* J. B. Beverley as Himself
* Scotty Wood as Himself Dino Sex as Himself
* Kim Cerny as Blond Partier
* Paul Cerny Jr. as Bearded Partier
* Don Moore as Partier Scared by Rat
* Donna Hutchison as Brunette Partier
* Zach Weaver as Lion Owner
* Brian Hutchison as Party Host
* Kathleen Pleasant
* Holden Williams
* Trevor Collins as Bloated Victim
* John Cornell
* Marie Keswick
* Mikol Bell
* Sara McGill
* Renee Bell as Mikes Wife
* Catherine Mannion

== Reception ==
 snuff fest. It is also a suitably fitting end to the series with actors Fred Vogel and Cristie Whiles delving into levels of intense paranoia, showcasing a decline in mental and psychical stability while torturing and butchering nameless individuals. It strives to achieve a point of expressing the mind of a modern serial killer and although this may be lost or unappreciated by those expecting a Mordum 2, it awards the film a credibility that was certainly missing in August Undergrounds Mordum". 

The Worldwide Celluloid Massacre stated that while the leads "have to act in this installment" it was still "the same boring sickness" and "the same pointless realistic snuff trash". 

== References ==

 

== External links ==

*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 