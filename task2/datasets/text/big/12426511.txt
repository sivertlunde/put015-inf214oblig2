Last Year's Snow Was Falling
{{Infobox film name = Last Years Snow Was Falling image_size = caption = director = Aleksandr Tatarskiy producer = Aleksandr Tatarskiy writer = Sergey Ivanov narrator = starring = Stanislav Sadalskiy music = Grigory Gladkov cinematography = Iosif Golomb editing = Lyubov Georgieva distributor = Studio Ekran released = 1983 runtime = 19 min. 45 sec. country = Soviet Union language = Russian budget = gross =
}} Soviet clay animation|clay-animated film directed by Aleksandr Tatarskiy (T/O Ekran studio).

The film reached a cult status after its first appearance on Central TV. The aphoristic remarks of the characters, full of absurd humor, turned into colloquial proverbs.

For this work Tatarskiy received the Silver Cooker award at the 1983 Varna International Film Festival.

Loosely based on some folk fairy tales.

==Plot summary==
The protagonist is a lazy, ignorant but tricky man. He is also tongue-tied - unable to pronounce some digits and letters. He likes beer and always gets into ridiculous situations. Fortunately he has a strict and authoritative wife. The story begins when his wife sends him to bring a New Year tree from the forest. But the forest in the winter is a magic place full of surprising events and transformations. Entangled in the miracles, having lost and found his own image more than once, the man goes back home with empty hands.

The plot includes two interrelated stories - about the mans dreams and about incredible transformations inside the magic cabin on chicken legs. The first story bases on the fairy tale about a greedy man who saw a rabbit in the forest, daydreamed about growing rich on it, and frightened it away with a shout .

The narator closes the story by saying that the man eventually  got the tree, but it was already spring that time, so he had to bring it back.

== Censorship ==
The absurd style of narration raised censors suspicions that the film "contains encoded messages to foreign intelligence". Also they said to Tatarskiy that he is disrespectful to the Russian man ("you have just one character in the film and he is an idiot").

Some phrases that later became colloquial (Who is here, for example, the last in line for the Tsar position? Nobody?! Then Ill be the First!) were defended by Tatarskiy and Ivanov with scandals. Despite their attempts the film was sent "for revision". It was cut anew and redubbed.

== Other facts ==
* Sadalsky did not appear in the credits. Shortly before the final cut, the actor was arrested inside the Cosmos Hotel with a foreign citizen. The information against Sadalsky was reported to Gosteleradio director S. Lapin who ordered the removal of Sadalskys name from the credits as a penalty for forbidden relations with foreigners.

==Quotes==
 
* Now, thats my size!
* Thats not quite enough!!!
* I understand nothing!..
* If Im such a merchant (cupets) out here, then why do I need that ill-famed wife?! Thats not quite enough!!!
* Noble animal!.. Fur! Meat!.. Ill roast some Pork rind|cracklings!..
* We, the boyars, are hard-working people! That is really our boyars lot...
* Oh, how I do love and respect this treasure thing!..
* Oh, what a fine doormat!.. Used to be...
* Oh, those fairy tales!.. And those fairy-tellers telling them!..
* Lady, let me go! I know the "magic" word - "Please!"
* Many fir-trees and many yous! But sense - not quite enough of it!!!
* Who wants a hare, hare-runhare?  
* Howd you like that heel-dance?! 
* When Ill be a Tsar, Ill get a piano|pianina. What the life is without a pianina?
* Who is here, for example, the last in line to be a Tsar ? Nobody? Then Ill be the first!
* Such a pleasant flexibility formed in the body!..
* For the third time he went to get the fir-tree and finally obtained it... But it was spring already and he returned it back...
* Theres lot of idiots here, but a small number of hares, I see.
* Once upon a time there lived a man. Well-shaped, well-done... Just like I was when I was young.
* Dont spit  into   ! Forgot the proverb? Well, no worries, you will remember it soon enough!
* When acorn is ripe it will be eaten by every pig!

== External links ==
*  
*   at Animator.ru

 

 
 
 
 
 
 