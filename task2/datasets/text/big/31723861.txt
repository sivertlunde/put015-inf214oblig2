Mr. Reeder in Room 13
{{Infobox film
| name           = Mr. Reeder in Room 13
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Norman Lee
| producer       =  John Corfield
| writer         = Victor Kendall  Elizabeth Meehan  Doreen Montgomery
| based on       = novel Room 13 by Edgar Wallace
| narrator       = 
| starring       = Peter Murray-Hill  Sally Gray  Gibb McLaughlin
| music          =  Ronnie Munro Eric Cross
| editing        =  Ted Richards	(as Edward Richards)
| studio         = British National Films
| distributor    = Associated British Film Distributors  (UK)
| released       = February 1938  (UK)
| runtime        = 78 minutes United Kingdom English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} British crime film directed by Norman Lee and starring Peter Murray-Hill, Sally Gray and Gibb McLaughlin.  It is based on the first J. G. Reeder book, Room 13 by Edgar Wallace.  The film was released in the U.S. in 1941 as Mystery of Room 13.  

==Plot==
Mr. J.G. Reeder (Gibb McLaughlin) is called in by the Bank of England to investigate a gang of forgers. Reeder enlists the aid of a younger man, Capt. Johnnie Gray (Peter Murray Hill), to infiltrate the gang by going undercover in Dartmoor jail.

==Cast==
* Peter Murray-Hill - Captain Johnnie Gray 
* Sally Gray - Claire Kent 
* Gibb McLaughlin - J.G. Reeder 
* Malcolm Keen - Peter Kent 
* Leslie Perrins - Jeffrey Legge
* Sara Seegar - Lila Legge 
* D. J. Williams (actor)|D.J. Williams - Emmanuel Legge 
* Rex Carvel - Sir John Flaherty 
* Robert Cochran - Detective Inspector Barker 
* Phil Ray - Fenner, convict

==Critical reception==
Britmovie wrote, "director Norman Lee keeps things moving along briskly and packs plenty of goings-on into its relatively short running time, but the outlandish plot requires some suspension of disbelief."   

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 


 
 