Letter from an Unknown Woman (1948 film)
{{Infobox film
| name           = Letter from an Unknown Woman
| image          = Letter from an Unknown Woman poster.jpg
| caption        = Theatrical release poster
| director       = Max Ophüls
| producer       = John Houseman William Dozier
| writer         = Story:   Max Ophüls
| starring       = Joan Fontaine Louis Jourdan
| music          = Daniele Amfitheatrof
| cinematography = Franz Planer
| editing        = Ted J. Kent
| distributor    = Universal Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
}} novella of the same name by Stefan Zweig. The film stars Joan Fontaine, Louis Jourdan, Mady Christians and Marcel Journet.

In 1992, Letter from an Unknown Woman was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 

==Plot==
In Vienna in the early twentieth century, Lisa (Joan Fontaine), a teenager living in an apartment complex, becomes fascinated by a new tenant, concert pianist Stefan Brand (Louis Jourdan). Stefan is making a name for himself through energetic performances. Lisa becomes obsessed with Stefan, staying up late to listen to him play, and sneaking into his apartment and admiring him from a distance. Despite her actions, they only meet once and Stefan takes little notice of her.

One day, Lisas mother (Mady Christians) announces her marriage to a wealthy and respectable gentleman, who lives in Linz, and tells Lisa that they will all move there. Lisa resists her mothers plans and runs away from the railway station and goes back to the apartment, where she is let in by the porter. She knocks on Stefans door, but no one answers. She decides to wait outside for him to return. Early the next morning, Stefan returns home with a woman. After seeing the two, a distraught Lisa travels to Linz where she joins her mother and new stepfather.

In Linz, she is transformed into a respectable woman and courted by a young military officer from a good family. He eventually proposes to Lisa, but she turns him down, saying that she is in love with someone else living in Vienna and is even engaged to be married with him. Confused and heartbroken, he accepts her situation. When they learn about Lisas actions, her mother and stepfather demand to know why she didnt accept the proposal. "I told him the truth", replies Lisa. 

Years later, Lisa is estranged from her parents and works in Vienna as a dress model. Every night she waits outside Stefans window, hoping to be noticed. One night he notices her, and although he does not recognize her, he finds himself strangely drawn to her. They go on a long, romantic date that ends with them making love. Soon after, Stefan leaves for a concert in Milan, promising to contact her soon, but he never does. Lisa eventually gives birth to their child, never trying to contact Stefan, wanting to be the "one woman who never asked you for anything".

Ten years later, Lisa is now married to an older man named Johann (Marcel Journet) who knows about her past love for Stefan, for whom she named their son. One day while at the opera, Lisa sees Stefan, who is no longer a top-billed musician and rarely performs. Feeling uneasy, she leaves during the performance. The chance wants that he follows and so they meet while waiting for her carriage. Stefan does not remember her, but once again is oddly drawn to her. Lisa is still uncomfortable with this, not wanting to anger her husband, and when her carriage arrives, she is met by a clearly vexed Johann.

A few nights later and against her husbands wishes, Lisa travels to Stefans apartment, and he is delighted to see her. Despite a seemingly illuminating conversation about Stefans past life and his motivations for giving up music, Stefan still does not recognize who Lisa really is. Distraught and realizing that Stefan never felt any love for her at all, Lisa leaves. On her way out she meets the servant and the two exchange a long glance. Sometime later, after her son dies of typhus, Lisa is taken to a hospital and is gravely ill herself. She writes a letter to Stefan explaining her life, her son, and her feelings toward him; the letter that narrates the whole film.

After Lisa dies, the letter is sent to Stefan, along with a card from the hospital staff announcing her death. In shock, Stefan thinks back to the three times they met and he failed to recognize her. "Did you remember her?", he asks his servant. The servant nods and writes down her full name, Lisa Berndle, on a piece of paper. Still in shock, Stefan leaves his building and sees the ghostly image of a teenage Lisa open the door for him, the same way she once did when he first noticed her all those years ago. Outside, a carriage waits to take him to meet a dueling opponent, Lisas husband, Johann. Finally intending to take responsibility for his actions, Stefan decides to engage in the duel.

==Cast==
*Joan Fontaine as Lisa Berndle
*Louis Jourdan as Stefan Brand
*Mady Christians as Frau Berndle
*Marcel Journet as Johann Stauffer Art Smith as John, mute butler
*Howard Freeman as Herr Kastner
*Carol Yorke as Marie
*John Good as Lt. Leopold von Kaltnegger
*Leo B. Pessin as Stefan, Jr
*Erskine Sanford as Porter
*Otto Waldis as Concierge
*Sonya Waldis as Frau Spitzer
Uncredited
*Betty Blythe as Frau Kohner; uncredited
*John T. Bambury as Midget; uncredited

==Adaptation notes== Howard Koch. The film is mostly faithful to the book, though featuring minor divergences. The male protagonist in the book is simply referred to (once) as R, and is a novelist rather than a musician. The film renames him Stefan Brand (referencing Zweig, who also lends his name to the protagonists infant son, also unnamed in the original source material). The "unknown woman" receives no name in the book; in the film she is called Lisa Brendle (a noted quirk of Ophüls, having his female characters names starting with an L). Fernand, a relative of Lisas mother and eventual husband, is turned into the completely unrelated "Mr. Kastner", with the family moving to Linz rather than Innsbruck. John, the servant, retains his name, but in the film, he is mute.

The novels sexual content is quite implicit, but because of censorship, the movie adaptation further dims it. In the book, the "unknown woman" spends three nights with the writer (rather than one) before his departure. She only meets him one more time, many years later, at the opera, at which she promptly loses her present lover in favor of spending a fourth night with the writer. At the conclusion of this, she is humiliated when he mistakes her for a prostitute, and rushes off, never to see him again. The movie adaptation splits these into two separate encounters (first meeting him at the opera, and then rushing off humiliated from his house), and ignores another sexual encounter.

Further divergences include a more prolonged "first encounter" between the two lovers (taking them through stagecoaches, fairs and ball rooms rather than simply cutting to the long-waited sexual encounter), revealing the disease that kills Stefan Jr. and Lisa to be typhus and ignoring Lisas tradition of sending Brand white roses every birthday. At the start of the novel, Brand has just turned 41 (and forgotten about his birthday). This is significant because the absence of white roses confirms Lisas death at the time of reading.

The most noted divergence is a structural change: there is no duel in the original story, nor is there a character such as Johann. The "unknown woman" from the book never marries, but lives off a series of lovers who remain unnamed and mostly unintrusive. Because of this, the protagonists actions offend no one in particular. In the film, Brand is challenged to a duel, which he initially plans to ditch. The finale reveals the contestant to be Johann, who demands satisfaction over Lisas affair. Having read Lisas letter, Brand boldly accepts the duel and walks into it, his fate uncertain. This redeeming action has no literary equivalent. In fact, Brands literary equivalent can only faintly recall Lisa after reading the letter, and theres no significant event past this.

==References==
 

==External links==
 
* 
* 
* 
*  by  Alexander Dhoest
*  by Carla Marcantonio
*  at Louisjourdan.net

 

 
 
 
 
 
 
 
 
 
 
 
 
 