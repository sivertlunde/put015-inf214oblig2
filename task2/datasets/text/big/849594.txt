Charley Varrick
 
{{Infobox film name            = Charley Varrick image           = Charley Varrick.jpg caption         = Film poster director        = Don Siegel producer        = Don Siegel writer          = John H. Reese (novel) Dean Riesner Howard Rodman starring  Andrew Robinson Joe Don Baker John Vernon Felicia Farr music           = Lalo Schifrin cinematography  = Michael C. Butler editing         = Frank Morriss distributor     = Universal Pictures released        =   runtime         = 111 min. language  English
|budget          =
}}
 Andrew Robinson, Joe Don Baker and John Vernon. The film was based on the novel The Looters by John H. Reese.

==Plot== Andrew Robinson), a heavily disguised Varrick robs a bank in the rural crossroads community of Tres Cruces, New Mexico. During the robbery, two policemen and Dutcher are killed. Nadine drives the get-away car and eludes the police, but she too has been mortally wounded and dies shortly thereafter. Varrick distracts the police from their chase of him by blowing up the getaway car with black powder and gasoline, with his wifes body inside. The ploy works, and Varrick and Sullivan escape.
 mob money laundering operation and that the mafia will pursue them more relentlessly than the police. Varrick tells Sullivan that they can only evade the mafia by laying low and not spending the money for the next three to four years.  The young, headstrong Sullivan will hear none of it, argues with Varrick, and plans to spend the money on the luxuries he craves.

Soon, Mafia financier Maynard Boyle (John Vernon) dispatches hitman Molly (Joe Don Baker) to find the robbers and recover the stolen money by any means necessary.

Realizing that Sullivans rashness endangers both of them, Varrick decides to double-cross him before Sullivan can do the same to him. He makes a plan to flee the country and puts in a rush order for two fake passports with Jewell Everett (Sheree North), a pretty photographer. She promptly betrays Varrick and puts Molly on his trail. The sadistic Molly catches up with Sullivan at Charleys trailer, brutally beating, and then killing him, while attempting to determine Varricks whereabouts.

Boyle, meanwhile, tells bank manager Harold Young (Woodrow Parfrey) that his superiors in the mob suspect that the robbery was an inside job, as Varricks gang robbed the bank during the brief period the money would be there.  Boyle also asks why Young unwittingly guided Varrick to the Mafias money in the banks safe instead of letting him leave with just the tellers money.  He suggests that Young will be tortured with a pair of pliers and a blowtorch until he provides a satisfactory answer.  The meek Young is so terrified that he commits suicide by shooting himself to avoid the interrogation.

Varrick then flies his plane to Reno, Nevada|Reno, where he buys flowers from a street vendor (Charles Matthau). When Boyles pretty secretary, Sybil Fort (Felicia Farr), leaves work carrying the roses, Varrick follows her home and surprises and seduces her at her high-rise apartment. Later she warns Varrick not to trust her boss. Nonetheless, Varrick sets up a meeting with Boyle to return the money in a remote automobile wrecking yard situated east of Reese, NM. After arriving in his crop duster, Varrick ostentatiously hugs Boyle and proclaims theyve gotten away with stealing the money.  Molly, surveilling the meeting in his car, 500 yards away, is convinced that the two must have been co-conspirators in the robbery.  He then runs down and kills Boyle with his car.

Molly then chases Varrick, who tries to escape by plane in the junkyard. Molly damages the crop-dusters tail with his car before it can take off. Varricks crippled plane flips over when Varrick slams on the brake. Flat on his back in the wreckage, it appears Varrick can do nothing to save himself, except tell Molly where the money is hidden.

Seemingly in mortal danger, Varrick has set a booby trap for Molly; he had flipped the plane on purpose, a trick he learned back in his barnstorming days. Varrick tells Molly the money is in the trunk of an old Chevy parked among the junkers.  When Molly opens the Chevys trunk to retrieve the hidden money he is killed in an explosion, landing 50 feet away on another upside down wrecked car. In the remains of the explosion is Sullivans body wearing Charleys wedding ring. Sullivans body is to be mistaken for that of Varrick, who earlier had switched their dental records, while removing his wifes and Harmons records. Varrick throws a thick wad of hundred-dollar bills into the burning flames and tosses his logo-embroidered jumpsuit into the trunk on top of Sullivans body where it too starts to burn. He then transfers the rest of the money to another supposedly junked car and makes his getaway.

==Cast==
* Walter Matthau as Charley Varrick Andy Robinson as Harman Sullivan
* Joe Don Baker as Molly
* John Vernon as Maynard Boyle
* Sheree North as Jewell Everett
* Felicia Farr as Sybil Fort
* Norman Fell as Garfinkle
* Woodrow Parfrey as Harold Young
* William Schallert as Sheriff Horton
* Jacqueline Scott as Nadine
* Benson Fong as Honest John
* Marjorie Bennett as Mrs Taft
* Joe Conforte as himself
* Tom Tully as Tom

==Production notes==
Director Don Siegel wanted Charley Varricks companys motto, "Last of the Independents," to be the title of the film.  The motto appears on the movie poster and briefly as a subtitle in the film trailer.

Tom Tully had a small part as a shopkeeper in a wheelchair. Charley Varrick was his last film. The little boy who tells the sheriff hes got blood on his head is played by Walter Matthaus actual son, Charles.

===Locations===
Siegel filmed several of his movies in northern Nevada, including Charley Varrick, The Shootist and Jinxed! Charley Varrick was set in New Mexico, but filmed primarily in two small Nevada towns, Dayton, Nevada and Genoa, Nevada. Both Dayton and Genoa lay claim to being the oldest towns in Nevada . The opening bank robbery exterior shots were filmed in Genoa at the old Douglas County court house.  The sheriffs chase of Varrick and his gang was filmed nearby at Genoa Lane, and on U.S. Route 395 in Nevada|U.S. Route 395. The interior bank scenes were filmed in Minden, Nevada|Minden. The trailer park scenes were shot in Dayton at the trailer park near Red Hawk Casino (closed in 2008) and the Carson River, near U.S. Route 50 in Nevada|U.S. Route 50, at the corner of Hart and Louie Streets. The photographers studio and gun store scenes were filmed in Gardnerville, Nevada|Gardnerville. The plane flight scene in the films ending was filmed at City Auto Wrecking, located at Interstate 80 in Nevada|Rt. 1 Mustang Exit, Sparks, Nevada|Sparks, near the defunct but now reopened Mustang Ranch brothel, ten miles east of Reno. Reno locations include the Chinese restaurant at 538 S. Virginia St. and the Arlington Towers apartment building where Varrick meets Miss Fort.

==Awards==
British Academy of Film and Television Arts Awards&nbsp;&mdash; Best Actor, 1974, Walter Matthau

==Award nominations== British Academy of Film and Television Arts Awards&nbsp;&mdash; Best Editing, 1974, Frank Morriss

==DVD release== Region 1 DVD on December 28, 2004. The DVD has no extras. On February 14, 2008 "Charley Varrick" was released as a Region 2 DVD in Europe in Widescreen and some Special Features. Both DVD Versions are uncut.

==Cultural impact==
*The 1978 song "Last of the Independents" by Rory Gallagher was inspired by Charley Varrick, according to his long-time bassist Gerry McAvoy in his book, Riding Shotgun. 
*John Vernons line "...&nbsp;go to work on you with a pair of pliers and a blowtorch&nbsp;..." is paraphrased in the 1994 film, Pulp Fiction.
*In Ray Davies 1995 semi-fictional autobiography, X-Ray, the film Charley Varrick is mentioned approvingly several times, and he claims it is his "favourite video." 
*The protagonists of the 2013 film 2 Guns rob a bank in Tres Cruces, New Mexico. Whilst not strictly a "remake", the film also has the same basic premise that a far larger than expected amount of money is found in the vault, as a result of money laundering. 

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 