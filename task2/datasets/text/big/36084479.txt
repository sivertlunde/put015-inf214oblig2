Searching For Santa!
{{Infobox Hollywood cartoon|
| cartoon_name = Searching For Santa!
| series = Krazy Kat
| image = 
| caption =  Bill Nolan
| story_artist = George Herriman
| animator = 
| voice_actor = 
| musician = 
| producer = Margaret J. Winkler
| studio = Winkler Pictures FBO ( )  Pathé|Pathé Pictures ( )
| release_date = 1925 
| color_process = Black and white
| runtime = 3:53 English
| preceded_by = Monkey Business
| followed_by = A Barnyard Frolic
}}

Searching For Santa! is a silent short film by Winkler Pictures, starring Krazy Kat. The cartoon was thought to be lost but was rediscovered in 2004.

==Plot==
Krazy is spending some time at a house with a human boy. The boy suddenly gets bored and starts crying. To entertain his friend, Krazy shows him a ball, but the boy just kicks it away in apathy. When the cat asks what the boy wants, the bawling boy wishes for Santa Claus to pay them a visit. Krazy then sets off towards the North Pole to fulfill his friends wish.

On his journey to find Santa, Krazy enters what looks like a subway station in the city. He goes through a secret passage inside and eventually reaches the exit at the Arctic. He then tries to ask the locals for directions, starting with what he thinks is an Eskimo. It turns out the "Eskimo" is a vicious polar bear which chases him. To lose the bruin from behind, Krazy constructs a sled with a sail on top. It is a smooth ride until he runs off the edge of a chasm.

At the bottom of the chasm, Krazy finds a turntable and a record. He plays the record on the player and dances, attempting to carol someone out of an igloo. Unfortunately, no one comes out. While he is still standing and thinking what to do, a hostile sea lion hurls a snow ball at him. The impact sends him bouncing into an Eskimo woman. The Eskimo woman responds by hurling Krazy back to the sea lion. The cartoon concludes with the sea lion having fun in rebounding the airborne feline.

==Notes==
*This is among the last theatrical films that feature Krazy in George Herrimans original design.
*The boy in the cartoon bears a resemblance to Willie Jones, the former human friend of Felix the Cat.

==External links==
*  at the Big Cartoon Database
* 

 

 
 
 
 
 
 
 
 
 


 