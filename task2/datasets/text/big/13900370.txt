Palermo Shooting
{{Infobox film
| name           = Palermo Shooting
| image          = PalermoShooting.jpg
| alt            = 
| caption        = German DVD cover
| director       = Wim Wenders
| producer       = Gian-Piero Ringel Wim Wenders
| writer         = Wim Wenders Norman Ohler Bernd Lange Campino Giovanna Mezzogiorno Dennis Hopper
| music          = Irmin Schmidt
| cinematography = Franz Lustig
| editing        = Peter Przygodda Oli Weiss
| studio         = Neue Road Movies
| distributor    = Senator Film
| released       =  
| runtime        = 124 minutes
| country        = Germany France Italy
| language       = English
| budget         = 
| gross          = 
}}
Palermo Shooting is a 2008 film written and directed by German director Wim Wenders, and starring Andreas Frege|Campino, Dennis Hopper, Giovanna Mezzogiorno, Lou Reed as himself, and an uncredited Milla Jovovich, also playing herself.

==Plot==
 
The film follows a German photographer (played by Campino, singer of German punk band Die Toten Hosen) who comes to Palermo because he needs to make a clean break from his past. In the city, he meets a young woman (Mezzogiorno) and a completely different way of life. 

==Production==
The film is the first one directed by Wenders in his hometown, Düsseldorf.    Filming also took place in the nearby cities of Essen and Neuss as well as Palermo and other areas of Sicily.

==Reception==
The film was released in Germany on 20 November 2008. The film had its U.S. premiere on 20 January 2009 at the Berlin and Beyond film festival at the Castro Theatre in San Francisco.

==References==
 

== External links ==
*  
*  , by Todd McCarthy, 25 May 2008
*    , by GABA Media and Entertainment Co-Chair Pamela Lenz Sommer, 20 January 2009

 

 
 
 
 
 
 
 


 