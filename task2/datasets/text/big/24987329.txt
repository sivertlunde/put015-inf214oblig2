La Bête (film)
 
 
{{Infobox film
| name           = The Beast
| image          = La bête.jpg
| image size     = 220px
| border         = yes
| alt            = 
| caption        = Official teaser poster
| director       = Walerian Borowczyk
| producer       = Anatole Dauman
| writer         = Walerian Borowczyk
| starring       = Sirpa Lane Lisbeth Hummel Marcel Dalio
| cinematography = Bernard Daillencourt Marcel Grignon
| editing        = Walerian Borowczyk
| studio         = Argos Films
| distributor    = Argos Films
| released       =  
| runtime        = 98 minutes
| country        = France
| language       = French Italian
}} erotic Fantasy fantasy horror film written, edited, and directed by Walerian Borowczyk. Although sometimes compared with Beauty and the Beast, there are no parallels in the plot except that it features the relationship between a beast (monster) and a woman.

==Background== Immoral Tales (1974), which was envisaged to be a film of six stories.  After Immoral Tales was remastered as a film of four stories, the footage became the dream sequence of The Beast. 

==Plot==
 
Wealthy businessman Philip Broadhurst dies and leaves his entire estate to his daughter Lucy on the condition that, within six
months of his death, she marries Mathurin, the son of his best friend, the Marquis Pierre de lEsperance, and that they must be
married by Cardinal Joseph do Balo, the brother of Pierres uncle, the crippled Duc Rammaendelo de Balo, who shares their crumbling farmhouse with Pierres daughter Clarisse, and their servant Ifany, whom we see copulating with Clarisse at every opportunity.

The problem is that Mathurin, who manages the family horse-breeding business, is dim-witted and deformed, and as a result has never been baptized. Pierre summons the local choirboy-loving priest to the house for the baptism, but Pierre, by promising the priest repairs to his church and a new bell, performs the ritual himself so that the priest doesnt find out the truth about Mathurin.

Lucy and her aunt, Virginia, are driven by their chauffeur towards the farm but their way is blocked by a fallen tree. They find a back route to the house but end up at the stables where they see two horses copulating. Lucy eagerly takes some photographs, much to the disgust of her aunt. They eventually arrive at a back door to the house, where Lucy asks Rammaendelo about ghostly rumors she has heard about the family. Rammaendelo, who is not in favor of the marriage because he is dependent on Mathurin to look after him, shows her a book that describes the beautiful Romildas fight with a beast in the local forest 200 years ago. Looking around the house, Lucy comes across several drawings depicting bestiality, and becomes sexually excited at the thought of her impending marriage, even though she has never met Mathurin.

Pierre blackmails Rammaendelo into persuading his brother to perform the marriage by telling him that he has proof that Rammaendelo poisoned his wife. However, Rammaendelo is unable to get through to the Cardinal on the telephone. Pierre sends a telegram instead, assuring him that Mathurin has been baptized and urging him to attend that very evening.

Everyone assembles for dinner, and Mathurins uncouth manners soon become apparent. Lucy and her aunt try to leave, but are
persuaded to stay. Everyone having drunk too much wine, most of the assembly fall asleep while waiting up for the Cardinal. Lucy retires to her room, undresses, puts on her thin wedding dress, and dreams that she is Romilda, playing a harpsichord. Seeing a lamb straying into the forest, she chases after it to find that it has been torn apart by a black hairy beast.

Meanwhile, Pierre overhears Rammaendelo on the telephone to the Cardinal trying to dissuade him from performing the marriage.
Angrily interrupting the conversation, Pierre slits Rammaendelos throat with a razor and tears the phone out of the wall. In the ensuing comic dream sequence, the beast chases Lucy through the forest. She loses most of her clothing in the process and ends up hanging by her arms from a branch, and the beast licks her and masturbates. Lucy wakes in a sweat. Was it just a dream? She tiptoes to Mathurins room but he is asleep, fully clothed, on his bed. Lucy returns to her room, masturbates, and dreams that the beast is copulating with her. She finds she enjoys it. She wakes again and is convinced that Mathurin must have visited her. She visits his room again but he is still sleeping soundly.

Lucy eagerly returns to her dream. The beast continues to masturbate and Lucy rubs his ejaculate all over herself. Eventually the beast dies of exhaustion. Lucy wakes and walks into Mathurins room to find him dead on the floor. She runs naked through the house screaming, and everyone runs to her aid. Virginia examines Mathurins body and discovers that a plaster cast on his arm is concealing a claw for a hand. Pulling his clothes off reveals that he is covered in thick black hair and has a tail. They run out of the house in terror just as the Cardinal arrives to find out what is going on. Virginia comforts the terrified Lucy as they speed away in the car, and Lucy dreams that she is naked in the forest again, burying the
beast.       

==Cast==
* Sirpa Lane as Romilda de lEsperance
* Lisbeth Hummel as Lucy Broadhurst
* Elisabeth Kaza as Virginia Broadhurst
* Pierre Benedetti as Mathurin de lEsperance
* Guy Tréjan as Pierre de lEsperance
* Roland Armontel as Priest
* Marcel Dalio as Duc Rammaendelo De Balo
* Robert Capia as Roberto Capia
*   as Clarisse De lEsperance

== Release == premiered on 6 January 1975 at the Avoriaz Fantastic Film Festival  and was released theatrically in Germany on 6 February 1981. 

==Reception==
The film did well in Europe, but the run of the film in France and the U.S. ran into controversy due to its erotic nature and show of bestiality. Many felt the film went over the top with its sex scenes, leading to its withdrawal from film for several years. In the UK the BBFC refused to classify a heavily cut version for general cinema release, and the same cut print narrowly avoided prosecution under the Obscene Publications Act by the Director of Public Prosecutions when it shown with Greater London Council approval at the independently run Prince Charles Cinema in London in September 1978. 

==Further Reading==
Kerri Sharp. "Hairy Hands Make Light Work". UK: Headpress19: World Without End (1999), pp. 37-40.

==References==
 

==External links==
*  
*  
*   from Moria
*  
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 