Insidious (film)
{{Infobox film
| name           = Insidious
| image          = insidious poster.jpg
| alt            = 
| caption        = Theatrical film poster
| director       = James Wan
| producer       = Jason Blum Steven Schneider Oren Peli
| writer         = Leigh Whannell Patrick Wilson Rose Byrne Barbara Hershey 
| music          = Joseph Bishara
| editing        = James Wan Kirk Morri
| cinematography = John R. Leonetti David M. Brewer	
| studio         = Alliance Films IM Global Stage 6 Films Blumhouse Productions
| distributor    = FilmDistrict
| released       =   }}
| runtime        = 102 minutes
| country        = United States 
| language       = English
| budget         = $1.5 million   
| gross          = $97 million 
}}
Insidious is a 2010 American  , was released on September 13, 2013, with Wan returning as director and Whannell returning as screenwriter. Because of the films success it became the basis for a maze for 2013s annual   is set to be released on June 5, 2015.

==Plot== Patrick Wilson) and Renai (Rose Byrne), their sons Dalton (Ty Simpkins) and Foster (Andrew Astor), and infant daughter Cali–move into a new home.  Shortly afterward, Dalton is drawn to the attic when he hears creaking noises and sees the door open by itself.  He falls from a ladder while investigating and sees a figure in the shadows.  Hearing his terrified screams, Renai and Josh rush to his aid and declare the attic "off limits" to the children.  The next day, Dalton falls into an inexplicable coma.

After three months of treatment without result, Renai and Josh are allowed to take Dalton home. Instantly, increasingly supernatural activity occurs; Renai begins hearing voices over the baby monitor when no one is in Calis room, Foster says that Dalton sleepwalks at night, Renai sees the frightening figure of a man in Calis room, who vanishes when Josh comes and the burglar alarm is repeatedly triggered without a cause. After Renai finds a bloody handprint on Daltons bed, she questions Josh about the house, but he ignores her. That night, the figure from Calis room appears outside the bedroom window and attacks Renai. The Lamberts abandon the house and move elsewhere.

In the second house, Renai sees the figure of a dancing boy who leads her to Daltons room. Joshs mother, Lorraine (Barbara Hershey), has a dream in which a figure in Daltons room replies "Dalton" when asked what it wanted; at the same time, she sees a monstrous red-faced figure behind Josh and screams, while Daltons room is ransacked and Dalton himself is found laying on the floor.
 
Lorraine calls paranormal investigators Elise Reiner (Lin Shaye), Specs (Leigh Whannell), and Tucker (Angus Sampson). Upon entering, Elise senses a presence in the house and once entering Daltons room, she sees something on the ceiling, to which Specs draws a demonic, red-faced figure with dark hollow eyes; the same figure Lorraine saw.

Elise explains that Dalton is not in a coma; he was born with the ability to travel mentally to the astral plane. He has traveled too far and become lost in a realm called "The Further", a place inhabited by the tortured souls of the dead. Without his mental presence, Daltons body appears comatose and spirits can use it to enter the physical world. Josh is skeptical until he realizes that all of Daltons drawings are of the demonic entity drawn by Specs.

Elise performs a seance to communicate with Dalton, but the demon appears and foully threatens the group before using Daltons body to attack them until it is stopped by Elise. She reveals that her acquaintance with Lorraine is decades old, because she previously performed the same service on Josh when he was eight years old (he was terrorized by the parasitic spirit of an old woman). Josh also possesses astral talent; Dalton inherited it from him. Elise tells Josh that the only way to rescue Dalton is to go into the Further himself.

Elise puts Josh in a trance and he is able to project himself to their previous house. He goes to the attic and finds a red door, but is attacked by the mysterious figure that attacked Renai. After defeating him, Josh enters the Demons lair, where Josh finds Dalton chained to the floor. Josh frees him, but they are caught by the demon; Josh tries to fight it, but they are forced to flee with the demon in pursuit. Returning through the red door, Josh confronts the old woman that haunted him as a child. The old woman dissolves into darkness after Josh shouts at it to leave him alone. When Josh and Dalton return to their bodies they wake up in their new home and the spirits disappear.

As they celebrate the end of their ordeal, Elise is packing up when she senses something and takes a photo of Josh. Josh flies into a violent rage, as he doesnt like to have his picture taken, strangles Elise and flees. Renai picks up the camera, to and sees that the image of Josh resembles a sinister old woman, implying that Josh has been possessed. Josh appears over her shoulder, and she turns around and gasps.

==Cast==
  Patrick Wilson as Josh Lambert
** Josh Feldman as Young Josh
* Rose Byrne as Renai Lambert
* Lin Shaye as Elise Rainier
* Ty Simpkins as Dalton Lambert
* Barbara Hershey as Lorraine Lambert
* Leigh Whannell as Specs
* Angus Sampson as Tucker
* Andrew Astor as Foster Lambert
* Heather Tocquigny as Nurse Kelly
* Corbett Tuck as Nurse Adele
* Ruben Pla as Dr. Sercarz
* John Henry Binder as Father Martin
 

===Insidious entities===
* Joseph Bishara as the Lipstick-Face Demon
* J. LaRose as the Long Haired Fiend
* Philip Friedman as the Old Woman
* Kelly Devoto and Corbett Tuck as Doll girls
* Lary Crews as the Whistling Ghost Dad
* Jose Prendes as Top Hat Guy
* Caslin Rose as the Ghoul / Contortionist 
* Ben Woolf as Dancing Boy

==Production==

===Filming=== Herald Examiner Patrick Wilson explained, "We had long days and a lot of pages a day, and we didn’t get a lot of coverage or rehearsal. But luckily, the benefit of doing a movie that’s not on a big budget—and the reason it’s usually done like that—is so if the filmmakers feel like, ‘OK, we’re not going to sacrifice anything on screen,’ which I don’t think they have, it lets them have complete control. So we were in good hands." 

===Music===
The musical score to Insidious was composed by Joseph Bishara, who also appears in the film as the demon.  Performed with a quartet and a piano, a bulk of the score was improvised and structured in the editing process, although some recording sessions began prior to filming.  On describing the approach of the films soundtrack, director James Wan explained, "We wanted a lot of the scare sequences to play really silent. But, what I like to do with the soundtrack is set you on edge with a really loud, sort of like, atonal scratchy violin score, mixing with some really weird piano bangs and take that away and all of a sudden, you’re like, What just happened there?" 

An exclusively digital soundtrack album was released by Void Recordings on October 11, 2011.  Additional songs featured in the film include: Tiny Tim (1968)
* "Nuvole Bianche" by Ludovico Einaudi (2004)
* "Black Angels" by George Crumb (1971)

==Distribution==

===Marketing===
The first promotional clip from Insidious was released on September 14, 2010.  The following December, production company IM Global released an image and sales poster for the film.  On January 22, 2011, FilmDistrict released the first teaser trailer for the film.  Less than a month later, the films theatrical trailer was made available online via daily entertainment news site Blastr. 

===Theatrical release===
Insidious had its world premiere in the Midnight Madness program at the Toronto International Film Festival on September 14, 2010. Less than 12 hours after its screening, the film was picked up by Sony Pictures Worldwide Acquisitions for theatrical distribution.   On December 29, 2010, it was announced that the film would be released theatrically on April 1, 2011 by the then-relatively new film company FilmDistrict.  The film was also screened at South by Southwest in mid-March 2011. 

===Home media===
Insidious was released on   hosted a free screening of the film at the Silent Movie Theater in Los Angeles followed by an interactive Q&A with director James Wan and screenwriter Leigh Whannell. 

==Reception==

===Box office=== Hop and Source Code. It has since grossed a total of US$54,009,150 domestically and US$43,000,000 internationally, for a total of $97,009,150 worldwide.  Insidious was the most profitable film of 2011. 

===Critical response===
Insidious received generally positive reviews. Review aggregate   gave the film 2 1/2 stars out of 4 saying, "It depends on characters, atmosphere, sneaky happenings and mounting dread. This one is not terrifically good, but moviegoers will get what theyre expecting."   

A number of negative reviews reported that the second half of the film did not match the development of the first. Mike Hale of   fairy tale."  Similarly, James Berardinelli commented, " f theres a complaint to be made about Insidious, its that the films second half is unable to live up to the impossibly high standards set by the first half."  Ethan Gilsdorf of The Boston Globe wrote that " he film begins with promise" but " he crazy train of Insidious runs fully off the rails when the filmmakers go logical and some of the strange gets explained away as a double shot of demonic possession and astral projection." 

Positive reviews have focused on the filmmakers ability to build suspense. John Anderson of   wrote: "director James Wan and screenwriter Leigh Whannell admire all sorts of fright, from the blatant to the insidiously subtle. This one lies at an effective halfway point between those extremes."  Peter Travers of   stated: "Insidious is the kind of movie you could watch with your eyes closed and still feel engrossed by it. Its a haunted-house thriller filled with all the usual creaking doors, groaning floors and things that go bump in the night, but itll also grab you with some disturbing, raspy whispers on a baby monitor, a few melancholy piano plunkings and the panicky bleating of an alarm as a front door is mysteriously flung open in the middle of the night." 

===Accolades===

{| class = "wikitable"
! Year
! Result
! Award
! Category
! Recipient
|-
| 2011
|  
| Fright Meter Awards
| Best Horror Film
| James Wan Leigh Whannell
|-
| 2011
|  
| Fright Meter Awards
| Best Director
| James Wan
|-
| 2011
|  
| Fright Meter Awards
| Best Actress
| Rose Byrne
|-
| 2011
|  
| Fright Meter Awards
| Best Supporting Actress
| Lin Shaye
|-
| 2011
|  
| Fright Meter Awards
| Best Screenplay
| Leigh Whannell
|-
| 2011
|  
| Saturn Awards
| Best Supporting Actress
| Lin Shaye
|-
| 2011
|  
| 2011 Scream Awards
| Best Horror Film
| −
|-
| 2011
|  
| 2011 Scream Awards
| Best Horror Actor
| Patrick Wilson
|-
| 2011
|  
| 2011 Scream Awards
| Best Horror Actress
| Rose Byrne
|-
|}

==Sequel and Follow-Up/Prequel==
;Sequel

 

A sequel,  , was released on Friday, September 13, 2013. In November 2011, it was reported that Sony Pictures had registered online domain names for a second film. 

;Follow-Up/Prequel

 

A follow-up/prequel,  , is scheduled for release on June 5, 2015.

==See also==
*List of ghost films

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 