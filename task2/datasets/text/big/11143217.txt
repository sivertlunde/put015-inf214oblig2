Andharangam
{{Infobox film
| name           = Andharangam
| image          = 
| caption        = 
| director       = Muktha Srinivasan
| producer       = M. Venugopal
| writer         = A. S. Pragasam
| story          = A. S. Pragasam Savitri Unni Deepa Thengai Manorama
| music          = G. Devarajan
| cinematography = R. Sambath
| editing        = L. Balu
| studio         = Maya Arts
| distributor    = Maya Arts
| released       =  
| runtime        = 149 mins
| country        =   India Tamil
}}

Andharangam is a Tamil language film starring Kamal Haasan. Savithri and Major Sundarrajan play his parents. It was an adult rated movie released when Kamal was in his early 20s. The movie was taken in black and white but some of the song scene was taken in geva color. 

==Cast==
* Kamal Haasan
* Major Sundarrajan Savitri
* Deepa
* Thengai Srinivasan
* Cho Ramaswamy Manorama
* V. Gopalakrishnan
* Kathadi Ramamurthy
* Satheesh
* Sukumari

==Soundtrack==
The music composed by G. Devarajan while lyrics written by Kannadasan and Vaali (poet) |Vaali.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers
|-
| 1 || Kuthirai Kutti || K. J. Yesudas
|-
| 2 || Nyayiru Oli Mazhaiyil || Kamal Haasan
|}

==References==
 

==External links==
 

 
 
 
 


 