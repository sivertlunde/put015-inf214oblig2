Moving August
{{Infobox film 
 | name = Moving August
 | image = Moving August DVD cover.jpeg
 | caption = Moving August DVD cover 
 | director = Christopher Fink 
 | writer = Christopher Fink Joseph Craig
 | starring = Eddie McClintock Sarah Wynter Josh Holloway Alexandra Adi
 | distributor = District 
 | released =  
 | runtime = 96 minutes
 | language = English 
 | country = United States
}}
Moving August is a 2002 American comedy genre film.

==Plot==
The pretty and quirky interior designer Michelle Kelly (Sarah Wynter) has convinced her struggling photographer boyfriend, August Loder (Eddie McClintock) to finally move in with her. On the same morning theyre moving August out, the girl moving in mistakenly arrives to move in... shes a hot looking free-spirit named Hunter (Alexandra Adi) and August falls in love with her at first sight.August and Hunter decide to help each other move in and out. Throughout the day, their two very outrageous groups of friends tangle through conflicts and sex while August tries to decide which girl he wants to be with before its too late.

==Cast==
*Eddie McClintock. . . . . .   August Loder
*Sarah Wynter . . . . . Michelle Kelly
*Josh Holloway. . . . . .  Loren Carol
*Alexandra Adi . . . . . .   Hunter Davis
*Brenda Bakke  . . . . . .   Ginny Forster
*Todd Tesen  . . . . . .   Joe Peck
*Gavin Perry  . . . . . .   Adam Loder
*Christopher Fink . . . . . .  Shopping Cart Guy

==External links==
* 

 
 
 
 
 
 


 