The Rack (film)
{{Infobox film
| name           = The Rack
| image_size     =
| image          = The Rack FilmPoster.jpeg
| alt            =
| caption        =
| director       = Arnold Laven
| producer       = Arthur M. Loew Jr.
| writer         = Stewart Stern (screenplay) Rod Serling (teleplay)
| narrator       =
| starring       = Paul Newman Wendell Corey Walter Pidgeon Edmond OBrien Anne Francis Lee Marvin Cloris Leachman
| music          = Adolph Deutsch
| cinematography = Paul Vogel
| editing        = Harold F. Kress Marshall Neilan Jr.
| studio         =
| distributor    = MGM
| released       =  
| runtime        = 100 min.
| country        =   English
| budget         = $779,000  . 
| gross          = $765,000 
| preceded by    =
| followed by    =
}}
 American war war drama film, based on a play written by Rod Serling for television.   It was directed by Arnold Laven and starred Paul Newman, Wendell Corey, Lee Marvin and Walter Pidgeon. After two years in a North Korean prison camp, an American officer returns home, only to be charged with collaboration by his own side. He is forced to defend his actions in court.

==Plot summary==
Having survived two years in Korea in a prisoner-of-war camp, Captain Edward W. Hall, Jr., returns home to San Francisco and reports to an Air Force base there. His father, a retired lieutenant colonel, is glad to have his son back, but still grieving over the death of his other son, Pete, in the war.

Petes widow, Aggie Hall, confides to her friend Caroline that it is difficult to be around her brother-in-law without being painfully reminded of having lost her husband. A welcome-home party is held for Capt. Hall, which surprises Colonel Dudley Smith, a friend of Ed, Sr. He finds out that Capt. Halls father is unaware that Ed Jr. is about to be tried in a court-martial for collaboration with the enemy.

Asking bluntly if the charges are true and being told that they are, Ed Sr. cruelly says to his son: "Why didnt you just die?"

Major Sam Moulton prosecutes the case. He calls eyewitnesses who testify that at the POW camp in the winter of 1951, Capt. Hall made speeches and signed documents on the enemys behalf. A much-decorated officer, Capt. John Miller, reveals the scars he received while a prisoner, all the while never conceding to his captors anything but his name, rank and serial number.

Capt. Hall has his sister-in-laws support, but his father refuses even to attend the trial. Hall is disconsolate and wishes to plead guilty. But his lawyer, Lt. Col. Frank Wasnick, appeals to him to take the witness stand and explain his actions.

In stark detail, Capt. Hall discloses the torture he underwent. How he was ordered to bury other soldiers, dead or alive. How he carried a wounded man for four days so he wouldnt collapse and be placed in a grave. How he was placed in solitary confinement for months at a time, denied light and company and forced to live in his own filth. After repeated demands to read propaganda statements, Capt. Hall agreed to do so but wrote one himself, using language that attempted to mock the enemys purpose.

The breaking point came soon after the enemy handed Capt. Hall a letter from his father that was intercepted, one revealing that his brother Pete was dead. Capt. Halls father, who finally has come to the trial, is devastated by his sons testimony. He forgives him, but the official judgment is not so kind. Capt. Hall is found guilty of treason.

==Cast==
* Paul Newman as Capt. Edward W. Hall Jr.
* Wendell Corey as Maj. Sam Moulton
* Walter Pidgeon as Col. Edward W. Hall Sr.
* Edmond OBrien as Lt. Col. Frank Wasnick
* Anne Francis as Aggie Hall
* Lee Marvin as Capt. John R. Miller
* Cloris Leachman as Caroline Robert Burton as Col. Ira Hansen
* Robert F. Simon as Law Officer (as Robert Simon)
* Trevor Bardette as Court President Adam Williams as Sgt. Otto Pahnke
* James Best as Millard Chilson Cassidy
* Fay Roope as Col. Dudley Smith
* Barry Atwater as Maj. Byron Phillips

==Reception==
According to MGM records the film earned $365,000 domestically and $400,000 overseas, making an overall loss of $422,000. 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 