Madakkayathra
{{Infobox film 
| name           = Madakkayathra
| image          =
| caption        =
| director       = George Vettam
| producer       = George Thankachan
| writer         = 
| screenplay     = PK Mohan
| starring       =
| music          = Alleppey Ranganath
| cinematography = TN Krishnankutty Nair
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film, directed by George Vettam and produced by George Thankachan. The film had musical score by Alleppey Ranganath.   

==Cast==
 

==Soundtrack==
The music was composed by Alleppey Ranganath and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Arayaal Kuruvikal || K. J. Yesudas || Bichu Thirumala || 
|-
| 2 || Uthraadakkilye || K. J. Yesudas || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 