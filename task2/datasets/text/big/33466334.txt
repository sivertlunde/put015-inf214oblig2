The Power of Few
{{Infobox film
| name           = The Power of Few
| image          = The_Power_of_Few_Poster.jpg
| caption        = Theatrical poster
| director       = Leone Marucci
| producer       = Jimmy Holcomb Roy Scott Macfarland Jay Thames Fred D’Amico Q’Orianka Kilcher Leone Marucci 
| screenplay     = Leone Marucci
| story          = Leone Marucci
| based on       =  Juvenile Navid Derek Richardson 
| music          = Mike Simpson 
| cinematography = 
| editing        = Jonathan Walls 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 95 minutes 
| country        = United States
| language       = English
| budget         = $7.5 million
| gross          =  $25,000 
}} American filmmaker Leone Marucci.
 interactive film Derek Richardson.   Through The Power of Few website, Marucci and Kilcher delivered an interactive experience embarked upon in 2006.   From online casting  to online editing, the global audience was provided original material from the film (and an online editing system) and invited to help create the finished film.   The interactive collaboration continued beyond the website as the production ran a community outreach program in the city of New Orleans during filming. 

The Power of Few debuted theatrically in the United States on 14 Feb 2013. Steelyard Pictures platform released the film theatrically starting in 2 markets and expanding to 40 markets throughout the Spring of 2013. Gaiam Vivendi is distributing the film on all non-theatrical platforms.

==References==
{{reflist|refs=
   
   
   
   
   
    
   
   
}}

==External links==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 