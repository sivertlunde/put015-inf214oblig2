Pyar Hi Pyar
{{Infobox film
| name           = Pyar Hi Pyar
| image          = 
| image_size     = 
| caption        =
| director       = Bhappi Sonie
| producer       = Rajaram, Satish Wagle
| writer         = K. A. Narayan  Anand Romani
| narrator       =  Pran
| music          = Shankar Jaikishan   Hasrat Jaipuri(lyrics)
| cinematography = Taru Dutt
| editing        = Hrishikesh Mukherjee
| distributor    = 
| released       = 1969
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}

Pyar Hi Pyar ( , "love, only love") is a  List of Bollywood films of 1969 | 1969 Bollywood film produced by Rajaram and Satish Wagle, the film is directed by Bhappi Sonie. The film stars Dharmendra, Vyjayanthimala, Pran (actor)|Pran, Helen (actress)|Helen, Madan Puri and Mehmood Ali|Mehmood. This was the only movie of Dharmendra and Vyjayanthimala together.

==Plot==
Young Vijay lives in an orphanage after being separated from his widowed mother, Laxmi. Years later Vijay has grown up and a wealthy man, Kailashnath Gupta, identifies him as his long-lost son and takes him home. Once there, Vijay decides to find employment as a police constable much to Kailashnaths displeasure. Vijays first assignment is to locate a beautiful young womans missing father. The young woman is Kavita and Vijay falls head-over-heels in love with her. Vijay sets about to accomplish this task, little knowing that a man named Satish has convinced Kailashnath that Vijay is not who he claims to be, and that Satish is Kailashnaths biological son.

The movies songs were popular and hit at that time. Mohd Rafi sang for Dharmendra and Mehmood in the same film. Agogo ageya for Mehmood and rest song for Dharmendra. Mein Kahin Kavi and Dekha hai tere surat of  Mohd Rafi ware huge hits.

Salim Khan father of Salman was drummer in the song Tu Mera Mein Teri of Mohd Rafi & Asha Bhosle picturised on Dharmendra and Helen.

==Cast==
* Vaijanthimala	 ...	Kavita
* Dharmendra    	 ...	Vijay Pratap Pran          	 ...	Satish Raj / Ashok Helen         	 ...	Cham Cham Dhumal	         ...	Jattadhari
* Raj Mehra      	 ...	Kailashnath Gupta
* Madan Puri	         ...	Dindayal
* D. K. Sapru      	 ...	Diwan
* Manmohan         	 ...	Shyam Kumar
* Sulochana Latkar	 ...	Yashoda
* Sulochana Chatterjee	 ...	Laxmi
* Krishan Dhawan	 ...	Kewal Mehra
* Mehmood Jr.	         ...	Mehmoodali
* Mehmood         	 ...	Goga
* Salim Khan         ...	Drummer

Playback Mohd Rafi sangs all 6 six songs in the movie. He playback for Dharmendra and Mehmood. Mein Kahin Kavi Na was the most popular number of the movie and became a huge hits.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dekha Hai Teri Aankhon Mein"
| Mohammed Rafi
|-
| 2
| "Dedo Pyar Lelo Pyar"
| Mohammed Rafi
|-
| 3
| "Tu Mera Main Teri"
| Mohammed Rafi, Asha Bhosle
|-
| 4
| "Main Kahin Kavi Na Ban Jaoon"
| Mohammed Rafi
|-
| 5
| "Go Go Aaagaya.....Hum Aangya Phir Dar Kaheka"
| Mohammed Rafi
|}

== External links ==
*  

 
 
 
 

 