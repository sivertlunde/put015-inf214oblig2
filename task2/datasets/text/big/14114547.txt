The Winning of Barbara Worth
{{Infobox film
| name           = The Winning of Barbara Worth
| image          = Poster - Winning of Barbara Worth, The 01 Crisco restoration.jpg
| alt            = 
| caption        = Theatrical release poster Henry King
| producer       = Samuel Goldwyn
| writer         = Frances Marion
| based on       =  
| starring       = {{Plainlist|
* Ronald Colman
* Vilma Bánky
* Gary Cooper
}}
| music          = Ted Henkel
| cinematography = {{Plainlist| George Barnes
* Thomas Branigan Gregg Toland
}}
| editing        = Viola Lawrence
| studio         = The Samuel Goldwyn Company
| distributor    = United Artists
| released       =  
| runtime        = {{Plainlist|
* 89 minutes
* 9 reels, 8,757 ft
}}
| country        = United States
| language       = English intertitles
| budget         =
| gross          =
}}
 Western silent Henry King So This Is Paris) in his first feature role.

Based on the novel The Winning of Barbara Worth by Harold Bell Wright, the film is about an engineer who vies with a local cowboy for the affections of a ranchers daughter while building an irrigation system for a Southwestern desert community. The movie was filmed in Californias Imperial Valley and in the Black Rock Desert of Nevada.    {{cite web
|title=The Winning of Barbara Worth |publisher=Silent Era |url=http://www.silentera.com/PSFL/data/W/WinningOfBarbaraWorth1926.html |accessdate=March 29, 2014}} 

==Cast==
* Ronald Colman as Willard Holmes
* Vilma Bánky as Barbara Worth
* Gary Cooper as Abe Lee
* Charles Willis Lane as Jefferson Worth
* Paul McAllister as The Seer
* E.J. Ratcliffe as James Greenfield Clyde Cook as Tex
* Erwin Connelly as Pat Mooney
* Ed Brady as McDonald
* Sammy Blum as Horace Blanton
* Fred Esmelton as George Cartwright
* Bill Patton as Little Rosebud

==Further reading==
*   

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 


 
 