Radha Ka Sangam
{{Infobox film
| name=Radha Ka Sangam
| image=Radha Ka Sangam.jpg
| image_size = 200px
| alt=
| caption=Theatrical release poster
| director=Kirti Kumar
| producer=Kirti Kumar
| story=
| based on= Jainendra Jain Govinda Juhi Chawla
| music=Anu Malik
| cinematography=Thomas Xavier
| editing=R. Rajendran
| studio=Sri Nirmala Devi Productions
| distributor=
| released=  
| runtime=130 minutes
| country=India
| language=Hindi
}}
 1992 Hindi romance film directed and produced by Kirti Kumar. The film features Govinda, Juhi Chawla and Kirti Kumar as main characters.   Originally, Divya Bharti was cast in the role of Radha, her first role. She was dropped due to disagreements with the director and Juhi Chawla was signed.

==Synopis==
Radha ka Sangam is based on re-incarnation. It is shown that Govinda (Govinda) and Radha (Juhi Chawla) love each other, They get married but after the marriage things do not go in their favour. A lot of tragic incidents happen in their lives. Govinda gets a jail sentence and Radha is also not well outside. When Govinda gets out of jail another tragedy strikes. Do they meet in their next birth? What happens next?

==Cast== Govinda
*Juhi Chawla
*Abhimanyyu Raj Singh
*Mala Sinha
*Kiran Kumar
*Kirti Kumar
*Disco Shanti
*Ragesh Asthana

==Music==

The music of the film is composed by Anu Malik and lyrics are penned by Hasrat Jaipuri and Shaily Shailendra.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
! # !! Title !! Singer(s)
|-
| 1
| "Prem Hai Janmo Ka Sangam"
| Anuradha Paudwal, Sukhwinder Singh
|-
| 2
| "Bichhuwa More Sajna Ka Pyar "
| Lata Mangeshkar, Suresh Wadkar
|-
| 3
| "O Radha Tere Bina Tera Shaam Hai Aadha"
| Lata Mangeshkar, Shabbir Kumar
|-
| 4
| "Do Bol Kehke Hum To Haare Hain"
| Lata Mangeshkar
|-
| 5
| "Banna Mera Aaya Hariyala "
| Anuradha Paudwal,Suresh Wadkar, Nitin Mukesh
|-
| 6
| "Kanha Kanha Kanha "
| Anuradha Paudwal
|-
| 7
| "Pinjara Banaya Chandni Ki"
|
|}

==References==
 

==External links==
* 

 
 
 

 