Croc (film)
{{Infobox television film
| name = Croc
| image = Croc, DVD Release.jpg
| alt = 
| caption = DVD Cover
| director = Stewart Raffill
| producer = Charles Salmon
| writer = Ken Solarz
| cast =  Scott Hazell
| music = Charles Olins Mark Ryder
| cinematography = Choochart Nantitanyatada
| editing = Laurie McDowell Kant Pan
| genre = Horror
| studio = Thai Occidental Productions
| distributor = RHI Entertainment
| network = Syfy
| released =  
| runtime = 100 minutes
| country = United States
| language = English
| budget = $750,000 (estimated)
| gross =
}} natural horror Movie Central On Demand in July 2007. It aired in the United States on the Syfy Channel on November 4, 2007. It is the 5th film in the Maneater Series produced under an agreement with Syfy. Filmed in Thailand, the film revolves around the efforts of the owner of a crocodile-farm, an animal welfare agent, and a hunter trying to kill a large saltwater crocodile that has begun killing people in the area.

==Plot==
The film begins with two dynamite fisherman working only to disturb a large saltwater crocodile which attacks, mutilates, and kills them both.

Jack McQuade (Peter Tuinstra) runs a crocodile farm in Thailand with the help of his nephew Theo (Scott Hazell) and Jacks sister (and Theos mother) Allison (Elizabeth Healey). Jack is being constantly harassed by a new neighboring resort owned by the Konsong brothers. They want to get Jack shut down because they have buildings on the land and want to steal the rest. The Konsong brothers set animal welfare, bill collectors, and tax collectors on the farm. As animal welfare investigator Evelyn Namwong (Sherry Phungprasert) refuses to shut down the farm, having found only minor violations, she is fired. After two teenagers are eaten by a horrorized crocodile, the Konsongs send goons to break in and release three of the farms crocodiles. Jacks crocodiles are blamed and he gets orders to shut down until further notice.

The Crocodile that killed the teens reappears and eats a young boy near the docks. Its filmed by a tourist nearby and identified as a 20-foot salt water crocodile. Being much much larger and of a different species than Jacks, his farms name is cleared and he is able to reopen.  With the monster continuing to snack on the local population, Jack and Evelyn run into Hawkins (Michael Madsen), a man who has been hunting it for months to get revenge for its many victims. They join forces to find and kill the crocodile. While they are talking, they hear gunfire and  two hunters claiming to have killed the Croc which is actually one of Jacks escaped crocs. They quickly rush over, only to see the real croc kill one of the hunters.

While they hunt the crocodile, the Konsongs send someone to kill Allison, who paid his taxes and other bills to help him out. The goon tries to run her down, but misses. He leaves behind his cell phone which links him back to Andy Konsong.  Andy heads home to warn his brother that their plot has been uncovered, only to find pieces of him floating in a bloodied pool. The Crocodile was in the pool when he was swimming and killed him. Panicked, Andy calls 911, but then gets sick to his stomach. While hes throwing up in the pool, the Crocodile emerges and kills him.

When Evelyn, Jack, Hawkins, and the crocodile farm manager go looking for the crocodiles pit, Allison and Theo follow in a small boat. The Crocodile appears and snatches Allison from the boat, much to Theos horror. Jack and the rest of the team continue searching for the pit underwater while Hawkins and Theo go to search on land. They find the pit and, to their relief, Allison, who is unconscious but still alive. She wakes up to find the Crocodile with its jaws right in front of her head, but it leaves when it hears Jack calling for her. It comes up behind Jack, gets a hold of his foot, and drags him underwater. As its going down, Hawkins and Theo shoot at it, with Hawkins managing to shoot it in the brain. It dies with Jacks foot still firmly clamped in its mouth and him underwater. Stuck too high to get down to help, Hawkins advises Allison to cut off Jacks leg since they cant get the Crocodiles mouth open. Instead, the park manager uses a bang stick to blast it open and Jack gets to keep his leg.

==Production== Sci Fi Movie Central on Demand  in July 2007 due to a pre-licensing agreement.      

==Distribution== SCI FI Genius Products released the film to Region 1 DVD on February 5, 2008.  The film was re-released on August 19, 2008 as part of the second volume of the "Maneater Series Collection" sets. The volume also included Eye of the Beast and Grizzly Rage, the fifth and sixth films in the series, respectively.  In 2008, RHI Entertainment released the film to the iTunes store for downloading. 

== Critical reception ==
The film holds a score of 3.7/10 on the Internet Movie Database  and a 20% audience sore on Rotten Tomatoes. 

==See also==
*List of killer crocodile films

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 