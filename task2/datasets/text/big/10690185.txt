Blood on Wolf Mountain
{{Infobox film
| image          = 
| caption        = 
| name           = Blood on Wolf Mountain
| writer         = Fei Mu Lan Ping Zhang Yi
| director       = Fei Mu
| cinematography = 
| producer       = 
| studio         = Lianhua Film Company
| country        = China
| runtime        = 70 minutes
| released       =  
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 狼山喋血记
| fanti          = 狼山喋血記
| pinyin         = Láng shān dié xuě jì}}
}} Chinese film war with Invasion of Manchuria in 1931. The film was produced by the Lianhua Film Company and was released in November of 1936 in film|1936. {{cite web | url = http://www.ibseninchina.com.cn/Jiangqing.htm#_ftnref30
| title =  Jiang Qing and the “Year of Nora” 1935: Drama and Politics in the Republican Period  | accessdate = 2007-04-15 |date= 2002-10-06 | publisher =International Conference "Women in Republican China"}} 

== Plot ==
The film tells the story of a village that is beset by a pack of wolves. Though the symbolism was clear, the Japanese themselves refused to acknowledge that they could be represented by blood-thirsty wolves. {{cite web | url = http://www.time.com/time/magazine/article/0,9171,946771-1,00.html
| title = A Blue Apple in a City for Sale | accessdate = 2007-04-15 |date= 1977-03-27| | publisher = Time Magazine}} 

== Cast == Lan Ping (who later in life would adopt the name Jiang Qing and gain notoriety as the wife of Mao Zedong and a member of the Gang of Four).

==See also==
*Second Sino-Japanese War, the political background behind Blood on Wolf Mountain
*Cinema of China

== References ==
 

==External links==
* 
*  at the Chinese Movie Database

 
 
 
 
 
 
 

 