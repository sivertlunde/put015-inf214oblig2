Ladies of the Jury
{{Infobox film
| name           = Ladies of the Jury
| image          = Ladies of the Jury poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lowell Sherman
| producer       = William LeBaron
| screenplay     = Marion Dix Edward Salisbury Field Eddie Welch 
| based on       =   Ken Murray Roscoe Ates Kitty Kelly
| music          = Max Steiner
| cinematography = Jack MacKenzie
| editing        = Charles L. Kimball
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Ken Murray, Roscoe Ates and Kitty Kelly. The film was released on February 5, 1932, by RKO Pictures.  {{cite web|url=http://www.nytimes.com/movie/review?res=9D07E7D8163EE633A25751C0A9629C946394D6CF|title=Movie Review -
  Ladies of the Jury - A Triumphant Woman. - NYTimes.com|publisher=|accessdate=9 September 2014}}  

==Plot==
Middle aged Mrs. Livingston Baldwin Crane (Edna May Oliver) is selected to serve on a jury.  The case is the murder trial of ex-showgirl Yvette Gordon (Jill Esmond), accused of killing her rich elderly husband.
Throughout the trial Mrs. Crane is disruptive and untraditional, but is able to ask the witnesses candid and important questions.

At the end of the trial, Mrs. Crane casts the soul “not guilty” vote, causing a discussion.  After lots of convincing and several votes, the count is ten not guilty to two guilty.  
During deliberation, Mrs. Crane is able to secretly hire a detective agency to further investigate the case.  They prove that Chauncy, Mr. Crane’s nephew, paid the maid Mrs. Snow to lie under oath so Chauncy could inherit all his uncle’s money.

== Cast ==	
*Edna May Oliver as Mrs. Livingston Baldwin Crane
*Jill Esmond as Mrs. Yvette Gordon Ken Murray as Spencer B. Dazy
*Roscoe Ates as Andrew MacKaig 
*Kitty Kelly as Mayme Mixter
*Cora Witherspoon as Lily Pratt
*Robert McWade as Judge Henry Fish
*Charles Dow Clark as Jay J. Presley, Jury Foreman
*Helene Millard as Miss Evelyn Elaine Snow, Cranes Maid Kate Price as Mrs. McGuire

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 