Caught in the Headlights
{{Infobox film
| name           = Caught in the Headlights
| image          = Caught-headlights.jpg
| caption        = 
| director       = Gavin Wilding
| producer       = David Doerksen Philip Doerksen
| writer         = Deborah Brock Jerrold W. Lambert
| starring       = Erika Eleniak Kim Coates Brigitta Dau Stacy Keach Erin Gray
| music          = Harry Manfredini
| cinematography = Mark Dobrescu
| editing        = Jana Fritsch
| distributor    = Edge Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}}

Caught in the Headlights is an action thriller film released in 2005. The film stars Erika Eleniak, Kim Coates, Brigitta Dau, Stacy Keach, and Erin Gray.

==Filming==
The film was shot on locations of Saskatoon, Saskatchewan, Canada.

==Cast==
* Erika Eleniak as Kate Parker
* Kim Coates as Harry Eden
* Brigitta Dau as Claire Scott
* Stacy Keach as Mr. Jones
* Erin Gray as Mrs. Jones

==Release==
The film was released in the United States on January 18, 2005.

==External links==
* 

 
 
 
 

 