Hearth Fires
{{Infobox film
| name           = Hearth Fires
| image          = 1972 Les Feux de la Chandeleur.jpg
| image_size     = 
| caption        = Movie Poster "Les Feux de la Chandeleur"
| director       = Serge Korber
| producer       = 
| writer         = Serge Korber  Catherine Paysan
| narrator       = 
| starring       = Annie Girardot Claude Jade Jean Rochefort Bernard Fresson
| music          = Michel Legrand
| cinematography = 
| editing        = 
| distributor    = 
| released       = 24 May 1972
| runtime        = 95 mins
| country        = France Italy
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1972 cinema French film directed by Serge Korber.  The film is also known as La Divorziata (Italy).

==Synopsis==
Marie-Louise (Anne Giradot) is a woman whose love for her ex-husband will not die. The Lawyer Alexandre (Jean Rochefort) left her, because she attended a few leftist marches and demonstrations. While her daughter Laura (Claude Jade) falls in love with Marc (Bernard Fresson), Marie-Louise keeps hoping that Alexandre will come back to her. Laura helps her to fight for love and Marie-Louise is so attached to this idea that when her son (Bernard Le Coq) finally convinces her that he will never return, the realization has dire consequences.

==Cast==
*Annie Girardot (Marie Louise Boursault)
*Jean Rochefort (Alexandre Boursault)
*Claude Jade (Laura Boursault)
*Bernard Le Coq (Jean-Paul Boursault)
*Bernard Fresson (Marc Champenois)
*Jean Bouise (Father Yves Bouteiller)
*Gabriella Boccardo (Annie)
*Ilaria Occhini (Clotilde)

==Awards==
This movie was nominated for Best Picture at the 1972 Cannes Film Festival.    

==Discography==

The CD soundtrack composed by Michel Legrand is available on Music Box Records label ( ).

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 