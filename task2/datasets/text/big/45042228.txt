Rosie (1998 film)
{{Infobox film
| name           = Rosie
| image          = 
| caption        =
| director       = Patrice Toye
| producer       = Antonino Lombardo
| writer         = Patrice Toye
| starring       = {{plainlist|
* Aranka Coppens
* Sara de Roo
* Dirk Roofthooft
}}
| music          = John Parish 
| cinematography = Richard Van Oosterhout
| editing        = Ludo Troch
| studio         = Prime Time
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Belgium France
| language       = Dutch
| budget         = 
| gross          = 
}} Best Foreign Language Film at the 71st Academy Awards, but was not nominated. 

== Plot ==
Rosie, a teenage girl in Belgium, attempts to deal with her dysfunctional family and an adult world that she does not understand.

== Cast ==
* Aranka Coppens as Rosie
* Sara de Roo as Irene
* Dirk Roofthooft as Bernard
* Joost Wijnant as Jimi

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 67% of six surveyed critics gave the film a positive review; the average rating was 6.1/10.   Glenn Lovell of Variety (magazine)|Variety called it "the most incisive look at adolescent angst since Peter Jackson’s Heavenly Creatures".   Janet Maslin of The New York Times wrote that films decision to hide plot details until the climax "adds suspense, and eventually chills, to what would otherwise be an all too familiar tale of domestic dysfunction". 

==See also==
* List of submissions to the 71st Academy Awards for Best Foreign Language Film
* List of Belgian submissions for the Academy Award for Best Foreign Language Film

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 
 