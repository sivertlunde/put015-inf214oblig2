South Central (film)
{{Infobox film name           = South Central image          = South Central 1992 film.jpg caption        = Theatrical release poster writer         = Stephen Milburn Anderson Donald Bakeer (novel) starring       = Glenn Plummer Byron Minns Lexie Bigham Christian Coleman director       = Stephen Milburn Anderson producer       = William B. Steakley Oliver Stone Janet Yang distributor    = Warner Bros. released       = September 18, 1992 runtime        = 100 minutes
| country       = United States language       = English music          = awards         = budget         = $23,000,000 gross          = $14,373,196
}} 1992 American crime drama directed by South Central Los Angeles. The film stars Glenn Plummer, Byron Minns and Christian Coleman. South Central was produced by Oliver Stone and released by Warner Bros. The movie received wide critical acclaim, with New Yorker Magazine praising it as one of the years best independent films. South Central also placed Stephen Milburn Anderson in the New York Times "Whos Who Among Hot New Filmmakers," along with Quentin Tarantino and Tim Robbins. The 1998 Edward Norton drama American History X is often compared to this film by critics and fans.

==Plot== black gang member of Hoover Street Deuces, or simply "Deuce". He gets paroled from the Youth Authority and he meets up with his fellow gang members Ray Ray (Byron Minns), Loco and Bear. As it turns out, Bobbys girlfriend Carole (LaRita Shelby) gave birth to his son Jimmy (Christian Coleman) while he was incarcerated.  The Deuce gang goes out to a club owned by rival drug dealer Genie Lamp to celebrate Bobbys release, but Genie confronts them and threats are exchanged.

As Bobby and Jimmy attempt to return home from the party, they are approached by Genie Lamp and his bodyguard. The two force Bobby to come to Genies apartment and snort a line of what he thought was cocaine, but is actually heroin. Genie threatens Bobby and the Deuce gang while he is incapacitated.  Bobby returns home the next morning to find that Genie has given some of the same heroin to Carole and flies into a fit of rage.  Bobby informs Ray Ray and the Deuce gang makes plans to execute Genie Lamp.  They grab Genie in an abandoned warehouse.  Bobby carries out the execution by firing a gun through a potato into Genies head, and the gang flees the area as they hear the police sirens approaching.  Later that evening, Ray Ray gives Bobby his "heart", a small tattoo below the left eyelid which is a symbol of full initiation into the Deuce gang and can only be earned by killing an enemy.

Some time later, Bobby and Carole are in hiding from the police who are pursuing Genies murderers.  Bobby takes a walk and Bear pulls up in a brand new convertible with Loco in the back seat.  They solicit the services of a prostitute who turns out to be an undercover cop and arrests them.  The guys are taken to jail and Bobby is questioned by a detective for the murder of Genie Lamp. Bobby refuses to cooperate despite overwhelming evidence against him and the police allow him to see his son one last time. Bobby gets a ten year prison sentence for the murder he committed.

Nearly ten years later, Jimmy is now with the Deuce gang, and meets with Ray Ray. Ray Ray informs Jimmy that he wants him to start stealing car stereos for him and Ray Ray will pay him for the stolen goods. Ray Ray then gives Jimmy some money and a marijuana joint. The next scene switches over to Bobby who is in prison lifting weights. Over time, the film shows Jimmy stealing car stereos and selling them to Ray Ray for $20.00 a piece.  While stealing one night, Jimmy is caught by Willie Manchester, the owner of the car, and is shot in the back.  He is taken to a hospital where he fights for his life.  When the news of this reaches Bobby in prison, he leaves the Deuce gang immediately and joins up with a Muslim group, whose leader Amal encourages him to get an education and to go straight when he gets out of prison in order to be a better father to his child.  Amal and his associates even remove the Deuce "heart" from Bobbys face.  Meanwhile, Jimmy recovers from his gunshot wounds, but is taken to a boys home for the crime he committed because the court has declared Carole an unfit mother.  

Bobby is released from prison and goes to the boys home to visit Jimmy.  The two begin to talk, but Jimmy is shocked that his father has denounced the Deuce gang and will not seek revenge against Willie Manchester for shooting him. Jimmy leaves the room in anger and insults Bobby for not being the proud Deuce gang leader that Jimmy thought his father would be.  

Sometime later, Bobby goes to visit Ray Ray.  Ray Ray is initially happy to see Bobby saying the Deuce gang owes him "ten years of their lives" for the time he served in prison.  However, Bobby is shocked to find Jimmy has run away from the boys home and Willie has been taken hostage.  Ray Ray gives Jimmy a gun and tells him to shoot Willie in revenge so he can get his "heart" just like his father did.  Bobby tries to intervene but gets into a fight with Bear.  Bobby eventually overpowers Bear, takes his gun, and steps in front of Willie Manchester threatening to kill Ray Ray if this goes any further.  He then tells Jimmy that committing a crime against a man can be rectified, but killing a man cant.  He tells Ray Ray that the gang owes him ten years and can repay him by giving him back his son.  Then he can give his son what the both of them (Bobby and Ray Ray) never had, a father.  Bobby sees a sad look on Jimmy and Ray Rays faces as everyone lowers their guns.  Ray Ray tells Willie to get on out of there and Jimmy runs into Bobbys arms.  Bobby tells Jimmy that eventually the court will let him come home and they will be a father and son together without being criminals.

==Soundtrack==
  hip hop, soul and R&B music was released on September 18, 1992 by Hollywood Records.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 