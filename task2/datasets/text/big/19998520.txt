Biswaprakash
{{Infobox film name = Biswaprakash image = caption = Movie poster for Biswaprakash starring = Sanjeev Samal Nandita Das Christina Ranck Carman Cordwell director = Susant Misra producer = National Film Development Corporation of India writer = Susant Misra editor = Susant Misra music = Vikash Das cinematography = Jugal Debata distributor = National Film Development Corporation of India released       = 1999 runtime        =   148 minutes country = India
|language Oriya
}} India Oriya Grand Prix National Film Award as the best film in regional category.

== Plot ==
Biswaprakash, a young man in his early twenties is a rebel revolting constantly against the obscure traditional oriental lifestyle of his family rooted deeply in the medieval socio-religious mores. A misfit for the consumerist culture of a society where the value system is changing at a faster pace, he is alienated from his family and friends.
While so many questions relating to society, sex and morality intrigue him, Anjali, an independent, liberated and down-to-earth young woman enters his life. She strives to convert their mansion-like house into a holiday home besides looking after her paralysed mother, all alone. The friendship between Biswa and Anjali flourish amidst severe twists and turns till a policeman intervenes and breaks up the relationship.

A disheartened Biswa turns to religious rituals in despair with the belief to redeem himself of the misery. And then, one day, another woman enters his life this time June, a young lady traveller from abroad. Biswa befriends June and in her, he finds a new hope of escaping the claustrophobia. But a time comes when June deserts him for a new destination leaving Biswa utterly lonely and trapped in a situation of no return.

== Cast ==
*Sanjeev Samal as Biswaprakash
*Nandita Das  as Anjali
*Christina Ranck
*Carman Cordwell
*Binyaka
*Anu Chowdhury

== Awards and participation ==
*Shanghai International Film Festival, 1999
*Cairo International Film Festival, 1999
*Rotterdam Film Festival, 2001 (won Critics Choice award.
*International Film Festival of India, 2000 (Indian Panorama section) National Film Awards 2000  (Silver Lotus for Best Oriya Film)
   

== Music ==
Jugal Debata has arranged music for this film

== Shooting location ==
This movie was shot in Puri, in the state of Orissa, India. That city is also famous for Lord Jagannath Temple. The production team found a very famous old palatial building, in the heart of the city, which was a perfect match with the story line. The owner of that building is the famous Kaviraj Purna Chandra Rath.

== References ==
 
4. http://cinemasagar.com/film/biswaprakash/

== External links ==
* 
* 
* 
* 
* 
*  

 
 
 