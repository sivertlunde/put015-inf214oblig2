The Wild and the Innocent
 
{{Infobox film
| name           = The Wild and the Innocent
| image_size     =
| image	         = The Wild and the Innocent FilmPoster.jpeg
| caption        =
| director       = Jack Sher
| producer       = Sy Gomberg
| writer         = Sy Gomberg Jack Sher
| based on       = story by Sy Gomberg
| narrator       =
| starring       = Audie Murphy Joanne Dru Gilbert Roland Jim Backus Sandra Dee
| music          = Hans J. Salter
| cinematography = Harold Lipstein
| editing        = George Gittens
| studio         = Universal-International
| distributor    =
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Western film starring Audie Murphy and Sandra Dee as two inexperienced young people who get into trouble when they visit a town for the very first time. 

==Plot==
Shy mountain trapper Yancy (Audie Murphy) travels through Wyoming with his uncle and aunt. After Uncle Lije (George Mitchell) is injured by a bear, Yancy is sent to trade their beaver pelts for money and supplies.

When he arrives at the trading post, he finds it has been burnt down by an Indian who was sold moonshine by a lazy sneak thief, Ben Stocker (Strother Martin). The trading post owner tells Yancy that he will have to ride two more days to the nearest town to trade his furs.

Meanwhile, Stocker tries to trade his oldest daughter Rosalie (Sandra Dee) to Yancy for some furs. Yancy tells him to get out of there or else. The next day, Yancy finds that Rosalie has run away from her father and wants him to take her to town.

When they reach the town and he trades his furs, Yancy gets Rosalie some new clothes so she will be presentable to look for a job. The sheriff, Paul (Gilbert Roland), says he will find Rosalie a job at the dance hall and Yancy believes that this will be alright. He later finds out what goes on at the dance hall and goes to get Rosalie.  The sheriff tells him to go and Yancy has to defend himself.  Rosalie come out and follows Yancy.  The next day as Yancy is loading up to leave for the mountains Rosalie is watching him and crying.  Yancy tells her to be glad that she is staying at the general store and will be cared for by Mr. Forbes (Jim Backus) and his wife (Betty Harford).  In the end, uncle Lije and Yancy are heading along a trail in the mountains, with Rosalie riding on the back of Yancy’s horse.

==Cast==
*Audie Murphy as Yancy Hawks
*Joanne Dru as Marcy Howard
*Gilbert Roland as Sheriff Paul Bartell
*Jim Backus as Cecil Forbes
*Sandra Dee as Rosalie Stocker George Mitchell as Uncle Lije Hawks
*Peter Breck as Chip
*Strother Martin as Ben Stocker
*Wesley Marie Tackitt as Ma Ransome
*Betty Harford as Mrs. Forbes
*Mel Leonard as Pitchman
*Lillian Adams as Kiri Hawks
*Val Benedict as Richie

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 