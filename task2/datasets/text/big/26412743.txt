The Day That Shook the World
 
 
 
 

{{Infobox film
| name           = The Day That Shook the World
| image	=	The Day That Shook the World FilmPoster.jpeg
| alt            =
| caption        = American poster of the movie
| director       = Veljko Bulajić
| producer       = Vlado Brankovic Bohumil Pokorný
| writer         = Screenplay: Stevan Bulajić Vladimír Bor Paul Jarrico Veljko Bulajić
| starring       = Christopher Plummer Florinda Bolkan Maximilian Schell
| music          = Juan Carlos Calderón Lubos Fiser
| cinematography = Jan Curík
| editing        = Roger Dwyre
| studio         = Jadran Film Barrandov Studios Kinema Sarajevo
| distributor    = American International Pictures (USA)
| released       =
| runtime        = 122 minutes
| country        = Czechoslovakia Yugoslavia Germany German
}}
 assassination of Sophie in Sarajevo in 1914 and the immediate aftermath that led to the outbreak of World War I. 
 Franz Joseph Serbian nationalist, on 28 June 1914, his death set in motion a chain of events that resulted in the First World War. The movie chronicles the events surrounding that death and its aftermath. The assassination gave the Germans and Austrians reason to fear that the Russian Empire was actively fomenting unrest in the Balkans, since Serbia was a bone of contention throughout the region.

Upon its release, The Day That Shook the World was met with mixed reviews. The New York Times critic described the film as a "footnote to history that is rarely moving".  The movie was dubbed in theatres (USA - English, Czechoslovakia - Czech, Germany - German) and has aired only once on television, on Czechoslovak Television two years after its release in cinemas.

==Cast==
* Christopher Plummer as Archduke Franz Ferdinand
* Florinda Bolkan as Sophie, Duchess of Hohenberg
* Maximilian Schell as Đuro Šarac
* Irfan Mensur as Gavrilo Princip
*   as Nedeljko Čabrinović Mehmed Mehmedbašić
* Libuše Šafránková as Yelena
* Otomar Korbelář as Franz Joseph I of Austria Franz Conrad
* Jiří Holý as Erich von Merizzi
*   as Countess Langus
* Jiří Kodet as Morsley

==Release==
The film was released to cinemas on 31 October 1977 in Yugoslavia and Czechoslovakia. English-language premieres were in January 1977 in the UK and US. American International Pictures released the movie in 1977 with English dubbing. Later in the 1990s the movie was released to VHS.

==Review==
Despite an awesome title, "The Day That Shook the World", is more quaint than explosive. As a dramatization of the events leading up to and including the royal murders that triggered World War I, it is a fragmented revival of the past that evolves largely as a picturesque adventure rather than as persuasive history.

Filmed on authentic Serbian locations, the "Day That Shook the World" appears to have been cut somewhat confusingly, for all of the obviously good, serious intentions of its little-known director, Veljko Bulajić, and his American screenwriter Paul Jarrico.
 Habsburg dominance is merely indicated. And the backgrounds of the seemingly educated, mostly youthful, student-conspirators are also glossed over.

Photographs of the Archduke and his spouse show a rather heavy-set, mature couple, but in the persons of Christopher Plummer and Florinda Bolkan they are a strikingly photogenic, regal and loving pair. Mr. Plummer not only adores the equally loving, stately, brunette Miss Bolkan and their three children, but also carries off official duties at military maneuvers and dinners with the same brashness he evinces in a mild clash with Franz Joseph, the irascible, octogenarian emperor.

Maximilian Schell plays the role of the tortured, eventually ill-fated, bearded revolutionary who trains the callow assassins, with simple, glum determination. Among the largely Yugoslav supporting cast, Irfan Mensur plays Gavrilo Princip, the young conspirator who fired the fatal shots. Like his fellow actors, he simply portrays a harried personality, more involved in a series of escapes from detection by the police than in character delineation.

In directing his fairly large cast, Mr. Bulajic has succeeded in creating a good deal of melodrama, some tension and a few tepid romantic interludes. Jan Curik, his cinematographer, has captured the scenic charms of a picturesque countryside and the visual qualities of vintage automobiles, colorful uniforms and sham battles with toy-like soldiers in maneuvers.

==Awards== Best Foreign Language Film at the 48th Academy Awards, but was not accepted as a nominee. 

==See also==
* List of submissions to the 48th Academy Awards for Best Foreign Language Film
* List of Yugoslav submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 
* http://movies.nytimes.com/movie/review?res=9506EEDD1539E334BC4C51DFB766838C669EDE
*http://news.google.com/newspapers?nid=1946&dat=19750708&id=f5MuAAAAIBAJ&sjid=Y6EFAAAAIBAJ&pg=2817,2377963  
*http://books.google.com/books?id=PuQCAAAAMBAJ&lpg=PA74&ots=5pUfQbKvXj&dq=florinda%20bolkan%20in%20sarajevo&pg=PA74#v=onepage&q=shook&f=false  
 

 
 
 
 
 
 
 
 