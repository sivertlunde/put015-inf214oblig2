Mirage (1972 film)
{{Infobox film
| name           = Mirage
| image          = 
| caption        = 
| director       = Armando Robles Godoy
| producer       = Bernardo Batievsky
| writer         = Bernardo Batievsky Armando Robles Godoy
| starring       = Miguel Angel Flores
| music          = 
| cinematography = Mario Robles
| editing        = 
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = Peru
| language       = Spanish
| budget         = 
}}

Mirage ( ) is a 1972 Peruvian drama film directed by Armando Robles Godoy. Robles Godoy wrote the screenplay together with Bernardo Batievsky. It is the only Peruvian film to date to be nominated for a Golden Globe Award.

==Plot==
A story about a young man who inherits a broken down estate at the edge of the Peruvian desert, with no explanation about the former owners or what had become of the once thriving house. By searching through the rocks and sands for relics, he discovers the answers to the mystery, told in flashback. The film combines the boys search with other socio-economic issues relevant to Peru in a confusing, but insightful manner.

==Cast==
* Miguel Angel Flores
* Helena Rojo
* Hernán Romero
* Orlando Sacha

==Awards and nominations== Best Foreign Language Film at the 45th Academy Awards, but was not accepted as a nominee. 

;Won
 Cartagena Film Festival
*Best Film
 Chicago Film Festival
*Best Foreign Language Film

;Nominated
 Golden Globe Awards Best Foreign Film &ndash; Foreign Language

==See also==
* List of submissions to the 45th Academy Awards for Best Foreign Language Film
* List of Peruvian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 