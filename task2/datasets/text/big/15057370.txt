Miss Gold Digger
{{Infobox film
| name           = Miss Gold Dig
| image          = Miss Gold Digger film poster.jpg
| caption        = Theatrical poster
| film name = {{Film name
 | hangul         =  
 | hanja          =   미스 신
 | rr             = Yonguijudo miseu shin
 | mr             = Yongŭijudo misŭ sin}}
| director       = Park Yong-jib
| producer       = 
| writer         = Park Eun-yeong
| starring       = Han Ye-seul Lee Jong-hyuk
| music          = Jo Yeong-wook
| cinematography = Kim Hak-su
| editing        = Park Gok-ji Sidus FNH
| released       =  
| runtime        = 108 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = $3,948,507 http://boxofficemojo.com/movies/intl/?page=&wk=2007W51&id=_fYONGUIJUDOMISEUS01 
}}
Miss Gold Digger (  literally Prudent-Careful Miss Shin) is a 2007 South Korean film.

== Plot ==
Sin Min-joo is juggling relationships with four different men, adopting a different persona for each one. But her love life becomes increasingly tangled, and the men show their true colours when they find out how theyve been manipulated.

== Cast ==
* Han Ye-seul ... Sin Min-joo
* Lee Jong-hyuk ... Dong-min
* Kwon Oh-joong ... Joon-Seo
* Kim In-kwon ... Yoon-Cheol
* Son Hoyoung ... Hyeon-Joon
* Jeong Gyoo-soo ... Mr Choi, board member
* Yoo Eun-jeong ... Yeong-mi
* Oh Seo-won ... Hye-jeong
* Jo Hye-jin ... Min-soos friend
* Yeon-woo Hyeon-jin ... Yoo-jin
* Jeong Da-yeong ... Dong-hee
* Jeong Hae-soo ... Yeong-mis husband
* Lee Won-jae ... Vice manager
* Yoo Soon-cheol ... Monk
* Park Joon-mook ... Condo kid
* Ban Hye-ra ... Condo woman
* Lee Jeong-hak ... Squad leader
* Kang Rae-yeon ... Yang Dae-ri (cameo)
* Lee Jae-hoon ... Drunken man (cameo)
* Im Ye-jin ... Min-soos mother (cameo)
* Chun Ho-jin ... Min-soos father (cameo)
* Yoon Joo-sang ... Dong-mins father (cameo)
* Hong Yeo-jin ... Dong-mins mother

== References ==
 

== External links ==
*    
*  
*  

 
 
 
 
 


 