Saturday Night Fever
 
 
{{Infobox film
| name = Saturday Night Fever
| image = Saturday_night_fever_movie_poster.jpg
| caption = Theatrical release poster
| director = John Badham
| producer = {{Plain list | 
* Robert Stigwood  Kevin McCormick (Executive Producer) 
}}
| screenplay = Norman Wexler
| based on =  
| starring = {{Plain list | 
* John Travolta
* Karen Lynn Gorney Barry Miller
* Joseph Cali
* Paul Pape
* Donna Pescow
* Bruce Ornstein
* Martin Shakar
* Julie Bovasso
* Fran Drescher
}}
| music = {{Plain list | 
* Barry Gibb
* Maurice Gibb
* Robin Gibb
* David Shire
}}
| cinematography = Ralf D. Bode
| editing = David Rawlins
| studio = RSO Records
| distributor = Paramount Pictures
| released =  
| runtime = 118 minutes (R-rated) 112 minutes (PG-rated)
| country = United States
| language = English
| budget = $3.5 million 
| gross = $237.1 million 
}}
Saturday Night Fever is a 1977 American  , clashes with his unsupportive and squabbling parents, racial tensions in the local community, and his associations with a gang of macho friends.

A huge commercial success, the film significantly helped to popularize   styles of clothing; pre-AIDS sexual promiscuity; and graceful choreography.
 Mod  acquaintance of Cohns. In 2010, Saturday Night Fever was deemed "culturally, historically, or aesthecially significant" by the Library of Congress and slated to be preserved for all time in their National Film Registry.
 Staying Alive (1983 in film|1983) also starred John Travolta and was directed by Sylvester Stallone.

==Plot== Barry Miller). A fringe member of this group of friends is Annette (Donna Pescow), a neighborhood girl who longs for a more permanent physical relationship with Tony.

One plot device in the films narrative is the Verrazano–Narrows Bridge on which the friends ritually stop to clown around.  The bridge has special significance for Tony as a symbol of escape to a better life on the other side—in more suburban Staten Island.

Tony agrees to be Annettes partner in an upcoming dance contest at 2001 Odyssey, but her happiness is short-lived when Tony is mesmerized by another woman at the club, Stephanie Mangano (Karen Lynn Gorney), who executes intricate dance moves with exceptional grace and finesse. Although Stephanie coldly rejects Tonys advances, she eventually agrees to be his partner in the dance competition, provided that their partnership will remain strictly professional.  Tonys older brother, Frank Jr. (Martin Shakar), who was the pride of the Manero family since he was ordained a Roman Catholic priest, brings despair to their parents when he tells them that he has left the priesthood. Tony shares a warm relationship with Frank Jr., but feels vindicated that he is no longer the black sheep of the family.
 dispensation for an abortion. When Frank tells him this would be highly unlikely, Bobbys feelings of despair intensify. Bobby lets Tony borrow his 1964 Chevrolet Impala to help move Stephanie from Bay Ridge to Manhattan, and futilely tries to extract a promise from Tony to call him later that night.

Eventually, the group gets their revenge on the Barracudas, and crash Bobby Cs car into their hangout. Tony, Double J, and Joey get out of the car to fight, but Bobby C. takes off when a gang member tries to attack him in the car. When the guys visit Gus in the hospital, they are angry when he tells them that he may have targeted the wrong gang. Later, Tony and Stephanie dance at the competition and end up winning first prize. However, Tony believes that a Puerto Rican couple performed better, and that the judges decision was racially biased.  He gives the Puerto Rican couple the first prize trophy, and leaves with Stephanie in tow. Once outside in a car, she denigrates their relationship and he tries to rape her.  She viciously resists and runs from him.

Tonys friends come to the car along with a drunk and stoned Annette.  Joey says she has agreed to have sex with everyone. Tony tries to lead her away, but is subdued by Double J and Joey, and sullenly leaves with the group in the car. Double J and Joey begin raping Annette while she is still too incapacitated to resist and continue even as she sobers up. Bobby C. pulls the car over on the Verrazano-Narrows Bridge for their usual cable-climbing antics. Typically abstaining, Bobby gets out and performs more dangerous stunts than the rest. Realizing that he is acting recklessly, Tony tries to get him to come down. Bobbys strong sense of alienation, his deadlocked situation with Pauline, and Tonys broken promise to call him earlier that day—all culminate in a suicidal tirade about Tonys lack of caring before Bobby slips and falls to his death into the river below them.

Disgusted and disillusioned by his friends, his family, and his life, Tony spends the rest of the night riding the subway into Manhattan.  Morning has dawned by the time he appears at Stephanies apartment.  He apologizes for his bad behavior, telling her that he plans to relocate from Brooklyn to Manhattan to try and start a new life. Tony and Stephanie salvage their relationship and agree to be friends, sharing a tender moment as the credits roll.

==Cast==
 
* John Travolta as Anthony "Tony" Manero
* Karen Lynn Gorney as Stephanie Mangano Barry Miller as Bobby C.
* Joseph Cali as Joey
* Paul Pape as Double J.
* Donna Pescow as Annette
* Bruce Ornstein as Gus
* Val Bisoglio as Frank Manero, Sr.
* Julie Bovasso as Flo Manero
* Martin Shakar as Father Frank Manero, Jr.
* Nina Hansen as Tonys grandmother
* Lisa Peluso as Linda Manero
* Sam Coppola as Dan Fusco
* Denny Dillon as Doreen
* Robert Weil as Becker
* Fran Drescher as Connie  Monti Rock III as the deejay
* Ann Travolta as pizza girl
* Helen Travolta as customer in paint store (Travoltas mother)
 

==Production==
Donna Pescow was almost considered "too pretty" for the role of Annette. She corrected this by putting on 40 pounds (18 kilograms) and training herself back to her native Brooklyn accent, which she trained herself away from while she was studying drama at the American Academy of Dramatic Arts. After production ended, she immediately lost the weight she gained for the role and dropped the accent.

John Travoltas mother Helen and sister Ann both appeared in minor roles in this movie. Travoltas sister is the pizzeria waitress who serves him the pizza slices, and his mother is the woman to whom he sells the can of paint early in the film.

John G. Avildsen was signed to direct but was fired three weeks prior to principal photography over a script dispute with producer Robert Stigwood. Despite this, one reference to Avildsen directing remains in the final film—John Travoltas character has a Rocky poster in his room (the first film in that series was directed by Avildsen).

==Filming locations== Bay Ridge, Sunset Park, and Bensonhurst, Brooklyn|Bensonhurst:

* Verrazano–Narrows Bridge.
* Basketball courts located along the Parkway near the Marine Avenue in Brooklyn, New York. The Verrazano Bridge west bound approach runs above the park containing the courts.
* Bench vista toward the Verrazano Bridge, located along the Shore Promenade in Shore Road Park, Brooklyn, New York.  Accessible to pedestrians via the Shore Road and 4th Avenue footpath park entrance. Accessible to motorists via the parking area alongside the Belt Parkway, east bound just   prior to Exit 2, 4th Avenue/Fort Hamilton Parkway, and approximately   from passing underneath the bridge. White Castle fast food restaurant, formerly located at 92nd Street and 4th Avenue, Brooklyn, New York now a Uno Chicago Grill.
* Phillips Dance Studio, West 7th Street and Bay Parkway.
* 2001 Odyssey was later renamed Spectrum (a gay club) in 1987, before being demolished in 2005. The club was located at 802 64th Street, Sunset Park, Brooklyn, New York.
* Six Brothers Hardware and Paints, formerly located at 7309 5th Avenue in Brooklyn, was the backdrop for Tonys workplace. Grand Union Staples store located at 9319 5th Avenue in Brooklyn, NY.
* Fishermens Corner a seafood restaurant, now Giuffre FIAT located on the north side of 94th Street between 4th Avenue and 5th Avenue in Brooklyn, NY.
* Barracudas hangout, 45th Street Sunset Park, Brooklyn.
* Lennys Pizza, 1969 86th Street (near 20th Avenue).

==Soundtrack==
 
;Track listing !! Artist !! Time
# "Stayin Alive" performed by Bee Gees - 4:45
# "How Deep Is Your Love" performed by Bee Gees - 4:05
# "Night Fever" performed by Bee Gees - 3:33 More Than a Woman" performed by Bee Gees - 3:17
# "If I Cant Have You" performed by Yvonne Elliman - 3:00
# "A Fifth of Beethoven" performed by Walter Murphy - 3:03 Tavares - 3:17
# "Manhattan Skyline" performed by David Shire - 4:44
# "Calypso Breakdown" performed by Ralph MacDonald - 7:50
# "Night on Disco Mountain" performed by David Shire - 5:12
# "Open Sesame" performed by Kool & the Gang - 4:01
# "Jive Talkin" performed by Bee Gees - 3:43 (*)
# "You Should Be Dancing" performed by Bee Gees - 4:14
# "Boogie Shoes" performed by KC and the Sunshine Band - 2:17
# "Salsation" performed by David Shire - 3:50
# "K-Jee" performed by MFSB - 4:13
# "Disco Inferno" performed by The Trammps - 10:51
:With the exception of (*) track 12 "Jive Talkin", all of the songs are played in the film.
:The novelty songs "Dr. Disco" and "Disco Duck", both performed by Rick Dees, are also played in the film but not included on the album.

According to the DVD commentary for Saturday Night Fever, the producers intended to use the song "Lowdown" by Boz Scaggs for use in the rehearsal scene between Tony and Annette in the dance studio, and choreographed their dance moves to the song. However, representatives for Scaggs label, Columbia Records, refused to grant legal clearance for it, as they wanted to pursue another disco movie project, which never materialized.  Composer David Shire, who scored the film, had to in turn write a song to match the dance steps demonstrated in the scene and eliminate the need for future legal hassles.  However, this track does not appear on the movies soundtrack.

The song "K-Jee" was used during the dance contest with the Puerto Rican couple that competed against Tony and Stephanie. Some VHS cassettes used a more traditional Latin-style song instead. The DVD restores the original recording.

The album has been added to the National Recording Registry in the Library of Congress. 

==Distribution==

===Theatrical releases===
 
Two theatrical versions of the film were released: the original R-rated version and an edited PG-rated version. (The PG-rated re-issue was in 1979; the middle-ground PG-13 rating was not created until 1984.)
 first run, and totaled 118 minutes.

After the success of the first run, in 1979, the films content was re-edited into a toned down, PG-rated version and re-released during a second run, not only to attract a wider audience, but also to capitalize on attracting the target audience of the teenagers who were not old enough to see the film by themselves, but who made the soundtrack album to the film a monster hit.  The R-rated versions profanity, nudity, fight sequence, and a multiple rape scene in a car, were all de-emphasized or removed from the PG version.

Producer Robert Stigwood said in a recent interview   on "The Inside Story: Saturday Night Fever", about the PG version: "It doesnt have the power, or the impact, of the original, R-rated edition."
 takes of the same scenes, substituting milder language initially intended for the network television cut. To maintain runtime, a few deleted scenes were restored (including Tony dancing with Doreen to "Disco Duck",  Tony running his finger along the cables of the Verrazano–Narrows Bridge, and Tonys father getting his job back).

In 1980, Paramount Pictures paired up the PG-rated version of the film as a double feature along with its other John Travolta blockbuster, Grease (film)|Grease. 

When Saturday Night Fever premiered on HBO in 1980, they aired both versions of the film: the PG version during the day, and the R version during the evening (HBO had a programming rule of only showing R rated films during the evening.  This was before switching to a 24-hour-a-day operation, while still under their old broadcast standards concerning R rated films).

===Home media=== TNT started showing the original R-rated version with a TV Parental Guidelines|TV-14 rating. The nudity was removed/censored, and the stronger profanity was either edited or (on recent airings) silenced. But this TV edit included some of the innuendos from the original film that were edited or removed from the PG version. Turner Classic Movies has aired the film in both versions (the R-rated version is commonly seen on their normal lineup, while the PG version has appeared on TCMs "Funday Night at the Movies" and "Essentials Jr." program blocks.)

The network television version (which premiered on November 16, 1980 on American Broadcasting Company|ABC) was basically a slightly shortened form of the PG-rated version, but contained several minutes of out-takes normally excised from both theatrical releases to make up for lost/cut material. It is among the longest cuts of the film.

On May 5, 2009, Paramount released Saturday Night Fever on Blu-ray Disc in 1.78:1 aspect ratio. This release retains the R-rated version of the film along with many special features new to home media. 

==Reception==

===Critical response===
Saturday Night Fever received positive reviews and is regarded by many critics as one of the best films of 1977.     
Rotten Tomatoes retrospectively collected reviews from 40 critics to give the film a score of 90%.  At Metacritic the film was given a score of 77/100 (mostly favorable) based on reviews from 7 critics.  
It was added to The New York Times "Guide to the Best 1,000 Movies Ever Made", which was published in 2004. 
In 2010, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

Film critic Gene Siskel, who would later list this as his favorite movie, praised the film: "One minute into Saturday Night Fever you know this picture is onto something, that it knows what its talking about." He also praised John Travoltas energetic performance: "Travolta on the dance floor is like a peacock on amphetamines. He struts like crazy."  Siskel even bought Travoltas famous white suit from the film at a charity auction. 

Film critic Pauline Kael wrote a gushing review of the film in The New Yorker: "The way Saturday Night Fever has been directed and shot, we feel the languorous pull of the discotheque, and the gaudiness is transformed. These are among the most hypnotically beautiful pop dance scenes ever filmed...Travolta gets so far inside the role he seems incapable of a false note; even the Brooklyn accent sounds unerring...At its best, though, Saturday Night Fever gets at something deeply romantic: the need to move, to dance, and the need to be who youd like to be. Nirvana is the dance; when the music stops, you return to being ordinary." 

===Awards and nominations===
Award wins: National Board of Review Award for Best Actor - John Travolta Golden Screen Award, Germany

Award nominations:
* Academy Award for Best Actor - John Travolta
* BAFTA Award for Best Film Music - Barry Gibb, Maurice Gibb, Robin Gibb
* BAFTA Award for Best Sound - Michael Colgan, Robert W. Glass Jr., Les Lazarowitz, John T. Reitz, John Wilkinson
* Golden Globe Award for Best Motion Picture - Musical or Comedy Golden Globe Award for Best Actor - Musical or Comedy – John Travolta
* Golden Globe Award for Best Original Score - Barry Gibb, Maurice Gibb, Robin Gibb, David Shire How Deep Is Your Love?" Writers Guild Best Drama Written Directly for the Screen - Norman Wexler

American Film Institute Lists
* AFIs 100 Years...100 Movies - Nominated
* AFIs 100 Years...100 Songs:
** Stayin Alive - #9
** More Than a Woman - Nominated
* AFIs 100 Years...100 Cheers - Nominated
* AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated

== References in popular culture == tilted camera angles show Belushi combing his hair in front of the mirror as "Stayin Alive" plays in the background.  Ironically, the oft-repeated phrase in the movie, "Can you dig it? I knew that you could," had been made famous on Saturday Night Live during a stand-up performance by Billy Crystal.

Mad Magazine spoofed the film in their September 1978 Issue 201.  It was entitled "Saturday Night Feeble" and was written by Arnie Kogen and illustrated by Mort Drucker.

English comedian Benny Hill also spoofed the film on his popular sketch program The Benny Hill Show called "Friday Night Fever."

The Childrens Television Workshop published a record album of music from Sesame Street under the title Sesame Street Fever, the cover of which spoofed the Saturday Night Fever soundtrack album cover, with muppet Grover wearing the white three-piece disco suit in the famous Travolta pose and Bert, Ernie, and Cookie Monster taking the place of the Bee Gees. Robin Gibb (of the Bee Gees) sings on two tracks for this album, "Sesame Street Fever" and "Trash", and has a dialogue with Cookie Monster on the intro for "C Is For Cookie."

The Goodies parodied the film in their Saturday Night Grease episode.

The 1980 film Airplane! contained a parody scene, with Robert Hays mocking the famous pose and the clothing shown on the poster and album cover, to the tune of "Stayin Alive" slightly sped up (the actual song used for that scene in Saturday Night Fever was "You Should Be Dancing").

In the 1985 film Teen Wolf, there is a scene in which Michael J. Foxs character as the wolf is getting ready for a school dance by standing in front of a bathroom mirror blow-drying his hair a la John Travolta as Tony Manero.
 Short Circuit More Than A Woman". (The director of both films was John Badham)

On the political comedy series Spin City which also starred Fox, Paul Lassiter played by Richard Kind is walking the hallways of City Hall with "Stayin Alive" playing in the background in the first season episode "Gabbys Song" after spending the night with his girlfriend Claudia portrayed by Faith Prince.

In the film Look Whos Talking (1989), the opening of "Staying Alive" is heard as Mikey, in the stroller, hits the street being wheeled by James (played by John Travolta).
 season 6, Bart for dinner, upon which he says, "Theres only one thing to do at a moment like this: strut!". Bart then struts to "Stayin Alive" in the same manner as Travoltas character at the end of the sequel Staying Alive.

In 1998, Singaporean filmmaker Glen Goei made Forever Fever (Thats the Way I Like It). Set in Singapore during the 1970s, the film starred Adrian Pang as the Tony Manero character who eventually develops a liking for disco dancing. The movie also used cover versions of songs from the Saturday Night Fever soundtrack.

In 2000, at the Inner Circle press dinner, mayor Rudy Giuliani spoofed John Travolta by dancing to "Disco Inferno" by The Trammps. Giuliani wore a white 70s-style disco suit.

On June 25, 2002, in an episode of Son of the Beach, David Arquette guest-starred as Johnny Queefer in a send-off episode entitled "Saturday Night Queefer", which also included parodies of the Bee Gees songs sung by a quartet of guys breathing helium balloons to get the high voices like the Gibb brothers.

In the 2004 video game World of Warcraft, the human male character can be made to perform Travoltas signature dance.

In the 2005 film Madagascar (2005 film)|Madagascar, when Marty the Zebra escapes the zoo and walks on the streets of Manhattan, the camera displays a similar fashion of the intro of Saturday Night Fever and the song "Stayin Alive" was played in the background.
 John Bishop includes a tribute to Saturday Night Fever at the end of his performance on his latest Sunshine stand-up tour. His finale whilst playing in theaters, he included a video of him re-enacting the opening scene and dancing at the discothèque however, whilst performing at arenas, he showed extracts of the video and the section where he is supposed to dance at the disco, he emerges on stage with a troupe of dancers and performs the dance routine like John Travolta.
 Fox TV series Glee (TV series)|Glee episode 16, "Saturday Night Glee-ver", which pays tribute to the film and features various songs from its soundtrack (especially the songs performed by the Bee Gees), covered by the series cast.  

==References==
 

==External links==
 
*  
*  
*  
*  
*   The NY Magazine article by Nik Cohn that inspired the film
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 