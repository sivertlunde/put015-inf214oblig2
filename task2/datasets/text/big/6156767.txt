Golden Chicken
 
 
{{Infobox Film
| name           = Golden Chicken
| image          = Golden Chicken.jpg
| image_size     = 
| caption        = DVD cover art
| director       = Samson Chiu
| producer       = 
| writer         = Samson Chiu Matt Chow
| narrator       = 
| starring       = Sandra Ng Eric Tsang Andy Lau Tony Leung Ka-fai Hu Jun Eason Chan
| music          = Peter Kam
| cinematography = Jacky Tang Cheung Ka-Fai
| distributor    = 
| released       = 
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}
Golden Chicken (金雞 gam1 gai1) is a 2002 Hong Kong comedy-drama film directed by Samson Chiu starring Sandra Ng and involving cameo appearances from Andy Lau and Eric Tsang.

It was followed by two sequels: Golden Chicken 2 in 2003 and Golden Chicken 3 in 2014.

==Synopsis==
Kam (Sandra Ng) is a long time Hong-Kong prostitute, when she gets locked in a cash machine vestibule with a would be thief (Eric Tsang) she tells him some stories of her life as a prostitute and how she come to be where she is today.

==Cast==
* Sandra Ng – Kam
* Eric Tsang – James Bong
* Andy Lau – Himself
* Tony Leung Ka-fai – Professor Chan
* Eason Chan – Steely Willy
* Hu Jun – Yeh
* Alfred Cheung – Doctor Cheung
* Chapman To – Club owner
* Felix Wong – Richard Tiffany Lee – Kimmy
* Kristal Tin – Kams mamasan
* Irene Tsu – Kams aunt

==Awards==
* 22nd Annual Hong Kong Film Awards
** Nomination – Best Picture
** Nomination – Best Actress (Sandra Ng Kwun-Yu)
** Nomination – Best Supporting Actress (Krystal Tin Yui-Lei)
** Nomination – Best Art Direction (Hai Chung-Man and Wong Bing-Yiu)
** Nomination – Best Costume Design (Hai Chung-Man and Dora Ng Lei-Lo)
* 40th Annual Golden Horse Awards
** Winner – Best Actress (Sandra Ng Kwun-Yu)
** Winner – Best Art Direction (Hai Chung-Man and Wong Bing-Yiu)
** Winner – Best Makeup and Costume Design (Hai Chung-Man and Dora Ng Lei-Lo)

==External links==
*  
*  
*  

 
 
 
 
 

 