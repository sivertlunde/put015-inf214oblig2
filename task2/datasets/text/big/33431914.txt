180 (2011 American film)
 
{{Infobox film
| name           = 180
| image          =
| alt            =  
| caption        = 
| director       = Ray Comfort
| producer       = Ray Comfort Mark Spence
| writer         = Ray Comfort
| based on       =  
| starring       = 
| music          = 
| cinematography = Chad Williams Emeal Zwayne Ray Comfort Dale Jackson Stuart Scott Brad Snow
| editing        = Mark Spence Eddie Roman
| studio         = Living Waters Publications
| released       =  
| runtime        = 33 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
180 (also known as 180: Changing the Heart of a Nation or 180 Movie) is a 33-minute 2011 anti-abortion movement|anti-abortion documentary film produced by Ray Comfort, founder of Living Waters Publications.       The film is distributed by Living Waters on DVD and has been posted publicly on the films official website and YouTube. The film is notable for comparing abortions to the Holocaust.

==Synopsis==
The film begins by showing images of the Holocaust, and stating that Hitler sanctioned the killing of 11 million people.  This is followed by Comfort interviewing people about Adolf Hitler; their responses indicate a lack of historical knowledge, although he also finds a neo-Nazi who claims to love Hitler.  Comfort proposes a hypothetical situation to his interviewees, asking if they would kill Hitler if they had the opportunity at that time in history. He asks more hypotheticals dealing with what his interviewees might do in other circumstances related to the Holocaust.

He then switches his topic to make similar comparisons to abortion within the United States and the right to life, personalizing his arguments to make comparisons between the Holocaust and abortion in order to place the interviewees on the spot.

The documentary concludes with Comfort stating that over 50 million abortions have occurred to date; he calls this the "American Holocaust".

==Background==
The documentary was originally intended to be a free DVD supplement to Comforts book Hitler, God, and the Bible.    Comfort compared his film to the YouTube video Charlie bit my finger, which had accumulated millions of views, and offered his hope that 180 would achieve the same viewership and thus serve to shift opinion on abortion. 

==Reception==
Within days, 180 had over half a million views on YouTube,  while weeks later, it hit the 1.2 million mark.  The 180 movie passed 3 million views as of May 16, 2012. 

On October 12, 2011, The Huffington Post wrote an article critical of 180. The article quoted Holocaust survivor Elie Wiesel, who said in a 1991 interview with On The Issues Magazine, "It is blasphemy to reduce a tragedy of such monumental proportions to this human tragedy, and abortion is a human tragedy."    According to The Huffington Post, "the film, which shows a series of graphic images, is gaining attention not only because of its controversial comparison, but because it highlights 14 people who do not know who Adolf Hitler was". 

The Christian Examiner called the film "dramatic" and stated that it was "gaining national attention". It reported on Ray Comforts use of "morally charged" questions and his attempts to change the minds of "mostly college-aged" interviewees, and how eight of those interviewed who previously espoused a pro-choice view changed their minds as a result of the interview. 

British newspaper The Catholic Herald criticised the film for bludgeoning and hectoring its subjects and audience and using "verbal violence" when love was the better way to change peoples minds; however it also found that many of its readers agreed with the films message and tactics. 
 ADL and himself a Holocaust survivor, criticized the film, stating "This film is a perverse attempt to make a case against abortion in America through the cynical abuse of the memory of those killed in the Holocaust," adding "It is, quite frankly, one of the most offensive and outrageous abuses of the memory of the Holocaust we have seen in years."  He decried the films assertion that there is somehow "a moral equivalency between the Holocaust and abortion" and its bringing Jews and Jewish history into a discussion that then urges viewers to "repent and accept Jesus as their savior." 

The Florida Independent reported that Ray Comfort is "turning his attention to high schools" in order to "fill in gaps in education about the Holocaust". It reports the film as "largely anti-abortion propaganda" which has "become extremely popular in anti-abortion circles". 

The makers also had trouble with some billboard owners who refused to carry advertising for the film, with Comfort complaining one Jewish company owner was particularly opposed.  Comfort also claimed "three of the largest billboard companies in Southern California" refused to advertise it. 

In 2015 a woman from the United Kingdom was dismissed from a Christian school and then contacted by Child Protective Services for showing the film to her own children. 

==Release==
The documentary was produced by Ray Comfort with the help of his ministry, Living Waters Publications.  It was released on Sunday, September 18, 2011.  As of November 4, 2011, the film had received over 1 million views on its official website. 

==References==
 

==External links==
*  
* 
*  

 
 
 
 
 