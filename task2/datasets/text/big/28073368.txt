Shadow of Evil
 
{{Infobox film
| name = Banco à Bangkok pour OSS 117
| image = Shadofevil.jpg
| caption = American poster
| stars = Kerwin Mathews Pier Angeli Robert Hossein
| screenplay = Pierre Foucaud  Michel Lebrun André Hunebelle
| story     = Jean Bruce
| director = André Hunebelle
| producer = Paul Cadéac
| cinematography = Raymond Pierre Lemoigne
| music = Michel Magne
| editing = 
| studio = Production Artistique Cinématographique Da.Ma. Cinematografica 
| distributor = 
| released = June 1964
| runtime = 118 minutes
| country = France
| budget = 
}}

Banco à Bangkok pour OSS 117 released in the USA as Shadow of Evil  is a 1964 French/Italian international co-production Eurospy spy-fi film.   It was based on Jean Bruces 1960 novel Lila de Calcutta, the 74th OSS 117 novel. It was the second OSS 117 film directed by André Hunebelle and produced by Paul Cadéac, the first in the series in colour, the first co-produced by the Italian company Da.Ma. Cinematografica and the last to star Kerwin Mathews as OSS 117.

The film was shot on Thai locations and featured action scenes arranged by Hunebelles stunt coordinator Claude Carliez with production design by René Moulaert.

==Plot== plague epidemics in India following health workers inoculating the locals to protect them from cholera.  Lemmon had discovered that the medicine made in Hogby Laboratries in Bangkok had been switched with plague germs infecting the population with fatal results.
 Malthusian organisation known as the "People Elect".  The "People Elect" (inoculated by Dr Sinn for immunity) desire to spread plague around the world to reduce the Earths population and stop atomic testing that is ruining the planet.

== Cast ==
* Kerwin Mathews as Hubert Bonisseur de La Bath
* Pier Angeli as Lila Sinn, the doctors sister
* Robert Hossein as Dr Sinn
* Dominique Wilms as Eva Davidson, Leacocks secretary
* Gamil Ratib as Akhom 
* Henri Virlojeux as Leacock 
* Jacques Mauclair as Mr Smith 
* Henri Guégan as Karloff 
* Raoul Billerey as Christopher Lemmon 
* Jacques Hilling as Hogby

==References==
 

==External links==
*  
* original French film trailer http://www.youtube.com/watch?v=tycLRoQtpbg
 

 
 
 
 
 
 
 
 
 


 