The Shaggy Dog (1959 film)
{{Infobox film
| name           = The Shaggy Dog  
| image          = The Shaggy Dog - 1963 - Poster.png
| caption        = Theatrical Poster Charles Barton Bill Walsh
| narrator       = Paul Frees (opening only)
| writer         = Lillie Hayward Bill Walsh from the novel by Felix Salten
| starring       = Fred MacMurray Jean Hagen Tommy Kirk Annette Funicello Tim Considine Paul J. Smith Edward Colman
| editing        = Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 104 min.
| country        = United States
| language       = English French Latin Spanish
| budget         = under $1 million Charles Tranberg, Fred MacMurray: A Biography, Bear Manor Media, 2014 
| gross          = $8.1 million (est. US/ Canada rentals) 
}}
 transformed into title character, Charles Barton and stars Fred MacMurray, Tommy Kirk, Jean Hagen, Kevin Corcoran, Tim Considine, Roberta Shore, and Annette Funicello. It was the first ever Walt Disney live-action comedy.
 Walt Disney Dean Jones, Tim Conway, and Suzanne Pleshette. It was followed by a 1987 TV-movie sequel, a 1994 TV-movie remake and a 2006 theatrical remake (see Legacy section below). A colorized version of the film can be found on the 1997 VHS.

== Plot ==
Wilbur "Wilby" Daniels is a boy who is constantly misunderstood by his father, Wilson. Wilson thinks Wilby is crazy half the time because of his elder sons often dangerous inventions. As a retired mailman who often ran afoul of canines, he has a hatred of dogs, and he is unable to understand why his younger son, Montgomery "Moochie" wants a dog so badly.

Wilby and his self-centered rival Buzz Miller take a new French girl, named Francesca Andrassé, to the local museum. Wilby gets separated from the other two, who leave without him. Wilby ends up in a new wing, where he encounters former acquaintance Professor Plumcutt (whose newspaper Wilby used to deliver), who tells him all about mystical ancient beliefs, including the legend of the Borgia family, who used shape-shifting as a weapon against their enemies.

On the way out, Wilby collides with a table holding a display case of jewelry, ending up with one of the rings in the cuff of his pants which he finds later. It is the cursed Borgia ring, and when he reads the inscription on it, he turns into Chiffon, Francescas shaggy Bratislavian sheepdog. Confused about what has happened, Wilby as a dog goes to Professor Plumcutt, who says he has invoked the Borgia curse upon himself, which can only be broken through a heroic act of selflessness. After getting chased out of his own house by his enraged father (who doesnt know that the dog is actually his older son), Wilby has a series of misadventures, as he constantly switches back and forth between human form and dog form. Only Moochie and Professor Plumcutt know his true identity when he is a dog, as Wilby has spoken to them both in dog form. Finally, he goes to a local dance in his human form and while dancing transforms himself into a dog. He runs out quickly and goes back home.

The next day, Wilby, in his dog form, and Moochie are talking when Francescas butler Stefano comes out and drags Wilby into the house. Stefano and Dr. Valasky (Francescas adoptive father), discuss plans to steal a government secret, and Wilby, still in his dog form, overhears. Unfortunately for him, he transforms into human Wilby right in front of the spies and is discovered, but not before he hears Dr. Valasky expressing his wish to get rid of his own daughter.

The spies capture him and force Francesca to leave with them, leaving Wilby, as a human again, bound and gagged in the closet at once. Moochie sneaks into the house after Dr. Valasky, Stefano and an unwilling Francesca leave, and discovers Wilby, who is transformed into a dog, bound in the closet. Wilby reveals the secret to his dumbfounded father, who goes to the authorities, but suddenly finds himself accused of being either crazy or a spy himself.
 harbor patrol to apprehend Dr. Valasky and stop his boat. Wilby, in his dog form, swims up and wrestles with the men, as Francesca gets knocked out of the boat. He then saves her life and drags her ashore, which finally breaks the curse. When Francesca regains consciousness, Buzz tries to take credit for saving her.  This angers Wilby, who is still a dog, and he attacks Buzz. Seconds later, Buzz is surprised to find himself wrestling with the human Wilby, and the real Chiffon reappears. Since he is soaking wet, Francesca concludes that he saved her from the ocean. She hugs and praises Chiffon, while Buzz and Wilby look on helplessly.

Now Mr. Daniels and Chiffon are declared heroes, and Francesca leaves for Paris without her evil adoptive father and former butler, both of whom have been arrested for illegal espionage; and she gives Chiffon to the Daniels family for them to keep as her way of thanking them. Since Mr. Daniels has gotten such commendation for foiling a spy ring because of "his love of dogs", he has a change of heart over his dislike of dogs (while also realizing that having a dog-hating attitude isnt really good at all), and allows Moochie to care for Chiffon as he wanted a dog all along. Wilby and Buzz decide to forget their rivalry over Francesca and resume their friendship instead.

==Production notes== AIP released their horror film, I Was a Teenage Werewolf, one of the studios biggest hits. Arkoff, pp. 61–75  The Shaggy Dog betrays its successful forebear with Fred MacMurrays classic bit of dialogue: "Don’t be ridiculous — my son isn’t any werewolf! He’s just a big, baggy, stupid-looking, shaggy dog!" Arkoff, pp. 61–75 
 Charles Barton, who also directed Spin and Marty for The Mickey Mouse Club. Veteran screenwriter Lillie Hayward also worked on the Spin and Marty serials, which featured several of the same young actors as The Shaggy Dog.

Veteran Disney voice actor Paul Frees had a rare on-screen appearance in the film &ndash; for which he received no on-screen credit &ndash; as Dr. J.W. Galvin, a psychiatrist who examines Wilbys father (MacMurray). Frees also did his usual voice acting by also playing the part of the narrator who informs the audience that Wilson Daniels is a "man noted for the fact he hates dogs."

==Cast==
*Tommy Kirk as Wilby Daniels
*Fred MacMurray as Wilson Daniels
*Jean Hagen as Frida Daniels
*Annette Funicello as Allison DAllessio
*Tim Considine as Buzz Miller
*Kevin Corcoran as Montgomery Moochie Daniels
*Cecil Kellaway as Professor Plumcutt
*Alexander Scourby as Dr. Mikhail Valasky
*Roberta Shore as Francesca Andrassy
*James Westerfield as Officer Hanson
*Strother Martin as Thurm
*Forrest Lewis as Officer Kelly
*Jack Albertson as reporter (uncredited)
*Ned Wever as Security Agent E.P. Hackett Gordon Jones as Police Captain Scanlon
*Jacques Aubuchon as Stefano
*Paul Frees as Opening narrator/Dr. J.W. Galvin, the psychiatrist

==Reception==
The Shaggy Dog was one of the top movies of 1959. 

==Novelization== Scholastic eight years later in 1967 made some interesting changes to the plot. First, Funicellos character Allison was removed entirely, and her name is not listed among the movies principal performers.  As a result, the rivalry between Wilby and Buzz is greatly reduced.  Also, Dr. Valasky is changed into Franceskas uncle, not her adoptive father.

==Legacy== hiatus who billing (or a large fee) from another major studio was one way these comedies were produced inexpensively; they also tended to use the same sets from the Disney backlot repeatedly.  This allowed Walt Disney Productions a low-risk scenario for production, any of these films could easily make back their investment just from moderate matinee attendance in neighborhood theatres, and they could also be packaged on the successful Disney anthology television series The Wonderful World of Disney (some of these films were expressly structured for this purpose).

Occasionally Walt Disney Productions would find one of these inexpensive comedies would become a runaway success and place at or near the top of the box office for their respective release year (The Absent-Minded Professor, The Love Bug).  The initial release of The Shaggy Dog grossed more than $9 million on a budget of less than $1 million – an almost unprecedented return on a film investment, making it more profitable than Ben-Hur (1959 film)|Ben-Hur, released the same year. The Shaggy Dog also performed very strongly on a 1967 re-release.

The popular television series My Three Sons (1959-1972) reunited MacMurray and Considine, and also features a pet shaggy sheepdog named "Tramp".

===Sequels===
*The film was followed in 1976 with a theatrical sequel, The Shaggy D.A., starring Dean Jones as a 45-year-old Wilby Daniels. 
*In 1987, a two-part television movie set somewhere in the 17 years between the events portrayed in The Shaggy Dog and The Shaggy D.A., entitled The Return of the Shaggy Dog, presented a post-Saturday Night Live Gary Kroeger as a 30-something Wilby Daniels.

===Remakes=== the first remake of the film was a television movie, with Disney regular Scott Weinger as a teenaged Wilbert Wilby Joseph Daniels, and Ed Begley, Jr. playing a part similar to the one originated by Fred MacMurray in 1959.
*In 2006, Disney released a   version. The colorized version however is not restored and suffers from age.  In the UK, however, the 1959 movie has only ever been made available on DVD in black and white.

== References ==
;Notes
 

;Bibliography
*  

==External links==
*   
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 