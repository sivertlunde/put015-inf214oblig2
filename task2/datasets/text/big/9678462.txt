Naalaiya Theerpu
{{Infobox film name           = Naalaiya Theerpu image          = Naalaiya Theerpu DVD Cover.jpg caption        = DVD Cover director       = S. A. Chandrasekhar producer       = Shoba Chandrasekhar screenplay     = S. A. Chandrasekhar story          = Shoba Chandrasekhar starring       =   music  Manimekalai
|cinematography = R. P. Imayavaramban editing        = Gautham Raju studio         = V. V. Creations distributor    =  released       =   runtime        = 160 mins country        = India language       = Tamil box office     = 105 Crores
}}
 Tamil film Vijay in the lead role alongside Keerthana and Easwari Rao. The film which also featured Srividya, Radharavi and Sarath Babu, had music composed by newcomer Manimekalai, cinematography by R. P. Imayavaramban and editing by Gautham Raju. The film released to mixed reviews on 4 December 1992.  

==Cast==
*Vijay (actor) as Vijay Keerthana
*Srividya as Mahalakshmi, Vijays mother
*Radha Ravi as Arun Mehta
*Vinu Chakravarthy as Sundaramoorthy
*Sarath Babu as Priyas brother
*Dhamu as Dhamu, Vijays friend Srinath as Vijays friend Mansoor Ali Khan as an Inspector
*K. R. Vijaya as a Judge
*S. S. Chandran as a MLA
*Gowtham Sundararajan as Rocky, Arun Mehtas son
*Easwari Rao as Rani, Sundaramoorthys daughter
*Jaiganesh as Rajasekhar, College Principal Paandu as a Poramboku
*S. N. Surendar

==Production==
Vijay made his debut as a leading actor with the film, aged eighteen.  The film featured lyrics written by Pulamaipithan, P. R. C. Balu and music director Bharani (music director)|Bharani. 

==Music==
The music was composed by M. M. Srilekha, under the name of Manimegalai at the age of 12. 

* Aayiram Erimalai - S. P. Balasubrahmanyam
* Ammadi Rani - S. N. Surendar, Minmini
* Maapillai Naan - S. N. Surendar, Minmini, Manimekalai
* MTV Parthuputta - Sangeetha
* Udalum Indha (Sad) - S. P. Balasubrahmanyam
* Udalum Indha - S. P. Balasubrahmanyam, K. S. Chitra
* Vaadai Kulirkaathu - K. S. Chitra

==References==
 

==External links==
*  

 

 
 
 
 


 