La Vie miraculeuse de Thérèse Martin
{{infobox film| name = La Vie miraculeuse de Thérèse Martin
| director = Julien Duvivier
| producer = Marcel Vandal   Charles Delac
| starring = Simone Bourday   André Marnay   François Viguier   Janine Borelli
| runtime = 113 minutes
| country = France
| released = 8 November 1929
}}

La Vie miraculeuse de Thérèse Martin (The Miraculous Life of Thérèse Martin), is a French film, silent, directed by Julien Duvivier, and released in 1929. It is a  " stark and striking biographical account of the late 19th century Discalced Carmelite nun who died at age 24 from tuberculosis and was canonized in 1925." The film is based on the spiritual autobiography Thérèse wrote, LHistoire dune âme. The same material inspired  Alain Cavaliers film Thérèse (film)|Thérèse. "Simone Bourday has genuine adolescent fervour as Thérèse and André Marnay is pathetically fine as her father. The sequence of the taking of the veil has extraordinary documentary force."  Art direction on the film was by the future director Christian-Jaque.

The film follows Thérèse Martin as she moves from the close circle of her family home in   is about me. I feel him near me. He torments me and holds me with a grip of iron to deprive me of all consolation, trying by increasing my sufferings to make me despair.  Oh, how necessary it is to pray for the dying. If you only knew!  How needful is that prayer we use at Compline: "Free us from the phantoms of the night!"  

==Cast==
*  Simone Bourday  as Thérèse of Lisieux
*  André Marnay as Louis Martin, her father
*  Janine Borelli
*  François Viguier
*  Suzanne Christy
*  Jane Dolys
*  Lionel Salem

==References==
 

==External links==
 

 

 
 
 
 

 