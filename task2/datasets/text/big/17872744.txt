It Started with Eve
{{Infobox film
| name           = It Started with Eve
| image          = It-started-with-eve-1941.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = Henry Koster
| producer       = Joe Pasternak
| screenplay     = {{Plainlist|
* Norman Krasna
* Leo Townsend
}}
| story          = Hanns Kräly
| starring       = {{Plainlist|
* Deanna Durbin
* Robert Cummings
* Charles Laughton
}}
| music          = Hans J. Salter
| cinematography = Rudolph Maté
| editing        = Bernard W. Burton
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
It Started with Eve is a 1941 American musical romantic comedy film directed by Henry Koster and starring Deanna Durbin, Robert Cummings, and Charles Laughton.    Based on a story by Hanns Kräly, the film is about a man whose dying father wants to meet his sons new fiancée, but she is unavailable, so he substitutes a hatcheck girl. Complications arise when the father unexpectedly recovers. The film received an Academy Award nomination for Best Original Music Score (Charles Previn and Hans J. Salter).    The film is considered by some critics to be Durbins best film, and the last in which she worked with the producer (Joe Pasternak) and director (Henry Koster) that groomed her for stardom. It Started with Eve was remade in 1964 as Id Rather Be Rich.

==Cast==
* Deanna Durbin as Anne Terry
* Charles Laughton as Jonathan Reynolds
* Robert Cummings as Jonathan Johnny Reynolds Jr.
* Guy Kibbee as Bishop Maxwell
* Margaret Tallichet as Gloria Pennington
* Catherine Doucet as Mrs. Pennington
* Walter Catlett as Doctor Harvey Charles Coleman as Roberts
* Leonard Elliott as Reverend Henry Stebbins
* Irving Bacon as Raven 
* Gus Schilling as Raven 
* Wade Boteler as Harry, the Newspaper Editor 
* Dorothea Kent as Jackie Donovan 
* Clara Blandick as Nurse   

==Production==
The film was originally known as Almost an Angel. 

==Reception==
In his review in The New York Times, Bosley Crowther called the film "light and unpretentious fare" and "should please—as they say—both young and old. Its the perfect 8-to-80 picture."    Crowther singled out the performances of Charles Laughton, who plays cupid, and Deanna Durbin. Crowther wrote: 
  
Regarding Durbins performance, Crowther wrote, "Miss Durbin is as refreshing and pretty as she has ever been and sings three assorted songs—including a Tchaikovsky waltz—with lively charm." 

==Awards and nominations==
* 1942 Academy Award nomination for Best Original Music Score (Charles Previn and Hans J. Salter). 

==References==
 

==External links==
*  
*  
*  
*   on Lux Radio Theater: November 20, 1944
*   on Stars in the Air: January 31, 1952

 
 

 
 
 
 
 
 
 
 
 
 