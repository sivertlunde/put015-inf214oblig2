The Sugar Babies
 Sugar Babies}}

The Sugar Babies (2007) is a feature-length documentary film about exploitation in the sugar plantations of the Dominican Republic. The film, narrated by Edwidge Danticat, suggests that the descendants of African slaves, brought over from Haiti, live and work in unfair conditions akin to "modern day slavery."

==Production==
The Sugar Babies was shot on location in the Dominican Republic, Haiti, England, and the United States.  The 99-minute film is originally in Spanish, French and Creole and sub-titled in English, and produced by The Hope, Courage and Justice Project of New Orleans, the Human Rights Foundation of New York, and the former Siren Studios of Miami.
 Thor Halvorssen, Constance Haqq is Co-Producer and Salvador Longoria and Tico Pujals are Associate Producers.  The film was edited by Jason Ocasio and scored by Bill Cruz.

==Controversy==
The film officially premiered at the Montreal International Haitian Film Festival, but preview screenings in Paris and Miami led to heated controversy.  

The Miami screening of the film, which included many members of the hispanic media of South Florida and from the Dominican Republic, was the subject of a cease and desist order one hour before the time of screening, as well as a bribery scandal when several radio producers came forward to state that Dominican diplomats had offered them bribes to disrupt the screening and give the film a bad review.  
The Paris screening of the film was also the subject of a sabotage attempt. 

The film made "Official Selection" at Unifems Through Womens Eyes Film Festival, the New Orleans International Human Rights Film Festival, and the Buffalo Niagara Film Festival. The film also made "Official Selection" at both the Miami International Film Festival and the Womens International Film Festival but was ostensibly withdrawn from both South Florida festivals due to pressure from the sugar industry.

The Sugar Babies won the Jury Prize for Best Documentary at the Delray Beach Film Festival, continues to be screened in film festivals and educational venues, and is on tour with Amnesty International in France.

==References==

 

==External links==

* 
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 