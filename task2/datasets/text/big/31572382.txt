Code of the Secret Service
{{Infobox film
| name           = Code of the Secret Service
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Noel M. Smith
| producer       = Bryan Foy Hal B. Wallis Jack Warner
| screenplay     = William H. Moran Lee Katz Dean Riesner
| starring       = Ronald Reagan Rosella Towne Eddie Foy, Jr. Moroni Olsen Edgar Edwards Jack Mower
| music          = Bernhard Kaun Max Steiner
| cinematography = Ted D. McCord
| blank1         = Art Direction
| data1          = Charles Novi Frederick Richards
| studio         = Warner Bros.
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
{{external media
| align  = right
| width  = 253px
| video1 =  
}} Murder in the Air (1940).
 law enforcement Attorney General) and Will H. Hays (creator of the Motion Picture Production Code, the movie industrys censorship guidelines), due to the studios part in producing early 1930s films glamorizing gangsters.   

The series also enabled Warner Bros. to create Reagans screen persona, with Reagan even showing up to the set of Code of the Secret Service and asking director Noel M. Smith, "When do I fight and whom?" 

==Plot== counterfeiting ring love interest, Elaine (Rosella Towne).   

==Cast==
The cast included:    
* Ronald Reagan as Lieutenant Brass Bancroft
* Rosella Towne as Elaine
* Eddie Foy, Jr. as Gabby Watters
* Moroni Olsen as Parker
* Edgar Edwards as Ross
* Jack Mower as Decker
* John Gallaudet as Dan Crockett Joseph King as Tom "Jim" Saxby
* Steve Darrell as Butch, a henchman
* Sol Gorss as Dutch, a henchman
* George Regas as Mexican police officer

==Reception==
Reagan called Code of the Secret Service "the worst picture I ever made"    and commented on it, "never has an   of such dimensions been laid."  Producer Bryan Foy attempted to shelve the film.  Warner Bros. refused to do so, but did agree to not release it in Los Angeles.  Commenting on the film, a ticket taker at a movie theater in another city reportedly told Reagan, "You should be ashamed." 

In a 1939 review, the Calgary Herald called the movie "quite far-fetched in places and not very interesting as a whole." 

==Ronald Reagan assassination attempt== President Ronald 1981 assassination attempt.]] 1981 assassination attempt.  The President was Ronald Reagan, the star of Code of the Secret Service.       

==See also==
* Ronald Reagan filmography

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 