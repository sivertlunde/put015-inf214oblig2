Chloe, Love Is Calling You
{{Infobox film
| name           = Chloe, Love Is Calling You
| image          = 
| image_size     = 
| caption        = 
| director       = Marshall Neilan
| producer       = J.D. Trop (producer)
| writer         = Marshall Neilan (story and screenplay)
| narrator       = 
| starring       = See below
| music          = George Henninger
| cinematography = Mack Stengler
| editing        = Joseph Josephson Helene Turner
| studio         = 
| distributor    = 
| released       = 1934
| runtime        = 62 minutes 54 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Chloe, Love Is Calling You is a 1934 American film directed by Marshall Neilan.

The film is also known as Chloe (American short title).

== Plot summary ==
A low-budget Southern drama about a light skinned woman who was raised in the swamps. This was actress Olive Bordens last film.
 

== Cast ==
*Olive Borden as Chloe (Betty Ann Gordon)
*Reed Howes as Wade Carson
*Molly ODay as Joyce, the Colonels niece
*Philip Ober as Jim Strong
*Georgette Harvey as Old Mandy
*Francis Joyner as Col. Gordon Augustus Smith as Mose, thieving worker
*Jess Cavin as Hill, thieving worker
*Richard Huey as Ben, the servant
*Shreveport Home Wreckers as Blues Band

== Soundtrack ==
*"Chloe" (Music by Neil Moret, lyrics by Gus Kahn)

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 

 