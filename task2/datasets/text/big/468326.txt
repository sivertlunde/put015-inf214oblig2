21 Grams
 
{{Infobox film
| name = 21 Grams
| image = 21 grams movie.jpg
| alt =
| caption = Theatrical release poster
| director = Alejandro González Iñárritu
| producer = Alejandro González Iñárritu Robert Salerno
| writer = Guillermo Arriaga
| starring = Sean Penn Naomi Watts Charlotte Gainsbourg Benicio del Toro
| music = Gustavo Santaolalla
| cinematography = Rodrigo Prieto
| editing = Stephen Mirrione This is that
| distributor = Focus Features
| released =  
| runtime = 124 minutes  
| country = United States
| language = English
| budget = $20 million  . Box Office Mojo. IMDb. Retrieved December 17, 2010. 
| gross = $60.4 million 
}}
21 Grams is a 2003 American drama film directed by Mexican director Alejandro González Iñárritu from a screenplay written by Guillermo Arriaga. It stars Sean Penn, Naomi Watts, Charlotte Gainsbourg, Danny Huston and Benicio del Toro.

Like Arriagas and González Iñárritus previous film, Amores perros (2000), 21 Grams interweaves several plot lines, around the consequences of a tragic automobile accident. Penn plays a critically ill mathematician, Watts plays a grief-stricken mother, and del Toro plays a born-again Christian ex-convict whose faith is sorely tested in the aftermath of the accident.

As the second part of Trilogy of Death,  21 Grams is presented in a nonlinear arrangement where the lives of the characters are depicted before and after the accident. The three main characters each have "past," "present," and "future" story threads, which are shown as non-linear fragments that punctuate elements of the overall story, all imminently coming toward each other and coalescing as the story progresses.

== Title == immortal human soul by recording a loss of body weight (representing the departure of the soul) immediately following death. The research by MacDougall attempted to follow the scientific method and showed some variance in results ("three-fourths of an ounce", which has since been popularized as "21 Gram|grams" is the reported weight loss from the death of the first subject). MacDougalls results were published in the peer reviewed journal "American Medicine". 

== Plot ==
The story is told in a non-linear manner. The following is a linear, chronological summary of the plot:

Jack Jordan is a former convict who is using his new-found religious faith to recover from drug addiction and alcoholism. Paul Rivers is a mathematics professor married with a dedicated wife, Mary Rivers and with a fatal heart condition. Unless he receives a new heart from an organ donor, he will not live longer than one month. Pauls wife wants him to donate his sperm so she can have his baby even if he dies. Cristina Peck is also a recovering drug addict and now lives a normal suburban life with a supportive husband and two children. She is a loving mother and active swimmer who has left her days of drugs and booze behind. These three separate stories/characters become tied together one evening when Jack kills Cristinas husband and children in a hit-and-run accident. Her husbands heart is donated to Paul, who begins his recovery.

Cristina is devastated by the loss and returns to drugs and alcohol. Paul is eager to begin normal life again, but he hesitantly agrees to his wifes idea of surgery and artificial insemination as a last-ditch effort to get pregnant. During consultations with a doctor before the surgery, Paul learns that his wife had undergone an abortion after they had separated in the past. Angered, Paul ends the relationship. He becomes very inquisitive about whose heart he has. He learns from a private detective that the heart belonged to Cristinas husband and begins to follow the widowed Cristina around town.

Jack is stricken with guilt following the accident and starts using drugs again. Despite his wifes protests to keep quiet and conceal his guilt, Jack tells her that his "duty is to God" and turns himself in. While incarcerated, he claims that God had betrayed him, loses his will to live and tries unsuccessfully to commit suicide. He is released after Cristina declines to press charges, as she realizes that putting Jack in prison will not bring her family back. When Jack is released, he is unable to reincorporate himself into normal family life, and instead leaves home to live as a transient, working in manual labor.
 rejecting the transplant and his outlook is grim. As Cristina begins to dwell more on her changed life and the death of her girls, she becomes obsessed with exacting revenge on Jack. She goads Paul into agreeing to murder him.

Paul meets with the private detective who originally found Cristina for him. He tells Paul that Jack is living in a motel and sells Paul a gun. Paul and Cristina check into the motel where Jack is staying. When Jack is walking alone, Paul grabs him and leads him out into a clearing at gunpoint with the intention of killing him. However, Paul is unable to kill Jack, who himself is confused, shaking and pleading during the event. Paul tells Jack to "just disappear," then returns to the motel, lying to Cristina about Jacks death. Later that night, while they are sleeping, Paul and Cristina are awakened by a noise outside their door. Its Jack, who, still consumed by guilt, orders Paul to kill him and end his misery. There is a struggle, and Cristina blind-sides Jack and begins to beat him with a wooden lamp. Paul has a heart attack and shoots himself both to avoid dying from asphyxia and to prevent Cristina from killing Jack.

Jack and Cristina rush Paul to the hospital. Jack tells the police that he was the one who shot Paul, but is released when his story cannot be confirmed. Paul dies, and the conflict between Cristina and Jack remains unresolved (they meet in the waiting room after Pauls death; if they converse, it is not shown). Cristina learns in the hospital that she is pregnant. After Pauls death, Cristina is seen tentatively preparing for the new child in one of her daughters bedrooms, which she had previously been unable to enter after her daughters death. Jack is shown returning to his family.

== Cast ==
 
* Sean Penn as Paul Rivers
* Naomi Watts as Cristina Peck
* Charlotte Gainsbourg as Mary Rivers
* Benicio del Toro as Jack Jordan
* Danny Huston as Michael
* Mike Wallace as Bar Doorman/Bouncer
* John Rubinstein as Gynecologist
* Clea DuVall as Claudia
* Eddie Marsan as Reverend John
* Melissa Leo as Marianne Jordan
* Marc Thomas Musso as Freddy
* Paul Calderón as Brown
* Denis OHare as Dr. Rothberg
* Kevin Chapman as Alan
* Lew Temple as County Sheriff
* Carly Nahon as Cathy
 

== Reception ==
=== Critical response ===
 
The film was received with much acclaim. 80% of all critics gave the film positive reviews per   also praised the acting and called the film "an extraordinarily satisfying vision" that "may well be the crowning work of this year." 

=== Box office ===
The film was a success with audiences, garnering a worldwide gross of approximately $60 million after being made for an estimated $20 million. 

=== Accolades ===
{| class="wikitable"
|-
! Award !! Category !! Recipient !! Result
|-
| rowspan="2" | 76th Academy Awards Best Actress
| Naomi Watts
|  
|- Best Supporting Actor
| rowspan="2" | Benicio del Toro
|  
|-
| rowspan="5" | 57th British Academy Film Awards Best Actor in a Leading Role
|  
|-
| Sean Penn
|  
|- Best Actress in a Leading Role
| Naomi Watts
|  
|- Best Editing
| Stephen Mirrione
|  
|- Best Original Screenplay
| Guillermo Arriaga
|  
|-
| rowspan="2" | 9th Critics Choice Awards Best Actor
| Benicio del Toro
|  
|- Best Actress
| Naomi Watts
|  
|-
| rowspan="2" | Florida Film Critics Circle Awards 2003    Best Actor
| Sean Penn  
|  
|- Best Actress
| Naomi Watts
|  
|-
| Las Vegas Film Critics Society Awards 2003 
| Best Actor
| Sean Penn  
|  
|-
| Los Angeles Film Critics Association Awards 2003  Best Actress
| Naomi Watts
|  
|-
| National Board of Review Awards 2003  Best Actor
| Sean Penn  
|  
|-
| rowspan="3" | Online Film Critics Society Awards 2003  Best Actress
| Naomi Watts
|  
|- Best Director
| Alejandro González Iñárritu 
|  
|- Best Original Screenplay
| Guillermo Arriaga
|  
|-
| rowspan="6" | Phoenix Film Critics Society Awards 2003
| Best Actor
| Sean Penn
|  
|-
| Best Actress
| Naomi Watts
|  
|-
| Best Supporting Actor
| Benicio del Toro
|  
|-
| Best Cast
!
|  
|-
| Best Editing
| Stephen Mirrione
|  
|-
| Best Original Screenplay
| Guillermo Arriaga
|  
|-
| rowspan="4" | 8th Golden Satellite Awards Best Actor – Motion Picture Drama
| Sean Penn  
|  
|- Best Actress – Motion Picture Drama
| Naomi Watts
|  
|- Best Supporting Actor – Motion Picture
| Benicio del Toro
|  
|- Best Original Screenplay
| Guillermo Arriaga
|  
|-
| rowspan="2" | 10th Screen Actors Guild Awards Outstanding Performance by a Female Actor in a Leading Role
| Naomi Watts
|  
|- Outstanding Performance by a Male Actor in a Supporting Role
| Benicio del Toro
|  
|-
| Southeastern Film Critics Association Awards 2003  Best Actress
| Naomi Watts
|  
|-
| 60th Venice International Film Festival  Volpi Cup for Best Actor
| Sean Penn
|  
|-
| rowspan="3" | Washington D.C. Area Film Critics Association Awards 2003 Best Actress
| Naomi Watts
|  
|- Best Supporting Actor
| Benicio del Toro
|  
|- Best Original Screenplay
| Guillermo Arriaga
|  
|-
| World Soundtrack Awards 2003  Discovery of the Year
| Gustavo Santaolalla 
|  
|}

== See also ==
* Hyperlink cinema – the film style of using multiple inter-connected story lines.

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*   from American Cinematographer
*   at ABC Online

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 