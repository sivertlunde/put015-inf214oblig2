Uncivil War Birds
{{Infobox Film |
  | name           = Uncivil War Birds |
  | image          = UncivilWarBirdsTITLE.jpg|
  | caption        = |
  | director       = Jules White
  | writer         = Clyde Bruckman | Ted Lorch John Tyrrell Lew Davis|
  | cinematography = Philip Tannura | 
  | editing        = Charles Hochberg |
  | producer       = Jules White |
  | distributor    = Columbia Pictures |
  | released       =  |
  | runtime        = 17 16"
  | country        = United States
  | language       = English
}}
 1934 and 1959 in film|1959.

==Plot== Union Army, minstrel song-and-dance routine in blackface, with Curly playing a Mammy archetype|Mammy-type character and Larry strumming a banjo.

==Production notes==
filming took place on August 24-25, 1945.    It is a remake of the 1939 Buster Keaton short Mooching Through Georgia; the stock shot of the union lieutenant on horseback with his battalion of eight was borrowed from that film.  The song "Dixie (song)|Dixie" replaces the Stooges regular opening theme of "Three Blind Mice" for this film, and continues as background music for approximately twenty seconds into the opening scene. 
 John Tyrrell and Lew Davis. 

===Curlys illness===
The film was produced after Curly Howard suffered a mild stroke. As a result, his performance was marred by slurred speech, and slower timing, though Curly was more energetic and displayed better timing than in previous shorts. In addition, Moe Howard and Larry Fine are paired together and given the lions share of the films dialogue.   

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 