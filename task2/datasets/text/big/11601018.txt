Commune (film)
{{Infobox film
| name           = Commune
| caption        = 
| image          = Commune FilmPoster.jpeg
| director       = Jonathan Berman
| producer       = Jonathan Berman
| writer         = Jonathan Berman
| starring       = 
| music          = Elliott Sharp
| cinematography = Jonathan Berman Tamas Bojtor Alan Deutsch Rob VanAlkemade
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
}}
Commune is a 2005 documentary film by Jonathan Berman.  The film is about an intentional community located in Siskiyou County, California called Black Bear Ranch and features narration by Peter Coyote who himself once resided at Black Bear.

==Reception==
Commune was well received with a score of 95% fresh at Rotten Tomatoes.  Metacritic lists Commune with a score of 74 out of 100. 

Maitland McDonagh of TV Guide pronounced it "A close examination of a quintessential 60s phenomenon that speaks volumes about the attitudes and experiences that shaped the decade...captivating.  Andrew OHehir of Salon.com said "Amid the dozens of documentaries made about various aspects of 60s society and culture, "Commune" stands out for its ambiguity, honesty and sheer human clarity...an extraordinary collage." 

A New York Times review, titled "Just a Hardy Bunch of Settlers Who Left America and Moved to California", described the commune veterans: "However weatherbeaten they appear, they still have a light in their eyes, and they exude the hardy spirit of pioneers who are older and wiser but unbowed," adding that they look back with "pride, amusement, and sadness."  Another review, in the New York Sun, provides more specifics on a fundraising technique one former member called "emotional blackmail," claiming that the $22,000 initial land purchase was acquired by pitching rock musicians that they were "making money off our lifestyle and that it was time for them to give something back." 

The San Francisco Chronicle,  The Village Voice,  and Variety (magazine)|Variety  all gave the film positive reviews.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 


 