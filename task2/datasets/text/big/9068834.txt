Rokudenashi Blues
{{Infobox animanga/Header
| name            = Rokudenashi Blues
| image           =  
| caption         = Cover art from volume 16 of the Rokudenashi Blues manga
| ja_kanji        = ろくでなしBLUES
| ja_romaji       = 
| genre           = Sports, Action genre|Action, Drama
}}{{Infobox animanga/Print
| type            = manga
| author          = Masanori Morita
| publisher       = Shueisha
| demographic     = Shōnen manga|Shōnen
| imprint         = 
| magazine        = Weekly Shōnen Jump, Shueisha Original
| first           = 1988
| last            = 1997
| volumes         = 42
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = film
| director        = Takao Yoshisawa
| producer        = 
| writer          = 
| music           = 
| studio          = Toei Animation
| released        = July 11, 1992
| runtime         = 30 minutes
}}
{{Infobox animanga/Video
| type            = film
| title           = Rokudenashi Blues 1993
| director        = Hiroyuki Kakudō
| producer        = 
| writer          = 
| music           = 
| studio          = Toei Animation
| released        = July 24, 1993
| runtime         = 
}}
{{Infobox animanga/Video
| type            = live film
| director        = Hiroyuki Nasu
| producer        = 
| writer          = 
| music           = 
| studio          = TV Tokyo, Pony Canyon
| released        = February 24, 1996
| runtime         = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = Rokudenashi Blues 2
| director        = Muroga Atsushi
| producer        = 
| writer          = 
| music           = 
| studio          = TV Tokyo, Pony Canyon
| released        = 1998
| runtime         = 
}}
{{Infobox animanga/Video
| type            = drama
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          = Nippon Television
| network         = 
| first           = July 6, 2011
| last            = September 28, 2011
| episodes        = 
| episode_list    = 
}}
 

  is a boxing themed manga series authored by Masanori Morita that was serialized in the Weekly Shōnen Jump from 1988 to 1997. 

==Story==
The story stars  , delinquent student of  , who wishes to become the world boxing champion.  The manga follows Maeda (and his friends and rivals) as he struggles through three years of high school while becoming one of the strongest and best known high school fighters in all of Tokyo.  Not just a pure action manga, Rokudenashi Blues is filled with humor and well-crafted story arcs about honor, friendship, and the pressures of being a delinquent student in Japan.  Short gag-stories with chibi versions of characters are published as "Rokudenashi Buru-chu".

==Characters==
Rokudenashi Blues has a wide-arching cast of characters that live in and go to school around Tokyo that must deal with the pressures and struggles that come from being involved in the delinquent struggles of honour and strength.

===The 4 Heavenly Kings===
The 4 Heavenly Kings are what the students of Tokyo consider the strongest fighters in Tokyo. They each come from a separate district and often come into contact with each other throughout the later half of the manga, which often result in bloody, drawn out dog fights between the schools involved.

;  
: The main character of the story, hes a famous delinquent from Teiken High, in the Kichijoji area of Musashino, Tokyo, known for his passion of Boxing, street fighting, and his moped, Chavez. Albeit being a delinquent, being easily angered and stupid at times (He punched a professor when he heard the girls uniform changed), he has a sense of honor and respect, and values his friends greatly. Taison is also known as one of the Tokyos elite 4s, or "Heavenly Kings", and aims to become the world champion in boxing. Taison has an interest for Chiaki Nanase. His name is based on wrestler Akira Maeda and former world heavyweight champion Mike Tyson. He lives with his brother in the Honky Tonk apartment complex. Him and his older brother both left the south of Japan for Tokyo to avoid inheriting their fathers profession, that of a monk. He is a lightweight when it comes to drinking and always breaks down into tears.

; Onizuka Shibuya district of Tokyo. He controls the school he attends with an iron fist, forcing his classmates to pay protection fees for their safety. When Maeda and his friends protect one of Onizukas classmates from him, a bloody war ensues between the two of them, with Onizuka shoving Sakamura out of a second floor window, breaking Mutohs ribs and capturing the rest of Maedas friends. In their final confrontation Onizuka injures both of Maedas hands and stabs a screwdriver through his leg. In the end however, Maeda uses the trademark karate kick he learned from his father to knock Onizuka out. During middle school, him and Akagi were classmates until Onizuka broke most of his teeth with a chair during a fight and Akagi transferred schools. After his defeat at Maedas hands, Onizuka changes dramatically, respecting his friends and losing his cruelty. Keeping with the punk rock look of most of the characters, Onizuka is strongly modeled off of Billy Idol.

; Yakushiji
:Yakushiji is a seemingly carefree, hopeless romantic who has terrible luck in love. He lives in Asakusa. Highly superstitious, throughout the manga he can often be seen reading his horoscope and visiting shrines for luck. However, his laid-back demeanor hides his fighting strength and his martial prowess. He often lapses into melancholy and seems the smartest and wisest of The 4 Heavenly Kings. When he was younger, him and Chiaki used be best friends, and he often thinks about her. He has a Teddy boy hairstyle which is dyed brown. Maeda has an intense dislike towards Yakushiji due to his relationship with Chiaki.

;Kasai
:The most violent and perhaps the strongest of the 4 Heavenly Kings, Kasai is filled with an all-consuming need to prove himself and keep the respect of his classmates by beating up anybody that he considers strong. The school he goes to has the worst reputation in all of Tokyo and is considered a Yakuza recruitment ground. Having dominated everybody in Ikebukuro he decides to once and for all prove who is the strongest of the 4 Heavenly Kings by humiliating each and every one of them. His rage and his desire to be number one starts to overwhelm him and destroy his sanity and his friendships. Shinjo, a main character from Moritas other manga, Rookies (manga)|Rookies, closely resembles Kasai in both appearance and personality.

===Kichijoji===
Kichijoji is the area of Tokyo that Maeda and his friends are from. At the start of the manga most of the action that takes place involves the different schools from the area such as Yonekura Industrial before branching off into the other districts of Tokyo and even to the south of Japan.

===Teiken High===
; 
: An intelligent, beautiful and emotional girl thats the complete opposite of Taison, she cares deeply about him. Many of the characters fall in love with Chiaki during the manga, much to Maedas annoyance as he has severe jealousy issues. One of her best friends from her childhood was Yakushiji, who is now one of the "Heavenly Kings" of Tokyo. She is head of the sports committee in Teiken High.

;  
: One of Maedas best friends. Gets extremely angry whenever someone mentions his wide forehead. While the only one with a proper girlfriend in the series, they constantly fight and bicker, each accusing the other of infidelity. His father runs a motorbike repair shop and Maeda usually makes Katsuji repair his mopeds and loan him porn movies. Katsuji has been best friends with Yoneji since Preschool and, originally, his bully.

;  
: One of Maedas best friends. Gets extremely angry whenever his large nose is mentioned. Had a crush on Chiaki early on in the manga but never really did anything because he knew Taison liked her. Despite his appearance as a delinquent, he is excellent at school, getting full marks on most tests and is meant to be pretty good at art. Yoneji has a large scar on his head from when he was hit with a rock and keeps his hair thick to hide it. His girlfriend, Sayuri, lives in the south of Japan.

; Yutaro Hatanaka
: A very skilled boxer who attended Teiken High, and is attempting to secure a place in the Olympics. Him and Maeda come to blows, with Maeda knocking him out with a wrestling move. However, when people begin to question the authenticity of the win due to Maeda being two weight categories heavier than Hatanaka and him not fighting in a straight boxing match Maeda forces Hatanaka to fight him again. This time Hatanaka easily defeats the emaciated Maeda. He leaves Teiken to train at boxing at a prestigious college by the sea. He is very easy-going and cheerful, but can be moved to anger. He used to be one mistake away from being expelled from Teiken High. According to Hatanaka, Maeda has been the only one to knock him down yet in a boxing match and that no one at his college compares to him.

; Hamada
: One of the original members of the boxing club and the person that took over running it when Hatanaka left. Hamada managed to bring the boxing club back to life and return it to its former glory after Hatanaka left. An extremely honourable and noble person, Hamada eventually got himself expelled in an effort to protect Seiji, who was forced by a teacher to plant cigarettes on him. To ease Seijis guilt and stop him from admitting what he did to his friends, Hamada hit the teacher in question to make the whole point moot. He later finds work at a building site, and his friends are pleased that he found a job that he enjoys.

;  
:The leader of the Cheering Squad. He is recognized for his moustache, large frame and strong tolerance towards alcohol. At the start of the manga, the Cheering Squad and Boxing Squad were in the middle of a feud, but this was quickly sorted out by Taison and his friends. He is twenty years old, having to continuously repeat his final year.

; Hidekazu Ohashi
:Original member of the Boxing Club that was involved in the dispute between Hatanaka and Maeda. He is well known for his extremely strong punch and his bright blong, permed haircut. He is best friends with Mutoh and the two both had to repeat their final year together. While originally a mediocre and below average boxer, during his repeat year he started training and working out seriously, being the last member of the original boxing club left.

; Mutoh
:Member of the Cheering Squad. Mutou is recognizable from his large scar under his right eye. This was accidentally self-inflicted when Mutou rubbed away tears when his hand was bandaged with a blade. When he was younger, he had a large, comical afro. Even though they were bitter enemies at the start of the manga, Ohashi and Mutoh are best friends.

;Ebihara "Marcy" Masatoshi
:A first year at Teiken High and one of its most powerful fighters, Ebihara is skilled at most martial arts and at using improvised weapons. Originally an antagonist at Teiken when he first arrived, Ebihara was able to overpower Maeda the first time they fought. He is recognisable by his distinctive mod look and the bandana he uses to prop up his overflowing mass of hair. This bandana is used to hide the large, disfiguring scar Ebihara has left over from middle school. This has left him with a crippling fear of knives and can go into a blind panic when he sees one. His nickname "Marcy" comes from when he used be in the Masako motorbike gang.

;Oba Hiroto
:A first year student, he trusts Taison with his life.

;  
:A first year student and Oba Hirotos best friend.

;  
:Chiakis best friend and Katsujis girlfriend. She can be a bit ditzy at times.

;Mika
:A girl in Maedas class that used to have a serious crush on him. Ohashi used think she had a thing for him but was sadly proved wrong. She now goes out with Koheijis younger brother.

;Junichi Nakajima
:Junichi is one of Maedas classmates. An avid photographer, Nakajima is considered an annoyance by Maeda and his gang, especially whenever sports are involved. He is the head of and only member of Teikens photography club and usually must resort to selling risque photos of his female classmates to magazines in order to keep it running. Maeda, or someone else, almost always breaks his cameras by accident whenever he is around.

;Mafuyu
: A sukeban, Mafuyu used be part of the destructive bike gang Brown Sugar, until she fell in love with Takeshi, a Misako, and rival gang member. She almost killed herself on Valentines Day before Maeda stopped her. She eventually overcomes her despair and falls in love again.

;Masahiko Kondo
:A kind hearted and much loved teacher at Teiken High, Masa often despairs over Maeda and his friends failure at making grades. Maeda has great respect for Masa and it was under Masas guidance that Hatanaka got one last shot at getting a boxing scholarship at college. When he was younger he was on the Japanese olympic team for wrestling but never actually managed to compete in the olympics.

===Yonekura Industrial===
; Shimabukuro
:One of Maedas longer running adversaries. They met when Shimabukuro crashed his car into Maedas, inadvertently saving his, and his friends lives from an oncoming train. Mistaken identity later led the two to become enemies. A long-running joke in the manga is that Maeda and Shimabukuro always meet by helping the other out, only to fight when they realize who it is they are dealing with. He is a strong fighter, coming 2nd place in the Japanese High School Judo Finals. While he should be in his final year at Yonekura, he had to repeat 2nd year due to poor grades. Him and Maeda seem to be of equal strength, though Shimabukuro knocked Maeda out with a Judo slam during their only serious fight. The two later resolved the conflict in hospital, both suffering from chicken pox. While often at each others throats, the other characters often wonder whether or not theyre best friends.

; Yahiro
:Sakamatos underclassmen who is the complete opposite of him. Yahiro often fights with Sakamoto, especially over his rigid sense of honour. He is an extremely competent fighter, having fought and beaten multiple opponents, and having even beaten Sakamoto once in a fight. He is extremely disrespectful and proud, usually considering himself above everyone else, and even though he treats Sakamoto the same way, it is obvious the two are close friends.

===Misako Technical College===
; Tsuyoshi Mihara
:The boss of Misako College and of the gang of the same name, Mihara was put into prison before the start of the manga. When he returns he plans on getting revenge on Teiken because it was Mafuyu who got him put there and for when Yoneji came to the school and beat up Naoto. Maeda manages to defeat him by bashing his head in on the gravestone of Mafuyus boyfriend, who he had blinded in one eye and crashed his motorbike as a result.

; Naoto
:A pupil at Misako and a former classmate of Yoneji and Katsujis when they were still at middle school. Something of a ladies man, Naoto seduced Yonejis girlfriend at the time Sayuri, into giving him a kiss. Yoneji witnessed this and because of that hates Naoto and broke it off with his girlfriend. When Sayuri returns into his life, he gets revenge on Naoto and manages to defeat, even though he trains at karate.

; Takeshi
:A former pupil and gang member of Misako, he died in a motorcycle accident before the manga started. He and Mafuyu used to go out and it was this relationship which brought about Miharas wrath. He was blinded in one eye when Mihara punched him and was told by doctors not to drive, advice which he sadly didnt follow.

==Media==

===Manga===
The manga was originally serialized in the manga magazine Weekly Jump. The manga was also created into a series of Shueisha Original Kanzenban issues.

===Anime===

====Rokudenashi Blues==== Toei
*Released: 1992
*Cast
**Taison Maeda: Hideyuki Hori
**Chiaki Nanase: Yoshino Takamori
**Katsuji Yamashita: Masaya Onosaka
**Yoneji Sawamura: Ryō Horikawa
**Kazumi Imai: Miki Itō
**Koeji Nakata: Yūsaku Yara
**Sorimachi: Shigeru Chiba
*Director: Takao Yoshizawa
*Screenplay: Yoshiyuki Suga
*Music: Yasunori Iwasaki

====Rokudenashi Blues 1993====
*Distributor: Toei
*Released: 1993
*Cast
**Taison Maeda: Hiroaki Hirata
**Chiaki Nanase: Sanae Horikawa
**Koeji Nakata: Kazuki Yao
**Monson Maeda: Shōzō Iizuka
**Fujio Maeda: Yūsaku Yara
**Yōkō Maeda: Ryō Horikawa
**Haruka: Junko Hagimori
**Naoto Watanabe: Ryūsei Nakao
**Hironari Komiyama: Hiroyuki Satō
**Junichi Nakajima: Yasuhiro Takato
**Ioka: Tesshō Genda
**Hikaru Midorikawa, Tsutomu Kashiwakura, Hideo Ishikawa, Daisuke Gōri, Yuriko Yamamoto, Megumi Urawa, Naoki Tatsuta, Kenichi Ono, Kazunari Tanaka, Kyōsei Tsukui, Shinobu Satouchi, Hideo Tanise, Yūko Nagashima, Mariko Onodera, Wakana Yamazaki
*Director: Hiroyuki Kakudou
*Screenplay: Yoshiyuki Suga, Shunichi Yukimura
*Music: Kimio Nomura

===Live Action===

====Rokudenashi Blues====
*Distributor: Pal Entertainments Group (PEG)
*Production: Pony Canyon, TV Tokyo, PEG
*Released: 1996
*Cast
**Taison Maeda: Kensaku Maeda
**Chiaki Nanase: Maju Ozawa
**Seikichi Harada: Junichi Kawamoto
**Hiroshi Shimabukuro: Ryūshi Yanagisawa
**Mitsuru Kizuka: Giant Jun
**Kazumi Imai: Yukari Obata
**Katsuji Yamashita: Yoshinori Umemoto
**Yoneji Sawamura: Teruhide Kimura
**Koheji Nakata: Yuji Hirata
**Hiroyuki Otomo: Ken Inaba
*Director: Hiroyuki Nasu Midori Kogane, Yoshiyuki Suga

====Rokudenashi Blues II====
*Distributor: PEG
*Released: 1998
*Cast
**Taison Maeda: Kensaku Maeda
**Chiaki Nanase: Moe Yamaguchi
**Kazuya Yuki: Yūsuke Shinuchi
**Katsuji Yamashita: Shu Ehara
**Yoneji Sawamura: Masaki Nishimori
**Kazumi Imai: Rie Fukaumi
**Hiroto Oba: Osamu Takizawa
**Nakata Yoheji: Yuki Tanaka
**Hiroyuki Otomo: Yōji Sasaki
**Jose Sanchez (Colombian Mobster): Antonio Ploszay
**Hironari Komiyama: Tōru Kaneko
**Misaki-  Takashi Matsuyama
**Teacher: Kazuyoshi Ozawa
*Director/Screenplay: Atsushi Muroga
*Music: Gorō Yasukawa

===CD Book===
An audio drama CD released by Shueisha in 1991.

====Cast====
*Taison Maeda: Kōichi Yamadera
*Chiaki Nanase: Sakiko Tamagawa
*Katsuji Yamashita: Issei Futamata
*Yoneji Sawamura: Kenyū Horiuchi
*Kazumi Imai: Yūko Mizutani
*Yutaro Hatanaka: Bin Shimada
*Hamada: Nobuyuki Furuta
*Hidekazu Ohashi: Yoku Shioya
*Masutatsu Wajima: Yukitoshi Hori
*Shokei Muto: Ken Yamaguchi
*Masahiko Kondo: Daisuke Gōri
*Principal: Kōzō Shioya
*Girl: Yumi Tōma

===Video Games=== TOSE and published by Bandai, were released for the Family Computer (1993) and Super Famicom (1994).

Five characters of Rokudenashi Blues appear in the  , a roleplaying game released in 1991.

Some Rokudenashi Blues characters are also featured in Cult Jump, an adventure-trivia game released for the Game Boy.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 