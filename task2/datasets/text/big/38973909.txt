Home Run (film)
{{Infobox film
| name           = Home Run
| image          = Home Run poster.jpg
| alt            = 
| caption        =  David Boyd
| producer       = Carol Spann Mathews, Tom Newman
| writer         =  
| screenplay     = Brian Brightly, Candace Lee, Eric Newman, Melanie Wistar
| starring       = Scott Elrod   Dorian Brown   Charles Henry Wyson
| music          = Scott Allan Mathews
| cinematography = David Boyd
| editing        = Ken Conrad
| studio         = Samuel Goldwyn Films/Provident Films
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $1.2 million
| gross          = $2,861,020 
}}
 David Boyd and stars Scott Elrod, Dorian Brown, Vivica A. Fox. the film was released in theaters on April 19, 2013. 

==Plot==
Pro baseball player Cory Brand is forced into a rehabilitation program in his Oklahoma hometown after several alcohol-related incidents. He is responsible for injuring his brother in an alcohol-related crash. Cory reluctantly enters a Celebrate Recovery. He eventually finds new hope when he gets honest about his checkered past, and takes on coaching duties for a Little League team. Cory reunites with his high school girlfriend, starts a relationship with his son and rebuilds his relationship with his family.

==Cast==
 
* Scott Elrod as Cory
* Dorian Brown as Emma 
* Charles Henry Wyson as Tyler 
* Vivica A. Fox as Helene 
* James Devoti as Clay
* Nicole Leigh as Karen 
* Drew Waters as Pajersky
* Robert Peters as J.T.
 

==Release and reception==
The film had a limited release on April 19, 2013 in the United States and has grossed over $2,861,020. 
 Barry Lyons, Brett Butler, Jose Alvarez, among other sports figures. 

The film has received mixed-to-positive reviews from critics. While critics differed on the films message, most praised the quality of the filmmaking.

Sean OConnell of The Washington Post gave the film a positive, three stars out of four review, saying, "Boyd uses upbeat musical cues and sun-dappled cinematography to manifest an authentic small-town, minor-league atmosphere that’s warm and welcoming, even as it addresses potentially devastating personal problems. There are religious undertones to “Home Run” as Brand labors through his rehabilitation, but Boyd doesn’t succumb to the pressure of clubbing his audience over the head with a metaphorical Louisville Slugger. The director trusts his cast to convey the message. They rise to the occasion. ... Those seeking riveting baseball sequences might leave frustrated...The strongest scenes take place in dingy hotel rooms, on a deserted farm or in the rehab sessions where Brand and his fellow addicts open their hearts in search of forgiveness. It’s during these moments that Home Run swings for the fences, and often connects." 

Betsy Sharkey of the Los Angeles Times was less positive, saying, "Almost from the beginning the message overwhelms the medium," and that the message was "overplayed". She said the portrayal of Corys alcoholism "unfolds in such fits and starts that we rarely feel Corys pain." However, she did praise Elrods acting. 

Tom Long of The Detroit News wrote, "Considering its a movie with an avowed mission; considering that mission has to do with addiction and spiritual righteousness; And considering that the story involves a major league baseball player coaching a Little League team made up of spunky kids... Home Run is actually pretty OK." He continued, "in sheer moviemaking terms, this is one well-made film. The acting is surprisingly solid throughout, and director David Boyd has an eye for crisp, lovely compositions. This movie may be preaching to the choir, but at least the preacher has good taste. ... Boyd weaves the preaching — which includes testimonials from actual recovering addicts — into the story, and thankfully avoids overly emotional hallelujah moments. For what it is, Home Run is a solid hit." 

==References==
{{Reflist|refs=
 {{cite web url        =http://www.boxoffice.com/statistics/bo_numbers/actual_estimate/2013-04-29 title      =Weekend Actuals (Domestic), Fri, Apr. 26 - Sun, Apr. 28 work       =  type       =Within boxoffice.com select the hyperlink Weekend Actuals (Domestic), located on right
 |publisher=Boxoffice
 |accessdate=29 Apr 2013
 |postscript=
}} 
}}

==External links==
 
*  
*  
*  

 
 
 
 
 