Guerrero 12
{{multiple issues|
 
 
 
}}

{{Infobox film
| name           = Guerrero 12
| image          = 
| caption        = 
| director       = Miguel A. Reina Carlos Hernández Alejandra Domínguez
| writer         = Miguel A. Reina Luis García  Jaime Guerrero   León Krauze Andrés Roemer   Juan Villoro
| music          =
| cinematography = Xavier Xequé Jesús Nagore
| editing        = 
| distributor    = 
| released       =
| runtime        = 70 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}}
Guerrero 12, also known as 12th Warrior, is a Mexican film documentary that plans to release in theaters in the winter of 2011 in Mexico. The film is directed by Mexican director Miguel A. Reina and produced by E Corp Studio, and was shot on three continents in high definition digital cinema.

==Plot==
Beyond the fun and sporty, it is clear that football has become part of the idiosyncrasies of people. In Mexico it is a religion, it is a phenomenon that promotes an inexplicable passion.

== Cast ==
* Álvaro "El Mago" Romero, The Mexican soccer fan, originally from Morelia, Michoacán, where a museum dedicated to him. Mario "Pichojos" Pérez, Currently the coach for only second division team in the state of Guerrero. Arturo Brizio, former football (soccer) referee from Mexico.
* Mauricio Cabrera, Analyst and commentator on Medio Tiempo, the most important sports blog in Latin America. Luis García, Former football player, national team.
* Jaime Guerrero, Journalist and reporter of presidential source.
* León Krauze, Host of W Radio and a member of the committee of the cultural magazine Letras Libres.
* Andrés Roemer, TV host, political scientist and philanthropist.
* Juan Villoro, Mexican writer and journalist.

==Soundtrack listing==
{{Track listing
| extra_column     = Artist(s)
| writing_credits  = yes
| title1           = Antonella
| writer1          = The Odyssey 
| extra1           = Sons of Androids
| length1          = 4:08
| title2           = Nada que perder
| writer2          = Luis Abraham Torres Reyes
| extra2           = Nana Pancha
| length2          = 3:37
| title3           = No hay culpables
| writer3          = Eduardo Echeverría, Milton Henestrosa, Dario Arias
| extra3           = Llanto
| length3          = 5:12
| title4           = Guerreros
| writer4          = Eduardo Vázquez Navarrete, Julián Orta Monroy
| extra4           = Acustick-0
| length4          = 2:40
| title5           = Desatino del cuervo
| writer5          = Eduardo Echeverría, Milton Henestrosa, Dario Arias
| length5          = 6:33
| extra5           = Llanto
| title6           = Solo una vez
| writer6          = Sergio Solórzano, Abraham González, Aarón Vázquez
| extra6           = Amya / Jorge Colín
| length6          = 5:19
| title7           = Décima circular
| writer7          = Joaquín García, Jorge León, Andrés Vogel
| extra7           = Perros Celestes
| length7          = 3:52
| title8           = Ser del cielo
| writer8          = Rich Arnauda
| extra8           = His-Panic & Los SilverHeads
| length8          = 5:28
| title9           = Guerrero Ancestral
| writer9          = José Antonio Torres Marin, Gerardo Torres, Rich Arnauda
| extra9           = Jatoma
| length9          = 3:42
| title10          = Sepia
| writer10         = Alejandro Preisser
| extra10          = Triciclo Circus Band
| length10         = 2:27
| title11          = Orquestal
| writer11         = Rich Arnauda
| extra11          = His-Panic & Los SilverHeads
| length11         = 3:41
| title12          = Pasión
| writer12         = Rich Arnauda
| extra12          = His-Panic & Los SilverHeads
| length12         = 3:06
| title13          = El Mago
| writer13         = Eduardo Vázquez Navarrete
| extra13          = Acustick-0
| length13         = 3:30
| title14          = Lloren mi tango
| writer14         = Alejandro Preisser
| extra14          = Triciclo Circus Band
| length14         = 4:10
| title15          = Puedes decir
| writer15         = Luis Álvarez
| extra15          = El Haragán y Compañía
| length15         = 3:08
}}

== External links ==
*  
*  

 
 
 
 
 
 
 


 
 