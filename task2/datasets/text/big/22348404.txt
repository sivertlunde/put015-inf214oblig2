The Cell 2
{{Infobox film
| name           = The Cell 2
| image          = The Cell 2.png
| caption        = DVD cover
| director       = Tim Iacofano
| producer       = Alex Barder Lawrence Silverstein
| writer         = Alex Barder Rob Rinow Lawrence Silverstein
| starring       = Tessie Santiago Chris Bruno Frank Whaley
| music          = John Massari
| cinematography = Geno Salvatori
| editing        = John Coniglio
| studio         = New Line Cinema
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Cell 2 is a 2009 direct-to-video sequel from the 2000 film The Cell. 

==Plot==
A serial killer calling himself "The Cusp" murders his victims and then revives them, until they beg to die. His first victim, psychic investigator Maya Casteneda (Tessie Santiago), survives and is bent on revenge. After she is tapped by the FBI, Maya realizes the only way to locate The Cusp is by entering his mind. But if she dies there, she will also die in real life.

==Release==
The Cell 2 was released on DVD in the United States on June 16, 2009. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 