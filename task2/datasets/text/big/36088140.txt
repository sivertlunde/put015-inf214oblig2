Ranadheera
{{Infobox film
| name = Ranadheera
| image =
| caption =
| director = V. Ravichandran
| producer = N. Veeraswamy
| writer = Subhash Ghai
| screenplay = V. Ravichandran
| based on =  
| narrator =
| starring = V. Ravichandran Kushboo Anant Nag
| music = Hamsalekha
| cinematography = R. Madhusudan
| editing = K. Balu
| studio = Eshwari Studios
| released =  
| runtime = 146 minutes
| country = India Kannada
| budget =
| gross =
}}
 musical action film starring V. Ravichandran and Kushboo in the lead roles. Directed by Ravichandran himself, the film was produced by his father N. Veeraswamy under Eshwari Productions. Kushboo gained popularity in South India through this film. The music was composed by Hamsalekha.
 Hindi film Hero (1983 film)|Hero directed by Subhash Ghai. 

==Cast==
* V. Ravichandran
* Kushboo
* Anant Nag
* Sudheer
* Lokesh
* Jayachitra
* Master Manjunath
* Umashree
* Jaggesh
* Doddanna

==Soundtrack==
{{Infobox album
| Name        = Ranadheera
| Type        = Soundtrack
| Artist      = Hamsalekha
| Cover       = Kannada film Ranadheera album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 1987
| Recorded    =  Feature film soundtrack
| Length      = 41:35
| Label       = Lahari Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

Hamsalekha composed music for the soundtracks and also wrote their lyrics. The album consists of ten soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Artist(s)
| total_length = 41:35
| lyrics_credits = yes
| title1 = Baa Baaro Baaro Ranadheera
| lyrics1 = Hamsalekha
| extra1 = S. P. Balasubrahmanyam, S. Janaki
| length1 = 1:32
| title2 = Preethi Maadabaaradu
| lyrics2 = Hamsalekha
| extra2 = S. P. Balasubrahmanyam, S. Janaki
| length2 = 6:15
| title3 = Ondaanondu Kaaladalli
| lyrics3 = Hamsalekha
| extra3 = P. Susheela, Ramesh
| length3 = 4:36
| title4 = Meenakshi Ninna Kanna Mele
| lyrics4 = Hamsalekha
| extra4 = S. P. Balasubrahmanyam
| length4 = 3:36
| title5 = Yaare Neenu Sundara Cheluve
| lyrics5 = Hamsalekha
| extra5 = S. P. Balasubrahmanyam, S. Janaki
| length5 = 6:12
| title6 = En Hudgiro Adyaakink Aadtiro
| lyrics6 = Hamsalekha
| extra6 = S. P. Balasubrahmanyam, S. Janaki, Master Manjunath
| length6 = 4:47
| title7 = Naavindu Haado Haadige
| lyrics7 = Hamsalekha
| extra7 = S. P. Balasubrahmanyam, S. Janaki
| length7 = 4:52
| title8 = Flute (Music Bit)
| lyrics8 = –
| extra8 = Hamsalekha
| length8 = 1:26
| title9 = Baa Baaro (Bit)
| lyrics9 = –
| extra9 = Hamsalekha
| length9 = 7:04
| title10 = Baaramma (Bit)
| lyrics10 = –
| extra10 = Hamsalekha
| length10 = 1:15
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 