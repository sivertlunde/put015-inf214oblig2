Woman Haters
 
 
{{Infobox Film |
  | name           = Woman Haters 
  | image          = Womanhatersposter34.jpg |
  | caption        =  The Stooges were not known professionally as "The Three Stooges" when the film was released as they were billed by their individual names |
  | director       = Archie Gottler |
  | writer         = Jerome S. Gottler | Jerry Howard Marjorie White Bud Jamison Walter Brennan Monte Collins A.R. Haysel Fred Toones Jack Norton June Gittelson |
  | producer       = Jules White |
  | cinematography = Joseph August | James Sweeney |
  | music          = Louis Silvers
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 19 18" |
  | country        = United States |
  | language       = English |  
}}
Woman Haters is the first short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Misogynists Tom (Moe) and Jack (Curly) talk him out of it. However during the party, Marys intimidating father threatens Jim to marry his attractive daughter by telling him a story about his other, unattractive daughter having a fiance who tried to abandon her on their wedding day. He and his brothers had roughed him up for it but also forced him to go through with the ceremony. Jim is convinced to go through the ceremony much to the mans dismay. Later, on a train ride, the confrontation escalates between the Stooges and Mary.

Mary uses her feminine charm to woo both Jack and Tom in an attempt to make Jim jealous. She sings a theme ("for you, for you my life my love my all") with each of the stooges in turn, as she flirts with them. Each is attracted to her charms as she proves the oath they swore as Women Haters was fraudulent(though Jack attempts to resist her). Finally, Mary tells Tom and Jack the truth, that she and Jim are married, and pushes her way into bed with the trio, knocking Tom and Jack out the train window in the process. The film closes as the Stooges, now old men, finally reunite at the now almost empty Woman Haters club house when Jim enters and decalres he wants to rejoin. What happened to Mary is not revealed.

==Production notes==
Woman Haters was the sixth entry in Columbias "Musical Novelty" series, with all dialogue delivered in rhyme. Jazz Age-style music plays throughout the entire short, with the rhymes spoken in rhythm with the music. Being the sixth in a “Musical Novelties” short subject series, the movie appropriated its musical score from the first five films. The memorable song “My Life, My Love, My All,” featured in this short, was originally “At Last!” from the film Um-Pa.

Curly Howard was billed under his pre-Stooge name "Jerry Howard" in this short.

The Stooges had different names in this short: Curly is "Jackie", Moe is "Tom" and Larry is "Jim". This also marked one of the few Stooge shorts that features Larry as the lead character. Others include Three Loan Wolves and He Cooked His Goose.
 ) in Woman Haters, their debut film made at Columbia Picture]]

Bud Jamisons character delivers the first "eye pokes" to the Stooges, as part of the initiation into the Woman Haters Club. He pokes Larry in the eyes first, followed by Curly. Finally, he delivers an eye poke to Moe, who mistakenly blames Curly and promptly slaps him, igniting the first real Stooge brawl of the short films.

This short includes a young Walter Brennan playing the train conductor being initiated into the Woman Haters Club by Moe and Curly.

In contrast to later Stooge films, Larry and Curly are much more willful and defiant to Moe, even giving him some slapstick vengeance of their own, rather than being mere subordinates.

Curly delivers his first "woo-woo-woo-woo!" and "Nyuk, nyuk, nyuk" in this short, although the latter is not quite delivered in the eventual "classic" style.

Curly spends most of this short wearing pants that are split in the back.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 