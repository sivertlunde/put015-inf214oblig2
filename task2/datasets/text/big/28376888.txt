Mr. Denning Drives North
{{Infobox film
| name           = Mr. Denning Drives North
| image          = "Mr_Denning_Drives_North"_(1952).jpg
| image_size     = 
| caption        = UK theatrical poster
| director       = Anthony Kimmins
| producer       = Anthony Kimmins   Stephen Mitchell
| writer         = Alec Coppel
| based on       = novel by Alec Coppel
| narrator       = 
| starring       = John Mills Phyllis Calvert Herbert Lom   Eileen Moore
| music          = Benjamin Frankel
| cinematography = John Wilcox
| editing        = Gerald Turney-Smith
| studio         = London Film Productions
| distributor    = British Lion Films
| released       = 21 January 1952
| runtime        = 93 minutes
| country        = England
| language       = English
| budget         =
| gross = £70,197 (UK) 
| preceded_by    = 
| followed_by    = 
}} British mystery film directed by Anthony Kimmins and starring John Mills, Phyllis Calvert and Eileen Moore.  The plot concerns an aircraft manufacturer (Mills) who accidentally kills the boyfriend (Herbert Lom) of his daughter (Moore) and tries to dispose of the body. Alec Coppel wrote the script, adapted from his own novel. It was made at Shepperton Studios.

==cast==
* John Mills as Tom Denning
* Phyllis Calvert as Kay Denning
* Eileen Moore as Liz Denning
* Sam Wanamaker as Chick Eddowes
* Herbert Lom as Mados
* Raymond Huntley as Wright
* Russell Waters as Harry Stoper
* Wilfrid Hyde-White as Woods
* Freda Jackson as Ma Smith
* Trader Faulkner as Ted Smith
* Sheila Shand Gibbs as Matilda
* Bernard Lee as Inspector Dodds
* Michael Shepley as Chairman of Court Ronald Adam as Coroner John Stuart as Wilson
* Hugh Morton as Inspector Snell David Davies as Chauffeur
* Ambrosine Phillpotts as Miss Blade

==Original novel==
{{Infobox book|  
| name          = Mr Denning Drives North
| title_orig    =
| translator    =
| image         =
| image_caption =
| author        = Alec Coppel
| illustrator   =
| cover_artist  =
| country       = United Kingdom English
| series        = 
| genre         = thriller
| publisher     = Harrap
| release_date  = 1951
| english_release_date =
| media_type    =
| pages         =
| isbn          =
| preceded_by   =
| followed_by   = 
}}
The film was based on a 1951 novel by Coppel.   
 
==Production==
It was John Mills first film in almost two years.  Sam Wanamaker had been living in England since 1949 and was offered the part after writing to his agent from holiday in France asking if any jobs were going. 

Trader Faulkner had a small role as a gypsy. 
==Critical reception==
The New York Times wrote, "this little melodrama serves as still another reminder, from a country that jolly well knows how to exercise it, that restraint can work minor wonders...Persuasive and tingling, minus one false note... No doubt about it. The British have what it takes." 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 