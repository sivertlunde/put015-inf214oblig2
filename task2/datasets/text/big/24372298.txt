Female Cats (film)
{{Infobox film
| name = Female Cats
| image = Female Cats (film).jpg
| image_size = 
| caption = Theatrical poster for Female Cats (1983)
| director = Shingo Yamashiro 
| producer = Yutaka Okada Kimio Shindō
| writer = Makoto Naitō Kochiho Katsura
| narrator = 
| starring = Ai Saotome
| music = Tarō Morimoto
| cinematography = Yonezō Maeda
| editing = Akira Suzuki
| distributor = Nikkatsu
| released = December 23, 1983
| runtime = 86 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1983 Japanese film in Nikkatsus Pink film#Second wave (The Nikkatsu Roman Porno era 1971–1982)|Roman porno series, directed by Shingo Yamashiro and starring Ai Saotome.

==Synopsis==
Innocent female doctor Mineko was seduced by Sachiko, an aggressive lesbian, in her younger days. Mineko begins dating a male coworker, and accepts an invitation to a party at the home of a transvestite patient. Sachiko comes out of Minekos past, enraged with jealousy, and begins tormenting Mineko and her boyfriend. Weisser, p. 140.    

==Cast==
* Ai Saotome: Mineko Kagami 
* Sachiko Itō: Kotoe Hiratsuka
* Koichi Iwaki: Shari
* Hiroshi Nawa: Tarō Iruka
* Kōji Minakami: Mamoru  Iruka
* Akiyoshi Fukae: Takehiko Ōbayashi
* Yoshishige Ōtsuka: Eiko Kanzaki
* Yoshishige Ōtsuka: Akiko Kanzaki
* 美露: Tommy
* Mitsu Senda: Jōji
* Kotomi Aoki: Akane Minakawa
* Mariko Nishina: Hiromi
* Keiichi Satō: Mikami
* Ryūji Katagiri: Abe
* Madoka Sawa: Julie
* Akira Takahashi: Keiji
* Hiromichi Nakahara: Movie star

==Critical appraisal==
Female Cats director Shingo Yamashiro was best known as an actor, having debuted in 1957.    He appeared in the Battles Without Honor and Humanity series, and one of his better-known appearance for Western audiences is in Kiyoshi Kurosawas Sweet  Home (1989).   

In his Behind the Pink Curtain: The Complete History of Japanese Sex Cinema, Jasper Sharp uses Female Cats as an example of the range of themes possible in the Roman Porno genre, calling it an Dario Argento|Argento-esque thriller.  In their Japanese Cinema Encyclopedia: The Sex Films, the Weissers compliment Yamashiros direction as the best thing about the film. They write that he, "goes the extra mile to compose many astonishingly good-looking sequences, punctuated by some creatively stylish camerawork."  A lesbian shower sequence between Mineko and Sachiko is singled out for praise. The scene is shot from the shower faucets point of view, through a spray of water.  However they fault the films unimaginative script and its "lurid plot". 

==Availability==
Female Cats was released theatrically in Japan on December 23, 1983.  It was released to home video in VHS format in Japan on March 16, 1984, and re-issued on June 22, 1990. It was released as part of the Nikkatsu Roman Porno Series of VHS releases on December 6, 1991.    It was released on DVD in Japan on December 4, 1998, and re-released on DVD on June 23, 2006, as part of Geneon Universal Entertainment|Geneons fourth wave of Nikkatsu Roman porno series.   In this format it was issued in the 4-disc Roman Porno series DVD Box, Vol. 1 on March 7, 2008.  It is currently scheduled to be released in the USA on DVD May 7, 2013, under the title She Cat.

==Bibliography==

===English===
*   
*  
*  
*  

===Japanese===
*  
*  
*  
*  
*  

==Notes==
 

 
 
 
 
 
 
 
 