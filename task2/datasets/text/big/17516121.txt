Science Ninja Team Gatchaman: The Movie
{{Infobox Film 
 | name           = Science Ninja Team Gatchaman: The Movie
 | image          =
 | director       = Hisayuki Toriumi	 Toshio Suzuki
 | writer         = Jinzō Toriumi
 | based on       = Science Ninja Team Gatchaman Nobuo Tanaka
 | music          = Koichi Sugiyama
 | cinematography = 
 | editing        = 
 | distributor    = Shochiku
 | released       =  
 | runtime        = 110 minutes
 | country        = Japan
 | language       = Japanese
 | budget         =
 }}

  is a 1978 Japanese anime science fiction film and a version of the anime series Science Ninja Team Gatchaman. 

== Plot ==
Sosai X traveled millions of light years to reach the planet Earth and creates a mutant Berg Katse. 30 years later he is the leader of the terrorist organisation known as Galactor. They want to conquer the world. Since Galactor controls the mechanical monster "Turtle King," the  nations of the world live in fear. 

Earths only hope lies with five teenagers who can move like shadows. They are Gatchaman, five superheroes who arrive in their spaceship "Godphoenix" to stop Galactors Machiavellian plans of world domination.

==Cast==

*Ken, the Eagle: Katsuji Mori

*Joe, the Condor: Isao Sasaki

*Jun, the Swan: Kazuko Sugiyama

*Jinpei, the Swallow: Yoku Shioya

*Ryu, the Horned Owl: Shingo Kanemoto

*Dr. Kozaburo Nambu: Toru Ohira

*Red Impulse Captain: Hisayoshi Yoshizawa

*Berg Katse: Mikio Terashima
 Nobuo Tanaka

*Director Anderson: Teiji Omiya

*Sabu: Hiroya Ishimaru

*Narrator: Kiyoshi Kobayashi

== References ==
 

== Further reading ==
* G-Force: Animated (TwoMorrows Publishing: ISBN 978-1-893905-18-4)

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 

 