It! (1966 film)
 
 
{{Infobox film
| name     =It!
| image          = It (1966 film) DVD cover.jpg
| writer         = Herbert J. Leder
| starring       = Roddy McDowell Jill Haworth Paul Maxwell Alan Sellers
| director       = Herbert J. Leder
| producer       = Robert Goldstein, exec. producer
Herbert J. Leder
Tom Sachs, assoc. producer
| studio =Gold Star Films Ltd. distributor = Warner Bros.-Seven Arts
| released   = November 15, 1967
| runtime        = 97 min.
| music          = Carlo Martelli
| language =English
}}

It! (aka Anger of the Golem, Curse of the Golem) is a 1967 British/American horror film made by Seven Arts Productions and Gold Star Productions, Ltd. that features the Golem of Prague as its main subject. The film was made in the style of the Hammer Studios films both in sound and cinematography. It! stars Roddy McDowall as the mad assistant museum curator Arthur Pimm, who evokes (brings to life) the golem by finding a hidden scroll in a hollowed out compartment of the golems right foot and placing it under its tongue. John Hamilton, The British Independent Horror Film 1951-70 Hemlock Books 2013 p 174-177 

==Plot==
  firemen fighting a warehouse conflagration|fire. The warehouse belongs to a London museum, whose curator, Grove and curators assistant, Arthur Pimm respond to the report of the fire. They inspect the rubble for anything salvageable. Standing undamaged in the smouldering ruins is a large rough-hewn stone statue that Grove observes is made in the style of "Mid-European Primitive." He examines it, but carelessly places his umbrella onto the statues arms that are partially outstretched, parallel to each other (see image below). Pimm timidly keeps his distance, but provides Grove his magnifying glass. As Pimm goes back to the car for a flashlight to aid in Groves investigation he is stopped by Grovess death cry. Pimm returns to find him dead at the foot of the statue with the statues right arm now somewhat lower than the other. Pimm suspiciously removes Groves keys from his pocket and calls the police. 

Pimm is then seen entering his apartment in conversation with his mother, who is seen from the back in her rocking chair. He tells her that he believes he is sure to be chosen the new curator by the museums board of trustees. He brings out a necklace with a jewel pendant, which he has borrowed from the museum for his mother to wear to indulge her. As he puts the necklace on his mother we see that she is a decaying corpse.

The next day Pimm is in his office at the museum when Mr. Trimingham, presumably a museum official with higher authority than the curator, comes in demanding Groves keys. Pimm pretends to find them when Trimingham dials the police. Pimm must find a way, then, to return to its display case the jewellery he borrowed the night before. An opportunity presents itself when the electrician, Ellis is killed by the newly installed statue falling on him after he had gone out of his way to mock it. Pimm discovers the dead man under the statue, cuing him to break into the display case and return the borrowed jewellery. The crime scene then looks like an apparent murder/robbery.

Mr. Trimingham wants to rid the museum of the statue due to its connection to the two unexplained deaths, and he contacts the New York Museum in order to sell them the statue. A Jim Perkins is sent to London to examine the statue. Perkins and Pimm examine the statue together, and Perkins also notices the Hebrew inscriptions on the robe of the statue, as did Grove before. Perkins expresses his belief that the statue is a   at a later date. Perkins explains that the golem was built as a guardian of "the community," presumably the Jewish community for which it was constructed. He discovers an inscription that reads "Judah Loew, Prague, 1500" with the last two digits being undecipherable. Perkins is certain that the statue is authentic, but Pimm pretends to be skeptical. Perkins suggests making a rubbing of the Hebrew inscriptions, which Pimm secretly does.
 
While Perkins romances Groves daughter, Ellen, whom Pimm had his eye on, Pimm takes his rubbing to a rabbi, who translates the Hebrew inscription (see, below). He agrees to give Pimm the translation only if Pimm tells him where he got the rubbing. After the rabbi reads the translation Pimm tells him that it is from a statue from Czechoslovakia. The rabbi exclaims that if it is authentic, it is the "great Golem," which would be the most powerful force on earth. The rabbi also tells Pimm as did Perkins that a small scroll, with the Hebrew word "emet" (אמת), "truth," written on it, placed in its mouth would bring it to life.

A new curator begins his duties at the museum, a Professor Weal, who describes himself as a "stickler" and a "fuss pot," stricter than Grove had been, who had been known as a "kidder." When Weal finds Pimm looking at the golem, he tells Pimm that he is making a new rule for the museum: no staff other than the night watchman may be in the "art rooms" without his express permission. 

Pimm speaks to the golem. He tells it that he is attuned to the spirit world and knows that there is a spirit living inside it. He directly asks the golem where the emet scroll is. With a clap of thunder the golems right arm appears fleetingly to point down at its feet. Pimm taps the golems feet with a hard object and finds a hollow spot on the top of the golems right foot. He opens this compartment and finds the emet scroll that he takes and puts in the golems mouth. A slow heartbeat then commences. Pimm deliriously declares to the golem, "I am your master." When he is confronted and fired by Weal for disobeying his new rule, Pimm locks him in the room and orders the golem to kill him, which is promptly done with a blow to the head. Pimm is then awakened at home by Inspector White and his assistant who are investigating Weals death. The police are more and more suspicious of Pimm after this murder.

Pimm meets Ellen in the park outside the museum the next day, and learns from her that she and Perkins had discovered Weals body the night before. Pimm persuades Ellen to join him for lunch. After lunch Pimm tells Ellen that he can do anything. He tells her that he can have a nearby Thames bridge pushed down. That evening he awakens the golem by putting the emet scroll in its mouth and drives it to the bridge Pimm wants it to destroy. This it does but not without the two of them being seen.

Ellen and Perkins are at her apartment when they hear a news flash about the destruction of the Thames Bridge. Pimm calls Ellen, who does not recognize his voice due to his drunkenness. He wants to meet her to tell her all about what he has been doing with the golem. Ellen is ready to put him off, but Perkins indicates that he wants to see Pimm. When Pimm sees that it is Perkins rather than Ellen he changes the subject from the golem to his own inside track to becoming the new curator. Pimm tells Perkins that he will help him acquire the golem for the New York Museum.

Pimm wants to rid himself of the golem and the temptation it poses for him, so he tries to burn it by dousing it with gasoline in a shack that he sets on fire. The golem-inscription proves true for it that in the 20th century it can "neither by fire, nor water, nor force, nor anything by man created" be destroyed. The golem accordingly returns to the museum to Pimms dismay. The golem has to break into the museum to get back to its pedestal, which again draws the attention of the police and Perkins.

Perkins confronts Pimm in the "golem room" for he has seen him with the golem walking out of the museum. Pimm admits to being overwhelmed by the power that the golem gives him, but he rebuffs Perkins. The police pop out to arrest Pimm and put him into a mental hospital. Pimm telepathically contacts the golem, whom he had commanded to swallow the emet scroll. The golem breaks Pimm out of the mental hospital and helps him retrieve his mothers corpse and kidnap Ellen. Pimm as assistant curator obtains entrance to a remote annex of the museum called "the Cloisters" through an unsuspecting Miss Swanson. Miss Swanson tries to draw the authorities to the Cloisters to rescue her and Ellen when she realizes what Pimm is up to, but Pimm kills her with her own bonfire.

The British military finds that conventional mortars and rockets are useless against the golem, so they get clearance to detonate a small nuclear warhead with the capability of destroying anything within a mile radius. Perkins heroically saves Ellen, who is thrown out of the Cloisters by Pimm, who delusively thinks that Ellen might take his mothers place in his affections. Perkins and Ellen arrive at the sandbag barrier just in time to escape the nuclear explosion, which one soldier believes has annihilated everything. Remarkably, again, in accordance with the inscription, the golem comes through it all intact, but ends the film by walking into the ocean.

==The films golem inscriptions==
:"Power bringeth destruction; beware, lest it be unleashed."

:"He who will find the secret of my life at his feet, him will I serve until beyond time.
:"He who shall evoke me in the 17th century, beware, for I cannot by fire be destroyed.
:"He who shall evoke me in the 18th century, beware, for I cannot by fire or by water be destroyed.
:"He who evokes me in the 19th century, beware, for I cannot by fire or by water or by force be destroyed.
:"He who in the 20th century shall dare evoke me, beware, for neither by fire, nor water, nor force, nor anything by man created can I be destroyed.
:"He who in the 21st century evokes me must be of Gods hand himself because on this earth the person of man existeth no more."

==Cast==
* Roddy McDowall as Arthur Pimm
* Jill Haworth as Ellen Grove
* Paul Maxwell as Jim Perkins
* Aubrey Richards as Professor Weal
* Ernest Clark as Harold Grove Oliver Johnston as Curator Trimingham
* Noel Trevarthen as Inspector White Ian McCulloch as Detective Wayne
* Richard Goolden as the old rabbi
* Dorothy Frere as Miss Swanson
* Tom Chatto as the young captain Steve Kirby as Ellis the electrician
* Russell Napier as Boss
* Frank Sieman as museum workman
* Brian Haines as museum guard Mark Burns as first officer
* Raymond Adamson as second officer Lindsay Campbell as policeman John Baker as second museum guard
* Alan Seller as the golem

== Production ==
 
  Pretty Boy Floyd (1960).

== Release ==
 
Since Seven Arts Productions acquired Warner Bros. in 1967 the film was released by Warner Bros.-Seven Arts in the United States. The film was widely released in the US in 1967 on a double bill with The Frozen Dead.

=== Home media ===
It! wasnt released on home video until December 9, 2008 when Warner Home Video released it with The Shuttered Room in its new series of "Horror Double Feature" DVDs. 

==Footnotes==
 

==External links==
* 
*  

 
 
 
 
 
 