Snakes on a Plane
 
 
{{Infobox film
| name           = Snakes on a Plane
| image          = SOAP poster.jpg
| alt            = Text at the center of the image says "Snakes on a Plane". Behind it is an overhead view of a jet passenger airplane with two snakes coiled around it. Towards the cockpit of the image the snakes heads face each other with their mouths open and fangs and teeth shown. The background is all black.
| caption        = Theatrical release poster
| director       = David R. Ellis
| producer       = {{Plain list | 
* Craig Berenson
* Don Granger
* Gary Levinson
}}
| screenplay     = {{Plain list | 
* David J. Taylor
* John Heffernan
* Sebastian Gutierrez
}}
| story          = {{Plain list | 
* David J. Taylor
* John Heffernan
* David Dalessandro
}}
| starring       = {{Plain list | 
* Samuel L. Jackson
* Julianna Margulies Nathan Phillips
* Bobby Cannavale
* Rachel Blanchard
}}
| music          = Trevor Rabin Adam Greenberg
| editing        = Howard Smith
| studio         = Mutual Film Company
| distributor    = New Line Cinema
| released       =  
| runtime        = 106 minutes
| country        = United States
| language       = English
| budget         = $33 million 
| gross          = $62 million 
}} action thriller thriller film  directed by David R. Ellis and starring Samuel L. Jackson. It was released by New Line Cinema on August 18, 2006, in North America. The film was written by David Dalessandro, John Heffernan, and Sheldon Turner and follows the events of hundreds of snakes being released on a passenger plane in an attempt to kill a trial witness.

The film gained a considerable amount of attention before its release, forming large fanbases online and becoming an Internet phenomenon, due to the films title, casting, and premise. In response to the Internet fan base, New Line Cinema incorporated feedback from online users into its Filmmaking#Production|production, and added five days of reshooting. Before and after the film was released, it was parodied and alluded to on television shows and films, fan-made videos, video games, and various forms of fan fiction|literature.
 Internet buzz, the films gross revenue did not live up to expectations, earning United States dollar|US$15.25 million in its opening weekend.       The film grossed US$62 million worldwide before its release on home video on January 2, 2007.

==Plot== FBI agents leis with viper that caused it. Co-pilot Rick, unaware of the snake, believes Sam has suffered a heart attack and continues toward LAX.
 ophiologist Dr. Steven Price (Todd Louiso|Louiso). Based on pictures of the reptiles emailed to him via a mobile phone, Price believes a Los Angeles snake dealer known for illegally importing exotic and highly dangerous snakes to be responsible. After a shootout with the dealer, he reveals that Kim hired the dealer to obtain the snakes. His supply of anti-venom is commandeered for the planes victims, and Harris gives orders to have Eddie Kim arrested. 

Harris contacts Flynn, telling him that anti-venom will be ready for the passengers when they land. However, Flynn discovers that the cockpit is filled with snakes and Rick is dead. After a brief discussion, Troy, Three Gs bodyguard, agrees to land the plane based on prior experience. After everyone gets prepared, Flynn shoots out two windows with his pistol, causing the plane to Uncontrolled decompression|depressurize. The snakes are blown out of the cockpit and the lower floor of the plane. Flynn and Troy take the controls of the plane and Troy reveals that his flight experience was from a video game flight simulator. After an emergency landing, the plane makes it to the terminal. The passengers leave the plane and anti-venom is given to those who need it. Just as Flynn and Sean are about to disembark the plane, a final snake jumps out and bites Sean in the chest. Flynn draws his gun and shoots the snake, and paramedics rush to Sean, who is unharmed due to a bulletproof vest. As a token of gratitude, Sean later takes Flynn to Bali and teaches him how to surf.

==Cast== FBI agent assigned to protect Sean Jones on his flight to Los Angeles.
* Julianna Margulies as Claire Miller, a flight attendant. Nathan Phillips as Sean Jones, a surfer and dirtbike racer who witnesses a brutal murder committed by Eddie Kim.
* Bobby Cannavale as Special Agent Henry "Hank" Harris, Flynns contact in Los Angeles. Chihuahua Mary-Kate aboard.
* Flex Alexander as Clarence "Three Gs", a famous rapper who is germophobic, keeps a bottle of hand sanitizer with him, and refuses to be touched by others.
* Kenan Thompson and Keith Dallas as Troy and Big Leroy, Clarences bodyguards.
* Lin Shaye as Grace, the senior flight attendant who acts as the flights purser.
* Terry Chen as Chen Leong, a martial artist who is among the surviving passengers.
* Elsa Pataky as Maria, a female passenger who brings her infant son aboard.
* Sunny Mabrey as Tiffany, a flight attendant who develops a crush on Sean.
* Tygh Runyan as Tyler, a very allergenic passenger.
* Emily Holmes as Ashley, Tylers wife. Tom Butler as Captain Samuel "Sam" McKeon, the captain of the plane.
* David Koechner as Richard "Rick", Captain McKeons co-pilot.
* Byron Lawson as Eddie Kim, a crime syndicate leader.
* Todd Louiso as Dr. Steven Price, a snake venom expert assigned by the FBI to communicate with Flynn.
* Scott Nicholson as Daniel Hayes, a U.S. Prosecutor who is murdered by Kim at the start of the film.
* Taylor Kitsch as Kylerick "Crocodile" Cho, a man who is attacked by the snakes while having sex in the bathroom with his girlfriend.
* Samantha McLeod as Kelly, a woman who is attacked by the snakes while having sex in the bathroom with her boyfriend. Kevin McNulty as Emmett Bradley, an air traffic tower controller.

==Development== Paramount showed interest in the script, followed up by New Line Studios, which took over the rights for production.

Originally, the film, under the working title "Snakes on a Plane", was going to be directed by Hong Kong action director Ronny Yu.  Jackson, who had previously worked with Yu on The 51st State, learned about the announced project in the Hollywood trade newspapers and, after talking to Yu, agreed to sign on without reading the script based on the director, storyline, and the title.   
 B movie-esque title generated a lot of pre-release interest on the Internet. One journalist wrote that Snakes on a Plane is "perhaps the most internet-hyped film of all time".    Much of the initial publicity came from a blog entry made by screenwriter Josh Friedman, who had been offered a chance to work on the script.    The casting of Samuel L. Jackson further increased anticipation. At one point, the film was given the title Pacific Air Flight 121, only to have it changed back to the working title at Samuel Jacksons request.    In August 2005, Samuel Jackson told an interviewer, "Were totally changing that back. Thats the only reason I took the job: I read the title."    On March 2, 2006, the studio reverted the title to Snakes on a Plane.  New Line hired two additional writers to smooth out the screenplay. 
 from PG-13 to R and bring it in line with growing fan expectations. The most notable addition was a revision of a catchphrase from the film that was parodied on the Internet by fans of the film, capitalizing on Samuel L. Jacksons typically foul-mouthed and violent film persona: "Enough is enough! I have had it with these motherfucking snakes on this motherfucking plane!".    Subsequently, the public responded favorably to this creative change and marketing strategy, leading some members of the press to speculate that "the movie has grown from something of a joke into a phenomenon".    

  was one of the many snakes used during filming]] milk snake corn snakes, mangrove snakes. diamondback rattlesnakes had been released at a showing of the film on August 22, 2006, in Phoenix, Arizona|Phoenix, Arizona. It was later revealed that one snake had made its way into the lobby of the theater on its own, and another had been found in the parking lot in a separate incident. The snakes were later released back into the desert.   

==Media coverage==
  in July 2006]]

===Print===
An illustrated book from Thunders Mouth Press, Snakes on a Plane: The Guide to the Internet Ssssssensation by David Waldon, details the Internet phenomenon and was published July 28, 2006. Waldon details various viral videos relating to the films craze, and interviewed their producers to find out what about the film captured their attention. 

===Music=== Captain Ahab (who ultimately won the contest), Louden Swain, the Former Fat Boys, Nispy, and others. In addition, a music video for the film, "Snakes on a Plane (Bring It)" by Cobra Starship, was released on July 10, 2006 on MTV2s Unleashed. The music video appeared on the films soundtrack as well as during the films closing credits.

In October 2005, Nathanial Perry and Chris Rohan recorded an audio trailer parody|spoof, which helped fuel the Internet buzz. Perry and Rohan recorded the "motherfucking snakes" line in the audio trailer which was added to the film during the week of re-shoots. In July 2006, New Line Cinema signed a worldwide licensing agreement with the Cutting Corporation to produce an audiobook of the film. 

===Television===
Beginning in May 2006, episodes of The Daily Show with Jon Stewart and its sister show The Colbert Report contained references to Snakes on a Plane s title, the catchphrase, and general premise.  On August 15, 2006, Samuel L. Jackson guest featured on The Daily Show, opening with the films catchphrase. Keith Olbermann featured stories about the film and Internet buzz several times on his MSNBC news program Countdown with Keith Olbermann|Countdown. In addition, G4 (TV channel)|G4s Attack of the Show! featured a semi-regular segment entitled "Snakes on a Plane: An Attack of the Show Investigation", and had a week dedicated to the film which included interviews and the appearance of hundreds of snakes on set. 

===Internet===
Snakes on a Plane generated considerable buzz on the Internet after Josh Friedmans blog entry  and mentions on several Internet portals. The title inspired bloggers to create songs, apparel, poster art, pages of fan fiction, parody films, mock movie trailers, and short film parody competitions.   On July 6, 2006, the official Snakes on a Plane website started a promotional sweepstakes called "The #1 Fan King Cobra Sweepstakes". The contest made innovative use of the publicity-generating potential of the Internet, requiring contestants to post links on forums, blogs, and websites and collecting votes from the users of those sites.

Many of the early fan-made trailers and later other viral videos and commercials circulated via YouTube, and captured media attention there with such titles as: Cats on a Plane (which was featured in Joel Siegels review of Snakes on a Plane on Good Morning America), Snakes Who Missed the Plane, All Your Snakes Are Belong To Us (a spoof of the All your base are belong to us phenomenon), Steaks on a Train,    and Badgers on a Plane (a spoof of "Badger Badger Badger"). Several websites also held contests about the film in fan-submitted short films and posters.

In August 2006, Varitalk launched an advertising campaign in which fans could send a semi-personalized message in Samuel Jacksons voice to telephone numbers of their choosing.    Within the first week, over 1.5 million calls were sent to participants. 

===Previews===
In June 2006, New Line commissioned famed UK audio-visual film  , and the first official trailer appeared online on June 26, 2006.  Another trailer circulated in July 2006, showing several of the snake attacks and a missing pilot and co-pilot. Rotten Tomatoes had video clips of the official trailers, as well as fan-made trailers. 

During a July 21, 2006 panel discussion at the Comic-Con International|Comic-Con Convention in San Diego, California, a preview clip from the film was shown to a crowd of more than 6,500 people. The panel included actors Samuel L. Jackson and Kenan Thompson, director David R. Ellis, and snake-handler Jules Sylvester. 

==Release==
  }}

Snakes on a Plane debuted on August 18, 2006. The film opened in 3,555 theaters and had some late-night screenings on August 17. In a move meant to exploit the attention from the film, a straight-to-DVD Z-movie horror film with a supernatural twist, Snakes on a Train, was released on August 15, 2006, three days before the films theatrical release.   

===Critical response=== normalized rating system, the film earned a mixed rating of 58% based on 31&nbsp;reviews by mainstream critics.    Reviewers reported audiences cheering, applauding, and engaging in "call and response", noting that audience participation was an important part of the films appeal.  

The Arizona Republics Randy Cordova gave the film a positive review, calling the film "... an exploitation flick that knows what it wants to do, and it gets the job done expertly." and a "... Mecca for B-movie lovers."  Mick LaSalle of the San Francisco Chronicle enjoyed the film, asking his readers "... if you can find a better time at the movies this year than this wild comic thriller, let me in on it."  Boston Globe reviewer Ty Burr reacted to Samuel L. Jacksons performance by saying he "... bestrides this film with the authority of someone who knows the value of honest bilge. Hes as much the auteur of this baby as the director and screenwriters, and that fierce glimmer in his eye is partly joy." 

Peter Travers of Rolling Stone gave the film one and a half stars out of four, saying that "after all the Internet hype about those motherfuckin snakes on that motherfuckin plane, the flick itself is a murky stew of shock effects repeated so often that the suspense quickly droops along with your eyelids."  David Denby of The New Yorker claimed that the film "... may mark a new participatory style in marketing, but it still gulls an allegedly knowing audience with the pseudo-morality of yesteryear." 
 Motion Picture Association of America to match fan expectations.  He argued that the film would have grossed more revenue at the box office with a PG-13 rating, stating that the demographic most likely to be drawn to a movie titled Snakes on a Plane is males between the ages of 12 and 15. "My fourteen-year-old son, Danny, for instance, felt a powerful inclination to go out and see the movie with his two sleep-over friends this Sunday night," he explained, "but I wouldnt permit it. Its rated R for good reason."  Medved ultimately awarded the film 2 1/2 stars out of 4 in a radio review, but said that he did so "grudgingly." 

===Box office===
Due to the Internet hype surrounding the film, industry analysts estimated the films opening box office to be between   for the number one position during its opening weekend, it did not meet these estimates and grossed only $15.25 million in its opening days, a disappointment for New Line Cinema.  In its second weekend, the film fell to sixth place with $6.4 million, a more than fifty percent drop from its opening weekend revenue.    By the end of its theatrical run, the film grossed $62,022,014 worldwide.   
 Robert K. Shaye, the founder of New Line, stated that he was "disappointed" that Snakes on a Plane was a "dud" despite "higher expectations".    The press declared that Snakes on a Plane was a "box office disappointment",   with The New York Times reporting that after all the "hype online, Snakes on a Plane is letdown at box office"  and Entertainment Weekly reporting that the film was an "internet-only phenomenon." 

===Home media=== deleted and extended scenes, several featurettes, Cobra Starships music video, and Trailer (film)|trailers. The U.S. Blu-ray Disc|Blu-ray was released on September 29, 2009. 

===TV version=== purposely dubbed its foul language with nonsense words for a broader audience. An example is Samuel L. Jacksons line toward the end of the film, "I have had it with these motherfucking snakes on this motherfucking plane", which is replaced with "I have had it with these monkey-fighting snakes on this Monday-to-Friday plane".   

=== Adaptations ===
Black Flame published the novelization of the film, written by Christa Faust.    The 405&ndash;page novel contains significant backstories for the characters and introduces other characters that were not featured in the film.    Comic book writer Chuck Dixon wrote a comic book adaptation of the film. DC Comics released the two-issue miniseries on August 16, 2006 and September 27, 2006 under their Wildstorm imprint.   

==Soundtrack==
{{Infobox album
| Name        = Snakes on a Plane: The Album
| Type        = soundtrack
| Artist      = Various artists
| Released    = August 15, 2006
| Recorded    =
| Genre       =
| Length      =
| Label       = Decaydance Records New Line Records
| Producer    = Jason Linn}}
{{Album ratings|title=Soundtrack
| rev1      = Allmusic
| rev1Score =   
|rev2= RapReviews|rev2Score=   
}} enhanced portion Captain Ahab and "Here Come the Snakes (Seeing Is Believing)" by Louden Swain. The single "Snakes on a Plane (Bring It)" peaked at the 32nd position of Billboards Hot Modern Rock Tracks in 2006.   
 William Beckett, Maja Ivarsson, Travie McCoy
# "The Only Difference Between Martyrdom and Suicide Is Press Coverage" (Tommie Sunshine Brooklyn Fire Remix) by Panic! at the Disco
# "Black Mamba" (Teddybears Remix) by The Academy Is...
# "Ophidiophobia" by Cee-Lo Green
# "Cant Take It" (The Baldwin Brothers "El Camino Prom Wagon" Remix) by The All-American Rejects
# "Queen of Apology" (Patrick Stump Remix) by The Sounds
# "Of All the Gin Joints in All the World" (Tommie Sunshines Brooklyn Fire Retouch) by Fall Out Boy
# "New Friend Request" (Hi-Tek Remix) by Gym Class Heroes The Bronx
# "Remember to Feel Real" (Machine Shop Remix) by Armor for Sleep
# "Wine Red" (Tommie Sunshines Brooklyn Fire Retouch) by The Hush Sound
# "Bruised" (Remix) by Jacks Mannequin
# "Final Snakes" by Shranky Drank
# "Wake Up" (Acoustic) by Coheed and Cambria
# "Lovely Day" by Donavon Frankenreiter
# "Hey Now Now" by Michael Franti & Spearhead
# "Snakes on a Plane - The Theme" (Score) by Trevor Rabin

==See also==
 
* List of killer snake films
*  
*  

==References==
 

==External links==
 
 
*   (archived version)
*  
*  
*  
*  
*  

 

 
 

 
 
   
 
   
   
   
 
 
 
 
 
 
 
 
 
 