Crossplot (film)
{{Infobox film
| name           = Crossplot
| caption        = 
| image	         = Crossplot FilmPoster.jpeg
| director       = Alvin Rakoff
| producer       = Robert S. Baker
| writer         = Leigh Vance John Kruse
| starring       = Roger Moore Claudie Lange Alexis Kanner
| music          = Stanley Black
| cinematography = Brendan J. Stafford
| editing        = Burt Rule
| distributor    = United Artists
| released       = 25 November 1969
| runtime        = 96 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Belgian actress M in the James Bond films, also appeared.

==Plot== Hungarian Marla Kugash (Lange). He finds her among the anti-war movement in the bohemian depths of swinging London. She is in the company of a young man, Tarquin, who is extremely protective of her and overtly aggressive to Fenn.
 aristocrat in spite of his anti-war sentiments.
 Hyde Park. They manage to foil the plot.

==Cast==
* Roger Moore as Gary Fenn 
* Martha Hyer as Jo Grinling 
* Alexis Kanner as Tarquin 
* Claudie Lange as Marla Kugash 
* Derek Francis as Sir Charles Moberley 
* Ursula Howells as Maggi Thwaites 
* Bernard Lee as Chilmore  Francis Matthews as Ruddock 
* Dudley Sutton as Warren 
* Mona Bruce as Myrna 
* Veronica Carlson as Dinah 
* Michael Culver as Jim 
* Gabrielle Drake as Celia 
* Tim Preece as Sebastian 
* Norman Eshley as Athol
* John Barrard as Wedding Guest

David Prowse of later Star Wars fame has a cameo as the best man at a wedding in the film.

==Reception== The Saint. Bond role Roger Moore would take on four years later. Crossplot currently holds an average two and a half star rating (4.9/10) on IMDb.

==References==
{{reflist|refs=
   
}}

==External links==
*  

 

 
 
 
 