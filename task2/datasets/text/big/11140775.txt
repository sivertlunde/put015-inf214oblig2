Indrudu Chandrudu
 
{{Infobox film
| name = Indrudu Chandrudu
| image = Indrudu_Chandrudu-M.jpg
| image size =
| caption =
| director = Suresh Krissna
| producer = D.Rama Naidu
| screenplay = Kamal Haasan 
| narrator = Jayalalitha
| music = Ilaiyaraaja
| cinematography =
| editing =
| studio =Suresh Productions
| distributor =
| released = 24 November 1989
| runtime = 158 min.
| country = India
| language = Telugu
| budget =
| gross =
| preceded by =
| followed by =
}}
 Indian feature directed by Suresh Krissna and produced by D.Rama Naidu, starring Kamal Haasan, Vijayashanti and Charan Raj in lead roles.

The story of the film is inspired by Richard Dreyfusss 1988 film Moon over Parador, which was based on a short story by Charles G. Booth called Caviar for His Excellency. It was later dubbed in Tamil as Indran Chandran and in Hindi as Mayor Sahab.

==Plot==
G.K Rayudu (Kamal Haasan) is a corrupt Mayor and along with his assistant Charan Raj takes bribe and sells a market to a rich businessman and forces the poor people doing business in the land to move out threatening physical harm. Although  Rayudu is married (to Srividya), he has a mistress. Rayudu comes to find out that his assistant and his mistress are married and has been cheating him out of his money for a long time. He finds the assistant’s stash of diamonds and takes it. After spending some time with his wife and kids, Rayudu gets a change of heart and decides to expose his and his assistant’s illegal activities. Before he leaves, he hides the diamonds inside one of his children’s toys. His assistant finds out, murders Rayudu in a fit of rage and stores the body in a freezer.

Panicking that soon people will realize that the Mayor is missing, he notices Rayudus younger lookalike, Chandram (Kamal Haasan) in a mental institution. Deducting that Chandram is faking his mental breakdown to beat a murder charge and avoid jail, they blackmail Chandram into playing the Mayor till a major deal is completed and the assistant gets a lot of money. Chandram is promised freedom if he is successful. Chandram learns Rayudu’s speech and mannerisms and with heavy prosthetics and make up, becomes a suitable double for the Mayor.

Meanwhile, an investigative reporter (Vijayashanti) is trying to expose the Mayor for his illegal activities but she hasn’t found any real evidence yet. As she almost comes close to exposing the identity of Chandram mistakenly, the assistant sends goons to kill her. Chandram, having heard this, removes his disguise, sneaks out and saves the reporter from the goon. When the reporter gets suspicious about the resemblance between him and the Mayor, Chandram lies that he is the Mayor’s son who doesn’t like his father’s illegal ways. The assistant’s search for his diamonds using Chandram proves futile.

The reporter decides to charm Chandram to get real evidence of the Mayor’s illegal activities and flirts with him and they try to meet many times. This leads to hilarious situations in which Chandram has to sneak out without the assistant’s knowledge. The assistant tries to kill her one more time, but Chandram, again saves her but his cover is blown. Both are on the run from the corrupt police and the assistant.

Chandram finally reveals the truth about his murder charge to the reporter. He used to be a singer and a dancer. A corrupt politician, on the pretext of helping him and his female dance partner become famous invites them to a hotel room. While the politician sends Chandram on an errand run, he brutally rapes and murders the female dancer and uses his influence to put the blame on Chandram. Chandram is found guilty but during trial, he fakes mental retardation and is put in the mental institution. The reporter and Chandram then falls for each other. Tired of running, they hatch a plan to stop the assistant and the politician who murdered the female dancer,who also happens to be the assistant’s partner. 
 
Chandram, goes back to the crowded market that he sold in the beginning of the movie, and proclaims that he will give the land back to the people and expose illegal activities he had committed with his assistant, in a big public gathering soon. The assistant, realizing that if he killed the mayor he would become the first suspect, searches for a way to get the upper hand. He traces down Chandram’s mother, kidnaps her and uses this as a leverage to blackmail Chandram into not doing anything silly at the public meeting.

The day before the meeting, since the Mayor’s children wanted to spend time with their father, Chandram impersonates the Mayor and plays with the kids. He accidentally finds the diamonds and leaves clues so that the children could find it in the future.

On the day of the meeting, a huge crowd is built up to hear the speech and it is attended by prominent ministers.  Chandram, impersonating Mayor, is being held on a close watch by the assistant. With the help of the reporter and her father, he finds the location where his mother is being held captive by the corrupt politician who had sent him to jail. He weasels into the car of a prominent minister who asks the assistant to use a different vehicle. Finally free from the assistant’s view, Chandram uses the opportunity to leave and rescue his mother and the corrupt politician is killed in the process. Chandram comes back to the minister’s vehicle just in time as the vehicle reaches the press conference, so the assistant does not suspect anything.

During the press conference, Chandram details all the illegal activities he had committed along with his assistant and other politicians and businessmen with evidence. Just as he finishes his speech, he is shot by an unknown assailant. Chandram turns to his assistant and blames him before he collapses and dies. The crowd of people become agitated and beats the assistant. An ambulance comes and picks up the Mayor’s body.

In the ambulance, Chandram gets up seemingly unharmed. The ambulance is driven by the reporter’s father and he was the unknown assassin. The faux assassination was a distraction to let everyone think that Chandram/Mayor is dead.  They then reveal the real body of the Mayor inside the ambulance. They drive the ambulance into a power grid after jumping out of the vehicle. The ambulance explodes with the real Mayor’s body inside and to everyone concerned; the Mayor was killed by the assistant during the press conference. Chandram, then walks away to start a new life with the reporter.

==Cast==
* Kamal Haasan - Mayor G. K. Rayudu and Chandram (Dual role)
* Vijayashanti - News reporter
* Srividya - Wife of Rayudu
* Nagesh
* Charan Raj - Assistant of Mayor Rayudu Jayalalitha
* P. L. Narayana
* Gollapudi Maruthi Rao
* E. V. V. Satyanarayana - Police inspector

== Soundtrack ==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    = 22:54

| all_writing     =
| all_lyrics      =
| all_music       = Ilaiyaraaja

| writing_credits =
| lyrics_credits  = yes
| music_credits   =
| title1          = College Agelo Teenage Mojulo
| note1           =
| writer1         =
| lyrics1         = Veturi Sundararama Murthy
| music1          =
| extra1          = S. P. Balasubramaniam, P. Susheela
| length1         = 4:05

| title2          = Dora Dora Donga Muddu
| note2           =
| writer2         =
| lyrics2         = Veturi Sundararama Murthy
| music2          =
| extra2          = S. P. Balasubramaniam, S. Janaki
| length2         = 4:46

| title3          = Laali Jo Laali Jo Ooruko Papayi
| note3           =
| writer3         =
| lyrics3         = Sirivennela Sitaramasastri
| music3          =
| extra3          = S. P. Balasubramaniam
| length3         = 4:47

| title4          = Nachina Fuddu Vechani Beddu
| note4           =
| writer4         =
| lyrics4         = Sirivennela Sitaramasastri
| music4          =
| extra4          = S. P. Balasubramaniam
| length4         = 4:33

| title5          = Sandhya Ragapu Sarigamalo
| note5           =
| writer5         =
| lyrics5         = Veturi Sundararama Murthy
| music5          =
| extra5          = S. P. Balasubramaniam, S. Janaki
| length5         = 4:43

| title6          =
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          =
| length6         =
| title7          =
| note7           =
| writer7         =
| lyrics7         =
| music7          =
| extra7          =
| length7         =
| title8          =
| note8           =
| writer8         =
| lyrics8         =
| music8          =
| extra8          =
| length8         =
}}

==Awards==
* Kamal Haasan won Filmfare Award for Best Actor – Telugu in 1989.

==Features==
* For the Tamil dubbing, Kamal Haasan, Srividya and Nagesh dubbed their own voice for their part.
* This is the only film that both Kamal Haasan and Vijayashanti acted together.
* This is second film for director Suresh Krissna with Kamal, the other is Sathya (film)|Sathya and later Aalavandhan. Mano and K. S. Chithra for Tamil version.
* Telugu director E V V Satyanarayana was one of the assistant directors for the film, early in his career.
* Producer Eknaath, popular for his Eknaath Video Magazine programme in the late 80s before he become a film producer, produced the Tamil version.
* The climax of the film was adapted completely from the climax of Moon over Parador. Aboorva Sagotharargal.

==External links==
*  
*  

 

 
 
 
 
 
 
 