Querida Bamako
 

{{Infobox film
| name           = Querida Bamako 
| image          = 
| caption        = 
| director       = Omer Oke, Txarli Llorente
| producer       = Abraprod S.L.
| writer         = 
| starring       = Djédjé Apali, Esther Vallés, Gorsy Edu
| distributor    = 
| released       = 2007
| runtime        = 98
| country        = Spain
| language       = 
| budget         = 
| gross          = 
| screenplay     = Joanes Urkixo
| cinematography = Gaizka Bourgeaud
| sound          = 
| editing        = Iván Miñambres, Haritz Zubillaga
| music          = Pascal Gaigne
}}

Querida Bamako  is a 2007 Spanish film.

== Synopsis ==
Moussa is a young boy from Burkina Faso. He was born and lives in the same village as his parents, his family and his wife, Fatima, although he prefers to call her "Bamako", as he met her there, in the capital of Mali, before they got married and had their baby, Mamadou. Although the land gives them enough to live on, the precarious balance has been upset by a long drought. Driven by the need to feed his family, and having asked the elders’ advice, Moussa decides to immigrate to Europe.

== References ==
 
*  

 
 
 


 