The Longest Hunt
{{Infobox film
 | name =The Longest Hunt 
 | image =   The Longest Hunt.jpg
 | image_size =
 | caption =
 | director = Bruno Corbucci
 | writer =
 | starring =
 | music =  Vasili Kojucharov
 | cinematography =Fausto Zuccoli
 | editing =
 | producer =
 | distributor =
 | released = 1968
 | runtime =
 | awards =
 | country =
 | language =Italian
 | budget =
 }} 1968 Cinema Italian spaghetti western directed by Bruno Corbucci.
 Little Tony.    Alex Cox in his book 10,000 Ways to Die refers to the film as "derivative and boring", "a reminder that talent as a director is not inheritable, or a family trait." 

==Plot==
US-American adventurer Stark has been sentenced to death in a Mexican village. A rich Mexican rancher saves him from getting hanged but he must return the favour by saving the ranchers son Fidel. He is told that Fidel has somehow been persuaded or even forced to join a gang. Stark shall bring Fidel back to his father. The American believes that the rancher is worried sick about his sons well-being. When he delivers Fidel he understands just on time that things are very different. The rancher intends to whitewash his honour by getting rid of Fidel because hes ashamed of having an illegitimate son.

== Cast == Brian Kelly as Stark
* Keenan Wynn as Major Charlie Doneghan
* Erika Blanc as Jocelyn
* Folco Lulli as Don Hernando Gutierrez
* Fabrizio Moroni as Fidel
* Linda Sini as Dona Sol Gutierrez
* Krista Nell as Sheila
*Rik Battaglia as Cpt. Norton
*Luigi Bonos as Sgt. Peck
*Enzo Andronico as Gunther
* Ignazio Leone as Doctor
* Luca Sportelli

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 