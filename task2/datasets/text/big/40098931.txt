Magic in the Moonlight
 
{{Infobox film
| name           = Magic in the Moonlight
| image          = Magic in the Moonlight poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Woody Allen
| producer       = Letty Aronson Stephen Tenenbaum Helen Robin
| writer         = Woody Allen
| starring       = Colin Firth Emma Stone Hamish Linklater Marcia Gay Harden Jacki Weaver Erica Leerhsen Eileen Atkins Simon McBurney
| music          = 
| cinematography = Darius Khondji
| editing        = Alisa Lepselter
| studio         = Perdido Productions
| distributor    = Sony Pictures Classics
| released       = July 25, 2014 August 15, 2014
| runtime        = 97 minutes  
| country        = United States
| language       = English
| budget         = $16.8 million   
| gross          = $32.3 million   
}}
Magic in the Moonlight is a 2014 American romantic comedy film written and directed by Woody Allen. The film stars Colin Firth, Emma Stone, Hamish Linklater, Marcia Gay Harden, Jacki Weaver, Erica Leerhsen, Eileen Atkins, and Simon McBurney. Set in the 1920s on the French Riviera, the film was released on July 25, 2014, by Sony Pictures Classics.

Magic in the Moonlight received a generally mixed reception. Critics praised the performances of Firth and Stone, but criticized the clichéd scriptwriting.

==Plot==
 
In 1928, a globally famous Magic (illusion)|illusionist, Wei Ling Soo, performs in front of a crowd in Berlin with his world-class magic act. As he walks off stage the film audience sees that he is actually a British man named Stanley (Colin Firth). He berates his employees and is generally curmudgeonly towards his well-wishers. In his dressing-room, he is greeted by old friend and fellow illusionist Howard Burkan (Simon McBurney). Howard enlists Stanley to go with him to the French Riviera|Côte dAzur where a rich American family, the Catledges, has apparently been taken in by a clairvoyant and mystic, Sophie (Emma Stone). In fact, the son of the family, Brice (Hamish Linklater), is smitten with Sophie, and his sister Caroline (Erica Leerhsen) and brother-in-law George (Jeremy Shamos) are concerned Brice is considering proposing marriage. Howard says that he has been unable to uncover the secrets behind her tricks and he admits that the more he watched her the more he believed she really has supernatural powers. So he would like Stanley, who has debunked charlatan mystics in the past, to help him prove she is a fraud.

Howard and Stanley travel to the French Riviera, but Stanley is soon astonished by Sophies ability to go into a fugue state and apparently pull out highly personal details about him and his family. Stanley witnesses a seance in which Sophie communicates with the deceased patriarch of the American family. A candle floats up from the table and Howard grabs it to try to discern what trickery is at play, but is astounded to find no apparent subterfuge. Stanley begins spending time with Sophie. He takes her to visit his aunt and they drive a convertible along the picturesque rocky corniches. When caught in a rain storm, they end up at an observatory that Stanley had visited as a child. After the rain subsides, they open the roof up and view the stars.

When Stanley and Sophie visit his aunt Vanessa (Eileen Atkins), Sophie is seemingly able, after holding aunt Vanessas pearls, to somehow relate secret details of Vanessas one great love affair. This finally convinces Stanley of Sophies authenticity and he has an emotional epiphany, feeling that his lifelong rationalism and cynicism have been misguided.

At a The Great Gatsby|Gatsby-esque party, Stanley and Sophie dance. As they walk together later that night, Sophie asks him if he has felt any feelings for her "as a woman". Stanley, who has admired her talents as a mystic and is grateful to her for opening his eyes to a new worldview, is taken aback and admits that he has not thought of her that way. She leaves upset. The next day Stanley holds a press conference to tell the world that he, who spent his life debunking charlatan mystics, has finally come to find one who is the real deal. The reporters drill him with questions, but the grilling is interrupted when he receives news his aunt Vanessa has been in a car accident.

Stanley rushes to the hospital, and in an emotional scene in a waiting room considers turning to prayer for solace. That is, if he now has come to believe in divination and mysticism, perhaps he should believe in God and prayer. He begins to pray for a miracle to save his aunt, but is unable to go through with it. The rationality that has been his whole life comes back and he rejects prayer, the supernatural and by extension, Sophie and her powers. He decides once more to prove she is a fraud.

Using a trick seen earlier in his stage act, Stanley appears to leave the room but stays to overhear Sophie and Howard discuss their collusion in what has been an elaborate ruse. He discovers that Sophie was able to know so much about him and his aunt because she and Howard collaborated to fool Stanley. Sophie was indeed a charlatan tricking the rich American family and was quickly discovered by Howard. Rather than unmask her and stop the ruse, he enlisted Sophie to help him one-up his best friend and rival, Stanley.

Stanley is initially angry at Howard and Sophie but decides to forgive them. In a conversation with his aunt Vanessa, who by then has recovered from her car accident, Stanley comes to the self-realization that he is in love with Sophie. He finds her and asks her not to marry Brice, but marry him instead. Sophie is taken aback and finds his haughty, awkward proposal unsuitable. She tells him she still plans to marry the wealthy Brice. Returning dejected to his aunt Vanessas, Stanley is surprised when Sophie follows him there and he proposes. They embrace and kiss as the film ends.

==Cast==
* Colin Firth as Stanley Crawford 
* Emma Stone as Sophie Baker 
* Hamish Linklater as Brice 
* Marcia Gay Harden as Mrs. Baker 
* Jacki Weaver as Grace 
* Erica Leerhsen as Caroline
* Eileen Atkins as Aunt Vanessa 
* Simon McBurney as Howard Burkan
* Lionel Abelanski as the Doctor

==Production==
In April 2013, Colin Firth and Emma Stone joined the cast of the film.          In July, they were joined by Jacki Weaver, Marcia Gay Harden, and Hamish Linklater,       and Allen began shooting in Nice, France.   

 

===Music===
Soundtrack 
#"You Do Something to Me" by Cole Porter, performed by Leo Reisman and His Orchestra
#"Its All a Swindle" ("Alles Schwindel") by Mischa Spoliansky and  , performed by Ute Lemper
#"Mack the Knife|Moritat" from The Threepenny Opera by Kurt Weill and Bertolt Brecht, performed by Conal Fowkes Joseph A. Nat Shilkret and His Orchestra
#"Big Boy" by Milton Ager and Jack Yellen, performed by Bix Beiderbecke A Connecticut Yankee by Richard Rodgers and Lorenz Hart, performed by Bix Beiderbecke
#"Sorry" by Raymond Klages, performed by Bix Beiderbecke & His Gang
#"The Sheik of Araby" by Harry B. Smith, Francis Wheeler and Ted Snyder, performed by Sidney De Paris and De Paris Brothers Orchestra
#"Chinatown, My Chinatown" by William Jerome and Jean Schwartz, performed by the Firehouse Five Plus Two
#"Remember Me" by Sonny Miller , performed by Al Bowlly
#"Charleston (song)|Charleston" by James P. Johnson and R. C. McPherson, performed by Paul Whiteman & His Orchestra
#"Sweet Georgia Brown" by Ben Bernie, Maceo Pinkard and Kenneth Casey, performed by The California Ramblers
#"You Call It Madness (But I Call It Love)" by Con Conrad, Gladys DuBois, Russ Colombo and Paul Gregory, performed by Smith Ballew and His Piping Rock Orchestra Anthony S. Edwin B. Edwards, performed by Bix Beiderbecke & His Gang
#"It All Depends on You" by Ray Henderson, Lew Brown and Buddy DeSylva|B. G. DeSylva, performed by Ruth Etting
#"Ill Get By (As Long as I Have You)" by Fred E. Ahlert and Roy Turk, performed by Conal Fowkes

Used in the film but not on the soundtrack are: 
*"The Adoration of the Earth" from The Rite of Spring by Igor Stravinsky, performed by the London Festival Orchestra
*Boléro by Maurice Ravel, performed by the Royal Philharmonic Orchestra movement from Symphony No. 9 in D minor by Ludwig van Beethoven, performed by the Royal Philharmonic Orchestra
*"Thou Swell" from A Connecticut Yankee by Richard Rodgers and Lorenz Hart, performed by Cynthia Sayer and Hamish Linklater Joseph McCarthy, performed by Cynthia Sayer and Hamish Linklater
*"Who? (song)|Who?" from Sunny (musical)|Sunny by Oscar Hammerstein II, Otto Harbach and Jerome Kern, performed by David ONeal and Hamish Linklater

==Release==
The film was set to be released on July 25, 2014.   On October 17, 2013, it was announced that FilmNation Entertainment would handle the international sales for the film  and Sony Pictures Classics had acquired North American distribution rights to it.  On July 25, 2014, the film opened in seventeen US theaters.  and expanded nationwide in the US on August 15, 2014. 

==Reception==

===Critical reception===

Magic in the Moonlight has received mixed reviews from critics. On   assigns the film a score of 54 out of 100, based on 40 critics, indicating "mixed or average reviews". 

 .  In The Wall Street Journal, Joe Morgenstern complimented Emma Stone and concluded, "Think of it as a 97-minute séance that draws you in, spins you around, subverts your suppositions, levitates your spirits and leaves you giddy with delight" Joe Morgenstern,  , The Wall Street Journal, July 24, 2014 

However, in   disliked the film, criticizing its familiarity to Allens previous work and believing the writing was uninspired.  Chris Nashawaty of Entertainment Weekly gave the movie a "B-" grade (from A+ to F), remarking that it was funny and "pleasant" but also forgettable.  Salon (website)|Salons Andrew OHehir felt that the characters were not drawn out enough because of poor writing. 

Writing for the Catholic News Agency, Carl Kozlowski pointed out that Firths skepticism about God was "very much in keeping with Allens openly atheistic worldview." Carl Kozlowski,  , Catholic News Agency, September 05, 2014 

===Box office===
The film opened in limited release in North America on July 25, 2014. In 17 theaters, it grossed $412,095 ($24,241 per screen) in its opening weekend.  It expended to 964 theaters on August 15, grossing $1,786,150 ($1,853 per screen) in three days  . By the end of its North American run, it grossed $10,539,326  .

Overseas, the film earned $21,800,000, for a worldwide gross of $32,339,326.  

==Marketing== trailer was released on May 21, 2014. 

 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 