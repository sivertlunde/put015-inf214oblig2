Uuno Turhapuro armeijan leivissä
{{Infobox film
| name          = Uuno Turhapuro armeijan leivissä
| image         = Uuno turhapuro armeijan leivissa cover.jpg
| caption       = DVD cover with Uuno leaning against an ancient gun
| director      = Ere Kokkonen
| writer        = Ere Kokkonen
| starring      = Vesa-Matti Loiri, Marjatta Raita, Spede Pasanen, Simo Salminen
| producer      = Spede Pasanen
| distributor   = Filmituotanto Spede Pasanen Ky
| released      = 1984
| runtime       = 1h 42min
| language      = Finnish
}} Finnish comedy and the ninth film in the Uuno Turhapuro film series starring Vesa-Matti Loiri as the title character, Spede Pasanen as the potty-mouthed mechanic Härski Hartikainen, and Simo Salminen as their constant companion Sörsselssön. It is to date the highest-grossing Finnish comedy and not too surprisingly the most well known in the Turhapuro series. It remains Finlands most seen domestic film made since 1968  , however the official statistics by the Finnish Film Foundation began in 1972, so all figures before that are estimates (although believed to be true). Thus this film is the most seen domestic film in the years with official statistical records (1972-today), and 7th in the Top20 of all time.

==Plot==
Uuno is forced to complete his mandatory military service when it is revealed that he only spent one day in the army in his youth. As is typical of the Turhapuro series, his family and friends become closely tied in with these events. His friends Härski Hartikainen and Sörsselssön return to the army for a refresher course and by chance Uunos father-in-law, Councillor Tuura is made the Finnish Defence Minister.

In one of the most memorable scenes, Uunos wife, Elisabeth, dresses as Uuno and substitutes him for a day as Uuno has an apparently urgent meeting (at a restaurant) and while becoming lost in the woods with a malfunctioning radio, Tuura accidentally declares war on Sweden.

==External links==
* 

 

 
 
 
 
 
 


 
 