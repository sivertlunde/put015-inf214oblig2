Downtown 81
{{Infobox film
|name=Downtown 81
|director=Edo Bertoglio
|writer=Glenn OBrien 
|producer=Glenn OBrien
|starring=Jean-Michel Basquiat Anne Carlisle
|released=1981 English
|country=United States
}}

Downtown 81 (a.k.a. New York Beat Movie) is a film that was shot in 1980-1981.

This film, directed by Edo Bertoglio, written and produced by Glenn OBrien with post-production in 1999-2000 by  Maripol, is a rare real-life snapshot of ultra-hip subculture of post-punk era Manhattan. Starring renowned artist Jean-Michel Basquiat and featuring such Village artists as James Chance, Amos Poe, Walter Steding, and Tav Falco, the film is a bizarre elliptical urban fairytale.  In 1999,  Michael Zilkha, founder of ZE records (the label of several of the films artists), became the films Executive Producer.

== Synopsis ==
The film opens with Jean (Basquiat) in the hospital with an undisclosed ailment. After checking out, he happens upon an enigmatic woman, Beatrice (Anna Schroeder), who drives around in a convertible. He arrives at his apartment only to discover that his landlord, played by former Yardbirds manager Giorgio Gomelsky, is evicting him.
 DNA to David McDermott to graffiti artists Lee Quinones and Fab Five Freddy. Jean eventually does manage to sell some of his art work to a rich middle-aged woman who is interested in more than just his art, but she pays with a check. As the film progresses, he wanders the streets of New York City, looking for Beatrice. He catches performances by Kid Creole and the Coconuts and James White and the Blacks. Finally he happens upon a bag lady (Debbie Harry) who turns into a princess when he kisses her. As a reward, she gives him a stack of cash.

== Parallels to real life ==
Writer Glenn O’Brien, who knew Basquiat from his  , and had adopted his technique of having actors essentially play themselves with a minimal plot. “The film is an exaggerated version of life” he said. 

Jean-Michel Basquiat was homeless at the time of the movie, and slept in the production office during most of the shooting.  The film production bought Basquiat canvas and paints to make paintings for the film. The paintings that appear in the movie belonging to Basquiat’s character are by Basquiat himself, and among his first canvases. Eric Fretz.  . Greenwood Press, 2010. 

Debbie Harry (who plays the fairy princess who gives him money), and her boyfriend Chris Stein, both of the band Blondie (band)|Blondie, bought a painting of Basquiat’s for $200 after the end of shooting. 

== Production ==
New York Beat was shot over December 1980 and January 1981. It was initially funded by Fiorucci and RCS MediaGroup|Rizzoli, but the movie was abandoned in the mid-80s due to financial problems. Producers OBrien and Maripol resurrected the film after acquiring the rights in 1999 (over a decade after Basquiats death). It was released in 2000 as Downtown 81.

The dialogue audio for the film was lost, so actor Saul Williams dubbed the late Basquiats voice. However, the musical soundtrack, mostly live club performances recorded on location using a RCA 24 track mobile unit, survived.

== Soundtrack == the Plastics, Marvin Pontiac, Kenny Burrell, the Specials, Chris Stein, Melle Mel with Blondie (band)|Blondie, Kid Creole and the Coconuts, James White and the Blacks, Vincent Gallo, Lydia Lunch, Steve French and Suicide (band)|Suicide.
Many of the recordings were of live performances, but DNA and Tuxedomoon were recorded in the studio for the soundtrack.

== Reviews ==
After it premiered as Downtown 81 at the 2000 Cannes Film Festival reviews were mostly favorable. Variety called it “an extraordinary real-life snapshot of hip, arty, clubland Manhattan in the post-punk era.” 

A rare movie review in Artforum said "Basquiat is a joy to watch. He floats through the movie with cool grace and unflagging energy; hes a natural in front of the lens..." Mike McGonigal. " ," Artforum, Summer 2000. 

British Art Critic Adrian Searle wrote that “Downtown 81 captures that New York moment when punk, emerging rap, art school cool and the East Village art and music scenes were at their creative best.” 

While the main appeal of the film seems to have been the art and music, some commentators also appreciated giving the modern viewer a peek at the decimated Lower East Side of 1980, saying "the real star of the film is the gritty milieu of a New York long gone",  and that "New York Beat...conveys the vast gulf between Manhattan’s rich and the forgotten corners of the city, and the marginal existence of the artistic underground who tried to survive in between these worlds." 

 

== References ==
 

==Further reading==
* Edo Bertoglio (director). Downtown ’81. Written by Glenn O’Brien; produced by Glenn OBrien and Patrick Montgomery.  Post-production by Maripol. Executive Producer Michael Zilkha.  Zeitgeist Films, 2000. 72 mins.
* Jeffery Deitch, Diego Cortez, and Glenn O’Brien. Jean-Michel Basquiat 1981: the Studio of the Street, Milan: Charta, 2007.
* Anthony Haden-Guest. "The Roving Eye"   .

== External links ==
*  
*  

 
 
 
 