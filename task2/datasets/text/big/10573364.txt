I Will Not Confess
{{Infobox film
| name           = I Will Not Confess
| image          =
| image_size     =
| caption        = Amal (Faten Hamama) and her brother (Ahmed Ramzy) in Lan Aataref
| director       = Kamal El Sheikh
| producer       = Helmy Rafla
| writer         = Zaki Saleh
| narrator       = 
| starring       = Faten Hamama Ahmed Mazhar Ahmed Ramzy
| music          = 
| cinematography = Abdel Halim Nasr
| editing        = Hussein Ahmed
| distributor    = 
| released       = October 16, 1961
| runtime        = 120 minutes
| country        = Egypt Arabic
| budget         = 
}}
 1961 Egyptian Egyptian film director Kamal El Sheikh.   

== Plot ==
Amal (Faten Hamama) is the wife of Ahmed (Ahmed Mazhar) who manages a textile factory. One day, she receives a threatening letter directed at Ahmed. The letter reveals that Ahmed committed a murder; he killed a teller to steal the factorys money. Amal remembers that her husband had come to her telling her that he had lost his money, and weeks later had restored his money. One of the factory workers (Salah Mansour) meets Amal and confirms what the letter said, claiming that he witnessed the crime.       

Amal seeks her brothers (Ahmed Ramzy) support. He goes to the worker and, after a heated and vehement argument, fights with him and eventually kills him. He immediately runs away. Minutes later, Amal reaches the workers home to talk to him, and finds him dead. She is blamed for the crime. Amal does not tell anybody from her family. Her health conditions worsens and is hospitalized. Amals brother confesses his crime. Her husband surrenders himself and confesses that he committed the crime saying that he only did all that to secure a better life for his wife.   

== Cast ==
*Faten Hamama as Amal
*Ahmed Mazhar as Ahmed
*Ahmed Ramzy as Amals brother
*Salah Mansour
*Sherifa Maher
*Nazim Shaarawy

== References ==
 

== External links ==
* 

 
 
 
 
 

 
 