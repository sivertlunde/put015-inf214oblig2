Fever Pitch (1997 film)
{{Infobox Film
| name = Fever Pitch
| image = 
| caption = 
| director = David Evans	
| producer = Amanda Posey
| screenplay = Nick Hornby
| based on =  
| starring = Colin Firth Ruth Gemmell Mark Strong Neil Pearson
| music = Boo Hewerdine Neil MacColl
| cinematography = Chris Seager
| editing = Scott Thomas Channel 4 Films 
| distributor = Phaedra Cinema
| released = 4 April 1997
| runtime = 102 minutes
| country = United Kingdom
| language = English 
| budget = 
| gross =
}}
 book of the same name by Nick Hornby.

==Synopsis== First Division Liverpool in final game Michael Thomas last-minute goal giving Arsenal the 2–0 win they needed to win the title.

==Cast==
* Colin Firth as Paul Ashworth
* Ruth Gemmell as Sarah Hughes
* Mark Strong as Steve
* Neil Pearson as Mr. Ashworth
* Lorraine Ashbourne as Mrs. Ashworth
* Holly Aird as Jo
* Stephen Rea as Ray, the governor
* Emily Conway as Sasha 
* Richard Claxton as Robert Parker
* Andy Raines as football referee

==Production== terracing at Highbury had since been replaced, the scenes of fans on the terraces were instead filmed at Fulham F.C.|Fulhams Craven Cottage stadium.

==Remake== Fever Pitch starring Jimmy Fallon and Drew Barrymore, with the 2004 World Series|World-Series-winning 2004 Boston Red Sox replacing Arsenal.
To avoid confusion, this film is known as The Perfect Catch in the U.K.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 