Banana da Terra
{{Infobox film
| name =  Banana da Terra
| image = Carmen Miranda, Banana da Terra 1939.jpg
| caption = Carmen Miranda in the movie scene Banana da Terra, 1939.
| director = Ruy Costa	
| producer = Alberto Byington Jr. Wallace Downey	
| writer = Mário Lago João de Barro	
| starring =
| music = 
| cinematography = Edgar Brasil	
| editing = Ruy Costa (as E.Sá)	  
| studio = 
| distributor = Sonofilmes 
| released = February 10, 1939
| runtime = 
| country = Brazil Portuguese
| budget =
| gross =
| website =
}}
 musical film from 1939, produced by Wallace Downey, with a screenplay by João de Barro and Mário Lago and direction of Ruy Costa. It was in this film that Carmen Miranda first appeared dressed as a "baiana". 

== Production ==
In 1939 Sonofilmes released the musical comedy Banana da Terra, which like many of its musical predecessors, belonged to the tradition of carnival films that included hit songs and were released just before the annual celebrations. According to the Jornal do Brasil newspaper, Banana da Terra was to premiere on 10 February at the MGM-owned cinemas in Rio and São Paulo, the Pedro II cinema in Petrópolis, in the state of Rio de Janeiro, and the Guarani cinema in Salvador, Bahia, as well as in Recife, Porto Alegre and Ribeirão Preto. Negotiations were also underway to show the film in the state capitals Curitiba and Belo Horizonte. Thanks to the links between Alberto Byington Jr, Wallace Downeys associate, and Hollywood, this Sonofilmes production was distributed by MGM in Brazil, and consequently premiered in the luxurious Metro Passeio in Rio. {{cite news|url=http://books.google.com.br/books?id=b-B9AwAAQBAJ&pg=PA125&dq=Banana+da+Terra+1939&hl=pt-BR&sa=X&ei=86IEVPCQMIvisATsrYLgAQ&ved=0CD4Q6AEwBA#v=onepage&q=Banana%20da%20Terra%201939&f=false|title=Brazilian National Cinema
|date=|work=Lisa Shaw & Stephanie Dennison|page=|accessdate=September 1, 2014}} 

Banana da Terra proved to be a great commercial success and to markedly influence the chanchada tradition, not least by combining self-deprecating humour with a Tongue-in-cheek |tongue-in-cheek critique of Hollywood clichés. {{cite book|url=http://books.google.com.br/books?id=gZJvAAAAQBAJ&pg=PT30&dq=Banana+da+Terra+1939&hl=pt-BR&sa=X&ei=86IEVPCQMIvisATsrYLgAQ&ved=0CDAQ6AEwAg#v=onepage&q=Banana%20da%20Terra%201939&f=false|title=The International Film Musical
|date=|author=Creekmur, Corey; Mokdad, Linda|page=|accessdate=September 1, 2014}} 

The plot of movie, first and foremost a construct to link together the various musical numbers, revolves around the imaginary Pacific island of Bananolândia, an allegorical tropical paradise, which was faced with the problem of a surplus of bananas. In this self-parodic comedy Brazil adopts the reflected identity of the exotic island of plenty.
 O que é que a baiana tem?", dresses in the "baiana" costume, in keeping with the songs lyrics.  It is said that the composer went to Mirandas house where he taught her performance. But it was Miranda who made the look her own and used it to launch her international career as the embodiment of a pan-Latin American identity. Banana da Terra was to be Mirandas last Brazilian film; it was when performing its hit song "O que é que a baiana tem?" at Rios Urca casino that she was "discovered" by the show business impresario Lee Shubert and taken to Broadway, and subsequently to Hollywood. 

== Plot ==
In Banana da Terra the actor Oscarito plays a man in charge of a publicity campaign for bananas who decides to kidnap the queen of Bananolândia, played by Dircinha Batista. She is taken to Rio and promptly falls in love with character played by Aloísio de Oliveira, a member of Carmen Mirandas backing group, the Bando da Lua. The action unfolds in the glamorous realm of Rios radio station and casinos, thus providing the perfect pretext for inclusion of a variety of musical numbers. 

== Cast ==
 
*Dircinha Batista 
*Oscarito	
*Aloísio de Oliveira
*Carmen Miranda		
*Aurora Miranda		
*Lauro Borges			
*Jorge Murad		
*Neide Martins	
*Mario Silva			
*Paulo Neto	
*Almirante		
*Alvarenga		
*Fernando Alvarez		
*Ivo Astolphi		
*Castro Barbosa	
*Linda Batista		
*Emilinha Borba	
*Oswaldo de Moraes Eboli		
*Carlos Galhardo	
*Hélio Jordão	
*Barbosa Júnior		
*César Ladeira		
*Virgínia Lane	
*Afonso Osório	
*Stênio Osório	
*Ranchinho		
*Linda Rodrigues	
*Orlando Silva	
*Romeu Silva	
*Napoleão Tavares	
 

== Musical Numbers ==
* O Que é Que a Baiana Tem? ... Performed by Carmen Miranda
* Pirulito ... Performed by Carmen Miranda and Almirante
* Menina do Regimento ... Performed by Aurora Miranda
* A Tirolesa ... Performed by Dircinha Batista
* Eu Vou Pra Farra ... Performed by Bando da Lua
* Não Sei Por Quê ... Performed by Bando da Lua
* Sei Que É Covardia, Mas ... Performed by Carlos Galhardo
* Sem Banana ... Performed by Carlos Galhardo
* Mares da China ... Performed by Carlos Galhardo
* Amei Demais ... Performed by Castro Barbosa
* A Jardineira ... Performed by Orlando Silva 

== Availability == O que é que a baiana tem?" survived to the time.

==References==
 

==External links==
* 

 
 
 
 
 
 
 