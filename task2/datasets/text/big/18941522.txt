The Locked Door
{{Infobox film
| name           = The Locked Door
| image          = StanwyckLockedDoor1929.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = George Fitzmaurice
| producer       = {{Plainlist|
* Joseph M. Schenck
* Joseph P. Kennedy
}}
| writer         = {{Plainlist|
* George Scarborough
* Earle Browne
}}
| screenplay     = C. Gardner Sullivan
| based on       =  
| starring       = {{Plainlist|
* Rod LaRocque
* Barbara Stanwyck
* William "Stage" Boyd
* Betty Bronson
}}
| music          = 
| cinematography = Ray June
| editing        = Hal Kern
| studio         = Feature Productions
| distributor    = United Artists
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Channing Pollock.  The play was first adapted for the screen in 1921 as The Sign on the Door, starring Norma Talmadge.  The Locked Door was Barbara Stanwycks second film appearance, first starring role, and first talking picture.

==Plot== 12 mile limit to get around the restrictions of Prohibition. Worse, Frank turns out to be a cad. When she tries to leave, he locks the door and tries to force himself on her, tearing her dress. Fortunately, the ship drifts back into U.S. waters and a police raid stops him from going any further. When a photographer takes a picture of the two under arrest, Frank buys it from him.

Eighteen months later, Ann is happily married to wealthy Lawrence Reagan (William "Stage" Boyd).
They are about to celebrate their first wedding anniversary when Frank resurfaces in Anns life, this time as the boyfriend of her naive young sister-in-law, Helen (Betty Bronson). Though both Ann and her husband tell Helen that Frank is no good (Lawrence knows that Frank is having an affair with the wife of one of his friends), it is clear to Ann that Helen does not believe them.

Ann goes to Franks apartment to stop him from taking advantage of Helen. She hides when Lawrence shows up unexpectedly. He warns Frank to leave town before Lawrences friend catches up with him and shoots him. Frank had already planned to go, but when Lawrence declares that he intends to administer a beating first, Frank draws a gun. He is shot in the ensuing struggle. Lawrence leaves without being seen, unaware that his wife has heard the whole thing.

To protect her husband, Ann phones the switchboard operator and reenacts her earlier assault, ending with her firing two shots. When the police arrive, the district attorney (Harry Mestayer) soon pokes holes in her story. Also, the photograph is found, providing a motive for murder. However, Frank is not yet dead; in his last few minutes of life, he explains what really happened, exonerating both Ann and Lawrence.

==Cast==
*Rod LaRocque as Frank Devereaux
*Barbara Stanwyck as Ann Carter
*William "Stage" Boyd as Lawrence Reagan
*Betty Bronson as Helen Reagan Harry Stubbs as the Waiter
*Harry Mestayer as the District Attorney
*Mack Swain as the Hotel Proprietor
*Zazu Pitts as the Telephone Girl
*George Bunny as the Valet
*Purnell Pratt as Police officer
*Fred Warren as Photographer
*Charles Sullivan as Guest 
*Edgar Dearing as Policeman
*Mary Ashcroft as Rum boat girl
*Violet Bird as Rum boat girl
*Eleanor Fredericks as Rum boat girl
*Martha Stewart as Rum boat girl
*Virginia McFadden as Rum boat girl
*Lita Chevret as Rum boat girl
*Leona Leigh as Rum boat girl
*Greta von Rue as Rum boat girl
*Dorothy Gowan as Rum boat girl
*Kay English as Rum boat girl Edward Dillon

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 