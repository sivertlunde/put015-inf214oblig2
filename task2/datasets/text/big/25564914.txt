Uncle Hyacynth
 
{{Infobox film
| name           = Uncle Hyacynth
| image	         = Uncle Hyacynth FilmPoster.jpeg
| caption        = A poster bearing the films Spanish release title: Mi tío Jacinto
| director       = Ladislao Vajda
| producer       = Raoul Ploquin
| writer         = Andrés Laszlo Ladislao Vajda
| starring       = Pablito Calvo
| music          = 
| cinematography = Heinrich Gärtner
| editing        = Julio Peña
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}
 Golden Bear (Audience Award) for his interpretation in this film.   

==Cast==
* Pablito Calvo - Pepote Antonio Vico - Jacinto
* José Marco Davó - Police inspector Juan Calvo - Second-hand-clothes dealer
* Mariano Azaña - Match seller 
* Pastora Peña - Stamp seller
* Julio Sanjuán - Organ grinder 
* Miguel Gila - Paco the con man
* Giulio Battiferri - Milkman
* José Isbert - Sánchez the watch forger
* Adriano Domínguez - Police officer
* Juan Calvo Domenech - Tailor
* Luis Sánchez Polack - Tailors clerk
* Joaquín Portillo Top - Pacos partner in swindle
* José María Lado
* Rafael Bardem - Art agent
* Paolo Stoppa - Art forger
* Pedro Porcel - 1# Client

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 