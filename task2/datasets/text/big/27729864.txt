Ninety Degrees in the Shade
 
 
{{Infobox film
| name           = Ninety Degrees in the Shade
| image          =
| caption        = 
| director       = Jiří Weiss 
| producer       = Raymond Stross David Mercer Jiří Mucha Jirí Weiss 
| starring       = James Booth Anne Heywood
| music          = Luděk Hulan
| cinematography = Bedrich Batka Russell Lloyd
| distributor    = Unger Films (USA)
| released       = 1965
| runtime        = 90 minutes
| country        = United Kingdom Czech Republic
| awards         =
| language       = English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    =
}}
 1965 Cinema Czech drama film directed by Jiří Weiss. 

==Plot==
A woman attempts to cover up the thefts committed by the manager at the shop she works at.

==Cast==
*James Booth.....Vorell
*Anne Heywood.....Alena
*Rudolf Hrusínský.....Mr. Kurka
*Ann Todd.....Mrs. Kurka
*Donald Wolfit.....Bazant
*Ladislav Potmesil.....Jirka Kurka
*Vera Tichánková.....Vavrova
*Jiřina Jirásková.....Vera
*Jorga Kotrbová.....Hanka
*Jan Libícek.....Man in Booth

==Awards and nominations==
Berlin International Film Festival
*Won, "UNICRIT Award" - Jirí Weiss

Golden Globe Award
*Nominated, "Best English-Language Foreign Film"

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 


 
 