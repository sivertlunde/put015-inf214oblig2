Best (film)
{{Infobox Film
| name           = Best
| image          =
| image_size     =
| caption        =
| director       = Mary McGuckian
| producer       = John Lynch, Mary McGuckian
| narrator       =
| starring       = John Lynch
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1 May 2000 (premiere) 12 May 2000 (general release)
| runtime        = 102 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}}
 Northern Irish soccer star George Best, particularly his years spent at Manchester United. It was directed by Mary McGuckian.

==Cast== John Lynch - George Best Sir Matt Busby
*Jerome Flynn - Bobby Charlton
*Ian Hart - Nobby Stiles
*Patsy Kensit - Anna
*Cal Macaninch - Paddy
*Linus Roache - Denis Law
*Adrian Lester - Rocky
*David Hayman - Tommy Docherty/The Barman
*James Ellis - Dickie Beal
*Roger Daltrey - Rodney Marsh
*Clive Anderson - Interviewer
*Sophie Dahl - Eva Haraldsted
*Stephen Fry - Frazer Crane
*Dave Duffy - Limosine Driver
*Neil Caple - Barber
*Nick Wall - Photographer
*John McCarthy - Hairdresser

==Filming locations==
*Isle Of Man
*Liverpool, Merseyside, England UK
*Saints RLFC, Knowsley Road, St. Helens, Merseyside|St. Helens, Merseyside, England UK

==Release dates==
*UK - 1 May 2000 (Belfast Premiere)
*UK - 12 May 2000 (General Release)
*Israel - 1 June 2000
*USA - 15 September 2000 (Temecula Valley International Film Festival)
*Iceland - 28 March 2001 (Video Premiere)
*Singapore - 26 July 2001
*Italy - 10 May 2002
*Netherlands - 6 April 2004 (DVD Premiere)

==Awards==
===Ft. Launderdale International Film Festival=== John Lynch (tied with Dirk Roofthooft & Pleure pas Germaine)

===Temecula Valley International Film Festival===
Best Foreign Film - Mary McGuckain

==External links==
* 

 
 
 
 
 

 
 