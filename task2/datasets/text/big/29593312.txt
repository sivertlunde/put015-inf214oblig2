Farewell to the Land
 
{{Infobox film
| name           = Farewell to the Land
| image          = Farewell to the Land.jpg
| caption        = Poster for Farewell to the Land.
| director       = Mitsuo Yanagimachi
| producer       = Michiko Ikeda Tetsuya Ikeda Mitsuo Yanagimachi
| writer         = Mitsuo Yanagimachi
| starring       = Jinpachi Nezu
| music          = 
| cinematography = Masaki Tamura
| editing        = Sachiko Yamaji
| distributor    = 
| released       =  
| runtime        = 130 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

  is a 1982 Japanese drama film directed by Mitsuo Yanagimachi. It was entered into the 32nd Berlin International Film Festival.   

==Cast==
* Jinpachi Nezu as Yukio Yamazawa
* Kumiko Akiyoshi as Junko
* Jirō Chiba as Akihiko Yamazawa (as Jirō Yabuki)
* Miyako Yamaguchi as Fumie Yamazawa
* Gō Awazu as Driver
* Sumiko Hidaka as Ine Yamazawa
* Yudai Ishiyama as Manager
* Keizō Kanie as Daijin
* Nenji Kobayashi as Farmer
* Kōjirō Kusanagi as Takejirō Yamazawa
* Seiji Matsuyama as Fumies brother
* Yuichi Minato as Office worker
* Aoi Nakajima as Fumiko
* Rei Okamoto as Taiwanese woman
* Kiminobu Okumura as Koichirō Yamazawa

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 