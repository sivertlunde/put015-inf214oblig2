They Met in Argentina
{{Infobox film
| name           = They Met in Argentina
| image          =
| caption        =
| director       = Leslie Goodwins Jack Hively
| producer       = Lou Brock
| writer         = Jerome Cady
| story          = Lou Brock James Ellison
| music          = Rodgers and Hart
| cinematography = J. Roy Hunt
| editing        = Desmond Marquette RKO Radio Pictures
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         = $500,000 
| gross          =
}} James Ellison), who is attempting to buy a racehorse from her father. It was one of a number of Hollywood films from the 1940s produced to reflect Americas "Good Neighbor policy" towards Latin American countries. They Met in Argentina was not well received by audiences, critics, or the Argentine government.

==Plot== James Ellison) is a Texan in the oil business who travels to Argentina to bid for some land. When his bid is unsuccessful, he teams up with colleague Duke Ferrell (Buddy Ebsen) to buy their employer a successful racehorse, Lucero, in the hope that this will compensate for the failed bid. Tim falls in love with Lolita OShea (Maureen OHara), the daughter of the racehorses owner, Don Enrique (Robert Barrat). Don Enrique is against selling Lucero, but when he realises his daughter is in love with Tim, he offers him the racehorse on the condition that he immediately returns to the USA. When Lolita realises Tim has left, she pursues him on horseback.   

==Background and production== Blood and Sand (1941).    

They Met in Argentina was based on a story by Lou Brock, who was also the films producer. Brock approached Rodgers and Hart to score the production. The pair wrote 12 songs in total, although only 7 of them were included in the final cut.    The songs featured in the soundtrack are "Youve Got the Best of Me", "North America Meets South America", "Amarillo", "Lolita", "Cutting the Cane", "Never Go To Argentina" and "Simpatica".    The dance sequences were choreographed by Frank Veloz.   

==Reception==
===Box Office===
The film fared poorly in cinemas, and made a loss of $270,000.   
===Critical===
It received negative reviews from critics, with Robert Dana in the New York Herald Tribune describing it as "an American musical at its worst".  Film critic Leonard Maltin later described it as a "dismal musical".  The Argentine government spoke out against the distribution of the film in Latin America.   

==References==
 

==External links==
*  at IMDB
* 

 
 
 