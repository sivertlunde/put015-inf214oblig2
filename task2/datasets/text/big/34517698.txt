Affectionately Yours
:for the Hong Kong film see Affectionately Yours (1985 film)
{{Infobox film
| name           = Affectionately Yours
| image          = Affectionately Yours.JPG
| image size     = 
| caption        = Merle Oberon from the film
| director       = Lloyd Bacon
| producer       = 
| writer         = Edward Kaufman (screenplay)
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Warner Brothers
| released       =  
| runtime        = 88 min.
| country        = United States
| language       = English
}}

Affectionately Yours is a 1941 American romantic comedy film directed by Lloyd Bacon.

== Plot summary ==

Foreign correspondent Rickey Mayberry (Dennis Morgan) hurriedly flies back from Spain to the U.S. to keep his wife from divorcing him, but hes followed on the flight by love interest Irene Malcolm (Rita Hayworth).  Mayberrys wife Sue (Merle Oberon) has indeed divorced him, although she still loves him.  The comedy of errors is compounded by Irene and also by Rickeys boss (James Gleason), who both conspire to keep the couple apart.

== Cast ==

* Merle Oberon as Sue Mayberry
* Dennis Morgan as Rickey Mayberry
* Rita Hayworth as Irene Malcolm
* Ralph Bellamy as Owen Wright
* George Tobias as Pasha
* James Gleason as Chet Phillips
* Hattie McDaniel as Cynthia the Cook
* Jerome Cowan as Pappy Cullen
* Butterfly McQueen as Butterfly
* Renie Riano as Mrs. Snell
* Frank Wilcox as Tom Tommy
* Grace Stafford as Chickie Anderson
* Carmen Morales as Anita
* Murray Alper as Blair
* William Haade as Matthews Pat Flaherty as Harmon
* James Flavin as Tomassetti Fred Graham, Craig Stevens)

== External links ==
 
*  
*  

 

 
 
 
 
 
 


 