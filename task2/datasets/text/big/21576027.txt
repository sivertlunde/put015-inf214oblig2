From Time to Time (film)
 
 
 
{{Infobox film
| name           = From Time to Time
| image          = From Time to Time.jpg
| director       = Julian Fellowes
| writer         = Lucy M. Boston Julian Fellowes
| starring       = Maggie Smith Timothy Spall Carice van Houten Alex Etel Eliza Bennett Hugh Bonneville Kwayedza Kureya
| music          = 
| cinematography = 
| editing        = 
| distributor    = Ealing Studios
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
}} The Chimneys Athelhampton Hall, Dorset.

==Plot== press gang.

==Cast==
* Maggie Smith as Mrs. Oldknow
* Alex Etel as Tolly
* Timothy Spall as Boggis
* Pauline Collins as Mrs. Tweedle
* Eliza Bennett as Susan (as Eliza Hope Bennett)
* Rachel Bell as Perkins
* Dominic West as Caxton
* Carice van Houten as Maria Oldknow
* Douglas Booth as Sefton
* Jenny McCracken as Mrs. Gross
* Christine Lohr as Mrs. Robbins
* Allen Leech as Fred Boggis
* Hugh Bonneville as Captain Oldknow
* Kwayedza Kureya as Jacob
* Harriet Walter as Lady Gresham

==Reception== Time Out described the film as "an emotionally wise but logically skewed children’s tale", the logical inconsistencies of which "largely restricts the film’s appeal to bookish pre-teens".  Henry Fitzherbert of the Daily Express praised the actors performances, particularly Smiths, and noted that it "casts a magical spell by the touching conclusion". 

==References==
 

==External links==
*  
*   at Netflix.com

 

 
 
 
 
 
 
 
 
 
 
 
 