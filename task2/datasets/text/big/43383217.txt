Mannukkul Vairam
{{Infobox film
| name           = Mannukkul Vairam
| image          = Mannukkul Vairam.jpg
| caption        =  Manojkumar
| producer       = Motherland Pictures Manojkumar
| Manojkumar
| Sujatha Rajesh Rajesh Murali Murali
| music          = Devendran
| cinematography = K. S. Selvaraj
| editing        = R. Baskaran
| studio         = Motherland Pictures
| distributor    = Motherland Pictures
| released       = 11 Dec 1986
| country        = India
| runtime        = 138 min
| language       = Tamil
}}
 1986 Cinema Indian Tamil Tamil film Rajesh and Murali in lead roles. The film, had musical score by Devendran.    

==Cast==
 
*Sivaji Ganesan Sujatha
*Rajesh Rajesh
*Murali Murali
*Ranjini Ranjini
*Vinu Chakravarthy
*Goundamani
*Senthil
*Ganthimathi Pandiyan
*Vani Viswanath
*Baby Lakshmi
*Keerthi
*Devi
*Madurai Saroja
*Vijaya
*Nagalakshmi
*K. K. Soundar
*Ramnath
*Suryakanth
*Krishnamoorthy
*Jayapal
*Kullamani
*Pasi Narayanan
*Vellai Subbiah
*Periya Karuppu Thevar
*M. L. A. Murugesh
*Karisai Amalan
*Rajamani
*Singamuthu
 

==Production==
Manojkumar, brother-in-law of director Bharathiraja made his directorial debut with this film. Producer Kovaithambi was impressed with the story narrated by Manojkumar and he immediately narrated the story to Sivaji Ganesan who agreed to act in the film. Kovaithambi said that film ran for 50 days, though it was not that successful but the film gave him satisfaction of making a film with Sivaji Ganesan. 

==References==
 

==External links==
*  
*  

 
 
 
 


 