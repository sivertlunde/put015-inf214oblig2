Wacuś
{{Infobox film
| name           = Wacuś
| image          =
| image_size     = 
| caption        = 
| director       = Michał Waszyński
| producer       = 
| writer         = Napoleon Sadek, Konrad Tom
| narrator       = 
| starring       = 
| music          =    
| cinematography =  
| editing        = 
| distributor    = 
| released       =October 3, 1935
| runtime        =59 minutes
| country        = Poland Polish
| budget         = 
}}
 1935 Poland|Polish romantic comedy film directed by Michał Waszyński.

==Cast==
*Adolf Dymsza...  Tadeusz Rosolek, alias Wacus Rosolek 
*Jadwiga Andrzejewska ...  Kazia Wolska 
*Mieczysława Ćwiklińska ...  The Widow Centkowska 
*Władysław Grabowski ...  Dr. Lecki 
*Maria Korska ...  Mother Wolska 
*Józef Orwid ...  Pawnshop Owner 
*Eugeniusz Koszutski ...  Antoni, pawnshop guard 
*Jerzy Marr ...  Roman Franek Suzinik, cousin 
*Klemens Mielczarek ...  Kubuś, teenage boarder 
*Michał Halicz ...  Gypsy on Horseback 
*Jerzy Kobusz ...  Pietrusiński, football goalie
*Irena Skwierczyńska ...  Gypsy Wife 
*Konrad Tom ...  Cosmeticians School Director 
*Leon Rechenski ...  School Watchman 
*Feliks Chmurkowski ...  The Schoolteacher

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 