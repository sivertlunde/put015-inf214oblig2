I-4:Loafing and Camouflage
{{Infobox film
| name           = I-4:Loafing and Camouflage
| image          = 
| image size     =
| caption        =
| director       = Vassilis Katsikis
| producer       =
| writer         = 
| narrator       =
| starring       = Giorgos Kimoulis  Thanasis Tsaltabasis  Haris Mavroudis  Petros Lagoutis  Kostas Fragolias  Stathis Panagiotidis  Maria Korinthiou  Isavela Kogevina
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 
| country        = Greece Greek
| budget         =
| gross          =$4,580,564 
| preceded by    =
| followed by    =
}} highest grossing films of the year.  

==Plot==
Five young men try to be exempted from army using their high social interfaces. But a scandal related with many exemptions from army breaks out and the five men are compelled to enlist. Finally they enlist in category I-4 that means they serve as unarmed. Next they go to serve as auxiliary unit in a commando troops. There, the soldiers are forced to face the cruel behaviour of commandos. But, in this camp the Greek colonel play an army role play game with the Turkish colonel of the Turkish camp, in the other coast of Aegean Sea|Aegean. This game gives to the I-4 soldiers the opportunity to avenge the commandos. Finally the conflict between commandos and I-4 upset the camp, but the I-4 soldiers manage to avoid the penalties.  

==Cast==
*Giorgos Kimoulis as Colonel Theofilos Theofilou 
*Thanasis Tsaltabasis as Thanasis Kioubasidis
*Haris Mavroudis as Alkis Koumentakis
*Petros Lagoutis as Miltos Mavreas
*Kostas Fragolias as Alexandros Galatis
*Stathis Panagiotidis as Theo Foutidis
*Maria Korinthiou as Eleni Theofilou
*Isavela Kogevina as Maria Theofilou

==References==
 

==External links==
* 

 
 