The Green Hornet Strikes Again!
{{Infobox film
| name           = The Green Hornet Strikes Again!
| image          =
| image_size     =
| caption        = John Rawlins
| producer       = Henry MacRae
| writer         = George H. Plympton Basil Dickey Sherman L. Lowe Fred MacIsaac Fran Striker (characters)
| narrator       =
| starring       = Warren Hull Keye Luke Pierre Watkin Anne Nagel Wade Boteler Eddie Acuff
| music          = Charles Previn
| cinematography = Jerome Ash
| editing        = Saul A. Goodkind (supervising) Joseph Gluck Louis Sackin Alvin Todd
| distributor    = Universal Pictures
| released       =  
| runtime        = 15 chapters (293 min) 
| country        = United States
| language       = English
| budget         =
}}
 Universal Serial movie serial The Green Hornet.  This was Universals 117th serial (the 49th with sound) of the 137 the studio produced.  The plot involves racketeering and is unusual for a film serial in having mostly stand alone episodes instead of a continuous story (although this was also the case for the original Green Hornet serial). 

==Plot==
Wealthy publisher Britt Reid and his trusted Asian servant disguise themselves as the vigilantes; The Green Hornet, and his faithful servant, Kato as they battle the growing power of a ruthless Crime Lord Boss Crogan and his varied rackets across the city. All of which have strong links to foreign powers...

==Cast== Gordon Jones in this role and also provided the voice of the Hornet (instead of radio voice Al Hodge in the original serial). 
*Wade Boteler as Michael Axford, Britt Reids bodyguard
*Anne Nagel as Lenore "Casey" Case, Britt Reids secretary
*Keye Luke as Kato, the Green Hornets sidekick
*Eddie Acuff as Ed Lowery, a reporter
*Pierre Watkin (listed in the credits as "Pierre Watkins") as Boss Crogan, racketeer
*James Seay as Bordine, one of Boss Crogans henchmen
*Arthur Loft as Tauer, Boss Crogans chief henchman Joe Devlin as Dolan, one of Boss Crogans henchmen William Hall as DeLuca, one of Boss Crogans henchmen
*Dorothy Lovett as Frances Grayson, an aluminum heiress, and Stella Merja, an actress hired to replace her

==Chapter titles==
Source: {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 228
 | chapter = Filmography
 }} 

# Flaming Havoc
# The Plunge of Peril
# The Avenging Heavens
# A Night of Terror
# Shattering Doom
# The Fatal Flash
# Death in the Clouds
# Human Targets
# The Tragic Crash
# Blazing Fury
# Thieves of the Night
# Crashing Barriers
# The Flaming Inferno
# Racketeering Vultures
# Smashing the Crime Ring

==References==
 

==External links==
* 
* 
* 
* 

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 