The Lumber Champ
{{Infobox Hollywood cartoon|
| cartoon_name = The Lumber Champ
| series = Pooch the Pup
| image = Lumber Champ Pooch.jpg
| caption = Pooch meets the girl coonhound for the 6th time.
| director = Walter Lantz
| story_artist = 
| animator = Manuel Moreno Lester Kline Fred Kopietz Charles Hastings
| voice_actor = 
| musician = James Dietrich
| producer = 
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = March 13, 1933
| color_process = Black and white
| runtime =   English
| preceded_by = The Terrible Troubadour
| followed_by = Natures Workshop
}}

The Lumber Champ is an animated short film distributed by Universal Pictures. It is the eighth of the thirteen Pooch the Pup cartoons.

==Plot==
Pooch (now having black ears) is a wood cutter who chops trees for the logging business. His boss is a tall husky cracks a whip at slow-moving works. While looking for trees to cut, Pooch spots his girlfriend, a coonhound, painting some pictures of the scenery. Delighted to see her, Pooch greets his sweetheart. They then sing the song "The Cute Little Things You Do" {{cite web
|url=http://lantz.goldenagecartoons.com/1933.html
|title=The Lumber Champ
|accessdate=2011-06-03
|publisher=The Walter Lantz Cartune Encyclopedia
}}  and walk around together. Looking from a distance, the husky sees them and develops an affinity for the female coonhound. The husky snatches her with his whip and shoots Pooch from a cannon in order to get away with the girl. Eventually, the husky attempts to run over the coonhound with a locomotive, but his attempt is foiled when Pooch redirects the railroad tracks. At the films conclusion, Pooch and his girlfriend embrace.

==Notes==
*Pooch still looks much like his original design, although his white ears have been replace by long black ones.
*The animated trees in the cartoon bear some resemblance to Groucho and Harpo of the Marx brothers.

==References==
 

==External links==
*  at the Big Cartoon Database

 
 
 
 
 
 
 
 
 
 


 