Frække Frida og de frygtløse spioner
 
{{Infobox film 
| name           = Frække Frida og de frygtløse spioner
| image          = Naughty Frida and the Fearless Spies.jpg
| image_size     =
| caption        = Movie poster
| director       = Søren Ole Christensen
| producer       = Kenneth Madsen 
| writer         = Søren Ole Christensen 
| narrator       = 
| starring       = Anette Brandt Mathias Klenske Ida Kruse Hannibal Gunilla Odsbøl
| music          = Søren Rasted Claus Norreen 
| cinematography = Nicolaj Brüel Henrik Ploug Christensen 
| editing        = Birger Madsen 
| distributor    = Nordisk Film 
| released       = 18 March 1994 
| runtime        = 75 min. 
| country        = Denmark  Danish 
| budget         =  
| preceded_by    = 
| followed_by    = 
}}
 1994 Danish Danish childrens film written and directed by Søren Ole Christensen. Christensen based the story on a series of childrens books by Lykke Nielsen. Music for the film was written and performed by Søren Rasted and Claus Norreen who later teamed up with René Dif and Lene Nystrøm later in 1994 to form the Bubblegum music|Bubblegum/eurodance music group, Joyspeed (later renamed to Aqua (band)|Aqua).

==Cast==
{| class="wikitable" |- bgcolor="#CCCCCC" 
!   Actor !! Role
|- Anette Brandt|| 	Frida
|- Mathias Klenske||	Adam
|- Ida Kruse Hannibal|| Desiré
|- Gunilla Odsbøl||	Amalie
|- Arne Siemsen||	Fridas father Poul
|- Charlotte Sieling||	Fridas mother Jytte
|- Finn Nielsen||	Richard Gunnersen
|- Axel Strøbye||	Fridas grandfather Carl
|- Birgit Sadolin||	Fridas grandmother
|- Jesper Klein||	Park Ranger
|- Tom McEwan||	Mr. Johnson
|- Morten Suurballe||	Mr. Goldbody
|- Lisbet Lundquist||	Neighbor
|- Paprika Steen||Lonni
|- Johnny
|-
|}

==Soundtrack==
{{Infobox Album
| Name        = Frække Frida og de frygtløse spioner
| Type        = soundtrack
| Artist      = Soren Rasted and Claus Norreen
| Cover       = Naughty Frida.jpg
| Released    = 1994
| Recorded    = 1993
| Genre       = Soundtrack
| Length      =  Danish
| Label       = 
| Producer    =
| Reviews     =
| Compiler    =
| Chronology  = 
| Last album  = 
| This album  = 
| Next album  = 
| Misc        = 
}}
 producers Søren Nystrøm Rasted|Søren Rasted and Claus Norreen had won a contest and were hired to produce the soundtrack. For some of the songs they hired the then club DJ René Dif. The three had previously not been associated with each other, but their getting along well whilst making the soundtrack made them decide to work together again on a future project. The future project would ultimately eventually lead to the creation and success of Aqua, with Lene Nystrøm as their lead vocalist. 

===Track titles===
* De frygtløse spioner (featuring Thomas Skovgaard) 
* Nattens fe (featuring Peter Smith) 
* Frække Frida (featuring Annette Brandt, Mathias Klenske, Gunilla Odsbøl & Ida Hannibal Kruse) 
* Si-bab-rapper-di-åhh (featuring Arne Siemsen, René Dif & Annette Brandt) 
* Når jeg blir stor (featuring Alice Søndergård & Annette Brandt) 
* Gunnersen (featuring Arne Siemsen) 
* Godmorgen (featuring Thomas Skovgaard) 
* Den magiske kasse (featuring Annette Brandt, Mathias Klenske, Gunilla Odsbøl & Ida Hannibal Kruse) 
* Hele verden rundt (featuring Alice Søndergård) 
* Ønskebrønd (featuring Peter Smith & Søren Rasted) 
* Devils child 
* Flugten 
* Si-bab-rapper-di-åhh (Dance Mix) (featuring Arne Siemsen, René Dif & Annette Brandt) 
* Frække Frida (featuring Christine Havkrog)

==External links==
*  
*  
*  

 

 
 
 
 
 
 

 
 