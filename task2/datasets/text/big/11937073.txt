Coming Out (1989 film)
 
{{Infobox film
| name = Coming Out
| image = Coming out poster german film.jpg
| alt = 
| caption = German theatrical release poster
| director = Heiner Carow
| producer = Horst Hartwig
| writer = Wolfram Witt
| starring = Matthias Freihof Dirk Kummer Dagmar Manzel
| music = Stefan Carow
| cinematography = Martin Schlesinger
| editing = Evelyn Carow
| distributor = DEFA
| released =  
| runtime = 113 minutes  
| country = East Germany
| language = German
}} romantic drama film directed by Heiner Carow and written by Wolfram Witt. Starring Matthias Freihof, Dagmar Manzel, and Dirk Kummer, the film deals with the process of the protagonists "coming out" and admitting to himself and others that he is a gay man. The film was shot entirely on location in East Berlin and includes scenes shot with amateurs in some of the citys gay bars and clubs.
 Premiering at Berlinale in 1990 for its frank treatment of the issue of homosexuality .   

==Plot==
The story revolves around a young high-school teacher, Philipp Klahrmann, who during his first day at work collides with a fellow teacher, Tanja, in one of the schools corridors. As Phillip insists on making sure the Tanja is okay, there is an obvious connection between the two that is much more than a simple friendly co-worker relationship. As a result of the brief accident in the hallway, Philipp ensures Tanja is okay and later on takes Tanja out for a drink. A romance quickly develops between the two. That same night, back at Tanjas apartment, the two engage in a frank discussion. Tanja reveals that they attended the same university and that she had a crush, as did most of her female classmates, on Philipp who was at that time "going steady" with another classmate. Eventually, the two partake in their first physical display of affection, and after sleeping together  Philipp bluntly asks if Tanja would want him as her husband.

Philipps character develops and the audience often questions his sexuality and witnesses his allure to both sexes, first when a girl, possibly a student, though it is never said turns down the advances of another man and immediately flirts with Philipp in a theatre by putting her hand on his leg. Later scenes include Philipp being hit on by many men and also continually being perceived as a pretty face.

The audience follows Tanja and Philipp on the development of their relationship, which seems believably comfortable despite some question-provoking scenes. Once such scene includes Philipp getting extremely worked up as a group of men on a train gang up on a presumably gay gentleman. Philipp jumps into the mix, and has punches thrown his way, without fighting too much, himself. While one cannot say for certain if Philipp gets involved in the fight because it is simply the right thing to do, or whether of not Philipp feels some form of increased sensitivity towards homosexual discrimination, the scene cues to the audience that there may be something more to Philipps personal identity/values that he has not displayed before. Another scene along the same lines as this is a scene where Phillip comes home to find Tanjas old friend, Jakob, at their apartment. The tension between the two men is blatantly obvious. As a flamboyant male figure, Jakob makes Philipp very uncomfortable and the audience senses the two men have met before, but are not yet filled in as to the details of their previous history. After Jakob leaves the apartment, Tanja accuses Philipp of being rude towards her guest, she has not picked up on the connection between the men.

The film then takes a prominent twist when Philipp, without Tanjas presence, is dragged into a gay bar, where a party is evidently taking place. It is not said whether Philipp was intentionally hanging around a gay bar, but can be hypothesized that Philipps encounter with Jakob may have motivated his decision to hang around this location. While at the bar, an intentionally loud and flamboyant party is taking place, most patrons at the bar are in costume and many are in drag. Philipp is cautious, but proceeds further into the bar, taking a seat  near an older male character who senses his hesitation in this setting and says, "Dont be scared. Everyone is at first. Be Brave." This quote signifies the idea that while Philipp may not be outwardly homosexual, others around him may sense his true identity.

The use of disguises especially facial disguises and coverings is important in this scene. A young, shy man dressed in a black and white jesters costume watches Philipp from a distance, they have some sort of connection. Later, the audience learns that this man, named Matthias, has taken an interest in being more than friends with Philipp. Phillip is speechless when he meets Matthias and realizes he has been waiting for this man his entire life. A very intoxicated Philipp is returned home that night by two male figures, one of which being Matthias, yet the viewer is unaware if anything happened physically between the two because the only scene regarding this question includes Matthias shutting the door behind himself as he leaves Philipps apartment to catch up with the other male character. Matthias exhibits a youthful joy as he leaves Philipps apartment. It should be noted that Philipp had this private apartment to himself, as the East German government issued apartments to independents. He often would stay at Tanjas flat, but still had this apartment as they would not be issued a new one fitting for two people until officially married. This allows Philipp a private atmosphere where he can entertain, male guests without Tanja knowing.

Time passes and eventually Matthias meets Philipp again, apparently just by chance, and quickly invites him to his birthday party that same evening. After struggling some with his conscience, Philipp ends up sneaking out at night after Tanja falls asleep, and returns to the bar where they first met, the location of the party. Philipp is ushered by the proprietor to the private party which is evidently waiting for Philipp in order to begin. Here we see how open Matthias is about his sexuality, desires for Philipp and that he is not afraid to show his affection in front of his family and friends. Philipp is introduced to Matthias family at the dinner "Yes, this is him." The night ends with the two men alone, talking about their relationship, and Philipp lies to Matthias when he is asked whether or not he has a lover. Philipp then asks Matthias if he wants to have a family and kids someday, to which Matthias replies "No. I know I cant anyways." The audience has to question whether this is referring to Matthias being physically incapable of reproducing, but can be assumed he is simply acknowledging he is homosexual and would not have a relationship with a woman in order to procreate. Eventually, the two sleep together, and Philipp is left in a tough situation having to return home to Tanja who is extremely worried by her significant others mysterious night away (despite the fact that Philipp had left a note for Tanja, that read: "Please dont be sad. I have to be alone for awhile. Dont worry or contact me. -Phillip")

Another scene with hidden meaning takes place when Tanja and Philipp listen to a gentleman do a stack-up singing act. The man sings the following lyrics:
:"My mother does not call me by my name,
:My mother is dead.
:My father does not call me by my name,
:My father is far away.
:God does not call me by my name,
:God thought up a real mean act.
:He plays the role of the cur in it,
:He howls all night long.
:I drove him away with a stick,
:He should leave me in peace.
:Oh heart of mine,
:Rest a while.
:When God has abandoned me rest a while
:When my body is stretched out
:Patiently
:Rest a while
:Rest a while
:Rest a while without God
:Rest a while."
This song is very meaningful when compared to the topic of homosexuality, as Philipp may very well relate to the idea that he has been abandoned by both his mother and father, and even God, after making the realization that he is homosexual in a very judgmental world (His parents despise the fact that their son may be homosexual). By saying that he "howls all night long"   and that he "should leave me in peace" reiterates the idea that ones sexual identity is not something that is easily forgotten, nor easily avoidable. This scene reflects the idea that while Philipp may acknowledge that being homosexual isolates him from powerful sources of support and love, God himself has committed a "mean act" and until he can be loved and accepted for who he is, there is nothing to do but rest and let it be.

Philipps relationship with Tanja rapidly deteriorates, as he becomes increasingly standoffish and avoids all communication over his personal thoughts with Tanja. Little by little the audience witnesses Philipp become aware of his homosexual preferences, and obvious struggles with his identity, despite the fact that Tanja may need him now more than ever, as she is assumedly pregnant. It is proven apparent that Philipp has been in denial about his homosexuality when he seeks out Jakob to discuss the previous history they have together. The audience learns that Philipps parents can be held responsible for getting Jakob transferred out of the school the boys were attending, all because of their sexual relationship. This is the very first time that Philipps "gay prehistory" is blatantly discussed in the film. This idea is further complicated by the fact that Philipps mother disapproves—even downright hates—the sexual preferences of her son and goes out of her way to remind him of that throughout the entire film.

Unsurprisingly, Philipp is eventually forced to come out to Tanja, after he and Matthias embrace during the intermission of an opera that Philipp is attending with Tanja. The hug shared by the two men clearly suggests a bond much deeper than friends, and, as it turns out, Mathias intentionally came to the opera looking for Philipp after failing to find him at his apartment. Matthias is distraught upon learning that Philipp is married and runs out of the opera house in obvious anguish and immeasurable grief.
 SED and DEFA, the older gay man explains how the communists helped him through his struggle. It seems as though the gentleman had been facing problems for his entire life due to his sexual orientation, and that Philipp better stand up and fight through it in order to make any progress towards obtaining happiness within himself.

The film ends with a scene in the classroom, as the head-teacher - who obviously has discovered Philipps sexual orientation - says that she and a group of teachers need to observe his class to see if he is still suitable to teach. Philipp is quiet for a long time, thus prompting the head-teacher to say Kollege Klahrmann! to which Klahrmann simply replies Ja. Freihof claims that this single utterance signifies the recognition, by Philipp, of his sexual orientation and everything attendant with it.  The film closes with a shot of Philipp leaving his apartment block on his bicycle, while the audience is left to ponder the fact that he no longer is together with his wife, Tanja, and that the man he truly loves, Matthias, was actually the character in the opening scene of the movie, who was being taken care of after an attempted-suicide due to the pressures of being homosexual in a prejudiced society.

==Cast==
 
* Matthias Freihof as Philipp Klarmann
* Dagmar Manzel as Tanja
* Dirk Kummer as Matthias
* Michael Gwisdek as Achim
* Werner Dissel as Older homosexual man
* Gudrun Ritter as Frau Moellemann, Waitress
* Walfriede Schmitt as Philipps mother
* Axel Wandtke as Jakob Pierre Bliss as Araber
* René Schmidt as Young man in the park
* Thomas Gumpert as Larry
* Ursula Staack as Wanton
* Robert Hummel as Lutz
* Horst Ziethen as Lanky boy
 

==Production==
The opening scene follows an ambulance through well-known areas and boroughs such as Prenzlauer Berg, Berlin-Mitte (Alexander Platz) and Friedrichshain on a night that the audience could assume is New Years Eve, due to the fireworks in the background. Other scenes in the movie are filmed on locations that were common meeting points for homosexuals in East Germany such as the Fairytale Fountain (Märchenbrunnen) in Volkspark Friedrichshain and bars such as "Schoppenstube" in Prenzlauer Berg and Zum Burgfrieden which was located at Wichertstraße 69, though it was closed in January 2000.

Scenes filmed in the school where Philipp teaches were filmed in the Carl-von-Ossietzky-Gymnasium, a historical building and school in Pankow with a breathtaking stairwell and some halls were used in a few scenes.
 rector of the University of Film and Television (Potsdam-Babelsberg) from 1986 to 1990. Two of his three sons are openly homosexual, one of whom is Berlin-based painter Norbert Bisky who is known for his bright paintings depicting adolescent and homosexual scenes, often with a political motifs reflected in title and nature of the paintings.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 