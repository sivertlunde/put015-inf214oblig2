Naked Killer
 
 
{{Infobox film name = Naked Killer image = Naked_Killer_DVD_Cover.jpg producer =Wong Jing writer = Wong Jing (writer) starring = Chingmy Yau Simon Yam Carrie Ng Madoka Sugawara Wai Yiu Ken Lo director = Clarence Fok Yiu-leung distributor =  released = 1992
|runtime = 91 min. language = Cantonese Chinese music =  awards = budget =
}} 1992 Cinema Hong Kong film written and produced by Wong Jing, and directed by Clarence Fok Yiu-leung. The film stars Chingmy Yau, Simon Yam and Carrie Ng.

The film is widely regarded as a cult classic.    

==Plot==
Kitty (Chingmy Yau) is a vicious young woman who has no qualms about stabbing girlfriend-bullying men in the Penis|genitals. Tinam (Simon Yam) is a cop who is undergoing a rather traumatic period: he shot his own brother by accident and as a result vomits every time he handles his gun.

When Kitty severely injures a man by stabbing him in the groin, Tinam attempts to arrest her but cant manage it. Kitty later turns up at the police station and manipulates the facts to the point that Tinam has no choice but to start a relationship with her. This reveals Kitty as a subtle manipulator. Tinam, who has become impotent, finds that he no longer feels the same way about Kitty, who is content with leading him along for now.

Kittys father is married to a new wife but it is a tense marriage and one evening he catches her with another man called Bee (Ken Lo). In the fight that follows, Kittys father falls down some stairs and is killed.

Furiously determined on revenge, Kitty breaks into Bees office and proceeds to kill him, his bodyguards and most of his staff (there is some indication that he is involved with organised crime). In the course of her escape she takes a woman hostage but then, unexpectedly, the woman helps her out, disposing of many of the pursuers herself.

The woman turns out to be Sister Cindy (Wai Yiu) who is in fact a professional assassin. She was probably out to kill Bee herself. Seeing that Kitty has potential, she proceeds to train her and give her a new identity. The training includes the killing of enraged, chained-up paedophiles in Sister Cindys cellar. Before long the pupil is outsmarting the teacher.

For her first mission, Kitty accompanies Sister Cindy and murders a member of the Japanese yakuza. This leads to a contract being placed on them and the assignment is entrusted to Princess (Carrie Ng), one of Sister Cindys former protégés and a lesbian with an equally deadly young lover called Baby (Madoka Sugawara).

While investigating the murder himself, Tinam goes to check up on an air hostess whom the victim met. The witness is Vivian Shang, who Tinam recognises as Kitty. She denies this but renews their relationship.

Sister Cindy proceeds to murder other people who could connect Kitty to Vivian Shang, including Tinams superior and a witness to the crotch-stabbing incident. Kitty does however stop her from killing Tinam himself. Kitty and Tinam consummate their relationship, but their different professions means that it will be difficult for things to go any further.

Sister Cindy for her part decides that Kitty has lost the killer touch, but on the other hand has found happiness with Tinam and tells her to leave and make the most of it.

Princess, who is supposed to kill Kitty, builds up an obsession of her own for the girl, which leads to some conflict with Baby. They do however set about killing Sister Cindy who, with death approaching, puts up a good fight. But she is ultimately defeated due to a ploy used by Princess earlier that day: she kissed Sister Cindy but her lipstick contained poison which, combined with some wine she has drunk, kills her.

Kitty goes into hiding but later confronts Princess, apparently willing to become her partner both in business and in bed. Princess subsequently falls into the same trap she set Sister Cindy: when they kiss, Kitty passes on some poisoned lipstick of her own. Tinam then bursts in, shooting away at Princess henchmen, apparently having overcome his vomiting problems.

In the battle, Tinam kills Baby and a furious Princess pursues him and Kitty back to Sister Cindys home. The poison in her system catches up with her however and she dies with Kitty taunting her by claiming that she on the other hand will get to hospital in time to survive.

By this time, the poison inside Kitty has taken effect. Unwilling to lose her again, Tinam fires his gun into the gas oven causing the house to go up in flames with the two of them inside.

==Reception==
Although this film received an 80% on Rotten Tomatoes   and was generally acclaimed by critics, its extreme and graphic violence and explicit sexual themes may have deterred many viewers. For those reasons the film has developed a cult following.

==Production notes==
*In spite of the title, the film shows little in the way of actual nudity. At the time, appearing naked in Asian movies was seen as detrimental to an actor or actress career prospects,  though many A-list actors chose to star in Category III films. Although she does appear topless while making love to Simon Yam, Chingmy Yau keeps her breasts covered with her arm.

*This was one of a number of movies in which Yam and Yau starred together in the 1990s.

*The controversial nature of the film with its violent content and sexual themes led to extensive cutting by the censors in both Hong Kong and abroad. DVD audio commentary by director Clarence Fok Yiu-leung available on Naked Killer DVD, released by Hong Kong Legends, ASIN: B000063VC9 

*Japanese actress Sugawara does appear topless and her posterior is shown during her love scenes with Ng, who remained clothed and insisted on wearing gloves while fondling Sugawaras body. 

*Studio shots were taken of Yau nude with Ng fully clothed.

==Cast==
*Kitty/Vivian Shang - Chingmy Yau
*Tinam - Simon Yam
*Princess - Carrie Ng
*Baby - Madoka Sugawara
*Sister Cindy - Wai Yiu (as Kelly Yao)
*Bee - Ken Lo

==Crew==
*Directed by: Clarence Fok Yiu-leung
*Written by: Wong Jing
*Screenplay by: Wong Jing 
*Produced by: Dennis Chan and Wong Jing

== See also ==
*Naked Weapon
*Cats Eye (manga)

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 