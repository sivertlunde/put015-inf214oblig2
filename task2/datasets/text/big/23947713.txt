Windjammer (1937 film)
 
{{Infobox Film
| name           = Windjammer
| image          = Dvd cover of Windjammer (1937 film).jpg
| image_size     = 
| caption        = 
| director       = Ewing Scott David Howard (associate producer)
| writer         = James Gruen Roul Haig (story) Daniel Jarrett (screenplay)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Frank B. Good
| editing        = Robert O. Crandall
| distributor    = 
| released       = 6 August 1937
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1937 cinema American film directed by Ewing Scott.

== Cast == George OBrien as Bruce Lane
*Constance Worth as Betty Selby William Hall as Captain Morgan
*Brandon Evans as Commodore Russell P. Selby Gavin Gordon as J. Montague Forsythe
*Stanley Blystone as Peterson
*Lal Chand Mehra as Willy
*Ben Hendricks Jr. as Dolan
*Lee Shumway as Yacht Captain
*Frank Hagney as Slum
*Sam Flint as Marvin T. Bishop
 Al Herman, Warren Jackson, House Peters Jr., Marin Sais, Ted Stanhope and Florence Wix appears uncredited.

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 


 