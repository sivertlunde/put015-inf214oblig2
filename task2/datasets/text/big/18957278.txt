Salgira
{{Infobox film
| name           = Salgira
| image          =
| image_size     =
| caption        =
| director       = Qamar Zaidi
| producer       = Najma Hassan
| writer         = Shams Hanafi 
| narrator       =
| starring       = Waheed Murad Shamim Ara Tariq Aziz Santosh Rissal Nighat Sultana Nirala Talish   Asif Ali Zardari (as child artist)
| music          = Nashad
| cinematography =
| editing        =
| distributor    =
| released       = 14 February 1969
| runtime        = approx. 3 hours
| country        = Pakistan
| language       = Urdu
| budget         =
}}

Salgira (Urdu: سالگرہ) (English: Birthday) is an Urdu-language Pakistani black-and-white film released in 1969. It is a melodious love story directed by Qamar Zaidi and produced by Najma Hassan and written by Shams Hanafi. The film cast Waheed Murad, Shamim Ara, Tariq Aziz, Santosh Rissal, Nirala, Nighat Sultana and Talish. Included in the soundtrack are two hits songs, Meri zindagi hai naghma... and Lay aai phir kahan par... sung by Noor Jehan.

Asif Ali Zardari, ex-president of Pakistan, acted as the young protagonist Waheed Murad. This is his only known film as a child actor.

==Release==
Salgira was released on the Valentines Day of 1969. The film completed 20 weeks on main cinemas and 61 weeks on other cinemas of Karachi and thus became a golden jubilee film. 

==Music==
The music of Salgira is composed by Nashad and the lyrics are written by Shevan Rizvi. Playback singers are Noor Jehan, Mehdi Hassan, Irene Parveen and Ahmed Rushdi.

===Songography===
*Lay aai phir kahan par... by Noor Jehan
*Meri zindgi hai naghma... by Noor Jehan
*Zulf ko teri ghataon ka... by Mehdi Hassan
*Lazat-e-soz-e-jiger pooch le perwaney se... by Ahmed Rushdi & Irene Perveen
*Tere wadey se meri zindagi saji... by Ahmed Rushdi & Irene Perveen

==Awards==
Salgirah won 2 Nigar Awards in the following categories: 

{| class="wikitable" border="1"
|-
! Category
! Recipient
|-
| Best music
| Nashad
|-
| Best female play back singer
| Noor Jehan
|}

==References==
 
 

 
 
 
 
 
 

 