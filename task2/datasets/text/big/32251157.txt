A Sixth Part of the World
{{Infobox film
| name            = A Sixth Part of the World
|  image              =
|  writer             = Dziga Vertov
|  director           = Dziga Vertov
|  producer           = Kultkino, Sovkino
|  cinematography = Mikhail Kaufman
| editing        = Elizaveta Svilova, Dziga Vertov
|  distributor        = Sovkino
|  released           = December 31, 1926  (USSR)
|  runtime            = 73 minutes Russian intertitles
|  country            = Soviet Union
|  budget             = SUR 80 000
}}
 travelogue format, it depicted the multitude of Soviet peoples in remote areas of USSR and detailed the entirety of the wealth of the Soviet land. Focusing on cultural and economic diversity, the film is in fact a call for unification in order to build a "complete socialist society". A mix between newsreel and found footage, Vertov edited sequences filmed by eight teams of kinoks (kinoki) during their trips. According to Vertov, the film anticipates the coming of sound films by using a constant "word-radio-theme" in the intertitles.  Thanks to A Sixth Part of the World and his following feature The Eleventh Year (1928), Vertov matures his style in which he will excel in his most famous film Man with a Movie Camera (1929).

==Plot==

Vertov starts by showing us, with intertitles in giant   Lenin (amazing shot downwards from above the prow) delivering new dogs to the   fair (ярмаркт). In an amazing stop motion|stop-frame sequence, rows of oranges align themselves in a packing box, wadges of packing material shuffle along and jump on top of them, and then the lids close (you can just see the line pulling one of the sides). We see coke being quenched also, as well as electricity pylons and insulators, and the village electricity co-op. We see sturgeon being hoiked out of tanks to make caviar. We see barrels of butter – it is yours! We see wheat being threshed, linen being spun and cotton being ginned. We see the country being modernised, although there are still some people who trust in Mohammed (film) or Christ (a man telling his rosary) or Buddha (film) and we are shown a Siberian shaman looking remarkably like a North American Indian, and even a reindeer being slaughtered (by axe blows to the neck) as a sacrifice. We see crowds of women in full-face veils, but also a modernising country as a woman lifts her veil. And we see some tundra-dwellers eating raw reindeer meat.

It’s a fantastic travelogue and anthropological document. Lenin’s mausoleum is his alone at this time (1926). The moral: everyone produces and is building socialism. It starts with slavery and ends with developing countries joining the socialist revolution.

==Vertovs intentions==

In an interview for Kino magazine in August 1926, Vertov explained his intentions: "A Sixth Part of the World is more than a film, than what we have got used to understanding by the word ‘film’. Whether it is a newsreel, a comedy, an artistic hit-film, A Sixth Part of the World is somewhere beyond the boundaries of these definitions; it is already the next stage after the concept of ‘cinema’ itself… Our slogan is: All citizens of the Union of Soviet Socialist Republics from 10 to 100 years old must see this work. By the tenth anniversary of October there must not be a single Tungus who has not seen A Sixth Part of the World” (quoted in Barbara Wurms essay in the DVD booklet) 

==Production==

At the beginning of 1918, Dziga Vertov was hired to edit the newsreel Kinonedelia ("Cineweek") for the Moscow Cinema Committee. With no formal education in the science of editing, he learnt to build a coherent newsreel with a minimun of stock. Practising editing on different kinds of short movies, Vertov began to theorize his own view on editing. In the hope of putting his theories into practice, in 1922 he formed the first group of kinoks|kinoki ("cine-eyes") in which he began to issue the Kinopravda ("Cine-truth") serie of films. At that time, Vertov published essays in specialized publications detailing his theories on cinema. In 1924, the Goskino film production company set up a documentary section called Kultkino and Vertov was placed in charge. That year, Goskino was replaced by Sovkino, which continue to operate the Kultkino section. In 1925, Gostorg, the Central State Trading Organisation, was seeking a director for a film promoting internal trade and praising the merits of the new social order. A Sixth Part of the World was produced by Kultkino within Sovkino. 

==Reception==

The film was well received by   production in 1927, being accused of exceeding more than three times the initial budget of 40,000 roubles (the film actually cost twice this budget). 

==DVD release==

Editions Filmmuseum released the film in 2009 in a 2-disc set with the film The Eleventh Year (1928)  .

==References==
 

==External links==
* 
*  Mubi
*  on Google Video (Notes: bad quality video with esperanto subtitles)

 

 
 
 
 
 
 
 
 
 
 