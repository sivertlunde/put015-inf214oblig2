Smithy (1946 film)
 
{{Infobox film
| name           = Smithy
| image          = Smithy_Poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Ken G. Hall
| producer       = N. P. Pery
| writer         = Ken G. Hall as "John Chandler" Alec Coppel
| based on = story by Max Afford Ken G. Hall
| narrator       =
| starring       = Ron Randell Muriel Steinbeck Henry Krips George Heath Bert Nicholas
| editing        = Terry Banks
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 119 minutes (Australia) 99 mins (US)
| country        = Australia
| language       = English
| budget         = ₤53,000  or £73,000 
| gross = over ₤50,000 (Australia)    ₤50,000 (USA) 
| preceded_by    = 
| followed_by    = 
}} Southern Cross", James Warner and Harry Lyon.
 The Southern Cross and in the US as Pacific Adventure.

==Plot==
In World War 2 some American and Australian airmen tell the story of Charles Kingsford-Smith. It starts in 1917 with him recovering from a wound incurred in fighting over the Western Front. Kingsford-Smith is rewarded with the Military Cross and is determined to make a career out of flying.

After the war he visits America and has a brief romance with Kay Sutton but later falls in love with and marries Mary Powell. He attempts to enter the England to Australia Air Race in 1919 but is stopped by Prime Minister Billy Hughes. He then decides to become the first person to fly from the US to Australia across the Pacific. He does the trip with Charles Ulm in a plane called the Southern Cross and becomes world famous.

Kingsford-Smith attempts to set up his own airline but is not successful and is forced to take people on joy flights to make a living. He breaks another record, crossing the Pacific from the Australia to the US in a single engine aeroplane with Patrick Gordon Taylor|P.G. Taylor. He almost dies flying to New Zealand with Bill Taylor and John Stannage, and retires the Southern Cross.

In 1935 he flies from Australia to England and disappears over the Indian Ocean.

==Cast==
 
*Ron Randell as Charles Kingsford-Smith
*Muriel Steinbeck as Mary Powell John Tate as Charles Ulm
*Joy Nichols as Kay Sutton 
*Nan Taylor as Nan Kingsford Smith John Dunne as Harold Kingsford Smith
*Alec Kellaway as Captain Alan Hancock
*John Dease as Sir Hubert Wilkins
*Marshall Crosby as Arthur Powell Edward Smith as Beau Sheil Alan Herbert as Tommy Pethybridge
*John Fleeting as Keith Anderson
*Joe Valli as Stringer
*G.J. Montgomery-Jackson as Warner
*Gundy Hill as Lyon William Morris Hughes as himself Captain P.G. Taylor as himself
*John Stannage as himself
*Bud Tingwell as an RAAF control tower officer
 

==Production==

===Development===
The film was the idea of N.P. Pery, the managing director of Columbia Pictures in Australia.  The Australian government had restricted the export of capital during the war, and Pery thought making a film could use up some of that money. Pery:
 Although I represent an American company, I have no hesitation in saying that I firmly believe the production of British films will blossom out, and will in time take its place side by side with the production of American films. Furthermore, I do not think I am indulging in Utopian fancies when I say that Australia, or rather, some spot in Australia, could be made the Hollywood of the British Commonwealth.  
Pery approached Ken G. Hall, who was Australias most commercially successful director, and asked him to make a film about an Australian who was well-known internationally. Hall says he briefly considered Don Bradman but never seriously because Bradman was not known in the US. The two main contenders were Dame Nellie Melba and Charles Kingsford-Smith. Melba was eventually rejected because of the costs involved with producing opera sequences and the difficulty of finding an appropriate singer to stand in for Melba.  That left Kingsford-Smith, who appealed in part because of his connection to the US. 
(According to a contemporary newspaper report, Pery also considered Billy Hughes. )

===Scripting===
Hall commissioned treatments from several writers, including Jesse Lasky, Jr., who was then stationed at Cinesound Productions with the US Signal Corps; Josephine ONeill, a Sydney film critic; Kenneth Slessor, film critic and poet; and Max Afford, one of Australias leading playwrights and radio writers.  Hall felt Affords was the best and the two of them developed a detailed treatment.  The treatment was adapted by Alec Coppel, an Australian writer who had enjoyed success in London and returned to Australia during the war. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 202. 

Sydney journalist Norman Ellison provided research.

===Casting===
Ken G. Hall looked at sixty applicants to play the title role, screen-testing eight.   Hall says the choice came down to Peter Finch and Ron Randell, a radio and theatre actor. Hall preferred Finch but sent extensive screen tests of both actors with Muriel Steinbeck back to Columbia in Hollywood. The studio picked Randell on the grounds of his greater romantic appeal.  

Muriel Steinbeck was the only actor considered for the female lead.  She had previously appeared with Randell in A Son Is Born, a film whose release was held up to take advantage of Smithys publicity.

===Shooting===
Although the movie was entirely financed by Columbia Pictures, Ken G. Hall made it using his old Cinesound crew and shot it mostly at Cinesounds studio in Bondi.

The aircraft used for the filming was the genuine Southern Cross, which has been purchased by the Australian Government 10 years earlier and refurbished by the RAAF.  A surplus RAAF CAC Boomerang was used in flying sequences for Kingsford Smiths Lady Southern Cross Lockheed Altair.

Two former co-pilots of Kingsford-Smith, P.G. Taylor and Harry Purvis play themselves, as does former Prime Minister Billy Hughes. Hughes plays himself as a younger man interviewing Kingsford-Smith.  Hall says Alec Coppel wrote a scene where Kingsford Smith tries to persuade Hughes let him compete in an air race and Hughes switches off his hearing aid. However Hughes was sensitive about his deafness and references to it were removed in the shooting script. 

The film featured the first on screen appearance of noted Australian actor Charles Bud Tingwell who was cast as a RAAF control tower officer – winning the role as he could supply his own RAAF uniform.

==Reception==
The movie made its world premiere at a gala screening in Sydney on 26 June 1946, attended by the cast and crew, the Premier of New South Wales, and Shirley Ann Richards, who was visiting Australia at the time. 

===Critical===
Reviews were generally positive, although not without criticisms.  

===US release===
The film was released in the US as Pacific Adventure. The Los Angeles Times noted the film "is obviously from Australia, although there is no mention of the fact... while technically acceptable is pretty much a stereotype of all the other histories of aviation pioneering... Ron Randell makes a likable hero." Advertising-Agency Woes Achieve High in Hilarity
Scheuer, Philip K. Los Angeles Times (1923-Current File) 13 Nov 1947: 19. 

The New York Times wrote that "it is unfortunate that the people who made this picture... did not draw a more exciting and exacting drama out of the colorful career of the noted airman... offers only the sketchiest account of Kingsford Smiths life... the scenarists made the mistake of not looking deep enough into their subject... the facts may be true enough but they seem more fictitious than real." 

The film was advertised in the US with the log line "Not a war picture". 

===Box Office===
It was the third most popular movie released in Australia in 1946. 

==Legacy==
Pery was keen for Columbia to make further movies in Australia.  However Harry Cohn, head of Columbia, was opposed to the idea. He later arranged for Smithy to be drastically re-cut and re-edited for its US release, calling it Pacific Adventure, a removing references to Australia, along with Perys credit. 

Cohn did offer Ron Randell a long-term contract in Hollywood, which the actor accepted. 

==References==
 
* "The Australian Film and Television Companion" &mdash; compiled by Tony Harrison &mdash; Simon & Schuster Australia, 1994
*Hall, Ken G. Directed by Ken G. Hall: Autobiography of an Australian Filmmaker, Lansdowne Press, 1977

==External links==
* 
*  at Australian Screen Online
*  at National Film and Sound Archive
*  – Gould Genealogy
* 
*  at Oz Movies

 
 

 
 
 
 
 
 