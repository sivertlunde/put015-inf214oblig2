The Glass Alibi
{{Infobox film
| name           = The Glass Alibi
| image          = Glass alibi 1946 poster.jpg
| image_size     = 
| alt            =
| caption        = Theatrical release poster
| director       = W. Lee Wilder
| producer       = W. Lee Wilder
| screenplay     = Mindret Lord
| narrator       = Paul Kelly Douglas Fowley Anne Gwynne Maris Wrixon Alexander Laszlo
| cinematography = Henry Sharp
| editing        = Asa Boyd Clark
| studio         = W. Lee Wilder Productions
| distributor    = Republic Pictures
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Paul Kelly, Douglas Fowley, Anne Gwynne and Maris Wrixon. 

==Plot==
A reporter marries a dying girl for her money, but she recovers from her illness so he plots her murder.

==Cast== Paul Kelly as Max Anderson
* Douglas Fowley as Joe Eykner
* Anne Gwynne as Belle Martin
* Maris Wrixon as Linda Vale
* Jack Conrad as Benny Brandini
* Selmer Jackson as Dr. John F. Lawson
* Cyril Thornton as Riggs
* Cy Kendall as Red Hogan
* Walter Soderling as Coroner
* Victor Potel as Gas Attendant
* George Chandler as Bartender
* Phyllis Adair as Nurse
* Ted Stanhope as Drug Clerk
* Dick Scott as Frank
* Eula Guy as Connie
* Forrest Taylor as Charlie

==References==
 

==External links==
*  
*  
*  
*  
*  


 
 
 
 
 
 
 
 
 
 

 