R... Rajkumar
 
 

{{Infobox film
| name           = R... Rajkumar
| image          = R... Rajkumar Theatrical poster (2013).jpg
| caption        = Theatrical release poster
| director       = Prabhu Deva
| producer       = Viki Rajani Sunil Lulla
| writer         = Shiraz Ahmed (dialogues)
| screenplay     = Prabhu Deva   Sunil Agarwal   Ravi S Sundaram
| story          = Prabhu Deva
| starring       = Shahid Kapoor   Sonakshi Sinha   Sonu Sood
| music          = Songs:  
| cinematography = Mohana Krishna
| editing        = Ballu Saluja
| studio         = Next Gen Films
| distributor    = Eros International
| released       =  
| runtime        = 145 mins
| country        = India
| language       = Hindi
| budget         =  
| gross          =  
}}

R... Rajkumar (earlier known as Rambo Rajkumar) is a 2013 Bollywood action film|action-masala film directed by Prabhu Deva and written by Shiraz Ahmed.  Produced by Viki Rajani and Sunil Lulla, the film stars Shahid Kapoor and Sonakshi Sinha in lead roles.  The theatrical trailer of R... Rajkumar was unveiled on 1 October 2013 on YouTube to mostly positive viewer response.  The film released on 6 December 2013, and received a mixed response from critics, though managed to get a decent opening at the box office, becoming a commercial success. 

== Plot ==
Romeo Rajkumar (Shahid Kapoor) is an aimless youth who arrives in small town of Dhartipur been ruled by two drug barons named Shivraj Gurjar (Sonu Sood) and his arch enemy Manik Parmar (Ashish Vidyarthi), controlled by a Mafia don named Ajit Taaka (Srihari), who operates in Hong Kong. Rajkumar now works for Shivraj. But his life changes forever when he claps eyes on the beautiful, educated Chanda (Sonakshi Sinha). What he does not know is, that Chanda is an orphan who is being raised by Manik Parmar. After some persistent wooing, Chanda falls for Rajkumars charms. Just when their love was about to flourish, Shivraj and Parmar compromise and Parmar decides to get Chanda married to Shivraj, against Chandas wish. Angered Rajkumar beats Shivrajs men and challenges him, saying that he will marry Chanda in front of Shivraj. Furious, Shivraj plans to get Rajkumar killed in an encounter by his pet police officer. But in return, gets a shocking surprise when it is revealed that Rajkumar actually works for Don Ajit Takka and was sent to Dhartipur only to take over the drug cartel to which both Shivraj and Parmar owe allegiance. Rajkumar captures Shivrajs truck full of drugs and drives it to Takka, revealing he himself was a big gangster all along. After giving Shivrajs stolen truck to Takka, he discovers that the truck was empty. In a twist of events, it turns out that Shivraj and Takka had joined hands, and purposely made Rajkumar steal the empty truck. Shivraj and Takka brutally kill Rajkumar and bury him. Shivraj starts his wedding celebrations with Chanda, only to be interrupted by the still alive Rajkumar, who is shown to have been rescued by one of Shivrajs henchmen after he was buried. Shivrajs men back off, because they are now friends with Rajkumar, hence leading to a one on one fight between the two. Finally, Rajkumar injures Shivraj with a power punch to the neck and later kills Don Takka with a ceramic tile. The film ends with all the men beating Parmar, while a severely injured Rajkumar and Chanda walk away hand in hand happily.

== Cast ==
* Shahid Kapoor as Romeo Rajkumar 
* Sonakshi Sinha as Chanda 
* Sonu Sood as Shivraj Gurjar
* Ashish Vidyarthi as Manik Parmar
* Asrani as Pandit
* Mukul Dev as Qamar Ali
* Srihari as Ajit Taaka 
* Poonam Jhawer as Bindu
* Ashok Samarth as Inspector
* Charmy Kaur (Special appearance in song "Gandi Baat")
* Prabhu Deva (Special appearance in song "Gandi Baat")
* Ragini Dwivedi (Special appearance in song "Kaddu Katega")
* Scarlett Mellish Wilson (Special appearance in song "Kaddu Katega")
* Namrata Asrani as Jason
* Ankitaa Asrani as Shouter

==Production==

Sonakshi Sinha performed some action scenes in the film.    Charmy Kaur shot for an item song for the film in June 2013.  Kannada actress Ragini Dwivedi will be making her Bollywood entry in the same song which will also feature Scarlett Mellish Wilson. 

The film was first titled Rambo Rajkumar. According to reports, the makers of the original Rambo (film series)|Rambo series had copyrighted the word Rambo making it unavailable for usage by other filmmakers. As a result, the makers decided on a title change for the film. However, since the films title is already known among the audience the makers decided on dropping a few letters from the title rechristening it to R... Rajkumar.  After the films title change, Shahid Kapoors character in the film, which was Rambo Rajkumar initially, was also changed.  During its filming, Kapoor narrowly avoided serious burns during a stunt sequence.  Song sequence was shot at Great Rann of Kutch. 

After the films release and subsequent success, the Academy of Motion Picture Arts and Sciences, which oversees the famous Academy Awards, sent a formal letter to producer Viki Rajani asking for a copy of the films script to be kept in the Oscar Library. This is the fifth Bollywood film, after Lagaan, Heroes (2008 film), Rock On!!, and Guzaarish, to be requested for this honor by the Academy. Speaking of the request, Viki Rajani said, "Its a privilege that our movie script has been recognized internationally and its an honour for us to be a part of the archives at the Academy Library. The movies was loved by the audience and has done well at the box-office. This is another happy moment for the entire team." Director Prabhu Deva, speaking of the request, said, "Making this movies has been a fun experience and this honour tops it all. What more can I ask for." 

==Soundtrack ==
{{Infobox album
| Name       = R... Rajkumar
| Type       = Soundtrack
| Artist     = Pritam Chakraborty
| Cover      =
| Released   = 6 November 2013
| Recorded   = 2012-2013
| Genre      = Film Soundtrack
| Length     =  
| Label      =  
| Last album = Phata Poster Nikhla Hero (2013)
| This album = R... Rajkumar (2013)
| Next album = Dhoom 3 (2013)
}}
The soundtrack of the film is composed by   was released physically on 6 November 2013. Music recording engineer was Sukumar Dutta
{{Track listing
| headline        = Track listing
| extra_column    = Singer(s)
| total_length    =  
| lyrics_credits  = yes
| title1          = Gandi Baat
| extra1          = Mika Singh, Kalpana Patowary
| lyrics1         = Anupam Amod
| length1         = 4:13
| title2          = Saree Ke Fall Sa (Touch Karke)
| extra2          = Antara Mitra, Nakash Aziz
| lyrics2         = Mayur Puri
| length2         = 3:53
| title3          = Dhokha Dhadi
| extra3          = Arijit Singh, Palak Muchhal
| lyrics3         = Swanand Kirkire, Neelesh Misra
| length3         = 4:09
| title4          = Mat Maari
| extra4          = Kunal Ganjawala, Sunidhi Chauhan
| lyrics4         = Ashish Pandit
| length4         = 4:21
| title5          = Kaddu Katega
| extra5          = Antara Mitra
| lyrics5         = Ashish Pandit
| length5         = 3:31
| title6          = Gandi Baat (Film Version)
| extra6          = Nakash Aziz, Ritu Pathak
| lyrics6         = Anupam Amod
| length6         = 4:26
| title7          = Saree Ke Fall Sa (Touch Karke - Remix)
| extra7          = Antara Mitra, Nakash Aziz, DJ Angel
| lyrics7         = Mayur Puri
| length7         = 3:49
| title8          = R... Rajkumar (Mashup)
| extra8          = DJ Angel
| length8         = 2:23
}}

==Critical reception==

The film opened with mixed reviews from critics.

India Times rated 3/5 stated, "It has some must-haves of a pot-boiler, but misses the real thing - a solid story."  Tushar Joshi of Daily News and Analysis gave 3 out of 5 stars to the film and said,"Watch it if you are in the mood for a entertainer."  Indian express criticized the film saying "R for Rubbish".  Rajeev Masand criticised the film that it is "relentlessly ugly, you have to wonder how it qualifies as entertainment of any sort" and gave a rating "half out of five for Prabhu Devas R... Rajkumar. The half star is strictly for Pritams music".  Anupama Chopra of Hindustan Times called it "cheerfully, boisterously regressive" and also claimed that "The story is both abysmal and exhaustingly loud because when the hero isnt doing pyar, pyar, pyar, hes indulging in maar, maar, maar and men — defying every law of physics — are flying in all directions.  Taran Adarsh of Bollywood Hungama wrote:" R... RAJKUMAR doesnt work. It is Prabhu Dhevas weakest Hindi film to date". 

== Box office ==
Though critics were harsh, the film opened to a warm audience response. Movie was a Hit in domestic circuit but dull in overseas.  

===India===
R... Rajkumar had an average to good opening day occupancy with an average occupancy of 40% with the film performing its best in places like Gujarat, Maharashtra, CP Berar and Rajasthan with 50% occupancy percentage while it lagged in North Indian circuits with 30% occupancy in places like Delhi and Punjab.    The film had a good opening day with collections of  . The second day collections were   and the third day collections were  , taking the weekend total to  .  The first week collections were   nett.     The film collected   on its second Friday and   on its second Saturday taking its 10-day total collection to  .    The film collected   in its second Monday taking its tally to a total of  .  The film collected over   in its second week taking its total to over  . 
The film had lifetime domestic collections of  , and emerged as a "box office hit".    

=== Overseas ===
R... Rajkumar had collections of $1 million in its first weekend overseas.   
  R... Rajkumar collected $1.7 million (₹103 million/₹103&nbsp;million) from overseas market.  

=== Worldwide ===
"R... Rajkumar" had worldwide collection of   after a three weeks run. "R... Rajkumar" had worldwide collection of  . It was declared a hit and was the 11th highest grossing film of 2013 in India. 

== References ==
 

== External links ==
*  
* 
 

 
 
 
 
 
 
 