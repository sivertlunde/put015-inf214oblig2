Emma (1996 theatrical film)
 
{{Infobox film
| name           = Emma
| image          = Emma1996.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Douglas McGrath
| producer       = Patrick Cassavetti Steven Haft
| screenplay     = Douglas McGrath
| based on       =  
| starring       = Gwyneth Paltrow Jeremy Northam Alan Cumming Toni Collette Ewan McGregor
| music          = Rachel Portman Ian Wilson
| editing        = Lesley Walker
| studio         = Matchmaker Films Haft Entertainment
| distributor    = Miramax Films
| released       =  
| runtime        = 120 minutes   
| country        = United Kingdom United States
| language       = English
| budget         = $7 million   
| gross          = $22,231,658 
}} period film novel of the same name by Jane Austen. Directed by Douglas McGrath, the film stars Gwyneth Paltrow, Alan Cumming, Toni Collette, Ewan McGregor, and Jeremy Northam.

==Plot==
The film describes a year in the life of Emma Woodhouse (Gwyneth Paltrow), a congenial but naïve young woman who thinks of herself as a romantic matchmaker in her small community in early-19th-century England.  When her governess, Miss Taylor (Greta Scacchi), gets married and goes to live with her new husband, Mr Weston (James Cosmo), Emma proudly takes the credit for having brought the couple together. Her father and their old family friend (and Emmas brother-in-law) George Knightley (Jeremy Northam) dispute her claim and disapprove of her trying to make more matches, but she ignores their warnings and sets her mind on setting up Mr Elton (Alan Cumming), the minister who performed the Westons marriage ceremony, with Harriet Smith (Toni Collette), an unsophisticated young woman just entering society.

As a close friendship develops between Emma and Harriet, it becomes clear that Harriet is being courted by Robert Martin (Edward Woodall), a farmer who has known Harriet since she was a girl. When Mr Martin proposes to Harriet, she is inclined to accept, but she has come to rely heavily on Emmas advice, and Emma persuades her to reject the proposal. Meanwhile, Mr Elton has been expressing a desire for Emma by taking an interest in a picture she drew of Harriet and by giving her a riddle for a book of riddles being compiled by Harriet. Emma misinterprets this as interest in Harriet, but when Mr Elton and Emma are alone, he fervently declares his love for Emma herself, and she finally realizes her mistake. She harshly rejects his pleas, and he later marries another woman, who turns out to be a vain socialite who competes with Emma for status in the community.

Over the next few months, various gatherings show who loves whom among Emmas friends. Emma is briefly attracted to a charming, gallant young man named Frank Churchill (Ewan McGregor), Mr Westons son who comes to visit from London, but Emma soon decides to set him up with Harriet. However, Frank is revealed to have a secret engagement with a shy, pretty woman named Jane Fairfax (Polly Walker). Harriet states that she has no interest in Frank, preferring Mr Knightley, who was the only man who would dance with her at a party. Mr Knightley only danced with Harriet out of politeness and has started to fall in love with Emma.

The conclusion of the story begins when Emma ridicules a poor woman named Miss Bates (Sophie Thompson) during a picnic, after which Mr Knightley angrily scolds Emma and leaves town for a while. She finds herself thinking about him while hes away, but does not realize she loves him until Harriet expresses interest in him. When Mr Knightley returns, he and Emma cross paths in a meadow and have a conversation that begins awkwardly but ends with him asking her to marry him and her gladly accepting. The news of their engagement upsets Harriet, who avoids Emma for a while, but returns a few weeks later, engaged to Mr Martin. The film ends with Emma and Mr Knightleys wedding.

==Production==

===Conception and adaptation===
Douglas McGrath "fell in love" with Jane Austens 1815 novel Emma (novel)|Emma, while he was an undergraduate at Princeton University. He believed the book would make a great film, but it was not until a decade later that he was given a chance to work on the idea.    After receiving an Academy Award nomination in 1995 for his work on Bullets Over Broadway, McGrath decided to make the most of the moment and took his script idea for a film adaptation of Emma to Miramax Films.  McGrath had initially wanted to write a modern version of the novel, set on the Upper East Side of New York City. Miramaxs co-chairman, Harvey Weinstein, liked the idea of a contemporary take on the novel.  McGrath was unaware that Amy Heckerlings Clueless (film)|Clueless was already in production, until plans for Emma were well underway. 

===Casting=== Flesh and Bone.    Of his decision to bring Paltrow in for the part, McGrath revealed "The thing that actually sold me on her playing a young English girl was that she did a perfect Texas accent. I know that wouldnt recommend her to most people. I grew up in Texas, and I have never heard an actor or actress not from Texas sound remotely like a real Texan. I knew she had theater training, so she could carry herself. We had many actresses, big and small, who wanted to play this part. The minute she started the read-through, the very first line, I thought, Everything is going to be fine; shes going to be brilliant."  Following the read through, the co-chairman of Miramax, Harvey Weinstein, decided to give Emma the green-light. However, he wanted Paltrow to appear in The Pallbearer first, before going ahead and allowing the film to be made.  While she recovered from wisdom-tooth surgery, Paltrow had a month to herself do her own research for the part.    She also studied horsemanship, dancing, singing, archery and the "highly stylized" manners and dialect during a three-week rehearsal period.  
  was picked by the director to portray Emma Woodhouse.]]
Jeremy Northam revealed that when he first tried to read Emma, he did not get very far and was not a fan.    When he read the script for the film, he was initially considered for another role, but he wanted to play George Knightley.  He stated "When I met the director, we got on very well and we talked about everything except the film. At the end of it, he said he thought Knightley was the part for me, so I didnt have to bring up the issue at all."  Northam added that Knightleys faith in Emma becoming a better person was one of the reasons he loved the character.  Australian actress Toni Collette was cast as Harriet Smith.    Collette also struggled to get into the Austen books when she was younger, but after reading Emma, which she deemed "warm and witty and clever", she began to appreciate them more.  Collette had to gain weight to portray "the Rubenesque Harriet" and she explained "I think its important for people to look real in films. Theres a tendency to go Barbie doll and I dont agree with that at all." 

Ewan McGregor was cast as Frank Churchill. He told Adam Higginbotham from The Guardian that he chose to star in Emma because he thought it would be something different from his previous role in Trainspotting (film)|Trainspotting.    McGregor later regretted appearing in the film, saying "My decision-making was wrong. Its the only time Ive done that. And I learnt from it, you know. So Im glad of that - because it was early on and I learnt my lesson. Its a good film, Emma, but Im just... not very good in it. Im not helped because Im also wearing the worlds worst wig. Its quite a laugh, checking that wig out."  Real-life mother and daughter, Phyllida Law and Sophie Thompson, portrayed Mrs and Miss Bates.    Thompson revealed that it was a coincidence that she and her mother were cast alongside each other, as the casting director had their names on separate lists.  McGrath initially believed Thompson to be too young to play Miss Bates, but he changed his mind after seeing her wearing glasses with her hair down. 

Alan Cumming appeared as Reverend Philip Elton, who falls in love with Emma.  Cumming wrote on his official website that the friendship that developed between himself and McGrath was one of the most memorable things about his time working on the film.    He went on to state that the worst thing about the shoot was his hair, which had been lightened and curled for the character.  Juliet Stevenson portrayed the "ghastly" Mrs Elton, while Polly Walker and Greta Scacchi starred as Jane Fairfax and Anne Taylor respectively.    Other cast members included Edward Woodall as Robert Martin, James Cosmo as Mr Weston and Denys Hawthorne as Mr Woodhouse, in one of his last film appearances.  

===Costume design===
  Ruth Myers created and designed the clothing for the film.    She wanted to mirror the lightness of the script within the costumes and give "a spark of color and life" to the early 19th century setting.    During her research, Myers noted a similarity between the fashions after the Napoleonic Wars and the 1920s in fashion|1920s, saying that they had "the same sort of flapperish quality".  The designer explained "The moment I set to research it, more and more it kept striking me what the similarities were between the two periods. It was a period of freedom of costume for women, and it was a period of constant diversions for the upper classes–picnics, dinners, balls, dances. What I wanted to do was make it look like the watercolors of the period, which are very bright and very clear, with very specific colors." 

Myers went on to reveal that she did not want the costumes to have a "heavy English look" and instead she wanted "to get the freedom of bodies that you see in all the drawings, the form of the body underneath, the swell of the breasts."  Myers told Barbara De Witt from the Los Angeles Daily News that using pastel-colored clothing to get the watercolor effect was one of her major challenges during the production.  The designer was later criticised for being inaccurate, but she stated that she did not want the costumes to look old or Sepia (color)|sepia.    Myers only had five weeks in which to create 150 costumes for the production, and she was constantly working on the set. 

Emmas wedding dress was made from silk crepe and embroidered with a small sprig pattern, while the sleeves and the train were made from embroidered net.    Of the dress, Myers stated "The inspiration for Emmas wedding dress began with a small amount of exquisite vintage lace that became the overlay. I wanted a look that would work not only for the period but also one that would compliment Gwyneth Paltrows youth, swan neck, and incredible beauty. I was also hoping to evoke happiness and the English countryside; the sun did shine on the day we shot the scene!" 

===Music===
{{Infobox album  
| Name        = Emma  
| Type        = Soundtrack
| Artist      = Rachel Portman
| Released    = July 29, 1996
| Genre       = Film score
| Label       = Hollywood Records
| Length      = 42:45
| Last album  = Marvins Room (film)|Marvins Room (1996)
| This album  = Emma (1996)
| Next album  =   (1997)
| Misc        = 
}}
The musical score of the film was written by British composer Rachel Portman. It was released on July 29, 1996.  Portman told Rebecca Jones from the BBC that her score was "purely classical". She continued "It is an orchestral piece, by which I mean that there is nothing in it that you wouldnt find in a symphony orchestra. It was influenced by my roots and my classical background."  Portman used various instruments to give a voice to the characters. She revealed that "a quivering violin" would represent Harriets uneasy stomach, while "a bittersweet clarinet" would accompany Emma though her emotional journey.    Josh Friedman from the Los Angeles Times believed Portmans "crafty score guides the audience through the heroines game playing, and ultimately, to her romantic destiny."  He also thought the music had "a sneaky, circular feel". 

Playbills Ken LaFave commented that the score "underlined the period romanticism" in Emma and contained a "string-rich, romantic sound".  Jason Ankeny, a music critic for Allmusic, wrote that Portmans score to Emma employed all of her "signatures" like "whimsical yet romantic melodies, fluffy string arrangements, and woodwind solos", which would be familiar to anyone who had listened to her previous film scores.    He stated, "it seems as if shes simply going through the motions, content to operate within the confines of an aesthetic that, admittedly, is hers and hers alone. By no means a bad score, Emma is nevertheless a disappointment – if youve heard a previous Rachel Portman score, youve pretty much heard this one as well."  On March 24, 1997, Portman became the first woman to win the Academy Award for Best Original Score.  The album contains 18 tracks; the first track is "Main Titles", and the final track is "End Titles". 

==Comparisons with the novel==
Although in general staying close to the plot of the book, the screenplay by Douglas McGrath enlivens the banter between the staid Mr Knightley and the vivacious Emma, making the basis of their attraction more apparent.

Austens original novel deals with Emmas false sense of class superiority, for which she is eventually chastised.  In an essay from Jane Austen in Hollywood, Nora Nachumi writes that, due partly to Paltrows star status, Emma appears less humbled by the end of this film than she does in the novel. 

While the movie trailer shows Emma in love with John and Frank: "Emma loves John" and "Emma loves Frank" are both displayed over film from the movie, Mr Knightleys given name is George.  It is George Knightleys brother who is named John, making this an error in the trailer.

==Reception==

===Critical response=== review aggregation rating average of 7.1 out of 10.  Metacritic, which assigns a score of 1–100 to individual film reviews, gave Emma an average rating of 66 based on 22 reviews. 

Ken Eisner, writing for Variety (magazine)|Variety, proclaimed "Gwyneth Paltrow shines brightly as Jane Austens most endearing character, the disastrously self-assured matchmaker Emma Woodhouse. A fine cast, speedy pacing and playful direction make this a solid contender for the Austen sweepstakes." 

===Accolades===
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Category
! Recipients and nominees
! Result
|-
| rowspan="2" | Academy Awards  Best Costume Design
| Ruth Myers
|  
|- Best Original Score
| Rachel Portman
|  
|-
| London Film Critics Circle 
| British Actor of the Year
| Ewan McGregor 
|  
|-
| Satellite Awards  Best Performance by an Actress in a Comedy or Musical
| Gwyneth Paltrow 
|  
|-
| USC Scripter Award 
| USC Scripter Award
| Douglas McGrath, Jane Austen 
|  
|-
| Writers Guild of America  Best Adapted Screenplay
| Douglas McGrath
|  
|-
|}

==See also==
 
* "Silent Worship" (song sung by Emma and Mr Churchill)

 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 