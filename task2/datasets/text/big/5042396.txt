Raavan
 
 
 
 

{{Infobox film
| name           = Raavan
| image          = Raavanposter2.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Mani Ratnam
| producer       = Mani Ratnam
| screenplay     = Mani Ratnam
| story          = Valmiki Vikram Abhishek Govinda Priyamani Ravi Kishan Nikhil Dwivedi
| music          = A. R. Rahman
| cinematography = Santosh Sivan V. Manikandan
| editing        = A. Sreekar Prasad
| studio         =  ShowMan Pictures Madras Talkies BlackMan Pictures
| released       =  
| runtime        = 131 minutes 
| country        = India Hindi
| budget         =   
| gross          =   
}} Aishwarya Rai Vikram in Tamil as Telugu  soundtrack is composed by A. R. Rahman.   The film was released on 18 June 2010. The films premiere was held in London on 16 June 2010. 

==Plot==
The film opens with shots showing Beera Munda (Abhishek Bachchan), a bandit, jumping off a cliff into the water. His gang is busy distracting the police and a few police vehicles are set ablaze.

Ragini Sharma (Aishwarya Rai), who is on a boating trip, is kidnapped by Beera. Dev Pratap Sharma (Vikram (actor)|Vikram), her husband and a superintendent of police, is informed of her abduction.

A brief montage of shots showcases the story of Beera. He is seen as a local hero, a sort of Robin Hood - who runs a parallel government, with his brothers - Mangal (Ravi Kishan) and Hariya (Ajay Gehi). Though considered a terrorist by the local police, Beera is respected by the villagers. He kidnaps Ragini hoping to avenge the death of his sister Jamuni (Priyamani). Ragini refuses to die at the hands of a bandit and jumps off the cliff, but is unable to kill herself. This prompts Beera to postpone her killing, as it is useless to kill someone who has no fear of death.

Dev and his team enter the forests with the aid of Sanjeevani Kumar (Govinda (actor)|Govinda), a forest guard. Despite searching deep in the forests, Dev is unable to hunt down Beera. Meanwhile, Beera and Mangal infiltrate the police tents when Dev is not present. By chance they stumble upon Inspector Hemant (Nikhil Dwivedi), Devs junior and his assistant. Beera kidnaps Hemant and takes him to his hideout, where Mangal shaves off his hair, snatches all his clothes and pin him deep into the soil, only his head sticking out from the ground.

Ragini discovers Hemant in this condition and reproaches Beera and Mangal for such inhuman acts. Beera eventually tells Ragini the story of his sisters death; Dev had led an encounter against Beera during Jamunis wedding. Devs shot grazed Beera in the neck. Beera escaped, but Hemant captured and took Jamuni to the police station. She was kept in police custody all night and was serially raped by the policemen, when she refused to reveal Beeras whereabouts. Beera returns home to find Jamuni distraught and traumatized. The next day, she commits suicide by drowning in a nearby well.

Hearing Beeras history and the reason for his blood-thirst, Ragini feels sorry for him. Eventually, they both seem to develop some weakness for each other. While Beera reveals his feelings openly to Ragini, she tries to hide hers as she is still not sure about them.

Meanwhile, Sanjeevani Kumar sneaks into the place where Ragini was kept under watchful protection. He tells her of Devs relentless search for her, but at that moment Mangal comes from behind and overpowers and captures Sanjeevani. He is taken as prisoner in front of Beera, whom he warns of the consequences of keeping another mans wife in his house. He also tells him to return Ragini to Dev, or be responsible for the destruction of his people. Beera bluntly refuses to follow his advice.

Unhappy that his brother is preparing for war, Hariya convinces Beera to allow him to offer a truce to Dev. Hariya goes with Sanjeevani to their camp. Dev initially seems to agree, but when Hariya comes out in the open, Dev shoots him to death. An enraged Beera sets the police tents on fire by an ambush attack with his gang. Dev and Beera come face to face for a final confrontation on an old, rickety mountain bridge. Dev fights Beera with brute force, but Beera manages to outwit him. He almost lets Dev fall to his death but saves him because his wife was waiting for him. Beera releases Ragini and leaves. Dev and Ragini re-unite after fourteen days. However, Dev yells at an invisible Beera, vowing to come back and destroy him.

While riding a train back to their hometown, Dev accuses Ragini of infidelity and informs her that it was Beera who told him so. Furious, Ragini leaves Dev to meet Beera through Mangal. When she does, she asks him what Beera had told Dev. Beera replies that he had said he protected Ragini for all the fourteen days and nothing else. They quickly realize Dev lied, hoping Ragini would lead him to his hideout. Dev appears with a police team and confront the duo, reminding Beera of the vow. Ragini stands in front of Beera to save him, but Beera pushes her out of the line of fire. He is shot multiple times. Raginis true feelings comes to surface when she tries to save Beera with all her might. Content that Ragini too has feelings for him, Beera falls off the cliff to his death with a smile.

==Cast== Vikram as Dev Pratap Sharma Govinda as Sanjeevani Kumar
*Abhishek Bachchan as Beera Munda
*Aishwarya Rai Bachchan as Ragini Sharma
*Ravi Kishan as Mangal   
*Priyamani as Jamuni   
*Nikhil Dwivedi as Inspector Hemant 
*Ajay Gehi as Hari/Hariya 

==Production== Manikandan was hired as the films DOP or cinematographer, however he walked out in May 2009 and was replaced by Santosh Sivan.  
 tandav dance between Abhishek and Aishwarya for the film.   Indian fashion designer Sabyasachi Mukherjee designed Aishwarya Rais costumes in the film. 

Raavan has been shot in numerous locations around India including the forests of Karnataka (Tumkur), Kerala (Athirappilly Falls),  Ooty, Dharmapuri (Hogenakkal Falls), Jhansi, Kolkata, Mahabaleshwar and in the Malshej Ghats in Maharashtra.  

==Reception==

===Critical response===
  IBN gave the film 1.5/5 and said, "Despite some eye-watering camerawork and a stunning action piece in the films climax, the film -- especially its first half -- is a carelessly edited mess of long scenes that make little sense when strung together".  Noyon Jyoti Parasara of AOL rated it 2.5/5 and stated "  Raavan is more a choreographed musical-cum-psychological drama but without proper character backing. What makes the movie worth watching is the peaks in the second half, of course apart from the imagery."  Taran Adarsh of Bollywood Hungama rated it 1.5/5 and said, "On the whole, Raavan is a king-sized disappointment, in terms of content".  Sukanya Venkatraghavan of Filmfare rated the film 3/5 and said, "Raavan has its moments but it lacks depth. The first half is fairly riveting but the second half slowly slips into a coma".  Nikhat Kazmi of Times of India rated it favourably at 3.5/5 saying, "There are enough punches in the second half to keep the momentum going, but by and large, the film scores mostly on art and aesthete".  Raja Sen of Rediff rated it 2/5 and said, "Raavan truly and tragically fails us is in taking one of our greatest epics, and making it unforgivably boring".  Parimal Rohit of Buzzine Bollywood said, "Raavan is ultimately a clever film, as it pushed the envelope on how one goes about defining who is good and who is evil." 

Among US and UK film critics sampled on the Rotten Tomatoes aggregate site, Raavan rated 64%, with eleven reviews.  Cath Clarke of The Guardian gave the film a rating of 2/5 and found it sexist,  while New York Post critic Lou Lumenick wrote, "If youre not a fan of Bollywood movies – which have long resisted crossover attempts in this country despite the success of hybrids such as Slumdog Millionaire — Mani Ratnams action melodrama Raavan probably isnt going to make a convert out of you." {{cite web|title=Raavan Film Review|work= New York Post| date =18 accessdate =18 naturalistically at a wedding, the other essentially a stunning war dance." 

The New York Times and the Los Angeles Times likewise gave it positive reviews: Rachel Saltz of the former made it a Times "Critics Pick" and lauded Ratnam as "a talented visual storyteller who directs action crisply and fills the screen with striking images" including "an eye-popping climactic battle",  while Kevin Thomas of the latter said the film "is replete with dizzying camerawork, myriad complications, violent mayhem, broad humor,   usual musical interludes, a cliffhanging climactic confrontation and a finish that strikes a note of poignancy." 

===Box office=== Rs 60 million on its opening day.  and 238.3&nbsp;million through its first week.  In North America, Raavan opened in 120 theaters and ranked No. 15 on the domestic weekend box office chart with $482,760. As of 24 June, it had collected $573,314 in this market.  The film was declared a flop by Box Office India. 

==Soundtrack==
 
The soundtrack for the film is composed by A. R. Rahman with lyrics penned by Gulzar (lyricist)|Gulzar. It features six songs and an additional song that was performed by Rahman at the audio launch. It was released on 24 April 2010 by T-Series. It features 6 songs composed by A. R. Rahman with lyrics penned by Gulzar. During the audio release, an additional track was performed by Rahman, titled "Jaare Ud Jaare", which was not included in the CD. The song was cited to be an "instant composition" by Rahman, "The night before the launch, Rahman closeted himself in his Mumbai studio and worked through the night to compose the song", the source adds. This song is believed to be included in the later stages.  The soundtrack also features three more additional songs that were featured in the movie. However the official track listing has only 6 songs.
The soundtrack is especially noted for the use of rich instruments, Indian as well as Middle Eastern.

==Awards and nominations==
;6th Apsara Film & Television Producers Guild Awards
Won 
* Apsara Award for Best Cinematography - V. Manikandan (shared with Guzaarish)
* Apsara Award for Best Re-recording - Tapan Nayak
* Apsara Award for Best Visual Credits - Srinivas Karthik Kotamraju
* Apsara Award for Best cinematography - Santosh Sivan

Nominated 
* Apsara Award for Best Performance in a Supporting Role (Female) - Priya Mani
 2011 Zee Cine Awards
Nominated  Best Actor in a Negative Role - Abhishek Bachchan

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 