Memphis Belle (film)
{{Infobox film
|  name           = Memphis Belle
|  image          = Memphis belle poster.jpg
| caption     = Theatrical Release Poster
| alt          = 
| director    = Michael Caton-Jones
| producer    = David Puttnam, Catherine Wyler
| writer      = Monte Merrick
| starring    = Matthew Modine Eric Stoltz Tate Donovan D. B. Sweeney Billy Zane Sean Astin Harry Connick Jr. Reed Diamond Courtney Gains Neil Giuntoli David Strathairn John Lithgow Jane Horrocks Mac McDonald
| music       = George Fenton Jim Clark
| distributor = Warner Bros.
| released    =   
| runtime     = 110 minutes
| country     = United Kingdom 
| language    = English 
| budget      = $23,000,000 (estimated)
| gross       = $27,441,977 (USA) 
|
}} Memphis Belle, based in England during World War II.  The 1990 version was co-produced by David Puttnam and Wylers daughter Catherine, and dedicated to her father. The film closes with a dedication to all airmen, friend or foe, who fought in the skies above Europe during World War II.

==Plot==
In May 1943, the crew of the Memphis Belle, a Boeing B-17 Flying Fortress of the US Army Air Force, are grounded in England while their aircraft is repaired. 

The group is under the command of Col. Craig Harriman (David Strathairn), a no-nonsense, stoical leader, tasked with keeping the pressure on Nazi targets. An Army publicist, Lt. Col. Bruce Derringer (John Lithgow) is visiting the base, to interview the Belle crew in anticipation of their flying their 25th mission, a requirement to complete their tour of duty.

Derringer is eager to use the crew on a war bonds tour stateside. He believes their success would help the war effort, and confides to Harriman that many people back home are upset at the losses the Air Force has suffered. Some are beginning to think daylight bombing is ineffective, while Harriman openly favors it.

The officers of the squadron are informed the target for the day will be Bremen, Germany. After a delay due to poor weather over the target, the Memphis Belle and her squadron are airborne. They are soon assembled in formation with bomb group and their escort of North American P-51 Mustang fighters. They face frequent harassment by defending German Messerschmitt Bf 109 fighters along the way. Eventually the escorting fighters, low on fuel, turn away, while the bombers continue alone.

Back at the base, Harriman and Derringer have harsh words after the latter starts decorating the mess hall for a celebration upon the Belles return. Derringer accuses Harriman of being cold-hearted and concerned only with advancing his military career. In response, Harriman brings Derringer into his office and angrily dumps a pile of letters on his desk and forces Derringer to read some of them. They are replies Harriman received from the grieving families of lost airmen. 

Meanwhile, the bombers have suffered significant losses. The German interceptors focus their attacks on the leading aircraft in the formation. The first lead bomber, Windy City, loses its engines and explodes in front of the Belle, piloted by Captain Dennis Dearborn (Matthew Modine) and co-pilot 1st Lt. Luke Sinclair (Tate Donovan). More attacks ensue, and the replacement lead bomber, C Cup, is forced to break formation when its nose is crippled by a Bf 109. The crew of the Belle watch in horror when they see an airman fall out of the stricken aircraft without a parachute. The Belle is then tasked to lead the formation to the target.

Finding the target – an aircraft assembly plant – becomes difficult as bombardier 1st Lt. Val Kozlowski (Billy Zane) cant see the target because of a smokescreen the Germans had created. Dearborn aborts the initial bomb run and orders the formation to circle around for a second attempt, which frustrates the Belle crew who have had to endure the ongoing attacks from the Germans. On the second run, Kozlowski spots the assembly plant through a gap in the smokescreen and the bombers successfully hit their target. Once clear of the anti-aircraft fire, they are again attacked by fighters. Staff Sgt. Richard "Rascal" Moores (Sean Astin) ball turret is destroyed, but he is saved by the safety strap. The attacks continue, blasting a hole in the fuselage, tearing off a large chunk of the tail, and setting the number four engine on fire. Radio operator Staff Sgt. (T/3) Danny "Danny Boy" Daly (Eric Stoltz), is wounded in the attacks, which puts Kozlowski in a difficult position when the crew enlists his medical "expertise" to save the injured radioman. Meanwhile, Dearborn and Sinclair skillfully drop the aircraft into a steep dive and put the fire out, despite great risk of losing the aircraft.

As the B-17 limps closer to base, Dearborn orders the landing gear dropped. Only one wheel descends due to electrical failure caused by battle damage, but the crew are able to manually lower the malfunctioning wheel just before landing. The ground crew and a humbled Lt. Colonel Derringer race to greet them with jubilant cheers. The crew exits the battered bomber and celebrate their victory with Daly on an ambulance.

==Cast==
* Matthew Modine as Captain Dennis Dearborn, pilot: A humorless and socially inept perfectionist.
* Tate Donovan as 1st Lt. Luke Sinclair, co-pilot: The carefree former lifeguard believes himself to be undervalued by Dearborn.
* D.B. Sweeney as 1st Lt. Phil Lowenthal, navigator: Lowenthal is nervous before their final mission and, convinced that he is doomed to die.
* Billy Zane as 1st Lt. Val Kozlowski, bombardier: While everyone believes the confident, self-assured Kozlowski to be a doctor, it is later revealed he attended only two weeks of medical school before enlisting.
* Eric Stoltz as Staff Sgt. (T/3) Danny "Danny Boy" Daly, radio operator: An earnest Irish-American, Daly was editor of the school paper, a valedictorian, and joined up right after graduating college.
* Reed Diamond as Staff Sgt. (T/3) Virgil "Virge" or "Virgin" Hoogesteger, top turret gunner and flight engineer: Hoogesteger worked for his familys restaurant and plans to open a chain of identical restaurants after the war despite his crew-mates warnings that such an unheard of enterprise could never succeed.
* Sean Astin as Staff Sgt. Richard "Rascal" Moore, ball turret gunner: The diminutive, often crude gunner considers himself a ladies man and delights in teasing his crew mates.
* Courtney Gains as Staff Sgt. Eugene "Genie" McVey, right waist gunner. A religious and superstitious Irish-American.
* Neil Giuntoli as Sgt. Jack Bocci, left waist gunner: A hot-tempered Chicago hoodlum, Bocci appears to look out only for himself but proves surprisingly kind to his fellow waist gunner McVey. 
* Harry Connick, Jr. as Staff Sgt. Clay Busby, tail gunner: After his father lost the family farm in a poker game, the laconic Busby earned money playing the piano in a New Orleans cathouse.
* David Strathairn as Col. Craig Harriman
* John Lithgow as Lt. Col. Bruce Derringer

==Production==
 
Five real B-17s were rounded up for the filming of Memphis Belle, out of eight that were airworthy during the late 1980s. Two were located in America (N3703G and N17W), two were in France (F-BEEA and F-AZDX The Pink Lady (aircraft)|The Pink Lady), and one (G-BEDF Sally B) in England. Since the original Memphis Belle was a B-17F model, almost all of the B-17s used in the film were heavily modified to look like earlier F models, having chin turrets removed, new tail turrets installed and being painted olive drab green. During filming, two B-17s portrayed the Belle (one was the movie version of theMemphis Belle (N3703G) and the other was Sally B for scenes requiring pyrotechnics such as smoke and sparks indicating machine gun "hits") while the rest had nose art and squadron markings changed numerous times to make it appear there were more aircraft. 
 extras for the film were obtained from auditions held in the area, and included current and former members of the Royal Air Force. The filmmakers also used Pinewood Studios to shoot interior scenes and to shoot various models of B-17s. 

A North American B-25 Mitchell was used to film the majority of the aerial scenes with several fixed and trainable cameras also mounted on B-17s and fighter aircraft for action shots. A Grumman TBM Avenger (with its tail section painted the same olive drab tones used on the B-17s) was used as back-up for a short time when the B-25 became unserviceable during filming. 

The film pilots were warbird display pilots coming from the UK, USA, France, Germany, New Zealand and Norway, the roster changing several times as pilots had to return to their full-time jobs during filming. The flying sequences were devised and planned under the co-ordination of Old Flying Machine Company (OFMC) pilots Ray Hanna and his son Mark, who also acted as chief pilots for the fighter aircraft used and flew the camera-equipped fighter and TBM Avenger aircraft during filming. 

===Historical accuracy===
With the exception of the aircraft names, this film is fiction based only very loosely on fact. The characters are composites, the names are not those of the real crew of the Memphis Belle and the incidents shown are supposed to be representative of B-17 missions in general. Indeed, the characters and situations of the film bear little resemblance to the crew of the actual Memphis Belle, the nature of her final mission, the accuracy of strategic bombing, or Allied policy on the bombing of civilians. No optimistic official celebration on the evening before the Belles 25th mission occurred, and there was no special welcome for the crew when the mission was over.The final, 25th mission of the real Belle was to Kiel, Germany, before being flown back to the United States. 

For the fighters, seven P-51 Mustangs were used, five of the P-51s were painted in the markings of the first USAAF Merlin-engined Mustang squadron to operate in Britain (a few months later in 1943 than the actual mission).  As there were no surviving flyable Messerschmitt Bf 109s, Luftwaffe fighter aircraft were represented by Hispano Aviación HA-1112|Ha-1112s, a Spanish version of the Bf 109 (which were also used to represent Bf 109s in the 1969 film Battle of Britain (film)|Battle of Britain) in mid-war generic paint schemes.

===Notable filming incidents===
One of the French B-17s, (F-BEEA) used as a filming platform hit a tree and a pile of gravel during takeoff from Binbrook and was destroyed by the subsequent fire. The crew of 10 escaped, with two of them suffering serious injury and three suffering minor injury. While lined up on runway 21 awaiting takeoff, a puff of smoke was observed by a ground engineer from the vicinity of engine three which he conjectured could have been due to an overboost. The aircraft commenced its ground roll and after about 100 yards swung slightly to the left, which the commander (the handling pilot) corrected with the rudder and by reducing power to number 3 and 4 engines. Once corrected full power on all engines was resumed but the aircraft swung right. The commander applied corrective rudder and reduced power to number 1 and 2 engines, but this was not immediately effective and the aircraft left the runway before straightening, parallel to the runway. Knowing that the aircraft was capable of being operated from grass landing strips, the pilot opted to continue the take off; however, after 4-500 yards and at an airspeed of 90-95 mph the aircraft swung right and its course was obstructed by a tree which hit the left wing and a pile of gravel which hit the number 4 propeller. The aircraft yawed to the right and came to rest in a cornfield; the fuselage broke into two places aft of the bomb bay and caught fire.&nbsp;   Air Accident Investigation Branch. Retrieved: October 27, 2014. 

==Soundtrack==
{{Infobox album|  
| Name       = Memphis Belle (Original Motion Picture Soundtrack)
| Type       = soundtrack
| Artist     = George Fenton
| Cover      = Memphis Belle Motion Picture Soundtrack.jpg
| Released   = October 1, 1990
| Recorded   = Abbey Road Studios, CTS Studios, and Angel Studios, London, England
| Genre      = Soundtrack
| Length     = 42:47
| Label      = Varèse Sarabande
| Producer   = George Fenton Eliza Thompson
| Last album =
| This album =
| Next album =
}}
 BAFTA award for Best Original Film Score in 1991.

The Original Motion Picture Soundtrack album was recorded at Abbey Road Studios, CTS Studios, and Angel Studios in London, England.  Included is Glenn Miller and His Orchestra performing "I Know Why (And So Do You)".

Track listing
# " ) - 3:50 Green Eyes" (Nilo Menendez, Eddie Rivera, Eddie Woods) - 3:25
# "Flying Home" (Benny Goodman, Lionel Hampton, Sydney Robin) - 2:57
# "The Steel Lady" (George Fenton|Fenton) - 1:44
# "Prepare For Take Off" ("Amazing Grace") (traditional) - 2:39
# "The Final Mission" (George Fenton|Fenton) - 3:51
# "With Deep Regret..." (George Fenton|Fenton) - 2:02
# "  and His Orchestra
# "The Bomb Run" (George Fenton|Fenton) - 1:30
# "Limping Home" (George Fenton|Fenton) - 2:25
# "Crippled Belle: The Landing" (George Fenton|Fenton) - 3:26
# "Resolution" (George Fenton|Fenton) - 1:06
# "Memphis Belle" (End Title Suite) (George Fenton|Fenton) - 7:37 Frederic E. Weatherly) - 3:20 - performed by Mark Williamson

==Reception==
Memphis Belle received mixed reviews, with Roger Ebert stating the film was "entertaining" yet filled with familiar wartime cliches. "This human element in the experience of the "Memphis Belle" crew somehow compensates for a lack of human dimension in the characters. We cant really tell the crew members apart, and dont much care to, but we can identify with them."  UK film reviewer Andy Webb had similar reservations. "Despite its good intentions to highlight the risks and heroics of the brave men who flew dangerous bombing missions deep into enemy soil during World War II, the one thing which you cant miss about "Memphis Belle" is that it is a cliche commercial production." 

==See also==
* Strategic bombing during World War II

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Carlson, Mark. Flying on Film: A Century of Aviation in the Movies, 1912–2012. Duncan, Oklahoma: BearManor Media, 2012. ISBN 978-1-59393-219-0.
* Farmer, James H. Celluloid Wings: The Impact of Movies on Aviation. Blue Ridge Summit, Pennsylvania: Tab Books Inc., 1984. ISBN 978-0-83062-374-7.
* Farmer, James H. "The Making of Memphis Belle." Air Classics, Volume 26, Number 11, November 1990.
* Morgan, Robert, and Ron Powers. The Man Who Flew the Memphis Belle. New York: Penguin Putnam, 2001. ISBN 0-451-20594-4.
* Orriss, Bruce. When Hollywood Ruled the Skies: The Aviation Film Classics of World War II. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 