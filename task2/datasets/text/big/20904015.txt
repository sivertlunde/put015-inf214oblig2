Anand Ashram
{{Infobox film
| name           = Anand Ashram
| image          = 
| image_size     = 
| caption        = 
| director       = Shakti Samanta
| producer       = Shakti Samanta
| writer         =Shakti Samanta Kamleshwar  (dialogue-Hindi)  Prabhat Roy  (dialogue-Bengali)  story = Sailajananda Mukhopadhyay
| narrator       = 
| starring       =Ashok Kumar Uttam Kumar  Sharmila Tagore
| music          = Shyamal Mitra
| cinematography = Aloke Dasgupta
| editing        = Bijoy Chowdhary	 
| distributor    =
|studio= Natraj Studios 
| released =  
| runtime        = 2:20:46
| country        = India Bengali
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 double Hindi and Bengali written and directed by Shakti Samanta, based on a story by Bengali film director and novelist, Sailajananda Mukhopadhyay. It starred Ashok Kumar, Uttam Kumar and Sharmila Tagore in lead roles. Samanta has previously made another double version, Amanush (1975) with the same lead actors, however this film didnt do well at the box office. 

==Plot==
Dr. Deepak lives with his wealthy father, a Thakur, in a small town in India. While the Thakur would like to get Deepak to marry a woman from an equally wealthy family, he has fallen in love with a poor woman named Asha, and would like to marry her. The Thakur is displeased, and asks Deepak to leave his house, never to return. A few months later, Thakurs employee, Girdhari, informs him that he has found an abandoned child by the river bank, and would like to adopt him. Initially the Thakur does not want anything to do with the child, but when he sees it for the first time, he decides to let Girdhari have his way. Years later, young Pratap has grown up under Girdhari and Thakurs care, and travels abroad to study in order to be a doctor. When he returns he decides to work with a chemical company. His travels take him to meet a beautiful woman named Kiran, her father, and an elderly man named Doctor. Pratap is perturbed by this doctor, who seems to know him very well, and wants him to leave his job at the chemical company, and start practicing medicine in the small town where they live. What Pratap does not know that this doctor is none other than his biological father, Deepak. What both dont know is what steps the Thakur will take when he finds out that Pratap is none other than his grandson.
==Cast==
*Uttam Kumar ...  Dr. Deepak
*Sharmila Tagore ...  Asha
*Rakesh Roshan ...  Dr. Pratap
*Moushumi Chatterjee ...  Kiran
*Ashok Kumar ...  Thakur Asit Sen ...  Girdhari
*Utpal Dutt ...  Kirans dad
* Anita Guha
* Chandrima Bhaduri
*Prema Narayan (Guest appearance)
*Alankar Joshi ...  Chandan (as Master Alankar)

==Music==

The film had music by Shyamal Mitra, with lyrics by Indeevar
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#eeeeee" align="center"
! Song !! Singer (s)!! Duration
|-
| Tum Itni Sundar Ho
| Yesudas, Preeti Sagar 
|3:55
|-
| Sara Pyar Tumhara
| Asha Bhonsle, Kishore Kumar  
|3:58
|-
| Tere Liye Maine Sabko Chhoda
| Kishore Kumar
|4:01
|-
| Raahi Naye Naye Rasta Naya Naya    Kishore Kumar 
|3:40
|-
| Safal Wohi Jeevan Hai   
| Shyamal Mitra 
| 3:38
|-
| Jab Chaho Chali Aaoongi     Lata Mangeshkar 
|3:49
|}

== References ==

 

==External links==
*  
*  
*  

 
 
 
 
 

 