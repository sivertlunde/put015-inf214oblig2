The Express
{{Infobox film
| name = The Express
| image = Expressposter08.jpg
| caption = Theatrical release poster
| director = Gary Fleder John Davis
| writer = Charles Leavitt
| based on = Ernie Davis: The Elmira Express by Robert C. Gallagher Rob Brown Charles S. Dutton Dennis Quaid
| music = Mark Isham
| cinematography = Kramer Morgenthau
| editing = Padraic McKinley William Steinkamp
| studio = Relativity Media Davis Entertainment
| distributor = Universal Pictures
| released =  
| runtime = 130 minutes
| country = United States
| language = English
| budget = $40 million   
| gross = $9,808,124 
}}

The Express (also known as The Express: The Ernie Davis Story) is a 2008 American    . 

The motion picture was a co-production between the film studios of Relativity Media and Davis Entertainment. It was commercially distributed by Universal Pictures theatrically, and by Universal Studios Home Entertainment for home media. Following its cinematic release, it failed to garner any award nominations from mainstream motion picture organizations for its production merits or lead acting. In the film, veteran actors Dennis Quaid and Charles S. Dutton star in principal supporting roles. The original motion picture soundtrack with a musical score composed by Mark Isham, was released by the Lakeshore Records label on October 28, 2008.

The Express premiered in theaters nationwide in the United States on October 10, 2008 grossing $9,793,406 in domestic ticket receipts. It earned an additional $14,718 in business through international release to top out at a combined $9,808,124 in gross revenue. Since the film had a $40 million budget, it was a financial failure. However, preceding its initial screening in cinemas, the film was generally met with positive critical reviews. The Blu-ray Disc|Blu-ray version of the film, featuring deleted scenes and the directors commentary was released on January 20, 2009.

==Plot== Rob Brown) Michael Mannix). 

Several years later, Syracuse University football head coach Ben Schwartzwalder (Dennis Quaid) searches for a running back to address the absence of Jim Brown (Darrin Dewitt Henson), the graduating player completing his All-American senior season. Schwartzwalder is impressed with Davis after viewing footage of him playing for Elmira Free Academy. Schwartzwalder convinces Brown to accompany him on a recruiting visit to see Davis and his family in hopes of luring him to sign with Syracuse. After their visit, Davis decides to enroll at Syracuse and spurns the recruiting efforts of other colleges. 

At the start of the 1959 college football season, Davis immediately excels playing for the varsity team, to lead Syracuse to victories over several college football teams. After Syracuse defeats UCLA to conclude the regular season undefeated, the team decides by choice to play the 2nd ranked Texas Longhorns in the Cotton Bowl Classic. During the game on January 1, 1960, Davis boldly attempts to lead his team to victory but is hampered by an injured leg and biased officiating. Towards the end of the game, Davis scores a crucial touchdown to preserve a Syracuse lead. The matchup concludes with a victory for Syracuse, and its first national championship. 

In 1961, Davis goes on to win the Heisman Trophy following his senior season in college. He later becomes a professional athlete in the National Football League and signs a contract with the Cleveland Browns. Later however, following a series of health concerns, Davis is taken to a hospital to undergo medical testing. During a routine practice session, team owner Art Modell (Saul Rubinek) informs Davis he will be unable to play the upcoming season due to his condition. Subsequently, Davis holds a press conference and announces he has been diagnosed with leukemia. The Cleveland Browns honor Ernie by allowing him to suit up in uniform and join the team while running out before a televised game. 

The films epilogue displays a series of graphics stating that Davis died on May 18, 1963 at the age of 23; while in condolence, President Kennedy expresses sympathy for Davis fine character as a citizen and an athlete. 

==Production==

===Development===
The premise of The Express is based on the true story of Ernie Davis, the charismatic athlete who became the first African American to win the Heisman Trophy, college footballs greatest achievement. Excelling in high school football, Davis was later recruited by dozens of predominantly white universities. A local sports columnist dubbed him the Elmira Express.    Davis was told of his terminal illness, leukemia, during the summer of 1962.  According to a saddened Art Modell, he said "They told him as gently as they could that it was an incurable case of leukemia. It was awful, but the way he took it, it seemed like much more of a blow to me and his teammates than it was to him." 
 John Brown, remembered him as a "genuine gentle man as well as a gentleman."  President John F. Kennedy called Davis "an outstanding young man of great character" and "an inspiration to the young people of this country."  The book titled Ernie Davis: The Elmira Express, authored by writer Robert C. Gallagher, became the basis for the film.

===Set design and filming=== Evanston (at Ryan Field, Northwestern Football Hyde Park Blue Island.  It concluded its fifty-three day shoot at Syracuse University.  Meticulous research was undertaken over several months to recreate the period uniforms and locations depicted, including the creation on film of several stadiums such as Archbold Stadium, that no longer exist. Existing buildings that were not on the Syracuse University campus had to be digitally removed from shots, such as the Carrier Dome.

===Soundtrack=== Frankie Miller, Ralph Bass, Ray Charles, and Lonnie Brooks among others, were used in-between dialogue shots throughout the film. 

{{Infobox album
| Name = The Express: Original Motion Picture Soundtrack
| Type = Film score
| Artist = Mark Isham
| Cover =
| Released = October 28, 2008
| Length = 49:28
| Label = Lakeshore Records
}}

{{Track listing
| collapsed = no
| headline = The Express: Original Motion Picture Soundtrack
| total_length = 49:28
| title1 = Prologue
| length1 = 1:31
| title2 = Jackie Robinson
| length2 = 2:06
| title3 = Elmira
| length3 = 1:57
| title4 = Lacrosse
| length4 = 2:07
| title5 = Training
| length5 = 4:17
| title6 = A Meeting
| length6 = 1:17
| title7 = A Good Man
| length7 = 5:45
| title8 = Im Staying In
| length8 = 1:18
| title9 = Cotton Bowl
| length9 = 7:36
| title10 = Dont Lose Yourselves
| length10 = 4:43
| title11 = Ernie Davis
| length11 = 1:37
| title12 = Heisman
| length12 = 1:12
| title13 = Draft
| length13 = 2:35
| title14 = Rain
| length14 = 1:51
| title15 = Im An Optimist
| length15 = 2:46
| title16 = What Kind of Bottle
| length16 = 1:49
| title17 = The Express
| length17 = 5:02
}}

==Historical inaccuracies== Mountaineer Field" in Morgantown, West Virginia, "rather than at Syracuses own Archbold Stadium," the Orangemens home field at that time in New York state.

Additionally, Lovece remarked that "Aside from the fact that the game didnt even take place there, Schwartzwalder had earlier led West Virginia high-school teams to state championships, and was a beloved and respected figure with devoted fans there who wouldnt have given his teams any lip — so much so that on his death in 1993, WVU even instituted the  . Retrieved 2010-08-08. 
 North Carolina" is inaccurate on all counts; Davis was a freshman in the 1958 season and therefore did not play on the Orangemens varsity team; Syracuse did not play North Carolina in football until 1995; and the name of UNCs home field has been Kenan Stadium since its construction in 1927. In addition the story of the game, as far as sequence of plays and scores go, is considerably out of order. 

Moreover, some claim that the racial tension depicted in the 1960 Cotton Bowl Classic versus the Texas Longhorns is inaccurate, though this is highly disputed. Bobby Lackey, quarterback for the University of Texas states, "I told the Cotton Bowl people that those things didnt happen, and they were making up stories to try and sell more movie tickets, I wasnt going to watch any of that." Lackey continued, "Larry Stephens was my roommate, if anything, he was trying to get the guy into a fight so he could get him thrown out of the game because their athletes were so much better than ours. But I dont know a one of my teammates that said anything derogatory. How are you going to say the N-word in a football game and spit on somebody? Coach Royal would not have put up with that kind of behavior. It was a long time ago, but I know we shook hands and told him nice game and that his team deserved to win." Lackey said, "Then we all walked off the field." 

However, Lou Maysel, in his University of Texas football history bio Here Come the Texas Longhorns, wrote that Stephens, "possibly the most even-tempered player on the Texas team," told John Brown, a black offensive tackle for Syracuse, "Keep your black ass out of it," when Brown protested a penalty to an official. Krizak, Gaylon (17 September 2008).  . San Antonio Express-News. Retrieved 2010-08-08.  Brown stated that there were "guys who called us racist names on the field," including a Texas lineman who kept calling him "a big black dirty  ."  Brown says that the player has since apologized and that he has forgiven the player. Additionally, Al Baker, Syracuses black fullback, said after the game, "Oh, they were bad. One of them spit in my face as I carried the ball through the line."   Patrick Whelan and Dick Easterly, both white players for Syracuse, said that although the film may have fictionalized parts of the story, the 1960 Cotton Bowl Classic was the teams worst confrontation with racism. 

The order of games played and the score of at least one game was fictionalized for the movie. Penn State, a longtime rival of Syracuse, provided Syracuse with their toughest test of the season, in which the Orangemen improved to a 7-0 record, defeating Penn State 20-18.  The film places this game among the first three games of Syracuses season, and cites the score of the game as 32-6. The actual correct score of 20-18 is shown in newspaper clippings later in the movie, when Ernie heads home after his fathers death. Ironically, the film premiered on October 3, 2008, in Syracuses Landmark Theatre; the day before Penn State defeated Syracuse 55-13 in a game during which Davis was honored at halftime. 
 jersey in a photo op. This incident could not have happened in real life—Browns rookie season was 1957 Cleveland Browns season|1957, and Modell did not purchase the Browns until  . Had there been any such presentation by the Browns at this time, it would have very likely been done by Browns head coach and general manager Paul Brown.
 Kennedy administration, which threatened legal action over the teams segregation. The United States Department of the Interior made it known that it would not allow a segregated Redskins team to play in the newly built Robert F. Kennedy Memorial Stadium|D.C. Stadium, which was federally owned.

==Reception==

===Critical response=== weighted average out of 100 to critics reviews, the film received a score of 58 based on 27 reviews. 

{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#FFFFE0; color:black; width:40em; max-width: 35%;" cellspacing="5"
|style="text-align: left;"|" The Express  is involving and inspiring in the way a good movie about sports almost always is. The formula is basic and durable, and when you hitch it to a good story, you can hardly fail. Gary Fleder does more than that in telling the story of Ernie Davis ("The Elmira Express"), the running back for Syracuse who became the first African-American to win the Heisman Trophy, in 1961."
|-
|style="text-align: left;"|—Roger Ebert, writing in the Chicago Sun-Times Ebert, Roger (8 October 2008).  . Chicago Sun-Times. Retrieved 2010-08-10. 
|}
Jim Lane, writing in the  . Retrieved 2010-08-10.  Impressed, he exlaimed, "The film is predictable but inspiring, without going overboard into Brian’s Song tear-jerking. Fleder (expertly assisted by cinematographer Kramer Morgenthau and editors Padraic McKinley and William Steinkamp) ices the cake with some first-rate game footage."  Roger Ebert in the Chicago Sun-Times called it "special" while remarking, "There is a lot of football in the movie. Its well presented, but there is the usual oddity that it almost entirely shows mostly success."  In the San Francisco Chronicle, Peter Hartlaub wrote that the film "deserves plenty of credit for abandoning the "Remember the Titans"/"Glory Road" school of screenwriting as laid out above and exploring the racial issues in Davis story in more realistic terms." He thought Quaid gave a "memorable performance" by portraying Schwartzwalder as "sort of an accidental civil rights hero."  Mike Clark of USA Today, said the film was "an entertaining race-laced contest of wills". He found the football scenes filled with "kinetic" energy, and the lead performances to be "appealing".  The film however, was not without its detractors. Peter Rainer of The Christian Science Monitor, believed the film was a "compendium of virtually every sports movie cliché ever contrived" and that the storyline was "milked for every drop of inspirational uplift."  Left equally unimpressed was Anthony Quinn of The Independent. Commenting on the segregational history, he said "we have to suffer apologetic non-dramas like this, the story of a fleet-footed black footballer (Rob Brown) who hits the big time just as his racial conscience starts to bother him". He thought the screenplay was "stewed in such pieties, served up as warm and homely as apple pie – only theres no taste to it."  Graham Killeen of the Milwaukee Journal Sentinel, added to the negativity by saying, "Producer John Davis (The Firm, Behind Enemy Lines), no relation to Ernie, and director Gary Fleder (Kiss the Girls, Dont Say a Word) are masters of the predictable, the safe and the bland. And Davis story doesnt play to their strengths." He ultimately called the film "an all-brawn, no-brain pigskin potboiler". 

Writing for the  . Retrieved 2010-08-10.  Describing some pitfalls, Wesley Morris of  . Retrieved 2010-08-10. 

{| class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#FFFFE0; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"The movie hints at the complexity of Browns natural truculence and the chip racism left on his shoulder. But  The Express  ultimately settles for making him a big glass of tall, dark, and handsome, a neutered personality deployed to spout platitudes."
|-
| style="text-align: left;" |—Wesley Morris, writing for The Boston Globe 
|}
Ann Hornaday of  . Retrieved 2010-08-10.  Similarly, John Anderson wrote in Variety (magazine)|Variety that the film was "a muscular movie with social conscience that portrays Ernie Davis – the first African-American collegian to win college footballs coveted Heisman Trophy – as the heir to Martin Luther King and Jackie Robinson." On its production merits, he commented how the film displayed "Terrific editing by William Steinkamp and Padraic McKinley" which "intermarries the onfield action, flashbacks to Davis Southern boyhood, a smattering of period footage and a great deal of stylized visualization to a degree that distracts from the very basic sports-movie arc of the story".  However, on a negative front in The Village Voice, Robert Wilonsky was not moved by the lead acting of Quaid or Brown. He thought Brown portrayed Davis with "quiet subtlety (to the point where he almost disappears in some scenes)" and felt Quaid was "stuck with the thankless role of accidental civil-rights pioneer". He summed up his disappointment stating, "like all formulaic biopics, The Express sacrifices the details for the Big Picture—hagiography without the humanity (wait, is that his girlfriend? Wife? What?), populated by sorta-enlightened Yankees, rabidly racist Southerners, and a ghost who remains as elusive as the running back no defender could ever catch." 

===Box office=== Max Payne, unseated Beverly Hills Chihuahua to open in first place grossing $17,639,849 in box office revenue.    During its final week in release, The Express opened in 31st place grossing $151,225 in business.  The film went on to top out domestically at $9,793,406 in total ticket sales through a 4-week theatrical run. Internationally, the film took in an additional $14,718 in box office business for a combined worldwide total of $9,808,124.  For 2008 as a whole, the film would cumulatively rank at a box office performance position of 146. 

===Home media=== Region 1 DVD in the United States on January 20, 2009. Special features for the DVD include; deleted scenes with optional commentary by director Gary Fleder; "Making of The Express"; "Making History: The Story of Ernie Davis"; "Inside the Playbook: Shooting the Football Games"; "From Hollywood to Syracuse: The Legacy of Ernie Davis"; and feature commentary with director Gary Fleder.    During its release in the home media market, The Express ranked number eleven in its first week on the DVD charts, selling 97,511 units totalling $1,949,245 in business.  Overall, The Express sold 370,534 units yielding $6,566,801 in revenue.   

The widescreen high-definition Blu-ray Disc version of the film was also released on January 20, 2009. Special features include "Making of The Express"; "Making History: The Story of Ernie Davis"; "Inside the Playbook: Shooting the Football Games"; "From Hollywood to Syracuse: The Legacy of Ernie Davis"; "50th Anniversary of the 1959 Syracuse National Championship"; and deleted scenes with optional commentary by director Gary Fleder.   

==See also==
 

==References==
;Footnotes
 

;Further reading
 
*  
*  
*  
*  
*  
*  
*  
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 