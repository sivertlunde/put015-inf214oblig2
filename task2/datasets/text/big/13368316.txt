Unfolding Florence: The Many Lives of Florence Broadhurst
{{Infobox film
| name           = Unfolding Florence: The Many Lives of Florence Broadhurst
| image          = Unfoldingflorence-poster.jpg
| caption        = Unfolding Florence: The Many Lives of Florence Broadhurst promotional poster
| director       = Gillian Armstrong
| producer       = Sue Clothier Charles Hannah
| writer         = Katherine Thomson
| starring       = Judi Farr Felicity Price
| music          = Paul Grabowsky
| cinematography = John Radel
| editing        = Nicholas Beauman
| distributor    = Becker Entertainment
| released       =  
| runtime        = 82 minutes
| country        = Australia
| language       = English
}} 

Unfolding Florence: The Many Lives of Florence Broadhurst, also known as A Colourful Life, is a 2006 Documentary film|documentary/drama based on the life of Florence Broadhurst. Starring Felicity Price, the film was written by Katherine Thomson and directed by Gillian Armstrong. The Australian documentary includes interviews with friends, family members and employees who knew Florence Broadhurst before her mysterious death in 1977. The film had a mixed critical response and was nominated for 4 awards despite being relatively obscure.
__TOC__

==Plot==
Unfolding Florence: The Many Lives of Florence Broadhurst chronicles the life of Florence Broadhurst, an Australian designer who owned her own fashion design company in London during the 1930s. Later in life she moved to Sydney, Australia, and became a painter, socialite and charity fund-raiser. At 60 years old she began her most prolific and successful occupation, becoming a wallpaper designer. Florence Broadhurst was murdered in her studio during 1977, at the age of 78. The film includes interviews, dramatizations and animations which are used to illustrate Broadhursts unusual lifestyle.   

==Reception==
The film received mixed reviews from critics, some calling it cluttered and difficult to follow,  while others applauded the director on her achievement. 

==Box office==
Unfolding Florence: The Many Lives of Florence Broadhurst grossed $429,248 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
*  
*  
*   Produced by  
 

 
 
 
 
 
 