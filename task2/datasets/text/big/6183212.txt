Towed in a Hole
 
{{Infobox film
| name = Towed in a Hole
| image = Towedinaholetitlecard.jpg George Marshall
| producer = Hal Roach
| starring = Stan Laurel Oliver Hardy Billy Gilbert
| music = Leroy Shield
| cinematography = Art Lloyd
| editing = Richard C. Currier
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 21 02"
| language = English
| country = United States
}}
Towed in a Hole is a 1932 Laurel and Hardy film.

==Plot==
Laurel and Hardy are in the fish business. They drive around town seeing if they can sell any. Stan suggests they catch their own fish and keep all the profits. Ollie likes the idea of cutting out the "Reseller|middleman" so they buy a boat at a junk yard. After testing it for leaks by filling it with water and some setbacks such as dropping an anchor through the hull and sawing through the mast, they succeed in fixing it up. When the boat is finally ready, the whole operation goes south when they decide to hoist the sail.

==Cast==
* Stan Laurel as Stanley
* Oliver Hardy as Ollie
* Billy Gilbert as Joe the junkyard owner

==Notes==
Towed in a Hole was remade by The Three Stooges in 1945 as Booby Dupes.   

==References==
 

== External links ==
*  
* 

 
 
 

 
 
 
 
 
 
 
 


 
 