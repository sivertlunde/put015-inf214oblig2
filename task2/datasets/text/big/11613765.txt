Here on Earth (film)
 
{{Infobox film
| name           = Here on Earth 
| image          = Hereonearth.jpg
| caption        = Theatrical release poster
| director       = Mark Piznarski
| producer       = David T. Friendly
| writer         = Michael Seitzman Chris Klein Leelee Sobieski Josh Hartnett
| music          = Kelly Jones Andrea Morricone
| cinematography = Michael D. OShea
| editing        = Robert Frazen
| distributor    = 20th Century Fox
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $10,873,148
}} Chris Klein, Leelee Sobieski, and Josh Hartnett. The original music score was composed by Kelly Jones and Andrea Morricone.

==Plot summary==
Kelvin "Kelley" Morse and Jasper Arnold become involved in a car race and accidentally damage a restaurant owned by Samantha Cavanaughs parents. Both are sentenced to perform community service by repairing the damage. Although Kelley comes from a wealthy family and Jaspers parents are working-class, they soon find themselves fighting over the same girl, Samantha. While Jasper and Samantha have been courting publicly for years, in secret, Kelley and Samantha begin to spend time together. They soon find that they have more in common than they imagined, and they fall in love. Eventually, Jasper learns of their interlude and doesnt like it.  During a trip to Kelleys home in Boston he reveals to Samantha that his mother killed herself. Samantha brings Kelley into the house and they sleep together. In the morning, after Sam makes Kelley breakfast, Kelleys father arrives and informs him he must attend college early and give up his fling with Samantha. Upon returning to the small town, Samanthas parents soon learn that their daughter has osteosarcoma, stemming from a track/field injury, and only a few months to live. Samantha tells Kelley that she thinks everyone has their own heaven and it is made of a combination of all the things we loved in life. She says that his mother has Kelley with her in her heaven. When Kelley learns the awful truth, he must decide if he should obey his fathers wishes and go to college or stay by the side of the first girl hes ever loved. In the end he returns to be with Samantha during her final months of life. At her funeral, Kelley recites a passage from a poem he and Sam loved. The film closes with a shot of Samantha running through a field in her version of heaven.

==Cast== Chris Klein as Kelvin Kelley Morse
* Leelee Sobieski as Samantha Sam Cavanaugh
* Josh Hartnett as Jasper Arnold
* Michael Rooker as Malcolm Arnold
* Annie Corley as Betsy Arnold
* Bruce Greenwood as Sheriff Earl Cavanaugh
* Annette OToole as Jo Cavanaugh
* Elaine Hendrix as Jennifer Cavanaugh

==Reception==
Here on Earth received negative reviews from critics, as it currently holds a 17% rating on Rotten Tomatoes based on 69 reviews.

==Box office==
The film opened at #5 at the North American box office, making $4.5 million USD in its opening weekend.

==Soundtrack== Where You Are
#Sixpence None the Richer - I Need Love
#Devin - Whatever Turns You On
#Tal Bachman - If You Sleep Tim James - Tic Tocs
#Beth Orton - Dont Need a Reason
#Stereophonics - Pick a Part Thats New
#Sixpence None the Richer - We Have Forgotten Neve - Skyfall
#Tori Amos - 1000 Oceans
#Andrea Morricone - Birches
#Andrea Morricone - Here on Earth Score Suite
 Black Balloon" by Goo Goo Dolls and "Breakout (Foo Fighters song)|Breakout" by Foo Fighters are played during the movie, but do not appear on the soundtrack album.

==References==
 
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 