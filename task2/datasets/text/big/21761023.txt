The Truth About Women
The British comedy film directed by Muriel Box and starring Laurence Harvey, Julie Harris, Mai Zetterling and Diane Cilento.  

==Plot==
When his son-in-law comes to him with a woeful tale of an unhappy relationship and a belief that all women are impossible to love, elderly Sir Humphrey Tavistock calmly puts him straight.

Tavistock regales him with decades-old anecdotes of found lovers and lost love. We meet in flashback the free-thinking Ambrosine Viney, an independent woman ahead of her time, and the sophisticated Louise Tiere, a diplomats wife. There are others as well, including one whom Tavistock adores and marries, only to lose her forever during childbirth.

==Cast==
* Laurence Harvey as Sir Humphrey Tavistock
* Julie Harris as Helen Cooper
* Diane Cilento as Ambrosine Viney
* Mai Zetterling as Julie
* Eva Gabor as Louise
* Michael Denison as Rollo
* Derek Farr as Anthony
* Elina Labourdette as Comtesse
* Roland Culver as Charles Tavistock
* Wilfrid Hyde-White as Sir George Tavistock
* Robert Rietti as the Sultan Catherine Boyle as Diana
* Ambrosine Phillpotts as Lady Tavistock
* Jocelyn Lane as Saida
* Lisa Gastoni as Mary Maguire

==References==
 

 

 
 
 
 
 
 
 
 
 
 


 
 