Patlabor: The Movie
{{Infobox film
| name           = Patlabor: The Movie
| image          = Patlabor The Movie poster.png
| caption        = Original theatrical poster
| director       = Mamoru Oshii
| writer         = Kazunori Itō
| based on       =  
| starring       = Miina Tominaga Toshio Furukawa Ryunosuke Ohbayashi Shigeru Chiba
| producer       = Makoto Kubo Shin Unozawa Taro Maki
| music          = Kenji Kawai IG Tatsunoko (production cooperation)
| distributor    = Shochiku
| released       =  
| runtime        = 98 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
  is a 1989 anime film directed by Mamoru Oshii, with an original story by Headgear (artist group)|Headgear. It was produced by Studio Deen with the help of IG Tatsunoko (later Production I.G), Bandai Visual and Tohokushinsha.  It is part of the Patlabor anime and manga series.

==Plot==
Set in 1999,   under the Babylon Project. Dominating the scene is the "Ark," a huge man-made island that serves at the Projects nerve center and chief Labor manufacturing facility.
 GSDF is also preoccupied as they send their own forces to stop an HOS-equipped HAL-X-10 Labor tank prototype.

As Division II goes out on the field, team commander Captain Gotoh, Sgt Asuma Shinohara, and mechanic Shige Shiba work with police Detective Matsui to find further leads on the case. They discover that all the errant Labors, plus other Labors in the Greater Tokyo Area, were installed with the companys new Hyper Operating System (HOS) software and can be triggered by high-frequency resonance emanating from wind-struck high-rise buildings. To SV2 pilot Noa Izumis relief, no copies of the software were installed in Division IIs AV98 Ingram police Labors. They also learn that HOS programmer Eiichi Hoba - who committed suicide days before - was obsessed with the Babylon Projects Biblical references (the Ark being alluded to Noahs Ark, for example) and planted a bug in the code that would cause the Labor to malfunction. A computer simulation predicts that gale-force winds acting on the Ark could send all the Labors in Tokyo into a massive rampage, especially since the Arks size and steel framework helps the resonance reach farther. Worse, the weather bureau announces that a typhoon is expected to hit Tokyo within two days. 
 MPD leadership to destroy the Ark as Shige tries to dig up more evidence of Hobas guilt to justify the operation. Kanuka Clancy returns from the US to help in the raid. Division II attaches flotation bags to their vehicles and head out to the Ark. Malfunctioning HOS-equipped Labors engage the team as soon as they land on the Ark. Ingram pilots Noa and Ohta, plus Kanuka in a hijacked AV-X0 Type Zero police Labor prototype, buys time for Hiromi, Asuma and Shinshi to break into the control room and activate the Arks self-destruct sequence. However, Kanuka loses control over the Type Zero in the chaos because it runs on HOS as well. Trapped by the Type Zero in one of the last remaining ledges, Noa climbs out of her damaged Ingram and fires her shotgun into the Labor’s Phase-change memory|P-RAM system to finally shut it down. With the successful destruction of the Ark, SV2 sends choppers to rescue the team.

==Voice actors==
{| class="wikitable"
|-
!  style="width:25%; text-align:center;"| Character
!  style="width:25%; text-align:center;"| Original Japanese
!  style="width:25%; text-align:center;"| English (Manga UK)
!  style="width:25%; text-align:center;"| English (Bandai Visual)
|-
!Noa Izumi
| Miina Tominaga
| Briony Glassico
| Julie Ann Taylor
|-
!Asuma Shinohara 
| Toshio Furukawa David Jarvis
| Doug Erholtz
|-
!Kiichi Goto
| Ryunosuke Ohbayashi
| Peter Marinker
| Roger Craig Smith
|-
!Shinobu Nagumo
| Yoshiko Sakakibara
| Sharon Holm
| Megan Hollingshead
|-
!Kanuka Clancy 
| Yō Inoue
| Tamsin Hollo
| Lisa Enochs
|-
!Isao Ohta 
| Michihiro Ikemizu
| Martin McDougall
| Sam Riegel
|-
!Mikiyasu Shinshi 
| Issei Futamata
| Ron Lepaz
| Joe Ochman
|-
!Hiromi Yamazaki 
| Daisuke Gouri
| Michael Fitzpatrick
| Jason C. Miller
|-
!Shigeo Shiba 
| Shigeru Chiba
| Edward Glen Peter Doyle
|-
!Seitaro Sakaki
| Osamu Saka
| Blair Fairman
| Jamieson Price
|-
!Jitsuyama
| Mahito Tsujimura
| Don Fellows
| Milton Lawrence
|-
!Matsui 
| Tomomichi Nishimura
| Mac McDougall
| Paul St. Peter
|-
!Kaihou
| Toshihiko Kojima
| Julian B. Wilson
| Paul St. Peter
|-
!Fukushima
| Shinji Ogawa
| Bill Roberts
| Bob Papenbrook
|-
!Kataoka
| Kouji Tsujitani
| William Dufries
| Liam OBrien
|}

==Home release== Manga UK R4 release uses the Australian Manga VHS master, and includes remixed 5.1 Manga UK dub and the original 2.0 Japanese dub.  . In 2008, both movies were released in Japan on Blu-ray with English audio and English subtitles. It was licensed in Europe by Beez Entertainment. Maiden Japan has licensed all three Patlabor films and will release the first film on Blu-ray and DVD on May 5, 2015. 

==References==
 

==External links==
*    
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 