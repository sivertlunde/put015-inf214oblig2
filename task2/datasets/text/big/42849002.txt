Black Ransom
 
 
{{Infobox film
| name           = Black Ransom
| image          = BlackRansom.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 撕票風雲
| simplified     = 撕票风云
| pinyin         = Sī Piào Fēng Yún
| jyutping       = Si1 Piu3 Fung1 Wan4 }}
| director       = Wong Jing Venus Keung
| producer       = Wong Jing
| writer         = 
| screenplay     = Wong Jing
| story          = 
| based on       = 
| narrator       = 
| starring       = Simon Yam Michael Miu Fala Chen
| music          = Henry Lai
| cinematography = Venus Keung Ng King Man
| editing        = Lee Ka Wing
| studio         = Mega-Vision Pictures
| distributor    = Mega-Vision Pictures
| released       =  
| runtime        = 93 minutes Hong Kong
| language       = Cantonese
| budget         = 
| gross          = US$339,557 (HK$1.89&nbsp;million)   
}}
 2010 Cinema Hong Kong action thriller film directed by Wong Jing and Venus Keung and starring Simon Yam, Michael Miu and Fala Chen.

==Plot== triad leaders were kidnapped, held at ransom and was murdered. The modus operandi was violent, brutal, professional and efficient. Mann soon discovers that the gang of kidnappers were formers members of the Special Duties Unit, led by Samo Ho (Michael Miu) and his girlfriend Can (Qu Ying), a former member of the VIP Protection Unit, who were familiar with the police modus operandi and were able to escape from apprehension. Sam also discovers that Mann is after him. A duel to the death between the two elites of the police force is ensured.

==Cast==
*Simon Yam as Inspector Mann Cheung
*Michael Miu as Sam Ho
*Fala Chen as Superintendent Koo Kwok Keung
*Liu Yang as Eva
*Qu Ying as Can
*Kenny Wong as Ice King
*Wada Hiromi as Yan
*Vincent Wong as Spring
*Xing Yu as Rocky
*Andy On as Gundam Ko
*Jiang Yang as Eagle
*Samuel Pang as Bill
*Adam Chan as Hui
*Ricky Chan as Inspector Tiger Chan
*Parkman Wong as Tang Qing
*Zuki Lee as Tang Qings wife
*Richard Cheung as Uncle Dragon
*Mark Cheung as Bull
*Wong Ching as Wide Mouth
*Winnie Leung as Mady
*Ben Cheung as David Ho
*Ada Wong as Ada
*Gloria Wong as Bobo
*Chan Pak Lei as Fifi
*Frankie Ng as Triad boss
*Lee Kim Wing as Triad boss
*Chan Kam Pui as Triad boss
*Siu Hung as Triad boss
*Luk Man Wai as SDU member
*Law Wai Kai as Thug
*Tam Kon Chung as Tang Qings thug
*Lai Chi Wai as Ices thug
*Benny Lai as Ices thug
*Johnny Cheung as Thug

==Box office==
The film grossed US$339,557 (HK$1.89&nbsp;million)  at the Hong Kong box office.

==See also==
*Wong Jing filmography

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 