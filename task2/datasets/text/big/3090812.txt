Bringing Down the House (film)
{{Infobox film
| name           = Bringing Down the House
| image          = Bringing down the house poster.jpg
| caption        = Theatrical release poster
| director       = Adam Shankman
| producer       = David Hoberman Ashok Amritraj
| writer         = Jason Filardi
| starring       = Steve Martin Queen Latifah Eugene Levy Joan Plowright Jean Smart Missi Pyle Betty White
| music          = Lalo Schifrin
| cinematography = Julio Macat
| editing        = Gerald B. Greenberg
| studio         = Touchstone Pictures Hyde Park Entertainment Buena Vista Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $33 million
| gross          = $164.7 million
}}
Bringing Down the House is a 2003 American comedy film written by Jason Filardi and directed by Adam Shankman. The film stars Steve Martin and Queen Latifah.

==Plot==
 
Peter Sanderson (Steve Martin) is a workaholic tax attorney working for a set of competitive colleagues and bosses names of Tobias, Kline, and Barnes while he tries to turn his life around and having just separated from his wife Kate Sanderson (Jean Smart) and lost privileges to his children, 15-year-old Sarah Sanderson (Kimberly J. Brown) and 8-year-old Georgie Sanderson (Angus T. Jones). One day he tells his wife that the previously planned Hawaii trip will not happen, but he lets them be in their house for some allotted time. Then he shows up late to a restaurant where Peter must deal with Virginia Arness (Joan Plowright), an eccentric English billionaire, to bring her business to his firm. But while tending to his career, he found some time corresponding with an online friend known only as "lawyer-girl". On their first blind date, Peter learns that "lawyer-girl" is a black woman named Charlene Morton (Queen Latifah), a wrongfully convicted felon accused of bank robbery claiming her innocence who wants Peters help in getting the charges dropped. He kicks her out but then decides let her stay due to the way, Mrs. Kline (Betty White) could feel if he was associated with her. Mrs. Kline, who is Peters neighbor, is also his boss sister and a racist bigot who sometimes plays poker with Georgie.

The next day as Peter picks up the kids, he tricks Charlene to make her leave, but she breaks into the house and get her friends to the house. As they drive home, he realizes that Charlene didnt give up on leaving his house and now forces her to leave the house. While Howie Rottman (Eugene Levy) and Peter are waiting to have Peter make a deal promised for Mrs. Virginia Arness, she appears to finally convince him to help her out. Ashley (Missi Pyle) who is a wanna-be bad girl thinks she is here to serve drinks and food. However, Peter makes it seem that she is the nanny of his house since his ex-wife is not with him. Much later, Charlene appears in Peters office informing him of new evidence. When his associate Sofia (Tracey Jones) told Peter that one of his bosses Mr. Edward Tobias (Jim Haynie) wants to see him, he & Howie try to get her out of there but to no avail as Tobias shows up before them. But the two gentlemen manage to get off the hook when Charlene tells Ed about Peter helping with the financial situation at a church and then leaves. At the golf resort, as Peter plays a round with Mrs. Arness and Howie and Charlene are having a talk, Ashley talks trash and nasty enough for Charlene to get in a fight with her.
 Steve Harris) about another piece of new evidence that could help or hurt Charlene. Then Charlene and Widow reconcile after which Peter confronts her, thinking they were planning another crime. Then Peter convinces Charlene not to have any further visitors. Next, Peter receives a call from Sarah who asks her father if she wants to speak to Charlene. So she does and she asks Charlene to take her home. When Charlene arrives at the house Sarah was in, Sarah informs Charlene that she lied to Peter and actually had a different boyfriend who tried to have sex with her to which Sarah refused. So to teach the boy a lesson, Charlene hangs him by his ankles at the balcony and forces him to tell Sarah that hes sorry and will never try to do it again. As Charlene takes Sarah home, Peter realizes that she was with bad friends but doesnt get angry after Charlene tells him, "Sarah didnt need a warden, she needed her father."

The next night Mrs. Arness comes for dinner but on that same night shes there she realizes that Peter has for a nanny, an escaped convict (Charlene), wanted for a murder she did not commit. So she walks out and at the same time, Peter kicks Charlene out. The next day at his firm, Peter fearing that his boss is going to fire him, realizes that Arness didnt call the cops but the FBI. Peter runs out of the firm in terror, but not before Howie asks him to tell Charlene if & when he sees her, this term: "The cool points are out the window and shes got him all twisted up in the game". When Peter gets into his car and after adjusting his rear view mirror, he sees Widow in the back. Widow threatens Peter not to reopen Charlenes case nor go anywhere near her. With that threat in his mind, Peter starts his car and drives off, causing Widow to roll out. This makes Peter realize that Charlene really "is" innocent and was framed by Widow as he made the case affect him all along as he was in hiding. So he decides to forgive Charlene and tries to call her, but he doesnt know where Charlene is until Sarah tells Peter that Charlene has his cell phone, so he calls her by his house phone. Charlene now knows who set her up and Peter is determined to get a confession. Howies positive attention begins to get Charlene to be with him. They reach the Arness house but trouble doesnt come due to William Shakespeare (her dog) being tied up and Julia, Arness assistant out of reach. Todd Gendler (Michael Rosenbaum), Peters rival & his apparent replacement gets a punch from Charlene for making a negative remark. Back at the house, the kids told their mom about Charlene and try to persuade her to believing that he moved on from the old ways and he doesnt think about himself anymore.

Later, we see Peter drive up to "The Down Low", the gangster club of Widows and makes a final confrontation, disguising himself to get in undetected. Widow arrives later and when he sees Peter in disguise, he gets his bodyguards to take Peter in the backroom. The other three (Charlene, Howie and Arness) arrive shortly after. Howie & Charlene figure out a way to save Peter while Arness is having drinks and getting stoned next to two customers. Meanwhile in the room, Peter tells Widow he & Charlene now knows who Widows accomplice was; but Widow tells Peter that there was no accomplice at all, it was just him in disguise. After Charlene calls the FBI, she goes go into the backroom as Peter & Widow came out. Widow throws Peter onto the floor, & then points the gun at both Charlene and Howie. Peter elbows Widow in the groin, causing him to drop the gun. Howie grabs the gun and points it at Widow and his bodyguards, but Peter accidentally nudges Howie and he fires the gun towards the bar, causing everyone to panic and run out the club. Charlene and Widow ensue in a fistfight, but Widow quickly gets his gun off the floor and points it at Charlene. Widow shoots Charlene and she falls on the couch, presumably dead. This angers Howie and he jumps on Widow trying to fight him. Peter goes over to Charlene to grief but Charlene wakes up and pulls Peters cellphone out of her shirt and reveals the bullet went through the phone, saving her life. The FBI agents come in the club to arrest them. However, Peter has a lot of proof to not only make Charlenes record expunged, but to put Widow away for a long time. Mrs. Arness almost wants no more from Peter, but Peter escorts her to an all-night diner, convincing her to make him her lawyer again.

The next day, Gendler tells Ed that he called Arness four times with no success since she said shell only talk to Peter. The bosses decide to keep Peter around when they see him move out. When Peter, Howie and Sofia move out, Tobias, and his partners are sorry for understanding the new dynamic in Peter but in self-retiring from the firm, he moves out of the office saying, "Ed, you can kiss my natural black ass!" At the new office, Charlene thanks him for everything he did with one final appreciated remark and she now leaves Peter to have a good conversation with his wife after they see her walk in. Peter gave the same line Howie gave to Charlene to Kate as a way of asking her to give him a second chance, she does and reconciles once and for all. Their reconciliation was briefly interrupted by Peters cell phone, but rather than answering, Peter just threw it away and they continued to hug. The end of the film shows Charlene putting braids on Howies head and she brings down the shade.

==Cast==
* Steve Martin as Peter Sanderson. An uptight lawyer who reluctantly helps Charlene with her case. They eventually bond, becoming close friends.
* Queen Latifah as Charlene Morton. An escaped convict who was framed seeks Peters help in proving her innocence. Over the course of the film, she bonds with Peter and the family as she poses as their nanny. She is also Howies love interest.
* Eugene Levy as Howie Rottman, Peters over-sexed best friend and colleague. He falls madly in love with Charlene upon meeting her.
* Jean Smart as Kate Sanderson, Peters ex-wife. Its apparent that they still harbor feelings for one another, as she is jealous of his new friendship with Charlene, believing them to be in a relationship.
* Missi Pyle as Ashley, Kates promiscuous, and alcoholic sister.
* Joan Plowright as Virginia Arness, An arrogant client of Peters.
* Betty White as Mrs. Kline, Peters racist neighbor.
* Kimberly J. Brown as Sarah Sanderson, Peters daughter and older child.
* Angus T. Jones as George "Georgie" Sanderson, Peters son and younger child.
* Michael Rosenbaum as Todd Gendler, Peters arrogant colleague & apparent replacement. Steve Harris as Widow, Charlenes shady ex-boyfriend.
* Matt Lutz as Aaron
* Victor Webster as Glen
* Kelly Price as herself

==Soundtrack==
 
A soundtrack containing hip hop and R&B music was released on March 4, 2003 by Hollywood Records. It peaked at 111 on the Billboard 200|Billboard 200 and 23 on the Top R&B/Hip-Hop Albums.

==Reception==

===Critical===
Since the films release in 2003, Bringing Down the House has received mixed to negative reviews from critics. Review aggregator Rotten Tomatoes currently gives the film a "rotten" score of 34% based on 148 reviews, the general consensus being, "Though the cast shines, they cant save this comedy, which is overly contrived and filled with outdated and offensive racial jokes."

===Box office===
On a budget of $35 million, the film became a surprise hit. It earned $132.6 million in the United States and an international gross of $32 million, bringing its worldwide gross to $164.6 million.  As of March 2009, it is ranked #231 of the All Time Top Grossing USA Motion Pictures. 

===Awards and nominations===
Queen Latifah
*Won &ndash; Teen Choice Awards 2003 for Choice Movie Actress Comedy
*Won &ndash; NAACP Image Award for Outstanding Actress in a Motion Picture
*Nominated &ndash; MTV Movie Award for Best Performance
*Nominated &ndash; MTV Movie Award for Best Fight (shared with Missi Pyle)

Steve Martin
*Nominated &ndash; MTV Movie Award for Best Dance Sequence

== References ==
 

== External links ==
 
*  
*  

 

 
 
 
 
 
 
 