Your Neighbor's Son
{{Infobox Film name = Your Neighbors Son image = YOUR NEIGHBOURS SON.JPG director = Jørgen Flindt Pedersen Erik Stephensen producer = Ebbe Preisler cinematography = Alexander Gruszynski editing = Peter Engleson released = 1976/1981 runtime = 55/65 minutes, NR language = Greek, English subtitles country = Denmark, Greece
}}
 documentary or moral human 1967 to 1974.

==Synopsis== Tuol Sleng, many of whom were under 19 years old. 

The film also interviews Michalis Petrou, a conscript who served in the Military Police and was trained to become one of the most notorious torturers of the EAT-ESA. Petrous testimony reveals that the training methods themselves were brutal and often torturous and was viewed as a necessity to ensure the robotic and brutal obedience of the trainees.

During the dramatic recreation of the training camps, the directors draw parallels to modern military recruit trainings and the methods used in the training of soldiers.

==Response==
The New York Times columnist Lawrence Van Gelder noted that while the film was technically flawed it was at the same time "memorable and unnerving" in its depiction of both the victims of the torture as well as the ordeals of the torturers themselves, and claimed "it is impossible to regard its torturers or its victims without realizing that the view may be a view into a mirror."   

==References==
 

==External links==
* 
* 
*  in Greek with Danish subtitles.

 
 
 
 
 
 
 
 
 
 