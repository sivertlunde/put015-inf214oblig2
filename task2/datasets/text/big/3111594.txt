The Marriage of Maria Braun
{{Infobox film
| name           = The Marriage of Maria Braun
| image          = Original-poster-marriage-of-maria-braun.jpg
| image size     = 
| alt            = 
| caption        = Theatrical poster
| director       = Rainer Werner Fassbinder
| producer       = Michael Fengler
| writer         = Peter Märthesheimer Pea Fröhlich
| narrator       = 
| starring       = Hanna Schygulla Klaus Löwitsch Ivan Desny Gisela Uhlen
| music          = Peer Raben
| cinematography = Michael Ballhaus
| editing        = Rainer Werner Fassbinder (as Franz Walsch) Juliane Lorenz
| studio         = Albatros Filmproduktion Westdeutscher Rundfunk Trio Film
| distributor    = 
| released       =    (Berlin International Film Festival|Berlinale)     (West Germany) 
| runtime        = 115 minutes
| country        = West Germany
| language       = German DM   DM  (West Germany)  United States dollar|$1.8 million  (United States|USA, within the first six weeks) 
}}

The Marriage of Maria Braun ( ) is a 1979 West German film directed by Rainer Werner Fassbinder. The film stars Hanna Schygulla as Maria, whose marriage to the soldier Hermann remained unfulfilled due to World War II and his post-war imprisonment. Maria adapts to the realities of post-war Germany and becomes the wealthy mistress of an industrialist, all the while staying true to her love for Hermann. The film was one of the more successful works of Fassbinder and shaped the image of the New German Cinema in foreign countries. The film is the first in Fassbinders BRD Trilogy, followed by Veronika Voss and Lola (1981 film)|Lola.

==Plot==
The film starts in Germany in 1943. During an Allied bombing raid Maria (Hanna Schygulla) marries the soldier Hermann Braun (Klaus Löwitsch). After "half a day and a whole night" together, Hermann returns to the front. Postwar, Maria is told that Hermann has been killed. Maria starts work as a hostess in a bar frequented by American soldiers. She has a relationship with an African-American soldier Bill (George Byrd), who supports her and gives her nylon stockings and cigarettes. She becomes pregnant by Bill.

Hermann, who was not killed, returns home to discover Maria and Bill undressing each other. A fight between Hermann and Bill ensues. When Hermann seems in danger Maria unintentionally kills Bill striking his head with a full bottle. Maria is tried by a military tribunal, and expresses her love for both Bill and Hermann - Hermann is so struck with Marias devotion that he takes the blame for the killing and is imprisoned. Maria likely aborts her pregnancy and asks her doctor promise to maintain the grave. On the train home, Maria catches the eye of a wealthy industrialist, Karl Oswald (Ivan Desny). Oswald, an older man, offers her a position as his assistant, and shortly thereafter Maria becomes his mistress to "maintain the upper hand". Maria visits Hermann again and tells him about the development, promising that their life will start as soon as he is released. Maria becomes wealthy and buys a house.

Oswald visits Hermann and offers to make him and Maria heirs to his wealth if Hermann deserts Maria after his release. Neither man tells Maria of their agreement. On release, Hermann emigrates to Canada and sends Maria a red rose each month to remind her he still loves her. Following Oswalds death Hermann returns to Germany and to Maria. When Oswalds will is read by the executor Senkenberg (Hark Bohm) Maria hears about Oswalds agreement with Hermann. Distressed, Maria lights a cigarette and dies from an explosion of gas after shed extinguished the burner of the stovetop shed used to light a previous cigarette. Whether this is intentional or not is not clearly demonstrated.

==Cast==
*Hanna Schygulla as Maria Braun
*Klaus Löwitsch as Hermann Braun
*Ivan Desny as Karl Oswald
*Gisela Uhlen as Mother
*Elisabeth Trissenaar as Betti Klenze
*Gottfried John as Willi Klenze
*Hark Bohm as Senkenberg
*George Byrd as Bill
*Claus Holm as Doctor
*Günter Lamprecht as Hans Wetzel 
*Anton Schiersner as Grandpa Berger
*Sonja Neudorfer as Red Cross nurse
*Volker Spengler as Train conductor
*Isolde Barth as Vevi
*Lilo Pempeit as Mrs. Ehmke
*Bruce Low as American at the conference
*Günther Kaufmann as American on the train
*  as Prosecuting attorney 
*Hannes Kaetner as Justice of the Peace
*Kristine de Loup as Notary

==Production==

===Writing and pre-production=== omnibus film Germany in Autumn. Fassbinder worked on a draft screenplay together with Klaus-Dieter Lang and Kurt Raab and presented it in the early summer of 1977 to his longtime collaborator Peter Märthesheimer, who at that time was working as a dramaturge at the Bavaria Film Studios. In August 1977 Märthesheimer and his partner Pea Fröhlich, a professor of psychology and pedagogics, were commissioned to write a screenplay based on the draft together.    Although it was Märthesheimers and Fröhlichs first screenplay their knowledge of Fassbinders works allowed them to match the screenplay to the characteristic style and structure of Fassbinders other works.  Fassbinder changed only a few details in the completed screenplay, including some dialogues and the end of the film. Instead of Maria Braun committing suicide in a car accident she dies in a gas explosion, leaving it unclear whether she committed suicide or died accidentally. 
 Berlin Alexanderplatz was scheduled for June 1978.    As Fassbinder was embroiled in a controversy over his stage play Der Müll, die Stadt und der Tod he was not ready for starting to shoot the film and withdrew to Paris, where he worked on the screenplay for Berlin Alexanderplatz.  Fengler was dreaming of an international star cast for the film. On his suggestion Fassbinder and Fengler visited Romy Schneider and asked her to play the role of Maria Braun. Due to Romy Schneiders alcohol problems, fickleness, and demands, the role was then given to Hanna Schygulla, her first collaboration with Fassbinder in four years.    Yves Montand also showed interest in the film, but wanted to play Marias husband Hermann and not - as suggested by Fassbinder and Fengler - the industrialist Oswald. As the role of Hermann was already promised to Klaus Löwitsch Montand was ultimately not offered any role. 

===Production===

 
From its beginnings, the financing of The Marriage of Maria Braun was precarious. Albatros Filmproduktion only contributed 42,500 Deutsche Mark|DM, the public broadcaster Westdeutscher Rundfunk 566,000 DM, the   400,000 DM and the distributor guaranted another 150,000 DM. This forced Fengler to find another financing partner, offering Hanns Eckelkamps Trio-Film a share in the film in December 1977.    Fengler had promised Fassbinders Tango-Film a share of 50 percent of the film profits, but as Fengler - by offering Trio-Film a share in the film - effectively overselled the rights only 15 percent of the film right ultimately remained with Fassbinder. Fassbinder subsequently referred to Fengler as gangster and it led to litigations against Fengler that continued even after Fassbinders death.   

Shooting began in January 1978 in Coburg. Bad-tempered and quarrelsome, Fassbinder shot the film during the day and worked on the script to Berlin Alexanderplatz during the night.  In order to sustain his work schedule he consumed large quantities of cocaine, supplied by the production manager Harry Baer and the actor Peter Berling. According to Berling this was the main reason why the film went over the budget, as the cash for the cocaine was coming from Fengler. 

In February 1978 the budget was reaching 1.7 million DM, and two most expensive scenes - the explosions at the beginning and at the end of the film - had not yet been shot. By this time Fassbinder had learned about Fenglers deal with Eckelkamp and the overselling of the film rights. He felt deceived and broke with his longtime collaborator Fengler. He demanded the status of a co-producer for himself and obtained an injunction against Fengler and Eckelkamp. Fassbinder fired most of the film crew, ended the shooting in Coburg at the end of February and then moved to Berlin, where he finished shooting the last scenes.  Consequently the biographer Thomas Elsaesser called the production of the film "one of Fassbinders least happy experiences" and Berlin "one of the decisive self-destructive episodes in Rainers life". 

==Distribution and reception==

===Release=== Cannes Film Festival in May 1978 spurred Fassbinder to prepare an answer print overnight and to present the film on 22 May 1978 to German film producers in a private screening. Attended by, among others Horst Wendlandt, Sam Waynberg, Karl Spiehs, Günter Rohrbach and the majority shareholder of the Filmverlag der Autoren, Rudolf Augstein the screening was a success.  Eckelkamp invested a further 473,000 DM to pay off the debts of the film production and became the sole owner of the rights to the film.  Owning all film rights, Eckelkamp negotiated a distribution deal with United Artists, thus outmaneuvering the Filmverlag der Autoren, which was usually distributing Fassbinders films. 
 Der Stern from March over a period of three months, thus increasing public interest in the film.  The official premiere of the film was on 20 February 1979 during the 29th Berlin International Film Festival. The West German theatrical release was on 23 March 1979. At the Berlin International Film Festival Hanna Schygulla won the Silver Bear for Best Actress, which did not satisfy Fassbinder who expected to win the Golden Bear. 

===Contemporary reception===
German film critics responded very positively to the film and praised the films combination of artistic values with mass appeal. In the weekly newspaper Die Zeit Hans-Christoph Blumenberg called the film "the most accessible (and thus most commercial) and mature work of the director".  Karena Niehoff wrote in the daily newspaper Süddeutsche Zeitung that The Marriage of Maria Braun "is a charming and even amusing film, at the same extraordinarily artful, artificial and full of twists and turns".  , "ein richtig charmanter und sogar witziger Kinofilm und zugleich ungemein kunstvoll, künstlich und mit Falltüren noch und noch" 
 David Denby Dietrich and Jean Harlow|Harlow".     Schygulla, too, was the runner-up for the National Society of Film Critics Award for Best Actress that year, losing to Sally Field for Norma Rae.
 Wedekind and Douglas Sirk and particularly touching is his idea of a man who looks on men and on women with equal fondness.  The French film critic Jean de Baroncelli discussed the allegorical qualities of the film in Le Monde on 19 January 1980 and wrote that the film presents Maria Braun with a "shining simplicity" as an allegory of Germany, "a character, that wears flashy and expensive clothes, but has lost her soul". 

Roger Ebert added the film to his Great Movies collection.   

===Commercial success and aftermath===
The Marriage of Maria Braun was not only a critical, but also a commercial success. From its release until October 1979 more than 400,000 tickets were sold in West Germany, and was shown for up to 20 weeks in some film theaters.    In West Germany alone the film made more than 4 million DM at the box office.  In the same year of its German release the distribution deals for 25 countries were negotiated. In August 1981 the film was the first film by Fassbinder to be shown in East German film theaters.  In the first six weeks of its theatrical release in the United States the film made $1.8 million at the box office. 
 official German Best Foreign The Tin Lili Marleen with Hanna Schygulla in the main role. Horst Wendlandt would produce the two other films in the BRD Trilogy, Lola (1981 film)|Lola and Veronika Voss. His success also allowed him to realize his last project, Querelle which was co-financed by Gaumont Film Company|Gaumont. 
 Federal Court of Justice, but also ruled that the Fassbinder heirs were entitled to a share of the films profits.  Today all film rights are owned by the Rainer Werner Fassbinder Foundation. 

==Bibliography==
*   (Screenplay)
*   (Novel based on the film)
*  

==References==
 

==Further reading==

*Anton Kaes. From Hitler to Heimat: The Return of History as Film. Cambridge, Massachusetts: Harvard University Press, 1989.

==External links==
* 
*   at    
*   at the    
*  

 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 