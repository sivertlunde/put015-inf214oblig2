X (1986 film)
  Norwegian film from 1986 in film|1986. The play was directed by Oddvar Einarson, and starred Bettina Banoun and Jørn Christensen. The film started through the collective Film Group 84, and was completed by Einarson own production company.

X was released on DVD in 2010, with a lengthy interview with the director as extras.

== Plot ==
Jon Gabriel is a young photo artist who is skilled in his field, but helpless in dealing with people. 13-year-old Flora is homeless, but experienced in life. The film depicts the evolution of the relationship between the two as they struggle to survive.

==Cinematic Style==

X is a low-budget film, reflected in its raw and unvarnished style. The film is set in Oslo, but there is a point in the movie that it should not be connected directly to a particular location. The pictures often show winter cold and dirty cityscapes, with little beautifying effects.

== Music==

Norwegian underground rock plays an important role in the films sound. The soundtrack features music by Andrej Bea, and an appearance by Holy Toy. The film also contains two songs by the Backstreet Girls, who  also play a scene in the movie.

==Awards and honors==

The film won the Amanda Award as best Norwegian film in 1987.  In the same year it received the Special Jury Prize at the Venice Film Festival.

In a competition to find the best ever Norwegian films in 2012, X came in ninth place. 

==References==
 

 
 
 

 