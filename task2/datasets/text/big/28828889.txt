A Queen for Caesar
{{Infobox film
 | name = A Queen for Caesar
 | image =  A Queen for Caesar.jpg
 | caption =
 | director = Piero Pierotti  Victor Tourjansky 
 | writer = 
 | story =  
 | starring =  
 | music =  Michel Michelet 
 | cinematography = Pier Ludovico Pavoni 
 | editing =
 | producer = 
 | distributor =
 | released = 1962 Italian 
 }} 1962  historical drama Egypt in Caesar and Cleopatra VII|Cleopatra, this film focuses entirely on the dynastic struggle within Egypt leading up to the arrival of Caesar, and in fact, we only see him in the closing scene of the film when he arrives at The Ptolemaic Palace in Alexandria.

This film was directed by Piero Pierotti and Victor Tourjansky. 20th Century Fox bought the rights for the film to keep it out of release lest it compete with their own Elizabeth Taylor Cleopatra (1963 film)|Cleopatra. 

==Cast== Cleopatra
*George Ardisson as Achillas
*Rik Battaglia as Lucius Septimius Ptolemy
*Franco Volpi as Apollodoros
*Ennio Balbo as Theodotos
*Nerio Bernardi as Scaurus
*Aurora de Alba as Rabis
*Nando Angelini as Sextus Pompeius
*Nino Marchetti as Pompeys messenger Gnaeus Pompeius
*Gordon Scott as Julius Caesar

==See also==
*List of historical drama films
*List of films set in ancient Rome
*Cleopatra VII
*Ptolemaic dynasty
*History of Ptolemaic Egypt
*1st century BC

==Notes==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 