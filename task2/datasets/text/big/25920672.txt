Case of the Full Moon Murders
{{Infobox Film
| name           = Case of the Full Moon Murders
| image          = TheCaseoftheSmilingStiffs.jpg
| image_size     = 
| caption        = Film poster under the title The Case of the Smiling Stiffs
| director       = Sean S. Cunningham Brud Talbot
| writer         = Jerry Hayling
| starring       = Sheila Stuart
| music          = Steve Chapin
| cinematography = Gus Graham
| editing        = Steve Miner
| distributor    = Sean S. Cunningham Films
| studio         = Dana Films Lobster Enterprises
| released       = October 17, 1973
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Case of the Full Moon Murders (also known as The Case of the Smiling Stiffs) is a 1973 film directed by Sean S. Cunningham.

==Plot==
A killer who may be a vampire leaves her victims with smiles on their faces.

==Tagline==
The First Sex-Rated Whodunit!

==Cast==
*Sheila Stuart as Emma
*Jed Ziegler as Ice Cream Vender
*Cathy Walker as Caroline
*Kenny Abston as Chuck
*Geoffrey Knowles as Beau
*Victoria Holloway as Daisy
*Fred J. Lincoln as Joe (credited as Fred Lincoln)
*Ron Millkie as Frank (credited as Ron Browne)
*Harry Reems as Silverman
*Sally Ziegler as Joes Mother
*Ann Marshall as Nora
*Jean Jennings as Strip Poker Girl
*Debbie Craven as Strip Poker Girl Harold Robinson as Couple Watching T.V.
*Christine Robinson as Couple Watching T.V.

==Filming locations==
The movie was filmed in Miami, Florida.

==External links==
* 
* 

 

 
 
 
 
 
 
 


 