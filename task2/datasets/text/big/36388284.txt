An Uncommon King
{{multiple issues|
 
 
}}

  in An Uncommon King.]]

An Uncommon King is a 2011 72-minute HD documentary film directed by award-winning documentary and television producer Johanna J. Lunn.   at the Internet Movie Database  It records the life of Sakyong Mipham Rinpoche, son of meditation master Chögyam Trungpa and leader of the Shambhala Buddhist lineage and Shambhala International, a global community with over 200 meditation centers. 

==Synopsis==
Through original and archival footage and interviews, the film follows Mipham Rinpoche from a young child living in a Tibetan refugee camp in India to his move to the West to being empowered as a Sakyong or “Earth Protector." The film captures Mipham Rinpoche on retreat in India and at work promoting his books and building a school in Tibet.  It also captures personal moments, including Rinpoche at home in Nova Scotia, running marathons,  recording music, and proposing to his Tibetan princess bride, Khandro Tseyang Ripa Mukpo.

"This biography is about the establishment of the lineage of Sakyongs," says director Johanna J. Lunn.  "Where Sakyong Mipham Rinpoche comes from and what his life purpose is. It’s a father–son story, it’s a story about wisdom passing from East to West. Above all it’s a story about basic goodness. It’s a living story that is unfolding all the time."

==Production==
An Uncommon King is an English-language film shot over 17 years in 12 different countries, including Tibet, India, China, Europe and North America. Produced by award-winning director Johanna J. Lunn and cinematographer James Hoagland, starring Sakyong Mipham Rinpcohe. 

==References==
 

 
 