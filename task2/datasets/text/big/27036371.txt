On the Banks of the Wabash (film)
{{Infobox film
| name           = On the Banks of the Wabash
| caption        = 
| image	=	On the Banks of the Wabash FilmPoster.jpeg
| director       = J. Stuart Blackton
| producer       = Albert E. Smith
| writer         =
| starring       = Mary Carr Madge Evans Burr McIntosh
| music          =
| cinematography = Nicholas Musuraca
| editing        =
| distributor    = Vitagraph Studios
| released       = October 22, 1923
| runtime        = 70 minutes
| country        = United States
| language       = Silent film (English intertitles)
}}
 silent rural melodrama film directed by J. Stuart Blackton and produced and distributed by his movie company, Vitagraph Studios. The film is very loosely based on Paul Dressers song/poem "On the Banks of the Wabash, Far Away". The film was an expensive production, with full-size riverboat steamboat and location shooting. It was one of the last major productions by Vitagraph before they were bought by Warner Bros. 

The film stars 14-year-old Madge Evans, Mary Carr, and James W. Morrison. The cameraman was Nicholas Musuraca. Reportedly, a private collector holds an abridged, or shortened, version of this film. 

==Cast==
*Mary Carr - Anne Bixler
*Burr McIntosh - "Cap" Hammond
*James W. Morrison - David
*Lumsden Hare - Paul Bixler
*Mary MacLaren - Yvonne
*Madge Evans - Lisbeth
*George Neville - Sash Brown
*Marcia Harris - Tilda Spiffen

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 