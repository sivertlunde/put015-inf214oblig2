They Stooge to Conga
{{Infobox Film |
  | name           = They Stooge to Conga
  | image          = Theytoogetoconga43LOBBY.jpg
  | caption        = 
  | director       = Del Lord 
  | writer         = Monte Collins Elwood Ullman  Stanley Brown John Tyrrell
  | cinematography = George Meehan 
  | editing        = Paul Borofsky 
  | producer       = Del Lord Hugh McCollum Jules White 
  | distributor    = Columbia Pictures 
  | released       =   
  | runtime        = 15 32" 
  | country        = United States
  | language       = English
}}

They Stooge to Conga is the 67th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are repairmen fixing the doorbell in a large house which is the secret headquarters of some Nazi spies, headed by the ruthless Hans (Vernon Dent). They manage to ruin most of the house while working on the wiring and then subdue the spies and sink an enemy submarine by remote control.

==Production notes==
*The title They Stooge to Conga is a parody of the eighteenth-century play She Stoops to Conquer.  Shemp in 1952s Listen, Judge.   
*The footage of the submarine jumping out of the water was recycled from Three Little Sew and Sews. The Three Stooges where Larry is wearing a sandwich board.

==Violence== acetylene torch.

Interestingly, though Columbia short subject head/director  

===Notable violent gags===
*When the trio first enter the house, Moe and Larry try to enter the house simultaneously. They are wedged in the doorway, and get thrusted out when Curly comes up from behind with the point of an anvil as a gouge.
*When Curly is pulling a wire out of a wall he pulls out a ringing phone. He answers it and says "This line is busy" and throws it away hitting Moe in the head. Moe throws it back in retaliation, hitting Curly in the head, satisfying Moe and startling Curly.
*When Moe is pulled through the wall by Larry and Curly, an actual 2x4 made of solid wood crashed onto Moes neck.
*When Moe twists Curlys nose with a tool, he uses a grinding wheel to file it back into shape.
*As Moe and Larry assist Curly up a telephone pole, Curly accidentally impales Moe in his scalp, eye and ear with a climbing spike on the bottom of his shoe.
*When Curly is halfway up the telephone pole, Moe burns him with a flame torch to get him all the way up.
*After Curly drops a wrench, it lands on Moes head, bouncing into Larrys hand. Moe uses the wrench on Larrys nose while hitting him in the throat.
*When Curly gets zapped via several telephone pole wires, he loses his grip and falls to the sidewalk, landing on Moe and Larry below.
*While Curly is "charged like a battery," Moe places a light bulb in Curlys ear, which lights up. To short him out, Larry places a screwdriver in Curlys opposite ear, bursting the light bulb.
*As Curly is sliding on electrical wires, he gets a shock, which pushes him through an open window.
*As the Nazi spies cook (Dudley Dickerson) is talking on the phone, the phone explodes in his face due to Curlys manipulating the electrical wires. The startled cook then backs away from the phone, right into an open waffle iron. The iron closes on the cooks buttocks, leading the cook to think he is being attacked by someone.
*When Moe takes a hammer, he hits Larry from behind, then thrusts it into Curlys mouth. Curly, in turn, bonks Moe with his own hammer 20 times in rapid succession.

== References ==
 

==External links==
*  .
*  
*  

 

 
 
 
 
 
 
 
 
 