Im Juli
 
{{Infobox film
| name           = Im Juli.
| image          = ImJuli.jpg
| caption        = German DVD cover
| director       = Fatih Akın
| producer       =
| writer         =
| narrator       =
| starring       = Moritz Bleibtreu, Christiane Paul
| music          =
| cinematography =
| editing        =
| distributor    = Koch-Lorber Films
| released       = 2000
| runtime        =
| country        = Germany-Turkey German
| budget         =
}}
Im Juli. ( ) is a 2000 German-Turkish road movie.

==Plot==
  Mayan sun symbol, which, according to Juli, has the power to lead him to the woman of his dreams, whom he will recognise by a similar sun symbol. As Juli has the rings counterpart, and as she is in love with him, she invites Daniel to a party that evening, hoping that they will meet.

Curious, Daniel goes to the party and meets Melek (İdil Üner), a young Turkish woman who is wearing a T-shirt imprinted with a sun symbol. Convinced that she is the woman of his dreams, Daniel talks to her. Melek is only passing through and looking for a place to spend the night. After spending the evening together seeing the sights of Hamburg, Daniel invites her to spend the night in his apartment.
 hitch a ride, with no predetermined destination.
 fate would have it, Daniel is the first car to stop, on his way back from the Hamburg Airport|airport, where he has just dropped Melek off. He has decided to drive to Istanbul in search of Melek, who, as she told him, will be under the bridge over the Bosporus at a certain time a few days later, although he does not know why. Daniel takes Juli with him in his stoner roommates rusty old car. And this is the beginning a long and exciting trip across a scorching hot Southeastern Europe.

==Symbolism==
The film opens with an eclipse, an event in which the sun and the moon cross paths.  Daniel crosses paths with Luna, whose name means "moon," and suffers the consequences.  July is a sunny month.

==Cast==
* Moritz Bleibtreu as Daniel Bannier
* Christiane Paul as Juli
* İdil Üner as Melek
* Mehmet Kurtuluş as İsa
* Jochen Nickel as Leo
* Branka Katić as Luna
* Birol Ünel as Club Doyen
* Sandra Borgmann as  Marion
* Ernest Hausmann as Kodjo, Daniels neighbour
* Gábor Salinger as Jewellery vendor in Hungarian market
* Cem Akın as Turkish border guard
* Fatih Akın as Romanian border guard
* Sándor Badár as Alin

== Trivia ==
* The sequence of the film when Daniel and Juli travel through Romania is made up of a series of still photos, since the production team was not able to obtain the required authorization to film in Romania.
* The director has a cameo as a Romanian border guard on a checkpoint of the Hungarian-Romanian border.
* The directors brother Cem Akin plays the role of a Turkish border guard on a checkpoint of the Bulgarian-Turkish border.

== Accolades ==
* Deutscher Filmpreis 2001: Moritz Bleibtreu (Best actor)
* Tromsø International Film Festival: Peoples choice award: Fatih Akin (Best direction)

== External links ==
*  

 

 
 
 
 
 
 
 
 