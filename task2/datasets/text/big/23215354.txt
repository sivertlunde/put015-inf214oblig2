The Green Elephant
 
 
{{Infobox film
| name           = The Green Elephant Зеленый Слоник
| image          = The_Green_Elephant_DVD_Cover.jpg
| image_size     = 
| caption        = 
| director       = Svetlana Baskova
| producer       = Oleg Mavromati
| writer         = Svetlana Baskova
| narrator       = 
| starring       = Sergey Pakhomov Alexandr Maslaev Vladimir Epifantsev Anatoly Osmolovsky
| music          = 
| cinematography = Svetlana Baskova
| editing        = Supernova Studio
| distributor    = Channel One Russia
| released       = 1999
| runtime        = 86 minutes
| country        = Russia 
| language       = Russian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1999 Cinema Russian art-house slasher gore film directed by Svetlana Baskova. The movie received a limited theatrical release in Russia, as the films violent imagery and graphic language made it unable to be distributed through the mainstream film circuit.  The film was shown at the 2005 International Film Festival Rotterdam and the program commented that the movie was "even more urgent because of the escalation of the war in Chechnya and growing criminality in the Russian army". 

The Green Elephant stars Sergey Pakhomov and Vladimir Epifantsev, and follows two Russian officers locked in a military prison cell that must deal with "social and psychological problems" in their isolation through brutality and torture.   

==Plot summary==
Two men (both being junior officers in Soviet Army) are being held in a penal military prison. The room which they share looks like a dark cellar with a dripping sewer pipe running through it. One of the prisoners seems to be delirious and never stops talking. He tells stories of his past, does push-ups, comes up with crazy ideas and even mimics a heron in an attempt to cheer up his cellmate, leading only to an increase in his anger. The clumsy talker receives a violent beating from the enraged prisoner.The angry officer is then taken from the cell by a guard to clean up a dirty toilet bowl with a fork. Some time later, after the angry officer falls asleep, the insane officer defecates on their shared plate for eating (calling it "sweety bread"), smears feces over his belly and consumes a large portion of it. After doing so he offers a plate with the fecal matter to the other man right after he wakes up and drives him mad again. The two main characters go through a series of humiliating acts conducted by a guard and a captain who seems to have strong sadistic tendencies. The story resolves with an act of graphic violence, including rape, sodomy, disembowelment, committing suicide and having an uncontrolable urge to break out of the prison.

==References==
 

==External links==
*  
*  : Director Svetlana Baskovas website
*  

 
 
 
 
 
 
 


 
 