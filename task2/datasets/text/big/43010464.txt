Revenge of the Living Dead Girls
{{Infobox film
| name           = Revenge of the Living Dead Girls
| image          = Revenge of the Living Dead Girls.jpg
| alt            =
| caption        =
| film name      = La revanche des mortes vivantes
| director       = Pierre B. Reinhard
| producer       = Jean-Claude Roy
| writer         = Jean-Claude Roy
| starring       =  Véronique Catanzaro Kathryn Charly Sylvie Novak Anthea Wyler Laurence Mercier Patrick Guillemin Gabor Rassov Christina Schmidt Cornélia Wilms
| music          = Christopher Reid
| cinematography = Henry Frogers
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         =
| gross          =
}}
Revenge of the Living Dead Girls ( ) is a 1987 French splatter film directed by Pierre B. Reinhard, written by Jean-Claude Roy, and starring Véronique Catanzaro, Kathryn Charly, Sylvie Novak, Anthea Wyler, Laurence Mercier, Patrick Guillemin, Gabor Rassov, Christina Schmidt, and Cornélia Wilms.  Toxic waste causes a zombie outbreak in France.

== Plot ==
In France, the CEO of a chemical company looks to cheaply dispose of their plants chemical waste.  He and his secretary come up with the idea to dump it illegally.  When the secretary contaminates a milk tanker, it causes several deaths in the town.  The toxic waste, illegally dumped in a nearby graveyard, then causes the recently dead townspeople to rise as ravenous zombies, who seek revenge on the unscrupulous company and its employees.

== Cast ==
* Véronique Catanzaro
* Kathryn Charly
* Sylvie Novak
* Anthea Wyler
* Laurence Mercier
* Patrick Guillemin
* Gabor Rassov
* Christina Schmidt
* Cornélia Wilms

== Release ==
Revenge of the Living Dead Girls premiered in France on 16 September 1987.   It was released on DVD on 8 August 2006. 

== Reception ==
Dave Bow of The Portland Mercury rated it 2/4 stars and wrote that story is confused and the ending makes no sense.   Peter Schorn of IGN rated it 2/10 and called it a weak ripoff of The Return of the Living Dead.   Ian Jane of DVD Talk rated it 3/5 stars and called it "an entertaining slice of European sleaze". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 