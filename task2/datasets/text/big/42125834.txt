Chess Game
{{Infobox film
| name           = Chess Game
| image          = Chess_Game_Film_Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Luis Antonio Pereira
| producer       = 
| screenplay     = Luis Antonio Pereira
| story          = 
| starring       = Antonio Calloni Priscila Fantin Tuca Andrada Carla Marins Salvatore Giuliano
| music          = André Paixão
| cinematography = Kika Cunha
| editing        = Marcelo Moraes
| studio         = Eclectic Entertainment
| distributor    = Elo Company 
| released       =  
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = $1 million
| gross          = 
}}
 thriller film written and directed by Luis Antonio Pereira. 

The film follows the story of a woman who is sentenced to prison because of a social security fraud involving a senator. The senator fears that she can tell the truth to the authorities, so he bribes the prison warden to prevent her from telling the truth. 

==Cast==
*Antonio Calloni as Senator Franco
*Priscila Fantin as Mina
*Tuca Andrada as Diretor Geraldo
*Carla Marins as Beth
*Salvatore Giuliano as Eugênio
*Tarciana Saad as Delegada Bandeira
*Wesley Aguiar as Alberto
*Luana Xavier as Martona
*Martha Paiva as Déia
*Carla Franca as Barney
*Fabiano Costa as Xavier
*Fabio Nascimento as Valtinho
*Erlene Melo as Nurse

==References==
 

==External links==
*  

 

 
 
 
 
 
 