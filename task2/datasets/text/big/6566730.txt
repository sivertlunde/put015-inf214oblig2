Bad Blood (1982 film)
 
{{Infobox film
| name = Bad Blood
| image = Bad_Blood_Film_1981_Poster.jpg
| caption = Theatrical release poster Mike Newell
| producer = Andrew Brown
| starring =  
| music = Richard Hartley
| cinematography = Gary Hansen
| distributor = Umbrella Entertainment
| released =  
| runtime = 105 minutes
| language = English
| country = New Zealand   United Kingdom
}}
Bad Blood is a 1982 British-New Zealand   (Whitcoulls, 1979) and adapted by NZ-born Andrew Brown.     

==Plot==
In October 1941, Stan Graham, a Westland smallholder, develops a persecution complex and starts to threaten his neighbours. They put up with it for a while but things become intolerable. One day a party of four policemen arrive to confiscate his firearms. This causes a flashpoint for him - he loves his guns and is not going to hand them over. He shoots all the cops and, in the ensuing altercations, three more locals before heading to the hills. A manhunt composed of police, army and homeguard is organised and delivers Grahams come-uppance. Based upon a true event that happened around the present community of Kowhitirangi, out of Hokitika.

==Cast== Jack Thompson Stan Graham
* Carol Burns as Dorothy Graham
* Denis Lill as Ted Best
* Martyn Sanderson as Les North
* Kelly Johnson as Jim Quirke
* Bruce Allpress as Inspector Calwell

==Release== Britain in 1981, it was released in New Zealand on the 12 November 1982.
It was released on DVD in May 2006 and again on the 16 September 2009. It was also released in the United Kingdom and limitedly in North America on 2 Oct. 2007.

==Reception==
Bad Blood received rave reviews in Britain and later in New Zealand. In New Zealand Bad Blood is consider a classic in New Zealand film, although consider risky when released it has since become a go to for Classics.

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 


 
 