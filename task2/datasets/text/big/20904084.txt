Farishta Ya Qatil
{{Infobox film
| name           = Farishta Ya Qatil
| image          = Farishta Ya Qatil.jpg
| image_size     =
| caption        = 
| director       = 
| producer       = 
| writer         =
| narrator       = 
| starring       = Shashi Kapoor   Rekha
| music          = Kalyanji Anandji
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 Bollywood action film directed by S.M. Abbas. The film stars Shashi Kapoor and Rekha.

==Cast==
*Shashi Kapoor   
*Rekha   
*Jayshree T.   
*Yunus Parvez   
*Utpal Dutt    Bindu   
*Sujit Kumar   
*Mac Mohan   
*Prem Nath   
*Om Shivpuri    Johnny Walker

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Are Baaton Ke Hum Badsha"
| Mohammed Rafi
|-
| 2
| "Ishq Mein Tum To"
| Mohammed Rafi, Kishore Kumar, Usha Mangeshkar
|-
| 3
| "Kahin Dekha Na Shabab Aisa"
| Mohammed Rafi, Asha Bhosle
|-
| 4
| "Kali Kali Zulfon Mein"
| Anuradha Paudwal, Usha Timothy
|-
| 5
| "Tasveer Hai Yeh Pyar Ki" Mukesh
|-
| 6
| "Kuchh Aese Bandhan Hote Hai"
| Mukesh, Lata Mangeshkar
|}

==External links==
*  

 
 
 
 
 

 
 