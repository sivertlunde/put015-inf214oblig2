Wake Up Sid
 
 

 

{{Infobox film
| name = Wake Up Sid
| image = Wake up Sid.jpg
| caption = Theatrical release poster
| director = Ayan Mukerji
| producer = Hiroo Yash Johar Karan Johar
| writer         = Niranjan Iyengar  (Dialogue) 
| story = Ayan Mukerji
| screenplay = Ayan Mukerji
| starring = Ranbir Kapoor Konkona Sen Sharma Anupam Kher Supriya Pathak Rahul Khanna Kashmira Shah
| music =  
| cinematography = Anil Mehta
| editing = Shan Mohammed
| distributor = Dharma Productions UTV Motion Pictures
| released = 2 October 2009
| runtime = 138 minutes
| country = India
| language = Hindi
| budget =  
| gross =  
}}
 coming of age drama film.  Directed by Ayan Mukerji and produced by Karan Johars Dharma Productions, the movie was distributed by UTV Motion Pictures, with visual effects contributed by the Prime Focus Group.  The film takes place in contemporary Bombay and tells the story of spoiled, careless rich-kid Sid Mehra (Ranbir Kapoor), a college student who is taught the value of owning up to responsibility by Aisha (Konkona Sen Sharma), an aspiring writer from Calcutta. It was critically and commercially successful. Ranbir Kapoor won numerous awards for his performance, and the films soundtrack was vastly popular.

==Plot==
Siddharth "Sid" Mehra (Ranbir Kapoor) is the carefree son of wealthy businessman Ram Mehra (Anupam Kher). A resident of Mumbai, Sids only interest is to have fun and spend his fathers money. Thus, he spends little time at college and is not prepared for final exams. His father, unaware of this fact, tells Sid it is time to join his company, which Sid reluctantly does only after his father tempts him with the promise of a new car.

Sid meets Aisha ( ). On the eve of her 27th birthday, Aisha invites Sid to her place. When Sid tentatively explores moving their relationship beyond friendship, he finds that Aisha perceives him as boyish and immature. Despite feeling hurt, he maintains his friendship with her.

When the exam results come out, Sid finds that he has failed. His failure leads to an intense family fight that ends with him moving out. With nowhere to go, Sid (who has never been on his own) asks Aisha if he can stay with her. Aisha is initially happy to have the company. When she finds out he has not eaten all day, Ayesha is amused by the fact that Sid cannot cook and does not know how to feed himself. Sid leaves her place a mess and throws tantrums. Over time, Sid learns that to care for himself, he has to begin cooking and cleaning. He also realises he must work, and Aisha helps him land a role as a photography intern at the magazine she works for.

Aisha is ecstatic when Kabir, her manager at Bombay Beat, selects her article for publishing at the magazine and asks her on a date. However, she realises she does not have much in common with Kabir, despite her initial attraction to him. She then understands that she is in love with Sid. As Sid begins working, he finds meaning in life. He begins to see for the first time how his behavior has hurt his loved ones and that he needs to change his dissolute ways. After he is hired as full-time staff and receives his first paycheck, he meets his father. They reconcile and Sids father asks him to come home. Sid, who often considers himself a burden on Aisha, tells her that he wont be dependent on her anymore and expects her to be thrilled. He doesnt realise that Aisha has fallen in love with him and so is furious to hear that he is leaving. Confused and hurt, Sid makes his departure. 

When Sid returns home, he thinks constantly of Aisha, though he does not believe she might share his feelings since she told him that they could only be friends. When the latest issue of Bombay Beat arrives, Sid reads Aishas column and is startled to discover that it is about her feelings for him. As it starts raining, he rushes to meet her at the same beach where they had gone the day they met. Sid expresses his love for her and the two embrace.

==Cast==
* Ranbir Kapoor as Siddharth "Sid" Mehra
* Konkona Sen Sharma as Aisha Banerjee
* Rahul Khanna as Kabir Chaudhary
* Anupam Kher as Ram Mehra
* Shikha Talsania as Laxmi
* Supriya Pathak as Sarita
* Namit Das as Rishi
* Kashmira Shah as Sonia
* Rahul Pendkalkar
* Shruti Bapna
* Kainaz Motivala as Tanya Lathia
* Natasha Rathod

==Release==

===Box office===
Wake Up Sid opened well in India and overseas markets.   Its opening weekend gross was   of which the domestic gross was  .  It was number one in the box office during its first and second weeks,   number four during its third week,  and number three during its fourth week. 

In the United Kingdom, the film collected US$165,934, while in the US, the collections were US$717,977.  By its fourth week, it grossed $348,351 in New Zealand and the United Kingdom. Do Knot Disturb also released during the same time, but Wake Up Sid got a larger portion of the audience. 

After grossing about  , the film was declared a hit.

===Critical reception===
Wake Up Sid was well received by a number of critics. Subhash K. Jha gave Wake Up Sid a rave review stating that it is, "a triumph on many levels   Ayan Mukherjee takes the age-old dramatic conflicts of our commercial cinema into understated corridors."    Rajeev Masand of CNN-IBN gave the film three out of five stars, stating that Wake Up Sid "has its heart in the right place and marks the breakout of a bright, shining star who has come into his own so early in his acting career. Watch it, and be awestruck by Ranbir."  Taran Adarsh of Bollywood Hungama gave the film four out of five stars as well as a "thumbs up" stating that it is "strongly recommended."   

Mayank Shekhar of the   gave it three and half out of four stars and suggests that, "Wake Up Sid becomes a sort of template of how GenNow navigate their lives: deal with their own little rebellions, find meaning to their own definitions of independence and handle their own set of mistakes. It feels good when the two friends finally meet in driving rain under the grey skies by the sea. Refreshing and heart-warming, Wake Up Sid really puts you in the mood for love."  Noyon Jyoti Parasara of AOL India gave it three and half out of five stars and praised the director saying, "Ayan Mukerji arrives in style and manages to leave his own mark on the film despite having a producer like Karan Johar whose other productions always tend to have his stamp. Wake Up Sid really puts you in the mood for love." 

Joginder Tuteja of the   argues that: "With no big production numbers (songs play over montage sequences), a quiet style and credible characters, Wake Up Sid is Bollywood in an indie mood, a film for people like Aisha and Sid: young and educated. It may not be as hip as Bombay Beat, the magazine where the two work, but it shows that Mr. Mukherji is a director to watch."   

===Controversy===
On 2 October, Maharashtra Navnirman Sena supporters protested to halt the screening of Wake Up Sid in Mumbai and Pune. The MNS objected the use of the word "Bombay" instead of "Mumbai" in the movie. 

===Shooting===
The Movie was entirely shoot in Mumbai, covering the South and Bandra Side. The college sequence was shot at H.R. College of Commerce and Economics near Churchgate. 

==Awards==
2010: Filmfare Awards
* Won: Best Debutante Director (Male) - Ayan Mukherjee
* Won:Filmfare Critics Award for Best Actor - Ranbir Kapoor Best Female Playback - Kavita Seth for "Iktara" 
  

==Soundtrack==
The soundtrack has music composed by Shankar-Ehsaan-Loy with lyrics by Javed Akhtar. The music was released on 21 August 2009. One song in the film has been composed by Amit Trivedi who scored the background music for the film.

{{Infobox album  
| Name = Wake Up Sid
| Type = soundtrack
| Artist = Shankar-Ehsaan-Loy
| Cover = wakeupsid.jpg
| Released =  
| Recorded = Feature film soundtrack
| Length = 1:34:31
| Label = Sony Music Entertainment
| Producer = Karan Johar Ronnie Screwvala
| Reviews =
| Last album =   (2009)
| This album = Wake Up Sid (2009)
| Next album = London Dreams (2009)
}}

{{tracklist
| headline = Tracklist
| all_music =
| music_credits = yes
| extra_column = Artist(s)
| title1 = Wake Up Sid!
| extra1 = Shankar Mahadevan
| music1 = Shankar-Ehsaan-Loy
| length1 = 3:51
| title2 = Kya Karun?
| extra2 = Clinton Cerejo
| music2 = Shankar-Ehsaan-Loy
| length2 = 4:13
| title3 = Aaj Kal Zindagi
| extra3 = Shankar Mahadevan
| music3 = Shankar-Ehsaan-Loy
| length3 = 4:13
| title4 = Iktara
| extra4 = Kavita Seth, Amitabh Bhattacharya
| music4 = Amit Trivedi
| length4 = 5:23
| title5 = Life Is Crazy
| extra5 = Shankar Mahadevan, Uday Benegal
| music5 = Shankar-Ehsaan-Loy
| length5 = 4:14
| title6 = Iktara
| extra6 = Tochi Raina, Amitabh Bhattacharya, Raman Mahadevan
| music6 = Amit Trivedi
| length6 = 3:44
| title7 = Wake Up Sid! (Club Mix)
| extra7 = Shankar Mahadevan
| music7 = Shankar-Ehsaan-Loy
| length7 = 3:44
}}

===Reception===
{{Album ratings
| rev1 = Bollywood Hungama
| rev1Score =   
| rev2 = Rediff.com
| rev2Score =   
}}
 
The reactions towards the music were favorable. Chandrima Pal of Rediff gave the album 3.5 stars, stating, "the music sticks to the brief. It is hip, urban and bubbly, and unhurried. And thanks to Shankar Ehsaan Loys musicianship, it is a slick, well-balanced production".    Joginder Tuteja (Bollywood Hungama) gave it 3 stars out of possible 5, suggesting that, "this may not really turn out to be the best seller of the year but should certainly be a perfect fit for the narrative."   

==Sequel==
Rishi Kapoor and Ranbir Kapoor confirmed the possibility of a sequel to the film.   

==References==
 

==External links==
*  - Dharma Productions
*  
*  

 
 
 

 
 
 
 
 