After the Waterfall
 
 
{{Infobox film
| name           = After the Waterfall
| image          = After the waterfall.jpg
| caption        = Theatrical release poster
| director       = Simone Horrocks
| producer       = Trevor Haysom
| writer         = Simone Horrocks Georgia Rose
| music          = Joel Haines
| cinematography = Jac Fitzgerald
| editing        = Cushla Dillon
| studio         = NZ Film Commission
| distributor    = Rialto Distribution
| released       =  
| runtime        = 94 minutes
| country        = New Zealand
| language       = English
| budget         = 
| gross          = 
}} Outrageous Fortune and was the first feature film directed by Simone Horrocks.  It was released theatrically in New Zealand on 4 November 2010.

==Plot==
John Drean (Antony Starr) is a park ranger whose marriage to his wife Ana (Sally Stockwell) begins to suffer when their daughter, Pearl (Georgia Rose), goes missing under his care. Four years later, John is still looking for his little girl and his life seems to be at a standstill, while Ana is in a relationship with Johns best friend, and the policeman behind Pearls disappearance, David (Cohen Holloway). Things begin to change for the better, however, when Johns father, George (Peter McCauley), believes he saw Pearl walking past a shop window.

==Cast==
* Antony Starr as John Drean
* Kate Beckinsale as Ana Drean
* Cohen Holloway as David
* Peter McCauley as George Georgia Rose as Pearl

==Production== Outrageous Fortune actor Antony Starr would be portraying the lead role.    Horrocks had previously attracted international attention when she was a semi-finalist for the prestigious Sundance Institute/NHK Filmmakers Award in 2001.

==Reception==

===Critical reception===
The film was released to critical acclaim throughout New Zealand with many praising Starrs acting. Darren Bevan of TVNZ stated that the film was great but slightly let down by Cohen Holloways performance but Starr makes up for the small hiccup.   
Francesca Rudkin of The New Zealand Herald gave the film 4/5 stars praising Starrs performance and the plot. She also praises Peter McCauleys performance and the soundtrack saying it adds to the atmosphere immensely.    Andrew Hedley of Flicks.co.nz praised Starrs acting but noted the film was at times inconsistent and poorly written.    Helen Martin of Onfilm magazine praised Starrs acting alongside the camera shots and music.    Christine Powley of the Otago Daily Times criticized the inexperience of the crew but praised how the movie felt Kiwi without resorting to Kiwiana.   

===Accolades===
The film received several nominations in the 2011 Aotearoa Film & Television Awards, including; director Simone Horrocks for "Outstanding Feature Film Debut", "Best Editing in a Feature Film", "Images & Sound Best Sound in a Feature Film" and "Best Lead Actor in a Feature Film" for Antony Starr.   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 