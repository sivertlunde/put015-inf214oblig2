Biswas Nao Korte Paren
 

{{Infobox film
| name           = Biswas Nao Korte Paren (You May Not Believe)
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Pradipta Bhattacharyya 
| producer       = Sougata Purakayastha
| writer         = 
| screenplay     = Pradipta Bhattacharyya 
| story          = 
| based on       =  
| narrator       =  Ritwik Chakraborty Nitya Ganguly
| music          = 
| cinematography = Subhadeep Dey 
| editing        = Pradipta Bhattacharyya 
| studio         = Bangladesh Television
| distributor    = Tara Muzik 
| released       = 2006
| runtime        = 24:40
| country        = India
| language       = 
| budget         = 
| gross          =
}}
Biswas Nao Korte Paren ("You May Not Believe") is a 2006 Indian film.

==Synopsis==
What is a name? Is it merely a word? A name of a person denotes his/her external identity. It is related to each and every social action and reaction of a person with other persons. The film deals with the plight of the protagonist whose name has changed overnight. The problem aggravates when the changed name connotes religious and cultural differences when Shymal (a Hindu name) or Salim (a Muslim name), the protagonist becomes Salim or Shyamal. The film brings into focus the crisis in his life and the pain of being ostracized and alienated by everyone that matters to him. In such a situation of loneliness, Shyamal identifies with his next-door neighbor who has also been alienated and is fighting his lonely existence. 

The film portrays excellent photographic work, interesting camera angles mostly associated with directors from Middle-Eastern countries. This film has found a niche fan-following within the urban Bengali crowd and its abstract ending was well-appreciated by the viewers. 
With this film, the director Pradipta Bhattacharya has emerged as one of the most talented young directors from Bengal following a path laid down by the big names such as Satyajit Ray, Mrinal Sen and Ritwik Ghatak.  
 
==Cast==
* Ritwik Chakraborty 
* Nitya Ganguly 

==Crew==
* Director &mdash; Pradipta Bhattacharyya 
* Executive Producer &mdash; Sougata Purakayastha (Tara Muzik) 
* Production Company &mdash; Bangladesh TV (BTV) 
* Distribution &mdash; Sougata Purakayastha (Tara Muzik) 
* Script &mdash; Pradipta Bhattacharyya 
* Director of Photography &mdash; Subhadeep Dey 
* Editor &mdash; Pradipta Bhattacharyya 
* Sound &mdash; Sukanta Majumdar 
* Unit Productions Manager &mdash; Atanu Mandal

==Festivals/Screenings ==
* International Social Communication Cinema Conference, Kolkata, India, February 2007
* Banga Amar television programme, Kolkata, November 2006
* 3rd Eye Asian Film Festival, Mumbai, India, November 2007
* Sony Pix Short Film Festival, Mumbai, India, December 2007
* 5th Kalpanirjhar international short fiction film, Kolkata, India, 2007 - 12th Best Indian Film
* 4th Kolkata Short Film Festival, Kolkata (India), December 2007
* Culture unplugged online film festival, India, 2008
* SAFFBD, Dhaka, Bangladesh, August 2007, not screened yet

==Awards==
* Best Indian Short fiction - 5th Kalpanirjhar short fiction film festival, 2007, Kolkata, India
* Best film under 30 minutes - Fulmarxx short film fest version 1.0, 2008
* Best film of the festival in all categories - Fulmarxx short film fest version 1.0, 2008
* Best Editing - Fulmarxx short film fest version 1.0, 2008 organized by MICA, Ahmedabad, India.

==References==
*  


 
 
 
 