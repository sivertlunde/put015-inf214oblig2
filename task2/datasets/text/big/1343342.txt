Ben-Hur (1959 film)
 Ben-Hur}}
 
{{Infobox film
| name           = Ben-Hur
| image          = Ben hur 1959 poster.jpg
| image_size     =
| caption        = Original theatrical poster by Reynold Brown
| screenplay     = Karl Tunberg 
| based on       =   by Lew Wallace
| narrator       = Finlay Currie
| starring       = Charlton Heston Jack Hawkins Haya Harareet Stephen Boyd Hugh Griffith
| director       = William Wyler
| producer       = Sam Zimbalist
| music          = Miklós Rózsa
| cinematography = Robert L. Surtees John D. Dunning Ralph E. Winters
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews Cineplex Entertainment|Loews Inc. 
| released       =  
| runtime        = 222 minutes 
| country        = United States
| language       = English
| budget         = $15.2 million Sheldon Hall, Epics, Spectacles, and Blockbusters: A Hollywood History Wayne State University Press, 2010 p 162  
| gross          = $146.9 million (initial release)
}}
Ben-Hur is a 1959 American  . The screenplay is credited to Karl Tunberg but includes contributions from Maxwell Anderson, S. N. Behrman, Gore Vidal, and Christopher Fry.
 MGM Studios in Culver City, California. The nine-minute chariot race has become one of cinemas most famous sequences, and the film score, composed and conducted by Miklós Rózsa, is the longest ever composed for a film and was highly influential on cinema for more than 15 years.
 Best Motion Best Director Best Supporting greatest films American film and the 2nd best American epic film in the AFIs 10 Top 10. In 2004, the National Film Preservation Board selected Ben-Hur for preservation by the National Film Registry for being a "culturally, historically, or aesthetically significant" motion picture.

A remake of the film, also produced by MGM, is scheduled to be released in February 2016.   

==Plot== Esther (Haya Roman citizen Messala (Stephen Roman garrison. Rome and its imperial power, while Ben-Hur is devoted to his faith and the freedom of the Jewish people.
 
 governor of galleys and imprisons Miriam and Tirzah. By punishing a known friend and prominent citizen, he hopes to intimidate the Jewish populace. Ben-Hur swears to take revenge.
 Roman Consul Quintus Arrius Macedonian pirates. Arrius admires Ben-Hurs determination and self-discipline and offers to train him as a gladiator or chariot racing|charioteer. Ben-Hur declines the offer, declaring that God will aid him in his quest for vengeance. When the Roman fleet encounters the Macedonians, Arrius orders all the rowers except Ben-Hur to be chained to their oars. Arrius galley is rammed and sunk, but Ben-Hur unchains the other rowers, and rescues Arrius. In despair, Arrius wrongly believes the battle ended in defeat and attempts to atone in the Roman way by "falling on his sword", but Ben-Hur stops him. They are rescued, and Arrius is credited with the Roman fleets victory.
 adopts him Roman ways and becomes a champion charioteer, but still longs for his family and homeland.
  Balthasar (Finlay Currie) and an Arab, Sheik Ilderim (Hugh Griffith). The sheik has heard of Ben-Hurs prowess as a charioteer, and asks him to drive his quadriga in a race before the new Judean governor Pontius Pilate (Frank Thring). Ben-Hur declines, even after he learns that Messala will also compete.

Ben-Hur returns to his home in Jerusalem. He meets Esther, and learns her arranged marriage did not occur and that she is still in love with him. He visits Messala and demands his mother and sisters freedom. The Romans discover that Miriam and Tirzah contracted leprosy in prison, and expel them from the city. The women beg Esther to conceal their condition from Ben-Hur so that he may remember them as they were before, so she tells him that they died. It is then that he changes his mind and decides to seek vengeance on Messala by competing against him in the chariot race.

During the chariot race, Messala drives a chariot with blades on the hubs to tear apart competing vehicles; he attempts to destroy Ben-Hurs chariot but destroys his own instead. Messala is fatally injured, while Ben-Hur wins the race. Before dying, Messala tells Ben-Hur that "the race is not over" and that he can find his family "in the Valley of the Lepers, if you can recognize them." Ben-Hur visits the nearby leper colony, where (hidden from their view) he sees his mother and sister.
 patrimony and the trial of Jesus before Pontius Pilate has begun. Ben-Hur witnesses the crucifixion of Jesus, and Miriam and Tirzah are miraculously healed during the rainstorm following the crucifixion.

==Cast==
* Charlton Heston as Judah Ben-Hur Quintus Arrius Esther
* Messala
* Hugh Griffith as Sheik Ilderim
* Martha Scott as Miriam
* Cathy ODonnell as Tirzah
* Sam Jaffe as Simonides
* Finlay Currie as Balthasar and the narrator
* Frank Thring as Pontius Pilate
* Terence Longdon as Drusus
* George Relph as Tiberius Caesar
* André Morell as Sextus
Harareet is the last surviving primary cast member.

==Production==
 
 , intended to be Jerusalem]] Robert Taylor Sidney Franklin was scheduled to direct, with Marlon Brando intended for the lead.  In September 1955, Zimbalist, who continued to claim that Tunbergs script was complete, announced that a $7 million, six-to-seven month production would begin in April 1956 in either Israel or Egypt in MGMs new 65mm widescreen process.  MGM, however, suspended production in early 1956. 
 The Ten costliest film ever produced up to that time.  When adjusted for inflation, the budget of Ben Hur was approximately $ }} in constant dollars. 
 Leo the nativity scene, Wyler received permission to replace the traditional logo with one in which Leo the Lion is quiet. Schumach, Murray. "Metro Stills Leo for the First Time." The New York Times. November 26, 1959.    -->
 The Bible. The film is set for release in February 2016. 

===Development=== suspension for his decision.  Karl Tunberg was one of the last writers to work on the script, and he cut out everything in the book after the crucifixion of Jesus, omitted the sub-plot in which Ben-Hur fakes his death and raises a Jewish army to overthrow the Romans, and altered the manner in which the leperous women are healed.   Morsberger and Morsberger, p. 482.  Zimbalist was unhappy with Tunbergs script, considering it to be "pedestrian"  and "unshootable". Kaplan, p. 440. 
 hack work. Herman, p. 394.  Zimbalist showed Wyler some preliminary storyboards for the chariot race and informed him that MGM would be willing to spend up to $10 million, and as a result Wyler began to express an interest in the picture. Herman, p. 395.  MGM permitted Wyler to start casting, and in April 1957, mainstream media outlets reported that Wyler was giving screen tests to Italian leading men, such as Cesare Danova. 

Wyler did not formally agree to direct the film until September 1957,  and MGM did not announce his hiring until January 3, 1958.  Even though he still lacked a leading man, Wyler took the assignment for many reasons: He was promised a base salary of $350,000 as well as 8 percent of the gross box office (or 3 percent of the net profits, whichever was greater),  and he wanted to work in Rome again (where he had filmed Roman Holiday (1954)).  Eagen, p. 559.  His base salary was, at the time, the largest ever paid to a director for a single film.  Professional competitive reasons also played a role in his decision to direct, and Wyler later admitted that he wished to out do Cecil B. DeMille,  and make a "thinking mans" Biblical epic.  In later years, William Wyler would joke that it took a Jew to make a good film about Christ. 

===Writing===
 ]] Quo Vadis) Fred Kaplan Julian and knew a great deal about ancient Rome. Kaplan, p. 441. 
 William Morris Messala reuniting and falling out in a single scene. Vidal broke the scene in two, so that the men first reunite at the Castle Antonia and then later argue and end their friendship at Ben-Hurs home. Vidal also added small character touches to the script, such as Messalas purchase of a brooch for Tirzah and Ben-Hurs purchase of a horse for Messala.  Vidal claimed that he worked on the first half of the script (everything up to the chariot race), and scripted 10 versions of the scene where Ben-Hur confronts Messala and begs for his familys freedom. Herman, p. 400.  Giddins, p. 247. 

Vidals claim about a homoerotic subtext is hotly debated. Vidal first made the claim in an interview in the 1995 documentary film  , in a comparison of script drafts, concludes that Vidal made significant and extensive contributions to the script. 
 Screen Writers Guild.  "Ben-Hur Credit Is Urged for Fry." The New York Times. October 29, 1959. 

The final script ran 230 pages.  The screenplay differed more from the original novel than did the 1925 silent film version. Some changes made the films storyline more dramatic. Others inserted an admiration for Jewish people (who had founded the state of Israel by this time) and the more pluralistic society of 1950s America rather than the "Christian superiority" view of Wallaces novel. 

===Casting===
{{multiple image
| align     = right
| direction =vertical
| footer    =
| width     =
| image1    = Stephen Boyd in Ben Hur trailer.jpg
| width1    = 160
| caption1  =Stephen Boyd
| image2    = Haya Harareet in Ben Hur trailer.jpg
| width2    = 160
| caption2  =Haya Harareet
| image3    =Hugh Griffith in Ben Hur trailer.jpg
| width3    = 160
| caption3  =Hugh Griffith
| image4    =Jack Hawkins in Ben Hur trailer.jpg
| width4    = 160
| caption4  =Jack Hawkins
}}

MGM opened a casting office in Rome in mid-1957 to select the 50,000 people who would act in minor roles and as extras in the film, Freiman, p. 25.  and a total of 365 actors had speaking parts in the film, although only 45 of them were considered "principal" performers.  In casting, Wyler placed heavy emphasis on characterization rather than looks or acting history. Parish, Mank, and Picchiarini, p. 27.  He typically cast the Romans with British actors and the Jews with American actors to help underscore the divide between the two groups. Solomon, p. 207.   The Romans were the aristocrats in the film, and Wyler believed that American audiences would interpret British accents as patrician. 

Several actors were offered the role of Judah Ben-Hur before it was accepted by Charlton Heston. Burt Lancaster stated he turned down the role because he found the script boring  and belittling to Christianity.  Paul Newman turned it down because he said he didnt have the legs to wear a tunic.    Marlon Brando,  Rock Hudson,  Geoffrey Horne,  and Leslie Nielsen  were also offered the role, as were a number of muscular, handsome Italian actors (many of whom did not speak English).  Kirk Douglas was interested in the role, but was turned down in favor of Heston,  who was formally cast on January 22, 1958. Pryor, Thomas M. "Heston Will Star in M-G-M Ben-Hur." The New York Times. January 23, 1958.  His salary was $250,000 for 30 weeks, a prorated salary for any time over 30 weeks, and travel expenses for his family. Herman, p. 396. 

Stephen Boyd was cast as the antagonist, Messala (character)|Messala, on April 13, 1958.  William Wyler originally wanted Heston for the role, but sought another actor after he moved Heston into the role of Judah Ben-Hur.  Because both Boyd and Heston had blue eyes, Wyler had Boyd outfitted with brown contact lenses as a way of contrasting the two men.  Marie Ney was originally cast as Miriam, but was fired after two days of work because she could not cry on cue.  Pryor, Thomas M. "Frenke Signs Pact With Seven Arts." The New York Times. August 4, 1958.  Heston says that he was the one who suggested that Wyler cast Martha Scott as Miriam, and she was hired on July 17, 1958.    Cathy ODonnell was Wylers sister-in-law, and although her career was in decline, Wyler cast her as Tirzah.  

More than 30 actresses were considered for the role of Esther. Pryor, Thomas M. "Israeli Actress Cast in Ben-Hur." The New York Times. May 17, 1958.  The Israeli actress Haya Harareet, a relative newcomer to film, was cast as Esther on May 16, 1958,  after providing a 30-second silent screen test. Pratt, p. 135.  Wyler had met her at the Cannes Film Festival, where she impressed him with her conversational skills and force of personality.  Sam Jaffe was cast as Simonides on April 3, 1958, Pryor, Thomas M. "Seven Arts Group Teaming With U.A." The New York Times. April 4, 1958.  and Finlay Currie was cast as Balthasar on the same day.  Wyler had to persuade Jack Hawkins to appear in the film, because Hawkins was unwilling to act in another epic motion picture so soon after The Bridge on the River Kwai.  Hugh Griffith, who gained acclaim in the post-World War II era in Ealing Studios comedies, was cast as the comical Sheik Ilderim.  The role of Jesus was played by Claude Heater (uncredited), an American opera singer performing with the Vienna State Opera in Rome when he was asked to do a screen test for the film. 

===Cinematography===
  used (2.76:1).]] composition in depth, a visual technique in which people, props, and architecture are not merely composed horizontally but in depth of field as well. He also had a strong preference for long takes, during which his actors could move within this highly detailed space. 

The movie was filmed in a process known as " . Haines, p. 114.  70mm anamorphic camera lenses developed by the Mitchell Camera Company were manufactured to specifications submitted by MGM.  These lenses squeezed the image down 1.25 times to fit on the image area of the film stock.  Because the film could be adapted to the requirements of individual theaters, movie houses did not need to install special, expensive 70mm projection equipment.  Six of the 70mm lenses, each worth $100,000, were shipped to Rome for use by the production. Cyrino, p. 74.  Freiman, p. 31.  

===Principal photography===
 

 ) would begin on March 1, 1958, and that 200 camels and 2,500 horses had already been procured for the studios use there. Pryor, Thomas M. "Hollywoods Varied Vistas." The New York Times. January 12, 1958.  The production was then scheduled to move to Rome on April 1, where Andrew Marton had been hired as second unit director and 72 horses were being trained for the chariot race sequence.  However, the Libyan government canceled the productions film permit for religious reasons on March 11, 1958, just a week before filming was to have begun.    It is unclear whether any second unit filming took place in Israel. A June 8, 1958, reported in The New York Times said second unit director Andrew Martin had roamed "up and down the countryside" filming footage. Schiffer, Robert L. "Israel Screen Scene." The New York Times. June 8, 1958.  However, the American Film Institute claims the filming permit was revoked in Israel for religious reasons as well (although when is not clear), and no footage from the planned location shooting near Jerusalem appeared in the film. 
 vitamin B complex injection to anyone who requested it (shots which Wyler and his family later suspected may have contained amphetamines). Herman, p. 403.  To speed things up, Wyler often kept principal actors on stand-by, in full costume and make-up, so that he could shoot pick-up scenes if the first unit slowed down. Actresses Martha Scott and Cathy ODonnell spent almost the entire month of November 1958 in full leprosy make-up and costumes so that Wyler could shoot "leper scenes" when other shots didnt go well.  Wyler was unhappy with Hestons performances, feeling they did not make Judah Ben-Hur a plausible character, and Heston had to reshoot "Im a Jew" 16 times. Herman, p. 404.  Shooting took nine months, which included three months for the chariot race scene alone. Solomon, p. 213.  Principal photography ended on January 7, 1959, with filming of the crucifixion scene, which took four days to shoot. Eagan, p. 560.  

===Production design===
 
Italy was MGMs top choice for hosting the production. However, a number of countries—including France, Mexico, Spain, and the United Kingdom—were also considered. Freiman, p. 26.  Cinecittà Studios, a very large motion picture production facility constructed in 1937 on the outskirts of Rome, was identified early on as the primary shooting location. Hawkins, Robert F. "Viewed on the Bustling Italian Film Scene." The New York Times. February 16, 1958.  Zimbalist hired Wylers long-term production supervisor, Henry Henigson, to oversee the film, and art directors William A. Horning and Edward Carfagno created the overall look of the film, relying on the more than five years of research which had already been completed for the production. Freiman, p. 29.   A skeleton crew of studio technicians arrived in the summer of 1956 to begin preparing the Cinecittà soundstages and back lot.  The largest Cinecittà soundstage was not used for filming, but was used as a vast costume warehouse. 

  camera dollies.  A workshop employing 200 artists and workmen provided the hundreds of friezes and statues needed.  The mountain village of Arcinazzo Romano,    from Rome, served as a stand-in for the town of Nazareth.  Beaches near Anzio were also used,  and caves just south of the city served as the leper colony. Herman, p. 409.   Some additional desert panoramas were shot in Arizona, and some close-up inserts taken at the MGM Studios, with the final images photographed on February 3, 1958. 

 
 MGM Studios process shots traveling mattes. 

One of the most lavish sets was the villa of Quintus Arrius, which included 45 working fountains and   of pipes. Freiman, p. 29.  Wealthy citizens and nobles of Rome, who wanted to portray their ancient selves, acted as extras in the villa scenes.   To recreate the ancient city streets of Jerusalem, a vast set covering   was built,  which included a   high  . January 11, 1959. 

Dismantling the sets cost $125,000.  Almost all the filmmaking equipment was turned over to the Italian government, which sold and exported it.  MGM turned title to the artificial lake over to Cinecittà.  MGM retained control over the costumes and the artificial lake background, which went back to the United States.  The chariots were also returned to the U.S., where they were used as promotional props.  The life-size galleys and pirate ships were dismantled to prevent them from being used by competing studios.  Some of the horses were adopted by the men who trained them, while others were sold.  Many of the camels, donkeys, and other exotic animals were sold to circuses and zoos in Europe. 

===Editing=== John D. Gone With The Ten Commandments. 

===Musical score===
The film score was composed and conducted by Miklós Rózsa, who scored most of MGMs epics, Herman, p. 411.  although Zimbalist had previously commissioned and then set aside a score from Sir  s for the main characters except for a phrase (5-#4-2-3-1) in the Lydian mode whenever Jesus Christ is seen or alluded to. This Lydian phrase is also fully orchestrated triumphantly in the closing scene of the movie. 

Rózsa won his third Academy Award for his score.  , it was the only musical score in the ancient and medieval epic genre of film to win an Oscar.  Like most film musical soundtracks, it was issued as an album for the public to enjoy as a distinct piece of music. The score was so lengthy that it had to be released in 1959 on three LP records, although a one-LP version with Carlo Savina conducting the Symphony Orchestra of Rome was also issued. In addition, to provide a more "listenable" album, Rózsa arranged his score into a "Ben-Hur Suite", which was released on Lion Records (an MGM subsidiary which issued low-priced records) in 1959.   This made the Ben-Hur film musical score the first to be released not only in its entirety but also as a separate album. 
 Raiders of National Philharmonic Sony Music CD set in 1991.    In 2012, Film Score Monthly and WaterTower Music issued a limited edition five-CD set of music from the film. 

==Chariot race sequence==
 
 
The chariot race in Ben-Hur was directed by Andrew Marton and Yakima Canutt,  filmmakers who often acted as second unit directors on other peoples films. Each man had an assistant director, who shot additional footage.  Among these were Sergio Leone,  who was senior assistant director in the second unit and responsible for retakes.  William Wyler shot the "pageantry" sequence that occurs before the race, scenes of the jubilant crowd, and the victory scenes after the race concludes.  The "pageantry" sequence before the race begins is a shot-by-shot remake of the same sequence from the 1925 silent film version. Brownlow, p. 413.  Wyler added the parade around the track because he knew that the chariot race would be primarily composed of close-up and medium shots.  To impress the audience with the grandeur of the arena, Wyler added the parade in formation (even though it was not historically accurate). Herman, p. 402. 

===Set design===
The chariot arena was modeled on a historic  s created the illusion of upper stories of the grandstands and the background mountains. Solomon, p. 210.  More than   of sand were brought in from beaches on the Mediterranean to cover the track.   Other elements of the circus were also historically accurate. Imperial Roman racecourses featured a raised   high spina (the center section), metae (columnar goalposts at each end of the spina), dolphin-shaped lap counters, and carceres (the columned building in the rear which housed the cells where horses waited prior to the race).  Herman, p. 398.  The four statues atop the spina were   high. Hudgins, Morgan. "Ben-Hur Rides Again." The New York Times. August 10, 1958.  A chariot track identical in size was constructed next to the set and used to train the horses and lay out camera shots. 

===Preparation===
 , were used for chariot teams in Ben-Hur.]]
Planning for the chariot race took nearly a year to complete.  Seventy eight horses were bought and imported from Yugoslavia and Sicily in November 1957, exercised into peak physical condition, and trained by Hollywood animal handler Glenn Randall to pull the quadriga (a Roman Empire chariot drawn by four horses abreast). Freiman, p. 27.   Andalusian horses played Ben-Hurs Arabians, while the others in the chariot race were primarily Lipizzans.  A veterinarian, a harness maker, and 20 stable boys were employed to care for the horses and ensure they were outfitted for racing each day.  The firm of Danesi Brothers Freiman, p. 28.  built 18 chariots,  nine of which were used for practice,  each weighing  .  Principal cast members, stand-ins, and stunt people made 100 practice laps of the arena in preparation for shooting. 

Heston and Boyd both had to learn how to drive a chariot. Heston, an experienced horseman, took daily three-hour lessons in chariot driving after he arrived in Rome and picked up the skill quickly.    Solomon, p. 129.  Heston was outfitted with special contact lenses to prevent the grit kicked up during the race from injuring his eyes.  For the other charioteers, six actors with extensive experience with horses were flown in from Hollywood, including Giuseppe Tosi, who had once been a bodyguard for Victor Emmanuel III of Italy. 

===Filming===
The chariot scene took five weeks (spread over three months) to film at a total cost of $1 million  and required more than   of racing to complete.  Marton and Canutt filmed the entire chariot sequence with stunt doubles in long shot, edited the footage together, and showed the footage to Zimbalist, Wyler, and Heston to show them what the race should look like and to indicate where close-up shots with Heston and Boyd should go. Herman, p. 405  Seven thousand extras were hired to cheer in the stands.    Economic conditions in Italy were poor at the time, and as shooting for the chariot scene wound down only 1,500 extras were needed on any given day. On June 6, more than 3,000 people seeking work were turned away. The crowd rioted, throwing stones and assaulting the sets gates until police arrived and dispersed them.  Dynamite charges were used to show the chariot wheels and axles splintering from the effects of Messalas barbed-wheel attacks.  Three lifelike dummies were placed at key points in the race to give the appearance of men being run over by chariots. Didinger, p. 157 

The cameras used during the chariot race also presented problems. The 70mm lenses had a minimum focal length of  , and the camera was mounted on a small Italian-made car so the camera crew could keep in front of the chariots. The horses, however, accelerated down the   straightaway much faster than the car could, and the long focal length left Marton and Canutt with too little time to get their shots. The production company purchased a more powerful American car, but the horses were still too fast, and even with a head start, the filmmakers only had a few more seconds of shot time. As filming progressed, vast amounts of footage were shot for this sequence. The ratio of footage shot to footage used was 263:1,  one of the highest ratios ever for a film. 

One of the most notable moments in the race came from a near-fatal accident when stunt man Joe Canutt, Yakima Canutts son, was tossed into the air by accident; he incurred a minor chin injury.  Marton wanted to keep the shot, but Zimbalist felt the footage was unusable. Marton conceived the idea of showing that Ben-Hur was able to land on and cling to the front of his chariot, then scramble back into the quadriga while the horses kept going. Herman, p. 407  The long shot of Canutts accident was cut together with a close-up of Heston climbing back aboard, resulting in one of the races most memorable moments.  Boyd did all but two of his own stunts.  For the sequence where Messala is dragged beneath a chariots horses and trampled, Boyd wore steel armor under his costume and acted out the close-up shot and the shot of him on his back, attempting to climb up into the horses harness to escape injury. A dummy was used to obtain the trampling shot in this sequence. Raymond, p. 32. 

Several  . March 16, 2001. 

==Release==
 
A massive $14.7 million marketing effort helped promote Ben-Hur.  MGM established a special "Ben-Hur Research Department" which surveyed more than 2,000 high schools in 47 American cities to gauge teenage interest in the film. Doherty, p. 189.  A high school study guide was also created and distributed.  Sindlinger and Company was hired to conduct a nationwide survey to gauge the impact of the marketing campaign.  In 1959 and 1960, more than $20 million in candy; childrens tricycles in the shape of chariots; gowns; hair barrettes; items of jewelry; mens ties; bottles of perfume; "Ben-Her" and "Ben-His" towels; toy armor, helmets, and swords; umbrellas; and hardback and paperback versions of the novel (tied to the film with cover art) were sold.  

Ben-Hur premiered at Loews Cineplex Entertainment|Loews State Theatre in New York City on November 18, 1959. Present at the premiere were William Wyler, Charlton Heston, Stephen Boyd, Haya Harareet, Martha Scott, Ramon Novarro (who played Judah Ben-Hur in the 1925 silent film version), Spyros Skouras (president of the 20th Century Fox), Barney Balaban (president of Paramount Pictures), Jack Warner (president of Warner Bros.), Leonard Goldenson (president of the American Broadcasting Company), Moss Hart (playwright), Robert Kintner (an ABC Television executive), Sidney Kingsley (playwright), and Adolph Zukor (founder of Paramount Pictures). 

===Box office=== theater rentals (the distributors share of the box office), generating approximately $74.7 million in box office sales. Outside of North America, it earned $32.5 in rentals (about $72.2 million at the box office) for a worldwide total of $66.1 million in rental earnings, roughly equivalent to $146.9 million in box office receipts. Block and Wilson, p. 324.  It was the fastest-grossing film  as well as the highest grossing film of 1959,  in the process becoming the second-highest grossing film of all-time behind Gone with the Wind.   

Ben-Hur saved MGM from financial disaster, Malone, p. 23.  making a profit of $20,409,000 on its initial release,    and another $10.1 million in profits when re-released in 1969.  By 1989, Ben-Hur had earned $90 million in worldwide theatrical rentals. 

===Critical reception===
 
Ben-Hur received overwhelmingly positive reviews upon its release. Wreszin and Macdonald, p. 13.   . November 19, 1959.  While praising the acting and William Wylers "close-to" direction, he also had high praise for the chariot race:  "There has seldom been anything in movies to compare with this pictures chariot race. It is a stunning complex of mighty setting, thrilling action by horses and men, panoramic observation and overwhelming use of dramatic sound."  Jack Gaver, writing for United Press International, also had praise for the acting, calling it full of "genuine warmth and fervor and finely acted intimate scenes".  Philip K. Scheuer of the Los Angeles Times called it "magnificent, inspiring, awesome, enthralling, and all the other adjectives you have been reading about it." Scheuer, Philip K. "Magnificent Ben-Hur Inspiring in Premiere." Los Angeles Times. November 25, 1959.  He also called the editing "generally expert" although at times abrupt.  Ronald Holloway, writing for Variety (magazine)|Variety, called Ben-Hur "a majestic achievement, representing a superb blending of the motion picture arts by master craftsmen," and concluded that "Gone With the Wind, Metros own champion all-time top grosser, will eventually have to take a back seat." Holloway, Ronald. "Ben-Hur." Variety. November 17, 1959.  The chariot race "will probably be preserved in film archives as the finest example of the use of the motion picture camera to record an action sequence. The race, directed by Andrew Marton and Yakima Canutt, represents some 40 minutes of the most hair-raising excitement that film audiences have ever witnessed." 
 Time Out, was equally dismissive, calling the film a "four-hour Sunday school lesson".  Many French and American film critics who believed in the auteur theory of filmmaking saw the film as confirmation of their belief that William Wyler was "merely a commercial craftsman" rather than a serious artist. 

===Accolades===

{| class="infobox" style="width: 23em; font-size: 85%;"
|-  style="background:#ccc; text-align:center;"
! colspan="2" | Academy Awards
|- Best Picture, posthumous award) 
|- Best Director,  William Wyler
|- Best Actor in a Leading Role, Charlton Heston
|- Best Actor in a Supporting Role, Hugh Griffith
|- Best Art Edward C. posthumous award)  (art direction); Hugh Hunt (set decoration)
|- Best Cinematography, Color, Robert L. Surtees
|- Best Costume Design, Color, Elizabeth Haffenden
|- Best Special Robert MacDonald and Milo Lory
|- Best Film John D. Dunning and Ralph E. Winters
|- Best Music, Scoring of a Dramatic or Comedy Picture,  Miklós Rózsa
|- Best Sound Recording, Franklin Milton, MGM Studio Sound Department
|-  style="background:#ccc; text-align:center;"
! colspan="2" | Golden Globe Awards
|-

! colspan="2" | BAFTA Awards
|- Best Motion Picture – Drama
|- Best Director, William Wyler
|- Best Supporting Actor – Motion Picture, Stephen Boyd
|} Best Adapted Room at the Top), and most observers attributed this to the controversy over the writing credit.  Herman, p. 412  MGM and Panavision shared a special technical Oscar in March 1960 for developing the Camera 65 photographic process. 
 Best Motion Best Director, Best Supporting Best Performance Directors Guild of America Award for Outstanding Directorial Achievement in a Motion Picture for William Wylers masterful direction. 
 100 Movies, 100 Thrills, Film Scores, 100 Cheers and #2 on the AFIs 10 Top 10 Epic film lists.  Judah Ben-Hur was also nominated as a hero and Messala nominated as a villain in the  AFIs 100 Years...100 Heroes and Villains list. In 2004, the National Film Preservation Board selected Ben-Hur for preservation by the National Film Registry for being a "culturally, historically, or aesthetically significant" motion picture. 

===Broadcast and home video releases=== The Wizard of Oz was still drawing huge TV ratings on its annual telecasts at the time.  Ben-Hur was repeated on CBS at least once more before being sold to local television stations.  
 the 1925 silent version of Ben-Hur.  A boxed "Deluxe Edition", issued in the U.S. in 2002, included postcard-sized reprints of lobby cards, postcard-sized black-and-white stills with machine-reproduced autographs of cast members, a matte-framed color image from the film with a 35mm film frame mounted below it, and a   reproduction film poster. 

In 2011, Warner Home Video released a 50th anniversary edition on Blu-ray Disc and DVD, making it the first home video release where the film is present on its original aspect ratio.  For this release, the film was completely restored frame by frame from an 8K scan of the original 65mm negative. The restoration cost $1 million, and was one of the highest resolution restorations ever made by Warner Bros.  A new musical soundtrack-only option and six new featurettes (one of which was an hour long) were also included. 

==Remake==
 
On April 25, 2014,   was added to the cast to play the role of Ildarin, the man who teaches the slave Ben-Hur to become a champion-caliber chariot racer.   

==See also==
*List of historical drama films
*List of films set in ancient Rome
*List of films featuring slavery

==References==
;Notes
 

;Citations
 

==Bibliography==
 
* Alexander, Shana. "Will the Real Burt Please Stand Up?" Life (magazine)|Life. September 6, 1963.
* 
* 
* "An Actor to Watch." Coronet. January 1, 1959.
* 
* "Ben-Hur Rides a Chariot Again." Life. January 19, 1959.
* 
* 
* 
* 
* 
* 
* 
* 
* Cole, Clayton. "Fry, Wyler, and the Row Over Ben-Hur in Hollywood." Films and Filming. March 1959.
* Coughlan, Robert. "Lew Wallace Got Ben-Hur Going—and Hes Never Stopped." Life. November 16, 1959.
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* Feeney, F.X. "Ben-Gore: Romancing the Word With Gore Vidal." Written By. December 1997-January 1998.
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* "On the Sound Track." Billboard (magazine)|Billboard. July 20, 1959.
* 
* 
* 
* 
* 
* 
* 
* 
*  
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* Vidal, Gore. "How I Survived the Fifties." The New Yorker. October 2, 1995.
* 
* 
* 
 

==External links==
 
 
*  
*  
*  

 
 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 