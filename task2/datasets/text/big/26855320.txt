The Visual Bible: Matthew
{{Infobox film
 | name     = The Visual Bible: Matthew
 | image    = The Visual Bible Matthew.jpg
 | caption  = The cover of the Mandarin version
 | director = Regardt van den Bergh
 | writer   = Johann Potgieter
 | Producer   = Chuck Bush 
 | narrator = Richard Kiley
 | starring = Bruce Marchiano
 | distributor = Visual Bible International
 | released =  
 | runtime  = 265 minutes
 | country  = South Africa Morocco United States
 | language = English
 | budget   = 
}} 1993 motion picture portraying the life of Jesus as it is found in the Gospel of Matthew. The complete Gospel is presented word-for-word based on the New International Version of the Bible.  It was directed by South African film maker Regardt van den Bergh and stars veteran actor Richard Kiley in the role of Saint Matthew|St. Matthew (who narrates the movie), newcomer Bruce Marchiano as Jesus and Gerrit Schoonhoven as Peter.   Marchiano portrays Jesus as a joyous, earthy, personal man with a sense of humour.  

The film has been dubbed into various foreign languages including Spanish, Cantonese and Mandarin.

==Plot==
In Palestine during the Roman Empire, Jesus Christ of Nazareth travels around the country with his disciples preaching to the people about God and salvation of their souls. He claims to be the son of God and the Christ. He is arrested by the Romans and crucified. He rises from the dead after three days.

==Cast== Old Matthew
* Bruce Marchiano as Jesus Christ
* Joanna Weinberg as Mary (mother of Jesus) Joseph
* Marcel van Heerden as John the Baptist Peter
* Andrew
* Charlton George as James, son of Zebedee John
* Phillip
* Bartholomew
* Thomas
* Young Matthew
* Tony Joubert as James, son of Alphaeus Thaddaeus
* Darryl Fuchs as Simon the Zealot
* Dawid Minnaar as Judas Iscariot
* Gordon van Rooyen as Caiaphas
* Brian OShaughnessy as Pontius Pilate
* Patrick Mynhardt as Herod the Great
* Chris Truter as Herod Antipas
* Pippa Duffy as Mary Magdalene
* Dominique Newman as the Young Woman Pharisee
* Dawie Maritz
* DeWet Van Rooyen
* Denise Newman
* Goliath Davids
* Keith Grenville
* Maria Zak and Sasha Zak	 as the Children of Babylon
==See also==
* 
* 
*Visual Bible
== References ==
 

== External links ==
*  
*  
*  


 
 
 
 
 
 
 
 
 
 
 