Aerial Anarchists
{{Infobox film
| name = Aerial Anarchists
| director = 
| producer = Charles Urban
| distributor = Kineto Film
| released =  
| runtime = 15 minutes
| country = United Kingdom
| language = Silent film English intertitles
}}
Aerial Anarchists is a 1911 British silent science fiction film directed by Walter R. Booth.

It is the third and final film in Booths science fiction series seeking to present a picture of futuristic aerial warfare. It followed on from Aerial Torpedo and Aerial Submarine and is the first real science fiction series made in the United Kingdom. The story focuses on an attack against London by a fleet of airships from an unknown country.

It has been suggested by sources including www.silentsf.com that this film is based upon the E. Douglas Fawcett novel Hartmann the Anarchist

==Plot==
There is currently no known surviving footage of this film and all information is based upon the original catalog synopsis. The film contains scenes of a bombing and its aftermath throughout London and features prominently the bombing of St. Pauls Cathedral and a railway disaster in which a train is seen to leap into a chasm.

==References==
* Wingrove, David. Science Fiction Film Source Book (Longman Group Limited, 1985)
*  
*  
*  

== External links ==
*  

 
 
 
 
 
 jkfsiadfygdua

 