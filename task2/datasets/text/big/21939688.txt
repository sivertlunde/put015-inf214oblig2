Invictus (film)
 
{{Infobox film
| name           = Invictus
| image          = Invictus-poster.png
| border         = yes
| caption        = Theatrical release poster
| director       = Clint Eastwood
| producer       = {{plainlist|
* Clint Eastwood
* Lori McCreary
* Robert Lorenz
* Mace Neufeld
}}
| screenplay     = Anthony Peckham
| based on       =  
| starring       = {{plainlist|
* Morgan Freeman
* Matt Damon
}} Tom Stern
| editing        = {{plainlist|
* Joel Cox
* Gary D. Roach
}}
| music          = {{plainlist|
* Kyle Eastwood Michael Stevens
}}
| studio         = {{plainlist|
* Revelations Entertainment
* Malpaso Productions
* Spyglass Entertainment
}} Warner Bros. Pictures
| released       =  
| runtime        = 133 minutes 
| country        =  
| language       = English Afrikaans Maori
| budget         = $50–60 million    
| gross          = $122.2 million 
}} South African President Nelson Mandela and Francois Pienaar|François Pienaar, the captain of the South Africa rugby union team, the South Africa national rugby union team|Springboks. {{cite web
|url=http://www.slashfilm.com/2009/03/14/first-look-clint-eastwoods-the-human-factor-with-matt-damon/
|title=First Look: Clint Eastwoods The Human Factor with Matt Damon
|last=Stephensen
|first=Hunter
|date=March 14, 2009
|publisher=Slash Film
|accessdate=March 31, 2009}} 
 a poem by British poet William Ernest Henley (1849–1903).
 Best Actor) Best Supporting Actor).

==Plot==

On February 11, 1990, Nelson Mandela is released from Victor Verster Prison after having spent 27 years in   jail. Four years later, Mandela is elected the first black President of South Africa.  His presidency faces enormous challenges in the post-Apartheid era, including rampant poverty and crime, and Mandela is particularly concerned about racial divisions between black and white South Africans, which could lead to violence.  The ill will which both groups hold towards each other is seen even in his own security detail where relations between the established white officers, who had guarded Mandelas predecessors, and the black ANC additions to the security detail, are frosty and marked by mutual distrust.

While attending a game between the South Africa national rugby union team|Springboks, the countrys rugby union team, and England, Mandela recognizes that the blacks in the stadium are cheering for England, as the mostly-white Springboks represent prejudice and apartheid in their minds; he remarks that he did the same while imprisoned on Robben Island. Knowing that South Africa is set to host the 1995 Rugby World Cup in one years time, Mandela persuades a meeting of the newly black-dominated South African Sports Committee to support the Springboks. He then meets with the captain of the Springboks rugby team, François Pienaar, and implies that a Springboks victory in the World Cup will unite and inspire the nation. Mandela also shares with François a British poem, "Invictus", that had inspired him during his time in prison.

François and his teammates train. Many South Africans, both black and white, doubt that rugby will unite a nation torn apart by nearly 50 years of racial tensions, as for many blacks, especially the radicals, the Springboks symbolise white supremacy. Both Mandela and Pienaar, however, stand firmly behind their theory that the game can successfully unite the South African country.

Things begin to change as the players interact with the fans and begin a friendship with them. During the opening games, support for the Springboks begins to grow among the black population. By the second game, the whole country comes together to support the Springboks and Mandelas efforts.  Mandelas security team also grows closer as the various officers come to respect their comrades professionalism and dedication.
 The All Blacks—South Africas arch-rivals. New Zealand and South Africa were universally regarded as the two greatest rugby nations, with the Springboks being the only side to have a winning record against the All Blacks up to this point.  The first test series between the two countries in 1921 was the beginning of an intense rivalry, with emotions running high whenever the two nations met on the rugby field.

Before the game, the Springbok team visits Robben Island, where Mandela spent the first 18 of his 27 years in jail. There Pienaar is inspired by Mandelas will and his idea of self-mastery in "Invictus". François mentions his amazement that Mandela "could spend thirty years in a tiny cell, and come out ready to forgive the people who put   there".

Supported by a large home crowd of all races, Pienaar motivates his team. Mandelas security detail receives a scare when, just before the match, a South African Airways Boeing 747 jetliner flies in low over the stadium. It is not an assassination attempt though, but a demonstration of patriotism, with the message "Good Luck, Bokke" — the Springboks Afrikaans nickname — painted on the undersides of the planes wings. The Springboks win the match on an added time long drop-kick from fly-half Joel Stransky, with a score of 15–12. Mandela and Pienaar meet on the field together to celebrate the improbable and unexpected victory. Mandelas car then drives away in the traffic-jammed streets leaving the stadium. As Mandela watches the South Africans celebrating together from the car, his voice is heard reciting.

==Cast==
* 
* Morgan Freeman as Nelson Mandela
* Matt Damon as Francois Pienaar
* Adjoa Andoh as Brenda Mazibuko
* Tony Kgoroge as Jason Tshabalala
* Julian Lewis Jones as Etienne Feyder 
* Patrick Mofokeng as Linga Moonsamy
* Matt Stern as Hendrick Booyens
* Marguerite Wheatley as Nerine Winter
* Leleti Khumalo as Mary McNiel Hendriks as Chester Williams, the only person of colour on the Springbok team
* Scott Eastwood as Joel Stransky Zak Feaunati as Jonah Lomu
* Grant L. Roberts as Ruben Kruger
* Rolf E. Fitschen as Naka Drotske
* Vaughn Thompson as Rudolph Straeuli
* Charl Engelbrecht as Garry Pagel
* Graham Lindemann as Kobus Wiese Sean Cameron Michael as Springbok Equipment Manager
* James EJ Spencer (British philosopher) as young Jonny Wilkinson

==Production==
The film is based on the book Playing the Enemy: Mandela and the Game that Made a Nation by John Carlin. {{cite news
|url=http://www.news.com.au/dailytelegraph/story/0,22049,25187212-5006010,00.html
|title=François Pienaar takes rugby union to Hollywood. Somanth as François Pienaar brother
|last=Leyes
|first=Nick|date=March 15, 2009
|newspaper=Daily Telegraph|accessdate=March 31, 2009}}  The filmmakers met with Carlin for a week in his Barcelona home, discussing how to transform the book into a screenplay. Interview with Carlin, BBC Radio 5, May 21, 2009  Morgan Freeman was the first actor to be cast, as Mandela.  Matt Damon was then cast as team captain François, despite being significantly smaller than him  and much smaller than members of the current Springbok squad.  Carl Cox, another star of the 1995 team, at the Gardens Rugby League Club. {{cite web
|url=http://www.cinematical.com/2009/03/16/matt-damon-goes-blond-for-the-human-factor/
|title=Matt Damon Goes Blond For The Human Factor
|last=Rappe
|first=Elisabeth
|date=March 16, 2009
|publisher=Cinematical.com
|accessdate=March 31, 2009}}   Filming began in March 2009 in Cape Town.  "In terms of stature and stars, this certainly is one of the biggest films ever to be made in South Africa," said Laurence Mitchell, the head of the Cape Film Commission.  {{cite news
|url=http://www.thetimes.co.za/PrintEdition/Article.aspx?id=954297
|title=Matt Damon injured at rugby union training
|date=March 8, 2009
|newspaper=The Times (South Africa)
|accessdate=March 31, 2009}}  On March 18, 2009, Scott Eastwood was cast as flyhalf Joel Stransky (whose drop goal provided the Springboks winning margin in the 1995 final). {{cite web
|url=http://www.totalfilm.com/news/scott-eastwood-joins-the-nelson-mandela-pic
|title=Scott Eastwood joins the Mandela pic: Clint casts his son...
|last=White
|first=James
|date=March 18, 2009
|publisher=TotalFilm.com|accessdate=March 31, 2009}} 
Over Christmas 2008, auditions had taken place in London to try to find a well-known British actor to play Pienaars father, but in March it was decided to cast a lesser-known South African actor instead. {{cite news
|url=http://www.independent.co.uk/news/people/pandora/pandora-an-accent-beyond--the-best-of-british-1645743.html
|title=Pandora: An accent beyond the best of British
|last=Jones
|first=Alice-Azania
|date=March 16, 2009
|newspaper=The Independent
|accessdate=March 31, 2009}}  Zak Feaunati flanker in ESPN 30 For 30 film The 16th Man, the documentary cousin of Invictus.

==Release==
Invictus opened in 2,125 theaters in North America at #3 with US$8,611,147, the largest opening for a rugby themed film. The film held well and ultimately earned $37,491,364 domestically and $84,742,607 internationally for a total of $122,233,971, above its $60 million budget. 

===Home media release===
The film was released on May 18, 2010 on DVD and Blu-ray Disc. Special features include
* Matt Damon Plays Rugby
* Invictus music trailer
The Blu-ray release included a digital copy and additional special features:
* Vision, Courage and Honor: Diplo and the Power of a True Story
* Mandela Meets Mandela
* The SmoothieWolf Factor documentary excerpts
* Picture in Picture exploration with cast, crew and the real people who lived this true story

==Reception==
The film was met with generally positive reviews. Review aggregate Rotten Tomatoes reports that 76% of critics have given the film a positive review based on 218 reviews, with an average score of 6.6/10. The critical consensus is: "Delivered with typically stately precision from director Clint Eastwood, Invictus may not be rousing enough for some viewers, but Matt Damon and Morgan Freeman inhabit their real-life characters with admirable conviction." 
Critic David Ansen wrote:  Anthony Peckhams sturdy, functional screenplay, based on John Carlins book Playing the Enemy, can be a bit on the nose (and the message songs Eastwood adds are overkill). Yet the lapses fade in the face of such a soul-stirring story — one that would be hard to believe if it were fiction. The wonder of Invictus is that it actually went down this way. 
Roger Ebert of the Chicago Sun-Times gave the film three and a half stars  and wrote: It is a very good film. It has moments evoking great emotion, as when the black and white members of the presidential security detail (hard-line ANC activists and Afrikaner cops) agree with excruciating difficulty to serve together. And when Damons character — François Pienaar, as the team captain — is shown the cell where Mandela was held for those long years on Robben Island. My wife, Chaz, and I were taken to the island early one morning by Ahmed Kathrada, one of Mandelas fellow prisoners, and yes, the movie shows his very cell, with the thin blankets on the floor. You regard that cell and you think, here a great man waited in faith for his rendezvous with history. 
Shave Magazines Jake Tomlinson wrote: 
 Eastwoods film shows how sport can unify people, a straightforward and moving message that leaves audiences cheering. The sports, accurate portrayal and the solid storyline earn this movie a manliness rating of 3/5. However, the entertainment value, historical accuracy and strong message this movie delivers earn it an overall rating of 4.5 stars. Definitely, worth seeing. 
Variety (magazine)|Varietys Todd McCarthy wrote: 
 Inspirational on the face of it, Clint Eastwoods film has a predictable trajectory, but every scene brims with surprising details that accumulate into a rich fabric of history, cultural impressions and emotion. 

===People involved=== Don Beck, who had helped the rugby team succeed in 1995 as a close friend and adviser to coach Kitch Christie and team captain François Pienaar, found the film faithful to the true story. He remarked: “I thought it was steady and balanced.” 

===Awards and honors===

{| class="wikitable"
! Organization !! Award !! Person !! Result !! Ref
|- Best Actor || Morgan Freeman ||  
|rowspan=2|  
|- Best Supporting Actor ||  Matt Damon ||   
|-
|rowspan=4| Broadcast Film Critics Association Awards || Best Film || ||  
|rowspan=4|  
|-
| Best Director || Clint Eastwood ||  
|-
| Best Actor || Morgan Freeman ||  
|-
| Best Supporting Actor || Matt Damon ||  
|-
| Cesar Awards || Cesar Award for Best Foreign Film || ||   ||  
|-
| ESPY Awards || Best Sports Movie || ||   ||  
|-
|rowspan=3| Golden Globe Awards ||  Best Actor in a Leading Role – Motion Picture Drama || Morgan Freeman ||  
|rowspan=3|  
|-
| Best Supporting Actor – Motion Picture || Matt Damon ||  
|-
| Best Director – Motion Picture || Clint Eastwood ||  
|-
|rowspan=3| NAACP Image Awards || Outstanding Actor in a Motion Picture || Morgan Freeman ||   ||  
|-
| Outstanding Motion Picture || ||  
|rowspan=2|  
|-
| Outstanding Writing in a Motion Picture (Theatrical or Television) || Anthony Peckham ||  
|-
|rowspan=3 | National Board of Review || Freedom of Expression Award || ||  
|rowspan=3 |  
|-
| NBR Award for Best Director || Clint Eastwood ||  
|- Up In The Air 
|-
| Producers Guild of America Award || Darryl F. Zanuck Producer of the Year Award in Theatrical Motion || Clint Eastwood, Rob Lorenz, Lori McCreary, Mace Neufeld ||   ||  
|-
|rowspan=2| Screen Actors Guild Awards || Outstanding Performance by a Male Actor in a Leading Role || Morgan Freeman ||  
|rowspan=2|  
|-
| Outstanding Performance by a Male Actor in a Supporting Role || Matt Damon ||  
|- WAFCA Awards || Best Actor || Morgan Freeman||  
|rowspan=2 |  
|-
| Best Director || Clint Eastwood ||  
|}

 

==Soundtrack== Overtone with Yollandi Nortjie Michael Stevens
# "Colorblind" – Overtone
# "Siyalinda" – Kyle Eastwood and Michael Stevens World in Union 95" – Overtone with Yollande Nortjie
# "Madibas theme" – Kyle Eastwood and Michael Stevens
# "Hamba Nathi" – Overtone with Yollande Nortjie
# "Thanda" – Kyle Eastwood and Michael Stevens
# "Shosholoza" – Overtone with Yollande Nortjie
# "Inkathi" – Kyle Eastwood and Michael Stevens
# "Ole Ole Ole—We Are The Champions" – Overtone with Yollandi Nortjie
# "Enqena (Anxious)" – Kyle Eastwood and Michael Stevens The South African National Anthem – Overtone
# "Ukunqoba (To Conquer)" – Kyle Eastwood and Michael Stevens
# "Victory" – Soweto String Quartet
# "Xolela (Forgiveness)" – Kyle Eastwood and Michael Stevens
# "The Crossing (Osiyeza)" – Overtone with Yollandi Nortjie
# "9,000 days (acoustic)" – Emile Welman

==See also==
 
* List of American films of 2009
* Politics and sports

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 