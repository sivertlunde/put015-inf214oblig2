Riverboat Rhythm
{{Infobox film
| name           = Riverboat Rhythm
| image          = Riverboat Rhythm poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Leslie Goodwins
| producer       = Nat Holt 
| screenplay     = Charles E. Roberts 
| story          = Robert Faber 
| starring       = Leon Errol Glen Vernon Walter Catlett Marc Cramer Jonathan Hale
| music          = Roy Webb
| cinematography = Robert De Grasse
| editing        = Marvin Coil 
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Riverboat Rhythm is a 1946 American comedy film directed by Leslie Goodwins and written by Charles E. Roberts. The film stars Leon Errol, Glen Vernon, Walter Catlett, Marc Cramer and Jonathan Hale. The film was released on February 13, 1946, by RKO Pictures.   

==Plot==
 

== Cast ==
*Leon Errol as Matt Lindsay
*Glen Vernon as John Beeler 
*Walter Catlett as Colonel Jeffrey Smitty Witherspoon
*Marc Cramer as Lionel Beeler
*Jonathan Hale as Colonel Edward Beeler
*Joan Newton as Midge Lindsey
*Emory Parnell as Sheriff Martin
*Harry Harvey, Sr. as Ezra Beeler
*Florence Lake as Penelope Beeler Witherspoon
*Dorothy Vaughan as Belle Crowley
*Ben Carter as Benjamin 
*Mantan Moreland as Mantan
*Frankie Carle as Band Leader Frankie Carle 
*Frankie Carles Orchestra as Orchestra 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 