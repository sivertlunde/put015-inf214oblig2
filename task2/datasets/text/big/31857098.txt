Leaving Fear Behind
  communist Chinese repression of Tibet. It was premiered in 2008 in the year when the 2008 Summer Olympics took place in Beijing, China.

== Production == Tibetan monk, conceived of a documentary interviewing ordinary Tibetan people on their views of the Dalai Lama and the Chinese government in the year leading up to the 2008 Beijing Olympics.    The documentary was to be called Leaving Fear Behind. The pair coordinated their efforts with a Dhondup Wangchens cousin Gyaljong Tsetrin, who remained in Switzerland.    In preparation for likely reprisals by the Chinese government, Dhondup Wangchen moved his wife, Llamo Tso, and their four children to Dharamsala, Himachal Pradesh|Dharamsala, India.       and the Tibetan Center for Human Rights and Democracy 
 riots erupted and began to spread through Tibetan-majority areas of China.  As part of the government response that followed, both Jigme Gyatso and Dhondup Wangchen were detained on March 28 in Tong De, Qinghai Province.   

== Reception ==
The 25-minute documentary resulting from Dhondup Wangchen and Jigme Gyatsos footage was described by The New York Times as "an unadorned indictment of the Chinese government".  The film was compiled from 40 hours of interview footage  shot by a single camera.  The documentary premiered on the opening day of the Olympics and was clandestinely screened for foreign reporters in Beijing. 

On 9 March 2012, the 53rd anniversary of the 1959 Tibetan uprising, a coalition of human rights and Tibetan activist groups calling for Dhondup Wangchens release held a rally in New York Citys Times Square; excerpts from Leaving Fear Behind were shown there on a twelve-foot video screen beneath the Xinhua Jumbotron.   

== See also ==
* 2008 Lhasa violence

==References==
 

==External links==
* 

 
 
 
 
 
 