Marutha Nattu Veeran
{{Infobox film
| name           = Marutha Nattu Veeran
| image          =
| image_size     =
| caption        =
| director       = T. R. Raghunath
| producer       = B. Radhakrishna
| writer         = 
| screenplay     =  Jamuna P. S. Veerappa Pasupuleti Kannamba|P. Kannamba
| music          = S. V. Venkatraman
| cinematography = R. Sampath
| editing        = T. R. Raghunath P. K. Krishnan A. P. Jagadish
| studio         =  Sri Ganesh Prasad Movies
| distributor    =  Sri Ganesh Prasad Movies
| released       =  
| country        = India Tamil
}}
 1961 Cinema Indian Tamil Tamil film, directed by T. R. Raghunath and produced by B. Radhakrishna. The film stars Sivaji Ganesan, Jamuna (actress)|Jamuna, P. S. Veerappa and Kannamba in lead roles. The film had musical score by S. V. Venkatraman.  

==Plot==
Marutha Nattu Veeran is a swashbuckling-adventure that stars Sivaji Ganesan in the lead role. Jeevagan (Sivaji Ganesan), a brave young man is appointed as the high guard to protect princess Ratna and soon they both fall in love. Meanwhile, the Kings minister, Veera Kesha (P.S.Veerappa) is secretly plotting with the Sultan, a known enemy of the throne. Veera Kesha frames Jeevagan as a traitor and turns the Kingdom against him. What sinister plan does the Sultan hold? Can Jeevagan prove his innocence and save his Kingdom?

==Cast==
 
*Sivaji Ganesan Jamuna
*P. S. Veerappa
*Pasupuleti Kannamba|P. Kannamba
*Sriram
*A. Karunanithi
*Rama Rao
*K. Natarajan
*M. R. Santhanam
*P. S. Venkatachalam
*K. R. Ramsingh
*Nandha Ram
*Kannan
*Ganapathy Bhat
*N. R. Sandhya
*M. Saroja
*K. V. Shanthi
*Jyothi
 

==Crew==
*Producer: B. Radhakrishna
*Production Company: Sri Ganesh Prasad Movies
*Director: T. R. Raghunath
*Music: S. V. Venkatraman
*Lyrics: Kannadasan & A. Maruthakasi
*Story: 
*Screenplay: 
*Dialogues: 
*Art Direction: K. Nageswara Rao
*Editing: T. R. Raghunath, P. K. Krishnan & A. P. Jagadeesh
*Choreography: P. S. Gopalakrishnan
*Cinematography: R. Sampath
*Stunt: Stunt Somu
*Audiography: A. Krishnan
*Dance: L. Vijayalakshmi

==Soundtrack== Playback singers are T. M. Soundararajan, P. B. Sreenivas, A. L. Raghavan, P. Suseela & A. G. Rathnamala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Paruvam Paartthu Arugil  || T. M. Soundararajan || || 03:46
|- 
| 2 || Vizhiyalai Mele Semmeen Pole || T. M. Soundararajan & P. Suseela || || 05:57
|- 
| 3 || Samaadhaaname Thevai  || T. M. Soundararajan || || 04:07
|- 
| 4 || Seikathoru Santhegam Kelu Kanmani || A. L. Raghavan & A. G. Rathnamala || || 03:11
|- 
| 5 || Pudhu Inbam Ondru Uruvaagi Indru || P. Suseela || || 02:41
|- 
| 6 || Arumbudhira Mutthudhira Azhagu Sirikkudhu || T. M. Soundararajan & P. Suseela || || 03:44
|- 
| 7 || Kallirukkum.... Enge Selkindaraai  || P. B. Sreenivas || || 04:22
|- 
| 8 || Aasai Kaadhal || P. Suseela || || 04:34
|-
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 