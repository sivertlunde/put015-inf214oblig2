Red Tavern
{{Infobox film
| name =   Red Tavern
| image =
| image_size =
| caption =
| director = Max Neufeld
| producer =  
| writer =  Aldo De Benedetti    Gherardo Gherardi 
| narrator =
| starring = Alida Valli   André Mattoni   Lauro Gazzolo   Oreste Bilancia
| music = Armando Fragna  
| cinematography = Alberto Fusi 
| editing = Giuseppe Fatigati     
| studio = Italcine  ICI 
| released = 8 February 1940
| runtime = 80 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Red Tavern (Italian:Taverna rossa) is a 1940 Italian comedy film directed by Max Neufeld and starring  Alida Valli, André Mattoni and Lauro Gazzolo. It was made at  Cinecittà in Rome. A young woman eventually marries a count after a series of misunderstandings. 

==Cast==
*  Alida Valli as Susanna Sormani  
* André Mattoni as Il conte Carlo Torresi  
* Lauro Gazzolo as Il marchese Domenico Torresi, suo zio  
* Oreste Bilancia as Il signor Sormanni  
* Lilia Dale as Ninon  
* Umberto Sacripante as Il ladruncolo  
* Aristide Garbini as Cesarone, il maggiordomo  
* Luigi Erminio DOlivo as Il tenore Farelli  
* Anna Doré as Floriana  
* Livia Minelli as La commesa del negozio di dischi 
* Alfredo Martinelli as Un cliente del negozio di dischi 
* Rita Durnova as Lisetta 
* Paola Doria as Francesca  
* Ernesto Torrini as Il cameriere del locale 
* Armida Bonocore as Maria  

== References ==
 

== Bibliography ==
* Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013.

== External links ==
*  

 

 
 
 
 
 
 
 

 