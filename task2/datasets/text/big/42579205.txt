Before the Winter Chill
 

{{Infobox film
| name     = Before the Winter Chill
| image    =
| director = Philippe Claudel
| producer = 
| Script =  Richard Berry
| cinematography = 
| music = 
| country = France
| language = French
| runtime = 103 minutes
| released =  
}}
Before the Winter Chill ( ) is a 2013 French drama film directed by Philippe Claudel.

==Synopsis==
Philippe Claudel and Kristin Scott-Thomas reunite in this subtle story of a man in the autumn of his life, torn between a loving wife and his dangerous attraction for a troubled, mysterious young woman.
Paul has never questioned his choices. Married right out of med school to the stunning Lucie, who set aside her own career to accommodate his brilliant one as a brain surgeon, he has been faithful, has earned the respect of his peers, raised a son and built a lovely home. Life is good.
But now, in the autumn of his life, something might threaten all that. It starts after a chance meeting with young and mysterious Lou who keeps turning up in his life.
When roses are delivered daily at his office and home, Paul presumes Lou is stalking him, yet he can’t resist the lure of an ambiguous and dangerous relationship with her.
Fascinated by her emotional frailty, her mystery and grace, Paul fails to see that he might be the target of a sinister plot, something dark and twisted that could threaten everything he’s built and everyone he loves.

== Cast ==
 
* Daniel Auteuil as Paul
* Kristin Scott Thomas as Lucie
* Leïla Bekhti as Lou Richard Berry as Gérard
* Laure Killing as Mathilde
* Jérôme Varanfrain as Victor
* Vicky Krieps as Caroline
* Anne Metzler as Zoé Gassard
* Annette Schlechter as Mme Malek
* Laurent Claret as Denis
* Lucie Debay as lamie de Lou   
* Jean-Louis Sbille as Fred
* Yvonne Gradelet

==References==
 

== External links ==
 
*  

 
 
 
 
 
 


 