I Need You (film)
{{Infobox film
| name = I Need You
| image =
| image_size =
| caption =
| director = Hans Schweikart
| producer = Gerhard Staab 
| writer =  Peter Francke   Hans Schweikart
| narrator =
| starring = Marianne Hoppe   Willy Birgel   Paul Dahlke   Fita Benkhoff
| music = Oskar Wagner    Franz Koch  
| editing =  Ludolf Grisebach      
| studio = Bavaria Film
| distributor = Deutsche Filmvertriebs 
| released = 12 May 1944
| runtime = 82 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} conductor and his actress wife enjoy a stormy relationship due to their clashing working commitments.

==Cast==
*    Marianne Hoppe as Julia Bach  
* Willy Birgel as Prof. Paulus Allmann  
* Paul Dahlke as Direktor Heinrich Scholz  
* Fita Benkhoff as Hedi Scholz  
* Ernst Fritz Fürbringer as Dr. Max Hoffmann  
* Erna Sellmer as Julias Hausmädchen Emilie 
* Joseph Offenbach as Dr. Wilberg  

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 