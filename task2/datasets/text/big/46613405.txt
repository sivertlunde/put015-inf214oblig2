Actress (film)
Actress is a 2014 American documentary film directed, edited and photographed by Robert Greene. The film was produced by Douglas Tirola and Susan Bedusa, and is a 4th Row Films and Prewar Cinema production. 

==Synopsis==

Actress is a documentary about Brandy Burre, most known for her recurring role as Theresa D’Agostino on HBO’s The Wire|The Wire   as she attempts to return to her acting career after abandoning it to concentrate on raising a family. 


Set in suburban Beacon, NY, Burre struggles with duties and relationships in her domestic life. During the film, she pursues re-entering her former profession by meeting old contacts in the industry and rebuilding herself while juggling motherhood and her personal life.


Actress has been recognized for its use of poetic, more directed techniques and mise-en-scene, a tactic that is something of an anomaly in documentaries.   Poetic aspects were used mostly to represent Burre’s crumbling emotional state. Greene has said that there was a performance to all of Burre’s behavior.   His use of “composed indie-film moments,” seen in the consciously lit, stage-like opening scene, slow motion shots, and collaboration with Burre, allowed her to become more than just a subject. This enabled the actress to, “explore her own authenticity,” in a way that became a very cathartic experience for her.   Seeing her dismantling personal life told in present tense, Burre performs in roles as a mother and caregiver, as well as an actress pursuing a career, and a woman in romantic turmoil with her longtime partner and father of her children. 


Greene has called Burre’s performance, “fragile and open,” while at other points, “opaque and hard,” and that her, “gestures of fragility to me are more devastating to me than watching someone totally unaware of what she’s doing. She’s authentically in pain, but she’s also demonstrating pain very consciously and dramatically. The combination of the real and the display is mind boggling to me. You don’t know where one begins and the other ends.”   

==Critical Reception==
The film premiered on April 26, 2014 at Art of the Real at Lincoln Center, New York, NY.  

It has been well-received critically, praised for blurring the line between fiction and non-fiction,   and has been called, “a story with universal appeal rendered in intimate flourishes,”   and, “beguiling, provocative and formally exciting.”  


Actress has been nominated for a Gotham Award for Best Documentary,   named, “best storytelling in a documentary feature,” at the Nantucket Film Festival.   Boston Globe’s Peter Keough called Actress a film that, “underscores the inextricability of real life and make-believe… combining artifice with cinema vérité,” and explores where the line between performance and genuine behavior meet and blur.”   

==Awards==

 CPH:DOX (2014)
 Nominated, CPH:DOX Award: Robert Greene
 Nominated, Politiken’s Audience Award: Robert Greene

 Cinema Eye Honors Awards, US (2015)
 Won, Cinema Eye Honors Award: The Unforgettables, Brandy Burre
 Nominated, Cinema Eye Honors Award: Outstanding Achievement in Direction, Robert Greene
 Nominated, Cinema Eye Honors Award: Outstanding Achievement in Editing, Robert Greene

 Gotham Awards (2014)
 Nominated, Best Documentary: Robert Greene (director/producer); Susan Bedusa (producer); Douglas Tirola (producer)

 Hot Docs Canadian International Documentary Festival (2014)
 Nominated, Best International Documentary: Robert Greene

 Indiewire Critics’ Poll (2014)
 ICP Award: Best Documentary, Robert Greene (5th Place)

==References==
 

 
 