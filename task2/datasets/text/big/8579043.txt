Hello (2008 film)
 
 
{{Infobox film
| name           = Hello
| image          = Hello2008.jpg
| image size     = 
| alt            =
| caption        = Theatrical release poster
| director       = Atul Agnihotri
| producer       = Atul Agnihotri
| screenplay     =Atul Agnihotri
| based on       =  
| narrator       = Katrina Kaif
| starring       = Sharman Joshi Sohail Khan Isha Koppikar Gul Panag Amrita Arora
| music          = Sajid (music director)|Sajid-Wajid
| cinematography = Sanjay F. Gupta
| editing        = Umesh Gupta Rajkumar Hirani
| studio         = 
| distributor    = Vinod Chopra Productions
| released       = 10 October 2008
| runtime        = 
| country        = India Hindi
| budget         =  
| gross          =  
}}
 Chetan Bhagats novel, One Night @ the Call Center. It also had cameo roles from Salman Khan and Katrina Kaif who played themselves in the film. It was released on 10 October 2008. Hello had an Average performance at Indian box-office.

==Plot==
Salman Khan is a Bollywood actor, who is on a live tour nearby Delhi, where his private jet has a crash. Luckily, he survives and in a lounge, he meets Katrina Kaif. She tells him a story, about six friends and their boss who all work together at a Mumbai-based call centre. They are instructed never to reveal their location, and speak with an American accent by a Boston-based company. There is Shyam (Sam) (Sharman Joshi), Priyanka (Gul Panag), Varun (Sohail Khan), Esha (Isha Koppikar), Radhika (Amrita Arora) and Military uncle (Sharat Saxena). Their boss, Subhash Bakshi (Dalip Tahil), attempts to further his career by plagiarising software, agrees to lay off 40% of the Indian workforce, and re-locate to Boston. One night, at a party, they all get drunk and decide to head back home since it was late. The six are on their way home, until they approach an accident, and are stuck on top of a cliff in their car, and if anybody tries to move, there would be chances that the car will lose its balance and fall off the cliff. They all remain calm, and when one of the friends tries to call someone, he finds his phone battery has run out, therefore, in rage he breaks his phone and throws it on the floor of the car. Later on, still trapped in the car, the broken phone receives a phone call; six surprised friends answer it, only to find the caller is God. God tells the six, they should keep calm, and just believe in themselves and all will be well. They do as they are told, and find that the fire brigade are here to save them. They all get saved. After the story, Khan asks the woman who she is, and she replies "if you believe in yourself, you will know the answer". She walks out of the lounge, and as Khan follows her, he seems to witness she has suddenly disappeared. He believes in himself, his jet gets fixed, and he flies back home.

==Cast==
* Sharman Joshi as Shyam (Sam)
* Sohail Khan as Varun (Vroom/Victor)
* Gul Panag as Priyanka
* Isha Koppikar as Esha (Elizabeth)
* Amrita Arora as  Radhika (Rebecca)
* Sharat Saxena as Military Uncle- Col.Arvind Tripathi
* Dalip Tahil as Bakshi
* Anusha Dandekar as Shefali Arbaaz Khan as Anuj
* Rishi Nijhawan as Ganesh
* Suresh Menon as Systems guy- Lokesh Chandaramani (Loki)
* Salman Khan as himself in a special appearance.
* Katrina Kaif as Mysterious story teller-Angel of God,in a special appearance.

==Soundtracks==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Title !! Singer(s)
|-
| "Bang Bang Bang" Wajid
|-
| "Caravan"
| Shafqat Amanat Ali
|-
| "Hello" Wajid
|-
| "Hello" (Party Mix) Wajid
|-
| "Mitwa Re" Wajid
|-
| "Rab Ka Banda" Sonu Nigam, Sunidhi Chauhan, Zubeen Garg
|-
| "Karle Baby Dance Wance" Daler Mehndi, Sunidhi Chauhan
|}

==Reception==
Hello had a fairly strong opening in the first week grossing approximately   60&nbsp;million in the first three days itself (overseas notwithstanding).  The film wasnt even a moderate success at the box office. Although it received mixed to negative reviews from critics, many citing the poorly sketched out characters as its biggest drawback, some praised its original concept. 

The film takes away a lot of criticism for modifying the original episodes that unfolds in the book. For example, the instance in the book, where Ganeshs photos are found on the net and revealed to Priyanka by Vroom, is missing from the movie, making it difficult to understand why Priyanka comes back to Shyam in the end. There are many such instances which reveal the weakness of the script. The cast also doesnt justify the original characters in the movie. Vroom, in the book is described as a lean tall guy, but is played by Sohail Khan, who is a complete contrast to it. Same applies to the characters like Esha (Isha Koppikar) and Priyanka (Gul Panag).

==References==
 

==External links==
*  

 

 
 
 
 
 