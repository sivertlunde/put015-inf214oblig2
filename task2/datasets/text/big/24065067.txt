S/Y Joy
{{Infobox film
| name           = S/Y Joy
| image          = SY Joy 1989 poster.jpg
| caption        = Swedish theatrical poster
| director       = Göran du Rées
| producer       = Anders Birkeland Börje Hansson
| writer         = Inger Alfvén (novel) Inger Alfvén (screenplay) Göran du Rées (writer)
| starring       = See below
| music          = Göran Lagerberg
| cinematography = Henrik Paersch
| editing        = Carina Hellberg Thomas Holéwa
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}

S/Y Joy ( ) is a 1989 Swedish drama film directed by Göran du Rées, made after the book by Inger Alfvén. 

==Plot==
A young couple, devastated over the loss of their child in an accident, take a vacation aboard a yacht, but the wife quickly becomes obsessed with uncovering a similar tragedy that occurred on an earlier voyage.

==Cast==
*Lena Olin as Annika Larsson
*Stellan Skarsgård as Klas Larsson
*Viveka Seldahl as Maja-Lena Skoog
*Hans Mosesson as Herbert Skoog

==Soundtrack==
*Style - "Dover-Calais" (Written by Tommy Ekman & Christer Sandelin)

==Awards== Best Actress.   

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 