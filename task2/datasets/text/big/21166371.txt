Call Me Madam (film)
 
{{Infobox film
| name           = Call Me Madam
| image          = CallMeMadam2.JPG
| caption        = Original film poster
| director       = Walter Lang
| producer       = Sol C. Siegel
| based on       =  
| screenplay     = Arthur Sheekman
| starring       = Ethel Merman Donald OConnor Vera-Ellen George Sanders Alfred Newman
| cinematography = Leon Shamroy Robert L. Simpson
| distributor    = 20th Century Fox
| released       =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = $2,460,000 
| gross          = $2,850,000 (US rental) 
}}
 same name.
 International Rag", Decca both CD set Irving Berlin in Hollywood. The film was out of circulation for many years but was issued on DVD in 2004.
 Alfred Newman won the Academy Award for Best Scoring of a Musical Picture, and Irene Sharaff was nominated for her costume design. Lang was nominated for Outstanding Directorial Achievement in Motion Pictures by the Directors Guild of America and the Grand Prize at the 1953 Cannes Film Festival,    and Sheekmans screenplay was nominated Best Written American Musical by the Writers Guild of America.

==Cast==
* Ethel Merman as Sally Adams
* Donald OConnor as Kenneth Gibson
* Vera-Ellen as Princess Maria (singing voice was dubbed by Carol Richards)
* George Sanders as General Cosmo Constantine
* Billy De Wolfe as Pemberton Maxwell
* Helmut Dantine as Prince Hugo
* Walter Slezak as August Tantinnin
* Steven Geray as Prime Minister Sebastian
* Ludwig Stössel as Grand Duke Otto (as Ludwig Stossel)
* Lilia Skala as Grand Duchess Sophie
* Charles Dingle as Sen. Brockway
* Emory Parnell as Sen. Charlie Gallagher
* Percy Helton as Sen. Wilkins

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 