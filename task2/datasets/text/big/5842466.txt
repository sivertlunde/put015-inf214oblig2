Show Biz Bugs
{{Multiple issues|
 
 
}}

{{Infobox Hollywood cartoon
| series            = Looney Tunes (Bugs Bunny)
| image             = Show Biz Bugs Lobby Card.PNG
| caption           = Lobby card.
| director          = Friz Freleng
| producer          = Edward Selzer (uncredited)
| story_artist      = Warren Foster
| animator          = {{plainlist|
* Gerry Chiniquy Arthur Davis
* Virgil Ross}}
| voice_actor       = Mel Blanc
| layout_artist     = Hawley Pratt
| background_artist = Boris Gorelick
| musical_direction = Milt Franklyn 
| studio            = Warner Bros. Cartoons
| distributor       = Warner Bros. Pictures The Vitaphone Corporation
| release_date      = November 2, 1957 (USA) {{cite book
| last1                 = Beck
| first1                = Jerry
| authorlink1           = Jerry Beck
| last2                 = Friedwald
| first2                = Will
| authorlink2           = Will Friedwald
| title                 = Looney Tunes and Merrie Melodies: A Complete Illustrated Guide to the Warner Bros. Cartoons
| year                  = 1989
| publisher             = Henry Holt and Company New York
| isbn                  = 0-8050-0894-2
| oclc                  = 19671400
| page                  = 303
}} 
| color_process     = Technicolor
| runtime           = 7 minutes
| movie_language    = English
}}

Show Biz Bugs is a 1957 Warner Bros. Looney Tunes animated short directed by Friz Freleng and featuring Mel Blanc as the voices of Bugs Bunny and Daffy Duck. " ". Big Cartoon DataBase,  August 30, 2014 

The basic setting and conflicts of this film were reprised for the linking footage for The Bugs Bunny Show television series.

==Summary== marquee is billing "according to drawing power," Daffy is determined to prove that he is the star of the show.
    Tea for Jeepers Creepers." Blue Cross.") In a final attempt to impress the audience, Daffy performs a deadly stunt (which he refers as "an act that no other performer has dared to execute!"), by drinking some gasoline, some nitroglycerin, some gunpowder, and some Uranium-238, "shake well," and swallowing a lit match ("Girls, you better hold onto your boyfriends!"), causing him to explode. The audience loves the performance, but Daffy (now a transparent ghost and ascending to heaven) "can only do it once."

==Critical reception==
In a commentary by Greg Ford, he cites that the short contains some of Warren Fosters "best gags as a writer". Ford also described it as "a definitive Bugs/Daffy showbiz rivalry cartoon". 

==Edited versions==
The scene at the end of this cartoon where Daffy performs his final act by drinking dangerous chemicals is almost always edited on broadcast and cable TV, but in different ways:
*The BBC version of the cartoon ends with a fake fade-out on the shot of Daffy black and smoldering after getting frustrated by Bugs missing the final note on the booby-trapped xylophone and deciding to do it himself. The BBC version also adds applause after the ending xylophone gag just before the cartoon ends.
*Cartoon Network has, at times, aired the original ending uncensored. When Cartoon Network began airing the short censored, the scene of Daffy drinking the gasoline and nitroglycerin was removed and replaced with a frozen shot of Bugs staring at Daffy from off-stage. In 2003, another censored version aired. It was similar to the version that aired on BBC (with the cartoon ending after the xylophone gag), only there was no applause added. As of 2011, the ending has been shown in full. Bugs Bunny and Tweety Show left in the ending, but cut Daffy drinking the gasoline, so it looks as if he drinks the nitroglycerin first. That is also how the short is shown in The Looney, Looney, Looney Bugs Bunny Movie.
*Nickelodeons version aired this cartoon with the original ending, but cut the part where Daffy strikes the match, asides to the audience "Girls, you better hold on to your boyfriends," and swallows the match (making it seem as if he exploded from "shaking well" after swallowing the uranium 238).
*When The Looney Looney Looney Bugs Bunny Movie (which includes clips from this cartoon as the climax) aired on The Disney Channel, Daffys death defying act was edited so severely that the only scenes left were Daffy holding the bottle of nitroglycerin and the explosion from after the match swallowing (making it seem as if Daffys holding the nitroglycerin caused the explosion). 

==Production== Tea for Jeepers Creepers". 

This short contains scenes with animation repurposed from earlier cartoons; reused footage includes scenes taken from "Hot Cross Bunny", "Bugs Bunny Rides Again", "Ballot Box Bunny", "Case of The Missing Hare", "Whats Up, Doc? (1950 film)|Whats Up, Doc?" and "Curtain Razor". 

==Previous film references==
The xylophone gag was previously used in the Private Snafu short Booby Traps and the Bugs/Yosemite Sam short Ballot Box Bunny (and later in Rushing Roulette), only in both cases the instrument used was a piano. The song used in each case, as in Show Biz Bugs, is "Believe Me, if All Those Endearing Young Charms."

The final act and the pigeon circus had been used in an earlier Porky Pig cartoon called Curtain Razor in which a fox does the same act Daffy does attempting to show Porky he is a star, and, much like Show Biz Bugs, the final act in Curtain Razor has been edited on Cartoon Network to remove him ingesting gasoline (the syndicated version of The Merrie Melodies Show also cuts the gasoline-drinking and edits it even further by cutting out the fox swallowing a match).

==Availability==
*Show Biz Bugs is available uncut on the   DVD set, disc 4 and on the   Blu-ray set, disc 1. It is also available on the "A Salute To Friz Freleng" VHS, the Superior Duck VHS, and the "Looney Tunes: Curtin Calls" laserdisc.

==References==
 

==External links==
*  
*  

 
 
 
 