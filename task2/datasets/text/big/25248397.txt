Wrong Turn at Tahoe
{{Infobox Film
| name = Wrong Turn at Tahoe
| image = Wrong Turn at Tahoe.jpg
| caption = 
| director = Franck Khalfoun
| producer = {{plainlist| Robert L. Levy
* Freddy Braidy
* Rich Cowan
* Johnny Martin
* Richard Salvatore
}}
| writer = Eddie Nickerson
| starring = {{plainlist|
* Cuba Gooding Jr.
* Miguel Ferrer Mike Starr Johnny Messner
* Harvey Keitel
}}
| music = Nicholas Pike
| cinematography = Christopher LaVasseur
| editing = Patrick McMahon
| distributor = Paramount Home Entertainment 
| studio = {{plainlist|
* Paramount Famous Productions
* North by Northwest Entertainment
* Pure Pictures Entertainment
}}
| released =  
| runtime = 90 minutes
| language = English
| budget = $6 million  
}}
Wrong Turn at Tahoe is a 2009 Direct-to-video|direct-to-DVD crime film directed by Franck Khalfoun and starring Cuba Gooding Jr., Miguel Ferrer, and Harvey Keitel.

==Plot==
Joshua and his boss Vincent drive to a hospital. Both seem to be shot and in pain. Joshua thinks back to his childhood, when his father was shot in front of his eyes.

In a flashback, Joshua and his partner, Mickey visit people who owe Vincent money. They visit a crazy druggie who tells them that a small time drug dealer named Frankie Tahoe wants to kill Vincent. Joshua and Mickey tell Vincent, and the trio intimidate a guy who works for Tahoe into telling them where to find Tahoe.  They find him at a club. After a talk where Frankie insults Joshua and Vincents religion, which he holds dear to him, Vincent kills Tahoe with a baseball bat. The trio dump the body in a landfill. While doing this, Vincent reveals that Mickey and his wife have been having an affair, and Vincent kills Mickey.

While Joshua and Vincent have breakfast, Joshua tells his boss that he has become weary of the violence and wants to retire. Vincent admits that he has violent outbursts but insists that Joshua owes him his life. Angered, Vincent says that Joshua can not retire, and he leaves. When Vincent goes home, but finds he has two men watching his house. While confronting  them, Joshua appears. The men tell Vincent that they have been targeted by Nino, a powerful crime boss. Nino calls his men, and when Vincent answers the phone, Nino invites them to his house.

After Nino taunts them, Vincent tells him that he will not be intimidated, and Nino walks away. Once home, Vincent finds his wife brutally murdered. Vincent attempts to give Joshua a large sum of money as severance and tells him to leave the city while he seeks vengeance. Joshua refuses and says that he will not retire while Nino is still alive. They wait until night and return to Ninos house. Fighting their way through Ninos henchmen, Vincent finds Nino, and the two have a standoff, during which Vincent is hit by a shotgun blast, but survives. Nino gets shot and dies.

As Vincent and Nino fight, Joshua engages in a hand-to-hand fight with one of Ninos henchmen. After Joshua kills him, another appears. As this man gains the upper hand, Vincent shoots him and saves Joshua. Joshua identifies the wounded man as the man who killed Vincents wife, and Vincent kills him. While leaving, Ninos wife appears; Joshua and she exchange fire, and both are hit. Ninos wife dies, and Joshua takes a hit to the stomach. In the present, as Vincent and Joshua are in the car, it is revealed that Vincent murdered Joshuas father. Vincent tells Joshua that he is too loyal to kill his own boss. Joshua puts his gun to Vincents head, leaving it to the viewer to decide if he kills his boss.

==Cast==
* Cuba Gooding Jr. as Joshua
* Miguel Ferrer as Vincent
* Harvey Keitel as Nino Mike Starr as Paulie Johnny Messner as Mickey
* Alex Meneses as Marisa
* Leonor Varela as Anna
* Louis Mandylor as Steven
* Noel Gugliemi as Frankie Tahoe
* Paul Sampson as Richie
* John Cenatiempo as Johnny
* Genevieve Alexandra as Tiff
* Reed McColm as Donnie
* Michael Sean Tighe as Jeff
* Rich Warren as Anthony
* Walter Poole as Gregory (bodyguard)

==Production==
The film was shot in Spokane, Washington.   

==Reception==
Jamie Russell of Radio Times rated it 2/5 stars and called it a forgettable thriller that copies Quentin Tarantino.   Jim Kershner of The Spokesman Review called it "Tarantino-light".   Bill Gibron of PopMatters rated it 5/10 stars and wrote, "In essence, Wrong Turn at Tahoe is film noir without the pulsating plot undercurrent or aesthetic flare. The result is all mood and no movement."   Tyler Foster of DVD Talk rated it 2/5 stars and wrote that Ferrers performance, which he praised, is not enough to make the film entertaining enough for a recommendation.   Daryl Loomis of DVD Verdict wrote, "There are heavy shades of Tarantino and Scorsese, but they make it their own". 

==References==

 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 