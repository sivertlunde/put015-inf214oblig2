Babu I Love You
{{Infobox film
| name           = Babu I Love You
| image          = Babu I Love You.jpg
| caption        = Poster
| director       = Sanjay Nayak 
| producer       = Somya Ranjan Patnaik,   Ajit Mishra
| writer         = Somya Ranjan Patnaik
| starring       = Siddhanta Mahapatra, Archita Sahu, Anu Chowdhury, Bijay Mohanty   
| music          = Manmath Misra
| cinematography = S. Ranjan   Samba Shiva Rao
| editing        = Sanjay Nayak
| studio         = Balunkeswar Films
| distributor    = Balunkeswar Films
| released       =  
| runtime        = 172 min
| country        = India
| language       = Oriya
| budget         = 
| gross          = 
}}
Babu I Love You is an Oriya drama and romance film released on May 9, 2005.  Starring Siddhanta Mahapatra, Archita Sahu and Anu Chowdhury in key roles.   {{cite web
|url=http://odiacinemaworld.blogspot.com/2010/08/oriya-odia-cinema-produced-during-2005.html|title=Oriya / Odia Cinema produced during  2005 in Orissa / Odisha
|publisher=odiacinemaworld.blogspot.com}}  
   It loosely inspired by Bollywood movie Teri Meherbaniyan.  
  
==Synopsis==
Raghupati (Raimohan Parida) is a cruel & powerful man in the village. He kills the the Jamidar (Landlord) of the village, occupies his palace and keeps his daughter Arati (Anu Choudhury) under terror. Also keeps  Jagu (Siddhanta Mahapatra) as a slave, because Jagus father exchange him against the loan he takes from Raghupati. Arati and Jagu as are in the same fate love each other. Raghupati with his aids Raghsb and Rajaram (Jairam Samal) exploits and rules the poor people of the village. One day a forest officer Akash (Chandan Kar) comes to the village to look after nearby forest. He meets Bijuli {Archita Sahu) by an accident and both falls in love each other. Gradually due his noble qualities, Akash becomes the ideal voice of the village. Akash counters and tries to stop the illegal activities  the trio Raghupati, Raghab and Rajaram. 

One day Akash  goes to the town to attend head office.. Before leaving, he asks his faithful dog  to guard Bijuli. But Bijuli tires of the dogs constant attention and locks him up. The trio Raghpati, Raghab and Rajaram tries to rape Bijuli.To save herself from rape, Bijuli suicides by stabbing herself. An enraged and heartbroken Akash lashes the dog for failing to guard Bijli, but Arati and Jagu  holds back, telling him that Bijuli herself locked the dog up. Eventually Akash brutally murdered by the trio and Jagu falsely send to jail for the murder. But the faithful dog comes to rescue, collects the evidence that Raghupati, Raghab and Rajaram are the culprits. Finally the trio arested. Jagu and Arati marries at last.

==Cast==
* Siddhanta Mahapatra    ...    Jaganath / Jagu
* Anu Choudhury	... 	Arati
* Chandan Kar   ... 	Akash
* Bijoy Mohanty	... 	Bijulis father
* Archita Sahu	... 	Bijuli
* Raimohan Parida	... 	Raghupati
* Jairam Samal	... 	Rajaram
* Papi Santuka	... 	Raghab
* Biju Badajena		...     Police Inspector
* Premanjana Parida... 	Jamidar (Aratis father)
* Kanta Singh	...     Village Lady
* Tapas Roy	... 	Naga
* Subhranshu Sarangi	... 	Police Inspector
* Bapi	... 	Hotel owner
* Seema	... 	Neuli
* Rajat	... 	Young Akash
==Soundtrack==
The Music for the film is composed by Manmath Misra
{| class="wikitable sortable" style="text-align:center;"
|-
! Song
! Lyrics
! Singer(s)

|- Babu I Bibhu Kishor, Ira Mohanty 
 
|- Adha Batu Chaligalu|| Arun Mantri ||Md. Aziz 
|- Tu Daki Ira Mohanty 
|- Mana Khali Thila Kali|| Manmath Misra|| Kumar BapiIra Mohanty 
|- Pabana re Ira Mohanty 
|- Saharia Babu|| Ira Mohanty 
|- Apple Kha|| Manas Pritam , Sanghamitra
|-

|} {{cite web |url=http://songs.incredibleorissa.com/babu-i-love-you/|title=BABU I LOVE YOU
Odia Film Songs  |publisher=IncredibleOrissa}}    
==Awards==
* Orissa State Film Awards 2006
** Best Actress in a supporting role ... Archita Sahu
** Best Art Director ... Amiya Maharana 
==References==
 

==External links==
*  

 
 
 