Heli (film)
 
{{Infobox film
| name           = Heli
| image          = Heli poster.jpg
| caption        = Theatrical release poster of Heli
| director       = Amat Escalante
| producer       = Jaime Romandía
| writer         = Amat Escalante Gabriel Reyes
| starring       = Armando Espitia Andrea Vergara Linda González Juan Eduardo Palacios
| music          = 
| cinematography = Lorenzo Hagerman
| editing        = Natalia López
| distributor    = Mantarraya
| released       =  
| runtime        = 105 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
}}
 Best Director Best Foreign Language Film,       but it was not nominated.

==Plot==
Heli tells the story of the titular protagonist (played by Armando Espitia), a seventeen-year-old boy living with his wife (Linda González) and his sister, Estela (played by Andrea Vergara). The film follows the arcs of these characters and Estelas boyfriend (played by Juan Eduardo Palacios) as they struggle with drugs, violence, and corruption.   

Heli is a young man working in a car assembly factory; he lives with his father who also works there, his wife Sabrina, his baby son Santiago and his sister Estela. His life is normal, slow, and without economical prospects, and he suffers from a troubled relationship with his wife.

Estela is revealed to be in a relationship with Beto, a cadet who at 17 is much older than her. Estela, despite Betos enthusiasm for engaging in sexual relations, is firm in her refusal of him, out of fear of becoming pregnant. Beto proposes they marry and run away together. To do so, he plans to sell some stolen cocaine packages that a corrupt general secretly drew from a cache the army confiscated and burned in an official event. Beto hides the drugs in Helis house with Estelas consent until the sale. However, Heli discovers the affair and reprimands his sister, locking her in her room, after secretly disposing of the drugs in an isolated water pit for cattle. 

Later that night, some federal policemen storm Helis house, killing Helis father when he tries to defend himself thinking they were being assaulted. The officers take Heli and Estela by force (Sabrina and their son not being present at the time) and with a badly beaten Beto they try to find the stolen cocaine. When they find out Heli destroyed the package, the policemen abandon Helis fathers body on the road, and drive to a secure house where drug dealers harshly torture Beto to the point of killing him. Helis life is spared, but Estela is taken elsewhere. The dealers hang Betos body from a pedestrian bridge, leaving a badly hurt Heli at the scene.

Heli manages to walk back home and the local police, learning of the incident, help Heli to heal and find the body of his father. Later they take Helis testimony; however, the police assumes Heli and his father are involved with drug dealing and decide not to help them because Heli, shaken and scared of the situation, is not willing to tell what happened nor to sign any written testimony in fear of having himself or his father framed as criminals. 

Heli has to deal with the traumas inflicted by these experiences and with the corruption of the police, while his wife begins to distance herself from him due to the erratic and even violent behaviour he begins to display. After going back to work at the car assembly plant, Heli gets distracted on the job and is eventually fired. The female detective assigned to his case, after learning that Estela was Betos girlfriend, asserts that the file of the case is closed, and makes sexual advances on Heli, which are initially reciprocated but eventually rejected. 

Estela returns home, traumatised to the point of losing the speech and pregnant beyond the stage she can legally have an abortion. Heli and Sabrina comfort her, and Estela draws a map to the location she was held and raped. Heli goes to there and kills the man living there. Seemingly relieved from some of the trauma, he returns home and successfully makes love to his wife, while Estela sleeps peacefully with her nephew.

==Cast==
*Armando Espitia as Heli, the titular character
*Andrea Vergara as Estela, Helis sister
*Linda González as Sabrina, Helis wife
*Juan Eduardo Palacios as Alberto, Estelas boyfriend
*Kenny Johnston as the American Commander and military training counselor

==Reception==
Following its  , Robbie Collin was more appreciative of the film and noted that the film "shows that even a bleak existence can make an uplifting story".  Escalante declared for the Mexican website "Animal Político" (Political animal) that the film itself isnt anti-Mexican. Additionally, Gabriel Reyes, (who helped Escalante on writing the script), declared that, "It would be socially irresponsible not to speak about the bad things that are occurring in our country."   

==Accolades==
{| class="wikitable"
|-
! Award !! Category !! Winner/Nominee !! Result
|- 2013 Cannes Cannes Film Festival Best Director Best Director Amat Escalante
| 
|- Festival du Nouveau Cinema Louve Dor for Best Feature Film in the International Competition
|
| 
|- Film Fest Munich  Arri Award 
|
| 
|- International Film Festival of the Art of Cinematography Plus Camerimage Silver Frog for Best Cinematography Lorenzo Hagerman 
| 
|- Lima Film Festival Jury Prize for Best Film 
|
| 
|- Havana Film Festival Casa de Las Américas Award
|
| 
|- Havana Film Festival Grand Coral - First Prize
|
| 
|- Havana Film Festival Cuban Critics Award
|
| 
|- Palm Springs International Film Festival Cine Latino Award
|
| 
|- Stockholm International Film Festival Best Cinematography
|
| 
|- The Platino Awards for Iberoamerican Cinema  Best Director
|
| 

|-
|Ibero-American Fenix Film Awards 2014 Best Director
|
| 

|-
|Ibero-American Fenix Film Awards 2014 Best Original Script
|
| 
|}

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Mexican submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 