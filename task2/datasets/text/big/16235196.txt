Three and Out
 
{{Infobox Film name       = Three and Out image      = Main portrait.JPG caption    = Theatrical poster director   = Jonathan Gershfield writer     = Steve Lewis and Tony Owen starring   = Mackenzie Crook Colm Meaney Gemma Arterton Imelda Staunton Kerry Katona music  Trevor Jones cinematography = Richard Greatrex
|distributor= Worldwide Bonus Entertainment released   =   runtime    = 95 minutes country    = United Kingdom budget     =  language   = English
}} 2008 Cinema British comedy film directed by Jonathan Gershfield. It premiered in London on the 21 April 2008 and was released in the UK and Ireland on 25 April 2008. It was released in Australia under the title A Deal Is a Deal.

==Plot==
Paul Callow (Mackenzie Crook) accidentally runs a man over with his underground train, after the man is pulled on to the tracks by his dog. After a week off he kills a second passenger who falls on to the tracks after having a heart attack.

Before taking time off for the second accident his colleagues tell him about a little-known rule at London Underground that no-one talks about: three under within a month, and you lose your job - earning yourself ten years salary in one lump sum. But being off for the next week means that Paul needs to find someone willing to kill themselves by the following Monday.

Paul sets about trying to find someone prepared to die under his train – and after hearing a report about Holborn Viaduct he comes across Tommy Cassidy (Colm Meaney) attempting to jump off. He grabs his hand and pulls him up but far from being grateful Tommy is angered by Pauls interference and moans about how "do gooders" will not just mind their own business, however upon hearing a police siren he agrees to get in Pauls car.

While in a bar Paul explains to Tommy that he will pay him if he is willing to jump in front of his train. Tommy is scornful asking what good will the money do him if he has to die. Paul says that he can have one last weekend of fun. Tommy however wants to do something meaningful.

Tommy agrees to his proposal and decides to spend his last weekend making amends with his estranged family.

Tommy hires a car and they travel to Liverpool. While there they find that Tommys wife Rosemary (Imelda Staunton) and daughter Frances (Gemma Arterton) have moved to the Lake District. Paul tags along to protect his investment.

While in the Lake District Tommy meets with resistance from his wife and daughter. He reconciles with his wife, who reveals she has found a new man. Paul meets the daughter at a local pub and the two get drunk and sleep together.

On the Sunday morning Tommy tries to talk to his daughter and sees her in bed with Paul, then chases him over some hills before having a mild cardiac attack and being taken to hospital. Tommy and Paul leave to go back to London.

Paul tells Tommy he does not want to kill him and Tommy insists that he go through with it. On the Monday morning Paul is told by his colleagues that the "three and out" rule was a joke, and that there is no pay off. When the time approaches Paul sees Tommy on the tracks and stops and reports an "animal on the tracks".  However, crying and recalling "a deals a deal", Paul accelerates towards Tommy, who recites William Butler Yeats poem Lake Isle of Innisfree as the train kills him.

Distraught, Paul sits at home then begins to write and changes the title of his novel to Three and Out, we see Frances receive a copy of the novel along with £10,000 which Paul had promised to Tommy. Hidden behind the cheque is the dedication "In Memory of Tommy Cassidy".  Realising her Dad has died, Frankie breaks down in tears.  She travels to London and meets Paul, they go deep sea diving with Great White Sharks, Tommys last wish.

==Cast==
*Mackenzie Crook as Paul
*Colm Meaney as Tommy
*Gemma Arterton as Frankie
*Imelda Staunton as Rosemary
*Antony Sher as Maurice
*Kerry Katona as Mary Gary Lewis as Callaghan
*Annette Badland as Maureen
*Mark Benton as Vic
*Rhashan Stone as Ash
*Sharon Duncan Brewster as Yvonne
*Frank Dunne as Danny
*Steve Money as Muscles
*Rob Witcomb as Gary
*Dean Lennox Kelly as Policeman

==Production==
Although set around the London Underground, most of the action in the film takes place above ground – in North London, Liverpool, and Cumbria. 

A scene set in South Africa was actually filmed in Gibraltar. 

==Controversy==
The film, whose filming was carried out in close co-operation with London Underground,  has been the cause of controversy due to the plot of the film involving a character seeking someone to commit suicide by diving under his train.  Before the film was released, the train drivers union, ASLEF, called the plot "insulting and foolish" despite not having seen the film    and have pointed out that train drivers who experience such incidents find them "life-changing traumas".    The union organized a protest at premiere of the film, handing out leaflets to the public, from whom it claims to have received strong support.   The filmmakers spokesperson stated that "difficult issues portrayed in the film have been handled sensitively." 

==Awards==

2009
* Oxford International Film Festival - Won - Best Screenplay
* Oxford International Film Festival - Nominated - Best Narrative Feature
* Method Fest Independent Film Festival - Won - The Stella Artois Award for Best Foreign Film 2009
* Method Fest Independent Film Festival - Won - Colm Meaney - Stockholm Krystal Best Supporting Actor—Feature Film 2009
* California Independent Film Festival - Won- Best Comedy
* California Independent Film Festival - Won - Best Actress - Imelda Staunton
* Durango Film Festival - Won - Audience Award - Best Feature Film
* Durango Film Festival - Won - Jury Award - Best Feature Film
* Tallahassee Film Festival - Won - Best Narrative Feature
* Garden State Film Festival - Won - International Feature Length Comedy
* Honolulu International Festival - Won - Special Mention - Outstanding Directorial Achievement in Motion Pictures
* Palm Springs International Film Festival - Official Selection 2009

2008
* Southern Appalachian International Film Festival - Won - Best Comedy or Musical
* Sao Paulo International Film Festival - Nominated - International Jury Award
* Fort Lauderdale International Film Festival - Official Selection 2008

==Box office==
Three and Out flopped,    entered at number 12 in the UK box office chart with an opening weekend take of £189,454.

==DVD release==
Contender Entertainment Group have released this film on Region 2 DVD on 15 September 2008 in The United Kingdom.  The extra features included cast & crew biographies, deleted scenes, trailers and a featurette on the making of this film.  It was rated as a 12 certificate and has a distinct sticker on the front saying Gemma Arterton as the new Bond Girl being in this film.  Gemma appears as Agent Fields in the 22nd James Bond film Quantum of Solace. 

==Soundtrack== Dreaming of You" by The Coral, "One Way or Another" by Blondie (band)|Blondie, "Spirit in the Sky" by Norman Greenbaum and "Night Time" by N.U.M.B. There is a soundtrack of the same name accompanying this film.

==Novelisation==
The novelisation of Three and Out by Tom Henry was released on 14 April 2008 by Rovinge Publishing Company Limited. It is based on the original screenplay by Steve Lewis and Tony Owen and provides a great deal of additional information on the characters lives as well as answering a number of questions not resolved by the film.

==References==
 

==External links==
* 
* 
* 

 
 
 