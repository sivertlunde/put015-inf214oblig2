King Solomon's Mines (1985 film)
{{Infobox film
| name           = King Solomons Mines
| image          = King Solomons Mines 1985.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = J. Lee Thompson
| producer       = Yoram Globus (producer) Menahem Golan (producer)  Rony Yacov (associate producer)
| writer         = Gene Quintano James R. Silke H. Rider Haggard (novel)
| narrator       = 
| starring = {{Plainlist|
* Richard Chamberlain
* Sharon Stone
* Herbert Lom
* John Rhys-Davies
}}
| music          = Jerry Goldsmith
| cinematography = Alex Phillips Jr.
| editing        = John Shirley
| distributor    = The Cannon Group Paramount Pictures  (Viacom Inc.)/MGM
| released       =  
| runtime        = 100 minutes
| country        = United States English
| budget         = $11 million Andrew Yule, Hollywood a Go-Go: The True Story of the Cannon Film Empire, Sphere Books, 1987 p95-96 
| gross          = $14,400,000 (US rentals) 
}} action adventure third of novel by the same name by Henry Rider Haggard. It stars Richard Chamberlain, Sharon Stone, Herbert Lom and John Rhys-Davies. It was adapted by Gene Quintano and James R. Silke and directed by J. Lee Thompson. This version of the story was a light, comedic take, deliberately referring to, and parodying Indiana Jones (in which franchise actor Rhys-Davies appeared in two installments). It was filmed outside Harare in Zimbabwe. 

It was followed by a sequel (filmed back-to-back) Allan Quatermain and the Lost City of Gold (1987).

==Plot== German military expedition on the same quest, led by Bockner (Herbert Lom), a single-minded knackwurst-munching, bald-headed Colonel and a ruthless Turkish slave-trader and adventurer, Dogati (John Rhys-Davies). Huston is being forced to interpret another map, also believed to be genuine.

The two rival expeditions shadow each other, clashing on several occasions, and finally enter the tribal lands of the Kukuana who capture them. The tribe is under the control of the evil priestess, Gagoola, who has Quatermain hung upside down over a pond full of crocodiles. Just when all seems lost, Umbopo arrives and after defeating Gagoolas warriors in combat, reveals his identity as an exiled tribal chief and the rightful ruler of the Kukuanas. As the tribesmen submit to him, Gagoola captures Jesse and flees into caves in the depths of the Breasts of Sheba, the twin mountain peaks where the mines are located. They are pursued by Bockner and Dogati, who attack the village in full force. They follow Quatermain and Umbopo to the entrance to the mines, but are hampered by a moat of quicksand. Bockner orders his men forward into the moat, but they have trouble crossing it. Dogati then kills all of Bockners soldiers, as well as most of his own men, and uses their bodies as stepping stones to cross the moat safely. As they approach the entrance, Bockner shoots Dogati and takes command of what little remains of the party.

Inside the mines, Quatermain and Umbopo rescue Jesse and find the resting place of all the former tribal queens, including the Queen of Sheba herself, encased in crystal. Umbopo explains that Gagoola had attempted to sacrifice Jesse in order to keep her power as the Kukuanas ruler because of Jessies strong resemblance to the Queen of Sheba. Then Gagoola appears and taunts Umbopo, who pursues her through the caverns. As Bockner and his men arrive next, Quatermain and Jessie flee for safety, but end up in the caverns treasure chamber, which is full of raw diamonds and other priceless treasures. As they gather some of the diamonds to take with them, Bockner hears their voices from outside the chamber, but before he can enter, Gagoola activates a hidden rock switch and seals Quatermain and Jessie inside the chamber. The switch also triggers a trap that causes the ceiling of the chamber, which is lined with stalactites to lower on them. Quatermain and Jessie manage to stop the ceiling trap, but then the chamber begins filling up with water. Just as the chamber fills completely, a lit stick of dynamite set by Bockner outside the chamber door explodes, sending them both spewing out of the chamber in the resulting flood to safety. 

Bockner enters the chamber and quickly lays claim to the treasure, only to be confronted by a wounded, but very much alive, Dogati, who was wearing a protective vest that shielded him from the bullets. He then forces Bockner to swallow some of the diamonds, intending to cut him open to retrieve them later on. Meanwhile, Umbopo finally corners Gagoola. But rather than face his judgment, she instead leaps down one of the volcanos shafts and is incinerated when she lands in the molten lava below. However, the reaction causes a series of eruptions throughout the mines. Dogati is partially buried when the treasure chambers ceiling collapses, but Bockner is unharmed. He gloats to Dogati after claiming a few more diamonds for himself, then leaves the chamber. But not before firing his gun at the ruined ceiling, burying Dogati alive. Quatermain, Jessie and Umbopa quickly flee for their lives through the collapsing caverns. They cross over a small booby- trapped lake (which one of Bockners men fell victim to earlier), only to be stopped by Bockner, who demands they surrender their diamonds to him. Quatermain places the diamonds on the central stepping stone that triggers the trap and tells Bockner to come take the diamonds himself if he wants them. Bockner does so and falls into the lake, only to be seized in the jaws of a Mokele-mbembe and dragged beneath the water. The trap resets itself and the diamonds rise back to the surface, but Umbopo warns Quatermain and Jesse not to take them, saying they belong to the mountain.

The trio continue their escape through the caverns, which becomes even more dangerous as the lava chamber they are in is full of fire and falling rocks. Quatermain tells Umbopo to take Jessie through to safety while he follows them. But before he can do so, he is struck down by Dogati, who survived the cave-in. A brutal fight between them ensues, but Quatermain gains the upper hand at the last instant, sending Dogati falling into the chambers lava pit to his death. Quatermain manages to escape from the mines at the last minute, just as the volcano explodes, sealing the entrance forever.

Returning to the village, Umbopo assumes his rightful place as the ruler of the Kukuanas and he and his people bid a fond farewell to Quatermain and Jesse. As they exit the village, they each reveal they had kept a diamond from the mines as a souvenir of their adventure and the movie ends with them kissing outside the village gates.

==Reception== 1950 film version with Stewart Granger and Deborah Kerr.  It was also nominated for two Razzie Awards including Worst Supporting Actor for Herbert Lom and Worst Musical Score for Goldsmith. 

==Releases==
MGM released the film on DVD on February 10, 2004. 

==Soundtrack==
The films score was composed and conducted by Jerry Goldsmith, and performed by the Hungarian State Opera Orchestra. Restless Records issued an album on LP and cassette; Milan later released it on compact disc minus the cue "The Ritual" and paired with Alan Silvestris The Delta Force. In 1991 Intrada Records released an expanded version, later reissued in 1997; Prometheus released the complete score in 2006. Quartet Records issued a two-disc edition in 2014 with the Prometheus content on disc one and the original album presentation on disc two.

===Quartet release===

Tracks in bold premiered on the Intrada CD, tracks in italics premiered on the Prometheus edition.

Disc 1: The Film Score

* Main Title (3:40)
* Welcoming Committee (0:51)
* No Sale (3:38)
* The Mummy (1:15)
* Have a Cigar (3:39)
* Good Morning (2:32)
* Under the Train (3:08)
* Dancing Shots (3:38)
* Pain (3:07)
* The Chieftain (1:03)
* Percussion Sweetener (0:40)
* Pot Luck (3:23)
* Upside Down People (5:04)
* The Crocodiles (3:08)
* The Mines (1:25)
* Pre-Ritual/The Ritual, Part I/The Ritual, Part II (6:16)
* Low Bridge (3:28)
* Falling Rocks (1:07)
* Final Confrontation (3:11)
* No Diamonds (4:14)
* Bonus Track: Ride (3:06)
* Bonus Track: Theme From King Solomon’s Mines (3:40)

Disc 2: The Original Album

* King Solomon’s Mines (Main Title) (3:39)
* Upside Down People (4:51)
* The Crocodiles (3:07)
* Pot Luck (3:23)
* Forced Flight (5:22)
* Dancing Shots (3:38)
* Good Morning (2:33)
* No Pain (3:07)
* The Ritual (5:05)
* No Diamonds (End Title) (4:21)

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 