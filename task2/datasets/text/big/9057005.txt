Lawaaris (1981 film)
 
{{Infobox film
| name           = Laawaris
| image          = Laawaris 1981.jpg
| image_size     = 
| caption        = Film poster
| director       = Prakash Mehra
| producer       = Prakash Mehra
| writer         = Shashi Bhushan Kader Khan Prakash Mehra Din Dayal Sharma Bindu Raakhee Om Prakash Ranjeet Ram Sethi
| music          = Kalyanji-Anandji
| cinematography = N. Satyen
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 189 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Laawaris (Hindi: लावारिस, English: Orphan or Bastard) is a blockbuster   who earned her first Filmfare nomination as best female playback singer, and the second time by Amitabh Bachchan. The second version turned out to be a mega hit especially due to Amitabhs performance in drag wear. The lyrics of the song were funny and gives tribute to all kinds of wives be they fat, tall, dark skinned or fair skinned. Even until today the song is popular among audiences. Laawaris starred Amitabh Bachchan, Amjad Khan, Zeenat Aman and Raakhee. In Urdu, "Laawaris" means bastard or loosely means orphan. Although the word has a negative connotation, it is not as negative as the English connotation. The story revolves around an orphan who stumbles over reality in search for his parents.

The film was an All Time Highest Earner that year, got highest verdict present at that time by Trade Guide , and was among those rare movies, which crossed 2 Crore per territory.  There was only 13 All time earner  movies till 1984 and Laawaris was among them. 

It earned additional Filmfare nominations for Best Actor (Amitabh Bachchan) and Best Supporting Actor (Suresh Oberoi). 

==Synopsis==
A child born of an illegitimate relationship is named Heera after the name of a dog by a drunkard who makes him work hard for liquor. After knowing that he is a Laawaris (orphan), he leaves home looking for a job with Ranjeet.  Fate brings Heera and his biological father together. Will they ever know about the relation between them? What happens if they find out about it?

The film is remembered for Amitabh Bachchans over-the-top acting and fiery dialogues using his all famous Bachchan-baritone, his charismatic and stylish screen presence.

==Soundtrack==
{{Infobox album  
| Name        = Laawaris
| Type        = soundtrack
| Artist      = Kalyanji-Anandji
| Cover       =
| Caption     = 
| Released    = 1981
| Recorded    =
| Genre       = Filmi
| Length      = Hindi
| Label       =
| Producer    = Prakash Mehra
| Reviews     =
| Compiler    =
| Misc        =
}}

{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      =
| all_music       = Kalyanji Anandji

| writing_credits = 
| lyrics_credits  = yes
| music_credits   =

| title1          = Mere Angne Mein
| note1           = Female
| writer1         =  Anjaan
| music1          =
| extra1          = Alka Yagnik
| length1         =

| title2          = Mere Angne Mein
| note2           = Male
| writer2         = Anjaan
| music2          =
| extra2          = Amitabh Bachchan
| length2         =

| title3          = Jiska Koi Nahin
| note3           =
| writer3         = Anjaan
| music3          =
| extra3          = Kishore Kumar
| length3         =

| title4          = Kab Ke Bichhde Hue
| note4           =
| writer4         = Anjaan
| music4          =
| extra4          = Kishore Kumar, Asha Bhosle
| length4         =

| title5          = Kahe Paise Pe
| note5           =
| writer5         = Anjaan
| music5          =
| extra5          = Kishore Kumar
| length5         =

| title6          = Apni To Jaise Taise
| note6           = 
| writer6         =
| lyrics6         = Prakash Mehra
| music6          =
| extra6          = Kishore Kumar
| length6         =

| title7          = Jiska Koi Nahin
| note7           = 
| writer7         = Anjaan
| music7          =
| extra7          = Manna Dey
| length7         = 
}}

==Remakes==

Laawaris was remade as
 Telugu in 1982, starring N. T. Rama Rao|NTR, Kaikala Satyanarayana and Jayasudha. Tamil in Vijayakumar and Gouthami.

Both films were massive hits in their respective languages.

==Awards and nominations==
{| class="wikitable"
|-
! Award & Category || Artist || Status || Notes
|-
| colspan="5" |
*Filmfare Awards (1982)
|- Best Actor || Amitabh Bachchan ||  
|- Best Supporting Actor || Suresh Oberoi ||  
|- Filmfare Award Best Female Playback Singer || Alka Yagnik ||   || for song Mere angne mein
|-
|}

==References==
 

== External links ==
*  
* http://boxofficeindia.com/showProd.php?itemCat=125&catName=MTk4MC0xOTg5

 
 
 
 
 
 
 