Stuff (film)
 
Stuff is a documentary about the house of Red Hot Chili Peppers guitarist John Frusciante. It was made in 1993 by Johnny Depp and Gibby Haynes, the lead singer of the Butthole Surfers. Dr. Timothy Leary is also present in the video. The films main purpose was to depict the chaos and instability of Frusciantes life. It was once aired in a Dutch TV show called Lola Da Musica, and was released in the 90s as a rare promo VHS. "Untitled #2" from Frusciantes Niandra Lades and Usually Just a T-Shirt is featured on this film with a poem read over it. Also featured in the film is an otherwise unreleased Frusciante song, which contains elements of another song named "Untitled #5" on his album Niandra Lades and Usually Just a T-Shirt.

==Soundtrack==
All songs written by John Frusciante unless noted otherwise:

# Title unknown (unreleased Niandra-era recording)  - 1:14
# "Running Away Into You" - 1:52
# "Untitled #2" (With poem) - 4:18
# "Untitled #3" - 1:48
# "Hallelujah (Leonard Cohen song)|Hallelujah" (Leonard Cohen) (Performed by John Cale) - 2:16

 

 
 
 
 
 
 
 


 