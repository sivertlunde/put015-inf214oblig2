Spin the Bottle (2000 film)
{{Infobox film
| name           = Spin the Bottle
| image          = Spin_The_Bottle_(film).jpg
| caption        = 
| director       = Jamie Yerkes
| producer       = 
| writer         = Amy Sohn
| starring       = Heather Goldenhersh  Jessica Faller  Mitchell Riggs   Kim Winter  Holter Graham
| music          = 
| cinematography = 
| editing        = 
| distributor    = TLA Releasing
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Spin the Bottle is a 2000 American film. Childhood friends meet up for a reunion.

== Cast ==
* Heather Goldenhersh as Rachel
* Jessica Faller as Alex
* Mitchell Riggs as Ted
* Kim Winter as Bev
* Holter Graham as Jonah

== References ==
 
 

== External links ==
*  
*  

 
 
 
 
 
 


 