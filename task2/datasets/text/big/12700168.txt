The Cloth Peddler (1917 film)
{{Infobox film
| name           = The Cloth Peddler
| image          = Arşın Mal Alan.jpg
| image_size     =
| caption        = Opera poster
| director       = Boris Svetlov
| producer       =
| writer         = Uzeyir Hajibeyov
| narrator       = Nirav Mehta
| starring       = Huseynqulu Sarabski, Ahmed Aghdamski, Alakbar Huseynzade, Mirzaagha Aliyev
| music          =
| cinematography = Grigory Lemberg
| editing        =
| distributor    = "Filma" Shareholders Association
| released       = January 3, 1917
| runtime        =
| country        = Azerbaijan Azerbaijani
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Azerbaijani silent Azerbaijani composer Uzeyir Hajibeyov.

==Plot==
  played the female role of Guelchoehra, Asgars beloved]] Azerbaijani tradition restricted him from communicating with the lady as a lover before marriage. So Asgar decides to disguise himself as a mere cloth peddler and the young woman Guelchoehra (Ahmed Aghdamski) falls in love with him. However, she is concerned that her father, Soltan bey (Alakbar Huseynzade) will not allow her to marry a cloth peddler. Young Asgar then reveals himself to her father and asks for her hand in marriage. Seeing that he is indeed a wealthy young man, the father agrees and the two are permitted to marry.

==Cast==
*Huseynqulu Sarabski (Asgar),
*Ahmed Aghdamski (Guelchohra),
*Alakbar Huseynzade (Soltan bey)
*Yunis Narimanov (Jahan khala)
*Mirzaagha Aliyev (Suleyman)
*Alexandra Olenskaya (Asya)
*Hanafi Terequlov (Vali)
*Eva Olenskaya (Telli)

==Release==
The film was remade into a more popular version in 1945 and was performed on the stage in Azerbaijan.
For the 1945 remake see The Cloth Peddler (1945 film).

==See also==
* 

 
 
 
 
 
 
 
 


 
 