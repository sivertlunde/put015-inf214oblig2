Nuits Rouges
{{Infobox film
| name           = Nuits Rouges
| image          = 1974 Nuits rouges.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Georges Franju
| producer       = Raymond Froment 
| writer         = 
| screenplay     = Jacques Champreaux 
| story          = 
| based on       =  
| narrator       = 
| starring       = {{plainlist|
*Gayle Hunnicutt
*Jacques Champreux
*Gert Froebe
*Joséphine Chaplin}}
| music          = 
| cinematography = Guido Renzo Bertoni 
| editing        = Gilbert Natot 
| studio         = {{plainlist|
*Terra Film
*S.O.A.T.   }}
| distributor    = Planfilm 
| released       =  
| runtime        = 105 minutes
| country        = {{plainlist|
*France
*Italy   }}
| language       = French   
| budget         = 
| gross          = 
}}
Nuits Rouges is a 1974 French-Italian crime and thriller film directed by Georges Franju. The film was released in the U.S. in an Dubbing (filmmaking)|English-dubbed version by New Line Cinema under the title Shadowman in 1975.

==Production==
Nuits Rouges was filmed in 1973. Ince, 2005. p. 58  The film is a 100 minute theatrical version of a film originally commissioned for television.  The budget for the film was so modest that Franju had to film all interiors of the film on a studio set. Ince, 2005. p.59 

==Release==
Nuits Rouges was released on November 20, 1974 in France. 

==Reception==
Nuits Rouges features mixed and even mocking reviews from French critics on its release. Ince, 2005. p.60-61  Nuits Rouges was released on DVD in the United Kingdom as part of Eurekas Masters of Cinema series along with another film by Georges Franju, Judex (1963 film)|Judex (1963). 

==Notes==
 

===References===
*  

== External links ==
*  

 

 
 
 
 
 
 
 
 


 
 