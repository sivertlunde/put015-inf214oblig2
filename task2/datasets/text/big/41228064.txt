The Amazing Adventures of the Living Corpse
{{Infobox film
| name           = The Amazing Adventures of the Living Corpse
| image          = The Amazing Adventures of the Living Corpse.jpg
| alt            = 
| caption        = 
| director       = Justin Paul Ritter
| producer       = {{plainlist|
* Justin Paul Ritter
* Morris Ruskin
}}
| writer         = {{plainlist|
* Ryan Plato
* Justin Paul Ritter
}}
| based on       =  
| starring       = {{plainlist|
* Michael Villar
* Marshall Hilton
* Ryan McGivern
}}
| music          = Daniel Iannantuono 
| cinematography = 
| editing        = Justin Paul Ritter 
| studio         = Shoreline Entertainment
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Amazing Adventures of the Living Corpse is a 2012 American animated 3D horror film directed by Justin Paul Ritter, written by Ritter and Ryan Plato, and starring Michael Villar as an intelligent zombie who seeks to protect his son.  It is based on The Living Corpse Exhumed, a comic book by Ken Haeser and Buz Hasson.

== Plot ==
As a zombie apocalypse begins, a group of zombies attack the residents in a house.  When the family recognizes one of the zombies as a family member, the zombie is shocked back to his senses and decides to protect his family.

== Cast ==
* Michael Villar as the Living Corpse

== Release ==
The Amazing Adventures of the Living Corpse premiered at the 2012 San Diego Comic Con.  Four days later, it was picked up for distribution by Anchor Bay Entertainment.   Director Ritter delayed the release of the film several times so that he could fix issues with it.   It was released on home video on June 18, 2013. 

== Reception ==
Gordon Sullivan of DVD Verdict said, "For most viewers the story isnt coherent enough to justify the low-tech animation."   Ryan Keefer of DVD Talk rated the film 2/5 stars and said, "The corpses adventures are less amazing and more mundane."   Todd Dugan of The Digital Bits rated it D- and said that he would not recommend it to either horror or animation fans. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 


 