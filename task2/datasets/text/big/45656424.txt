Lucretia Lombard
{{Infobox film
| name           = Lucretia Lombard
| image          =
| caption        = Jack Conway
| producer       = Warner Brothers Harry Rapf
| writer         = Bertram Milhauser(co-scenario) Sada Cowan (co-scenario)
| based on       = novel, Lucretia Lombard, by Kathleen Norris
| starring       = Irene Rich Monte Blue Norma Shearer
| cinematography = 
| editing        =
| distributor    = Warner Brothers
| released       = December 8, 1923
| runtime        = 7 reels
| country        = USA
| language       = Silent..English intertitles
}} silent film Jack Conway and produced and distributed by Warner Brothers. It starred Irene Rich, Monte Blue and young Norma Shearer, just prior to her signing with MGM.  

This film still survives at George Eastman House and Pacific film Archive.  Now a Public Domain film it can be viewed online at YouTube or Mashpedia.com

==Cast==
*Irene Rich - Lucretia Lombard
*Monte Blue - Stephen Winship
*Marc McDermott - sir Allen Lombard
*Norma Shearer - Mimi
*Alec B. Francis - Judge Winship
*John Roche - Fred Winship
*Lucy Beaumont - Mrs. Winship
*Otto Hoffman - Sandy, Lombards Servant

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 

 