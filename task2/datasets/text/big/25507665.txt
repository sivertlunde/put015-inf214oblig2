The Caller (2008 film)
{{Infobox film
| name = The Caller
| director = Richard Ledes
| image = TheCallerposter.jpg
| writer = Richard Ledes  Alain-Didier Weill 
| producer = Ged Dickersin
| starring = Frank Langella  Elliott Gould  Laura Harring Robert Miller
| cinematography = Stephen Kazmierski
| editing = Madeleine Gavin
| released =  
| runtime = 95 minutes
| country = United States
| language = English
}}
The Caller is a 2008 film by Richard Ledes. The film, which stars Frank Langella, Elliott Gould, and Laura Harring, premiered at the Tribeca Film Festival where it won the Made in NY Narrative Award.  The screenplay was co-authored with Alain Didier-Weill.

==Plot==
Jimmy Stevens (Frank Langella) is a high level executive at an international energy consulting firm. Haunted by the criminal practices of his company, he decides to expose their corruption. He realizes this betrayal will lead to his murder, so he hires out a detective to trail him during his last days.

Unaware that the man who has hired him and the man he is following are one and the same, Turlotte (Elliott Gould) begins a thrilling game of cat and mouse with Stevens and New York City becomes the arena for the uncertain contest. Slowly, the investigation begins to yield clues that come to reveal the larger story of Jimmys mysterious past. 
 
As hints of his childhood in occupied France during WWII are unearthed, a haunting memory surrounding a lone, dying man and the two young boys who witness his last breath becomes the key to the present. As the clock winds down and the hired guns close in on Jimmy, Turlotte puts the puzzle pieces together with just enough time to fulfill his fated duty.

==Cast==
* Frank Langella as Jimmy Stevens
* Elliott Gould as Frank Turlotte
* Laura Harring as Eileen Corey Johnson as Paul Winsail
* Edoardo Ballerini as Teddy
* Helen Stenborg as Jimmys mother

==Reception==
As of June 2014, the film had a score of 13% on Rotten Tomatoes from 16 reviews.  The New York Post gave it a critical review calling it an "opaque oddity" and suggesting it only received a theatrical release because of Frank Langellas Oscar nomination.  Time Out New York gave it 2 stars.  Screen International called it "ponderous". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 