The Yards
 
{{Infobox film
| name           = The Yards
| image          = The Yards Poster.jpg
| caption        = Theatrical release poster James Gray Nick Wechsler Paul Webster Kerry Orent
| writer         = James Gray Matt Reeves James Caan
| music          = Howard Shore
| cinematography = Harris Savides Jeffrey Ford
| studio         = Paul Webster/Industry Entertainment
| distributor    = Miramax Films
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $24 million
| gross          = $889,352 (US)  . Box Office Mojo. Retrieved 2011-02-27. 
}}
 2000 American James Gray. James Caan. 

The setting is the commuter rail yards in New York City, in the boroughs of the Bronx, Queens, and Brooklyn. In the films plot, bribery, corporate crime and political corruption are commonplace in "the yards," where contractors repair railway cars for the city Transit Authority (TA). Rival companies sabotage each others work to win bids. The undercutting leads to murder.

==Plot==
Leo Handler (Mark Wahlberg) rides the subway to his mothers house in Queens, New York, where she throws a surprise party in honor of his parole. His cousin Erica (Charlize Theron) is at the party with her boyfriend Willie Gutierrez (Joaquin Phoenix). Willie takes Leo aside and thanks him for serving time in prison, implying that Leo had taken a fall for their gang of friends.
 James Caan). 

The next day, at the railway car repair company Frank owns, Leo is encouraged to enter a 2-year machinist program and Frank offers to help finance his studies. Needing to work right away, Leo asks about working with Willie for the company but Frank discourages that idea. Leo is advised by Willie not to worry about it, saying Frank tried to get him into a machinist program as well.

At Brooklyn Borough Hall, Willie explains how corrupt the contract system is for repair work on the subway. After a hearing to award contracts, Willie is approached by Hector Gallardo (Robert Montano) about leaving Franks firm for his. Willie brushes him off, taking Leo with him to Roosevelt Island, where he bribes an official in charge of awarding contracts. 

One night, Willie takes Leo to a rail yard, where he and a gang sabotage the work of Gallardos firm in order to lower their quality rating and lessen their ability to get contracts. Leo is told to stand watch while the crew sabotages the train couplings. Willie heads into the yard masters office to pay him off with Knicks tickets, but is told to get his crew off the tracks, Gallardo having brought him $2,000 in cash. The yard master sounds the alarm, which draws a police officer. Terrified of returning to jail, Leo tries to run. When the cop begins to hit Leo with his night stick, Leo beats him into unconsciousness. As he runs off, he sees Willie kill the yard master. 

With the cop in a coma at a hospital, the crew tells Leo that he must murder the officer to prevent him from identifying Leo when he wakes up. If the cop lives, Leos the one who will be killed. Leo flees. When the cop awakes, he identifies Leo as his attacker, triggering a broad manhunt. The police assume Leo is also responsible for the yard masters murder. When they raid his mothers apartment, she has a heart attack, leaving her in an even weaker state. 

Even though Willie has told him to lay low, Leo emerges from hiding to visit his sick mother. Erica is tending to her. She finds out Willie was with him at the yards and realizes it was Willie who actually killed the yard master. She breaks off their engagement.

Erica implores her stepfather Frank to help, but instead Leo realizes that Frank is prepared to kill him. Out of options, Leo turns to Gallardo for protection. With Gallardos lawyers at his side, Leo turns himself in at a public hearing into the rail yard incident and contract corruption. Realizing that Leos testimony is in no ones interest, Frank and Gallardo negotiate a new split of the contracts with the Queens Borough President (Steve Lawrence) in a backroom deal. 

Willie goes to see Erica, trying to win her back. Frank has told him that Erica and Leo had been in love when they were younger, and once were caught having sex. Fearful of his temper and jealousy, Erica triggers the silent house alarm. Willie tries to embrace her, but as she pulls away, he accidentally throws Erica off the second floor landing, causing her to fall to her death. Outside the house, he surrenders to police who have responded to the alarm.

Police enter the hearing to inform Ericas mother Kitty (Faye Dunaway) and stepfather Frank of the incident at the house, and the discovery of Ericas body.

After his stepdaughters funeral, Frank takes Leo aside to promise help in the future. After turning away in muted disgust, Leo joins the grieving Kitty and the rest of the family in an embrace of support. Leo then leaves Queens on the elevated train.

(Note: A non-directors cut of the film ended with Leo testifying against Frank and his company, rejecting the deal. The version currently available on the Miramax DVD in the US is the directors cut.)

==Cast==
*Mark Wahlberg... Leo Handler 
*Joaquin Phoenix... Willie Gutierrez 
*Charlize Theron... Erica Soltz 
*James Caan... Frank Olchin
*Ellen Burstyn... Val Handler 
*Faye Dunaway... Kitty Olchin 
*Steve Lawrence... Arthur Mydanick
*Robert Montano... Hector Gallardo 
*Tony Musante... Seymour Korman
*David Zayas... Officer Jerry Rifkin

==Production==
The film was based on an actual corruption scandal in the mid-1980s involving the director (James Gray)s father.

MTA New York City Transit (the citys Metropolitan Transit Authority) first refused the production companies the right to film at any of its yards because it believed the film portrayed the agency in a bad light.
 207th Street shop on the New York City Transit system and at an abandoned freight yard in Brooklyn.

It was shot in the spring and summer of 1998 but not released until the fall of 2000 due to studio delays.

==Box office==
On a relatively limited release, the film, which had a $24 million budget, took in just $889,352 in the United States and Canada, and $34,684 in Australia. 

==Awards==
===Won===
* National Board of Review Awards (2000):
** Best Supporting Actor: Joaquin Phoenix
* Broadcast Film Critics Association (2001):
** Best Supporting Actor: Joaquin Phoenix

===Nominated===
* Cannes Film Festival (2000): James Gray

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 