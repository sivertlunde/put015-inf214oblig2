Mar Mitenge
{{Infobox film
 | name = Mar Mitenge
 | image = MarMitengefilm.jpg
 | caption = Promotional Poster
 | director = Kawal Sharma
 | producer = Suresh Malhotra
 | writer = 
 | dialogue =  Madhavi Bhanupriya Shakti Kapoor Vinod Mehra Shakti Kapoor Amrish Puri Vinod Mehra Kader Khan
 | music = Laxmikant-Pyarelal
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  1988 (India)
 | runtime = 140 min.
 | language = Hindi Rs 6 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1988 Hindi Indian feature directed by Kawal Sharma, starring Jeetendra, Mithun Chakraborty, Madhavi (actress)|Madhavi, Bhanupriya, Shakti Kapoor, Amrish Puri, Shakti Kapoor, Vinod Mehra and Kader Khan.

==Plot==

Mar Mitenge is an action drama, featuring Jeetendra and Mithun Chakraborty, well supported by Madhavi (actress)|Madhavi, Bhanupriya, Shakti Kapoor, Amrish Puri and Kader Khan.

==Cast==

*Jeetendra ...  Ram Verma
*Mithun Chakraborty ...  Laxman Verma
*Madhavi ...  Radha
*Bhanupriya ...  Jenny
*Shakti Kapoor ...  Manjit Singh
*Amrish Puri ...  Ajit Singh
*Asrani ...  Jalaluddin Mohammed Akbar
*Vinod Mehra ...  Police Inspector Thakur
*Kader Khan ...  Pasha
*Satyendra Kapoor ...  Master Shrikant Verma
*Jayshree Gadkar ...  Mrs. Shrikant Verma
*Chand Usmani ...  Akbars Grandmother
*Yunus Parvez ...  The Groom
*Tiku Talsania ...  Police Inspector G.B. Parab
*Viju Khote ...  Constable P.K. Kale
*Manik Irani ...  Manglu
*Gurbachchan Singh ...  Darshan
*Bob Christo ...  Bob
*Mac Mohan ...  Prakash
*Sudhir ...  Manjits Henchman
*Sunil Dhawan
*Azaad Irani ...  Azad
*Manvi
*Prabhat Agrawal
*Pradeep Singh Rawat ...  Manjits Henchman
*Vijay Pande
*H.L. Pardesi ...  Vegetables Seller
*Master Rinku ...  Young Ram
*Master Bunty ...  Young Laxman
*Master Rajat ...  Young Munna
*Baby Deepali
*Moolchand ...  Jeweller

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aale Re Aale"
| Shailender Singh, Shabbir Kumar
|-
| 2
| "Mar Mitenge"
| Mohammed Aziz, Shailender Singh, Anupama Deshpande
|-
| 3
| "Haye Mere Rabba"
| Anupama Deshpande
|-
| 4
| "Dekho Mera"
| Kishore Kumar, Anuradha Paudwal
|-
| 5
| "Badon Ka Hai Farmana"
| Mohammed Aziz, Shailender Singh, Anuradha Paudwal, Kavita Krishnamurthy
|}

==References==
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Mar+Mitenge

==External links==
*  

 
 
 
 
 

 