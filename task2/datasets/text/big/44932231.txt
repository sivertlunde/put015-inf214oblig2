Solah Satra
 
{{Infobox film
| name = Solah Satra
| image = 
| caption = 
| director = Munna Rizvi
| producer = Kiran Vohra
| starring = Arbaaz Ali Khan Ashwin Verma Ekta Sohini
| music =  Nadeem-Shravan
| studio = Vijay Kiran Films
| released =  
| country = India
| language = Hindi
}}

Solah Satra is a 1990 Indian romance film directed by Munna Rizvi, starring Arbaaz Ali Khan and Ekta Sohini in lead roles, Ashwin Verma and Kulbhushan Kharbanda as antagonists. Ajit Vachani, Sushmita Mukherjee and Kunika play supporting roles. Famous Pakistani pop singer Hasan Jahangir made a special appearance in the song "Aapan Ka To Dil Hai Awara". The film was produced by Kiran Vohra under the Vijay Kiran Films banner.

==Cast==
*Arbaaz Ali Khan as Vicky
*Ekta Sohini as Neha
*Ashwin K Verma as Inspector Vishal
*Kulbhushan Kharbanda as Kedar Nath
*Sushmita Mukherjee as Rosie
*Ajit Vachani as Police Commissioner
*Kunika as Kedar Naths wife
*Hasan Jahangir as Himself (Special appearance in a song "Aapan Ka To Dil Hai Awara")

==Soundtrack==
{{Infobox album
|  Name        = Solah Satra
|  Type        = Album
|  Artist      = Nadeem-Shravan
|  Cover       = 
|  Released    = 1990
|  Recorded    =  Feature film soundtrack
|  Length      =  HMV
|  Producer    = Nadeem-Shravan
|  Reviews     = 
|  Last album  = Baap Numbri Beta Dus Numbri (1990)
|  This album  = Solah Satra (1990)
|  Next album  = Mere Humdaam (1990)
}}
 HMV banner with lyrics penned by Rani Malik and Saeed Rahi. Famous Pakistani pop singer Hasan Jahangir lent his voice for the track "Aapan Ka To Dil Hai Awara". "Ajab Zindagi Ka" by Vinod Rathod was the most successful song of the track.

===Track listing===
{{Track listing
|headline=Solah Satra Disc One: Track listing
|extra_column=Singer(s)
|lyrics_credits=yes 
|length_credits=no
| title1= Aapan Ka To Dil Hai Awara
| extra1= Hasan Jahangir, Chorus
| lyrics1 = Rani Malik
| length1 = N/A
| title2 = Jab Se Tujhe Dekha
| extra2 = Udit Narayan
| lyrics2 = Saeed Rahi
| length2 = N/A
| title3 = Yeh Pandrah Sola Satra
| extra3 = Mohammed Aziz, Sarika Kapoor
| lyrics3 = Saeed Rahi
| length3 = N/A
}}

{{Track listing
|headline=Solah Satra Disc Two: Track listing
|extra_column=Singer(s)
|lyrics_credits=yes
|length_credits=no 
| title1= Meri Nazar Mera Jigar
| extra1= Suchitra Krishnamoorthi
| lyrics1 = Rani Malik
| length1 = N/A
| title2 = Ajab Zindagi Ka
| extra2 = Vinod Rathod
| lyrics2 =
| length2 = N/A
| title3 = Ajab Zindagi Ka (Sad)
| extra3 = Sarika Kapoor
| lyrics3 =
| length3 = N/A
| title4 = Main Karta Hoon Tumse Pyar
| extra4 = Udit Narayan
| lyrics4 = Saeed Rahi
| length4 = N/A
}}

==External links==
* 
* 

 
 
 
 