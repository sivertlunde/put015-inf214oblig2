Game of Seduction
 
{{Infobox film
| name           = Game of Seduction  Une femme fidèle 
| image          = 
| caption        = 
| director       = Roger Vadim
| producer       = Francis Cosne Raymond Eger
| starring       = Sylvia Kristel Nathalie Delon Jon Finch
| screenplay     = Roger Vadim Daniel Boulanger
| based on       = 
| music          = Pierre Porte Mort Shuman
| cinematography = Claude Renoir
| editor         = Victoria Mercanton
| distributor    = Alpha France
| released       =     
| runtime        = 
| country        = France
| language       = French
| budget = 
| gross = 273,394 admissions (France) 

}} drama directed by Roger Vadim.

==Cast==
* Sylvia Kristel as Mathilde Leroy
* Nathalie Delon as Flora de Saint-Gilles
* Jon Finch as Comte Charles de Lapalmmes
* Gisèle Casadesus as Marquise de Lapalmmes
* Marie Lebée as Isabelle de Volnay
* Jean Mermet as The father Anselme
* Anne-Marie Deschodt as Duchesse de Volnay
* Edouard Niermans as Carral
* Annie Braconnier as Victoire
* Katy Amaizo as Pauline
* Serge Marquand as Samson
* Jacques Berthier as Monsieur Leroy

==Accolades==
{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Recipient !! Result
|-
| 1977
| César Award Best Cinematography
| Claude Renoir
|  
|-
|}

==References==
 

==External links==
*  
 
 
 
 
 
 


 
 