Passion of Mind
{{Infobox film
| name = Passion of Mind
| image = Passion of mind.jpg
| image_size =
| caption = Promotional film poster
| director = Alain Berliner
| producer = Gary Lucchesi Ronald Bass  Carole Scotta  Tom Rosenberg David Field
| narrator =
| starring = Demi Moore  Julianne Nicholson  William Fichtner  Sinéad Cusack
| music = Randy Edelman
| cinematography = Eduardo Serra
| editing = Anne V. Coates
| distributor = Paramount Classics
| released =  
| runtime = 105 minutes
| country = United States
| language = English French
| budget = $12,000,000
| gross = $769,272
| preceded_by =
| followed_by =
}} Belgian director Alain Berliner, best known for the arthouse success Ma Vie en Rose.

==Plot==
Marty (Moore) is a high-powered single literary agent in Manhattan. Marie (also Moore) is a widow in Provence with two daughters and a peaceful life. Marty has been seeing a therapist (Peter Riegert) to deal with her vivid dreams of Maries life; when Marie falls asleep she dreams Martys life, but is much less disturbed by it. Each woman is convinced that the other is a figment of her imagination. When Marty meets Aaron (William Fichtner) they become friends and eventually lovers; terrified that her vivid other life means that shes losing her mind, Marty does not want to tell Aaron about it but finally does. Marie, meanwhile, has met William (Stellan Skarsgård); she too is reluctant to tell William about her dreams, particularly since she (as Marty) is falling in love with Aaron, but realizes that she cannot keep such an important part of her life a secret.

The two men react very differently: William is jealous, while Aaron is skeptical but not at all threatened, and wants only for Marty to be happy. Dreams and real life begin to merge when Marie goes on holiday with William to Paris, and Marty wakes up with an ashtray from the hotel on her night stand. Eventually Marty/Marie must come to terms with reality and choose which life is real and which is illusion.

==Cast==
*Demi Moore as Martha Marie / Marty Talridge
*Julianne Nicholson as Kim
*William Fichtner as Aaron Reilly
*Sinéad Cusack as Jessie (as Sinead Cusack)
*Joss Ackland as Dr. Langer, the French Psychiatrist
*Peter Riegert as Dr. Peters, the New York Psychiatrist
*Stellan Skarsgård as William Granther
*Eloise Eonnet as Jennifer Jenny Talridge
*Gerry Bamman as Edward Ed Youngerman

==Reception==
Reviews to Passion of Mind were largely negative from critics, with Rotten Tomatoes giving it a 17% rating based on 35 reviews. Moore was nominated for a Razzie Award for Worst Actress for this film.

==References==
{{Reflist|refs=
 {{Citation
 | author    = New York Times Theater Reviews
 | title     = The New York Times Film Reviews: 1999-2000
 | publisher = Taylor & Francis
 | page      = 319
 | year      = 2002
 | isbn      = 0415936969
 | url       = http://books.google.com/books?id=_E350jJ-Ui4C&pg=PA319
 | postscript= .
}} 
}}

==External links==
*  
*  

 
 
 
 
 
 
 


 