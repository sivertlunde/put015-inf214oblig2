Exorcismus
{{Infobox film
| name           = Exorcismus
| image          = Exorcismus-poster.jpg
| caption        = Spanish film poster Manuel Carballo
| producer       = Julio Fernández
| writer         = David Muñoz
| starring       = Tommy Bastow Stephen Billington Doug Bradley Sophie Vavasseur
| music          = 
| cinematography = Javier Salmones
| editing        = Manuel Carballo
| studio         = Filmax
| distributor    = Filmax
| released       =  
| runtime        = 
| country        = Spain
                         United Kingdom
| language       = English
}} Manuel Carballo and written by David Muñoz,  it stars Sophie Vavasseur and Stephen Billington. 

==Plot== possessed by the Devil. When terrible things start to happen to her friends and family, her parents grudgingly call in the help of her uncle who is a priest to drive out the evil spirits. 

==Cast==
* Sophie Vavasseur as Emma Evans 
* Tommy Bastow as Alex Evans 
* Richard Felix as John 
* Stephen Billington as Christopher 
* Doug Bradley as Priest 
* Isamaya ffrench as Rose Evans
* Lazzaro Oertli as Mark Evans (Emmas brother)

==Production==
In November 2008 Luis de la Madrid was set to direct the Exorcism film  and was, on 22 September 2009, replaced by Manuel Carballo.  The Spanish director David Muñoz wrote the script of the film,  shooting began on 5 October 2009 in the Studios of Filmax. 

==Release==
Screening rights were secured in the UK by E1 Entertainment, in Australia by Vendetta, in Brazil by Playarte, in Italy by Mediafilm, by Gulf Film for the Middle East, Lusomundo for Portugal and CDi acquired the screening rights for Argentina, Paraguay, Uruguay and Chile.  The film will release in the United States as The Possession of Emma Evans,  which is set for 2011. 

== References ==
 

==External links==
* 

 
 
 
 
 
 
 
 
 