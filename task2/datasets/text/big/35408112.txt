Bombay 405 Miles
{{Infobox film
| name           = Bombay 405 Miles
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        =  Brij
| producer       = Brij
| writer         = 
| screenplay     = K.A. Narayan
| story          = 
| based on       =  
| narrator       = 
| starring       = Vinod Khanna Shatrughan Sinha Zeenat Aman
| music          = Kalyanji-Anandji
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film produced and directed by Brij Sadanah|Brij. It stars Vinod Khanna, Shatrughan Sinha and Zeenat Aman in pivotal roles.  It was released on May 02, 1980.

==Cast==
* Vinod Khanna as Kanhaiya
* Shatrughan Sinha as Kishan
* Zeenat Aman as Radha Pran as Masterji
* Amjad Khan as Veer Singh
* Deven Verma as Girdharilal Pawa
* Iftekhar as Ranvir Singh

==Plot==

Ordered to stay away from Delhis border for several years, two convicts, Kishan and Kanhaiya, decide to hitch a ride through a goods train to Bombay. While on this train, they come to the rescue of an elderly male with a female child, Munni. On his last breath, the elderly male asks them to take care of Munni as millions of rupees are involved. Greed overtakes both and they take the child with them in order to locate her next of kin. Unable to find anyone they advertise in the newspapers, and demand a hefty sum from Veer Singh, who answered the advertisement. What Kishan and Kanhaiya dont know that is that officially Munni is already dead, along with her mother, and brother, purportedly killed by their father, Ranvir Singh, when he suspected his wifes fidelity. Kishan and Kanhaiya find themselves immersed in a web of lies and deceit as they try to fathom what the truth really is. But before they could get to the bottom of this matter, their lives themselves are under threat - by a man who will stop at nothing to get what he wants.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Arre De De De Zaraa"
| Kishore Kumar, Mahendra Kapoor
|-
| 2
| "Arre Hogaye Hum Aap Ke Kasam Se"
| Mohammed Rafi, Asha Bhosle
|-
| 3
| "Arre Kya Karoon Kya Karoon"
| Lata Mangeshkar
|- 
| 4
| "Kasam Na Lo Koi Humse"
| Kishore Kumar, Asha Bhosle
|-
| 5
| "Na Na Na Ye Kya Karne Lage Ho"
| Hemlata
|}
== References ==
 

==External links==
* 

 
 
 

 