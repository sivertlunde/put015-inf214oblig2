American Teen
{{Infobox film
| name           = American Teen
| image          = American teen 08.jpg
| caption        = Promotional poster which parodies the 1985 teen film The Breakfast Club
| director       = Nanette Burstein
| producer       = Nanette Burstein Chris Huddleston Eli Gonda Jordan Roberts
| executive producer   = Elisa Pugliese
| writer         = Nanette Burstein
| starring       = Hannah Bailey Colin Clemens Megan Krizmanich Jake Tusing Mitch Reinholt
| cinematography = Laela Kilbourn, Wolfgang Held, Vasco Nunes
| editing        = Nanette Burstein Tom Haneke Mary Manhardt
| distributor    = Paramount Vantage A&E Television Networks|A&E IndieFilms
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $5 million
| gross          = $1,130,270
}}
American Teen is a 2008 documentary film directed by Nanette Burstein (On the Ropes, The Kid Stays in the Picture) and produced by 57th & Irving. It competed in the Documentary Competition at the 2008 Sundance Film Festival, where it received the Directing Award: Documentary.    Following the Sundance Film Festival, the movie was picked up by Paramount Vantage and was released to general cinema July 25, 2008.   

Much of the movie was filmed at Warsaw Community High School in Warsaw, Indiana. Director Nanette Burstein originally reviewed more than 100 different schools in the pre-production process, and ten schools replied, agreeing to participate. After she interviewed incoming seniors at all 10, she chose Warsaw. 

==Primary characters==
* Hannah Bailey - the "rebel". Hannah is an artist, musician, writer and wannabe filmmaker whose liberal views are not a good fit for the conservative small-town culture of Warsaw and she cannot wait until graduation when she can finally leave Indiana. Early in the year, her long-time boyfriend breaks her heart, causing her to miss weeks of school while she recovers from the heartbreak, and later, while she deals with possible embarrassment at school for missing so many days. She dates Mitch for a while during the year, but their relationship causes friction with his clique of friends, and he breaks up with her via text message. Because of this at the years end, she heads to San Francisco to live for a year before attending film school in New York.
* Colin Clemens - the "jock". Colin is a well liked, humorous student whose primary focus during the year is athletics. As the star basketball player for Warsaw, he realizes the year is a make-or-break opportunity to impress scouts and get a scholarship. To not get a scholarship will likely mean enlisting in the military - something his father constantly brings up through the film. He ends up attending Indiana Tech on a Basketball Scholarship, where he obtains a place on the Deans List with a 3.7 GPA.
* Megan Krizmanich - the "queen bee" or "princess". Megan is a popular student in many ways. She is academically talented, popular, and involved in many extracurricular activities. Her ultimate goal is to get into the University of Notre Dame, something that many of her family members have done before her. She is mean, but she is also humanized by the portrayal of her own social and emotional struggles.   It is revealed that after graduation she went on to attend Notre Dame to study Pre-Med and Finance, also mentioning that  she has "mellowed out" and is not much of a mean girl anymore.
* Mitch Reinholt - the "heartthrob". Mitch is popular at school because of his easygoing ways, his good looks, and his sports talent (he is a teammate of Colins). He is the least featured of the five primary students, and is even left off some versions of the films movie poster.  Mitch features prominently into the story when he starts dating Hannah. He ends up attending Indiana University, where he studies Biochemistry and Pre-Med. 
* Jake Tusing - the "geek" or "gamer". Jake is an introverted student whose hobbies are the marching band and video games. He longs to find and keep a girlfriend, and much of his storyline revolves around his dating life. He ends up studying at Lawrence University for a year before attending Vincennes University.

==Criticism== sensationalized feel,  and others have gone so far as to claim that the film feels scripted and the very presence of the cameras take the reality out of the situation. 

==References==
 

==External links==
*  
* 
* 
*  at Los Angeles Times
*  

 

 
 
 
 
 
 
 
 
 