Chakravakam (1974 Telugu film)
{{Infobox film
| name           = Chakravakam
| image          =
| image_size     =
| caption        =
| director       = V. Madhusudhan Rao
| producer       = D. Ramanaidu
| writer         = Koduri Kausalya Devi (story) Acharya Atreya (dialogues)
| narrator       =
| starring       = Shobhan Babu S. V. Ranga Rao Vanisree
| music          = K. V. Mahadevan
| cinematography =
| editing        =
| studio         = Suresh Productions
| distributor    =
| released       = 1974
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
Chakravakam  is a 1974 Telugu Drama film based on the novel titled Chakravakam written by Koduri Kausalya Devi.

==Cast==
* Shobhan Babu
* Vanisree
* Chandrakala
* S. V. Ranga Rao
* Nagabhushanam
* Allu Ramalingaiah Krishna Kumari
* Rajasulochana

==Soundtrack==
* "Ee Nadila Naa Hrudayam Parugulu Teestundi" (Singers: V. Ramakrishna and P. Susheela)
* "Kothaga Pellaina Kurravaniki Pattapagale Tondara" (Singers: V. Ramakrishna and P. Susheela)
* "Veenaleni Teeganu Neevuleni Bratukunu" (Singers: V. Ramakrishna and P. Susheela)
* "Veenalona Teegalona Ekkadunnadi Ragamu" (Singer: P. Susheela)

==Boxoffice==
The film ran successfully for 200 days in Hyderabad, India|Hyderabad. 

==References==
 

==External links==
*  

 
 
 
 


 