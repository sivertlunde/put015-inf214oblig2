Love Crimes (1992 film)
{{Infobox film
| name = Love Crimes
| image = 1992 love crimes.jpg
| caption = Lizzie Borden
| producer = Lizzie Borden Rudy Langlais
| writer = Screenplay: Allan Moyle   Story: Allan Moyle
| starring = Sean Young Patrick Bergin Arnetia Walker James Read
| music = Graeme Revell
| cinematography = Jack N. Green
| editing = Nicholas C. Smith Mike Jackson
| distributor = Miramax Films
| released =  
| runtime = 84 minutes (theatrical) 91 minutes (unrated directors cut)
| country = United States
| language = English
| budget = $8,500,000
| gross = $2,287,928
}}
 thriller directed Lizzie Borden starring Sean Young as an assistant district attorney who tries to seduce and apprehend a psychopath (Patrick Bergin).

The screenplay is by Allan Moyle and Laurie Frank, based on a story by Moyle. Young earned a Razzie Award nomination as Worst Actress for her performance.

==Plot==
Assistant district attorney Dana Greenway conspires with police lieutenant Maria Johnson to go after a serial sexual predator who identifies himself to his victims as "David Hanover," a distinguished photographer.

Greenway goes undercover, changing her appearance and passing herself off as a repressed schoolteacher. She eventually encounters Hanover, who seduces her, photographs her nude and causes Lt. Johnson to believe that Greenway might actually have fallen under his spell.

Flashbacks to her troubled childhood, including abuse from a father who locked her in a closet, haunt Greenway as she attempts to come to her senses and get the better of Hanover, who clearly intends to humiliate and then kill her.

==Cast==
* Sean Young as Dana Greenway
* Patrick Bergin as David Hanover
* James Read as Stanton Gray
* Arnetia Walker as Lt. Maria Johnson

==Reception==
Love Crimes received negative reviews from critics and flopped at the box office. The film is listed in Golden Raspberry Award founder John Wilsons book The Official Razzie Movie Guide as one of the 100 Most Enjoyably Bad Movies Ever Made. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 


 