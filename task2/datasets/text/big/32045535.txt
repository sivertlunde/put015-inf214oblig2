Paradise Kiss (film)
{{Infobox film
| name = Paradise Kiss
| image = Paradise Kiss Movie Poster.jpg
| caption = Film Poster
| director = Takehiko Shinjo
| producer = Shinzō Matsuhashi Toshiya Nomura
| screenplay = Kenji Bando
| based on =  
| starring = Keiko Kitagawa Osamu Mukai Yusuke Yamamoto
| music = Yoshihiro Ike
| cinematography = Hikaru Yasuda
| editing = Yoshifumi Fukazawa Warner Bros. 20th Century King Records Studio Swan Hori Agency Dimension Blue Yahoo! Japan Shodensha
| distributor = Warner Bros. Pictures
| released =  
| runtime = 116 minutes
| country = Japan
| language = Japanese
| budget =
| gross =
}}
 the same name. The film is directed by Takehiko Shinjo, and it stars actors Keiko Kitagawa, Osamu Mukai, Yusuke Yamamoto.   

==Cast==
* Keiko Kitagawa as Yukari Caroline Hayasaka
* Osamu Mukai as Jouji George Koizumi
* Yusuke Yamamoto as Hiroyuki Tokumori
* Shunji Igarashi as Isabella
* Kento Kaku as Arashi Nagase
* Aya Omasa as Miwako Sakurada
* Natsuki Kato as Kaori Asou Hitomi Takahashi as Yukino Koizumi
* Shigemitsu Ogi as Joichi Nikaido
* Michiko Hada as Yasuko Hayasaka
* Hiroyuki Hirayama as Seiji Kisaragi
* Tomohisa Yuge as Yamaguchi, a cameraman
* Nicole Ishida as Kaoris model

==Trivia== Fuji TV drama Buzzer Beat in 2009.
*Osamu Mukai made 3 movies and 18 TV Dramas prior to Paradise Kiss in 2011.

==References==
 

==External links==
*    
*  

 
 

 

 