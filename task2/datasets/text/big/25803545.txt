Exam (film)
{{Infobox film
| name           = Exam
| image          = Exam poster.jpg
| caption        = Theatrical poster
| director       = Stuart Hazeldine
| producer       = Stuart Hazeldine Gareth Unwin
| writer         = Stuart Hazeldine Simon Garrity
| starring       = Adar Beck  Chris Carey  Gemma Chan Nathalie Cox John Lloyd Fillingham Chuk Iwuji Luke Mably Pollyanna McIntosh Jimi Mistry Colin Salmon
| music          = Stephen Barton Matthew Cracknell
| cinematography = Tim Wooster
| editing        = Mark Talbot-Butler
| studio         = Bedlam Productions Hazeldine Films
| distributor    = Independent  United Kingdom|ref2=   }}
| runtime        = 101 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British psychological Chris Carey, Telugu language in India as Key in 2011.  

==Plot==
The film is set in present-time United Kingdom in an alternate history. Eight candidates dress for what appears to be an employment assessment exam. They enter a room and sit down at individual desks. Each desk contains a paper with the word "candidate", followed by a number, from one to eight. The Invigilator, a representative of the company named DATAPREV, explains that the exam takes 80 minutes and consists of one question only, and that there are three rules: they must not talk to the Invigilator or the armed guard at the door, spoil their paper, or leave the room. Not obeying the rules will result in disqualification.

After the clock starts, it turns out that the question papers are blank. One candidate is immediately removed from the room for spoiling her paper by writing on it. The seven remaining candidates realize they can talk to each other and work together. "White", who is arrogant and rude, suggests nicknames based on physical appearance: Black, White, Brown, Blonde, Brunette, and Dark, refer to their skin and hair colours, and Deaf is for one candidate who does not pay attention to the others. The candidates use the lights, bodily fluids, and fire sprinklers to try to reveal a hidden question on the paper, with no luck. White takes control of the group but really works against them, engineering the disqualifications of Brunette and Deaf. White then taunts the others, saying he knows the question but will not tell them.

It is revealed that the company in question is responsible for a miracle drug which a large part of the population are dependent upon after a viral pandemic. Black subdues White, ties him to a chair, and calls him a distraction to the group. White says he is infected with the virus and needs his medication, but the others do not believe him. Brown suggests Dark is actually a plant from the company, while demonstrating much knowledge about the internal workings of the company. This leads Brown to torturing her into revealing that she works for the company but is still applying for the job like everyone else. Just then, White goes into convulsions, which proves he has the disease. Dark asks the unseen Invigilator for help and is disqualified.

Brown tries to destroy the medication that White brought for himself, but Blonde retrieves it and gives it to White. The others release White and demand to know the question, but he suggests the explanation is that there is no question—the company is going to hire the last remaining candidate. This triggers a fight between White and Black for the guards gun, which White wins. White forces Brown to leave the room at gunpoint. However, before Blonde leaves, she turns off the voice-activated lights, which allows Black to attack White.

When the lights are turned on, Black is dead from a gunshot, and Blonde is hiding in the hallway, with just one foot still inside the room. Before White can kill her, the clock runs to zero. When White addresses the Invigilator and proclaims himself the best candidate, he is disqualified; it is revealed that Deaf had earlier set the countdown forward by a few minutes. With only Blonde left, she remembers that Deaf had been using his glasses and a piece of broken glass with an exam paper earlier but had been ignored. Taking the abandoned glasses, she finds the phrase "Question 1." on the exam paper in minuscule writing.

Blonde realises that Question 1 refers to the only question asked of them by the Invigilator: "Any questions?" She answers, "No." The Invigilator and Deaf enter; the Invigilator reveals Deaf is actually the CEO of the company and inventor of the cure for the virus, which can also heal any wound. The bullet that hit Black contained this cure, and he awakens, healed. With the drug about to be released to the public, the company needed to hire someone who was not only able to make tough choices but who is also compassionate, which Blonde had demonstrated throughout the exam. Blonde passes the exam and accepts her new job.

==Cast==
* Adar Beck as Dark 
* Gemma Chan as Chinese Girl
* Nathalie Cox as Blonde
* John Lloyd Fillingham as Deaf Chukwudi Iwuji as Black (as Chuk Iwuji)
* Pollyanna McIntosh as Brunette
* Luke Mably as White
* Jimi Mistry as Brown
* Colin Salmon as the invigilator Chris Carey as the guard

==Production==
After seeing some of his friends films fail due to studio interference, Stuart Hazeldine decided that he wanted full control over his feature debut.     The original story involved an exam at a school, but Stuart Hazeldine changed it to be a job interview.  The ending is also Hazeldines creation, as the original story didnt have one.  Hazeldine wanted to separate the characters by race, culture, gender, and, especially, worldview.     The films pandemic was influenced by contemporary fears of bird flu and distrust of pharmaceutical companies.  Originally, the script had more science fiction elements, but Hazeldine stripped them out to keep the film grounded.  About the twist ending, Hazeldine said he wanted the film to be about more than just the twist, and he tried to appeal to audiences who seek a story about human nature. 

==Release==
 

The film premiered in June 2009 as part of the Edinburgh Film Festival  and was then part of the Raindance Film Festival 2009.  It was released in UK cinemas on 8 January 2010. 

On 11 February 2010, IFC Films acquired the rights for the US release, where it was released as part of the Santa Barbara International Film Festival.    The DVD and Blu-ray were released in the UK on 7 June 2010.    There was no theatrical release in the US, but IFC Films released the film via video on demand on 23 July 2010  and on DVD on 16 November 2010. 

On 4 September 2012, a stage adaptation of the film opened in Manchester. 

==Reception== The Telegraph 12 Angry El Método (The Method), a 2005 Spanish film. 

===Awards===
* Won the Independent Feature Award at Santa Barbara Film Fest. 
* Won the Bronze Hitchcock at the Dinard British Film Festival 
* Nominated for Best UK Feature at Raindance. 
* Nominated for a BAFTA for Outstanding Debut. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 