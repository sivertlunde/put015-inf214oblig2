Sa 'yo Lamang
{{Infobox Film
| name           = Sa yo Lamang
| image          = SAYOLAMANGPOSTER.png
| caption        = Theatrical movie poster
| director       = Laurice Guillen
| producer       = Charo Santos-Concio Malou Santos
| writer         = Ricky Lee Ralph Jacinto Quiblat Anna Karenina Ramos
| starring       = Lorna Tolentino Christopher de Leon Bea Alonzo Diether Ocampo Coco Martin Enchong Dee Shaina Magdayao Zanjoe Marudo Lauren Young Empress Schuck Miles Ocampo
| music          = Sayo Lamang (Performed by Juris Fernandez)
| cinematography = Lee Briones Meily
| editing        =
| distributor    = Star Cinema
| country        = Philippines
| awards         =
| released       =    
| runtime     = 105 minutes Tagalog
| preceded_by    =
| followed_by    =
| gross          = ₱50,230,216 .   
}} Filipino religious-family drama film produced and released by Star Cinema.  It is Star Cinemas offering for its 17th year anniversary. 

==Synopsis==

From the director who gave us Tanging Yaman, Laurice Guillen, is back with another heart-warming family drama which teaches us about love, faith and life. Sa ‘Yo Lamang stars Christopher De Leon, Lorna Tolentino, Bea Alonzo, Coco Martin, Enchong Dee, Miles Ocampo, Shaina Magdayao and Empress Schuck.

==Plot==
Dianne seems to have a happy family.She and her mom just bought a house for them,her younger siblings are doing great at school, and her boyfriend of two years is already preparing for their future together. But everything in her life shatters when her dad, Franco, returns after ten years of being with his mistress. More than the irritating presence of Franco, what hurts Dianne the most is how her mom wholeheartedly accepts Franco back to live with them again, and how her siblings allow him to be their father again, as if he never left. Confused and hurt, Dianne does everything to throw Franco out of their lives. But as she succeeds with her plans, she is shocked to learn that their family’s problem is more than the return of the man who has hurt her in the past.And thus the start of the journey of Dianne and her family as they discover their family’s secrets.

==Cast and characters==

Main Cast
*Christopher De Leon as Franco Alvero - Franco is the father of Dianne, Coby, James and Lisa and also Amandas husband.
*Lorna Tolentino as Amanda Alvero - Amanda is the mother of Dianne, Coby, James and Lisa and also Francos wife.
*Bea Alonzo as Dianne Alvero - Dianne is Amanda and Francos first daughter and first child also.
*Coco Martin as Coby Alvero - Coby is Amanda and Francos first son and second child.
*Enchong Dee as James Alvero - James is Amanda and Francos 2nd son. He is Amandas 4th child, however, he is Francos 3rd child.
*Miles Ocampo as Lisa Alvero - Lisa is Amandas 3rd daughter, Francos 4th child and Amandas 5th child.

Supporting Cast
*Shaina Magdayao as Karen - Karen is Cobys love interest.
*Zanjoe Marudo as John - John is Diannes future husband.
*Diether Ocampo as Paul - Paul is Diannes ex-boy friend.
*Lauren Young as Lorraine - Lorraine is James love interest.
*Empress Schuck as Agnes - Agnes is Amandas 2nd daughter but 3rd child.

Special Participation
*Dominic Ochoa as Father Eric - He is the Alvero family friend.
*Igi Boy Flores as Kennedy - He has a crush on Lisa.

==Reception==
===Reviews=== 29th Luna Awards. 

===Soundtrack===
The official theme song titled Sa Yo Lamang, is sang by Juris together with the Ars Cantica Ensemble. The official music video was directed by Cathy Garcia-Molina.

===International screenings===
The film had international screenings on September 17, 2010 in select cities in the United States such as Las Vegas, NV, San Francisco, CA, Los Angeles, CA, San Diego, CA, Seattle, WA, Honolulu, HI, Bergenfield, NJ, Milpitas, CA, Chicago, IL. It will also have screenings in Ontario, Canada, Alberta, Vancouver, Canada, and Guam.

===Box Office===
The film grossed ₱20 million on its first week falling at number two behind Despicable Me. Within the films 4th week, it had grossed a total of ₱48 million. 

==Awards==
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2011
| rowspan="1" align="left"| GMMSF Box-Office Entertainment Awards  Prince of Philippine Movies 
| align="center"| Coco Martin
|  
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 