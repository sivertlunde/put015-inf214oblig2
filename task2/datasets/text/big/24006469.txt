The Boys from St. Petri
{{Infobox film
| name           = The Boys from St. Petri
| image          = 
| caption        = 
| director       = Søren Kragh-Jacobsen
| producer       = Mads Egmont Christensen Hedda Craner Bent Fabricius-Bjerre
| writer         = Søren Kragh-Jacobsen Bjarne Reuter
| starring       = Tomas Villum Jensen
| music          = 
| cinematography = Dan Laustsen
| editing        = Leif Axel Kjeldsen
| distributor    = 
| released       = 11 October 1991
| runtime        = 111 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

The Boys from St. Petri ( ) is a 1991 Danish drama film directed by Søren Kragh-Jacobsen. It was screened out of competition at the 1992 Cannes Film Festival.   

The film is inspired by the activities of the Churchill Club, but the actual plot is fiction.

==Cast==
* Tomas Villum Jensen - Lars Balstrup
* Morten Buch Jørgensen - Gunnar Balstrup
* Nikolaj Lie Kaas - Otto Hvidmann
* Christian Grønvall - Søren Brinck
* Karl Bille - Olaf Luffe Juhl
* Søren Hytholm Jensen - Anders Møller
* Joachim Knop - Aage Terkilsen
* Xenia Lach-Nielsen - Irene
* Bent Mejding - Provst Johannes Balstrup
* Helle Merete Sørensen - Fru Ingeborg Balstrup
* Ida Nepper - Gerda Balstrup
* Philip Zandén - Jacob Rosen Rosenheim
* Solbjørg Højfeldt - Fru Hvidmann
* Tilde Maja Frederiksen - Lis Hvidmann
* Amalie Ihle Alstrup - Kylle Hvidmann
* Sofie Wandrup - Bitten Hvidmann

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 