Mortdecai (film)
 
{{Infobox film
| name = Mortdecai
| image = Mortdecai poster.jpg
| alt = 
| caption = Theatrical release poster
| director = David Koepp
| producer = {{Plainlist|
* Christi Dembrowski
* Johnny Depp
* Andrew Lazar}}
| screenplay = Eric Aronson
| based on =  
| starring = {{Plainlist|
* Johnny Depp
* Gwyneth Paltrow
* Ewan McGregor
* Olivia Munn
* Paul Bettany
* Jeff Goldblum}}
| narrator = Johnny Depp
| music = {{Plainlist|
* Geoff Zanelli
* Mark Ronson}}
| cinematography = Florian Hoffmeister
| editing = Jill Savitt
| studio = {{Plainlist|
* Infinitum Nihil
* Mad Chance Productions
* Odd Lot Entertainment}} Lionsgate
| released =  
| runtime = 106 minutes  
| country = United States
| language = English
| budget = $60 million    
| gross = $30.4 million 
}} action comedy film directed by David Koepp and written by Eric Aronson. The film is adapted from the book anthology Mortdecai (specifically its first installment Dont Point That Thing At Me) written by Kyril Bonfiglioli. It stars Johnny Depp in the title role and also features Gwyneth Paltrow, Ewan McGregor, Olivia Munn, Paul Bettany, and Jeff Goldblum. The film was released by Lionsgate on January 23, 2015. Mortdecai was a box office flop and was critically panned.  

==Plot==
 
Lord Charlie Mortdecai is an eccentric art dealer with a curly mustache. He is at a casino in Hong Kong meeting with a gangster named Fang to sell a rare vase. Mortdecai is about to collect the money when Fang reminds him that during their last transaction, Mortdecai sold him a piece of art for $3 million when it was only worth $1 million. As payback, Fang wants to collect one of Mortdecais fingers. One of Fangs men tries to do the job when Mortdecais faithful manservant/thug Jock steps in and beats the goon. A shootout ensues, forcing Mortdecai and Jock to flee. Recently, Mortedcai and his wife Johanna have come into debt. After returning to his home in London, Johanna sees the mustache her husband has grown and she is utterly repulsed by it, gagging whenever he kisses her. She ponders what they will do about their financial concerns. Here, we learn how much Mortedcai loves his wife, and he also mentions a bit about Jock, specifically how much sex he has and how its gotten him in hot water (i.e., sleeping with a farmers daughter).

A woman named Bronwen is working on a painting by Francisco Goya when she is shot in the back with an arrow. A thief swipes the painting and leaves her home, when he is knocked out by yet another thief. Inspector Alistair Martland is put on the investigation to recover the painting. He visits Mortdecai to ask for his services. Martland has been in love with Johanna since their college years, and he harbors a slight vendetta against Mortdecai after catching them sleeping together. He informs Mortdecai and Johanna of the theft, with their prime suspect being a criminal named Emil Strago. Mortdecai agrees to help on the condition of receiving 10% in payment after they plan on selling the painting to an American dealer by the name of Milton Krampf.

Mortdecai first visits an art aficionado named Sir Graham to get clues. Then he visits Spinoza, an art smuggler who had been screwed over by Mortdecai in the past. While Spinoza angrily yells and swears at Mortdecai, Emil shows up and shoots at the men. Spinoza is killed, while Mortdecai and Jock manage to escape. Meanwhile, Johanna does her own investigation. She meets with a man known as The Duke, who informs her that the painting was taken by a friend of his from the war. It supposedly has codes written on the back that lead to the location of a hidden stash of Nazi gold.

A while later, Mortdecai is kidnapped by Russian thugs working for a man named Romanov, with Jock in pursuit. They, like Strago, think that Mortdecai has the painting, and threaten to electrocute Mortdecais testicles until he manages to escape out of a window (using one of Romanovs thugs as a makeshift crash-mat) before subsequently driving off into the night on a motorbike driven by Jock. After returning to London, Martland, wanting some private time with Johanna, forces Mortdecai to go to America to meet with Milton Krampf, under the guise of finalizing the debt-easing sale of Mortdecais beloved Rolls-Royce to the American, so as to track down the stolen painting Krampf is rumoured to possess. Mortdecai and Jock arrive in Los Angeles, and upon meeting Krampf and his apparently nymphomaniac daughter Georgina at their house, discover that the stolen painting has been stashed in the Rolls Royce the entire time, hidden there by the mechanic Spinoza (who stole it from Emil shortly after the murder of Bronwen) at the behest of Krampf, who only agreed to buy Mortdecais car in order to use it as a smuggling device for the Goya painting. Krampf informs Mortdecai that he intends to display the painting at a party later on, inviting him to attend. As a result of this development, Jock proposes that during the party they sneak into the room where its being kept and swipe the painting for themselves when nobody is looking. 

Later that night at the party, it is revealed that Georgina is in league with Emil, the latter getting the former to distract Mortdecai by attempting to seduce him so as to steal the painting first). The attempt almost works, until the pair are caught in a semi-compromising position by Johanna, who has also shown up to the party with Martland. Mortdecai, seeing Jock in the midst of their own theft attempt, flees the scene and goes to help his man servant. They find Krampf has been murdered by Emil, who has nabbed the painting. Martland and Johanna come in with Martland aiming a gun at Emil. However, it turns out Georgina is in cahoots with Emil, and they are also lovers. They take the painting and run for it, with the other four chasing after them. The villains go to a motel and try to find the codes with lighter fluid and a blowtorch. In the chaos, Emil spills the fluid and ignites it with the torch near a conveniently placed pile of petrol. Everybody runs as the motel room explodes, destroying the painting with it. Johanna informs Mortdecai that this painting was a fake. The real one is with The Duke, which he actually did tell her when he invited to show it to Johanna in the bathroom (she thought he was talking about showing her his penis). The couple return to London to find The Duke, only to learn that he has died. His widow does allow them to go to the bathroom and find the painting.

The couple puts the painting up for an auction later that evening, hidden as a fake under a familiar painting owned by Mortdecai. Fang is informed of this and goes down to collect Mortdecais finger. Romanov and his goons also show up to the auction with Sir Graham. Mortdecai and Jock subdue both Romanovs and Fangs goons. Mortdecai runs to the auction, where Emil has been seated next to Johanna with a knife. Mortdecai arrives and makes a high bid until Romanov outbids him. The painting is sold to him, giving Johanna a moment to pepper-spray Emil in the face, and he is apprehended by Martland.

We learn that Romanov purchased the wrong fake, and he decides to have Sir Grahams balls. Martland accepts that he will never be with Johanna, and he leaves her be. Mortdecai has collected some money, but the rest is paid off for taxes he skipped. In the end, Mortdecai and Johanna continue to love each other and remain faithful through thick and thin. He agrees to finally shave his mustache if it will make her happy, because he would do anything for her. Johanna decides to let it stay. She kisses him passionately, until moments later when she gags, forcing Mortdecai to follow suit.

==Cast==
* Johnny Depp as Charlie Mortdecai
* Ewan McGregor as Inspector Martland
* Gwyneth Paltrow as Johanna Mortdecai
* Paul Bettany as Jock Strapp
* Olivia Munn as Georgina Krampf 
* Jeff Goldblum as Milton Krampf 
* Ulrich Thomsen as Romanov
* Paul Whitehouse as Spinoza Michael Byrne as the Duke
* Nicholas Farrell as Auctioneer

==Production==
Principal photography and production began in London on October 21, 2013.   

Parts of the film were shot on location at Hedsor House in the UK,    where Depp, Munn, and Bettany filmed scenes in Hedsor Houses Boudoir and Bridal Suites.

Scenes were also shot on location at the National Art Library in the Victoria and Albert Museum in London.   

==Release==
On April 23, 2014, Lionsgate announced that the film would be released on February 6, 2015.  On September 24, 2014, the release date was shifted to January 23, 2015. 

===Marketing and promotion===
A photo from the film featuring Depp was revealed on May 8, 2014.  Four character posters - featuring Depp, Paltrow, McGregor, and Munn with mustaches - were released in November 2014.
 The Gambler. 

==Reception==

===Box office===
Along with being critically panned, Mortdecai was also a box office flop, earning only $4.2 million in the United States on its opening weekend, finishing in 9th.  

At the end of its theatrical run, the film grossed $7.7 million in North America and $22.7 internationally for a worldwide total of $30.4 million, against a budget of $60 million. 

===Critical response===
 
Mortdecai was widely panned by critics. On Rotten Tomatoes, the film holds a rating of 13%, based on 84 reviews, with an average rating of 3.4/10. The sites consensus reads, "Aggressively strange and willfully unfunny, the misguided Mortdecai sounds a frightfully low note in Johnny Depps post-Pirates filmography."  On Metacritic, the film has a score of 27 out of 100, based on 21 critics, indicating "generally unfavorable reviews".  	

During the first month of 2015, Christopher Rosen of The Huffington Post said that "Mortdecai seems destined to be rated as the worst film of 2015, and deservedly so." 

==Future==
On February 7, 2014, Lionsgate announced that they had plans for a film series with the 
character. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 