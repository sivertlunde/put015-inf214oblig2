Killing Birds
{{Infobox film
| name                 = Zombie 5: Killing Birds 
| image                = Zombie 5 DVD.jpg
| caption              = DVD Cover
| director             = Claudio Lattanzi
| producer             = Joe DAmato Claudio Lattanzi
| writer               = Claudio Lattanzi Daniele Stroppa Sheila Goldberg
| starring             = Robert Vaughn
| cinematography       = Joe DAmato
| music                = Carlo Maria Cordio
| editing              = Kathleen Stratton
| studio               = Filmirage
| distributor          = Shriek Show
| released             =  
| runtime              = 91 minutes Italian
| country              = Italy
| budget               = 
}}
 Italian horror movie released in the United States under the title Zombie 5:  Killing Birds; aka Raptors.    Despite this title, zombies only feature in the last few minutes of the movie, and only one character is attacked by birds.

==Plot==
Fred Brown (Robert Vaughn), a soldier, returns to Louisiana from the Vietnam war to find his wife (Brigitte Paillet) in bed with another man, and kills them both and his parents (Ellis and Nona Paillet), only sparing his son. While he is cleaning his knife, a falcon attacks him and tears out his left eye and blinds him in the other. He ends up at a hospital where he says goodbye to the child before he is taken into foster care.

Twenty years later, a small group of college seniors, Steve Porter (Timothy W. Watts), Mary (Leslie Cummins), Paul (James Villemaire), Anne (Lara Wendel), Rob (James Sutterfield), Jennifer (Lin Gathright) and a local cop, Brian (Sal Maggiore Jr.), are assigned to locate the Green billed woodpecker, a rare species which will be officially extinct by 1992, four years after the current point of the movie. Steven and Anne meet with Fred Brown, who gives them a packet of information on the bird they’re searching for. The group makes the former home of Brown their “base” for as long as they’re searching, which turns up nothing on their first day other than a rotting corpse in an abandoned truck.

Upon settling in the house, strange occurrences, such as doors closing on their own and Steve seeing things that aren’t there, begin. These become more prevalent as time goes by, including a nightmare where Brown kills Mary in a similar fashion to how he killed his wife. While exploring the aviary she discovered earlier, Jennifer is chased into a tool shed by a zombie, only to be beaten to death by another that was hiding in there. Brian is burnt to death in a freak accident caused by the fuel leaking from the generator. Mary, who suspects it to be the man in her dream who killed Jennifer, finds her corpse.

The survivors make it to the camper only to realize that Brian had the keys. While Rob makes an attempt to hot wire the camper, a zombie attacks and kills Mary. They quickly abandon the camper to make a last stand in Brown’s house while Brown suffers a mild heart attack when he realizes what is happening. While trying to fix the generator, a door slams open and blows dust into the room, frightening Rob into running into the generator. His necklace is caught in the generator, and he is strangled to death when the wire cuts into his throat. Paul, who witnessed the whole thing and didn’t help Rob, tells Steve and Anne that it was the zombies who killed him.

It is revealed by an email sent by Brown to Rob’s computer that Steve is the baby Brown spared twenty years earlier. A zombie breaks into the house, and nearly kills Anne, however Steve saves her at the cost of a shotgun he found. Steve, Paul and Anne hide in the attic, where a fourth zombie attacks and results in the death of Paul when he panics.

In the morning, the zombies’ attacks have ceased, allowing Steve and Anne to escape the house, meeting Brown in the process. He tells them to get out and that the zombies were never after Steven and Anne, only those of their group that showed fear of them. The credits begin to roll as Steve and Anne look away in despair when they hear Brown’s scream emanate from the house. Their fates are not revealed.

==Cast==
*Robert Vaughn as Dr. Fred Brown
*Lara Wendel as Anne
*Timothy W. Watts as Steve Porter
*Leslie Cummins as Mary
*James Villemaire as Paul
*Sal Maggiore Jr. as Brian
*James Sutterfield as Rob
*Lin Gathright as Jennifer
*Brigitte Paillet as Steves Mother
*Ellis Paillet as Steves Grandfather
*Nona Paillet as Steves Grandmother
*John H. Green as Dr. Green

==Production==
The filming took place in and around New Orleans, Louisiana. It was the first time a Zombi film had been shot in the United States since Lucio Fulcis Zombi 2. The film was shot in rural swamps outside of New Orleans with certain scenes being shot in Fairview-Riverside State Park.

Robert Vaughn was originally supposed to wear blue contact lenses and his eyes were to be digitally removed in post-production; however, the contact lenses proved too painful and they kept falling out. Joe DAmato compromised on a crude prosthetic and dark glasses.

==Distribution==
The film was released in Italy in 1988 under the title Uccelli Assassini; however, after   made money upon its US release (despite harsh critical reaction to the film), Shriek Show released it as Zombie 5: Killing Birds.

Despite the title of Zombie 5: Killing Birds in the US, the film was not included in the Zombie Flesh Eaters series in the UK as  , under the title Zombie 5: The Killing Birds.

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 