Chhaya (film)
{{Infobox film
| name = Chhaya
| image = Chhaya.jpg
| director = Hrishikesh Mukherjee
| producer = A.V. Meiyappan
| writer = Sachin Bhowmick Rajendra Krishan
| starring = Sunil Dutt Asha Parekh Nirupa Roy Nasir Hussain
| music = Salil Choudhury
| lyrics = 
| cinematography = Jaywant Pathare T.B. Seetaram
| editing = R.G. Gope
| released = 
| runtime = 
| country = India
| language = Hindi
| budget = 
| preceded_by = 
| followed_by = 
}}
Chhaya (English: Shadow) is a Bollywood film directed by legendary director Hrishikesh Mukherjee.  It released in 1961. The star cast include Sunil Dutt and Asha Parekh.  Nirupa Roy won the Filmfare Best Supporting Actress Award. 

==Plot==
A young and very attractive girl, Sarita, lives with her wealthy father, aunt, cousin and maid. Her father hires a tutor to teach her with essay writing, named Arun. Arun and Sarita slowly fall in love, especially when she finds out hes a famous poet by the name of Rahee. Her father is adamantly against the union because Arun is poor. He wants Sarita to marry the son of a wealthy man, Moti Lal. Furthermore, Saritas aunt wants her to marry her nephew Ramu, also known as Romeo and gets him hired as Saritas music tutor. Moti Lal then gets a letter saying that Sarita is not her fathers legitimate daughter, and her birth, religion and caste are called into question.

==Cast==
* Sunil Dutt
* Asha Parekh
* Nirupa Roy
* Nasir Hussain
* Lalita Pawar
* Achala Sachdev

==Music==
The music for this movie was composed by Salil Chaudhury.

* Itna Na Mujhse Too Pyar Badha - Talat Mehmood & Lata Mangeshkar

* Itna Na Mujhse Too Pyar Badha (sad) - Talat Mehmood
These 2 songs are inspired by Mozarts Symphony No. 40 in G Minor KV 550
 Mukesh & Lata Mangeshkar

* Chham Chham Nachti Aai Bahar - Lata Mangeshkar

* Ankhon Mein Masti Sharab Ki - Talat Mahmood


*Ansoo Samajh Ke Kyon Mujhe - Talat Mehmood

*Ya Keh De Hum Insaan Nahin - Mohd Rafi

==References==
 

==External links==
* 

 
 

 
 
 

 