The Red Chapel
{{Infobox film
| name           = The Red Chapel
| image          = The Red Chapel.jpg
| caption        = 
| director       = Mads Brügger
| producer       = Peter Engel Peter Aalbæk Jensen
| writer         = 
| narrator       = 
| music          = 
| cinematography = René Johannsen
| editing        = René Johannsen Zentropa Productions
| distributor    = Kino Lorber Films
| released       =  
| runtime        = 88 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| gross          = 
}}
The Red Chapel ( ) is a 2009 Danish  . The film turns deeply emotional as Jacob Nossell has spastic paralysis and North Korea has been accused of disposing of the disabled. The film won Best Nordic Documentary at Nordisk Panorama 2009 and Best Foreign Documentary at the 2010 Sundance Film Festival where it was included in the Official Selection. It is filmed and edited by René Johannsen.

The film features roughly the same contents as the 4-part documentary series Det Røde Kapel.

==Plot==
The authorities demand much control over the performance of the theatre troupe, and try to use it for propaganda purposes. The film crew plays along, but among themselves and in the voice-over they are critical of the regime.

== Reception ==
Los Angeles Times reviewer Mark Olson called it "shocking, funny and wildly outrageous" and "a real find". {{Cite news
 | author = Mark Olsen
 | title = Sundance 2010: Satire and smarts in The Red Chapel and The Imperialists Are Still Alive!
 | url = http://latimesblogs.latimes.com/movies/2010/01/sundance-2010-satire-and-smarts-mix-in-the-red-chapel-and-the-imperialists-are-alive.html
 | date = January 30, 2010
 | work = Los Angeles Times
}} 
Kyle Smith from the New York Post described it as "a clear-eyed and inspired documentary". {{Cite news
 | author = Kyle Smith
 | title = Korean commie comedy
 | url = http://www.nypost.com/p/entertainment/movies/korean_commie_comedy_W7WxmNVCegYMv5UM7tKaWL
 | date = 29 December 2010
 | work = New York Post
}} 
The New York Times reviewer Neil Genzlinger found it sloppy and thought it had "no revelations to offer". {{Cite news
 | author = Neil Genzlinger
 | title = A Fake Danish Comedy Troupe Goes To North Korea, With Strange Results
 | work = The New York Times
 | url = http://movies.nytimes.com/2010/12/29/movies/29red.html
 | date = 28 December 2010
}} 

==See also==
*The Idiots, another Danish film playing on social discomfort with disability.
* List of documentary films about North Korea

==Footnotes==
{{reflist|refs=
   
}}

==External links==
*  The official film website
* 
*  at the Sundance Film Festival website, including trailer
*  from The Hollywood Reporter
* {{Cite news
 | url = http://online.wsj.com/article/SB10001424052748704774604576035683345919002.html 
 | title =  Phenomenal Flicks Under the Tree 
 | author = Steve Dollar
 | work = The Wall Street Journal
 | date = 23 December 2010
}}

 
 
{{Succession box
| title= 
| years=2010
| before=Rough Aunties
| after=Hell and Back Again}}
 

 
 
 
 
 
 
 
 
 
 