Melvin Goes to Dinner
{{Infobox film
| name           = Melvin Goes to Dinner
| image          = MelvinGoestoDinner.jpg
| image_size     =
| caption        =
| director       = Bob Odenkirk
| producer       =
| writer         = Michael Blieden
| narrator       =
| starring       = Michael Blieden Stephanie Courtney Matt Price Annabelle Gurwitch Kathleen Roll Maura Tierney Jenna Fischer Jack Black
| music          = Michael Penn
| cinematography =
| editing        =
| distributor    = Arrival Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States English
| budget         =
| gross          = $4,168 (US) 
}}
 2003 United American film adaptation of Michael Bliedens stage play Phyro-Giants!, directed by Bob Odenkirk. Blieden wrote the screenplay from his stage play, and he also stars in the film (as he did in the Los Angeles stage production),  along with Stephanie Courtney, Matt Price  and Annabelle Gurwitch.

==Plot==
Melvin is a onetime medical student who has dropped out of medical training and now works (after a fashion) in a planning office of an unnamed city; the office supervisor is his big sister, so she "mothers" him instead of making him perform well.  Melvin accidentally makes telephone contact with an old friend, and they decide to meet that evening for dinner.  The friend decides to arrive early at the restaurant for drinks with a lady friend.  By the time the dinner appointment arrives, there are 4 people involved, all of them connected in some way to at least one of the other parties.  The evening passes in a leisurely dinner with much conversation, sometimes intimate.  The connections between the parties are revealed throughout the evening.  The movie includes several flashbacks, which at the start are not explained but which become understandable by the end.

==Production== South by Southwest Film Festival in Austin, Texas, and the Best Picture and Best Ensemble Awards at the Phoenix Film Festival.

The movie provides an interesting sidelight of "spot the actor", since it uses many actors who are mainliners in other television productions, such as Odenkirks former Mr. Show co-star David Cross as a self-help seminar leader.  However, the main characters are all played by the stage actors who performed in the Los Angeles stage production on which the screenplay is based.

==Cast==
*Writer:  Michael Blieden
*Screenplay: Michael Blieden
*Director: Bob Odenkirk
*Melvin:  Michael Blieden
*Joey:  Matt Price
*Alex:  Stephanie Courtney
*Sarah: Annabelle Gurwitch
*Waitress: Kathleen Roll
*Hostess:  Jenna Fischer
*Tranice: Melora Walters (uncredited)
*Keith:  Bob Odenkirk
*Leslie: Maura Tierney
*Rita:  Jacqueline Heinze
*Laura:  Laura Kightlinger
*Vesa:  Fred Armisen
*Solly:  Jerry Minor
*Psychiatric Patient: Jack Black (uncredited)
*(extra):  Kristen Wiig
*(extra):  Bill Odenkirk (brother of Bob Odenkirk)
*(extra): Daisy Gardner
*(extra):  Jennifer Courtney
*(extra):  Allan Havey
*(extra):  Don McEnery
*(extra):  Nathan Odenkirk (son of Bob and Naomi Odenkirk)
*(extra):  Scott Adsit
*(extra):  Scarlett Lam
*(extra):  Wendy Rae Fowler
*(extra):  Susan Chuang
*(extra):  Michele Fitzgerald
*(extra):  Jennifer Biederman
*(extra):  Tucker Smallwood
*(extra):  B. J. Porter James Gunn
*(extra):  Marc Evan Jackson

==References==
 

==External links==
* 
* 
* 
*  (2003)

 

 
 
 
 
 
 