We Dreamed America
 
{{Infobox Film
 | name = We Dreamed America
 | caption = 
 | director = Alex Walker
 | producer = Felix Bechtolsheimer
 | writer = Alex Walker Felix Bechtolsheimer
 | starring = Alabama 3 Kitty, Daisy and Lewis Hey Negrita Matt Ord The Broken Family Band The Barker Band
 | distributor = Verve Pictures (UK)
 | released =  
 | runtime = 48 minutes
 | country = United Kingdom
 | image = We Dreamed America VideoCover.jpeg
}}

We Dreamed America is a 2008 documentary film, directed by Alex Walker, that delves deep into the dark recesses of the British music scene. Examining the relationship and ongoing exchange between British and American roots music, the film looks at the British fascination with the most American of genres, Country Music. The film tells the stories of six British Americana bands, while giving an insight into how country music fits into the British music industry.

==Cast== Bob Harris, Little Feat, Sid Griffin and Guy Clark. 

==Release==
The movie was released in the UK on DVD by Verve Pictures on October 6, 2008.

==References==
 

==External links==
* 
* 
*  

 
 
 
 

 