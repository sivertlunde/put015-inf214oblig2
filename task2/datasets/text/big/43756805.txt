Fortress of Peace
{{Infobox film
| name           = Fortress of Peace
| image          = 
| alt            = 
| caption        = 
| film name      = Wehrhafte Schweiz Nous pouvons nous défendre
| director       = John Fernhout
| producer       = Lothar Wolff
| writer         = Gustav Däniker Rudolf Farner
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 20 min.
| country        = Switzerland
| language       = German, French, English CHF 1 mio.
| gross          =  
}}
Fortress of Peace, originally titled Wehrhafte Schweiz in German and Nous pouvons nous défendre in French, is a Swiss short film made in 1964, directed by John Fernhout and produced by Lothar Wolff. It was nominated for the 1965 Academy Award for Best Live Action Short Film.

Wehrhafte Schweiz is a Swiss Army propaganda film, made for the Expo 64 national exhibition in the then-prevailing spirit of geistige Landesverteidigung, "cultural national defense". It portrays the Swiss Army fighting against an unnamed, unseen enemy, using heavy weapons such as flamethrowers, artillery, tanks and bomber aircraft. After the enemy is repelled, the film closes with idyllic shots of beautiful Swiss landscapes. The 20-minute film was shot using the latest action film techniques in the Cinerama format on expensive MCS-70 Super Panorama 70mm stock.
 CHF 1 million production cost, concerns about military secrecy and the use of foreign rather than Swiss creative staff. Critical opinion was mostly positive, although concerns were voiced about the film being too loud and aggressive, not highlighting the Swiss tradition of militia service, and for being too reminiscent of propaganda films by foreign totalitarian regimes. In a slightly adapted form, the film was also shown abroad under the English title Fortress of Peace.

In 2014, the deteriorated film stock was restored by the Swiss Army and Memoriav, a Swiss cultural heritage association, for public screenings in Switzerland on the occasion of the 50-year anniversary of Expo 64.

==External links==
* 

==References==
* 

 
 
 
 