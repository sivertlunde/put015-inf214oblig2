Kunguma Poovum Konjum Puravum
{{Infobox film
| name           = Kunguma Poovum Konjum Puravum
| image          = 
| alt            =  
| caption        = 
| director       = Rajamohan
| producer       = SPB Charan
| writer         = Rajamohan
| starring       = Ramakrishnan Thananya Tharun Chatriya Nagamma
| music          = Yuvan Shankar Raja
| cinematography = Siddharth
| editing        = N. B. Srikanth Praveen K. L.
| studio         = Capital Film Works
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}
 Indian Cinema Tamil drama film, directed by debutante Rajamohan, who earlier worked under renowned directors like A. Venkatesh (director)|A. Venkatesh and S. D. Vijay Milton, which stars newcomers Ramakrishnan and Thananya in lead roles. It is produced by SPB Charan under his banner Capital Film Works and has musical score by acclaimed music director Yuvan Shankar Raja. The film released on 24 April 2009 to Good responses.

==Plot==

Thulasi (Thananya) comes to Muttam village along with her grandmother (Nagamma) after her parent’s desert her. She joins a government school in the village to pursue her studies. Enters Koochan (Ramakrishnan) who falls for her instantly. Koochans mother Chandra (Agavamma) helps the poor Thulasi to pursue her studies. However coming to know about their romance, Agavamma insults Thulasi and her grandmother and drives them out of the village when Koochan is out on a school tour. A shocked Koochan returns only to meet with a road mishap. Meanwhile, Thulasi, who seeks refuge in a family at Tuticorin is forced to marry a rowdy Drama (Tharun Chatriya). On the day of their marriage, he gets arrested by police. His wayward lifestyle invites trouble and he eventually gets arrested for murdering a youth.

An annoyed Thulasi returns to Muttam only to see Koochan lost himself in liquor after his love failure. Meanwhile, Koochan resolves to set right Thulasis life. But things take a turn when Dharma comes to Muttam after getting bail.

==Crew==
*Story, Screenplay, Direction: Rajamohan
*Production: SPB Charan
*Cinematography: Siddharth
*Music: Yuvan Shankar Raja
*Editing: Praveen KL & Srikanth NB
*Art direction: Videsh
*Choreography: Ajay Raj & Saravana Rajan
*Lyrics: Vaali (poet)|Kavignar Vaali, Gangai Amaran
*Stunts: Selva

==Soundtrack==
{{Infobox album |  
| Name = Kunguma Poovum Konjum Puravum 
| Type = soundtrack 
| Artist = Yuvan Shankar Raja
| Cover = KPKP small.jpg
| Released =  24 January 2009 (India)
| Recorded = 2008 Feature film soundtrack 
| Length = 27:36
| Lyrics = Vaali (poet)|Vaali, Gangai Amaran
| Label = Think Music
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Silambattam (2008)
| This album  = Kunguma Poovum Konjum Puravum (2009) 
| Next album  = Siva Manasula Sakthi (2009)
}}
 Kavignar Vaali and Gangai Amaran.  Father S. P. Balasubramaniam and son SPB Charan, the producers of the film, have each sung one of the films, as did composer Yuvan Shankar Raja himself and his cousin Venkat Prabhu. The songs and the film score have been recorded using a live orchestra and without any electronic instruments such as synthesizers to create a rural feel to the music. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Lyrics!! Notes 
|-
| 1
| Muttathu Pakkathil
| Venkat Prabhu
| 4:02
| Gangai Amaran
|
|-
| 2
| Kadaloram Oru Ooru
| Yuvan Shankar Raja
| 5:33 Vaali
|
|-
| 3
| Chinnan Sirusu
| Javed Ali, Bela Shende
| 5:03
| Vaali
|
|-
| 4
| Na Dharmanda
| S. P. Balasubramaniam
| 4:00
| Gangai Amaran
|
|-
| 5
| Oru Nimisham
| Velmurugan
| 3:27
| Gangai Amaran
|
|-
| 6
| Kadalaoram Oru Ooru
| S. P. B. Charan
| 5:31
| Vaali
|
|}

==Production==

===Development===
Nearly one year after the stupendous success of his film Chennai 600028, in May 2008, SPB Charan decided to produce his next film. He then got to meet Rajamohan, as erstwhile assistant, who worked under prominent directors like A. Venkatesh (director)|A. Venkatesh and Rajakumaran, who told him the story of his film, which he titled as "Kunguma Poovum Konjum Puravum", impressed of which, Charan decided to produce that film. Rajamohan became a director, since he wanted to fulfill the wish of his brother, who wanted to became a director, but died in an accident, before his wish was satisfied.

===Casting=== Cheran and felt he would be the right person for the role and gave him the role of the protagonist. Tharshana, who later changed her name to Thananya, a second year medical student hailing from Andhra Pradesh, was picked as the heroine to play the female lead, after she was discovered by Charan and Rajamohan in a medical college. Though she was hesitating at first, she eventually agreed to take over the role of Thulasi, after Charan and Rajamohan explained her the importance and the scope of her role in the film and convinced her.  Apart from Ramakrishnan and Tharshana, another newcomer, Tharun Chatriya, and an older woman, who dons the role of the grandmother of Thulasi, are said to be important characters in the film. Moreover, eight assistant directors and some "lightmen", do act in the film as well, who Rajamohan and Charan wanted to pay a tribute for their hard work and the effort they put in. 

Yuvan Shankar Raja, a regular of Charans films, was assigned as the music director, as were the other crew members, who had also worked for Charans previous venture Chennai 600028.

===Filming===
The shooting of the film started on 9 June 2008 in Nagercoil and was entirely shot in a small village called "Muttam" near Nagercoil and in surrounding areas.

==Trivia== SRM Valliammai Engineering College at Kattankulathur, Kanchipuram district, the team of the film conducted a two-day event at the campus, where rural games were organized and 30 winners were chosen to attend the audio release of the film along with big stars from the film industry.

==References==
 

==External links==
*  
*  

 
 
 
 