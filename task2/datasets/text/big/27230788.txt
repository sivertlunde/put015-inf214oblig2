The Time of His Life
{{Infobox film
| name           =  The Time of His Life
| image          = 
| caption        = 
| director       = Leslie S. Hiscott
| producer       = Elizabeth Hiscott   W.A. Smith
| writer         = Brock Williams   Richard Hearne   Leslie S. Hiscott 
| screenplay     = 
| story          = 
| based on       = 
| starring       = Richard Hearne   Ellen Pollock   Richard Wattis   Frederick Leister
| music          = Elizabeth Hiscott
| cinematography = Kenneth Talbot
| editing        = Erwin Reiner
| studio         = Shaftesbury Films
| distributor    = Renown Pictures Corporation
| released       = 1955
| runtime        = 74 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
The Time of His Life is a 1955 British comedy film directed by Leslie S. Hiscott and starring Richard Hearne, Ellen Pollock, Richard Wattis and Robert Moreton. A man is released from prison, and goes to live with his socialite daughter. 

==Cast==
* Richard Hearne - Charles Pastry
* Ellen Pollock - Lady Florence
* Richard Wattis - Edgar
* Robert Moreton - Humphrey
* Frederick Leister - Sir John
* Peter Sinclair - Kane
* John Downing - Simon
* Anne Smith - Penelope
* Darcy Conyers - Morgan
* Yvonne Hearne - Guest
* Peggy Ann Clifford - Cook
* Arthur Hewlett - Prison Governor
* Harry Towb - Steele

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 