They Gave Him a Gun
{{Infobox film
| name           = They Gave Him a Gun
| image          = They Gave Him a Gun poster.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = W. S. Van Dyke
| writer         = Cyril Hume Richard Maibaum Maurice Rapf
| based on       =  
| starring       = Spencer Tracy Gladys George Franchot Tone
| music          = Sigmund Romberg
| cinematography = Harold Rosson Ben Lewis
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $606,000  . 
| gross          =$1,313,000 
}}

They Gave Him a Gun is a 1937 American crime drama film directed by W. S. Van Dyke and starring Spencer Tracy, Gladys George, and Franchot Tone. The picture bears a resemblance to later films noir in its dark theme regarding the struggles and failures of a man trying to take a criminal shortcut to the American dream. The screenplay was written by Cyril Hume,Richard Maibaum, and Maurice Rapf, based on the 1936 book of the same name by William J. Cowen.  

==Plot==
The movie begins in World War I when a young man named Jimmy (Franchot Tone) unexpectedly becomes a hero by killing all the Germans in a machine gun nest. But he is then severely wounded and spends time in a hospital being cared for by a nurse, Rose (Gladys George), with whom he falls in love. But she is really in love with Jimmy’s buddy, Fred (Spencer Tracy), a carnival barker. However, when Fred doesnt return from the battlefield, the two think he’s been killed (when he was merely captured) and so they make wedding plans. Then when Fred returns he decides to support Jimmy and Rose marrying, even though it breaks his heart. After the war Fred meets up with Jimmy again and discovers that Jimmy is a racketeer who uses his battle skills to commit murder. So he tells Rose, who had no idea. She then reports her husband to the police so he will go to prison and be reformed. But Jimmy breaks out of prison and tries to take Rose on the lam with him. At this point Fred intervenes. Jimmy, feeling undeserving, commits suicide by police.

== Cast ==
* Spencer Tracy as Fred P. Willis
* Gladys George as Rose Duffy
* Franchot Tone as James "Jimmy" Davis
* Edgar Dearing as Sgt. Meadowlark Mary Lou Treen as Saxe 
* Cliff Edwards as Laro
* Charles Trowbridge as Judge

==Box office==
According to MGM records the film earned $718,000 in the US and Canada and $595,000 elsewhere resulting in a profit of $253,000. 

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 