The Lady Consents
{{Infobox film
| name           = The Lady Consents
| image          = 
| alt            =
| caption        = 
| film name      =  Stephen Roberts
| producer       = Edward Kaufman
| writer         = 
| screenplay     = P. J. Wolfson Anthony Veiller
| story          = P. J. Wolfson
| based on       =  
| starring       = Ann Harding Herbert Marshall
| narrator       = 
| music          = Roy Webb
| cinematography = J. Roy Hunt
| editing        =  RKO Radio Pictures
| distributor    = 
| released       =   }}
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
 Stephen Roberts from a screenplay by P. J. Wolfson and Anthony Veiller, from Wolfsons story, "The Indestructible Mrs. Talbot".  Starring Ann Harding and Herbert Marshall, RKO Radio Pictures released the film on February 7, 1936.

==References==
 

 