The Eagle (2011 film)
 
 
{{Infobox film
| name           = The Eagle
| image          = The Eagle Poster.jpg
| caption        = Theatrical release poster Kevin Macdonald
| producer       = Duncan Kenworthy
| writer         = 
| screenplay     = Jeremy Brock
| story          = 
| based on       =  
| starring       = Channing Tatum Jamie Bell Donald Sutherland Mark Strong
| music          = Atli Örvarsson
| cinematography = Anthony Dod Mantle
| editing        = Justine Wright
| studio         = Toledo Productions Film4 Productions DMG Entertainment Universal Pictures  
| released       =  
| runtime        = 114 minutes
| country        = United Kingdom United States
| language       = English Scottish Gaelic
| budget         = $25 million   
| gross          = $35,467,108
}} historical adventure adventure film Kevin Macdonald, Roman eagle Ninth Spanish Legions supposed disappearance in Britain.
 American co-production, Ireland on 25 March 2011.

==Plot summary==
 Ninth Legion Britain to eagle standard Celtic tribesmen. He is decorated for his bravery but honourably discharged due to a severe leg injury.
 Calleva (modern Silchester) in southern Britain, Marcus has to cope with his military career having been cut short and his fathers name still being held in disrepute. Hearing rumours that the eagle standard has been seen in the north of Britain, Aquila decides to recover it. Despite the warnings of his uncle and his fellow Romans, who believe that no Roman can survive north of Hadrians Wall, he travels north into the territory of the Picts, accompanied only by his slave, Esca. The son of a deceased chieftain of the Brigantes, Esca detests Rome and what it stands for, but considers himself bound to Marcus, who saved his life during an amphitheatre show.

After several weeks of travelling through the northern wilderness, Esca and Marcus encounter Guern, a Roman born Lucius Caius Metellus, one of the survivors of the Ninth Legion, who attributes his survival to the hospitality of the Selgovae tribe. Guern recalls that all but a small number of deserters were killed in an ambush by the northern tribes – including Escas Brigantes – and that the eagle standard was taken away by the Seal People, the most vicious of the tribes. The two travel further north until they are found by the Seal People. Identifying himself as a chieftains son fleeing Roman rule and claiming Marcus as his slave, Esca is welcomed by the tribe. After allowing the Seal People to mistreat Marcus, Esca eventually reveals that his actions were a ploy and helps his master to find the eagle. As they retrieve it, they are ambushed by several warriors, including the Seal Princes father. Marcus and Esca manage to kill them and, with the aid of the Seal Princes young son, escape from the village.
 grants the governor in Londinium. There is some talk of the Ninth Legion being reformed with Marcus as its commander. But when Marcus and Esca wonder what they will do next, Marcus leaves the decision to Esca.

===Alternate ending===	 
An alternate ending is featured in the DVD. Marcus decides to burn the eagle standard on the altar where the final battle occurred, instead of delivering it to the Roman governor. He tells Esca that he does this because the eagle belongs to the men who fought for it. Marcus and Esca are then shown approaching Hadrians Wall on foot and talking about their plans for the future.

==Main cast==
*Channing Tatum as Marcus Flavius Aquila
*Jamie Bell as Esca
*Donald Sutherland as Marcuss Uncle Aquila
*Mark Strong as Guern/Lucius Caius Metellus
*Tahar Rahim as Prince of the Seal People
*Denis OHare as Centurion Lutorius
*Douglas Henshall as Cradoc Paul Ritter as Galba
*Dakin Matthews as Legate Claudius
*Pip Carter as Tribune Placidus
*Ned Dennehy as Chief of the Seal People

==Production==
 , among other locations. ]] 62nd Cannes Film Festival in May 2009, The Eagle of the Ninth secured distribution deals "for every global market". 
 Celtic peoples, Pictish is the more likely language to have been spoken at the time.  "Its the best we can do," Macdonald said. "All you can do is build on a few clues and trust your own instincts. That way, no one can tell you you were wrong."  Only 1% of Scots speak Gaelic, limiting the talent pool to just 60,000 people. By August 2009, several Gaelic-speaking boys had auditioned for the role of a boy of the Seal people, aged nine to twelve, but without success,  so Macdonald held open auditions in Glasgow for the role.  It was eventually given to nine-year-old Thomas Henry from New Barnsley, Belfast, who had been educated in Irish Gaelic.
 Inouit  , living off seals and dressed in sealskins. We are going to create a culture about which no one knows much, but which we will make as convincing as possible. We are basing it on clues gained from places like Skara Brae and the Tomb of the Eagles in Orkney, so that we will have them worshipping pagan symbols, like the seal and the eagle. The reason they have seized the emblem of the Roman eagle from the legion is because to them it   a sacred symbol.  
 Old Dornie. The Pictish village which was constructed at Fox Point was used on most days of the filming. Other sites included Achnahaird beach, where a horse chase was filmed, and Loch Lurgainn. Macdonald intended to use locals as extra (actor)|extras. This was a success with many locals appearing as extras after going to castings in nearby Ullapool. Their roles included "Seal Warriors", "Seal Princesses" and "Elders".
 contemporary symbolism",  with Bell using a neutral English accent. 

According to Channing Tatum, the actors trained 4–5 hours a day for each role. 

Although the film mostly stuck to the plot of Sutcliffs novel, there were a few modifications and condensations, e.g. it introduced a few violent scenes into what had been a novel written for older children.

==Release==
===Critical reception===
The Eagle received mixed reviews, with the review aggregation website Rotten Tomatoes reporting that 38% of critics gave the film a positive review with an average score of 5.3/10, based on 143 professional reviews. The sites consensus stated, "The Eagle has a pleasantly traditional action-adventure appeal, but its drowned out by Kevin Macdonalds stolid direction and Channing Tatums uninspired work in the central role."  Metacritic gave the film an average score of 55/100 based on 35 critical reviews.  The reception by audiences was similar, with audiences giving the film an average score of C+ according to CinemaScore.   

Roger Ebert gave The Eagle three stars out of four saying that "it evokes the energy of traditional sword-and-shield movies" and praising its realistic battle scenes and limited use of Computer-generated imagery|CGI. 

===Box office performance===
The film had a worldwide gross of $35,467,108 as of 9 May 2011, higher than its $25 million budget. 

In the United States, The Eagle was released on 11 February 2011, in 2,296 theatres.  .   and Just Go With It. 

===Home media===
The Eagle was released on DVD and Blu-ray Disc on 21 June 2011. 

==See also==
*The Eagle of the Ninth
*Centurion (film)|Centurion (film), a 2010 film on a similar topic
*Legio IX Hispana
*2011 in film

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 