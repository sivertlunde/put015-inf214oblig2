Local Color (film)
{{Infobox Film
| name           = Local Color
| image          = Local-color-movie.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = George Gallo
| producer       = Jimmy Evangelatos Julie Gallo David Sosna
| writer         = George Gallo Trevor Morgan Ray Liotta Charles Durning Samantha Mathis
| music          = Chris Boardman
| cinematography = Michael Negrin
| editing        = Malcolm Campbell
| distributor    = monterey media (U.S.)
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
}} Trevor Morgan in the lead role. It is based on a true story, that of the director/writers experience when he was 18. The character of Nikolai Serov was based on George Cherepov, to whom Gallo had been an apprentice in the 1970s. 

Gallo also painted all of the oil paintings in this movie, having "cleaned out his whole garage" of his paintings. In particular, the paintings that John (Trevor Morgan) showed to Serov (Armin Mueller-Stahl) when asking him for advice in the beginning of the movie were the exact paintings that Gallo as a teenager had showed Cherepov.
 Covington and Baton Rouge.  Production began in July 2005. The company stayed in two different hotels to avoid delays driving from New Orleans to a nearby location at the end of the schedule thus able to wrap production only 8 days before Hurricane Katrina without interruption.

==Cast== Trevor Morgan as John Talia Jr.
* Armin Mueller-Stahl as Nikolai Serov
* Ray Liotta as John Talia Sr.
* Charles Durning as Yammi
* Samantha Mathis as Carla
* Ron Perlman as Curtis Sunday
* Diana Scarwid as Edith Talia
* Julie Lott as Sandra Sunday Tom Adams as grey artist
* Taso Papadakis as metal artist
* David Sosna as college dean
* Nancy Casemore as Mrs. Huntington-Quail
* David Sheftell as Mikey
* Timothy Velasquez as nasty kid
* Melissa Allman as gallery girl
* Jimmy Evangelatos as waiter
* Michael Negrin as John (50 years old)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 