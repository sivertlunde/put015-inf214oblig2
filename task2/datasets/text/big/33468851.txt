Hello Ghost
{{Infobox film name           = "Hello Ghost" image          = HelloGhost2010Poster.jpg caption        = South Korean poster
| film name = {{Film name hangul         =    rr             = Hellou Goseuteu  mr             = Hellou Kosŭt‘ŭ}} director       = Kim Young-tak   producer       = Lim Sung-bin Choi Moon-soo  writer         = Kim Young-tak  starring       = Cha Tae-hyun Kang Ye-won music          = Kim Jun-seok cinematography = Kim Yung-chul editing        = Kim Sun-min distributor  NEW Co., Ltd. released       =   runtime        = 111 minutes country        = South Korea language       = Korean budget         =  gross          = $19,774,455 
}}
Hello Ghost ( ) is a 2010 South Korean comedy film about a mans multiple failed suicide attempts. After the most recent one, he discovers he can see a family of ghosts. The ghosts agree to leave him alone under the condition that he fulfill their requests.
 grossing Korean film in 2010.  The film had a total of 3,042,021 admissions nationwide. 
 Chris Columbus. 

==Plot==
Sang-man (Cha Tae-hyun) attempts to commit suicide by overdosing on pills, but fails. He then attempts to kill himself by jumping off a bridge and into a river, but is saved. Brought to the hospital, Sang-man awakens and sees a man smoking next to him. He tells the other hospital patrons, but no one believes him. During his stay in the hospital he eventually meets four ghosts. Meanwhile, Sang-man meets nurse Jung Yun-soo (Kang Ye-won) at the hospital and quickly falls in love.

Sang-man is released from the hospital and the ghosts follow him back to his apartment. He attempts to politely ask the ghosts why they are there, but he doesnt get an answer. He decides to visit a shaman to get some help and to learn how to get rid of the ghosts. He is told that the ghosts are using his body to enter the world of the living so the ghosts can experience their unfulfilled desires. Sang-man tries to get rid of them, but ghosts haunt him everywhere he goes. Sang-man then attempts to help the ghosts fulfill their unrealized dreams to get them to leave him alone once and for all.

One ghost, a fat smoker, wants to get his taxi back and drive it. He also uses Sang-mans body to swim at the beach. Another ghost, an old man, wants to return a camera to his friend. This leads to Sang-man being arrested, because the camera is in the hands of a police officer. Another ghost, a child, wants to watch a cartoon movie. The last ghost is a crying woman who wants to cook and to eat together with people she cares about. All these events also lead to Sang-man getting to know Yun-soo better.

Yun-soo fathers dies, and Sang-man is the last person he speaks with. Yun-soo is uncomfortable with this, and pushes Sang-man away until she realizes that Sang-man is telling the truth when she sees her fathers last gift for her.

Sang-man tells the ghosts to go away and blames them for Yun-soos rejection. When he wakes up, the ghosts are gone. He goes to the hospital and asks Yun-soo out for lunch, and she accepts. She tells him that some people may lose their memories from extreme Acute stress reaction|shock. As soon as Yun-soo asks about the parsley in the kimbap, Sang-man remembers that his mother used to put parsley in kimbap instead of spinach.

Sang-man runs to his apartment, and on his way he remembers what has happened to him. His father was the smoker, his mother the lady, his grandfather the old man, and his older brother the kid. On a family trip, his father forgot to put gas in the car. The car stopped in the road, and was hit by a truck and pushed off a cliff. Sang-man is the only survivor of the accident, after which he lost his memory and grew up in an orphanage. Sang-man calls to them in his apartment. One by one they appear and explain that their wishes each had something to do with the promises they left for Sang-man, and helped him remember them. His mother apologizes for leaving him alone and says that they wanted Sang-man to live, and they will always be with him. At the end, it shows that it was his family that stopped Sang-man from committing suicide.

==Cast==
* Cha Tae-hyun as Sang-man
* Kang Ye-won as Jung Yun-soo
* Lee Mun-su as Older Ghost
* Ko Chang-seok as Smoking Ghost
* Jang Young-nam as Crying Ghost
* Chun Bo-geun as Elementary School Student Ghost

== Remake == Chris Columbus for the remake of this film which is possible release date of 2015. 

==References==
 

==External links==
* http://www.helloghost.co.kr/
*  
*  
*  

 
 
 
 
 