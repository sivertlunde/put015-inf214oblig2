L'honorable Stanislas, agent secret
{{Infobox film
| name           = Lhonorable Stanislas, agent secret
| image          = Lhonorable Stanislas, agent secret.jpg
| image size     = 200
| caption        = 
| director       = Jean-Charles Dudrumet
| producer       = Films de la Licorne, Italgamma
| writer         = Michel Cousin Jean-Charles Dudrumet
| starring       = Jean Marais Geneviève Page
| music          = Georges Delerue
| cinematography = 
| editing        = 
| distributor    = 
| released       = 4 September 1963 (France)
| runtime        = 91 minutes
| country        = France, Italy
| awards         = 
| language       = French
| budget         = 
}}
 French Comedy comedy film from 1963, directed by Jean-Charles Dudrumet, written by Michel Cousin, starring Jean Marais and Geneviève Page.  The film was known under the title "How to Be a Spy Without Even Trying", "The Reluctant Spy" (USA), "Spionaggio senza frontiere" (Italy). 
 parody of espionage films. It was followed by sequel Pleins feux sur Stanislas.

==Plot==
Stanislas Dubois is a businessman who dresses according to contemporary standards. When he meets a woman on a date in a restaurant, hes clad in a coat. Upon leaving he confuses a similar coat with his own one. Following this mix-up he becomes increasingly aware that hes got meanwhile something about him which attracts peculiar people. Hidden in his new coat is a microfilm which interests more than one secret service. Once hes already drawn into the world of international espionage, a secret service of his own country hires him to lure enemy spies into a trap, hereby serving as bait.

== Cast ==
* Jean Marais (Stanislas Evariste Dubois, director of an advertising agency)
* Geneviève Page (Ursula Keller, museum guide)
* Noël Roquevert (Inspector Mouton)
* Gaïa Germani (Andrea, the female spokesman)
* Maurice Teynac (Alfred Thirios, the trafficker)
* Jean Galland (Colonel Derblay, press relations officer)
* Christian Marin (Lecanut, the secretary of Inspector Mouton)
* Mathilde Casadessus (Maria Linas, the diva)
* Marcelle Arnold (Miss Morin, the secretary of Stanislas)
* Hélène Dieudonné (grandmother of Stanislas)
* Germaine Dermoz (mother of Stanislas)
* Yvonne Clech (the lady on the train)
* Louis Arbessier (the museum director)
* Robert Rollis (mailman)
* Pierre Tornade (inspector)

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 
 
 


 