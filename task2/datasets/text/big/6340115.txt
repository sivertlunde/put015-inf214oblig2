Manny & Lo
 
{{Infobox film
| name = Manny & Lo
| caption =
| image	=	Manny & Lo FilmPoster.jpeg
| director = Lisa Krueger
| producer = Marlen Hecht Dean Silvers Klaus Volkenborn  
| writer = Lisa Krueger
| starring = Scarlett Johansson Aleksa Palladino Mary Kay Place
| music = John Lurie
| cinematography = Tom Krueger
| editing = Colleen Sharp
| distributor = Sony Pictures Classics
| released =  
| runtime = 88 minutes
| budget =
| gross =
}}

Manny & Lo is a 1996 comedy-drama film directed by Lisa Krueger, starring Scarlett Johansson, Aleksa Palladino, and Mary Kay Place.

==Synopsis==
Two sisters, 11-year-old Amanda (nicknamed Manny) and 16-year-old Laurel (nicknamed Lo), run away from several foster homes, sleeping wherever they can, including in model homes. But when Lo becomes pregnant, the two find that they cant make it through this crisis on their own. With nowhere else to turn, they decided to kidnap Elaine, a clerk at a baby supply store. But it seems that Elaine just may need Manny and Lo as much as they need her.

==Cast==
* Scarlett Johansson as Manny
* Aleksa Palladino as Lo
* Mary Kay Place as Elaine
* Dean Silvers as Suburban Family
* Marlen Hecht as Suburban Family
* Forrest Silvers as Suburban Family
* Tyler Silvers as Suburban Family
* Lisa Campion as Convenience Store Clerk
* Glenn Fitzgerald as Joey
* Novella Nelson as Georgine
* Susan Decker as Baby Store Customer #1
* Marla Zuk as Baby Store Customer #2
* Bonnie Johnson as Baby Store Customer #3
* Melissa Johnson as Child
* Angie Phillips as Connie
* Cameron Boyd as Chuck
* Paul Guilfoyle as Country House Owner
* Tony Arnaud as Sheriff
* Nicholas Lent as Los Baby

==External links==
*  
*  
*  

 
 
 
 
 
 

 