Yamaleela
{{Infobox film
| name           = Yamaleela
| image          = Yamaleela.jpg
| caption        =
| director       = S. V. Krishna Reddy
| producer       = Kishore Rati   K.Achchi Reddy
| writer         = Diwakar Babu   (Dialogues )   S. V. Krishna Reddy   (story) 
| narrator       = Ali Indraja Satyanarayana Brahmanandam Tanikella Bharani Kota Srinivasa Rao
| music          = S. V. Krishna Reddy
| cinematography = Sarat  K Rajendraprasad
| editing        = K. Ramgopal Reddy
| distributor    = Manisha Films
| released       = 28 April 1994
| runtime        = 140 minutes
| country        = India
| language       = Telugu
| budget         =   75 lakhs
| preceded_by    =
| followed_by    =
}}
 Telugu film Ali as Venkatesh and Lucky Man starring Kartik, Sanghavi and Manjula. A sequel for the movie titled Yamaleela 2 was released in November 2014.

==Plot==
This story is dependent on the mother sentiment. The story is very simple. Hero’s mother lived a very royal life in a great palace as her husband was Jameendar that time. But later she lost her property due to some circumstances and she also lost her husband. And left that palace along with 2 year old kid (Ali).From that moment, she has been living a normal life. One fine day, Ali happens to listen the entire story of her mother and decides there itself to make his mother happy by purchasing the same palace for her mother. So he does lots of mischievous things to earn money. Bringing Yama (Hinduism)|Yamadharmaraja, lord of Naraka (Hinduism)|Naraka, to Earth is part of that. The story ends up with a good climax. The comedy scenes of Yama(Satyanarayana) and chitragupta(Bramhanandham) are most hilarious. Their conversation with the local police is most memorable hilarious comedy. Music is excellent. This movie is musical hit. All songs in this movie have become most popular. This is also a good movie for children. Totally, this is a good family entertainment.

==Cast==
  Ali
*Indraja Indraja ... Lily
*Kaikala Satyanarayana   ... Yama
*Manju Bhargavi ... Alis mother
*Bramhanandam ... Chitragupta
*Gundu Hanumantha Rao
*Tanikella Bharani ... Thota Ramudu
*Amanchi Venkata Subrahmanyam|A.V.S.
*Kota Srinivasa Rao
*M. Balaiah ... Brahma Krishna (cameo)
*Baby Likhita
*Chakradhara Rao Chittibabu
*Dham Jenny
*Kishore Raati (Cameo)
*Lathasri
*Master Milan
*Pooja
*Saakshi Ranga Rao
 

==Crew==
* 
*Screenplay: S. V. Krishna Reddy
*Music: S. V. Krishna Reddy
*narrative|Story: S. V. Krishna Reddy
* 
* , K.S Chithra Editor : K. Ramgopal Reddy
*Dialogue : Diwakar Babu
* , Jonnaviththula
*Art : RS Raju
*Photography : Sarat, K Rajendraprasad Producer : Kishore Rati, Achchi Reddy
*Banner : Manisha Films

==Soundtrack==
* "Sirulolikinche Chinni Navvule Mani Manikyalu"
* "Ni Jinu Pantu Chusi Bullemmo"
* "Erra Kaluva Puvva Yeddama Chali Manta"
* "Jumbare Jujumbare Jumbarahi"
* "Abhivandanam Yama Rajagrani"

==Remakes==
{|class="wikitable"
|-
! Year !! Film !! Language !! Cast
|- 1996 ||Lucky Lucky Man Tamil ||Karthik, Sanghavi
|- 1996 ||Taqdeerwala Hindi ||Daggubati Venkatesh|Venkatesh, Raveena Tandon
|-
|}

==Awards== Filmfare Awards Best Film – Telugu - K. Atchi Reddy

==External links==
*  

 
 

 
 
 
 
 
 