Our Happy Lives
 
{{Infobox Film
| name           = Our Happy Lives
| image          = 
| image_size     = 
| caption        = 
| director       = Jacques Maillot
| producer       = Laurent Bénégui
| writer         = Jacques Maillot Eric Veniard
| narrator       = 
| starring       = Marie Payen
| music          = 
| cinematography = Luc Pagès
| editing        = Andrea Sedlácková
| distributor    = 
| released       = 19 May 1999
| runtime        = 147 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Our Happy Lives ( ) is a 1999 French drama film directed by Jacques Maillot. It was entered into the 1999 Cannes Film Festival.   

==Cast==
* Marie Payen - Julie
* Cécile Richard - Cécile
* Camille Japy - Emilie
* Sami Bouajila - Ali
* Eric Bonicatto - Jean-Paul
* Jean-Michel Portal - Lucas
* Sarah Grappin - Sylvie
* Olivier Py - François
* Alain Beigel - Antoine
* Fanny Cottençon - La mère de Cécile
* Marc Chapiteau - Le père de Sylvie
* Frédéric Gélard - Vincent
* Jean-Paul Bonnaire - Le père de Lucas
* Jalil Lespert - Etienne

==References==
 

==External links==
* 

 
 
 
 
 


 