Shipbreakers
{{Infobox Film
| name           = Shipbreakers
| image          = 
| image_size     = 
| caption        = 
| director       = Michael Kot
| producer       = Ed Barreveld   Michael Kot   Peter Starr
| writer         = Shelley Saywell (Narration)
| narrator       = Ted Biggs
| starring       = 
| music          = Ken Myhr
| cinematography = Derek Rogers
| editing        = Deborah Palloway	
| distributor    = National Film Board of Canada
| released       = 2004
| runtime        = 73 mins
| country        = Canada
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Shipbreakers (2004 in film|2004) is a documentary film.  A co-production of the National Film Board of Canada with Storyline Entertainment directed by Michael Kot, the film explores the practice of ship breaking decommissioned vessels in Alang, India.
 PCBs and asbestos. An average of one worker a day dies from falls and other accidents while others are expected to succumb to future cancers.

==Awards==
*Gemini Award for Best Photography in a Documentary Program or Series (Derek Rogers)
*Ecofilms Festival First Prize - Golden Deer Award
*Golden Sheaf Awards for Best Director Non-fiction and Best Nature/Environment Documentary   

==References==
 

== External links ==
*   
*  

 
 
 
 
 
 
 
 


 