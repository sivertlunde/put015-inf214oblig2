On Top (film)
{{Infobox Film
| name           = Með allt á hreinu (On Top)
| image          = 
| image_size     = 
| caption        = 
| director       = Ágúst Guðmundsson
| producer       = 
| writer         = 
| narrator       = 
| starring       = Egill Ólafsson Ragnhildur Gísladóttir Valgeir Guðjónsson Eggert Þorleifsson
| music          = 
| cinematography = 
| editing        = 
| distributor    =  1982
| runtime        = 
| country        = Iceland
| language       = Icelandic
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
On Top (Icelandic: Með allt á hreinu ( )) is an Icelandic musical comedy from 1982 in film|1982, directed by Icelandic director Ágúst Guðmundsson. The film, which still enjoys widespread popularity in Iceland, tells the story of two bands (one all-male and one all-female) that go on a tour together and then become engaged in a rivalry.

The cast is composed members of the pop bands Stuðmenn (also the name of the band in the film) and Grýlurnar (Gærurnar in the film), and the film is largely based on performances by the two bands. According to Valgeir Guðjónsson the L.A. Times film critic reviewed the movie and started the review by saying "On Top features a bunch of Icelandic fruitcakes wearing strange clothes"

==Cast==
*Egill Ólafsson
*Ragnhildur Gísladóttir
*Valgeir Guðjónsson
*Eggert Þorleifsson
*Jakob Frímann Magnússon
*Anna Björnsdóttir

==External links==
* 
*  

 
 
 
 
 


 
 