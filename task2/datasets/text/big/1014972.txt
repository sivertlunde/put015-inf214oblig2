Winged Migration
{{Infobox film
| name            = Winged Migration
| image           = Winged Migration movie.jpg
| image_size      = 225px
| caption         = theatrical poster
| director        = Jacques Perrin, Jacques Cluzaud, Michel Debats
| writer          = Jean Dorst Jacques Perrin
| narrator        = Jacques Perrin Philippe Labro
| producer        = Christophe Barratier, Jacques Perrin
| music           = Bruno Coulais
| cinematographer =
| editing         =
| distributor     = Sony Pictures Classics
| released        = 12 December 2001 ( France )
| runtime         = 98 minutes
| country        = France Italy Germany Spain Switzerland United States
| language        = English, French
| budget          = €23,630,000
| gross           =
}}
 2001 documentary film directed by Jacques Cluzaud, Michel Debats and Jacques Perrin, who was also one of the writers and narrators, showcasing the immense journeys routinely made by birds during their Bird migration|migrations.

The film is dedicated to the French ornithologist Jean Dorst.

==Production==
The movie was shot over the course of four years on all seven continents. It was shot using in-flight cameras, most of the footage is aerial, and the viewer appears to be flying alongside birds of successive species, especially Canada geese. They traverse every kind of weather and landscape, covering vast distances in a flight for survival. The filmmakers exposed over 590 miles of film to create an 89-minute piece. In one case, two months of filming in one location was edited down to less than one minute in the final film.
 imprinted on staff members, and were trained to fly along with the film crews. The birds were also exposed to the film equipment over the course of their lives to ensure that the birds would react the way the filmmakers want. Several of these species had never been imprinted before. Film was shot from ultralight aviation|ultralights, paragliders, and hot air balloons, as well as trucks, motorcycles, motorboats, remote-controlled robots, and a French Navy warship. Its producer says that "Winged Migration" is neither a documentary nor fiction, but rather a "natural tale". "Making of" special feature on the DVD 

The film states that no special effects were used in the filming of the birds, although some entirely CGI segments that view Earth from outer space augment the real-life footage.

The films soundtrack by Bruno Coulais was recorded by Bulgarian vocal group Bulgarka Junior Quartet in Bulgarian, as well as Nick Cave in English and Robert Wyatt. The vocal effects include sequences in which panting is superimposed on wingbeats to give the effect that the viewer is a bird.

Release Date

*	France  2001/12/12                    
*	Belgium  2001/12/12
*	Switzerland  2001/12/13...(German speaking region)
*	Greece  2001/12/14
*	South Korea  2002/03/29
*	Germany  2002/04/04
*	Netherlands  2002/04/18
*	Hong Kong  2002/06/20
*	Russia  2002/06/21...(Moscow Film Festival)
*	Czech Republic  2002/07/8...(Karlovy Vary Film Festival)
*	Czech Republic  2002/07/18
*	Canada  2002/09/08...(Toronto Film Festival)
*	Turkey  2002/09/13
*	Israel  2002/09/19
*	Hungary  2002/09/19
*	Spain  2002/09/27
*	Italy  2002/11/15
*	Lithuania  2002/12/27
*	Poland  2003/04/04
*	Japan  2003/04/05...(Tokyo)
*	USA  2003/04/5...(Philadelphia International Film Festival)
*	Slovenia  2003/04/17
*	USA  2003/04/18...(Limited)
*	Norway  2003/05/02...(Kristiansand International Childrens Film Festival)
*	Canada  2003/05/30
*	Australia  2003/06/19
*	Norway  2003/06/20
*	New Zealand  2003/07/13...(Auckland International Film Festival)
*	Mexico  2003/07/25
*	Denmark  2003/08/15...(Copenhagen International Film Festival)
*	Finland  2003/09/05
*	UK  2003/09/05
*	Denmark  2003/09/12
*	Sweden  2003/ 11/07
*	Iceland  2004/01/23...(French Film Festival)
*	Argentina  2004/07/29
*	Chile  2005/03/31
*	Ireland  2011/07/10...(Galway Film Fleadh)

==Reception==
Winged Migration has an overall approval rating of 96% on Rotten Tomatoes. 
By gross ticket sales, the film still holds seventh place in nature documentaries   and eighteenth in documentary overall. 

==Awards and honors== Best Documentary Feature.    It won César Award for Best Editing|"Best Editing" at the 27th César Awards, where it was also nominated for César Award for Best Music Written for a Film|"Best Music" and César Award for Best Debut|"Best Debut".

•	Oscar 2003
Best Documentary Feature (nominated) -Jacques Perrin

•	European Film Award 2002
Best Documentary Feature (nominated) -Jacques Perrin

•	CFCA Award 2004
Best Cinematography (nominated) -Laurent Charbonnier
Best Cinematography (nominated) -Luc Drion
Best Cinematography (nominated) -Laurent Fleutot
Best Cinematography (nominated) -Sylvie Carcedo
Best Cinematography (nominated) -Philippe Garguil
Best Cinematography (nominated) -Olli Barbé
Best Cinematography (nominated) -Dominique Gentil
Best Cinematography (nominated) -Thierry Machado
Best Cinematography (nominated) -Stéphane Martin
Best Cinematography (nominated) -Fabrice Moindrot
Best Cinematography (nominated) -Ernst Sasse
Best Cinematography (nominated) -Thierry Thomas
Best Cinematography (nominated) - Michel Terrasse
Best Documentary (nominated)

•	Chicago Film Critics Circle Awards 2004
Best Cinematography (nominated) - Bernard Luti
Best Cinematography (nominated) - Michel Benjamin

•	César 2002
Best Editing (Meilleur montage) Marie-Josèphe Yoyotte
Best First Work (Meilleure première oeuvre) (nominated) - Michel Debats
Best First Work (Meilleure première oeuvre) (nominated) - Jacques Cluzaud

•	The European Film Award 2002
Best Documentary Award (Nominated) - Jacques Cluzaud
Best Documentary Award (Nominated) - Michel Debats

==Images==
  One of the CGI shots in Winged Migration, in which an Arctic tern flies above southern Africa
 

==See also==
* Bill Lishman - He imprinted geese and taught them to follow him in a low-speed ultralight aircraft in a migration path from Canada to Virginia. "The idea of the traveling birds we owe to him." Lishman appears in the "Making of" documentary on the DVD release. 

==References==
;Notes
 

 

== External links ==
*  
*    
*  
*  
*  on FFilms.org

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 