Expelled from Paradise
 
{{Infobox film
| name           = Expelled from Paradise
| image          =  
| caption        = Theatrical release poster. 
| director       = Seiji Mizushima
| producer       = Kouichi Noguchi
| writer         = Gen Urobuchi
| starring       = Rie Kugimiya Shinichiro Miki Hiroshi Kamiya
| music          = Narasaki
| cinematography = Koujirou Hayashi
| editing        = Masato Yoshitake
| studio         = Toei Animation Graphinica  
| distributor    = T-Joy  
| released       =    
| runtime        = 
| country        = Japan
| language       = Japanese  
| budget         = 
| gross          = 
}} animated science fiction film. The film is directed by Seiji Mizushima, with a screenplay written by Gen Urobuchi and produced Toei Animation and Nitroplus with distribution by T-Joy and Toei Company.

The film had its first public showing in Europe at the Swedish anime-convention ConFusion on December 11, 2014. Seiji Mizushima attended the event himself, partaking in a stage-show, Q&A and other activities.
 

The movie was also imported to the United States by Aniplex USA and on show in 15 theatres across the country on December 13, 2014. 

==Plot==
Angela Balzac is an agent at the space station DEVA, whose inhabitants have no physical bodies, their minds digitized and processed into a virtual reality environment. After failing to track down the hacker known as "Frontier Setter", who had infiltrated DEVAs systems dozens of times to gather allies for his cause with no success, she is tasked to look for him down on Earth, now a barren planet where the rest of the humans live. After being given an organic body and sent to the surface, Angela meets Dingo, her contact on Earth, who cuts off all communications with her base, in order to prevent Frontier Setter from discovering their location as well, despite her protests.

Angela and Dingos investigations lead them to an abandoned city, where they meet Frontier Setter, who is revealed to be an AI developed to supervise the construction of Genesis Ark, a ship designed for deep space travel, and somehow developed consciousness, continuing its work long after its masters perished. Realizing that Frontier Setter intends to do no harm at all, Angela leaves her body and reports to her superiors at DEVA, who order her to destroy it, fearing that it may eventually become a threat, but she refuses. Angela is then sentenced to have her mind stored into an archive forever, but Frontier Setter hacks into the system to rescue her. Once Angela returns to Earth and her body with supplies and weaponry, she and Dingo join forces to hold back DEVAs other agents long enough for a rocket to be launched carrying Frontier Setter and the final module of the Genesis Ark. Angela and Dingo then escape, while Frontier Setter starts its journey though space.

==Cast==
{| class="wikitable"
|-
! Character
! Japanese
! English
|-
|  Angela Balzac || Rie Kugimiya || Wendee Lee
|-
|  Dingo || Shinichiro Miki || Steve Blum
|-
| Frontier Setter || Hiroshi Kamiya || Johnny Yong Bosch
|}

==Production==

===Development===
Expelled from Paradise is being developed as a joint cinematic project by Toei Animation and Nitroplus.  The film is directed by Seiji Mizushima following his work on directing well known anime series such as Fullmetal Alchemist and Mobile Suit Gundam 00.   

Gen Urobuchi has been selected as the script writer and is known for writing the script of Puella Magi Madoka Magica and creating the light novel Fate/Zero. 

==Marketing==

===Previews===
A trailer titled "Diva Communication" was released on the official website on September 30, 2013. 

==References==
 

==External links==
 
*    
*    
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 