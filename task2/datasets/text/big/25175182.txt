George & A.J.
{{Infobox film
| name           = George & A.J.
| image          = George & A.J. poster.jpg
| alt            = The title screen from the Pixar short animated film "George and A.J." Against a blue background, there are nursing home-style nametags with the duos pictures on them, including their names. George is on the left. He is an African-American man with a thin moustache and a balding head. A.J. is a big hefty or overweight man with blonde hair and a mullet.
| caption        = Poster
| director       = Josh Cooley
| producer       = Galyn Susman Pete Docter (executive) Jonas Rivera (executive)
| writer         = Josh Cooley Bob Peterson
| music          = Michael Giacchino
| cinematography =
| editing        = Tim Fox Jon Vargo (assistant editor)
| studio         = Pixar Animation Studios Walt Disney Pictures Walt Disney Studios Home Entertainment
| released       =    
| runtime        = 4 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} the film.   It was later released to fans of Disney/Pixar on their Facebook page  and later to the official Disney/Pixar YouTube page.   It was later released on Pixar Short Films Collection Volume 2. All of the animation is done in a limited "storyboard" style, with some objects moving by Key frame|keyframes, while other things like characters and their mouths barely move at all; however, the characters expressions and the story are still conveyed.

==Plot==
The short starts out with a scene from Up, where two nurses from the Shady Oaks Retirement Village knock on Carl Fredricksens door to escort him to the nursing home. As seen in the film, Carl instead takes off in his house, while Russell braces himself underneath it. George and A.J. stare dumbfounded at the sky while their vans alarm goes off (it had been bumped by Carls house).

They continue to stare dumbfounded when they return on the van, and on bed during sleep time. The next day, a news reporter does a story about sightings of a floating house. She attempts to get George and A.J.s opinion, but they are still staring dumbfounded. Meanwhile, other senior citizens around the city, including at Shady Oaks, are watching the news report and celebrating Carls "escape". 

A week later, George and A.J. are back at their task of escorting elderly citizens to Shady Oaks in their van. Unfortunately, the seniors have gotten ideas from what Carl did, and every time George and A.J. come to a house to take a person away, the senior living there escapes with their house somehow. Finally, they arrive back at the Shady Oaks Retirement Village, only to find an old man outside the door who yells "So long, suckers!" and hits his cane against one of many canisters attached to the outside of the building; the other canisters soon follow and spew out a powerful gas. After the man goes inside, the entire building launches into the sky. Still dumbstruck, George and A.J. stare up. A few canisters fall down, and one lands on their van, setting off its alarm again. Suddenly, from behind them, a giant blimp starts to descend. It turns out to be Charles Muntzs Spirit of Adventure Airship|dirigible, and Carl is flying it with Russell next to him. The airship lands on top of the van, crushing it and causing to effectively stop its alarm. Carl and Russell climb out and George incredulously says "Mr. Fredricksen?" They walk past the nurses, and Russell mentions that next time hed like to steer. A.J. turns to George and says "That was the craziest thing Ive ever seen!" They look down to find Dug in front of them. Dug speaks "Hi there!" through his collar, and George and A.J. are even more shocked than before.

==Cast==
* Jason Topolski as George
* A.J. Riebli III as A.J.
* Steve Purcell  as Carl Fredricksen
* Peter Sohn as Russell Bob Peterson as Dug
* Kim Donovan as Reporter 
* Claire Munzer as Old Lady
* Valerie LaPoint as Cat Lady (Mrs. Peterson)

==Inconsistencies with Up==
 
* There are slight dialogue differences in the scene where George and A.J. first talk with Carl at his house. {{cite video date      = 2009-11-12 title      = George & A.J. medium     = Short Film publisher  = Pixar location   = Emeryville, CA accessdate = 2010-06-03 time       = 0:07
}} 
* In this short film, Russell is seen bracing himself underneath the floorboards of Carls house. {{cite video date      = 2009-11-12 title      = George & A.J. medium     = Short Film publisher  = Pixar location   = Emeryville, CA accessdate = 2010-06-03 time       = 0:39
}}  While this attempts to explain how Russell ended up on Carls porch later in the film, Russell is not seen underneath Carls house when it takes off in the film.
* In Up, Carls surname is always spelt as Fredricksen. Yet in the news items which are seen crawling across the screen during the news program, his surname is spelt "Fredrickson" twice, a difference that could be attributed to a mistake on the part of the news media. {{cite video date      = 2009-11-12 title      = George & A.J. medium     = Short Film publisher  = Pixar location   = Emeryville, CA accessdate = 2010-06-03 time       = 1:24
}}  {{cite video date      = 2009-11-12 title      = George & A.J. medium     = Short Film publisher  = Pixar location   = Emeryville, CA accessdate = 2010-06-03 time       = 1:29
}} 
* The Shady Oaks Retirement Village building blasts off into the sky in this film, {{cite video date      = 2009-11-12 title      = George & A.J. medium     = Short Film publisher  = Pixar location   = Emeryville, CA accessdate = 2010-06-03 time       = 2:53
}}  yet during the credits of Up, Carl is seen socializing with other residents of the home along with some of Muntzs dogs, implying he ended up living there after the film.
* At the end of the short, Carl is seen in the airship without his jacket. {{cite video date      = 2009-11-12 title      = George & A.J. medium     = Short Film publisher  = Pixar location   = Emeryville, CA accessdate = 2010-06-03 time       = 3:25
}}  Although through most of the end of the film he is seen without a jacket, he does have one at the end in the airship.

==References==
 

==External links==
*   at Pixars official page
*  


 
{{succession box
 | before = Dugs Special Mission short films
 | years  = 2010 Day & Night}}
 

 
 

 
 
 
 
 
 
 
 
 