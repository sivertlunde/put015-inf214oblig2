Naughty But Nice (1927 film)
{{Infobox film
| name           = Naughty But Nice
| image          = Naughty but nice.JPG
| image_size     = 225px
| caption        = theatrical release poster
| director       = Millard Webb
| producer       = John McCormick
| writer         = Carey Wilson 
| starring       = Colleen Moore
| cinematography = George Folsey
| editing        = Alexander Hall
| studio         = First National Pictures
| distributor    = First National Pictures
| released       =  
| country        = United States
| runtime        = 70 minutes
| language       = Silent film (English intertitles)
}}
 silent American comedy film. Colleen Moore plays Bernice Sumners, a hayseed sent to a ritzy boarding school for finishing after her family strikes it rich in oil. This film is notable for giving actress Gretchen Young &ndash; later known as Loretta Young &ndash; her start in films.

==Plot==
Bernice Sumners is sent to a finishing school by her Texas uncle after oil is discovered on his property. At the school she blossoms into a young woman. Bernice is a compulsive liar. One evening she and a friend go to a hotel before a theater date, planning to meet popular Paul Carroll, but they run into the school principal in the hotel lobby. Bernice tells a lie about why they are there, and from there one lie builds upon the other until Bernice ends up in the hotel room of Ralph Ames of the Secret Service, who is in the process of changing (thus, the poster graphic of a mans bare legs in garters). Bernice calls Ralph her husband, and he plays along until the house of cards comes crumbling down around her. She ends up falling for the popular Paul Carroll, and the two marry. 

==Cast==
* Colleen Moore as Berenice Summers
* Donald Reed as Paul Carroll	 
* Claude Gillingwater as Judge John R. Altwold	 
* Kathryn McGuire as Alice Altwold
* Hallam Cooley as Ralph Ames
* Edythe Chapman as Mrs. Altwold
* Clarissa Selwynne as Miss Perkins
* Burr McIntosh as Uncle Seth Summers

==References==
Notes
 

Bibliography
*Codori, Jeff (2012), Colleen Moore; A Biography of the Silent Film Star,  ,(Print ISBN 978-0-7864-4969-9, EBook ISBN 978-0-7864-8899-5).

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 