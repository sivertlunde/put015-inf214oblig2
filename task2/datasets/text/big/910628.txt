Bob Roberts
 
{{Infobox film
| name           = Bob Roberts
| image          = Bob_roberts_poster.jpg
| caption        = Theatrical release poster
| director       = Tim Robbins
| producer       = Forrest Murray
| writer         = Tim Robbins
| starring       = Tim Robbins Giancarlo Esposito Ray Wise Gore Vidal John Cusack Peter Gallagher Alan Rickman Susan Sarandon James Spader Fred Ward
| music          = David Robbins
| cinematography = Jean Lépine Lisa Churgin Live Entertainment StudioCanal Working Title Films
| distributor    = Paramount Pictures   PolyGram Filmed Entertainment  
| released       = September 4, 1992
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $4,479,470 (USA)
}} satirical mockumentary film written, directed by and starring Tim Robbins. It tells the rise of Bob Roberts, a right-wing politician who is a candidate for an upcoming United States Senate election. Roberts is well financed, due mainly to past business dealings, and is well known for his music, which presents conservative ideas with gusto.

The film is Robbins directorial debut, and is based on a short segment of the same name and featuring the same character that Robbins did for Saturday Night Live on December 13, 1986.

==Plot== conservative Republican Republican folk Brian Murray), a British documentary filmmaker who is following the Roberts campaign. Through his lens we see Roberts travel across the state, performing songs about drug users, lazy people and the triumph of traditional family values over the rebelliousness of the 1960s. As the campaign continues, Paiste remains in the lead until a scandal arises involving him and a young woman who was seen emerging from a car with him. Paiste claims that she was a friend of his granddaughter who he was driving home, but he cannot shake the accusations. 

Throughout the campaign, reporter Bugs Raplin (  in his right hand he physically could not have fired the gun. Following the incident, Raplin contends that Roberts was never actually shot and that the gun was fired into the ground.
 paralysed paraplegia|from the waist down, he is seen tapping his feet at a celebration party. While Terry Manchester is interviewing Roberts supporters outside the new Senators hotel, a boy runs up shouting, "Hes dead, hes dead, they got him!" When Manchester asks him what he is talking about, the boy shouts, "Bugs Raplin! Hes dead! They got him!" A joyful celebration breaks out among Roberts supporters, the shot changes to an image of his hotel room, and an upright walking shadow suggesting Roberts profile passes the window before the lights go out. The film ends with a radio news report about Raplins death at the hands of a right-wing politics|right-wing fanatic and a shot of Manchester standing in the Jefferson Memorial, looking at the words, "I have sworn upon the altar of God eternal hostility against every form of tyranny over the mind of man", inscribed there.

==Cast==
* Jack Black as Roger Davis, a fan of Roberts music and politics (film debut)
* Tim Robbins as Robert "Bob" Roberts, Jr., a folksinger and businessman who runs for the U.S. Senate
* Giancarlo Esposito as John Alijah "Bugs" Raplin, a journalist determined to expose Roberts 
* Alan Rickman as Lukas Hart III, Roberts campaign chairman
* Ray Wise as Chet MacGregor, Roberts campaign manager Brian Murray as Terry Manchester, a British documentary film-maker
* Gore Vidal as Senator Brickley Paiste, the incumbent Senator against whom Roberts campaigns
* Rebecca Jenkins as Delores Perrigrew, a member of Roberts campaign staff
* Harry Lennix as Franklin Dockett, a Roberts campaign aide Robert Stanton as Bart Macklerooney, a Roberts campaign aide
* Kelly Willis as Clarissa Flan, a folk musician Tom Atkins as Dr. Caleb Menck, Roberts personal doctor
* David Strathairn as Mack Laflin, Raplins attorney
* James Spader as Chuck Marlin, a local news anchor for station WLNO
* Pamela Reed as Carol Cruise, Chuck Marlins co-anchor at WLNO
* Helen Hunt as Rose Pondell, a field reporter at WLNO
* Peter Gallagher as Dan Riley, host of Good Morning, Philadelphia
* Lynne Thigpen as Kelly Noble, an interviewer on Good Morning, Philadelphia
* Kathleen Chalfant as Constance Roberts, Bob Roberts mother Matt McGrath as Burt, a friend and bandmate of Davis
* Anita Gillette as Mrs. Davis, wife of the mayor of Harrisburg, mother of Roger
* Susan Sarandon as Tawna Titan, a local news anchor for WFAC-TV News
* Fred Ward as Chip Daley, co-anchor at WFAC-TV News
* Fisher Stevens as Rock Bork, a field reporter at WFAC-TV News
* Bob Balaban as Michael Janes, Cutting Edge Live producer
* John Cusack as Cutting Edge Live host 
* Allan F. Nicholls as Cutting Edge Live director
* Robert Hegyes as Ernesto Galleano, a reporter
* Steve Pink as an angry Penn State protester

==Style==
The films style is drawn from a number of real and mock documentaries, and its shots are crafted to create this effect, in many cases through the use of hand-held cameras. Not only does Roberts character draw from 60s era iconography of Bob Dylan, it also contains scenes inspired by the 1967 documentary, Dont Look Back , made about the singer, employing a similar (although consciously constructed) cinema verité style.   The film also draws from Rob Reiners 1984 mockumentary This Is Spinal Tap which Robbins states is one of his favorite films,  and directly references this during the scene in which Roberts gets lost in an auditorium attempting to find the stage before his performance. In the case of Gore Vidals character, the majority of the lines were not scripted, and instead Vidal based his role upon his own political beliefs, and his real-life positions on many of the fictional election topics.  

==Response==
 media in anachronistic in the context of the 1990s,   but others have praised it for framing political commentary as a Hollywood comedy. 

Bob Roberts currently holds a 100% approval rating on Rotten Tomatoes.

American Film Institute recognition:
* AFIs 100 Years... 100 Laughs - Nominated 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 