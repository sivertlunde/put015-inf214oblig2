Scrambled Brains
{{Infobox Film |
  | name           = Scrambled Brains
  | image          = Scrambledbrains 1sht.jpg
  | image size     = 190px
  | caption        = 
  | director       = Jules White  Felix Adler
  | starring       = Moe Howard Larry Fine Shemp Howard Babe London Vernon Dent Emil Sitka
  | cinematography = Henry Freulich
  | editing        = Edwin H. Bryant
  | producer       = Jules White 
  | distributor    = Columbia Pictures 
  | released       =   
  | runtime        = 15 50"
  | country        = United States
  | language       = English
}}

Scrambled Brains is the 132nd short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
Moe and Larry are at a sanatorium where Shemp is being treating for suffering from hallucinations. Shemp says he is indeed cured, and insists on saying farewell to his new Engagement|fiancée, beautiful nurse Nora (Babe London). When Nora calls out to Shemp, she appears, much to the scare of Moe and Larry: poor Nora is a homely, toothless thing who seems to have won Shemps heart. It then becomes clear that Shemp is far from cured, and needs additional therapy.
 nervous breakdown, Shemp insists on seeing Nora, with hopes of finally getting married. 
 ) in Scrambled Brains]]

On their way to the doctor, the Stooges become wedged in a phone booth with a stranger (Vernon Dent), leading to a fist and pie fight. Back in their apartment, they find she is waiting for her father who happens to be the man the Stooges brawled with in the phone booth.

==Production notes==
Larry Fine often cited this film as his all-time favorite, with You Nazty Spy! and Cuckoo on a Choo Choo his runner ups. He would often screen this film during his last days residing at the Motion Picture & Television Country House and Hospital. 

The gag of making a doll whine by leaning a rocking chair on it was borrowed from Laurel and Hardys 1940 film Saps at Sea. 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 