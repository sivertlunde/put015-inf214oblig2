Driving Miss Wealthy
 
 
{{Infobox film
| name = Driving Miss Wealthy
| writer = Jessica Fong Lo Yiu-Fai James Yuen
| starring = Lau Ching-Wan Gigi Leung
| director = James Yuen
| producer = Henry Fong Derek Yee
| music    = Raymond Wong Ying-Wah
| editing  = Leung Kwok-Wing
| studio   = One Hundred Years of Film Co. Ltd. Film Unlimited S & W Entertainment Youth Film Studio
| distributor = China Star Entertainment Group
| released = 3 May 2004
| runtime = 103 min.
| country =Hong Kong Cantonese
| awards = 
}}
 Hong Kong romantic comedy film  that reunites La Brassieres Lau Ching-Wan and Gigi Leung.  In the film, Lau poses a chauffeur hired to look after the spoiled rich woman played by Leung.

==Synopsis==

Leung plays Jennifer, the spoiled-rotten daughter of a millionaire. When Jennifers father realizes that shes spending way too much money, he hires Kit (Lau) to pretend to be a Filipino chauffeur and chaperone her. Then, Jennifers father decides that hes going to teach her the value of money and hard-work, so he pretends to be ill, leaving all the money to Pamela, his business partner. Pamela kicks Jennifer out into the street to live with Kit/Mario. The two learn to live together and work hard to get back on top.

==Cast==
* Lau Ching-Wan
* Gigi Leung
* Jamie Luk
* William Tuen
* Jim Chim

==External links==
*  

 
 
 
 


 