Pál Adrienn
{{Infobox film
| name           = Pál Adrienn
| image          = PálAdrienn2010Poster.jpg
| caption        = Film poster
| director       = Ágnes Kocsis
| producer       = Jean Bréhat
| writer         = Ágnes Kocsis Andrea Roberti
| starring       = Éva Gábor (actor)|Éva Gábor
| music          = 
| cinematography = Ádám Fillenz
| editing        = Tamás Kollányi
| distributor    = 
| released       =  
| runtime        = 136 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

Pál Adrienn is a 2010 Hungarian drama film directed by Ágnes Kocsis.  It was screened in the Un Certain Regard section at the 2010 Cannes Film Festival.   

==Cast==
* Éva Gábor (actor)|Éva Gábor
* István Znamenák
* Ákos Horváth
* Lia Pokorny
* Izabella Hegyi

==Awards==
* 63rd Cannes Film Festival, Un Certain Regard – FIPRESCI Award
* Sarajevo Film Festival – Cinelink: Magic Box Award for the screenplay (2007)   
* Jameson Cinefest International Film Festival, Miskolc, Hungary - Film Critics Award
* Arsenals Int. Film Festival, Riga, Latvia - Condamnation of the Interfilm Jury
* Zurich Film Festival – Critics Choice Award
* Cinepécs - Hungary - Film New Europe Visegrád Award 
* 2in1 Festival, Moscow – “Prize for the Best Hero(ine)” for the best performance to Éva Gábor
* Manaki Brothers International Cinematographers Film Festival, Bitola -Award for exceptional artistic achievement in the art of cinematography 
* Cottbus Film Festival – Prize for the Best Director
* Cottbus Film Festival –Prize for an Outstanding actress to Éva Gábor
* Cottbus Film Festival – Prize of the Ecumenical Jury
* The Hungarian Film Critics’ Award for the best director of the year
* The Hungarian Film Critics’ Award for the best cinematography of the year

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 