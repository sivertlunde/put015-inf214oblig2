The Zombinator
 
{{Infobox film
| name           = The Zombinator
| image          = The Zombinator.jpg
| alt            = 
| caption        = 
| director       = Sergio Myers
| producer       = Sergio Myers Patrick Kilpatrick
| writer         = Sergio Myers
| starring       = Patrick Kilpatrick Joseph Aviel
| music          = Todd Maki
| cinematography = Travis Cleary Russ Welch
| editing        = Charles Dayment
| studio         = 7 Ponies Productions
| distributor    = Shoreline Entertainment
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Zombinator (Dead Z in the UK) is a 2012 American horror film written and directed by Sergio Myers. It stars Patrick Kilpatrick and Joseph Aviel. Aviel plays a zombie hunter who tries to protect a group of college students from a mercenary (Kilpatrick) and the zombies that his employer has created.

== Plot ==
Nina, a fashion blogger, goes to Youngstown, Ohio to shoot a documentary but is caught up in a zombie apocalypse. An ex-soldier and zombie hunter, the Zombinator, appears in the city and tries to protect the citizens from the machinations of an evil corporation and their mercenary, the Colonel.

== Cast ==
* Patrick Kilpatrick as The Colonel
* Joseph Aviel as the Zombinator
* Lucia Brizzi as Nina

== Production ==
The Zombinator was shot in four and a half days in Youngstown, Ohio.     Director Sergio Myers was in Youngstown to film a documentary for fashion website frockOn.com when he was inspired to make a zombie film. He then recruited several members of frockOn.com to help make the film.   Shooting took place without a script. 

== Release ==
In 2012, The Zombinator was picked up by Shoreline Entertainment.   It premiered in Youngstown in December 2012.   A wider U.S. release was planned for 2014.   It was released on DVD May 13, 2014. 

== Reception == The Mercury rated it 1.5/4 stars and wrote that it has a "likeable D-grade premise and typical D-grade execution".   Garon Cockrell of The Pratt Tribune wrote, " his movie started off surprisingly well. But the filmmakers should have realized that the whole documentary crew conceit was just not going to work."   Tom Doty of The Floyd County Times, wrote, "Finally a zombie flick so dull and uninspired that it may slow down the deluge of undead straight-to-video flicks."   Daryl Loomis of DVD Verdict wrote, "Im not sure that theres anything we need to see less in horror than a new found footage zombie movie."   Mark L. Miller of Aint It Cool News wrote, "It feels like the intentions are there to attempt to be something different by glomming a bunch of different things together, but instead the film feels like an unfocused messterpiece." 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 


 