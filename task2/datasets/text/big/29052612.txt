La Zizanie (film)
{{Infobox film
| name           = La Zizanie
| image          = La-Zizanie-poster.jpg
| caption        =
| director       = Claude Zidi
| producer       = Bernard Artigues
| writer         = Claude Zidi Michel Fabre
| starring       = Louis de Funès Annie Girardot
| music          = Vladimir Cosma
| cinematography = Claude Renoir
| editing        = Monique Isnardon Robert Isnardon
| distributor    = A.M.L.F
| released       = 16 March 1978
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         =
}}
La Zizanie is a 1978 French comedy film directed by Claude Zidi, written by Zidi and Michel Fabre, and starring Louis de Funès and Annie Girardot. The English title is The Spat. 

== Cast ==
* Louis de Funès as Guillaume Daubray-Lacaze
* Annie Girardot as Bernadette Daubray-Lacaze
* Maurice Risch as the imbecile
* Jean-Jacques Moreau as the foreman
* Geneviève Fontanel as Madame Berger
* Jacques François as the prefect
* Georges Staquet as the union representative Mario David as the lorry driver

== References ==
 

==External links==
*  
*  
*  
*   at the Films de France

 

 
 
 
 
 
 
 


 
 