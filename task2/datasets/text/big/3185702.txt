Starcrash
{{Infobox film
 | name           = Starcrash
 | image          = Starcrash 1979 film poster.jpg
 | image_size     = 200px
 | caption        = Theatrical poster by John Solie
 | director       = Luigi Cozzi
 | producer       = Nat Wachsberger Patrick Wachsberger
 | writer         = Luigi Cozzi
 | starring       = Marjoe Gortner Caroline Munro Judd Hamilton Robert Tessier Christopher Plummer David Hasselhoff Joe Spinell John Barry
 | cinematography = Paul Beeson Roberto DEttorre Piazzoli
 | editing        = Sergio Montanari
 | distributor    = New World Pictures (USA)
 | released       = March 9, 1979
 | runtime        = 94 min.
 | country        = USA, Italy
 | language       = English / Italian
 | budget = $4 million Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 158-159 
}}
 American 1978 English title of The Adventures of Stella Star (in the US). The screenplay was written by Luigi Cozzi (pen name Lewis Coates) {{cite book
 | first=Steve | last=Ryfle | year=1999
 | title=Japans favorite mon-star: the unauthorized biography of "The Big G" directed the film. The cast included Marjoe Gortner, Caroline Munro, Judd Hamilton, Christopher Plummer, David Hasselhoff, Joe Spinell and Robert Tessier {{cite web
 | first=Jeremy | last=Wheeler | title=Star Crash (1978)
 | publisher=The New York Times | url=http://movies.nytimes.com/movie/46473/Star-Crash/overview
 | accessdate=2009-04-12 John Barry Somewhere in Time, Dances with Wolves).

It was filmed in Technicolor with Dolby sound, and has a runtime of 94 minutes. The US release is 92 minutes, and received an MPAA rating of PG. The film is generally regarded by some critics as a campy B movie with cheap special effects and a weak, derivative plot that some people find unintentionally humorous. It appeared a year after the original Star Wars and tried to re-mix the same elements, and later gain a cult following. {{cite web
 | first=Phil | last=Hall | date=2005-05-13
 | title=The Bootleg Files: "Starcrash"
 | publisher=Film Threat | url=http://www.filmthreat.com/index.php?section=features&Id=1456
 | accessdate=2009-04-12 }}  {{cite web
 | author=Holland, Chris; Hamilton, Scott | date=1999-05-19
 | title=Starcrash | publisher=Stomp Tokyo
 | url=http://www.stomptokyo.com/movies/s/starcrash.html
 | accessdate=2009-04-12 }}  Flash Gordon, Star Trek and Barbarella (film)|Barbarella.

In 2004, nationally syndicated television series Cinema Insomnia released a DVD version hosted by Mr. Lobo. The film was later picked up by Shout! Factory, who released it on DVD and Blu-ray in 2010 as part of the "Roger Cormans Cult Classics" series.

==Plot==
In a distant galaxy, a starship searches for the evil Count Zarth Arn. Closing in on a planet, the ship is attacked by a mysterious weapon (a red blobby field) which drives the crew insane. Three escape pods launch during the attack, but the ship crashes into the atmosphere of the planet and is destroyed.

Meanwhile, outlaw smuggler Stella Star and her sidekick Akton run into the Imperial Space Police, led by robot sheriff Elle. Akton and Stella escape by jumping into hyperspace. When they emerge, they discover an escape pod from the attacked starship, and in it, a sole survivor. Before they can escape, they are apprehended by the police, who tracked their hyperspace trail.

Tried, convicted and found guilty of piracy, they are each sentenced to life in prison on separate prison planets. When a riot breaks out at Stellas prison, she uses the diversion to escape the prison, which explodes shortly afterwards. Police Chief Thor recaptures her and she is taken to an orbiting ship, where she is reunited with Akton. They are contacted holographically by the Emperor of the Galaxy, who thanks them for recovering the starship survivor. They are offered clemency if they help find three more missing escape pods, one of which may contain the Emperors only son.

The Emperor is in a feud with Count Zarth Arn, who has a secret weapon hidden away on a planet somewhere. The weapon has immense power, and he can use it to take over the galaxy. The Emperor orders Stella and Akton to find the Counts weapon, and find and rescue his son. With Chief Thor and Elle accompanying them, Stella and Akton set off on their quest.

They have a long way to travel, but Stella excitedly notes that hyperspace can make a journey that wouldve taken two months take only two hours. They quickly arrive at the location Akton computes for the first escape pod.

Stella and Elle take a shuttle from the spaceship and land near the pod on a sandy, rocky beach. There are no living survivors. Stella meets an amazonian warrior tribe and is escorted to their underground fortress. On arrival, Elle is ambushed, shot and left for dead, and Stella is taken captive. Stella is taken before Corelia, Queen of the Amazons, who is in league with Zarth Arn. Elle doesnt actually die, and he makes his way to the throne room, taking Corelia hostage to secure Stellas release. They escape, but the queen mentally activates a giant female robot which chases them. Things look dire until the pair are rescued by Akton and Thor, in the spaceship.

On a desolate and uninhabited snow-covered frozen planet, Stella and Elle investigate the second escape pod crash site, but as with the first crash site, they find no survivors. Upon their return to the ship, Thor, who has ambushed and apparently knocked out Akton, reveals that he is an agent of Zarth Arn, and will shortly join him as his Prince of Darkness. Thor locks Stella and Elle outside on this planet where the temperature drops "thousands of degrees" at night, where he knows they will freeze to death. Elle and Stella lie down to freeze, and Elle takes Stellas hand so he can keep her heart going in "suspended animation".

Akton revives himself and battles Thor. Thor gains the upper hand and tries to crush Akton, but Aktons powers suddenly escalate, and he repels Thors attack and absorbs blaster fire seemingly without effort, to Thors considerable disbelief. Akton reflects Thors final laser shots back towards him with his hand, killing him. But the sun has already set, and the planets surface is frozen solid. When morning comes, Akton brings Elle and frozen Stella back onto the ship, where he uses his powers to thaw her out.

On the planet of the third escape pod, Stella and Elle are attacked by barbarian tribesmen who smash Elle to pieces and abduct Stella. She escapes and flees to a nearby cave where she is attacked by more tribesmen, but a man in a golden mask arrives, firing lasers through his eyes, and rescues her. He is revealed to be the Emperors son, Simon. Akton arrives and starts a laser sword duel with Simon, unaware of his identity. Simon proves his relation to the Emperor and the trio set off to find the Counts secret weapon.

Arriving at an underground laboratory, the three are captured by the guards. The Count appears and reveals his plan to use them as bait to bring the Emperor to the planet, and then have his weapon self-destruct, destroying the planet, the Emperor, and all of them. He leaves to conquer the Emperors home world, ordering his two robot golems to keep the group there. Akton engages them in a laser sword duel, and the trio eventually defeat the robots, but Akton is mortally wounded. He says goodbye, and vanishes in a plume of electrical fuzz. The Emperor arrives at the planet. He is aware that the whole thing is mined with nuclear bombs as a trap, and that they are out of time. He quips that one doesnt get to be Emperor without getting some perks, and uses a green ray from his flagship to "stop time" for three minutes, giving them all enough time to escape. The flagship pulls away as the planet explodes behind it.

Stella stands with the Emperor on his flagship, as a huge battle commences between his armada and the Counts space station. The Count also attacks the Emperors homeworld, but the attack is a failure. The Emperors soldiers storm the space station, but after a pitched battle they are stopped short and killed by the Counts reinforcements.

With no option left, the Emperor decides to ram his flagship into the Counts space station, destroying them both. But Elle has been salvaged and rebuilt by the Emperors men, and Stella and Elle volunteer to commandeer an evacuated space station, the Floating City, and to smash it into the Counts station. They fly the city towards the space station, and manage to escape together just as their station crashes into the Counts, finally winning the war.

Stella and Elle are picked up the Emperors son Simon. He is happy Stella survived and asks her to marry him.

==Main cast==
*   for English language prints of the film.
*   lightsaber.
* Judd Hamilton - Elle: A powerful robot policeman endowed with emotions, who ends up helping Stella and Akton. Apparently destroyed by cavemen on the third planet, he comes back later after being repaired by the Emperors men.
* David Hasselhoff - Simon: the Emperors only son, only survivor of Zarth Arns assault on his ship.
* Christopher Plummer - The Emperor: The known universes benevolent and wise ruler, whose only son has disappeared after an encounter with the space forces of evil Count Zarth Arn.
* Joe Spinell - Count Zarth Arn: a megalomaniac renegade, who is bent on dethroning the Emperor and proclaiming himself supreme ruler of the universe.
* Robert Tessier - Thor: Chief of the Imperial State Police, and Elles superior. Turns out to be a traitor working for Zarth Arn. Knocks out Akton on the second planet, believing him dead, but is then killed by Akton who is able to deflect laser blasts with his hands.
* Nadia Cassini - Corelia: Queen of the Amazon women on the first planet that Stella and her crew visit. She is an ally of Count Zarth Arn.

==Soundtrack== John Barry. The soundtrack was given a limited release of 1,500 copies through BSX Records in December 2008 and features fourteen tracks of score. 

#"Starcrash Main Title" (2:36)
#"Escape Into Hyperspace" (1:49)
#"Captured" (2:09)
#"Launch Adrift" (1:42)
#"Beach Landing" (2:09)
#"The Ice Planet/Heading for Zarkon" (3:03)
#"The Emperors Speech" (3:17)
#"Strange Planet/The Troggs Attack" (2:37)
#"Akton Battles the Robots" (2:18)
#"Network Ball Attack" (1:00)
#"Space War" (4:40)
#"Goodbye Akton" (3:34)
#"Starcrash End Title" (2:57)
#"Starcrash Suite" (7:14)

==Production==
Shooting took over six months and was frequently brought to a halt due to financing problems. The film was originally made for American International Pictures but after seeing the final cut they declined to release it. New World Pictures stepped in instead. 

The 1981 science fiction film  Escape from Galaxy III was also known as Starcrash II. 

Christopher Plummer said of Starcrash: " ive me Rome any day. Ill do porno in Rome, as long as I can get to Rome. Getting to Rome was the greatest thing that happened in that for me. I think it was only about three days in Rome on that one. It was all shot at once."  Of his role, he said: " ow can you play the Emperor Of The Universe? What a wonderful part to play. It puts God in a very dicey moment, doesnt it? Hes very insecure, God, when the Emperor’s around." 

==Reception==
Kurt Dahlke of DVD Talk said, "Starcrash is a masterpiece of unintentionally bad filmmaking. Pounded out in about 18 months seemingly as an answer to Star Wars, Luigi Cozzis knock-off buzzes around with giddy brio, mixing ridiculous characters with questionably broad acting, an incredibly simple yet still nonsensical plot derivative to Star Wars, and budget special effects that transcend into the realm of real art. Its a completely ridiculous movie, thats great to watch with a few friends and a beer or two. And it still manages to make my jaw drop." 
 The Room. 

==Awards== 7th Saturn Best Foreign Film. 

==References==
 

==External links==
* 
*  at the Internet Archive
* 
* 
* 
*  at www.badmovies.org
*  at terrediconfine.eu  
*  at Livestream

 

 
 
 
 
 
 
 
 
 
 
 