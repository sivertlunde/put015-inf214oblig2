Pride and Prejudice (1940 film)
 
{{Infobox film
| name           = Pride and Prejudice
| image          = Prideundprejudice.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Robert Z. Leonard
| producer       = Hunt Stromberg
| writer         = Helen Jerome  
| screenplay     = {{Plainlist|
* Aldous Huxley
* Jane Murfin
}}
| story          = 
| based on       =  
| starring       = {{Plainlist|
* Greer Garson
* Laurence Olivier
* Mary Boland
* Edna May Oliver
* Maureen OSullivan
* Ann Rutherford
* Frieda Inescort
}}
| music          = Herbert Stothart
| cinematography = Karl Freund
| editing        = Robert Kern MGM
| distributor    = MGM
| released       =  
| runtime        = 117 minutes
| language       = English
| country        = United States
| budget         = $1,437,000 Glancy, H. Mark "When Hollywood Loved Britain: The Hollywood British Film 1939-1945" (Manchester University Press, 1999)   . 
| gross          = $1.8&nbsp;million  
}}
 MGM on July 26, 1940 in the United States, and was critically well received. The New York Times film critic praised the film as "the most deliciously pert comedy of old manners, the most crisp and crackling satire in costume that we in this corner can remember ever having seen on the screen." 

The film differs from the novel in a number of ways. The period of the film, for example, is later than that of Austens novel—a change driven by the studios desire to use more elaborate and flamboyant costumes than those from Austens time period. Some scenes were altered significantly. For example, in the confrontation near the end of the film between Lady Catherine de Bourgh and Elizabeth Bennet, the formers haughty demand that Elizabeth promise never to marry Darcy was changed into a hoax to test the mettle and sincerity of Elizabeths love. In the novel, this confrontation is an authentic demand motivated by Lady Catherines snobbery and, especially, by her ardent desire that Darcy marry her own daughter.

==Plot==
Mrs. Bennet (Mary Boland) and her two eldest daughters, Jane (Maureen OSullivan) and Elizabeth (Greer Garson), are shopping for new dresses when they see two gentlemen and a lady alight from a very expensive carriage outside. They learn that the men are Mr. Bingley (Bruce Lester), who has just rented the local estate of Netherfield, and Mr. Darcy (Laurence Olivier), both wealthy, eligible bachelors, which excites Mrs. Bennet. Collecting her other daughters, Mrs Bennet returns home, where she tries to make Mr. Bennet see Mr. Bingley, but he refuses, having already made his acquaintance. 

At the next ball, Elizabeth sees how proud Mr. Darcy is when she overhears him refusing to dance with her, and also meets Mr. Wickham, who tells Elizabeth how Mr. Darcy did him a terrible wrong. When Mr. Darcy does ask her to dance with him, she refuses, but when Mr. Wickham asks her right in front of Darcy, she accepts.

The Bennets cousin, Mr. Collins (Melville Cooper), who will inherit the Bennet estate upon the death or Mr. Bennet, arrives, looking for a wife, and decides that Elizabeth will be suitable. At ball held at Netherfield, he keeps following her around and wont leave her alone. Mr. Darcy surprisingly helps her out, and later asks her to dance. After seeing the reckless behaviour of her mother and younger sisters however, he leaves her again, making Elizabeth very angry with him once more. The next day, Mr. Collins asks her to marry him, but she refuses point blank. He then goes and becomes engaged to her best friend, Charlotte Lucas (Karen Morley). 

Elizabeth visits Charlotte in her new home. There, she is introduced to Lady Catherine de Bourgh (Edna May Oliver), and also encounters Mr. Darcy again. Later, he asks her to marry him, but she refuses, partly due to the story Wickham had told her about Darcy depriving him of his rightful fortune, and also because she has just learned that he broke up the romance between Mr. Bingley and Jane. They get into a heated argument and he leaves. 

When Elizabeth returns to Longborn, she learns that Lydia has eloped with Wickham. Mr. Darcy visits her and tells her that Wickham will never marry Lydia. He reveals that Wickham had tried to elope with his then 15-year-old sister, Georgiana. After he leaves, Elizabeth realizes that she loves him, but believes he will never see her again because of Lydias disgraceful act. Lydia and Wickham return married to the house. Later, Lady Catherine visits and reveals that Mr. Darcy found Lydia and forced Wickham to marry her. Darcy reappears, and he and Elizabeth proclaim their love for each other. The movie ends with a long kiss between Elizabeth and Darcy, with Mrs. Bennet spying on them and seeing how her other daughters have found good suitors.

==Cast==
 
 
* Greer Garson as Elizabeth Bennet
* Laurence Olivier as Fitzwilliam Darcy
* Mary Boland as Mrs. Bennet
* Edna May Oliver as Lady Catherine de Bourgh
* Maureen OSullivan as Jane Bennet
* Ann Rutherford as Lydia Bennet
* Frieda Inescort as Caroline Bingley
* Edmund Gwenn as Mr. Bennet (Pride and Prejudice)|Mr. Bennet
* Karen Morley as Charlotte Lucas Collins Heather Angel as Kitty Bennet Marsha Hunt as Mary Bennet
* Melville Cooper as Mr. Collins Edward Ashley George Wickham
* Bruce Lester as Mr. Bingley
* E.E. Clive as Sir Willam Lucas
* Majorie Wood as Lady Lucas
* Vernon Downing as Captain Carter

==Production==
The filming of Pride and Prejudice was originally scheduled to start in October 1936 under Irving Thalbergs supervision, with Clark Gable and Norma Shearer cast in the leading roles.    Following Thalbergs death on September 13, 1936, pre-production activity was put on hold. In August 1939, MGM had selected George Cukor to direct the film, with Robert Donat now cast opposite Shearer.  The studio had considered filming in England, but these plans were changed at the start of the war in Europe in September 1939, which caused the closure of MGMs England operations. Cukor was eventually replaced by Robert Z. Leonard due to a scheduling conflict. 

==Reception==
The film was critically well received. Bosley Crowther in his review for The New York Times described the film as "the most deliciously pert comedy of old manners, the most crisp and crackling satire in costume that we in this corner can remember ever having seen on the screen." Crowther also praised casting decisions and noted of the two central protagonists:
  }}

TV Guide, commenting upon the changes made to the original novel by this adaptation, calls the film "an unusually successful adaptation of Jane Austens most famous novel. Although the satire is slightly reduced and coarsened and the period advanced in order to use more flamboyant costumes, the spirit is entirely in keeping with Austens sharp, witty portrait of rural 19th century social mores." The reviewer goes on to note:
  }}

The film received an 88% rating from Rotten Tomatoes (7 fresh and 1 rotten reviews). 

==Box Office==
According to MGM records the film earned $1,849,000, resulting in a loss of $241,000. 

==Differences between the film and novel==
 
Among aficionados of Jane Austens original novel, this movie adaptation diverges drastically from the novel and being excessively "Hollywoodized"—and for putting the women in clothes based on the styles of the late 1820s and the styles of the 1830s which were different from the Regency styles appropriate to the novels setting.

The timeframe of the movies plot progression is noticeably compressed, with certain events being juxtaposed in the film that were separated by days, weeks or even months in the novel. Wickham did not attend the Meryton ball where Darcy first insults Elizabeth, and in fact never attended any ball at which Darcy was also present. There is an archery scene between Darcy and Elizabeth that is not present at all in the novel. Darcys revelation of Wickhams attempted elopement with his own sister, Georgiana, is not done in person after Lydia and Wickhams elopement as is shown in the movie; in the novel, he describes these events to Elizabeth in the letter he delivers to her the day after she turns down his marriage proposal during her stay at Hunsford.

The film eliminates two journeys:
#In the novel, Jane travels to London with the Bennett girls Aunt and Uncle Gardiner after the Christmas holiday. The Bingleys, the Hursts and Darcy had already left Hertfordshire for London at the time, shortly after the Netherfield ball on 26 November, and Jane specifically goes in the hopes of seeing Charles Bingley and continuing their fledgling courtship.  Darcy and Bingleys sisters actively conceal her being in London from Charles in their efforts to put an end to Charles and Janes budding romance, which Darcy confesses to during his proposal to Elizabeth at Hunsford.
#Later in the novel, Elizabeth plans an extensive tour of the lake country with the same aunt and uncle, only to have her uncles business responsibilities cut short the time available for the trip and rerouting the trio to Derbyshire. In Derbyshire, Elizabeth has an opportunity to see Pemberly, Darcys home estate, and to see a different side of Darcy than she had encountered previously. She also meets Georgiana Darcy and develops a rapport with the shy teenager, to Darcys great pleasure. This is the first time Darcy has seen her since the disastrous marriage proposal as well, and his first opportunity to show her how much he has changed in the wake of her scathing rejection and rebukes. It is during this trip that Elizabeth and Darcy both find out about Lydias elopement, via Janes letters to Elizabeth begging her to return with their aunt and uncle in the face of the family crisis.

Also changed is the timing of Lydias elopement with Wickham. In the novel, Lydia follows Wickhams regiment to Brighton as the companion of the colonels wife, and elopes with Wickham from there. As described above, this takes place while Elizabeth is in Derbyshire with her Aunt and Uncle Gardiner, not after Elizabeths return from her visit to the Collins at Hunsford as happens in the movie. When Lydia and Wickham visit Longbourne after their wedding in the novel, which is several weeks after the initial elopement and Elizabeths return from Derbyshire, it is Lydia who reveals that Darcy was at their wedding, and it is Elizabeths Aunt Gardiner who, by letter, reveals his role in getting the elopees to the altar. Mary and Kitty do not have suitors at the end of the novel.

The following characters were eliminated completely from this film adaptation:
* Georgiana Darcy (Fitzwilliam Darcys younger sister)
* Mr. and Mrs. Gardiner (Elizabeths maternal uncle and his wife)
* Colonel and Mrs. Forster (the commanding officer of the Meryton regiment and his wife, whom Lydia goes to Brighton as a companion to in the novel)
* Maria Lucas (Charlotte Lucas younger sister)
* Mr. and Mrs. Hurst (Charles Bingleys married sister and her husband)

==Awards== Best Art Direction, Black and White (Cedric Gibbons and Paul Groesse).   

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 