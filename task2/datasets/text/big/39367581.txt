Dusty Ermine
{{Infobox film
| name = Dusty Ermine
| image =
| image_size =
| caption =
| director = Bernard Vorhaus
| producer = Julius Hagen
| writer = Neil Grant (play)   Arthur Macrae    Paul Hervey Fox   Lawrence du Garde Peach   Harry Fowler Mear   Michael Hankinson
| narrator =
| starring = Anthony Bushell   Jane Baxter   Ronald Squire   Margaret Rutherford
| music = W.L. Trytel 
| cinematography = Curt Courant   Otto Martini   Kurt Neubert
| editing = Ralph Kemplen
| studio = Julius Hagen Productions
| distributor = Twickenham Film Distributors   Grand National Pictures (US)
| released = 10 September 1936
| runtime = 84 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} play of the same title by Neil Grant.

==Production== Elstree complex, and also included extensive location filming in the Alps.  It was directed by Vorhaus who had worked on a number of earlier films for the company. Vorhaus was so impressed by the performance of Margaret Rutherford in a theatre production he saw her in, that he insisted on casting her in the film. He added a new comic relief role to the original play especially for her.  The films art direction was by Andrew Mazzei.
 distribution led to financial problems and the collapse of his company the following year during the Slump of 1937.  Vorhaus directed one further British film, Cotton Queen, before returning to America.

==Synopsis==
After being released from prison Jim Kent, a leading forger, is approached by an international counterfeiting organisation. He rejects their offer of employment as he intends to go straight, but when he discovers that his nephew is now working for the outfit he travels to Switzerland to try and help him out. An ambitious young detective from Scotland Yard is also on the trail of the forgery ring, and mistakenly comes to the conclusion that Jim Kent is still working as a master counterfeiter.

==Cast==
* Anthony Bushell as Detective Inspector Forsythe 
* Jane Baxter as Linda Kent 
* Ronald Squire as Jim Kent 
* Arthur Macrae as Gilbert Kent 
* Margaret Rutherford as Evelyn Summers
* Austin Trevor as Swiss Hotelier-Gang Leader 
* Davina Craig as Goldie, the maid 
* Athole Stewart as Mr. Kent 
* Katie Johnson as Emily Kent 
* Felix Aylmer as Police Commissioner 
* Hal Gordon as Detective Sergeant Helmsley  George Merritt as Police constable 
* Wally Patch as Thug

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film: Filmmaking in 1930s Britain. George Allen & Unwin, 1985 .
* Richards, Jeffrey (ed.). The Unknown Thirties: An Alternative History of the British Cinema, 1929-1939. I.B. Tauris & Co, 2000.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 