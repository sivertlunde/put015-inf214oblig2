A Civil Action (film)
{{Infobox film
| name           = A Civil Action
| image          = A Civil Action poster.jpg
| director       = Steven Zaillian
| caption        = Theatrical release poster
| producer       = Scott Rudin Steven Zaillian David Wisnievitz Robert Redford Rachel Pfeffer David McGiffert Henry J. Golas
| writer         = Steven Zaillian
| based on       =  
| starring       = John Travolta Robert Duvall James Gandolfini Dan Hedaya John Lithgow William H. Macy Kathleen Quinlan Tony Shalhoub Stephen Fry James Gandolfini
| music          = Danny Elfman
| cinematography = Conrad L. Hall
| editing        = Wayne Wahrman
| studio         = Touchstone Pictures Paramount Pictures Buena Vista Pictures   United International Pictures  
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = $75 million
| gross          = $112,215,759
}} book of the same name by Jonathan Harr.  Both the book and the film are based on a true story of a court case about environmental pollution that took place in Woburn, Massachusetts|Woburn, Massachusetts, in the 1980s. 
 reported decision in the case is at 96 F.R.D. 431 (denial of defendants motion to dismiss). 

Duvall was nominated for an Academy Award for his performance.

==Plot==
Environmental toxins in the city of Woburn, Massachusetts contaminate the areas water supply, and become linked to a number of deaths of neighboring children. Jan Schlichtmann (John Travolta), a cocky and successful Boston attorney who zips around town in his Porsche, and his small firm of personal injury lawyers are asked by Woburn resident Anne Anderson (Kathleen Quinlan) to take legal action against those responsible.
 tanneries could be responsible for several deadly cases of leukemia, but also are the main employers for the area. Jan decides to go forward against two giant corporations (real-life companies Beatrice Foods and W. R. Grace and Company) with links to the tanneries, thinking that the case could possibly earn him millions, as well as enhancing his and his firms already considerable reputations. 

Bringing a class action lawsuit in federal court, Jan represents families who demand a clean-up of the contaminated area and an apology. However, the case develops a life of its own and takes over the lives of Jan and his firm. The lawyers for the tanneries parent corporations are not easy to intimidate, a judge (John Lithgow) makes a key ruling against the plaintiffs, and soon Jan and his partners find themselves in a position where their professional and financial survival has been staked on the outcome of the case.

Jan stubbornly declines settlement offers, gradually coming to believe that the case is about more than just the money. He allows his pride to take over, making outrageous demands and deciding that he must win at all costs. Pressures take their toll, with Jan and his partners going deeply into debt. After a lengthy trial, the case is dismissed in favor of Beatrice, Jan having turned down an offer of $20 million from Beatrice attorney Jerry Facher (Robert Duvall) while the jury was deliberating. The plaintiffs are forced to accept a settlement with Grace that barely covers the expense involved in trying the case, leaving Jan and his partners broke. The families are deeply disappointed, and Jans partners dissolve their partnership, effectively breaking up the firm. Jan ends up alone, living in a small apartment and running a small time law practice. He later files for bankruptcy.
 Environmental Protection Agency, building on Jans work on the case, later brought its own enforcement action against the offending companies, forcing them to pay millions to clean up the land and the groundwater. It takes Jan several years to settle his debts, and he now practices environmental law in Boston.

===Differences from the book===
  Environmental Protection Agency and its potential consequences that might have allowed the plaintiffs another trial against Beatrice, and which did ultimately lead to a conviction of perjury against John Riley, and improper conduct for Mary Ryan, are referred to only briefly in the epilogue.

The characters Charles Nesson, Mark Phillips, Rikki Klieman, Teresa Padro and others have been removed from the film version, as well as the plot points that they contribute.

==Cast==
;Plaintiffs
*John Travolta as Jan Schlichtmann, attorney
*Tony Shalhoub as Kevin Conway, attorney
*William H. Macy as James Gordon, financial adviser
*Željko Ivanek as Bill Crowley, attorney
*Kathleen Quinlan as Anne Anderson
*Mary Mara as Kathy Boyer

;Defendants
*Robert Duvall as Jerry Facher, attorney for Beatrice Foods Bruce Norris as William Cheeseman, attorney for W. R. Grace and Company
*Peter Jacobson as Neil Jacobs
*Sydney Pollack as Al Eustis, CEO of W. R. Grace and Company

;Other significant persons Walter J. Skinner
*Dan Hedaya as John Riley, owner of John J. Riley Tannery, a subsidiary of Beatrice Foods
*James Gandolfini as Al Love, W. R. Grace employee
*Stephen Fry as  , scientific expert witness
*Howie Carr as Radio Talk Show Host
*Kathy Bates as Bankruptcy Judge (uncredited)

==Reception== Patch Adams. The film was successful on limited release. 

==Music==
The music score was written by Danny Elfman.

Other songs include:

*"Theres a Rainbow Round my Shoulder"
**Brian Setzer Orchestra
*"Hard Workin Man" (featured on the opening credits)
**Written by Jack Nitzsche, Ry Cooder, Paul Schrader
**Performed by Captain Beefheart
**Courtesy of MCA Records; under license from Universal Music Special Markets
*"Little Drummer Boy"
**Written by Katherine Davis, Henry Onorati and Harry Simeone
**Performed by Vienna Boys Choir; London Symphony Orchestra
**Courtesy of Sony Music Special Products, a division of Sony Music Entertainment
*"Take Me To The River"
**Written by Al Green and Mabon Hodges
**Performed by Talking Heads
**Courtesy of Sire Records Company, by arrangement with Warner Special Products and licensed courtesy of EMI Records Ltd.
**Version used is the live version from Stop Making Sense
*"Theme from A Summer Place"
**Written by Max Steiner

==Awards==
*Academy Awards, USA Oscar Best Actor in a Supporting Role (Robert Duvall). This was Duvalls third loss in the Best Supporting category and his fifth loss overall. 
**Nominated: Best Cinematography (Conrad L. Hall). Hall lost out to Saving Private Ryan. Blockbuster Entertainment Awards
**Nominated: Favorite Actor - Drama (John Travolta)
**Favorite Supporting Actor: Drama (Robert Duvall)
*Boston Society of Film Critics Awards
**Won: BSFC Award Best Supporting Actor (William H. Macy, also for Pleasantville (film)|Pleasantville (1998) and Psycho (1998 film)|Psycho (1998).) A Simple Plan (1998). Chicago Film Critics Association Awards
**Nominated: CFCA Award Best Supporting Actor (Robert Duvall) Florida Film Critics Circle Awards
**Won: FFCC Award Best Supporting Actor (Robert Duvall)
*Golden Globes, USA
**Nominated: Golden Globe Best Performance by an Actor in a Supporting Role in a Motion Picture (Robert Duvall)
*Political Film Society, USA
**Won: PFS Award Human Rights
*Satellite Awards
**Nominated: Golden Satellite Award Best Performance by an Actor in a Supporting Role in a Motion Picture - Drama (Robert Duvall)
*Screen Actors Guild Awards
**Won: Actor Outstanding Performance by a Male Actor in a Supporting Role (Robert Duvall) USC Scripter Award
**Won USC Scripter Award  (Jonathan Harr (author), Steven Zaillian (screenwriter))
*Writers Guild of America, USA
**Nominated: WGA Award (Screen) Best Screenplay Based on Material Previously Produced or Published

== See also ==
* Aberjona River
* Anderson v. Cryovac
* Beatrice Foods
* Trichloroethylene
* Trial movies Erin Brockovich, which was released two years later and dramatized a similar real-life case
* W. R. Grace and Company

== References ==
 

== External links ==
 
*  
*  
*  
*  
*   (early newspaper articles by reporter Charles C. Ryan)
*    hosted by  
*   by Eric Asimow, Picturing Justice: The On-Line Journal of Law & Popular Culture, February 1999
*   Index and copies of every pleading filed in the Woburn suit, maintained by Florida State University College of Law

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 