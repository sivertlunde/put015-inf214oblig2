Tarzan's Savage Fury
{{Infobox film
| name           = Tarzans Savage Fury
| image          = "Tarzans_Savage_Fury"_(1952).jpg
| image_size     = 
| caption        = 
| director       = Cy Endfield
| producer       = 
| writer         = Hans Jacoby
| based on       =   
| narrator       = 
| starring       = Lex Barker Dorothy Hart Patric Knowles
| music          = 
| cinematography = 
| editing        = 
| distributor    = RKO Pictures
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
}}
Tarzans Savage Fury is a 1952 film starring Lex Barker as Tarzan, Dorothy Hart as Jane Porter (Tarzan)|Jane, and Patric Knowles.  The movie was directed by Cy Endfield. While most Tarzan films of the 1930s, 40s and 50s presented Tarzan as a very different character from the one in Edgar Rice Burroughs novels, this movie does make some allusions to the novels. It was shot in Chatsworth, Los Angeles|Chatsworth, Californias Iverson Ranch. The film was the last to be directed by Cyril "Cy" Endfield in the US. Finding himself one of Hollywoods film-makers blacklisted  by the House Un-American Activities Committee he moved to Britain.  The film was co-written by Cyril Hume, whod contributed substantially to the "Tarzan" series back in its bigger budget MGM days.   

==Plot==
Tarzan agrees, against his better judgement, to guide British government agents Edwards and Rokov into the land of the Wazuri Tribe, to harvest uncut diamonds for national-defense purposes. But it transpires the agents are secretly criminals who intend to use the gems for their own sinister purposes.  

==Cast==
* Lex Barker as Tarzan Jane
* Patric Knowles as Edwards, an English traitor
* Charles Korvin as Rokov, a Russian agent
*Tommy Carlton as Joseph Joey Martin
*Wesley Bly as Native Captive (uncredited)
*Darby Jones as Witch Doctor (uncredited)
*Peter Mamakos as Pilot (uncredited)
*Bill Walker as Native Chief (uncredited)

==Critical reception== Variety review  succcinctly said, "A series of unexciting jungle heroics are offered . ." &nbsp;  Recent TV guides for re-run viewers say little more – The Radio Times, "plenty of action helps the story along" ; TV Guide, film was "uninteresting and slowly paced."   

==External links==
*  

==References==
 

 
 

 
 
 
 
 
 
 
 
 


 