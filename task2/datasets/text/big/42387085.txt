Malla (film)
{{Infobox film name           = Malla image          = image_size     = caption        = director       = V. Ravichandran producer       = Ramu writer         = V. Ravichandran narrator  Vishnuvardhan
|starring       = V. Ravichandran Priyanka Upendra Mohan Shankar Umashree music          = V. Ravichandran cinematography = G S V Seetharam editing        = M. Sanjeeva Reddy studio         = Ramu Enterprises released       =   runtime        = 147 minutes country        = India language       = Kannada budget         =
}}
 Kannada Romance romance - action film written, directed, composed and enacted by V. Ravichandran in dual roles. The film also stars Priyanka Upendra and Mohan Shankar in the lead roles.  The film was produced by Ramu for his home banner Ramu Enterprises making his the first team up with Ravichandran.

The film found a widespread release across Karnataka state on 27 February 2004 and was declared a blockbuster at the box-office. However it received mixed response by the critics who claimed that the film highlights the unwanted intimacy scenes between the lead pair.  The film is also reportedly the first to introduce the famous Kerala based traditional art Kalarippayattu in Kannada cinema.  The stunt master K. D. Venkatesh was awarded in special category at the Karnataka State Film Awards for the year 2004.

Facts: *This is the first hit in 2004 in sandalwood, Because Kannada film industry faced utter flop crisis in this period.
* Bangadhi meenu song removed from main film
* Ravi chandrans performance much appreciated by his audience.
* Even Super star Rajani kanth approached Ravi to Stop Music direction, while making this film. 
*Ravi chandrans set consciousness one second become talk of audience in this movie.
- Heros sweet night takes place in Big cot sized lamp ( Deepa)
- Mallas grave yard having his face motif. ( Ubbu shilpa)
* When Ravi chandran undergone one surgery, shooting stopped. Ramu faced financial crisis. But Ravi continued shooting at one point he started cross his budget limit. Ravi did not asked a single paise extra by Ramu (producer) finished the movie with his money. Handed over finished movie to Ramu.This incident proved Ravi chandran once again as Gentle man of Kannada movie industry.
* Ravi chandran fighting with Sheep (Ram) is highlight of this movie.
* This movie as biggest flash back.

==Cast==
* V. Ravichandran as Mallikarjuna (Malla)/ Shiva
* Priyanka Upendra as Priya
* Mohan Shankar as Hanuma
* Umashree
* KSL Swamy
* Pavitra Lokesh
* Vijaykashi
* Shankar Ashwath
* Lakshman

==Soundtrack==
The music of the film was composed and lyrics written by V. Ravichandran.

{{Infobox album  
| Name        = Malla
| Type        = Soundtrack
| Artist      = V. Ravichandran
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    =  Feature film soundtrack
| Length      = 
| Label       = Jhankar Music
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Yammo Yammo
| lyrics1  	= V. Ravichandran
| extra1        = Srinivas (singer)|Srinivas, Anuradha Sriram
| length1       = 
| title2        = Karunaade
| lyrics2 	 = V. Ravichandran
| extra2        = L. N. Shastry
| length2       = 
| title3        = Ee Preethiya Marethu
| lyrics3       = V. Ravichandran
| extra3  	= S. P. Balasubrahmanyam, K. S. Chithra
| length3       = 
| title4        = Masthu Nee Masthu
| extra4        = Hemanth
| lyrics4 	 = V. Ravichandran
| length4       = 
| title5        = Olagirodhu
| extra5        = Udit Narayan, Suma Shastry
| lyrics5       = V. Ravichandran
| length5       = 
| title6        = Mangalyam
| extra6        = L. N. Shastry, Suma Shastry
| lyrics6       = V. Ravichandran
| length6       = 
| title7        = Bangadi
| extra7        = Mano (singer)|Mano, K. S. Chithra
| lyrics7       = V. Ravichandran
| length7       = 
| title8        = Angada Angada
| extra8        = L. N. Shastry, Suma Shastry
| lyrics8       = V. Ravichandran
| length8       = 
}}

==References==
 

==External source==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 

 