Sawan Ki Ghata
{{multiple issues|
 
 
}}

{{Infobox film
| name =Sawan Ki Ghata
| image =
| image_size =
| caption =
| director = Shakti Samanta
| producer = Shakti Samanta
| writer = Gulshan Nanda (story)
| narrator = Mumtaz
| music = O. P. Nayyar S.H. Bihari (lyrics)
| cinematography =
| editing =
| distributor =
| released = 1966
| runtime =
| country = India Hindi
| budget =
| preceded_by =
| followed_by =
}}
 1966 Bollywood Madan Puri.

==Cast==
* Manoj Kumar ... Gopal
* Sharmila Tagore ... Seema
* Mumtaz ... Saloni Pran ... Kailash Chaudhary
* Sajjan ... Bansilal Jeevan ... Rana Shamsher Madan Puri ... Limo

==Music==
The films music is by O. P. Nayyar|O.P.Nayyar and the lyrics were written by S.H. Bihari. Almost all the songs of the film were liked by the audience, especially the songs "haule haule saajana", "meri jaan tum pe sadke", "zulfon ko hata le chehre se" and "aaj koi pyar se" have melodious compositions. An interesting trivia about this film is that a duet by Mohd.Rafi and Asha Bhonsle Honton pe hansi aankhon mein nasha was recorded but keeping in view the length of this movie, the song was not included in the picture.

* Aaj koi pyaar se dil ki baatein keh gaya (Asha Bhosle)
* hHothon pe hansi aankhon mein nasha (Asha Bhosle, Mohammed Rafi)
* Jo dil ki tadap na jaane usi se mera pyaar (Asha Bhosle)
* khuda huzoor ko meri bhi zindagi de de (Asha Bhosle, Usha Mangeshkar)
* Meri jaan tumpe sadke ehsaan itna kar do (Asha Bhosle)
* Meri jaan tumpe sadke ehsaan itna kar do (Mahendra Kapoor)
* Zara haule haule chalo more saajna ham bhi peechhe hain tumhaare (Asha Bhosle)
* Zulfon ko hataa do chehre se thodaasa ujaala hone do (Mohammed Rafi)
* Honton pe hansi aankhon mein nasha (Mohammed Rafi, Asha Bhonsle) - song recorded but not included in the movie.

==External links==
*  

 
 
 
 


 