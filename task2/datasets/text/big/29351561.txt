Sanam Teri Kasam (2009 film)
 
 
{{Infobox film
| name           = Sanam Teri Kasam (2009)
| image          = Sanam_Teri_Kasam_(2009_film).jpg
| caption        =
| director       = Lawrence DSouza
| producer       = Sudhakar Bokade
| writer         = Talat Rekhi
| narrator       =
| starring       = Saif Ali Khan  Pooja Bhatt Atul Agnihotri Sheeba Akashdeep
| music          = Nadeem-Shravan
| cinematography =
| editing        = 
| distributor    = Ultra Music
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         =
| gross          =
}} romantic drama film directed by Lawrence DSouza and starring Saif Ali Khan, Pooja Bhatt, Atul Agnihotri and Sheeba Akashdeep. The film was released in 2009, although it was produced in 1994.   
A long legal battle canned it for the next fifteen years, from 1994 to 2009.
The film was previously titled Sambandh and Yeh Pyar Hi To Hai. The audio cassettes were released as Sambandh.

==Plot==
Vijay Verma (Saif Ali Khan) is a rich and selfish playboy who enjoys playing with women by giving a fake name to every girl. His friend, Gopal, (Atul Agnihotri) tells him several times to stop this act otherwise he will regret it. One day, Vijay meets a lovely tourist, Seema (Pooja Bhatt) and falls in love with her after a few meetings. During a separation, Seema  sends Vijay a letter about her father agreeing to marriage with Vijay. He replies saying that he will leave immediately to get to her place. This is when the narrative introduces a shocking heel turn. Seemas father arrives at the airport and is greeted by his future son-in-law, and, shortly after, Seemas marriage ceremony takes place. 
After the marriage, Seema finds out that the person who has married her is none other than Gopal and she was tricked into marriage with him; when he knew that Vijay loved her too.
Seema and her unexpected new spouse turn up at Vijays home, and its frustrating then to see Vijay quietly take the abuse doled out by the two-faced husband. Lots of yearning glances exchanged between Vijay and Seema, lots of smirking by the husband. Its a revenge tale, of course. Theres a reason Seema got tricked into wedlock.

==Cast==
* Saif Ali Khan...Vijay Verma
* Atul Agnihotri...Gopal
* Pooja Bhatt...Seema Khanna
* Sheeba Akashdeep...Dr. Renu Mahinder Nath
* Vikas Anand...Ramdin Kaka
* Saeed Jaffrey...Vikram Verma
* Alok Nath...Khanna

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Itna Bhi Na Chaho Mujhe"
| Kumar Sanu, Alka Yagnik
|-
| 2
| "Sanam Yeh Pyaar Hi To Hai"
| Kumar Sanu
|-
| 3
| "Main Dil Ki Dil Mein"
| Kumar Sanu, Pankaj Udhas
|-
| 4
| "Ek Baar Ek Baar Pyaar Se" Poornima
|-
| 5
| "Tum Gwahi Do"
| Kumar Sanu
|-
| 6
| "Yeh Dil Darr Raha Hai"
| Sapna Mukherjee
|-
| 7
| "Main Pyar Tumse"
| Kumar Sanu, Alka Yagnik
|}

==References==
    

==External links==
* 

 
 
 
 