The Face of an Angel
 
 
{{Infobox film
| name = The Face of an Angel
| image = 
| alt = 
| caption = 
| director = Michael Winterbottom
| producer = Melissa Parmenter
| screenplay = Paul Viragh
| based on =  
| starring = {{Plainlist|
* Kate Beckinsale
* Daniel Brühl
* Cara Delevingne}}
| music = 
| cinematography = 
| editing = 
| production companies = {{Plainlist|
* BBC Films
* Cattleya
* Multitrade
* Revolution Films
* Vedette Finance
* Ypsilon Films}}
| distributor = Soda Pictures
| released =  
| runtime = 101 minutes  
| country = {{Plainlist|
* United Kingdom
* Italy
* Spain}}
| language = English
| budget = 
| gross = 
}}
The Face of an Angel is a 2014 British psychological thriller film directed by Michael Winterbottom and written by Paul Viragh, inspired by the book Angel Face, drawn from crime coverage by Newsweek/Daily Beast writer Barbie Latza Nadeau. The film stars Kate Beckinsale, Daniel Brühl, and Cara Delevingne.

The film is based on the real-life story of Amanda Knox who was accused of the murder of Meredith Kercher in 2007.

==Cast==
 
* Kate Beckinsale  as Simone Ford
* Daniel Brühl  as Thomas
* Cara Delevingne  as Melanie
* Genevieve Gaunt as Jessica Fuller
* Ava Acres as Bea
* Sai Bennett as Elizabeth Pryce
* Rosie Fellner as Katherine John Hopkins as Joe Peter Sullivan as James Pryce
* Alistair Petrie as Steve
* Corrado Invernizzi as Francesco
* Valerio Mastandrea as Edoardo
* Andrea Tidona as Pubblico Ministero
* Austin Spangler as Timothy
* Ranieri Menicori as Carlo Elias
 

==Production==
On 6 September 2013, Daniel Brühl joined the adaptation film of Angel Face to lead the cast.    On 10 October 2013, Cara Delevingne joined the cast of the film.    On 14 October 2013, Kate Beckinsale was set to join the drama film.   

Principal photography began in mid-November 2013 in Italy.  On 3 February 2014, WestEnd Films showed the first promo-reel to the buyers at European Film Market in Berlin International Film Festival, when the film was in post-production. 

==Reception==
The film received average reviews. On Rotten Tomatoes, it has a 57% rating based on 23 reviews. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 