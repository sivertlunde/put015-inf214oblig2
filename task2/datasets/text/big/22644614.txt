De grens
{{Infobox film
| name           = De grens
| image          = 
| image_size     = 
| caption        = 
| director       = Leon de Winter
| producer       = 
| writer         = Leon de Winter
| narrator       = 
| starring       = Johan Leysen
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = 20 September 1984
| runtime        = 100 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}} Dutch thriller film directed by Leon de Winter. It was screened in the Un Certain Regard section at the 1984 Cannes Film Festival.   

==Cast==
* Johan Leysen - Hans Deitz
* Linda van Dyck - Marleen Ruyter
* Angela Winkler - Rosa Clement
* André Dussollier - Marcel Boas
* Héctor Alterio - Andras Menzo
* Mariana Rey Monteiro - Sabinos mother
* Cecília Guimarães - Pensionhoudster
* Paula Guedes - Ondervraagster
* Isabel Ribeiro - Marcels wife
* José María Blanco
* Rui Cardoso
* Rosa Clement
* Hans Deitz
* Helena Isabel

==References==
 

== External links ==
*  

 
 
 
 
 
 


 
 