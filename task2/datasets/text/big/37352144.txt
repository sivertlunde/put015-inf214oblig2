The Pleasure Seekers (1920 film)
{{infobox film
| name           = The Pleasure Seekers
| image          = The Pleasure Seekers (1920) - 1.jpg
| image_size     = 175px
| caption        = Newspaper ad
| director       = George Archainbaud
| producer       = Selznick Pictures
| writer         = John Lynch (story)  Edward J. Montagne (scenario)
| starring       = Elaine Hammerstein
| music          =
| cinematography =
| editing        =
| distributor    = Select Pictures
| released       = December 30, 1920
| runtime        = 60 minutes
| country        = United States
| language       = Silent (English intertitles)
}} lost 1920 American silent film produced by Selznick Pictures and distributed by the Select Company. It was directed by George Archainbaud and stars Elaine Hammerstein.  

==Plot==
As summarized by a film publication,    Craig Winchell (Campbell) is threatened with disownment by his wealthy father John Winchell (Currier) unless he gives up his wild ways, and particularly Mrs. Clara Marshall (Clayton), a divorcee with a rather tarnished reputation. Craig, determined to follow his fathers wishes, leaves for a long motor trip. His automobile breaks down outside the home of Reverend Snowden (Furey) in a small town. There he meets Snowdens secretary, Mary Murdock (Hammerstein), and falls in love with her. The death of Snowden permits Mary to leave, and she and Craig are married. Craig brings his bride to meet his father, but John refuses to see her, imagining what type of woman his son would marry. Mary sets to win over the father, and takes a position as his secretary and completely captivates him. When he discovers that she is Craigs wife, he is overjoyed. It is then that Craig accidentally meets again Clara Marshall. With the lure of the gay life strong, he promises to attend her party the next night. Mary learns of the party and that her husband has lied to her about his whereabouts. When John threatens to go there and drag his son away, Mary says that it is her responsibility to get him. Dressed in the finest gown that John can buy, she goes to Claras home. When Craig compares the two women face-to-face, his remorse is sincere as he appreciates the true value of the wife that he deceived.

==Cast==
*Elaine Hammerstein - Mary Murdock
*James A Furey - Snowden
*Webster Campbell - Craig Winchell
*Marguerite Clayton - Clara Marshall
*Frank Currier - John Winchell

==References==
 

==External links==
*  
*  at George Eastman House

 

 
 
 
 
 
 
 


 