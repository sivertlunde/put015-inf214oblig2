Appointment in Tokyo
{{Infobox Film
| name = Appointment in Tokyo
| caption =
| image	=	Appointment in Tokyo FilmPoster.jpeg
| director = Jack Hively
| producer = Office of War Information
| writer =
| narrator =
| starring =
| music =
| cinematography =
| editing =
| distributor =
| released = 1945
| runtime = 56 minutes
| country = United States
| language = English
}} USS Missouri on September 1, 1945. 
 reconquest of Battle of Manila, and the rescue of American and Filipino POWs from Japanese prison camps.

The New York Times reviewer Bosley Crowther felt that the action footage is of the "highest caliber," and the battle in Manilas streets is recounted in shots that are as vivid as any others in war photography. But Crowther continued that "to be quite frank about it, the cinematic structure of this film is inadequate to the vital subject and far inferior to that of previous war reports. Nor should the film, be regarded as a full review of the Pacific war." He objected that this material is "strangely grandiose with a melodramatic flavor." "Questionable, too," he went on, "from the point of straight reporting," is the "elaborate emphasis placed upon the personality of Gen. Douglas MacArthur,"  represented in no less than twenty-six shots. 

==See also==
* List of Allied propaganda films of World War II

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 