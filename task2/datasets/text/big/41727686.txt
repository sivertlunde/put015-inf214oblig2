The Danger Mark
{{infobox film
| title          = The Danger Mark
| image          =
| imagesize      =
| caption        = Hugh Ford
| producer       = Adolph Zukor Jesse Lasky
| writer         = Robert W. Chambers(play) Charles Maigne(scenario)
| starring       = Elsie Ferguson Mahlon Hamilton
| music          = William Marshall
| editing        =
| distributor    = Paramount Pictures
| released       = July 7, 1918
| runtime        = 50 minutes; 5 reels
| country        = USA
| language       = Silent film(English intertitles)

}} Hugh Ford and starring Elsie Ferguson.   It was produced by Famous Players-Lasky,  and distributed by Paramount Pictures. It is based on a play by Robert W. Chambers.    Prior to the films release, the play was published in "serial form and later issued as a book." {{cite news
 |title=At the Illinois |work=  }} 

==Cast==
*Elsie Ferguson—Geraldine Seagrave
*Mahlon Hamilton—Duante Mallett
*Crauford Kent—Jack Dysart
*Gertrude McCoy—Sylvia Mallett
*Edmund Burns—Scott Seagrave
*Maude Turner Gordon—Kathleen Severn
*William T. Carleton—Colonel Mallett

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 