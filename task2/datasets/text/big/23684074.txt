The Old Jar Craftsman
{{Infobox film
| name           = The Old Jar Craftsman
| image          = The Old Jar Craftsman.jpg
| caption        = Theatrical poster for The Old Jar Craftsman (1969)
| director       = Choi Ha-won 
| producer       = Lee Jong-byeok
| writer         = Yeo Su-jong  Shin Bong-seung
| starring       = Hwang Hae
| music          = Choi Chang-kwon
| cinematography = You Young-gil
| editing        = Hyeon Dong-chun
| distributor    = Dong Yang Films Co., Ltd.
| released       =  
| runtime        = 95 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
| film name      = {{Film name
 | hangul         =    
 | hanja          = 
 | rr             = Dokjitneun neulkeuni
 | mr             = Tok chinnŭn nŭlgŭni
 | context        = }}
}} Best Foreign Language Film at the 42nd Academy Awards, but was not accepted as a nominee. 

==Plot==
A lonely old man who makes a living as a potter saves the life of a young woman. The two marry and have a son. The womans old lover finds her, and she runs away with him. The old potter commits suicide. Years later, the woman, now a beggar, returns to her old home and visits her son at the old potters grave. Based on a novel.   

==Cast==
* Hwang Hae 
* Yoon Jeong-hee
* Nam Koong Won
* Heo Jang-gang
* Kim Jung-hoon
* Kim Hee-ra
* Kim Jeong-ok
* Choe Bong
* Kim So-jo
* Jeon Shook 

==See also==
* List of submissions to the 42nd Academy Awards for Best Foreign Language Film
* List of South Korean submissions for the Academy Award for Best Foreign Language Film

==References==
 

==Bibliography==
===English===
*   
*  
*  
===Korean===
*  
*  

 
 
 
 
 

 
 
 
 
 
 

 