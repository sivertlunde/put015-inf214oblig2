Tokyo March
 
 
{{Infobox film
| name           = Tokyo March
| image          = Tokyo koshin-kyoku poster.jpg
| image_size     =
| caption        = Cover of sheet music for the theme song
| director       = Kenji Mizoguchi
| producer       =
| writer         =
| narrator       =
| starring       =Shizue Natsukawa Reiji Ichiki
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1929 (Japan)
| runtime        = 24 (existing fragment)
| country        = Japan Japanese
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
  is a 1929 black and white Japanese silent film, originally presented with benshi accompaniment, directed by Kenji Mizoguchi. It is a classic melodramatic love tragedy addressing social inequality in modern Japan, depicted in Mizoguchis typical style. The theme song "Tokyo March" was originally sung by Chiyako Sato.

==Cast==
*Shizue Natsukawa as Michiyo
*Reiji Ichiki as Yoshiki
*Isamu Kosugi as Sakuma
*Takako Irie as Sayuriko

==External links==
* 

 

 
 
 
 

 