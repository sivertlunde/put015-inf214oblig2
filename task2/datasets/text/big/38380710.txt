Parkland (film)
 
{{Infobox film
| name = Parkland
| image = Parkland poster.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Peter Landesman
| producer = Gary Goetzman Tom Hanks Bill Paxton Nigel Sinclair Matt Sinclair
| screenplay = Peter Landesman
| based on =  
| starring =  James Badge Dale Zac Efron Jackie Earle Haley Colin Hanks David Harbour Marcia Gay Harden Ron Livingston Jeremy Strong Billy Bob Thornton Jacki Weaver Tom Welling Paul Giamatti 
| music = James Newton Howard 
| cinematography = Barry Ackroyd
| editing = Markus Czyzewski Leo Trombetta American Film Company Playtone Exclusive Media Group
| released =  
| runtime = 94 minutes  
| country = United States
| language = English
| budget = $10 million 
| gross = $1,412,181   
}} historical drama Nigel and Matt Sinclair.    The film is based on Vincent Bugliosis 2008 book Four Days in November: The Assassination of President John F. Kennedy.

==Plot== Parkland Hospital; Secret Service; an unwitting most famous home movie in history; the FBI agents who were visited by Lee Harvey Oswald before the shooting; the brother of Lee Harvey Oswald, left to deal with his shattered family; and JFK’s security team, witnesses to both the president’s death and Vice President Lyndon Johnson’s rise to power.

==Cast==
* James Badge Dale as Robert Edward Lee Oswald, Jr.   
* Zac Efron as Dr. Charles James "Jim" Carrico  
* Jackie Earle Haley as Father Oscar Huber    Malcolm O. Perry  James Gordon Shanklin 
* Marcia Gay Harden as Head Nurse Doris Nelson    
* Ron Livingston as James P. Hosty   Jeremy Strong as Lee Harvey Oswald 
* Billy Bob Thornton as Secret Service Agent Forrest Sorrels 
* Jacki Weaver as Marguerite Oswald   
* Tom Welling as Secret Service Agent Roy Kellerman 
* Paul Giamatti as Abraham Zapruder 
* Dana Wheeler-Nicholson as Lillian Zapruder
* Bitsie Tulloch as Marilyn Sitzman 
* Brett Stimely as President John F. Kennedy 
* Kat Steffens as First Lady Jacqueline Kennedy
* Gil Bellows as David Powers
* Sean McGraw as Lyndon B. Johnson Earl Rose
* Mark Duplass as Kenneth ODonnell
* Jimmie Dale Gilmore as Reverend Saunders
* Matt Barr as Paul Mikkelson
* Jonathan Breck as Winston Lawson
* Gary Grubbs as Dr. Kemp Clark
* Bryan Batt as Malcolm Kilduff
* Glenn Morshower as Mike Howard
* Armando Gutierrez as Officer Glen McBride
* Austin Nichols as Secret Service Agent Emory Roberts

==Production== various conspiracy theories surrounding the Kennedy assassination.     

==Release== premiered at the 70th Venice International Film Festival and was also screened at the 2013 Toronto International Film Festival.     Coinciding with the assassinations 50th anniversary year, the film was released theatrically in the United States on October 4, 2013.   

==Reception==
Parkland received mixed reviews, currently holding a 49% rating based on 119 reviews on review aggregator website  , the film has a 51/100 rating, signifying "mixed or average reviews". 

==Home media==
The film was released on DVD and Blu-ray Disc|Blu-ray on November 5, 2013.

==Historical accuracy==
Historian Peter Ling awarded Parkland four stars for enjoyment and three stars for historical accuracy. Reviewing the film, he praised its attempt to "capture the desperate efforts made to save Kennedy in the operating room". He told historyextra: "It shows that the head nurse, Doris Nelson (played by Marcia Gay Harden), had to take a piece of JFKs skull and some brain tissue from Mrs Kennedy  , and that junior doctor, Jim Carrico (played by Zac Efron), had to be told to stop the frenetic but fruitless cardiac massage at one oclock, when the team declared JFK dead." 

However, he noted the "suspect influences" on Abraham Zapruders decision to hand over his tapes to Life magazine. He said: "Once copies have been given to the Secret Service and the FBI, Zapruder has to choose from many media outlets who want to buy the film. He chooses Life because he says he respects the publication, but the movie seems to hint that any suppression of the film’s contents is in line with Zapruder’s wishes, and not because of suspect influences at Life itself, whose managing director had CIA connections." 

Journalist Hugh Aynesworth, who witnessed the assassination of JFK and was a consultant for Parkland,  : "I worked with the producers on character development. They did a tremendous job describing the mother and brother of Oswald. I worked with the Oswalds in the beginning  . He added "It’s very accurate, but that’s one of the problems. People want more; they want a conspiracy theory...There were so many people impacted, and the film shows the bravery of some of the people at the hospital and the bravery of Jackie." 

==See also==
* Concussion (2015 film)|Concussion (2015, directed by Peter Landesman)
==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 