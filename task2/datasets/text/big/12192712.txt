The Kids Are Alright (film)
 
 
 
{{Infobox film
| name           = The Kids Are Alright 
| image          = Thekidsarealrightmovieposter.jpg
| caption        = 
| director       = Jeff Stein 
| producer       = Tony Klinger Bill Curbishley
| writer         = 
| starring       = Roger Daltrey John Entwistle Keith Moon Pete Townshend
| music          = The Who
| cinematography = Anthony B. Richmond
| editing        = Ed Rothkowitz	  
| distributor    = New World Pictures
| released       =  
| runtime        = 101 minutes
| country        = United Kingdom
| language       = English
| budget         = $2 million
| gross          = 
}} rock band The Who, including live performances, promotional films and interviews from 1964 to 1978.

==Production==
The film was primarily the work of American fan Jeff Stein who, despite having no previous experience in filmmaking, convinced the band to support the project and served as the films director. Stein had produced a book of photographs from the bands 1970 tour when he was just 17. In 1975, he approached Pete Townshend, The Whos principal composer and lead guitarist, about compiling a collection of film clips to provide a historical reference for the bands fans. Townshend initially rejected the idea, but was persuaded by the groups manager, Bill Curbishley, to give their cooperation.

When Stein and his film editor, Ed Rothkowitz, soon previewed a 17-minute compilation of clips from their US television appearances to the band and their wives, they could hardly believe the reaction. "Townshend was on the floor, banging his head. He and Moon were hysterical. Daltreys wife was laughing so hard she knocked over the coffee table in the screening room. Their reaction was unbelievable. They loved it. Thats when they were really convinced that the movie was worth doing."
 Kilburn in December 1977, and, although not included in the film, it appeared on the soundtrack album.

The sound editing was supervised by bassist John Entwistle and, with the exception of a 1965 performance of "Anyway, Anyhow, Anywhere" where Entwistle had to replace a missing bass track, and the footage of Moon smashing a drum kit - as the original 8mm footage was silent, Moon overdubbed drum sounds - most of the sound was authentic. Entwistle did fight for - and won - getting him and Pete to overdub their backing vocals on the Woodstock footage because Entwistle deemed the original gigs backup vocals "dire." During the process of sound editing, on 7 September 1978, Keith Moon died. All of the band members except Townshend had seen a rough cut of the film just a week before and, after Moons death, they were determined not to change anything.
 Faces drummer Kenney Jones.
 An album was released as a soundtrack in June 1979 that included some songs and performances from the film. The album reached #26 in the UK, and fared better in the US where it peaked at #8 on the Billboard album charts and went Platinum.

==Contents==
With the collection of material he included, Stein attempted to create not a linear, chronological documentary, but "a celluloid rock n roll revival meeting" and "a hair-raising rollercoaster ride" that was worthy of the bands reputation. The performances which comprise the body of the film are organized around a number of playful encounters by the band members with various variety and talk show hosts, Pete Townshends playful relationship with his fans, admirers and critics, and the endless antics of Keith Moon.

===Television shows and interviews=== Los Angeles Tommy Smothers in some witty ad-libs before "My Generation". Moon made the biggest impact, however, when the destructive nature of his on-stage persona reached its highest level. After The Whos performance of "My Generation", they began smashing their instruments. Moon packed explosive charge in his bass drum which set Townshends hair on fire and rendered him temporarily deaf for 20 minutes, while cymbal shrapnel left a gash in Moons arm. Townshend then took the acoustic guitar Smothers was holding and smashed it to bits on the ground. Since the guitar that Smothers was holding can clearly be seen with no strings on it, Townshends smashing of Smothers guitar was staged and expected by those on stage.

Clips of a 1973 interview from London Weekend Televisions Russell Harty Plus appear six times throughout the film. While Harty delves into the background of the members lives, Moon again steals the show as he rips off Townshends shirt sleeve and then promptly strips down to his underwear.

One of the TV interviews included in the film features   or all of those crappy people could ever hope to achieve!"
 ABC televisions Shindig! and one of only two surviving tapes from the groups many appearances on the British program Ready Steady Go!, both recorded in 1965, are included along with numerous interview clips from BBC Radio, as well as mostly b/w interviews, stage and blue-screen performances (such as of Tommy, Can You Hear Me?) on the music programme Beat-Club recorded at the Radio Bremen studios in Hamburg, Germany. Segments filmed in each of the band members homes include several conversations between Moon and fellow drummer Ringo Starr.

===Large concerts===
Performances from three of the bands largest concert appearances bear witness to the bands progression from the British mod scene to global superstardom:
 Woodstock Music and Art Fair on 17 August 1969 was not an artistic success in the eyes of the band, but it helped Tommy (rock opera)|Tommy become a critical blockbuster. Warner Bros. allowed Jeff Stein to look through their 400,000 feet of film from the three-day festival.  Stein, then, reconstructed a "new" cut of the Whos song highlights (as opposed to the "split-screen" images from the original Woodstock film).  He chose three songs: "Sparks", "Pinball Wizard", and "See Me, Feel Me".  He also added a snippet of "My Generation" when Townshend smashed his guitar following a brief excerpt of "Naked Eye". 1975 US tour reached its peak before a crowd of 75,962 at the Pontiac Silverdome on 6 December. The images in the film were broadcast to large screens in the stadium so those in the far reaches could actually see the band members on stage.  From this appears the "Roadrunner/My Generation Blues" medley.  However, the soundtrack includes "Join Together" which precedes "Roadrunner". Monterey International Pop Festival on 18 June 1967 brought about their first big media exposure in the United States. In the film, The Whos Monterey Pop appearance cuts away to footage from past concerts depicting the band destroying their equipment before returning to the destructive end of "My Generation".  This performance does not appear on the soundtrack.

===Discarded footage===
At least three chapters in the film preserve performances that were discarded or thought to be lost:

* When the English National Opera allowed the band to play in the London Coliseum on 14 December 1969, the show was recorded for later release. The poor quality of the footage, however, made it expendable to the group and Jeff Stein retrieved the footage from a trash dump. The bands rendition of "Young Man Blues" is included in the film.
* A promotional film for the song "Happy Jack" was shot on 19 December 1966 for a BBC Television series called Sound and Picture City but the show was never aired. A Quick One, While Hes Away" &mdash; shot on 11 December 1968.  Originally, the clips picture was cropped and bordered by flashing lights to compensate with the films copy (and that version used different camera angles at times).  After the Stones former label, ABKCO, released Rock and Roll Circus on DVD, Stein extracted the Whos performance from the DVD and inserted it back in.

===Moons final performances===

The film incidentally became a sort of "time capsule" for the band, after Keith Moon died only one week after he had seen the rough cut of the film with Roger Daltrey. Moon, according to Daltrey, was deeply shocked by how much he had changed physically in just 15 years, "from a young good-looking boy to a spitting image of Robert Newton". After Moons death, the rough cut did not suffer a single change, since neither Jeff Stein nor the rest of the band wanted to turn the film into an homage to remember Moons passing, but to celebrate his life and career with The Who.

Moons last performances with the band were:
 Who Are You", his last studio performance. Stein wanted to show The Who recording in the studio, even though the band had already finished recording the song. Stein planned to have the band mime over the original recording, but The Who played it live at the Ramport Studios, London, on 9 May 1978. The only playback tracks were Entwistles bass guitar, the acoustic guitar solo in the middle, the backing vocals and synthesizer track.
*The show at Shepperton Studios, London, on 25 May 1978, his last live performance.

==DVD edition==
For many years the film was released on VHS in an edited 90-minute form, extracted from a TV broadcast copy made in the 1980s, which itself was a broadacst of the RCA Spectravision magnetic disc version, a format popular in the late 70s, early 80s.  Several scenes were removed and the audio had several pitch problems and dropouts, due to different film stocks and original film regions. 

In 2003, a DVD edition of the film was released. The film had been transferred from the restored 35mm interpositive  and the audio was extensively restored. In addition to the original film, with English subtitles, on-screen liner notes, commentary with Jeff Stein and DVD producer John Albarian, and a 27-page booklet, the DVD contained a bonus disc with over three hours of additional materials:

*"See My Way": Q&A with director Jeff Stein
*"Behind Blue Eyes": Q&A with Roger Daltrey
*"Miracle Cure": Documentary on the restoration of The Kids Are Alright
*"Getting In Tune": Audio comparison (old vs new)
*"Trick of the Light": Video comparison (old vs new)
*"The Whos London": A tour of Who locations in London
*"The Ox": Isolated tracks of John Entwistle for "Baba ORiley" and "Wont Get Fooled Again"
*"Anytime You Want Me": Multi-angle feature for "Baba ORiley" and "Wont Get Fooled Again"
*"Pure and Easy": Trivia game. The prize: A rare radio trailer of Ringo Starr promoting The Kids Are Alright Who Are You" 5.1 studio mix

The DVD was released by Pioneer Home Entertainment.  The digitally-restored version of the film was premiered at the New York Film Festival in October 2003 with Daltrey, Lewis, Stein and Albarian in attendance.

==References==
 
* Liner notes from The Kids Are Alright (Special Edition) (1979) DVD, Pioneer Entertainment (USA), Inc., 2003
*  

==External links==
*   at thewho.net. Includes photos and mp3 links.
*  
*   Review

 
 

 
 
 
 
 
 
 