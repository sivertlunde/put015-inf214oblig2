When the Gods Fall Asleep
{{Infobox film
| name           = When the Gods Fall Asleep
| image          = Whenthegods.jpg
| caption        = Original film poster
| director       = José Mojica Marins
| producer       = Nelson Teixeira Mendes
| writer         = Jose Mojica Marins Rubens Francisco Luchetti
| starring       = José Mojica Marins
| music          = Kelpson Correa Leonardo Maluf	 	
| cinematography = Edward Freund
| editing        = Jovita Pereira Dias
| studio         = N.T.M. Filmes
| distributor    = Multifilmes
| released       =  
| runtime        = 82 minutes
| country        = Brazil
| language       = Portuguese
| budget         =
}}
 low budget black humored social satire.      

In this film, Finis Hominis (after returning to the asylum) again feels the need to escape the asylum in order to put right the worlds increasing social unrest that he sees in the news.

==Plot== insane asylum, and is known for his occasional escapes from the institution, including the most recent episode during which Finis Hominis became a powerful world figure during the few days of his escape.

Again hospitalized, he sees in the news increasing social, religious, and political unrest in the world, and again feels the need to escape the institution to put order in the streets. He wanders through society, influencing and interfering in isolated incidents, correcting wrongs and exposing corruption primarily in a strictly accidental or coincidental manner.

There is also a parallel sub-plot regarding the impending closure of the asylum due to the cessation of funding from an anonymous benefactor.

==Cast==
*José Mojica Marins
*Andréa Bryan
*Rosalvo Caçador
*Maria Cristina
*Nivaldo de Lima
*Sabrina Marquezine
*Palito
*Amires Paranhos
*Walter C. Portella
*Araken Saldanha (as the voice of José Mojica Marins)
*Roney Wanderney

==References==
 

== External links ==
*   
* 

 

 
 
 
 
 
 
 