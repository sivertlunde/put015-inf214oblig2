Kibbutz (film)
{{Infobox film
| name           = Kibbutz
| image          = Kibbutz Poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical poster
| director       =  Racheli Schwartz
| producer       = Gal Schwartz
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Roni Katzanelson Eyal Zehav
| editing        = Yael Perlov
| studio         = 
| distributor    = Seventh Art
| released       = 
| runtime        = 54 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = 
}}
Kibbutz ( ) is a 2005 Israeli documentary directed by Racheli Schwartz about Kibbutz Hulata, where she lived for 30 years.

Schwartz follows various members, including her own family, over the course of five years, tracing the stages of grieving and disillusionment that follow the kibbutzs economic collapse and disintegration as the community reduces its communal commitment to its members.  Three older women from the founding generation become symbols of the kibbutz’s lost ideals and abandoned history, as they die off, one by one.

For the director, the narrative is a very personal story and admits early on that “making the movie helped me to decide to stay.” 

==References==
 

==External links==
* 
* 

 
 
 
 


 