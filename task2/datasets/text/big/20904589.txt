Duniya Meri Jeb Mein
{{Infobox film
| name           = Duniya Meri Jeb Mein
| image          = Duniya Meri Jeb Mein.jpg
| image_size     = 
| caption        = 
| director       = Tinnu Anand
| producer       = 
| writer         =
| narrator       = 
| starring       =Shashi Kapoor and Rishi Kapoor 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1979
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1979 Bollywood action film directed by Tinnu Anand. The film stars Shashi Kapoor and Rishi Kapoor. The film had role of Shahsi Kapoor without a heroine. He got his leg chopped off due to an accident during run chase fraud robbery arranged by Villian Ranjeet. Beautifully song composited by Rajesh Roshan with Mohd Rafi Kishore Kumar and Amit Kumar singing for the male heroes. It is an action packed movie with Helen having a song by Asha Bhosle. It is an entertaining movie worth watching.

==Cast==
*Shashi Kapoor ...  Karan Khanna 
*Rishi Kapoor ...  Vishal Khanna 
*Neetu Singh ...  Neeta 
*Keshto Mukherjee ...  Watchman  Agha ...  Diwanchand  Helen ...  Dancer / singer in bar 
*Pinchoo Kapoor ...  Thakurdas 
*Raj Mehra ...  Gulabchand 
*Nadira ...  Mrs. Robins 
*Paintal ...  Bhaskar 
*Lalita Pawar ...  Supervisor 
*Jagdish Raj ...  Supervisor 
*Ranjeet ...  Rawat  Sudhir ...  Inspector Yadav 
*Piloo J. Wadia ...  Girls College Principal

==Tracks==

#  Dekh mausam: Lata Mangeshkar, Amit Kumar & Chorus 
#  Duniya meri jeb mein: Kishore Kumar, Asha Bhonsle & Chorus
#  Kuchh sochoon: Kishore Kumar, Asha Bhonsle & Chorus
#  Tere jaisa bhai (Happy): Kishore Kumar, Mohammed Rafi & Chorus
#  Tere jaisa bhai (sad): Kishore Kumar, Mohammed Rafi & Chorus
#  Yeh bhi dil mangta: Asha Bhonsle

==External links==
*  

 
 
 
 


 
 