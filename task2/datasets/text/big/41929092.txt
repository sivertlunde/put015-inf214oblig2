Missionary (film)
 
{{Infobox film
| name           = Missionary
| image          = Missionary.jpg
| border         = yes
| caption        = 
| director       = Anthony DiBlasi
| producer       = 
| writer         = Bruce Wood, Scott Poiley Mitch Ryan, Kip Pardue
| music          = Dani Donadi
| cinematography = Austin F. Schmidt
| editing        = Anthony DiBlasi
| studio         = Poiley Wood Entertainment, Missionary Film Production
| distributor    = Freestyle Releasing 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Missionary is a 2013 horror thriller film by Anthony DiBlasi.  It received its world premiere on July 25, 2013 at the Fantasia International Film Festival and stars Dawn Olivieri as a beautiful single mother caught up in one mans obsession with her. 

==Synopsis==
Katherine (Dawn Olivieri) is a mother trying to raise her son Kesley (Connor Christie) the best she can, despite being recently separated from her husband Ian (Kip Pardue). When Mormon missionary Elder Kevin Brock (Mitch Ryan) offers to help her son practice American football|football, both he and Katherine are drawn to one another. Despite some worries about the 10 year age difference between the two of them, Katherine and Kevin are initially happy in their blossoming relationship. It takes a sour turn when Katherine decides to try to work things out with her estranged husband, which doesnt sit well with Kevin. Already a little mentally unstable, Kevins warped mind tries to use his religion to justify making Katherine his... forever.

==Cast==
*Dawn Olivieri as Katherine Kingsmen Mitch Ryan as Kevin Brock
*Kip Pardue as Ian Kingsmen
*J. LaRose as Sarge Powell
*Connor Christie as Kesley Kingsman
*Jordan Woods-Robinson as Alan Whitehall
*Randy Molnar as President Andersen
*Danielle Kimberley as April Britton
*Dushawn Moses as Doctor West
*Jesse Malinowski as Elder Lillejord
*Jeff Chase as Brian
*Mary Lankford Poiley as Crystal (as Mary Lankford)

==Reception==
Critical reception for Missionary has been mixed to positive,   and Dread Central called the movie an "exemplary specimen of human horror done right.".     Part of the films criticism stemmed from the movies familiarity to other films in the stalker/slasher genre,  but Fearnet noted that "You know most of what Missionary has in store before you hit play, but the final product is still a well-crafted and efficient little thriller."  Several reviewers brought up concerns about whether or not the film would be considered offensive to people and Empire Online commented that DiBlasi and crew were "careful not to demonise the entire religion, and   time to make its elders likeably normal".   Bloody Disgusting gave the film a positive review and wrote that "DiBlasi has crafted a tightly wound downward spiral of infidelity, violence, and small-town adversity that’s a model of restraint." 

==References==
 

==External links==
* 
* 

 
 
 