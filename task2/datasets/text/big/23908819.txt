Pummarò
{{Infobox film
| name           = Pummarò
| image          = 
| caption        = 
| director       = Michele Placido
| producer       = Claudio Bonivento Pietro Valsecchi
| writer         = Michele Placido Sandro Petraglia Stefano Rulli
| starring       = Thywill Abraham
| music          = 
| cinematography = Vilko Filač
| editing        = Ruggero Mastroianni
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Italy
| language       = Italian
| budget         = 
}} Italian drama film directed by Michele Placido. It was screened in the Un Certain Regard section at the 1990 Cannes Film Festival.   

==Cast==
* Thywill Abraham
* Kwaku Amenya
* Salvatore Billa
* Ottaviano DellAcqua
* Nicola Di Pinto
* Franco Interlenghi
* Gerardo Scala
* Pamela Villoresi
* Hermann Weisskopf
* Jacqueline Williams

==See also==
* Films about immigration to Italy

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 