Kollaikaran
{{Infobox film
| name           = Kollaikaran
| image          = 
| caption        =
| director       = Tamil Selvan
| producer       = 
| story          = 
| screenplay     =
| starring       = Vidharth Sanchita Shetty P. Ravi Shankar
| music          = AL. Johan
| cinematography = 
| editing        = LVK Doss
| studio         = Prasad Cine Arts
| distributor    = 
| released       =   
| country        = India
| budget         =
| runtime        = 
| language       = Tamil
}}
Kollaikaran ( ) is a 2012 Tamil film directed by Tamil Selvan. The film, starring Vidharth of Mynaa fame along with Sanchita Shetty,  released on 13 January 2012. 

==Plot==

It is a story of a guy named Kuruvi Vidharth who does small crimes.  He falls in love with a girl named Krishnaveni Sanchita Shetty who on finding him to be a criminal rejects him.

Kurivis elder sister got married into Krishnavenis family member and they become close again.

As fate would have it he was falsely accused of stealing temples jewel.  He eventually kills the one who stole the jewels and goes to jail

The movie ends with Kuruvi back in the same temple and Krishnaveni requesting him to come to home.

==Cast==
*Vidharth as Kuruvi
*Sanchita Shetty as Krishnaveni Ravi Shankar as Nagendran

==Critical Reception==
Rohit Ramachandran of Nowrunning.com rated Kollaikaran 1.5/5 calling it "a staple product from hack-haven Kollywood."  Abhijith Ramesh, a well known critic from Tamil Cinema Community (TCC) dismissed it saying "not worth your time and money" but gave 2.75/5

==Soundtrack==
{{Infobox album
| Name = Kollaikaran
| Type = soundtrack
| Artist = AL. Johan
| Cover = 
| Released = 
| Recorded = 2011
| Genre = Film soundtrack
| Length = 
| Label = 
| Producer = 
| Last album = 
| This album = 
| Next album = 
}}

Film score and the soundtrack are composed by AL. Johan.
{{tracklist
| headline        = Tracklist 
| extra_column    = Singer(s)
| total_length    = 
| title1          = Oorae Sonnanga Harini and Tippu
| length1         = 
| title2          = Kozhutha Goyya
| extra2          = Solar Sai, Krishna Iyer and Kalpana
| length2         = 
| title3          = Veliorae Kiliyae
| extra3          = Vijay Prakash, Shreya Goshal
| length3         = 
| title4          = Sami Kutham
| extra4          = Shankar Mahadevan
| length4         = 
| title5          = Kozhutha Goyya Palam 
| extra5          = Solar Sai, Krishna Iyer and Kalpana
| length5        = 
| title5          =Theme Music
| extra5          = Johan
| length5        = 
}}

==References==

 

 
 
 
 
 


 