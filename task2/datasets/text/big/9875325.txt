Black Roses (film)
 
{{Infobox film
| name           = Black Roses
| caption        =
| image	         = Black Roses FilmPoster.jpeg
| director       = John Fasano
| producer       = Cindy Cirile
| writer         = Cindy Sorrell
| narrator       =
| starring       =
| music          = Elliot Solomon
| cinematography = Paul Mitchnick
| editing        = Ray van Doorn
| distributor    = 
| released       =  
| runtime        = 90 min.
| country        = United States
| language       =
| budget         =
| gross          =
}} American horror heavy metal fans for the all star soundtrack which has become increasingly difficult to find. 

==Plot== Lizzy Borden, among others.  Most of the music for the band "Black Roses" was performed by the members of King Kobra, with Mark Free on vocals, and Carmine Appice on drums.

==Cast==
*Julie Adams as Mrs. Miller
*Carmine Appice as Vinny Apache
*Peter Bontje as Flunkie
*Anthony C. Bua as Tony Ames
*Dave Crichton as Mr. Miller
*Jesse DAngelo as Jason Miller
*Carla Ferrigno as Priscilla Farmsworth
*Margaret Groome as Mrs. Sullivan
*Frank Dietz as Johnny Pratt
*Vincent Pastore as Tonys Dad
*Sal Viviano as Damien 

==Black Roses==
Original music performed by
* Mark Free - Vocals
* Mick Sweda - Guitar
* Alex Masi - Guitar
* Carmine Appice - Drums
* Chuck Wright - Bass
* Elliot Solomon - Keyboards

==Soundtrack==
The soundtrack was released on CD by Metal Blade Records.

*Bang Tango - "Im A Stranger"
*Black Roses - "Dance on Fire"
*Black Roses - "Soldiers of the Night"
*Black Roses - "Rock Invasion"
*Black Roses - "Paradise (Were on Our Way)"
*David Michael-Phillips - "King of Kool"
*Hallows Eve - "D.I.E."
*King Kobra - "Take It Off" Lizzy Borden - "Me Against the World"
*Tempest  - "Streetlife Warrior"

==Release== Thomas Hodge and a foreword by Mondo Media creative director Justin Ishmael. 

The film was released on DVD in the United States by Synapse Films in 2007. 

==External links==
* 
*  

==References==
 

 
 
 
 
 