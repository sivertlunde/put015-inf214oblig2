He Snoops to Conquer
{{Infobox film
| name           =  He Snoops to Conquer
| image          =  "He_Snoops_to_Conquer"_(1944).jpg
| caption        =
| director       = Marcel Varnel
| producer       = Marcel Varnel   Ben Henry
| writer         = Stephen Black   Norman Lee   Howard Irving Young   Langford Reed George Formby Robertson Hare Elizabeth Allan
| music          = Harry Bidgood Eddie Latta 
| cinematography = Roy Fogwell
| editing        = Max Brenner
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 103 mins
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} George Formby, Robertson Hare, Elizabeth Allan, and Claude Bailey. Its plot involves an odd job man who becomes mixed up in corruption in politics and town planning.  Its title is a paronomasia of the famous comedic play, She Stoops to Conquer.

==Plot== cross section salvage instead.

Gribble had agreed to conduct the polling in return for being paid £27.10s  . which he needed to give to a loan shark. However, facing upcoming municipal elections with a clearly unpopular the towns leader decide to invite the inventor Sir Timothy Strawberry to stand for the council to boost its popularity. Strawberry is a wealthy, reclusive, eccentric who enjoys popularity in the town because of his extensive Philanthropy. Strawberry was the only man who did not respond to the polling because Gribble could not get past the door by the butler. Gribble is told he cannot have his money until he completes his survey and is sent off to find out of Strawberry has sound opinions, but again fails to get into the house. He then enjoys a chance meeting with Strawberry in the street, when after a mishap, they find themselves careering through the town on the road sweeper. Gribble accidentally presses a button that releases all the unfavourable polling forms through the street. To avoid the police on their tail they go and shelter in Strawberrys house, where Gribble meets Strawberrys daughter Jane who he is immediately smitten by.

Despite finally persuading Strawberry to fill out his form Gribble is sacked by his bosses when they discover that it was he who originally showed the newspapermen round the town. His problems mount when he is beset by an angry mob of townspeople who have found the abandoned forms on the street and blame Gribble for the cover up. He is also pursued by a bailiff for the money he owes. However, Jane comes up with the idea of Gribble running for the council on a pro-town planning platform. With the support of the newspaper he soon builds up a head of steam and looks likely to be elected. Oxbold and his colleagues plan top this by getting their hands on the forms to destroy the evidence of their dishonesty. After Gribbles furniture is possessed by the bailiffs including the vase where he had stored the forms, he takes part in a desperate race against clock in order to recover them and produce them at a major town planning conference. Gribble fails to recover them but is saved by Strawberry who had recorded them electronically. The film ends with the crooked councillers exposed and Gribble being hailed by the people.

==Cast== George Formby - George Gribble
* Robertson Hare - Sir Timothy Strawbridge
* Elizabeth Allan - Jane Strawbridge
* Claude Bailey - Councillor Oxbold
* James Harcourt - Councillor Hopkins
* Aubrey Mallalieu - Councillor Stubbins Gordon McLeod - Angus McGluee
* Vincent Holman - Butler
* Katie Johnson - Ma, Georges Landlady

==Reception and analysis==
*TV Guide concluded the film as a "not very good outing for Formby." 
*Halliwells Film Guide called it a "spotty star comedy with insufficient zest for its great length." 
*In "Masculinities in British Cinema", Dr. Andrew Spicer wrote that George Formby "personified a cheery, comic ‘little man’, who, despite his bumbling stupidity, has gumption and the will-to-win; in Let George Do It he even punches Hitler on the nose. But although Formby provided what Mass-Observation thought was vital for public morale in the ‘dark days’ of the war, comic relief, his popularity declined rapidly at the end of the war. Neither He Snoops to Conquer (1944) or George in Civvy Street (1946), which attempted to align Formby with social change and reconstruction was successful."  

==References==
 

==External links==
 

 

 
 
 
 
 