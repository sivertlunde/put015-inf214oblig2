Marhi Da Deeva (1989 film)
 

{{Infobox film name = Marhi Da Deeva image =  image_size =  border =  alt =  caption =  director = Surinder Singh producer = Ravi Malik writer =  screenplay =  story = Gurdial Singh based on =   narrator =  starring = Raj Babbar Deepti Naval Parikshit Sahni Pankaj Kapoor Kanwaljit Asha Sharma Harbhajan Jabhal Gopi Bhalla music = Mahinderjit Singh cinematography = Anil Sehgal editing = Subhash Sehgal studio =  distributor = released =   runtime = 115 minutes country = India language = Punjabi
|budget =  gross =
}}
 National Award Punjabi film directed by Surinder Singh, starring Raj Babbar, Deepti Naval and Parikshit Sahni in lead roles.    It was a critically acclaimed    and well-received movie based on a novel of the same name by Gurdial Singh.      

== Music ==

Mahinderjit Singh composed the music with playback singers Jaspal Singh and Prabhsharan Kaur Sidhu. 

== Cast ==

{|class="wikitable"
|-
!Actor/Actress
!Role
|- Raj Babbar||Jagseer Singh/Jagsa
|- Deepti Naval||Bhan Kaur/Bhani
|- Parikshit Sahni||Dharam Singh
|- Bhant Singh/Bhanta
|- Asha Sharma||Nandi (Jagsas mother)
|- Harbhajan Jabhal||Thola Singh (Jagsas father)
|- Pankaj Kapoor||Raunki
|- Gopi Bhalla||Nikka Singh/Nikka
|}

== References ==
 

== External links ==
* 

 
 
 
 


 