Murder on the Campus
{{Infobox film
| name           = Murder on the Campus
| image          = "Murder_on_the_Campus"_(1933).jpg
| caption        =
| director       = Richard Thorpe
| producer       = George R. Batcheller
| writer         = Whitman Chambers (story) Andrew Moses (continuity)
| narrator       =
| starring       = See below
| music          =
| cinematography = M.A. Anderson
| editing        =
| distributor    = Chesterfield Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States English
| budget         =
| gross          =
| website        =
}}

Murder on the Campus is a 1933 American film directed by Richard Thorpe. The film is also known as On the Stroke of Nine in the United Kingdom.  It was based on the novel The Campanile Murders, by Whitman Chambers (D. Appleton & Company|Appleton, 1933).  

==Plot==
Reporter Bill Bartlett is researching a piece on students, but soon finds himself investigating a murder. He hears a gunshot coming from a college bell  tower, and finds himself a murder suspect when police captain Ed Kyne discovers him at the scene of the crime. Bartlett also finds himself in love with one of the chief suspects, Lillian Voyne, and is designated to cover the story as a reporter. After two more students are killed, Bartlett enlists the help of C. Edson Hawley, respected college professor and amateur detective.

== Cast ==
*Charles Starrett as Bill Bartlett
*Shirley Grey as Lillian Voyne
*J. Farrell MacDonald as Police Capt. Ed Kyne Ruth Hall as Ann Michaels
*Dewey Robinson as Detective Sgt. Charlie Lorrimer
*Maurice Black as Blackie Atwater
*Edward Van Sloan as Prof. C. Edson Hawley
*Jane Keckley as Hilda Lund
*Richard Catlett as Wilson, Frat House Manager

==Critical reception==
TV Guide wrote, "the plot of this tidy suspense mystery is developed neatly, with a believable solution to the murders" ;  and Shades of Grey wrote, "after a shakey start (with some pretty lame acting by Starrett and Grey), Murder on the Campus comes together as a fine little murder mystery."  

==References==
 
== External links ==
* 
* 

 

 

 
 
 
 
 
 
 


 