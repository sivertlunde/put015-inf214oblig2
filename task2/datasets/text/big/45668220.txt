Los muertos
{{Infobox film
| name           = Los Muertos
| image          = 
| caption        = Theatrical release poster
| director       = 
| producer       = 
| writer         = Lisandro Alonso
| starring       = Argentino Vargas
| music          = 
| cinematography = Cobi Migliora
| editing        = Lisandro Alonso
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = Argentina
| language       = Spanish, Guarani
| budget         = 
}}

Los Muertos ( ) is a 2004 Argentine drama film directed by Lisandro Alonso. It follows a released prisoner (Vargas) who, convicted of murder years ago, looks for his now adult daughter. Vargas journey is slow and contemplative though Alonso refuses to externalize his psychology, making much of the films events open-ended and impressionistic.

The film is considered within Alonsos "Lonely Man Trilogy," which also includes La Libertad and Liverpool

==Cast==
* Argentino Vargas as Vargas

==References==
 

==External links==
* 

 

 
 
 
 
 


 