Delirium (1979 film)
{{Infobox film
| name           = Delirium
| image          = Delirium 1979 poster.jpg
| alt            =
| caption        = Original theatrical release poster
| film name      =
| director       = Peter Maris
| producer       = Peter Maris Sonny Vest 
| writer         = Peter Maris Richard Yalem
| screenplay     =
| story          = Eddie Krell Jim Loew 
| based on       =  
| starring       =
| narrator       =
| music          = David C. Williams
| cinematography = Bill Mensch
| editing        = Dan Perry
| studio         =  
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =  
}}

Delirium (also known as Psycho Puppet) is a 1979 American thriller film, directed by Peter Maris and written by Maris and Richard Yalem.
 Video Nasties".

== Cast ==

* Turk Cekovsky as Paul Dollinger
* Debi Chaney as Susan Norcross
* Terry TenBroek as Larry Mead (as Terry Ten Broek)
* Barron Winchester as Eric Stem
* Bob Winters as Donald Andrews
* Garrett Bergfeld as Mark
* Nick Panouzis as Charlie Gunther
* Harry Gorsuch as Capt. Hearn
* Chris Chronopolis as Det. Parker
* Lloyd Schattyn as Det. Simms
* Jack Garvey as Devlin
* Mike Kalist as Specter
* Myron Kozman as Wells
* Pat Knapko as Jenny Thompson
* Letty Garris as Hitchhiker
* Charlotte Littrel as Vietnamese prostitute

== Release ==

Delirium was released in July 1979 in the United States.

== Critical reception ==

TV Guides review was generally unfavourable, criticising the films use of Vietnam War flashbacks to explain the killers motivations. 

== References ==

 

== External links ==

*  

 


 