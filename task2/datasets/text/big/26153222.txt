Bed & Breakfast (1992 film)
{{Infobox film
| name           = Bed & Breakfast 
| image          = Bed & Breakfast film cover.jpg
| caption        = 
| director       = Robert Ellis Miller
| producer         = Jack Schwartzman
| writer       = Cindy Myers
| starring       = Roger Moore Talia Shire Colleen Dewhurst Nina Siemaszko Jamie Walters Juliana Paes Marcio Garcia
| music          = David Shire
| cinematography =
| editing        = 
| distributor    = Hemdale Film Corporation
| released       =  
| runtime        = 97 minutes
| country        =United States
| language       = English
| budget         = 
| gross          = 
}}
Bed & Breakfast is a 1992 romantic comedy film directed by Robert Ellis Miller and stars Roger Moore, Talia Shire, Colleen Dewhurst, and Nina Siemaszko.    Filming took place in 1989 in York Beach, Maine. This was the final film of late actress Collen Dewhurst, who died in 1991.

==Plot==
A charming Englishman (Roger Moore) changes the lives of three generations of women who run a near-bankrupt bed and breakfast. The three women find him out cold on a beach and offer him free board in return for fixing the place up and being the handy man.  Claire (Talia Shire), widow of Senator Blake Wellesly, is initially unwilling to let him in the house, partly due to the mystery around him caused by amnesia. Her mother-in-law Ruth (Colleen Dewhurst), recently retired and craving adventure, insists on allowing him inside. Claires teenage daughter Cassie (Nina Siemaszko), who is rebellious against her mothers old-fashioned behavior most of the time, names him Adam.

==Cast==
*Roger Moore as Adam
*Talia Shire as Claire Wellesly
*Colleen Dewhurst as Ruth Wellesly
*Nina Siemaszko as Cassie Wellesly
*Ford Rainey as Amos
*Stephen Root as Randolph Jamie Walters as Mitch
*Cameron Arnett as Hilton
*Bryant Bradshaw as Julius
*Victor Slezak as Alex Caxton
*Jake Weber as Bobby

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 