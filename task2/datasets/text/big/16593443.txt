Teens in the Universe
{{Infobox film
| name           = Teens in the Universe
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Viktorov
| producer       = 
| writer         = Isai Kuznetsov Avenir Zak
| narrator       = 
| starring       = Innokenti Smoktunovsky Lev Durov
| music          = 
| cinematography = 
| editing        = 
| distributor    = Gorky Film Studio
| released       =  
| runtime        = 84 min
| country        = Soviet Union
| language       = 
| budget         = 
| gross          = 
}}
Teens in the Universe ( , transliteration|translit. Otroki vo vselennoy) is a Soviet 1974 film directed by Richard Viktorov based on a script by Isai Kuznetsov and Avenir Zak. Preceded by Moscow-Cassiopeia (first part, 1973). Runtime - 84 min.

== Cast ==
* Innokenti Smoktunovsky as I.O.O.
* Vasili Merkuryev as academician Blagovidov
* Lev Durov as academician Filatov Yuri Medvedev as academician Ogon-Duganovsky
* Pyotr Merkuryev as academician Kurochkin

=== Space ship Zarya crew === Mikhail Yershov as Vitya Sereda
* Aleksandr Grigoryev as Pasha Kozelkov
* Vladimir Savin as Misha Kopanygin
* Vladimir Basov Jr. as Fedya "Lob" Lobanov
* Olga Bityukova as Varya Kuteishchikova
* Nadezhda Ovcharova as Yulia Sorokina
* Irina Popova as Katya Panfyorova

=== Other cast ===
* Vadim Ledogorov as Agapit
* Igor Ledogorov as Agapits father
* Natalya Fateyeva
* Anatoli Adoskin
* Aleksandr Lenkov
* Nikolai Pogodin
* Raisa Ryazanova
* S. Safonov
* Nadezhda Semyontsova
* Vladimir Shiryayev
* Olga Soshnikova
* Aleksandr Vigdorov
* Mikhail Yanushkevich
* Aleksandr Zimin

==Awards==
* Premio for the Best Film for Kids and Youth of the All-Union Cinema Festival, Kishinev, 1975
* Special Premio "Silver Asteroid" of the International Cinema Festival of Science Fiction Films, Triest, 1976
* Gran Premio of the International Festival at Panama, 1976
* State Premio of RSFSR in the honour of Vasilyiev Brothers, 1977.

==External links==
* 
* 

 
 
 
 
 


 
 