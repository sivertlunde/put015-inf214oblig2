Dol (film)
{{Infobox film
| name           = Dol
| image          = Dol (film).jpg
| image_size     = 
| alt            = 
| caption        = Theatrical poster
| director       = Hiner Saleem
| producer       = Hiner Saleem Michel Loro
| writer         = Hiner Saleem
| starring       = Nazmi Kirik Belcim Bilgin Omar Chawshin Rojîn Ulker
| music          = Özgür Akgül Mehmet Erdem Vedat Yildirim
| cinematography = Andreas Sinanos
| editing        = Dora Matzoros Bonita Papastathi
| studio         = Hiner Saleem Production Novociné
| distributor    = Mitosfilm
| released       =  
| runtime        = 90 minutes Kurdistan Germany France Kurdish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 2007 film directed by the Kurdish director Hiner Saleem and produced in Germany, France and Southern Kurdistan.

==Plot==
Azad and his fiancée Nazenin are about to get married but their plan falls into disarray as a dispute breaks out with Turkish soldiers on their wedding day resulting in death of a Turkish commando. Azad escapes and goes on a journey across the Kurdish regions of Iran and Iraq which brings him closer to the culture and living conditions of his people.

==Cast==
* Nazmi Kirik
* Belcim Bilgin
* Omer Chawshin
* Rojîn Ulker
* Tarik Akreyî
* Ciwan Haco
* Abdullah Keskîn
* Sibel Erdogan
* Dariush Fayeq

==External links==
*  
*  , review by Jonathan Crocker, Time Out London.
*  , review by Ariana Mirza.

 
 
 
 
 