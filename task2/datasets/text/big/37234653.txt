Oedipus the King (1968 film)
{{Infobox film
| name           = Oedipus the King
| caption        =
| image	=	Oedipus the King 1968.jpg
| director       =  Philip Saville 
| producer       =Timothy Burrill and Michael Luke	
| writer         =Sophocles, Michael Luke, Philip Saville  Richard Johnson
| music          =Yiannis Hristou (as Jani Christou)
| cinematography =Walter Lassally
| editing        =
| studio         = Crossroads World Film Services, Universal Pictures
| distributor    = 
| released       =1968
| runtime        = 97 mins
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 Richard Johnson as Creon, Roger Livesey as the Shepherd, and Donald Sutherland as the leading Member of the Chorus.       Sutherlands voice, however, was dubbed by another actor. Filmed in Greece at a  ruined Greek theatre in Dodoni,  it was not seen in Europe and the U.S. until the 1970s and 80s after legal release and distribution rights were granted to video and TV and was considered a rare film.

Savilles first theatrical effort, the film remained theatrical in nature,  and is known for its intensive dialogue typical of an ancient play.      
However, the film went a step further than the play, by actually showing, in flashback, the murder of Laius (Friedrich Ledebur). It also showed Oedipus and Jocasta in bed together, making love.

==Cast==
*Christopher Plummer as Oedipus
*Lilli Palmer is Jocasta Richard Johnson as Creon
*Orson Welles as Tiresias
*Cyril Cusack as Messenger
*Roger Livesey as Shepherd
*Donald Sutherland as Chorus Leader
*Friedrich von Ledebur as King Laius
*Dimos Starenios as Priest
*Alexis Mann as Palace Official
*Oenone Luke as Antigone
*Cressida Luke as Xemene
*Minos Argyrakis as Chorus
*Manos Destounis as Chorus
*George Dialegmenos as Chorus
*Valentine Dyall as Chorus Leader (voice)

==Reception==
 , filming location of the film]] New York Magazine described it as "almost comical" in a September 1968 review;    a 1972 review said "An elaborate production, overly academic and without much force or cinematic merit."    However, in 1968 the Illustrated London News praised its "cinematic fluidity"    and Jon Solomon in 2001 said that the film was "distinguished by intensity and fine acting", with Plummers Oedipus boasting "an arrogant, strong-willed title character". However, Solomon also remarked that the film "would never have won first prize at an ancient Athenian contest.    Leonard Maltin in 2006 said that the "film version of Sophocles play is OK for students who have a test on it the next day, but others wont appreciate this version."   
The script was originally based on Paul Roches translation directly from the Greek done in the early 1950s. (Penguin USA OEDIPUS PLAYS/PAUL ROCHE). Torn between the dynamic theatricality of the Ancient  Greek and modern realism the film loses what it might have had by lurching between. If Sophocles words, through the poetic translation provided by Roche, had been given full voice, in the amphitheatre at Dodoni , without the pretence of the real, the film could have become a cult. Sadly Producer Luke and Director Saville never quite gelled their ideas and an interesting 1960s experiment  foundered in the confusion.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 