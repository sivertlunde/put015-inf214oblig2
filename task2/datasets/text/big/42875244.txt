Deadly-2
{{Infobox film
| name = Deadly-2
| image =
| caption =
| director = Ravi Shrivatsa
| writer = Ravi Shrivatsa Aditya  Meghana   Suhasini   Devaraj
| producer = M Manjunath Gowda
| music = L. N. Shastry
| cinematography = Mathew Rajan
| editing = Lakshman Reddy
| studio = K K Films
| released =  
| runtime = 126 minutes
| language = Kannada
| country = India
| budget =
}}
 2010 Kannada action - crime biographical film directed and written by Ravi Shrivatsa. The film is based on the real life incidents of an infamous underworld don, Soma and is a sequel to the same teams 2005 released film, Deadly Soma.  The film features Auditya reprising his role as the protagonist  and Meghana as his lady love replacing Rakshita in the prequel. Suhasini Maniratnam, Devaraj and Ravi Kale play other pivotal roles. 

The film featured original score composed by L. N. Shastry. The film, upon release received mixed response from both critics and audience and got a negative publicity owing to the violence being glorified throughout the film. 

== Cast == Aditya as Somashekara
* Meghana as Jyothi
* Devaraj as Ashok Kumar
* Suhasini Maniratnam
* Ravi Khale
* Praveen
* Sangeetha
* Syed Kirmani in a guest appearance

==Plot==
This film is based on a real life story of a young man from a well-civilized family who takes the criminal route due to unavoidable circumstances.

== Music ==
The background music was composed by L. N. Shastry. There is only one situational song composed by him for the film.

==See also==
* Deadly Soma

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 


 

 