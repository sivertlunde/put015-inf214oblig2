The Noon Whistle
 
{{Infobox film
| name           = The Noon Whistle
| image          = 
| image size     = 
| caption        = 
| director       = George Jeske
| producer       = Hal Roach
| writer         = Stan Laurel
| narrator       = 
| starring       = Stan Laurel
| music          = 
| cinematography = Frank Young
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  English intertitles
| budget         = 
}}
 silent comedy film starring Stan Laurel.   

==Cast==
* Stan Laurel - Tanglefoot James Finlayson - OHallahan, the foreman
* Katherine Grant - Secretary
* Sammy Brooks - A millworker (as Sam Brooks)
* William Gillespie - President of the lumber company
* Noah Young - A millworker
* John B. OBrien - A millworker (as Jack OBrien)

==See also==
* List of American films of 1923
* Stan Laurel filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 