The Captain from Loyola
{{Infobox film
| name =   The Captain from Loyola
| image =
| image_size =
| caption =
| director = José Díaz Morales
| producer = Juan Mari   Guillermo Calderon   Pedro A. Calderon Ricardo Toledo   José Díaz Morales
| narrator =
| starring = Rafael Durán   Maruchi Fresno   Manuel Luna
| music = Manuel Parada
| cinematography = Theodore J. Pahle
| editing = Bienvenida Sanz
| studio = Producciones Calderón   Intercontinental Films
| distributor = CEPISCA
| released = 1 April 1949 
| runtime = 100 minutes
| country = Spain Spanish
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}

The Captain from Loyola (Spanish:El capitán de Loyola) is a 1949 Spanish historical film directed by José Díaz Morales and starring Rafael Durán, Maruchi Fresno and Manuel Luna.  The film portrays the life of Ignatius of Loyola, and was part of a group of lavish historical films made in Spain during the era.

==Cast==
*  Rafael Durán as Íñigo de Loyola  
* Maruchi Fresno as Reina Juana 
* Manuel Luna as Beltrán 
* Asunción Sancho as Marcelilla 
* José María Lado as Armador  
* José Emilio Álvarez 
* María Rosa Salgado as Infanta Catalina  
* Francisco Pierrá
* Manuel Dicenta as Pedro Fabro  
* Carlos Díaz de Mendoza 
* Manuel Kayser 
* Eduardo Fajardo 
* Arturo Marín as Alcalde de Pamplona  
* Manuel Arbó 
* Dolores Moreno  Domingo Rivas
* Manuel Guitián
* María Antonia Giménez 
* Rufino Inglés
* José Prada
* María Cañete 
* Santiago Rivero 
* Fernando Aguirre 
* Rosario Royo 
* Alfonso Manzanares 
* Carlos Pontes  Francisco Alonso 
* Carmelo Gandarías
* Alicia Palacios
* Ricardo Acero as Francisco Javier

==References==
 

==Bibliography==
* DLugo, Marvin. Guide to the Cinema of Spain. Greenwood Publishing Group, 1997.

==External links==
* 

 
 
 
 
 
 
 
 
 

 