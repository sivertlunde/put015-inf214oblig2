Cadence (film)
{{Infobox Film
| name           = Cadence
| image          = Cadenceposter.jpg
| caption        = Promotional movie poster for the film
| director       = Martin Sheen
| producer       = Timothy Gamble Frank Giustra Peter E. Strauss
| writer         = Dennis Shryack
| starring       = Martin Sheen Charlie Sheen Larry Fishburne Michael Beach
| music          = Georges Delerue
| cinematography = Richard Leiterman
| editing        = Martin Hunter
| distributor    = New Line Cinema
| released       =  : January 18, 1990
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2,070,871 

}}
Cadence is a 1990 film directed by (and starring) Martin Sheen, in which Charlie Sheen plays an inmate in a United States Army military prison in West Germany during the 1960s.  Sheen plays alongside his father Martin Sheen and brother Ramon Estevez. The film is based on a novel by Gordon Weaver.

==Plot== PFC Franklin Bean (Charlie Sheen) gets drunk and goes AWOL upon the death of his father. As punishment, he is thrown into a stockade populated entirely by black inmates. But instead of giving into racism, Bean joins forces with his fellow inmates and rises up against the bigoted prison warden, MSgt. Otis McKinney (Martin Sheen).

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor || Character
|-
| Charlie Sheen || Pfc. Franklin Fairchild Bean
|-
| Martin Sheen || MSgt. Otis V. McKinney
|-
| Larry Fishburne || Roosevelt Stokes
|-
| Blu Mankuma || Bryce
|-
| Michael Beach || Webb
|-
| Harry Stewart || Harry Sweetbread Crane
|-
| John Toles-Bey || Lawrence
|- James Marshall || Cpl. Harold Lamar
|-
| Ramon Estevez || Cpl. Gerald Gessner
|-
| Jay Brazeau || Mr. Vito
|-
| Samantha Langevin || Mrs. Vito
|-
| Ken Douglas || Kramer
|-
| Weston McMillan || DeLuc
|-
| David Michael ONeill || Sager
|-
| Allan Lysell || Sheridan
|-
| Don S. Davis || Haig
|-
| Roark Critchlow || Brooks
|-
| Alec Burden || Abel Fox
|-
| Steven Hilton || Col. Clark
|-
| Joe Lowry || Col. Porter
|-
| Lochlyn Munro || Bartender
|-
| Tom McBeath || Principal
|-
| Jennifer Griffin || Tattoo Woman
|-
| Brent Stait || MP
|-
| Deryl Hayes || MP in bar
|-
| Tony Pantages || GI in bar
|-
| Christopher Judge || MP in bar
|-	 Matt Clark || Franklin F. Bean, Sr.
|-
| Robert Gazzola || Bean (age 8)
|-
| David Glyn-Jones || Funeral Director
|-
| F. Murray Abraham || Capt. Ramon Garcia
|-
| Jenn Griffin || Tattoo woman
|}

==Cast Notes==

*Laurence Fishburne worked with Martin Sheen ten years earlier, on Apocalypse Now.

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 
 

 