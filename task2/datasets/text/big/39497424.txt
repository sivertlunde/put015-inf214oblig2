The Last Witch Hunter
{{Infobox film
| name           = The Last Witch Hunter
| image          = The Last Witch Hunter poster.jpg
| alt            =
| caption        = Teaser poster
| director       = Breck Eisner
| producer       = Adam Goldworm   Tom Rosenberg   Gary Lucchesi   Eric Reid
| writer         = Cory Goodman   D.W. Harper   Melisa Wallack 
| starring       = Vin Diesel   Rose Leslie   Elijah Wood   Michael Caine
| music          = Steve Jablonsky  
| cinematography =
| editing        = Mark Canton Productions One Race Entertainment 
| distributor    = Lionsgate Entertainment
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Last Witch Hunter is an upcoming supernatural action film, which will be directed by Breck Eisner. The film will star Vin Diesel as an immortal witch hunter who must stop a plague from ravaging New York City.    The film is scheduled to be released on October 23, 2015.

==Synopsis== witch hunter is tasked with the job of coming between the covens of New York City and their goal to destroy humanity by way of a horrific plague. In order to accomplish this he must partner up with a beautiful female witch, something that he thought he would never do.

==Cast==
* Vin Diesel as Kaulder
* Rose Leslie as Chloe
* Elijah Wood as the 37th Dolan
* Michael Caine as Father Dolan
* Rena Owen as Glaeser
* Julie Engelbrecht as Witch Queen
* Ólafur Darri Ólafsson as Belial
* Isaach De Bankolé as Schlesinger
* Lotte Verbeek as Helena

==Production==
Plans to film The Last Witch Hunter were announced in 2012 and initially Timur Bekmambetov was to direct the film based on a script written by Cory Goodman.  Bekmambetov was later replaced by Breck Eisner and Goodmans script was re-written by D.W. Harper before Melisa Wallack was brought on work on the films script.       Vin Diesel was announced to be performing in the film, which will be produced through Lionsgate-Summit.  The production filed for a film tax credit in Pennsylvania and was allocated a tax credit of $14 million.   In February 2014, Vin Diesel posted a photo of the films concept artwork to his Facebook page and Lionsgate CEO Jon Feitheimer commented that if successful, The Last Witch Hunter could become a film franchise.  In July 2014, it was announced that Rose Leslie would be joining the cast as Vin Diesels co-star,  and in August, Elijah Wood, Michael Caine, and Ólafur Darri Ólafsson were also announced as attached to the film.    Julie Engelbrecht and Lotte Verbeek will also star.  

===Filming===
Filming for The Last Witch Hunter was initially delayed due to the death of Paul Walker,    as the death delayed shooting for Furious 7. Lionsgate officially began setting up for filming in Pittsburgh in June 2014.  The filming began on September 5, 2014, in Pittsburgh, as Diesel posted a first look of himself on Facebook.    The shooting lasted until December 5. 

==Release==
On October 24, 2014, Lionsgate set the film for an October 23, 2015 release. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 