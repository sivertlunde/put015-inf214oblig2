AfterLife (film)
 
 {{Infobox film
| name           = AfterLife
| image          = 
| caption        = 
| director       = Alison Peebles
| producer       = Catherine Aitken Ros Borland
| writer         = Andrea Gibb
| screenplay     = 
| story          = 
| based on       =  
| starring       = Lindsay Duncan Kevin McKidd Paula Sage James Laurenson
| music          = Paddy Cunneen
| cinematography = Grant Cameron
| editing        = Colin Monie
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

AfterLife is a 2003 film drama set in Scotland directed by Alison Peebles and original screen play by Andrea Gibb. An ambitious Scottish journalist forced to choose between his high-flying career or caring for his younger sister who has Down syndrome. 

Afterlife won the Audience Award at The Edinburgh Film Festival 2003. 

Stars Lindsay Duncan, with Kevin McKidd and Shirley Henderson. Newcomer Paula Sage holds her own as the Bingo (Commonwealth)|Bingo-playing sister Roberta. Sages role won her a BAFTA Scotland award for best first time performance and Best Actress in the Bratislava International Film Festival, 2004. 

James Laurenson and Isla Blair also have acting roles.

==Cast==

*Lindsay Duncan	as May Brogan
*Kevin McKidd as Kenny Brogan
*Paula Sage as Roberta Brogan
*James Laurenson as Professor Wilkinshaw
*Shirley Henderson as Ruby
*Fiona Bell as Lucy
*Anthony Strachan as Mike
*Emma DInverno as Rosa Mendoza
*Eddie Marsan as Walters Jez
*Isla Blair as Jackson, Dr
*Stuart Davids as Big Tony
*Julie Austin as Foghorn Heather
*Martin Carroll as Bingo manager
*Maureen Carr as Cissie
*Molly Innes as Social worker
*Isabelle Joss as Care home nurse
*Julie Wilson Nimmo as Hospital nurse
*Alison Peebles as Radiographer

==External links==
* 
* 

 
 
 
 