Shor in the City
 
 
{{Infobox film
| name           = Shor in the City
| image          = Shor In the City.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Raj Nidimoru Krishna D.K.
| producer       = Ekta Kapoor Shobha Kapoor
| writer         = Raj Nidimoru Krishna D.K. Sita Menon Zakir Hussain Amit Mistry
| music          = Sachin-Jigar Harpreet
| cinematography = Tushar Kanti Ray
| editing        = Ashmith Kunder
| studio         =
| distributor    = ALT Entertainment Balaji Motion Pictures
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =    
| gross          =    (nett.domestic gross) 
}}
Shor in the City is a 2011 Indian Hindi film directed by Raj Nidimoru and Krishna D.K. starring Tusshar Kapoor, Sendhil Ramamurthy, Preeti Desai, Girija Oak, Radhika Apte, Nikhil Dwivedi, Pitobash Tripathy, Sundeep Kishan, and Amit Mistry.  The background score of the film was composed by Roshan Machado. The first look of the film was unveiled on 21 March 2011  while the film was released in India on 28 April 2011.  

==Plot== NRI who returns to India to start his own small business and meets Shalmili (Preeti Desai). Sawan (Sundeep Kishan) is a young cricketer hoping to break into the under-22 Mumbai cricket team. The story focuses on their trials and tribulations as they battle life in the city of Mumbai during the chaotic period of the festival of Ganesh Chaturthi.

The film tries to deal with concepts of chance, the constant struggle between hope and despair and self-actualization. The opening of the movie starts with the song "Karma is a bitch" which fits in with the central theme of the story where the characters of the film keep trying to come to terms with their own actions. Abhay has a dark past which is not revealed in the film but it can be speculated that he came to India to stay away from it. Tilak initially holds himself responsible for the tragic accident that happens in the movie which brings him closer to his wife and viewing life from a different perspective. The goons who traumatize Abhay eventually end up being shot by their own bullets on the day of Ganesha Visarjan. Tilak gets a new life and finds his treasure at home. Sawan who is financially pressed decides to keep the money he finds at the bank robbery and gives up on the cricket selections – a choice he could have well chosen not to take. Towards the end of the movie all characters move on different paths.

==Cast==
* Sendhil Ramamurthy as Abhay
* Tusshar Kapoor as Tilak
* Nikhil Dwivedi as Ramesh
* Preeti Desai as Shalmili
* Sundeep Kishan as Sawan
* Pitobash as Mandook
* Radhika Apte as Sapna
* Girija Oak as Sejal Zakir Hussain
* Amit Mistry as Tipu

==Production==
Shor in the City was made on a small budget of 30&nbsp;million and a further 27.5&nbsp;million on P&A, with total investment coming to around 60&nbsp;million. Trade analysts estimated that around 50% of this will be recovered from non-theatrical revenue but as always it is the theatrical revenue that decides a films fate at the box office.     Balaji Motion Pictures, head of distribution and acquisition Girish Johar says "We gave Shor in the City a tight release of around 500 screens across the country and the film found appreciation. It grossed around 38.5&nbsp;million in just 4 days and adding the revenue from non-theatrical rights, the whole investment on the film has been more than recovered."   

==Reception==

===Critical response===
The film was well received by critics. Nikhat Kazmi of The Times of India awarded it four stars out of five and stated "With a zany screenplay (Raj Nidimoru and Krishna DK), excellent cinematography by Tushar Kanti Ray and peppy music by Sachin-Jigar, Shor in the City is another breaking-norm film from Ekta Kapoor".  Taran Adarsh of Bollywood Hungama gave it three and a half stars and wrote "Shor in the City belongs to one of those rare categories of movies with sensibilities that would not only entice the festival crowd and the cinema literate, but also lure the ardent moviegoer."  Aniruddha Guha of the Daily News and Analysis gave it three stars and said "Shor in the City is the kind of reassuring film you yearn to watch amid, well, what Bollywood has to offer every week. Also, it articulates something you have only probably thought before – Karma IS a bitch."  Rajeev Masand of CNN-IBN awarded three and a half stars saying "A delicious mix of quirky humor, gruesome violence, and surprising sensitivity, Shor in the City works on the strength of its smart script and consistent performances from its ensemble cast."  Rediff awarded the film four stars and said "Raj-Krishnas Shor in the City robotically registers itself in Indian cinemas history."  Anupama Chopra of NDTV gave the movie four stars and wrote "Shor in the City is a terrific film. It’s surprising and disturbing and has a vein of rich, dark humor coursing through it."  Shubhra Gupta of the Indian Express gave it a three star rating and commented "What makes Shor in the City an instant clutter-breaker is its darkly comic treatment. It makes you smile because its humour comes from within. It’s not grafted. And it’s got heart : we feel for the characters." 
Tushar Joshi of MiD DAY gave it a four star rating and wrote "Loaded with humor, sarcasm and wit. That truly is the beauty of the makers who succeed in arresting you with their tales. The spectacular climax is easily one of the best written in recent times." 
Karan Anshuman of Mumbai Mirror gave it a four star rating, saying "Three stories, eleven days, myriad layers, believable characters, fine performances, spirited direction, taut script, momentary explosions of originality." 

==Boxoffice==
Shor in the City nett. grossed   in India.    Shor in the City also opened poorly at around 10–15% in 475 cinemas(500 screens) in India. It was estimated that the film has advantage of low costs which should be recovered but theatrical business may not be much.  Shor in the City grossed around 17.5&nbsp;million nett over the first weekend. Its best business was in some Mumbai multiplexes.  Shor in the City grossed 35.0&nbsp;million nett approx in its first week. The distributor share is 1.75 approx.  Shor in the City made no headway on the weekdays.    

==Awards==
Shor in the City was officially selected for the Pusan International Film Festival and the Dubai International Film Festival in 2010. The film won the Best Director Award for Raj Nidimoru and Krishna DK at MIAAC in New York. 

;Zee Cine Awards
* 2012: Zee Cine Award Best Female Playback Singer: Shreya Ghoshal for the song "Saibo"

==Soundtrack==
The songs are composed by Sachin-Jigar and Harpeet. Two songs from Harpreet were taken from his Sufi compilation called Teri Justujoo released by Sony in 2008.  Lyrics are penned by Sameer and Priya Panchal. Shor in the City Soundtrack Tops The Charts in few days. The song Karma Is a Bitch is influenced from Ida Marias Bad Karma. 

==Soundtrack==
{{Infobox album |  
 Name = Shor in the City
| Type = Soundtrack
| Artist = Sachin - Jigar
| Cover = 
| Released = 28 April 2011
| Genre = Film soundtrack
| Length =
| Label = Sony Music
| Producer = Ekta Kapoor
| Last album = Hum Tum Shabana (2011)
| This album = Shor in the City (2011)
| Next album = Tere Naal Love Ho Gaya (2012)
}}

===Track listing===
{{track listing
| extra_column = Singer(s)
| music_credits = yes
| lyrics_credits = yes

| title1 = Saibo
| extra1 = Shreya Ghoshal, Tochi Raina
| music1 = Sachin-Jigar
| lyrics1=Sameer; additional lyrics: Priya Panchal
| length1 = 3:16

| title2 = Karma Is a Bitch
| extra2 = Suraj Jagan, Priya Panchal
| music2 = Sachin-Jigar
| lyrics2=Sameer; additional lyrics: Priya Panchal
| length2 = 3:18

| title3 = Shor
| extra3 = Mohan Kannan
| music3 = Sachin-Jigar
| lyrics3=Sameer; additional lyrics: Priya Panchal
| length3 = 4:09

| title4 = Deem Deem
| extra4 = Shriram Iyer
| music4 = Harpreet
| lyrics4=Nishu
| length4 = 2:43

| title5 = Saibo
| note5 = Remix by DJ Sukethu featuring PaVan
| extra5 = Shreya Ghoshal, Tochi Raina
| music5 = Sachin-Jigar
| lyrics5=Sameer; additional lyrics: Priya Panchal
| length5 = 3:21
}}

===Bonus tracks===
{{track listing
| extra_column = Singer(s)
| music_credits = yes
| lyrics_credits = yes

| title6 = Teri Justajoo
| extra6 = Roopkumar Rathod, Kshitij Tarey
| music6 = Harpreet
| lyrics6= Ravi
| length6 = 4:35

| title7 = Ujale Baaz
| extra7 = Mohan
| music7 = Agnee
| lyrics7= Agnee
| length7 = 4:06

| title8 = Bam Lahiri
| extra8 = Kailash Kher
| music8 = Kailasa
| lyrics8= Kailash Kher
| length8 = 5:17
}}

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 