Three Hats for Lisa
{{Infobox film
| name           = Three Hats for Lisa
| image size     =
| image          =  "Three_Hats_for_Lisa"_(1966).jpg  
| alt            =
| caption        = UK poster by Tom Chantrell
| director       = Sidney Hayers
| producer       = Jack Hanbury
| writer         = David D. Osborn
| screenplay     = Leslie Bricusse Talbot Rothwell
| story          =
| narrator       = Joe Brown Sophie Hardy  Sid James Eric Rogers
| cinematography = Alan Hume
| editing        = Tristam Cones
| studio         = Seven Hills Productions
| distributor    = Anglo-Amalgamated Film Distributors  (UK)
| released       = 
| runtime        = 99 minutes
| country        = UK English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} British musical musical comedy Joe Brown, Sid James, Sophie Hardy, Una Stubbs and Dave Nelson. 

==Plot==
Three young Cockneys take a day off work to meet an Italian movie star at Heathrow airport. She travels with them and their taxi driver in search of some typically British hats. The rule of the game is to steal a hat from its wearer. Lisa wants a bobbys helmet, a businessmans bowler, and the bearskin cap off a palace guard. A musical chase ensues around Swinging Sixties London, evading press and police.

==Cast== Joe Brown - Johnny Howjego
* Sophie Hardy - Lisa Milan
* Una Stubbs - Flora
* Sid James - Sid Marks
* Dave Nelson - Sammy
* Peter Bowles - Pepper
* Seymour Green - Signor Molfino
* Josephine Blake - Miss Penny
* Jeremy Lloyd - Guards Officer Michael Brennan - Police Sergeant
* Eric Barker - Station Sergeant
* Howard Douglas - Cinema caretaker
* Dickie Owen - Policeman
* Norman Mitchell - Truck driver
* Arnold Bell - Hilton Doorman
* Barrie Gosney - Reporter

==Songs==
All songs were written by Leslie Bricusse

"This is a Special Day" (written by Leslie Bricusse and Robin Beaumont) 
Performed by Joe Brown

"The Boy on the Corner of the Street Where I Live" 
Performed by Una Stubbs, Sandra Hampton and Beth McDonald

"Something Tells Me (I Shouldn’t Do This)" 
Performed by Joe Brown, Una Stubbs and Dave Nelson

"I’m the King of the Castle" 
Performed by Joe Brown, Una Stubbs and Dave Nelson

"Bermondsey" 
Performed by Joe Brown, Sophie Hardy, Sid James, Una Stubbs and Dave Nelson

"L O N D O N (London Town)" 
Performed by Joe Brown, Sophie Hardy, Sid James, Una Stubbs and Dave Nelson

"Three Hats for Lisa" 
Performed by Joe Brown, Sophie Hardy, Sid James, Una Stubbs and Dave Nelson

"Two Cockney Kids" 
Performed by Joe Brown and Una Stubbs

"Have You Heard About Johnny Howjego?" 
Performed by Sid James, Una Stubbs and Dave Nelson and chorus

"Thats What Makes A Girl A Girl" 
Performed by Joe Brown, Sophie Hardy, Sid James, Una Stubbs and Dave Nelson

"I Fell in Love With An Englishman" 
Performed by Sophie Hardy

"A Man s World" 
Performed by Sophie Hardy

"Covent Garden" 
Performed by Joe Brown, Sophie Hardy, Sid James, Una Stubbs and Dave Nelson and chorus

"One Day in London" 
Performed by chorus

==References==
 

 

 

 
 
 
 
 
 
 
 


 
 