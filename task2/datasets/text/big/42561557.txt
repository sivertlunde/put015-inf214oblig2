Paris Pranaya
{{Infobox film name           = Paris Pranaya image          = image_size     = caption        = director       = Nagathihalli Chandrashekar producer       = Amarnatha Gowda Harinath Policharla Tumkur Dayanand Vidyashankar writer         = Nagathihalli Chandrashekar narrator       = starring  Tara
|music          = Stephen Prayog cinematography = Krishna Kumar editing        = Basavaraj Urs studio         = 21st Century Lions Cinema released       =   runtime        = 151 minutes country        = India language  Kannada
|budget         =
}}
 Kannada romantic Tara and Sharath Lohitashwa feature in other prominent roles.  The film was produced by 21st Century Lions Cinema banner.

The film released on 18 April 2003 to generally positive reviews from critics.  Extensively shot in many European countries such as Paris, Rome, Southern France and Spain, the film covers a scene of the annual "Vishwa Kannada Sammelana - 2002" held at Detroit.  It went on to win awards at the Filmfare Awards South and Karnataka State Film Awards for the year 2003.

==Cast==
* Raghu Mukherjee as Krish alias Krishna
* Minal Patil as Poorvi Rajesh as H. K. Master 
* Sharath Lohitashwa as Aditya Tara as Cell Seetha
* Harinath Policharla
* Sumalatha
* Sudha Belawadi
* Nagathihalli Chandrashekar guest appearance

===Voice-over===
* Rajesh Krishnan dubbed for Raghu Mukherjee
* Nanditha dubbed for Minal Patil

== Music ==
{{Infobox album  
| Name       = Paris Pranaya
| Type       = soundtrack
| Artist     = Stephen Prayog
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Akash Audio
| Producer   = 
| Last album = 
| This album = Paris Pranaya (2003)
| Next album = Karka Kasadara (2005)
}} Kannada cinema. Krishna Nee Yamuna Kalyani.

{{Track listing
| collapsed       = 
| headline        = Paris Pranaya (Original Motion Picture Soundtrack)
| extra_column    = Artist(s)
| total_length    = 36:11
| all_music       = Stephen Prayog
| lyrics_credits  = yes
| title1          = Pyaris Pranaya
| lyrics1         = Nagathihalli Chandrashekar
| extra1          = Suresh Peters
| length1         = 5:05
| title2          = Dhig Dhig Digantadache
| lyrics2         = Nagathihalli Chandrashekar
| extra2          = Srinivas (singer)|Srinivas, K. S. Chitra
| length2         = 6:40
| title3          = Yede Thumbi Hadidenu
| lyrics3         = G. S. Shivarudrappa
| extra3          = Nanditha
| length3         = 5:16
| title4          = Krishna Nee Begane Baaro
| lyrics4         = Nagathihalli Chandrashekar
| extra4          = Shreya Ghoshal
| length4         = 5:47
| title5          = A Biliyara
| lyrics5         = Nagathihalli Chandrashekar
| extra5          = Rajesh Krishnan, Madhu Balakrishnan
| length5         = 3:45
| title6          = Appanaane
| lyrics6         = Nagathihalli Chandrashekar
| extra6          = Nanditha, Hemanth
| length6         = 3:54
| title7          = Rome Rome
| lyrics7         = Nagathihalli Chandrashekar
| extra7          = Shreya Ghoshal, Sonu Nigam
| length7         = 5:44
}}

==Awards==
* Karnataka State Film Awards Best Music Director - Stephen Prayog Best Lyricist - Nagathihalli Chandrashekar Best Female Playback Singer - Nanditha

* Filmfare Awards South Best Film - Tumkur Dayanand

==References==
 

==External source==
*  
*  
*  

 
 
 
 
 
 
 
 
 

 