Lavender (2000 film)
 
 
{{Infobox film
| name           = Lavender
| image          = Lavender2000HK.jpg traditional = 薰衣草
 | simplified = 薰衣草
 | jyutping = Fan Yi Cho}}
| alt            = 
| caption        = Lavender theatrical poster
| director       = Riley Yip Kam-Hung
| producer       = Claudie Chung Chun
| writer         = Riley Yip Kam-Hung
| starring       = Takeshi Kaneshiro Kelly Chen Eason Chan
| music          = Ronald Ng Luk-Sing
| cinematography = Kwan Pun-Leung
| editing        = Maurice Li Ming-Wen Golden Harvest (China) Company Limited GH Pictures United Filmmakers Organization (UFO)
| distributor    = Golden Harvest Company Golden Village Warner Bros.
| released       =   
| runtime        = 111 minutes
| country        = Hong Kong
| language       = Cantonese 
| budget         = 
| gross          = HK $14,554,221.00
}}
  Lost and Found" and 1998 "Anna Magdalena". 

== Plot ==
A young womans life is changed when an angel with a broken wing lands on her balcony.

Athena (Kelly Chen) is a aroma therapy teacher and scent shop owner. She is lonely and depressed, her life is empty, she desperately longs and misses her deceased boyfriend Angelo. Each day she purchases a helium balloon, writes an message on it and releases it to the skies hoping it will reach Angelo. The days and nights come and go without much surprises until one night when an angel named Angel (Takeshi Kaneshiro) crashes into her apartment balcony. At first she thinks he is a burglar until he explains to her that he is an injured angel who cant return to heaven until the Holy Door is opened plus he must gather love to be able to fly again. She lets him stay at her place until he can return to heaven but he must be her servant to repay for staying at her home. He tells her he wont be much of an bother since he doesnt consume human food as he only survives on humans giving him love. 
 SDU team officer who had dead suddenly of heat stroke one day during a typical training day and that Athena has never really gotten over his death. 

Angel and Chow Chow become fast friends. Every night Chow Chow brings Angel out clubbing with him, where Angel receives loves and money from women he dances for. Angel uses the money he earns to buy many pairs of shoes. When Athena asks him why he has brought so many pairs of shoes he tells her because heaven doesnt have shoes. Even with his newly purchased many pairs of shoes Angel still prefers to wear the old laced up boots Athena gave him. Athena does not approve of Angel and Chow Chows new found friendship and tells Angel not to associate with Chow Chow anymore as she sees Chow Chow as a bad influence. 

Seeing how Athena has never gotten over Angelo he tries to help her move on with Chow Chows help. They decide to have a single guy party at her house to set her up with someone. She is furious with their party and retaliates by throwing out all the guess shoes. Soon Angel finds out his favorite pair of shoes was also thrown out, he leaves Athenas house for days in search of his lost shoes. Athena and Chow Chow get into an heated argument about Athenas actions causing Angel to leave, both realize Angels place in their heart and separately heads out in search of him. Athena finds an ecstatic Angel soaking wet in the pouring as he has found his missing shoes. They head back to Athenas home and have a heart felt conversation.

The next day Athena and Angel head out to somewhere in Europe to prepare for Angels return to heaven. Athena packs all of Angelos stuff for Angel to bring to Angelo in heaven. They visit an lavender field to pick up an lavender bouquet to also bring to Angelo since that was his favorite scent when he was alive. The two later take a train to head to the Holy Door location. While on the train Athena and Angel make love so Angel will have the strength to fly back to heaven. Both snuggle each other after their intimate day waiting for the Holy Door to open, once it opens Angel flies out the train window and return to heaven. Parting with Angel helps Athena to finally have closure of her relationship with Angelo. 

== Cast ==
*Takeshi Kaneshiro as Angel
*Kelly Chen as Athena Chen
*Eason Chan as Chow Chow
*Cheng Pei-pei as Madame Tung
*Michael Clements as Michael
*Terence Yin as angel playing violin
*Vincent Kok as angel in aroma shop
*Alice Tam as Athenas aroma student
*Audrey Mak as Athenas aroma student
*Poon An-Ying as garbage lady
*Fo Lin as balloon seller
*Ho Chung-Wai as Stella Bar Bartender
*Kung Siu-Ling as Cat
*Vanilla Hung as May
*Howard Cheung as Lion
*Telly Liu as Tiger
*Peter Yip as SDU at party
*Roderick Lam as SDU at party
*Vincent Chik as SDU at party
*Tsang Tso-Choi as Beggar angel 

== Production crew ==
*Production Manager: Pang Yau-Fong, Cheung Chin, Dennis Chan Yiu-Wa
*Sound Recording: Teko Leung Chung-Wai
*Art Director: Yee Chung-Man, David Poon Chi-Wai, Pater Wong Bing-Yiu
*Script Supervisor: Kenneth Yuen Kam-Lun
*Lighting: Wong Wai-Chuen
*Makeup: Kwan Lee-Na
*Props: Wong Sai-Kit, Jack Wong Wai-Leung, Bo Wai-Tong, Chan Chun-Ting
*Costume Designer: Elsa Chan Ki-Ling, Dora Ng Lei-Lo, Pater Wong
*Hair Stylist: Samuel Wong Kwok-Hung
*Presenter: Raymond Chow Man-Wai
*Sound Editor: Kinson Tsang King-Cheung, Yuen Teng-Yip
*Assistant Director: Thomas Chow Wai-Kwan, Jo Chan Yuk-Lun

==Awards==
{| class="wikitable"
! style="width:50px;"| Year
! style="width:240px;"| Ceremony
! style="width:400px;"| Category
! style="width:60px;"| Result
|- 2001
|align="center" rowspan=6| Hong Kong Film Award
|align="center"| Best Supporting Actor  Eason Chan 
|  
|- Best Cinematography  Kwan Pun-Leung 
|  
|- Best Art Direction  Hai Cheung Man, Poon Chi-Wai, Wong Bing-Yiu 
|  
|- Best Costume Design  Dora Ng 
|  
|- Best Original Film Score  Ronald Ng 
|  
|- Best Original Film Song  "Lavender" - Music: Ronald Ng, Lyrics: Lam Jik, Performed by: Kelly Chen 
|  
|}

==References==
 

== External links ==
* 
*http://www.lovehkfilm.com/reviews/lavender.htm
*http://hkmdb.com/db/movies/view.mhtml?id=8859&complete_credits=1&display_set=eng

 
 
 