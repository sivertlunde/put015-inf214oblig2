A Wicked Woman
 
{{Infobox film
| name           = A Wicked Woman
| image          = 
| image_size     = 
| caption        = 
| director       = Charles Brabin
| producer       = Harry Rapf
| writer         = Florence Ryerson Zelda Sears
| story          = 
| based on       =   
| narrator       = 
| starring       = Mady Christians Jean Parker Charles Bickford
| music          = 
| cinematography = 
| editing        = 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = $378,000  . 
| gross          = $333,000 
}}
A Wicked Woman is a 1934 drama film starring Mady Christians as a woman who kills her abusive husband to protect her family and builds a new life to raise their four children. It was directed by Charles Brabin and also starred Jean Parker and Charles Bickford. It was based on the novel Wicked Woman by Anne Austin.

==Cast==
*Mady Christians as Naomi Trice, aka Naomi Stroud
*Jean Parker as Rosanne
*Charles Bickford as Naylor, the man who wins Naomis love
*Betty Furness as Yancey William Henry as Curtis
*Jackie Searl as Curtis as a Child (as Jackie Searle)
*Betty Jane Graham as Yancey as a Child Marilyn Harris as Rosanne as a Child
*Paul Harvey as Ed Trice
*Zelda Sears as Gram Teague Robert Taylor as Bill Renton
*Sterling Holloway as Peter
*Georgie Billings as Neddie
*DeWitt Jennings as The Sheriff

==Box Office==
According to MGM records the film earned $206,000 in the US and Canada and $127,000 elsewhere resulting in a loss of $181,000. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 