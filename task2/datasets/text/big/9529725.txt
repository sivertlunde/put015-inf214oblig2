The Beast of Hollow Mountain
{{Infobox Film
| name           = The Beast of Hollow Mountain
| image          = 
| image_size     = 
| caption        = 
| director       = Edward Nassour Ismael Rodríguez	 	
| producer       = Edward Nassour William Nassour
| writer         = Willis OBrien (as El Toro Estrella) Robert Hill Jack DeWitt (dialogue) 
| narrator       = 
| starring       = Guy Madison Patricia Medina Carlos Rivas Mario Navarro
| music          = Raúl Lavista
| cinematography = Jorge Stahl Jr.
| editing        = Holbrook N. Todd Maury Wright Fernando Martínez
| distributor    = United Artists
| released       = 1956 
| runtime        = 81 min.
| country        =   USA
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 American cowboy living in Mexico who discovers his missing cattle are being preyed upon by an Allosaurus. The Allosaurus would later attack local villagers in a town, and eventually be destroyed by getting lured into some quicksand and drowning.

The first film to show dinosaurs and cowboys in the same picture, it is notable for being based on a story idea by special effects innovator Willis OBrien. It used a form of stop motion called replacement animation to bring the dinosaur to life.  OBrien co-wrote the script under the pseudonym El Toro Estrella; OBrien was also to have originally done the special effects for this movie, but this did not happen for reasons unknown.

==Plot==
 
In southern Mexico at the turn of the 20th century, tales are told of cattle and farmers mysteriously disappearing. These events occur at a location called "Hollow Mountain" where a curse is supposed to be residing. The mountain has never been explored and the swamp at its base is said to claim the lives of anyone foolish enough to go to its banks. In spite of these tales and possible perils, American cowboy Jimmy Ryan leads three cowboys into the area in search of lost cattle. When they arrive they find mysterious tracks and believe the curse from Hollow Mountain is responsible. Whilst trying to track the curse down one of them falls into a tar pit at the base of the swamp and nearly drowns, but is rescued.

Back in town, Ryan meets a Mexican boy, Panchito, and his father Pancho, who own a large ranch out in the country, not far from Hollow Mountain. As the two are leaving to check on the cattle, a gang of young bullies throw firecrackers at them, causing Pancho to fall off his horse and get dragged across the ground. Ryan notices this happen and stops the horse saving Pancho. Afterwards he begins falling in love with the beautiful Sarita, who had also stopped to help Pancho. Ryan and Sarita head to the cafe of Don Pedro, an old Mexican who Ryan talks to about the disappearing cattle. Ryan says that it is becoming clear that cows dont jump into swamps and because the swamp is shrinking it is losing its deadliness. While the two are talking, Enrique, a tough old Mexican, comes by. Enrique does not want Ryan to ranch his cattle here because he says Ryan should just ranch in Texas where he came from. But since the selling prices are cheaper here ranching has become less difficult. The two almost fight but Don breaks it up.

Meanwhile, Ryan and Felipe read a note from Sarita saying she is out at the ranch checking on the cattle. Ryan and Felipe hire Pancho and Panchito, and together they head out to the ranch. At the ranch Sarita is disappointed to hear that the Panchos have abandoned life in her cottage and have come to work for Jimmy instead. She tries to persuade them to leave, but they both refuse. Panchito says that he will take over for his fathers work so he does not have to live in the small cottage sat up near the farm. Since Panchito is very young, Sarita worries about him, and the two also tell that the former people who have worked here have disappeared, just like the missing cattle.

Sarita goes to the top of Hollow Mountain to see Ryan again, and the two share a romantic moment together. Ryan becomes jealous when Sarita says she will me married to Enrique in two weeks and is surprised to hear from Sarita that Enrique is actually quite nice, just a bit rough when you first meet him. When she leaves to go home, she finds her horse is missing. Reluctantly she agrees to ride Ryans horse with him and he carries her back to town.

When they arrive, Enrique spots them. Enraged at seeing Jimmy hanging out with his girlfriend, he attacks Ryan and a street fight ensues. Eventually Ryan wins and is able to overpower Enrique, but is scolded by Don, who does not want street fights in "his" town. Don tells Ryan that Enrique wants to buy Ryans ranch. But Ryan, who is determined to stand his ground no matter what, refuses. Don says that friction will continue between the two if Ryan does not give up the ranch before a new shipment of cattle comes, but Don is forced to retreat when Ryan still refuses.

A few days before the wedding of Enrique and Sarita, Enrique still fumes about Ryan and what he did with her. Sarita tries to calm him down but he still complains. Although Enrique adds that it was mainly just because he loves Sarita. Sarita pleads for him to become friends with Ryan but obviously Enrique refuses. Back at Hollow Mountain, Ryan and Felipe lead the Panchos to their cottage, whose former owner mysteriously disappeared. While Panchito guards the horses, the three men head out and find the body of another missing cow. Pancho wants to explore the swamp but is stopped by Ryan, who feels it is too unsafe for him. In town Ryan stops to say hi to Sarita, who apologizes for her rude attitude about Ryan in their last encounter. Yet again, Enrique spots them and cusses about Ryan again. But this time he has anew plan in store: he sends out two henchmen to attempt to steal some cattle while Ryan and Felipe are away.

Meanwhile, Ryan gets mad when the manager of the town says that a new shipment of cattle cannot come. The manager says that if more cattle come, there will be more people trying to convert his property. Ryan agrees to this and leaves. As he leaves, he finds two people with Felipe. Felipe has been tricked into thinking the two spies sent by Enrique were actually allies planning to help, so they (bad idea) take the two out to the ranch, watched by Enrique. Later Ryan visits Panchos house and while feeding a calf gets a letter from Sarita to meet her at the graveyard.

At the ranch, Pancho asks Panchito to wait for him at the cottage while he goes to the swamp to look for the lost cattle. If his father is not back by dusk, Panchito will go tell Ryan about what happened and then they will look for Pancho. Panchito, aware of the curse said to exist in the area, pleads for his dad to stay there, but he says everything will be all right and then leaves. At the graveyard, Sarita is relieved to see that Ryan has seen her message. However, as they talk, Ryan gets mad when Sarita asks him why has he not given up his ranch up to Enrique. She then says that she really wants hostilities to end between Jimmy and Enrique, so that is why she is asking him to move, although secretly she does like him. Meanwhile, Pancho is still walking through the swamp. He hears a huge roar, and a prehistoric creature from the dawn of time, the Beast of Hollow Mountain, makes its first appearance, eating him alive.

Ryan and Felipe begin to worry now that the Panchos have still not returned home. Felipe is stunned to find out that Ryan says he will be leaving tomorrow and leaving Felipe in charge of the ranch. Ryan also says that Enrique will finally have a place to raise his cattle without any further conflict between him and Ryan. Suddenly, Panchito come to their door, screaming and crying, saying that his father has not returned. The three go into the swamp to look for him but only find his sombrero hat, which he had taken off earlier. They figure that quicksand was not the cause for Panchos death. Deciding not to show his hat to Panchito, they call to Pancho but with no success. Panchito, grief-stricken, tries multiple times to go after his father in the swamp, but is stopped by Ryan and Felipe. Meanwhile a festival is going on in town, and women are busy gathering food and putting up streamers and displays. But Panchito, heartbroken, will not eat anything. Ryan talks to Don about Panchos death. Don says that Panchito can be cared for in a foster home he has prepared for him. But grief-stricken Panchito is so sad he says he will not be friends with Ryan again. Ryan again meets Sarita and tells both her and Don that he will be moving himself and his cattle today, leaving the land for Enrique. He says bye to Sarita and then leaves town.

The festival is well underway, with dancers and firecrackers entertaining crowds. Enrique is delighted to find out that Ryan is leaving and now devises his plan to stampede the cattle away from the station and make them end up as Enriques. The men decide to laze around for some time before stampeding the cattle and lie down and drink water together. Meanwhile, as Sarita prepares to wed Enrique, Panchito decides once and for all to go to the swamp after his father. Margarita, the assistant of Don Pedro, tries to stop him, but he gets away. Margarita tells Sarita about Panchitos departure and she goes out to stop him.

At the ranch, the Beast from Hallow Mountain makes its first full appearance and kills one of the cattle, forcing the others into a stampede. The stampede takes the two men off guard and they are trampled to death in the process. The cattle race toward the village where the festival is taking place. Ryan and Felipe hear them coming and race to stop them. Enrique and his men become aware of the cattle as well, but their efforts to stop them are futile and the cattle continue toward the village, just missing Panchito, who continues after his father. The cattle stampede into the village, causing much panic and disrupting the festival; the cowboys continue to try to stop them but with no success. Don notices the stampede and blames it on Enrique, who in turn gets mad at one of his men whom he had told to stampede (but not toward the village). Just then, Margarita tells them that Ryan and Sarita have gone after Panchito, who had run away. Enrique and his men decide to follow Ryan and Sarita who says she will not marry Enrique until this is all over.

While Panchito is in the swamp, searching for his dad, he is attacked by the Beast, which chases him across a river and to the small cottage, where Sarita greets him and the two decide to hide in the cottage. The Beast arrives at the cottage and manages to break in. As it feels around for the two, Sarita keeps it away be stabbing at it with a long pole. But the Beast knocks it out of her hand and continues to attack them. But then Ryan arrives and distracts it with his gun, causing it to lose interest in Panchito and Sarita. He orders Sarita and Pancho to get out of the cottage and while they flee to Panchitos horse Ryan continues to distract the Beast and leads it up a mountain. While he is dealing with the creature, Enrique comes back and attempts to kill Ryan, but the sight of the Beast causes his horse to buck and throw him off. The Beast chases Enrique across a swamp and onto a plain, where Ryan grabs him and the two flee on Ryans horse. They soon come to a steep slope, and are forced to slide down on their horse and are thrown off at the bottom. The Beast follows them down.

Ryan and Enrique flee to a small cave with the Beast in hot pursuit. At the cave entrance, the Beast reaches into the opening, trying to reach them. Sarita meanwhile rounds up Don and other cowboys to come to Ryan and Enriques aid. The Beast continues to reach in, and despite the stabbing of Ryans knife it manages to reach Enrique and pull him out of the cave. The Beast kills Enrique and then turns on Ryan, who is only saved when Sarita and the other cowboys fire at the Beast and distract it. While the Beast chases them, Ryan and Felipe head over to the tar pit and are found by the Beast. When the Beast arrives, Ryan grabs a rope and first whacks the Beasts nose with it. Then, after throwing his knife into the Beasts nose, he throws the lasso around a tree branch, hoists himself upwards on the rope, and begins to swing back and forth, barely out of the Beasts reach. Taunted by this so-close stunt, the Beast walks forward a few steps and gets its feet caught in the tar. It roars helplessly as it begins to sink down into the tar while Ryan is reunited with Sarita, Panchito, Felipe, and the others. Sarita weeps and the others, including Ryan, look on sadly as the Beast, roaring in agony, dies in the black, sticky tar pit. They stare at the pit for a few seconds and then walk slowly toward their horses.

==Cast==
*Guy Madison as Jimmy Ryan
*Patricia Medina as Sarita Carlos Rivas as Felipe Sanchez Eduardo Noriega as Enrique Rios
*Julio Villarreal as Don Pedro
*Mario Navarro as Panchito
*Pascual Garcia Pena as Pancho
*Lupe Carriles as Margarita

==Production==
The Willis OBrien story idea that inspired the basis for his script for this film also inspired his earlier unproduced script "The Valley of the Mist" which would later be made as The Valley of Gwangi by OBriens protégé Ray Harryhausen.

While the bulk of the stop motion work was done with replacement animation the close ups of the dinosaur were filmed using an articulated puppet.

This was the first film with stop motion effects shot in Wide Screen and color.

==See also==
*Weird West
*The Valley of Gwangi

==References==
 
DVD Savant Review by Glenn Erickson, The Lost World (1925)  March 31, 2001  

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 