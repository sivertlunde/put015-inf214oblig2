Grandad Rudd
 
{{Infobox film
| name           = Grandad Rudd
| image          = Grandad_Rudd.jpg
| image_size     = 
| caption        = 
| director       = Ken G. Hall
| producer       = Ken G. Hall Vic Roberts   George D. Parker
| based on     = play by Steele Rudd stories Grandpas Selection and Our New Selection by Steele Rudd
| narrator       = 
| starring       = Bert Bailey  Fred MacDonald
| music          =  George Heath
| editing        = William Shepherd
| studio         = Cinesound Productions
| released       = February 1935
| runtime        = 90 minutes
| country        = Australia
| language       = English
| budget         = £8,000 "Counting the Cash in Australian Films", Everyones 12 December 1934 p 19-20     or ₤15,000 
| gross          = £18,000 
}} On Our Selection, and was later followed by Dad and Dave Come to Town and Dad Rudd, MP.

==Plot==
The movies plot is similar to that of the play: Dad Rudd (Bert Bailey) has become a successful father but is very tight with his money and oppresses his sons Dave (Fred MacDonald), Joe (William McGowan) and Dan (George Lloyd). The sons eventually stand up to their father and manage to persuade him to give them a wage increase – but he increases their rent by an equal amount.

As in the play, there is a serious subplot about Dads grandchild Betty (Elaine Hamill) who becomes engaged to a corrupt neighbour, Henry Cook (John D’Arcy), despite the true love of another farmer, Tom Dalley (John Cameron). The  climax involves a comic cricket game involving the Rudds.

==Cast==
 
* Bert Bailey as Dad Rudd
* Fred MacDonald as Dave Rudd
* George Lloyd as Dan
* William McGowan as Joe
* Kathleen Hamilton as Madge
* Lilias Adeson as Lil
* Les Warton as Regan
* Elaine Hamill as Betty
* John Cameron as Tom
* John DArcy as Henry Cook
* Molly Raynor as Amelia Banks
* Bill Stewart as Banks
* Marie DAlton as Mrs. Banks
* Marguerite Adele as Shirley Sanderson
* George Blackwood as School-Master
* Ambrose Foster as Young Dave
* Peggy Yeoman as Mum Rudd
 

==Original play==
{{Infobox play
| name       = Grandad Rudd
| image      = 
| image_size = 
| caption    = 
| writer     = Steele Rudd
| characters = 
| setting    = 
| premiere   = 22 September 1917
| place      = 
| orig_lang  = English
| subject    = 
| genre      = comedy
}}
The play Grandad Rudd was first produced in 1917, being based on the stories Grandpas Selection and Our New Selection.

===Plot=== On Our Selection: Dad has become a prosperous farmer and member of Parliament, while Dave has married Lily and become a father. Dad tries to bully Dave and his other son Joe (who has also married), but their wives encourage them to rebel against their father.

There were subplots involving a love triangle between Dads granddaughter Nell, handsome Tom Dalley, who has invented a potato harvester, and unscrupulous produce agent Henry Cook; the return of a prodigal son, Dan Rudd, keen to claim Dads estate, and his romance with Amelia Banks; and their neighbours, Mrs Regan and the Banks family. 

===Original Production===
The original production was presented by Bert Bailey and Julius Grant, and saw Bert Bailey and Fred MacDonald repeat their stage roles as Dad and Dave respectively. Making its debut on 22 September 1917, it ran for seven weeks in that city, only ending because the theatre had to vacate for another production. It then toured around the country over the next few years, although it was never as successful as On Our Selection. 

==Production==

===Development=== On Our Selection (1932) saw Cinesound announce plans to make Grandad Rudd as a follow up almost immediately, but Steele Rudd issued a statement claiming that since he wrote the play, no movie could be made without his permission.  For a time there was talk the second Dad Rudd film would be Rudds New Selection, but this did not eventuate. 
 Vic Roberts and George D. Parker.

Although Grandad Rudds production had been planned prior to making Strike Me Lucky (1934), its importance to Cinesound grew when that earlier film failed at the box office and the new studio needed a hit.

===Shooting===
Shooting took place over five weeks.  On this and the other two Dad Rudd sequels, Cinesound paid Bert Bailey £150 a week plus 25% of the profits. 

==Reception==
Ken G Hall later said the film was successful "but it was not in the On Our Selection class as a money-spinner".  According to Bert Baileys obituary, the star thought this drop was caused in part by him playing the role with a clean shaven top lip. "The slight change took him out of character." 

The film was released in England under the title of Ruling the Roost. 

Cinesound originally intended to follow this movie with a version of Robbery Under Arms but decided not to proceed because of uncertainty arising from a ban the NSW government had on films about bushrangers.  The company ended up ceasing production for several months in 1935 to enable Hall to travel to Hollywood and research production methods.

==References==
 

==External links==
*  in the Internet Movie Database
*  at Australian Screen Online
*  at Oz Movies
*  at National Archives of Australia
 
 

 
 
 
 
 