The God Who Wasn't There
{{Infobox film
| name=The God Who Wasnt There
| image=TGWWT.jpg
| director=Brian Flemming
| writer=Brian Flemming Sam Harris Richard Carrier Alan Dundes Barbara Mikkelson David P. Mikkelson Robert M. Price Scott Butcher Ronald Sipus
| producer=Brian Flemming Amanda Jackson
| distributor=Beyond Belief Media Microcinema International
| released= 
| runtime=62 mins  English
}}

  independent documentary documentary written existence of Jesus, examining evidence that supports the Christ myth theory against the existence of a historical Jesus, as well as other aspects of Christianity.    

==Overview==
===Christ myth theory===
Most of the film is a presentation of the argument for the Christ myth theory. Flemming and those he interviews in the film make these claims:
* The history of Christianity, especially the doctrine of the earliest Christians, is consistent with Jesus having been a mythical character, with historical details only added on later. resurrection and ascension and presents them as having occurred in a mythic realm rather than an earthly one. 
* The death-resurrection-ascension sequence was common in previous mythologies and religions, making it more likely that the Jesus character was inspired by his similar forebears than that he actually lived on Earth. 
* Other details of the Jesus biography offered in the Gospels also have precedent in previous mythologies and religions, especially Judaism. For example, the Massacre of the Innocents appears to be directly inspired by a nearly identical story in the Book of Exodus.

===Other criticisms of Christianity===
Besides defending the Jesus myth hypothesis, the film criticizes some other aspects of Christianity: Savior of Christian doctrine and must therefore be regarded by Christians as damned. similar passages damning anyone who doubts the existence of the Holy Spirit. He is appalled by the notion that Jesus will forgive murder, theft, and any other sin but not this type of disbelief. 
* Because Jesus knows peoples innermost thoughts, and that therefore one must police ones thoughts to avoid any doubt, Flemming summarizes this idea with the statement that the greatest sin in fundamentalist Christianity is "to think."
* Flemming asserts that Christians have historically been obsessed with blood sacrifice, and illustrates this viewpoint by pointing out that Mel Gibsons 2004 film The Passion of the Christ, which contains very few scenes that do not feature graphic violence or suffering, was more financially successful than any previous film about Jesus.  that Jesus will come back to Earth in their lifetime, and that this sort of thinking is not conducive to long-term governmental policies.

==Interviews and commentary==
Several notable personalities make appearances in the documentary:
* Scott Butcher is the creator of the Rapture Letters.com website.
* Richard Carrier is an atheist activist and more recently, scholarly writer on the Christ Myth Theory,  who holds a PhD in ancient history from Columbia University, New York. 
* Alan Dundes was an anthropologist and folklorist. Until his death shortly after being interviewed for the documentary, he was Professor of Folklore and Anthropology at the University of California, Berkeley.  Sam Harris is a researcher into the neurobiology of religious belief, and author of The End of Faith, Letter to a Christian Nation, and The Moral Landscape.
* Barbara Mikkelson and David P. Mikkelson are the founders of Snopes.com. Center for Inquiry Institute.  , Secular Student Alliance, accessed April 15, 2010. 
* Ronald Sipus, principal of Village Christian School, which Brian Flemming attended as a youth.  Sipus walked out of the interview, accusing Flemming of misrepresenting himself and his reasons for the interview.

The following only appear on the DVDs commentary track: British Evolutionary evolutionary biologist and science writer. 
* Earl Doherty is a contemporary advocate of the Jesus Myth theory and author of The Jesus Puzzle.
* The Raving Theist is a lawyer and former atheist blogger, who has since converted to Christianity. 
 Rose Bowl in Pasadena, California|Pasadena, California on November 18, 2004.

==Blasphemy Challenge== Blasphemy Challenge, an Internet-based project encouraging atheists to declare themselves publicly. 

==See also==
* Jesus Camp
* Jesus Christ in comparative mythology
* Marjoe
* Religulous

==References==
 

==External links==
* 
* 
* 
* 
*  by Apologetics Coordinator at the North American Mission Board Michael R. Licona
**  to Liconas Review
*** 
*  thegodwhowasntthere.com. Retrieved November 17, 2011

 

 
 
 
 
 
 