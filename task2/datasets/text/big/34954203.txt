Le Rêve de Rico
 

{{Infobox film
| name           = Le Rêve de Rico 
| image          = 
| caption        = 
| director       = Selven Naïdu
| producer       = La Huit Production
| writer         = 
| starring       = Aljojah Izoo, Sharon Pillay Valoo, Marie-Thérèse Irana, Annie Izoo, Jacelyn Irana, Berty Maclou
| distributor    = 
| released       = 2001
| runtime        = 22
| country        = France Mauritius
| language       = 
| budget         = 
| gross          = 
| screenplay     = Olivier Lorelle, Selven Naïdu (basado en una novela de Jean-Claude Larrieu)
| cinematography = Pierre-Olivier Larrieu
| sound          = Marc-André Relave
| editing        = Selven Naïdu, Michel Vuillermet
| music          = Selven Naïdu
}}

Le Rêve de Rico  is a 2001 film.

== Synopsis ==
Rico, 11, wakes up early to study for an exam. Today he has to pass the Certificate of Primary Education|CPE, the exam that, in Mauritius, will get him into High school. He gets on the bus, but unfortunately, it breaks down. So he grabs a ride on a truck, but a fire brings traffic to a standstill. A neighbor offers to take him in his car. He’s on his way, but fate has other plans…

== References ==
 

 
 
 
 


 