Kippur
 
{{Infobox film
| name           = Kippur
| image          = Kippur_film.jpg
| caption        = DVD release cover
| director       = Amos Gitai
| producer       = Amos Gitai Michel Propper Laurent Truchot
| writer         = Amos Gitai Marie-Jose Sanselme
| starring       = Liron Levo Tomer Russo Uri Klauzner Yoram Hattab Guy Amir
| music          = Jan Garbarek
| cinematography = Renato Berta
| editing        = Monica Coleman
| studio         = Canal+ Agav Hafakot Kino International
| released       = October 5, 2000 (Israel) November 3, 2000 (U.S.)                                             
| runtime        = 123 minutes 
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = $114,283   
| preceded_by    = 
| followed_by    = 
}} 

Kippur (כיפור) is a 2000 Israeli drama war film directed by Amos Gitai. The storyline was conceived from a screenplay written by Gitai and Marie-Jose Sanselme; based on Gitais own experiences as a member of a helicopter rescue crew during the 1973 Yom Kippur War. The film stars actors Liron Levo, Tomer Russo and Uri Klauzner in principal roles. 
 Kino International theatrically, and by Kino Video for home media. Following its cinematic release, the film was entered into the 2000 Cannes Film Festival among other awards selections.    Kippur explores war, politics, and human rescue.

Kippur premiered in theaters nationwide in Israel on October 5, 2000. The film was screened through limited release in the United States on November 3, 2000 grossing $114,283 in domestic ticket receipts. In the U.S., Kippur was at its widest release showing in 5 theaters nationwide. It was generally met with positive critical reviews before its initial screening in cinemas.

==Plot==
It is October 6, 1973, and Egypt along with Syria have continued their undeclared war on Israel by launching attacks in the Sinai Peninsula and Golan Heights. Weinraub (Liron Levo) and his friend Ruso (Tomer Ruso) are Israeli citizens who are called up through a military draft to fight in the surprise conflict. The two make their way to the Golan Heights to locate their reserve unit which they served under during their military training. However, during the chaotic circumstances, they never find it, and end up sleeping by the side of the road. 

The next morning, they are awakened by Dr. Klauzner (Uri Klauzner), who asks for a ride to Ramat David where he serves on an Air Force base. After transporting Dr. Klauzner to the base, Weinraub and Ruso agree to volunteer with a first-aid rescue team. Their ongoing mission involves evacuating dead and wounded soldiers from the battlefield. Later on October 10, their helicopter crew is deployed to Syria for a covert operation. During their mission, the helicopter is struck by a missile, killing one of the co-pilots and injuring everyone on board. Weinraub and Ruso are among those who survive, and are picked by another rescue helicopter. They become patients at a field hospital, thus ending their role in the war.

==Cast==
 
{| cellpadding="0" cellspacing="0"
| Liron Levo
| &nbsp;... Weinraub
|-
| Tomer Russo
| &nbsp;... Russo
|-
| Uri Klauzner
| &nbsp;... Dr. Klauzner
|-
| Yoram Hattab
| &nbsp;... Pilot
|-
| Juliano Mer-Khamis
| &nbsp;... Officer
|-
| Guy Amir
| &nbsp;... Gadassi
|-
| Ran Kauchinsky
| &nbsp;... Shlomo
|-
| Kobi Livne
| &nbsp;... Kobi
|-
| Liat Levo
| &nbsp;... Dina
|-
| Pini Mittleman
| &nbsp;... Hospital Doctor
|-
|}
 

==Production==
===Development===
 security and intelligence bodies, and the political branch of government.

The emotional impact on the individual Israeli soldiers is expanded upon in the film.  .  The complete transformation from a quiet civilian life to a chaotic war scene is depicted in the storyline. The Israeli soldiers cope with assisting dead and seriously wounded troops, while taking enemy fire. 

===Filming===
 
The film is largely autobiographical, based on Gitais own experiences as a member of a helicopter rescue crew during the war.     Scenes were shot with the assistance of the Israeli Defense Forces which provided much of the military equipment used in the film. Most of the characters are named after the actors who play them, with the exception of the title character, who is given only the last name Weinraub, which was Amos Gitais family name until his father changed it to the Hebrew name Gitai. 

The helicopter crash that ends the film actually happened.  Gitais helicopter was shot down by a Syrian missile on his 23rd birthday. The co-pilot was killed and several others wounded.  Gitai reportedly considered it the pivotal moment of his life. 

===Music=== score for the film was originally composed by musician Jan Garbarek.    The sound effects in the film were supervised by Alex Claude.  The mixing of the sound effects were orchestrated by Philippe Amouroux and Cyril Holtz while being supervised by Eli Yarkoni. 

==Reception==
===Critical response=== weighted average Toronto and Cannes film festivals   and received a nomination for the Peace Award from the Political Film Society. 

Kevin Thomas, writing in the  . Retrieved 2010-10-17. 
In The Village Voice, critic J. Hoberman reserved compliment for the lead acting and directing saying, "Gitais strategy encourages the viewer to ponder the logistics of war—as well as those of filming war." He noted though, that the "ensemble acting sometimes falters, and due to Gitais camera placement, it can be difficult to distinguish between the various characters—although Klauzner establishes an indelible identity in a brief moment of downtime when he discusses his childhood in Europe during World War II." 

{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; color:black; width:40em; max-width: 35%;" cellspacing="5"
|style="text-align: left;"|"Closely based on Gitais own combat experience during the Yom Kippur War and filmed with the utmost attention to detail, this mission is the movie—as well as the most radical narrative filmmaking of Gitais career."
|-
|style="text-align: left;"|—J. Hoberman, writing in The Village Voice Hoberman, J. (October 31, 2000).  . The Village Voice. Retrieved 2010-10-17. 
|} NY Daily News, believed that instead of "heightening our sense of empathy, we become numbed by the repetition" of the film.  

 
David Sterritt of   viewed Kippur as a "radically different – more nakedly autobiographical, more naturalistic, more forgiving – from Gitais highly conceptual and stylized body of work, there are clear thematic continuities."  Left unimpressed though, was critic Michael Rechtshaffen of The Hollywood Reporter who wrote that the film was "A patience-trying docudrama almost completely devoid of any trace of narrative structure or even defined characters."  Critic Leonard Maltin referred to the film as being "unique" and "a painstaking, grueling picture of war." 

===Box office===
The film premiered in cinemas on November 3, 2000 in limited release throughout the U.S.. During its opening weekend, the film opened in a distant 66th place grossing $17,007 in business showing at 5 locations.    The film Charlies Angels (film)|Charlies Angels soundily beat its competition during that weekend opening in first place with $40,128,550.  The films revenue dropped by 29% in its second week of release, earning $11,981.    For that particular weekend, the film fell to 71st place screening in 4 theaters but not challenging a top fifty position. The film Charlies Angels, remained in first place grossing $24,606,860 in box office revenue.  In its final limited weekend showing in theaters, the film ended up in 99th place grossing $1,978.  The film went on to top out domestically at $114,283 in total ticket sales through an 10-week theatrical run.   For 2000 as a whole, the film would cumulatively rank at a box office performance position of 303. 

===Home media=== Region 1 DVD in the United States on August 28, 2001. Special features for the DVD include; Letterbox 1.85 screen format, stereo audio in Hebrew with English subtitles, and interactive menus with scene access.  Currently, there is no scheduled release date set for a future Blu-ray Disc version of the film.

==See also==
 
*2000 in Israeli film
 

==Bibliography==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

==References==
 

==External links==
*  
*   at The Films of Amos Gitai
*  
*  
*   at Rotten Tomatoes
*   at Metacritic
*   at Box Office Mojo

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 