Aarathi (film)
{{Infobox film 
| name           = Aarathi
| image          =
| caption        =
| director       = P Chandrakumar
| producer       = Soorya Narayanan Potti John Paul John Paul Seema
| music          = MB Sreenivasan
| cinematography = Anandakkuttan
| editing        = G Venkittaraman
| studio         = Soorisini Creations
| distributor    = Soorisini Creations
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, Seema in lead roles. The film had musical score by MB Sreenivasan.   

==Cast==
*Sukumaran
*Mala Aravindan
*Ravi Menon Seema
*Vincent Vincent

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by Sathyan Anthikkad. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Hridaya vaathaayanangal || K. J. Yesudas || Sathyan Anthikkad || 
|-
| 2 || Kaumaara Swapnangal || S Janaki || Sathyan Anthikkad || 
|-
| 3 || Koumaara Swapnangal   || S Janaki || Sathyan Anthikkad || 
|}

==References==
 

==External links==
*  

 
 
 

 