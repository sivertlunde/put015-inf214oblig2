My Daughter Joy
{{Infobox film
| name           = My Daughter Joy
| image          = 
| caption        = 
| director       = Gregory Ratoff 
| producer       = Gregory Ratoff
| writer         = 
| starring       = Edward G. Robinson 
| cinematography = 
| music          = 
| studio         = London Films
| distributor    = British Lion Films (UK) Columbia Pictures (US)
| released       = 21 August 1950
| runtime        = 
| country        = United Kingdom English
| gross = £106,399 (UK) 
}} British drama film directed by Gregory Ratoff and starring Edward G. Robinson, Peggy Cummins and Richard Greene.  A millionaire spoils his only daughter, but has a strained relationship with his wife.

==Cast==
* Edward G. Robinson ...  George Constantin
* Peggy Cummins ...  Georgette Constantin
* Richard Greene ...  Larry
* Nora Swinburne ...  Ava Constantin
* Walter Rilla ...  Andreas
* Finlay Currie ...  Sir Thomas McTavish
* James Robertson Justice ...  Prof. Keval Ronald Adam ...  Colonel Fogarty
* David Hutcheson ...  Annix
* Gregory Ratoff ...  Marcos
* Peter Illing ...  Sultan
* Harry Lane ...  Barboza
* Don Nehan ...  Polato
* Roberto Villa ...  Prince Alzar
* Ronald Ward ...  Dr. Schindler

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 


 