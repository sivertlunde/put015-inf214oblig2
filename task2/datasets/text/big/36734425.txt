Dishonorable Disclosures
{{Infobox film
| name           = Dishonorable Disclosures
| image          = Dishonorable_Disclosures_promo.jpg
| alt            = 
| caption        = 
| director       =  
| producer       =  
| writer         =  
| based on       = 
| starring       = 
| music          = 
| cinematography =  
| editing        =  
| studio         =  OPSEC
| released       =  
| runtime        = 22 minutes United States
| language       = English
| budget         = 
| gross          = 
}} American documentary President Barack Obama has endangered lives by allegedly leaking information about the Death of Osama bin Laden|U.S. military raid that killed Osama bin Laden in 2011.   
 Special Operations President Obama 2012 presidential Swift Boat" attacks against Democratic presidential candidate John Kerry in 2004. 

==Summary==
The 22-minute film alleges President Obama and his administration intentionally leaked sensitive details about covert intelligence operations.     According to a   involved, along with other sensitive information. 
 USMC Lt. Col. Bill Cowan alleges that Obama divulged the covert information to Hollywood for political gain, saying in the video "When we divulge national security information such as the identity of the organization that killed Osama bin Laden, we have now put all of those men, all of their families, everybody around them at some sort of risk."   The film also alleges classified information was leaked about the Stuxnet virus attack on the Iranian nuclear program and Joint Prioritized Effects List|Obama’s “kill list” of suspected terrorists.   

==Responses to Dishonorable Disclosures== OPSEC described Tea Party groups.  Specifically, Shane wrote that the films featured former SEAL members include one whose Facebook page identifies him as a spokesman for the Tea Party Express and several Republican campaigns, and that OPSEC’s president ran unsuccessfully as a Republican candidate for Congress in 2010.  Shane asserted that the film, "in an effort to portray Mr. Obama as a braggart taking credit for the accomplishments of special forces and intelligence operatives,"  edited out Obamas crediting the "tireless and heroic work of our military and our counterterrorism professionals"  from his announcement of bin Ladens killing; in this regard Shane quoted Admiral William H. McRaven as having said that Obama "shouldered the burden" for this operation, "made the hard decisions,"  and was "instrumental in the planning process."  Shane further wrote that OPSECs president acknowledged Republican ties of some members but said that “as many or more (of the films participants) are apolitical. ... This issue is more than just politics.” 

  in July 2011 after Afridi was arrested by the Pakistani intelligence service.  Bergen also wrote that it was entirely Obamas decision, made against the advice of both the vice president and secretary of defense, to launch the raid based on fragmentary intelligence that bin Laden might be there.  Bergen asserted that the United States use of drones in Pakistan "is one of the worlds worst kept secrets," that disclosure of the Stuxnet virus attacks on the Iranian nuclear program had been reported since 2010, and that Iran publicly acknowledged the cyberattack two years earlier. 

Although Obamas speech did not mention SEAL Team 6 by name, Vice President Joe Bidens speech on May 3, 2011 to the Atlantic Council Annual Awards Dinner did specifically congratulate the "Navy SEALs and what they did last Sunday." 
 Obama Campaign Democratic presidential Senator John Kerry in United States presidential election, 2004|2004.   A spokesperson for the campaign said, "No one in this group is in a position to speak with any authority on these issues and on what impact these leaks might have, and its clear theyve resorted to making things up for purely political reasons."   

==References==
 

==External links==
*  at the Internet Movie Database
*Special Operations OPSEC Education Fund Inc  
*Dishonorable Diclosures on YouTube  
*Official   "President Obama on Death of Osama bin Laden" showing the May 1, 2011 speech by President Obama, posted to YouTube channel "whitehouse" on May 1, 2011 ( ).
*Whitehouse.gov   of Barack Obamas speech of May 1, 2011 ( ) announcing the death of Osama bin Laden.