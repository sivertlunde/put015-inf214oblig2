Paradise Canyon
{{Infobox film  
| name          = Paradise Canyon
| image         = Bposter canyon.jpg
| caption       = Original theatrical poster
| director      = Carl L. Pierson
| writer        = {{plainlist|
*Lindsley Parsons Robert Emmett
}}
| starring      = John Wayne
| producer      = Paul Malvern Jerry Roberts
| cinematography= Archie Stout
| distributor   = Monogram Pictures Corporation
| released      =  
| runtime       = 52 minutes
| language      = English
}} Western film starring John Wayne, directed by Carl L. Pierson.  The film was Waynes final Monogram Pictures/Lone Star Production Western.

John Wyatt (John Wayne) is a government agent sent to smash a counterfeiting operation near the Mexican border. Joining Doc Carters (Earle Hodgins) medicine show, they arrive in the town where Curly Joe (Yakima Canutt), who once framed Carter, resides. Learning that Curly Joe is the counterfeiter, Wyatt goes after the man himself.

==Cast==
*John Wayne as John Wyatt
*Marion Burns as Linda Carter aka Princess Natasha
*Reed Howes as Henchman Red
*Earle Hodgins as Doc Carter
*Gino Corrado as Rurales Captain
*Yakima Canutt as Curly Joe Gale Gordon Clifford as Mike – singer, Texas Two
*Perry Murdock as Ike – singer, Texas Two

==Colorization==
In 2008, Legend Films colorized and renamed the film as Guns Along The Trail, for a DVD collection that features several other "Poverty Row" era John Wayne films.

==See also==
* John Wayne filmography

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 