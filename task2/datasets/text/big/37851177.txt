Truth (2013 film)
{{Infobox film
| name = Truth
| image = Truth_poster.jpg
| caption = 
| director = Rob Moretti
| producer = Ashley Ahn Sean Paul Lockhart 
| writer = Rob Moretti 
| editing = Cassandra McManus	
| music = Jonathan Bartz
| starring = Sean Paul Lockhart Rob Moretti  Blanche Baker
| released =  
| studio = Left of Center Entertainment
| runtime = 94 minutes
| distributor = Artsploitation Films 
| country = United States
| language = English
| budget = 
| gross = 
}}
Truth is a 2013 American psychological thriller film directed and written by Rob Moretti. It stars Sean Paul Lockhart, Blanche Baker, and Rob Moretti. The movie was filmed in  Englewood Cliffs, New Jersey and Montclair, New Jersey, United States.

==Plot==
After a chance encounter over the internet, Caleb (Sean Paul Lockhart), who suffers from borderline personality disorder, meets and falls head over heels for Jeremy (Rob Moretti), and soon the line between love and lies blur. Struggling to keep his past a secret, including his mentally ill mother, Caleb slowly succumbs to his darker side. A sudden turn of events finds Jeremy held captive, until Calebs quest for the truth is revealed.

==Cast==
*Sean Paul Lockhart	... Caleb Jacobs
*Rob Moretti ... Jeremy Dorian
*Blanche Baker ... Dr. Carter Moore 
*Suzanne Didonna ... Calebs mother
*Rebekah Aramini ... Leah
*Max Rhyser	... Young man in the cafe
*Philip Joseph McElroy ... Young Caleb
*John Van Steen ... Orderly

==Critical reception==
Truth garnered mixed to negative reviews. At Metacritic, the film scored a 23, based on 4 reviews.  

Inkoo Kang of Village Voice wrote "Truth is hammier than Easter brunch, but its depictions of rejection transfiguring into violence are always affecting and distressing." 

Frank Scheck of The Hollywood Reporter said, "Its Hitchcockian aspirations are sabotaged by a tendency towards lurid melodrama that is more laughable than chilling." 

Jay Weissberg of Variety (magazine)|Variety commented: "A low-budget potboiler with an overblown score not loud enough to drown out the hackneyed dialogue."

Jeannette Catsoulis of The New York Times wrote of the story, "Filled with sappy dialogue and screeching strings, Truth is a puerile excavation of secrets and sickness." 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 