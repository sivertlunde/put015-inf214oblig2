Mangammagari Manavadu
{{Infobox film
| name           = Mangammagari Manavadu
| image          = Mangamma Gari Manavadu.jpg
| image_size     =
| caption        =
| writer         = Ganesh Patro   Bharathi Raja
| screenplay     = Kodi Ramakrishna 
| producer       = S. Gopala Reddy
| director       = Kodi Ramakrishna
| starring       = Nandamuri Balakrishna Bhanumathi Ramakrishna Suhasini 
| music          = K. V. Mahadevan
| cinematography = D. Prasad Babu
| editing        = K. Satyam
| studio         = Bhargav Art Productions 
| distributor    =
| released       =  
| runtime        = 129 minutes
| country        = India Telugu
| budget         =
| gross          =
}}
Mangammagari Manavadu ( , drama film produced by S. Gopala Reddy on Bhargav Art Productions banner, directed by Kodi Ramakrishna. Starring Nandamuri Balakrishna, Bhanumathi Ramakrishna, Suhasini in the lead roles and music composed by K. V. Mahadevan. The film recorded as Industry Hit at box office. 

==Plot==
Veeranna (Balakrishna) is grandson of Mangamma (Bhanumathi). Anitha is her daughter and Gokina Rama Rao son-in-law. Malli (Suhasini) is their daughter and grand daughter of Mangamma. Mangamma does not like her son in law family resulting in frequent quarrels. But Veeranna and Malli love each other. Yeleswaram Ranga is big rythu of neighbouring village. He does not like Mangamma. Every year there used to be bull fights. Rama Rao bets in the race and promises to marry his daughter with the winner. The opponents cheat them and wins by controlling the bull. Rama Rao commits suicide. They attacked Balakrishna. He killed them and runs away from the village. Everyone thinks he is dead. Finally he returns from Military service with a girl he would like to marry. The reasons behind the twist is rest of the story.

==Cast==
* Nandamuri Balakrishna as Veeranna	
* Bhanumathi Ramakrishna as Mangamma
* Suhasini as Malli
* Gollapudi Maruthi Rao
* Gokina Rama Rao
* Raavi Kondala Rao
* Yeleswaram Ranga
* Balaji
* Anitha
* Y. Vijaya

==Soundtrack==
{{Infobox album
| Name        = Mangammagari Manavadu
| Tagline     = 
| Type        = film
| Artist      = K. V. Mahadevan
| Cover       = 
| Released    = 1984
| Recorded    = 
| Genre       = Soundtrack
| Length      = 21:08
| Label       = AVM Audio
| Producer    = K. V. Mahadevan
| Reviews     =
| Last album  = Janani Janmabhoomi   (1984)  
| This album  = Mangammagari Manavadu   (1984)
| Next album  = Maa Pallelo Gopaludu   (1985)
}}

Music composed by K. V. Mahadevan. Lyrics written by C. Narayana Reddy All songs are blockbusters. Music released on AVM Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !! Singers !! length
|- 1
|Danchave Menatta Koothura SP Balu, P. Susheela
|3:31
|- 2
|Chandurudu Ninnu Choosi SP Balu,P. Susheela
|4:07
|- 3
|Gumma Choopu SP Balu,P. Susheela
|4:51
|- 4
|Vangathota Kaada SP Balu,P. Susheela
|4:27
|- 5
|Sri Suryanarayana Bhanumathi Ramakrishna, Vani Jayaram
|4:12
|}

==Box office==
* The film is successful at Boxoffice with good mass appeal and celebrated Silver Jubilee. It is the first 100 days film for Nandamuri Balakrishna.  It ran for 100 days in Karnataka and 332 days in Hyderabad and 500 above days in a theater in guntur and 1000 above days in a theater in Macherla. 

==Others== Hyderabad

==References==
 

==External links==
*  
*  

 
 
 