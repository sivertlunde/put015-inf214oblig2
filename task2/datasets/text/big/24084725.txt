They Who Dare
 
{{Infobox film
 | name = They Who Dare
 | image= They Who Dare VideoCover.png
 | image_size = 
 | caption = DVD cover
 | director = Lewis Milestone
 | producer = Aubrey Baring and Maxwell Setton
 | writer = Robert Westerby (screenplay)
 | starring = Dirk Bogarde Denholm Elliott Akim Tamiroff
 | cinematography = Wilkie Cooper
 | music = Robert Gill
 | editing = Vladimir Sagovsky
 | distributor = British Lion Film Corporation
 | released = 2 February 1954
 | runtime = 107 mins.
 | country = United Kingdom English
| gross = £132,074 (UK) 
}} Second World War war film directed by Lewis Milestone and starring Dirk Bogarde, Denholm Elliott and Akim Tamiroff. The story is based on events that took place during World War II in the Dodecanese islands where special forces attempted to disrupt the Luftwaffe from threatening Allied forces in Egypt.  The title of the film is a reference to the motto of the Special Air Service: "Who Dares Wins".

==Plot synopsis== Greek officers and two local guides, are sent on a mission to destroy two German airfields on Rhodes. Led by Lieutenant Graham (Dirk Bogarde), the party are taken to Rhodes by submarine. They come ashore at night and have to traverse the mountains to reach their targets. At a pre-designated location, the party split and the two groups press on and succeed in their attack on the airfields. However, eight of the group are captured and only two, Lieutenant Graham and Sergeant Corcoran, make it back to the pick-up point where they are rescued by the submarine.

==Production==
The film was partly shot on location in Cyprus.

==Cast in credits order==
 
*Dirk Bogarde as Lieut. Graham
*Denholm Elliott as Sgt. Corcoran
*Akim Tamiroff as Captain George One
*Harold Siddons as Lieut. Stevens R.N.
*Eric Pohlmann as Captain Papadapoulos William Russell as Lieut. Poole
*Gérard Oury as Captain George Two
*Sam Kydd as Marine Boyd
*Peter Burton as Marine Barrett David Peel as Sgt. Evans
*Michael Mellinger as Toplis
*Alec Mango as Patroklis
*Anthea Leigh as Marika
*Eileen Way as Greek woman
*Lisa Gastoni as George Twos girlfriend
*Kay Callard as nightclub singer
*George Sandford as Extra (Uncredited)
 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 