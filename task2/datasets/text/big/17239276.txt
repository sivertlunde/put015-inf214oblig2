Love Is Not All Around
 
 
{{Infobox film
| name           = Love Is Not All Around
| image          = 
| caption        = 
| director       = Patrick Kong
| writer         = Patrick Kong
| starring       = Alex Fong Lik-Sun Linda Chung Stephy Tang Hins Cheung Miki Yeung Terry Wu Sammy Leung Kary Ng (voiced)
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
}}
Love Is Not All Around (traditional Chinese： 十分愛) is a 2007 Hong Kong film written and directed by Patrick Kong.

==Plot==
Bo (Stephy Tang) is an expert on dating boys, explaining her strategy to her friends at Wing (Sammy Leung) and Chings (Linda Chung) wedding.  During the wedding Bo sees Bik Lin, her "love rival".  Bo also meets Michael (Philip Ng) for the first time and soon asks him to drive her home. After the wedding, the newly weds head back to their house along with Siu Mo (Terry Wu), where it is seen hinted that Siu Mo likes Ching.  Three months later, while Bo is shopping she sees Wing with Bik Lin across the street, Bik Lin puts eye drops on Wings eye.

Bo goes to Chings house for dinner that night, waiting for Wing, who comes home late and said he had eaten outside, Bo becomes angry.  The next day, Bo meets one of her previous boyfriends, Ryan (Alex Fong).  Ryan was very calm and it was him that had come up to Bo first.  During the flashback that followed, Bo and Ryan were seen happily together, Ryan confesses his love and asks if Bo likes him too, Bo says yes.  Then skipping to the night where they break up, Bos new boyfriend, Vincent, whom she was trying to get to through Ryan, pushes Ryan to the ground.  Ryan angrily gets up and yells at Bo that "karma will get you!".  Following the breakup, Bo encounters some bad luck, believing it was Ryans curse that was responsible.

A few days later, she sees Ryan and talks to him, Ryan leaves after taking a phone call but explains it was his sister.  Wing meets up with Bo later and confessed that he does have an affair, with someone that she knows.  After that, Wing keeps pestering Bo, confiding his problems to her.  Bo was deeply in thought about Wings affair and the reappearance of Ryan while washing the dishes when she accidentally squirted detergent into her eyes.  Her friend Mon (Miki Yeung) rushes Bo to the hospital.  Bo was forced to sit down and wait. She was squirting detergent everywhere from the bottles she had brought when a doctor comes forward and amusingly replaces them with a box of Dragons beard candy.  The next day while Bo was getting her medications, someone comes up and says that he sees her eyes are better.  Seeing whom it was, she lights up with recognition, the "dragons beard candy!".  The doctor introduces himself as Joe (Hins Cheung) and he asks Bo out for dinner that night.

After dinner, Joe plays a piece on the piano at Bos house.  When he finishes, Bo and Joe prepares for a kiss.  The scene then changes to when they both rush out of the house, and then goes to a nearby 7-Eleven, Joe goes in to buy the "preparations" for when they get back.  Bo was waiting outside when Ryan turns up.  Bo tries to get him to go away, when Joe comes back, the cashier from 7-Eleven follows out saying that Joe had left the most important "preparation" item and the moment turns awkward.  Bo leaves.

She was trying to hail a taxi when Ryan drives over with his car, Bo gets into his car.  While walking Bo home, Ryan and Bo kisses and Joe sees, who came back to give Bo her bag.  Bo sees Bik Lin at a karaoke, when Ching arrives, she creates an opportunity for Bik Lin to leave as Bo believes she was meeting Wing there.  Bo wakes up on another morning when she finds that shes been "cursed", appearing disheveled, because of seeing Ryan again.  She gets called to an outdoor mountain event, where Ryan also appears.

Bo was running away from Ryan when she falls down a slope, Ryan comes down to help her and asks her what is wrong.  Ryan dismisses the claim of cursing Bo.  The others find them and Bo was happily telling Ryan they were going to be saved when Ryan says "Bo, you never believe anyone, youll never find true love".  A few days later Bo goes to Ryans house to confess her love when she sees another woman in his house, she then leaves, she has not seen Ryan since.  Bo, Ching and Mon then heads to a hotel believing Wing and the woman was there, although Bo sees Joe with another woman and she gets angry, Bo texts Joe that she hates him and never to find her again.  It was shown what she didnt see was the girl was Joes friends girlfriend.

They charge up to the hotel room, only to find Bik Lin and Michael was in there, Bik Lin explains that she only didnt want Bo to find out Michael had chosen her and not Bo.  Ching blames Bo, saying that she lied to her, and slaps her (Siu Mo had followed Bo and Ching had originally thought Bo was the other woman).  Heading home, Bo realises that Mon was the other woman, Mon confesses, but shows no remorse, Bo says she has lost two friends that night, one was Ching, the other was Mon.  One year later, she meets Ching on the streets again, after Ching explaining that Wing had come back and the past should be forgotten (as well as Siu Mo dying of brain cancer), Bo goes to Ryans house.  She confessed that she loved Ryan and could he forgive her for what she has done, Ryan does not speak and Bo leaves.

Ryan rushes out after a few mins, Bo was sitting on the stairs, Bo cries and says that she was selfish before and could they start again, Ryan agrees.  Ryan said that he had wanted to tell her that the woman she saw one year ago was his sister.  The movie ends when Ryan meets up with Bo at a restaurant, Ryans sister was at another table, her boyfriend asked if Ryan was her ex and if the woman was his new girlfriend. She says no, the person with her ex is his sister, revealing Ryan was a player who had lied to both of them that the other was his sister.  The final scene shows Ryan and Bo holding hands, in a voiceover Bo saying that love is all about lying and deceiving, thus showing true to the statement Bo had made in the beginning of the movie, "sometimes the things you see might not be real and the things that are real you might not see".

==Cast==
*Alex Fong Lik-Sun as Ryan
*Stephy Tang as Bo
*Linda Chung as Ching
*Hins Cheung as Joe
*Miki Yeung as Mon
*Terry Wu as Siu-mo
*Sammy Leung as Wing
* )
*Philip Ng as Michael
*Janice Man as Ryans ex-girlfriend
*Sheila Chan as Bos mother

==Music==
Five main songs were played during the movie.

{| class="wikitable"
|-
! Title
! Literal translation
! Performer
! Notes
|-
| 逼得太緊
| Compelled Too Tightly
| Kary Ng
| Main Theme and also used at the end of the movie
|-
| 兩個世界
| Two Worlds
| Terry Wu
| Used in background when Ching learnt of Siu Mos death
|-
| 親近對．親熱錯
| Close is Right, Love is Wrong
| Alex Fong Lik-Sun
| Used in background during when Ryan and Bo fell down the mountain slope 
|-
| 日久生情
| Enduring Love
| Stephy Tang
| Used in background on the flashback between Bo and Ryan
|-
| 無能為力
| Helpless
| Hins Cheung
| Piano piece played by Joe at Bos house
|}

==Awards==
27th Hong Kong Film Awards
* Won: Best Original Film Song (Kary Ng)
* Nominated: Best New Performer (Linda Chung)

== External links ==
*  

 

 
 
 