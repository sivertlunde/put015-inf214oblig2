Colored Frames
{{Infobox film
| name           = Colored Frames
| image          = 
| alt            = 
| caption        = 
| director       = Lerone D. Wilson
| producer       = Nonso Christian Ugbode
| writer         =  Ed Clark  Ann Tanksley  Mary Schmidt Campbell  Gustave Blache III  Francks Deceus  Danny Simmons
| music          = Magali Souriau   Lenae Harris
| cinematography = 
| editing        = Lerone D. Wilson
| studio         = 
| distributor    = Microcinema International
| released       =  
| runtime        = 56 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Colored Frames is a 2007 documentary film taking a look at the role of fine art in the Civil Rights Movement, as well as the legacy of discrimination in the art community both historically and contemporarily. The documentary is a showcase of a wide variety of works primarily by African-American artists, and a discussion of modern sociopolitical topics focused on race, gender, and class. {{cite web
| first = Bob
| last = Etier
| date = 2012-02-11
| title = Colored Frames: A Visual Art Documentary (2007) Looks at Art and Racism
| url = http://technorati.com/entertainment/film/article/colored-frames-a-visual-art-documentary/
| accessdate = 2012-02-21
}} 
Beginning in late 2011 the film began airing nationally in the U.S. via American Public Television. {{cite web
| title = American Public Television Catalog - Colored Frames
| url = http://www.aptonline.org/catalog.nsf/vLinkTitle/COLORED+FRAMES
| accessdate = 2012-02-21
}}   

Artists who appeared on camera in this documentary included: 
* Benny Andrews
* John Ashford
* Gustav Blache
* Linda Goode Bryant
* Mary Schmidt Campbell
* Nanette Carter
* Ed Clark
* Francks Deceus
* Larry Hampton
* Gordon C. James
* June Kelly of the June Kelly Gallery
* Wangechi Mutu
* Ron Ollie
* Danny Simmons
* Duane Smith
* Tafa

== References ==
 

==External links==
*   
*  

 
 
 
 
 
 
 


 