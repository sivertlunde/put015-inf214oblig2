Drums Across the River
{{Infobox film
| name           = Drums Across the River
| caption        =
| image	         = Drums Across the River FilmPoster.jpeg Nathan Juran
| producer       = Melville Tucker
| writer         = John K. Butler
| starring       = Audie Murphy
| music          =
| cinematography = Harold Lipstein
| editing        = Virgil W. Vogel
| distributor    = Universal-International (UI)
| released       =  
| runtime        = 78 min
| country        = USA
| language       = English
| budget         =
| gross          =
}} Western directed Nathan Juran, starring Audie Murphy and Walter Brennan. 

==Plot==
Gary Brannon (Murphy), is a peaceful homesteader living a quiet existence with his father Sam (Walter Brennan). Frank Walker (Lyle Bettger) is hoping to open up the Ute Indian territory for gold-mining purposes and tries to foment a war between the Utes and the local whites, while he steals a gold shipment and pins the blame on Gary. Gary starts off hating the Utes because they were responsible for killing his mother but gradually comes to be on their side and wants to expose the machinations of Walker.

==Cast==
* Audie Murphy as Gary Brannon
* Walter Brennan as Sam Brannon
* Lyle Bettger as Frank Walker
* Lisa Gaye as Jennie Marlowe
* Hugh OBrian as Morgan
* Mara Corday as Sue Randolph
* Jay Silverheels as Taos
* Emile Meyer as Nathan Marlowe
* Regis Toomey as Sheriff Jim Beal
* Morris Ankrum as Chief Ouray Bob Steele as Billy Costa
* James K Anderson as Jed Walker George Wallace as Les Walker
* Lane Bradford as Ralph Costa
* Howard McNear as Stilwell

==Production==
The film was shot mostly on the Universal backlot, with location filming at Barton Flats in Californias San Bernardino Mountains. This was Murphys last film with Nathan Juran. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 