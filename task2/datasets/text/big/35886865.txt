Ace of Aces (1933 film)
 {{Infobox film
| name           = Ace of Aces
| image          = Ace-of-aces-movie-poster.jpg
| caption        = Original theatrical poster
| director       = J. Walter Ruben
| producer       = Merian C. Cooper (executive producer)
| screenplay     = John Monk Saunders (screenplay) H.W. Hanemann (screenplay),
| based on       = The Bird of Prey by John Monk Saunders Richard Dix Elizabeth Allan Ralph Bellamy
| music          = Max Steiner 
| cinematography = Henry Cronjager
| editing        = George Hively	 	 
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures 
| released       =  
| runtime        = 144 minutes
| country        = United States
| language       = English 
| budget         =  
| gross          = 
}} Richard Dix, it was similar to many of the period films that appeared to glorify the "knights of the air", but was more complex, examining the motivations of those who choose to go to war. 

==Plot==
When the United States enters World War I, Nancy Adams (Elizabeth Allen) becomes a Red Cross nurse, while her fiancé, Rex "Rocky" Thorne (Richard Dix), registered as a conscientious objector, pursues his sculpting career. She mocks his pacifism and accuses him of cowardice. Thorne, in retaliation for her rejection and determined to prove his bravery, enlists and trains to become a fighter pilot. As part of the American Expeditionary Forces, he is, at first, a hesitant combat pilot until he encounters his first enemy aircraft and shoots it down. Renouncing his past, Thorne becomes completely committed to the war in the air, vowing to become the "ace-of-aces". 

While on furlough in Paris, Thorne, now the leading Flying ace|ace, runs into his former love, who is a nurse on the front lines.  She has been impacted by her experiences and torn by guilt, agrees to spend the night with her former fiancé. Returning to his squadron, Thorne suffers a head wound in a skirmish and as he reaches his base, he shoots down a German cadet on a mercy mission to drop a note over the airfield, telling the squadron of the whereabouts of one of their downed airmen. 

Thorne lands in the hospital, next to the German cadet who is dying, and gives in to the young mans pleas for a drink, giving him some wine, knowing it will speed his death. When he recuperates, unable to bear his guilt for killing an innocent flyer, Thorne asks to be relieved from combat to become an instructor. Before leaving his squadron, the realization that a young pilot has bettered his record of victories, leads him to one last mission, albeit unauthorized. Encountering the new German fighter aircraft that are the match for his fighter, he prepares to close in for a "kill", but sees the face of the German cadet that he had needlessly shot down, and is unable to fire. In the fight, Thorne is wounded and crashes, but lives to return home and marry his sweetheart.	

==Cast==
As appearing in Ace of Aces, (main roles and screen credits identified):   Turner Classic Movies. Retrieved: May 20, 2012.    Richard Dix as 2nd Lieutenant Rex "Rocky" Thorne
* Elizabeth Allen as Nancy Adams
* Ralph Bellamy as Major Blake
* Theodore Newton as Lieutenant Foster "Froggy" Kelley
* Nella Walker as Mrs. Adams
* Anderson Lawlor as Lieutenant Roger "Red" Cahill
* Frank Conroy as Colonel Wentworth
* Joe Sauers as Captain Daly Arthur Jarrett as Lieutenant James "Jenny" Lind
* Claude Gillingwater Jr. as Lieutenant Tommy Grey
 
* Clarence Stroud as Lieutenant Billy Winstead
* Claude Stroud as Lieutenant Carroll Winstead Frank Clark as Helmut Gorin, German cadet
* William Cagney as Lieutenant Meeker
* Howard Wilson as Lieutenant Tim "Tombstone" Terry
* Frank Melton as Lieutenant Phil "Pee Wee" Parker
* Carl Eric Hansen as Lieutenant Nicholas Healy
* George Lollier as Lieutenant Ralph "Rudderbar" Smith
* Grady Sutton as Party guest
* Betty Furness as Party guest
 
William Cagney, the brother of James Cagney made his motion picture debut in Ace of Aces as an actor and went on to become a Hollywood producer.   Turner Classic Movies. Retrieved: May 20, 2012. 

==Production==
  made up the core of the German enemy air force.]] The Eagle and the Hawk released in May 1933. His second story, The Bird of Prey became the basis of Ace of Aces, which  was  shot in the summer and came out in October 1933. 
 Waco 7s, Travel Air Curtiss JN4 Jenny, Nieuport 28|Garland-Lincoln LF-1 (Nieuport 28 Replica), and a Fleet Model 1, along with two Royal Aircraft Factory S.E.5|SE.5 mock-ups while a Waco also served as a camera plane for the aerial sequences.  Noted Hollywood stunt pilot Frank Clarke was one of a team of eight pilots who flew in the film.  Some of the aerial footage was taken from the earlier Howard Hughes opus, Hells Angels (film)|Hells Angels (1930). 

==Reception==
Contemporary reviews of Ace of Aces were not positive; Mordaunt Hall, The New York Times reviewer noted, "In a style that is slow, obvious and at times childishly sentimental  ... Richard Dix is heavy-handed and generally inexpert in the principal role. Elizabeth Allan, who seems to one biased observer to be the most genuinely talented and charming of Hollywoods recent acquisitions, gives Ace of Aces its only good moments."  More recent reviews have focused on the "strongly anti-war drama", and the callousness of killing men in the air in Ace of Aces, revealed as the hero has to examine his life and the choices he has made as a fighter pilot.  Leonard Maltin considered it, a "Sincere antiwar tract, but far too melodramatic." 

Filmmaker Guy Maddin is an avowed fan of the film, describing it in an article for Film Comment as an "absolutely perfect low-budget fighter-pilot picture that deserves the same reputation as Gun Crazy" and praising Dix as a "forgotten American treasure."  

==References==
===Notes===
 
===Bibliography===
 
* Farmer, James H. Broken Wings: Hollywoods Air Crashes. Missoula, Montana: Pictorial Histories Pub Co., 1984. ISBN 978-9-999926-515.
* Neibaur, James L. The RKO Features: A Complete Filmography of the Feature Films Released or Produced by RKO Radio. Jefferson, North Carolina: McFarland & Company, 2005. ISBN 978-0-786421-664.
* Wynne, H. Hugh. The Motion Picture Stunt Pilots and Hollywoods Classic Aviation Movies. Missoula, Montana: Pictorial Histories Publishing Co., 1987. ISBN 0-933126-85-9.
 

==External links==
*  
*  
*  

 
 
 
 
 
 