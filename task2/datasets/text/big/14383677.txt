The Comic
{{Infobox film
| name = The Comic
| image = Comicposter.jpg
| image_size =
| caption = theatrical poster
| director = Carl Reiner
| producer = Carl Reiner Aaron Ruben
| writer = Carl Reiner Aaron Ruben
| narrator = Dick Van Dyke
| starring = Dick Van Dyke Mickey Rooney Michele Lee Jack Elliott
| cinematography = W. Wallace Kelley
| editing = Adrienne Fazan
| studio = Columbia Pictures Corporation
| released =  
| runtime = 94 minutes
| country = United States English
| budget =
}}

The Comic is a 1969 comedy film co-written, co-produced and directed by Carl Reiner.  It stars Dick Van Dyke as Billy Bright &ndash; the original title of the film &ndash; and Michele Lee as Brights love interest, Reiner himself and Mickey Rooney as Brights friend and work colleagues.  Reiner wrote the screenplay with Aaron Ruben, which was inspired by the end of silent film era, and in part, by the life of silent film superstar Buster Keaton.

== Plot ==
Billy Bright (Dick Van Dyke), a Silent comedy film|silent-era film comedian, narrates this film which begins at his characters funeral in 1969 and tells his life story in Flashback (narrative)|flashbacks, unable to see his own faults and morosely (and incorrectly) blaming others for the turns of the events in his life.

Headstrong and talented, vaudeville clown Bright arrives on his first California film location insisting that he will perform his bit role only if he can wear the outrageous costume and makeup of the character he has been known for on the stage.  The director refuses and Bright begins to storm off, but when his car rolls off a cliff he is forced to accept the terms.  As soon as the cameras are rolling, however, he improvises (and sabotages) his way to becoming the hero of the scenario.  His combination of acquiescence and audacity pays off, and before long he has become a major film comedy star in the 1910s and 20s, the silent picture era of Buster Keaton, Harold Lloyd, Charlie Chaplin and Stan Laurel.  He steals his leading lady, Mary (Michele Lee), from the film bigwig shes been dating and the two increasingly popular performers marry, starting their own production company together.  As early as her pregnancy, however, she begins to suspect his adultery; when she confronts him he tries to turn the tables and shame her into apologizing for the accusation.  When, at the height of their fame and fortune, he is served with papers naming him in a Hollywood power couples divorce filing, she leaves him, taking their young son — and the couples palatial estate.

Bright sinks into despair and   and only friend, Cockeye (Mickey Rooney).

A late-1960s talk show host (Steve Allen as himself) has the faded star on in an effort to revive Brights career, and the elderly comedian proves capable of — if somewhat pathetic to the groovy stars of the day on the couch alongside him — for recreating his old pratfall schtick.  The pitch works, but this time the only vehicle that will allow him to run through his preferred brand of slapstick is a detergent TV commercial|commercial.  The denouement of Brights life, and the film, finds him in and out of the hospital, and visited by his now-grown son Billy Jr. (also played by Van Dyke in a dual role), reduced to setting the alarm in his dingy two-room apartment, and catching airings of he and his former wifes old comedies at odd hours on TV — which he watches without a hint of a smile.

==Cast==
  
*Dick Van Dyke as Billy Bright / Billy Bright, Jr.  
*Michele Lee as Mary Gibson
*Mickey Rooney as Martin "Cockeye" Van Buren
*Cornel Wilde as Frank Powers
*Nina Wayne as Sybil
*Pert Kelton as Mama
*Steve Allen as Himself
*Barbara Heller as Ginger
 
*Ed Peck as Edwin G. Englehardt
*Jeannine Riley as Lorraine
*Gavin MacLeod as 1st Director
*Jay Novello as Miguel
*Craig Huebing as Doctor
*Paulene Myers as Phoebe
*Fritz Feld as Armand
 

== Production ==
In his autobiography,  . 
 The Morning After, Van Dyke disclosed that he had only recently overcome a serious drinking problem in his own life.

== Response ==
Not many people saw the film upon its release, but in later years, it would be liked as a movie about one silent stars rise and fall, patterned after the many silent stars that tried to make it and failed in later years: among the exceptions would be silent and sound film star Stan Laurel, whom Dick Van Dyke loved. He would later learn that since the end of Laurels time with Oliver Hardy, many imitators came and went and no attempt was made to legally stop them from imitating Laurel, his friend Hardy or even copying his face, which in part inspired this film. Van Dyke said of The Comic: "very few people saw that movie, but we were proud of it." 

== Availability ==
The Comic was produced on a VHS tape published by RCA/Columbia Pictures Home Video (Burbank, CA) in 1986 ( ).  Columbia TriStar Home Entertainment also released a VHS edition the same year (ISBN 0800132823).

==References==
;Notes
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 