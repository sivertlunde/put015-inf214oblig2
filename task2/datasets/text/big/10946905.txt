Everybody Go Home
{{Infobox film
| name           = Tutti a casa
| image          = Everybody Go Home.jpg 
| caption        = 
| director       = Luigi Comencini
| producer       = Dino De Laurentiis
| writer         = Age & Scarpelli Luigi Comencini Marcello Fondato
| starring       = Alberto Sordi Eduardo De Filippo Serge Reggiani Martin Balsam Nino Castelnuovo Claudio Gora Carla Gravina 
| music          = Angelo Francesco Lavagnino
| cinematography = Carlo Carlini
| editing        = Giovannino Baragli
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Everybody Go Home ( ) is a 1960 Italian comedy-drama film directed by  Luigi Comencini.  It features an international cast including the U.S. actors Martin Balsam, Alex Nicol and the Franco-Italian Serge Reggiani. Nino Manfredi was rejected for the starring role because Alberto Sordi wanted it.
 Escape by The Fascist, The Abandoned, The Four Days of Naples, and Salò, or the 120 Days of Sodom.

==Plot==
Along the Venetian seaside, on the morning of September 8, 1943, Alberto Innocenzi, junior NCO of the Royal Italian Army is shocked when (in response to the separate surrender signed by the Badoglio government in Cassibile (village)|Cassibile) the former allies of the Wehrmacht surround and take by storm the base where hes stationed. Innocenzi, along with some disbanded soldiers, manages to distance the German troops and is thoroughly shocked when, contrary to his plan of finding a higher echelon to which to report, most of the men accept the fact that the war is over for them and "everybody should just go home".

This reaction at first angers him, but in the end he joins army engineer Ceccarelli and sergeant Fornaciari in discarding their uniforms for civilian clothes and heading southwards for a veritable odyssey along the Italian "boot", cut in two by German and Allied occupation and wracked by partisan warfare, bloody reprisals, German press-ganging and other perils. The trio meets a band of Italian resistance movement|anti-fascist guerrillas but decline to join them (while an Italian army captain they met along the road does). Later Innocenzi, caught in the "everybody for himself" mentality which seems to dominate the landscape, succumbs to the temptation of abandoning his mates to help a sultry black marketeer smuggle a load of flour to Rome, as she needs a driver and does not have room for any other passengers. The deal goes awry due to a mechanical failure in a rubble-strewn town where the famished populace plunders the lorry, and after some recriminations and a brief scuffle, Innocenzi rejoins his companions. They witness the killing of a rookie, a naive Italian soldier who tried to protect a Jewish girl during a German roundup; finally they manage to reach Fornaciaris rural home.

The former sergeant is delighted of having returned to his young wife, children and old father and offers Innocenzi and Ceccarelli hospitality for the night; his wife reveals that the family has been hiding a former U.S. POW who escaped from the Folpiano prison camp to protect him from the fascist militia patrols and Fornaciari, albeit grudgingly, accepts to keep protecting him. After a darkly humorous polenta dinner (served farm-style with salsa over a wooden table, where Innocenzi and the U.S. officer end up arguing over the right of reaching for the sausage length placed at the tables centre) they all go to sleep but a nighttime fascist patrol breaks in and manages to find the Allied serviceman. Amidst the childrens cries and his wifes weeping, Fornaciari is hauled away toward a grim fate, and Ceccarelli and Innocenzi flee the premises without being able to help.

The couple manages to reach Littoria (now Latina), where Innocenzis widowed father lives alone and offers Ceccarelli (who is Naples|Neapolitan) to stay a few days before resuming his travel south. Innocenzi is shocked when his father introduces him to a fascist party leader who is recruiting men for the army of the Repubblica Sociale Italiana, the fascist puppet state instituted by the Germans after the liberation of Benito Mussolini from his Gran Sasso exile. His father angrily responds to his objections, citing the misery hes living in and all the sacrifices he incurred to allow make him to study and become an army officer; a deep rift is created between father and son, and Innocenzi asks Ceccarelli to take him south to Naples as well, to which the engineer happily consents.

However, Naples is in grimmer conditions than Rome, directly on the line of fire after the Allied landing at Salerno, barely kept under control by brutal German detachments who round up able-bodied men to send them to Germany as slave workers. To reach the city the duo passes through a roadblock manned by fanatical, hungry fascists; Ceccarelli, generously chooses to sacrifice a suitcase of delicacies he was meant to deliver to the wife of his Commanding Officer (who had signed his dismissal on health grounds due to his persistent stomach ulcer). The situation (with the offering readily accepted by the roadblock patrol) causes Innocenzi much panic, having had raided the case during a night-time train trip and substituting the goods with stones and newspaper sheets; he urges his companion to run away before the fascists open it, but they are unsuccessful.
 Breda Breda M 37 machine gun they have captured but cannot operate. With a renewed stern look on his face, Innocenzi opens fire against the Germans, bringing the movie to a close.

==Cast==
* Alberto Sordi: Lieutenant Alberto Innocenzi
* Eduardo De Filippo: Mr. Innocenzi, father of Alberto
* Serge Reggiani:  Assunto Ceccarelli 
* Martin Balsam: Sergeant Quintino Fornaciari  
* Nino Castelnuovo: Codegato  
* Carla Gravina: Silvia Modena  
* Claudio Gora: Colonel
* Mario Feliciani: Capt. Passerini
* Alex Nicol: American prisoner
* Guido Celano: Fascist
* Didi Perego: Caterina Brisigoni, flour dealer

==Awards==
*Italian National Syndicate of Film Journalists Nastro dArgento
**Winner: Best Production (Migliore Produzione) - Dino De Laurentiis
*2nd Moscow International Film Festival
**Winner: Special Golden Prize   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 