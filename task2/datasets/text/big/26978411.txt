La nostra vita
 
{{Infobox film
| name           = La nostra vita
| image          = La nostra vita.jpg
| caption        = Film poster
| director       = Daniele Luchetti
| producer       = Gina Gardini
| writer         = Daniele Luchetti Sandro Petraglia Stefano Rulli
| starring       = Raoul Bova
| music          = 
| cinematography = Claudio Collepiccolo
| editing        = Mirco Garrone
| studio         = Cattleya
| distributor    = 01 Distribution
| released       =  
| runtime        = 93 minutes
| country        = Italy France
| language       = Italian
| budget         = € 6.2 million
| gross          = € 3 million
}}
La nostra vita is a 2010 French-Italian film directed by Daniele Luchetti. It competed for the Palme dOr at the 2010 Cannes Film Festival,    with Elio Germano sharing the prize for Best Actor with Javier Bardem for his role in the Mexican film Biutiful directed by Alejandro González Iñárritu.   

==Plot==
Claudio is a young mason who lives a happy life with his wife Elena and their children; they manage together their daily difficulties with love and complicity. 
His life is suddenly struck when Elena dies while giving birth to their third son. Claudio reacts by blackmailing his employer, who had purposely hidden the corpse of a man, illegally employed and dead while working in a drunk state: in exchange for his silence, Claudio obtains to be entitled to the whole construction business (and not only the wall building). 
This is the beginning of a new phase for Claudio, who now concentrates on becoming richer, and buying "things" to his sons in the hope of making them happier after the loss of their mum. More and more difficulties, however, put Claudio in dire straits, which eventually force him to borrow money from his relatives and friends, in order to save himself from bankruptcy. Accompanied by the son of the dead illegal worker, Claudio will manage to change his vision of life, and to give importance to the real values.

==Cast==
* Elio Germano as Claudio
* Isabella Ragonese as Elena
* Raoul Bova as Piero
* Stefania Montorsi as Liliana
* Luca Zingaretti as Ari
* Giorgio Colangeli as Porcari
* Alina Madalina Berzunteanu as Gabriela
* Marius Ignat as Andrein
* Awa Ly as Celeste
* Emiliano Campagnola as Vittorio

==Production== Rai Cinema Italian Culture CNC and Canal+ via pre-sales. 

==Release==
The film premiered in Italy on 21 May 2010. As of 20 June 2010, Box Office Mojo reported that the film had made 3,805,514 dollars (3,075,807 Euro) at the Italian box office. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 