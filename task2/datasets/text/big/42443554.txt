Mojugara Sogasugara
{{multiple issues|
 
 
}}

{{Infobox film|
| name = Mojugara Sogasugara
| image = 
| caption = Vijay
| writer = Vijay Vishnuvardhan  Shruti  Sonakshi
| producer = N. Kumar
| music = Hamsalekha
| cinematography = Johnny Lal
| editing = S. Prasad
| studio = Sri Lakshmi Films
| released = 1995
| runtime = 143&nbsp;minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada romantic Shruti and Sonakshi in the lead roles. 

==Plot==

 

== Cast ==
  Vishnuvardhan as Vijay and Vinod Shruti
* Doddanna
* Lokesh
* Pandari Bai
* Tennis Krishna Jayanthi
* Sathya Bhama
* Sihi Kahi Chandru
* Pandaribai
* Rama Murthy
* Keerthi
* Dingri Nagaraj
* Shivaram
* Sonakshi
* Baby Ranjitha
* Rajanand
* Sathyajit
* Kunigal Vasanth
 

== Soundtrack ==
The music of the film was composed byu and lyrics written by Hamsalekha. After release, the soundtrack was well received.  Audio was released on Lahari Music.
The song Kannadave Nammamma of this film is rated as one of the top 10 Kannada patriotic songs by Filmibeat. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 = Yaramma Ivalu
| extra1 = Mano (singer)|Mano, Manjula Gururaj
| lyrics1 = Hamsalekha
| length1 = 
| title2 = Hoovamma Hoovamma
| extra2 = Mano (singer)|Mano, Lata Hamsalekha
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Chorarigondu Kaala Mano
| lyrics3 =Hamsalekha
| length3 = 
| title4 = Mojugarana Sogasugarana
| extra4 = Mano (singer)|Mano, Manjula Gururaj
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Kannadave Nammamma Vishnuvardhan
| lyrics5 = Hamsalekha
| length5 = 
}}

==Reception==
Produced by N. Kumar, upon release the film received positive response for its songs by Hamsalekha and performances of its lead actors. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 