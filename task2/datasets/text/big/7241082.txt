Miss Grant Takes Richmond
{{Infobox film
| name           = Miss Grant Takes Richmond
| image_size     =
| image	         = Miss Grant Takes Richmond FilmPoster.jpeg
| caption        =
| director       = Lloyd Bacon
| producer       = S. Sylvan Simon
| writer         = Everett Freeman (story) Devery Freeman Nat Perrin Frank Tashlin
| starring       = Lucille Ball William Holden Heinz Roemheld
| cinematography = Charles Lawton Jr.
| editing        = Jerome Thoms
| distributor    = Columbia Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}

Miss Grant Takes Richmond is a 1949 comedy film starring Lucille Ball and William Holden, directed by Lloyd Bacon and released by Columbia Pictures. It was released under the title Innocence is Bliss in the UK.

==Plot==
For Ellen Grant (Ball), the worst student at the Woodruff Secretarial School, it comes as a great surprise when Dick Richmond (Holden) hires her to work at his realty company. Actually, it is her apparent empty-headedness that has won her the job. The real estate firm, and now Ellen, are merely fronts for a bookmaking operation run from the back of the office, where Dick and his associates, Gleason (James Gleason) and Kilcoyne (Frank McHugh), take bets on races.
 Will Wright) in eviction proceedings against several of her friends. There is an acute shortage of low-cost housing, exacerbated by Johnsons plans to tear down what he has and rebuild more expensive units. 
 Stephen Dunne), Dick has to play along. Little does she know her plans to construct affordable housing are driving Dicks organization into financial trouble. He cannot fire her without questions being asked, so he tries being aggressively romantic with her. This backfires, however: both he and Ellen find themselves enjoying embracing and kissing. 

Young widow Mrs. Peggy Donato (Janis Paige) comes to see her old flame, Dick, to try to get him to run her much larger bookmaking operation (inherited from her late husband). She and Ellen soon detest each other. Dicks trouble really begins when Ellen unwittingly takes a bet from Mrs. Donato on a fixed race, putting Dick in debt to her for $50,000. Mrs. Donato, who would rather have Dick than the winnings, tells him that if he does not go away with her or pay her, her gang will deal with him.

To raise the money, Dick lets Ellen take charge of the housing development, having Kilcoyne embezzle enough funds from down payments on the new homes. When the funds run out before the homes are built, she accepts full responsibility, believing that her own incompetence was to blame. Seeing the girl he has come to love suffer, Dick decides to go away with Mrs. Donato and pay the people back.

Ellen discovers the truth behind the missing money and the betting racket, but forgives Dick and cooks up a scheme to force Mrs. Donato to leave Dick alone, pretending to be the brains behind the bookmaking operation, back by her own "gang". Peggys men, however, are too tough. Just in time, Gleason and Kilcoyne show up with the $50,000, won by a bet placed with Mrs. Donatos own organization. Dick and Ellen embrace.

==Cast==
*Lucille Ball as Ellen Grant
*William Holden	as Dick Richmond
*Janis Carter as Peggy Donato
*James Gleason as Timothy P. Gleason
*Gloria Henry as Helen White
*Frank McHugh as Mr. Kilcoyne
*George Cleveland as Judge Ben Grant Stephen Dunne as Ralph Winton

==Notes== Sunset Boulevard.

Miss Grant Takes Richmond was well received by critics and movie-goers in 1949. 
 
==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 