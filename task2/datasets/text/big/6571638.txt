The Ghost Walks
{{Infobox film
| name           = The Ghost Walks
| image          = Poster of the movie The Ghost Walks.jpg
| caption        =
| director       = Frank R. Strayer
| producer       = Maury M. Cohen
| writer         = Charles Belden
| narrator       =
| starring       =
| music          =
| cinematography = M.A. Anderson
| editing        = Roland D. Reed
| distributor    = Chesterfield Motion Pictures Corporation Hollywoods Attic Sinister Cinema Teakwood Video
| released       =  
| runtime        = 69 min
| country        = United States English
| budget         =
}}
The Ghost Walks is a 1934 horror film, directed by Frank R. Strayer.

==Plot==
On a stormy night, a theatrical producer, his secretary, and playwright Prescott Ames are stranded when their car skids off the road and gets stuck. The three take refuge in the nearby home of Dr. Kent, a friend of Ames. One of Kents patients, who is staying at the house, is acting strangely, and the others in the house tell the newcomers that she is behaving this way because it is the anniversary of her husbands murder. At dinner, the group begins exchanging accusations about the murder, when suddenly the lights go out, and soon afterwards comes the first in a series of mysterious and fearful events.

The producer thinks all the strange occurrences are part of a ploy to get him to produce a play for Ames.  In a great line, one of the other characters exclaims "These fools think we are putting on a play for their benefit!"

The usual homespun collection of storm effects, sliding panels, bumps in the night and mysterious prowlings.  The standard mixture of comedy and terrors, The Ghost Walks is more competently staged than scripted.

==Cast==
*John Miljan as Prescott Ames
*June Collyer as Gloria Shaw
*Richard Carle as Herman Wood
*Henry Kolker as Dr. Kent
*Johnny Arthur as Homer Erskine
*Spencer Charters as a Guard
*Donald Kirke as Terry Shaw aka Terry Gray
*Eve Southern as Beatrice
*Douglas Gerrard as Carroway (billed as Douglas Gerard)
*Wilson Benge as Jarvis
*Jack Shutta as Head Guard
*Harry Strang as Guard

==See also==
*List of ghost films

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 