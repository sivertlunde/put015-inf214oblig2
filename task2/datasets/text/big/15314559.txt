Iodo (film)
{{Infobox film
| name           = Iodo
| image          = Iodo (film).jpg
| alt            = 
| caption        = Theatrical poster
| film name = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = I-eodo
 | mr             = I-ŏto
 | context        = }}
| director       = Kim Ki-young 
| producer       = Lee Woo-seok
| writer         = Ha Yoo-sang
| based on       =  
| starring       = Lee Hwa-si Kim Jeong-cheol Park Jeong-ja Park Ahm
| music          = Han Sang-ki
| cinematography = Jeong Il-seong
| editing        = Hyeon Dong-choon
| studio         = DongAh Exports Co. Ltd.
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Iodo (hangul|이어도) is a 1977 South Korean mystery film directed by Kim Ki-young, and based on the 1974 novel of the same title by Lee Cheong-jun. It was shown at the 28th Berlin International Film Festival. 

== Synopsis ==
  
When a man from an island ruled by women disappears, the man suspected of killing him investigates his past. 

== Cast ==
  
* Lee Hwa-si... Sohn Min-ja (Barmaid) 
* Kim Chung-chul... Sun Wu-hyun
* Park Jung-ja... Munyeo
* Park Am... 제주일보 편집국장
* Gwon Hye-mi... Park Yeo-in
* Choi Yun-seok... Cheon Nam-seok
* Yeo Po
* Ko Sang-mi
* Lee Joung-ae
* Son Young-soon

== References ==

=== Notes ===
 

=== Bibliography ===
 
*  
*  
*  
*  
*  
*  
*  
*  
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 