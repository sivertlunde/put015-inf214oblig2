Seven Dollars on the Red
{{Infobox film
| name           = Sette dollari sul rosso
| image          =Seven Dollars on the Red.jpg
| image_size     = 
| caption        = 
| director       = Alberto Cardone, Melchiade Coletti
| producer       = Mario Siciliano 
| writer         = Juan Cobos
| narrator       = 
| starring       = 
| music          =Francesco De Masi
| cinematography = José F. Aguayo
| editing        = José Antonio Rojo
| distributor    =  1966 in Italy
| runtime        = 
| country        = Italy Italian
| budget         = 
}}
 1966 Italy|Italian Spaghetti Western film directed by Alberto Cardone. Its stars Anthony Steffen as the main character.

Despite the name similarity, the film is not a part of Sergio Leones Dollars trilogy. Evidently the film was inspired by this. On release in the United States, several of the cast members and production team had their names changed for the English audience.

Some parts of the soundtrack, composed by Francesco De Masi, are featured in the videogame Red Dead Revolver.

==Cast==
*Anthony Steffen ...  Johnny Ashley 
*Elisa Montés ...  Sybil 
*Fernando Sancho ...  El Cachal / Sancho 
*Roberto Miali ...   Jerry  
*Loredana Nusciak ...  Emily 
*Bruno Carotenuto ...  Rosario  
*José Manuel Martín ...  El Gringo / Chulo 
*Spartaco Conversi ...  Bill 
*Alfredo Varelli ...  1st Sheriff
*Gianni Manera ...  Gambler 
*Franco Fantasia ...  Sheriff of Wishville  
*Annie Giss ...  Julie / Starlight 
*Franco Gulà ...  Walt 
*Renato Terra ...  Manuel 
*Nino Musco...  Oeste ( 
*Miriam Salonicco ...  Oestes Wife 
*David Mancori ...  Jerry as Child 
*Fortunato Arena ...  Prisoner 
*Silvana Bacci ...  Mexican Woman 
*Halina Zalewska ...  Mexican Woman

==Plot==
The bandit Sancho kills the wife of Johnny Ashley, and because he cannot have a child of his own he abducts Johnny’s son Jerry to raise him as his own. Jerry grows up to become an evil man who kills his fiancée Sybil when she threatens to disclose his plans for a robbery. Johnny keeps searching to find his son and avenge his wife. He crosses paths with Jerry and in turn they save the other man’s life. Eventually Johnny confronts and kills Sancho. He learns about Jerry from Sancho’s wife. When the son comes to avenge his ”father,” Johnny tries to disarm him, but Jerry is accidentally killed without learning the truth. 

== External links ==
*  

 
 
 
 
 
 

 