Hirak Rajar Deshe
{{Infobox film name           = Hirak Rajar Deshe (In the kingdom of diamonds) image          = Dvd hirak rajar deshe.jpg alt            = Hirak Rajar Deshe (In the kingdom of diamonds) caption        = Theatrical release poster director       = Satyajit Ray producer       = Government of West Bengal writer         = Satyajit Ray screenplay     = Satyajit Ray story          = Satyajit Ray music          = Satyajit Ray cinematography = Soumendu Roy editing        = Dulal Dutta starring       = Tapen Chatterjee, Rabi Ghosh, Utpal Dutt, Soumitra Chatterjee,  Santosh Dutta, Promod Ganguli, Ajoy Banerjee, Kartik Chatterjee, Haridhan Mukherjee distributor    =  released       =   runtime        = 118 mins language  Bengali
|country        = India budget         = 
}}
 Goopy Gyne Bagha Byne series directed by Satyajit Ray. A unique aspect of the film is that most of the dialogues exchanged by the protagonists of the film are rhyming. The only person who did not speak in rhyme, was the teacher, symbolizing that though the thoughts of everybody else is bound, the teacher was a free-thinker.
The film was followed by the third film of the series directed by Satyajit Rays son, Sandip Ray.

==Plot==
The magically musical duo of Goopy Gyne and Bagha Byne make a comeback in this sequel, where they are invited to the court of the Hirak Raja (The Diamond King), for their musical skills. They are to perform at the kingdoms Jubilee Celebrations.

Goopy and Bagha are bored with their lives as crown princes of Shundi and Halla. They are looking for a change, which comes in the form of a chance to visit Hirak Rajya (Land of Diamonds), known for its huge diamond mines. They jocundly set out for Hirak Rajya. 
But little do they know of the machinations of Hirok Raja (Utpal Dutt). The king is a tyrant. Diamonds and riches get pent up in his treasuries, while his subjects starve and suffer. Those who protest are taken care of in the Jantarmantar, a chamber for brainwashing devised by the scientist (Santosh Dutta), who the king mocks calling as "Gobeshok Gobochondro Gyanotirtho Gyanorotno Gyanambudhi Gyanochuramoni." His ministers are mere puppets. The only enemy the king has in his land is Udayan Pandit (Soumitra Chatterjee). He is a school teacher and, more than that, he is a believer of values. The king forcefully closes his school down. Udayan flees to hide in the mountains.

Meanwhile, Goopy and Bagha are on their way to Hirok Rajya. By coincidence, they meet Udayan, who intimates them of the kings true nature. The two impress Udayan with their magical powers, who plans to use them against the tyrant. Goopy and Bagha agree.
The duo then head into Hirok Rajya, where they are welcomed with grandeur. They entertain the rogue king,fooling him into believing that they think he is great. They rob the treasury (which was guarded by a tiger) using their magical music, for bribing the guards.

The king has his tricks, too. He captures Udayan and all his students, and takes them to the Jantarmantar for brainwashing. But Goopy Bagha have already reached there using their magical powers. They have also bribed the Gobeshok onto their side, with the guards. On reaching the laboratory, the king and his ministers are stunned magically by Goopys singing and then pushed into the Brainwashing machine.
After the king is brainwashed he turns to the good side,he then along with the villagers pull down his own statue situated at the center of the village,and everything goes back to normal in the land of hirak raja.

==Crew==
* Producer: Government of India
* Director: Satyajit Ray
* Editor Dulal Datta
* Art Direction : Ashoke Bose
* Sound : Robin Sen Gupta, Durgadas Mitra 

==Cast==

{| class="wikitable sortable"
|-
! Character !! Actor(s)
|-
| Goopy || Tapen Chatterjee
|-
| Bagha || Rabi Ghosh
|-
| Hirak Raja || Utpal Dutt
|-
| Udayan Pandit || Soumitra Chatterjee
|-
| Shundi Raja || Santosh Dutta
|-
| Gobeshok Gobochondro Gyanotirtho Gyanorotno Gyanambudhi Gyanochuramoni A.K.A. Scientist || Santosh Dutta
|-
| Prohori (Sentry) || Kamu Mukhopadhyay
|-
| Udayans Father || Promod Ganguli
|-
| Udayans Mother || Alpana Gupta
|-
| Charandas || Rabin Majumdar
|-
| Fazl Mia || Sunil Sarkar
|-
| Balaram || Nani Ganguli
|-
| Bidusak || Ajoy Banerjee
|-
| Court Poet || Kartik Chatterjee
|-
| Court Astrologer || Harindhan Mukherjee
|-
| Ministers || Bimal Deb Tarun Mitra Gopal Dey Sailen Ganguli Samir Mukherjee
|}

==Awards==

{| class="wikitable sortable"
|-
! Year !! Award !! Awardee
|-
| 1980 || National Film Award for Best Male Playback Singer || Anup Ghoshal
|-
| 1980 || National Film Award for Best Music Direction || Satyajit Ray
|-
| 1980 || National Film Award for Best Feature Film in Bengali || Satyajit Ray
|-
| 1984 || Cyprus International Film Festival - Special Award  || Satyajit Ray
|}

==Songs==

All the songs were composed and penned by Satyajit Ray. He prominently used Anup Ghoshal as a voice of Goopy. So most of the songs were sung by Anup Ghoshal except one ("Kotoi Rongo Dekhi Duniay") sung by Amar Pal.
 National Film Best Music Best Lyrics Best Male Playback Singer Award for the song "Paaye Podi Baghmama".

{| class="wikitable sortable"
|-
! Track !! Title !! Singer !! Duration
|-
| 01 || "Mora Dujonai Rajar Jamai"|| Anup Ghoshal || 05:09
|-
| 02 || "Aar Bilombo Noy" || Anup Ghoshal || 02:15
|-
| 03 || "Kotoi Rongo Dekhi Duniay" || Amar Pal || 01:46
|-
| 04 || "Aaha ki Anondo Akashe Batashe" || Anup Ghoshal || 02:54
|-
| 05 || "Aha Shagore Dekho Chaye" || Anup Ghoshal || 01:40
|-
| 06 || "Eje Drishyo Dekhi Anyo" || Anup Ghoshal || 01:32
|-
| 07 || "Ebare Dekho Gorbito Bir" || Anup Ghoshal || 01:09
|-
| 08 || "Eshe Hirok Deshe" || Anup Ghoshal || 02:55
|-
| 09 || "Dhoronako Shantrimoshai" || Anup Ghoshal || 01:41
|-
| 10 || "Paaye Podi Baghmama" || Anup Ghoshal || 03:21
|-
| 11 || "Nohi Jontro" || Anup Ghoshal || 04:26
|-
| 12 || "Mora Goopy Bagha Dujon Bhayra Bhai" || Anup Ghoshal || 01:19
|}

==Background scorer==

{| class="wikitable sortable"
|-
! Track !! Title !! Duration
|-
| 01 || "King Arrives" || 00:28
|-
| 02 || "Ministers Get Necklace" || 01:12
|-
| 03 || "Entry of Scientist" || 
|-
| 04 || "The Brain-Washing Machine" || 01:32
|-
| 05 || "The Reciting Of Mantras" || 
|-
| 06 || "Udayan Advises His Students" || 01:14
|-
| 07 || "Burning The Books" || 
|-
| 08 || "Hiding The Distressed" || 00:51
|-
| 09 || "At The Kingdom Of Hirak" || 01:48
|-
| 10 || "Rings For The Kings" || 00:46
|-
| 11 || "Statue Inauguration" || 01:17
|-
| 12 || "Inside Diamond Mine" || 00:36
|-
| 13 || "To Find Udayan" || 02:10
|-
| 14 || "In The Secret Room" || 02:58
|}

==Sequels==

===Goopy Bagha Phire Elo===
 
Sandip Ray, son of director Satyajit Ray directed another sequel named Goopy Bagha Phire Elo. The film released twelve years after release of Hirak Rajar Deshe.

===Future===
Sandip Ray want to make another sequel to this series. He had received many request to make the fourth Goopy - Bagha movie. Ray said to The Times of India about the plot of fourth film: "Making a Goopy Bagha movie without Tapen and Rabi is unthinkable. The only way I can do a fourth is by taking the story forward and introducing Goopy and Baghas sons," he said. The idea to weave a story around the next generation came from a line from the introductory song Mora dujonai rajar jamai in Hirak Rajar Deshe — "aar ache polapan, ek khan ek khan... (we have one child each)". 

==See also==
* Joychandi Pahar

== References ==
 

== External links ==
*  
* 
* 

 
 

 
 
 
 
 
 
 