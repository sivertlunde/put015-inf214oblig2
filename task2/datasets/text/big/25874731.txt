Talk of the Town Tora-san
{{Infobox film
| name = Talk of the Town Tora-san
| image = Talk of the Town Tora-san.jpg
| caption = Theatrical poster 
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Reiko Ōhara
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 104 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}
 1978 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Reiko Ōhara as his love interest or "Madonna".  Talk of the Town Tora-san is the twenty-second entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
Mistakenly believing that his brother-in-laws boss is planning to commit suicide, Tora-san attempts to prevent him.    

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Reiko Ōhara as Sanae Arakawa
* Shimojo Masami as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hayato Nakamura as Mitsuo Suwa
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō
* Chishū Ryū as Gozen-sama

==Critical appraisal== Japan Academy Prize ceremony. Yoji Yamada was also nominated for Best Director at the ceremony for these two film.  The German-language site molodezhnaja gives Talk of the Town Tora-san three and a half out of five stars.   

==Availability==
Talk of the Town Tora-san was released theatrically on December 27, 1978.  In Japan, the film has been released on videotape in 1996, and in DVD format in 1998, 2002 and 2008. 

==References==
;Notes
 

;Bibliography
;English
*  
*  

;German
*  

;Japanese
*  
*  
*  
*  

==External links==
*  
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 


 