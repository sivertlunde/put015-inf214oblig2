Sunny and the Elephant
  2007 feature film written and directed by Frederic Lepage, a French author and producer.

It tells the story of a young city boy wanting to become the mahout, the elephant driver. Despite his masters belief that only karen tribe people are born to be mahouts, the lad finally wins his heart and becomes a real mahout.

It stars Keith Chin, Simon Woods, Grirggiat Punpiputt, Gal Srikarn Nakavisut, Glen Chin, Xuen Dangars, Siriyakorn Pukaves, Raymond Tsang, and many other famous Asian actors. Music by Joe Hisaishi.

Sunny and the Elephant was presented at the American Film Market in 2007, and at the Bangkok International Film Festival in September 2008. It was released worldwide in 2008.

==External links==

* 

 
 


 