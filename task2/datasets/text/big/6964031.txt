Stumble
 
 
{{Infobox film name     = Stumble image          = director       = Prakash Belawadi| writer         = starring       = Ananth Nag  Suhasini Mani Ratnam producer       = distributor    = cinematography = editing        = released       = 2003 runtime        = language  English
|music          = preceded_by    =
}}
Stumble is Prakash Belawadis debut film. It won the Indian National Film Award for Best Feature Film in English in 2003.  It depicts the new economy, the Dot-com bubble|dot-com bust, stock market scams, mutual funds, and voluntary retirement.
 Suhasini are in the lead rôles, alongside Priya Ganapati and Allen Mandonca. Belawadi found his cast through the theatre world of Bangalore,  and was wholly filmed there  . Jesu Domnic and Shyam Nanjundaiah were Editors for this movie. One of the editors Shyam Nanjundaiah also played a small part in the film, appearing alongside Ananth Nag for a brief scene.

The movie was featured at the international technical festival, Pragyan at NIT Trichy in January 2006.  It was also screened at the Mumbai National Film Festival. 

==References==
 
 

 
 
 


 