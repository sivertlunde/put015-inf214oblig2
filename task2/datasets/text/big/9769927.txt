Mala Época
{{Infobox film
| name           = Mala época
| image          = Malaepocaposter.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Mariano De Rosa Rodrigo Moreno Salvador Roselli Nicolás Saad
| producer       = Mario Santos
| writer         = Mariano De Rosa Rodrigo Moreno Salvador Roselli Nicolás Saad
| narrator       =
| starring       = Daniel Valenzuela
| music          = Mono Cieza
| cinematography = Javier Julia Lucas Schiaffi
| editing        = Alejandro Brodersohn Guillermo Grillo Pablo Trapero
| distributor    = Production Company: Universidad del Cine
| released       =  
| runtime        = 110 minutes
| country        = Argentina Spanish
| budget         =
}} Argentine drama film. The film was executive produced by Mario Santos. 

This dramatic anthology of four episodes represents the collaborative effort of Mariano De Rosa, Rodrigo Moreno, Salvador Roselli, and Nicolás Saad. Each wrote and directed an episode. All are graduates of Argentinas primary film school, the Universidad del Cine, which helped fund the film.

The film offers an unflattering look at Argentine society as it prepares to enter the 21st century.

==Plot==
The picture has four vignettes and all of them take place in the late 1990s in Buenos Aires during political elections.

The Wish: centers on a poor boy from the country who finds success in the fast city by participating in one of its many illegal operations.

Life and Works: follows a band of Paraguayan bricklayers as they try to reestablish a sense of cultural pride and community after meeting a woman whom one of them believes is the Virgin Mary.

Hard Times: follows a teenage outcast and his efforts to find romance with an upper-class Buenos Aires girl.

Comrades: the sound recorder of a political campaign finds himself falling for the candidates  girlfriend.

==Cast==
* Pablo Vega as Oscar
* Daniel Valenzuela as Omar
* Nicolás Leivas as Santiago
* Diego Peretti as Antonio
* Virginia Innocenti as Carmen
* Carlos Roffé as Carlos Brochato
* Florencia Bertotti as Connie
* Alberto Almada as Jaime

==Distribution==
The film was first presented at the Mar del Plata Film Festival on November 13, 1998.  It opened wide in the country on January 1, 1999.

It also competed in the 1998 Torino International Festival of Young Cinema, and was nominated for the Prize of the City of Torino.

==Awards==
Wins
*   Prize, Best Latin-American Film, for its original conception that combines various aspects of contemporary life expressing the young filmmakers point of view; Special Mention; both for Mariano De Rosa, Rodrigo Moreno, Salvador Roselli, and Nicolás Saad; 1998.
* Toulouse Latin America Film Festival: Audience Award; Nicolás Saad, Mariano De Rosa, Salvador Roselli, and Rodrigo Moreno; 1999.

Nominations
* Torino International Festival of Young Cinema, Torino, Italy: Prize of the City of Torino, Best Film - International Feature Film Competition, Nicolás Saad, Mariano De Rosa, Salvador Roselli, and Rodrigo Moreno; 1998.
* Argentine Film Critics Association Awards: Silver Condor, Best First Film, Mariano De Rosa, Rodrigo Moreno, Salvador Roselli and Nicolás Saad; Best Supporting Actor, Carlos Roffé; 2000.

==References==
 

==External links==
*  
*   at the cinenacional.com  
*   ("Hard Times" segment of film)

 
 
 
 
 
 