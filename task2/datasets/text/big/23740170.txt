Gandharvam
{{Infobox film
| name           = Gandharvam
| image          = Gandharvam poster.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Sangeeth Sivan
| producer       = Suresh Balaje
| writer         = Dennis Joseph
| narrator       =  Kanchan
| music          = S. P. Venkatesh
| cinematography = Santosh Sivan
| editing        = Sreekar Prasad
| studio         = 
| distributor    = 
| released       = 1993
| runtime        = 155 min
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Kanchan appearing in lead roles along with Jagathi Sreekumar, Devan (actor)|Devan, Vijayakumar (actor)|Vijayakumar, Prem Kumar and Kaviyoor Ponnamma. A romantic film packed with fun and adventure, this film was a super hit in theaters.

==Plot==
Sam Alexander (Mohanlal), a fun loving automobile workshop cum garage owner is more keen in scripting and directing plays than running his business. A theater addict, Sam, along with his friends, Pappu Mesthiri (Jagathi Sreekumar), Mammoonju (Kunjan), and Preman (Premkumar), is now working on Shakuntahlam, the famous play by Kalidas. Gracy Kutty (Kaviyoor Ponnamma), his mother is too worried with his easy going and careless attitude towards life. With one day, the main actress (Kalpana (Malayalam actress)|Kalpana) who was supposed to play the role of Shakunthala eloping with Krishnan Kutty (Nandu), a fellow artist, Sam is busy searching for a new face to enact Shakunthala. Accidentally, one day, he meets up with Sridevi Menon (Kanchan (actress)|Kanchan), with whom he fell in love with. He attempts all methods to woo her and finally she falls in. Sam casts her as Sahkunthala in his drama and she unwillingly agrees for it without the knowledge of her parents. Sreedevi, daughter of Meledath Vishwanatha Menon, of the rich businessman in the city, thus acts in the drama, but on way back home, both Sam and Sreedevi are caught red handed by Vishnu Menon (Devan (actor)|Devan), her brother. Ravindran Nair (Vijayakumar (actor)|Vijayakumar), the I.G.of police, and friend of Menon had plans of getting his son  Rajkumar (Kasan Khan) married to Sreedevi, so that he could own up a huge part of the business empire. With the help of Ravindran Nair, Sam is beaten up by police. Once out of jail, both Sam and Sreedevi elopes away and spend a few days away from the worries. In mean time, the duo are again caught by  Menon. Sam is arrested for kidnapping Sreedevi and is jailed. At jail, he comes to know that Sreedevi is pregnant with his kid, but has no way to save the kid. Upon knowing that Sreedevi is pregnant, she is transferred to a distant place. Once, she gives birth to a baby boy, the attitude of Menon and Vishnu changes and they started looking upon him as their heir. But, sensing danger, Ravindran Nair  sends Rajkumar to kidnap and kill the kid. Vishnu realizes the plan of Rajkumar and tries to save his sister and kid. In meantime, Sam escapes from jail and reaches out the house where Sreedevi stays. She attempts to commit suicide in bid to escape from Kumar, but is saved miraculously by Sam, who makes a dramatic entry. He defeats Kumar and saves the kid. Menon and Vishnu happily get both united.

==Trivia==
 Yodha and Daddy in 1992. Nirnayam in 1995. Kanchan was finalized. This is the only Malayalam film in her career. Vijayakumar also made his debut in Malayalam with Gandharvam. Shankar played a guest role as Chandran

==Cast==
{| class="wikitable" border="1"
! Actor || Role
|-
| Mohanlal 
| Samuel Alexander/ Sam
|- Kanchan
| Sreedevi Menon
|- Devan
| Meleveetil Vishnu Menon
|-
| Kaviyoor Ponnamma
| Graceykutty
|-
| Shanthi Krishna
| Lakshmi
|- Vijayakumar
| IG Ravindran Nair
|-
| Jagathi Sreekumar
| Pappu Mesthiri
|- Kunchan
| Mammunju
|-
| Prem Kumar
| Preman
|- Shankar
| Chandran
|-
| Mukesh Rishi
| Rangan
|-
| Subair
| Police Inspector Soman
|-
| Geetha Vijayan
| Sony
|- Nischal 
|- Hari (actor)|Hari  Advocate
|- Aboobacker
|Abdul Salim/Abookka
|- Mohan Jose
|
|- Nandhu
|
|- Krishnankutty Nair Krishnankutty Nair
|
|- Gazal Khan
|
|- Kalpana (Malayalam Kalpana
|
|}

==Soundtrack==

{| class="wikitable" border="1"
! Song || Playback || Lyrics || Duration
|-
| Athire Ninmukham... 
| K. S. Chithra Kaithapram
| 
|-
| Maliniyude Theerangal... Sujatha
| Kaithapram
| 
|-
| Omane Ninmukham... Yesudas
| Kaithapram
|
|-
| Pranaya Tharangam...
| Yesudas, K. S. Chithra Kaithapram
|
|-
| Nenjil Kanjabanam...
| S. P. Balasubrahmanyam
| Kaithapram
|
|}

==Reception==

The film had a tremendous initial, and ended up as a boxoffice hit.  

==References==
 

==External links==
*  

 
 
 