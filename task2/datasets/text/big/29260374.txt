Charithram
 
{{Infobox film|
  name           = Charithram|
  image          = CharithramMalayalam1.jpg|
  director       = G. S. Vijayan|
  writer         = S. N. Swamy| Rahman Lizy Lizy Shobhana|
  cinematography = |
  editing        = |	 
  released       =  |
  runtime        = |
  budget         =  |
  gross          =  |
  country        = India |
  language       = Malayalam |
  music          =  Rajamani M. G. Radhakrishnan|
  studio         = Suryodaya Creations |
  
}} Malayalam thriller Rahman in Pappu in other pivotal roles. It is written by S. N. Swamy and directed by G. S. Vijayan in his directional debut. {{cite web|title=Charithram
 Movie details|url=http://www.malayalachalachithram.com/movie.php?i=2200|publisher=malayalachalachithram|accessdate=2014-11-28|}} 

==Plot==
Philip Manavaalan (Mammootty) is the owner of a financing company in Kerala and the movie begins with his wedding preparations.  After the wedding, Philips wife, Cecily (Shobhana) is curious about Raju (Rahman), Philips dead brother and so Philip relates the story of Raju to Cecily.

Raju was young, vibrant and a man of many talents.  However, he soon fell prey to the world of drugs and started extracting money to buy drugs from his brother by hook or by crook.  Philip, upon learning of his brothers drug problems, admitted him into a rehab facility.  Raju appeared to recover and was then sent to Madras (Tamil Nadu) for further studies.  On one of his visits to Chennai, Philip realised that Raju had gone back to his old ways and was worse than ever before. One night, Raju stole some money and absconded with Philips car .  Philip came to know of this later when he received a call informing him that his car had met with an accident and the cars driver had perished in the explosion.  Philip performed the last rites on his brothers body, which was burnt beyond recognition, in Madras itself and returned to Kerala.  Cecily sympathises with her husband and soon gets over this sad incident.

However, there is a twist in the story, when Raju returns home and surprises everyone.  Mammootty has a hard time believing that this man is his brother.He performs many tests to prove whether the man is really his brother Raju.He is assisted by his friends in all these tests..But every test yields positive results.Soon almost everyone is convinced that it was indeed Raju who had come back.However Mammootty is still doubtful.Meanwhile Renu(Lizy) the daughter of Philips friend Cherian (Janardhanan) a police officer,who had been bethrothed to Raju,convinced that Raju had come back begins an affair with him.Philip sees this and informs Cherian about it.Cherian talks to his daughter trying to convince her what a mistake it would be if it is not Raju.But Renu is adamant and expresses her belief that Raju had returned and also tells that she will marry none but Raju.So, Janardhanan visits Mammootty along with their common friend Murali who is a lawyer to propose Renu for Raju.But Mammootty denies saying that he doesnt believe it is his brother who had come back.Everyone gets frustrated that Philip still wont accept Raju in spite of the positive results of all the tests conducted before.At last Cherian decides to perform a scientific test to prove the truth.He goes to Chennai where Rajus body had been cremated and excavates the remains.He takes a picture of the skull and a picture of Raju and superimposes the two.It doesnt match!..Upon reaching his house,he calls Murali and Jagathy Sreekumar to inform them about this. They go to his house and wait for Mammootty to come.He comes soon after in the hope that Cherian has got evidence to prove that Raju was dead and it was an impostor who had come.But when he got there,he finds Cherian in a happy state.Cherian passes on the information to Philip and shows him the super-imposed picture that doesnt match.But still he is not convinced.When questioned why,he replies that Raju died lying in his hands.Cherian asks him to explain.Philip then reveals that  Raju had not died in a car accident as he said to everyone.The day he died,Raju stole money from Philips cupboard and tried to escape with it.Philip stopped him and asked him where he was going.Raju rudely said that he was leaving as he could no longer live with Philip.He took the money so that he could live the way he wanted to.Philip begged Raju not to go and ruin himself in bad company.Raju did not listen and tried to go but Philip held him back.Raju pushed him away.In anger,Philip slapped Raju who lost his balance and fell from the first floor to the ground,breaking his neck and head in the fall.Philip rushed down to his brother and lifted him.But Raju died soon.Shocked at what he had done by mistake,Philip cries holding his brothers body.Suddenly he realised that he will be held guilty for Rajus death.Seeing no way to prove his innocence,he decided to dispose Rajus body.

==Cast==
* Mammootty as Philipose Manavalan (Thampi) Rahman as Raju Manavalan Lizy as Renu
* Shobana as Cecily
* Jagathy Sreekumar as Shivan Kutty Janardanan as Cherian Murali
* Kuthiravattam Pappu as Krishnan
* Bobby Kottarakara
* Nandu

==References==
 

==External links==
* 

 

 
 
 
 