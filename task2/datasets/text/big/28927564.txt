The Heirloom Mystery
{{Infobox film
| name =  The Heirloom Mystery
| image =
| image_size =
| caption =
| director = Maclean Rogers
| producer = A. George Smith
| writer =  Kathleen Butler   G.H. Moresby-White
| narrator = John Robinson
| music = 
| cinematography = Geoffrey Faithfull
| editing = 
| studio = George Smith Productions
| distributor = RKO Pictures
| released = November 1936
| runtime = 69 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} British drama film directed by Maclean Rogers and starring Edward Rigby, Mary Glynne and Gus McNaughton.  After being secretly commissioned by a man to create a replica piece of furniture so he can sell the valuable original without his wife knowing, Charles Marriotts firm find themselves under investigation.

It was made at the Nettlefold Studios in Walton-on-Thames|Walton, as a quota quickie for distribution by RKO Pictures. 

==Cast==
* Edward Rigby as Charles Marriott
* Mary Glynne as Lady Benton
* Gus McNaughton as Alfred Fisher
* Marjorie Taylor as Mary John Robinson as Dick Marriott Martin Lewis as Sir Arthur Benton
* Kathleen Gibson as Doris
* Louanne Shaw as Millie
* Bruce Lester as Alf Dene
* H.F. Maltby as Mr. Lewis
* Michael Ripper as Minor role

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 

 