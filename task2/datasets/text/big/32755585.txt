Frogs for Snakes
{{Infobox film
| name           = Frogs for Snakes
| image          = Frogs for Snakes.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Amos Poe
| producer       = Phyllis Freed Kaufman Larry Meistrich Daniel J. Victor
| screenplay     = Amos Poe
| narrator       = 
| starring       = Barbara Hershey Robbie Coltrane John Leguizamo Ron Perlman
| music          = Lazy Boy
| cinematography = Enrique Chediak
| editing        = Jeff Kushner
| studio         = Shooting Gallery Rain Film
| distributor    = Artisan Entertainment
| released       = 20 February 1998 (Berlin International Film Festival|B.I.F.F.) 14 August 1998  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $20,693 (USA) 
| preceded_by    = 
| followed_by    = 
}}
Frogs for Snakes is a 1998 film written and directed by Amos Poe.

==Plot==
Out of work actress Eva (Barbara Hershey|Hershey), pays her way by working as a waitress at a diner in Manhattans Lower East Side owned by Quint (Ian Hart|Hart). She makes extra cash by making collections for her ex-husband, loan shark Al (Robbie Coltrane|Coltrane). The film also involves Evas new boyfriend Zip (John Leguizamo|Leguizamo), wanna-be actress Myrna (Lisa Marie (actress)|Marie), Als girlfriend Simone (Debi Mazar|Mazar), gangster Gascone (Ron Perlman|Perlman), and Als driver UB (Deblinger).
 American Buffalo, and he offers a role to UB if he will murder Zip.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Barbara Hershey || Eva Santana 
|-
| Robbie Coltrane || Al Santana
|-
| Ian Hart || Quint
|-
| John Leguizamo  || Zip
|-
| Debi Mazar || Simone
|-
| Ron Perlman || Gascone
|-
| Justin Theroux ||  Flav Santana
|-
| Clarence Williams III || Huck Hanley
|-
| Harry Hamlin || Klench
|- Lisa Marie || Myrna LHatsky
|-
| Nick Chinlund || Iggy Schmurtz
|-
| David Deblinger || UB
|-
| Zak Kerkoulas || Augie
|}

==Critical reception==
The film received mostly negative reviews.

Roger Ebert of The Chicago Sun-Times gave the film zero stars:
 

Mark Savlov of The Austin Chronicle:
 

==Soundtrack listing==
#"Subterranean Homesick Blues" by Bob Dylan
#"Uneasy Street" by Lazy Boy
#"Sweet Thing" by The Gaturs
#"Theme From Headtrader" by Lazy Boy
#"Destination Moon" by Dinah Washington
#"Delivery For Mr. Rilke" by Jeffrey Howard
#"Downtown" by Toledo Diamond
#"The Man with The Golden Arm" by Barry Adamson
#"Blood on White Shoes" by Howard Shore
#"Not a Waltz" by Wolly
#"Horns for Harry" by Jeffrey Howard Tipsy
#"Fly Poe
#"Si Mi Chiamo Mimi" from La bohème, with vocalist Luba Orgonášová and Slovak Radio Symphony Orchestra|Czecho-Slovak Radio Symphony Orchestra
#"Fattening Frogs for Snakes" by Patti Smith

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 