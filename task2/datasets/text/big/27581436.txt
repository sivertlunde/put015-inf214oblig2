Ruby Cairo
{{Infobox film
| name           = Ruby Cairo
| image          = Ruby Cairo 2.jpg
| image_size     =
| alt            =
| caption        = Theatrical movie poster
| director       = Graeme Clifford
| producer       = Haruki Kadokawa Lloyd Phillips Hiroshi Sugawara
| writer         = Robert Dillon Michael Thomas
| narrator       = Jack Thompson John Barry
| cinematography = László Kovács (cinematographer)|László Kovács
| editing        = Caroline Biggerstaff Paul Rubell Mark Winitsky 
| studio         = Majestic Films International
| distributor    = Miramax Films
| released       =  
| runtime        = Theatrical: 90 minutes Video: 111 minutes
| country        = United States English
| budget         =
| gross          = $608,866 (USA)
}} 1992 film directed by Graeme Clifford. It stars Andie MacDowell, Liam Neeson and Viggo Mortensen.
One scene features Aleister Crowleys The Book of the Law.

==Tagline==
"Adults play the most dangerous games".

==Plot==
When Bessie Faros husband Johnny dies in a plane crash in Veracruz, Mexico, she finds that his air cargo business is deeply in the red. When she visits the airlines terminal in Veracruz, she discovers her husband was pumping large amounts of money into bank accounts all over the world. As she begins systematically recovering her husbands money, she discovers that someone else has beat her to some of the accounts. Aided by Fergus Lamb, a chance acquaintance, she goes to Cairo to find some answers. But she is being followed.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Andie MacDowell || Elizabeth "Bessie" Faro / Ruby Cairo
|-
| Liam Neeson || Dr. Fergus Lamb, Feed the World
|-
| Viggo Mortensen || John E. "Johnny" Faro
|- Jack Thompson || Ed, Feed the World Foundation Manager
|-
| Amy Van Nostrand || Marge Swimmer, Faro Neighbor
|-
| Pedro Gonzalez Gonzalez ||  Uncle Jorge
|-
| Paul Spencer || Johnny Faro (Boy)
|-
| Chad Power || Niles Faro
|}

==Box office and video==
The film fared poorly at the box office, grossing only $608,866 in the United States. It was retitled Deception when it was recut and released on video with 21 additional minutes of footage.   As of 2010, the film is available on Amazon.com and on Netflix.  

==Production== director was stoned throughout filming. 

==Soundtrack== John Barry You Belong to Me performed by Patsy Cline.

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 