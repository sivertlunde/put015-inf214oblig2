Accepted
 
{{Infobox film
| name           = Accepted
| image          = Acceptedposter.jpg
| caption        = Theatrical release poster
| director       = Steve Pink
| producer       = {{Plainlist|
* Tom Shadyac
* Michael Bostick
}}
| screenplay     = {{Plainlist|
* Adam Cooper
* Bill Collage
* Mark Perez
}}
| story          = Mark Perez
| starring       = {{Plainlist|
* Justin Long
* Blake Lively
* Anthony Heald
* Lewis Black
 
}}
| music          = David Schommer
| cinematography = Matthew F. Leonetti
| editing        = Scott Hill
| studio         = Shady Acres Entertainment Universal Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $23 million 
| gross          = $38,505,009 http://www.boxofficemojo.com/movies/?id=accepted.htm 
}}
 Wickliffe and Orange in California at Chapman University. This film was later remade in Bollywood as F.A.L.T.U starring Jackky Bhagnani.

==Plot==
<!-- NOTE: The plot summary is currently 632 words long, which is within the permitted length. Please do not alter it or make it unnecessarily longer without first discussing and gaining consensus on the articles talk page. The plot summary does NOT intend to reproduce the experience of watching, nor is it meant to cover EVERY detail. It should serve as an overview of the major plot elements and events. PER WP:FILMPLOT, PLOT SUMMARIES FOR FEATURE FILMS AND TELEVISION SERIES SHOULD BE BETWEEN 400 AND 700 WORDS. See "Wikipedia:How to write a plot summary" (WP:PLOTSUM) for general guidelines.

In addition, do NOT put the fan site link (http://www.southharmoninstituteoftechnology.org) to the article. Persistent spammers may be BLOCKED from editing and have their websites BLACKLISTED, preventing anyone from linking to them from all Wikimedia sites as well as potentially being PENALIZED by search engines. See "Wikipedia:Spam" (WP:SPAM)for general guidelines.-->
 fake IDs. SAT exam. To make the "college" seem legitimate, Bartleby convinces Sherman to create a functional website for the school.
 Jeremy Howard) wishes to study.

Bartleby creates a school newspaper (the S.H.I.T. Rag), invents a mascot (the S.H.I.T. Sandwiches), and throws themed parties. Meanwhile, the dean of Harmon College, Richard Van Horne (Anthony Heald), makes plans to construct the Van Horne Gateway, a park-like "verdant buffer zone" adjacent to the college that the dean hopes will "keep knowledge in and ignorance out". He dispatches Hoyt Ambrose (Travis Van Winkle) to free up the nearby properties, but when Bartleby refuses to relinquish the lease for the South Harmon property, Hoyt sets to work trying to reveal the college as a fake. The dispute turns personal, since Bartleby has been vying for the affections of Hoyts ex-girlfriend, Monica Moreland (Blake Lively). Hoyt exposes South Harmon as a fake institution through Sherman, who is attempting to join Hoyts fraternity as a legacy, but is constantly humiliated and abused by them. After debasing Sherman once more, the fraternity violently forces him to hand over all the files he has created for South Harmon.

Hoyt contacts all the students parents, and with Van Horne, he reveals the school as a sham. Soon after, the school is forced to close, but Sherman, who has had enough with Harmon Colleges corruption, files for accreditation on behalf of South Harmon, giving Bartleby a chance to make his college legitimate. At the subsequent State of Ohio educational accreditation hearing, Bartleby makes an impassioned speech about the failures of conventional education and the importance of following ones own passions, convincing the board to grant his school a one-year probationary accreditation to test his new system, thus foiling Van Hornes plans. The college reopens, with more students enrolling, including Sherman and Monica. In addition, Bartleby finally earns the approval of his father, who is proud that his son now owns a college. As the film closes, Van Horne walks to his car in the parking lot, only to watch it suddenly explode. Bartleby watches in astonishment as the eccentric student from earlier makes his interest in psychokinetic explosion a reality.

==Cast==
 
* Justin Long as Bartleby Gaines 
* Jonah Hill as Sherman Schrader III
* Blake Lively as Monica Moreland
* Anthony Heald as Dean Richard Van Horne
* Lewis Black as Ben Lewis
* Travis Van Winkle as Hoyt Ambrose
* Diora Baird as Kiki
* Adam Herschman as Glen
* Maria Thayer as Rory Thayer
* Columbus Short as Hands Holloway
* Mark Derwin as Jack Gaines
* Ann Cusack as Diane Gaines
* Hannah Marks as Lizzie Gaines
* Joe Hursley as Maurice / The Ringers
* Sam Horrigan as Mike Welsh
* Robin Lord Taylor as Abernathy Darwin Dunlap Jeremy Howard as Freaky student
* Kaitlin Doubleday as Gwynn
* Ross Patterson as Mike McNaughton
* Kellan Lutz as Dwayne
* Brendan Miller as Wayne
* Ray Santiago as Princeton boy
* Greg Sestero as a Frat boy (uncredited)
* Ned Schmidtke as  Dr. J. Alexander
* Artie Baxter  as  Mike Chambers 
* Jim OHeir   as Sherman Schrader II
* Darcy Shean  as Mrs. Schrader  
 

==Release==
 

===Critical response===
Accepted was released to mixed reviews. Rotten Tomatoes gives the film a rating of 37% based on reviews from 111 critics. The sites consensus states that "like its characters who aren’t able to meet their potential, Accepted s inconsistent and ridiculous plot gets annoying, despite a few laughs".    Metacritic gives the film a score of 47%, based on reviews from 27 select critics.    It received 3 out of 5 stars from Allmovie.     The film has since gone on to become a cult classic.

 

===Box office=== World Trade Step Up s second. 

By the end of its run, on October 19, 2006, Accepted had grossed $36,323,505 domestically and $2,181,504 internationally, with a worldwide total of $38,505,009. 

==Home media==
  
The film was released on DVD on November 14, 2006, in both widescreen and fullscreen formats. The DVD came supplied with deleted scenes and a gag reel.  Accepted was also one of the films released on HD DVD format before the format was discontinued.

 

==Soundtrack==
{{Infobox album
| Name = Accepted (Original Motion Picture Soundtrack)
| Type = Soundtrack
| Artist = Various
| Cover = Accpetedalbum.jpg
| Released = August 8, 2006
| Length =46:03
| Label = Shout Factory
| Reviews = 
}}
{{Track listing
| collapsed       = yes
| headline        = Accepted (Original Motion Picture Soundtrack)
| extra_column    = Artists
| total_length    = 46:03

| all_writing     = 
| all_lyrics      = 
| all_music       =

| writing_credits = 
| lyrics_credits  = 
| music_credits   =

| title1          = U-Mass 
| note1           = 
| writer1         = 
| lyrics1         = 
| music1          = 
| extra1          = Pixies
| length1         = 3:02

| title2          = Gravity Rides Everything 
| note2           = 
| writer2         = 
| lyrics2         = 
| music2          = 
| extra2          = Modest Mouse
| length2         = 4:20

| title3          = The Hives Declare Guerre Nucleaire 
| note3           = 
| writer3         = 
| lyrics3         = 
| music3          = 
| extra3          = The Hives
| length3         = 1:35

| title4          = Bole 2 Harlem 
| note4           = 
| writer4         = 
| lyrics4         = 
| music4          = 
| extra4          = Bole 2 Harlem
| length4         = 4:09

| title5          = Eleanor Rigby 
| note5           = Originally by The Beatles
| writer5         = 
| lyrics5         = 
| music5          = 
| extra5          = David Schommer featuring David Jensen 
| length5         = 3:57
 TKO 
| note6           = 
| writer6         = 
| lyrics6         = 
| music6          = 
| extra6          = Le Tigre
| length6         = 3:26
 Where Do I Begin 
| note7           = 
| writer7         = 
| lyrics7         = 
| music7          = 
| extra7          = The Chemical Brothers feat. Beth Orton
| length7         = 6:31

| title8          = Shermans Way 
| note8           = 
| writer8         = 
| lyrics8         = 
| music8          = 
| extra8          = David Schommer
| length8         = 2:12

| title9          = Keepin Your Head Up 
| note9           = 
| writer9         = 
| lyrics9         = 
| music9          = 
| extra9          = The Ringers
| length9         = 1:50

| title10         = Dont You (Forget About Me) 
| note10          = Originally by Simple Minds
| writer10        = 
| lyrics10        = 
| music10         = 
| extra10         = David Schommer 
| length10        = 1:58

| title11         = Holiday 
| note11          = 
| writer11        = 
| lyrics11        = 
| music11         = 
| extra11         = Weezer
| length11        = 3:34

| title12         = Let The Drummer Kick 
| note12          = 
| writer12        = 
| lyrics12        = 
| music12         = 
| extra12         = Citizen Cope
| length12        = 4:17

| title13         = To Be Young (Is To Be Sad, Is To Be High) 
| note13          = 
| writer13        = 
| lyrics13        = 
| music13         = 
| extra13         = Ryan Adams
| length13        = 3:05

| title14         = You Think We Suck
| note14          = 
| writer14        = 
| lyrics14        = 
| music14         = 
| extra14         =  Ape Fight
| length14        = 2:19
}}

Other songs not included on the soundtrack: Close to Me" by The Cure plays in Bartlebys room whenever the disco ball comes down
* "Holiday (Green Day song)|Holiday" by Green Day. There has been some confusion over the fact that Weezer and Green Day each have a song by the title "Holiday" in the film, but only the Weezer song is on the official soundtrack.
* "Blitzkrieg Bop" by The Ramones. Played at a party held at South Harmon, and Bartleby sings the majority of it on stage. The DVD also features a music video of the song featuring the cast and crew.
* "Blitzkrieg Bop" by Justin Long with The Ringers
* "Dont Stop (Fleetwood Mac song)|Dont Stop" by Fleetwood Mac is played during the high school graduation party.
* "Im Better" by Scott Thomas
* "Sweet Confusion" by Divine Right
* "Spotlight" by The Ringers
* "Walkin the Walk" by The Daniel May Quartet
* "String Quartet in G, Opus 18" by FLUX Quartet
* "Bridges and Balloons" by Joanna Newsom

==See also==
 
* Camp Nowhere
* F.A.L.T.U.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 