Assassin for Hire
{{Infobox film
| name           = Assassin for Hire
| caption        = 
| image	=	Assassin for Hire FilmPoster.jpeg Michael McCarthy
| producer       = Julian Wintle
| writer         = Rex Rienits
| screenplay     = 
| story          = 
| based on       =   Ronald Howard Katharine Blake John Hewer
| music          = Ronnie Emanuel
| cinematography = Robert LaPresle
| editing        = Eric Hodges
| studio         = Anglo-Amalgamated
| distributor    = Anglo-Amalgamated Film Distributors (UK)   William H. Horne & David Dietz (US)
| released       =   
| runtime        = 67 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Michael McCarthy Ronald Howard Katharine Blake.  Its plot follows a contract killer who becomes stricken with remorse when he is led to believe he has murdered his brother.

==Plot== Italian heritage, works as a professional contract killer in order to pay for his gifted younger brothers violin lessons so that he can escape from a life of poverty and crime. A series of mistakes lead him to wrongly believe he has killed his brother, and he confesses his crimes to the police. 

==Cast==
* Sydney Tafler - Antonio Riccardi  Ronald Howard - Detective Inspector Carson  Katharine Blake - Maria Riccardi 
* John Hewer - Giuseppe Riccardi 
* June Rodney - Helen Garrett 
* Gerald Case - Detective Sergeant Stott 
* Reginald Dyson - Josef Meyerling 
* Sam Kydd - Bert 
* Ian Wallace - Charlie  Martin Benson - Catesby 
* Ewen Solon - Fred

==Production==
It was the first feature film made by Anglo-Amalgamated. It was made at Merton Park Studios from a screenplay by Rex Rienits which he based on the script of a television film Assassin for Hire which he had written the previous year.  It was intended as a supporting feature, although it may have been shown as a headline feature in some cinemas.

==References==
 

==Bibliography==
* Chibnall, Steve & McFarlane, Brian. The British B Film. Palgrave MacMillan, 2009.

==External links==
*  

 

 
 
 
 
 


 