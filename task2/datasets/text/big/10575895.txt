Love (1971 film)
{{Infobox film
| name = Love
| image	=	Love FilmPoster.jpeg
| caption =
| director =Károly Makk
| producer =
| writer = Péter Bacsó Tibor Déry (novel)
| starring = Lili Darvas Mari Törőcsik
| cinematography = János Tóth
| editing = György Sívó
| distributor = Ajay Film Company (USA)
| released =  
| runtime = 84 minutes
| country = Hungary
| language = Hungarian
| budget =
| gross =
}}
 Jury Prize Best Foreign Language Film at the 44th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Lili Darvas - Az öregasszony
* Mari Törőcsik - Luca
* Iván Darvas - János
* Erzsi Orsolya - Irén
* László Mensáros - Az orvos
* Tibor Bitskey - Feri (as Bitskei Tibor)
* András Ambrus - Börtönőr
* József Almási - Tanár
* Zoltán Bán - Borbély
* Éva Bányai - Feriék szolgálója
* Ágnes Dávid - Feriék szolgálója
* Mária Garamszegi - Feriék szolgálója
* Alíz Halda - Tanárnő
* Magda Horváth - Kissné
* Nóra Káldi - Az öregasszony fiatalon (as Káldy Nóra)

==Historical background==
Károly Makk tells the story of a young Hungarian woman whose husband has been arrested by the secret police and who eases the last months of his mother with the tale that her son is in America. In 1953 after the death of Soviet premier Joseph Stalin many arrested people were released in Hungary. The film "Love" won the Jury Prize at the 1971 Cannes Film Festival and is one of Derek Malcolm / The Guardians Top 100 Films of the 20th Century.

==See also==
* List of submissions to the 44th Academy Awards for Best Foreign Language Film
* List of Hungarian  submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 