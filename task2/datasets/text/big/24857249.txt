Broken Springs
{{Infobox film
| name           = Broken Springs
| image          =
| director       = Neeley Lawson
| producer       = Neeley Lawson
| writer         = Neeley Lawson
| starring       = Teague Quillen Travis Moody Brandon Jenkins Shannon Wallen
| cinematography = Ron Loepp
| editing        = Neeley Lawson
| music          = Jake McMurray Bryan Tanori Chris Ingle
| distributor    =
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} independent horror feature effort.   It stars Teague Quillen, Jake Lawson and Shannon Wallen.  The movie was filmed in late fall of 2008, mainly in Gate City, Virginia, United States|U.S. and Rogersville, Tennessee, United States|U.S.. 

==Plot==
The movie centers on three high school students whose world is turned upside down by tainted moonshine which turns everyone who drinks it into a flesh eating zombie. It does not take long for the whole town to be overrun.

==Cast==
 
 
* Teague Quillen as Ken
* Travis Moody as Dave
* Brandon Jenkins as Brandon
* Shannon Wallen as Billy Jack
* Sean Loepp as Ron
 
* Scott Parker (actor)|W. Scott Parker III as Cecil
* Jake Lawson as Flea Market Preacher
* Hunter Roberts as Cop
* Debbie Green as Kens Mom
* Jeff Bobo as Danny
* Brent McConnell as Drunk
 

==Release== The Spooky Movie Film Festival (aka Washington D.C. International Horror Festival),   and the Telluride Horror Show Film Festival.   A teaser trailer was released on YouTube on October 26, 2009. 

In 2012, the film was distributed under the title 101 Zombies and became available for rent on YouTube, Charter Cable On-Demand and Amazon.

==Soundtrack==
The Soundtrack featured songs from The Flow of Opinion and Jake McMurray. 

==Critical reception==
Variety (magazine)|Variety wrote that the film borrowed "equally from George A. Romero and Joe Dante for its wit and politics", and that "fans exhausted with big-budget zombie movies will be refreshed" by the film. 

OC Weekly reviewer Matt Coker remarked, "How can one not love a film with undead, zombie” and bastards in the same title?",  "barely" recommended the film, writing that as it acts as an "homage of sorts" to other low/no budget zombie films, and has "just enough humor and ironic stereotypes to make up for the poor acting, bad lighting and looooooong build up to the inevitable conclusion". 

==References==
{{reflist|refs=

   

   

   

   

   

   

   

   

   

   

   

   

}}

==External links==
*  
*  

 
 
 
 
 
 
 
 
 