Three... Extremes
{{Infobox film
| name           = Three... Extremes
| image          = Three... Extremes film.jpg
| caption        = Theatrical release poster
| director       = Fruit Chan Park Chan-wook Takashi Miike
| producer       = Ahn Soo-hyun Peter Ho-sun Chan Fumio Inoue Naoki Sato Shun Shimizu
| writer         = Dumplings:   Box: Bun Saikou Haruko Fukushima
| starring       = Bai Ling Tony Leung Ka-fai Lee Byung-hun Im Won-hee Kyoko Hasegawa Atsuro Watabe
| music          = Chan Kwong-wing Kōji Endō Peach Present
| cinematography = Chung Chung-hoon Christopher Doyle Koichi Kawakami
| editing        = 
| distributor    = 
| released       =  
| runtime        = 118 minutes
| country        = China Japan South Korea Mandarin Japanese Japanese Korean Korean
| budget         = 
}}
Three... Extremes ( ) is a 2004 international East Asian horror film collaboration consisting of three segments by three directors from three countries. It is a sequel to, and follows the concept of Three (2002 film)|Three (2002), this time with more established directors. 

==Films==
 
===Dumplings=== Hong Kong feature length version of this short.

===Cut=== South Korean film directed by Park Chan-wook which tells the story of a successful film director and his wife who are kidnapped by an extra from his own films, who forces the director to play his sadistic games. If he fails, his wifes fingers will be chopped off one by one every five minutes. 

===Box=== Japanese film by Takashi Miike about a softly spoken young woman who has a bizarre recurring nightmare about being buried in a box in the snow. Searching for her long lost sister, she realizes her dreams and reality may possibly be connected.

==Cast==
===Dumplings===
* Miriam Yeung as Mrs Lee
* Bai Ling as Mei
* Pauline Lau as Lees maid 
* Tony Leung Ka-fai as Lee
* Meme Tian as Connie

===Cut===
* Lee Byung-hun as Director
* Im Won-hee as Stranger
* Kang Hye-jung as Directors wife
* Yum Jung-ah as actress in vampire role
===Box===
* Kyoko Hasegawa as Kyoko
* Atsuro Watabe as Yoshii/Higata
* Mai Suzuki as Young Kyoko
* Yuu Suzuki as Young Shoko

==Dumplings theatrical==
Dumplings (film)|Dumplings was extended and turned into a full length theatrical film that was released into British cinemas by Tartan Films in the spring of 2006.

==Reception==
 
Three...Extremes received mixed reviews, and holds a score of 66 out of 100 at review aggregator Metacritic.  Roger Ebert awarded it three and a half out of four stars and called it "deeply, profoundly creepy." 

==Box office==
 
The film was released on November seventeenth, 2005 in nineteen North American theatres.  It grossed $36,414 ($1,916 per screen) in its opening week-end, and its final gross stands at a modest $77,532.

==References==
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 