The Hateful Eight
 
{{Infobox film
| name           = The Hateful Eight
| image          = The Hateful Eight film poster.png
| alt            = 
| caption        = Teaser poster
| director       = Quentin Tarantino
| producer       = {{Plain list |
* Richard N. Gladstein 
* Shannon McIntosh 
* Stacey Sher
}}
| writer         = Quentin Tarantino
| starring       = {{Plain list |
* Samuel L. Jackson
* Kurt Russell
* Jennifer Jason Leigh
* Walton Goggins
* Tim Roth
* Demián Bichir
* Michael Madsen
* Bruce Dern
* Channing Tatum
}} Robert Richardson
| editing        = Fred Raskin
| studio         = 
| distributor    = The Weinstein Company
| runtime        = 
| country        = United States
| language       = English
| budget         = $44 million 
| gross          = 
}}
 western film Civil War in Wyoming and it revolves around eight westerners who seek refuge in a stagecoach stopover on a mountain pass during a blizzard.

The films development was announced by Tarantino in November 2013. However, after the script was leaked in January 2014, Tarantino decided to cancel the movie and publish the script as a novel instead. After directing a live read of the leaked script at the United Artists Theater, in Los Angeles, Tarantino said that he had changed his mind and was going to film The Hateful Eight. Filming officially began on January 23, 2015 in Telluride, Colorado|Telluride, Colorado. The film is set for a fall 2015 release by The Weinstein Company.

== Cast ==
* Samuel L. Jackson as Major Marquis Warren aka "The Bounty Hunter"
* Kurt Russell as John Ruth aka "The Hangman"
* Jennifer Jason Leigh as Daisy Domergue aka "The Prisoner"
* Walton Goggins as Chris Mannix aka "The Sheriff"
* Demián Bichir as Bob aka "The Mexican"
* Tim Roth as Oswaldo Mobray aka "The Little Man"
* Michael Madsen as Joe Gage aka "The Cow Puncher"
* Bruce Dern as Gen. Sanford Smithers aka "The Confederate"
* Channing Tatum as ? James Parks as O.B. Jackson
* Dana Gourrier as Minnie
* Zoë Bell as Six-Horse Judy Gene Jones as Sweet Dave
* Keith Jefferson as Charly
* Lee Horsley as Ed
* Craig Stark as ?
* Belinda Owino as Gemma

== Production ==
=== Development and script leak ===
 
 , as part of LACMAs Live Read series on April 19, 2014.]]
In November 2013, Tarantino said he was working on a new film and that it would be another Western. He stated that it would not be a sequel to Django Unchained.    On January 12, 2014, it was revealed that the film would be titled The Hateful Eight.  The production of the western would most likely have begun in the summer of 2014, but after the script for the film leaked in January 2014, Tarantino considered dropping the movie and publishing it as a novel instead.   He claimed to have given the script to a few trusted colleagues, including Bruce Dern, Tim Roth and Michael Madsen.  
 James Parks, Walton Goggins, Zoë Bell, James Remar, Dana Gourrier and the first three actors to be given the script before the leak, Bruce Dern, Tim Roth and Michael Madsen.   
=== Influences === The Virginian, The High Chaparral,” Tarantino said. “Twice per season, those shows would have an episode where a bunch of outlaws would take the lead characters hostage. They would come to the Ponderosa and hold everybody hostage, or go to Judge Garth’s place — Lee J. Cobb played him — in The Virginian and take hostages. There would be a guest star like David Carradine, Darren McGavin, Claude Akins, Robert Culp, Charles Bronson or James Coburn. I don’t like that storyline in a modern context, but I love it in a Western, where you would pass halfway through the show to find out if they were good or bad guys, and they all had a past that was revealed. “I thought, ‘What if I did a movie starring nothing but those characters? No heroes, no Michael Landons. Just a bunch of nefarious guys in a room, all telling backstories that may or may not be true. Trap those guys together in a room with a blizzard outside, give them guns, and see what happens.’ ” 

=== Pre-production === James Parks, Gene Jones, Keith Jefferson, Lee Horsley, Craig Stark and Belinda Owino. 

=== Filming ===
The shooting was set to begin in early 2015 after being pushed back from November 2014.  In early September, the filming was set to begin in January 2015.  On September 26, 2014, the Denver Post reported that the state of Colorado had signed to fund the films production with $5 million, so it was announced that the complete film would be shot in southwest Colorado.    A 900-acre, high-mesa ranch had been issued to the production for the filming. There was a meeting on October 16, which the countys planning commission would plan to use a permit for the construction of a temporary set.  Principal photography was reportedly began on December 8, 2014, in Colorado on the Schmid Ranch near Telluride, Colorado|Telluride,    but later on January 23, 2015, The Weinstein Company officially announced that the filming had begun. 

===Cinematography=== Mutiny on the Bounty (1962) and  Its a Mad, Mad, Mad, Mad World (1963).

== Release ==

===Distribution===
On September 3, 2014, The Weinstein Company acquired the worldwide distribution rights to the film for a fall 2015 release.    On February 9, 2015, The Hollywood Reporter revealed that TWC would sell the film worldwide but Tarantino was making deal and demanding to personally approve the global distributors for the film. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 