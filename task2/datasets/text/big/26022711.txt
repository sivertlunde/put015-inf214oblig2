Tora-san Goes to Vienna
{{Infobox film
| name = Tora-san Goes to Vienna
| image = Tora-San Goes to Vienna.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer = Kiyoshi Shimizu Kiyoaki Kurosu
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Keiko Takeshita
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 109 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  is a 1989 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Keiko Takeshita as his love interest or "Madonna".  Tora-san Goes to Vienna is the forty-first entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
During his wandering throughout Japan, Tora-san meets a suicidal man. He travels with the man to Vienna, but winds up homesick for Japan.    

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Keiko Takeshita as Kumiko Egami
* Akira Emoto as Heiba Sakaguchi
* Keiko Awaji as Madam
* Shimojo Masami as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hidetaka Yoshioka as Mitsuo Suwa
* Hisao Dazai as Boss (Umetarō Katsura)

==Critical appraisal==
The German-language site molodezhnaja gives Tora-san Goes to Vienna three and a half out of five stars.   

==Availability==
Tora-san Goes to Vienna was released theatrically on August 5, 1989.  In Japan, the film was released on videotape in 1996, and in DVD format in 1998, 2005, and 2008. 

==References==
 

==Bibliography==
===English===
*  
*  
*  

===German===
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 