The Boy in Blue (1919 film)
{{Infobox film
| name           = The Boy in Blue
| image          = O garoto vestido de azul 5.jpg
| caption        = Screenshot
| director       = F. W. Murnau
| producer       = Ernst Hofmann
| writer         = Edda Ottershausen
| starring       = Ernst Hofmann
| cinematography = Karl Freund Carl Hoffmann
| editing        = 
| distributor    = 
| released       =  
| runtime        = 54 minutes
| country        = Weimar Republic
| language       = Silent German intertitles
| budget         = 
}}
  silent German debut film. The film is now considered to be a lost film, though the Deutsche Kinemathek film archive possesses 35 small fragments ranging from two to eleven frames in length.      

Thomas Gainsboroughs painting The Blue Boy and Oscar Wildes novel The Picture of Dorian Gray were  inspirations for Murnau to create this film. 

==Cast==
* Ernst Hofmann – Thomas von Weerth
* Blandine Ebinger – Schöne Zigeunerin / Fair gypsy
* Margit Barnay – Junge Schauspielerin / Young actress
* Karl Platen – Alter Diener / Old servant
* Georg John – Zigeuner-Hauptmann / Gypsy commander
* Leonhard Haskel – Theaterdirektor
* Marie von Buelow – Bettlerin
* Rudolf Klix – Bobby
* Hedda Kemp – Dame im Schleier
* Hans Otterhausen – Guckkastenmann
* Hans Schaup – Der alte Dietrich / Thomas Diener

==See also==
* List of lost films

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 