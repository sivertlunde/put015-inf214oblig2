High Crime
 
{{Infobox film
| name           = High Crime
| image          = High Crime.jpg
| image_size     =
| caption        = German-language poster for High Crime.
| director       = Enzo G. Castellari
| producer       = Maurizio Amati
| writer         = Maurizio Amati Tito Carpi Enzo G. Castellari Gianfranco Clerici Vincenzo Mannino Leonardo Martín
| starring       = Franco Nero James Whitmore Delia Boccardo Fernando Rey
| music          = Guido De Angelis Maurizio De Angelis
| cinematography = Alejandro Ulloa
| editing        = Vincenzo Tomassi
| distributor    =
| released       =  
| runtime        = 100 min.
| country        = Italy Spain Italian
}} UK video title The Marseilles Connection, is a 1973 poliziottesco film directed by Enzo G. Castellari. The film stars Franco Nero, James Whitmore, Delia Boccardo and Fernando Rey.
 Italian cop Street Law (Il cittadino si ribella, 1974), The Big Racket (Il grande racket, 1976) and The Heroin Busters (La via della droga, 1977).

==Plot== Lebanese drug Daniel Martín), turns out to be a mole working for the unknown new gangsters.

Bellis boss, Commissioner Aldo Scavino (Whitmore), has put together a dossier on the citys mafia connections, but thinks that there is not enough hard evidence to take down all the gangsters from top to down. After several discussions with Belli, he finally agrees to take the dossier to the district attorney. However, he is murdered and the dossier is stolen. Belli now takes over Scavinos seat as the Commissioner and eventually finds the murderer. The murderer names Umberto Griva (Duilio Del Prete) as his boss, as Belli expected. When Grivas brother Franco (Silvano Tranquilli) is found murdered, it seems that someone with even higher political connections is trying to take over the citys drug trafficking.

Belli then starts from square one and, after a warning from Cafiero, decides to send his daughter away to a safer place. However, his daughter is soon murdered and his girlfriend Mirella (Boccardo) beat up. With a helpful hint from Cafiero, Belli finds out about a large drug smuggling operation. As Belli arrives on the scene, a shootout ensues, and Belli survives while all the criminals are killed.

==References==
 

==External links==
*  .

 

 
 
 
 
 
 
 
 
 