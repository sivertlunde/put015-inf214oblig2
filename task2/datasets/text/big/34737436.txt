Matru Ki Bijlee Ka Mandola
 
 
 
{{Infobox film
| nm             = Matru Ki Bijlee Ka Mandola
| image          = Matru Ki Bijlee Ka Mandola poster.jpeg
| alt            =
| caption        = Theatrical release poster
| director       = Vishal Bhardwaj
| producer       = Vishal Bhardwaj Fox Star Studios
| writer         = Vishal Bhardwaj  (Dialogue) 
| screenplay     = Vishal Bhardwaj Abhishek Chaubey
| story          = Vishal Bhardwaj Abhishek Chaubey Imran Khan Anushka Sharma Arya Babbar Shabana Azmi Navneet Nishan
| music          = Vishal Bhardwaj
| Lyrics         = Gulzar
| cinematography = Kartik Vijay
| editing        = A. Sreekar Prasad
| studio         = VB Pictures
| distributor    = Fox Star Studios ShowMaker Pictures
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =   http://thefilmstreetjournal.com/generalarticle.aspx?id=580  
| gross          =   (Domestic gross) 
}}
 Imran Khan and Anushka Sharma in the titular roles, while Arya Babbar and Shabana Azmi play supporting roles. The film was released worldwide on 11 January 2013.

==Plot==
  Imran Khan) known in short as Matru. Harry is a wealthy, cynical businessman who dreams of turning the Mandola village (named after his family) into a shining example of his success and a microcosm of a fledgeling economy in itself. But this dream can only be realised if the villagers agree to sell their land to the government at unfairly low rates in order for the land to be converted into a Special Economic Zone.

While being the shrewd, sophisticated and composed individual that Harry normally is, he is also quite fond of drinking. Whenever drunk, he turns into the advocate of equality and betterment of the villagers, but regrets these thoughts and deeds when sober. When he chooses not to drink he hallucinates, as a withdrawal symptom, about a pink buffalo Gulabo, the mascot of the brand of alcohol that he enjoys. The knowledge of these hallucinations are later used by Matru as a ploy to get Harry drunk in order to serve his own plans.

Bijlee Mandola is the only child of Harry Mandola. She lost her mother when she was young and now lives in Mandola village after having received a higher education in New Delhi and later in Oxford, England. Bijlee is now in love with Baadal (Arya Babbar), the son of a politician Chaudhari Devi (Shabana Azmi) and is all set to marry him.

Chaudhari Devi is a corrupt politician who, along with her obedients, is helping Harry realise his dream. Baadal and Bijlees union is strategically apt as it serves Harrys and Chaudhari Devis personal goals. While Chaudhari Devi conspires to control Harrys wealth by marrying her son to his daughter, Harry seeks Chaudhari Devis help in realising his dream in exchange for their childrens marriage.

Matru is a revolutionary, fighting for the villagers cause of not letting their land being taken away. He is educated in Law from JNU, holds a job as Harrys driver and is responsible for regulating Harrys drinking. Matrus revolutionary instincts are shown to be significantly influenced by those of Mao Tse Tung.

The film starts with a negotiation at a liquor shop set in crop laden fields, between the liquor shop owner and a heavily drunk Harry. The shop owners rude refusal to sell alcohol to Harry due to the day being a dry day, provokes Harry to run his Limousine into the shop.

Once drunk, Harry is shown to be an entirely different individual, who wants the land of the villagers to be returned to them, Matru to marry Bijlee while himself to retire into a religious man.
 
The villagers, in their fight are supported and advised by Mao who regularly sends messages to the villagers, written on cloth. Later in the film it is revealed that Mao is none other than Matru who is advising the villagers without revealing himself in order to retain the key position as Harrys aide.

Bijlees love for Baadal is depicted to be a contradiction to her subtle, probably subconscious, knowledge of Baadals incompatibility with her. This subtle thought aggravated by Baadals affiliation and acts supporting his mothers corrupt cause, overpowers Bijlees will to marry him and polarises her into helping Matru and the villagers instead. After this polarisation, seeing Chaudhari Devi and Baadals ambitions now with more clarity and while helping Matru and the villagers, Bijlee falls in love with Matru.

Although they have the help of Matru, his college friends and Bijlee, the villagers lose their entire harvest to an unexpected rain storm. This harvest, which would have fetched enough money to settle their debts to the government bank and still some more for their own survival, is now completely destroyed. Thus the villagers are forced to surrender their land, against their will, to the government in order to settle their debts.

Later, discovering Bijlees alliance with the villagers, Chaudhari Devi, now cautious, decides to finalise the decree to convert the now acquired land into an SEZ only once Bijlee and Baadal are married.

All this builds up the tension leading up to the wedding at the climax wherein Matru and the villagers have a plan to get Harry drunk who will then call off the wedding. This plan apparently works and Harry is shown to be drunk, succumbing to Matru and the villagers plan. Now while drunk, he enters the wedding venue, calls off the wedding, chases away Chaudhari Devi, Baadal and their supporters and decides to get Bijlee and Matru married. Consequently, upon refusal by Matru on the pretext that Harry will not feel the same way about him and Bijlee when sober, Harry, to everyones surprise, reveals that he had sworn on Bijlee to not consume even a drop of alcohol and has stood by his word, in turn revealing that while he did all that he did appearing to be drunk, he wasnt drunk at all in the first place.

==Cast==
* Pankaj Kapur as Harphool Singh Mandola a.k.a. Harry Imran Khan as Hukum Singh Matru
* Anushka Sharma as Bijlee Mandola
* Shabana Azmi as Chaudhari Devi
* Arya Babbar as Baadal
* Navneet Nishan as Mrs. Talwar
* Ranvir Shorey in a cameo appearance
* Lekha Washington in a cameo appearance
* Sunil Chitkara as Inspector

==Production==

===Filming===
The film was released on 11 January 2013.

===Trailers and release===

The first trailer of the film was released on YouTube in October 2012 which created a lot of buzz.

Anushka, about her character in the film, said "I play a strong, bull-headed, outspoken girl in the film. My look highlights the bold approach of the character." The actress also seemed appreciative of Imrans look. "Imran looks hot", she said. In the movie, Anushka Sharma will be sporting a tattoo on her lower waist which reads "Dekho Magar Pyaar Se" ("Look, but with affection"), which was unveiled in a press conference on 4 December 2012.

One other thing which caught the attention of people was the pink buffalo, "Gulabi Bhains".

The Censor Board of India gave the film an A (Adult) certificate, which director Vishal Bhardwaj refused;  therefore the film was rechecked and was given a U/A certificate by the Censor Board.

==Soundtrack==
{{Infobox album
| Name = Matru Ki Bijlee Ka Mandola
| Type = soundtrack
| Artist = Vishal Bhardwaj
| Label = Sony Music India
| Producer = Hitesh Sonik, Clinton Cerejo
}}
The films music was composed by Vishal Bhardwaj with lyrics by Gulzar. The soundtrack was released by Sony Music. The title song, by Sukhwinder Singh and Ranjit Barot, was released as a single track on 30 November 2012. Another single track, "Oye Boy Charlie", by Rekha Bhardwaj, Mohit Chauhan and Shankar Mahadevan was released on 12 December 2012. The full album was released on 19 December 2012 on Saavn.com. Musicperk.com rated the album 8/10, saying it is "Well-worth a listen". 

{{Track listing

| extra_column    = Singer(s)
| total_length    =

| title1          = Matru Ki Bijlee Ka Mandola 
| extra1          = Sukhwinder Singh, Ranjit Barot
| length1         = 3:52

| title2          = Oye Boy Charlie 
| extra2          = Rekha Bhardwaj, Mohit Chauhan, Shankar Mahadevan
| length2         = 5:44

| title3          = Khamakha 
| extra3          = Vishal Bhardwaj, Prem Dehati
| length3         = 5:59

| title4          = Lootnewale
| extra4          = Sukhwinder Singh, Master Saleem
| length4         = 4:06

| title5          = Shara-Rara-Ra
| extra5          = Prem Dehati
| length5         = 1:29

| title6          = Badal Uthiya
| extra6          = Rekha Bhardwaj
| length6         = 3:18

| title7          = Chaar Dina Ki Imran Khan, Prem Dehati
| length7         = 2:05

| title8          = Chor Police
| extra8          = Pankaj Kapur
| length8         = 0:58

| title9          = Nomvula African Umoja
| length9         = 1:40

| title10         = Badal Uthiya (Reprise) 
| extra10         = Prem Dehati
| length10        = 3:18

| title11         = Lootnewale (Reprise)
| extra11         = Sukhwinder Singh
| length11        = 1:59
}}

==Critical reception==
Raja Sen for Rediff.com awarded it 4/5 stars, opining Matru Ki Bijlee Ka Mandola has enough substance to warrant repeated viewings.     Anupama Chopra of the Hindustan Times rated the movie 2.5/5. She reasoned that "Parts of the film soar, but many are saggy and ultimately I was just underwhelmed." 

==Box office==

===India===
Matru Ki Beejlee Ka Mandola opened below the mark where business at multiplexes was much better than single screens.    It collected   on its opening day.    Its little growth on its second day managed around   net.  The film had a first weekend with a collection of   including paid previews.    The film netted around   in its first week.    It further dropped 87% and netted around   in its second weekend.  It netted   in its second week.  It earned   at the end of its run in domestic market.   

===Overseas===
Matru Ki Bijlee Ka Mandola collected $1.5 million in first 10 days of release overseas, as reported by Box Office India. 

==Box office reception==

Matru Ki Bijlee Ka Mandola

VERDICT: BELOW AVERAGE

Release Date:  11 January 2013
Opening Weekend: Rs 21.50Crores
Opening Week: Rs 32.27Crores
Lifetime: Rs 40Crores

COST VS REVENUES
Cost of Production/Acquisition: Rs 28Crores
Prints & Advertisement: Rs 16Crores
Total Cost to Producer: Rs 44Crores

REVENUES
India Theatrical: Rs 18Crores
Satellite: Rs 15Crores (pre-sold) 
Music & Home Video: Rs 5Crs
Overseas Collections: Rs 4Crs

Total Gross Revenue: Rs 42Crs
Total Loss: Rs 2Crs

Observations
1. The music didn’t really culminate in box-office numbers.
2. The main protagonist was Pankaj Kapur, not Imran Khan.
3. Amount fetched from Satellite Rights saves it somewhat but still a losing proportion due to unimpressive Overseas and Domestic theatrical run.
4. Sub-distributors who acquired the movie on MG basis for a high cost will end up losing more than 25-40 per cent of their investment.

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 