Dum (2003 Tamil film)
 
{{Infobox film
| name = Dhum
| image = Dhum.jpg
| caption =
| director = A. Venkatesh (director)|A. Venkatesh
| writer = Prasanna Kumar (dialogues)
| story  = Puri Jagannadh
| based on =  
| starring = Silambarasan Rakshitha Ashish Vidyarthi
| producer = Rockline Venkatesh Deva
| editing = V. T. Vijayan
| cinematography = A. Venkatesh (cinematographer)|A. Venkatesh
| studio = Rockline Productions
| released =  
| runtime = 160 minutes
| country = India
| language = Tamil
}} Tamil action Telugu in 2002 as Idiot (2002 film)|Idiot. The movie got mixed reviews from critics but was declared as a super hit at the box office. 

==Plot==
Simbu is the son of a police constable. He gets into fights and ends up in jail where his own father gets him out on bail. While he is returning home from a party drunk, a group of college students beats him up. That is when Rakshita comes and takes him to hospital and gives blood. She is the daughter of a police commissioner. Later, he falls in love with Rakshitha. That leads to several problems which are faced bravely by Simbu in the later part of the film. 

==Cast==
* Silambarasan as Satya
* Rakshitha as Suchitra
* Ashish Vidyarthi as Thilak
* S. S. Rajendran
* Radha Ravi
* Delhi Ganesh as Chokkalingam Livingston
* Sumitra

==Production==
The film was initially set to be titled Idiot after the Telugu version, but the title was later changed.  A. Venkatesh remade the film from Puri Jagannadhs 2002 Kannada film Appu (2002 film)|Appu starring Puneet Rajkumar, which was also remade in Telugu in 2002 as Idiot (2002 film)|Idiot starting Ravi Teja . Rakshitha appeared in all three versions of the film. During the making of the film, Silambarasan did his own stunts including a risky jump from the fifth floor of a building. 

==Release==
Dhum is a mixture of two cliches. One, the rich girl-poor boy love story, is an old and oft-seen one while the other, a young man standing up to someone in power, has gained popularity in recent times with the success of Dhill and Dhool. BalajiBbalasubramanian of Thiraipadam.com claims thats "its uninspired treatment of a cliched story and over-stylized actions".  The film performed well at the box office and it was declared as a super hit. 

==Box office==
* The film grossed $3 million at the box office.
* It ran for 100 days.
* This film was considered one of the biggest blockbuster hits in Simbus career of acting.
* This movie has also increased Silambarasans popularity.

==Soundtrack== Idiot composed by Chakri (music director)|Chakri.
* Adra Adra Dum - Silambarasan
* Chanakya Chanakya - Sadhana Sargam
* Kalakuven Kalakuven - Silambarasan
* Kalakuven Kalakuven - Shankar Mahadevan
* Kannamma Kannamma - Anuradha Sriram & Udit Narayan
* Karuppo Sivappo - Silambarasan Hariharan
* KK

==References==
 

 

 
 
 
 
 
 