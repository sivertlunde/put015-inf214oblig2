Nouvelle chance
{{Infobox Film
| name           = Nouvelle chance
| image          = 
| image size     = 
| caption        =  Anne Fontaine
| producer       = Philippe Carcassonne Pascal Houzelot Anne Fontaine Julien Boivent
| narrator       = 
| starring       = Danielle Darrieux
| music          = 
| cinematography = Caroline Champetier
| editing        = Isabelle Dedieu
| distributor    = 
| released       = 24 May 2006
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}}
 2006 cinema French drama Anne Fontaine. It was screened out of competition at the 2006 Cannes Film Festival.   

==Cast==
* Danielle Darrieux - Odette Saint-Gilles
* Arielle Dombasle - Bettina Fleischer
* Jean-Chrétien Sibertin-Blanc - Augustin Dos Santos
* Andy Gillet - Raphaël
* Christophe Vandevelde - Franck
* Michel Baudinat - Le prêtre
* Katsuko Nakamura - Kumiko
* Øystein Singsaas - Monsieur Wulka
* Mariana Otero - Madame Da Costa
* Philippe Storez - Philippe
* Xavier Morineau - Lemployé du Ritz
* Oscar Relier - Le barman du Ritz (as Oscar Reillier)
* Nabil Massad - Le client du Ritz
* Poundo Gomis - Fille casting 1
* Vanessa Navarro - Fille casting 2

==References==
 

==External links==
* 

 

 
 
 
 
 
 