Thegidi
{{Infobox film
| name           = Thegidi
| image          = Thegidi.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = P. Ramesh
| producer       = C. V. Kumar C. Senthil Kumar   T. E. Abinesh
| writer         = P. Ramesh
| starring       = Ashok Selvan Janani Iyer Jayaprakash
| music          = Nivas K. Prasanna
| cinematography = Dinesh Krishnan
| editing        = Leo John Paul
| studio         = Vel Media=
| distributor    = Thirukumaran Entertainment ABI TCS STUDIOS
| released       =  
| runtime        = 126 minutes
| country        = India
| language       = Tamil
| budget         =   2.4 crores   
| gross          =
}}
Thegidi (English: Dice / Deception) is a Tamil thriller (genre) film directed by P. Ramesh and produced by C. V. Kumar under his banner Thirukumaran Entertainment. The film features Ashok Selvan and Janani Iyer in the leading roles, while Nivas Prasanna composes the films music. The film released on February 28, 2014, and opened to generally positive reviews from critics.  A Telugu dubbed version Bhadram was thereafter released on March 21, 2014.

==Plot==
Vetri (Ashok Selvan) is a newly graduated criminology  student who has an immense passion in becoming a detective. He has a very good observing capacity, which makes his professors believe that he will become a good detective someday. He gets a job from a detective company and is assigned with the task of shadowing and surveillance. He stays with his best friend Nambi and collects all the details about his targets. His next target is Madhu (Janani Iyer), a beautiful girl, for whom Vetri falls at first sight. Vetris completed targets are killed one by one and Vetri understands that someone has used him  to collect details about the victims, and the next one on the list is  Madhu. He teams up with Inspector Raghuram and continue his enquiry with his help. On thorough investigation, Vetri finds that Sailesh and Sadagopan, who are a part of an insurance company, plot to take insurance policies, for the people who are single. The first two installments are paid by these plotters. By the third installment, the policy holders die in an accident involving death. The policies are then claimed by another unknown person having fake relationship with the policy holder. The third person is then found to be Vallabha, who is none other than the professor who motivated Vetri to become a detective. The claimed amount is then shared by the trio. Vetri then finds out the plot of his professor and his idea of linking him up with the detective agency. After narrating the story, the professor puts a bullet into his head. It is then shown that Vetri lives happily with friends and family taking care of the professors old mother. Things seem fishy, when a newspaper contains a card pointing the same agency with which Vetri had initially worked with. The story ends with Vetri picking up a call.

==Cast==
* Ashok Selvan as Vetri
* Janani Iyer as Madhusree
* Jayaprakash as Raghuram
* Kaali Venkat as Nambi
* Jayakumar as Sailesh
* Pradeep Nair as Poornachandran (Sadagoppan)
* Rajan as Govardhan
* Sai Prashanth as Kamalakannan
* Kavithalaya Krishnan as Ranganathan
* Rekha Suresh as Rukmani
* Shalu as Aparna
* Asritha as Vidya

==Production==
The film was announced in June 2013 as a joint venture between Thirukumaran Entertainment and Vel Media.  Along with C. V. Kumar, the film reunited several of the crew from Soodhu Kavvum (2013) with cinematographer Dinesh, editor Leo John Paul and actor Ashok Selvan all from that unit. The films director was revealed to be P. Ramesh and Nivas K. Prasanna would compose music.  Janani Iyer, seen before in Avan Ivan (2011) and Paagan (2012), was signed on to depict the role of a college girl. 

The film, described as a "murder mystery", was wrapped up within 45 days and in November 2013, post-production works were ongoing. It was reported that all the songs were set to be montages and further filming was not required, even though the songs were incomplete during production. 

==Soundtrack==
The films soundtrack and score were composed by newcomer Nivas K. Prasanna. 
 IANS wrote, "If Thegidi turns out to be a hit, debutant music composer Nivas Prasanna deserves huge credit for the success. He carries the tension of the film right till the end with his background score and songs that are placed in the narrative at right junctures".    Sify wrote, "  highlight of the film is Nivas Prasanna’s music and extraordinary BGM. The romantic melody Yaar Ezhuthiatho... is very soothing and BGM goes with the narration".    Rediff wrote, "The music by Nivas is an absolute joy. All the songs, as well as the background score are perfect". 

{| class="wikitable" width=80%";
|-  style="background:#cccccf; text-align:center;"
| No. || Song || Lyricist || Singers ||Time 
|-
| 1|| "Yaar Ezhuthiyatho" ||Kabilan|| Sathya Prakash || 04:16 
|-
| 2|| "Vinmeen Vithaiyil" ||Kabilan|| Abhay Jodhpurkar & Saindhavi || 04:58 
|-
| 3|| "Kangalai Oru" ||Kabilan|| Ajeesh || 02:21  
|-
| 4|| "Neethaane" ||Kabilan|| Shankar Mahadevan || 07:17   
|- Kurinchi Prabha|| Andrea Jeremiah || 05:13   
|}

==Release==
After the censor process, the team announced the release date as February 28. 

===Critical reception===
Baradwaj Rangan of The Hindu wrote "there are plenty of tense moments, thanks to the deliberate pacing (that steers clear of cheap, amped-up thrills) and the fact that the film keeps zooming in on a small cast of characters. Thegidi is proof that if the small things are worked out well, the bigger ones will take care of themselves".  Sify called the film "a well-made, edge-of- the-seat thriller with enough twists and turns. It keeps the deception game going till the last scene, with a taut script and outstanding BGM by Nivas Prasanna".  Indiaglitz gave it a 3.5/5 rating and stated, "Thegidi is one such brilliant movie flaunted by beaming background score grips your adrenaline rush through the movie".  Rediff gave it a 2.5 star rating and called it "a great effort by debutant director P Ramesh, who seems to have extracted the best from his team, be it the actors or the technicians". 
 IANS gave it 3.5 stars and wrote, "With its share of ups and downs, Thegidi is a taut film executed in style. 

===Box office===
The film had a slow start at the box office,  but improved later due to positive reviews from critics and word of mouth.    According to Behindwoods.com, the films shows increased in its 4th week.  At the Chennai box office, the film had collected  96,62,193 after 8 weeks.  In March 2014, Producers Councils President Keyaar said that Thegidi was one of the two Tamil films that turned out to be profitable ventures for all sections in 2014 till then.  Sify later reported that the film was profitable if the collections from the theatrical run and the dubbing and satellite rights were added together.  Producer C. V. Kumar in late March informed that the rights for music and overseas were sold for  7 lacs and  24 lacs respectively and that the film had collected a theatrical share of  2.2 crores till then. 

==Sequel==
In April 2014, director Ramesh stated that he would make a sequel to Thegidi in future. He said, "Thegidi 2 is definitely on the cards, but it wont happen immediately. I have started working on a new project which I intend to work on before the sequel".    He had planned a sequel even before the release of Thegidi. "I didnt have a bound script ready for the sequel, but I had decided to continue the sequel with Vallabas character that you see in Thegidi. I decided this while working on the prequel". 

== See also ==

* List of films featuring surveillance

== References ==
 

==External links==
*  

 

 
 
 
 
 