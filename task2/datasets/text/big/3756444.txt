Blood Rain (film)
{{Infobox film
| name           = Blood Rain
| image          = Blood Rain film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         = 혈의 누
 | hanja= 의  
 | rr=Hyeolui Nu
 | mr=Hyŏl-ŭi nu }}
| director       = Kim Dae-seung
| producer       = Kim Mi-hee
| writer         = Lee Won-jae Kim Seong-je
| starring       = Cha Seung-won Park Yong-woo Ji Sung
| music          = Jo Yeong-wook
| cinematography = Choi Young-hwan
| editing        = Kim Sang-beom Kim Jae-beom
| distributor    = Cinema Service
| released       =  
| runtime        = 119 minutes
| country        = South Korea
| language       = Korean
| gross          = $14,270,938  . Box Office Mojo. Retrieved 2012-03-04. 
}} South Korean Roman Catholicism in the Joseon Dynasty. Although primarily a period thriller, director Kim Dae-seung weaves together an unconventional mix of styles—a puzzle-box mystery plot traditionally associated with detective fiction, class-conscious social commentary, lush cinematography, sets and costume design, and a flair for gore.  

== Plot ==
It is year 1808 on Donghwa Island, a small island with a technologically advanced (for its time) paper mill. The presence of the mill has spawned a bustling village, and given its townspeople a certain degree of wealth. With climate and trees perfectly suited for papermaking—and a location remote enough to ensure both privacy and secrecy—the island has established a profitable business in high quality paper, with trade routes stretching as far away as China.

This isolated and largely autonomous island begins to be plagued by a string of gruesome murders. However, its not just the mounting death toll thats causing residents to worry, but the sadistic, methodical way in which the victims were killed. With the killer still on the loose, the government sends in special investigator Wonkyu to crack the case. While conducting his dogged investigation, he soon uncovers a myriad of hidden secrets, tracing the murders back to an incident that occurred some seven years earlier, in which the former owner of the mill was executed for practicing Catholicism. The townspeople, for their part, are convinced that the dead mans ghost has come back for revenge. As the young officer digs deeper into the islands dark past, Wonkyu discovers that there may be something even more frightening than the murders or the murderer—a truth that will make him question the depths of human nature.

== Cast ==
* Cha Seung-won as Wonkyu   
* Park Yong-woo as In-kwon  
* Ji Sung as Du-ho
* Yoon Se-ah as So-yeon Manshin
* Oh Hyun-kyung as Kim Chi-sung
* Choi Jong-won as Royal emissary Choi
* Chun Ho-jin as Commission agent Kang

==Awards and nominations==
;2005 Chunsa Film Art Awards
* Best Film
* Best Director – Kim Dae-seung
* Best Supporting Actor – Park Yong-woo
* Best Cinematography – Choi Young-hwan
* Best Editing – Kim Sang-beom, Kim Jae-beom
* Best Lighting – Kim Seong-kwan 
* Best Technical Effects – Shin Jae-ho

;2005 Grand Bell Awards 
* Best Art Direction – Min Eon-ok
* Best Costume Design
* Nomination – Best Film
* Nomination – Best Director – Kim Dae-seung
* Nomination – Best Supporting Actor – Park Yong-woo
* Nomination – Best Screenplay – Lee Won-jae
* Nomination – Best Cinematography – Choi Young-hwan
* Nomination – Best Editing – Kim Sang-beom, Kim Jae-beom

;2005 Blue Dragon Film Awards
* Best Visual Effects
* Nomination – Best Film
* Nomination – Best Director – Kim Dae-seung
* Nomination – Best Supporting Actor –  Park Yong-woo
* Nomination – Best Cinematography – Choi Young-hwan
* Nomination – Best Art Direction – Min Eon-ok

;2005 Korean Film Awards
* Best Supporting Actor – Park Yong-woo
* Best Art Direction – Min Eon-ok
* Best Visual Effects
* Best Sound
* Nomination – Best Director – Kim Dae-seung
* Nomination – Best Supporting Actress – Choi Ji-na
* Nomination – Best New Actress – Yoon Se-ah
* Nomination – Best Screenplay – Lee Won-jae
* Nomination – Best Cinematography – Choi Young-hwan
* Nomination – Best Music – Jo Yeong-wook

;2006 Yubari International Fantastic Film Festival
* Grand Prize

;2006 Baeksang Arts Awards
* Best Film
* Nomination – Best Actor – Cha Seung-won
* Nomination – Best Screenplay – Lee Won-jae

==References==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 