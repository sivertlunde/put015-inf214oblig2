Blues in the Night (film)
{{Infobox film
| name           = Blues in the Night
| image          = Blues-in-the-night-1941.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Anatole Litvak
| producer       = Hal B. Wallis
| screenplay     = Robert Rossen
| based on       =  
| starring       = Priscilla Lane Betty Field Richard Whorf Lloyd Nolan Jack Carson
| music          = Heinz Roemheld
| cinematography = Ernest Haller
| editing        = Owen Marks
| distributor    = Warner Bros.
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} musical drama Best Song Oscar for "Blues in the Night" (Music by Harold Arlen; lyrics by Johnny Mercer).   

==Plot==
While playing in a bar in St. Louis, jazz pianist Jigger Pine meets aspiring clarinetist Nickie Haroyen who tries to convince him to put together a jazz band. After a drunk patron starts a fight, Nickie and Jigger are thrown in jail with Jiggers drummer and bassist. They hear a prisoner singing a blues song and are inspired to set out for New Orleans where they hope to learn how to perfect an authentic bluesy sound. There they meet fast-talking trumpet player Leo and his wife Character who is a talented singer. Together, the quintet rides the rails, honing their technique in dive bars across the country.

One day while sheltering in a boxcar they meet a mysterious stranger named Del who first robs them and then, impressed by their camaraderie, offers them a job in a New Jersey roadhouse called The Jungle. The group discovers that the roadhouse is actually owned by Dels former partners in crime, aspiring singer Kay, accomplice Sam and her crippled sidekick Brad. Del has escaped from jail to retrieve his share of a robbery the three committed, but when Kay tells him they have spent all the money, he decides to take over The Jungle and transform it into an illegal gambling club. Kay tries to rekindle her past romance with Del, but he rejects her and she turns her attention to Leo. Although the band is happy playing their brand of jazz each night at the club, Character is worried about Leo and Kay. Jigger reveals to Leo that Character is pregnant and he decides to give up Kay. She soon sets her sights on Jigger who is secretly in love with her. When Sam tries to get her to alert the police to Dels whereabouts, she tells Del and Sam is killed by Dels henchmen. Del orders her to leave The Jungle so she convinces Jigger to quit the band and go to New York City to join a commercial, mainstream jazz band.

Although successful, Jigger is unhappy in his new life, feeling he is not playing authentic jazz. Kay finally grows bored of life with Jigger and leaves him when he tells her he is quitting the band. He descends into alcoholism and has a mental breakdown. His friends find him and help nurse him back to health, hiding the fact that Characters baby has died. They return to The Jungle where Jigger begins playing again, but Kay shows up without any money looking for help. She and Del have a quarrel which begins to turn violent but Jigger comes to her defense, fights with Del.  During that struggle, Kay gets Dels gun shoots and kills him. Jigger decides to protect Kay and help her escape from the police, but Brad hears their plans and realizes that Kay is leaving forever. The band shows up and try to talk Jigger out of leaving with Kay, revealing that Character lost the baby.  They compare Jiggers mental problems with Brad being a cripple, stating that Brad has no choice about how he will end up but Jigger does have a choice.  Brad overhears the band.  He joins Kay in Dels car where she was waiting for Jigger.  Brad drives away with her into a violent storm, deliberately wrecking the car so they are both killed. The band resolves to leave The Jungle behind and they return to their life on the road, happy to again be playing their preferred version of jazz.

==Cast==
* Richard Whorf as Jigger Pine, a talented jazz pianist Priscilla Lane as Ginger "Character" Powell, the bands singer
* Betty Field as Kay Grant, Dels scheming former girlfriend
* Jack Carson as Leo Powell, Characters loud-mouthed, conceited husband who plays trumpet for the band
* Lloyd Nolan as Del Davis, a gangster and racketeer
* Elia Kazan as Nickie Haroyen, the bands clarinetist who gave up law school for music 
* Wallace Ford as Brad Ames, a crippled former guitar player who is hopelessly in love with Kay 
* Howard Da Silva as Sam Paryas, an opportunistic member of Dels gang    
* Peter Whitney as Pete Bossett, the bands bassist
* Billy Halop as Peppi, the bands young drummer George Lloyd as Joe, the St. Louis cafe owner Charles C. Wilson as Barney
*   as Baritone Singer in Jail Cell
* Matt McHugh as the Drunk
* Ernest Whitman as Black Prisoner #1 
* Napoleon Simpson as Black Prisoner #2
* Dudley Dickerson as Black Prisoner #3 
* Anthony Warde as Dels Henchman #1 
* Sol Gorss as Dels Henchman #2

==Production== Edwin Gilbert called Hot Nocturne and began retooling it for Broadway theatre|Broadway. He eventually sold the rights to Warner Bros. who gave the script to Robert Rossen to complete. After initially retitling it New Orleans Blues, the studio named it after its principal musical number "Blues in the Night", which later became a popular hit. Kazan agreed to give up his screenwriting credit and appeared as a clarinetist in the film. He later remarked that after acting in the film he became convinced he could "direct better than Anatole Litzvak".  James Cagney and Dennis Morgan were the studios first two choices to play the gangster Del Davis, but the role was eventually given to Lloyd Nolan. John Garfield was cast in the role of pianist Jigger Pine who was eventually played by Richard Whorf. 

==Reception==

===Critical response=== East Coast release took place shortly before the attack on Pearl Harbor. The film has since achieved a small cult following, including The Simpsons creator Matt Groening.   

==Music==
The films music is by Harold Arlen with lyrics by Johnny Mercer. Additional music was written by Heinz Roemheld and Ray Heindorf (only Roemheld was credited). The film features the bands of Jimmie Lunceford and Will Osborne. With the exception of Priscilla Lane none of the actors were musicians so their playing had to be dubbed by other artists. The trumpet music performed by Jack Carsons character was dubbed by Snooky Young and Frankie Zinzer while the piano music was dubbed by Stan Wrightsman.   Saxophonist and clarinetist Archie Rosate played Elia Kazans clarinet solos.

#"Blues in the Night" (William Gillespie)
#"This Time the Dreams On Me" (Priscilla Lane) 
#"Hang on to Your Lids, Kids" (Priscilla Lane)
#"Says Who, Says You, Says I" (Mabel Todd) 
#"Wait Till It Happens to You" (Betty Field) (dubbed by Trudy Erwin)

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 