Blood Brothers (1996 film)
{{Infobox Film
| name           = Blood Brothers
| image          = 
| caption        = 
| director       = Ernie Fritz
| producer       = Jon Landau, Barbara Carr, Jack Gulick, Ernie Fritz, Lee Rolontz, 
| writer         =   
| director     =   Ernie Fritz
| starring       = Bruce Springsteen, The E Street Band, Jon Landau
| music          = Bruce Springsteen, "High Hopes" written by Tim Scott McConnell
| cinematography = Ernie Fritz
| editing        =   Ernie Fritz, Kathy Dougherty 
| distributor    = Columbia Music Video
| released       = March 3, 1996
| runtime        = 90 mins
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Blood Brothers is a 1996   with The E Street Band in 1995.  The film was nominated in 1997 for a Grammy for "Best Music Video - Long Form"  
 Greatest Hits Reunion Tour The Rising album.

Fritz documented the entire period - capturing Springsteen and manager/producer Jon Landau deciding what songs to put on Greatest Hits album, working on arrangements of the new songs, the recording sessions themselves and performances at the promotional shows.

Critical reaction to the Blood Brothers documentary was very positive.  TV Guide called it "superlative ... more than a record of a legendary bands reunion. It offers a glimpse of Springsteen that fans rarely see."

One writer for the tabloid The New York Post offered his personal opinion on one aspect of the content captured in the film rather than on its merits as a documentary, saying "It makes one a little uneasy to see how quickly the members of the band forgive their former employer, for a chance to work for a few days with him again."
 Blood Brothers EP in CD form), and on DVD on January 16, 2001.

== External links ==
*  

 

 
 
 
 
 
 

 