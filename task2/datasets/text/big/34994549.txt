Walzerkrieg
Walzerkrieg German biographical Ludwig Berger and starring Renate Müller, Willy Fritsch and Paul Hörbiger. It depicts the life of the Austrian ballet dancer Katti Lanner who eventually settled in Victorian Britain.  A French-language version La guerre des valses was also directed by Berger. 

==Cast==
* Renate Müller - Katti Lanner 
* Willy Fritsch - Pauker Gustl 
* Paul Hörbiger - Joseph Lanner 
* Anton Walbrook - Johann Strauß 
* Rosi Barsony - Ilonka 
* Theo Lingen - Sir Phillips 
* Hanna Waag - Queen Viktoria 
* Trude Brionne - Katis Freundin Susi 
* Karel Stepanek - Kellner Leopold 
* Heinz von Cleve - Albert von Coburg 
* Hugo Flink - Der Wirt 
* Hansjörg Adolfi - Der Richter 
* Ossy Kratz-Corell - 1st Piccolo 
* Bert Schmidt-Moris - 2nd Piccolo  Tommy Thomas - 3rd Piccolo

==References==
 

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* Elsaesser, Thomas. Weimar Cinema and After: Germanys Historical Imaginary. Routledge, 2000.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 