Out (1982 film)
{{Infobox Film
| name           = Deadly Drifter
| image          = DeadlyDrifter.jpg
| caption        = DVD cover
| director       = Eli Hollander
| producer       = Eli Hollander
| writer         = Ronald Sukenick  (novel & screenplay) , Eli Hollander  (screenplay) 
| starring       = Peter Coyote, O-Lan Jones, Jim Haynie
| music          = David Cope
| cinematography = Robert Ball
| editing        = Eli Hollander
| distributor    =Troma Entertainment
| released       = 1982
| runtime        = 83 min
| country        =   USA English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Deadly Drifter (also known as Out) is a satirical 1982 film directed by Eli Hollander. The film is based on Ronald Sukenicks 1973 novel, OUT. It stars Peter Coyote, O-Lan Jones, and Danny Glover. The movie tells the tale of Rex (Coyote) roaming the U.S. doing various assignments for a mysterious group of "urban guerrillas" they call "Our Friends". In each meeting the person or persons designated "It" carries a hidden stick of dynamite. Director Eli Hollander summarizes the film, "A subtitle of it could be From Yippie to Yuppie. And the 80s are certainly the age of yuppies. The film does kind of chronicle the history of the transformation from the 60s into the 80s."

==Synopsis==

Deadly Drifter is about a team, known as "Our Friends", searching for the "Old Man", who is dying. Their trip across the U.S. takes many twists and turns along the way, as you see through the eyes of the main character, Rex/Harrold, who ultimately questions society. Whales are part of his ultimate revelation.

==Structure==
Deadly Drifter is structured in a 10 part journey/road film across America from the East to the West. The characters appear and disappear, morphing into other personalities and often using lines from previous scenes, thus the film, though linear, is a cyclic story. The movie was immortalized by O-Lan Joness heartrending lines "allow simmer" and "you cant have the schleung."

==External links==
* 
*  
* "Out in Plain Sight, Brett Taylor, Paracinema # 11, December 2011.

 
 
 
 
 
 


 