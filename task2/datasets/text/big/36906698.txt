Bianco, rosso e...
{{Infobox film
 | name =
 | image = Bianco, rosso e poster.jpg
 | caption =
 | director = Alberto Lattuada
 | writer =Tonino Guerra Ruggero Maccari  Jaja Fiastri  Alberto Lattuada
 | starring = Sophia Loren
 | music = Fred Bongusto
 | cinematography = Alfio Contini
 | editing = Sergio Montanari
 | producer =
 | distributor =
 | released = 1972
 | runtime = 96 min
 | awards =
 | country = Italy
 | language = Italian
 | budget =
 }}
Bianco, rosso e... (internationally released as White Sister and The Sin) is a 1972 Italian comedy film directed by Alberto Lattuada.   The film was a commercial success. 

== Cast ==
* Sophia Loren: Sister Germana
* Adriano Celentano: Annibale Pezzi
* Fernando Rey: Il primario
* Juan Luis Galiardo: Guido
* Giuseppe Maffioli: Dr. Arrighi
* Luis Marín: Brigadiere libico
* Tina Aumont: Ricci
* Enzo Cannavale: Quinto
* Bruno Scipioni: Chiacchiera 
* Dori Dorika: Dorotea
* Alessandra Mussolini: Sister Germana as a child

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 