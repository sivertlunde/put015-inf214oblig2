The Ketchup Effect
{{Infobox film
| name           = The Ketchup Effect
| image          = The Ketchup Effect.jpg
| alt            = 
| caption        = Swedish cover
| director       = Teresa Fabik
| producer       = Lars Blomgren Genya Kihlberg
| writer         = Teresa Fabik
| starring       = Amanda Renberg Björn Kjellman Ellen Fjæstad Linn Persson Filip Berg Marcus Hasselborg
| music          = Jacob Groth
| cinematography = Pär M. Ekberg
| editing        = Sofia Lindgren
| studio         = 
| distributor    = NonStop Sales AB Sandrew Metronome Distribution Sverige AB
| released       =  
| runtime        = 90 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}}
Hip Hip Hora!, known as The Ketchup Effect in most English-speaking countries, is a 2004 Swedish teen movie, directed by Teresa Fabik.

==Plot==
Friends and future seventh-year (upper primary school) students Sofie, Amanda and Emma anxiously await the start of term. This is virtually no major change education-wise, but a social watershed moment when cliques come into play, more or less serious relationships start being developed, and students, to hear them tell it, leave behind childish things to move into their teens; certainly the three girls are confident in how grown up they now are.

Sofie develops a crush on Mouse, a ninth-year and the local heartthrob. Mouse takes slight notice of her and tells her about an upcoming party. There he tries to hook up Sofie with Sebastian "Sebbe", a relatively quiet and awkward boy from his group. Sebbe and Sofie end up alone and drunk in a bedroom. At the prodding of his friends Sebbe asks for a blow job, which Sofie refuses to do; then Sebbe requests a hand job, which she agrees to do but doesnt know how. He compares the act to emptying a plastic ketchup bottle. Ideally he would have described it more accurately; in a scene featuring prosthetic frontal male nudity, Sofie grabs his penis and delivers a few sharp strikes with her palm. The situation then breaks up.

After this situation, knowing that Sebbe has been hurt, Sofie leaves the room and gets extremely drunk. Mouse then proceeds to take advantage of what has occurred by assaulting her, even though she is passed out. The guys take pictures of her in compromising positions, but Sebbe holds back from joining in. The next day, there are pictures all over the place of her and Mouse is spreading rumours about her. Every time she has to go past him, he and his friends grope her. To the teachers, it looks like he has a harmless crush on her. Her dad finds out as someone sent him a picture anonymously. He then says that if she didnt dress so provocatively, the guys might not respond in such a crude manner. Sofie leaves and is at the underground station when she runs into Sebbe again. They see Mouse coming and so they run to his house. They have the awkward first conversation, as Sebbe knows more of what went on than she did. They listen to some of his music and he actually starts to hit on her a bit. At one point, they are at the underground station and he compliments her boobs, which she doesnt appreciate and leaves. He realises hes said the wrong thing, but its too late.
 queen bee. They get invited to a party at someones house. Sofie manages to get to where the party is, uninvited. She enters and her friends ignore her. She goes to a window and jumps. After a short period of time, she is seen going to the hospital, with Amanda running after the ambulance before they let her in. Sofie is in the hospital, not paralysed, but unable to move. Her dad arrives at the hospital and when Amanda tries to open up to him, he turns a blind eye and goes straight to his daughters side. After she returns home, Amanda and Emma repeatedly call her mobile phone, but she refuses to answer it. Eventually they come to her house, where Sofie tells them to "go to hell" because they did not stand by her like good friends should.

Sofie eventually returns to her school, deciding not to transfer, and dressed the way an upper primary school student should. She sees Mouse again and, as a safeguard against future assault, injures him in the groin. He then tries to attack her but slips onto the floor. Everyone in the school is laughing at him and his friends do not stand behind him right away. After Sofie and her friends run away, Sebbe runs after them too, wanting to talk to her. He proceeds to apologise and tells her that he likes her a lot, and that he wants to be friends with her. He also asks if they can go out to dinner, and she accepts. The final scene shows the two eating dinner, with a bottle of ketchup on the table.

==Cast==
*Amanda Renberg as Sofie
*Björn Kjellman as Krister
*Ellen Fjæstad as Amanda
*Linn Persson as Emma
*Filip Berg as Sebbe
*Marcus Hasselborg as Mouse
*Björn Davidsson as Jens
*Carla Abrahamsen as Beatrice
*Robin Lindbom as Loka
*Manuel Bjelke as Manuel
*Margareta Pettersson as Headmaster
*Cattis Olsen as School welfare officer
*Ulrika Dahllöf as Teacher
*Josephine Bauer as Åsa
*Cecilia Ljung as Amandas mother (scenes deleted)

==Awards==
*Teresa Fabik won the Amanda Award for Best Nordic Newcomer in 2004. 
*Sofia Lindgren won Guldbagge Award for Best Achievement for editing in 2005.

==DVD release==
*The movie was released on DVD in Finland on 11 March 2005.

==References==
 

==External links==
*  
*  
*  

 
 
 
 