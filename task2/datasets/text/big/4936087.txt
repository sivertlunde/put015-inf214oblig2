Saint Ralph
{{Infobox film
| name           = Saint Ralph
| image          = Saint_ralph_2004.jpg
| caption        = DVD Cover Michael McGowan
| producer       = Teza Lawrence Andrea Mann Seaton McLean
| writer         = Michael McGowan
| starring       = Adam Butcher Campbell Scott
| music          = Andrew Lockington
| cinematography = Rene Ohashi 
| editing        = Susan Maggi
| studio         = 
| distributor    = Alliance Atlantis 
| released       =  
| runtime        = 98 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = $1,410,952 
}} Michael McGowan. Its central character is a teenage boy who trains for the 1954 Boston Marathon in the hope a victory will be the miracle his mother needs to awaken from a coma.

The film premiered at the 2004 Toronto International Film Festival and was given a theatrical release in 2005. 

==Plot== Catholic private cross country team to relieve him of his excess energy.

When Ralphs mother falls into a coma, he is told it will take a miracle for her to survive. When running coach Father Hibbert, a former world class marathoner who was forced to quit running when he injured his knee, claims it would be a miracle if a member of his team won the Boston Marathon, Ralph decides to train for it in the hope his victory would fulfill the miracle needed to save his mothers life.

At first, Ralph cannot even keep up with his teammates in practice. He reads books to learn about running, uses the new techniques, and gradually improves. Father Hibbert decides to train him despite disapproval from Father Fitzpatrick. Ralph begins to win the respect of his classmates, and eventually wins the attention of the local media when he wins a prestigious regional race.

When Father Fitzpatrick learns Ralph intends to run the Boston Marathon, he threatens to expel him if he participates, as well as remove Father Hibbert from the priesthood should he try to interfere. Both Ralph and his mentor must then decide how deeply they believe in miracles, and what is possible when a person risks everything without promise of success.

Both he and Father Hibbert defy Father Fitzpatrick and decide to go to Boston.  Ralph ends up winning second place after a close race with the previous years winner and gives the medal to his mother who wakes up from her coma.

==Production==
The film was shot on location in Cambridge, Ontario|Cambridge, Hamilton, Ontario|Hamilton, and Toronto.

The song "Hallelujah" by Leonard Cohen is performed by Gord Downie.

After premiering at the Toronto International Film Festival, the film was shown at the Portland International Film Festival, the Festival du Film dAventures de Valenciennes and the Paris Film Festival before opening in Canada on April 8, 2005.

==Cast==
*Adam Butcher as Ralph Walker
*Campbell Scott as George Hibbert
*Gordon Pinsent as Father Fitzpatrick
*Jennifer Tilly as Nurse Alice
*Shauna MacDonald as Emma Walker
*Tamara Hope as Claire Collins
*Michael Kanev as Chester Jones 

==Critical reception==
In his review in the New York Times, Stephen Holden said, "This crude, inspirational tear-jerker is as sweet as a bowl of instant oatmeal smothered in molasses."  Sean Axmaker of the Seattle Post-Intelligencer thought the "conventionally heartwarming underdog drama doesnt miss a cliche." He continued, "The climactic race footage is well directed... and the understated period detail adds to the charm, but the route is painfully familiar and the obstacles are as contrived as the clunky performances. The exception is Scott, whose sensitive turn as a priest inspired by Ralphs conviction and commitment gives the film a touch of grace at the cost of revealing McGowans drab direction of every other actor. Yet there is something inherently rousing in the finale, no matter how hackneyed, as if his conviction ultimately holds its own against the cliches."  In the Austin Chronicle, Marrit Ingman rated the film two stars and called it "very conventional in its characterizations and narrative." He added, "The movie does have two things going for it (besides Tilly in a nurse’s uniform). One is its brisk, sunny aplomb. It zips right along in cheerful montages and abbreviated scenes, divided by intertitles announcing the feasts of saints. The second is Butcher, who seems to be something like the masculine Amanda Bynes of Canada: goofy and authentically youthful but remarkably assured onscreen... The film isnt going to catapult Butcher to international stardom, but he holds his own in it and helps to sell its curious logic."  

==Award and nominations==
*Genie Award for Best Motion Picture (nominee)
*Genie Award for Best Performance by an Actor in a Leading Role (Adam Butcher, nominee)
*Genie Award for Best Performance by an Actor in a Supporting Role (Campbell Scott and Gordon Pinsent, nominees)
*Genie Award for Best Achievement in Costume Design (Anne Dixon, nominee) Canadian Screenwriting Award for Best Feature Film (Michael McGowan, winner) 
*Canadian Society of Cinematographers Award for Best Cinematography in a Theatrical Feature (Rene Ohashi, nominee)
*Directors Guild of Canada Award for Outstanding Achievement in Direction of a Feature Film (Michael McGowan, winner) 
*Directors Guild of Canada Award for Outstanding Team Achievement in a Family Feature Film (winner)
*Directors Guild of Canada Award for Outstanding Achievement in Production Design for a Feature Film (Matthew Davies, nominee) 
*London Canadian Film Festival Audience Award (winner)
*Paris Film Festival Grand Prix Award (winner)
*Young Artist Award for Best International Family Feature Film (winner)
*Young Artist Award for Best Performance in a Feature Film by a Leading Young Actor (Adam Butcher, nominee) 
*Young Artist Award for Best Performance in a Feature Film by a Supporting Young Actor (Michael Kanev, nominee)  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 