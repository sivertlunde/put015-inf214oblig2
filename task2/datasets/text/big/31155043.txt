Laila Majnu (1962 film)
{{Infobox film
| name           = Laila Majnu 
| image          = 
| image size     =
| caption        =
| director       = P. Bhaskaran
| producer       = B. N. Konda Reddy P. Bhaskaran
| writer         = Jagathy N. K. Achary
| narrator       =
| starring       = Prem Nazir<br/L. Vijayalakshmi
| music          = Baburaj
| cinematography = D. V. Rajaram
| editing        = Kripa Shankar
| studio         = Kerala Pictures
| distributor    = 
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Laila Majnu  is a 1962 Malayalam language romance film directed by P. Bhaskaran and starring Prem Nazir, L. Vijayalakshmi, Sathyan (actor)|Sathyan, Thikkurissi Sukumaran Nair|Thikkurissi, T. S. Muthiah, Chandni, Adoor Bhasi and Kodungalloor Ammini Amma. It was the first Malayalam version of the classic Sufi legend filmed extensively in Hindi and other South Indian languages. The films acclaimed music is composed by Baburaj. Egyptian dancer Laila appeared in one of the songs.  

==Production==
The film was produced by cinematographer B. N. Konda Reddy jointly with poet P. Bhaskaran under the banner of Kerala Pictures. P. Bhaskaran directed the film as well as penned the lyrics for songs. It was the first Malayalam version of the Sufi legend filmed extensively in Hindi and other South Indian languages. The film was shot at Vauhini Studios (founded by Konda Reddy) and some of the scenes were shot in the deserts of Rajasthan.

==Plot==
 
Qais is son of an ordinary Arab merchant Amir Ameeri. He falls in love with the beautiful Laila, daughter of the immensely rich and powerful landlord Sarvari. Though Laila reciprocates his love in equal measure, her father opposes this match. All the attempts of Sarvari to separate the lovers fail. Sarvari shifts his residence to another city on the ouskirts of the holy town of Mecca secretly. Heartbroken at this separation, Qais loses his mental faculties and wanders aimlessly in the deserts like a lunatic. Laila meets Qais in the loneliness of the desert. Qais is beaten severely by Sarvaris men. Amir Ameeri finds his son in a very pathetic condition and he takes him to Sarvaris palace. Amir Ameeri falls at the feet of Sarvari and begs for his mercy and requests for the marriage of Laila with Qais. Sarvaris heart melts and he agrees for the marriage.

Meanwhile, Baqthum the Prince of Iraq happen to see beautiful Laila. He informs Sarvari his desire to marry Laila. Baqthum had to betray his former lover Zarina to win Lailas love. Sarvari breaks his promise with Amir Ameeri and conducts Lailas marriage with Baqthum. But after the marriage, Laila doesnt allow Baqthum to touch her.

Broken hearted Qais turns a real lunatic. He wanders in the deserts, always uttering the name of Laila. Eventually once Laila happen to meet Qais in the lonely deserts on her return from Iraq. The separated lovers meet in the deserts. Laila and Qais lose their lives in a sandstorm and unite in death.

==Cast==
* Prem Nazir as Qais
* L. Vijayalakshmi as Laila Thikkurissi as Sarvari
* T. S. Muthaiah as Amir Ameeri Sathyan as Baqthum
* Chandni as Zareena
* Adoor Bhasi
* Master Radhakrishnan
* Sobha
* Bahadoor
* Shantha
* Baby Vilasini
* S.A Jameel
* Kochappan
* Kodungalloor Ammini Amma

==Soundtrack== 
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Annathinum Panjamilla || KS George, Mehboob || P. Bhaskaran || 
|- 
| 2 || Chundukanneeraal || KP Udayabhanu || P. Bhaskaran || 
|- 
| 3 || Kandaal nalloru || AP Komala, Chorus, Santha P Nair || P. Bhaskaran || 
|- 
| 4 || Kanninakathoru || KS George, Mehboob || P. Bhaskaran || 
|- 
| 5 || Kazhinjuvallo || P. Leela || P. Bhaskaran || 
|- 
| 6 || Koottililam Kili || P. Leela, AP Komala || P. Bhaskaran || 
|- 
| 7 || Kuppivala Nalla Chippivala || Chorus, Gomathy, Santha P Nair || P. Bhaskaran || 
|- 
| 8 || Oru Kulappoovirinjaal || Santha P Nair || P. Bhaskaran || 
|- 
| 9 || Pavanurukki || P. Leela, KP Udayabhanu || P. Bhaskaran || 
|- 
| 10 || Premamadhumaasa Vanathile || P. Leela, KP Udayabhanu || P. Bhaskaran || 
|- 
| 11 || Snehathin Kaananachola || P. Leela || P. Bhaskaran || 
|- 
| 12 || Thaarame Thaarame || P. Leela, KP Udayabhanu || P. Bhaskaran || 
|- 
| 13 || Thoovaala || KS George, Mehboob, Santha P Nair || P. Bhaskaran || 
|}

==References==
 

==External links==
*  
*http://www.malayalachalachithram.com/movie.php?i=102

 

 
 
 
 