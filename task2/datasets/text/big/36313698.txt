Shuttlecock Boys
 
 
{{Infobox film
| name           = Shuttlecock Boys
| image          = Shuttlecock Boys Film Poster.jpg
| caption        = Shuttlecock Boys poster
| director       = Hemant Gaba
| producer       = Hemant Gaba Thakur Dass Pankaj Johar
| screenplay     = Hemant Gaba
| starring       = Aakar Kaushik Manish Nawani Vijay Prateek Alok Kumar  
| music          = Avinash Baghel
| cinematography = Shanti Bhushan
| editing        = James Joseph Valiakulathil
| studio         = Penny Wise Films Private Limited
| released       =  
| runtime        = 76 minutes
| country        = India
| language       = Hindi
| budget         =   3,500,000 (70,000 USD$)
}} handheld on 16 mm with sync sound using real locations in Delhi, Noida & Gurgaon.

==Plot==
Shuttlecock Boys   revolves around the lives, successes and failures of four friends who hail from lower-middle-class backgrounds in Delhi. Should we accept what life offers to us on a platter or should we try to chase our dreams, taking the path less taken? Thats the question our protagonists Gaurav, Manav, Pankaj and Loveleen seek answer to, as they decide to embark upon an eventful journey that will change the course of their lives and careers forever. 
The one thing that unites these four friends is their common love for badminton. Every evening they meet for a couple of games in their neighbourhood joking about their lives and pulling each others legs. This part of the day is the most pleasant part of their otherwise dreary lives. While playing badminton one such night, they decide to do something on their own. This very attempt becomes their lifeline putting them on a litmus test of determination, courage, luck and spirit of friendship. 

==Production== Guerilla Style. The interiors were all real locations mostly office locations and houses.
 Chinese Lanterns Shanti Bhushan Line Production. Bhavya Palia who had never even seen a film set joined stepped in as Line Producer in the very last moments just before the shoot and eventually managed the production along with Producer / Executive Producer Pankaj Johar. Prachi Singh who was initially hired just as an Assistant Director ended up donning multiple hats and handled costumes and production design. Catering was managed by Pushpa Gaba from Director/Producers homes.

==Music==
There are four songs in the film – 
#Ruk Naa Tu Ab Kahin (Lyrics – Prachi Singh)
#Zindagi Hai Ek Shuttle (Lyrics – Manas Mishra)
#Dilli Ke Chaar Ladke (Lyrics – Gaurav Solanki)
#Zindagi Hai Ek Shuttle (Slow Version)

All four songs are composed & sung by Avinash Baghel. The songs have been used as background score.

==Festival==
* Australian Festival of South Asian Arts,  Sydney
* New Jersey Independent South Asian Cine Fest,  Edison, New Jersey
* Golden Village Cinemas,  Singapore Gotham Screen International Film Festival, New York 
* Seattle South Asian Film Festival,  Seattle
* Chicago South Asian Film Festival,  Chicago
* India International Film Festival Tampa Bay,  Florida
* Cine ASA Guwahati International Film Festival,  Assam
* Jagran Film Festival,  New Delhi
* Narmad International Film Festival,  Surat

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 