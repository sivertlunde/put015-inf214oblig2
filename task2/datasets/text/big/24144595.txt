Take Me Back to Oklahoma
{{Infobox film
| name           = Take Me Back to Oklahoma
| image          =
| image_size     =
| caption        =
| director       = Albert Herman
| producer       = Edward Finney
| writer         = Robert Emmett Tansey (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Marcel Le Picard
| editing        = Frederick Bain
| distributor    =
| released       = 11 November 1940
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Take Me Back to Oklahoma is a 1940 American film directed by Albert Herman.

== Cast ==
*Tex Ritter as Tex Lawton
*Bob Wills as Himself, leader Texas Playboys
*Slim Andrews as Slim Hunkapillar Terry Walker as Jane Winters Robert McKenzie as Deacon Ames
*Karl Hackett as Storm
*Donald Curtis as Henchman Snapper
*Gene Alsace as Henchman Red
*Olin Francis as Mule Bates
*Carleton Young as Ace Hutchinson
*George Eldredge as Sheriff
*Johnny Lee Wills as Texas Playboy
*Leon McAuliffe as Texas Playboy
*Son Caz Lansford as Texas Playboy
*Wayne Johnson as Texas Playboy
*Eldon Shamblin as Texas Playboy
*White Flash as Texs Horse

== Soundtrack ==
* "Village Blacksmith" (by Lew Porter and Johnny Lange)
* "Kalamity Kate" (by Lew Porter and Johnny Lange) Charles Mitchell)
* "Good Old Oklahoma" (by Bob Wills)
* "Take Me Back To Tulsa" (by Bob Wills)
* "Going Indian" (by Bob Wills)
* "Lone Star Rag" (by Bob Wills)
* "Bob Wills Special" (by Bob Wills)

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 