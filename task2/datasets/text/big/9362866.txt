King of Beggars
 
 
{{Infobox film name = King of Beggars image = KingofBeggars.jpg caption = Film poster traditional = 武狀元蘇乞兒 simplified = 武状元苏乞儿 pinyin = Wǔ Zhuàngyuán Sū Qǐér jyutping = Mou5 Zong6-jyun4 Sou1 Hat1-ji1}} director = Gordon Chan producer = Stephen Shiu writer = Gordon Chan John Chan starring = Stephen Chow Sharla Cheung Ng Man-tat Norman Tsui music = Joseph Koo cinematography = David Chung Ma Koon-wah editing = Mei Fung Kwong Chi-leung Yu Sai-lun studio = Wins Movie Productions distributor = Gala Film Distribution Limited released =   runtime = 100 minutes country = Hong Kong language = Cantonese budget = gross = HK$31,514,995 
}}

King of Beggars is a 1992 Hong Kong martial arts comedy film directed by Gordon Chan, starring Stephen Chow, Sharla Cheung, Ng Man-tat and Norman Tsui. The story is loosely based on legends about the martial artist Su Can (better known as "Beggar So"), who lived in the late Qing dynasty and was one of the Ten Tigers of Canton.

==Plot==
Set in the  .

So Chan is the spoiled son of a wealthy general in Guangzhou|Canton. Although he is lazy and illiterate, he excels in martial arts. While visiting a brothel, So falls in love with Yu-shang, a courtesan who dares to behave rudely towards him. So vies for Yu-shangs services with Chiu Mo-kei, a high-ranking government official. By outbidding Chiu, So inadvertently foils Yu-shangs attempt to assassinate Chiu to avenge her father, who was murdered by Chiu. Yu-shang agrees to marry So if he can win the title of "Martial Arts Champion" (武狀元).

To win Yu-shangs hand-in-marriage, So enters the imperial martial arts contest to win the championship title. Sos father helps him cheat his way through the written examination, while his personal expertise in martial arts carries him through the physical tests. So eventually emerges as champion, but just as the emperor is about to grant him the title, Chiu reveals that So is illiterate, proving that he cheated in the written examination. The enraged emperor orders Sos family properties and possessions to be confiscated and decrees that they shall remain as beggars for the rest of their lives.
 Hung Tsat-kung. Using his improved literacy, he reads the sects ancient martial arts manual and learns seventeen of the "Eighteen Dragon Subduing Palms" (降龍十八掌), while the last style is not shown in the book.

Meanwhile, Chiu puts Yu-shang into a magical trance and attempts to use her as a puppet to assassinate the emperor and start a rebellion. So leads his beggar followers across to Great Wall of China to stop Chiu and they engage Chius forces while So saves Yu-shang in the nick of time. So uses all the skills he had learnt to fight Chiu, but they prove insufficient to completely defeat Chiu. When Chiu conjures a windstorm, Sos manual falls out and forms a flip book which animates the first seventeen of the "Eighteen Dragon Subduing Palms". So suddenly realises that the final stance is a combination of the seventeen palms and he uses it to destroy Chiu and save the emperor.

In the final scenes, Yu-shang agrees to marry So, and the grateful emperor asks So what reward he desires. So chooses to remain as a beggar king and the emperor expresses worries about So wielding much influence over the masses. So reminds him that as long as the people are cared for, there will be not enough beggars to pose a threat to the emperor. Before the film ends, So and Yu-shang are seen wandering the streets with their large family, using an imperial tablet to force rich people to give them money.

==Cast==
* Stephen Chow as So Chan / "Beggar So"
* Sharla Cheung as Yu-shang / Ru-shuang
* Ng Man-tat as General So
* Vindy Chan as Tracy
* Norman Tsui as Chiu Mo-kei
* Lam Wai as Sengge Rinchen
* Wong Chung as Uncle Mok
* Natalis Chan as Secretary of Justice
* Lawrence Cheng as Professor
* Matthew Wong as Emperor
* Peter Lai as Uncle Cheng
* Jackson Ng as Botaroto
* Yang Mi as So Chans daughter
* Chow Yee-fan
* Lee Kin-yan
* Kingdom Yuen
* Yuen Cheung-yan

 {{Cite web |url=http://www.imdb.com/title/tt0100963/ |title=King of Beggars 
 |accessdate=29 June 2010 |publisher=imdb.com}} 
   

==Music==
The films theme song, Cheung-lo Man-man Bun-nei Chong (長路漫漫伴你闖; The Long Road Accompanies You On Your Adventure), was sung by George Lam in Cantonese.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 