The Mailman (film)
{{Infobox film
| name           = The Mailman
| image          = TheMailman2004Film.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = DVD cover.
| director       = Tony Mark
| producer       = Erin Starr   Todd Wade
| writer         = Brian Mazo
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Ari Tinnen   Jesse Merrill   Rob Arbogast   Bryan Lukasik   Jamielyn Kane   Diana Kauffman   Gordon Anthony Davis
| music          = David Weinstein
| cinematography = Courtney Jones
| editing        = Tony Mark   Todd Wade
| studio         = Rolling Pictures, Inc.
| distributor    = IFM World Releasing
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $33,000 
| gross          = 
}}
 thriller directed Tony Mark, and written by Brian Mazo.

== Plot ==

Returning home from school one day in 1986, a young Darius Foxx witnesses his mailman father shoot his mother and her lover after walking in on them together. After killing the couple, Dariuss father looks directly into his sons eyes, and shoots himself in the mouth. Sixteen years later, Darius moves to the town of Smithfield as the new mailman, cryptically telling anyone who asks that his predecessor is "out of town" for an indeterminate amount of time. Behind his initially cheerful facade, Darius is a psychopath who spends his free time reading and tampering with the mail of people on his route, rewriting letters and smashing packages, among other acts of vandalism. Aiding Darius is a local teenage delinquent named Daniel Everson, who had become disillusioned after discovering that his older sister, Beth, and best friend, Jay, are dating, and after being told by Darius that he is adopted. Unbeknownst to Daniel however, Darius is a serial killer who murders people who become suspicious of him, such as a gas meter man, and Daniels girlfriend, Veronica.

After the two enact a plan to blackmail the mayor using information on his familys illegal activities they had gleaned from the mail, Darius reveals to Daniel that he is his biological brother, and that he had moved to Smithfield to find and reconnect with him. Shortly after returning to Dariuss house, Darius sends Daniel out to get beer, and while he is gone, Darius kills a hitman sent by the mayor to deal with him. Just as Darius disposes of the hitmans body, Beth and Jay break in, having become concerned about the new mailmans activities, and his relationship with Daniel. After the two discover the body of the original mailman (the actual owner of the house) stuffed in a closet, Darius appears, knocks them out, and ties them up, intent on raping Beth and forcing Jay to watch. As Darius strips Beth, Daniel returns, and is ordered to kill Beth by Darius, who says she is not his real sister, and that her family will never truly accept him like he does. Instead of obeying his brother, Daniel stabs him with a letter opener, and releases Beth and Jay. While a hysterical Daniel breaks down sobbing, Beth and Jay embrace, not noticing Dariuss eyes open.

== Cast ==

* Rob Arbogast as Darius Foxx
* Ari Tinnen as Beth Everson
* Bryan Lukasik as Daniel Everson/William Foxx
* Jesse Merrill as Jay
* Gil Zuniga as Jack Everson
* Mari Levitan as Beverly Everson
* Jamielyn Kane as Veronica
* Gordon Anthony Davis as Mayor Eastman
* Diana Kauffman as Lisa
* Danielle Petty as Ms. Sinclair
* Sally Robbins as Ms. Pickard
* Dan Harper as Meter Man
* James Thomas as Vince Eastman
* Trumayne Bolden as Vance Eastman
* Jennifer Snow as Mindy
* Dawn Shindle as Wendy
* Blu Fox as Messenger
* Thomas Farnsworth as Young Darius Foxx
* Amie Barsky as Mrs. Foxx
* Alan Waserman as Mr. Foxx
* Paul J. Alessi as Lover

== Reception ==

Dread Central gave The Mailman a score of one and a half out of five, claiming it "just never gets to any sort of peak or level that draws in the viewer" and "With so much potential to be a deep thriller about family ties and morals, it is hard to believe The Mailman ends up being just another one of those cheap films that goes nowhere and does nothing".  A score of two out of five was awarded by Moria, which wrote that while The Mailman was bogged down by its low-budget, sub-par cinematography and melodramatic direction, it is still "a wonderfully sordid little film" with Bryan W. Lukasik, Ari Tinnen and Rob Arbogast giving worthwhile performances. 

The Mailman was labelled ridiculous and given one star out of five by Slasherpool, though the website did admit that the film looked good, and that the acting was decent. 

== See also ==
 The Paperboy, a similar film from 1994.

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 