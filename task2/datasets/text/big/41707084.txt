Back in the Day (2014 film)
{{Infobox film
| name           = Back in the Day
| image          = Back in the Day.jpg
| alt            = 
| caption        = 
| director       = Michael Rosenbaum
| producer       = Kim Waltrip
| writer         = Michael Rosenbaum
| starring       = Morena Baccarin Nick Swardson Michael Rosenbaum
| music          = Rob Danson
| cinematography = Bradley Stonesifer
| editing        = Sandy S. Solowitz
| studio         = 
| distributor    = Screen Media Films
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Back in the Day is a 2014 comedy film, directed and written by Smallville actor Michael Rosenbaum. It is distributed by Screen Media Films. 

==Cast==
* Michael Rosenbaum as Jim Owens
* Morena Baccarin as Laurie
* Kristoffer Polaha as Len Brenneman
* Isaiah Mustafa as T
* Harland Williams as Skunk
* Emma Caulfield as Molly
* Liz Carey as Angie Kramer
* Sarah Colonna as Carol
* Nick Swardson as Ron Freeman
* Danielle Bisutti as Annette Taylor
* Jay R. Ferguson as Mark
* Mike Hagerty as Principal Teagley
* Richard Marx as Neighbor

==Reception==
At Metacritic, the film received an "overwhelming dislike" after it only received seven out of a hundred. 
Rotten Tomatoes gave the film a rating of 3.8/10 based on 7 reviews, hitting 14% on the Tomatometer. 

==External links==
* 

==References==
 

 
 
 

 