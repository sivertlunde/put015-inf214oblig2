Curfew (2012 film)
 
{{Infobox film
| name = Curfew
| director = Shawn Christensen
| producer = Damon Russell Mara Kassin Andrew Napier
| writer = Shawn Christensen Kim Allen
| cinematography = Daniel Katz
| studio = Fuzzy Logic Pictures
| soundtrack = Alexander "Truth" / Goodnight Radio "Sophia So Far" / Boy Friend "Lazy Hunter" / Adam & The Amethysts "Prophecy" / Avec Pas DCasque "Si on change les équipes ce n’est plus une revanch”/ Parallels "Dry Blood" / 
| score =  Darren Morze (score) 
| runtime = 19 minutes
| country = United States
}}
Curfew is a 2012 short film directed by Shawn Christensen. The film won the Academy Award for Best Live Action Short Film at the 85th Academy Awards. 
 SXSW 2014 titled Before I Disappear.

==Plot==
Richie is in the process of ending his life in a bathtub, when he gets a call from his estranged sister, Maggie, asking him to look after his niece, Sophia, for the night. Richie cancels his plans and sets out to babysit his niece.

When he meets Sophia, she makes it clear that she has no interest in talking to him, nor does she seem to care much about him. Richie mentions that he drew flipbooks when he was younger, starring a protagonist named “Sophia”, and that he wonders if his sister got Sophia’s name from those flipbooks. He then takes Sophia to an old rundown building where he used to live, and finds the flipbooks he wants to show her, but Sophia gets scared and wants to go home.

After Richie apologizes, they return to the bowling alley and Sophia starts asking all about his life. They start to become friends, and Richie admits that the reason he hasn’t been allowed to see her all these years, is because he once dropped Sophia on her head while taking care of her as a baby. Sophia finds this incident amusing, just as her favorite song comes on over the loudspeakers. Suddenly, everyone in the bowling alley seems to be dancing along with the song, except for Richie. Sophia begs him to dance with her, tugging at his arm until his wrist comes out of its sleeve, revealing his suicide attempt. Richie snaps back to reality.

When Richie brings Sophia back home, he notices a restraining order sitting on the kitchen table, citing assault and harassment. His sister comes back looking bruised and she thanks Richie for his help, but she wants him to leave. She doesn’t want her daughter having any more “false idols”. Richie tells her how much he looked up to her when they were younger, and how much he still looks up to her now. He returns home to his bathtub, and attempts to continue what he started in the beginning but Maggie interrupts him with a phone call again, this time on nicer terms.

==Awards==

{| class="wikitable" style="text-align: center"
|----- bgcolor="#94bfd3"
| colspan=4 align=center | Awards for Curfew
|----- bgcolor="#94bfd3" 
!width="90"|Year
!width="250"|Association
!width="300"|Award Category
!width="90"|Status
|- 2013 in 2013
| Academy Awards
| Best Short Film - Live Action
|  
|-
| Prescott Film Festival
| Best Narrative Short Film
|  
|-
| Cortopolis
| Audience Award
|  
|-
| Lakecity International Short Film Festival
| Best Foreign Film
|  
|-
| Hill County Film Festival
| Best Short Film
|  
|-
| rowspan="2" | Vaughan Film Festival
| Best Short Film
|  
|-
| Best Actress
|  
|-
| Film Caravan
| Audience Award
|  
|-
| rowspan="2" | Rincon International Film Festival
| Best Actress
|  
|-
| Best Short Film
|  
|- 2012 in 2012
| Stockholm International Film Festival
| Best Short Film
|  
|-
| Nashville Film Festival
| Best Narrative Short Film
|  
|-
| Cleveland International Film Festival
| Best Live Action Short Film
|  
|-
| Woodstock Film Festival
| Best Short Narrative
|  
|-
| Clermont-Ferrand Short Film Festival
| Audience Award (International)
|  
|-
| rowspan="2" | Brussels Short Film Festival
| Best Actor
|  
|-
| Audience Award (International)
|  
|-
| rowspan="2" | Ozu International Film Festival
| Grand Jury Prize
|  
|-
| Audience Award
|  
|-
| rowspan="2" | Sapporo Short Film Festival
| Best Young Actress
|  
|-
| Audience Award
|  
|-
| Granada International Film Festival
| Best International Short Film
|  
|-
| rowspan="3" | Seattle Shorts Film Festival
| Grand Jury Prize
|  
|-
| Best Director
|  
|-
| Best Actor
|  
|-
| Short Shorts Film Festival
| Audience Award (International)
|  
|-
| Tacoma Film Festival
| Best Narrative Short Film
|  
|-
| rowspan="3" | 24fps Intl Short Film Festival
| Best Actor
|  
|-
| Silver Medal
|  
|-
| Audience Award
|  
|-
| 15 Short Film Festival
| Audience Award
|  
|-
| Concorto Film Festival
| Youth Jury Award 
|  
|-
| La Boca Del Lobo Intl Film Festival
| Best International Short Film
|  
|-
| rowspan="4" | Soria International Short Film Festival
| Best Screenplay
|  
|-
| Grand Jury Prize (International)
|  
|-
| Audience Award
|  
|-
| Youth Jury Award (International)
|  
|-
| Windsor International Film Festival
| Can-Am Peoples Choice Award
|  
|-
| Williamstown Film Festival
| Christopher Reeve Audience Award
|  
|-
| Saguenay International Short Film Festival
| Creativity Prize
|  
|-
| Abitibi-Temiscamingue Intl Film Festival
| Best Short Film
|  
|-
| rowspan="4" | Du Grain a Demoudre Film Festival
| Best Short Screenplay
|  
|-
| Audience Award
|  
|-
| Young Cinephiles Award
|  
|-
| Jean Prevost High School Award
|  
|-
| Sequence Short Film Festival
| Audience Award (International)
|  
|-
| Port Townsend Film Festival
| Special Jury Commendation
|  
|-
|}

==Release==
The film was released in February 2013 on iTunes and in a theatrical run with the other 14 Oscar-nominated short films by ShortsHD.   

==References==
 

==External links==
*  
*  

 
 

 
 
 


 