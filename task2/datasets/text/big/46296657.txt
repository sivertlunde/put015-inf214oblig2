Drishyam (2015 film)
 
 
{{Infobox film
| name           = Drishyam
| image          = Drishyam movie first look.png
| caption        = First Look
| director       = Nishikant Kamat
| producer       = Kumar Mangat Pathak Ajit Andhare Abhishek Pathak
| story          = Jeethu Joseph
| writer         = Upendra Sidhaye Tabu Shriya Saran Rajat Kapoor
| music          =  
| studio         = Panorama Studios Viacom 18 Motion Pictures
| distributor    = Viacom 18 Motion Pictures
| released       =  
| country        = India
| language       = Hindi
}} Indian thriller Tabu and Malayalam film of the same name.  The film is scheduled for release on 31 July 2015. 

==Cast==
* Ajay Devgn Tabu 
* Shriya Saran 
* Rajat Kapoor

==Production== Tabu will be playing the role of a cop and Shriya Saran will play the role of Ajay Devgns wife.   

==Filming== Malayalam & Telugu speaks for itself."     Actor Ajay Devgn had been in Canada to shoot snow scenes for his upcoming film Shivaay, but light snow conditions had him reschedule that film and return to India to instead begin Drishyam.        The First 20 Day Shooting schedule of the film began on 13 march 2015 in Goa and ended on 1 April 2015. The second schedule of the film will began on 2nd week of April.   

==References==
 

==External links==
*  

 