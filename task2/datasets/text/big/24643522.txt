Floored (film)
{{Infobox Film
| name = Floored
| image =
| image_size =
| caption = 
| director = James Allen Smith
| producer = Joseph Gibbons
| writer = Andrew McAllister, James Allen Smith
| narrator =
| starring = Bobby Ansani Jeff Ansani Ron Beebe
| music = Stefan Nelson
| cinematography = Chris Baron James Allen Smith
| editing = Andrew McAllister
| distributor = 
| released = September 1, 2009
| runtime = 77 minutes
| country = United States
| language = English
| budget = 
| gross =
| followed_by =
}}

Floored is a 2009 documentary film about the people and business of the Chicago trading floors. The film focuses specifically on several Chicago floor traders who have been impacted by the electronic trading revolution and whose jobs have been threatened by the use of computers in the trading world. Directed by James Allen Smith, the film runs for 77 minutes.
 financial crisis of 2009; Greg Burns of the Chicago Tribune has said of the films director, "By dealing with hard times in the workplace, Smith taps into a vein running through millions of lives in this brutal recession, as the nation’s unemployment rate soars toward 10 percent"  

Critics have suggested a Metafiction|meta-fictional subtext to the documentary, linking the films storyline about Chicago traders to the plight of independent filmmakers in a time of the declining influence of film festivals and the ubiquitousness of free internet content.  Ironically, the documentary itself was widely available on video sharing sites before its official "web premiere" on September 6, 2013. 

==Notes==
 

==External links==
* 
*  New York Times. 2010-05-07
*  Futures Magazine. 2013-08-13

 
 
 
 
 
 
 
 
 


 