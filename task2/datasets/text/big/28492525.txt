S.P.U.K.
 
 
 
 
 

{{Infobox film
| name           = S.P.U.K.  AKA  Sreća Pojedinca - Uspjeh Kolektiva
| image          = 
| alt            = 
| caption        = 
| director       = Milivoj Puhlovski
| producer       =  
| writer         = Hrvoje Hitrec
| screenplay     = 
| story          =  
| based on       =  
| starring       =  
| music          = 
| cinematography = Enes Midzic
| editing        = Robert Lisjak production companies =  
| distributor    =  
| released       =  
| runtime        = 99 minutes
| country        = Yugoslavia
| language       =  
| budget         = 
| gross          = 
}} Yugoslavian comedy film written by Hrvoje Hitrec and directed by Milivoj Puhlovski which released in 1983.      

==Plot==
The Governments Sava Commander wants his youth labor brigade to be the best, but this goal seems difficult to reach when he sees graffiti being repeated all over his area of command. The Commander begins to hunt for the graffiti makers, but they are difficult to find and stop.

==Cast==
 
* Damir Saban as Lovro
* Cintija Asperger as Vlasta
* Danko Ljustina as Vlado
* Zvonimir Juric as Kuhar
* Radoslav Spitzmuller as Boro
* Elizabeta Kukic as Koka
* Branimir Vidic as Mrva
* Predrag Predjo Vusovic as Redford
* Vili Matula as Nindja
* Pjer Zardin as Clapton
* Mario Mirkoviv as Miso
* Anja Sovagovic-Despot as Bolnicarka
* Ivanisevic Nedeljko as Bilder
* Tanja Mazele as Zita
* Borivoj Zimonja as Radio-voditelj
* Tomislav Lipljin as Lijecnik
* Milan Plecas as Sekretar
* Otokar Levaj as Licilac
* Slavica Knezevic as Seksolog
* Djuro Utjesanovic as Dermatolog
* Mladen Crnobrnja as Sociolog
* Djuro Crnobrnja as Kino-operater
* Zdenka Trach as Teta
* Zeljko Haberstok as Ribic
* Zeljka Galler as Redfordova djevojka
* Emira Filipovic as Zvjezdana
* Mehdi Jashari as Medo / Dobrovoljac
* Ranko Vujisic as Recitator
* Zlatko Rado as Dezurni
* Mirko Susic as Pomocnik
* Vlasta Klemenc as Miss
* Slobodanka Burcel as Boba
 

==Reception==
In 2013, Jutarnji called the film a cult comedy classic.   

==References==
 
==External links==
*  

 
 
 
 
 