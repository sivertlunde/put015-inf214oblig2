Ataque de Pánico!
{{Infobox film name = Ataque de Pánico! image = Ataque de panico.JPG caption = Screenshot from the film director = Fede Álvarez writer = Fede Álvarez starring = Christian Zagia Sergio Rondan Diego Garrido Ariadna Santini producer = Emiliano Mazza Fede Álvarez music = John Murphy Rodrigo Gómez cinematography = Pedro Luque editing = Fede Álvarez distributor =  studio = MURDOC Films  released =   runtime = 4:48  country = Uruguay language = Spanish budget = $300  gross =
}} science fiction short film, directed by independent filmmaker Fede Álvarez. 

==Plot== Giant robots appear out of the mist and attack Montevideo, the capital of Uruguay. Accompanied by a squadron of spacecraft, they fire weapons at the city and destroy key buildings, leading to mass panic. The military fights back to little avail. At the end of the film, the robots fuse together to form a giant sphere, which then detonates and engulfs the city in a fireball. No explanation is given for the attack.

==Production== trailer of the film was uploaded to YouTube in October 2006, with some scenes from the finished version.  The official production budget of the film was given as only $300. In addition to writing, editing and directing the film, Álvarez created the visual effects based on computer-generated imagery. 

==Release==
The film was premiered on October 31, 2009 at the   deal to develop and direct a full-length film. " ". BBC. December 17, 2009. Retrieved 2009-01-01. 

==Soundtrack== John Murphy.  The music was originally composed for the 2002 film 28 Days Later.

==Reaction==
After being uploaded to YouTube, the films reputation spread by word of mouth, and received a boost when it was linked from the blog of  . November 30, 2009. Retrieved 2009-01-01.  Fede Álvarez stated in a BBC interview: "I uploaded Panic Attack! on a Thursday and on Monday my inbox was totally full of e-mails from Hollywood studios." 
 Ghost House Evil Dead, Evil Dead franchise, released in the United States on April 5, 2013. "  Orlando Weekly. April 12, 2013. Retrieved 2013-04-13. 

The film has been cited as an example of the increasing influence of the Internet in finding new talent for Hollywood studios. 

==Cast==
*Diego Garrido
*Pedro Luque
*Ariadna Santini
*Rodolfo Sayagues
*Martín Sarthou (actual Uruguayan news anchor)

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 