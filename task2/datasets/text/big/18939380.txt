A Place to Go
{{Infobox film

 | name = A Place to Go
 | image size     =
 | alt = 
 | caption = 
 | director = Basil Dearden
 | producer = Michael Relph
 | writer = Clive Exton Michael Fisher Michael Relph
 | narrator = 
 | starring = Bernard Lee Rita Tushingham Charles Blackwell
 | cinematography = Reginald H. Wyer
 | editing = John D. Guthridge
 | studio =  Bryanston Films
 | released = July 1963 
 | runtime = 86 minutes
 | country = United Kingdom
 | language = English
 | budget = 
 | gross = 
 | preceded by    =
 | followed by    =
 | image= A Place to Go VideoCover.jpg
}} 1963 British kitchen sink school of film making which was extremely popular in Britain at the time.

==Cast==
* Bernard Lee as Matt Flint
* Rita Tushingham as Catherine Donovan
* Michael Sarne as Ricky Flint
* Doris Hare as Lil Flint
* Barbara Ferris as Betsy John Slater as Jack Ellerman David Andrews as Jim
* William Marlowe as Charlie Batey
* Michael Wynne as Pug
* Roy Kinnear as Bunting
* Norman Shelley as Magistrate
* Jerry Verno as Nobby Knowles

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 