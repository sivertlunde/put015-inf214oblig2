Budget Padmanabham
{{Infobox film
| name           = Budget Padmanabham
| image          = Volga_videos_poster.jpg
| image_size     =
| caption        =
| director       = S. V. Krishna Reddy
| producer       = Grandi Narayana Rao (Babji)
| writer         = Divakar Babu  
| story          = G. Arunachalam
| screenplay     = S. V. Krishna Reddy
| narrator       =
| starring       = Jagapathi babu Ramyakrishna
| music          = S. V. Krishna Reddy
| cinematography = Sarath
| editing        = Nandamuri Hari 
| studio         = Sri Dhanalakshmi Films
| distributor    =
| released       =  
| runtime        = 2:31:00
| country        = India
| language       = Telugu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Telugu film and remake of Tamil film Budget Padmanabhan   directed by S. V. Krishna Reddy and produced by G. Naraya Rao . Jagapathi babu and Ramyakrishna played the lead roles. 

==Plot==
Padmanabham ( ) pile on to him and then Ramyas good-for-nothing brother Ravi (Ravi Teja) also seeks shelter in their house. The worst, though, is yet to come for the grumbling Padmanabham: his wife gives birth to triplets.

==Cast==
 
*Jagapati Babu as Padmanabham
*Ramyakrishna as Ramya
*Vidya as Sonali
*Ravi Teja as Ravi
*Anil
*Ahuti Prasad as Padmanabhams father
*Tanikella Bharani
*L.B. Sriram as Totti Subrahmanyam alias Vaastu Subrahmanyam Rallapalli as lawyer
*Y. Vijaya
*M. S. Narayana as Padmanabhams colleague parrot astrologer Jenny as Subrahmanyams house owner
*Gowtam Raju
*Namala Murthy as Ramaiah
*Madhuvani
*Rajita
*Sailaja
*Nagaraju
 

==Soundtrack==
{{Infobox album
| Name        = Budget Padmanabham
| Tagline     = 
| Type        = Soundtrack
| Artist      = S. V. Krishna Reddy
| Cover       = 
| Released    = 2001
| Recorded    = 
| Genre       = Soundtrack
| Length      = 24:36
| Label       = Supreme Music
| Producer    = S. V. Krishna Reddy
| Reviews     =
| Last album  = Kodanda Ramudu   (2000)
| This album  = Budget Padmanabham   (2001)
| Next album  = Premaku Swagatam   (2002)
}}

Music composed by S. V. Krishna Reddy. Lyrics written by Chandrabose (lyricist)|Chandrabose. Music released on Supreme Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 24:36
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Monalissa Monalissa  Usha
| length1 = 5:10

| title2  = Bava Bava  SP Balu, Unni Krishnan
| length2 = 5:12

| title3  = Padakintlo Ee Kshanam
| extra3  = Pankaj Udas,Nitya Santhoshini
| length3 = 5:09

| title4  = Sommutha Aadaa Cheyyara
| extra4  = SP Balu
| length4 = 4:09

| title5  = Evaremi Anukunna  
| extra5  = SP Balu
| length5 = 4:56
}}

== References ==
 

 

 
 
 
 