Lambert & Stamp
{{Infobox film
| name           = Lambert & Stamp
| image          = LambertandStamp.jpg
| alt            = 
| caption        = Sundance film poster
| director       = James D. Cooper
| producer       = James D. Cooper Douglas Graves Loretta Harms
| writer         = 
| starring       = 
| music          = The Who
| cinematography = James D. Cooper
| editing        = Christopher Tellefsen
| studio         = Harms/Cooper, Motorcinema, Inc. Production
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English French German 
}}
Lambert & Stamp is a 2014 American documentary film, produced and directed by James D. Cooper.   The film had its world premiere at 2014 Sundance Film Festival on January 20, 2014.  

After its premiere at Sundance Film Festival, Sony Pictures Classics acquired the distribution rights of the film.   The film later screened at 2014 Sundance London Film Festival on April 18, 2014.   The film will have a theatrical release in United States on April 3, 2015. 

==Synopsis==
The film narrates that how aspiring filmmakers Chris Stamp and Kit Lambert, searching a subject for their underground movie, wound up discovering, mentoring and co-managing English rock band The Who.

==Reception==
Lambert & Stamp received positive reviews from critics. Rob Nelson of Variety (magazine)|Variety, said in his review that "James D. Coopers impeccably directed debut is a definitive screen bio of the Who and its-rock operatic rise."  David Rooney in his review for The Hollywood Reporter praised the film by saying that "A wonderfully alive behind-the-music chronicle that rescues two genuine mavericks from the footnotes of rock history." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 