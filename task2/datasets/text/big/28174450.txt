Bad Teacher
 
{{Infobox film
| name           = Bad Teacher 
| image          = Bad Teacher Poster.jpg
| caption        = Theatrical release poster
| director       = Jake Kasdan
| producer       = Jimmy Miller David Householter
| writer         = Lee Eisenberg Gene Stupnitsky 
| starring       = Cameron Diaz Justin Timberlake Lucy Punch John Michael Higgins  Phyllis Smith Jason Segel
| cinematography = Alar Kivilo Michael Andrews
| editing        = Tara Timpone Radar Pictures Mosaic Media Group
| distributor    = Columbia Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $20 million  
| gross          = $216,197,492   
}}
Bad Teacher is a 2011 American comedy film directed by Jake Kasdan based on a screenplay by Lee Eisenberg and Gene Stupnitsky, starring Cameron Diaz, Justin Timberlake, Lucy Punch and Jason Segel. The film was released in the United Kingdom on June 17 and in the United States and Canada on  , 2011.

== Plot ==
Elizabeth Halsey (Cameron Diaz) is an immoral, gold-digging Chicago-area teacher at the fictional John Adams Middle School who curses at her students, drinks heavily, smokes Cannabis (drug)|marijuana, and shows movies while she sleeps through class. She plans to quit teaching and marry her wealthy fiancé, but when he dumps her after realizing she is only after his money, she must resume her job. She tries to win over substitute teacher Scott Delacorte (Justin Timberlake), who is also wealthy. Amy Squirrel (Lucy Punch), a dedicated but overly enthusiastic teacher and colleague of Elizabeth, also pursues Scott while the schools gym teacher, Russell Gettis (Jason Segel), makes it clear that he is interested in Elizabeth romantically. She, however, is not interested in him. 
 enlarge her breasts, and becomes all the more motivated to do so once she learns Scotts ex-girlfriend had large breasts. However, she cannot afford the $9,300 procedure. To make matters worse, Scott admits that he has a crush on Amy, only viewing Elizabeth as a friend. Elizabeth attempts to raise money for the surgery by participating in her 7th grade class car wash in provocative clothing and by manipulating parents to give her money for more school supplies and tutoring, but her efforts are not enough. Amy, acting on the growing resentment between them due to Elizabeth pursuing Scott and ignoring school rules, attempts to warn the principal about Elizabeths embezzlement scheme, but he dismisses her claims as groundless.
 Thomas Lennon), a state professor who is in charge of creating and distributing the exams. Elizabeth convinces Carl to go into his office to have sex, but instead drugs him with alcoholic beverages and steals the test answers. A month later, Elizabeth wins the bonus, completing her needed funds, and pays for the appointment to get her breasts enlarged.
 dry hump and Elizabeth secretly calls Amy using Scotts phone leaving a message recording all the action, ensuring she knows about the affair. However, Scotts peculiar behavior, which was subtly exposed by Russell when Scott would agree with anything even if its contradictory, disappoints Elizabeth. Elizabeth later gives advice to one of her students (Matthew J. Evans) who has an unrequited crush on the superficial Chase (Kathryn Newton) in class, which causes her to reflect on how she has been superficial as well.

Left behind at the school, Amy switches Elizabeths desk with her own to trick the janitor into unlocking Elizabeths sealed drawer. Amy finds Elizabeths journalist disguise and drugs, which leads her to suspect Elizabeth cheated on the state exam. Amy informs the principal and gets Carl to testify against her. However, Elizabeth took embarrassing photos of Carl while he was drugged and uses them to blackmail him to say she is innocent. Having noticed her desk was switched with Amys, Elizabeth informs the principal that some teachers in the school are doing drugs. When the police arrive and bring their sniffer dog to search the school, they find Elizabeths mini liquor bottles, marijuana and OxyContin pills in Amys classroom, in Elizabeths desk, unaware that while Amy switched the desks, she framed herself. Amy is arrested and transferred to the worst school in the county by the superintendent. Scott asks Elizabeth to start over, but Elizabeth rejects him in favor of a relationship with Russell.
 guidance counselor.

== Cast ==
* Cameron Diaz as Elizabeth Halsey, a gold-digging, drug-abusing teacher
* Lucy Punch as Amy Squirrel, Elizabeths co-worker/rival who attempts to discredit Elizabeth
* Jason Segel as Russell Gettis, a gym teacher who is smitten with Elizabeth
* Justin Timberlake as Scott Delacorte, a substitute teacher whom Elizabeth likes
* Phyllis Smith as Lynn Davies, Elizabeths best friend and fellow teacher
* John Michael Higgins as Wally Snur, the principal at JAMS Dave Allen as Sandy Pinkus
* Molly Shannon as Melody Tiara, Garretts mother who invites Elizabeth to spend Christmas with her family
* Eric Stonestreet as Kirk, Elizabeths roommate Thomas Lennon as Carl Halabi, an educator who gets seduced and blackmailed by Elizabeth
* Nat Faxon as Mark, Elizabeths wealthy fiancé who dumps her at the beginning of the film
* Kaitlyn Dever as Sasha Abernathy, a student who seems to idolize Elizabeth
* Matthew J. Evans as Garrett Tiara, a lovestruck boy
* Noah Munck as Tristan Munck, one of Elizabeths students
* Kathryn Newton as Chase Rubin-Rossi, Garretts crush
* Aja Bair as Devon, Gaby and Chase Rubin-Rossis friend, later ends up with Garret
* Andra Nechita as Gaby, Devon and Chase Rubin-Rossis friend Christine Smith as Doctors Assistant (one with augumented breasts)

== Production ==
Bad Teacher is directed by Jake Kasdan based on a screenplay by Lee Eisenberg and Gene Stupnitsky. Columbia Pictures purchased Eisenberg and Stupnitskys spec script in August 2008.  In May 2009, Kasdan was hired to direct Bad Teacher.  The following December, Cameron Diaz was cast in the films lead role.  Justin Timberlake was cast opposite Diaz in March 2010, and filming began later in the month. 

== Release ==

=== Box office performance ===
The film was released in North America on June 20, 2011 in 3,049 theaters. It took in $12,243,987—$4,016 per theater—in its opening day, and grossed a total of $31,603,106 in its opening weekend, finishing second at the box office, behind Cars 2.  In Germany, the film reached No. 1 on the Media Control|countrys Cinema Charts in its opening week after 496,000 people saw the film. This caused Kung Fu Panda 2, which reached No. 1 the week before, to fall to No. 2.  The film grossed $100.3 million in the U.S.A. and Canada while its worldwide total stands at $216.2 million. 

=== Critical reaction ===
The film received mixed reviews from critics. As of July 2014, review aggregator Rotten Tomatoes reported that 45% of 177 critics had given the film a positive review, with a rating average of 5.3 out of 10. The sites consensus was that "In spite of a promising concept and a charmingly brazen performance from Cameron Diaz, Bad Teacher is never as funny as it should be."  Metacritic reported the film had an average score of 47 out of 100, based on 38 reviews.  CinemaScore polls reported that the average grade moviegoers gave the film was a C+.   

=== Accolades ===
{| class="wikitable"
! Award !! Category !! Recipient(s) !! Result
|- 2011 ALMA Awards  
| Favorite Movie Actress – Comedy/Musical
| Cameron Diaz 
|  
|-
| rowspan="3" | 2011 Teen Choice Awards 
| Choice Movie – Comedy
|
|  
|-
| Choice Movie Actor – Comedy
| Justin Timberlake
|  
|-
| Choice Movie Actress – Comedy
| Cameron Diaz
|  
|-
| rowspan="2"| 33rd Young Artist Awards  
| Best Performance in a Feature Film - Supporting Young Actor
| Matthew J. Evans
|  
|-
| Best Performance in a Feature Film - Supporting Young Actress
| Kaitlyn Dever
|  
|- 2012 BMI Film & TV Awards 
| Film Music Award Michael Andrews
|  
|}

===Home media===
Bad Teacher was released on DVD, Blu-ray, and a combo pack on October 18, 2011. 

==Sequel and TV series==
 
 
On June 20, 2013, Sony announced that it was working on Bad Teacher 2. The company hired Justin Malen to write the sequel. Lee Eisenberg and Gene Stupnitsky, who wrote the first film, will return as producers. A release from Sony studios said the project is "being developed for Cameron Diaz to star in the film but no deal is yet set with the actress." Jake Kasdan will again be the director. 

On May 23, 2013,   played the Cameron Diaz role,  while Sara Gilbert, Ryan Hansen, David Alan Grier, Kristin Davis and Sara Rodier also appeared. On May 10, 2014, CBS canceled Bad Teacher after airing only three episodes.  Bad Teacher last aired on May 22, 2014.  However, in July of 2014, CBS aired the remaining unseen episodes by showing two episodes on Saturday nights.

== References ==
 

== External links ==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 