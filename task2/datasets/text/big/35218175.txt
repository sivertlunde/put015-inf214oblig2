Mondomanila
 
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Mondomanila
| image          =
| caption        = Theatrical release poster
| director       = Khavn
| producer       = Kamias Road Stephan Holl Antoinette Köster Achinette Villamor
| screenplay     = Norman Wilwayco & Khavn  
| music          = Khavn
| starring       = Tim Mabalot Marife Necesito Palito Whitney Tyson
| world sales    = House of Film Rapid Eye Movies HE GmbH Hubert Bals Fund
| released       =  
| runtime        = 75 minutes
| country        = Philippines
| language       = Tagalog
}} Filipino crime comedy drama film directed by independent filmmaker Khavn. It is based on thework by Norman Wilwayco, who also co-wrote the script with Khavn. It premiered at the International Film Festival Rotterdam in January 2012.   

Mondomanila merges drama, musical, horror, experimental, and exploitation cinema. It tells the story of teenage anti-hero Tony de Guzman and the rough neighborhood he calls home.   

==Plot==

Tony de Guzman is an anti-hero. Life, according to him, is short, brutal and is never on your side. Grab what you can, when you can. Settle scores. Be randy. Defy the rules. Cheat the system. Tough it out.

Tony knows nothing but tough times living, as he does, in the bleak circus of the slums he calls home amongst denizens of the underworld: the crippled pimp, the lonely housewife, the neighborhood gay and his macho father, the prostitutes, the small-time politician, and the Yankee pedophile. This is his story and the story of the world he lives in: a hopeless, closed-in decrepit world gone to seed.

Mondomanila is an unflinching and unflinchingly funny look at life in the underbelly of the urban diaspora—-with songs.

==Release==

===Festival circuit===
{| class="wikitable" style="text-align: center;"
|-
! Year
! Festival
! Award
! Notes
|-
| 2010 
| Cinemanila International Film Festival
|  
| Closing film Film screened was a rough cut, a work in progress
|-
| 2012
| International Film Festival Rotterdam
| 
| World premiere
|-
| 2012
| Jeonju International Film Festival
| 
| 
|-
| 2012
| Anthology Film Archives
| 
| 
|-
| 2012
| Yerba Buena Center for the Arts
| 
|
|-
| 2012
| Edinburgh International Film Festival
| 
| premiered in EIFF with another Khavn film, Philippine New Wave: This Is Not A Film Movement   
|-
| 2012
| Festival Paris Cinéma
| 
| 
|-
| 2012
| Sarajevo Film Festival
| 
| 
|-
| 2012
| Fantasia Festival
|   "For the accuracy with which the director looks at his country’s most disadvantaged, uncompromising but full of humanity"
| Opening Film - Camera Lucida Section
|}

==Background==

Khavn, known for his staunch advocacy of guerilla shooting methods, has a reputation for shooting very quickly on a small budget. However, unlike his other features, Mondomanila took Khavn nine years and numerous attempts, to make.   

Mondomanila entered production in 2003 but was stopped after the first day because of major miscasting issues. Most of the scenes were scrapped; what Khavn managed to salvage became parts of another film, Mondomanila: Overdosed Nightmare, which premiered at the Cinemalaya International Film Festival in 2008.  Writes critic Richard Bolisay:
 "This film has lived up to its name—cranky, blasphemous, berserk, carnivorous—for the nightmare is not only overdosed, it is sacrilegious, the darkness is so imperial it looks like a work of evil, a devil of a craftsman Khavn is—and there is no point in deconstructing its stark elements because it only extinguishes the fire. The surrealist at work subscribes to absolute nihilism. The motive is anarchy. The enemy is false religion."    
 Palanca in 2000. The short film, which starred Filipino actor Marvin Agustin, premiered at the International Film Festival Rotterdam in 2005,    and was singled out by Brazilian critic Jorge Didaco, then writing for Senses of Cinema, as one of the ten best films of 2005.   

In between 2004 and 2009, Khavn worked on varying versions of the screenplay; one was a ten-hour epic which, though more faithful to the novel, had to be abandoned for practical reasons: cost would have been enormous and Khavn could not find project backers. 
 German film company, Rapid Eye Movies, co-managed by Stephan Holl and Antoinette Köster.

In 2010, Khavn screened a work in progress version of Mondomanila at the MFMF! Mondomanila FilmFest Motherf%*#ers     and the Cinemanila International Film Festival. The rough cut won for Khavn Best Director.

House of Film, a Beverly Hills-based sales company, for worldwide distribution is the world sales agent of Mondomanila.   

==Reception==

{{multiple image
 | align     = right
 | direction = horizontal
 | header    =
 | header_align = left/right/center
 | header_background =
 | footer    =
 | footer_align = left/right/center
 | footer_background =
 | width     =
 | image1    =
 | width1    = 125
 | caption1  = Muse, duck egg vendor & Tony Ds bestfriend
 | image4    =
 | width4    = 150
 | caption4  = Dugyot, the duck lover
}}
{{multiple image
 | align     = right
 | direction = horizontal
 | header    =
 | header_align = left/right/center
 | header_background =
 | footer    =
 | footer_align = left/right/center
 | footer_background =
 | width     =
 | image2    =
 | width2    = 120
 | caption2  = Ogo X, the one-armed rapper
 | image3    =
 | width3    = 157
 | caption3  = Ungay, the coal vendor
}}
In an interview with Festival Scope during the films premiere in Rotterdam, Khavn was asked why he did not choose to do a literal adaptation of the novel. He explained:

  }}
 John Waters, for its unabashed obnoxiousness, and Alejandro Jodorowsky, for its unabashed strangeness. However, the film is a strange creature altogether, more venomous than any of Waters’ trash and more useful than any of Jodorowsky’s excesses."   

Indiocines Macky Macarayan found some of the characters confusing, and believes a traditional ending featuring the narrator would have served the film better. But he praises the film for being a very honest social commentary, calling it "raw, uninhibited, and unforgiving". "Everything is exaggerated, meant as an inverse, or metaphorically presented. Subtlety is not in Khavns vocabulary; he likes to confront the issues head on, but at least he has some style. Like Nolans Inception you have to dig further."   

One viewer notes that in the film, Khavn combines the abrasive aspects of his two other films, Squatterpunk and The Family That Eats Soil, to "transvaluate Julia Kristevas notions of the abject and turns it from a source of horror to something darkly humorous".   

Film blogger Sanriel Ajero finds Mondomanila "quite offensive, as one would expect from the sick mind of Khavn, and by this virtue alone, should warrant a divisive reaction from its audience" but praises its vision. "Personally, I’m with the half that enjoyed the hell of it. Surely one of the few brave local directors we have now, that continually pushes the boundaries of violence and gore, even of lyricism and poetry.”   

Lagaristas Dodo Dayao calls Mondomanila "Glee on crack", likening it to Dusan Makavejev’s Sweet Movie but calls it "quintessential Khavn. Only with a grander sense of culmination."

  }}

==References==
 

== External links ==
*  
*  
*   — An Interview with Dodo Dayao for  
*   — An Interview with Ping Medina for the  
*   — An Interview with Yusef Sayed for  

 
 
 
 