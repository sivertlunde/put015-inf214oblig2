Hariharan Pillai Happy Aanu
{{Infobox film
| name           = Hariharan Pillai Happy Aanu
| image          = Hariharan Pillai Happy Aanu.gif
| image_size     = 
| alt            = 
| caption        = 
| director       = Viswanathan Vaduthala
| producer       = Johny Sagarik  
| writer         = Sunil Impress   P.S. Kumar
| narrator       = 
| starring       = Mohanlal  Jyothirmayi  Cochin Haneefa  Jagathy Sreekumar
| music          = Stephen Devassy
                 Rajeev Alunkal(lyrics)
| cinematography = Anandakuttan
| editing        =   Bhoominathan 
| studio         = 
| distributor    = 
| released       = 26 November 2003 
| runtime        = 
| country        = India
| language       = Malayalam
| preceded_by    = 
| followed_by    = 
}}
Hariharan Pillai Happy Aanu (  by Viswanath starring Mohanlal and Jyothirmayi. This was Viswanathans debut film as a director and the debut music directorial venture of renown pianist and Arrangement|arranger, Stephen Devassy.

== Synopsis ==
Hariharan Pillai (Mohanlal) is running his construction company with uncle Velappan (Cochin Haneefa). He constructs a house for the rich money lender Satyapalan (C.I. Paul), who does not compensate them after completing the house. Satyapalans daughter Kavya (Jyothirmayi) initially dislikes Hariharan, however she eventually falls for him. Pillai also has to take care of sister Latha, who is in love with Nikhil. How Hariharan Pillai sorts out all the issues, and gets Kavya to form the rest of the story.

*Mohanlal ...  Hariharan Pillai
*Jyothirmayi ...  Kavya
*Cochin Haneefa ...  Velappan
*Jagathy Sreekumar ...  Vasu
*C.I. Paul ...  Satyapalan Vijayaraghavan ...  Dilip Kumar
*Kaviyoor Ponnamma ...  Padmavathiyamma
*Salim Kumar ...  Sundaran Siddique
*Jagadish ...  S. I. Vinodkumar
*Ponnamma Babu...  Ramani
*T. P. Madhavan ...  Rosario
*Ambili Devi ...  Latha
*Nikhil ...  Sethu
*Salu Kuttanadu ...  Abu
*Machan Varghese
*Priyanka ...  Mythili
*Gayathri
*Deepika Mohan ... Rozarios wife

== Soundtrack ==
The films soundtrack contains 8 songs, all composed by Stephen Devassy and Lyrics by Rajiv Alunkal.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Ambaadippoove"
| Biju Narayanan
|-
| 2
| "Ambaadippoove  " Jyotsna
|-
| 3
| "Maayaamayoori"
| M. G. Sreekumar
|-
| 4
| "Mundiri Vaave"
| K. J. Yesudas, Biju Narayanan, K. S. Chitra
|-
| 5
| "Mundiri Vaave"
| K. J. Yesudas 
|-
| 6
| "Pularikal"
| Kalabhavan Jimmi
|-
| 7
| "Thallu Thallu"
| M. G. Sreekumar, Chorus
|-
| 8
| "Thinkalnilaavil"
| P. Jayachandran, Sujatha Mohan
|}

==Release==

The movie had a wide release by Jhony Sagariga of over seventy screens across Kerala, the biggest ever for a Malayalam movie at that time.

==Box office==

In spite of a star cast and wide release, the movie failed to impress the audience and was a major flop

==External links==
*  
* http://popcorn.oneindia.in/title/5036/hariharan-pillai-happy-aanu.html

 
 
 
 


 
 