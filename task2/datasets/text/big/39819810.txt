Mountains on Fire
{{Infobox film
| name           =  Mountains on Fire
| image          = 
| image_size     = 
| caption        = 
| director       = Karl Hartl   Luis Trenker
| producer       = Charles Delac   Marcel Vandal
| writer         = Luis Trenker (novel)   Karl Hartl
| narrator       =  Claus Clausen
| music          = Giuseppe Becce
| editing        = Karl Hartl   Marc Sorkin   Ernst Fellner
| cinematography = Sepp Allgeier   Albert Benitz   Giovanni Vitrotti
| studio         = Les Films Marcel Vandal et Charles Delac 
| distributor    = Globus-Film
| released       = 28 September 1931
| runtime        = 109 minutes
| country        = Germany
| language       = German 
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German war on location in the Dolomites.

==Synopsis==
Shortly before the First World War, an Italian and an Austrian take part in a mountaineering expedition together. Not long afterwards they find themselves fighting on different sides. 

==Cast==
*  Luis Trenker as Florian Dimai 
* Lissy Arna as Pia, his wife
* Luigi Serventi as Arthur Franchini, his friend Claus Clausen as Leutnant (lieutenant) Kall 
* Erika Dannhoff   
* Paul Graetz  
* Michael von Newlinsky   
* Emmerich Albert as Tyrolean mountain guide
* Luis Gerold as Tyrolean mountain guide
* Hans Jamnig as Tyrolean mountain guide 
* Hugo Lehner as Tyrolean mountain guide
* Roland von Rossi as Tyrolean mountain guide

==References==
 

==Bibliography==
* Trenker, Luis (1931) Berge in Flammen. Ein Roman aus den Schicksalstagen Südtirols. Berlin: Neufeld & Henius
* Trenker, Luis and Degon, Pierre A. (1934) La guerre au Tyrol : combats dans les Dolomites (1915-1918) Paris: Payot
* Naumann, Heer. War of Extermination: The German Military in World War II, 1941-1944. Berghahn Books, 2004.
* Keller, Tait (April 2009)   Environmental History Vol. 14, No. 2, pp. 253-274

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 

 
 