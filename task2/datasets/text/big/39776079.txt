The Caviar Princess
{{Infobox film
| name           = The Caviar Princess
| image          = 
| image_size     = 
| caption        = 
| director       = Carl Lamac
| producer       = Victor Skutezky
| writer         = Hans H. Zerlett   Walter Wassermann   Charlie Roellinghoff
| narrator       = 
| starring       = Anny Ondra   Maria Forescu   André Roanne   Albert Paulig
| music          = Pasquale Perris
| editing        = 
| cinematography =  Otto Heller
| studio         = Hom-AG für Filmfabrikation   Sofar-Film 
| distributor    = 
| released       = 17 January 1930
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent comedy film directed by Carl Lamac and starring Anny Ondra, Maria Forescu and André Roanne.  The films art direction was by Heinrich Richter. It is also known by the alternative title The Virgin of Paris.

==Cast==
* Anny Ondra as Annemarie 
* Maria Forescu as Ihre Tante 
* André Roanne as Attaché 
* Albert Paulig as Minister 
* Ida Wüst as seine Frau 
* Josef Rovenský as Jazzsänger 
* Hans Mierendorff as Finanzmagnet 
* Paul Rehkopf as Schutzmann 
* Sig Arno as Dorfbursche 
* Carl Walther Meyer as Journalist

==References==
 

==Bibliography==
*Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910–1933. Berghahn Books, 2005.

==External links==
* 

 
 
 
 
 
 
 
 


 