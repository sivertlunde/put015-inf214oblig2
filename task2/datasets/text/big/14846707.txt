The Last Chase
{{Infobox Film name        = The Last Chase image       = The Last chase poster.jpg| caption     = Theatrical poster writer      = C.R. OChristopher starring    = Lee Majors Burgess Meredith Chris Makepeace Alexandra Stewart director    = Martyn Burke distributor = Crown International Pictures producer  William Duffy music       = Gil Melle released    = April, 1981 country     = USA language    = English
}}
The Last Chase is a 1981 dystopian science fiction film starring Lee Majors, Burgess Meredith and Chris Makepeace, and directed by Martyn Burke. It was produced by Argosy Films.

==Plot==
At an unspecified future time, the United States is a police state.  A substantial percentage of the population was wiped out by a devastating viral pandemic twenty years previously.  Amidst the resulting chaos and general panic, democracy collapsed and a totalitarian cabal seized power.  After moving the seat of government to Boston, the new dictatorship outlawed ownership and use of all automobiles, boats, and aircraft, on the pretext (later proven false) that an even bigger crisis, the exhaustion of fossil fuel supplies, was imminent.  The loss of other personal freedoms followed, and surveillance cameras now monitor private citizens every move. 

In Boston, Franklyn Hart (Majors), a former race car driver who lost his family to the deadly plague, is a spokesman for the mass transit system.  He travels the city making propaganda speeches, publicly renouncing his former profession, deploring the selfishness of private vehicle ownership, and exalting the virtues of public transportation.  In private, he is barely able to contain his contempt for the oppressive, autocratic bureaucracy and the dismal party line that he is compelled to promote.  He remains despondent over the loss of his family, and longs for the basic rights and privileges that he and all other Americans once took for granted.

Years before, as private vehicles were being confiscated, Hart sequestered his race car—an orange Porsche roadster—in a secret compartment beneath his basement.  Over the ensuing years he has gradually restored it to drivable condition, raiding long-abandoned junkyards for parts in the dead of night.  His goal is to drive across the country to "Free California", an independent territory that has broken away from the rest of totalitarian America.  Young electronics whiz Ring McCarthy (Makepeace) discovers Harts plan, and Hart reluctantly agrees to bring him along on his perilous journey.  

The ubiquitous surveillance system catches Hart vaulting a junkyard fence; Hart and McCarthy flee Boston in the roadster as police close in.  Although gasoline has not been sold for twenty years, Hart has access to a virtually inexhaustible supply, the few inches of residual fuel remaining at the bottom of subterranean storage tanks in every abandoned gas station in the country.  He uses a portable hand pump to refuel from these tanks as necessary.

News of the duos daring adventure spreads across the country.  The government—represented by a Gestapo-like figure named Hawkins (George Touliatos)—watches with growing concern as the public takes notice and cheers Harts defiance of authority.  Calls for a return to personal autonomy and democracy are heard, for the first time in two decades.  Hart must be stopped; but ground pursuit is impossible, because there are no functional road vehicles capable of chasing down a race car.  (Police cars are basically electric golf carts.)

Hawkins orders J.G. Williams (Meredith), a retired Air Force pilot, to track down and destroy Hart and his car in a Korean War-vintage F-86 Sabre jet.  At first, Williams is caught up in the excitement of the chase and the thrill of flying a fighter jet once again, and gives little thought to the consequences of his mission.  He locates his target and strafes the car, wounding Hart.   A community of armed rebels takes Hart and McCarthy in, hides the car, and treats Harts wounds; but a team of mercenaries soon locates and attacks the enclave.  Hart and McCarthy escape during the firefight.

Back on the open road, Williams once again has the roadster in his crosshairs; but now he is having second thoughts.  As an old rebel himself, he is starting to identify with Harts situation.   Prodded by Hawkins, Williams initiates several more confrontations, but each time he backs off, to Harts and McCarthys bewilderment.   McCarthy rigs a radio receiver and listens in on Williamss cockpit radio communications, then establishes a dialog with him using Morse code via a hand-held spotlight.  Eventually Williams confides that he is sympathetic to their cause.

But Hawkins is also monitoring Williamss radio communications, and after learning of his change of heart, orders the activation of a Cold War-era laser cannon at a position ahead of Harts route.  Williams attempts to warn Hart, but his radio communications have been jammed.  Williams releases his external fuel tanks ahead of the car, hoping the inferno will stop the car short of the cannons range; but Hart, assuming Williams has changed his allegiances yet again, drives on.  Williams strafes the laser, but cannot pierce its heavy armor; so he sacrifices himself in a kamikaze-style attack, destroying his jet and the laser installation, and allowing Hart and McCarthy to drive on toward California.

Hawkins watches as Free California broadcasts the arrival of Hart and McCarthy for everyone to see, and vocalizes his concern that the consequences of this public show of defiance "could set us back to the 1980s!"

==Home video==
The movie was originally released on VHS and Laserdisc by Vestron Video (now a division of Lions Gate Entertainment).  It was re-released in DVD format in May, 2011 via Code Red.

==In other media==
In May 1989, The Last Chase was the subject of episode K20 of the cult KTMA show Mystery Science Theater 3000.

==External links==
* 
=== Mystery Science Theater 3000 ===
*  
*  

 

 
 
 
 
 
 