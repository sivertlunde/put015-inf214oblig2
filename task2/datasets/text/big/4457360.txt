Let's Get Lost (1988 film)
{{Infobox Film
| name           = Lets Get Lost
| image          = Lets_get_lost_poster.jpg
| image_size     = 
| caption        = Theatrical poster Bruce Weber
| producer       = Bruce Weber
| writer         = Bruce Weber
| narrator       =  William Claxton Russ Freeman Carol Baker Vera Baker Diane Vavra Ruth Young
| music          = Chet Baker
| cinematography = Jeff Preiss
| editing        = Angelo Corrao
| distributor    = Little Bear
| released       = September 15, 1988 (Toronto International Film Festival)
| runtime        = 120 minutes
| country        = United States English
| budget         = United States dollar|$1,000,000 (estimated)
| gross          = 
| preceded_by    = 
| followed_by    = 
}} American documentary Bruce Weber. Happy Go Lucky which Baker recorded for Pacific Records. 

==Plot== Russ Freeman, to the 1980s, when his heroin addiction and domestic indifference kept him in Europe. By juxtaposing these two decades, Weber presents a sharp contrast between the younger, handsome Baker — the statuesque idol who resembled a mix of James Dean and Jack Kerouac — to what he became, “a seamy looking drugstore cowboy-cum-derelict,” as J. Hoberman put it in his Village Voice review.   
 William Claxton in 1953 to appearances on The Steve Allen Show and kitschy, low budget Italian films Baker did for quick money.

==Development==
Bruce Weber first became interested in Chet Baker when he spotted a photograph of the musician in a Pittsburgh record store on the cover of the 1955 vinyl LP Chet Baker Sings and Plays with Bud Shank, Russ Freeman and Strings when he was 16 years old. Weber first met Baker in the winter of 1986 at a club in New York City    and convinced him to do a photo shoot and what was originally only going to be a three-minute film.    Weber had wanted to make a short film from an Oscar Levant song called "Blame It on My Youth". They had such a good time together that Baker started opening up to Weber. Afterwards, Weber convinced Baker to make a longer film and the musician agreed.    Filming began in January 1987. Interviewing Baker was a challenge as Weber remembers, "Sometimes wed have to stop for some reason or another and then, because Chet was a junkie and couldnt do things twice, wed have to start all over again. But we grew to really like him".   
 Time Out. "But then Chet gets here, and hes had a fight with his girlfriend, and he wants to record a song… So what happens is that your world becomes like a jazz suite. You have to go along with him."
 Bruce Weber bought when he was 16 years old at a Pittsburgh record store. 

==Reception==
Lets Get Lost had its world premiere at the Toronto International Film Festival.

The documentary was well received by critics and currently has a 93% rating on Rotten Tomatoes. Entertainment Weekly gave the film an "A-" rating and said that Weber "created just about the only documentary that works like a novel, inviting you to read between the lines of Bakers personality until you touch the secret sadness at the heart of his beauty". {{cite news
 | last = Gleiberman
 | first = Owen
 | coauthors =
 | title = Lets Get Lost
 | work = Entertainment Weekly
 | pages =
 | language =
 | publisher = 
 | date = June 13, 2007
 | url = http://www.ew.com/ew/article/0,,20042381,00.html
 | accessdate = 2008-02-20 }}  In her review for the Los Angeles Times, Carina Chocano wrote, "If theres a driving force to Webers film, it seems to be delving into the nature and purpose of star quality and personal magnetism, which Baker had in droves but which didnt save him". {{cite news
 | last = Chocano
 | first = Carina
 | coauthors =
 | title = Lost traces jazz legends shocking descent
 | work = Los Angeles Times
 | pages =
 | language =
 | publisher = 
 | date = January 11, 2008
 | url = http://www.calendarlive.com/movies/reviews/cl-et-lost11jan11,0,1913213.story archiveurl = archivedate = 2008-01-20}}  In his review for the Washington Post, Hal Hinson wrote that what Weber "provides us is rapturous, deeply involving, and more than a little puzzling". {{cite news
 | last = Hinson
 | first = Hal
 | coauthors =
 | title = Lets Get Lost
 | work = Washington Post
 | pages =
 | language =
 | publisher = 
 | date = June 2, 1989
 | url = http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/letsgetlostnrhinson_a0a941.htm
 | accessdate = 2008-02-20 }}  Terrence Rafferty, in his review for The New York Times, wrote, "The enduring fascination of Let’s Get Lost, the reason it remains powerful even now, when every value it represents is gone, is that it’s among the few movies that deal with the mysterious, complicated emotional transactions involved in the creation of pop culture — and with the ambiguous process by which performers generate desire". {{cite news
 | last = Rafferty
 | first = Terrence
 | coauthors =
 | title = A Jazzman So Cool You Want Him Frozen at His Peak
 | work = The New York Times
 | pages =
 | language =
 | publisher = 
 | date = June 3, 2007
 | url = http://www.nytimes.com/2007/06/03/movies/03raff.html?_r=1&oref=slogin
 | accessdate = 2008-11-17 }} 

A newly restored print was screened at the 2008 Cannes Film Festival. {{cite news
 | last = 
 | first = 
 | coauthors =
 | title = Cannes Classics Set For Fifth Year
 | work = indieWIRE
 | pages =
 | language =
 | publisher = 
 | date = May 7, 2008
 | url = http://www.indiewire.com/buzz/cannes.html archiveurl = archivedate = 2008-03-13}} 

===Awards===
Lets Get Lost was nominated for an Academy Award for Best Documentary Feature in 1988.   

==Home media==
Lets Get Lost was released on VHS and Laserdisc in Japan by Nippon Columbia on November 21, 1993.

Lets Get Lost was originally going to be released on DVD in 2007 along with an expanded version of the films soundtrack.  According to Weber, the DVD was to be released in December 2007 but failed to do so.  The DVD was released in the United Kingdom on July 28, 2008. The DVD was finally released in the United States by Docurama on December 3, 2013. There is a Blu-ray that was released in Italy for Region B.

It is also available for streaming and downloading.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 