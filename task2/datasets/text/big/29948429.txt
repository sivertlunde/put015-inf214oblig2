Mere Sajana Saath Nibhana
{{Infobox film
 | name = Mere Sajana Saath Nibhana
 | image = MereSajanaSaathNibhana.jpg
 | caption = DVD Cover
 | director = Rajesh Vakil
 | producer = K C Bokadia
 | writer = 
 | dialogue =  Shantipriya Prem Chopra
 | music = Anand-Milind
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = February 07, 1992
 | runtime = 135 min.
 | language = Hindi Rs 2.5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1992 Hindi Indian feature directed by Shantipriya and Prem Chopra. The film is the remake of Tamil hit Kizhakku Vasal.

==Plot==

Mere Sajana Saath Nibhana is the story of a simpleton (Mithun) and his love on Juhi Chawla. The third angle is provided by Shantipriya. Prem Chopra plays the Villain, who has an eye on Juhi Chawla.

==Cast==

*Mithun Chakraborty
*Juhi Chawla
*Shanti Priya
*Prem Chopra

==Soundtracks==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| Dam Dam Dafli Bajaoon
| Udit Narayan
|- 
| 2
| Jhoom Jhom Kahta Hai 
| Udit Narayan, Kavita Krishnamurthy
|-
| 3
| Banno Re Aise Kyun 
| Udit Narayan, Anand Kumar
|- 
| 4
| Mandir Toote To 
| Udit Narayan
|-
| 5
| Nakhre Dikha Ke Dil Ko Chur
| Udit Narayan, Sadhana Sargam
|-  
| 6
| Kangana Kunware Kangana
| Udit Narayan, Sadhana Sargam
|}

==References==
 
* http://www.imdb.com/title/tt0104854/
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Mere+Sajna+Saath+Nibhana

==External links==
*  

 
 
 
 
 
 