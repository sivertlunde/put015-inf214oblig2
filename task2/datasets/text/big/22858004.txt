Neelathamara (2009 film)
{{Infobox film
| name           = Neelathamara
| image          = Neelathaamara.jpg
| alt            =  
| caption        = 
| director       = Lal Jose Menaka  Revathy Kalamandir
| writer         = M.T. Vasudevan Nair Archana Kavi Kailash Suresh Nair Rima Kallingal Samvrutha Sunil Jaya Menon Vidyasagar
| cinematography = Vijay Ulaganath
| editing        = Ranjan Abraham
| studio         = Revathy Kalaamandhir
| distributor    = India : Playhouse Europe: PJ Entertainments
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =   1.5 crores 
| gross          = 
}}
Neelathamara ( ) (English: The Blue Lotus) is a 2009  . Retrieved 30 November 2009  The film was produced by Suresh Kumar under the banner of Revathy Kalamandir. It stars  . Retrieved 4 December 2009 

==Plot==
Beena (Amala Paul), a program producer with NDTV 24x7, is in her ancestral village for a television program. She wants her fiancé to meet her grandmother, who has just been discharged from hospital. Beena, the daughter of K.P. Haridas, who died a few years back is not in good terms with her mother Ratnam over her second marriage. Ratnam happens to visit the house at the same time. There she meets Kuttimalu, a middle-age lady, who was once a housemaid in her teenage days. Kuttimalu is welcomed by grandmother with the same warmth that she enjoyed long back, and she says that her daughters are now well settled and are leading a happy lives. Ratnam is also affectionate towards Kuttimalu, who had once had an affair with Haridas.
 Archana Kavi)  had arrived as a maid with Appukuttan, her cousin and her grandmother. Kuttimalu succeeds in winning the heart of Haridass mother in no time. She was an innocent village girl who always found fun in sharing secrets with Ammini (Rima Kallingal), a girl of her age.

Ammini informs her about the myth of Neelathamara (blue lotus). According to believers, if they offer a one rupee note at the temple pond and prays deeply to the god, the flower will blossom the next morning and his/her wish will turn into truth. The arrival of Haridas (Kailash (Actor)|Kailash), a final-year law student, adds more color to her life. He succeeds in alluring Kuttimalu in a short span, which she believes is real. She fails to understand that it was just fun that Haridas is looking for. Her prayer at the river pond results in blossoming of Neelathamara, which takes her to cloud nine. But news of the engagement of Haridas to Ratnam comes in as a shock, which breaks her down mentally. She slowly realizes that Haridas was never serious in his affair with her and tries to overcome the grief by silently serving Ratnam, his new wife. Ratnam one day comes to know about the affair and, when asked, Haridas replies casually, which makes her go berserk. Ratnam orders Kuttimalu to leave the house she accepts silently. She is taken home by Appukuttan, whom she married.

Now years have passed and Haridas is no more and both ladies have matured. The film ends with Kuttimalu once again with full heart preparing to take care of the octogenarian mother of Haridas.

==Production==
*Neelathamara, though was announced as a remake of the old film with the same name, had several changes. While the old film was a love story between Kunjimalu and Haridas, the remake focused more on the contemporary lives of both Ratnam and Kunjimalu. M. T. Vasudevan Nair himself re-worked on the script. 
*This film had mostly newcomers, which was more like an experiment by Lal Jose.
*The camera work by Vijay Ulaganath was well appreciated. Archana Kavi also received positive remarks from both critics and masses. 
*This was actress Amala Pauls first film. Her next release was her debut as Anaka in Tamil; the critically acclaimed and box office hit Tamil film Mynaa.
*The main villain role was played by Suresh Nair, the younger brother of Ambika (actress)|Ambika, which was originally portrayed by Sathar. 
* Noted Malayalam Poet and Lyricist Mullanezhi played a small role, as an old man sitting under the banyan tree near the temple.

==Cast== Archana Kavi as Kunjimalu (Old Film - Ambika) Kailash as Haridas (Old Film - Ravi Kumar)
* Samvrutha Sunil as Rathnam (Old Film - Bhavani)
* Rima Kallingal as Shaarathe Ammini (Old Film - Sarojam)
* Sreedevi Unni as Maluamma (Old Film - Santha Devi)
* Suresh Nair as Appukuttan (Old Film - Sathar)
* Joy Mathai as Karyasthan Achuthan Nair (Old Film - Bahadoor)
* Jaya Menon as Shaarathe amma (Old Film - Adoor Bhavani)
* Parvathi T.  as Elder Kunjimalu
* Amala Paul as Beena
* Mullanezhi as the old man sitting under the banyan tree

==Soundtrack==
{{Infobox album |  
| Name       = Neelathamara
| Type       = Soundtrack Vidyasagar
| Cover      = 
| Released   = 2009
| Recorded   = 
| Genres     = World Music
| Length     =
| Label      =  Vidyasagar
| Last album = Kuruvi (2008)
| This album = Neelathaamara (2009)
| Next album = Peranmai (2009)
}} Vidyasagar with lyrics penned by Vayalar Sarath Chandra Varma. Vijay Prakash, famous through recent A. R. Rahman songs, was introduced to Malayalam through this film. The track "Anuraga Vilochananayi", sung by Shreya Ghoshal and V.Shreekumar (Shreekumar Vakkiyil), turned out to be one of the most successful songs of the year. It was the chart topper for many continuous weeks. 
 Filmfare Award for Best Music Director and Mathrubhumi-Amrita Film Award for Best Music Director, for his work in the film.   The track "Anuraga Vilochananayi" won the Most Popular Song of the Year Award at 2009 Vanitha Film Awards. 

{| class="wikitable"
! #
! Title
! Artist(s)
! Length
|-
| 1
| "Anuraga Vilochananayi"
| Shreya Ghoshal, Shreekumar Vakkiyil
| 4:36
|-
| 2
| "Neelathamare" Karthik
| 4:24
|-
| 3
| "Pakalonnu"
| Vijay Prakash, Balaram
| 4:51
|-
| 4
| "Entho Mudo"
| Cherthala Ranganatha Sharma
| 2:57
|-
| 5
| "Needhaya Radha"
| Cherthala Ranganatha Sharma
| 3:11
|}

==References==
 
General
#  
#  
#  
#  
#  
 
Specific
 

==External links==
*  
* 
* 

 

 
 
 
 
 
 
 
 