A Hill in Korea
{{Infobox film
| name           = A Hill in Korea
| image          = Hillkorpos.jpg
| image size     =
| caption        = Original American film poster
| director       = Julian Amyes
| producer       = Anthony Squire
| writer         = Max Catto (book) Anthony Squire Ian Dalrymple George Baker Harry Andrews Stanley Baker
| music          = Malcolm Arnold
| cinematography = Freddie Francis
| editing        = Peter R. Hunt
| distributor    = British Lion Films Distributors Corporation of America (US)
| released       = 1956
| runtime        = 81 min.
| country        = UK
| language       = English
| budget         =
| preceded by    =
| followed by    =
}} British war film based on Max Cattos 1953 novel of the same name. The original name was Hell in Korea, but was changed for distribution reasons, except in the U.S.  It was directed by Julian Amyes and the producer was Anthony Squire. Incidental music was written by Malcolm Arnold.

It was the first major feature film to portray British troops in action during the Korean War and introduces Michael Caine (himself a veteran of the Korean War) in his first credited film role.

==Plot==
During the retreat of 1951, a small force of British soldiers is in danger of being cut off by the advancing Chinese army. The plot emphasizes the plight of the National Service men who, as they say, were "old enough to fight, but too young to vote."

The film also depicts a "friendly-fire" incident, in which the British are bombed by the Americans.

The film opens in Korea with a British Army patrol, led by Lt. Butler. In the patrol is tough veteran Sergeant Payne, a slightly psychotic Corporal Ryker, and the cowardly signaller Wyatt. As they search a small village, one of the party falls victim to a bomb planted in a small shack. With the death of one of his men, Butler moves the patrol out of the village. Out in the open plain, Butler and Payne discover a large force of Chinese soldiers heading directly for them. Sending Payne and the patrol back towards their own lines, Butler and three of his men stay behind to cover the withdrawal. After fending off two attacks, Butler discovers Lance Corporal Hodge is dead. Payne returns with the patrol, informing Butler that they were cut off by other enemy forces.

The patrol heads through the village and up a winding path towards an isolated temple located on a hill, with only a steep cliff to its rear. On the way, Wyatt throws away the only radio because he cannot be bothered to carry it up the hill. Then they run into an enemy patrol on the path. They ambush the Chinese, and continue up to the temple. With the Chinese knowing now exactly where they are, Butler must keep his troops together, and fend off the enemy attacks.

==Cast== George Baker as Lieutenant Butler
* Harry Andrews as Sergeant Payne
* Stanley Baker as Corporal Ryker
* Michael Medwin as Private Docker Ronald Lewis as Private Wyatt
* Stephen Boyd as Private Sims
* Victor Maddern as Private Lindop
* Harry Landis as Private Rabin Robert Brown as Private OBrien
* Barry Lowe as Private Neill Robert Shaw as Lance Corporal Hodge
* Charles Laurence as Private Kim Percy Herbert as Private Moon
* Eric Corrie as Private Matthews  David Morrell as Private Henson
* Michael Caine as Private Lockyear

==Reception==
A Hill in Korea currently scores a three star rating (6/2/10) on IMDb.

==References==
"Time Out Film Guide," published by Penguin - ISBN 0-14-029395-7

==External links==
* 
* 

 
 
 
 
 
 
 
 