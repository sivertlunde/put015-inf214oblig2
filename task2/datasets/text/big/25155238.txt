So F**king Rock Live
 
 
{{Infobox Film |
  | name = So F**king Rock Live
  | image = So F**king Rock DVD Cover.jpg
  | caption = Original DVD Cover
  | director = Matt Askern	 Helen Parker  Celia Moore
  | writer = Tim Minchin
  | starring = Tim Minchin
  | distributor = Universal Studios
  | runtime = 126 minutes
  | released =  
  | language = English
  }}
So F**king Rock Live (also known as So Fucking Rock Live)  is a DVD released by Australian musician and stand-up comedian Tim Minchin. It is a recording of his live performance at Bloomsbury Theatre|Londons Bloomsbury Theatre in May 2008 and contains songs and material from his previous Darkside (Tim Minchin album)|Darkside and So Rock albums. 

==Synopsis==

The show contains Minchins trademark blend of comedic songs and poetry linked by short segments of observational comedy. It features many of the songs that have helped contribute to Minchins popularity, including Peace Anthem For Palestine, which he himself has stated is his favourite song to perform.    In the mammoth two hour performance, he tackles a series of issues including the theory of natural selection, babies and his worries about sex.

==Songs==

===Act 1===
#"So Fucking Rock"
#"Inflatable You"
#"Rock N Roll Nerd"
#"Mitsubishi Colt"
#"Ten Foot Cock And A Few Hundred Virgins"
#"If You Open Your Mind Too Much Your Brain Will Fall Out (Take My Wife)"
#"F Sharp"
#"Canvas Bags"

===Act 2===

#"Nothing Can Stop Us Now"
#"Angry (Feet)"
#"Some People Have It Worse Than I"
#"If You Really Loved Me"
#"Peace Anthem For Palestine"
#"You Grew On Me"
#"Dark Side"

===Encore===

#"Not Perfect"
#"Second Encore"

==Re-Release==
In October 2009, the DVD was re-released, exclusive to HMV stores as a double disc special edition. It consisted of the original show, a second disc full of interviews and TV appearances and also a lyric book for all of the songs that appear in the show.   

==References==
 

==External links==
*  
*  

 

 
 