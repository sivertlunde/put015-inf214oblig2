Love Can Seriously Damage Your Health
{{Infobox film
| name           = El amor perjudica seriamente la salud
| image          = Love Can Seriously Damage Your Health Poster.jpg
| image size     =
| caption        =
| director       = Manuel Gómez Pereira
| producer       = César Benítez Manuel Gómez Pereira Joaquín Oristrell
| writer         = Yolanda García Serrano Manuel Gómez Pereira Juan Luis Iborra Joaquín Oristrell
| starring       = Penélope Cruz Javier Bardem Ana Belén Juanjo Puigcorbé Gabino Diego
| music          = Bernardo Bonezzi	
| cinematography = Juan Amorós
| editor         = Guillermo Represa
| released       =   January 10, 1997
| runtime        =  117 minutes
| country        = Spain
| language       = Spanish
| budget         =
|}}
 Spanish  comedy film directed by Manuel Gómez Pereira and starring Penélope Cruz, Ana Belén, Javier Bardem, Juanjo Puigcorbé and Gabino Diego.

==Synopsis ==
Paris. King Juan Carlos I offers a diner at Hôtel Crillon. Diana (Ana Belén) meets Santi (Juanjo Puigcorbé), an old lover, who now is the Kings bodyguard. They remember how they met each other, 30 years ago, in another hotel, in Madrid.  Diana (Penélope Cruz) and Santi (Gabino Diego) met when The Beatles were going to play. He works in the hotel. She hides under John Lennons bed because shes in love with the singer. But, what starts is an attraction between them. It is an impossible love because she wants to be rich and famous, and he is poor and has to join the Army. When they meet again as grown-ups, Diana has become a socialite, and Santi is a pilot. Both are married, but the love goes on...

==Cameo appearances==
The film uses cameo appearances by real figures from vintage footage and other fields to great comic effect.   They include  , Aitana Sánchez-Gijón or film director Fernando Colomo have got small roles.

==External links==
*  

 
 
 
 
 
 


 
 