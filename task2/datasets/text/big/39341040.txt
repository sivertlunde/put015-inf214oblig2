From the Tropics to the Snow
{{Infobox film
| name           = From the Tropics to the Snow
| image          = 
| caption        =  Jack Lee Richard Mason
| producer       = Jack S. Allan executive producer: Stanley Hawes
| writer         = John Morris Cedric Flower Pat Flower
| starring       = 
| cinematography = 
| music          = 
| distributor    = 
| released       = 1964
| runtime        = 28 mins
| country        = Australia English
}}
From the Tropics to the Snow is a 1964 Australian documentary film. It was one of the best known Australian films of the 1960s.  It was produced under the auspices of the Commonwealth Film Unit (CFU), later reincorporated as Film Australia. It was co-directed by Jack Lee and Richard (Dick) Mason, and featured a young Reg Livermore (in his first film role) as one of the narrators.

One of the regular duties of the CFU at this time was the production of short films that were purpose-made for overseas distribution and were intended to promote Australia as an attractive destination for migrants and tourists. From the Tropics to the Snow marked a significant break with the traditional style of such features, and is noted for its subversive and satirical approach to its subject.

Rather than using the stilted, authoritative single-voice narration that was typical of such promotion documentaries at the time, Mason and Lee opted for a strikingly reflexive approach, using a  lively (and frequently tongue-in-cheek) multi-voice narration, which is introduced through the dramatic device of a production planning meeting between the films Producer-in-Chief (Alexander Archdale), the director (Alastair Smart) and the screenwriter (Livermore). This allowed the film to become an ironic critique of itself, humorously examining the mechanics of "documentary" film construction, and the competing pressures and choices that faced filmmakers when creating such films. It also gently parodied Lee and Masons own situation as "young turks" charged with turning out what were in essence "production-line" propaganda films for a hidebound government department. 

==References==
 

==External links==
*  at IMDB

 

 
 
 
 
 
 
 
 


 
 