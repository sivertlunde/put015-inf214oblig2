Two in a Big City
{{Infobox film
| name = Two in a Big City 
| image =
| image_size =
| caption =
| director = Volker von Collande
| producer =  Robert Wüllner
| writer =  Ursula von Witzendorff   Volker von Collande
| narrator =
| starring = Claude Farell   Karl John   Marianne Simson
| music = Willy Kollo   
| cinematography = Carl Hoffmann   Erich Nitzschmann  
| editing =     Walter von Bonhorst 
| studio = Tobis Film 
| distributor = Tobis Film
| released =23 January 1942 
| runtime = 80 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Two in a Big City (German:Zwei in einer großen Stadt) is a 1942 German romantic comedy film directed by Volker von Collande and starring Claude Farell, Karl John and Marianne Simson. A German soldier on leave in Berlin goes looking for his pen pal who he has never met called Gisela. He meets instead a woman with the same name and falls in love with her. 

==Cast==
*  Claude Farell as Gisela Meinhold   Karl John as Bernd Birckhoff  
* Marianne Simson as Inge Torff  
* Hansi Wendler as Gisela Brückner 
* Volker von Collande as Dr. Eberhard Berg  
* Hannes Keppler as Peter Pelle  
* Käthe Haack as Oberhelferin  
* Paul Henckels as Bumke  
* Margarete Kupfer as Frau Böhme  
* Hubert von Meyerinck as Spießer 
* Wolf Trutz as Ein Fremder 
* Georg Haentzschel as Band Leader  
* Horst Winter as Singer 
* Josef Dahmen as Gesang 
* Gerhard Dammann 
* Cordula Grun 
* Horst Lommer 
* Erik Radolf 
* Erna Sellmer 
* Werner Stock as Gesang 
* Alice Treff 
* Ewald Wenck

== References ==
 

== Bibliography ==
* Hull, David Stewart. Film in the Third Reich: A Study of the German Cinema, 1933-1945. University of California Press, 1969.

== External links ==
*  

 
 
 
 
 
 
 

 