Stampede (film)
{{Infobox film
| name           = Stampede
| caption        =
| image	         = 
| director       = Lesley Selander
| producer       = Scott R. Dunlap John C. Champion Blake Edwards
| writer         = Edward Beverly Mann (novel) Blake Edwards (screenplay) John C. Champion (screenplay)
| starring       = see list below
| music          = Edward J. Kay
| cinematography = Harry Neumann
| editing        = Richard V. Heermance Allied Artists
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Western directed by Lesley Selander.

==Plot== Rod Cameron), and Tim McCall (Don Castle).  The brothers own a large ranch in Arizona and sell some of their surplus grazing land to some settlers.  When the settlers arrive they find the land bone dry because the McCall brothers have dammed the river and control all the water.
 Steve Clark) and his daughter Connie Dawson (Gale Storm) complain to the local sheriff (Johnny Mack Brown) but the sheriff claims there is nothing he can do.  A love interest develops between Tim McCall and Connie Dawson, while simultaneously the settlers try to dynamite the dam and stampede the McCall cattle.

== Cast == Rod Cameron as Mike McCall
* Gale Storm as Connie Dawson
* Johnny Mack Brown as Sheriff Aaron Ball
* Don Castle as Tim McCall
* Donald Curtis as Stanton
* John Miljan as T.J. Furman
* Jonathan Hale as Varik John Eldredge as Cox
* Adrian Wood as Whiskey
* Wes Christiansen as Slim James Harrison as Roper
* Duke York as Maxie Steve Clark as John Dawson
* I. Stanford Jolley as Link Spain
* Marshall Reed as Henchman Shives

==References==
 

== External links ==
* 
 
 
 
 
 
 
 
 
 
 


 