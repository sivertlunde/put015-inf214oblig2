Abala (film)
{{Infobox film 
| name           = Abala
| image          =
| caption        =
| director       = Thoppil Bhasi
| producer       =
| writer         =
| screenplay     = Madhu Jayabharathi KPAC Lalitha Adoor Bhasi
| music          = V. Dakshinamoorthy
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by Thoppil Bhasi. The film stars Madhu (actor)|Madhu, Jayabharathi, KPAC Lalitha and Adoor Bhasi in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast== Madhu 
* Jayabharathi
* KPAC Lalitha
* Adoor Bhasi
* Sankaradi 
*Bahadoor
* Sreelatha Namboothiri 
* T. R. Omana
* M. G. Soman
* Vijayakumar

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi, Ashwathy, SK Nair, Kottayathu Thamburan, Puthukkad Krishnakumar and Thunchathezhuthachan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Annarkkanna || S Janaki || Sreekumaran Thampi || 
|-
| 2 || Ennini Darsanam || Kalyani Menon || Ashwathy || 
|-
| 3 || Mangala || K. J. Yesudas || SK Nair || 
|-
| 4 || Manjil Neeraadum || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 5 || Pathivrithayaakanam || S Janaki || Kottayathu Thamburan, Puthukkad Krishnakumar || 
|-
| 6 || Priyamodu Paarthanu || Kalamandalam Sukumaran, Kalyani Menon || Ashwathy, Kottayathu Thamburan || 
|-
| 7 || Srishtikarthaave || Kalyani Menon || Thunchathezhuthachan || 
|}

==References==
 

==External links==
*  

 
 
 

 