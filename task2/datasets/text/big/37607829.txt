Brooklyn Babylon
{{Infobox film
| name           = Brooklyn Babylon
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Marc Levin
| producer       = Henri M. Kessler
| writer         = Marc Levin, Bonz Malone
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Tariq Trotter, Sara Goberman, David Vadim
| music          = 
| cinematography = Mark Benjamin
| editing        = Emir Lewis
| studio         = Bac Films, Canal+, Studio Canal
| distributor    = Artisan Entertainment, Studio Canal, Crosstown Releasing
| released       =  
| runtime        = 89 minutes
| country        = USA  France
| language       = English
| budget         = 
| gross          = 
}}
 Song of Solomon, set in the backdrop of the Crown Heights riot, starring Black Thought of The Roots.

==Plot summary==
In   and Solomon.   

==Production==
Brooklyn Babylon was the second made of Levins late nineties hip-hop trilogy, which began with Slam (film)|Slam, a searing prison drama starring Saul Williams, Sonja Sohn, and Bonz Malone. The third installment was 2000s Whiteboyz, a black comedy about white farm kids in Iowa who want to be black rappers, starred Danny Hoch, Dash Mihok, Mark Webber, and Piper Perabo.

==Cast== Tariq Trotter as Solomon, Member of The Lions
*Karen Starc as Sara (as Karen Goberman)
*Bonz Malone as Scratch 	
*David Vadim as Judah
*Carol Woods as Cislyn
*Slick Rick as Buddah
*Mad Cobra as Key Bouncer
*Mina Bern as Nanna
*Joanne Baron as Aunt Rose
*Olek Krupa	as Uncle Vlad
 Roots members Ahmir "Questlove" James "Kamal" Leonard "Hub" Kyle "Scratch" Jones played other members of The Lions, and Roots beatbox artist Rahzel served as the films narrator.

==Festivals==
The film was entered in the Slamdance Film Festival, Valenciennes Film Festival, and Agen American Indie Film Festival.
Marc Levin was nominated for the Grand Special Prize at the Deauville Film Festival.

==Reception==
Brooklyn Babylon received mixed reviews by critics, earning an 50% "Fresh" rating on Rotten Tomatoes  and a score of 26 on Metacritic.  

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 