Commissariato di notturna
{{Infobox film 
| name = Commissariato di notturna
| image = Commissariato di notturna.jpg
| caption =
| director =  
| writer =Giacomo Furia Guido Leoni
| starring = Rosanna Schiaffino Gastone Moschin Emma Danieli Leopoldo Trieste Carlo Giuffré Annie Cordy
| music = Renato Rascel
| cinematography = Claudio Racca
| editing =
| producer =
| distributor =
| released = 1974
| runtime =
| awards =
| country =Italy
| language =Italian
| budget =
}} Italian Crime film|crime-comedy film directed by  .     It was one of the few films that tried to mix the classical commedia allitaliana with the poliziottesco genre. 

== Cast ==
*Rosanna Schiaffino: Sonia 
*Gastone Moschin: Commissario Emiliano Borghini 
*George Ardisson: Amedeo Furlan aka il Laureando 
*Antonio Casagrande: Gennarino 
*Emma Danieli: Lucia Bencivenga 
*Giacomo Furia: brigadiere Santini 
*Gisela Hahn: donna tedesca 
*Leopoldo Trieste: Brigadiere Spanò 
*Liana Trouche: Luisa, moglie di Borghini
*Mario Valdemarin: Ferrari 
*Maurice Ronet: Vittorio Cazzaniga 
*Luciano Salce: On. Luigi Colacioppi 
*Carlo Giuffrè: Antonio Carnevale aka Teodoro 
*Annie Cordy: Pupa 
*Aldo Bufi Landi: Appuntato  
*Jean Lefebvre: Dindino 
*Michele Gammino: Brigadiere Frascà 
*Piero Gerlini: Monsignor Guidardini 
*Roger Coggio: Cristoforo 
*Ada Pometti: Anna Maria, Gennarinos daughter
*Nerina Montagnani: Old woman in black

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 