The Parallax View
 
{{Infobox film
| name           = The Parallax View
| image          = Parallax_View_movie_poster.jpg
| image_size     =
| caption        = Theatrical release poster
| screenplay     =  
| based on       = novel by Loren Singer
| starring       = Warren Beatty Hume Cronyn William Daniels Paula Prentiss 
| director       = Alan J. Pakula
| producer       = Alan J. Pakula
| cinematography = Gordon Willis
| editing        = John W. Wheeler
| music          = Michael Small
| distributor    = Paramount Pictures
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English}}

The Parallax View is a 1974 American political thriller film directed and produced by Alan J. Pakula, and starring Warren Beatty, Hume Cronyn, William Daniels and Paula Prentiss. The film was adapted by David Giler, Lorenzo Semple Jr and an uncredited Robert Towne from the 1970 novel by Loren Singer. The story concerns a reporters dangerous investigation into an obscure organization, the Parallax Corporation, whose primary, but not ostensible, enterprise is political assassination.
 All the Presidents Men (1976 in film|1976). In addition to being the only film in the trilogy to not be distributed by Warner Bros. Pictures, The Parallax View is also the only one of the three not to be nominated for (or win) an Academy Award.
 

==Plot== Bill Joyce) atop the Seattle Space Needle. A waiter armed with a revolver is chased but falls to his death. Meanwhile, a second waiter, also armed, leaves the crime scene unnoticed. A Congressional special committee determines that the assassination was the work of a lone gunman.

Three years later, Carter visits her former boyfriend and colleague, newspaper reporter Joe Frady (Warren Beatty). Lee tells Frady that she feels there is more to the assassination than was reported at the time.  Six of the witnesses to Carrolls assassination have since died, so she fears she will be next. Frady does not take her seriously. Carter is soon found dead and her death is judged by the police to be either a voluntary or accidental drug overdose.

Investigating Carters leads, Frady goes to the small town of Salmontail whose sheriff, L.D. Wicker (Kelly Thordsen), attempts to trap him below a dam while the floodgates are opening. Frady narrowly escapes but the sheriff drowns. Frady finds information about the Parallax Corporation in the sheriffs house and learns that its real business is recruiting political assassins.

As Frady is interviewing Carrolls former aide Austin Tucker (William Daniels) aboard Tuckers boat, a bomb explodes. Frady survives but is believed dead. He decides to apply to Parallax under an assumed identity. Jack Younger (Walter McGinn), a Parallax official, assures Frady that he is the kind of man they are interested in. Frady is accepted for training in Los Angeles, where he watches a slide show that conflates positive images with negative actions.

Frady recognizes a Parallax man from a photo Tucker showed him: the Parallax Assassin was a waiter in the Space Needle restaurant the day Senator Carroll was murdered. He watches the Parallax Assassin retrieve a bag from a car, then drive to an airport and check it as baggage on a plane (a Globe Airlines Boeing 707 jetliner). Frady boards the plane. He notices a Senator aboard, but not the Parallax man, who is on the airports roof watching the plane take off. Frady writes a warning to the effect of "There is a bomb on this plane" on a napkin and slips it onto the drink service cart. The warning is found and the plane returns to Los Angeles. Passengers are evacuated moments before a bomb explodes.

Fradys generally skeptical editor Bill Rintels (Hume Cronyn) listens to a secretly recorded tape of a conversation Frady had with Jack Younger. Rintels places it in an envelope, apparently with other such tapes. A disguised Parallax assassin delivers coffee and food to Rintels office. Rintels is poisoned and the tapes disappear. 
 Jim Davis). Frady hides in the auditoriums rafters to secretly observe the Parallax men, who are posing as security personnel. Frady realizes too late he has been set up as a scapegoat. Hammond is shot dead by an unseen gunman. As Frady is trying to escape, he is seen in the rafters and a Parallax agent kills Frady with a shotgun.

The same committee that determined a lone gunman killed Senator Carroll now reports that Frady, acting alone, killed Senator Hammond out of a misguided sense of patriotism and a paranoid belief that the Senator was trying to kill him. The committee further expresses the hope that their verdict will end political assassination conspiracy theories. They do not take questions from the press.

==Cast==
 
*Warren Beatty as Joseph Frady
*Paula Prentiss as Lee Carter
*Hume Cronyn as Bill Rintels
*William Daniels as Austin Tucker
*Walter McGinn as Jack Younger
*Kelly Thorsden as Sheriff L.D. Wicker
*Chuck Waters as Thomas Richard Linder
*Earl Hindman as Deputy Red William Joyce as Senator Charles Carroll
*Bettie Johnson as Mrs Carroll
*Bill McKinney as Parallax Assassin
*Jo Ann Harris as Chrissy – Fradys Girl
*Ted Gehring as Schecter – Hotel Clerk
*Lee Pulford as Shirley – Salmontail Bar Girl
*Doria Cook-Nelson as Gale from Salmontail Jim Davis as Senator George Hammond
*Joan Lemmo as Organist
*Kenneth Mars as Former FBI Agent Will Turner William Jordan as Tuckers Aide Edward Winter as Senator Jameson
*Anthony Zerbe Prof. Nelson Schwartzkopf (uncredited)
*Richard Bull as Parallax Goon
 

==Production==
Most of the images used in the assassin training montage were of anonymous figures or patriotic backgrounds, with occasional historical individuals such as Richard Nixon, Adolf Hitler, Pope John XXIII, and Lee Harvey Oswald (in the picture taken moments after his shooting). The montage also uses a drawing by Jack Kirby of the Marvel Comics character Thor (Marvel Comics)|Thor. The drawing is a cropped image from the cover of Thor Annual #4 (December 1971), which in turn is a reprint of the cover image from Thor #131, "They Strike from Space" (August 1966). The montage also includes a cropped image from the cover of Thor #135 (December 1966) featuring a creature known as the Man-Beast.  

The film was nearly unable to begin production at all because the screenplay was not ready when Warren Beatty had marked in the start of filming, and with a Writers Guild of America strike looming Paramount was leaning towards shuttering the project altogether. However, Beatty did single-handed rewrites with the help of Robert Towne (this would later become controversial because Townes many enemies in Hollywood began spreading rumors that he had crossed the WGA picket line and continued to work with his longtime friend and collaborator on the film) and the movie did get underway and was able to finish on time.

The distinctive anamorphic photography, with long lens, unconventional framing, and shallow focus, was supervised by Gordon Willis.  
 Gorge Dam, Washington State. (48 41 51" N, 121 12 29" W)
 Seattle is seen extensively in the first assassination sequence.

The airport scene was filmed at Bob Hope Airport in Burbank, California.  

==Critical reception== Executive Action (1973), another movie released at about the same time that advanced a conspiracy theory of assassination. Its a better use of similar material, however, because it tries to entertain instead of staying behind to argue."    In his review for The New York Times, Vincent Canby wrote, "Neither Mr. Pakula nor his screenwriters, David Giler and Lorenzo Semple, Jr., display the wit that Alfred Hitchcock might have used to give the tale importance transcending immediate plausibility. The moviemakers have, instead, treated their central idea so soberly that they sabotage credulity."    Time (magazine)|Time magazines Richard Schickel wrote, "We would probably be better off rethinking—or better yet, not thinking about—the whole dismal business, if only to put an end to ugly and dramatically unsatisfying products like The Parallax View."    In 2006, Entertainment Weekly critic Chris Nashawaty wrote, "The Parallax View is a mother of a thriller... and Beatty, always an underrated actor thanks (or no thanks) to his off-screen rep as a Hollywood lothario, gives a hell of a performance in a career thats been full of them."    The motion picture won the Critics Award at the Avoriaz Film Festival (France) and was nominated for the Edgar Allan Poe award for Best Picture. Gordon Willis won the Best Cinematography award from the National Society of Film Critics (USA).

==See also==
*Assassinations in fiction
*List of films featuring surveillance
*The Manchurian Candidate
*Arlington Road
* Permindex

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 