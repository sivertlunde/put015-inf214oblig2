Sole Survivor (2000 film)
 
{{Infobox Film
| name           = Sole Survivor
| image          = Sole Survivor (2000 film).jpg
| caption        =
| director       = Mikael Salomon
| producer       = Dean Koontz Hans Proppe Wendy Williams
| writer         = Dean Koontz (novel)  Richard Christian Matheson (teleplay)
| narrator       = 
| starring       = Billy Zane Gloria Reuben Isabella Hofmann Mitchell Kosterman Rachel Victoria
| music          = Mark Snow
| cinematography = Jon Joffin Christopher Rouse
| studio         = Columbia TriStar Television Lions Gate Television
| distributor    = Fox Network Columbia TriStar Television
| released       = United States: 13 September 2000
| runtime        = 164 min. Canada
| English
}}
 Canadian Sci-Fi Dean Koontzs novel of the same name, made and released in 2000 and directed by Mikael Salomon.

==Plot==
After the death of his wife and daughter in a plane crash, a newspaper reporter named Joe Carpenter discovers that the crash may have been related to a nefarious scientific experiment involving children. A woman, Rose Tucker, who claims she was a survivor of the crash, approaches at his wifes grave. This leads into a plot by the Quartermass organization to capture her and a young girl she is protecting - the girl has the powers to heal and to transport. A villainous killer named Victor Yates and a young boy who can control minds from a distance lead the attack.

Tucker and Carpenter realize that Yates was once a CIA agent turned mercenary for the United States government, who was then hired to kill the scientists that were working on the projects at The Quartermass  Organization, genectically engineering children to become weapons for the next decade. Carpenter also realizes that his wife and daughter were the victims of an experiment gone wrong. Donner, an agent for the Bureau, comes to their aide, not knowing hes a Quartermass agent. After killing Donner, they seek refuge at Carpenters friends house, The Ealings.

After hearing their story, The Ealings help Carpenter sneak inside the Organization and save the children being held hostage. The Ealings  also help Tucker with her injuries by taking her to the hospital. Yates is ambushed in the bathroom of the building by Carpenter. Yates tells Carpenter that he didnt mean for anyone to get hurt during the plane crash and that he is sorry for what has happened. Carpenter will forgive him if he helps save the children. Dewey, the psychic boy who destroyed the plane, kills Yates, as he now knows he would . This movie differs from how the book ended. Joe and the girl are on the run and the boy dies

==Cast==
* Billy Zane - Joe Carpenter
* Gloria Reuben - Rose Tucker
* Isabella Hofmann - Barbara Christman
* Rachel Victoria - 21-21
* Mitchell Kosterman - Becker
* Wally Dalton - Jeff Ealing
* Christine Willes - Mercy Ealing
* Glenn Morshower - Robert Donner
* John C. McGinley - Victor Yates
* Dan Joffre - Dewey
* Susan Bain - Clarise Vadance

==Awards==
The film was nominated, in the company Academy of Science Fiction, Fantasy & Horror Films for a Saturn Award for Best Single Genre Television Presentation in 2001.

==External links==
*  

 
 

 
 
 
 
 
 
 
 