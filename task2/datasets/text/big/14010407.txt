Seven Waves Away
{{Infobox Film
| name           = Seven Waves Away
| image          = Poster of the movie Seven Waves Away.jpg
| image_size     = 
| caption        = US theatrical release poster Richard Sale
| producer       = John R. Sloan Tyrone Power (uncredited)
| based on       =  
| narrator       = 
| starring       = Tyrone Power Mai Zetterling Lloyd Nolan Stephen Boyd
| music          = Arthur Bliss
| cinematography = Wilkie Cooper
| editing        = Raymond Poulton
| studio         = Copa Productions
| distributor    = Columbia Pictures
| released       = April 17, 1957 (US)
| runtime        = 97 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 1957 UK|British drama film starring Tyrone Power, Mai Zetterling, Lloyd Nolan, and Stephen Boyd. When his ship goes down, an officer has to make an agonizing decision on his overcrowded lifeboat. 
 William Brown in 1841. The William Brown hit an iceberg 250 miles off Newfoundland and lost 31 of its 65 passengers. Two boats with 17 crewmen and the remaining passengers escaped the wreck, but more than a dozen passengers were sacrificed from the crowded longboat. 

Though there is no direct acknowledgment by the filmmakers, the film ends with a voice-over stating, "The story which you have just seen is a true one. In real life, Captain   Alexander Holmes was brought to trial on a charge of murder. He was convicted and given the minimum sentence of six months because of the unusual circumstances surrounding the incident."

==Plot== luxury liner mine in lifeboat designed John Stratton), the ships radio operator, that both transmitters were destroyed before a call for help could be sent. Holmes decides to try to reach the nearest land, Africa, 1500 miles away.

With a major storm approaching, Frank Kelly (Lloyd Nolan) warns Holmes that the overloaded boat will be swamped unless some of the passengers are jettisoned. The mortally injured Kelly then sacrifices himself by jumping overboard. Holmes decides to get rid of the old and injured, over the shocked protests of his girlfriend, ships nurse Julie White (Mai Zetterling). When he orders Will McKinley (Stephen Boyd) to dispose of an unconscious woman, McKinley obeys, then jumps in after her. One by one, Holmes sends others to certain death, until there are 15 left aboard. Edith Middleton (Moira Lister) observes that an atomic scientist, a brilliant playwright, and a famous former opera singer have been sacrificed to save two "apemen", a racketeer, and a devout coward. Passenger Michael Faroni (Eddie Byrne) demands Holmes go back for the others. When Holmes refuses, Faroni seriously wounds him in the shoulder with a switchblade and is in turn shot dead with a flare gun.

The lightened lifeboat weathers the storm and the rest of the survivors thank Holmes for saving them. Realizing he is now a liability due to his wound, Holmes throws himself overboard, but Julie brings him back aboard. Then, they spot a ship. As it comes to pick them up, the others, with the exceptions of Julie White and Edith Middleton, quickly distance themselves from Holmes actions.

==Cast==
* Tyrone Power as Executive Officer Alec Holmes
* Mai Zetterling as Julie White
* Lloyd Nolan as Frank Kelly
* Stephen Boyd as Will McKinley, a ships officer
* Moira Lister as Edith Middleton James Hayter as "Cookie" Morrow
* Finlay Currie as Mr. Wheaton John Stratton as Jimmy "Sparks" Clary
* Victor Maddern as Willy Hawkins
* David Langton as John Hayden
* Eddie Byrne as Michael Faroni
* Noel Willman as Aubrey Clark
* Moultrie Kelsall as Daniel Cane
* Robert Harris as Arthur J. Middleton Gordon Jackson as John Merritt
* Orlando Martins as Sam Holly

==See also==
* Souls at Sea, a 1937 Henry Hathaway film also based on the William Brown incident
* Lifeboat (film)|Lifeboat, 1944 Alfred Hitchcock film

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 