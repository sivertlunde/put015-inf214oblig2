The Recipe (film)
{{Infobox film name           =  The Recipe  image          = TheRecipe2010Poster.jpg caption        = Film poster director       = Anna Lee producer       = Kim Jin-young  writer         = Anna Lee   Jang Jin  starring       = Ryu Seung-ryong  Lee Yo-won  Lee Dong-wook music          = Han Jae-gwon cinematography = Na Hui-seok editing        = Kim Sang-bum   Kim Jae-beom distributor    = CJ Entertainment  released       =   runtime        = 107 minutes country        = South Korea language       = Korean budget         =  admissions     = 45,165  gross          = $293,898 
}}
The Recipe ( ; literally "Soybean paste") is a 2010 South Korean film about a television producer who finds out many surprising truths while tracking down the story about a mysterious bean paste stew. Behind the captivating taste lies a woman’s unfortunate life and love story.

==Plot==
Jang Hye-jin (Lee Yo-won) is an ordinary woman whose doenjang jjigae (soybean paste soup) is to die for. Yet Jang is not famous for her recipe and lives a quiet life.

When a notorious murderer on death row requests Jang’s soup as his last meal, television producer Choi Yoo-jin (Ryu Seung-ryong) starts looking for Jang and the recipe. But Choi is not the only one looking for Jang.

Kim Hyun-soo (Lee Dong-wook), Jang’s old flame, decides to return to his hometown to look for his first love. At the same time, three people die after eating Jang’s soup. What is the secret behind the soup and how many more people will have to die for it? 

==Production==
Twelve years after her last film Rub Love, director Anna Lee wonderfully mixes two different genres - the thriller and the love story. The English title "The Recipe" contains a lot of meaning: a record of a person’s life, a memory of taste, an experience of loss and a chronicle of hurts. In this sense, the film is a recipe for everything. With his original touch, Lee illustrates the obsession with unforgettable memories of love and a fatalistic point of view. The soybean soup recipe contains pain, longing and salvation. In a sense, the recipe is the very history of Korea’s memory and tradition. 

==Cast==
*Ryu Seung-ryong ...  Choi Yoo-jin
*Lee Yo-won ... Jang Hye-jin 
*Lee Dong-wook ... Kim Hyun-soo 
*Jo Sung-ha ... Park Min / Park Gu 
*Lee Yong-nyeo ... Han Myung-suk (owner of mountainside restaurant) 
*Kim Jung-suk ... Detective Kang 
*Nam Jeong-hee ... grandmother 
*Yu Seung-mok ... Kim Deuk-gu 
*Kim Se-dong ... blind man 
*Park Hye-jin ... owner of Soondae Soup restaurant 
*Yoo Soon-woong ...salt farm owner 
*Lee Sang-hee ... owner of small store in the countryside

==References==
 

==External links==
*   at Naver
*  
*  
*  

 
 
 
 
 
 