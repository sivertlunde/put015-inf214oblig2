Mighty Baby (film)
 
 
{{Infobox film
| name = Mighty Baby
| film name = {{Film name| traditional = 絕世好B
| simplified  = 绝世好B}}
| director = Chan Hing-Ka Patrick Leung
| producer = Chan Hing-Ka
| writer = Chan Hing-Ka
| starring = Lau Ching-Wan Louis Koo
| released =  
| budget = $19,021,894
| language = Cantonese
}} Hong Kong film directed by Chan Hing-Ka and Patrick Leung. It is a sequel to the 2001 La Brassiere.

==Cast and roles==
* Lau Ching-Wan - Johnny Hung
* Louis Koo - Wayne Koo
* Rosamund Kwan - Sabrina
* Gigi Leung - Lena Li
* Cecilia Cheung - Boey
* Carina Lau - Samantha
* Chikako Aoyama - Nanako
* Rosemary Vandenbroucke - Eileen
* Cherrie Ying - Ginger
* Vincent Kok - Ringo Li
* Tats Lau - Romeo
* Rachel Ngan - Emma
* GC Goo-Bi - Gigi
* Asuka Higuchi		
* Chim Sui-man - Raymond Kim
* Chapman To - Kassey
* Wong Chun-chun		
* Kate Yeung		
* Wilson Yip - Officer Yip

==Synopsis==

In this sequel to the "Mighty Bra", Lena (Gigi Leung), Johnny (Lau Ching Wan) and Wayne (Louis Koo) are tasked with developing the "Mighty B" line of baby products. Since the last successful development of the "Mighty Bra", Johnny (Lau Ching Wan) is now head of the department, he hires a neurotic secretary Sabrina (Rosamund Kwan). Lena and Wayne are planning their wedding, however, Wayne has "Baby-Phobia" which throws a wrench into their task of developing the premier "Mighty B" line of baby products. Lena hires child behavior expert Boey (Cecilia Cheung), to work with Wayne to overcome his phobia but things start to go awry when Wayne starts developing feelings for Boey.

==External links==
*  
*  
*  

 
 
 