Grama Kanya
{{Infobox film
| name           = Grama Kanya  
| image          = 
| image_size     = 
| caption        = 
| director       = Sarvottam Badami
| producer       = Sagar Movietone
| writer         = Jayant Shyam
| narrator       =  Surendra Yakub Yakub Aruna
| music          = Shankarrao Khatu
| cinematography = Faredoon Irani
| editing        = 
| distributor    =
| studio         = Sagar Movietone
| released       = 1936
| runtime        = 137 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1936 Hindi melodrama film directed by Sarvottam Badami.    The film produced by Sagar Movietone had cinematography by Faredoon Irani, story by Jayant Shyam and dialogue by Waqif.    The music was composed by the renowned bhajan singer Shankarrao Khatu.    The cast included Surendra, Sabita Devi, Yakub (actor)|Yakub,  Aruna, Kayam Ali, and Gulzar. 

The story is about a young man, Kumar (Surendra), who loves a girl, Vilas (Aruna), but has to marry another Bansari (Sabita Devi), due to family obligations. The film follows the complications that arise out of the situation. 

==Plot==
Kumar (Surendra) comes to the city from his village to study in college. His father has been sanctioned a loan from the rich Dinanath (Kayam Ali) to enable Kumar to study. Dinanath gives the loan on the basis of Kumar getting married to his daughter Bansari (Sabita Devi). Kumar is in love with another girl Vilas (Aruna), and wants to marry her. Parental obligations and duty make him forgo Vilas who is pregnant and get married to Bansari. The story continues with the accidental killing of his father by Kumar, and Vilas narrative from thereon. 

==Cast==
* Sabita Devi as Bansari
* Surendra as Kumar Yakub as Vinod
* Aruna as Vilas
* Kayam Ali as Dinanath
* Gulzar
* Sankatha Prasad
* Baby Indira
* Pande
* Jamu Patel

==Music== 
The music director was the renowned bhajan singer Shankarrao Khatu. According to Ranade, the songs from the film like "Tulsi Mata Pyari" and "Kanhaiya Bansiwale Re" became popular.    There were nine songs in the film and the singers were Surendra, Rajkumari and Sabita Devi. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Jhootha Hai Sansar
| Surendra
|-
| 2
| Kanhaiya Bansiwale Re Haan Main Na Boloongi
| Rajkumari
|-
| 3
| Kis Ne Hai Yeh Reet Banai Surendra
|-
| 4
| Prem Ek Umang Hai
| 
|-
| 5
| Rain Andheri Chhai Dukh Badri Ghir Aayi
|
|-
| 6
| Ras Bhare Nain Tore Balam ghatayein Barsan Lagi
| Surendra
|-
| 7
| Tulsi Maat Pyari Tum Sati Ho Dukh Niwari
| Rajkumari
|}
     

==References==
 

==External links==
* 

 

 
 
 