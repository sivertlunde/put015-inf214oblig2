Jungle Master
{{Infobox film
| name           = Jungle Master
| image          = Jungle Master poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Kerr Xu
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = Hippo Animation
| distributor    = 
| released       =   
| runtime        = 
| country        = China
| language       = Mandarin
| budget         = 
| gross          = United States dollar|$0.8 million 
}}
Jungle Master (Chinese: 绿林大冒险) is a 2013 Chinese computer-animated film directed by Kerr Xu.  The English dub features the voices of Victoria Justice, David Spade, Josh Peck, Jon Lovitz, Christopher Lloyd, and Jane Lynch.

==Plot==
 
When a girl named Rainie runs away from home after her mom forgot about her birthday, she is magically transported from the big city to an enchanted jungle in an unfamiliar land where she meets Blue. With the help of Blues grandfather and their newfound friend Mulla, they embark on an epic adventure to help Blue become the leader he is destined to be and save the rainforest from the villainous Boss Cain. 

==Cast==
===English cast===
* Victoria Justice - Rainie
* David Spade - Boss Cain
* Josh Peck - Blue
* Jon Lovitz - Mulla
* Christopher Lloyd - Dr. Wells
* Jane Lynch - Ilene
* Shondalia White - Tulla
* Jason Sarayba - Justin
 Steve Kramer, Michelle Ruff, and Keith Silverstein

==References==
 

 
 
 
 


 
 