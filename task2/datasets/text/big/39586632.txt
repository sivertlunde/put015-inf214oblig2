The Little Napoleon
{{Infobox film
| name           = The Little Napoleon
| image          = 
| image_size     = 
| caption        = 
| director       = Georg Jacoby 
| producer       = 
| writer         = Robert Liebmann   Georg Jacoby
| narrator       = 
| starring       = Egon von Hagen   Paul Heidemann   Harry Liedtke   Jakob Tiedtke
| music          = 
| editing        = Max Schneider   Emil Schünemann   Walter von Gudenberg
| studio         = European Film Alliance
| distributor    = 
| released       = 29 November 1923
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent historical historical comedy film directed by Georg Jacoby and starring Egon von Hagen, Paul Heidemann and Harry Liedtke. It depicts the life and amorous adventures of Jérôme Bonaparte, the younger brother of Napoleon, who installed him as King of Westphalia.

The film is today best known for the small role played by Marlene Dietrich as Kathrin, a ladys maid. Her brief appearance was filmed over several days during the summer of 1922.  It marked Dietrichs film debut, though she was later unhappy with her early silent films.  She was cast after impressing the director, Georg Jacoby, during a meeting arranged for them. Dietrich played gradually more substantial roles during the rest of the decade until her breakthrough film The Blue Angel (1930).
 Paramount to establish a production base in the lucrative German market.  The EFA was wound up shortly after the films completion, which delayed its release.  It finally premièred on 29 November 1923 at the Marmorhaus in Berlin. 

==Cast==
* Egon von Hagen as Napoleon Bonaparte 
* Paul Heidemann as Jérôme Bonaparte 
* Harry Liedtke as George von Melsungen 
* Jakob Tiedtke as Jeremias von Katzenellenbogen 
* Wilhelm Bendow as Jerômes Diener 
* Paul Biensfeldt as Feldmarchall 
* Marquisette Bosky as Primaballerina 
* Antonia Dietrich as Charlotte 
* Marlene Dietrich as Kathrin 
* Kurt Fuß as Leiter des königlichen Baletts 
* Alice Hechy as Annemarie 
* Loni Nest as Lieselotte 
* Kurt Vespermann as Florian Wunderlich 
* Loni Pyrmont

==References==
 

==Bibliography==
* Bach, Steven. Marlene Dietrich: Life and Legend. University of Minnesota Press, 2011.
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.
* Spoto, Donald. Blue Angel: The Life of Marlene Dietrich. Rowman & Littlefield, 2000.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 