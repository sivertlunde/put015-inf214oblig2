Cry of Battle
{{Infobox film
| name           = Cry of Battle
| image          = Cryobatpos.jpg
| caption        = Original film poster
| director       = Irving Lerner
| producer       = Joe Steinberg Eddie Romero Bernard Gordon
| based on       =  
| starring       = James MacArthur Van Heflin Rita Moreno Leopoldo Salcedo
| music          = Richard Markowitz
| cinematography = Felipe Sacdalan
| editing        = Allied Artists
| released       =  
| runtime        = 99 minutes
| country        = United States English
}}
Cry of Battle is a 1963 American coming of age war film based on the 1951 novel Fortress in the Rice by Benjamin Appel who was a journalist and special assistant to the U.S. Commissioner for the Philippines from 1945-46. The film stars Van Heflin,  James MacArthur, Rita Moreno, Leopoldo Salcedo and Sidney Clute. Set during the Japanese occupation of the Philippines, the working title was To Be a Man. 

==Plot== Japanese attacking Tagalog as he waits for news.

Careo returns again to tell Dave that his father has left the Philippines, but Dave is joined by a fellow American, Joe Trent, a rough merchant sailor who was Third Mate on a cargo ship that was sunk by the Japanese.  Joes ship was one of a merchant line owned by Daves father and Joe schemes that Daves wealthy father will reward him for keeping his son safe. Joe repays the Filipino familys hospitality by raping the granddaughter and advises Dave to flee with him as Joe tells him the angry locals will kill him first and ask questions later. Faced with the hysterics of the raped granddaughter, Dave flees with Joe to join an American guerilla unit led by Colonel Ryker.

On the way they meet a band of armed Filipinos led by Atong and the English speaking woman Sisa.  The quick thinking Joe tells the band that if they bring them to Colonel Ryker, Ryker will reward them. After being brought to Ryker, he asks for proof of Daves identity and tells him that the Japanese would probably give him a comfortable existence and also might repatriate him to the United States on a neutral ship due to his fathers business dealings with Japan.  Dave replies that his collaboration with Japan was before the war and he would rather fight with the guerillas. The group join Rykers guerillas and over time participate in many operations against the Japanese.

Joe is promoted to Lieutenant and is to accompany a Filipino Captain on a raid against the target of a Japanese held sugar refinery and railway. Joe brings Dave, Atong, Sisa and a group of their original armed band on the mission led by the Captain.  After the Captain is killed Joe sizes up Atong when Atong kills one of his own men over the ownership of the dead Captains pistol. Joe demonstrates his control by making Atong give the pistol to Dave. Not wishing to complete their mission, Joe sends Dave and Sisa into a village to ask the locals for food.  As they are negotiating, Joes band massacres the villagers to steal their rice with Joe shooting Atong during the raid. Sisa quickly switches her loyalties to Joe.

==Production== Bernard Gordon to write the screenplay.  Gordon saw the opportunity to use the screenplay as a comment on American attitudes towards Third World people and attitudes towards masculinity explaining the films working title of To Be a Man.   During the film ,Dave asks Joe if raping his hosts granddaughter made him feel like a man.  Joe responded that fighting when you have to and having a woman when you can was feeling like a man. Joe also initiates Dave into manhood by using his winnings in a poker game to give Dave a night with a prostitute.  In Daves first battle, Dave captures a panicked Japanese soldier with Joe grabbing Daves hands holding his rifle with bayonet that he thrusts into the prisoner.
 Best Supporting West Side Story. A Filpino designed her dress for the Awards ceremony with Edith Head voting her dress the most original of the night.  She returned the Philippines the next day. 

Morenos planned nude bathing scene in the film attracted a great deal of publicity.  She eventually filmed the scene wearing a dress. 

==In popular culture== War Is Hell) playing at the Texas Theatre in Dallas, Texas on November 22, 1963. After allegedly shooting President John F. Kennedy, Governor John Connally, and Dallas patrolman J.D. Tippit earlier that day, Lee Harvey Oswald snuck into this theater without paying. After box office cashier Julie Postal received a tip on Oswald from nearby shoe store employee John Brewer, she called Dallas Police and Oswald was arrested before the movie ended.  Oswald was fatally shot two days later while being transferred to another jail by club owner Jack Ruby. 

==Notes==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 