Ripper 2: Letter from Within
{{multiple issues|
 
 
}}

{{Infobox Film
| name = Ripper 2: Letter from Within
| image = Ripper2withincover.jpg
| image_size =
| caption = 
| director = Jonas Quastel Lloyd A. Simandl
| producer = Jonas Quastel
| writer = John A. Curtis   Pat Bermel
| narrator =
| starring = Erin Karpluk   Nicolas Irons Peter Allen
| cinematography = John Drake
| editing = David Campling
| distributor = Velocity Home Entertainment
| released =    
| runtime = 89 min.
| country = United Kingdom
| language = English
| budget =
}}

Ripper 2 (aka US title: Ripper 2: Letter from Within) is a 2004 British horror film directed by Jonas Quastel, starring Erin Karpluk and Nicolas Irons, and is a sequel to Ripper (film)|Ripper. It was written and produced by John A. Curtis and Pat Bermel. 

== Plot ==
The director of an asylum offers to the serial killer Molly Keller a chance to be submitted to a pilot unconventional experiment in Prague, in the Weisser Institute. Molly accepts, and she travels to the clinic, where Dr. Samuel Wiesser developed a treatment using a virtual world, and Molly and deranged youngsters would be trial subjects. However, something does not work well in the experiment, and when the patients die in their trip, the same happens in the real world. The explanation is a huge twist point in the very end of the story.

== Cast ==

* Erin Karpluk as  Molly Keller
* Nicholas Irons as Erich Goethe
* Mhairi Steenbock as Juliette Dureau
* Jane Peachey as Lara Svetlana
* Daniel Coonan as Grant Jessup
* Colin Lawrence as Roberto Edwards
* Myfanwy Waring as Sally Trigg
* Andrea Miltner as Marya
* Curtis Matthew as Psychologist
* Richard Bremmer as Dr. Samuel Wiesser

== References ==
 

== External links ==
*  

 
 
 
 
 

 