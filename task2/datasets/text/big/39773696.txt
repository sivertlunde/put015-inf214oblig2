Crucified Girl
{{Infobox film
| name           = Crucified Girl
| image          = 
| image_size     = 
| caption        = 
| director       = Jacob Fleck   Luise Fleck
| producer       = Liddy Hegewald   Gustav Althoff
| writer         = Marie Luise Droop   Ludwig Fritsch
| narrator       = 
| starring       = Valerie Boothby   Gertrud de Lalsky   Evelyn Holt   Robert Leffler
| music          = 
| editing        = 
| cinematography = Nicolas Farkas 
| studio         = Hegewald Film
| distributor    = Hegewald Film 
| released       = 26 August 1929
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent drama film directed by Jacob Fleck and Luise Fleck  and starring Valerie Boothby, Gertrud de Lalsky and Evelyn Holt. The films art direction was by Artur Gunther and August Rinaldi.

==Cast==
* Valerie Boothby   
* Gertrud de Lalsky   
* Evelyn Holt   
* Robert Leffler   
* Fritz Odemar   
* Livio Pavanelli   
* Ernő Verebes  
* Wolfgang Zilzer

==References==
 

==Bibliography==
*Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910–1933. Berghahn Books, 2005.

==External links==
* 

 
 
 
 
 
 
 
 


 
 