Dirty Little Billy
{{Infobox film
| name           = Dirty Little Billy
| image          = Billythekidposter.jpg Promotional film poster
| director       = Stan Dragoti
| producer       = Jack L. Warner
| writer         = Charles Moss Stan Dragoti
| narrator       =  Richard Evans Lee Purcell Frank Welker Gary Busey
| music          = Sascha Burland
| cinematography = Ralph Woolsey
| editing        = David Wages
| distributor    = Columbia Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}}
Dirty Little Billy is a western film released in 1972 and was the directorial debut of Stan Dragoti. It stars Michael J. Pollard as Billy the Kid. Gary Busey plays a small role. Clearly influenced by the darker, more sinister style of spaghetti westerns, The film offered a unique insight into the beginnings of the notorious outlaw Billy the Kid. Nick Nolte made his feature film debut as town gang leader.

==External links==
*  

 
 

 
 
 
 
 
 
 

 