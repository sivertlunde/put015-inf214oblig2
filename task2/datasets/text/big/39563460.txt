Child 44 (film)
{{Infobox film
| name           = Child 44
| image          = Child 44 poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Daniel Espinosa
| producer       = {{Plainlist|
* Ridley Scott
* Michael Schaefer
* Greg Shapiro
}} Richard Price
| based on       =  
| starring       = {{Plainlist|
* Tom Hardy
* Gary Oldman
* Noomi Rapace
* Joel Kinnaman Jason Clarke
* Vincent Cassel
}}
| music          = Jon Ekstrand
| cinematography = Philippe Rousselot
| editing        = Dylan Tichenor
| studio         = {{Plainlist|
* Summit Entertainment
* Worldview Entertainment
* Scott Free Productions
}} Lionsgate
| released       =  
| runtime        = 137 minutes
| country        = {{Plainlist|
* United States
* United Kingdom
*Czech Republic 
*Romania}}
| language       = English
| budget         = $50 million 
| gross          = $3.3 million   
}} mystery thriller thriller film Richard Price, Jason Clarke, bombed at the box office.

==Plot== Ministry of State Security (MGB) Agent Leo Demidov (Tom Hardy) uncovers a strange and brutal series of child murders by a serial killer who everyone claims does not exist because it is Soviet doctrine that capitalism creates serial killers, not communism.

==Cast==
* Tom Hardy as Leo Demidov
* Noomi Rapace as Raisa Demidova
* Joel Kinnaman as Vasili Nikitin
* Gary Oldman as General Nesterov
* Vincent Cassel as Major Kuzmin Jason Clarke as Anatoly Brodsky
* Josef Altin as Alexander
* Sam Spruell as Doctor Tyapkin
* Ned Dennehy as The Coroner
* Fares Fares as Alexei Andreyev
* Nikolaj Lie Kaas as Ivan Sukov
* Anna Rust as Sasha
* Xavier Atkins as Pavel
* Sonny Ashbourne Serkis as Artur
* Kevin Clarke as MGB Agent
* Petr Vanek as Fyodor
* Max Rowntree as Andrej 
* David Bowles
* Michael Nardone as Semyon Okun
* Fedja Stukan as Sergei
* Anssi Lindström as Alexander Pickup
* Harmon Joseph as Vadim
* Charles Dance as Major Grachev
* Tara Fitzgerald as Inessa Nesterov
* Samuel Buttery as Varlan Babinic
* Agnieszka Grochowska as Nina Andreeva

==Production==
Principal photography began in June 2013 in Prague, Ostrava, Kladno Czech Republic. 

==Release== Russian Ministry Victory Day Great Patriotic War and images and characters of Soviet people of that era".  Russian minister of culture Vladimir Medinsky welcomed the decision, but stressed that it was made solely by the Central Partnership. However, in his personal statement Medinsky complained that the film depicts Russians as "physically and morally base sub-humans", and compared the depiction of Soviet Union in the film with J. R. R. Tolkiens Mordor, and wished that such films should be screened neither before the 70th anniversary of the victory in the Great Patriotic War, nor any other time.  However, he also stated that the film would be available in Russia on DVD and online. 

As a result of the decision the film was also withdrawn from cinemas in Belarus,  Ukraine,  Kazakhstan, and Kyrgyzstan, while release of the film has been postponed until October in Georgia (country)|Georgia. 

Ukrainian film director and producer Alexander Rodnyansky criticized the decision not to release Child 44 as bad for the countrys film industry. "Before, films where Soviet and Russian heroes were presented not in the best way have been released in Russia, but nothing similar happened. Now everything to do with history should clearly fit into a kind of framework set by the culture ministry." 

==Reception==
Critical response to Child 44 ranged from mixed to negative. On Rotten Tomatoes, the film has a rating of 23%, based on 64 reviews, with an average rating of 4.6/10. The sites critical consensus reads, "Theres a gripping story at the heart of Child 44 and a solid performance from Tom Hardy in the lead, but it all still adds up to a would-be thriller that lacks sufficient thrills."  On Metacritic, the film has a score of 41 out of 100, based on 25 critics, indicating "mixed or average reviews". 

Andy Lea of the Daily Star Sunday gave Child 44 three stars out of five. He wrote that "the film is less than a sum of its parts" and is "a little bogged down with subplots". However, Lea said that Hardy "is excellent in the lead role" and Espinosa "crafts some brilliant individual scenes". 

Writing in The Guardian, Peter Bradshaw gave the film 2 stars out of 5 and reported that "Tom Rob Smith’s page-turning bestseller from 2008 has been turned into a heavy, indigestible meal of a film, full of actors speaking English with vyery hyeavy Ryussian accyents – actors from England, Sweden, Lebanon, Poland, Australia, almost anywhere but Russia". Bradshaw added that, "Tom Hardy brings his robust, muscular presence to the role of Leo and he is watchable enough, but the forensic and psychological aspects are just dull; there is no fascination in the detection process.   Everything is immersed in a cloudy brown soup".    Also in The Guardian, reviewer Phil Hoad wrote that: "Child 44 has a fascinating premise and setting   failed to convincingly package this as either an upscale thriller along the lines of Tinker Tailor Soldier Spy, as implied by a powerhouse cast also featuring Gary Oldman, Noomi Rapace and Paddy Considine; or as something racier à la The Girl with the Dragon Tattoo or Gone Girl (indeed, the film itself falls awkwardly between these two stools)". Hoad added, "As for the debacle over the Slavic-slathered English spoken by the entire cast, it further highlights the uncertainty about whether Child 44 was intended for the multiplex or the arthouse. Presumably a decision made to placate the former, opting to turn the film into an Iron Curtain version of ’Allo ’Allo damaged its integrity. Aren’t we past this kind of cultural bastardisation? It is possible for foreign-language films to cross over: The Lives of Others, which meted out its own totalitarian intrigue in German, took $66m overseas – the kind of cash Child 44 will never see".   

In The Observer, Jonathan Romney found that: “In writer Richard Price’s boil-down of the labyrinthine original, the whodunit loses all momentum” adding that “the whole thing is scuppered by having everyone speak in borscht-thick Russian accents” before concluding that, "  shot in several shades of Volga mud and drags like a Thursday afternoon in Nizhniy Novgorod".   

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 