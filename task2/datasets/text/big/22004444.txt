The Prussian Cur
{{Infobox film
| name = The Prussian Cur
| image =The Prussian Cur (1918) - 1.jpg
| caption =Film still
| director = Raoul Walsh William Fox
| writer = Raoul Walsh
| narrator =
| starring = Miriam Cooper
| cinematography = Roy F. Overbaugh
| editing =
| studio = Fox Film Corporation
| distributor =
| released =  
| runtime = 7 reels (approximately 70 minutes)
| country = United States
| language = English
| budget =
| gross =
}} silent propaganda film produced during World War I. Now considered a lost film, it is notable for telling the story of the Crucified Soldier.

The films director Raoul Walsh called it his "rottenest picture ever" for its anti-German sentiment, while its star Miriam Cooper (Walshs wife) also held it to be the worst film in which she had ever appeared. 
 Fox and many other early film studios in Americas first motion picture industry were based at the beginning of the 20th century.   

==Cast==
*Miriam Cooper – Rosie OGrady
*Sidney Mason – Dick Gregory Captain Horst von der Goltz – Otto Goltz
*Leonora Stewart – Lillian OGrady James Marcus – Patrick OGrady Pat OMalley – Jimmie OGrady Walter McEwen Count Johann von Bernstorff William W. Black – Wolff von Eidel Ralph C. Faulkner – Woodrow Wilson Walter M. Wilhelm II Charles Reynolds Wilhelm I Crown Prince Frederick Field Marshal von Hindenburg Admiral von Tirpitz
*John E. Franklin – James W. Gerard
*John W. Harbon – U.S. Congressman

==See also==
*List of lost films

==References==
 
* 

==External links==
* 
* 

 

 
 
 
 
 
 


 