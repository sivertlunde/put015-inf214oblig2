Fløjtespilleren
 
{{Infobox Film
| name           = Fløjtespilleren
| image          = Fløjtespilleren.jpg
| image size     = 
| caption        = 
| director       = Alice OFredericks
| producer       = Henning Karmark
| writer         = Grete Frische Morten Korch
| narrator       = 
| starring       = Helga Frier
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Wera Iwanouw
| distributor    = 
| released       = 18 December 1953
| runtime        = 98 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
| preceded by    = 
| followed by    = 
}}

Fløjtespilleren is a 1953 Danish family film directed by Alice OFredericks. 

==Cast==
* Helga Frier - Cirkusdirektør Karla
* Peter Malberg - Laurits Laust Jeppesen
* Poul Reichhardt - Kurt / Martin Vest
* Grethe Holmer - Bodil Nielsen
* Ib Schønberg - Onkel Hans
* Ib Mossin - René
* Louis Miehe-Renard - Jack Hviid
* Jeanne Darville - Rosa Anita Mogensen
* Ove Rud - Frank Otto Munk
* Jakob Nielsen - Sorte Henrik
* Thorkil Lauritzen - Sprechstallmeister
* Beatrice Bendtsen - Kvinde i merkanteri
* Ole Neumann - Dreng
* Rudi Hansen - Cirkuspige
* Otto Møller Jensen - Cirkusdreng
* Knud Hallest - Toldbetjent
* Povl Wøldike - Læge
* Irene Hansen - Marianne
* Alma Olander Dam Willumsen - Kurts mor (as Alma Olander Dam)
* Margrethe Nielsen - Fru Hviid
* Hans Egede Budtz - Kommandør

==External links==
* 

 

 
 
 
 
 
 

 