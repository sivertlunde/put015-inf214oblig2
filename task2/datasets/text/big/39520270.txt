Let's Live Tonight
{{Infobox film
| name           = Lets Live Tonight 
| image          = 
| image_size     = 
| caption        = 
| director       = Victor Schertzinger 
| producer       = Robert North
| writer         = Bradley King   Gene Markey
| narrator       = 
| starring       = Lilian Harvey   Tullio Carminati   Janet Beecher   Hugh Williams
| music          = 
| editing        = Gene Milford Joseph Walker 
| studio         = Columbia Pictures  
| distributor    = Columbia Pictures 
| released       = March 16, 1935
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} musical comedy Columbia for Invitation to Black Roses, which relaunched her German career.

==Partial cast==
*   Lilian Harvey as Kay Carlotta Routledge 
* Tullio Carminati as Nick Monte Kerry 
* Janet Beecher as Mrs. Routledge 
* Hugh Williams as Brian Kerry 
* Tala Birell as Countess Margot de Legere 
* Luis Alberni as Mario Weems 
* Claudia Coleman as Lily Montrose 
* Arthur Treacher as Ozzy Featherstone 
* Gilbert Emery as Maharajah de Jazaar 
* Virginia Hammond as Mrs. Mott 
* Adrian Rosley as Cafe Propreitor 
* Max Rabinowitz as Pianist 
* André Cheron (actor)|André Cheron as Frenchman 
* John Binet as French Steward

==References==
 

==Bibliography==
* Ascheid, Antje. Hitlers Heroines: Stardom and Womanhood in Nazi Cinema. Temple University, 2003.
* Bergfelder, Tim & Cargnelli, Christian. Destination London: German-speaking emigrés and British cinema, 1925-1950. Berghahn Books, 2008.

==External links==
* 
 
 
 
 
 
 
 
 

 