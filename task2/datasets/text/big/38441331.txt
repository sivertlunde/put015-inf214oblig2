Sonna Puriyathu
{{Infobox film
| name           = Sonna Puriyathu
| image          = Sonna puriyathu.jpg
| alt            = 
| caption        = poster
| director       = Krishnan Jayaraj
| producer       = 360 Degree Film Corp
| writer         = K. Chandru D. Saravana Pandian (dialogues)
| story          = Krishnan Jayaraj Yatish Mahadev
| screenplay     = Krishnan Jayaraj Shiva   Vasundhara Kashyap
| music          = Yathish Mahadev
| cinematography = Saravanan
| editing        = T S Suresh
| studio         = 360 Degree Film Corp
| distributor = Sri Thenandal Films
| released       = July 26th 2013
| runtime        = 138 minutes
| country        = India
| language       = Tamil
| budget         = 4.5 Crores
| gross          = 
}} Tamil comedy Shiva and Vasundhara in the lead roles.  The film released on 26 July 2013 to mixed-positive reviews from critics. The film got a good opening  and finally declared as an above average in the boxoffice. 

==Plot==
Shiva (Shiva (actor)|Shiva) is a dubbing artist and a person who is not happy with love and marriages. His dream is to buy a Volkswagen car for himself. But his mother (Meera Krishnan) forces him by blackmailing him to marry and he finally accepts without satisfaction. The girl is Anjali (Vasundhara Kashyap) who is revealed to be a devotional girl and is a press reporter. When the marriage of one of Shivas friends takes place, both Anjali and Shiva are invited. Anjali is invited because the friend was forced by Shiva in order to stop their marriage. But in that marriage, Shiva gets to know that Anjali is the exact opposite of what he thought she was, and that Anjali herself wanted to stop their marriage. They finally break up and they make their parents also accept (Shiva lied to Anjalis father that he got hurt during cricket match and Anjali cooks up a plot claiming that she became pregnant).

Shiva and Anjali celebrate their marriage cancellation in a pub. The next day, they found themselves in a hotel room together. Shiva and Anjali suspects that Shiva friend Gowri did the arrangement but after review the CCTV footage it is revealed that a Seth guy did the arrangement. Shiva and Anjali solve the matter and go to Anjalis house to drop her. Anjalis father prepared marriage with Anjali co-worker. Few days later the broker Rajesh Kanna (Manobala), who helped Shivas mother, visits them with the news that they are going to organize a game show. The price money from the game show will help Shiva to achieve his long time dream. In the game, Shiva and Anjali win all the rounds. Anjali started falling in love with Shiva. Anjali feels sad when she comes to know that Shiva participated in the game only to win Volkswagen Car and breaksup with him. With the insistence of his friends, Shiva realises Anjalis love. In order to unite with Anjali, Shiva set up a plan by making a stage actress to act as bridegroom to marry so that Anjali would marry him. Rajesh Kanna who comes to know of Shivas plan kidnaps Shiva. Later he leaves Shiva when he claims that Anjali is going to marry someone. When Siva arrives at register office. He sees Anjali getting married. Saddened, Shiva goes to a park and he witnesses Flashmob dancing towards him. It is revealed that it was Anjalis plan to surprise Shiva whereas his friend was also involved in this plan. Shiva and Anjali gets married and live happily.

==Cast== Shiva as Shiva
* Vasundhara Kashyap as Anjali
* Blade Shankar as Shivas friend
* Pradeep Nair as Bimbo
* Jacqueline as Shwetha
*Jangiri Madhumitha
* R. S. Sivaji as Anjalis father
* Manobala as Rajesh Khanna
* Gangai Amaran as TV show host
* Meera Krishnan as Shivas mother Aarthi
* Vatsala Rajagopal as Shivas grandmother
* Singamuthu as Naattamai
* Rajini Nivetha as Sharmilla Tagore
* Raghav as Settu Paiyan
* Saravanan
* Halva Vaasu Sam Anderson as Himself (Cameo appearance)

==Production==
 
Shiva will be seen in the role of dubbing artist who dubs for Tamil dubbed Hollywood movies and he plays a character who is scared of marrying.  Sam Anderson who is best known for Yaaruku Yaaro made a cameo appearance.  The film was shot extensively in Chennai and Gobichettipalayam.

==Soundtrack==
{{Infobox album|  
| Name        = Sonna Puriyathu
| Type        = Soundtrack
| Artist      = Yatish Mahadev
| Cover       = Sonna Puriyathu Audio Cover.jpg
| Border      = 
| Alt         = 
| Caption     = Front Cover
| Release     = 
| Recorded    = 2013 Feature film soundtrack
| Length      =  Tamil
| Label       = Think Music
| Producer    = Yatish
| Last album  = Indira Vizha  (2009)
| This album  = Sonna Puriyathu  (2013)
| Next album  = Varusanaadu  (2013)
| Reviews     =
}}
Music is composed by Yathish Mahadev who earlier composed for Indira Vizha. Lyrics were written by Na Muthukumar and Madhan Karky. Shiva wrote the lyrics for the films Hindi number Rosa Heh, in 20 minutes time, and sang it in 10 minutes.  The song was not included in the audio CD.  The audio was released in 9 January 2013.  Behindwoods wrote:"Sonna Puriyadhu’s music is simple and to the point". 

* Kelu Magane Kelu - Jagadeesh
* Kaaliyana Saalaiyil - SPB Charan, Chinmayi
* Devathaya Rakshasiya - Ranjith
* Un Tholil Saindhu - MK Balaji
* Ayyayo Poche - MK Balaji, Yatish
* Sonna Puriyathu - Theme music
* Sagaroo - Instrumental
* Dance to it - Instrumental

==Marketing==
Posters where Shiva mocking James Bond, Titanic, Harry Potter were released in 2012.  

==Release==
The satellite rights of the film were secured by Kalaignar TV. The film was given a "U" certificate by the Indian Censor Board and the Distribution rights were initially purchased by Studio Green  but later rights have been acquired by Sri Thenandal Films. 

==Reception==
Sonna Puriyathu received mixed reviews from critics. The New Indian Express said, "Mildly amusing and a promising work by a debutant, Sonna Puriyathu could have done with more punch in its screenplay".  The Hindu showered accolades on the movie with the editor Sudhish Kamath giving the movie a 7/10 in his Twitter account and called everything is right about the movie, citing that "A breezy anti-romance comedy that works as a Tamizh Padam spin off with enough laughs to merit a watch.". 
Indiaglitz.Com rated 2.75/5 and gives a verdict that "One word – Shiva, he rocks, sizzles throughout his screen presence with a volley of one liner and gimmicks.  MetroMasti.Com rated 3/5 and with a verdict "Sonna Puriyathu proves to be a comic caper and entertains audience as well. Shiva is perfect with his comedy timings as ever and Vasundhara is back with a perfect role after Poraali and has done full justice to it." The movie is running to packed houses with increased shows in metropolitan cities like Chennai, Bangalore garnering more positive reviews from the audiences saying this is the best performance Shiva has ever done."  Times of India gave 3 stars for this movie. 

==References==
 


==External links==
*  

 
 
 
 
 