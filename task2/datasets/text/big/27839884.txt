Shadow Magic
 
{{Infobox film
| name           = Shadow Magic 西洋镜
| image          = 
| image_size     = 
| caption        = 
| director       = Ann Hu
| producer       = Ann Hu Han Sanping
| writer         = Ann Hu Chen Yixiong Huang Dan Liu Huaizhuo Bob McAndrew Kate Raisz Tang Louyi Zuo Shiqiang
| narrator       =  Xia Yu Jared Harris
| music          = Zhang Lida
| cinematography = Nancy Schreiber Wen Deguang John Gilroy
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 115 minutes
| country        = China Mandarin English
| budget         = 
}}
 China co-production Xia Yu, Jared Harris and Xing Yufei.  The movie was Ann Hus directorial debut.

==Plot== camera and projector with some silent film shorts from the West. Liu is fascinated by the "shadow magic" and offers to help Raymond bring in audiences for his theatre.  Hence Liu and Raymond become partners.  However, while the innovation thrills many, the more conservative Chinese in the capital frown upon the creation as a Western "pollutant".

Liu falls in love with Ling (Xing Yufei), the daughter of a well-respected Beijing Opera star, Lord Tan Xinpei (Li Yusheng).  However, as he is poor and only an apprentice at the photography shop, Liu has no chance of marrying Ling.  Instead, Lius father and his boss at the Fengtai photography shop, Master Ren (Liu Peiqi), try to arrange a marriage for him with an older widow, Jiang (Fang Qingzhuo).  When Master Ren finds out Liu has been working for a foreigner dabbling with "shadow magic", he becomes infuriated. Liu tries to reason with Master Ren, explaining to him that this "shadow magic" is an innovation in the world of photography, and once Liu learns more about it they should incorporate it into their own business. Master Ren is not swayed, however, and dismisses him. Because of this, Liu is also disowned by his father.  Liu and Raymonds friendship becomes tested as he feels the strain of being branded a traitor in his own country.
 banished from China.

Half a year passes, and Liu recovers from his burns.  Raymond, fondly remembering their friendship and times in China, sends him the nitrate negatives of movies they shot together in Beijing, imploring that he should not let the work they started together die.  Liu decides to try making a film projector himself and successfully screens these local movies to a group of Beijing natives.  Lius father decides to support him in this venture and Ling visits him on the sly at the projection room, where they kiss.  The film ends with the Beijing natives gradually accepting the motion picture technology, once they see how beautifully it captures the majesty of the Chinese landscape.
 captions reveal The Battle of Dingjunshan.

== References ==
 

== External links ==
*  

 
 
 
 
 