Hello Caesar!
{{Infobox film
| name = Hello Caesar!
| image =
| image_size =
| caption =
| director = Reinhold Schünzel
| producer =Reinhold Schünzel
| writer =  S.Z. Sakall    Reinhold Schünzel
| starring = Reinhold Schünzel   Mary Nolan   Wilhelm Diegelmann
| music = 
| cinematography = Ludwig Lippert   
| editing =     
| studio = Reinhold Schünzel Film  UFA
| released = 5 May 1927 
| runtime = 90 minutes
| country = Germany German intertitles
| budget =
| gross =
}}
Hello Caesar! (German:Halloh - Caesar!) is a 1927 German silent comedy film directed by Reinhold Schünzel and starring Schünzel,  Mary Nolan and Wilhelm Diegelmann.  

==Cast==
*  Reinhold Schünzel as Caesar, Artist  
* Mary Nolan 
* Wilhelm Diegelmann as Willard, Varietédirektor  
* Julius Falkenstein as Baron von Glatzenstein 
* Ilka Grüning as Frau Svoboda 
* Paul Kretschmar as Zwillingsbrüder 
* Richard Kretschmar as Zwillingsbrüder 
* Toni Philippi as Rosl, Tochter von Frau Svoboda  

== References ==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

== External links ==
*  

 

 
 
 
 
 
 
 
 

 