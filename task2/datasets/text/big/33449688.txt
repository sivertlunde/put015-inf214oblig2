Assassin (1969 film)
{{Infobox film name           = Assassin image          = 
| film name = {{Film name hangul         =   hanja          =   rr             = Amsalja mr             = Amsalja}} director  Lee Man-hee producer       = Kim Tai-soo writer         = Lee Eun-seong starring       = Jang Dong-he Namkoong Won Park Am music          = Jeon Jong-kun cinematography = Lee Suck-ki editing        = Yu Jae-won distributor    =  released       =   runtime        = 73 minutes country        = South Korea language       = Korean budget         = 
}}
Assassin ( ) is a 1969 South Korean thriller film.

==Plot==
A man is given a task to killed Hwang To-jin, a North Korean spy who turned himself to the South, and penetrates into the South. He meets Hwang To-jin’s daughter and comes to feel skeptical of his behavior. Then he receives an order to hurry the assassination. He finally realizes that he has been foolish and turns himself to the police to help round up the spy network.  

==Cast==
*Jang Dong-he
*Nam Koong-won
*Park Am
*Kim Hea-kyung
*Oh Ji-myung
*Jeon Young-sun
*Choe Bong
*Lee Hae-ryong
*Park Ki-teak
*Kim Ki-bum

==References==
 

==External links==
*  
*  

 
 
 
 
 


 
 