Zoo zéro
{{Infobox film
| name           = Zoo zéro
| image          =
| image size     =
| caption        =
| director       = Alain Fleischer
| producer       = François Barat Pierre Barat
| writer         = Alain Fleischer
| narrator       =
| starring       = Catherine Jourdan Klaus Kinski
| music          =
| cinematography = Bruno Nuytten
| editing        = Eric Pluet
| distributor    =
| released       = 9 May 1979
| runtime        = 101 minutes
| country        = France
| language       = French
| budget         =
| preceded by    =
| followed by    =
}}

Zoo zéro is a 1979 French film directed by Alain Fleischer and starring Klaus Kinski.   

==Cast==
* Catherine Jourdan - Eva / The singer
* Klaus Kinski - Yavé, le directeur du zoo
* Pierre Clémenti - Ivo, le bégue
* Lisette Malidor - Ivy, landrogyne Rufus - Yves, le chauffeur ventriloque
* Piéral - Uwe, limprésario
* Alida Valli - Yvonne, la mère
* Christine Chappey - Yvette
* Anthony Steffen - Evariste, le commissaire
* Jacky Belhassen - Yvon, le bruiteur
* Fabien Belhassen - Yvan, limitateur

==References==
 

==External links==
* 

 
 
 
 

 