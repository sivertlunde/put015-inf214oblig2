The King of Masks
 
{{Infobox film
| name           = The King of Masks
| image          = Kingofmasks.jpg
| caption        = American DVD cover
| director       = Wu Tianming
| writer         = Wei Minglun
| starring       = Zhu Xu Zhou Renying Zhao Zhigang
| music          = Zhao Jiping
| cinematography = Mu Da-Yuan
| distributor    = Hong Kong:  
| released       = June 13, 1999 (USA)
| runtime        = 101 min. (China) 91 min. (USA)
| country        = Peoples Republic of China
| awards         = Mandarin
| budget         = 
}}
 1996 Cinema Chinese film directed by Wu Tianming.

==Synopsis== bian lian. He laments that he has no male heirs to carry on his mysterious and complicated art and trade. At an illegal child market, Wang buys what he believes to be an orphan boy to become his adopted grandson and apprentice.

However, Wang soon learns his new disciple is in fact a girl. As tradition dictates that he cannot pass his art onto a girl, he tries to abandon her, but she stubbornly stays with him. He later calls her "doggie" and has her refer to him as "boss. He then begins to train her to be a flexible contortionist street performer. While looking at his masks, she accidentally sets his humble residence on fire. Out of guilt, she runs away. She is then captured by two men and held in a room with a boy to be sold later. Doggie helps the boy escape and takes him to Wang to have as a grandson.

Then, Wang is falsely accused of kidnapping the boy, a rich familys child, and is thrown in jail.  Doggie goes to one of his friends, a famous performer in the local opera, threatening to kill herself if he or any of his guests, including a local military general, are unable to help Wang. The King of Masks is eventually freed, and he finally accepts the girl as his granddaughter and teaches her his art.

==Reception==
The film won a number of awards at various film festivals around the world. It had a limited American theatrical release in 1999 and earned about one million US dollars. 

===Awards and nominations=== 1995
** Outstanding Co-Production Film 1996
** Best Co-Produced Film
** Best Director — Wu Tianming
* Frankfurt Children and Young Peoples Film Festival, 1996
** Lucas, Childrens Section
* Tokyo International Film Festival, 1996
** Best Actor — Zhu Xu
** Best Director — Wu Tianming 1997
** Audience Award
* Carrousel International du Film, 1997
** Camério for Best Actor — Zhu Xu
** Camério for Best Actress — Zhou Renying
** Camério for Best Film
* Istanbul International Film Festival, 1997
** C.I.C.A.E. Award
** Golden Tulip
* Singapore International Film Festival, 1997
** Silver Screen Award for Best Asian Director — Wu Tianming 1998
** Crystal Simorgh for Best Actor — Zhu Xu 1999
** Childrens Film Award 2000
** Best Foreign Language Motion Picture (nominated)

==External links==
*  
*  
*  
*   at the Chinese Movie Database
*   learning package for school children at Chinese Cinema 07

 
 
 
 
 
 