The Legend of Billie Jean
{{Infobox film 
| name = The Legend of Billie Jean
| image = legend of_billie_jean.jpg
| caption = Theatrical release poster
| writer = Lawrence Konner Mark Rosenthal
| starring = {{Plainlist|
* Helen Slater
* Keith Gordon
* Christian Slater
* Peter Coyote
}} Matthew Robbins
| producer = Rob Cohen
| distributor = TriStar Pictures
| studio = Delphi III Productions The Guber-Peters Company
| released =  
| runtime = 96 minutes
| country = United States
| language = English
| cinematography = Jeffrey L. Kimball
| editing = Cynthia Scheider
| music = Craig Safan
| budget = 
| gross = $3,099,497 (USA)
}} Matthew Robbins.

==Plot== Honda Elite to a local lake to enjoy a day of swimming and relaxation. A group of teenage boys, led by Hubie Pyatt (Barry Tubb), who sexually harass Billie Jean on a regular basis, starts trouble with them, but Binx humiliates Hubie and they get away. Later at the lake, Billie Jean tells Binx about Vermont, and Binx expresses a desire to go there. During their conversation about travelling there, Hubie and his friends arrive, apparently looking to get revenge for Binxs humiliation. They find Binxs scooter and steal it.

Billie Jean, and her friends Putter (Yeardley Smith) and Ophelia (Martha Gehman) go to the police about the scooter. They speak to Detective Ringwald (Peter Coyote) about it, but he doesnt take them seriously. He assumes its just a harmless squabble between youngsters. When Binx goes to get his scooter back on his own, he comes home severely beaten, with his scooter trashed.

Billie Jean, Binx, and Ophelia go to Mr. Pyatts shop to get the money ($608.00) to repair the scooter. While initially appearing helpful and understanding, Mr. Pyatt then propositions Billie Jean with a Pay as you go, earn as you learn plan by which he will have sex with her. He then attempts to rape her.

Meanwhile, Binx has discovered a gun in the empty store and attempts to taunt Mr. Pyatt with it. Mr. Pyatt tells him the gun is unloaded, but Binx accidentally fires it, wounding Mr Pyatt in the shoulder. The group races away from the shop and become fugitives.

By the time Detective Ringwald realizes that he made a mistake in not listening to Billie Jean, the situation is spinning out of control. Throughout it all, Billie Jean wants only the $608 to fix her brothers scooter and an apology from Mr. Pyatt. With help from Lloyd Muldaur (Keith Gordon), the disgruntled teenage son of the district attorney, who voluntarily becomes her "hostage", Billie Jean makes a video of her demands, featuring herself with her long, blond hair chopped into a crew cut as a sign of her rebellion. As media coverage increases, Billie Jean becomes a teen icon – a symbol of youth empowerment and the evidence of the injustices adults are capable of, and young fans follow her every movement. Facing uncertain dangers, both physical and legal, Billie Jean is forced to turn her friends Putter and Ophelia in to the police for their safety. When Ringwald and the police arrive and he demands to know where Billie Jean is, Ophelia proudly and defiantly replies, "Everywhere!"

Mr. Pyatt issues a bounty for her apprehension, and Billie Jean realizes the best plan is to put an end to the extraordinary circumstances and to turn herself in. To avoid attracting too much attention, she and her brother Binx both arrive in disguise. But the disguise is blown, and the consequences descend into a violent riot, which results in Binx getting shot. As Binx is taken away in an ambulance, Billie Jean confronts Mr. Pyatt and gets him to admit his actions that led to him being shot in his store. The onlookers (including Hubie), seeing how Billie Jean was exploited and their indirect involvement in it, destroy all the Billie Jean merchandise and leave in disgust.  At the end of the film Billie Jean and Binx find themselves far up in Vermont seeking some recuperation and a fresh start. Binx, after complaining about the cold, admires a red snowmobile.

==Cast==
* Helen Slater as Billie Jean Davy
* Christian Slater as Binx Davy
* Keith Gordon as Lloyd Muldaur
* Yeardley Smith as Putter
* Barry Tubb as Hubie Pyatt
* Martha Gehman as Ophelia
* Peter Coyote as Detective Larry Ringwald Richard Bradford as Mr. Pyatt
* Dean Stockwell as District Attorney Muldaur
* Mona Lee Fultz as Donna Davy Bobby Jones as Kenny
* John M. Jackson as Kennys Father
* Rodney Rincon as Police Sergeant
* Caroline Williams as Woman in Pickup
* Rudy Young as Man in Pickup
* Bobby Fite as Mini-Mart Boy
* Kim Valentine as Mini-Mart Girl
* Cindy Haag as Girl in Camaro
* Janet Smalley as Putters Mother
* Charles Redd as News Announcer
* Joshua Butts as Tape Delivery Boy
* Ray Hanna as Jimmy J. Judge
* Kit Sullivan as Deputy Sheriff
* B.J. Thompson as Interview Boy
* Celia Newman as Interview Girl
* Tony Slowik as Interview Teen
* Gary Small as Interview Short

==Production==
 
 
 Sunrise Mall and several locations along South Padre Island Drive. 
* The original title of the film was Fair is Fair.   

==Soundtrack==
 
 score for Rebel Yell" climbed up to number six on the UK Singles Chart in October 1985 after its first unsuccessful release in 1984.

* "Invincible (Pat Benatar song)|Invincible" (Theme from "The Legend of Billie Jean") – Pat Benatar
* "Closing In" – Mark Safan Boys in Town" – Divinyls
* "Heart Telegraph" – Divinyls
* "Rebel Yell" – Billy Idol
* "It’s My Life" – Wendy O. Williams
* "Time to Explain" – Bruce Witkin & The Kids
* "Self Defense" – Chas Sanford

==Reception==
The film holds a 44% "Rotten" score on the film review aggregator website Rotten Tomatoes, based on 9 reviews, though it currently lacks of consensus summary.  Janet Maslin of The New York Times said that the film is "competently made, sometimes attractively acted (particularly by Peter Coyote)...and bankrupt beyond belief. Its hard to imagine that even the film makers, let alone audiences, can believe in a sweet, selfless heroine who just cant help becoming a superstar." 

==Home media==
The film was released on home video on VHS in 1985.

In 2009, Sony Pictures Home Entertainment released in Europe a Spanish-titled DVD La Leyenda de Billie Jean, with 4:3 open matte image, but without any bonus material. A remastered NTSC DVD including commentary by Helen Slater and Yeardley Smith was released on November 1, 2011, via their manufactured on demand service. 

Mill Creek Entertainment released a retail version of the DVD, along with a Blu-ray edition on July 22, 2014.   

==See also==
* List of American films of 1985

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 