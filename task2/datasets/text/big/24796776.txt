Backstage (2005 film)
 
 
{{Infobox film
 | name = Backstage
 | image size     =
 | caption = 
 | director = Emmanuelle Bercot
 | producer = Caroline Benjo
 | writer = Emmanuelle Bercot  Jérôme Tonnerre
 | narrator = 
 | starring = Emmanuelle Seigner Noémie Lvovsky
 | music = Laurent Marimbert
 | cinematography = Agnès Godard
 | editing = Julien Leloup
 | distributor = Haut et Court
 | released = 16 November 2005
 | runtime = 155 minutes
 | country = France
 | language = French
 | budget = 
 | preceded by    =
 | followed by    =
 | image =  Backstage (2005 film) Video Cover.png
}}
Backstage is a French film directed by Emmanuelle Bercot, released in 2005. It was screened in the Official Selection (Out of Competition) category of the 62nd Venice International Film Festival.

==Synopsis==
Lucie, who is 17 years old, is an ordinary teenager  and a fan of the popular singer, Lauren Marks, played by Emmanuelle Seigner. One day, Lucie’s destiny leads her to enter into the life of her idol.

==Cast==
*Lauren Marks : Emmanuelle Seigner
*Lucie : Isild Le Besco
*Juliette : Noémie Lvovsky
*Seymour : Valéry Zeitoun
*Daniel : Samuel Benchetrit
*Marie-Line : Edith Le Merdy
*Jean-Claude : Jean-Paul Walle Wa Wana
*Nanou : Mar Sodupe
*A Director : Eric Lartigau
*The chambermaid : Lise Lamétrie
*Lauren’s father : Claude Duneton
*Gisèle : Joëlle Miquel

==External links==
* 
* 

 

 
 
 
 
 
 

 