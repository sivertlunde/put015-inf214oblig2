Bride of the Wind
{{Infobox film
| name           = Bride of the Wind
| image          = Bride of the wind.jpg
| image size     =
| caption        = Film poster
| director       = Bruce Beresford
| producer       = Margit Bimler Gerald Green Frank Hübner Evzen Kolar Lawrence Levy
| writer         = Marilyn Levy
| starring       = Sarah Wynter Jonathan Pryce Vincent Perez Simon Verhoeven
| music          = Stephen Endelman Peter James
| editing        = Timothy Wellburn Paramount Classics
| released       = June 8, 2001 (U.S.)
| runtime        = 99 minutes
| country        = United Kingdom Germany Austria
| language       = English
| budget         =
| gross          = $419,414
| followed by    =
}} period drama Academy Award-nominee Bruce Beresford and written by first-time screenwriter Marilyn Levy. Loosely based on the life of Alma Mahler, Bride of the Wind recounts Almas marriage to famed composer Gustav Mahler and her romantic exploits. The title of the film alludes to a painting by Oskar Kokoschka named "Die Windsbraut", literally meaning "The Bride of the Wind", though often translated as "The Tempest". The artist dedicated this painting to Alma Mahler.

The film met a hostile reception from most critics and did poorly at the box office. 

==Poster Art== Gustav Klimts stylistic elements.

==Cast==
*Sarah Wynter as Alma Mahler
*Jonathan Pryce as Gustav Mahler
*Vincent Perez as Oskar Kokoschka
*Simon Verhoeven as Walter Gropius
*Gregor Seberg as Franz Werfel
*August Schmölzer as Gustav Klimt
*Johannes Silberschneider as Alexander von Zemlinsky
*Hans Steunzer as Richard Strauss
*Robert Herzl as Arnold Schoenberg

==References==
 

==External links==
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 