Shake Hands with the Devil: The Journey of Roméo Dallaire
 
{{Infobox film
| name           = Shake Hands with the Devil: The Journey of Roméo Dallaire
| image          = ShakeHandsPoster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Peter Raymont
| producer       = Executive Producers: Tom Neff Producers: Peter Raymont Lindalee Tracey
| story          = Roméo Dallaire
| narrator       = 
| starring       = Roméo Dallaire
| music          = Mark Korven
| cinematography = John Westheuser
| editing        = Michèle Hozer
| distributor    = Canadian Broadcasting Corporation California Newsreel (United States)
| released       =   
| runtime        = 91 minutes (English) 56 minutes (French)
| country        = Canada
| language       = English
| budget         = 
| gross          = $68,249 (domestic)  
|}}
Shake Hands with the Devil: The Journey of Roméo Dallaire is a 2004   ( . 

==Synopsis==
Between April and June 1994, an estimated 800,000 Rwandans were killed in 100 days. Most of the dead were Tutsis by the hands of the Hutus. The genocide began when Rwandan president Juvenal Habyarimanas plane was shot down above Kigali airport on April 6, 1994.

Canadian Armed Forces General Roméo Dallaire was put in charge of a United Nations peacekeeping force during this 1994 genocide. His proposal called for 5,000 soldiers to permit orderly elections and the return of the refugees. The soldiers were never supplied and the killing began.
 Mark Doyle, among others.

==Interviews==
* General Roméo Dallaire
* Stephen Lewis Mark Doyle
* Gerald Caplan
* Paul Kagame
* Major Brent Beardsley
* Bonaventure Niyibizi
* Rosette Musabe

==Distribution== CBC On Demand.

A 91-minute and 56-minute English Versions; 56-minute French Version was released on DVD, Region 1 (North America).

The film has its American television premiere on  .

==Critical reception==
When the film opened in New York City, film critic Stephen Holden gave the film a positive review, and wrote, "The film...is a respectful portrait of General Dallaire, now retired, who comes across as a thoughtful, resolute but profoundly shaken man, more philosopher than warrior...If Terry Georges wrenching film Hotel Rwanda and Raoul Pecks HBO movie Sometimes in April have already put a tragic human face on a catastrophe that the American mass media barely acknowledged while it was happening, Shake Hands With the Devil ratifies their horrifying visions. General Dallaires descriptions of the sights, sounds, and smells of human butchery, as well as the movies images of piles of dead bodies, severed limbs and rooms of skulls, are grimmer than anything seen in those films...Beyond apportioning blame, Shake Hands With the Devil acknowledges that the capacity for evil is a human component. Under certain conditions, entire populations can lose their humanity and go berserk. With madness all around him, General Dallaire maintained his humanity and (just barely) his sanity."  .  The New York Times, film review, "	
Ten Years Later, Back at the Killing Fields to Heal the Spirit",  May 18, 2005. Last accessed: April 16, 2008. 

Jonathan Curiel, staff writer for the San Francisco Chronicle, interviewed Dallaire when the film was released, and wrote, "Raymonts movie, Dallaire says, is another way to raise awareness about Rwandas legacy. But whether he likes it or not, Shake Hands With the Devil is also a chance to peer into Dallaires inner thoughts—to get to know a man who says hes not a hero but a humanist. If history is best understood through the decisions of individual men and women, then Raymonts film lets audiences revisit the siege of Rwanda through the eyes of a retired officer (and newly appointed Canadian senator) who tried to prevent hell on earth." 

==Awards==
Wins
* Sundance Film Festival: Audience Award, World Cinema - Documentary, Peter Raymont; 2005.
* Directors Guild of Canada: DGC Team Award, Outstanding Team Achievement in a Documentary, Peter Raymont (director); 2005.
* Gemini Awards: Gemini, Best Picture Editing in a Documentary Program or Series, Michèle Hozer; 2005.
* Banff Television Festival: Banff Rockie Award, Best Feature Length Documentary, Best Canadian Program; 2005.
* Philadelphia Film Festival: Jury Award, Best Documentary, Peter Raymont; 2005.
*  , Best Documentary,  , Tom Neff and Jimmy Holcomb; 2006. 

==References==
 

==External links==
*  
*    at DVD Beaver (includes images)
*   at California Newsreel
*   film clip at YouTube Documentary Channel)

 
 
 
 
 
 
 
 
 
 