Personal Services
{{Infobox film
| name = Personal Services
| image = Personal services poster.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Terry Jones
| producer = Tim Bevan
| writer = David Leland
| starring = Julie Walters Shirley Stelfox Alec McCowen Danny Schiller Tim Woodward
| music = John Du Prez
| cinematography = Roger Deakins
| editing = George Akers Zenith Entertainment
| distributor = Vestron Pictures
| released =  
| runtime = 105 minutes
| country = United Kingdom
| language = English
| budget = 
| gross = $1,744,164    24 Sept. 1995: 9 . The Sunday Times Digital Archive.] Web. 29 Mar. 2014. 
}}
Personal Services is a 1987 British comedy film directed by Terry Jones and written by David Leland. It is the story of the rise of a madam of a suburban brothel which caters to older men. The story is inspired by the real experiences of Cynthia Payne, the legendary "House of Cyn" madam.

==Plot==
Christine Painter (Julie Walters) is a sexually naive waitress and single-mother who pays for her teenage sons tuition by renting flats to local tarts. She later becomes a call girl herself - joining her friend Shirley (Shirley Stelfox) and their "maid" Dolly (Danny Schiller) - and specializes in fetish/kink roleplay with older gentlemen. With her new family of friends and admirers, she becomes a huge sensation leading to wealth and infamy in the tabloids. The brothel business also brings out Christines self-confidence, and leads to repairing a previously strained relationship with her father.

==Cast==
* Julie Walters as Christine Painter
* Shirley Stelfox as Shirley
* Alec McCowen as Wing Commander Morten
* Danny Schiller as Dolly
* Tim Woodward as Timms
* Victoria Hardcastle as Rose Dave Atkins as Sydney
* Ewan Hooper as Edward
* Alan Bowyer as David Painter
* Antony Carrick as Edgar
* Beverley Foster as Elizabeth
* Leon Lissek as Mr. Popozogolou
* Peter Cellier as Mr. Marples
* Benjamin Whitrow as Mr. Marsden Stephen Lewis as Mr. Dunkley
==Ban==
The film was banned in the Republic of Ireland upon theatrical release. At the time, there were four films that were banned in Ireland, and Jones had directed three of them (Personal Services, Monty Pythons Life of Brian, and Monty Pythons The Meaning of Life).

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 