Tender Mercies
{{Infobox film
| name           = Tender Mercies
| image          = Tender_mercies.jpg
| caption        = Theatrical release poster
| alt            = A movie poster with a large picture of a bearded man wearing a cowboy hat, suspended in the background of a photo of a much smaller scaled woman and young boy talking in a field. A tagline beside the man reads "Robert Duvall is Mac Sledge, down and out country singer. His struggle for fame was over. His fight for respect was just beginning." At the bottom, the words "Tender Mercies" appear, along with much smaller credits text. The top of the poster includes additional promotional text.
| director       = Bruce Beresford
| producer       = Philip S. Hobel
| writer         = Horton Foote
| starring = {{plainlist|
* Robert Duvall
* Tess Harper
* Betty Buckley
* Wilford Brimley
* Ellen Barkin
}}
| music          = George Dreyfus
| cinematography = Russell Boyd William Anderson
| studio         =   Universal Pictures
| released       =  (limited)
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $4.5 million
| gross          = $8.44 million
}} alcoholic country music singer who seeks to turn his life around through his relationship with a young widow and her son in rural Texas. Robert Duvall plays the role of Mac; the supporting cast includes Tess Harper, Betty Buckley, Wilford Brimley, Ellen Barkin and Allan Hubbard.

Financed by EMI Films, Tender Mercies was shot largely in Waxahachie, Texas. The script was rejected by several American directors before the Australian Beresford accepted it. Duvall, who sang his own songs in the film, drove more than   throughout the state, tape recording local accents and playing in country music bands to prepare for the role. He and Beresford repeatedly clashed during production, at one point prompting the director to walk off the set and reportedly consider quitting the film.
 Universal Pictures made little effort to publicize Tender Mercies, which Duvall attributed to the studios lack of understanding of country music.
 Best Picture. Best Original Best Actor for Duvall, his first and only win as of  .

==Plot==
Mac Sledge (Robert Duvall), a washed up, alcoholic country singer, awakens at a run-down Texas roadside motel and gas station after a night of heavy drinking. He meets the owner, a young widow named Rosa Lee (Tess Harper), and offers to work in exchange for a room. Rosa Lee, whose husband was killed in the Vietnam War, is raising her young son, Sonny (Allan Hubbard), on her own. She agrees to let Mac stay under the condition that he does not drink while working. The two begin to develop feelings for one another, mostly during quiet evenings sitting alone and sharing bits of their life stories.

Mac resolves to give up alcohol and start his life anew. After some time passes, he and Rosa Lee wed. They start attending a Baptist church on a regular basis. One day, a newspaper reporter visits the motel and asks Mac whether he has stopped recording music and chosen an anonymous life. When Mac refuses to answer, the reporter explains he is writing a story about Mac and has interviewed his ex-wife, Dixie Scott (Betty Buckley), a country music star who is performing nearby.

After the story is printed, the neighborhood learns of Macs past, and members of a local country–western band visit him to show their respect. Although he greets them politely, Mac remains reluctant to open up about his past. Later, he secretly attends Dixies concert. She passionately sings several songs that Mac wrote years earlier, and he leaves in the middle of the performance. Backstage, he talks to Dixies manager, his old friend Harry (Wilford Brimley). Mac gives him a copy of a new song he has written and asks him to show it to Dixie. Mac tries to talk to Dixie, but she becomes angry upon seeing him and warns him to stay away from their 18-year-old daughter, Sue Anne (Ellen Barkin).
 baptized together in Rosa Lees church.
 On the Wings of a Dove," which references a dove from the Lord saving Noah and descending at Jesus baptism.
 
Boys at school bully Sonny about his dead father, and he and Mac grow closer. The members of the local country band ask Mac permission to perform one of his songs, and he agrees. Mac begins performing with them and they make plans to record together. His newfound happiness is interrupted 
when Sue Anne dies in a car accident. Mac attends his daughters funeral at Dixies lavish home in Nashville and comforts her when she breaks down.

Back home, Mac keeps quiet about his emotional pain, although he wonders aloud to Rosa Lee why his once sorry existence has been given meaning and, on the other hand, his daughter died. Throughout his mourning, Mac continues his new life with Rosa Lee and Sonny. In the final scene, Sonny finds a football Mac has left him as a gift. Mac watches the hotel from a field across the road and sings "On the Wings of a Dove" to himself. Sonny thanks him for the football and the two play catch together in the field.

==Production==

===Writing=== 1966 film of the same name. Following what Foote saw as a far more successful adaption of his 1968 play Tomorrow in 1972, his interest in filmmaking was rekindled, under the condition that he maintain some degree of control over the final product. Foote said of this stage in his career, "I learned that film really should be like theatre in the sense that in theatre, the writer is, of course, very dominant&nbsp;... If we dont like something, we can speak our minds.&nbsp;... It is always a collaborative effort.&nbsp;... But in Hollywood it wasnt so. A writer there has in his contract that you are a writer for hire, which means that you write a script, then it belongs to them."  .  This renewed interest in cinema prompted Foote to write Tender Mercies, his first work written specifically for the screen.    In the view of biographer George Terry Barr, the script reflected "Footes determination to battle a Hollywood system that generally refuses to make such personal films." 

The story was inspired partially by Footes nephew, who struggled to succeed in the country music business. Foote was initially interested in writing a film based on his nephews efforts to organize a band, which he saw as paralleling his own youthful attempts to find work as an actor. During his research, however, he met an experienced musician who had offered to help his nephews band, and Foote found himself growing more interested in a story about him, rather than the band itself.  .  Foote said, "This older man had been through it all. As I thought about a storyline, I got very interested in that type of character."  The moment in the film where a woman asks, "Were you really Mac Sledge?" and he responds, "Yes maam, I guess I was," was based on an exchange that Foote overheard between a washed-up star and a fan. Foote said the entire film pivots on that statement, which he believed spoke volumes about Macs personality and former status. 

Foote based Sledges victory over alcoholism on his observations of theater people struggling with the problem. He sought to avoid a melodramatic slant in telling that aspect of the story.   Foote described his protagonist as "a very hurt, damaged man&nbsp;... silence was his weapon".    He chose the title Tender Mercies, from the Book of Psalms, for its relation to the Rosa Lee character, who he said seeks only "certain moments of gentleness or respite,   grandness or largeness".  Foote sought to portray each character as realistic and flawed, but not unsympathetic.  Although the script conveyed a strong spiritual message with religious undertones, Foote felt it was important to balance those religious elements with a focus on the practical challenges of everyday life. 
 Hollywood conventions.  . 

===Development=== To Kill a Mockingbird (1962), which Foote adapted from the Harper Lee novel, was involved in Tender Mercies as an actor and co-producer from its earliest stages. He said the script appealed to him because of the basic values it underlined and because the themes were universal even though the story was local. Duvall felt it portrayed people from the central region of the United States without parodying them, as he said many Hollywood films tend to do.    Duvalls early involvement led to rumors that he had requested Foote write the script for him, something that both men denied. 
 Breaker Morant. Philip Hobel said, "What we saw in Breaker Morant is what we like as filmmakers ourselves—an attention to the environment, a straightforward presentation; its almost a documentary approach."   
 bush country, and the Texans he met in the isolated areas reminded him of residents of the Outback. He met Foote and discussed the script with him. The screenwriter, who gave Beresford tours of small Texas towns, felt the directors Australian background made him sensitive to the storys rural characters and would help him achieve the sought-for authenticity.  Beresford agreed to direct and was hired after receiving final approval from Duvall (the actor had a clause in his contract allowing him such approval, the first time he had this power on a film).   

 
The film was given a budget of $4.5 million ($  in   dollars), modest by Hollywood standards at the time.  Philip Hobel said it took about a year to secure the financing from EMI Films, whose major 1981 release,   highway. Mary Ann Hobel said the owner, when approached about its availability, immediately handed over the keys: "We said, Dont you want a contract, something in writing? And he said, We dont do things that way here.    
 William Anderson, who had worked on all of the directors previous features, as Film editing|editor.  .  He selected Elizabeth McBride as costume designer. It was her first time in the position on a feature film, and she went on to build a reputation for costuming Texan and other Southern characters. 

===Casting===
 
Duvall had always wanted to play a country singer, and Foote was rumored to have written the role of Mac Sledge specifically for him. Foote denied the claim, claiming he found it too constraining to write roles for specific actors, although he did hope Duvall would be cast in the part. Tender Mercies became a very important personal project for Duvall, who contributed a significant number of ideas for his character.   In preparing for the role, he spent weeks roaming around Texas, speaking to strangers to find the right accent and mannerisms. He also joined a small country band and continued singing with them every free weekend while the film was being shot.    In total, Duvall drove about   to research the part, often asking people to speak into his tape recorder so he could practice their inflections and other vocal habits.  Upon finding one man with the exact accent he wanted, Duvall had him recite the entire script into the recorder.   

Tess Harper was performing on stage in Texas when she attended a casting call for a minor role in the film. Beresford was so impressed with her that he cast her in the lead. He later said that the actresses he had seen before her demonstrated a sophistication and worldliness inappropriate for the part, while she brought a kind of rural quality without coming across as simple or foolish. Beresford said of Harper, "She walked into the room and even before she spoke, I thought, Thats the girl to play the lead.   Harper said she knew she won the role when Beresford appeared on her doorstep with a bottle of champagne in each hand.  Tender Mercies was Harpers feature film debut, and she was so excited about the role she bit her script to make sure it was real.  When filming ended, Duvall gave her a blue cowgirl shirt as a gift with a card that read, "You really were Rosa Lee".  . 

Beresford visited several schools and auditioned many children for the role of Sonny before he came across Allan Hubbard in Paris, Texas|Paris, Texas. Beresford said Hubbard, like Harper, was chosen based on a simple, rural quality he possessed.  The boy was able to relate easily to the character because, like Sonny, his father died at an early age; later, some media reports falsely claimed that his father was killed during the Vietnam War, just as Sonnys was in the films backstory.  None of the filmmakers knew Hubbards father had died until after filming began.  Duvall developed a strong, trusting relationship with Hubbard, which Foote felt improved the duos on-screen chemistry.  Hubbard would often play guitar with Duvall during breaks from filming. 
 Fort Worth, Grapevine Opry; when her concert scenes were filmed there, her whole family participated as Extra (actor)|extras.  Duvall said he thought Buckley perfectly conveyed the underlying frustration of a country singer and "brought a real zing to   part." 

Ellen Barkin was cast after impressing Beresford during a New York audition. At the time, she had appeared only in television movies; Diner (film)|Diner, her feature film debut, was not yet in theaters.  When filming on Diner wrapped, Barkin joked to her agent about future roles, "No more troubled teenagers, unless the movie is with Robert De Niro, Robert Duvall or Robert Redford."  Duvall said of Barkin, "She brings a real credibility for that part, plus she was young and attractive and had a certain sense of edge, a danger for her that was good for that part."  Some media outlets reported that Duvall and Barkin were involved romantically for a brief time during filming. 

Wilford Brimley was cast at the urging of his good friend Duvall, who was not getting along well with Beresford and wanted "somebody down here thats on my side, somebody that I can relate to".  Beresford felt Brimley was too old for the part, but eventually agreed to the casting. 

===Filming=== Ellis County, Texas.]] Ellis County in north central Texas.  Beresford largely avoided the Victorian architecture and other picturesque elements of Waxahachie and instead focused on relatively barren locations more characteristic of West Texas. The town portrayed in the film is never identified by name. Foote said when he wrote the script he did not have the same isolated and lonely vision for the setting Beresford did, but he felt the atmosphere the director captured served the story well.  

Principal photography took place between November 2 and December 23, 1981.  The plants used in the gardening scenes were brought inside at night to keep them from freezing.  .  Due to the tight schedule, the cast and crew worked seven days a week with very long hours each day. Although the Australian filmmakers and the crew, who were mostly from Dallas, got along very well both on and off the set,  Beresford and Duvall were at odds during the production. Beresford, in his usual approach, meticulously planned each scene, and Duvall, who preferred a free-form give-and-take on set, felt restricted by the directors methods. Although Duvall regularly acknowledged his talent as a director, he said of Beresford, "He has this dictatorial way of doing things with me that just doesnt cut it. Man, I have to have my freedom."  Although he had no problem with Duvalls acting methodology, the actors temperament infuriated Beresford. While filming one scene with Harper and Barkin, he became so frustrated during a phone conversation with Duvall that he said, "Well if you want to direct the film, go right ahead," and walked off the set.  Beresford flew to New York and reportedly was ready to quit, until Duvall flew out to speak with him. After further arguments, the two made amends and returned to work on the film.  . 

Beresford also clashed on set with Brimley. On the very first day of filming, he asked the actor to "pick up the pace", prompting Brimley to reply, "Hey, I didnt know anybody dropped it."  On another occasion, when Beresford tried to advise Brimley on how Harry would behave, Duvall recalled Brimley responding, "Now look, let me tell you something, Im Harry. Harrys not over there, Harrys not over here. Until you fire me or get another actor, Im Harry, and whatever I do is fine cause Im Harry."  Duvall said he believed the on-set wrangling resulted in a combination of the directors and actors visions and ultimately improved the picture. Likewise, Beresford said he did not feel the fights negatively affected the film because he and Duvall never disagreed on the interpretation of the Mac Sledge character. 

Harper described the extent to which Duvall inhabited his character: "Someone once said to me, Well, hows Robert Duvall? and I said, I dont know Robert Duvall. I know Mac Sledge very well.   Beresford, too, said the transformation was so believable that he could feel his skin crawling up the back of his neck the first day of filming.  Duvall made an effort to help Harper, who was making her film debut. While preparing to shoot a scene in which Mac and Rosa Lee fight, he yelled at a make-up artist in front of Harper to make her angry and fuel her performance; he apologized to the make-up artist after the scene was shot. 

Cinematographer Russell Boyd largely utilized   and long shot so it could flow uninterrupted, with the lonely Texas landscape captured in the background. When studio executives received the footage, they contacted Beresford and requested close-up shots be intercut, but he insisted on keeping the long take intact. Duvall said he felt the scene underscored Macs stoicism in the face of tragedy and loss.  

===Music===
{{Listen filename     = RobertDuvallTenderMercies.ogg title        = "If Youll Hold the Ladder (Ill Climb to the Top)", performed by Robert Duvall in Tender Mercies description = Robert Duvall insisted on singing his own songs in Tender Mercies. type         = music pos          = right header       = filename2 = BettyBuckleyTenderMercies.ogg title2 = "Over You", performed by Betty Buckley in Tender Mercies description2  Best Song. type2 = music
}} Bob Ferguson country song featured in the film.  Duvall collaborated with Beresford in deciding on the unusual staging of the emotional scene in which Mac sings it after reflecting on the reunion with his daughter. The song is performed with Mac looking out a window with his back to the camera, his face unseen. Horton Foote thought the choice made the scene more moving and called it "an extraordinary moment" in the film.   Duvall wrote two of Macs other songs, "Fools Waltz" and "Ive Decided to Leave Here Forever."  Several leading country singers, including Willie Nelson, George Jones and Merle Haggard, were believed to have inspired Mac and Duvalls portrayal of him, but Duvall insisted the character was not based on anyone in particular.   Another country star, Waylon Jennings, complimented his performance, saying he had "done the impossible." 
 Austin Roberts Bobby Hart, Academy Award.  Although Buckley performed it in the film, country singer Lane Brody was chosen to record it for radio release,  and Mac Davis later sang it at the 1984 Academy Awards ceremony.  Other songs in the film include "It Hurts to Face Reality" by Lefty Frizzell, "If Youll Hold the Ladder (Ill Climb to the Top)" by Buzz Rabin and Sara Busby, "The Best Bedroom in Town" and "Champagne Ladies & Barroom Babies" by Charlie Craig, "Im Drinkin Canada Dry" by Johnny Cymbal and Austin Roberts, and "You Are What Love Means To Me" by Craig Bickhardt.  

==Themes and interpretations==

===Love and family===
 
Mac Sledge finds redemption largely through his relationship and eventual marriage with Rosa Lee.  .   This is in keeping with the motif of fidelity common in the works of Foote, inspired, said the writer, by his marriage to Lillian Vallish Foote. He told   used by Alcoholics Anonymous. Both the film and the support groups program advocate the idea of hitting rock-bottom, making a decision to stop drinking, dealing with the past and adopting a spiritual way of life. 

Tender Mercies also emphasizes the father–child theme common in the works of Foote, a theme that operates on both transcendent and temporal levels. Mac is reunited not only with his spiritual father through his conversion to Christianity, but also with his biological daughter, Sue Anne, when she pays him a surprise visit. Scholar Rebecca Luttrell Briley suggests that although Mac begins to plant new roots with Rosa Lee and Sonny in earlier scenes, they are not enough to fully satisfy his desire for redemption, as he is nearly driven to leave the family and return to his alcoholic ways. According to Briley, Sue Annes visit prompts Mac to realize that reconciliation with her and a reformation of their father–daughter relationship is the ingredient that had been lacking in his quest for redemption. This is further demonstrated by Macs singing "On the Wings of a Dove" to himself after their meeting;  .  the lyrics describe God baptizing his son Jesus, which connects Sledges spiritual reconciliation with the divine to the earthly reconciliation with his own child.  However, the death of Sue Anne also demonstrates that, according to Briley, "all relationships cannot be mended, some by choice and some by chance, and the poignancy of missed opportunities between fathers and their children on this earth is underlined in this scene."  . 

The relationship between Mac and Sonny, whose name derives from "son", is central to the films exploration of the father–child theme. Sonny tries to conjure an image of his biological father, whom he never had the chance to know, through old photographs, his mothers memories and visits to his fathers grave. Sonny finds a father figure in Mac—when another young boy asks Sonny if he likes Mac more than his real father, Sonny says that he does, because he never knew the other man; Briley says that this "emphasizes the distinction between companionship and blood relationship Foote has pointed out before."  The final scene, in which Mac and Sonny play catch with a football Mac bought him as a gift, symbolizes the fact that although Mac has lost the chance to reconcile with his daughter, he now has a second chance at establishing a father–child relationship with Sonny.   The father–child theme also plays out through Macs relationship with the young band members, who say that he has been an inspiration to them, playing a paternal role in their lives before they even met him. Sledge eventually teams up with the musicians, offering them fatherly counsel in a much more direct way.  . 

===Religion=== baptized for the first time, along with Sonny. During a church scene, he also sings the hymn "Jesus, Savior, Pilot Me", which serves as a symbol for his new direction in life.  After they are baptized, Sonny asks Mac whether he feels any different, to which Mac responds, "Not yet." According to scholars, this response indicates Macs belief that his reunion with God will lead to meaningful changes in his life.   .  It is after this moment, Briley points out, that Mac is able to forge other relationships, such as those with his young bandmates, and "develop his own potential for success as a man."  .  Briley also proposes that Macs response — "Yes, maam, I guess I was" — to a fan who asks if he was really Mac Sledge suggests that he has washed away his old self through baptism. 
 Romans 12, in which Paul the Apostle appeals to Christians to live out their lives in service to others "through the mercies of God".  .  Many of the elements of Macs redemption, conversion to Christianity and budding relationship with Rosa Lee occur off-camera, including their wedding. Jewett writes, "This is perfectly congruent with the theme of faith in the hidden mercies of God, the secret plot of the life of faith in Romans.&nbsp;... It is a matter of faith, elusive and intangible."  Jewett compares Macs story to that of Abraham, because "just like Sledges story,   centers on the provision of a future through the tender mercies of God".  As told in Romans 4, Abraham and his wife Sarah are too old to produce a son, but Abraham develops the faith that God will provide them an heir, which is exactly what occurs, though—as Paul describes—Abraham did nothing practical to guarantee or deserve such a miracle. Jewett describes Mac as similarly undeserving of redemption, based on his selfish and abusive past, typified by his condition in his first encounter with Rosa Lee: in a drunken stupor following a motel room fight. She takes him in and eventually falls in love with him, despite his having done nothing to deserve her care or his redemption: "It is an undeserved grace, a gift of providence from a simple woman who continues to pray for him and to be grateful for him." 
 why evil exists that is commonly faced by Christians.  .   .  Scholar Richard Leonard writes, "For all believers, the meaning of suffering is the universal question.&nbsp;... No answer is completely satisfying, least of all the idea that God sends bad events to teach us something."  Following the death of his daughter, Mac moves forward with uncertainty as the film ends. Jewett writes of this conclusion, "The message of this film is that we have no final assurances, any more than Abraham did. But we can respond in faith to the tender mercies we have received." 

The exact words of Rosa Lee, comforting Mac in a scene where he is frustrated, are, "I love you, you know?  And every night when I say my prayers and I thank the Lord for His blessings and His tender mercies to me, you and Sonny head the list."  In a later scene, in about the middle of the film, Rosa Lee is alone at bedtime praying and she recites the words of Psalm 25:4-6, KJV, "Shew me thy ways, O LORD; teach me thy paths. Lead me in thy truth, and teach me: for thou art the God of my salvation; on thee do I wait all the day."  At this point, Macs truck pulls into the driveway and interrupts her prayer, as she gets up to greet Mac.  The following verse in the text of Psalm 25, verse 6 (which presumably Rosa Lee would have also prayed had she not been interrupted) reads, "Remember, O LORD, thy tender mercies and thy lovingkindnesses; for they have been ever of old."  It appears that Rosa Lee regularly recited this Psalm at bedtime.

===Death and resurrection===
 
Mac experiences his spiritual resurrection even as he wrestles with death, in both the past—Sonnys father in the Vietnam War—and present—his own daughter in a car accident.  The latter threatens to derail Macs new life, captured in the moment when he learns of it and  turns off the radio that is playing his new song.  Leonard writes of this resurrection, "Depression hangs like a pall over Tender Mercies   what makes this film inspiring is that it is also about the joy of being found.&nbsp;... Mac finds the way, the truth, and the life he wants."  In a climactic scene, Mac tells Rosa Lee that he was once nearly killed in a car crash himself, which forces him to address the question of why he was allowed to live while others have died. Jewett writes of this scene, "Mac Sledge cant trust happiness because it remains inexplicable. But he does trust the tender mercies that mysteriously led him from death to life."  . 

Mac is portrayed as near death at the beginning of the film, having woken up in a drunken stupor in a boundless, empty flatland with nothing in his possession, a shot that scholar Roy M. Anker said "pointedly reflects the condition of his own soul".  The dialogue in other scenes suggests the threat of mortality, including a moment when Mac has trouble singing due to his bad voice and says, "Dont feel sorry for me, Rosa Lee, Im not dead yet."  In several lasting shots, the vast sky dwarfs Mac, Rosa Lee and Sonny, starkly symbolizing their isolation, as well as the fragility of human existence.  The fact that Mac sustains his newfound life with Rosa Lee and Sonny after his daughters death, rather than reverting to his old pattern of alcoholism and abuse, is consistent with a recurring theme in Footes works of characters overcoming tragedy and finding in it an opportunity for growth and maturation. 

==Release==

===Distribution===
Philip and Mary Ann Hobel spent a long time seeking a distributor for Tender Mercies without any success. Duvall, who began to doubt the film would be widely released, was unable to help the Hobels because he was busy trying to find a distributor for Angelo My Love, a film he had written, directed and produced. Eventually, Universal Pictures agreed to distribute Tender Mercies.  .  Test screenings for the film were held, which Beresford described as the most unusual he had ever experienced. The director said that the preview audiences appeared to be very engaged with the picture, to the point the theaters were so silent, "if you flicked a piece of paper on the floor, you could hear it fall." However, the post-screening feedback was, in Beresfords words, "absolutely disastrous."  As a result, Universal executives lost faith in the film and made little effort to promote it.   .  Foote said of the studio, "I dont know that they disliked the film, I just think they thought it was inconsequential and of no consequence at all. I guess they thought it would just get lost in the shuffle."  Others in the film industry were equally dismissive; one Paramount Pictures representative described the picture as "like watching paint dry". 

===Festivals and theatrical run===
Tender Mercies was released on March 4, 1983,    in only three theaters: one in New York City, one in Los Angeles, and one in Chicago. New York Times critic Vincent Canby observed that it was released during "the time of year when distributors usually get rid of all of those movies they dont think are worth releasing in the prime moviegoing times of Christmas and the midsummer months".  The simultaneous release of Angelo My Love led to some more publicity for Duvall himself, but was of no help to Tender Mercies.  Duvall also believed that Universals lack of familiarity and comfort with southern culture and the country music genre further reduced their faith in the film. When country star Willie Nelson offered to help publicize it, a studio executive told Duvall she did not understand how the singer could contribute to the promotion, which Duvall said was indicative of the studios failure to understand both the film and the country music genre.  

Tender Mercies was shown in competition at the 1983 Cannes Film Festival,    where it was described as a relatively optimistic alternative to darker, more violent entries like One Deadly Summer, Moon in the Gutter and Merry Christmas, Mr. Lawrence.  It was also shown at the 1983 International Film Festival of India in New Delhi.    A jury headed by director Lindsay Anderson determined that none of the films in contention, including Tender Mercies, were good enough to win the Golden Peacock, the festivals top prize. Film critic Jugu Abraham said the jurys standards were higher than those of the Academy Awards, and  that Tender Mercies  lack of success at the festival was a "clear example of what is good cinema for some, not being so good for others". 

===Home media===
Following its brief theatrical run, Universal Studios quickly sold the films rights to cable companies, allowing Tender Mercies to be shown on television. When the film unexpectedly received five Academy Award nominations nearly a year after its original release, the studio attempted to redistribute the film to theaters; however, the cable companies began televising the film about a week before the Oscar ceremony, which essentially halted any attempts at a theatrical rerelease.  When the film first played on HBO in March 1984, it surpassed the three major networks in ratings for homes with cable televisions.  Tender Mercies was released on VHS some time later, and was first released on DVD on June 22, 1999. 

==Reception==

===Box office===
Tender Mercies was not considered a box office success.   .     In its first three days, March 4–6, the film grossed $46,977 from exclusive engagements at the Tower East Theater in New York ($21,183), the Fine Arts Theater in Los Angeles ($18,254) and the Carnegie Theater in Chicago ($7,540).  Tender Mercies eventually played at a total of 37 theaters and grossed $8,443,124. 

===Critical response===
  PG rating and omitting sex, drugs and violence. He also felt, however, that it tended toward melodrama on a few occasions and that the soundtrack had "a bit of syrupy music ... especially at the end". 

Some reviews were less favorable. David Ansen of Newsweek said, "While one respects the filmmakers small-is-beautiful philosophy, this story may indeed be too small for its britches.&nbsp;... Beresfords nice little movie seems so afraid to make a false move that it runs the danger of not moving at all."  Linda Beath of The Globe and Mail said Duvalls performance was "fabulous," but that the film was "very slight" compared to Beresfords Australian pictures.  Gary Arnold of The Washington Post panned the film, criticizing its mood and tempo and describing Buckley as its only true asset: "Tender Mercies fails because of an apparent dimness of perception that frequently overcomes dramatists: they dont always know when theyve got ahold of the wrong end of the story they want to tell." 

Many critics specifically praised Duvalls performance. Sterritt called it "one of the most finely wrought achievements to reach the screen in recent memory."  In Corlisss description, "Duvalls aging face, a road map of dead ends and dry gulches, can accommodate rage or innocence or any ironic shade in between. As Mac he avoids both melodrama and condescension, finding climaxes in each small step toward rehabilitation, each new responsibility shouldered."  Ansen said, "Robert Duvall does another of his extraordinary disappearing acts. He vanishes totally inside the character of Mac Sledge."    Maslin said he "so thoroughly transformed into Mac that he even walks with a Texans rolling gait"; she also complimented the performances of the supporting cast.  According to a review in People (magazine)|People, "Duvall gives it everything he has, which is saying a great deal. His beery singing voice is a revelation, and his unfussy, brightly burnished acting is the kind for which awards were invented." The review also described Betty Buckley as "bitchy and brilliant".  Duvall was praised as well for pulling off his first true romantic role; the actor said of the response, "This is the only film where Ive heard people say Im sexy. Its real romantic. Rural romantic. I love that part almost more than anything." 

Reflecting on the film a decade after it came out, critic Danny Peary said he found Duvalls restrained portrayal "extremely irritating" and criticized the entire cast, save for Buckley, for their "subdued, emotions-in-check, phony honest performances. You just wish the whole lot of them would start tickling each other."  .  In his book Alternate Oscars, listing his personal opinions of who should have won the Academy Awards each year, Peary excluded Tender Mercies from all the categories, and chose Michael Caine as deserving of the Best Actor honor for Educating Rita.  In June 2009, critic Roger Ebert included Tender Mercies in The Great Movies, his series of reviews celebrating what he considers the most important films of all time. He praised what he called one of Duvalls most understated performances, as well as Footes minimalist storytelling and the restraint and patience of Beresfords direction. Ebert said of Footes screenplay, "The down-to-earth quality of his characters drew attention away from his minimalist storytelling; all the frills were stripped away.&nbsp;... Rarely does a movie elaborate less and explain more than Tender Mercies." 

===Accolades=== Best Actress Best Supporting Actress, but ultimately she was nominated in neither category. 
 Britons Michael The Eagle To Kill 1963 ceremony, made sure he was present to collect his award for Best Original Screenplay.  The critical success of the film allowed Foote to exercise considerable control over his future film projects, including final veto power over major decisions; when such power was denied, Foote would simply refuse to do the film. 

{| class="wikitable collapsible collapsed" style="clear:none; font-size:90%; padding:0 auto; width:50%; margin:auto"
! colspan=2 | Awards and nominations for Tender Mercies
|-
! colspan=2 | 56th Academy Awards 
|- Best Actor (Robert Duvall) || style="background:#FAEB86" align="center"| Won
|- Best Original Screenplay (Horton Foote) || style="background:#FAEB86" align="center"| Won
|- Best Picture (Philip S. Hobel) || align="center"| Nominated
|- Best Director (Bruce Beresford) || align="center"| Nominated
|- Best Song (Austin Roberts, Bobby Hart for "Over You") || align="center"| Nominated
|-
! colspan=2 | 41st Golden Globe Awards 
|- Best Actor – Motion Picture Drama (Robert Duvall) || style="background:#FAEB86" align="center"| Won
|- Best Motion Picture – Drama (Philip S. Hobel) || align="center"| Nominated
|- Best Director (Bruce Beresford) || align="center"| Nominated
|- Best Supporting Actress – Motion Picture (Tess Harper) || align="center"| Nominated
|- Best Original Song (Austin Roberts, Bobby Hart for "Over You") || align="center"| Nominated
|-
! colspan=2 | 1983 Cannes Film Festival  
|-
| Palme dOr (Bruce Beresford) || align="center"| Nominated
|-
! colspan=2 | Kansas City Film Critics Circle Awards   
|- Award for Best Actor (Robert Duvall) || style="background:#FAEB86" align="center"| Won
|-
! colspan=2 | Los Angeles Film Critics Association Awards 
|- Award for Best Actor (Robert Duvall) || style="background:#FAEB86" align="center"| Won
|-
! colspan=2 | New York Film Critics Circle Awards 
|- Award for Best Actor (Robert Duvall) || style="background:#FAEB86" align="center"| Won
|-
! colspan=2 | Writers Guild of America Award 
|- Best Original Screenplay (Horton Foote) || style="background:#FAEB86" align="center"| Won
|-
! colspan=2 | Directors Guild of America Award 
|- Outstanding Directing – Feature Film (Bruce Beresford) ||  align="center"| Nominated
|-
! colspan=2 | Young Artist Award 
|-
| Best Young Supporting Actor in a Motion Picture (Allan Hubbard) || align="center"| Nominated
|}

== References ==
===Footnotes===
 

===Bibliography===
* 
* 
* 
* 
* 
* 
* 
* 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 