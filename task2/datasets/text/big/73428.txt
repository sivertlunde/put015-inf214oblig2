Do the Right Thing
 
 
{{Infobox film
| name           = Do the Right Thing
| image          = DO THE RIGHT THING.jpg
| caption        = Theatrical release poster
| director       = Spike Lee
| producer       = Spike Lee
| writer         = Spike Lee
| starring       = {{Plain list|
* Danny Aiello
* Ossie Davis
* Ruby Dee
* Richard Edson
* Giancarlo Esposito
* Spike Lee
* Bill Nunn
* John Turturro John Savage
 
}} Bill Lee
| cinematography = Ernest Dickerson
| editing        = Barry Alexander Brown
| studio         = 40 Acres and a Mule Filmworks
| distributor    = Universal Pictures
| released       =  
| runtime        = 120 minutes 
| country        = United States
| language       = English
| budget         = $6 million   
| gross          = $37.3 million   
}}

Do the Right Thing is a 1989 American comedy-drama hood film  produced, written, and directed by Spike Lee, who also played the part of Mookie in the film. Other members of the cast include Danny Aiello, Ossie Davis, Ruby Dee, Richard Edson, Giancarlo Esposito, Bill Nunn, John Turturro, and Samuel L. Jackson. It is also notably the feature film debut of both Martin Lawrence and Rosie Perez. The movie tells the story of a neighborhoods simmering racial tension, which comes to a head and culminates in tragedy on the hottest day of summer.
 Best Supporting greatest films of all time.     In 1999, it was deemed to be "culturally significant" by the U.S. Library of Congress, and was selected for preservation in the National Film Registry, one of just six films to have this honor in their first year of eligibility.

==Plot==
Mookie (Spike Lee) is a young black man living in Bedford-Stuyvesant, Brooklyn with his sister, Jade (Joie Lee). He and his girlfriend, Tina (Rosie Perez), have a son. Hes a pizza delivery man at the local pizzeria, but lacks ambition. Sal (Danny Aiello), the pizzerias Italian-American owner, has been in the neighborhood for twenty-five years. His older son, Pino, intensely dislikes blacks, and does not get along with Mookie. Pino (John Turturro) is at odds with his younger brother, Vito (Richard Edson), who is friendly with Mookie.
 Public Enemy on his boombox wherever he goes; and Smiley (Roger Guenveur Smith), a mentally disabled man, who meanders around the neighborhood trying to sell hand-colored pictures of Malcolm X and Martin Luther King, Jr.

While at Sals, Mookies friend, Buggin Out (Giancarlo Esposito), questions Sal about his "Wall of Fame", a wall decorated with photos of famous Italian-Americans. Buggin Out demands that Sal put up pictures of black celebrities since Sals pizzeria is in a black neighborhood. Sal replies that he doesnt need to feature anyone but Italians as it is his restaurant. Buggin Out attempts to start a protest over the Wall of Fame. Only Radio Raheem and Smiley support him.

During the day, the heat and tensions begin to rise. The local teenagers open a fire hydrant and douse the street, before police officers intervene. Mookie and Pino begin arguing over race, which leads to a series of scenes in which the characters spew racial insults into the camera. Pino and Sal talk about the neighborhood, with Pino expressing his hatred, and Sal insisting that he is not leaving. Sal almost fires Mookie, but Jade intervenes, before Mookie confronts her for being too close to Sal.
 squad car, and drive off, leaving Sal, Pino, and Vito unprotected.

The onlookers, enraged about Radio Raheems death, blame Sal and his sons. Mookie grabs a trash can and throws it through the window of Sals pizzeria, causing the crowd to rush into the restaurant and destroy it, with Smiley finally setting it on fire. Da Mayor pulls Sal, Pino, and Vito out of the mobs way. Firemen and riot patrols arrive to put out the fire and disperse the crowd. After police issue a warning, the firefighters turn their hoses on the rioters, leading to more fighting and arrests. Mookie and Jade sit on the curb, watching in disbelief. Smiley wanders back into the smoldering building and hangs one of his pictures on what is left of Sals Wall of Fame.

The next day, after having an argument with Tina, Mookie returns to Sal, who feels that Mookie betrayed him. Mookie demands his weekly pay, leading to an argument, before they cautiously reconcile, and Sal finally pays him. Mister Señor Love Daddy (Samuel L. Jackson), a local DJ, dedicates a song to Raheem.

The film ends with two quotes about violence from Martin Luther King and Malcolm X before fading to a photograph of them shaking hands.

==Cast==
*Spike Lee as Mookie
*Danny Aiello as Sal
*Ossie Davis as Da Mayor
*Ruby Dee as Mother Sister
*Giancarlo Esposito as Buggin Out
*Bill Nunn as Radio Raheem
*John Turturro as Pino
*Richard Edson as Vito
*Roger Guenveur Smith as Smiley
*Rosie Perez as Tina
*Joie Lee as Jade Steve White as Ahmad
*Martin Lawrence as Cee
*Leonard L. Thomas as Punchy
*Christa Rivers as Ella
*Robin Harris as Sweet Dick Willie
*Paul Benjamin as ML
*Frankie Faison as Coconut Sid
*Samuel L. Jackson as Mister Señor Love Daddy Steve Park as Sonny
*Rick Aiello as Officer Gary Long
*Miguel Sandoval as Officer Mark Ponte
*Luis Antonio Ramos as Stevie John Savage as Clifton
*Frank Vincent as Charlie
*Richard Parnell Habersham as Eddie
*Ginny Yang as Kim
*Nicholas Turturro (extra) (uncredited)

==Production==
Spike Lee wrote the screenplay in two weeks.  The original script of Do the Right Thing ends with a stronger reconciliation between Mookie and Sal. Sals comments to Mookie mirror Da Mayors earlier comments in the film and hint at some common ground and perhaps Sals understanding of why Mookie was motivated to destroy his restaurant. It is unclear why Lee changed the ending. 

The film was shot entirely on Stuyvesant Avenue between Quincy Street and Lexington Avenue in the Bedford-Stuyvesant neighborhood of Brooklyn. The streets color scheme was heavily altered by the production designer, who used a great deal of red and orange paint in order to help convey the sense of a heatwave.
 Steve Park, Steve White, and Robin Harris.

==Controversies==
The film was released to protests from many reviewers, and it was openly stated in several newspapers that the film could incite black audiences to riot.   Lee criticized white reviewers for implying that black audiences were incapable of restraining themselves while watching a fictional motion picture. Spike Lees Last Word, special feature on the Criterion Collection DVD (2000) 

One of many questions at the end of the film is whether Mookie "does the right thing" when he throws the garbage can through the window, thus inciting the riot that destroys Sals pizzeria. Critics have seen Mookies action both as an action that saves Sals life, by redirecting the crowds anger away from Sal to his property, and as an "irresponsible encouragement to enact violence".    The question is directly raised by the contradictory quotations that end the film, one advocating nonviolence, the other advocating violent self-defense in response to oppression.  

Spike Lee has remarked that he himself has only ever been asked by white viewers whether Mookie did the right thing; black viewers do not ask the question.  Lee believes the key point is that Mookie was angry at the death of Radio Raheem, and that viewers who question the riots justification are implicitly failing to see the difference between property and the life of a black man. 

In June 2006, Entertainment Weekly magazine placed Do the Right Thing at No. 22 on its list of The 25 Most Controversial Movies Ever. 

==Critical reception==
Do the Right Thing was met with universal acclaim. On Rotten Tomatoes, the film has a rating of 92%, based on 66 reviews, with an average rating of 8.9/10. The sites critical consensus reads, "Smart, vibrant and urgent without being didactic, Do the Right Thing is one of Spike Lees most fully realized efforts -- and one of the most important films of the 1980s."  On Metacritic, the film has a score of 91 out of 100, based on 15 critics, indicating "universal acclaim", and placing it as the 68th highest film of all-time on the site. 

Both Gene Siskel and Roger Ebert ranked the film as the best of 1989 and later ranked it as one of the top 10 films of the decade (#6 for Siskel and #4 for Ebert).  Ebert later added the film to his list of The Great Movies. 

==Awards and nominations== 1990 Academy Awards
* Best Actor in a Supporting Role&nbsp;– Danny Aiello (nominated)
* Best Writing, Screenplay Written Directly for the Screen&nbsp;– Spike Lee (nominated)
 1990 Belgian Syndicate of Cinema Critics Grand Prix (nominated)

1989 Cannes Film Festival
* Palme dOr&nbsp;– Spike Lee (nominated)   
 1990 Chicago Film Critics Association Awards
* Best Director&nbsp;– Spike Lee (won)
* Best Picture (won)
* Best Supporting Actor&nbsp;– Danny Aiello (won)
 1990 Golden Globes
* Best Director (Motion Picture)&nbsp;– Spike Lee (nominated)
* Best Motion Picture&nbsp;– Drama (nominated)
* Best Performance by an Actor in a Supporting Role in a Motion Picture&nbsp;– Danny Aiello (nominated)
* Best Screenplay (Motion Picture)&nbsp;– Spike Lee (nominated)
 1991 NAACP Image Awards
* Outstanding Lead Actress in a Motion Picture&nbsp;– Ruby Dee (won)
* Outstanding Supporting Actor in a Motion Picture&nbsp;– Ossie Davis (won)
 1989 Los Angeles Film Critics Association Awards
* Best Director&nbsp;– Spike Lee (won)
* Best Music&nbsp;– Bill Lee (won)
* Best Picture (won)
* Best Supporting Actor&nbsp;– Danny Aiello (won)
 1989 New York Film Critics Circle Awards
* Best Cinematographer&nbsp;– Ernest Dickerson (won)

2010&nbsp;– The 20/20 Awards
* Best Picture&nbsp;– (nominated)
* Best Director&nbsp;– Spike Lee (won)
* Best Supporting Actor&nbsp;– Danny Aiello (nominated)
* Best Supporting Actor&nbsp;– John Turturro (nominated)
* Best Original Screenplay&nbsp;– Spike Lee (nominated)
* Best Editing&nbsp;– Barry Alexander Brown (won) Public Enemy (won)

AFIs 100 Years 100 Movies 2007

Additional AFI titles include:

* AFIs 100 ...Cheers Nominated
* AFIs 100... Thrills Nominated
* AFIs 100 Years... 100 Movies Nominated
* AFIs 100 Songs... Public Enemy Fight The Power No. 40

National Film Preservation Board
* National Film Registry (1999)

MTV Movie Awards
* The Bucket of Excellence (lifetime achievement award, MTV Movie Awards 2006|2006)

==Soundtrack==
{{Infobox album  
| Name = Do the Right Thing  
| Type = Film Bill Lee
| Cover =
| Released = 1989
| Recorded = December 12, 1988 – December 16, 1988
| Genre = Film score
| Length = 35:36 Columbia
| Producer = Spike Lee  (executive producer#Music|exec.) 
}}
{{Infobox album  
| Name = Do the Right Thing  
| Type = Soundtrack
| Artist = Various artists
| Cover = 
| Released = 1989
| Recorded =
| Genre = Soundtrack
| Length = 53:14
| Label = Motown Records
| Producer = Gregory "Sugar Bear" Elliott  (executive producer#Music|exec.) , Ted Hopkins  (executive producer#Music|exec.) , Mark Kibble  (executive producer#Music|exec.) , Spike Lee  (executive producer#Music|exec.) , Johnny Mercer  (executive producer#Music|exec.) 
}}
 Hot R&B/Hip-Hop Public Enemys Hot Dance Hot Rap Singles chart.  

===Score===
{{Track listing
| music_credits = yes
| title1          = Mookie Goes Home
| note1           =
| writer1         =
| length1         = 1:21
| title2          = We Love Roll Call Y-All
| note2           =
| writer2         =
| length2         = 1:40
| title3          = Father to Son
| note3           =
| writer3         =
| length3         = 4:24
| title4          = Da Mayor Drinks His Beer
| note4           =
| writer4         =
| length4         = 1:03
| title5          = Delivery for Love Daddy
| note5           =
| writer5         =
| length5         = 1:08
| title6          = Riot
| note6           =
| writer6         =
| length6         = 1:08
| title7          = Magic, Eddie, Prince Aint Niggers
| note7           =
| writer7         =
| length7         = 1:58
| title8          = Mookie  
| note8           =
| writer8         =
| length8         = 6:45
| title9          = How Long?
| note9           =
| writer9         =
| length9         = 3:43
| title10         = Mookie  
| note10          =
| writer10        =
| length10        = 6:32
| title11         = Da Mayor Loves Mother Sister
| note11          =
| writer11        =
| length11        = 1:23
| title12         = Da Mayor Buys Roses
| note12          =
| writer12        =
| length12        = 1:14
| title13         = Tawana
| note13          =
| writer13        =
| length13        = 1:31
| title14         = Malcolm and Martin
| note14          =
| writer14        =
| length14        = 1:46
| title15         = Wake Up Finale
| note15          =
| writer15        =
| length15        = 7:26
}}

===Soundtrack===
{{Track listing
| music_credits = yes
| extra_column  = Producer(s)
| title1          = Fight the Power Public Enemy Hank Shocklee, Carl Ryder, Eric Sadler
| length1         = 5:23
| title2          = My Fantasy Teddy Riley, Guy
| extra2          = Riley, Gene Griffin
| length2         = 4:57
| title3          = Party Hearty
| music3          = Experience Unlimited|E.U.
| extra3          = Kent Wood, JuJu House
| length3         = 4:43
| title4          = Cant Stand It
| music4          = Steel Pulse
| extra4          = David R. Hinds, Sidney Mills
| length4         = 5:06
| title5          = Why Dont We Try?
| music5          = Keith John
| extra5          = Vince Morris Raymond jones larry decarmine
| length5         = 3:35
| title6          = Feel So Good
| music6          = Perri
| extra6          = Paul Laurence, Jones
| length6         = 5:39
| title7          = Dont Shoot Me
| music7          = Take 6
| extra7          = Mervyn E. Warren
| length7         = 4:08
| title8          = Hard to Say
| music8          = Lori Perry, Gerald Alston
| extra8          = Laurence
| length8         = 3:21
| title9          = Prove to Me
| music9          = Perri
| extra9          = Jones, Sami McKinney
| length9         = 5:24
| title10         = Never Explain Love
| music10         = Al Jarreau
| extra10         = Jones
| length10        = 5:58
| title11         = Tu y Yo/We Love  
| music11         = Rubén Blades
| extra11         = Blades
| length11        = 5:12
}}

==References==
 

==Bibliography== Faber and Faber Limited, 2005. ISBN 0-393-06153-1.
* Spike Lees Last Word. Documentary on the Criterion Collection DVD of Do the Right Thing. 2000.
* Spike Lee et al. Commentary on the Criterion Collection DVD of Do the Right Thing. 2000.

==Further reading==
* 
* 

 

==External links==
 
*  
*   Criterion Collection
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 