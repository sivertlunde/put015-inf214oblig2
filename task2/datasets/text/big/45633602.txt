36 Vayadhinile
{{Infobox film
| name           = 36 Vayadhinile
| image          = 36 vayathinile.jpg
| alt            =  
| caption        = 
| director       = Rosshan Andrrews
| producer       = Suriya 
| writer         = Viji (dialogues)
| screenplay     = Bobby Sanjay
| story          = Roshan Andrews Rahman Abhirami Abhirami
| music          = Santhosh Narayanan
| cinematography = R. Diwakaran
| editing        = Mahesh Narayanan
| studio         = 2D Entertainment
| distributor    = 
| released       = May 15 2015  
| runtime        = 
| country        = India
| language       = Tamil
| budget         =  
| gross          =
}} Malayalam film How Old Rahman essays a supporting role.

== Cast ==
 
*Jyothika as Vasanthi Rahman as Tamil selvan Abhirami
*Nassar
*Delhi Ganesh
*Khalid Hussain as Horticulture Officer
*Ilavarasu Prem
*Siddharth Basu
*Sanjay Bharathi
*M. S. Bhaskar
*Devadarshini
*Muthukumar
 

==Production== Rahman was added to the cast in October 2014, to play the role originally essayed by Kunchacko Boban.   Music director Santhosh Narayanan was signed on to compose the films score and soundtrack. 
 Abhirami also signed on to portray a pivotal role, marking a comeback to Tamil films after an eleven-year absence.  Sanjay Bharathi was also offered a small role in the film by producer Suriya, who had been working with him in another production. Masss (2015).

==Soundtrack==
{{Infobox album Name = 36 Vayadhinile Type = Soundtrack
|Artist= Santhosh Narayanan Cover =  Caption = Released = 17 February 2014 Genre = Feature film soundtrack Length = Language = Tamil
|Label = Think Music Producer = Santhosh Narayanan Last album Enakkul Oruvan (2014) This album = 36 Vayadhinile (2015) Next album =Irudhi Suttru (2015)  Reviews =
}}

The films soundtrack was composed by Santhosh Narayanan. The soundtrack album features eleven tracks, which includes three songs, five instrumentals from the original score and three karaoke numbers.  The lyrics for the three songs were written by Vivek. The album was launched at Leela Palace, Chennai on 6 April 2015.   Two songs, "Rasathi" and "Happy-Naalu Kazhudha" were performed live by Santhosh at the event, which was hosted by Dhivyadharshini.  Sify gave the album 4 out of 5 stars and wrote, "Santhosh Narayanan delivers another terrific album adding to the list of his impressive filmography. 36 Vayadhinile has a good collection of some catchy songs & soothing background score which is a great attribute to the movie". 

{{tracklist
| headline        = Track listing
| extra_column    = Singer(s)
| all_lyrics      = Vivek
| total_length    =

| title1          = Happy - Naalu Kazhudha
| extra1          = Santhosh Narayanan
| length1         = 
| title2          = Pogiren
| extra2          = Kalpana Raghavendar
| length2         = 
| title3          = Rasathi
| extra3          = Lalitha Vijayakumar
| length3         = 
| title4          = President
| extra4          = —
| length4         = 
| title5          = Kannadi
| extra5          = —
| length5         = 
| title6          = Vidiyal Thedi
| extra6          = —
| length6         = 
| title7          = Kanavugal Sumandhu
| extra7          = —
| length7         = 
| title8          = Kanneer
| extra8          = —
| length8         = 
}}

== References ==
 

==External links==
 

 
 
 
 
  