Woman on the Beach
 
{{Infobox film
| name           = Woman on the Beach
| image          = Woman on the Beach film poster.jpg
| caption        = Theatrical poster
| film name = {{Film name
 | hangul         =  
 | hanja          =  의  
 | rr             = Haebyeoneui Yeoin
 | mr             = Haebyŏnŭi Yŏin}}
| director       = Hong Sang-soo
| producer       = Oh Jung-wan
| writer         = Hong Sang-soo Kim Tae-woo 
| music          = Jeong Yong-jin
| cinematography = Kim Hyung-koo
| editing        = Ham Sung-won
| studio         = BOM Film Productions
| distributor    = Mirovision
| released       =  
| runtime        = 127 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = $749,147 
}} released in 2006.      
  

== Plot ==
Film director and screenwriter Kim Jung-rae asks his friend Won Chang-wook to drive with him from the homes in Seoul to the resort town of Shinduri, on the western coast of South Korea.  Chang-wook initially resists, but accepts the request on the condition that he can bring Kim Mun-suk, a composer and aspiring singer whom he describes as being his girlfriend.  Jung-rae is writing a treatment for a film titled "About Miracles," concerning the mysterious connections that undergird everyday life—themes that play a major role in the work of Hong Sang-Soo.  Mun-suk quickly makes clear that she does not consider herself Chang-wooks girlfriend, and she finds herself and Jung-rae increasing drawn together.  As the three drive on, Mun-suk discusses her years living abroad in Germany and reveals that she has had a number of relationships with Europeans, a fact that greatly disturbs both Chang-wook and Jung-rae.  Mun-suk is particularly disappointed in Jung-raes reaction, claiming, "Youre not like your films."  Nevertheless, Mun-suk and Jung-rae later kiss on the beach and then sleep together in a low-rent hotel room.

The next day as the three drive back to Seoul, Jung-rae pulls back from his intimacy with Mun-suk and returns to the beach alone two days later.  Missing Mun-suk despite his actions, Jung-rae hits on two women, one of whom vaguely resembles Mun-suk, by introducing himself as a film director and asking to interview them for his screenplay.  Jung-rae proceeds to seduce Choi Sun-hee in much the same fashion as he had Mun-suk just a few days prior.  While sleeping with Sun-hee in the same beachside motel where hed been with Mun-suk, Jung-rae is surprised to find that Mun-suk has returned to Shinduri, found his room, and started banging on the door loudly and very late at night.  Jung-rae sneaks Sun-hee out of his room through a separate exit the next morning as Mun-suk sleeps at the foot of his door with a terrible hangover.  Jung-rae attempts to reconcile with Mun-suk and lies about his night with Sun-hee, although his lie is increasingly transparent to all concerned.  Having alienated Mun-suk and left Sun-hee without a goodbye, Jung-rae returns to Seoul with a creative breakthrough on his screenplay.

== Cast ==
* Kim Seung-woo ... Director Kim Jung-rae
* Go Hyun-jung ... Kim Mun-suk
* Song Seon-mi ... Choi Sun-hee Kim Tae-woo ... Won Chang-wook
* Moon Sung-keun ... (voice)
* Jung Chan ... Guy driving Mun-suk home
* Lee Ki-woo ... Beach resort caretaker
* Oh Tae-kyung ... Waiter at empty sushi restaurant
* Choi Ban-ya ... Sun-hees friend

== Release ==
Woman on the Beach was released in South Korea on August 31, 2006, and received a total of 225,388 admissions nationwide.  

==Awards and nominations==
2006 Busan Film Critics Awards  Kim Tae-woo
* Best New Actress - Go Hyun-jung

2006 Korean Film Awards
* Nomination - Best Actress - Go Hyun-jung
* Nomination - Best Director - Hong Sang-soo
* Nomination - Best Cinematography - Kim Hyung-koo
* Nomination - Best New Actress - Go Hyun-jung

2006 Directors Cut Awards
* Best Director - Hong Sang-soo

2007 Asian Film Awards
* Nomination - Best Director - Hong Sang-soo
* Nomination - Best Screenplay - Hong Sang-soo
* Nomination - Best Music - Jeong Yong-jin

2007 Baeksang Arts Awards
* Nomination - Best Director - Hong Sang-soo
* Nomination - Best New Actress - Go Hyun-jung

2007 Grand Bell Awards
* Nomination - Best New Actress - Go Hyun-jung

== References ==
 

== External links ==
* http://www.filmbom.com/womanonthebeach  
*  
*  
*  

 

 
 
 
 
 
 
 
 