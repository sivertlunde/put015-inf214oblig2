On the Beach (2000 film)
{{Infobox name = On the Beach
| image          = On The Beach (2000).jpg
| caption        = DVD cover image size      = 200px
| format         = Science fiction
| director       = Russell Mulcahy
| producer       = John Edwards
| writer         = John Paxton (1959 screenplay) David Williamson and Bill Kerby (teleplay)
| starring       = Armand Assante Bryan Brown Rachel Ward Grant Bowler Jacqueline McKenzie Steve Bastoni
| music          = Christopher Gordon	
| cinematography = Martin McGrath
| editing        = Mark Perry
| studio         = Australian Film Commission Coote Hayes Productions Edwards/Sullivan Productions Southern Star Entertainment
| distributor    = Showtime Networks
| released       =  
| runtime        = 195 minutes
| rating         =
| country        = United States Australia
| language       = English
| budget         =  	  	
| preceded_by    =
| followed_by    =
}}

On the Beach is a 2000  , May 26, 2000, p. F1. Retrieved: January 11, 2015.  It was originally aired on   , April 19, 2007. Retrieved: January 11, 2015. 
 the 1959 the 1957 novel by Nevil Shute   but updates the setting of the story to the films then-future of 2006, starting with placing the crew on the fictional Los Angeles class submarine|Los Angeles-class USS Charleston (SSN-704) submarine.  )

==Plot== caterpillar drive nuclear exchange. The nuclear war was preceded by a standoff between the United States and Peoples Republic of China|China, after the latter blockaded and later invaded Republic of China|Taiwan. The submarine crew finds refuge in Melbourne, Australia which the radioactive fallout has not yet reached.

When Captain Dwight Towers (Armand Assante), Australian scientist Julian Osborne (Bryan Brown) and Australian liaison officer Peter Holmes (Grant Bowler) find out there is an automated digital broadcast coming from Alaska in the Northern Hemisphere, the submarine is sent to investigate. The submarine surfaces in San Francisco, where the Golden Gate Bridge has collapsed and the city shoreline is in ruins.

No trace of survivors is found on their mission, and after the unsuccessful attempt to trace the messages, Towers returns to Melbourne where Moira Davidson (Rachel Ward) is waiting for him. Realizing the inevitable nuclear cloud will reach Australia, their impending doom begins to unravel the social fabric of the survivors in Australia and anarchy and chaos erupt. Some choose to live their final weeks recklessly in a deadly car race, while others seek a more peaceful means to face the end of their lives. Holmes and his wife (Jacqueline McKenzie) seek solace in their love for each other as Towers and Moira decide to remain together till the end.

==Cast==
 
* Armand Assante as Captain Dwight Towers
* Rachel Ward as Moira Davidson
* Bryan Brown as Dr. Julian Osborne
* Jacqueline McKenzie as Mary Davidson Holmes
* Grant Bowler as Lt. Peter Holmes
* Allison Webber as Jenny Holmes
* Tieghan Webber as Jenny Holmes
* Steve Bastoni as First Officer Neil Hirsch
* David Ross Paterson as Chief Wawrzeniak (credited as David Paterson)
* Kevin Copeland as Sonarman Bobby Swain
* Todd MacDonald as Radioman Giles
* Joe Petruzzi as Lt. Tony Garcia
* Craig Beamer as Crewman Reid
* Jonathan Oldham as Crewman Parsons
* Trent Huen as Crewman Samuel Huynh
* Donni Frizzell as Crewman Rossi
* Jonathan Stuart as Crewman Burns
* Sam Loy as Seaman Sulman
* Charlie Clausen as Seaman Byers
* Robert Rabiah as Cook Gratino
* Marc Carra as Cook Walmsey
* Rod Mullinar as Admiral Jack Cunningham
* Felicity Boyd as Lt. Ashton Bill Hunter as Prime Minister Seaton
* Charles "Bud" Tingwell as Professor Alan Nordstrum (credited as Charles Tingwell)
 

==Production==
In On the Beach (2000), the  , May 28, 2000, p. TV3. Retrieved: January 11, 2015. 

A number of alterations from the book and original film adaptation are made, including a different ending differing from both the novel and film in that the submarine Commander chooses to die with his newfound love instead of  , May 28, 2000. Retrieved: January 11, 2015. 

The film ends with a quote from a Walt Whitmans poem "On The Beach at Night", describing how frightening an approaching cloud bank seemed at night to the poets child, blotting the stars out one by one, as the father and child stood on the beach on North Shore (Massachusetts)|Massachusetts North Shore.    As much as it resembles the plot of the movie and of Shutes novel, however, the book gives only an incidental reference to the Whitman poem,  and the phrase "on the beach" is a Royal Navy term that means "retired from the Service." 

==Reception==
On the Beach (2000) had mixed reviews because with its three-hour account of impending doom, reviewers considered it "slow going".     ever, other reviewers, however, found aspects to praise. Held, Richard.   klat.com, March 11, 2013. Retrieved: January 11, 2015.   Richard Scheib, the Science Fiction, Horror and Fantasy Film Review critic saw the film as benefiting from the lengthier timeline, "The mini-series certainly has the luxury to pad the story out and tell it with more length than the film did. As a result there is a greater degree of emotional resonance to the characters than the 1959 film had ... Mostly the mini-series works satisfyingly as a romantic drama, which it does reasonably depending on the extent to which one enjoys these things. Crucially though the mini-series does manage to work as science-fiction and Russell Mulcahy delivers some impressive images of the aftermath of the nuclear holocaust. There are some fine scenes with Armand Assante and the submarine crew walking through the ruins of Anchorage discovering how the people there committed suicide en masse, and some excellent digital effects during the periscope tour of the ruins of San Francisco." 

On the Beach (2000) received two   (Associated Press), December 22, 2000, p. 8B. Retrieved: January 11, 2015. 

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Shute, Nevil. On The Beach. New York: William Morrow and Company, 1989, First edition 1957. ISBN 978-0-34531-148-1.
 

==External links==
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 