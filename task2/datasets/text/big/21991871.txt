Thakilu Kottampuram
{{Infobox film 
| name           = Thakilu Kottampuram
| image          = Thakilu Kottampuram.jpg
| caption        =
| director       = Balu Kiriyath
| producer       = S Thankappan
| writer         = S Thankappan Balu Kiriyath (dialogues)
| screenplay     = Balu Kiriyath
| starring       = Prem Nazir Sheela Mohanlal Jagathy Sreekumar
| music          = Darsan Raman P Susheeladevi
| cinematography = Ashok Chowdhary
| editing        = NP Suresh
| studio         = JK Productions
| distributor    = JK Productions
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by Balu Kiriyath and produced by S Thankappan. The film stars Prem Nazir, Sheela, Mohanlal and Jagathy Sreekumar in lead roles. The film had musical score by Darsan Raman and P Susheeladevi.   

==Cast==
 
*Prem Nazir as Rajakrishna Kurup
*Sheela as Mridula
*Mohanlal as Paul
*Jagathy Sreekumar as Shishupalan
*Adoor Bhasi as Kunjunni Kurup
*Sukumaran as Unnikrsihna Kurup
*Anjali Naidu as Uma
*Baby Manjusha as Sreemol
*Jalaja as Padmaja
*Mala Aravindan as Vasu Santhakumari as Mridulas mother
*Sathyachithra 
*Praveena (old) as Urmila
*Sreekala as Thresiakutty
*Rathidevi as Thresa
 

==Soundtrack==
The music was composed by Darsan Raman and P Susheeladevi and lyrics was written by Balu Kiriyath. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Da da da daddy || K. J. Yesudas, Baby Kala, KS Beena || Balu Kiriyath || 
|-
| 2 || Ekaanthathayude || P Susheela || Balu Kiriyath || 
|-
| 3 || Erinjadangumen   ||  || Balu Kiriyath || 
|-
| 4 || Kannippoompaithal || K. J. Yesudas, KS Beena || Balu Kiriyath || 
|-
| 5 || Swapnangale Veenurangu || K. J. Yesudas || Balu Kiriyath || 
|}

==References==
 

==External links==
*  

 
 
 

 