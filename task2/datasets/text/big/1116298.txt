G-Sale
{{Infobox film|
  name     = G-Sale |
  image          =|
  caption  = G-Sale Limited Edition DVD |
  director       = Randy Nargi |
  writer         = Randy Nargi |
  starring       = Jessi Badami   Scott Burns   Tracey Conway |
producer       = Jessi Badami |
  distributor    = Bogwood Films |
  released   =   |
  runtime        = 77 minutes |
  language = English |
  budget =  |
}}
G-Sale is a 2003 mockumentary film about garage sales and the people who are obsessed by them. The film is reminiscent  of the movies of Christopher Guest and won several film festival awards.
 

==Plot==
Bogwood, Washington is a pleasant suburban community with a special distinction—it has more garages per capita than any other town in America. Not surprisingly, Bogwood is also the "Garage Sale Capital of the U.S.A."

When retirees Doris & Clayton Fenwick decide to empty their nest of retro-modern antiques, they set the wheels in motion for a frantically funny "g-sale" involving Bogwoods most avid garage sale junkies: Angela Cocci (an obsessive market researcher), Ed LaSalle (a beleaguered computer programmer and creator of the cult fantasy roleplaying game "Caves & Beasts"), Dick Nickerson (a retired star of the 60s sitcom "Pot o Gold"), and BJ Harwood & Helen Ziegler (partners and owners of a trendy retro-modern antique store).

These colorful characters try to outmaneuver each other to score their ultimate garage sale treasure: an antique board game worth a fortune.

==Location== Bainbridge Island, Washington.

==References==
 

==External links==
*  
*  

 
 
 
 
 


 