Aavanazhi
{{Infobox film
| name           = Aavanazhi 
| image          = Avanazhi.jpg
| image_size     = 
| alt            = 
| caption        = Poster designed by P. N. Menon (director)|P. N. Menon
| director       = I. V. Sasi
| producer       = Sajan
| writer         = T. Damodaran
| narrator       =  Geetha  Janardanan  Seema  Nalini Shyam
| cinematography = Jayaram V.   
| editing        = K. Narayanan   
| studio         = Saj Productions
| distributor    = 
| released       =  
| runtime        = 152 minutes 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Aavanazhi (  action film. Directed by I. V. Sasi, based on the script written by T. Damodaran, Aavanazhi was a huge hit at the box office. Mammootty appears in the lead role of Balram, a police inspector. This film deals with social and political issues of that time. Starring with Mammootty in lead roles are  Sreenivasan and Satyamev Jayate. I.V. Sasi brought out two sequels &mdash; Inspector Balram in 1991 and Balram vs. Taradas in 2006.

==Plot==
Balram (Mammootty), popularly known as Karadi (Bear) is an honest Circle Inspector. Frustrated in his life after several personal setbacks, including a failed affair of the heart, Balram has turned to drinking and womanizing. Balram is assigned to nab Satyaraj (Captain Raju), who evaded the clutches of the police after murdering Chackochan (Paravoor Bharathan), a contractor. Balram successfully arrests him, but in court, Satyaraj is represented by Advocate Jayachandran (Sukumaran). Satyraj is acquitted by court, but Balram decides to frame in several other criminal charges pending against him.  Usha (Nalini), Balrams ex-lover is now married to Jayachandran, who is now involved in several illegal business ventures. Balram falls in love with Seetha (Geetha (actress)|Geetha), a prostitute, whom he decides to marry. Meanwhile, Radha (Seema (actress)|Seema), a young lady is determined to avenge herself on Balram, who she believes to have killed her brother while in police custody. But in reality, it was a handwork of Satyaraj, on instruction from Vincent (Janardhanan (actor)|Janardanan). Falsely implicated, Balram had been suspended from police for a couple of years, but has been reinstated. Though Seetha tried to convince Radha of the truth, she is not ready to accept it. To gain her revenge over Balram, Radha decides to offer Satyaraj a safe stay away from the police eyes. Balrams open fight with Satyaraj and his "outside the law" dealing with him forms the rest of the story.

==Cast==
 
*Mammootty as Balram Geetha as Seeta
*Sukumaran as Jayachandran Janardanan as Vincent Nalini as Usha Seema as Radha
*Captain Raju as Sathyaraj
*C. I. Paul as Chandrahasan
*Kundara Johny as Alex Innocent as Vishnu Azeez as Azeez Kunchan as Samshayam Vasu Shafeeq
*Jagannatha Varma as Kumar Augustine as Ummer
*Sankaradi as Vishwanathan
*Thikkurisi Sukumaran Nair as Nampoothiri
*Santhakumari as Radhas mother Sreenivasan as Srini
*Prathapachandran
*Paravoor Bharathan as Chackochan
 

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
|- 1987
|Marana Shasanam Telugu cinema|Telugu Krishnam Raju, Jayasudha, Madhavi (actress)|Madhavi, Shobana
|S. S. Ravi Chandra
|-
| 1987 Satyamev Jayate Hindi
| Vinod Khanna, Meenakshi Sheshadri, Madhavi (actress)|Madhavi, Anita Raj
| Raj N. Sippy
|-
| 1987
|Kadamai Kanniyam Kattupaadu Tamil cinema|Tamil Jeevitha
|Santhana Bharathi
|-
|}

==External links==
*  

 
 
 
 
 
 
 
 

 
 