Gods of the Plague
 1970 cinema West German black-and-white drama film directed by Rainer Werner Fassbinder.

==Plot==
After his release from prison, ex-convict Franz Walsch surely finds his way back into the Munich criminal underworld and also finds that his attentions are meanwhile torn now between two women, Joanna and Margarethe, as well as by Günther, his friend who earlier shot his brother.

==Production==
The film was shot in Munich and Dingolfing during five weeks in October and November 1969 with Fassbinder eventually coming to consider it shortly before passing away as the fifth best feature film he made during his entire career. 

==Analysis==
  symbolism. 

==Cast==
*Hanna Schygulla as Joanna Reiher
*Margarethe von Trotta as Margarethe
*Harry Baer as Franz Walsch
*Günther Kaufmann as Günther
*Carla Egerer as Carla Aulaulu
*Ingrid Caven as Magdalena Fuller
*Jan George as Policeman
*Marian Seidowsky as Marian Walsch
*Yaak Karsunke as Inspector
*Micha Cochina	as Joe
*Hannes Gromball as Supermarket Manager
*Lilith Ungerer as Girl
*Katrin Schaake as Café Owner
*Lilo Pempeit as Mother
*Rainer Werner Fassbinder as Pornography Customer
*David Morgan as Catcher
*Thomas Schieder as Tommy
*Kurt Raab as Pub Guest
*Irm Hermann as Bartender
*Peter Moland
*Doris Mattes as Marie Luise
*Eva Madelung as Girl
*Ursula Strätz as Antique Shop Owner

==Release== Eclipse series Love Is Colder Than Death, Katzelmacher, The American Soldier, and Beware of a Holy Whore. 

==Reception== heterosexual love Time Out praised the film as "a witty, stylish meditation on the genre, filtered through the decidedly dark and morbid sensibility of its director." 

==References==
 

==Bibliography==
*Silverman, Kaja. "Fassbinder and Lacan: A Reconsideration of Gaze, Look, and Image." In: Bryson, Norman,  , 2013, p.&nbsp;272ff. ISBN 0819574236, 9780819574237.

==External links==
*  at the    
* 
* 
*  TCM Movie Database Film Portal  
 
 
 
 
 
 
 
 
 
 
 
 
 