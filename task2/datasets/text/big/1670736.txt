Swimming with Sharks
 
 
{{Infobox film
| name           = Swimming With Sharks
| image          = Swimming with Sharks poster.jpg
| alt            = A man towering over another shouting, the words Swimming with Sharks filling the background
| caption        = Theatrical release poster George Huang
| producer       =
| writer         = George Huang
| starring       = Kevin Spacey Frank Whaley Michelle Forbes Benicio del Toro T.E. Russell
| music          = Tom Hiel
| cinematography = Steven Finestone
| editing        =
| distributor    = Trimark Pictures
| released       =  
| runtime        = 93 minutes 
| country        = United States
| language       = English
| budget         = $700,000
| gross          = $382,928
}} George Huang. The film stars Kevin Spacey, Frank Whaley, and Michelle Forbes.

==Plot==
Buddy Ackerman, an influential movie mogul, hires Guy, a naïve young writer, as his assistant. Guy, who has just graduated from film school, believes that his new job is a golden opportunity. Despite warnings from Rex, the outgoing assistant who has become hardened under Buddys reign, Guy remains optimistic.
 sadistic (and public) verbal abuse, and has him bending over backwards to do meaningless errands that go beyond just his work life. Guy is humiliated and forced to bear the brunt of his insults. Guys only solace is his girlfriend, Dawn, a producer at Buddys firm. When Buddy apparently fires Guy in a phone call, Guy snaps and kidnaps Buddy in order to exact revenge. He ties Buddy up and subjects him to severe beatings, torture and mind games. It is later revealed that due to a botched call waiting function on Buddys home phone, Guy hears Buddy and Dawn arranging a rendezvous at Buddys house.

Once in Guys power, Buddy reveals for the first time a human, vulnerable side, telling a tragic story about his wifes death and revealing that he too was once a bullied assistant to powerful, tyrannical men and spent a decade putting up with such abuse to get to where he is today. He also reveals that abusing Guy was his way of teaching him that he has to earn his success. Dawn arrives at the scene to find Guy aiming a gun at Buddys face, and Buddy tells Guy that he has to pull the trigger in order to get ahead in the business. After a moments indecision, Guy fires the gun.

The climax of the film reveals that Guy killed Dawn (who is blamed for kidnapping and torturing Buddy), and was subsequently promoted. In the final scene, Guy coldly tells a former colleague to find out what he really wants and then do anything to get it; during this speech, Buddy stands by, smiling as he calls Guy into his office for a meeting.

==Cast==
* Kevin Spacey as Buddy Ackerman
* Frank Whaley as Guy
* Michelle Forbes as Dawn Lockard
* Benicio del Toro as Rex
* T.E. Russell as Foster Kane
* Roy Dotrice as Cyrus Miles
* Matthew Flynt as Manny
* Patrick Fischler as Moe
* Jerry Levine as Jack

==Production==
  George Huang decided to write the script after having a conversation with Robert Rodriguez. Rodriguez was in Los Angeles after his film El Mariachi brought him to the attention of Sony Pictures, where he befriended Huang. Huang told Rodriguez of his frustrations with filmmaking when the director encouraged him to quit his post at Sony and pursue writing full-time so Huang could produce a script to direct himself.
 Frank Evers, who brought in financing from independent investors, and significant production support from Sony Pictures Entertainment. The film was subsequently sold to Trimark Pictures (later assumed by Lionsgate in 2000). Cineville produced the film with Steve Alexander overseeing production.

Although writer George Huang himself worked as an assistant for Barry Josephson, who was the Senior Vice President of Development at Sony Pictures at the time, some have suggested that Buddys character was inspired by real life movie mogul Scott Rudin, while others suggest he is based on producer Joel Silver with Guy being based on Alan Schechter, Silvers assistant in the early 90s.

The director Buddy hires in the film, Foster Kane, is named after Orson Welles character in the 1941 film Citizen Kane, Charles Foster Kane.

==Reception==
The film was met with positive reviews. At Rotten Tomatoes, Swimming with Sharks holds a score of 80% of positive reviews by critics, based on 35 reviews; the consensus states: "Swimming With Sharks is a smart, merciless Hollywood satire thats darkly hilarious and observant, thanks to Kevin Spaceys performance as ruthless studio mogul Buddy Ackerman." 

American Film Institute recognition:
*AFIs 100 Years...100 Heroes and Villains:
** Buddy Ackerman - Nominated Villain 

==Stage adaptation== Matt Smith as Guy, Arthur Darvill as Rex and Helen Baxendale as Dawn. Academy Award nominee Demian Bichir opened a Spanish version of the play in Mexico City on January 2012.
 Drama Centre George Young as Guy and Janice Koh as Dawn.

==References==
 

==External links==
 
*  

 
 
 
 
 
 
 
 
 
 
 