Paris at Midnight
{{infobox film
| name           = Paris at Midnight
| image          =
| imagesize      =
| caption        =
| director       = E. Mason Hopper E. J. Babille (assistant)
| producer       = Metropolitan Pictures Corporation of California
| writer         = Honoré de Balzac (story) Frances Marion (adaptation)
| starring       = Jetta Goudal Lionel Barrymore
| cinematography = Norbert Brodine
| editing        = 
| distributor    = Producers Distributing Corporation (PDC)
| released       =  
| runtime        = 70 minutes; 7 reels
| country        = United States Silent (English intertitles)
}}
Paris at Midnight is a surviving 1926 silent film drama starring Jetta Goudal and Lionel Barrymore and was directed by E. Mason Hopper. It was distributed by Producers Distributing Corporation aka PDC. 

A copy is preserved at the Cinémathèque Française, Paris. 

==Cast==
*Jetta Goudal - Delphine
*Lionel Barrymore - Vautrin
*Mary Brian - Victorine Tallefer
*Edmund Burns - Eugene de Rastagnic
*Emile Chautard - Pere Goriot
*Brandon Hurst - Count Tarrefer
*Jocelyn Lee - Anastasie
*Mathilde Comont - Madame Vauquer
*Carrie Daumery - Madmoiselle Miche
*Fannie Yantis - Julie
*Jean De Briac - Frederic Tallefer
*Charles Requa - Maxime de Trailers

==References==
 

==External links==
* 
* 
*  (*if page does not load click on the   link, then click back)

 
 
 
 
 
 
 
 


 