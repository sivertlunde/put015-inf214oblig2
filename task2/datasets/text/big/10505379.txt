The Hunt for Eagle One
{{Infobox film|
| name           = The Hunt for Eagle One
| image          = The_Hunt_for_Eagle_One.jpg
| director       = Brian Clyde
| producer       = Roger Corman Cirio H. Santiago Amy Searles
| writer         = Michael Henry Carter
| starring       = Mark Dacascos Theresa Randle Rutger Hauer Joe Suba Zach McGowan
| distributor    = Sony Pictures
| released       = January 17, 2006
| runtime        = 88 minutes English Filipino Filipino Spanish Spanish
| budget         =
| gross          =
| music          = Mel Lewis
| cinematography = Andrea V. Rossotto
| awards         =
}}
 
The Hunt for Eagle One is a 2006 film. The story takes place during  , came out on DVD, some months later.

==Plot summary==
After a successful amphibious insertion, the Marines begin to prepare for combat against the local rebels in the Philippines. While making a routine fly-by, a UH-1 Huey helicopter, carrying some Philippine troops and Marines, is shot down by the rebels. Of the occupants is Captain Amy Jennings (USMC) and Major Aguinaldo (Ricardo Cepeda). Jennings and Aguinaldo escape the wreckage, and try to flee from the pursuing rebels.

Meanwhile, a rescue team is dispatched to save Jennings and Aguinaldo but is shot down by AA fire. Lt. Daniels (played by Mark Dacascos) and his fireteam continue onward to find Captain Jennings and Major Aguinaldo.

Jennings and Aguinaldo are captured by the rebels, and taken to a village. There, Jennings tries to escape, but is caught. The following morning, Aguinaldo is executed by the rebel leader. They take Jennings to their headquarters.

Gen. Frank Lewis (Rutger Hauer) is in charge of the Marine Corps on the island, and receives hell from the higher ranking Marine Corps officials, regarding the captured Jennings.

The Rescue team approaches the village, and learns that the rebels have moved Jennings to their HQ. They begin following the rebels. Jennings is forced by the rebels to tell the United States (through a video camera) that the Marines must leave the island; but she refuses. In response, the rebels torture her.
 incendiaries in the lab) in an anthrax lab to eliminate the threat of chemical warfare. They escape from the blast. But the rebel leader survives and crawls out and curses them and raises his gun. Jennings shoots him dead. The film ends with Lt. Daniels and a Filipino attached to him mourning the loss of their soldiers on a beach.

==Cast==
*Mark Dacascos as Lieutenant Matt Daniels
*Theresa Randle as Captain Amy Jennings
*Rutger Hauer as General Frank Lewis
*Joe Suba as Captain Seth Cooper
*Zach McGowan as Specialist Hank Jackson

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 