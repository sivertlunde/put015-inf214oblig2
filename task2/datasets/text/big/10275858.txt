Viennese Nights
{{Infobox film name           = Viennese Nights image          = VienneseNights1930.jpg producer       = Richard Rodgers director       = Alan Crosland writer         = Oscar Hammerstein II Sigmund Romberg starring  Alexander Gray Vivienne Segal Walter Pidgeon Jean Hersholt Louise Fazenda music          = Oscar Hammerstein II Sigmund Romberg cinematography = James Van Trees (Technicolor) editing        = Hal McLaren distributor    = Warner Bros. released       =   runtime        = 102 minutes language       = English country        = United States
}}
 Alexander Gray and Walter Pidgeon. 

==Synopsis==
 
The film begins in Vienna in the year 1890 and we find that Walter Pidgeon, Alexander Gray and Bert Roach, who are three close friends, are going to join the Austrian army. Eventually, Pidgeon become a lieutenant and as a superior officer he is forced to distance himself from his two former friends. Gray and Pidgeon end up falling in love with a poor girl, played by Vivienne Segal, who is the daughter of a cobbler. Although Segal truly loves Gray, she chooses to marry Pidgeon because of his wealth and position, believing that money and the social mobility that goes with it will bring her happiness. Gray is heartbroken and travels to the United States with his friend Roach. Gray gets a job playing violin in an orchestra but struggles to support his wife and child. In the course of time, Segal travels to the United States and meets Gray and their love is rekindled. Gray learns of Segals unhappy marriage and they plan to make a new life together. Segal, however, discovers that Gray is married and has a child. Feeling sorry for Grays son, she sacrifices her happiness and returns to Pidgeon, her husband. The film now progresses forty years in time to the year 1930. Segal is now a grandmother and she is planning for her granddaughter, played by Alice Day, to marry a wealthy man since the familys fortunes are now on the wane. Day, however, falls in love with a composer, who happens to be the grandson of Gray. Segal immediately recalls her romance with Gray and of the mistake she once made. She consents to her granddaughters marriage and reminiscences about the man she really loves, who is now dead. One day after the wedding, while at the park, Segal sees Gray and her spirit walks off with him and leaves her body. The film ends as she is finally reunited with her long lost love.

==Songs==
* "You Will Remember Vienna"
* "I Bring a Love Song"
* "When You Have No Man To Love"
* "Goodbye My Love"
* "Here We Are"
* "Im Bringing You Bad News"
* "Im Lonely"
* "Oli, Oli, Oli"
* "Ottos Dilemma"
* "Poem Symphonic"
* "Pretty Gypsy"
* "The Regimental March"

==Cast== Alexander Gray as Otto Stirner
* Vivienne Segal as Elsa Hofner 
* Walter Pidgeon as Franz von Renner 
* Jean Hersholt as Herr Hofner, Elsas Father 
* Louise Fazenda as Gretl Kruger 
* Bert Roach as Gus Sascher 
* Alice Day as Barbara, Elsas Granddaughter 
* Philipp Lothar Mayring as Baron von Renner, Franz Father (as Lothar Mayring) 
* June Purcell as Mary, a Singer on the Stage (as June Pursell) 
* Milt Douglas as Bill Jones, a Stage Performer (as Milton Douglas) 
* Freddie Burke Frederick as Otto Stirner Jr.
* Dorothy Hammerstein as Socialite next to Elsa in theatre box  Ullrich Haupt as Hugo, Elsas Rejected Suitor
* Isabelle Keith as Franz Rejected Girlfriend 
* Bela Lugosi as Count von Ratz, Hungarian Ambassador 
* Russ Powell as Herr Schultz 
* Virginia Sale as Emma Stirner
* Mary Treen as Shocked Woman on Street
* Paul Weigel as Man in Vienna Opera Box
* Biltmore Trio as Trio in 1930 Nightclub Singing "Here We Are"

==Production==
Viennese Nights was the first of four original screen musicals that the team of Sigmund Romberg and Oscar Hammerstein II were to create for Warner Brothers over a two-year period. They were to be paid $100,000 a piece per film against 25 percent of the profits. This deal was made early in 1930, before anyone realized that the Great Depression would be imminent later that year. These economic problems caused studios to stay away from the lavish spectacle of musicals which were now seen as frivolous and anachronistic. Under these circumstances, Warner Brothers were forced to buy out the contract they had signed with the Romberg-Hammerstein team, early in 1931, after their second musical Children of Dreams (1931), which had already been produced, had been released to dismal reviews.
 The Cat Alexander Grays last starring role in a feature.

Among the players, Bela Lugosi makes his first appearance in color in this feature in a bit part as a Hungarian ambassador named Count von Ratz. Lugosis part was filmed before his claim to fame as the title role in Dracula (1931 English-language film)|Dracula for Universal Pictures.
 The Phantom of the Opera.  Viennese Nights marks the third time the set was photographed in Technicolor, the first two being Phantom and The King of Jazz.

==Preservation==
Unlike most Warner Brothers early Technicolor films (with a huge number of them existing either only in black and white or are lost completely), Viennese Nights still survives in color. The film survived as a single nitrate Technicolor print in Jack Warners personal vault on the Burbank lot, and transferred to the UCLA Film and Television Archive, along with the nitrate collection of studio prints. A full set of Vitaphone sound discs was discovered at Warner Bros. in 1988. Additionally, the Vitaphone discs for the Foreign Version (non-dialogue, but English-language songs and musical underscore) has also survived but without picture. The domestic version has been preserved by the UCLA Film and Television Archive, including the overture, intermission, and exit music. The Archive has created a 35mm Eastmancolor preservation negative from that print and the restored showprint has played archival engagements around the world. At the present time the film cannot be shown commercially or on Home Video, without the underlying rights being re-negotiated by the current copyright owner, Warner Bros.

==See also==
* List of early color feature films

== External links ==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 