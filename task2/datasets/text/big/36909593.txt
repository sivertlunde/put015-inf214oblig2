Kronjuvelerna
{{Infobox film
| name           = Kronjuvelerna
| image          = 
| caption        = 
| director       = Ella Lemhagen
| producer       = Lars Blomgren Peter Bengtsson Jessica Ask Gunnar Carlsson Tomas Michaelsson Christian Wikander Gabija Siurblyte Lone Korslund
| writer         = Ella Lemhagen Carina Dahl
| starring       = Alicia Vikander Bill Skarsgård Loa Falkman
| music          = 
| cinematography = Anders Bohman
| editing        = 
| studio         = Filmlance
| distributor    = 
| released       = 
| runtime        = 120 minutes
| country        = Sweden Swedish
| budget         =  
| gross         = 
| preceded_by    = 
| followed_by    = 
}}

Kronjuvelerna (literally: The Crown Jewels) is a Swedish drama film directed by Ella Lemhagen based upon a story by Carina Dahl. It stars Alicia Vikander and Bill Skarsgård.

==Plot==
Fragancia is arrested for the attempted murder of Richard Persson, the son of a powerful factory owner. 

==Cast==
*Alicia Vikander as Fragancia Fernandez
*Bill Skarsgård as Richard Persson
*Loa Falkman as Factory Owner Persson
*Michalis Koutsogiannakis as Fernandez Fernandez
*Jesper Lindberger as Jesus Fernandez
*Alexandra Rapaport as Marianne Fernandez
*Natalie Minnevik as Belinda
*Björn Gustafsson as Pettersson-Jonsson
*Amanda Junegren as Young Fragancia Fernandez
*Jonatan Bökman as Young Richard Persson
*Noah Byström as Young Pettersson-Jonsson
*Michael Segerström as Father Hjalmar Timbuktu as Remmy
*Kjell Wilhelmsen as Butcher Jonsson
*Tomas von Brömssen as Commissioner Samnerud
*David Lenneman as Goldie
*Martin Eliasson as Karsten
*Nour El-Refai as Midwife

 

== References ==
 

==External links==
* 

 
 
 
 

 