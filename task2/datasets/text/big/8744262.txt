Miss Julie (1951 film)
{{Infobox film
| name           = Miss Julie
| image          = Missjulie1951.jpg
| caption        = Theatrical release poster
| director       = Alf Sjöberg
| producer       = Rune Waldekranz
| screenplay     = Alf Sjöberg
| based on       =  
| starring       = Anita Björk Ulf Palme
| music          = Dag Wirén
| cinematography = Göran Strindberg
| editing        = Lennart Wallén AB Sandrew-Produktion
| distributor    =
| released       =  
| runtime        = 89 minutes
| country        = Sweden
| language       = Swedish
| budget         =
}} play of the same name by August Strindberg. The film deals with class, sex and power as the title character, the daughter of a Count in 19th century Sweden, begins a relationship with one of the estates servants. The film won the Grand Prize at the 1951 Cannes Film Festival.   

==Plot==
In spite of being the daughter of a count, Julie had an unhappy childhood. She is haunted by according memories. When her engagement fails, shes even more depressed than usual. A servant offers solace and takes advantage of her. She succumbs to his seductive manners, yet right after this incident she takes her own life.

==Cast==
* Anita Björk as Miss Julie
* Ulf Palme as Jean
* Märta Dorff as Kristin, cook 
* Lissi Alandh as Countess Berta, Julies mother 
* Anders Henrikson as Count Carl, Julies father 
* Inga Gill as Viola 
* Åke Fridell as Robert 
* Kurt-Olof Sundström as Julies fiancé 
* Max von Sydow as hand 
* Margaretha Krook as governess
* Åke Claesson as doctor 
* Inger Norberg as Julie as a child 
* Jan Hagerman as Jean as a child

==Production==
Sjöberg had directed a stage adaption of August Strindbergs Miss Julie in 1949, starring Ulf Palme as Jean and Inga Tidblad in the title role. For the film adaptation, not only Palme was kept but also many of the set designs. Tidblad however, at the age of almost 50, was replaced by the 27-year-old Anita Björk. Tidblads interpretation was held as an ideal by the director and only during the exterior sequences, which had not been part of the stage version, did Björk feel that the part actually belonged to her.  

Filming took place between 28 April and 18 July 1950 in Sandrew Metronome|Sandrews studios and various locations in the Stockholm area, including Dalarö, Stora Wäsby Castle, and the Drottningholm Palace Park. 
 I Confess in 1952, after seeing her in Miss Julie. However, when Björk arrived in Hollywood with her lover Stig Dagerman and their baby, Jack Warner, head of Warner Brothers. insisted that Hitchcock find another actress.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 


 
 