The Invisible Army
 
{{Infobox film
| name           = The Invisible Army
| image          = 
| image_size     = 
| caption        = 
| director       = Johan Jacobsen
| producer       = Tage Nielsen
| writer         = Knud Sønderby
| starring       = Bodil Kjer
| music          = 
| cinematography = Karl Andersson Rudolf Frederiksen	
| editing        = Edith Schlüssel
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

The Invisible Army ( ) is a 1945 Danish war film directed by Johan Jacobsen and starring Bodil Kjer.

==Cast==
* Bodil Kjer - Alice
* Ebbe Rode - Jørgen
* Mogens Wieth - Poul
* Maria Garland - Alices mor
* Poul Müller - Michelsen
* Asbjørn Andersen - Direktøren Henry Nielsen - Portner på Metrodan
* Jakob Nielsen - Værkfører på Metrodan
* Ole Monty - Betjent
* Svend Methling - Præst Lau Lauritzen - Tysk Kriminalrat
* Edvin Tiemroth - Modstandsmand
* Buster Larsen - Modstandsmand
* Kjeld Petersen - Modstandsmand
* Kjeld Jacobsen - Mand der fortæller rygte videre
* Preben Mahrt - Mand der fortæller rygte videre
* Gyrd Løfquist
* Karl Jørgensen
* Steen Gregers
* Valdemar Skjerning - Politimester
* Ellen Margrethe Stein - Georges mor
* Sigurd Langberg - Georges far
* Aage Foss
* Carl Johan Hviid - Viktor Emilius Sørensen
* Poul Reichhardt - Modstandsmand

==External links==
* 

 
 
 
 
 
 
 


 