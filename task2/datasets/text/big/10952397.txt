The Statue (1971 film)
{{Infobox Film
| name           = The Statue 
| image          = Original movie poster for the film The Statue.jpg
| image_size     = | caption        = 
| director       = Rodney Amateau
| producer       = Anis Nohra executive Josef Shaftel
| writer         = Denis Norden Alec Coppel  based on = Chip, Chip, Chip, a play by Alec Coppel
| narrator       = 
| starring       = David Niven Virna Lisi Robert Vaughn Ann Bell
| music          = Riz Ortolani
| cinematography = Piero Portalupi
| editing        = Ernest Hosler  studio = Josef Shaftel Productions
| distributor    = Cinerama Releasing
| released       = 1971
| runtime        = 84 minutes gross = 40,890 admissions (France) 205,231 admissions (Spain) 
| country        = United Kingdom Italy
| language       = English
}} Italian beauty Virna Lisi in the key roles. Monty Pythons John Cleese and Graham Chapman appear an early roles as the Niven characters psychiatrist and a newsreader respectively.  Niven plays a nobel-prize winning professor who suspects his wife, played by Lisi, of infidelity when she makes and unveils an 18-foot statue of him with private parts recognisably not his own.  Critical and audience reception of the film was poor, though Niven was praised for Nivens efforts to sustain the film as the main character. 

==Plot==
Professor Alex Bolt has developed a new universal language, Unispeak, which has made him internationally famous. His wife Rhonda has made a sculpture of her husband at the behest of the US State Department, commissioned by his friend, US Ambassador to England, Ray, for $50,000, in order to promote Unispeak. It is intended that the sculpture be unveiled in Londons Grosvener Square.

The sculpture is an 18-foot nude one of Alex. He is upset and tries to get it suppressed, especially when he notes every aspect of the statue resembles him except for the size of its penis. Rhoda points out that she has only seen Alex eighteen days in the past three years. Alex becomes convinced Rhonda has had an affair and based the size of the genitalia on the model, who he dubs "Charlie".

Alex seeks advice from his friend Harry, an advertising man trained as a psychiatrist. He tries to track down the model of the statue in order to get it to suppressed. He interrogates a household employee, Joachim, who thinks Alex is hitting on him and beats him up. Alex then goes to a Turkish bathhouse to interview possible Charlies, but is thrown out.

Harry suggests that Alex forget about it, which he tries to do and he apologies to Rhonda. However the thought of Charlie causes him to be impotent. This leads to a fight with Rhonda and Alex resumes his search for Charlie.

Ray then sees the statue and becomes concerned about it having a bad effect on his reputation. He arranges for the statue to be stolen, which Rhonda blames on Alex.

Alex eventually discovers the model was the statue of David by Michelangelo. 
Rhonda ends up making a new statue based on Ray.

==Cast==
* David Niven as Alex Bolt 
* Virna Lisi as Rhonda Bolt 
* Robert Vaughn as Ray 
* Ann Bell as Pat 
* John Cleese as Harry 
* Tim Brooke-Taylor as Hillcrest 
* Hugh Burden as Sir Geoffrey 
* Erik Chitty as Mouser 
* Derek Francis as Sanders 
* Susan Travers  as Mrs. Southwick

==Songs==
*"Charlie" by the Statuettes - lyrics by Norman Newwell, music by Riz Ortolani
*"Skin" Sequence - choreography by Gia Landi, lyrics by Audrey Nohra, music by Luis Enriquez Bacalov

==Production==
Dyan Cannon and Robert Culp were originally announced as supporting David Niven. Henry Fondas new series
NORMA LEE BROWNING. Chicago Tribune (1963-Current file)   20 Mar 1970: b19. 

Filming began in Rome on 1 May 1970 at Cinecitta Studios. Niven Statue Is Unveiled
Los Angeles Times (1923-Current File)   26 May 1970: f14.  MOVIE CALL SHEET: Amateau Will Direct Statue
Martin, Betty. Los Angeles Times (1923-Current File)   30 Jan 1970: e15. 

"Its a fun role, in a fun picture," said Virna Lisi. Virna Lisi: Italian Actress, Housewife and Evolutionized Sexpot
ABA, MARIKA. Los Angeles Times (1923-Current File)   19 July 1970: r18. 

==Reception==
The Los Angeles Times called it a "silly, strained farce." MOVIE REVIEW: Statue Features Virna Lisi, Niven
Thomas, Kevin. Los Angeles Times (1923-Current File)   29 Jan 1971: g10. 

==References==
 

== External links ==
*  
*  at TCMDB
*  from Roger Ebert
*  at New York Times
 

 
 
 
 


 