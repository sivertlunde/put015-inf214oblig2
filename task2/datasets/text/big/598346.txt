Capturing the Friedmans
 
 
{{Infobox film name            = Capturing the Friedmans image           = Capturing the Friedmans poster.jpg caption         = director        = Andrew Jarecki producer        = Andrew Jarecki Marc Smerling writer          = starring        = Arnold Friedman Elaine Friedman David Friedman Jesse Friedman music           = Bill Harrington Andrea Morricone cinematography  = Adolfo Doring editing         = Richard Hankin distributor     = Magnolia Pictures released        =   runtime         = 107 min. country         = United States language        = English}}

Capturing the Friedmans is an HBO documentary film directed by Andrew Jarecki.  It focuses on the 1980s investigation of Arnold and Jesse Friedman for child molestation. It was nominated for the Academy Award for Documentary Feature in 2003.   

Some of the Friedmans alleged victims and family members wrote to the Awards Committee protesting the nomination, their identities confirmed but protected by the judge who presided over the court case.   

==History==
Jarecki initially was making a short film, which he completed, about childrens birthday party entertainers in New York (Just a Clown), including the popular clown David Friedman (Silly Billy). During his research, Jarecki learned that David Friedmans brother, Jesse, and his father, Arnold, had pled guilty to child sexual abuse, and the family had an archive of home movies. Jarecki interviewed some of the children involved and ended up making a film focusing on the Friedmans. 

==Summary==
The investigation into Arnold Friedmans life started after the U.S. Postal Service in 1987 intercepted a magazine of child pornography received from the Netherlands. In searching his Great Neck, New York home, investigators found a collection of child pornography. After learning that Friedman taught children computer classes from his home, local police began to suspect him of abusing his students.

In police interviews, some of the children Friedman taught stated Friedman played bizarre sex games with them during their computer classes. Jarecki interviewed some of these children himself; some stated that they had been in the room with other children alleging abuse, and that nothing had happened. The film portrayed police investigative procedures as the genesis of a "witch-hunt" in the Friedmans community. The charges assume that abuse took place with multiple children, over an extended period, yet none of them ever told anything of it to anyone, nor were they in distress when parents arrived to pick them up from the computer classes.

The Friedmans took home videos while Arnold Friedman (and, later, his son Jesse) awaited trial. They were allowed to stay at home in order to prepare for court. The videos were not made with publishing in mind, but as a way to record what was happening in their lives. The movie shows much of this footage: family dinners, conversations, and arguments. Arnolds wife quickly decided that her husband was indeed guilty and advised him to confess and protect their son; she soon divorced him.
 pled guilty prison for mitigation that his father had molested him. According to Jesses lawyer Peter Panaro, who visited Arnold in a Wisconsin federal prison, Arnold admitted molesting two boys, but not those who attended his computer classes. He is also quoted as claiming that, when he was 13, he had sex with his younger brother, Howard, who was eight years old at the time of the abuse; Howard Friedman, interviewed in the movie, says he does not recall this. Jesse Friedman, in a subsequent statement, said that his father told him and his brothers of his having sex with his younger brother. 

Arnold Friedman committed suicide in prison in 1995, leaving a $250,000   in 2001 after serving 13 years of his sentence. Currently, he is running an online book-selling business. 

==Response== archetypal figures in the Friedman home that he knows to push things any further through heavy-handed assessment would be redundant." He praised Jarecki for operating under the premise "that first impressions cant be trusted and that truth rests with each person telling the story." 

  for 2003.  Capturing the Friedmans was voted the fifth most popular film in the Channel 4 programme, The 50 Greatest Documentaries of all time, in 2005.

In one of the few negative reviews, Los Angeles Times writer Kenneth Turan wrote a critique of both the film and Jarecki stating, "Jareckis pose of impartiality gets especially troublesome for audiences when it enables him to evade responsibility for dealing with the complexities of his material." 
 Sundance in January, he was struck by how they were split over Arnold and Jesses guilt. Since then, hes crafted a marketing strategy based on ambiguity, and during Q&As and interviews, he has studiously avoided taking a stand." 

==Auxiliary Materials==
The 2003 DVD release included a second DVD: "Capturing the Friedmans - Outside the Frame". It included:
* Unseen home movies ("Passover Seder", "Grandma Speaks", "Jesses Last Night")
* Great Neck Outraged.
* New Witnesses and Evidence.
* Uncut footage of the prosecutions star witness.
* Friedman family scrapbook and hidden audio tapes.
* Just a Clown (the movie with David Friedman that led to the whole Friedman project).
* Jesses Life Today.
* An altercation at the films New York premiere. (The retired head of the Nassau County Polices Sex Crimes Unit, Frances Galasso, speaks.)
* The Judge (Abbey Boklan) speaks out at the Great Neck premiere.
* A ROM section with key documents from the family and the case.

The most important of these materials were from the premiere, in a discussion period after which Galasso got in an altercation with Debbie Nathan, and from the showing in Great Neck, at which the trial judge Abbey Boklan was present. Both made the claim that the film had ignored relevant evidence of Jesses guilt. This included his appearance on the Geraldo Rivera show, when Jesse confessed his guilt, and the fact that there was another defendant (Ross Goldstein), who turned states evidence and pled guilty (he is not named, much less interviewed, in the film), and two other unindicted boy co-conspirators. Jesses lawyer at the time, Peter Panaro, said that he had advised Jesse not to appear on Geraldo Rivera (Panaro was also present on the show), and in fact had Jesse sign an affidavit saying that he was doing so against legal advice.

== Subsequent legal developments ==
In August 2010, a federal appeals court upheld the conviction of Jesse Friedman on technical legal grounds, {{Cite web
| url = http://caselaw.findlaw.com/us-2nd-circuit/1535102.html | title = FRIEDMAN v. REHAL | accessdate = June 16, 2013 | author = US Court of Appeals, Second Circuit | date = August 17, 2010 | publisher = Findlaw}}  but took the unusual step of urging prosecutors to reopen Friedman’s case, saying that there was a “reasonable likelihood that Jesse Friedman was wrongfully convicted”.  The decision cited "overzealousness" by law enforcement officials swept up in the hysteria over child molestation in the 1980s.

Following the appeals court ruling, the Nassau District Attorneys office began a three-year investigation led by District Attorney  , a founder of the Innocence Project and one of the country’s leading advocates for overturning wrongful convictions and a member of OJ Simpsons defense team.  However, Scheck has subsequently complained that key documents were not available to the panel, and urged the matter be reopened. 

Prior to the reports release, details emerged, including letters from some of the alleged victims in which they recant their accusations and implicate the police in coercing their statements.  Prior to the reports release, The Village Voice conducted an interview with Jesse Friedman,  who described himself as "freakishly optimistic", and also reported that Ross Goldstein, a childhood friend of Jesse Friedmans, had broken his 25-year silence to explain he had been coerced into false cooperation with the district attorneys office: "He told the review panel of how hed been coerced into lying, how prosecutors coached him through details of the Friedmans computer lab, which hed never even seen, and how he was imprisoned for something hed never done." 

On February 10, 2015, Jesse Friedman was back in state appellate court seeking to have Nassau County prosecutors turn over to him the remainder of their evidence against him, which they have not done despite a 2013 court order telling them to do so. 

== See also ==
* McMartin preschool trial
* Day-care sex-abuse hysteria

== References ==
 

== External links ==
*  
*  
*  
*  , verified 10/19/2014.

 

 
 
 
 
 
 
 
 
 
 