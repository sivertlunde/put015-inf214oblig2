Jackson: My Life... Your Fault
{{Infobox Film |
  name=Jackson: My Life... Your Fault|
  image=|
  caption=|
    writer=Duncan Roy|
  starring=Benjamin Soames Alan Gilchrist|
  director=Duncan Roy|
  producer=|
  distributor= British Film Institute|
  released=1995|
  runtime=41 min.|
  language=English|
  movie_series_label=Series:|
  movie_series=|
  music= |
    budget= |
}}
Jackson: My Life... Your Fault is a 1995 gay-themed film directed by Duncan Roy starring Benjamin Soames and Alan Gilchrist.

==Plot introduction==
Jackson (Benjamin Soames) has lived sheltered with his mother since his fathers death when he was a little boy. He remembers the event from childhood, and is still troubled by it. His mother (superbly played by the excellent actress Georgina Hale), is an over-protective type, who deliberately plays upon Jacksons emotions to keep him tied to her. On meeting an attractive policeman, he needs to decide whether to grow up or not.

Quite graphic in its depiction of gay lifestyle between the leads, Jackson also has to consider his love for another man, and his lifestyle.

==External links==
* 

 
 
 


 
 