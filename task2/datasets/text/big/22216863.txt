Bliss (2007 film)
{{Infobox film
| name           = Mutluluk (Bliss)
| image          = 
| director       = Abdullah Oğuz
| producer       = Abdullah Oğuz
| writer         = Kubilay Tunçer  Zülfü Livaneli
| starring       = Özgü Namal  Talat Bulut   Murat Han   Lale Mansur
| music          = Zülfü Livaneli
| cinematography = Mirsad Herović
| editing        = Levent Çelebi
| distributor    = Kenda Film
| released       = 2007
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
| preceded_by    =
| followed_by    =
}} Turkish film directed by Abdullah Oğuz, and starring Özgü Namal, Talat Bulut, and Murat Han. It was chosen to open the 2007 Asian Film Festival in Mumbai  and the 2007 Medfilm Festival in Rome.  It won the audience prize and the young jury prize at the 2008 Turkish Film Festival in Nuremberg    and won the audience award for international films at the 2008 Miami International Film Festival. 
Also, Mutluluk was rewarded by European Council with the Prix Odyssee Human Rights Award in 2007.

==Plot==
Meryem (Özgü Namal), a young woman of about 17 years old, is believed to have been violated, and her villages customs call for her to be killed to restore honor and dignity to her family and village. A young, newly returned war veteran and son of the village leader, Cemal (Murat Han), is ordered to take Meryem to Istanbul and kill her, but at the last minute he doesnt allow himself to complete the task and the two, now unable to ever return to their village, run away together. Eventually, they meet up with ex-Professor Irfan, a man who has decided to live on a boat away from his wife and constricted life.  The movie is an interesting study of a theme that is common of many Muslim societies in the Middle East.  In the end the real truth comes out and there is a surprise ending.

This movie is adapted from the international best selling novel by Zülfü Livaneli. 

== References ==
 

==External links==
* 

 
 
{{succession box
| title=Yeşilçam Award#Best Film Award|Yeşilçam Best Film Award
| years=2008
| before=-
| after=Three Monkeys}}
 

 
 
 
 