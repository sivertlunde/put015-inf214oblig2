Never Take Sweets from a Stranger
 
 
{{Infobox film
| name           = Never Take Sweets from a Stranger
| image          = Nevertakesweets.jpg
| caption        = UK release poster
| director       = Cyril Frankel
| producer       = Anthony Hinds
| writer         = John Hunter (from play by Roger Garis)
| starring       = Patrick Allen Gwen Watford Felix Aylmer
| music          = Elisabeth Lutyens
| cinematography = Freddie Francis
| editing        = Alfred Cox, James Needs
| studio         = Hammer Film Productions
| distributor    = Columbia Pictures
| released       = 4 March 1960
| runtime        = 81 min.
| aspect ratio   = 2.35:1
| process        = MegaScope
| country        = United Kingdom
| language       = English
| budget         =
}}

Never Take Sweets from a Stranger (US Never Take Candy from a Stranger) is a 1960 British film, directed by Cyril Frankel and released by Hammer Film Productions. The screenplay was developed by John Hunter from the play The Pony Trap by Roger Garis. It stars Patrick Allen, Gwen Watford and Felix Aylmer, the latter being cast notably against type. The twin themes are paedophilia and the sexual abuse of children, and the way in which those with sufficient pull can corrupt and manipulate the legal system to evade responsibility for their actions. The film is regarded as bold and uncompromising for its time in the way in which it handles its subject matter.

==Synopsis==
The film is set in a small Canadian town to which the British Carter family (Peter, Sally and 9-year-old daughter Jean) have just moved, following Peters appointment as school principal. One night Jean appears restless and disturbed, and confides to her parents that earlier that day while playing in a local wood, she and her friend, Lucille, went into the house of an elderly man who asked them to remove their clothes and dance naked before him in return for some candy, which they did and Jean doesnt believe they did anything wrong. But her parents are appalled by what they hear and decide to file a complaint. The accused man, Clarence Olderberry Sr., is however the doyen of the wealthiest, most highly regarded and influential family in town and matters conspire to turn against the Carters as the townspeople start to close rank against the newcomers. The police chief casts doubt on Jeans story, while Olderberrys son warns the Carters that if they pursue the matter through the legal system, he will ensure that Jeans evidence and trustworthiness will be torn to shreds in court.

When the case come to trial, it is with an obviously stacked jury and in an atmosphere of extreme hostility towards the Carters. As threatened, the Defense Counsel proceeds to question Jean in a harrying, bullying manner which leaves her confused, frightened and giving the impression of an unreliable witness. Inevitably Olderberry is acquitted.

The Carters realise that there can be no future for them in the town, and make plans to leave. Shortly before their departure, Jean rides her bicycle and meets Lucille. They are in the wood again when they see Olderberry approaching them, offering them a bag of sweets. He grabs hold of Jeans bicycle. This time forewarned, the girls run away in panic and come to a lake, dropping Lucilles shopping bag on the way, and they find a rowboat in which they attempt to flee to the other side of the lake. The boat is however still moored to the lakeshore, and Olderberry begins to pull it back in.
 SAR dogs lead the police to a cabin, where Lucille is lying dead on the floor, and Olderberry Sr. is there, behaving strangely, his clothes are disarrayed, insane expression on his face. Olderberry Jr. gazes shocked at his father, realising the girls were telling the truth.

While Sally waits anxiously at home, word has spread all over town about the search for the girls and Olderberry. Many of the town residents gather in front of the Carters house. The police brings Peter and Jean. Peter tells Sally that Jean managed to get away unharmed from Olderberry Sr. and was found wandering in the wood on the other side of the lake. Sally asks what happened to Lucille. Before Peter can answer, Olderberry Jr. approaches them, overwhelmed with guilt and remorse. He whimpers and repeatedly exclaims that his father killed Lucille while the crowd listens to him silently. The police takes him away and the crowd disperses.

==Cast==
* Patrick Allen as Peter Carter
* Gwen Watford as Sally Carter
* Janina Faye as Jean Carter
* Felix Aylmer as Clarence Olderberry Sr.
* Niall MacGinnis as Defense Counsel
* Michael Gwynn as Prosecutor
* Alison Leggatt as Martha
* Bill Nagy as Clarence Olderberry Jr.
* MacDonald Parke as Judge
* Estelle Brody as Eunice Kalliduke
* Robert Arden as Tom Demarest
* Frances Green as Lucille
* John Bloomfield as Foreman of Jury

==Reception==
On its original release, the film made little impact at the box-office and its press was mainly negative. This was partly because at the time the issue of paedophilia and child sexual abuse was a great taboo, rarely referred to or spoken about, and merely to produce a film dealing openly with the issue was deemed sordid and distasteful. Another hindrance to commercial success was that the film was far from easy to categorise, so it was difficult to market to any specific film audience demographic. In terms of genre it had elements of suspense, horror, courtroom drama and social commentary, but did not fit neatly into any general classification. In addition some of the publicity chosen for the film &ndash; such as a promotional poster with an image of armed police with tracker dogs, and the tagline "A nightmare manhunt for maniac prowler!" &ndash; was misleading as it implied a fugitive-on-the-run chase thriller. Hammer Studios boss   for example saying: "Gwen Watford and Patrick Allen, as the distraught parents, and Alison Leggatt, as a wise, understanding grandmother, lead a cast which is directed with complete sensitivity by Cyril Frankel. Both Watford and Allen are completely credible while Leggatt, well-served by John Hunters script, is outstanding.  Aylmer, who doesnt utter a word throughout the film, gives a terrifying acute study of crumbling evil."   The film quickly disappeared from view, and for many years remained little-known and rarely screened. Indeed no indication can be found that it was ever shown on British television.

By the 1990s, at a time when a general reassessment and re-evaluation of Hammers back catalogue, including its more obscure entries, was under way, critics and aficionados revisited Never Take Sweets from a Stranger with fresh eyes, and found a brave, honest and in some ways groundbreaking film. In 1994, Hammer denizen Christopher Lee noted: "Never Take Sweets from a Stranger, an excellent film, was decades ahead of its time."  Its reputation has continued to improve, and in 2010 the film made its first appearance on DVD, along with five other elusive and sought-after Hammer rarities, in a US triple DVD collection called Icons of Suspense. 

==Location filming==
Despite its nominal Canadian setting, exterior filming for Never Take Sweets from a Stranger took place at Black Park in Wexham, Buckinghamshire. Black Park was featured in numerous Hammer productions due to its atmospheric appearance on film and its proximity to Hammers Bray Studios base. 

==Alternate dialogue==
Aside from the alternative title, there is some mild swearing in the original British Sweets prints. It features a line from Patrick Allens character around the ten minute mark: "If he touched her, I swear Ill kill the bastard." In the US Candy prints (and the 2010 Icons of Suspense DVD), the word swine is used instead on the audio instead, also recorded by Allen, although the picture remains the same and he can clearly still be seen to say bastard.

==References==
 

== External links ==
*  
*  
*   at Eccentric Cinema

 

 
 
 
 
 
 
 
 
 
 