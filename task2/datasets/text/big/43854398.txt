Butter on the Latch
{{Infobox film
| name           = Butter on the Latch
| image          = 
| caption        = 
| director       = Josephine Decker
| producer       = Josephine Decker Laura Heberton (co-producer) Rachel Wolther (co-producer)
| writer         = Josephine Decker
| starring       = Isolde Chae-Lawrence Stephan Goldbach Charlie Hewson Sarah Small Yury Yakor
| music          = 
| cinematography = Ashley Connor
| editing        = Josephine Decker
| distributor    = 
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Butter on the Latch is a 2013 experimental psychological thriller/drama film written, produced, and directed by Josephine Decker.

==Cast==
*Isolde Chae-Lawrence as Isolde
*Stephan Goldbach as Guy
*Charlie Hewson as Steph
*Sarah Small as Sarah
*Yury Yakor as Guy

==Release==
===Media=== VOD distribution by Cinelicious Pics along with Deckers 2014 film Thou Wast Mild and Lovely with a November 2014 release.  

==Reception==
===Critical response===
Butter on the Latch received a mostly positive response from critics.  It currently hold a 78% positive rating at the review aggregator Rotten Tomatoes. 

Jenni Miller of The A.V. Club praised the film, stating "The different techniques Decker uses—the improvised dialogue that feels like listening to one side of a phone conversation, the woozy cinematography and sound design, the disorienting editing—create a sense of claustrophobia. The film’s world is beautiful and scary, but also as intimate as a childhood sleepover."   Nicolas Rapold of The New York Times opined that "Butter on the Latch thrives on its casually true snapshots of confusion and connection."   Eric Kohn of Indiewire also praised the film, commenting that Decker "...never tries to impose a conventional narrative structure on the proceedings, and the ambiguities develop their own bizarrely compelling rhythm" and that her career is "one to keep an eye on."   Peter Debruge of Variety (magazine)|Variety noted that "... Decker has fashioned the kind of feature debut the film industry simply doesn’t support, but would do well to encourage: a visually poetic, virtually free-form groove in which emotion, rather than narrative, guides viewers through a young woman’s visit to a Balkan folk music camp." 

Conversely, Leslie Felperin of The Hollywood Reporter described the film as a "pretentious low-budget effort" and added, "With her fitfully charming short Me The Terrible, Decker has shown she knows how to craft a conventional film. It’s clear that her blender-style editing technique and oblique approach to narrative, along with Connor’s blurry, randomly framed cinematography, are artistically motivated decisions which they have every right to make. Some of those decisions, if made with discretion and restraint, might have been quite effective. But slapped altogether like this, the film just looks a mess, apart from some of the rather pretty shots of banana slugs and redwoods." 

Richard Brody of The New Yorker later named Butter on the Latch the tenth best film of 2014.     Deckers other 2014 film, Thou Wast Mild and Lovely, also made Brodys top ten, coming in second place to Wes Andersons The Grand Budapest Hotel. 

==References==
 

==External links==
* 

 
 
 
 
 
 