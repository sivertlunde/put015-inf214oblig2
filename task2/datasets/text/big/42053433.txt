Our School's E.T.
{{Infobox film name           = Our Schools E.T.  image          =  director       = Park Kwang-chun producer       = Choi Yong-gi   Park Bong-su writer         = Choi Jin-won story          = Lee Hyeon-cheol starring  Lee Min-ho   Moon Chae-won   Lee Chan-ho  music          = Choi Man-sik cinematography = Choi Deok-kyu    editing        = Shin Min-kyung studio         = Courage Films distributor    = SK Telecom Finecut (international) released       =   runtime        = 120 minutes country        = South Korea language       = Korean
}} English education, so he decides that becoming an English teacher (E.T.) overnight is his only hope.  

==Plot==
Chun Sung-geun is the gym teacher of a private high school in an upscale district where competition is heated among students to enter top colleges. Called "  classes. The school decides to hire another English teacher—at the cost of a gym teacher. In danger of losing his position, he remembers by chance that he obtained a license to teach English a decade ago. The school board president tells him to pass a test in order for her to acknowledge it. So Sung-geun, who hasnt spoken (much less taught) English for ten years, faces the near-impossible task of becoming an English teacher within two weeks. His only weapon is his amazingly incomparable physical strength, and with the help of a few students who understand his warm heart, the simpleton Sung-geun begins his rigorous training to exercise his brain "muscles" to master English in his journey to become the schools E.T.

==Cast==
*Kim Su-ro as Chun Sung-geun
*Lee Han-wi as Principal Joo Ho-shik
*Kim Sung-ryung as school board director Lee
*Baek Sung-hyun as Baek Jung-goo  
*Park Bo-young as Han Song-yi Lee Min-ho as Oh Sang-hoon    
*Moon Chae-won as Lee Eun-shil
*Lee Chan-ho as Ok Ki-ho
*Kim Hyeong-beom as Cha Seung-ryong, English teacher
*Kim Ki-bang as "Meok-tong" (meaning hard-headed)
*Park Jin-taek as Jin-gook
*Kim Young-ok as Sung-geuns mother
*Kim Byung-ok as president of publishing company 
*Ha Jung-woo as handsome doctor (cameo appearance|cameo)
*Jang Hee-soo as Ki-hos mother 
*Lee Young-yi as Eun-shils mother
*Lee Hyung-kwon as Vice principal
*Gong Ho-seok as Teacher Choi
*Lee Chan-young as Byeong-jin
*Oh Yeon-seo as Soo-jin
*Maeng Bong-hak as Vice principal of countryside school
*Choi Yoon-jung as Yoon-jung
*Jeon In-geol as Kyeong-sam
*Kim Dong-beom as Je-dong
*Min Ji-hyun as Ki-hos seat partner
*Kim Jae-rok as math teacher
*Jung Jong-yeol as music teacher
*Lee Sa-bi as chemistry teacher
*Yoo Jeong-ho as English Kim
*Kim Si-hyang as Sang-hoons girlfriend
*Oh Soon-tae as Jo Ba-nom

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 