Too Many Husbands
{{Infobox film
| name           = Too Many Husbands
| image          = Too Many Husbands - 1940 Poster.png
| caption        = 1940 Theatrical Poster
| director       = Wesley Ruggles
| producer       = Wesley Ruggles
| based on       =  
| screenplay     = Claude Binyon
| starring       = Jean Arthur Fred MacMurray Melvyn Douglas
| music          = M. W. Stoloff Joseph Walker
| editing        = William A. Lyon Otto Meyer
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 

Too Many Husbands (released in the United Kingdom as My Two Husbands) is a 1940 romantic comedy film about a woman who loses her husband in a boating accident and remarries, only to have her first spouse reappear—yet another variation on the 1864 poem Enoch Arden by Alfred, Lord Tennyson. The film stars Jean Arthur, Fred MacMurray and Melvyn Douglas, and is based on the 1919 play Home and Beauty by W. Somerset Maugham, which was retitled to Too Many Husbands when it came to New York.  The movie was directed by Wesley Ruggles.
 Tennyson poem 1911 film 1914 version 1915 film version with Lillian Gish.

A couple of months after Too Many Husbands was released by Columbia, RKO put out a movie the same year that was more popular in its time and better remembered today, My Favorite Wife, a variation on the story with Cary Grant as the remarried spouse whose former wife Irene Dunne returns from sea. Too Many Husbands was remade as a musical, Three for the Show (1955), with Jack Lemmon and Betty Grable. My Favorite Wife came back yet again as Move Over, Darling (1963), with Doris Day and James Garner. 

==Plot==
Vicky Lowndes (Jean Arthur) loses her first husband, Bill Cardew (Fred MacMurray), in a boating accident in which he is presumed drowned. The lonely widow is comforted by Bills best friend and publishing business partner Henry Lowndes (Melvyn Douglas). Six months later, she marries him. Six months after that, Bill shows up, having been stranded on a uninhabited island. Vicky has a tough choice to make.

==Cast==
* Jean Arthur as Vicky Lowndes
* Fred MacMurray as Bill Cardew
* Melvyn Douglas as Henry Lowndes Harry Davenport as George, Vickys father
* Dorothy Peterson as Gertrude Houlihan
* Melville Cooper as Peter, the Lowndes butler
* Edgar Buchanan as Detective Adolph McDermott
* Tom Dugan as Lieutenant Sullivan

== Nomination == Academy Award for Best Sound Recording.   

==See also==
* My Favorite Wife, another 1940 film, in which it is the wife (played by Irene Dunne) who returns, just as her husband (Cary Grant) embarks on his honeymoon.
* Move Over, Darling, the 1963 remake of My Favorite Wife, starring Doris Day, James Garner, and Polly Bergen.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 