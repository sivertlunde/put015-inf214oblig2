Highway Dragnet
{{Infobox film
| name           = Highway Dragnet
| image          = Highway Dragnet 1954.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Nathan Juran
| producer       = William F. Broidy Jack Jungmeyer
| screenplay     = Herb Meadow Jerome Odlum Additional dialogue: Fred Eggers  Tom Hubbard 
| story          = Uell Stanley Andersen|U.S. Anderson Roger Corman
| narrator       =
| starring       = Richard Conte Joan Bennett Wanda Hendrix Reed Hadley
| music          = Edward J. Kay
| cinematography = John J. Martin
| editing        = Ace Herman
| studio         = William F. Broidy Pictures Corporation
| distributor    = Allied Artists Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} crime drama film noir directed by Nathan Juran, based on story by Uell Stanley Andersen|U.S. Anderson and Roger Corman. The film stars Richard Conte, Joan Bennett and Wanda Hendrix. 

==Plot== Marine hitches a ride with a female fashion photographer and her young assistant to escape from the Las Vegas police on a murder charge.

==Cast==
* Richard Conte as  Marine Sgt. James Henry
* Joan Bennett as  Mrs. Cummings
* Wanda Hendrix as  Susan Willis
* Reed Hadley as  Det. Lt. Joe White Eagle
* Mary Beth Hughes as  Terri Smith
* Iris Adrian as Sally

==Production== exploitation Highway Highway Patrol.  Portions of the film were shot in the Coachella Valley, California.   

==Reception==
The New York Times called the film a "second rate "whodunnit". Palace Shows Highway Dragnet
O. A. G.. New York Times (1923-Current file)   20 Feb 1954: 8. 

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images)

 

 
 
 
 
 
 
 
 
 