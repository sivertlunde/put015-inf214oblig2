Lawang Sewu: Dendam Kuntilanak
{{Infobox film
| name           = Lawang Sewu: Dendam Kuntilanak
| image          = Lawang Sewu DVD Cover.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = DVD Cover (2008)
| director       = Arie Azis
| producer       = Manoj Punjabi
| writer         = Aviv Elham
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Thalita Annemarie Latief  Salvina  Bunga Jelita  Tsania Marwah  Marcell Darwin  Ronald Gustav
| music          = 
| cinematography = 
| editing        = 
| studio         = MD Pictures
| distributor    = 
| released       =  
| runtime        = 
| country        = Indonesia
| language       = Indonesian
| budget         = 
| gross          = 
}}
Lawang Sewu: Dendam Kuntilanak (Lawang Sewu: Kuntilanaks Vengeance) is a 2007 Indonesian horror film directed by Arie Azis and starring Thalita Annemarie Latief, Salvina, Bunga Jelita, Tsania Marwah, Marcell Darwin, and Ronald Gustav. The plot takes place in Lawang Sewu, a supposedly haunted building in Semarang.

== Plot ==

 
 

Seven teenagers from Jakarta, Diska (Thalita Annemarie Latief), Naya (Salvita), Cika (Bunga Jelita), Dinda (Tsania Marwa), Yugo (Marcell Darwin), Armen (Melvin) and Onil (Ronald Gustav), are in Semarang to celebrate their graduation from high school. All but Diska, driving the car, are drunk after going to a discothèque. On the way to Nayas grandmothers house, Armen, Onil, and Yugo ask Diska to pull over so that they can relieve themselves; they stop at Lawang Sewu, and the boys urinate onto the grounds from outside the fence. Cika, who also must urinate, does not feel comfortable with the boys and so goes into the complex to relieve herself.
 possesses Dinda and causes her to insult the others. After Dinda returns to normal, Diska yells at her for violating the sanctity of the building by entering it while Menstruation|menstruating. Onil is told to take Dinda outside, but before they can leave the complex they are approached by a kuntilanak with a ball and chain wrapped around her leg; Onil wets his pants, and the two hug as the ghost approaches them.

Meanwhile, Diska, Yugo, Armen, and Naya find Cikas body inside the building. As they are attempting to take the corpse away, the kuntilanak comes and chases them. Diska makes it outside the building, but Armen, Yugo, and Naya are chased into the basement, where they are terrorized by the kuntilanak. Diska drives away to get help from Nayas grandmother. When they return, they attempt to drive away the ghosts by performing an exorcism; the ghosts rise up and kill Nayas grandmother, driving Diska into the basement.
 unintentionally pregnant with Armens child, was chased out of Jakarta by Armen and his friends; Diska, who wanted to help her, was stopped by Naya and Cika. Upon arriving in her hometown of Semarang, Ratih was disowned by her family and in desperation threw herself into a well at Lawang Sewu.

Upon learning the truth, Diska runs to the well in order to close it. Meanwhile, the kuntilanak kills Naya and Armen. As the kuntilanak is preparing to kill Yugo, Diska manages to close the well and stop it. Together Diska and Yugo leave to go to their own homes.

== Production ==

Lawang Sewu: Dendam Kuntilanak was produced by MD Pictures, which had previously produced   (2007) and Suster Ngesot (2007).  It was directed by Arie Azis and written by Aviv Elham, and drew its inspiration from stories about Lawang Sewu in Semarang, Central Java, a colonial building rumored to be haunted. 

While filming the scene where Dinda is possessed by the ghost of Noni Van Ellen, Tsania Marwah reportedly began speaking in fluent Dutch, a language she had never studied before. 

== Themes ==

Manoj Punjabi, the films producer, stated that he believed Lawang Sewu: Dendam Kuntilanak carried an important message, that humans as social creatures must always follow social norms and be aware of their surroundings; if not, he added, their secrets would be revealed. 

== Release and reception == XXI Theatre on 19 September 2007, with a wide release the following day.  It received a poor reception. Evi Febrian Surya, writing for the Semarang-based Suara Merdeka, gave the film a scathing review, saying that it "truly raped an icon of Semarang."  

== Notes ==

 

== References ==

Footnotes

 

Bibliography
 
*   }}
*   }}
 

== External links ==

*  

 
 
 