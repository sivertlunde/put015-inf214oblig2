Gypo (film)
{{Infobox film |
|  name           = Gypo |  
|  image          =  |
|  caption        =  |
|  producer       =  |
|  director       = Jan Dunn (not credited, per Dogme95|Dogme rules |
|  writer         = Jan Dunn |
|  starring       = Pauline McLynn,   Chloe Sirene   Paul McGann |
|  distributor    =  |
|  country        = United Kingdom |
|  language       = English |
|  runtime        = 98 minutes |
|  released       = 2005 |
| budget         = GB£300,000
| gross          = 
}}
Gypo   is a 2005 independent film written and directed by Jan Dunn. Its story details the breakdown of a family in a small town in United Kingdom|Britain, told in three narratives.
 Dogme style. It was the first British film to be registered as such (as Dogme #37).   

==Plot summary==
The film is shown through the eyes of the three main characters; Paul and Helen, a married couple, and Tasha, a teenage refugee. It shows how the family falls apart under the strain of unexpected emotions. 

==Cast==
{| class="wikitable"
|- bgcolor="#efefef"
! Actor !! Role
|- Pauline McLynn|| Helen 
|- 
| Chloe Sirene || Tasha
|- 
| Paul McGann || Paul
|-
| Rula Lenska || Tashas Mother
|-
| Freddie Connor || Tashas Husband
|-
| Olegar Fedoro || Tashas Father
|-
|}

==Awards==
*British Independent Film Awards  Best Achievement in Production San Francisco Gay and Lesbian Film Festival  Best First Feature

Plus two other nominations and Special Mention at Torino International Gay and Lesbian Film Festival

==References==
 
* 

==External links==
*  at the Internet Movie Database
*  at Rotten Tomatoes

 
 
 
 
 
 


 