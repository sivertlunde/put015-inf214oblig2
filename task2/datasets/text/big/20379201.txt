La Sorcière (film)
{{Infobox film
| name           = La Sorcière
| image          = 
| image size     = 
| caption        = 
| director       = André Michel
| producer       = Robert Woog
| writer         = Paul Andréota Jacques Companéez Christiane Imbert Aleksandr Kuprin
| narrator       = 
| starring       = Marina Vlady
| music          = 
| cinematography = Marcel Grignon
| editing        = Victoria Mercanton
| distributor    = 
| released       = 11 April 1956
| runtime        = 97 minutes
| country        = France
| language       = French
| budget         = 
| preceded by    = 
| followed by    = 
}} 1956 drama film directed by André Michel based on a screenplay by Paul Andréota and Jacques Companéez. Adapted from the Alexander Kuprin novel Olesya. At the 6th Berlin International Film Festival it won the Silver Bear award for an Outstanding Artistic Contribution.   

== Plot summary ==
A French civil engineer (Maurice Ronet) is working in Sweden where he meets a local girl named Ina.

== Cast ==
* Marina Vlady as Ina
* Nicole Courcel as Kristina Lundgren
* Maurice Ronet as Laurent Brulard
* Michel Etcheverry as Camoin Rune Lindström as Reverend Hermansson
* Erik Hell as Pullinen
* Eric Hellström as Erik Lundgren
* Ulla Lagnell as Mrs. Hermansson
* Naima Wifstrand as Maila
* Ulf Palme as Matti

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 
 