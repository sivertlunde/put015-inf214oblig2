Idavela
{{Infobox film
| name           = Idavela
| image          = Idavela.jpg
| image size     = 
| alt            = 
| caption        = CD Cover Mohan
| producer       = Shreyes Films
| writer         = Padmarajan Ashokan Idavela Nalini
| music          = M. B. Sreenivasan
| cinematography = U. Rajagopal
| editing        = G. Venkitaraman
| studio         = Shreyes Films
| distributor    = Century Films
| released       =  
| runtime        = 144 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Nalini in lead roles. It was the debut film of Idavela Babu, who got his screenname through this film.   

==Plot==
The film starts out as a campus story involving four boys, Thomas (Ashokan), Ravi (Idavela Babu), Alosius and Swami. There is the usual pranks that the boys of the age do, although the movie doesnt just revolve around these.The second half of the movie takes us to scenic locales of Munnar. The second half also introduces the major female character, Molu. The movie centres around the problems faced by them in a hotel in Munnar.

The movie opens with showing the 4 college boys to be indulged in smoking, drinking and watching blue films. Each time they get caught and are punished. Their parents are also informed about the same and they are a disgrace to their families as well. The college NCC decides to conduct a camp for 10 days and these 4 are asked to first cut their hair and then report their name to attend the camp. However they choose not to give their names. They inform at home of going to the camp and plan to  spend those 10 days going elsewhere on their own.

On the day of departure Thomas picks up Ravi from his home. Ravis brother asks Thomas to take care of Ravi. The 4 meet at the railway station and go to Munnar. They ask the local taxi to take them to a good hotel and the driver takes them to hotel blue meadow. They get a room by providing fake names. The bearer there is Madhavan (Innocent). The boys have come there with the intention of having sex and inquires with Madhavan regarding the same, who says the hotel is a reputable one and such activities dont happen there.

They go for a long walk, find a girl near the lake and follow her to find that she stays in the room next to theirs in the same hotel. They try to get friendly with her. She (Nalini) mentions her name to be Malu or they can call her Molu as her mother calls her. She develops a liking towards Ravi and dislikes Thomas. This creates jealousy among the other boys towards Ravi. Ravi falls in love with Molu which he tells her but she doesnt respond. Thomas warns Ravi that they are not here to stay forever and has to tell her their intention in his next meeting with her. The next day Ravi meets with Molu, asks her to pose for a few photos which she does willingly. They walk a long distance away from the hotel with the other 3 boys following them without their knowledge. Then Ravi informs Molu that he has been sent to her to tell her to cooperate with the boys. She smilingly tells him to lie to his friends that they made love and she wouldnt mind it. Overhearing this Thomas jumps in front of her and tries to rape her. Ravi runs to the hotel to get help with Alosius trying to stop him. Ravi runs into a muddy swamp and gets engulfed by it. Alosius tries to save him and gets stuck in the mud. Hearing his shout, the other 2 boys along with Molu rush there and pull him out. Molu realizes that Ravi has drowned and Alosius confirms the same. They try in vain to find Ravi in the swamp. Thomas threatens to kill Molu if she tells anyone about this incident. Molu informs them that she also feels responsible for whats happened and wouldnt inform anyone. She returns to the hotel. The boys wait for the evening, when Christmas celebrations start, to return to the hotel so that no one would notice that Ravi is missing. At night Madhavan brings them their drink which they had ordered the previous day. After Madhavan leaves Thomas asks the boys to have their drinks but they refuse. They accuse him of being hard hearted and of being responsible for the death of Ravi. Thomas throws the bottle through the window and walks out to the balcony to find Molu standing next door. She accuses him of being a cheater and murderer. The next morning Alosius and Swami are packing their bags when Molu walks in to their room. She enquires about Thomas to find that he is nowhere to be found. Madhavan informs them that he saw Thomas walking a far distance away. Aloysius, Swami and Molu run towards the direction pointed by Madhavan calling out Thomas name. They come near the muddy swamp and find his cigarette packet and match box nearby. They prod the mud with a log of wood to find a hand come up which confirms their doubts that Thomas has drowned in the swamp.

==Cast== Ashokan as Thomas John
* Idavela Babu as Ravi Nalini as Molu / Malu Innocent as Madhavettan
* Sankaradi as Nambeesan Sir
* Prem Prakash as Ravis brother
* Thodupuzha Vasanthi
The roles of Alosius and Swami are played by newcomers.

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by Kavalam Narayana Panicker.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amme Kanyaamariyame (Chilluvazhi Paayum) || Chorus, JM Raju || Kavalam Narayana Panicker ||
|-
| 2 || Gloria Gloria Gloria Swargeeya (Vinnin Saanthi Sandesham) || Chorus, JM Raju || Kavalam Narayana Panicker ||
|-
| 3 || Manjumma vekkum || Krishnachandran || Kavalam Narayana Panicker ||
|}

==References==
 

==External links==
*  
*   at the Malayalam Movie Database
*  

 

 
 
 
 
 
 