Tropic of Cancer (film)
{{Infobox film
| name           = Tropic of Cancer
| image          = Tropic of Cancer poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Joseph Strick
| producer       = Joseph Strick
| screenplay     = Betty Botley Joseph Strick 
| based on       =   Phil Brown Dominique Delpierre
| music          = Stanley Myers
| cinematography = Alain Derobe 
| editing        = Sidney Meyers Sylvia Sarner 
| studio         = Tropic Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} same title. rated X in the United States, which was later changed to an Motion Picture Association of America film rating system#X is replaced by NC-17|NC-17 rating.  In the UK the film was refused a theatrical X certificate by the BBFC. Strick had previously  adapted other works of literature  - Jean Genets The Balcony and James Joyces Ulysses (novel)|Ulysses.

==Plot==
The film is a sex comedy, a series of vignettes and sex fantasies about Americans abroad.
 Depression milieu of rootless Russian and American exptriates in the Paris of the early 1930s, something for which he was criticised by the critic Pauline Kael. " When the story is made timeless, the characters are out of nowhere, and the author-hero is not discovering a new kind of literary freedom in self-exposure, hes just a dirty not-so-young man hanging around the tourist spots of Paris."

==Cast==
*Rip Torn as Henry Miller
*James T. Callahan as Fillmore
*David Baur as Carl
*Laurence Lignères as Ginette Phil Brown as Van Norden
*Dominique Delpierre as Vite Cheri
*Magali Noël as The Princess
*Raymond Gérôme as M. Le Censeur
*Ginette Leclerc as Madame Hamilton
*Sabine Sun as Elsa
*Sheila Steafel as Tania
*Gladys Berry as American Lady
*George Birt as Sylvester
*Stuart De Silva as Ranji
*Steve Eckardt as Cronstadt
*Philippe Gasté as Train Passenger
*Gisèle Grimm as Germaine
*Eléonore Hirt as Yvette
*Jo Lefevre as Accordionist
*Françoise Lugagne as Iréne
*Edward Marcus as Boris 
*Henry Miller as Spectator
*Christine Oscar as Helen
*Elliott Sullivan as Peckover

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 