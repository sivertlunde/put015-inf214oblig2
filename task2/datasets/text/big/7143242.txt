Fist of Fury III
 
 
{{Infobox Film | 
| name = Fist of Fury III Jeet Kune the Claws and the Supreme Kung Fu
| director = Lee Tso Nam
| starring = Bruce Li Ku Feng Bruce Tong Yim-Chaan Chow Siu-Loi
| released =  
| country = Hong Kong
| language = Cantonese
}}

Fist of Fury III ( ) is a  ) from Fist of Fury II, the brother of the Bruce Lee character in Fist of Fury.

==Synopsis==

After avenging the death of his brother, Chen Shen (Bruce Li) returns home from Shanghai.  He tells his mother (who went blind from crying over her sons death) that he will no longer fight.  However, being a movie with the words "fist" and "fury" in the title, Chen doesnt keep his promise for very long.

Japanese occupiers who are aware of Chens history terrorize his family by, among other things, vandalizing his mothers store and beating up his brother.  Later, they frame Chen for a murder.  After the Japanese boss arrives in town and causes a ruckus, Chen breaks out of jail for a final confrontation.

==Trivia==
* The on-screen title of the English-dubbed version released in North America cropped for television and home is simply The Fist of Fury or in some cases "Fist of Fury II". The numerals have been partially or completely cropped off. This has led to VHS releases that identify the film as "Fist of Fury II" by mistake.
* IMDB credits Lee Tso Nam as the director, while the Hong Kong Movie Database credits Do Liu-Boh

== External links ==
*  

 
 
 
 


 
 