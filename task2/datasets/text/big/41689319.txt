Fangs of the Arctic
{{Infobox film
| name =  Fangs of the Arctic
| image =
| image_size =
| caption =
| director = Rex Bailey
| producer = Lindsley Parsons    Ace Herman William Raynor   Warren Douglas 
| narrator =
| starring = Kirby Grant   Lorna Hanson   Warren Douglas    Leonard Penn
| music = Edward J. Kay  
| cinematography = William A. Sickner 
| editing = Ace Herman 
| studio = Allied Artists Pictures
| distributor = Allied Artists Pictures
| released = January 18, 1953
| runtime = 62 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 series of ten films featuring Kirby Grant as a Canadian Mountie. 

==Cast==
*   Kirby Grant as RCMP Corporal Rod Webb 
* Lorna Hanson as Sandra Dubois  
* Warren Douglas as Matt Oliver 
* Leonard Penn as Henchman Morgan  
* Richard Avonde as Henchman Cheval   Robert Sherman as RCMP Constable Mike Kelly  
* John Close as Henchman Howell  
* Phil Tead as MacGregor, Trading Post Owner  
* Roy Gordon as Briggs  Kit Carson as Andrews Chinook as Chinook, Webbs dog

==References==
 

==Bibliography==
* Drew, Bernard. Motion Picture Series and Sequels: A Reference Guide. Routledge, 2013. 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 