Crawlspace (2004 film)
 
 

{{Infobox Film |
  name        = Crawlspace |
  image       = |
  caption     = |
  writer      = Peter Sved |
  director    = Peter Sved |
  producer    = Adam Dolman |
  editing     = David Cole |
  music       = Tamara OBrien |  
  distributor = AFTRS |
  released    = April 2004 |
  runtime     = 8 min. |
  rating      = n/a |
  country     = Australia |
  language    = English |
  budget      = |
  followed_by =
}} animated short The Australian Film, Television and Radio School in Sydney.

==Plot summary==
A strange man stuck in a desert wakes from his coma and goes in search of water. He discovers a lush green forest, alive with plants and animals the likes of which he could never have dreamt of. He finds a watersnake (a bizarre creature that is a snake with a human hand as its head; is made of a semi-transparent fluid; has the ability to float in air) that has been captured by a giant plant that resembles a venus flytrap, and rescues the strange creature from its clutches. In return, the creature reveals to him the secret place in the forest where water can be found.

==Festival Screenings and Awards==

=== Crawlspace has been screened at various festivals around the world, including ===

*Hiroshima International Animation Festival 2004, Japan (Winner - Stars of Students Award)

*ComGraph 2004: Asia-Pacific Digital Art & Animation Competition, Singapore (Winner - Merit Award, Student Computer Animation Section)

*Australian Effects and Animation Festival 2004, Sydney (Nomination, Best Graduation Film)

*Asia International Short Film Festival 2004, Seoul, Korea

*Asia Pacific Film Festival 2004, Kuala Lumpur, Malaysia

*St Kilda Film Festival 2004, Melbourne

*Mill Valley Film Festival 2004, California, USA

*Trebon International Animation Festival 2005, Czech Republic

*Ecozine International Film Festival 2009, Spain

=== Crawlspace has also been nominated in the following categories ===

*2005 MPSE Nomination for Best Sound in Student film for Derryn Pasquils sound design

*2005 MPSE Golden Reel Nomination, LA, for Tamara OBriens original score

==External links==
* 

 
 
 
 
 
 


 
 