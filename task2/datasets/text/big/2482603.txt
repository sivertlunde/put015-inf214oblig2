The Girl Who Knew Too Much (1963 film)
{{Infobox film
| name           = The Girl Who Knew Too Much
| image          = Thegirlwhoknewtoomuch.jpg
| alt            =
| director       = Mario Bava
| producer       = Massimo De Rita
| writer         = Mario Bava Enzo Corbucci Ennio de Concini Eliana DeSabata Mino Guerrini Franco E. Prosperi John Saxon Valentina Cortese Dante DiPaolo Gianni di Benedetto
| music          = Roberto Nicolosi Les Baxter  (U.S. version) 
| cinematography = Mario Bava
| editing        = Mario Serandrei
| studio         = Galatea Film Coronet s.r.l.
| released       = 10 February 1963
| runtime        = 86 min.
| country        = Italy
| language       = Italian ITL 55,000,000  (Italy) 
}}
 Italian filmmaker John Saxon as Dr. Marcello Bassi and Letícia Román as Nora Davis. The plot revolves around a young woman named Nora, who travels to Rome and  witnesses a murder. The police and Dr. Bassi dont believe her since a corpse cant be found. Several more murders follow, tied to a decade-long string of killings of victims chosen in alphabetical order.
 horror conventions.  This was Bavas last film shot in black-and-white.   

== Plot ==

On vacation, Nora Davis (Letícia Román) arrives by plane in Rome to visit her elderly ailing aunt. Noras aunt is being treated by Dr. Marcello Bassi (John Saxon).
Noras aunt passes away on the first night of Noras visit and she walks to the nearby hospital to notify Dr. Bassi. On the way, she is mugged and knocked out in Piazza di Spagna.

When she wakes up, she sees the body of dead woman, lying on the ground near her; a bearded man pulls a knife out of the womans back.
Nora reports this to the police in the hospital, who dont believe her when they find no evidence and think shes hallucinating.

Later, at a cemetery, Nora meets a close friend of her aunts, Laura Torrani (Valentina Cortese), who lives in the Piazza di Spagna. Laura plans to vacation soon and allows Nora to stay in her house for the remaining time of the vacation. Nora later explores Lauras closet and drawers and comes across newspaper clippings of articles on a serial killer dubbed the "Alphabet Killer" due to his having alphabetically killed people according to their surnames. The killer has already murdered victims whose last names begin with "A," "B," and "C," respectively.

Nora also finds that the last victim Lauras sister, whom Nora had seen in a vision. According to the reports in the paper, this murder took place ten years ago. Nora then receives a telephone call, in which an anonymous voice tells her that "D is for death," and informs her that she will be the killers next victim.
Nora receives help from Dr. Marcello, who takes her on a trip to various Roman tourist sites to calm her down as they become more romantically interested in each other.

When they return to the Craven house, she receives a call from a person who orders her to go to a particular address. Nora goes there, and she is guided to a vacant room. With Dr. Marcello, she discovers that the voice that guided her to this spot is tape recorded, and the voice warns Nora to leave Rome before it is too late.
Nora and Marcello discover that the room is leased to Landini. After several unsuccessful attempts to locate Landini, Nora and Marcello go to the beach to relax. Upon their return to the Craven house, they find Landini, who has been informed that they were inquiring about him. Investigative reporter Landini (Dante DiPaolo) has secretly been following them since he spotted Nora in the square.

The reporter wrote about the murder story when it first broke, but he believes that the police would catch the wrong person if he reported the details of the crime. Landinis refusal to publish a report of the murder has put him in financial need. Nora decides to help Landini, but, as they tour Rome, they find no clues.
Nora visits Landinis apartment the next day, finding clues that lead her to think that he is the murderer that and she is his next intended victim, but Landini appears to have committed suicide. The same day, Laura returns to Rome from her vacation while Nora and Marcello plan to go to America the following morning. From reading the newspaper, Nora learns that the body of a young woman was found, and she recognizes it as the murdered woman she saw on the night of her arrival in Italy.  After identifying the victims corpse at the morgue, Nora believes that she has witnessed the murder.

Alone in the house that night, Nora notices that the study door is open. On entering, she sees a man rising uncomfortably from his chair. Nora recognizes him as the man who had stood over the dead body of the woman whose corpse she had seen after awakening from having been knocked unconscious upon her arrival in Italy.
The man walks towards Nora but collapses to the floor, a knife in his back. Nora is then confronted by Laura who, enraged, confesses to the killings and explains that she stabbed her husband because of his attempts to turn her over to the police.

Laura reveals that her desire to steal her sisters money compelled her to murder. Laura attempts to attack Nora, but Laura is suddenly shot dead by her husband. Nora finds that the bearded man she had seen in a daze actually was disposing of the body of his murdering wife. Nora then leaves Italy, happily reunited with Marcello.

==Production==
Director Mario Bava thought the plot of The Girl Who Knew Too Much was silly and focused more on the technical aspects of the film. 

==Release and reception== jazz score with one described as a "more noisy" one performed by Les Baxter. 

Director Mario Bava didnt look back positively on the film, claiming that he "thought   was too preposterous. Perhaps it could have worked with James Stewart and Kim Novak, whereas I had...oh, well, I cant even remember their names."  The film is often remarked upon by modern critics. Of the five critics who have reviewed the film at Rotten Tomatoes the film has received slightly above average reviews noting the stylish look to the film, but negatively pointing out its story. 

== References ==

 

== Notes ==

*  
*  
*  

== External links ==

*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 