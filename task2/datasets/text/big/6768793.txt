Suyamvaram
{{Infobox film
| name = Suyamvaram
| image =
| director =
| screenplay = Siraaj
| starring =
| producer = Giridharilal Nagpal
| story = Giridharilal Nagpal
| studio = L. S. Movies
| music = S. A. Rajkumar
| cinematography =
| editing = L. Kesavan
| distributor =
| released = July 16, 1999
| country = India
| runtime = 129 mins Tamil
| budget =
| revenue =
}}
 1999 Tamil Tamil  ensemble cast from actors in the Tamil film industry and shot by a large technical team from the industry. The film was planned by Giridharilal Nagpal and brought together 14 major directors, 19 cinematographers and over thirty leading actors in the Tamil film industry.

The film also holds the Guinness World Record for casting the most stars in a film and also for being the quickest ever full feature-length film made, with filming being completed in 23 hours and 58 minutes. The film released on July 16, 1999 to positive reviews from critics, praising the intentions of the makers.

== Plot == Preetha Vijaykumar Napoleon is the family doctor.

The movie opens with Kuselans 60th birthday celebrations and Kuselan gets a heart attack. At the hospital, the family is informed that Kuselan will not live long and to keep him comfortable in his last days. From the devastated family, Kuselan asks for one last favour: that all his children get married before he dies. The children all agree and a state wide ad is put out saying that those chosen to marry Kuselans children would get a piece of the family properties and 1 crore in cash. Needless to say, the mention of wealth brings in potential brides and grooms by the truck full to be interviewed by either Bhagyaraj and Oorvasi or the two doctors Janagaraj and Senthil.
 Livingston respectively. Karthik who shows up at the interview thinking it is an interview for a doctors post in a clinic while Suvalakshmi, who is handicapped, pairs with Parthiban in the end. Mansur Ali Khan, with Vichitra in tow, tries to worm his way in as a groom and when that effort fails, he kidnaps the 9 brides for ransom. Napoleon, who calls his friend Assistant Commissioner Arjun Sarja for help. After finding the brides and after the marriage Kuselan explains that his heart attack was just a joke and he wanted them to get married and thats why he said he had a heart attack.

== Cast ==
In order of appearance:
  Vijayakumar as Kuselan
*Manjula Vijayakumar as  Suseela Napoleon as Krishna Parthiban as Azhagappan
*Sathyaraj as Arunachalam Prabhu as Aavudaiappan Abbas as Indiran Roja as Easwari Kasthuri as Uma
*Maheswari as Aishwarya
*Preetha Vijayakumar as Hema Rambha as Urvashi
*Suvalakshmi as Ezhilarasi Kushboo as tribal girl Ishwarya as Savithri Heera as Indirans girlfriend
*Pandiarajan as Pallavan
*Vineeth as Gautham Livingston as Kanthen
*Prabhu Deva as Kanna Karthik as Ram Kumar Arjun as Sanjay Vignesh as potential groom Bhagyaraj as Gnanapithan Urvashi as Arivozhi Janagaraj as Mithrabuthan Senthil as Panchabutham Mansoor Ali Khan as Kabilan
*Vichithra as Vichitra
*Cochin Hanifa as Arunachalams friend
*S. S. Chandran 
*Kavithalaya Krishnan as a relative
*Dhamu as a patient
*Madhan Bob as a priest
*Vaiyapuri as a potential groom Anu Mohan as a detective Mohan Raman as a wedding guest Ambika as a wedding guest
 

==Crew==
* Directors: J. Paneer, A. R. Ramesh, Keyaar, E. Ramdoss, Arjun Sarja|Arjun, Guru Dhanapal, Liyakat Ali Khan, R. Sundarrajan, Selva (director)|Selva, K. Subash, Sundar C, Suraj (director)|Siraaj, K. S. Ravikumar, P. Vasu
* Screenplay: Siraaj Vidyasagar
* Cinematographers: M. V. Paneerselvam, U. K. Senthil Kumar, R. B. Imaiyavarman, Ram Gunasekharan, R. Rajaratnam, R. H. Ashok, K. B. Ahmed, G. Mogan, Victor. S. Kumar, K. S. Selvaraj, Ashok Rajan, Babu, R. Ragunatha Reddy, L. B. Rao, A. Karthikraja, K. S. Udhayashankar, D. Shankar, Vijayasri
* Editor: L. Kesavan, K. Thanikachalam, P. Sai Suresh, P. Madhan Mohan
* Art Director: G. K

==Production==
Producer Giridharilal Nagpal announced his intentions of making, Suyamvaram, a film shot within 24 hours in January 1999, citing it had been a fourteen-year dream. The films launch occurred in March 1999 with Rajinikanth and Kamal Haasan in attendance.  He announced his intentions of shooting the film within 24 hours on April 5 and 6, 1999 across film studios in Chennai. Along with Nagpal, the film brought together 19 associate directors, 45 assistant directors, 19 cameramen, 36 assistant cameramen, nine steadycam operators, 14 heroes, 12 heroines, villains, comedians, five dance masters, 16 assistants, 140 chorus dancers, stunt coordinators, art director, makeup, costume and set designers, 15 film units, a still photographer and 1,483 extras to make the film.    Despite months of planning, Nagpal has left many details to the last minute with no script produced and directors describing scenes to actors, who would rehearse them once before filming.    Two representatives, on behalf of the Guinness World Records, were present to oversee the time-schedule.    Giridharlal said, "The goal is to finish every stage of film-making within the stipulated 24 hour period, Shooting, developing rushes, editing, dubbing, re-recording and final mixing for the master copy will all be done in that time. The script is being divided into 11 parts, and one director will shoot one part, all of them working on the same day, at different sets and venues". 
 Abbas and Heera was shot and at Guindy, Ramdoss shot scenes involving Pandiarajan and Kasthuri.  A dance sequence at a discothèque in Abu Palace was shot at 5.30pm with Vineeth and Maheswari and kidnap scenes were shot at night at Kushaldoss House. At 3am on April 6, the film ran two hours behind schedule and four directors improvised and changed the storyline with two scenes being cut and planned into one.  The climax sequence was shot at Vijaya Vauhini studios, designed to look like a wedding hall. At 6.30, the entire family is collected at one place and the final scene is shot. The film finished filming at 6.50am on April 6, 1999 with ten minutes to spare. 
 Arjun appeared as a cop in the film as well as directing and coordinating the action scenes in the films climax.  Art director, G. K., was in charge of co-ordinating sets at all 21 locations and managed to keep in control of events through his mobile phone; while Giridharilals son Vinay, scooted from location to location, ensuring that the project was developing smoothly. 

==Release==
The film was released on July 16, 1999 to positive reviews from critics, with reviewers praising the production of the film rather than the overall content. Shobha Warrier of Rediff.com claimed the attempt "particularly praiseworthy if we take into consideration the time factor and the amount of co-ordination the film-makers had to do", whilst adding that the movie was "slicker and better made than many churned out regularly by some film factories".  A reviewer from Indolink.com cited  that "it is a laudable venture" and "they have even tried to have a storyline for this movie".    In regard to the technical aspects the critic claimed that "the songs are okay - nothing to rave about".  Another reviewer labelled that it "is not only a fairly humorous entertainer but an example of how the artistes and technicians of Tamil filmdom can co-operate wholeheartedly", praising the final product. 

The film was dubbed into Telugu as Pellante Idera! by P. R. Kutumba Rao and released in October 2001.  A Hindi remake of the film was pondered by the producer, but later shelved. 

== Soundtrack ==

{{Infobox album  
| Name        = Suyamvaram
| Type        = soundtrack Vidyasagar
| Cover       =
| Released    = 1999
| Recorded    = 
| Genre       = Film soundtrack
| Length      = 
| Label       = 
| Producer    = 
| Reviews     = 
|-
}}

The soundtrack consists of five songs composed by four music directors. 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song Title !! Singers !! Music Director 
|- Deva
|- 
|Kathirunthaalea Rajakumari || Sujatha Mohan || S.A. Rajkumar 
|-  Unni Krishnan || S.A. Rajkumar
|- 
|Sekka Sivanthavalea || Hariharan (singer)|Hariharan, Sujatha Mohan || Sirpy
|-  Vidyasagar 
|}

== References ==
 

==External links==
 
 
 
 
 
 
 
 

 
 
 
 
 