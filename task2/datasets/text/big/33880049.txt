I Want Your Love (film)
 
{{Infobox film
| name = I Want Your Love (2010)
| image = I Want Your Love (Japan).jpg
| alt = 
| caption = 2010 short film  
| director = Travis Mathews
| producer = Jack J. Shamama Michael Keith Christopher Nana
| writer = Travis Mathews
| starring = Jesse Metzger Brenden Gregory
| music = Eddie Foronda
| cinematography = Michael Keith Travis Mathews
| editing = Travis Mathews
| released =  
| runtime = 14 minutes  
| country = United States
| language = English
}}
{{Infobox film
| name = I Want Your Love (2012)
| image = I Want Your Love 2012.jpg
| alt = 
| caption = Theatrical release poster
| director = Travis Mathews
| producer = Jack J. Shamama Michael Keith Christopher Nana
| writer = Travis Mathews
| starring = Jesse Metzger  Brontez Purnell  Ben Jasper   Keith McDonald  Jorge Rodolfo  Ferrin Solano  
| music = Eddie Foronda
| cinematography = Michael Keith Travis Mathews
| editing = Travis Mathews
| released =  
| runtime = 71 minutes  
| country = United States
| language = English
| budget = $80,000
}}
I Want Your Love is the title of both a 2010 short film and a 2012 feature-length film. Both films were directed and written by Travis Mathews. The drama films both revolve around the friends and ex-lovers of Jesse Metzger, a gay man in his mid-thirties who is forced to move back to his hometown from San Francisco due to financial reasons.
 the gay pornographic studio NakedSword. This led to the full-length film being refused exemption from classification to screen at the Melbourne Queer Film Festival, a decision to which actor James Franco (who invited Mathews to collaborate on his film Interior. Leather Bar.) reacted negatively.

==2010 short film==

===Cast===
* Jesse Metzger  Brenden Gregory

===Plot===
Jesse and Brenden playfully negotiate their way toward having sex together, for the first time, on Metzgers last night in San Francisco before he returns to the Midwest.

==2012 film==

===Cast===
* Jesse Metzger, a performance-arts director and the main character, whos forced to move back to his hometown. 
* Brontez Purnell, a friend of Jesses, who works at a clothing shop.
* Ben Jasper, Jesses ex-boyfriend, who works in advertising and stops by to say goodbye. 
* Keith McDonald, Jesses friend and roommate. 
* Ferrin Solano, Waynes boyfriend, who moves in with Wayne. 
* Jorge Rodolfo, Waynes friend, of whom Ferrin is initially jealous, but who eventually joins Wayne and Ferrin for a threesome.
* Peter DeGroot, Jesses one-night stand.

===Plot===
Jesse Metzger, a gay man in his mid thirties who works in the domain of performance arts, finds himself forced to move back to his hometown because he can no longer afford living in San Francisco. As he plans his move, his best friend Wayne is having his boyfriend Ferrin move in. The two have trouble acclimating through the movie, and Ferrin is worried about Waynes increasing interest in Jorge, a friend of Waynes.

Jesse discusses his fears about moving with his other roommate, Keith, who seems to always help Jesse by saying the right things. Meanwhile, Jesse is having trouble with his job, which involves creativity, a quality he is losing under all the pressure. He contacts his ex-boyfriend Ben to say goodbye. Ben is excited, and goes shopping to impress Jesse, where he meets with an old friend, Brontez. The two chat and agree to meet in a goodbye party for Jesse, which Wayne had planned for later that night. Jesse, despite having reminisced his love making with Ben, and Ben feel good about meeting each other, but upon meeting, they both realize their feelings are gone. Later that day, Ben calls Brontez to confirm seeing him later at night in Jesses party.

At the party, Jesse does not show up and stays downstairs with Keith, who is leaving for the weekend. The guests arrive. Ferrin suggests a threesome with Wayne and Jorge, to which they both agree. During the sex, Jorge leaves the two lovers and they finish off alone. Meanwhile, Ben and Brontez flirt and eventually have sex. Downstairs, Jesse wears Keiths clothes and lays down listening to music. Keith shows up, surprising Jesse. The two chat until their sexual tension reaches the point where they have sex, which is interrupted by Jesse himself, who tells Keith that this "isnt what he wants."

In the morning, Ben picks up Jesse. On their way to the airport, Jesse laughs loudly, claiming he is, despite his fears, strangely excited.

==Production==
I Want Your Love is about gay relationships among a group of San Francisco friends. The short film was released in April 2010, with the cooperation of NakedSword, a gay porn studio, and proceeded to be shown at a number of LGBT film festivals around the world. The full-length film was also shown at a number of LGBT film festivals in 2012. 

==Restriction in Australia==
The Australian Classification Board denied I Want Your Love festival exemption for the Sydney Mardi Gras Film Festival.  The move has been controversial, with critics highlighting the fact that Donkey Love, a documentary about zoophilia in Colombia, was permitted to screen at the Sydney Underground Film Festival.   In 2013, actor James Franco spoke out in defence of the film, stating that the refusal to grant a festival exemption to the film was "hypocritical" and "an embarrassment". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 