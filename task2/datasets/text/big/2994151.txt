Bhoot (film)
 

{{Infobox film
| name = Bhoot
| image = Bhoot Poster.jpg
| writer =
| starring = Ajay Devgan Urmila Matondkar Fardeen Khan Rekha Nana Patekar Seema Biswas Victor Banerjee
| director = Ram Gopal Varma
| producer = Nitin Manmohan
| released =  
| runtime = 119 minutes
| country = India
| language = Hindi
| cinematography = Vishal Sinha
| music = Salim-Sulaiman
}}
Bhoot (  horror film. It was directed by Ram Gopal Varma and stars Ajay Devgan and Urmila Matondkar. It was perceived to be different from a typical Bollywood movie as it did not contain songs. It was later dubbed in Telugu as 12 Va Anthasthu and remade in Tamil as Shock (2004 film)|Shock.  The film was box office hit.  Urmila won several accolades and awards for her performance as a ghost possessed wife. Taran Adarsh wrote about her performance, "...the film clearly belongs to Urmila Matondkar all the way. To state that she is excellent would be doing gross injustice to her work. Sequences when she is possessed are simply astounding. If this performance doesnt deserve an award, no other performance should. It beats all competition hollow."  The director has come up with a sequel called Bhoot Returns which was released on 12 October 2012. {{cite web|url=http://news.worldsnap.com/entertainment/bollywood/rgv-uses-optical-illusion-in-bhoot-returns-poster-139759.html |title=RGV uses optical illusion in ‘Bhoot Returns’ poster
 |publisher=Worldsnap News |date=2012-09-04 |accessdate=2012-09-04}} 

==Plot==
The story is about Vishal (Ajay Devgn) who is married to Swati (Urmila Matondkar). The couple buy a high rise apartment at a ridiculously low price. The caretaker of the apartment, Mr Thakkar (Amar Talwar) explains to Vishal that a widow named Manjeet Khosla (Barkha Madan), the previous resident, committed suicide after killing her own son. Vishal hides this fact from Swati, as she will object to buying such a residence. But Mr.Thakkar accidentally slips in the secret.

Swati is livid at Vishal, although he disbelieves the notions of ghosts and bad luck. Then, Swati starts behaving strangely. Vishal consults Dr Rajan (Victor Banerjee). But soon enough, Vishal witnesses Swati killing the watchman of the apartment in a supernatural way, and his skepticism is rudely challenged. Inspector Qureshi(Nana Patekar), who reaches the apartment to investigate the death, becomes suspicious of the duo and their strange behaviour. He follows Vishal and Dr. Rajan. 

Vishals maid witnesses Swathi shouting and throwing Vishal away. She helps him tie her. She tells him that Swathi was shouting like Manjeet and tells him that an exorcist can help her but not doctors. Finally, Vishals maid calls an exorcist named Sarita (Rekha). Sarita sees the ghosts of Manjeet and her son. She advises Vishal to meet Manjeets mother (Tanuja), since she can placate her daughters spirit. Vishal complies and meets Manjeets mother. He learns from Manjeets mother that Manjeet was not the type of woman who would commit suicide. He explains the situation to her and asks her help. She comes with him and somehow placates Manjeets spirit. 
They come to know that Mr.Thakkars son, Sanjay tries to molest Manjeet and when she resists she accidentally falls off the balcony and dies. Hence, Sarita advises Vishal to call him. Vishal makes an unknown call to Sanjay and tells him that his father is sick. When Sanjay arrives, Vishal cleverly tells Mr. Thakkar and Sanjay to help him take Swathi to the hospital. 

It is then revealed that many years ago, Sanjay came to visit his father and got his heart on Manjeet as he saw her in the apartment. He broke into her house, and had developed a lust for her, but when she rejected him, he pushed her and she accidentally fell off the balcony and died. But Manjeets son saw the murder, upon which Sanjay hired the watchman to kill him. Manjeet, who has still occupied Swatis body, sees Sanjay and chases him. Qureshi tries to stop her, having no idea of the real story. Swathi tries to kill Sanjay by strangulating him. But, Sarita asks Manjeet to leave him as the blame will come upon Swathi. 

Sanjay escapes, only to find himself surrounded by Vishal, Sarita, Manjeet and Qureshi who now knows the truth. A terrified Sanjay confesses to the crimes, upon which Manjeets mother urges her to stop. Sanjay is arrested by Inspector Qureshi, and thrown into jail. Manjeet leaves Swatis body, and Vishal and Swati live a good life in the apartment. Meanwhile, in the lockup, Qureshi tells Sanjay that death sentences are light penalties for a criminal like him. He wishes that Sanjay gets a bigger punishment. After Qureshi leaves the darkened cell, Sanjay finds himself face to face with Manjeet. He starts begging for mercy, but his voice soon fades out.

==Cast==
* Urmila Matondkar ... Swati
* Ajay Devgan ... Vishal
* Nana Patekar ... Inspector Liyaqat Qureshi
* Rekha ... Sarita
* Fardeen Khan ... Sanjay
* Tanuja ... Mrs Khosla
* Seema Biswas ... Bai
* Victor Banerjee ... Dr. Rajan
* Sabeer Masani ... Watchman
* Barkha Madan ... Manjeet Khosla
* Master Akshit ... as Manjeets Son
* Rajendra Sethi ... as Patil "House Broker"
* Amar Talwar ... as Thakker

==Soundtrack==
The soundtrack  is under the label T-Series. The music of the film was composed by Salim-Sulaiman. It consists of 7 songs, 1 remix and an instrumental. The whole soundtrack is not used in the movie, except the song "Ghor Andhere" for the ending credits.

===Track listing===
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Duration
|-
| "Bhoot Hai Yahan Koi"
| Asha Bhosle
| 4:57
|-
| "Bhoot Hai Yahan Koi - II"
| Gayatri Iyer
| 4:11
|-
| "Bhoot Hoon Main"
| Sunidhi Chauhan, Vijay Prakash & Salim Merchant
| 3:57
|-
| "Bhoot Hoon Main - Remix"
| Sunidhi Chauhan, Vijay Prakash & Salim Merchant
| 3:35
|-
| "Dead But Not Asleep"
| Asha Bhosle
| 2:26
|-
| "Din Hai Na Yeh Raat"
| Usha Uthup
| 3:36
|-
| "Ghor Andhere"
| Sunidhi Chauhan, Vijay Prakash, Clinton Cerejo & Salim Merchant
| 5:08
|-
| "Yeh Sard"
| Gayatri Iyer & Anand Raj Anand (Uncredited)
| 5:22
|-
| "Yeh Sard - Instrumental"
|
| 5:22
|}

==Awards==
;Bollywood Movie Awards
* Bollywood Movie Award – Best Director - Ram Gopal Varma
;Filmfare Awards
* Filmfare Critics Award for Best Actress - Urmila Matondkar
;Star Screen Awards
* Screen Award for Best Actress - Urmila Matondkar
;Zee Cine Awards
* Zee Cine Award for Best Actor – Female - Urmila Matondkar

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 