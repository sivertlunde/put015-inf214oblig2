Bamboozled
 
 Bamboozle}}
{{Infobox film
| name = Bamboozled
| image = Bamboozled-2000-posterimg.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Spike Lee
| producer = Jon Kilik Spike Lee 
| writer = Spike Lee
| starring = Damon Wayans Savion Glover Jada Pinkett Smith Tommy Davidson Michael Rapaport
| music = Terence Blanchard
| cinematography = Ellen Kuras
| editing = Sam Pollard
| studio = 40 Acres and a Mule Filmworks
| distributor = New Line Cinema
| released =  
| runtime = 135 minutes
| country = United States
| language = English
| budget = $10 million
| gross = $2,463,650   
}}
 satirical film written and directed by Spike Lee about a modern televised minstrel show featuring black actors donning blackface makeup and the violent fall-out from the shows success. The film was given a limited release by New Line Cinema during the fall of 2000, and was released on DVD the following year.

==Plot==
 
Pierre Delacroix (whose real name is Peerless Dothan), (Damon Wayans) is an uptight, Harvard University-educated black man, working for a television network known as CNS (for "Continental Network System"). At work, he has to endure torment from his boss Thomas Dunwitty (Michael Rapaport), a tactless, boorish white man. Not only does Dunwitty talk like an urban black man, and use the word "nigger" repeatedly in conversations, he also proudly proclaims that he is more black than Delacroix and that he can use nigger since he is married to a black woman and has two mixed- race children. Dunwitty frequently rejects Delacroixs scripts for television series that portray black people in positive, intelligent scenarios, dismissing them as The Cosby Show|"Cosby clones".

Facing the necessity of either coming up with a hit black-centric show or being fired, Delacroix decides to aim for the latter. Delacroix would be in violation of his contract if he resigned, but getting fired would release him from it and allow him to seek work at another network. With help from his personal assistant Sloane Hopkins ( , extremely racist jokes and puns, and even offensively stereotyped Computer-generated imagery|CGI-animated cartoons that caricature the leading stars of the new show. 

Delacroix develops the program believing that the network would reject such over-the-top racism and fire him immediately. Delacroix and Hopkins decide to recruit two impoverished street performers, Manray (Savion Glover, named after American artist Man Ray) and Womack (Tommy Davidson) -- homeless squatters who regularly perform outside CNS headquarters building to star in the show. While Womack is horrified when Delacroix tells him details about the show, Manray willfully agrees to star in the show, seeing it as his big chance to become rich and famous for his tap-dancing skills.

To Delacroixs horror, not only does Dunwitty enthusiastically endorse the show, it also becomes hugely successful. As soon as the show premieres on television, Manray and Womack end up becoming big stars while Delacroix, contrary to his original stated intent, defends the show as being satirical. Delacroix quickly embraces the show and his newfound fame; he even wins awards for creating and writing the show, while Hopkins becomes horrified at the racist nightmare she has helped to unleash. 

In the meantime, an underground, militant rap group called the Mau Maus (presumably named after Mau Mau), led by Hopkins older brother Julius (Mos Def), becomes increasingly angry at the content of the show. Though they had earlier auditioned for the programs live band position and were rejected, the group plan to bring the show down using violence. 

Eventually, Womack quits, fed up with the show and Manrays increasing ego. Manray and Hopkins grow closer, which angers Delacroix. Delacroix tries to break up Manrays relationship with Hopkins by accusing her of sleeping with Manray to further her career. Delacroix reveals that Hopkins only got her position as his assistant by sleeping with him. The move backfires and drives Manray and Hopkins even closer.

Hopkins creates a tape of racist footage culled from assorted movies, cartoons, television shows, and newsreels to try to shame Delacroix into stopping production of the show, but he refuses to view the tape. After an argument with Delacroix over all these differences, as well as realizing he is being exploited, Manray defiantly announces that he will no longer wear blackface. He appears in front of the studio audience, who are all in blackface, during a TV taping and does his dance number in his regular clothing. The network executives immediately turn against Manray, and Dunwitty (who is also wearing blackface) personally fires him from the show and throws him out of the studio.

The Mau Maus kidnap Manray, and then announce a plan to publicly execute Manray on a live webcast. The authorities work feverishly to track down the source of the internet feed, but Manray is nevertheless assassinated while doing his famous tap dancing (as a sort of sacrificial figure at his death). At his office, Delacroix (now in blackface make-up himself, mourning Manrays death) begins to fantasize the various coon-themed antique collectibles in his office staring him down and coming to life and goes into a rage, destroying many of the racist collectibles. 

The police quickly catch The Mau Maus, shooting them down in a hail of bullets. The camera lingers on their corpses, especially female rapper Smooth Blaks (Charli Baltimore) corpse.  They leave only one survivor, a white member known as "One-Sixteenth Black" (MC Serch), who tearfully proclaims that he is "black" and demands to die with the rest of his group instead of being arrested. 

Furious, Hopkins confronts Delacroix at gunpoint with her brothers revolver and demands that he watch the tape she prepared for him. Delacroix, after watching the tape, tries to get the gun, but is shot in the stomach. Hopkins, horrified, flees while proclaiming that it was Delacroixs own fault that he got shot. Delacroix, after positioning the gun to make the gunshot wound to the stomach appear self-inflicted, watches the tape as he lies dying on the floor.
 montage of Gone with Babes in Holiday Inn, Judge Priest, Ub Iwerks cartoon Little Black Sambo, Walter Lantzs cartoon Scrub Me Mama with a Boogie Beat, the Screen Songs short Jingle Jangle Jungle, the Merrie Melodies short All This and Rabbit Stew, and, from the Hal Roach comedy Schools Out, Our Gang kids Allen "Farina" Hoskins and Matthew "Stymie" Beard. After the montage, as the cameras point to Delacroixs dead body on the floor, the camera then shows Manray doing his last Mantan sequence on stage.

==Cast==
* Damon Wayans as Pierre Delacroix/Peerless Dothan
* Savion Glover as Manray/"Mantan"
* Jada Pinkett Smith as Sloan Hopkins
* Tommy Davidson as Womack/"Sleep n Eat"
* Michael Rapaport as Thomas Dunwitty
* Mos Def as Julius Hopkins/"Big Blak Afrika"
* Thomas Jefferson Byrd as "Honeycutt" Paul Mooney as Junebug
* Gano Grills as "Double Blak"
* Canibus as "Mo Blak"
* Charli Baltimore as "Smooth Blak"
* MC Serch as "One-Sixteenth Blak"
* The Roots as The Alabama Porch Monkeys

==Production==
Most of the film was shot on   film stock. Spike Lee|Lee, Spike (2001). Audio commentary for Bamboozled. New Line Home Entertainment. 

==Soundtrack==
 
The soundtrack album for the film was released September 26, 2000 by Motown Records. The album consisted of hip hop and contemporary R&B, and was India.Aries first time on an album, with six singles.

==Reception==
Bamboozled received mixed reviews;    it currently holds a 48% rotten rating on Rotten Tomatoes, with the consensus "Bamboozled is too over the top in its satire and comes across as more messy and overwrought than biting." 

==Box office==
The movie earned only $2,463,650 on a $10 million budget.  

==See also==
* Color Adjustment - a 1992 documentary film by Marlon Riggs about the portrayal of blacks in television
* Melvin Van Peebles Classified X - a 1998 documentary film by Mark Daniels and Melvin Van Peebles about the history of blacks in cinema.

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 