Tokumei Sentai Go-Busters the Movie: Protect the Tokyo Enetower!
{{Infobox film
| name = Tokumei Sentai Go-Busters the Movie: Protect the Tokyo Enetower!
| film name = {{Film name| kanji = 特命戦隊ゴーバスターズ THE MOVIE 東京エネタワーを守れ!
| romaji = Tokumei Sentai Gōbasutāzu Za Mūbī Tōkyō Enetawā o Mamore!}}
| image          = Fourze & Go-Busters.jpg
| alt            = 
| caption        = The shared poster for Fourze the Movie and Go-Busters the Movie
| director = Takayuki Shibasaki
| producer =  
| writer = Yasuko Kobayashi
| narrator = Shoo Munakata
| starring =  
| music = Megumi Ōhashi
| cinematography = Fumio Matsumura
| editing = 
| studio  =  
| distributor = Toei Company
| released =  
| runtime = 27 minutes
| country = Japan
| language = Japanese
| italic title = force
}}
  is the theatrical release for the 36th Super Sentai Series  .  Tokyo Towers mascot character   will also make an appearance in the film. 

==Plot==
Meeting Ryuji Iwasaki, Yoko Usami, and their Buddyloid partners, Hiromu Sakurada and Cheeda Nick join them in the maiden voyage of the FS-0O, an original Buster Machine, and its Buddyloid Ene-tan. But upon being altered to an Enetron disruption in the Akeisho District, where the Enetron regulation system   is located, Hiromu takes control of the FS-0O to finds a group of Bulgers gathering water for the Metaloid Steamloid. The Go-Busters battle the Buglers with the Buddyloids covering the civilians before Steamloid shoots off a steam that corrodes the Buddyloids bodies. As the Buddyloids are being tended to, learning of their enemys ability that explained why no Valgass Megazord is being transported,  the Go-Busters find that Tokyo Enetower is emitting transport energy. By then, Masato Jin arrives and points the team to an item on the tower that would teleport the surrounding area to subspace for its Enetron Tanks once fully powered. With Masato unable to help as Beet J. Stag falls victim to Steamloids steam, Ene-tan offers her aid to get the Go-Busters to Tokyo Enetower to defeat the Metaloid before deploying the Buster Machines.

Though Enter states their futility against the legions of Buglers, Blue Buster purposely overheats to take out the Buglers so Red Buster and Yellow Buster can fight their way through with the former battling Steamloid and managing to plug the Metaloids pipes. But when the Go-Busters Weak Points take effect, they are nearly killed when the FS-0O comes to their aid to take the Go-Busters to safety. After having their wounds tended to, with Steamloids mist fading so their Megazords can fight, the Go-Busters learn that Enter is using some of the Enetron amassed in the Tokyo Enetower to bring in the four Megazord archetypes and a fifth mysterious model. With three minutes left, the Go-Busters quickly destroy Steamloid with forty seconds left to get into their Buster Machines before the four Valgass Megazords arrive.

With two minutes left, GT-02 Gorilla knocks the Vaglass Megazords down to climb the tower while Go-Buster Ace uses the RH-03 to take the tower from above. However, Enter counters by piloting Megazord Type Epsilon and sends Go-Buster Ace and the GT-02 into the bay. Luckily, with the SJ-05 aiding them, Go-Buster Ace and the GT-2 combined with the FS-0O into Go-Buster Kero-Oh. Engaging Megazord Epsilon after taking out Megazord Gamma, Go-Buster Kero-Oh breaks up with Go-Buster Ace destroys Megazord Epsilon while the other Busters destroy the Vaglass Megazords as the FS-0O destroys the teleportation device before the time runs out. However, a new problem rises from the FS-0O damaging Tokyo Enetower with the other Buster Machines repairing the tower. As Enter washes ashore while vowing revenge, the Go-Busters and their Buddyloids go sight seeing at Tokyo Enetower.

==Cast==
* Hiromu Sakurada:  
* Ryuji Iwasaki:  
* Yoko Usami:  
* Cheeda Nick (Voice):  
* Gorisaki Banana (Voice):  
* Usada Lettuce (Voice):  
* Masato Jin:  
* Beet J. Stag (Voice):  
* Ene-tan (Voice):   
* Takeshi Kuroki:  
* Toru Morishita:  
* Miho Nakamura:  
* Enter:  
* Steamloid (Voice):   
* Narration:  

==Songs==
;Ending theme
* 
**Lyrics: Shoko Fujibayashi
**Composition, Arrangement: Kenichiro Oishi Katsuhiro Suzuki & Keiji Fujiwara, Ryouma Baba & Tesshō Genda, Arisa Komiya & Tatsuhisa Suzuki

==References==
 

==External links==
*  &  

 

 
 
 
 
 