Twin Faces
{{Infobox film
| name =  Twin Faces
| image =
| image_size =
| caption =
| director = Lawrence Huntington
| producer =J. Steven Edwards  
| writer =  Gerald Elliott   Douglas Reekie
| narrator = Anthony Ireland Paul Neville
| music = 
| cinematography = 
| editing = 
| studio = Premier Sound Films 
| distributor = Paramount British Pictures
| released = August 1937
| runtime = 67 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Anthony Ireland, Francesca Bahrle and Frank Birch. The film was made at Highbury Studios as a quota quickie for release by the Hollywood studio Paramount Pictures.  It is also known by the alternative title Press Button B.

==Cast==
*  Anthony Ireland (actor)| Anthony Ireland as Jimmy the Climber  
* Francesca Bahrle as Judy Strangeways  
* Frank Birch as Ben Zwigi   Paul Neville as Cmdr. Strangeways   Victor Hagen as Inspector Coates   George Turner as Maurice  
* Ivan Wilmot as Levenstein  
* Frank Tickle as John Cedar

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 

 