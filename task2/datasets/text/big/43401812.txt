Barquero
 
{{Infobox film
| name     = Barquero
| image    = Gordon Douglas
| producer =
| writer   =
| starring = Lee Van Cleef   Warren Oates
| cinematography =
| music =
| country = United States
| language = English
| runtime = 115 min
| released =  
}}
 Gordon Douglas.

After stealing a shipment of Silver and weapons, the brutal and unstable Remy (Warren Oates) and his band of mercenaries must cross the river in order to flee into Mexico. The boatman of a small settlement, Travis (Lee Van Cleef), refuses to transport the gang on his barge, knowing that to do so would be suicide. A tense standoff develops between the gang and the settlers who are on opposite sides of the river which explodes into a violent and bloody climax.

== Cast ==
* Lee Van Cleef - Travis
* Warren Oates - Remy
* Forrest Tucker - Mountain Phil
* Kerwin Mathews - Marquette
* Mariette Hartley - Anna
* Marie Gomez - Nola
* Armando Silvestre - Sawyer

== External links ==
* 

==References==
 

 
 

 