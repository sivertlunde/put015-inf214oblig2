Padichakallan
{{Infobox film 
| name           = Padichakallan
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = AL Sreenivasan
| writer         = S. L. Puram Sadanandan
| screenplay     = S. L. Puram Sadanandan
| starring       = Prem Nazir Adoor Bhasi Manavalan Joseph Pappukutty Bhagavathar
| music          = G. Devarajan
| cinematography = V Selvaraj
| editing        = VP Krishnan
| studio         = ALS Productions
| distributor    = ALS Productions
| released       =  
| country        = India Malayalam
}}
 1969 Cinema Indian Malayalam Malayalam film, directed by M. Krishnan Nair (director)|M. Krishnan Nairand produced by AL Sreenivasan. The film stars Prem Nazir, Adoor Bhasi, Manavalan Joseph and Pappukutty Bhagavathar in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Prem Nazir
*Adoor Bhasi
*Manavalan Joseph
*Pappukutty Bhagavathar
*Sreelatha Namboothiri
*T. R. Omana
*Aranmula Ponnamma
*Bharathi Vishnuvardhan
*K. P. Ummer
*Krishnan Latha
*Paravoor Bharathan
*Radhika
*Snehalatha
*T. K. Balachandran
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kandu Kothichu || LR Eeswari || Vayalar Ramavarma || 
|-
| 2 || Kannente Mukhathottu || CO Anto || Vayalar Ramavarma || 
|-
| 3 || Kilukilukkaam Kili || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Manassum Manassum || K. J. Yesudas, LR Eeswari || Vayalar Ramavarma || 
|-
| 5 || Thaananilathe Neerodu || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 6 || Urakkam Varaatha || K. J. Yesudas, P Susheela || Vayalar Ramavarma || 
|-
| 7 || Vidhimunpe Nizhal || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 