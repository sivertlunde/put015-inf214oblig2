Ober Da Bakod
{{infobox television |
| show_name = Ober Da Bakod
| image =  
| caption = 
| picture_format = 480i SDTV
| runtime = 1 hour
| creator = Viva Television
| executive_producer(s) = Lenny C. Parto 
| developer= GMA Network
| starring = Janno Gibbs   Leo Martinez   Anjo Yllana   Donita Rose   Gelli de Belen   Donna Cruz   Malou de Guzman 
| country = Philippines
| network = GMA Network
| first_aired =  
| last_aired =   
| num_episodes = 
}}
 
{{Infobox film
| name           = Ober Da Bakod:The Movie
| producer       = Executive Producer: Vic del Rosario
| starring       = Janno Gibbs Anjo Yllana Leo Martinez Gelli de Belen Donita Rose Malou de Guzman Anjanette Abayari Michelle Aldana Donna Cruz Angelu de Leon Viva Films GMA Network
| released       = 1994
| country        = Philippines
| language       = Filipino Tagalog}}
 Philippine sitcom Viva Television and co-produced and broadcast by GMA Network from 1992–1997.

==Plot==
The brothers Mokong and Bubuli (sons of Lucring Dayukdok) have feelings for the rich sisters Honey Grace and Barbie Doll (daughters of Don Robert Dinero), Mokong for Honey Grace and Bubuli for Barbie Doll. Don Robert doesn’t like the brothers because they are poor. So that they can be free to court Honey Grace and Barbie Doll, Mokong and Bubuli climb over the fence.

Don Robert always makes plans to prevent Mokong and Bubuli to get close to his daughters. Nonetheless, he fails due to the intervention of his mortal enemy and former girlfriend, Aling Lucring who happens to be Mokong and Bubulis overprotective mother.  As a result, total mayhem ensues between the two families.

A running gag of the series is that in the end of every episode, two policemen, Officers Tedera and Officer Ginto, arrest Don Robert based on whatever schemes he had concocted.  As Don Robert tries to explain his way out of this arrest, the police officers always tell him, "Sa presinto ka na lang magpaliwanag." ("Explain everything at the precinct.")  Those words have now become a semi-common Filipino catchphrase.

==Show information==

===Cast===
* Janno Gibbs as Mokong
* Anjo Yllana as Bubuli
* Donita Rose as Barbie Doll
* Gelli de Belen as Honey Grace
* Malou de Guzman as Lucring
* Leo Martinez as Don Robert

===Other Cast===
* Danny “Brownie” Pansalin as Brownie
* Donna Cruz as Muning
* Dale Villar as Flip
* Angelu de Leon as Kuting
* Manilyn Reynes as Manirella/ Kasoy
* Amanda Page as Miss Quickie
* Onemig Bondoc as Bubwit
* Ruffa Mae Quinto as Pegassu
* Assunta de Rossi
* Randy Santiago as Mike
* Mikee Cojuangco
* Lindsay Custodio
* Antoinette Taus
* Alvin G. Basco as Onin
* Mario Guinto as Policeman
* Edgar Tedero as Policeman

===Staff===
* Director: Ariel Ureta Viva Television GMA Productions (now Alta Productions Group, Inc.)

==See also==
*List of shows previously aired by GMA Network
*GMA Network

==External links==
*  on Internet Movie Database
*  on Internet Movie Database

 
 
 
 
 
 
 
 
 
 
 