Raped with Eyes: Daydream
{{Infobox film
| name = Raped with Eyes: Daydream
| image = Raped with Eyes - Daydream.jpg
| image_size =
| caption = Theatrical poster for Raped with Eyes: Daydream (1982)
| director = Toshiyuki Mizutani 
| producer = 
| writer = Toshiyuki Mizutani
| narrator = 
| starring = Kazuhiro Yamaji Shinobu Nami Makoto Yoshino
| music = Hiroki Sakaguchi
| cinematography = Yūichi Nagata
| editing = 
| studio = Takahashi Productions 
| distributor = Shintōhō
| released = September 1982
| runtime = 60 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1982 Japanese pink film directed by Toshiyuki Mizutani.

==Synopsis==
Kazu is a man who indulges in daydream fantasies of sexual dalliances with women. As his fantasies become increasingly violent and even homicidal, neither he nor the films audience is sure whether these are really harmless, if perverse, fantasies or if they are really happening.   

==Cast==
* Kazuhiro Yamaji ( ) as Kazu    
* Shinobu Nami ( )
* Makoto Yoshino ( )
* Mika Hijiri ( )
* Seiko Munakata ( )

==Background==
Director Toshiyuki Mizutanis first film as was Lust Hunting: Office Lady Rape (also 1982), which Thomas and Yuko Mihara Weisser label a "wicked debut". For his interest in cinematic violence critics dubbed him "The Pink Demon". Raped with Eyes: Daydream is Mizutanis best-known work.  With this film, Mizutani added the elements of surrealism and fantasy which enabled him to distance himself and the audience from the on-screen mayhem and to add a further level of meaning to the scenario. The Weissers write, "The psychotic perspective of the film compensates for the offensive message." 

==Availability==
Toshiyuki Mizutani filmed Raped with Eyes: Daydream for director Banmei Takahashis Takahashi Productions,  and it was released theatrically in Japan by Shintōhō in September 1982.  On May 21, 1999 under the title  , Arena Entertainment released Raped with Eyes: Daydream on DVD as the second in their  . 

==Bibliography==

===English===
*  
*  
*  

===Japanese===
*  
*  

==Notes==
 

 
 
 
 
 


 
 