Ek Phool Teen Kante
{{Infobox film
| name           = Ek Phool Teen Kaante
| image          = 
| image_size     =
| caption        =
| director       = Anup Malik
| producer       = 
| writer         = 
| narrator       =
| starring       = Vikas Bhalla  Monica Bedi
| music          = Jatin Lalit
| cinematography = 
| editing        = 
| distributor    = 
| released       = August 29, 1997
| runtime        = 
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} Indian Bollywood film directed by Anup Malik and produced by Deven Tanna. It stars Vikas Bhalla and Monica Bedi in pivotal roles. It was released on August 29, 1997. 
 

==Soundtrack==

{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Yeh Kaisa Nasha"
| Kumar Sanu, Alka Yagnik
|- 
| 2
| "Chehra Na Dekhungi"
| Kavita Krishnamurthy
|- 
| 3
| "Sun O Sherawali"
| Vinod Rathod, Kavita Krishnamurthy
|- 
| 4
| "Abe Ab Kya Hua"
| Babul Supriyo, Sudesh Bhosle
|- 
| 5
| "Yeh Ladki Badi" 
| Abhijeet Bhattacharya|Abhijeet, Alka Yagnik
|- 
| 6 
|"Jaanam Mere Humdum Meri Zindagi"
| Kumar Sanu, Alka Yagnik
|}

==References==
 

==External links==
* 

 
 
 
 

 