China Strike Force
 
 
{{Infobox film
| name           = China Strike Force
| image          = China-Strike-Force-poster.jpg
| caption        = Hong Kong poster
| film name = {{Film name| traditional    = 雷霆戰警
 | simplified     = 雷霆战警}}
| director       = Stanley Tong
| producer       = Andre E. Morgan Barbie Tung
| writer         = Steven Whitney Stanley Tong Paul Chun
| music          = Nathan Wang
| cinematography = Jeffrey C. Mygatt
| editing        = 
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Hong Kong
| language       = English Cantonese
| budget         = 
| gross          = HK$ 20,513,906
}} 2000 Hong Hong Kong action film starring Aaron Kwok, Norika Fujiwara, Leehom Wang and Ruby Lin. It was directed by Stanley Tong and written by Steven Whitney and Stanley Tong.

==Synopsis==
Two young Chinese Police Officers, Darren (Aaron Kwok) and Alex (Leehom Wang) investigate a murder they witnessed.

==Production==
China Strike Force was shot in both Mandarin and English. The director Stanley Tong shot the film in Shanghai as the location as he found it cheaper than shooting the film in Hong Kong and he also wanted to get to know mainland China better. 

==Cast==
* Aaron Kwok as Darren Tong
* Norika Fujiwara as Norika
* Coolio as Coolio
* Mark Dacascos as Tony Lau Alexander Leehom Wang as Alex Cheung
* Ruby Lin as Ruby Lin Paul Chun as Sheriff Lin
* Lau Siu-Ming as Uncle Ma
* Ken Lo as Ken
* Jennifer Lin as Jennifer
* Jung Yuen as Lee (Coolios bodyguard)
* Tony Wang as Mr. Leuong (Sheriff Lins assistant)
* Benny Lai
* Jackson Liu
* Paul Cheng
* Tung Li-Hsueh

 
 

==Release== 20th Annual Hong Kong Film Awards, the film was nominated for Best New Performer (Wang Lee Hom), Best Action Choreography (Stanley Tong and Ailen Sit Chun Wai) and Best Sound Design (Paul Pirola). 

==References==
 

==External links==
* 
* 
*   at the Disobiki.

 

 
 
 
 
 
 
 
 
 

 