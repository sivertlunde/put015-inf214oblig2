Resurrection (1927 film)
{{Infobox film 
| name = Resurrection
|image=File:Ressurection poster.jpg
|caption=Film poster
| director = Edwin Carewe
| producer = Edwin Carewe
| writer = Edwin Carewe Finis Fox Leon Tolstoi (Novel)
| starring = Dolores del Río Rod La Rocque Rita Carewe Matt MacDermott
| music = 
| cinematography = Robert Kurrle
| editing = Jeanne Spencer
| distributor = Edwin Carewe Productions
| released =  
| runtime = 100 minutes
| country = United States
| language = English 
| budget = 
}}
Resurrection is a 1927 Hollywood adaptation of the Leo Tolstoy novel Resurrection (novel)|Resurrection.  Filmmaker Edwin Carewe adapted the book to a feature length silent production starring Dolores del Río and featuring an appearance by Ilya Tolstoy.  In 1931 in film|1931, Edwin Carewe directed an Resurrection (1931 English-language film)|all-talking remake of this film starred by Lupe Vélez.

==Plot==
Katyusha, a country girl, is seduced and abandoned by Prince Dimitry. Dimitry finds himself, years later, on a jury trying the same Katyusha for a crime he now realizes his actions drove her to. He follows her to imprisonment in Siberia, intent on redeeming her and himself as well. 

==Cast==

===1927 version===
* Dolores del Río as Katyusha Maslova
* Rod La Rocque as Prince Dimitry Ivanich
* Lucy Beaumont as Aunt Sophya
* Vera Lewis as Aunt Marya

===1931 English version===
* Lupe Vélez as Katyusha Maslova John Boles as Price Dimitry Ivanich
* Rose Tapley as Aunt Sophya
* Nance ONeil as Aunt Marya

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 