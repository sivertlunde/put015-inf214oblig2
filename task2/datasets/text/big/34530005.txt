I Will Repay (film)
 
{{Infobox film
| name           = I Will Repay
| image          =
| caption        =
| director       = Henry Kolker
| producer       =
| writer         = Emma Orczy  Isabel Johnston   Kinchen Wood
| starring       = Holmes Herbert Flora le Breton Pedro de Cordoba   Ivan Samson
| music          =
| cinematography =
| editing        =
| studio         = Ideal Film Company FBO (US)
| released       = 1923 (UK)   12 July 1924 (US)
| runtime        = 70 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} silent period I Will Repay by Emma Orczy, which is a sequel to The Scarlet Pimpernel (part of a large series of such novels). It was released under the alternative title Swords and the Woman

==Plot==

The film takes place in Paris, 1793: durung the Reign of Terror. Paul de Roulade (Pedro de Cordoba) is a hero to the Proletariat, who cry him Citizen de Roulade. One evening he gets into an argument with the Vicomte de Marny over the virtues of a dancing girl; this leads to a fight (provoked by the viscount), forcing Paul to kill the nobleman. The viscounts sister Juliette de Marny (Flora le Breton) swears revenge: to achieve this, she infiltrates the de Roulade household and beguiles Paul into falling in love with her.

It is revealed that Paul has secret Royalist sympathies and, wary of the bloodshed, he decides to smuggle Marie Antoinette out of France . To do this, he enlists the aid of none other than the Scarlet Pimpernel (Holmes Herbert).

When Juliette learns of Pauls intrigue, she realises that this is her chance to get revenge by betraying him to the Jacquards. Or will she fall in love with him as he has with her? Meanwhile, we get a few scenes of Charlotte Corday on trial for the murder of Marat.

==Cast==
* Holmes Herbert - Sir Percy Blakeney
* Flora le Breton - Juliette de Mornay
* Pedro de Cordoba - Paul Deroulede
* Ivan Samson - Viscount de Mornay
* A.B. Imeson - Tinville
* Georges Tréville - Duc de Mornay Robert Lang - Villefranche

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 

 