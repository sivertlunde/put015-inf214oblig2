Ice Princess
 
{{Infobox film
| name           = Ice Princess
| image          = Ice Princess.jpg
| director       = Tim Fywell   
| producer       = Bridget Johnson
| screenplay     = Hadley Davis
| story          = Meg Cabot Hadley Davis
| starring       = Joan Cusack Kim Cattrall Michelle Trachtenberg Hayden Panettiere
| music          = Christophe Beck
| cinematography = David Hennings
| editing        = Janice Hampton
| studio         = Walt Disney Pictures Bridget Johnson Films On The Ice Productions Buena Vista Pictures
| country        = United States
| released       = 
| runtime        = 98 minutes
| language       = English
| gross          = $27,645,491
| budget         = $25 million 
}}
Ice Princess is a 2005 American figure-skating film directed by Tim Fywell, written by Hadley Davis from a story by Meg Cabot and Davis, and starring Michelle Trachtenberg, Joan Cusack, Kim Cattrall and Hayden Panettiere. The film focuses on Casey Carlyle, a normal teenager who gives up a promising future academic life in order to pursue her new-found dream of being a professional figure skater. The film was released on March 18, 2005. Ice Princess had a unsuccessful performance at the box office, grossing $24 million in the United States during its theatrical run against a production budget of $25 million.

==Plot==
Seventeen-year-old Casey Carlyle (Michelle Trachtenberg), a very smart and talented science student, plans to pursue a scholarship to Harvard University. For the scholarship, Casey must present a personal summer project about physics. While watching a figure skating competition with her mathematically inclined friend Ann, Casey realizes that her favorite childhood hobby, ice skating, would make a perfect project. She decides to try to improve her own skating by applying physics and what she has discovered from watching other skaters.

She becomes proficient and skips two levels to become a junior skater. She helps junior skaters Gennifer Harwood (Hayden Panettiere), Tiffany Lai, and Nikki Fletcher (Kirsten Olson) improve their skating. Torn between her Harvard dream and her growing love of skating, Casey has difficulty juggling schoolwork, skating, and a part-time job. Her mother, Joan (Joan Cusack), realizes that skating is affecting Caseys school performance and insists that she stop, but Casey refuses. Tension arises between Caseys coach Tina Harwood (Kim Cattrall), a disgraced former skater, and her seventeen-year-old daughter Gennifer.

Tina, who manages the rink where Casey trains, puts Gennifer on a strict training program. Frustrated by the new restrictions, Gen quits after Tina attempts to sabotage Caseys performance during a competition. Casey withdraws from the Harvard scholarship competition to devote herself to skating. Despite her betrayal, Casey asks Tina to be her personal coach and help her train for Figure skating competition#Qualifying competitions in the United States|sectionals. Their collaboration is a successful one.

During sectionals, Casey slips and falls while attempting a triple Salchow jump|Salchow. Discouraged, she sees Joan in the stands, and the realization that her mother has finally embraced Caseys dream boosts her confidence. She recovers and gives a highly-rated artistic performance. Casey comes in second place behind Nikki, and is going to the nationals. Gens brother, Teddy (Trevor Blumas), gives Casey flowers to congratulate her, and they share a passionate kiss. The film ends with Joan and Tina playfully arguing about how many college courses Casey should take, discussing Teddy and Caseys budding Romance (love)|romance, Caseys sponsors, and Caseys future in figure skating.

==Cast==
*Michelle Trachtenberg as Casey Carlyle
*Joan Cusack as Joan Carlyle
*Kim Cattrall as Tina Harwood
*Hayden Panettiere as Gennifer "Gen" Harwood
*Trevor Blumas as Teddy Harwood
*Juliana Cannarozzo as Zoey Bloch
*Erik King as Chip Healey
*Diego Klattenhoff as Kyle Dayton
*Jocelyn Lai as Tiffany Lai
*Shanique Ollivierre-Lake as Chantal DeGroat
*Paul Sun-Hyung Lee as Mr. Lai
*Martha MacIsaac as Mean Party Girl
*Kirsten Olson as Nikki Fletcher
*Connie Ray as Mrs. Fletcher
*Signe Ronka as Emma Flanders
*Amy Stewart as Ann
*Kristina Whitcomb as Ms. Fisher Lee
*Michelle Kwan as woman ESPN reporter

==Production==
Blumas said that he was put on hold for two months during the audition process, and that there had been “a lot of switch-overs with the directors”.    Blumas ended up playing Teddy as a sort of father figure.  He began training to drive a Zamboni soon after arriving in Toronto; according to him, he later ended up smoothing the ice on some mornings at the rink where they were shooting.  Panettiere did much of her own skating, including a fast spin seen at the end of the film.  Trachtenberg trained for eight months, including the time they were filming (during which time she says she worked twenty-hour days).  She had to be on the ice longer than most of the other actors as she was one of the few adults on the film.    She had stunt doubles to handle the falls and some of the complex moves,  although Trachtenberg did learn a specific move that could not be done by a stunt double as the differences in their build would be apparent. She sustained some injuries while working on the film.  According to Trachtenberg, a mistake was made in one of the physics formulas her character recites, which was later fixed; a shot of the back of her head was used and the correct term was looped in.    Trachtenberg described the film as “not a Disney kitschy movie” and was somewhat apprehensive of the idea of a sequel for fear of belittling the original.  Cusack noted that the relationship between Casey and her mother had already been well-developed in the script, but said that it generated a good deal of discussion during the production, and Cusack ultimately described her role as "meaningful" in terms of the acting and also how it related to her personally. 
 De La Salle College.   

==Reception==
===Box office===
In its opening weekend, the film grossed $6,807,471 million in 2,501 theaters in the United States and Canada, ranking #4 at the box office, behind The Ring Two, Robots (2005 film)|Robots and The Pacifier. By the end of its run, Ice Princess grossed $24,402,491 domestically and $3,243,000 internationally, totaling $27,645,491 worldwide.   

===Critical reception===
Film critic Roger Ebert gives Ice Princess three out of four stars and commended the film for its entertaining nature and ability to overcome cliche and "formula".  Reactions from other critics have been mixed, as 52% of the T-meter critics on Rotten Tomatoes reviewed the film positively.  Todd Gilchrist of IGN questioned the speed at which Casey becomes adept at skating and pointed out some other improbabilities and clichés, but strongly praised Cusack’s and Cattrall’s performances as emotionally powerful and fully human.  United States Conference of Catholic Bishops Office for Film and Broadcasting rated the film A-I (suitable for general patronage) and provided the film a modest praise as a good family film.  Oppenheim Toy Portfolio awarded the film their platinum award.  It is rated G by the Motion Picture Association of America.

==Soundtrack==
{{Infobox album
| Name        = Ice Princess Original Soundtrack
| Type        = Soundtrack
| Artist      = Various artists
| Cover       = 
| Released    = March 15, 2005
| Recorded    = 
| Genre       = Pop, dance-pop, teen pop
| Length      = 45:37 Walt Disney
| Producer    = Desmond Child, Matthew Gerrard, Greg Kurstin, Jamie Houston, Leah Haywood, Daniel James   |title=Ice Princess - Original Soundtrack : Songs, Reviews, Credits, Awards |publisher=AllMusic|date=2005-03-15 |accessdate=2012-08-08}} 
}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =   
}} Aly & AJ, Jesse McCartney, and Raven-Symoné, and various other artists. It peaked at number 53 on the Billboard 200|Billboard 200 and at number 2 on Top Soundtracks.

;Track listing
#Reach - Caleigh Peters
#If I Had It My Way - Emma Roberts
#Get Your Shine On - Jesse McCartney
#You Set Me Free - Michelle Branch
#Reachin for Heaven - Diana DeGarmo No One Aly & AJ
#Its Oh So Quiet - Lucy Woodward
#Get Up - Superchick
#I Fly - Hayden Panettiere
#Just a Dream - Jump5 Bump - Raven-Symoné
#There Is No Alternative - Tina Sugandh Unwritten - Natasha Bedingfield

;Not included on the soundtrack. Madonna appears in the background of the Casey Carlyles scene of the regional competition, but is not included on the soundtrack.
* "Freak Out" by Avril Lavigne appears in the official trailer, but is not included on the soundtrack.
* "Toxic (song)|Toxic" by Britney Spears was included in the ice skating scene from Zoe, but wasnt included on the soundtrack.
{| class="wikitable sortable"
|+ Chart positions for Ice Princess Original Soundtrack
|-
! Chart (2005) !! Peak position
|- Billboard 200|Billboard 200
| style="text-align:center;"| 53  
|- Top Soundtracks|Billboard Top Soundtracks
| style="text-align:center;"| 2
|}

==References==
 

==External links==
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 
 
 