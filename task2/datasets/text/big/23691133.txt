Mr. Payback: An Interactive Movie
{{Infobox film
| name           = Mr. Payback: An Interactive Movie
| image          = Mr-Payback.jpg
| image_size     =
| alt            = 
| caption        = 
| director       = Bob Gale, Charles Croughwell
| producer       = Mark Franco II, Jeremiah Samuels, Holly Keenan II  
| writer         = Bob Gale
| narrator       = 
| starring       = Billy Warlock, Leslie Easterbrook, Christopher Lloyd,  Bruce McGill, Holly Fields II  
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Sony New Technologies
| released       = 1995  
| runtime        = approximately 20 to 30 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} interactive movie, the story Back to the Future trilogy, worked on this film as well, and the music was scored by Michael Tavera, who had composed the music for the animated Back to the Future series.

==Cast==
*Billy Warlock—Payton Bach (Mr. Payback)
*Holly Fields—Gwen
*Bruce McGill—James Konklin
*Christopher Lloyd—Ed Jarvis
*Leslie Easterbrook—Diane Wyatt
*David Correia—Raoul Alvarez
*Victor Love—Lloyd Braxton
*Carol-Ann Plante—Cara Cook
*Michael Talbott—Car Jerk
*Brendan Ford—Park Vandal
*Gilbert Rosales—Bike Thief
*Eddie Deezen - Phil the Guard
*Robby Sutton—Moe
*Sasha Jenson—Larry
*Joseph D. Reitman—Dick

==Critical reception==
Mr. Payback received negative reviews from film critic|critics. Roger Ebert of the Chicago Sun Times gave the film a mere half-star out of a possible four, and called it "the kind of film where horrified parents might encourage the kids to shout at the screen, hoping the noise might drown out the flood of garbage."    Ebert also commented on how movies should act on you, not the other way around. Ebert later called it the worst movie of 1995. Gene Siskel also disliked the film, thus leading to it obtaining two thumbs down on Siskel & Ebert; later on their worst of show for the year of release, he commented that the other option is to play a video game instead, rather than try to mix both of them together (since the latter will not work). Ty Burr of Entertainment Weekly tagged the film with an "F" grade. 

==References==
 

== External links ==
*  
*    (Article on Mr. Payback)

 
 
 


 