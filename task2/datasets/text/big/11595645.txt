The Tempest (1979 film)
 
 
{{Infobox film
| name           = The Tempest
| image          = The Tempest (1979 film) poster.jpg
| image_size     =
| caption        =
| director       = Derek Jarman
| producer       = Guy Ford Mordecai Schreiber
| writer         = Adaptation: Derek Jarman Play: William Shakespeare
| narrator       =
| starring       = Heathcote Williams Toyah Willcox Karl Johnson Peter Bull Richard Warwick John Lewis
| cinematography = Peter Middleton
| editing        = Lesley Walker
| distributor    =
| released       =  
| runtime        = 92 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 1979 film play of the same name. Directed by Derek Jarman, with Heathcote Williams as Prospero, it also stars Toyah Willcox, Jack Birkett and Helen Wellington-Lloyd from Jarmans previous feature, Jubilee (film)|Jubilee (1977 in film|1977), as well as his long-time cohort Karl Johnson. 

==See also==
*The Tempest (1911 film)|The Tempest (1911 film)
*Tempest (1982 film)|Tempest (1982 film)
*The Tempest (2010 film)|The Tempest (2010 film)
*List of William Shakespeare film adaptations

==References==
 

== External links ==
* 
* 

 
 

 

 
 
 
 
 
 

 