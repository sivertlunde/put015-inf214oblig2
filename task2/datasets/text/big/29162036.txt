The Train (2011 film)
{{Infobox film
| name = The Train
| image = The_Train_2011.jpg
| image_size =
| caption =
| director = Jayaraj
| producer =
| writer = Jayaraj
| narrator =
| starring = Mammootty Jayasurya Kota Srinivasa Rao Sheena Chohan Srinivas
| cinematography = Sinu Sidharth
| editing = Vivek Harshan
| studio = Harvest Dreams
| distributor =
| released =  
| runtime =
| country = India
| language = Malayalam
}}
The Train is a 2011 Malayalam thriller film written and directed by Jayaraj, and starring Mammootty, Jayasurya and Sheena Chohan. The film is based on the 11 July 2006 Mumbai train bombings. It was earlier titled as Track with Rahman.     It released on 27 May. It gets a dubbed release in Hindi and Tamil language|Tamil.  It was a disaster in the box office.

==Synopsis==
The film is based on the 11 July 2006 Mumbai train bombings. The film is a take on the lives of Malayalee families affected by the bomb blasts. Mammootty plays a police officer who is a keen observer. Jayasurya plays a singer who travelled on one of the trains. The story involves bomb blasts in seven different railways (stations and on train) .


==Cast==
*Mammootty as Kedarnath
*Jayasurya as Karthik
*Kota Srinivasa Rao as Tiwary
*Sheena Chohan as Kedarnaths wife
*Sabitha Jayaraj as Suhana
*Jagathy Sreekumar as Joseph
*Saikumar as Haneefa
*Salim Kumar as Ramu
*Anchal Sabharwal as Meera
*Zeenath as Suhanas Mother
*Valsala Menon as Allus Grandmother
*Charanpreet Singh as DJ Friend

== Soundtrack ==
The films soundtrack contains 7 songs, all composed by Srinivas (singer)|Srinivas. Lyrics by Rafeeq Ahamed and Jayaraj.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chirakengu  "
| Alka Ajith
|-
| 2
| "Chirakengu  "
| Arvind Venugopal, Sharanya Sreenivas]]
|-
| 3
| "Ithile Varoo  " Srinivas
|-
| 4
| "Ithile Varoo "
| Sujatha Mohan
|-
| 5
| "Ladki"
| Srinivas, Hishaam, Maya Sreecharan
|-
| 6
| "Naavoru"
| K. J. Yesudas, Latha Hentry, Sharanya Sreenivas
|-
| 7
| "O Saathiya"
| Javed Ali
|}

==Production==
The film was launched on 5 July 2010 in Mumbai by Harvest Dreams Private Limited. The movie is expected to be completed in three schedules.   

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 


 
 