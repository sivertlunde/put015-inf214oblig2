Jal (film)
 
 
{{Infobox film
| name           = Jal
| image          = Jal_Movie_Official_Poster.jpg
| alt            =  
| caption        = Official Movie Poster
| director       = Girish Malik
| producer       = Oneworld Films Pvt. Ltd. 
| screenplay     = Girish Malik Rakesh Mishra
| story          = Rakesh Mishra Yashpal Sharma Mukul Dev Saidah Jules
| music          = Sonu Nigam Bickram Ghosh
| cinematography = Sunita Radia
| editing        = 
| studio         = Intermezzo Studio Alien Sense Films Pvt Ltd
| distributor    = DAR Motion Pictures
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} water diviner named Bakka, who tries to solve the drought problems in his village, but faces unforeseen circumstances when he tries to help a female bird watcher save flamingos.   The film is directed by Girish Malik and is his debut film as a director. Busan International Film Festival 2013 and in the "Indian Panorama" section of the International Film Festival of India.  
The film released in India on 4 April 2014.  One World Films has collaborated with DAR Motion Pictures for the distribution of Jal in India.  The film has received the National Film Award for Best Special Effects for the year 2013 at the 61st National Film Awards.  Best Picture Best Original Score Category. The film advanced in the top 114 films in the Best Original Score Category. 

==Synopsis==
Jal revolves around a young man named Bakka (Purab Kohli), who is gifted with a special ability to find water in the desert.  With the backdrop of water scarcity, the film tells a complex and intriguing story of love, relationships, enmity, deceit and circumstances that bring about the dark side of human character. Jal is a high-octane, action-drama with a shocking climax. Shot on an epic scale, the film is a visual treat and has managed to capture Rann of Kutch like never before.

==Cast==
* Purab Kohli 
* Tannishtha Chatterjee  
* Kirti Kulhari Yashpal Sharma  
* Mukul Dev 
* Saidah Jules
* Ravi Gossain Rahul Singh
* Gary Richardson
* Rohit Pathak
* Bharat Hathi

==Music==
The music of the film is composed by the legendary singer Sonu Nigam and ace tabla player/percussionist Bickram Ghosh.  This is their first composing collaboration for a Bollywood film.  
 Marwa and Puriya combined with rare musical instruments like the Armenian Duduk. The entire song was created in a day. 
 Best Original Score Category. 

==Soundtrack==
{{Infobox album 
| Name = Jal
| Type = Soundtrack
| Artist = Sonu Nigam – Bickram Ghosh
| Cover = 
| Border         =
| Alt            =
| Caption        =
| Released = 19 March 2014 (India) Feature Film Soundtrack
| Recorded =
| Length = 
| Label = Saga Music & Unisys Info Solutions
| Producer       = Sonu Nigam – Bickram Ghosh
| Chronology     = 
| Last album = Sooper Se Ooper (2013)
| This album = Jal (2014)
| Next album = Chowky (Film)|Chowky (2015)
| Misc    = 
}}
{| class="wikitable"
|-  style="background:#ccc; text-align:centr;"
| Track # || Title || Singer(s) || Lyrics || Composer(s)
|- Shubha Mudgal || Sonu Nigam – Sanjeev Tiwari || Sonu Nigam   Bickram Ghosh   
|-
|  2 || You Fill My Life|| Ustad Ghulam Mustafa Khan, Suzanne || Sonu Nigam – Bickram Ghosh || Sonu Nigam Bickram Ghosh 
|-
| 3 || Zaalima || Sonu Nigam|| Sonu Nigam|| Sonu Nigam Bickram Ghosh  
|-
| 4 || Paani Si Behti Jaye || Ambarish Das || Sanjeev Tiwari || Sonu Nigam Bickram Ghosh 
|-
| 5 || Bakka Pani De  || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 6 || The Mirage and the Tornado  || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 7 || The Sweet Dream of Water  || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 8 || Mesmerized By Rann || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 9 || The Tragedy of Rann  || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 10 || The Barren Land || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 11 || The Cry of the Soul || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 12 || Dried Up Tears || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 13 || Nature and the Machine || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 14 || Ankhiyan – Anthem || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 15 || The Water Ritual 1 || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 16 || The Water Ritual 2 || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 17 || The Hope of Water ||  Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 18 || The Flow of Water || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 19 || The Sorrow Within || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 20 || Bakka And Kesar Tryst With Water || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 21 || Chase || Instrumental ||  Instrumental || Sonu Nigam Bickram Ghosh 
|-
| 22 || Anger || Instrumental || Instrumental || Sonu Nigam Bickram Ghosh 
|}

==Critical reception==
* Film Critic Subhash K. Jha gave it 4 stars and wrote that Jal is a work of remarkable resonance and the sheer visual mastery of Girish Maliks directorial debut takes your breath away. 

* News Nation gave it 3.5 stars : Jal is an intriguing fiction film by Girish Malik which voices the harsh truth about how the scarcity of water is threatening the pink-legged birds, Flamingos and human beings alike during the harsh summers in the Rann of Kutch, India. 

* Times of India gave the film 3.5 stars : Jal captures the bare beauty of the golden cracked earth and its tortuous tapestry in artfully mounted frames (cinematography: Sunita Radia). Its a picture-perfect album with stark sights and parched souls. 

* Neha Gupta of Bollywood3 stated that the movie has been shot beautifully and Purab Kohli has proven his worth with this one too and looks absolutely effortless. Jal conveys an important message and the concept is also good. 

* Taran Adarsh from BollywoodHungama has rated 3.5/5 Stars and stated "JAL makes a rock-solid impact. Its poignant and powerful and I suggest, you take time out to watch this truly gripping fare" 

* FilmFare gave the film 4/5 stars and appreciated the film "Great visuals and premise desiccated by a pedantic pace". 

==References==
  {{cite news
|url=http://www.cinando.com/DefaultController.aspx?PageId=FicheFilm&IdC=27443&IdF=115838&lang=es
|title=Water (JAL)
|date=
|work=cinando.com
|accessdate=29 November 2011}}  {{cite news
|url=http://digital.turn-page.com/issue/31146/30
|title=Jal Directed by Girish Malik
|date=20 May 2011
|work=Picklemag
|accessdate=}}  {{cite news
|url=http://dearcinema.com/news/dar-distribute-girish-maliks-jal/2218
|title=DAR to Distribute Jal
|date=8 January 2014
|work=DearCinema
|accessdate=11 January 2014
|location=Calcutta, India}}  {{cite news
|url=http://www.telegraphindia.com/1110323/jsp/entertainment/story_13751510.jsp
|title=On a brand new beat
|date=23 March 2011
|work=The Telegraph (Calcutta)
|accessdate=9 May 2011
|location=Calcutta, India}}  {{cite news
|url=http://www.telegraphindia.com/1110515/jsp/graphiti/story_13982039.jsp
|title=A class act
|date=15 May 2011
|work=Telegraphindia
|accessdate=16 May 2011
|location=Calcutta, India}}  {{cite news
|url=http://www.planetbollywood.com/displayArticle.php?id=n032811125046
|title=Bickram Ghosh-Sonu Nigam Team Up, Combine Marwah-Purva With Armenian Duduk!
|date=
|work=Planet Bollywood
|accessdate=9 May 2011}}  {{cite news
|url=http://www.dnaindia.com/entertainment/report_sonu-nigam-turns-bollywood-composer_1525339
|title=Sonu Nigam turns Bollywood composer
|date=28 March 2011
|work=DNA India
|accessdate=16 May 2011}}  {{cite news
|url=http://www.radioandmusic.com/content/editorial/news/bickram-ghosh-sonu-nigam-collaborates-movie-jal
|title=Bickram Ghosh-Sonu Nigam collaborates for the movie Jal
|date=28 March 2011
|work=Radio And Music
|accessdate=16 May 2011}}  {{cite news
|url=http://www.planetradiocity.com/musicreporter/celebrity-interview-details/Sonu-and-I-have-got-two-more-films-coming-along:-Bickram-Ghosh/614
|title=Sonu and I have got two more films coming along: Bickram Ghosh
|date=23 December 2011
|work=Planet Radio City
|accessdate=25 December 2011}}  {{cite news
|url=http://digital.turn-page.com/i/129783/50
|title=JAL: Water is Thicker than Blood
|date=
|work=Pickle Magazine
|accessdate=5 March 2014}}  {{cite news
|url=http://www.newsnation.in/article/37716-movie-review-jal-%E2%80%93-purab-kohlis-show-water-diviner.html
|title=Jal – Purab Kohlis show as water diviner 
|date=
|work=News Nation
|accessdate=3 April 2014}}  {{cite news
|url=http://timesofindia.indiatimes.com/entertainment/hindi/movie-review/jal/movie-review/33185135.cms
|title=Jal
|date=
|work=Times of India
|accessdate=4 April 2014}} 


 


=== External links ===
*  
*   on YouTube

 
 
 