Terror Toons
{{Infobox film
| name           = Terror Toons
| image          = TerrorToons.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by Brain Damage Films
| director       = Joe Castro
| producer       = Joe Castro
| writer         = 
| screenplay     = Rudy Balli
| story          = Rudy Balli   Joe Castro   Steven J. Escobar
| based on       = 
| narrator       =  Lizzy Borden   Beverly Lynne   Brandon Ellison   Fernando Padilla
| music          = J.M. Logan
| cinematography = Isaac Garza
| editing        = Steven J. Escobar
| studio         = Jesco Film Entertainment
| distributor    = Brain Damage Films
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         = $2,300
| gross          = 
}}

Terror Toons is a 2002 horror comedy film directed by Joe Castro, and written by Rudy Balli. It was followed by two sequels,   in 2007, and Terror Toons 3 in 2015.

== Plot ==

In the "cartoon dimension" mad scientist Doctor Carnage experiments on a man, then disembowels him and rips his skull out through his stomach. On Earth, sisters Cindy and Candy are left alone with Cindys friend Amy when the girls parents go to a wedding. While Cindy and Amy call over Rick and Eddie, Candy watches a Terror Toons DVD she received in the mail up in her room. Created by Satan, the DVD depicts the antics of Doctor Carnage and his accomplice Max Assassin, a stolen lab monkey mutated into a monster by Carnage.
 Strip Ouija, Candy dozes off, being woken up when Carnage and Max appear in her room. The two rip Candys spine out, behead her friend Tommy when he drops by, dismember a pizza delivery man with a giant pizza cutter, and do a hypnotic disco dance number that causes Eddie to vomit up his own innards. When Cindy, Amy and Rick try to escape, they find all the doors have been replaced with vertigo-inducing spirals. Rick is taken and has his brain experimented on by Carnage and Max, and Cindy and Amy are separated when a police officer released from Terror Toons is blown up by a stick of dynamite hidden in a box of donuts.

Cindy is captured by Carnage, and along with the lobotomized Rick, is forced to watch as Carnage and Max saw Amy in half as apart of a magic act. The two villains then take Cindy to a cartoon version of Hell and present her to the Devil, who explains he intends on using Terror Toons to ravage the Earth and corrupt children. Realizing that "anything goes" in cartoons, Cindy becomes a superhero and challenges the Devil, who sends her back to her house. Upon discovering a machine producing Terror Toons DVDs, Cindy is attacked by Max, but she uses her new powers to snap his neck, and stomp his brains out. When Carnage comes at her with a giant axe, Cindy uses it against him, cutting his head in half with it. As tiny monsters pour out of Carnages split skull, Cindy destroys the Terror Toons DVD press with a crowbar.

Sometime later, the parents return, and find Rick banging his head against a wall while Cindy laughs hysterically, surrounded by the remains of her friends and sister. Next door, a boy finds another copy of Terror Toons in his mailbox, and rushes inside with it. The boys front door slams shut, and Carnages giggle is heard.

== Cast ==

* Beverly Lynne as Cindy Lizzy Borden as Candy
* Brandon Ellison as Rick
* Kerry Liu as Amy
* Fernando Padilla as Eddie
* Jack Roberts as The Devil
* Gil Chase as The Father
* Shimmy Maxx as The Mother
* Fernando Gasca as Tommy
* Alexi Bustamante as Pizza Boy
* James Sullivan as Cartoon Cop
* Stephanie Vasquez as Cartoon Girl #1
* Diane Heppner as Cartoon Girl #2
* Scott Barrow as Max Assassin
* Matt Falletta as Doctor Carnage
* Ian Villalobos as The Neighbors Son

== Production ==

Terror Toons was shot in three days on a budget of $2,300.      

== Reception ==

Terror Toons was labelled "an entertaining bad movie" by The Worldwide Celluloid Massacre.  Similarly, Soiled Sinema said, "Terror Toons is awful, but it is the most fun Ive had with a horror film in a while".  The film was called "an awesome horror comedy" by Slasherpool, which awarded it a grade of four out of five. 

Cold Fusion Video said that while the film was flawed and had a weak finale, it was still enjoyable, while All Horror Movies wrote "this is a good movie to watch with beer and popcorn because its cheesy and has bad acting" and gave it a 3½ out of 5.   Three stars were given by Film Threat, which commended the inventiveness and gore, and criticized the poor acting and moronic characters.  The gore and originality of the production were also praised by Monsters at Play. 

Wildside Cinema stated "I adore Terror Toons" and "Sure its bad. The direction is mundane. The effects are weak. The writing is lame. The concept is flaccid. And the acting is a joke. But... its undeniably fun."   Movie Feast gave it a mild recommendation, and concluded that "Terror Toons is a fun, often tasteless mix of comedy and gore that is appropriately lacking in substance". 

Something Awful gave Terror Toons a -48 out of -50, making it one of the lowest scoring films covered by the website.  The film was called crap by Horror Movie a Day, garbage by The Video Graveyard, and cheap and terrible by Doctor Gores Movie Reviews.   

== See also ==

* Evil Toons, a similar film released in 1992.

== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 