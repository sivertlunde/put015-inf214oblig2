Wise Quacks
{{Infobox Hollywood cartoon
|series=Looney Tunes (Porky Pig/Daffy Duck)
|image=
|caption=
|director=Robert Clampett
|story_artist=
|animator=Izzy Ellis
|voice_actor=Mel Blanc
|musician=Carl Stalling
|producer=Leon Schlesinger
|studio=Leon Schlesinger Productions
|distributor=Warner Bros.
|release_date=1939 (USA)
|color_process=Black-and-white
|runtime=7 minutes
|preceded_by=
|followed_by=
|movie_language=English
}}
Wise Quacks is a 1939 Looney Tunes animated short film featuring Porky Pig and Daffy Duck. A different design of Daffy was used in this cartoon.  In this cartoon, Daffy has a light-colored mask around his eyes and a zig-zag ring around his neck. This design would be used one more time in Porkys Last Stand in 1940 and some advertisements during the time.

==Plot==
Mrs. Daffy surprises Daffy with news that she has several eggs waiting to be hatched. Porky reads the announcement of the expecting duck duo in the newspaper. The eggs hatch as Porky comes to congratulate his old friend. Later, an eagle tries to make off with one of the babies. Daffy, still drunk off of corn juice from both worrying about the birth as well as celebrating the hatchlings, pursues the birdnapper. The eagle gathers reinforcements to take on the drunk duck. Porky comes to the rescue to find Daffy and the gang of eagles all getting drunk together, much to Mrs. Daffys dismay.

==External links==
*  

 
 
 
 
 
 
 
 

 