The Conscript
 
{{Infobox film
| name           = The Conscript
| image          = 
| caption        = 
| director       = Roland Verhavert
| producer       = Jan van Raemdonck
| writer         = Nic Bal Roland Verhavert Hendrik Conscience (novel)
| starring       = Jan Decleir
| music          = 
| cinematography = Ralf Boumans
| editing        = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Belgium
| language       = Dutch
| budget         = 
}}
 Best Foreign Language Film at the 47th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Jan Decleir as Jan Braems
* Ansje Beentjes as Katrien
* Gaston Vandermeulen as Grootvader
* Gella Allaert as Katriens moeder
* Bernard Verheyden as Karel
* Idwig Stéphane as Korporaal
* Eddy Asselbergs as Boef (I)
* Leo Madder as Boef (II)
* Denise Zimmerman as Vrouw van kommandant
* Rudi Van Vlaenderen as Dokter
* Johan Vanderbracht as Kommandant
* Gilbert Charles
* Werner Kopers as Maris
* Marieke van Leeuwen as Hoertje

==See also==
* List of submissions to the 47th Academy Awards for Best Foreign Language Film
* List of Belgian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 