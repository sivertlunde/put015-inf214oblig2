The Motorist Bride
{{Infobox film
| name           = The Motorist Bride
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Eichberg 
| producer       = Richard Eichberg James Cox   Helmuth Orthmann
| narrator       = 
| starring       = Hans Mierendorff   Lee Parry   Ernst Hofmann   Angelo Ferrari
| music          = 
| editing        = Heinrich Gärtner
| studio         = Richard Eichberg-Film
| distributor    = Süd-Film
| released       = 22 January 1925
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent romance film directed by Richard Eichberg and starring Hans Mierendorff, Lee Parry and Ernst Hofmann. The film is notable for the use of Lilian Harvey as a stunt double for Parry during the mountaineering scenes shot in Switzerland.  Harvey quickly graduated to become the top star of Richard Eichbergs production company.

It premiered in Berlin on 22 January 1925.  The films art direction is by Jacek Rotmil.

==Cast==
* Hans Mierendorff as Johann Amberg 
* Lee Parry as Eva, seine Tochter 
* Ernst Hofmann as Hans von Corell 
* Angelo Ferrari as Frank Bruhn 
* Sinaida Korolenko as Lili 
* Max Grünberg as Heinz Ellhof 
* Erwin van Roy as Gottlieb Daffke 
* Hans Stürm as Gustav Briese 
* Margarete Kupfer as Marta 
* Gerhard Ritterband as Max 
* Lilian Harvey as Eva (Stunt Double)

==References==
 

==Bibliography==
* Ascheid, Antje. Hitlers Heroines: Stardom and Womanhood in Nazi Cinema. Temple University Press, 2003.
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 
 


 
 