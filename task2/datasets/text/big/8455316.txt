Bratz (2007 film)
 
 
{{Infobox film
| name           = Bratz 
| image          = Bratz The Movie poster.jpg
| alt            = 
| caption        = Theatrical release poster Sean McNamara
| producer       = Isaac Larian Avi Arad Steven Paul
| screenplay     = Susan Estelle Jansen
| story          = Adam de la Peña David Eilenberg
| starring       = Nathalia Ramos Skyler Shaye Logan Browning Janel Parrish Chelsea Kane
| music          = John Coda
| cinematography = Christian Sebaldt
| editing        = Jeff Canavan Arad Productions Lionsgate
| released       =  
| runtime        = 102 minutes  
| country        = United States
| language       = English
| budget         =  
| gross          = $26,013,153   
}}
Bratz, is a 2007 American live-action film based on the Bratz line of cartoon characters and dolls. The screenplay was written by John Doolittle and Susie Singer Carter.  

==Plot== Ian Nelson), lip read. The friends begin to drift apart, as they are compelled to stay within their cliques. Two years later, when an accidental food fight causes them to get detention, they realize that they miss being BFFs and decide to be friends again. They also try to get the other schoolmates to socialize outside their cliques, but their attempts fail when Merediths 2nd Super Sweet 16 party (which ends disastrously) has them seated with their original cliques. The upcoming talent show and its prize of a scholarship gives them the idea to bring all the cliques together again with an act, but the chances are slim with Merediths constant attempts to steal the spotlight. In the end, there is a tie. Meredith gets the trophy, but the girls get the scholarship, which they later give to Cloe.

==Cast== 
* Nathalia Ramos as Yasmin
* Logan Browning as Sasha
* Janel Parrish as Jade
* Skyler Shaye as Cloe
* Chelsea Kane as Meredith Baxter Dimly
* Anneliese van der Pol as Avery
* Malese Jow as Quinn Ian Nelson as Dylan
* Stephen Lunsford as Cameron
* Jon Voight as Principal Dimly
* Lainie Kazan as Bubbie
* Emily Rose Everhard as Cherish Dimly
* Chet Hanks as Dexter
*Carl Hancock Rux as Mr. Whitman

==Production== choreograph the Hey Paula, her reality show on her personal life.
 South Los Angeles, California, while in session. 

==Reception==
Bratz received mostly negative reviews from critics,The  .

===Awards and nominations===
It was nominated for 5 Golden Raspberry Awards in 2007, but won none. Worst Picture (lost to I Know Who Killed Me)  Worst Actress for Logan Browning, Janel Parrish, Nathalia Ramos and Skyler Shaye (lost as a tie to Lindsay Lohan in I Know Who Killed Me (as the characters of Aubrey and Dakota) Worst Supporting Actor for Jon Voight (lost to Eddie Murphy in Norbit (as the character of Mr. Wong)
* Worst Screen Couple for Any combination of two totally airheaded characters (lost to Lindsay Lohan and Lindsay Lohan ("as the yang to her own yin") in I Know Who Killed Me)
* Worst Remake or Rip-off ("a rip-off if there ever was one") (lost to I Know Who Killed Me)

===Box office===
Bratz opened at #10 at the box office. In its entire theatrical run, it earned a worldwide gross of $26,013,153. 

==Home Media== 
The film was released on DVD on November 27, 2007.

==Musical numbers==
 
{| class="wikitable"
|-
! Song || Chiefly Sung By || Other Singers || Scene
|-
| "Fabulous" || Meredith || None || My Super Sweet 16 Party
|-
| "Its All About Me" || Meredith || Meredettes || School Talent Show
|-
| "Bratitude" || Bratz ||  some students of the school|| School Talent Show
|-
| "Open Eyes" || Bratz || None || MTV Video Music Awards Pre-show
|-
| "Express Yourself" || (Background Music) Black Eyed Peas || None || When Jade is showing off the outfit she changed into on the first day
|-
| "Rockstar" || (Background Music)Prima J || None || My Super Sweet 16 Party
|}

==See also==
* Bratz 4 Real
* List of American films of 2007

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 