The Guardsman
 

{{infobox film
| name            = The Guardsman
| image           =
| imagesize       = 
| caption         = Sidney Franklin Harold S. Bucquet (asst director)
| producer        = Albert Lewin (*uncredited) Irving Thalberg (*uncredited)
| writer          = Ferenc Molnar (play) Maxwell Anderson Ernest Vajda (screenplay) Claudine West (continuity)
| starring        = Alfred Lunt Lynn Fontanne
| music           =
| cinematography = Norbert Brodine
| editing         = Conrad Nervig
| distributor     = Metro-Goldwyn-Mayer
| released        = November 7, 1931
| runtime         = 89 minutes (10 reels)
| country         = United States
| language        = English
}} Elizabeth and Earl of Essex, but otherwise has nothing to do with that play.
 Sidney Franklin. 
 Broadway in Best Actor Best Actress in a Leading Role respectively. 

==Plot==
The story revolves around a husband-and-wife acting team. Simply because he is insecure, the husband suspects his wife could be capable of infidelity. The husband disguises himself as a guardsman with a thick accent, woos his wife under his false identity, and ends up seducing her. The couple stays together, and at the end the wife tells the husband that she knew it was him, but played along with the deception.

==Cast==
*Alfred Lunt - The Actor
*Lynn Fontanne - The Actress
*Roland Young - Bernhardt the Critic
*Zasu Pitts - Liesl, the Maid
*Maude Eburne - Mama
*Herman Bing - A Creditor

;Unbilled
*Ann Dvorak - Fan
*Geraldine Dvorak - Operagoer Michael Mark - The Actors Valet
*Eric Mayne - Theatregoer

==Remakes== Oscar Strauss operetta The Chocolate Soldier, starring Risë Stevens and Nelson Eddy. The stage production had used the plot of George Bernard Shaws Arms and the Man, but Shaw had been deeply offended and angered at the result. For the film MGM decided to use the plot of Molnars The Guardsman instead, but they kept the stage score of The Chocolate Soldier. The film was a great success.

*On March 2, 1955, a 60-minute version of the play was aired on the CBS Television series The Best of Broadway.

*In 1984, a new non-musical version, entitled Lily in Love, starring Christopher Plummer and Maggie Smith, was made, but the play was so altered that the names of the characters were changed and Molnar was not even given screen credit. The film was a total flop.

==References==
 

==External links==
* 
* 
*  at Internet Archive

 

 
 
 
 
 
 
 
 
 
 
 