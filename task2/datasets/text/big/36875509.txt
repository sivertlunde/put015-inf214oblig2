List of lesbian, gay, bisexual or transgender-related films of 1995
 
This is a list of lesbian, gay, bisexual or transgender-related films released in 1995. It contains theatrically released cinema films that deal with or feature important gay, lesbian or bisexual or transgender characters or issues and may have same-sex romance or relationships as an important plot device.

==1995==
{| class="wikitable sortable"
! width="16%" | Title
! width="4%" | Year
! width="15%" | Director
! width="15%" | Country
! width="13%" | Genre
! width="47%" | Notes
|- valign="top"
|- Bent Out of Shape|| 1995 ||   ||   || Short, drama||
|-
|Boys on the Side|| 1995 ||   ||    || Comedy, drama||
|- Bugis Street|| 1995 || Yonfan ||    || Drama|| Historically accurate film, focusing on the lives of Singaporean transvestites with realistic-looking locations and studio sets, and camp dialogue.     
|-
|Butterfly Kiss|| 1995 ||   ||   || Drama||
|-
|Carrington (film)|Carrington|| 1995 ||   ||    || Drama||
|- Jeffrey Friedman ||        || Documentary||
|- Costa Brava|| 1995 ||   ||   || Drama||
|-
| || 1995 ||   ||  || Comedy, crime, drama||
|-
|Flirt (1995 film)|Flirt|| 1995 ||   ||       || Drama||
|- French Twist|| 1995 ||   ||  || Comedy||
|-
|Frisk (film)|Frisk|| 1995 ||   ||  || Drama||
|-
|Gonin|| 1995 ||   ||   || Crime, drama||
|-
|Hotel y domicilio|| 1995 ||   ||   || Drama, thriller ||
|-
| || 1995 ||   ||  || Romantic, comedy, drama||
|-
|Jeffrey (film)|Jeffrey|| 1995 ||   ||  || Comedy, drama||
|-
|Kommt Mausi raus?!|| 1995 ||  , Angelina Maccarone ||  || Drama, Comedy, Romance ||
|-
|Lie Down with Dogs|| 1995 ||   ||  || Romantic, comedy||
|-
|Like Grains of Sand|| 1995 ||   ||   || Drama||
|-
|Marciando nel buio|| 1995 ||   ||   || Drama || aka Marching in Darkness
|-
|Menmaniacs - The Legacy of Leather|| 1995 ||   ||    || Documentary ||
|-
|The Midwifes Tale|| 1995 ||   ||   || Romance ||
|- Other Voices, Other Rooms|| 1995 ||   ||     || Drama||
|-
|Parallel Sons|| 1995 ||   ||  || Romantic, drama||
|-
|Sebastian (1995 film)|Sebastian|| 1995 ||   ||     || Drama||
|-
| || 1995 ||   ||   || Drama ||
|-
|Showgirls|| 1995 ||   ||    || Drama||
|-
|Stonewall (1995 film)|Stonewall|| 1995 ||   ||   || Comedy, drama||
|-
|To Wong Foo, Thanks for Everything! Julie Newmar|| 1995 ||   ||  || Comedy||
|- Total Eclipse|| 1995 ||   ||         || Romantic, drama||
|-
|When Night Is Falling|| 1995 ||   ||   || Romantic, drama||
|-
| || 1995 ||   ||    || Documentary||
|- Wild Side|| 1995 ||   ||  || Action, thriller||
|}

==References==
 

 

 
 
 