Syndicate Sadists
{{Infobox film
| name = Syndicate Sadists
| caption =
| image	= Syndicate Sadists FilmPoster.jpeg
| director = Umberto Lenzi
| producer = Luciano Martino
| writer = Vincenzo Mannino
| narrator =
| starring = Tomás Milián Joseph Cotten Maria Fiore Mario Piave Luciano Catenacci
| music = Franco Micalizzi
| cinematography = Federico Zanni
| editing = Daniele Alabiso
| distributor =
| released =  
| runtime = 92 minutes
| country = Italy Italian
| budget =
}} 1975 poliziotteschi film. This film by Umberto Lenzi, one of the directors many work in the crime thriller genre. It stars Joseph Cotten and Tomas Milian.

==Plot==
Milián plays Rambo, an ex-cop who seeks revenge against two powerful crime families who were responsible for the murder of his friend.

==Cast==
*Tomas Milian: Rambo 
*Joseph Cotten: Paternò
*Adolfo Lastretti: Ciccio Paternò
*Mario Piave: Pino Scalia
*Maria Fiore: Maria Scalia
*Duilio Cruciani: Luigino Scalia
*Silvano Tranquilli: Eng. Marco Marsili
*Ida Galli (credited as Evelyne Stewart): wife of Eng. Marsili
*Alessandro Cocco: Giampiero Marsili
*Luciano Catenacci: Conti
*Femi Benussi: Flora
*Guido Alberti: Owner of the pool hall 
*Antonio Casale: Philip Duval

== Production ==
The film predates Ted Kotcheffs First Blood, the film which introduced audiences to the John Rambo of David Morrell by seven years. Tomas Milian happened to read David Morrells novel while flying from the U.S. to Rome. Loving the story he tried to talk some Italian producers into making a film out of it, with him starring as John Rambo. Nothing came of this, but he was allowed to use the Rambo moniker in the next poliziottesco he starred in.   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 