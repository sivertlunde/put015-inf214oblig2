House of the Rising Sun (film)
{{Infobox film
| name           = House of the Rising Sun
| image          = House of the Rising Sun.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Brian A. Miller
| producer       = {{plainlist|
* John G. Carbone
* Mark Sanders
* Kelly Slattery
* Jude S. Walko
}}
| writer         = {{plainlist|
* Chuck Hustmyre
* Brian Miller
}}
| starring       = {{plainlist| Dave Bautista
* Amy Smart
* Dominic Purcell
* Danny Trejo
}}
| music          = Norman Orenstein
| cinematography = William Eubank
| editing        = 
| studio         = Berkshire Axis Media
| distributor    = Grindstone Entertainment
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Dave Bautista. Filming took place in Grand Rapids, Michigan. The screenplay was written by Chuck Hustmyre and Brian A. Miller, based on Chuck Hustmyres novel of the same title. 

==Plot== Dave Bautista) is an ex-vice cop trying to turn his life around after spending 5 years in prison. He works as the head of security for The House of the Rising Sun, a strip club and illegal gambling den. During a night on the job, a masked gang hold him at gunpoint to rob the strip joint of $300,000, ending with the club owners son, Peter (John G. Carbone), getting killed in the shootout. When the police arrive, they suspect Ray led the robbery.

Rays relationship with Jenny Porter (Amy Smart) is rocky following her regretful affair with the right-hand man of his boss, Tony (Dominic Purcell), when he was imprisoned. Ray is enlisted by his boss Vinnie Marcella (Lyle Kanouse) to track down his sons killers. He finds help from his former police colleague Jimmy LaGrange (Brian Vander Ark), who lends him information in his pursuit, but Vinnie and Tony start to believe they wrongly trusted him and that he is behind the robbery.
 inside man who killed the other shooters. The shooter refuses to bear testimony, and Ray kills him when threatened by a gun.

Tony meets with Vinnies older brother, Carlos (Danny Trejo), who Charlie works for. However, Tony is ordered to kill Charlie upon hearing he spoke with Ray, luring Charlie out of his house to kill him while Joey kills his wife, the latter murder which leads back to Ray because of his pocket knife. On the run, with the help of Jenny, Ray discovers Tony was involved in killing the shooters. Confronting Carlos with the development, it becomes known Tonys wife Priscilla is aiding his agenda by sleeping around with Carlos and distracting him so that Tony can take over his brother Vinnies strip club. Offended, Priscilla shoots Carlos dead, but Ray avoids a bullet and shoots her dead.

Ray calls the police. He informs Vinnie at his office they were both set up, with Tony framing Ray for the robbery and turning Vinnies brother Carlos against Vinnie to eventually take over the club. Tony shows up with the money to confirm it as truth, and Jenny is being held hostage by his partner Joey. Tony is bitter that Carlos, who he worked with to build the company, handed it down to Vinnie, who vows revenge for the death of his son and betrayal before Tony kills him. With Tony offering $25,000, Rays former co-vice cop Jimmy shows up to kill him for Tony, instead Jimmy gives Ray a gun causing a shoot-out. Ray takes cover, but Jimmy is hit, and Tony takes off with the money and Jenny down to the floor of the club. There, Jenny escapes with the money and tosses it to the strippers.

In the parking lot, Ray fights Tony until the police arrive, and he tries to convince them Tony did the robbery and set him up. When Tony shoots towards Jenny, the police shoot him down. Jenny pleads to the police that Ray is innocent, but her testimony is insufficient and they still arrest him and take him into custody. In the end, Jenny is able to kiss Ray for a final time before he is driven off back to prison.

==Cast== Dave Bautista as Ray Shane
* Amy Smart as Jenny Porter
* Danny Trejo as Carlos
* Dominic Purcell as Tony
* Craig Fairbrass as Charlie Blackstone
* Brian Vander Ark as Jimmy LaGrange
* Roy Oraschin as Dylan Sylvester
* Lyle Kanouse as Vinnie Marcella
* Jesse Pruett as Detective Slattery

==Awards==
Wins
* Action On Film International Film Festival: Action on Film Award; 2011. Bautista also won the Performer of the Year award for this film. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 