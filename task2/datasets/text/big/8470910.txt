I Drink Your Blood
{{Infobox Film
| name           = I Drink Your Blood
| image          = I Drink Your Blood I Eat Your Skin.jpg
| image_size     = 
| caption        = Poster advertising a double feature of I Drink Your Blood and I Eat Your Skin.
| director       = David E. Durston
| producer       = Jerry Gross
| writer         = David E. Durston
| narrator       = 
| starring       = Bhaskar Roy Chowdhury Lynn Lowry (uncredited) Jack Damon Tyde Kierney
| music          = Clay Pitts
| cinematography = Jacques Demarecaux Joseph Mangine (uncredited)
| editing        = Lyman Hallowell
| distributor    = Grindhouse Releasing / Box Office Spectaculars
| released       =   December 1970   July 1, 1978    DVD released 2000 (Grindhouse Releasing)
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} cult horror film originally released in 1970. The film was written and directed by David E. Durston, produced by Jerry Gross, and starred Bhaskar Roy Chowdhury and Lynn Lowry (who is uncredited in the film). 

Like many B-movies of its time, I Drink Your Blood was a Times Square exploitation film and drive-in theater staple.

== Plot ==

A Manson Family style cult conduct a bizarre Satanic ritual in the woods at night. A member notices a local girl, Sylvia, furtively watching the event. She had befriended a member, Andy, who invited her to watch. The angered group leader Horace Bones leads an assault on the girl.

The next morning, Sylvia emerges from the woods beaten and apparently raped. She is found by Mildred, the woman who runs the local bakery, and Pete, Sylvias younger brother. They return Sylvia home to her grandfather, Doc Banner. Mildred seeks help from her boyfriend, leader of the construction crew working on the nearby dam which has bought up most of the town leaving it deserted.

The cult members van has broken down so they elect to remain in the town. They buy pies from Mildred who explains as that most of the town is deserted and awaiting demolition they can stay in any vacant building they wish.

Learning of the assault on Sylvia, Doc confronts the cult but they assault him and force him to take LSD. Pete intervenes and Doc is released.

Pete takes a shotgun to get revenge but encounters a rabid dog which he shoots. He takes blood from the dog and the next morning injects it into meat pies at the bakery, and sells them to the cult members. All but member Andy eat the pies and become rabid. They start attacking one another. A crazed female cult member absconds but is picked up and seduced by the construction workers sent to investigate the cult. She passes rabies to them and they become homicidal maniacs attacking the cult members and the towns surviving residents. 

==Censorship==

The film was one of the first movies to receive an X-rating from the Motion Picture Association of America based on violence rather than nudity.  Several scenes needed to be altered to qualify the film for an "R" so the producer distributed the original film asking that each projectionist censor the film as seen fit for their market. There were 280 prints made and countless differently censored versions were in circulation. The prints for the Los Angeles and New York City runs were censored by the films director.

The Encyclopedia of Horror said that "as the film now stands what looks like it might have been a raw, ferocious thriller has become a frustrating exercise in splicing, incessantly building up to scenes of bone-crushing horror and violence which never actually happen." 

A 2002 DVD release presented the original uncensored version along with numerous extras.

== Location ==
 Sharon Springs, NY, a small village once famous as a summer spa town. By the time of the production Sharon Springs had largely become a ghost town, and the producers were allowed to use the abandoned hotels as locations. The town has since been revitalized and is a center for tourism in upstate New York. 

== Release == Box Office Spectaculars sought out film director David E. Durston and the two collaborated on the official release of I Drink Your Blood for DVD in North America through Murawskis Box Office Spectaculars distribution company, which continues to hold the worldwide rights to the film.

== Remake ==
On 17 September 2009, it was announced David E. Durston planned a remake of his film that would have starred Sybil Danning.  Durston died in 2010.

==References==
 
*   of Grindhouse Releasing

== See also ==
*Hippie exploitation films

== External links ==
*  
*  

 

 
 
 
 
 
 