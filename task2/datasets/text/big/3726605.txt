Fever (1999 film)
Fever (1999) is a psychological thriller by Alex Winter.

==Plot==

Nick Parker (Henry Thomas) is a struggling young artist suffering a mental and physical breakdown. When a violent murder happens in his apartment building, it pushes him to the edge of sanity. Suspected by his sister (Teri Hatcher) and tracked by a police detective (Bill Duke), Nick begins to think he may have committed the murder himself except for the appearance of a mysterious drifter (David OHara) who has moved in upstairs. Is he a witness or a murderer, and was it all a setup or illusion? The bottom line is: Who can you trust when you can no longer trust yourself?

==Reviews==
* A.O. Scott in The New York Times:  Pure Hitchcockian panic. An arresting example of what a talented filmmaker can do with the sparest of means. 
* Godfrey Cheshire in Variety (magazine)|Variety:  An eerie, insinuating tale of urban dread and mental breakdown,   reps an impressively sophisticated solo directorial debut. 
* Dennis Lim in the Village Voice: With the directors impeccably chic expressionism and Henry Thomass persuasive, dread-soaked performance, Fever sustains a convincingly spooky ambience throughout. Winter achieves a degree of technical polish rare among American independents.
* Phil Hall, Film Threat Mediocre thriller about a starving artist suspected of murder.


Official Selection, Cannes Film Festival, 1999.

==External links==
*  

 
 
 
 
 

 