Thieves in Thailand
 
 
Thieves in Thailand (  "Haramiyyah fi Tayland") is a 2003 Egyptian film directed by Sandra Nashaat.  It is a 35 mm film and lasts for 105 minutes.  Lisa Anderson of the Chicago Tribune uses the film as an example of increasing conservatism in Egypt. 

==Plot==
It is set in   who travels to Thailand. Nakla, Sherif Iskandar. " ." ( ) Al Ahram Weekly. 27 March - 2 April 2003. Issue No. 631. Retrieved on February 21, 2013.  No intimate contact, including a kiss, is seen. 

==Production==
The naming was meant to coincide with Ismail Yassin films. The films, which used the same cast members and the same style but had differing themes and stories, started with Ismail Yassin fi ("Ismail Yassin in"). Likewise this film was produced after Nashaats Harameya fi KG2 (Thieves in KG2). 

==Cast==
 
* Maged El-Kidwani - Fatin 
* Karim Abdel-Aziz - Ibrahim, Fatins brother 
* Hanan Tork - Hanan - Hanan is Ibrahims wife and is forced to be his accomplice 

==Reception==
The film was very popular in Egypt.  Sandra Nashaat said "The film was as well received as I expected, although I feel it could have been seen by a greater number of people. But of course due to the war fewer people are going to the movies. One of my concerns was that Harameya fi Thailand  would be compared and contrasted with Harameya fi KG2, because they are two entirely different films. In the end the inevitable happened, but at least it was positive: many liked Harameya fi Thailand more." 

Sherif Iskander Nakhla of Al-Ahram Weekly said "As a whole the film has memorable moments, yet its narrative structure falls short of the highest standards."  Lisa Anderson of the Chicago Tribune described the film as "A mindless romp spiced with lush Thai landscapes". 

==See also==
 
* Cinema of Egypt
* Thieves in KG2

==References==
* Armes, Roy. Dictionary of African Filmmakers. Indiana University Press, July 11, 2008. ISBN 0253000424, 9780253000422.
* Hillauer, Rebecca. Encyclopedia of Arab Women Filmmakers. American University in Cairo Press, 2005. ISBN 9774249437, 9789774249433.

==Notes==
 

==External links==
*  

 
 
 
 