The Camels are Coming (film)
 
 
{{Infobox film
| name           = The Camels are Coming
| image          = 
| image_size     = 
| caption        = 
| director       = Tim Whelan
| producer       = Michael Balcon
| writer         = Guy Bolton   Jack Hulbert   W.P. Lipscomb
| music          = Jack Beaver   Louis Levy
| cinematography = 
| editing        = 
| narrator       = 
| starring       =  Jack Hulbert  Anna Lee  Hartley Power   Harold Huth
| distributor    = Gaumont British Distributors
| released       = October 1934
| runtime        = 79 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British comedy comedy adventure film directed by Tim Whelan and starring Jack Hulbert, Anna Lee, Hartley Power and Harold Huth. A British officer in the Royal Egyptian Air Force combats drug smugglers. 

==Production==
Based on a story by Tim Whelan and Russell Medcraft, the film was directed by Tim Whelan, an American who made a number of British films. It was filmed at Islington Studios and on location in Egypt around Cairo and Gizeh including at the famous Shepheards Hotel.

Many of the scenes later had to be reshot in London as sand had got into the cameras, and high winds prevented the recording of dialogue. 

It was a major success at the British box office, but was not released in the United States. 
 Robert Stevenson, whom she subsequently married.

==Cast==
* Jack Hulbert – Jack Campbell
* Anna Lee – Anita Rodgers
* Hartley Power – Nicholas
* Harold Huth – Doctor Zhiga
* Allan Jeayes- Sheikh
* Peter Gawthorne – Colonel Fairley
* Norma Whalley – Tourist
* Peggy Simpson – Tourist
* Tony De Lungo – Smugglers servant
* Percy Parsons – Arab

==References==
 

===Bibliography===
* Reid, John Howard. Hollywoods Classic Comedies Featuring Slapstick, Romance, Music, Glamour Or Screwball Fun!. 2007.
* Richards, Jeffrey. The Unknown 1930s: An Alternative History of the British Cinema, 1929– 1939. I.B. Tauris & Co, 1998.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 