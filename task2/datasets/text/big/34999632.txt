The Christmas Choir
{{Infobox film name = The Christmas Choir caption = Official Film Poster image = The Christmas Choir poster.jpg director = Peter Svatek producer = Michael Prupas writer = Donald Martin starring = Jason Gedrick Rhea Perlman Tyrone Benskin Michael Sarrazin Marianne Farley Cindy Sampson music = James Gelfand cinematography = Eric Cayla editing = Stephanie Gregoire distributor = Hallmark Channel country = Canada United States released =   runtime = 90 minutes language = English budget =
}} holiday film Donald Martin and directed by Peter Svatek, and based upon a true story of a man who volunteered to work at a homeless shelter and started a choir with its residents.  The film first aired in December 2008 on the Hallmark Channel.    The film was Michael Sarrazins final screen role. The film was inspired from Montreals Acceuil Bonneau Choir.   

==Plot== shelter for other homeless people. He eventually meets the caring yet shrewish nun Sister Agatha (Rhea Perlman), who re-appraises his troubles and changing struggles in life. He decides the only to way to help build money and better opportunities to the shelter is to organize them into a choir.

Although, the choirs musical abilities become more proficient each day, Peter and the other singers go through their own personal trials and the many obstacle problems they bring to the group. They, finally, begin to trust in each other and learn how the mystery of music can hold for a community.

==Selected cast==
* Jason Gedrick as Peter Brockman
* Rhea Perlman as Sister Agatha
* Tyrone Benskin as Bob
* Michael Sarrazin as Henry Brockman
* Marianne Farley as Marilyn
* Roc LaFortune as Hector
* John Dunn-Hill as Fred
* Luis Oliva as Juan
* Claudia Ferri as Rita
* Cindy Sampson as Jill Crosby

==Reception==
Of its 2009 DVD release, Movie2Movie wrote that the film dealt about real people with real problems, and that it was one of more than 20 films by director Peter Svatek. They made note that the first impression is that while it is a sweet film, it feels of the low-budget Christmas films which are often seen as commonplace. But they caution that the viewer should not judge the film too quickly, in that its subdued nature ensures the films charm and the film has its strength through being based upon real events.   

==Release==
The film had its release on DVD in December 2009 in Netherlands.  It was released in 2011 on DVD in Sweden as Julkören.

==References==
 
*   from Buffalo News.
*  

==External links==
*  
*  
*   from Moviefone.
*   at Hallmark Channel.

 
 
 
 
 
 
 
 