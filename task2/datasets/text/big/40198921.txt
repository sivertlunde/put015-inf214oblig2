Richard the Lionheart (2013 film)
{{Infobox film
| name = Richard The Lionheart
| image =
| caption = Theatrical poster
| director = Stefano Milla
| producer = Phil Gorn Stefano Milla Blinov Oleg Adrianovich
| writer = Gero Giglio Stefano Milla
| screenplay = Gero Giglio
| starring = Malcolm McDowell Stewart Arnold Veronica Calilli Chandler Maness Maurizio Corigliano Davide Ferricchio Sharon Fryer Christopher Jones Alice Lussiana Parente Burton Perez Umberto Procopio Thomas Tinker Carrion Yudith Andrea Zirio
| music = Giovanni Lodigiani
| cinematography = Luca Grivet Brancot Gary Rohan
| editing = Stefano Milla
| studio = Claang Entertainment DOMA Entertainment WonderPhil Productions
| distributor =Grindstone Entertainment Group Lionsgate Home Entertainment New Select WonderPhil Productions thefyzz
| released = February 9, 2013 (Germany) January 8, 2014 (Japan) January 21, 2014 (United States)
| runtime = 104 minutes 
| country = United States Italy English
| budget = 
| gross = 
}}
 Richard The King Henry Henry the Young.

==Synopsis==
The aging King Henry II has chosen his son Richard to lead England in the coming war against France. To test Richards loyalty, honor and skill, Henry sends him to a hellish prison in which the captives must fight a never-ending stream of enemies in order to survive. As Richard overcomes each new challenger, his strength, ingenuity and character are proven, and the legend of Richard the Lionheart is born.

==Cast== King Henry II
*Stewart Arnold as Selector
*Veronica Calilli as Celtic Goddess Richard The Lionheart
*Maurizio Corigliano as Barbarian
*Davide Ferricchio as Forgotten
*Sharon Fryer as Celtic Goddess (voice)
*Christopher Jones as One Eye
*Alice Lussiana Parente as Girl
*Burton Perez as Basileus Caesar
*Thomas Tinker as Philippe
*Carrion Yudith as Ghaliya Henry the Young

==Production==
The film was directed by Stefano Milla and stars Chandler Maness, Burton Anthony Perez, and Malcolm McDowell.   

==See also==
* Cultural depictions of Henry II of England
* Cultural depictions of Henry the Young King
* Cultural depictions of Richard I of England

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 