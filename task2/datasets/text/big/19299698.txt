Jubilo, Jr.
{{Infobox film
| name           = Jubilo, Jr.
| image          =
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         = Frank Capra Hal Roach H. M. Walker
| narrator       =
| starring       = Will Rogers
| music          =
| cinematography = Robert Doran Art Lloyd
| editing        =
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 short silent silent comedy film directed by Robert F. McGowan.       It was the 27th Our Gang short subject released.

==Synopsis==
A hobo, played by Will Rogers tells of his adventure as a child when he tried to raise the money to buy his mother a hat for her birthday.

==Notes==
* Jubilo, Jr. would be remade in 1932 as Birthday Blues.
*Though never included in the original television package of Our Gang silents, this film did appear in the TV series Silents Please.
*The grocer is played by Mickey Daniels’ father Richard Daniels.
*About thirty seconds of unused footage was used in the later Our Gang film Boys Will Be Joys.

==Cast==

===The Gang===
* Mickey Daniels as Young Jubilo
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Allen Hoskins as Farina
* Mary Kornman as Mary
* Andy Samuel - Andy / Charlie Chaplin
* Dick Henchen as Dick
* Pal the Dog as Pal

===Additional cast===
* Will Rogers as Jubilo / Himself
* Lassie Lou Ahern as Circus Performer
* Allan Cavan as Hat vendor
* Charley Chase as Director
* Richard Daniels as Grocer
* Otto Himm as Photographer
* Lyle Tayo as Mother
* Leo Willis as Tramp pal of Jubilo
* Noah Young as Emil, Jubilos father
* Joy Winthrop as Extra outside church
* Jerry McGowan
* Roberta McGowan

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 