Stratosphere Girl
{{Infobox film
 | name = Stratosphere Girl
 | image = Stratosphere Girl movie.jpg
 | caption = Stratosphere Girl film poster
 | director = Matthias X. Oberg
 | writer = Matthias X. Oberg
 | starring = Chloé Winkel, Jon Yang
 | producer = Karl Baumgartner
 | distributor = TLA Releasing
 | budget =
 | released = 9 September 2004 ( Germany ) min
 | language = English, Dutch, Japanese
  | }}
 2004 film from Germany written and directed by Matthias X. Oberg.
 German teenager who travels to Japan to work at an exclusive club for rich businessmen.

==Plot==
  Japanese Disc jockey|DJ.  Encouraged by him, she goes to Japan to work at an exclusive club for rich businessmen, who like to meet with young blonde women.  From the start, the film is surreal with unique characters, clear and sharp cinematography, and slow panning camera work. Manga drawings are also used to enhance the plot and ambiance.

Angela seeks work at the aforementioned club and, after having been begrudgingly let in, she is met with derision by the other girls working there. However, despite having spurned some of the other girls, she soon proves to be a favourite among the patrons by pretending to be a Lolita-style 15-year-old to please the businessmen.

The plot has a sinister undertone of the possibility of murder of a girl, Larissa, whom Angela has replaced.  As the film goes on, we learn Larissa was possibly murdered, not by Japanese men in search of sick sexual fantasy fulfillment, but at the envious and jealous hands of her workmates.  In the last scenes we learn Larissa lives and, furthermore, this is when Angela is heralded with the contract to be a Manga artist.

==External links==
* 
* 

 
 
 
 
 
 
 


 