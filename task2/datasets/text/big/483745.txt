Real Genius
{{Infobox film
| name           = Real Genius
| image          = real genius.jpg
| alt            = 
| caption        = Film poster
| director       = Martha Coolidge
| producer       = Brian Grazer
| screenplay     = Neal Israel Pat Proft Peter Torokvei
| story          = Neal Israel Pat Proft
| starring = {{Plainlist|
* Val Kilmer Gabe Jarret
* Michelle Meyrink
* William Atherton
}}
| music          = Thomas Newman, The Textones
| cinematography = Vilmos Zsigmond
| editing        = Richard Chew
| studio         = Delphi III Productions
| distributor    = TriStar Pictures
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $12,952,019 (North America)
}}
Real Genius is a 1985 satirical comedy film directed by Martha Coolidge. The films screenplay was written by Neal Israel, Pat Proft, and Peter Torokvei. It stars Val Kilmer and Gabriel Jarret.

The film is set on the campus of Pacific Tech, a technical university similar to Caltech. Chris Knight (Kilmer) is a genius in his senior year working on a chemical laser. Mitch Taylor (Jarret) is a new student on campus who is paired up with Knight to work on the laser.

The film received positive reviews from critics. It grossed $12,952,019 at the United States and Canadian box office.

==Plot== CIA officers watch a video presentation of a top-secret project called "Crossbow": a space shuttle mounted with a computer-guided laser weapon incinerates a man on the ground with pinpoint accuracy. Researchers on the project have yet to devise a system to generate enough power to operate it. When it becomes clear that this weapon has no wartime applications and is intended solely for illegal assassinations, one agent decries the project as immoral. He is assassinated.

Professor Jerry Hathaway (Atherton) meets high school student Mitch Taylor (Jarret). He informs Mitch that he has been admitted to Pacific Technical University, where he will room with physics "legend" Chris Knight (Kilmer). Hathaway is secretly developing the laser for the CIA; instead of doing the work himself he has his unpaid students do it, and he misappropriates the project funding to remodel his house. Arriving on campus, Mitch meets Chris and is disappointed to learn that he is an irreverent slacker who spends his time pulling elaborate high tech pranks (such as covering the dorm floor in ice for skating). Mitch also meets Jordan (Meyrink), a hyperkinetic female student, and the mysterious Lazlo Hollyfeld (Gries), who seems to be living in Mitchs closet. Hathaways sycophantic graduate assistant Kent (Prescott) becomes hostile when Hathaway puts Mitch in charge of the laser project.
 breakdown when he learned that his theories were being used to build weapons. Chris tells Mitch that if he does not want to "crack" like Hollyfeld, he must learn to have fun, and the first order of business is to get even with Kent, calling it a "moral imperitave" to do so. They accomplish this by disassembling Kents car and rebuilding it inside his dorm room.

Under increasing pressure from the CIA, Hathaway berates Chris for failing to solve the laser power problem and threatens to fail him and prevent him from graduating. Chris devotes himself to solving the power problem and achieving a perfect score on Hathaways final exam. His efforts appear to be ruined when Kent sabotages the laser. In a fit of anger at the lasers destruction, he has an epiphany that solves the power problem. The beam of the redesigned laser has unlimited range and produces an estimated six megawatts of power, exceeding the original requirement.

While the team celebrates its success, Lazlo insists that the high-energy laser can only be used as a weapon, and in fact that it must have been conceived for this purpose. Chris is devastated. Hathaway has removed the laser from the lab. Chris, Mitch, and Jordan trick Kent into revealing the date when the laser is going to be tested. The group tails Hathaway to a nearby Air Force base. While Chris and Mitch talk their way onto the base, Lazlo remotely cracks the lasers computer and changes its target coordinates to Hathaways house, where the team has placed a huge tin of popcorn. Meanwhile, Chris and Mitch remove some vital circuits that prevent the laser from overheating. When the laser beam hits the house, it is diffused by a prism placed by Chris and the popcorn heats and expands; the house bursts at the seams as popcorn pours out onto the lawn. The laser overheats and destroys itself. The group (joined by the Dean of the college and a local Congressman to witness the unethical weapon test) watch as kids play in the popcorn. Hathaway returns home and is mortified by the damage.

==Cast==
* Val Kilmer as Chris Knight
* Gabriel Jarret as Mitch Taylor
* Michelle Meyrink as Jordan Cochran
* Mark Kamiyama as "Ick" Ikagami
* William Atherton as Prof. Jerry Hathaway
* Jon Gries as Lazlo Hollyfeld Robert Prescott as Kent
* Ed Lauter as David Decker
* Louis Giambalvo as Maj. Carnagle 
* Patti DArbanville as Sherry Nugill
* Severn Darden as Dr. Meredith 
* Stacy Peralta as shuttle pilot
* Beau Billingslea as George
* Joanne Baron as Mrs. Taylor
* Sandy Martin as Mrs. Meredith
* Dean Devlin as Milton
* Yuji Okumoto as Fenton
* Deborah Foreman as Susan Decker

== Production ==
To prepare for Real Genius, Martha Coolidge spent months researching laser technology and the policies of the CIA, and interviewed dozens of students at California Institute of Technology|Caltech.  The screenplay was extensively rewritten, first by Lowell Ganz and Babaloo Mandel, later by Coolidge and Peter Torokvei. 

Producer Brian Grazer remembers that when Val Kilmer came in to audition for the role of Chris Knight, he brought candy bars and performed tricks. Kilmer remembered it differently. "The character wasnt polite, so when I shook Grazers hand and he said, Hi, Im the producer, I said, Im sorry. You look like youre 12 years old. I like to work with men." 
 Canyon Country, northwest of Los Angeles, and placed in the house. 

To promote the film, the studio held what it billed as "the worlds first computer press conference" with Coolidge and Grazer answering journalists questions via computer terminals and relayed over the CompuServe computer network. 

The dorm in the film is based on Dabney House at Caltech, and Caltech students played extras in the film. 

==Soundtrack==
# "You Took Advantage of Me" performed by Carmen McRae
# "The Tuff Do What?" performed by Tonio K
# "Summertime Girls" performed by Y&T The System The Call
# "Im Falling" performed by The Comsat Angels
# "One Night Love Affair" performed by Bryan Adams
# "All She Wants to Do Is Dance", performed by Don Henley
# "Number One" performed by Chaz Jankel
# "Youre the Only Love" performed by Paul Hyde and the Payolas
# "Standing In The Line" performed by The Textones, written by Carla Olson
# "Everybody Wants to Rule the World" performed by Tears for Fears

==Reception==

===Box office===
Real Genius was released on August 9, 1985 in 990&nbsp;theaters grossing $2.5&nbsp;million in its first weekend. It went on to make $12,952,019 in North America. {{cite news
 | title = Real Genius
 | work = Box Office Mojo
 | publisher = Internet Movie Database
 | url = http://www.boxofficemojo.com/movies/?id=realgenius.htm
 | accessdate = March 30, 2009 }} 

===Critical response===
Real Genius received mixed to positive reviews and has a 74% rating on Rotten Tomatoes, based on 23 reviews.  In her review for The New York Times, Janet Maslin wrote, "the film is best when it takes   seriously, though it does so only intermittently". {{cite news
 | last = Maslin
 | first = Janet
 | title = Real Genius
 | work = The New York Times
 | pages =
 | publisher =
 | date = August 7, 1985
 | url = http://movies.nytimes.com/movie/review?_r=1&res=9A05E3DD1138F934A3575BC0A963948260&partner=Rotten%20Tomatoes
 | accessdate = March 30, 2009 }}  David Ansen wrote in his review for Newsweek magazine, "When its good, the dormitory high jinks feel like the genuine release of teen-age tensions and cruelty. Too bad the story isnt as smart as the kids in it". {{cite news
 | last = Ansen
 | first = David
 | title = Hollywoods Silly Season
 | work = Newsweek
 | pages =
 | publisher =
 | date = August 26, 1985
 | url =
 | accessdate = }}  In her review for the Washington Post, Rita Kempley wrote, "Many of the scenes, already badly written, fail to fulfill their screwball potential... But despite its enthusiastic young cast and its many good intentions, it doesnt quite succeed. I guess theres a leak in the think tank". {{cite news
 | last = Kempley
 | first = Rita
 | title = Real Genius Reels, Falls
 | work = Washington Post
 | pages =
 | publisher =
 | date = August 9, 1985
 | url =
 | accessdate = }} 
Chicago Sun Times film critic Roger Ebert awarded the film three and a half stars out of four, saying that it "contains many pleasures, but one of the best is its conviction that the American campus contains life as we know it". {{cite news
 | last = Ebert
 | first = Roger
 | title = Real Genius
 | work = Chicago Sun-Times
 | pages =
 | publisher =
 | date = August 7, 1985
 | url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19850807/REVIEWS/508070301/1023
 | accessdate = March 30, 2009 }}  In his review for the Globe and Mail, Salem Alaton wrote, "Producer Brian Grazer craved a feel-good picture, and she   turned in the summers best, and she didnt cheat to do it. Theres heart in the kookiness. Real Genius has real people, real comedy and real fun". {{cite news
 | last = Alaton
 | first = Salem
 | title = This time the teen antics are funny Real Genius is a real gem
 | work = Globe and Mail
 | pages =
 | publisher =
 | date = August 12, 1985
 | url =
 | accessdate = }}  Time (magazine)|Time magazines Richard Schickel praised the film for being "a smart, no-nonsense movie that may actually teach its prime audience a valuable lesson: the best retort to an intolerable situation is not necessarily a food fight. Better results, and more fun, come from rubbing a few brains briskly together". {{cite news
 | last = Schickel
 | first = Richard
 | title = Guess Who Flunked the IQ Test? Time
 | pages =
 | publisher =
 | date = August 12, 1985
 | url = http://www.time.com/time/magazine/article/0,9171,1050506,00.html
 | accessdate = April 23, 2009 }} 

==Scientific accuracy==
In the MythBusters episode, "Car vs. Rain", first broadcast on June 17, 2009, the MythBusters team tried to determine whether the final scene in the film, the destruction of Dr. Hathaways house with laser-popped popcorn, is actually possible. First they used a ten-watt laser to pop a single kernel wrapped in aluminum foil, showing that popping corn is possible with a laser, then they tested a scaled-down model of a house.  The popcorn was popped through induction heating because a sufficiently large laser was not available. The result was that the popcorn was unable to expand sufficiently to break glass, much less break open a door or move the house off its foundation.  Instead, it ceased to expand and then simply charred.   

It was also specifically stated in the program that a five-megawatt laser still did not exist, even in military applications, and that the largest military laser they knew of was 100 kilowatts. 

In January, 2011, it was further demonstrated on video  in a home setting that a kernel of corn directly exposed to laser light from accessible consumer level lasers could be popped as reported by TechCrunch. 

The solid xenon-halogen laser proposed and built by Chris in the latter half of the film, though in the realm of science fiction, was based on theory of the time. Real Genius through consultant Martin A. Gundersen (who played the Math Professor) was later given a citation in an academic publication which detailed the scientific basis behind the laser. 

==Influences==
The character of "Jordan" was the basis for the animated character Gadget Hackwrench in Disneys Chip and Dales Rescue Rangers. 

==TV Series==
A potential TV series is in the works. 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 