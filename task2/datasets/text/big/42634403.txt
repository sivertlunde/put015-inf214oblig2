Something in the Air (2002 film)
{{Infobox film
| name           = Something in the Air
| image          = Uma Onda no Ar.jpg
| caption        = Theatrical release poster
| director       = Helvécio Ratton
| producer       = Simone Magalhães Matos
| writer         = Helvécio Ratton Jorge Durán Alexandre Moreno Adolfo Moura Babu Santana Benjamin Abras
| music          = Gil Amancio
| cinematography = José Tadeu Ribeiro
| editing        = Mair Tavares
| studio         = Quimera Filmes
| distributor    = Mais Filmes
| released       =  
| runtime        = 92 minutes
| country        = Brazil
| language       = Portuguese
| budget         = R$1.75–1.8 million    
| gross          = R$153,644 
}}
 broadcaster established Alexandre Moreno, Adolfo Moura, Babu Santana and Benjamim Abras were chosen to star the film after 3,000 people tried for their roles.    It was shot in Aglomerado da Serra and used about 300 of the local people as extras. 

==Cast== Alexandre Moreno as Jorge
*Adolfo Moura as Zequiel
*Babu Santana as Roque
*Benjamim Abras as Brau
*Priscila Dias as Fátima
*Edyr de Castro as Neusa 
*Tião DÁvilla as police officer
*Hamilton Borges Walê as Baiano
*Renata Otto as Lídia

==Reception== City of God as it focuses on the good things of a favela and as it deals with the violence.     Morisawa praised the actors but labeled its screenplay and editing "somewhat loose" which makes it "just nice" and "dilutes the strength of its message."  Although he praised its music, Couto stated that it fails with its dialogues and supporting cast. He pointed that some may say that he technical triumphs of Meirelles film may hide its "moral emptiness". However, Couto would like to see a film that is great in both technical aspects, as Meirelles one, and that has an "ethical-political commitment" as Rattons film.  
 
==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 