The Immigrant (2013 film)
 
{{Infobox film
| name           = The Immigrant
| image          = The Immigrant 2013 poster.jpg
| caption        = Film poster James Gray
| producer       = James Gray Anthony Katagas Greg Shapiro Christopher Woodrow
| writer         = Ric Menello James Gray
| starring       = Marion Cotillard Joaquin Phoenix Jeremy Renner
| music          = Chris Spelman
| cinematography = Darius Khondji
| editing        = John Axelrad Kayla Emter
| studio         = Worldview Entertainment Keep Your Head Kingsgate Films
| distributor    = The Weinstein Company
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English Polish
| budget         = $16 million 
| gross          = $5.9 million 
}}
 James Gray, starring Marion Cotillard, Joaquin Phoenix, and Jeremy Renner. It was nominated for the Palme dOr at the 2013 Cannes Film Festival.    The working titles of the film were Low Life and The Nightingale. 

==Plot== ravaged home Great War Second Polish Republic|Poland. Magda is quarantined because of her lung disease. Ewa is almost deported, but Bruno (Joaquin Phoenix) notices her and her fluency in English, bribes an officer to let her go, and takes her to his house. Knowing Ewa has to make money to get Magda released, Bruno lets her dance at the Bandits Roost theater and prostitutes her. Bruno also becomes interested in her romantically.

Ewa looks for her expatriate relatives living in New York, but her uncle by marriage turns her in to the authorities; he says he had heard she had gotten in trouble for engaging in illicit behavior on the ship from Europe, and he wishes to distance himself from sheltering a prostitute. Policemen take her back to Ellis Island, and once again she is slated for deportation. While at Ellis Island Ewa watches a performance by Emil (Jeremy Renner) (Brunos cousin, making a living as a performing illusionist called Orlando); after his performance he hands her a white rose. The next morning Bruno manages to get her released. 

Ewa meets Emil again at the Bandits Roost. Emil asks Ewa to come onstage to aid him in his mind reading trick, but the men in the audience start yelling slanders at Ewa. The scene ends in brawl between Bruno and Emil and with Bruno and the girls being fired from the theatre. Soon after Bruno has his "doves" parade around Central Park to attract men to sleep with them. Another encounter between Emil and Ewa proves Emils feelings for her. Emil falls for Ewa, much to Brunos discontent, which causes continued and intense conflicts between the two men. One violent conflict concludes with Brunos being jailed overnight.

One day, Emil sneaks into Brunos home to see Ewa. While there, he promises to get her the money to save her sister so they can all leave New York together. Coincidentally, shortly after making such promise, Emil hides as Bruno returns. Bruno also makes a promise to Ewa: he is to arrange for her a meeting with her sister. But Emil interrupts Bruno, as he pulls out an unloaded gun and points it at Bruno. Emil pulls the trigger just to frighten him, but his attempt at intimidating Bruno backfires, as Bruno stabs him to death in apparent self-defense. 

Overcoming the shock and distress of the death, Bruno and Ewa dump Emils body in the street at night to get rid of unwanted police investigations, but the police are told by another prostitute whom Ewa had had conflicts with that Ewa killed Emil. Bruno hides Ewa from the police, who then give him a severe beating and steal a large bundle of money he had been carrying. Ewa learns Bruno had had enough money to pay for her sisters release all along but was hiding it from her as he did not want her to leave him. Bruno claims he has now had a change of heart and would help Ewa and her sister if he had any money. Ewa makes another contact with her aunt and successfully pleads for her aunt to give her the money for Magda. With it, Bruno pays his contact on Ellis Island to release Ewas sister and gives them both tickets to New Jersey. Ewa and Magda leave, while a repentant Bruno stays in New York, intending to confess to the police about Emils killing.

==Cast==
* Marion Cotillard as Ewa Cybulska
* Joaquin Phoenix as Bruno Weiss
* Jeremy Renner as Orlando the Magician / Emil
* Yelena Solovey as Rosie Hertz
* Dagmara Dominczyk as Belva
* Maja Wampuszyc as Aunt Edyta
* Angela Sarafyan as Magda Cybulska
* Ilia Volok as Wojtek
* Antoni Corone as Thomas MacNally
* Dylan Hartigan as Roger
* DeeDee Luxe as Bandits Roost Tart
* Gabriel Rush as Delivery Boy
* Kevin Cannon as Missionary

== Production ==

Director James Gray said The Immigrant is "80% based on the recollections from my grandparents, who came to the United States in 1923", and he described it as "my most personal and autobiographical film to date".  He was also inspired by Giacomo Puccinis opera Il Trittico. 

Because Gray wrote about 20 pages of dialogue in Polish, Cotillard "had to learn Polish to take on the role and speak English with a credible Polish accent."   

==Release==
 .]] 2013 Rio 2013 New 2013 Chicago 2014 Miami 2014 Newport 2014 Sedona Film Festival. 

The film was released in the United States on May 16, 2014. 

==Reception==
The Immigrant received generally positive reviews from critics.   gave the film a rating of 76/100, based on 34 reviews. 

Michael Phillips of Chicago Tribune described the film as "Grays most satisfying to date, an ode to melodrama of another day, done with style and surprising restraint."  Ed Gonzalez of Slant Magazine gave the film 3 and a half out of 4 stars, saying that "The Immigrant feels closer in spirit to Roberto Rossellinis collaborations with Ingrid Bergman" and calling it "Grays Voyage to Italy".  Brian Clark of Twitch Film gave the film a mixed review, commenting that "while the film boasts great performances, the narrative and overall drama lacks the ferocity, momentum and intensity of Greys other work".  Lee Marshall of Screen International in his unfavorable review wrote that "though Gray offers a well-crafted package, especially on the visual front, theres surprisingly little contemporary resonance in this immigration melodrama". 
 Time ranked Marion Cotillards performance in the film as the fourth best performance of 2014, shared with her performance in Two Days, One Night. 

==Accolades==
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Category
! Recipient(s)
! Result
|- Cannes Film Festival
| Palme dOr
| James Gray
|  
|- International Cinephile Society Awards
| Prix du Jury
| James Gray
|  
|- 
| Best Actress
| Marion Cotillard
|  
|-
| Best Picture Not Released in 2013
|
|  
|-
| Munich Film Festival
| ARRI/OSRAM Award - Best International Film
| James Gray
|  
|-
| Ghent International Film Festival
| Grand Prix
| James Gray
|  
|-
| Chicago International Film Festival
| Audience Choice Award
| James Gray
|  
|- Newport Beach Film Festival
| Outstanding Achievement in Directing
| James Gray
|  
|-
| Outstanding Achievement in Filmmaking - Ensemble Cast
| Marion Cotillard, Joaquin Phoenix and Jeremy Renner
|   
|-  2014 New York Film Critics Circle Awards
| Best Actress
| Marion Cotillard  (shared with Two Days, One Night)  
|  
|-
| Best Cinematographer 
| Darius Khondji 
|  
|-
| Boston Society of Film Critics Award
| Best Actress
| Marion Cotillard  (shared with Two Days, One Night)  
|  
|-
| Toronto Film Critics Association
| Best Actress
| Marion Cotillard
|  
|- 30th Independent Independent Spirit Awards  Independent Spirit Best Female Lead 
| Marion Cotillard 
|  
|-  Independent Spirit Best Cinematography  Darius Khondji 
|  
|}

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 