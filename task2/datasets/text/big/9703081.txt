The Brain Eaters
{{Infobox film
| name           = The Brain Eaters
| image          = Braineatersposter.jpg
| image_size     = 
| caption        = Film poster by Albet Kallis
| director       = Bruno VeSota
| producer       = Ed Nelson Roger Corman (exec) (uncredited)
| writer         = Gordon Urquhart
| based on       = novel The Puppet Masters by Robert A. Heinlein (uncredited)
| narrator       =  Joanna Lee Jody Fair David Hughes Leonard Nimoy
| music          = Tom Jonson
| cinematography = Lawrence Raimond	
| editing        = Carlo Lodato
| studio         = Corinthian Productions
| distributor    = American International Pictures
| released       = 1958
| runtime        = 60 min
| country        = United States
| language       = English
| budget         = $26,000 
| preceded_by    = 
| followed_by    = 
}}
 Riverdale and Joanna Lee, with a brief appearance by Leonard Nimoy (name misspelled in 10th place in the credits as "Leonard Nemoy").  The film was released by American International Pictures as a double feature with Earth vs. the Spider

==Plot==
A team of local scientists discover alien parasites when they investigate a mysterious, three-story-tall, cone-like object that has appeared outside of town. It becomes obvious that the parasites first victims, whose minds have been taken over, are the towns leading citizens.

==Cast==
* Ed Nelson as Dr. Paul Kettering (billed as Edwin Nelson)
* Alan Jay Factor as Glenn Cameron (billed as Alan Frost)
* Cornelius Keefe as Senator Walter K. Powers (billed as Jack Hill) Joanna Lee as Alice Summers
* Jody Fair as Elaine Cameron
* David Hughes as Dr. Wyler
* Robert Ball as Dan Walker
* Greigh Phillips as the Sheriff
* Orville Sherman as Mayor Cameron
* Leonard Nimoy as Professor Cole (billed as Leonard Nemoy)

==Production==
Actor Bruno VeSota wanted to direct. He approached Roger Corman with the script and Corman helped him raise the low budget and arranged distribution through AIP. The film was shot in six days. Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p121-122 

The movie was known during production as The Keepers, The Keepers of the Earth, Attack of the Blood Leeches and Battle of the Brain Eaters. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 33 
 John Paynes intentions to produce a movie based on Heinleins novel

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 