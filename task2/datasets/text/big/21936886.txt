Panchavadi Palam
{{Infobox film
| name           = Panchavadi Palam
| image          = Panchavadi_Palam.jpg
| caption        =
| director       = K. G. George
| producer       = Gandhimati Balan
| story          = Veloor Krishnankutty
| screenplay     = K. G. George
| narrator       =
| starring       = Bharath Gopi Nedumudi Venu Sukumari
| music          = M. B. Sreenivasan
| cinematography = Shaji N. Karun
| editing        = MN Appu
| released       =  
| country        = India
| language       = Malayalam
}}
 1984 Malayalam film directed by K. G. George and starring Bharath Gopi|Gopi, and Nedumudi Venu.    The screenplay was written by K. G. George, based on a short story of the same name by Veloor Krishnankutty. The film is a satirical comedy with intentionally exaggerated story line and caricature-like characters.

The film tries to caricature the political scenario in the state of Kerala and especially corruption by politicians.

== Plot ==
A henpecked politician wants his name attached to a new bridge, even if that means destroying another, perfectly serviceable bridge. This politician Dussasana Kuruppu (Bharath Gopi) is the president of the village. He along with his other members tries to demolish the bridge on advice of his fellow party member, his right hand and crooked mind Sikhandi Pillai (Nedumudi Venu). The opposition leader Ishak Tharakan (Thilakan) makes moves to thwart this and to capture the position of the village presidency. As a compromise step, both groups agree to relocate the bridge to a distant location so that not only a new bridge, a new road also will be needed to reach the bridge.The two tenders, one for bridge and one for road is given to two local contractors, one belongs to the ruling party and the other one supported by opposition. Ultimately the bridge is built and opened to public on the same day of the marriage of Dussasana Kurups daughter and the Bridge contractor. The marriage function is attended by many including the minister of state for public works department and the entire people who came for marriage walks through the bridge causing it to collapse killing the crippled Kathorayan (Sreenivasan (actor)|Sreenivasan). The film ends by showing the broken bridge in the background. 

==Cast==
*Bharath Gopi as Dussasana Kuruppu
*Nedumudi Venu as Sikhandi Pillai
*Sreevidya as Mandodhari
*Thilakan as Ishak Tharakan
*Sukumari as Rahel
*Jagathi Sreekumar as Habel
*Venu Nagavalli as Jeemudhavahanan  Sreenivasan as Kathorayan Kalpana as Anarkali
*K. P. Ummer as Jahangir Thatha
*Alummodan as Yudaskunju Innocent as Barabas
*Mohan Jose
*V. D. Rajappan as Avarachan Swami Shubha as Poothana

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by Chowalloor Krishnankutty.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Naanayam kandal || KP Brahmanandan, CO Anto || Chowalloor Krishnankutty ||
|-
| 2 || Viplavaveeryam || Chorus, CO Anto || Chowalloor Krishnankutty ||
|}

== References ==
 

==External links==
* 

 
 
 
 


 
 