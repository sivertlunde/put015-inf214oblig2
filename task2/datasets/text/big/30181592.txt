Tonka (film)
{{Infobox film
| name = Tonka
| image = Tonka poster.jpg
| caption  = Theatrical release poster
| director = Lewis R. Foster
| producer = James C. Pratt
| writer = David Appel Lewis R. Foster Lillie Hayward
| starring = Sal Mineo Philip Carey Jerome Courtland Rafael Campos H.M. Wynant Joy Page Britt Lomond Herbert Rudley Sydney Smith John War Eagle Gregg Martel Slim Pickens Robert Buzz Henry
| music = Oliver Wallace
| cinematography = Loyal Griggs
| editing = Ellsworth Hoagland Walt Disney Productions Buena Vista Distribution
| released =  
| country = United States
| runtime = 97 minutes
| language = English
| gross = $2.5 million (est. US/ Canada rentals) 
}}
 Western adventure Buena Vista Distribution. 
 the horse of the title. It has also been released under the title A Horse Named Comanche. 

==Cast==
*Sal Mineo as White Bull
*Philip Carey as Captain Myles Keogh
*Jerome Courtland as Lieutenant Henry Nowlan
*Rafael Campos as Strong Bear
*H.M. Wynant as Yellow Bear
*Joy Page as Prairie Flower General George Custer
*Herbert Rudley as Captain Frederick Benteen
*Sydney Smith as General Alfred Terry
*John War Eagle as Chief Sitting Bull
*Gregg Martell as Corporal Korn
*Slim Pickens as Ace
*Robert "Buzz" Henry as Lieutenant Crittenden
*Eddie Little Sky as Spotted Tail 

==Notes==
 

==External links==
*   
* 

 

 
 
 
 
 
 
 
 
 
 

 