My Heart Belongs to Daddy (film)
{{Infobox film
| name = My Heart Belongs to Daddy 
| image =
| image_size =
| caption =
| director = Robert Siodmak
| producer = Sol C. Siegel 
| writer =  F. Hugh Herbert
| narrator =
| starring = Richard Carlson   Martha ODriscoll   Cecil Kellaway
| music = Leo Shuken   Victor Young
| cinematography = Daniel L. Fapp 
| editing = Alma Macrorie
| studio = Paramount Pictures 
| distributor = Paramount Pictures  
| released = November 7, 1942 
| runtime = 75 minutes
| country = United States English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
My Heart Belongs to Daddy is a 1942 American comedy film directed by Robert Siodmak and starring Richard Carlson, Martha ODriscoll, Cecil Kellaway. 

==Main cast== Richard Carlson as Prof. Richard Inglethorpe Culbertson Kay, aka R.I.C. Kay, aka Rick 
* Martha ODriscoll as Joyce Whitman  
* Cecil Kellaway as Alfred Fortescue  
* Frances Gifford as Grace Saunders  
* Florence Bates as Mrs. Saunders  
* Mabel Paige as Nurse Eckles  
* Velma Berg as Babs Saunders  
* Francis Pierlot as Dr. Mitchell 
* Ray Walker as Eddie Summers - Band Leader  
* Fern Emmett as Josephine - the Maid 
* Milton Kibbee as Chauffeur  
* Betty Farrington as Cook

==References==
 

==Bibliography==
* Greco, Joseph. The File on Robert Siodmak in Hollywood, 1941-1951. Universal-Publishers, 1999. 

==External links==
*  

 

 
 
 
 
 
 

 