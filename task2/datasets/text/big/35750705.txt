ACAB – All Cops Are Bastards
{{Infobox film
| name           = ACAB – All Cops Are Bastards
| image          = ACAB – All Cops Are Bastards.jpg
| alt            = 
| caption        = 
| director       = Stefano Sollima
| producer       = Marco Chimenz, Giovanni Stabilini, Riccardo Tozzi
| screenplay     = Barbara Petronio, Daniele Cesarano, Leonardo Valenti
| based on       = ACAB by Carlo Bonini
| starring       = Pierfrancesco Favino Filippo Nigro Marco Giallini 
| music          = Mokadelic
| cinematography = Paolo Carnera
| editing        = Patrizio Marone
| studio         = Cattleya Babe Film Rai Cinema
| distributor    = 01 Distribution
| released       =  
| runtime        = 112 minutes
| country        = Italy
| language       = Italian
}}

ACAB – All Cops Are Bastards is a 2012 Italian drama film directed by Stefano Sollima.

== Production ==

The film was the debut feature from director Stefano Sollima, who had previously directed TV crime show Romanzo Criminale. 

== Plot ==
 public demonstrations, evictions and everyday family life.

==Cast==
* Pierfrancesco Favino: Cobra
* Filippo Nigro: Negro
* Marco Giallini: Mazinga Andrea Sartoretti: Carletto
* Domenico Diele: Adriano
* Roberta Spagnuolo: Maria
* Livio Beshir: Mustafà
* Nick Nicolosi: school headmaster
* Alessandro Procoli: Mosque leader
* Alessandro Sanguigni: Cameo

==Music==
The music was composed and performed by the Italian post-rock band Mokadelic and was recorded entirely at the famous Forum Music Village studios in Rome.

== Critical response == sadism and vigilantism.    Cinealliance likewise found it powerful, with an intelligent and story effective direction, and despite fascist overtones.  Filmosphere gave it 3.5/5.   

== Awards and nominations ==
* 2012 – David di Donatello
** Nomination David di Donatello for Best New Director to Stefano Sollima
** Nomination David di Donatello for Best Supporting Actor to Marco Giallini
** Nomination David di Donatello for Best Cinematography to Paolo Carnera
** Nomination David di Donatello for Best Make-up to Manlio Rocchetti
** Nomination David di Donatello for Best Editing to Patrizio Marone
** Nomination David di Donatello for Best Sound to Gilberto Martinelli

==See also==
* A.C.A.B.
* Diaz – Don’t Clean Up This Blood
* Riot control

==References==
 

==External links==
*  

 
 
 
 
 
 
 