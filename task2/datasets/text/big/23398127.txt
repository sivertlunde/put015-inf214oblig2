Derborence (film)
 
{{Infobox film
| name           = Derborence
| image          = 
| caption        = 
| director       = Francis Reusser
| producer       = Jean-Pierre Bastid Jean-Marc Henchoz
| writer         = Charles Ferdinand Ramuz
| starring       = Isabel Otero
| music          = Maria Carta 
| cinematography = Emmanuel Machuel
| editing        = Christine Benoît Francis Reusser
| distributor    = 
| released       =  
| runtime        = 94 minutes
| country        = Switzerland France
| language       = French
| budget         = 
}}

Derborence is a 1985 French-Swiss drama film directed by Francis Reusser, based on Charles-Ferdinand Ramuzs 1934 novel of the same name. It was entered into the 1985 Cannes Film Festival.    

==Plot==
Thérèse, who works with her mother in a small village, has a crush on Antoine who lives in the mountains where he herds his uncles livestock. Their love finds a tragical end.

==Cast==
* Isabel Otero - Thérèse Maye
* Jacques Penot - Antoine Maria Machado - Aline
* Jean-Marc Bory - Nendaz
* Bruno Cremer - Séraphin
* Jean-Pierre Sentier - Plan
* Jean-Noël Brouté - Dzozet
* Teco Celio - Biolla
* Jean-Marc Stehlé - Loutre
* Maria Carta - Singer
* Michèle Foucher

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 