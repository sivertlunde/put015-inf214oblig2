Man Is Not a Bird
{{Infobox film
| name           = Man Is Not a Bird
| image          = Man Is Not a Bird 1965 film.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Dušan Makavejev
| producer       = 
| writer         = Dušan Makavejev
| starring       = Milena Dravić Janez Vrhovec Eva Ras Stole Aranđelović Boris Dvornik
| music          = Petar Bergamo
| cinematography = Aleksandar Petković
| editing        = Ljubica Nešić
| studio         = Avala Film
| distributor    = 
| released       = 1965
| runtime        = 81 minutes
| country        = Yugoslavia
| language       = Serbo-Croatian
| budget         = 
| gross          = 
}}
 European art film made in 1965.  It was the first film from director Dušan Makavejev.

In a 1974 review, Vincent Canby described it "by far the most original, intelligent, witty and important film Ive seen so far this year," and "the most sophisticated and complex film from a Communist country that Ive ever seen." 
{{citation
  | url=http://movies.nytimes.com/movie/review?res=9A00E3D6173FEF34BC4953DFB466838F669EDE
  | first=Vincent
  | last=Canby
  | title=Movie Review - A Man Is Not A Bird
  | date= February 1, 1974 |  publisher=The New York Times
}} 

==Plot==
Highly skilled engineer Jan Rudinski (Janez Vrhovec) comes to a mining town to install new heavy equipment. While in a barber shop, he asks his pretty blond hairdresser Rajka (Milena Dravic) if she knows where he can get a room for a few weeks. She does, in her parents home, where she also lives. She soon starts flirting with her much older lodger, while also fending off the advances of a handsome young truck driver (Boris Dvornik).

In an unrelated subplot, mine worker Barbulovic (Stole Aranđelović) gets into one mess after another. He is arrested for starting a bar brawl in which the singer, Fatima, is stabbed. He is released after a few days, and complains to his manager about his docked pay, to no avail. When his wife (Eva Ras) returns home from a visit, she discovers that three of her dresses are missing. She accuses him of giving them to his mistress. When she sees the other woman wearing one of them at the market, she attacks her rival. They are taken to the police station, but let go with some advice.

With her parents away for several days, Rajka invites Jan to sleep with her. The middle-aged man hesitates, concerned about the age difference between them, but soon gives in.  They are happy together. When she expresses concern that his work is nearly over and he will be leaving soon, he tells her he will take her with him. Rajkas parents return and learn what has been going on, whereupon they berate the couple.

Jan is asked to complete his work several days ahead of schedule so the plant can be part of a big export deal. When he drives his crew to finish the job as requested, a government representative from Belgrade comes to present him with a medal for his long exemplary service. Jan is puzzled when Rajka does not show up at a concert and banquet in his honor. She, it turns out, has succumbed to the charms of the truck driver. Jan asks her afterward if she has another lover. When she is evasive, he asks how old his rival is; she answers 20 or 22, but claims it does not matter to their relationship. This does not appease his anger, and she flees.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 

 