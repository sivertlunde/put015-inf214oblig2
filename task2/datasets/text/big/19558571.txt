Støv på hjernen (1961 film)
 
{{Infobox film
| name           = Støv på hjernen
| image          =
| caption        =
| director       = Poul Bang
| producer       =
| writer         = Arvid Müller Eva Ramm Aage Stentoft Øyvind Vennerød
| narrator       =
| starring       = Helle Virkner
| music          = Sven Gyldmark
| cinematography = Ole Lytken Aage Wiltrup
| editing        = Edith Nisted Nielsen	
| distributor    =
| studio         = Saga Studio
| released       =  
| runtime        = 104 minutes
| country        = Denmark
| language       = Danish
| budget         =
}}

Støv på hjernen is a 1961 Danish film directed by Poul Bang and starring Helle Virkner. It is a remake of the Norwegian film Støv på hjernen (1959 film)|Støv på hjernen from 1959.

==Cast==
* Helle Virkner - Fru Bodil Henriksen
* Søren Elung Jensen - Hr. Arne Henriksen
* Dirch Passer - Alf Thomsen
* Hanne Borchsenius - Frk. Monalisa Jacobsen
* Bodil Udsen - Fru Rigmor Hansen
* Ove Sprogøe - Hr. Thorbjørn Hansen
* Karin Nellemose - Fru Birthe Mynster
* Emil Hass Christensen - Hr. Mogens Mynster
* Beatrice Palner - Fru Lene Svendsen
* Henning Palner - Hr. Viggo Svendsen
* Paul Hagen - Sælgeren
* Karl Stegger - Hr. Tim Feddersen
* Ingrid Langballe - Fru Bolette Feddersen
* Else Kornerup - Sælger ved husmodermøde
* Kitty Beneke - Fru Kristoffersen
* Børge Møller Grimstrup - Mand der vil have stilhed
* Miskow Makwarth - Gæst hos Mynster
* Bjørn Puggaard-Müller - Bibliotekar
* Gunnar Lemvigh - Repræsentant fra Pral
* Gunnar Bigum - Scooter forhandler
* Jan Priiskorn-Schmidt - Claus Henriksen
* Ralf Dujardin - Thomas Henriksen
* Edith Hermansen - Hustru til mand, der vil have stilhed
* Alex Suhr - Taxa-chauffør
* Klaus Nielsen - Gæst hos Mynster

==External links==
* 

 
 
 
 
 
 