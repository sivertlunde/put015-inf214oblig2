Alice Neel (film)
{{Infobox film
| name           = Alice Neel
| image          = ALICE_POSTER_web.jpg
| caption        = Promotional poster for Alice Neel
| director       = Andrew Neel
| producer       = Ethan Palmer,   Rebecca Spence
| writer         = Andrew Neel Robert Storr
| music          = Jonah Rapino
| cinematography = Andrew Neel, Ethan Palmer, Hillary Spera
| editing        = Luke Meyer
| studio          = SeeThink Films
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
}}
Alice Neel is a 2007 documentary film about the life of Alice Neel, exploring the struggles she faced as a woman artist, a single mother, and a painter who defied convention. The documentary was directed by Neels grandson, Andrew Neel.

Alice Neel premiered at The Sundance Film Festival in 2007 and later won the Audience Award at the 2007 Newport Beach Film Festival later that year. The film was produced by SeeThink Productions.

== External links ==
*  
*  
*  
*  
; Reviews
*  
*  
*  
*  

 
 
 
 
 
 

 
 