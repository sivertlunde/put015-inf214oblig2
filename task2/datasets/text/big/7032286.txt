Jedara Bale
 
 

 
{{Infobox Film
| name       = Jedara Bale ಜೇಡರ ಬಲೆ
| image      = Jedara Bale.jpg
| director   = Dorai-Bhagavan
| producer   = T. P. Venugopal
| language   = Kannada
| writer     = B. Dorairaj S. K. Bhagvan
| screenplay = B. Dorairaj S. K. Bhagvan
| music      = G. K. Venkatesh Rajkumar Jayanthi Jayanthi K. Narasimharaju Udaya Udayakumar
| cinematography = B. Dorairaj R. Chittibabu
| editing    = N. C. Rajan Raghupathi
| distributor= Manthralaya Movies
| released   = 12 January 1968
| runtime    = 139 minutes
| country    = India
| budget     =  
}}
 Kannada film Narasimharaju and Udaya Kumar (actor)|Udayakumar. It was the first of a series of movies created along the lines of the James Bond films. The music of the film was composed by G. K. Venkatesh. 

==Plot summary== Rajkumar plays a CID Police Agent, who is code-named as "999". The story revolves around the attempt to stop a formula which can convert any metal into gold reaching the hands of hooligans. Uday Kumar plays the honcho of the villain gang.

== Cast == Rajkumar as Prakash Udayakumar
* Jayanthi
* K. S. Ashwath as Rao Bahaddur Narasimha Rao Narasimharaju as Baby, a Taxi driver
* M. P. Shankar
* Shakti Prasad

==Trivia==
* It is the first Bond movie ever made in Kannada and to introduce cabaret dances in Indian film industry.
* The movie had many firsts in the Kannada Industry, like the hero not using wigs, the heroine donning swim suits etc.
* The success of the movie inspired 3 more Bond-inspired movies, all of which were made with Rajkumar (actor)|Rajkumar:
*Goa Dalli CID 999
*Operation Jackpot Nalli CID 999 (debut film of Hindi actress Rekha)
*Operation Diamond Racket

* More movies followed like Ratnagiri Rahasya and Dwarakishs Kulla series.

==Sources==
* 

== References ==
 

== External links ==
*  

 
 
 
 
 


 