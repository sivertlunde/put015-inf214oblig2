Ujwadu
{{Infobox film
| name           = Ujwadu
| image          = 
| image size     = 
| caption        = 
| director       = Kasargod Chinna
| producer       =  KJ Dhananjaya Anuradha Padiyar
| writer         = Gopalakrishna Pai
| screenplay     = 
| story          = 
| starring       = 
| music          = V. Manohar
| cinematography = Utpal Nayanar
| editing        = Suresh Urs
| studio         = Mithra Media Pvt. Ltd
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Konkani
| budget         = Rs.45 laks
| gross          = 
}}
 Konkani film Saraswatha tradition and culture.

Released October 14, 2011, and with music by V Manohar, the film stars Shivadhwaj, Neethu, Sadashiv Brahmavar, Sandhya Pai, Prameela Nesargi, Dr. Ananth Prabhu, Sandhya Pai, and Umashree.

==Plot==
Ujwadu depicts the story of simple happy-go-lucky Gowda Saraswat Brahmins way of life with turmoil and struggle within.

==Cast==
* Umashree
* Shivadhwaj
* Neethu
* Sadashiv Brahmavar
* Sandhya Pai
* Prameela Nesargi
* Dr. Ananth Prabhu
* Sandhya Pai
* Srinivas Sheshgiri Prabhu ( as Gajju)

==Production== Muhurat in Kumaraswamy Lay Out. Celebrities from Kannada Film world including Girish Kasaravalli, T. S. Nagabharana, Jayamala, Umashree and Dr. K. Ramesh Kamath who produced and directed Konkani film in the early Eighties namely ‘Jana Mana’. 
Ujwadu has been cleared by the Regional Censor Board and granted U certificate, 

The film was shot around Karkala and Mangalore, and is the Third film of GSB Konkani language.  Prior to its release, the only two GSB Konkani films produced were Tapaswini and Jana Mana. {{cite web|title=www.gsbkonkani.net/konkani.htm 
|accessdate=24 December 2011}} 

==References==
 
* Konknni Cholchitram, Isidore Dantas, 2010
* 50 Years of Konkani Cinema, Andrew Greno Viegas, 2000
* www.indiaglitz.com/channels/Kannada/article/69972.html 

 
 
 
 


 