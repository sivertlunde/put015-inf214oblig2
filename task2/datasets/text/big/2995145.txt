April Story
{{Infobox film
| name           = April Story (四月物語)
| image          = Aprilstorydvd2.jpg
| writer         = Shunji Iwai
| starring       = Takako Matsu Fujii Kahori Tanabe Seiichi
| director       = Shunji Iwai
| producer       = Hidemi Satani
| distributor    = Rockwell Eyes
| released       = March 14, 1998
| runtime        = 67 min.
| country        = Japan Japanese
| music          = Shunji Iwai
}}

April Story (  ese short film directed by Shunji Iwai starring Takako Matsu.

==Plot==
Uzuki Nireno, a shy girl from the countryside of northern Hokkaidō leaves her family and gets on a train bound for Tokyo so she can attend the university of her choice; to be near the boy she fell in love with, who moved to Tokyo from her hometown.

==Cast==
*Takako Matsu as Uzuki Nireno  	
*Seiichi Tanabe as Yamazaki 	 	
*Kahori Fujii as Teruko Kitao
*Kazuhiko Kato as The Man in the Art Gallery
*Go Jibiki

==Crew==
*Director/Writer: Shunji Iwai  	
*Director of Photography: Noboru Shinoda 	
*Lighting Director: Yuki Nakamura 	
*Production Designer: Yuji Tsuzuki

==DVD Editions==
*Japanese R2 (No English Subtitles)
*Korean R3 (English Subtitles) Full Chinese subtitles)

==Festivals==
*Toronto Film Festival, 1998
*Pusan International Film Festival, 1998

==Awards==
*1998 Pusan International Film Festival: Audience Award

==References==
 

==External links==
*  

 

 
 
 
 

 