The Committee on Credentials
 
{{Infobox film
| name           = The Committee on Credentials
| image          = The Committee on Credentials.jpg
| caption        = George Marshall
| producer       = Bison Motion Pictures
| writer         = Harvey Gates Peter B. Kyne Harry Carey
| cinematography =
| editing        =
| distributor    = Universal Pictures
| released       =  
| runtime        = 3 reels rereleased at 2 reels
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 silent film Harry Carey. Peter Bernard Kyne. It follows the protagonist Ballrat Bob, who tries to protect an acquaintance and squatter, Clem, from gambling away all of his money.

==Plot==
In an attempt to protect the welfare of Clems wife, Ballrat Bob takes Clems savings and puts them in safe keeping with Clems wife. This causes Clem to think his wife is having an affair; he goes on to shoot up the town and eventually ends up in a confrontation with Bob. 

Kyne said of his novel, "I have at last finished writing "The Pride of Palomar." It isnt at all what I wanted it to be; it isnt at all what I planned it to be, but it does contain something of what you and I both feel, something of what you wanted me to put into it. Indeed, I shall always wish to think that it contains just a few faint little echoes of the spirit of that old California that was fast vanishing when I first disturbed the quiet of the Mission Dolores with infantile shrieks—when you first gazed upon the redwood-studded hills of Sonoma County." 

Of the silent films in the early 20th century, Peter French says calls it a dramatic tale of "personal crimes, moral tales, and the place of crime in public life". 

==Cast== Harry Carey
* George Berrell
* Neal Hart
* Joe Rickson
* Olive Carey (as Olive Fuller Golden)
* Elizabeth Janes

==See also==
* List of American films of 1916
* Harry Carey filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 