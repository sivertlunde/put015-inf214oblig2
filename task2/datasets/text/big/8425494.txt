Dharma Chakram
{{Infobox film
| name           = Dharma Chakram
| image          =
| image_size     = 
| caption        =  Suresh Krishna
| producer       = D. Ramanaidu
| music          = M. M. Srilekha
| screenplay     = Suresh Krishna Venkatesh Ramya Prema
| writer         = Suresh Krishna (story) MVS Harinath Rao (dialogues)
| cinematography = K. Ravindra Babu
| editing        = K.A. Martand  K.Venkatesh
| studio         = Suresh Productions
| distributor    = 
| released       = 13th January 1996
| runtime        = 2:24:34 
| country        = India
| language       = Telugu
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Tollywood film Suresh Krishna. Prema in Venkatesh received Nandi Award for Best Actor & won the Filmfare Best Actor Award (Telugu).

==Plot== MLA thats why he is separated from him. Sandhya (Ramya Krishnan) a journalist tries for Rakesh interview but he does not cooperate so she follows him like white on rice and falls in love with him. Sandhya tells her love to Rakeshs mother Sarada then she reveals Rakeshs past.

Rakesh is a energetic guy 4 years back who enjoys day to day life, he falls in love with a middle class girl Surekha (Prema (actress)|Prema), but his father Mahendra fixes his marriage with a big politicians daughter for his political growth. Rakesh rejects that match and makes marriage arrangements with Surekha, to stop it Mahendra traps Surekha and proves her as a prostitute in court for that Surekha commits suicide. In the anger Rakesh goes to kill Mahendra but he leaves him for his mothers sake and they both separated from him. After listening this Sandhya still loves him, she expresses her love to him and slowly he also starts liking her and prepares himself to marry her. 

Meanwhile, Mahendra gets a problem to his political carrier because of his family disturbances, he tries again to patch up with his family but Rakesh turns down him, in that frustration he kills his keep Balamani (Krishnaveni). To escape from the murder case he asks Rakesh to take case but he refuses, he blackmails him a lot even then Rakesh doesnt come down. At last Mahendra plays with Saradas emotions which influences Rakesh to take up Mahendras case, that torches Rakesh a lot because he knows that he is doing injustice.

Finally, his mother gives freedom to him to fight against Mahendra. Ultimately Rakesh proves that Mahendra is a culprit and protects the justice.

== Cast ==
  Venkatesh as Rakesh
* Ramya Krishnan as Sandhya Prema as Surekha
* Girish Karnad as Mahendra
* Srividhya as Sarada
* D. Ramanaidu as Party President
* Brahmanandam  AVS as Gumastha Gurulingam Rallapalli as Anjibabu
* J. V. Somayajulu as Judge
* Subbaraya Sharma as Surekhas Father
* MVS Harinath Rao as Singaram
* Kota Shankar Rao as Public Prosecutor
* Ananth as Singarams Henchmen Saradhi as TV Shop Owner
* Telangana Shakuntala
* Krishnaveni as Balamani
* Ratna Sagar
* Dubbing Janaki
 

==Soundtrack==
{{Infobox album
| Name        = Dharma Chakram
| Tagline     =
| Type        = Soundtrack
| Artist      = M. M. Srilekha
| Cover       =
| Released    = 1996
| Recorded    =
| Genre       = Soundtrack
| Length      = 28:38
| Label       = Supreme Music 
| Producer    = M. M. Srilekha 
| Reviews     =
| Last album  = Oho Naa Pellanta   (1996) 
| This album  = Dharma Chakram   (1996) 
| Next album  = Navvulatha   (1997)
}}

Music composed by M. M. Srilekha. Music released on SUPREME Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 28:38
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Thama Soma Mama  Chandrabose
| SP Balu
| length1 = 4:11

| title2  = Meera Sammere
| lyrics2 = Veturi Sundararama Murthy SP Balu,K. Chitra 
| length2 = 4:57

| title3  = Cheppana Cheppana Chandrabose
| SP Balu,M. M. Srilekha
| length3 = 5:03

| title4  = Hello Hello 	 Chandrabose
| SP Balu,K. Chitra 
| length4 = 4:31

| title5  = Sogasuchuda  Chandrabose
| SP Balu,K. Chitra
| length5 = 4:51

| title6  = Aagadaye Ranam   
| lyrics6 = Veturi Sundararama Murthy   SP Balu
| length6 = 5:05
}}

== External links ==
*  

 

 
 
 
 
 
 


 