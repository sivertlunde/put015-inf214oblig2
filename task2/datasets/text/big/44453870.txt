Mamachya Gavala Jaaoo Yaa

{{multiple issues|
 
 
}}

{{Infobox film
| name            = Mamachya Gavala Jaaoo Yaa
| film name       = मामाच्या गावाला जाऊ या
| image           = Mamachya Gavala Jaaoo Yaa Movie Poster.jpg
| caption         = Theatrical release poster
| director        = Sameer Hemant Joshi
| producer        =  Punkaj Challani
| writer          = Sameer Hemant Joshi
| cinematographer = 
| starring        = Abhijit Khandelkar  Mrunmayee Deshpande  Saahil Malge   Shubhankar Atre   Arya Bhargude   Devendra Gaikwad   Ninad Mahajani   Saniya Godbole   Shaunak Joshi
| music           = Prashant Pillai
| released        =      
| language        = Marathi
| country         = India
| runtime = 103 minutes
| distributor     = Punkaj Challani Films
}}
 Marathi thriller adventurous film directed by Sameer Hemant Joshi starring Abhijit Khandelkar and Mrunmayee Deshpande in lead roles.    

==Plot==
The story of the film is about the uncle(Mama) we all look up to. He is a friend, a partner in crime, and someone your mother adores, a guy who you look up to, but who lives far away! But distance often bringhearts closer, and that’s why one of the most adorable of the relationships keeps growing stronger with our Mama!
Mamachya Gavala Jaaoo Yaa is about this quintessential Mama and his world-apart nephews and niece on an adventurous journey!

==Cast==
* Abhijeet Khandelkar as Nandu Devkar
* Mrunmayee Deshpande
* Saahil Malge
* Shubhankar Atre
* Arya Bhargude
* Devendra Gaikwad 
* Ninad Mahajani
* Saniya Godbole
* Shaunak Joshi

==Soundtrack==
Music of the film is scored by Prashant Pillai while the lyrics are penned by Sandip Khare and Avdhoot Gupte.

==Release==
The film released all over the Maharashtra on 21 November 2014.

==References==
 

==External links==
*  

 
 
 


 