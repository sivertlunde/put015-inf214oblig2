Parenti serpenti
{{Infobox film
| name = Parenti serpenti
| image = Parenti serpenti.jpg
| caption =
| director = Mario Monicelli
| story = Carmine Amoroso
| writer = Carmine Amoroso (play), Piero De Bernardi, Mario Monicelli, Suso Cecchi dAmico
| starring = Alessandro Haber Cinzia Leone Paolo Panelli
| music = Adelio Cogliati
| cinematography = Franco Di Giacomo
| editing = Ruggero Mastroianni
| producer = Giovanni Di Clemente
| distributor = Clemi Film
| released =1992
| runtime = 105 min
| awards =
| country = Italy 
| language = Italian
| budget =
}} 1992 Italian black comedy film written and directed by Mario Monicelli.      It won the Silver Ribbon for  Best Costumes.   

== Plot ==
 
An old couple invite all their children and grandchildren to their home in Sulmona, in Abruzzi, to celebrate the Christmas holidays. After a day spent at church and playing bingo at home, the grandmother asks her two daughters and two sons to decide amongst themselves which of them will take her and her husband to live with them, now that they are getting old. Their children are initially pleased to hear that their parents want to see more of them, but no one wants to take on the responsibility of having them move into their home...

== Cast ==
* Marina Confalone: Lina
* Alessandro Haber: Alfredo
* Tommaso Bianco: Michele
* Cinzia Leone: Gina
* Eugenio Masciari: Alessandro
* Paolo Panelli: Grandfather Saverio
* Monica Scattini: Milena

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 