High School Musical: El Desafío
{{Infobox film
| name           = High School Musical: El Desafío  (The Challenge)
| image          = HSM El Desafio.PNG
| caption        = Theatrical release poster
| director       = Jorge Nisco
| producer       = Walt Disney Pictures   Patagonik Film Group
| writer         = Pablo Lago
| narrator       = 
| starring       = Fernando Dente Agustina Vera Delfina Peña Walter Bruno
| music          = Fernando López Rossi
| cinematography = 
| editing        = 
| distributor    = Walt Disney Pictures
| released       =  
| runtime        = 98 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          = $1,766,913 
}} Battle of the Bands. It began filming in February 2008 and it is the one Disney-branded feature film made in Latin America. The first was Una Pelicula de Huevos and the second is El Arca. {{cite web 
| title       = High School Musical retunes to suit Latin America
| accessdate  = December 15, 2008
| author      = Reuters/Billboard
| date        = July 12, 2008
}} 
The movie has a script created specifically for the cinema and an original plotline. Eight new songs have been produced exclusively for the movie. In addition to these songs, Alejandro Lerner also composed the main theme, which he performed, and which is played during the closing credits.

The roles in the movie are played by the winners and finalists of the Argentine TV program  : Agus, Fer, Delfi, Walter, Gastón, Vale, Sofi and Juanchi. It also has the special participation of Andrea del Boca, Adriana Salonia, Peter McFarlane and the debut in a film of Liz Solari. The movie arrived in Argentine theaters on July 17, 2008, reaching in its debut week second place in the national box-office charts.
 {{cite web
| url         = http://www.clarin.com/diario/2008/07/18/um/m-01718217.htm
| title       = The new "Batman" began with all in the Buenos Aires cinemas 
| accessdate  = November 13, 2008
| date        = July 18, 2008
| publisher   = Clarín.com
| location    = Buenos Aires
| language    = Spanish
| quote       = (Fifth paragraph) In the Argentine billboard, meanwhile, remained in the second place is the Disney production filmed in the country, "High School Musical: El Desafio", which have an attendance of 32,301 people in 100 rooms, also in his debut.
}} 

== Plot ==
A new school year begins at the High School Argentina (HSA), and the students return from the summer vacations. Fer, the captain of the school rugby team, the Jaguars, discovers that Agus, his neighbour and classmate, has changed a lot over the summer. Delfi, however, continues being vain and wastes her time dominating her poor brother, Walter, and her associates Alicia, Clara and Valeria, or, as she prefers to call them, "The Invisibles".

The principal of the school and Ms. DArts, the art teacher, invite the students to take part in the schools first battle of the bands, where the kids will have a chance to be showcased as true music stars. Anne-Claire, a former student and now a famous singer, comes to the school as adviser to the contest. Delfi envies her greatly.

Working against the clock and with limited resources, the kids put all the forces for the big day. Fer and Agus, together with Juanchi, Sofi, Facha, Gaston and Walter participate in the contest, forming a band called the Scrum. At the same time, Delfi participates with her friends, and she tries the impossible task of separating Walter from his new friends. But only one band will be the winner, the one which can understand that teamwork, personal development, and study will make them better artists and also better people.

==Characters==

===Main===

* Walter (Walter Bruno) is the male  .)

* Agus (Agustina Vera) is the female protagonist of the movie. She is the shy and studious student who, under the astonished eyes of all her peers, suddenly becomes an attractive young girl with a talent for singing. When she feels insecure, Anne-Claire encourages her to be herself and exhibit her artistic talents without fears. (The equivalent of Gabriella Montez.)

* Delfi (Delfina Peña) is the  .)

* Fer (Fernando Dente) is Delfis brother, who must use all his ingenuity to evade the surveillance of his sister to be the coach of Fers band, the Scrum. With Aguss help, and with his perseverance and recklessness, Walter manages to become independent and show his true artistic abilities. (The equivalent of Ryan Evans.)

===Other students===

* Gastón (Gastón Vietto) is Fers best friend, who has a deep platonic love with Anne-Claire. There is nothing in the world he wouldnt do for her. (The equivalent of Chad Danforth.)

* Juanchi (Juan Macedonio). When there is a funny comment or sudden laughter, that is Juanchi, the guy who never loses his sense of humor. He is also a good drummer. (The equivalent of Chad Danforth.)

* Facha (Augusto Buccafusco). He does what he can with his guitar ... literally because the instrument has the unfortunate habit of falling down! Anne-Claire is the one who finally pulls out of the muck at the crucial moment in his performance. (The equivalent of Jason Cross.)

* Sofi (Sofía Agüero Petros) is the talented composer of the band. She also demonstrates her positive attitude when time is their principal enemy. (The equivalent of Kelsi Nielsen.)

* Vale (Valeria Baroni) is a great singer, but because she joins Delfis musical group, she is always overshadowed by the "star". (The equivalent of Taylor McKessie.)

* Clarita and Alicia (María Clara Alonso and Sophie Oliver Sánchez) are Delfis unconditional allies and members of her band. The two girls are completely subordinated to the whims and arrogance of the "star". (The equivalent of The Sharpettes.)

===Supporting===

* Ms. DArts (Andrea del Boca) is HSAs art teacher, who, together with the principal, convenes the Battle of the Bands. (The equivalent of Ms. Darbus.)

* The High School Principal (Peter McFarlane)

* Anne-Claire (Liz Solari) is the adviser of the contest, is a former HSA student and now a famous singer and pianist. (the equivalent of Taylor McKessie.)

* Agus Mum (Adriana Salonia)

* Fers Dad (Mauricio Dayub) rugby coach of the team, pushes them hard. (the equivalent of Coach Bolton)

* Donato (Daniel Martins)

* Marta (Carolina Ibarra)

==Premiere==

The special premiere took place on July 14, 2008 at the Gran Rex Theater. Following the film, the cast and the director, Jorge Nisco, went to the Alvear Hotel for a photo session and a press conference.

The film debuted in cinemas throughout Argentina on July 17, 2008.

==Soundtrack==
{{Infobox album 
| Name        = High School Musical: El Desafio
| Type        = soundtrack
| Artist      = Cast of HSM: El Desafio
| Cover       = HSM El Desafio CD.JPG
| Released    = June 17, 2008
| Recorded    = 2007
| Genre       = Pop music|Pop, teen pop, pop rock, musical theatre
| Length      = 
| Label       = Sony BMG
| Producer    = Fernando López Rossi
| Last album  = Sueños   (2007)
| This album  = High School Musical: El Desafio (2008)
| Next album  = 
}}{{Album ratings
| rev1      =Musimundo  | rev1Score =    
|noprose=yes}}
Before the release of the movie, the films soundtrack CD was released, featuring eight new songs produced by Fernando López Rossi. The album has a bonus track with the song "Now Is Time to Shine" (originally in Spanish, "Ya es tiempo de brillar") written by Alejandro Lerner especially for the film, and includes the music video of the song "The Summer Was Ended" ("El Verano Terminó").

===List of songs===

{| class="wikitable" border="1"
|- 
! # || Song || Performed by
|- bgcolor=#FFF0F5"
| 1 || El Verano Terminó (Summer Has Ended)  || All
|- bgcolor="efefef"
| 2 || Siempre Juntos (Forever Together) || The Jaguars
|- bgcolor=#FFF0F5"
| 3 || Doo up || Delfy and Walter
|- bgcolor="efefef"
| 4 || Yo sabía... (I Knew...) || Agus and Fer
|- bgcolor=#FFF0F5"
| 5 || A Buscar el Sol (Look for the Sun) || Agus
|- bgcolor="efefef"
| 6 || Hoy Todo es Mejor (Today Everything is Better) || Fer
|- bgcolor=#FFF0F5"
| 7 || Superstar || Delfi and The Shadows (Vale, Clara and Alicia)
|- bgcolor="efefef"
| 8 || Mejor Hacerlo Todo Juntos (Its Better Doing It All Together) || Agus, Fer, Juanchi, Gaston, Facha and Sofi
|- bgcolor=#FFF0F5"
| 9 || Actuar, Bailar, Cantar (Act, Dance, Sing) || All
|- bgcolor="efefef"
| 10 || Ya es Hora de Brillar (Now It Is Time to Shine) || Alejandro Lerner
|- bgcolor=#FFF0F5"
| BT || El Verano Terminó (Video) (Summer Has Ended (Video))  || All
|}

==Other international versions==

Following this production in Argentina, the film was also remade for Mexico and in Brazil:

*In Mexico, as in   and hip hop style to appeal to the Mexican public. {{cite web 
| url         = http://uk.reuters.com/article/musicNews/idUKN1126047920080712
| title       = "High School Musical" retunes to suit Latin America
| accessdate  = December 15, 2008
| author      = Reuters/Billboard
| date        = July 12, 2008
}}  Ironically, the Mexican version was also filmed in Argentina. The Mexican version premiered on September 4, 2008. {{cite web
| url         = http://www.todopuebla.com/noticias/espectaculos/2008-09-04/14937
| title       = High School Musical: El Desafío is coming
| accessdate  = November 13, 2008
| date        = September 3, 2008
| publisher   = TodoPuebla.com
| location    = Puebla
| language    = Spanish
| archiveurl  = http://web.archive.org/web/20081208061401/http://www.todopuebla.com/noticias/espectaculos/2008-09-04/14937
| archivedate = 2008-12-08
}} 

*In  . From there emerged Renata Pinto Gomes Ferreira and Olavo Setembro Cavalheiro as the leads, along with Paula Pereia Barbosa and Fellipe Ferreira Guadanucci as the antagonists. {{cite web
| url         = http://hsm-club.nireblog.com/post/2008/05/23/high-school-musical-a-selecao
| title       = High School Musical - A seleção
| accessdate  = November 13, 2008
| date        = May 23, 2008
| publisher   = HSM-Club
| language    = Portuguese
}}  The Brazilian version is planned to premiere in fev 2010.

== References ==
 

==External links==
*    
*    

 

 
 
 
 
 