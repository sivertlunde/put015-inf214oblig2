It Takes a Man and a Woman
{{Infobox film
| image            = It Takes a Man and a Woman.jpg
| caption          = Theatrical movie poster
| name             = It Takes a Man and a Woman
| director         = Cathy Garcia-Molina
| producer         = Vicente G. del Rosario III 	 
| music            = Jessie Lasaten
| cinematography   = Manuel Teehankee
| screenplay       = Carmi Raymundo
| writer           = Carmi Raymundo
| editing          = Marya Ignacio	
| starring         = John Lloyd Cruz Sarah Geronimo Isabelle Daza Rowell Santiago Johnny Revilla Bing Pimentel Al Tantay Irma Adlawan Miles Ocampo Andrei Garcia Matet de Leon Joross Gamboa Gio Alvarez Guji Lorenzana Dante Rivero
| distributor      = Star Cinema
| studio           = ABS-CBN Film Productions Viva Films Star Cinema 
| released         =  
| country          = Philippines
| language         =  
| runtime          = 127 minutes
| gross            =   PHP 405,258,082.39    
}}
 preceding sequel.
 nominal values) Best Actor, Best Actress and Best Editing. 

==Plot== second film. Miggy is now in a relationship with Belle (Isabelle Daza), while Laida is now a fiercer woman after living in the United States. They try to co-exist in the same company, while Laida tries to oppose Miggys business decisions through a series of events which made them realize the real definition of true love.
 
After two years, it is shown that Miggy has lost the trust of the board members since he lost the companys aircraft business, and is in need of Laidas help. Coming back from New York, Laida is now stronger than she was before and seems to have moved on from Miggy. The movie shows flashbacks of how Laida and Miggys relationship turned around when Laidas father cheated on her mother and flies back to Manila. However, at the same time Miggys father receives a heart attack and wants Laida to be with him. Unfortunately Laida is stuck comforting her mother and is in delay of attending Miggys fathers funeral. He is comforted by Belle, an old family friend, and later share a kiss. Laida witnesses this, and breaks up with Miggy.

Miggys life is screwed up with the loss of Laida and his father, and becomes more stubborn. When Laida comes back to Flippage, she wears different clothing and speaks differently while presenting her presentation. She and Miggy feel awkward towards each other and when trust is brought up, she leaves the conference room. Miggy runs after her. He refuses to say that he needs her but in the end, Laida returns to help. As the days go by, Laida is very distant from her old friends. Even though she denies being jealous, everybody knows how she really feels towards Miggy and Belles relationship.

After they fight over Laidas decision to present a womans theme on Bachelor to MET, Zoila and friends present a theme of transformation on Laida. Miggy agrees and the next day, they begin a photo shoot at Laidas home, but something is missing and Zoila fails to see what it is. Laidas father says its her smile, and Laidas smile is up to her eyes. They try to put Laidas family in front of her, but it doesnt seem to make her smile. Zoila and friends do the rain dance but she laughs and doesnt get the smile they are asking for. They ask Miggy to do the rain dance but Laida just seems to be really upset and calls it a day. That evening, Miggy buys pizza for all and just like the first film he gets a plate, puts a pizza on it and gives it to Laida. She refuses. Later on, Zoila and her friends wont stop till they make Laida happy. They decide to sing Laida and Miggys favorite song Kailan with Miggy singing his favorite part. However, when Miggy sings, Laida gets teary eyed so he stops singing. They look into each others eyes and Miggy sees how unhappy and hurt Laida is. Laida calls it a night.

The next day, Miggy is looking for Laida when she hands him a letter of resignation. He chases her to the elevator and presses a button to keep them stuck in it. Inside, she expresses how she really feels and Miggy blames her for throwing everything away after just one mistake. She argues back, telling him she did not go to Canada just for him, accepted all his flaws and left her mother alone for a little while whilst she went to visit him after the funeral, was late and all he does is throw everything away because she was late. She walks out the elevator, and Miggy is very upset.

The next few days, she reads a newspaper article about why they hired her and what Miggy went through. She sees him and they sit down and talk. He explains that he had everything but when his father died, he lost confidence. Then, he lost Laida, which destroyed him, and couldnt cope with everything so he lost the aircraft to the Ortegas. Laida makes a truce and accepts his apology. The next day, they fly to New York and find a hotel to stay in. However, a problem occurs in the hotel and they are forced to sleep in Laidas apartment, and Miggy sleeps on the bedroom floor. Their meeting with MET gets canceled. Instead, they take a tour of New York. The next day, theyre about to share a kiss when someone knocks on the door- Belle. They even show their presentation to MET with Belle, and make their way home.

The next few days, Miggy realizes he still loves Laida and breaks up with Belle. In the board meeting, MET decides not to let them publish their magazine in the Philippines. However, they agree on a partnership between MET and Flippage on the condition of Laida going back to New York. At the airport, as Laida walks by, everybody is singing lines of Kailan, Miggy and Laidas favorite song. Zoila and her friends are there, so are Miggys family. Miggy asks Laidas parents for her hand in marriage. Laidas mother says its up to her to decide. Laida agrees and they are married. The story ends on the couples honeymoon.

==Cast==
*John Lloyd Cruz as Miguel "Miggy" Montenegro. The youngest of the Montenegro family, Miguel is troubled and hides that he has still loved Laida all along. In this film, he is in a relationship with Belle. He is having problems in the family with losing the aircraft and is trying to do everything to gain the trust back of the board members. 
*Sarah Geronimo as Adelaida "Laida" Magtalas. Now a fiercer woman, she tries to hide her true feelings she still has for her one true love, Miguel. She is an EC in New York, and comes back to the Philippines to help Flippage, the company where she first started. There are new changes in the Montenegro company, her friends, her family especially her relationship with Miggy. 
*Isabelle Daza as Belle Laurel. The girlfriend, who was never loved. She tries hard to be involved with Miggy, although deep inside, she knows where Miggys true heart belongs. It is revealed that she was the reason of the breakup between Miggy and Laida. Also, her and Miggy broke up and she left him but he came back for her. She follows Miggy to New York in Laidas apartment where she breaks the kiss moment between the two.
*Rowell Santiago as Arturo "Art" Montenegro. Art trusts Miggy more now, and has got a bigger and fonder relationship with him since their fathers death.
*Johnny Revilla as Roger Montenegro
*Bing Pimentel as Alice Montenegro
*Al Tantay as Tomas Magtalas
*Irma Adlawan as Baby Magtalas
*Miles Ocampo as Rose Magtalas
*Andrei Garcia as Lio Magtalas. The little brother, still hoping for his sister and his idol Miggy to rekindle their relationship. 
*Matet de Leon as Zoila
*Joross Gamboa as John Rae
*Gio Alvarez as Vincent
*Guji Lorenzana as Carlo
*Dante Rivero as Luis Montenegro (Special Participation)

==Music== Expressions (2013). 

==Release==
===Box office=== nominal values) nominal values) nominal values).   It is the 2nd highest-grossing film and the highest-grossing Filipino film released in the Philippines in 2013.  The film is arguably the 5th highest-grossing Filipino film of all-time.  

===Critical reception===
The film has received generally unfavorable reviews from critics.   Although review aggregator Rotten Tomatoes website 
had an audience score of 74%, one consensus states: "The final leg of director Cathy Garcia-Molinas exceptionally broad, partly English-dubbed cockles-warmer of a trilogy outright apes Hollywood rom-com formulas with a personality so affably lobotomized it wouldnt dare frighten delicate tastes".  

Philbert Ortiz Dy gave 3 out 5 stars and saying, "There are plenty of sweet moments to be found in It Takes A Man and A Woman, but there’s a lot of bloat to get through  It might have done the film good to just take a more subdued approach, trusting in the talents of the two leads to provide the mainstream appeal. That said, the current approach still provides some charm. There’s just a sense that it could have been so much more".  
Likewise, Nestor Torre of the Philippine Daily Inquirer wrote: "To be sure, the film wins thematic points with its emphasis on the importance of forgiveness, of really moving on—so, when the constantly bickering ex-lovers finally face up to their hurts and/or guilt, the heretofore sluggish and unfocused drama finally hits viewers where they live, hurt and love. —Unfortunately, it’s too little, too late to save the film". 

In a more positive view, It Takes a Man and a Woman received a grade of A from the Cinema Evaluation Board of the Philippines and a G rating by the Movie and Television Review and Classification Board.  

===Accolades===

Despite of poor reviews from critics, the film was still able to earn some nominations and wins from award-giving bodies in the Philippines. These include three   awarded the Box-Office King and Queen to Cruz and Geronimo; as well as the most popular screenwriter to Carmi Raymundo. 

==References==
 
 

 

== External links ==
 
*  
*  
*  

 
 
 
 
 
 
 
 
 