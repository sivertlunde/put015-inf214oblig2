Night of the Sharks
{{Infobox film
| name           = Night of the Sharks
| image	         = Notspos.jpg
| caption        = Original film poster	
| director       = Tonino Ricci
| producer       = Fulvio Lucisano Nino Segurini
| writer         = Tito Carpi Tonio Ricci Christopher Connelly
| music          = Stelvio Cipriani
| cinematography = Giovanni Bergamini
| editing        = Gianfranco Amicucci    
| distributor    = Amanecer Films Belma Cinematografica Cinematográfica Jalisco S.A. Koala Cinematografica
| released       =  
| runtime        = 90 minutes
| country        = Italy/Spain/Mexico
| language       = various
| budget         =
}}
 1988 Italian/Spanish/Mexican international co-production set in Miami and Cancún, Mexico but filmed in the Dominican Republic.  It was directed and co-written by Tonino Ricci.

==Plot==
James Ziegler has spent five years of his life planning the perfect crime, planting covert listening device on the communication devices of influential mega rich businessman Mr Rosentski.  His dream finally comes through when he records a sensitive conversation between Rosentski and the President of the United States; so sensitive that he demands $2 million in diamonds from Rosentski for a disc of the recording. The easy going Mr R pays James for the disc but his Public Relations Enforcer bungles an attempt to retrieve the disc, the diamonds and James who escapes with the disc, the diamonds and his life.

James goes to hide out with his brother David living as a beach bum and shark hunter on the coast outside of Cancún.  David lives with his friend and business partner Paco and has a seagoing neighbour, a man (and woman) eating shark named Cyclops.  Landing in a seaplane near Davids shack, the plane explodes with James surviving long enough to hand over the disc and the diamonds.

Mr Rosentski is displeased with his enforcers further jumping the gun by killing James before the disc and diamonds are recovered. Rosentski correctly surmises that James has gone to hide with his brother David and arranges a meeting with Davids ex-wife, Liz. Mr Rosentski promises he will pay off Lizs considerable business debts as well as reward her if she recovers the disc and diamonds.  

Howerver Mr Rosentskis hot tempered and bruised ego enforcer wants the diamonds for himself and leads an assassination squad against David.  David abhors firearms but has no objections to using edged weapons, booby traps, molotov cocktails and his neighbour Cyclops.

==Cast==
Treat Williams as  David Ziegler   
Antonio Fargas as   Paco    
Carlo Mucari  as   James Ziegler   
Janet Agren  as   Liz Ziegler   
John Steiner  as   Rosentski    
Stelio Candelli  as   Rosentskis Enforcer     
Nina Soldano  as   Juanita    
Salvatore Borghese  as  Garcia     Christopher Connelly  as   Father Mattia   

==External links==
*  
 
 
 
 
 
 
 
 
 

 