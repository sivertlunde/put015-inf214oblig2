Toto in Madrid
{{Infobox film
| name           =Toto in Madrid
| image          = 
| caption        =  Steno
| producer       = Cormoran Films
| writer         = Vittorio Metz Roberto Gianviti
| starring       = Totò Louis de Funès
| music          = Gorni Kramer
| cinematography = Manuel Berenguer  Alvaro Mancori
| editing        = Giuliana Attenni
| distributor    = 
| released       = 6 February 1959 (France)
| runtime        = 95 minutes
| country        = Italy
| awards         = 
| language       = Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Italian Comedy comedy film from 1959, directed by Stefano Vanzina, written by Vittorio Metz, starring Totò and Louis de Funès. The film is known under the titles: "Toto in Madrid" (English title), "Totò a Madrid", "Un coup fumant" (France), "La culpa fue de Eva" (Spain). 

== Plot ==
The penniless painter Toto is commissioned by unknown Spanish fraudsters to mirror the famous masterpiece Maja Desnuda by Francisco Goya, with some details. In fact, these thieves have agreed with a rich billionaire who plans to buy the original masterpiece. When Toto discovers that all that was nothing more than a scam, its too late...

== Cast ==
* Totò: Toto Scorcelletti
* Louis de Funès: Professor Francisco Montiel
* Abbe Lane: Eva
* Mario Carotenuto: Raoul La Spada
* Giacomo Furia: Tobia
* Luna Pilar Gomez Ferrer: Gloria Harrison
* Ricardo Valle: Pablo Segura, the torero
* Enzo Garinei: the lover
* Guido Martufi: Oriundo
* Anna-Maria Marchi: Caterina
* Anna Maestri: Mrs. Treno
* Gianni Partanna : notary
* Anna-Maria Di Giulio: Don Alonzos wife
* Silvia de Vietri: the chambermaid
* Luigi Pavèse: the policeman
* José Guardiola: José
* Francesco Mulé: Don Alonzo

== References ==
 

== External links ==
*  
*   at the Films de France 
 
 
 
 
 
 
 
 


 