Avanthan Manithan
{{Infobox film
| name           = Avanthan Manithan
| image          =
| image_size     =
| caption        =
| director       = A. C. Tirulokchandar
| producer       =
| writer         = Chi. Udaya Shankar
| screenplay     = Cho
| music          = M. S. Viswanathan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Tamil
}}
 1975 Cinema Indian Tamil Tamil film, Cho in lead roles. The film had musical score by M. S. Viswanathan.    The film was a remake of Kannada film Kasturi Nivasa.

==Cast==
*Sivaji Ganesan
*Jayalalithaa
*R. Muthuraman Cho
*Sachu Chandrababu
*M. R. R. Vasu

==Soundtrack==
The music was composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Oonjalukku || T. M. Soundararajan || Kannadasan || 05.38
|- Susheela || Kannadasan || 04.41
|-
| 3 || Ah... Engirundho Oru Kural || Vani Jayaram || Kannadasan || 04.20
|-
| 4 || Aattuviththaal Yaaroruvar || T. M. Soundararajan || Kannadasan || 04.09
|-
| 5 || Manidhan Ninaippadhundu || T. M. Soundararajan || Kannadasan || 04.52
|-
| 6 || Jalitha Vanitha (Oonjalukku) || T. M. Soundararajan || Kannadasan || 05.44
|- Susheela || Kannadasan || 04.24
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 


 