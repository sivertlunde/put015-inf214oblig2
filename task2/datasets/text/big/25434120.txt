Rudranetra
{{Infobox film
| name           = Rudranetra
| image          =
| caption        =
| director       = K. Raghavendra Rao
| producer       =
| writer         =
| starring       = Chiranjeevi, Vijayashanti, Radha (actress)|Radha, Rao Gopal Rao
| music          = Ilaiyaraaja
| cinematography = K.S. Prakash
| editing        = Kotagiri Venkateswara Rao
| distributor    =
| released       = June 16, 1989
| runtime        =
| country        = India Telugu
| budget         =
}} Telugu film directed by K. Raghavendra Rao. The film stars Chiranjeevi, Vijayashanti, Radha (actress)|Radha, and Rao Gopal Rao in important roles. It was dubbed in Hindi as Secret Agent Raja.

==Plot==
Agent Netra(chiru), a Casanova kinda guy works for a private detective agency run by Satyanarayana. He is given a special assignment to track down the underworld activities and their drug dealings. RaoGopalRao is a bigwig in the society, but has another shade as don of the underworld. He uses his elder daughter Rekha to experiment all his drugs. Netra plans to trap his younger daughter Radha and through her tries to reach the villains.

But Netra is drowned by goons, when he tries to reach their den which is hidden under water. Disappointed satyanarayana sends in another agent Vijayasanthi to investigate netras incomplete mission. She lands in that village where netra was drowned in water but finds a look a like of netra(who is actually Netra himself. Netra planned a fake death to cheat all the villains) as a servant at her guest house. He saves her from drowning when she tries to explore the hidden den under water and unwraps his disguise to reveal how he escaped from drowning. They both reach Malaysia trying to reach the roots of the underworld.

RaoGopalRao reveals that his elder daughter rekha is not his own daughter and how he used her for his experiments and he kills her. Radha unable to withstand her fathers cruelty, tries to escape from home but is house arrested and forced into marriage. Netra and vijayasanthi attack the base of villains and destroy it. Radha and Netra unite to end the story.

==Soundtrack==
The soundtrack for the movie is provided by maestro Ilaiyaraaja.

Track listing:

1. Abbabbabba - S. P. Balasubrahmanyam, K. S. Chithra 
2. Andamivvu - S. P. Balasubrahmanyam, K. S. Chithra 
3. Ek do theen - S. P. Balasubrahmanyam, K. S. Chithra 
4. Jettu speedu - S. P. Balasubrahmanyam, K. S. Chithra 
5. Khajuraho - S. P. Balasubrahmanyam, K. S. Chithra 
6. L ante O ante - S. P. Balasubrahmanyam, K. S. Chithra

==External links==
* 

 
 
 
 

 
 