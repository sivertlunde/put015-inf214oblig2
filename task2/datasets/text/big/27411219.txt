Ave Maria (1984 film)
 
{{Infobox film
|name=Ave Maria
|image=1984 Ave Maria.jpg
|caption=Isabelle Pasco
|producer=Irène Silberman
|music=Jorge Arriagada
|cinematography=Dominique Brenguier
|editing=Luc Barnier
|studio= Films Galaxie
|language=French
|released= 
|country=France
|runtime=105 minutes
}}
Ave Maria is a 1984 French drama film directed by Jacques Richard, who co-wrote screenplay with Paul Gégauff.

==Primary cast==
*Anna Karina -  Berthe Granjeux
*Féodor Atkine -  Adolphe Eloi
*Isabelle Pasco -  Ursula
*Pascale Ogier -  Angélique
*Dora Doll -  Constance
*Bernard Freyd -  Mathieu

==External links==
* 
* 

 
 
 

 