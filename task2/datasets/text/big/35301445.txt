Out of the Shadows (film)
{{Infobox film
| name = Out of the Shadows
| image = Out_of_the_Shadows_DVD.jpg
| caption = DVD cover
| runtime = 55 minutes Kevin Sullivan
| producer = Trudy Grant
| writer = Kevin Sullivan
| narrator = Donald Sutherland
| music = James Mark Stewart
| country = Canada
| language = English
| released =  
}}
Out of the Shadows is a 2012 documentary produced by Sullivan Entertainment. The documentary features Dr. Joris Dik as he works toward re-attributing ‘Portrait of an Old Man with a Beard’ as a Rembrandt painting. The results of the process documented in the film were announced on December 2, 2011 as the Rembrandt Research Project classified the ‘Old Man’ as a Rembrandt.  The film is narrated by Donald Sutherland.

==Synopsis==
Art historians and scientists have joined forces, enabling new details about paintings to be exposed. Using cutting edge x-ray techniques, art historians are now able to see beneath the surface on famous pieces of art. The combination of new data and expert knowledge of artists’ methods allow the Rembrandt Research Project and a group of material scientists to make startling new discoveries.
 David Teniers and two medieval religious relics. These studies conducted by esteemed scientists such as Dr. Joris Dik from the University of Delft, Dr. D Peter Siddons from the Brookhaven National Laboratory and Dr. Arthur Woll from Cornell University use innovative and non-invasive x-ray and 3D imaging techniques to achieve a greater understanding of how these incredible works were created. Out of the Shadows reveals how, for the first time, modern science is making it possible to step back in time and trace the hands of some of the world’s greatest artists as they breathed life into their masterpieces one layer of paint at a time.

==References==
 

==External links==
* 

 

 
 
 
 
 

 
 