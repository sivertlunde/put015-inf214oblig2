Trabbi Goes to Hollywood
{{Infobox film
| name           = Trabbi Goes to Hollywood
| image          =Trabbi Goes to Hollywood.jpg
| caption        = 
| director       = Jon Turteltaub
| producer       = Brad Krevoy Steven Stabler
| writer         = Jon Turteltaub David Tausik R. M. London Michelle Johnson Billy Dee Williams Dom DeLuise James Tolkan
| music          = Joey Balin Christopher Franke
| cinematography = Phedon Papamichael
| editing        = Armen Minasian Nancy Richardson
| distributor    = 
| released       = March 16, 1991 (Germany)
| runtime        = 87 minutes
| country        = United States Germany
| awards         = 
| language       = English German
| budget         = 
| preceded_by    =
| followed_by    = 
}}

Trabbi Goes to Hollywood (English title: Driving Me Crazy) is a 1991 US comedy film directed by Jon Turteltaub, starring Thomas Gottschalk, Billy Dee Williams, Dom DeLuise, and James Tolkan.

==Synopsis==
  running on are reunited, rendering the Trabbis use as an escape vehicle obsolete.

stars telling the hope of all east German has after the Berlin Wall fall and the opportunity to reach new relationships with Western companies. The next part of the film focus in Gunther Schmidt (Gottschalk) an East German inventor who development a car who only works with vegetables. Gunther member of a local band during the preparations of his local East German birth town for receive the visit of an American tycoon John Mcready (George Kennedy) he receives a letter from BMW rejecting the project and band companion says to him that like another attempts to offer the idea to all European car manufacturers forget the idea. After the official reception Gunther reaches to Mcready and wonder him if in the United States his revolutionary car idea will reach success and Mcready invite him to go to the U.S. especially at the Los Angeles Automobile Festival and gives him a card if you need his help. With this idea Gunther leaves his hometown and in the moment one of the neighbours gives him a large sausage and she has a relative Ricki (Michelle Johnson) who lives there and if don´t have any trouble if decide to inn. Gunther arrives to the venue of the event but he had trouble with enter and reach to all car manufacturers because they reach and chat with Mr.B (Dom DeLuise) a kind of car genius and says to all that he has develop a new car prototype. With the help of Max ( Billie Dee Williams) the valet who Gunther knows decide together to go with each manufacturers chairman to offer the idea. Desperate for find a new car like he promises, Mr B orders to one of his aide Vince (James Tolkan) to find one and presented it like Mr. B idea. During the search Vince sees the Trabant who Gunther brings from his East Germany hometown parked in the hotel where the auto show held. Driving in a freeway and for the less speed Vince operates a device developed by Gunther and produces that the Trabbi goes fast. So intrigued by the fact Vince with Mr B. decides to search and investigate the Trabbi motor and discovered that he only works with vegetables. Max says to Gunther that his Trabbi was stolen and decide to recover it.In his search he notices that Mr. B had it and summon a media conference where will launch the car like his own. Passing off as the Peugeot chairman with Ricki enters to Mr B mansion and says to all the media that car was his idea and immediately is expelled. Looking the card that John Mcready gave him he decided to visit him in his corporate offices for help and discover him that he wants to install a contaminated factory in his town and John tells that the only way to save his town is that he gives two million dollars money who gave to East Germany authorities to allow build the factory. Decide to recover his car and save his town Gunther with Max drives the car off in the Mr. B mansion and he decides not allow lose an amount of dollars who the manufacturers promises to give him with Vince starts a race chase in Los Angeles streets. Together go with them an American Motor Company executive with interest in the car who decides to give four million dollars for the rights to build the car. In a moment near to a defile Gunther with Max waiting to Ricki for decide the new ways to reach to the fabricants. The American Motors executives chats with them and offers the money but Gunther notices that Vince takes the Trabbi to defile and unsuccessfully Gunther tries to rescue. The executive ask to Mr.B if he can make the car again and starts to cry. In that part Gunther get out of the defile says to the executive that he can build it and with the money save his hometown.

==Cast==
*Thomas Gottschalk as Gunther Schmidt Michelle Johnson as  Ricki
*Billy Dee Williams as  Max
*Dom DeLuise as  Mr. B
*James Tolkan as Vince
*George Kennedy as  John McCrady
*Milton Berle as Hotel Clerk 
*Steve Kanaly as Mr. Goodwyn  
*Richard Moll as Buzz
*Morton Downey, Jr. as Taj  Tommy Tiny Lister as Cubey (credited as Tiny Lister Jr.)
*Andre Rosey Brown as Bluto
*Peter Lupus as GM Boss
*Vlade Divac as Yugo Boss
*Antony Alda as Jack 
*Robert Miano as Omar

==Reception==
Trabbi Goes to Hollywood received a generally poor reception. The German film magazine Cinema evaluates the film as a "ramschiger Rübenrotz" ("junky turnip snot") and summarized the film plots value with the words "Thommy Go Home!". 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 