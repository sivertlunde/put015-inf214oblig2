The Assigned Servant
 
 
{{Infobox film
| name           = The Assigned Servant
| image          = The_Assigned_Servant.jpg
| image_size     = 
| caption        = Production still from the film John Gavin
| producer       = Herbert Finlay Stanley Crick
| writer         = Agnes Gavin
| narrator       =  John Gavin
| music          = 
| cinematography = Herbert Finlay
| editing        = 
| studio = Crick and Finlay
| distributor    = 
| released       = 26 August 1911
| runtime        = 3,000 feet 
| country        = Australia
| language       = Silent film
| budget         = ₤300 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 13.  or £500 
}}
 John and Agnes Gavin and is considered a lost film.

==Plot==
In England, Ralph Frawley is arrested for rabbit poaching and transported to Van Diemens Land as a convict. He is assigned as a servant to a settler and falls in love with the daughter of the house. He marries her in secret but when this is revealed he is sent back to prison to serve the rest of his term. He escapes by a spectacular leap and swims to freedom. He turns to bushranging and robs the mail coach. He is saved by his aboriginal friend during a fight with police. After learning his wife has died he returns to England. 

==Cast== John Gavin
*Alf Scarlett Charles Woods
*Dore Kurtz
*Sid Harrison
*Agnes Gavin

==Production==
Filming took under a month, which over a week spent on location.    During the shoot, two actors injured themselves during a scene where they fought on top of a cliff and fell twenty feet below into the water. The actor Frank Gardiner cut his head falling from a horse during a chase scene, and an actor playing a trooper had four teeth knocked out during a fight. 

Filming took place in the National Park, with Georges River heavily featured. 

==References==
 

==External links==
* 
*  at the National Film and Sound Archive

 

 
 
 
 
 
 


 