A Pleasant Journey
{{Infobox film
| name           = A Pleasant Journey
| image          =Apleasantjourney22lr.jpg
| image_size     = 159px
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach Tom McNamara
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Pathé
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}

A Pleasant Journey is a 1923 silent short comedy film and the tenth Our Gang short subject comedy released. The Our Gang series (later known as "The Little Rascals") was created by Hal Roach in 1922, and continued production until 1944.

==Plot==

The plot revolves around the gang swapping places with a group of runaway boys who are supposed to be taken by train back to San Francisco.  This plot is repeated in the 1932 short, Choo-Choo!.  While aboard the train, the gang wreaks havoc for the other passengers.  Jackie Condon rescues his dog, T-Bone, from the baggage compartment and this causes considerable disturbance with the conductor.  He later changes clothes with a little girl, played by Mary Kornman, and they both get spankings by the adults.  A traveling salesman volunteers to entertain the children with his noisemakers and fireworks.  The gang then parade up and down the train with whistles and kazoos.  They set off the fireworks, release sneezing powder (a gag repeated later in the 1930 short Teachers Pet (1930 film)|Teachers Pet), pass around other practical jokes and mayhem results.  When they finally arrive at San Francisco, the child care worker receives a telegram informing him that he has the wrong children and must take them back.

==Notes==
A Pleasant Journey was somewhat reworked in 1932 as Choo-Choo!

This is Joe Cobbs third appearance, and he only makes a brief cameo appearance in this film, as one of the passengers on the train.

==Cast==

===The Gang===
* Jackie Condon as Jackie
* Mickey Daniels as Mickey Jack Davis as Jack
* Allen Hoskins as Farina
* Ernie Morrison as Ernie

===Additional cast===
* Mary Kornman as Mary
* Joe Cobb as Joe
* Elmo Billings as A runaway orphan
* Doris Oelze as Baby
* Gabe Saienz as A runaway orphan
* Tommy Tucker as Fat kid
* George "Freckles" Warde as Boy throwing apples
* Charles A. Bachman as The police sergeant
* Roy Brooks as The chief of police
* Louise Cabo as A mother William Gillespie as Tilford Gillespie
* Wallace Howe as The welfare physician / The man with the gout
* Mark Jones as The novelty salesman
* Sam Lufkin as The cab driver
* Joseph Morrison as The porter Charles Stevenson as A conductor / A police officer
* Charley Young as A conductor
* George B. French as Train passenger
* Richard Daniels as Train passenger
* Clara Guiol as Train passenger
* Robert F. McGowan as Man encountering Farina

==See also==
* Our Gang filmography

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 


 