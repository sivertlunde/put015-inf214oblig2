Yakyū-kyō no Uta
{{Infobox animanga/Header
| name            = Yakyū-kyō no Uta
| image           = Yakyū-kyō no Uta.jpg.png
| caption         = Cover of the first volume 
| ja_kanji        = 野球狂の詩
| ja_romaji       = 
| genre           = Baseball 
}}
{{Infobox animanga/Print
| type            = manga
| author          = Shinji Mizushima
| publisher       = Kodansha
| demographic     = Shōnen manga|Shōnen
| magazine        = Weekly Shōnen Magazine
| first           = 1972
| last            = 1976
| volumes         = 
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = live film
| director        = Akira Katō
| producer        = Hiromi Higuchi
| writer          = Masayasu Ōehara, Rokurō Kumagaya
| music           = Shin Takada
| studio          = 
| runtime         = 93 minutes
| released        = March 19, 1977
}}
{{Infobox animanga/Video
| type            = anime
| director        = 
| producer        = Kōichi Motohashi
| writer          = 
| music           = Michiaki Watanabe
| studio          = Nippon Animation
| first           = December 23, 1977
| last            = March 26, 1979
}}
{{Infobox animanga/Video
| type            = film
| title           = Yakyū-kyō no Uta: Kita no Ōkami, Minami no Tora
| director        = Eiji Okabe
| producer        = Kōichi Motohashi
| writer          = 
| music           = Taiji Nakamura
| studio          = 
| runtime         = 90 minutes
| released        = September 15, 1979
}}
{{Infobox animanga/Video
| type            = drama
| director        = 
| producer        = Setsurō Wakamatsu
| writer          = Keiji Okutsu
| music           = 
| studio          = Telepack
| network         = Fuji TV
| released        = January 7, 1985
| episodes        = 
| episode_list    = 
}}
{{Infobox animanga/Print
| type            = manga
| title           = Yakyū-kyō no Uta Heisei-hen
| author          = Shinji Mizushima
| publisher       = Kodansha
| demographic     = Shōnen manga|Shōnen
| magazine        = Mister Magazine
| first           = 1997
| last            = 2000
| volumes         = 3
| volume_list     = 
}}
{{Infobox animanga/Print
| type            = manga
| title           = Shin Yakyū-kyō no Uta
| author          = Shinji Mizushima
| publisher       = Kodansha
| demographic     = Shōnen manga|Shōnen Comic Morning
| first           = 2000
| last            = 2005
| volumes         = 11
| volume_list     = 
}}
 
 manga magazine Weekly Shōnen Magazine between 1972 and 1976, and has been adaptated into several spin-off (media)|spin-off manga, a live-action film, an anime television series, an anime film, and a Japanese television drama. In 1973, it received the 4th Kōdansha Literature Culture Award for childrens manga. 

== Media ==

=== Manga ===
The Yakyū-kyō no Uta manga series was written and illustrated by Shinji Mizushima, and originally serialized by Kodansha in Weekly Shōnen Magazine from 1972 to 1976.    It was published into a single tankōbon volume on October 1, 1972, on June 16, 1974, on January 25, 1976, and on January 21, 1979.  Between July 12, 1995 and October 12, 1995, it was published in 13 tankōbon#bunkoban|bunkoban.   A four-Tankōbon#shinsōban|shinsōban version subtitled   was released between November 21, 1997 and June 23, 1998.  
 Comic Morning,  and published on 11 tankōbon between January 23, 2001 and October 21, 2005.  

Four bound volumes were published under Platinum Comics line between June 11, 2003 and July 23, 2003:  ,  ,  , and  .    
 crossover manga between Yakyū-kyō no Uta and Dokaben, another Mizushima manga, was first published in 2005.  On February 8, 2006, it was released by Kodansha in a bound volume under the title  .  Later, on September 30, 2009, a   was published. 

In February 10, 2009, a series entitled  , that follows the story of Yūki Mizuhara, a real-life female baseball player, started to be published. Spawning three bound volumes, it was last published on April 10, 2009 by Kodansha.  

=== Live-action film ===
Akira Katō directed a live-action adaptation that was released on March 19, 1977 on movie theatres. It starred Midori Kinōuchi, was produced by Hiromi Higuchi, written by Masayasu Ōehara and Rokurō Kumagaya, and its score was composed by Shin Takada. 

=== Anime ===
A 25-episode anime television series was created by Nippon Animation, and was broadcast on Fuji Television between December 23, 1977 and March 26, 1979.  An anime film titled   was released in theatres on September 15, 1979.  It is an adaptation of chapters 13 and 14: "Kita no Ōkami, Minami no Tora" Part 1 and Part 2. 

=== TV drama === Yuki Saito as Yūki Mizuhara and Shirō Itō as Tetsugorō Iwata. 

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 