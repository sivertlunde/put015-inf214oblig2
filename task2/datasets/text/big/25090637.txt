Bulldog Drummond's Peril
{{Infobox film
| name           = Bulldog Drummonds Peril
| image          =
| image_size     =
| caption        = James P. Hogan Stuart Walker (associate producer) The Third Round) Stuart Palmer (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Harry Fischbeck
| editing        = Edward Dmytryk
| distributor    = Paramount Pictures
| released       = 17 March 1938
| runtime        =
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 James P. The Third Round.

==Plot==
The intended wedding of Captain Hugh "Bulldog" Drummond to Phyllis Clavering at her villa in Switzerland is stopped short (once again) when someone murders the Swiss policeman who is guarding their wedding presents. The killer makes off with their prize possession, a synthetic diamond, made by a secret process by Professor Bernard Goodman, the father of their good friend Gwen Longworth. A guest, Sir Raymond Blantyre, head of the Metropolitan Diamond Syndicate, disappears at the same time, and Drummond suspects that Sir Raymond, who has the most to lose if Professor Goodman proceeds with his plans to publish his secret process, has something to do with the theft. He leaves Phyllis and chases back to England. Colonel Nielsen, of Scotland Yard, as usual scoffs at Drummonds suspicions and insists that a man as respected as Sir Raymond could not possibly be involved in such a crime. But, as usual, he has Drummond followed just in the event he turns up something unexpected. An explosion that wrecks Goodmans house, and apparently kills him, makes Drummond more positive that the diamond king has again resorted to murder to protect his business. He follows Professor Botulian, a lifelong rival of Goodmans, whom he believes to be involved in the affair. His hunt leads him to a lonely house on the outskirts of London where he finds Goodman a prisoner. Before they can escape, Drummond and Goodman are joined by Tenny (E. E. Clive), his valet, and Phyllis, who are also (as usual) being held by the crooks. Faced by Blantyne and his armed confederates, Drummond begins to fight his way out, but is met by superior forces

===Differences from novel===
Loosely based upon The Third Round (1924) by Herman Cyril McNeile

==Cast==
*John Barrymore as Col. Neilson John Howard as Bulldog Drummond|Capt. Hugh Bulldog Drummond Louise Campbell as Phyllis Clavering Reginald Denny as Algy Longworth
*E.E. Clive as Tenny
*Porter Hall as Dr. Botulian Elizabeth Patterson as Aunt Blanche
*Nydia Westman as Gwen Longworth Michael Brooke as Anthony Greer
*Halliwell Hobbes as Prof. Bernard Goodman Matthew Boulton as Sir Raymond Blantyre
*Zeffie Tilbury as Mrs. Weevens David Clyde as Constable McThane Clyde Cook as Constable Sacker
*Austin Fairman as Roberts
*Gregory Gaye as Raoul
*Pat X. Kerry as 1st Expressman
*David Thursby as 2nd Expressman
*Torben Meyer as Hoffman

==Soundtrack==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 