The Misadventures of Merlin Jones
{{Infobox film
| name           = The Misadventures of Merlin Jones
| image          = Poster of the movie The Misadventures of Merlin Jones.jpg
| image_size     = 
| alt            = 
| caption        =  Robert Stevenson Ron Miller Helen Levitt
| narrator       =  Leon Ames Buddy Baker Edward Colman
| editing        = Cotton Warburton Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $4,000,000 (US/ Canada) 
}}

The Misadventures of Merlin Jones is a 1964 Walt Disney production starring Tommy Kirk and Annette Funicello.  Kirk plays a college student who experiments with mind-reading and hypnotism, leading to run-ins with a local judge.  Funicello plays his girlfriend (and sings the films title song written by brothers Robert and Richard Sherman).

This film led to a 1965 sequel called The Monkeys Uncle.

==Plot== Midvale College student Merlin Jones (Tommy Kirk), who is always involved with mind experiments, designs a helmet that connects to an electroencephalographic tape that records mental activity. He is brought before Judge Holmby (Leon Ames) for wearing the helmet while driving and his license is suspended. Merlin returns to the lab and discovers accidentally that his new invention enables him to read minds. Judge Holmby visits the diner where Merlin works part-time, and Merlin, through his newly found powers, learns that the judge is planning a crime. After informing the police, he is disregarded as a crackpot. Merlin and Jennifer (Annette Funicello), his girlfriend, break into Judge Holmbys house looking for something to prove Holmbys criminal intent but are arrested by the police. Holmby then confesses that he is the crime book author, "Lex Fortis," and asks that this identity be kept confidential. Merlins next experiment uses hypnotism. After hypnotizing Stanley, Midvales lab chimp, into standing up for himself against Norman (Norm Grabowski) - the bully student in charge of caring for Stanley, Merlin gets into a fight with Norman, and is brought before Judge Holmby again. Intrigued by Merlins experiments, the judge asks for Merlins help in constructing a mystery plot for his next book. Working on the premise that no honest person can be made to do anything they wouldn’t do otherwise – especially commit a crime – Merlin hypnotizes Holmby and instructs him to kidnap Stanley. Shocked when the judge actually commits the crime, Merlin and Jennifer return the chimp, but are charged for the theft themselves. The judge sentences Merlin to jail, completely unaware of his own role in the crime. Livid at the injustice, Jennifer persuades Holmby of his own guilt, and the good judge admits that there might be a little dishonesty in everybody.

==Production notes== Helen Levitt, two writers who were Hollywood blacklist|blacklisted. 

To date Disney has not officially stated whether or not this film was actually two episodes of a planned television series, however, this has long been suspected to be the case,  with at least one critic, Eugene Archer, of The New York Times, writing upon its release:  
"Movies made for television are commonplace these days, but the idea of screening television shows in movie theaters is still farfetched. Who is expected to spend the $2? Strange as it sounds, this seems to be the explanation behind Walt Disneys latest hit, "The Misadventures of Merlin Jones." It is a pastiche of two separate stories with the same set of characters, each running less than an hour (leaving time for commercials), stitched together in the middle and released yesterday in neighborhood theaters."  
 
Filming took place in early 1963. Filmland Events: Miss Pickford, Lloyd Will Receive Honor.
Los Angeles Times (1923-Current File)   03 Jan 1963: C7.  In March of that year it was reported NBC were so pleased with the results they wanted more Merlin Jones adventures.  It appears that Disney then decided to release the movie theatrically.

==Reception==
===Critical===
The Chicago Tribue called it "a kooky comedy of the type young people will enjoy thoroughly... good natured nionsense."  "Disney Film Good Fun for Family: "THE MISADVENTURES OF MERLIN JONES"
TINEE, MAE. Chicago Tribune (1963-Current file)   14 Feb 1964: b16.  
===Box Office===
Although critics were not impressed, audiences seemed to love it, as the film grossed over $4 million in North America, surprising even Disney. Disney: Self-Perpetuating Money Machine: Mary Poppins Works Her Magic for Happy Shareowners a sequel in 1965. 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 