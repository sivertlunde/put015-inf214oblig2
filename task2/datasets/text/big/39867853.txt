Joe + Belle
{{Infobox film
| name           = Joe + Belle
| image          = JOE + BELLE heart.jpg
| alt            = 
| caption        = When love is not madness, it is not love
| director       = Veronica Kedar
| producer       = Veronica Kedar Amir Fishman
| screenplay     = Veronica Kedar Stav J. Davis
| starring       = Veronica Kedar Sivan Levy Romi Aboulafia
| cinematography = Ron Haimov
| editing        = Daniel Keysary
| studio         = 
| music          = Daphna Keenan
| distributor    = Wolfe
| released       =  
| runtime        = 80 minutes
| budget         = $30,000
| country        = Israel
| language       = Hebrew English French
}}
 Romantic Comedy film directed by Veronica Kedar about a drug dealer called Joe and a suicidal psychopath called Belle.   

==Plot==
Joe (Veronica Kedar) is a drug dealer who was born in New York but lives in Israel. She travels from Bangkok to Tel Aviv smuggling drugs. Joe delivers the drugs to Abigail (Romi Aboulafia) who tells her to meet at the club Dark Eighties later that night in order to sell the drugs. Belle (Sivan Levy) a patient staying at Tel Aviv Psychiatric Hospital is released into the care of her mother (Florence Bloch), yet after learning that her parents got rid of her pet Fred she runs away. She climbs onto the roof of a building and strips off her clothes to commit suicide. Before jumping she notices an open window and decides to go inside. After getting close to the window she finds it is in fact closed but she smashes it and goes inside anyway.
 smitten by Joe and talks her into allowing her to go to Dark Eighties together, although she still carries the razor around with her. At the club Joe tries to leave Belle but she follows her home again. Once back home Joe locks Belle outside, but she knocks on the door continuously until Joe lets her in. When Joe goes out for cigarettes Belle looks around the kitchen for food but finds a gun hidden inside the microwave which she trades for her razor and then goes to sleep on the couch. Joes ex-boyfriend Matan (Yotam Ishay) arrives at the apartment and mistakes the sleeping Belle for Joe. Belle wakes up when Matan kisses her feet and she pulls out the gun, he tries to talk her into giving him the gun but she just shoots him instead. Joe arrives to find Matan on the ground and she takes the gun from Belle, but when Matan suddenly wakes up she shoots him in shock, killing him. Abigail, who is waiting for her boyfriend Matan to return home, thinks he may be cheating on her with Joe and leaves a threatening voice mail on his answer machine.
 sexually propositions a man with a car who follows her into an alley where she knocks him out and steals his car keys. Discovering that neither of the girls can drive they leave the car and head back to the apartment. They wrap the body in blankets and then Joe calls Abigail and asks to borrow her van. Initially agreeing to help move the body, Abigail becomes distressed when she realizes that it is Matan wrapped in the blankets. After dumping the body in a river Joe and Belle talk about ex lovers and Joe finds out Belle is a Lesbian. The three girls then go to a nearby club and Abigail leaves to phone corrupt police officer Tzedek (Raanan Hefetz) and tells him Joe and Belle killed Matan. Joe and Belle panic when they see flashing police lights outside, slip out of the club and hide in a local hotel room. After confirming their feelings for each other they begin to kiss when police officer Assi (Nitai Gvirtz) knocks on their door. Initially investigating another crime as he is about to leave he gets notified of Matans murder over the police radio and Belle hits him over the head with the hotel phone and handcuffs him to the bathtub.
 
Joe and Belle get on a bus and phone Abigail to let her know they are going to Eilat. Abigail, Tzedek and female police officer Shlomtsi (Noa Provisor) head to Eilat to arrest Joe and Belle. After the phone call Joe becomes suspicious of Abigail and they get off the bus and head to Sderot, a city that is an ongoing target of Qassam rocket attacks. After playfully running around Sderot a warning siren for qassam rocket attacks sounds and they hide out in a public bathroom. Whilst hiding out they have sex for the first time. Afterwards they go to a cafe to eat and they meet Yoni (Shalev Gelber) who is trying to buy drugs. Joe decides to quit being a drug dealer and gives all the drugs she has to Yoni for free. Shlomtsi and Tzedek who are still waiting in Eilat begin to suspect Abigail. They arrest her for the murder based on the evidence of blood in her van, her sheets were used to wrap the body and the threatening answer machine message. Joe and Belle rent an apartment and go to a local open mic night were Joe sings a song for Belle. The film ends with Joe and Belle, who are no longer being chased by police for murder, happily living in Sderot together, despite the constant rocket attacks.

==Cast==
* Veronica Kedar as Joe
* Sivan Levy as Belle
* Romi Aboulafia as Abigail
* Yotam Ishay as Matan
* Raanan Hefetz as Tzedek
* Noa Provisor as Shlomtsi
* Florence Bloch as Belles Mother
* Shalev Gelber as Yoni
* Nitai Gvirtz as Assi

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 