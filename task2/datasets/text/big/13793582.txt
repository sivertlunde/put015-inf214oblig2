An Inspector Calls (1954 film)
{{Infobox film
| name           = An Inspector Calls
| image          = An Inspector Calls (1954 film) poster.jpg
| image_size     =
| caption        = Theatrical release poster
| writer         = Desmond Davis (screenplay)  J.B. Priestley (play) Jane Wenham Brian Worth Eileen Moore
| director       = Guy Hamilton
| producer       = A. D. Peters
| cinematography = Edward Scaife  (as Ted Scaife)
| editing        = Alan Osbiston
| country        = United Kingdom
| distributor    = British Lion Film Corporation  Associated Artists Productions
| released       = 16 March 1954  (London)  25 November 1954  (USA) 
| runtime        = 80 minutes
| music          = Francis Chagrin
| language       = English
}} play of Jane Wenham Brian Worth.

==Plot==
Set in 1912, a dinner party held by the upper class Birling family is interrupted by Police Inspector Poole, investigating the suicide of a lower class girl Eva Smith whose death is linked to each family member.  In the original play, the Inspectors name was Inspector Goole. 

==Cast==
 
*Alastair Sim as Inspector Poole (Known as Inspector Goole in the book) Jane Wenham as Eva Smith Brian Worth as Gerald Croft
*Eileen Moore as Sheila Birling
*Olga Lindo as Sybil Birling Arthur Young as Arthur Birling
*Bryan Forbes as Eric Birling
*Norman Bird as Foreman Jones-Collins
*Charles Saynor as Police Sergeant Arnold Ransom John Welsh as Mr. Timmon: Hat Sales Manager  
*Barbara Everest as Mrs. Lefson: Charity Committee Woman   George Woodbridge as Stanley: Fish & Chips Shop Owner George Cole as conductor on tram
 

==Production==
An Inspector Calls was filmed at Shepperton Studios, Shepperton, Surrey, England, under the auspices of the Watergate Productions Ltd.

Although the play never shows Eva Smith, the film opens in flashbacks that show each member of the familys involvement in Smiths life. The relationships between Eva and Gerald, and later, Eric, are smoothed over in accordance to the censorship of the day. Still, enough elements are retained to give the viewer a good idea of the depth of involvements.

In the play, Eva is first sacked for being involved in a strike; in the film, she is simply sacked for suggesting that the wages requested were necessary to live on. Similarly, in the play, Sheila is trying on a dress when the incident with Eva occurs in the shop; in the film, the incident is over a hat.

The film makes Inspector Poole out to be more explicitly "supernatural" than does the play. In the play, he is ushered in by the maid, while in the film he simply appears suddenly in the dining room as if from nowhere, accompanied by an ominous chord in the background music. In the middle of the film, Poole inspects his pocket watch and asks Eric to enter the room. Poole states he has just heard Eric come through the door; but eerily he states this before Eric does come through the door. Likewise, at the end, when the family receives the phone call that the local police are on their way to question them, Poole is supposedly in the study, but when the family checks to see if he is there, they find an empty chair.

==Reception==
C. A. Lejeune, film critic of The Observer, recommended the film; despite its lack of technical polish, its slow pace and often trite dialogue, she found it thought-provoking. 

== References ==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 