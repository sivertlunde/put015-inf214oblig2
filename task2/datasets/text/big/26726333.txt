Searchers 2.0
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Searchers 2.0
| image          = Searchers 2.0 cover.jpg
| caption        =
| director       = Alex Cox
| producer       = Jon Davison
| writer         = Alex Cox
| narrator       =
| starring       = Del Zamora Ed Pansullo Jaclyn Jonet Sy Richardson
| music          = Dan Wool
| cinematography = Stephen Fierberg
| editing        = Alex Cox
| distributor    =
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $180,000
| gross          =
}} road film Straight to Hell, 20 years prior. 

==Plot==
The story follows two out of work actors, Mel and Fred. Mel works part-time as a day laborer. They learn that screenwriter Fritz Frobisher, who had violently abused the two actors with a whip during a childhood acting job, will be appearing at a screening of one of his films that will take place in Monument Valley. The two decide to travel from Los Angeles to the screening, but neither owns a car, so they convince Mels daughter, Delilah, to drive them on their revenge journey.

==Cast==
* Del Zamora as Mel Torres
* Ed Pansullo as Fred Fletcher
* Leonard Maltin as Film critic
* Jaclyn Jonet as Delilah Torres
* Sy Richardson as Fritz Frobisher
* Cy Carter as Director

==Production==
Portions of the film were shot in the  ) 

==References==
 

==External links==
*  
*    

 

 
 
 
 