Old Overland Trail
{{Infobox film
| name           = Old Overland Trail
| image          = Old Overland Trail poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = William Witney
| producer       = Edward J. White 
| writer         = Milton Raison 
| starring       = Rex Allen Slim Pickens Roy Barcroft Leonard Nimoy Virginia Hall Gil Herman
| music          = R. Dale Butts 
| cinematography = John MacBurnie 
| editing        = Harold Minter 
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Western film directed by William Witney and written by Milton Raison. The film stars Rex Allen, Slim Pickens, Roy Barcroft, Leonard Nimoy, Virginia Hall and Gil Herman. Thee film was released on February 25, 1953, by Republic Pictures.  
 
==Plot==
 

== Cast ==
*Rex Allen as Rex Allen
*Slim Pickens as Slim Pickens
*Roy Barcroft as John Anchor
*Leonard Nimoy as Chief Black Hawk
*Virginia Hall as Mary Peterson
*Gil Herman as Jim Allen
*Wade Crosby as Draftsman
*Zon Murray as Mack
*Harry Harvey, Sr. as Storekeeper
*The Republic Rhythm Riders as Musicians 
*Koko as Rexs Horse

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 