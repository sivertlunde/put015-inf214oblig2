Love Life (2006 film)
{{Infobox film
| name           = Love Life
| image          = Love-life-by-damion-.jpg
| caption        = Original film poster
| director       = Damion Dietz
| screenplay     = 
| story          = 
| starring       = 
| producer       = Damion Dietz	
| music          = Jeffery Alan Jones	
| cinematography = Peter Hawkins
| editing        = Michael D. Akers
| distributor    = 
| released       = 2006
| runtime        = 72 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
}} gay man and a bisexual lesbian and how complicated it can get with arrival of other suiters. The film premiered at the Frameline Film Festival in San Francisco and was selected as the benefit film for the Human Rights Campaign.

==Synopsis==
Joe (Stephan D. Gill), a closeted gay former pro-athlete is in a "marriage of convenience" with Mary (Stephanie Kirchen), college friend, a bisexual woman who has married him to please her mother and not be cut off from finances and inheritance. But there is also an agreement that each is free to have sex partners on the side. Mary isnt really that interested in sex, while Joe cruises parks for other guys. Even though shes not supposed to be, Mary is jealous and resentful of the men Joe picks up, but mostly she is afraid that he might someday fall in love with one of them and decide to end their sham marriage.

Things get edgy when Joe is attracted to Thomas (Keith Bearden), a handsome landscaper who came to give an estimate. Joe gets mad at Mary because she followed him in the park, he storms out, checks into a gay-owned motel and calls Thomas. Meanwhile Mary is also telling the same story over wine to her college friend, Aura (Jill Kocalis). The film cuts the two love scenes together - one between the two men (Joe and Thomas) and one between two women (Mary and Aura). 

==Cast==
*Stephan D. Gill as Joe
*Stephanie Kirchen as Mary
*Keith Bearden as Thomas
*Jill Kocalis as Aura
*Ronnie Kerr as Stranger in park

==References==
 

==External links==
* 
 

 
 
 
 
 

 