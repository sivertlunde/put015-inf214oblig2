White Water Fury
{{Infobox film
| name           = White Water Fury
| image          = White Water Fury.jpg
| caption        = DVD cover
| director       = Jon Lindström
| producer       = Lennart Dunér
| writer         = Mats Gustavsson Rita Holst Jon Lindström
| starring       = Emil Forselius Rafael Edholm Alexander Skarsgård
| music          = Patrik Frisk
| cinematography = Jens Fischer	 	
| editing        = Darek Hodor
| studio         = Cinetofon
| distributor    = Scanbox
| released       =  
| runtime        = 
| country        = Sweden
| language       = Swedish
| budget         =
| gross          = 
}}
White Water Fury (  film directed by Jon Lindström and starring Emil Forselius, Rafael Edholm and Alexander Skarsgård.

White Water Fury is about four guys who go on a kayaking trip. Later on in the journey they meet the two sisters: Marie and Susanne. They decide to camp together and after a wild night the youngest sister Susanne gone. The guys go back to their everyday lives while Marie gets more and more nervous about where her sister has gone. The canoe that belonged to the sisters is found crushed on the river, a short distance from the campsite. Järngänget, as the guys call themselves, become more nervous and starts to question each others stories as to what really happened that fateful night. 

==Cast==
*Emil Forselius as Lukas
*Rafael Edholm as Simon
*Alexander Skarsgård as Anders
*Peter Lorentzon as John
*Yaba Holst as Marie
*Josephine Bornebusch as Susanne
*Per Oscarsson as Åke
*Marika Lagercrantz as Lindberg
*Agneta Ekmanner as mother
*Görel Crona as Stina
*Bill Skarsgård as Klasse
*Marie Ahl as Astrid
*Thomas Oredsson as Harald
*Göran Forsmark as municipal chairman

==External links==
* 
* 

 
 
 
 


 
 