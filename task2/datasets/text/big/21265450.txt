Teresa's Tattoo
 
{{Infobox Film |
 | name      = Teresas Tattoo
 |image=DVD cover of the movie Teresas Tattoo.jpg
 | director  = Julie Cypher John E. Vohlers
 | producer  = Lisa M. Hansen  Philip McKeon  Marc Rocco
 | writer    = Georgie Huntington Marc Cushman (additional dialogue)
 | cinematographers  = Sven Kirsten
 | starring  = C. Thomas Howell Lou Diamond Phillips Melissa Etheridge k.d. lang Kiefer Sutherland
 | music = Andrew Keresztes
 | distributor = CineTel Films
 | released  = March 1994
 | runtime  = 88 minutes
 | country  = United States English 
}}

Teresas Tattoo is a 1994 action–comedy–crime film directed by Julie Cypher and John E. Vohlers. The film is also known as Natural Selection. The film stars C. Thomas Howell, Lou Diamond Phillips, Melissa Etheridge, who also performed songs for the film, k.d. lang, and Kiefer Sutherland. It was filmed in Los Angeles, California, USA.

Teresas Tattoo was produced by CineTel Films, Trimark Pictures, and Yankee Entertainment Group Inc. It was distributed by Trimark Pictures.

==Plot==
Mathematician Teresa just wanted to study during the College spring break. But her friends, who wanted her to live a little, dragged her out to parties. The next thing she knows, she has been drugged, kidnapped, made a redhead, tattooed, and is wearing leather! Its the old switcheroo in this action comedy that follows the exploits of desperate extortionists. Gloria is a fluffhead with a Chinese dragon tattooed upon her chest. She wears lovely holograph earrings that just happen to contain classified detail of the U.S. space program. She is taken hostage by the bumbling extortionists and their leader Carl, former head of a freezer treat company. Unfortunately for them, Gloria accidentally drowns in their pool when she tangles with a beach ball. Now the crooks must find a lookalike for Gloria. They find her in Teresa, a college girl with a talent for mathematics. She is captured and tattooed. She soon escapes leading the crooks on a merry chase. Joining in the hunt for Teresa is an FBI agent and her new boyfriend.  http://www.allmovie.com/cg/avg.dll
 

==Availability==

The movie was released on VHS in the U.S. by Vidmark Entertainment and in Canada by Malofilm Video. The movie has been released on DVD in the UK, but as of December 26, 2009, Lions Gate has not yet announced any plans to release the movie onto a Region 1 DVD, most likely because of music copyrights.

==Cast==
*Matt Adler as Titus
*C. Thomas Howell as Carl
*Nancy McKeon as Sara
*Lou Diamond Phillips as Wheeler
*Casey Siemaszko as Michael
*Brian Davila as Elvis
*Jonathan Silverman as Rick
*Melissa Etheridge as the hooker
*Adrienne Shelly as Teresa / Gloria
*Diedrich Bader as Higgins Anthony Clark as Mooney
*Eric Gilliland as the police officer
*Michael Dryden as the hooker with bird
*Tippi Hedren as Evelyn Hill
*k.d. lang as Michelle
*Nanette Fabray as Martha Mae
*Kiefer Sutherland as road block officer (un-credited)
*Sean Astin as Step-Brother (un-credited)

==Production crew==
*Lisa M. Hansen (producer)
*Philip McKeon (producer)
*Paul Hertzberg (executive producer)
*Marc Rocco (executive producer)
*Georgie Huntington (associate producer)
*Catalaine Knell (co-producer)
*Donald C. McKeon (co-producer)
*Russell D. Markkowitz (line producer)
*Nancy Gayhart  (post-production coordinator)

==Soundtrack==
*"2001" Written and Performed by Melissa Etheridge
*"I Really Must Be Going" Written and Performed by Melissa Etheridge
*"All American Girl" Written and Performed by Melissa Etheridge
*"Do It For The Rush" Written and Performed by Melissa Etheridge
*"Save Myself" Written by Melissa Etheridge Performed by Mare Winningham
*"No Strings Attached" Written by Simone Lazer and Audrey Koz Performed by Betty Ball
*"When Youre Near" Written and Performed by David Adjian
*"Pool Cue Music" Written and Performed by Richard Friedman 
*"All Night Long" Written by Mark Gast Performed by Raging Storm
*"Betrayal Of Kings" Written by Mark Gast Performed by Salems Wych
*"Lover Lay Down" Written and Performed by K.O.
*"Alah" Written and Performed by Andrew Kereazies
*"I Feel You" Written and Performed by Andrew Kereazies
*"Silver Bullet" Written by Leigh Lawson & Jack Marsh Performed by Leigh Lawson
*"Coming Down On Me" Written by Leigh Lawson & Pete Sadony Performed by Leigh Lawson

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 