Magnum Force
 
{{Infobox film
| name           = Magnum Force
| image          = Magnum Force.jpg
| caption        = Theatrical film poster by Bill Gold
| director       = Ted Post Robert Daley
| based on       = Characters created by Harry Julian Fink R.M. Fink
| story          = John Milius
| screenplay     = John Milius Michael Cimino
| starring       = Clint Eastwood Hal Holbrook Mitchell Ryan David Soul Felton Perry Robert Urich Tim Matheson
| music          = Lalo Schifrin Frank Stanley
| editing        = Ferris Webster
| studio         = The Malpaso Company
| distributor    = Warner Bros.
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $39,768,000 
}} Harry Callahan after the 1971 film Dirty Harry. Ted Post, who also directed Eastwood in the television series Rawhide (TV series)|Rawhide and the feature film Hang Em High, directed the film, the second in the Dirty Harry (film series)|Dirty Harry series. The screenplay was written by John Milius (who provided an uncredited rewrite for the original film) and Michael Cimino. This film features early appearances by David Soul, Tim Matheson and Robert Urich. At 124 minutes, it is also the longest Dirty Harry film.

==Plot==
  SFPD motorcycle cop stops Ricca’s limo for a minor traffic violation. Suddenly, the patrolman pulls his service revolver (a Colt Python .357 Magnum), shoots all four men in the car, then rides away.

Inspector Harry Callahan (Clint Eastwood) and his partner Earlington "Early" Smith (Felton Perry) visit the crime scene. Callahans superior, Lieutenant Neil Briggs (Hal Holbrook) views Callahan and his tactics as reckless and dangerous. The dislike is mutual; Callahan mocks Briggs, saying "A mans got to know his limitations", after Briggs reveals to him his pride on not ever having to draw his gun in the line of duty. 
 Airborne Ranger and Special Forces member and that the others are as good or better shots than himself.  The young officers zeal and marksmanship impress Callahan.
 suicidal after Christine White).

After hearing McCoys rant, Callahan visits Carol and finds out from her that McCoy was playing Russian Roulette only the night before. In the meantime, Carol tries to seduce Callahan, but her kids and Earlys call for backup during his undercover operation break the mood.  Back at his apartment building, Callahan meets a girl called Sunny (Adele Yoshioka) who immediately asks "What does a girl have to do to go to bed with you?", to which Harry replies, "Try knocking on the door."  A bit later in the evening, Sunny knocks on Harrys door and they spend the night together, though their tryst is briefly interrupted when Lt. Briggs summons him to the city morgue to view the bodies of more victims. At the morgue, Briggs announces Callahan and his partner are back on homicide detail.

A motorcycle cop murders drug kingpin Lou Guzman and associates using a Colt Python equipped with a suppressor.  However, Guzman is under surveillance across the street by Callahans old partner, Frank DeGeorgio (John Mitchum), who sees McCoy dump his bike outside Guzmans apartment complex just before he witnesses Guzmans murder.  The assassin motorcycle cop then encounters McCoy in the garage of Guzmans building and kills him to eliminate a potential witness. Meanwhile, Harry presents his suspicions about McCoy to Briggs, who informs him of McCoys death.  Later at a combat pistol championship where Davis becomes "the new pistol champ", a puzzled DeGeorgio tells Callahan that Davis was the first police officer to arrive after the murder of Guzman and McCoy.

Davis promptness at the crime scene raises Callahans suspicions of the rookie cops. During the shooting competition with the rookies, Callahan borrows Davis Colt Python and purposely embeds a slug in a range wall. Later at night he retrieves the slug, and ballistics then reveal that the slug matches those found at the crime scene involving Guzman and McCoy. Harry begins to suspect that a secret death squad within the department is responsible for the murders.

Examining the slug while Harry refuses to reveal its source, Briggs ignores Callahans suspicions and insists that mob killer Frank Palancio is behind the deaths. When Briggs obtains a warrant for Palancios arrest and tells Harry to lead the arrest team, Callahan requests two of the four rookies, Davis and Sweet, as his backup. Palancio and his gang are tipped off shortly before the police raid via a phone call and told that killers dressed as police officers will hit them in two minutes. Palancio kills officer Sweet during the resulting shootout with a shotgun. During the prolonged arrest operation, Palancio and his men are also killed.

After the raid, the three remaining renegade cops, sitting on their bikes, confront Callahan in his garage complex. When Harry tells them theyve killed a dozen people and asks what theyre going to do next week, Davis cold-bloodedly replies, "Kill a dozen more". They present Callahan with a veiled ultimatum to join their organization "Either youre for us or youre against us"; he responds, "I’m afraid youve misjudged me". Without saying a word, they drive away on their motorcycles. While checking his mailbox, Harry discovers a bomb left by the vigilantes in case he refused their offer. He then inspects his apartment for further traps just before saving Sunnys life by preventing her from opening his mailbox. Callahan defuses the bomb, but a second bomb at Smiths home kills his partner Early as Harry tries to warn him.

Callahan calls Briggs and tells him about the bombs. When Briggs arrives from City Hall, Harry shows him the bomb that was in his mailbox. Briggs wants to get the bomb back to headquarters and asks Callahan to drive. In the car, Lt. Briggs examines the mailbomb and determines that it can be detonated by a mailbox key or a timer. He then directs Harry on the freeway and draws his revolver and forces the surprised inspector to disarm. Briggs then reveals himself as the leader of the death squad. He cites the traditions of frontier justice and summary executions, and says, “You’re a great cop, Harry, you had a chance to join the team but you’d rather stick with the system” Callahan responds, "Briggs, I hate the goddamn system, but until someone comes along with some changes that make sense Ill stick with it". Unimpressed, Briggs tells Callahan "Youre about to become extinct. Get off at the next exit."
 old aircraft between ships, the two run out of deck space. Callahan is able to dump his bike after a jump and avoid the decks end, but Davis falls to his death.

Harry makes his way back to the car, but a bleeding Briggs gets the drop on Harry. Rather than shoot Callahan, Briggs threatens to prosecute him for killing all three vigilante police officers. As the inspector is forced to exit the vehicle, he surreptitiously activates the timer on the mail bomb and tosses it in the back seat.  As Briggs slowly drives away, the car explodes, killing him. The final scene is a close-up of Callahans face in front of the raging inferno as he comments, "Mans got to know his limitations", before he walks away.

===Deleted scenes===
Cut from the final film were two scenes that explain why Harry grows to suspect Astrachan, Davis, Grimes, and Sweet of the killings of Charlie McCoy, Guzman, Ricca, the pimp, and all of the other criminals. 
 
*The first scene cut takes place after Davis and Harry watch McCoys funeral flight take off and before the combat pistol championship; after the flight Harry and Davis drive from the airport to a bowling alley for a few drinks; a black youth is suddenly chased outside and assaulted by four toughs; Davis attacks the toughs while Harry dispatches one with his beer mug. After subduing the robbers, Davis harangues a group of eyewitnesses for letting such crimes take place. Harry witnesses Daviss harangue and sees in it his own approach to crime fighting, albeit far more severe. 

*Later, after examining the bullet from Daviss gun at the combat pistol championship range, Harry checks on old issues of a police magazine. He finds articles condemning the revolving door justice allowed by liberal politics - and these articles are authored by the four rookie cops.

==Cast== Harry Callahan  
*Hal Holbrook as SFPD Homicide Lt. Neil Briggs
*David Soul as SFPD Traffic Officer John Davis
*Tim Matheson as SFPD Traffic Officer Phil Sweet
*Kip Niven as SFPD Traffic Officer Red Astrachan
*Robert Urich as SFPD Traffic Officer Mike Grimes
*Felton Perry as SFPD Stakeout Inspector Earlington "Early" Smith  
*Mitch Ryan as SFPD Traffic Officer Charlie McCoy 
*Margaret Avery as the Prostitute
*Bob McClurg as the Cab Driver
*John Mitchum as SFPD Stakeout Inspector Frank DiGiorgio  
*Albert Popwell as the Pimp, J.J. Wilson
*Richard Devon as Carmine Ricca Christine White as Carol McCoy Tony Giorgio as Frank Palancio ring
*Jack Kosslyn as Walter
*Bob March as Estabrook
*Adele Yoshioka as Sunny

==Production==

===Development===
Writer John Milius came up with a storyline in which a group of rogue young officers in the San Francisco Police Force systematically exterminate the citys worst criminals, conveying the idea that there are even worse rogue cops than Dirty Harry. McGilligan (1999), p.233  Clint Eastwood specifically wanted the story to show that despite the 1971 films perceived view of Inspector Callahan, Harry was not a complete vigilante. David Soul, Tim Matheson, Robert Urich and Kip Niven were cast as the young vigilante cops. McGilligan (1999), p.234  Milius was a gun aficionado and political conservative and the film would extensively feature gun shooting in practice, competition, and on the job.  Given this strong theme in the film, the title was soon changed from Vigilance to Magnum Force in deference to the .44 Magnum that Harry liked to use. Milius thought it was important to remind the audiences of the original film by incorporating the line "Do ya feel lucky?" repeated in the opening credits. 

With Milius committed to filming Dillinger (1973 film)|Dillinger, Michael Cimino was later hired to revise the script, overseen by Ted Post, who was to direct. According to Milius, his script did not contain any of the action sequences (the car chase scene and duel on the aircraft carriers) at the end of the film. His was a "simple script". John Milius commentary on Magnum Force Deluxe Edition DVD  The addition of the character Sunny was done at the suggestion of Eastwood, who reportedly received letters from women asking for "a female to hit on Harry" (not the other way around). 

===Filming===
Frank Stanley was hired as cinematographer and Lalo Schifrin once again conducted the score and filming commenced in late April 1973.  During filming Eastwood encountered numerous disputes with Post over who was calling the shots in directing the film, and Eastwood failed to authorize two important scenes directed by Post in the film because of time and expenses; one of them was at the climax to the film with a long shot of Eastwood on his motorcycle as he confronts the rogue cops. McGilligan (1999), p.235  As with many of his films, Eastwood was intent on shooting it as smoothly as possible, often refusing to do retakes over certain scenes.  Post later remarked: "A lot of the things he said were based on pure, selfish ignorance, and showed that he was the man who controlled the power. By Magnum Force Clints ego began applying for statehood".  Post remained bitter with Eastwood for many years and claims disagreements over the filming affected his career afterwards. McGilligan (1999), p.236  According to director of photography Rexford Metz, "Eastwood would not take the time to perfect a situation. If youve got seventy percent of a shot worked out, thats sufficient for him, because he knows his audience will accept it." 

==Effect==
The film would launch a number of careers, including David Soul (Starsky & Hutch television series), Robert Urich (S.W.A.T., Vega$ and Spenser for Hire) and Tim Matheson (Animal House and Fletch (film)|Fletch). Future Threes Company TV star Suzanne Somers can be seen as the topless blonde at the mobsters pool party.

==Controversy== Urban Legend (1998).
According to scriptwriter John Milius, this drain cleaner scene was never meant to be filmed, but was only mentioned in his original script. 

==Release==

===Box office performance=== first film sixth highest grossing film of 1973.

===Reception===
New York Times critics such as Nora Sayre criticized the conflicting moral themes of the film and Frank Rich believed it "was the same old stuff".  Pauline Kael, a harsh critic of Eastwood for many years mocked his performance as Dirty Harry, commenting that, "He isnt an actor, so one could hardly call him a bad actor. Hed have to do something before we could consider him bad at it. And acting isnt required of him in Magnum Force".  Rotten Tomatoes sampled 20 reviewers and judged 80% of the reviews to be positive.   

== References ==
;Notes
 

;Bibliography
* 
* 

==External links==
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 