The Millionairess
 
 
 
{{Infobox film
| name           = The Millionairess
| image          = The Millionairess poster.jpg
| caption        = original film poster
| director       = Anthony Asquith
| producer       = Dimitri De Grunwald Pierre Rouve
| writer         = George Bernard Shaw Riccardo Aragno Wolf Mankowitz
| starring       = Sophia Loren Peter Sellers Alastair Sim
| music          = Georges Van Parys
| cinematography = Jack Hildyard
| editing        = Anthony Harvey
| distributor    = 20th Century Fox
| released       = 18 October 1960
| runtime        = 90 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
 1936 play of the same name.

==Synopsis==
By the terms of her late fathers will, spoiled London heiress Epifania Parerga, the richest woman in the world, cannot marry unless her prospective husband is able to turn £500 into £15,000 within a three-month period. When Epifania becomes smitten with Alastair, a muscular tennis player, she rigs the contest by giving him £500 in stock and then buying it back for £15,000. Alastair is unable to live with the domineering Epifania, however, and leaves her for the more domestic Polly Smith.

Contemplating suicide, Epifania melodramatically plunges into the Thames, and when Dr. Ahmed el Kabir, a self-effacing, selfless Indian physician who runs an inadequately equipped clinic for the poor, ignores her plight and paddles past in his rowboat, she swims to shore and accuses him of being an assassin. Julius Sagamore, the shrewd family solicitor, then suggests that Epifania undergo therapy with noted society psychiatrist Adrian Bond. The opportunist Bond makes a bid for her hand, but after he criticises her father, Epifania throws him into the Thames, and when Kabir rows out to help Bond, Epifania jumps in the river after him. To ensnare Kabir, Epifania feigns injury, but the dedicated doctor remains impervious to her charms and indifferent to her wealth.

Determined to win the doctor, Epifania buys the property surrounding his clinic and then erects a new, modern facility. After Kabir rejects Epifanias offer to run the facility, she suggests that they marry instead. Intimidated by the headstrong heiress, Kabir manufactures a deathbed promise that he made to his mother, pledging that he would not marry unless his prospective bride can take 35 shillings and earn her own living for three months. Undaunted, Epifania accepts his challenge and then discloses the details of her fathers will and hands him £500. When Kabir protests that he has no head for money, Epifania plops down the wad of bills and leaves.

Setting out to prove her worth, Epifania takes 35 shillings and heads for a sweatshop pasta factory. There, she threatens to expose the labour violations unless Joe, the proprietor, allows her to manage the plant. Three months later, Epifania has installed labour-saving machines, thus boosting productivity and making the plant a big success. Kabir, meanwhile, has tried in vain to give away his £500. After Kabir becomes drunk at a scientific dinner hosted by a wealthy doctor, he finds a sympathetic ear in his former professor and mentor, who generously offers to accept his money. At the clinic, Kabir eagerly turns over the cash to the professor. Soon after the professor leaves, Epifania appears and informs Kabir that she has met his mothers challenge. When he replies that he has failed and given all the money away, Epifania is deeply offended. Deciding to turn her back on the world of men, she announces that she plans to fire her board of directors, disband her empire and retire to a Tibetan monastery once she has evicted all the monks.

Desperate to keep his job, Sagamore realises that Kabir is responsible for Epifanias erratic behaviour and goes to see the doctor. At the clinic, Sagamore tells Kabir that Epifania has vowed to withdraw from the world at the stroke of midnight. Concerned, Kabir hurries to the reception where Epifania is to bid farewell to her previous existence. Certain that their marriage is now imminent, Sagamore meets the terms of the will by purchasing Kabirs medical papers for £15,000. After Kabir rushes to Epifania, they kiss and he finally expresses his love.

==Cast==
* Sophia Loren as Epifania
* Peter Sellers as Doctor Kabir
* Alastair Sim as Sagamore
* Vittorio deSica as Joe
* Dennis Price as Adrian
* Gary Raymond as Alastair
* Alfie Bass as Fish Curer
* Miriam Karlin as Mrs. Joe Noel Purcell as Professor
* Virginia Vernon as Polly
* Graham Stark as Butler
* Diana Coupland as Nurse
* Pauline Jameson as Muriel
* Eleanor Summerfield as Mrs. Willoughby
* Willoughby Goddard as President
* Basil Hoskins as 1st Secretary
* Gordon Sterne as 2nd Secretary
* Tempe Adam as Gloria
* Wally Patch as Whelk Seller
* Charles Hill as Corelli

==Reception==
Sellers plays Ahmed el Kabir, a socialist Indian doctor, who meets Epifania Parerga (Loren), a wilful and arrogant heiress who has married to satisfy the conditions of her fathers will but whose marriage has not produced the hoped-for happiness. Rather memorable performances are given by Alastair Sim, playing Julius Sagamore, the head of the millionairesss business empire, Vittorio de Sica as Joe, the proprietor of a pasta production sweatshop, and Alfie Bass as a self-employed Cockney fish curer.
 Goodness Gracious Me", sung by Sellers and Loren in their film characters. Martin commissioned David Lee and Herbert Kretzmer to write the song. Martin himself produced the recording. Martin envisioned the song as a recording to be incorporated in the soundtrack of the film. The films producers did not agree to this, but the studio was happy to see the song released as a stand-alone single to promote the film. The song became a UK chart hit in 1960 and succeeded in publicising the film.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 