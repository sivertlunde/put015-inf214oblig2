When the Grapevines Bloom on the Danube
When Austrian comedy film directed by Géza von Cziffra and starring Hansjörg Felmy, Ingeborg Schöner and Gaston Brocksieper.  The cinematographer was Heinz Pehlke.  Its German title is An der Donau, wenn der Wein blüht.

==Cast==
* Hansjörg Felmy ...  Frank Richter
* Ingeborg Schöner ...  Gabriele Welser
* Gaston Brocksieper ...  Florian Richter
* Peter Weck ...  Dr. Walter Kuntz
* Letícia Román ...  Susi Müller
* Elisabeth Markus ...  Hofrätin Welser
* Richard Eybner ...  Diener Ferdinand
* Ottilie Iwald ...  Frau Beck
* Edd Stavjanik ...  Chauffeur Martin
* Hans Habietinek ...  Tagportier
* Fritz Puchstein ...  Nachtportier

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 