Asoo Billa

{{Infobox film
| name           = Asoo Billa
| image          =
| image size     =
| caption        = 
| director       = Husnain
| producer       =
| writer         =
| starring       = Shaan (Pakistan actor)|Shaan, Sana Nawaz|Sana, Baber Ali, Naghma
| music          =
| cinematography =
| editing        =
| released       =
| runtime        =
| country        = Pakistan Punjabi
| budget         =
| preceded by    =
| followed by    =
}}

Asoo Billa is a typical Punjabi masala action revenge flick. It became a sizeable and surprising hit in 2001. The Pakistani film is directed by classic Punjabi director Husnain, who has produced films like Nikah and Naukar Tey Malik.

==Plot==
Asoo’s (Shaan (Pakistan actor)|Shaan) father, who works in a local godown finds that “poe-durr” (heroin) is being smuggled from the godown and he being the upright, virtuous (idiotic) type decides that he absolutely must blow his own trumpet and go to the police to blow the whistle on the nefarious drug smuggling operation going on under his own very nose at the behest of his own employers.

Asoo’s dad meets a terrible fate, as the police turns out to be a party to the drug smuggling ring instead of rewarding the poor man for his civic sense and courage, thrashesand beats him to a pulp. Then the old man is fraudulently charged with the theft of Rs. 2 lac, humiliated and thrown into prison. Meanwhile a shell shocked Asoo arrives at the police station where he is told that unless he manages to get hold of the money that his father has been accused of stealing…matters would get out of hand. When poor Asoo fails to turn up with the money on time the corrupt officer turns up at his home and humiliates Asoo’s mother and hits her and worse causes the dupatta to fall of her head. This indignity for Asoo is the last straw and as he arrives at the Police station in a fury it is only to find his dad being pummeled into a meatball. This for young Asoo is too much to take and all of a sudden the mild mannered Asoo is transformed into a drooling, axe-wielding maniac who proceeds to bludgeon to death what seems like the entire local police force.

The next day in court Asoo pleads guilty to murder and is sentenced to death so vehemently by the judge that he snubs his nib while writing the word death…….deep, meaningful symbolism Lollywood style! Now the blood bath begins as his friend Ghiasia whisks Asoo away from jail in a deadly grenade attack. Asoo takes refuge with Sana the golden-hearted prostitute who shows up habitually to launch into a frenetically energetic dance complete with serious pelvic thrusting and torso twisting. Asoo Billa becomes the local Robin Hood, stealing from the rich to provide for the destitute and now it remains to be seen if he can complete his mission of destroying all his enemies before he is himself struck down and it doesn’t take a genius to guess exactly what does transpire.

==Success in Pakistan==
Asoo Billa became a sizeable box office smash in the first half of 2001 running to packed houses all over the country in the pre-Osama lull before the storm. It is difficult to imagine why this particular film scored a bull’s eye while so many others that appear to be identical in theme and execution and actors failed.

 
 
 