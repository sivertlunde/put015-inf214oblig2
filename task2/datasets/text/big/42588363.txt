Ganadevata
 
{{Infobox film
| name           = Ganadevata
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Tarun Majumdar
| producer       = Department of Information and Cultural Affairs, Government of West Bengal
| writer         = 
| screenplay     =Tarun Majumdar Rajen Tarafdar  Arnab Majumdar  (dialogue) 
| story          = 
| based on       =    
| narrator       =  Anup Kumar
| music          = Hemanta Mukherjee
| cinematography = Shakti Banerjee
| editing        = Ramesh Joshi
| studio         = 
| distributor    = 
| released       =   
| runtime        = 172 min.
| country        = India Bengali
| budget         = 
| gross          =

}} Bengali drama film directed by Tarun Majumdar, based on a novel by same name by Tarashankar Bandopadhyay.  The film stars Soumitra Chatterjee, Ajitesh Bannerjee, Samit Bhanja, Nilkantha Sengupta, Robi Ghosh and Anup Kumar in lead roles. The epic novel is set in 1920s during the British Raj, about the breakdown of socio-economic structures, impact of industrialization and non-cooperation movement in rural Bengal . It had also won the writer Bandopadhyay, the 1966 Jnanpith Award.  
 Best Popular Best Child Artist Award for Kanchan De Biswas.      

==Cast==
* Soumitra Chatterjee as Debu Pandit
* Kali Banerjee
* Sandhya Roy as Durga
* Tapen Chatterjee as Tara Napit
* Ajitesh Bannerjee
* Samit Bhanja
* Nilkantha Sengupta as Tarini
* Robi Ghosh as Haren Ghosal
 Anup Kumar
* Santosh Dutta as Surveyor
* Debraj Roy
* Madhabi Mukherjee
* Sumitra Mukherjee
* Monu Mukhopadhyay
* Anamika Saha
* Santu Mukhopadhyay
* Samit Bhanja

== References ==
 

==External links==
*  

 
 

 
 
 
 
 
 
 


 