Chal Chala Chal
 
{{Infobox film
| name           = Chal Chala Chal
| image          = Chal Chala Chal.jpg
| alt            =  
| caption        = DVD Cover
| director       = T K Rajeev Kumar
| producer       =  Dharmesh Rajkotia   Manna Shetty   G.P. Vijayakumar
| writer         =
| screenplay     =
| story          =
| starring       = Govinda Rajpal Yadav Reema Sen Om Puri
| music          = Songs:  
| cinematography =  Madhu Neelkanthan
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        = 125 minutes
| country        = India
| language       = Hindi Rs 70&nbsp;million
| gross          =
}}
 Indian film Varavelpu starring Mohanlal and many scenes from another Malayalam film Ee Parakkum Thalika.

==Plot==

A story of a simpleton whose life changes when he becomes the owner of a private bus.Deepak (Govinda (actor)|Govinda) is a simpleton. He has been switching jobs as he does not want to succumb to the corrupt system. Since years, with unwavering efforts, financial hardships and an unshaken faith in the judicial systems, he has been helping his father Omkarnathji (Om Puri) in a legal matter.

Omkarnathji, the ex-principal of a private school, is fighting a court case against the school to get his due provident fund and pension. He later wins the case and the school is ordered to give a part of its property as compensation, if there is no money in its funds.

And thus Deepaks life gets an addition: a bus.

Instead of selling it off, acting on his fathers advice, he decides to run the bus. While the rest of the family members - two sisters: Chhaya and Aprana (Upasana Singh and Amita Nangia) and their Ghar-Jamaaee husbands Vinayak Agrawal, a Lawyer (Asrani (actor)|Asrani) and U.U. Upadhyay (Manoj Joshi) are against it. They feel its a low profile job. Their primary interest is in selling off the huge bus and devouring their share of property.

But Deepak has immense trust in his fathers judgment. Sundar (Rajpal Yadav) a jolly good sweetheart, who is desperately trying for an American Visa, is also an age old friend. He comes in handy with initial investments and they establish a company, Chal Chala Chal transport.

And so begins the ride of their lives.

The bus is in a dilapidated condition and much of money is gone for the repairs.

Its a roller-coaster ride where mishaps are more than the commuters. The bus driver, Basantilal (Razzak Khan) wears thick glasses and the conductor Harilal (Asif Basra) has a sugar factory in his mouth and an eye for cash... adding to it like a cherry on the triple-decker pastry- Sunders enmity with a rat which has eaten his passport.

Corruption chases Deepak in transport business as well U.U. Upadhyay is a chief vehicle inspector. He tried his level best to harass Deepak and extort money, raising troubles.

These workers are messing up Deepaks life and business, but he cant raise a finger against them, for they are under the cushioned wings of the Union Leader Mr. Singh (Murli Sharma).

They only relief should have been the lovely lady on the bus Payal (Reema Sen), but the bus hits her fracturing her leg. Now she is also in the vengeance mode, extorting money from Deepak.

What saves Deepak from these mad house characters, is his faith in his principals and his fathers love-acting as the strong backbone in bitter sweet times.

==Cast== Govinda as Deepak O. Nath
* Rajpal Yadav as Sunder
* Reema Sen as Payal
* Razzak Khan as Basantilal (Bus Driver)
* Murli Sharma as Gajendra Singh (Union Leader)
* Asif Basra as Harilal (Bus Conductor)
* Manoj Joshi as Uttam Upadhyay
* Upasna Singh as Chhaya U. Upadhyay
* Asrani as Advocate Vinayak Agrawal
* Amita Nangia as Arpana V. Agrawal
* Om Puri as Omkarnath
* Satyajit Sharma as Nirmal Kumar
* Veerendra Saxena as Banwari - Tailor
* Firdaus Mewawala as the Judge

==References==
 

==External links==
*  

 
 
 
 