The Last Contract
 
{{Infobox film
| name           = The Last Contract
| image          = Sistakontraktet29715.jpg
| alt            = 
| caption        = The Last Contract British DVD cover
| director       = Kjell Sundvall
| producer       = Börje Hansson
| writer         = John W. Grow Mats Arehn
| starring       = Michael Kitchen Mikael Persbrandt Pernilla August
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Sweden
| language       = Swedish English
| budget         = 
| gross          = 
}} murder of Swedish Social Democratic Prime Minister Olof Palme on 28 February 1986. A Swedish police officer (played by Mikael Persbrandt) discovers the plan to assassinate Palme and tries to prevent it. The film also starrs Pernilla August, Reine Brynolfsson and Cecilia Ljung.
  assassin (played by Michael Kitchen) was hired to kill Olof Palme.

== Plot ==
The film suggests that a British professional hit-man was hired by the CIA to assassinate the Swedish Prime Minister Olof Palme, because of Palmes stance on nuclear weapons in Scandinavia . The hit-man (Michael Kitchen) works for money alone and is very careful. He finds a Norwegian man that hates the Swedish P.M. He also involves a man to be blamed (this character is Christer Pettersson, originally convicted of the real assassination, but freed on appeal). The Norwegian man kills Palme just as his wife notices Christer Pettersson. Soon afterwards the hitman kills the killer, and all traces pointing to the professional hitman are gone.

==Cast==
*Mikael Persbrandt ... Roger Nyman, the secret police
*Michael Kitchen ... John Gales alias Ray Lambert, the professional who assassinates the hit-man in order to clean up.
*Pernilla August ... Nina Nyman, Roger Nymans wife
*Reine Brynolfsson ... Bo Ekman, police officer and Roger Nymans best friend
*Bjørn Floberg ... Tom Nielsen, the Palme assassin, who is then killed himself by John Gales alias Ray Lambert.
*Jacqueline Ramel ... Helene Salonen, of the secret police
*Cecilia Ljung ... Holmgren, Lisa, journalist
*Joakim Forslund ... Markus Nyman, Rogers child
*Julia Brådhe-Dehnisch ... Josefin Nyman (as Julia Dehnisch)
*Agnes Granberg ... Sara Nyman, Rogers child
*Johan Lindell ... Bernhard Lange, of the secret police
*Mathias Henrikson ... Peter Bark
*Donald Högberg ... Appelqvist, of the secret police
*Per Ragnar ... KG, of the secret police
*Paul Birchard ... Bertram Norris
*Anders Ekborg ... Bengt Löfgren
*Leif Andrée ... Max Berg, the corrupt journalist
*Ola Isedal ... Karl Larsson, the drug addict
*Stephen Webber ... Wilson, the American
*Stig Engström ... Bartender at the press club
*Emil Forselius ... The Robber
*Claudia Galli ... Shop Assistant
*Tshamano Sebe ... Ery Sontanga, the African
*Faith Maqoqa ... Judy Sontanga, the African
*Bankole Omotoso ... Luwamba
*Dipuo Huma ... Lamberts cleaning lady
*Stephan Karlsén ... Arkivman
*Sven-Åke Wahlström ... Palmehatare
*Lasse Petterson ... Palmehatare
*Berndt Östman ... Portier (as Bernt Östman)
*Roger S. Karlsson ... Guard
*Bo Bertil Lundqvist ... Statsministern

==External links==
* 
* 
*   

 
 
 
 
 
 
 
 
 
 