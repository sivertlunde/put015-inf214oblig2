Merrill's Marauders (film)
{{Infobox film
| name           = Merrills Marauders
| image          = Mermaraudpos.jpg
| caption        = Original film poster
| director       = Samuel Fuller
| producer       = Milton Sperling (United States Pictures Productions)
| writer         = Samuel Fuller Milton Sperling, based on the book The Marauders by Charlton Ogburn Jr (1956)
| working title  = The Marauders Jeff Chandler Peter Brown Will Hutchins John Hoyt Samuel V. Wilson Howard Jackson Franz Waxman (uncredited score from Objective, Burma!) American Patrol by F. W. Meacham
| cinematography = William H. Clothier Technicolor Cinemascope
| technical adviser = Samuel V. Wilson
| editing        = Folmar Blangsted
| distributor    = Warner Bros.
| released       =  
| runtime        = 98 min.
| country        = United States English
}}
 1962 Cinemascope unit of the same name in the Burma Campaign, culminating in the Siege of Myitkyina. 
 Charlton Ogburn Jeff Chandler stock company who were then the lead actors in American television shows.

==Plot==

The  film begins with off-screen narration over black-and-white historical footage of the WWII Burmese campaign, including mention of all American allies who participated. The film then segues into Technicolor as we observe Lt. Stockton’s (Ty Hardin) platoon moving through the jungle toward their first objective, the Japanese-held town of Walawbum. After Stockton radios Gen. Merrill (Jeff Chandler) that they are nearing their goal, he and the rest of the brigade carry out a successful raid.

Afterwards, General Joseph Stilwell (John Hoyt) arrives in Walawbum to order Merrill on another objective, the railroad center of Shaduzup, and ultimately the strategic airstrip at Myitkyina. With reluctance, Merrill later summons Stockton to brief him on their next mission and the unit continues their march through hellish swamps before taking Shaduzup from the enemy. 

The brigade continues their mission up steep mountains for several days and nights before digging in just outside Myitkyina. As night falls, the unit endures a massive artillery barrage. The dawn then brings a Japanese banzai attack, which Merrill’s men successfully repel. Then, while desperately rallying what is left of his unit to move on to the base at Myitkyina, the general suddenly collapses from a heart attack. The men, led by Stockton, slowly rise up and trudge onward toward Myitkyina as an incredulous "Doc" (Andrew Duggan) cradles Merrill in his arms. In fact, it is Doc’s off-screen narration we hear next as he informs us that Myitkyina was indeed taken.

==Cast== Jeff Chandler as Brig. Gen. Frank D. Merrill
* Ty Hardin as 2nd Lt. Lee Stockton Peter Brown as Bullseye
* Andrew Duggan as Capt. Abraham Lewis Kolodny, MD
* Will Hutchins as Chowhound
* Claude Akins as Sgt. Kolowicz
* Pancho Magalona as Taggy
* Luz Valdez as Burmese girl
* John Hoyt as Gen. Joseph Stilwell Charlie Briggs as Muley
* Chuck Roberson as Battalion Commander
* Chuck Hayward as Officer Chuck Hicks as Cpl. Doskis
* Samuel Vaughan Wilson as Lt. Col Bannister Jack Williams as Medic

==Production== producer Milton stuntmen (Chuck Jack Williams and Chuck Hicks), two Filipino film stars and the films technical advisor. Due to bad weather, Fuller shot six days over the allotted 41-day shooting schedule. 
 dry run Jeff Chandler and cast him. 

Samuel Fuller and Milton Sperling simplify, but follow the events and narrative of Ogburns historical account. However, they use the character structure of Denis and Terry Sanders screenplay for The Naked and the Dead; an earnest young lieutenant "Stock" in command of a military intelligence and reconnaissance platoon is a mediator between his men and a fatherly  Brigadier General Frank Merrill. The screenplay also features a grave medical officer "Doc" continually briefing Merrill (and the audience) on the physical and psychological condition of the men and on Merrill himself.
 flashbacks and viciously hate each other, the Marauders are professional veterans who respect each other. They gradually deteriorate when the one mission becomes two, then three, then an endless one, and a beat-the-clock narrative in capturing an airstrip before the monsoon grounds aircraft. The audience sees the effects on the men of not only enemy action, but hunger, tropical diseases, disorientation, sleep deprivation, and breakdown (physical, moral, mental). The soldiers continue, demonstrating superb combat leadership, professionalism, sacrifice, and endurance. 
 panning across the battle instead of cutting to close-ups of who was shooting whom. The studio told Fuller it looked "too artistic" and had a second unit director re-shoot some of the scenes (only one scene appeared in the final print),  and also changed the original ending to feature soldiers on dress parade, which angered Fuller; he fought the studio; they dropped plans to film The Big Red One.

During the film, Jeff Chandler, who had back problems, injured himself playing baseball with some of the American soldiers working on the film.  Despite the pain, Chandler continued filming; his pain is noticeable. On returning to the U.S., he died under anesthesia during back surgery.  Merrils Marauders was critically and financially successful, and was the final Warner Bros. film made in Cinemascope. The film was illustrated in a movie tie-in Dell Comics American comic book. Eighteen years after this film, Sam Fuller made another war film, The Big Red One, this time based on his own combat experiences in Wartime Europe.

==Stock footage and music== Battle Cry in the attack at Walawbum.  Warner Bros. also used bits of Max Steiners score for Operation Pacific and  Franz Waxmans score from Objective, Burma! that was also used in Warners Up Periscope (1959).  The 1885 tune American Patrol appears in not only the final parade scene but in bits throughout the film that either indicates that the film was scored after the addition of the changed ending or that American Patrol may have been the original title music rather than Howard Jacksons title theme.

==References==
 

==External links==
*  
*  
*  
* Merrills Marauders at the Movies http://www.marauder.org/movie.htm
*  
* comic book of the film http://www.marauder.org/comic.htm
* Peter Brown website of the film http://www.peterbrown.tv/merrills.html
* Will Hutchins memories http://www.westernclippings.com/hutch/hutch_2008_02.shtml
 

 
 
 
 
 
 
 
 
 
 
 
 