Vášnivý bozk
{{Infobox film
| name           = Vášnivý bozk
| image          = Vasnovy bozk.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Miroslav Šindelka
| producer       = Rudolf Biermann
| writer         = 
| screenplay     = Miroslav Šindelka  Ondrej Šulaj
| story          = 
| based on       = 
| narrator       = 
| starring       = Jozef Kroner  Ivana Chýlková  Jiří Bartoška  Szidi Tobias
| music          = Michal Kaščák
| cinematography = Marek Jícha
| editing        = Roman Varga
| studio         = 
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = Slovakia Czech Republic Czech
| budget         = 
| gross          = 
}}
 Slovak psychodrama, starring Jozef Kroner, Ivana Chýlková and Jiří Bartoška.  

==Cast==
*Ivana Chýlková as Hana
*Jiří Bartoška as Igor
*Szidi Tobias as Blanka
*Jozef Kroner as Schneider
*Matej Landl as Viktor
*Roman Luknár as Lajko
*Andrej Hryc as Mário
*Katarína Kolníková as grandmother
*Ján Mildner as grandfather

===Additional credits===
* František Lipták - art director
* Katarína Hollá - costume designer sound
* Petr Dvořák - electrician score mix

==See also==
*List of Slovak submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 

 