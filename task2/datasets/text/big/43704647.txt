Abatar (film)
{{Infobox film
| name           = Abatar 
| image          = 
| image_size     = 
| caption        = 
| director       = Premankur Atorthy
| producer       = Sree Bharat Lakshmi Pictures
| writer         = 
| narrator       = 
| starring       = Durgadas Bannerjee Tulsi Chakraborty Ahindra Choudhury Devbala
| music          = Himangshu Dutta
| cinematography = Nitin Bose
| editing        = 
| distributor    =
| studio         = Sree Bharat Lakshmi Pictures
| released       = 1941
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} Bengali mythological drama film directed by Premankur Atorthy.       The film was produced by Sree Bharat Lakshmi Pictures.    The music of the film was composed by Himangshu Dutta who is referred to as Surasagar Himangshu Dutta. He made use of Rabindra Sangeet in his compositions and helped familiarize S. D. Burman to its use in composing semi classical songs.    The cast included Durgadas Bannerjee, Ahindra Choudhury, Jyotsna Gupta, Tulsi Lahiri, Utpal Sen and Panna Devi.    The film is a mythological rendering of King Indranath’s misfortunes and the descent to earth of Gods incarnated as humans.

==Plot== Kamala manifests Narayan on earth. The kingdom goes through difficult times. Omkarananda is arrested and Rupasi goes to meet Bastabesh/Indranath who is attracted to her. Rupasi takes refuge in  Birodhananda’s house to avoid Bastabesh’s advances.  Birodhananda is interested in getting his son Tribhanga married to Rupasi.  Bastabesh goes after Rupasi/Kamala who blinds him. Finally the gods perform their miracle and a repentant Bastabesh is restored his sight and kingdom.

==Cast==
*Durgadas Bannerjee as Narayan/Tribhanga
*Ahindra Choudhury as Indranath/Bastavesh
*Jyotsna Gupta as Kamala/Rupasi
*Tulsi Lahiri as Narad/Birodhananda
*Utpal Sen as Omkarananda
*Renuka Roy as Maya
*Panna Devi as Kalyani/Rani
*Kamla Jharia as singer
*Santosh Singha
*Nitish Gangopadhyay
*Bhumen Roy as Jantraraj
*Satya Mukhopadhyay
*Kartik Roy

 
==References==
 

==External links==
* 

 

 
 
 

 