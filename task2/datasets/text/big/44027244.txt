Paalkkadal
{{Infobox film
| name           = Paalkkadal
| image          =
| caption        =
| director       = TK Prasad
| producer       = KKS Kaimal
| writer         = C Radhakrishnan
| screenplay     = Sharada Mohan Prema
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         = Sangeetha Pictures
| distributor    = Sangeetha Pictures
| released       =  
| country        = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film, Prema in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
 
*Sheela Sharada
*Mohan Sharma Prema
*Sankaradi Raghavan
*Bahadoor
*Kottarakkara Sreedharan Nair
*MG Soman
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Divaaswapnaminnenikkoru || P. Madhuri, Vani Jairam || Sreekumaran Thampi ||
|-
| 2 || Indraneelaambaram || P Jayachandran || Sreekumaran Thampi ||
|-
| 3 || Kunkumapottiloorum || Vani Jairam || Sreekumaran Thampi ||
|-
| 4 || Rathidevatha Shilpame || K. J. Yesudas || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 