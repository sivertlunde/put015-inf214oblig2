Marilyn (1953 film)
{{Infobox film
| name           = Marilyn
| image          = Marilyn 1953 film poster.jpg
| caption        = Theatrical release poster
| director       = Wolf Rilla
| producer       = Ernest G. Roy
| writer         = Wolf Rilla
| story          = Peter Jones (play Marion)
| starring       = Sandra Dorne Maxwell Reed
| music          = Wilfred Burns
| cinematography = Geoffrey Faithfull
| editing        = Peter Seabourne Nettlefold Studios
| distributor    = Butchers Film Service (UK) Astor Pictures (US)
| released       = 1953 (UK) August 1955 (US)
| runtime        = 70 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Marilyn, released in the United States as Roadhouse Girl, is a 1953 British film noir directed by Wolf Rilla starring Sandra Dorne and Maxwell Reed. 

==Plot==
Randy mechanic Tom Price (Reed)  lusts after seductive Marilyn (Dorne), the young wife of ill-tempered garage owner George Saunders (Dwyer). Assuming that Marilyn is fooling around with his employee, Saunders begins punching him out. Defending himself, Tom accidentally kills his boss. Marilyn helps him to cover up the crime, and together the two strike out to find a new life. 

Several months later, the couple is running a just-getting-by roadhouse. Wealthy Nicky Everton (Mayne) agrees to lend the couple some money, figuring that Marilyn will offer her affections as repayment. Throughout all this, Rosie - Marilyns maid - has kept the murderous secret, until she is taken for granted once too often by her self-centered boss.

==Cast==	 
* Sandra Dorne as Marilyn Saunders
* Maxwell Reed as Tom Price
* Leslie Dwyer as George Saunders
* Vida Hope as Rosie
* Ferdy Mayne as Nicky Everton
* Hugh Pryse as Coroner
* Kenneth Connor as Customer in Roadhouse  Ben Williams as Jury Foreman

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 