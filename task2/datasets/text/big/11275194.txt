Blank Generation (1980 film)
{{Infobox film
| name           = Blank Generation
| image          = Blank Generation80.jpg
| image size     =
| caption        = DVD box cover
| director       = Ulli Lommel
| producer       = Roger Deutsch
| writer         = Richard Hell Ulli Lommel Robert Madero
| starring       = Carole Bouquet Richard Hell
| music          = Elliot Goldenthal, Richard Hell
| cinematography = Atze Glanert
| editing        =
| distributor    = International Harmony 1980
| runtime        = 90 min.
| country        = USA English
| budget         =
| preceded by    =
| followed by    =
}}
 1980 American-produced music film. It was directed and co-written by Ulli Lommel, and it stars Carole Bouquet as the French journalist Nada and Richard Hell.  

==Cast==
*Carole Bouquet as Nada
*Richard Hell as Billy
*Ulli Lommel as Hoffritz
*Suzanna Love as Lizzy
*Howard Grant as Jack
*Ben Weiner as Kellerman
*Andy Warhol as Himself
*Robert Madero as Harry
*Bill Mirring as Jonathan Marlowe
*David Pirrock as Bobby Butler
*J. Frank Butler as Bobbys Father
*Ivan Julian as member of the Voidoids
*Robert Quine as member of the Voidoids	 Mark Bell as member of the Voidoids
*Walter Steading as a violin player

==Soundtrack==
Acclaimed composer Elliot Goldenthal composed music for the film.

All songs were written by Richard Hell and performed by Richard Hell and the Voidoids.

==References==
 

==External links==
* 

 

 
 
 
 
 

 