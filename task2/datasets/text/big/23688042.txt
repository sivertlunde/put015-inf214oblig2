The Queen of Sheba (1952 film)
{{Infobox film
| name           = The Queen of Sheba
| image          =The Queen of Sheba (1952 film).jpg
| image_size     =
| caption        = 
| director       = Pietro Francisci
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = Nino Rota
| cinematography =    Mario Montuori
| editor       =  
| distributor    = 
| released       = 1952
| runtime        = 
| country        = Italy Italian
| budget         = 
}}
 1952 Italy|Italian adventure film directed by Pietro Francisci.      

==Plot==
King Solomon (Gino Cervi) sends his son, Prince Rehoboam (Gino Leurini) on a spy mission to Sheba where he falls in love with the beautiful Queen (Leonora Ruffo).  He tries to prevent a war between their two countries but After the Queen finds out her lover is a spy, she leads her army in an assault against Jerusalem.  The siege is a failure and ends with the Queen and Prince reuniting with the blessing of both King Solomon and Shebas advisors.

==Cast==

*Leonora Ruffo as Balkis, Queen of Sheba
*Gino Cervi as  King Solomon of Jerusalem
*Marina Berti as  Zamira, Betrothed of Rehoboam
*Gino Leurini as  Prince Rehoboam of Jerusalem
*Franco Silva as  Kabaal, Commander of Sheban Army
*Mario Ferrari as  Chaldis, High Priest of Sheba Dorian Gray as  Ati
*Umberto Silvestri as  Isachar, Companion of Rehoboam
*Isa Pola as  Tabui
*Nyta Dover as Kinnor
*Franca Tamantini as  False mother
*Fulvia Mammi as  True mother
*Achille Majeroni as  Blind merchant
*Aldo Fiorelli as  Abner
*Pietro Tordi as  Onabar 
*Mimmo Palmara 
*Ugo Sasso  
*Afro Poli 
*Cesare Fantoni

== References ==
 

== External links ==
*   
 
 
 
 
 
 
 


 