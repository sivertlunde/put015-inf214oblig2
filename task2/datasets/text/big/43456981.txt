Against the Wild
 
Against the Wild is a 2013 Canadian direct-to-video childrens film starring CJ Adams, Erin Pitt, Natasha Henstridge and Ted Whittall. The film is directed by Canadian director Richard Boddington.

==Plot==
Siblings Zach Wade (CJ Adams) and his sister Hannah (Erin Pitt) with their dog are on their way by plane to Northern Canada to join their father who runs a mining company. After the engine of the plane theyre flying on catches fire and crashes in the middle of the Canadian countryside, the siblings must find their way to safety and survive by using whatever skills they have.

==Reception and financial performance==
Richard Boddington received the Directors Choice Grand Prize for the film at the 2013 Rhode Island International Film Festival.  The film received a 29% average on Rotten Tomatoes by 32 ratings. 

Since the film was not released in theaters, being direct-to-video its financial profits were from DVD sales only which amounted to about 2 million dollars. 

Against The Wild was nominated for the Emmy Award in the best feature film category in October 2014. 

==References==
 

==External links==
*  on IMBD

 
 
 