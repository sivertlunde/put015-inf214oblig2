Chinna Gounder
{{Infobox film 
| name =  Chinna Gounder 
| image =  Chinna Gounder.jpg 
| caption =  Official CD Cover 
| director =  R. V. Udayakumar 
| writer = R. Selvaraj
| screenplay = R. V. Udayakumar  Sukanya Manorama Manorama Goundamani Senthil Vadivelu
| producer = Venu Chettiyar v. mohan V. Natarajan
| music = Ilaiyaraaja
| cinematography = A. Karthik Raja
| editing = B. S. Nagaraj
| studio = Anandhi Films 
| distributor = G. Venkateswaran|G. V. Films 
| released = 15 January 1992
| runtime = 143 minutes
| country = India
| language = Tamil
}}
 Tamil language Venkatesh and  Vijayashanti in the lead. It was also remade in Kannada as Chikkejamanru with Ravichandran and Gouthami. The film completed a 100-day run. 

==Plot== Village panchayat man (The Arbitrator) who sticks to honesty and deliver authentic judgment irrespective of the accuseds background.In the opening scene itself, the panchayat man is applauded for his judgment (against the Villain-his uncle) by a popular retired Judge.

The panchayat man (Vijayakanth) lives a respected life in a village and sees through that all the people lives happily. Whenever injustice comes to the fore, he appears to resolve it in an authentic manner.This man lives with his mother. One day he comes to know someone is stealing the fruits and using the benefits of his farm which comes to his knowledge and found that,that is a women (Heroine).she escapes in a funny manner and he recognize her. From then on, heroine and hero, mother gets into verbal war,in fact not a serious one. Then situation makes heroine to collect money for repaying her debt and ultimately landing up in getting married to the panchayat man.  At one instance gounder was blamed for abusing his wifes sister and was punished by his mother in front of village people .Then villain conspires against him, without knowing it his wife also mistakes him. In sudden turn of events she was plotted as accuest for murder of a village man who is alliance of the villan, when gounder comes to see her in jail she doesnot like to see him and abuses him for pregnency of her sister, at court there are no solid evidence to prove her guilty that she had killed the village man, after releasing she goes to gounder house where her sister gives birth to child and reveals that she and the man her sister accused of killing had secret relationship and the baby is result of that and gounder is not responsible for the child. Gounder wife realises her mistake search for gounder where she was abducted by villan. At the final battle gounder defeats the villan and made him to stand at the village panchayat. Atlast villain is forgiven and he realizes his mistake and starts to apologize to the people of the village but everyone disrespects and ignore him and put an end to the injustice.

==Cast==
* Vijayakanth as Chinna Gounder (Thavasi) Sukanya as Deivaanai (Chinna Gounders wife) Manorama as Chinna Gounders mother
* Salim Ghouse as Sakkarai Gounder
* Sathyapriya as Sundari
* Goundamani as Vellai
* Senthil as Vellais assistant
* Vadivelu as Thavasis servant
* Kamala Kamesh

==Classic scenes==
One of the most famous scenes of this movie is the one where the movies main protagonist spins a top on the female protagonists navel.   

==Soundtrack==
The music composed by Ilaiyaraaja while lyrics by R. V. Udayakumar.
* Antha Vanatha I - Ilaiyaraaja
* Chinna Kili Vanna Kili- S. P. Balasubrahmanyam, S. Janaki
* Chutti Chutti - Malaysia Vasudevan
* Kannu Pada - Ilaiyaraaja
* Koondu Kulla - S. P. Balasubrahmanyam, S. Janaki
* Muthumani Malai - S. P. Balasubrahmanyam, P. Susheela
* Sollaal Adicha - Ilaiyaraaja
* Antha Vanatha II - S. Janaki

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 