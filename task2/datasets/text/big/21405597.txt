Mu Sapanara Soudagar
{{Infobox film
| name = Mu Sapanara Soudagar
| image = 
| caption = 
| starring = Sabyasachi Mishra Arindam Roy Archita Sahu Ajit Das Akhila Mihir Das
| director = Sanjay Nayak
| producer = Sarada Prasana Panda
| writer = Sarada Prasana Panda
| music = Santiraj Khosla
| cinematography = Ranjan
| editing = Sanjay Nayak
| distributor = suvam films pvt ltd
| released =  
| runtime = 
| country = India
| language = Oriya
}} Agnee Shakhi. The oriya film is superbly done by Sanjay Nayak as director. The biggest plus point of this movies success is Sabyasachi Mishra as Omm, which is critically acclaimed by viewers and critics. Sabyasachi(Omm) played the title character as an anti hero. Archita(Shriya) played love interested of both Omm and Chandan, which was played by Arindam Roy.

==Cast==
*Sabyasachi Mishra - Omm
*Arindam Roy       - Chandan
*Archita Sahu      - Shriya
*Ajit Das          - Omms father
*Akhila Patnaik   - Raja
*Bina Moharana     - Omms mother
*Mihir Das (guest appearance)
*Ommkar

== Box office==
After the super success of Dhana re rakhibu Sapatha mora, Subham films released their second venture on the occasion of Raja Festival. The movie was made on a medium budget. Due to its fast-paced storyline and some good performances, the movie got a very good response at the box office and was declared as a superhit.  Actor Sabyasachi Mishra won many awards for his classy performance in this movie.

== References ==
 

== External links ==
*  
*  

 
 
 

 