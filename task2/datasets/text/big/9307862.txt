The Traveler (1974 film)
{{Infobox film
| name = The Traveler
| image = Traveler kiarostami.jpg
| director =Abbas Kiarostami
| writer =Abbas Kiarostami
| starring =
| producer =
| released = Iran 1974
| country = Iran Persian
| runtime =71 min.
| distributor =The Criterion Collection (R1 DVD, accompanied with Close-Up)
}} 1974 Iranian drama film directed by Abbas Kiarostami. The film depicts a troubled but resourceful boys quest to attend a football match at any cost, and his indifference to the effects of his actions on other people, even those closest to him. Thematically, the film explores childhood, societal conventions, and the origins of moral and amoral behavior. Visually, it affords a candid glimpse of Iranian life in the early 1970s.

==Plot== canes Qassems rials for each "sitting." In a famous sequence, he issues directions to his "subjects" while snapping photo after photo with the empty camera. Onscreen, we see a succession of children posing for portraits that will never materialize. Still lacking sufficient cash, Qassem resolves to sell his teams soccer ball and goals, even though they are group property. Finally he has collected enough for his journey.

Late that evening, Qassem counts down the time in his squalid bedroom, then climbs down a drainpipe and runs through the dark streets to catch the bus to Tehran. Next morning, after the boy waits in a long queue to purchase admission to the stadium, the supply of tickets runs out as hes about to hand over the fee. Wandering among ticket scalpers, he tries to haggle but ends up paying an exorbitant price to finally gain entry. As the stadium seats fill up, he learns from the middle-aged man next to him that three hours remain until the game starts. Impatient, he has the man save his seat, and leaves to explore the athletic complex by himself. After small talk with a laborer, some clambering on a scaffold, and a wistful glimpse of an indoor swimming pool, Qassem lies down for a brief nap in the shade. His dreams are troubled by images of his own guilt and punishment. When he finally wakes, he finds the stadium empty and strewn with litter, the game long over. 

==See also==
*List of Iranian films

==External links==
* 

 

 
 
 
 
 
 
 
 