Emergency Hospital (film)
{{Infobox film
| name           = Emergency Hospital
| image          = Emergency Hospital poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lee Sholem
| producer       = Howard W. Koch
| screenplay     = Don Martin  John Archer Byron Palmer Rita Johnson Peg La Centra
| music          = Paul Dunlap
| cinematography = William Margulies 
| editing        = John F. Schreyer 
| studio         = Schenck-Koch Productions Bel-Air Productions
| distributor    = United Artists
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Archer, Byron Palmer, Rita Johnson and Peg La Centra. The film was released on November 2, 1956, by United Artists.  

==Plot==
 

== Cast ==
*Walter Reed as Police Sgt. Paul Arnold
*Margaret Lindsay as Dr. Janet Carey John Archer as Dr. Herb Ellis
*Byron Palmer as Ben Caldwell
*Rita Johnson as Head Nurse Norma Mullin
*Peg La Centra as Nurse Fran Richards
*Robert Keys as Police Officer Mike Flaherty
*Rhodes Reason as Juvenile Officer Ross 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 