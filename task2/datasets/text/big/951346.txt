Insecticidal
 

{{Infobox film
| name           = Insecticidal
| image          = Insecticidal.jpg
| alt            =  
| caption        = 
| director       = Jeffery Scott Lando
| producer       = Ed Brando Sam Eigen
| writer         = Jeff OBrien
| starring       = Meghan Heffern Rhonda Dent Travis Watters Shawn Bachynski
| music          = Christopher Nickel
| cinematography = Pieter Stathis
| editing        = Wade Taves
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}

Insecticidal is a 2005 horror movie featuring giant killer insects. In the film, a college student performs experiments on insects until a housemate tries to kill them. The insects grow to huge sizes and turn the sorority house into a nest, trying to kill the residents.

==Cast==
 
In order of appearance:
*Meghan Heffern - Cami
*Rhonda Dent - Josi
*Samantha McLeod - Sophi
*Shawn Bachynski - Martin
*Vicky Huang - Fumi
*Travis Watters - Mitch
*Anna Amoroso - Jenni
*Natalia Tudge - Twisti (as Natalia Walker)
*Ryan Zwick - Dick
*Nelson Carter Leis - Kyle
*Anna Farrant - Belli Chris Guy - Cherri
*Alan Steele - Pizza Boy
*Sean Whale - Sluggo
*Simon Sippola - Security Guard (voice)

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 
 