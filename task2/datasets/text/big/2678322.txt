Lapin kullan kimallus
{{Infobox film
| name          = Lapin kullan kimallus
| image         = Lapin kullan kimallus poster.jpg
| caption       = Movie poster
| director      = Åke Lindman
| writer        = Åke Lindman, Heikki Vuento
| starring      = Vesa Vierikko, Pirkka-Pekka Petelius, Lasse Pöysti, Jarmo Mäkinen, Martti Suosalo, Kaija Pakarinen
| producer      = Hanna Hemilä, Åke Lindman
| distributor   = Buena Vista Home Entertainment
| released      = 1999
| runtime       = 2h
| country       = Finland
| language      = Finnish
}} Finnish for Finnish Film|movie, directed by Åke Lindman.
 Lapland in the end of the 19th century. It stars Pirkka-Pekka Petelius as Nils Lepistö and Vesa Vierikko as Jakob Ervasti.

==Plot summary==
 Teno River, where they manage to find a couple of grams of gold. They travel back to Oulu to lay official claim on the find site but are told it has already been claimed. Finally a large gold rush to Lapland starts.

==Notes==
Despite the name, the movie has nothing to do with the Finnish brand of beer named Lapin Kulta (Gold of Lapland).

==External links==
*  

 
 
 
 
 


 
 