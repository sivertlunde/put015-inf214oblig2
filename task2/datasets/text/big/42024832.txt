Senthamizh Paattu
{{Infobox film
| name = Senthamizh Paattu
| image = 
| caption =
| director = P. Vasu
| writer = P. Vasu
| producer = M. S. V. Gopi Prabhu Sukanya Sukanya Kasthuri Kasthuri
| cinematography = Jayanan Vincent
| editing = P. Mohanraj
| music = M. S. Viswanathan Ilaiyaraaja
| studio = Janaki Films
| distributor = 
| released = 25 October 1992
| runtime = 141 min
| country = India
| language = Tamil
| gross =
}} 1992 Indian Prabhu and Sukanya in the lead roles, while Salim Ghouse plays a supporting role. The film, which has music composed by M. S. Viswanathan and Ilaiyaraaja was a Deepavali release of 1992. The film was remade in Telugu as Amma Koduku with Rajasekhar (actor)|Rajasekhar.

==Cast== Prabhu
* Sukanya
* Kasthuri
* Goundamani Vijayakumar
* Manjula Vijayakumar Sujatha
* Salim Ghouse
* Mayilsamy
* Ravikumar
* Kazan Khan

==Production==
The success of P. Vasus family drama En Thangachi Padichava (1988) starring Prabhu prompted further collaborations in the same genre between the pair with Chinna Thambi (1991) and Senthamizh Pattu (1992) being released soon after.  

==Soundtrack==
The music composed by Ilaiyaraaja.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali (poet) Vaali || 05:08
|-
| 2 || Chinna Chinna Thooral || S. P. Balasubrahmanyam || 05:09
|- Mano || 05:09
|-
| 4 || Kalayil Kethattu || S. P. Balasubrahmanyam, Swarnalatha || 05:12
|- Mano || 04:54
|-
| 6 || Solli Solli || S. P. Balasubrahmanyam, Sunandha || 05:11
|-
| 7 || Vanna Vanna || Jikki || 05:01
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 