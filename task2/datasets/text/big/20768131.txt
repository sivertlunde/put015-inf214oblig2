My Name Is Jerry
{{Infobox film
| name           = My Name Is Jerry
| image          = myNameIsJerry_Poster_2009.jpg
| alt            =  
| caption        = Official poster for the film
| director       = Morgan Mead
| producer       = Rodger Smith Bruce Economou Zach Baliva
| writer         = David Hamilton Andy Janoch Morgan Mead Doug Jones Don Stark Catherine Hicks Allison Scagliotti
| music          = Rick DiGiallonardo
| cinematography = Nathan Wilson
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} The Walking Dead.

==Synopsis==
Door-to-door salesman, Jerry Arthur’s life is filled with slamming doors and low expectations. The world has lost its attraction. He is a middle-aged divorcé who lives on his own. He has an unsure and unwanted future, a daughter he has had no contact with for ten years, a dead-end job and low self-esteem. In other words, Jerry is close to hitting rock bottom. Jerrys life changes when his ex-wife dies and their daughter moves in with him. Around the same time, he stumbles on a group of young people with their futures before them. Their energy and music reminds him that you can influence your life for the better regardless of the past...if you choose to do so. Through new and old friends alike, Jerry discovers you can move forward even when you can’t see exactly where you are going.  As Jerry puts it, “What happened to going with your gut? My gut’s been telling me to turn things around.”

==Cast== Doug Jones as Jerry: Jerry is a door-to-door salesman who is stuck in a rut.  He’s going through the day to day motions of his life at home and at work because he’s afraid to take a chance and doesn’t know where to start. All it takes is a little inspiration from the “eloquence” of punk to jump-start his re-invention.
*Don Stark as David: As a fellow Milton-Hill salesman to Jerry, David loves the sale, and doesnt mind the dirty reputation.  In fact, he is long over it, because he works to buy the perfect life.  At this point in his life, he finds value in excess.  But he is smart and enjoys a good time.
*Catherine Hicks as Dana Holderman: Dana has spent her life working for men in a largely chauvinistic profession. She fights to be called their equal, and is, at times, harder than she actually needs to be. However, she also cares deeply about the company she works for, and will go to great lengths not to endanger it. All this toughness isolates a different side of her though, and at times she is extremely lonely and vulnerable.
*Allison Scagliotti as Trisha: Trisha has been without her father most of her life, and has no siblings, leaving her in the care of her over-achieving mother.  She is desperately in search of a father figure, but is afraid of being hurt by her father, Jerry, because of the things her mother has told her.
*Katlyn Carlson as Jordan: Jordan is the kind of girl that comes from a house with all older brothers. She is looking for inspiration in her life, and she has hidden desires to be a clothing designer, but isnt quite at peace with her choices in life.  She is suffering a certain level of discontent due to a recently ended relationship.
*Steven Yeun as Chaz: Chaz works at a record store, and likes most music but is generally known as a hipster.  He is part of a band with Slim and Zeb mostly because it is something to do and his friends are involved. Chaz is a flirt and a party-guy. He also lives with Jordan and regularly finds himself trying to be big brother to a girl who already has too many brothers.
*Levi Holloway as Slim: Slim sings in a rock band and is the tortured soul artist among the members.  Passionate and fearless, he takes everything seriously.  However, he is also known by his friends Chaz and Zeb for his ridiculous and mischievous sides.
*Josh Breit as Zeb: Zeb is the strong silent type, and only speaks when he has something profound to say.  Some think that Zeb takes himself too seriously, when in fact, he is usually just bored and finds ways to amuse himself.  If Chaz is the mouth and Slim is the soul, then Zeb is the brain.

==Production==

===Development===
Originally conceived in 2006 by Mead and writing partner Andy Janoch as a starring vehicle for Doug Jones, Mead took the original project to Ball State University professor Rodger Smith, who along with his Institute for Digital Education and Entertainment (IDEE) program, was interested in involving the university with a film project.  Mead, Janoch and Smith went through several scripts drafts before Mead contacted David Hamilton, a college-friend who was working in Los Angeles for the series ER (TV series)|ER.  Hamilton did a script re-write and along with fellow ER alum Zach Baliva, came on board to produce the film.

Though Ball State University had the equipment to produce a motion picture, they did not have the necessary personnel, so professionals from Los Angeles, Chicago, and Austin were hired to take the roles of department heads, to further the students experience on the project, and to give the film a more polished-look.  Nathan Wilson was hired as the Director of Photography, after having worked on several high-profile films.  Michael Bricker was brought on board as Production Designer, based on his work on smaller films produced in Austin, TX and with the University of Texas at Austin.  Blair Scheller was hired out of Chicago for his experience in the sound departments on big-budget motion pictures.  All of this led to a more rounded experience for the Ball State University students who would fill out the support crews and production positions.

Originally slated for a January start, the script was re-written to be a set in the summer time, with a New Years Eve celebration being changed to a party taking place on the Fourth of July.  Production finally began in July 2008, in Muncie, Indiana, after Jones had completed his promotional duties for Hellboy II.  Principal photography lasted for 20 days.  During the shooting of several scenes, the temperatures would reach triple-digits, but the young crew and the experienced cast kept working and shooting wrapped only one day behind what had originally been scheduled, with only a single pick-up shot filmed some time later (to replace a shot that was unusable due to a digital glitch in the original footage).

===Casting===
With star Doug Jones already in place, casting sessions began in Los Angeles, Chicago, Indianapolis, and Muncie, Indiana.  The Los Angeles casting sessions brought Allison Scagliotti, Don Stark and Catherine Hicks into the mix.  From Chicago, Josh Breit, Steven Yeun, Levi Holloway and Jonathan Keaton.  All the other roles in the film were performed by local stage actors from the Muncie area (where the film was shot) and students from the University.  The trickiest part to cast turned out to be that of Jordan, the young woman who steals Jerrys heart and motivates his journey.  While the producers were unable to decide who to cast for a long period of time, Mead found his Jordan in an audition for Trisha, Jerrys daughter, from Chicago-based Katlyn Carlson. "   Mead has been impressed with the actress abilities and physical presence, and insisted that she was Jordan.  The producers agreed and Carlson won a role for which she did not audition, but was nonetheless excited to receive.

===Awards===
*Winner Best Actor in a Feature To Doug Jones  Strasbourg International Film Festival
*Winner Best Picture  International Filmmaker Festival
*Winner Supporting Actress  International Filmmaker Festival
*Winner Best Soundtrack  International Filmmaker Festival
*Official Selection  Heartland Film Festival
*Winner Best Comedy  Route 66 Film Festival
*Best Feature Film  Santa Clarita Valley Film Festival
*The Hoosier Award  The Indiana Film Journalists Associtation  (To Doug Jones, Morgan Mead, and David Hamilton for their contributions).

==References==
 

==External links==
* 
* 
*  at YouTube

 
 
 
 