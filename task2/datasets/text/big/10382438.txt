Drive-Thru (film)
 
{{Infobox film
|name=Drive-Thru
|image=Drive_thru.jpg
|caption=Promo poster
|director=Brendan Cowles Shane Kuhn
|producer=
|writer=Brendan Cowles Shane Kuhn
|narrator=
|starring=Leighton Meester Nicholas DAgosto Melora Hardin Larry Joe Campbell Lola Glaudini
|music=Ralph Rieckermann
|cinematography=Vincent E. Toto
|editing=Daniel R. Padgett
|distributor=Lions Gate Entertainment
|released=  
|runtime=91 minutes
|country=United States English
|budget=
|gross=
}} horror comedy serial killing clown mascot Horny the Clown. The film was released on May 29, 2007.

==Plot==
  meat cleaver. Horny proceeds outside and murders Brittany and Tiffany.

Meanwhile, Mackenzie (Leighton Meester) is having a house party with her boyfriend Fisher (Nicholas DAgosto), and friends Val (Sita Young), Van (Penn Badgley) and Starfire (Rachael Bella). The group find a ouija board and decide to ask it what their future will hold, to which the ouija board spells out "N1KLPL8", a message that is unclear until the next day when the group see a news bulletin, showing the message is the license plate on Brandons car. The following day at school, Val is attacked by Horny in the locker room. Mackenzie complains that someone stole her camera at the party, but after school the janitor, Lenny (Sean Whalen), gives her the camera.

Mackenzie stays behind to develop the photos, to find they reveal the deaths of the four murdered teenagers. Horny chases Mackenzie into the gymnasium, where she finds Vals head has been placed in a modified microwave, that when turned on causes her head to explode. Horny chases Mackenzie throughout the school, where she finds Lenny has been hung, before bumping into a police officer, who fails to find any bodies. Mackenzie is taken down to the police station with her Mom, Marcia (Melora Hardin) and Dad, Bill (Paul Ganus) to be questioned by Detective Brenda Chase (Lola Glaudini) and Detective Dwayne Crockers (Larry Joe Campbell). The two detectives dont believe Mackenzies story however, and suspect Lenny as the killer.

The next day, the detectives visit Jack Benjamin (John Gilbert), the owner of Hella Burger, who proves to be no help in solving the case. That night, Mackenzie and Fisher get ready to work at a carnivals haunted house. As they work, they have an argument with Chad (Tyler King) and Tina (Maliabeth Johnson), before they go into the ride. Suddenly, the lights are switched off and Chad is decapitated, before Tina is stabbed to death by Horny. Mackenzie and Starfire enter the haunted house after it breaks down. They find Fisher in a state of shock after witnessing the murders.

Mackenzie goes to visit Fisher in the hospital with Marcia. Mackenzie becomes annoyed with Marcia as she believes she is hiding something from her, as all the murdered teenagers are the children of her old high-school friends. Detective Chase overhears this and questions Tinas father, Bert ( ), Spanky (Chad Cunningham) and Carrie (Sarah Buehler), who are stoned at the Hella Burger. Horny soon arrives and butchers Chuck and Spanky with his cleaver.

As Mackenzie arrives home, Marcia tells her that when she was young, her and her friends accidentally murdered Archie Benjamin (Van De La Plante), Jack Benjamins son, on his 18th birthday at Hella Burger. The group kept it a secret and the police thought it was an accident. After this, Mackenzie goes to visit Jack with Fisher, Starfire and Van. The group split up, but Van soon finds Starfire has been murdered, her corpse shoved into a freezer in the garage. Horny quickly appears and slices Van in half. Mackenzie and Fisher are next attacked, resulting in Mackenzie being knocked unconscious and Fisher being cornered. Horny somehow makes Fishers eyes turn bloody red, before he throws him through a window. Meanwhile, Detective Chase and Detective Crockers come to the house and find Jack Benjamin behind his own bed, trying to hide from Archie.

Mackenzie wakes up in a Hella Burger, tied to a chair, surrounded by her murdered friends. A birthday cake is in front of her, as it is now her 18th birthday. Horny appears and pours gasoline over her, before Marcia arrives and shoots Horny. Marcia tries to free Mackenzie, but Horny is still alive and knocks out Marcia. While this happens, Mackenzie drinks a bottle of whiskey she had, and as Horny taunts her with a lit candle, she sprays the whiskey onto the flame, setting Horny on fire. Mackenzie and Marcia leave as Horny burns alive.

Mackenzie and Marcia rush to the hospital to see Fisher, who was described as in critical condition. However, Fisher wakes up from his coma, still with blood shot eyes, revealing Archie has possessed him. Fisher escapes the hospital. Meanwhile, Detective Crockers is ordering food at Hella Burger before Horny jumps on his car and murders him through the windshield after saying, "Have a hella nice day!".

==Cast==
*Leighton Meester as Mackenzie Carpenter
*Nicholas DAgosto as Fisher Kent
*Melora Hardin as Marcia Carpenter
*Lola Glaudini as Detective Brenda Chase
*Larry Joe Campbell as Detective Crockers
*Penn Badgley as Van
*Rachael Bella as Starfire Shedrack Anderson as Chuck Taylor
*Sean Whalen as Lenny
*Robert Curtis Brown as Bert McCandless
*John Gilbert as Jack Benjamin
*Maliabeth Johnson as Tina McCandless
*Clyde Kusatsu as Fred Kukizaki 
*Edward DeRuiter as Brandon Meeks
*Van De La Plante as Horny the Clown / Archie Benjamin
*Sita Young as Val Espinoza
*Haven Lamoureux as Tony
*Morgan Spurlock as Robbie
*Gordon Clapp as Voice of the Horny The Clown

==Production==
 
==Release==
The film was released on May 29, 2007. 

==Reception==
 
Critical reception has been negative. Dread Central called the dialogue "painfully bad" and criticized the films ending. 

==References==
 
==External links==
* 
* 

 
 
 
 
 