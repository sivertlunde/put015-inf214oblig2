Theriyama Unna Kadhalichitten
{{Infobox film
| name           = Theriyama Unna Kadhalichitten
| image          =
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = K. Ramu
| producer       = V. Vinoth Kumar
| writer         =  
| screenplay     =
| story          =  
| Dialogue       = 
| starring       = Vijay Vasanth Resna Pavithran
| music          = P. R. Srinath
| cinematography = L. K. Vijay
| editing        = 
| studio         = Triple V Records
| distributor    = 
| released       =  
| country        = India
| language       = Tamil
}}
 Tamil film written and directed by Ramu. The film features Vijay Vasanth and Resna Pavithran in the lead roles, and was released in October 2014.

==Cast==
*Vijay Vasanth as Karthik
*Resna Pavithran as Gayathri Pawan as Prakash
*Uma Padmanabhan
*Nizhalgal Ravi
*Mayilswamy Pandu
*Rajya Rajya Lakshmi
*Aravind Akash as special appearance

==Production==
The film marked the first production venture of Vinoth Kumar and he chose to name his studio as Triple V Productions after his father entrepreneur H. Vasanthakumar, his brother actor Vijay Vasanth and himself. He was impressed by a script written by K. Ramu, who waited a few years to narrate the film to a producer, having previously directed television serials and apprenticed under Priyadarshan.  The films soundtrack was launched in July 2013 with several of Vijay Vasanths co-stars from Chennai 600028 (2007) in attendance.  The film had a delayed release, and was stuck for close to a year after production had finished. 

==Release==
The film had a limited release across Tamil Nadu on 2 October 2014, owing to the presence of bigger budget films at the box office. It opened to negative reviews with a critics from The Hindu noting "there is little that is credible" about the venture.  Another reviewer noted "Towards the latter part, the narration peps up, maintaining interest and pace till the end. Makes one wonder if its the same director who had directed the first half".  The film, nevertheless, celebrated a success meet to promote the film. 

==References==
 

 
 
 
 
 


 