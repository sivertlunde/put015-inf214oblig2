Dr. Mabuse the Gambler
{{Infobox film
| name           = Dr. Mabuse the Gambler
| image          = Mabusegambler.jpg
| caption        = 
| director       = Fritz Lang
| producer       = Erich Pommer
| writer         = Fritz Lang Norbert Jacques Thea von Harbou
| starring       = Rudolf Klein-Rogge Aud Egede-Nissen Gertrude Welcker Alfred Abel Bernhard Goetzke
| music          = 
| cinematography = Carl Hoffmann 
| editing        = 
| released       =  
| runtime        = 270 minutes
| country        = Weimar Republic German intertitles
| budget         =
| based on        =  
}}
Dr. Mabuse the Gambler ( ) is the first film in the Dr. Mabuse series, about the character Doctor Mabuse who featured in the novels of Norbert Jacques. It was directed by Fritz Lang and released in 1922. The film is silent and filmed mostly 16 frames per second. It would be followed by The Testament of Dr. Mabuse (1933) and The Thousand Eyes of Dr. Mabuse (1960).

It is four and a half hours long and divided into two parts: Der große Spieler: Ein Bild der Zeit and Inferno: Ein Spiel von Menschen unserer Zeit. The title, Dr. Mabuse, der Spieler, makes use of two meanings of the German Der Spieler which can mean gambler or actor. The character Dr. Mabuse, who disguises himself and is a notorious gambler, embodies both senses of the word. Therefore, the Player might be a more appropriate translation of the title.

The film is included in the book 1001 Movies You Must See Before You Die, being the first of five Lang films to be entered. 

== Plot ==
Part I — The Great Gambler: A Picture of the Time (Part I - Der große Spieler: Ein Bild der Zeit)

Dr. Mabuse is a criminal mastermind, doctor of psychology, and master of disguise, armed with the powers of   dancer Cara Carozza, who loves him. 

As the film opens, Mabuse orchestrates the theft of a commercial contract in order to create a temporary panic in the stock market, which he exploits to make huge profits.  

Edgar Hull, the son of a millionaire industrialist, becomes Mabuses next victim. As "Hugo Balling", Mabuse gains access to Hulls gentlemens club and wins a small fortune at cards from the hypnotized Hull, who is made to play badly and recklessly. Afterwards, Hull is unable to account for his behavior. 

State prosecutor Norbert von Wenk takes an interest in Hull, believing he is the latest in a string of victims similarly tricked by the elusive "Great Unknown". Von Wenk goes undercover at a gambling den, where he encounters a disguised Dr. Mabuse. Mabuse attempts to hypnotize von Wenk, but he effectively resists. Mabuse flees. Von Wenk, quickly regaining his faculties, gives chase through the city, but the doctor escapes. Boarding a taxicab driven by Georg, von Wenk is gassed, robbed, and set adrift in a rowboat.

Dr. Mabuse realizes that Hull is assisting the state prosecutor, and resolves to eliminate both men. Carozza, who has been romancing Hull on Mabuses orders, lures the young man to a new illegal casino; when von Wenk calls in the police to raid the place, Carozza, Hull and a police bodyguard exit through the back door, where Georg awaits. He kills Hull, but Carozza is caught and jailed. Von Wenk questions her for information about the "Great Unknown", but she refuses to speak. Von Wenk enlists the aid of Countess Told (nicknamed the "Passive Lady"), an aristocrat bored by her dull husband and seeking thrills wherever she can find them, to try to get the information by trickery. The countess is placed in the same cell, an apparent victim of another raid, but Carozza is not fooled. Carozza reveals only her great love for Mabuse, ensuring her silence. The countess, moved by Carozzas passion, tells von Wenk that she cannot continue to assist him.

Dr. Mabuse does nothing to extricate Carozza from jail. He instead attends a séance where he meets Countess Told, who (while under his hypnotic influence) invites him to her house. Once there, Mabuse, taken by the Countesss beauty, decides to display his power by telepathically inducing her husband, Count Told, to cheat at poker. His guests are outraged when they detect it, and the Countess faints. Dr. Mabuse uses the distraction to abduct her and imprison her in his lair.

Part II — Inferno: A Game for the People of our Age (Part II - Inferno: Ein Spiel von Menschen unserer Zeit)

A sick and disgraced Count Told seeks the help of Dr. Mabuse to treat his depression; Mabuse uses this chance to isolate the count in his manor and cut off any inquiries about the countesss whereabouts. The counts condition worsens, and he is tormented by hallucinations.

Meanwhile, Carozza is moved to a womans prison and again interrogated by von Wenk. Fearing betrayal, Mabuse sanctions Carozzas death. Georg smuggles poison to her cell, which she takes out of loyalty. Another of Mabuses henchmen, Pesch, bombs von Wenks office while posing as an electrician, but von Wenk is unharmed and Pesch detained. Mabuse – again fearing betrayal – arranges for Pesch to be killed by a sniper while being transported in a police wagon.

Intent on leaving town, Mabuse gives the captive countess the choice of going with him voluntarily. Her refusal angers him, and Mabuse vows that he will kill the count. Through his powers of suggestion, he induces the count to commit suicide with a razor blade. When von Wenk investigates his death, he questions Dr. Mabuse as the counts psychoanalyst. Dr. Mabuse speculates that the count had fallen under the control of a hostile will, and asks von Wenk if he is familiar with the experiments of one “Sandor Weltemann”, who will be performing a public demonstration of telepathy and mass hypnosis at a local theater.

Von Wenk and his men attend Weltemanns show. Weltemann is none other than Mabuse in disguise, and his magic show provides him an opportunity to hypnotize von Wenk, who falls into a trance. Mabuses secret command to von Wenk is to leave the auditorium, get in his car, and drive off a cliff, but von Wenks men intercede just in time. Coming to his senses, von Wenk orders a siege of Mabuses house.

Dr. Mabuse and his men make a final stand. In the ensuing gunfight, Hawasch and Fine are killed, Spoerri and Georg are taken into custody, and the countess is rescued. Dr. Mabuse flees through an underground sewer to Hawaschs counterfeiting workshop, where he becomes trapped, as the doors cannot be opened from the inside. There, Mabuse is confronted by the ghosts of his victims and various demonic illusions. 
 Das Testament des Dr. Mabuse (The Testament of Dr. Mabuse, known also as The Last Will of Dr. Mabuse) it is revealed that he is confined to an insane asylum.

== Cast ==
* Rudolf Klein-Rogge – Dr. Mabuse
* Aud Egede-Nissen – Cara Carozza
* Gertrude Welcker – Gräfin Dusy Told
* Alfred Abel – Graf Told / Richard Fleury  (US version) 
* Bernhard Goetzke – Chief-Inspector Norbert von Wenk / Chief-Inspector De Witt  (US version) 
* Paul Richter – Edgar Hull
* Robert Forster-Larrinaga – Spoerri
* Hans Adalbert Schlettow – Georg, the Chauffeur
* Georg John – Pesch
* Charles Puffy – Hawasch
* Grete Berger – Fine, a servant
* Julius Falkenstein – Karsten
* Lydia Potechina – Die Russin / Russian woman
* Julius E. Herrmann – Emil Schramm
* Julietta Brandt (uncredited)
* Max Adalbert (uncredited)
* Anita Berber (uncredited)
* Paul Biensfeldt (uncredited)
* Gustav Botz (uncredited)
* Lil Dagover (uncredited)

== Production notes ==
Based on Norbert Jacques novel of the same name, Dr. Mabuse, der Spieler was adapted to the screen by Lang and his wife Thea von Harbou. Although the novel was a best-seller at the time, Jacques was convinced by Lang and von Harbou to discontinue his plans for a literary series in exchange for a movie sequel to the hit 1922 film. The three went on to conceive Das Testament des Dr. Mabuse (1933), also starring Rudolf Klein-Rogge as Mabuse. Langs final film, Die tausend Augen des Dr. Mabuse (1960), picked up the character once more and rounded up the trilogy. Wolfgang Preiss took over the title role since Klein-Rogge had died in 1955. The movie was produced by Arthur Brauner, who would go on to produce five more movies centering on the character of Dr. Mabuse (whom Preiss portrayed in four more movies), including a 1962 remake of Langs 1933 film.
 Murnau Foundation. The final, restored version lasts 297 minutes. Generally, the movie was cut down and re-edited when originally shown. The USA video version lasts a little under four hours. At the time of its release, Soviet editors also  re-cut the Dr. Mabuse films into one shorter film. The lead editor in charge of the cuts was Esfir Shub assisted by Sergei Eisenstein.

==References==
 
== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 