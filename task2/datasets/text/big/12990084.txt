Ogro
 
{{Infobox film
| name           = Operación Ogro
| image          = Operación Ogro (film) poster.jpg
| image_size     =
| caption        = Spanish DVD cover
| director       = Gillo Pontecorvo
| producer       = Franco Cristaldi José Sámano
| writer         = Giorgio Arlorio Ugo Pirro Gillo Pontecorvo
| narrator       =
| starring       = Gian Maria Volonté José Sacristán Ángela Molina Eusebio Poncela Ana Torrent
| music          = Ennio Morricone
| cinematography = Marcello Gatti
| editing        = Mario Morra
| distributor    =
| released       =  
| runtime        = 100 minutes
| country        = Spain Italy Spanish
| budget         =
}}
 1979 Spain|Spanish Italian drama film written and directed  by Gillo Pontecorvo. 

The film is based on true events, following the eponymous book by Julen Agirre (pseudonym of Eva Forest). 

The film won the David di Donatello (an annual Italian motion picture award) for Best Film. 

All the actors, a mix of Spaniards and Italians, spoke  Spanish in the dubbed version released in Spain, and in Italian in the dubbed Italian version.

==Plot==
 

==Cast==
*Gian Maria Volonté: Izarra
*José Sacristán: Iker
*Ángela Molina: Amaiur
*Eusebio Poncela: Txabi
*Saverio Marconi: Luque
*Georges Staquet: The builder
*Nicole Garcia: Karmele
*Féodor Atkine: José María Uriarte (alias Yoseba)
*Estanis González	
* 
*José Manuel Cervino: The milkman
*Ana Torrent: Basque girl

==History==
Operación Ogro ("Operation Ogre") was the name given by ETA to the assassination of Luis Carrero Blanco, the then Prime Minister of Spain in 1973 and the successor of Francisco Franco, the then Spanish dictator. This attack was carried out on 20 December 1973.

An ETA commando group using the code name Txikia (after the nom de guerre of ETA activist Eustakio Mendizabal killed by the Guardia Civil in April 1973) rented a basement flat at Calle Claudio Coello 104, Madrid on the route over which Carrero Blanco used to go to Mass at San Francisco de Borja church.

Over five months, this group dug a tunnel under the street - telling the landlord that they were student sculptors to disguise their real purpose. The tunnel was packed with 80&nbsp;kg of explosives that had been stolen from a Government depot.
 Dodge Dart passed. The explosion sent Carrero Blanco and his car 20 metres into the air and over a five-storey building. The car crashed down to the ground on the opposite side of a Jesuit college, landing on the second-floor balcony. Carrero Blanco survived the blast but died shortly afterwards. His bodyguard and driver were killed instantly. The "electricians" shouted to stunned passers-by that there had been a gas explosion, and subsequently escaped in the confusion.

== Quotes ==
José María Uriarte: 
 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 