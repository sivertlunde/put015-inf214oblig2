Jor Jar Muluk Tar
 
{{Infobox film
| name           = Jor Jar Muluk Tar
| image          =
| caption        =
| director       = Haranath Chakraborty
| producer       = Rajib Bhadra Sandip Das Shaan Akshat Rajesh Sharma Kanchan Mullick Laboni Sarkar Sumit Gangopadhyay Arun Bandyopadhyay 
| lyrics         =  Goutam Susmit
| story          = Somnath Bhattacharya
| screenplay     = Somnath Bhattacharya
| editor         = Swapan Guha
| choreographer  = James Anthony
| studio         = 
| released       =  
| runtime        = 
| music          = Ashok Raj
| genere         = Drama,Suspense
| country        = India Bengali
| awards         =
| budget         =
}}
 Bengali film Rajesh Sharma, Kanchan Mullick, Laboni Sarkar and others. The film has been music composed by Ashok Raj.

==Cast==
* Prosenjit Chatterjee as Bikram Chatterjee
* Rudranil Ghosh
* Raaj as Rocky
* Sanchita Shaan as Shaan
* Arpita Mukherjee as Lisa Megha
* Akshat
* Rajatava Dutta as Rana Rajesh Sharma as Cop
* Kanchan Mullick
* Laboni Sarkar
* Sumit Gangopadhyay as Goon
* Arun Bandyopadhyay as M.L.A. 

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer (s) !! Duration !! Music !! Lyricist
|-
| 1
| Jor Jar Muluk Tar (Title Song)
| Sujoy Bhowmik
| 4:23
| Ashok Raj
| Goutam Susmit 
|-
| 2
| Moquabila
| Sujoy Bhowmik
| 4:10
| Ashok Raj
| Goutam Susmit
|-
| 3
| Bondhu Tindin
| Zubeen Garg
| 4:22
| Ashok Raj
| Goutam Susmit 
|-
| 4
| Jhum Barabar
| Sujoy Bhowmik,Anweshaa Dutta|Anweshaa,Joy
| 5:53
| Ashok Raj
| Goutam Susmit
|-
| 5
| Jago Re
| Sujoy Bhowmik,Joy
| 4:48
| Ashok Raj
| Goutam Susmit 
|-
|}

==References==
 

==External links==
*   at the Gomolo
*  
*  
*  
*  
*  

 

 
 
 
 
 

 