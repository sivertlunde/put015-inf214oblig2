Shalom (film)
{{Infobox Film 
| name         = Shalom (Wayfarers Prayer)
| image        = SHALOMPOSTER.jpg 
| writer       = Yaky Yosha
| starring     = Yaky Yosha Dorit Yosha Irit Mohar Nisim Azikary Israel Gurion
| director     = Yaky Yosha
| producer     = Dorit Yosha Orgad Vardimon 
| movie music  = Yaky Yosha]
| distributor  = Yaky Yosha Ltd. 
| released     = 1973 (Israel) 
| runtime      = 80 minutes
| country      = Israel
| language     = Hebrew 
| budget       = 
| music        = 
}}
 Yaky Yoshas first feature. 
It attempts to answer the dilemmas and distresses youth in the pre Yom Kippur War Israel.

== Plot ==
Shalom, a young Israeli at the outset of his life, was born into a bourgeois family in Tel-Aviv. His parents wish he would go to college, but Shalom doesnt feel like studying. His father isnt quite convinced his beloved son is doing the best he can when not doing anything at all.
Shalom has a dilapidated station-wagon, two girlfriends to ride and love and "dont think twice, its all right", as one of them sings to her sweet babbling infant. Shalom goes out on the road looking for his own self – an Israeli easy rider. In his wandering he comes across a group of artists debating over Israels social-political fate. War and peace, occupied territories and settlements, rich and poor. One thing is not up for discussion, the future doesnt seem bright.
Shalom makes up his mind to leave it all and go to United States|America. Makes up his mind, but stays.

== External links ==
*  

== References ==
* The content of this article was translated from   (Shalom) in the Hebrew-language Wikipedia, acknowledged here under terms of the GNU Free Documentation License.
 

 
 
 
 
 
 


 