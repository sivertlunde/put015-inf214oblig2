Jetsam (film)
 
{{Infobox Film
| name           = Jetsam
| image          =Jetsam (2008).jpg
| image_size     = 
| caption        = Some things are washed up for a reason
| director       = Simon Welsford
| producer       = Simon Welsford
| writer         = Simon Welsford
| narrator       =  Alex Reid Shauna Macdonald Jamie Draven Cal Macaninch Adam Shaw
| music          = Mat Davidson
| cinematography = Zac Nicholson
| editing        = Ned Baker
| distributor    = Jinga Film
| released       = 
| runtime        = 84 minutes
| country        = United Kingdom
| language       = English
| budget         = £3,000 estimated
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Alex Reid, Shauna Macdonald and Jamie Draven.  The two-week shoot wrapped on
27 May 2006, and pre-production was completed nearly a year later, on 8 March 2007. The film premiered at the 51st London International Film Festival, and received many positive notices. It has also received acclaim at the Slamdance Film Festival. It was finally granted a national release in the UK in August 2009.

== Plot ==
Grace (Alex Reid) is washed up on a beach along with a man (Jamie Draven) whom she cannot recollect.  But since it becomes quickly apparent that the man means to kill her, Grace is forced to resort to extreme measures to stay alive while putting her memory back together.

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 


 