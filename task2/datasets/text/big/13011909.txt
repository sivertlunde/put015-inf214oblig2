The Scoundrel (1988 film)
{{Infobox film name = The Scoundrel image =  image_size =  caption =  director = Vagif Mustafayev producer = Azerbaijanfilm writer = Ramiz Fataliyev Vagif Mustafayev narrator =  starring = Mamuka Kikaleishvili Hasanagha Turabov Yashar Nuri Rasim Balayev music =  cinematography =  editing = G. Zhuravlyova distributor =  released = 1988 runtime = 97 min. country = Soviet Union language = Russian
|budget =  gross =  preceded_by =  followed_by = 
}} Soviet bureaucracy Azerbaijani adult Georgian actor Mamuka Kikaleishvili.

==Cast==
* Mamuka Kikaleishvili - Hatem
* Hasanagha Turabov - Gazanfar
* Yashar Nuri - Mashalla
* Larisa Borodina - Irada
* Aghahuseyn Kerimov - Mirza Baba
* Hamlet Khanizade - Alasgar
* Melik Dadashov - Hatems co-worker
* Zernigar Aghakishiyeva - mother-in-law
* Haji Ismayilov - Haji
* Rasim Balayev - ministers assistant
* Saida Guliyeva - Hatems wife
* Mukhtar Maniyev - thief
* Eldaniz Zeynalov - Hatems co-worker
* Nazim Aghayev - Mukhtar
* Lala Baghirova - secretary
* Brilliant Dadashova - neighbor
* Talat Rahmanov - singer Balayan
* Jahangir Aslanoghlu - Bahram
* Chingiz Mustafayev - homeless
* Ramiz Melikov - dancer
* Basti Jafarova
* Huseyn Muradov - minister

==See also==
*Azerbaijani films of the 1980s

== External links ==
* 

 
 
 
 
 
 
 
 


 
 