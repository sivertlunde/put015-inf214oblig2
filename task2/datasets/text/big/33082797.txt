Da grande (film)
{{Infobox film
| name           = Da grande
| image          = Da grande (film).jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Franco Amurri
| producer       = Achille Manzotti
| writer         = Franco Amurri Stefano Sudriè
| starring       = Renato Pozzetto Giulia Boschi Ottavia Piccolo Alessandro Haber
| music          = Pino Massara
| cinematography = Luciano Tovoli
| editing        = Raimondo Crociani
| studio         = 
| distributor    = 
| released       =  
| runtime        = 94 minutes Italy
| Italian
| budget         = 
| gross          = 
}} Italian romantic comedy film directed by Franco Amurri, starring Renato Pozzetto, Ottavia Piccolo and Alessandro Haber.   

Da grande is one of the coming-of-age comedies that were released in the late 1980s,   and was the inspiration to Big (film)|Big,  the 1988 international blockbuster directed by Penny Marshall and starring Tom Hanks.

== Plot ==

Eight-year-old Marco Marinelli is a bedwetter, scolded by his mother (Ottavia Piccolo) and teased by his classmates. On his birthday he finds out that his father (Alessandro Haber), who is facing economic difficulties, hasnt brought him the Lego he was promised. He runs in tears to his room and puts all his heart into wishing he were big and not subject to these indignities. As a result, he bursts through his clothes in the guise of a forty-year-old man (Renato Pozzetto) and seeks refuge in the house of his schoolteacher (Giulia Boschi), who hes secretly in love with. Mentally, he is still eight years old, and its a puzzle what to do with him, until someone discovers that he has an uncanny rapport with children. Then he becomes a full-time and highly requested babysitter, but shortly after he is suspected of abducting the by-now long-missing child Marco. He then runs out of money, and fakes the kidnapping of himself, but while chased by the police he eventually turns back into the eight-year-old boy.

== Cast ==
*Renato Pozzetto as grown-up Marco
*Joska Versari as normal Marco
*Giulia Boschi as Francesca (Marcos teacher)
*Alessandro Haber as Claudio (Marcos father)
*Ottavia Piccolo as Anna (Marcos mother)
*Alessandro Partexano as the police detective
*Giampiero Bianchi as Nicola (Marcos uncle)
*Gaia Piras as Silvietta (Marcos sister)
*Ilary Blasi as the girl playing hide-and-seek

== Reception ==
Da grande won the Nastro dargento for best story (Silver Ribbon), the oldest movie award in Europe, assigned since 1946 by Sindacato Nazionale dei Giornalisti Cinematografici Italiani (the association of Italian film critics).  

== References ==
 
 

== External links ==
*  
*  
*  

 

 
 
 
 
 