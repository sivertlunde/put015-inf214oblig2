Dwandha Yudham
{{Infobox film 
| name           = Dwandha Yudham
| image          =
| caption        =
| director       = CV Hariharan
| producer       = 
| writer         = CV Hariharan
| screenplay     = CV Hariharan
| starring       = Adoor Bhasi Kuthiravattam Pappu Sukumari Jagathy Sreekumar
| music          = Jerry Amaldev
| cinematography =
| editing        =
| studio         = Suguna Screen
| distributor    = Suguna Screen
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by CV Hariharan. The film stars Adoor Bhasi, Kuthiravattam Pappu, Sukumari and Jagathy Sreekumar in lead roles. The film had musical score by Jerry Amaldev.   

==Cast==
*Adoor Bhasi
*Kuthiravattam Pappu
*Sukumari
*Jagathy Sreekumar
*Manavalan Joseph
*Alummoodan
*Jayamalini Kunchan
*Nagesh
*Poojappura Ravi

==Soundtrack==
The music was composed by Jerry Amaldev and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Kali Theekkali || K. J. Yesudas || P. Bhaskaran || 
|-
| 2 || Kadikkaan pattatha || K. J. Yesudas, MG Radhakrishnan, Junior Mehboob, Omanakkutty || P. Bhaskaran || 
|-
| 3 || Parippuvada thiruppan || K. J. Yesudas, P Jayachandran || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 