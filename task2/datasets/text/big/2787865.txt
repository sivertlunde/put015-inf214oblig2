El Intruso (1999 film)
{{Infobox film
| name           = El Intruso
| image          =
| caption        =
| director       = Guillermo Álvarez
| producer       = Guillermo Álvarez
| writer         = Guillermo Álvarez
| starring       = Guillermo Álvarez Luz Ángela Bermúdez Germán Torres
| music          =
| cinematography = Pedro Salamanca
| editing        = Guillermo Álvarez
| distributor    =
| released       =  
| runtime        =
| country        = Colombia Spanish
| budget         =
}}

El Intruso is a 1999 Colombian film that was produced, written, directed by, and starred Guillermo Álvarez. The plot revolves around a couple who move to a small village, and are then investigated after the discovery of the body of the womans lover.

==Cast==

*Luz Ángela Bermúdez - Lucila 
*Germán Torres Rey - Rumaldo 
*Guillermo Álvarez - Inspector

==Awards==

Bogotá Film Festival (1999) 

*Honorable Mention for Guillermo Álvarez
*Nominated for the Golden Precolumbian Circle Best Colombian Film

== External links ==
*  

 
 
 
 
 
 


 
 