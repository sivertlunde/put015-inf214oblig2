The Giant Buddhas
 
{{Infobox film
| name = The Giant Buddhas
| image = Giant Buddhas.jpg
| starring = Taysir Alony   Sayeed Mirza Hussain   Nelofer Pazira   Zémaryalaï Tarzi   Xuanzang
| director = Christian Frei
| country = Switzerland 
| producer = Christian Frei
| released =  
| runtime = 95 minutes
| language = Arabic, Dari, English, French and Mandarin 
| music = Jan Garbarek   Philip Glass   Steve Kuhn   Arvo Pärt
| cinematography = Peter Indergand
}}
The Giant Buddhas is a documentary film by Swiss filmmaker Christian Frei about the destruction of the Buddhas of Bamyan in Afghanistan. It was released in March 2006. The movie film quotes local Afghans that the destruction was ordered by Osama Bin Laden and that initially, Mullah Omar and the Afghans in Bamyan had opposed the destruction. 

==Awards and nominations==
*DOK Leipzig 2005: Silver Dove
*Dokufest Prizren 2006: first prize ex aequo
*Trento Film Festival 2006: Silver Gentian
*Tahoe/Reno International Film Festival 2006: Best of the Fest - Documentary
*Sundance Film Festival 2006: nominated for a Grand Jury Prize for World cinema-documentaries 
*Swiss Film Prize 2006: nominated for the Swiss Film Prize for Best Documentary
*Toronto International Film Festival 2005: Official Selection

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 
 