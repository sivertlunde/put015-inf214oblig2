Hawk of the Hills (1927 serial)
 
{{Infobox film
| name           = Hawk of the Hills
| image          = 
| caption        = 
| director       = Spencer Gordon Bennet
| producer       = 
| writer         = George Arthur Gray Walter Miller
| cinematography = Frank Redman Edward Snyder
| editing        = 
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 10 episodes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 Western film serial directed by Spencer Gordon Bennet.   

==Cast==
* Allene Ray - Mary Walter Miller - Laramie
* James Robert Chandler - Clyde Selby
* Jack Ganzhorn - Henry Selby
* Frank Lackteen - The Hawk
* Paul Panzer - Manson
* Wally Oettel - Shorty
* Harry Semels - Sheckard
* Jack Pratt - Colonel Jennings
* J. Parks Jones - Lieutenant MacCready
* Frederick Dana - Larry
* John T. Prince - The Hermit
* Whitehorse (as Chief Whitehorse)
* George Magrill
* Evangeline Russell - Indian Maiden
* Chief Yowlachie - Chief Long Hand

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


  
 