Don Quixote de la Mancha (1947 film)
{{Infobox film
| name           = Don Quixote de la Mancha
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Rafael Gil
| producer       = Juan Manuel de Rada	
| writer         = 
| screenplay     = Rafael Gil
| story          = Miguel de Cervantes Saavedra
| based on       =  
| narrator       = 
| starring       = Rafael Rivelles   Juan Calvo   Fernando Rey   Sara Montiel
| music          = Ernesto Halffter
| cinematography = Alfredo Fraile
| editing        = Juan Serra
| studio         = Compañía Industrial Film Español S.A.
| distributor    = 
| released       =  
| runtime        = 137 min   USA: 107 min
| country        = Spain
| language       = Spanish
| budget         = 
| gross          = ESP 769,579 (Spain)
}} the great 1933 version and the later Russian film version, which scrambled up the order of the adventures as many film versions do. Characters such as Cardenio (Don Quixote)|Cardenio, Dorotea, and Don Fernando, which are usually omitted because their respective subplots have little to do with the main body of the novel, were kept in this film. http://www.imdb.com/title/tt0039330/ 

The film, which starred Rafael Rivelles as Don Quixote and Juan Calvo as Sancho Panza, featured a young Fernando Rey as Sanson Carrasco and popular Spanish actress Sara Montiel as Antonia, Quixotes niece. The music for the film was composed by Ernesto Halffter, and the movie was shot on location in La Mancha and other Spanish regions. 

It did not fare as well in the United States, where it opened in 1949, as it had in Spain. In its American runs, it was whittled down to a more customary length of 107 minutes.  

==References==
  
 
 
 
 
 
 

 