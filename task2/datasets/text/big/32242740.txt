The Adventures of Pureza: Queen of the Riles
{{Infobox film
| name           = The Adventures of Pureza
| image          = Theadventuresofpureza.png
| caption        = Theatrical movie poster
| director       =  Soxie H. Topacio
| producer       = Enrico Santos Charo Santos-Concio Malou Santos
| writer         = Gina Marissa Tagasa
| story          = Enrico Santos
| starring       =  
| music          = Vincent de Jesus
| cinematography = Mackie Galvez
| editing        = Beng Bandong
| studio         = Sine Screen
| distributor    = Star Cinema
| released       =  
| runtime        = 105 minutes
| country        = Philippines
| language       =  
| budget         =
| gross          = P19,844,817   retrieved on July 27, 2011 
}}
The Adventures of Pureza: Queen of the Riles  is a 2011 Filipino comedy film starring Melai Cantiveros as Pureza. The film was produced by Sine Screen and Star Cinema.  The film a had limited release on July 13, 2011.

==Plot==
A poor girl, by the name of Pura who lives near the rails of the train experiences fun, adventure, and romance, in the greatest adventure of her life. Together with her friend, Ruben, they try to find a model, by the name of Daniella Fabella Dela Bamba. If they did not succeed, they will be killed by the evil Mother Baby/Mother Greedy, and the Tsinelas gang. In search, Pura meets a boy by the name of Gerald, and falls in love with him. However, Gerald does not return her feelings, because he is in love with Daniella who is pregnant. Pura finds her mother, Purisima, who currently is a nun in a cathedral. Her mother helps her escape Mother Babys guards. Sooner, Pura also learns that her best friend, Ruben, is in love with her, and falls in love with him in the process. When Mother Baby is defeated, she and Ruben get married, in the Train Station.

==Cast==
*Melai Cantiveros    as Pura Buraot/Pureza Mayriles/Sor. Eyes
*Jason Francisco  as Ruben Padilla/Sor. Throat
*Joem Bascon as Gerald Tanderson
*Martin del Rosario as Ulam Buraot/ Ulysses Buraot
*Bianca Manalo as Daniella Fabella de la Bamba
*Gina Pareno as Mother Baby/Mother Greedy
*Nico Antonio as Hipon
*Bekimon as Zeppy
*Bella Flores as Sr.Pepa Papparazzi
*Bentong as mother babys guard
*Gerard Acao as mother babys guard
*Pokwang as Sr.Purisima

==Reception==
The film garnered P19,844,817 in its 4-week of showing.  It was in direct competition against Harry Potter and the Deathly Hallows – Part 2, in the Philippine box office.    

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 
 
 
 
 
 