Missed Call (film)
 
{{Infobox film 
| name     = Missed Call 
| image    =
| director = Mridul Toolsidas & Vinay Subramaniam
| starring = Ankur Vikal Heeba Shah Ram Kapoor Salim Ghouse
| released =  
| country  = India
| language = Hindi
}}

Missed Call is a 2005 Indian Hinglish film produced by Reelism Films. The film was shot on 16mm in just a matter of 15 days on shoe-string budget. Shot in cinema verite style with on location sync- sound, it went on to win many prestigious awards including the Opening Film at Indian Panorama 2005 and representing India in Cannes 2006. Missed Call till date has been screened at 12 international film and in 2008 it received  the distinction of being adjudged the Best International Film at Israel Film Festival.

Missed Call is a poignant tribute to the passionate young men and women who dream of putting their stories on celluloid. Missed call was perhaps the first ever debut film from India to get the rare distinction of being selected to represent the country at the Cannes International Film Festival (2007), along with the masterpieces like The Guru and Lage Raho Munnabhai. Besides being selected to open the International Film Festival, Goa in 2006, the film was adjudicated the Best International film at Israel’s Red Sea International Film Festival in 2008. The film has also been selected by Films Division, India under the honour list of “Best of Indian Cinema.”

== Plot ==
The protagonist, a 20-something Gaurav Sengupta (Ankur Vikal) is obsessed with the idea of capturing every moment in life on celluloid and narrates the story as seen through his eyes. It is his journey from joy to despair, from hope to frustration and from love to lust.

== Production ==
The movie was produced by Reelism Films and was their debut production. The movie was directed by the director duo, Mridul Toolsidas and Vinay Subramanian.

== Cast ==
* Ankur Vikal as Gaurav Sengpupta
* Salim Ghouse as Arindam Kumar Sengputa 
* Heeba Shah as Gayatri
* Ram Kapoor as Vinay Murthy
* Tinnu Anand as DK Bose
* Seema Rahmani as Rose

== Reviews ==
Strikingly original and darkly humorous, MISSED CALL is told in a captivating, vérité style that makes this directorial debut a must see for independent film aficionados worldwide. It boasts delightfully realistic performances and witty writing that capture the passion, pain and promise of urban youth navigating the rocky road toward adulthood. - Indian Film Festival  

"Missed Call" is a cutting example of niche cinema getting as close to the urban reality about the average bourgeois youngster as a camera can possibly take the audience. But at the end of the day the sense of aimlessness that overwhelms Gaurav Sengupta is much too familiar to connect with the audience as anything but an odd film about a square among circles. - WebIndia 123 

"The film is about a filmmaker, his fight, his defeat, his eccentricities, his creativity and a burning passion for the medium. The directors sure deserve kudos because its a story that needs to be told." - Times of India, Hyderabad

==References==
 

== External links ==
#  
#  
#  
#  . Business Wire India
#  . IBN Live
#  . "Hindustan Times"
#  . "Business of Cinema"
#   "The Hindu"
#   "Deccan Herald"
#   "One India"
#   "The Telegraph"

 
 


 