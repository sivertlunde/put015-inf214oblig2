The Painting Pool
 
{{Infobox film
| name           = The Painting Pool
| image          = The Painting Pool.jpg
| border         = yes
| caption        = 
| director       = Maziar Miri
| producer       = Manouchehr Mohammadi
| writer         = Hamed Mohammadi
| starring       = Shahab Hosseini   Negar Javaherian   Fereshteh Sadre Orafaee   Sepehrad Farzami   Siamak Ehsaei   Elham Korda
| music          = 
| cinematography = 
| editing        = 
| distributor    = Iranian Independents
| released       =  
| runtime        = 90 minutes
| country        = Iran
| language       = Persian
| budget         = 
}}
The Painting Pool ( , Transliteration|translit.&nbsp;Hoze Naghashi) is a 2013 Iranian drama film written and directed by Maziar Miri.  The film released on March 13, 2013 in Tehran and centers upon the interpersonal dynamics between a set of mentally challenged parents and their child.  Iran reviewed the movie as a potential selection for contention for a 2014 Academy Award for Best Foreign Language Film,  but did not select the film.

==Synopsis==
Maryam (Negar Javaherian) and Reza (Shahab Hosseini) try to do their best in life, but are met with several obstacles due to being mentally challenged. Their son Soheil was born without any defects and initially does not realize how his parents differ from other adults until he begins to grow older. This leads to complications when his teachers request that his mother come to school for a parent-teacher conference, as Soheil is afraid that his parents will humiliate him. As a result Soheil spends more and more time with Amirali, his teachers son, and eventually moves in with them after his father loses his job.

==Cast==
*Shahab Hosseini as Reza
*Negar Javaherian as Maryam
*Fereshteh Sadre Orafaiy
*Sepehrad Farzami
*Siamak Ehsaei
*Elham Korda

==Reception==
Serat News reviewed the film. 

===Awards===
*Audience Award Best Film by Audiences at the Fajr Film Festival (2013, won)  
*Best Set & Costume Design at the Fajr Film Festival (2013, won)   
*Best Actress in a Leading Role at the Fajr Film Festival (2013, nominated - Negar Javaherian)
*Best Cinematography at the Fajr Film Festival (2013, nominated)
*UNESCO Award at the Asia Pacific Screen Awards (2013, won)    
*Best Performance by an Actress at the Asia Pacific Screen Awards (2013, nominated - Negar Javaherian) 

==References==
 

==External links==
*  
*  

 
 
 
 
 