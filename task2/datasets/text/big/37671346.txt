Admission (2013 film)
{{Infobox film
| name           = Admission
| image          = Admission movie poster.jpg
| image_size     = 220px
| alt            = 
| caption        = Theatrical release poster Paul Weitz
| producer       = Paul Weitz Kerry Kohansky-Roberts Andrew Miano
| screenplay     = Karen Croner
| based on       =  
| starring       = {{Plainlist|
* Tina Fey
* Paul Rudd
}}
| music          = Stephen Trask
| cinematography = Declan Quinn
| editing        = Joan Sobel
| studio         = Depth of Field
| distributor    = Focus Features
| released       =  
| runtime        = 108 minutes  
| country        = United States
| language       = English
| budget         = $13 million   
| gross          = $18,007,317 
}} romantic comedy-drama Paul Weitz and starring Tina Fey and Paul Rudd. The film was released in the United States and Canada on March 22, 2013.     It is an adaptation of a novel by Jean Hanff Korelitz, also called Admission.

==Plot== Admissions Officer Portia Nathan (Tina Fey) is caught off guard when she makes a recruiting visit to an alternative high school overseen by a former college classmate, the free-wheeling John Pressman (Paul Rudd). With vast experience in the coaching, consoling, and criticism involving Princetons admission, she pays a visit to the Quest School, where John teaches while raising an adopted son. After exposing Portia to outspoken Quest students impressions of college, he takes her to meet the rather unconventional Jeremiah Balakian, a child prodigy.

Back on campus, Portias longtime boyfriend Mark breaks up with her after impregnating a "Virginia Woolf scholar" named Helen. After an awkward romantic attraction to Pressman, she arranges for Jeremiah to visit Princeton, where she and a colleague, Corinne, are rivals to succeed the soon-to-retire Dean of Admissions.
 transcript results in his being deemed unfit to attend the University. Portia, in an act that greatly endangers her position, schemes to gain Jeremiah entrance into the school, knowing that Princeton cannot reveal such a scandal.
 biological mother. Portia appears at the Adoption Agency, trying to locate her son, where she describes her life with a different perspective. When asked how would she feel to meet her actual child, she replies that she would feel "nervous, but lucky."

In the end, now dating Pressman, she receives a letter about her son, which says he is not ready to meet her yet. Pressman points out to her that she is on the Wait list, "... and thats not so bad".

==Cast==
* Tina Fey as Portia Nathan
* Paul Rudd as John Pressman
* Michael Sheen as Mark
* Lily Tomlin as Susannah
* Wallace Shawn as Clarence
* Nat Wolff as Jeremiah Balakian
* Gloria Reuben as Corinne
* Travaris Spears as Nelson
* Christopher Evan Welch as Brandt
* Sonya Walger as Helen
* Leigha Hancock as Yulia Karasov Daniel Levy as James
==Production== Paul Weitz, About a Boy, and based on the novel of the same name by Jean Hanff Korelitz. The film was shot at both the Princeton University campus and at Manhattanville College in Purchase, New York.    A trailer for the film was released on November 20, 2012.    The film was released on March 22, 2013. Admission was the first major motion picture to use RushTera for post production collaboration.

==Reception==
Admission received mixed reviews from critics.   gives an average score of 48% based on 39 reviews, indicating "mixed or average reviews." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 