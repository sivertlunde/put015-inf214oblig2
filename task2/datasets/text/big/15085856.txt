La Chamade (film)
{{Infobox film
| name        = La Chamade
| image       = La_Chamade_Poster.jpg
| caption     = Theatrical release poster
| director    = Alain Cavalier 		
| producer    = Maria Rosaria
| writer      = Alain Cavalier Françoise Sagan
| starring    = Catherine Deneuve Michel Piccoli Roger Van Hool
| music       = Maurice Leroux
| cinematography = Pierre Lhomme
| editing     = Pierre Gillette
| distributor =  Les Artistes Associés (United Artists) 
| released    =  
| runtime     = 103 minutes 
| country     = France Italy
| language    = French
| budget      = 
| gross       = $5,836,932 
}} romantic drama film written and directed by Alain Cavalier and starring Catherine Deneuve, Michel Piccoli, and Roger Van Hool. Based on the 1965 novel La Chamade by Françoise Sagan, the film is about a beautiful woman who is mistress to a rich, good-hearted businessman who provides for all her material needs, but for whom she has no true love. When she meets a charming young man her own age, she falls in love and soon becomes pregnant with his child. After Charles helps her through her crisis, her feelings for the young man gradually fades and she returns to the good-hearted businessman who has patiently waited for her.    La Chamade was filmed on location in Paris and Nice.   

==Plot==
A woman and a poor artist begin an affair and eventually move in together, but the woman cannot get used to his life, his working-class existence. She leaves her lover to return to her relationship with a man of means. Ostensibly, she is rejecting her lover because she feels stifled by his position in society. But the class differences are metaphor for the quality of the love, with a woman deciding to be with a man who loves her for who she is rather than as an object of affection, merely the focus of a selfish love. She wants to be with the one who doesnt ask her to change.

==Cast==
* Catherine Deneuve as Lucile
* Michel Piccoli as Charles
* Roger Van Hool as Antoine
* Amidou as Etienne Philippine Pascal as Claire
* Louis Rioton 
* Monique Lejeune
* Christiane Lasquin 
* Matt Carney
* Jacques Sereys as Johnny
* Irène Tunc as Diane
==Production==
The novel was a best seller. European Notebook: European Notebook
By MARC SLONIM. New York Times (1923-Current file)   21 Nov 1965: BR26.   Invitation To a Parley
By PATRICIA MacMANUS. New York Times (1923-Current file)   13 Nov 1966: 388.  

Filming took place in April 1968 and was interrupted by riots in Paris. Suzy Says: Only the Beginning
Chicago Tribune (1963-Current file)   30 Apr 1968: b1.  

==Reception==
Upon its theatrical release, La Chamade received generally positive reviews. In his review in The New York Times, Vincent Canby wrote, "Cavalier may have created a practically perfect screen equivalent of the novelists prose style."    In addition to praising the performances by Deneuve and Piccoli, Canby writes:
 

On the review aggregator web site Rotten Tomatoes, the film holds a 68% positive audience rating based on 66 ratings.   

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 

 