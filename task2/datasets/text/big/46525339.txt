The Mongrel
{{Infobox film
 | name = The Mongrel
 | image =  Locandina_The_Mongrel.jpg
 | caption =
 | director =  Alessandro Gassmann
 | writer =  Federico Schlatter
 | story = Alessandro Gassmann  Vittorio Moroni
 | starring = 
 | music =  Pivio and Aldo De Scalzi
 | cinematography =Fabrizio Lucci
 | editing =   
 | producer =  
 | released =  
 }} Italian Thriller (genre)|thriller-drama film.  It marked the directorial debut of actor  Alessandro Gassmann, who also co-wrote the script and starred in the film.

For his performance Gassmann won the Globo doro for best actor.  The film was nominated for two David di Donatello, for best new director and for best original song ("La vita possibile"), {{cite news|last=Anna Finos|title= Nastri dArgento Awards,  for best cinematography, best new director and best score. 

== Cast == 
* Alessandro Gassmann as  Roman
*  Giovanni Anzaldo as  Nicu
*  Manrico Gammarota as  Geco
*  Sergio Meogrossi as Talebano
*  Matteo Taranto as  Dragos
* Mădălina Diana Ghenea as  Dorina
* Michele Placido as  Avv. Silvestri
* Maiga Bailkissa as Lourdes
*  Carolina Facchinetti as  Chiara 
*  Nadia Rinaldi as Maddalena

==References==
 

==External links==
* 

  
 
 
   
 
 
 

 
 