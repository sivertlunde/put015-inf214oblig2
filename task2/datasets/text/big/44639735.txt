The Naked Cage
{{Infobox film
| name           = The Naked Cage
| image          = The Naked Cage poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Paul Nicholas 	
| producer       = Samuel Benedict Chris D. Nebe 
| writer         = Paul Nicholas 	
| starring       = Shari Shattuck Angel Tompkins Lucinda Crosby Stacey Shaffer Christina Whitaker Nick Benedict
| music          = Christopher L. Stone 
| cinematography = Hal Trussell 
| editing        = Warren Chadwick Anthony DiMarco 	
| studio         = The Cannon Group
| distributor    = Cannon Film Distributors
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $3,136,701 
}}

The Naked Cage is a 1986 American drama film written and directed by Paul Nicholas. The film stars Shari Shattuck, Angel Tompkins, Lucinda Crosby, Stacey Shaffer, Christina Whitaker and Nick Benedict. The film was released on March 7, 1986, by Cannon Film Distributors.  

==Plot==
 

== Cast ==
 
*Shari Shattuck as Michelle
*Angel Tompkins as Diane
*Lucinda Crosby as Rhonda
*Stacey Shaffer as Amy
*Christina Whitaker as Rita
*Nick Benedict as Smiley
*John Terlesky as Willy
*Faith Minton as Sheila
*Aude Charles as Brenda
*Angela Elayne Gibbs as Vonna 
*Carole Ita White as Trouble
*Lisa London as Abbey
*Leslie Scarborough as Peaches
*Valerie McIntosh as Ruby
*Larry Gelman as Doc
*Suzy London as Martha
*Flo Lawrence as Mother 
*James Ingersoll as Father
*Seth Kaufman as Randy William Bassett as Jordan
*Nora Niesen as Bigfoot
*Jennifer Anne Thomas as Mock
*Chris Anders as Miller
*Al Jones as Bartender 	
*Sheila MacRae as Bank Teller 
*Bob Saurman as Motorcycle Cop
*Rick Avery as Security Officer
*Christopher Doyle as Police Officer
*Gretchen Davis as Prison Guard
*Beryl Jones as Prison Guard
*Michael Kerr as Prison Guard
 

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 