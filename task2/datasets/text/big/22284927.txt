Bionicle: The Legend Reborn
 
{{Infobox film
| name           = BIONICLE: The Legend Reborn
| image          = Bionicle The Legend Reborn cover big.png
| image size     = 230px
| director       = Mark Baldo
| producer       = Kristy Scanlan Joshua Wexler
| writer         = Sean Catherine Derek
| story          = Greg Farshtey
| starring       = Michael Dorn Jim Cummings Marla Sokoloff David Leisure Mark Famiglietti James Arnold Taylor Armin Shimerman Fred Tatasciore
| music          = Cory Lerios  & John DAndrea
| cinematography =
| editing        = Aeolyn Kelley
| studio         = Tinseltown Toons Threshold Animation Studios The Lego Group
| distributor    = Kidtoon Films Universal Studios Home Entertainment
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
BIONICLE: The Legend Reborn is an animated film based on the  . This film was released by Universal rather than Miramax, who released the first the three Bionicle films.

The film stars Michael Dorn as the voice of Mata Nui in what was supposed to be a trilogy of films, but production schedules for the sequels were cancelled due to Lego discontinuing the release of Bionicle toys.

==Plot==
Mata Nui (Michael Dorn), the Great Spirit of the Matoran universe on Aqua Magna, has been exiled from his home by his "brother," the evil Makuta Teridax. Teridax took over his gigantic robot body just after Mata Nuis reawakening and trapped the latters spirit inside the legendary Kanohi Mask of Life. Teridax then banished the mask into orbit, in order to prevent Mata Nui from interfering with his takeover of the Matoran Universe.

The mask crash lands on a planet called Bara Magna, a remote, decaying wasteland of scrap parts and burnished metals. A Scarabax beetle watches as the Mask of Life creates a body for Mata Nui, who almost steps on the Scarabax. He picks up the Scarabax, who touches his mask. Upon doing so, the beetle changes into a living weapon. Just then, a being called a Vorox attacks Mata Nui and after a struggle, flees without its stinger, which has broken off. Mata Nui picks up the stinger as a vehicle carrying a villager named Metus (David Leisure) speeds towards him.

The fast-talking Agori recruiter takes Mata Nui to Vulcanus as he tells the once ruler about life on Bara Magna. Local villages scavenge for what remains, building shelters, survival gear and ultimately arenas where they can settle their disputes. Putting the best Glatorian from each village against one another, tribes can quickly end quarrels based on the outcome of the fight. Metus introduces Mata Nui to Raanu (Armin Shimerman), the Agori leader of Vulcanus, who is too interested in the fight between veteran fighter Strakk (Jeff Glen Bennett) and Vulcanus prime Glatorian, Ackar (Jim Cummings), to talk. Strakk eventually falls in battle, but gets up and fells Ackar after conceding, a move that gets him banned from the fights. Mata Nui intervenes and gets beaten down by the angry Strakk. As Mata Nui tries to use the stinger to defend himself, the mask transforms it into a sword. Mata Nui quickly defeated Strakk, gaining Ackar as a friend in the process.

Metus later attempts to recruit Mata Nui, who refuses. Ackar introduces the hero to Kiina (Marla Sokoloff), a feisty Glatorian from the village of Tajun. In exchange for showing the two Glatorians a secret cavern underneath Tajun, Kiina would like to leave Bara Magna with Mata Nui, believing that anywhere is better than her native planet. On their way to Tajun, the three Glatorians are ambushed by a Skopio beast in Sandray Canyon, along with a group of Bone Hunters, who were told the Glatorians location by an Agori traitor. After trapping both beneath a rockslide, they discover that Tajun had been raided by the combined efforts of the formidable Skrall army and the Bone Hunters, despite the fact that the two forces were rival tribes. A Glatorian rookie, Gresh (Mark Famiglietti), had been trying to defend the village and was badly injured. The heroes follow Kiina to the caverns, where they encounter Berix (James Arnold Taylor), a thief. While Berix tends to Gresh, the other three discover a hidden chamber containing a picture of the robot that was once Mata Nuis body.

As the Glatorians leave, Gresh complains about his weapon being damaged, then Ackar wonders if what Mata Nui did with Click (the beetle Mata Nui befriended) and the Vorox tail would work with the Glatorian weapons. As his mask seems to work on things that are/were alive and learning from Berix that bone or claw is mostly used to make such weapons, Mata Nui successfully repairs and transforms the weapons. In addition, these weapons grant the Glatorians elemental abilities of fire, water, and air for Ackar, Kiina, and Gresh, respectively. The three practice their newfound powers as they travel to Greshs village, Tesara, while Ackar teaches Mata Nui to stay alert in battle and to study and find his opponents weakness. To the protests of Raanu and Metus, the four Glatorians stop a fight between Vastus (James Arnold Taylor) and Tarix (Jeff Glen Bennett), telling the crowd of Agori that they must unite their villages against the threat of the Skrall—Bone Hunter alliance. Mata Nui proves his worth to the crowd by transforming Tarixs and Vastus weapons.

In the hot springs not far from Tesara, Kiina follows a mysterious figure. It turns out to be Berix. Kiina is now convinced that Berix is the traitor. But then, the actual traitor corrects Kiina. The two are then captured by the Bone Hunters and the true traitor. Back in Tesara, Raanu and Metus alert the Glatorians about Kiina and Berixs kidnapping. Mata Nui ignores Ackars and Greshs offers for assistance and sets off for the Skrall camp in Roxtus on his own.

Kiina and Berix are held in a suspended cage near the mouth of a cave at Roxtus, both arguing about the formers caverns. Mata Nui confronts Tuma, the Skrall leader and challenges him to a one-on-one fight, and eventually defeats him. As he claims Tumas shield in victory and frees Kiina and Berix, the traitor, now revealed to be Metus, appears. He explains how he got the nomads and the Skrall to unite under him since he felt the Agori never gave him any respect, and that he will have taken over Bara Magna before the Glatorians realize it. Now in control of the army, he orders the Skrall and Bone Hunters to kill off the three of them. Just then, the Skrall and Bone Hunters end up running for their lives when they are attacked by an immense being composed of Scarabax beetles. Mata Nui gives a Skrall shield to Berix who, after landing on a Rock Steed, unintentionally defeats several Bone Hunters.  

As the Glatorians fight off the enemy forces, Mata Nui sees Metus fleeing in his vehicle and follows him. A couple of unchained and vengeful Vorox overturn Metus chariot, sending him tumbling to the feet of Mata Nui. Mata Nui picks him up and, despite Metus attempt to bargain with him, presses the traitor to his mask, transforming Metus into a snake as a representation of what he truly is. As he slithered away, he claims that his united army cannot be defeated. Realizing this to be the key to winning, Mata Nui has the Glatorians combine their powers against the waves of Skrall and Bone Hunters. As the defeated army flees, the Glatorians rejoice, except for Kiina, who believes Berix to be killed in battle. She quickly finds him alive under a pile of rocks holding a Skrall shield.

As the Glatorians and Berix watch the combined efforts of the Agori and the Scarabax beetles pulling the villages together, they notice that the combined villages formed a large robotic body, similar to the one Mata Nuis spirit once inhabited. To further this, Berix pulls out a coin with the Unity-Duty-Destiny symbol, the Bara Magna symbol, and the Mask of Life symbol on one side, and the Skrall symbol on the other. Berix shows them the Skrall shield and acknowledges that they are both similar to each other and that the mazelike symbol forms a map. With this information in hand, Ackar, Kiina, Gresh, and Berix prepare to set off for their next adventure with a legend. Reborn.

==DVD extras==
* Extended ending ("Metuss Revenge")
* Deleted scenes
* "Bye Bye Babylon" music video by Cryoshell
* Character gallery
* Commercials

==Music==
There are two licensed songs used in the ending credits of the movie:

*"Ride" - Presence Cryoshell

==Cast==
* Michael Dorn as Mata Nui
* Jim Cummings as Ackar
* Marla Sokoloff as Kiina
* James Arnold Taylor as Berix & Vastus
* Mark Famiglietti as Gresh
* David Leisure as Metus
* Armin Shimerman as Raanu
* Fred Tatasciore as Tuma Jeff Glen Bennett as Strakk & Tarix
* Dee Bradley Baker as Bone Hunters, Skrall & Vorox
* Mark Baldo as Villagers

==Production==
Unlike the first three films, The Legend Reborn was produced by Threshold Animation Studios and distributed by Universal Home Entertainment, while the original trilogy was produced by Miramax.

After reading the script the director, Mark Baldo thought of casting Michael Dorn of Star Trek for the voice of Mata Nui. 

The idea to make Click, Mata Nuis companion Scarabax beetle, have the ability to turn into a shield for his master at will was the idea of director Mark Baldo. It was not originally in the script, Farshtey thought it was a cute idea so he approved it.

The film was to be the beginning of a new Bionicle trilogy, and the writers were working on a draft for a sequel, but the Lego company cancelled the toyline and hence the movies.

==Reception==
BIONICLE: The Legend Reborn has received mixed to positive reactions, but amongst fans of Bionicle, the film was given mostly mixed reactions. 

While many fans praised the voice acting, visuals, and action sequence, they also criticized the film for its childlike tone, compared to the      , which each had a more dark and almost sombre tone.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 