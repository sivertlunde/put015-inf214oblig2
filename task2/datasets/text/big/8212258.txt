Shinjuku Triad Society
{{Infobox film
| name           = Shinjuku Triad Society
| image          = ShinjukuTriadSocietyPALCover.jpg
| caption        = Shinjuku Triad Society PAL DVD cover
| imdb_id        =
| producer       = Tetsuya Ikeda Toshiki Kimura Ken Takeuchi Tsutomu Tsuchikawa
| director       = Takashi Miike
| writer         = Ichirô Fujita
| starring       =
| music          = Atorie Shira
| cinematography = Naosuke Imaizumi
| editing        = Yasushi Shimamura
| released       = 1995
| runtime        = 100 min. Japanese
}}
 Ley Lines.

==Plot== triad group with a police officer as well as opposing yakuza organizations. When the younger brother to a renegade police officer becomes the lawyer to the triad group, an argument between the two leads to the downfall of the organization.

== Cast ==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- 
| Kippei Shiina || Kiriya
|-
| Tomorowo Taguchi || Wang
|-
| Takeshi Caesar || Karino
|-
| Ren Osugi || Yakuza boss
|-
| Yukie Itou ||
|- 
| Kyosuke Izutsu ||
|-
| Kazuhiro Mashiko ||
|-
| Airi Yanagi ||
|}

==External links==
*  

 

 
 
 
 
 
 

 