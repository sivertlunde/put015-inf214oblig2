Tepeyac (film)
{{Infobox film
| name           = Tepeyac 
| image          = 
| alt            =  
| caption        = 
| director       = José Manuel Ramos   Carlos E. Gonzáles   Fernando Sáyago
| producer       = Enrique Rosas
| writer         = José Manuel Ramos   Carlos E. Gonzáles
| starring       = *  
*  
* Pilar Cotta: Lupita Flores
* Fray Juan de Zumárraga
* Fray Bernardino de Sahagún
| music          = 
| cinematography = 
| editing        = 
| studio         =  Colonial
| distributor    = 
| released       =   
| runtime        = 64 min
| country        = Mexico
| language       = 
| budget         = 
| gross          = 
}}

Tepeyac  ( ,  Carlos E. Gonzáles and  Fernando Sáyago.  rescued by Aurelio de los Reyes and restored by National Autonomous University of Mexico.

==Synopsis==
Lupita Flores knows her fiancé Carlos Fernández was in a boat sunk by a German submarine and she prays to the Virgin of Guadalupe.

Later on, she cannot sleep and she starts reading a book about the Virgins apparition to an Indian named Juan Diego and she revives the story.  Carlos is eventually saved and Lupita and Carlos go to Basilica of Our Lady of Guadalupe on December 12.

==References==
 

==External links==
* 
*   http://www.filmoteca.unam.mx/cinelinea/silente/silente_tepe_cap.html Página para ver la película en la Filmoteca de la UNAM filmoteca.unam.mx]

== Bibliography ==
* David E. Wilt: The Mexican Filmography 1916 through 2001. McFarland & Co Inc, Jefferson NC 2004. ISBN 978-0-7864-6122-6

 
 
 
 
 
 
 

 
 