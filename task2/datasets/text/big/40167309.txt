Cold in July (film)
 
{{Infobox film
| name           = Cold in July
| image          = Moviepostercoldinjuly.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jim Mickle
| producer       = {{plainlist|
* Linda Moran
* Rene Bastian
* Adam Folk
* Marie Savare
}}
| writer         = {{plainlist|
* Jim Mickle
* Nick Damici
}}
| based on       =  
| starring       = {{plainlist|
* Michael C. Hall
* Sam Shepard
* Don Johnson
}}
| music          = Jeff Grace
| cinematography = Ryan Samul
| editing        = {{plainlist|
* John Paul Horstmann
* Jim Mickle
}}
| studio         = {{plainlist|
* BSM Studio
* Belladonna Production
* Backup Media
* Paradise City
}}
| distributor    = IFC Films
| released       =  
| runtime        = 110 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = $423,223 (US) 
}} novel of the same name by author Joe R. Lansdale.    IFC Films theatrically released the film on May 23, 2014.

==Plot==
 
After being awakened by his wife Ann, who herself was startled by the noise of their front door window being broken, Richard Dane accidentally shoots an intruder. The intruder is identified by the police as Freddy Russell, apparently a wanted felon. Shaken by the experience, Richard attempts to move on with his life.

The day Freddy Russell is buried, Richard visits the cemetery where the deceased felon is being buried. Freddys father Ben Russell, a paroled convict, is also there and accosts Richard in his car, making a veiled reference to his son.

The encounter alarms Richard who then immediately proceeds to pick up his son personally from school and arranges for his wife to meet him at the police station. Richard notes that Ben has followed him. He asks the police help but as nothing has actually happened yet, the police decline to do anything.

When their house broken into again, the police now grant his family official protection, and maintain a constant vigil at his house, stationing a police officer as a guard inside the house. The first night under guard, the intruder makes his presence known by knocking out the guard and steals away into Jordans room, locking the door. He never left the house and had been hiding in the crawlspace the entire time. However he does not do anything aside from escaping to a nearby river. He is apparently tracked to Mexico where he is then apprehended. The police invite Richard to the police station so they may close the case for good.

While there, Richard notices a wanted poster for one "Frederick Russell" - Richard notes that the face on the poster and the man he remembered shooting are far too different to be the same man. When attempting to point out the contradiction to police officer Ray Price, he is dismissed; Price reasoning that his memory must be fuzzy from the night, noting his own shock from the incident. He is dismissed a second time when he rings officer Price wanting to discuss it again.

Annoyed at being spurned, he visits the police station and notices Ben taken away by police in an unmarked car. He follows them and after dragging Ben out, Richard witnesses the police injecting some unknown substance into him, leaving him on a train track and splashing him with alcohol. Noticing a train approaching, Richard saves Ben and brings him to a remote cabin, chaining him up so as to prevent his escape.

Ben, still believing that Richard killed his son, and incredulous at his claims that the man he shot was not Freddy, accompanies Richard to the cemetery that Freddy is supposedly buried at and both men exhume the body. Ben confirms that the man buried is not his son, and notes that the top-half of the mans fingers are chopped off, most likely in an attempt at preventing identification.

The next day, Officer Ray Price pays Richard a visit at his workplace, both to apologize and to explain the apparent contradiction between the man Richard claimed he saw and the picture on the wanted poster; as Freddy was a wanted man, he most likely changed his appearance to avoid capture.

This does not satisfy Richard who is also visited by one Jim Bob Luke - a private investigator known to Ben, who discovers that Fred Russell is mentioned multiple times in newspaper stories regarding the shooting death of a burglar. He theorizes that after Freddy became involved with the Dixie Mafia, he was caught by Federal investigators. In exchange for information, Freddy had his identity concealed through Witness Protection, and they faked his death to prevent the mafia from coming after him.

Jim, Richard and Ben decide to investigate further, Richard wanting to know who he actually shot and Ben wanting to find his son. They move to Houston, Texas discovering that Freddy, assuming the name of Frank Miller probably resides there. While initially unable to find Freddy, an attempt to meet him at Frank Millers residential address results in an accidental encounter with a large Mexican man, whose car trunk contains a multitude of home videos. After subduing him, the three men take one VHS tape out of curiosity. Jim and Richard start to watch the tape, appearing to be nothing more than a pornographic film, but as the tape continues they are  both shocked to discover the tape was actually a snuff film, featuring Freddy Russell, while wearing a catchers helmet, viciously beating a prostitute to death with a baseball bat. Richard wants to go to the police with the tape but Jim believes the police know what Freddy has been doing and are unconcerned since Freddy is far more valuable to them bringing down such a dangerous organization like the Dixie Mafia. Jim doesnt think Ben should see the tape but after Ben insists on knowing whats on it, Jim has no choice but to let Ben watch it.

Ben is so disturbed by the tape that he is resolved to kill his own son at any cost. Richard decides to head back home, but after spending some time thinking, changes his mind and returns to Houston to help Jim and Ben carry out their plan.

They discover Fred is using a video rental shop as a cover and from there, they follow him to a remote mansion. They infiltrate it and begin killing everyone they find inside. After finding Freddy as the last one alive, Ben hesitates which results in Jim Bob getting shot. Fred is able to shoot both Richard and Ben before Ben finally lands a hit. After a very brief reunion where Ben declares himself to Fred as his father, Ben shoots him in the head before bleeding out.

After setting the mansion ablaze to conceal what happened there, the two men rescue the hostage prostitute. Jim recovers from his wounds while Richard returns home to his family.

==Cast==
* Michael C. Hall as Richard Dane
* Sam Shepard as Ben Russell
* Don Johnson as Jim Bob Luke
* Vinessa Shaw as Ann Dane
* Nick Damici as Ray Price
* Wyatt Russell as Freddy
* Lanny Flaherty as Jack Crow
* Rachel Zeiger-Haag as Valerie
* Brogan Hall as Jordan Dane

==Production==

===Development===
Producers Linda Moran and Rene Bastian had an easier time of funding the film than Mickles earlier work, as his reputation had grown; Moran said that they "worked on raising the money for this film for quite some time in every way we thought possible."     B Media Global, a division of French company Backup Media, fully financed Cold in July. It is the first film to be fully financed by Backup Media.   Already a fan of Lansdale, Mickle turned to reading through his stack of Lansdale novels for pleasure.  When he read through Cold in July in just a few hours, he excitedly realized that he had not seen these elements in a film before and looked to adapt it.   Mickle said the film was difficult to pitch due to how it constantly evolves and re-invents itself.   In order to reassure financiers, Mickle described Korean thrillers that had succeeded with similar shifts in tone, but he says that he doubts that he convinced anyone.   

Mickle stated that he had difficulty getting the film made; it took seven years from when he first read the novel.  The first draft of the script, a verbatim copy of the novel, was 220 pages long; from there, Mickle and Damici edited the script down to a more manageable size.  The final script was also the furthest from the novel, but they felt it was closest in spirit.     Lansdale said that he saw all the revisions of the script except for the last and "felt respected through the entire process."   Although the original book was not designed to be filmed, Lansdale said films had been a secondary influence on the story, and the story "certainly fits with film." 

As they adapted the novella, they realized that their faithful scripts were much too long, which did not do justice to the novellas pacing and length.  Eventually, they were able to edit it down by removing unnecessary dialog that could be replaced through body language.  Changing the time period was never considered, as Mickle considered the themes of masculinity and manhood to come from a previous era that would not work in a more modern setting.   Shepard also contributed to the script.  Mickle and Damici had struggled with one particularly difficult scene and debated removing it entirely.  When Shepard offered to rewrite it, Mickle gratefully accepted.  Shepard returned with a single typewritten page and offered to run it by Damici, but Mickle said it was not necessary and accepted it as-is. 

Mickle and Damici were drawn to Lansdales cross-genre style, as their own films were also cross-genre.  Although primarily known for their work in horror films, Damici and Mickle said that they were more interested in storytelling and making good films, and Damici said that they can return to making horror films at any time.   Mickle used Halls character to ground the film so that audiences would not feel lost as the film crossed genres.  Mickle wanted to avoid obvious genre categorization and predictability, as he felt that genre films needed to be shaken up.  Buyers had previously encouraged him to re-cut one of his films so that audiences could easily identify the genre, limited to six subgenres, in the first ten minutes.  Mickle said their advice blew his mind. 

=== Pre-production ===
In September 2013, Variety (magazine)|Variety reported that Sam Shepard, Don Johnson, and Vinessa Shaw joined Michael C. Hall, who had already been cast.   Although he plays a killer, Hall said that the script quieted any concerns he had about being typecast.  Cold in July was filmed after Dexter (TV series)|Dexter ended, and Hall was drawn to the role based on his desire to play a more normal character; Hall called the role therapeutic, as it allowed him to move on from playing the iconic serial killer Dexter Morgan.   The role further appealed to Hall for his characters lack of control over his own life, which contrasted strongly with the self-assured and in-control Dexter. 
 Six Feet Under and Dexter (TV series)|Dexter.  Although Mickle had concerns that audiences would find Hall difficult to accept as an uncomplicated everyman, he believed Hall to be one of the best working actors.  Hall further impressed Mickle with his off-screen personality, which was surprisingly normal and completely unlike his TV characters.     Once Hall had been cast, Mickle said that the rest of the casting "fell in place from there."     Mickle had reached out to Shepard seven years earlier but heard that Shepard was retiring from acting.  After Hall had been cast, however, Mickle once again contacted Shepard.   Shepard expressed interest in the script, and Moran suggested casting Don Johnson, who had impressed Mickle with his recent film roles.   When he read the script, Johnson said he was "taken by how it had different rhythms and tempos", and he "didnt know what the hell was going to happen by page 10."   

=== Filming and post-production === Road House We Are What We Are, Mickle wanted to explore more masculine themes in Cold in July.     Mickle called Cold in July "the other side of the coin" to We Are What We Are and a film about being "sucked into your own kind of action movie."   Mickle tried to work in many 1980s themes and references to John Carpenters work from that period.   After his previous films, Mickle had tired of city-based productions and was looking for something less urban.   Shooting began in Kingston, New York on July 29, 2013.   Shooting included Esopus, New York|Esopus, Woodstock, New York|Woodstock, and other Hudson Valley locations.   It was shot on a RED EPIC camera.   Lansdale spent two weeks on the set and said he "loved it".   Filming took 25 days, which Hall called "refreshing" compared to his long run on a TV series. 

Johnson based his character on a composite of people he knew.  He did not read the source novel, as he wanted his performance to be based on the screenplay and untainted by echoes from the novel.  Of Mickle, Johnson said that he was quickly impressed with the directors skill and enjoyed working with him.   Hall finished his run on Dexter a week before filming began on Cold in July.   Hall did not base his performance on Dexter, and he did not consciously think of parallels between Cold in July and Dexter during filming.  Hall said that Mickle had a "welcoming, fun vibe" and was a "great leader", and he said that he would enjoy working with Mickle again. 

Long-time collaborator Ryan Samul worked closely with Mickle on the cinematography.  They planned out the scenes in advance and used 3D graphics to visualize difficult shots.    Cold in July was the first time Mickle worked with an editor instead of performing the job by himself.  In an interview, he said that tight deadlines forced him to delegate the duties to a like-minded collaborator.  He described it as "a weird experience" and an experiment.   Mickle said that he never before had to cut scenes from his films during editing, but Cold in July was "a constant balance of shifting things, removing sub plots, and putting them back in different places."   The score was composed by Jeff Grace.  Mickle and Grace had collaborated on several films previously, and they had previously planned what kind of score they wanted for Cold in July.  It was influenced by John Carpenters scores.   Samuel Zimmerman of Fangoria also describes an influence from Sam Peckinpahs 1980s work. 

== Release ==
This film made its premiere and competed in the 2014 Sundance Film Festival.    IFC Films announced that they acquired North American rights to the film in a deal estimated at $2 million,   and they released the theatrical and video on demand on though IFC "movies on the same day as theatrical release" May 23, 2014.    The film was selected to be screened as part of the Directors Fortnight section of the 2014 Cannes Film Festival.     It was released on home video in the United States on September 30, 2014. 

== Reception ==
  rated it 73/100 based on 30 reviews.   David Rooney of The Hollywood Reporter wrote, "Jim Mickle continues to show that he’s among the most distinctive genre filmmakers on the indie scene with this cracked but flavorful thriller."   Kyle Smith of the New York Post called it a midnight movie that is "good, but not a scorcher".   Brian Tallerico of Film Threat rated it 3.5/5 stars and wrote, "Any issues with the actual narrative fall away when one considers the risk-taking here."   Andrew OHehir of Salon.com called it "tense, gripping, gruesome, often hilarious, brilliantly engineered and highly satisfying."   Todd Brown of Twitch Film called it "a remarkable bit of work from one of the brightest lights on the American indie scene."   Rodrigo Perez of Indiewire wrote that the film is initially irritating and contrived, but it becomes "far better than it has any right to be and perhaps, more significantly, is unusually absorbing and memorable."   Scott Foundas of Variety (magazine)|Variety described it as "a modest, unpretentious exercise in old-fashioned thrills and chills, made with a level of care and craft that elevates it well about the fray."   Stephen Holden of The New York Times called it a "pulpy neo-noir" film that "goes gleefully haywire".   Betsy Sharkey of the Los Angeles Times wrote, "Tense and violent, it grabs you from the first moments and rarely loosens its hold until the last body drops."   Ed Gonzalez of Slant Magazine rated it 2.5/4 stars and called it a "compulsively watchable" film that eventually "falls into a rabbit hole of tiresome plot machinations".   A. A. Dowd of The A.V. Club rated it B– and wrote that it "thrillingly shuffles genres, before settling on a dumb one". 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 