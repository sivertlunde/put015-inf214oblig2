Two Doors
 
  
 }}
{{Infobox film
| name          = Two doors
| image         = File:Two_Doors_poster.jpg
| caption       = 
| director      = Kim il-rhan, Hong Ji-you
| producer      = Yonbunhongchima
| writer        = 
| starring      = 
| music         = Pyo Yong Soo
| cinematography= 
| editing       = Kim il-rhan, Hong Ji-you
| studio        = 
| distributor   = Cinema Dal
| released      =  
| runtime       = 101 minutes
| country       = South Korea
| language      = Korean
}}
Two Doors ( ) is a Korean documentary film tracks down the 2009 Yongsan tragedy, which resulted in the death of 5 evictees and one police commando.

The evictees, who were cornered into to climbing up a steel watchtower to appeal for their right to live, came back down as cold corpses only 25 hours after they started building the tower. Those who survived became law offenders. A long battle for the truth started as the police statement accused the evictees for causing the tragedy by carrying firebombs up the tower for an illegal and violent protest while on the other side criticism voiced that the governmental authority’s excessive use of force had aggravated the tragedy. The heightened tension between the two parties and opposite views of the truth led for the judgment of the government and the eradication of illegal and violent protests by a decision from the court.

 
 
 

 