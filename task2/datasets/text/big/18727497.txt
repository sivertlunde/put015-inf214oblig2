Manina, the Girl in the Bikini
{{Infobox Film 
| name = Manina, the Girl in the Bikini
| image = Maninaposter.jpg
| caption = Original French-language poster
| director = Willy Rozier producer = Wily Rozier
| starring = Brigitte Bardot,  Jean-François Calvé and   Howard Vernon
| writer = Willy Rozier Xavier Vallier
| released = 1952 (France) 24 October 1958 (US)
| runtime = 86 minutes 
| country = France distributor = Atlantis Films (US) gross = 1,115,424 admissions (France)  French
| tagline = A Revealing Episode on a Lonely Island!
}}
Manina, the Girl in the Bikini is a 1952 French film directed by Willy Rozier and starring Brigitte Bardot, Jean-François Calvé and Howard Vernon. The film is one of Bardots first film roles, at the age of 17, and was controversial for the scanty bikinis worn by the young Bardot in the film, one of the first occasions when a bikini had appeared in film and when the bikini was still widely considered immodest.

Though released in France in 1953 as Manina, la fille sans voiles, the film was not released in the United States until 1958 as Manina, the Girl in the Bikini and in the United Kingdom until 1959 as The Lighthouse-Keepers Daughter. In other countries it was released under other names. The film was able to be screened in the United States notwithstanding the Hays Code prohibition of exposure of the midriff as a foreign film.

The film was shot in Cannes, Nice and Paris in the summer of 1952. Brigitte Bardots father had signed a contract, on behalf of his minor daughter, specifying that the film was not to show indecent images. When in the course of filming, a series of "highly suggestive" photographs of his daughter was released, he accuses the producing company of not respecting the contract and demanded that the film not be projected without the permission of a court. He lost the suit. Raymond Boyer, Ghislain Dussart, Isabelle Salmon, Brigitte Bardot, preface by Brigitte Bardot, Paris,  ed. Vade Retro, 1994. ISBN 2-909828-07-7. 

==Plot==
A 25-year-old Parisian student, Gérard Morere (Calvé), hears a lecture about a treasure Troilus lost at sea after the Peloponnesian War, and thinks he knows where it is, thanks to a discovery he made five years earlier when diving near the island of Lavezzi Islands|Levezzi, in Corsica. He gets friends and an innkeeper to invest in his dream, enough to get him to Tangiers where he convinces a cigarette smuggler, Eric (Vernon), to take him to the island. 

There they find 18-year-old Manina (Bardot), the light-keepers daughter, who is beautiful and pure. Eric thinks Gérard may have conned him, but Gérards belief in the treasure compels patience. Gérard dives by day and romances Manina at night. Gérard finds the treasure but Erick runs away with it, but he is wrecked in a storm.

==References==
 

== External links ==
*  
*  
*  at New York Times
 
 
 
 
 
 
 
 
 


 
 