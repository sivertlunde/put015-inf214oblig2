Girls at Arms 2
{{Infobox film
| name           = Girls at Arms 2
| image          = 
| caption        = 
| director       = Finn Henriksen
| producer       = Henrik Sandberg
| writer         = Peer Guldbrandsen Finn Henriksen
| starring       = Berrit Kvorning
| music          = 
| cinematography = Erik Wittrup Willumsen
| editing        = Lizzi Weischenfeldt
| distributor    = 
| released       = 11 October 1976
| runtime        = 101 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Girls at Arms 2 ( ) is a 1976 Danish comedy film directed by Finn Henriksen and starring Berrit Kvorning.

==Cast==
* Berrit Kvorning - Premierløjtnant Merete Clausen
* Klaus Pagh - Hermann Clausen
* Dirch Passer - Vasby
* Karl Stegger - Major Basse
* Helle Merete Sørensen - Vibsen
* Ulla Jessen - Magda
* Magi Stocking - Børgesen
* Marianne Tønsberg - Irmgard Martinsen
* Birger Jensen - Frits
* Søren Strømberg - Otto
* Lille Palle - Orgeltramper
* Finn Nielsen - Oversergent Brysk
* Lotte Tarp - Journalisten Kirsten
* Lise Schrøder - Journalist
* Olaf Ussing - Forsvarsministeren
* John Larsen - Henrik
* Jan Hertz - Max
* Alvin Linnemann - Kroværten
* Lilli Holmer - Butiksdetektiv
* Lise Henningsen

==External links==
* 

 
 
 
 
 


 
 