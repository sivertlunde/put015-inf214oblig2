Clean and Sober
{{Infobox film
| name           = Clean and Sober
| image          = Clean and sober.jpg
| image_size     =
| caption        = Promotional movie poster for the film
| director       = Glenn Gordon Caron
| producer       = Ron Howard Jay Daniel
| writer         = Tod Carroll
| starring       = Michael Keaton Kathy Baker Morgan Freeman M. Emmet Walsh Tate Donovan
| music          = Gabriel Yared
| cinematography = Jan Kiesser
| editing        = Richard Chew
| studio         = Imagine Entertainment
| distributor    = Warner Bros.
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = $12 million
| gross          = $8,674,093
}}
Clean and Sober is a 1988 American drama film directed by Glenn Gordon Caron and starring Michael Keaton as a real estate agent struggling with a substance abuse problem. This film was a dramatic departure from comedies for Keaton. The supporting cast includes Kathy Baker, M. Emmet Walsh, Morgan Freeman and Tate Donovan.
 Night Shift Gung Ho (1986), served as co-producer.

==Plot==
 addicted to embezzles $92,000 drug rehabilitation program which lasts about a month and which guarantees anonymity. He checks in, figuring he can hide out there.  While in rehab he meets Craig, a tough but supportive drug rehabilitation counselor. With great difficulty, Craig helps Daryl to realize he is an addict and that his life is complete chaos. He says to him, "The best way to break old habits is to make new ones."
 abusive relationship manipulative way of winning her back. Daryl tries to remain in Charlies life to help her stay sobriety|sober. After another fight with Lenny, she leaves the house, attempts to use drugs (and perhaps return to Daryl) and is killed in a car accident.
In despair, Daryl also feels a strong temptation to return to drugs. He visits Richard, who talks him out of it. Near the storys end, Daryl, confused but hopeful and reborn, accepts his 30 Day Sobriety Chip in front of an audience of fellow members, as he tells his story.

The film ends with a distorted shot of cars taking off into the night.

==Cast==
* Michael Keaton as Daryl
* Kathy Baker as Charlie
* Morgan Freeman as Craig
* M. Emmet Walsh as Richard
* Luca Bercovici as Lenny
* Tate Donovan as Donald
* Claudia Christian	 as Iris
* Brian Benben as Martin
* J. David Krassner	as Tiller
* Dakin Matthews as Bob
* Ben Piazza as Kramer
* Rachel Ryan as Karen Peluso

==Reception==
The film received generally favorable reviews at the time. Ebert praised the "superb supporting performances" and noted that "Although the subject matter of this film is commonplace in our society...the actual process of surrender and recovery is hardly ever the subject of films, maybe because it seems too depressing."    Variety wondered if the film was "perhaps too grim"     IMDb 

==Awards==
Michael Keaton won the 1988 National Society of Film Critics Award for Best Actor for his performances in both Clean and Sober and Beetlejuice.

==References==
 

==External links==
* 
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 