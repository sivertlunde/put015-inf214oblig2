Red State (2011 film)
{{Infobox film
| name = Red State
| image = Red State Poster.jpg
| alt = 
| caption = Teaser poster
| director = Kevin Smith
| producer = Jonathan Gordon
| writer = Kevin Smith
| starring = Michael Angarano Kerry Bishé Nicholas Braun Kyle Gallner John Goodman Melissa Leo Michael Parks Kevin Pollak Stephen Root Dave Klein
| music =
| editing = Kevin Smith
| studio = The Harvey Boys 
| distributor = SModcast Pictures   Lionsgate  
| released =     
| runtime = 88 minutes 
| country = United States
| language = English
| budget = $4 million 
| gross = $1,874,460  
}}
 independent Action film|action-adventure-Thriller (genre)|thriller-horror film, written and directed by Kevin Smith, starring John Goodman, Melissa Leo and Michael Parks.

For months, Smith maintained that the rights to the film would be auctioned off to a distributor at a controversial event to be held after its premiere at the Sundance Film Festival, but instead Smith purchased the film himself, which, according to John Horn of Los Angeles Times, "might have been a difficult sale for any distributor".    Smith chose to self-distribute the picture under the SModcast Pictures banner with a traveling show in select cities.

On June 28, 2011, Smith announced a one-week run in Quentin Tarantinos New Beverly Cinema (making the film and its actors eligible for Academy Award consideration).  The film was released via video on demand on September 1, 2011 through Lionsgate, was released in select theaters again for a special one-night-only engagement on September 23, 2011 (via SModcast Pictures), and was released on home video October 18, 2011.  

==Plot==
 
Travis (Michael Angarano) notices a fire station siren being removed from its pole and members of the Five Points Trinity Church, led by Abin Cooper (Michael Parks), protesting the funeral of a murdered local gay teenager. Jared (Kyle Gallner), a friend of Travis, reveals he received an invitation from a woman (Sarah Cooper) (Melissa Leo)he met on a sex site for group sex with Travis and Billy Ray (Nicholas Braun). They borrow Travis parents car and travel to meet her. Along the way, they accidentally sideswipe the vehicle of Sheriff Wynan (Stephen Root), while he was having sex with a man. Afraid, the boys drive off.

Sheriff Wynan returns to the station and tells his deputy Pete (Matt L. Jones) to go and look for the vehicle. Meanwhile, the boys arrive at the womans trailer. She encourages them to drink, and after being drugged by the beer, they pass out while undressing. Jared wakes up while being moved in a covered cage. He realizes he is in the sanctuary at Five Points after he identifies Cooper. Cooper begins a long, hate-filled sermon before identifying another captive, a homosexual they lured in through an internet chat room. They bind him to a cross using plastic cling wrap, execute him with a revolver and drop him into a small crawl space where Travis and Billy Ray are bound together.
 James Parks). ATF Agent Joseph Keenan (John Goodman), who begins setting up outsidethe church.
 raid of the complex to ensure that no witnesses remain.  Keenan is clearly disturbed by this, but passes on the order to another tactical agent named Harry (Kevin Alejandro) who also struggles with this decision and argues with Keenan.. Keenan dismisses Harrys protests for personal reasons, rationalizing his decision based on personal gain and the reputation of the ATF, and Harry storms off in disgust.

During the shoot-out, Cheyenne (Kerry Bishé) escapes and is captured by an ATF agent (Marc Blucas) who is about to shoot her, but is instead killed by Cheyennes mother, Sarah. Cheyenne returns to the house and unbinds Jared, begging him to help her hide the congregations children. Jared refuses and her pleas turn into a fight. Sarah notices them and attacks Jared. Cheyenne tries to break up the fight and accidentally shoots Sarah in the process, killing her. Cheyenne sends the children up into the attic, and Jared changes his mind and decides to help Cheyenne hide the children. They run outside to plead with Keenan to spare the children but are killed by Harry.

The shoot-out is interrupted by a mysterious trumpet blast.  The remaining Coopers lower their weapons and run outside rejoicing, claiming that "the Rapture" has come upon them. Abin calmly approaches a stunned ATF team and confidently taunts them that Gods wrath is upon the Earth. He raises his arms and stands in the face of a confused and worried Keenan in a moment of triumph, daring him to defy God. 

Several days later, during a briefing before high-ranking government officials, Keenan reports that he then head-butted Cooper and took the rest of the congregation into custody. He explains that the trumpet noises were not the Rapture but came from a group of college students who lived down the road and were irritated with Cooper. As a prank they rigged up an old fire house siren to an iPod with loud trumpet noises, unaware of the shootout taking place over the hill. Keenan is promoted despite disobeying a direct order from his superiors at the time to kill everyone at the compound.

Keenan is surprised that he is not punished for his actions but his superiors explain that their initial decision to kill the members of the congregation was mostly personal and that they are satisfied with the alternative punishment of taking away the prisoners constitutional rights to due process by classifying their crimes as terrorism and locking them up without ever letting them go to trial. Keenan laments this outcome in a story he shares about a couple of hungry brawling dogs he once knew that taught him about the darker side of human nature and the way simple beliefs can turn humans into bloodthirsty animals.

Cooper is finally seen pacing around his cell singing and sermonizing to himself until another prisoner (Kevin Smith) tells him to "shut the fuck up".

===Original ending===
During various interactive Q&As for the film, Smith has stated that the original ending actually continued with the  Rapture actually happening and  the Four Horsemen of the Apocalypse descending on the scene. 

==Cast==
* Michael Parks as Pastor Abin Cooper ATF Special Agent Keenan, investigating disturbances at Cooper homestead
* Melissa Leo as Sarah Cooper, Pastor Coopers daughter, Calebs wife and Cheyennes mother
* Kyle Gallner as Jarod, teen who answered online sex ad
* Kerry Bishé as Cheyenne, Sarah and Calebs daughter and Abin Coopers granddaughter
* Michael Angarano as Travis, another teen who replied to sex ad
* Nicholas Braun as Billy Ray, third teen
* Ralph Garman as Caleb, Sarahs husband, Abin Coopers son-in-law and Cheyennes father
* Stephen Root as Sheriff Wynan James Parks as Mordechai, Pastor Coopers son and Fiona Mays father
* Haley Ramm as Maggie
* Kevin Pollak as ATF Special Agent Brooks
* Matt L. Jones as Deputy Pete
* Kevin Alejandro as Tactical Agent Harry
* Anna Gunn as Travis mother
* Betty Aberlin as Abigail
* Marc Blucas as ATF Sniper
* Elizabeth Tripp as Melanie, Abin Coopers granddaughter, Caleb and Sarahs daughter
* Jennifer Schwalbach Smith as Esther, Mordechais wife and Fiona Mays mother
* Molly Livingston as Fiona May, Abin Coopers granddaughter, daughter of Mordechai and Ester.
* Kaylee DeFer as Dana, a classmate of Jarod and his friends
* Alexa Nikolas as Jesse
* Hoc Sy as Butch
* Kevin Smith as prisoner who tells Pastor Abin Cooper to shut up.

==Production== Wizard World Chicago 2006 convention that his next project would move in a different direction, and it would be a straight horror film.  In April 2007, Smith revealed the title of the movie to be Red State and said that it was inspired by infamous pastor Fred Phelps, or as Smith claimed, "very much about that subject matter, that point of view and that position taken to the absolute extremism|extreme. It is certainly not Phelps himself but its very much inspired by a Phelps (like) figure."  The first draft was finished in August 2007 with Smith wanting to film it before Zack and Miri Make a Porno.   Setting it apart from the majority of his other films, Smith made it clear that Red State was a horror film, stating that there would be no toilet humor in the film. 
 investments from his fans but this idea was later dropped.   
 Bob and Cop Out, passed on supporting Red State with necessary funding.    The budget was provided from two main private investor groups that raised the $4 million, one based in New York, one in Canada.   

On July 24, 2010, it was also reported that actor Michael Parks had signed on to the film in a starring role,  and on September 5, 2010, Smith confirmed that Matt L. Jones was also cast.  On the September 20 edition of his and Ralph Garmans podcast Hollywood Babble-on, Kevin Smith announced that John Goodman had joined the cast.  Smith edited the film throughout production and showed a first cut at the films wrap party. 
 Sundance in Jan for RED STATE." On November 8, Smith announced on Twitter that the movie was viewed by Sundance, to determine if it was eligible for entry in the 2011 festival.  On December 1, Smith announced on his Plus One podcast that Red State would be screened at the 27th Sundance Film Festival in the non-competition section. 
 Bryan Johnsons film Vulgar (film)|Vulgar as being an inspiration for Red State.

==Marketing and distribution==
  }}
Throughout the months of November and December, teaser posters were released featuring characters from the movie in auctions via his Twitter account with the winning bidder hosting the poster exclusively on their website, while the money raised by the auction went to charity.  Smith released a teaser trailer for the film on December 23, 2010. 
 Sundance screening of the film, he revealed that was merely a ploy and Smith planned to self-distribute as a traveling roadshow beginning March 5 at Radio City Music Hall, and would tour the film across North America before releasing Red State directly to DVD and VOD. 

  }}

===Auction controversy===
Controversy soon erupted after Kevin Smiths speech at the films debut screening at Sundance Film Festival|Sundance. Although Smith had decided to self-distribute the film, according to the films producer Jonathan Gordon the option of self-distributing the movie was not considered at first: Hiring longtime specialty exec Dinerstein (whose film marketing consultancy also arranges self-distribution deals), bringing aboard Cinetic Media (which arranged service deals for sale titles like last years Banksy doc "Exit Through the Gift Shop") with co-seller WME, and slapping the word "March" at the end of the teaser trailer has led many to suspect Smith has a self-distribution backup plan should an attractive offer fail to materialize. But is self-distribution or a service deal even an option theyre considering? "No," says Gordon. "We want to have someone who loves the movie, understands it, knows how to handle it and get the most out of it."  

The sudden announcement of self-distribution after initially announcing an auction provoked a backlash from the media and accusations of dishonesty,       with some analysts commenting that they watched Kevin Smith "implode" and that he had "lost cred" and one prominent buyer saying, "He stole two hours and insulted every one of us...He was a little like the twisted preacher Michael Parks played in his film. It became life imitating art."  Smith joked about the people who had expected to buy the film at his Sundance speech:

 
Now, were obviously not selling the movie, so Im sorry to . . . the distributors in the room. . . . Number one: Im not that sorry. Its a fucking film festival. Come see a movie. . . . no hard feelings. Hopefully you dont mind. . . . Thank you for coming. . . . I will say this in my own defense . . . a lot of youse work for studios . . . you guys make a lot of trailers - youve lied to me many times. . . . Ive seen many trailers where Im like "this is awesome", and I put my money down, and Im like "You fucking lying whores". So ladies and gentleman, as you can see were up here alienating all our future work, just burning the bridge as we cross it, and ah, that means theres probably not going to be much studio help for me and Jon in the future.  

According to some writers, the internet community seemed to galvanize in response to the controversy, "...it seems Kevin Smith finally has the Internet critical community united on the same side: against him."  Smith countered allegations of dishonesty by saying, "And I told the truth, in my tweet. I said, If I get to Sundance, I intend to pick my distributor in the room, auction-style. Auction-style—did I not do that?...I stood up there and said that Im gonna take my movie—Im gonna take it out and try not to spend money doing it." 
 Sundance debut of his film and the events leading up to it:
 Smith was one of the first in the business to have a website and sell merchandise – pieces of film from his movies and action figures – to fans. But one source who has worked with him thinks Smith might be one of the first filmmakers to exploit and then be undone by social media, and that access to social media has eliminated any filter that might have protected Smith from emotional outbursts that, in this persons view, have undermined his career.  

Smith responded to Masters, saying that it was "a Jerry Maguire moment. Ive got a little fish in a plastic bag and one idealistic secretary on my side, and the Bob Sugars seem to be leaning in doorways, smirking." 

===Indie Film 2.0===
Smith described his motivations, strategy, and thought process behind the marketing method at his Sundance appearance    and on various podcast shows, Q&As,  tweets,  etc. He described the strategy as carefully planned with his business partners, including Jonathan Gordon, who had recently had an interesting experience as a short-lived executive at Universal.  Smith did not take a salary for the film, noting that the plan was to pay investors back first.

In his Sundance Q&A Smith compared the Indie scene from the 1990s to the present film landscape. He described the traditional film marketing system, in which studios buy films and then spend large amounts of money marketing it (often comparable to the film cost). He noted that it took 7 years for his film Clerks to become profitable due to this system (see Hollywood accounting). He also compared selling a film to giving a child away for someone else to raise it.  Other telling quotes from his Sundance appearance referenced a sort of rebirth of the notion of independence in show business.

 "Were starting over . . . true independence isnt making a film and selling it to some jackass. True independence is schlepping that shit to the people yourself. And thats what I intend to do. . ."  

 "Yes, anybody can make a movie, we know that now . . . What we aim to prove is that anybody can release a movie as well, and its not enough to just make it and sell it anymore, Im sorry. . . . Indie film isnt dead people, it just grew up. Its just indie film 2.0 now, and in indie film 2.0 we dont let them sell our movie, we sell our movie ourselves."   

 "Root for us if you will, hate us if you must. . . . But I cant think of a more interesting business news story that youre ever gonna hear about this fucking year, man. Were definitely gonna go out there and try to find a new distribution model."  

Smith planned to spend no money on traditional advertising (billboards, TV, print media), instead using the aforementioned in-person theatre tour with accompanying Q&A show, word-of-mouth, Twitter,  social media, podcasts,  and other means not traditionally used by the studio system.

In April 2011, Smith revealed that Red State had already made its budget back with the film making $1 million on the first leg of the tour, $1.5 million from a handful of foreign sales and $3 million from a domestic distribution deal for VOD. 

==Tour and screenings==
The Westboro Baptist Church protested the films release at the Sundance Film Festival because some of the elements of the film were "modeled after the founder and members of the Westboro Baptist Church." 

The tour went well and the experience was described in detail in many of the SModco podcasts.  On one of the tour stops, two defectors from the Westboro Baptist Church appeared at the public Q&A to ask Smith questions from the audience. He invited them on stage and proceeded to interview them. 

The film was released to cinemas in Australia on October 13, 2011.  

==Animated version==
In 2015 filmmaker Dan Costales, worked with Sound designer of Bobb Barito and animation work from Dennis Fries, an animated short film based on Red State,  which was released in March 2015 in the USA. 

==Reception==

===Box office===
Red State was a box office disappointment, earning $1,104,682 in The United States and $769,778 internationally for a total of $1,874,460, well below its budget of $4 million.  

===Critical response===
The film received mixed reviews from critics and has a "rotten" score of 58% on Rotten Tomatoes based on 84 reviews with an average rating of 6 out of 10. The critical consensus states "Red State is an audacious and brash affair that ultimately fails to provide competent scares or thrills."    The film also has a score of 50 out of 100 On Metacritic based on nine critics indicating "mixed or average reviews". 
 Waco disaster. UGO also panned the film, saying, "Kevin Smith, a wonderful public speaker and genuinely fun guy, has yet to master the basics of movie making."  According to Drew Mcweeny of "Motion Captured", "Kevin Smiths Red State fails onscreen and off at its world premiere...A shoddy film and a bait-and-switch event fail to satisfy on any level."  Raffi Asdourian of The Film Stage wrote that, "While there are glimpses of Smiths wry humor scattered throughout, Red State cant help but feel like a B action movie that started off with ambitious ideas but collapses under its  own preachy weight... its clear that the smart alec writer still has some things to learn about making a great film."  Matt Goldberg of Collider.com wrote that, "Red State is a radical departure for Smith and yet he lacks the confidence to properly execute the action-horror-thriller hes devised." 
 
James Rocchi writing for indieWire wrote that, "...Smith has gotten as far as he has with his comedies because it is a writers genre more so than it is a directors. Horror is the genre of a director—pacing, feel, shots, editing—and Smiths skills are not up to the task..." 
 Richard Kelly also offered his take on the film and Smith while appearing on Smiths SMovieMakers podcast.    He said "I have never seen a filmmaker reinvent himself the way you just have. I wont say anything else because I dont want to spoil anything. Its really really exciting…"  Smith blogged on his official film website that filmmaker Quentin Tarantino saw the film and gave him positive feedback about it; he also named it as his 8th favorite film of 2011.   Former collaborator Ben Affleck also loved the film and ended up casting Goodman, Parks and Bishé in his film Argo (2012 film)|Argo. 

In October 2011, Red State won the Best Motion Picture award at the 2011 Sitges Film Festival, while Michael Parks was named Best Actor.  Parks character, Abin Cooper, received a nomination for Villain Of The Year from the Virgin Media Movie Awards.  

==References==
 

==External links==
*  
*  
*  
*   at SModcast|SModcast.com
*  
*  

 

 
 
 
 
 
 
 
 