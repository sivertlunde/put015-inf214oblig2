Shogun Assassin
 
{{Infobox film
| name           = Shogun Assassin
| image          = ShogunAssassin.MoviePoster.png
| caption        = Theatrical release poster Robert Houston
| producer       = Shintaro Katsu Robert Houston
| writer         = Bobby Houston 
| based on       = Lone Wolf and Cub by Kazuo Koike and Goseki Kojima
| starring       = Tomisaburo Wakayama Kayo Matsuo Minoru Ôki Shôgen Nitta Shin Kishida Akihiro Tomikawa
| music          = Hideaki Sakurai
| cinematography = Chishi Makiura
| editing        = Lee Percy Toshio Taniguchi
| cover art      = Jim Evans (artist)
| distributor    = Toho
| released       = November 11, 1980 
| runtime        = 89 minutes
| rating         =
| country        = Japan
| awards         =
| language       =
| budget         =
}}
Shogun Assassin, known in Japan as  , is a jidaigeki film made for the British and American markets and released in 1980.  In 2006 it was restored and re-released on DVD in North America by AnimEigo.

Shogun Assassin was edited and compiled from the first two films in the   (Kozure Ōkami: Kowokashi udekashi tsukamatsuru or Wolf with Child in Tow: Child and Expertise for Rent), and most of   (Kozure Ōkami: Sanzu no kawa no ubaguruma or Wolf with Child in Tow: Perambulator of the River of Sanzu). Both were originally released in 1972 in film|1972.  There were six films in all in the series. These in turn were based on the long-running 1970s manga series, Lone Wolf and Cub, created by the writer Kazuo Koike and the artist Goseki Kojima.
 Robert Houston Roger Cormans New World Pictures to the grindhouse movie circuit in the United States, and then later as a video cassette from MCA/Universal Home Video. When released in the United Kingdom by the Vipco video tape label in 1983, Shogun Assassins extreme violence almost caused it to be banned by the Home Office. Vipco played this for publicity in the cover art of their 2000 release on DVD, which was stamped "Banned since 1983!"
 Ogami Ittō, is played by Tomisaburo Wakayama, brother of the producer, Shintaro Katsu, who is known for playing Zatoichi in a series of 26 films starting in the 1960s.

==Plot==
 
As the opening credits roll, an abbreviated version of Ogami Ittōs (Tomisaburo Wakayama|Tomisaburô Wakayama) past as Shogunate Decapitator and his wifes murder by ninja are seen, with Daigorō (Akihiro Tomikawa) providing the narration.

Two hooded samurai attack Ogami while he is pushing a cart with Daigorō inside. Ogami fends off the attack of the first, breaking the samurais sword and splitting his head. The second attacker jumps over the first, with the first still clasping Ogamis blade. Ogami pulls off a handrail from the cart and a blade comes out, transforming it into a spear. Ogami then uses the spear to impale the second attacker. As the first is dying, he reminds Ogami that he is marked for death.

As Ogami and Daigorō sit by a roadside fire and eat their evening meal, Ogami remembers how he offered the infant Daigorō the life-death choice: either Ogamis sword (which would mean that Daigorō would join him on his mission of vengeance against the Shogun) or Daigorōs ball (which would mean that Daigorō would kill be killed, so that he could be with his mother in heaven). Daigorō chooses the sword. The next day, the Shoguns officials bring Ogami the Shoguns orders: either swear eternal loyalty or commit suicide with Daigorō. Ogami decides to fight his way to freedom with Daigorō, only to have his path blocked by the Shogun and his men. The Shogun challenges Ogami to fight Kurando (the Shoguns son) in a duel; if Ogami wins, he wins his freedom. Ogami accepts, and eventually cuts off Kurandos head.

Ogami and Daigorō journey on, never stopping in one place for very long as the Shoguns ninjas are always following them. As they wander, Daigorō recalls how Lord Bizen (Taketoshi Naitô) and his men were given orders to kill him. Even though Bizens men are wearing chain mail beneath their robes, Ogamis skill and blade are too powerful. Ogami lures Lord Bizen into the middle of a stream and uses an underwater sword-slash technique to kill him. Ogami sees the Shogun watching from a distance, and he swears to the Shogun that he will destroy him and all of his ninjas.  

The Supreme Ninja (Kayo Matsuo) receives orders from the Shogun to kill Ogami and Daigorō. Lord Kurogawa (Akiji Kobayashi) doesnt believe the Supreme Ninjas women are up to the task, so she proves otherwise by ordering her ninjas to kill Junai (Kurogawas strongest ninja).

Ogami and Daigorō meet secretly with a client to discuss a business proposition. Ogami is offered to kill Lord Kiru (the Shoguns brother), and in return he will receive ten pieces of gold. Ogami accepts the mission, and he is told that Lord Kiru is being escorted by a three-brother team known as the Masters of Death. 

During Ogami and Daigorōs journey to find Lord Kiru, they are attacked several times by The Supreme Ninjas women. Ogami kills them each time. Ogami finally faces the Supreme Ninja herself. She attacks Ogami with a weighted net that contains fishhooks, but Ogami cuts himself free and the Supreme Ninja flees.

Ogami and Daigorō keep on travelling, but they now come face-to-face with Lord Kurogawas entire ninja force. Pushing Daigorō in his cart to safety, Ogami uses the spear blades in the carts handrails to attack. All but two of the ninjaw are cut down, and Ogami is left wounded. He manages to push Daigorō to the safety of a deserted hut, before collapsing from the loss of blood. Daigorō goes in search of water for his father, bringing it back in his mouth, and takes some food offerings from a roadside shrine, leaving his jacket in honorable exchange.

The Supreme Ninja meets with Lord Kurogawa to report her failure, but Lord Kurogawa has another plan: to strike at Ogami through Daigorō.

Later that night, Daigorō is lured outside the hut by the sound of a woman singing. Waking up to find Daigorō gone, Ogami searches for his son, where he finds Daigorō is a prisoner of Lord Kurogawa and the Supreme Ninja. Daigorō is tied up and suspended over a deep well. Kurogawa demands that Ogami surrenders, or he will drop Daigorō down the well. Ogami refuses, so Kurogawa and his men attack. Kurogawa lets go of the rope suspending Daigorō over the well, but Ogami manages to stamp his foot down on the rope and kill Kurogawa (and his two ninjas) at the same time. Ignoring the Supreme Ninja, who has not moved throughout the fight, Ogami carefully pulls Daigorō up to safety. Instead of killing the Supreme Ninja, Ogami walks away with Daigorō.

Ogami and Daigorō board a ship which is carrying the Masters of Death to their rendezvous with Lord Kiru. Also on board is the Supreme Ninja. During the night, the remaining rebels start a fire on board the ship. In the ensuing inferno, the Masters of Death tell Ogami that they recognize him, but that they will not attack him as along as he makes no move against them. Ogami agrees to their terms, and cuts through the deck ceiling. Ogami then puts Daigorō in his cart and throws them both overboard to safety. The Supreme Ninja attacks Ogami from underwater, but he overpowers her. Getting Daigorō, himself and the Supreme Ninja to shore and to the shelter of a fishermans hut, he strips all three of them naked and gathers them close together, telling the Supreme Ninja that they must share their body heat or die. The Supreme Ninja doesnt understand why he would save her and finds that she cannot kill Ogami or his son. The next day, Ogami and Daigorō leave her there, knowing that she will have to return to the Shogun, report her failure and commit suicide.

The Masters of Death escort Lord Kiru and his entourage through a desert area, where they are attacked by a concealed rebel force. The Masters of Death then fight off and kill all of the rebels. Lord Kiru is then taken to safety. However, they havent gone far before they see Daigorō standing in their way, where he points to reveal Ogami waiting. The Masters of Death finally face off against Ogami, but one by one they are cut down and killed. Ogami then chases after Lord Kirus procession, driving off the guards. Lord Kiru protests that he is the Shoguns brother, but Ogami tells Kiru that the "Shogun means nothing" to him. Ogami then kills Lord Kiru with his sword. 

As he and his father walk away from the carnage, Daigorō looks back one last time and says via voice-over, "I guess I wish it was different ... but a wish is only a wish".

==Cast== Lone Wolf: The Shoguns disgraced, former executioner. The Lone Wolf (also known as Ogami Ittō) is forced to become an assassin after his wife is murdered. He travels throughout the land with his son, Daigorō, and together they seek revenge against the Shogun. They are known as the "Lone Wolf and Cub".  
* Kayo Matsuo as Supreme Ninja: Known as the Supreme Ninja, she is the leader of an all female group of ninja assassins. She is hired by the Shogun to kill the Lone Wolf.
* Minoru Ôki (credited as Minoru Ohki) as Master of Death: The Masters of Death are a team of three ninja brothers that are hired to escort (and protect) the Shoguns brother (Lord Kiru).
* Shôgen Nitta as Master of Death: A member of the Masters of Death.
* Shin Kishida as Master of Death: A member of the Masters of Death.
* Akihiro Tomikawa (credited as Masahiro Tomikawa) as Daigorō: The Lone Wolfs three-year-old son.
* Reiko Kasahara as Azami
* Akiji Kobayashi (credited as Shoji Kobayashi) as Lord Kurogawa 
* Taketoshi Naitô as Lord Bizen: One of the Shoguns sons. 
* Tokio Oki as The Shogun: A sworn enemy of the Lone Wolf, the Shogun murdered the Lone Wolfs wife for his defiance. Since then, the Shogun has tried repeatedly to murder the Lone Wolf and his son (Daigorō).

==Post-production==
===Editing===
Shogun Assassin was dubbed into English whereas the originals are in Japanese language|Japanese.  The film, being compiled from separate stories, uses a much-simplified version of the situation.  For instance, any mention of clan war is gone and the opponent Retsudo is simply called "The Shogun."
 Jim Evans. American actress Sandra Bernhard and director (and former radio actor) Lamont Johnson provided voices in the dubbed edition.

==Soundtrack== Moog Moog Modular synthesizer system at Wonderland, Lindsays private recording studio. (The performance is credited to "The Wonderland Philharmonic".) However, certain music portions from the original Kozure Ôkami films are still part of the soundtrack.

==Release and Reception==
===Reviews===
Vincent Canby of The New York Times, wrote "Shogun Assassin... is as furiously mixed up as Whats Up, Tiger Lily? the classic that Woody Allen made by attaching an English soundtrack to a grade-Z Japanese spy movie. Aside from the little-boys narration, the movies not much fun once youve gotten the picture, which is that of a tubby, outcast samurai wandering the length and breadth of Japan, pushing an antique baby carriage that contains his tiny, remarkably observant son." 
 DVD Talk said, "A radical reworking of not one but two Japanese movies combined into a single action-filled extravaganza, Shogun Assassin floored audiences with its dream-like, poetic action and pressure-cooker bloodletting." 

J.C. Maçek III of WorldsGreatestCritic.com wrote, "Shogun Assassin as a product of artistic and stylistic films, is an artistic and stylistic film itself, with the dubbing (which includes even the voice of Sandra Bernhard) working as a genre-setting asset rather than a liability. This keeps the cartoonish, yet somehow still deadly, mood that penetrates each reel. In that this film was a huge influence on later works (like Kill Bill), its safe to bet that youve got geysers of blood shooting from every wound and plenty of dismemberment and painful-looking slashes." 

==Sequels==
AnimEigo has released four sequels: Shogun Assassin 2: Lightning Swords of Death, actually the third film in the Baby Cart series,  ; Shogun Assassin 3: Slashing Blades of Carnage, which is the fourth film in the Baby Cart series,  ; Shogun Assassin 4: Five Fistfuls Of Gold, which is the dubbed version of the fifth film,  ; and Shogun Assassin 5: Cold Road to Hell, the dubbed version of  , the sixth and final film in the series.

==In popular culture==
Several audio clips from Shogun Assassin are used on rapper GZAs classic album Liquid Swords (produced by RZA). In addition, the film is invoked in Kill Bill Volume 2 (for which RZA provided original music) in the end scenes where the protagonist and her four-year-old daughter watch it as a bedtime story.
 sampled in Cage (rapper)|Cages 1997 single "Agent Orange", also on his 2003 album Movies for the Blind.

==See also==
*Jidaigeki
*Exploitation film

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 