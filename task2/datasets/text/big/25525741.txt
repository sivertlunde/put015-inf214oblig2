Do Not Disturb (1965 film)
{{Infobox film
| name           = Do Not Disturb
| image          = Do Not Disturb 1965 poster.jpg
| caption        = Theatrical release poster George Marshall
| producer       = Martin Melcher Aaron Rosenberg
| writer         = William Fairchild (play)
| screenplay     = Richard L. Breen Milt Rosen
| narrator       =
| starring       = Doris Day Rod Taylor
| music          = Lionel Newman Alexander Courage
| cinematography = Leon Shamroy Robert L. Simpson
| distributor    = 20th Century Fox
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $3.89 million 
| gross          = $8,000,000 
| preceded by    =
| followed by    =
}}

Do Not Disturb is a DeLuxe Color CinemaScope (1965 in film|1965) romantic comedy film directed by Ralph Levy, starring Doris Day and Rod Taylor as Janet and Mike Harper, a married couple who relocate to England when Mike is transferred by the company for whom he works. 

==Plot==
American couple Mike and Janet Harper (Rod Taylor and Doris Day) move to England for Mikes work, his company which deals in wool textiles and wool fashions. Despite Mikes want for them to live in a flat in the heart of London, Janet, who is not a big city girl, ignores his want and instead finds them a house to rent thirty miles outside of London in Kent, which means that Mike has to commute into town by train. This commute is not ideal for Mike, who often for convenience stays in one of the companys flats in town rather than go home. This commuting situation makes Janet feel even more neglected than she already did previously.

Janet believes Mike may be having an affair with his secretary quickly turned assistant, Claire Hackett (Maura McGiveney). Janets beliefs are fueled in part by the Harpers busybody landlady, Vanessa Courtwright (Hermione Baddeley), who thinks Janet can play Mikes game by entering into an affair of her own, whether it be real or made-up. It has the potential to be real with the arrival onto the scene of Frenchman Paul Bellari (Sergio Fantoni), an antiques dealer and decorator who Janet hires to decorate the house. Although neither Mike nor Janet had any initial thoughts of cheating on the other, Claire and Paul may have thoughts of their own, especially when Claire with Mike, and Paul with Janet, are thrown into one compromising position after another, which in and of themselves could potentially break up the Harpers marriage regardless. 

==Cast==
* Doris Day as Janet Harper
* Rod Taylor as Mike Harper
* Hermione Baddeley as Vanessa Courtwright
* Sergio Fantoni as Paul Bellari
* Reginald Gardiner as George Simmons
* Maura McGiveney as Claire Hackett
*  Aram Katcher as Culkos
* Leon Askin as Willie Langsdorf
* Lisa Perav as Alicia Petrova
* Michael Romanoff as Delegate
* Albert Carrier as Claude Reynard
* Barbara Morrison as Mrs. Ordley
* Dick Winslow as One-Man Band / Accordion Player
* Raquel Welch as Woman in lobby
* Britt Ekland as Party Girl

==Production== George Marshall had to step in as director. This caused the film to finish behind schedule. 
==Reception==
The film had admissions of 10,730 in France. 

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 