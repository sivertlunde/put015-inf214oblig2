Ut av mørket
{{Infobox film
| name           = Ut av mørket
| image          = 
| caption        = 
| director       = Arild Brinchmann
| producer       = 
| writer         = Arild Brinchmann Alex Brinchmann
| starring       = Urda Arneberg
| music          = 
| cinematography = Sverre Bergli
| editing        = 
| distributor    = 
| released       = 27 February 1958
| runtime        = 104 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
}}

  ( ) is a 1958 Norwegian film directed by Arild Brinchmann. It was entered into the 8th Berlin International Film Festival.   

==Cast==
* Urda Arneberg - Kari Holm
* Pål Skjønberg - Per Holm, architect
* Ola Isene - Enger, chief physician
* Marit Halset - Doctor Krag
* Mona Hofland - Liv Holst, Per Holms colleague
* Turid Steen - Patient
* Alfred Maurstad - Director
* Erling Lindahl - Physician
* Evy Engelsborg - Nurse

==References==
 

==External links==
* 

 
 
 
 
 
 
 