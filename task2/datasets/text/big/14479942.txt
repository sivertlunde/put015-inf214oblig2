Mithunam (1993 film)
 
 
{{Infobox film
| name           = Mithunam
| image          = Mithunam.jpg
| caption        = DVD Cover
| director       = Priyadarsan
| producer       = Pranavam Arts Sreenivasan
| Urvashi  Innocent  Sreenivasan K.P.A.C. Lalitha
| music          = Songs:  
| cinematography = S. Kumar
| editing        = N. Gopalakrishnan
| distributor    = Pranamam Pictures
| released       =  
| runtime        = 130 minutes 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} 1993 Malayalam Malayalam satirical Urvashi in the lead roles.  The film was produced by Mohanlal under the banner of Pranavam Arts and was distributed by Pranamam Pictures. And the music was composed by M. G. Radhakrishnan.It was released along with Suresh Gopis Ekalavyan (film)|Ekalavyan. The movie got positive reviews. And nowadays the movie has a big cult to follow. This is Mohanlals 200th film.

== Plot ==
Sethumadhavan (Mohanlal) has plans to start a biscuit factory . But the bureaucracy and the corruption of the officials are the hurdles in front of him. Along with that he has to solve the problems in his dysfunctional family. Added to that he marries his long-term fiancée Sulochana (Urvashi (actress)|Urvashi), who always cribs that he doesnt have love towards her, which he had shown before their marriage. .

==Cast (in credits order)==
*Mohanlal as Sethumadhavan kurup Sreenivasan as Preman (Friend and Assistant to Sethu) Urvashi as Sulochana (Wife of Sethu)Dubbed by Bhagyalakshmi
*Jagathi Sreekumar as Suguthan (Brother-in-law of Sethu) Innocent as Lineman K. T. Kurup (Elder brother of Sethu)
*Thikkurisi Sukumaran Nair as Kurup Master(Father of Sethu)
*Sankaradi as Ammavan of Tharavadu (Sethus uncle)
*C. I. Paul as Government Official Janardanan as Sivaraman
*Kuthiravattam Pappu as Palisa Peethambaran
*Nedumudi Venu as Cherkkonam Swamy Meena as housemaid
*Sukumari as Swamini Amma
*Zeenath as Subadra
*K. R. Valsala as Renuka
*Kuttedathi Vilasini as Syamas kunjamma
*Kozhikode Narayanan Nair as Village Officer Usha as Shyama
*Kanakalatha as Sivaramans wife Kaveri Sulochanas sister
*James as Paappi

== Crew ==
* Cinematography: S. Kumar
* Editing: N. Gopalakrishnan nair
* Art: Sabu Cyril
* Makeup: Vikraman Nair, Salim
* Costumes: M. M. Kumar, Murali
* Choreography: Kumar
* Stunts: Thyagarajan master
* Advertisement: Gayathri venugopal
* Processing: Gemini Pictures Circuit
* Stills: T. N. Ramalingam
* Production controller: Radha K. Nair
* Office administration: K. Manoharan
* Outdoor: Vishakh pokkiriraja
* Associate director: Murali Nagavalli

== Songs ==
The songs in the movie has been composed by M. G. Radhakrishnan and was written by O. N. V. Kurup. The songs were distributed by Magna Sound.
 Background Score for the movie was composed by S. P. Venkatesh.

; Songs list
* Poomanjin: M. G. Sreekumar, Sujatha Mohan
* Allimalar kaavil pooram kaanan: M. G. Sreekumar
* Njattuvelakkiliye: K. S. Chithra
* Njattuvelakkiliye: M. G. Sreekumar

==References==
 

==External links==
*  
* http://popcorn.oneindia.in/title/6198/midhunam.html
* http://www.chakpak.com/movie/midhunam/8738
* http://www.sukip.com/Movies/Midhunam_1993
*  http://www.thecompleteactor.com/all-movies.html

 

 
 
 
 
 