The Last Journey of Ninoy
{{Infobox film
| name           = The Last Journey of Ninoy
| image          = The Last Journey of Ninoy.jpg
| caption        = Film poster
| released       =  
| director       = Jun Reyes
| producer       = Tony Gloria
| writer         = Daime Dumaup Ma. Jovita Zarate Jun Reyes Divine Gil Reyes
| editing        = Claire Villareal
| music          = Jasper Perez
| runtime        = 160 minutes Filipino English English
| See cast
| distributor     = Unitel Productions, Inc.
}} 
The Last Journey of Ninoy is a documentary film directed by Jun Reyes that premiered on 21 August 2009 in commemoration of Ninoy Aquino Day and on 23 August at ABS-CBN.    The film includes the final interview given by Benigno Aquino, Jr.|Aquinos wife who then became the first woman to Philippine presidency, Corazon Aquino.   It was produced by Unitel Productions, Inc. and the Benigno Aquino Jr. Foundation.

==Plot== he was assassinated at the Manila International Airport. It features interviews and commentaries from Aquinos wife and former Philippine president, Corazon Aquino.

==Cast== Ninoy Aquino
*Pia Millado as Cory Aquino

==Release and reception==
The film premiered on 21 August 2009 at the Powerplant Mall in Makati.    In 23 August, the film premiered in national television simultaneously via ABS-CBN with the ABS-CBN News Channel and The Filipino Channel. 

===Critical response===
Reviews for the documentary film were generally positive. Rose Feliciano of the ABS-CBN News wrote "I was touched and I cried at the end of the film!" and "As I am! This film must be shown to all students nationwide...and to the public in general .....again and again! It truly inspires!" 

==Accolades==
The film was a finalist in the 2009 Aljazeera International Documentary Film Festival and in the 2009 New York Film Festival. 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 