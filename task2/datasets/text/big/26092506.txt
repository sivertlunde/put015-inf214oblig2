Okul (film)
 
{{Infobox film
| name           = Okul
| image          = Okul.jpg
| image_size     = 
| caption        = Theatrical film poster
| director       = Yağmur Taylan Durul Taylan
| producer       = Sinan Çetin
| writer         = Doğu Yücel
| narrator       =
| starring       = Nehir Erdoğan Burak Altay Sinem Kobal Melisa Sözen Berk Hakman
| cinematography = Soykut Turan
| music          = Kevin Moore
| editing        = 
| distributor    = 
| released       = 9 January 2004
| runtime        = 105 mins
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    =
}} 2004 Cinema Turkish horror-comedy film  directed by the Taylan brothers (Yağmur Taylan and Durul Taylan). It is adapted from the novel Hayalet Kitap by Doğu Yücel who also wrote the screenplay.      Kevin Moore provided the films soundtrack.

==Plot==
The school magazine editor Gökalp is in love with Güldem, a girl in his class. He writes stories and letters to her in order to win her heart, but she isnt interested. So he commits suicide after leaving a secret letter. A year after his death, strange things begin to happen. These are not just experienced only by Güldem, but also by those close to her such as her boyfriend Ersin, her best friends Şebnem and Ceyda, the new school magazine editor Umut and Vedat Bey who spies on the students with cameras. Gökalps ghost returns for revenge just as the ÖSS exam week is about to begin.

==Cast==
*Nehir Erdoğan (Güldem)
*Burak Altay (Gökalp)
*Sinem Kobal (Şebnem)
*Melisa Sözen (Ceyda)
*Berk Hakman (Ersin)
*Barış Yıldız (Umut)
*Caner Özyurtlu (Ediz)
*Serdar Deniz (Giz)
*Ahmet Mümtaz Taylan (Vedat Bey)
*Deniz Akkaya (Alev)
*Emre Kınay (Kemal)
*Berk Hakman (Ersin)
*Barış Yıldız (Umut)
*Caner Özyurtlu (Ediz)
*Serdar Deniz (Giz)
*Deniz Güngören (Orçun)

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 