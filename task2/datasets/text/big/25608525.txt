Heroes of the Alamo
:For other films about the Alamo, see Alamo_(disambiguation)#Films.
{{Infobox Film
| name           = Heroes of the Alamo
| image          = Heroesofthealamo1.jpg
| caption        = original film poster
| director       = Harry L. Fraser
| producer       = Anthony J. Xydias 
| writer         = Roby Wentz
| starring       = Bruce Warren Lane Chandler Earl Hodgins Rex Lease Roger Williams
| music          = Lee Zahler
| cinematography = Robert Cline
| editing        = 
| art director   = 
| titles         =  
| distributor    = Sunset Productions Columbia Pictures Astor Pictures
| released       =   24 September 1937
| runtime        = 75 min.
| country        =   English
}}
Heroes of the Alamo (1937) is a low-budget retelling of the events of the Texas Revolution and the Battle of the Alamo.  It was produced by Anthony J. Xydias and reuses the battle scenes of his 1926 silent film Davy Crockett at the Fall of the Alamo. About 35 minutes of the latter film is available on the DVD of Heroes of the Alamo, all that remains of the silent film.

==Plot== Almaron (Bruce Santa Anna (Julian Rivero), Sam Houston (Edward Piel) appointed General to build the army of Texas, and Dickinsons participation in both the Battle of Gonzales and the Battle of the Alamo.

==Cast== Al Dickinson Anne Dickinson
*Earle Hodgins  ...  Stephen F. Austin
*Lane Chandler  ... Col. Davy Crockett Roger Williams ...  Col. Jim Bowie
*Rex Lease  ...  Col. William B. Travis
*Jack C. Smith  ...  William H. Wharton
*Lee Valanios  ...  Col. James Bonham
*Edward Peil Sr.  ...  Gen. Sam Houston
*Julian Rivero  ...  Gen. Antonio López de Santa Anna
*Willy Castello  ...  Gen. Martín Perfecto de Cos
*Paul Ellis  ...  Gen. Manuel Fernández Castrillón

==Release== Texas Centennial but fell behind in his plans. 

Columbia Pictures acquired the film for 1938 release  and changed the billing of the actors in the film. 
 The Yellow Rose of Texas being sung by the defenders though it had not been written at that time.

This was the last film Xydias produced. When World War II broke out he was in the Philippines and was interned by the Japanese.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 