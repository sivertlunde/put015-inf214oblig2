Filmed in Supermarionation
{{Infobox film
| name = Filmed in Supermarionation

|image=File:Filmed in Supermarionation.jpg

|caption=Filmed in Supermarionation poster
| director = Stephen La Rivière
| producer = Stephen La Rivière Andrew T. Smith Tim Beddows
| writer = Andrew T. Smith Stephen La Rivière
| starring = Gerry Anderson Sylvia Anderson
| music = Barry Gray
| distributor = Network Distributing
| released       =  
| runtime = 114 minutes
| country = United Kingdom
| language = English
}} UK on 11 October, 2014, having been premiered at the British Film Institute on 30 September, 2014. It was subsequently released on DVD and Blu-ray.  

==Synopsis== Century 21 studios under the watchful eyes of Gerry and Sylvia Anderson. The documentary is hosted by Lady Penelope and Parker, the puppet stars of Thunderbirds (TV series)|Thunderbirds, who seek to uncover the story behind their creation.  

==Production== Thunderbirds voice cast and puppets and sets were recreated to as close to the specification of the original Supermarionation series as possible. In addition to attempting to recreate 1960s film-making techniques, 21st century methods were also employed in the interest of matching the original productions. Although special effects sequences were shot on 35mm film stock, puppet sequences were captured digitally. This allowed for the image to be manipulated in post production to better match the unique qualities of 1960s film photography. The digital workflow also allowed for wire removal and digital set extensions to be utilised. 

==Reception==

Upon release, Filmed in Supermarionation was generally well received by critics writing across a wide range of publications.   Peter Bradshaw of The Guardian gave the film four stars, writing that, "There is something very romantic about this success story of British entrepreneurial creativity."    Rich Trenholm of CNET was similarly positive in stating, "the documentarys vibrant storytelling captures the vitality, innocence and sense of joy of the series themselves".    

Martin Townsend, in his New Years editorial for The Sunday Express, enthused, "The likes of Apple and Microsoft may be very impressive companies, but if I wanted to inspire children to be creative entrepreneurs Id show them the Supermarionation film."  

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 