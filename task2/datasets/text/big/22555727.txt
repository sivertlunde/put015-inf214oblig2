The Life of General Villa
{{Infobox film
| name           = The Life of General Villa
| image          =
| caption        =
| director       = Christy Cabanne
| producer       = H. E. Aitken Frank N. Thayer D. W. Griffith
| writer         = Frank E. Woods
| starring       = Pancho Villa Raoul Walsh
| cinematography = Raoul Walsh
| distributor    = Mutual Film Corporation Mexican War Film Corp.
| released       =  
| runtime        = 105 min
| country        = United States Silent English intertitles
| budget         =
| gross          =
}} silent Biographical biographical Action (genre)|action&ndash;drama film starring Pancho Villa as himself, shot on location during a civil war. The movie incorporated both staged scenes and authentic live footage from real battles during the Mexican Revolution, around which the plot of the movie revolved.

The film was produced by D.W. Griffith and featured future director Raoul Walsh as the younger version of Villa.  Walsh wrote extensively about the experience in his autobiography Each Man in His Time, describing Villas charisma as well as noting that peasants would knock the teeth out of corpses with rocks in the wake of firing squads in order to harvest the gold fillings, which was captured on film and had the projectionists vomiting in the screening room back in Los Angeles.

Currently the film is presumably lost film|lost, with only unedited fragments and publicity stills known to exist.

The filming of the movie and associated events were dramatized in the film And Starring Pancho Villa as Himself (2003) with Antonio Banderas starring as Villa.

==Production==
 ]]
Pancho Villas reason for starring in the movie was financial as he needed funds to fight the Mexican Revolution. He eventually signed a contract with the Mutual Film Corporation where he received a $25,000 advance and was promised 50% of the profits from the film for agreeing to let the company shoot his battles in daylight, and for re-enacting them if more footage was needed. (The contract resides in a museum in Mexico City at the Archivo Federico Gonzalez Garza, folio 3057.)

The following year, Walsh played John Wilkes Booth in Griffiths epic The Birth of a Nation and directed the first gangster movie, Regeneration (1915 film)|Regeneration, on location in the Bowery on the Lower East Side of Manhattan.

==Cast==
* Pancho Villa as himself
* Raoul Walsh as Villa as a young man Teddy Sampson as Villas Sister Irene Hunt as Villas Sister Walter Long as Federal Officer
* W. E. Lawrence as Federal Officer
* Juano Hernández as Revolutionary Soldier

==See also==
* List of lost films

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 