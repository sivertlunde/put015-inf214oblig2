Bear (2010 film)
{{Infobox film
| name           = Bear
| image          = Bear-2010-poster.jpg
| alt            =
| caption        =
| director       = John Rebel
| producer       = Freddie Wong Alicia Martin Roel Reiné Ethan Wiley
| writer         = Roel Reiné Ethan Wiley
| starring       = Katie Lowes Brendan Michael Coughlin Mary Alexandra Stiefvater Trevor Morris
| cinematography = John Rebek
| editing        = Herman P. Koerts
| studio         = Wiseacre Films Rebel Film BV
| distributor    = A-Film Distribution
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Bear is a natural horror Z movie directed by John Rebel and stars Patrick Scott Lewis and Katie Lowes.  The film was released on June 4, 2010 in the United Kingdom.

==Plot==
Businessman Sam, his wife Liz and his musician brother Nick with his girlfriend Christine are driving through a remote countryside to their fathers birthday dinner. Several miles into a back road shortcut, they get a flat tire and are unable to get a cell phone signal to call for help. While repairing the tire, Sam berates Nick for wasting his life being a musician, his latest fling with Christine being another mistake on his judgement list. As they are arguing, they are approached by a grizzly bear. Despite Nicks efforts to convince the group to calmly leave, Sam takes matters into his own hands and shoots the bear down with a handgun. After the bear dies, they are approached by a larger male bear who charges them in revenge, causing them to retreat into their minivan. In his rage, the bear overturns the minivan, trapping the humans inside. After striding around for several minutes, the bear leaves, allowing the crew to turn the minivan over. But as they start driving, the axles break under both sets of tires, stranding them again. The group tries to leave on foot, but they are ambushed by the bear who chases them to an out-of-ground pipe. He tricks them into leaving for the minivan and attacks again; this time he is able to catch Christine who is killed while the others watch.

Nick begins to think that the bear is taking out his revenge on them one-by-one due to a native American legend that bears are actually the reincarnated spirits of shaman and are capable of human thoughts and emotion. Sam dismisses this and they set a trap to try to keep the bear in the car in order for them to escape. The trap is sprung, but Nick is nearly killed by the bear, forcing Sam and Liz to release the bear and retreat back into the vehicle themselves. Sam opts as a marathon runner to jog the several miles to the restaurant, but when he arrives he is attacked by the bear before he can get help. Meanwhile, Liz tells Nick that she and Sam have been having serious financial troubles which has caused a rift in their marriage putting him into extraordinary debt and possibly under scrutiny for embezzlement from Sams company, which is the reason he gave their father a Porsche (as the banks then wouldnt be able to collect it). Sam is returned rather violently by the bear and released to climb into the car. Nick surmises there must be a reason Sam had been returned, and Liz reveals to him that shes pregnant. But it was established earlier that due to the rift in their marriage, they hadnt slept together in about six months while she is two months pregnant. Nick realizes that he is the father, from an affair he and Liz had months prior. This breaks Sams heart as Nick realizes the true reason the bear had locked all three of them together; Sam, realizing that Liz might be happier with Nick, offers to sacrifice himself and attack the bear so the other two can escape. As he is dying, Nick rushes to help and is killed as well. The bear then approaches Liz, who sinks to her knees as the bear sniffs her, closing her eyes ready to die. But the attack never comes, and the bear instead leaves her alone, likely having sensed her pregnancy. As she begins down the road by herself, the camera turns to the bodies of Sam and Nick lying next to each other and the screen goes black.

==Cast==
* Katie Lowes as Christine
* Mary Alexandra Stiefvater as Liz
* Patrick Scott Lewis as Sam
* Brendan Michael Coughlin as Nick

==Soundtrack== Trevor Morris. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 