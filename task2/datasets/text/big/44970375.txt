Mugiyada Kathe
{{Infobox film 
| name           = Mugiyada Kathe
| image          =  
| caption        =  Durai
| producer       = P H Rama Rao Pandari Bai Durai
| Durai
| Rajesh Sumithra Sumithra Pandari Bai Dinesh
| music          = Rajan-Nagendra
| cinematography = V. Manohar
| editing        = M R Bhaskaran
| studio         = Sri Panduranga Productions
| distributor    = Sri Panduranga Productions
| released       =  
| country        = India Kannada
}}
 1976 Cinema Indian Kannada Kannada film, Durai and produced by P H Rama Rao and R Pandari Bai. The film stars Rajesh (Kannada actor)|Rajesh, Sumithra (actress)|Sumithra, Pandari Bai and Dinesh in lead roles. The film had musical score by Rajan-Nagendra.  

==Cast==
  Rajesh
*Sumithra Sumithra
*Pandari Bai
*Dinesh
*M S Sathya
*Bhaskar
*Sundar Krishna Urs
*K Deepak
*Mynavathi
*Ramadevi
*R T Rama
*Padmanjali
*Premakumari
*Indira Arasu
*K. S. Ashwath in Guest Appearance
*T. N. Balakrishna in Guest Appearance Gangadhar in Guest Appearance
*Advani Lakshmi Devi in Guest Appearance
*Shailashree in Guest Appearance
*Radhika
 

==Soundtrack==
The music was composed by Rajan-Nagendra.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Janaki || Chi. Udaya Shankar || 03.39 
|- 
| 2 || Mutthu Uruli Hogi || S. P. Balasubrahmanyam || Chi. Udaya Shankar || 03.35 
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 