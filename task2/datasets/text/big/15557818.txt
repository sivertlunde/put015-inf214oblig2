Come Next Spring
{{Infobox Film
| name           = Come Next Spring
| image_size     = 
| image	=	Come Next Spring FilmPoster.jpeg
| caption        = 
| director       = R. G. Springsteen
| producer       = 
| writer         = Montgomery Pittman
| narrator       = 
| starring       = Ann Sheridan Steve Cochran
| music          = Max Steiner
| cinematography = 
| editing        = 
| distributor    = Republic Pictures
| released       = March 9, 1956
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Come Next Spring (1956) is a drama film made in Trucolor for Republic Pictures starring Steve Cochran as a former alcoholic who returns to the wife he deserted years ago, played by Ann Sheridan, and their children. 
 Scott Walker Sergeant York.

==Production==
Steve Cochran formed his own production company, Robert Alexander Productions after his actual first two names. Come Next Spring was his first film, written by Cochrans friend Montgomery Pittman and featuring Pittmans stepdaughter Sherry Jackson. Filmed on Sacramento locations, Republic promised Cochran an "A Picture" release but released it as the lower half of a double feature.  

==Cast==
*Ann Sheridan as Bess Ballot
*Steve Cochran as Matt Ballot
*Walter Brennan as Jeffrey Storys
*Sherry Jackson as Annie Ballot
*Richard Eyer as Abraham Ballot
*Edgar Buchanan as Mr. Canary
*Sonny Tufts as Leroy Hytower Harry Shannon as Mr. Totter
*James Westmoreland as Bob Storys
*Mae Clarke as Myrtle

==Notes==
 

==External links==
* 
*  
*  


 
 
 
 
 
 

 