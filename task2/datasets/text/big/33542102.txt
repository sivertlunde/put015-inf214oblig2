The Stolen Kiss
 
{{infobox film
| name           = The Stolen Kiss
| image          = The Stolen Kiss (1920) - Binney & La Rocque.jpg
| imagesize      = 200px
| caption        = Still with Binney and La Rocque
| director       = Kenneth Webb
| producer       = Realart
| writer         = Lucille Van Slyke (novel: Little Miss the Day) Kathryn Stuart (scenario)
| starring       =  Constance Binney
| music          =
| cinematography = George Folsey Harry Stradling (asst camera)
| editing        =
| distributor    = Realart
| released       = April 4, 1920
| runtime        = 
| country        = United States Silent (English intertitles)
}} 1920 American silent film romance drama starring Constance Binney.  Kenneth Webb directed. The Realart Company, an affiliate of Paramount Picture, produced the film. A print is preserved at the British Film Institute, London.  

==Cast==
*Constance Binney - Felicia Day / Octavia, her Mother
*Rod La Rocque - Dudley Hamilt
*George Backus - Maj. Trenton
*Bradley Barker - John Ralph
*Robert Schable - Allen Graemer
*Frank Losee - Peter Alden
*Richard Carlyle - James Burrell
*Edyna Davies - Dulcie
*Ada Neville - Mlle. DOrmy (as Ada Nevil)
*Agnes Everett - Marthy
*Eddie Fetherston - Jack Hall
*Jean Lamb - Mrs. Hall
*Joseph Latham - Tom Stone

==References==
 

==External links==
 
* 
* 
*  (University of Washington, Sayre Collection)

 
 
 
 
 
 
 


 
 