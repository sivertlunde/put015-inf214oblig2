Boy Meets Girl (1938 film)
 
{{Infobox film
| name           = Boy Meets Girl
| image          = Boy Meets Girl (1938 film) poster.jpg
| caption        = Theatrical release poster
| director       = Lloyd Bacon
| producer       = Samuel Bischoff
| screenplay     = Bella Spewack Sam Spewack
| based on       = Boy Meets Girl (play) by Bella Spewack Sam Spewack Pat OBrien
| music          = 
| cinematography = Sol Polito William Holmes
| studio         = Warner Bros.
| distributor    = Warner Bros. 
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Pat OBrien, Marie Wilson, Bella and Sam Spewack is based on their 1935 stage play of the same name, which ran for 669 performances on Broadway theater|Broadway.   The two zany screenwriters played by Cagney and OBrien were based on Ben Hecht and Charles MacArthur, while Ralph Bellamys part as the producer was based on Darryl Zanuck of 20th Century Fox. 

==Plot==
Two screenwriters, Law and Benson ( ), a good-looking young Englishman who is an extra on one of the studios films.

Larry is tired of having a scene-stealing baby as a co-star, and his agent, Rossetti (Frank McHugh), devises a scheme to have Larry woo Susie in order to marry her and, as Happys father, get him out of show business and into a normal life. When they hear about this, Benson and Law hire Rodney, unaware that Susie knows him, to pretend to be Susies long-lost husband, Happys father; Rodney thinks its just an acting job, and is not in on the deception.  Their plan works, and Larry disavows any planned future with Susie, but an wanted result is that Baby Happy is fired due to the scandal.  

When their plot is exposed, the two writers are fired, and Law makes plans to move to Vermont to suffer and write the Great American Novel, but Benson, whose wife has just left him, is too deep in debt to leave - so they come up with another plan.  They have a friend in London send a wire to B.K. (Pierre Watkin), the head of the studio, with an offer from a British studio to buy it, as long as Baby Happy is under contract.  Under the circumstances, Happy, Benson and Law are all re-hired.

Just then, Rodney bursts into the office and asks Susie to marry him and come to England.  Benson and Law try to persuade Susie that Rodney is a no-good cheat and philanderer only after her money, but the American representative of the British studio shows up to identify him as the son of an English lord.  The rep also reveals that the plan to purchase the studio is a fraud. Producer C.F. wants to fire Benson and Law again, but their new contracts are iron-clad.  Susie leaves with Rodney, heading for England, and C.F. learns that his wife is pregnant.

==Cast==
  
* James Cagney as Robert Law Pat OBrien as J. Carlyle "J.C." Benson Marie Wilson as Mrs. Susan "Susie" Seabrook
* Ralph Bellamy as C. Elliott "C.F." Friday
* Frank McHugh as Rossetti, Larrys agent
* Dick Foran as Larry Toms
* Bruce Lester as Rodney Bevan
* Ronald Reagan as radio announcer at premiere
 
* Paul Clark as Happy
* Penny Singleton as Peggy
* Dennie Moore as Miss Crews
* Harry Seymour as Slade, a songwriter
* Bert Hanlon as Green, a songwriter
* James Stephenson as Major Thompson
* Pierre Watkin as B.K., the head of the studio
 

==Production==
The Hollywood Reporter said that Marion Davies was originally considered to play Susie; according to modern sources, Davies lover, William Randolph Hearst, rejected the film for Davies because he thought it was too racy for her,  while others report that he did so because his Cosmopolitan Productions was severing relations with Warner Bros.  Still others put the decision on Davies herself, upset with cast changes for the film or with the size of her part. In any case, Davies did not do the picture, and never made another film. 

Boy Meets Girl was Cagneys eighth picture with comedy-specialst director   

In the scene which takes place at a glitzy Hollywood movie premiere, with Ronald Reagan &ndash; newly signed to Warner Bros. &ndash; playing the radio announcer, the name of the film is given as The White Rajah, starring Errol Flynn.  This was the name of a script actually written by Flynn, but rejected for production as being too weak.  Supposedly, Flynn was not happy with the in-joke. 

==Reception==
Reviewers at the time of release thought that the requirements of the   

==References==
 

==External links==
*  
*  
*  
*  at Internet Archive

 
 

 
 
 
 
 
 
 
 
 
 
 