And God Created Woman (1956 film)
 
{{Infobox film name          = And God Created Woman image         = Et_Dieu.jpg image_size    = 225px caption       = French theatrical release poster director      = Roger Vadim producer      = Claude Ganz Raoul Lévy writer        = Roger Vadim Raoul Lévy starring      = Brigitte Bardot Curd Jürgens Jean-Louis Trintignant music         = Paul Misraki
|cinematography= Armand Thirard editing       = Victoria Mercanton distributor   = Éditions René Chateau Kingsley International Pictures (US) NEW HIGHLIGHTS ON THE LOCAL SCREEN SCENE: Movie Showcase to Be a Part of Seven Arts Center--Imports--Other Items
By A.H. WEILER. New York Times (1923-Current file)   16 June 1957: X7.  Criterion Collection (DVD) released      =   21 October 1957 runtime       = 95 minutes country  France
|language French  budget = $300,000 (est.) Vadim Is Frank On, Off Screen
Scheuer, Philip K. Los Angeles Times (1923-Current File)   20 July 1965: C8.  
| gross = $4 million (US) Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 276  3,919,059 admissions (France) |}}
 French drama film directed by Roger Vadim and starring Brigitte Bardot.  Though not her first film, it is widely recognized as the vehicle that launched Bardot into the public spotlight and immediately created her "sex kitten" persona, making her an overnight sensation.
 distributor Kingsley-International representation of sexuality in American cinema, and most available prints of the film were heavily edited to conform with the prevailing censorial standards of 1957. 
 remake of the film was directed by Vadim and released in 1988.

==Plot==
Juliette (Brigitte Bardot) is an 18-year old orphan with a high level of sexual energy. She makes no effort to restrain her natural sensuality &ndash; lying nude in her yard, habitually kicking her shoes off and walking around barefoot, and disregarding many societal restraints and the opinions of others. These factors cause a stir and attract the attentions of most of the men around her.

Her first suitor is the much older and wealthy Eric Carradine (Curd Jürgens). He wants to build a new casino in town, but his plans are blocked by a small shipyard on the stretch of land which he needs for the development; the shipyard is owned by the Tardieu family.

Antoine, the eldest Tardieu son (Christian Marquand), returns home for the weekend to discuss the situation and Juliette is waiting for him to take her away with him. His intentions are short-term, and he spurns her by leaving town without her.

Tiring of her antics, Juliettes guardians threaten to send her back to the orphanage. To keep her in town, Carradine pleads with Antoine to marry her, which he laughs off, but his naive younger brother Michel (Jean-Louis Trintignant), secretly in love with Juliette, rises to the challenge and proposes. Despite being in love with his older brother, she accepts. 

When Antoine is contracted to return home for good, the trouble starts for the newlyweds. In a huff, Juliette takes off in a boat belonging to the family, gets in trouble, and has to be saved by Antoine. The pair are washed up on a wild beach, and make love.

Juliette begins acting bizarrely. She takes to her bed, claiming to have a fever. She confesses to Michels little brother Christian (Georges Poujouly) about her fling with Antoine on the beach. Maman (Marie Glory) hears about it, tells Michel when he comes home, and advises that he kick Juliette out in the morning. Michel goes to their room to talk with Juliette, but she has gone off to the Bar des Amis to drink and dance. 

Michel goes looking for her, but Antoine locks him inside, telling him that he should forget that bitch whore. Michel tries to shoot the lock away, but it doesnt work. He winds up having to fight his brother for the key. 

Juliettes friend Lucienne (Isabelle Corey) calls Eric to tell him how bizarre Juliette is acting, and Eric comes over to collect her, but Juliette refuses to go. Eventually, Michel catches up with Juliette at the Bar, but she refuses to even talk with him and goes on dancing. Michel orders her to stop, but she pays him no heed, so he takes out his gun. Just as hes about to shoot her, Eric steps in and takes a bullet in his hand. Antoine offers to drive Eric to a doctor, and they leave the Bar. Michel angrily slaps Juliette four times, and Juliette smiles at him. On their way to the doctor, Eric tells Antoine that hes going to transfer him out of St Tropez. That girl was made to destroy men, he adds. In the final scene, Michel and Juliette walk home together, hand in hand.

==Cast==
  
*Brigitte Bardot as Juliette Hardy
*Curd Jürgens as Éric Carradine
*Jean-Louis Trintignant as Michel Tardieu 
*Marie Glory as Mme. Tardieu
*Georges Poujouly as Christian Tardieu
*Christian Marquand as Antoine Tardieu
*Jane Marken as Madame Morin
*Jean Tissier as M. Vigier-Lefranc
*Isabelle Corey as Lucienne
 
*Jacqueline Ventura as Mme Vigier-Lefranc
*Jacques Ciron as The Secretary of Éric
*Paul Faivre as M. Morin
*Jany Mourey as The Orphanage Representative
*Philippe Grenier as Perri
*Jean Lefebvre as The Man who wanted to dance
*Leopoldo Francés as The Dancer
*Jean Toscano as René
 

==Reception==
===Box Office===
The film was a big hit in France and one of the ten most popular films at the British box office in its year of release. 

===Critical response===
When the film was released in the United States, Bosley Crowther, the film critic for The New York Times, found Brigitte Bardot attractive but the film lacking and was not able to recommend it.  He wrote, "Bardot moves herself in a fashion that fully accentuates her charms. She is undeniably a creation of superlative craftsmanship. But thats the extent of the transcendence, for there is nothing sublime about the script of this completely single-minded little picture...We cant recommend this little item as a sample of the best in Gallic films. It is clumsily put together and rather bizarrely played. There is nothing more than sultry fervor in the performance of Mlle. Bardot." 

Film critic Dennis Schwartz wrote, "The breezy erotic drama was laced with some thinly textured sad moments that hardly resonated as serious drama. But as slight as the story was it was always lively and easy to take on the eyes, adding up to hardly anything more than a bunch of snapshots of Bardot posturing as a sex kitten in various stages of undress. The public loved it and it became a big box-office smash, and paved the way for a spate of sexy films to follow. What was more disturbing than its dullish dialogue and flaunting of Bardot as a sex object, was that underneath its call for liberation was a reactionary and sexist view of sex." 

==Censorship==
At the time of its release the film was condemned by the National Legion of Decency.   

Police made attempts to suppress its screening in the US. COURT BARS BLOCKING OF FRENCH-MADE FILM
Los Angeles Times (1923-Current File)   27 Dec 1957: 5.   FILM SEIZURE ATTACKED: Mayor of Philadelphia Says Action May Be Illegal
Special to The New York Times.. New York Times (1923-Current file)   13 Feb 1958: 23.  

Currently, the review aggregator Rotten Tomatoes reported that 73% of critics gave the film a positive review, based on eleven reviews." 

==References==
Notes
 

==External links==
* 
*  essay at the Criterion Collection by Chuck Stephens

 

 

 
 
 
 
 
 
 
 
 
 
 