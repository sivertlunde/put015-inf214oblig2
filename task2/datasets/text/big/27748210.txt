Danger Route
 
 
{{Infobox Film
| name           = Danger Route
| image          = Dangroupos.jpg
| image_size     = 
| caption        = Original film poster
| director       = Seth Holt
| producer       = Max Rosenberg Milton Subotsky Ted Wallis Andrew York (novel)
| narrator       =  Gordon Jackson  John Mayer   Lionel Bart (title song)
| cinematography = Harry Waxman
| editing        = Oswald Hafenrichter  Amicus
| distributor    = United Artists
| released       = October 1967
| runtime        = 91 mins
| country        = British
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Richard Johnson Andrew Yorks 1966 novel The Eliminator  that was the working title of the film.  The film was released in the United States as a double feature with Attack on the Iron Coast. 

==Plot==
A leading British secret agent/assassin returns home to the Channel Islands from a mission in the Caribbean fearing his nerve has gone, and attempts to resign. He is persuaded by his superiors to undergo a final mission and assassinate a defector but the job turns out to be much more complex than he had been led to believe. 

==Cast==
* Richard Johnson (actor)| Richard Johnson - Jonas Wilde 
* Carol Lynley - Jocelyn 
* Barbara Bouchet -  Marita 
* Sylvia Syms -  Barbara Canning  Gordon Jackson -  Brian Stern 
* Diana Dors - Rhoda Gooderich 
* Maurice Denham  - Peter Ravenspur 
* Sam Wanamaker -  Lucinda  David Bauer - Bennett 
* Robin Bailey - Parsons 
* Harry Andrews - Canning 
* Julian Chagrin - Matsys 
* Reg Lye - Balin 
* Leslie Sands - Man in Cinema 
* Timothy Bateson -Halliwell
==Production==
The film was an attempt to cash in on the popularity of James Bond movies. Milton Subotsky called the movie doomed, saying the director Seth Holt was ill during filming, the script never worked and the cameraman was replaced in the middle of the shoot. Box office response was poor. Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 47-48 
==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 