The Thing (2011 film)
{{Infobox film
| name           = The Thing
| image          = Thingprequelfairuse.jpg
| caption        = Theatrical Poster
| director       = Matthijs van Heijningen Jr. Eric Newman
| writer         = Eric Heisserer
| based on       =   and Characters created by John Carpenter Bill Lancaster
| starring       = Mary Elizabeth Winstead Joel Edgerton Ulrich Thomsen Adewale Akinnuoye-Agbaje Eric Christian Olsen
| music          = Marco Beltrami
| cinematography = Michel Abramowicz Peter Boyle Jono Griffith   Frank J. Urioste  
| studio         = Strike Entertainment Morgan Creek Productions Universal Pictures
| released       =  
| runtime        = 103 minutes 
| country        = United States Canada
| language       = English Norwegian Danish
| budget         = $38 million   
| gross          =  $27.4 million 
}}
 science fiction of the Norwegian and alien buried deep in the ice of Antarctica, realizing too late that it is still alive.

==Plot==
In 1982, an alien spacecraft is discovered beneath the Antarctic ice by a Norwegian research team: Edvard (Trond Espen Seim), Jonas (Kristofer Hivju), Olav (Jan Gunnar Røise), Karl (Carsten Bjørnlund), Juliette (Kim Bubbs), Lars (Jørgen Langhelle), Henrik (Jo Adrian Haavind), Colin (Jonathan Lloyd Walker), and Peder (Stig Henrik Hoff). Paleontologist Kate Lloyd (Mary Elizabeth Winstead) is recruited by Dr. Sander Halvorson (Ulrich Thomsen) and his assistant Adam Finch (Eric Christian Olsen) to investigate the discovery. They travel to the Norwegian base, Thule Station, a weather research installation that was located in Antarctica near U.S. Outpost 31, in a helicopter manned by Carter (Joel Edgerton), Derek (Adewale Akinnuoye-Agbaje), and Griggs (Paul Braunstein). After viewing the spacecraft, Kate, Sander and Adam are told the group also discovered an alien body from the crash buried in the ice.

The body is brought to the base in a block of ice. That evening, while the team celebrates their find, Derek sees the alien burst from the ice and escape the building. The team searches for the creature and discovers that it killed Lars dog. Olav and Henrik find the alien which then grabs and engulfs Henrik. The rest of the group arrive and set fire to the creature, killing both it and Henrik. An autopsy of the scorched alien corpse reveals that its cells are still alive and are consuming and imitating Henriks own.
 dental fillings near a blood-soaked shower. She runs outside to flag down the helicopter after it takes off. When it attempts to land, Griggs transforms into the Thing and attacks Olav, causing the helicopter to crash in the mountains. When Kate returns to the shower, she finds the blood is gone. The team agrees to evacuate, but Kate confronts them with her theory that the Thing can imitate them and has likely already done so. They dismiss her claims, but Juliette says she saw Colin leaving the showers. Juliette and Kate look for the vehicle keys to prevent the others from leaving, when suddenly Juliette transforms and attacks Kate. As Kate flees, she runs past Karl who is consumed by the creature instead. Lars arrives with a flamethrower and burns the Juliette-Thing.

Carter and Derek return to the base, but the team refuses to believe that they could have survived the crash. Kate has Carter and Derek isolated until a test can be prepared to verify they are human. Adam and Sander work on a test, but the lab is sabotaged. Kate proposes another test; believing that the Thing cannot imitate inorganic material, she inspects everyone and singles out those without metal fillings: Sander, Edvard, Adam, and Colin. Lars and Jonas go to retrieve Carter and Derek for testing, and discover they have broken out of isolation. As Lars searches near a building, he is suddenly pulled inside. The group hears Carter and Derek breaking into the building and rushes to intercept them. Edvard orders Peder to burn them. 

Peder takes aim, but Derek now has a gun and shoots several times, killing Peder and rupturing the flamethrowers fuel tank, which ignites. The explosion knocks Edvard unconscious. When brought to the rec room, Edvard transforms, infecting Jonas and Derek before assimilating Adam. Kate torches the infected Jonas and Derek before she and Carter pursue the Thing. While the pair searches, Sander is also infected. After they separate, the Thing into which Edvard and Adam are fused corners Carter in the kitchen, but Kate burns it before it can attack. Kate and Carter see Sander drive off into the blizzard and pursue him in the remaining snowcat.
 screen fades black.
 the two chase him.

==Cast==
*   as the 1982 films protagonist, R.J. MacReady, Kate Lloyd was written to have similar traits as the character Ellen Ripley from the Alien (franchise)|Alien film series.    
*   veteran running a supply operation to the bases. He and his two co-pilots are left in the dark as to why they are there and what is the mysterious thing the scientists have found.  
* Ulrich Thomsen as Dr. Sander Halvorson, the arrogant Danish leader of alien research. He orders the team to obtain a sample of the recently discovered creature despite Kates warnings.  
* Adewale Akinnuoye-Agbaje as Derek Jameson, an American helicopter co-pilot and also a Vietnam veteran who is Carters best friend.  
* Eric Christian Olsen as Adam Finch, a young American scientist working as Dr. Sanders research assistant who invites Kate to the Norwegian base.    
* Trond Espen Seim  as Edvard Wolner, a notable Norwegian geologist who is the station commander and an old friend of Sander.  Georgia who is part of Edvards team. 
* Jørgen Langhelle as Lars, an ex-soldier who works as the dog keeper of the Norwegian base, also the only member of the Norwegian base who does not speak English.  
* Kristofer Hivju as Jonas, a nervous but friendly Norwegian polar ice researcher.  
* Stig Henrik Hoff  as Peder, a Norwegian rifle-toting camp member who is Edvards right hand man. 
* Paul Braunstein as Griggs, a co-pilot member of the American helicopter transport team.
* Jonathan Lloyd Walker as Colin, an eccentric English radio operator.  
* Jo Adrian Haavind as Henrik, another Norwegian base member who assists the alien research team.  
* Jan Gunnar Røise as Olav, a Norwegian Snowcat vehicle driver and guide.  
* Carsten Bjørnlund as Karl, a Norwegian geologist also part of Edvards team. 
* Ole Martin Aune Nilsen as Matias, the helicopter pilot of the Norwegian base currently in a mission to restock kerosene at McMurdo Station.

==Production==

===Development===
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "Its a really fascinating way to construct a story because were doing it by autopsy, by examining very, very closely everything we know about the Norwegian camp and about the events that happened there from photos and video footage thats recovered, from a visit to the base, the director, producer and I have gone through it countless times marking, you know, theres a fire axe in the door, we have to account for that…were having to reverse engineer it, so those details all matter to us ‘cause it all has to make sense."
|-
| style="text-align: right;" |&nbsp;— Eric Heisserer describing the process of creating a script that is consistent with the first film.   
|} The Exorcist... And we really felt the same way about The Thing. Its a great film. But once we realized there was a new story to tell, with the same characters and the same world, but from a very different point of view, we took it as a challenge. Its the story about the guys who are just ghosts in Carpenters movie - theyre already dead. But having Universal give us a chance to tell their story was irresistible."   

In early 2009,   had felt less reverential. 
 Danish actors improvise elements folk song called Sámiid Ædnan ("Lapland (region)|Lapland").      Many scenes involving characters speaking Norwegian were subtitled,    and the language barrier between them and the English speaking characters is exploited to add to the films feeling of paranoia.  Director Matthijs van Heijningen said that the film would show the alien creature in its "pure form", as it was discovered in its ship by the Norwegians; however, it is not revealed whether this is the creatures original form or the form of another creature it had assimilated.    Addressing rumors stating that John Carpenter wished to have a cameo appearance in the film,  Carpenter himself corrected these in an interview for the fan site "Outpost 31", in August 2012. "  rumors are not true", Carpenter stated in the interview.   

===Filming and post-production=== fast cut screen captures creature effects monster suit that Tom Woodruff wore.  The effects team opted to use cable-operated animatronics over more complex hydraulic controls, as they felt they gave a more "organic feel".  In order to emulate the creature effects of the first film, Heisserer revealed that traditional practical effects would be used on the creatures whenever possible.  The films computer-generated imagery was created by Image Engine, the effects house who worked on Neil Blomkamps 2009 film District 9.    Computer Graphics were used to digitally create extensions on some of the practical animatronic effects, as well as for digital matte paintings and set extensions.  Alec Gillis stated that the advancement of animatronic technology since 1982 combined with digital effects allowed the effects team to expand upon the possible creature conceptions.    Matthijs van Heijningen preferred to use practical effects over computer imagery, as he believed actors give better performances when they have something physical to react to.   However in post release interviews, Alec Gillis revealed that while Amalgamated Dynamics creature designs for the film remained intact, most of their practical effects ended up being digitally replaced in post production. The creation of Gillis all practical effects independent horror film Harbinger Down was partially in response to this.       Stunt men covered in fire-retardant gel were used in scenes when characters are set on fire.   The original Ennio Morricone score was reflected in the films score, but it was initially reported that Morricone did not score the film, nor was his music from the 1982 version used.  However, his theme "Humanity (Part II)" appears in a bonus scene during the prequels ending credits (indicating how it leads directly into the 1982 film).

The interior of the crashed alien spacecraft was created by production designer Sean Haworth.    To design the ship, Haworth had to recreate what little was shown of the spacecraft in the Carpenter film, then "fill the gaps" for what was not originally shown. Haworth and a team of approximately twelve others then created the inside of the ship as a several story-high interior set constructed mostly out of a combination of foam, plaster, fiberglass, and plywood.  The ship was designed specifically to look as if it were not made to accommodate humans, but rather alien creatures of different size and shape who could walk on any surface.  A section of the craft called the "pod room" was designed to imply the alien creatures manning it had collected specimens of different alien species from around the universe for a zoological expedition.  

While the film was originally set for release in April, Universal Pictures changed the date to October 14, 2011,  to allow time for reshoots. The intention of the reshoots was to "enhance existing sequences or to make crystal clear a few story beats or to add punctuation marks to the films feeling of dread."     On his Facebook page, Matthijs van Heijningen, Jr. claimed that the reshoots of the film included making an entirely different ending, referring to the original cut as the "Pilot Version" and the new cut as the "Tetris Version". In the original ending, Kate was to discover the original pilots of the spaceship which had all been killed by The Thing, which was an escaped specimen they had collected from another planet, implying that the ship was crashed in an attempt to kill the monster. "I liked that idea because it would be the Norwegian camp in space. Kate sees the pod room and one pod being broken, giving her the clues what happened. What didnt work was that she wanted to find Sander and stop the ship from taking off and still solve the mystery in the ship. These two energies were in conflict."   

==Release==

===Box office===
The Thing grossed $8,493,665 over the opening weekend and ended up third on the  , who goes on to say "  was naturally at a disadvantage: a vague "thing" doesnt give prospective audiences much to latch on to. It was therefore left up to fans of the original, who are already familiar with the concept, to turn out in strong numbers."  The film grossed $9,530,415 in foreign countries,  bringing the total worldwide box office gross so far to $27,428,670. 

===Critical reception=== Christopher Orr of The Atlantic wrote that the narrative choices open to a prequel "exist on a spectrum from the unsurprising to the unfaithful", but van Heijningen "has managed this balancing act about as well as could be hoped" and although the line between homage and apery is a fine one, "in our age of steady knockoffs, retreads, and loosely branded money grabs, The Thing stands out as a competent entertainment, capably executed if not particularly inspired."   

Other critics singled out Mary Elizabeth Winstead for praise in her performance as the lead, Dr. Kate Lloyd. "  stands out with her portrayal of a paleontologist. She keeps a cool, logical head whilst others around her start to panic. Its a refreshing change from your traditional horror film where the lead characters do moronic things as if to prolong the story", Matthew Toomey of The Film Pie wrote.  Josh Bell of Las Vegas Weekly rated the film three out of five stars and wrote, "Winstead makes for an appealing protagonist, and Kate is portrayed as competent without being thrust into some unlikely action-hero role." 
 the 1982 film.  In Patrick Sauriol of Coming Attractions review, he states, "Stack it up against John Carpenters version and it looks less shiny, but lets face it, if you’re that kind of Thing fan you’re going to go see the new movie anyway. Try and judge todays Thing on its own merits." 

===Accolades===
The film was nominated for two awards at the  , respectively.
{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Recipient!! Result !! Ref.
|-
|rowspan="2"| 2012
|rowspan="2"| Saturn Awards Saturn Award Best Horror/Thriller Film
|The Thing
| 
| rowspan="2" style="text-align:center;"|    
|-
| Best Make-Up
| Tom Woodruff, Jr. and Alec Gillis
| 
|-
|}

==Soundtrack==
{{Infobox album
| Name = The Thing: Original Motion Picture Soundtrack
| Type = Soundtrack
| Artist = Marco Beltrami
| Cover =
| Released = October 11, 2011
| Recorded =
| Genre = Film score
| Length = 55:31
| Label = Varèse Sarabande
| Producer =
}}
The music composed for the film by Marco Beltrami was released in October 11, 2011. The soundtrack was released under the label Varèse Sarabande. 

===Track listing===
# "Gods Country Music" – 1:27
# "Road to Antarctica" – 2:41
# "Into the Cave" – 0:39
# "Eye of the Survivor" – 2:25
# "Meet and Greet" – 2:55
# "Autopsy" – 3:08
# "Cellular Activity" – 1:38
# "Finding Filling" – 3:25
# "Well Done" – 1:32
# "Female Persuasion" – 4:51
# "Survivors" – 3:28
# "Open Your Mouth" – 4:20
# "Antarctic Standoff" – 3:28
# "Meating of the Minds" – 4:28
# "Sander Sucks at Hiding" – 2:22
# "Cant Stand the Heat" – 2:10
# "Following Sanders Lead" – 2:39
# "In the Ship" – 2:39
# "Sander Bucks" – 0:45
# "The End" – 2:33
# "How Did You Know?" – 2:29

===Reception===
AllMusic rated the album 3.5/5 saying, "Composer Marco Beltramis appropriately tense and brooding score for director Matthijs van Heijningen, Jr.s 2011   The Thing dutifully echoes Ennio Morricones stark score for the original version, which in its own way echoed the soundtrack work of that films director, John Carpenter." 

===Uncredited===
The Norwegian characters play an excerpt from the song Sámiid Ædnan.

==Home media==
The Thing was released on Blu-ray and DVD on January 31, 2012 in the US.  The film earned an additional $5,174,780 through DVD sales. 

==Related==
The film was made into a maze at both Universal Studios Hollywoods and Universal Orlando Resorts 2011 Halloween Horror Nights events, having the subtitle Assimilation at Hollywoods version.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 