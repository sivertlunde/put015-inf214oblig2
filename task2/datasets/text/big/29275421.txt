Fortuna (film)
 
{{Infobox film
| name           = Fortuna
| image          = Fortuna film.jpg
| image_size     = 
| alt            = 
| caption        = Israeli DVD cover
| director       = Menahem Golan
| producer       = Yoram Globus Menahem Golan Meir Magen
| screenplay        = Menahem Golan
| story          = Menahem Talmi
| narrator       = 
| starring       = Gila Almagor Ahuva Goren
| music          = Dov Seltzer
| cinematography = Yitzhak Herbst
| editing        = Danny Shick
| studio         = Films Festival Francais Noah Films
| distributor    = Tal-Shahar   Trans American Films (1969) (USA) (dubbed)
| released       = UK February 1967 USA January 1969 
| runtime        = 115 minutes
| country        =  
| language       = Hebrew
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}} 1966 film directed by Menahem Golan. It was released in the United States in 1969 in a dubbed version.

==Plot==
A man falls in love with a beautiful girl who leads him down the wrong path.  He must decide what to do with life and is not sure he can continue living and contemplates suicide.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Ahuva Goren || Fortuna
|-
| Gila Almagor || Margo
|-
| Pierre Brasseur || Bozaglo 
|- Mike Marshall || Pierre 
|-
| Shmuel Oz || Yosef
|-
| Saro Urzì || Monsieur Simon  
|}

==Availability==
Although the film is hard to find, some websites do sell the DVD online. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 