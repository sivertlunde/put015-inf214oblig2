The Invisible Wall (1947 film)
{{Infobox film
| name           = The Invisible Wall
| image          = The Invisible Wall film poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Eugene Forde
| producer       = Sol M. Wurtzel (uncredited) Paul Frank (associate producer)
| screenplay     = Arnold Belgard
| story          = Howard J. Green Paul Frank
| starring       = Don Castle Virginia Christine Richard Gaines
| music          = Dale Butts
| cinematography = Benjamin H. Kline
| editing        = Frank Baldridge
| distributor    = 20th Century Fox
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Invisible Wall is a 1947 American film noir directed by Eugene Forde starring Don Castle, Virginia Christine and Richard Gaines. 

==Plot==
Told in flashback, the film recounts how gambler Harry Lane (Castle) ended up accused of murder: Returned from the war he took up his old job working for bookmaker Marty Floyd (Keane). Dispatched by his boss to deliver $20,000 to a contact in Las Vegas, he foolishly loses some of the money at the gambling tables, then loses the rest of it to a conman (Gaines). His attempts to recover the money result in him accidentally killing the conman. In the end Lane is cleared when it is revealed that the victim was no victim after all.

==Cast==
* Don Castle as Harry Lane
* Virginia Christine as Mildred Elsworth
* Richard Gaines as Richard Elsworth
* Arthur Space as Roy Hanford
* Edward Keane as Marty Floyd Jeff Chandler as Al Conway Mary Gordon as Mrs. Bledsoe
* Harry Cheshire as Eugene Hamilton
* Rita Duncan as Alice Jamison Harry Shannon as Det. Capt. R.W. Davis
* Earle S. Dewey as Doctor Winters

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 

 