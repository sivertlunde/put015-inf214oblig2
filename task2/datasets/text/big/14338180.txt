Immortal Sergeant
{{Infobox film
| name           = Immortal Sergeant
| image          = Immortal Sergeant.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = John M. Stahl
| producer       = Lamar Trotti
| writer         = Lamar Trotti
| based on       =   Thomas Mitchell
| music          = David Buttolph Arthur Miller James B. Clark
| studio         = 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 91 mins.
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Thomas Mitchell as the title character. The film was based on the novel of the same name by John Brophy.

==Plot== Thomas Mitchell) armored car, but is seriously wounded. He orders Spence to leave him behind; when Spence refuses to obey, he shoots himself. 

Spence leads the remaining three men towards an oasis. Before they can reach it though, a transport plane lands and disgorges German soldiers who set up a base. After sneaking in to steal badly needed food and water, Spence has to assert his leadership when one of his men advocates surrendering. Instead, Spence leads them in a surprise attack under the cover of a Dust storm|sandstorm. The British emerge victorious, though one man is killed and Spence is wounded.

The corporal comes to in a Cairo hospital and finds he is to be given a medal and promoted to lieutenant. His newfound assertiveness extends to his personal life. He proposes to his girlfriend Valentine (Maureen OHara), whom he had thought of (in Flashback (narrative)|flashbacks) throughout his ordeal.

== Cast ==
* Henry Fonda as Corporal Colin Spence Thomas Mitchell as Sergeant Kelly
* Maureen OHara as Valentine
* Allyn Joslyn as Cassity
* Reginald Gardiner as Benedict
* Melville Cooper as Pilcher
* Bramwell Fletcher as Symes
* Morton Lowry as Cottrell
* John Banner as Soldier (uncredited)
* Wilson Benge as Waiter (uncredited)
* Lane Bradford as Returning Soldier (uncredited)
* Gordon B. Clarke as Soldier (uncredited) James Craven as NCO (uncredited)
* Italia DeNubila as Dance Specialty (uncredited)
* Bess Flowers as Nightclub Patron (uncredited)
* Bud Geary as Driver (uncredited)
* Frederick Giermann as Soldier (uncredited)
* Henry Guttman as Soldier (uncredited)
* Sam Harris as Party Guest (uncredited)
* Charles Irwin as NCO (uncredited)
* Peter Lawford as Soldier (uncredited)
* Anthony Marsh as Assistant Post Corporal (uncredited)
* Bob Mascagno as Dance Specialty (uncredited)
* John Meredith as Soldier (uncredited) Edmund Mortimer as Party Guest (uncredited)
* Jean Prescott as Minor Role (uncredited)
* Cyril Ring as Man at Train Depot as Soldiers Depart (uncredited)
* Donald Stuart as Post Corporal (uncredited)
* David Thursby as Bren Carrier Driver (uncredited)
* Leslie Vincent as Runner (uncredited)
* Hans von Morhart as Soldier (uncredited)
* Sam Waagenaar as German (uncredited) John Whitney as Soldier (uncredited)
* Eric Wilton as Headwaiter (uncredited)

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 