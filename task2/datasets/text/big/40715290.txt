Sundarakilladi
 
{{Infobox film
| name           = Sundarakkilladi
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Muralikrishnan Fazil
| screenplay     = 
| story          =  Dileep  Shalini  Nedumudi Venu  Asokan  Sadique  Kutiravattom Pappu
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Malayalam film directed by Murali krishnan and stars Dileep (actor)|Dileep, Shalini Kumar, Nedunudi Venu, Kuthiravattom Pappu, Nandu, Reshmi Boban, Asokan in the lead.

==Plot==
Swapnabhoomi is a village where strange customs and rituals exist. When a water shortage occurs the leader (Nedumudi Venu) sends a person (Ashokan (actor)|Ashokan) to inform Premachandran (Dileep (actor)|Dileep) of the situation and request him to come to Swapnabhoomi and dig a well. Because of his poverty, he accepts the advance and agrees.Then he discovers that other workers who had tried to dig a well there died because of earthquake after digging 45 metre of land. He tries to escape from the people of Swapnabhoomi, but fails and starts the work as a challenge. Later he falls in love with the stepdaughter of the physician Devayaani (Shalini Kumar) who treats him when he meets with an accident while digging the well. But the rule of Swapnabhoomi is that a girl of the village may only court a man who comes from Swapnabhoomi itself. One day a man discovers their love and informs the head. The head orders them to stop seeing each other. If Premachandran disobeys, they will kill him or if Devayaani disobeys, then they will send her to a mountain where man eaters live. The head tells Premachandran that, if the well fills with water, then he can marry Devayaani. Later Premachandran came to know that before the last day of work rain should be there. Because of the fear, the head orders the destruction of the well, but Premachandran jumps into the well. When Devayaani learns about this, she breaks the rule and comes to see him. The people take her to the place where they have to do rituals and send her to the mountain. But water fills the well of Swapnabhoomi. Premachandran informs everyone and takes her to his home.
  
==Cast== Dileep as Premachandran
*Shalini Kumar as Devayani (Premachandrans Girl Friend)
*Nedumudi Venu as Head of Swapna Bhoomi
*Babu Namboothori as Devayanis Father
*Kuthiravattom Pappu as Vasu
*Nandhu as Pradeep Asokan as Devayanis Love interest
*Remya Salim as Sumangala
*T. P. Madhavan 
*Sadiq
*Sankaradi
*Santo Krishnan

==References==
 

 
 


 