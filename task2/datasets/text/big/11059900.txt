Sliver (film)
{{Infobox film
| name = Sliver
| image = Sliver poster.jpg
| imagesize =
| caption = Theatrical release poster
| director = Phillip Noyce Robert Evans
| screenplay = Joe Eszterhas
| based on = Sliver (novel)|Sliver by Ira Levin
| starring = Sharon Stone William Baldwin Tom Berenger 
| music = Howard Shore
| cinematography = Vilmos Zsigmond
| editing = Richard Francis-Bruce
| distributor = Paramount Pictures
| released = May 21, 1993
| runtime = 107 minutes
| language = English
| budget = $40 million
| gross = $116,300,000 
}} of the New York high-rise apartment building. {{cite news
| url=http://www.nytimes.com/1993/05/30/movies/film-bucks-and-blondes-joe-eszterhas-lives-the-big-dream.html
| title=FILM; Bucks and Blondes: Joe Eszterhas Lives The Big Dream
| first=Maureen
| last=Dowd
| date=1993-05-30
| publisher=The New York Times
| accessdate=2013-03-07}}  Phillip Noyce directed the film, from a screenplay by Joe Eszterhas. {{cite news
| title=Beyond `Sliver: `Lies Screenwriter Joe Eszterhas Takes On The Critics Of His Sexy Scripts
| url=http://articles.chicagotribune.com/1997-10-26/news/9710260477_1_showgirls-jade-basic-instinct
| first=Gary
| last=Dretzka
| date=1997-10-26
| publisher=Chicago Tribune MPAA (which originally gave the film an NC-17 rating), the filmmakers were forced to make extensive reshoots before release. These reshoots actually necessitated changing the killers identity. The film stars Sharon Stone, William Baldwin and Tom Berenger.
 East 38th Park Avenue. Madison Avenue New York, one block west and two blocks south of the fictional address. The building has since become a condominium development. It was built in 1985 and has 32 floors. While the movie made use of the buildings courtyard, the lobby was a Los Angeles film set.

When he signed on to direct the film, Phillip Noyce remarked "I liked the script a lot. Or at least, I liked the idea of jumping on the Joe Eszterhas bandwagon." 

==Synopsis==
Carly Norris (Sharon Stone) moves into an exclusive New York residential building, not long after the previous tenant, Naomi Singer, falls to her death from her balcony. She immediately crosses paths with other tenants including the handsome Zeke (William Baldwin).

Carly and Zeke soon start meeting quite often; Carly eventually agrees to go to the gym with Zeke, who begins to turn her on by grabbing her hips while exercising, and they subsequently begin a sexual relationship. Carly is also being romantically pursued by Jack (Tom Berenger), a novelist who is another resident of her building.

Two of Carlys neighbors (Keene Curtis, Polly Walker) die under suspicious circumstances. As she discovers more about Zeke and Jack, she begins to distrust both and also uncovers shocking secrets about other people who live around her. Carly eventually finds out that Jack killed Naomi because of his jealousy of Zeke. It is revealed that Zeke knew that Jack was the killer, but chose to ignore it because it would expose his other secret: he has surveillance cameras allowing him to spy on every apartment, including hers.

Although she is both curious about and disturbed by the cameras, Carly eventually destroys Zekes surveillance room and his video monitors, telling him to "get a life" before leaving him and the building.

==Cast==
*Sharon Stone as Carly Norris
*William Baldwin as Zeke Hawkins
*Tom Berenger as Jack Landsford
*Polly Walker as Vida Warren
*Colleen Camp as Judy Marks Amanda Foreman as Samantha Moore
*Martin Landau as Alex Parsons
*Nicholas Pryor as Peter Farrell
*C. C. H. Pounder as Lt. Victoria Hendrix
*Nina Foch as Evelyn McEvoy
*Keene Curtis as Gus Hale
*Anne Betancourt as Jackie Kinsella
*Tony Peck as Martin Kinsella
*Allison Mackie as Naomi Singer

==Original ending==
In the films original ending Sharon Stone and the killer fly over a volcano when the killer suddenly confesses his crimes. He then veers the aircraft into the volcano as the end credits roll and leaves the audience to decide whether they survive. 

Preview audiences disliked the idea of the heroine turning immoral. All the film from the cameramen who actually crashed into a volcano in Hawaii and narrowly escaped death was removed.   

==MPAA ratings issues== Showtime special Paramount released the unrated version to video there was no male frontal nudity included, though the sex scenes were considerably more graphic.

==Home video releases== aspect ratio, aspect ratio transfer. The release also contained what some reviewers  have noted as an unusual amount of dirt and scratches for a film print that is only a little over a dozen years old, though the casual viewer is unlikely to detect anything errant. In May 2006 an R-rated for-rent-only version was released to rental outlets.  In 2013, the film was released on Blu-ray Disc, using the same matted 2.10 aspect ratio version of the R-rated theatrical cut.

==Reception== thriller elements, that it diluted some of the plotlines of the novel, and that the actors were not on form. Many also singled out the editing and ending, calling the latter hasty and unconvincing. {{cite news
| title=MOVIE REVIEW : Erotic Thriller Sliver Leaves a Lot to Be Desired : This wrongheaded version of Ira Levins pulp novel may be about voyeurism, but it doesnt provide much to watch.
| url=http://articles.latimes.com/1993-05-22/entertainment/ca-38289_1_ira-levin
| first=Peter
| last=Rainer
| date=1993-05-22
| publisher=Los Angeles Times Razzie Awards, including Worst Picture, Worst Director, Worst Actor (William Baldwin), Worst Actress (Sharon Stone), Worst Supporting Actor (Tom Berenger), Worst Supporting Actress (Colleen Camp) and Worst Screenplay, but failed to "win" any.

The movie debuted at No. 1 at the box office. {{cite news
| title= Stone Gets a Sliver of Box Office but Not a Runaway Movies: Hot Shots! also opens strongly but the blockbuster hopes are now on Memorial Day weekend.
| url=http://articles.latimes.com/1993-05-24/entertainment/ca-39255_1_box-office
| first=David J.
| last=Fox
| date=1993-05-24
| publisher=Los Angeles Times
| accessdate=2013-03-07}}  By the second week the box office taking dropped to No. 6. {{cite news
| title= Slys Back in Peak Form at Box Office : Movies: Cliffhanger grabs the largest opening for a non-sequel on any Memorial Day weekend. Made in America opens in second place.
| url=http://articles.latimes.com/1993-06-01/entertainment/ca-42182_1_memorial-day-weekend
| date=1993-06-01
| first=David J.
| last=Fox
| work=Los Angeles Times
| accessdate=2013-03-07}}  Sliver eventually grossed $36.3 million domestically and $80 million around the world to a total of $116.3 million worldwide.

==See also==
 
* Sliver (soundtrack)|Sliver (soundtrack)
* List of films featuring surveillance

==References==
 

==External links==
*  
*  
* {{cite web
| url=http://www.ew.com/ew/article/0,,306778,00.html
| title=All About the Watchtower
| first=Tim
| last=Purtell
| date=1993-06-04
| publisher=Entertainment Weekly
| accessdate=2013-03-07}}

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 