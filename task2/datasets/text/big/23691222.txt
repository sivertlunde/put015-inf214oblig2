South of Caliente
{{Infobox film
| name           =South of Caliente
| image          =
| image_size     =
| caption        =
| director       = William Witney
| producer       = Edward J. White
| writer         = Eric Taylor
| narrator       =
| starring       = Roy Rogers Dale Evans
| music          = R. Dale Butts
| cinematography = Jack A. Marta
| editor       =
| distributor    = 1951
| runtime        = 67 min.
| country        = United States English
| budget         =
}}
 1951 United American film starring Roy Rogers.

One highlight of the film is the gypsy dance as done by Lillian Molieri (aka Lupe Mayorga). Her husband Francisco Mayorga worked with Roy in "Hands Across The Border." and her young adopted son, (Later to be a Hall of Fame Guitarist) Bill Aken did a vocal duet with Roy on the song "The Big Silver Screen" in 1959. A unique two generation family association in which all three family members appeared with Roy Rogers.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC" Actor
!width="200"|Role
|-
| Roy Rogers
| Roy Rogers
|-
| Dale Evans
| Doris Stewart
|-
| Pinky Lee
| Pinky
|-
| Douglas Fowley
| Dave Norris
|-
| Ric Roman
| Josef
|-
| Leonard Penn Captain
|-
| Willie Best
| Willie
|-
|}

==External links==
*  

 
 
 
 
 
 
 


 