The Overcoat (1952 film)
{{Infobox film
| name           = Il Cappotto
| image          = Il cappotto.jpg
| caption        = Italian DVD cover
| director       = Alberto Lattuada
| producer       = Enzo Curreli
| writer         = Alberto Lattuada Giorgio Prosperi Giordano Corsi Enzo Curreli Luigi Malerba Leonardo Sinisgalli Cesare Zavattini
| narrator       = 
| starring       = Renato Rascel Yvonne Sanson Antonella Lualdi Nino Marchetti
| music          = Felice Lattuada
| cinematography = Mario Montuori
| editing        = Eraldo Da Roma
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = Italy
| language       = Italian
| budget         = 
}} short tale of the same name written by Nikolai Gogol.

==Plot==
The story of a very poor city-hall clerk (Renato Rascel) whose only desire is to own a new overcoat.

==Cast==
* Renato Rascel - Carmine De Carmine
* Yvonne Sanson - Caterina
* Giulio Stival - Il sindaco
* Ettore Mattia - Il segretario generale
* Giulio Calì - Il sarto
* Olinto Cristina
* Anna Carena - Laffittacamere (as Anna Maria Carena)
* Sandro Somarè - Il fidanzato di Vittoria
* Luigi Moneta
* Silvio Bagolini - Il venditore ambulante
* Dina Perbellini
* Loris Gizzi
* Mario Crippa
* Alfredo Ragusa
* Nino Marchetti (as Nino Marchetti)

==Awards==
Special Nastro dArgento to Renato Rascel for the creation of the leading character. The film was also entered into the 1952 Cannes Film Festival.   

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 