Balkan Is Not Dead
 
{{Infobox film
| name = Balkan Is Not Dead
| image = Balkan Is Not Dead.jpg
| caption = 
| director = Aleksandar Popovski
| producer = Robert Jazadziski, Robert Naskov
| writers = Ana Lasic,Aleksandar Popovski	
| starring = 
| music = Kiril Džajkovski
| cinematography =Dejan Dimeski
| editing =Atanas Georgiev
| studio = 
| distributor = 
| released =  
| runtime = 100 min
| country = Macedonia
| language = Macedonian
| budget = € 850.000 
| gross = 
}}
Balkan Is Not Dead ( ) is a 2012 Macedonian film.

==Plot==
A Macedonian family from Bitola at the turn of the twentieth century tries to survive, preserve its roots and remain together. Following their father’s death, two sisters and a brother remain alone: the older and quieter sister ELENI and the younger ANDJA and DUKO – the discontented and rather short-tempered twins. Afflicted by her father’s refusal to let her be with the man she loves – the young Turkish cadet, Kemal – Eleni has decided to dedicate her life to waiting. Her younger sister, searching for Damjan, her young fiancé – her brother’s best friend and leader of the insurgents against the Turks – falls captive to the murderous and slighted Osman, a renegade from the Sultan’s authority. Osman becomes enamoured of Andja and murders Damjan, despite or rather precisely because of the fact that Anja trades for Damjan’s freedom the only valuable thing she has – her virginity. Broken and unable to give him again what she has already given him, she spends her days in a harem as the most cared for blossom that has lost its fragrance. Duko becomes obsessed with vengeance, while Osman becomes obsessed with love. In order to get Andja to love him, he would do anything for her. To this end, he also uses his troops and his most loyal “watchdog”, Rasim, his man for the dirty jobs, the sole person who understands him entirely and who is prepared to lay down even his own life for him. Torn between her love for one Turk and her hatred for another, Eleni sets out to look for Andja. From a passive spinster, she becomes a fighter for women’s rights, advocating freedom, a more modern society and abolition of harems….

She communicates her ideas to Kemal in her letters that she never stops sending. These are precisely the ideas to which he dedicates his entire life. The eldest sister Eleni remains alone. She sees Kemal only one more time before she surrenders her life to eternal silence.

Side plot: Edisse, a French prostitute of the highest class sets out on a journey from Paris to the Sultan who has purchased her and aboard the Orient Express encounters Ikonomo a Macedonian merchant who finances Osman and she falls in love with him. Although he too falls in love with her,he soon realises that these are dangerous times and hands her over to Osman who, in turn, needs to hand her over to the Sultan. There she meets Andja and her sister Eleni. This is also the only time these three women are at the same place. When Edisse finally reaches the Sultan, he is already dead. She returns to Paris as a witness of as many as three unfulfilled Balkan loves that have fallen victim to the relentless course of the new time, which would eventually be characterised, among other things, by the struggle forequality.

==Cast==
* Nikola Ristanovski - Osman
* Natasa Tapuskovic - Eleni
* Dragana Kostadinovska - Angja
* Rade Serbedzija - Ikonomo
* Nikola Aceski - Duko
* Blagoj Veselinov - Damjan
* Dijana Vidusin - Edis
* Jordan Simonov - Rasim
* Natasa Naumovska - Anamka1
* Ertan Saban - Kemal Ataturk
* Edin Jakubovic - Muzafer
* Dimitrija Doksevski - Metodija
* Salaetin Bilal - Valijata
* Saska Dimitrovska - Krstana
* Petre Arsovski - Popot

== References ==
 

== External links ==
*  
*  

 
 
 