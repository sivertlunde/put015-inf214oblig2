The Devil and Daniel Webster (film)
{{Infobox film
| name           = The Devil and Daniel Webster
| image          = The devil and daniel webster DVD.jpg
| image_size     =
| caption        = DVD cover
| director       = William Dieterle
| producer       = William Dieterle
| writer         = Dan Totheroh Stephen Vincent Benét
| narrator       = Edward Arnold James Craig Anne Shirley Jane Darwell Simone Simon
| music          = Bernard Herrmann
| cinematography = Joseph H. August
| editing        = Robert Wise Criterion (DVD Region 1 DVD)
| released       =  
| runtime        = 107 mins (full version) 85 mins (cut version)
| country        = United States
| language       = English
| budget         =
| gross          =
}} The Devil Edward Arnold, James Craig.

A retelling of the Faust legend, set in mid-1840s rural New England, it was directed by German-born actor-director William Dieterle who (under his original name, Wilhelm Dieterle) played a featured role in F.W. Murnaus epic silent version of Faust (1926 film)|Faust in 1926.

==Plot== James Craig) Edward Arnold), a friend of his wifes family, and a widely-loved figure who champions the cause of the poor farmers - although we learn that Webster himself is being tempted by Mr Scratch to sell his soul, in return for fulfilling his ambition to become President.
 Anne Shirley) and his pious mother (Jane Darwell). Later, as the townspeople celebrate the harvest in Jabezs barn, Mary gives birth to their first child, whom they name Daniel in honour of Mr Webster, but minutes later, Jabez discovers that the local girl they had hired as a maid has vanished. In her place he finds the beautiful and sinister Belle (Simone Simon), who has been sent by Mr. Scratch. She bewitches Jabez, driving a wedge between him and Mary. Soon, Jabez has stopped going to church, and after his family leaves, he secretly hosts gambling parties at his home. As Daniel grows, he too falls under Belles malign influence, and she turns him into a spoiled, disobedient brat.

In a few more years, Jabez is one of the richest men in the country - he lives like a lord, and has built a lavish new mansion (which his mother refuses to live in). He throws a huge ball, but it ends in disaster - after a nightmarish dance between Belle and Miser Stephens, Jabez finds Stephens dead on the floor - he too had signed a pact with Mr Scratch, and his time was up. Realising all his guests have fled, Jabez turns on Mary, blaming her for all his troubles, and he throws her out. Now desperate, and realising his own time is almost up, he tries to erase the deadline Mr Scratch has burned into the tree outside the barn, but Scratch appears and again tempts Jabez, offering to extend his deal, in return for the soul of his son. Horrified, Jabez flees, and chases after the cart. He begs Marys forgiveness and pleads with Webster to help him find some way out of his bargain with the Devil. Webster agrees to take his case. Mr. Scratch offers an extension in exchange for Jabezs son, but Jabez turns him down. He then begs Webster to leave before it is too late, but Webster refuses to go, boasting that he has never left a jug or a case half finished.

When Mr. Scratch shows up to claim his due, Webster has to wager his own soul before his fiendish opponent will agree to a trial by jury. Mr. Scratch chooses the jury members from among the most notoriously evil men of American history (including Benedict Arnold) with John Hathorne (one of the magistrates of the Salem witch trials) as the judge. When Webster protests, Mr. Scratch points out that they were "Americans all." With his own soul at risk, Webster proceeds to defend Jabez Stone, who is accused of breaching contract.

He begins by stating that he envies the jury because as Americans they were present at the birth of a nation, part of a heritage they were born to share. Unfortunately, they were fooled like Jabez Stone, trapped in their desire to rebel against their fate, but what would they give to be given another chance? Webster explains that it is the eternal right of everyone, including the jury, to raise their fists against their fates, but when that happens, one finds oneself at a crossroads. They took the wrong turn just as Stone did, but he found out in time, and this night he is there to save his soul. Daniel asks the jury to give Stone another chance to walk upon the earth, for what would they give to see those things they remember? They were all men once, breathing clean American air, which was free and blew across the earth they loved.

Webster starts to expound on the virtue of simple and good things – "... the freshness of a fine morning ... the taste of food when youre hungry ... the new day thats every day when youre a child ... " – and how without the soul, those things are sickened. He reminds the jury that Mr. Scratch had told them that the soul meant nothing, they believed him, and they lost their freedom. Next, Webster discourses on freedom as not just a big word: "It is the morning, the bread, and the risen sun ... ", it was the reason for making the voyage to come to America. Mistakes were made, but out of things wrong and right a new thing has come: a free man with a freedom that includes his own soul. Yet how can the jury be on the side of the oppressor, when Stone is a brother, a fellow American? Webster then implores the jury to let Stone keep his soul, which, after all, doesnt belong to him alone, but to his family and country. "Dont let the country go to the devil," thunders Daniel Webster. "Free Stone."

After a few moments, Hathorne asks the jury for their verdict and in response the foreman tears up the contract, releasing Jabez from his deal. Webster then kicks Mr Scratch out, but as he is ejected the fiend promises that Webster will never fulfil his ambition to become President of the United States.

==Cast==
  Edward Arnold Thomas Mitchell initially played Webster, but towards the end of principal shooting he was badly injured in a filming accident and Dieterle had to replace him with Arnold (who took the role on a days notice ). Mitchell suffered a skull fracture and was incapacitated for weeks when he was thrown from a carriage while filming the scene in which he gives Jabezs son a ride. Because Deiterle had shot the film in more or less chronological sequence, and the climactic trial scene was still to be shot, he was forced to reshoot most of Mitchells scenes with Arnold (although Mitchell was retained in a few long-shots) 

*Walter Huston as Mr. Scratch James Craig as Jabez Stone Anne Shirley as Mary Stone
*Jane Darwell as Ma Stone
*Simone Simon as Belle
*Gene Lockhart as Squire Slossum
*John Qualen as Miser Stevens, the local loan shark before Jabez
*H. B. Warner as Justice John Hathorne
*Robert Pittard as Store Clerk
*Alec Craig as Eli Higgins

==Adaptation==
The jury of the damned in the film is slightly altered from the original, as revealed in the following dialogue:  Governor Dale, Walter Butler, Big and Little Harp, robbers and murderers. Blackbeard|Teach, the cutthroat. Thomas Morton (colonist)|Morton, the vicious lawyer. And General Benedict Arnold, you remember him, no doubt.
:Webster: A jury of the damned.
:Scratch: Dastards, liars, traitors, knaves.
:Webster: This is monstrous. quick or the dead.
:Webster: I asked for a fair trial.
:Scratch: Americans all.

In the original story, Webster regrets Benedict Arnolds absence, but in the film, he is present and Webster objects, citing him as a traitor and therefore not a true American. His objection is dismissed by the judge.

==Alternative versions==
The original release was 107 minutes long. It was a critical, but not a box-office, success, recording a loss of $53,000 on its initial run. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p166  It was subsequently re-released under the title The Devil and Daniel Webster with nearly half an hour cut, reducing the film to 85 minutes. The cuts were crudely done. The film was restored to its full length in the 1990s and has been issued in that form on home video. However, the title has remained The Devil and Daniel Webster. The restored portions on the video had been taken from inferior prints of the movie, but the quality has been notably improved on the DVD release. A preview print  titled Here Is a Man was found in the estate of the director and served as the basis for the films restoration and DVD release.

==Awards== Academy Award Best Music, Scoring of a Dramatic Picture  and Walter Huston was nominated for Academy Award for Best Actor.
In addition to his original music score, Herrmann also incorporated several traditional folk tunes including,
The Devils Dream|"Devils Dream", On Springfield Mountain|"Springfield Mountain", and a diabolical version of Pop Goes the Weasel|"Pop Goes The Weasel" played on the fiddle by Mr. Scratch. 

The film was nominated for AFIs 10 Top 10#Fantasy|AFIs Top 10 Fantasy Films list. 

==Use in popular culture== Homer sells his soul to the Devil (Ned Flanders) for a donut and subsequently must face trial at the stroke of midnight after accidentally eating the last piece of donut; One of the members of the jury is Blackbeard.  Bad Moon Rising" by Creedence Clearwater Revival was inspired after John Fogerty was watching the hurricane scene from the film.

==See also==
* Trial movies

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 