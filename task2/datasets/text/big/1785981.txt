King's Ransom (film)
{{Infobox film
| name           = Kings Ransom
| image          = Kings_Ransom_poster.JPG
| caption        = Theatrical poster
| director       = Jeffrey W. Byrd
| producer       = Darryl Taja David Brewington Jeremy Barber Luke Ryan Matt Moore Mike Drake Toby Emmerich
| writer         = Wayne Conley
| narrator       = 
| starring       = Anthony Anderson Jay Mohr Donald Faison Kellita Smith Nicole Ari Parker Charlie Murphy Regina Hall Roger Cross
| music          = Marcus Miller
| cinematography = Robert McLachlan
| editing        = Jeffrey Cooper
| distributor    = New Line Cinema
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $4,008,527 (domestic)  $4,143,652 (worldwide) 
}}
 directed by written by Wayne Conley, who was a writer for Kenan & Kel. The film stars Anthony Anderson, Jay Mohr, Kellita Smith, Regina Hall, Donald Faison, Nicole Ari Parker, Charlie Murphy, Loretta Devine, Brooke DOrsay, and Leila Arcieri. Kings Ransom was released in the United States on April 22, 2005, and, on a production budget of $15,000,000, brought in $4,008,527 domestically and $4,143,652 worldwide.

==Plot==
Malcolm King (Anderson) is a wealthy, selfish, obnoxious businessman who is about to divorce his wife Renee (Smith). She plans to ruin him financially during the court proceedings, and King is willing to do anything to protect his fortune.

He enlists his mistress, Peaches (Hall), and her brother, Herb (Murphy), to stage a mock kidnapping. They are to make and receive a huge ransom demand, which would keep the money safe from his wife.

Unfortunately for him, two other people have similar plans to kidnap him; Angela (Parker), an aggrieved employee and Corey (Mohr), a good-natured yet hapless nobody who lives in his grandmothers basement and needs $10,000 after being threatened by his adopted sister.

==Cast==
*Anthony Anderson as Malcolm King
*Jay Mohr as Corey
*Kellita Smith as Renee King
*Donald Faison as Andre
*Regina Hall as Peaches Clarke
*Nicole Ari Parker as Angela Drake
*Loretta Devine as Miss Gladys
*Charlie Murphy as Herb Clarke
*Leila Arcieri as Kim Baker
*Brooke DOrsay as Brooke Mayo
*Roger Cross as Byron
*Jackie Burroughs as Granny
*Christian Potenza as Officer Holland
*Lawrence Dane as Detective Conley
*Lisa Marcos as Raven
*Robert Norman Smith as David

==Box office==
Kings Ransom was produced on a $15 million budget, but only grossed $2,137,685 on its opening weekend and ranked at #10 at the box office. It was released in 1,508 theaters and had $1,417 average. The film eventually closed on June 2, 2005 upon grossing $4,008,527 in the domestic market, and $135,125 in the foreign market for a worldwide total of $4,143,652. Altogether, the film ended up a huge disappointment, commercially. 

==Critical response== normalized rating out of 100 from critics, gave the film a metascore of 11 based on 13 reviews, indicating "overwhelming dislike". 

==See also==
* 2005 in film

==References==
 
  tags!-->

==External links==
*  
*  
*  

 
 
 
 
 
 