Deathrow (film)
 
{{Infobox film
| name           = Deathrow
| image          =
| image_size     = 200px
| caption        = Movie poster for Deathrow
| director       = Joel Lamangan
| producer       = Executive producer Gilberto M. Duavit Felipe Gozon Menardo R. Jiminez
| writer         = Joel Lamangan Butch Jimenez Ricky Lee Manny Palo
| narrator       = 
| starring       = Eddie Garcia Cogie Domingo Angelika dela Cruz Jaclyn Jose Pen Medina
| music          = Ryan Cayabyab
| cinematography = Monino Duque
| editing        = Kelly N. Cruz Jess Navarro
| distributor    = GMA Films
| released       = December 25, 2000
| runtime        = 118 min
| country        = Philippines Tagalog / Filipino
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Deathrow is a 2000 Filipino melodrama film, directed by Joel Lamangan, about an orphaned boy whos roped into taking part in a robbery that ultimately changes his life. The movies plot is centered on 16-year-old Sonny Corpus (Cogie Domingo), a resident of the slums of  Manila. After being caught and convicted of robbery, Corpus is sentenced to death row. The film revolves around Corpuss life in prison and coping with his past actions.

The film received numerous awards, including Best Actor (Eddie Garcia), Best Production Design (Joey Luna) and Best Film Editing (Jess Navarro and Kelly N. Cruz) at the 2000 Metro Manila Film Festival. The film was also featured at the Film Academy of the Philippines, and won awards for Best Actor (Eddie Garcia), Best Editing (Jess Navarro), Best Production Design (Joey Luna) and Best Supporting Actor (Pen Medina).

==Plot==
16-year-old Sonny Corpus (Cogie Domingo), born and raised in the slums of Manila, frequently hangs out with his friends Celso, Jimmy, and Rodel. The three pressured Sonny to spend more time with them. Unknown to Sonny, his friends planned to rob a house one night. During the robbery, Celso accidentally fired at the owner of the house Anita Linda, killing her. Police came and Jimmy and Rodel were killed, prompting Sonny to freeze and Celso to run. The police finds Sonny at the scene and arrests him.

Sonny was detained and cross-examined. Sonnys lawyer pointed out that his client was still a minor. However, he failed to show ample evidence. Although Sonny didnt kill anyone during the robbery, he was found guilty of murder, which sends him to death row. After a harsh introduction to prison life during his struggle to come to terms with the courts verdict, he finds his place among the convicts.
 inmate who dealer for his cocaine smuggling business. Sonny hesitantly accepts. The cocaine business was known to the jail warden Fajardo (Spanky Manikan) who happens to be an accomplice in the illegal business. Gabino (Pen Medina), Mios second-in-command, attempts to win Sonnys support as part of his secretive preparations for a coup against Mio.

Fellow inmate Lolo Sinat (Eddie Garcia), a 77-year-old gangster who seems to be the most powerful and senior inmate on death row, eventually takes Sonny under his wings. He advised Sonny to stop his involvement in the drug business. When a high official visited the prison, Sonny confessed to him about the drug business. Gabino exacts revenge by giving Sonny a severe beating. Sonny was also raped and tortured.

Sonny sought the help of Gina (Jaclyn Jose), a public attorney who also handles the case of Lolo Sinat, to appeal his case. Lolo Sinat does not trust Gina, often calling her a stupid lawyer. Gina retaliated and told the old man that he doesnt know how to love. Lolo Sinat tells Sonny about his past and how he was brought to jail. The two became closer, and Lolo Sinat promised Sonny that he will help the boy so that when Lolo Sinat faces God, he could say that once in his life, he has done something right.

With the help of Lukas (Mon Confiado), Lolo Sinat and Sonny escaped, but were also caught the next day. Gina then tells Lolo Sinat that his death penalty will be served the next week. With that, the old man encouraged Gina to help Sonny appeal his case. Gabino tried to rape Sonny again, but the boy fought him. Sonny found a bread knife and stabbed Gabino several times, killing him instantly. Lolo Sinat covered up for Sonny.

Before being brought to the lethal injection chamber, Lolo Sinat gave his old walkman to Sonny.

After several weeks, Ginas appeal for Sonnys case became successful. Celso was brought to justice and Sonny was released from jail.

==Cast==
*Cogie Domingo – Sonny Corpus
*Eddie Garcia – Lolo Sinat
*Ray Ventura – Mayor Mio
*Pen Medina – Gabino
*Jaclyn Jose – Gina 
*Angelika dela Cruz – Sabel
*Tony Mabesa – Governor Asunta
*Spanky Manikan - Fajardo 
*Mon Confiado - Lukas
*Noni Buencamino - Nardo
*Ace Espinosa – Young Lolo Sinat
*Janine Desiderio – Ruby 
*Allan Paule – Cenon 
*Mel Kimura – Cenons Wife
*Jim Pebanco – Lupe 
*Maureen Mauricio – Sonnys aunt
*Anita Linda - robbery victim
*Joseph Izon – Celso 
*Marky Alonzo – Rodel 
*Randy Ramos – Jimmy 
*Richard Quan – Armand
*Tessie Villarama – Judge
*James Patricks – Sonnys Lawyer

==Media release==
The series was released onto DVD-format and VCD-format by GMA Records.

==Awards and nominations==
GMA Films entry to the 2000 Metro Manila Film Festival where it won awards for 2nd Best Film, Best Actor (Eddie Garcia), Best Production Design (Joey Luna), Best Film Editing (Jess Navarro and Kelly N. Cruz), Best Sound Recording (Albert Michael Idioma and Rudy Gonzales).

Won FAP (Film Academy of the Philippines) Awards for Best Actor (Eddie Garcia), Best Editing (Jess Navarro), Best Production Design (Joey Luna) and Best Supporting Actor (Pen Medina). 

Won Gawad Urian Awards for Best Actor (Eddie Garcia), Best Editing (Jess Navarro and Kelly N. Cruz) and Best Sound (Albert Michael Idioma and Rudy Gonzales).

It was given the Prix Du Meilleur Film Engage au Service d’une Cause (Prize for the Best Committed Film Championing a Cause) at the 23rd International Festival of Independent Films in Brussels, Belgium  in 2001 for its sensitive yet realistic depiction of the plight of a juvenile delinquent on death row.

It was nominated for Golden Cairo Award in the 2001 Cairo International Film Festival. It was shown and competed in other international film festivals.

It was shown at the 2001 Toronto Film Festival. 

==References==
 

==External links==
*   at the Internet Movie Database
*   at  
*http://www.answers.com/topic/deathrow-film
 

 
 
 
 
 
 
 