The Scarlet Blade
 
 
{{Infobox film
| name           = The Scarlet Blade
| image_size     =
| image	=	The Scarlet Blade FilmPoster.jpeg
| caption        = Poster with the US title
| director       = John Gilling
| producer       = Anthony Nelson Keys
| writer         = John Gilling
| narrator       =
| starring       = Jack Hedley Lionel Jeffries Oliver Reed June Thorburn
| music          = Gary Hughes
| cinematography = Jack Asher
| editing        = John Dunsford
| studio         = Hammer Films 
| distributor    = Warner-Pathé  
| released       =  
| runtime        = 83 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} British adventure film directed by John Gilling for Hammer Film Productions.

It is a period drama set during the English Civil War and stars Oliver Reed and Lionel Jeffries.

==Plot summary==
When King Charles I is captured by Roundhead forces led by the tyrant Colonel Judd and his right-hand man Captain Sylvester, it is up to a band of locals loyal to the King to try to rescue him. They are helped by Judds daughter Claire who secretly helps them in defiance of her father...

==Main cast==
* Lionel Jeffries as Colonel Judd  
* Oliver Reed as Captain Tom Sylvester  
* Jack Hedley as Edward Beverley, the Scarlet Blade  
* Robert Rietti as King Charles I
* June Thorburn as Claire Judd  
* Michael Ripper as Pablo  
* Harold Goldblatt as Jacob  
* Duncan Lamont as Major Bell  
* Clifford Elkin as Philip Beverley  
* Suzan Farmer as Constance Beverley   John Harvey as Sergeant Grey  
* Charles Houston as Drury

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 

 