Emir (film)
 
{{Infobox film
| name           = Emir
| image          = Emir.jpg
| alt            =  
| caption        = The original promotional poster.
| director       = Chito S. Roño
| producer       = Digna Santiago   Rolando Atienza   Nestor Jardin
| screenplay     = Jerry Gracio
| starring       = Frencheska Farr   Bayang Barrios   Bodjie Pascua   Gigi Escalante   Julia Clarete   Jhong Hilario
| music          = Chino Toledo   Vin Dancel   Ebe Dancel   Diwa de Leon   Gary Granada 
| cinematography = Neil Daza
| editing        = Jerrold Tarog
| studio         = Film Development Council of the Philippines   Cultural Center of the Philippines    Dreamaker Productions
| distributor    = VIVA Films
| released       =  
| runtime        = 135 minutes
| country        = Philippines
| language       = Tagalog, Arabic, English
                   and Ilocano
| budget         = Philippine Peso|PhP.70 million (estimated)  
| gross          = PhP.3,089,773 
}} Filipino drama film|drama-musical film by acclaimed Filipino director Chito S. Roño. The film revolves around a girl that becomes a domestic helper in a fictional oil-rich Arab nation of Yememeni.

==Plot== Ilocano family, Tagalog and play tumbang preso.

After twelve years of service, Amelias life has improved. Unfortunately, a war is coming and the royal family is forced to leave the palace in case they are attacked. When the palace is attacked, some of the maids and even the wife of the Sheikh are killed, but Amelia and Ahmed manage to escape by going to a secret door in the house and, with the help of Boyong the driver, go to a faraway place to hide.

One day, men riding on horseback took Ahmed away from Amelia. With the help of the government, Amelia returns home, where she starts her own business and lives a good life. Later she learns that the men who took Ahmed away were under orders from the Sheikh. Years later, someone with a familiar voice calls her "Yaya" and she sees Ahmed who has fully grown up. The film ends with Ahmed, Amelia and Boyong in Amelias house. The shy Boyong even gives Amelia a gold necklace, because he never had the chance to.

== Cast and Characters ==
*Frencheska Farr as Amelia
*Dulce as Ester
*Sid Lucero as Victor
*Jhong Hilario as Boyong
*Kalila Aguilos as Tersing
*Beverly Salviejo as Diday
*Julia Clarete as Angie
*Liesl Batucan as Pearlsha
*Melanie Dujunco as Mylene
*Gigi Escalante as Amelias grandmother
*Bayang Barrios as Amelias mom
*Bodjie Pascua as Amelias dad
*Emil Sandoval as Jamal
*Ned Hourani as Sheik
*Valerie Bariou as Sheika
*Ali Rasekhi as King
*Huriyeh Maghazehi as sister of Ahmed
*Joshua Elias Price Hourani as Ahmed (7 years old)
*Mahdi Yazdian Varjani as Ahmed (12 years old)
*Roxanne Almendral as Ahmed

== References ==
 

== External links ==
*  

 

 
 
 
 