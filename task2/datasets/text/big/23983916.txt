Mixed Blessings (film)
{{Infobox film
| name           = Mixed Blessings
| image          = Mixed Blessings DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = Bethany Rooney
| producer       = 
| writer         = Danielle Steel (novel) L. Virginia Browne Rebecca Soladay
| narrator       = 
| starring       = Gabrielle Carteris Scott Baio Bess Armstrong
| music          = Mark Snow
| cinematography = Mike Fash
| editing        = Janet Bartels-Vandagriff
| studio         =
| distributor    = NBC
| released       = December 10, 1995  (Canada)  December 11, 1995  (U.S.) 
| runtime        = 96 minutes USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1995 NBC 1985 novel of the same name written by Danielle Steel. It contains three stories of couples who are facing parenthood for the first time.   Scott Baio, Bess Armstrong, Gabrielle Carteris, and Bruce Greenwood lead the all-star cast. 

== Plot ==
Three couples are followed as they struggle to have children. Diana and Andy Douglas are a newlywed with great careers trying to have a baby for eleven months, without any results. As they visit the doctor, they are crushed to find out that Diana has problems with her ovary and she has a 1 in 10,000 chance to become pregnant. Diana, who always wanted to have a child, considers finding a surrogate mother, but the process proves to be very painful because Andy always wanted to have a child as well, she thinks she is preventing him from living his dream and files for divorce.
 adopt a child. They find Jane, a student who thinks a baby will destroy her promising future. After giving birth to a girl, Hilary, Diana is filled with joy to finally become a mother. Jane, however, changes her mind and claims her baby back. Meanwhile, Diana turns out to be pregnant after all and in the end, she ends up with two children, following Janes decision not to have the child after all.
 Las Vegas, Charlie makes a visit to the doctor and finds out he is sterile. However, five weeks later, Barbie announces she is pregnant, which means she cheated on him. Charlie immediately leaves her and later meets Beth, another orphan who is now enjoying her life as a single mother. They fall in love with each other and soon marry. Not only does Charlie become the father figure of her child, but they also decide to adopt another child.

The plot also centers on Pilar and Brad Coleman, an older couple who, after Brads daughter announces she is pregnant, decide to try and become pregnant as well. The age proves to be a great obstacle. Even after an artificial insemination, she suffers a miscarriage. Crushed, she decides not to continue the process, until she becomes pregnant. She eventually delivers twins, but one of them dies.

==Cast==
*Gabrielle Carteris as Diana Goode Douglas
*Scott Baio as Charlie Winwood
*Bess Armstrong as Pilar Graham Coleman
*Bruce Greenwood as Andy Douglas
*James Naughton as Brad Coleman
*Alexandra Paul as Beth
*Julie Condra as Barbara Elizabeth Barbie Chandler
*Barbara Tyson as Gayle
*Michelle Beaudoin as Jane
*Cassandra Rocan  as Baby
*Katelyn Rocan  as Baby

==Broadcast== CHCH on December 10, 1995.  The movie was shown the following night in the U.S. on NBC.

==References==
 

==External links==
* 

 

 
 
 
 
 