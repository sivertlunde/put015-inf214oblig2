The Marine 3: Homefront
{{Infobox film
| name           = The Marine 3: Homefront
| image          = The marine 3 homefront poster.jpg
| alt            = Mike Mizanin
| director       = Scott Wiper
| producer       = Michael Luisi
| writer         = {{plainlist|
* Scott Wiper
* Declan OBrien
}}
| starring       = {{plainlist| Mike Mizanin
* Neal McDonough
* Ashley Bell
}}
| music          = Robert Revell
| cinematography = Ron Stannett
| editing        = Dallas Puett
| studio         = WWE Studios  (Homefront Productions, Inc.) 
| distributor    = 20th Century Fox
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = $1.5 million   
| gross          = 
}} Mike Mizanin film series and is a sequel to The Marine starring John Cena and The Marine 2, starring Ted DiBiase, Jr.

==Plot==
Sergeant Jake Carter of the MARSOC United States Marine  returns home to Bridgeton, a rural town located near Seattle, Washington. As he steps off the bus, he reunites with his best friend Harkin. When he returns home he greets his sisters Lilly and Amanda, and they throw a party for his homecoming. Meanwhile in Seattle, a regional bank is robbed by a syndicate believed to be extremists; their leader, Jonah Pope, forces the bank manager to give them what they demand. They take half the money and burn the rest in front of him. It is revealed that the syndicate dwells in a junkyard on the outskirts of town, their leader planning an explosion in the city because of revenge from his past life.

Meanwhile, Jake talks to Amanda about her running the house, but they get interrupted by Lilly, who was about to leave with her boyfriend, Darren (Jeff Ballard) in a bar. Jake follows them and later ends up in a fight. Lilly sees the scene and leaves with Darren; Harkin, who also witnesses the fight, calls the cops and gives Jake a warning ticket. Harkin warns him that if he gets into a fight again hell be arrested. Moments later, Amanda arrives to pick him up and admits to Jake that she loves Harkin. The next day, Lilly and Darren stop by the same junkyard in which the syndicate dwells, and the two later witness a commotion among Jonas Pope and a smuggler. Pope becomes furious and shoots the smuggler; he then orders his henchmen to seize the witnesses when he hears Lilly yell for help. Amanda contacts Jake about Lillys abduction, and Jake drives off to the junkyard, where he sneaks up on a henchman who dumps off the body of the smuggler. Jake is able to hold him at gun point, and when another henchman arrives, Jake physically overpowers them after they attempt to kill him. Jake then attacks another henchman, who is revealed to be an FBI Agent before Special Agent Wells comes to his aid. Jake is brought to the FBI and informs them that the syndicate does more than kidnapping but bombing the city with RDX (Research Department Explosive), a very capable explosive.

Jake is restricted by Wells when he plans to rescue the women by himself; instead, Wells orders the SWAT to storm the junkyard. Meanwhile, Lilly and Darren, struggling to escape, get help from a henchman who is actually an FBI Agent in disguise; he leads them to a room and tells them to lock it until he returns. SWAT storms the base, but they are overpowered by the syndicate. Jake attempts to fight Wells, but Harkin stops him. Harkin drops him off the junkyard to fight the syndicate. Jake finishes them off one-by-one, but he gets a bone to pick with a man named Gabriel, whom he defeats. Meanwhile, Wells receives a call from Pope telling him that he should send an unarmed cop in a sedan to escort Lilly; if it does not arrive in 30 minutes, Lilly will die. When the sedan arrives, Eckert kills the cop and wears his uniform. Jake then meets up with Darren; Darren, feeling guilty of Lillys captivity, leads Jake to the spot. Jake attempts to rescue them, but he is stopped by Galen Jackson; the sedan then speeds away. Pope contacts Wells that he and Eckert should be left alone or he will kill Lilly. At the junkyard, Jackson searches for Jake but was killed by Darren. Jake, determined to save the city, grabs a dirt bike and speeds away with Darren on his tail.

When Pope and Eckert reach the city they were suddenly ambushed Eckert gets killed by a fellow agent while Pope takes Lilly hostage but he was cornered by Harkin followed by Jake and they both successfully kill Pope. Lilly then tells them that the sedan is a bomb so Jake drives the RDX and manages to escape the untimely explosion. Harkin, Lilly, and Amanda rescue him, and the two sisters hug him. Lilly is amazed when Darren arrives, and they kiss.

==Cast== Mike "The Miz" Mizanin as Jake Carter
* Neal McDonough as Jonas Pope
* Michael Eklund as Eckert
* Ashley Bell as Lilly Carter
* Camille Sullivan as Amanda Carter
* Jared Keeso as Harkin
* Jeff Ballard as Darren

==Production== Ted DiBiase. On February 25, 2012, WWE Studios announced a three film distribution deal with 20th Century Fox Home Video and The Marine 3: Homefront was the first of that deal. 20th Century Fox will handle the global distribution of the film on DVD, Video On Demand, and Online outlets. WWE Studios also used WWEs television and internet status to promote and market the film.

The film originally cast Bob Holly as the leading role but due to a neck injury, the role was passed to Randy Orton. Orton later tweeted to confirm it.  On April 3, 2012, it was reported that Randy Orton dropped out of the role because of his bad conduct discharge from the Marines in the late 1990s. Orton twice was absent without leave and disobeyed an order from a commanding officer. Ortons old Marine unit displayed public outrage of Randy being cast in the film due to his history.  Orton later appeared in a WWE Studios film, starring in The Condemned 2: Desert Prey.

On April 30, 2012, it was announced that Mike Mizanin would replace Orton as the lead in the film.     On June 11, 2012, it was announced that Ashley Bell and Neal McDonough have joined the cast. Scott Wiper was in talks to direct the film and to write the script.  Filming began in the June 2012 and was shot in Vancouver and Maple Ridge, British Columbia. 

==Release==
The Marine 3: Homefront was released on DVD and Blu-ray on March 5, 2013, where it grossed $3,384,892 on U.S. sales.   It placed at #12 on the top DVD sales, and #15 on top Blu-ray sales. 

==Reception==
The Marine 3: Homefront gained mixed reviews from critics, a review from Pro Wrestling Torch.com stating, "although Marine 3 is not a good movie and Miz is miscast in the lead role, he did show some chops as an actor here and could definitely play a supporting role in future releases and not seem out of place. Unfortunately much like his current role in WWE, Miz is out of his element in Marine 3." 

Another review from The Action Elite.com states: "The Miz is actually a very good and entertaining action star. I am surprised and impressed with his performance. The action is more grounded in reality than the original. The fights are brutal and fun to watch. If you need a nice change of pace, you will definitely enjoy The Marine 3. It is a very fast paced piece of action cinema that will get you pumped." 

==Sequel==
Filming for   began in April 2014. The Miz was recast to reprise his role as Carter and was joined by WWE Diva Summer Rae as the first female wrestler to appear in a WWE Studios film. It will be the first The Marine film to star the same WWE superstar as its predecessor.

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 