Goodbye Pork Pie
 
 
{{Infobox film
| name           = Goodbye Pork Pie
| image          = Goodbye Pork Pie (DVD Cover).jpg
| writer         = Geoff Murphy Ian Mune Kelly Johnston Bruno Lawrence
| director       = Geoff Murphy
| cinematography = Alun Bollinger
| editing        = Michael J. Horton
| producer       = Geoff Murphy Nigel Hutchinson
| released       = 6 February 1981
| runtime        = 105 min
| language       = English
| country        = New Zealand
| budget         = New Zealand dollar|NZ$350,000 
| gross = NZ$1.4 million 
}}
Goodbye Pork Pie is a 1981 New Zealand film directed by Geoff Murphy and written by Geoff Murphy and Ian Mune. The film was New Zealands first largescale local hit. One book described it as Easy Rider meets the Keystone Kops. 

It was filmed during November 1979, using only 24 cast and crew. Its overheads were surprisingly minimal, to the point that the Police cars used doubled as crew and towing vehicles, and that the director Geoff Murphy performed some of the stunts himself.

==Plot== Northland town Kelly Johnson) opportunistically steals a wallet accidentally dropped by a wealthy woman named Lesley Morris. Finding cash and her drivers licence inside it, he uses them to rent a yellow Mini from The Hertz Corporation|Hertz. With no particular aim in mind, he drifts down to Auckland. Meanwhile in Auckland, the middle-aged John (Tony Barry), has just had Sue, his girlfriend of six years, walk out on him and fly home to Invercargill. After a night on the bottle, John decides to go down to Invercargill.
 drive off without paying, drawing police attention to the car.

Further down the road, Gerry and John pick up Shirl (Claire Oberman), who is heading to Wanganui, and after informing the duo that she is a virgin, Gerry makes a bet that this will change before reaching Wanganui. After failing to pay for petrol on purpose in the central North Island, they are pursued by a motorcycle officer, with the duo ending up avoiding arrest by driving into a car wreckers. On the road to Wanganui, the trio discuss about whether Gerry had sex with Shirl or not when he tried to silence her in the chase beforehand. However, the discussion ends in a stalemate.

Shirl doesnt leave at Wanganui, and decides to stays with the guys and go to Wellington. In Wellington, the trio meet Mulvaney (Bruno Lawrence), an old associate of Johns, who gives the trio overnight accommodation at his garage in Wellington, and supplies them with money and drugs in return for parts of the car. The next morning, the trio leave and head to the inter-island ferry terminal. Gerry accidentally runs a red traffic light, and is immediately pursued by the police through central Wellington. The trio avoid the police by driving through the Wellington Railway Station and stowing the Mini in an empty boxcar being shunted onto the Interislander|inter-island ferry.
 West Coast until that night, so spend the day on the town. Gerry and John return to the train, but notices Shirl is not there, and the train leaves without her.

After leaving the train, Gerry and John stop at a tearoom further down the coast. They find out from the television that Shirl was arrested for shoplifting, and that theyre wanted in a national man-hunt of the "Blondini Gang". Crossing back over the Southern Alps, the Mini is pursued by a determined police officer down the Lake Hawea shoreline. He almost catches Gerry and John, but ends up driving off the road and down the bank trying to avoid a combine harvester blocking the road.  The duo sell more parts off the car at Cromwell, New Zealand|Cromwell, but they are short-changed and John take a full petrol can as extra payment.
 PIT the Mini, and Gerry falls off and is hit by the pursuing police car. John says goodbye to Gerry, then takes the car and proceeds to Invercargill.

John encounters a roadblock at the entrance to Invercargill and diverts through a cemetery to avoid it, but not before the Armed Offenders Squad shoots a hole in the Cromwell fuel can, spilling the petrol. Approaching Sues house, the insecure exhaust dragging along the road ignites the fuel, setting it alight. John reunites with Sue, but their reunion is short-lived when the burning Mini draws the attention of emergency services. With police surrounding the house, John finally admits defeat and surrenders himself.

==Cast== Kelly Johnson as Gerry Austin
*Tony Barry as John
*Claire Oberman as Shirl
*Shirley Gruar as Sue
*Don Selwyn as the Kaitaia police officer
*Bruno Lawrence as Mulvaney
*Marshall Napier as the Lake Wanaka/Lake Hawea police officer
*John Bach as Snout

==Impact== Sleeping Dogs, New Zealand cinema as it showed that New Zealanders could make successful films about New Zealand. It was the first really financially successful New Zealand film of modern times.

==Production==
Goodbye Pork Pie was filmed chronologically over six weeks in late 1979, following the north to south route taken by the films protagonists. Filming began in the northern town of Kaitaia and ended in Invercargill, near the bottom of the South Island. Director Geoff Murphy had been good friends with star Tony Barry (Smith) as well as  Bruno Lawrence before Goodbye Pork Pie. The three had played together in multi-media group Blerta.

Geoff Murphy cameos in the film as a man working at the second petrol station. Co-producer Nigel Hutchinson sells a banana milk shake "with an egg in it" to John a short time before Gerry falls off the car.

===Cars===
The yellow mini was a 1978 British Leyland Mini 1000, registered IZ6393. However, three 1978 Minis were used during filming. They were sourced from the New Zealand Motor Corporation (assemblers of British Leyland products in NZ). After the film two of the Minis that were undamaged were returned to The New Zealand Motor Corporation. The third, which had a hole cut in the roof and the front bodywork removed, was used for promotion and is still in New Zealand, its actual registration is IX2992 A fourth, 1959 Mini was used for the final scene where it was burnt out. The Police Holden HQs used in the film doubled as towing and support vehicles for the cast and crew. It is the same police cars chasing the Mini throughout both the North and South Islands.

===Locations=== Mangere Bridge. The new bridge, at the time of the films production, was on hold for a couple of years in an unfinished state due to prolonged industrial action. 
 Meremere coal-fired power station. 
 National Park, State Highway 4 and State Highway 47.

The Interislander ferry that the Mini is stowed on between Wellington and Picton is the  .
 Christchurch railway station where the mini is stowed while in Christchurch, was closed in 1993 (with a new station built at Addington) this building was used as a movie theatre and Science Alive from 1993, until the building was badly damaged in the 2011 Christchurch earthquake. The building was demolished in 2012.
A later scene in the film shows Blondini and John in Cromwell, New Zealand|Cromwell, Central Otago. The part of Cromwell shown is now underwater, due to the Lake Dunstan hydroelectric project. 

The scene at McNab, Southland where Gerry is caught by the police shows a decrepit old toilet block at the side of the road. It was actually a temporary structure built specifically for the film.

==References==
 

==External links==
* 
*   on NZ On Screen
* 


 

 
 
 
 
 
 
 