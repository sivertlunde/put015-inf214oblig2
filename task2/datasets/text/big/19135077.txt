Out of the Shadow (1961 film)
{{Infobox film
| name           = Out of the Shadow
| image          = "Out_of_the_Shadow"_(1961_film).jpg
| image_size     =
| caption        =
| director       = Michael Winner
| producer       = Olive Negus-Fancey Michael Winner
| writer         = Michael Winner
| narrator       =
| starring       =  Terence Longdon  Donald Gray  Diane Clare  Robertson Hare Dermot Walsh
| music          = Jackie Brown Cy Payne
| cinematography = Dick Bayley
| editing        =
| studio         = Border Film Productions
| distributor    = New Realm Pictures
| released       = 1961
| runtime        = 61 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}} 1961 British thriller film directed by Michael Winner and starring Terence Longdon, Donald Gray, Diane Clare, Robertson Hare and Dermot Walsh. 

==Plot==
Reporter Mark kingston learns that his brother, who was studying at Cambridge University, has committed suicide. Unconvinced, he begins his own investigation when the police dismiss his suspicions. With the help of Mary Johnson, whose professor father has turned up missing, Kingston attempts to prove that there has been a murder and not a suicide on the campus. 

==Cast==
* Terence Longdon as Mark Kingston 
* Diane Clare as Mary Johnson
* Donald Gray as Inspector Wills  
* Robertson Hare as Ronald Fortescue 
* Dermot Walsh as Professor Taylor 
* Felicity Young as Waitress  Douglas Muir as Killer

==DVD release== Final Appointment.

==External links==
* 
* Buy DVD at  

==References==
 

 
 
 
 
 