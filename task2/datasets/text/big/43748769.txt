Vazhvile Oru Naal
{{Infobox film
| name           = Vazhvile Oru Naal வாழ்விலே ஒரு நாள்
| image          =
| image_size     =
| caption        =
| director       = A. Kasilingam
| producer       = U. R. Jeevarathinam
| writer         = A. V. P Asaithambi
| screenplay     = A. V. P Asaithambi T. N. K. Perumal
| story          = A. V. P Asaithambi
| starring       = Sivaji Ganesan G. Varalakshmi Sriram Rajasulochana
| music          = C. N. Pandurangan T. G. Lingappa S. M. Subbaiah Naidu
| cinematography = G. K. Ramu V. Kumaradevan
| editing        = P. V. Narayanan
| studio         = Mercury Films
| distributor    = Mercury Films
| released       =  
| country        = India Tamil
}}
 1956 Cinema Indian Tamil Tamil film, directed by A. Kasilingam and produced by U. R. Jeevarathinam. The film stars Sivaji Ganesan, G. Varalakshmi, Sriram and Rajasulochana in lead roles. The film had musical score by C. N. Pandurangan, T. G. Lingappa and S. M. Subbaiah Naidu.  

==Cast==
*Sivaji Ganesan
*G. Varalakshmi
*Sriram
*Rajasulochana
*V. K. Ramasamy (actor)|V. K. Ramasamy

==Soundtrack==

The music was composed by C. N. Pandurangan, T. G. Lingappa & S. M. Subbaiah Naidu.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Thendrale Vaaraayo || T. M. Soundararajan & U. R. Jeevarathinam || || 
|- 
| 2 || Ninaive Avar Ninaive || T. A. Mothi & Jikki || ||
|- 
| 3 ||  ||  || ||
|- 
| 4 ||  ||  || ||
|- 
| 5 ||  ||  || ||
|- 
| 6 ||  ||  || ||
|- 
| 7 ||  ||  || || 
|- 
| 8 ||  ||  || || 
|- 
| 9 ||  ||  || || 
|}

==References==
 

==External links==
*  

 
 
 
 
 


 