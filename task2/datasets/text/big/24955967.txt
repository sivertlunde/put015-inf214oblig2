The Treasure Hunter
 
{{Infobox film
| name           = The Treasure Hunter
| image          = The Treasure Hunter-poster.jpg
| caption        = Promotional poster for The Treasure Hunter 
| director       = Chu Yin-Ping
| producer       = Han Sanping Han Xiao Li Jiang Tai Raymond Lee Pei Gin Yam Du Yang Ding Li Dong Zheng Rong Han Xiao
| writer         = Charcoal Tan Yip Wan Chiu Lam Chiu Wing Lam Ching Yan Shao Hui Ting
| starring       = Jay Chou Lin Chi-ling Eric Tsang Chen Daoming Baron Chen Will Liu Miao Pu
| music          = Ricky Ho
| cinematography = Zhao Xiaoding
| editing        = Chen Bo Wen
| studio         = Chang Hong Channel Film & Video Co.
| distributor    = Chang Hong Channel Film & Video Co.
| released       =  
| runtime        = 105 minutes
| country        = Taiwan
| language       = Mandarin Cantonese
| budget         =
| gross          = 
}}
The Treasure Hunter ( ) is a 2009 Taiwanese action film directed by Taiwanese director Chu Yin-Ping and stars Taiwanese actors Jay Chou and Lin Chi-ling. Ching Siu-tung served as action director.

==Cast==
{| class="wikitable"
|-
! width=30% | Cast  
! width=50% | Role
|-
|-
| Jay Chou || Qiao Fei 喬飛
|-
| Lin Chi-ling || Lan Ting 藍婷
|-
| Eric Tsang || Pork Rib 排骨
|-
| Chen Daoming || Master Hua Ding Bang 華爺（華定邦）
|-
| Baron Chen || Desert Eagle 大漠飛鷹
|-
| Will Liu || Friday 星期五
|-
| Miao Pu || Swords Thirteen 刀刀（刀十三郎）
|-
|}

==Synopsis==
In the northwest desert where countless prosperous dynasties have flourished and fallen, there is a rumor that buried amongst the sand exists a tomb containing countless riches. A group of mysterious guardians have been guarding the map to the location of the treasure until a fierce rivalry erupts. A notorious international crime group, The Company, manage to hunt down the map keeper but not before he manages to pass the map to a young chivalrous man, Qiao Fei (Jay Chou).

Qiao Fei is forced to give up the map to save the life of his mentors daughter Lan Ting (Lin Chi-ling). Teaming up with Hua Ding Bang (Chen Daoming), who is a famous archeologist, and Lan Ting they embark on a dangerous journey to recover the map and fight to protect the ancient treasure. 

===Critical response===
The film was generally panned by critics.

According to Perry Lam of Muse (Hong Kong magazine)|Muse Magazine, Chou plays his role with a mix of nonchalance and self-righteousness which is simply irritating. He fits perfectly into a movie that, though trying hard to be escapist entertainment, often approaches its material with utmost solemnity; anything resembling a laugh, let alone fun, is taken out. 

==References==
 

==External links==
*  at the Hong Kong Movie Database
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 