Upside Down (film)
 
{{Infobox film
| name           = Upside Down
| image          = Upside Down Poster.jpg
| caption        = US release poster
| director       = Juan Diego Solanas
| producer       = Claude Léger Dimitri Rassam Aton Soumache Jonathan Vanger Alexis Vonarb
| writer         = Juan Diego Solanas
| starring       = Jim Sturgess Kirsten Dunst
| music          = Benoît Charest Sigur Rós
| cinematography = Pierre Gill
| editing        = Dominique Fortin Paul Jutras
| studio         = Onyx Films Studio 37
| distributor    = Warner Bros. (France) Millennium Entertainment (USA) 
| released       =  
| runtime        = 107 minutes
| country        = France Canada
| language       = English
| budget         = $50&nbsp;million 
| gross          = $8,106,475   
}} romantic science fiction film written and directed by Juan Diego Solanas, starring Jim Sturgess and Kirsten Dunst.

==Plot==
The film starts with Adam telling the story of his two-planet home world, unique from other planets or planetary systems as it is the only one that has "dual gravity". This phenomenon of dual gravity allows the two planets to orbit each other in what would otherwise be impossibly close proximity. There are three immutable laws of gravity for this two-planet system:

# All matter is pulled by the gravity of the world that it comes from, and not the other.
# An objects weight can be offset using matter from the opposite world (inverse matter).
# After a few hours of contact, matter in contact with inverse matter burns.

The societies of the two worlds are segregated by law. While the upper world (Up Top) is rich and prosperous, the lower (Down Below) is poor. Up Above buys cheap oil from Down Below and sells electricity back to Down Below at higher prices. A person from Down Below going Up Top (or having contact with anyone from Up Top) is strictly forbidden and can be punishable by incarceration or death. People from Up Top regularly visit Down Below to experience novelties like dancing on ceilings. The only physical connection linking the two worlds is the headquarters of the "TransWorld" company.

Adam lives in an orphanage in Down, having lost his parents in an oil refinery explosion. The only living relative he has is his great-aunt, whom he visits every week. His great-aunt has a secret recipe for flying pancakes using pollen from pink bees which gather pollen from both worlds. The recipe has passed through generations and will be inherited by Adam.

As a child, Adam secretly climbs a mountain that gets very close to Up. There he meets Eden, a girl from Up. Years later in their teens, they are in a relationship. They meet on the mountains and Adam uses a rope to pull Eden towards Down, and they head to the woods for a stroll. They are later discovered, and while Adam frantically releases Eden back to her world, he catches a bullet in his arm and drops her. Helpless, he watches Eden lying motionless on the ground as blood oozes from her head. When he returns home, his aunt Becky is arrested and her home is put to the torch.
 obtain rare stamps from Down. Bob offers to help him contact Eden.

With the help of Bob, Adam meets Eden by putting Up-material in his clothes to disguise himself as a worker from Up, using Bobs name as his own. But Eden doesnt recognize him because of amnesia from the accident as a teen. The Up-material in Adams clothes starts to burn so he has to return to Down. Later on, Bob is fired but as he leaves, he secretly gives Adam his ID to help him exit the TransWorld building and into Up. Later, by calling Eden through Bobs phone, Adam manages to get a date.

Meanwhile, his cosmetic cream becomes of great importance to the company. While Adam is doing a presentation of the cream, Eden enters the lecture hall and discovers his true identity. After she flees the auditorium Adam runs to find her but Bobs ID, having been terminated, lands him in trouble. He escapes to Bobs house. He shows him that mixing liquids from both gravity fields can make a hybrid solution that resists both gravitational fields and simply floats between the two. Adam then reveals that he didnt give TransWorld the main secret ingredient of his compound, leaving the company unable to manufacture the product without him.

With Bobs help, he goes back to the restaurant where he met with Eden before and finds out she has begun to recover old memories of him. But the police arrive and he has to run. Upon returning to his planet he goes to the mountain top where he met Eden. Eden comes to find him and they meet again as they did long ago. But police are on their trail and, as they fail to escape, Eden is arrested while Adam falls the remaining distance between worlds. But he survives because of a vest containing inverse matter which he still had strapped to his torso. TransWorld agrees to drop the charges against Eden if Adam gives them his formula and never contacts Eden again.

Now Adam has gone back to his old life, believing he will never see Eden again. But Eden, not so easily dissuaded, goes to Bob for help. Bob finds Adam and surprises him by showing he can stay Down without the help of the opposite-matter accoutrements; Bob has been able to use Adams methods to create a way to negate the effect of gravity. Bob tells him he had purchased the patent of his beauty cream before TransWorld attempted to do so. He finishes by informing him that Adam also has a "date" with someone.

The film ends with Eden revealing she has become pregnant with twins, and the camera zooms far out to reveal towering skyscrapers on both sides, showing that both sides have become prosperous.

==Cast==
* Jim Sturgess as Adam Kirk
* Kirsten Dunst as Eden Moore
* Timothy Spall as Bob Boruchowitz
* Jayne Heitmeyer as Executive
* Blu Mankuma as Albert

==Release==
The film was released in France on 27 March 2013 (Mauvais Genre Film Festival)    and, more generally, on 1 May 2013 through the local branch of Warner Bros., while Samuel Goldwyn Films (uncredited) and Millennium Entertainment bought the North American and Lionsgate bought the United Kingdom distribution rights. 

The film was released in a limited capacity (11 theaters initially) on 15 March 2013 in the USA. The film was released on 23 August 2012 in Russia.   

The film was released on Blu-ray and DVD on 25 June 2013.

==Production==
The French production company Studio 37 initially searched for an American co-producer, and received positive response from Hollywood representatives who read the screenplay. However, because of cultural differences, they decided to look for European partners instead, as they thought it would be essential for the project to be driven primarily by its director.  The film was eventually produced by Studio 37, Onyx Films and the Montreal-based company Transfilm, for a budget of $50&nbsp;million.   
 Cannes Film Market in 2009 that Kirsten Dunst and Emile Hirsch were in talks to play the films two leading roles.  A few months later the same magazine reported that Jim Sturgess had been cast instead of Hirsch. 

Principal photography started in Hancock, Michigan in February 2010.  Filming and post-production were located in the U.S. because of the countrys low taxes for film productions. Producer Dimitri Rassam said: "We couldnt have made Upside Down without the French funding system but there was no way we could have shot   because the tax rebate is not attractive enough." 

==Reception==
As of mid May 2013, Upside Down received 29% positive reviews according to movie review aggregator Rotten Tomatoes with the consensus "In spite of its wonderfully unusual premise and talented cast, Upside Down fails to offer much in the way of compelling drama to anchor its admittedly dazzling visuals." 

Mick LaSalle was one of several reviewers who admired the films "brilliant" and "imaginative basis" while feeling ultimately disappointed, saying its "rich and bizarre premise is supported by fully realized visuals that make the fantastic real... its all very enjoyable." However, he wrote, "The only problem is that, after creating the most wonderful fantastic frame, Upside Down doesnt devise a picture worthy of it. The story is serviceable. It starts small, and it stays small, even though the circumstances surrounding the story seem to cry out for something bigger." 

  also had a mixed reaction, with its reviewer praising the "wonderful visual shock" and its "marvelous sense of space and style" and writing, "Solanas idea is a pretty audacious one, visually. A political one too, as it turns out that for generations the upper world (think Northern Hemisphere) has been getting fat exploiting the resources of the lower one (think Southern Hemisphere)," but concluding that the film "doesnt really develop its story, or its themes." 

Frank Scheck found the film confusing, saying, "You practically need an advanced degree in physics to fully comprehend the convoluted physical machinations depicted in Upside Down, Juan Solanas dizzyingly loopy sci-fi romance. Depicting the Romeo and Juliet-style romance between lovers from twin planets with opposite gravitational pulls, this head-scratcher boasts visual imagination to spare even as its logistical complexities and heavy-handed symbolism ultimately prove off-putting." 

==See also==
*Cosmicomics
*Head over Heels (2012 film)|Head over Heels (2012 film)
*Patema Inverted

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 