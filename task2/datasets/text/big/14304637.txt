Sundara Purushan (1996 film)
 
{{Infobox film
| name           = Sundara Purushan
| director       = Sabapathy Dekshinamurthy|D. Sabapathy 
| producer       = R. B. Choudary Livingston Sivaram Gandhi (dialogues) Livingston Rambha Rambha Vadivelu
| music          = Sirpy
| cinematography = Arthur A. Wilson
| editing        = J. Shankar Super Good Films Super Good Films
| released       =  
| runtime        = 149 minutes
| country        = India
| language       = Tamil
}}
 Livingston and Rambha in Andala Ramudu Sunil and Aarti Agarwal.

==Plot== Livingston has Rambha right from his childhood. Things go wrong for him when his mother dies; his father marries another woman which he dislikes and runs away from home. He returns home (to his grandmother) after twelve years only after his father’s death. He even accepts his brother (Vadivelu) born to his step mom.

His love for Rambha remains unchanged. In fact, he returns home after so long time only to win her love and marry her.  On the contrary, she loves another person, who is an orphan and also jobless. Her father  dislikes this; he lays a condition that he would agree for their marriage only if her lover finds a job.

Ignorant of the fact, but with good intention, The hero offers a job to the lover. Later, he learns about the fact and gets despaired. In such a circumstance, the hero’s brother resolves to unite his brother with the heroine and eliminate the hero’s misery. The brother tactfully implicates the lover in a murder and sends him to prison. Thus he plays a spoil game in averting marriage between Rambha and Raghu. In such a distressed condition, Rambha’s father with no other option remaining pleads with Livingston to marry Rambha. he readily agrees and marries her.

What happens when she learns about the fact later? Will she continue her married life with him or does she goes to her lover or anything strange happens. The remaining part of the movie is based on these circumstances.

==Cast== Livingston
*Rambha Rambha
*Vadivelu

==Production==
Livingston told the story of "Sundarapurushan" for every producers, but producers said that he only should do the direction not to play lead role, but it was R. B. Choudhary who encouraged him to do it. 

==Soundtrack==
* Aandipatti Arasampatti
* Eerakathu
* Marutha Azhagaro
* Rajarajane
* Setup Maathi
* Vennila
* Thangamanasin

==References==
 

 
 
 
 
 