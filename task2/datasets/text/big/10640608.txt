Athisaya Piravi
{{Infobox film
| name = Athisaya Piravi
| image = Athisaya Piravi DVD Cover.jpg
| caption = DVD Cover
| director = SP. Muthuraman
| writer = Panju Arunachalam Kanaka Nagesh Jaiganesh Betha Sudhakar
| producer = A. Poorna Chandra Rao
| music = Ilaiyaraaja
| cinematography = T. S. Vinayagam
| editing = R. Vittal S. P. Mohan
| studio = Lakshmi Productions
| distributor = Lakshmi Productions
| released = June 15, 1990
| runtime =
| country = India
| language = Tamil
| budget =
}} 1990 Kollywood|Indian Kanaka and music composed by Ilaiyaraaja. The film was directed by SP. Muthuraman. It was a block buster at the box office. The film is a remake of Yamudiki Mogudu, a 1988 Telugu film starring Chiranjeevi (in a dual role), Radha (actress)|Radha, Vijayashanti, Kota Srinivasa Rao and Allu Rama Lingaiah.

==Plot==
Kaalai (Rajinikanth), is in love with a girl called Sheeba. As usual, she is rich while Kaalai is poor. Sheebas fathers business partner has a son who wants to marry Sheeba, but Kaalai is in his way. Sheebas dad soon finds out about this and him and his partner plan to kill Kaalai. Sheebas dad tricks Kaalai into thinking that he was a good person and make Kaalai think that he is going to marry him to Sheeba. But this is all a plan to kill him. They succeed in killing Kaalai. Kaalai goes to the underworld and meet the Lord of Death himself, Yamadharma Raja (Vinu Chakravarthy). Kaalai explains to Yama that he is getting married and someone planned to kill him, so Yama gives Kaalai the body of Balu. Balu is identical to Kaalai. His uncle and aunt planned to kill him. So now there is a Rajinikanth with the body of Balu and the brain of Kaalai. From then on the movie enters a masala genre.From that day kaalai stays in balus house forgetting about is previous life. He made balus uncle, aunt and his cousin chinni jayanth to work hard and treated them on the same way as they ill treated him and his mother. One day Nagesh father of sheeba, who was an old friend of balus uncle comes to meet him. seeing him, kaalai who is inside inside balu remembers his previous life. So immediately he fleds off to his previous place. there he finds the criminals attacking his area. He summons before them, all are astonished and escapes from the place. He then explains to his mother and sheeba, he was not dead. Mean while kanaga from balus village comes to chennai to search for balu. So kalai struggles very much to manage between both lovers. In between, rajini with help of visithragupta (cho ramaswamy) plans and outwrights jaiganesh and nagesh, such that he was an wealthy person, and plans to nose cut them at right time. But when balus uncle and aunt with chinni jayanth arrives to chennai, the confusion revealed to nagesh. and nagesh catches both the families of kalai and balu, and tries to kill him. But rajini kanth maintains his cleverness and saves both the family. But sheeba and kanaga was in anger with rajini. Vinuchakaravarthi tells him a confidential mantra, which rajini tells to both of kanaga and sheeba in privacy, and they both get compromised. 



==Cast==
* Rajinikanth as Kaalai and Balu Kanaka as Lakshmi
* Sheeba as Sumathi
* Nagesh
* Jaiganesh
* Betha Sudhakar
* Cho Ramaswamy as VichitraGupta
* Vinu Chakravarthy as Yamadharma Raja
* V. K. Ramaswamy (actor)|V. K. Ramasamy as Chitra Gupta Madhavi as Rambai
* kugan as Kugan

==Legacy==
  dwarf South Indian actor Kingkong dancing to "Holiday Rap" by MC Miker G & DJ Sven.

==Soundtrack==
This sound track has 6 songs are composed by Ilaiyaraja.
 
{| class="wikitable"
|-bgcolour="#CCCCCC"
!Songs!! Singers!! Lyrics
|- Vaali
|-
| Idhaz Sindudum || Malaysia Vasudevan || Pulamaipithan
|-
| Paattukku Paattu || Malaysia Vasudevan, K. S. Chitra || Gangai Amaran
|-
| Singari Pyirai || Malaysia Vasudevan, S. Janaki || Piraisoodan
|- Malaysia Vasudevan, S. Janaki  || Vaali
|-
| Unna partha || Malaysia Vasudevan, K. S. Chitra || Vaali
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 