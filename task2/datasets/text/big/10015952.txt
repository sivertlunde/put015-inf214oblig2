Red Skies of Montana
{{Infobox film
| name           = Red Skies of Montana
| image          = 
| producer       = Samuel G. Engel
| director       = Joseph M. Newman
| screenplay     = Art Cohn
| story          = Harry Kleiner George R. Stewart
| starring       = Richard Widmark Constance Smith Jeffrey Hunter Richard Boone
| music          = Sol Kaplan
| cinematography = Charles G. Clarke
| editing        = William Reynolds
| distributor    = 20th Century Fox
| released       = February 1952
| country        = United States
| runtime        = 99 min. English
| gross          = $1.25 million (US rentals) 
}}

Red Skies of Montana is a 1952 adventure drama in which Richard Widmark stars as a smokejumper who attempts to save his crew while being overrun by a forest fire, not only to preserve their lives, but to redeem himself after being the only survivor of a previous disaster.

The film was loosely based on the August 1949 Mann Gulch fire,  and filmed on location in Technicolor with the cooperation of the United States Forest Service.

Bugle Mountain (also known as "Bugle Peak"), located in the Scapegoat Wilderness near Lincoln, Montana, gave its name to the fictional setting of the forest fire in the Selway National Forest shown during the first 30 minutes of the film.

==Plot== shock and wandering through the devastated region. Cliff is rushed to the hospital, where he gradually recovers, although he cannot remember how he got separated from his men, or why he was the only one to survive.

Upon his return home, Cliff is greeted by Pops son Ed, who is also a smokejumper. Ed expresses genuine concern for Cliff, but Cliff, sensitive about his lack of memory and worried that he might be responsible for his crews deaths, becomes antagonistic. A board of review conducts a hearing into the matter, and Cliff grows increasingly defensive after several grueling days of repetitious questioning. Cliffs paranoia grows that he might be thought a coward who deserted his men despite the assurances of his devoted wife Peg and Dick, who lets him return to work only as supervisor of training. Ed continues to grill Cliff, asking him how he might have come to be in the protected rock slide area that was the only possible place of survival when the bodies of his crew were found on an exposed ridge across the valley. Eds suspicions escalate and Cliff reacts even more bitterly. One night, an emergency crew is called out to repair downed transmission lines, and when Cliffs longtime friend Boise Peterson is shocked by a live wire, Cliff saves him. Ed pointedly remarks that it was not necessary for Cliff to prove his bravery. Cliff is cleared by the board of review but confides to Peg that he is plagued by doubts about his courage. Later, Dick shows Ed a watch, mistakenly sent to another mans family, that Ed recognizes as his fathers. Upset again, Ed confronts Cliff with the watch, and jogs his memory.
 snag fell on the rockslide and the crew continued running. Cliff attempted to stop Pop, pulling off his watch and ID tag as they grappled, but Pop knocked him into a crevice that protected Cliff from the worst of the fire. Ed furiously accuses Cliff of deserting his men and goes AWOL, parachuting from a private airplane onto Bugle Peak, where he finds Pops identification bracelet on the ridge, not on the rockslide, where Cliff says he saw Pop last. Believing he has obtained proof that Cliff abandoned his men on the ridge, Ed returns to base, only to discover that Cliff and another team of men have been sent to fight a fire in Carson Canyon. Confronting Dick with the ID tag, Ed accuses Cliff of killing his father, and Dick fires him from the smokejumper unit for going AWOL on a personal grudge. In Carson Canyon, Cliffs crew brings the fire under control but weather conditions threaten a re-burn, prompting Cliff to request more men and equipment. 
 anchor point heavier equipment to bring in Ed. Cliff orders the others to dig foxholes, knowing that burying themselves and allowing the fire to pass over them is their only hope for survival. The men protest but grudgingly comply when Cliff insists. Ed is surprised to discover that Cliff is responsible for his rescue, and when he is brought back to the anchor point, the crew panics and starts to flee. Ed sees Cliff knock down Boise to quell the panic and realizes Cliff was telling the truth about Bugle Peak. After the fire has passed, all of the smokejumpers have survived and Ed, reconciling with Cliff, sheepishly grins and asks for a cigarette, inspiring Boise to do the same.  When Dick realizes the entire crew has survived, he reinforces Cliffs men from the air as an even larger ground force with bulldozers swings into action.

==Cast==
* Richard Widmark as Cliff Mason
* Constance Smith as Peg Mason
* Jeffrey Hunter as Edward J. (Ed) Miller
* Richard Boone as Richard Dick Dryer
* Warren Stevens as Steve Burgess
* James Griffith as Boise Peterson
* Joe Sawyer as R.A. (Pop) Miller

==Production== John Lund as the stars. Due to injuries suffered by Mature and Lund, and the likelihood of bad weather, the project was "put off till spring." The production was soon abandoned, however, and in April 1951, it was reported that the screenplay was going to be completely rewritten, and that Glenn Ford was going to be the pictures star.

*According to a news item in the July 6, 1951 edition of the Hollywood Reporter, footage of a forest fire in the Gila National Forest, New Mexico was filmed by a special camera crew for use in the film.

* A one-hour television adaptation, entitled Smoke Jumpers, for the The 20th Century Fox Hour was broadcast in November 1956. The telefilm was directed by Albert S. Rogell and starred Dan Duryea, Dean Jagger and Joan Leslie.
 Ford Trimotor 5-AT-C actually used by the United States Forest Service in its operations. {{cite book| first =  | last = National Smokejumpers Association| chapter =| title = Smokerjumpers | editor = 
| year = 2003| publisher = Turner Publishing Company | place = Paducah, Kentucky| isbn = 1-56311-854-8| olcl = | url = | pages =}}, p. 80  The aircraft served with the USFS from June 5, 1951 to August 4, 1959, when it crashed and burned while landing at the Moose Creek spike camp airstrip in the Selway-Bitterroot Wilderness, Idaho, 50 miles west of Missoula, killing two smokejumpers and a Nez Perce National Forest supervisor.     

==See also==
* List of firefighting films

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 