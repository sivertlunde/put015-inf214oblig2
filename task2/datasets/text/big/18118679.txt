Torch Song Trilogy (film)
{{Infobox film
| name           = Torch Song Trilogy
| image          = Torchsongtrilogyposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Paul Bogart
| producer       = Ronald K. Fierstein
| writer         = Harvey Fierstein
| starring       = Harvey Fierstein Anne Bancroft Matthew Broderick Brian Kerwin
| music          = Peter Matz Allan K. Rosen
| cinematography = Mikael Salomon
| editing        = Nicholas C. Smith 
| distributor    = New Line Cinema
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $4,865,997   
}}
 adapted by play of the same title.     

The film was directed by Paul Bogart and stars Fierstein as Arnold, Anne Bancroft as Ma Beckoff, Matthew Broderick as Alan, Brian Kerwin as Ed, and Eddie Castrodad as David. Executive Producer Ronald K. Fierstein is Harvey Fiersteins brother.
 Charles Pierce, Fierstein created the role of Bertha Venation specifically for him. Broderick originally refused the role of Alan because he was recuperating from an automobile accident in Ireland. Tate Donovan was cast, but two days into the rehearsal period Broderick had a change of heart and contacted Fierstein, who fired Donovan.

Although the play was over four hours, the film was restricted to a running time of two hours at the insistence of New Line Cinema, necessitating much editing and excisions. The time frame was regressed to begin several years earlier than when the play was set.

==Plot== bisexual schoolteacher, and they fall in love. Ed, however, is uncomfortable with his sexuality and he leaves Arnold for a girlfriend, Laurel. homophobic attack. adoption of a gay teenage son, David (Eddie Castrodad), as well as Arnolds use of their family burial plot for Alan. They have a series of arguments where Arnold demands that she accept him for who he is, insisting that if she cant then she has no place in his life. The following morning, before she returns to Florida, they have a conversation where, for the first time, they seem to understand each other. With both David and Ed (who is now more mature and settled) in his life, and a successful new career creating his own stage revue, Arnolds life is finally complete.

==Cast==
* Harvey Fierstein as Arnold Beckoff
* Anne Bancroft as Ma Beckoff
* Matthew Broderick as Alan Simon
* Brian Kerwin as Ed Reese Karen Young as Laurel
* Eddie Castrodad as David
* Ken Page as Murray Charles Pierce as Bertha Venation
* Axel Vera as Marina Del Ray

==Soundtrack==
The soundtrack for Torch Song Trilogy was released on the Polydor label on LP, cassette, and CD on December 8, 1988. The album charted on the jazz charts of industry magazines Billboard (magazine)|Billboard and Cashbox (magazine)|Cashbox.

The song "This Time the Dreams On Me" sung by Ella Fitzgerald, which is used several times throughout the film including over the closing credits, was excised from the planned soundtrack album by Norman Granz, Fitzgeralds long-time manager, when he invoked a contractual clause which gave Fitzgerald the right to refuse her material to appear on an album featuring another artist (known in the music industry as a "coupling clause").  In actuality, Granz was unhappy with the money offered by the record company, PolyGram Records (now part of Universal Music), for the use of the song in the film and refused permission for its inclusion on the album out of spite.

Original music by Peter Matz and contemporary pop tunes such as Rod Stewarts "Maggie May" were used in the film, but not contained on the soundtrack as its producers, Larry L. Lash and Matz, felt they broke the overall "torch song" theme of the album. The tracklisting is as follows:
 Joe Williams Charles Pierce, Axel Vera
# "But Not for Me" - Billie Holiday
# "Body and Soul" - Charlie Haden Quartet West
# "Svelte" - Harvey Fierstein
# "Skylark" - Marilyn Scott
# "I Loves You, Porgy" - Bill Evans
# "Cant We Be Friends?" - Anita ODay
# "Love for Sale" - Harvey Fierstein
# "Whats New?" - Billie Holiday

==Home media==

Torch Song Trilogy was released on VHS in 1989, and on DVD in May 2004. The DVD version contains an audio commentary track by actor and writer Harvey Fierstein.

==Reception== Time Out, Roger Ebert and Janet Maslin all praising the film. It holds a 71% score on Rotten Tomatoes based on 17 reviews. 

Janet Maslin from The New York Times wrote “Like La Cage aux Folles, Torch Song Trilogy presents a homosexual world that any mother, with the possible exception of Arnold Beckoff’s, would love. Greatly shortened from Mr. Fierstein’s long-running, Tony Award-winning play, the film version emphasizes the lovable at every turn, but the surprise is that it does this entertainingly and well.”    Roger Ebert commented “As written and performed by Harvey Fierstein as a long-running stage hit, it was seen as a sort of nostalgic visit to the problems that gays had in the years before the horror of AIDS. The movie has more or less the same focus, but because it’s a movie, it becomes more intimate and intense.” 

==Awards and nominations==
At the 1989 Deauville Film Festival, director Paul Bogart was nominated for the Critics Award and won the Audience Award. The film was also nominated for Best Feature and Fierstein was nominated for Best Male Lead at the Independent Spirit Awards that same year.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 