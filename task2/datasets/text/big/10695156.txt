Words and Music (1929 film)
{{Infobox film
| name           = Words and Music
| director       = James Tinling
| writers        = Frederick Hazlitt Brennan (story) Jack McEdwards (story) Andrew Bennison
| starring       = Lois Moran David Percy Helen Twelvetrees Frank Albertson
| cinematography = Don Anderson Charles G. Clarke Charles Van Enger
| editing        = Ralph Dixon
| music          = Con Conrad Archie Gottler Sidney D. Mitchell Dave Stamper
| sound          = Joseph E. Aiken Donald Flick Fox Film Corporation
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
}}
 American musical musical comedy film, directed by James Tinling, and starring Lois Moran, David Percy, Helen Twelvetrees, and Frank Albertson. It was written by Andrew Bennison with story by Frederick Hazlitt Brennan and Jack Edwards.
 Fox Film Corporation, the film is notable as the first in which John Wayne is credited as "Duke Morrison." Wayne was also credited as "Duke Morrison" as a property assistant in the Art Department. Ward Bond, Wayne’s lifelong good friend, also had a bit part in the movie.

==Plot==
Two young college students, Phil (David Percy) and Pete (John Wayne), compete for the love of pretty girl named Mary (Lois Moran), and also to win the $1500 prize in a song-writing contest to write the best show tune for the annual college revue. The two men each ask Mary to sing for them, but eventually, she chooses Phil as her beau, and it is he who also has the winning song.

Although the film was largely devoid of much plot line, as was typical of musical review pictures of the period, there is a great deal of singing and dancing. Many of Lois Moran’s numbers were actually footage that was cut from the film Fox Movietone Follies of 1929, which were edited out when the film was found to be too long. This film was created to make use of the deleted scenes, and so was fashioned around Lois Moran’s singing talent.
 Paul Gerard Smith, Edmund Joseph), "Stepping Along" (Kernell), "Shadows" (Con Conrad, Sidney D. Mitchell, Archie Gottler).

==Cast==
*Lois Moran as Mary Brown
*David Percy as Song and dance principal
*Helen Twelvetrees as Dorothy Blake
*William Orlamond as Pop Evans Elizabeth Patterson as Dean Crockett
*John Wayne as Pete Donahue (as Duke Morrison)
*Frank Albertson as Skeet Mulroy
*Tom Patricola as Hannibal
*Eddie Bush as Biltmore Quartet member (as Biltmore Quartet)
*Paul Gibbons as Baltimore Quartet member (as Biltmore Quartet)
*Bill Seckler as Biltmore Quartet member (as Biltmore Quartet)
*Ches Kirkpatrick as Biltmore Quartet member (as Biltmore Quartet)
*Bubbles Crowell as Bubbles

==See also==
* John Wayne filmography

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 