Yves Saint Laurent (film)
 
 
{{Infobox film
| name           = Yves Saint Laurent
| image          = Yves Saint Laurent film.jpg
| caption        = Theatrical release poster
| director       = Jalil Lespert
| producer       = Yannick Bolloré Wassim Béji
| writer         = Jacques Fieschi Jalil Lespert Jérémie Guez Marie-Pierre Huster
| based on       =  
| starring       = Pierre Niney Guillaume Gallienne Charlotte Le Bon Laura Smet Marie de Villepin Xavier Lafitte Nikolai Kinski
| music          = Ibrahim Maalouf 
| cinematography = Thomas Hardmeier
| editing        = François Gédigier
| studio         = Canal+ SND Films Wy Productions
| distributor    = SND Films
| released       =   
| runtime        = 105 minutes  
| country        = France
| language       = French English Russian Arabic Japanese
| budget         = $14,7 million
| gross          = $31,523,083 
}} biographical drama Yves Saint Best Actor for Pierre Niney.     

==Plot==
Yves Saint Laurent and Pierre Bergé promote the French fashion industry and stay friends against all odds.

==Cast==
  Yves Saint Laurent
* Guillaume Gallienne as Pierre Bergé
* Charlotte Le Bon as Victoire Doutreleau
* Laura Smet as Loulou de la Falaise
* Marie de Villepin as Betty Catroux
* Xavier Lafitte as Jacques De Bascher
* Nikolai Kinski as Karl Lagerfeld
* Ruben Alves as  Fernando Sánchez
* Marianne Basler as Lucienne Saint-Laurent
* Astrid Whettnall as Yvonne De Peyerimhoff
* Anne Alvaro as Marie-Louise Bousquet
* Michèle Garcia as Raymonde Zehnacker
* Patrice Thibaud as Christian Dior
* Amira Casar as Anne-Marie Munoz
* Alexandre Steiger as Jean-Pierre Debord
* Jean-Édouard Bodziak as Bernard Buffet
 

==Production==
In March 2013, The Weinstein Company acquired the rights to the film to distribute in the United States, while Entertainment One holds U.K., Australian and Benelux distribution rights, including Canadian distribution rights. 

===Filming===
  and lead actor Pierre Niney at the films premiere in Paris.]]
Principal photography began in June 2013. Part of the filming was done with Bergé, who sent "out models on the runway for a reconstitution of Saint Laurent’s famous Opéra Ballets Russes collection of 1976, which was filmed at the fashions show’s original venue, the Westin hotel (formerly known as the InterContinental.)"    Bergé’s foundation loaned the film "77 vintage outfits from its archives and allowed Lespert to film certain scenes at its headquarters on Avenue Marceau in Paris."    Bergé "praised Lespert’s film — based largely on a Laurence Benaïm biography of Saint Laurent and Bergé’s reminiscences in his book “Letters to Yves” — for showing the designer’s demons." Bergé said "...there are details I don’t like, but that is of no importance whatsoever. You have to take the movie as it is — as a whole."   

==Reception==
===Critical response===
Yves Saint Laurent received mixed reviews. On film aggregation website Rotten Tomatoes, it holds a rating of 43%, with an average score of 5.2/10, based on reviews from 60 critics. The sites consensus reads "While it boasts its share of fine performances, Yves Saint Laurent is also disappointingly bland and formulaic — especially given its subjects dazzling reputation."  On another website, Metacritic, it has a score of 51/100 (indicating "mixed or average"), based on reviews from 25 critics. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 