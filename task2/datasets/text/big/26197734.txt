Meteor Apocalypse
{{Infobox film
| name           = Meteor Apocalypse
| image          = Meteor Apocalypse.jpg
| writer         =  
| director       = Micho Rutare
| producer       =  
| cinematography = Adam Silver
| music          = Douglas Edward
| editing        = Danny Maldonado
| studio         = The Asylum
| starring       =  
| released       =   {{cite news
 | title=Damon, DeNiro lead latest releases to video stores
 | date=March 3, 2010 | work=Winston-Salem Journal
 | url=http://www2.journalnow.com/content/2010/feb/25/damon-deniro-lead-latest-releases-to-video-stores/
 | author=Staff | accessdate=2010-03-03 }} 
| runtime        = 90 minutes
| country        = United States
| language       = English
}}

Meteor Apocalypse is a 2009 American science fiction film {{cite news
 | first=Jean | last=Merl | date=December 19, 2009
 | title=L.A.-area movie ranch hits upon a sequel after fire
 | work=The Los Angeles Times
 | url=http://www.latimes.com/news/local/la-me-singing-springs19-2009dec19,0,7220619.story | accessdate=2010-01-11 }}  directed by Micho Rutare.

==Plot==
A long-period comets orbit is determined to be crossing directly in the path of Earths orbit. All of the worlds nuclear states fire their missiles at the comet, but pieces of the comet continues to strike the Earth, contaminating the groundwater and causing millions to become sick.

The story follows David Dmatti (Joe Lando) as he searches for his wife Kate (Claudia Christian) and sick daughter Allison (Madison McLaughlin). They were initially quarantined in the Las Vegas Valley. David, finds and revives Lynn (Cooper Harris) at a gas station and brings her with him to Las Vegas.

When they arrive, they learn most of the city was destroyed and the quarantined were transferred to Los Angeles. David is also able to get an antidote for the mysterious illness. When it is discovered that the largest comet fragment will most likely hit Los Angeles, a panicked evacuation is begun. The problem is the quarantined are left behind. While trying to rescue Davids family, Lynn dies, leaving enough antidote for Davids daughter. He and his family are soon reunited, just as the final fragment strikes the city.

==Cast==
* Gregory Paul Smith - Damien
* Joe Lando - David Dematti
* Claudia Christian - Kate Dematti
* Cooper Harris - Lynn Leigh
* Brendan Andolsek Bradley - Curtis Langley
* Sueann Han - Candace Mills
* Madison McLaughlin - Alison DeMatti
* Peter Husmann - Jack Nielson
* Jennifer Smart - Madeline
* David Dustin Kenyon - Dustin Landau-marauder

==Production==
The film is produced and is distributed by The Asylum Films.

==Release==
The release date for the film in North America was February 23, 2010

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 