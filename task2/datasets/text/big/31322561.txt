Alarm in the Circus
{{Infobox film
| name           = Alarm in the Circus
| image          = 
| image size     =
| caption        =
| director       = Gerhard Klein
| producer       =Paul Ramacher
| writer         = Wolfgang Kohlhaase Hans Kubisch 
| starring       = Erwin Geschonneck
| music          = Günter Klück
| cinematography = Werner Bergmann
| editing        =Ursula Kahlbaum
| studio = DEFA
| distributor    = PROGRESS-Film Verleih
| released       =  
| runtime        = 75 minutes
| country        = East Germany
| language       = German
| budget         =
| gross          =
}}
 East German crime film directed by Gerhard Klein. It was released in List of East German films|1954. 

==Plot==
Klaus and Max are two poor boys from West Berlin, whose families are to poor to pay for their higher education. They face a bleak future. Their only hobby is boxing, and they are both desperate to purchase real boxing gloves. The two meet Klott, a gangster who owns a bar that serves American soldiers. Klott offers to pay them if they would assist him to steal valuable horses from a circus in East Berlin. The two agree and travel to the Soviet zone, where they meet a girl named Helli, a member of the Free German Youth, who explains to them that in the communist east, the lack of money will not bar their way to education. The two realize the error of their ways, contact the Volkspolizei|Peoples Police and help the officers hinder Klotts plans and arrest the other thieves working for him. The two remain in East Berlin.  

==Cast==
* Erwin Geschonneck as Klott
* Uwe-Jens Pape as Jimmy
* Karl Kendzia as Batta
* Ulrich Thein as Herbert
* Hans Winter as Klaus
* Ernst-Georg Schwill as Max
* Gertrud Keller as Helli
* Annelise Matschulat as Mrs. Weigel Siegfried Weiß as Hepfield
* Peter Dornseif as Police officer
* Günther Haack as Catcher
* Horst Giese as uncredited role

==Production== neorealism in Berlin - Schönhauser Corner (1957). 

==Reception== National Prize, 3rd degree, for their work on the film. 

The Catholic Film Service defined the film as "exciting, well-made crime film that presents the background of a divided Berlin in a highly authentic manner."  Peter C. Rollins and John E. OConnor wrote that it had "drawn a clear contrast between the citys halves that fit the official communist paradigm." 

==References==
  

==External links==
* 
*  on ostfilm.de.
*  on filmportal.de. 
*  film-zeit.de
*  on cinema.de

 
 
 
 
 
 