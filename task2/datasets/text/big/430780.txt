The Mosquito Coast
 
 
{{Infobox film
| name           = The Mosquito Coast
| image          = Mosquito Coast movie poster.jpg
| caption        = Theatrical release poster
| director       = Peter Weir
| producer       = Jerome Hellman
| screenplay     = Paul Schrader
| based on       =  
| starring       = Harrison Ford Helen Mirren River Phoenix
| music          = Maurice Jarre
| cinematography = John Seale
| editing        = Thom Noble
| studio         = 
| distributor    = Warner Bros.
| released       =  
| runtime        = 117 min.
| country        = United States
| language       = English
| budget         = $25 million (Estimated)
| gross          = $14,302,779 (Domestic)
}} novel of Cartersville and Rome in Georgia (U.S. state)|Georgia, in addition to Baltimore, Maryland, and Belize.

==Plot==
The film opens with Charlie Fox (River Phoenix) explaining that his father, Allie Fox (Harrison Ford), is a brilliant inventor with "nine patents, six pending." Allie has grown fed up with the American Dream and American consumerism, believing that Americans "buy junk, sell junk and eat junk," and that there is an impending nuclear war on the horizon as a result of American greed and crime.

Allie and Charlie go to a hardware store to buy components for a new invention, an ice machine known as Fat Boy. Upon seeing that the product was made in Japan, Allie refuses to purchase it. After Allie and Charlie acquire the components at a local dump, he finishes assembling his creation. Allies boss and asparagus farm owner, Mr. Polski (Dick ONeill) complains that Allie is not tending to the asparagus, which is rotting. Allie, Charlie, and Allies youngest son, Jerry (Jadrien Steele), meet Mr. Polski, and Allie shows him "Fat Boy." The machine leaves Polski unimpressed. As he drives past the fields, a dejected Allie comments on immigrants picking asparagus, and says that where they come from, they might think of ice as a luxury. The home of the migrant workers is in a state of disarray, exemplifying their poverty.

That night, Jerry tells "Mother" (Helen Mirren), that he believes something terrible is about to happen. Mother rebuffs her son, explaining that she believes something good will happen. The next morning, Allie throws a party for the immigrant workers before telling his family that theyre leaving the United States. After they board a Panamanian barge, the family meets Reverend Spellgood (Andre Gregory), a Christian missionary, his wife (Melanie Boland), and their daughter, Emily (Martha Plimpton). Emily flirts with Charlie. Allie and the Reverend begrudgingly try to get along, despite having entirely different religious views. When the barge docks in Belize City, the families disembark and go their separate ways. Allie, with the consent of the Belize government, purchases a small village called Jeronimo in the rainforest along the river.

Mr. Haddy ( . Allie sets to constructing a huge version of "Fat Boy" that can supply Jeronimo with ice. Upon completing the machine, Allie hears rumors of a native tribe in the mountains that have never seen ice. Allie recruits his two sons to carry a load of ice into the jungle to supply the tribe. Upon arriving, Allie finds that the load has melted, and that the tribe has already been visited by missionaries. 

When Allie returns to Jeronimo, he learns that Spellgood has left with much of the populace, scaring them with stories of Gods biblical destruction. The near-empty town is visited by rebels, who demand to use Jeronimo as a guerrilla base. Allie and his family accept to accommodate them while Allie constructs a plan to be rid of them. Set on freezing them to death, Allie bunks the rebels up in the giant ice machine, tells Charlie to lock its only other exit, and activates it. The rebels, waking in panic, try to shoot their way through. To Allies horror, the rebels gunfire sets off an explosion within the machine. By the next morning, both the machine and the familys home is in ruins. Worse, the chemicals from the destroyed machine have severely polluted the river.

Forced downstream, Allie and his family arrive at the coast. Mother and the children rejoice, believing they can return to the United States. Allie, refusing to believe his dream has been shattered, announces that they have all they need on the beach and, lying, tells the family that America has been destroyed in a nuclear war. Settling on the beach in a houseboat he has built, and refusing assistance from Mr. Haddy, a paranoid Allie believes that the family has accomplished building a utopia. One night, the storm surge from a tropical cyclone nearly forces the family out to sea until Charlie reveals that he has been hiding motor components given to him by Mr. Haddy, allowing them to start the motor on the boat. The family becomes physically and emotionally weaker for lack of food, shelter, and other human companionship. 

Traveling upstream once again, the family stumbles across Spellgoods compound. Coming ashore, Allie sees barbed wire, and mutters that the settlement is a Christian concentration camp. While the rest of the family sleeps, Charlie and Jerry sneak over to the Spellgood home. After finding out that the United States was not destroyed and that Emily will assist them in escaping from Allie, Charlie obtains the keys to a jeep. Before Charlie can convince Mother and his sisters to leave, Allie sets Spellgoods church on fire. Spellgood shoots Allie, paralyzing him from the neck down. The family escapes aboard the boat.

The film concludes with the group traveling downriver again, where Allie drifts in and out of consciousness. Allie asks his wife if they are going upstream. She lies to him—going against the wishes of her husband for the first time. Charlies final narration reports the death of Allie, but gives hope that the rest of the family can live their lives freely from now on.

==Cast==
* Harrison Ford as Allie Fox
* Helen Mirren as "Mother" Fox
* River Phoenix as Charlie Fox
* Conrad Roberts as Mr. Haddy
* Andre Gregory as Reverend Spellgood
* Martha Plimpton as Emily Spellgood
* Melanie Boland as Mrs. Spellgood
* Dick ONeill as Mr. Polski
* Jadrien Steele as Jerry Fox
* Hilary Gordon as April Fox
* Rebecca Gordon as Clover Fox
* Alice Heffernan-Sneeds as Mrs. Polski
* Jason Alexander as Hardware Clerk William Newman as Captain Smalls
* Aurora Clavel as Mrs. Maywit
* Butterfly McQueen as Ma Kennywick

==Production==
Producer  , a role Ford had also been offered. 

As the film went into pre-production, and Weir was in Central America scouting for locations, the financial backing for the film fell through and the project was suspended indefinitely. Pfeiffer, Lee. The Films of Harrison Ford. Citadel Press, 2002.  In the meantime, Weir was approached to direct Witness (1985 film)|Witness starring Harrison Ford. The film, which was Weirs first American production, was a critical and commercial success, garnering eight Academy Award nominations including Weir for Best Director, Ford for Best Actor, and the film itself for Best Film. During the production of Witness, Weir discussed The Mosquito Coast with Ford who became interested in the role of Allie Fox (though Fords agent was less enthusiastic). With Ford attached to the project, financial backing and distribution for the film was easier to find (ultimately from Saul Zaentz and Warner Bros.). 

Filming began the week of February 7, 1986 in Belize and finished there on April 26 before moving to Georgia. Weir and Ford famously missed the Academy Awards ceremony for which they had both been nominated for Witness,  which won two Oscars, for Best Screenplay and Best Film Editing). Some post-production editing was done in Australia.
 Gone with the Wind. She plays a lapsed churchgoer, and in real life was a vocal atheist.  The film also features a brief appearance by Jason Alexander. Running on Empty. It was also his performance as the son to Harrison Fords character that led Ford to recommend Phoenix to Steven Spielberg for the part of teenage Indiana Jones in Indiana Jones and the Last Crusade.

==Reception==
===Critical response===
The film was initially a critical and commercial disappointment, but has since received much stronger modern reviews. Film review aggregator Rotten Tomatoes gives the film a positive score of 75% based on review from 20 sampled, with an average rating of 6.4 out of 10.  Siskel & Ebert were split, Siskel giving the film a "thumbs up" and Ebert giving it a "thumbs down,"  criticizing it as "boring."  Vincent Canby of The New York Times called it "utterly flat."  In her review for the Washington Post, Rita Kempley wrote, "Sooner or later a man of invention will pollute paradise, a grand contradiction that gives Mosquito its bite and Ford inspiration for his most complex portrayal to date. As a persona of epic polarities, he animates this muddled, metaphysical journey into the jungle."  In her review for the Los Angeles Times, Sheila Benson wrote, "Hes orchestrated The Mosquito Coasts action to match Foxs progressive mental state, from rage to explosion to squalls and finally to hurricane velocity; however, the film leaves us not with an apotheosis, but exhaustion."  In his review for the Globe and Mail, Jay Scott wrote, "The Mosquito Coast is a work of consummate craftsmanship and its spectacularly acted, down to the smallest roles ... but its field of vision is as narrow and eventually as claustrophobic as Allies."  The negative reviews the film received prompted Harrison Ford to defend the film in the media: "there have been mixed reviews and I think the film has been very unfairly treated in some quarters. I have never seen a serious film treated so badly by the critics. And I think theyre wrong. I dont mind saying Im here trying to counter those negative reviews ... Im not defensive about the picture, but I want the public to hear another point of view. Critics see a film and then rush to review it. This is the sort of movie that really doesnt sink home for about three days. It is disturbing and makes you think. It stays with you." 

Out of a budget of $25 million, the film made only a little over $14 million in North America. 

Despite being one of his least commercially successful films, Ford has defended it, saying in a 1992 interview: 
 

==References==
 

==External links==
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 