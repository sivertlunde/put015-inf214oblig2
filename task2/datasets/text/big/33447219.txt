Dana Paani
{{Infobox film
 | name = Dana Paani
 | image = Dana Paanifilm.jpg
 | caption = VCD Cover
 | director = Deven Verma
 | producer = Deven Verma
 | writer = 
 | dialogue = 
 | starring = Ashok Kumar Mithun Chakraborty Padmini Kolhapure Nirupa Roy Kader Khan Prem Chopra Sadashiv Amrapurkar Aruna Irani Shafi Inamdar Asha Sachdev Deven Verma
 | music = Anu Malik
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =   1989 (India)
 | runtime = 135 min.
 | language = Hindi Rs 5 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1989 Hindi Indian feature directed by Deven Verma, starring Ashok Kumar, Mithun Chakraborty, Padmini Kolhapure, Nirupa Roy, Kader Khan, Prem Chopra, Sadashiv Amrapurkar, Aruna Irani, Shafi Inamdar, Asha Sachdev and Deven Verma

==Plot==

Dana Paani is an action film with  Mithun Chakraborty and Padmini Kolhapure playing the lead roles, supported by Ashok Kumar, Nirupa Roy, Prem Chopra and Sadashiv Amrapurkar.

==Cast==

*Mithun Chakraborty...... Satya Prakash Tripathi
*Ashok Kumar
*Padmini Kolhapure
*Nirupa Roy...... Satya Prakashs Mother
*Aruna Irani
*Prem Chopra
*Sadashiv Amrapurkar...... Kutti Seth
*Shafi Inamdaar...... Gautam
*Sharat Saxena...... Street Beggar
*Asha Sachdev
*Deven Verma
*Madhu Malhotra...... Aasha
*Manek Irani...... 
*Sudhir Dalvi...... The Judge
*Rajesh Puri...... Pyare Bhai
*Shreeram Lagoo......
*Mohan Choti
*Nawab
*Ghanshyam...... 
*Baby Gazala...... 

==References==
* http://www.bollywoodhungama.com/movies/cast/5263/index.html
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Dana+Paani -

==External links==
*  

 
 
 
 

 