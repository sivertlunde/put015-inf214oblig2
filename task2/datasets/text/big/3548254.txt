Snowboard Academy
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Snowboard Academy
| image          = Snowboard_Academy.jpg
| writer         = Rudy Rupak James Salisko
| starring       = {{plain list|
* Corey Haim
* Jim Varney
* Brigitte Nielsen
* Joe Flaherty 
}}
| director       = John Shepphird 
| producer       = Rudy Rupak
| distributor    = Columbia TriStar
| released       = October 28, 1997
| runtime        = 88 min
| music          = Ross Vannelli
| awards         = 
| language       = English
| budget         =
}}

Snowboard Academy is a 1997 independent slapstick comedy film, starring Corey Haim, Jim Varney and Brigitte Nielsen.

==Plot summary==
A competition is held between two brothers on their fathers ski slopes one is a skier the other a snowboarder. The competition would determine if snowboarders could be allowed to be a part the ski patrol. Into the scene arrives Rudy James, played by Jim Varney  who stumbles his way into a job as ski patrol, entertainment host, and jack of all trades.  What was cut out of the film was that Rudy James was hired by Mimi played by Brigitte Nielsen  to ineptly sabotage the ski hill so that she could win it in a divorce proceeding.  The two brothers discover her plot to damage their fathers ski hill (played by SCTVs Joe Flaherty https://en.wikipedia.org/wiki/Joe_Flaherty) and further hilarity ensues.

== Locations ==
Snowboard Academy was shot on location in Chantecler Ski Resorts, which is part of the Laurentides in the city of Sainte-Adele, Quebec. Shooting lasted 20 days.  Additional stunt footage was shot in Squaw Valley Ski Resort,  California. Stunts were shot and coordinated by Michael Roban 

== Other ==
Rudy Rupak was 26 years old at the time he produced this film, making him Canadas youngest film producer at the time.  The project was meant to be both a video game and movie. Rudy credits producer Claudia Castravelli for rescuing the film on the first day of pre-production when the initial bank rejected Columbia Tri-Stars contract because at that point the company was changing ownership from a US company to becoming a part of Sony.  Claudia showed up with a check from another source.  The script, written by Rudy Rupak and James Salisko, was considered too edgy and made reference to things like X games and extreme sports. Columbia Pictures however toned down what would have been a PG-13 movie into a PG one given the pressures of an election year when Bob Dole was bent on chastising Hollywood for its alleged immorality. Scenes like peeing onto skiers from a chair lift, and skating on a blunt shape board were scrapped. Rupaks efforts to sell a videogame based on Snowboarding never materialized.  Rudy Rupak went on to produce two other feature films including Shes Too Tall and The Final Goal which was directed by Paul Kassar, the director of 24.

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 