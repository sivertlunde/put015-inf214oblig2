Manmadan Ambu
 
 
{{Infobox film
| name           = Manmadan Ambu
| image          = Manmadhan ambu.jpg
| caption        = Theatrical poster
| director       = K. S. Ravikumar
| producer       = Udhayanidhi Stalin
| writer         = Kamal Haasan
| starring       = Kamal Haasan R. Madhavan Trisha Krishnan Ramesh Aravind Sangeetha
| music          = Devi Sri Prasad
| cinematography = Manush Nandan
| editing        = Shan Mohammed
| released       =    Red Giant Movies
| distributor    = Red Giant Movies (worldwide) Sree Gokulam Films (Kerala)  Gemini Film Circuit (Andhra Pradesh)
| country        = India
| runtime        = 152 minutes
| language       = Tamil
| budget         =    
| gross          = 
}}
 Tamil romantic Urvashi among others in supporting roles. The film features music composed by Devi Sri Prasad, with several songs written and sung by Kamal Haasan himself, while Manush Nandan and Shan Mohammed made their debut as cinematographer and editor.
 Telugu as Manmadha Banam and distributed by Gemini Film Circuit. Upon release, the film opened to mixed reviews and had an average run at the box office.

==Plot==
Ambujakshi alias "Ambu" (  in a bright park and Madan was suspicious of her relationship with the actor. While returning, he lets Ambu drive his new car. Madan advised Ambu to stop acting, but Ambu claimed it as her profession which she cant give up, leading to an argument that caused their car to crash near a rock. At the same time they blamed a small white car that just passed by, to be the reason for the crash. Unable to bear any more arguments, Ambu broke her relationship with Madan and walked away.

Madan now suspects that Ambu may be having a relationship with her colleagues in the film industry. To end that, he hires detective Major Raja Mannar (Kamal Haasan) to follow her when she goes on a cruise for vacation in Barcelona. Mannar accepts, as he needs money to pay the hospital bills of his friend Rajan (Ramesh Arvind), who is diagnosed with cancer, and his wife Mallika (Urvashi (actress)|Urvashi)takes care of him . Contrary to Madans suspicions, Ambu is loyal and virtuous; when Mannar reports this, Madan refuses to pay him as his suspicions were unfounded.

Disappointed, Mannar to save his dying friend Rajan fabricates a story and tells Madan that she is having a secret affair during her trip. In the process, he introduces himself as a manager of a security company to Ambu, Deepa and Deepas children, and becomes close to the group. While getting closer to Ambu, Mannar says to Madan that Ambu is good but the other guy is bad, but Madan does not want to hear that and eventually breaks up with Ambu. In the background, Madans mother (Usha Uthup)who never liked Ambu calls her brother to inform that Madan has broken up with Ambu and that they should get his daughter (Oviya) married to Madan as agreed in the past. While recollecting his past as an Army officer to Ambu, Mannar reveals that he lost his wife three years ago in a car accident. Ambujakshi realises to her horror that the accident was caused by her during the argument with Madan. Both of them decide to confront each other with the truth, but Ambu misunderstands Kurup (Kunchan) as Madhans detective and slaps him and tells that she loves Mannar. Meanwhile Rajan should undergo an operation immediately after the chemotherapy to keep him alive. To make things worse Madan announces that he will visit them in person at Venice. Ultimately, Mannar and Deepa (who now knows that Mannar was the actual spy that Madhan sent) stage a plan with the help of Kurup to deceive Madan for the final break-up with Ambu.

Madan arrives at the place and various mix-ups and misunderstandings take place among the characters. Finally, Madan realises that Ambu has fallen in love with Mannar and accepts it with a heavy heart. At the same time, Rajan recovers from cancer. The film ends as everyone returns to India on the cruise, with Madan and Deepa starting a relationship.

==Cast==
* Kamal Haasan as Major. R. Mannar
* Trisha Krishnan as Ambujakshi Madhavan as Madanagopal
* Sangeetha as Deepa
* Ramesh Arvind as Rajan Urvasi as Mallika Kunchan as Kunju Kurup
* Manju Pillai as Manju Kurup
* Biriyaalayam Thurai as Thurai
* Usha Uthup as Indira (Madhans Mother)
* Oviya as Sunanda
* Caroline as Juliet (Mannars deceased wife)
* Aasish Mahesh as Viswanath Srinivasan Sriman as Madanagopals friend Suriya in a guest appearance as himself
* K. S. Ravikumar in a guest appearance as himself
* Devi Sri Prasad in a cameo appearance
* Emma de Courcel in a guest appearance (frightened Persian girl with a dog)

==Production==
After  s were performed.  Haasan, along with Crazy Mohan, wrote the films screenplay and dialogues. 

Shoots were subsequently held aboard on a cruise liner from Dubai, and the film was shot across various regions of Europe including Paris and Marseille in France, Barcelona in Spain and Rome and Venice in Italy. Parts of the film were also shot in Kodaikanal in South India; remaining portions were completed in Chennai.

==Release==
Kalaignar TV bought the satellite rights. 

===Reception=== Kamal stamp all over it."  Malathi Rangarajan of The Hindu stated that, "Sprinkled with humour, joy, love, sadness and sentiment with an undercurrent of jealousy running through it, MMA   is a cocktail of emotions – tasty, but at times queer!"  NDTV resident editor T.S. Sudhir wrote, "Dont go expecting a Panchatantiram from the Kamal-KS Ravikumar combo, for you will be disappointed. MMA has starting trouble and one hour into the film, you are desperately waiting for the comic fireworks to start, given that the film has been marketed as a laugh riot. The riot, when it happens post the interval, leaves you with a feeling of being shortchanged." He further mentioned, "The problem with MMA is as much with Kamal as with the audience for you expect nothing short of brilliance from this Master of all trades. In MMA, Kamal has shot the Cupids arrow (which is what Manmadhan Ambu means) rather lazily. Go without expecting a world record in archery!" 

The Telugu dub version of the film, Manmadha Baanam also opened to mixed reviews from critics. Jeevi from Idlebrain gave the film 3 out of 5 rating and stated "The comedy in this film is predictable at times and fresh in certain scenes. The plus points of the movie are main leads and comedy quotient. On the flipside, the characters and narration becomes confusing towards end and the editing should have been smoother. On a whole, Manmadha Banam is a decent comedy."  Reviewer from 123 Telugu also gave the film 3 out of 5 and said "Manmadha Banam is an interesting movie. It has flaws, but its good parts outweigh them by far."  Deepa Garimella from Full Hyderabad gave the film 7 out of 10 rating and stated that "While the movie is immensely enjoyable, thanks to the well-conceptualized scenes and the stellar performances all round, it is slow. So slow, in fact, that it feels like 2 movies – pre-interval being a drama and post-interval a comedy." and she also praised the performances of the actors.  Y. Sunita Chowdary from Cinegoer said "Manmadha Banam does have a plot but the character development, narration and the pace makes it a very restless and a tedious watch." and was critical about the dubbing. She finally concluded by stating that "The film could have drawn a little wider audience than the filmmakers intended, if the length would have been a half an hour shorter." 

===Box office===
Manmadhan Ambu grossed   in Chennai alone over a period of 4 weeks. In UK, it grossed $77,360 and in Malaysia $653,942.  Sify declared the film as "average". 

==Soundtrack==
{{Infobox album
| Name       = Manmadan Ambu
| Type       = soundtrack
| Artist     = Devi Sri Prasad
| Cover      = Manmathan_Ambu.jpg
| Released   =  
| Recorded   = Panchathan Record Inn and AM Studios
| Genres     = 
| Label      = Venus 
| Producer   = Devi Sri Prasad
| Last album = Singam (2010)
| This album = Manmadan Ambu (2010)
| Next album = Mr. Perfect (film)|Mr. Perfect (2011)
}}
{{Album ratings
| rev1 = Behindwoods
| rev1Score =     
| rev2 = Rediff
| rev2Score =   
}}

The films music was scored by Devi Sri Prasad, after initially Shruti Hassan and later Thaman were reported to be the music composer.  The controversial song Kamal Kavidhai, had come under criticism due to lines about a womans desire and for references to Hindu deities like Aranganathar and Sri Varalakshmi. Therefore, producer Udhayanidhi Stalin opted to remove it from the film. 

{{Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| all_lyrics = Kamal Haasan, except where noted
| title1 = Dhagudu Dhattham
| extra1 = Kamal Hassan
| length1 = 4:57
| title2 = Whos The Hero
| extra2 = Andrea Jeremiah
| length2 = 4:26
| title3 = Neela Vaanam
| extra3 = Kamal Hassan, Priya Himesh
| length3 = 4:27
| title4 = Oyyale
| extra4 = Mukesh, Suchitra
| lyrics4 = Viveka
| length4 = 3:55
| title5 = Kamal Kavidhai
| extra5 = Kamal Hassan, Trisha Krishnan
| length5 = 5:12
| title6 = Manmadan Ambu
| extra6 = Devi Sri Prasad
| length6 = 4:21
| title7 = Theme Music
| extra7 = Instrumental
| length7 = 1:49
}}

===Critical response===
The album received average reviews from music critics. C. Karthik from Behindwoods rated the album 3/5 and quoted "Overall, DSP can be very proud of this album as he has deviated from his normal offerings. A westernised folk song, jazz, melodies, a poem and a kuthu...an album could not ask for more variety and DSP has delivered. Though the music gets a little heavy at times, he has Kamal, with his voice and lyrics, to save him. The new experiments shows his maturity and his intention for being innovative. With the movies release soon, the songs must be playing non-stop on air."  Pavithra Srinivasan of Rediff also gave the album 3/5 saying that "DSP has a reputation for sticking with his regulation format of tunes and here too, you can see it pop up at certain places but theres also a departure from the usual, mostly an influence of Haasan in both lyrics and music. Whatever the reason, the end result is an album that provides you a treat. Go for it."  Indiaglitz said "Overall, this film must have been a different experience for DSP who all along dwelled in fast rhythmic world. The whole album has an up market western jazz feel except for that one song. For a story that happens in Europe, DSP has done it right, we guess." 

==Controversies== Censor Board Kamal himself, The Scientist. The whole song has been depicted in such a way so as to highlight the events that led to the death of Mannars (Kamals character) French wife Juliet, in reverse. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 