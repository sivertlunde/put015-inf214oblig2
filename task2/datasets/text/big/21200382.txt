Dick (film)
{{Infobox film
| name = Dick
| image = Theatricalposterdick.jpg
| image_size = 215px
| caption = theatrical release poster
| director = Andrew Fleming
| producer = Gale Ann Hurd
| writer = Andrew Fleming Sheryl Longin Michelle Williams
| music = John Debney
| cinematography = Alexander Gruszynski
| editing = Mia Goldman
| studio = Phoenix Pictures
| distributor = Columbia Pictures
| released = August 4, 1999 (US)
| runtime = 94 minutes
| country = France Canada United States
| language = English
}}
 presidency of Richard ("Tricky Dick") Nixon and features several cast members from Saturday Night Live and The Kids in the Hall. 
 Michelle Williams Deep Throat" figure partly responsible for bringing down the presidency of Richard Nixon. Dan Hedaya plays Nixon.

==Plot== Michelle Williams) Watergate building. One night, on a quest to mail a letter to enter a contest to win a date with teen idol singer Bobby Sherman, the two girls sneak out of Arlenes home, at the same time as the Watergate break-in. They manage to enter and leave through the parking garage by taping the latch of a door, accidentally causing the break-in to be discovered. They are seen by G. Gordon Liddy (Harry Shearer), who they believe to be committing a jewel robbery; they panic and run away. The security guard, startled by the taped door, calls the police, who immediately arrest the burglars.
 President Nixon himself (Dan Hedaya), who takes Haldeman aside to complain about the bugging operation being so fouled up.
 Vietnam peace Brezhnev accord, by bringing along cookies that they have inadvertently baked marijuana into. (Near the end of the film, when Betsys brother, Larry (Devon Gummersall), reveals the cookies "secret ingredient" and realizes the President ate them, he concludes that this was likely a leading cause of Nixons paranoia.) They also become familiar with the key figures of Nixons administration, including the long-suffering, frequently ignored Henry Kissinger, and inadvertently learn the major secrets of the Watergate scandal without realizing what they know.

Arlene, previously infatuated with Bobby Sherman, now falls equally hard for the president.  Just after reading an 18½ minute gap|18½-minute message of love into his tape recorder, she plays back another part of the tape and, after hearing his coarse, brutal rantings, quickly realizes his true nature. When they confront Nixon, he fires and threatens them.
 Deep Throat film of the same name). Woodward and Bernstein &mdash; portrayed as petty, childish, and incompetent &mdash; are naturally skeptical of the two girls. To make matters worse, their only piece of physical evidence, a list of names of those involved from the Committee to Re-Elect the President, is eaten by Betsys dog.

Nixons men realize that the girls are a real threat and attempt tactics such as  . They give a transcription of it to Woodward and Bernstein (keeping the tape as a "souvenir") thus ending Nixons political career. Nixon, finds Arlenes message on his tape and erases it, reasoning that hed be "crucified" if it was perceived that he had an affair with a 15-year-old girl. After the resignation, as Nixons helicopter flies over Betsys house, the two girl hold up a sign depicting the anachronistic phrase "You suck, Dick", further angering the now ex-president. 

==Cast==
  
* Kirsten Dunst as Betsy Jobs Michelle Williams as Arlene Lorenzo
* Dan Hedaya as President Richard M. Nixon  
* Bruce McCulloch as Carl Bernstein
* Will Ferrell as Bob Woodward
* Saul Rubinek as Henry Kissinger
* Teri Garr as Helen Lorenzo
* Dave Foley as Bob Haldeman
 
* Ryan Reynolds as Chip
* Harry Shearer as G. Gordon Liddy
* Ted McGinley as Roderick
* Karl Pruner as Frank Jobs
* Devon Gummersall as Larry Jobs
* Jim Breuer as John Dean
* G. D. Spradlin as Ben Bradlee
 

==Reception==
The film generally received positive reviews from critics; on Rotten Tomatoes, the film has a 70% "fresh" rating, with the consensus "A clever, funny slice of alternate history, Dick farcically re-imagines the Watergate era and largely succeeds, thanks to quirky, winning performances from Michelle Williams, Kirsten Dunst and Will Ferrell".  Leonard Maltin gave the film three stars, calling it a "clever cross of Clueless (film)|Clueless and All the President’s Men".  

== References ==
Notes
 

==External links==
 
*  
*   at Rotten Tomatoes

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 