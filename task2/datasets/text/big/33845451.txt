The Sensual Man
{{Infobox film
| image          = The Sensual Man.jpg
| caption        =
| director       = Marco Vicario
| producer       =
| writer         =
| starring       = Giancarlo Giannini Rossana Podestà Lionel Stander Ornella Muti Gastone Moschin
| music          = Armando Trovajoli
| cinematography = Tonino Delli Colli
| editing        =Nino Baragli
| distributor    =
| released       =  
| runtime        =
| country        = Italy
| language       =
| budget         =
| gross          =
}}
The Sensual Man ( , also known as The Sensuous Sicilian) is a 1974 Italian comedy film.

It was developed from the novel of the same title, written by Vitaliano Brancati.  It was shot in Catania, Sicily and Rome. 

==Cast==
*Giancarlo Giannini: Paolo Castorini
*Rossana Podestà: Lilia
*Riccardo Cucciolla: Paolos father
*Lionel Stander: Paolos Grandfather, Baron Castorini
*Gastone Moschin: Uncle Edmondo
*Adriana Asti: Beatrice
*Marianne Comtell: Paolos mother
*Vittorio Caprioli: Salvatore, the pharmacist
*Ornella Muti: Giovanna
*Barbara Bach: Anna
*Neda Arneric: Caterina, Paolos wife
*Dori Dorika: Paolos sister
*Pilar Velázquez: Ester
*Femi Benussi: Prostitute in red
*Umberto DOrsi: The Marquis
*Orchidea de Santis: Prostitute with fur coat
*Oreste Lionello: Painter
*Mario Pisu: Lorenzo Banchieri
*Attilio Dottesio: Doctor Mondella
*Eugene Walter: Jacomini

==Plot== Sicilian baron, Paolo Castorini has spent his life (beginning before puberty) dealing with girls, and later, women, usually in matters of the flesh.  But later in life he begins to search for a deeper meaning to life.  When his father is brought to his deathbed, Paolo (after making a pass at the dying mans nurse) is surprised to learn that the apparently staid, upright father had been unfaithful as a young man, as also Paolos grandfather, and that such unfaithfulness had brought consequences both moral and Sexually transmitted disease|medical.

==Release and reception==
The movie, of 1:48 hr running time, was released and circulated in Italy, and also played in US arthouses under the title Paolo il Caldo.  Then in 1977 it was re-released for the English-speaking public under the title The Sensual Man, with English subtitles.  It received a US MPAA film rating of "R". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 
 