South of Wawa
{{Infobox film
| name = South of Wawa
| image =
| producer = Susan Cavan Andras Hamori Barbara Tranter Robert Boyd
| writer = Lori Lansens
| starring = Rebecca Jenkins Catherine Fitch Scott Renderer Dawn Greenhalgh
| music = Jeff Bird Tobias Schliessler
| editing = Bruce Lange
| distributor = Accent Entertainment
| released = September 5, 1991 (Toronto Festival of Festivals)
| runtime = 91 min. English
| budget =
}} Canadian comedy Robert Boyd.

The film stars Rebecca Jenkins as Lizette, a woman stuck in an unhappy marriage who organizes a road trip with her coworker Cheryl Ann (Catherine Fitch) to see Dan Hill in concert.

The films soundtrack includes songs by Cowboy Junkies, Rickie Lee Jones, Lyle Lovett and Lee Aaron.

The film was released on VHS in 1995 in Canada by Cineplex Odeon, but as of April 19, 2010, a DVD of the film has yet to be announced.

==Cast==
*  Catherine Fitch  ...  Cheryl Ann
*  Rebecca Jenkins  ...  Lizette
*  Scott Renderer  ...  Terry Andrew Miller  ...  Simon
*  Samantha Langevin  ...  Helen
*  Stuart Clow  ...  Cam
*  Dawn Greenhalgh  ...  Donna
*  Elias Zarou  ...  Joe
*  Stephanie Forder  ...  Darlene
*  George Touliatos  ...  Serge
*  Michael Gencher  ...  John "The Polack"

==External links==
*  

 
 
 
 
 
 
 
 