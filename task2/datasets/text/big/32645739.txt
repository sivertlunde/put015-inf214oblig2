The Big Picture (2010 film)
{{Infobox film
| name           = The Big Picture
| image          = The Big Picture (2010) UK poster.jpg
| alt            = The curve of a camera lens, inside it the face of a woman and to the right the face of the man, the photographer. 
| caption        = UK release poster
| director       = Eric Lartigau
| producer       = Pierre-Ange Le Pogam
| screenplay     = Eric Lartigau Laurent de Bartillat
| based on       =  
| starring       = {{Plain list | 
* Romain Duris
* Marina Foïs
* Niels Arestrup
* Catherine Deneuve
* Branka Katic
}}
| music          = Evgueni & Sacha Galperine
| cinematography = Laurent Dailland
| editing        = Juliette Welfling
| studio         = {{Plain list | 
* EuropaCorp TF1 Films
* Ciby 2000
}}
| distributor    = EuropaCorp
| released       =  
| runtime        = 114 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}} Douglas Kennedy.

==Synopsis==
A successful Paris lawyer with a seemingly perfect life discovers that his wife is having an affair and accidentally kills her lover in a moment of madness. He escapes the law by faking his own death, assuming his victims identity, and making a fresh start on the Adriatic coast as a photographer. This eventually leads him to realise what was missing in his life before: he finally sees the big picture.

 

== Reception == 
The film was critically praised. Review aggregation website Rotten Tomatoes gives the film a score of 88% based on reviews from 41 critics, with an average score of 7/10.  

== References ==
  

== External links ==
*  
*   
*  
*  
*   Retrieved 2012-12-03
*   on Antoine DAgatas photography for the film, in Le Monde, 2 novembre 2010.  
* {http://ruthlessculture.com/2011/07/29/some-thoughts-on-the-big-picture-2010/}

 
 
 
 
 

 