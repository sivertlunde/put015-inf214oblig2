Idi Muzhakkam
{{Infobox film 
| name           = Idi Muzhakkam
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       = Sreekumaran Thampi
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi Shubha Sukumaran Shyam
| cinematography = Anandakkuttan
| editing        = K Narayanan
| studio         = Bhavani Rajeswari
| distributor    = Bhavani Rajeswari
| released       =  
| country        = India Malayalam
}}
 1980 Cinema Indian Malayalam Malayalam film, Shubha and Sukumaran in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
 
*Jayan as Bheeman
*Ratheesh as Jose Shubha as Chirutha
*Sukumaran as Krishnan Thirumeni
*Roja Ramani as  Panchali
*Sukumari as Gouri 
*Jagathy Sreekumar as Rajan Unnithan/Thyagacharya
*Manavalan Joseph as Valiya Panikkar
*Balan K Nair as Govindan Unnithan
*Janardanan
*Kanakadurga as Gayathridevi/Marykutti
*Lalu Alex as Moosa
*N. Govindankutty as Varkey
*Poojappura Ravi as Kochu Panikkar
 

==Soundtrack== Shyam and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Amme maayaamaye || Vani Jairam, Chorus || Sreekumaran Thampi || 
|-
| 2 || Kaalam Thelinju || S Janaki, P Jayachandran || Sreekumaran Thampi || 
|-
| 3 || Maranju daivama vaanil || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 4 || Odivaa Kaatte || K. J. Yesudas || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 