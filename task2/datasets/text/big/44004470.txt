Hotel High Range
{{Infobox film 
| name           = Hotel High Range
| image          =
| caption        =
| director       = P. Subramaniam
| producer       = P Subramaniam
| writer         = Nagavally R. S. Kurup
| screenplay     = Nagavally R. S. Kurup Sharada Thikkurissi Sukumaran Nair Kallayam Krishnadas Aranmula Ponnamma
| music          = G. Devarajan
| cinematography = ENC Nair
| editing        = N Gopalakrishnan
| studio         = Neela
| distributor    = Neela
| released       =  
| country        = India Malayalam
}}
 1968 Cinema Indian Malayalam Malayalam film, directed and produced by P. Subramaniam . The film stars Sharada (actress)|Sharada, Thikkurissi Sukumaran Nair, Kallayam Krishnadas and Aranmula Ponnamma in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  Sharada as Nalini
*Thikkurissi Sukumaran Nair as Rajasahib
*Kallayam Krishnadas
*Aranmula Ponnamma as Rameshs mother
*Bahadoor
*Jyothi
*Kottarakkara Sreedharan Nair as dhanesh
*Nellikode Bhaskaran
*Paravoor Bharathan as Vasu
*Ramakrishna as Ramesh
*S. P. Pillai as Velu Pilla
*K. V. Shanthi as Madhumathi/Gracy
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ajnaatha Gaayaka || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Gangaa Yamunaa || Kamukara || Vayalar Ramavarma || 
|-
| 3 || Kainiraye || P Susheela || Vayalar Ramavarma || 
|-
| 4 || Pandoru Shilpi || K. J. Yesudas, B Vasantha, T. R. Omana || Vayalar Ramavarma || 
|-
| 5 || Puthiya raagam puthiya thaalam || LR Eeswari || Vayalar Ramavarma || 
|-
| 6 || Snehaswaroopini || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 