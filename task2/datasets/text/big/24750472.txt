Tanu Weds Manu
 
 
{{Infobox film
| name           = Tanu Weds Manu
| image          = tanuwedsmanu.jpg
| caption        = Theatrical release poster
| director       = Anand L. Rai Vinod Bachan Shailesh R. Singh Surya Singh
| writer         = Himanshu Sharma
| screenplay     =
| story          = 
| based on       =  
| starring       =  
| music          = Krsna  RDB
| cinematography = Chirantan Das
| editing        = Hemal Kothari
| studio         =
| distributor    = Viacom 18 Motion Pictures Paramhans Creations and Movies N More Pvt.. 
Soundrya Production
| released       =  
| runtime        = 119 Minutes 
| country        = India
| language       = Hindi
| budget         =   
| gross          =   
}} Hindi romantic UP and East Punjab.  It was dubbed in German and released under the title Tanu Und Manu Trauen Sich.    A sequel, titled Tanu Weds Manu Returns  is scheduled for release on May 22, 2015.

==Plot==
Manoj "Manu" Sharma (R. Madhavan) is a NRI doctor living in London. He comes to India to find an Indian bride and get married. The day he arrives, he is taken to Kanpur by his parents and his friend Pappi (Deepak Dobriyal) to meet Tanuja "Tanu" Trivedi (Kangna Ranaut). While at Tanus house the parents talk and decide they like each others families very well and that if Manu likes Tanu, they make the relationship official the same day. Before anyone can meet Tanu, however, Tanus mother tells Manus family that Tanu has been ill since yesterday and asks if Manu would mind going up to meet her.
Tanu initially seems shy and quiet behind her veil, but Manu eventually realizes that she is in fact asleep. He still comes to like her, and tells both the families that he is willing to marry her.
Soon after the families go on a pilgrimage, and during the train ride Tanu takes him away from their family members, and tells Manu that she had intentionally taken five tablets to fall asleep so as to be rejected by him. She tells him rudely that she loves someone else, and that she has his name tattooed on her chest; she also tells him the thugs who had roughed him up when he initially came to Kanpur were sent by her boyfriend. She demands that Manu reject her. Manu considers the odds, and though he likes her, he asks his father to announce that he cannot marry her.

Weeks pass, and Manu meets different girls, but cannot forget Tanu. Among all the girls, he meets Ayushi (Neha Kaul), who has trouble using her left hand, and her brother Raja (Jimmy Shergill), a small thug. When Raja asks him about his thoughts of getting married to his sister, Manu says that he does like Ayushi, but cant marry her because he loves someone else. Though Manu decides to return to London, Pappi tells him that their friend Jassi (Eijaz Khan) is getting married, and asks him to come to Punjab with him. Upon arriving, Manu finds out that Tanu is present there, and she is a friend of Payal (Swara Bhaskar), Jassis bride-to-be.

Over the course of few days, he becomes close to Tanu, but their new friendship is shattered when Tanus parents arrive, and Tanu thinks that Manu called them. After the misunderstanding, Manu decides to return to London. Tanu realizes her error, and asks him not to leave, as her lover is coming tomorrow, and she wants help in eloping and marrying him. It turns out that Tanus lover is Raja. They are about to be married, but are not able to find a pen to sign the papers. Though Manu has one, he lies about not having it, and the marriage is postponed. Raja confesses that he doesnt want to be married this way, but no one in Tanus family will accept him, thats why they are eloping. Manu talks to Tanus father, and convinces him to accept Raja.

During Tanus wedding preparations, Tanu sees Manus wedding gift (a poster made up of her pictures Manu had taken on different occasions), but does not realize that he loves her. Raja is not able to buy Tanu a wedding dress due to time shortage, so he asks Manu to help him out. After Manu gets Tanu her dress, he confesses to Tanu that he did have a pen, but did not want her to be married, that is why he lied. At that precise moment, Raja appears, and takes Tanu with him. Now Tanu has mixed feelings, and after reaching home, when she confirms with Manu that he loves her, she decides to marry him, not Raja.

Raja, however, is not willing to call off the wedding, and says to Tanus father that if the wedding does not take place, he will kill him. Manu still prepares for the wedding. By bribing the Inspector, the family of Tanu get Raja arrested. However, Raja arrives to the wedding place at the same time as Manu, and he threatens to kill him. Manu however refuses to be scared and proceeds with Tanu for the wedding. It is then when Raja realises that Manu is the right choice for Tanu, and allows them to get married.

==Cast==
* R. Madhavan as Manoj "Manu" Sharma
* Kangna Ranaut as Tanuja "Tanu" Trivedi
* Jimmy Shergill as Raja Awasthi
* Eijaz Khan as Jassi
* Swara Bhaskar as Payal
* Ravi Kishan as Rajas friend
* Deepak Dobriyal as Pappi
* Rajendra Gupta as Mr. Trivedi, Tanus father
* K K Raina as Mr. Sharma, Manus father
* Navni Parihar as Mrs. Trivedi, Tanus mother
* Dipti Mishra as Mrs. Sharma, Manus mother
* Neha Kaul as Ayushi
* Archana Shukla as Bulbul Sas

==Production==
The film had been originally announced with Konkana Sen Sharma playing the role of Tanu. However after a period of stall, the film was re-announced with Kangna Ranaut replacing the character. 

==Reception==
===Critical response===
The film opened to generally positive to average reviews. According to critics, Tanu Weds Manu has a cliched storyline, but has some unconventional and unpredictable situations. The narration of the story is slow according to some critics. 

Critic Taran Adarsh of Bollywood Hungama rated it 3.5/5 and noted that "Tanu Weds Manu is a feel-good, light-hearted entertainer with the right dose of humor, drama and romance, besides a popular musical score and some smart dialogue that act as toppings. If you like simple, uncomplicated films that tug at your heartstrings, then chances are that you might just like this sweet little rom-com". 

Komal Nahta, from Koimoi says "On the whole, Tanu Weds Manu is a family entertainer which will hit the bull’s eye. It may be a slow starter but it will pick up phenomenally by positive word of mouth and ultimately go on to become a hit." 

Nikhat Kazmi, from the Times of India stated "the first thing that strikes you about Tanu Weds Manu is a striking sense of familiarity. It wasnt long before you saw Shahid Kapoor playing a similar sacrificial lover to Kareena Kapoor in Jab We Met. But the deja vu doesnt last long. For despite the predictable – and paper thin – storyline, the film manages to hook you with its sheer atmospherics. Fun while it lasts, Tanu Weds Manu throws up Bollywoods newest obsession – small town girls and their gunas (values) – once again. A meatier storyline and a less messed up climax would have worked wonders for the film"

Anupama Chopra from NDTV gave 2.5/5 and noted "Though the director picked up an interesting subject, he has not succeeded in executing his story effectively on screen – there are not enough laughs in the film. But there is something missing to make it a perfect romantic comedy. If you are looking for a great romantic comedy, this is not the one, but watch it for Madhavan and his chemistry with Deepak".

Noyon Jyoti Parasara of NowRunning.com too gave 2.5/5 and wrote "TWM turns out to be an average film because of the weak second half. It has a feel-good factor to it, which should work with some part of the audience." 

Critic Sukanya Verma from Rediff gave it a 3/5 rating explaining "Tanu Weds Manu is a pleasant experience for most part. Sharma fails to maintain the zing till the very end though. A disappointing, lengthy and gabby third-act makes Tanu Weds Manus running time of 2 hours and 15 minutes longer than it is. It doesnt take offence at anything but will charm you anyway.   

Monica Chopra from sify rated 3/5 and mentioned "Whats not to like? Theres the shaadi-baarat, the romance, lovely clothes, and the atmospherics that transport you into another world.Yes, the film is cliché-ridden, has elements of rom-coms like Jab We Met, and Shergills character track doesnt quite add up, but the film isnt aiming for anything more than a one-time watch entertainer. Anand L Rai (Thodi Life Thoda Magic, Strangers) makes a sweet romantic comedy thatll have you smiling as you leave the theatre". 

Rajeev Masand of CNN IBN gave it a two star rating explaining "Tanu Weds Manu isnt all bad. There are portions in the first half that enjoyable. But held together by a fractured script, they fail to take good shape". 

===Box office===
Tanu Weds Manu collected Rs. 185&nbsp;million in its first week of release. The film held up well in its second week with collections of around Rs. 85.0&nbsp;million.  Its two-week total was Rs. 272.5&nbsp;million nett.  Its business was steady in the third week with around Rs. 50&nbsp;million nett which took the collections to Rs. 322.5&nbsp;million nett in three weeks. In the fourth week, the film collected Rs. 35.0&nbsp;million nett taking its four-week collections to Rs. 360&nbsp;million nett. 

==Soundtrack==
{{Infobox album
| Name = Tanu Weds Manu
| Type = Album
| Artist = Krsna
| Cover =
| Released= 1 February 2011  Feature film soundtrack
| Producer = Krsna
| Programmer = Pritesh Mehta
}}
{{Album ratings
| rev1 = Bollywood Hungama
| rev1Score =   
}}

The soundtracks are composed by Krsna with lyrics written by Rajshekhar. Upon release, the album received positive reviews from most of the critics.  
{| border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s) !!Duration
|-
| "Sadi Gali" – (Rhythm Dhol Bass)
| Lehmber Hussainpuri
| 4:24
|-
| "Yun Hi"
| Mohit Chauhan
| 4:19
|-
| "Rangrez"
| Krsna
| 6:12
|-
| "Piya"
| Roop Kumar Rathod
| 5:29
|-
| "Mannu Bhaiya"
| Sunidhi Chauhan, Ujjaini, Niladri
| 4:49
|-
| "Jugni" Mika
| 3:11
|-
| "Rangrez" (Hey Rangrez Mere Hey)
| Wadali brothers (Puranchand Wadali, Pyare Lal Wadali)
| 6:24
|}

==Remake==
Tanu Weds Manu remake is done in Telugu as Mr. Pellikoduku with actress Isha Chawla along with south Indian actor Sunil (actor)|Sunil. 

==Sequel==
After the success of the film, the makers have planned a sequel to the film titled Tanu Weds Manu Returns with Kangana Ranaut and R.Madhavan reprising their roles. Kangana Ranaut was cast in  dual role in the sequel after her critically acclaimed performance in Queen (film)|Queen.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 