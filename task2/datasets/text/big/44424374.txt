Day by Day, Desperately
{{Infobox film
 | name = Day by Day, Desperately
 | image =Day by Day, Desperately.jpg
 | caption =
 | director = Alfredo Giannetti
 | writer = 
 | story =  
 | starring =  
 | music =  Carlo Rustichelli
 | cinematography = Aiace Parolin
 | editing = Ruggero Mastroianni
 | producer =  Franco Cristaldi
 | distributor =
 | released = 1961 Italian 
 }} 1961 Italian drama film written and directed by Alfredo Giannetti.    According to the film critic Morando Morandini, the film is "a naturalistic drama of strong emotional charge, crossed by a vein of desperate lyricism."   

== Cast ==

* Nino Castelnuovo: Gabriele Dominici
* Tomas Milian: Dario Dominici
* Madeleine Robinson: Tilde 
* Tino Carraro: Pietro 
* Franca Bettoia: Marcella 
* Milly (actress)|Milly: Luisella Riccardo Garrone: un cliente di Pietro Dominici
* Mario Scaccia: un infermiere del manicomio
* Rosalia Maggio: collega di Marcella
* Lino Troisi: un agente di Polizia 
* Marcella Rovena: amica di Pietro Dominici 
* Alvaro Piccardi: Daniele 
* Mario Brega: un borgataro

==References==
 

==External links==
* 
     
   
  
  
 
 
 
   


 
 