Thaamirabharani
 
{{Infobox film
| name = Thaamirabharani
| image = Thaamirabharani DVD Cover.jpg
| caption = DVD Cover Hari
| Hari
| Vishal Prabhu Prabhu Bhanu Vijayakumar
| producer = B. Bharathi Reddy
| music = Yuvan Shankar Raja Priyan
| editing = V. T. Vijayan Vijaya Productions
| released =  
| runtime = 159 minutes
| language = Tamil
| country = India
| budget = 
}} Tamil action film written and directed by Hari (director)|Hari. The film stars Vishal Krishna|Vishal, newcomer Bhanu (actress)|Muktha(Bhanu), Prabhu Ganesan|Prabhu, Vijayakumar (actor)|Vijayakumar, Nadhiya and Nassar in lead roles. The score and soundtrack are composed by Yuvan Shankar Raja. It was released on 14 January 2007 during Thai Pongal along with Joseph Vijay|Vijays Pokkiri and Ajith Kumar|Ajiths Aalwar, eventually becoming a commercial success at the box office, running for more than 100 days in theatres.  
 Telugu and river of the same name, which flows through Thirunelveli and Thoothukudi, where the film is set.

==Plot==
The story is centred on Bharaniputran (a.k.a. Bharani). This loud, angry young man, who doesn’t mind letting his fist fly furiously, is totally in awe of his uncle Saravanaperumal. Bharani and his mother Pechchikani live with Saravanaperumal and his mother Thangapazham. The villagers just venerate this big family in a huge way.

Pitted against them is the other big one — the PVS family (peopled by Sakunthaladevi, Vellathurai and Vellathurais father. And then there is Bhanumathy Saravanaperumal (a.k.a. Bhanu), daughter of Sakunthaladevi Saravanaperumal and Saravanaperumal. Bhanu falls in love with Bharani whereas Bharani unknowingly insults her all the time, even using the press against her at one particular point. As a revenge for Bharani insulting Bhanu, Vellathurai orders Saravanaperumal to be killed. But Saravanaperumal escapes with a minor cut on the back. Angered, Bharani tries to cut off Vellathurais hand in a fight. Vellathurais youngest brother Selvam comes to make a truce and loses his right hand (and his marriage). Bharani comes to know that he has rights to marry Bhanu as we are told  in a fight that Sakunthaladevi is the estranged wife of Saravanaperumal. The two had parted ways over Saravanaperumal’s extreme fondness for his sister Pechchikani and Bharani. The story later is told that they had broken up because of Saravanaperumals sisters husband who had died, hence she stays with them. Sakunthaladevi did not like this so they separated.

It all builds towards a heightened climax as the two young lovers try to hammer out peace between the two families. Vellathurai tries to envelope her with a bad guy Kaarmegam, the villain and here there is a twist. Bharani goes to jail for killing Kaarmegam and after the jail term, he returns to the village, of whom the village folk are waiting for him. Vellathurai & his father come to the village to kill Bharani by hiding when they overhear Sakunthaladevi confessing to Saravanaperumal that it was she who she killed Kaarmegam and that Bharani took the blame for the murder in order to save her & somehow unite her with her husband, resulting in Bharani refusing to marry Bhanu as he wants her to marry Selvam, her uncle. Vellathurai & his father whove been overhearing this come out out of hiding and ask for forgiveness from Saravanaperumal and Bharani for the wrongs and make him accept & marry Bhanu, thus ending with both families uniting at last.

==Cast==
* Vishal Krishna|Vishal as Bharaniputhiran
* Prabhu Ganesan|Prabhu as  Saravanaperumal Muktha (Bhanu) as Bhanumathy Saravanaperumal
* Nadhiya as Sakunthaladevi Saravanaperumal
* Nassar as Vellathurai
* Vijayakumar (actor)|Vijayakumar as Subbaiah (Vellathurais father)
* Nizhalgal Ravi as Thangapaandi
* Rohini (actress)|Rohini as Pechchikani (Bharanis mother)
* Ganja Karuppu as Muthu
* Manorama (Tamil actress)|Manorama as Thangapazham (Saravanaperumals mother)
* Sampath Raj as Kaarmegam
* Aarthi (actress)|Aarthi as Meena
* Chaams as Sombunakki (uncredited)
* Akash as Selvam

Before the debutant heroine, Banu, was chosen, the producers cast another lead, Udayatara. During the course of shooting the film, Bhanu changed her name from Muktha for numerological reasons.

==Release==
===Critical reception===
*Rediff wrote:"Hari has managed to put together a fairly redeemable mixture of action, humour and sentiments, making the movie worth a watch". 
*B Balaji wrote:"Thaamirabharani is all about conflicts. With a large number of characters on either side, two or more of them seem to be fighting, either verbally or physically, most of the time. Though this makes the movie quite loud, it also gives the movie a lot of energy". 
*Hindu wrote:"Hari has a few interesting twists and suspenseful sequences in the narration. The story is not new but it is gripping". 

==Soundtrack==
{{Infobox album
| Name = Thaamirabharani
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Thaamirabharani.jpg
| Released =  
| Recorded = 2006 Feature film soundtrack
| Length = 
| Label = Sa Re Ga Ma
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Madana (film)|Madana (2006)
| This album  = Thaamirabharani (2006)
| Next album  = Deepavali (2007 film)|Deepavali (2007)
}}
 Hari for the first time and with Vishal for third time. The soundtrack was released on 22 December 2006. It features five tracks with lyrics penned by Na. Muthukumar. The song "Karuppana Kaiyale" was adapted from the old devotional song "Karpoora Nayagiye" by L. R. Eswari.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration (min:sec) !! Notes
|- 1 || "Karuppaana Kaiyale" || Ranjith (singer)|Ranjith, Roshini || 3:28 ||
|- 2 || "Kattabomma Oorenakku" || Vijay Yesudas || 4:51 || 
|- 3 || "Thaaliyae Thevaiyillai" || Hariharan (singer)|Hariharan, Bhavatharini || 4:47 ||
|- 4 || "Thiruchendhuru Muruga" || Naveen Mathav || 5:27 ||
|- 5 || KK || 5:16 || 
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 