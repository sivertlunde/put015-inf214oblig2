In the Blood (1988 film)
 
 
 
 
}}
{{Infobox film
| name           = In the Blood
| image          = IntheBlood.jpg
| caption        = Film poster
| film name      = {{Film name
| traditional    = 神探父子兵
| simplified     = 神探父子兵
| pinyin         = Shén Tàn Fù Zǐ Bīng 
| jyutping       = San4 Taam3 Fu6 Zi2 Bing1 }}
| director       = Corey Yuen
| producer       = Wu Ma
| writer         = Yuen Gai-Chi
| narrator       =
| starring       = Andy Lau Bill Tung Chin Siu-ho Siu Hung-Mooi Wu Ma
| music          = Chirs Babida
| cinematography = Tom Lau
| editing        = Peter Cheung Golden Harvest
| released       =  
| runtime        = 101 minutes Hong Kong
| language       = Cantonese
| budget         =
| gross          = HK$6,804,354
| preceded_by    =
| followed_by    = 
}} 1988 Cinema Hong Kong action film directed by Corey Yuen and starring Andy Lau and Bill Tung.

==Cast==
*Andy Lau - Wah
*Siu Hung-Mooi - Tze
*Chin Siu-ho - Fai Ying
*Bill Tung - Commissioner Louis
*Wu Ma - Uncle Ma
*Corey Yuen - Dan
*Woo Gam - Auntie King
*Sammo Hung - Hung Kei (cameo)
*Po Tai - Tai Bo
*Philip Ko - No. 6
*Lau Chau Sang - Curry
*Richard Ng - Parking Meter Man Anthony Chan - Hold-up man
*Alfred Cheung - hold-up man
*Kam Kong Chow - Yee Po
*Lee Chi Kit - Dans Colleague
*Chu Tau - Fai Yings Man
*Peter Chan Lung - Officer 5726 (uncredited)
*Yuen Mo Chow - Pier Thug (uncredited)
*Kwan Yung - Pier Thug (uncredited) Lee Scott

==See also==
*Andy Lau filmography
*Sammo Hung filmography

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 