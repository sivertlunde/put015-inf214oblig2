The Tin Flute (film)
 
{{Infobox film
| name           = The Tin Flute
| image          = 
| caption        =  Claude Fournier
| producer       = Marie-José Raymond
| writer         = Gabrielle Roy Claude Fournier
| starring       = Mireille Deyglun
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 123 minutes
| country        = Canada
| language       = French
| budget         = 
}}
 Claude Fournier same name. Best Foreign Language Film at the 56th Academy Awards, but was not accepted as a nominee. 

The film was co-produced by the National Film Board of Canadas French-language branch, Ciné St-Henri Inc. and Société Radio-Canada, the French-language branch of the Canadian Broadcasting Corporation.

==Cast==
* Mireille Deyglun as Florentine Lacasse
* Pierre Chagnon as Jean
* Michel Forget as Azarius Lacasse
* Marilyn Lightstone as Rose-Anna Lacasse
* Charlotte Laurier as Yvonne
* Thuryn Pranke as Philippe
* Thomas Hellman as Daniel
* Martin Neufeld as Emmanuel
* Linda Sorgini as Marguerite Dennis OConnor as Phil Morin
* Howard Ryshpan as Docteur Katz
* Liliane Clune as Jenny

==See also==
* List of submissions to the 56th Academy Awards for Best Foreign Language Film
* List of Canadian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 