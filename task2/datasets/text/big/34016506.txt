Confession (1955 film)
{{Infobox film
| name           = Confession
| image          = Confession 1955 film poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Ken Hughes
| producer       = Alec C. Snowden
| writer         = Ken Hughes   Don Martin (play)
| narrator       =  Sydney Chaplin John Bentley Peter Hammond
| music          = Richard Taylor
| cinematography = Phil Grindrod
| editing        = Geoffrey Muller
| studio         = Merton Park Studios
| distributor    = Anglo-Amalgamated Film Distributors|Anglo-Amalgamated (UK)   Allied Artists Pictures (US)
| released       = June 1955
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British drama Sydney Chaplin, John Bentley. 

==Production==
The film was made at Merton Park Studios by Anglo-Amalgamated. Along with Little Red Monkey, released the same year, the film was an international hit and led to the company producing films with a higher production quality than they had previously, often importing American stars to give the films more international appeal.

==Cast== Sydney Chaplin - Mike Nelson 
* Audrey Dalton - Louise Nelson  John Bentley - Inspector Kessler  Peter Hammond - Alan  John Welsh - Father Neil 
* Jefferson Clifford - Pop Nelson 
* Patrick Allen - Corey 
* Pat McGrath - Williams 
* Robert Raglan - Becklan 
* Betty Wolfe - Mrs Poole 
* Richard Huggett - Young priest 
* Eddie Stafford - Photographer Percy Herbert - Barman  
* Felix Felton - Bar customer

==References==
 

==Bibliography==
* Chibnall, Steve & McFarlane, Brian. The British B Film. Palgrave MacMillan, 2009.

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 