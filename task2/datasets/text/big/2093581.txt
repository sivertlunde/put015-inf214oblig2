The Curse of the Werewolf
 
 
 
{{Infobox film
| name           = The Curse of the Werewolf
| image          = Curseofthewerewolf.jpg
| image_size     =
| caption        = film poster by Bill Wiggins
| director       = Terence Fisher
| producer       = Michael Carreras Anthony Hinds
| writer         = Anthony Hinds (aka John Elder)
| narrator       = Clifford Evans
| starring       = Clifford Evans Oliver Reed Yvonne Romain Catherine Feller Anthony Dawson Michael Ripper
| music          = Benjamin Frankel Arthur Grant
| editing        = Alfred Cox 
| studio         = Hammer Film Productions
| distributor    = Universal Studios|Universal-International Pictures
| released       = 1 May 1961 (UK) 7 June 1961 (U.S.)
| runtime        = 91 minutes
| country        = United Kingdom English
| budget         =
| gross          =541,006 admissions (France) 
}}
 British film Bray Studios.  The leading role of the werewolf was Oliver Reeds first credited film appearance. 

==Plot==
The story is set in 18th Century Spain. A beggar is imprisoned by a cruel marques after making inappropriate remarks at the noblemans wedding. The beggar is forgotten, and survives another fifteen years. His sole human contact is with the jailer and his beautiful mute daughter (Yvonne Romain). The aging, decrepit Marques makes advances on the jailers daughter while she is cleaning his room. When she refuses him, the Marques has her thrown into the dungeon with the beggar. The beggar, driven mad by his long confinement, rapes her and then dies.

The girl is released the next day and sent to "entertain" the Marques. She kills the old man and flees. She is found in the forest by the kindly gentleman-scholar Don Alfredo Corledo (Clifford Evans) who lives alone with his housekeeper Teresa (Hira Talfrey). The warm and motherly Teresa soon nurses the girl back to health, but she dies after giving birth to a baby on Christmas Day (a fact that Teresa considers "unlucky" since the child was born out of wedlock).

Alfredo and Teresa raise the boy, whom they name Leon. Leon is cursed by the evil circumstances of his conception and by his Christmas Day birth. An early hunting incident gives him a taste for blood, which he struggles to overcome. Soon, a number of goats are found dead, and a herders dog is blamed.

Thirteen years later, Leon as a young man (Oliver Reed) leaves home to seek work at the Gomez vineyard. Don Fernando Gomez (Ewen Solon) sets Leon to work in the wine cellar with Jose Amadayo (Martin Matthews) with whom he soon forms a friendship. Leon falls in love with Fernandos daughter, Cristina (Catherine Feller), and becomes despondent at the seeming impossibility of marrying her, and allows Jose take him to a nearby brothel, where he transforms and kills a woman and Jose, then returning to Alfredos house. Too late, he learns that Cristinas loving presence prevents his transformation, and he is about to run away with her when he is arrested and jailed on suspicion of murder.  He begs to be executed before he changes again, but the mayor does not believe him. His wolf nature rising to the surface, he breaks out of his cell, killing a prisoner and the guard. Shocked and disgusted by his appearance, the local people summon his scholarly step-father, who has obtained a silver bullet made from a crucifix blessed by an archbishop. Though torn with grief, Alfredo shoots Leon dead and covers his body with a cloak.

==Cast==
* Clifford Evans as Don Alfredo Corledo
* Oliver Reed as Leon Corledo
* Yvonne Romain as Servant girl
* Catherine Feller as Cristina
* Anthony Dawson as Marques Siniestro
* Josephine Llewelyn as Marquesa
* Richard Wordsworth as Beggar
* Hira Talfrey as Teresa
* Warren Mitchell as Pepe Valiente
* Anne Blake as Rosa Valiente George Woodbridge as Dominique the goat herder
* Michael Ripper as Old Soak
* Ewen Solon as Don Fernando
* Peter Sallis as Don Enrique
* Denis Shaw as Gaoler
* Francis de Wolff as Bearded customer Charles Lamb as Marques Chef
* Desmond Llewelyn (uncredited) as Marques’s footman

==Home video release==
In North America, the film was released on 6 September 2005 along with seven other Hammer horror films on the 4-DVD set The Hammer Horror Series (ASIN: B0009X770O), which is part of MCA-Universals Franchise Collection.

==In other media== John Bolton from a script by Steve Moore.  The cover of the issue featured a painting by Brian Lewis as Leon in human and werewolf forms.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 