Cake (2005 film)
{{Infobox Film name     = Cake image          =Cakefilmposter1.jpg director       = Nisha Ganatra writer         = Tassie Cameron starring       = Heather Graham David Sutcliffe Sandra Oh Cheryl Hines Bruce Gray Keram Malicki-Sánchez Sarah Chalke and Taye Diggs
| music          = Andrew Lockington producer       = Miranda de Pencier distributor    = Lions Gate Entertainment released   = runtime        = 94 min. language = English budget         =
}}
 
Cake is a 2005 romantic comedy film directed by Nisha Ganatra.

==Plot==
The picture follows the life of Pippa McGee (Heather Graham) as she takes that giant step between 29 and 30 that involves growing up, becoming responsible and discovering true love.
 heart attack. Not only does Pippa have to run the magazine, Wedding Bells, she also has to save it from the chopping block. Wedding Bells future is at risk, as hungry vultures wait to take over her fathers media conglomerate.

Pippa and her straight-laced father have never truly gotten along since her mother died. To complicate things, Pippa becomes involved in a love triangle with her fathers right-hand man Ian (David Sutcliffe) and the free-spirited photographer Hemingway Jones (Taye Diggs).

Everything is completed by the cast of token friends, Lulu (Sandra Oh), Jane (Sarah Chalke) and Rachel (Sabrina Grdevich), who provide Pippa with the moral support she needs to get the job done, both in her love life and in her job as editor.

==Cast==
*Heather Graham as Pippa McGee
*David Sutcliffe as Ian Grey
*Taye Diggs as Hemingway Jones
*Sandra Oh as Lulu
*Keram Malicki-Sanchez as Frank
*Cheryl Hines as Roxanne
*Bruce Gray as Malcolm McGee
*Sarah Chalke as Jane
*Sabrina Grdevich as Rachel
*Michael McMurtry as Luke
*Amy Price-Francis as Sasha
*Reagan Pasternak as Sydney
*Jefferson Brown as Clifford
*Suzanne Cyr as Suzanne
*Frank Chiesurin as Chad
*Dominic Cuzzocrea as Antique Dealer
*Billy Khoury as Diego
*Carlo Rota as Bob Jackman
*Kasia Vassos as Lane
*Martin Roach as Bartender
*Ron White as Janes Father
*Jonathan Keltz as Valet

==External links==
* 
* 

 
 
 
 
 
 
 
 
 