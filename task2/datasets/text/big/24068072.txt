Women (1985 film)
 
 
{{Infobox film
| name           = Women
| image          =
| image_size     =
| alt            =
| caption        =
| director       = Stanley Kwan
| producer       = Vicky Leung Lee
| writer         = Tai An-Ping Chiu   Kit Lai
| narrator       =
| starring       = Cherie Chung Chow Yun-fat Cora Miao Elaine Jin
| music          = Wing-fai Law
| cinematography = Bill Wong
| editing        =
| studio         = Pearl City Films   Shaw Brothers
| distributor    = Shaw Brothers
| released       = 12 June 1985
| runtime        =
| country        = Hong Kong Cantonese
| budget         =
| gross          = HK $9,487,785  
}}
 Hong Kong drama directed by Stanley Kwan.  It was his  directorial debut. Like Kwans following films Women focuses on female characters and their efforts to overcome cultural restrictions.  The cast includes Cherie Chung, Cora Miao, Elaine Jin and Chow Yun-fat.  It was nominated for nine Hong Kong Film Awards including Best Picture.

==Plot synopsis==

The film follows Bao-er (Cora Miao) as she starts her new life as a single mother after divorcing her husband (Chow Yun-fat) having found out he was having an affair with another women (Cherie Chung).

==Awards and nominations==

The film was nominated for nine Hong Kong Film Awards but failed to win any.  In addition Cora Miao received a Best Actress nomination at the Golden Horse Film Festival.

5th Hong Kong Film Awards:
*Nominations:
**Best Picture
**Best Director (Stanley Kwan)
**Best Actor (Chow Yun-fat)
**Best Actress (Cora Miao)
**Best Supporting Actress (Elaine Jin)
**Best Screenplay (Tai An-Ping Chiu, Kit Lai)
**Best Cinematography (Bill Wong)
**Best Original Film Score (Wing-fai Law)
**Best Art Direction (Tony Au)

Golden Horse Film Festival:
*Nominations:
**Best Actress (Cora Miao)

==References==

 

==External links==
*  

 
 
 
 
 

 