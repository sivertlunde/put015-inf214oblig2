Friends with Kids
{{Infobox film
| name           = Friends with Kids
| image          = Friends with kids poster.jpg
| caption        = Theatrical release poster
| director       = Jennifer Westfeldt
| producer       =  
| screenplay     = Jennifer Westfeldt
| starring       =  
| music          = Marcelo Zarvos and The 88
| cinematography = William Rexer II
| editing        = Tara Timpone
| studio         = Points West Pictures Red Granite Pictures Lionsgate Roadside Attractions
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = Less than $10 million 
| gross          = $12,186,625 
}} Adam Scott, Maya Rudolph, Chris ODowd, Edward Burns, Megan Fox and Jon Hamm also star in the film.

== Plot ==
Jason (an advertising executive) and Julie (a charitable investment advisor), longtime best friends now in their mid-30s live in the same building in Manhattan. Not romantically involved, they are close friends with two childless married couples, the placid Alex and Leslie and the sex-obsessed Ben and Missy. During the next four years, after both couples have children, their marriages suffer. Following a chaotic birthday party for Jason at Alex and Leslies place in Brooklyn, Jason and Julie discuss how it would be better to have children first, to get it out of the way since time is running out, and only then meet the person that you wanted to marry.  After more discussion, they decide to have a child together, despite never having had romantic feelings for each other, and then to continue to date other people, to find their one.  Although their friends predict disaster, Jason and Julie adjust to their new relationship with baby Joe far better than their friends had imagined. 

Jason and Julie begin dating again and enter into budding relationships with young actress Mary Jane and divorced father Kurt, respectively. During a couples winter getaway in Vermont, Ben calls Jason and Julies thought process and parenting skills into question. In the ensuing argument, Ben decries their arrangement as untenable in the long term and humiliates Missy. Jason defends his decision to have a child with Julie, saying that he loves her deeply and that she was the soundest choice of person for him to start a family with.

After returning from Vermont, Ben and Missy separate and divorce. Shortly thereafter, at Julies birthday dinner out (about 18 months after Joes birth), Jason is surprised to find that she invited only him. Julie tells him that Kurt wants her to meet his children that weekend but that this new degree of commitment has made her realize that she is in love with Jason, who, along with Joe, have become her closest family. A stunned Jason tells Julie that his love for her has never been romantic and has asked Mary Jane to move in with him. Heartbroken, Julie leaves the restaurant, and soon moves out of her Manhattan apartment to Brooklyn, putting some space between herself and Jason. A few months thereafter, Jason and Mary Jane break up over their differing feelings about children, and both Julie and Jason return to dating others. Several months later, at a bar with Ben, Jason confides that he does have feelings for Julie, but that their messy split makes acting on such feelings impossible. Ben disagrees, noting the differences between his and Missys sex-based relationship and Jason and Julies long-lasting friendship.

Shortly before Julies next birthday, after dropping 2-1/2-year-old Joe off at Julies house after a day out, Jason presents her with a present: a photo scrapbook of the couple, and then the three of them (that hed made for her birthday prior, but she never received due to their abrupt parting), consistent with Julies prior statement that Jason and Joe were her family. They reminisce over several of the photos and then put Joe to bed, after Jason says a few things about staying the night (as Joe wants him to). Jasons emotional shift and words make Julie emotional and uncomfortable, so she sends Jason home.

He leaves, but quickly returns, and tells her what he said to her a year ago was all wrong. He finally realized it—she is the love of his life, she is his person, "and thats just the way it is". She tells him she cant be with someone who isnt into her, and he, after a passionate kiss, offers to have sex with her to prove he is into her, in every possible way. She accepts his offer, passionately kisses him back, and they tumble onto her bed.

== Cast == Adam Scott as Jason Fryman   
*Jennifer Westfeldt as Julie Keller 
*Kristen Wiig as Missy 
*Jon Hamm as Ben 
*Maya Rudolph as Leslie
*Chris ODowd as Alex
*Edward Burns as Kurt
*Megan Fox as Mary Jane  
*Lee Bryant as Elaine Keller, Julies mother
*Kelly Bishop as Mary Fryman, Jasons mother
*Cotter Smith as Phil Fryman, Jasons father

== Production == Adam Scott admitted that the couples suspicion was not baseless, as he and his wife had become "the worst friends to Jen and Jon because we were so busy" after getting married and having children.  She was encouraged to move forward with her idea after an informal reading of the screenplay took place at her and Hamms home in late 2010.

This film is notable for reuniting four cast members from the recent romantic-comedy hit Bridesmaids (2011 film)|Bridesmaids—Kristen Wiig, Maya Rudolph, Chris ODowd, and Jon Hamm.
 Mad Men and would resume for a fifth to get Friends with Kids off the ground and ready to shoot. According to one of the producers, resources for the production were easy to find. "The impossible algorithm is to line up the cast, the calendar and the cash in such a way that you get to make the movie", said co-producer Joshua Astrachan. "It just never is that easy to put an independent film together." 

Principal photography lasted for four weeks,  beginning in New York during December 2010  and carrying out into early 2011.  With a budget of less than US$10 million, Friends with Kids was made by Red Granite Pictures, Points West Pictures, and Locomotive. Red Granite Pictures led by Riza Aziz and Joey McFarland also fully financed the film and distributed it internationally through Red Granites distribution arm led by Danny Dimbort and Christian Mercuri.     

The soundtrack to the film featured many Jazz staples including Duke Ellingtons "Angelica", which is performed by Brian Newman, Alex Smith, Paul Francis, and Steve Whipple.

==Release==
The film premiered at the 2011 Toronto International Film Festival on September 9, 2011. {{Citation
 | title = Friends with Kids premiere photos
 | url = http://www.digitalhit.com/galleries/40/575
 | year = 2011
 | author = Lambert, Christine
 | journal = DigitalHit.com
 | accessdate = 2012-01-02 Lionsgate announced that it had acquired the distribution rights to the film. {{Cite web
  | title = Lions Gate Press Release
  | url=http://investors.lionsgate.com/phoenix.zhtml?c=62796&p=irol-newsArticle&ID=1608950&highlight=
  | accessdate = 21 September 2011}} 

It was released in the United States and Canada on March 9, 2012, Sweden on June 1, 2012, and Australia on June 7, 2012.

===Critical response===
Reviews of the film were generally mixed to positive. It has received a 67% rating on Rotten Tomatoes based on 138 reviews and an average rating of 6.5/10. {{Cite web
  |url=http://www.rottentomatoes.com/m/friends_with_kids/
  |title= Friends With Kids (2012)
  |accessdate=2014-04-20
  |publisher=Rotten Tomatoes}}  It also has a score of 55 on Metacritic based on 36 reviews, indicating "mixed or average reviews".   

==Home media==
The DVD was released on July 17, 2012.  It includes
the usual commentaries, deleted scenes, a blooper reel, and an 8 minute mini-featurette on why the film came to be made.

== References ==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 