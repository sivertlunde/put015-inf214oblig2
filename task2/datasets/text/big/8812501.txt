The Railrodder
{{Infobox film
| name           = The Railrodder
| image          = The Railrodder (film).jpg
| image_size     = 204px
| caption        =
| director       = Gerald Potterton Buster Keaton (uncredited) John Spotton (uncredited)
| producer       = Julian Biggs
| writer         = Gerald Potterton Buster Keaton (uncredited)
| narrator       =
| starring       = Buster Keaton
| music          = Eldon Rathburn
| cinematography = Robert Humble
| editing        =
| distributor    = National Film Board of Canada
| released       = 1965
| runtime        = Short: 24 min.
| country        = Canada
| language       = Silent film
| budget         =
| preceded_by    =
| followed_by    =
}} short comedy travelogue of Canada, The Railrodder was also Keatons final silent film, as the film contains no dialogue and all sound effects are overdubbed.

==Synopsis==
The film begins with Keaton, playing an unnamed English gentleman, reading a newspaper in London, England. A full-page ad proclaiming "SEE CANADA NOW!" catches his attention. He promptly throws the newspaper away and jumps into the River Thames|Thames. He subsequently reemerges on the east coast of Canada (at Lawrencetown, Halifax County, Nova Scotia|Lawrencetown, Nova Scotia), having apparently swum across the Atlantic Ocean|Atlantic, where he is greeted by a sign indicating the direction to the other side of Canada, 3,982½ miles away.

Buster starts his long hike, but soon finds a one-man, open-top rail maintenance vehicle, commonly known as a speeder, parked on a rail track. He sits in the drivers seat intending to take a nap, but he accidentally puts the vehicle in gear, and it speeds off down the track.
 infinite on the inside, as he pulls out everything from pillows and a bison fur coat to a full tea service. Along the way he also has some close calls with locomotives and even other speeders coming the other direction, but emerges harm-free each time.

The backdrop to all of this is the Canadian countryside, as the film provides scenic views of Quebec, Ontario, the Prairies, the Rockies and the West Coast, c. 1964-65. Cities visited by Buster include Montreal, Ottawa and Vancouver.

The film ends with Buster finally arriving at the West Coast. After taking in the view for a few moments, he gets ready to start the long ride back, only to discover his rail car has been taken by a Japanese gentleman who has just emerged from the ocean—presumably the Strait of Georgia—and has decided to take his own tour of Canada. With a shrug, Buster starts walking down the long track.

==Buster Keaton Rides Again==
Concurrent with the production of this film, the National Film Board produced a documentary entitled Buster Keaton Rides Again which combines behind-the-scenes footage of the making of The Railrodder during the fall of 1964,  including sequences showing Keaton and the director discussing (and occasionally arguing over) gags in the film, as well as Keaton meeting fans across Canada, with retrospective footage of Keatons Hollywood career. With a running time of 55 minutes, it is more than twice the length of The Railrodder. According to Kino Videos website for the film, this documentary contains the only known footage of Keaton working behind-the-scenes on a film.  The documentary was filmed in black and white, as opposed to the short film itself, which is in color.

==Availability==
The Railrodder and Buster Keaton Rides Again are available for free streaming on the National Film Boards website as well as on DVD. In addition its also on the NFBs YouTube channel. In Canada, the NFB itself markets the DVD, while Kino Video distributes the film in the United States.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 