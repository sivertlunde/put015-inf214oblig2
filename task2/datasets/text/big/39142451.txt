D'Annunzio (film)
{{Infobox film
 | name = DAnnunzio
 | image = DAnnunzio (film).jpg
 | director = Sergio Nasca
 | writer = Sergio Nasca Piero Chiara
 | starring = Robert Powell  Stefania Sandrelli
 | music = Sergio Sandrelli
 | cinematography =Romano Albani
 | editing = Nino Baragli
 | producer =
 | distributor =
 | released = 1985
 | runtime = 113&nbsp;min
 | awards =
 | country = Italy
 | language = Italian
 | budget =
 }} 1985 Cinema Italian biographical film directed by Sergio Nasca.   

== Plot summary ==
The film focuses the Decadentism, that develops in France and Italy in the late 19th century. Gabriele dAnnunzio is a more said poet, coming from the rural region of Abruzzo, precisely from the seaside town of Pescara. He is already famous for his aesthetic poetry, and hes also a journalist in Rome. There dAnnunzio begins to spend his days in a worldly, living purely in art and in full wealth. He hates democracy, even more mass, and look for the passion and pleasure in the rich ladies of the court, until he meets the noble Elvira Fraternali Leoni, who shes affectionately called "Barbara". This love arouses in dAnnunzio of the inspiration for the writing of his first great novel of the Decadentism: The Pleasure (Il Piacere).

== Cast ==
* 
*Stefania Sandrelli : Elvira Fraternali Leoni, aka Barbara
*Laurent Terzieff : Michetti
*Florence Guérin : Clo Albrini
*Sonia Petrovna : Maria Cruyllas di Gravina
*Teresa Ann Savoy : Maria di Gallese
*Fiorenza Marchegiani : Olga Ossani
*Paolo Bonacelli : Ercole Leoni
*Roberto Alpi : Edoardo Scarfoglio
*Cesare Barbetti : De Bosis
*Eva Grimaldi : Viola

==Plot and sources== Emil Fuchs had an affair with Elvira Fraternali, and this affair is one of the sources for the plot.    

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 