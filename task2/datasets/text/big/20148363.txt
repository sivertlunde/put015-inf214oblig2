Roommates (2006 film)
{{Infobox film
| name           = Roommates
| image          = Roommates film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =  
 | rr             = D-day
 | mr             = Ditei}}
| director       = Kim Eun-kyeong
| producer       = Ahn Byeong-ki
| writer         = Kim Eun-kyeong
| starring       = Lee Eun-sung Yoo Joo-hee Kim Ri-na  Heo Jin-yong Seo Hye-rin
| music          = Oh Bong-jun
| cinematography = Kim Hoon-kwang
| editing        = Park Se-hui
| distributor    = CJ Entertainment
| released       =  
| runtime        = 90 minutes
| country        = South Korea
| language       = Korean
| budget         = United States dollar|$1 million
| gross          =
}} 2006 South Korean film and the third installment of the 4 Horror Tales film series, all with different directors but with the same producer; Ahn Byeong-ki.

==Plot==
Roommates Yoo-jin, Eun-soo, Bo-ram, and Da-young are cramming for a college entrance exam. Its difficult for them to adapt to the stifling atmosphere of the all female lodging institute and to get along with each other, due to their differing personalities. Yoo-jin has the most difficulty with the stuffy institute life. She begins to have visions of events that took place at the institute in the past, such as the tragic fire that occurred years ago. Yoo-jin gradually becomes consumed with fear, and the relationship among the four begins to suffer with dangerous results.

==External links==
*  
*  
*  

 

 
 
 
 
 
 


 
 