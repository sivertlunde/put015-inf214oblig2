Second Sight: A Love Story
{{Infobox film
| name            = Second Sight: A Love Story
| image           =
| caption         =
| director        = John Korty
| producer        = Doug Chapin Jack Clements Barry Krost William Watkins
| writer          = Novel:   Screenplay: Dennis Turner
| narrator        =
| starring        = Elizabeth Montgomery Barry Newman Nicholas Pryor
| music           = Dana Kaproff
| cinematography  = James Glennon
| editing         = Jim Oliver M. Scott Smith  
| distributor     = Warner Bros.
| released        = March 13, 1984
| runtime         = 120 min.
| country         = United States English
| budget          =
| gross           =
}}
Second Sight: A Love Story is a 1984 television film starring Elizabeth Montgomery and Barry Newman. The film is based on the book Emma and I, by Sheila Hocken.

== Plot == blind woman named Alexandra McKay (Montgomery), who has been blind for over 20 years, finally agrees to acquire a guide dog. She is worried that people will try to get close to her out of pity, so she distances herself emotionally from everyone, but becomes attached to her seeing-eye Labrador Retriever companion, Emma.

She meets Richard Chapman, who eventually becomes her boyfriend, and the new romance begins to pull Alexandra out of her shell. When the opportunity arises for a delicate operation that may restore her sight, Alexandra is alternately elated and scared. Richard convinces Alaxandra to attempt the operation.

== Cast and crew ==
*Elizabeth Montgomery as Alexandra McKay
*Barry Newman as Richard Chapman
*Nicholas Pryor as Mitchell McKay
*Mitzi Hoag as Leslie McKay
*Richard Romanus as Dr. Ross Harkin Michael Horton as Gary Renzi
*Liz Sheridan as Mrs. Carlisle
*Susan Ruttan as Robin
*Ben Marley as Michael
*Frances Bay as Old Woman

== External links ==
*  
*  
*OConnor, John J.,  , The New York Times, March 13, 1984

 

 
 
 
 
 


 