Kidnapped (1935 film)
{{Infobox Film
| name           = Kidnapped
| image          = 
| image_size     = 
| caption        = 
| director       = Lau Lauritzen, Jr. Alice OFredericks
| producer       = 
| writer         = Lau Lauritzen, Jr. Alice OFredericks
| narrator       = 
| starring       = Ib Schønberg
| music          = 
| cinematography = Carl Helm Eskild Jensen
| editing        = Edith Schlüssel
| distributor    = 
| released       = 10 January, 1935
| runtime        = 92 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Kidnapped is a 1935 Danish family film directed by Lau Lauritzen, Jr. and Alice OFredericks.   

==Cast==
* Ib Schønberg - Peter Basse Arthur Jensen - Larsen
* Olga Svendsen - Hansine
* Osa Massen - Grethe
* Eigil Reimers - Benjamin Smith
* Per Gundmann - Automobilagent Hansen
* Holger-Madsen - Kriminalassistent
* Alex Suhr
* Holger Strøm
* Christian Schrøder
* Bell Poulsen
* Connie Meiling - Lille Connie

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 