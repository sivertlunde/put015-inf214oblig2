Compound Fracture (film)
{{Infobox film
| name           = Compound Fracture
| image          = CompoundFracturePoster.jpg
| caption        = Theatrical release poster
| director       = Anthony J. Rickert-Epstein
| producer       = Renae Geerlings Tyler Mane Anthony J. Rickert-Epstein John Saunders Robert Ziglar
| writer         = Renae Geerlings Tyler Mane
| starring       = Tyler Mane Derek Mears Muse Watson Renae Geerlings Leslie Easterbrook
| music          = Joel J. Richard
| music supervisor         = Tyler Bates
| cinematography = Anthony J. Rickert-Epstein
| editing        = Anthony J. Rickert-Epstein Glenn Garland
| studio         = Mane Entertainment 
| distributor    = Level 33 Entertainment
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Friday the I Know What You Did Last Summer).

==Cast==
*Tyler Mane as Michael Woolffsen 
*Muse Watson as Gary Woolffsen
*Derek Mears as William
*Renae Geerlings as Juliette 
*Leslie Easterbrook as Annabelle  Alex Saxon as Brandon
*Jelly Howie as Christine
*Susan Angelo as Chloe
*Todd Farmer as Jack 
*Daniel Roebuck as Jim

==Release==
Compound Fracture was exclusively screened in Los Angeles on September 25, 2012.  Throughout 2013, Mane and Geerlings embarked on a limited release and film festival tour in major cities across the United States and Canada, starting with Manes hometown of Saskatoon, Saskatchewan.  The film opened the Dark Matters Film Festival in Albuquerque, New Mexico and was the official selection at the Gold Coast International Film Festival.  

Compound Fracture was released on DVD on May 13, 2014. 

==Reception==
The film currently has an audience approval rating of 84% on the film review website, Rotten Tomatoes. 

HorrorSociety.com gave the film 4.5 out of 5 skulls noting, "This film is ultimate proof that you do not need to have a major studio or millions of dollars behind you to make horror gold."   Fangoria called the film "a solid first outing from Mane Entertainment, utilizing an excellent cast in a layered story of madness, knotted family roots, and supernatural revenge."  

Dread Central gave the film 3.5 out of 5 stars, going on to say "These days, in a been there, seen that genre world where there seems to be very few surprises left for us fans, Mane and his co-writer Geerlings deliver a fantastic little supernatural revenge tale that should keep the indie genre fans out there pleased."  

The film was awarded "Best of Festival" at the 2013 Hot Springs Horror Film Festival.  

==References==
 

==External links==
*  
*  
*   
*  
*  

 
 
 
 
 