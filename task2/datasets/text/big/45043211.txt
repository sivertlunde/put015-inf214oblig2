The Home Town Girl
{{Infobox film
| name           = The Home Town Girl
| image          = Vivian Martin 1919.jpg
| alt            = 
| caption        = Ad for film
| director       = Robert G. Vignola
| producer       = 
| screenplay     = Oscar Graeve Edith M. Kennedy 
| starring       = Vivian Martin Ralph Graves Lee Phelps Carmen Phillips Stanhope Wheatcroft Herbert Standing
| music          = 
| cinematography = Frank E. Garbutt 	
| editor         =	
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Comedy comedy film directed by Robert G. Vignola and written by Oscar Graeve and Edith M. Kennedy. The film stars Vivian Martin, Ralph Graves, Lee Phelps, Carmen Phillips, Stanhope Wheatcroft and Herbert Standing. The film was released on May 11, 1919, by Paramount Pictures.  

==Plot==
 

==Cast==
*Vivian Martin as Nell Fanshawe
*Ralph Graves as John Stanley
*Lee Phelps as Frank Willis
*Carmen Phillips as Nan Powderly
*Stanhope Wheatcroft as Steve Ratling
*Herbert Standing as Peter Jellaby
*Pietro Sosso as Mr. Fanshawe
*Edythe Chapman as Mrs. Fanshawe
*William Courtright as Ryder Brother
*Tom Bates as Ryder Brother
*Thomas Persse as Manager

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 