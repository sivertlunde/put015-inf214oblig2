Without Compassion
 
{{Infobox film
| name           = Without Compassion
| image          =
| caption        =
| director       = Francisco José Lombardi
| producer       = Francisco José Lombardi Gustavo Sánchez
| writer         = Augusto Cabada Fyodor Dostoyevsky
| starring       = Diego Bertie
| music          =
| cinematography = Pili Flores-Guerra
| editing        = Luis Barrios
| distributor    =
| released       =  
| runtime        = 120 minutes
| country        = Peru
| language       = Spanish
| budget         =
}}

Without Compassion ( ) is a 1994 Peruvian drama film directed by Francisco José Lombardi, and based on Crime and Punishment, by Fyodor Dostoyevsky. It was screened in the Un Certain Regard section at the 1994 Cannes Film Festival.   

==Cast==
* Diego Bertie as Ramón Romano
* Adriana Dávila as Sonia Martinez
* Jorge Chiarella as Mayor Portillo
* Marcello Rivera as Julian Razuri
* Ricardo Fernández as Leandro Martinez
* Carlos Oneto as Priest
* Hernán Romero as Alejandro Velaochaga
* Mariella Trejos as Señora Aliaga
* Humberto Modenesi as Señor Aliaga
* Juan Jose Criados as Nico
* Isabel Solari as Paula
* Mónica Domínguez as Journalist
* Ruth Escudero as Jueza
* Marcial Mattheus as Doctor
* Benjamín Sevilla as Jabali
* Óscar Orellana as Photographer

==Awards and nominations==
;Won
* Havana Film Festival: Best Actor (Diego Berti)

;Nominated
*  

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 