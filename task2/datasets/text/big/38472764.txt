Girls Against Boys (film)
 
{{Infobox film
| name           = Girls Against Boys
| image          = 
| caption        = 
| director       = Austin Chick
| producer       = Aimee Shieh Clay Floren Daniel Sollinger
| writer         = Austin Chick
| starring       = Danielle Panabaker Nicole LaLiberte Andrew Howard Michael Stahl-David Liam Aiken
| music          = Nathan Larson
| cinematography = Kat Westergaard
| editing        = 
| studio         = 
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  $7,529 
}} thriller film written and directed by Austin Chick and starring Danielle Panabaker and Nicole LaLiberte. 

==Plot==
College student Shae (Danielle Panabaker) blows off going to a party with her friend Karen (Reyna de Courcy) to go to The Hamptons for the weekend with her boyfriend Terry (Andrew Howard), an older man who is separated from his wife. That evening, Terry comes round to her apartment. He apologises and says that they cant see each other any more - he and his wife have decided to reconcile, for the sake of their daughter. The next day, Shae goes shopping at a farmers market and sees Terry with his wife and daughter. She follows them to their house and he notices her. They do not speak and after he closes the door, she leaves. That night, she goes to a bar where she works. During her shift, she goes to the cloak room and starts to cry. She is interrupted by her colleague Lu (Nicole LaLiberte), who comforts her and invites her out for a drink.

They go out to another bar where they meet Simon (Michael Stahl-David), Eric (Carmine DiBenedetto) and Duncan (Will Brill). They drink and dance with them and then go back to Eric and Duncans apartment. Shae kisses Simon and then goes to the bathroom, where she falls asleep. Feeling unwell again, she decides to go home. Simon offers to take her home, saying that he was about to leave anyway. They get a taxi together and are dropped off outside her apartment. He asks if he can come up, but she tells him she has a boyfriend. He persists, asking first for her number and then for a kiss goodnight. She tells him to leave and he becomes violent, grabbing her neck and forcibly kissing her. She hits him and runs upstairs. Before she can unlock her door, he catches her, pushes her onto the floor and rapes her.

Later, in her apartment, Shae tries to call her mom and Karen, but can only reach their voicemails. With no-one to talk to, she goes to Terrys house. He is furious that she has turned up at his house unannounced and takes her home. They go into her apartment and Shae, having not told him that she was raped yet, hugs him. He misunderstands and starts kissing her. She tells him to stop but he tries to force himself on her. She shouts at him to get off her and starts crying. He leaves, telling her that he doesnt know what she wants from him.

Lu calls her and they go to a police station to report the crime. There, the hostile and skeptical Officer Daniels (Matthew Rauch) gives her paperwork to fill in and sends her to the waiting area. Shae files her report with an unsympathetic detective (Kelvin Hale) and Lu notices Daniels looking at her. She flirts with him and she asks him if he wants to go somewhere to have sex. They do and she cuffs him to the bed, takes his gun and shoots him.

Lu goes to Shaes apartment and tells her that they need to deal with the situation themselves, going back to Eric and Duncans apartment and finding out where Simon lives. She tells Shae that she has a gun, which she got from a police officer. They go back there and probe Duncan for Simons address. He doesnt know it and when he jokes about how Eric told them he and Shae had sex, Lu shoots him. Shae laughs and then throws up, but is okay with it. When Eric comes back, he tells them Simons address and Lu shoots him too. They go to Simons workshop and find him working alone. Shae knocks him unconscious and they tie him to his table. When he wakes up, they torture and kill him.

Next, they ambush Terry, kidnapping him at gunpoint and driving him off in his own car. They drive to a secluded part of the woods and take him out of the car. Shae points the gun at him and he begs for her forgiveness. She cant bring herself to kill him and he starts to run away but Lu takes the gun and shoots him. They stay in a motel that night and when Shae starts crying, Lu gets into bed with her and holds her. The next morning, they talk about what they did and while Shae says that it doesnt make her feel any better, Lu enjoyed it and says that she hurts people because she can. They go home and return to their lives. After a class, Shae is asked out by her classmate Tyler (Liam Aiken) and they go to the fair. She goes back to her apartment that evening and finds Lu waiting for her, annoyed that she is back so late because she had wanted to surprise her with a ready dinner.

Shae goes to a Halloween party with Tyler, who is DJing. Lu secretly arrives and when Shae is in the bathroom and everyone else is watching fireworks on the roof, she kills Tyler with a sword. Shae finds his body and returns home, finding Lus sword on a table. Lu steps out of the shower nude and tells Shae that she did it "for us" and to "protect" her. Shae takes the sword and kills Lu.

Some time later Shae returns to work at the bar. During a break, she is interrupted by a crying co-worker (Makenzie Leigh). Shae asks "is it a guy?" and the woman replies that it is. As the two sit together, a vision of Lu is seen behind Shae.

==Cast==
* Danielle Panabaker as Shae
* Nicole LaLiberte as Lu
* Andrew Howard as Terry
* Michael Stahl-David as Simon
* Liam Aiken as Tyler
* Carmine DiBenedetto as Eric
* Will Brill as Duncan
* Matthew Rauch as Officer Daniels
* Reyna de Courcy as Karen
* Kelvin Hale as "Big Gulp"
* Makenzie Leigh as Crying Girl
* Caroline Lagerfelt as Professor Sara Randolph
* Suzie Cho as Officer Ramirez

==Critical reception==
The film received mixed reviews from critics. It has a score of 21% on Rotten Tomatoes based on 19 reviews. 

==References==
 

==External links==
*  

 
 
 
 