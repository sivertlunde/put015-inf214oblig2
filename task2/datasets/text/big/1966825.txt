Macbeth (1948 film)
 
{{Infobox film name           = Macbeth image          =Macbeth-1948-Poster.jpg image_size     = caption        = U.S. theatrical release poster director       = Orson Welles producer       = {{Plainlist|
*Orson Welles
*Charles K. Feldman Richard Wilson
}} writer         = {{Plainlist|
*William Shakespeare (play)
*Orson Welles (adaptation, uncredited)
}} starring       = {{Plainlist|
*Orson Welles
*Jeanette Nolan
*Dan OHerlihy
*Roddy McDowall
*Edgar Barrier
*Alan Napier
}} music          = Jacques Ibert cinematography = John L. Russell editing        = Louis Lindsay distributor    = Republic Pictures released       =   runtime        = 107 minutes country        = United States language       = English budget         = $800,000  gross          =288,950 admissions (France)   at Box Office Story 
}} American film adaptation by Orson Welles of William Shakespeares tragedy Macbeth.

==Production==
===Pre-production=== Wuthering Heights and Bride of Frankenstein."   

Teaming with producer Charles K. Feldman, Welles successfully convinced Herbert Yates, founder and president of Republic Pictures, of the prospect of creating a film version of Macbeth. Yates was attempting to raise the level of his studio, which produced Roy Rogers Westerns and low-budget features, into that of a prestige studio. Republic had already tried to present off-beat features, including Gustav Machatýs Jealousy (1945) and Ben Hechts Specter of the Rose (1946).  
 provide Welles with a large budget. Welles guaranteed to deliver a completed negative of Macbeth on a budget of $700,000. When some members of the Republic board of directors expressed misgivings on the project, Welles had a contract drawn in which he agreed to personally pay any amount over $700,000.  

Welles had previously staged the so-called Voodoo Macbeth in 1936 in New York City with an all-black cast, and again in 1947 in Salt Lake City as part of the Utah Centennial Festival. He borrowed aspects from both productions for his film adaptation. 
 A Midsummers Romeo and Henry V (which was produced in Great Britain in 1944 but not seen in the U.S. until 1946) helped to propel Welles Macbeth forward. 

===Casting===
Welles cast himself in the title role, but was initially stymied in casting   Publishers 1992 ISBN 0-06-016616-9.  

Welles brought in Irish actor Dan OHerlihy in his first U.S. film role as Macduff (thane)|Macduff, and cast former child star Roddy McDowall as Malcolm (Macbeth)|Malcolm. Welles also cast his daughter Christopher in the role of Macduffs son; this was her only film appearance. 

The cast of Macbeth is listed in the AFI Catalog of Feature Films. 
 Macbeth 
*Jeanette Nolan … Lady Macbeth Macduff
*Roddy Malcolm
*Edgar Barrier … Banquo
*Alan Napier … A Holy Father Duncan
*John Dierkes … Ross
*Keene Curtis … Lennox Witch
*Lionel Braham … Siward Siward
*Jerry Farber … Fleance Macduff Child
*Morgan Farley … Doctor
*Lurene Tuttle … Gentlewoman and Witch
*Brainerd Duffield … First Murderer and Witch
*William Alland … Second Murderer
*George Chirello … Seyton
*Gus Schilling … A porter

===Adaptation===
In bringing Macbeth to the screen, Welles made several changes to Shakespeares original. He added sequences involving the witches to increase their significance. At the beginning of the film, they create a clay figurine of Macbeth, which is used to symbolize his rise and ruin. Cowie, Peter. "The Cinema of Orson Welles."1978, A.S. Barnes & Co.  It collapses in a heap, seemingly of its own volition, immediately after Macbeth is beheaded. The witches seem to cast a spell on the doll, and anything that happens to it seems to happen also to Macbeth, as in voodoo (Welles had previously staged a famous Voodoo Macbeth). The witches also return at the end of the film, viewing the drama from afar and uttering  "Peace, the charms wound up" as the final line (this line is spoken in the first act in the original text, when the witches initially confront Macbeth). 
 Druidical pagan sleepwalking and madness scene; in the play, he is not present. 
 King Lear, and as Peter Greenaway did in Prosperos Books.

===Filming=== westerns that were normally made at Republic Studios. In order to accommodate the tight production schedule, Welles had the Macbeth cast pre-record their dialogue.

Welles later expressed frustration with the films low budget trappings. Most of the costumes were rented from Western Costume, except those for Macbeth and Lady Macbeth. "Mine should have been sent back, because I looked like the Statue of Liberty in it," Welles told filmmaker Peter Bogdanovich. "But there was no dough for another and nothing in stock at Western would fit me, so I was stuck with it."  

Welles also told Bogdanovich that the scene he felt was most effective was actually based on hunger. "Our best crowd scene was a shot where all the massed forces of Macduffs army are charging the castle", he said. "There was a very vivid urgency to it, because what was happening, really, was that wed just called noon break, and all those extras were rushing off to lunch."  

Welles shot Macbeth in 23 days, with one day devoted to retakes. 

==Release and reception==
Republic initially planned to have Macbeth in release by December 1947, but Welles was not ready with the film. The studio entered the film in the 1948 Venice Film Festival, but it was abruptly withdrawn when it was compared unfavorably against Oliviers version of Hamlet (1948 film)|Hamlet, which was also in the festivals competition. 
 Scottish burrs and by his decision to telescope the Shakespeare text into a compact 107 minute film. 

After its original release, Republic had Welles cut two reels from the film and ordered him to have much of the soundtrack re-recorded with the actors speaking in their natural voices, and not the approximation of Scottish accents that Welles initially requested. This new version was released by Republic in 1950. While critical reaction was still not supportive, the film earned a small profit for the studio. 

Welles would maintain mixed emotions about Macbeth. In a 1953 lecture at the Edinburgh Fringe Festival, he said: "My purpose in making Macbeth was not to make a great film – and this is unusual, because I think that every film director, even when he is making nonsense, should have as his purpose the making of a great film. I thought I was making what might be a good film, and what, if the 23-day day shoot schedule came off, might encourage other filmmakers to tackle difficult subjects at greater speed. Unfortunately, not one critic in any part of the world chose to compliment me on the speed. They thought it was a scandal that it should only take 23 days. Of course, they were right, but I could not write to every one of them and explain that no one would give me any money for a further days shooting...However, I am not ashamed of the limitations of the picture." 

The truncated version of Macbeth remained in release until 1980, when the original uncut version with the Scottish-tinged soundtrack was restored by the UCLA Film and Television Archive and the Folger Shakespeare Library.  Critical opinion of the film has drastically improved since its original release, with many now regarding it as one of Welles most notable films. 

==References==
 

==External links==
 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 