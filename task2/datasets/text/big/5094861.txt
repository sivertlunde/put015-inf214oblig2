Ernst Thälmann (film)
{{Infobox Film
| name           = Ernst Thälmann
| image          = Fotothek df roe-neg 0006580 014 Bild Litfaßsäule mit Filmplakaten für "Ernst Thä.jpg
| image_size     =  Central Station of Leipzig, 1954. 
| director       = Kurt Maetzig
| producer       = Adolf Fischer
| writer         = Willi Bredel, Michael Tschesno-Hell
| narrator       = 
| starring       = Günther Simon 
| music          = Wilhelm Neef
| cinematography = Karl Plintzner
| editing        = Lena Neumann 
| studio         = DEFA
| distributor    = PROGRESS-Film Verleih
| released       = Sohn seiner Klasse:    Führer seiner Klasse:   
| runtime        = Sohn seiner Klasse: 127 minutes Führer seiner Klasse: 140 minutes
| country        = East Germany
| language       = German
| budget         = 10,000,000 East German Mark 
}}
 East German film in two parts about the life of the German Communist leader Ernst Thälmann, directed by Kurt Maetzig and starring Günther Simon in the title role. The first picture, Ernst Thälmann - Sohn seiner Klasse (Son of his Proletariat|Class), was released in 1954. It was followed by the 1955 sequel Ernst Thälmann - Führer seiner Klasse (Leader of his Class).

==Plot==

===Ernst Thälmann - Son of his Class=== revolution has Western Front - and his friend Fiete Jansen rebel against their officers, Zinker and Quadde, and desert. Harms dies in a shelling. In Berlin, the American capitalist Mr. McFuller demands to crush the Spartacus League|Spartacists. Zinker, now a member of the Freikorps, murders Karl Liebknecht and Rosa Luxemburg. Thälmann hears of it and promises their sacrifice will not be in vain. Jansen falls in love with Harms daughter, Änne.
 Social Democrats who reject violence, ambushes the Freikorps and captures their officers. The Social Democrat Police Senator Höhn frees them after they lightheartedly promise not to use violence.
 USPD congress, calling to unite with the Communist Party of Germany|KPD, when the Soviet steamship Karl Liebknecht, loaded with wheat for the citys unemployed, reaches the port. Höhn sends Quadde, now a police captain, to prevent the distribution of the cargo, but after a stand-off the police retreat. Thälmann visits Vladimir Lenin and Joseph Stalin in Moscow with other German communists.
 communist uprising in Hamburg, and manage to hold out against the Reichswehr and the police. Fiete killes Zinker. Then, a delegate from the Central Committee announces that armed struggle is no longer the policy of the party, and the weapons promised to them by the leadership will not arrive. The communists are forced to flee. Jansen is sentenced to death, but eventually his life are spared. Thälmann appears in the Hamburg harbor and promises not to abandon the struggle.

===Ernst Thälmann - Leader of his Class=== Reichstag and presidential elections take place, veteran SPD member Robert Dirhagen is reluctant to support Paul von Hindenburg, although this is the party line. Thälmann calls for class unity against the Nazis, but the SPD leaders do not want to collaborate with him.
 elections for The Nazis seize power.
 burn the SS guards Hamburg is bombed, and she dies in her cell.
 The communist Jansen and the Social Democrat Dirhagen shake hands. In Berlin, Thälmann leaves his cell to be executed, while contemplating on Pavel Korchagins words from How the Steel Was Tempered: "...All my life, all my strength were given to the finest cause in all the world - the fight for the liberation of mankind."

==Cast==
 
*Günther Simon as Ernst Thälmann
*Wolf Kaiser as Zinker
*Werner Peters as Gottlieb Quadde
*Nikolai Kryuchkov as Soviet colonel
*Michel Piccoli as Maurice Rouger
*Siegfried Weiss as industrialist
*Fritz Diez as Adolf Hitler
*Werner Dissel as uncredited role
*Fred Delmare as soldier
*Hannjo Hasse as army officer
*Horst Kube as concentration camp commandant
*Angela Brunner as Irma Thälmann
*Arthur Pieck as Wilhelm Pieck (part 1)
*Hans Wehrl as Wilhelm Pieck (part 2)
*Karl Brenk as Walter Ulbricht
*Gerd Wehr as  
*Karl Weber as Friedrich Ebert
*Martin Flörchinger as Karl Liebknecht (part 1)/Saarland delegate (part 2)
*Judith Harms as Rosa Luxemburg
*Kurt Wetzel as army officer (part 1)/Hermann Göring (part 2)
*Hans Stuhrmann as Joseph Goebbels
*Eberhard Kratz as Fritz Tarnow
*Erich Brauer as Carl Severing
*Peter Schorn as Vladimir Lenin
*Gerd Jäger as Joseph Stalin (scenes removed)
*Steffie Spira as Clara Zetkin
*Joe Münch-Harris as Gustav Noske
*Hans Flössel as Philipp Scheidemann
*Karl-Eugen Lenkerring as Gustav Stresemann
 
*Fred Kötteritzsch as Franz von Papen
*Will van Deeg as Heinrich Himmler
*Georges Stanescu as Georgi Dimitrov
*Theo Shall as judge (part 1)/Marcel Cachin (part 2)
*Hubert Temming as Jacques Duclos
*Karl Heinz Weiss as Maurice Thorez
*Carla Hoffmann as Rosa Thälmann
*Hans-Peter Minetti as Fiete Jansen
*Karla Runkehl as Änne Jansen
*Isa Henselmann as Ännchen Jansen
*Wilfried Ortmann as Hannes Harms
*Ursula Röschmann as Mrs. Harms
*Paul R. Henker as Fietes jailer (part 1)/Robert Dirhagen (part 2)
*Erich Franz as Arthur Vierbreiter
*Erika Dunkelmann as Martha Vierbreiter
*Raimund Schelcher as Krischan Daik
*Herbert Richter as Kruczinski
*Rudolf Klix as Willbrandt
*Johannes Arpe as Police senator Höhn
*Sergei Kalinin as Karl Liebknecht captain
*Karl Kendzia as Adolf Wahlkeit
*Otto Dierichs as enraged industrialist
*Wilhelm Koch-Hooge as Captain Schröder
*Kurt Dunkelmann as Thälmanns first jailer
*Otto Krone as Thälmanns second jailer
*Pitt Kröger as the second jailers son
*Hans-Peter Thielen as Hartrein
*Werner Pledath as Hauck senior
*Hannes Fischer as Hauck junior
*Paul Paulsen as Mr. McFuller
 

==Production==

===Background===
 
 , and therefore, their successors in the Socialist Unity Party of Germany were the legitimate leaders of a new German state.  Thälmann became the center of what many historians saw as a cult of personality. Heller, Plamper. p. 312.  Nothnagle. p. 121.  Roden. p. 170.  This veneration required all controversial aspects of his political career be repressed from mass consciousness.  Journalist Erich Wollenberg wrote that in the Ernst Thälmann films, "the Thälmann cult reached its apotheosis." Monteath. p. 109. 

===Inception===
The film was conceived in 1948, after the   and Michael Tschesno-Hell, both political functionaries, were exempted from all their other duties to concentrate on writing the script. A Thälmann Committee was convened to direct the production of the film; its members included representatives from the Ministry of Culture, the Ministry of Press and Agitation, the DEFA studio,  and Thälmanns widow, Rosa, although she was removed in 1949.  The committee held its first meeting on 8 October 1948. At the third meeting, on the 27th, the members decided that portraying Thälmanns entire life would make the film too cumbersome, agreeing it should concentrate only on the important historical events. The resolution also stated that the plot should focus on meetings between Thälmann and small groups of people, who would be seen embracing Socialism after being convinced by "the radiance of his personality". At the fourth meeting, it was suggested to begin the plot only in 1931 and stress Thälmanns part in the  ; yet member Otto Winzer pointed out that in order to appeal to the youth, the picture should deal with the protagonists earlier years. 

===Development===
 , to promote the film.]] SPD took place.  It also featured his childhood and youth with his parents, his falling in love with the young Rosa Koch and his years as a simple worker who turned to communism. 

DEFA concluded that Bredels and Tschesno-Hells script would require splitting the film into three parts. This was deemed to long by the committee. After a year of deliberations, most of the original screenplay was rejected.  In January 1951, it was decided to have a two-part picture, the first dealing with the time from the end of World War I to 1930, and the second taking off in 1932 and continuing until the founding of the German Democratic Republic. The two parts were named Ernst Thälmann - Sohn des Volkes and Ernst Thälmann - Führer des Volkes (son and leader of the people, respectively). The titles were later changed to Sohn and Führer seiner Klasse. 

The political establishment had closely monitored the work. According to historian René Börrner, "no other film, in the years before or after, received such attention from the SED". Börrner. p. 31.  On 21 August 1951, Walter Ulbricht sent the committee a letter in which he requested that a meeting between Thälmann and Joseph Stalin would be portrayed. 
 Formalistic approach, Socialist realist line. During 1952, Bredels and Tschesno-Hells script was again subject to revisions and had to be rewritten. In July, State Secretary of Press and Agitation Hermann Axen told the Thälmann committee that the main problem to be solved was "The authors primitive depiction of Thälmann", which failed to present his "grand revolutionary instinct". Later, committee member Hermann Lauter demanded to include historical events with no relation to Thälmanns life, like the October Revolution. 

===Approval===
 
During late 1952, the writers accepted most of the demands. Their final draft was approved by the managerial committee and the Ministry of Culture only on 13 March 1953. The work on the screenplay of Leader of his Class began in summer 1953. Russel Lemmons claimed that this time, the writers "knew what was expected of them". The script was completed on 8 September, and later accepted with only minor changes. 
 Vladimir Semyonov, Sergei Gerasimov were present as well. Semionov personally made an adjustment to the script; he requested that a scene in which Thälmann appeared to be concerning doubt would be removed, since it was not in accordance with the principles of the proletarian struggle. In general, however, he approved of the presentation;  the script also introduced elements fitting the atmosphere of the Cold War, in the form of the films main villain, the American capitalist Mr. McFuller. 
 Barracked Peoples Police were daily used throughout the shooting in the roles of extras. 

==Reception==

===Contemporary response===
 . Note the Thälmann Protome on the left.]] Berlin Conference brought about a temporary rapprochement between the two states. 
 collective farms National Prize, language = German|author=DEFA Foundation  language = German|author=DEFA Foundation 
|date=|publisher= |work=defa.de|accessdate=20 June 2011}} 
 language = Czech|author= 
|date=|publisher= |work=kviff.com|accessdate=22 April 2011}} 

===De-Stalinization=== new course in the politics of the Eastern Bloc, including in the field of art. Joseph Stalins character, which was celebrated during his lifetime, was now being edited out of many motion pictures; some films made before 1953 were banned altogether. 

On 5 June 1956, a month before the 9th Karlovy Vary Festival,   , which espoused a strict anti-Stalinist line, a group of officials in the East German Ministry of Culture held a conference from the 25th to the 27th of November 1961. They decided to remove all the footage involving the figure of Stalin from the film. All copies, even those abroad, were subject to the resolution.  In the post-1961 version, Stalin does not make an appearance, but his name remains in the opening credits, along with the actor portraying him, and is mentioned on several occasions.

===Critical reaction===
  awards flowers to several actors who played in the films. On the left, Johannes Arpe (Höhn). Holding bouquet: Rudolf Klix (Willbrandt).]]
In East Germany, the films were received with favourable acclaim. On 28 March 1954, Minister of Culture Johannes R. Becher called Son of his Class a "national heroic epic," and a "masterful depiction of history" at an article published in the Tägliche Rundschau newspaper.  Berliner Zeitung columnist Joachim Bagemühl wrote that "Maetzig created massive crowd scenes, the likes of which were rarely seen in film hitherto." {{cite web |url=http://bildungsserver.berlin-brandenburg.de/6025.html|title=Ernst Thälmann – Führer seiner Klasse|trans_title=Ernst Thälmann - Leader of his Class| language =German|author= 
|date=|publisher= |work=Bildungsserver Berlin|accessdate=22 April 2011}}  Journalist Herbert Thiel dubbed the second part "an outstanding film" in a Schweriner Volkszeitung article from 1 October 1955. The Das Volk magazine critic Kurt Steiniger claimed  his "heart beated in coordination with the thousands of people around Thälmann" when he watched the picture. On 18 October, a reporter of the Mitteldeutsche Neuste Nachrichten wrote "not a single person will not ask himself... how is it, that this film touched me so deeply?"  Author Henryk Keisch commented: "in the midst of those unprecedentedly monumental scenes... There is a distinct man, with distinct emotions and thoughts... it is a grand work of art." Börrner. p. 32.  In 1966, the GDRs Cinema Lexicon called Ernst Thälmann a "thrilling and informative document about the indestructible force of the best parts of the German people, successfully recreating... the heroic struggle of the German workers led by Thälmann." 
 language = German|author=uncredited author  language = German|author=uncredited author 
|date=9 November 1955|publisher=Der Spiegel |work=|accessdate=23 April 2011}} (bottom of the page).  Detlef Kannapin wrote the films were "propagating a myth", intended to "espouse propaganda elements... in a Socialist Realist style" and their main aim was to depict Thälmann as "the great, faultless leader."  Seán Allan and John Sandford described it as combining "fact with the officially endorsed distortion of history." Allan, Sandford. p. 70.  Sabine Hake wrote the film was made after Maetzig turned to directing pictures with "straightforward propagandistic intentions."  Russell Lemmons concluded that eventually, instead of a story of a simple man rising to greatness, it was a history of the German working movement in the 20th century. 

In a 1996 interview, Kurt Maetzig told "I believe the first part is bearable and even has artistic qualities, while the second deteriorated... Due to over-idealization. In many aspects, it is simply embarrassing." 

===Historical accuracy===
 
Shortly after the script of Son of his Class was approved, DEFA director-general   told the Thälmann committee members that he was concerned about the veracity of the plot. He pointed out three inaccuracies: in 1918, there were no German Revolution of 1918–19|Workers and Soldiers Councils on the Western Front, only inside Germany; the American general accomppanying Mr. McFuller could not have been present in Berlin during the crushing of the Spartacus Uprising, since peace with the United States was not achieved yet; and finally, Wilhelm Pieck was not with Rosa Luxemburg and Karl Liebknecht on 9 November 1918. Only Piecks part was omitted from the screenplay. Bredel told Schwab the rest would be left for the decision of the Politburo. The scenes which the director-general opposed to appear in the film. 
 Academy of Sciences on 17 November 1955, West German film critic Klaus Norbert Schäffer told writer Michael Tschesno-Hell the second part focused solely on the communist resistance to the Nazis, ignoring the Social-Democrats and others who opposed the regime. He also mentioned that while Thälmann was incarcerated in three different prisons, the film gives the impression he was held only in one. Another point made by Schäffer was that the arms shipment promised to the communist rebels in Hamburg was intercepted by the army, and not held back by Thälmanns enemies in the party, as seen in Son of his Class. Tschesno-Hell responded to Schäffer by telling: "there are great truths and minor truths. In art, it is completely legitimate to permit the great ones have precedence."  René Börrner noted the film skipped over the years between 1924 and 1930, thus ignoring Thälmanns ascendance to the position of party chief - and the many controversies and ideological rifts which characterized the KPD in those days. Börrner. p. 31. 
   struggle against Kapps supporters. 

Historian Detlef Kannapin noted that, while the film portrays Thälmann as seeking to convince the reluctant Social-Democrats to join forces against the Nazis, he never pursued this policy. As late as October 1932, he referred to the SPD as the chief rivals of the communists, and often called them "Social fascism|Social-Fascists". The Cominterns resolution to form an anti-Nazi bond with the Social-Democrats was only made in 1935, when Ernst Thälmann was already imprisoned. According to Kannapin, the figure of Robert Dirhagen, the minor SPD member, symbolizes the Social-Democrat wing of the SED, which united with KPD under Soviet pressure.  Seán Allan and John Sandford wrote that in the film, the blame for Hitlers rise was "laid solely on the Social-Democrats", thus justifying the KPDs Stalinist line and its rivalry with the SPD before 1933.  The film has several other fictional details presented as historical occurrences: While a Soviet ship named Karl Liebknecht existed, it was the flagship of the Caspian Sea Fleet and never reached Hamburg.  There was no 143rd Tank Guards Division or any other military formation named after Thälmann in the Red Army. 

===Cultural impact===
Mandatory screenings of both parts continued to be held in factories and collective farms years after their release. The films became part of the curriculum in the East German education system, and all the pupils watched them in school.  Footage from the movies was used to make eight short films, with lengths ranging from 8 to 27 minutes, that were shown to young children.  It held a particularly significant status in the Ernst Thälmann Pioneer Organisation; in 1979, the movements manual still listed the film as an important source of information about Thälmanns life. 

==See also==
*Karl Liebknecht (film)

==References==
 

==Bibliography==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

==Annotations==
 

==External links==
* 
* .
*  and   on DEFA Sternstunden.

 

 
 
 
 
 
 
 
 
 
 