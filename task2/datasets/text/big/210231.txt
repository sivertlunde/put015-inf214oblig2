The Unsinkable Molly Brown (film)
 
{{Infobox film
| name           = The Unsinkable Molly Brown
| image          = The Unsinkable Molly Brown.jpg
| caption        = Theatrical release poster
| director       = Charles Walters
| producer       = Lawrence Weingarten
| writer         = Helen Deutsch Based on the libretto by Richard Morris
| starring       = Debbie Reynolds Harve Presnell Ed Begley
| music          = Meredith Willson
| cinematography = Daniel L. Fapp
| editing        = Frederic Steinkamp
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 128 minutes
| country        = United States
| language       = English
| gross          = $11,070,559 
}}
 book of The Unsinkable Mary Poppins.

==Plot== Molly Brown is determined to find a wealthy man to marry. She journeys to Leadville, Colorado and is hired as a saloon singer by Christmas Morgan. After miner Johnny Brown renovates his cabin, the two wed, and he sells his claim in a silver mine for $300,000. Soon after the money Molly hid in the stove accidentally is burned, Johnny discovers the richest gold vein in state history. 

The Browns and Shamus move into a Denver mansion, and Molly sets out to improve her social status by trying to ingratiate herself with the citys elite, all of whom snub her and her nouveau riche ways. She and Johnny go to Europe, where they are embraced by royalty, and the couple return to Denver with their new friends. Mollys plan to introduce them to the people who formerly rejected her is derailed by Johnnys rough and tumble friends, whose unexpected and boisterous arrival ruins the gala party Molly is hosting.

Molly decides to return to Europe, leaving Johnny behind. She initially falls for the charms of Prince Louis de Lanière, but eventually decides she prefers to live with Johnny in Leadville. For the first time in her life, she realizes that someone elses feelings and priorities need to be considered. Setting sail for home aboard the RMS Titanic|Titanic, she becomes a heroine when the ship sinks and she helps rescue many of her fellow passengers. When her deed makes international headlines, Molly is welcomed home by Johnny and the people of Denver.

==Production== Broadway cast Tony Award MGM executives wanted Shirley MacLaine for the film. After she signed, producer Hal Wallis claimed she was under contract to him, and MacLaine was forced to withdraw from the project. When Debbie Reynolds was cast instead, MacLaine publicly accused her of agreeing to accept a lower salary in order to land the role, and director Charles Walters, who preferred MacLaine, tried to persuade Reynolds to turn down the part. 

Exteriors were filmed in the Black Canyon of the Gunnison National Park in western Colorado.

Only five of the seventeen musical numbers from the stage musical were used in the film, and Meredith Willson wrote "Hes My Friend" to extend the song score.  Peter Gennaro, who had choreographed the original Broadway production, staged the musical sequences. 

The film was the  .    

==Cast==
*Debbie Reynolds ..... Molly Brown
*Harve Presnell ..... Johnny Brown
*Ed Begley ..... Shamus Tobin
*Jack Kruschen ..... Christmas Morgan
*Hermione Baddeley ..... Mrs. Grogan
*Vassili Lambrinos ..... Prince Louis de Lanière
*Martita Hunt ..... Grand Duchess Elise Lupovinova
*Harvey Lembeck ..... Polak

==Musical numbers==
*"Belly Up to the Bar, Boys" ... Shamus Tobin, Christmas Morgan, Molly Brown, and Ensemble
*"I Aint Down Yet" ... Molly Brown
*"Colorado, My Home" ... Johnny Brown
*"Ill Never Say No" ... Johnny Brown and Molly Brown
*"Hes My Friend" ... Molly Brown, Johnny Brown, Mrs. Grogan, Grand Duchess Elise Lupovinova, Shamus Tobin, Christmas Morgan, and Ensemble
*"Johnnys Soliloquy" ... Johnny Brown

==Critical reception==
A.H. Weiler of The New York Times called the film "big, brassy, bold and freewheeling" but added, "The tones are ringing, but often hollow. Molly is a colorful character all right, and the screen, which is as wide as can be, is filled with vivid colors that help project the fact that this is merely a satisfying musical comedy and not an inspired subject." He continued, "This is not to say that Meredith Willsons score is not tuneful and lilting but to this listener it is good, sweet corn that is more palatable than memorable. Mr. Gennaro, on the other hand, has devised dances that more than complement Mr. Willsons music. They may seem to be improvised but they have the true marks of professionalism in their carefully plotted verve, bounce and exuberance." He concluded, "The Unsinkable Molly Brown, in the person of Miss Reynolds, and the other principals, often mistakes vigor for art. But Metros lavish and attractive production numbers make up for this basic superficiality. For all of its shallowness, Molly is a cheerful and entertaining addition to the local screen scene". 

Variety (magazine)|Variety observed, "In essence, its a pretty shallow story since the title character, when you get right down to it, is obsessed with a very superficial, egotistical problem beneath her generous, razzmatazz facade. On top of that, Wilsons score is rather undistinguished. Debbie Reynolds thrusts herself into the role with an enormous amount of verve and vigor. At times her approach to the character seems more athletic than artful. Harve Presnell . . . makes a generally auspicious screen debut as the patient Johnny. His fine, booming voice and physical stature make him a valuable commodity for Hollywood". 

Channel 4 called it an "amiable comedy with a handful of good tunes" that "lacks the satirical bite which its story may suggest. Sometimes the director seems to feel more at ease with the melodramatic moments than the comedy ones". 

Time Out London noted, "As ebulliently energetic as ever, Reynolds makes the brash social climbing both funny and touching, but the film itself gets trapped in two minds between satire and sentimentality. The score . . . though pleasant, is rather thinly spread; but the sets are a delight in the best traditions of the MGM musical, and Walters does a wonderfully graceful job of direction". 

TV Guide rated the film three out of four stars and commented, "A rambunctious and spirited effort from Reynolds . . . saves this otherwise weakly scripted, familiar musical from the long list of forgotten pictures".  

==Awards and nominations== Best Actress; George Davis, Best Color Best Color Best Color Best Music, Best Sound.   

The film was nominated for the Golden Globe Award for Best Motion Picture – Musical or Comedy and Reynolds was nominated for the Golden Globe Award for Best Actress – Motion Picture Musical or Comedy.

Helen Deutsch was nominated for the Writers Guild of America Award for Best Written American Musical.

==DVD release==
Warner Home Video released the Region 1 DVD on September 19, 2000. The film is in anamorphic widescreen format with an audio track in English and subtitles in English and French.

==References==
 

== External links ==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 