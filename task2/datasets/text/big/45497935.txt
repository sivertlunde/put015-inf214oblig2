The Broadway Malady
{{Infobox Hollywood cartoon|
| cartoon_name = The Broadway Malady
| series = Krazy Kat
| image = 
| caption = 
| director = Manny Gould Ben Harrison
| story_artist = Manny Gould Jack Carr
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| studio = 
| distributor = Columbia Pictures 
| release_date = April 18, 1933
| color_process = Black and white
| runtime = 5:51 English
| preceded_by = Bunnies and Bonnets Russian Dressing
}}

The Broadway Malady is a short animated film distributed by Columbia Pictures, and stars Krazy Kat. The title is derived from the 1929 feature film The Broadway Melody. The cartoon, however, makes no references to the feature film.

==Plot==
At a subway station, Krazy tries to get a ride on a train. But before he could get aboard, numerous commuters come and quickly fill the carriages. By the time the commuters are in, Krazy is flat on his front. A station worker tries to help him by pushing those inside to make more space. But as Krazy attempts to come in, he is overtaken by more outside commuters. The jam-packed train leaves the station.

Another train stops by moments later. This time Krazy chews some garlic pieces and blows a cloud wall which keeps the incoming commuters at bay. With this, Krazy boards the train with ease. But as he takes a seat, the commuters quickly come in until Krazy gets pushed out the door. Thankfully the station worker is generous to slip him back in. Krazy finally gets to ride as that train departs.

Krazys train is so full that its cars bulge almost to the point of bursting. Nevertheless it remains in one piece. And although he manages to get a ride, Krazy has to avoid getting smushed by the passengers. When the train reaches its destination, the passengers come out flat, and a station employee has to inflate them with a pump. Krazy is in a worse situation as he is in pieces similar to a jigsaw puzzle. Fortunately, the station employee puts him back together on time.

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 
 
 

 