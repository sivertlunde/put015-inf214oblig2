Dany (film)
{{Infobox film
| name = Dany
| image = Danny (film).jpg
| caption = Film poster
| writer = T. V. Chandran
| producer = T. V. Chandran
| starring = Mammootty, Mallika Sarabhai, Vani Viswanath, Siddique (actor)|Siddique, Vijayaraghavan (actor)|Vijayaraghavan, Ratheesh, Maya Moushmi, Narendra Prasad
| director = T. V. Chandran
| cinematography = K. G. Jayan
| editing = Venugopal Johnson
| associate director = Yadavan Chandran
| studio = Film Commune
| runtime = 120 minutes
| language = Malayalam
| released =  
| country = India
}}
 National Film Award and three Kerala State Film Awards.

==Plot==
The film is about Daniel Thompson, a saxophone player who is a mute witness to many of the historical happenings taking place around in the world. The film traces the life of this character up to his seventy third year and comments upon many things that may have social and political relevance.  Dany as he is known, was born on the day of the Dandi Salt March. His wife leaves him on the day the first Communist Government of Kerala loses power. Danys love, disappointments, triumphs everything coincides with historical events. Dany dons many roles in life. He sings in the church. Then he becomes a saxophone player. He marry Margaret, daughter of a rich man named Chavero when the old man ask him to save the honor of their family. Margaret is pregnant and her lover has died. But with the passage of time, Dany is abandoned by all and he finds himself destined to lead a desolate existence. He ends up in a hospital. And it is here that he meets Bhargavi Amma, a retired professor, who is also desolate after the death of her daughter and after being totally isolated in life. Dany and Bhargavi Amma develop an intimacy and they travel back together from the hospital. But Dany passes away on the way. Anyhow Bhargavi Amma decides to perform the funeral rites in the Christian manner in the compound of her house, a house of orthodox Hindus. This creates certain problems, but Bhargavi Amma decides to dare all such problems.

==Cast==
* Mammootty as Daniel Thompson aka Dany
* Mallika Sarabhai as Bhargavi Amma
* Vani Viswanath as Margarette, Danys wife Siddique as Freddy Vijayaraghavan as Robert, Danys son.
* Maya Moushmi as Louisiana, Daughter of Dany and Clara
* Ratheesh as Dr. Renji Thomas
* Narendra Prasad as Fr. Simon
* N. F. Varghese as Prof. Padmanabha Menon
* Poornima Mohan as Madhavi
* Urmila Unni as Jayalakshmi
* Devadevan as younger Robert and Albert, Roberts son
* Aliyar as Narayanan Nair Irshad as Murukan
* Sona Nair as Anna
* Reena
* Sivaji as Ramabhadran
* P. Sreekumar as Chavaro

==Awards== National Film Awards  Best Feature Film in Malayalam - T. V. Chandran
; Kerala State Film Awards  Best Director - T. V. Chandran Best Photography - K. G. Jayan Best Processing Chitranjali

==References==
 

==External links==
*  
*   at the Malayalam Movie Database
* Unni R. Nair. (June 7, 2001).  . Screen India
*  . Mathrubhumi.

 
 

 
 
 
 