Beach Red
{{Infobox film
| name           = Beach Red
| image          = BeachRed poster.jpg
| caption        = Theatrical release poster
| director       = Cornel Wilde
| producer       =
| writer         = Story: Peter Bowman Screenplay: Clint Johnston, Don Peters, Cornel Wilde Jaime Sánchez
| music          =
| cinematography = Cecil R. Cooney
| editing        = Frank P. Keller
| distributor    = United Artists
| released       = 1967
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         =
}}

Beach Red is a 1967 World War II film starring Cornel Wilde (who also directed) and Rip Torn. The film depicts a landing by the U.S. Marine Corps on an unnamed Japanese held Pacific island, however is thought to be Red Beach, Palo, Leyte in the Philippines. A reference to the recent Bougainville Campaign early in the film presumably dates the action to November 1943 or later. During World War II Allied amphibious operations, designated invasion beaches were code named by colour; such as Beach Red, Beach White, Beach Blue etc.

The film is based on a lengthy piece of prose, not quite a novel, written by Peter Bowman and based on his experiences with the US Army Corps of Engineers in the Pacific Islands campaigns.

==Plot== Tom Leas painting The Price.
 voice over stream of consciousness style is also used to portray thoughts of numerous characters. Like Wildes previous production of The Naked Prey the film does not use subtitles for characters speaking Japanese.

The film contains large sections of voice over sections, often juxtaposed with still photographs of wives etc. (who inexplicably are dressed in 1967 attire rather than WW2). It contains a huge amount of crying by the troops, and an abnormal sympathy for the enemy. In one scene an injured marine is lying close to an injured Japanese soldier in a scene paralleling the famous scene in All Quiet On The Western Front just after they bond, other marines appear and kill the Japanese soldier causing distress to the first marine.
 Captain the company commander, Rip Torn plays his believable company gunnery sergeant who says the films tagline "Thats what were here for.  To kill.  The rest is all crap!" .

==Production== infiltrate American lines. 
*When seeking assistance from the U.S. Marine Corps, Cornel Wilde was told that due to the commitments of the Vietnam War all the Corps could provide the film was color stock footage taken during the Pacific Island campaigns.  The film provided had deteriorated so Wilde had to spend a considerable part of the films budget to restore the film to an acceptable quality in order to blend into the film.  The Marine Corps was grateful that their historical film had been restored at no cost to them. 

I    was in the US Navy stationed in the Philippines when this was filmed. Many US sailors appeared in the movie. It was the only time I ever disembarked down a cargo net from a LST into a LSVP. I wasnt there when the scenes on land were filmed. Maybe there were no US personnel in those scenes.

==Soundtrack==
The film only has one musical theme, a song by written Antonino Buenaventura that appears in the titles sung in a folk song manner by Jean Wallace and appears in various orchestrations throughout the film.  The titles of the film are various paintings that suddenly segues into the preparations for the landing.

==Quotes==
Nobodys talking themselves red, white and blue in the face and only the sea is behind you if you turn. Its just you and your firearm, the enemy and his, and a perfectly democratic opportunity to use your own judgement.- Bowman, Peter Beach Red: A Novel Random House, 1945

==Notes==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 