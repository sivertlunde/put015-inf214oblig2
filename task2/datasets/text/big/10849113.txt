Punk: Attitude
{{Infobox film
 | name        = Punk: Attitude
 | director    = Don Letts
 | image       = Punk- Attitude FilmPoster.jpeg
 | writer      =  Mick Jones, Jello Biafra, Darryl Jenifer
 | distributor = 
 | budget      = 
 | released    = 2005
 | runtime     = 
 | language    = English
 }}
Punk: Attitude is a film by Don Letts. {{cite video people  Mick Jones, Jello Biafra, Darryl Jenifer  date      = 2005 title     = Punk: Attitude  medium    = Documentary, Rockumentary publisher = Freemantlemedia 3DD Metropolis location  =  isbn      =  oclc      =  quote     =   genre and following from its beginning in the mid-1970s up to its effect on modern rock music and other genres. The cast is a veritable list of alternative musicians and directors offering their opinions on what may have been the largest music revolution ever.

The film was officially released on the 25th of April 2005 at the Tribeca Film Festival in the U.S.A. Reviews have generally been favorable with an average of 3.5 - 4 stars with many people commenting on the accuracy and approach of the film.

== Plot == gigs and band meetings highlight the aggression and destructive entities with surprising accuracy. The movie wraps up by emphasizing the influence that punk has on modern music.

== Cast == Mick Jones, Jello Biafra, Siouxsie Sioux, and Darryl Jenifer.

== DVD Releases == Capitol Entertainment but has since gone out of print. 

On January 11, 2010 Shout! Factory re-released the film complete with all the original bonus material as well as another DVD worth of extras. 

== References ==
 

== External links ==
*  
*   review by Rolling Stone  
*   review by Contact Music

 
 
 


 