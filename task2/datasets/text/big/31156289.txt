Vidhyarthikale Ithile Ithile
{{Infobox film 
| name           = Vidhyarthikale Ithile Ithile
| image          =
| caption        = John Abraham
| producer       = Minnal
| writer         = John Abraham M Azad (dialogues)
| screenplay     = M Azad Madhu Jayabharathi Adoor Bhasi Paul Vengola
| music          = MB Sreenivasan
| cinematography = K Ramachandrababu
| editing        = Ravi
| studio         = Mehboob Movies
| distributor    = Mehboob Movies
| released       =  
| country        = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, John Abraham and produced by Minnal. The film stars Madhu (actor)|Madhu, Jayabharathi, Adoor Bhasi and Paul Vengola in lead roles. The film had musical score by MB Sreenivasan.   

==Cast==
  Madhu
*Jayabharathi
*Adoor Bhasi
*Paul Vengola
*Manorama
*Master Vijayakumar
*Paravoor Bharathan
*Ranga Rao
*S. P. Pillai
*T. K. Balachandran
*MRR Vasu
 

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chinchilam Chiluchilam || Adoor Bhasi, Manorama || Vayalar Ramavarma || 
|-
| 2 || Nalanda Thakshashila   || S Janaki, Chorus || Vayalar Ramavarma || 
|-
| 3 || Nalanda Thakshashila   || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 4 || Velichame Nayichaalum || S Janaki, Chorus || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 

 