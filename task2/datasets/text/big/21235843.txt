The Village (1953 film)
{{Infobox film
| name           = The Village
| image	         = The Village FilmPoster.jpeg
| caption        = 
| director       = Leopold Lindtberg
| producer       = Kenneth L. Maidment Lazar Wechsler Elizabeth Montagu David Wechsler
| starring       = John Justin
| music          = 
| cinematography = Emil Berna
| editing        = Gordon Hales
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Switzerland
| language       = German
| budget         = 
}}

The Village ( ) is a 1953 Swiss drama film directed by Leopold Lindtberg.

==Cast==
* John Justin - Alan Manning
* Eva Dahlbeck - Wanda Piwonska
* Sigfrit Steiner - Heinrich Meile
* Mary Hinton - Miss Worthington
* W. Woytecki - Dr. Stefan Zielinski
* Guido Lorraine - Mr. Karginski
* Maurice Régamey - Mr. Faure
* Helen Horton - Miss Sullivan
* Roland Catalano - Signore Belatti
* Krystina Bragiel - Anja
* Voytek Dolinski - Andrzej Trevor Hill - Michael
* David Coote - Pinky (uncredited)

==Awards==
;Won
* 3rd Berlin International Film Festival - Bronze Berlin Bear   

;Nominated
* 1953 Cannes Film Festival - Palme dOr     

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 