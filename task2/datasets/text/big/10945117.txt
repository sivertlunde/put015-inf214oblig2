The Salon (film)
{{Infobox film
| name           = The Salon
| image          = Salon_ver2.jpg
| caption        = Promotional poster
| director       = Mark Brown
| producer       = David Odom Breht Gardner Brent Odom Carl Craig David Odom Lita Richardson Mark Brown Vivica A. Fox Zatella Beatty
| writer         = Mark Brown Shelley Garrett
| starring       = Vivica A. Fox Darrin Henson Dondre Whitfield Kym Whitley Monica Calhoun
| cinematography = Brandon Trost
| editing        = Earl Watson
| distributor    = CodeBlack Entertainment Freestyle Releasing
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| gross          = $139,084 
}} directed by Mark Brown, executive produced by David Odom, and starring Vivica A. Fox, Kym Whitley, and Monica Calhoun. The movie was filmed in Baltimore, Maryland|Baltimore, Maryland.

==Plot==
The Salon is an independent feature film that was developed, financed and produced by Howard University grads David Odom and Mark Brown.  In the movie, Jenny Smith (Vivica A. Fox|Fox) owns a modest neighborhood beauty parlor that is hugely popular with the folks who reside on her street, but mom-and-pop businesses are going belly-up all over the place and lately a corporate giant has been clamoring to set up shop on the block. Despite formidable pressure from the Department of Water and Power, Jenny refuses to accept the offer made for her shop and decides to test her luck against the DWP in the local courthouse.

Tagline: Where you get more than just a hair cut!

==Cast==
*Vivica A. Fox &mdash; Jenny
*Brooke Burns &mdash; Tami
*Darrin Henson &mdash; Michael
*DeAngelo Wilson &mdash; D.D.
*Dondre Whitfield &mdash; Ricky
*Garrett Morris &mdash; Percy
*Kym Whitley &mdash; Lashaunna
*Monica Calhoun &mdash; Brenda
*Sheila Cutchlow &mdash; Kandy
*Taral Hicks &mdash; Trina
*Terrence Howard &mdash; Patrick
*Tiffany Adams &mdash; Wanda

==See also==
* Barbershop (film)|Barbershop (film)
* Beauty Shop :D

==References==
  
  tags!-->

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 