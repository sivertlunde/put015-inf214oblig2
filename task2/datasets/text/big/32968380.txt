Six of a Kind
{{Infobox film name           = Six of a Kind image          = caption        = director       = Leo McCarey producer       = writer         = Keene Thompson Douglas MacLean Walter DeLeon Harry Ruskin starring       = Charles Ruggles Mary Boland W.C. Fields George Burns music          = cinematography = Henry Sharp editing        = LeRoy Stone distributor    = Paramount Pictures released       =   runtime        = 62 minutes country        = United States language       = English budget         = gross          =
}}

Six of a Kind is a 1934 comedy film directed by Leo McCarey. It is a whimsical and often absurd Road movie about two couples who decide to share their expenses on a trip to Hollywood. 

==Cast==
*Charles Ruggles (as Charlie Ruggles) - J. Pinkham Whinney
*Mary Boland - Flora Whinney
*George Burns - George Edward
*Gracie Allen - Gracie Devore
*W.C. Fields - Sheriff John Hoxley
*Alison Skipworth - Mrs. K. Rumford
*Phil Tead - Clerk in Newspaper Office

==External links==
*  

 
 
 
 
 

 