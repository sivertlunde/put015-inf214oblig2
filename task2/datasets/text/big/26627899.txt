Mohabbat (1997 film)
{{Infobox film
| name           = Mohabbat
| image          = Mohabbat.jpg
| director       = Reema Rakesh Nath
| producer       =
| writer         = Reema Rakesh Nath
| starring       = Madhuri Dixit Akshay Khanna Sanjay Kapoor
| music          = Nadeem Shravan
| cinematography =
| editing        =
| distributor    =
| released       = September 19, 1997 
| runtime        = 180 mins
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Romantic Hindi film directed by Reema Nath and starring Madhuri Dixit, Sanjay Kapoor and
Akshay Khanna.  The film released on September 19, 1997 and was a flop at the box-office. 

==Plot==
The wealthy Kapoor family consists of Madanlal, his wife Geeta, daughter Roshni, and son, Gaurav (Sanjay Kapoor). One day while Gaurav is returning home from a bank, he is attacked by a group of men led by Shiva (Shiva Rindani), but a young man named Rohit Malhotra (Akshaye Khanna) comes to his rescue. Gaurav hires Rohit in his firm, and both become fast and inseparable friends. Both unknowingly fall in love with the same woman, Shweta Sharma (Madhuri Dixit), but Gaurav finds out and decides to step away. Shweta and Rohit are in love and want to get married. Then Shiva attacks Rohit and throws him off a cliff. Believing him to be dead, a shocked and devastated Shweta loses her voice. The Kapoors find out that Gaurav loves Shweta and they approach her brother, Shekhar (Farooq Sheikh), and arrange their marriage. They get engaged and Gaurav finds a lookalike of Rohit who is Tony Braganza.
What Gaurav doesnt know is that if Tony is Rohit or not. Thus begins a merry go-round of love, emotions and sacrifice, all of which is Mohabbat

==Cast==
*Madhuri Dixit as Shweta Sharma
*Sanjay Kapoor as Gaurav M. Kapoor
*Akshay Khanna as Rohit Malhotra / Tony Braganza
*Farooq Sheikh as Shekhar Sharma
*Shiva Rindani as Shiva
*M.F. Husain as Himself (Special Appearance) Arjun as Goon

==Music==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Title !! Singer(s)
|-
| "Aina Bataa Kaise"
| Vinod Rathod & Sonu Nigam
|-
| "O Baby Dont Break My Heart " Abhijeet & Kavita Krishnamurthy
|-
| "Chori Chori Chup Chup"
| Kavita Krishnamurthy
|-
| "Dil Ki Dhadkan"
| Udit Narayan & Kavita Krishnamurthy
|-
| "Pyar Kiya Hai"
| Vinod Rathod & Kavita Krishnamurthy
|-
| "Main Hoon Akela" Abhijeet & Kavita Krishnamurthy
|}

==References==
 
 

==External links==
*  at the Internet Movie Database

 
 
 
 