Square Grouper: The Godfathers of Ganja
Square Grouper: The Godfathers of Ganja is a 2011 documentary by director Billy Corben (Cocaine Cowboys) and produced by Alfred Spellman and Billy Corben through their Miami-based media studio Rakontur. The term square grouper was a nickname given to bales of marijuana thrown overboard or out of airplanes in South Florida in the 70s and 80s. 

In sharp contrast to the brazenly violent "Cocaine Cowboys" of the 1980s, Miamis marijuana smugglers were cooler, calmer, and typically nonviolent. Square Grouper paints a vivid portrait of Miamis pot smuggling culture in the 70s and 80s and its major players: the smuggling Black Tuna Gang, the pot dealing Ethiopian Zion Coptic Church and the tiny fishing village Everglades City. 

==Synopsis==
In 1979, the United States Customs Service|U.S. Customs Service reported that 87% of all marijuana seizures in the U.S. were made in the South Florida area. Due to the regions 5,000 miles of coast and coastal waterways and close proximity to the Caribbean and Latin America, South Florida was a pot smugglers paradise. Square Grouper: The Godfathers of Ganja is a colorful portrait of Miamis pot smuggling scene of the 1970s, populated with redneck pirates, a ganja-smoking church, and the longest serving marijuana prisoner in American history.  

===The Ethiopian Zion Coptic Church===
In the early 1970s, a fundamentalist Christian sect known as the Ethiopian Zion Coptic Church formed in Jamaica. The Coptics beliefs were typical of any fundamentalist Christian organization…with the exception of one.  The Church believed that marijuana (or "ganja," as they called it) was their sacrament…and all members, including children, smoked it around the clock.  The Church started a massive marijuana export operation and expanded throughout the 70s, eventually becoming the largest employers and landowners in the struggling Caribbean nation.
 Star Island.  Initially, the Church received recognition as a legitimate religious organization by the government.  But as the media caught wind of the group and their rather unorthodox religious ceremonies, things started to change.  Ultimately, a 1979 60 Minutes piece featuring footage of young children puffing large "spliffs" of marijuana caused public outrage and compelled the government to finally put an end to the Coptics.  Soon after, many Church members were indicted and eventually convicted of smuggling large quantities of marijuana.  

===The Black Tuna Gang===
Robert Platshorn and Robert Meinster moved to Miami to get in on the lucrative marijuana business.  They started to smuggle small loads from Colombia to the U.S. and used the Fontainebleau Hotel for their base of operations.  At the same time, the Drug Enforcement Administration were facing budget crises and possible dissolution and the film makes the claim that they used Platshorn and Meinster as their targets to justify their continued existence. 

On May 1, 1979, Attorney General Griffin Bell held a press conference announcing the indictment of Platshorn and Meinster and several of their associates, labeling them as the "biggest marijuana smugglers ever", and claiming they were responsible for importating at least a million pounds of marijuana—ten times the amount the organization actually moved. Platshorn was sentenced to 64 years in prison and Meinster to 54, making them the longest serving prisoners related to marijuana convictions in American history. 

===Everglades City===
Everglades City, a tiny fishing village 80 miles west of Miami, has always been outlaw country.  The majority of the towns 500 residents are from five families, with almost everyone related in some way.  The regions coastline is a vast labyrinth of mangroves known as the Ten Thousand Islands.  The unique geography, coupled with the fact that only locals knew how to navigate it, made the town a perfect location for smuggling. 

Smuggling activity in Everglades City had a long history.  In the early 1900s, they smuggled endangered animals.  During the prohibition era, they were involved rum running.  When drugs flooded South Florida in the 70s and 80s and the National Park Service began to phase out commercial fishing, the mainstay of the Everglades City economy, residents involved themselves in marijuana smuggling. 
  
The DEA decided they had to put a stop to the smuggling.  They executed two large, highly publicized raids in 1983 and 1984, leading to the arrest of nearly 80% of the adult male population of Everglades City.  

==Distribution==

Square Grouper premiered at the 2011 SXSW film festival  and was distributed by Magnolia Pictures. 


== References ==
 

== External links ==
* 
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 