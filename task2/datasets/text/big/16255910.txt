All My Friends Are Leaving Brisbane
 
{{Infobox film
| name           = All My Friends Are Leaving Brisbane
| image          = All My Friends Are Leaving Brisbane Poster.jpg
| caption        = Official Production Poster
| director       = Louise Alston
| producer       = Louise Alston   Jade van der Lei
| writer         = Stephen Vagg
| based on = play by Stephen Vagg Cindy Nelson Ryan Johnson   Romany Lee   Gyton Grantley   Sarah Kennedy
| music          = Caitlin Yeo
| cinematography = Judd Overton
| editing        = Nicola Scarrott
| distributor    = Accent Film
| released       = October 2007 (AUS)
| runtime        = 76 minutes
| country        = Australia
| awards         =
| language       = English
| budget         =  
| gross          =
| preceded by    =
| followed by    =
}} 

All My Friends Are Leaving Brisbane is a 2007 Australian romantic comedy film directed by Louise Alston and written by Stephen Vagg. It follows Anthea, a 25-year-old girl who hates her job and has to sit back and watch as all her friends move away from her hometown, Brisbane, to make a better life. In 2013, The Guardian referred to it as a "cult film" inspired by "a typically Brisbane lament... the departure of people in their late 20s to Sydney, Melbourne, London or New York." 

==Synopsis==
Anthea (Charlotte Gregg) is undergoing a crisis of confidence: overworked, no boyfriend, and now all her friends are leaving Brisbane. She is tempted to leave herself, but is opposed by her longtime best platonic male friend Michael (Matt Zeremes).

Michael thinks people who leave Brisbane are copycats who follow the crowd; he is quite happy to stay in Brisbane, he is in a stable job and a stable very low-maintenance "sex-with-the-ex" relationship with his ex-girlfriend, Stephanie (Sarah Kennedy). In short, he is in a rut.

Antheas temptation to leave Brisbane increases with the impending departure of her flatmate Kath (Cindy Nelson). However, she then hears that her ex-boyfriend Jake (Gyton Grantley) is coming back to Brisbane to live. To Michaels annoyance, she dreams of a great future with him.

Michael is then thrown out of his comfort zone by starting a new relationship with a girl he meets at work; Simone (Romany Lee). Slightly "alternative" and good natured, Simone is totally different from the sorts of girls he normally deals with, and he finds himself in a relationship over which he does not have total control.

On her last day in Australia, Anthea and Michael finally resolve their feelings for each other.

==Cast==
*Charlotte Gregg as Anthea
*Matt Zeremes as Michael Ryan Johnson as Tyson Cindy Nelson as Kath
*Romany Lee as Simone
*Sarah Kennedy as Stephanie
*Francesca Gasteen as Natalie
*Micheal Priest as Aram

==Production==
Originally, All My Friends are Leaving Brisbane was a stage performance at the University of Queenslands Cement Box Theatre, where director Louise Alston first saw it. She could see that the story would make an ideal feature film and worked with writer Stephen Vagg on developing a script.

Producer Jade van der Lei then became interested in the film and was able to raise a budget of  . The film was shot in the middle of a Queensland summer, January 2006, over a three-week period. Afterward, The filmmakers successfully applied for post-production funding from the Australian Film Commission, which enabled additional shooting. The film completed post-production in early 2007 and made its world debut at the 2007 Brisbane International Film Festival. 
 New Farm, Kangaroo Point, Royal Exchange Hotel, and refers to Brisbane artists such as Powderfinger, Nick Earls, and John Birmingham. The soundtrack includes a cover version of "Streets of Your Town" from Brisbane band The Go-Betweens by Brisbane singer Dave McCormack.

==Release==
All My Friends Are Leaving Brisbane was released on 27 May 2008 in Australia through Accent Film. 

===Box office===
All My Friends Are Leaving Brisbane took $42,524 at the box office in Australia,  which is equivalent to $48,135 in 2009 dollars.

===Adaptation===
Stephen Vagg wrote an adaptation of the original play which relocated the action to Adelaide. Retitled All My Friends Are Leaving Adelaide, it premiered at the 2012 Adelaide Fringe Festival. 

===Awards=== AFI Awards. 

===Connection to Jucy=== Cindy Nelson, Ryan Johnson, and Charlotte Gregg.

==References==
 

==External links==
* 
* 
*  at Urban Cinefile

 
 
 
 
 
 
 