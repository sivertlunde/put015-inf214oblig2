The Trial (1962 film)
{{Infobox film
| name           = The Trial
| image          = TheTrialDVDCover.jpg
| caption        = 
| screenplay     = Orson Welles
| based on       = The Trial by Franz Kafka
| starring       = Anthony Perkins Orson Welles Jeanne Moreau Romy Schneider Akim Tamiroff Elsa Martinelli
| director       = Orson Welles Edmond Richard
| producer       = Alexander Salkind
| distributor    = Astor Pictures Corporation
| released       =  
| runtime        = 118 minutes
| country        = France Italy Germany
| language       = English
| budget         = United States dollar|US$1.3 Million gross = 998,779 admissions (France)   at Box Office Story 
}}
The Trial is a  " to pinscreen scenes created by the artist Alexandre Alexeieff. Anthony Perkins stars as Josef K., a bureaucrat who is accused of a never-specified crime, and Jeanne Moreau, Romy Schneider, and Elsa Martinelli play women who become involved in various ways in Josefs trial and life. Welles plays the Advocate, Josefs lawyer and the films principal antagonist.

The Trial has grown in reputation over the years, and some critics, including Roger Ebert, have called it a masterpiece.  It is often praised for its scenic design and cinematography, the latter of which includes disorienting camera angles and unconventional use of focus (optics)|focus. 

== Plot ==
Josef K. (Anthony Perkins) is sleeping in his bedroom, in an apartment he shares with other lodgers. He is awakened when a man in a suit opens his bedroom door. Josef assumes the glib man is a policeman, but the intruder does not identify himself and ignores Josefs demand to produce police ID. Several detectives enter and tell Josef he is under open arrest.  In another room Josef K. sees three co-workers from his place of employment; they are there to provide evidence regarding some unstated crime. The police refuse to inform Josef K. of his misdeeds, or if he is even being charged with a crime, and they do not take him into custody.

After the detectives leave, Josef converses with his landlady, Mrs. Grubach (Madeleine Robinson), and neighbor, Miss Burstner (Jeanne Moreau), about the strange visit. Later he goes to his office, where his supervisor thinks he has been having improper relations with his teenaged female cousin.  That evening, Josef attends the opera, but is abducted from the theater by a police inspector (Arnoldo Foà) and brought to a courtroom, where he attempts in vain to confront the still-unstated case against him.

Josef returns to his office and discovers the two police officers who first visited him being whipped in a small room.  Josef’s uncle Max suggests that Josef consult with Hastler (Orson Welles), a law advocate.  After brief encounters with the wife of a courtroom guard (Elsa Martinelli) and a roomful of condemned men awaiting trial, Josef is granted an interview with Hastler, which proves unsatisfactory.

Hastler’s mistress (Romy Schneider) suggests that Josef seek the advice of the artist Titorelli (William Chappell), but this also proves unhelpful.  Seeking refuge in a cathedral, Josef learns from a priest (Michael Lonsdale) that he has been condemned to death. Hastler abruptly appears at the cathedral to confirm the priest’s assertion.

On the evening before his thirty-first birthday, Josef is apprehended by two executioners and brought to a quarry pit, where he is forced to remove some of his clothing.  The executioners pass a knife back and forth, apparently deliberating on who will do the deed, before handing the knife to the condemned man, who refuses to commit suicide.  The executioners leave Josef in the quarry and toss dynamite in the pit.  Josef laughs at his executioners and picks up the dynamite. From a distance there is an explosion and smoke billows into the air. Cowie, Peter. "The Cinema of Orson Welles." 1973, A.S. Barnes & Co. 

==Cast==

* Anthony Perkins - Josef K.
* Jeanne Moreau - Marika Burstner
* Romy Schneider - Leni
* Elsa Martinelli - Hilda
* Suzanne Flon - Miss Pittl
* Orson Welles - Albert Hastler, The Advocate
* Akim Tamiroff - Bloch
* Madeleine Robinson - Mrs. Grubach
* Paola Mori - Court archivist
* Arnoldo Foà - Inspector A
* Fernand Ledoux - Chief Clerk of the Law Court
* Michael Lonsdale - Priest
* Max Buchsbaum - Examining Magistrate
* Max Haufler - Uncle Max
* Maurice Teynac - Deputy Manager
* Wolfgang Reichmann - Courtroom Guard
* Thomas Holtzmann - Bert the law student
* Billy Kearns - First Assistant Inspector
* Jess Hahn - Second Assistant Inspector
* Naydra Shore- Irmie, Joseph K.s cousin
* Carl Studer - Man in Leather
* Jean-Claude Rémoleux - Policeman #1
* Raoul Delfosse - Policeman #2 William Chappell - Titorelli

==Production==

In 1960, Welles was approached by producer Alexander Salkind to make a film from a public domain literary choice. Salkind promised that Welles would have total artistic freedom and he would not interfere with Welles’ creation. Welles and Salkind agreed to create a film based on the Franz Kafka novel The Trial, only to discover later the text was not in the public domain and that they needed to obtain the rights to the property.    Earlier that year Welless son, Michael Lindsay-Hogg, had casually mentioned an idea to Welles about adapting The Trial as a stage play, prompting Welles to state that The Trial was an important book and that he should re-read it. 

Salkind committed 650 million French francs (U.S.$1.3 million in 1962 currency) to the budget for The Trial and secured backing from German, French and Italian investors. Brady, Frank. "Citizen Welles." 1989, Charles Scribner’s Sons, ISBN 0-684-18982-8 

Welles took six months to write the screenplay. In adapting the work, he rearranged the order of Kafka’s chapters. In this version, the chapter line-up read 1, 4, 2, 5, 6, 3, 8, 7, 9, 10. However, the order of Kafkas chapters was arranged by his literary executor, Max Brod, after the writers death, and this order is not definitive. Welles also modernized several aspects of the story, introducing computer technology and changing Miss Burstner’s profession from a typist to a cabaret performer. Welles also opened the film with a fable from the book about a man who is permanently detained from seeking access to the Law by a guard. To illustrate this allegory, he used the pin screen animation of Alexandre Alexeieff, who created animated prints using thousands of pins. 

Welles also changed the manner of Josef K.s death. Kafka originally had the executioners pass the knife over the head of Josef K., thus giving him the opportunity to take the weapon and kill himself, in a more dignified manner - Josef K. does not, instead he is fatally stabbed by his executioners in the heart, and as he dies Josef K. says "like a dog." In the film, whilst the executioners still offer him the knife, Josef K. refuses to take it, and goads the executioners by yelling "Youll have to do it!"  The film ends with the smoke of the fatal dynamite blast forming a mushroom cloud in the air while Welles reads the closing credits on the soundtrack. 

Welles initially hoped to cast U.S. comic actor Jackie Gleason as Hastler, but he took the role himself when Gleason rejected the part.  Welles also dubbed the dialogue for 11 actors in The Trial. Welles reportedly dubbed a few lines of Anthony Perkins’ dialogue and challenged Perkins to identify the dubbing.  Perkins was unable to locate the lines where Welles dubbed his voice. Bogdanovich, Peter. "Who the Hell’s In It?" 2004, Alfred A.Knopf, ISBN 978-0-375-40010-0 		
 Hungarian bit-players in The Trial. Sallis pronounces the episode to be "Kafka-esque, to coin a phrase", but never does reveal if he actually did go through with the dubbing.

Welles began the production in Federal Peoples Republic of Yugoslavia|Yugoslavia. To create Josef K.’s workplace, he created a set in an exposition hall just outside Zagreb, where 850 secretaries banged typewriters at 850 office desks. Other sequences were later shot in Dubrovnik, Rome, Milan and Paris.  Welles was not able to film The Trial in Kafka’s home city of Prague, as his work was banned by the communist government in Czechoslovakia.   

In Paris, Welles had planned to shoot the interiors of his film at the Bois de Boulogne studios, but Salkind had difficulties collecting promised capital to finance the film.  Instead, he used the Gare dOrsay, an abandoned Parisian railway station. Welles rearranged his set design to accommodate this new setting, and he later defended his decision to film at Gare dOrsay in an interview with Cahiers du cinéma, where he stated: "Everything was improvised at the last moment, because the whole physical concept of my film was quite different. It was based on the absence of sets. And the gigantic nature of the sets, which people have objected to, is partly due to the fact that the only setting I had was that old abandoned station." 
 Don Quixote, to oversee the post-production work. 

In a later interview with Peter Bogdanovich, Anthony Perkins stated that Welles gave him the direction that The Trial was meant to be seen as a black comedy. Perkins would also state his greatest professional pride came in being the star of a Welles-directed feature. 

While filming in Zagreb, Welles met 21-year-old Croatian actress Olga Palinkaš. He renamed her Oja Kodar and she became Welles companion and occasional artistic collaborator during the latter years of his career. 

==Release==
 West Side Story instead. 

Welles continued to edit the film up until its December 1962 premiere in Paris. In an interview with the BBC, he mentioned that on the eve of the premiere he jettisoned a ten-minute sequence (it is actually about six minutes long) where Josef K. meets with a computer scientist (played by Greek actress Katina Paxinou) who uses her technology to predict his fate. Welles explained the last-minute cut by noting: "I only saw the film as a whole once. We were still in the process of doing the mixing, and then the premiere fell on us... It should have been the best in the film and it wasnt. Something went wrong, I dont know why, but it didnt succeed." 

Ultimately, the US theatrical release of The Trial came in 1963.

==Reception==
===Contemporary===
The Trial polarized critics upon release. Immediately after its completion, Welles said, "Say what you like, but The Trial is the best film I have ever made."  The film was reacted to more positively in France, where it won the "Best Film" award of the French Syndicate of Cinema Critics in 1964.

Charles Higham’s 1970 biography on Welles dismissed the film as "an agonizing experience ... a dead thing, like some tablet found among the dust of forgotten men." 

===Retrospective=== David Thomson said the film was "an astonishing work, and a revelation of the man ... a stunning film."  Today, the film enjoys enthused reviews on Rotten Tomatoes, with 24 out of 27 counted critics (89%) awarding the film a positive review. Prolific film critic Roger Ebert called the film "an exuberant use of camera placement and movement and inventive lighting," awarding it a full four stars. 

In the British Film Institutes 2002 Sight & Sound poll, Fernando Martin Peña voted The Trial one of his 10 favorite films. 

==Post-release history==
 Welles planned to create a documentary on the making of The Trial. Cinematographer Gary Graver was hired to film Welles addressing a University of Southern California audience on the film’s history.  The footage was shot with a 16mm camera on color reversal stock, but Welles never completed the proposed documentary. The film is now in the possession of Germany’s Filmmuseum Munich, and has since been restored. 

No copyright was ever filed on The Trial, which resulted in the film being a public domain title in the US. It is possible that the copyright was restored by the Uruguay Round Agreements Act|URAA, however, no "Notice of Intent to Enforce" was filed with the US Copyright Office. 

In 2000, a restored version based on the long-lost original 35mm negative was released on DVD by Milestone Films.  For many years, the film has been available in cheaply-made home video of inferior quality, not produced under license. In 2012, a remastered high definition version was released on Blu-ray by Studio Canal which owns the worldwide copyright. 

==See also==
* Trial film

==References==
 

==External links==
 
*  
*  
*  
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 