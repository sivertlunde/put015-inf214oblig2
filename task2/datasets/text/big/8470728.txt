Adolescencia
{{Infobox film
| name           = Adolescencia
| image          = 
| caption        = 
| director       = Francisco Múgica
| producer       = Francisco Múgica
| writer         = Carlos A. Olivari
| starring       = Pola Alonso Ana Arneodo
| music          = Enrique Delfino
| cinematography = José María Beltrán
| editing        = Juan Soffici
| distributor    = Lumiton
| released       = 11 March 1942
| runtime        = 86 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| followed_by    = 
}} Argentine film directed by Francisco Múgica and written by Carlos A. Olivari. The film starred Pola Alonso and Ana Arneodo

==Synopsis==
A girl falls in love with a boy arrived from the United States and displaces her childhood sweetheart.

==Release==
The film premiered on 11 March 1942 in Buenos Aires.

==Cast==
*Pola Alonso
*Ana Arneodo
*María Arrieta
*Alberto Contreras
*Rufino Córdoba
*Alfredo Jordan
*Mirtha Legrand
*Ángel Magaña
*Federico Mansilla
*Domingo Márquez

== External links ==
*  
 
 
 
 
 
 