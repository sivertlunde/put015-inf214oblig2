Blue (1993 film)
 
 
{{Infobox film
| name           = Blue
| image          = Blue (film).jpg
| image_size     =
| caption        = Blue field
| director       = Derek Jarman James Mackay Takashi Asai
| writer         = Derek Jarman
| narrator       = Derek Jarman Tilda Swinton Nigel Terry John Quentin
| starring       = Momus Peter Christopherson Karol Szymanowski Erik Satie
| cinematography =
| editing        = Channel Four Films BBC Radio 3 Arts Council of Great Britain
| distributor    = Basilisk Communications Ltd
| released       = Venice Biennale, June 1993, Edinburgh International Film Festival, August 1993, 3 October 1993 (New York Film Festival)
| runtime        = 79 min.
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} blind at the time of the films release.

The film was his last testament as a film-maker, and consists of a single shot of saturated blue colour filling the screen, as background to a soundtrack where Jarmans and some of his favourite actors narration describes his life and vision.
 stereo soundtrack. play and it was later released as a compact disc|CD.

The film has been released on DVD in Germany and in Italy. On 23 July 2007 British distributor Artificial Eye released DVD tying Blue together with Glitterbug, a collage of Jarmans Super 8 footage.

Cinematographer Christopher Doyle has called Blue one of his favourite films, calling it "one of the most intimate films Ive ever seen." 

==See also==
 
* List of avant-garde films of the 1990s

== External links ==
*  
*  
* 
*  

== References ==
 

 

 
 
 
 
 
 
 


 