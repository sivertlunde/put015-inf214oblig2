Owning Mahowny
{{Infobox film name = Owning Mahowny image = Owning Mahowny film.jpg caption = Owning Mahowny film poster director = Richard Kwietniowski writer =  Maurice Chauvet based on =   starring = Philip Seymour Hoffman John Hurt Minnie Driver Maury Chaykin producer = Andras Hamori Seaton McLean music = The Insects Richard Grassby-Lewis studio = Natural Nylon
|distributor= Sony Pictures Classics   Alliance Atlantis   budget = $10 million gross = $1 million
|released=   runtime = 104 minutes country = Canada United Kingdom language = English
}} gambling addiction with a cast that includes Philip Seymour Hoffman, Minnie Driver, Maury Chaykin and John Hurt. Based on the true story of a Toronto bank employee who embezzled more than $10 million to feed his gambling habit, Owning Mahowny was named one of the ten best films of the year by critic Roger Ebert.

==Plot== skimming larger and larger amounts for his own use and making weekly trips to Atlantic City, where he is treated like a king by a greedy casino manager (John Hurt). Mahownys girlfriend, fellow bank employee Belinda (Minnie Driver), can not understand what is happening. Mahownys criminal acts come to light when Toronto police begin to investigate his longtime bookie (Maury Chaykin).
 Chris Collins) put the emphasis squarely on the gambling Behavioral addiction|addiction, not on the flash and sizzle of big casinos or multi-million-dollar frauds.

==The real-life story== embezzled over $10 million from his employers in just 18 months to support his gambling habit. Molonys story was told in the best-selling 1987 book Stung by journalist Gary Ross, which formed the basis for the screenplay.

In an interview on the web site of Stung publishers McClelland and Stewart, Ross says he has kept in touch with Molony and updated what happened to him after the events portrayed in the movie. Molony served six years in prison after pleading guilty to fraud. He has not gambled since his arrest, has married his girlfriend (Belinda in the movie), has three sons and works as a financial consultant.   

==Cast==
* Philip Seymour Hoffman as Dan Mahowny
* Minnie Driver as Belinda
* Maury Chaykin as Frank Perlin
* John Hurt as Victor Foss
* Sonja Smits as Dana Selkirk
* Ian Tracey as Detective Ben Lock
* Jason Blicker as Dave Quinson Chris Collins as Bernie
* Matthew Ferguson as Martin
* Janine Theriault as Maggie
* Conrad Dunn as Edgar
* Mike "Nug" Nahrgang as Parking Attendant Philip Craig as Briggs
* Tony Munch as Observer #2
* M. J. Kang as Secretary

==Reception==
Review-aggregator site Rotten Tomatoes indicates that 79% of critics gave Owning Mahowny a favorable review, with an average rating of 7/10.  Film critic Roger Ebert named Owning Mahowny one of the top ten films of 2003,  and assessed Hoffmans performance as "a masterpiece of discipline and precision", calling him a "fearless poet of implosion,   plays the role with a fierce integrity, never sending out signals for our sympathy because he knows that Mahowny is oblivious to our presence."  However, some critics believed that in his self-effacing performance, Hoffman refused to conform to expectations of a typical movie character and that the movie suffered as a result.    Stephanie Zacharek considered him to be a "character who squirms right out of our grasp", and despite being the movies anchor, hes "such a vaporous one, he leaves us feeling adrift". 

Asked whether Philip Seymour Hoffmans portrayal matched with the real Brian Molony, journalist Gary Ross replied, "Remarkably so. They have the same stocky build, bushy moustache, glasses, slightly unkempt look, and earnestness. And Philip somehow managed to assimilate the psychic essence of Molony — a yawning emptiness that nothing except gambling was able to fill." 

Owning Mahowny earned $1 million, significantly less than its $10 million budget. 

==Accolades==
Owning Mahowny received nominations for Best Motion Picture, Best Performance by an Actor in a Leading Role, Best Screenplay (Adapted) and Best Achievement in Music — Original Score (Richard Grassby-Lewis, Jon Hassell) at the 24th Genie Awards.

==References==
 

==External links==
*  
*  
*  
*   The Torontoist, December 8, 2009

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 