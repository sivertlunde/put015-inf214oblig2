West of Tombstone
{{Infobox film
| name           = West of Tombstone
| image          = 
| alt            = 
| caption        = 
| director       = Howard Bretherton
| producer       = William Berke 
| screenplay     = Maurice Geraghty
| story          = 
| starring       = 
| music          = Jack A. Goodrich
| cinematography = George Meehan
| editing        = Mel Thorson
| studio         = Columbia Pictures
| distributor    =
| released       =  
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} American B-Western directed by Howard Bretherton starring Charles Starrett. 

== Plot ==
Marshal Steve Langdon hears a rumor that the legendary criminal Billy the Kid is maybe still alive. Langdon finds Billys coffin empty and believes that the respected older citizen Wilfred Barnet is Billy the Kid. He informs Wilfred’s lovely daughter Carol about. When Barnet confesses his true identity to his daughter and says that he got a second chance by Pat Garrett to start a new life. His clerk overhears the conversation and informs his old gang and this leads to trouble. Barnet helps Marshal Langdon against the gang. They defeat the gang but Barnet is mortially wounded. Steve decides to let the old man rest in peace and reports that Billy the Kid died long ago.

== Cast ==
* Charles Starrett as Marshal Steve Langdon
* Russell Hayden as Lucky Barnet
* Cliff Edwards as Harmony Haines
* Marcella Martin as Carol Barnet
* Gordon De Main as Wilfred Barnet
* Clancy Cooper as Dave Shurlock
* Jack Kirk as the Sheriff
* Budd Buster as Wheeler
* Tom London as Morris
* Lloyd Bridges as Martin (uncredited)

== External Links ==
*  
*  