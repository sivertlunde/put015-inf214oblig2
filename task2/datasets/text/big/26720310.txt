Hostel: Part III
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Hostel: Part III
| image          = Hostelpart3poster.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = Scott Spiegel
| producer       = Chris Briggs Mike Fleiss Scott Spiegel
| writer         = Michael D. Weiss
| based on       =  
| starring       = Kip Pardue Brian Hallisay John Hensley Sarah Habel Skyler Stone Zulay Henao Thomas Kretschmann Chris Coy
| music          = Frederik Wiedmann
| cinematography = Andrew Strahorn
| editing        = George Folsey, Jr. Brad E. Wilhite
| studio         = Next Entertainment Stage 6 Films RCR Media Group
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 88 minutes
| country        = United States of America
| language       = English Hungarian
| budget         = $6 million (estimated) 
}}
Hostel: Part III is a 2011 American horror film directed by Scott Spiegel and the third installment of the Hostel (2005 film)|Hostel film series. It was written by Michael D. Weiss, This is the first film in the series to be neither written nor directed by Eli Roth, and the first not to have a theatrical release.

The plot centers around four men attending a bachelor party in Las Vegas. While there, they are enticed by two sexy prostitutes to join them at a private party way off the Strip. Once there, they are horrified to find themselves the subjects of a perverse game of torture, where members of the Elite Hunting Club are hosting the most sadistic show in town. It was released Direct-to-video|direct-to-DVD on December 27, 2011. 

==Plot== Ukrainian couple, Victor and Anka, are currently staying. Anka and Victor go unconscious after being drugged by the beer Travis gave them.  It is then revealed that Travis is a member of the Elite Hunting Club.  Victor later wakes up in a cell in an abandoned building, and watches as two guards drag Anka out of her cell.
 escorts Carter unfaithful with Amy and almost lost her, and he doesnt want it to happen again. Scott wakes up the next morning in his hotel room with Carter and Justin. The three wonder where Mike is, as he isnt picking up his phone.
 trailer but suffocate her.

Scott, Carter, Justin, and Kendra get a text from "Mike" to meet him and Nikki in a hotel room.  When they get there, everyone is kidnapped by Travis and wake up in individual cells along with Victor. The two guards take Justin away, and Carter calls the guard, telling him that he is also a client. Justin gets strapped into a chair, and Carter, Flemming, and Travis watch as a woman shoots arrows into him. Scott is also strapped and asks Carter why he is doing this. Carter reveals he wants Amy for himself, as they were in a relationship before she ended up with Scott.  He says that once Scott dies, he will comfort Amy and she will want to be with him.

Flemming orders Scott to be let go from the chair. Scott and Carter fight, and Scott ends up stabbing Carter.  Scott then breaks out and flees. Victor kills one of the guards and frees himself but is killed by another guard. Scott calls the cops and frees Kendra, who is shot by Travis.  Scott and Travis fight, and Scott kills him. Flemming sets the building to explode and attempts to drive away, but Carter kills him and takes his car. Carter sees Scott and locks the front gate before Scott can get to him.  He drives away while the building explodes with Scott still inside.

Sometime later, Carter is comforting Amy, and she invites him to stay the night. Amy tells Carter that Scott is still alive and stabs Carter in the hand with a corkscrew.  A burned Scott appears and kills him with an electric Cultivator|tiller.

==Cast==
* Kip Pardue as Carter
* Brian Hallisay as Scott
* John Hensley as Justin
* Sarah Habel as Kendra
* Skyler Stone as Mike
* Zulay Henao as Nikki
* Thomas Kretschmann as Flemming
* Chris Coy as Travis
* Nickola Shreli as Victor
* Evelina Oboza as Anka
* Kelly Thiebaud as Amy
* Derrick Carr as Mossberg
* Frank Alvarez as Mesa Tim Holmes as Beardo
* Barry Livingston as Doctor
*Alicia Vela-Bailey as Japanese Cyberpunk Woman

==Production==
 
In June 2008, it was announced that  , was in talks to write and direct a third film in the series. 
In July 2009, Eli Roth confirmed that he would not be directing Hostel: Part III.   Total Film later reported that Roth would be involved, albeit as producer only, and that the film will abandon the European locations of the previous films in favor of an American setting. A trailer for Hostel: Part III was released in October 2011 confirming the films Las Vegas setting.

Internal casino shots were filmed at the Greektown Casino in Detroit, Michigan (as evident by the Greektown logo shown on the TV monitors during the casino sequences). The Detroit Greektown Casino is the only Native American owned and operated casino located in the heart of a large populated metropolis in North America. 
  

==Release==
There were meant to be many viral marketing tools attached to the film including a collection of QR codes that would, if scanned, give exclusive content. You can see one at 1:9:26. Because of the films reception from test audiences, the marketing campaign was dropped, and if you scan the code now the result would just show up as "top left 8." 
  

Hostel Part III was released on DVD and Video on demand on December 27, 2011 in the United States, and on January 18, 2012 in Europe.  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 