Anna of Brooklyn
{{Infobox film
| name           = Anna of Brooklyn
| image          = Anna of Brooklyn.jpg
| image size     = 
| caption        = 
| director       = Vittorio De Sica Carlo Lastricati
| producer       = Milko Skofic
| writer         = Luciana Corda Ettore Maria Margadonna Dino Risi Joseph Stefano
| narrator       = 
| starring       = Gina Lollobrigida
| music          = 
| cinematography = Giuseppe Rotunno
| editing        = Eraldo Da Roma
| distributor    = 
| released       = 19 April 1958
| runtime        = 105 minutes
| country        = Italy
| language       = Italian
| budget         = 
| preceded by    = 
| followed by    = 
}} 1958 comedy film by Italian directors Vittorio De Sica and Carlo Lastricati. It stars Gina Lollobrigida and Vittorio de Sica.

==Cast==
* Gina Lollobrigida - Anna
* Vittorio De Sica - Don Luigi
* Dale Robertson - Raffaele
* Amedeo Nazzari - Ciccone
* Peppino De Filippo - Peppino
* Carla Macelloni - Rosina
* Gabriella Pallotta - Mariuccia
* Luigi De Filippo - Zitto-Zitto
* Clelia Matania - Camillina
* Renzo Cesana - Baron Trevassi
* Terence Hill - Chicco - Don Luigis nephew (as Mario Girotti)
* Augusta Ciolli - Aunt Carmela

==Awards==
;Won
* 1958  

;Nominated
*     

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 