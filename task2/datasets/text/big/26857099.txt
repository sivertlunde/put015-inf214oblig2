The Descendants
 
 
{{Infobox film
| name           = The Descendants 
| image          = Descendants film poster.jpg
| image_size     = 215px
| alt            = A man looking over his shoulder at the beach behind him, two people standing in the distance by the water. 
| caption        = Theatrical release poster
| director       = Alexander Payne
| producer       = {{Plain list | Jim Burke
*Alexander Payne Jim Taylor
}}
| screenplay     = {{Plain list |
*Alexander Payne
*Nat Faxon
*Jim Rash
}}
| based on       =  
| starring       = {{Plain list |
*George Clooney
*Shailene Woodley
*Beau Bridges
*Judy Greer
*Matthew Lillard
*Robert Forster
}}
| music            =  
| cinematography = Phedon Papamichael
| editing        = Kevin Tent
| studio         = Ad Hominem Enterprises
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 115 minutes  
| country        = United States
| language       = English
| budget         = $20 million   
| gross          = $177.2 million 
}}
 novel of the same name by Kaui Hart Hemmings. The film stars George Clooney, Shailene Woodley, Beau Bridges, Judy Greer, Matthew Lillard and Robert Forster, and was released by Fox Searchlight Pictures in the United States on November 18, 2011    after being screened at the 2011 Toronto International Film Festival. 
 Best Adapted Best Picture Best Actor – Drama for Clooney.

==Plot== trust of 25,000 pristine acres on Kauai island. The land has great monetary value, but is also a family legacy. While Matt has always ably managed his own finances, most of his cousins have squandered their inheritances. With the trust expiring in seven years due to the rule against perpetuities, the King clan is pressuring Matt to sell the land for hundreds of millions of dollars. Amidst these discussions, a boating accident has rendered Matts wife, Elizabeth, comatose.
 Big Island. living will directs all life support to be discontinued. When Matt tells Alex, she reveals that Elizabeth was having an affair at the time of the accident, causing a major rift between mother and daughter.

Two close family friends tell Matt that Elizabeth was unhappy and wanted to leave him for her lover, Brian Speer (Matthew Lillard), a real estate agent. After Matt arranges for friends to bid Elizabeth goodbye, he decides Speer should also have an opportunity. He and the girls, and also Alexs slacker friend Sid (Nick Krause), travel to Kauai to find Brian. While there, Matts cousin, Hugh (Beau Bridges) mentions that Brian is brother-in-law to Don Hollitzer, the developer to whom the family wants to sell the land. Brian stands to make a small fortune from the sales commission.

Matt confronts Brian and informs him Elizabeth is dying and offers him an opportunity to see her one last time (before the last day of her wifes death Julie came in his behalf). Brian declines, admitting that although Elizabeth was in love with him, it was only a fling to him; he loves his wife, Julie (Judy Greer) and their children, then apologizes to Matt for the pain he caused.

When Elizabeth is disconnected from life support, her father, Scott (Robert Forster) admonishes Matt for not being a more generous and loving husband. Matt agrees, but Sid and Alex both unexpectedly defend Matt.

At the King family meeting, Matt overrules the majority of his cousins who favor selling to Hollitzer. Matt decides to keep the land and look for a different solution to the problem posed by the Rule Against Perpetuities. Shocked, Hugh tells Matt that he and the other cousins will take legal action if Matt refuses to sell, but Matt is undeterred. 

Matt finally comes to terms with his wifes betrayal and her impending death. He tenderly kisses her goodbye, followed by Alex and Scottie, and later, scatter Elizabeths ashes in the ocean off Waikiki. The film concludes with the three at home sitting together sharing ice cream and watching television, all wrapped in the decorative blanket Elizabeth had been lying in.

==Cast==
 
* George Clooney as Matthew "Matt" King
* Shailene Woodley as Alex King
* Beau Bridges as Cousin Hugh
* Judy Greer as Julie Speer
* Nick Krause as Sid
* Amara Miller as Scottie King
* Matthew Lillard as Brian Speer
* Robert Forster as Scott 
* Patricia Hastie as Elizabeth King
* Mary Birdsong as Kai Mitchell
* Rob Huebel as Mark Mitchell
* Milt Kogan as Dr. Johnston
* Laird Hamilton as Troy 
* Michael Ontkean as Rondie
* Matt Corboy as Kaiba
* Celia Kenney as Bryson
 

==Production==
 cameo as Matt Kings secretary.
 O ahu. Big Island Hawai i (the Big Island), was the inspiration for the private boarding school. 

Post-production began on June 14, and continued into February 2011.  It screened at the Telluride, Toronto {{cite journal
 | title = The Descendant premiere photos – 36th Toronto International Film Festival | url = http://www.digitalhit.com/galleries/40/581 | year = 2011 | author = Lambert, Christine | journal = DigitalHit.com
 | accessdate =January 3, 2012 }}  and New York film festivals and was originally scheduled to have a limited release on December 16, 2011, but was moved to November 23, 2011  and then November 18, 2011.    
 Hawaiian music, Jeff Peterson, Makana (musician)|Makana, and Reverend Dennis Kamakahi. 

==Reception==

===Box office===
The Descendants opened in North America on November 16, 2011 in a Limited release in 29 theaters and grossed $1,190,096 averaging $41,038 per theater and ranking 10th at the box office. The film then has its Wide release on December 9 in 876 theaters and grossed $4,380,138 averaging $5,000 per theater and ranking 7th at the box office. The film was in cinemas for 156 days and its widest release in The United States was 2,038 theaters. The film ended up earning $82,584,160 domestically and $94,659,025 internationally for a total of $177,243,185. 

===Critical response===
The Descendants received critical acclaim upon release, with many critics praising the acting (particularly Clooney and Woodley). On Rotten Tomatoes it has an approval rating of 89% based on 235 reviews with an average rating of 8.1 out of 10. The consensus states "Funny, moving, and beautifully acted, The Descendants captures the unpredictable messiness of life with eloquence and uncommon grace."  The film also has a score of 84 out of 100 on Metacritic based on 43 reviews, indicating "universal acclaim". 
  

=== Top ten lists ===
The Descendants has appeared on the following critics top ten lists for the best films of 2011:
{| class="wikitable" style="font-size: 95%;"
|-
! Critic
! Publication
! Rank
|- 1st   
|- 1st 
|- MSN Movies||1st 
|- 1st 
|- 1st 
|- 1st 
|- Daily News||2nd 
|- 2nd 
|- 3rd 
|- 3rd 
|- Michael Phillips 4th 
|- Anne Thompson 4th 
|- 5th 
|- 6th 
|- MSN Movies||6th 
|- David Denby 7th 
|- 7th 
|- 7th 
|- 7th 
|- MSN Movies||9th 
|- 9th 
|- ScreenGeeks UK||9th 
|}

===Accolades===
{| class="wikitable"
|+ List of awards and nominations
! Awards Group !! Category !! Recipients and nominees !! Result
|- 84th Academy Awards  Best Picture
| Jim Burke, Alexander Payne and Jim Taylor
|  
|-
 Best Actor in a Leading Role
| George Clooney
|  
|- Best Director
| Alexander Payne
|  
|- Best Editing
| Kevin Tent
|  
|- Best Adapted Screenplay
| Alexander Payne, Nat Faxon & Jim Rash
|  
|-
| style="text-align:center;"| Argentine Academy of Cinematography Arts and Sciences Awards  
| Best Foreign Film 
| Alexander Payne, Jim Burke, Alexander Payne, and Jim Taylor
|  
|-
| style="text-align:center;"| American Film Institute  
| Movies of the Year
| 
|  
|-
| style="text-align:center;"| Art Directors Guild  
| Contemporary Film
| Jane Anne Stewart  
|  
|- Australian Academy of Cinema and Television Arts   Best Film&nbsp;– International
| 
|  
|- Best Screenplay&nbsp;– International
| Alexander Payne, Nat Faxon and Jim Rash
|  
|- Best Actor&nbsp;– International
| George Clooney
|  
|-
|rowspan="2" style="text-align:center;"| Boston Society of Film Critics Award 
| Boston Society of Film Critics Award for Best Actor
| George Clooney
|  
|-
| Best Use of Music in a Film
| 
|  
|- 65th British Academy Film Awards
| BAFTA Award for Best Film
| Jim Burke, Alexander Payne and Jim Taylor
|  
|- 
| BAFTA Award for Best Actor in a Leading Role
| George Clooney
|  
|-
| BAFTA Award for Best Adapted Screenplay
| Alexander Payne, Nat Faxon & Jim Rash
|  
|-
| style="text-align:center;"| Casting Society of America  
| Outstanding Achievement in Casting for a Big Budget Drama Feature
| John Jackson, John McAlary
|  
|- Chicago Film Critics Association   
| Best Picture
| 
|  
|-
| Best Director
| Alexander Payne
|  
|-
| Best Actor
| George Clooney
|  
|-
| Best Supporting Actress
| Shailene Woodley
|  
|-
| Best Adapted Screenplay
| Alexander Payne, Nat Faxon, and Jim Rash
|  
|-
| Best Promising Performer
| Shailene Woodley
|  
|-
| rowspan="7" style="text-align:center;"| Critics Choice Movie Awards 
| Best Picture
| 
|  
|-
| Best Actor
| George Clooney
|  
|-
| Best Supporting Actress
| Shailene Woodley
|  
|-
| Best Young Actor/Actress
| Shailene Woodley
|  
|-
| Best Acting Ensemble
| 
|  
|-
| Best Director
| Alexander Payne
|  
|-
| Best Adapted Screenplay
| Alexander Payne, Nat Faxon and Jim Rash
|  
|-
|rowspan="7" style="text-align:center;"| Denver Film Critics Society  
| Best Film 
| 
|  
|-
| Best Director 
| Alexander Payne
|   
|-
| Best Actor 
| George Clooney 
|  
|-
| Best Supporting Actress
| Shailene Woodley 
|  
|-
| Best Supporting Actress 
| Judy Greer
|  
|-
| Best Breakout Star 
| Shailene Woodley 
|  
|-
| Best Screenplay
| Alexander Payne, Nat Faxon & Jim Rash 
|  
|-
| rowspan="3" style="text-align:center;"| Detroit Film Critics Society   Best Film
| 
|  
|-
| Best Actor
| George Clooney
|  
|-
| Breakthrough Performance
| Shailene Woodley
|  
|- Florida Film Critics Circle  
| Best Picture
| 
|  
|-
| Best Supporting Actress
| Shailene Woodley
|  
|-
| Best Adapted Screenplay
| Alexander Payne, Nat Faxon, and Jim Rash
|  
|- 69th Golden Golden Globe Best Picture – Drama || – ||  
|- Golden Globe Best Director || Alexander Payne ||  
|- Golden Globe Best Actor – Drama || George Clooney ||  
|- Golden Globe Best Supporting Actress || Shailene Woodley ||  
|- Golden Globe Best Screenplay || Alexander Payne, Nat Faxon and Jim Rash ||  
|- 2013 Grammy Grammy Awards   Best Compilation Soundtrack For Visual Media
|
|  
|- Independent Spirit Independent Spirit Best Film || – ||  
|- Best Director || Alexander Payne ||  
|- Best Supporting Female || Shailene Woodley ||  
|- Best Screenplay || Alexander Payne, Nat Faxon, and Jim Rash ||  
|-
|rowspan="2" style="text-align:center;"| Los Angeles Film Critics Association  Best Film
| 
|  
|- Best Screenplay
| Alexander Payne, Nat Faxon, and Jim Rash
|  
|-
| style="text-align:center;"| MTV Movie Awards  Best Breakthrough Performance
| Shailene Woodley
|  
|-
|rowspan="4" style="text-align:center;"|  || – ||  
|- Best Actor || George Clooney ||  
|- Best Supporting Actress || Shailene Woodley ||  
|- Best Adapted Screenplay || Alexander Payne, Nat Faxon, and Jim Rash ||  
|- New York Film Critics Online  
| Best Screenplay
| Alexander Payne, Nat Faxon, and Jim Rash
|  
|- Online Film Critics Society   Best Picture 
| 
|  
|- Best Actor 
| George Clooney
|  
|- Best Supporting Actress 
| Shailene Woodley
|  
|- Best Adapted Screenplay 
| 
|  
|-
| style="text-align:center;"| Palm Springs International Film Festival  
| Chairmans Award
| George Clooney  
|  
|- Phoenix Film Critics Society 
| Best Picture
|
|  
|-
| Best Director
| Alexander Payne
|  
|-
| Best Actor in a Leading Role
| George Clooney
|  
|-
| Best Actress in a Supporting Role
| Shailene Woodley
|  
|-
| Best Screenplay: Adaptation
| Alexander Payne, Nat Faxon, and Jim Rash
|  
|- Breakthrough Performance on Camera Shailene Woodley
| 
|- Best Performance by a Youth in a Lead or Supporting Role: Female Amara Miller
| 
|- Producers Guild of America Award  
| Outstanding Producer of Theatrical Motion Pictures
| Jim Burke, Alexander Payne, Jim Taylor
|  
|- 18th Screen Screen Actors Best Ensemble || Beau Bridges, George Clooney, Robert Forster, Judy Greer, Matthew Lillard, Shailene Woodley ||  
|- Screen Actors Best Actor || George Clooney ||  
|- Satellite Awards Best Film – Drama || ||  
|- Best Actor || George Clooney ||  
|- Best Supporting Actress || Judy Greer ||  
|- Best Director || Alexander Payne ||  
|- Best Adapted Screenplay || Alexander Payne, Nat Faxon and Jim Rash ||  
|- Best Editing || ||  
|- Washington D.C. Area Film Critics Association 
| Best Actor
| George Clooney
|  
|-
| Best Adapted Screenplay
| Alexander Payne, Nat Faxon and Jim Rash
|  
|-
| Best Film
| 
|  
|-
| Best Director
| Alexander Payne
|  
|-
| Best Supporting Actress
| Shailene Woodley
|  
|}

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*   at The Numbers
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 