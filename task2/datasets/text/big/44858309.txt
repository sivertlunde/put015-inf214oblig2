An International Marriage
{{Infobox film
| name           = An International Marriage
| image          = An International Marriage - 1916 - newspaperad.jpg
| alt            = An old newspaper advertisement for the film with a photo of Rita Jolivet on the left. 
| caption        = Newspaper advertisement.
| director       = Frank Lloyd
| producer       = 
| screenplay     = George Broadhurst
| starring       = Rita Jolivet Marc Robbins Elliott Dexter Grace Carlyle Olive White Courtenay Foote
| music          = 
| cinematography = James Van Trees
| editing        = 
| studio         = Oliver Morosco Photoplay Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent film directed by Frank Lloyd and written by George Broadhurst. The film stars Rita Jolivet, Marc Robbins, Elliott Dexter, Grace Carlyle, Olive White and Courtenay Foote. The film was released on July 23, 1916, by Paramount Pictures.  

==Plot==
The story revolves around a Florence Brent (Rita Jolivet) who is an American heiress who wishes to marry a Duke in Europe. The father of the duke wants her to have a title before marriage however, so she quickly marries the Dukes friend who is a Count with plans to quickly divorce him and retain the title. After the marriage though, she finds out that the Duke is already married. After all that, Florences sweetheart arrives from America with a pistol and tells the Count he wants to marry her. She decides to go off with her sweetheart, and moves back to New York with him.  

== Cast ==
*Rita Jolivet as Florence Brent
*Marc Robbins as Bennington Brent
*Elliott Dexter as John Oglesby
*Grace Carlyle as Eleanor Williamson 
*Olive White as Mrs. Williamson
*Courtenay Foote as Duke of Burritz
*Page Peters as Count Janefski
*Herbert Standing as Archduke Ferdinand
*Adelaide Woods as Agnes Sotherton
*Jean Woodbury as The Archduchess

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 

 