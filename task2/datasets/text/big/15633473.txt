Wedding Night
 
{{Infobox film
| name           = Wedding Night
| image          = Wedding Night poster.jpg
| image_size     =
| caption        =
| director       = Émile Gaudreault
| producer       = Denise Robert Yves Lafontaine (executive producer) Daniel Louis (line producer)
| writer         = Émile Gaudreault Marc Brunet
| narrator       =
| starring       = François Morency Geneviève Brouillette
| music          = FM Le Sieur
| cinematography = Daniel Jobin
| editing        = Richard Comeau
| distributor    = Seville Distribution
| released       = June 1, 2001 (Quebec)
| runtime        = 92 minutes
| country        =   French
| budget         = Canadian dollar|$ 2,800,000 (estimated)
| gross          =
| preceded_by    =
| followed_by    =
}}
Wedding Night (French: Nuit de noces) is a 2001 Canadian comedy film.

==Plot==

After winning a contest Florence and Nicolas set out to get married in Niagara Falls accompanied by their family and friends. No sooner do they arrive than the situation turns sour, and the couple decides to call the whole thing off. Stuck in an unfamiliar town with their respective relatives, Florence and Nicolas have their illusions shattered regarding love and living as a couple. In their own ways the members of both families try to reconcile the ex-future husband and wife, but things are not so simple.

==Recognition==

* 2002 - Golden Reel Award - Won
* 2002 - Jutra Award Best Sound - Nominee
* 2002 - Jutra Award Best Supporting Actress - Nominee

== External links ==
*  
*  

 
 
 
 
 
 
 
 