Insidious: Chapter 3
 
{{Infobox film
| name           = Insidious: Chapter 3
| image          = Insidious – Chapter 3 poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Leigh Whannell 
| producer       = {{Plainlist|
* Jason Blum
* Oren Peli
* James Wan}}
| screenplay     = Leigh Whannell
| starring       = {{Plainlist|
* Dermot Mulroney
* Stefanie Scott
* Angus Sampson
* Leigh Whannell
* Lin Shaye}}
| music          = Joseph Bishara 
| cinematography = Brian Pearson 
| editing        = Timothy Alverson
| studio         = {{Plainlist|
* Automatik Entertainment
* Blumhouse Productions
* Entertainment One}}
| distributor    = {{Plainlist|
* Focus Features
* Stage 6 Films}}
| released       =  
| runtime        = 97 minutes  
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}  supernatural horror directorial debut. It is a prequel to the first two films and the third installment in the Insidious (film series)|Insidious film series. The film stars Dermot Mulroney and Stefanie Scott, with Angus Sampson, Whannell, and Lin Shaye reprising their roles from the previous films.

The film is scheduled to be released on June 5, 2015 by Focus Features. 

==Plot==
A prequel set before the haunting of the Lambert family that reveals how gifted psychic Elise Rainier reluctantly agrees to use her ability to contact the dead in order to help a teenage girl who has been targeted by a dangerous supernatural entity.

==Cast==
 
* Dermot Mulroney as Sean Brenner
* Stefanie Scott as Quinn Brenner
* Angus Sampson as Tucker
* Leigh Whannell  as Specs
* Lin Shaye as Elise Rainier
* Ashton Moio as Hector
* Steve Coulter as Carl
* Ele Keats as Lillith Brenner
* Michael Reid MacKay as The Man Who Cant Breathe
* Tate Berney as Alex Brenner
* Hayley Kiyoko as Maggie
* Phil Abrams as Mel
* Ruben Garfias as Ernesto
* Joseph Bishara as Lipstick-Face Demon
* Tom Fitzpatrick as Bride in Black / Parker Crane
* Anne Bergstedt Jordanova as Neighbor
* Amaris Davidson as Nurse
* Anna Ross as Dead Teenage Junkie
 

==Production== Patrick Wilson went on to say that he "  know where else it could go", and that "  been through the wringer, and I think the movie sets it up well at the end   And thats great, thats how it should end."  On November 13, 2013, it was announced Focus Features and Stage 6 Films would release the film on May 29, 2015.  

On March 11, 2014, Screen Rant reported that the third film wont focus on the Lambert family, but will focus on a new family and story, and wont connect to the last scene in the second film and both Whannell and Angus Sampson will return as ghost hunters Specs and Tucker along with Lin Shaye as Elise.  On May 7, 2014, Wan tweeted that Whannell will be directing the third film, which will mark his directorial debut.  In June 2014, Stefanie Scott and Dermot Mulroney were cast in the film.   On September 22, 2014, during the Cinema Diverse Film Festival in Palm Springs, actress Ele Keats said she had recently wrapped an undisclosed supporting role in the film. 

===Filming===
Principal photography began on July 9, 2014  in Los Angeles under the title "Into The Further", on a scheduled 29 day shoot.  Several scenes were shot in the San Fernando Valley at the Delfino Studios in Sylmar, where the Brenners apartments interiors were built. 

A first look image was released on July 22, 2014.  Filming wrapped on August 18, 2014. 

==Marketing and promotion==
A first teaser poster was released on line on September 18, 2014, featuring a grey wall with a vent and the red text "The man who cant breathe, the man who lives in the vent, I heard him saying your name last night, I heard him on your room while you were gone, hes in there right now, standing in your room."  The first teaser trailer for the film was released by Focus Features on October 23, 2014.  The same day director Leigh Whannell invited fans to join him for a live Q&A session on the movies official Facebook page.  A few days later, on October 28, 2014 the same Facebook page reached 4 million fans.  On December 17, 2014 fans were invited to connect with Insidious on Kik Messenger for exclusive content. 

On March 16, 2015, Focus Features debuted a teaser for the full official trailer that was eventually released the following day, on March 17, 2015 during a series of launch events in selected cities, including Miami (where lead star Stefanie Scott held a Q&A session), Chicago (with supporting actress Hayley Kiyoko in attendance) and New York City (where Fangoria Magazine hosted a Q&A session with Lin Shaye).  A new poster featuring Stefanie Scott was also released the same day.  On July 3, 2014 Focus Features moved the already announced US theatrical release date from April 3 to May 29, 2015.  On December 10, 2014 the distribution company announced that the theatrical release date had been moved back again from May 29 to June 5, 2015. 

At WonderCon 2015 (3-5th April), a teaser clip was screened and was well received by the audience. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 