Everybody's All-American (film)
{{Infobox film
| name           = Everybodys All-American
| image          = Everybodysallamerican.jpg
| image_size     =
| caption        = Theatrical Release Poster
| director       = Taylor Hackford
| producer       = Executive Producer:    
| writer         = Screenplay:   Frank Deford
| narrator       =
| starring       = Dennis Quaid Jessica Lange Timothy Hutton John Goodman Carl Lumbly
| music          = James Newton Howard
| cinematography = Stephen Goldblatt Don Zimmerman
| distributor    = Warner Bros.
| released       =  
| runtime        = 127 minutes
| country        = United States English
| budget         =
| gross          = $12,638,294
}} motion picture directed by Taylor Hackford and based on the novel Everybodys All-American by longtime Sports Illustrated contributor Frank Deford.

The film covers 25 years in the life of a college football hero. It stars Dennis Quaid, Jessica Lange, Timothy Hutton and John Goodman.

==Plot==
Gavin Grey (Dennis Quaid) is a 1950s star athlete known by the moniker "The Grey Ghost," who plays football at Louisiana State University. His campus girlfriend Babs Rogers (Jessica Lange), nephew Donnie (Timothy Hutton) who also goes by the nickname "Cake," and friend Ed Lawrence (John Goodman), adore his personality and charm. During an important matchup in the Sugar Bowl, Gavins play,  which at times defined his competitiveness throughout his career, causes a player from the opposing team to fumble the ball, while he later returns it to score a game-winning touchdown.

Later, Gavin ends up marrying Babs, starts a family, and eventually gets drafted by a professional football team. Meanwhile, Lawrence opens a popular sports bar in Baton Rouge. Everyone is pleased for Gavin, including his friendly rival Narvel Blue (Carl Lumbly), who might have achieved professional stardom had he chosen an athletic career path. Reality quickly sets in for Gavin as life in the NFL is difficult, competition fierce, and the schedule grueling. Gavin is a respectable running back for the Washington Redskins, but hardly the idol worshipped by everyone back home during his college years. Concurrently, Lawrence has accrued a number of gambling debts, as he is later murdered amid an episode of organized crime. 
 Ray Baker) whom he despises, spending countless hours telling old college football stories to clients. Donnie moves on with his life, getting engaged to a sophisticated woman named Leslie Stone (Patricia Clarkson), while supporting Gavin and Babs through a marital breakdown. A despaired figure in the end, Gavin mends his relationship with Babs as he spends his withdrawal from professional sports reminiscing about his famed athletic youth.

==Production== Tim Fox during filming. Footage of Quaid rolling in pain on the sidelines of the snow game appears in the finished film.

* A key scene featuring a candlelight parade involving large numbers of extras was filmed, on the steps of the Louisiana State Capitol, when snow started falling. Despite the beauty of the scene, director Taylor Hackford elected to reshoot the scene, as snow in Baton Rouge in November was such a rare event that he was worried it would be seen as a special effects goof in the film.
 Tiger Stadium during the halftimes of actual LSU games. The goalposts were altered to resemble the vintage "H" posts as needed during filming. Vertical posts were moved in place for the bottom portion of the H, and a multi-colored fabric covering was used to conceal the center upright. Upon completion of filming, the vertical posts and fabric were retracted so as not to interfere with the LSU games. In late 1993, LSU installed an updated model of the vintage posts permanently in the stadium. 
 Alabama game. Mississippi State. 
 Thomas Rickmans script in 1982 until Warner Bros. balked at the $16 million price tag, leading man Tommy Lee Jones and the fact that American football movies never do any business overseas. During its 6 years in development hell, Warren Beatty, Robert Redford and Robert De Niro all circled the project.

*Despite the fact that the novel was written about the University of North Carolina, when it was filmed at LSU, rumors started that Gavin Grey was based on the former LSU All-American Billy Cannon, who won the 1959 Heisman Trophy and played 11 seasons for three professional teams, but served three years in federal prison in the mid-1980s for his role in a counterfeiting ring. Deford laughs at that. "Never met Cannon and knew nothing about him personally," he says. "Gavin was strictly a composite of many athletes from several sports that I had covered."

*The film contains a much more hopeful and upbeat ending than the book, where Gavin takes his own life after trying to kill Babs as well.
 

==Reception==
Reaction to the film was mostly mixed-to-negative, as Rotten Tomatoes gives it a 30% rating based on 10 reviews.

==External links==
*  
* 
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 