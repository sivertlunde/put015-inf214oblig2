Unnale Unnale
{{Infobox film
| name = Unnale Unnale
| image = Unnale-unnale.jpg
| caption = Jeeva
| producer = Viswanathan Ravichandran
| writer = Jeeva S. Ramakrishnan
| starring = Vinay Rai Sadha Tanisha Mukherjee Lekha Washington Raju Sundaram Sathish
| music = Harris Jayaraj
| cinematography = Jeeva
| editing = V. T. Vijayan Aascar Film Pvt. Ltd 
| released =   
| runtime = 162 minutes
| country = India
| language = Tamil
}} Jeeva and produced by Oscar Ravichandran its stars Vinay Rai, Sadha and Tanisha Mukherjee in the lead roles. Raju Sundaram and Lekha Washington, prominent actors from the south Indian film industry, played the roles of the lead characters friends in the film.

The film revolves around the aftermath of a relationship between a careless man and a serious woman. Despite being in a relationship, the latter walks out on the former due to his antics with other women. However, the man changes his ways and on a business trip to Melbourne, he encounters another woman. This other womans personal assistant turns out to be the mans former lover. The events that follow and who the man eventually gets together with form the crux of the story.

The film opened to Indian audiences after several delays, on 14 April 2007, coinciding with the Tamil New Year. Despite receiving mixed reviews on the actors performances, the film went on to win the box office battle on Tamil New Years Day and become a major hit at the South Indian box office.  Owing to its success, it was later dubbed into the Telugu language as Neevalle Neevalle. 

== Plot ==
The film opens with two people – a man (Aravind Akash) and a woman – interviewing civilians thoughts on love and the opposite sex. After a mixture of answers, they cease their questions and the credits roll. The credits end as a boy, after being rejected by his girlfriend, commits suicide by jumping onto a passing car. Karthik (Vinay Rai) walks off disturbed to his girlfriend Jhansis (Sadha) office. There, he is criticized by her for his antics with other women and his lack of passion for their love. She walks off, ending their relationship.

Karthik is then shown as a civil engineer in Chennai, still playful and fun-loving. Accepting a request from his manager to go on a business trip to Melbourne, he bids farewell to his friends (Raju Sundaram and Sathish). On the plane, he encounters a playful, flirtatious girl, Deepika (Tanisha Mukherjee), whom he sits next to during the flight. After spending hours together, they become good friends and exchange details. However, by coincidence, Deepika is travelling to Melbourne to work in the same company as Jhansi, who is settled there. Together they seem to conveniently bump into Karthik at every corner, prompting an unwelcome reunion for Jhansi and Karthik.

While Deepika takes a liking to Karthik, Jhansi still ignores him, even though Karthik has ambitions of getting back together. At a restaurant, Karthik bumps into a fellow Tamilian (Srinath (actor)|Srinath) and explains his love for Jhansi. The story then moves two years back for a flashback of events. Karthik had met Jhansi at a festival at a temple, where she played a prank on Karthik and his friends. Taking a liking to her, Karthik begins to follow Jhansi intentionally, hoping for her to fall in love with him. Soon after this happens, she becomes more and more suspicious of Karthik. This comes to a head when Karthik acts as the boyfriend of his friends (Raju Sundaram) girlfriend, Pooja, (Paloma Rao), only for Jhansi to get confused and mistake him. His reputation as a trusting boyfriend takes another turn for the worse, when at a wedding, the bride (Lekha Washington) gives him a kiss after he improvises a song.

As the pair have a love-hate relationship, the break-up beckons when Jhansi sends her friend (Vasundra) undercover to go flirt with Karthik, however, he lies claiming he was at home ill, prompting Jhansi to end their relationship. At the end of the flashback, Srinath suggests to Karthik, that its more important to move on than think regretfully and Karthik gets over his relationship with Jhansi. However, finding out about Deepikas love towards Karthik, Jhansi becomes jealous and reinstates her love for Karthik. Soon, as she sees Deepika and Karthiks compatibility and understanding her faulty, she finally understands what went wrong in their relationship. Then, Karthik tells her through a meaningful conversation that he still loves her, not Deepika. After this, Jhansi run away to Sydney anonymously.

At the end of the film, the male interviewer who appeared at the start questions Jhansi on her decision to leave anonymously, however she refuses to answer, at first. Later, she replies that though she still loves Karthik, she was unable to understand Karthik well. She knew it would not work out and she did not want to hurt Karthik further, thus becoming the sole reason for her departure. She then confirms that she met up with Deepika recently, and had found that she is married to Karthik and has a child. She then walks away, claiming her future lies in her own hands and she is happy the way she is now.

== Cast ==
The cast from the film was minimal, mainly focused on the three lead actors. 

* Vinay Rai as Karthik – A fun-loving young man who despite being a civil engineer, keeps his main intentions of attracting women. The child in his heart is released when he gets to know Jhansi.
* Tanisha Mukherjee as Deepika - A bubbly, happy-go lucky young girl, who likes the antics of Karthik unlike Jhansi, prompting her to fall in love.
* Sadha as Jhansi – A very reserved, hypocritical and caring young woman who expects Karthik to stay loyal to her. Her dominant vivacity presents Karthik, no hope but to leave her.
* Raju Sundaram as Raju – An energetic friend to Karthik with a severe habit of ruining the situation.
* Sathish Krishnan as Sathish – A silent friend to Karthik who attempts to reconcile the relationship. Srinath as Vaidyanathan – Meets Karthik in Melbourne and becomes an advisor to him.
* Lekha Washington, Paloma Rao and Vasundhara Kashyap also play cameo roles.

== Production ==

=== The key elements === Jeeva had directed three films, 12B, Ullam Ketkumae and Run (2004 film)|Run, a Hindi film. Unnale Unnale became his last finished film prior to his death on the sets of his next film, Dhaam Dhoom. 

The film was originally announced under the title of July Kaatril (In the Winds of July)  with a scheduled casting of Arya (actor)|Arya, Sonia Agarwal and Parvati Melton.  However all three of the proposed lead cast opted out of the film. Agarwal cited that her marriage was on the cards and she would find it difficult to sign on for a year, whilst the other two were unable to star in the film due to undisclosed reasons. 
 Vaali and Pa. Vijay assisting him with the lyrics, Jeevas wife, Aneez, debuted as a costume designer. 

=== Casting, location and music ===
  Arya opted Tulu advertisements.  However, they finalised the choice of the actor only after the script was ready. The choice of Sadha was more difficult and since Anniyan she had received many negative reviews and the film became important to rekindle her success.  The role of Deepika went to Tanisha Mukherjee, who despite being a leading heroines sister (Kajol), she was yet to maintain a hit film. Mukherjee, came down to South India to look for new opportunities and attracted the producer into signing her on. She immediately expressed her satisfaction with the character and consented for the role. The camaraderie that the lead actors shared during the filming, added on to their good performances. Raju Sundaram and Sathish were picked for their dancing skills while actresses Paloma Rao, Lekha Washington, Aravind Akash and Vasundra all played floating cameos in the film.

The film was mostly shot in Melbourne, Australia overlooking the Melbourne Docklands.  The climax was shot in Sydney while the beginning and the flashback was picturized in various cities across South India.  For the films music and soundtrack, Jeeva renewed his previous association (12B and Ullam Ketkumae) with Harris Jayaraj. Playback singer, Chinmayi dubbed for Tanisha Mukherjee, 

== Release ==

=== Reception === Madurai Veeran and Arputha Theevu on Tamil New Years Day, 14 April 2007.  Owing to the success of the film, the number of reels grew to about hundred. The film completed 100 days of screening in the theaters in the state of Tamil Nadu.  The reception in Malaysia was equally successful and was released in six major metropolises for up to 9 weeks, the film collected $114,883 (then approximately  5 million) within its 50 day run. 

=== Critical acclaim and controversies===
Despite its relative commercial success, the film received mixed reviews on the storyline and the performances of the actors themselves. Rediff.com praised Sadha but describe Tanisha Mukherjees and Vinay Rais performances as mediocre,  while the reviewers at SearchIndia.com contradict the statement, praising Tanisha and criticizing the other pair. The film made business for Vinay, who signed up many films after the project.

== Home media== Dolby Digital progressive 24 Frame rate|FPS, widescreen and NTSC format. 

== Soundtrack ==
{{Infobox album
| Name        = Unnale Unnale
| Type        = soundtrack
| Artist      = Harris Jayaraj
| Cover       = Unnale Unnale.jpg
| Released    =  
| Recorded    = 
| Genre       = Film soundtrack
| Length      = 27:34 Sa Re Ga Ma
| Producer    = Harris Jayaraj
| Last album  = Pachaikili Muthucharam (2007)
| This album  = Unnale Unnale (2007)
| Next album  = Munna (film)|Munna (2007)
}}
 Vaali and Filmfare Award for Best Music Director and Vijay Award for Best Music Director.
{{Track listing
| extra_column = Singer(s)
| headline     = Original Tracklist Krish and Arun | length1 = 6:02
| title2 = Hello Miss Imsaiyae | extra2 = G. V. Prakash Kumar and Anushka Manchanda | length2 = 4:31
| title3 = Vaigasi Nilavae | extra3 = Haricharan and Madhushree | length3 = 5:41
| title4 = Mudhal Naal | extra4 = KK (singer)|KK, Pop Shalini and Mahalaxmi Iyer | length4 = 4:27 Karthik and Harini | length5 = 4:43
| title6 = Ilamai Ulasam | extra6 = Krish and Pop Shalini | length6 = 2:10 Krish | length7 = 1:58
}}

;Telugu Tracklist
{{Track listing
| extra_column = Singer(s)
| all_lyrics   = Bhuvana Chandra Krish and Arun | length1 = 6:02
| title2 = Hello Miss Imsaiyae | extra2 = G. V. Prakash Kumar and Chinmayee | length2 = 4:31
| title3 = Vaishaka Vennela | extra3 = Haricharan and Swarnalatha | length3 = 5:41
| title4 = Modhalennadu | extra4 = Naresh Iyer, Pop Shalini and Mahalaxmi Iyer | length4 = 4:27 Karthik and Harini | length5 = 4:43
| title6 = Yavvana Ullasam | extra6 = Krish and Pop Shalini | length6 = 2:10
}}

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 