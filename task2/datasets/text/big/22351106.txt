Ex Cathedra (film)
{{Infobox film
| name           = Ex Cathedra
| image          = Excathedramovieposter.jpg
| caption        = Film poster by Banter Media
| director       = Liam Andrew Wright
| producer       = Vicky Petela Liam Andrew Wright
| writer         = Liam Andrew Wright
| starring       = Richard Massara Garth Maunders Isaac Harper Paul Bamgbose Glen Cardno Rebecca Herrod
| cinematography = Mike Ritchie
| editing        = Liam Andrew Wright Michael Brown
| released       =  
| runtime        = 113 minutes
| country        = England
| language       = English
}}
Ex Cathedra is a British drama film written and directed by Liam Andrew Wright and production company Banter Media. It was Wrights directorial debut. The films narrative centers on the an original story combining religion, Recreational drug use|drugs, love and hope. It was an independently financed film, funded entirely by the films young producers and followed the model set by 2004 film Primer (film)|Primer. 

==Plot==
The last prophet on earth who, addicted to the drug exente, must accept his gift and fight his addiction or risk losing the woman he loves in a battle between her drug dealers and a businessman who has fallen for her mysterious charms. 

Ex Cathedra follows the story of three lost souls Alexis, Kevin and Corban; existing in a world governed by ‘Exente’- a powerful yet socially acceptable drug- all fighting for emancipation. Kevin’s mundane world is falling apart and through seemingly accidental circumstances is lured into the world of the drug Exente. In this world of drugs he meets Alexis, who has already to succumbed to the power of the drug. Could Kevin be the only chance Alexis has to escape this bleak existence? While Kevin and Alexis run into a dangerous conflict with two local drug dealers, mysterious barman Corban is ever present and is destined to determine the fate of the three of them.

==Cast==
* Richard Massara as Corban
* Garth Maunders as Kevin
* Isaac Harper as Duke
* Paul Bamgbose as Gemini
* Glen Cardno as Dr. John Mackenzie
* Rebecca Herrod as Alexis

==References==
 

==External links==
*  
*  
*  
*   Review from British Film Magazine

 
 
 
 
 
 
 
 
 
 
 