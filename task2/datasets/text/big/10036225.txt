Prophecies of Nostradamus
{{Infobox Film
| name           = Prophecies of Nostradamus
| image          = Prophecies of nostradamus movie.jpg
| image_size     = 
| caption        =  Toshio Masuda
| producer       = Osamu Tanaka Tomoyuki Tanaka
| writer         = Tsutomu Goto (story) Toshi Yasumi (earlier screenplay) Yoshimitsu Banno
| narrator       = Kyōko Kishida (Japan) Jack Ryland (US)
| starring       = Tetsuro Tamba Toshio Kurosawa Kaoru Yumi Yoko Tsukasa
| music          = Isao Tomita
| cinematography = Rokuro Nishigaki
| editing        = Nobuo Ogawa
| distributor    = Toho United Productions of America (UPA) (USA)
| released       =  : July 13, 1979
| runtime        = 114 min. (Japan) 88 min. (US)
| country        = Japan Japanese
| budget         = 
| gross          =
| 
|  
}}
 Toshio Masuda, The Last War (1961) and Yasumi was credited out of respect.

The film is notorious for its rarity. After complaints from a "No Nukes" group in Tokyo, Toho pulled the film from circulation in 1980. 

==Plot==
In 1835, Gentetsu Nishiyama begins preaching the prophecies of Michel de Nostradame using a copy of his book, "Centuries." When Nishiyama gets persecuted by the Tokugawa Shogunate for supposed heresy, his wife and son flees with the book in hand, passing down the knowledge to future generations. At the onset of World War II, his descendant, Gengaku, is interrogated by an Imperial Japanese Army officer about the familys continued preaching of the prophecies, which predicted the rise of Nazism and the Axis defeat.

In the present day of 1999, biologist Dr. Ryogen Nishiyama is called in to analyze recent scientific phenomena, such as the appearance of giant mutant slugs, children wielding advanced abilities, and large ice packs just north of Hawaii. He is also a leading figure in the fight against environmental pollution and the global arms race, as well as global disasters. The UN sends a research expedition to New Guinea to investigate a radioactive dust cloud that appeared over the island, but the team suddenly goes out of contact. Nishiyama joins a second team to find them and discover that the area around the teams last known position is now infested by mutant bats, one of whom kills a team member. Nishiyamas group finds the remains of the original team, but are disheartened that some of them are barely alive; they are forced to kill the survivors. After cannibals attack the teams dead colleague, they bury all the members.

An SST explodes in the atmosphere over Japan, with the explosion puncturing the ozone layer and unleashing ultraviolet rays below. The polar icecaps melt triggering massive floods in Japan. After more natural disasters hit the country, the civilian populace turns to looting as rationing takes effect. Society breaks down further, with several people committing suicide. The panic escalates until nuclear war breaks out and mutated survivors fight each other for food.

It is revealed that the nuclear war is one of many nightmare scenarios Nishiyama is explaining before the Japanese Cabinet. As the prime minister explains a resolve to find a solution, Nishiyama, his daughter Mariko, and her boyfriend Akira (Nishiyamas colleague) leave the Diet complex.

==Cast==
* Tetsuro Tamba - Gentetsu / Gengaku / Dr. Ryogen Nishiyama 
* Toshio Kurosawa - Akira Nakagawa 
* Kaoru Yumi - Mariko Nishiyama 
* Yoko Tsukasa - Nobuo Nishiyama 
* So Yamamura - Prime Minister 
* Kyoko Kishida - Voice of the Prophecy

==Release history==
Prophecies of Nostradamus is infamous for its depiction of mutated human beings. After the film was released, a protest group lodged a complaint with the Eirin (the Japanese film ratings board), citing the New Guinea sequence and the post-climactic scene featuring two mutated children. Toho publicly apologized and cut the movie down to 90 minutes before putting the movie back into circulation for the rest of its theatrical run. After its theatrical release and a 1980 television broadcast, the uncut version of the film was officially pulled from circulation by Toho. The 90-minute re-cut does occasionally make appearances in re-releases and it is this version which is on file at the Library of Congress. 

Despite all the negative publicity, Toho created an international version of the film dubbed in Hong Kong, which was also 90 minutes long like the re-edited Japanese cut. This version was released on home video in Eastern Europe as well as theatrically in the United States (New York and Los Angeles only). This dubbed version deletes many scenes including ones connecting the Nishiyama family to the prophecies, and a subplot about Nishiyamas assistants baby who turns deformed.

Henry Sapersteins UPA Productions picked up the film and released another edited version to American television in the early 1980s. Titled The Last Days of Planet Earth, UPAs version runs 88 minutes long and features scenes from both the international and Japanese versions.  Much of the semi-graphic violence, including the cannibalism of one of the researchers during the New Guinea Expedition and the peeling of skin of the corpse in the cave which originally revealed the bone, is deleted from the cut version. The Last Days of Planet Earth was released by Paramount/Gateway Home Video in 1995 on VHS and Laserdisc.

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 