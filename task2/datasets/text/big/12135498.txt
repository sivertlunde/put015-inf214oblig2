That Happy Couple
{{Infobox Film
| name           = Esa pareja feliz
| image          = Esa pareja feliz.jpg
| caption        = Theatrical release poster
| director       = Juan Antonio Bardem Luis García Berlanga
| producer       =
| writer         = Juan Antonio Bardem Luis García Berlanga
| starring       = Fernando Fernán Gómez Elvira Quintillá
| music          = Jesús García Leoz  (as Jesús G. Leoz)
| cinematography = Willy Goldberger (as Guillermo Golberger)
| editing        = Pepita Orduña  	 (as Pepita Orduna)
| distributor    =
| released       = 31 August 1953
| runtime        = 83 minutes
| country        = Spain
| language       = Spanish
| budget         =
}}
Esa pareja feliz is a Spanish comedy film co-written and co-directed by Juan Antonio Bardem and Luis García Berlanga. It was their feature film debut. The film was made in 1951 but not released until 1953.

==Cast==
*Fernando Fernán Gómez as	Juan Granados Muñoz
*Elvira Quintillá as Carmen González Fuentes
*Félix Fernández as Rafa
*José Luis Ozores as Luis
*Fernando Aguirre as Organizador
*Manuel Arbó as Esteban
*Carmen Sánchez as Dueña del salón de té
*Matilde Muñoz Sampedro as Amparo
*Antonio García Quijada as Manolo
*Antonio Garisa as Florentino
*José Franco as Tenor
*Alady as Técnico
*Rafael Bardem as Don Julián, el Comisario
*Rafael Alonso as Gerente Seguros La Víspera
*José Orjas
*Francisco Bernal
*Lola Gaos as Reina en Rodaje
*Matías Prats as Locutor Deportivo (voice)

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 


 
 