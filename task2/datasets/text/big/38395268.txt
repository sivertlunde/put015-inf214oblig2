Song of the Cornfields
{{Infobox film
| name           = Song of the Cornfields
| image          =
| image_size     =
| caption        =
| director       = István Szőts 
| producer       =  István Szőts
| writer         =  Ferenc Móra (novel)   István Szőts
| narrator       =
| starring       = Alice Szellay   János Görbe   József Bihari   Marcsa Simon
| music          = Tibor Polgár
| cinematography = Barnabás Hegyi   Árpád Makay
| editing        = Mihály Morell
| studio         = Szőts-film
| distributor    =  
| released       = 1947
| runtime        = 81 minutes
| country        = Hungary Hungarian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
Song of the Cornfields (Hungarian: Ének a búzamezökröl) is a 1947 Hungarian drama film directed by István Szőts and starring Alice Szellay, János Görbe and József Bihari. A Hungarian soldier returning from fighting in the Second World War marries the woman he believes to be the widow of a former comrade who he thinks died in the Prisoner of War camp in which they were held.

Song of the Cornfields was one of only three Hungarian films to be made in 1947, as the film industry struggled to recover from the destruction of the final war years.  The film was based on a novel by Ferenc Móra. It was the second film directed by Szőts, following up his well-received 1942 debut People of the Mountains which featured several of the same cast members. The film was banned in Hungary because of its depiction of the controversial issue of Hungarian prisoners held by the Soviet Union|Soviets. The Hungarian Communist leader Mátyás Rákosi walked out of a screening in protest, and the film was not shown again in Hungary until 1979.  Szőts next scheduled film Treasured Earth was taken away from him and given to Frigyes Bán to direct.

==Cast==
* Alice Szellay as Etel 
* János Görbe as Ferenc 
* József Bihari as Mátyás
* Marcsa Simon   
* László Bánhidi   
* Lajos Gárday  
* Jenö Danis
* László Joó   
* Erzsi Kõmíves
* Viola Orbán   
* Károly Rácz  
* Eszter Sándor  
* Gyöngyi Váradi

==References==
 

==Bibliography==
* Cunningham, John. Hungarian Cinema: From Coffee House to Multiplex. Wallflower Press, 2004.

==External links==
* 

 
 
 
 
 
 
 
 

 