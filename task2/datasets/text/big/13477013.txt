The Dinosaur and the Missing Link: A Prehistoric Tragedy
{{Infobox film
| name           = The Dinosaur and the Missing Link: A Prehistoric Tragedy
| image          = The Dinosaur and the Missing Link.ogv
| image_size     = 250px
| caption        = The complete film
| director       = Willis H. OBrien
| producer       = Herman Wobbler (producer)
| writer         =
| starring       = See below
| cinematography = Willis H. OBrien
| editing        =
| distributor    =
| released       =  
| runtime        = 5 minutes
| country        = United States
| language       =
| budget         =
| gross          =
| website        =
}}

The Dinosaur and the Missing Link: A Prehistoric Tragedy is a 1917 American comedy silent film animated by Willis OBrien that premiered in 1915, and is one of OBriens only wholly animated films. It was financed and distributed by Thomas Edison.

The film is also known as The Dinosaur and the Baboon (American reissue title).

== Plot ==
It starts with a man going to give some flowers to a girl. He fails when he hits a tree. However, he keeps going. However, the "Self Appointed Hero" of the story steals the girls heart. Meanwhile, an evil Gorilla-like creature called the Missing Link is watching them. When the Missing Link goes to the lake to get some water, where the dinosaur is, the dinosaur kills the Missing Link and goes away. Then the "Hero" finds the Missing Link and takes the credit for killing it.

== External links ==
* 
* 
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 