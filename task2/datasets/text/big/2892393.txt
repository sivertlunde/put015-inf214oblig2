Silent Night, Deadly Night 4: Initiation
{{Infobox film
| name = Silent Night, Deadly Night 4: Initiation
| image = Silent Night Deadly Night 4.jpg
| image_size = 215px
| alt = 
| caption = VHS cover
| director = Brian Yuzna
| producer = Richard N. Gladstein
| screenplay = Woody Keith Arthur H. Gorson S.J. Smith
| starring = Clint Howard Neith Hunter Tommy Hinkley Reggie Bannister Allyce Beasley Maud Adams
| music = Richard Band
| cinematography = Philip Holahan
| editing = Peter Teschner
| studio = Silent Films, Inc. Live Video
| released =  
| runtime = 86 minutes
| country = United States
| language = English
}}
Silent Night, Deadly Night 4: Initiation is a 1990 American   (1991). The plot focuses on a Los Angeles newspaper reporter who, while investigating the unexplained death of a woman, becomes entangled with a group of witches who are preparing her for their ritual on Christmas Eve.

==Plot==
  classified ads editor. Her boss, Eli, seems to give all of the men in her office the breaks, including her boyfriend Hank. When a woman is discovered dead on the sidewalk, half-burned into ashes in an apparent case of the spontaneous human combustion, Kim decides to pursue the story on her own without Elis approval. Her first stop is The Munn Fresh Meat, a butcher shop run by Jo near the site of the suicides landing. Kim asks him about the jumper. He tells her that the suicide probably got to the roof of the building she jumped throughout only, from the apartments there.

Around the corner, she enters a used book store. Kim asks for a book on the spontaneous combustion, which the proprietor, Fima, finds for her. At the checkout, she insists that Kim take another book from her, The Initiation of the Virgin Goddess. Kim initially refuses, but Fima insists and declares it to her invitation to a picnic shes having with some friends for the next day. Fima shows Kim the way to the roof of the building. Kim inspects the spot where the jumper leapt from and tests it for herself by standing on the ledge. Behind her, Ricky (a slave) arrives. He sticks his head in a ventilation pipe as Kim approaches him. From inside, Ricky pulls out a large, slimy insect larva. He shows it to Kim, who screams and runs off.

Back in her own apartment, Kim discovers her sink is full of cockroaches; she frantically sprays the insecticide right onto them. She settles down on the kitchen table to eat her dinner and flips open the book that Fima has loaned her. The page she randomly arrives at is still, titled "The Spiral: Symbol of Womens Power". Kim glances at her plate full of noodles and notices that they are arranged in a vague spiral. She spies another roach crawling out of a loaf of bread. Using the book, she bats at it. In the process, her spaghetti dish crashes to the floor. Since while leaving it onto the floor, she leaves to spend the rest of her Christmas Eve with Hanks family.
 Eve was made from Adam and Eve|Adams rib. Hank testily replies that that story is just an allegory, but Gus disagrees, claiming it is "goddamned real".

When Kim returns to her apartment, she scrapes the dead roach from Fimas book and flips it open. The page she randomly lands on is titled "The Fire of Lilith" and it depicts a woman whose lower half is engulfed into flames. Kim glances down at the plate of spaghetti on the floor. The spaghetti is now arranged into a flame-like pattern. Across from the spaghetti, she thinks she see a giant roach under the couch. She throws the book at it and it scurries off. She looks, but is unable to find it. While looking, she notices the spaghetti on the floor pulsating. She gets violently ill and passes out on the floor. Knocking on the door from her coworker Janice wakes her up the next day. Kim decides not to go to work and drives to Fimas picnic instead. Since when she makes it there, the motherly Fima introduces her to Katherine Harrison (Jeanne Bates), a self-described old crone, and the young Jane Yanana. They tell her about Lilith, Adams first wife and the "spirit of all that crawls". Kim begins to feel the effects of the wine they are drinking and lays down. Just as she begins to see something in the tree branches above her, Hank arrives and drives her to work.

At the Eye, Eli, instead of being angry about Kim not showing up for work all morning, lets her officially have the spontaneous combustion story. Kim wonders how Hank knew where to find her. He says that Janice told him. When Kim asks Janice how she knew, Janice claims that Kim herself had told her. Hank and Kim return to the roof to investigate the story much more further. There, they find a spiral drawn onto the ground as well as Rickys lean-to. Hank leaves and Kim decides to visit Fimas apartment. Fima serves her a cup of tea, which makes Kim nauseated at first and later ends up making her sleepy. Fima tells Kim of her daughter Lilith, whom Kim reminds her of. Fima offers her another date and demands that Kim to eat it. She does, even though it looks like a roach in her hand. Soon after that, Kim passes out.

She wakes up being surrounded by Jane, Fima, Katherine, and Li; they strip the parts of her clothes and paint a dark line from her forehead to her chin and a spiral around her navel. Ricky brings in the same larva from the ventilation pipe, and he places it on her stomach. As Katherine chants a spell and Ricky and Fima slice open a live rat over her, the larva crawls into Kims body. Under her skin, the larva crawls up Kims body. It emerges from her mouth as a full-grown, giant, multi-segmented roach; Kim vomits the creature out. Ricky slices the creature in half and drips its innards onto Kims face in a near-sexual manner. Kim wakes up later fully dressed, still in Fimas apartment. The four women encourage her to stay, but she escapes and runs home. There she finds Hank waiting for her. The stressed Kim starts a fight with him, but he calms her down and they begin to make love. While doing so, Ricky casually enters the apartment, sits on their bed and begins watching TV. When he flips to a channel showing a clip from  , he laughs. The noise alerts Hank, who demands that Ricky leave. They scuffle and Hank is stabbed to death by Ricky. Kim manages to answer her ringing phone during the fight and screams for Janice to help her. Ricky captures Kim and binds her. Janice arrives, but doesnt help Kim. Instead, she admonishes Ricky for the mess and tells him to take Kim straight to Fima while she cleans up.
 phallic mask, rapes Kim. Kim reawakens alone in the meat locker; at first her fingers bind themselves together in a knot. Then she experiences incredible pain as her legs bind together into an insect-like tail. Kim passes out one more time. She awakens in the meat locker as Jo opens the door. She frees her legs from a brittle cocoon-like substance and covers her as best as she can. Jo tells her that she has been initiated and that she should rather go.

Kim returns to Fimas bookstore where Fima admits that the woman who jumped was her daughter Lilith and that she wants Kim to take her place. She explains that the experience that Kim just have freed her from the men and the fear that they bring. Kim believes she was simply hallucinating, but Fima insists that she created everything that she saw using the magic within her. Fima then reveals that she must kill a man in order to complete her initiation or else she will be consumed by fire just as Lilith was, and Kim refuses. Kim brings a policeman, Detective Burt, to her apartment. There, everything is spotless and theres no trace of Hanks body. At her offices Christmas party, Eli claims that Hank is away on the assignment page. Janice there welcomes her to the family. Furious and confused, Kim storms out of the office and walks down onto the sidewalk. She notices Ricky following her and ducks into a motel room. Her feet begin to get painfully hot. She jumps into the shower, but they still burst into tiny flames. Ricky enters the room and, in pain, Kim agrees to kidnap Lonnie to complete the initiation.

Kim lures Lonnie out of Hanks parents house and into a waiting van. When Ann goes to investigate, Ricky tapes her mouth shut and strangles and electrocutes Gus with a string full of Christmas lights. As the house begins to burn, Ricky jumps into the van and they drive back to Fimas rooftop. With Lonnie tied up on the roof, Fima hands Kim a knife and tells her to kill him. Kim refuses and stabs Fima instead. In anger, Fima pulls the knife from her belly and stabs Ricky. A giant larva feed on the wounded man as Kims legs begin to get hot. Kims hands knot themselves together once again, then they start to burn on fire. She stabs her modified, flaming hands into Fimas knife wound. This transfers the curse of Lilith to her, who dives off the roof just as her daughter had. Kim hugs Lonnie and tells him that its all over.

Clint Howard and Neith Hunter reprise their characters of Ricky and Kim for The Toy Maker, although both only make some minor appearances. Ricky is alive and working as a shopping mall Santa in the film, while Kim has become Lonnies legal guardian after the death of Ann and Gus.

==Cast==
* Clint Howard as Ricky
* Neith Hunter as Kim
* Tommy Hinkley as Hank
* Reggie Bannister as Eli
* Allyce Beasley as Janice
* Maud Adams as Fima
* Hugh Fink as Jeff
* Richard N. Gladstein as Woody
* Glen Chin as Jo
* Jeanne Bates as Katherine
* Laurel Lockhart as Ann
* Ben Slack as Gus
* Conan Yuzna as Lonnie
* Marjean Holden as Jane
* Ilsa Setzoil as Li
* David Wells as Detective Burt

==Release== Live Home Video in conjunction with the Thanksgiving week, and just before the holiday season.  Image Entertainment released it on laserdisc.  

In December 2009, it was released on   and The Toy Maker by Lions Gate Entertainment.  That DVD set is now out of print and unlike the third film, this film and the fifth film havent been reissued on DVD by Lions Gate yet.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 