Salaakhen (1975 film)
{{Infobox film
| name           = Salaakhen
| image          = Salaakhen_poster.jpg
| image_size     = 
| caption        = 
| director       = A. Salaam
| producer       = Parvesh C Mehra
| writer         = 
| narrator       =  Mehmood
| music          = Ravindra Jain
| cinematography = 
| editing        = 
| distributor    = Shemaroo Video Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Salaakhen is a 1975 Hindi movie produced by Parvesh Mehra and directed by A. Salaam. The film stars Shashi Kapoor, Sulakshana Pandit, A. K. Hangal, Mehmood Ali|Mehmood, Amrish Puri and Ramesh Deo. The films music is by Ravindra Jain

==Plot==
Raju (Shashi Kapoor) and Guddi (Sulakshana Pandit) are childhood friends and neighbors, who are virtually inseparable. Rajus father is arrested after a dramatic police chase for break, enter, and theft resulting in their separation. Guddi grows up to be a professional stage singer and dancer, while Raju grows up to be a card-sharp and a thief. Years later, both Raju (now called Chander) and Guddi (now called Seema) meet and fall in love with each other, unaware that they were childhood friends. While Seema is on her way to her birthplace for religious reasons, Chander too is headed that way, to get himself arrested so that he can be jailed for a motive, that gets him a hefty sum of money from a gangster.

==Cast==
*Shashi Kapoor - Raju/Chander
*Sulakshana Pandit - Guddi/Seema
*Goga Kapoor - Casino Owner
*A. K. Hangal - Ram Lal, Seemas father Mehmood - Abdul Rehman
*Amrish Puri - Master
*Anjana Mumtaz - Farida
*Ramesh Deo - Gautam
*Pinchoo Kapoor - Rich Man
*Mac Mohan - Prince
*Moolchand - Shopkeeper
*Shivraj - Advani Sudhir - Inspector Ahmed
*Shammi Kapoor

==Crew==
*Director - A. Salaam
*Producer - Parvesh C. Mehra
*Music Director - Ravindra Jain
*Lyricist - Dev Kohli, Hasrat Jaipuri, Ravindra Jain
*Playback Singers - Asha Bhosle, Hemlata, Kishore Kumar, Sulakshana Pandit

==Music==
*Song "Seema Seema Seema" was listed at #29 on Binaca Geetmala annual list 1976 
{|class="wikitable"
|-
!Song Title !!Singers !!Lyricist !!Time
|-
|"Chal Chal Kahin Akele Mein" Sulakshana Pandit Dev Kohli
|1:44
|-	
|"Chal Chal Kahin Akele Mein" Sulakshana Pandit, Hemlata Dev Kohli
|6:39
|-	
|"Maze Uda Lo Jawani Rahe Na Rahe" Asha Bhosle Hasrat Jaipuri
|5:56
|-	
|"Mere Dekh Ke Lambe Baal" Asha Bhosle Hasrat Jaipuri
|5:47
|-	
|"Seema Seema Seema" Kishore Kumar, Asha Bhosle Ravindra Jain
|5:51
|-
|}

== External links ==
*  

 
 
 

 