Warlock (1989 film)
{{Infobox film
| name           = Warlock
| image          = Warlocksands.jpg
| caption        = American theatrical release poster
| image_size     = 250px
| director       = Steve Miner
| producer       = Steve Miner
| writer         = David Twohy
| starring       = Julian Sands Lori Singer Richard E. Grant
| music          = Jerry Goldsmith
| cinematography = David Eggby
| editing        = David Finfer
| distributor    = Trimark Pictures New World Pictures
| released       = December 1, 1989 (USA)
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $9,054,451
}} cult horror film produced and directed by Steve Miner and starring Julian Sands, Lori Singer, and Richard E. Grant. It was written by David Twohy. The soundtrack was by Jerry Goldsmith.

== Plot ==
The Warlock (Sands) is taken captive in Boston, Massachusetts in 1691 by the witch-hunter Giles Redferne (Grant). He is sentenced to death for his activities, including the bewitching of Redfernes bride-to-be, but before the execution Satan appears and propels the Warlock forward in time to 20th century Los Angeles, California. Redferne follows through the portal.

The Warlock attempts to assemble The Grand Grimoire, a Satanic book that will reveal the "true" name of God.  Redferne and The Warlock, then embark on a cat-and-mouse chase with the Grand Grimoire, and Kassandra, a nurse who encounters Giles attempting to use a witch compass to track the Warlock. Explaining some basic rules of the Warlock, such as their weakness to purified salt, Kassandra follows Giles after seeing the compass work.

The Warlock assembles two thirds of the grimoire, and Giles is stunned to learn that the last portion is buried in his grave, buried off of church lands under the cursed sign of a witch. The Warlock appears and starts a ritual to assemble the Grimoire. After seeing the name of God appear on the book, the Warlock is about to call it out and unmake existence when Kassandra injects him with saline, and he bursts into flame. Redferne returns to his own time. In the epilogue, Kassandra is seen burying the Grimoire in the Great Salt Flats.

== Cast ==
* Julian Sands as Warlock
* Lori Singer as Kassandra
* Richard E. Grant as Giles Redferne
* Mary Woronov as Channeler Kevin OBrien as Chas
* Richard Kuss as Mennonite

== Production ==
=== Cut scenes ===
==== The Channeler or Breaking Woman ====
In the original version of the channeling sequence, actress Mary Woronov ripped open her blouse to reveal the "Eyes of Satan" in place of her nipples. The Warlock freezes her and pushes her to the floor, making her body shatter. Then the Warlock stomps on her chest to retrieve the eyes. This shot was cut after test screenings resulted in laughter at the prosthetic breasts and disapproval over the violent nature of the channelers death.

It is doubtful Julian Sands returned for the re-shoot and that could explain the change in Mary Woronovs hair from the beginning of the shot to the moment when she collapses on the table. The filmmakers also used a shot of the Warlock kneeling down over the channelers broken body instead of a new shot of Sands. The film was far enough in production and a quick glimpse of the frozen body with the Warlock beside it was included in the trailer. There are videos on Internet that feature the scene, but only as a reconstruction based on the remains of the original footage. 

==== Rooster compass ====
One promotional shot showed the Warlock squatting down inside a circle with a rooster tied to the center. This was explained in the novelization as a compass that the Warlock uses to track down Redferne and Kassandra. The film omits any explanation as to how the Warlock found the two in the airport.

== Release ==
Although completed in 1988, Warlock fell into release limbo in the United States when New World Pictures suffered financial difficulties, it was shelved for two years. The film was eventually picked up by Trimark Pictures and given a limited release beginning in January 1991.  The film turned into a modest success for Trimark, grossing $9,094,451 and becoming the companys biggest grosser until Eves Bayou. 

== Sequels and legacy ==
A sequel was made in 1993 and titled  , starring again Julian Sands. A   came in 1999, this time with Bruce Payne in the title role.
 video game Super NES Sega Genesis in 1995. Bluewater Productions also began a Warlock comic book series in 2009. 

== See also ==
* List of films featuring diabetes

== References ==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 