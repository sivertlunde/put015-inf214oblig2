Singin' in the Rain
 
{{Infobox film
| name           = Singin in the Rain
| image          = Singing in the rain poster.jpg
| caption        = Theatrical release poster
| director       = {{Plain list|
*Gene Kelly
*Stanley Donen
}}
| producer       = Arthur Freed
| screenplay     =  {{Plain list|
*Betty Comden
*Adolph Green
}}
| story          =  {{Plain list|
*Betty Comden
*Adolph Green
}}
| starring       = {{Plain list|
*Gene Kelly
*Donald OConnor
*Debbie Reynolds
*Jean Hagen
*Millard Mitchell
*Cyd Charisse
}}
| music          = Nacio Herb Brown
| cinematography = Harold Rosson
| editing        = Adrienne Fazan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $2.5 million      
| gross          = $7.7 million  
}}
  musical Comedy comedy film choreographed by Kelly and Donen. It offers a lighthearted depiction of Hollywood in the late 20s, with the three stars portraying performers caught up in the transition from silent films to "Sound film|talkies."
 Oscar nomination list of the greatest American films in 2007.

== Plot ==
Don Lockwood is a popular silent film star with humble roots as a singer, dancer and stuntman. Don barely tolerates his vapid, shallow leading lady, Lina Lamont, though their studio, Monumental Pictures, links them romantically to increase their popularity. Lina herself is convinced they are in love, despite Dons protestations otherwise.
 flashbacks showing him taking on a wide range of menial and humiliating roles on stage and in film alongside his best friend Cosmo Brown.
 talking picture  but his guests are unimpressed. To Dons amusement and Kathys embarrassment, she pops out of a mock cake right in front of him as part of the entertainment, revealing herself to be a chorus girl. Furious at Dons teasing, she throws a real cake at him, only to hit Lina right in the face. Don is smitten with her, but she runs off into the night. Don searches for her for weeks after discovering she was fired, believing himself to be responsible, but Lina tells him while filming a love scene that she made sure Kathy lost her job as an act of revenge and jealousy. Later, Don finds Kathy working in another Monumental Pictures production and they apologize to each other. She confesses to having been a fan of Don all along and they begin to fall in love.

After a rival studio has an enormous hit with its first talking picture, 1927s The Jazz Singer, R.F. decides he has no choice but to convert the next Lockwood and Lamont film, The Dueling Cavalier, into a talkie. The production is beset with difficulties in capturing sound, but by far the worst problem is Linas grating voice. An exasperated diction coach tries to teach her how to speak properly, but to no avail. Don also takes diction lessons (albeit with much better results). The Dueling Cavaliers test screening is a disaster; the actors speaking is barely audible thanks to the awkward placing of the microphones, Don repeats the line "I love you" to Lina over and over, to the audiences derisive laughter,  and in the middle of the film, the sound goes out of synchronization, with hilarious results.

After the premiere, Don, Kathy and Cosmo come up with the idea to turn The Dueling Cavalier into a musical called The Dancing Cavalier, complete with a modern musical number called "Broadway Melody". Don will be able to show off his natural singing and dancing talent, but they are stumped when they must think about what to do with Lina. Cosmo, inspired by a scene in "The Dueling Cavalier" where Linas voice was out of sync, suggests they dub Linas voice with Kathys. They bring the idea to R.F., who goes ahead with it. When Lina finds out, she is infuriated. She becomes even angrier when she discovers that R.F. intends to give Kathy a screen credit and a big publicity promotion. Lina, after consulting lawyers, threatens to sue R.F. unless he cancels Kathys buildup and orders her to continue working uncredited as Linas voice. R.F. reluctantly agrees to her demands.

The premiere of The Dancing Cavalier is a tremendous success. When the audience clamors for Lina to sing live, Don, Cosmo, and R.F. improvise and get her to lip sync into the microphone while Kathy, hidden behind the stage curtain, sings into a second one. While Lina is "singing", Don, Cosmo and R.F. gleefully raise the curtain. When Cosmo replaces Kathy at the microphone, the sham becomes obvious. Embarrassed, Lina flees in humiliation. A distressed Kathy tries to run away as well, but not before Don proudly announces to the audience that shes "the real star of the film". The final shot shows Kathy and Don kissing in front of a billboard for their new film, Singin in the Rain.

== Cast ==
*  and Turner Classic Movies. 
*Donald OConnor as Cosmo Brown. The role was based on, and initially written for, Oscar Levant.
*Debbie Reynolds as Kathy Selden. Early on in production, Judy Garland (shortly before her contract termination from MGM), Kathryn Grayson, Jane Powell, Leslie Caron, and June Allyson were among the names thrown around for the role of the "ingenue." However, director Stanley Donen and Gene Kelly insisted that Debbie Reynolds always was first in their mind for the role.  Although the film revolves around the idea that Kathy has to dub over for Linas voice, in the scene where Kathy is dubbing a line of Linas dialogue ("Our love will last til the stars turn cold"), Jean Hagens normal voice is used. Reynolds herself was dubbed in "Would You?" and "You are My Lucky Star" by an uncredited Betty Noyes.   
*Jean Hagen as Lina Lamont. Judy Holliday was strongly considered for the role of Lina, until she suggested Hagen, who had been her understudy in the Broadway production of Born Yesterday. Fresh off her role in The Asphalt Jungle, Hagen read for the part for producer Arthur Freed and did a dead-on impression of Hollidays Billie Dawn character, which won her the role. Her character was based on the silent picture star Norma Talmadge who bombed during the transition to talkies.
*Millard Mitchell as R.F. Simpson. The initials of the fictional head of Monumental Pictures are a reference to producer Freed. R.F. also uses one of Freeds favorite expressions when he says that he "cannot quite visualize it" and has to see it on film first, referring to the Broadway ballet sequence, a joke, since the audience has just seen it.
*Cyd Charisse as Gene Kellys dance partner in the "Broadway Melody" ballet
*Douglas Fowley as Roscoe Dexter, the director of Don and Linas films
*Rita Moreno as Zelda Zanders, the "Zip Girl" and Linas informant friend. Considered to be based on Clara Bow (The It Girl).
*King Donovan (uncredited) as Rod, head of the publicity department at Monumental Pictures.
*Judy Landon (uncredited) as Olga Mara, a silent screen vamp who attends the premiere of The Royal Rascal. She is considered to be based on Pola Negri and Gloria Swanson.
*Madge Blake (uncredited) as Dora Bailey, a radio show host. Considered to be based on Louella Parsons.
*Kathleen Freeman (uncredited) as Phoebe Dinsmore, Linas diction coach Bobby Watson (uncredited) as diction coach during "Moses Supposes" number
*Jimmy Thompson (uncredited) as the singer of "Beautiful Girl"
*Mae Clarke (uncredited) as the hairdresser who puts the finishing touches on Lina Lamonts hairdo

== Songs ==
Singin in the Rain was originally conceived by MGM producer Arthur Freed, the head of the "Freed Unit" responsible for turning out MGMs lavish musicals, as a vehicle for his catalog of songs written with Nacio Herb Brown for previous MGM musical films of the 1929-39 period. George Feltenstein (2002). "Producers Note," included in the liner notes of the "Music from the original motion picture soundtrack (deluxe edition) Singin in the Rain" double CD by Rhino Entertainment and Turner Classic Movies  Screenwriters Betty Comden and Adolph Green contributed lyrics to one new song. Track list in the liner notes of the "Music from the original motion picture soundtrack (deluxe edition) Singin in the Rain" double CD by Rhino Entertainment and Turner Classic Movies. 

All songs have lyrics by Freed and music by Brown, unless otherwise indicated.  Some of the songs, such as "Broadway Rhythm", "Should I?", and most notably "Singin in the Rain," were featured in numerous films. The films listed below mark the first time each song was presented on screen.
*"Fit as a Fiddle (And Ready for Love)" from College Coach (1933) CineBooks Motion Picture Guide review of the film included on the Microsoft Cinemania 1997 CD  (music by Al Hoffman and Al Goodhart)
*"Temptation (1933 song)|Temptation" (instrumental only) from Going Hollywood (1933)
*"All I Do Is Dream of You" from Sadie McKee (1934) 
*"Singin in the Rain (song)|Singin in the Rain" from Hollywood Revue of 1929 (1929) 
*"Make Em Laugh" considered an original song, but bearing close relation to Cole Porters "Be a Clown", used in another Freed musical, The Pirate (1948).
*"Beautiful Girl Montage" comprising "I Got a Feelin Youre Foolin" from Broadway Melody of 1936 (1935),  "The Wedding of the Painted Doll" from The Broadway Melody (1929),  and "Should I?" from Lord Byron of Broadway (1930) 
*"Beautiful Girl" from Going Hollywood (1933)  or from Stage Mother (1933)  You Were Meant for Me" from The Broadway Melody (1929) 
*"You Are My Lucky Star" from Broadway Melody of 1936 (1935)  Comden and Adolph Green|Green) Good Morning" Babes In Arms (1939)  San Francisco (1936) 
*"Broadway Melody Ballet" composed of "The Broadway Melody" from The Broadway Melody (1929)  and "Broadway Rhythm" from Broadway Melody of 1936 (1935)  (music by Nacio Herb Brown and Lennie Hayton)

== Cut scenes ==
 
*In an early draft of the script, the musical number "Singin in the Rain" was to be sung by Reynolds, OConnor, and Kelly on their way back from the flop preview of The Dueling Cavalier.
*In addition, "You Were Meant For Me" was not included in that draft. Instead, the love song was supposed to be Gene Kellys version of "All I Do is Dream of You", which would be sung after the party at R. F. Simpsons house, when Kelly chases after Reynolds. The song would have ended up at Kellys house. The footage of this scene has been lost. 
*Reynolds solo rendition of "You Are My Lucky Star" (to a billboard showing an image of Lockwood) was cut from the film, but has survived and is included on the original soundtrack and DVD version of the film.  
*Rita Moreno was originally to have sung "I Got a Feelin Youre Foolin", but this ended up being part of the "Beautiful Girl" medley.
*Due to being seen as too sexy for 1952 film audiences, seven seconds of footage were cut from the Gotta Dance number featuring Kelly and Charisse and the scene was re-scored with the fairly noticeable jump cut remaining in the middle of the shot. As of 2015, the seven seconds of footage excised from the scene remains lost to time.

== Production ==
  dancing while singing the title song "Singin in the Rain"]]
In the famous dance routine in which Gene Kelly sings the title song while twirling an umbrella, splashing through puddles and getting soaked to the skin, Kelly was sick with a   fever.  The rain in the scene caused Kellys wool suit to shrink during filming. A common myth is that Kelly managed to perform the entire song in one take, thanks to cameras placed at predetermined locations. However this was not the case as the filming of the sequence took place over 2–3 days.    Another myth is that the rain was mixed with milk in order for the drops to show up on camera; the desired visual effect was produced, albeit with difficulty, through Backlighting (lighting design)|backlighting.  

Debbie Reynolds was not a dancer at the time she made Singin in the Rain; her background was as a gymnast. New 50th Anniversary Documentary What a Glorious Feeling, hosted by Debbie Reynolds on the films DVD.  Kelly apparently insulted her for her lack of dance experience, upsetting her. In a subsequent encounter when Fred Astaire was in the studio, he found Reynolds crying under a piano. Hearing what had happened, Astaire volunteered to help her with her dancing. Kelly later admitted that he had not been kind to Reynolds and was surprised that she was still willing to talk to him afterwards. After shooting the "Good Morning" routine, Reynolds feet were bleeding.  Years later, she was quoted as saying that "Singin in the Rain and childbirth were the two hardest things I ever had to do in my life." 

Donald OConnor had to be hospitalized after filming the "Make em Laugh" sequence. He smoked up to four packs of cigarettes a day. 

Most of the costumes from this film were eventually acquired by Debbie Reynolds and housed in her massive collection of original film costumes, sets and props. Many of these items were sold at a 2011 auction in Hollywood. While most items were sold to private collectors, Donald OConnors green check "Fit As a Fiddle" suit and shoes were purchased by Costume World, Inc. and are now on permanent display at the Costume World Broadway Collection Museum in Pompano Beach, Florida. 

== Reception ==
  Singin in the Rain, McGredy 1994]]
According to MGM records, during the films initial theatrical release it made $3,263,000 in the US and Canada and $2,367,000 internationally, earning the studio a profit of $666,000.  .  It was the tenth highest grossing movie of the year in the US and Canada.    

== Awards and honors == Best Original Music Score.

Donald OConnor won a Golden Globe for this film.  Betty Comden and Adolph Green received the Writers Guild of America for the best written American musical. 

Singin in the Rain has appeared twice on Sight and Sounds list of the ten best films of all time, in 1982 and 2002. Its position in 1982 was at number 4 on the critics list; on the 2002 critics list it was listed as number 10 and it tied for 19 on the directors list.  Review aggregate website Rotten Tomatoes reported that 100% of critics gave the film a positive review based on 47 reviews, with an average score of 9.2/10. The film is currently No. 14 on Rotten Tomatoes list of best rated films.  Rotten Tomatoes summarizes the critical consensus as, "Clever, incisive, and funny, Singin in the Rain is a masterpiece of the classical Hollywood musical."  In 2008, Singin in the Rain was placed on Empires 500 Greatest Movies of All Time List, ranking at #8, the highest ranked G-rated movie on the list.

In 1989, Singin in the Rain was among the first 25 films chosen for the newly established National Film Registry for films that are deemed "culturally, historically or aesthetically significant" by the United States Library of Congress and selected for preservation.
 
American Film Institute recognition
*AFIs 100 Years... 100 Movies – #10
*AFIs 100 Years... 100 Laughs – #16
*AFIs 100 Years... 100 Passions – #16
*AFIs 100 Years... 100 Heroes and Villains:
**Lina Lamont – Nominated Villain
*AFIs 100 Years... 100 Songs:
**"Singin in the Rain (song)|Singin in the Rain" – #3
**"Make em Laugh" - #49 Good Morning" – #72
*AFIs 100 Years... 100 Movie Quotes:
**"What do they think I am, dumb or something? Why, I make more money than Calvin Coolidge! Put together!" – Nominated
*AFIs Greatest Movie Musicals - #1
*AFIs 100 Years... 100 Movies (10th Anniversary Edition) – #5

== Home video ==
The 40th Anniversary Edition VHS version released in 1992 includes a documentary, the original trailer, and Reynolds solo rendition of "You Are My Lucky Star," which had been cut from the final film. 
 audio commentary on the 2002 Special Edition DVD, the original negative was destroyed in a fire, but despite this, the film has been digitally restored for its DVD release.   A Blu-ray edition was released in July 2012.

== In popular culture == plot had been used in the 1946 French film Étoile sans lumière (aka "Star without Light"), directed by Marcel Blistène, starring Edith Piaf and Mila Parély and later in the 1959 British film Follow a Star, directed by Robert Asher and starring Norman Wisdom and Jerry Desmonde.
*African-American filmmaker Julie Dashs 1982 short Illusions by Julie Dash|Illusions took a more serious look at the concept of actor looping, depicting a black actress in 1942 Hollywood providing the singing voice for a white actress, at the behest of a female studio executive who herself is a light-skinned black woman passing for white.
*The German slapstick show Nonstop Nonsens used the melody of "Make Em Laugh" as the main Theme music|theme.
*It has long been tradition at Kellys hometown Pittsburgh Pirates games at PNC Park to play the scene from the film during rain delays.  Alex DeLarge, A Clockwork Orange (1971). The Gene Kelly version was played during the end credits. An urban legend was that Gene Kelly was reportedly disgusted when it was used in the film and walked away from McDowell at a party. Kellys widow Patricia Ward Kelly has clarified that his displeasure was not with the violent juxtaposition, but that he was not paid for the use of his recording by Kubrick, which he saw as disrespect since they had previously been friends. Compo (Bill Bill Owen) outside Nora Battys (Kathy Staffs) house in an episode of Last of the Summer Wine. In Fame (1980 film)|Fame, Coco Hernandez (played by Irene Cara) dances in a puddle on a train station while singing "Singin in the Rain".
*World Champion Canadian figure skater Kurt Browning did an on-ice (and water) recreation in a Stars on Ice television special. 
*In Legal Eagles, Robert Redford sings and dances "Singin in the Rain" when he cannot sleep.
*In the Pixar film Wall-E (2008), the robot Wall-E briefly swings himself around a light pole in an echo of the "Singin in the Rain" number, even though the titular song was not used in the film.
*Gene Kellys "Singin in the Rain" sequence is one of the opening scenes of The Great Movie Ride at Disneys Hollywood Studios. Kelly approved his Audio-Animatronics likeness prior to its delivery to Florida.
*The "Good Morning" song is used as part of the opening show at Disney Worlds Magic Kingdom each morning. 
*The dance to the title song is parodied in the Broadway musical Spamalot in the dance break to "Always Look on the Bright Side of Life," complete with tap dancing in raincoats and twirling umbrellas.
*In The Simpsons episode "Brawl in the Family", Groundskeeper Willie was singing with an umbrella, but in acid rain. Cleveland and Stewie performed a partial rendition of the "Good Morning" dance routine.  Also, in the episode, "Peterotica", Quagmire recreates the "Make Em Laugh" number. The Substitute", Will Schuester and Mike Chang perform the "Make Em Laugh" song and dance routine as a duo (which was the hallucination of a sick Will). The cast also performs a mash-up of "Singin in the Rain" and Rihannas "Umbrella (song)|Umbrella" at the end of the episode.
*In the 1998 American remake of the film Godzilla (1998 film)|Godzilla, the main character Dr. Niko Tatopoulos sings "Singin in the Rain". Volkswagen Golf GTI commercial features Kellys "Singin in the Rain" sequence, but with Kelly performing hip-hop dance moves and a remixed version of the song playing in the background.
*In the 2003 film Shanghai Knights, Jackie Chan imitates Gene Kellys dance moves (complete with umbrella) towards the end of a fight scene set in London, England. The original "Singin in the Rain" music plays over this sequence as a tribute.
*On The Muppet Show, the Muppets want Gene Kelly to recreate "Singin in the Rain" on a reconstructed set, though all he finally does is hum it as he crosses the stage.
*on the animated series The Shoe People, the character Wellington sings the song. However, it isnt raining; he simply says "I know its not raining really, but I can dream cant I?".
*Joseph Gordon-Levitt sang "Make Em Laugh" during the opening monologue when he hosted Saturday Night Live.
*On Fringe (TV series)|Fringe, after he is removed from the Observer Corps, September takes the name Donald OConnor because Singin in the Rain was the first movie he watched with Walter Bishop.
*In the movie Silver Linings Playbook, Tiffany and Pat watch a clip of "Moses Supposes" and copy some of the dance moves for their routine.
*In an episode of Sesame Street, Grover stars in a production of "Singin in the Rain", where the special effects are different forms of weather from rain such as snow or wind. When they finally get rain, Grover yells "Cut!" When asked why, Grover replies "I dont want to get my fur wet!".
*In the episode of The Munsters called Dance With Me Herman, Herman Munster sings the first few bars of the title song Singin in the Rain, after which he falls over a couch.
*On the album All Samples Cleared!, Biz Markie sings parts of "Singin in the Rain" on the song Im Singin.
*In the movie, What About Bob?, Bill Murray sings an altered version of "Singing in the Rain" while dancing through the kitchen with a bowl full of chicken.

== See also == Love Me or Leave Me
*Thoroughly Modern Millie
*List of films considered the best

== References ==
 

== Notes ==
 

== Further reading ==
*Comden, Betty, and Green, Adolph. Singin in the Rain. New York: Lorrimer Publishing Limited. 1986. ISBN 0 85647 116 X.
*Hess, Earl J., and Pratibha A. Dabholkar. Singin’ in the Rain: The Making of an American Masterpiece (Lawrence: University Press of Kansas, 2009). ISBN 978-0-7006-1656-5.
*Wollen, Peter. Singin in the Rain. London: BFI Publishing. 1992. ISBN 0-85170-351-8.

== External links ==
 
 
* 
*  at the American Film Institute Catalog of Motion Pictures
* 
* 
* 
* 
* 
* 
*   
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 