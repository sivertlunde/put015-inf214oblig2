Igor (film)
{{Infobox film
| name = Igor
| image = Igorposter.jpg
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Tony Leondis
| producer = John D. Eraklis Max Howard Matthew J. Parker Darius A. Kamali Chris McKenna John Hoffman Dimitri Toscas Tony Leondis
| story = Chris McKenna Sean Hayes Jennifer Coolidge Eddie Izzard Jay Leno Arsenio Hall Christian Slater John Cleese
| music = Patrick Doyle 
| cinematography = Dominique Monfery
| editing = Hervé Schneid 
| studio = Exodus Film Group Sparx Animation Studios
| distributor = Metro-Goldwyn-Mayer   Entertainment One  
| released =  
| runtime = 86 minutes
| country = United States
| language = English
| budget = $25 million   
| gross = $30,747,504 
}} computer animated fantasy comedy Igor who dreams of winning first place at the Evil Science Fair. Produced by Exodus Film Group and animated by Sparx Animation Studios,    the film was released on September 19, 2008 by MGM. 
 Chris McKenna, John Hoffman Sean Hayes, Jennifer Coolidge, Arsenio Hall, Eddie Izzard, Jay Leno, Christian Slater and John Cleese.

==Plot==
The kingdom of Malarias environment is devastated by a mysterious storm. Its king, Malbert then blackmails the rest of the world to pay the town not to unleash the various doomsday devices invented by its Evil Scientists. They in turn are assisted by Igor (fictional character)|Igors, while the kingdoms annual Evil Science Fair showcases the scientists latest weapons. One Igor, however, who serves the somewhat tedious-minded Doctor Glickenstein, is a talented inventor who aspires to be an Evil Scientist himself. Among his inventions are his friends "Scamper," a re-animated, immortal rabbit with a death wish and "Brain," a unintelligent human brain transplanted into a life support jar.
 brainwash her into becoming evil turns her into an aspiring actress instead. Igor decides to exhibit Eva at the science fair anyway, telling her that the fair is an "Annie (musical)|Annie" audition.

Igors nemesis Dr. Schadenfreude learns of Evas existence and attempts to steal her, but when that fails, he exposes Igor to King Malbert, who sends Igor to an "Igor Recycling Plant." Schadenfreude activates Evas Evil Bone and unleashes her into the Science Fair, where she destroys all of the Evil Inventions. Scamper and Brain help Igor escape from the recycling plant and they learn that Malbert had deliberately killed Malarias crops with a weather ray that made storm clouds so he could implement his "Evil Inventions" plan, thereby keeping himself in power. Rushing into the arena, Igor reasons with Eva to deactivate her Evil Bone and allowing her to regain her sweet personality, while Scamper and Brain deactivate the weather ray which falls and crushes Malbert. Schadenfreude attempts to take power but the citizens revolt upon learning of Malberts deception. The monarchy has been dissolved and Malaria normally becomes a republic, with Igor as president. Schadenfreude is relegated to a pickle salesman, while the annual Science Fair is now an annual musical theater showcase.

==Cast==
* John Cusack as Igor
* Molly Shannon as Eva
* Steve Buscemi as Scamper Sean Hayes as Brain
* Jennifer Coolidge as Jaclyn / Heidi
* Eddie Izzard as Dr. Schadenfreude
* Jay Leno as King Malbert
* Arsenio Hall as Carl Cristall
* Christian Slater as Schadenfreudes Igor
* John Cleese as Dr. Glickenstein
* Paul Vogt as Buzz Offmann
* James Lipton as Himself

==Production== Chris McKenna  made his first films story and writings.

===Casting===
Producer   was originally cast as Doctor Schadenfreude, but Eddie Izzard replaced him  and James Lipton appears as himself during a television viewing.

==Reception==
Igor has received generally mixed reviews by critics; it currently holds a 36% rating on   classified the film as having received "mixed or average reviews".  John Anderson of   took his child to a screening and asked him to criticize the film, reporting that he "had a fairly good time".

===Box office=== My Best Friends Girl.  As of January 22, 2009, the film has grossed $19,528,602 in the United States and Canada and $11,218,902 in foreign countries totaling $30,747,504 worldwide.  In the UK, the film opened on 32 screens with a gross of £56,177, for a screen average of £1,756, and placing it at No. 20 in the box office chart. The mainstream release opened on 17 October, at 418 screens, and made £981,750 with a screen average of £2,348. This placed it at No. 3 for that weekend. The UK total gross is £1,110,859. 

==Home media==
The films DVD/Blu-ray Disc|Blu-ray release on January 3, 2009 though Fox Home Entertainment ranked 4th in its opening weekend at the DVD sales chart, making $3,509,704 off 175,000 DVD units. As per the latest figures, 596,146 DVD units have been sold, translating to $11,739,919 in revenue. 

==Soundtrack== Pennies from Heaven - Louis Prima
* Jump Jive n Wail - Louis Prima
* Baby, Wont You Please Come Home - Louis Prima
* Beep! Beep! - Louis Prima
* The Bigger The Figure - Louis Prima & His Orchestra with Keely Smith The Twist - Hank Ballard
* I Can See Clearly Now - Johnny Nash
* Pocketful of Sunshine - Natasha Bedingfield
* Tomorrow - Molly Shannon

==See also==
* List of animated feature films

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 