Sneham (1977 film)
{{Infobox film
| name           = Sneham
| image          =
| caption        =
| director       = A. Bhim Singh
| producer       = KNS Jaffarsha
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi
| starring       = Sukumari Adoor Bhasi Mallika Sukumaran Master Rajkrishna
| music          = Jaya Vijaya
| cinematography =
| editing        = G Murali
| studio         = JS Films
| distributor    = JS Films
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film,  directed by A. Bhim Singh and produced by KNS Jaffarsha. The film stars Sukumari, Adoor Bhasi, Mallika Sukumaran and Master Rajkrishna in lead roles. The film had musical score by Jaya Vijaya.  

==Cast==
 
*Sukumari
*Adoor Bhasi
*Mallika Sukumaran
*Master Rajkrishna
*Master Sridhar
*Nellikode Bhaskaran
*Vidhubala
 

==Soundtrack==
The music was composed by Jaya Vijaya and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Eenam Paadithalarnnallo || Jolly Abraham || Sreekumaran Thampi ||
|-
| 2 || Kaliyum Chiriyum || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 3 || Pakalkkili || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 4 || Sandhyayinnum Pulariye Thedi || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 5 || Swarnam Paakiya || K. J. Yesudas || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 

 
 
 


 