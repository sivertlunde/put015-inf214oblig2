Adieu Gary
{{Infobox film
| name           = Adieu Gary
| image          = Adieu_Gary_Poster.jpg
| alt            = 
| caption        = Adieu Gary poster
| director       = Nassim Amaouche
| producer       =  Jean-Philippe Andraca
*Christian Bérard
 
| writer         = 
| starring       =  Jean-Pierre Bacri
*Dominique Reymond
*Yasmine Belmadi
 
| music          = Le Trio Joubran
| cinematography = Samuel Collardey
| editing        = 
| studio         =  Les Films A4
*Rhône-Alpes Cinéma
*Studio Canal
 
| distributor    = 
| released       =  
| runtime        = 
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
Adieu Gary is a 2009 French film directed by Nassim Amaouche. This is the first feature-film director who was brought over to the International Week of criticism at the 2009 Cannes Film Festival where it won the Grand Prix.

== Plot ==
In the middle of a forgotten French suburb, a family and close friends day-dream of life and love. In this debut feature by writer/director Nassim Amaouche, we become embroiled in their world, discovering the ties that bind the various characters, crafted by a brilliant French-Arab ensemble cast that includes Jean-Pierre Bacri, Dominique Reymond, Yasmine Belmadi and Alexandre Bonnin. The film revolves around conscientious Francis, his recently freed ex-con son Samir, neighbour (and Francis illicit lover) Maria and her imaginative son José - who deals with all this by escaping into a Wild West fantasy world where his father is cowboy hero Gary Cooper.

==Cast==
* Jean-Pierre Bacri as Frenche
* Dominique Reymond as Maria
* Yasmine Belmadi as Samir
* Mhamed Arezki as Icham
* Sabrina Ouazani as Nejma
* Hab-Eddine Sebiane as Abdel
* Alexandre Bonnin as José
* Bernard Blancan as Le voisin de Francis
* Azzedine Bouabba as Azzedine
* Frédéric Hulne as Le médecin
* Mohamed Mahmoud Ould Mohamed as le père dAbdel et Nejma
* Abdelhafid Metalsi as Le nouveau voisin
* Mariam Koné as La vendeuse en ange

==External links==
* 
* 

 
 

 