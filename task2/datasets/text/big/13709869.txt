I Am Because We Are
{{Infobox film
| name           = I Am Because We Are
| image          = I Am Because We Are poster.jpg
| caption        = cover for the DVD to the film
| director       = Nathan Rissman Madonna Angela Becker
| writer         = Madonna
| narrator       = Madonna
| music          = Patrick Leonard
| cinematography = Kevin Brown Grant James Nathan Rissman Marc Shap John Martin White
| editing        = Danny Tull Semtex Films
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom United States Malawi
| language       = English
| budget         = 
| gross          = 
}} Madonna through Semtex Films. 
 Sundance Channel for World AIDS Day. On March 26, 2009, the film was uploaded by Madonnas team onto YouTube and Hulu for free viewing.  

The film won the VH1 Do Something Docu Style Award in 2010.

==Film==
The film documents the lives of orphans in the African country of Malawi, where an estimated 500,000 children have lost parents to HIV and AIDS.  Many of these children live on the streets. The film also shows the efforts with Madonnas charitable organisation Raising Malawi in helping with improving their lives and conditions. 

Madonna said about the film: "To say that this film is a labor of love is trivial. Its also the journey of a lifetime. I hope you all are as inspired watching it as I was making it." 

The film, released in 2008, was described by long-time friend Rosie ODonnell as "devastating and amazing."  Both Madonna and ODonnell attended a private screening on October 11, 2007.

==Book==
A companion book containing 180 pages with 106 duotone and 44 four-color photographs taken during the visit, as well as stills from the video was released in January 2009. Author proceeds from the sale of the book were to be donated to the charitable organization Raising Malawi for their extensive work with orphans throughout Malawi. 

*Released: January 2009
*Photographer: Kristen Ashburn, with foreword by Madonna
*Publisher: PowerHouse

==Title==
The title is derived from former Archbishop of Cape Town and apartheid|anti-apartheid activist Desmond Tutus speech regarding the African philosophy of Ubuntu (philosophy)|Ubuntu. Ubuntu is an idea present in African spirituality that says "I am because we are", or we are all connected, we cannot be ourselves without community, health and faith are always lived out among others, an individuals well being is caught up in the well being of others.  In Malawi, this African philosophy is known as "uMunthu". In Chichewa, it is kali kokha nkanyama, tili awiri ntiwanthu (when you are on your own you are as good as an animal of the wild; when there are two of you, you form a community).” 

==Reception==
 

The film received positive reviews in the media. The Times, giving it 4 out of 5 stars, wrote: "This rich material makes for a completely absorbing film. Certain scenes, such as women and children literally dying of AIDS in front of the camera, drew gasps from the shocked audience in the screening room at Cannes. Its impossible to tear your eyes away from the screen. Not that the film portrays Malawian people as innocent victims of circumstances beyond their control. Rissman doesnt shy away from the culture of drinking, crime and violence that is prevalent in the country. Together, he and Madonna have made a shocking and incredibly moving film that is much more than an extended Comic Relief appeal." 

A news article in The Guardian said, "Madonna the documentary-maker came, saw and conquered the worlds biggest film festival yesterday with a powerful polemic on the effects of disease and poverty on Malawi...The queen of reinvention presented her film, I Am Because We Are, which she wrote and produced, to general acclaim. Alongside her was her former gardener Nathan Rissman, who directed." 

The film was shown at the 2008 Traverse City Film Festival, held from July 29 to August 3. Michael Moore, who founded the festival, said in May 2008 about the film: "I saw her film a month ago and was so deeply moved in a way that rarely happens with movies these days. I asked her immediately if she would come to our festival and she said, "yes." I have known her for years and she is truly one of the most caring and generous people I have met. Her presence here in Traverse City will have a profound impact on people."  In the fall of 2008, Madonna released a video message announcing plans to build a school in Malawi, the Raising Malawi Academy for Girls.

==References==
 

==External links==
*  
*  
*  

 

 
 
 