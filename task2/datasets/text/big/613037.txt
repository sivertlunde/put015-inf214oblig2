After Hours (film)
{{Infobox film
| name           = After Hours
| image          = After Hours (film) POSTER.jpg
| caption        = Theatrical release poster
| director       = Martin Scorsese
| producer       = {{plainlist|
* Amy Robinson
* Griffin Dunne
* Robert F. Colesberry
}}
| writer         = {{plainlist|
* Joseph Minion
* Martin Scorsese (uncredited)
}}
| starring       = {{plainlist|
* Rosanna Arquette
* Verna Bloom Thomas Chong
* Griffin Dunne
* Linda Fiorentino
* Teri Garr John Heard Richard Cheech Marin
* Catherine OHara
}} 
| cinematography = Michael Ballhaus
| music          = Howard Shore
| editing        = Thelma Schoonmaker
| studio         = {{plainlist| The Geffen Company
* Double Play Productions
}}
| distributor    = Warner Bros.
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $4.5 million 
| gross          = $10.6 million 
}}
 John Heard. Paul Hackett (Dunne) experiences a series of misadventures as he tries to make his way home from SoHo.

Warner Home Video have released the film on VHS in 1991 for both widescreen and pan-and-scan NTSC laserdiscs.   

==Plot== plaster of Paris paperweights resembling cream cheese bagels. Later in the night, under the pretense of buying a paperweight, Paul visits Marcy, taking a cab to her apartment.  On his way to visit Marcy, a $20 bill is blown out the window of the cab, leaving him with only some spare pocket change. The cab driver is furious that he cannot pay, thereby beginning the first in a long series of misadventures for Paul that turn hostile through no fault of his own. At the apartment Paul meets the sculptor Kiki and Marcy, and comes across a collection of photographs and medications which imply that Marcy is severely disfigured from burns on her legs and torso.  As a result of this implication, and as a result of a strained conversation with Marcy, Paul abruptly slips out of the apartment.  

Paul then attempts to go home by subway, yet the fare has increased at the stroke of midnight and he finds that his pocket change is no longer sufficient to purchase a token. He goes to a bar where Julie, a waitress, becomes enamoured with him. The bars owner, Tom Schorr cannot open the cash register to give Paul his subway fare. They exchange keys so Paul can go to Toms place to fetch the cash register keys. On the way, he spots two burglars, Neil and Pepe, with one of Kikis sculptures. When he returns the sculpture to the apartment, he finds Marcy has committed suicide while Kiki and a stout man named Horst have already left to go to Club Berlin, a nightclub. 
 punks attempt to shave his head into a Mohawk hairstyle. On the street, Paul is mistaken for a burglar and is relentlessly pursued by a mob of local residents.

Paul finds Tom again, but the mob (with the assistance of Julie, Gail, and Gails Mister Softee truck) chases Paul. He ultimately seeks refuge back at the Club Berlin. Paul uses his last quarter to play Is That All There Is? by Peggy Lee and asks a woman named June to dance. Paul explains hes being pursued and June, also a sculptress, offers to help him. She protects him by pouring plaster on him in order to disguise him as a sculpture. However, she wont let him out of the plaster, which eventually hardens, trapping Paul in a position that resembles the character depicted in Edvard Munchs painting The Scream. The burglar duo then breaks into the Club Berlin and steals him, placing him in the back of their van. He falls from the burglars cargo near the gate to his office as the sun is rising, and he returns to work, bringing the film full circle.

==Selected cast==
 
* Griffin Dunne as Paul Hackett
* Rosanna Arquette as Marcy Franklin
* Teri Garr as Julie John Heard as Tom Schorr
* Catherine OHara as Gail
* Linda Fiorentino as Kiki Bridges
* Verna Bloom as June
* Tommy Chong as Pepe
* Cheech Marin as Neil
* Will Patton as Horst
* Clarence Felder as Club Berlin bouncer
* Dick Miller as Pete, diner waiter
* Bronson Pinchot as Lloyd
* Martin Scorsese as Club Berlin searchlight operator
* Victor Argo as Diner Cashier
* Larry Block as Taxi Driver
* Rocco Sisto as Coffee Shop Cashier
 

==Production== Last Tempation of Christ production was a huge disappointment to Scorsese. It spurred him to focus on independent companies and smaller projects.  The opportunity was offered to him by his lawyer Jay Julien, who put him through Griffin Dunne and Amy Robinsons independent group: "Double Play Company".
The project was called "A Night in Soho" and it was based on the script by Joseph Minion. The screenplay, originally titled "Lies" after the 1982 Joe Frank monologue that inspired the story,  was written as part of an assignment for his film course at Columbia University. He was 26 years old at the time the film was produced.    The script finally became "After Hours" after Scorsese made his final amendments. 

One of Scorseses inputs involves the dialogue between Paul and the doorman at Club Berlin, inspired by Kafkas Before the Law, one of the short stories included in his novel The Trial.   As Scorsese explained to Paul Attanasio, the short story reflected his frustration towards the production of The Last Temptation of Christ, for which he had to continiously wait, as Joseph K had to in The Trial.  
 The Last Temptation of Christ, and Burton gladly stepped aside when Scorsese expressed interest in directing.  

After Hours was the first fictional film by Scorsese in ten years in which  . 1985. Retrieved 2009-12-10. 

British director Michael Powell took part in the production process of the film (Powell and editor Thelma Schoonmaker married soon afterwards). Nobody was sure how the film should end. Powell said that Paul must finish up back at work, but this was initially dismissed as too unlikely and difficult. They tried many other endings, and a few were even filmed, but the only one that everyone felt really worked was to have Paul finish up back at work just as the new day was starting. 

==Reception== Best Director The Last Temptation of Christ.    It currently holds a 90% rating on review aggregator Rotten Tomatoes.  Film critic Roger Ebert gave After Hours a positive review and a rating of four out of four stars. He praised the film as one of the best in the year and said it "continues Scorseses attempt to combine comedy and satire with unrelenting pressure and a sense of all-pervading paranoia."  He later added the film to his "Great Movies" list.  In The New York Times, Vincent Canby gave the film a mixed review and called it an "entertaining tease, with individually arresting sequences that are well acted by Mr. Dunne and the others, but which leave you feeling somewhat conned." 

==Lawsuit==
Radio artist Joe Frank later filed a lawsuit, claiming the screenplay lifted its plot setup and portions of dialogue, particularly in the first 30 minutes of the film, from his 1982 NPR Playhouse monologue "Lies".  Though Frank never received official credit, he reportedly was "paid handsomely" in a settlement. 

==Themes and motifs ==
 phallic symbol,  and Marcy makes a reference to her husband using a double entendre when saying, "I broke the whole thing off" when talking about her and her husbands sex life.  A mouse trap clamps shut on a mouse when Julie tries to gift Paul the bagel paperweight.

==Music== musical score for After Hours was composed by Howard Shore, who went on to collaborate multiple times with Scorsese. Although an official soundtrack album was never released, many of Shores cues appear on the 2009 album Howard Shore: Collectors Edition Vol. 1.  In addition to the score, other music credited at the end the film is:

# "Symphony in D Major, K. 95 (K. 73n): 1st movement" attributed to Wolfgang Amadeus Mozart (the work is not among Mozarts officially numbered symphonies, but is sometimes numbered as 45)
# "Air on the G String (Air From Suite No. 3)" by Johann Sebastian Bach
# "En la Cueva" Performed by Cuadro Flamenco
# "Sevillanas" Composed and Performed by Manitas de Plata Night and Day", Words and Music written by Cole Porter Body and Soul" Composed by John Green
# "Quando, Quando, Quando", Music by Tony Renis, Lyrics by Pat Boone
# "Someone to Watch over Me", Lyrics by Ira Gershwin, Music by George Gershwin, Performed by Robert and Johnny
# "Youre Mine" Written by Robert Carr and Johnny Mitchell, Performed by Robert and Johnny We Belong Together" Performed by Robert and Johnny Angel Baby" Written by Rosie Hamlin, Performed by Rosie and the Originals
# "Last Train to Clarksville" Composed by Bobby Hart and Tommy Boyce, Written by Tommy Boyce and Bobby Hart, Performed by The Monkees
# "Chelsea Morning" Composed and Performed by Joni Mitchell
# "I Dont Know Where I Stand" Composed and Performed by Joni Mitchell
# "Over the Mountain and Across the Sea" Composed by Rex Garvin, Performed by Johnnie and Joe
# "One Summer Night" Written by Danny Webb, Performed by The Danleers
# "Pay to Cum" Written and Performed by the band Bad Brains
# "Is That All There Is" Composed by Jerry Leiber and Mike Stoller, Performed by Peggy Lee

==References==
 

==External links==
 
* John Walker. (1989)  . THES / artdesigncafe. 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 