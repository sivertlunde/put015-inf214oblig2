Requiem pour un Vampire
{{Infobox film
| name = Requiem pour un Vampire
| image = Requiem,_jean_rollin-1971.jpg
| imagesize =250px
| caption = Original Theatrical Release Poster
| director = Jean Rollin
| producer = Sam Selsky
| writer =
| starring = Marie-Pierre Castel Mireille Dargent Philippe Gasté Dominique Louise Dhour
| music = Pierre Raph
| cinematography =
| editing =
| distributor = Les Films ABC
| release = 1971, France
| runtime = 95 minutes
| country = France
| language = French
| othername = Requiem for a Vampire Vierges et Vampires Caged Virgins The Crazed Vampire
}}

Requiem pour un Vampire (English title: Requiem for a Vampire) is a 1971 erotic horror/fantasy film directed by Jean Rollin, about two young women who find themselves trapped in a haunted castle ruled by a horde of sexually blood-crazed vampires. 

==Plot==
Two women dressed as clowns and a male driver are being chased through the countryside, for unknown reasons. As the man drives, the women shoot at their pursuers. When the man is shot, the women are forced to burn the car with his body inside and once they remove their costumes, they run through a forest, and later a cemetery, in which one of the women, Michelle, is almost buried alive.

Walking through a field, they come to the outside of a gothic castle. There they are bitten by vampire bats, which lead them to go into the castle, where they make love in a cozy bed. They tour the castle and discover a few skeletons along with a woman playing an organ. She begins to follow them, so they shoot at her, but she doesnt die. They run away and are caught by some men who force themselves on them. A vampire woman stops the men, and the vampire woman who chased them almost bites them until they break away. They soon come across a male vampire, the last of his kind. He has plans for the women. They are bitten in order to continue his bloodline, but they must stay virgins. Michelle likes the idea of everlasting life but her girlfriend has serious doubts, and by sleeping with Frédéric, a random passerby, she not only jeopardizes the vampires plans but also puts the mutual love and friendship between her and Michelle to the ultimate test. The vampire realizes that he must not continue the bloodline, and lets Michelle and her girlfriend escape.

==Production==

===Casting===
Marie-Pierre Castel, who starred in Rollins previous films La Vampire Nue and Le Frisson des Vampires, joined the cast of Requiem because her twin sister Catherine Castel was unavailable. An agent had introduced Rollin to Mireille Dargent, who would play Maries partner in the film. This agent was a crook, and collected Dargents wages for his own benefit. Rollin hired a lawyer and the agent paid the earnings back to her.  Requiem was Louise Dhours first film. She used to sing Damias songs at Paris nightclub La Ville Grille. As the vampires maidservant in the film, she was given the most important of her roles and thus became part of the Films ABC family. 

===Location===
The movie was filmed in the small village of Crêvecoeur. The graveyard was located outside the village on a knoll. The castle, a historical place entirely furnished with genuine antiques, all of which were worth a fortune, had been rented from the duchess of La Roche-Guyon|Roche-Guyon. It wasnt her castle that Rollin and the crew were interested in, but the ruins of the dungeon above, that overlooked the entire area. 

===Censorship===
 

Due to the nudity, some of the scenes had to be shot twice for such countries as the UK. For the scene where Dargent is whipping the naked Castel, another scene was shot with her wearing a bra and panties. For the two other scenes, where Castel and Dargent make love, two scenes were shot: the original with them completely naked, and the other fully clothed. In the original scene where Dargent is being chased by the man, she was topless; in the censored scene, she was clothed. 

==Cast==
* Marie-Pierre Castel as Marie
* Mireille Dargent as Michelle
* Philippe Gasté as Frédéric
* Dominique as Erica 
* Louise Dhour as Louise
* Michel Dalesalle
* Antoine Mosin
* Agnès Petit
* Olivier François
* Dominique Toussaint
* Agnes Jacquet
* Anne-Rose Kurra
* Paul Bisciglia as Lhomme au Vélo

==Releases==
===VHS===
The film was released on VHS in France as part of the Collectorror collection.

The US and Canadian VHS edition, retitled Caged Virgins, was released on August 22, 1995.

The UK VHS edition was released by Salvation.

===DVD===
Several versions of the film were released on DVD in different countries.

The film was released in France as part of the Jean Rollin Collection on June 24, 2004, with extras including an interview with Rollin, a biography and a filmography.

For the US and Canadian DVD release, the film was retitled Requiem for a Vampire and released on March 30, 1999. The US and Canadian rerelease was issued on February 24, 2009, with extras including alternative scenes, a 10-minute interview with Louise Dhour, trailers, additional scenes and photo gallery.

A 3-disc Limited Edition was released by Encore in Europe in a new 1.78:1 anamorphic widescreen version, with extras including audio commentary by Rollin, original trailers, an introduction by Rollin, a slideshow, alternative scenes, interviews with Louise Dhour and Paul Bisciglia, and a 64-page page booklet. 

The UK DVD was released on 9 September 2004 by Redemption in a full-frame version, with extras including trailers, photo gallery, video art, publicity and a music video.

==References==
 

==External links==
*  

 

 
 
 
 
 
 