Touchy Feely
{{Infobox film
| name           = Touchy Feely
| image          = Official Film Poster for Touchy Feely.jpeg
| caption        = 
| director       = Lynn Shelton
| writer         = Lynn Shelton
| producer     = Steven Schardt
| starring       = Rosemarie DeWitt Ellen Page Allison Janney Josh Pais Scoot McNairy Ron Livingston Tomo Nakayama
| music          = 
| cinematography = Benjamin Kasulke
| editing        = Lynn Shelton
| studio         = 
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $35,966 
}}
Touchy Feely is a 2013 film directed by Lynn Shelton. It was nominated for the Grand Jury Prize at the 2013 Sundance Film Festival.  
A massage therapist is unable to do her job when stricken with a mysterious and sudden aversion to bodily contact. Meanwhile, her uptight brothers floundering dental practice receives new life when clients seek out his healing touch.

==Plot== ecstasy together in order to help her relax goes awry Abby decides not to move in with Jesse.

Meanwhile her brother Paul, (Josh Pais), is struggling financially as the family dentistry practice is crumbling and his risk-averse attitude means he refuses to seek out new clients. When his daughter Jenny (Ellen Page) invites her friend Henry in for a routine teeth-cleaning Paul is displeased. However Paul manages to accidentally cure Henrys Temporomandibular joint dysfunction and news of his healing powers begins to attract new clients. Inspired by the outpouring of enthusiasm from his clients he visits Abbys friend Bronwyn in order to learn Reiki to better help his clients. However after a disgruntled ex-client collapses in the waiting-room while screaming that Paul is a fraud his fortunes are reversed and his clients disappear. Upset at watching her father revert to his former self, Jenny runs out of the office crying.

Abby decides to break up with Jesse in a note and moves in with her brother. She takes the ecstasy alone and while wandering outside meets her ex-boyfriend, Adrian (Ron Livingston). He takes her to his grandmothers home and while there she is able to touch him. Paul finds the other ecstasy tablet and he takes it while he waits for Jenny to contact him. He wanders through the city eventually ending up at Bronwyns home where they kiss. 

Jenny goes to Jesses apartment and invites him out to go see music with her. Afterwards they go back to his apartment where Jenny asks him to kiss her and he turns her down telling her he loves Abby. Jenny falls asleep at his home and in the morning they are awoken by a knock at the door from Abby who is able to hug Jesse.

==Cast==
*Rosemarie DeWitt as Abby
*Ellen Page as Jenny
*Josh Pais as Paul
*Allison Janney as Bronwyn
*Ron Livingston as Adrian
*Scoot McNairy as Jesse
*Tomo Nakayama as Henry

==Production==
Director Lynn Sheltons friend Megan Griffiths suggested the title after hearing her talk about it. 
 Shelton was interested in working with her during the filming of An American Crime.  According to Page though there was a script a good deal of the dialogue was improvised. 

==Reception==
Touchy Feely received mixed to negative reviews. It holds a score of 53 on Metacritic and 36 on Rotten Tomatoes.  

==References==
 

==External links==
* 
* 
* , Magnolia Pictures website.
 

 
 
 
 
 
 