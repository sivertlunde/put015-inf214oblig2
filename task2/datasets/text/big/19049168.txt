The Red Meadows
{{Infobox film
| name           = The Red Meadows
| image          = De røde enge.jpg
| image_size     =
| caption        = 1945 movie poster by Kai Rasch
| director       = Bodil Ipsen Lau Lauritzen Jr.
| producer       = Jens Dennow Henning Karmark
| writer         = Lech Fischer Ole Juul (book)
| narrator       = 
| starring       = Poul Reichhardt Lisbeth Movin
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Marie Ejlersen
| distributor    = ASA Film 1945
| runtime        = 85 min.
| country        = Denmark Danish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1945 Denmark|Danish war drama directed by Bodil Ipsen and Lau Lauritzen Jr. based on resistance fighter Ole Valdemar Juuls 1945 novel of the same name. The film, starring Poul Reichhardt and Lisbeth Movin, is a suspense tale revolving around the memories of a Danish saboteur as he awaits his execution in a German war-time prison. Filmed in Denmark only months after the end of the German occupation during WWII, Red Meadows was a tribute to the Danish resistance fighters.    The film received the Palme dOr at the Cannes Film Festival and is considered a stylistic masterwork. 

== Plot ==

In German-occupied Denmark during WWII, the young Danish   where he is able to contact Toto. Both Michael and Ruth find transport to Sweden where they can finally rest.

== Cast ==
{| class="wikitable" |- bgcolor="#CCCCCC" 
!   Actor !! Role
|-  Poul Reichhardt||Micheal Lans
|-  Lisbeth Movin || Ruth Isaksen
|-  Per Buckhøj || Prison guard Steinz
|-  Gyrd Løfqvist || Gustav
|-  Preben Kaas || Erik
|- Kjeld Jacobsen || Hansen
|- Arne Hersholdt || German field marshal
|- Karl Jørgensen German major
|-  Lau Lauritzen Lau Lauritzen || Toto
|-  Preben Neergaard Prikken
|- 
|Bjørn Watt-Boolsen|| Tom
|-  Preben Lerdorff Rye || Alf
|-  Freddy Koch ||  Dryer
|-  Hjalmar Madsen Hotel clerk
|-  German officer
|- 
|}

== References ==
 

== External links ==
*   at  IMDb
*   at Den Danske Film Database  
*   at Det Danske Filminstitut  

 

 
 
 
 
 
 
 