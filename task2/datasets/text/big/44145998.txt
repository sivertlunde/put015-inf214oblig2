Attahaasam
{{Infobox film
| name           = Attahaasam
| image          =
| caption        =
| director       = KS Gopalakrishnan
| producer       =
| writer         =
| screenplay     =
| starring       =
| music          = MG Radhakrishnan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, directed by KS Gopalakrishnan.  The film had musical score by MG Radhakrishnan.   

==Cast==
*Sukumaran	
*Kalaranjini

==Soundtrack==
The music was composed by MG Radhakrishnan and lyrics was written by Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chellam chellam || KS Chithra, Chorus || Poovachal Khader || 
|-
| 2 || Vanamaali nin maaril || K. J. Yesudas, KS Beena || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 