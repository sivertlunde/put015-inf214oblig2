Mugamoodi
 
{{Infobox film
| name           = Mugamoodi
| image          = Mugamoodi-poster.jpg
| caption        = Movie poster
| director       = Mysskin
| producer       = Ronnie Screwvala Siddharth Roy Kapur
| writer         = Mysskin
| starring       =   K
| cinematography = Sathya
| editing        = Gaugin
| studio         = UTV Motion Pictures
| distributor    = UTV Motion Pictures Lone Wolf Productions
| released       =  
| runtime        = 162 minutes   
| country        = India Tamil
| budget         =     Box office =  
}}
 Tamil Romance romantic superhero Narain in lead roles.    It is the first Tamil super-hero movie.  The film was declared an Average grosser by the Box Office, grossing around Rs 55 crores on a budget of Rs 20 crores, with additional revenues from music and TV sales.     The film is about a man who cannot tolerate injustice and corruption and hence turns into a vigilante to fight against it for a better society. This film was dedicated to Bruce Lee. With mixed critical response the film was released on 31 August 2012, along with its dubbed Telugu version titled Mask.   It has also been dubbed in Hindi as Mahabali Ek Super Hero.

==Plot==
Bruce Lee (Jiiva) is a martial arts specialist but unluckily he is unable to use those skills to make a living. Instead, he falls in love with Police Deputy Commissioner Gauravs (Nassar) daughter Shakthi (Pooja Hegde), and tries to impress her and her elder sisters kids with his martial art skills. To make it more fun for them he pretends to be a superhero by wearing on a costume each time he visits. On his way back on one such visit, Lee finds himself caught in between cops and robbers chase in the city. He decides to assist the police and manages to nab one robber from the gang and hands him over to the police. Lee doesnt realize that by doing so he has asked upon himself the revenge of Dragon (Narain (actor)|Narain) who heads the same gang. As the police were involved too, Dragon eyes to take revenge on Police Commissioner Gaurav also. Dragon then connives a plan that eventually frames Lee as a robber and the attempted to kill the Police Commissioner. What lies ahead for Lee now is to prove his innocence to the Police, win his love back Shakthi and more importantly bring Dragon and his gang to justice.

==Cast==
* Jiiva as Anand/Bruce Lee/Mugamoodi Narain as Anguchamy/Dragon
* Pooja Hegde as Shakthi Gaurav
* Girish Karnad as Anands Grand father
* Nassar as Gaurav Selva as Sifu Chandru
* Anupama Kumar as Roshini and Shakthi`s Mother
* Aadukalam Naren as Anands father
* Aadukalam Murugadoss Krishna Kumar
* Kalaiyarasan
* Misha Ghoshal
* Aadithya
* Darshan as Narains sidekick

==Production== Suriya to Vishal to replace him, forcing Myshkin to stall the project.  As the project remained dormant, Myshkin temporarily shelved the project and moved on to script a horror film. 
 Arya signed The Joker The Dark 2011 Miss Universe India beauty pageant was eventually finalized as the female lead.  K, who had scored for Mysskins Yuddham Sei (2010) previously, was selected as the film composer over two other music directors, while Madhan Karky would pen the lyrics, associating for the first time with Mysskin.  Veteran Telugu actor Akkineni Nageswara Rao was added to the cast in November 2011 which proved false and he was replaced by Girish Karnad, while Prakash Raj was also reported to be playing an important role.  The following month, Nassar was selected to portray a police officer.  Pooja Hegde will appear as Jiivas lover in the film.   

Reports further claimed that both Jiiva and Narain would undergo special training in martial arts with experts from the Shaolin Temple in China being flown in. A large group of young aspiring artists were roped in to be part of the fight sequences which are touted be the highlight of the movie. The artists were trained for over 6 months at YMCA, Nandanam, Chennnai rigorously.   High-end gadgets to be used in the film were reportedly designed by the Indian Institutes of Technology (IIT), while teams from the National Institute of Fashion Technology (NIFT) were roped in to create the costumes and looks of the characters.  Furthermore, a Los Angeles-based firm would handle special effects, while action sequences are planned to be choreographed by stuntmen from Hong Kong.  Costumes are designed by Gabriella Wilkins and it is said that Jiivas superhero outfit weighs more than 10&nbsp;kg.    Shooting of the film started officially on 12 December 2011 with the opening scene of Kung Fu as that was so spoken about.  The film is expected to be released during Summer 2012.  The film was being canned in the nights in and around Triplicane in Chennai where the fight scene of Jiiva and Narain was shot in terrace.  Mysskin is planning a 20 day schedule to shoot the climax at Karaikal.The shoot concluded at Karaikal where daredevil stunts were performed by the lead actor at an altitude of 180 feet high where in the Karaikal port`s conveyor belt passes over. Many scenes were also filmed at AVM studio where huge hospital set was erected and also at Victoria hall, Deaf and Dumb school.;  Tony Leung Siu Hung, a stunt coordinator from Hong Kong, has joined the climax shoot of the film thats being canned in a massive scale at Karaikal.  

==Soundtrack==
{{Infobox album
| Name = Mugamoodi
| Type = Soundtrack K
| Cover  = 
| Released = 6 July 2012 Feature film soundtrack
| Length =  Tamil	
| Label = 
| Producer = K
| Reviews =
| Last album = Aarohanam (2012)
| This album = Mugamoodi (2012)
| Next album = Annayum Rasoolum (2013)
}} Vijay and received by Kannada lead actor Puneet Rajkumar. 

Rediff wrote: "Mugamoodis numbers grab your attention and carry you with them".  Milliblog wrote: "Tepid soundtrack from K".  Behindwoods wrote: "On the whole, the music has provided a good platform for the superhero to soar". 

{{tracklist
| headline        = Track listing 
| extra_column    = Singer(s)
| total_length    = 
| lyrics_credits  = yes
| title1          = Vaayamoodi Summa Iru Da
| extra1          = Aalap Raju
| lyrics1         = Madhan Karky
| length1         = 4:34
| title2          = Lullaby For Loss
| extra2          = 
| lyrics2         =
| length2         = 2:32
| title3          = Bar Anthem
| extra3          = Mysskin
| lyrics3         = Mysskin
| length3         = 5:11
| title4          = Blue Panther On The Prowl
| extra4          = 
| lyrics4         =
| length4         = 1:37
| title5          = Maayavi
| extra5          = Chinmayi
| lyrics5         = Madhan Karky
| length5         = 4:29
| title6          = Dont Drive Your Car With This Music On
| extra6          = 
| lyrics6         =
| length6         = 4:42
| title7          = Cape Of Good Hope
| extra7          = 
| lyrics7         =
| length7         = 2:44
| title8          = Hell, Heaven And The Ladder
| extra8          = 
| lyrics8         =
| length8         = 1:40
| title9          = Come Lets Fall In Love
| extra9          = 
| lyrics9         =
| length9         = 4:23
}}

==Release== Sun TV. The film was given a "U" certificate by the Indian Censor Board.  This film was released worldwide screens on 31 August 2012 and it would be the 100th film to be released in 2012.  Also, it is to be noted that the director has planned for upcoming sequels of Mugamoodi. Therefore, the director has planned to release a sequel every year.  The first look and trailer of Jiivas Mugamoodi took place in a grand scale on 29 June 2012 at Sathyam Cinemas. The trailer was launched by Suriya and received by director Lingusamy while the first look was released by Suriya and Gautham Vasudev Menon.  Mugamoodi released in over 500 screens in Tamil Nadu, and also opened in neighbouring states in good number of screens as well as in northern parts of India including Delhi and Mumbai. The film released simultaneously in Sri Lanka, Malaysia, Singapore, UK, Canada, US and other parts of the world. 

==Critical reception==
Mugamoodi received mixed to negative reviews. IBN reviewed the film positive and cited that it had a "different flavour" further adding: "For those who loves the West and Easts action films, Mugamoodi combines both to give some pleasure".  Behindwoods Review Board rated it 3 out of 5 stating that it had a "pacy first half,   slow second half but   compensated by the overall performance and effort of the cast & crew".  Malathi Rangarajan from The Hindu noted that it "travels on a terrain that’s new to Tamil cinema" and lauded Mysskin, saying that his "effort to make Mugamoodi appear as authentic as possible deserves to be commended".  Prakash Upadhyaya of oneindia.in recommended the film with a 3.5 out 5 rating. 

However, Sify termed the film average and a "one-time watch", calling it "good in parts" and claiming that it had "very good music and background score by K, but lacks a proper script and a racy presentation".  Pavithra Srinivasan of Rediff gave it 2 out of 5 stars, writing that it had "all the makings of a successful film but ends up disappointing", particularly criticizing Mysskins screenplay that "fails him spectacularly".  Rohit Ramachandran of Nowrunning.com rated it 1.5 out of 5 stating that the film was "heavily flawed and utterly boring".  Vivek Ramz of in.com rated it 1.5 out of 5 and said that "Mugamoodi is no Batman, just Kanthaswamy revisited!"  

==Awards and nominations==

{| class="wikitable"
|-
! Award
! Category
! Nominee
! Result
|-
 2nd South Indian International Movie Awards SIIMA Award Best Actor in a Negative Role Narain (actor)|Narain
|  
|}

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 