Hoot (film)
{{Infobox film
| name           = Hoot
| image          = Hoot film.jpg
| caption        = Theatrical release poster
| director       = Wil Shriner Frank Marshall Jimmy Buffett
| screenplay     = Wil Shriner
| based on       =  
| starring       =  Luke Wilson Logan Lerman Brie Larson Tim Blake Nelson Neil Flynn Robert Wagner
| music          = Jimmy Buffett Michael Chapman
| editing        = Alan Edward Bell
| studio         = Walden Media The Kennedy/Marshall Company
| distributor    = New Line Cinema
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $8,224,998 
}} family comedy novel of the same name. It was written and directed by Wil Shriner, and produced by New Line Cinema and Walden Media. The film was released on May 5, 2006.

The film is about a group of children trying to save a burrowing owl habitat from destruction. The habitat is located on the intended construction site of a pancake house. The developer of the project intends to proceed regardless of the environmental damage it would cause.

Hoot features live burrowing owls and music by Jimmy Buffett. Buffett is also listed as a co-producer, and he played the role of Mr. Ryan, the science teacher, in the movie.

The film was generally regarded as unsuccessful in its initial theatrical run, and received largely mixed to negative reviews from notable film critics and film-review websites.

==Plot==
 
Middle school student Roy A. Eberhardt (Logan Lerman) has just moved to Florida from Montana. He is mercilessly teased and bullied by Dana Matherson until he accidentally breaks Danas nose while on the school bus. Because of this, Roy is suspended from riding the school bus for 3 days and must write Dana an apology letter. Roy slowly becomes friends with Beatrice "The Bear" Leep (Brie Larson), and her stepbrother, "Mullet Fingers" (Cody Linley).
Meanwhile someone is responsible for vandalism on a local construction site where a "Mother Paulas Pancake House" restaurant- owned by corrupt CEO, Chuck Muckle- is about to be built. In order to catch the vandals and prevent further vandalism, Officer David Delinko (Luke Wilson) has parked his police cruiser on the building site. Delinko falls asleep and an unknown vandal vandalizes the car by spray painting its windows with black spray paint. The next day at breakfast, Roy and his parents read about the spray painted police car. The police chief then gives Delinko a small police scooter to replace the vandalized cruiser.

Soon, it is learned that in order to build the pancake house, they must first destroy the burrowing owls living on site.
 Fingers continually sabotages construction efforts on the site. Leroy Curly Branitt (Tim Blake Nelson), the inept construction foreman, is trying to keep the construction schedule going, despite the presence of the owls.

In the end, the trio reveals to Delinko and the rest of the town that there are burrowing owls on the lot. They then manage to get everyone to be quiet long enough for the owls to emerge, and Muckle is subsequently arrested by Delinko. The construction site eventually becomes an owl preserve.

Ultimately Roys parents decide to stay in Florida, Officer Delinko finally gets promoted to detective, Dana goes to military camp, and Muckle does community service for life.

==Cast==
*Logan Lerman as Roy A. Eberhardt
*Brie Larson as Beatrice "The Bear" Leep
*Cody Linley as Napoleon Bridger "Mullet Fingers" Leep
*Luke Wilson as Officer David Delinko
*Tim Blake Nelson as "Curly" Brannit
*Neil Flynn as Mr. Eberhardt
*Kiersten Warren as Mrs. Eberhardt
*Clark Gregg as Chuck Muckle
*Jessica Cauffiel as Kimberly Dean Collins as Garrett
*Robert Wagner as Mayor Grandy
*Eric Phillips as Dana Matherson
*Damaris Justamante as Mrs. Matherson (cameo)
*Jimmy Buffett as Mr. Ryan
*John Archie as Captain
*Robert Donner as Kalo
*Carl Hiaasen as Muckles Assistant, Felix

==Production==
The principal filming locations were in Fort Lauderdale and Lauderdale by the Sea, on Floridas Atlantic Coast, and the Gulf Coast hamlet of Boca Grande on Gasparilla Island.   

Most of Hoot was shot in Florida between July 6, 2005 and September 2, 2005. Some new scenes were shot in Los Angeles on January 21, 2006.  For example, the scene where Mullet Fingers leaps out of a tree after dropping a bulldozer seat was actually shot in Los Angeles. 

Hoot was shot during hurricane season, and the set did not escape Hurricane Katrina, which struck Southern Florida on August 25, 2005. Brie Larson and Cody Linley were moved from their beach-front hotel (Marriott Harbor Beach) to another hotel because of the storm. 

==Distribution==
===Theatrical release===
  and   nudged Hoot into second place in terms of biggest theatre drops.  Hoot topped The Seeker: The Dark is Rising in reaching number one in the "worst super-saturated (3000 plus screens)" openings in the US and Canada: Hoot opened in almost 42% of all screens. 
Hoots production budget was $15 million, although the costs for such a wide opening would probably have made the film considerably more expensive to distribute than it was to produce - the cost of its prints would have been twice as much as the production budget, according to respected industry opinions. 

Hoot entered the collection of the Museum of Modern Art (MOMA) in 2009. 

===Home media===
DVD sales were more successful than the box office gross. The DVD was released on August 15, 2006 and sold 114,528 units, bringing in $2,058,068 in the opening weekend. A newer figure indicates that 703,786 units have been sold, translating to $10,972,266 in revenue. 

==Reception==
===Critical response===
The movie received mostly mixed to negative reviews and has a Rotten Tomatoes "Rotten" rating of 26%  and a Metacritic score of 46 (mixed or average reviews). 

One of the most positive reviews came from the  s."  San Francisco Chronicles Ruthe Stein gave the film a positive review (3 stars out of 4) and said, "...the film does nothing to dilute the save-the-Earth-and-every-creature-on-it message of Carl Hiaasens ingeniously plotted award-winning childrens book." 

Roger Ebert gave Hoot 1.5 stars (out of 4) and included Hoot in his 2007 book - Your Movie Sucks - where he says "Hoot has its heart in the right place, but I have been unable to locate its brain" and "... the kids (especially Mullet Fingers) are likeable but not remotely believable".  Michael Medved panned Hoot (2 stars out of 4) saying that "...the lame plot centers around a greedy developer who wants to bulldoze a lot inhabited by rare burrowing owls" and "though Id like to root for Hoot, its entertainment value is moot". 

===Accolades=== Best Performance in a Feature Film - Leading Young Actor. 

==Soundtrack==
 
The soundtrack of Hoot has three elements: an original score, pop songs sung by a variety of artists, and pop songs (covers and originals) sung by Jimmy Buffett.
 Michael Utley Phil Marshall.

The pop songs sung by a variety of artists are: Lovely Day" - performed by Maroon 5, featuring Bill Withers and Kori Withers. Written & originally performed by Bill Withers.
* "Back of the Bus" - performed  by G Love and Special Sauce. Written by Garrett Dutton.
* "Let Your Spirit Fly" - performed by Ry Cuming. Written by Ry Cuming. Coming Around" - performed by Brie Larson. Written by Brie Larson, Jacques Brautbar and Rami Perlman.
* "Funky Kingston" - performed by Toots & the Maytals. Written by Frederick Hibbert.
* "Florida" - performed by Mofro. Written by John J.J. Grey.
The songs sung by Jimmy Buffett are: Robert Parker.
* "Floridays" - performed by Jimmy Buffett.  Written by Jimmy Buffett. Roger Guth.
* "Werewolves of London" - performed by Jimmy Buffett.  Written by LeRoy Marinell, Waddy Wachtel and Warren Zevon; originally performed by Warren Zevon.
* "Wondering Where the Lions Are" - performed  by Jimmy Buffett.  Written & originally performed by Bruce Cockburn.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 