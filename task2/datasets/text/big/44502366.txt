Easy to Love (1934 film)
{{Infobox film
| name           = Easy to Love
| image          = 
| image_size     = 
| caption        = 
| director       = William Keighley
| producer       = Henry Blanke (uncredited) Carl Erickson (screenplay and adaptation) Manuel Seff (screenplay) David Boehm (adaptation)
| story          = 
| based on       =  
| narrator       = 
| starring       = Genevieve Tobin Adolphe Menjou Mary Astor Edward Everett Horton
| music          = 
| cinematography = 
| editing        = 
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 61-65 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Code American romantic comedy film starring Genevieve Tobin, Adolphe Menjou, Mary Astor and Edward Everett Horton. This was William Keighleys solo directorial debut (he had co-directed two earlier films with Howard Bretherton). He and Tobin would marry in 1938.

When a woman finds out her husband is having an affair, she sets out to get even.

==Cast==
* Genevieve Tobin as Carol
* Adolphe Menjou as John
* Mary Astor as Charlotte
* Edward Everett Horton as Eric
* Patricia Ellis as Janet
* Guy Kibbee as Justice of the Peace
* Hugh Herbert as Detective
* Paul Kaye as Paul Smith
* Hobart Cavanaugh as Hotel Desk Clerk
* Robert Greig as Andrews
* Harold Waldridge as Elevator Boy

==External links==
*  
*  

 
 
 
 
 
 
 


 