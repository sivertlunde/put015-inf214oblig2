Daratt
{{Infobox Film | name = Daratt (Dry Season)
 | image = Daratt.jpg
 | caption = 
 | director = Mahamat Saleh Haroun
 | producer = Abderrahmane Sissako Mahamat Saleh Haroun
 | writer = Mahamat Saleh Haroun
 | starring = Ali Bacha Barkaïm Djibril Ibrahim Aziza Hisseine Khayar Oumar Defallah Fatimé Hadje and Youssouf Djaoro
 | music = Wasis Diop
 | cinematography = Abraham Haile Biru
 | editing = Marie-Hélène Dozo
 | distributor = 
 | Country = France / Belgium / Chad / Austria
 | released = 27 December 2006 
 | runtime = 96 minutes French
 | budget = Euro|€1,503,903 
 }}
Daratt ( : "Saison sèche") is a 2006 film by Chadian director Mahamat Saleh Haroun.

The film was one of seven films from non-Western cultures commissioned by Peter Sellars New Crowned Hope Festival to commemorate the 250th birthday of Wolfgang Amadeus Mozart.  Inspiration for the themes of revenge and reconciliation was taken from Mozarts La clemenza di Tito.

Darratt won the Grand Special Jury Prize at the 63rd Venice International Film Festival, as well as eight other prizes at Venice and the Panafrican Film and Television Festival of Ouagadougou.

== Synopsis ==
 ) (right)]]
Set in the wake of the long Chadian civil war, 16-year-old Atim (Ali Bacha Barkai) is sent by his grandfather to the city to kill Nassara (Youssouf Djaoro), the man who murdered his father before Atims birth.  Atim, carrying his fathers gun, finds Nassara running a bakery.  Unexpectedly, the taciturn Nassara takes Atim under his wing as the son he never had and begins teaching him how to run the bakery.  The emotionally conflicted Atim is drawn into the life of Nassara and his pregnant wife (Aziza Hisseine), before a finale that Variety (magazine)|Variety described as "sharp, fast and unexpected." 

== Notes and references ==
 

== External links ==
*  

 
 
 
 
 
 
 