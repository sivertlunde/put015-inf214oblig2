The Girl from Capri
{{Infobox film
| name           = The Girl from Capri 
| image          = 
| image_size     = 
| caption        = 
| director       = Frederic Zelnik 
| producer       = Frederic Zelnik
| writer         = Frederic Zelnik
| narrator       = 
| starring       = Lya Mara   Ulrich Bettac   Robert Scholz   Hermann Böttcher
| music          = 
| editing        =
| cinematography = Mutz Greenbaum   Gustave Preiss
| studio         = Zelnik-Mara-Film  
| distributor    = National-Film
| released       = 10 July 1924
| runtime        = 
| country        = Germany 
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent comedy film directed by Frederic Zelnik and starring Lya Mara, Ulrich Bettac and Robert Scholz. It premiered in Berlin on 10 July 1924. 

==Cast==
* Lya Mara   
* Ulrich Bettac   
* Robert Scholz   
* Hermann Böttcher   
* Julia Serda

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 