Zen Noir
{{Infobox film
| name = Zen Noir
| image = Zen Noir.jpg
| caption =
| director = Marc Rosenbush
| producer = Frank Crim  Erika Gardner  Marc Rosenbush
| writer = Marc Rosenbush
| starring = Duane Sharp  Kim Chan  Debra Miller   Ezra Buzzington
| music = Steven Chesne
| cinematography = Christopher Gosch
| editing = Camden Toy
| distributor =
| released =  
| runtime = 71 minutes
| country = United States
| language = English
| budget =
}} murder mystery by independent filmmaker Marc Rosenbush, starring Kim Chan, Duane Sharp, Ezra Buzzington, Debra Miller, Jennifer Siebel and Howard Fong.

==Awards==
The film, which opened in theatres on September 15, 2006, has won several prestigious film festival awards.
*Grand Jury Award for Best Feature at the Washington DC Independent Film Festival
*Audience Award for Best Feature at the Rhode Island International Film Festival
*Best Feature at the Moondance Film Festival
*Best Actress at Indiefest Chicago
*Best Cinematography at the Ashland Independent Film Festival.

==External links==
*  
*  

 
 
 
 
 
 


 