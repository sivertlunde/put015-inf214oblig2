Return to Sender (film)
{{Infobox film 
| name     = Return to Sender
| image = Return to Sender (film).jpg
| director       =Bille August
| starring       =Aidan Quinn Connie Nielsen Mark Holton
| writer         =Neal Purvis and Robert Wade
| music          =Harry Gregson-Williams
|}} 2004 film written by Neal Purvis and Robert Wade and directed by Bille August. It is also known under the title Convicted. 

The film stars Aidan Quinn, Connie Nielsen and Mark Holton.   
 IFTA Best Actor in a Feature Film award for his performance.

Quinn plays an unscrupulous attorney who is challenged by Nielsen, his latest client/target.   

==Plot==
Quinns character, a jaded ex-lawyer, has been befriending and exploiting death row convicts and selling their final letters to the media.   While attempting to foster his relationship with Nielsens character, he becomes convinced that she is innocent. 

==Reception==
On the review aggregator website Rotten Tomatoes, the film has a 33% rating under the title Convicted   and a 39% rating under the title Return to Sender 

==References==
 

== Cast ==
* Connie Nielsen as	Charlotte Cory
* Aidan Quinn as Frank Nitzche
* Tim Daly as Martin North
* Kelly Preston as Susan Kennan Mark Ryan as Mark Schlesser
* Mark Holton as Joe Charbonic
* Sara-Marie Maltha as Stella / Julie
* Bill Thomas as Gubby
* Randy Colton as Joe Hammond
* Montana Sullivan as Hammonds Son

== External links ==
* 

 
 
 
 
 
 

 