First Position
{{Infobox film
| name           =First Position
| image          = First Position 2011.jpg
| caption        = 
| director       = Bess Kargman
| producer       = Rose Caiola Nick Higgins Bess Kargman Jennilyn Merten
| writer         = 
| starring       = 
| music          = Chris Hajian
| cinematography = Nick Higgins
| editing        = Kate Amend Bess Kargman
| studio         = First Position Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
First Position is a 2011 American documentary film. It follows six young dancers preparing for the Youth America Grand Prix in New York City, an annual competition for dancers ages 9–19 to earn a place at an elite ballet company or school. Directed by Bess Kargman, it features Michaela DePrince, Aran Bell, Miko Fogarty, Jules Fogarty, Joan Sebastian Zamora and Rebecca Houseknecht as they intensively train and prepare for what could be the turning point of their lives.

==Reception==
The film garnered critical acclaim, receiving a rating of 96% on the website Rotten Tomatoes.  Manohla Dargis of The New York Times praised the film as creating "pocket portraits of children whose dedication to their art is by turns inspiring, daunting and, at times, a little frightening."  Frank Scheck of The Hollywood Reporter wrote that First Position "overcomes its predictable elements thanks to the inherent visual drama of watching children strain their bodies to the limit in obsessive pursuit of their goals." 

The film was the first runner-up for Best Documentary at the Toronto International Film Festival where it premiered, winning the Jury Prize at the San Francisco Doc Fest, and audience awards for Best Documentary at the Dallas International Film Festival and at the Portland International Film Festival, where Bess Kargman also won Best New Director. The films takings were $48,024 on its opening weekend in the first weekend of May. As of 24 June 2012, the film has grossed $894,471 in the United States. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 