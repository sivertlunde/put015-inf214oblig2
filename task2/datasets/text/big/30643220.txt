A Song for Tibet
{{Infobox film
| name           =   A Song for Tibet
| image          =   
| image size     =
| caption        =    Anne Henderson
| producer       =   Anne Henderson Ali Kazimi Kent Martin Colin Neale Abby Jack Neidik
| writer         =   Erna Buffie Anne Henderson
| narrator       =
| starring       =   
| music          =   Neil Smolar
| cinematography =   Ali Kazimi Pierre Landry Lynda Pelley
| editing        =   Anne Henderson	
| studio         =   
| distributor    =   
| released       =   1991
| runtime        =   57 minutes
| country        =     Canada English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
  Dalai Lama, Anne Henderson, A Song for Tibet received the Award for Best Short Documentary at the 13th Genie Awards as well as the Peoples Choice Award for Best Documentary Film at the Hawaii International Film Festival.     The film was co-produced by Arcady Films, DLI Productions and the National Film Board of Canada.    Ali Kazimi was director of photography.      

The film focuses on two Tibetans in exile in Canada: Thubten Samdup, who escaped from Tibet after the 1959 uprising against the Chinese, who teaches traditional performing arts in Montreal and heads the Canada-Tibet Committee; and Dicki Chhoyang, born in a refugee camp in India, who knows Tibet only through stories recounted by her parents. The film follows Dicki and Samdup from Montreal to Dharamshala, Himachal Pradesh|Dharamshala, India and also documents the Dalai Lamas first public appearance in Canada. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 