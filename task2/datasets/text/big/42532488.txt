The First Woman Who Passes
{{Infobox film
| name =  The First Woman Who Passes
| image =
| image_size =
| caption =
| director = Max Neufeld
| producer =  
| writer =  Alessandro De Stefani 
| narrator = Carlo Lombardi   Niní Gordini Cervi   Lisa Varna
| music = Carlo Innocenzi 
| cinematography = Tino Santoni
| editing = Vincenzo Zampi    
| studio = Italcine  ICI 
| released = 26 October 1940
| runtime = 82 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical comedy Carlo Lombardi and Niní Gordini Cervi. The film is set in seventeenth century France.  It was made at the Palatino Studios in Rome.

==Cast==
* Alida Valli as Gabrielle de Vervins  Carlo Lombardi as Il duca di Richelieu  
* Niní Gordini Cervi as La marchesa de Prie  
* Giuseppe Rinaldi as Raoul dAubigny  
* Lisa Varna as Marietta  
* Achille Majeroni as Il vescovo di Fleury  
* Olinto Cristina as Il conte di Vervins  
* Guglielmo Barnabò as Il console dAuvray  
* Giuseppe Pierozzi as Il duca di Borbone  
* Renato Malavasi as Andrea  
* Augusto Marcacci as Il governatore Delaroche  
* Emilio Petacci as Lagrange  
* Mario Giannini as Luigi XV  
* Diana Lante as Madame Charolais 
* Eugenio Duse as Piccard 
 
== References ==
 

== Bibliography ==
* Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 