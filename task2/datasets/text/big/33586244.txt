Midnight Warning
{{Infobox film
| name           = Midnight Warning
| image          = 
| image_size     = 
| caption        = 
| director       = Spencer Gordon Bennet
| producer       = Cliff P. Broughton (supervising producer) George W. Weeks (producer)
| writer         = Norman Battle (story) John T. Neville (scenario)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Jules Cronjager
| editing        = Byron Robinson
| studio         = 
| distributor    = 
| released       = 1932
| runtime        = 63 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Midnight Warning is a 1932 American film directed by Spencer Gordon Bennet.

The film is also known as Eyes of Mystery (new American title).

== Plot summary ==
 

== Cast ==
*William "Stage" Boyd as William "Bill" Cornish
*Claudia Dell as Enid Van Buren
*Huntley Gordon as Mr. Gordon, Hotel Manager
*John Harron as Erich
*Hooper Atchley as Dr. Steven Walcott
*Lloyd Whitlock as Rankin, Hotel Desk Clerk
*Phillips Smalley as Dr. Bronson
*Lloyd Ingraham as Adolph Klein Henry Hall as Dr. Barris - the Psychiatrist

== Soundtrack ==
 

== External links ==
* 
* 

 
 
 
 
 
 


 