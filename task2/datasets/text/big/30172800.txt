Miss Representation
{{Infobox film
| name           = Miss Representation
| image          = Miss Representation (2011).jpg
| image size     =
| caption        = 
| director       = Jennifer Siebel Newsom
| producer       = Jennifer Siebel Newsom Julie Costanzo
| writer         = Jennifer Siebel Newsom Jessica Congdon Claire Dietrich Jenny Raskin
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| music          = Eric Holland
| cinematography = Svetlana Cvetko John Behrens Ben Wolf Norman Bonney Nathan Levine-Heaney Brad Seals Boryana Alexandrova Nicole Hirsch-Whitaker
| editing        = Jessica Congdon
| studio         = Girls Club Entertainment
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = United States|$750,000 (est.)
| gross          = 
}}
 mainstream media limited and often disparaging portrayals of women.  The film premiered in the documentary competition at the 2011 Sundance Film Festival. 

== Synopsis ==
The film interweaves stories from teenage girls with provocative interviews, to give an inside look at the media and its message.  The film’s motto, “You cant be what you cant see,” underscores an implicit message that young women need and want positive role models, and that the media has thus far neglected its unique opportunity to provide them. The film includes a social action campaign to address change in policy, education and call for socially responsible business.  

==Screenings== Commission on the Status of Women.   The film premiered on January 22, 2011 at the Sundance Film Festival, and was followed by appearing at the Athena Film Festival at Barnard College in NYC in February.

== Recognition ==
The Oprah Winfrey Network acquired broadcast rights for the film following its premiere. 

; Audience Award from http://therepresentationproject.org/films/miss-representation/  2011 Palo Alto International Film Festival. 2011 Sonoma Film Festival.

; Official Selection at  2011 Atlanta Film Festival. 2011 Dallas Film Festival. 2011 Denver Film Festival. 2011 New Port Beach Film Festival. 2011 New Zealand Film Festival. 2011 San Francisco Film Festival. 2011 Silver Docs Film Festival. 2011 Sundance Film Festival.

; Other  2011 Maui Film Festival: Movies Matter Award 2012 Gracie Allen Awards: Outstanding Documentary. 

==Advocacy Efforts==

A   grew out of the film that includes 1) a Twitter campaign to call out offensive media, 2) a crowd-sourced list of media that represent women and girls fairly, 3) a virtual internship program to recruit representatives, 4) guides for media representation conversation starters, 5) guides for electing females for political office, 6) weekly action alerts, 7) gender equality principles and 8) resources & tools for action.

===Mission===
The Representation Project is a movement that uses film and media content to expose injustices created by gender stereotypes and to shift people’s consciousness towards change. Interactive campaigns, strategic partnerships and education initiatives inspire individuals and communities to challenge the status quo and ultimately transform culture so everyone, regardless of gender, race, class, age, or circumstance can fulfill their potential.
 

== See also ==
* Celluloid ceiling

==References==
{{reflist|refs=

   

   

   

   

 Lauzen, Martha, PhD., The Celluloid Ceiling: Behind-The-Scenes Employment of Women in the Top 250 Films of 2009 

 Rutgers Center for American Women and Politics 

   

}}

== External links ==

*   
*  

 
 
 
 
 
 
 