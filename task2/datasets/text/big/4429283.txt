Hondo (film)
 
{{Infobox film
| name           = Hondo
| image          = Hondo 1953.jpg
| caption        = 1953 film poster
| director       = John Farrow John Ford (uncredited, final scenes only)
| producer       = Robert M. Fellows John Wayne
| writer         = screenplay by James Edward Grant from a short story by Louis LAmour
| starring       = John Wayne Geraldine Page Ward Bond Michael Pate James Arness Leo Gordon
| studio         = Batjac Productions Wayne-Fellows Productions
| music          = Hugo W. Friedhofer Emil Newman
| cinematography = Robert Burks Louis Clyde Stoumen Archie J. Stout
| editing        = Ralph Dawson
| distributor    = Warner Bros. (original) Paramount Pictures (current)
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $3,000,000
| gross          = $4,100,000   
}}
 Western film starring John Wayne and directed by John Farrow. The screenplay is based on the July 5, 1952 Colliers short story "The Gift of Cochise" by Louis LAmour.  The book Hondo was a novelization of the film also written by LAmour, and published by Gold Medal Books in 1953.  The supporting cast features Geraldine Page as Waynes leading lady, Ward Bond, James Arness and Leo Gordon.  

The shoot went over schedule, and Farrow had to leave the production as he was contractually obligated to direct another movie. The final scenes featuring the Apache attack on the circled wagons of the Army and settlers were shot by John Ford, whom Wayne had asked to finish the film; Ford was uncredited for this work.

==Plot summary==
Hondo Lane (John Wayne) arrives on foot at a ranch where Mrs. Angie Lowe (Geraldine Page) and her son Johnny (Lee Aaker) have been deserted by her husband for months. Part Apache, Hondo tries to avoid confrontation with war chief Vittorio (Michael Pate), but finally bails out inexperienced West Point graduate Cavalry Lieutenant (Tom Irish) and old friend scout Buffalo (Ward Bond).  When Hondo finally admits he had to kill Mr Lowe (Leo Gordon) in self-defense, Angie reveals her husband was a gambler, unfaithful, unkind, and only married her for her inheritance. She disagrees with Hondos devotion to "truth", calls his desire to confess to Johnny selfish, and asks to leave with him and the remains of a wagon train heading towards his farm that looks just like hers.

==Details== US Army break her two horses for riding, so Lane offers to break a horse himself. He also asks where her husband is, and she says he is rounding up calves and cattle in the mountains and should return soon.

Johnny watches with fascination as Lane saddles one of the horses and rides the bucking and untamed animal with ease. Lane also offers to do a few chores around the ranch, including sharpening an axe and chopping firewood. Lane deduces by the neglected work around the ranch that her husband has not been at the ranch for some time, a fact she confesses is true. When night falls and it starts to rain, Angie offers to let Lane sleep in her home on a floor bed in the corner. Angie sees that the butt of his rifle is inscribed "Hondo" Lane, whom she knows had killed three men the year before, but doesnt know the circumstances. She attempts to shoot him, but due to the first chamber being empty for safety, Hondo is not hurt. He loads the chamber and tells her to keep it that way and keep it high, out of Johnnys reach.
 guidon on two Indians, whom he subsequently killed.  It is now clear to the Major (Paul Fix) that all of the Apache nation is raiding and killing settlers. At the ranch, Angie and Johnny are beset by Apaches, led by Chief Vittorio (Michael Pate) and his main under-chief, Silva (Rodolfo Acosta). Angie is not made nervous by their presence as she has always let them use their water, and they had never attacked her family before. Soon, however, they are manhandling Angie, and Johnny emerges from the house with the loaded pistol and shoots at Silva, nicking Silva in the head and then, as Silva recovers and approaches him, he throws the pistol at Silva. Vittorio is impressed by Johnnys bravery and makes him an Apache blood brother by cutting Johnnys thumb with a knife and giving him an Apache name. Vittorio also wonders where Angies husband is and she tells him that hell return soon. Vittorio tells her that unless her husband does so, she must take an Apache husband because the boy needs a father to teach him to become a man.
 Frank McGrath) follow Hondo through the desert as he makes his way to Angies ranch. Hondo camps near a river but leaves it when he detects two Indians stalking him nearby. Lowe enters the camp and he and his guide are attacked by the two Indians. The guide is killed, but Hondo shoots and kills an Apache about to kill Lowe. Lowe is briefly grateful but turns his gun on Hondo in retaliation for the bar beating. Hondo defends himself, killing Lowe. Hondo finds a tintype of Johnny alongside Lowes body, confirming that Lowe is Johnnys father and Angies husband.

Continuing towards Lowes ranch, Hondo runs into an Apache party, who pursue Hondo through the desert. He kills several but they eventually capture him. They take Hondo to the top of a nearby mesa when Vittorio appears. They stake him out and begin to torture and prepare to kill him because he is wearing his old Army hat and they wish to find out the location and number of the Cavalry soldiers. An Indian shows Vittorio the picture of Johnny from Hondos saddlebag, and Vittorio thinks Hondo is Angies husband. He orders the Indians  to untie him; and Silva declares the blood rite as Hondo had killed his brother. Knives are used in the fight of the blood rite. Silva wounds Hondo in the shoulder, but Hondo pins Silva to the ground. Hondo puts his knife to Silvas throat, and gives him the option to take back the blood rite or die as did his brother. Silva gives in. Vittorio takes Hondo to Angies ranch, and when Vittorio asks if Hondo is her husband, she lies, saving Hondo. The Chief warns Hondo to raise Johnny in the Apache way and leaves them.
 pony soldiers will soon return. He asks Hondo not to join them and to keep the Indians location a secret. Hondo promises to do the first but not the latter, and Vittorio shows respect for Hondos truthfulness. Angie tells him she loves him, and they cement their relationship with a kiss.
 Winchester rifle bushwhacked Lowe. Angie overhears Lennies demands.
 womanizing and gambling. She says it would be an unkind thing to tell the boy about the true nature of his fathers death and that the secret wont follow them to Hondos ranch in California. Hondo responds to her emotional plea with an Indian word that seals a marriage|squaw-seeking ceremony, "Varlabania", which he tells her means "forever". The Army leaves to move further on into Apache territory and as promised Hondo refuses to go with them but confirms with Buffalo that he knows where Vittorio and his party are and that the young Lt. is leading them into a massacre.  Buffalo knows but he also knows that scouts such as himself have been helping to train young West Point officers for many years.
 circles their wagons. They escape the encirclement twice but the Apaches continue their pursuit. Hondo loses his mount and is attacked by Silva, but Hondo kills him, retrieving Lt. McKays uniform shirt from his body. The Indians retreat again to choose a replacement chief.

Lt. McKay says that General Crook will be arriving in the territory with a large force to pursue the Apache. Hondo sadly notes the end of the Apache "way of life," denoting that it is too bad as it was a good way.
The movie ends with the idea that once back to the fort, Hondo, Angie and Johnny would continue on to Hondos ranch in California as a family.

==Cast==
* John Wayne as Hondo Lane
* Geraldine Page as Angie Lowe
* Ward Bond as Buffalo Baker
* Michael Pate as Vittorio – Chiricahua Apache Chief
* James Arness as Lennie – Army Indian Scout
* Rodolfo Acosta as Silva
* Leo Gordon as Ed Lowe
* Tom Irish as Lieutenant McKay
* Lee Aaker as Johnny Lowe
* Paul Fix as Major Sherry
* Rayford Barnes as Pete – Card Player in Saloon Frank McGrath as Lowes Partner
* Morry Ogden as Horse Rider – Opening Scene
* Chuck Roberson as Kloori – Apache warrior / Cavalry Sergeant killed in Indian Attack / Stunt Double for John Wayne.
* Sam as Hondos dog

==Filming locations==
* Camargo, Chihuahua, Mexico
* Chihuahua (state)|Chihuahua, Mexico
* Delle, Utah, USA
* Lone Peak|Lonerock, Utah, USA Skull Valley, Utah, USA
* Snow Canyon State Park, Utah, USA
* Tooele County, Utah, USA
* Utah, USA

==Development and production== Batjac purchased the rights to Louis LAmours short story "The Gift of Cochise" in 1952, and set Waynes friend and frequent collaborator James Edward Grant to write the adaptation. LAmour was given the rights to write the novelization of the film, which became a bestseller after the films release. The film shoot was scheduled for the summer of 1953 in the Mexican desert state of Chihuahua (state)|Chihuahua. 
 3D format. Warner Brothers supplied the production with the newly developed "All Media Camera," which could shoot in any format, including 3-D, using twin lenses placed slightly apart to produce the stereoscopic effect necessary for it. Despite the fact that they were smaller than the twin camera process used previously for 3D, the All-Media Cameras were still bulky and made the film shoot difficult, causing delays when transporting the cameras to remote desert locations. Further, the director John Farrow and DP Robert Burks were unfamiliar with the new technology and had trouble adjusting to using it, and the cameras were frequently broken due to wind blowing sand into the mechanism or from other inclement weather conditions. Farrow used the technology to produce fewer gimmicks than other 3D films did at the time, with only a few scenes show people or objects coming at the camera, such as gunfire or knives. Instead he preferred to use it to increase the depth of the expansive wide shots of the Mexican desert, or figures against a landscape.
 Broadway stage Oscar nomination for Best Supporting Actress, the first of only two acting nominations ever for a film shot or presented in 3D; the award went to Donna Reed for From Here to Eternity. 

John Ford shot the final scenes of the wagon train attack as a favor for Wayne when Farrow had to leave the film before its completion due to a conflicting contractual obligation to begin another film.  Ford accepted no credit for directing the last sequence of the film. 

==Theatrical release== House of Wax, producing a richer sense of perspective. 

The film was released on November 27, 1953. Hondo played across the country in the 3D format as it was intended using the Polaroid 3D projection system and became quite popular with audiences, eventually grossing $4.1 million and placing it sixteenth in box office for that year. 

==Restoration and DVD release==
An initial restoration of Hondo was overseen by Waynes son Michael Wayne|Michael, head of Batjac Productions, in the late 1980s culminating in a 3D television broadcast of the movie in June 1991. 3D glasses were sold to viewers with proceeds going to charity. 

A frame-by-frame digital restoration by Prasad Corporation of the film was later completed, and the DVD of it was released on October 11, 2005. 

==Notes== Native Americans from New Mexico.  The original book depicted the events taking place in Southeast of what is now Arizona. The action scenes and the 3D photography are also highpoints of the film. 
 The Undefeated. 

Film footage from Hondo was later incorporated into the opening sequence of Waynes last film, The Shootist, to illustrate the backstory of Waynes character. 

This film marked one of the first appearances of Geraldine Page, who had been a popular stage actress. 

Part of a 1988 episode of Married... with Children, entitled "All in the Family," has Al Bundy readying himself to watch Hondo in peace during a three-day weekend, but Peggy Bundy|Peggys family comes to visit, and their ensuing problems prevent him from seeing the film, just as their antics prevented him from seeing Shane (film)|Shane the year before. Moreover, a 1994 episode of Married... with Children, entitled "Assault and Batteries," has a subplot in which Al is desperate not to miss a television airing of Hondo because, as he explains, it only airs "once every 17 years." Al does miss this airing at the end of the episode and will have to wait until February 18, 2011 to see it again.   Al holds the film in very high esteem, once telling Peggys family members, "Your lives are meaningless compared to Hondo!"

One of the most prevalent images of John Wayne remains a full-length publicity photo from the film in which Wayne wears the buckskin suit and military hat. 

The two male and female main character names from Hondo ("Lane" and "Mrs. Lowe") reappear in John Waynes 1973 western film The Train Robbers, with Ann-Margret instead of Geraldine Page as Mrs. Lowe. 

==See also==
*John Wayne filmography

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 