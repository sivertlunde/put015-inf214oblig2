The Bondage Master
{{Infobox film
| name           = The Bondage Master
| image          = The Bondage Master.jpg
| image_size     =
| caption        = Video cover to The Bondage Master
| director       = Hitoshi Hoshino
| producer       =
| writer         = Atsushi Furushō
| narrator       =
| starring       = Yukijirō Hotaru Yokiru Ikuta Hitomi Shiraishi
| music          = Atorie Shira
| cinematography = Shigeru Komatsubara
| editing        =
| studio         =
| distributor    = Tohokushinsha Film (Japan) Central Park Media (USA)
| released       = April 26, 1996 (Japan) September 26, 2000 (USA)
| runtime        = 83 minutes
| country        = Japan Japanese
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

 , also known as Rope Detective, is a 1996 Japanese V-Cinema erotic thriller directed by Hitoshi Hoshino (Jin Hoshino) and starring Yukijirō Hotaru.

==Plot synopsis==
Shiro is the Bondage Master who has a special technique with ropes and women. When a model is found murdered and tied with Shiros signature style, he has to find the real murderer to clear his name while eluding the vengeance of the models yakuza boyfriend.

==Cast==
* Yukijirō Hotaru as Shiro
* Yokiru Ikuta as Rumi
* Ai Yasunaga as Keiko
* Hiromitsu Noriyasu as Hitoshi
* Hitomi Shiraishi as Saya

===English Voice Cast=== Sandy Williams as Shiro Suzy Prue as Rumi Petra Kosic as Keiko
* Richard St. Louis as Hitoshi Pink Champale as Saya Kandi Snackwell as Reiko
* Tristan Goddard as Vendor
* Jason Maxwell as Akiyama
* Barbara Busch as Satomi
* Carrie Montgomery as Rina
* Ted as Yamaguchi Fergus Lawless as Masaya
* Wop-Wop as Miyoshi
* Eric Snare as Sano
* Ted Newly as Kayama

==Release==
The Bondage Master was released as a VHS video in Japan on April 26, 1996 by Tōhokushinsha.  Central Park Media had licensed the film under their Asia Pulp Cinema label and released it on an English subtitled VHS on September 26, 2000.  Central Park Media also released a DVD featuring an English audio track on June 8, 2004. 

==References==
 

==See also==
*Sadism and masochism in fiction

==External Links==
*  

 
 
 
 
 
 
 


 
 