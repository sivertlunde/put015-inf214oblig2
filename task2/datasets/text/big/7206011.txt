Hard Ticket to Hawaii
{{Multiple issues|
 
 
}}
{{Infobox film
| name           = Hard Ticket to Hawaii
| image          = Hard Ticket to Hawaii (film) cover.jpg
| image_size     = 180px
| caption        = VHS cover
| director       = Andy Sidaris
| producer       = Arlene Sidaris
| writer         = Andy Sidaris
| narrator       =
| starring       = Ronn Moss Dona Speir Hope-Marie Carlton  Cynthia Brimhall  Harold Diamond Rodrigo Obregon
| music          = Kevin Klinger Gary Stockdale
| cinematography = Howard Wexler
| editing        = Michael Haight
| distributor    = Malibu Bay Films
| released       =  
| runtime        = 100 min.
| country        = US
| language       = English
| budget         =
}}

Hard Ticket to Hawaii is a 1987 action adventure film starring Ronn Moss (Rowdy Abilene), Dona Speir (Donna), Hope Marie Carlton (Taryn), Cynthia Brimhall (Edy), and Harold Diamond (Jade).

==The Triple B Series by Andy Sidaris==
 
*1. Malibu Express (1985)
*2. Hard Ticket to Hawaii (1987)
*3. Picasso Trigger (1988)
*4. Savage Beach (1989) Guns (1990) Do or Die (1991)
*7. Hard Hunted (1992)
*8. Fit to Kill (1993)
*9. Enemy Gold (1993)
*10. The Dallas Connection (1994)
*11. Day of the Warrior (1996)
*12.   (1998)

==Reception==
According to Rotten Tomatoes, there were no critic scores respectively, however, the audience reviews scored to about 45%.  Despite the negative audience reviews, the film has garnered a cult following due to its over-the-top humor, cheesy dialogue, and the absurdity. In 2014, Paste Magazine named the film the "best B movie of all time." 

==References==
 

==External links==
* 
*  
*  on  

 
 

 
 
 
 
 
 

 