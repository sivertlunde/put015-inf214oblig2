Ellam Avan Seyal
{{Infobox Film
| name           = Ellam Avan Seyal
| image          = Ellam Avan Seyal.jpg
| director       = Shaji Kailas
| producer       = Karumari Kandhasamy J. Durai
| writer         = Shaji Kailas Prabhakaran
| story          = A. K. Sajan
| narrator       = 
| starring       = R. K. (actor)|R. K. Raghuvaran Roja Selvamani Bhama Manivannan Visu Vadivelu Ashish Vidyarthi
| music          = Vidhyasagar Ishaan Dev Rajamani
| cinematography = Raja Rathinam
| editing        = Don Max
| distributor    = R.K Arts
| released       = November 28, 2008
| runtime        = 
| country        = India Tamil
| awards         = 
| budget         = 
}}
 Tamil film directed by Shaji Kailas. The film stars R. K. (actor)|R. K., Ashish Vidyarthi, Raghuvaran, Roja Selvamani, Bhama, Manivannan, Visu and Vadivelu in a modern day vigilante story. 

==Synopsis==
A lawyer, who defends criminals, wages vigilante justice against them in private. Movie deals with the murder of a girl who was ragged and brutally murdered by her classmates. The lawyer later finds the truth that she was brutally raped by college authorities for a complete night.

==Production==
Ellam Avan Seyal is the remake of Suresh Gopi starring Chinthamani Kolacase, also directed by Shaji Kailas. The film was also remade as Telugu with Sri Mahalakshmi and Sri Hari playing the role of Suresh Gopi. The Malayalam and Tamil versions were successful, but the Telugu version did poorly. The Telugu version had a different director.

==Cast==
* R. K. (actor)|R. K. as Advocate Laxman Krishna aka LK 
* Bhama as Chinthamani
* Ashish Vidyarthi as Anbukkarasu
* Manoj K. Jayan as David Williams
* Vadivelu as Vandu Murugan
* Raghuvaran as Jagadeeswaran
* Nasser as Easwarapandian
* Manivannan as Veeramani
* Visu Roja
* Sukanya
* Lakshmy Ramakrishnan as Olga Mariadas Vijayakumar as Ramakrishnan
* Thalaivasal Vijay
* Sangili Murugan
* Rachana Maurya (Item number)

==References==
 

==External links==

 
 
 
 
 
 


 