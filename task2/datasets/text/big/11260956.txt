Volcano (animated short)
{{Infobox Hollywood cartoon|
| cartoon_name = Volcano Superman
| image = Volcano (animation) title card.jpg
| caption = Title card from Volcano
| director = Dave Fleischer
| story_artist = Bill Turner   Carl Meyer
| animator = Willard Bowsky   Otto Feuer
| voice_actor = Bud Collyer   Joan Alexander   Jackson Beck
| musician = Sammy Timberg
| producer = Max Fleischer
| studio = Fleischer Studios
| distributor = Paramount Pictures
| release_date = July 10, 1942 (USA)
| color_process = Technicolor
| runtime = 8 min. (one reel)
| preceded_by = Electric Earthquake (1942)
| followed_by = Terror on the Midway (1942)
| movie_language = English
}}

Volcano is the eighth of the seventeen animated Technicolor short films based upon the DC Comics character of Superman, originally created by Jerry Siegel and Joe Shuster. This eight-minute animated short, produced by Fleischer Studios, features Supermans adventures in saving a small island community from a volcanic eruption.  It was originally released on July 10, 1942 by Paramount Pictures.   

==Plot==
 
The story begins with a narrator describing Mt. Monokoa, a volcano which has been dormant for about 300 years:

"On this peaceful island crowned by the great volcano, Mt. Monokoa, occurred the mightiest erruption that ever shook the Earth, burying the beautiful city beneath it in molten lava, and creating destructive tidal waves that raced around the world. For 300 years, this mighty volcano lay dormant. A new and more beautiful city sprang up at its base. But now, after centuries of inactivity, slight tremors are being felt. At the Bureau of Meteorology, a group of scientists watchfully check delicate instruments to determine the seriousness of this renewed activity."

The scene shows various scientists at the Bureau of Meteorology checking the volcano, which is showing signs of eruption, threatening the small town yet again. Lois Lane and Clark Kent are sent to the scene by Perry White, who gives each of them steam tickets to the island, as well as press passes, ordering Lois to keep them safe as she stows them in her purse.

On arrival, Clark cant find his press pass (Lois has it hidden in her purse), and goes into town to acquire another as Lois takes a tour of the volcano. Her guide tells her that the plan to save the city is to blow a hole in the side of the volcano, thus altering the course of the lava flow from the town, to the ocean on the other side of the island.  Suddenly, the volcano begins to erupt, and lava spews out of its top, flowing towards the town.  Lois and her guide are separated.  Two men on the other side of the volcano scramble to get to the machinery which will blow a hole in its side, but a giant boulder cuts the wire connecting the dynamite to the machine, rendering it useless.  From the town below, Clark sees the volcano let out a particularly explosive blast and says, "This looks like a job for Superman."

He changes into his costume and flies up to the volcano, catching a huge boulder hurtling toward the town and flinging it into the ocean.  This momentarily leaves him stunned on a ledge on the mountainside as the lava creeps nearer and nearer to the town and Lois is struggling to get away from the volcano by edging hand over hand along a cable wire connecting two mountains.  She makes it to a cable car just as the cable begins to give way.  Superman saves her, then flies back, catches the cable car and flings it at a mountain near the advancing lava, causing a landslide which dams up the lava flow to the town.  Then Superman flies to the dynamite machinery and notices the cut wire.  He pulls the two wires together to complete the connection, and in a huge blast the side of the volcano explodes, and the remaining lava slides harmlessly into the ocean.

In the final scene, Lois and Clark are on the boat home, and Clark commends Lois for her excellent story.  Lois remarks that its too bad he wasnt there as well, whereupon Clark pulls the second press pass sticking out of her open purse and says, "I wouldve been, if I hadnt lost my pass."

==Cast==
*Bud Collyer as Superman/Clark Kent, Professor, Police Officer, Demolitions Expert #1
*Joan Alexander as Lois Lane
*Jackson Beck as the Narrator, Perry White, Guard, Demolitions Expert #2

==References==
 

==External links==
*  
*  at the Internet Archive
*  at the Internet Movie Database
*   on YouTube

 

 
 
 
 
 