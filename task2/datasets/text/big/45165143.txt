Dope (film)
{{Infobox film
| name           = Dope
| image          = DopeTeaserPoster.jpg
| alt            = 
| caption        = Poster
| director       = Rick Famuyiwa
| producer       = Forest Whitaker Nina Yang Bongiovi
| writer         = Rick Famuyiwa
| starring       = Shameik Moore Tony Revolori Kiersey Clemons Kimberly Elise Chanel Iman Tyga Blake Anderson Zoë Kravitz A$AP Rocky
| music          = 
| cinematography = Rachel Morrison
| editing        = Lee Haugen
| production companies = Significant Productions JuntoBox Films
| distributor    = Open Road Films  (United States)  Sony Pictures  (Internationally) 
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Dope is a 2015 American comedy-drama film written and directed by Rick Famuyiwa and starring Shameik Moore, Tony Revolori, Kiersey Clemons, Blake Anderson, Zoë Kravitz, A$AP Rocky, Tyga, Keith Stanfield, Casey Veggies, Vince Staples, Chanel Iman and Rick Fox. The film was produced by Forest Whitaker and executive produced by Puff Daddy and Pharrell Williams.  
 Sony (who will distribute the film internationally) for a reported $7 million plus $20 million for marketing and promotion.   It has been selected to close the Directors Fortnight section at the 2015 Cannes Film Festival.    Dope is scheduled to be released in North American theaters on June 19, 2015. 

==Cast==
 
*Shameik Moore as Malcolm
*Kiersey Clemons as Diggy
*Tony Revolori as Jib
*Zoë Kravitz as Nakia
*A$AP Rocky as Dom
*Blake Anderson as Will
*Keith Stanfield as Bug
*Rick Fox as Councilman Blackmon
*Kimberly Elise as Lisa Hayes
*Chanel Iman as Lily
*Allen Maldonado as Alan the Bouncer
*Ashton Moio as Lance
*Roger Guenveur Smith as Austin Jacoby 
*Deaundre Bonds as Stacey Quincy as Jaleel
*Mimi Michaels as Mindy
*Tyga as DeAndre
*Larnell Stovall as The Driver
*Julian Brand as Mario
*Lidia Porto as Marta
*Bruce Beatty as Mr. Bailey
*Narrated by Forest Whitaker
 

==Reception==
Dope received universal acclaim from film critics. On Rotten Tomatoes, the film holds a rating of 100%, based on 10 reviews, with an average rating of 7.8/10.  On Metacritic, the film has a score of 76 out of 100, based 6 critics, indicating "generally favorable reviews".  The Guardian gave the film five stars  out of five, describing the entire cast as "revolutionary". 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 