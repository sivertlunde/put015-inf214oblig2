Meet the Baron
 
 
{{Infobox film
| name           = Meet the Baron
| image          = 
| image_size     = 
| caption        = 
| director       = Walter Lang
| producer       = David O. Selznick
| writer         = Norman Krasna Herman J. Mankiewicz
| narrator       = 
| starring       = Jack Pearl Jimmy Durante Edna May Oliver Zasu Pitts Ted Healy Moe Howard Larry Fine Curly Howard
| music          =    
| cinematography =  Allen G. Siegler
| editing        =  James E. Newcom
| distributor    = Metro-Goldwyn-Mayer
| released       =   
| runtime        = 68 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Meet the Baron (1933) is a comedy film starring Jack Pearl, Jimmy Durante, Edna May Oliver, Zasu Pitts, Ted Healy and the Three Stooges. The title of the film refers to Pearls character of Baron Munchhausen, which he made famous on his radio show. 

==Plot==
A couple of bunglers (Jimmy Durante and Jack Pearl) are abandoned in the jungles of Africa by Baron Munchausen. A rescue team mistake Pearl for the missing Baron, and take the two of them back to America where they receive a heros welcome.

The phony Baron is invited to speak at Cuddle College, run by Dean Primrose (Edna May Oliver). There he falls for Zasu Pitts and meets three crazy janitors (The Three Stooges), and faces exposure as a fraud.

==Cast==
*Jack Pearl as The Famous Baron Munchausen of the Air
*Jimmy Durante as Joe McGoo - the Favorite Schnozzle of the Screen
*Zasu Pitts as Zasu, Upstairs Maid
*Ted Healy as Head Janitor
*Edna May Oliver as Dean Primrose
*The Metro-Goldwyn-Mayer Girls as Dancers
*Henry Kolker as Baron Munchausen
*William B. Davidson as 
*Moe Howard as Janitor
*Larry Fine as Janitor Jerry Howard as Janitor
*Ben Bard as Charley Montague

==See also==
*Three Stooges Filmography

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 

 