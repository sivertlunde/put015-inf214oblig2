Delhi Belly (film)
 
 

{{Infobox film
| name = Delhi Belly
| image = Delhi belly poster.jpg
| caption = Theatrical release poster
| director = Abhinay Deo
| producer = Aamir Khan Kiran Rao Ronnie Screwvala Jim Furgele
| writer = Akshat Verma
| based on =   Imran Khan Vir Das Kunaal Roy Kapur Poorna Jagannathan Shenaz Treasurywala
| music = Ram Sampath
| cinematography = Jason West
| editing = Huzefa Lokhandwala
| studio = Aamir Khan Productions
| distributor =UTV Motion Pictures
| released =  
| runtime = 100 minutes
| country = India
| language = English Hindi
| budget = 250&nbsp;million rupees (US$4 million)
| gross = 920&nbsp;million rupees (US$14.5 million)
}}
 Indian black Imran Khan, Dhobi Ghat Tamil as Settai. 

==Plot==
 Imran Khan), Delhi Belly. Nitin hands Sonias package to Arup for delivery to Somayajulu, along with a package containing his stool sample for delivery to Nitins doctor. Arup mixes up the two bags. Somayajulu, furious, tortures Vladimir to find his package.

Meanwhile, Nitin photographs his landlord with a prostitute. He sends an envelope with the photographs to his landlord to blackmail him. Tashi is with Sonia, when his colleague Menaka (Poorna Jagannathan) calls him on the pretext of work. When he reaches the place he realizes that it is just a party and Menaka called him just to have fun. Menakas ex-husband Rajeev sees them together and gives Tashi a black-eye in a fit of jealousy. Tashi retaliates and knocks Rajeev out. As Tashi and Menaka leave they are chased by a furious Rajeev and his friends who shoot at them. The duo barely manages to escape.

Vladimir informs Somayajulu that the mix-up must have been caused by Sonia as she didnt know what she was carrying in the package. Somayajulu calls Sonia, informs her about the mix-up and asks her to give him the address of the person who had delivered the package. When Tashi arrives into his apartment, he walks into Somayajulu who has Arup standing on a stool with a noose around his neck. On hard interrogation, Somayajulu discovers the mix-up and realizes that the package must be with Nitins doctor.

Nitin gets the package from his doctors office, wherein Somayajulu finds his thirty diamonds hidden inside. Upon recovering his booty, he orders his henchmen to kill the three roommates. One of them is about to shoot Tashi, when another kicks the stool on which Arup was standing to hang him. Luckily for the roommates, the ceiling of the apartment collapses, since it cant take Arups weight. The cave-in knocks out Somayajulu and his men, leaving one with broken arms. Tashi, Arup and Nitin escape with the diamonds and spend the night at Menakas place. The next day they sell the diamonds to a local jeweller.

As the roommates prepare to get out of town with the money, they get a call from Somayajulu who has kidnapped Sonia. He threatens to kill her if they dont return the diamonds. The trio try to buy back the diamonds from the jeweller, who demands double the sale amount.

Without the money, Tashi comes up with a plan. Nitin, Arup, Tashi and Menaka disguise themselves in burqa and rob the jeweller, leaving him the bag of money. They flee in Tashis car with the police on their tail and go to the hotel where Somayajulu is holding Sonia. As they are about to make the exchange with Somayajulu, the police arrive at the hotel room. There is a shoot-out between the police and Somayajulus gang.

Nitin, Arup, Tashi and Sonia, who had hit the floor during the gunfight, are left as the only survivors. Menaka, who by now realises that she likes Tashi, is upset to learn about his engagement and walks away from him. Tashi breaks off his engagement to Sonia. Later, it is revealed that Nitin did not return the cash to the jewellery store owner, and had kept most of the money for himself (whereupon he abandons blackmailing the landlord). The film ends when Menaka comes to the roommates apartment to return Tashis cars hubcap lost while escaping from Rajeev. Tashi jumps into her car through the open window and kisses her passionately.

Producer-actor Aamir Khan is seen dancing in a song and dance performance as the credits start rolling.

==Cast== Imran Khan as Tashi Dorjee Lhatoo
* Kunaal Roy Kapur as Nitin Berry
* Vir Das as Arup
* Poorna Jagannathan as Menaka Vashisht
* Shenaz Treasurywala as Sonia Mehra
* Raju Kher as Zubin Mehra (Sonias Father)
* Vijay Raaz as Somayajulu
* Lushin Dubey as Mrs. Mehra (Sonias Mother)
* Paresh Ganatra as Manish Chand Jain
* Rahul Pendkalkar as Prateek Jain Rahul Singh as Rajeev Khanna (Menakas Ex-Husband)
* Rajendra Sethi as Sudhir Adlakha (the jeweller)
* Pradeep Kabra as Somayajulus Man
* Kim Bodnia as Vladimir Dragunsky
* Anusha Dhandekar as VJ Sophaya
* Aamir Khan as Disco Fighter (Guest Appearance in song "I Hate You (Like I Love You)")

==Production==

===Development=== Dhobi Ghat. 

===Casting=== Imran Khan ensemble piece. Its not a film about Imran Khan. He is only one of the several protagonists. There are others whose characters are just as important. There is Kunal Roy Kapoor, Vir Das and Poorna Jagannathan, an Indian actress from LA".  The film was stuck at the editing table for a long time. Initially, Aamir was to spearhead the editing but he got busy with his own films. 

==Release== Government of Harvard India Conference 2012. Director Abhinay Deo was invited as a guest. 

===Critical reception=== The Telegraph called Delhi Belly "an insanely funny ensemble comedy" and praised writer Akshat Vermas "original screenplay, which knows the difference between physical comedy and slapstick humour."  Behindwoods gave a score of three and a half stars and said that the film was "Only for those with a cast iron stomach." further citing "In conclusion, it may be said that Delhi Belly caters to a section of the populace that is cool when shit happens."  Taran Adarsh of Bollywood Hungama rated the movie with four and a half stars and wrote – "Eventually, Delhi Belly works big time predominantly for the reason that its a pioneering motion picture, an incredible film that dares to pierce into an untapped and brand new terrain. The unblemished, racy screenplay coupled with super performances and a chartbusting musical score will make it a winner all the way."  Nikita Kapoor of FilmiTadka rated Delhi Belly with four out of five stars and wrote in her review "a special shout goes out to the writer of this brilliant stuff, Akshat Verma, Delhi Belly is a first of its kind Adult Comedy in India, this can seriously turn out to be a path breaker, trend setter, but yes I also worry, in an industry which likes to follow the formula, Indian shores might soon hit with gross and vulgar rip-offs of American Pie, etc."  Mihir Fadnavis of Daily News and Analysis called the script "hilarious" and "bitingly perceptive" and gave the film four out of five stars, saying "I havent had this much fun at the movies in a long time".  Mathures Paul of the The Statesman gave the film four out of five stars, and wrote, "Its a fashionable film for fashionable youth."  Raja Sen of Rediff gave the movie three stars out of a possible five, saying "Delhi Belly has a tight, pacy plot which has lots of satisfying little set-ups and pay-offs".  Rajeev Masand of CNN-IBN gave the film three and a half out of five stars, saying "Delhi Belly is a filthy comic thriller that works because its a smartly paced wild-ride". He also praised Vijay Raazs performance, saying that the gangsters role had been "played wonderfully". 

The movie also received high praise from critics outside India. Lisa Tsering of The Hollywood Reporter called it a "Sexy, filthy and thoroughly entertaining comedy" and that it "marks a welcome shift in contemporary Indian cinema." She however pointed out that Aamir Khan "overstays his welcome" in his cameo at the end of the film, and that "a momentary glimpse would have had more impact."  Kevin Thomas of The Los Angeles Times said that "Akshat Vermas script is imaginative and funny, the films stars are engaging and "Delhi Belly" adds up to pleasing escapist fare."  Peter Bradshaw of The Guardian also praised the film, saying that "The sheer daftness and goofiness of this Bollywood comedy-farce makes it likable." 

Other reviewers, however, deplored the scatological basis of most of the humour and the hackneyed scenarios in the movie. Shubhra Gupta of The Indian Express had this to say in her review: "After a while, the continuous bad tummy rumbles and farts, and the non-stop cussing, wears thin. And please, wearing burqas as disguise is not the only way you can have characters on the run in the grungier parts of town, even if you overlay the chase with the ultra-clever, super-catchy Bhaag D K Bose ditty. There are, believe us, other ways." 

===Box office===
The film opened very well at the box office. It grossed   in the first week of screening all around India and US$1.6 million overseas.   The film grossed   in India by the end of its third week, being declared a super hit.  Delhi Belly grossed   worldwide.   

== Remakes == Tamil as Settai.

==Soundtrack==
{{Infobox album 
| Name = Delhi Belly
| Type = Soundtrack
| Artist = Ram Sampath
| Cover =
| Released = 6 June 2011
| Recorded = 2010-2011 Feature film soundtrack
| Length = 35:07
| Lyrics = Amitabh Bhattacharya, Ram Sampath, Akshat Verma, Munna Dhiman, Chetan Shashital
| Label = UTV Music
| Producer = Ram Sampath
| Reviews =
| Chronology = Ram Sampath
| Last album = Luv Ka The End (2011)
| This album = Delhi Belly (2011)
| Next album = Talaash (2012 film)|Talaash (2012)
}} expletive in North India.  Akshat Verma came up with the idea of using the phrase D K Bose. Abhinay Deo and Aamir Khan gave their nod to the song as they felt the catch phrase went with the young and irreverent theme of the film. 

===Track listing===
{{Track listing
| extra_column = Performer(s)
| lyrics_credits = yes
| title1 = Bhaag D.K. Bose
| extra1 = Ram Sampath
| lyrics1 = Amitabh Bhattacharya
| length1 = 4:02
| title2 = Nakkaddwaley Disco, Udhaarwaley Khisko
| extra2 = Keerthi Sagathia
| lyrics2 = Akshat Verma, Munna Dhiman
| length2 = 3:58
| title3 = Saigal Blues
| extra3 = Chetan Shashital
| lyrics3 = Chetan Shashital, Ram Sampath
| length3 = 3:56
| title4 = Bedardi Raja
| extra4 = Sona Mohapatra
| lyrics4 = Amitabh Bhattacharya
| length4 = 2:58
| title5 = Ja Chudail
| extra5 = Suraj Jagan Hook Lyrics : Akshat Verma 
| length5 = 3:18
| title6 = Tere Siva
| extra6 = Ram Sampath, Tarannum Mallik
| lyrics6 = Munna Dhiman
| length6 = 4:40
| title7 = Switty Tera Pyaar Chaida
| extra7 = Keerthi Sagathia
| lyrics7 = Munna Dhiman
| length7 = 2:54
| title8 = I Hate You (Like I Love You)
| extra8 = Keerthi Sagathia, Shazneen Arethna, Sona Mohapatra, Aamir Khan
| lyrics8 = Akshat Verma, Ram Sampath
| length8 = 5:50
| title9 = Badardi Raja (Remix)
| extra9 = Sona Mohapatra
| lyrics9 = Amitabh Bhattacharya
| length9 = 3:04
| title10 = Switty (Punk)
| extra10 = Keerthi Sagathia, Ram Sampath
| lyrics10 = Munna Dhiman
| length10 = 3:30
}}

==Sequel==
With the success of Delhi Belly, there were several rumours going around speculating a sequel in the "Disco Fighter". When questioned about the possibility of the sequel Kiran Rao, director wife of Aamir Khan stated ""The film is nowhere in the scene. We havent started planning or strategising anything for the sequel at all. Whatever news is out till now is baseless". 

==Accolades==
{| class="wikitable"
|-
! Ceremony
! Category
! Recipient
! Result
|- 57th Filmfare Awards  Best Film
| Delhi Belly
| rowspan="4"  
|- Best Director
| Abhinay Deo
|- Best Supporting Actor
| Vir Das
|- Best Music Director
| Ram Sampath
|- Best Screenplay
| Akshat Verma
| rowspan="3"  
|- Best Editing
| Huzefa Lokhandwala
|-
| Best Production Design
| Shashank Tere
|- 18th Colors Screen Awards   Best Film
| Delhi Belly
| rowspan="15"  
|- Best Film
| Abhinay Deo
|- Best Supporting Actor
| Kunal Roy Kapur
|- Best Supporting Actress
| Poorna Jagannathan
|- Best Comedian
| Kunal Roy Kapur
|-
| Best Comedian
| Vijay Raaz
|-
| Best Ensemble Cast
|
|- Most Promising Newcomer – Female
| Poorna Jagannathan
|- Best Background Music
| Ram Sampat
|- Best Male Playback
| Ram Sampat
|- Best Music Director
| Ram Sampat
|- Best Dialogue
| Akshat Verma
|-
| Best Cinematography
| Jason West
|-
| Best Sound Design
| Vinod Subramaniam and Dwarak Warrier
|-
| Best Choreography
| Farah Khan
|-
| Screen Award for Best Screenplay
| Akshat Verma
| rowspan="4"  
|- Best Story
| Akshat Verma
|- Best Editing
| Hufeza Lokhandwala
|-
| Best Production Design
| Shashank Tere
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 