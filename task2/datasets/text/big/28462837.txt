Electric Moon
{{Infobox film
| name           = Electric Moon
| image          =
| image size     =
| alt            =
| caption        =
| director       = Pradip Krishen
| producer       = Bobby Bedi (Kaleidoscope Entertainment)
| writer         =
| screenplay     = Arundhati Roy
| story          =
| narrator       =
| starring       = Roshan Seth Alice Spivak Tigmanshu Dhulia
| music          = Simeon Venkov
| cinematography = Giles Nuttgens
| editing        = Pradip Krishen
| studio         =
| distributor    =
| released       =   (UK)
| runtime        = 102.5 mins
| country        = United Kingdom/India 
| language       = English
| budget         =
| gross          =
}}
Electric Moon is a 1992 Indian film directed by Pradip Krishen and written by Arundhati Roy.  The film was produced by Grapevine Media for Channel 4 Television and Bobby Bedis Kaleidoscope Entertainment and was reviewed at the International Film Festival of India (IFFI) and the 36th London Film Festival (1992).   
 Best Feature Film in English.      
 satirical parody The Guru (1969), and in time the film acquired a cult following.    

In a 2005 interview Roy said,  "The movie I had in my head and  different from the one we shot. I wanted it to  have a more anarchic quality, but I didnt know enough about cinema  to make that come through on screen."   

==Cast==
* Roshan Seth - Ranveer
* Naseeruddin Shah - Rambuhj Goswami
* Leela Naidu - Sukanya Socks
* Gerson Da Cunha - Raja Ran Bikram Singh, Bubbles
* Raghuvir Yadav - Boltu 
* Alice Spivak - Louise Robinson
* Frances Helm - Emma Lane
* James Fleet - Simon Lidell
* Gareth Forwood - Ian
* Malcolm Jamieson - Thierry 

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 