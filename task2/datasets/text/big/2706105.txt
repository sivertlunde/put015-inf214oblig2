Wolf Creek (film)
 
 
{{Infobox film
| name           = Wolf Creek
| image          = Wolfcreek.png
| alt            = 
| caption        = Australian theatrical release poster
| director       = Greg McLean
| producer       = David Lightfoot Greg McLean
| writer         = Greg McLean Nathan Phillips Cassandra Magrath Kestie Morassi Frank Tétaz
| cinematography = Will Gibson
| editing        = Jason Ballantine
| studio         = FFC Australia/Film Finance Corporation South Australian Film Corporation 403 Productions True Crime Channel Best FX (Boom Sound) Emu Creek Pictures Mushroom Pictures
| distributor    = Roadshow Entertainment
| released       =  
| runtime        = 99 minutes   104 minutes  
| country        = Australia
| language       = English Swedish
| budget         =  1 million
| gross          = $27.7 million 
}} Ivan Milat Bradley Murdoch in 2001. 
 Ireland and the United Kingdom in September 2005.  In its home country of Australia, the film received a general release in November 2005, apart from the Northern Territory, out of respect for the trial surrounding the murder of Peter Falconio.   The film was purchased for distribution by Dimension Films in the United States, where it was released on Christmas day 2005.
 against women, with several stating they walked out of their screenings;  other critics praised the films grindhouse aesthetics and called its straightforward depiction of crime and violence "taboo-breaking".  Despite receiving mixed reviews, the film was nominated for seven Australian Film Institute awards, including Best Director (for McLean). In 2010, it was included in Slant Magazines list of the 100 best films of the decade. 

==Plot== backpacking across the country with Ben Mitchell, an Australian friend from Sydney. Currently in Broome, Western Australia, they constantly get drunk at wild, extravagant pool parties and camp out on the beach. Ben buys a dilapidated Ford XD Falcon for their road journey from Broome to Cairns, Queensland via the Great Northern Highway.
 Halls Creek Wolf Creek crater formed by a 50,000-ton meteorite. While exploring the crater, Ben and Liz kiss.

Hours later, upon returning to their car, the group discovers that their watches have all suddenly broken and that the car will not start. Unable to solve the problem, they prepare to sit out the night. After dark, a rural man named Mick Taylor comes across them and offers to tow them to his camp to repair the car. Initially hesitant, the group allows Mick to take them to his place, an abandoned mining site several hours south of Wolf Creek. Mick regales them with tall stories of his past while making a show of fixing the car. His manner unsettles Liz and Kristy, although Ben is less concerned. While they sit around a fire, Mick gives the tourists drugged water which eventually causes them to fall unconscious.

Liz awakens late the next afternoon to find herself tied up in a shed. She manages to break free as night falls, but before she can escape from the mining site, she hears Mick torturing Kristy in a garage, and witnesses him sexually assaulting her. Liz sets the now-dismantled Falcon on fire to distract him, and goes to help Kristy while Mick is busy trying to extinguish the blaze. When he returns Liz manages to shoot Mick with his own rifle, the bullet hitting him in the neck and apparently killing him. The women attempt to flee the camp in Micks truck, but a wounded Mick stumbles out of the garage and shoots at them with a double-barreled shotgun, before giving chase in another truck. The girls evade Mick by rolling his truck off a cliff and hiding behind a bush, before returning to the mining site to get another car. Liz leaves the hysterical Kristy outside the gates, telling her to escape on foot if she does not return in five minutes.

Liz enters another garage and discovers Micks large stock of cars as well as an organised array of travellers’ possessions, including video cameras. She watches the playback on one of them and is horrified to see Mick "rescuing" other travellers stranded at Wolf Creek in almost identical circumstances to her own. She then picks up another camera which turns out to be Bens, and while viewing some of Bens footage, she notices Micks truck in the background, indicating hes been following them long before they got to Wolf Creek. She gets into a car and attempts to start it, but Mick shows up in the back seat and stabs her through the drivers seat with a large knife. After more bragging, he hacks three of Lizs fingers off in one swipe, then picks her up and headbutts her into near unconsciousness. He then severs her spinal cord with a knife, paralysing her and rendering her a "head on a stick." Mick then proceeds to interrogate her as to Kristys whereabouts.
 Holden HQ Statesman, prompting Kristy to take off in the dead mans car. She succeeds in running Mick off the road, but he gets out of the car and shoots out Kristys back tire, causing the car to flip over. A disoriented, Kristy climbs out of the wreckage and attempts to crawl away, but is shot dead by Mick. He bundles Kristys body into the back of the wrecked car, along with the body of the dead motorist, and torches the car before driving off.

The action cuts to Ben, whose fate until now has not been revealed. He awakens to find himself nailed to a mock crucifix in a mine shaft, with an aggressive, caged Rottweiler in front of him. He manages to painfully extract himself from the crucifix and enters the camp in early daylight. Ben escapes into the outback, but becomes hysterical and dehydrated, eventually passing out beside a dirt road. He is discovered by two shocked Swedish travellers who take him to Kalbarri, Western Australia|Kalbarri, where he is airlifted to a hospital.

A series of title cards states that the bodies of Kristy and Liz were never found, despite several major police searches. Early investigations were disorganised and hampered by confusion, lack of physical evidence, and the alleged credibility of Ben. After four months in police custody, Ben was cleared of all suspicion. The film ends with the silhouette of Mick Taylor walking into the sunset with his rifle in hand.

==Cast==
* John Jarratt as Mick Taylor 
* Cassandra Magrath as Liz Hunter
* Kestie Morassi as Kristy Earl  Nathan Phillips as Ben Mitchell
* Guy ODonnell as Car Salesman
* Geoff Revell as Graham (petrol station attendant)
* Andy McPhee as Bazza (pervert in petrol station)
* Aaron Sterns as Bazzas mate
* Michael Moody as Bazzas older mate
* Gordon Poole as Old man
* Guy Petersen and Jenny Starvall as Swedish backpackers who help Ben
* Greg McLean (cameo) as Policeman

==Production== Wolfe Creek", and is located in northern Western Australia. It is the second largest meteorite crater in the world from which meteorite fragments have been recovered. Wolf Creek was filmed almost entirely in South Australia, but the aerial shots of the crater in the film show the genuine Wolfe Creek crater.
 Sundance there was also a full moon.
 Ivan Milat: he spent significant time alone in the isolated outback and went for weeks without showering. 

The sign on the front gate of Micks mining site reads "Navithalim Mining Co."; Navithalim spelt backwards reads: Milaht Ivan, evidently referencing Ivan Milat.

Since the film had a relatively low budget, many of the action scenes involved the real actors; for example, after running through the outback when her character escapes, star Kestie Morassi ended up with hundreds of thorns and nettles in her feet.  During the shooting of Morassis torture scene in the shed, her non-stop screams and crying began to discomfort and unsettle the crew; executive producer Matt Hearn said that the female members of the shooting crew were brought to tears by it, as if someone were actually being tortured. 
 composite shots).

==Basis in reality==
Wolf Creek was marketed as being "based on true events."

The abduction of British tourist Peter Falconio and the assault of his girlfriend Joanne Lees in July 2001 by Bradley John Murdoch in the Northern Territory are cited as influences.  Murdochs trial was still under way at the time of the films initial release in Australia, and for this reason the Northern Territory court placed an injunction on the films release there in the belief that it could influence the outcome of the proceedings. Many are misled into thinking that the entire movie is based on a true story, when it only had many influences from other murders around Australia, such as the Ivan Milat Backpacker Murders and the Peter Falconio murder case.

==Reception==

===Box office===
Wolf Creek opened on 151 cinemas in Australia on 3 November 2005 (the film had previously been shown at a number of film festivals) and took A$1.225&nbsp;million in its first weekend, making it the number one film for the weekend. In the United Kingdom, the film was given a modest release on 16 September 2005, and grossed £1,500,000. The film opened on Christmas Day 2005 in the United States and grossed $16,188,180 on American screens, while also garnering an extra $11,574,468 overseas, bringing the total gross to US$27,762,648. 

===Critical response===
On   based on 26 critics indicating "mixed or average reviews. 

Critic   film critic Moira Macdonald said that Wolf Creek was the first film she ever walked out on. She called watching the film "punishment" and wondered how someones real death inspired this "entertainment". The Independent praised its departure from the generic rules of the horror film genre.  The Guardian|Guardian film critic Peter Bradshaw awarded it 4/5 stars.  Time Out said "by making us feel the pain, Greg McLeans ferocious, taboo-breaking film tells us so much more about how and why we watch horror movies".    They admitted, however, that the film was not for everyone. The film magazines Empire (film magazine)|Empire and Total Film gave the film 4/5 stars, with Empire calling it "a grimy gut-chiller that unsettles as much as it thrills, violently shunting you to the edge of your seat before clamping onto your memory like a rusty mantrap".  Fangoria called it the scariest film of the year.
 The Shining." http://www.vulture.com/2013/10/25-best-horror-movies-since-the-shining.html#photo=2x00018 

===Awards and nominations===
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards|AACTA Award AACTA Award Best Direction Greg McLean
| 
|- AACTA Award Best Original Screenplay
| 
|- AACTA Award Best Editing Jason Ballantine
| 
|- AACTA Award Best Cinematography Will Gibson
| 
|- AACTA Award Best Actress in a Supporting Role Kestie Morassi
| 
|- AACTA Award Best Original Music Score Frank Tétaz
| 
|- AACTA Award Best Sound Des Kenneally
| 
|- Peter Smith
| 
|- Pete Best
| 
|- Tom Heuzenroeder
| 
|- Saturn Awards|Saturn Award Saturn Award Best Horror Film David Lightfoot
| 
|- Greg McLean
| 
|-
|}

==Alternate versions==
The original cut of Wolf Creek ran 104 minutes, approximately 5 minutes longer than the 99-minute cut that was released in cinemas. The extra footage in this cut included an additional scene at the beginning of the film after the party scene, in which Kristy awakens in bed next to Ben at a beach cottage the following morning; this created a romantic subplot between the characters, and was cut from the film for "complicating" matters unnecessarily.   

The other additional footage took place when Liz returns to the mining site after leaving Kristy behind; rather than immediately entering the car garage, as she does in the theatrical cut, she finds a revolver and fills it with cartridges, and then explores an abandoned mine shaft in order to search for Ben. She subsequently drops her pistol into the shaft, and climbs down inside to find dozens of decomposing bodies. This explains why, in the theatrical cut, the revolver disappears after she enters the car garage. According to director Greg McLean, this scene was cut from the film after test screenings because it was "simply too much", along with all of the other gruesome events that had taken place prior.  The scene in which Lizs spine is severed by Mick was also slightly longer, including more close-ups and shots.

When the film premiered in the United States on DVD, both an R-rated cut (which is identical to the theatrical release), and an unrated cut (which incorporates the aforementioned scenes) were released.

==Sequel==
 
After the success of the first film, McLean postponed plans to immediately work on a sequel in favor of directing Rogue (film)|Rogue.    Production was initially expected to commence in 2011 and John Jarratt was announced to reprise his role of Mick Taylor.  In August 2011 Geoffrey Edelsten was announced as a private investor for the movie and that he would be funding Australian dollar|A$5 million into the production of Wolf Creek 2 after reading the script. Later that same year, Edelsten withdrew his funding, alleging that he had been misled by McLean and Emu Creek Pictures into believing that he would not be the largest single private investor, a claim the production company denied.  Filming and production of Wolf Creek 2 was postponed until late 2012, when additional funding was made available through the South Australian Film Corporation. 

Filming took place in late 2012 and early 2013 in Australia,  and the movie had its world premiere on 30 August 2013 at the Venice Film Festival. The film was given a wide release in Australia on 20 February 2014. 

==Webseries==
Screentime and Emu Creek Pictures announced in February 2015 the Webseries adaption of the film franchise. John Jarratt is set to return in his role Mick Taylor, which will be written, directed and produced by Greg McLean.  The sixth part Miniseries will run on the Video on demand portal Stan. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 