Kanne Radha
{{Infobox film
| name = Kanne Radha
| image = KanneRadhafilm.jpg
| caption = LP Vinyl Records Cover
| director = Rama Narayanan
| writer = Karthik Radha Radha
| producer = T.R.Srinivasan
| music = Illayaraja
| editor =
| released = 1982
| runtime = Tamil
| budget =
}}
 1982 Tamil directed by Karthik and Radha in lead roles. 

==Cast==
 Karthik
*Radha Radha
*Raja Raja
*Vanitha Vanitha
*V. K. Ramasamy (actor)|V. K. Ramasamy
*Thengai Srinivasan
*Gundu Kalyanam
*Vadivukkarasi
*Vasantha

==Soundtrack==
{{Infobox album Name     = Kanne Radha Type     = film Cover    = caption  = LP Vinyl Records Cover Released =  Music    = Illayaraja Genre  Feature film soundtrack Length   = 22:25 Label    = ECHO
}}

The music composed by Ilaiyaraaja.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics 
|-
| 1 || Maalai Suda Kanne Radha... || S. P. Balasubrahmanyam, S. P. Sailaja || Vairamuthu 
|-
| 2 || Kulunga Kulunga Ilamai || Uma Ramanan || Alagudi Somu 
|-  Vaali 
|- Vaali 
|}

==References==
 

==External links==

 

 
 
 
 
 
 


 