Asew
{{Infobox film
| name =  Asew 
| image =
| image_size =
| caption =
| director = Phil Jutzi
| producer =  
| writer =  At. Timann 
| narrator =
| starring = Fritz Rasp   Olga Tschechowa   Hilde von Stolz   Wolfgang Liebeneiner
| music = Willy Schmidt-Gentner   
| cinematography = Eduard Hoesch   
| editing =  Else Baum   Vicki Baum     
| studio = Atlantis-Film   Maxim-Film 
| distributor = Tobis-Sascha Film
| released =  18 January 1935  
| runtime = 78 minutes
| country = Austria   Nazi Germany German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Asew or Double-Agent Asew or Asew the Agent Provocateur (German:Lockspitzel Asew) is a 1935 German-Austrian thriller film directed by  Phil Jutzi and starring Fritz Rasp, Olga Tschechowa and Hilde von Stolz.  The films sets were designed by Julius von Borsody.

The film portrays the activities of Yevno Azef a Russian who had worked as a agent provocateur for the Tsarist  Okhrana and infiltrated the Socialist Revolutionary Party.

==Cast==
*   Fritz Rasp as Lockspitzel Asew  
* Olga Tschechowa as Tanja Asew, seine Frau 
* Hilde von Stolz as Nelly, Chansonette  
* Wolfgang Liebeneiner as Wronski   Ellen Frank as Vera Wronksi, seine Schwester 
* Herbert Hübner as Lopuchin, Russischer Polizeigeneral  
* Siegfried Schürenberg as Sawinkoff  
* Franz Schafheitlin as Urzoff   Wilhelm König as Kaljajew  
* Traudl Stark as The Child
*Aruth Wartan  Otto Hartmann 
*Karl Forest 
*Gretl Wawra

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.
* Hull, David Stewart. Film in the Third Reich: A Study of the German Cinema, 1933-1945. University of California Press, 1969.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 