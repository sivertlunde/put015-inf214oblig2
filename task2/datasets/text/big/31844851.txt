Sweet and Low-Down
:For the 1999 Woody Allen film, see Sweet and Lowdown. For the Dave Van Ronk album, see Sweet & Lowdown.
{{Infobox film
| name           = Sweet and Low-Down
| image          = 
| image_size     = 
| caption        = 
| director       = Archie Mayo
| producer       = William LeBaron
| writer         = Richard English Edward Haldeman
| narrator       = 
| starring       = Benny Goodman Linda Darnell Jack Oakie
| music          = Cyril J. Mockridge
| cinematography = Lucien Ballard
| editing        = Dorothy Spencer
| studio         = 
| distributor    = 20th Century Fox
| released       = September 21, 1944
| runtime        = 76 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Sweet and Low-Down (1944 in film|1944) is a film directed by Archie Mayo, and starring Benny Goodman and Linda Darnell.  It was nominated for an Academy Award in 1945. 

The film was a fictionalized version of life with Goodman, his band, and their manager while entertaining at military camps.

==Plot summary==
 

==Cast==
* Benny Goodman as Himself
* Linda Darnell as Trudy Wilson
* Jack Oakie as Popsy
* Lynn Bari as Pat Stirling James Cardwell as Johnny Birch
* Allyn Joslyn as Lester Barnes
* John Campbell as Dixie Zang
* Roy Benson as Skeets McCormick Dickie Moore as Military Cadet General Mogie Cramichael

==Notes==
Lynn Bari seems to have been typecast by Fox as a big band singer, playing the role in Sun Valley Serenade (1941) and Archie Mayos Orchestra Wives (1942). Her voice had been dubbed in those films by Pat Friday and in this film, she was dubbed by Lorraine Elliot. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 


 