Garuda (film)
 
 
{{Infobox Film
| name           = Garuda
| image          = GarudaThaiposter.jpg
| caption        = Thai poster
| director       = Monthon Arayangkoon 
| producers      = Monthon Arayangkoon   Pawinee Wichayapongkul
| writer         = Monthon Arayangkoon
| starring       = Sornram Teppitak  Sara Legge   Dan Fraser   Chalad Na Songkhla   Yani Tramod   Phairote Sangwaribut 
| distributor    = R.S. Film   Tokyo Shock (U.S. DVD Release)
| released       = April 1, 2004
| runtime        = 115 minutes
| country        = Thailand
| language       = Thai, English
}}
 Thai Kaiju film. It is considered to be Thailands first Giant Monster/Kaiju movie by fans of the genre.

== Plot ==
 
The discovery of a mysterious fossil sets the stage for a terrifying confrontation between modern day man and Thai folklore. A tunnel excavation has revealed a rock so dense that it cannot be penetrated by even the strongest drill. When the workers discover a collection of unrecognizable fossils that bear no similarities to the familiar dinosaur types, they enlist the aid of archeologist Lenna Pierre and her American partner Tim in revealing the origins of the mysterious geological find. Awakened by the large scale excavation and enraged at having been trapped beneath the Bangkok concrete for hundreds of years, the ancient Garuda sets out on a bloody rampage as Leena, Tim, and the military struggle to find a means of bringing Garudas destructive reign to terror to an end. 

==Release==
 
Garuda was released in North America on Video on Demand in 2007, then later on DVD by Tokyo Shock.

==Reception==
 
*  

==Music==
Iconic segments of music from the popular anime Naruto can be heard in this movie. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 