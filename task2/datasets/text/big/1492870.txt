The Marrying Kind
{{Infobox film
| name           = The Marrying Kind
| image          = Poster of the movie The Marrying Kind.jpg
| director       = George Cukor
| writer         = Garson Kanin Ruth Gordon
| starring       = Judy Holliday Aldo Ray Madge Kennedy Joseph Walker
| music          = Hugo Friedhofer
| released       =  
| distributor    = Columbia Pictures
| runtime        = 92 min. English
| gross          = $1.7 million (US rentals) 
}}
 John Alexander, Barry Curtis, Tom Farrell, Gordon Jones, Madge Kennedy, Nancy Kulp, Mickey Shaughnessy, and Joan Shawlee.

==Synopsis==
The ups and downs of marriage and commitment are realized as a disgruntled and grieving couple recount their marriage to the divorce judge.  As the judge attempts to decipher whether or not their love for one another is gone, key moments of their lives together are remembered in this witty comedy/drama.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 