U nás v Kocourkově
{{Infobox film
| name           = U nás v Kocourkově
| image          = U nás v Kocourkove.jpg
| alt            = 
| caption        = 
| film name      = 
| director       =Miroslav Cikán
| producer       = 
| writer         = 	Miroslav Cikán, Jaroslav Mottl and Karel Polácek 
| screenplay     = Václav Wasserman 
| story          = 
| based on       =  
| starring       =   Jan Werich, Jindrich Plachta, and Václav Trégl
| narrator       =  Jaroslav Ježek, Julius Kalas 
| cinematography = Jaroslav Blažek 	
| editing        = Antonín Zelenka 	
| studio         =  Lloydfilm
| distributor    = 
| released       =1934  
| runtime        =87 minutes
| country        =Czechoslovakia
| language       = 
| budget         = 
| gross          = 
}}

U nás v Kocourkově is a 1934 Czechoslovak drama film, directed by Miroslav Cikán.  It stars   Jan Werich, Jindrich Plachta, and Václav Trégl. It was one of several films the director made with Werich,    and features him as a convict. The film is largely set in a prison.   

==Cast==
*Jan Werich as Ferdinand Kaplan - Convict no. 1313
*Jindrich Plachta as Jalovec - poacher
*Václav Trégl as Ludvík Espandr - Barber
*Zdenka Baldová as Nykysová - widow
*Ladislav Pesek as Dr. Nykys
*Jaroslav Vojta as Mayor Adam
*Hermína Vojtová as the Mayors wife
*Marie Tauberová as Blazenka
*Svetla Svozilová as Lily - circus performer
*Jaroslav Marvan as Director of prison
*Stanislav Neumann as Prisoner in solitary confinement
*Jaroslav Prucha as Detective
*Jan Richter as Detective 	

==References==
 

==External links==
*  at the Internet Movie Database

 
 
 
 
 
 
 
 
 