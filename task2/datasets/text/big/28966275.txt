Take a Giant Step
 
{{Infobox Film
| name           = Take a Giant Step
| image          = 
| image_size     = 
| caption        = 
| director       = Philip Leacock
| writer         = Julius J. Epstein Louis S. Peterson (play)
| narrator       = 
| starring       = Johnny Nash Estelle Hemsley Ruby Dee Frederick ONeal Beah Richards Ellen Holly
| music          = Jack Marshall	
| cinematography = Arthur E. Arling
| editing        = Frank Gross
| studio         = Hill-Hecht-Lancaster Productions (as Shela Productions Inc.)
| distributor    = United Artists
| released       = 1959
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = $300,000
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Take a Giant Step (1959 in film|1959) is a coming-of-age drama film, directed by Philip Leacock about a black teenager living in a predominantly white environment and having trouble coping as he reaches an age at which the realities of racism are beginning to affect his life more directly and pointedly than they had in his childhood. Adapted from the Broadway play by Louis S. Peterson, the film stars Johnny Nash, who would ultimately become more well known for his singing career, including the hit song "I Can See Clearly Now", as the lead character, Spencer "Spence" Scott.  Co-stars included Ruby Dee as the Scott familys housekeeper, Estelle Hemsley as Grandma Martin (Hemsley was nominated for a Golden Globe Award for best supporting actress), and Beah Richards as Spences mother.  The movies executive producer was Burt Lancaster through his Hecht-Hill-Lancaster production company. 

==Plot==
Spencer "Spence" Scott (Johnny Nash) is a 17-year old black high school senior who has lived his entire life in a middle-class white neighborhood of an unnamed city in the northern United States. Having been raised with a sense of self-respect, he is starting to become frustrated by the effects of racism. When his history teacher speaks ill of the intellect of black slaves during the American Civil War, he objects, and when the teacher dismisses the objection, he storms angrily out of the classroom and slips to into the bathroom to calm down by smoking a cigar.  He is discovered there and is suspended from school. At the same time, his white friends are beginning to exclude him from their activities because they want to include girls, and none of the girls parents approve of their daughters socializing in circles that include a black boy.

Spence confides in "Gram" (Estelle Hemsley), his ailing but wise grandmother, but he cannot face the prospect of telling his parents (Frederick ONeal, Beah Richards) what has happened, so he decides to leave home, catching a bus into a black neighborhood. His time on his own is short-lived, however, as he is socially unprepared for an adult world. Although he is intelligent and well-read, he finds that his academic knowledge doesnt carry him far, as he approaches an attractive older woman in a bar, presenting to her a very logical case as to why hed be a good boyfriend, and assuring her that hed be willing to marry if they were to fall in love. He is disillusioned to discover that the woman is unhappily married and wants only to find a man in the bar who has some money and a nice car, so that she can have a one-night fling with him to temporarily escape from her troubles.

Upon returning home, Spences parents berate him for not having stayed "in his place" when dealing with white people, as they have learned to do. Spence tells them that he ashamed of them for that attitude and Gram comes to Spences defense, chiding the couple for not supporting him in his stance against his teachers racism. She notes that they moved to the middle-class neighborhood to help instill in Spence a sense of self-respect that he might not have attained if hed grown up in a slum, but they are now angry at him for displaying that very self-respect.  She also criticizes their emphasis on providing him material comforts, as opposed to spending time with him, as evidenced by the fact that they are completely unaware of his having recently become something of an outcast among his peers.

After Gram dies, Spence turns to Christine (Ruby Dee), the Scotts housekeeper.  He confesses to her both his sexual frustration and his overall loneliness.  He proposes that since Christine is lonely also &mdash; she is a widow, her only child was stillbirth|stillborn, and she is probably over a thousand miles removed from any of her family &mdash; they might find some happiness together for a short time, even if their age difference precludes a long-term relationship.  To her own surprise, Christine finds that she is actually considering the idea, but the decision is made for her when she is terminated from the household.  Spences mother reasons that with the family no longer needing anyone to look after Gram, a housekeeper is unnecessary.  She is also perceptive enough to be wary of a possible romantic relationship developing between Spence and Christine.

Frustrated by his mothers decision and the fact that she has asked his friends to come over and spend time with him, Spence grows very angry at her over the idea of his having to beg people to be friends with him.  His mother grows just as angry in return, insisting he will learn that having to swallow such humiliations is just part of being black in white-dominated world.  Unable to discern any course of action that will alleviate his current unhappiness, Spence ultimately decides simply to persevere and focus upon building a brighter future for himself.  He politely tells his friends that he wont have time to see them anymore, later explaining to his mother that he "said goodbye to them before they say it to me" because he had accepted that they were only his friends "up to a point". His mother still doesnt believe that was the right course of action, but says that she only wants him to be happy and the film ends with the two affirming their love for one another.

==Production==
Prior to his selection as the lead in Take a Giant Step, Johnny Nash had made a name for himself as a singer, appearing on The Arthur Godfrey Show.  Nash said that he was told that Burt Lancaster saw him on television, thought he would be right for the part and had him brought in to audition.  Nash acknowledged that the story sounded like it might be "a press agents dream", but said that he liked the story so much, he preferred to just accept it, rather than ever ask Lancaster whether it was true.  Frederick ONeal, Beah Richards and Estelle Hemsley had all performed in the stage version of Take a Giant Step, prior to being cast in the film.

Once the film wrapped, United Artists had difficulty distributing it due to objections over its content.  The films full release was delayed into the middle of 1960, as it was re-edited and censored.   Once released, the film was negatively reviewed by The New York Times as an "unworthy film" that handled the lead characters problems "in a clumsy, shoddy fashion".  The review credited Nash with an "earnest" performance, but said that he lacked "poignancy".  Supporting actress Ruby Dee was praised as "sensible and sympathetic". But aside from these points, the review was sharply critical of the script, directing and acting performances.   Hemsley, however, received a Golden Globe nomination for her performance.

== References ==
 

== External links ==
*  

 
 
 
 
 
 