True Colors (1991 film)
{{Infobox film
| name           = True Colors
| image          = TrueColors1991Poster.jpg
| image size     = 
| alt            = 
| caption        = Film Poster
| director       = Herbert Ross
| producer       = Laurence Mark Herbert Ross
| writer         = Kevin Wade
| narrator       = 
| starring       = James Spader John Cusack Richard Widmark Mandy Patinkin Paul Guilfoyle Trevor Jones
| cinematography = Dante Spinotti
| editing        = Robert M. Reitano Stephen A. Rotter
| studio         = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 111 minutes
| country        = 
| language       = English
| budget         = 
| gross          = $418,807
| preceded by    = 
| followed by    = 
}}

True Colors (1991 in film|1991) is an American drama film written by Kevin Wade and directed by Herbert Ross. The cast included John Cusack, James Spader and Richard Widmark in his final movie role.

==Plot summary== Department of Justice, a fact that doesnt sit well with his girlfriend, Diana (Imogen Stubbs), the daughter of Senator Stiles (Widmark). The couple end their relationship before Tim has a chance to propose. Peter, who is embarrassed by his lower-class roots, plans a career in politics, eventually manipulating his way into a job on a congressmans campaign staff. 

The friendship between them is severely tested when Peter confesses to Tim during a ski vacation that he and Diana have been carrying on an affair. He adds that he plans to ask Diana to marry him. Tim, angry at the betrayal, speeds off down a hazardous ski trail. The less experienced Peter follows him down. Theres a steep cliff at the end. Peter doesnt hear Tims belated warnings in time, hurtles down to the bottom breaking his leg and cracking two ribs. Tim forgives Peter and even agrees to be the best man at Peter and Dianas wedding.

Shortly after marrying Diana, Peter falls under the influence of John Palmieri (Mandy Patinkin), who has ties to organized crime and political corruption. He bribes Peter into going along with an influence peddling scheme. Meanwhile, things have begun to spiral out of control in Peters extended family. Earlier in the film, Stiles is diagnosed with early onset Alzheimers disease, something that will end his political career. Peter seizes the opportunity to manipulate his father-in-law into supporting his run for Congress. When the older man refuses, Peter threatens to leak news of the illness to the press. The senator reluctantly acquiesces, but promises Peter that he will eventually get caught.

Tim, who by this time has risen through the ranks of the Department of Justice, launches an investigation into political corruption that will eventually lead back to Palmieri. When Palmieri discovers Peters ties to Tim, he bullies Peter into setting Tim up by putting him in touch with a bogus tipster. Tim runs with the false info, creating  public embarrassment for the DOJ and compromising the investigation. Tim is suspended, and Peter hires him to work for him on his upcoming campaign.

Stiles reveals the conversation that he had with Peter to Diana and she decides to divorce Peter. Shortly thereafter, Tim discovers Peters role in getting him suspended from his job. Tim volunteers to help the DOJ work to bring Peter down.
 arraigned at that very moment. Peter viciously attacks Tim. When the fighting subsides and the two calm down, Tim tells Peter he has brought his ruin upon himself.
 immunity in exchange for testimony against Palmieri. Tim then tells Peter that he will be forced to testify against him if he does not accept the plea deal. The two have a reconciliation of sorts and go their separate ways.

==Cast==
* John Cusack as Peter Burton
* James Spader as Tim Gerrity
* Imogen Stubbs as Diana Stiles
* Richard Widmark as Sen. James Stiles
* Mandy Patinkin as John Palmeri
* Dina Merrill as Joan Stiles
* Paul Guilfoyle as John Laury
* Philip Bosco as Sen. Frank Steubens
* Brad Sullivan as FBI Agent Abernathy

==Filming==
Much of the filming occurred in Charlottesville, Virginia, and the nightclub scene was at TRAX, the storied venue once run by Coran Capshaw, who would launch and manage the Dave Matthews Band.

==External links==
* 

 

 
 
 
 
 
 
 