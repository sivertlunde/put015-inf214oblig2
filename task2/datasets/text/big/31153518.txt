Niram Marunna Nimishangal
{{Infobox film 
| name           = Niram Marunna Nimishangal
| image          =
| caption        = Mohan
| producer       =
| writer         = Perumpadavam Sreedharan
| screenplay     = Perumpadavam Sreedharan
| starring       = Jayabharathi Sukumaran Shyam
| cinematography = Kannan
| editing        = G Venkittaraman
| studio         = Navadarsana Films
| distributor    = Navadarsana Films
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by Mohan (director)|Mohan. The film stars Jayabharathi and Sukumaran in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
*Jayabharathi
*Sukumaran

==Soundtrack== Shyam and lyrics was written by Bichu Thirumala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Nanma niranjori || S Janaki, Chorus || Bichu Thirumala || 
|-
| 2 || Omanakal || S Janaki || Bichu Thirumala || 
|-
| 3 || Sooryodayam veendum varum || K. J. Yesudas, Chorus || Bichu Thirumala || 
|}

==References==
 

==External links==
*  

 
 
 

 