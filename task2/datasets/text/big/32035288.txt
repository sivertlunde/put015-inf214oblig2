Adventures of William Tell
{{Infobox film
| name           = Adventures of William Tell
| image          = AdventuresofWilliamTell.jpg
| image_size     =
| caption        = Screenshot from the film
| director       = Georges Méliès
| producer       =
| writer         =
| narrator       =
| starring       =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = France Silent
| budget         =
| gross          =
}}
 
 1898 France|French short black-and-white silent trick film, directed by Georges Méliès, featuring a clown trying to shoot fruit off the head of a dummy which comes to life. The film is, "a knockabout farce based on jump-cuts and the timely substitution of dummies for real bodies," with, according to Michael Brooke of BFI Screenonline, "a level of onscreen violence not previously seen in a surviving Méliès film," which marks, "a bridge between the onstage effects of the famous Théâtre du Grand Guignol and countless later outpourings of comically extreme screen violence as seen in everything from Tex Avery cartoons to the early films of Sam Raimi." 

It was released by Mélièss Star Film Company and is numbered 159 in its catalogues, where it was advertised as a scène comico-fantastique. 

==Synopsis==
A clown constructs a mannequin of William Tell and places a piece of fruit on its head but the mannequin comes to life and hurls the fruit at the clown before he can take aim with his crossbow. The clown check removes and replaces the mannequin’s arm and head, at which point it comes to life again, tramples the clown and makes its escape, with the now irate clown in pursuit.

==Current status==
Given its age, this short film is available to freely download from the Internet.

==References==
 

== External links ==
*  
*   on YouTube

 

 
 
 
 
 

 