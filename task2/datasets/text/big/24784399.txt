Hadersfild
Hadersfild Serbian film from 2007, directed by Ivan Živković, and the script was written by Uglješa Šajtinac and Dejan Nikolaj Kraljačić.

==Plot summary==
The story is set in a small town in Serbia. Rasha (Goran Šušljik) is thirty-two, lives with his alcoholic father (Josif Tatić) and tries, whilst failing, to make ends meet by giving literature lessons to teenage girls and hosting a program on the local radio, presenting new books and interviewing authors. 

Ivan (Nebojša Glogovac), a promising judoist in his teenage years, has since had a history of neurosis, psychotic episodes, hospitalization, being heavily medicated, involvement in various occult groups and practices, prior to having been baptized in the Orthodox Church. 

Milla (Suzana Lukić) is very attractive, energetic, resourceful and very straightforward: it immediately becomes clear that apart from having a student/teacher relationship, she and Rasha are lovers. 

Dule (Vojin Ćetković) is a wannabe yuppie: he works for the local representative of major confectionery brands and is doing his best to act the part of a successful businessman who is well aware of global business trends as he sees them. 

The monotony of their lives is interrupted by the arrival of Igor (Damjan Kecojević), who has lived in Huddersfield since the beginning of the nineties, and this is the first time hes come to visit since. 

They all get together in the evening, and what begins as a cheerful high school reunion party of close friends with a lot of catching up to do, turns into an emotional roller coaster of reminiscing, dark humor, bitterness, uncontrollable laughter, anger and grim soul searching.

==Media coverage==
The film has proven to be a hit within Serbia and has played at the International Thessaloniki Film Festival in Greece, receiving rave reviews from critics. 

==References==
 

==External links==
* 

 
 
 