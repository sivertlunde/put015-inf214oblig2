Sare Chuattar Ghosh Para
{{Infobox film
| name           = Sare Chuattar Ghosh Para
| image          = Sare_Chuattar_Ghosh_Para.jpg
| alt            = 
| caption        = 
| film name      =  
| director       =  Soumya Supriyo
| producer       =  Ajit Sureka
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = Ashok Bhadra
| cinematography = 
| editing        = 
| studio         =  
| distributor    =  
| released       =  
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          =  
}}
Sare Chuattar Ghosh Para (2014) is an Indian Bengali film directed by Soumya Supriyo and produced by Ajit Sureka. The music of the film was composed by Ashok Bhadra.         

== Plot ==
The story of the film revolves around Sagnik, Anindya and Kaushi, who live together and dream big. Anindya wants to become a hero, Kaushik is a boxer and is desirous to marry his boxer coach, and Sagnik wants to become a playback singer. However none of them can pursue their goals for lack of money and other reasons. On day they see an advertisement promising   and jump for the opportunity.

== Cast ==
* Arnab Banerjee
* Vivek Trivedi
* Pamela
* Partho Sarathi Chakraborty

== Reception ==
The film received mostly negative reviews. in The Times of India critic rating, the film got half out of five stars and wrote this is probably the most badly made buddy-comedy flick ever made in the history of Indian Cinema. 

== References ==
 

 
 