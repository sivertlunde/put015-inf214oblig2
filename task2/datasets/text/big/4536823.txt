The Mindscape of Alan Moore
{{Infobox film
 | name = The Mindscape of Alan Moore
 | caption = 
 | director = DeZ Vylenz
 | producer = 
 | writer = 
 | starring = Alan Moore
 | distributor = Shadowsnake Films
 | released =  
 | runtime = 
 | language = English
 | music = Drew Richards RZA
 | budget = 
 | image = The Mindscape of Alan Moore VideoCover.jpeg
}}
The Mindscape of Alan Moore is a 2003 documentary film which chronicles the life and work of  , September 22, 2008 

The Mindscape of Alan Moore is Shadowsnake’s first completed feature project, part One of the Shamanautical / 5 Elements series. It is the directorial debut of DeZ Vylenz. It is the only feature film production on which Alan Moore has collaborated, with permission to use his work.

This feature was shot on film, in colour, and is 78 minutes in length.

==Synopsis==
Alan Moore presents the story of his development as an artist, starting with his childhood and working through to his comics career and impact on that medium, and his emerging interest in Magician (paranormal)|magic.  

The film features the first film adaptations of scenes from Moores acclaimed series V for Vendetta and Watchmen, shot in early 2002.  Another key scene features a direct reference to the character of John Constantine from the comic book Hellblazer.

==Release==
The film was showcased at the San Francisco World Film Festival and Marketplace 2003 (23–25 October 2003),  where it received a Special Recognition Award for creative achievement in documentary filmmaking. 

==DVD== David Lloyd, Melinda Gebbie, as well as Paul Gravett.  

==Music== Alan Douglas, Lustmord and Spectre (musician)|Spectre.

==References==
 

==External links==
* 
* 
* Film review: " "

 

 
 
 
 
 
 

 