Collateral (film)
{{Infobox film
| name           = Collateral
| image          = Collateral (Movie).jpg
| caption        = Theatrical poster Michael Mann
| producer       = Michael Mann Julie Richardson
| writer         = Stuart Beattie
| starring       = {{Plainlist|
*Tom Cruise
*Jamie Foxx
*Jada Pinkett Smith
*Mark Ruffalo
*Peter Berg
*Bruce McGill 
}}
| music          = James Newton Howard Paul Cameron Jim Miller Paul Rubell
| distributor    = DreamWorks Pictures   Paramount Pictures  
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = $65 million
| gross          = $217.8 million 
}} Michael Mann type as Los Angeles, California in January 2004, and the supporting cast includes Jada Pinkett Smith and Mark Ruffalo. Foxx and Cruises performances were widely praised, with Foxx being nominated for the Academy Award for Best Supporting Actor. Adam Sandler and Russell Crowe were originally considered for the parts that went to Foxx and Cruise.

==Plot== cab driver named Max Durocher (Jamie Foxx) is working to earn enough to start his own limousine business. One of this evenings fares is United States Department of Justice|U.S. Justice Department prosecutor Annie Farrell (Jada Pinkett Smith). On the drive to her office, they strike up a conversation as a result of a bet on what route will get her downtown the fastest. She takes a liking to Max and leaves him with her business card to Maxs surprise and delight. 

Maxs next fare is a businessman named Vincent (Tom Cruise) who exits the building just after Annie entered. Vincent is impressed by Maxs skill at navigating the streets of LA and offers Max $600 to drive him for the entire night, against regulations. Max reluctantly agrees, as the money is too much to pass up. As Max waits at the first stop, a man falls onto the cab roof from above. Vincent has killed his first of five marks for the night, drug dealer Ramón Ayala, and is forced to reveal himself as a Contract killing|hitman. He coerces a terrified Max into hiding the body in the trunk, clean up the car and continue with their arrangement.

Vincent, concerned about Max leaving, leaves him tied to the steering wheel in an alley, as he goes to kill a defense attorney named Sylvester Clarke. Max sounds his horn and flashes his lights, attracting the attention of a group of young men whom he asks for help, but rather than trying to help him, one of the thugs takes Maxs wallet and Vincents briefcase but Vincent returns and kills him and his friend, much to Maxs horror. Vincent tells Max they have time to kill and offers to buy him a drink at a jazz club he likes. The club owner Daniel Baker (Barry Shabaka Henley) turns out to be Vincents third mark and after the club closes for the night Vincent kills him in front of Max who had thought they were there just to share a drink and a conversation about jazz music. Max tells Vincent that he wants out, but Vincent assaults him, threatening to kill him if he refuses to obey.

Their next stop is the hospital because Max always visits his ill mother Ida (Irma P. Hall) there every night and Vincent insists he doesnt break the routine. Vincent pretends to be Maxs colleague and quickly bonds with Ida, who takes a liking to him. Betrayed and upset, Max decides to make a run for it grabbing Vincents briefcase on the way out. A foot chase ensues and Max ends up tossing the briefcase onto the freeway from a pedestrian bridge, hoping to end this nightmare once and for all. With his target list destroyed, Vincent forces Max to meet drug lord Felix Reyes-Torrena (Javier Bardem) to regain the lost information on his last two marks, threatening to murder Maxs mother otherwise. Posing as Vincent, Max meets with Felix and successfully acquires the information on a USB flash drive. Using the cabs computer, Vincent can see the details of the next target, Korean gangster Peter Lim, who is at a nightclub.
 LAPD detective FBI special agent Frank Pedrosa (Bruce McGill), who identifies the targets as witnesses in a federal grand jury indicting Felix tomorrow. In retaliation, Felix has hired Vincent to kill all five key witnesses. Pedrosa assembles a force to secure witness Lim and converges on the crowded nightclub simultaneously with Vincent, who in turn is being followed by Felixs men. Vincent manages to execute all of Lims guards, Felixs hitmen and Lim himself, before exiting the club amidst the chaos. Fanning rescues Max and smuggles him outside, but is shot and killed by Vincent, who beckons Max back into the cab.

Following their getaway, the two start trading personal insults and criticisms. Vincent mocks Max for his lack of ambition, while Max attempts to shame Vincent for caring nothing for human life. This escalates into an argument that prompts Max into deliberately crashing the cab, hoping to kill them both. However, both survive and Vincent escapes. A policeman arrives at the wreck to help, ultimately noticing the corpse in the trunk (drug dealer Ramón Ayala), and starts to arrest Max, who at first surrenders, relieved that all is over, but then spots Annies profile on the cab computer and realizes she is Vincents final target. He overpowers the policeman and takes Vincents gun before running off to try to warn Annie that Vincent is on his way to kill her.
 metro rail train, but they end up cornered. Boxed in and left with no other option, Max makes his last stand. As they fire blindly at each other, Max mortally wounds Vincent in the dramatic shootout. Vincent loses all energy and cannot retaliate. He slumps into a seat and slowly dies as he repeats an anecdote heard earlier in the film about a man who died on a train and went unnoticed for six hours. Safe at last, Max and Annie get off at the next station, in the dawn of a new day.

== Cast ==
 
* Tom Cruise as Vincent, a professional hitman hired by middlemen to kill four witnesses and a prosecutor.
* Jamie Foxx as Max Durocher, a taxi driver whom Vincent employs to drive him to the locations of the hits.
* Jada Pinkett Smith as Annie Farrell, the lawyer prosecuting Felix Reyes-Torrena.
* Mark Ruffalo as Ray Fanning, an LAPD detective on the tail of Vincent and Max.
* Peter Berg as Richard Weidner, Fannings partner.
* Bruce McGill as Frank Pedrosa, an FBI agent staking out Felix Reyes-Torrenas club.
* Irma P. Hall as Ida Durocher, Maxs mother.
* Barry Shabaka Henley as Daniel Baker, a jazz club owner and one of the witnesses.
* Richard T. Jones as traffic cop #1
* Klea Scott as Zee, one of Pedrosas team members.
* Bodhi Elfman as young professional man
* Debi Mazar as young professional woman
* Javier Bardem as Felix Reyes-Torrena, a Mexican cartel drug lord who hires Vincent 
* Emilio Rivera as Paco, one of Felixs bodyguards and hitmen.
* Jamie McBride as traffic cop #2
* Thomas Rosales, Jr. as Ramon Ayala, a low-level player in the exotic substances business and one of the witnesses.
* Inmo Yuon as Peter Lim, the owner of the club Fever and one of the witnesses. Frank Martin, Stathams character from The Transporter (franchise)|The Transporter series.
* Angelo Tiffe as Sylvester Clarke, a former criminal attorney who represented Ramone and one of the witnesses.
 

== Production ==
  Sydney airport, and had the idea of a homicidal maniac sitting in the back of a cab with the driver nonchalantly conversing with him, trusting his passenger implicitly. Beattie drafted his idea into a two-page treatment entitled "The Last Domino," then later began writing the screenplay. The original story centered around an African-American female cop who witnesses a hit, and the romance between the cab driver and his then librarian girlfriend. The film has limited resemblance to the original treatment.

Beattie was waiting tables when he ran into friend Julie Richardson, whom he had met on a UCLA Screenwriting Extension course. Richardson had become a producer, and was searching for projects for Edge City, Frank Darabont, Rob Fried and Chuck Russells company created to make low budget genre movies for HBO. Beattie later pitched her his idea of "The Last Domino." Richardson pitched the idea to Frank Darabont, who brought the team in for a meeting, including Beattie, and set up the project under Edge City. After two drafts, HBO passed on the project. At a general meeting at DreamWorks, with executive Marc Haimes, Beattie mentioned the script. Marc Haimes immediately contacted Richardson, read the script overnight, and DreamWorks put in an offer the following day.

Collateral sat on DreamWorks development books for three years. Mimi Leder was initially attached to direct, it then passed on to Janusz Kamiński. It wasnt until Russell Crowe became interested in playing Vincent that the project started generating any heat. Crowe brought Michael Mann on board, but the constant delays meant that Crowe left the project. Mann immediately went to Tom Cruise with the idea of him playing the hitman and Adam Sandler as the cab driver. Sandler later dropped out (due to his work on comedy) and was replaced by Jamie Foxx.
 Travis Bickle). However, the studio refused, insisting they wanted a younger actor in the role.
 Miami Vice. 35 mm.

Early drafts of Collateral s script set the film in New York City. However, later revisions of the script moved the films setting to Los Angeles.

===Music===

The Collateral soundtrack was released on August 3, 2004, by Hip-O Records. 

====Track listing====
{{Tracklist
| headline        = Collateral: Original Motion Picture Soundtrack
| total_length    = 51:53
| writing_credits = yes
| title1          = Briefcase
| note1           =
| writer1         = Tom Rothrock
| length1         = 2:07
| title2          = The Seed (2.0)
| note2           = Extended Radio Edit
| writer2         = The Roots, Cody Chesnutt
| length2         = 4:13
| title3          = Hands of Time
| note3           =
| writer3         = Groove Armada
| length3         = 4:19
| title4          = Guero Canelo
| note4           = Calexico
| length4         = 3:00
| title5          = Rollin Crumblin
| note5           =
| writer5         = Tom Rothrock
| length5         = 2:21
| title6          = Max Steals Briefcase
| note6           =
| writer6         = James Newton Howard
| length6         = 1:48
| title7          = Destino De Abril
| note7           =
| writer7         = Green Car Motel
| length7         = 5:15
| title8          = Shadow on the Sun
| note8           =
| writer8         = Audioslave
| length8         = 5:43
| title9          = Island Limos
| note9           =
| writer9         = James Newton Howard
| length9         = 1:33
| title10         = Spanish Key
| note10          =
| writer10        = Miles Davis
| length10        = 2:25
| title11         = Air on the G String
| note11          =
| writer11        = Johann Sebastian Bach
| length11        = 5:46 Ready Steady Go (Korean style)
| note12          =
| writer12        = Paul Oakenfold
| length12        = 4:48
| title13         = Car Crash
| note13          = Antonio Pinto
| length13        = 2:19
| title14         = Vincent Hops Train
| note14          =
| writer14        = James Newton Howard
| length14        = 2:02
| title15         = Finale
| note15          =
| writer15        = James Newton Howard
| length15        = 2:18
| title16         = Requiem
| note16          = Antonio Pinto
| length16        = 1:56
}}

The soundtrack also features the song "Iguazú" written by Gustavo Santaolalla.

== Reception ==

=== Critical response ===
The film received positive reviews, with particular praise going to Tom Cruise and Jamie Foxxs performances. On the review aggregator Rotten Tomatoes, 86% of critics gave the film positive reviews, based on 226 reviews. The critical consensus states that "Driven by director Michael Manns trademark visuals and a lean, villainous performance from Tom Cruise, Collateral is a stylish and compelling noir thriller."   On Metacritic, the film had an average score of 71 out of 100, based on 41 reviews. Tom Cruise went on to garner critical acclaim, while Foxx received several award nominations.  Richard Roeper placed Collateral as his 10th favorite film of 2004. The film was voted as the 9th best film set in Los Angeles in the last 25 years by a group of Los Angeles Times writers and editors with two criteria: "The movie had to communicate some inherent truth about the L.A. experience, and only one film per director was allowed on the list". 

=== Box office ===
The film opened on August 6, 2004, in 3,188 theaters in the United States and Canada and grossed approximately $24.7 million on its opening weekend, ranking #1 at the box office.  It remained in theaters for 14 weeks and eventually grossed $101,005,703 in the U.S. and Canada. In other countries it grossed a total of $116,758,588 for a total worldwide gross of $217,764,291. 

== Awards ==
{| class="wikitable" style="font-size:90%"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | Awards
|- style="text-align:center;"
! style="background:#ccc;"| Award
! style="background:#ccc;"| Category
! style="background:#ccc;"| Recipient(s)
! style="background:#ccc;"| Outcome
|- Academy Awards
| Academy Award for Best Supporting Actor
| Jamie Foxx
|  
|-
| Academy Award for Best Film Editing Jim Miller and Paul Rubell
|  
|- ASCAP Film and Television Music Awards
| Top Box Office Film Antonio Pinto
|  
|-
| American Society of Cinematographers
| Outstanding Achievement in Cinematography in Theatrical Releases
| Dion Beebe and Paul Cameron
|  
|-
| Art Directors Guild
| Feature Film – Contemporary Film
| David Wasco, Daniel T. Dorrance, Aran Mann, Gerald Sullivan and Christopher Tandon
|  
|-
| rowspan="6"| BAFTA Award Best Cinematography
| Dion Beebe and Paul Cameron
|  
|- Best Actor in a Supporting Role
| Jamie Foxx
|  
|-
| David Lean Award for Direction Michael Mann
|  
|- Best Editing
| Jim Miller and Paul Rubell
|  
|- Best Original Screenplay
| Stuart Beattie
|  
|- Best Sound
| Elliott Koretz, Lee Orloff, Michael Minkler and Myron Nettinga
|  
|-
| rowspan="2"| Broadcast Film Critics Association Best Supporting Actor
| Jamie Foxx
|  
|- Best Film
| -
|  
|-
| rowspan="2"| Black Reel Awards Best Supporting Actor
| Jamie Foxx
|  
|- Best Supporting Actress
| Jada Pinkett Smith
|  
|-
| Golden Globe Awards Best Supporting Actor
| Jamie Foxx
|  
|-
| MTV Movie Awards Best Villain
| Tom Cruise
|  
|-
| rowspan="4"| Saturn Award Best Actor
| Tom Cruise
|  
|- Best Director
| Michael Mann
|  
|- Best Action or Adventure Film
| -
|  
|- Best Writing
| Stuart Beattie
|  
|-
|}

==Notes==
 

==References==
{{reflist|2|refs=
   
   
   
   
    
   
}}

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 