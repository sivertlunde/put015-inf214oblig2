Alolam
{{Infobox film
| name           = Alolam
| image          = 
| caption        = Mohan
| producer       =
| writer         =  K R Vijaya  Nedumudi Venu Thodupuzha Vasanthi
| music          = Ilaiyaraaja
| cinematography = 
| editing        =
| distributor    = 
| released       =  
| runtime        = 122 mins
| country        = India
| language       = Malayalam
| budget         =
| gross          = 
}}
Alolam is a 1982 Malayalam film. It was directed by Mohan. This classic hit movie reveals a good social message.

== Plot ==
A childless couple comes to a village. The husband who is a veterinary doctor (Bharath Gopi) is befriended by a man named Thampuran (Nedumudi Venu) who is a womanizer. He takes advantage of the monotony and bleakness of the doctors life to tempt him into an act of infidelity, endangering the doctors precarious family life.  The final 20 minutes of the movie is a riveting drama, intensely capturing the tragedy that has befallen the couple. The movie can also be seen as commentary on decaying family and village values.
 

==References==
 

 
 
 
 
 

 