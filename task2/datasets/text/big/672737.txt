Kitchen Stories
 
{{Infobox film
| name           = Salmer fra kjøkkenet Kitchen Stories
| image          = Kitchen stories poster.jpg
| caption        =
| director       = Bent Hamer
| producer       = Jörgen Bergmark Bent Hamer
| writer         = Jörgen Bergmark Bent Hamer
| starring       =
| music          = Hans Mathisen
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 95 minutes
| country        = Norway Sweden
| language       =
| budget         =
| gross          =
}}
Kitchen Stories ( ) is a 2003 Norwegian film by Bent Hamer.

==Plot==
Swedish efficiency researchers come to Norway for a study of Norwegian men, to optimize their use of their kitchen. Folke Nilsson (Tomas Norström) is assigned to study the habits of Isak Bjørvik (Joachim Calmeyer). By the rules of the research institute, Folke has to sit on an umpires chair in Isaks kitchen and observe him from there, but never talk to him. Isak stops using his kitchen and observes Folke through a hole in the ceiling instead. However, the two lonely men slowly overcome the initial post-war Norwegian-Swede distrust and become friends.

==Inspiration==
Bent Hamer was amused after perusing post-war research books on the efficiency of the Swedish housewife, and pondered on the idea of research being done on men. This led to him making the film Kitchen Stories.

==Cast ==
*Joachim Calmeyer as Isak Bjørvik
*Tomas Norström as Folke Nilsson
*Bjørn Floberg as  Grant
*Reine Brynolfsson as Malmberg
*Sverre Anker Ousdal as Dr. Jack Zac. Benjaminsen
*Leif Andrée as Dr. Ljungberg
*Gard B. Eidsvold ground crew
*Lennart Jähkel as Green
*Trond Brænne as spokesman
*Bjørn Jenseg as caretaker
*Jan Gunnar Røise as assistant caretaker
*Karin Lunden as female corporate assistant

==Awards==
* Best Film, Amanda Award, Norway 2003
* Best Director, Copenhagen International Film Festival 2003
* Best Director, São Paulo International Film Festival 2003
* FIPRESCI Prize, Tromsø International Film Festival 2003

==External links==
*  

 

 
 
 
 
 
 
 


 
 