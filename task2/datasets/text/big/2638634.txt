Frankenstein Unbound
 
{{Infobox film
| name           = Frankenstein Unbound
| image_size     =
| image	         = Frankenstein Unbound FilmPoster.jpeg
| caption        =
| director       = Roger Corman
| producer       = Jay Cassidy (assoc.) Roger Corman Kobi Jaeger Laura J. Medina (assoc.) Thom Mount
| writer         = Brian Aldiss (novel) Roger Corman F.X. Feeney
| narrator       = John Hurt
| starring       = John Hurt Raúl Juliá Nick Brimble Bridget Fonda Catherine Rabett Jason Patric Michael Hutchence
| music          = Carl Davis
| cinematography = Armando Nannuzzi Michael Scott Jay Lash Cassidy
| studio         = A Mount Company Production
| distributor    = 20th Century Fox
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $11.5 million Chris Nashawaty, Crab Monsters, Teenage Cavemen and Candy Stripe Nurses - Roger Corman: King of the B Movie, Abrams, 2013 p 210  
| gross          = $334,748
}}

Frankenstein Unbound is a 1990 horror movie based on Brian Aldiss novel of the same name.  The film was directed by Roger Corman, returning to the directors chair after a hiatus of almost twenty years. This is his final directorial effort to date. The film starred John Hurt, Raúl Juliá and Bridget Fonda.

==Plot==
In 2031, Dr. Buchanan and his team work to develop the ultimate weapon, an energy beam that will completely remove whatever it is aimed at. Buchanan hopes he can create a weapon so powerful that it will end all war and have the added benefit of no impact on the environment. Unfortunately, the prototype has unpredictable side effects, creating erratic global weather patterns and rifts in space and time that have caused some people to vanish. As he drives home from the testing facility, Buchanan himself is caught in one such rift.

Buchanan and his car reappear in Switzerland in 1817. In a village, he meets Victor Frankenstein. The men discuss science over dinner and it is revealed that Frankensteins young brother has been killed. A trial is to determine the guilt or innocence of the boys nanny, who is suspected in the murder.

Several villagers claim to have seen a monster in the woods and suggest this is the killer. Buchanan observes the trial and becomes interested in a young woman taking notes. She turns out to be Mary Shelley, author of the Frankenstein novel. Shelley gives credence to the talk of monsters, but the judge does not. The nanny is found guilty and sentenced to die at the gallows. Buchanan knows the monster killed the child. He implores Frankenstein to come forward and reveal the truth, but Frankenstein refuses. Buchanan then asks Shelley for help, telling her that he is from the future. They are attracted to each other, but Mary, fearing to know too much about the future and her own destiny, chooses not to become involved. Buchanan is on his own. He drives his car to Frankensteins workshop and finds the doctor in discussion with the monster.

The monster has killed Frankensteins fiance, saying that if a mate was not made for him then he would deprive Frankenstein of his. Frankenstein asks Buchanan to use his knowledge of electricity to assist in resurrecting the dead woman. Buchanan instructs the monster to run cables to a weather vane on the roof. While the monster is distracted, Buchanan re-routes some of the electrical cables to begin powering up the prototype laser in his car.

As the lightning strikes the tower again and again, the battery on the laser begins to charge and the corpse on the table begins to move. At the same moment, the woman is restored to life and Buchanans energy beam is fully charged; he fires. The castle is destroyed.

But the laser opens another space-time rift, sending Buchanan, Frankenstein and the two monsters far into the future. They land on a snowy mountain with no sign of civilization. Frankenstein and the monster both try to entice the woman to them, only to have her force Frankenstein to shoot and kill her. Enraged, the monster kills Frankenstein and trudges off into the snowstorm. Buchanan follows, hoping to kill the monster before he reaches a city and kills again.

Eventually the monster is cornered in a cave filled with computers and machines. When Buchanan enters, the machines chirp to life and a voice says "Welcome back, Dr. Buchanan." The monster tells Buchanan that the cave is the central brain for the nearby city, the last one remaining after the world has been devastated by Buchanans ultimate weapon. Buchanan engages security devices and the monster is burned to death by lasers. Buchanan makes his way to the nearby city through the snow.

As he walks, the monsters voice is heard saying that he cannot truly be killed, for now he is "unbound."

== Cast == Frankenstein and the Monster.

Jason Patric appears briefly as Lord Byron, and in a rare film appearance, Australian rock singer Michael Hutchence plays Percy Shelley.  The computers voice is provided by Terri Treas. 

==Production==
 
Corman was paid $1 million to direct. 

==Reception==
 
Frankenstein Unbound received mixed reviews from critics, as it holds a 44% rating on Rotten Tomatoes based on 16 reviews. It didnt do well at the box office also, grossing nearly $335,000.

==Locations==
This feature was filmed in Italy. 
==Release==
 
==See also==

* List of films featuring Frankensteins monster

==References==
 

== External links ==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 