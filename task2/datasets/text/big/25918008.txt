Deep Freeze (film)
{{Infobox film
| name           = Deep Freeze
| image          = Ice Crawlers.jpg
| alt            = 
| caption        = theatrical poster as Ice Crawlers
| director       = John Carl Buechler
| producer       = John Carl Buechler James Rosenthal
| screenplay     = Robert Boris Dennis A. Pratt Matthew Jason Walsh
| story          = Robert Boris Norman Cole Rebekah Ryan Ken Williams
| cinematography = Tom Calloway
| editing        = JJ Jackson
| studio         = Regent Productions
| distributor    = Regent Worldwide Sales
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Deep Freeze  (also known as Ice Crawlers) is a 2003 horror film directed by John Carl Buechler.   Written by Robert Boris, Dennis A. Pratt, and Matthew Jason Walsh, the film was shot in Germany in 2001. The film was retitled Ice Crawlers for USA release. 

==Plot==
The Geotech Company has set up a large   makes its appearance and attacks Dr. Kelsey.  One by one, the rest get picked off by the monster.

==Partial cast==
* Götz Otto as Nelson
* Robert Axelrod as Lenny Norman Cole as Munson
* Rebekah Ryan as Kate
* Allen Lee Haff as Curtis Alexandra Kamp-Groeneveld as Dr. Monica Kelsey
* Karen Nieci as Arianna
* Howard Holcomb as Tom
* David Millbern as Ted Jacobson
* David Lenneman as Update
* Billy Maddox as Clyde
* Tunde Babalola as Shockley

==Reception== The Thing, but there’s a certain amount of depth to the characters and there are no wannabe scream queens in the cast, which is a blessing." 

 , and the lead writer is the man responsible for Backyard Dogs.  The reviewer then lambasts the monster, a trilobite with the seeming and inexplicable power of teleportation and mind reading. Dubbing the creature "Trilly", the reviewer writes "Trilly, you see, is no ordinary man-eating, prehistoric insect awoken from an icy slumber. No, Trilly has magical powers that your typical man-eating, prehistoric insect didn’t have. Trilly seems to be psychic. Much like Santa, Trilly knows when you are sleeping and knows when you’re awake and really knows when you’re taking a bath or shower. Most importantly, Trilly knows when you’re alone. It becomes numbing the sheer number of times there are several people in a room discussing whatever but the moment one of them is alone in that room, Trilly suddenly strikes, having apparently hid somewhere in the room waiting patiently for the very moment when someone would be left alone. It’s ridiculous how often this happens."  He notes that the plot makes no attempt to explain how the creature is able to roam so freely and so quickly for a large and slow beast.  He concludes that the film "takes itself way too seriously given the silly concept of a killer prehistoric roach", writing that it appeared that "Buechler was trying to make a 1950’s style creature feature but mistakenly chose to go about things with the mentality of a modern slasher flick". "There is no suspense here and most shocking, given the director’s pedigree, we don’t even get any good kills. Despite being directed by a guy that has created more than his fair share of gory effects for the movies, Ice Crawlers is surprisingly anemic." 
 The Thing - minus the craftsmanship, the artistry and the talent."  His conclusion was that the film was "one of director J.C. Beuchlers least professional efforts". 

==References==
 

   

   

   

   

 

==External links==
*  

 

 
 
 
 
 