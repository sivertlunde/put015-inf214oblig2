My Wrongs 8245–8249 & 117
 
{{Infobox film
| name = My Wrongs #8245–8249 & 117
| image = ChrisMorris_MyWrongsDVD.jpg
| caption = 
| producer = Mark Herbert Chris Morris
| writer = Chris Morris
| starring = Paddy Considine
| music = Chris Morris Adrian Sutton Richard Hawley
| cinematography = Danny Cohen
| editing = Billy Sneddon
| distributor = Warp Films
| released = 14 November 2002  (premiere, London Film Festival)  24 February 2003  (DVD release) 
| runtime = 12 minutes English
}} Chris Morris, starring Paddy Considine as a mentally disturbed man taking care of a friends Doberman Pinscher (named Rothko, and voiced by Morris) while shes away. The dog talks to him and convinces the nameless protagonist that he is on trial for everything hes done wrong in his life, and the dog is his lawyer. Unfortunately, the dog tends to make things worse for him, and the mans life falls further into disrepair.
 British record label Warp Records. It was released on DVD in 2003 in film|2003, on a region 0, PAL disc. The disc featured a false commentary track, among other bonuses. The packaging included a handwritten list of various wrongs committed by the protagonist, although one would have to destroy the case to read them all. The title comes from this tendency of the protagonist to record his sins: the film depicts numbers 8245–8249 in the timeline, and number 117, which was an unguarded comment made as a small boy, in a flashback.

In 2003, the film won a BAFTA for best short film. 

The short film was based on a monologue from Chris Morris earlier radio programme Blue Jam, and was attempted as a sketch in the television adaptation Jam (TV series)|Jam. The sketch didnt work for undisclosed reasons in Jam, but very short clips were edited into the TV series (there are small sections before the last sketches in episodes 1 and 6, showing the man running after the dog, with the leash around his neck).

==External links==
*  
*  
*  

 

 
 
 
 
 