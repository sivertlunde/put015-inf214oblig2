Corpse for the Lady
{{Infobox film
| name           = Corpse for the Lady
| image          = Cadavere per signora.jpg
| caption        = Film poster
| director       = Mario Mattoli
| producer       =  
| writer         =  
| starring       = Sylva Koscina
| music          = Gianni Ferrio
| cinematography = Alessandro DEva
| editing        = Roberto Cinquini
| released       =  
| runtime        = 90 minutes
| country        = Italy 
| language       = Italian
}}
Corpse for the Lady ( ) is a 1964 Italian comedy film directed by Mario Mattoli and starring Sylva Koscina.

==Cast==
* Sylva Koscina - Laura Guglielmetti
* Sergio Fantoni - Commisario
* Scilla Gabel - Renata
* Sandra Mondaini - Marina
* Lando Buzzanca - Enzo
* Franco Franchi - Gianni
* Ciccio Ingrassia - Luigi
* Rosalba Neri - Giovanna
* Elsa Vazzoler - Costanza
* Francesco Mulé - Augusto Ferrante (as Francesco Mulè)
* Toni Ucci - Michele
* Piero Mazzarella - Tommaso Borsotti
* Angelo Santi (as Angelo Santi Amantini)
* Paolo Bonacelli - Gedeon

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 