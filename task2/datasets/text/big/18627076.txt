Woody Dines Out
{{Infobox Hollywood cartoon|
| cartoon_name        = Woody Dines Out Woody Woodpecker
| image               = WoodydinesTITEL.jpg
| caption             =
| director            = Shamus Culhane
| story_artist        = Ben Hardaway Milt Schaffer Don Williams Emery Hawkins Pat Matthews Grim Natwick
| background_artist   = Phillip DeGuard
| voice_actor         = Ben Hardaway Hans Conried
| musician            = Darrell Calker
| producer            = Walter Lantz
| studio              = Walter Lantz Productions Universal Pictures
| release_date        = April 1, 1945 (United States|U.S.)
| color_process       = Technicolor
| runtime             = 6 41"
| movie_language      = English
| preceded_by         = Chew-Chew Baby
| followed_by         = The Dippy Diplomat
}}
 Universal Pictures. It is the final wartime Woody Woodpecker short released a month before V-E Day and 4 months before V-J Day.

==Plot==
Woody walks about town and realizes that all of the restaurants are closed. He finds one store with a sign in its window that reads: "We stuff birds." He assumes that it is a restaurant when it is actually a taxidermists shop. He approaches the counter to place his order. From his coat pocket, the taxidermy|taxidermist, an anthropomorphic cat (voice by Hans Conried), removes an ad from the Museum of Natural History announcing a $100,000 reward for a stuffed king-size woodpecker. He secretly places knock-out drops in the food he prepares for Woody. The food puts Woody to sleep, but he recovers on the cutting table. He escapes the taxidermist by climbing onto an elevator; the taxidermist falls down the elevator shaft to the basement, where he abandons his $100 grand ambition.
 

==Cultural references==
 Funeral March" can be heard in the background score as the taxidermist places the knock-out drops into Woodys food. blackout borscht." Allied Powers at the time. army rationing war effort.
*The background for the title card is two plates of food, which ties in to the cartoon name.

==See also==

*List of Woody Woodpecker theatrical cartoons

==References==
*Cooke, Jon, Komorowski, Thad, Shakarian, Pietro, and Tatay, Jack. " ". The Walter Lantz Cartune Encyclopedia.

 
 
 
 
 


 