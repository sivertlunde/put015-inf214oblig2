The Hayseeds' Melbourne Cup
 
{{Infobox film
| name           = The Hayseeds Melbourne Cup
| image          = Hayseeds_Melbourne.jpg
| image_size     = 
| caption        = Contemporary advertisement
| director       = Beaumont Smith
| producer       = Beaumont Smith
| writer         = Beaumont Smith
| based on      = 
| narrator       = 
| starring       = Tal Ordell Fred MacDonald
| music          = 
| cinematography = A.O. Segerberg
| editing        = 
| studio         = Beaumont Smith Productions
| distributor = Beaumont Smith Kookaburra Films
| released       = 28 January 1918 
| runtime        = 4,000 feet
| country        = Australia
| language       = silent
| budget         = 
}}
The Hayseeds Melbourne Cup is a 1918 Australian rural comedy from director Beaumont Smith. It was the fourth in his series about the rural family, the Hayseeds, and centers around Dad Hayseed entering his horse in the Melbourne Cup.

It is considered a lost film.

==Synopsis==
Dad Hayseed buys a horse for ₤20 called Cornstalk and it starts winning so many local races he decides to enter it in the Melbourne Cup. He is accompanied by his son Jim (the trainer), cousin Harold (jockey) and Mum Hayseed. Jim falls in love with an actress, and Mum has a nightmare about washing steps of Parliament House. On the race day, Mum boils a billy at Flemington and Cornstalk wins the race despite the efforts of a crooked bookmaker.

==Cast==
*Fred MacDonald as Jim Hayseed
*Tal Ordell as Dad Hayseed
*Harry McDonna as Cousin Harold
*Gladys Leigh as Mrs Hayseed
*Guy Hastings
*Ethel Grist
*Mattie Ive
*Jessie Dale
*Driscoll

==Production== Victoria in November 1917.  It included footage from the real 1917 Melbourne Cup Carnival, as well as Parliament House, Flinders Street Station, Collins Street, Bourke Street, and the stables belonging to famous horse trainer James Scobie.  

It was followed by Townies and Hayseeds.

==References==
 

==External links==
*  in the Internet Movie Database
*  at National Film and Sound Archive

 

 
 
 
 


 