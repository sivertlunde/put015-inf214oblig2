Doraemon: Nobita's Space Heroes
{{Infobox film
| name           = Doraemon: Nobitas Space Heroes
| image          = Doraemon 2015.PNG
| alt            = 
| caption        = Japanese poster of the film
| director       = Yoshihiro Osugi
| producer       = 
| writer         =  Higashi Shimzu
| based on       =   
| starring       = Wasabi Mizuta Megumi Ohara Yumi Kakazu Tomokazu Seki Subaru Kimura Inoue Marina Noto Mamiko Tanaka Yuji Mizuki Arisa Ichimura Masachika Furukawa Yuron Nobuaki Sekine Soga Natsumi Yuji Tanaka Furuta Uta Kotono Mitsuishi
| music          = Kan Sawada
| cinematography = 
| editing        = 
| studio         = Shin-Ei Animation (Japan)  Bang Zoom! Entertainment (US)
| distributor    = Toho (Japan) Walt Disney (US)
| released       =  
| runtime        = 100 minutes 
| country        = Japan, United States
| language       = Japanese, English
| budget =
| gross = US$34 million (Japan) 
}}

 , also known as Doraemon The Super Star 2015, is a Japanese anime film  and the 36th List of Doraemon films|Doraemon film. It was released in Japan on 7 March 2015. This movie commemorates the 35th anniversary of the Doraemon (1979) anime series and 10th anniversary of the Doraemon (2005) anime.

== Plot==
One day Doraemon, Nobita, Shizuka, Gian and Suneo were shooting a film as space heroes. When they were shooting the film in the open lot a boy called Aron comes and alerts them of aliens attacking his planet. They agree to help him by turning into real superheros, but it isnt as easy as it looks.

== Cast==
{| class="wikitable"
! Character !! Japanese voice Actor
|-
| Doraemon || Wasabi Mizuta 
|-
| Nobita Nobi || Megumi Ohara 
|-
| Shizuka Minamoto || Yumi Kakazu 
|-
| Gian || Subaru Kimura
|-
| Suneo Honekawa || Tomokazu Seki
|-
|   Aron || Inoue Marina
|-
|   Burger Director || Noto Mamiko
|-
|   Haido || Tanaka Yuji
|-
|   Meba || Mizuki Arisa
|-
|   Ikaros || Ichimura Masachika
|-
|   Ogon || Furukawa Yuron
|-
|   Ojisan || Nobuaki Sekine
|-
|   Koro || Soga Natsumi
|-
|   Hyde || Yuji Tanaka
|-
|   Ribbon || Furuta Uta
|-
|   Nobitas Mother || Kotono Mitsuishi
|}

==Guest Characters==
* Aron - He comes from another planet to Earth to get help as his planet is being invaded by space monsters
* Burger Director - He is a gadget from Doraemon made to help the person using the gadget shoot a film.

==Box office== 
The film topped the Japanese box office during its opening weekend (March 7-8) earning US$5.3 million on 557,000 admissions from 365 screens.      As of May 4, 2015, it had grossed US$34 million at the Japanese box office.   

== References ==
 

==External links==
*   
* 

 
 

 
 
 
 


 