Dil Tera Aashiq
{{Infobox film
| name = Dil Tera Aashiq दिल तेरा आशिक़  
| writer = Reema Rakeshnath
| starring = Madhuri Dixit, Salman Khan, Anupam Kher
| image= Dil Tera Aashiq.jpg
| caption = Film poster
| director = Lawrence DSouza
| producer = Rakesh Nath
| released =  
| country = India Hindi
| music = Nadeem-Shravan
| box office  = 12.5 million
}}
 Indian Bollywood film directed by Lawrence DSouza. The film stars Salman Khan and Madhuri Dixit in pivotal roles. The film premiered on 5 October 1993.

== Synopsis ==
Thakur Ranvir Singh Chaudhary (Anupam Kher) is rich and influential man of his town. He doted on his only sister Radha, but after her marriage to a man who was much below their status, he could never forgive her. After Radhas death, the responsibility of looking after the children, Vijay, Bittu & Gudiya falls on his shoulders. He hires the services Soniya, masquerading as an old governess Savitri Devi (Madhuri Dixit) to take care of the younger two. Young and dashing Vijay (Salman Khan) falls in love with Soniya in the dancing school where both are dance instructors. Thakur, a confirmed bachelor, is also drawn towards Savitri Devi; Naseeb Kumar (Kader Khan), a good friend of Chaudhary, helps him to win the love of Savitri.

Pratap Singh, a deadly enemy of Chaudhary, tries all evil ways to get his work done.

== Cast ==
*Madhuri Dixit  as  Sonia Khanna/Savitri Devi
*Salman Khan  as  Vijay
*Anupam Kher  as  Chaudhary Ranbir Singh
*Kader Khan  as  Naseeb Kumar
*Asrani  as  Natwar Lal
*Shakti Kapoor  as  Black Eye
*Mangal Dhillon  as  Black Eyes Henchman
*Deepak Tijori  as  Announcer
*Guddi Maruti  as  Passenger in train
*Anjana Mumtaz  as  Mrs. Khanna
*Piloo Wadia  as  Mrs. Lobo
*Raja Duggal  as  Household Servant
*Anil Kapoor
* Razak Khan
* Tej Sapru
* Master Monti

==Soundtrack==
{{Infobox album  
| Name = Dil Tera Aashiq
| Type = Soundtrack
| Artist = Nadeem-Shravan
| Cover = 
| Released = 1993
| Recorded =  Feature film soundtrack
| Length = 
| Label = Tips Music Films
| Producer = Nadeem-Shravan
| Reviews = 
| Last album = Sainik   (1993)
| This album = Dil Tera Aashiq (1993)
| Next album = Tadipaar  (1993)
}}
{{Album ratings
| rev1 = Planet Bollywood
| rev1Score =      
}}
Rakesh Budhu of Planet Bollywood gave the album 8.5 stars stating, "Nadeem-Shravan have always had the touch for making these romantic soundtracks work, Dil Tera Aashiq is another contribution to the shine on the crown of melody kings". 
{{Tracklist
| all_music = Nadeem-Shravan Sameer
| extra_column = Playback

| title1 = Dil Tera Aashiq
| extra1 = Kumar Sanu & Alka Yagnik 
| length1 = 5:08
| title2 = Humse Sajna Kyon Ruthe
| extra2 = S.P. Balasubrahmanyam
| length2 = 5:53

| title3 = Humse Sajna Kyon Ruthe
| extra3 = Alka Yagnik
| length3 = 5:57

| title4 =  Kam Se Kam Itna Kaha Hota
| extra4 = Alka Yagnik, Mukul Agarwal
| length4 = 6:00

| title5 = Mujhe Kuchh Kahna
| extra5 = Sadhna Sargam, Sudesh Bhosle
| length5 = 8:26

| title6 = Namaste Namaste
| extra6 = Alka Yagnik, Vinod Rathod
| length6 = 6:07

| title7 = Pyaasa Kuen Ke Paas
| extra7 = Udit Narayan
| length7 = 5:10

| title8 = Pyar Ke Badle Pyar Milega
| extra8 = Kumar Sanu & Alka Yagnik
| length8 = 5:39
}}

==References==
 

==External links==
* 

 
 
 
 