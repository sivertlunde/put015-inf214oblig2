Danny Collins (film)
 
{{Infobox film
| name           = Danny Collins
| image          = Danny Collins Official Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Dan Fogelman
| producer       = {{Plainlist|
* Nimitt Mankad
* Jessie Nelson
* Denise Di Novi
* Shivani Rawat}}
| writer         = Dan Fogelman
| starring       = {{Plainlist|
* Al Pacino
* Annette Bening
* Jennifer Garner
* Bobby Cannavale
* Christopher Plummer}}
| music          = {{Plainlist|
* Ryan Adams Theodore Shapiro}}
| cinematography = Steve Yedlin
| editing        = Julie Monroe
| studio         = {{Plainlist|
* Big Indie Pictures
* ShivHans Pictures}} Bleecker Street Media
| released       =  
| runtime        = 106 minutes  
| country        = United States
| language       = English
| budget         = $10 million 
| gross          = $5.4 million   
}} feature directorial debut. Inspired by the true story of folk singer Steve Tilston,  the film stars Al Pacino, Annette Bening, Jennifer Garner, Bobby Cannavale and Christopher Plummer. The film was released on March 20, 2015.

==Plot==
 
Aging 1970s rocker Danny Collins (Al Pacino) cannot give up his hard-living ways. But when his manager Frank Grubman (Christopher Plummer) uncovers a 40-year-old undelivered letter written to him by John Lennon and Yoko Ono, he decides to change course, and embarks on a heartfelt journey to rediscover his family, find true love and begin a second act.

==Cast==
* Al Pacino as Danny Collins
* Annette Bening as Mary Sinclair
* Jennifer Garner as Samantha Leigh Donnelly
* Bobby Cannavale as Tom Donnelly
* Christopher Plummer as Frank Grubman
* Katarina Čas as Sophie
* Giselle Eisenberg as Hope Donelly
* Melissa Benoist as Jamie
* Josh Peck as Nicky Ernst

==Production==
In November 2010, Steve Carell was attached to play the rockers son but he ultimately dropped out due to scheduling conflicts.     In June 2011, Al Pacino was in discussions to star in the film.  In October 2012, Jeremy Renner  was announced as Carrells replacement and Julianne Moore also joined the film.  Both were replaced with Bobby Cannavale and Annette Bening, respectively. Chicago in Los Angeles. 

==Reception==
Danny Collins has received generally positive reviews from critics. On Rotten Tomatoes, the film has a rating of 78%, based on 83 reviews, with an average rating of 6.6/10. The sites critical consensus reads, "Thanks to Al Pacinos stirring central performance - and excellent work from an esteemed supporting cast — Danny Collins manages to overcome its more predictable and heavy-handed moments to deliver a heartfelt tale of redemption."  On Metacritic, the film has a score of 58 out of 100, based on 29 critics, indicating "mixed or average reviews". 

==References==
 

==External links==
*  
*   at History vs. Hollywood

 
 
 
 
 
 
 
 
 
 
 
 
 
 