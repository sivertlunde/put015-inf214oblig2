Raw Justice
{{Infobox film
| name           = Raw Justice
| image          = Rawjustice.jpg
| image_size     =
| caption        = DVD cover   Overview on New York Times Website 
| director       = David A. Prior David Winters
| writer         = David A. Prior
| starring       = David Keith Robert Hays Pamela Anderson
| music          = Lennie Moore William T. Stromberg Carlos González
| editing        = Tony Malanowski
| distributor    =
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 35,254,617
}}

Raw Justice also known as Good Cop, Bad Cop or Strip Girl is a 1994 film starring Stacy Keach, David Keith, Airplane! star Robert Hays and Pamela Anderson. 

== Plot ==
New Orleans journalist Donna Stiles (April Bogenschutz) is in her home one night, preparing to take a shower, when a man sneaks into her home and kills her.
 Charles Napier), calls on Donnas former fiancee, cop-turned-bounty hunter Mace (David Keith), to stop chasing bail-jumpers and bring in the killer. Mitch McCallum (Robert Hays), who once dated Donna, with disastrous results, and is now accused of the murder, insists that he is innocent.

Mace has an uneasy relationship with the regular police force, especially Detective Atkins (Leo Rossi). Mace tackles his mission wholeheartedly until Mitch is nearly killed by a bomb planted in his home. Mace and Mitch are ambushed and pursued; they barely escape, accompanied by Sarah (Pamela Anderson), a hooker who witnessed the attacks and must go into hiding with Mace and Mitch.

Mace threatens Bernie (Bernard Hocke), a bail bondsman, with a baseball bat to find out who posted Mitchs bond and wanted him killed out on the street. After Mace leaves Bernies office, Atkins uses the same bat to beat Bernie to death, setting Mace up to be blamed for Bernies death.

{{Quote box
 | quote  = Raw Justice, a mildly diverting item wherein Baywatchs Pamela Anderson throws caution and undergarments to the wind. Otherwise, this murder mystery is trash that knows its name, and goes about its exploitative work with breezy good humor.
 | source =  
 | width  = 150px
 | align  = left
}}

Later, Mitch saves Mace from a gunman in his hotel room. Mace figures out that Mitch has been framed by Deputy Mayor Bob Jenkins (Stacy Keach), who had Donna killed so he could steal a disc from her computer, fill it with false accusations of incest, then use the disc to blackmail Mayor Stiles into refusing to run for office again, because Jenkins is tired of playing second fiddle to Mayor Stiles. Atkins has been working for Jenkins.

Jenkins admits to Mayor Stiles that Jenkins is the mastermind behind Donnas murder as Atkins pursues Mace, Mitch and Sarah across the bayou, finally cornering them in a clip joint, where Mace uses a giant dart to kill Atkins. Jenkins takes Stiles hostage and demands safe passage out of the city.

Jenkins shoots Mayor Stiles in the shoulder, and a helicopter arrives for Jenkins, who releases Mayor Stiles and gets on the helicopter. Disguising himself as Jenkinss pilot, Mace parachutes to safety just before the helicopter slams into a skyscraper, causing an explosion that kills Jenkins.

==Production==
Parts of the movie were filmed in Mobile, AL. Including the motorcycle chase between the Mace and McCallum characters through Springdale Mall (which has since been mostly demolished).

Pamela Anderson would later describe her simulated sex scenes as a "horrible experience". She later testified, "I was thrown around, I was scratched, I was bruised, I was bitten. I cried, I went home, I called my mother".  Her experiences caused her to pull out of another film, "Hello, She Lied".

==Critical attention==
Raw Justice received the Bronze Award at the Worldfest-Charleston in the category for dramatic theatrical films.  It won under the title Good Cop, Bad Cop. Critics seemed to focus on Anderson and her then current role on syndicated Baywatch.  Specifically how steamy the love scenes were in the film, noting that had Baywatch been a network show, she probably would have heard from the network management. Kenny, Glenn   Posted Aug 19, 1994   The notable cast was the focus of other reviewers.  Many found it boring. Notable excerpts of the love scenes were made available online.

==Primary cast==
* David Keith as Mace
* Robert Hays as Mitch McCallum
* Pamela Anderson as Sarah
* Leo Rossi as Detective David Atkins Charles Napier as Mayor David Stiles
* Stacy Keach as Deputy Mayor Bob Jenkins
* Javi Mulero as Detective Gordo Garcia
* Bernard Hocke as Bernie
* April Bogenschutz as Donna Stiles
* Marshall Russell as Sonny
* Jeanette Kontomitras as Blaze
* Larry McKinley as Desk Sergeant
* Hal Jeansonne as Bartender Joe 
* David Veca as Thug #1

== External links ==
*  
*  

==Notes==
 

 
 
 
 
 