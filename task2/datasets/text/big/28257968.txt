For Love Alone
 
{{Infobox film
| name           = For Love Alone
| image          = For Love Alone.jpg
| image_size     = 
| alt            = 
| caption        = VHS cover
| director       = Stephen Wallace
| producer       = Margaret Fink David Thomas
| writer         = Stephen Wallace
| based on = novel by Christina Stead
| starring       = Helen Buday Sam Neill Hugo Weaving
| music          = Nathan Waks
| cinematography = Alun Bollinger
| editing        = Henry Dangar
| studio         = Western Film Productions
| distributor    = Greater Union
| released       =  
| runtime        = 102 minutes
| country        = Australia
| language       = English
| budget         = AU$3.8 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p178-179-181 
| gross          = AU$193,000 (Australia)
}}

For Love Alone is a 1986 Australian film directed by Stephen Wallace and starring Helen Buday, Hugo Weaving and Sam Neill.  The screenplay was written by Wallace, based on the 1945 novel of the same name by Christina Stead.  The film marked the screen debut of Naomi Watts.  The film was entered into the 37th Berlin International Film Festival.   

==Plot==
In the 1930s, Teresa (Helen Buday|Buday) is a naive young woman dealing with the oppressive attitudes of society and her fathers austere ways.  She has a fling with university Latin professor Jonathan Crow (Hugo Weaving|Weaving). It takes her a while to figure out that he will not get serious with her. Teresa then starts dating liberal-minded banker James Quick (Sam Neill|Neill). Once shes settled down with Quick, the idealistic Teresa becomes enamored with another intellectual, poet Harry (Williams). Quick encourages the affair, hoping that Teresa will come to realize that theres more to true love than mere sexual impulsivity.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Helen Buday || Teresa
|-
| Sam Neill|| James Quick
|-
| Hugo Weaving || Jonathan Crow
|-
| Huw Williams  || Harry
|-
| Hugh Keays-Byrne || Andrew
|-
| Odile Le Clezio || Kitty
|-
| Naomi Watts ||  Leos girlfriend
|}

==Production==
The film was a pet project for Margaret Fink who took six years to raise finance. The bulk of the money came from a pre-sale to Greater Union and from UA. Fink had been impressed by Stir and asked Stephen Wallace to direct.  

Fay Weldon wrote some early drafts but neither Fink or Wallace were happy with them so Wallace did the adaptation himself.  Wallace says it took him three and a half years to write the script. Paul Kalina, "Stephen Wallace", Cinema Papers, Feb-March 1985 p15  

Peter Strauss was originally cast to play James Quick but was replaced by Sam Neill. Genevieve Picot was tentatively cast in the lead role but it took two years to raise the money by which time it was felt she was too old so she was replaced by Helen Buday, who had only made one film previously. 

Most of the movie was shot in Sydney starting in March 1985 with a four day shoot at Oxford University.  

==Reception==
The movie received poor reviews. Stephen Wallace says he was particularly shocked by the reviews from feminists and womens magazines. "They attacked it so viciously. So I guess it wasnt a totally competent film. I know it was a bit slow and clumsy and I have to accept that."   accessed 21 November 2012 

For Love Alone grossed $193,000 at the box office in Australia  which was considered a disappointment. Stephen Wallace regrets his treatment of the subject matter was not bolder. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 