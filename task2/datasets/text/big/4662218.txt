Paramasivan
 
{{Infobox film
| name           = Paramasivan
| image          = Paramasivan.JPG
| alt            = 
| director       = P. Vasu
| producer       = Singanamala Ramesh|S. Ramesh Babu
| writer         =
| story          = Mahesh Bhatt Laila Vivek Vivek Prakash Raj Jayaram
| music          = Vidyasagar (music director)|Vidyasagar|
| cinematography = Sekar V. Joseph
| editing        = Suresh Urs
| studio         = Kanagarathna Movies Samrat Pictures
| released       =  
| runtime        = 165 minutes
| country        = India Tamil
| budget         = 
}}
Paramasivan ( ) is a 2006 Cinema of India|Indian, Tamil language|Tamil-language action film directed by P. Vasu and produced by Singanamala Ramesh|S. Ramesh Babu. The film features Ajith Kumar in the lead role with Laila Mehdin|Laila, Vivek (actor)|Vivek, Prakash Raj and Jayaram playing other pivotal roles.

It was released on 14 January 2006 to positive reviews.  

The film was inspired from the Hindi language|Hindi-language film Kartoos (1999) starring Sanjay Dutt.

==Plot==
Subramania Siva (Ajith) is in jail, awaiting the death sentence for killing corrupt and evil members of the police force who had killed his father and sister. SP of Police, Nethiadi Nandakumar (Prakash Raj) is an honest, fearless and upright police officer who has been given the mission to flush out the terrorist outfit behind the Coimbatore blasts. He decides to employ unconventional methods to fulfill his mission and engages Siva to assist him, renaming him Paramasivan, and giving him a single-point agenda – trace out and erase the persons responsible for the Coimbatore blasts. Unaware to Paramasivam, Nandakumar intends on killing him after the mission is completed.

The fly in the ointment is played by Nair (Jayaram), a CBI officer, who is out to trace Paramasivan and stop his unlawful activities. His assistant, SI Agniputran is played by Vivek, who provides lighter moments. How Paramasivan finishes the villains and finishes his job forms the rest of the story. Paramasivan comes to know of Nandakumars idea to kill him after the mission during the last few scenes where the CBI officer Nair intervenes and eventually the hero is forgiven and starts a new life with his lover Laila.

==Cast==
 
* Ajith Kumar as Subramanium Siva / Paramasivan / Eshwar Laila as Malar Vivek as Agniputran
* Prakash Raj as PC Nethiadi Nandakumar
* Jayaram as CBI Nair
* Nassar
* Kota Srinivasa Rao Rajesh
* Santhana Bharathi
* Avinash
* Mayilsamy Sathyan
* Seetha
* Ishwarya
* KPAC Lalitha
* Rahasya in a special appearance
 

==Production== Shankar in Laila selected to play the leading female role.  The film was launched on 10 September 2005 by Rajinikanth, while the title was changed to Paramasivan. 

Following Ajith Kumars exit from Balas directorial venture, Naan Kadavul, this film changed producers the next month with Singanamala Ramesh|S. Ramesh Babu agreeing to finance the film. Nisha Kothari was first requested to appear in an item number in the film but the role was later taken by Rahasya.  For the film, Ajith Kumar lost fifteen kilograms to get into shape.  K. Balachandars Kavithalaya Productions took up the distribution of Paramasivan for the Chengalpet area. 

==Soundtrack==
{{Infobox album |  
| Name       = Paramasivan
| Type       = Soundtrack Vidyasagar
| Cover      = 
| Released   = 2006
| Recorded   = 
| Genres     = World Music
| Length     =
| Label      =  Vidyasagar
| Last album = Thambi (2006)
| This album = Paramasivan (2006)
| Next album = Aathi (2006)
}}
{| class="wikitable" style="width:70%;"
|-
! Song Title !! Singers
|- Sujatha
|-
| Aasai Dosai || Priya Subramaniam
|-
| Kannan || Kalyani Menon, Saindhavi, Lakshmi Rangarajan
|-
| Natchathira Paravaikku || Tippu (singer)|Tippu, Rajalakshmi
|-
| Thangakkili || Madhu Balakrishnan,Gopika Poornima, Srivarthini
|-
| Undivil || Shankar Mahadevan, Malathy Lakshman
|- Ranjith
|-
| Theme || Instrumental
|}

==See also==
 
* List of action films of the 2000s
* List of Tamil films of 2006
 

== References ==
 

==External links==
*  
*  

 

 
 
 
 
 
 