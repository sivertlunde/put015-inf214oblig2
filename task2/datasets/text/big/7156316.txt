Who Done It? (1942 film)
 
{{Infobox film | name = Who Done It?
  | image = whodoneit.jpg
  | image_size = 175px
  | caption = VHS Cover
  | director = Erle C. Kenton
  | producer = Alex Gottlieb Stanley Roberts John Grant
  | starring =Bud Abbott Lou Costello William Gargan William Bendix Mary Wickes
  | music = Charles Previn
  | cinematography =
  | editing = Arthur Hilton
  | distributor = Universal Pictures
  | released =  
  | runtime = 76 min
  | country = United States
  | language = English
  | budget =
}} 1942 List of comedy-mystery films|comedy-mystery film starring Bud Abbott and Lou Costello.

==Plot==
Chick Larkin (Bud Abbott) and Mervyn Milgrim (Lou Costello) both work at the soda counter of a local radio station.  Their true passion, however, is to become writers on a radio mystery show.  They attend a broadcast of the radio program Murder at Midnight along with one of the writers, Jimmy Turner (Patric Knowles) and the producer, Jane Little (Louise Allbritton).

As the show begins, the network president, Colonel J.R. Andrews (Thomas Gomez), is mysteriously electrocuted.  Seeing this as an opportunity to become radio writers, Chick and Mervyn impersonate detectives and attempt to solve the crime.

Meanwhile, Moran (William Gargan) and Branningan (William Bendix), two real detectives, consider the fake detectives to be prime suspects.  A chase ensues throughout the studio and other murders are discovered, including that of Dr. Marek (Ludwig Stössel), Andrews personal physician. Larkin and Milgrim flee the studio before hearing that Milgrim has
apparently won $10,000 on the Wheel of Fortune radio program, for which he must return to the studio in order to claim the prize. Larkin and Milgrim return, only to be arrested by the real detectives, whom Turner and Little manage to convince that there should be a full reenactment of the program that led to the murders, under the ruse that the true culprit will be revealed.

An eavesdropping Nazi spy (Don Porter), who uses the radio station to transmit information to his cohorts, attends the broadcast. It turns out that the spy murdered the Colonel and his physician because they found out about his illegal radio transmissions.  During the broadcast, he is revealed to be the killer and escapes to the roof, where he is nabbed by Larkin and Milgrim.

==Cast==
===Main===
* Bud Abbott as Chick Larkin / Voice of Himself on Radio
* Lou Costello as Mervin Q. Milgrim / Voice of Himself on Radio
* Patric Knowles as Jimmy Turner
* William Gargan as Police Lt. Lou Moran
* Louise Allbritton as Jane Little
* Thomas Gomez as Col. J.R. Andrews
* William Bendix as Detective Brannigan
* Don Porter as Art Fraser
* Jerome Cowan as Marco Heller
* Mary Wickes as Juliet Collins
* Ludwig Stössel as Dr. Anton Marek
===Supporting===
* Norman Abbott as Organist (uncredited)
* Bobby Barber as Test Technician in Booth (uncredited)
* Gladys Blake as Telephone Operator (uncredited)
* Margaret Brayton as Radio Actress (uncredited)
* Eddie Bruce as Man Telephoning Brazil (uncredited)
* Paul Dubov as Radio Actor (uncredited)
* Ed Emerson as Announcer (uncredited)
* Alice Fleming as Mrs. Laffingwell Telephoning Moscow (uncredited)
* Jerry Frank as Customer (uncredited)
* Edward Keane as Carter (uncredited)
* Joe Kirk as Harry the Radio Technician (uncredited)
* Edmund MacDonald as Murder at Midnight Announcer (uncredited)
* Frank McClure as Acrobatic Show Spectator (uncredited)
* Gene ODonnell as Radio Actor (uncredited)
* Milton Parsons as Coroner (uncredited)
* Frank Penny as Spinelli (uncredited)
* Abel Pina as Acrobat (uncredited)
* Antonio Pina as Acrobat (uncredited)
* Henry Pina as Acrobat (uncredited)
* Jenaro Pina as Acrobat (uncredited)
* Jerry Pina as Acrobat (uncredited)
* Harry Strang as Truck Driver (uncredited)
* Walter Tetley as Elevator Operator (uncredited)
* Buddy Twiss as Announcer (uncredited)
* Billy Wayne as Jake, Telephoner to Alaska (uncredited)
* Crane Whitley as Radio Actor (uncredited)
* Duke York as Attendant with the Coroner (uncredited)

==Rerelease==
Who Done It? was re-released in 1949 with Keep Em Flying, and in 1954 with Ride Em Cowboy. 

==Whos on first?==
There are two references to the teams popular Whos on First? routine.  During the murder scene, Abbott confuses Costello with the "volts-and-watts" routine. Lou bleats, "Next youll be telling me Watts on second base!"  Later, in the radio giveaway program scene, Bud and Lou tune the radio to a broadcast of their own Whos on First? routine, which they promptly turn off.

==Post-production==
After completion of this film, Abbott and Costello began a tour of the United States to help promote the selling of U.S. War Bonds. 

==Home media==
This film has been released three times on VHS 1989,1991 and 2000.  It has also been released twice on DVD.  The first time, on The Best of Abbott and Costello Volume One, on February 10, 2004, and again on October 28, 2008 as part of Abbott and Costello: The Complete Universal Pictures Collection.

==References==

 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 