He's Just Not That Into You (film)
{{Infobox film
| name           = Hes Just Not That Into You
| image          = Notintoyouposter.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Ken Kwapis
| producer       = Nancy Juvonen Michael Disco Gwenn Stroman Drew Barrymore
| screenplay     = Abby Kohn Marc Silverstein
| based on       =   Kevin Connolly Bradley Cooper Ginnifer Goodwin Scarlett Johansson Justin Long
| music          = Cliff Eidelman John Bailey
| editing        = Cara Silverman
| studio         = Flower Films
| distributor    = New Line Cinema
| released       =  
| runtime        = 129 minutes  
| country        = United States
| language       = English
| budget         = $40 million
| gross          = $178,846,899   
}} Kevin Connolly, Bradley Cooper, Ginnifer Goodwin, Scarlett Johansson, and Justin Long.

The film was produced by Barrymores production company, Flower Films, while serving as an executive producer. Grossing just under $180 million at the worldwide box office, the film became a financial success despite being met with mixed critical response.

==Plot==
Nine people in Baltimore deal with their respective romantic problems, usually thwarted by the differing ideals and desires of their chosen partner.  At the center of this is Gigi Phillips (Ginnifer Goodwin), a young woman who repeatedly misinterprets the behavior of her romantic partners.

===Gigi and Alex===
Gigi is a single woman who repeatedly misreads mundane actions and comments from her dates as indications that they are romantically interested in her.  She then frets when the guy does not call.
 Kevin Connolly) at a bar, she befriends the bar owner Alex (Justin Long) who reveals the strategies men use to avoid a woman.  He explains that if a man is interested in a woman, he will overcome any obstacles to ensure they date again, and that Gigi has been misinterpreting and obsessing over imagined "signs" that she receives from men.  This friendship continues, and Gigi interprets his eagerness to always assist, such as taking Gigis call while he is on a date, as a sign that he is interested in her.  She eventually makes a move but Alex claims that he is not interested in her romantically and chastises her for ignoring his advice.  She angrily replies that at least she has not let herself become cynical and bitter like him. 

Gigi has moved on from Alex, no longer desperately trying to construct evidence that he is interested in her.  However, in a role reversal, Alex began mooning over Gigi.  After leaving several unanswered messages Alex arrives at Gigis apartment to declare his love.  Alex reveals that he cannot stop thinking about her and that he is turning into her.  Gigi thinks that she is the rule but after Alex suddenly kisses her passionately, he says that she is his exception.

===Janine, Ben, and Anna=== yoga instructor flirtatious friendship skinny dipping. She invites him to join her but he refuses to which she says, "Thats ok you can just watch," and Ben replies, "You may be the best friend I ever had."
 lung cancer), Janine pointedly asks Ben if he has been smoking and later is perturbed to find cigarette butts hidden in the back yard of the house.  Ben insists that they are not his.  Their contractor Javier (Luis Guzman), aggressively interrogated by Janine, says none of his workers smoke at the house.  During a tense shopping trip to select flooring, Ben reveals to Janine that he has cheated on her.  Janine is devastated but rationalizes Bens behavior, blaming herself for being too cold in the relationship, and declares that she wants to save their marriage.

After a positive meeting about her singing career, Anna begins to have sex with Ben in his office.  They are interrupted by Janine who, hoping to spice up their marriage, has arrived unexpectedly.  Anna hides in a closet, and Ben attempts to send Janine away who then makes a heartfelt plea to save their marriage, and there are indications she succeeds in seducing him.  After Janine leaves, Anna angrily departs vowing to cut ties with Ben.  As Janine tidies up Bens clothes back at their newly completed house, fully satisfied to make it a home, she discovers a fresh pack of cigarettes in a pocket.  She explodes in anger.  When Ben arrives home he finds his clothes neatly folded with a whole carton of cigarettes and a note attached from Janine asking for a divorce.  Janine moves into a new apartment by herself to start a new life.  Anna is later seen performing at an upscale Nightclub|nightclub.  Ben is alone, purchasing the same beer at the same supermarket where he met Anna.

===Conor, Anna, and Mary===
Anna enjoys a close friendship with Alexs friend, real estate agent, Conor.  He is romantically interested in her, while Anna is only interested in a casual relationship with Conor.  Conor misinterprets her hugs and cute nicknames for potential romantic interest.

Annas friend, Mary Harris (Drew Barrymore), is in ad sales for a local newspaper.  She helps Conor promote his real estate business in a series of print ads.  Like Gigi, she meets many men (mostly online), but despite constantly monitoring Email|emails, Pager|pager, phone, and Myspace messages, the "meetings" go nowhere.
 gay clientele, two gay men tell him how he is going wrong with Anna.  Taking their advice, Conor decides to declare his love to Anna.  Vulnerable after falling out with Ben, Anna agrees to start a serious relationship with him.  When Conor proposes buying a house and moving in together, Anna admits that she does not want to, and they break up.

Mary later runs into Conor, recognizing him from his ad photo, and introduces herself, since they have only spoken over the phone.  They hit it off, and start Dating|dating.

===Beth and Neil===
Gigis other co-worker, Beth Murphy (Jennifer Aniston), is living with her boyfriend, Neil (Ben Affleck), who is also friends with Ben.  After seven years together, Beth wants to get married, but Neil does not believe in marriage.  With Gigis newfound pragmatic stance on relationships after advice from Alex, she announces that she will no longer misinterpret vague gestures from men as more than they really are.  This spurs Beth to confront Neil about their relationship.  When he still does not want to get married, she breaks up with him.
 heart attack.  Beth looks after him as he recuperates while her sisters wallow, and their husbands remain glued to the television playing video games and watching football with constant takeout while the household falls into chaos.  As Beth reaches the end of her patience looking after her siblings and in-laws as well as her recuperating father, Neil arrives with groceries and helps with cleaning the house and laundry.  The two reconcile, with Beth saying that Neil is more of a husband to her than her sisters spouses are to them, and she will not insist that they be married.  Neil later proposes to her, and they marry in an intimate ceremony aboard his sailboat.

==Cast==
 
* Ben Affleck as Neil
* Jennifer Aniston as Beth Murphy
* Drew Barrymore as Mary Harris
* Jennifer Connelly as Janine Gunders Kevin Connolly as Conor Barry
* Bradley Cooper as Ben Gunders
* Ginnifer Goodwin as Gigi Phillips
* Scarlett Johansson as Anna Marks
* Justin Long as Alex
* Kris Kristofferson as Rod Murphy
* Hedy Burress as Laura
* Stephen Jared as Steven
* Busy Phillips as Kelli Ann
* Leonardo Nam as Joshua Nguyen Rod Keller as Bruce
* Wilson Cruz as Nathan
* Morgan Lily as Little girl
* Natasha Leggero as Amber Gnech
* Cory Hardrict as Tyrone
* Greg Behrendt as Priest
* Sasha Alexander as Catharine
* Corey Pearson as Jude
* Frances Callier as Frances
* Angela V. Shelton as Angela
* Bill Brochtrup as Larry
* Peter OMeara as Bill
* Brandon Keener as Jarrad
* John Ross Bowie as Dan the Wiccan Luis Guzman (uncredited) as Javier Some Kind of Wonderful) as Watts
 

==Production==
Baltimore, Maryland, was selected for the setting of Hes Just Not That Into You as an alternative to the common New York or Los Angeles settings of romantic comedies.    In addition, screenwriter Marc Silverstein had lived in the city for several years prior to attending college.  However, only exterior shots were filmed in the city. Interior scenes were filmed in Los Angeles.

The films original release date was postponed, moving from October 24, 2008 to February 13, 2009 then to February 6, 2009.

==Reception==

===Critical response=== normalized rating out of 100 reviews from mainstream critics, gave the film a 47% approval rating based on 30 reviews.    While the film received mixed reviews, Ginnifer Goodwin, Jennifer Connelly, Jennifer Aniston and Ben Affleck are often praised by critics as being the stand-outs in the film.  

===Box office===
In its opening weekend, the movie made $27.8 million, topping the box office. Its total US gross amounted to $93,953,653 while internationally it grossed $84,436,590 bringing the worldwide gross to $178,390,243 against a $40 million budget.  . The-Numbers.com. Retrieved 2010-11-24. 

===Awards===
{| class="wikitable"
|-
! Year
! Group
! Category
! Recipient
! Result
|-
| rowspan="2" | 2009
| rowspan="2" | Teen Choice Award
| Choice Movie – Romance
| Hes Just Not That Into You
|  
|-
| Choice Movie Actress – Comedy
| Jennifer Aniston
|  
|}

==Home media==
The DVD and Blu-ray Disc was released on June 2, 2009. The Blu-ray version of the release includes a digital copy. It has grossed $26,350,178 in US DVD sales. The film was sold to E! for 4% of the films domestic box office (~$3.6 million) for television broadcast after the opportunity passed for the USA Network and HBO to pick it up. 

==Soundtrack==
 
{{Infobox album  
|  Name        = Hes Just Not That Into You: Original Motion Picture Soundtrack
|  Type        = Soundtrack
|  Artist      = Various Artists
|  Cover       = 
|  Released    = March 10, 2009
|  Recorded    =
|  Genre       = Soundtrack
|  Length      =
|  Label       = New Line Records
|  Producer    =
|  Reviews     =
}}

The soundtrack album was released on March 10, 2009 by New Line Records.

# "Id Like To" – Corinne Bailey Rae (4:06)
# "Im Amazed" – My Morning Jacket (4:34)
# "Dont You Want Me" – The Human League (3:57)
# "Supernatural Superserious" – R.E.M. (3:24)
# "Madly" – Tristan Prettyman (3:18)
# "This Must Be the Place (Naive Melody)" – Talking Heads (4:55)
# "By Your Side" – The Black Crowes (4:29)
# "I Must Be High" – Wilco (2:59) James Morrison (3:32)
# "If I Never See Your Face Again" – Maroon 5 (3:19) The Replacements (3:04) Fruit Machine" – The Ting Tings (2:53)
# "Smile (Lily Allen song)|Smile" – Lily Allen (3:15) Keane (3:57)
# "Love, Save the Empty" – Erin McCarley (3:17)
# "Friday Im in Love" – The Cure (3:35) Last Goodbye" – Scarlett Johansson (2:32)
# "Hes Into Me" – Cliff Eidelman (2:24)

Notes
* Although Scarlett Johansson is seen singing in her last scene of the film, the song she performs is not actually heard.
* Eidelmans score cue is not included on the physical song CD; it is only part of its download release.

==Score==
 
{{Infobox album  
| Name        = Hes Just Not That Into You: Original Motion Picture Score
| Type        = Film score
| Artist      = Cliff Eidelman
| Cover       = 
| Released    = March 10, 2009
| Recorded    =
| Genre       = Film score
| Length      = 31:55
| Label       = New Line Records
| Producer    =
| Reviews     =
}}

The score for Hes Just Not That Into You was composed by Cliff Eidelman, who recorded his score with an 80-piece ensemble of the Hollywood Studio Symphony at the Newman Scoring Stage.    New Line Records released a score album.

# "Prologue/The Signs" – 2:39
# "Mixed Messages" – 0:57
# "This Other Woman" – 1:19
# "Not to Be Trusted" – 1:55
# "No Exceptions" – 2:05
# "Sailing" – 1:27
# "The Love of Your Life" – 1:16
# "Are You Going to Marry Me" – 1:32
# "Mary at the Blade" – 0:42
# "The Pool" – 1:12
# "Hes Into Me" – 2:24
# "You Dont Fall in Love That Way" – 2:07
# "Tables Turn on Alex" – 0:56
# "Janine Revealed" – 2:43
# "Beths New Day" – 1:38
# "Annas Truth" – 0:54
# "Will You Marry Me" – 3:07
# "End Credit Suite" – 3:03

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 