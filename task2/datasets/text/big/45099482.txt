Le Lit
{{Infobox film
| name           = Le Lit
| image          = 
| caption        =
| director       = Marion Hänsel
| producer       = Jean-Marc Henchoz
| screenplay     = Marion Hänsel
| based on       =  
| starring       = {{plainlist|
* Heinz Bennent
* Natasha Parry
* Johan Leysen
}}
| music          = Serge Kochyne
| cinematography = Walther van den Ende
| editing        = Susana Rossberg 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Belgium Switzerland
| language       = French
| budget         = 
| gross          = 
}}
Le Lit is a 1982 drama film directed by Marion Hänsel and based on the 1960 novel of the same name by Dominique Rolin. The film starred Heinz Bennent, Natasha Parry, and Johan Leysen. Le Lit received the André Cavens Award for Best Film given by the Belgian Film Critics Association (UCC). 

== Cast ==
* Heinz Bennent as Martin
* Natasha Parry as Eva
* Johan Leysen as Dr. Bruno Nanteuil
* Francine Blistin as Caroline
* Patrick Massieu as Tardif
* Marion Hänsel as Tilly

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 
 