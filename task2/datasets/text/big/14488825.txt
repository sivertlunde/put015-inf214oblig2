He Cooked His Goose
{{Infobox Film |
  | name           = He Cooked His Goose
  | image          = Hecookedhisgoose52.jpg 
  | image size     = 190px
  | director       = Jules White  Felix Adler Larry Fine Shemp Howard Angela Stevens Mary Ainslee Diana Darrin
  | producer      = Jules White 
  | distributor    = Columbia Pictures 
  | released       =  
  | runtime        = 15 37"
  | country        = United States
  | language       = English
}}

He Cooked His Goose is the 140th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
The Stooges are separate characters in this entry. Larry is a womanizer who is having an affair with Moes wife, Belle (Mary Ainslee) while making eyes at Shemps fiancée, Millie (Angela Stevens), as well. Moe, however, tracks down the conniving Larry at his pet shop, and gives him the works. Larry is able to think fast and convinces Moe he is innocent, which calms him down. Realizing he needs to cover his tracks, Larry looks for a "fall guy" in the form of Shemp.  

Shemp arrives shortly after Moe had left to confront him for making a pass at his fiancee. Larry lies about why he was at her apartment, and Shemp accepts his apology. Larry remembers his need for a fall guy and helps Shemp get a job as an underwear salesman. The first place he goes is Moes home. ) in He Cooked His Goose]]

While Shemp is modeling his ware for Belle, Larry calls both Millie and Moe and lies to them about Shemps advances on Moes wife. Both of them go storming over to Moes, with Moe carrying a loaded gun. Looking to avoid being killed, Shemp flees up the chimney. After he fools Moe with a Santa Claus disguise, Shemp makes a quick getaway. Shemp then spies Larry coming. Now aware that Larry set him up, Shemp knocks Larry out and dresses him in the Santa outfit and sends him to Moe, Millie, and Moes wife. Moe unmasks Larry to the surprise of them all and chases him out of the apartment. Moe shoots Larry in the buttocks times before accidentally shooting himself in the foot while celebrating.

==Production notes== reworked in 1959 as Triple Crossed, using ample stock footage from the original. It is one of only three films in which Larry Fine is the main character. The other two where he takes the lead role are Woman Haters and Three Loan Wolves. 

He Cooked His Goose was filmed in early January 1952 when Christmas trees were available, but not released until July. When Belle answers the door for Shemp, the Christmas tree she was hanging earlier with Moe is missing. 

Former stooge Curly Howard passed away on January 18, 1952, approximately two weeks after He Cooked His Goose was filmed. 

Over the course of their 24 years at Columbia Pictures, the Stooges would occasionally be cast as separate characters. This course of action always worked against the team; author Jon Solomon concluded "when the writing divides them, they lose their comic dynamic".    In addition to this split occurring in He Cooked His Goose (and its remake Triple Crossed), the trio also played separate characters in Rockin in the Rockies, Cuckoo on a Choo Choo, Gypped in the Penthouse, Flying Saucer Daffy and Sweet and Hot.

A side-gag to this short is Moe knocking down the decorated Christmas tree in his home. He knocks it down the first time, trying to hang the star at the top. The second time comes because he forgot about the hanger in his suit jacket, and accidentally hooks the tree, pulling it down.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 