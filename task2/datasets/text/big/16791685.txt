His Majesty, the American
 
{{Infobox film
| name           = His Majesty, the American
| image          = His Majesty, the American.jpg
| caption        = Theatrical release poster 
| director       = Joseph Henabery
| producer       = Douglas Fairbanks
| writer         = Joseph Henabery Douglas Fairbanks Marjorie Daw
| cinematography = Victor Fleming Glen MacWilliams
| editing        =
| distributor    = United Artists
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         = $300,000 {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 }}p39 
}}

His Majesty, the American is a 1919 American comedy film directed by Joseph Henabery and starring Douglas Fairbanks.    It was the first film released by United Artists. {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 }} p30  Several copies of the film exist in archives and collections. 

==Cast==
* Douglas Fairbanks as William Brooks Marjorie Daw as Felice, Countess of Montenac
* Frank Campeau as Grand Duke Sarzeau
* Sam Southern as King Phillipe IV
* Jay Dwiggins as Emile Metz
* Lillian Langdon as Princess Marguerite
* Albert MacQuarrie as Undetermined Role (as Albert McQuarrie)
* Bull Montana as Undetermined Role
* William Gillis as Undetermined Role (as Will Gillis)
* Phil Gastrock as Undetermined Role (as Phil Gastrox)
* Boris Karloff as The Spy (uncredited)
* Karla Schramm (uncredited)

==See also==
* Boris Karloff filmography

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 
 

 