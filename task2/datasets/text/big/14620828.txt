Tarzan and the She-Devil
{{Infobox film
| name           = Tarzan and the She-Devil
| image          = "Tarzan_and_the_She-Devil"_(1953).jpg
| image_size     =
| caption        = Kurt Neumann
| producer       =
| writer         = Karl Kamb Carroll Young
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       = Lex Barker Joyce MacKenzie Raymond Burr Tom Conway
| music          = Paul Sawtell
| cinematography = Karl Struss
| editing        = Leon Barsha
| studio         = Sol Lesser Productions
| distributor    = RKO Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} American film Kurt Neumann.  

Tarzan is held captive during much of the movie, and critics derided it as lacking action. This was Barkers fifth and final appearance as Edgar Rice Burroughs ape-man.  Barker, who had replaced Johnny Weismuller in the role of Tarzan, would be succeeded by Gordon Scott.

==Plot==
Beautiful but deadly Lyra the She-Devil and her ivory-hunting friends have discovered a large herd of bull elephants and plot to capture them, forcing an East African native tribe to serve as bearers. Their ivory poaching plans meet opposition when Tarzan gives his deafening jungle cry. The tusked creatures come running, stomping all over Lyras plans.  

==Cast==
*Lex Barker as Tarzan Jane
*Raymond Burr as Vargo
*Monique van Vooren as Lyra, the She-Devil
*Tom Conway as Fidel
*Michael Granger as Philippe Lavarre (as Michael Grainger) Henry Brandon as MTara, Lycopo Chief

==Critical reception==
The Radio Times said  "despite the exotic title and a great villain in Raymond Burr, this is a standard tale of ivory-seeking elephant hunters being stymied by the king of the jungle."  

==External links==
* 

==References==
 

 

 
 
 
 
 
 
 
 

 