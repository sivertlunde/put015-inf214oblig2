The Serpent's Tooth (1917 film)
{{infobox film
| name           = The Serpents Tooth
| image          =
| imagesize      =
| caption        =
| director       = Rollin S. Sturgeon
| studio         = American Film Company
| writer         = Forrest Halsey (story) Doris Schroeder (scenario)
| starring       = Gail Kane
| music          =
| cinematography = John Seitz
| editing        =
| distributor    = Mutual Film
| released       = May 28, 1917 (USA) July 29, 1921 (Portugal) reels
| country        = United States Silent (English intertitles)
}}

The Serpents Tooth is a 1917 American silent drama film starring Gail Kane from the stage and released through the Mutual Film company. It is a lost film. 

==Cast==
* Gail Kane as Faith Channing
* William Conklin as James Winthrop
* Edward Peil, Sr. as Jack Stilling
* Jane Pascal as Hortense Filliard
* Frederick Vroom as Matthew Addison-Brown
* Mary Wise as Mrs. Addison-Brown (Mary Lee Wise)
* Charles P. Kellogg as Carrington
* Gayne Whitman as Sid Lennox (* as Al Vosburgh)

==Reception== city and state film censorship boards. The Chicago Board of Censors, because of the plot involving drug use, gave the film an "adults only" permit and required cuts in Reel 1 of the first view of a woman in a low cut gown and two closer views of the same; in Reel 3 of the intertitle "You make her use it. Its grounds for divorce in this state." and the shot of man putting drug into the womans medicine; in Reel 4 of the intertitle "You say you couldnt get any more. I have been more successful."; and in Reel 5 of the intertitles "Your damned lover is a liar." and "Its the drug that loves you - the drug Ive fed her night and day," and the scene of the choking of the wife and knocking her down. 

==References==
 

==External links==
*  
*  
*   at tcm.com

 
 
 
 
 
 
 


 