Fury (2014 film)
 
 
{{Infobox film
| name           = Fury
| image          = Fury 2014 poster.jpg
| alt            = Staring into the distance, a disheveled soldier stands in front of a tank, with "Fury" written on the barrel and other soldiers leaning/sitting on it.
| caption        = Theatrical release poster
| director       = David Ayer
| producer       = {{plainlist|
* Bill Block
* John Lesher
* Alex Ott
* Ethan Smith
* David Ayer }}
| writer         = David Ayer
| starring       = {{plainlist|
* Brad Pitt
* Shia LaBeouf
* Logan Lerman
* Michael Peña
* Jon Bernthal
* Jason Isaacs
* Scott Eastwood }} Steven Price
| cinematography = Roman Vasyanov
| editing        = Dody Dorn
| studio         = {{plainlist|
* Le Grisbi Productions
* QED International
* LStar Capital
* Crave Films }}
| distributor    = Columbia Pictures
| released       =  
| runtime        = 134 minutes   
| country        = United States    China]   UK
| language       = English German
| budget         = $68 million   
| gross          = $211.8 million  
}}

Fury is a 2014 American war film written and directed by David Ayer. The film stars Brad Pitt, Shia LaBeouf, Logan Lerman, Michael Peña, and Jon Bernthal. The film portrays US tank crews in Nazi Germany during the final days of World War II. Ayer was influenced by the service of veterans in his family and by reading books, such as Belton Y. Coopers Death Traps, about American armored warfare in World War II. Ayer aimed for a greater degree of realism in the film than in other World War II dramas.   

Rehearsal began in early September 2013 in Hertfordshire, England followed by principal photography on 30 September 2013, in Oxfordshire. Filming continued for a month and a half at different locations, which included the city of Oxford, and concluded on November 13. Fury was released on 17 October 2014. The film received positive reviews from critics and proved to be successful at the box office, grossing over $211 million.

==Plot==

As the   to him. Norman is then ordered to clean the tank, and vomits outside after finding a part of "Reds" face. While at a forward operating base, its revealed that Wardaddy greatly despises the Waffen-SS, shown when he harasses an injured captive SS officer before telling Norman to kill every one of them he sees.

The surviving crew, who have been together since the North African Campaign, belittle the new recruit upon meeting him, both for his lack of experience and for his reluctance to kill Germans, especially the teenagers of the Hitlerjugend; a decision which results in the death of their platoon leader, Lieutenant Parker, and the destruction of his tank and crew. In an effort to educate him to the realities of war, Wardaddy orders Norman to kill a captive German artilleryman. When Norman refuses, Wardaddy forces the gun into his hand and executes the prisoner by pulling the trigger over Normans hand.

The bond between Norman and Wardaddy becomes stronger after capturing a small German town. While searching a house, Wardaddy and Norman discover a German woman, Irma, and her younger cousin Emma. Norman is then left behind closed doors in the bedroom with Emma. After they come out of the bedroom, the four have breakfast together, but rest of Furys crew barges in, rudely teasing the women and angering Wardaddy and Norman. Shortly afterwards, a German bombardment hits the town, killing Emma and some of the American soldiers caught out in the open in the square. Norman tells Wardaddy that he has begun to enjoy killing German soldiers.
 Ruhr district to Hanover crosses the Weser river). On the way to the crossroads, they are ambushed by a heavily-armed German Tiger I, which quickly destroys one of the tanks. The remaining three tanks reluctantly attack the German tank, despite knowing they are outgunned. The Americans make an effort to rush and outflank the Tiger, but both of Furys fellow Shermans are destroyed in the attempt. With some decisive and experienced maneuvering, Fury gets behind the Tiger where its armor is weakest, and destroys it. Bible notes that he believes they were spared for a reason and the men proceed to the crossroads, knowing that they are the only tank left to protect the camp down the road.

Just as they reach the crossroads, the tank  hits a landmine and throws a track, becoming immobilized. Norman is ordered to scout a nearby hill and he spots a reinforced company of three hundred Waffen-SS panzergrenadiers heading their way. The crew initially wants to abandon the tank and escape on foot, but Wardaddy refuses to leave. The crew, not wanting to abandon their leader, decide to stay and plan an ambush.

After disguising Fury to make it appear as though it has been severely damaged and knocked out, the crew waits for the German soldiers inside the tank. Bible recites "Isaiah, Chapter Six", and Wardaddy breaks out a bottle of whiskey and shares it with the others. Norman takes a hearty slug of whiskey, and Coon-Ass calls him "a fighting, fuckin, drinking machine", leading the men to dub him "Machine." When the Germans arrive, the crew takes them by surprise. Outnumbered and outgunned, Wardaddy and his men nevertheless inflict heavy losses on the Germans using both the tanks and the crews weapons. As they continue to fight and begin to run low on ammunition, Grady, Gordo, and Bible are all killed one by one, and Wardaddy is gravely wounded by an SS sniper. Norman and Wardaddy retreat back into the tank where they share their last words. When SS soldiers drop two grenades into the tank, Wardaddy, wounded and unable to move, orders Norman to escape through the bottom emergency hatch of the tank. Norman dives through the hatch and takes shelter in the crater made by the landmine explosion as the grenades detonate, killing Wardaddy. A young Waffen-SS trooper discovers Norman beneath the destroyed tank but does not report him. Norman continues to hide as the surviving German soldiers move on.

The next morning, Norman awakens and crawls back into the tank. He covers Wardaddy with his coat and takes his revolver as he hears movement outside. As Norman awaits his fate, he is discovered by U.S. Army troops, who tell him that hes a hero. As Norman is being transported to safety, he looks back at the carnage of the hundreds of dead German SS troops surrounding the remains of the destroyed Fury.

==Cast==
  US Army Staff sergeant#U.S. Army|S/Sgt. Don "Wardaddy" Collier 
* Logan Lerman as Private (rank)#United States Army|Pvt. Norman "Machine" Ellison 
* Shia LaBeouf as Technician Fifth Grade|T/5 Boyd "Bible" Swan 
* Michael Peña as Corporal#United States Army|Cpl. Trini "Gordo" Garcia 
* Jon Bernthal as Private first class#United States Army|Pfc. Grady "Coon-Ass" Travis 
* Jason Isaacs as Captain (United States O-3)|Cpt. "Old Man" Waggoner 
* Scott Eastwood as Sergeant#United States|Sgt. Miles  2nd Lt. Parker
* Brad William Henke as S/Sgt. Davis 
* Jim Parrack as S/Sgt. Binkowski
* Anamaria Marinca as Irma
* Alicia von Rittberg as Emma
* Kevin Vance as S/Sgt. Peterson
* Branko Tomović  as German Corporal
* Iain Garrett as Sgt. Foster
* Eugenia Kuzmina as Hilda Meier
* Stella Stocker as Edith 
 

==Production==
  – the only operating Tiger I tank in the world – was lent by the The Tank Museum for the film. It is the first time a genuine Tiger I tank was used in a contemporary war film since 1946; 131 was restored to running condition between 1990 and 2003, further work was only completed in 2012]]
  Deadline reported Sony Pictures won the auction from Universal Pictures for the film and Columbia Pictures acquired the films domestic rights. 

===Casting=== THR announced that Michael Peña was in negotiations to play a member of Pitts tank crew.    On May 17, Jon Bernthal joined the cast as Grady Travis, a cunning, vicious and world-wise Arkansas native.    On August 26, Scott Eastwood also joined the cast, playing Sergeant Miles.    On 19 September Brad William Henke joined as Sergeant Roy Davis, commander of another tank, Lucy Sue. (The second Sherman destroyed by the Tiger).    Jason Isaacs was cast on 7 October 2013.    Other cast members include Xavier Samuel, Jim Parrack, Eugenia Kuzmina, Kevin Vance, and Branko Tomović.   
 Jewish actors (Logan Lerman, Jon Bernthal, Shia LaBeouf, and Jason Isaacs) playing soldiers fighting Nazi Germany.   

===Preparation=== Navy SEALs. Pitt stated, "It was set up to break us down, to keep us cold, to keep us exhausted, to make us miserable, to keep us wet, make us eat cold food. And if our stuff wasnt together we had to pay for it with physical forfeits. Were up at five in the morning, were doing night watches on the hour."

Ayer also pushed the cast to physically spar each other, leading to many black eyes and bloody noses. They insulted each other with personal attacks as well. On top of that, the actors were forced to live in the tank together for an extended period of time where they ate, slept, and defecated.

In regard to his choices, Ayer defended himself, saying, "I am ruthless as a director. I will do whatever I think is necessary to get what I want.” 

===Filming=== Watlington that there would be sounds of gunfire and explosions during the filming of Fury.  

On October 15, 2013, a stuntman was accidentally stabbed in the shoulder by a bayonet while rehearsing at the set in Pyrton. He was taken to John Radcliffe Hospital, Oxford by an air ambulance. Police confirmed that they were treating it as an accident.  In November 2013, the film caused controversy by shooting a scene on Remembrance Day in which extras wore Wehrmacht and Waffen-SS uniforms. Ayer apologized for the incident, and Sony also made an apology.  Filming was wrapped-up on November 15, 2013 in Oxfordshire. 

===Music===
  Steven Price signed on to score the film.     Varèse Sarabande released the original soundtrack album for the film on October 14, 2014. 

==Portrayal of history==
 
Fury is a fictional film about a tank crew during the final days of the war in Europe. Ayer was influenced by the service of veterans in his family and by reading books such as Belton Y. Coopers Death Traps about American armored warfare in World War II. Ayer went to considerable lengths to seek authentic uniforms and weapons appropriate to the period of the final months of the war in Europe. 
The film was shot in England in large part due to the availability of working World War II-era tanks. The film featured Tiger 131, the last surviving operational Tiger I. The tank belongs to Bovington Tank Museum at Bovington, England. It is the first time since the 1946 film Theirs Is the Glory that a real Tiger tank – and not a prop version – has been used on a film set.    Tiger 131 is a very early model Tiger I tank; externally it has some significant differences from later Tiger I models, most noticeably the outermost row of road wheels (of the trio per axle, used in the Schachtellaufwerk overlapping and interleaved arrangement characteristic of the Tiger I) which are also rimmed in rubber, as well as the dustbin shaped cupola.  In the last weeks of the war a number of these early model Tigers were used in last ditch defence efforts; one of Germanys last Tigers to be lost at the Brandenburg Gate in Berlin was of a similar vintage.  Ten working M4 Sherman tanks were used. The Sherman tank Fury was played by an M4A2E8 Sherman tank named RON/HARRY (T224875), also lent by Bovington Tank Museum. 
 Allied tankers, Lafayette G. Canadian Sherman January 24, 1945. The fighting in the film also bears similarity to the 1943 film Sahara (1943 film)|Sahara starring Humphrey Bogart in which the crew of an M3 Lee named "Lulu Belle" and a contingent of stranded British soldiers, defend a remote well in Libya against a larger German force of the elite Afrika Korps, to the demise of most of the Allies.

==Release==
Sony Pictures had previously set November 14, 2014 as the American release date for Fury.  On August 12, 2014, the date was moved up to October 17.    The film was released in the United Kingdom on October 22, 2014.

Fury had its world premiere at Newseum in Washington, D.C. on October 15, 2014,  followed by a wide release across 3,173 theaters in North America on October 17. 

===Home media===
The film was released on DVD and Blu-ray Disc in the United States on January 27, 2015. 

===Partnership with World of Tanks===
The film additionally had a partnership with the popular online video game World of Tanks, where the main tank from the film, Fury, was available for purchase in-game using real currency for a limited time after the films release. The tank also served as the centerpiece in themed events in the vein of the film following its release.  , Online news article reporting on the partnership, Retrieved November 15th, 2014   , Another online news article reporting on the partnership, Retrieved November 15th, 2014    World of Tanks official websites news post on the matter, describing partnership and special in-game events, Retrieved November 15th, 2014 

As part of the UK DVD release, the game also hid 300,000 codes inside copies of the film, which gave in-game rewards and bonuses.<ref
name="SPREAD_THE_WORD_CONTEST(GamePage)">
  World of Tanks Blitz official websites news post on the matter, describing partnership and special in-game events, Retrieved February 23rd, 2015 

===Internet leak=== leaked onto To Write Love on Her Arms).  Within three days of the initial leak, Fury had been downloaded an estimated 1.2 million times. 

==Reception==

===Box office===
Fury was a box office success. The film grossed $85.8 million in North America and $126 million in other territories for a worldwide total of $211.8 million, against a budget of $68 million. 

;North America 
Fury was released on October 17, 2014, in North America across 3,173 theatres.  It earned $1.2 million from Thursday late night showings from 2,489 theatres.   On its opening day, the film grossed $8.8 million.    The film topped the box office on its opening weekend earning $23,500,000 at an average of $7,406 per theatre.   The films opening weekend gross is David Ayers biggest hit of his (now five-film) directorial career, surpassing the $13.1 million debut of End of Watch and his third-biggest opening as a writer behind The Fast and the Furious ($40 million) and S.W.A.T. (film)|S.W.A.T. ($37 million).  In its second weekend the film earned $13 million (-45%). 

;Other territories  Teenage Mutant Ninja Turtles which earned £1.92 million ($3.1 million) from the top spot.   In its second weekend the film added $14.6 million in 44 markets, bringing the overseas cumulative audience   to $37.8 million. It went number one in Finland ($410,000) and in Ukraine ($160,000). 

===Critical response===
Fury has received positive reviews from critics, who praised its visual style, Ayers direction, and the performances of its main cast members. On Rotten Tomatoes, the film has a "Certified Fresh" rating of 77%, based on 221 reviews, with an average rating of 6.9/10. The sites consensus reads, "Overall, Fury is a well-acted, suitably raw depiction of the horrors of war that offers visceral battle scenes but doesnt quite live up to its larger ambitions."  On Metacritic, the film has a score of 64 out of 100, based on 47 critics, indicating "generally favorable reviews". 

The Boston Globe s Ty Burr gave 2.5 out of 4 stars and talked about Pitts character Sergeant Don "Wardaddy" Collier, commenting on Wardaddys portrayal as "the battle-scarred leader of a tank squad pushing through Germany toward Berlin, Brad Pitt creates a warrior who is terse, sometimes noble, more often brutal." Another critic, Burr, explained that Ayer portrayed in the character of Wardaddy "a figure both monstrous and upstanding. In one scene, he shoots a captured enemy officer in the back. A few scenes later, hes protecting two German women from being assaulted by his own men." Burr further stated that, "Fury gives us terrible glimpses: tank treads rolling over a body pancaked into the mud, an elderly woman cutting meat off a dead horse, a woman in a wedding dress among a crowd of refugees. Fury wants to lead us to a fresh consideration of the good war while simultaneously celebrating the old bromides and clichés. No wonder it shoots itself in the tank." 

Newsday s Rafer Guzman admired director Ayer, who "does a good job of putting us inside the tank Fury"; with "all the extra blood and brutality, this is still a macho and romanticized war movie", and he singled out Pitt, who he said "serves honorably in the John Wayne role".  Deadline Hollywood s Pete Hammond praised Lermans performance saying, "It is a great performance, very Oscar-worthy in part of Logan Lerman. Those scenes between Brad Pitt and Logan Lerman trying to teach him the trips of war and how to man up is remarkable." 

 
Film critic  s Steven Rea gave the film 3 out of 4 stars and praised, "Fury presents an unrelentingly violent, visceral depiction of war, which is perhaps as it should be. Bayonets in the eye, bullets in the back, limbs blown apart, corpses of humans and horses splayed across muddy, incinerated terrain. Ayer brought a similar you-are-there intensity to his 2012 cops-on-patrol drama, End of Watch (also with Peña)." But on the opposite side of Reas admiration, he thinks, "It wouldnt be right to call Fury entertaining, and in its narrow focus (as narrow as the view from the tanks periscope), the film doesnt offer a broader take on the horrors of war - other than to put those horrors right in front of us, in plain view."  Chris Vognar wrote the review for The Dallas Morning News giving the film "B+" grade, in which he writes about "War" which he thinks is, "hell," and also "relentless, unsparing, unsentimental and violent to the mind, body and soul. Fury conveys these truths with brute force and lean, precise drama."  Kenneth Turan for the Los Angeles Times praised the film highly, writing: "Best job I ever had" sentence "is one of the catchphrases the men in this killing machine use with each other, and the ghastly thing is they half believe its true."   

  also gave the film a positive review saying: "This is a memorable motion picture, accurately depicting the horrors of war without reveling in the depravity of man (like Platoon). Equally, it shows instances of humanity without resorting to the rah-rah, sanitized perspective that infiltrated many war films of the 1950s and 1960s. Its as good a World War II film as Ive seen in recent years, and contains perhaps the most draining battlefield sequences since Saving Private Ryan. 
 Michael Phillips wrote a negative review, saying "At its weakest, Fury contributes a frustrating percentage of tin to go with the iron and steel." 
 New York magazine s David Edelstein admired the film in his own words, "Though much of Fury crumbles in the mind, the power of its best moments lingers: the writhing of Ellison as hes forced to kill; the frightening vibe of the scene with German women; the meanness on some soldiers faces and soul-sickness on others." 

===Accolades===
{| class="wikitable"
|- style="text-align:center;"
! colspan=4 style="background:#B0C4DE;" | List of awards and nominations
|- style="text-align:center;"
! style="background:#ccc;" width="40%"| Award / Film Festival
! style="background:#ccc;" width="30%"| Category
! style="background:#ccc;" width="25%"| Recipient(s) and nominee(s)
! style="background:#ccc;" width="15%"| Result
|- style="border-top:2px solid gray;"
|- Broadcast Film Critics Association Best Action Movie
|
| 
|- Best Actor in an Action Movie Brad Pitt
| 
|- Hollywood Film Awards Hollywood Editing Award Jay Cassidy and Dody Dorn
| 
|- Hollywood Music in Media Awards Original Score - Feature Film Steven Price Steven Price 
| 
|- Motion Picture Sound Editors Feature English Language - Effects/ Foley Fury
| 
|- National Board of Review
| 
|
| 
|- National Board Best Cast
|
| 
|-
|rowspan=2|Peoples Choice Awards Favorite Movie Actor Brad Pitt
| 
|- Favorite Movie Dramatic Actor
| 
|- Phoenix Film Critics Society Best Actor in a Supporting Role Logan Lerman
| 
|- Screen Actors Guild Screen Actors Outstanding Performance by a Stunt Ensemble in a Motion Picture
|
| 
|- Satellite Awards Best Art Direction & Production Design  Andrew Mendez, Peter Russell 
| 
|- Best Editing  Dody Dorn, Jay Cassidy 
| 
|- Best Original Score  Steven Price 
| 
|- Santa Barbara International Film Festival Virtuosos Award Logan Lerman
| 
|-
|}

==Further reading==
*Frank Jacob,  

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 