The Cross of Lorraine
 
{{Infobox film
| name           = The Cross of Lorraine
| image          = Poster of the movie The Cross of Lorraine.jpg
| image_size     =
| caption        =
| director       = Tay Garnett
| producer       = Edwin H. Knopf
| writer         =
| screenplay     = Ring Lardner Jr. Michael Kanin Robert Hardy Andrews Alexander Esway
| story          = Robert Aisner Lilo Dammert
| based on       =   
| starring       = Jean-Pierre Aumont Gene Kelly
| music          = Bronislau Kaper Sidney Wagner
| editing        = Dan Milner
| studio         = Metro-Goldwyn-Mayer
| distributor    = Loews Inc.
| released       =  
| runtime        = 90 minutes
| country        = United States English
| budget         = $1,010,000  . 
| gross          = $1,248,000 
}} prisoners of war escaping a German prison camp and joining the French Resistance. Directed by Tay Garnett, it stars Jean-Pierre Aumont and Gene Kelly and was partly based on Hans Habes novel A Thousand Shall Fall. The title refers to the French Cross of Lorraine, which was the symbol of the Résistance and the Free French Forces chosen by Charles de Gaulle in 1942.   

==Plot==
At the start of World War II, Frenchmen from all walks of life enlist. Defeated by the invading Germans in 1940, Marshal Philippe Pétain signs a peace agreement and the troops surrender. However, instead of being repatriated to their homes, a group of soldiers find themselves in a brutal prison camp. Most of the men resist as best they can, and some, like Paul (Jean-Pierre Aumont), are willing to spend time in solitary confinement and be subjected to beatings, while others, such as Duval (Hume Cronyn), collaborate with their jailers to get an easier life. The men find solace from Father Sebastian (Sir Cedric Hardwicke), a priest who was also in the army and who counsels them wisely. Eventually Paul helps his fellow prisoners to escape. When they liberate a village, they realise that continued fighting is the only option, so they join the French Resistance.

==Production==
The Cross of Lorraine is one of the many Hollywood World War II propaganda films showing life in occupied Europe, with the purpose of explaining to an American audience why US involvement in the European war was just as important as the war against the Japanese in the Pacific.

The film is partly based on the German refugee author Hans Habes autobiographical Ob Tausend fallen (A Thousand Shall Fall) from 1941, about his war experiences fighting in the French Foreign Legion against his former homeland in 1940, being captured and then escaping the German prison camp.

A number of German, Austrian, French and Dutch actors, who had fled Europe because of the war, participate in the film, not only Peter Lorre, Jean-Pierre Aumont, Richard Ryen and Frederick Giermann, but also several of those who participate in minor roles and as extras.

The Cross of Lorraine was the second Metro-Goldwyn-Mayer production about the French Resistance, the first being Reunion in France, released in 1942. 

==Cast==
 
 
*Jean-Pierre Aumont as Paul
*Gene Kelly as Victor
*Sir Cedric Hardwicke as Father Sebastian
*Richard Whorf as François
*Joseph Calleia as Rodriguez
*Peter Lorre as Sergeant Berger
*Hume Cronyn as Duval
*William Roy as Louis
 
*Tonio Selwart as Major Bruhl Jack Lambert as Jacques
*Wallace Ford as Pierre
*Donald Curtis as Marcel
*Jack Edwards, Jr. as René
*Richard Ryen as Lieutenant Schmidt
*Frederick Giermann as Corporal Daxer
 
==Box office==
According to MGM records the film earned $585,000 in the US and Canada and $663,000 elsewhere resulting in a loss of $179,000. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 