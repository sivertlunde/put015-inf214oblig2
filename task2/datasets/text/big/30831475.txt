Raaj (film)
{{Infobox film
| name           = Raaj
| image          = 
| writer         = V. N. Aditya
| starring       = Sumanth Vimala Raman  Priyamani Koti
| cinematography = 
| producer       = Kumar Brothers
| distributor    = Kumar Brothers Cinema
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         = 
}} Koti is Tamil and Malayalam under the title Maharani - The Beauty Queen. RKD Studios have bought the sole and exclusive rights of the film in Hindi & all other Northern Indian languages.

==Plot==
The story is about a fashion photographer, Raaj (Sumanth), who is caught in between his wife, Mythili, played by  (Priyamani) and his ex-lover, Priya (Vimala Raman). Raaj chose not to tell Mythili about Priya and finds it difficult to adjust with his wife during the early days of marriage. However, their relationship improves with time and they finally get close. Just when things look to settle down, Priya comes back into Raajs life unexpectedly. Hes very curios about why Priya left him abruptly in the past. Priya is later kidnapped by her scorned lover,  played by Ajay. Raaj saves her from his clutches subsequently. In an unexpected twist, it is revealed that Mythili and Priya were actually classmates in school. Raaj later uncovers Priyas ulterior motive, tells Mythili the truth, and reconciles with his wife. 

==Cast==
* Sumanth .... Raaj
* Vimala Raman .... Priya
* Priyamani .... Mythili

== Soundtrack == Sunitha
* "Andamutho Pandemuga" | Siddharth, Malavika
* "Kalakaaduga" | Shashikiran, Anjana Soumya
* "Nanne Nenu Marichipoya" | Deepu, Sravana Bhargavi
* "Prathi Kala Naalo" | Srikrishna, Pranavi
* "Bhimavaram Bulloda" | Srikrishna, Sunitha

==References==
 

 
 
 


 