Angie (1993 film)
{{Infobox film
| name           = Angie 
| image          = Angie93.jpg
| image_size     = 
| caption        = 
| director       = Martin Lagestee
| producer       = Martin Lagestee, San Fu Maltha
| writer         = Martin Lagestee
| narrator       = 
| starring       = Annemarie Röttgering, Derek de Lint, Daniël Boissevain	
| music          =   Clifford Scholten, Joop Koopman
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1993
| runtime        = 93  minutes
| country        = Netherlands Dutch
| budget         = 
}} 1993 Netherlands|Dutch crime thriller film directed by Martin Lagestee.  The film is about a girl named Angie (Annemarie Röttgering) who becomes involved in a life of crime.

==Plot==
After her return from an orphanage with her mother, Angie tries again to build a normal life. The mutual distrust is enormous. After a nasty incident with her mothers new friend Angie turns to her older brother Alex, a delinquent. While Angie is determined to make something of her life, she gets quickly caught up in the criminal world and pulls off a heist with her brother and crew and hit the road.

==Cast==
*Annemarie Röttgering	 ... 	Angie Kempers
*Derek de Lint	... 	Peter Koudbier
*Daniël Boissevain	... 	Alex, Angies older brother
*Hidde Schols	... 	Frank Rothuizen
*Marijke Veugelers	... 	Angies mother
*Peter Tuinman	... 	Detective Burger
*Jack Wouterse	... 	Frits

==References==
 

== External links ==
*  

 
 
 
 
 


 
 