Nenem…Chinna Pillana?
 
 
{{Infobox film
| name = Nenem…Chinna Pillana?
| image = 
| caption = 
| director = P. Sunil Kumar Reddy
| producer = D. Ramanaidu
| writer = Satyanandh    (Dialogues ) 
| narrater =
| story = Balabhadrapatruni Ramani
| starring = Rahul Ravindran Tanvi Vyas Sanjjanaa
| music = M.M.Srilekha
| cinematography = Sabu James
| editing = 
| studio = Suresh Productions
| distributor = 
| released =  
| runtime =
| country = India
| language = Telugu
| budget = 
}}
 Nenem…Chinna Pillana?  (  film directed by P. Sunil Kumar Reddy and produced by D. Ramanaidu under Suresh Productions.  Andala Rakshasi fame Rahul Ravindran  and Tanvi Vyas playing lead roles in this film.    The film was initially titled as Pattudhala later it was changed to Nenem…Chinna Pillana?.   

==Cast==
*Rahul Ravindran as Krish
*Tanvi Vyas as Swapna
*Sanjjanaa
*Sharath Babu Suman
*Raghubabu
*L.B. Sriram
*Kasi Viswanath
*Thagubothu Ramesh
*Jaya Prakash Reddy
*D. Ramanaidu (cameo)

==Production==

===Casting=== Miss India NRI girl in the film.	
 D. Ramanaidu has played a cameo in this film. 

===Filming===
The film was launched on 13 February 2013 in Hyderabad, India|Hyderabad. {{cite web |url= http://www.indiaglitz.com/channels/telugu/article/90938.html title = Ramanaidus Pattudhala starts| publisher= indiaglitz.com |date= 13 February 2013 |accessdate= 14 February 2013    }}  The regular shooting of the film began on 1 April 2013.The first schedule of the film was wrapped in Hyderabad where several scenes and a song were canned on the lead cast.  The film has completed a major schedule in Denmark and Sweden.Where hero’s introduction scenes,two songs and several important scenes were shot. 

==Release==
The film was all set to release on 26 September 2013,  but had to be postponed due to various events that led to advance release of another film Attharintiki Daaredhi. 

== References ==
 

 
 
 
 
 


 