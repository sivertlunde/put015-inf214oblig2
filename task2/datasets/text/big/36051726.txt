Ornette: Made in America
{{Infobox film
| name           = Ornette: Made in America
| caption        = 
| image	         = Poster for Ornette Made in America.jpg
| director       = Shirley Clarke
| producer       = Kathelin Hoffman
| writer         = 
| starring       = Ornette Coleman
| music          = Ornette Coleman
| cinematography = Edward Lachman
| editing        = Shirley Clarke
| distributor    = Milestone Films
| released       =  
| runtime        = 77 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 1984 American Robert Palmer, George Russell, Don Cherry and Denardo Coleman.  
 Convention Center. The opening of the now-defunct Caravan of Dreams nightclub serves as a catalyst for the films production, but Shirley Clarke had actually been working on the film for a span of over 20 years. The 1968 footage with Ornette, his young son, Denardo, and frequent collaborator Charlie Haden was filmed by Clarke for a separate film that never came to fruition. 

Ornette was Shirley Clarkes last film and reintroduced her to the independent film and jazz music circuits that she had influenced during the 1960s. Clarkes intrusive, fast cutting editing style and Kit Fitzgeralds avant-garde music video work won the film praise.  

In late 2012, Milestone Films rereleased Ornette in select theaters and distributed the restored film on DVD and Blu-ray. This is part of a greater effort on behalf of Milestone to rerelease all of Clarkes old films.  

==External links==
* 

==References==
 
 
*
*
*
*

 
 
 
 
 