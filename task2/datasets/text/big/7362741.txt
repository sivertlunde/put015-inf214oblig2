Woman Is the Future of Man
{{Infobox film
| name           = Woman Is the Future of Man
| image          = Woman Is the Future of Man poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =         
| hanja          =  는  의  다
| rr             = Yeojaneun namjaui miraeda
| mr             = Yŏjanŭn namjaŭi miraeda}}
| director       = Hong Sang-soo
| producer       = Lee Han-na Marin Karmitz
| writer         = Hong Sang-soo Kim Tae-woo Sung Hyun-ah
| music          = Jeong Yong-jin
| cinematography = Kim Hyeon-gu
| editing        =  Ham Seong-weon 
| distributor    = CJ Entertainment
| released       =  
| runtime        = 87 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =   (US)      (France) 
}} 2004 Korean South Korean film directed by Hong Sang-soo. The film wasnt a box-office hit, but was entered in the competition category of the 2004 Cannes Film Festival    and received screenings at several other festivals. The title of the film is a translation of a line from a poem by Louis Aragon that the director saw printed on a French postcard. 

==Plot== Kim Tae-woo), a graduate from an American film school who has recently returned to his home country. While having dinner in a restaurant, Kim convinces Lee to arrange a meeting between them and Kims old girlfriend Park Seon-hwa (Sung Hyun-ah). Unbeknownst to Kim, however, Lee had become involved in a relationship with her after Kims departure to the United States. The three meet for a night of drinking, as past tensions and attractions reemerge.  In the end, both self-centered men abandon Park as they had years ago.

==Production==
Woman Is the Future of Man was co-financed by South Korean production companies UniKorea and Miracin Korea, and French producer Marin Karmitzs company MK2.     It was given a production budget of 1.3 million US$.    It was the fifth feature-length film directed by Hong Sang-soo, who also wrote the screenplay.

==Reception==
The film was released in South Korea on May 5, 2004. It didnt enjoy a particularly successful theatrical run; the total attendance figure of 284,872 ranked it 46th out of 75 domestic feature-length releases in the country in 2004.   

The film was screened alongside another South Korean film, Oldboy (2003 film)|Oldboy, at the competition category of the 2004 Cannes Film Festival,  marking the first time that two films from the country were in the competition simultaneously.    Yoo Ji-tae played parts in both films; he had the role of the main antagonist in Oldboy. Unlike Oldboy, Woman Is the Future of Man did not win any of the awards and reportedly met with a mostly unenthusiastic reception.    It was the third film of director Hong Sang-soo to be screened in Cannes, following The Power of Kangwon Province (1998) and Virgin Stripped Bare by Her Bachelors (2000), and the first to be entered in the competition category.  In 2005 Hongs next film, Tale of Cinema, also competed in Cannes.

The film was also screened at the Melbourne International Film Festival, the Vancouver International Film Festival, the New York Film Festival, the London Film Festival and the Pusan International Film Festival.         

Critics gave mixed reviews for the film. Manohla Dargis of The New York Times, reviewing the film during the New York Film Festival, called it "intellectually stimulating   aesthetically bold", and mentioned its suitability for an art-house crowd.  Duane Byrge of The Hollywood Reporter was less positive, calling the film amateurish, a dud compositionally and accused the story of being plodding.    On the review aggregator Metacritic, it has an average score of 64 out of 100 as of March 24, 2008.   

==References==
 

==External links==
*  
*  
*  
*  


 

 
 
 
 
 
 
 