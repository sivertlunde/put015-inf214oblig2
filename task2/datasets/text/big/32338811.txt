The Man to Beat Jack Johnson
 

{{Infobox film
| name           = The Man to Beat Jack Johnson
| image          = TheMantoBeatJackJohnson.jpg
| image_size     = 
| caption        = Screenshot from the film
| director       =  Walter Tyler
| writer         =
| narrator       =
| starring       = Willy Sanders
| music          =
| cinematography =
| editing        =
| studio         = Tyler Film Company
| distributor    =
| released       =  
| runtime        = 3 mins 7 secs
| country        = United Kingdom Silent
| budget         =
}}
 1910 UK|British short black-and-white silent comedy film, produced by the Tyler Film Company, featuring four-year-old Willy Sanders demonstrating his boxing and wrestling skills against an adult opponent. The film, "has the feel of a filmed music hall act (which it may have been)" thanks, according to Michael Brooke of BFI Screenonline, to a, "simple idea (and a slightly disturbing one)," which is, "primitive in its execution." A clip from this film is featured in Paul Mertons interactive guide to early British silent comedy How They Laughed.         

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 