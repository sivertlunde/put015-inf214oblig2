Sikke'n familie
{{Infobox Film
| name           = Sikken familie
| image          = 
| image_size     = 
| caption        = 
| director       = Jon Iversen Alice OFredericks
| producer       = 
| writer         = Ian Hay Jon Iversen Alice OFredericks
| narrator       = 
| starring       = Jørgen Beck
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen Flemming Jensen
| editing        = Wera Iwanouw
| distributor    = ASA Film
| released       = 12 August 1963
| runtime        = 100 minutes
| country        = Denmark
| language       = Danish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Sikken familie is a 1963 Danish family film directed by Jon Iversen and Alice OFredericks.

==Cast==
* Jørgen Beck - Politiinspektør John Stoke
* Bent Børgesen - Ronny Blackmore
* Charlotte Ernst - Caroline Winther
* Judy Gringer - Maggie
* Lone Hertz - Victoria Stoke
* Gunnar Lauring - James Winther
* Ib Mossin - Bill Johnson
* Lisbeth Movin - Kvinde, hos hvem der bliver begået indbrud
* Baard Owe - Peter Winther
* Poul Reichhardt - Sam Jackson
* Jessie Rindom - Ellinor Winther
* Karl Stegger - Pastor Tittaton

==External links==
* 

 

 
 
 
 
 
 
 
 