The Barnyard
{{Infobox film
| name           = The Barnyard
| image          = 
| caption        = 
| director       = Larry Semon
| producer       = Larry Semon
| writer         = Larry Semon
| starring       = Oliver Hardy
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}

The Barnyard is a 1923 film featuring Oliver Hardy and directed by Larry Semon.   

==Cast==
* Larry Semon - Lay Zee, Farm Hand
* Kathleen Myers - The Farmers Daughter
* Oliver Hardy - Farm Hand (as Babe Hardy) Frank Hayes - The Farmers Wife
* Spencer Bell - Helper
* William Hauber
* Al Thompson
* Joe Rock

==See also==
* List of American films of 1923
* Oliver Hardy filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 