Strange Fate
{{Infobox film
| name = Strange Fate
| image =
| image_size =
| caption =
| director = Louis Cuny
| producer = 
| writer =  A. Lacombe (novel)   Marcelle Maurette   Jean Sarment
| narrator =
| starring = Renée Saint-Cyr   Aimé Clariond   Henri Vidal
| music =   
| cinematography =   Léonce-Henri Burel 
| editing =  
| studio = Andre Paulve Film   Celia Film 
| distributor = 
| released =   5 June 1946   
| runtime = 110 minutes
| country = France  French 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Strange Fate (French:Étrange destin) is a 1946 French drama film directed by Louis Cuny and starring Renée Saint-Cyr, Aimé Clariond and Henri Vidal. 

==Cast==
*  Renée Saint-Cyr as Patricia  
* Aimé Clariond as Le professeur Gallois 
* Henri Vidal as Alain de Saulieu  
* Nathalie Nattier as Germaine  
* Denise Grey as Mme dEvremond  
* Robert Favart as Lassistant du professeur Gallois  
* Gabrielle Fontan as Mme Durtain 
* Hélène Bellanger 
* Charlotte Ecard 
* Luce Fabiole 
* Gaston Girard 
* Georges Gosset 
* Marie-Thérèse Moissel 
* Julienne Paroli 
* Marcelle Rexiane
* Roger Rudel 
* Jean-Charles Thibault

== References ==
 

== Bibliography ==
* Cook, Samantha. Writers and production artists. St James Pr, 1993.

== External links ==
*  

 
 
 
 
 
 

 