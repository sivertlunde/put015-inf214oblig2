Ski Patrol (1940 film)
:See also Ski Patrol (disambiguation)

{{Infobox film
| name = Ski Patrol
| image =
| image_size =
| caption =
| director = Lew Landers
| producer = Ben Pivar, Warren Douglas Paul Huston
| narrator =
| starring = Frank Skinner (uncredited)
| cinematography = Milton R. Krasner
| editing = Edward Curtiss
| distributor = Universal Pictures
| released =  
| runtime = 64 min.
| country = United States
| language = English
}}

Ski Patrol is a 1940 American war film directed by Lew Landers, produced by Ben Pivar and Warren Douglas and released by Universal Pictures.
 1936 Olympics, Winter War, and the Finnish heroes defend a snow-laden mountain pass. The plot takes great historical liberties in its storyline. 

== Cast ==
* Philip Dorn as Lt. Viktor Ryder
* Luli Deste as Julia Engel Stanley Fields as Birger Simberg
* Samuel S. Hinds as Capt. Per Vallgren
* Edward Norris as Paavo Luuki
* John Qualen as Gustaf Nerkuu
* Hardie Albright as Tyko Gallen
* John Arledge as Dick Reynolds
* John Ellis as Knut Vallgren Henry Brandon as Jan Sikorsky
* Kathryn Adams Doty as Lissa Ryder
* Leona Roberts as Mother Ryder
* Abner Biberman as Russian Field Commander
* Wade Boteler as German Olympics Spokesman
* Addison Richards as James Burton, speaker
* Reed Hadley as Ivan Dubroski

== External links ==
*  
*  

 

 
 
 
 
 
 
 

 