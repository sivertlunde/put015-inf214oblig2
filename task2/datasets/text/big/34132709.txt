Chann Pardesi
 
{{Infobox film
| name = Chann Pardesee
| image =
| caption =
| director = Chitrartha Singh
| producer = Baldev Gill J. S. Cheema Mrs. Swarn Sedha
| screenplay = Ravinder Peepat
| story = Baldev Gill
| starring = Raj Babbar Rama Vij Kulbhushan Kharbanda Amrish Puri Om Puri
| music = Surinder Kohli
| cinematography = Manmohan Singh
| editing = Subhash Sehgal
| distributor =
| released = 1981
| runtime = 147 mins
| country = India
| awards = National Award Punjabi
| budget =
| gross =
}}

 Punjabi movie released in 1981. It was directed by Chitrarth Singh and had Raj Babbar, Rama Vij, Amrish Puri, Kulbhushan Kharbanda and Om Puri in lead roles. It was the first Punjabi film for Raj Babbar. Chann Pardesee was the first Punjabi movie to receive a National Film Award. It was a big commercial success all over Punjab.  

==Plot==
Harnek Singh (alias Nek) (Kulbhushan Kharbanda) is an honest farm worker from the village who works hard for his employer, Jagirdar Joginder Singh (Amrish Puri). Nek is in love with the village belle, Kammo (Rama Vij), and hopes to marry her after he accumulates enough money and some land. One day the Jagirdar asks his assistant, Tulsi (Om Puri), to send Nek out of the village for about 20–25 days. When Nek returns home, Joginder is pleased with him and allocates six acres of land so that he can be independent and marry Kammo. A delighted Nek marries Kammo and they settle down to a harmonious married lifestyle. Soon Kammo gives birth to a son, Laali (Raj Babbar). It is then that their world is turned upside down when Nek finds out that the child is not his but that of Joginder, who had raped Kammo in his absence. Angered, Nek kills a man named Kanhaiya, is arrested, tried in Court, and sentenced to 20 years in prison. Nek breaks off all ties with Kammo, suspecting her of infidelity.

Years later, grown up Laali is in love with a beautiful girl named Channi (Sunita Dheer), whom he wants to marry. When he tells his mother about this, she refuses to giver her permission and blessings, but instead wants him to marry another village girl Nimmo (Rajni Sharma), whom she approves of. 

Watch what happens when Laali discovers the reason why his mother does not approve of Channi. Laali brought the money to Jagirdar Joginder Singh for Channis wedding as Laali considers her like a sister since they are best friends and Tulsi (Om Puri) was going to take the money away before Laali beat up Tulsi and then Joginder fires at Tulsi as his enemy and the terrible secret (of Joginder being Laalis biological father) comes out before Jagirdar goes to jail before Channis wedding.

==Performance==
All the actors gave superb performance and the film was very realistic. It did not have superfluous songs and diversions that most of the Indian films have.

==Cast==
*Rama Vij ...Kammo
*Raj Babbar ...Laali
*Kulbhushan Kharbanda ...Harnek/Nek
*Amrish Puri ...Jangirdar Joginder Singh
*Om Puri ...Tulsi
*Sushma Seth ...Jassi (Joginder Singhs wife)
*Sunita Dheer ...Channi
*Mehar Mittal ... Pappu
*Rajni Sharma ...Nimmo

== References ==
 

==External links==
*  
* 

 
 
 