A Facebook Romance
 
 music composer and film director Mohydeen Izzat Quandour.

==Plot==
A Jordanian girl, who had run away from home because she refused to marry her cousin (a tradition in Jordan) goes to live with her married sister in New York. After nearly 5 years in America, she meets a Jordanian businessman on Facebook, played by Salar Zarza, and they become very close. She is impressed by photos of his expensive cars and factories and all his talk of success. She begins to dream that maybe this is her man and she should grab him before another girl gets the chance.

==Cast==
* Mohamed Karim as Samir
* Lamitta Frangieh as Lubna
* Mohamed Al Abadi as Abu Akram
* Mona Shehabi as Maha and Lubna’s mother
* Ed Ward as Michael
* Sandra Kawar as Girl friend
* Mohammed Al Fassid as Omar
* Salar Zarza as Waleed
* Nabeel Kony as Father of Lubna

==Reception==
The film won three Angel Film Awards at the 2012 Monaco International Film Festival, for best actor Mohamed Karim,  best supporting actor Ed Ward, and best ensemble cast.
 

==References==
 

==External links==
* 
*  

 
 
 
 

 