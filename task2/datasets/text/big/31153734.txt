Bharya (1962 film)
{{Infobox film
| name           =Bharya
| image          = 
| image size     =
| caption        =
| director       =Kunchacko
| producer       = M Kunchacko
| writer         = Kanam EJ Ponkunnam Varkey
| starring       = Sathyaneshan Nadar|Sathyan, Rajasree Ragini (actress)|Ragini, Kottayam Chellappan, S. P. Pillai, Manavalan Joseph, Nellikkodu Bhaskaran, Bahadoor, Adoor Pankajam
| music          = G. Devarajan
| cinematography = 
| editing        = S. Williams
| distributor    = Udaya
| released       = 20 Dec 1962
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Sathyan and Ragini in the lead roles. It was directed by Kunchacko  based on a novel with the same title by Kanam EJ.

The novel was based on the controversial Thiruvalla Ammalu murder case. Ponkunnam Varkey wrote the dialogues, which became a cult favourite among family audiences, and was subsequently released along with the soundtrack album. This was for the first time in Kerala and second time in South India that the dialogues were released as a separate gramophone record. 

==Cast==
  Sathyan as Benny
*Rajasree as Gracy Ragini as Leela
* Manavalan Joseph
* Bahadoor
* Nellikode Bhaskaran
* S. P. Pillai
* Adoor Pankajam
* Kottayam Chellappan
* Namboori Mathew
* Sadanandan
* Jijo
* Gopalakrishnan
* KS Gopinath
* Piravam Mary
* Baby Seetha
 

==Soundtrack== 
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aadam Aadam || K. J. Yesudas, P Susheela || Vayalar Ramavarma || 
|- 
| 2 || Dayaaparanaaya || K. J. Yesudas || Vayalar Ramavarma || 
|- 
| 3 || Kaanaan Nalla Kinaavukal || S Janaki || Vayalar Ramavarma || 
|- 
| 4 || Lahari Lahari Lahari || A. M. Rajah, Jikki || Vayalar Ramavarma || 
|- 
| 5 || Manassamatham Thannaatte || A. M. Rajah, Jikki || Vayalar Ramavarma || 
|- 
| 6 || Mulkkireedamithenthinu || P Susheela || Vayalar Ramavarma || 
|- 
| 7 || Neelakkuruvee Neeyoru || || Vayalar Ramavarma || 
|- 
| 8 || Omanakkayyil || P Susheela || Vayalar Ramavarma || 
|- 
| 9 || Panchaarappaalumittaayi || K. J. Yesudas, P. Leela, Renuka || Vayalar Ramavarma || 
|- 
| 10 || Periyaare || P Susheela, A. M. Rajah || Vayalar Ramavarma || 
|}

==References==
 

==External links==
* 
* 

 
 
 
 
 


 