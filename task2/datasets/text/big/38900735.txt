Złota Maska (film)
{{Infobox film
| name           = Złota Maska
| image          =
| image_size     = 
| caption        = 
| director       = Jan Fethke
| producer       = 
| writer         = Jan Fethke, Napoleon Sądek
| based on       =  
| narrator       = 
| starring       = Lidia Wysocka, Aleksander Żabczyński, Władysław Walter
| music          = Zygmunt Wiehler
| cinematography = Seweryn Steinwurzel
| editing        = 
| studio = Elektra-Film
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = Poland Polish
| budget         = 
}} Polish for Polish melodrama World War occupied Poland in 1940 in film|1940. The copy which survived has altered credits and subplot removed — scenes with Igo Sym (Nazi collaborator) were cut after the war. 

== Obsada ==
* Lidia Wysocka – Magda Nieczaj
* Aleksander Żabczyński – Ksawery Runicki
* Władysław Walter – Nieczaj, father of Magda and Adela
* Mieczysława Ćwiklińska – Runicka, Ksawerys mother
* Maria Buchwald – Adela Nieczaj
* Irena Wasiutyńska – Mira Borychowska
* Stefan Hnydziński – Biesiadowski
* Józef Orwid – uncle Zaklesiński
* Jerzy Kobusz – Kamionka, the apprentice
* Zofia Wilczyńska – chambermaid
* Leszek Pośpiełowski – count Gucio
* Andrzej Bogucki – baron Wolski
* Aleksander Bogusiński – Jan, the butler
* Feliks Żukowski – Pieczynga, the estate manager
* Janina Krzymuska – Polkowska
* Helena Zarembina – gossiper
* Wanda Orzechowska – Karnicka
* Jadwiga Zaklicka – Miras friend
* Igo Sym – Raszewski

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 
 