Kadavul Paathi Mirugam Paathi
{{Infobox film
| name           = Kadavul Paathi Mirugam Paathi
| image          = Kadavul paathi mirugam paathi.jpg
| alt            =  
| caption        = 
| director       = Raaj Zacharias Suresh 
| producer       = Raj Zacharias
| story          =  Sethu Abhishek Vinod  Surabhi Prabhu Swetha Vijay 
| music          = Rahul Raj
| cinematography = Kishore Mani
| editing        = V. T. Vijayan
| studio         = Celebs & Red Carpet Studios
| distributor    = 
| released       =  
| runtime        = 
| country        = India Tamil
| budget         = 
| gross          = 
}}
Kadavul Paathi Mirugam Paathi ( ) is an Tamil-language action-thriller film directed by Raaj Menon and Suresh. The film produced by Raj Zacharias, also features him in the lead role while Sethu (actor)|Sethu, Abhishek Vinod and Swetha Vijay form the leading cast. Although originally planned with a different ensemble cast in 2011, the project finally materialised in mid 2013 and released on 20 March 2015 to negative reviews.  The original score and soundtrack of the movie were composed by Rahul Raj.

==Cast==
*Raj Zacharias Sethu
*Abhishek
*Swetha Vijay
*Surabhi Prabhu  
*Arjuna in a guest appearance
*Pooja Umashankar in a guest appearance

==Production== Narain and Aadhi were Santhanam amongst others would play supporting roles. Aadhi later stated that he would play a guest appearance, though noted that in October 2011 that Raaj Menon had not contacted him since June about the project.  The film subsequently failed to take off and the project was postponed.

The film re-began work in October 2013 and a teaser trailer was released in 2014 featuring an all-new cast with producer Raj Zacharias playing a leading role. The film added in Suresh to co-direct the film with Raaj Menon, while Rahul Raj replaced Navneeth Sunder as the films composer. Actor Sethu (actor)|Sethu, previously seen in Mynaa (2010), also featured in a prominent role, while debutants Abhishek Vinod ans Swetha Vijay also appeared in the film. An item number featuring actress and supermodel Surabhi Prabhu and actor Arjuna of Kangaroo (film)|Kangaroo fame, was shot in October 2013 and was choreographed by Gayathri Raghuram.  Actress Pooja Umashankar enacted a cameo role in the film, revealing that she had accepted the offer as an ode of friendship to her college friend Raj Zacharias. 

==Music==
{{Infobox album|  
| Name = Kadavul Paathi Mirugam Paathi
| Longtype =
| Type = Soundtrack
| Artist = Rahul Raj
| Released = 21 June 2014
| Recorded = 2014 Feature film soundtrack
| Length = 17:36 Tamil
| Label = Triple V Records
| Producer = Rahul Raj
| Reviews =
| Last album = Mannar Mathai Speaking 2 (2014)
| This album = Kadavul Paathi Mirugam Paathi (2014)
| Next album = Paathshala (Telugu film)|Paathshala (2014)
}} Malayalam and Telugu film Sathyam Cinemas, Chennai. Yugabharathi, Viveka and Karunakaran are the lyricists. Producer Studio Green|K. E. Gnanavel Raja was present during the occasion, along with several other dignitaries.

The soundtrack met with positive reviews on release. Karthik Srinivasan of Milliblog.com commented "The soundtrack’s highlight is Enadhu ulagil, that lays out its Reetigowlai cards right at the outset and goes on to make superb pop-style use of the raaga! Long overdue, good enough debut by Rahul Raj in Tamil."  Vipin Nair of Musicaloud.com too rated the album high, and concluded saying "Commendable Tamil film debut from Malayali composer Rahul Raj. Naan Indru in particular is total chartbuster material!". 

{{Track listing
| extra_column = Singers
| total_length = 17:36
| title1 = "Naan Indru Naan Thaana"
| extra1 = Sooraj Santhosh
| length1 = 4:03
| title2 = "Enadhu Ulagil"
| extra2 = Gayathri Suresh
| length2 = 4:04
| title3 = "Meenamma Meenamma"
| extra3 = Suchitra Karthik
| length3 = 3:53
| title4 = "Beast Rock"
| extra4 = Arjun Sasi, Rahul Raj
| length4 = 3:21
| title5 = "The Dark Theme"
| extra5 = Anna Katharine
| length5 = 2:15
}}


==References==
 

== External links ==
*  

 
 
 