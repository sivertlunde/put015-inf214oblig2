Beyond the Fire
{{Infobox film
| name           = Beyond the Fire
| image          = BeyondTheFireMoviePoster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = Maeve Murphy
| producer       = Dean Silvers Marlen Hect
| writer         = Maeve Murphy 
| starring       = Scot Williams Cara Seymour
| music          = Neel Dhorajiwala Colm ÓSnodaigh
| cinematography = Mattias Nyberg
| editing        = Agnieszka Liggett
| studio         = 
| distributor    = 
| released       =  
| runtime        = 77 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Beyond the Fire is a 2009 British romantic drama film, written and directed by Maeve Murphy and produced by Dean Silvers and Marlen Hecht.

==Plot==
Beyond the Fire tells the story of Sheamy and Katie. Sheamy is a gentle but troubled Irish ex-priest who arrives in London to find his old family friend and mentor Father Brendan. Katie is a warm hearted woman with her own emotional scars. After arriving in London, Sheamy tries to make contact with Father Brendan. As he is not at home when he calls, he contacts the only other person he knows in London, Rory, a distant relative by marriage. Rory is a musician and band member and, after meeting Sheamy at a gig (where Sheamy makes a good first impression on Katie, who manages the band), offers him a bed and introduces him more fully to Katie, his flatmate. There is an immediate and obvious attraction between Sheamy and Katie. The film follows their attempts to form a lasting relationship despite both their pasts continuing to haunt them.

==Cast==
* Scot Williams as Sheamy OBrien 
* Cara Seymour as Katie
* Victoria Aitken as Amy
* Alison Cain as Lisa
* Brett Findlay as Paul

==Awards==
* Winner of Best International Feature at Garden State Film Festival 2010
* Winner of Best UK Feature at London Independent Film Festival 2009
* Official Selection Belfast Film Festival 2009

==TV Broadcasts==
Beyond The Fire had its UK TV premier on BBC 2 on 22 March 2013  as well as being made available on BBC iPlayer and was critically acclaimed by TV critics. It was "Pick of The Day" in the Daily Mail  and Top film Tips "8 Best TV Movies of the Week" on the Sabotage Times.  It also received a 5 star listing from My TV Guide Listing UK. 
Beyond The Fire was also broadcast in the Republic of Ireland on the 7th of April 2010 on TV3 (Ireland)|TV3. 

==References==
 

==External links==
*  

 
 
 
 
 
 