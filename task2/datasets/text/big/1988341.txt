Hairshirt (film)
{{Infobox film
| name           = Hairshirt (aka Too Smooth)
| image          = Hairshirt1998Poster.jpg
| caption        = film poster
| director       = Dean Paras
| producer       = Dean Paras Neve Campbell Christian Campbell
| writer         = Dead Paras
| screenplay     = 
| story          = Dean Paras Nate Tuck Katie Wright
| based on       =  
| starring       = Dean Paras Neve Campbell Katie Wright Rebecca Gayheart Stefan Brogren
| music          = Nathan Barr
| cinematography = Igor Jadue-Lillo
| editing        = John Axelrad Samuel Craven Dean Paras
| studio         = Lunatic Productions Lionsgate
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Hairshirt (released to home video as Too Smooth) is a 1998 romantic comedy film starring Dean Paras, Neve Campbell, Katie Wright, Rebecca Gayheart and Stefan Brogren.

==Plot==
Danny Reilly (Dean Paras) is a self-obsessed man who, after dumping Renee Weber (Neve Campbell), falls in love again with Corey Wells (Katie Wright). But Renee makes it her mission to see that Danny never falls in love again and sets out for attack when he falls for Corey. Who will get the girl when Dannys constantly talking roomie Tim (Stefan Brogren) falls in love with Corey too?

==Cast==
* Dean Paras as Danny Reilly
* Neve Campbell as Renee Weber
* Katie Wright as Corrinne "Corey" Wells
* Rebecca Gayheart as Jennifer Scott
* Stefan Brogren as Timothy "Tim" Wright
* David DeLuise as Peter Angelo
* Adam Carolla as Bruce Greenberg
* Christian Campbell as Adam Lipton Dax Sheppard as Party Vomiter Adam Scott as Bar Fan
* Marley Shelton as Hot Blonde
* Ele Keats as Slapping Girl
* Alfonso Cuaron as Director

==External links==
*  

 
 
 
 
 
 


 