Millennium (film)
 
 
{{Infobox film
| name           = Millennium
| image          = Millennium-movie-1989.jpg
| caption        = DVD cover Michael Anderson
| producer       = John M. Eckert Freddie Fields John C. Foreman Louis M. Silverstein John Varley
| based on       =  
| starring       = Kris Kristofferson Cheryl Ladd Robert Joy Brent Carver Al Waxman Daniel J. Travanti Eric N. Robertson
| cinematography = Rene Ohashi
| editing        = Ron Wisman
| studio         = Gladden Entertainment
| distributor    = 20th Century Fox (theatrical) Artisan Entertainment (DVD)
| released       =  
| runtime        = 108 min.
| country        = United States English
| gross          =  
}} 1989 science Michael Anderson Eric N. Robertson. The film was marketed with the tagline "The people aboard Flight 35 are about to land 1,000 years from where they planned to."
 John Varley. Varley started work on a screenplay based on that short story in 1979, and later released the expanded story in book-length form in 1983, titled Millennium (novel)|Millennium.

== Plot ==
The film begins in the cockpit of a U.S. passenger airliner (Boeing 747), shortly before they are struck from above by another airliner (McDonnell Douglas DC-10) on a landing approach. The pilot handles the airplane as well as he can while the flight engineer goes back to check on the passenger cabin. He comes back in the cockpit screaming, “Theyre dead! All of them! They’re burned up!”
 theoretical physicist named Dr. Arnold Mayer (Travanti) has a professional curiosity about the crash, which borders on science fiction. While giving a lecture, he talks about time travel and the possibility of visitors from the future.

Time travelers are, in fact, visiting the present day and stealing passengers from doomed aircraft. In the future, because of pollution, the human population is no longer able to reproduce, so teams are sent in to the past to abduct groups of people who are about to die and keep them in stasis until they will be sent into the far future to repopulate the Earth. Every incursion into the past causes an accompanying "timequake" whose magnitude is proportional to the effects of the incursion into the past. Each "timequake" causes physical damage in the time from which the incursion has been made. This is why they are abducting people who will not be able to affect the future any further and replacing them with copies of those who would have died. Thus, the co-pilots strange comment came because all the passengers had been replaced with pre-burned duplicates in preparation for the upcoming crash.

 
While on one of the missions, an operative is shot and loses a stun weapon on board a plane before it crashes. This weapon winds up in the possession of Dr. Mayer, setting him on the path to working out whats happening.  Twenty-five years later, Smith finds a similar artifact in the crash portrayed at the beginning of the film.

Worried that these two individuals of the 20th century might change history by their discoveries, Louise Baltimore (Ladd) travels back to 1989 in order to distract Bill Smith and discourage him from pursuing his investigation further. Louise manages to gain Bills trust, as well as seduce him into a one-night stand, which she hopes will complete the distraction. It is later revealed that Louise becomes pregnant in this encounter. However, because of still more errors on the part of the time travel team, as well as paradoxical events, Bill becomes even more suspicious. He soon pays a visit to Dr. Mayer. At that point, Louise materializes from the future and reveals her mission to both of them in the hope that they will voluntarily keep quiet. However, in a mishap with the stun weapon, Mayer kills himself.

Mayer was instrumental in the development of the time travel Gate technology, so his death results in an unresolvable paradox - a force infinity timequake - which will destroy the entire civilization of the "present" future. It is decided that it is time to send all of the people who have been collected to the distant future before the Gate is destroyed. Louise decides to take Bill with her to the future.

Bill and Louise step through and disappear into the Gate, which takes them to another time and another place, in order to save their lives, and to fulfil a destiny to repopulate Earth. A simple, but poignant, message is recited by Sherman the Robot, quoting Winston Churchill, in the closing seconds of the film. As the blast wave of the gate being destroyed vaporizes Sherman he states, "This is not the end. This is not the beginning of the end. It is the end of the beginning" as the scene transitions to the sun rising above the clouds.

== Cast ==
* Kris Kristofferson: Bill Smith
* Cheryl Ladd: Louise Baltimore
* Daniel J. Travanti: Dr. Arnold Mayer
* Robert Joy: Sherman the Robot
* Lloyd Bochner: Walters
* Brent Carver: Coventry
* David McIlwraith: Tom Stanley
* Maury Chaykin: Roger Keane
* Al Waxman: Dr. Brindle
* Lawrence Dane: Captain Vern Rockwell
* Thomas Hauff: First Officer Ron Kennedy
* Peter Dvorsky: Don Janz
* Raymond ONeill: Harold Davis
* Philip Akin: Kevin Briley
* Susannah Hoffman: Susan Melbourne
* Victoria Snow: Pinky Djakarta
* Claudette Roche (as Claudette Roach): Inez Manila
* Bob Bainborough: Investigator Cedric Smith: Eli Seibel
* Edward Roy: Gantry Controller
* Gary Reineke: Carpenter Michael J. Reynolds: Jerry Bannister
* Chapelle Jaffe: Council Chamber Member, Stockholm Christopher Britton: Council Chamber Member, Buffalo Gerry Quigley: Council Chamber Member, Khartoum
* Leonard Chow: Council Chamber Member, Beijing
* Jamie Shannon: Young Bill Smith
* Yank Azman: Evacuation Leader

== Production ==
{{quote box quote = "We had the first meeting on Millennium in 1979. I ended up writing it six times. There were four different directors, and each time a new director came in I went over the whole thing with him and rewrote it. Each new director had his own ideas, and sometimes youd gain something from that, but each time somethings always lost in the process, so that by the time it went in front of the cameras, a lot of the vision was lost." source = Millennium writer John Varley. Interview in St. Louis Post-Dispatch Monday, July 20, 1992  width = 35% align = left
}}
 Around the World in 80 Days, stepped in.  Millenniums production designer, Gene Rudolf, had to produce a future setting that implied putrefaction and atrophy.

 
The largest set was the time-travel center for Louise Baltimores operation. Rudolf created rusted catwalks that traversed a large open space. Buildings crumbled and exposed their infrastructures. The walls were painted dull green, black and coppery. Rudolf wanted the future to look dirty, sick and poisoned.

Several scenes are set in the vault for the decrepit council members overseeing the time travel operation. Rudolf designed their chamber as a semicircle of seven transparent, upright cylinders, each serving as a life-support device. Four of the cylinders held actors. The others were filled with bodily organ props and medical equipment that served as the last still living remnants of these members.

To create the time-travel effects of the Gate itself, cinematographer René Ohashi produced the ghostly shimmering lights by spinning metal wheels covered in Mylar.

Since actual aircraft could not be sent through the set, miniature models and a full-size mock-up of the tail-section of a Boeing 707 were used. Optical effects were used to make the planes look as if they were entering the set.

The penultimate scene took place in a contemporary American home. Rudolfs set was dominated by large horizontal windows. The room was filled with clocks, hourglasses and navigational equipment, in line with Dr. Arnold Mayers fascination with time travel.
 Toronto Lester B. Pearson International Airport, in the former Terminals 1 & 2. For the outdoor shot where Baltimore (Ladd) steals the car, two-way traffic was run in front of the Terminal 2 arrivals level where it is ordinarily a one-way road.

==Reception==
As of July 2014, the film holds a 13% rating on review aggregator Rotten Tomatoes. 

== Alternate endings ==
The original North American theatrical and VHS release of the film features a close-up of Sherman as the gate explodes, followed by a shot of the sun rising over clouds.
  
The International theatrical release  features a much wider shot of the gates explosion, followed by a wormhole/time portal effect.  The scene then dissolves into an underwater shot of the two main characters swimming from above, followed by a view of the characters in a nude, Eden-esque embrace.

The 1999 North American DVD release contains the International version of the ending.  The simpler North American version can also be found on the DVD as a bonus feature on the last page of the Production Notes, as well as is the one featured on Netflix.

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 