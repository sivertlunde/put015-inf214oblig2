This Guy's in Love with U Mare!
{{Infobox film
| name           = This Guys In Love With U Mare!
| image          = This Guys in Love With U Mare!.jpg
| caption        = Theatrical movie poster
| director       = Wenn V. Deramas
| producer       = Charo Santos-Concio Malou Santos Vic R. Del Rosario Jr.
| writer         = Wenn V. Deramas
| starring       =  
| music          = Vincent de Jesus
| cinematography = Elmer Despa	
| screenplay     = Mel Mendoza-Del Rosario Keiko Aquino Wenn V. Deramas
| editing        = Marya Ignacio
| studio         =  
| distributor    = Star Cinema
| released       =  
| runtime        = 115 minutes
| country        = Philippines
| language       =  
| budget         = 
| gross          = PHP 249,105,182  
}}
This Guys In Love With U Mare! is a 2012 Filipino comedy parody film under Star Cinema and Viva Films. It stars Vice Ganda, Luis Manzano and Toni Gonzaga, and it is directed by Wenn V. Deramas. fifth highest-grossing Filipino film of all time.

==Plot==
With three years of a happy relationship, Mike (Luis Manzano) decided to end his relationship with his gay lover, Lester (Vice Ganda), After their break-up, Lester soon finds out that Mike is cheating with him for the last year with a bank accountant Gemma (Toni Gonzaga). This leads to Lester having a plan to destroy Mike and Gemmas relationship, by acting like a real man and thinking that Mike will come back to him. He commanded his four gay friends and also his workers on his parlor, Babushka (IC Mendoza), Ricky (Lassie Marquez), Fanny (Ricky Rivero) and Bambi (Ricci Chan) to pretend as a group of hold-uppers to hold-up Gemma after her shift. Lester pretends to be her "knight and shining armor" and saves her from the "hold-uppers" (his friends). After the rackus, Gemma took Lester home to bring her to her parents (Boboy Garovillo and Carla Martinez). After a dinner with the rest of Gemmas family, Lester went home. He stopped by his parlor and shocked at his friends broken faces (the bruises he left after he saved Gemma from them). After they tried to have their revenge on him, he soon apologized and get together again. Gemma called Mike that same evening, telling him that Mike should meet with Lester in order for him to learn martial arts, which he declines. With his familys suffering for poorness, Mike doesnt know where to get money for his and Gemmas wedding. He proposed to Gemma, which caused Lester to have his plan. 

One day at her job, Lester visits and deposits P5,000,000 on their bank. Gemma and Mike met up again, after seeing the flowers that Lester gave Gemma (Mike doesnt know who Gemmas suitor is) he became extremely upset which caused their argument. Gemma and Lester began dating. One day Gemma was visited by her family to her room, all of them except her mother left the room, which tells her, if she loves Lester, she replied that she was confused and doesnt know what to answer. One day at her job, Lester calls and invites to come to The Dream Concert she really wants to go. After a long and fun night, Mike soon learned that it was Lester who was Gemmas suitor. One day after a tiring game of basketball, Lester got sick and was taken care of by Gemma. Lester learns that Gemma was a good person and wanted to stop what he was doing to her. Mike told Gemma that something is wrong. He then brings Gemma to Lesters house and showed her his room (Lesters room was filled with pictures of Mike), which Gemma became really angry and disappointed. He then broke up with Mike and cancelled their wedding.

Mike apologized to Lester for using him, which Lester accepted. Mike and Lester did everything for Gemma to forgive them, but both of them failed. When Mike was apologizing to Gemma, Gemma taunts Mike that she will jump in the bridge that they were standing to. But soon, Gemma forgives Mike and Lester.

Gemma and Mikes wedding was held at a beach and live happily ever after. Lester found his one true love (the guy that saved him from the bridge where he almost drowned (Sam Milby). But it was only to be revealed that the guy who saved him was a priest and was going to held Gemma and Mikes wedding, which causes Lester for a traumatic experience.

==Cast==

===Main===
*Vice Ganda as Lester
*Toni Gonzaga as Gemma
*Luis Manzano as Mike

===Supporting Cast===
*DJ Durano as David
*Gladys Reyes as Lesters Mom
*Karla Estrada
*Ricci Chan
*Lassie Marquez as Ricky
*IC Mendoza as Babushka
*Ricky Rivero as Fanny
*Cecil Paz as Tancing
*Eagle Riggs
*Manuel Chua as Peter Buboy Garovillo as Daddy
*Carla Martinez
*Edgar Mortiz
*Tessie Tomas
*Jayson Gainza as Nitoy
*Edward Mendez
*Dang Cruz
*Carlo Lacana as Larry
* Via Antonio

===Special Participation===
*Sam Milby
*Aegis
*April Boy Regino

==Background and development==
The film was first announced on April 19, 2012 through   also quoted in an interview that she is excited to do the film with his friends, Ganda and Manzano.  This movie is Toni Gonzagas first movie produced by Viva Films.

Reports told that the film is a "Love triangle|love-triangle" film. 

==Reception==

===Critical reception===
{{Album ratings
| rev1 = Click The City
| rev1Score =   
| rev2 = Young Critics Circle
| rev2Score =   
| rev3 = Pisara
| rev3Score =   
}}

Philbert Ortiz Dy of ClickTheCity gave the film a favorable review.     He states that,"...I have to admit I’m a little astounded by This Guy’s In Love with U, Mare. The sexual politics of the film are really intriguing, and surprisingly progressive for a mainstream release. The film depicts relationships that go far beyond the hetero-normative ideals usually portrayed in mainstream cinema. The movie has plenty of fun with gender roles, confounding larger ideas about what it means to be straight or gay in a modern society. It ends up making a case for a more fluid form of sexuality, one that isn’t limited by prejudices towards femininity or masculinity.".  He also cite some bad sides of the film stating, "...Some of the elements of the formula are still kind of annoying The premise is a little hacky, and the comedy gets old really fast is a lot less pleasing. The comedy still feels stale. Laughs are mostly generated through sped up footage, making fun of people for being ugly and word-for-word recitations of dramatic scenes from other movies. Some of the gags are stretched out for far too long, and that extends the film’s runtime". 

Mark Angelo Ching of PEP.ph stated his review through his personal website.     He stated that "What makes this movie okay is the abundance of smart jokes. There are many sight gags and hidden jokes in many scenes that are played with no hard effort  Vices best scenes happen in the first half, when hes on a revenge streak. When he calms down and the movie gets dramatic, the film gets boring."  The other lead characters, on the other hand, deliver shaky performances. Toni Gonzaga portrays her character to be smart, but then she fails to see Lesters true intent. Luis Manzano plays his character dumb at times, and smart at times, it gets confusing what is right. In fairness, this may not be the two actors fault, but the directors. 

A review from Twitch Film gave the film a poor rating.  Ogg Cruz stated, "It is what it is, a disposable piece of entertainment that does not have the will or courage to reinvent the wheel. As a product of commerce, the film understandably insists on being merely lightweight, parading characters that are mere two-dimensional sketches with either a skewed or an overly simplistic understanding of gender roles and morality. The film manages to be funny within the very same framework that all comedies about self-deprecating gays are funny While it is apparent that there are attempts to blur the borders between genders and relationships, it unfortunately misses the opportunity to actually create discourse out of its premise, to graduate from the decades-old subgenre."    

Skilty Labastilla of the Young Critics Circle Film Desk, on the other hand, lambasted the film, saying that the film is "shrill, unfunny, and disturbing". He adds, "the film’s bigger sins, in my opinion, has little to do with its filmmaking and everything to with its peddling of disturbing messages: 1) To be a real man, one has to be physically violent., 2) Making fun of people whose looks do not meet society’s standards of beauty is completely OK., and 3) Gay men will lust after every young man they see."    

===Box-office===
According to Mico Del Rosario, advertising and promotion manager for Star Cinema, the movie grosses PHP 23 Million on its opening day.  The film grosses P123 Million on its first week. P315 Million as of its 3rd week of showing and still counting.

==Theme song==
*The theme song has been re-edited from the lyrics and the title song "This Guys In Love With You Pare" by Parokya ni Edgar, with the instrumental Call Me Maybe, re-composed.

==References==
 
==External links==
 

 
 
 
 
 
 
 
 
 
 