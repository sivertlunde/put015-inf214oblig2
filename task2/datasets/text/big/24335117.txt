My Name Is Tanino
{{Infobox film
| name           = My Name is Tanino
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Paolo Virzì
| producer       = Vittorio Cecchi Gori  Giovanni Lovatelli Francesco Bruni  Francesco Piccolo  Paolo Virzì
| narrator       = 
| starring       = Corrado Fortuna Rachel McAdams 
| music          = Carlo Virzì
| cinematography = Arnaldo Catinari
| editing        = Jacopo Quadri
| studio         = Cecchi Gori Group  Whizbang Films Inc.
| distributor    = Medusa Film 
| released       =   (Venice Film Festival)
| runtime        = 124 minutes Italy  Canada
| language       = Italian, English
| budget         = 
| gross          = €1,044,026 (Italy)
| preceded_by    = 
| followed_by    = 
}}
 picaresque plot is about Tanino, an Italian liberal arts student who falls in love with a young American tourist he met in Sicily and decides to track her down in the United States.

==Plot==


Gaetano Mendollia, said Tanino, is a boy born in Castelluzzo del Golfo (really Castellammare del Golfo small town in the province of Trapani), a seaside resort in Sicily. But he is also a student of cinematography in Rome, with the ambition to become a director.
He knows Sally, an American girl with whom he had a brief history. At the end of the holiday, Sally must return to Seaport, a town of fantasy of Rhode Island, but forgets her camera and Tanino, with the pretext to give it back, but also to avoid military service, part a few days later for the United States, at night and without warning anyone.
 WASP .
Finally, after the FBI leaks and train rides (on the roof), will arrive to New York. Here he will meet his idol director always Seymour Chinawsky (rival by name and style of Charles Bukowski ), now reduced to poverty, who dies soon after having promised to do a film with him.
Tanino always comes out the winner by the situation because of its ingenuity. 
Has traveled a long journey, but also close, within themselves searching for their own identity and their future.

== Cast ==

*Corrado Fortuna: Tanino Mendolia
*Rachel McAdams: Sally Garfield
*Frank Crudele: Angelo Maria Li Causi
*Licinia Lentini: Marinella, mother of Tanino Mary Long: Santa Li Causi
*Beau Starr: Omobono
*Jessica De Marco: Angelina
*Lori Hallier: Leslie Garfield
*Barry Flatman: Mr. Garfield
*Don Francks: Chinawsky

== Trivia ==

Sally is played by Rachel McAdams who debuted as an international actress in this   Italian film.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 