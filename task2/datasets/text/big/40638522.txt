Aavathum Pennale Azhivathum Pennale
{{Infobox film
| name           = Aavathum Pennale Azhivathum Pennale
| image          = 
| image_size     =
| caption        = 
| director       = Senthilnathan
| producer       = Tamil Fathima
| writer         = K. C. Thangam  (dialogues) 
| story          = A. S. Ibrahim Rowther
| screenplay     = Senthilnathan
| starring       =  
| music          = Bala Bharathi
| cinematography = Rajarajan
| editing        = G. Jeyachandran
| distributor    =
| studio         = Tamilannai Cine Creation
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Tamil
}}
 1996 Tamil Tamil crime Mansoor Ali Khan in lead roles. The film, produced by Tamil Fathima, had musical score by Bala Bharathi and was released on 17 May 1996.  

==Plot==

The film begins with the murder of a reputed heart surgeon Dr. Charles (Vijay Krishnaraj). The next day, Sivalingam (Manivannan) is killed by a mysterious person. His wife Chandra (Jayabharathi) is a corrupt and influential politician who supplies weapons to the terrorists. The mysterious person warns Chandra that he will continue to kill. Antony (C. Arunpandian), an honest police officer, is charged to protect Chandra.

==Cast==

*C. Arunpandian as Antony Mansoor Ali Khan as Prabhakaran
*Jayabharathi as Chandra
*M. N. Nambiar as Deena Dayalan
*Manivannan as Sivalingam Rajashree as Lakshmi
*Periyar Dasan as Amavasai, Prabhakarans father
*Vijayakumari as Prabhakarans mother
*T. N. S. Ashokakumar Vivek as Dhanush
*Balu Anand as Manush
*Vijay Krishnaraj as Dr. Charles
*Peeli Sivam
*Raviraj
*Idichapuli Selvaraj
*Kovai Senthil as Kuppusamy
*Nellai Siva as Sandana Karuppan
*Kullamani
*J. Lalitha
*LIC Narasimhan as Dr. Maari
*Silk Smitha as an item number

==Soundtrack==

{{Infobox album |  
| Name        = Aavathum Pennale Azhivathum Pennale
| Type        = soundtrack
| Artist      = Bala Bharathi
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack |
| Length      = 30:45
| Label       = 
| Producer    = Bala Bharathi
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Bala Bharathi. The soundtrack, released in 1996, features 6 tracks with lyrics written by Piraisoodan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Chinna Chitti || S. P. Balasubrahmanyam  || 5:03
|- 2 || Mavane Magarasanae || K. S. Chithra || 5:02
|- 3 || Mavane Magarasanae || Anuradha Sriram || 5:14
|- 4 || Nillu Nillu || Swarnalatha || 5:04
|- 5 || Mano || 5:01
|- 6 || Uyire Uyir Dheepame || Bala Bharathi || 5:21
|}

==References==
 

 
 
 
 
 