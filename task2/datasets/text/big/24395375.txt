The Vagabond Lover
{{Infobox film
| name           = The Vagabond Lover
| image          = Vagabond.jpg
| image_size     = 
| alt            =
| caption        = Front cover of 2005 DVD release
| director       = Marshall Neilan
| producer       = William LeBaron
| writer         = James Ashmore Creelman
| narrator       =
| starring       = Rudy Vallee Sally Blane Marie Dressler Charles Sellon
| music          = Victor Baravalle (music director) Harry M. Woods (songwriter)
| cinematography = Leo Tover Arthur Roberts
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =   |ref2= }} 	
| runtime        = 65 minutes
| country        =United States
| language       = English
| budget         = $204,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p55 
| gross          = $756,000 
| preceded_by    =
| followed_by    =
}}

The Vagabond Lover (1929 in film|1929) is an American black-and-white, comedy-drama musical film about a small-town boy who finds fame and romance when he joins a dance band. The film was directed by Marshall Neilan, and is based on the novel of the same name, written by James Ashmore Creelman, who also wrote the screenplay.  It marked the debut of Rudy Vallee in his first talking picture, and also starred Sally Blane, Marie Dressler, and Charles Sellon. 
 released wide on December 1. A DVD version was released on March 29, 2005.

==Plot==
Rudy Bronson is a senior in a small college in the Midwest.  While in school, he completes a correspondence course in the saxophone, given by the nationally known Ted Grant.  Bronson and his friends form a band, but have difficulty finding work.  Believing that Grant will help them land professional jobs, the band heads to the Long Island, New York home of Grant.  Once there, they pester Grant for an interview, to the point where Grant leaves his home, along with his manager, to stay in New York City, until Bronson gives up and goes home.

After Grant has left, his next door neighbor, Mrs. Whitehall, grows suspicious of the unknown young men hanging around his house.  Thinking they might be burglars, she calls the police.  Whitehall and her niece, Jean, go over to Grants house to confront Bronson.  Thinking quickly, one of Bronsons friends introduce him as Ted Grant, who Whitehall, despite being neighbors, has never met.  The police are still suspicious, but when Bronson and his band plays for them, they believe he is Grant.  In fact, Whitehall is so impressed, and slightly embarrassed over having called the police, that she hires Bronsons band to play at a charity concert.

As they are waiting for the day of the concert to arrive, Bronson and Jean become romantically involved, and the band becomes relatively successful. However, on the night before the charity event, Jean discovers that Bronson has been impersonating Grant, and while she doesnt go public with her discovery, she is understandably quite upset with Bronsons subterfuge.  However, another socialite does report Bronson to the police, but before he can be arrested, Grant returns and claims credit for discovering Bronson and his band.  The band becomes a great success, and Bronson is reconciled with Jean.

==Cast==
*Rudy Vallee as Rudy Bronson
*Sally Blane as Jean Whitehall
*Marie Dressler as Mrs. Ethel Bertha Whitehall
*Charles Sellon as Officer George C. Tuttle
*Norman Peck as Swiftie
*Danny OShea as Sam
*Edward J. Nugent as Sport (as Eddie Nugent)
*Nella Walker as Mrs. Whittington Todhunter
*Malcolm Waite as Ted Grant
*Alan Roscoe as Grants Manager
*The Connecticut Yankees as Musical Ensemble

==Soundtrack==
* "I Love You, Believe Me, I Love You"
:Music by Ruby Cowan and Phil Boutelje
:Lyrics by Philip Bartholomae
:Played by the Connecticut Yankees
:Sung by Rudy Vallee
* "Im Just a Vagabond Lover"
:Written by Rudy Vallee and Leon Zimmerman
:Sung by Rudy Vallee during the credits
:Also performed by the dance troupe at the benefit
* "Nobodys Sweetheart"
:Music by Billy Meyers and Elmer Schoebel
:Lyrics by Gus Kahn and Ernie Erdman
:Performed by the Connecticut Yankees
* "Georgie Porgie Pudding and Pie"
:Traditional
:Sung by a quartet of young orphans at the benefit
* "If You Were the Only Girl in the World"
:Music by Nat Ayer
:Lyrics by Clifford Grey
:Played by the Connecticut Yankees
:Sung by Rudy Vallee
* "Then Ill Be Reminded of You" Ken Smith
:Lyrics by Edward Heyman
:Performed by the Connecticut Yankees
:Sung by Rudy Vallee
* "A Little Kiss Each Morning (A Little Kiss Each Night)"
:Written by Harry M. Woods
:Performed by the Connecticut Yankees
:Sung by Rudy Vallee
* "Sweetheart, We Need Each Other"
:Music by Harry Tierney
:Performed by the dance troupe at the benefit

==Reception==
Tana Hobart from All Movie Guide described the film positively: "  Classic romantic tale   is fun with Marie Dressler outstanding in her role as the wealthy eccentric."

The film was a hit and made a profit of $335,000,  and was one of four top hits for RKO in 1929.   

Mordaunt Hall of the New York Times, gave it an overall positive review, noting that film, "... relies on fun, tuneful songs and appealing music."  He applauded Rudy Vallees and Marie Dresslers performances, although he did have some negative points regarding the dialogue and was ambivalent regarding some of the acting.   

==Notes==
*Rudy Vallees band, The Connecticut Yankees, also made their film debut in The Vagabond Lover. 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 