Sometimes They Come Back... for More
{{Infobox television film
| name           = Sometimes They Come Back... for More
| image          = Poster of the movie Sometimes They Come Back… for More.jpg
| image_size     = 
| caption        = 
| director       = Daniel Zelik Berk
| producer       = Daniel Zelik Berk
| writer         = Stephen King Adam Grossman Darryl Sollerh 
| narrator       = 
| starring       = Clayton Rohner
| music          = Brian Langsbard
| cinematography = Christopher Walling
| editing        = Todd Clark
| distributor    = Trimark Pictures
| released       =  
| runtime        = 89 min.
| country        = United States
| language       = English
| budget         = 
| preceded_by    = Sometimes They Come Back... Again
}} Sometimes They Come Back. The video was directed by Daniel Zelik Berk and released in 1998.

==Plot==
The U.S. military has a secret illegal mining operation in Antarctica (region)|Antarctica. When one of the personnel stationed at the base goes on a rampage, two military operatives, Cpt Sam Cage (Clayton Rohner) and Maj Callie Wilson (Chase Masterson) drop in to search the base. They discover two survivors, medical officer Cpt Jennifer Wells (Faith Ford) and technical officer Lt Brian Shebanski (Max Perlich). The base radio is mysteriously smashed.

Going into the mining area of the base, Shebanski sees what appears to be another survivor and starts a chase through the corridors, taking an elevator to the second floor. When Wells collapses because of the gases in the mine he takes her to an elevator, only to discover they are actually on the fourth floor. Back in the main compound they discover that a body they had found in the snow has now moved and is gone. They find dead and dying personnel, and a book about conjuring the Devil. More bodies disappear when nobodys looking, and reappear later as lurching menaces. Soon Jennifer and Sam find themselves the only ones still alive, fighting the undead and their diabolical master.

==Cast==
* Clayton Rohner as Captain Sam Cage 
* Faith Ford as Dr. Jennifer Wells 
* Max Perlich as Lieutenant Brian Shebanski 
* Chase Masterson as Major Callie OGrady 
* Damian Chapa as Dr. Carl Schilling 
* Jennifer ODell as Mary 
* Michael Stadvec as Captain Robert Reynolds  Stephen Hart as Major Frank Whittaker 
* Douglas Stoup as Lieutenant Baines 
* Frank Ceglia as Soldier in Bar

==External links==
*  
*  
*  

==See also==
* Project Iceworm - a top-secret United States Army program during the Cold War to build a network of mobile nuclear missile launch sites under the Greenland ice sheet.

 

 
 
 
 
 
 
 
 


 