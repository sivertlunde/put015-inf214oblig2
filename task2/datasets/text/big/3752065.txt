The Three Burials of Melquiades Estrada
{{Infobox film
| name = The Three Burials of Melquiades Estrada
| image = The Three Burials of Melquiades Estrada Poster.png
| image_size = 220px
| alt =
| caption = Theatrical release poster
| director = Tommy Lee Jones
| producer = Luc Besson Michael Fitzgerald Pierre-Ange Le Pogam
| writer = Guillermo Arriaga
| starring = Tommy Lee Jones Barry Pepper Julio Cedillo Dwight Yoakam January Jones
| music = Marco Beltrami
| cinematography = Chris Menges Hector Ortega
| editing = Roberto Silvi
| studio = EuropaCorp Javelina Film Company
| distributor = Sony Pictures Classics
| released =  
| runtime = 121 minutes  
| country = France United States    
| language = English Spanish
| budget = $15 million  . Retrieved 31 July 2008. 
| gross = $9,045,364 
}}
The Three Burials of Melquiades Estrada is a 2005 Mexican-American neo- , Julio Cedillo, Dwight Yoakam, and January Jones.
 Marines during As I Lay Dying by William Faulkner which displays the same plot, promise, and challenges encountered in the movie.

==Plot== flashbacks and sometimes the same event is shown twice from different perspectives.

Melquiades Estrada, a Mexican illegal immigrant working in Texas as a cowboy, shoots at a coyote which is menacing his small flock of goats. A nearby border patrolman, Norton, thinks he is being attacked and shoots back, killing Melquiades. Norton quickly buries Melquiades and does not report anything. Melquiades body is found and is reburied in a local cemetery by the sheriffs office. Evidence that he may have been killed by the U.S. Border Patrol is ignored by the local sheriff, Belmont, who would prefer to avoid trouble with the Border Patrol.

Pete Perkins, a rancher and Melquiades best friend, finds out from a waitress, Rachel, that the killer was Norton. Perkins kidnaps Norton after tying up his wife, Lou Ann, and forces him to dig up Melquiades body. Perkins had promised Melquiades that he would bury him in his home town of Jiménez in Mexico if he died in Texas. Perkins undertakes a journey on horseback into Mexico with the body tied to a mule and his captive Norton in tow. It is clear to Sheriff Belmont that Perkins has kidnapped Norton, and so police officers and the Border Patrol begin to search for them. Belmont sees them heading towards the Mexico border, but as he takes aim at Perkins, he cant bring himself to shoot and returns to town, leaving the pursuit to the border patrol.

On their way across the harsh countryside, the pair experience a series of surreal encounters. They spend an afternoon with an elderly blind American man (Levon Helm), who listens to Mexican radio for company. The man asks to be shot since there is no one left to take care of him. He does not want to commit suicide because, he argues, doing so would offend God. Perkins refuses as, if he killed the man, he would offend God. Norton attempts to escape and is bitten by a rattlesnake and eventually discovered by a group of illegal immigrants crossing into Texas. Perkins gives one of them a horse as barter payment for guiding them across the river to an herbal healer. She turns out to be a woman whose nose Norton had broken when he punched her in the face a few weeks previously during an arrest. At Perkinss request, she saves Nortons life before exacting her revenge by breaking Nortons nose with a coffee pot.

The captivity, the tiring journey, and the rotting corpse slowly take a profound psychological toll on Norton. At one point the duo encounter a group of Mexican cowboys watching American soap operas on a television hooked up to their pickup truck. The program is the same episode that was airing when Norton had sex with his wife in their trailer earlier in the movie. Norton is visibly shaken and is given half a bottle of liquor by one of the cowboys. Nortons wife is shown as she decides to leave the border town to return to her home town of Cincinnati. She has grown distant from her husband and seems unconcerned about his kidnapping, stating that he is "beyond redemption."

Perkins and Norton then arrive at a town that is supposed to be near Jiménez — the town Melquiades Estrada claimed was his home. No one in the town has heard of Jiménez. Perkins has some luck in locating a woman Melquiades indicated was his wife but, when Perkins confronts her, she states that she has never heard of Melquiades Estrada and lives in town with her husband and children. She does visibly react to Estradas Polaroid photograph Perkins shows her of Melquiades standing behind her and her children, stating that she does "...not want to get in trouble with her husband." Perkins continues onward searching for Melquiades descriptions of a place "filled with beauty." Eventually they come upon a ruined house which Perkins feels was the one Melquiades had mentioned. Perkins and Norton repair the walls, construct a new roof and bury Estrada for the third and final time.

Perkins then demands that Norton beg forgiveness for the killing, but Norton responds with obstinacy. Perkins fires several shots from his pistol around Norton until he breaks down and relents, begging forgiveness from Melquiades. Perkins accepts his outpouring of grief and leaves Norton a horse, and in passing calls him "son." As Perkins rides away, Norton calls out and asks him if he will be okay, suggesting that Norton may have found the redemption his wife had felt he was incapable of having.

==Cast==
* Tommy Lee Jones as Pete Perkins
* Barry Pepper as Ptmn. Mike Norton
* Julio Cedillo as Melquiades Estrada
* Dwight Yoakam as Sheriff Belmont
* January Jones as Lou Ann Norton
* Melissa Leo as Rachel
* Vanessa Bauche as Mariana
* Levon Helm as Old Man with Radio
* Mel Rodriguez as Captain Gómez
* Cecilia Suárez as Rosa
* Ignacio Guadalupe as Lucio

==Production==
  Big Bend Van Horn, and Redford, Texas|Redford, all in Texas.

==Reception==
The film received generally positive reviews; it currently holds an 87% rating at   directorial debut is both a potent western and a powerful morality tale." 

==Awards and nominations== Cannes Film Festival   
 Best Actor - Tommy Lee Jones Best Screenplay - Guillermo Arriaga
* Nominated: Golden Palm - Tommy Lee Jones

Belgian Syndicate of Cinema Critics Grand Prix

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at The Numbers
*   Movie: A Journal of Film Criticism, No.1, 2010

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 