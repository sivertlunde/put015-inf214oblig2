Victor Frankenstein (film)
{{Infobox film
| name = Victor Frankenstein
| image     = 
| border    = 
| alt       = 
| caption   =  Paul McGuigan John Davis
| writer    = Max Landis
| based on  =  
| starring  = James McAvoy Daniel Radcliffe Jessica Brown Findlay Craig Armstrong
| cinematography = Fabian Wagner
| editing   =
| studio    = Davis Entertainment
| distributor  = 20th Century Fox
| released =  
| runtime  =
| country  = United States language = English
| budget   =
| gross    =
}} Paul McGuigan Igor and James McAvoy as the scientist Victor Frankenstein. The film is scheduled to be released by 20th Century Fox on October 2, 2015.

==Plot==
Told from Igors perspective, we see the troubled young assistants dark origins, his redemptive friendship with the young medical student Victor Von Frankenstein, and become eyewitnesses to the emergence of how Frankenstein became the man who created the legend we know today. 

==Cast==
*James McAvoy as Victor Frankenstein    Igor 
*Jessica Brown Findlay as Lorelei    Andrew Scott as Roderick Turpin 
*Mark Gatiss as Dettweiler 
*Callum Turner as Alistair 

==Production== Paul McGuigan would direct the film.  Daniel Radcliffe also began talks to join the film that month and officially joined the cast as Igor in March 2013.  In July 2013, James McAvoy joined the cast to play Victor Frankenstein.  In September 2013, Jessica Brown Findlay joined the cast of the film. 
 Longcross and Twickenham Film Studios and location shooting at Chatham Historic Dockyard.  Principal photography began on November 25, 2013, and ended on March 20, 2014.  

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 

 