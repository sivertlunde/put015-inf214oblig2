The Lottery
 

"The Lottery" is a short story written by Shirley Jackson on June 1948 and first published in the June 26, 1948 issue of The New Yorker.  The story describes a small town in contemporary America which has an annual ritual known as "the lottery". It has been described as "one of the most famous short stories in the history of American literature," as well as  being described as "a chilling tale of conformity gone mad." 

Response to the story was negative, surprising Jackson and The New Yorker. Readers canceled subscriptions and sent hate mail throughout the summer.  The story was banned in the Union of South Africa.  Since then, it has been accepted as a classic American short story, subject to critical interpretations and media adaptations, and it has been taught in middle schools and high schools for decades since its publication.

== Plot==
Details of contemporary small-town American life are embroidered upon a description of an annual ritual known as "the lottery".  In a small village of about 300 residents, the locals are in an excited yet nervous mood on June 27. Children gather stones as the adult townsfolk assemble for their annual event, which in the local tradition is practiced to ensure a good harvest (one character quotes an old proverb: "Lottery in June, corn be heavy soon"), though there are some rumors that nearby communities are talking about giving up the lottery.
 stoned to death while she bemoans the unfairness of the situation.

==Reception==

=== Readers ===
Many readers demanded an explanation of the situation in the story, and a month after the initial publication, Shirley Jackson responded in the San Francisco Chronicle (July 22, 1948):

 

Jackson lived in North Bennington, Vermont, and her comment reveals that she had Bennington in mind when she wrote "The Lottery."  In a 1960 lecture (printed in her 1968 collection, Come Along with Me), Jackson recalled the hate mail she received in 1948:

 

The New Yorker kept no records of the phone calls, but letters addressed to Jackson were forwarded to her. That summer she regularly took home 10 to 12 forwarded letters each day. She also received weekly packages from The New Yorker containing letters and questions addressed to the magazine or editor Harold Ross, plus carbon copies of the magazines responses mailed to letter writers.

 

===Critical interpretations===
Helen E. Nebekers essay,  The Lottery: Symbolic Tour de Force", in American Literature (March 1974), claims that every major name in the story has a special significance.
 

Fritz Oehlshlaeger, in "The Stoning of Mistress Hutchinson Meaning of Context in The Lottery  (Essays in Literature, 1988), wrote: 

The 1992 episode of The Simpsons, "Dog of Death", features a scene referencing "The Lottery". During the peak of the lottery fever in Springfield, news anchor Kent Brockman announces on television that people hoping to get tips on how to win the jackpot have borrowed every available copy of Shirley Jacksons book The Lottery at the local library. One of them is Homer Simpson|Homer, who throws the book into the fireplace after Brockman reveals that, "Of course, the book does not contain any hints on how to win the lottery. It is, rather, a chilling tale of conformity gone mad."    In her book Shirley Jackson: Essays on the Literary Legacy, Bernice Murphy comments that this scene displays some of the most contradictory things about Jackson: "It says a lot about the visibility of Jacksons most notorious tale that more than 50 years after its initial creation it is still famous enough to warrant a mention in the worlds most famous sitcom. The fact that Springfields citizenry also miss the point of Jacksons story completely   can perhaps be seen as an indication of a more general misrepresentation of Jackson and her work." 

== Dramatizations ==
In addition to numerous reprints in magazines, anthologies and textbooks, "The Lottery" has been adapted for radio, live television, a 1953 ballet, films in 1969 and 1997, a TV movie, an opera, and a one-act play by Thomas Martin.

===1951 radio version===
 .  Writer Ernest Kinoy        expanded the plot to include scenes at various characters  homes before the lottery and a conversation between Bill and Tessie Hutchinson (Bill suggests leaving town before the lottery happens, but Tessie refuses because she wants to go shopping at Floyd Summerss store after the lottery is over). Kinoy deleted certain characters, including two of the Hutchinsons three children, and added at least one character, John Gunderson, a schoolteacher who publicly objects to the lottery being held, and at first refuses to draw. Finally, Kinoy included an ending scene describing the townspeoples post-lottery activities, and an afterword in which the narrator suggested, "Next year, maybe there wont be a Lottery. Its up to all of us. Chances are, there will be, though."  The production was directed by Andrew C. Love.    

=== Television adaptation ===
Ellen M. Violett wrote the first television adaptation, seen on Albert McCleerys Cameo Theatre (1950–55).

=== 1969 film ===
Larry Yusts short film, The Lottery (1969), produced as part of Encyclopædia Britannicas Short Story Showcase series, was ranked by the Academic Film Archive "as one of the two bestselling educational films ever." It has an accompanying ten-minute commentary film, Discussion of "The Lottery" by University of Southern California English professor Dr. James Durbin. Featuring the film debut of Ed Begley, Jr., Yusts adaptation has an atmosphere of naturalism and small town authenticity with its shots of Pickup truck|pick-up trucks and townspeople in Fellows, California. 

=== 1996 TV film ===
Anthony Spinners feature-length TV film, The Lottery, which premiered September 29, 1996, on NBC, is a sequel loosely based on the original Shirley Jackson story.  It was nominated for a 1997 Saturn Award for Best Single Genre Television Presentation.

== References ==
 

== Sources ==
*  .

== External links ==
*  
*  
*  &nbsp;– analysis, themes, quotes, multimedia for students and teachers

;Audio
*  
*  
*  
*  

;Video
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 