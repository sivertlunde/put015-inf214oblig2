The New Pupil
{{Infobox film
| name           = The New Pupil
| image          =
| image_size     =
| caption        = Edward Cahn
| producer       = Jack Chertok Richard Goldstone for MGM
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Clyde DeVinna
| editing        = Adrienne Fazan MGM
| released       =  
| runtime        = 10 50"
| country        = United States
| language       = English
| budget         =
}}
 short comedy Edward Cahn. It was the 190th Our Gang short that was released.

==Plot==
Sally (Juanita Quigley) is a new student at the school that the gang goes to. Upon Sallys arrival, Alfalfa and Spanky literally fall over each other trying to get her attention, leaving the gangs traditional sweetheart Darla in the lurch. But when it turns out that Sally cannot stand either one of the boys, she and Darla cook up a scheme to dampen their romantic aspirations.   

==Notes and reception==
*This film was released before Bubbling Troubles but produced after.
*The New Pupil marked the debut of Billy Laughlin who would play the character "Froggy". Harold "Slim" Switzer. At 14 years of age, he was the oldest member of the cast.

==Cast==
===The Gang===
* Darla Hood as Darla Hood
* George McFarland as Spanky
* Carl Switzer as Alfalfa Mickey Gubitosi (later known as Robert Blake) as Mickey
* Billie Thomas as Buckwheat

===Additional cast===
* Juanita Quigley as Sally Stevens
* Billy Laughlin as Harold
* Patsy Currier as Darlas friend
* May McAvoy as Sallys mother
* Anne ONeal as Teacher
* Joe "Corky" Geil as Classroom extra
* Giovanna Gubitosi as Classroom extra
* Paul Hilton as Classroom extra
* Darwood Kaye as Classroom extra
* Tommy McFarland as Classroom extra
* Harold Switzer as Classroom extra

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 