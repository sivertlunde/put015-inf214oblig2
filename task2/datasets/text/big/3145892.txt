Ditto (2000 film)
{{Infobox film
| name        = Ditto
| image       = Ditto film poster.jpg
| caption     = 
| director    = Kim Jeong-gwon
| producer    = Lee Dong-kueon Jonathan Kim
| writer      = Jang Jin
| starring    = Yoo Ji-tae Kim Ha-neul
| distributor = Hanmac Films
| released    =  
| runtime     = 110 minutes
| country     = South Korea
| language    = Korean
| budget      =  
| film name      = {{Film name
 | hangul      = 동감
 | hanja       =  
 | rr          = Donggam
 | mr          = Tonggam}}
}}
Ditto ( ; lit. "Sympathy" or "The same feeling") is a 2000 South Korean science-fiction romance film directed by Kim Jeong-gwon.  Two students in the same school, one in 1979, the other in 2000, are somehow able to talk to each other using amateur radio.

==Plot==
The film begins with Yoon So-eun (Kim Ha-neul), a student at Silla University eagerly awaiting a certain student Dong-hee (Park Yong-woo) returning to school after serving in the army. She meets him outside the amateur radio club room, where she tells him HAM radio is her hobby, and he thanks her for all the letters she wrote him during the war. Visiting her friend Sunmi-Hur who is recuperating from a broken leg in the hospital, So-eun tells her she is in love with Dong-hee.

At home, one night, she is awakened by a call on her radio, from another radio enthusiast, Ji In (Yoo Ji-tae). They discover that they attend the same university and plan to meet in front of the school clock tower, where Ji In offers to lend So-eun a book for amateur HAM radio operators. At the set time, both wait for the other, So-eun in front of an unfinished clock tower in the heat and dust, and Ji In in front of a completed one, in heavy rain. Later at night, they argue about the why the other person did not show up, and about the weather, and Ji In is suddenly shocked to discover he is speaking over a radio with a disconnected power plug. Further, Ji In, is then jolted with the information which So-eun reveals: she is a Junior in 1979, he is a sophomore in 2000. At first he begins to doubt So-euns words, but is slowly convinced despite the improbability of what is happening to him.

Meanwhile, So-euns relationship with Dong-hee is progressing well, and they end up dating. So-eun talks about her new-found relationship with Sunmi at the hospital, but is not able to spend as much time with her now. Ji In is close to a girl at school Seo Hyeon-ji (Ha Ji-won) whom he takes for granted as she is always hanging around him, but for whom he comes to care for deeply. So-eun is increasingly irritated with the conversations she has with Ji In over the radio, thinking he is lying about living in the year 2000. He then offers her some snippets from her future and his past, which finally make her believe that Ji In has been speaking the truth. So-eun then excitedly asks him about the future, and they also start to confide in each other about their personal lives, Ji In telling So-eun to think of him as a sort of diary that talks. She begins talking about being in love, and Dong-hee.

During some student demonstrations, Dong-hee is injured and So-eun goes to visit him tearfully, and signs her name on the plaster cast on his arm. She also visits Sunmi who is at the same hospital. That night, she talks to Ji In about signing her name on Dong-hee, and how that gave her the feeling that he belongs to her. Ji In suddenly mentions that his parents went to Silla University at the same time as her, and that she might know them, and reveals their names as Sunmi-Hur and Dong-hee. This breaks So-euns world apart, and she runs to the hospital to see the sleeping Dong-hee with Sunmis name now signed on his plaster cast. She goes about in a daze, remembering Sunmi and Dong-hee bonding while they were at the hospital, avoiding Sunmi, who has returned to school from the hospital, and feeling a strange sense of unknown guilt as she dreams about Dong-hee. She has another conversation with Ji In, where he talks about finding out ones true destination, and breaks up with Dong-hee the next day.

Ji In, on a visit to his parents country home, sees pictures of his parents from school, the plaster cast his fathers arm had been in once, the names signed on it and sees So-euns name on there. He probes through the school yearbook of 1979 and is startled with the revelation that the lady he has been speaking with every night, was in love with his own father. The realization strangely disturbs and affects him. He wonders what would happen to him if So-eun chose to be with his father, and sets out to find out how So-eun is faring in his time, the year 2000. He finds her as a professor of English at another university and is touched to see her still single, but beautiful and happy, while So-eun smiles at him with vague recognition as she passes by. He leaves to tell So-eun over the radio that he saw her that day.

However, the radio does not seem to work now, and it is the end of the strange companionship across the barriers of time. Ji In goes back to his world, accepting his girlfriend Hyeon-ji, and So-eun gets on with her life, having realized Dong-hee was not meant to be her destination.

== Cast ==
From 1979:
* Kim Ha-neul as Yoon So-eun
* Park Yong-woo as Dong-hee
* Lee Seung-min (aka Kim Min-joo) as Hur Sun-mi
From 2000:
* Yoo Ji-tae as Ji In
* Ha Ji-won as Seo Hyeon-ji

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 