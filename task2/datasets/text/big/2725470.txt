The Naked Kiss
{{Infobox film
| name           = The Naked Kiss
| image          = Thenakedkissposter.jpg
| caption        = Theatrical release poster
| alt            =
| director       = Samuel Fuller
| producer       = Samuel Fuller
| writer         = Samuel Fuller
| starring       = Constance Towers Anthony Eisley Michael Dante Virginia Grey
| music          = Paul Dunlap
| cinematography = Stanley Cortez
| editing        = Jerome Thoms
| distributor    = Allied Artists Pictures Corporation
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $200,000 
}}
The Naked Kiss is a 1964 neo-noir film written and directed by Samuel Fuller, starring Constance Towers as Kelly, Anthony Eisley as Captain Griff and Michael Dante as J.L. Grant. 

==Plot==
Kelly (Towers) is a prostitute who shows up in the small town of Grantville, just one more burg in a long string of quick stops on the run after being chased out of the big city by her former pimp. She engages in a quick tryst with local police chief Griff (Eisley), who then tells her to stay out of his town and refers her to a cat-house just across the state line.

Instead, she decides to give up her illicit lifestyle, becoming a nurse at a hospital for handicapped children.  Griff doesnt trust reformed prostitutes, however, and continues trying to run her out of town.

Kelly falls in love with J.L. Grant (Dante), the wealthy scion of the towns founding family, an urbane sophisticate, and Griffs best friend. After a dream-like courtship where even Kellys admission of her past cant deter Grant, the two decide to marry. It is only after Kelly is able to finally convince Griff that she truly loves Grant and has given up prostitution for good that he agrees to be their best man.

Shortly before the wedding, Kelly arrives at Grants mansion, only to find him on the verge of molesting a small girl. As he grinningly tries to persuade her to marry him, arguing that she too is a deviant, the only one who can understand him, and that he loves her, Kelly strikes him with a phone receiver, killing him instantly. Jailed, and under heavy interrogation from Griff, she must convince him and the town that she is telling the truth about Grants death.

As Kelly tries to exonerate herself, one disappointment follows another, and enemies old and new parade through the jailhouse to defame her.  In despair, she is at last able to find Grants victim and prove her innocence.   

==Cast==
* Constance Towers as Kelly
* Anthony Eisley as Capt. Griff
* Michael Dante as J.L. Grant
* Virginia Grey as Candy
* Patsy Kelly as Mac, Head Nurse
* Marie Devereux as Buff
* Karen Conrad as Dusty
* Linda Francis as Rembrandt
* Bill Sampson as Jerry
* Sheila Mintz as Receptionist
* Patricia Gayle as Nurse
* Jean-Michel Michenaud as Kip
* George Spell as Tim
* Christopher Barry as Peanuts
* Patty Robinson as Angel Face
* Edy Williams as Hatrack

==Reception==

===Critical response===
The staff at Variety (magazine)|Variety magazine gave the film and acting a positive review, writing, "Good Samuel Fuller programmer about a prostie trying the straight route, The Naked Kiss is primarily a vehicle for Constance Towers. Hooker angles and sex perversion plot windup are handled with care, alternating with handicapped children good works theme...Towers overall effect is good, director Fuller overcoming his routine script in displaying blonde lookers acting range." 

Critic Jerry Renshaw liked the film and wrote, "The Naked Kiss finds Sam Fullers tabloid sensibilities boiling to the surface, as it dwells on the uncomfortable and taboo subjects of deviancy, prostitution, and small-town sanctimony. In typical Fuller style, its a hard look at a nightmarish world, lurid and absorbing enough to demand that the viewer watch. Its part melodrama, part sensationalism, and part surreal, but above all its absolutely, positively 100% Sam Fuller, with all the nuance and subtlety of a swift kick in the butt." 

Eugene Archer, writing in The New York Times, wrote that The Naked Kiss "has style to burn" and shows that Fuller is "one of the liveliest, most visual-minded and cinematically knowledgeable filmmakers now working in the low-budget Hollywood grist mill", but denounced the plot as "patently absurd" and "sensational nonsense", judging the whole as a "wild little movie". 

==Home media==
A digitally restored version of the film was released on DVD and Blu-ray by The Criterion Collection. The release includes new video interview with star Constance Towers by film historian and filmmaker Charles Dennis, excerpts from a 1983 episode of The South Bank Show dedicated to Samuel Fuller, an interview with Fuller from a 1967 episode of the French television series Cinéastes de notre temps, and an interview with Fuller from a 1987 episode of the French television series Cinéma cinémas. There is also a booklet featuring an essay by critic and poet Robert Polito and excerpts from Fuller’s autobiography, A Third Face: My Tale of Writing, Fighting, and Filmmaking. 

==References==
 

==External links==
*  
*   at DVD Beaver (includes images)
*   essay at the Criterion Collection by Michael Dare
*   essay at the Criterion Collection by Robert Polito
*  
*  

 

 
 
 
 
 
 
 
 
 