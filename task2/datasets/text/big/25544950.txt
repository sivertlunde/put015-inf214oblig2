Gone with the Pope
{{Infobox film
| name           = Gone with the Pope
| image          = Gone with the Pope.jpg
| image_size     = 
| caption        = 
| director       = Duke Mitchell
| producer       = Duke Mitchell Bob Murawski   Sage Stallone   Chris Innis  
| writer         = Duke Mitchell
| narrator       = 
| starring       = Duke Mitchell Peter Milo Jim LoBianco Giorgio Tavolieri
| music          = Duke Mitchell Jeffrey Mitchell
| cinematography = Peter Santoro Robert Leighton Robert Florio Paul Hart   Jody Fedele  
| distributor    = Grindhouse Releasing (USA)
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}}
Gone with the Pope (also known as Kiss the Ring) is a 1976 independent film written, directed and produced by Italian-American crooner-actor Duke Mitchell that was first released in 2010 by Grindhouse Releasing.  

==Plot==
The movie tells the story of four ex-convicts who journey to Rome to attempt to kidnap the Pope, planning to charge a ransom of "a dollar from every Catholic in the world."

==Production==
Gone with the Pope was shot on location in  .    Mitchell was said to have been inspired by Francis Ford Coppolas The Godfather.   

==Release== Egyptian Theatre.  The film is screened at domestic and international film festivals in the United States and Australia. The film was subsequently released on home video on March 24, 2015, as a Blu-ray/DVD combo. 

==References==
 

==Further reading==
*  A fairly rare review of the film from a mainstream newspaper.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 