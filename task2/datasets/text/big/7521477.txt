Betty Boop's May Party
{{Infobox Hollywood cartoon|
| cartoon_name = Betty Boops May Party
| series = Betty Boop
| image = 
| caption =
| director = Dave Fleischer
| story_artist = 
| animator = David Tendlar William Henning 
| voice_actor = Kate Wright 
| musician = Duke Ellington ("It Dont Mean a Thing If It Aint Got That Swing")
| producer = Max Fleischer
| distributor = Paramount Pictures
| release_date = May 12, 1933
| color_process = Black-and-white
| runtime = 7 mins
| movie_language = English
}}

Betty Boops May Party is a 1933 Fleischer Studios animated short film starring Betty Boop, and featuring Koko the Clown and Bimbo (Fleischer)|Bimbo.

==Plot==

An elephant punctures a rubber tree, whose spraying sap turns the whole town rubbery.  Betty and the gang use their newfound limberness to dance and sing.

==Notes and comments==
Betty sings "Here We Are", written by Harry Warren and Gus Kahn.

==External links==
* 
* 
 

 
 
 
 
 


 