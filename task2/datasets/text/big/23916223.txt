Darkdrive
{{multiple issues|
 
 
}}

{{Infobox Film
| name = Darkdrive
| image = 
| caption = 
| director = Phillip J. Roth    
| producer = 
| writer = Alec Carlin
| starring = Ken Olandt Claire Stansfield Julie Benz
| music = 
| cinematography = 
| editing = 
| distributor = Leo Home Video
| released = November 1996 (Canada)
| runtime = 
| country =   English
| budget = 
| preceded_by = 
| followed_by = 
}}
Darkdrive is a science fiction movie which premiered in Canada in November 1996 and went straight to DVD in the USA. It stars Ken Olandt and Julie Benz. Darkdrive was the first DVD ever released in United Kingdom.

==Plot==
In the near future, a new law sends criminals to a virtual reality prison built by a shadowy corporation called Zircon based in Seattle. Steven Falcon, the designer of the system, realizes the dangers of it being overloaded and of hackers who try to break in and steal files only to have the prisoners try to break out via that method. When Falcon threatens to quit the operation he becomes marked for murder by his employers who force him to enter the virtual reality/afterlife to find the source of the danger of the system crashing.

==Cast==
*Ken Olandt as Steven Falcon
*Claire Stansfield as R.J. Tilda
*Julie Benz as Julie Falcon
*Gian Carlo Scandiuzzi as Matthew Stolopin
*Brian Faker as Ben
*Brian Finney as Bill
*Marcus Aurelius as Dayton
*Tony Doupe as Thackery
*William Hall, Jr. as the Doorman at Zeaks

==External links==
* 

 

 
 
 
 
 
 
 



 