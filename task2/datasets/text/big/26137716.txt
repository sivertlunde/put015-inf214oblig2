After Five in the Forest Primeval
{{Infobox film
| name           = After Five in the Forest Primeval
| image          = After Five in the Forest Primeval poster.jpg
| alt            = 
| caption        = Promotional poster
| director       = Hans-Christian Schmid
| producer       =  David Howard
| starring       = Franka Potente Axel Milberg Dagmar Manzel Farina Brock
| music          = Rainer Michel
| cinematography = Klaus Eichhammer
| editing        = Hansjörg Weißbrich
| studio         = Arte
| distributor    = Senator Film
| released       =  
| runtime        = 99 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}
After Five in the Forest Primeval ( ), sometimes also known as Its a Jungle Out There, is a 1995 German romantic comedy film directed by Hans-Christian Schmid and starring Franka Potente in her first film role. The film tells the story of a 17-year old girl Anna who, in search of freedom in an urban environment, runs away from home with a boy who has a crush on her. Her parents launch into a search for Anna and, during the process, reminisce about their own search for freedom in their youth. 

The film was intended to be a  , October 29, 2001. Accessed February 10, 2010.   Blaney, Martin.  , Screen International, October 27, 2002. Accessed February 10, 2010  During the festival, the German distributing company Senator Film picked up the film for theatrical distribution  and the film opened in German theaters on April 25, 1996. At the 1996 Bavarian Film Awards, Franka Potente won the award for Best Young Actress.    The film was also nominated for accolades at the 1997 Bavarian Film Awards where it won the award for Best Production. 

==Cast==
* Franka Potente as Anna 
* Axel Milberg as Wolfgang 
* Dagmar Manzel as Karin 
* Farina Brock as Clara 
* Sibylle Canonica as Johanna 
* Peter Ender as Oliver 
* Thomas Schmauser as Simon 
* Johann von Bülow as Nick
* Julia Thurnau as Micks friend

==See also==
* 1995 in film
* Cinema of Germany
*  

==References==
 

==External links==
*  

 
 
 