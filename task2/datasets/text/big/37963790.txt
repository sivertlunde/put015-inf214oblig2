Chakoram
{{Infobox film
| name           = Chakoram
| image          =  |
| caption        = Film Poster
| writer         = A. K. Lohithadas Murali Philomina  Kuthiravattom Pappu  Mamukkoya  Cochin Hanifa  Sudheesh
| director       = M. A. Venu
| producer       = V. V. Babu
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Malayalam
| cinematography = Prathapan
| studio         = Srushti Films International
| distributor    = Chandrakanth Release
| editing        = Hariharaputhran Johnson
| budget         =
}}
 1994 Malayalam Malayalam Romance romantic drama Murali in the lead roles along with Philomina, Kuthiravattom Pappu, Mamukkoya, Cochin Hanifa and Sudheesh in other pivotal roles. The music for the film was composed by Johnson (composer)|Johnson.

Shanthi Krishna won the Kerala State Film Award for Best Actress for her role as Sharadammini, and Venu won the Kerala State Film Award for Best Debut Director. 

==Plot==
The film is about an arrogant and bold spinster, in her late thirties, whose life turns into a meaningful one with the arrival of an ex-army-man in the neighbourhood.

==Cast==
* Shanthi Krishna as Sharadammini Murali as Lance Naik Mukundan Menon
* Philomina as Amminiyamma
* Kuthiravattom Pappu as Ammama
* Mamukkoya as Pookunju
* Cochin Hanifa as Sreedharan Kartha
* Sudheesh as Unni Santhakumari
* Bobby Kottarakkara as Govindankutty
* Ottapalam Pappan

==References==
 

==External links==
* 

 
 
 
 
 
 

 