7½ Phere
{{Infobox film
| name = 7 1/2 Phere: More Than a Wedding
| image = Sadesatphere.jpg
| caption = Movie poster
| director = Ishaan Trivedi
| writer = Ishaan Trivedi Irfan Khan Manoj Pahwa Neena Kulkarni Anang Desai  Ninad Kamat Vallabh Vyas Manini De Chahat Khanna
| music = Shantanu Moitra
| producer = Modawal Harshavardhan Sonal Malhotra Modawal Nimit Modawal
| cinematography = Attar Singh Saini
| studio  = Epitome Entertainment Pvt. Ltd.
| released =  
| runtime = 136 minutes Hindi
| country = India
| budget =
}} Irfan Khan in lead roles. The film was released in India on 29 July 2005 with mixed response. 

    

== Plot==
After having tasted great success with soaps based on traditional families, a national TV network is getting into the next phase of programming - that is Reality TV so they decide to go for the evergreen subject of marriages in Indian family. Channel asks their blue-eyed guy to produce the show. Asmi Ganatra (Juhi Chawla), the first time director, and her team finds out that at present the only family in Mumbai that meets the programming brief is Joshis.

Joshis are stunned when Asmi visits them with a request to cover the marriage for the TV network. Though the brides father is willing to go along with the idea, but familys Mafia, consisting of old guard plays the card of familys pride, they Veto down the proposal. In the meanwhile, Asmi has discovered that Manoj Joshi (Irrfan), brides youngest uncle, has fallen for her.

Knowing very well that her career would be blocked if she is unable to pull off the deal with the brides family, she lures him to an arrangement where a multi camera set-up is secretly installed in the huge house.

Manoj realizes to his horror that he has opened a Pandora box. Unaware of hidden cameras, his family behaves true to themselves and slowly skeletons start tumbling out of the cupboard. The privacy of Joshis family gets invaded by Candid cameras and incidents which should remain buried comes out in open and gives the Channel all sorts of maal masala (juicy tid-bits) to increase their TRP Ratings.

== Cast ==
 
* Juhi Chawla as Asmi Ganatra
* Irrfan Khan as Manoj
* Manoj Pahwa as Inspector Rohit Kande Nina Kulkarni as  Rati Pant 
* Anang Desai as Suresh Joshi
* Shri Vallabh Vyas as Mahesh Taoji Joshi Manini De as Raveena Joshi  
* Chahat Khanna as Piya S. Joshi
* Manish Chaudhary as Nimit Joshi
* Ninad Kamat Shauni Khanna
* Rushad Rana
* Ashok Banthia
 

==Reception==
Hindustan Times praised the film, calling it light-hearted and rib-tickling. They noted it as the first time that actors Juhi Chawla and Irrfan Khan had worked together in a film, and as being both Irrfans entry into and Juhis return to the comedy genre. They advised that the chemistry between the two actors was to watched and appreciate as their "conflict and resolution form an integral part of the film."   

==Soundtrack==
{| border="2" cellpadding="5" cellspacing="0" style="margin: 1em 1em 1em 0; background: ddd; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song Name !! Singer !!  Composer !!  Lyrics !! Notes
|- 1 ||"Aao Nee Kudiyon"|| Sunidhi Chauhan || Shantanu Moitra || Subrat Sinha ||
|- 2 ||"Kyun Aaj Kaa"|| Antara Choudhary || Shantanu Moitra || Subrat Sinha || 
|- 3 ||"Aaja Soneya"|| Shazia Manzoor || Bally Jagpat || Shazia Manzoor ||
|- 4 ||"Aika Dhondiba"|| Pronali Chaliha || Shantanu Moitra || Manrel Gaikwad ||
|- 5 ||"Jiya Dil Se, 1998
|- 6 ||"Dil Dil Se, 1998
|- 7 ||"Tera Saath Hai Kitna Pyare"|| Babul Supriyo, Anupama Deshpande || Kalyanji Anandji || Indivar || Originally from movie Janbaaz, 1986
|- 8 ||"Kyun Aaj Kaa"|| Hema Sardesai || Kalyanji Anandji || Indivar || Originally from movie Don (1978 film)|Don, 1978
|-
|}

==References==
 

== External links ==
*  

 
 
 
 


 