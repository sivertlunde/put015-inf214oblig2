At War in the Diamond Fields
{{Infobox film
| name           = At War in the Diamond Fields  
| image          = 
| image_size     = 
| caption        = 
| director       = Hans Schomburgk 
| producer       = Hans Schomburgk
| writer         = Hans Schomburgk
| narrator       = 
| starring       = Oskar Marion   Meg Gehrts-Schomburgk   Willy Kaiser-Hey   Magnus Stifter
| music          = 
| editing        =
| cinematography = August Brückner
| studio         = Übersee-Film
| distributor    = Terra Film
| released       = 13 August 1921
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent adventure film directed by Hans Schomburgk and starring Oskar Marion, Meg Gehrts-Schomburgk and Willy Kaiser-Heyl. It premiered on 13 August 1921. 

==Cast==
* Oskar Marion   
* Meg Gehrts-Schomburgk 
* Willy Kaiser-Heyl   
* Magnus Stifter   
* Pietro Bruce   
* Hedda Forsten   
* Madge Jackson  
* Walter von Allwoerden   
* Wolfgang von Schwindt

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic.Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 
 


 
 