Colombiana
 
{{Infobox film
| name            = Colombiana
| image           = Colombiana.jpg
| image_size      = 215px
| alt             = A woman holding a gun in two hands, as if in prayer. 
| caption         = Theatrical release poster
| director        = Olivier Megaton
| producer        = Luc Besson Pierre-Ange Le Pogam
| screenplay      = Luc Besson Robert Mark Kamen
| starring        = Zoe Saldana Michael Vartan Cliff Curtis Lennie James Callum Blue Jordi Mollà
| music           = Nathaniel Méchaly
| cinematography  = Romain Lacourbas
| editing         = Camille Delamarre
| studio          = EuropaCorp Entertainment Film  
| released        =  
| runtime         = 108 minutes 
| country         = France United States
| language        = English Spanish
| budget          = $40 million  
| gross           = $61 million   
}}
Colombiana is a 2011 American action film, co-written and produced by Luc Besson and directed by Olivier Megaton. The film stars Zoe Saldana in the lead role    with supporting roles performed by Michael Vartan, Cliff Curtis, Lennie James, Callum Blue, and Jordi Mollà. A Bengali remake of this movie named Agnee starring Mahiya Mahi, Arefin Shuvo and Misha Sawdagor was released on 14 February 2014. The film set a box office record in Bangladesh.

==Plot==
In 1992,  ) leave to battle Marco and his men but both of them are gunned down as Cataleya listens from the kitchen. Marco tries to manipulate her into giving the information. When he asks what she wants, she stabs him in the hand with a kitchen knife, saying "To kill Don Luis", and escapes. She safely makes it to the U.S. Embassy and gives the Embassy the information in exchange for a passport and passage to the United States. She escapes to the airport, where she makes it to the US and takes a bus to Chicago. Once she finds Emilio, Cataleya asks him to train her as an Assassination|assassin.

Fifteen years later, a 24-year-old Cataleya (Zoe Saldana) has become an accomplished assassin. Her uncle serves as her broker, providing her with contracts. She is assigned to kill the notorious gangster Genarro Rizzo (Affif Ben Badra), who is currently in police custody. Implementing an elaborate plan, she gets herself arrested while dressed in a disguise. She manages to escape from her cell with tools she hid in her disguise, travel through the ventilation system, successfully kill Rizzo, and return to her cell. The next morning she is released.
 FBI Special Special Agent CIA Agent Steve Richard (Callum Blue), concludes correctly that Fabios daughter is in the U.S. and orders Marco (whose hand still hurts from 15 years ago) and some operatives to find her.      

Emilio is devastated when he learns Cataleya has a signature since she has thereby put her relatives in grave danger. Cataleyas newest target is William "Willy" Woogard (Sam Douglas), a millionaire who fled to the Caribbean with $50 million from his Ponzi scheme. She sneaks into his house and shoots him, and he falls inside his shark tank, where the sharks maul him to death.

Cataleya later visits her lover, Danny Delaney (Michael Vartan), and spends the night with him. Danny snaps a picture of her while she sleeps. That morning she meets with her uncle, Emilio, who furiously tells her that eight people were slaughtered in Miami, one of them being his friend, stating "theyre getting too close". Emilio then retires his niece from her work.  

Danny shows her picture to his friend Ryan, but when Danny leaves to stop his car from being ticketed, Ryan forwards it to his sister-in-law, a police clerk, to find out who she is. Now in the police computers, the photo is recognized by the body/morph recognition software as that of the woman who was in the same prison as Genarro Rizzo the night he was killed. Detective Ross is notified, and the FBI quickly trace her location. Supported by a SWAT team they leave for her apartment.

After Cataleya gives her goodbyes to her family she goes home, but gets a call from Danny, who confesses that he took a picture of her. Upon seeing the SWAT team enter the apartment, she manages to escape through the garage and goes to Emilios home, only to discover that Mama, Pepe (Angel Garnica), and Emilio have been tortured and killed by Don Luiss men, leaving her devastated.  

Cataleya ambushes FBI detective Ross in his home in order to find out where Don Luis is. She threatens to kill Rosss family members one by one if he doesnt try harder to help her. Fearing for the safety of his family, Ross meets with Steve Richard, who is unhelpful at first, but after Cataleya fires a warning shot through his "bulletproof" office window, Richard gives up Don Luiss current location. Cataleya then goes to a land surveyor Stanley Smith and threatens him for the floor plans of Don Luis mansion.

Cataleya assaults Don Luiss premises with heavy weaponry and wipes out all the guards, then confronts Marco in a bathroom and, after a violent hand-to-hand battle, stabs him in the neck with a gun slide action. Don Luis escapes by driving off in a van, but is stopped by a garbage truck. Cataleya calls him on Marcos cell phone, but Don Luis laughs and says that he will kill her and she will never find him because he is never where Cataleya wants him to be. Cataleya responds that he is exactly where she wants him to be. Unknowingly, Pepes two attack dogs are right behind Luis seat and they violently maul Luis to death.

Danny is shown being interrogated by the FBI, but when Ross leaves, Danny gets a call from Cataleya, who gives him her real name and they exchange a few words before she hangs up. Ross nearly catches him, but releases him as there is no charge over any crime. Cataleya then boards a bus headed for an unknown destination, leaving her fate undetermined.  

==Main cast==
* Zoe Saldana as Cataleya Restrepo/Valerie Phillips
* Amandla Stenberg as a young Cataleya
* Michael Vartan as Danny Delaney, Cataleyas lover
* Cliff Curtis as Emilio Restrepo, Cataleyas uncle FBI 
* Callum Blue as Steve Richard, CIA Agent
* Beto Benites as Don Luis Sandoval, a drug lord
* Jordi Mollà as Marco, Don Luiss top henchman
* Graham McTavish as Head Marshal Warren, a U.S. Marshal 
* Max Martini as Special Agent Williams, an FBI Agent
* Ofelia Medina as Mama, Cataleyas grandmother
* Jesse Borrego as Fabio Restrepo, Cataleyas father
* Cynthia Addai-Robinson as Alicia Restrepo, Cataleyas mother
* Doug Rao as Michael Shino
* Sam Douglas as William Woogard, a man who absconded with $50 million, one of Cataleyas targets
* Affif Ben Badra as Genarro Rizzo, one of Cataleyas targets

==Production==
Filming began around 20 August 2010 in locations including Chicago, New Orleans, Mexico, and France.  The film was produced by Luc Bessons EuropaCorp company and the script was written by Besson and Robert Mark Kamen.  

==Reception==

===Critical response===
Review aggregation website Rotten Tomatoes gave Colombiana a 26% rating based on 92 reviews, with the sites consensus that "Zoe Saldana has the chops but shes taken out by erratic and sloppy filmmaking." 

Christy Lemire of   blast of bloody blam blam is the latest chapter in the Luc Besson book of badly bruised lovelies who are better not crossed. What he began in 1990 with  , said that "There are guilty pleasures to be had in this frenzied B starring Zoe Saldana, who gives an acrobatic performance that makes the overcooked material watchable."   

===Controversy===
A nonprofit group called PorColombia criticized the film, saying that it stereotyped Colombia in a negative way.    Carlos Macias, president of PorColombia, claimed that the film is proof of a "total lack of creativity" of "Hollywood".  When asked about the situation in an interview, Saldana said "Shame on them? I dont know, I wish I knew how to address stupid unintelligent comments but I dont, Im not a stupid person."  In his review in Senses of Cinema, David Martin-Jones named and analyzed a number of shortcomings. Even so he stated finally that in spite of all its resemblances to Hollywood blockbusters this film provided in comparison at least "a different perspective" concerning immigration and international wealth inequality. 

===Box office=== The Help. The Debt.    The film has made $36,665,854 in United States and Canada, and $24,300,000 internationally countries, bringing its total to $60,965,854 worldwide.

==See also==
 
* Colombia in popular culture

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at Senses of Cinema

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 