Mogudu (film)
{{Infobox film
| name = Mogudu
| image = Poster Mogudu.jpg
| image_size = 200px
| alt =  
| caption = Krishna Vamsi
| producer = Nallamalapu Srinivas (Bujji)
| writer = Bheem (dialogues)
| based on =   Gopichand Taapsee Roja
| music = Babu Shankar 
| cinematography = Srikanth Naroju
| editing = Gowtham Raju
| studio = Lakshmi Narasimha Productions
| distributor =
| released =    
| runtime =
| country = India
| language = Telugu
| budget =
| gross =
}}
 Telugu film Krishna Vamsi Roja and Naresh in Tamil as My Dear Purushan.

==Plot==
Bujji ( Tottempudi Gopichand|Gopichand) is the only son of a wealthy and influential farmer Anjaneya Prasad (Gadde Rajendra Prasad). The family is well knit together and has strong values and attachments. Bujji comes across Raja Rajeshwari (Taapsee Pannu) in unexpected circumstances and falls in love with her. Raja Rajeshwari is the daughter of a powerful politician Chamundeshwari (Roja (actress)|Roja) and Shankar Rao (Naresh). The families agree to the marriage and just when everything seems nice and happy, unexpected circumstances cause a rift between them.
Into this scenario comes Jo (Shraddha Das) who is madly in love with Bujji. This further escalates the tensions. The rest of the movie is about whether Bujji sorts out the differences between the families and wins his wife back.

==Cast==
* Tottempudi Gopichand as Ramprasad (Bujji)
* Taapsee Pannu as Rajeshwari
* Shraddha Das as Jo
* Gadde Rajendra Prasad as Anjaneya Prasad Roja as a politician Chamundeshwari
* Naresh as Shankar Rao
* Krishnudu Venu Madhav
* Ahuti Prasad
* Geethanjali

==Critical reception==
  said "The film is just an above average fare. It is a disappointment for the directors ardent fans who expect much from his films. However praised the lead performances, saying "Gopi Chand looks stylish in his designer wear. He is good in action sequences and shows his talent as a performer. But, it is Rajendra Prasad who steals the show with his stellar performance". 

Deepa Garimella of fullhyd.com praised the films screenplay and the casting, but said that the movie is a "disappointing, unfulfilling anecdote of a newly-wed husband and a wife dealing with an unpleasant family fracas", rating it 5 out of 10. {{cite web|title=Mogudu Review|url=http://www.fullhyderabad.com/profile/movies/4574/2/mogudu-movie-review#tabs
|publisher=fullhyd.com|author=Deepa Garimella|date=6 November 2011}} 

==Audio release==
The film has about eight songs and the music is composed by newcomer Babu Shankar who is from the advertising field and cinematographer Srikanth is also making his debut with this film.  The audio was launched at Rock Heights, Hyderabad for which several film stars attended. D. Suresh Babu unveiled the logo of the film. Rajendra Prasad released the first look trailer of the flick. NTR unveiled the audio CD and presented the first album to VV Vinayak.  

==References==
 

==External links==
*  

 

 
 
 
 
 