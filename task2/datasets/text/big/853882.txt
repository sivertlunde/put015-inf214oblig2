Scoop (novel)
 
 
{{infobox book |  
| name          = Scoop
| title_orig    = 
| translator    = 
| image         =    
| caption = Jacket of the first UK edition
| author        = Evelyn Waugh
| illustrator   = 
| cover_artist  = 
| country       = United Kingdom
| language      = English
| series        = 
| genre         = Novel
| publisher     = 
| release_date  = 1938
| english_release_date =
| media_type    = Print (hardback & paperback)
| pages         =
| isbn          = 
| preceded_by   = A Handful of Dust
| followed_by   = Put Out More Flags
}}

Scoop is a 1938 novel by the English writer Evelyn Waugh, a satire of sensationalist journalism and foreign correspondents.

==Plot== fictional East African state of Ishmaelia to report the crisis there. Lord Copper believes it a very promising little war and proposes to give it fullest publicity. There, despite his total ineptitude, he accidentally manages to get the "scoop" of the title. When he returns, however, credit is diverted to the other Boot, and he is left to return to his bucolic pursuits, much to his relief.

==Background==
The novel is partly based on Waughs own experience working for the Daily Mail, when he was sent to cover Benito Mussolinis expected invasion of Ethiopia|Abyssinia—what was later known as the Second Italo-Abyssinian War (October 1935 to May 1936). When he got his own scoop on the invasion he telegraphed the story back in Latin for secrecy, but they discarded it.  Waugh wrote up his travels more factually in Waugh in Abyssinia (1936), which complements Scoop.
 Lord Northcliffe. Before he died tragically, mentally deranged and attended by nurses, Northcliffe was already exhibiting some of Coppers eccentricities—his megalomania, his habit of giving ridiculous orders to underlings." 

It is widely believed that Waugh based his hapless protagonist, William Boot, on Bill Deedes, a junior reporter who arrived in Addis Ababa aged 22 with "quarter of a ton of baggage".  In his memoir At War with Waugh, Deedes wrote that; "Waugh like most good novelists drew on more than one person for each of his characters. He drew on me for my excessive baggage—and perhaps for my naivety..." He further observed that Waugh was reluctant to acknowledge real life models, so that with Black Mischiefs portrait of a young ruler, "Waugh insisted, as he usually did, that his portrait of Seth, Emperor of Azania, was not drawn from any real person such as Haile Selassie."  According to Peter Stothard, a more direct model for Boot may have been William Beach Thomas, "a quietly successful countryside columnist and literary gent who became a calamitous Daily Mail war correspondent". 

The novel is full of all but identical opposites: Lord Copper of The Beast, Lord Zinc of the Daily Brute (the Daily Mail and Daily Express);  the CumReds and the White Shirts, parodies of Communists (comrades) and Black Shirts (fascists) etc.

Other real-life models for characters (again, according to Deedes): "Jakes is drawn from John Gunther of the Chicago Daily News – In   excerpt, Jakes is found writing, The Archbishop of Canterbury who, it is well known, is behind Imperial Chemicals.. Authentic Gunther."  The most recognisable figure from Fleet Street is Sir Jocelyn Hitchcock, Waughs portrait of Sir Percival Phillips, working then for the Daily Telegraph.  Mrs Stitch is partly based on Lady Diana Cooper    and Mr Baldwin is a combination of Francis Rickett  and Antonin Besse; while Waughs despised Oxford tutor C. R. M. F. Cruttwell makes his customary cameo appearance, as General Cruttwell.

"Feather-footed through the plashy fen passes the questing vole", a line from one of Boots countryside columns, has become a famous comic example of overblown prose style. It inspired the name of the environmentalist magazine Vole (magazine)|Vole, which was originally titled The Questing Vole.

One of the points of the novel is that even if there is little news happening, the worlds media descending upon a place requires that something happen to please their editors and owners back home, and so they will create news. 

==Reception==
 Charles MacArthurs The Front Page." 
 The Observers 100 best English-language novels of the 20th century. 

===Adaptations=== 1987 British TV movie starring Michael Maloney and Denholm Elliott. 
 William Boyd adapted the novel into a screenplay, which was directed by Gavin Millar. It aired on 26 April 1987.

The fictional newspaper in Scoop served as the inspiration for the title of Tina Browns online news source, The Daily Beast. 

In 2009 the novel was serialised and broadcast on BBC Radio 4.

==Notes and references==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 