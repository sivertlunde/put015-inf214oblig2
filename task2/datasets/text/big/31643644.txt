Something Ventured
 
{{Infobox film
| name = Something Ventured
| image = SV poster final2.jpg
| caption = Film Poster
| director = Daniel Geller Dayna Goldfine
| writer = 
| starring =  Paul Holland Molly Davis Daniel Geller Dayna Goldfine Celeste Schaefer Snyder
| music = Laura Karpman
| released =  
| studio = 
| distributor = Zeitgeist Films
| country = United States
| language = English
| runtime = 84 minutes
}}

Something Ventured is a 2011 documentary film investigating the emergence of American venture capitalism in the mid-20th Century. Something Ventured follows the stories of the venture capitalists who worked with entrepreneurs to start and build companies like Apple Inc.|Apple, Intel, Genentech, Cisco, Atari, Tandem Computers|Tandem, and others. Something Ventured is a full-length independent film which includes interviews with prominent American venture capitalists and entrepreneurs of the 1960s, 70s and 80s, as well as archival photography and footage. The film has aired across the US on local PBS stations as well as on public television in Norway. The film is available on iTunes, Netflix, Amazon.com and from Zeitgeist Films.
 Tom Perkins, Jimmy Treybig (founder of Tandem Computers|Tandem), Nolan Bushnell (founder of Atari), Dr. Herbert Boyer (co-founder of Genentech), Mike Markkula (second president/CEO of Apple inc|Apple), Sandy Lerner (co-founder of Cisco), John Morgridge (early CEO of Cisco), and Robert Campbell (founder of what would become Power Point|PowerPoint).

Narrated by author  . Something Ventured’s North American distribution partner is Zeitgeist Films.

== Production ==
Executive Producer: 
Paul Holland and Molly Davis

Director:
Dayna Goldfine and Dan Geller

Producer: 
Dayna Goldfine, Dan Geller and Celeste Schaefer Snyder

Editor: 
Jen Bradwell and Gary Weimberg

Original Music and Score:
Laura Karpman

Narrator:
Po Bronson

Cinematography:
Dan Geller

Sound:
Richard Beggs

== Press ==
*   April 24, 2011. San Francisco Chronicle. 
*   KQED Radio. April 22, 2011.
*   Inc. April 22, 2011.
*   San Francisco Chronicle. April 18, 2011.
*   Fox Business. April 14, 2011.
*   Variety. April 3, 2011.
*   Bloomberg Television. March 18, 2011.
*   Fox Business. March 16, 2011.
*   The Dylan Ratigan Show. March 15, 2011.
*   TechNewsDaily. March 15, 2011.
*   CNBC. March, 14th, 2011.
*   NBC. Austin, TX. March 12, 2011.
*   The Austin Chronicle. March 11, 2011.
*   The New York Times. March 7, 2011.

== External links ==
*  
*  

 
 
 
 
 
 
 