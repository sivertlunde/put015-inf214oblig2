Weekend Getaway
{{Infobox film
| name           = Weekend Getaway
| image size     = 
| image	         = Weekend Getaway poster.jpg
| alt            = 
| caption        = Theatrical Poster
| director       = Desmond Elliot
| producer       =  
| writer         = 
| screenplay     =  
| story          = 
| narrator       = 
| starring       =  
| music          = 
| cinematography = 
| editing        = Victor Ehi-Amedu
| studio         = Emem Isong Productions
| distributor    =  
| released       =   
| runtime        = 
| country        = Nigeria
| language       = English
| budget         =
| gross          =₦22,895,273 (domestic gross) 
}}
Weekend Getaway is a 2012 Nigerian romantic drama film directed by Desmond Elliot, starring Genevieve Nnaji, Uti Nwachukwu, Ini Edo and Ramsey Nouah. It received 11 nominations and eventually won 4 awards at the 2013 Nollywood & African Film Critics Awards (NAFCA).   It also received 2 nominations at the 2013 Best of Nollywood Awards with Alex Ekubo eventually winning the award for Best Actor in a supporting role. The film was a box-office success in Nigerian cinemas generally because of its star-studded cast. 

==Cast==
*Genevieve Nnaji
*Uti Nwachukwu
*Ini Edo
*Ramsey Nouah
*Beverly Naya
*Alexx Ekubo
*Bryan Okwara
*Ekere Nkanga
*Ime Bishop Umoh
*Bobby Obodo
*Uru Eke
*Monalisa Chinda
*Alex Ekubo

==Critical reception==
The film was widely met with negative critical reviews. It has a 20% rating on Nollywood Reinvented, who criticized its originality, story and predictability. 

Wilfred Okiche of YNaija did an extensive review. He commented that while the star quality was there, they didnt have much of a script to work with. He noted also excessive product placements, poor editing, and bad acting. 

NollywoodCritics spoke on the negative correlation of the various story lines and commented "Genevieve is a Secret agent, Ini Edo is a Maid in Manhattan, Monalisa Chinda is a Sugar mummy, Ramsey Noah is from Cinderella Story..."  

Efe Doghudje of 360Nobs on the other hand gave an average rating of a 6 out of 10 stars, calling the film "cute and kind of funny." The reviewer thought the acting was good in some spots but in many ways not believable. Utis "scene with Genevieve which was supposed to be smart, witty, sarcastic and sensual lacked such intensity of acts like Jinx and James Bond (Hallie Berry and Pierce Brosnan) or Mr. & Mrs. Smith, secret service agents (spies whichever suits you) with an eye on the prize." 

==References==
 

 
 
 
 
 
 
 
 