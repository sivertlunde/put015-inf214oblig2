Missing in Action 2: The Beginning
{{Infobox Film
| name           = Missing in Action 2: The Beginning
| image          = Missing in action 2.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Lance Hool
| producer       = Menahem Golan Yoram Globus
| writer         = Arthur Silver Larry Levinson Steve Bing 
| starring = {{Plainlist|
* Chuck Norris
* Soon-Tek Oh
* Steven Williams
* Bennett Ohta
* Cosie Costa
* Joe Michael Terry
}} Brian May
| cinematography = 
| editing        = Mark Conte Marcus Manton
| distributor    = Cannon Films MGM Paramount Pictures  (Viacom Inc.)
| released       =  
| runtime        = 100 min.
| country        = United States
| language       = English
| budget         = $2,410,000 (estimate)
|gross= $10,755,447 (USA)     Andrew Yule, Hollywood a Go-Go: The True Story of the Cannon Film Empire, Sphere Books, 1987 p111 
}} Missing in Action, both of which star Chuck Norris. It was directed by Lance Hool and written by Steve Bing, Larry Levinson and Arthur Silver. 

Missing in Action 2 was filmed at the same time as the first Missing in Action film, and was actually set to be released first before the producers changed their minds. It was followed by a  .

== Plot ==

Ten years before freeing American POWs from a brutal General, Colonel James Braddock (Chuck Norris) was held in a  North Vietnamese POW camp run by sadistic Colonel Yin (Soon-Teck Oh), who forces the POWs to grow opium for a French drug runner named Francois (Pierre Issot), and tries to get Braddock to admit to and sign a long list of war crimes. During his teams time in captivity, they are relentlessly subjected to various forms of humiliating torture, and Braddock being told  that his wife has left him and has remarried. One of Braddocks comrades starts to suffer with malaria, and is injected with what they are told to be antibiotics. They later find out that he was in fact injected with a lethal dose of opium instead. Braddock must escape and rescue his fellow prisoners from Yin.

==Cast==

*Chuck Norris as Col. James Braddock
*Soon-Tek Oh as Col. Yin 
*Steven Williams as Capt. David Nester
*Bennett Ohta as Capt. Ho
*Cosie Costa as Mazilli
*Joe Michael Terry as Opelka
*John Wesley as Frankie
*David Chung as Dou Chou
*Sergio Kato as Stunt Double

== Reception == 
The LA Times gave it a mixed reception but very positive to the director calling his direction as intensely megalomaniac.
The movie gained a negative reception. 

== Box office ==
The movie opened at #3, grossing $3,868,515 in the opening weekend. It was released in 1,336 theaters for a $2,895 average. The opening week grossing accounted for 36% of its total grossing.  The total US market revenue is $10,755,447. 

== References ==
 

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 
 