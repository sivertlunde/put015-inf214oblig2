Caloola, or The Adventures of a Jackeroo
 
 
{{Infobox film
  | name     = Caloola, or The Adventures of a Jackeroo
  | image    = 
  | caption  =  Alfred Rolfe		
  | producer = 
  | writer   = 
  | based on = novel Caloola by Clement Pratt 
  | starring = 
  | music    = 
  | cinematography = 
  | editing  = 
  | studio = Australian Photo-Play Company
  | distributor = 
  | released = 20 November 1911
  | runtime  = 2,000 feet 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = 
  }}
 Alfred Rolfe based on a novel published the previous year by Clement Pratt.  

It is considered a lost film. 

==Plot==
An Englishman, Charlie Hargreaves, is falsely accused of an embezzlement and goes to Australia, where he finds work as a jackeroo at Caloola Station. He falls in love with Hilda, the station owners daughter, but they are both captured by aboriginals.

The girls parents arrange a search party and come to the rescue, but the chief of the tribe takes the girl. He is about to throw her over a cliff when the jackeroo comes to the rescue. He encounters a bushfire and manages to escape death in a watery grave.  

==Cast== Charles Villiers

==Release==
The bushfire sequence was heavily promoted in advertising. 

==References==
 

==External links==
*  
*  at AustLit
*  at AustLit
 

 
 
 
 
 
 


 