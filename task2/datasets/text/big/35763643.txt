Spoiled (film)
 
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Spoiled
| image          = SpOILed - the movie POSTER oil addiction energy movie.jpg
| caption        = poster
| alt            = The film poster shows a man walking past a Pumpjack, to the right of the poster, holding a empty red gas can. The middle has the films name and tagline, and the bottom contains a list of the directors previous works, as well as the films credits, rating, and release date.
| director       = Mark Mathis
| producer       = Mark Mathis
| writer         = Kevin Miller
| starring       = Mark Mathis
| music          = Yuca, Stephen Easterling, Andy Gabrys
| cinematography = Ben Huddleston, Nathan Frankowski
| editing        = Steve Plamisano
| studio         = WestWave Films
| distributor    = WestWave Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Spoiled (stylized as spOILed) is a 2011 documentary film about energy myths, environmental issues with energy, the problems with alternative energy, and the global warming controversy associated with energy. Spoiled also discusses wind energy, solar energy, fossil fuels, green energy, alternative energy, and sustainable energy as a part of its renewable energy documentary scope. 

==Synopsis== investigating energy myths by asking if people today believe we are "addicted" to oil and if oil is destroying our lives the way other addictions do. The energy movie documentary explores human relationship to oil addiction, and reveals a pattern of misinformation, disinformation, and deception about energy myths.
The renewable energy documentary investigates what is likely to be the greatest challenge ever faced by humanity; that of trying to run the modern world on less oil addiction.

==Cast==
*Mark Mathis

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 