The Merry Monahans
 
{{Infobox film
| name           = The Merry Monahans
| image          =
| caption        =
| director       = Charles Lamont
| producer       = 
| writer         = 
| starring       = 
| music          = Hans J. Salter
| cinematography = Charles Van Enger
| editing        = 
| distributor    = Universal Pictures
| released       = 15 September 1944
| runtime        = 91 min.
| country        = United States
| language       = English 
| budget         =
}}

The Merry Monahans is a 1944 in film|1944, United States|American, black-and-white film starring Donald OConnor, Peggy Ryan, and Jack Oakie.

The story is of a vaudeville family trying to make money through hard times. The film features the great song and dance duet with OConnor and Ryan, "I Hate To Lose You".  Film composer Hans J. Salter was nominated for an Academy Award for his score.

== Cast ==

* Donald OConnor as Jimmy Monahan
* Peggy Ryan as Patsy Monahan
* Jack Oakie as Pete Monahan
* Ann Blyth as Sheila DeRoyce
* Rosemary DeCamp as Lillian Miles / Lillian DeRoyce
* John Miljan as Arnold Pembroke Gavin Muir as Weldon Laydon
* Isabel Jewell as Rose Monahan
* Ian Wolfe as Clerk
* Robert Homans as Policeman
* Marion Martin as Soubrette
* Lloyd Ingraham as Judge
* unbilled players include Andrew Tombes

== External links ==

*  

 

 
 
 
 
 
 
 
 


 