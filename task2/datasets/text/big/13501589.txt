Thieves' Gold
 
{{Infobox film
| name           = Thieves Gold
| image          = 
| caption        = 
| director       = John Ford
| producer       = 
| writer         = Frederic R. Bechdolt George Hively Harry Carey
| music          = 
| cinematography = John W. Brown Ben F. Reynolds
| editing        = 
| distributor    = 
| released       =  
| runtime        = 50 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 Western film Harry Carey. The film is considered to be lost film|lost.   

==Production== Universal Special Feature in 1918. It was a 50-minute silent film on five reels, part of the "Cheyenne Harry" series of film featurettes. The original story, "Back to the Right Train" by Frederick R. Bechdolt, was adapted for the screen by scenarist George Hively. This installment of "Cheyenne Harry" won notably negative reviews by critics at the time of its release. 

==Plot==
Cheyenne Harry tries to help his outlaw friend Padden evade arrest after Padden has drunkenly shot another man. In the end, the two mismatched friends fight it out, leaving Padden dead. In a romantic subplot, Harrys fiancée Alice leaves him, but finally returns.

==Cast== Harry Carey as Cheyenne Harry Molly Malone as Alice Norris
* John Cook as Uncle Larkin
* Martha Mattox as Mrs. Larkin
* Vester Pegg as Curt Simmons aka "Padden"
* Harry Tenbrook as "Colonel" Betoski
* Helen Ware as Mrs. Savage
* L. M. Wells as Savage
* Millard K. Wilson as undetermined role

==See also==
* John Ford filmography
* Harry Carey filmography
*List of lost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 