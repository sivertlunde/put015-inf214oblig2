Time and Tide (2000 film)
 
 
{{Infobox film
| name           = Time and Tide
| film name = {{Infobox name module
| traditional    = 順流逆流
| simplified     = 顺流逆流
| jyutping       = Seon6 lau4 jik6 lau4
| pinyin         = Shùnliú nìliú}}
| image          = Time-and-Tide-poster.jpg
| caption        = Film poster
| director       = Tsui Hark
| producer       = Tsui Hark
| writer         = Tsui Hark Koan Hui Anthony Wong Joventino Couto Remotigue Candy Lo
| music          = Tomy Wai
| cinematography = 
| editing        = Marco Mak
| studio         = Film Workshop  
| distributor    = 
| released       =  
| runtime        = 116 minutes
| country        = Hong Kong Mandarin Portuguese Portuguese English English
| budget         = 
| gross          = HK$ 4,465,047
}} 2000 Hong Hong Kong action film directed by Tsui Hark. The film is set in Hong Kong where a young man becomes a bodyguard and befriends a mercenary determined to begin life a new with the woman he just married. The two men find themselves working together to foil an assassination attempt which propels them toward confrontation with each other.

The film was re-written several times during production and post-production stages to accommodate director Tsui Harks casting choices. The film was nominated for six Hong Kong Film Awards and received generally positive reviews from western critics.

==Plot== Anthony Wong), to earn money to give to Ah Jo who wants nothing to do with him.  Nearly nine months later, Tyler meets up with a butcher named Jack (Wu Bai) and his pregnant wife, Ah Hui (Candy Lo), who helps Tyler, at Ah Huis fathers birthday, prevent the fathers assassination.  Tyler tries to convince Jack to start a bodyguard service with him, but Jack turns him down.

A group of South American mercenaries, known as the Angels, arrive and threaten Jack, who they call Juan, and their second in command Miguel (Couto Remotigue) offers him a chance to rejoin them if he kills his own father-in-law.  Instead, Jack kills the leader of the Angels and evades Uncle Jis bodyguards, knocking Tyler out in the process, and then steals a case full of cash from under their noses and escapes from the Angels.  Jack drops his wife off at her fathers mansion, giving her a key to a train station locker and telling her to go there when their child is born.  Tyler is interrogated by the cops in connection with Jack, who accuse him of being connected to the killer because he saw him up close.  Tyler tells them nothing.

An Jo tries unsuccessfully to parole Tyler, but Uncle Ji succeeds.  Tyler thanks An Jo, and Uncle Ji locks him up in a transportation crate for a night before letting him out and getting Tyler to tell him about Jack.  Uncle Ji gives Tyler more money, which Tyler tries to give to Ah Jo; he finds out that she has just gone into the hospital to give birth.  Tyler heads off after Jack, and breaks into his apartment only to find the Angels and Jack have the place staked out.

A running gun battle ensues between Jack and the Angels as Tyler desperately tries to survive in Jacks apartment.  Tyler ends up trapped in the apartment with the gas leaking and only manages to survive the explosion by hiding in the refrigerator.  Jack tricks the Angels into killing one of their own and distracting them long enough so he and Tyler can escape the rest.  Miguel admits defeat and tries to call a truce with Jack, but then calls it off when one of the other Angels spots Jacks wife who arrives on the scene to see her former apartment burning.

The Angels take off after An Hui, following her to the train station. Tyler steals a taxi with his fake gun, giving his wallet to the driver, who promptly takes it to the cops.  The police, still suspicious of Tyler from earlier, place him at the scene of the gun battle and explosion send a SWAT team after him to the station.  At the station, Tyler confronts An Hui with his fake gun and takes the money Jack stole from the Angels.  An Hui goes into labor and one of the Angels shoots a cop, and an innocent, while trying to shoot Tyler.

In the ensuing panic, Tyler drags An Hui off to safety and the SWAT team arrives to deal with the situation. As they send in a medic team to retrieve An Hui, the Angels open fire on them. Jack arrives on the scene just before the SWAT team, including Miguel and other disguised Angels, starts a full invasion with tear gas. In the midst of the gases, Jack, the Angels, and the SWAT team hunt one another, with SWAT officers silently killed one by one by the Angels, who then disguise themselves in the SWAT gear from the fallen officers, enabling them to kill even more unsuspecting officers. Tyler escapes with An Hui to the railway tracks, where Jack saves the life of the SWAT team lieutenant and surrenders to him.

The remaining Angels escape through a tunnel to a stadium full of concert goers.  Tyler has to hunker down in the tunnels as An Hui starts to give birth, and Jack convinces the SWAT Lieutenant to let him go after Miguel in the stadium.  Jack confronts Miguel in the catwalks above the concert goers and defeats him. Meanwhile, Tyler helps An Hui successfully give birth before the last of the Angels shows up. A fight ensues, and just as Tyler is about to lose, the last of the Angels gets shot repeatedly by An Hui.

Jack is given a head start to escape the police and visits with his wife, and Tyler recovers the money before going off to the hospital to see his child.

==Production==
  re-wrote the character of Jack  for Wu Bai (pictured) to match the music he made in his band China Blue.]] The Blade. The Personals, China Blue.  Cathy Tsui was cast as Ah Jo in the film. She found a scene involving her playing with a dog difficult as she was afraid of them. To make her more comfortable, she was sent to a dog farm where she played with dogs every day to become comfortable around them. {{cite web|url=http://www.cinespot.com/einterviews03.html|work=Cinespot|title=
An Exclusive Interview with Cathy Chui|accessdate=22 January 2011}} 

The film was radically altered in post-production. The original cut was over three hours long, which Tsui Hark considered to be too slow-paced. He reorganized the entire script including cutting many of Nicholas Tse and Anthony Wong Chau Sangs scenes, leaving Wu Bais sequences as they were. 

==Release==
The film had its world premiere at the Venice Film Festival in 2000.   The film was released in Hong Kong on 19 October 2000.    The film made HK$ 4,465,047 in Hong Kong.  The film was released in 4 May 2001 in the United States. 

==Reception==
At the Hong Kong Film Awards, the film received six nominations including Best New Performer and Best Supporting Actress (Candy Lo), Best Film Editing (Marco Mak), Best Action Choreography (Xiong Xin Xin), Best Original Film Score (Tommy Wai, Joventine Couto Remotigue), and Best Sound Design.  The film received generally positive reviews from western critics. The film received 63% positive reviews at the film rating website Rotten Tomatoes. 

==Notes==
 

===References===
* {{Cite book
 | last= Morton
 | first= Lisa
 | title= The Cinema of Tsui Hark McFarland
 |year= 2009
 |isbn= 0-7864-4460-6
 |url=http://books.google.ca/books?id=MiulRecfarcC&printsec=frontcover
 |accessdate=22 January 2011
}}

==External links==
*  
*  
*  
*  

 

 
 
   
 
 
 
 