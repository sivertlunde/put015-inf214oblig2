Tutu Much
 
{{Infobox film
| name = Tutu Much
| image = TUTUMuchPoster.jpg
| director = Elise Swerhone
| producer = Merit Jensen Carr Vonnie Von Helmolt
| music  = Shawn Pierce
| cinematography = Keith Eidse Charles Konowal
| editing = Brad Caslor
| released =  
| runtime = 83 minutes
| country = Canada
| language = English
}}
Tutu Much, stylised as TuTuMUCH, is a 2010 Canadian documentary film that focuses on the 9-12 year old entering class of the Royal Winnipeg Ballets Summer School Program.

It is produced by Ballet Girls Inc, a co-production between Merit Motion Pictures and Vonnie VON HELMOLT Films. It is directed by Elise Swerhone.

==Plot==
The Royal Winnipeg Ballets Summer School Program is the first step to beginning a career as a professional ballet dancer. But that doesnt mean that its easy. 9 young girls come from all over the world to enter the program, knowing that if they dont get in by a certain age, it will be far too late to become a ballerina. A dancer can try her hardest but not make it into the next stage just based on her physical musculature. Those that do make it face a difficult decision - spend their childhood and teen years away from their families and focusing on the daily strains of ballet training, or live as a normal teenager.

== References ==
 

== External links ==
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 


 
 