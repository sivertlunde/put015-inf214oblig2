Mickey's Rescue
{{Infobox film
| name           = Mickeys Rescue
| image          =
| caption        =
| director       = Jesse Duffy|J.A. Duffy Wesley Morton (assistant director)
| producer       = Larry Darmour
| writer         = Joseph Levering (story)
| narrator       = Douglas Scott James Robinson Shirley Jeane Spencer Bell Hattie McDaniel
| music          = Lee Zahler
| cinematography = James S. Brown, Jr.
| editing        = Dwight Caldwell
| distributor    = Post Pictures Corp.
| released       =  
| runtime        = 18 minutes
| country        = United States
| language       = English
| budget         =
}}
 talkie short Mickey McGuire series starring a young Mickey Rooney. Directed by Jesse Duffy, the two-reel short was released to theaters on March 23, 1934 by Post Pictures Corp.

==Plot==
In order to give Billy a proper education, a rich couple decide to adopt Billy. Feeling that something bad might happen to him, Mickey and the Scorpions trail the wealthy couple to a hotel. There, Billy finds out that life as a rich kid isnt so hot. When Mickey and the gang finally find him, Billy has accidentally gone out of window, and is hanging high above the ground. Can Mickey and Hambone save Billy?

==Cast==
Order by credits:
*Mickey Rooney - "Mickey McGuire" Douglas Scott - "Stinkey Davis"
*Billy Barty - Billy McGuire ("Mickeys Little Brother")
*Marvin Stephens - "Katrink" James Robinson - "Hambone Johnson" Shirley Jeane Rickert - "Tomboy Taylor" Spencer Bell -  Hotel Doorman (uncredited)
*Hattie McDaniel - Maid (uncredited) Robert McKenzie - Dental patient (uncredited)

===cast notes===
Last appearance of the character Stinkey Davis.

==References==
 
 

== External links ==
*  

 
 
 
 
 
 
 


 