MouseHunt (film)
{{Infobox film
| name           = MouseHunt
| image          = Mouse hunt ver4.jpg
| caption        = Theatrical release poster
| director       = Gore Verbinski
| producer       = Bruce Cohen Tony Ludwig Alan Riche
| writer         = Adam Rifkin Lee Evans Vicki Lewis Maury Chaykin Christopher Walken
| music          = Alan Silvestri
| cinematography = Phedon Papamichael Craig Wood DreamWorks Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $38 million
| gross          = $122,417,389
}}
 Lee Evans. William Hickeys death.

In the story, two Laurel and Hardy|Laurel-and-Hardy-like brothers struggle against one small house mouse for possession of a house that was willed to them by their father. The intelligent and crafty mouse outwits them completely. The film is set in a humorously indeterminate 20th century time period, with styles ranging from the 1940s to the 1970s.

==Plot== William Hickey) Lee Evans) and Ernie (Nathan Lane). When Lars declines an offer by representatives from the large Zeppco conglomerate to buy their string factory, his greedy wife April (Vicki Lewis) promptly throws him out. Meanwhile, Ernie serves Mayor McKrinkle (Cliff Emmich) at his restaurant in anticipation of becoming a famous chef for serving such a high-profile guest. However, the mayor accidentally consumes a cockroach (which came from Rudolfs old box of Cuban cigars that Ernie took for himself) and suffers a second heart attack, this one fatal. As a result, Ernies restaurant is condemned and he too is out on the street.
 exterminator named Caesar (Christopher Walken) to handle the mouse, though he too is outsmarted.
 Belgian hair-models named Hilde (Camilla Søeberg) and Ingrid (Debra Christofferson) while he is waiting, he is struck by a bus and rushed to a hospital, missing his appointment. Lars later meets Ernie in the hospital and explains that April, realizing Lars possible auction profits (and having made love with him upon finding out), has agreed to pay the mortgage off.  
 
Upon returning home to find a banged-up and delirious Caesar being carted away by paramedics, the brothers resume their task to kill the mouse with renewed obsession. When Ernie chases the mouse up a chimney and gets stuck, Lars tries to light a match while the mouse starts a gas leak, creating a terrible explosion that blasts Ernie out of the chimney and into the lake where he lands in his sunken jacuzzi tub. In rage, Ernie grabs a shotgun and fires it at the mouse, accidentally shooting a compressed can of pesticide left by Caesar that explodes and causes enormous damage to the property.
 orange at finish him off, they instead seal the mouse in a box and mail him to Fidel Castro in Cuba. With the mouse seemingly gone and with April having paid off the mortgage, the brothers reconcile again and finish renovating the house.

The night of the auction finally arrives, which is attended by Falko, April, Hilde and Ingrid, and a wide variety of international multi-millionaires. Falko attempts to get Ernie to call off the auction with a $10 million offer, but Ernie declines and the auction soon begins. However, Lars discovers the mouses box in the snow outside, returned due to insufficient postage and with a big hole gnawed through it. Lars and Ernie panic upon seeing the mouse return, but attempt to maintain their composure as the auction continues. When the mouses antics starts sparking panic and riot in the guests, the brothers desperately attempt to flush out the mouse by feeding a hosepipe into the wall. As the auction reaches a record $25 million bid, the house rapidly floods through the walls and finally the floors, and all the people are washed out of the house as it promptly collapses. Watching as April and all the bidders leave in disgust, the brothers only consolation is the fact that the mouse must finally be dead. 

With nowhere else to go, the brothers return to the factory and fall asleep, with only a single chunk of cheese for food. The mouse, having followed the brothers, restarts and feeds the cheese into the machinery to make a ball of string cheese, which inspires Ernie and Lars. In the final scene, Ernie and Lars end their war with the mouse and have successfully rebuilt the factory as a novelty string cheese company. Lars has begun a relationship with Hilde, and Ernie is able to put his culinary skill to work in developing new cheese flavors with the mouse as his personal taste-tester. 

The film ends with Rudolfs portrait (which changes emotions throughout the movie) finally smiling, next to a frame containing his lucky string and his lifes quote, "A world without string is chaos."

==Cast==
* Nathan Lane as Ernie Smuntz Lee Evans as Lars Smuntz
* Vicki Lewis as April Smuntz
* Maury Chaykin as Alexander Falko
* Eric Christmas as Ernie and Lars lawyer
* Michael Jeter as Quincy Thorpe
* Debra Christofferson as Ingrid
* Camilla Søeberg as Hilde
* Ian Abercrombie as auctioneer
* Annabelle Gurwitch as Roxanne Atkins
* Eric Poppick as Theodore Plumb, the banker
* Ernie Sabella as Maury William Hickey as Rudolf Smuntz (this was Hickeys last film before his death)
* Christopher Walken as Caesar
* Cliff Emmich as Mayor McKrinkle
* Frank Welker as Mouse, Catzilla

==Reception==
MouseHunt received mixed reviews from film critics. Rotten Tomatoes reports that 43% of 30 critics had given the film a positive review. The film was a financial success. It was released on December 19, 1997 and opened up in North America at #4 and grossed $6,062,922 in the opening weekend. It wrapped up its run on July 1, 1998 or 27.9 weeks with $61,917,389 in the North American market and $60,500,000 in other territories for a worldwide total of $122,417,389. Its budget was $38 million.

==See also==
 
* 1997 in film
* Cinema of the United States
* List of American films of 1997

==References==
Notes
 
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 