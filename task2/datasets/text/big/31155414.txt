Thankakudam
{{Infobox film 
| name           = Thankakudam
| image          =
| caption        =
| director       = SS Rajan
| producer       =
| writer         = Moithu Padiyathu
| screenplay     = Moithu Padiyathu
| starring       = Prem Nazir Sheela Adoor Bhasi T. S. Muthaiah
| music          = MS Baburaj
| cinematography = P Bhaskara Rao
| editing        = G Venkittaraman
| studio         = Iqbal Pictures
| distributor    = Iqbal Pictures
| released       =  
| country        = India Malayalam
}}
 1965 Cinema Indian Malayalam Malayalam film,  directed by SS Rajan and produced by . The film stars Prem Nazir, Sheela, Adoor Bhasi and T. S. Muthaiah in lead roles. The film had musical score by MS Baburaj.   

==Cast==
*Prem Nazir
*Sheela
*Adoor Bhasi
*T. S. Muthaiah Ambika
*Haji Abdul Rahman
*Nilambur Ayisha
*Philomina
*Santhosh Kumar

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Mehboob || P. Bhaskaran || 
|-
| 2 || Madhurikkum Maathala   || S Janaki || P. Bhaskaran || 
|-
| 3 || Madhurikkum Maathalappazhamaanu || S Janaki || P. Bhaskaran || 
|-
| 4 || Malayalathil Pennilla || LR Eeswari, Chorus || P. Bhaskaran || 
|-
| 5 || Mandaarappunchiri || KP Udayabhanu || P. Bhaskaran || 
|-
| 6 || Padachavan Valarthunnua || K. J. Yesudas || P. Bhaskaran || 
|-
| 7 || Yesunaayaka Deva || P Susheela, Kamukara || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 