The Pearl of Death
{{Infobox film
| name           = The Pearl of Death
| image          = The Pearl of Death - 1944 - Poster.png
| image_size     =
| caption        = 1944 Theatrical poster
| director       = Roy William Neill
| producer       = Howard Benedict
| based on       = Characters created by Sir Arthur Conan Doyle
| writer         = Bertram Millhauser
| narrator       =
| starring       = Basil Rathbone Nigel Bruce Evelyn Ankers
| music          = Paul Sawtell
| cinematography = Virgil Miller
| editing        = Ray Snyder
| distributor    = Universal Studios
| released       =  
| runtime        = 69 min
| country        = United States
| language       = English
| budget         =
}}
 fourteen such Conan Doyles short story "The Adventure of the Six Napoleons" but features some additions, such as Evelyn Ankers as an accomplice of the villain, played by Miles Mander, and Rondo Hatton as a brutal killer.

==Plot== Borgia Pearl" from the Royal Regent Museum under the very nose of Holmes and Watson, but when caught the pearl is not found on him and he is released.

Later, Holmes hears of an apparently motiveless murder. An elderly Colonel is found with his back broken amid a pile of smashed Porcelain|china. Holmes takes an immediate interest in the case as the unusual method of killing is that of "The Hoxton Creeper" (Rondo Hatton), known to be Conovers right-hand man.

Another murder occurs, of a little old lady, also surrounded by smashed china. Conover makes two attempts to kill Holmes, who surmises that Conover is desperately trying to recover the stolen pearl. 

After a third killing Holmes finds the common feature of each: a bust of Napoleon I of France|Napoleon. Conover, when being pursued by the police, had fled through the workshop where they were being made, and hid the pearl inside one of six identical busts.

Holmes tracks down the vendor of the busts and find out that one is still unaccounted for, as does Conovers accomplice Naomi. Conover and The Creeper arrive at the house of the owner of the final bust, only to find that Holmes has taken his place. Overpowered, Holmes convinces The Creeper that Conover will double-cross him, and the Creeper turns on Conover and kills him, after which Holmes kills the Creeper, before the police finally arrive. Holmes smashes the final bust and recovers the pearl "with the blood of five more victims on it". 

Holmes is shown as unusually (for him) impatient with Lestrades inclination to come to the wrong conclusions in this entry, even shouting at him because of his inability to grasp the situation on a couple of occasions. For his part, Lestrade is shown taunting Holmes and obviously enjoying it.

==Cast==
 
*Basil Rathbone as Sherlock Holmes
*Nigel Bruce as Doctor Watson|Dr. John H. Watson
*Evelyn Ankers as Naomi Drake
*Dennis Hoey as Inspector Lestrade
*Miles Mander as Giles Conover
*Ian Wolfe as Amos Hodder Charles Francis as Digby
*Holmes Herbert as James Goodram Richard Nugent as Bates Mary Gordon as Mrs. Hudson
*Rondo Hatton as The Creeper
* Wilson Benge as Second Ships Steward 
* Billy Bevan as Constable 
* Harry Cording as George Gelder 
* Al Ferguson as Security Guard   Colin Kenny as Security Guard 
* Connie Leon as Ellen Carey 
* John Merkyl as Doctor Julien Boncourt 
* Leyland Hodgson as Customs Officer 
* Lillian Bronson as Harkers Housekeeper 
* Harold De Becker as Boss 
* Leslie Denison as Police Sergeant Murdock
*J.W. Austin as Police Sergeant Bleeker 
* Arthur Mulliner as Thomas Sandeford 
* Arthur Stenning as First Ships Steward 
* Eric Wilton as Conovers Chauffeur Charles Knight as Bearded Man 
* Audrey Manners as Body of Teacher
 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 