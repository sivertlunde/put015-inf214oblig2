The Scarlet Wooing
{{Infobox film
| name           = The Scarlet Wooing
| image          =
| caption        =
| director       = Sidney Morgan 
| producer       = Frank E. Spring 
| writer         =  Sidney Morgan Eve Balfour   George Keene   Marguerite Blanche   Joan Morgan
| music          = 
| cinematography = 
| editing        = 
| studio         = Progress Films
| distributor    = Butchers Film Service
| released       = April 1920   
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama Eve Balfour, George Keene and Marguerite Blanche. An author writes a scandalous novel in order to raise funds for his daughters operation.

==Cast==
*    Eve Balfour (actress)|   Eve Balfour as Mrs. Raeburn  
* George Keene as Paul Raeburn  
* Marguerite Blanche as Nancy  
* Joan Morgan as May Raeburn   George Bellamy as Dr. Andrew Hooper   Harry Newman as Roland Standish  
* Arthur Walcott as John Pollock  
* Edward Godal as Clubman  
* Nigel Black-Hawkins as Clubman

==References==
 

==Bibliography==
* Low, Rachael. The History of the British Film 1918-1929. George Allen & Unwin, 1971.

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 