Rise Up (film)
Rise Up is a 2007 documentary film by director and cinematographer Luciano Blotta.  The film documents the world of underground music in Jamaica following the efforts of three musicians (Turbulence (musician)|Turbulence, Kemoy, and Ice Anastasia) to break into the mainstream, and vicariously exploring a variety of socio-cultural issues relating to poverty, violence, and post-colonial corruption.
 Jamaican Observer as the "Rise of a new Classic".   

==Turbulence (Jamaican Recording Artist)== Turbulence was featured as one of three main acts in Rise Up.    The film, documents Turbulence’s rise to prominence as an internationally recognized and nationally celebrated reggae artist, and highlights the political and socially active nature of his music. 

The breakout single, “Notorious”, brought Turbulence and T.H.C. Muzik international acclaim. First released in 2004 on the Scallawah riddim, "Notorious" rose to Number 1 on local and international Reggae/Dancehall charts in Jamaica, New York City, London, Toronto and Japan in 2005, bolstered by the supporting music video by Rise Up director and cinematographer Luciano Blotta. The track was re-released as the feature track of the Notorious album in 2006. 

==Education and Visual Culture==
Rise Up was adapted into an educational program, which was featured by the "Docs for Schools" initiative sponsored by HotDocs.    The film also received an endorsement from Kino-Eye Center for Visual Innovation, a visual cultural research institute, and has been adapted by Kino-Eye Center president, Cesare Wright, as part of the film and media studies curriculum at the Univ. of Rochester and Rice University. According to Wright, Rise Up is "particularly interesting as an example of creative ethnography" and "stands in stark contrast to a reductive filmic tradition of exoticism, Orientalism, and tropicalization." 

==References==
 

 
 