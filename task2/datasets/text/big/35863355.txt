Cruel Summer (film)
{{Infobox film
| name           = Cruel Summer
| image          = Cruel Summer poster.jpg
| alt            = 
| caption        = Promotional poster
| director       = Kanye West Alexandre Moors    Amanda Palmer  (Executive Producer) 
| writer    = Kanye West Elon Rutberg 
| story          = Kanye West Elon Rutberg  Alexandre Moors  Scott Mescudi Sean Anderson Tauheed Epps Daniel Oyebanjo Terrence Thornton Chauncey Hollis Benjamin Hudson Cydel Young Aziz Ansari
| music          = Kanye West 
| studio         =  Island Def Jam Doha Film Institute
| released       =  
| runtime        = 35 minutes  Cruel Summer
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
Cruel Summer is a short film by American musician Kanye West. It premiered at the 2012 Cannes Film Festival on May 23, 2012,     and was shown out of competition. 

A custom pyramid-shaped screening pavilion was constructed for the films debut, designed by conceptual firms DONDA, OMA and 2x4. The theater featured seven screens - three in the front, one on the floor, one on the ceiling and one on both the right and left side of the space, the concept was inspired by Burberrys immersive Beijing show in 2011, the previous year, which featured eight screens, three in the front and back, one on both left and right and one on the ceiling. Cruel Summer was shot using a specialized camera rig, which allowed the directors to capture multiple angles simultaneously. This style of filming and screening a movie has since become known as the "Seven Screen Experience". 
 the album of the same name by Wests record label GOOD Music.  It has been described as a "fusion of short film and art", with the LA Times raving Cruel Summer has a "thumping surround-sound quality that makes a 3-D Michael Bay effort feel like an iPad short."   

== Synopsis == Scott Mescudi), a high-end car thief, falls in love with a blind Arabian princess whose father (Ali Suliman) will only allow them to wed if he can pass a series of three challenges. Loosely based on old Arabian folk tales, the story culminates in a final challenge where Rafi must cure the Princess of her blindness in order to gain her companionship. 

== Background ==
Emissaries of Wests team traveled to Dubai, Abu Dhabi and Doha, Qatar in January 2012 to commence initial pre-production. They met with prominent government officials and private executives in each location to secure funding and shooting permits.   

It later emerged that "Cruel Summer" would be exclusively shot in Doha, the capital city of Qatar.    The project was produced in association with the Doha Film Institute, a cultural and film financing organization chaired by Her Excellency Sheikha Al-Mayassa bint Hamad bin Khalifa Al-Thani, a member of Qatars royal family and daughter of the ruling Emir, His Highness Sheikh Hamad bin Khalifa Al Thani.

Sarah A., a first time Qatari actress played the female lead in "Cruel Summer". Never before has a Qatari woman starred in a movie in the countrys history.

Production of the film took place in mid-April, just one month before the movies premiere during Cannes Film Festival. Post-production and editing subsequently occurred in New York City.
 McQueen or Tarsem was to meet the entertainment value of Cirque du Soleil or Walt Disney." 

 

==Reviews==
Cruel Summer received positive reviews.   
The Hollywood Reporter called the film "groundbreaking" and exclaimed "It turns out Kanye West didn’t just want to make a short film -- he wants to completely change the way movies are watched."   
The Los Angeles Times wrote the film has "new music from West and a thumping surround-sound quality that makes a 3-D Michael Bay effort feel like an iPad short. "Cruel Summer" was shot with multiple cameras, with each screen offering a different perspective on the action."  
Rolling Stone commented the piece has "plenty of striking imagery" and praised Wests "great visual sense". 

==Track listing==
{{Track listing
 Mercy
| note1           = featuring Kanye West, Big Sean, Pusha T and 2 Chainz Terrence Thornton, Tauheed Epps, James Thomas, Denzie Beagle, Winston Riley, Reggie Williams

| title2          = The Morning
| note2           = featuring Raekwon, Pusha T, Common (rapper)|Common, 2 Chainz, Cyhi the Prynce, Kid Cudi, Dbanj and Kanye West Ramon Ibanga, Corey Woods, Lonnie Lynn, Cydel Young, Andrea Martin, Alan Lerner, Frederick Loewe

| title3          = Im the Man
| note3           = featuring Kanye West & Boy Le Prime

| title4          = Triumf Miguel

| title5          = The One
| note5           = featuring Kanye West, Big Sean, 2 Chainz and Marsha Ambrosius
| writer5         = West, Anderson, Epps, Ambrosius, B. Thomas, Jones, Fauntleroy, C. Smith, Chuck D|C. Ridenhour, The Bomb Squad|J. Boxley, Dave Barker, Winston Riley, Ansell Collins

| title6          = No More
| note6           = featuring DBanj

| title7          = Paradise
| note7           = featuring Coldplay and Mr. Hudson

| title8          = Bliss
| note8           = featuring John Legend and Teyana Taylor

| title9          = DoHa (Instrumental)

| title10         = Sin City
| note10          = featuring John Legend, Travi$ Scott, Teyana Taylor, Cyhi the Prynce and Malik Yusef John Stephens, Scott, T. Brown, Taylor, J. Young, C. Young, Jones, Victoria McCants
}} 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 