Jaws (franchise)
 
{{Infobox film
|name=Jaws
|image=jaws_boxset.jpg
|image_size=
|caption=The Jaws trilogy box set containing the first three films on DVD.
|director=Steven Spielberg   Jeannot Szwarc   Joe Alves   Joseph Sargent  
|producer=
|writer= based on=Jaws Jaws by Peter Benchley
|starring=
|studio=
|distributor=Universal Studios (1975–87)
|released=  –  
|runtime=478 minutes
|country=United States
|language=English
|budget=
|gross=$798,415,075
|music=}}
  novel that expanded into four films, a theme park ride, and other tie-in merchandise. The main subject of the franchise is a great white shark, and its attacks on people in specific areas of the United States. The Brody family is featured in all of the films as the primary antithesis to the shark. The original film was based on a novel written by Peter Benchley, which itself was inspired by the Jersey Shore shark attacks of 1916. Benchley would adapt his novel, along with help from Carl Gottlieb and Howard Sackler, into the 1975 film Jaws (film)|Jaws, which was directed by Steven Spielberg. Although Gottlieb would go on to pen two of the three sequels, neither Benchley or Spielberg would return to the film series in any capacity.
 blockbuster movies Academy Award. Best Picture.
 theme park one of the greatest films ever, and frequently appears in the top 100 of various American Film Institute rankings.

==Overview==

===Jaws (1975)===
  novel of marine biologist Robert Shaw). The three voyage out onto the ocean in Quints boat - the Orca. The shark kills Quint, but Brody manages to destroy it by shooting at the highly pressurized air tank that he has wedged in its mouth. In the end, Brody and Hooper are seen swimming away from the sinking Orca, having both of them managed to survive uninjured the shark attack on the boat.

===Jaws 2 (1978)===
 
The first sequel, Jaws 2, depicts the same town four years after the events of the original film, when another great white shark arrives on the shores of the fictional seaside resort of Amity Island. Directed by Jeannot Szwarc and starring Roy Scheider again as Police Chief Martin Brody, who, after a series of deaths and disappearances, suspects that the culprit is another shark. However, he has trouble convincing the towns selectmen. He has to act alone to save a group of teenagers, including his two sons, who encounter the shark whilst out sailing.

===Jaws 3-D (1983)===
 
The plot of  ) is the Chief Engineer of the park, and his younger brother, Sean (John Putch), arrives at the resort to visit him. The events of the earlier films are implied through Seans dislike of the water because of "something that happened when he was a kid". The events and character development from Jaws 3-D are independent from the rest of the series.   

===Jaws: The Revenge (1987)===
 
The fourth and final film,  , sees the storyline returning to Amity Island, but ignores all plot elements introduced in Jaws 3-D. No mention is made to Michaels girlfriend from the previous film, Katherine Morgan ( ), claims that he died through fear of the shark. Her youngest son, Sean (Mitchell Anderson), now working as a police deputy in Amity, is dispatched to clear a log from a buoy. As he does so, he is attacked and killed by a shark. Ellen becomes convinced that a shark is deliberately victimizing her family for the deaths of the first two sharks. Michael (Lance Guest) convinces her to spend some time with his family in The Bahamas. However, as his job involves a lot of time on and in the sea, Ellen fears that he will be the sharks next victim. When her granddaughter, Thea (Judith Barsi), narrowly avoids being attacked by a shark, Ellen takes a boat in order to kill her familys alleged stalker. Hoagie (Michael Caine), Michael and his friend Jake (Mario Van Peebles) find Ellen, and then proceed to electrocute the shark, driving it out of the water and impaling it on the prow of Ellens boat.

==Cast and characters==
{|class="wikitable" style="text-align:center; width:99%;"
|-
!rowspan="2" style="width:15%;"| Characters
!colspan="4" style="text-align:center;"| Film
|-
!style="text-align:center; width:15%;"| Jaws (film)|Jaws  (1975)
!style="text-align:center; width:15%;"| Jaws 2  (1978)
!style="text-align:center; width:15%;"| Jaws 3-D  (1983)
!style="text-align:center; width:15%;"|    (1987)
|-
!Chief Martin Brody
|colspan="2" | Roy Scheider
|style="background:#d3d3d3;"|
|  
|-
!Ellen Brody Lorraine Gary
|style="background:#d3d3d3;"| Lorraine Gary
|-
!Mrs. Taft Fritzi Jane Courtney
|style="background:#d3d3d3;"| Fritzi Jane Courtney
|-
!Mr. Posner Cyprian R. Dube
|style="background:#d3d3d3;"| Cyprian R. Dube
|-
!Michael Brody Chris Rebello Mark Grunner  Dennis Quaid Lance Guest
|-
!Sean Brody  Jay Mello Mark Gilpin John Putch Mitchell Anderson Jay Mello (archive footage)
|-
!Mayor Larry Vaughn Murray Hamilton
|colspan="2" style="background:#d3d3d3;"|
|-
!Deputy Lenny Hendricks Jeffrey Kramer
|colspan="2" style="background:#d3d3d3;"|
|-
!Mrs. Kinter Lee Fierro
|style="background:#d3d3d3;"|
|style="background:#d3d3d3;"| Lee Fierro
|-
!Matt Hooper Richard Dreyfuss
|colspan="3" style="background:#d3d3d3;"| 
|-
!Quint Robert Shaw Robert Shaw
|colspan="3" style="background:#d3d3d3;"|
|-
!Harry Meadows  Carl Gottlieb
|colspan="3" style="background:#d3d3d3;"|
|-
!Len Peterson  
|colspan="1" style="background:#d3d3d3;"| Joseph Mascolo
|colspan="2" style="background:#d3d3d3;"|
|-
!Dr. Laureen Elkins
|colspan="1" style="background:#d3d3d3;"| Collin Wilcox Colin Wilcox
|colspan="2" style="background:#d3d3d3;"|
|-
!Tina Wilcox
|colspan="1" style="background:#d3d3d3;"| Ann Dusenberry
|colspan="2" style="background:#d3d3d3;"|
|-
!Eddie Marchand
|colspan="1" style="background:#d3d3d3;"|
|colspan="1" | Gary Dubin
|colspan="2" style="background:#d3d3d3;"|
|-
!Larry Vaughn Jr.
|colspan="1" style="background: #d3d3d3;"| David Elliott
|colspan="2" style="background: #d3d3d3;"|
|-
!Tom Andrews
|colspan="1" style="background: #d3d3d3;"| Barry Coe
|colspan="2" style="background: #d3d3d3;"|
|-
!Grace Witherspoon
|colspan="1" style="background: #d3d3d3;"| Susan French
|colspan="2" style="background: #d3d3d3;"|
|-
!Andy Williams 
|colspan="1" style="background: #d3d3d3;"| Gary Springer
|colspan="2" style="background: #d3d3d3;"|
|-
!Jackie Peters
|colspan="1" style="background: #d3d3d3;"| Donna Wilkes
|colspan="2" style="background: #d3d3d3;"|
|-
!Marge
|colspan="1" style="background: #d3d3d3;"| Martha Swatek
|colspan="2" style="background:#d3d3d3;"|
|-
!Kathryn Morgan
|colspan="2" style="background:#d3d3d3;"| Bess Armstrong
|colspan="1" style="background:#d3d3d3;"|
|-
!Kelly Anne Bokowski
|colspan="2" style="background:#d3d3d3;"| Lea Thompson
|colspan="1" style="background:#d3d3d3;"|
|-
!Calvin Bouchard
|colspan="2" style="background:#d3d3d3;"| Louis Gossett, Jr.
|colspan="1" style="background:#d3d3d3;"|
|-
!Carla Brody
|colspan="3" style="background:#d3d3d3;"| Karen Young Karen Young
|-
!Thea Brody
|colspan="3" style="background:#d3d3d3;"| 
|colspan="1" | Judith Barsi
|-
!Hoagie Newcombe
|colspan="3" style="background:#d3d3d3;"| Michael Caine
|-
!Jake
|colspan="3" style="background:#d3d3d3;"| Mario Van Peebles
|-
|}

==Films==

===Novel===
  Doubleday editor Tom Congdon was interested in Benchleys idea of a novel about a great white shark terrorizing a beach resort.    After various revisions and rewrites, Benchley delivered his final draft in January 1973.    The title was not decided until shortly before the book went to print. Benchley says that he had spent months thinking of titles, many of which he calls "pretentious", such as The Stillness in the Water and Leviathan Rising. Benchley regarded other ideas, such as The Jaws of Death and The Jaws of Leviathan, as "melodramatic, weird or pretentious".  According to Benchley, the novel still did not have a title until twenty minutes before production of the book. 
 The Book Bantam bought the paperback rights for $575,000. 
 David Brown, Universal Pictures, heard about the book at identical times at different locations. Brown heard about it in the fiction department of Cosmopolitan (magazine)|Cosmopolitan, a lifestyle magazine then edited by his wife, Helen Gurley Brown. A small card gave a detailed description of the plot, concluding with the comment "might make a good movie". Brown, David, "A Look Inside Jaws", produced by Laurent Bouzereau, available as a bonus feature on some laserdisc and DVD releases of Jaws  The producers each read it overnight and agreed that it was "the most exciting thing that they had ever read" and that, although they were unsure how they would accomplish it, they had to produce the film. Zanuck, Richard D., "A Look Inside Jaws", produced by Laurent Bouzereau, available as a bonus feature on some laserdisc and DVD releases of Jaws  Brown says that had they read the book twice they would have never have made the film because of the difficulties in executing some of the sequences. However, he says that "we just loved the book. We thought it would make a very good movie." 

===Production===
Zanuck and Brown had originally planned to hire John Sturges to direct the film, before considering Dick Richards.    However, they grew irritated by Richards vision of continually calling the shark "the whale"; Richards was subsequently dropped from the project.  Zanuck and Brown then signed Spielberg in June 1973 to direct before the release of his first theatrical film, The Sugarland Express.  Spielberg wanted to take the novels basic concept, removing Benchleys many subplots.   Zanuck, Brown and Spielberg removed the novels adulterous affair between Ellen Brody and Matt Hooper because it would compromise the camaraderie between the men when they went out on the Orca. 

Peter Benchley wrote three drafts of the  , asking for advice.  Gottlieb rewrote most scenes during principal photography, and John Milius contributed dialogue polishes. Spielberg has claimed that he prepared his own draft. The authorship of Quints monologue about the fate of the cruiser   has caused substantial controversy as to who deserves the most credit for the speech. Spielberg described it as a collaboration among John Milius, Howard Sackler, and actor Robert Shaw.    Gottlieb gives primary credit to Shaw, downplaying Milius contribution.   

Three mechanical sharks were made for the production: a full version for underwater shots, one that moved from camera-left to right (with its hidden side completely exposing the internal machinery), and an opposite model with its right flank uncovered.    Their construction was supervised by production designer   tone.   

The studio ordered a sequel early into the success of Jaws.  The success of The Godfather Part II and other sequels meant that the producers were under pressure to deliver a bigger and better shark. They realized that someone else would produce the film if they didnt, and they preferred to be in charge of the project themselves.    Spielberg declined to be involved in the sequel.  

Like the first film, the production of Jaws 2 was troubled. The original director, John D. Hancock, proved to be unsuitable for an action film and was replaced by Jeannot Szwarc. The Making of Jaws 2, Jaws 2 DVD, Written, directed and produced by Laurent Bouzereau  Scheider, who only reprised his role to end a contractual issue with Universal,    was also unhappy during production and had several heated exchanges with Szwarc.       Marthas Vineyard was again used as the location for the town scenes. Although some residents guarded their privacy, many islanders welcomed the money that the company was bringing.  The majority of filming was at Navarre Beach, Florida, because of the warm weather and the waters depth being appropriate for the shark platform.  Like the first film, shooting on water proved challenging. After spending hours anchoring the sailboats, the wind would change as they were ready to shoot, blowing the sails in the wrong direction.    The corrosive effect of the saltwater damaged some equipment, including the metal parts in the sharks.  As with the first film, footage of real sharks filmed by Australian divers Ron & Valerie Taylor was used for movement shots that could not be convincingly achieved using the mechanical sharks. 
 spoof named National Lampoon John Hughes and Todd Carroll were commissioned to write a script.    The project was abandoned due to conflicts with Universal Studios. 

 , who had also revised the screenplays for the first two Jaws films, was credited for the script alongside Matheson.   
 Nassau in The Bahamas, but the location did not offer the "perfect world" that the 38-day shoot required. The cast and crew encountered many problems with varying weather conditions. 

In February 2010, film website Cinema Blend reported that a source from Universal Pictures has indicated that Universal is "strongly considering" remaking Jaws in 3-D film|3-D, following the commercial success of Avatar (2009 film)|Avatar.  The source also reported that 30 Rock star Tracy Morgan was considered to portray Matt Hooper in the remake, which they say could be more comedic and make more use of special effects.  The studio has not officially commented upon the rumor.  

===Crew===
{| class="wikitable" style="white-space:nowrap; border:1;"
! align="center" | Film
! align="center" | Year
! align="center" | Director
! align="center" | Composer
! align="center" | Writer(s)
! align="center" | Producer(s)
|-
|1. Jaws (film)|Jaws 1975 in 1975
| Steven Spielberg John Williams Peter Benchley, Carl Gottlieb & Howard Sackler (uncredited) David Brown David Brown & Richard D. Zanuck
|-
|2. Jaws 2 1978 in 1978
| Jeannot Szwarc Carl Gottlieb & Howard Sackler
|-
|3. Jaws 3-D 1983 in 1983
| Joe Alves Alan Parker Alan Parker Carl Gottlieb & Richard Matheson Rupert Hitzig
|-
|4.   1987 in 1987
| Joseph Sargent Michael Small Michael de Guzman Joseph Sargent
|-
|}

===Music=== E and F (musical note)|F,    became a classic piece of suspense music, synonymous with approaching danger. Williams described the theme as having the "effect of grinding away at you, just as a shark would do, instinctual, relentless, unstoppable."    When the piece was first played for Spielberg, he was said to have laughed at Williams, thinking that it was a joke. Spielberg later said that without Williams score the film would have been only half as successful, and Williams acknowledges that the score jumpstarted his career.  Williams won an Academy Award for Original Music Score for his work on the first film. The Music of Jaws 2, Jaws 2 DVD, Written, directed and produced by Laurent Bouzereau 
  Alan Parker composed and conducted the score for Jaws 3-D, while the final film was scored by Michael Small. The latter was particularly praised for his work, which many critics considered superior to the film.      

===Box office===
Jaws was the first film to use "wide release" as a distribution pattern. As such, it is an important film in the history of film distribution and marketing.     Prior to the release of Jaws, films typically opened slowly, usually in a few theaters in major cities, which allowed for a series of "premieres." As the success of a film increased, and word of mouth grew, distributors would forward the prints to additional cities across the country.  The film became the first to use extensive television advertising.    Universal executive Sidney Sheinbergs rationale was that nationwide marketing costs would be amortized at a more favorable rate per print than if a slow, scaled release were carried out. Scheinbergs gamble paid off, with Jaws becoming a box office smash hit and the father of the summer Blockbuster (entertainment)|blockbuster.  

When Jaws was released on June 20, 1975, it opened at 464 theaters.    The release was subsequently expanded on July 25 to a total of 675 theaters, the largest simultaneous distribution of a film in motion picture history at the time. During the first weekend of wide release, Jaws grossed more than $7 million, and was the top grosser for the following five weeks.    During its run in theaters, the film became the first to reach more than $100 million in U.S. box office receipts.   Jaws eventually grossed more than $470 million worldwide ($  billion in 2010 dollars ) and was the highest grossing box office film until   debuted two years later.      

Jaws 2 was the most expensive film that Universal had produced up until that point, costing the studio almost $30 million.  According to David Brown, the film made 40% gross of the original. This was attractive to studios because it reduced market risk.  The film became the highest-grossing sequel in history, succeeded by the release of Rocky II in 1979. It opened in 640 theaters, making $9,866,023 in its opening weekend.  The final domestic gross for Jaws 2 was $81,766,007, making it the sixth highest domestic grossing film of 1978.   

Jaws 3-D grossed $13,422,500 on its opening weekend,    playing to 1,311 theaters at its widest release. It has achieved total lifetime worldwide gross of $87,987,055.    Despite being #1 at the box office, this illustrates the series diminishing returns, since Jaws 3-D has earned nearly $100,000,000 less than the total lifetime gross of its predecessor  and $300,000,000 less than the original film.   
 worst movies ever made. Even though it received negative reviews, the film was able to cover costs (estimated US$23 million) with a worldwide box office take of $51,881,013.  The film, though, continued the series diminishing returns. It only grossed $7,154,890 in its opening weekend, when it opened to 1,606 screens.  This was around $5 million less than its predecessor.   It has also achieved the lowest total lifetime gross of the series.  
{| class="wikitable" style="white-space:nowrap; border:1;"
| rowspan="2" align="center" | Film
| rowspan="2" align="center" | U.S. release date
| colspan="3" align="center" | Box office revenue
| rowspan="2" align="center" | Reference
|-
| align="center" | Domestic
| align="center" | Foreign
| align="center" | Worldwide
|-
| Jaws (film)|Jaws
| align="center" | June 20, 1975
| align="center" | $260,000,000
| align="center" | $210,653,000
| align="center" | $470,653,000
| align="center" | 
|-
| Jaws 2
| align="center" | June 16, 1978
| align="center" | $81,766,007  
| align="center" | $106,118,000
| align="center" | $187,884,000 
| align="center" | 
|-
| Jaws 3-D
| align="center" | July 22, 1983
| align="center" | $45,517,055
| align="center" | $42,470,000
| align="center" | $87,987,055
| align="center" |   
|-
|  
| align="center" | July 17, 1987 
| align="center" | $20,763,013
| align="center" | $31,118,000
| align="center" | $51,881,013
| align="center" | 
|-
! colspan="2" | Jaws film series
| align="center" | $408,056,075
| align="center" | $390,359,000
| align="center" | $798,415,075
!
|}

===Critical reception=== the greatest 100 Years... 100 Years... 100 Thrills.

The sequels are not held in such high regard. Many reviewers criticized Jaws 2 director Jeannot Szwarc for showing more of the shark than the first film had, reducing the Hitchcockian notion "that the greatest suspense derives from the unseen and the unknown, and that the imagination is capable of conceiving far worse than the materialization of a mere mechanical monster."    However, the performances of Scheider, Gary and Hamilton in Jaws 2 were praised.  However, the teenagers, who are "irritating and incessantly screaming"... dont make for very sympathetic victims".   

Reception for Jaws 3-D was generally poor.  " and suggests that Alves "fails to linger long enough on the Great White."  It has an 11% rotten rating at Rotten Tomatoes.  The 3-D was criticized as being a gimmick to attract audiences to the aging series    and for being ineffective.   Derek Winnert says that "with Richard Mathesons name on the script youd expect a better yarn" although he continues to say that the film "is entirely watchable with a big pack of popcorn." 
 worst movies ceremony to collect his Academy Award for Best Supporting Actor earned for Hannah and Her Sisters because of his shooting commitments on this film.   

In an era in which documentaries were attempting responsible, accurate reporting about the natural world, ecocriticism says that Hollywood continued to produce films that exploited the fear of animals.    Scholar Greg Garrard cites David Ingrams suggestion that the Jaws series "represents a backlash against conservationist ideas in which an evil, threatening nature is eventually mastered through male heroism, technology and the blood sacrifice of the wild animal".   Greg Garrard observes in Jaws The Revenge that "the marine biologist Mike Brodys environmentalist concerns are effectively ridiculed as his colleague is eaten by the enraged fish; he joins the hunt for it and the shark in turn hunts him down." 

===Unofficial sequels and rip-offs=== Great White (also known as The Last Shark), as an unofficial sequel. 

==Merchandise==
  game where the player had to use a hook to fish out items from the sharks mouth before the jaws closed.   

Jaws 2 inspired much more merchandising and sponsors than the first film. Products included sets of trading cards from Topps and Bakers bread, paper cups from Coca-Cola, beach towels, a souvenir program, shark tooth necklaces, coloring and activity books, and a model kit of Brodys truck.    A novelization by Hank Searls, based on an earlier draft of the screenplay by Howard Sackler and Dorothy Tristan, was released, as well as Ray Loynds The Jaws 2 Log, an account of the films production. 
 Xbox and PC platforms.  An officially licensed iPhone game based on the original film was released by Bytemark Games and Universal Partnerships & Licensing in 2010,  while in 2011 Universal licensed a follow -up game (in the form of an Mobile app|App) called Jaws Revenge. This game was made by Fuse Powered Inc.  A game titled Jaws: Ultimate Predator was released on the Nintendo Wii and 3DS in 2011.

==References==

===Notes===
 

===Bibliography===
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
*  
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

==External links==
* 
 

 
 
 
 
 