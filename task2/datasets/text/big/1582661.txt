The Musketeer
{{Infobox film
| name = The Musketeer
| image = Musketeer_imp.jpg
| caption = theatrical poster
| director = Peter Hyams
| producer = Moshe Diamant
| writer = Gene Quintano
| starring = Catherine Deneuve Mena Suvari Stephen Rea Tim Roth Justin Chambers
| music = David Arnold
| cinematography = Peter Hyams Stefano Paradiso
| editing = Terry Rawlings
| studio = MDP Worldwide Crystal Sky Worldwide Universal Pictures   Miramax Films  
| released =  
| runtime = 104 minutes
| country = United States
| language = English
| budget = $40 million
| gross = $34,585,771
}}
 American film based on Alexandre Dumas, pères classic novel The Three Musketeers, directed by Peter Hyams and starring Catherine Deneuve, Mena Suvari, Stephen Rea, Tim Roth and Justin Chambers.

The film features Tsui Harks regular actor Xin-Xin Xiong as a stunt choreographer. 

==Plot summary==
As a young boy, dArtagnan witnesses the murder of his parents at the hands of Febre (Tim Roth), chief henchman of Cardinal Richelieu (Stephen Rea). dArtagnan is nearly killed after using his dead fathers sword to fight Febre which leaves him with a permanent scar and blinds him in one eye. dArtagnan is taken in by family friend Planchet (Jean-Pierre Castaldi), a former musketeer, one of the loyal protectors of King Louis XIII (Daniel Mesguich). 

Years later, upon arriving in Paris, grown dArtagnan (Justin Chambers) finds that the musketeers have been disbanded by order of Cardinal Richelieu, who is usurping the kings authority with the help of Febre. Richelieu is also trying to foment hostility between France, England, and Spain in order to gain more political power for himself. dArtagnan convinces two of the musketeers, Porthos (Steve Speirs) and Aramis (Nick Moran), to free the imprisoned head of the musketeers, Treville (Michael Byrne), thus earning their trust. He takes a room at a Paris boarding house, where he takes a fancy to the chambermaid, Francesca (Mena Suvari), who is the daughter of the deceased seamstress to the Queen. Febre, at the orders of Richelieu, incites a mob to attack the French Royal Palace during a state dinner for Lord Buckingham (Jeremy Clyde), a visiting English dignitary. dArtagnan, with the help of Porthos, Aramis, and another musketeer, Athos (Jan Gregor Kremp), saves King Louis, the Queen (Catherine Deneuve), and Lord Buckingham from being hurt or killed. Francesca recruits dArtagnan to make a clandestine trip to the north coast of France with the Queen to meet with Buckingham in whose honor the state dinner was being held, in order to keep peace between the two countries. dArtagnans landlord, however, overhears them and tells Febre.

During the trip, dArtagnan fights off repeated attacks by Febres henchmen. Afterwards, he and Francesca get intimate, only to have Febre discover them and kidnap Francesca and the Queen. Febre forces the Queen to write a letter to Buckingham asking him to meet her at a heavily fortified castle of his choosing, using the Queens ring to convince him of the authenticity of the message. Richelieu, finally, realizes just how far Febre is willing to go. He means to start a war between France and England and Spain, a war that will cripple France. Knowing he has lost control of his chief henchman, he secretly visits dArtagnan and tells him of Febres plans and pleads for his help to stop Febre. dArtagnan agrees but only because Febre is holding Francesca. dArtagnan returns to Paris and convinces the surviving musketeers that their responsibility to the Crown remains their highest priority. They join him at the castle where Francesca, the Queen and Lord Buckingham are being held. They charge the castle on horseback, losing several of their number in the process. The diversion they create, however, allows Planchet to drive his carriage in position in front of the castle gates below the field of cannon fire from the castle. He is able to subsequently fire a mortar directly into the castle gates.

The remaining musketeers battle the remaining cardinals guards, while dArtagnan engages Febre in a massive sword fight, finally killing him. Afterwards, dArtagnan and The Three Musketeers are given medals for their service. dArtagnan covertly threatens Richelieu. At the movies end, dArtagnan and Francesca are seen to be married.

==Cast==
* Justin Chambers as Charles de Batz-Castelmore dArtagnan|dArtagnan
* Tim Roth as Febre, the Man in Black
* Stephen Rea as Cardinal Richelieu
* Mena Suvari as Francesca Bonacieux Queen
* Daniel Mesguich as King Louis XIII
* Jean-Pierre Castaldi as Planchet
* Nick Moran as Aramis
* Steve Speirs as Porthos Athos
* Michael Byrne as Treville, Head of the Musketeers David Schofield as Rochefort, Richelieu Henchman
* Jeremy Clyde as Lord Buckingham
* Bill Treacher as Mr Bonacieux
* Tsilla Chelton as Madame Lacross

* Rock band Sonic Youth appear, heavily disguised as minstrels, playing a medieval and almost unrecognisable version of "Youth against Fascism"

==Box office==
The film grossed $27 million in Canada and the United States, and $7 million in other markets for a combined worldwide gross of $34 million. 

==Critical reception==
The film received poor reviews, garnering only 11% positive reviews on Rotten Tomatoes.  Many critics cited terrible acting and confusing editing.   The reviewer of The New York Times Stephen Holden noticed a cartoon shape of Dartagnan; an aggressive film editing, that in his opinion, destroys a positive impression from the fight scenes; incompatibility of swordplay and martial arts and also a good authentic view of Paris. 

==References==

 

==External links==
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 