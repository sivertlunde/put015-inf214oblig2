Sexo, pudor y lágrimas
 
{{Infobox film
| name        = Sexo, pudor y lágrimas
| image       = Sexopudorylagrimas.jpg
| caption     = Promotional poster for Sexo, pudor y lágrimas
| writer      = Antonio Serrano
| starring    = Demián Bichir Mónica Dionne Víctor Huggo Martin Cecilia Suárez Jorge Salinas Susana Zabaleta Angélica Aragón
| director    = Antonio Serrano Argos Cine Tabasco Films IMCINE
| music       = Aleks Syntek
| distributor = Lola Films (Mexico) 20th Century Fox (USA and Argentina)
| released    =  
| runtime     = 109 min.
| country     = Mexico
| language    = Spanish Ariel awards
| budget      =
}}
 Mexican film, Like Water for Chocolate). It was the first film directed by Antonio Serrano.

The film is based on the successful play of the same name, which ran for two consecutive years and was written by Serrano himself. After the film was released it broke box-office records in Mexico (118 million Mexican pesos, roughly 10.9 million US dollar|U.S. dollars), it was shown for more than twenty-seven weeks, and was seen by more than eight million people in Mexico alone. 

==Awards== Ariels from the Mexican Academy of Film:
** Best actress: (Susana Zabaleta)
** Art direction: (Brigitte Broch)
** Best original score
** Best original script
** ambientación
*Audience Award (XIV Guadalajara Film Festival).

==Plot==

Tomás (Demián Bichir) returns to Mexico after a seven-year trip around the world to visit his friends Carlos (Víctor Huggo Martin) and Ana (Susana Zabaleta), a couple going through relationship problems. Ana is seduced by Tomás, who is also her ex-boyfriend, which causes Carlos to kick Tomás out of their home. Although instead of Tomás leaving, Ana leaves and moves across the street to the apartment of their friends Miguel (Jorge Salinas) and Andrea (Cecilia Suárez), another couple going through problems. The situation becomes a battle of the sexes when Miguel is kicked-out for cheating on Andrea and sent to live with the "guys" across the street and María (Mónica Dionne), their friend, joins the "girls" in a boycott against all men. Tomas then has a fling with Andrea and gets caught in the act. After seeing the emptiness of his life, punctuated with him making a scene at a local nightclub, Tomas declares his love for Ana before apparently committing suicide by walking into an elevator shaft.

In the Region 4 DVD version, several alternate final scenes are explored, including Tomas surviving the fall and emerging in a full body cast.

==References==
 

==External links==
* 
*  on the New York Times site
* 
*   
*    on Zinema.com Mexican cinema site of ITESM

 

 
 
 
 
 
 
 
 


 
 