Pennin Manathai Thottu
{{Infobox film
| name           = Pennin Manathai Thottu
| image          = 
| caption        =
| director       = Ezhil
| writer         = V. Prabhakaran  (Dialogue)
| screenplay     = Ezhil
| story          = Ezhil Ramji
| producer       = M. Kajamaideen
| music          = S. A. Rajkumar
| editing        = V. Jai Shankar
| cinematography = V. Manikandan
| studio         = Roja companies
| released       = 7 July 2000
| runtime        = 165mins Tamil
| country        = India
}} South Cinema Indian Tamil Tamil film released in 2000. 
It contains the song "Kalluri Vaanil" danced by Prabhu Deva Sundaram. The film did well in the box office.

==Plot==
Sunil (Prabhu Deva) is one of the leading heart surgeons in the country. He lives with Ganpat (Mouli) and is loved by everyone in the family. Sunitha (Jaya Seal) arrives at the house, along with a kid who has a heart problem. But she lashes out at Sunil when she learns that he is the doctor and meets him face to face. Turns out she has had a bitter past with him. They had been in love in college but he had deserted her at a crucial time, so she hates him for it and doesnt even want him to operate on the kid.

==Cast==
* Prabhu Deva as Sunil
* Jaya Seal as Sunitha
* Sarathkumar as Balaram Vivek as Kandhasamy
* Mayilsamy
* Crane Manohar
* Chaplin Balu as Thozhar Arivumathi
* T. S. B. K. Moulee
* Vaiyapuri
* Madhan Bob
* S. N. Lakshmi
* Paandu
* Kanal Kannan
* Krishnamoorthy
* Indhu
* Rajasekar

==Production==
After the success of Thulladha Manamum Thullum Vijay and Ezhil immediately decided to follow up this film with another collaboration, Pennin Manathai Thottu, with either Isha Koppikar or Roja to be roped in as the lead actress. However soon after pre-production, Vijay was replaced by Prabhu Deva.  Newcomer Jaya Seal who appeared in television advertisements was selected as heroine. Sarathkumar was selected to play a guest role while Madhan Bob was assigned to play a small negative role for first time.

The shooting for the film was held at locations in Chennai, Hyderabad, and Bangalore. A song was shot on Prabhu Deva and his brother Raju Sundaram with a set resembling a market place was erected, and about 40 dancers participated in the dance with the duo. 

==Soramimi spoof== Prabhu Deva Sundaram became widely known on the internet in the form of a soramimi spoof and viral video, following its subtitling as "Benny Lava" by YouTube user Mike Sutton (Buffalax).

The name Benny Lava comes from Suttons homophonic translation of the Tamil lead line "Kalluri vaanil kaayndha nilaavo?" as "My loony bun is fine, Benny Lava!" (original meaning: "The moon (metaphor for my love) that scorched the college campus"). This video led other YouTube users to refer to Prabhu Deva as "Benny Lava". 

Another soramimi spoof and viral video featuring this song became widely known among Brazilian internet users. For the creators of this spoof, the first verse of the song (Kalluri vaanil kaayndha nilaavo?) sounds like "Vai lá, Rivaldo, sai desse lago" (Portuguese for "Come on, Rivaldo, get out of that lake"). 

Another version by YouTube user mordonopolpo was in Italian, where "Kalluri vaanil kaayndha nilaavo" became "Ah lurida! Vin quando mi lavo" (Italian for "Hey dirty (f)! Come in when I wash up myself").
 Heavy Metal documentary Global Metal. As of at least April 1, 2011, the original video has been removed because of the disabling of Buffalaxs YouTube account. However, several other users have uploaded copies of the original video on their YouTube accounts.

==Soundtrack==
Soundtrack is composed by S. A. Rajkumar. "Kannukkulle" was a hit. There are a total of 6 Tracks in this film.

{{Infobox album
| Name = Pennin Manathai Thottu
| Longtype = to Pennin Manathai Thottu
| Type = Soundtrack
| Artist = S. A. Rajkumar|S. A. Rajkumar
| Cover =
| Released = 2000 Feature film soundtrack Tamil
| Producer = S. A. Rajkumar|S. A. Rajkumar
| Label = Star Music Sa Re Ga Ma RPG Music Bayshore
}}

{{tracklist
| extra_column = Singer(s)
| title1 = Kannukkulle&nbsp;
| extra1 = P. Unni Krishnan
| length1 = 4:44
| title2 = Udhadukkum Devan
| length2 = 4:37
| title3 = Nan Saltu Kottai
| extra3 = Sukhwinder Singh, Krishnaraj
| length3 = 3:56
| title4 = Kalluri Vaanil
| extra4 = Devan, Anuradha Sriram
| length4 = 4:45
| title5 = Thiyagarajarin
| extra5 = P. Unni Krishnan, Nithyasree
| length5 = 4:56
| title6 = Kannukkulle Unnai
| extra6 = Unni Menon
| length6 =  4:45
}}

==Reviews==
Balaji B wrote:"the intention of the director may have been to ratchet up the tension, it ends up feeling like an unnecessary extension of an already threadbare movie". 

==References==
 

 

 
 
 
 