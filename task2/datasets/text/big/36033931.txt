Kamaraj (film)
{{Infobox film
| name           = Kamaraj
| image          = Kamaraj_Official_DVD_Cover.jpg
| image_size     =
| caption        = DVD cover
| director       = A. Balakrishnan
| producer       = Ramana Communications
| writer         = Chembur Jayaraj, J. Francis Krupa
| narrator       = Mahendran Vijayan Vijayan
| music          = Ilayaraja
| cinematography = M. M. Rengasamy
| editing        = V. T. Vijayan
| distributor    = 
| released       =  
| runtime        = 121 minutes
| country        = India
| language       = Tamil
| budget         =
| gross          =
}}
 Tamil biographical chief minister Parliament during 1952-1954 and 1969-1975.

Music was composed by Ilaiyaraaja and the film was produced by Ramana Communications. The film had a theatrical release across India in February 2004.

==Plot==
The narration is divided into three phases. The pre-Independence period, depicting Kamarajs childhood, the influence of Satyamurthy, Kamarajs growth as a politician and his prison life.

The second phase depicts his taking over as the Congress Chief Minister of the state, the reforms he tries to bring in, especially in education, his largesse and his bending the rules for a good cause-like for the urgent eye-operation of a kid. It also displayed his sense of humour (like his response when his mother in the village sends word that she needed a fan and a blanket), his refusal to take advantage of his position (asking for the newly installed tap to be removed from his village-house).

The third phase where he puts forth the Kamaraj Plan, resigns from the post of CM, involves himself in party work; his influence on national politics, his emergence as a kingmaker and finally his disillusionment with the emerging non-ethics in political life.

The closing scene (taken from the record files of the actual funeral), show swarms of humanity mourning the death of their beloved leader.

==Cast==
*Richard Madhuram as K. Kamaraj
*J. Mahendran as K. Rajaram
*V. S. Raghavan
*Charuhasan Vijayan
*Samuthirakani (2014) Vijayakumar as Periyar E. V. Ramasamy (2014)

==Production==
Balakrishnan had previously made a television serial on K. Kamaraj and wanted to make a feature length film on the politician. Richard Madhuram, an airport worker, was selected for the film after Charuhasan had spotted him and recommended him to the director as a lookalike of the political leader in 1995. After pre-production, the film was launched on 23 August 2002 with several politicians in attendance at the launch event.  M. S. Bhaskar has lent his voice for Richard Madhuram in the film.

==Release==
The film gained positive reviews prior to release, with a critic from Sify.com noting that it was a "class act" and that "Balakrishnan has been faithful to the subject and has come out with a mature form of cinema."   The film was released across Tamil Nadu in February 2004, but took a low key opening at the box office. A year after release, the makers chose to dub the film into English and release it in 2007 as Kingmaker in attempt to increase financial gains from the project. 
 Vijayakumar was featured as Periyar E. V. Ramasamy|Periyar. Scenes were later digitalised and extra portions featuring actor Samuthirakani were filmed in June 2014.  

==Soundtrack==
The film featured five songs composed by Ilaiyaraaja. 
* Vande Matharam
* Naadu Paarthathunda
* Oorukku Uzhaithavane
* Senthamil Naadenum
* Kamaraj Speech

==See also==
*Bharathi (film) Periyar (film)

==References==
 

==External links==
* 

 
 
 
 
 