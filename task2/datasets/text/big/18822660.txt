Art of the Devil 3
{{Infobox film
| name           = Art of the Devil 3
| image          = Artofthedevil3poster.jpg
| caption        = One of several Thai film posters.
| director       = Pasith Buranajan 
Kongkiat Khomsiri  Isara Nadee Seree Phongnithi Yosapong Polsap Putipong Saisikaew Art Thamthrakul
| producer       =
| writer         = Kongkiat Khomsiri Yosapong Polsap
| starring       = Napakpapha Nakprasitte Supakorn Kitsuwon Namo Tongkumnerd Paweena Chariffsakul
| music          =
| cinematography =
| editing        =
| distributor    = Five Star Production
| released       =  
| runtime        = 100 minutes
| country        = Thailand
| language       = Thai
| budget         =
}}
Art of the Devil 3 (  directed by Kongkiat Khomsiri, Art Thamthrakul, Yosapong Polsap, Putipong Saisikaew, Isara Nadee, Pasith Buranajan and Seree Pongniti (known collectively as the "Ronin Team"). It was released by Five Star Production.

A prequel to the 2005 film Long khong (Art of the Devil 2), Art of the Devil 3 fills in the back story of Miss Panor, portrayed by Napakpapha Nakprasitte, who turns to black magic to exact revenge upon her tormentors.

==Plot==

The film opens with a magician, Dit, torturing his former master and demanding the 3rd Eye, which will make him a full disciple of the Three-Eyed God. The master warns that Dit cannot handle all the black magic hes absorbed. Dit stabs his former master in the forehead and tries to dig out the 3rd Eye, but the old man does not seem to have it, leaving Dit to kill him. Later, Dit is in his hotel room, bathing in reeds and herbs, trying to heal a rotting wound on his stomach, caused by too much black magic devouring him from the inside out. Despite this, the wound festers and grows larger.

In a hospital, Aajaan Panor is strapped to a bed in the psychiatric ward, as her nurse and doctor look on. The doctor prescribes a change in medication, claiming that it will not harm Miss Panors unborn child. The nurse is a woman named Pen, who is Tas aunt and is also newly pregnant. Pens husband, Aod, later arrives at the hospital to pick her up, and together they drive to the train station to get Ta.

Ten years ago, Panor had tutored both Pen and Ta. Tas father, Prawase, falls in love with Panor; in order to marry her, he poisons his first wife, Duen. Tas grandfather and great-grandmother enlist Dits help to bring Duen back to life by transferring her soul into a new body. Using Tas blood as a conduit, they encourage him chant the sutra, but he is too nervous, and the ritual fails. In the present day, Ta has returned home to spend some time with his family, and they hope that he will be able to complete the ceremony this time around. The family again hires Dit to help them.

Pen takes Panor from the hospital and brings her to the family barn, where Tas grandfather has preserved Duens corpse in salt. As the ritual begins, Pen straps Panor into stirrups and removes her foetus. As Panor struggles, Dit sees the 3rd Eye bulging from her forehead, and decides he must get it at all costs. Ta chants the sutra successfully, Panors soul is torn from her body and trapped in a mirror, and Duens soul settles into Panors body. Dit ties a red string around Duen/Panors wrist and says that it will help attach the old soul to this new body. Aod goes to bury the foetus and mirror at the base of a small shrine, but is spooked by some noises in the jungle and runs off, leaving everything behind. Meanwhile, the 3rd Eye is causing Duen/Panor to experience some creepy visions—mostly corpses of people slain in the name of black magic. While arguing with some of these invisible creatures, the red string falls off her wrist.

Duen/Panor next goes to a secret hut in the jungle where she has tied up her husband, Prawase. She reveals that she is Duen in Panors body, and accuses him of trying to abandon her. She tortures him by tearing out his toenails. When she leaves the hut, Prawase gets loose and shoots himself in the head. Deep in the jungle, Duens soul starts to come loose from Panors body, and she fights with Dit to keep both pieces together. In their struggles, the mirror containing Panors soul is broken, Duens soul is evicted, allowing Panors to return and Dit runs away. At this exact moment, Tas great-grandmother starts screaming that the family is about to be destroyed by a ghost.

In the barn, Panor ties Tas grandfather to a chair and lays out some black magic accoutrements. Panor then pins his eyes open with safety pins, then rubs salt on them, then claims that she will make the grandfather watch the death of his whole family.

Using her black magic (and the extra power of the 3rd Eye), Panor causes Tas great-grandmother to stab herself in the ear with a metal spike. In the ensuing panic, Aod hears Pen calling for him, and rushes outside to see a vision of her, bleeding. Following the vision to the village windmill, Panors magic enchants a rope that becomes a noose and encircles his neck. Pen and Ta both arrive and try to save Aod, but they cannot fight the windmill, and Aod is strangled.

Back in the barn, Panor instructs the grandfather to call out to Ta and Pen, and proceeds to drip hot wax into his eyes. Pen and Ta come running, but she causes them to lose their way. Panor uses her magic to cause Pens foetus to swell up and explode out of her body. Ta finds her body and is faced with a vision of Panor, then attempts to attack her with a stick, but it passes through her body. Dit arrives and stabs Panors spirit with a magic knife, demanding that she gives him the 3rd Eye in the process. Proving to powerful for him, she pushes Dit away and causes his ever-growing stomach wound to completely consume his chest.

Ta flees and finally reaches the barn, where his grandfather is sitting at a table with the corpses of his family gathered around him. Panor arrives and captures Ta, then burns off his skin with a blow torch (as seen in the previous film). One last flashback reveals that while Panor was a child, she visited a monk to create a spell that would make any man she encountered become attracted to her as she had been bullied by her classmates for her looks.

The film ends with the coda One Year Later, and shows Ta meeting his five school chums at the train station, thus beginning Art of the Devil 2.

==Film festivals==
* 2008 Fantastic Fest
* 2008 Fantasia Festival

==External links==
*  
* 

 
 
 
 
 
 
 
 
 
 