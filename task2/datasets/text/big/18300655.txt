The New Dress
 
{{Infobox film
| name           = The New Dress
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         = Grace Henderson
| narrator       =
| starring       = Wilfred Lucas
| cinematography = G. W. Bitzer
| editing        =
| distributor    = Biograph Company
| released       =  
| runtime        = 17 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         =
}}
 short silent silent drama film directed by D. W. Griffith, starring Wilfred Lucas and featuring Blanche Sweet.   

==Cast==
* Wilfred Lucas - Jose Dorothy West - Marta
* W. Chrystie Miller - The Father
* Vivian Prescott - The Painted Woman
* Mack Sennett
* Blanche Sweet - At Wedding / At Market
* Kate Toncray - At Wedding / At Market Charles West - At Wedding / At Cafe (as Charles H. West)

==See also==
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 