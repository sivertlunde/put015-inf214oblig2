The Scout (film)
{{Infobox film
| name         = The Scout
| image        = The scout movie poster (1994).jpg
| caption      = Theatrical release poster
| writer       = Roger Angell Andrew Bergman Albert Brooks Monica Johnson
| producer     = Andre Morgan Albert S. Ruddy Michael Ritchie
| cinematography = László Kovács (cinematographer)|László Kovács 
| starring     = Albert Brooks Brendan Fraser Dianne Wiest
| music        = Bill Conti Don Zimmerman Pembroke J. Herring
| distributor  = 20th Century Fox
| released     =  
| runtime      = 101 minutes
| language     = English
| movie_series =
| awards       =
| budget       = $20 million
| gross        = $2,694,234
}} Michael Ritchie, the director of The Bad News Bears.

==Plot== King Kong and he remarks to his fellow scouts sitting in the crowd that he is looking for the next King Kong.
 The Lord.") vomit on Yankee Stadium.
 Mexican countryside. While attending a game in Mexico, the desperate Percolo gets his first look at Steve Nebraska (Brendan Fraser), a young American with a perfectly consistent 100+ mph fastball who, as a bonus, hits home runs on seemingly each at-bat.  Al exclaims, "I found him!  I found him!  I found Kong!  Oh my God Almighty!"  Steve is very popular with the fans, especially females.

Al notifies the Yankee brass of his find, but he is unceremoniously fired and told not to bring anyone back. Undaunted, he takes Steve back to the United States with him, becoming his unofficial agent in the process. Steve freaks out in the middle of the terminal at Newark International Airport when he momentarily thinks Al is missing.
 Yankee Stadium MLB team. A bidding war ensues after Steve effortlessly strikes out Keith Hernández and homers off Bret Saberhagen. (Both athletes play themselves in the film.) Nebraska soon signs a $55 million contract—which, in 1994, would have been the highest-paying contract in baseball history—with Als old team, the Yankees.
 psychiatrically evaluated in order to ensure he will not turn out to be as unstable as Als earlier finds. Al picks a psychiatrist named "H. Aaron" out of the Yellow Pages, appreciating the similarity to Hank Aaron and expecting a quick evaluation, followed by a massive payday for both Steve and himself. However, the doctor (Dianne Wiest) finds Steve to be a deeply troubled young man, so severely abused as a child that hes blocked just about every memory of his early life.

Al begs the doctor to grant a positive evaluation, promising her that Steve will undergo therapy before beginning his professional career. She gives her reluctant consent.

When the Yankees reach the World Series. Steve is contractually obligated to pitch in Game 1, despite not being mentally prepared to do so. By the night of the big game, Steve ends up atop Yankee Stadium, refusing to come down to pitch. Al pleads with him to play. However, his conscience gets the better of him and Al offers the kid a chance to walk away from it all, no strings attached.  "Maybe well become closer since nobody on the planets gonna want to talk to either of us," says Al.

The scouts loyalty convinces Steve to face reality (as well as realizing that the worst that can happen is that hell lose, which he says is not so bad since half the players lose every day).  Steve is brought down to the field in a helicopter—an idea that is actually Als, but which Yankee owner George Steinbrenner (playing himself) takes credit for himself.

Not only does Steve return to the field, he pitches a perfect game, striking out all 27 St. Louis Cardinals batters on 81 consecutive strikes. While facing Ozzie Smith (playing himself) with two outs in the ninth inning, Steves final pitch is so fast, it knocks down his catcher and the home plate umpire.

Steve also hits two solo home runs. (At games played in American League parks like Yankee Stadium theres supposed to be a designated hitter, but teams can choose to have their pitchers bat.) He personally accounts for the only scoring in a 2-0 Yankees victory (which Steve quite literally wins all by himself).

Though the Yankees win the Series opener, it is never made clear who win the Series overall. But for both Al and Steve, a perfect game is a perfect ending.

==Production==
In a July 1999 interview with Gavin Smith in Film Comment, Brooks said that The Scout was originally intended for Rodney Dangerfield. "It was lying around, never going to get made, and I said I would like to do that."

Brooks said that he contributed to a rewrite of the script because "it was written very silly." The version he worked on, he said, "did not end like Rocky with that bullshit big ending." But according to Brooks, the studio forced Ritchie to change the ending. 

==Cameos== John Sterling, Keith Hernández, Bret Saberhagen, George Steinbrenner, Brian Cashman, Ozzie Smith and Bobby Murcer, among others, play themselves in the film.

==Reception==
The Scout was a box-office flop. Reviews were predominantly negative, with TV Guides stating, "The Scout feels like a classic case of too many cooks spoiling the broth."  Variety also negatively reviewed the film, saying that Brooks and Ritchie "never quite commit to either of the movies disparate chords -- bailing out of the batters box in terms of the psychological drama and, after some amusing moments at the outset, generally steering clear of broad comedy."  Time magazines Richard Schickel praised the film, writing, "The Scout is the best comedy-fantasy about baseball ever made, which goes to show that if Hollywood keeps trying, eventually someone will get it right."  

==References==
 

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 