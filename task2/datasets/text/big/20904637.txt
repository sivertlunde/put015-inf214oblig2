Magroor
{{Infobox film
| name           = Magroor
| image          = 
| image_size     = 
| caption        = 
| director       = Brij
| producer       = 
| writer         =
| narrator       = 
| starring       = Shatrughan Sinha  Vidya Sinha
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1979
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1979 Bollywood film directed by Brij. It stars Shatrughan Sinha and Vidya Sinha.

==Cast==
*Shatrughan Sinha ...  Ranjit Sinha / Raju 
*Vidya Sinha ...  Anju 
*Prem Nath ...  Mamaji 
*Shreeram Lagoo ...  Chacha  Helen ...  Ruksana 
*Nadira ...  Mrs. Disa 
*Paintal ...  Rakesh, Anjus brother

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Haseen Raat Men"
| Mohammed Rafi, Asha Bhosle
|-
| 2
| "Hum Mohabbat Men Jane Kya Kar Jate"
| Kishore Kumar, Suman Kalyanpur
|-
| 3
| "Rukhsana Rukhsana Main Tera Diwana"
| Manna Dey, Asha Bhosle
|-
| 4
| "Shyam Tumhara Main Naam Pukaroo"
| Mahendra Kapoor, Anuradha Paudwal
|}
==External links==
*  

 
 
 
 
 