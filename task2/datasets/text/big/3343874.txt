The Little Princess (1917 film)
{{Infobox film
| name           = The Little Princess
| image          = ZaSu Pitts-Mary Pickford.png
| caption        = ZaSu Pitts and Mary Pickford in The Little Princess
| director       = Marshall Neilan
| producer       = Mary Pickford
| writer         = Frances Hodgson Burnett (Novel) Frances Marion
| narrator       =
| starring       = Katherine Griffith  Mary Pickford Norman Kerry ZaSu Pitts Theodore Roberts
| music          =
| cinematography = Charles Rosher Walter Stradling
| editing        =
| distributor    = Artcraft Pictures Corporation
| released       =  
| runtime        = 62 minutes
| country        = United States English intertitles
| budget         =
}}
 
The Little Princess is a 1917 American silent film directed by Marshall Neilan based upon the novel A Little Princess by Frances Hodgson Burnett. This version is notable for having been adapted by famed female screenwriter Frances Marion. 

==Cast==
*Katherine Griffith as Miss Minchin
*Mary Pickford as Sara Crewe
*Norman Kerry as Captain Richard Crewe
*Anne Schaefer as Amelia Minchin
*ZaSu Pitts as Becky
*Gertrude Short as Ermigarde
*Theodore Roberts as Cassim
*Gustav von Seyffertitz as Mr. Carrisford
*Loretta Blake as Lavinia
*George McDaniel as Ram Dass

==References==
 

==External links==
 
*  
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 