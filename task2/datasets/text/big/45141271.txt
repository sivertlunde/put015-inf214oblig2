Premakkoo Permitte
{{Infobox film
| name           = Premakkoo Permitte
| image          =
| caption        = 
| director       = R. Nagendra Rao
| producer       = Harini
| writer         = Shivakumar R N Jayagopal (dialogues)
| screenplay     =  Kalpana Narasimharaju Narasimharaju Jr Revathi
| music          = Vijaya Bhaskar
| cinematography = V Manohar
| editing        = P S Murthy
| studio         = Premier Studio
| distrubutor    = 
| released       =  
| country        = India Kannada
}}
 1967 Cinema Indian Kannada Kannada film, Narasimharaju and Jr Revathi in lead roles. The film had musical score by Vijaya Bhaskar.   

==Cast==
 
*Kalyan Kumar Kalpana
*Narasimharaju Narasimharaju
*Jr Revathi
*Arun Kumar
*Kumari Lalitha
*R. Nagendra Rao
*Dinesh
*H Ramachandra Shastry
*P Vadiraj
*B Ramadevi
*B Kamalamma
*Indrani
*U Mahabala rao
*Srinivas
*Guruswamy
*Udupi B Jayaram
*G M Nanjappa
*Venkataramaiah
 

==Soundtrack==
The music was composed by Vijaya Bhaskar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || R. N. Jayagopal || 03.40
|-
| 2 || Premakkoo Permitte || PB. Srinivas || R. N. Jayagopal || 03.36
|- Janaki || R. N. Jayagopal || 03.55
|- Susheela || R. N. Jayagopal || 03.23
|- Janaki || R. N. Jayagopal || 03.14
|-
| 6 || Kempu Roja Mogadavale || PB. Srinivas || R. N. Jayagopal || 03.23
|}

==References==
 

==External links==

 
 
 
 


 