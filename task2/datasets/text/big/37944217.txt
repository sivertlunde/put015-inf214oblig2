I Love Me
{{Infobox film
| name           = I Love Me
| image          = I Love Me promo poster.jpg
| caption         = Theatrical poster
| director       = B. Unnikrishnan
| producer       = Vyshakh Rajan Sethu
| starring       = Unni Mukundan Asif Ali Anoop Menon Isha Talwar
| music          = Deepak Dev
| cinematography = Satheesh Kurup
| editing        = K. M. Shameer
| distributor    = Vyshaka Cinema
| released       =  
| country        = India
| language       = Malayalam
| budget         = 
}}
 Sethu and it is for the first time B. Unnikrishnan directs a film that has not been scripted by him.   

==Plot==

Ram Mohan (Anoop Menon), a high profile businessman, brings two criminals - Xavi (Unni Mukundan) and Prem (Asif Ali) from Kochi to Bangkok to kill a person, so that he can save his sinking business empire. The situation gets complicated when Samantha (Isha Talwar) enters the equation.

==Cast==
* Unni Mukundan as Xavi
* Asif Ali as Prem
* Anoop Menon as Ram Mohan
* Isha Talwar as Samantha
* Rupa Manjari as Sameera
* Biju Pappan as Maniyappan
* Kadambari as Susan
* Vanitha Krishnachandran
* Indrans
* Joju George

==Production==
The film was scripted by  , Bangkok and Kochi. 


==BoxOffice==

The movie was a box office disaster.

==Reception==
Veeyen of Nowrunning.com gave the film a negative rating of 2/5 and stated, "at a time when novel themes and experiments are filling our screens, this one just doesnt impress". 

==References==
 

==External links==
*  

 
 
 
 
 