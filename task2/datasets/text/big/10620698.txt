Shaolin Rescuers
 
 
 
Shaolin Rescuers aka "Avenging Warriors of Shaolin" is a Shaw Brothers film directed by Chang Cheh. It is one of the Shoalin Temple themed martial arts films and the adventures of the Shaolin hero Hung Si-Kuan starring the Venom Mob and Jason Pai Piao.

==Plot==
 Pai Mei (Wang Li) and Kua Ching (Lu Feng). During the fight, famous Shaolin heroes San Te and Fong Sai-Yuk are killed, while Hung Si-Kuan (Jason Pai Piao) narrowly escapes.
 Sun Chien) is a light skill student at a local school who is constantly abused by his master and a fellow student (Ku Kuan Chung),  He ends up befriending Ah-Chun and Cha-Po after they save him from getting beat by his fellow students.

Meanwhile, Pai Mei, Kua Ching and his fighting men arrive at a temple looking for the injured Hung Si-Kuan and end up in a fight with the monks and Han Chi (Chiang Sheng). Han Chi escapes but the monks are killed, so Chi goes looking for Hung Si-Kuan. Meanwhile, Hung has arrived at Chu Tsais school, as the master is an old comrade. However, the teacher rejects Hung’s request for help and immediately reports to Pai Mei.

Chu Tsai, Ah-Chun and Cha-Po find the injured Hung Si-Kuan and they conjure up schemes to get him medicine. Once healed, Hung Si-Kuan thanks the three by teaching them special techniques to perfect their skills. Han Chi also meets up with the four after helping them in a fight at Cha-Po’s restaurant.

However, Chu Tsais fellow student had spotted Chu purchasing the medicine for Hung. He reports this to Pai Mei and Kua Ching, and leads the priest and his followers to Hung Si-Kuan’s hiding place. Hung and company escape and hide in an old temple. Although they are trapped, Kua decides to wait until dawn to attack, so as not to risk one of the fugitives escaping in the dark.

Morning arrives and Kua Ching attacks.  Although Kua and his men are all killed, Chu Tsai is killed in the battle while Ah-Chin and Cha-Po are mortally wounded.  With government troops on the way, Ah-chin and Cha-po volunteer to stay behind and occupy the troops so that Han Chi and Hung Si-Kuan can escape.  The film ends as the two realize they have achieved their dream: Dying for a noble cause.

==Cast==
*Jason Pai Piao – Hung Si-Kuan
*Lu Feng – Kua Ching
*Lo Mang – Chun Ah-Chin
*Chiang Sheng – Han Chi
*Kuo Chui - Ying Cha-Po
*Sun Chien - Chu Tsai
*Ku Kuan Chung – student
*Wang Li – Pai Mei
*Yang Hsuing – Brass hammer fighter
*Yu Tai Ping - Sword and Shield fighter
*Tony Tam Jan Dung – Tiger kid
*Lau Shi Kwong– Leopard kid
*Teekay Miknighton - opening credits

==External links==
*  

 
 
 
 
 
 
 

 