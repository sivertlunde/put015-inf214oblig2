The Woman in the Dunes
 
{{Infobox film
| name           = The Woman in the Dunes
| image          = Woman in the Dunes poster.jpg
| caption        = Japanese theatrical poster
| director       = Hiroshi Teshigahara
| producer       = Kiichi Ichikawa Tadashi Ōno
| writer         = Kōbō Abe
| starring       = Eiji Okada Kyōko Kishida
| music          = Tōru Takemitsu
| cinematography = Hiroshi Segawa
| editing        = Fusako Shuzui
| distributor    = Toho
| released       =  
| runtime        = 123 minutes 147 minutes (directors cut)
| country        = Japan
| language       = Japanese
| budget         = $100,000
}}
  is a 1964 Japanese film directed by Hiroshi Teshigahara and starring Eiji Okada and Kyōko Kishida. It received positive critical reviews and was nominated for two Academy Awards. The screenplay for the film was adapted by Kōbō Abe from his 1962 novel of the same name.

==Plot==
A schoolteacher, Junpei Niki (Eiji Okada), is on an expedition to collect insects that inhabit sand dunes. When he misses the last bus, villagers suggest he stay the night. They guide him down a rope ladder to a house in a sand quarry where a young widow (Kyōko Kishida) lives alone. She is employed by the villagers to dig sand for sale and to save the house from burial in the advancing sand.

When Junpei tries to leave the next morning, he finds the ladder removed. The villagers inform him that he must help the widow in her endless task of digging sand. Junpei initially tries to escape. Upon failing he takes the widow captive but is forced to release her in order to receive water from the villagers.

Junpei becomes the widows lover. He still, however, desperately wants to leave. One morning, he escapes from the sand dune and starts running while being chased by the villagers. Junpei is not familiar with the geography of the area and eventually gets trapped in some quicksand. The villagers free him from the quicksand and then return him to the widow.

Eventually, Junpei resigns himself to his fate. Through his persistent effort to trap a crow as a messenger, he discovers a way to draw water from the damp sand at night. He thus becomes absorbed in the task of perfecting his technology and adapts to his "trapped" life. The focus of the film shifts to the way in which the couple cope with the oppressiveness of their condition and the power of their physical attraction in spite of — or possibly because of — their situation.

At the end of the film Junpei gets his chance to escape, but he chooses to prolong his stay in the dune. A report after seven years declaring him missing is then shown hanging from a wall, written by the police and signed by his mother Shino.

==Cast==
* Eiji Okada – Entomologist Niki Junpei
* Kyōko Kishida – Woman
* Hiroko Itō – Entomologists wife (in flashbacks)
* Kōji Mitsui
* Sen Yano
* Ginzō Sekiguchi

==Critical reception==
The film has a rating of 100% on review aggregator site Rotten Tomatoes, based on 14 critical reviews with an average rating of 8.7 out of 10.   

Roger Ebert wrote "Woman in the Dunes is a modern version of the myth of Sisyphus, the man condemned by the gods to spend eternity rolling a boulder to the top of a hill, only to see it roll back down."  Strictly Film School describes it as "a spare and haunting allegory for human existence".  According to Max Tessier, the main theme of the film is the desire to escape from society. 

The films composer, Tōru Takemitsu, was praised. Nathaniel Thompson wrote, "  often jarring, experimental music here is almost a character unto itself, insinuating itself into the fabric of the celluloid as imperceptibly as the sand." 

==Awards== Special Jury Best Foreign Oscar in Italian film Best Director The Sound Grand Prix of the Belgian Film Critics Association.

==See also==
* List of Japanese submissions for the Academy Award for Best Foreign Language Film
* List of submissions to the 37th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

 
{{Navboxes title = Awards list =
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 