Rainbow Dance
 
{{Infobox Film
| name = Rainbow Dance
| image_size =
| caption =
| director = Len Lye
| producer = John Grierson
| story =
| screenplay =
| narrator =
| starring = Rupert Doone
| music = Burton Lane
| editing = Jack Ellitt
| studio =
| distributor = GPO Film Unit
| released = 1936
| runtime = 4 minutes
| country = United Kingdom
| language = English
}} 1936 United British animated film, created by New Zealand-born animation pioneer Len Lye and released by the GPO Film Unit. This is Lyes second film to be viewed by the public. It uses the Gasparcolor process. Credits also list Australian music pioneer Jack Ellitt ("Synchronization") and Frank Jones ("Camera").

==Synopsis==
A man (Rupert Doone) is holding an umbrella in the rain. Then, he starts dancing, and as he does, the backgrounds completely change. Then, he starts dancing near the ocean, with a woman and fish following. Then, he plays tennis with cel-animated circles as another man watches. A colorful array of shapes follow, and the man sits and thinks, as the shapes come back and images come off the score sheet. The music ends, and a mans voice says the following: Post Office Savings Bank puts a pot of gold at the end of the rainbow for you, followed by No deposit is too small for the Post Office Savings Bank.

==External links==
*  
*  

 
 
 
 
 
 


 