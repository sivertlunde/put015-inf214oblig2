The Diaries of Vaslav Nijinsky
 
 {{Infobox film
| name           = The Diaries of Vaslav Nijinsky
| image          = 
| image size     =
| caption        = 
| director       = Paul Cox
| producer       =
| writer         = Paul Cox
| based on = 
| narrator       = Derek Jacobi
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 2001
| runtime        = 
| country        = Australia English
| budget         =
| gross = A$39,996 (Australia) 
| preceded by    =
| followed by    =
}}
The Diaries of Vaslav Nijinsky is a 2001 Australian film written, shot, directed and edited by Paul Cox about Vaslav Nijinsky, based on the premier danseurs published diaries. 

Cox had the idea of making a film about Nijinsky for over 30 years ever since he heard Paul Scofield read extracts from Nijinkskys diaries on the radio. He used voiceover readings by Derek Jacobi combined with images related to the dancers life. Several Leigh Warren Dancers portrayed Nijinsky in different roles.    accessed 12 November 2012 

==Reception==
 
==References==
 

==External links==
*  at Australian Screen Online
*  at IMDB

 

 
 
 
 

 