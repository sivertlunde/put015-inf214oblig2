Environment (film)
{{Infobox film
| name           = Environment
| image          = Environment.jpg
| image_size     =
| caption        = James Kirkwood
| producer       =
| writer         = James Hughes
| starring       = Mary Miles Minter
| cinematography =
| editing        =
| distributor    = Mutual Film
| released       =  
| runtime        =
| country        = United States Silent English English intertitles
| budget         =
}}
 James Kirkwood.

==Plot==
Liz Simpkins is the daughter of town drunk John. While keeping the household, she falls in love with minister Henry Pennfield. Meanwhile, Mildred Holcombe, the daughter of the wealthy David, falls in love with artist Arnold Brice. One night, she is secretly at his place. Liz knows about her whereabouts and stops by to warn Mildred her brother Arthur is coming over. Mildred is able to sneak out just in time, but Arthur catches Liz in Brices room.

This incident causes a big scandal. Because her father has recently died of alcoholism, it is decided Liz will be sent to an institution. The selfish Mildred begs Liz not to tell the truth, because her father will never forgive her if he finds out the truth. However, Henry witnessed the conversation and announces he will marry Liz. 

==Cast==
* Mary Miles Minter - Liz Simpkins George Fisher - Henry Pennfield Harvey Clark - John Simppkins
* George Periolat - David Holcombe
* Emma Kluge - Mrs. Holcombe
* Margaret Shelby - Mildred Holcombe
* Arthur Howard - Arthur Holcombe
* Gayne Whitman - Arnold Brice
* Lucille Ward - Mrs. Bloom

==References==
 

==External links==
* 

 
 
 
 
 
 


 