Man in Blues
 
 
{{Infobox film
| name           = Man in Blues
| image          = 
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 男人之苦
| simplified     = 男人之苦
| pinyin         = Nán Rén Zhī Kǔ
| jyutping       = Naam4 Jan2 Zi1 Fu2}}
| director       = Yip Wai Ying
| producer       = Nam Yin
| writer         = 
| starring       = Wayne Lai Nadia Chan
| music          = Mak Chun Hung
| cinematography = Mike Pang
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}} Category III in Hong Kong.

==Plot==
Jackie Lai (Wayne Lai), a famous erotic movie star, decides to quit his career in order to take better care of his daughter Winsy (Jenny Shing). He hires his neighbor Pearl (Nadia Chan) to tutor Winsy. While then, he slowly wins Pearls approval despite her aversion to his sordid past.

==Cast==
*Wayne Lai as Jackie Lai
*Nadia Chan as Pearl Ho
*Jenny Shing as Winsy Lai
*Jenny Yam as Judy Low
*Bobby Yip as Kim Tai Chi
*Donna Chu as Maggie
*Lee Siu-kei as Director Chan
*Charlie Cho as Boss Chow
*Kau Man Lung as Brother Nine
*Cheng Chu Fung as Mr. Wong
*Eddie Chan as Brother Fung
*Jameson Lam as Director Lam
*Eileen Yeung
*Wilson Ng

==External links==
* 
*  at Hong Kong Cinemagic

 
 
 
 
 
 
 


 
 