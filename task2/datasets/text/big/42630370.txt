Harodim
{{Infobox film
| name = Harodim
| image = Poster_for_Harodim.jpg
| caption  = theatrical poster
| director  = Paul Finelli
| producer  = Thomas Feldkircher  Walter Köhler
| writer  = Paul Finelli
| music          = Depth Code
| cinematography  = Tomas Erhart
| editing        =
| starring  = Peter Fonda Michael Desante  Travis Fimmel
| genre    = Mystery Suspense
| released  =  
| runtime  =  90 minutes
| country  = Austria 
| language  = English & German
}}
Harodim is a 2012 is a thriller movie that was written and directed by Paul Finelli.  Paul Finelli is both a writer and producer who also directed Ride and Der Atem des Himmels (2010).  The film is A Terra Mater Factual Studios production, in association with Tomcat Prods., Finger Films.   The film stars Peter Fonda, Michael Desante, and Travis Fimmel, and was filmed in Austria. 

==Plot==
A naval intelligence officer, Lazarus Fell (Travis Fimmel), trained in black ops is tracking the most wanted terrorist in the world, and the man he was told was responsible for killing his father, Solomon Fell (Peter Fonda), during the 9/11 attacks.  Lazarus is betrayed by his superiors, but gets away and fakes his own death in order to find the terrorist and extract a confession. 
After hunting the terrorist for more than 10 years, Lazarus finds him (Michael Desante) living openly as a wealthy capitalist. Lazarus, motivated by his own personal revenge abducts the man.  Before executing him, Lazarus videotapes the interrogation.  The terrorist reveals the truth behind the events of 9/11, the real origins and purpose of al Qaeda, and the plans of a secret society which is using mind control in a plot to take over the world.  The secret society has infiltrated the government and military of the United States at the highest levels, and owns the banks and therefore international finance.  Lazarus is shocked to learn that he has been selected to play a pivotal role in this world domination.

==Cast==
*Peter Fonda as Solomon Fell
*Michael Desante as the terrorist
*Travis Fimmel as Lazarus Fell

==References==
 

==External links==
* 

 
 
 
 
 