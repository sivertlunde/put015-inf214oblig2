Land of Dreams (film)
{{Infobox film
| name           = Land of Dreams
| image          = 
| caption        = 
| director       = Jan Troell
| producer       = Göran Setterberg
| writer         = Jan Troell
| starring       = Rollo May Ingvar Carlsson Tage Erlander
| music          = Tom Wolgers
| cinematography = Jan Troell
| editing        = 
| studio         = 
| distributor    = Swedish Film Institute
| released       =  
| runtime        = 184 minutes
| country        = Sweden
| language       = Swedish English
| budget         = 
}} existential psychologist Rollo May, the politician Ingvar Carlsson soon before he became the prime minister of Sweden, and former prime minister Tage Erlander. Filming took place from 1983 to 1986. 

==Release==
The film premiered in Sweden on 8 February 1988, distributed by the Swedish Film Institute.  It was screened in the Forum section of the 38th Berlin International Film Festival.  It won the Swedish Film Critics Award for best domestic film of 1988. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 
 