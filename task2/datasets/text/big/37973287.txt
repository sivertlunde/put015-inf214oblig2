Maciste nella terra dei ciclopi
{{Infobox film
| name           = Maciste nella terra dei ciclopi
| image          = Macagcycpos.jpg
| caption        = Original Spanish film poster
| director       = Antonio Leonviola
| producer       = Luigi Carpentieri Ermanno Donati
| writer         = Oreste Biancoli Gino Mangini
| narrator       = 
| starring       = Gordon Mitchell Chelo Alonso
| music          = Carlo Innocenzi
| cinematography = Riccardo Pallottini
| editing        = Mario Serandrei
| distributor    = Panda Films Medallion Pictures (US)
| released       = Italy 29 March 1961 USA December 1962
| runtime        = 100 min (Italy) 94 min (USA)
| country        = Italy
| language       = 
| budget         = 
| gross          = 
}}
 1961  Italian sword and sandal film starring Gordon Mitchell and Chelo Alonso. It was Gordon Mitchell’s first lead role though he was credited as “Mitchell Gordon”. It was also the motion picture debut of 1960 Universe Championships|Mr. Universe Paul Wynter.

==Plot== Ulysses is put to death to please the Polyphemus|Cyclops. This is almost accomplished in a raid on a village by the Queens soldiers where a descendant of Ulysses is killed and his wife enslaved; however, their infant son is taken away to be protected by Maciste.

==Cast==
*Gordon Mitchell as  Maciste    
*Chelo Alonso as  Queen Capys    
*Vira Silenti as  Queen Penope    
*Dante DiPaolo as  Ifito    
*Aldo Bufi Landi as  Sirone    
*Giotto Tempestini as  Aronio    
*Raffaella Carrà as  Eber   
*Paul Wynter as  Mumba    
*Massimo Righi as  Efros    
*Aldo Pedinotti  as Cyclops

==Reception==
It has been objected that in spite of the original title there is no "Atlas" to be found in either the Italian nor the English dubbed film version. 

==Biography==
* 

==References==
 

==External links==
* 

 
 
 
 


 
 