The Aviator (2004 film)
{{Infobox film
| name           = The Aviator
| image          = The Aviator Poster.jpg
| caption        = Theatrical release poster
| director       = Martin Scorsese
| producer       = {{Plainlist| Michael Mann
* Sandy Climan
* Graham King
* Charles Evans, Jr.
}} John Logan
| based on       =  
| starring       = {{Plainlist|
* Leonardo DiCaprio
* Cate Blanchett
* Kate Beckinsale
* John C. Reilly
* Alec Baldwin
* Alan Alda
* Ian Holm
* Danny Huston
* Gwen Stefani
* Jude Law
 
}}
| music          = Howard Shore Robert Richardson
| editing        = Thelma Schoonmaker
| studio         = {{Plainlist| Forward Pass Appian Way Miramax Films Intermedia
* Initial Entertainment Group Cappa Productions
}}
| distributor    = Warner Bros. Pictures
| runtime        = 170 minutes
| budget         = $110 million
| gross          = $213.7 million   Box Office Mojo. Retrieved January 11, 2011. 
}} biographical drama John Logan Michael Mann, Sandy Climan, Graham King, and Charles Evans, Jr.. It stars Leonardo DiCaprio as Howard Hughes, Cate Blanchett as Katharine Hepburn and Kate Beckinsale as Ava Gardner. The supporting cast features Ian Holm, John C. Reilly, Alec Baldwin, Jude Law as Errol Flynn, Gwen Stefani as Jean Harlow, Willem Dafoe, Alan Alda, and Edward Herrmann.
 Charles Higham, the film depicts the life of Howard Hughes, an aviation pioneer and director of Hells Angels (film)|Hells Angels. The highly stylized film portrays his life between the late 1920s and late 1940s, during which time Hughes became a successful film producer and an aviation magnate while simultaneously growing more unstable due to severe obsessive-compulsive disorder.
 Best Picture, Best Director Best Original Best Actor Best Actor Best Sound Best Cinematography, Best Film Best Costume Best Art Best Actress in a Supporting Role for Cate Blanchett.  This achievement would not be matched for seven years until Martin Scorseses film Hugo (film)|Hugo.

==Plot==
In Houston, 1913, nine-year-old Howard Hughes is warned by his mother of the diseases that she is afraid he will succumb to. Fourteen years later, he begins to direct the movie, Hells Angels (film)|Hell’s Angels, however after the release of The Jazz Singer, the first partially talking film, Hughes becomes obsessed with shooting his film realistically, and decides to convert the movie to a sound film. Despite the film being a hit, Hughes remains unsatisfied with the end result and orders the film to be re-cut after its Hollywood premier. He becomes romantically involved with actress Katharine Hepburn, who helps to ease the symptoms of his worsening OCD|obsessive-compulsive disorder (OCD).
 Pan American World Airways (Pan Am). Trippe gets his friend, Senator Owen Brewster, to introduce the Community Airline Bill, which would give Pan Am exclusivity on international air travel. As Hughes’ fame grows, he is linked to various starlets, provoking Hepburn’s jealousy, later causing them to break up following her announcement that she has fallen in love with fellow actor Spencer Tracy. Hughes quickly finds a new love interest with 15-year-old Faith Domergue, and later actress Ava Gardner.
 troop transport unit. In 1946, with the Hughes H-4 Hercules|"Spruce Goose" flying boat still in construction, Hughes finishes the Hughes XF-11|XF-11 reconnaissance aircraft and takes it for a test flight. With one of the engines malfunctioning mid-flight, he crashes the aircraft in Beverly Hills, getting severely injured. With the end of WWII, the army cancels their order for the H-4 Hercules, although Hughes still continues the development with his own money. When he is discharged, he is told that he has to choose between funding the airlines or his ‘flying boat’, in which he then orders Dietrich to mortgage the TWA assets so he can continue the development.

Hughes grows increasingly paranoid, planting microphones and tapping Gardners phone lines to keep track of her. His home is searched by the FBI for incriminating evidence of war profiteering, provoking a powerful psychological trauma on Hughes, with the men searching his possessions and tracking dirt  through his house. Privately, Brewster offers to drop the charges if Hughes will sell TWA to Trippe, an offer he rejects. With Hughes in a deep depression, Trippe has Brewster summon him for a Senate investigation, as they’re confident that he’ll not show up. After being shut away for nearly three months, Gardner visits Hughes and personally grooms and dresses him in preparation for the hearing.

Hughes defends himself against Brewsters charges and accuses Trippe of bribing the senator. Hughes concludes by announcing that he has committed to completing the H-4 aircraft, and that he will leave the country if he cannot get it to fly. He successfully test flies H-4 aircraft, and after the flight, talks to Dietrich and his engineer, Glenn Odekirk, about a new jetliner for TWA. The sight of men in germ-resistant suits causes Hughes to have a mental breakdown. As Odekirk hides him in a restroom whilst Dietrich fetches a doctor, Hughes begins to have flashbacks of his childhood, his obsession for aviation, and his ambition for success, whilst repeating the phrase, "the way of the future".

==Cast==
 
* Leonardo DiCaprio as Howard Hughes 
* Cate Blanchett as Katharine Hepburn
* John C. Reilly as Noah Dietrich
* Kate Beckinsale as Ava Gardner
* Alec Baldwin as Juan Trippe Senator Owen Brewster
* Ian Holm as Professor Fitz
* Danny Huston as Jack Frye
* Gwen Stefani as Jean Harlow
* Jude Law as Errol Flynn
* Willem Dafoe as Roland Sweet Adam Scott as Johnny Meyer Glenn "Odie" Odekirk
* Kevin ORourke as Spencer Tracy
* Kelli Garner as Faith Domergue Katharine Houghton  Robert E. Gross
* Stanley DeSantis as Louis B. Mayer
* Edward Herrmann as Joseph Breen
* J. C. MacKenzie as Ludlow Ogden Smith
* Josie Maran as Thelma the Cigarette Girl
* Jane Lynch as Amelia Earhart  
 

==Production==

===Development===
 , but development stalled. Hughes, p.144-147 
 James Steele. The Hughes brothers were going to direct Johnny Depp as Howard Hughes, based on a script by Terry Hayes,  Universal canceled it when they decided they did not want to fast track development to complete with Disney. Following the disappointing release of Snake Eyes in August 1998, Disney placed Mr. Hughes in turnaround (filmmaking)|turnaround.
 Michael Mann John Logan. The Insider. New Line Cinema picked it up in turnaround almost immediately, with Mann planning to direct after finishing Ali (film)|Ali.  Mann was eventually replaced with DiCaprios Gangs of New York director Martin Scorsese.

Howard Hughes suffered from obsessive-compulsive disorder (OCD). He had an obsession with germs and cleanliness. In production of the film, Martin Scorsese and Leonardo DiCaprio worked closely with Dr. Jeffrey Schwartz, Ph.D. of UCLA to portray the most accurate depiction of OCD. With the help of Dr. Schwartz their representation of the disorder was accurate and clear. The filmmakers had to focus both on previous accounts of Hughes’ behaviours as well as the time period. At the time when Hughes was suffering there was no psychiatric definition for what ailed him. Instead of receiving proper treatment, Hughes was forced to hide his stigmatized compulsions. His disorder began to conflict with everyday functioning and eventually destroyed his once-successful career.

Leonardo DiCaprio dedicated hundreds of hours of work to portray Howard Hughes’ unique case of OCD on screen. Apart from doing his research on Hughes, DiCaprio met with people suffering from OCD. In particular, he focused on the way some individuals would wash their hands, later inspiring the scene in which he cuts himself scrubbing in the bathroom. The character arc of Howard Hughes was a drastic one: from the height of his career to the appearance of his compulsions and, eventually, to him sitting naked in a screening room, refusing to leave and repeating over the phrase “the way of the future.” DiCaprio successfully found this range of behaviour in his portrayal of Hughes. 

===Cinematography===
  film used in scenes depicting events before 1935.]] colorized and incorporated into the film. The color effects were created by Legend Films. 

===Production design=== CGI special Pearl Harbor (2001) had been a crucial factor in Scorseses decision to use full-scale static and scale models in this case. The building and filming of the flying models proved both cost-effective and timely. Cobb, Jerry.   MSN.com, February 25, 2005. Retrieved March 1, 2008. 
 miniature used reverse engineered from photographs and some rare drawings and then modeled in Rhinoceros 3D by the New Deal art department. These 3D models of the Spruce Goose as well as the XF-11 were then used for patterns and construction drawings for the model makers. In addition to the aircraft, the homes that the XF-11 crashes into were fabricated at 1:4 scale to match the 1:4 scale XF-11. The model was rigged to be crashed and break up several times for different shots. 

Additional castings of the Spruce Goose flying boat and XF-11 models were provided for new radio controlled flying versions assembled by the team of model builders from Aero Telemetry. The Aero Telemetry team was given only three months to complete three models including the 450&nbsp;lb H-1 Racer, with an   wingspan, that had to stand-in for the full-scale replica that was destroyed in a crash, shortly before principal photography began. 
 Long Beach and other California sites from helicopter or raft platforms. The short but much heralded flight of Hughes’ Hughes H-4 Hercules|HK-1 Hercules on November 2, 1947 was realistically recreated in the Port of Long Beach. The motion control Spruce Goose and Hughes Hangar miniatures built by New Deal Studios are presently on display at the Evergreen Aviation Museum in McMinnville, Oregon, with the original Hughes HK-1 "Spruce Goose". 

==Release==

===Distribution===
Miramax Films distributed the film in the United States, the United Kingdom as well as Italy, France and Germany. Miramax also held the rights to the US television distribution, while Warner Bros. Pictures retained the rights for home video/DVD distribution and the theatrical release in Canada and Latin America. Initial Entertainment Group released the film in the remaining territories around the world.  

===Box office performance===
The Aviator was given a limited release on December 17, 2004 in 40 theaters where it grossed $858,021 on its opening weekend.  It was given a wide release on December 25, 2004, and opened in 1,796 theaters in the United States, grossing $4,240,000 on its opening day and $8,631,367 on its opening weekend, ranking #4 with a per theater average of $4,805.   On its second weekend, it moved up to #3 and grossed $11,364,664 – $6,327 per theater. 

The Aviator grossed $102,610,330 in the United States and Canada and $111,131,129 overseas. In total, the film has grossed $213,741,459 worldwide. 

===Home media=== Michael Mann. The second disc includes "The Making of The Aviator ", "Deleted Scenes", "Behind the Scenes", "Scoring The Aviator", "Visual Effects", featurettes on Howard Hughes as well as other special features. The DVD was nominated for Best Audio Commentary (New to DVD) at the DVD Exclusive Awards in 2006. 
 High Definition on Blu-ray Disc and HD DVD on November 6, 2007. 

==Critical response==

The film received highly positive reviews with the review tallying website Rotten Tomatoes reporting that 186 out of the 213 reviews were positive with a score of 87% and certification of "Fresh".  On review aggregator site Metacritic the film scored an average of 77 out of 100, based on 41 reviews.  Roger Ebert of Chicago Sun-Times gave the film four stars out of four and described the film and its subject, Howard Hughes, in these terms: "What a sad man. What brief glory. What an enthralling film, 166 minutes, and it races past. Theres a match here between Scorsese and his subject, perhaps because the directors own life journey allows him to see Howard Hughes with insight, sympathy – and, up to a point, with admiration. This is one of the years best films." 

David T. Courtwright from the University of North Florida characterized Aviator as a technically brilliant and emotionally disturbing film. He argues that the main credit for Martin Scorsese is that he managed to restore the name of Howard Hughes as of a pioneer aviator of the USA. In his opinion, Scorsese and Logan understood and depicted the aviation history well as they understood the paradox of the generation of aviation pioneers. Particularly, their sacrifice of a feeling of adventure in favour of creating risk-free, comfortable aeroplanes.
Furthermore, Scorsese used Hughes’s romances with film stars in order to correctly depict the Hollywood’s interwar culture with its nightlife and its cult of bosomy celebrity. 

===Accolades===
 

==See also==
* The Aviator (soundtrack)|The Aviator (soundtrack)
* Mental illness in film

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Floyd, Jim. The Avro Canada C102 Jetliner. Erin, Ontario: Boston Mills Press, 1986. ISBN 0-919783-66-X.
* Higham, Charles. Howard Hughes: The Secret Life. New York: St. Martins Griffin, 2004. ISBN 978-0-312-32997-6.
* Maguglin, Robert O. Howard Hughes, His Achievements & Legacy: the Authorized Pictorial Biography. Long Beach, California: Wrather Port Properties, 1984. ISBN 0-86679-014-4.
* Marrett, George J. Howard Hughes: Aviator. Annapolis, Maryland: Naval Institute Press, 2004. ISBN 1-59114-510-4.
* Wegg, John. General Dynamic Aircraft and their Predecessors. London: Putnam, 1990. ISBN 0-85177-833-X.
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*   at VFX

 
 
 
 

 
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 