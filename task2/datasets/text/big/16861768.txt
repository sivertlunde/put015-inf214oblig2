Ek Hi Raasta (1993 film)
{{Infobox film
| name           = Ek Hi Raasta
| image          =Ek Hi Raasta.jpg
| image_size     =
| caption        = 
| director       = Deepak Bahry
| producer       = Chander Sadanah
| writer         = Lalit Mahajan  Sumeet Malhotra
| narrator       = 
| starring       = Ajay Devgan  Raveena Tandon   Raza Murad   Saeed Jaffrey
| music          = Mahesh Kishor
| cinematography = Damodar Naidu   
| editing        = Nazir Hakim
| distributor    = Tips Music Films
| released       = 1993
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1993 Bollywood action film about a terrorist with aspirations to rule India. Directed by Deepak Bahry, the film is a remake of the  Ek Hi Raasta (1977) which starred Rekha. Films under the title were also released first in 1939 and then in 1956. The film stars Ajay Devgan, Raveena Tandon, Raza Murad and Saeed Jaffrey.

==Synopsis==
Kubla(Raza Murad) is a terrorist and would like to rule over India. To do this, he needs vital information about the Indian armed forces, and he recruits an Indian army officer, Vikram(Mohnish Bahl). To get this information, Vikram kills another soldier and ensures that Karan Singh (Ajay Devgan), yet another army man, gets the blame. This goes according to plan, and Karan Singh is on the run. Then Kubla and Vikram abduct Col. Choudry(Saeed Jaffrey) and his daughter, Priya(Raveena Tandon). They give Bhagat(Kulbhushan Kharbanda) Singh two-and-a-half hours to locate the information or else the Colonel will be killed, and Priya (Raveena Tandon) raped. (Retrieved from IMDB)

==Cast==
*Ajay Devgan as Karan Singh 
*Raveena Tandon as Priya Choudhry 
*Raza Murad as  Kubla 
*Saeed Jaffrey as  Colonel Choudhry 
*Deven Verma as Mehra 
*Kulbhushan Kharbanda as  Bhagat Singh 
*Mohnish Bahl as Vikram Singh 
*Sharat Saxena as  Army officer

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! Lyric
|-
| 1
| "Meri Seeti Bas Gayee"
| Kumar Sanu, Sadhana Sargam
| Gulshan Bawra
|-
| 2
| "Kaliyon Ke Khilne Ka"
| Udit Narayan, Bela Sulakhe
| Rani Malik
|-
| 3
| "Aankh Mere Yaar Ki Dukhe"
| Pankaj Udhas
| Naqsh Lyallpuri
|-
| 4
| "Tere Mere Pyar Ka Kissa"
| Kumar Sanu, Bela Sulakhe
| Kulwant Jani
|-
| 5
| "Yaar Ko Milne Jaana Hai"
| Vinod Rathod, Chorus
| Dev Kohli
|-
| 6
| "Aankh Mere Yaar Ki Dukhe"
| Pankaj Udhas, Kavita Krishnamurthy
| Naqsh Lyallpuri
|-
| 7
| "Pyar Ghazab Ki Cheez Hai"
| Vipin Sachdeva, Chorus
| Dev Kohli
|}

==External links==
*  

 
 
 
 


 
 