The Talent Given Us
{{Infobox film
| name           = The Talent Given Us
| image          = Talentofus One sheet.jpg
| caption        = 
| director       = Andrew Wagner
| producer       = 
| writer         = Andrew Wagner
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
}}
The Talent Given Us is a film by Andrew Wagner. Most of the cast in the movie is Wagners own family.

Roger Ebert praised "The Talent Given Us" as "one of the most original, daring, intriguing, and honest films of the year," while 2004 CineVegas juror Wendy Mitchell, writing about the film for indieWIRE last year, said that the movie "could qualify as the bravest movie I have ever seen." It was later named to indieWIREs list of the best films of 2004 without distribution.   

== References ==
 

==External links==
*  
*  

 
 
 


 