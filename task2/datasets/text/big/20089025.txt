Speedy Delivery
{{Infobox Film
| name=Speedy Delivery
| image=Poster of Speedy Delivery.jpg
| director=Paul B. Germain
| writer=
| starring= David Newell   Fred Rogers
| producer= Paul B. Geramin   Stuart Dean Friedel
| music= Bryan Senti
| cinematography= Paul B. Germain Stuart Dean Friedel
| editing= Paul B. Germain
| distributor=
| released= 
| runtime=72 minutes
| language=English
| budget=$4,000 
}}
Speedy Delivery is a 2008 documentary film directed and produced by Paul B. Germain. The film follows the life story of David Newell, better known as Mr. McFeely from the childrens television show Mister Rogers Neighborhood. 

Shot in   and Pomona College.   Currently, Speedy Delivery is airing regionally on PBS and has screened at The Feel Good Film Festival founded by Kristen Ridgway Flores, Flyway Film Festival, and screened at the 2009 Seattle Childrens Film Festival.   

==Plot==
Speedy Delivery follows David Newell/Mr. McFeely around his own neighborhood, Pittsburgh, Pennsylvania, examining the two roles he has played for over forty years. Intercut with retrospective interviews, the film explores David Newells life of service, as both playing the character Mr. McFeely and as director of public relations for Family Communications, Inc., the parent company founded by Fred Rogers.  This company created and produced Mister Rogers Neighborhood during the majority of its years on air since its debut in 1967. 

The film follows David Newell on four appearances located in Pennsylvania and Maryland. Beginning in Pittsburgh, he appears at the opening of the Schenley Plaza Carousel, meeting and greeting with fans. Next, he travels to Baltimore, Maryland to help open up a special Mister Rogers traveling exhibit, including a replica of the set. Finally, he returns to Pittsburgh for two more appearances in Bridgeville, Pennsylvania and Cranberry Park. All along the way, the film uses retrospective interviews with former cast members and current co-workers to discuss topics like Newells childhood, the passing of Fred Rogers, where the character Mr. McFeely came from and what kept Mister Rogers Neighborhood successful and authentic for over four decades.

==Alternate versions==
There are two cuts of Speedy Delivery. The theatrical cut runs approximately one hour and twelve minutes. The television cut runs approximately fifty seven minutes, standard for PBS specifications. The theatrical cut is available on DVD and includes a directors commentary.

==On television==
Speedy Delivery is airing regionally on PBS stations across the nation. 

==References==
 

==External links==
*  
*  
*  
*   Greene County Messenger
*   Roanoke Times
*   Lansing State Journal
*   Independent Films Direct
*   Pittsburgh Post Gazette
*   Pitt News
*   The Lafayette
*   Lafayette Campus News
*   Yale Daily News
*   The Tartan
*   Lafayette Alumni News
*   Pomona College Magazine

 

 
 
 
 
 