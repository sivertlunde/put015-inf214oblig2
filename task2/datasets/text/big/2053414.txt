Last Night (1998 film)
{{Infobox film name           = Last Night image          = Last Night poster.jpg caption        = Theatrical poster director       = Don McKellar producer       = Niv Fichman Daniel Iron writer         = Don McKellar starring       = Don McKellar Sandra Oh Tracy Wright Callum Keith Rennie Sarah Polley David Cronenberg music          = Alexina Louie Alex Pauk cinematography = Douglas Koch editing        = Reginald Harkema studio         = Rhombus Media La Sept Canadian Broadcasting Corporation distributor  Lionsgate
|released       =   runtime        = 95 minutes country        = Canada language       = English budget         = $2,300,000 CAD  ($1,600,000 USD) gross     = $591,165 
}}

Last Night is a 1998 Canadian drama film by Don McKellar. It was filmed in Toronto.

== Plot ==
Set in Toronto at an unknown date, Last Night tells the story of how a variety of intersecting characters spend their final evening on Earth. It seems the world is to end at midnight as the result of a calamity that is not explained, but which has been expected for several months. There are several scenes of an ominously glowing sun, which gets progressively larger and brighter even into the night.
 streetcar who is paralyzed by despair.

In the climax of the film, Patrick and Sandra decide to fulfill the suicide pact that her husband was unable to complete. As midnight approaches, they both sit on the roof facing each other, listening to the song "Guantanamera", each holding a loaded pistol to the others temple. As the final minutes approach, Sandra implores Patrick to resolutely carry out the pact. But as the final seconds approach, both characters are overcome with emotion and simultaneously let their pistols slip away as they slowly embrace in a kiss and the last moments of all the major characters are seen. It is at this moment that the world, and the lives of all of its characters, end.

== Awards ==
Among its 12 awards, it won the "Award of the Youth" at the   for best feature film by a first-time director (Don McKellar), and the Genies for "Best Performance by an Actress in a Leading Role" (Sandra Oh) and "Best Performance by an Actor in a Supporting Role" (Callum Keith Rennie). In 2002, readers of Playback (magazine)|Playback magazine voted Last Night the ninth greatest Canadian film of all-time.  . Playback. Retrieved March 03, 2014
 

== Cast ==
*Don McKellar as Patrick Wheeler
*Sandra Oh as Sandra
*Callum Keith Rennie as Craig Zwiller
*Sarah Polley as Jennifer Jenny Wheeler
*David Cronenberg as Duncan
*Geneviève Bujold as Mme. Carlton
*Arsinée Khanjian as Streetcar Mother
*Jackie Burroughs as The Runner
*Tracy Wright as Donna

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 