Convention City
{{Infobox film
|  name           = Convention City 
|  image	  = Convention City FilmPoster.jpeg
|  producer       = Henry Blanke
|  director       = Archie Mayo 
|  writer         = Robert Lord
|  starring       = Joan Blondell Guy Kibbee Dick Powell Mary Astor Adolphe Menjou
|  studio         = First National Pictures
|  distributor    = Warner Bros.
|  released       =  
|  country        = United States
|  language       = English
|  runtime        = 69 minutes
|  budget         = $239,000
}} 1933 American pre-Code sex comedy film directed by Archie Mayo, and starring Joan Blondell, Guy Kibbee, Dick Powell, Mary Astor and Adolphe Menjou. The film was produced by Henry Blanke and First National Pictures and distributed by Warner Bros.  
 lost and has become one of the more coveted lost films because of its reported racy content.   

== Plot ==
The plot revolves around the convention of the Honeywell Rubber Company in  ) interferes with his attempted seduction of J.B.s daughter and George attempts to seduce Nancy Lorraine (Joan Blondell). The position of sales manager is bestowed upon a drunken employee as a bribe after he catches J.B. about to visit "Daisy La Rue, Exterminator".   

== Cast ==
*Joan Blondell as Nancy Lorraine
*Adolphe Menjou as T.R. (Ted) Kent
*Dick Powell as Jerry Ford
*Mary Astor as Arlene Dale
*Guy Kibbee as George Ellerbe
*Frank McHugh as Will Goodwin
*Patricia Ellis as Claire Honeywell
*Ruth Donnelly as Mrs. Ellerbe
*Hugh Herbert as Hotstetter Grant Mitchell as J.B. Honeywell

==Production== double meanings. Finally, they would have people in the front office just watch for what wed say off-color."    Several lines were ordered to be removed by the MPPDA censors including:

:"No, but it wont be marriage. Ill guarantee you that. A traveling salesman needs a wife like a baby needs a box of matches."
:"Now you take off that dress and Ill take off my toupee, huh!"
 
:"Girls voice: Listen, sister, if they tire you, you better leave town before the Hercules Tool Company gets here." 

In a studio memo, Jack Warner warned producer Hal Wallis that he may be going too far in the costuming:

:"We must put brassieres on Joan Blondell and make her cover up her breasts because, otherwise, we are going to have these pictures stopped in a lot of places. I believe in showing their forms but, for Lords sake, dont let those bulbs stick out." 

Script changes, suggested by the chair of the Motion Picture Division of the State of New York Department of Education Dr. James Wingate and Jason S. Joy, director of the Studio Relations Committee, and production head Hal Wallis were nominally incorporated into the script.   

== Reception ==
===Critical reviews===
Dr. James Wingate, chair of the Motion Picture Division of the State of New York Department of Education — which oversaw the states censorship board — described it as "a pretty rowdy picture, dealing largely with drunkenness, blackmail, and lechery, and without any particularly sympathetic characters or elements."    
 Will Hays is doing." 

The New York Times said that, "Several of the jokes need a subterranean mind to be correctly understood. An accurate appraisal of Convention City should include the information that the Strand Theatre (Brooklyn)|Strands audiences laughed long and loud." The Times also praised Adolphe Menjous performance while finding Joan Blondells performances to be getting tiresome as she was playing the same irreverent character in her films. 

===Box office===
Convention Citys cost $239,000 to produce, and earned $384,000 in domestic revenue and $138,000 from foreign release, for an eventual profit of $53,000.   

==Survival status == Depression but was rarely enforced as financially strapped studios often overlooked its authority in the want to make more risque pictures that were good box office.  

As a result of the Code, the film was taken out of circulation when its theatre run ended.  The controversy surrounding the film prompted exhibitors and theatre convention organizers to request the film.  

In 1936, Warner attempted to re-release Convention City in a censored form, but Breen deemed it beyond redemption and would not grant it the seal of approval needed for the film to run in theatres. Warner then reportedly ordered that the film be destroyed.  However, Ron Hutchinson, the editor of the newsletter The Vitaphone Project, discovered that a print of the film was screened in 1937, nearly three years after the ban had been enacted indicating that at least one print had not been taken out of circulation and was not destroyed.    Further evidence that the all prints of the film were not seized and destoryed was found in the August 29, 1942 edition of the Spanish newspaper ABC (newspaper)|ABC. According to an advertisement, the film (known as  ¡Que Semana! in Spanish) was being screened at the Muñoz Seca Theater in Madrid. 

In the late 1949s and 1950s, Warner Bros. destroyed many of its negatives due to nitrate film decomposition. Studio records indicate that the negative of Convention City and filmography pre-1931 was marked "Junked 12/27/48" (December 27, 1948).  No prints of the film are known to currently exist, though rumors that private collectors who own foreign prints have continued to surface as late as 1999. 
 United Artists UA by Ted Turner).

The original screenplay still survives in the Warner Bros. script archives. In March 1994, a pre-code film festival was held in South Village. Among the films viewed, was a dramatic reading of the screenplay for Convention City. 

==See also==
*List of lost films

==References==
 

==External links==
*  
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 