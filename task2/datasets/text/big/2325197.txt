Toolbox Murders
 
{{Infobox film
| name           = Toolbox Murders
| image          = ToolboxMurders.jpg
| image_size     = 
| caption        = DVD released by Lionsgate Films
| director       = Tobe Hooper
| producer       = Tony DiDio   Gary LaPoten   Terence S. Potter   Jacqueline Quella
| writer         = Jace Anderson   Adam Gierasch
| based on       =   Marco Rodriguez
| music          = Joseph Conlan
| cinematography = Steve Yedlin
| editing        = Andrew Cohen
| studio         = Alpine Pictures   Scary Movies LLC   Toolbox Murders, Inc. Columbia TriStar Film Distribution International (Spain, theatrical)  Paramount Home Entertainment (Spain, home video)
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 1978 film of the same name and was produced by the same people behind the original. The film centralizes on the occupants of an apartment who are stalked and murdered by a masked killer.

== Plot == medical intern, amenities and a few of the residents by Byron, the building manager. As Steven works long hours, Nell is left alone most of the time, and befriends Julia Cunningham, a neighbor down the hall, and Chas Rooker, an elderly man who reveals some of the history of the structure, mentioning it was made by Jack Lusman, who disappeared mysteriously, and that builders died while working on it.

As days pass, the Lusman Arms are plagued by mysterious circumstance; strange noises are heard throughout it and coming from the intercoms, Nell finds a trinket containing human teeth in a wall, and Julia and another tenant vanish, having been killed (with a drill and a nail gun, respectively) by the ski masked murderer, who hides their bodies. While looking into Julias disappearance, Nell speaks with Chas, who offers cryptic warnings about the nature of the building, and sneaks Nell a note reading "Look for her in Room 504". Nell takes the advice, and discovers that there is no Room 504, and that all the other floors lack apartments whose numbering should end with 4.
 Golden Age of Hollywood, torture chambers, and dozens of corpses. The killer, who had just butchered another tenant and the handyman, appears, and removes his mask to reveal that he is a monster, which the credits refer to as "Coffin Baby".

A teenage resident discovers that the webcam he had been using to spy on Julia had recorded her death, prompting him to go to Steven, who finds Nells notes about the building, and goes looking for her along with the boy, Byron, and the Doorman (profession)|doorman. The men send the teen to get the police after they find a passageway into Coffin Babys lair, which they enter. Coffin Baby kills Byron and the doorman, and gives chase to Nell and Steven, the former of whom theorizes that Coffin Baby needs death and the Lusman Arms to continue existing. The Barrows are found by Chas, who tries to lead them to safety, and murmurs that Coffin Baby came into the world when he clawed his way out of his dead and buried mothers womb.

Coffin Baby leaps out from under a pile of human remains, fatally throws Chas at a wall, and captures Nell, but she is saved by Steven, who bludgeons Coffin Baby, and knocks a shelf onto him. The authorities arrive, and take Steven to a hospital, and as Nell returns to her apartment, the police lift up the debris that fell on Coffin Baby, who has disappeared. Coffin Baby crashes through Nells window and tries to kill her, but is disoriented by the runes she had earlier drawn on her arms, distracting him long enough for a pair of police officers to barge in, and shoot him out a window, causing him to be hanged by a cord Nell had wrapped around his neck. The officers check on Nell, then go to the window, only to find that Coffin Baby has once again vanished.

== Cast ==
* Angela Bettis as Nell Barrows
* Brent Roam as Steven Barrows Marco Rodríguez as Luis Saucedo
* Rance Howard as Charles "Chas" Rooker
* Juliet Landau as Julia Cunningham
* Adam Gierasch as Ned Lundy
* Greg Travis as Byron McLieb
* Christopher Doyle as Coffin Baby
* Adam Weisman as Austin Sterling
* Christina Venuti as Jennifer
* Sara Downing as Saffron Kirby
* Jamison Reeves as Hudson
* Stephanie Silverman as Dora Sterling
* Alan Polonsky as Philip Sterling
* Charlie Paulson as Hans
* Eric Ladin as Johnny Turnbull
* Sheri Moon Zombie as Daisy Rain
* Price Carson as Officer Daniel Stone
* Carlease Burke as Officer Melody Jacobs
* Bob McMinn as Shadow Man
* Ralph Morris as Nells Father
==Production==
 
== Release == limited theatrical release in 2003 and 2004. The film was put out on DVD in the United States in 2005. It was originally rated NC-17, but some gory moments were trimmed for an R rating. The scenes were released as extras on the US release of the Toolbox Murders DVD.

== Reception ==
  mean score of 5.9/10. 
 splatter genre in any significant way, but the chills and kills prove Hooper, when armed with the right script, can still tighten the fright screws". 

Jason Buchanan from Allmovie gave the film 3 / 5 stars calling it "a pretty fun but inconsequential horror-flavored diversion". Buchanan also commented "The kills in The Toolbox Murders are both creative and brutal even when they are wildly unrealistic and even if Hoopers unique vision fails to maintain the stark realism of the original, its flawed logic can make for an interesting ride for viewers with the ability to check their brains at the door and accept a film that departs from the original to exist solely on its own terms". 

TV Guide gave the film a negative review, awarding the film 2 / 4 stars calling it "an excuse for a series of stalk-and-slash scenes". 
== Sequels ==
Toolbox Murders was followed by a 2013 sequel entitled Toolbox Murders 2;  however, U.S. release of that film has been held up by litigation with its director, Dean Jones, who took the principal photography and other footage filmed for Toolbox Murders 2 without the permission of the producers and used it to create a separate film entitled Coffin Baby based on a reworking of the Toolbox Murders script.  Although the killer in the 2004 Toolbox Murders film (christopher Doyle) is named Coffin Baby in the credits of that movie, Jones claims that his Coffin Baby movie is completely independent of the Toolbox Murders franchise. Despite the stalled release of Toolbox Murders 2, its producers are currently developing Toolbox Murders 3.

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 