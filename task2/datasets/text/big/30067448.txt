Golden Rendezvous
 
 
{{Infobox film
| name           = Golden Rendezvous
| image          = Golden Rendezvous dvd cover.jpg
| caption        = DVD cover 
| alt            =  Allan Scott, Chris Bryant, John Gay, Stanley Price Gordon Jackson 
| director       = Ashley Lazarus 
| producer       = Murray Frank, Andre Pieterse, Robert Porter
| distributor    =  United Artists (USA), Rank Film Distributors (UK)
| released       =  
| runtime        = 109 minutes (theatrical release)
| music          = Jeff Wayne
| cinematography = Kenneth Higgins
| editing        = Ralph Kemplin
| studio         = 
| language       = English 
| budget         = 
| gross          = 
}}
 Gordon Jackson.  It was based on the 1962 novel The Golden Rendezvous by Alistair MacLean.

==Plot==
The Caribbean Star, a combination cargo ship and floating casino is hijacked by terrorists led by Luis Carreras (John Vernon), who installs an atomic bomb, holding both the passengers and the bomb hostage, hoping to exchange them for the gold bullion on an U.S. Treasury ship. However, First Officer John Carter (Richard Harris), Susan Beresford (Ann Turkel), and Dr. Marston (Gordon Jackson) join forces to foil the plan.

==Cast==
* Richard Harris - John Carter 
* Ann Turkel - Susan Beresford  Gordon Jackson - Dr. Marston 
* John Vernon - Luis Carreras 
* David Janssen - Charles Conway 
* Burgess Meredith - Van Heurden 
* Leigh Lawson - Tony Cerdan 
* Robert Flemyng - Capt. Bullen  Keith Baxter - Preston 
* Robert Beatty - Dr. Taubman 
* Dorothy Malone - Mrs. Skinner 
* John Carradine - Fairweather 
* Chris Chittell - Rogers  Richard Cox - Browning  Michael Howard - Benson Larry Taylor - Attacker

==References==
 

==External links==
* 
* 
* 
 

 
 
 
 
 
 
 
 


 