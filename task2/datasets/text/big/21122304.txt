Princess Arete
{{Infobox film
| name = Princess Arete
| film name = {{Film name| kanji = アリーテ姫
| romaji = Arīte Hime}}
| image = 
| caption =
| director = Sunao Katabuchi
| producer = Eiko Tanaka
| screenplay =
| based on =  
| starring =
| music = Akira Senju
| cinematography =
| editing =
| studio = Studio 4°C
| distributor =
| released =  
| runtime = 105 minutes 
| country = Japan
| language = Japanese
| budget =
| gross =
}}

  (also Princess Arite) is a 2001 animated film released by Japanese animation studio Studio 4°C based on Diana Coless 1983 story The Clever Princess. The film is a non-traditional approach to the standard tales of fairy tale princesses, and it is known in Japan as one of the most successful animated feminist works. 

==Details==
 
Princess Arete was released in theaters in 2001 by Studio 4°C. The film is 105 minutes in length. It was awarded the first "New Century Tokyo International Anime Fair 21" award for excellence in the "Theatrical Release" category. The film is based on an original story by Diana Coles entitled "The Clever Princess", renamed to   in Japan. The plot of the film is changed substantially from the original, mostly because director Sunao Katabuchi, when he came on board the project, felt uncomfortable telling a feminist story from a male point of view, and thus focused on more general themes such as individualism, respect, diversity. The storyline itself remains that of a princess living in a tower pursued by suitors, and a wicked magician who takes her away.

The films plot has been compared to those of films produced by Studio Ghibli,  however the film has also been criticized for its slow pacing.  . T.H.E.M. Anime. 2001. 

==Plot==
Under the rule of her solemn father the King, the young Princess Arete grows up in a lonely tower awaiting a suitor worthy of her. While the suitors accomplish countless treasure-collecting quests for the King, the princess begins to experience an awakening curiosity in the outside world. Sneaking out of the castle several times she begins to recognize that she has been missing out on life while locked away in the isolation of her tower. When her life is disrupted by the arrival of several very enthusiastic suitors, Arete makes up her mind to escape into the wider world however she is caught by the guards and sent before her father.

Just before she is brought to him, the King has met with a powerful sorcerer named Boax to discuss his proposal that Princess Arete be betrothed to the sorcerer. Although the King hesitates at first, he eventually gives in after Boax enchants her into becoming a traditional princess and accepting his advances. Boax then takes the princess to his far away castle prior to the wedding. There he reveals that he does not love her and that the only reason he has brought her here is to imprison her in a dungeon as he is familiar with a prophecy that a princess named Arete will have the power to take away his eternal life.

As Arete sits in her new lonely surroundings, she slowly becomes friends with the one sympathetic person in Boax castle - a villager named Ample. Ample explains to Arete that Boax has arranged with the nearby town that he will provide them with water for their fields so long as they feed him. Despite this arrangement, Ample reveals that she has begun digging her own well and that soon she will be free from dependence on Boax magic. Encouraged by Ample, Arete at last loses the enchantment Boax had placed on her and regains her old spirits. Soon after she is released by Boax who still believes her to be enchanted and sent on a magical quest of her own. Returning in secret to Boax castle, Arete instead discovers the source of Boax magical ability to create water for the town. In trying to recover the source of the water, Arete is discovered by Boax who in a fit of despondency destroys his own castle releasing water across the land.

With the towns health restored, Arete is free to leave so she decides to go abroad into the countryside and to live together with the country people.

==Characters==
*Princess Arete (Hōko Kuwashima) - The young princess of the kingdom. Arete is filled with great interest in life, however as a princess she is expected to live a life she considers hopelessly mundane. As she has grown up, her role in the kingdom has grown in importance and she is moved to live on top of a high tower to enjoy a privileged seclusion from the facts and practicalities of the real world. From here she can only look down upon the vital town below and dream of traveling the world that she has read about in a book she keeps hidden under her bed. This seclusion has engendered strong curiosity about her in the town, and knights compete daily for her hand by questing for magical objects created by an ancient race. To relieve her isolation, the princess begins to sneaks out of the tower to visit the town.
*Boax (Takeshi Koyama) - A wizard who has suddenly arrived at the castle on a flying machine to claim the hand of the princess. In fact, Boax does not love the princess at all, and only plots to kidnap her because a prophecy foretells that only she has the power to take away his eternal life. Boax changes her appearance to that of a more traditional princess and enchants her into saying that she wishes to marry him. This at last persuades the King to allow him to take her away. He then imprisons her in his dungeon.
**Young Boax (Akio Suyama)
*The witch (Satomi Koorogi) - Witch meets the princess in the secret corridor of the castle. There she imparts a magical, wish-granting ring to Arete.
*Ample (Minami Takayama) - A village woman who lives near Boax castle. She is responsible for bringing food to Boax in exchange for which Boax magically supplies the village with water. She believes in Arete and the two become friends.
*Grovel (Yūsuke Numata) - Boaxs servant. Grovel was once a frog however he was changed into a human-like character through Boax magic.
*Dullabore (Eiji Takemoto) - The first suitor that Arete meets. He attempts to impress her with tales of derring-do, however Arete sees through his posturing.
*Blond Knight (Norihisa Mori) - The second suitor that Arete meets. He attempts to fool her with a rose he has stolen from the castle gardens, however Arete calls his bluff and sends him away.
*King (Takashi Nagasako) - Aretes somber father. The King takes little part in the action, only serving to attempt to marry her off.
*Suitors (Tomohisa Asō, Takkō Ishimori, Masaya Takatsuka) - Aretes other suitors.
*Guard (Yoshikazu Nagano, Kōhei Kowada) - The Kings guards who keep watch over Aretes tower and the rest of the town.
*Maid (Abigail?) (Naomi Shindō) - Aretes head maid.
*Master Tailor (Kazuhiko Nishimatsu) - One of the first townspeople Arete meets. He asks her if she wants a job sewing but when Arete says no the tailor sends her away.
*Tailor apprentices (Umi Tenjin, nobuko) - Children from the town who are learning the trade. Arete has seen them from her lofty window in the tower and is naturally curious about them.
*Narrator (Yūko Sasaki (voice actress)|Yūko Sasaki) - All narration is presented in the form of epic songs.

==Staff==
* Planning: Akira Yamashita, Takano, Tooya Nobuyuki, Eiko Tanaka, Toyoyuki Hama
* Production: Fumio Ueda, Masafumi Hukui, Takehiko Tino, Mari Sakurai, Hiraku Ushiyama
* Original Story: Diana Coles "The Adventures of Princess Arete" (published 1983)
* Director, Screenplay: Sunao Katabuchi
* Character design: Satoko Morikawa
* Animation Director: Kazutaka Ozaki
* Art Director: Minoru Nishida
* CGI Director: Kei Sasakawa
* Color settings: Lin Ya
* Sound Director: Hayase Susugi Hiroshi
* Music: Akira Senju
* Editing: Takeshi Seyama
* Producer: Eiko Tanaka
* Associate Producer: Ryōichi Fukuyama
* Manufacturing: Production Committee Arete, Beyond C., Dentsu, Shogakukan, Imagica, Omega Project
* Production: Studio 4°C
* Distribution: Omega Entertainment

==Theme songs==
* "Wings of Gold"
** Lyrics: Taeko Oonuki
** Composer, Arranger: Akira Senju
* "Krasnoe Solntse" (Red Sun)
** Lyrics: Origa
** Composer, Arranger: Akira Senju

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 