Deal of a Lifetime
{{Infobox film
| name        = Deal of a Lifetime
| image       = Dealoflicover.jpg
| caption     = Deal of a Lifetime US DVD Cover
| starring    = Kevin Pollak Shiri Appleby Michael A. Goorjian 
| director    = Paul Levine
| producer    = Daniel Helberg, Yorman Pelman
| distributor = Mgm/Ua Studios
| released    =  
| country     = United States
| runtime     = 94 min.
| language    = English
}} romantic comedy film starring Shiri Appleby, Michael A. Goorjian, and Kevin Pollak.

The film centers on the main character Henry Spooner (played by Michael A. Goorjian), the school nerd who has a crush on Lori, the prettiest, most popular girl in his high school. After a conversation with his friend he mutters under his breath that he would sell his soul to the Devil to go out with Lori. From that point on, he gets his wish to date Lori, but things dont go according to plan.

Part of the soundtrack includes the song "Let Yourself Go", which was written and performed by L.A. musician Paul Delph for his final album A God That Can Dance. The song is included in the final scene. 

== References ==
 

==External links==
*  

 
 
 


 