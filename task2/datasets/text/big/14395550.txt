Chop Suey & Co.
 
{{Infobox film
| name           = Chop Suey & Co.
| image          = 
| caption        = 
| director       = Hal Roach
| producer       = Hal Roach
| writer         = H. M. Walker
| starring       = Harold Lloyd
| music          = 
| cinematography = Walter Lundin
| editing        = 
| distributor    = Pathé Exchange
| released       =  
| runtime        = 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd.   

==Cast==
* Harold Lloyd - The Boy
* Snub Pollard 
* Bebe Daniels  
* Sammy Brooks
* Lige Conley - (as Lige Cromley)
* Wallace Howe
* Bud Jamison
* Dee Lampton
* Marie Mosquini
* Fred C. Newmeyer
* James Parrott
* Noah Young

==See also==
* List of American films of 1919
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 


 