Johnny Doughboy
  }}

{{Infobox film
| name           = Johnny Doughboy
| image          = 
| image_size     = 
| caption        = 
| director       = John H. Auer
| producer       = John H. Auer
| writer         = Lawrence Kimble Frederick Kohner
| narrator       = 
| starring       = Jane Withers
| music          = 
| cinematography = John Alton
| editing        = Wallace Grissel
| studio         = 
| distributor    = Republic Pictures
| released       = December 31, 1942
| runtime        = 64 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Johnny Doughboy is a 1942 film directed by John H. Auer for Republic Pictures and starring Jane Withers, and featuring cameos by numerous former child stars. Best Musical Score. 

== Plot summary ==
Sixteen-year-old actress Ann Winters (Jane Withers), who has been a movie star since childhood, declares that she no longer wants to play "childish" roles. Her attitude irritates her agent, Harry Fabian (William Demarest), who tells her that the studio has decided that her next picture will be Ann of Honeysuckle Farm, in which she is to play a twelve-year-old. Ann leaves a note for her sympathetic secretary, "Biggy" Biggsworth (Ruth Donnelly), saying that she does not want a career if she cannot have a life, too, then runs away for a two-week vacation. Just then, innocent Penny Ryan (also Jane Withers) from Oriole, NE arrives. Penny is the winner of a contest concocted by Harry in which the fan who most resembles Ann gets to spend two weeks with her in her home. Harry and Biggy are stunned by Pennys appearance, for she is an exact double for Ann. Harry decides to induce Penny to impersonate Ann, who is to start her new film the next day or else be suspended by the studio.

Pennys first test comes shortly after her arrival, with the visit of "The 20 Minus Club," a group of former child stars who have not enjoyed Anns continued success. Included in the group is Johnny Kelly (Patrick Brook), Anns former co-star and boyfriend. The kids plead with Penny, whom they believe is Ann, to join their "Junior Victory Caravan" show, with which they want to entertain the troops. Harry signals to Penny that Ann could not get involved with such a project and Penny reluctantly declines. While Penny is settling into her life as Ann, the real Ann runs out of gas on a mountain road and goes to a nearby house, which is the residence of playwright Oliver Lawrence (Henry Wilcoxon). Ann tries to hide her identity so that Oliver will not regard her as a child, and he agrees to let her spend her holiday at his home, with his housekeeper Mammy (Etta McDaniel) as chaperone. Ann develops a crush on the charming writer, while Penny spends more time with The 20 Minus Club. After viewing their show, Penny really believes in the kids, but must deflate their hopes by saying that she is above such amaturish endeavors.

Disillusioned by Hollywood scheming, Penny decides to return home, but Biggy reveals Anns whereabouts and suggests that Penny consult her about the show. Penny rounds up the gang, and Johnny drives them to Lawrences home. Ann is shocked by Pennys appearance, but quickly asserts that she and Oliver are practically engaged, and that she has given up film work for the theater. Oliver overhears their conversation and realizes the seriousness of Anns crush on him. He tells Penny not to give up hope, then sends for his daughter Jennifer (Joline Westbrook). Oliver tells Ann that Jennifer is "the woman in his life," and Ann, believing that Jennifer is his girl friend, runs outside. There she meets Johnny, who thinks that her tearful mien is because she wants to do the show but cannot because of her own career pressures. Johnny tells her how "swell" he thinks she is, and Ann realizes how much he and the other kids mean to her. Soon after, Ann, Johnny and the kids stage their show, and Penny is a member of the appreciative audience.

==Cast==
*Jane Withers as Ann Winters, and Penelope Ryan
*Henry Wilcoxon as Oliver Lawrence
*Patrick Brook as Johnny Kelly
*William Demarest as Harry Fabian
*Ruth Donnelly as "Biggy" Biggsworth
*Etta McDaniel as Mammy
*Joline Westbrook as Jennifer Lawrence
*Bobby Breen as Himself
*Baby Sandy as Herself
*Carl "Alfalfa" Switzer|"Alfalfa" Switzer as Alfalfa
*George "Spanky" McFarland|"Spanky" McFarland as Spanky
*Kenneth Brown as Butch
*Billy Lenhart as Buddy
*Cora Sue Collins as Herself
*Robert Coogan as Himself
*Grace Costello as Herself
*The Falkner Orchestra as Themselves
*Karl Kiffe as Himself

==Notes==
The fictional "20 Minus Club" is depicted as an organization for once famous child stars. The members (all of whom portrayed themselves) really were Hollywood has-beens, and this would be among the last films many of these performers appeared in. Members include actor/singer Bobby Breen, four-year-old "Baby Sandy" (Universal Pictures answer to Shirley Temple), former Our Gang stars Carl "Alfalfa" Switzer and George "Spanky" McFarland (reunited on film for the final time), musicians "Butch" and "Buddy" (Kenneth Brown and Billy Lenhart), actress Cora Sue Collins, actor Robert Coogan, dancer Grace Costello, drummer Karl Kiffe and the Faulkner Orchestra.

== External links ==
* 
*  

==References==
 

 

 
 
 
 
 
 
 