I Killed Wild Bill Hickok
{{Infobox Film
| name           = I Killed Wild Bill Hickok
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Talmadge
| producer       = Johnny Carpenter
| writer         = Johnny Carpenter
| narrator       = 
| starring       = See below
| music          = Darrell Calker
| cinematography = Virgil Miller
| editing        = Maurice Wright
| distributor    = 
| released       = 16 June 1956
| runtime        = 63 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 Tom Brown) who is the villain of the film.  The film was produced and written by Johnny Carpenter who also stars and narrates under the name John Forbes.   The film is the second of two films produced by The Wheeler Company.

== Cast ==
*Johnny Carpenter as Johnny Rebel Savage
*Denver Pyle as Jim Bailey
*Virginia Gibson as Anne James Tom Brown as Sheriff Wild Bill Hickok
*Helen Westcott as Bella Longtree
*I. Stanford Jolley as Henry Longtree
*Frank Red Carpenter as Ring Pardo
*Roy Canada as Nato, the Indian
*Harvey B. Dunn as Dr. Reed
*Lee Sheldon as Kate Savage
*Phil Barton as Pancho, chief thug
*William Mims as Dan Bevins, a rancher
*R.J. Thomas as Tommy, the blacksmith
*Bill Chaney as Tex, a cowpoke

==Notes==
 

== External links ==
* 
* 

 
 
 
 
 
 


 