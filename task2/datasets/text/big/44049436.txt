Vayal
{{Infobox film 
| name           = Vayal
| image          =
| caption        =
| director       = Antony Eastman
| producer       = MD Mathew
| writer         = Kaloor Dennis
| screenplay     = Kaloor Dennis Shubha Cochin Haneefa
| music          = G. Devarajan
| cinematography = C Ramachandra Menon
| editing        = NP Suresh
| studio         = Arpana Films
| distributor    = Arpana Films
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, Shubha and Cochin Haneefa in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Kaviyoor Ponnamma as Saraswathi
*Sankaradi as Kaimal Shubha as Karthu
*Cochin Haneefa as Paappi
*Jalaja as Nandinikutty
*Kuthiravattam Pappu as Sankunni
*MG Soman as Govindankutty
*Mala Aravindan as Naanu Nair
*Silk Smitha as Parvathi
*TG Ravi as Vaasu
*Sukumari as Savithri
*Santo Krishnan as Gunda
*Benny as Unni
*Joseph E.A  as Venu

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by RK Damodaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Onnaanaam Kandathil || P. Madhuri || RK Damodaran || 
|-
| 2 || Varna Mayilvaahanathil || K. J. Yesudas, Chorus || RK Damodaran || 
|}

==References==
 

==External links==
*  

 
 
 

 