The Men Who Tread on the Tiger's Tail
 
{{Infobox film
| name           = The Men Who Tread On the Tigers Tail
| image          = Tora no o wo fumu otokotachi poster.jpg
| director       = Akira Kurosawa
| producer       = Motohiko Itō
| writer         = Akira Kurosawa
| starring       = Takashi Shimura Susumu Fujita Denjirō Ōkōchi Iwai Hanshirō X Toho Studios
| distributor    = Toho Company Ltd.
| released       =  
| runtime        = 59 minutes
| country        = Japan Japanese
| budget         =
| music          = Tadashi Hattori
| awards         =
}} period drama film, written and directed by Akira Kurosawa. It is based on the kabuki play Kanjinchō, which is in turn based on the Noh play Ataka (play)|Ataka.

The film was initially banned by the occupying Supreme Commander of the Allied Powers (SCAP) due to its portrayal of feudal values.  It was later released after the signing of the Treaty of San Francisco in 1952.

==Plot==
In 1185, the Heike family fights against the Minamoto family. After a bloody naval battle in the Pacific Ocean, Yoshitsune Minamoto defeats the enemy and the survivors commit suicide. When the triumphant Yoshitsune arrives in Kyoto, his brother, the Shogun Yoritomo, is uneasy and orders his men to arrest Yoshitsune. However, Yoshitsune escapes with six loyal samurai led by Benkei and they head to the country of his only friend Hidehira Fujiwara. Near the border, after crossing the forest disguised as monks, their porter discovers that they are Yoshitsune and the six samurais and advises that the fearful Kajiwara and his soldiers are waiting for them in the border to arrest them. Yoshitsune disguises as a porter and at the barrier at the border, Benkei has to convince Kajiwara that they are six monks traveling to collect donation to build a large temple in Kyoto.

==Cast==
 
 
* Denjirō Ōkōchi as Benkei
* Susumu Fujita as Togashi
* Kenichi Enomoto as porter Masayuki Mori as Kamei
* Takashi Shimura as Kataoka
* Akitake Kōno as Ise
* Yoshio Kosugi as Suruga
* Iwai Hanshirō X as Yoshitsune (Minamoto no Yoshitsune)
* Demio Yokoo as Hitachibō
* Yasuo Hisamatsu as Kajiwaras messenger
* Shōji Kiyokawa as Togashis messenger
 

==External links==
 
*  
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 
 
 
 
 
 

 