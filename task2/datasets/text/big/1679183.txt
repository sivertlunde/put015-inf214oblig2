The Cutting Edge
 
{{Infobox film
| name           = The Cutting Edge 
| image          = The_Cutting_Edge_Poster.jpg 
| caption        = Theatrical poster
| image size     = 
| writer         = Tony Gilroy 
| director       = Paul Michael Glaser  Karen Murphy 
| starring       = D. B. Sweeney Moira Kelly Roy Dotrice Terry OQuinn Patrick Williams  Elliot Davis 
| editing        = Michael E. Polakow 
| studio         = Interscope Communications
| distributor    = Metro-Goldwyn-Mayer 
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| gross          = $25,105,000 
}}
 figure skater Olympic figure Soviet pair in the climax of the film, which is set at the site of the 16th Winter Olympic Games, in Albertville, France. The film was also shot in Hamilton, Ontario, Canada.   

==Plot==
Kate Moseley (Moira Kelly) is a world-class figure skater training for the 1988 Winter Olympics. She has genuine talent, but years of being spoiled by her wealthy father Jack (Terry OQuinn) have made her all but impossible to work with.
 team is also in the Olympics, and just minutes before his match, he and Kate literally run into each other at the arena. During the game Doug suffers an eye injury which damages his peripheral vision, and he is forced to retire. Later in the Games, Kate falls during her program, costing her pair a chance at the gold medal.

In the next two years, while training for the Winter Olympics, Kate has driven away all potential skating partners with her attitude and perfectionism; her coach, Anton Pamchenko (Roy Dotrice), needs to find another replacement, an outsider. He proceeds to track down Doug, who is back home in Minnesota, working in a steel mill, living with his brother and playing in a semi-professional hockey league on the side. Desperate for another chance at Olympic glory, Doug agrees to work as Kates partner, even though he has a macho contempt for figure skating.

However, Kates snooty, prima donna behavior gets on his nerves immediately. Their first few practices do not go well. In time, though, their relationship grows warmer, and they learn to work together and become a pair to be reckoned with both on and off the ice.  They advance to the U.S. Nationals, and despite strong performances in the short program and long program, they are on track to come in third-place, which does not advance them to the Olympics. However, when one of the leading pairs falls during the competition, they advance.

At the finals at the Albertville Olympics, they look to be one of the top pairs competing for the gold. Everything is going well until they realize that they have fallen in love with each other. Doug and Kate are forced to reconcile these new feelings with their mutual desire to win at all costs.

==Cast==
* D. B. Sweeney as Doug Dorsey
* Moira Kelly as Kate Moseley
* Roy Dotrice as Anton Pamchenko
* Terry OQuinn as Jack Moseley
* Dwier Brown as Hale Forrest
* Chris Benson as Walter Dorsey
* Kevin Peeks as Brian Newman
* Rachelle Ottley as Lorie Peckarovski
* Barry Flatman as Rick Tuttle
* Christine Hough and Doug Ladret, 1988 Canadian figure skating pairs champions and two-time Olympians (1988 and 1992), as Soviet team Smilkov and Brushkin

==Music== Patrick Williams. The films theme song "Feels like Forever" was performed by Joe Cocker and written by Diane Warren and Bryan Adams.

===Soundtrack===
 Patrick Williams score (indicated below in bold).

# Street of Dreams - Nia Peeples Neverland
# Black Box Arrow
# It Aint Over til Its Over - Rosemary Butler & John Townsend
# Shame Shame Shame - Johnny Winter
# Turning Circles - Sally Dworsky
# Baby Now I - Dan Reed Network
# Ive Got Dreams to Remember - Delbert McClinton
# Feels Like Forever (Theme From The Cutting Edge) - Joe Cocker
# Ich Namen Gita/Olympic Hockey
# Battle of the CDs
# Limo to Mansion/Nine Months Later
# Kate Skates Alone
# Chicago Practices
# Hoedown
# Tequila
# Olympic Fanfare/Dubois & Gercel
# Doug & Kate Get Angry
# The Russians Skate
# Finale
# End Credits

The following songs are heard in the movie but not included on the soundtrack album:
*  Lauretta - Malcolm McLaren
* Love Shack - Rosemary Butler
* Auld Lang Syne - Rosemary Butler & Warren Wiebe
* Walking the Dog - John Townsend The Race - Yello

==Reception==
The Cutting Edge was released on March 27, 1992, and grossed $25,105,517 domestically. 

 
The film has a 60% rating (based on 15 reviews) on Rotten Tomatoes.  . rottentomatoes.com. Retrieved 2010-10-27.  Two decades later, it continues to be broadcast on local television stations across the U.S., particularly around the holiday season, indicating a lasting popularity with viewers.

==Sequels==
As another indication of its lasting popularity, the film was followed by several sequels:   (2006),   (2008) and   (2010), each with mostly different casts.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 