The Horizontal Lieutenant
{{Infobox film
| name           = The Horizontal Lieutenant
| image          = The Horizontal Lieutenant.jpg
| image size     = 225px
| caption        = theatrical release poster
| director       = Richard Thorpe
| producer       = Joe Pasternak
| based on       = novel The Bottletop Affair by Gordon Cotler George Wells
| starring       = Jim Hutton Paula Prentiss
| music          = George Stoll
| cinematography = Robert J. Bronner
| editing        = Richard W. Farrell studio = Euterpe
| distributor         = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $1,020,000 
| gross          = $1,850,000 
}}
 American romantic romantic comedy war film directed by Richard Thorpe, based on the 1961 novel The Bottletop Affair by Gordon Cotler. It is a military comedy about an unfortunate army intelligence lieutenant who finds himself isolated on a remote Japanese island army outpost during World War II.   It stars Jim Hutton and Paula Prentiss and was directed by Richard Thorpe.
 Bachelor in Paradise.  Every Hollywood Press Agent Dreams of a Story Like Hers. This Time Ifs True
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   22 Oct 1961: b18.  

==Plot==
Merle Wye, a second lieutenant in the U.S. Army stationed in Hawaii, is attracted to nurse Molly Blue, but she barely gives him the time of day.

The inept Merle is transferred to a remote smaller island, where he is assigned the task of finding a man called Kobayashi who is suspected of  pilfering Army supplies. One thing after another goes wrong for Merle, who is knocked cold when Kobayashi finally turns up, but given a medal of valor anyway after Molly is able to capture the prisoner on his behalf.

==Cast==
*Jim Hutton as Merle Wye
*Paula Prentiss as Molly Blue Jack Carter as Billy Monk
*Jim Backus as Jeremiah Hammerslag
*Charles McGraw as Col. Charles Korotny
*Miyoshi Umeki as Akiko
*Marty Ingels as Buckles
*Lloyd Kino as Sgt. Jess Yomura
*Linda Wong as Michido
*Yoshio Yoda as Sgt. Roy Tada
*Yuki Shimoda as Kobayashi
==Production==
The novel was published in 1959. Books of The Times
By ORVILLE PRESCOTT. New York Times (1923-Current file)   31 July 1959: 21.  

Hutton and Prentiss were under contract to MGM at the time. Young Jim Hutton Owner of Long Term Film Contract
Tinee, Mae. Chicago Daily Tribune (1923-1963)   12 Nov 1961: f11.  
==Reception==
According to MGM records, the film earned $1.1 million in the US and Canada and $750,000 overseas, resulting in a loss of $380,000.  . 

==References==
Notes
 

==External links==
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 

 
 