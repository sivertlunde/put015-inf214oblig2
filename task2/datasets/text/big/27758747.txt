A Circle of Deception
{{Infobox film
| name           = Circle of Deception
| image          = ACircleOfDeception1960Poster.jpg
| caption        = US poster Jack Lee
| producer       = Tom Morahan
| writer         = 
| screenplay     = Nigel Balchin Robert Musil
| story          = 
| based on       =   
| starring       = Bradford Dillman Suzy Parker Harry Andrews
| music          = Clifton Parker
| cinematography = Gordon Dines
| editing        = Gordon Pilkington
| studio         = 
| distributor    = Twentieth Century Fox
| released       =  
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 Jack Lee and starring Bradford Dillman, Suzy Parker and Harry Andrews. 

==Plot==
A Canadian officer (Bradford Dillman) is sent on a secret and dangerous mission during the Second World War with false information in an attempt to misslead the Germans about the Normandy Landings of 1944.

==Cast==
* Bradford Dillman as Captain Paul Raine 
* Suzy Parker as Lucy Bowen 
* Harry Andrews as Captain Thomas Rawson 
* Robert Stephens as Captain Stein  Paul Rogers as Major William Spence  John Welsh as Major Taylor 
* Ronald Allen as Jim Abelson 
* A.J. Brown as Frank Bowen 
* Martin Boddey as Henry Crow 
* Charles Lloyd-Pack as Ayres 
* Jacques Cey as Cure 
* John Dearth as Captain Ormrod 
* Norman Coburn as Carter 
* Hennie Scott as Small boy 
* Richard Marner as German colonel

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 