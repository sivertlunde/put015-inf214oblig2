100 Degree Celsius
{{Infobox film
| name = 100 Degree Celsius
| image = 
| caption = 
| alt =
| director = Rakesh Gopan
| producer = Royson Vellara
| writer = Rakesh Gopan Ananya Haritha Parokod
| music = Gopi Sundar
| cinematography = Sathyanarayanan Suriyan
| editing = Don Max
| studio = RR Entertainments
| distributor = 
| released =  
| runtime = 
| country = India
| language = Malayalam
| budget = 
| gross = 
}} Ananya and Haritha Parokod.  Based on a real-life incident, 100 Degree Celsius revolves around the lives of five women - a housewife, banker, IT professional, TV reporter and a college student.    The film started shooting in June 2013.  100 Degrees Celsius will be Malayalam cinemas first two-part film.    It is produced under the banner of R.R. Entertainments. Cinematographer is Satyanarayanan and music director is Gopi Sundar.   

==Cast==
* Shwetha Menon as Nila
* Bhama as Nancy
* Meghana Raj as Revathy Ananya as Ganga
* Haritha Parokod as Lovely
* Kaviyoor Ponnamma as Lovelys mother in law Sethu 
* Anil Murali as Balu
* Mithun Ramesh as Lovelys husband
* Sreejith Ravi as Sura
* Ganesh Kumar as Police Officer
* Shivaji Guruvayoor
* Sanju Sivaram as Sreekuttan
* Kalabhavan Haneef
* Anjali Upasana as Nanda
* Sreelatha Namboothiri as Nancys mother in law
* T Parvathy as Adv. Rani Varma
* Deepika as Doctor

==Production==
Debutante director Rakesh Gopan, who had served as associate to VKP and Rajasenan said, "Its based on a true story that happened in Kochi. The main characters are an IT professional, a TV channel reporter, a bank employee, a college student and a housewife", adding that the team will also bring out the real people who inspired the story, during the promotion of the film. "All these characters live in a flat and the story revolves around an incident that happens there. They try to cover this up and ultimately, it leads to their lives turning upside down. A thriller, the film deals with various issues that women in todays society have to go through - for instance, blackmailing" Rakesh said.  Rakesh Gopan after months of scripting, found it was longer for a single movie and decided to split it into two parts. 
 Ananya replaced Gauthami,  Aparna was replaced by Kollywood actress Haritha Parokod, who made her debut in Malayalam.  Shwetha Menon plays a techie, Bhama, a bank employee, Meghna Raj, a news channel reporter, Ananya, a student, and Haritha a home-maker.  Earlier, it was reported that Lakshmi Rai was also a part of the project. However, the director refuted the rumours and said, "Lakshmi Rai was never part of it."  Krish J Sathar was roped in as the male lead.  Later the makers also cast a relative newcomer, Anu Mohan.  With the film split into two, the makers also decided to make changes in the cast and cast Tamil and Telugu actor Shaam and Bollywood actress Radhika Apte, who both will make their Mollywood debut. 

==Critical reception==
The Times of India gave the film 3 stars out of 5 and wrote, "If what you want is to sit back and relax with a packet of popcorn, 100 Degree Celsius is not the film for you, as you would mostly be on your seats edge, scene after scene. Rakesh Gopan joins the pack the debutant directors in Malayalam, who tell unprecedented stories in our tinsel town. His effort is laudable, and besides a handful of glitches in execution that sets occasional boredom, the film is involving enough to keep one interested till the end".  Indiaglitz.com gave a rating of 6.5 out of 10 and wrote, "With all other factors appealing, this 100 degree Celsius can be prescribed for  an onetime watch, especially for the lovers of adrenaline raising thriller films". 

Sify was more critical of the film and wrote, "With an absurd script, this film struggles to keep you engaged right from the start. The story just goes here and there without any logic. All you are surprised about is the serious way all those inane scenes have been presented here", going on to call it a "royal mess, nothing less". 

Upon release, Dr. Manoj Narayanan Namboothiri, a psychologist from Trivandrum, filed a case against the director and the Censor Board, citing that "the film shows women in a poor light and can give out wrong messages to society". 

==References==
 

== External links ==
*  

 
 
 
 