Pkah Thgall Meas
{{Infobox Film
| name           = Pkah Thgall Meas
| image          = Photo-78159-M.png
| caption        = The Cambodian film poster.
| director       = Sok Min Chi
| producer       = 
| writer         = Sok Min Chi
| narrator       = 
| starring       = Vichara Dany Vann Vannak Sok Samer Nom Nap
| music          = 
| cinematography = 
| editing        = 	 	
| distributor    = Ah Mata Production
| released       =   1975
| runtime        = 
| country        = Cambodia
| language       = Khmer
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1975 Cinema Cambodian drama film. It was a well-known film in Cambodia before the Khmer Rouge.

== Plot ==
Twin brothers are born on the same day that a murder occurs. The twins separate from each other; one is adopted into a rich family while the other remains on the poor ground. Several years past when the poor twin begins to fall in love with a rich mans daughter named Pkah Tgall Meas. The couple faces many adversities from a cruel wealthy man. It is up to the rich twin to save the life of his poor twin brother and his love Pkah Thgall Meas.

== External links ==

 
 
 
 


 