A Long Haul
 
{{Infobox film
| name           = A Long Haul
| image          = A Long Haul.jpg
| caption        = 
| director       = Nathaniel Kramer
| producer       = Nathaniel Kramer
| starring       =  
| cinematography = Nathaniel Kramer
| editing        = Nathaniel Kramer
| released       =  
| runtime        = 44 minutes
| country        = United States
| language       = English
}}
A Long Haul is a 2010 documentary film about Montauk, NY charter boat captain Bart Ritchie and his struggles with the effects of a declining economy and governmental regulations on the fishing industry. The film was produced and directed by Nathaniel Kramer, who, in addition to being a filmmaker, is a photographer and recreational fisherman. The film has been well received, screening as an official selection in numerous festivals including: the Astoria/LIC International Film Festival,  DocMiami International Film Festival,  Connecticut Film Festival,  EdinDocs,  Lighthouse Film Festival,  New Filmmakers New York,  Philadelphia Independent Film Festival,  The IndieFest,  Accolade Festival,  and won Best Documentary Short Film, Best Director, and Best Cinematography for a Short Documentary at the Los Angeles Movie Awards. 

==Cast== Captain
*Kurtis Briand: First Mate
*Leighton Brillo-Sonnino

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 


 