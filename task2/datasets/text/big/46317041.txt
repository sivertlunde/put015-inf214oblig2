The Tidal Wave
{{Infobox film
| name           = The Tidal Wave
| image          =
| caption        =
| director       = Sinclair Hill
| producer       = 
| writer         = Ethel M. Dell (novel)   Sinclair Hill
| starring       = Poppy Wyndham   Sydney Seaward   Annie Esmond  John Mackenzie
| editing        = 
| studio         = Stoll Pictures 
| distributor    = Stoll Pictures
| released       = August 1920
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama film directed by Sinclair Hill and starring Poppy Wyndham, Sydney Seaward and Pardoe Woodman.  It is based on a short story by Ethel M. Dell. A fisherman rescues an artist from the sea, and falls in love with her.

==Cast==
*   Poppy Wyndham as Columbine  
* Sydney Seaward as Matt Brewster  
* Pardoe Woodman as Frank Knight 
* Annie Esmond as Aunt Liza 
* Judd Green as Adam Brewster

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 

 