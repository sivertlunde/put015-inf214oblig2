The Glorious Adventure (1922 film)
 
 
 
{{Infobox film
| title          = The Glorious Adventure
| image          = 
| caption        = 
| director       = J. Stuart Blackton
| producer       = J. Stuart Blackton
| writer         = J. Stuart Blackton Felix Orman Nicholas Musuraca (titles)
| narrator       = 
| starring       = Diana Manners Gerald Lawrence Cecil Humphryes Victor McLaglen
| music          = 
| cinematography = William T. Crespinel
| editing        = 
| studio         = J. Stuart Blackton Productions
| distributor    = Stoll Film Studios (UK) United Artists (US)
| released       = 23 April 1922 (UK) 27 August 1922 (US)
| runtime        = 70 min.
| country        = UK/US
| language       = Silent film English intertitles
| budget         = 
| gross          = 
}}
The Glorious Adventure (1922) is British colour feature film directed by J. Stuart Blackton, written by Felix Orman. The films sets were designed by Walter Murton.   

==Cast==
* Diana Manners - Lady Beatrice Fair 
* Gerald Lawrence - Hugh Argyle 
* Cecil Humphreys - Walter Roderick 
* Victor McLaglen - Bulfinch 
* Alice Crawford - Stephanie Dangerfield 
* Lois Sturt - Nell Gwyn 
* William Luff - King Charles II  Fred E. Wright - Humpty
* Elizabeth Beerbohm - Barbara Castlemaine 
* Flora le Breton - Rosemary 
* Lennox Pawle - Samuel Pepys 
* Haidee Wright - Mrs. Bullfinch 
* Rudolph De Cordova - Thomas Unwin 
* Lawford Davidson - Lord Fitzroy 
* Rosalie Heath - Catherine of Braganza 
* Gertrude Sterroll - Duchess Constance of Moreland 
* Tom Heslewood - Solomon Eagle 
* Maximilien Weiss - Lord Lamontagne
* Marjorie Day - Olivia 
* Geoffrey Clinton - Charles Hart 
* Tom Coventry - Leclerc 
* Jeff Barlow - The Kings chief valet 
* John Marlborough East - The Kings major domo 
* Violet Virginia Blackton - Lady Beatrice Craig Gordon - Courtier

==Production background==
The film was made entirely in Prizmacolor, and starred Lady Diana Manners, Gerald Lawrence, Cecil Humphreys, and Victor McLaglen, and was released by United Artists. Alma Reville, later married to Alfred Hitchcock, may have co-written the script as well as acting as "script girl".

Neither this film, or the 1918 film of the same name produced by Samuel Goldwyn, is related to the famous book The Glorious Adventure (1927) by Richard Halliburton.

==Preservation status==
According to the IMDB and SilentEra websites, a print of the film exists.

==See also==
*List of early color feature films

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 


 
 