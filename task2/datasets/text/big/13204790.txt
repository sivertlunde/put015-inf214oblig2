Under the Bombs
{{Infobox film
| image          =underthebombs.jpg
| caption        =Poster used in the Venice days
| name           =Under the bombs - تحت القصف
| writer         =Philippe Aractingi    Michel Leviant
| starring       =Nada Abou Farhat   Georges Khabbaz
| director       = Philippe Aractingi
| released       =  
| runtime        =98 minutes
| country        =Lebanon
| music          = René Aubry    Lazare Boghossian
| budget         = 
| gross          = $488,227 
| cinematography = Nidal Abdel Khalek
| language       = Arabic French
| producer       = Philippe Aractingi   Hervé Chabalier   François Cohen-Séat   Henri Magalon   Paul Raphaël
| distributor    = Lions Gate Films {US}
}} Lebanese drama film directed by Philippe Aractingi. The film is set in Lebanon right at the end of the 2006 Lebanon War.

==Plot== Lebanese Muslim Lebanese Christian taxi driver Tony (Georges Khabbaz) to drive her to Southern Lebanon. In their search for Maha and Karim, they encounter the devastation wrought by the war and learn each others personal secrets, including the fact that Tonys brother was a member of the South Lebanon Army and is now living in exile in Israel.

==Cast ==
*Nada Abou Farhat as Zeina
*Georges Khabbaz as Tony

==Release== 2008 Sundance Film Festival on the 2006 Lebanon War. The other was Guy Nattiv and Erez Tadmors Strangers (2007 Israeli film)|Strangers. 

The film was presented in 2008 as part of the Giornate degli Autori - Venice days, a parallel section of the Venice film festival.   

==Critical response==
Jonathan Curiel of the San Francisco Chronicle gave the film the Chronicle s highest rating, stating that "Aractingi, a Lebanese director, has - with the help of superb performances by Khabbaz and Abou Farhat - made a work that deftly navigates complicated truths." 

==Awards==
*2008: Venice Days – Giornate degli Autori—Arca Cinema Giovani Award: Best Film “Other Visions” and the Eiuc Human Rights Film Award
*Jury Junior Prize at Festival International du Film Francophone de Namur
*Critics Prize and NETPAC Prize NETPAC at International Antalya Golden Orange Film Festival
*Gold Muhr and the Best Actress Award (Nada Abou Farhat) at the Dubai Film Festival
*Prix Coup de Coeur, Best Music Award and Audience Award at Luchon International Film and TV Festival. 

==References==
 

==External links==
*  
* 
*  

 
 
 
 
 
 
 