Let's Go to Prison
{{Infobox film
| name           = Lets Go to Prison
| image          = Lets Go to Prison.jpg
| caption        =
| director       = Bob Odenkirk
| producer       = Marc Abraham Matt Berenson Paul Young Thomas Lennon Michael Patrick Jann
| starring       = Dax Shepard Will Arnett Chi McBride David Koechner
| music          = Alan Elliott
| cinematography = Ramsey Nickell
| editing        = Eric L. Beason Denis Thorlaksen
| studio         = Carsey-Werner|Carsey-Werner Films
| distributor    = Universal Studios
| released       =  
| country        = United States
| runtime        = English
| budget         = $4,000,000 
| gross          = $4,630,045
}}

Lets Go to Prison is an American comedy that was released in theatres November 17, 2006, starring Dax Shepard, Will Arnett and Chi McBride, and directed by Bob Odenkirk. The movie was loosely based on the non-fiction book, You Are Going to Prison by Jim Hogshire.

== Plot ==
John Lyshitski (Dax Shepard) has spent most of his life in prison, serving three different sentences (starting when he was eight, when he stole the Publishers Clearing House van). Each of his three trials were before Judge Nelson Biederman III, who seemed to show no mercy passing down indiscriminately harsh sentences. After being released from his third sentence, John decides to take revenge on Biederman. After trying to determine when Biederman would be presiding over his next case, he discovers (much to his dissatisfaction) that he had died three days prior to his release.

He turns his attention to the judges brash son, Nelson Biederman IV (Will Arnett). At a dedication ceremony for Nelson III, John sneaks into Nelsons BMW, where he steals his change, spits in his coffee, and exhausts his emergency inhaler. After the ceremony, Nelson IV yells at his attorney (who has the speakerphone on in the fully occupied board room) clearly upset about having to go through such an ordeal, demanding that they leave him be and have a Fresca ready for him upon his arrival. He drives down the road and, finding his inhaler empty, immediately panics and hyperventilates. He stops at a pharmacy and scrambles through the shelves, desperately seeking some kind of replacement for the inhaler he lost. The pharmacy owners think hes another Substance dependence|junkie, seeking some kind of fix. After finding and using an inhaler, he hides behind a counter and holds up the inhaler. The storeowner, however, mistakes the inhaler for a tiny pistol which leads the owners to call the police.

Nelson ends up arrested and charged with assault and armed robbery. After turning down the idea of stepping down and pleading guilty, he demands that the Biederman foundation do whatever they have to do to have him acquitted and released. The board contemplates for a moment, considering asking the governor to get Nelson out. However, as they are sick and tired of his stubborn attitude, they soon realize that this is their opportunity to get rid of him, and conspire to get him a poor defense in trial. With this severely inadequate legal representation, Nelson is found guilty and sentenced to three to five years at Rossmore State Penitentiary. John is not satisfied with Nelson just going to prison, however, so John decides to join him in prison by intentionally selling marijuana to undercover police officers. At his sentencing he pleads guilty to the same judge Nelson IV had, and asks for the same sentence (3–5 years) at the same prison. After negotiating with the judge and bribing a guard, he ends up as Nelson’s cellmate, and here he pretends to be his friend, all in an attempt to give him the wrong advice on surviving life in prison.

While he is in jail, John consistently gives him bad advice and informs him about the people within the prison, including Lynard, the extremely violent leader of The White Kingdom, a white supremacist gang in the prison.
 gay fellow romantic partners passion parlor. Nelson first submits to being his partner out of fear, and although he clearly has no kind of romantic interest in him at first, he becomes quite fond of Barry and just goes along with it.

Nelson reaches his one-year parole hearing relatively unscathed, and actually the "top dog" in the prison. However, John will not allow his target to escape prison so easily: he drugs Nelson and writes "WHITE POWER" on his forehead and a swastika on each side of his neck. This leads to the parole board deciding that Nelson "needs" more time to be rehabilitated. Infuriated, Nelson brings John up on the act; John then confesses to putting Nelson in jail and the two get embroiled in a fight. It is here that John regrets bunking with Nelson when he realizes Nelson has nothing to lose but his will to murder, and John is the target. After an ensuing brawl, the guards set up a death match between the two.

However, John and Nelson secretly conspire and inject each other with a coma-inducing drug. The guards and prisoners believe that they are dead and bury the pair outside in the graveyard. Nelson uses Barrys love for him to his own advantage as he gives his recently released "lover" access to his funds to bribe off the mortician to avoid an autopsy. Barry later digs the two up, freeing them. The three men start a new life and set up a winery called "Baby Duck Winery" (composed of "toilet wine"), where a critic is about to fail their wine. John shows up and forces the critic to give them a good review, and drink the wine. The film ends with Nelson, Barry, and John, now the best of friends, taking a drive along the countryside, listening to Move This by Technotronic.

==Cast==
*Dax Shepard as John
*Will Arnett as Nelson
*Chi McBride as Barry
*David Koechner as Shanahan
*Dylan Baker as Warden
*Michael Shannon as Lynard
*David Darlow as Judge Biederman
*Bob Odenkirk as Duane
*A. J. Balance as John - 18 years
*Tim Heidecker as wine tester

==Reception==
The movie received mostly negative reviews, holding a 12% "rotten" rating at Rotten Tomatoes. 

Metacritic gives it a score of 27 out of 100 ("Generally unfavorable"). 

Box Office Mojo reports that the film opened in 11th place with a meagre take of $2,220,050. It closed with a domestic gross of $4,630,045. 

==Production notes== The Blues Fox show Prison Break (2005).

During the end credits, Barry sings a song called "Shower With U" (credited as "Barrys Love Theme" on the soundtrack) in which he repeatedly sings "I wanna take a shower with you".

The studio made significant alterations during the films editing process that made Odenkirk unhappy with the final result (which also happened with the Mr. Show with Bob and David film, Run Ronnie Run, which Odenkirk wrote). According to writers Tom Lennon and Robert Ben Garants appearance on the Nerdist Podcast from August 23, 2011, changes included a happier ending, the removal of a sparse drums-only score recorded by Meg White of The White Stripes, and other alterations that made a significant change to the overall tone of the film.  "Worst Reviews" Staff (2006).   WorstPreviews.com.  Retrieved October 19, 2014.  

== DVD ==
The movie was released on DVD March 6, 2007 with deleted scenes and an alternate ending. 

The Unrated version features pre-credits and post-credits scene features a real-life officer  giving the details about the film.

==References==
 
 

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 