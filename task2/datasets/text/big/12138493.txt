The Doe Boy
 
{{Infobox Film
| name           = The Doe Boy
| image_size     = 
| caption        = 
| director       = Randy Redroad
| producer       = Anthony J. Vozza Chris Eyre (executive producer) Jennifer Easton
| writer         = Randy Redroad Kevin Anderson Jeri Arredondo Gordon Tootoosis Judy Herrera
| music          = Adam Dorn aka Mocean Worker
| cinematography = László Kadar Matthew Booth
| studio         = Easton LTD. Partnership Curb Entertainment Wellspring (United States|U.S.)
| released       = 2001
| runtime        = 87 min.
| country        = United States English
}}
 independent drama film written and directed by Randy Redroad.  It was selected as the United States winner of the Sundance Film Festival/NHK International Filmmakers Award in 2000.  The Doe Boy was produced by filmmaker, Chris Eyre.

==Plot==
Set in 1984 in the heart of the Cherokee Nation of Oklahoma, The Doe Boy tells the coming of age story of Hunter (James Duval), a young man of mixed heritage who is also a haemophiliac.

==Cast==
* James Duval...Hunter Kevin Anderson...Hank Kirk
* Jeri Arredondo...Maggie Kirk
* Andrew J. Ferchland...Young Hunter
* Gordon Tootoosis...Marvin Fishinghawk
* Judy Herrera...Geri 
* Jim Metzler...Dr. Moore
* Nathaniel Arcand...Junior
* Robert A. Guthrie...Cheekie
* Gil Birmingham...Manny 
* Alex Rice...Bird
* Orvel Baldridge...Oliver
* Kyle White...Young Junior

==Awards and nominations==

* Sundance/NHK International Filmmakers Award 
* Taos Talking Pictures - Best First Time Director
* Wine Country Film Festival - Best First Feature, Best Actor (James Duval)
* Great Plains Film Festival - Best Feature
* Deauville American Film Festival 2001 - Official Competition
* IFP/Gotham Open Palm Award - Outstanding Directorial Debut - Finalist
* Perrier Bubbling Under Award - Finalist
* Galway Film Fleadh - Best First Time Director Co-Winner 
* First Nations Film Festival, Montreal - Grand Prize
* Great Plains Film Festival - Best Feature
* Empire State Film Festival  - Grand Prize
* Route 66 Film Festival, Chicago Best Feature With Diversity Emphasis
* American Indian Film Festival - Best Film, Best Director, Best Actor (James Duval), Best Actress (Jeri Arredondo) Best Supporting Actress (Judy Herrera)

==External links==
*  
*   at Rotten Tomatoes
*  

 
 
 
 
 
 
 
 
 