Waramutsého!
 

{{Infobox film
| name           = Waramutsého!
| image          = 
| caption        = 
| director       = Bernard Auguste Kouemo Yanghu
| producer       = Courte Échelle Prod
| writer         = 
| starring       = Steve Achiepo, Clément Ntahobari
| distributor    = 
| released       = 2008
| runtime        = 21
| country        =Cameroon
| language       = 
| budget         = 
| gross          = 
| screenplay     = Bernard Auguste Kouemo Yanghu
| cinematography = Bertrand Artaud
| editing        = Jean-Michel Cazenave, Geoffroy Cernaix
| music          = Philippe Bonnaire
}}

Waramutsého! is a 2008 film.

== Synopsis ==
Kabera and Uwamungu are two men from Rwanda who live together in the suburbs of Toulouse. Chaos erupts in their country and Kabera finds out that members of his family have participated in the massacre of the family of his good friend, Uwamungu.

== Awards ==
* Festival Cinema Africano Asia America Latina de Milán 2009
* FESPACO 2009

== References ==
 

 
 
 


 