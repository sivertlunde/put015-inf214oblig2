Faleze de nisip
Sand Cliffs ( ) is a Romanian motion picture drama released in 1983  and banned four days after its première by the regime of Nicolae Ceaușescu.  The film, adapted from the novel Zile de Nisip ("Days of sand") by Bujor Nedelcovici, was directed by Dan Pița, with dialogue and script by Bujor Nedelcovici and Dan Pița, cinematography by Vlad Păunescu and a sound track composed by Adrian Enescu. Cast included Victor Rebengiuc, Marin Moraru, Gheorghe Visu and Carmen Galin.  Faleze de Nisip  was screened in 1991 at the Berlin Film Festival, in 1994 at Los Angeles, and was one of the three Romanian films to be shown in 2007 at the European Film Festival in Lima, Peru. 

==Plot==
The action takes place during the summer holiday at the Black Sea. The plot confronts an influential surgeon and a carpenter accused to have stolen the doctors personal belongings from the beach. The neurotic doctor involves himself in the inquiry, ultimately directing the interrogation. The burglary victim, Theodor Hristea, a surgeon at the peak of his career and social life, self-confident and well connected (played by Victor Rebengiuc, a major Romanian actor), his girlfriend, Cristina (played by Carmen Galin) and their common friend, Stefan (played by Marin Moraru) - find themselves on holiday near the summer resort of Mamaia. The doctor has a detective passion and accuses – just on account of a physical resemblance with the real thief - a young carpenter ("The Kid", played by Gheorghe Visu) who happens to come at the same beach the day following the burglary. The police are summoned, a long inquiry follows, declarations are taken, and The Kid is sent to trial. The doctor plays an active part in the investigation. Although the doctors girlfriend and his friend doubt the identity of the accused, the surgeon insists in being right and succeeds in sending the innocent young man to prison.  

The accused, totally irrelevant on the social scale, is fired due to his unexplained absences from work during the long dragging period of the investigation that failed to produce any proof, as well as his several months in prison. And still, up to a certain point, the young carpenter proves to be, psychologically, the strongest. He gets back to life, gets married, continues to work, although more introverted than before, circumscribed to his sad destiny. Obsessed by his need to be always right, the doctor goes to meet the young man again and make him confess the theft that he in fact has never committed. Infuriated, the young man starts shouting to be left alone, and more as a self-defence stabs the neurotic doctor in the stomach with the tool he happened to be working with at that moment.   The film ends with a scene showing both men running on the beach: The Kid to escape from being caught, and the doctor, badly wounded, to persuade the young man to accept a theft he has never committed.

==Political reactions==
In a Plenary Session of the Romanian Communist Party in 1983 at Mangalia, upon advice from people in his entourage responsible with the ideology of culture, and possibly even from people in the artistic field, Ceaușescu exploded into a rage against Faleze de Nisip. He stressed that the film was breaking with the socialist ideology. He accused the film for not faithfully portraying "the new communist man". He mentioned that this sort of films was the wrong way to use art in favour of socialism. Faleze de Nisip, already banned from the market, was erased from the filmography of its own director, Dan Pița. Only at the end of 1990 – 9 years after its release — was the film shown again in public.

==References==
 

== External links ==
* Doinel Tronaru,  , at LiterNet (May 29, 2008, originally published in România Liberă, January 2004, retrieved June 30, 2009.  
* Dani Năneștean, , Clujeanul on-line, May 19, 2008, retrieved on June 30, 2009.   
* Marian Rădulescu,  , September 2007, retrieved June 30, 2009.  
* Stefan Oprea,   in Convorbiri Literare - on-line, retrieved June 30, 2009.  

 

 
 
 
 
 
 
 
 