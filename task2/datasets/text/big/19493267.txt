Tag til Rønneby Kro
 
{{Infobox film
| name           = Tag til Rønneby Kro
| image          =
| caption        =
| director       = Jon Iversen Alice OFredericks
| producer       = Henning Karmark
| writer         = Børge Müller
| starring       = Johannes Meyer
| music          =
| cinematography = Rudolf Frederiksen Alf Schnéevoigt
| editing        = Marie Ejlersen
| distributor    =
| released       =  
| runtime        = 99 minutes
| country        = Denmark
| language       = Danish
| budget         =
}}

Tag til Rønneby Kro is a 1941 Danish family film directed by Jon Iversen and Alice OFredericks.

==Cast==
* Johannes Meyer - Kroejer Bartholdi
* Bodil Kjer - Anne Lise
* Ib Schønberg - Tjener Sørensen
* Poul Reichhardt - Journalist Daniel Jensen
* Petrine Sonne - Daniels tante Henry Nielsen - Landbetjent Mortensen
* Sigrid Horne-Rasmussen - Journalisten Molly
* Sigurd Langberg - Bankdirektøren Julius
* Svend Bille - Ministeren
* Knud Heglund - Professoren Lau Lauritzen - Sagføreren
* Valdemar Møller - Smukke Peter
* Jeanne Darville

==External links==
* 

 

 
 
 
 
 
 
 
 


 