Lily van Java
 
{{Infobox film
| name      = Lily van Java
| image     = Lily van Java scene.jpg
| image size   = 
| border     = 
| alt      = 
| caption    = A scene showing the films main family Nelson Wong
| producer    =
| writer     =  production
| music     = 
| cinematography = Wong brothers
| editing    = 
| studio     = {{plain list|
*South Sea Film
*Halimoen Film
}}
| distributor  = 
| released    =  
| runtime    = 
| country    = Dutch East Indies Silent
| budget     = 
| gross     = 
}} Nelson Wong. ethnic Chinese-produced films in the country.  It is likely a lost film.

==Premise==
The young daughter of a rich man, already in a loving relationship, is forced to marry someone she does not love. 

==Production== Ethnic Chinese businessmen, capitalising on the success of films produced in Shanghai, China, established two production houses, one in Batavia (modern day Jakarta) and the other in Surabaya.  South Sea Film, the production house in Batavia established by Liem Goan Lian and Tjan Tjoen Lian, was advertised as the first Chinese filmmaking cooperative in the country. Its first script was for Lily van Java;   which the company had to pass through the   (national censorship bureau) for fear of violating traditional values. 

According to JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue), an American named Len Ross was initially booked to direct the film;  Ross was reportedly in the country to film a work entitled Java for Metro-Goldwyn-Mayer and shot several scenes in mid-1928.  The cast was ethnic Chinese, and the female stars&nbsp;– Lie Lian Hwa and Lie Bo Tan&nbsp;– were reportedly daughters of the Surabayan gangster Lie Bauw Kie and trained in silat;  other cast members included Kwee Tiang An and Yah Kwee Pang. However, after Ross withdrew the script was put on hold. 
 Nelson Wong, who had formerly been booked with his brothers Joshua and Othniel to record a film for Tio Tek Djien,  approached David Wong (no relation), a high-ranking General Motors employee in Batavia, for funding. With this the Wong Brothers Halimoen Film was able to finish the film.    Some sources indicate that the same cast was used,   while others suggest that the leading role was taken by a student from Shanghai named Lily Oey.   
 Malay and Richard Abel writes that the films technical quality, like all other local productions of the time, was poor and unable to compete with imported works,  and the Indonesian film scholar Misbach Yusa Biran writes that the shots were considered blurry even at the time. 

==Release and reception==
Lily van Java was released in 1928.  Its success is disputed. The reporter Leopold Gan wrote that the film was highly successful, to the point that after several years copies were worn through from overplaying.  However, Joshua Wong later recalled in an interview that the film had been a failure; David Wong is reported to have avowed to no longer fund any films after Lily van Java.  Lacking a backer, the Wong Brothers went on hiatus. 
 Njai Dasima (1929).  By the early 1930s Chinese-owned businesses were the dominating force in the countrys film industry. 

The film is likely a lost film. The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==References==
Footnotes
 

Bibliography
 
*{{cite book
 |url=http://books.google.ca/books?id=9cc71Uekc_EC
 |title=Encyclopedia of Early Cinema
 |isbn=978-0-415-23440-5
 |last1=Abel
 |first1=Richard
 |year=2005
 |location=New York
 |ref=harv
 |publisher=Routledge
}}
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |location=Jakarta
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
*{{cite journal
 |last1=Biran
 |first1=Misbach Yusa
 |author1-link=Misbach Yusa Biran
 |last2=Ramadhan K.H.
 |first2=Misbach
 |last3=Labrousse
 |first3=P. (translator)
 |title=Les Vedettes du Cinéma Indonésien
 |trans_title=The Stars of Indonesian Cinema
 |language=French
 |url=http://www.persee.fr/web/revues/home/prescript/article/arch_0044-8613_1973_num_5_1_1048
 |journal=Archipel
 |pages=165–174
 |volume=5
 |issue=5
 |year=1973
 |ref=harv
 |doi=10.3406/arch.1973.1048
}}
*{{cite book
 |url=http://books.google.ca/books?id=k3HTdu1HuWQC
 |title=Malaysian Cinema, Asian Film: Border Crossings and National Cultures
 |isbn=978-90-5356-580-3
 |author1=van der Heide
 |first1=William
 |year=2002
 |ref=harv
 |location=Amsterdam
 |publisher=Amsterdam University Press
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
*{{cite web
 |title=Lily van Java
 |language=Indonesian
 |url=http://www.jakarta.go.id/jakv1/encyclopedia/detail/1659
 |work=Encyclopedia of Jakarta
 |publisher=Jakarta City Government
 |location=Jakarta
 |accessdate=21 August 2012
 |archiveurl=http://www.webcitation.org/6A4fwxwEY
 |archivedate=21 August 2012
 |ref= 
}}
*{{cite web
 |title=Lily van Java
 |language=Indonesian
 |url=http://filmindonesia.or.id/movie/title/lf-l011-28-301201_lily-van-java#.UAon3KDE_Mw
 |work=filmindonesia.or.id
 |publisher=Konfidan Foundation
 |location=Jakarta
 |accessdate=21 July 2012
 |archiveurl=http://www.webcitation.org/69JVEjVU9
 |archivedate=21 July 2012
 |ref= 
}}
*{{cite web
 |last=PaEni
 |first=Mukhlis
 |title=Mozaik Film Nasional
 |trans_title=Mosaic of National Films
 |language=Indonesian
 |url=http://www.lsf.go.id/film.php?module=artikel&sub=detail&id=51
 |publisher=Indonesian Film Censorship Bureau
 |location=Jakarta
 |date=7 June 2010
 |accessdate=21 August 2012
 |archiveurl=http://www.webcitation.org/6A4gS6UDZ
 |archivedate=21 August 2012
 |ref= 
}}
*{{cite book
 |title=Profil Dunia Film Indonesia
 |trans_title=Profile of Indonesian Cinema
 |language=Indonesian
 |last=Said
 |first=Salim
 |publisher=Grafiti Pers
 |location=Jakarta
 |year=1982
 |oclc=9507803
 |ref=harv
}}
 

==External links==
* 

 
 

 
 
 