Makam Piranna Manka
{{Infobox film 
| name           = Makam Piranna Manka
| image          =
| caption        =
| director       = NR Pillai
| producer       = Ponkunnam Varkey
| writer         = Ponkunnam Varkey
| screenplay     = Ponkunnam Varkey
| starring       = Jayan Jayabharathi KPAC Lalitha Adoor Bhasi
| music          = V. Dakshinamoorthy
| cinematography = C Namasivayam
| editing        =
| studio         = Kavyadhara
| distributor    = Kavyadhara
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by NR Pillai and produced by Ponkunnam Varkey. The film stars Jayan, Jayabharathi, KPAC Lalitha and Adoor Bhasi in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
 
*Jayan
*Jayabharathi
*KPAC Lalitha
*Adoor Bhasi
*Prameela
*Sankaradi
*Bhageeradhi Amma
*Elizabeth
*Janardanan
*MG Soman
*Moossathu
*Usharani
*Veeran
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Ettumanoor Somadasan and Aravind Abhayadev. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aashrama Mangalyadeepame || K. J. Yesudas, P Susheela || Ettumanoor Somadasan || 
|-
| 2 || Ini Njaanurangatte || P Susheela || Ettumanoor Somadasan || 
|-
| 3 || Kaakkikkuppaayakkaara || Vani Jairam || Ettumanoor Somadasan || 
|-
| 4 || Mallee Saayaka Lahari || K. J. Yesudas || Ettumanoor Somadasan || 
|-
| 5 || Nithyakanyake Karthike || K. J. Yesudas, Kalyani Menon || Ettumanoor Somadasan || 
|-
| 6 || Thottaal Pottunna Penne || K. J. Yesudas || Aravind Abhayadev || 
|}

==References==
 

==External links==
*  

 
 
 

 