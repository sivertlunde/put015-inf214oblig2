Squirm (film)
{{Infobox film
| name           = Squirm
| image          = Squirmposter.jpg
| caption        = 
| producer       = George Manas
| director       = Jeff Lieberman
| writer         = Jeff Lieberman
| starring       = {{plainlist |
* Don Scardino
* Patricia Pearcy
* R.A. Dow
* Jean Sullivan
* Peter MacLean
* Fran Higgins
* William Newman
* Barbara Quinn
}} Robert Prince
| cinematography = Joseph Mangine
| editing        = Brian Smedley-Aston
| distributor    = American International Pictures
| released       = July 30, 1976 (USA)
| country        = United States
| runtime        = 93 minutes English
| budget         = Unknown
| gross          = Unknown
}}
 1976 Cinema American List nature horror Rick Baker. The film was shot over the course of 24 days in Port Wentworth, Georgia.

==Plot==
 
When a powerful storm knocks Fly Creek, Georgias power lines down onto wet soil, the resulting surge of electricity drives large, bloodthirsty worms to the surface and out of their soil-tilling minds. The townspeople soon discover that their sleepy fishing village is overrun with worms that burrow right into their skin. Inundated by hundreds of thousands of carnivorous creatures, the terrorized locals race to find the cause of the rampage before becoming tilled under themselves.

==Cast==
* Don Scardino as Mick	
* Patricia Pearcy as Geraldine "Geri" Sanders	
* R.A. Dow as Roger Grimes
* Jean Sullivan as Naomi Sanders
* Peter MacLean as Sheriff Jim Reston
* Fran Higgins as Alma Sanders William Newman as Quigley
* Barbara Quinn as The Sheriffs Girl
* Carl Dagenhart as Willie Grimes
* Angel Sande as Millie
* Carol Jean Owens as Lizzie
* Kim Leon Iocovozzi as Hank
* Walter Dimmick as Danny
* Leslie Thorsen as Bonnie
* Julia Klopp as Mrs. Klopp
==Production==
 
==Release==
The film was released theatrically in the United States by American International Pictures in July 1976. 

This movie was initially rated R by the MPAA and released theatrically in that form in the U.S. Shortly after this initial theatrical release, the U.S. distributor, American International Pictures, made some minor cuts to the picture and resubmitted it to the CARA. This new cut of the picture received a PG rating and, subsequently, was also released theatrically by AIP. No additional edits were made specifically for the U.S. video release. The R-rated version has a slightly longer shot in the shower in the beginning of the film, and a slightly longer shot of the worms burrowing into Rogers face.

=== Home media ===
The film was released on DVD by MGM Home Entertainment in 2003.   The MGM re-release VHS contains the PG version, while the DVD contains the R version. The R-rated version is one minute longer than the PG-rated version.

The uncut R-rated version was released in the UK on Blu-ray and DVD by Arrow Video on September 23, 2013.   
This same version was released in the US on Blu-ray by Shout! Factory/Scream Factory on October 28, 2014.   

== Reception ==
=== Influence === TBS in the 1980s after Atlanta Braves baseball games. Braves announcer Skip Caray famously "promoted" the movie by sarcastically offering Braves fans an autographed baseball if they actually stayed up to watch it, then sent in a review of it. TBS received over 1000 reviews in response.

Pittsburgh musician Weird Paul Petroskey created an entire album, Worm in My Egg Cream, dedicated to the "worm in the egg cream" scene. All 16 tracks on the album are titled "Worm in My Egg Cream" and it makes extensive use of samples from the film.

In 1999, Squirm was one of the final films to be featured on the cult TV series Mystery Science Theater 3000.  The film was heavily edited for its MST3K appearance. Among the many scenes cut was the scene of Mick trudging through the swamp, the conversation between Mick and Alma, the worms graphic attack on Roger, the gruesome fate of Mrs. Sanders, and the climax where Roger crawls after Mick and attempts to bite him.

==References==
 

==External links==
*  
*  
*  
*   at badmovies.org
*  

 

 
 
 
 
 
 
 
 
 
 