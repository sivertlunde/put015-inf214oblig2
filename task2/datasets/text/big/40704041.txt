Ade Kannu
{{Infobox film
| name           = Ade Kannu
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Chi. Dattaraj
| producer       = Subhadra Jawahar
| writer         = 
| screenplay     = Chi. Udaya Shankar
| story          = Wali
| based on       =  
| narrator       =  Rajkumar  Gayathri
| music          = G. K. Venkatesh
| cinematography = S. V. Srikanth
| editing        = P. Bhaktavatsalam
| studio         = 
| distributor    = Hayavadana Movies
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Rajkumar and Gayathri in lead roles.

==Cast== Rajkumar
* Gayathri
* Thoogudeepa Srinivas
* Satish
* Shivaprakash
* Vijayaranjini
* Uma Shivakumar
* Bhatti Mahadevappa

==Soundtrack==
{|class="wikitable"
! Song !! Singer(s) !! Lyrics
|- Rajkumar || Chi. Udaya Shankar
|-
| "Ide Nota Ide Aata" || Rajkumar, Vani Jayaram || Chi. Udaya Shankar
|-
| "Ninne Naguvu" || P. B. Srinivas, Rajkumar || Chi. Udaya Shankar
|-
| "Ade Kannu" || Rajkumar || Chi. Udaya Shankar
|-
|}

==References==
 

==External links==
*  

 
 
 
 


 