Our Man in Marrakesh
{{Infobox film name     = Our Man in Marrakesh image    = Our Man in Marrakesh - UK cinema poster.jpg caption  = British cinema poster producer = Harry Alan Towers director = Don Sharp writer   = Peter Yeldham Harry Alan Towers starring =Tony Randall Senta Berger Herbert Lom Wilfrid Hyde-White Terry-Thomas music    = Malcolm Lockyer cinematography = Michael Reed editing        = Teddy Darvas
| studio  = Towers of London distributor    = Anglo-Amalgamated released       =   runtime        = 92 minutes country        = United Kingdom language       = English
}} 1966 British comedy spy film directed by Don Sharp and starring Tony Randall, Herbert Lom and Senta Berger. 

==Plot==
One of six travellers who catch the bus from Casablanca airport to Marrakesh is carrying $2 million to pay a powerful local man (Herbert Lom) to fix United Nations votes on behalf of an unnamed nation. But not even the powerful man knows which of them it is - and his background checks reveal that at least three of them arent who they claim to be. As agents from other nations may be among them, he and his henchmen have to be very careful until the courier chooses to reveal himself - or herself...

==Main cast==
 
* Tony Randall as Andrew Jessel
* Senta Berger as Kyra Stanovy
* Terry-Thomas as El Caid
* Herbert Lom as Mr Casimir
* Wilfrid Hyde-White as Arthur Fairbrother
* Grégoire Aslan as Achmed
* John Le Mesurier as George Lillywhite
* Klaus Kinski as Jonquil Margaret Lee as Samia Voss
* Emile Stemmler as Hotel clerk
* Helen Sanguinetti as Madame Bouseny
* Francisco Sánchez as Martinez
* William Sanguinetti as Police chief
* Hassan Essakali as Motorcycle policeman
* Keith Peacock as Philippe
* Burt Kwouk as the Import Manager
 

==Reception== Modesty Blaise. This caused the critic in The Times to write a combined review titled "Humorous variations on theme of the secret agent", where Our Man in Marrakesh is noted for having a   story similar to A Man Could Get Killed, but lacking the formers  wit. However, the film gets some credit for a literally colourful chase through Marrakeshs dyers quarter. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 


 