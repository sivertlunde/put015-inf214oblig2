Athisaya Penn
{{Infobox film
| name           = Athisaya Penn அதிசய பெண்
| image          =
| image_size     =
| caption        =
| director       = M.V. Raman
| producer       = M.V. Raman
| writer         = Javar Seetharaman
| narrator       =
| starring       = Akkineni Nageswara Rao Vyjayanthimala R. S. Manohar E. V. Saroja
| music          = S. M. Subbaiah Naidu
| cinematography = T. Muthu Sami
| editing        = M. V. Raman
| studio         = Raman Productions
| distributor    = Raman Productions
| released       = 6 March 1959
| runtime        = 
| country        = India Tamil
| budget         =
}} 1959 Tamil Romantic comedy film written by Javar Seetharaman and directed by M. V. Raman. The film starred Akkineni Nageswara Rao, Vyjayanthimala in the lead while R. S. Manohar, S. A. Ashokan,  E. V. Saroja, Madhuri Devi, K. Sankarapani, K. N. Kamlam, M. S. Thirowpathi, V. Seetharaman, Angamuthu and Eswar as the ensemble cast, was produced by M. V. Raman himself with his production company Raman Productions. The films score was composed by S. M. Subbaiah Naidu, edited by M. V. Raman and was filmed by T. Muthu Sami.

==Plot==
The story is about A. Nageswara Rao who is good-hearted person and always helps poor people even though he is from rich Zamindar family. One day he travels to Chennai to stay with his cousin R. S. Manohar, who cheats a lot with girls. When they both go for a hunt in the jungle, Manohar meets a man who demands he marry his jilted daughter . Manohar murders the father and he frames the crime on ANR, and forced him to flee. Finally ANR and his lover Vyjayanthimala prove that Manohar is the guilty one, and he tells the truth in front of everyone. Now the police arrest Manohar and ANR marries Vyjayanthimala amid happy celebrations.

==Cast==
* Akkineni Nageswara Rao
* Vyjayanthimala
* R. S. Manohar
* E. V. Saroja
* Madhuri Devi
* K. Sankarapani
* K. N. Kamlam
* S. A. Ashokan
* M. S. Thirowpathi
* V. Seetharaman
* Angamuthu
* Eswar

==Soundtrack==
The music composed by S. M. Subbaiah Naidu. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length (m:ss)
|-
| 1 || Anipillai Tennampillai || T. M. Soundararajan || 03:18
|-
| 2 || Eena Meena Deeka   || P. Susheela || 03:19
|-
| 3 || Eena Meena Deeka   || T. M. Soundararajan || 03:09
|-
| 4 || Eppo Varuvaro || P. Susheela || 03:05
|-
| 5 || Makara Veenai || A. P. Komala || 02:30
|-
| 6 || Sila Per Vazha || T. M. Soundararajan || 03:15
|-
| 7 || Thanni Podura Pazhakam || T. M. Soundararajan || 03:14
|-
| 8 || Unnai Ninaichale || P. Susheela || 03:13
|}

==Inspiration== Aasha which 7th highest grossing film of 1957. It starred by Kishore Kumar and Vyjayanthimala who reprises her role from the original and also directed by M. V. Raman.

==References==
 

==External links==
*  
*   at Upperstall.com

 
 
 
 
 


 