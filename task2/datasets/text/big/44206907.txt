The Brass Legend
{{Infobox film
| name           = The Brass Legend
| image          = The Brass Legend poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Gerd Oswald 
| producer       = Herman Cohen
| screenplay     = Don Martin 
| story          = Jess Arnold George Zuckerman Donald MacDonald Robert Burton
| music          = Paul Dunlap
| cinematography = Charles Van Enger
| editing        = Marjorie Fowler
| studio         = Robert Goldstein Productions
| distributor    = United Artists
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film Donald MacDonald Robert Burton. The film was released on December 12, 1956, by United Artists.  

==Plot==
 

== Cast ==	
*Hugh OBrian as Sheriff Wade Addams
*Nancy Gates as Linda Gipson
*Raymond Burr as Tris Hatten
*Rebecca Welles as Millie Street  Donald MacDonald as Clay Gipson Robert Burton as Tom Gipson
*Eddie Firestone as Shorty
*Willard Sage as Jonathan Tatum
*Robert Griffin as Doc Ward
*Stacy Harris as George Barlow
*Dennis Cross as Carl Barlow Russell Simpson as Deputy Pop Jackson
*Norman Leavitt as Deputy Cooper
*Vicente Padula as Sanchez 
*Clegg Hoyt as Bartender
*Jack Farmer as Earl Barlow
*Michael Garrett as Deputy Charlie
*Charles Delaney as Deputy
*Paul Sorensen as Burly Apache Bend Townsman
*Sam Flint as Old Apache Bend Townsman
*Rick Warick as Deputy

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 