The Curse (1987 film)
{{Infobox Film 
 | name = The Farm
 | image = The Curse (1987 film).jpg
 | caption = 
 | director = David Keith
 | producer = Ovidio G. Assonitis Moshe Diamant Lucio Fulci (also spfx) Anselmo Parrinello
 | writer = H. P. Lovecraft David Chaskin
 | starring = Wil Wheaton Claude Akins Malcolm Danare Cooper Huckabee John Schneider Amy Wheaton Steve Carlisle Kathleen Jordon Gregory
 | music = Franco Micalizzi
 | cinematography = Roberto Forges Davanzati
 | special effects = Lucio Fulci
 | editing = Claudio M. Cutry
 | distributor = Trans World Entertainment Metro-Goldwyn-Mayer (1998- )
 | released = September 11, 1987
 | runtime = 92 minutes
 | country = United States
 | language = English 
 | budget = 
 | gross = $1,930,001 (USA)
 }}
The Curse (also known as The Farm) is a 1987 American horror film adaptation of H. P. Lovecrafts The Colour Out of Space directed by David Keith. Famed Italian director Lucio Fulci was listed as a co-producer in the credits, and is said  to have supervised the special gore effects in the film. 
Compared to previous adaptations of the Lovecraft story, this version stays closer to the source material. 

Three other Euro-horror "sequels" were distributed on video titled Curse II: The Bite (1989), Curse III: Blood Sacrifice (1991), and Curse IV: The Ultimate Sacrifice (1993); although all four films are completely unrelated to each other except through title.

==Plot==
A meteorite lands on the farmland property of Nathan Crane (Claude Akins) in Tellico Plains, Tennessee. Local physician Alan Forbes (Cooper Huckabee) is unable to explain why the rock keeps shrinking. He is dissuaded from contacting the authorities by Charlie Davidson (Steve Carlisle), a realtor who does not want the new arrival to discourage the Tennessee Valley Authority (TVA) from establishing a new reservoir in the area. As the rock shrinks away to nothing, a glowing color seeps out and into the ground. Within a few weeks, the farms crops bloom but are soon discovered to be inedible. 

Shortly after, the local animals, as well as Nathans wife, begin to go mad and a previously unknown element is discovered in the propertys well. Soon Nathan and his son Cyrus (Malcolm Danare) are also driven insane and begin terrorizing those who come near, including Nathans other children, Zack (Wil Wheaton) and Alice (Amy Wheaton). 
 John Schneider) and the house collapses into the ground. 

==Cast==
* Claude Akins as Nathan Crane
* Cooper Huckabee as Dr. Allen Forbes  John Schneider as Carl Willis 
* Malcolm Danare as Cyrus Crane
* Wil Wheaton as Zack Crane
* Amy Wheaton as Alice Crane
* Steve Carlisle as Charlie Davidson 
* Kathleen Jordon Gregory as Frances Crane
* Hope North as Esther Forbes
* Steve Davis as Mike

==Production==

David Keith used his farm in Tellico Plains, Tennessee, for the exterior scenes. The interior scenes were shot in Rome.   

==DVD release==

MGM released the film onto DVD in 2007 as a double feature with its in-name-only sequel,  .

==Reception==

Lovecraft scholar Charles P. Mitchell referred to the film as faithful to the authors original work, but claimed that " he last twenty minutes of the film are so disjointed that they virtually ruin the entire film."    

In their book  ,  ," has it all... everything except good dialog, believable acting, and a cohesive plot." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 