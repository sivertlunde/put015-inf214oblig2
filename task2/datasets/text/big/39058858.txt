Hunterwali
 
 
{{Infobox film
| name           = Hunterwali
| image          = File:Nadia-hunterwali-1935.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Homi Wadia
| producer       = Wadia Movietone
| writer         = Homi Wadia
| screenplay     = Homi Wadia Joseph David
| story          = J.B.H. Wadia
| based on       = 
| narrator       = 
| starring       = Fearless Nadia
| music          =  Master Mohammed
| cinematography = Balwant Dave 
| editing        = 
| studio         = Wadia Movietone
| distributor    = Wadia Movietone
| released       = 1935
| runtime        = 164 minutes
| country        = India
| language       = Hindi
| budget         = Rs 80,000
| gross          = 
}}
 Hindi Action stunt film by the Wadia Movietone company of Bombay (now Mumbai), featuring Fearless Nadia as the heroine. A story of a princess who fights injustice as the masked crusader Hunterwali (lit. "lady with the whip"), the film propelled Nadia and the Wadia brothers of Wadia Movietone to fame.

Hunterwali was the first lead role for Nadia. She performed many stunts in the film which were applauded by the audience. The film, an expensive venture, was a blockbuster. It inspired numerous products, incorporating Hunterwali in their brand names. Because of the movies success, Nadia became a cult icon and starred in numerous stunt films, becoming Indian cinemas "earliest and most popular stunt actress". 

==Plot==
The story begins on a stormy night, with a prologue explaining that Krishnavati and her infant son are getting evicted from her house by the Prime Minister (Vazier), Ranamal. Earlier, Ranamal had also had her brother murdered. The film switches to 20 years later when Krishnavatis son Jaswant is an adult. The royal car hits Jaswant in an accident. Then the scene leads to Princess Madhuri (Fearless Nadia) offering Jaswant compensation in the form of gold for the injury caused. Jaswant gallantly refuses the gift and the princess is attracted to him. The villain Ranamal also has a crush on the princess and wants to marry her. This proposal is opposed by her father the king who is imprisoned by Ranamal. Madhuri, assumes the role of "Hunterwali", a masked vigilante, the "protector of the poor and punisher of evildoers". She then goes around performing stunts like jumping over a moving carriage and then defeating 20 soldiers in one sweep with swashbuckling whipping style. She does not spare Jaswant either as she steals his prized possession, a horse called "Punjab", but soon gives it back to him. Jaswant plots his vendetta and finds Madhuri bathing nude in the river and kidnaps her and gifts her to Ranamal for a reward, but she later escapes. At the end, Madhuri and Jaswant join hands to fight Ranamal together and defeat him.    

==Cast==

The members of the cast were: 
* Fearless Nadia (Mary Evans) as Princess Madhuri aka Hunterwali
* Boman Shroff, as Jaswant
* Jaidev, as Cunnoo, Hunterwalis sidekick
* Sharifa, as Krishnavati, mother of Jaswant
* Master Mohammed as the King 
* John Cawas
* Gulshan
* Sayani
* Atish

==Production==
 

Hunterwali was a Black and White film running for 164 minutes.  During the production stage, there was an objection to the films title. Language pundits objected to the blending of two words; Hunter, an English word denoting  "whip"  and wali, which was a Hindi word made a corrupted hybrid which did not do justice to either language. 

The main character in the film, Hunterwali/Madhuri, is the feisty and blue eyed beauty, Nadia in the disguise of a man. Mary Evans aka Nadia, an Australian entered Indian cinema in 1934 and had acted in two films, Desh Deepak and Noor-e-Yaman, before Hunterwali. Hunterwali was the first lead role of Nadia.  Her character for the role had evolved over years with her training in riding, as a dancer, a fitness freak, a circus artist and as a theatre artist during the 1920s and early 1930s, which was accentuated by a svelte heavily built but supple Amazonian athletic figure with blond hair and blue-eyes. All these qualities made her an instant attraction to the Wadia brothers of Wadia Movietone, though initially they were sceptical about a white woman "breaking into a Brown male bastion"    of stunt films and acceptance by the conservative audience during the British Raj.   

The producers Wadia Movietone (founded by the brothers JBH Wadia and Homi Wadia) specialised in stunt-based and mythological films at that time. The story line was developed by J.B.H. Wadia to suit Nadias character. The plot is developed around the historical theme of a brave fearless Indian girl who forgoes her royal luxurious lifestyle to be a peoples person. The film was directed by Homi Wadia (Nadias future husband) who also wrote the screenplay for the film, with Joseph David writing the dialogue.       The concept of Hunterwali was inspired by Hollywood films like Robin Hood starring Douglas Fairbanks.  

Inspired by the film serial The Perils of Pauline, Nadia did all her stunts.    In the role of a masked avenging angel, the film pictured Nadia as a swashbuckling princess in disguise. In this disguise, she straddled on her horse around the countryside chasing enemies, wearing her hot pants, "with her big breasts and bare white thighs, and when she was not swinging from chandeliers, kicking or whipping men, she was righting wrong with her fists and imperious scowl."   

The film is embellished with many bhajans by Govind Gopal. One of the popular songs starts with "Hunterwali hai bhali duniya ki leth," which is a glorification of Hunterwali. Master Mohammed, who acted as the king who also provided the music score. 

The Wadias had spent a fortune not only in making the film but also in its publicity campaign. It was premièred in Bombay at the Super Cinema, on Grant Road. It took six months to make with a budget of Rs. 80,000, which was a fortune. There were no takers for releasing the film, in view of adverse comments about the blonde beauty of a heroine, so the partners pooled their resources and launched a huge publicity campaign. As a result of which, there were huge crowds to see the film on its opening night and thereafter it was a runaway success. 

==Reception and legacy==

Hunterwali was a runaway success and a bonanza in terms of money earned, as it ran for 25 weeks making record earnings for the year.  This was Nadias and Wadia Movietones first big success.  

Nadia as Hunterwali/Princess Madhuri was the highlight of the film, despite of her inadequacy in rendering dialogues in Hindi.  Her stunts on trains and horse carriages as well as those with animals as well as her blond appearance were well received by the audience.    A popular scene in the film was the moment when Hunterwali announced in the third reel that "aajse main Hunterwali hoon" ( "from today, I am the woman with the whip"), which resulted in the maximum cheering from the crowd.  One of the interesting scenes, presented as tableaux, is that of Hunterwali lifting a man over her head with the sidekicks pirouetting around her, performing gymnastics, in the middle of a chase; this scene almost became a stereotype scene in most of her later films. 

Hunterwali was the most popular character of its time and was listed as "Bollywoods best loved character" in 100 years of Indian cinema by CNN-IBN.  Hunterwalis yell in the film "hey-y-y" became a catch phrase.  Following the success of the film, Nadia became known as  "Hunterwali Nadia" and became a legendary figure in the Indian cinema.    Hunterwali Nadia emerged as feminist icon of Hindi cinema, who portrayed as the one who would break the glass ceiling and overturn the patriarchal order.  Despite her accented Hindi, Nadia became a popular star, following the film.  She also became the highest paid actress in the Indian film industry for the next two decades.  Nadias success and popularity was furthered by similar performances in the films of the same genre – also huge hits – in the next few years.   

Hunterwalis adulation was exploited to promote various products with the name "Hunterwali" prefixed to the brand name such as whips, belts, playing cards, chappals, bags, bangles, match boxes, shoes, shirts etc.   Wadia later regretted that he did not patent the word "Hunterwali", which could have brought him a fortune in royalties. The suffix of the word wali also became a fad with film producers who named many stunt films using it, like Cyclewali, Chabukwali and Motorwali.  Wadia himself capitalised on the Hunterwali icon by releasing a sequel Hunterwali Ki Beti ("daughter of Hunterwali") with Nadia in 1943. Like Hunterwali, the sequel became a blockbuster.   

A 62-minute retrospective documentary titled Fearless: the Hunterwali Story was made by Nadias grandnephew Riyad Vinci Wadia in 1993 on the actress life.  In April 2013, during the centenary festival of Indian cinema in Delhi, the film along with silent classic, Diler Jiger (1931, Gallant Hearts), was shown in the "tent cinema" erected as a part of the celebrations.  

==Notes==
 

==Bibliography==
* 
* 
*  
*  }}

==External links==
 
*  

 

 
 
 
 
 
 