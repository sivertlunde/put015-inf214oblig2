The Watermelon Woman
{{Infobox film
| name = The Watermelon Woman
| image = WatermelonWomanPoster.JPG
| caption = Original movie poster
| director = Cheryl Dunye
| producer = Alexandra Juhasz Barry Swimar
| writer = Cheryl Dunye
| narrator =
| starring = Cheryl Dunye Guinevere Turner Paul Shapiro
| cinematography = Michelle Crenshaw
| editing = Cheryl Dunye
| distributor = First Run Features
| released =  
| runtime = 90 minutes
| country = United States English
| budget =$300,000 
| gross =
}}
The Watermelon Woman is a 1996 feature film by filmmaker Cheryl Dunye about Cheryl, a young black lesbian working a day job in a video store while trying to make a film about a black actress from the 1930s known for playing the stereotypical "mammy" roles relegated to black actresses during the period. It was the first feature film directed by a black lesbian.  

==Plot== Philadelphia with documentary about the Watermelon Woman and find out more about her life.
 flirts with Cheryl.

Cheryl starts interviewing members of the public, asking them if they have heard of the Watermelon Woman. She interviews her mother who does not remember the name, but recognises a photograph of her. She tells Cheryl that she used to hear the Watermelon Woman singing in clubs in Philadelphia. Tamaras mother tells Cheryl to get in contact with Lee Edwards&nbsp;— a man who has done a lot of research into black films. Cheryl and Tamara go to see Lee, and he tells them about 1920s and 30s black culture in Philadelphia. He explains to them that in those days, black women usually played domestic servants.

Cheryl meets her mothers friend Shirley, who turns out to be a lesbian. Shirley tells her that the Watermelon Womans name was Fae Richards, that she was a lesbian too, and that she used to sing in clubs "for all us stone butches". She says that Fae was always with Martha Page, the white director of Plantation Memories, and that Martha was a mean and ugly woman.

When Cheryl and Tamara get caught ordering video tapes under Dianas name, Diana takes the tapes and tells Cheryl that she will have to come to her home to collect them. Cheryl goes to Dianas house, stays for dinner, and watches some of the tapes with her, telling her about her project. They have sex, and Cheryl decides that although Diana is not her usual type of woman, she likes being with her.

Cheryl meets cultural critic Camille Paglia who tells her about the Mammy archetype, saying that it represented a goddess figure. Cheryl goes to the CLIT archive of lesbian material, and finds photographs of Fae Richards, including one given by Fae to a June Walker. With Dianas help, Cheryl manages to contact Martha Pages sister who denies that Martha was a lesbian.

As Cheryl and Diana grow closer, Tamara makes it clear that she dislikes Diana and disapproves of their relationship. She accuses Cheryl of wanting to be white, and Diana of having a fetish for black people.
 partner for 20 years. They arrange to meet, but June is taken to hospital and leaves a letter for Cheryl instead. In the letter she says that she is angry with Martha Page, that Martha had nothing to do with what Faes life. She urges Cheryl to tell their history.

Having separated from Diana, and fallen out with Tamara, Cheryl finishes her project, never managing to make further contact with June.

==Cast==
* Cheryl Dunye as Cheryl
* Guinevere Turner as Diana
* Valarie Walker as Tamara
* Lisa Marie Bronson as Fae The Watermelon Woman Richards
* Cheryl Clarke as June Walker
* Irene Dunye as herself
* Brian Freeman as Lee Edwards
* Camille Paglia as herself
* Sarah Schulman as CLIT archivist
* V.S. Brodie as Karaoke Singer
* Robert Reid-Pharr 

==Background==
The Watermelon Woman was Dunyes first feature film and the first by a black lesbian.   It was made on a budget of $300,000, financed by a $31,500 grant from the National Endowment for the Arts (NEA), a fundraiser, and donations from friends of Dunye.    The photographic Fae Richards Archive, documenting the fictional actress life, was created by New York City-based photographer Zoe Leonard.  Made up of 78 images, the collection was later exhibited in galleries and as a book.

==Distribution==
The Watermelon Woman premiered at the 1996 Berlin International Film Festival and went on to play at several other international film festivals during 1996 and 1997, including the New York Lesbian & Gay Film Festival, L.A. Outfest, the San Francisco International Lesbian & Gay Film Festival, the Tokyo International Lesbian & Gay Film Festival, the Créteil International Womens Film Festival, the London Lesbian and Gay Film Festival, and the Toronto International Film Festival.  

The film was released in the United States on March 5, 1997, distributed by First Run Features.  It was released onto Region 1 DVD on September 5, 2000. 

==Reception==

===Awards===
In 1996, The Watermelon Woman won the Teddy Award for Best feature film at the Berlin International Film Festival and the Audience Award for Outstanding Narrative Feature at L.A. Outfest. {{cite web
  | last = Swartz
  | first = Shauna
  | title =  Review of The Watermelon Woman
  | publisher = AfterEllen.com
  | date = 2006-03-15
  | url = http://www.afterellen.com/archive/ellen/Movies/2006/3/watermelon.html
  | accessdate =2008-04-27 }} 

===Critical===
Critical reviews of the film were generally positive. Stephen Holden of The New York Times called the film "both stimulating and funny".  He praised Dunye for her "talent and open-heartedness" and enjoyed the films moments of comedy.  He said that the film "lets you find your own way to its central message about cultural history and the invisibility of those shunted to the margins."  Writing for the San Francisco Chronicle, Ruthie Stein had a similar opinion to Holden, saying that, despite the seriousness of the films topics, it "never takes itself too seriously."  She praised Dunyes "engaging personality" and said that she "has infused   with a lightness that seems to match her spirit."  The Advocates Anne Stockwell said that "this rollicking, sexy movie never gets self-important."  She praised the "footage" of Fae Richards and Zoe Leonards work on the photo archive of the fictional actress as "one of the films joys". 

Emanuel Levy rated the film as a "B", saying that it was "only a matter of time before a woman of color made a lesbian film."  He said that while " oking fun at various sacred cows in American culture", it "makes statements about the power of narrative and the ownership of history."  In a review for The Austin Chronicle, Marjorie Baumgarten called the film "smart, sexy   funny, historically aware, and stunningly contemporary." 

===Criticism of NEA funding=== dyke sex scene ever recorded on celluloid".  On June 14, Julia Duin wrote an article for The Washington Times, quoting DeLombards review and questioning the $31,500 grant given to Dunye by the NEA.  
 Representative Peter House Education Subcommittee on Congress to minority or female recipients.  A spokesperson for Hoekstra said that he had no problem with gay content, just those that contained explicit sex. 

==See also==
*Sacred cow (idiom)

==References==

===Notes===
{{Reflist|2|refs=
 {{cite web |last=Haslett |first=T. |author2=N. Abiaka  |title=Cheryl Dunye&nbsp;— Interview|publisher=Black Cultural Studies Web Site Collective |date=April 12, 1997 |url=http://www.cheryldunye.com/pages/interview.html |accessdate=April 27, 2008
}} 
 Sullivan, p. 211.
  The Phoenix  | date = May 8, 1997  | url = http://www.bostonphoenix.com/alt1/archive/movies/reviews/05-08-97/THE_WATERMELON_WOMAN.html  | accessdate=April 29, 2008
}} 
 {{Citation  | title = The Watermelon Woman&nbsp;— Cast  | newspaper = The New York Times  | publisher = The New York Times Company  | url = http://movies.nytimes.com/movie/136227/The-Watermelon-Woman/cast  | accessdate =June 6, 2008
}} 
 {{Citation  | last = Baumgarten  | first = Marjorie | title = Film Listings: The Watermelon Woman  | newspaper = The Austin Chronicle | publisher = Austin Chronicle Corp. | date = July 18, 1997  | url =http://www.austinchronicle.com/gyrobase/Calendar/Film?Film=oid%3A142174  | accessdate=May 13, 2010
}} 
 McHugh, p.275.
 
 {{Citation  | last = Warner  | first = David  | title = Dunye, Denzel and more  | newspaper = Philadelphia City Paper  | date = October 17, 1996  | url =http://www.citypaper.net/articles/101796/article023.shtml  | accessdate = April 28, 2008
}} 
 {{Citation  | last = Stockwell  | first = Anne  | title = Color-corrected film  | newspaper = The Advocate  | publisher = LPI Media  | page = 53  | date = March 4, 1997  | url = http://books.google.com/?id=VWQEAAAAMBAJ&pg=PT53  | accessdate =May 15, 2010
}}  Variety  | url = http://www.variety.com/profiles/Film/main/134826/The%20Watermelon%20Woman.html?dataSet=1  | accessdate=April 27, 2008
}}   
 {{cite web  | title = Official Site  | year = 2005  | url = http://www.cheryldunye.com/pages/watermelon.html  | accessdate =April 27, 2008
}} 
 {{cite web  | title = The Watermelon Woman Movie Reviews  | publisher = Rotten Tomatoes  | url = http://uk.rottentomatoes.com/m/watermelon_woman/  | accessdate =April 28, 2008
}} 
 {{citation  | last = Swartz  | first = Shauna  | title =  Review of The Watermelon Woman  | newspaper = AfterEllen.com  | date = March 15, 2006  | url = http://www.afterellen.com/archive/ellen/Movies/2006/3/watermelon.html  | accessdate =April 27, 2008
}} 
 {{Citation  | last = Holden  | first = Stephen  | author-link = Stephen Holden  | title = On Black Films and Breezy Lesbians  | newspaper = The New York Times  | publisher = The New York Times Company  | date = March 5, 1997  | url = http://movies.nytimes.com/movie/review?_r=1&res=9C04E2DD1430F936A35750C0A961958260  | accessdate =May 13, 2010
}} 
 {{Citation  | last = Stein  | first = Ruthie  | title = Watermelon Woman Digs Fruitfully Into a Faux Past  | newspaper = San Francisco Chronicle  | publisher = Hearst Corporation  | date = July 25, 1997  | url = http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/1997/07/25/DD20796.DTL  | accessdate =May 13, 2010
}} 
 {{Citation  | last = Levy  | first = Emanuel  | author-link = Emanuel Levy  | title = The Watermelon Woman  | newspaper = EmanuelLevy.com  | url = http://www.emanuellevy.com/search/details.cfm?id=2533
  | accessdate =May 13, 2010
}} 
 {{Citation  | last = DeLombard  | first = Jeannine  | title = The Watermelon Woman Review  | newspaper = Philadelphia City Paper  | date = March 3, 1996  | url = http://www.citypaper.net/articles/042497/article043.shtml  | accessdate = April 28, 2008
}} 
 Wallace, p.457.
 
 {{Citation  | last = Miller  | first = Judith  | author-link =   | title =  Lobbyists Fight Cuts On Arts Day In Capital  | newspaper = The New York Times  | publisher = The New York Times Company | date = March 13, 1997  | url =http://query.nytimes.com/gst/fullpage.html?res=9E05E2DD1039F930A25750C0A961958260&sec=&spon=  | accessdate=April 29, 2008
}}  page = publisher = LPI Media | accessdate = May 13, 2010
}} 
 {{Citation  | last = Rich  | first = Frank  | author-link =   | title =  Lesbian Lookout  | newspaper = The New York Times  | date = March 13, 1997  | url = http://query.nytimes.com/gst/fullpage.html?res=9C0DE2DB1039F930A25750C0A961958260&sec=&spon=  | accessdate=April 29, 2008
}} 
}}

===Bibliography===
*{{Citation
 | last =McHugh
 | first =Kathleen
 | author-link =
 | publication-date =
 | contribution =Autobiography
 | contribution-url =http://books.google.co.uk/books?id=tRoF5inVS3AC&pg=PA271&source=gbs_toc_r&cad=0_0&sig=lT5X0CUAuLmJeNc8ER9nI1OSgK4#PPA267,M1
 | editor-last =Lewis
 | editor-first =Jon
 | title =The End of Cinema as We Know It
 | publisher =Pluto Press
 | isbn =0-7453-1879-7
 | url =http://books.google.com/?id=tRoF5inVS3AC
 | year =2002
}}
*{{Citation
 | last =Sullivan
 | first =Laura L.
 | author-link =
 | year =2004
 | publication-date =
 | contribution =Chasing Fae: The Watermelon Woman and Black Lesbian Possibility
 | contribution-url =http://books.google.co.uk/books?id=dGbKz7-z6YcC&pg=RA1-PA211&source=gbs_toc_r&cad=0_0&sig=TsiP3HrCPXgm6pDqFf1_TxFyboM
 | editor-last =Bobo
 | editor-first =Jacqueline
 | editor-link =
 | editor2-last =Hudley
 | editor2-first =Cynthia
 | editor2-link =
 | editor3-last =Michel
 | editor3-first =Claudine
 | editor3-link =
 | title =The Black Studies Reader
 | publisher =Routledge
 | isbn =0-415-94553-4
 | url =http://books.google.com/?id=dGbKz7-z6YcC
}}
*{{Citation
  | last = Wallace
  | first = Michele
  | authorlink = Michele Wallace
  | title = Dark Designs and Visual Culture
  | publisher = Duke University Press
  | year = 2004
  | pages = 457–459
  | url = http://books.google.com/?id=q7aqehufGNQC
  | isbn =0-8223-3413-5 }}

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 