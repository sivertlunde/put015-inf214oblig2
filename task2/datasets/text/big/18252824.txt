Billy Ze Kick (film)
{{Infobox film
| name           = Billy Ze Kick
| image          = 
| alt            =  
| caption        = 
| director       = Gérard Mordillat
| producer       = 
| writer         = Gérard Guérin Gérard Mordillat Francis Perrin Dominique Lavanant Zabou Breitman Marie France
| music          = Jean-Claude Petit
| cinematography = Jean Monsigny
| editing        = Michèle Catonné
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Billy Ze Kick is a 1985 French film directed by Gérard Mordillat.  Zabou Breitman received a César Award|César nomination for Most Promising Actress.

== Plot ==
Billy Ze Kick is name of a fictional serial killer in a bedtime story that a police inspector reads to his daughter. Soon three girls turn up murdered in his neighbourhood, and the killer leaves a note signed "Billy Ze Kick."

== Cast == Francis Perrin as Inspecteur Chapeau
*Dominique Lavanant as Madame Achere Zabou Breitman as Juliette Chapeau
*Marie France as Miss Peggy
*Patrice Valota as Eugene
*Jacques Pater as Inspecteur Cordier
*Pascal Pistacio as Hippo
*Cérise Bloc as Julie-Berthe
*Michael Lonsdale as Commissaire Bellanger
*Yves Robert as Alcide
*Benjamin Azenstarck as Ed

==References==
 

== External links ==
* 

 
 

 