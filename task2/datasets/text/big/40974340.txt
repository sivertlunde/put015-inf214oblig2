My Valet
{{Infobox film
| name           = My Valet
| image          = File:MabelNormandMyValet1915.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Mack Sennett
| producer       = Mack Sennett
| writer         = Mack Sennett
| narrator       = Raymond Hitchcock Mack Sennett Mabel Normand
| music          =
| cinematography =
| editing        =
| distributor    = Keystone Film Company Triangle Distributing
| released       =  
| runtime        = 33 minutes
| country        = United States
| language       = English
| budget         =
}} Raymond Hitchcock, Sennett, and Mabel Normand.  The film was released by the Keystone Film Company and Triangle Distributing with a running time of 33 minutes. It was released on November 7, 1915 in the United States. The movie is in black and white and produced in English. 

==Cast== Raymond Hitchcock	 ...	
John Jones 
Mack Sennett	 ...	
Johns Valet 
Mabel Normand	 ...	
Mabel Stebbins 
Fred Mace	 ...	
French Count 
Frank Opperman	 ...	
Hiram Stebbins 
Alice Davenport	 ...	
Mrs. Stebbins

==References==
 

==External links==
*   in the New York Times
*   in the Internet Movie Database
*   at Turner Classic Movies
*   in Looking for Mabel Normand

 
 
 
 

 