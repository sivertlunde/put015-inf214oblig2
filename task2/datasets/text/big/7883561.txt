Rented Lips
{{Infobox film
| name           = Rented Lips
| image          = Poster of the movie Rented Lips.jpg
| image_size     =
| caption        =
| director       = Robert Downey, Sr.
| producer       =
| writer         = Martin Mull
| narrator       =
| starring       = Martin Mull Dick Shawn Jennifer Tilly Robert Downey, Jr.
| music          = Van Dyke Parks
| cinematography = Robert D. Yeoman
| editing        =
| distributor    = The Vista Organization
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
}}
 1988 comedy-satire film directed by Robert Downey, Sr., and starring his son, Robert Downey, Jr., as well as Martin Mull, Dick Shawn, and Jennifer Tilly.

It was the final film appearance of Shawn, who died in 1987.

==Premise==
 
A documentary film maker, whose most recent work is Aluminum, Our Shiny Friend, is offered the chance to shoot his long dreamt of musical on Indian Farming Techniques if he completes the shooting of a porn film financed by an evangelical church and uses the same crew and cast for both films.

Complications ensue.

==Release==

Sometime after its theatrical run, International Video Entertainment released the movie on VHS. The movie has not been released on DVD and as of December 21, 2009, Lions Gate has not announced any plans for a DVD release.

==Cast==
* Martin Mull as Archie Powell
* Dick Shawn as Charlie Slater
* Jennifer Tilly as Mona Lisa
* Robert Downey, Jr. as Wolf Dangler
* Kenneth Mars as Rev. Farrell
* Edy Williams as Heather Darling
* June Lockhart as Archies Mother

==External links==
*  
*  

 

 
 
 
 
 
 
 


 