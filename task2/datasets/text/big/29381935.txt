Agniputra
{{Infobox film
 | name = Agniputra 
 | image = 
 | caption = DVD Cover
 | director = B. Vijay Reddy
 | producer = J. K. Movie Units
 | camera=
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty
 | music = Nikhil-Vinay
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  February 04, 2000
 | runtime = 125 min.
 | language = Hindi Rs 4.0 Crores
 | preceded_by = 
 | followed_by = 
 }}
Agniputra is an action Hindi film made in 2000. A revenge drama, with Mithun in the lead role.

==Plot==
The story begins with a man Arjun(mithun) working in a band with his family, consisting of 3 sisters and his widowed mother. Arjun then falls in love with the daughter of a MLA,  who was involved with his family. The police make a false accusation against Arjun and his sisters and accuse them of being sex workers. It turns out that the MLA is behind the case. The rajas sisters end up being sexually assaulted by the MLA, and two of them end up committing suicide. Arjun decides to seek vengeance against the goons. and with some toil and foil he eventually outsmarts them.

==Cast==

*Mithun Chakraborty
*Shashikala
*Deep Shikha
*Maleeka R Ghai
*Prem Chopra
*Asrani
*Vinita
*Pramod Moutho

==Soundtrack==
The soundtrack is composed by Nikhil-Vinay with lyrics by Anand Bakshi. Poornima
* "Ek Hassen Ladki" - Udit Narayan
* "Kisne Dekha Kisne Jana" - Sonu Nigam
* "Solah Baras Intezar Karliya" - Asha Bhosle
* "Tu Ne Mujhe Pukara" - Kumar Sanu, Anuradha Paudwal

==Reception==

The Film was a moderate Success,  as it followed Mithuns low budget formula based at ooty

==References==
 
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Agni+Putra

==External links==
* 

 
 
 
 
 
 