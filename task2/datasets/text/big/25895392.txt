Hearts and Flowers for Tora-san
{{Infobox film
| name = Hearts and Flowers for Tora-san
| image = Hearts and Flowers for Tora-san.jpg
| image size = 190px
| caption = Theatrical release poster
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Yoshitaka Asama Ayumi Ishida
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 110 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}
 Ayumi Ishida as his love interest or "Madonna".  Hearts and Flowers for Tora-san is the twenty-ninth entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
During his travels, Tora-san gets drunk with an old man in Kyoto. Though Tora-san never fully comprehends his importance, the old man is a Living National Treasure ceramist. At his home, Tora-san makes a good impression on the old mans maid, who apparently falls in love with Tora-san.       

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura Ayumi Ishida as Kagari
* Nizaemon Kataoka as Kanō
* Shimojo Masami as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hidetaka Yoshioka as Mitsuo Suwa
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō
* Chishū Ryū as Gozen-sama
* Akira Emoto as Kondō

==Critical appraisal== Ayumi Ishida Japan Academy Prize for their roles in Hearts and Flowers for Tora-san. Long-time composer for the series, Naozumi Yamamoto, was nominated for Best Music Score. Enomoto won the award for Best Supporting Actor at the Blue Ribbon Awards. 
Stuart Galbraith IV writes that the film "is no better than average for this series, but then again this series average is awfully high." He notes that this is the first episode in the series in which Tora-sans nephew Mitsuo plays a significant role in the plot.  The German-language site molodezhnaja gives Hearts and Flowers for Tora-san three and a half out of five stars.   

==Availability==
Hearts and Flowers for Tora-san was released theatrically on August 7, 1982.  In Japan, the film has been released on videotape in 1986 and 1996, and in DVD format in 2002 and 2008. 

==References==

===Notes===
 

===Bibliography===
;English
*  
*  
*  
*  

;German
*  

;Japanese
*  
*  
*  
*  

==External links==
* 
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 