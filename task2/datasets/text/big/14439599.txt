Witness to the Mob
 
{{Infobox television film
| name = Witness to the Mob
| image = Witness to the Mob.jpg
| caption = 
| genre = Crime Drama
| director = Thaddeus OSullivan 
| writer = Stanley Weiser
| starring = Nicholas Turturro Tom Sizemore Michael Imperioli and  Vincent Pastore 
| producer = Caroline Baron (producer) Robert De Niro (executive producer) Brad Epstein (executive producer) Pamela Reis (associate producer) Jane Rosenthal (executive producer) Amy Sayres (associate producer)  
| music = Stephen Endelman Sonny Kompanek	
| cinematography = Frank Prinzi David Ray NBC Studios Tribeca Productions
| distributor = NBC
| network = NBC
| released =  May 10, 1998 
| runtime = 240 min.
| country = USA  English 
| budget =  
}}
Witness to the Mob is a made for TV film which premiered on May 10, 1998.

==Plot== Cosa Nostra Witness Protection Program.
The film is based on the book about Gravano, titled Underboss, written by Peter Maas. Main source of information was Gravano himself.

==Cast==
*Nicholas Turturro - Sammy Gravano
*Tom Sizemore - John Gotti
*Debi Mazar - Deborah Gravano
*Abe Vigoda - Paul Castellano
*Philip Baker Hall - Salvatore "Toddo" Aurello
*Frank Vincent - Frank DeCicco
*Lenny Venito - Sal DiMaggio
*Johnny Williams - Angelo Ruggiero
*Frankie Valli - Frank Locascio
*Michael Imperioli - Louie Milito
*Vincent Pastore - Michael DeBatt
*Peter McRobbie - George Pape/Gerard Pappa
* Nicholas Kepros - Vincent "The Chin"/Vincent Gigante
*Michael Ryan Segal - Nicky Cowboy/Nicholas Mormando
*Tony Sirico - Tommy Gambino/Thomas Gambino Richard Bright - Joseph Paruta
*Richard E.Council - Louie DiBono/Louis DiBono
*Peter Appel - Eddie Garofal/Edward Garofalo
*Tony Kruk - Di Bernardo/Robert DiBernardo
*Stephen Payne - Bosko Radonjich
*Leonardo Cimino - Neil Dellacroce 
== External links ==
*  

 

 
 
 
 
 


 
 