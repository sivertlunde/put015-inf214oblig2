Fort Yuma (film)
{{Infobox film
| name           = Fort Yuma
| image          = Fortyumapos.jpg
| image_size     =
| caption        = Film poster
| director       = Lesley Selander
| producer       = Aubrey Schenck
| writer         = Danny Arnold
| narrator       =
| starring       = Peter Graves
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = United Artists
| released       =  
| runtime        = 78 min
| country        = United States
| language       =
| budget         =
| gross          =
}} Western film starring Peter Graves.  

==Plot==
Peter Graves is Lieutenant Ben Keegan in the U.S. Cavalry fighting Apache Indians.

==Production==
The film was edited before release with many violent scenes being excised. 

The film was originally denied a seal from the Production Code Administration. Geoffrey Shurlock told producer Howard W. Koch that it contained "sadism and excessive gruesomeness". To get a seal, Koch reduced the number of killings from 24 to 10. Removed were scenes where a man is spread-eagled and torn apart by horses; an arrow impaling a hand to wood; and a scene depicting the bodies of hanged Indians, swaying from tree limbs. 

==Starring==
*Peter Graves as the Lt. Ben Keegan
*Joan Vohs  as  Melanie Crown John Hudson as  Sgt. Jonas
*Joan Taylor as Francesca James OHara as Cpl. Taylor

==Notes==
 

==References==
*Langman, Larry & Borg, Ed Encyclopedia of American War Films Garland Publ., 1989
*Wetta, Frank Joseph & Curley, Stephen J. Celluloid Wars: A Guide to Film and the American Experience of War Greenwood Publishing Group, 01/01/1992
*  
*  
 
 
 
 
 
 
 
 

 