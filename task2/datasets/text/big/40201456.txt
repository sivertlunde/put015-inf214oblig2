Tiny Times 2
{{Infobox film
| name           = Tiny Times 2
| image          = Tiny Times 2 poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Guo Jingming
| producer       = 
| writer         = Guo Jingming
| screenplay     = 
| story          = 
| based on       = Tiny Times 2.0 by Guo Jingming
| narrator       = 
| starring       = Yang Mi   Ko Chen-tung   Amber Kuo   Rhydian Vaughan   Haden Kuo   Xie Yilin   Chen Xuedong
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 116 minutes
| country        = China 
| language       = Mandarin
| budget         = 
| gross          = US$47.22 million 
}}
Tiny Times 2 (Chinese: 小时代：青木时代) is a 2013 Chinese film written and directed by Guo Jingming and is a sequel to 2013s film Tiny Times.   The film was filmed together with the first film, and was based on the second half of Guos own novel.  Tiny Times 3, the third installment of the Tiny Times series was released on July 17, 2014.

==Cast==
* Yang Mi as Lin Xiao
* Ko Chen-tung as Gu Yuan
* Amber Kuo as Gu Li
* Rhydian Vaughan as Gong Ming Bea Hayden Kuo
* Xie Yilin as Tang Wanru
* Chen Xuedong as Chong Guang
* Wang Lin as Ye Chuanping
* Li Yueming as Jian Xi
* Shang Kan as Kitty
* Jiang Chao as Xi Cheng
* Calvin Tu as Wei Hai
* Ding Qiaowei as Yuan Yi
* Yolanda Yang as Lin Quan

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 