The White Dragon (film)
 
 
{{Infobox film name = The White Dragon image = The-White-Dragon-Poster.jpg alt =  caption = Film poster traditional = 小白龍情海翻波 / 飛俠小白龍 simplified = 小白龙情海翻波 / 飞侠小白龙 pinyin = Xiǎobáilóng Qínghǎi Fānbō / Fēixiá Xiǎobáilóng}} director = Wilson Yip producer = Joe Ma Y.Y. Kong Cao Biao writer = Wilson Yip Law Yiu-fai starring = Cecilia Cheung Francis Ng Andy On Benz Hui Patrick Tang Liu Lei music = Tommy Wai cinematography = Cheung Man-po Tony Miu editing = Cheung Ka-fai studio = China Star Entertainment Group, One Hundred Years of Film Company Ltd., China Film Co-Production Corp., Singing Horse Production Ltd., China Film Group Corporation distributor = China Star Entertainment Group, China Film Group Corporation released =   runtime = 93 minutes country = Hong Kong language = Cantonese budget =  gross = 
}}
The White Dragon is a 2004 Hong Kong wuxia comedy film directed by Wilson Yip and starring Cecilia Cheung and Francis Ng.

The White Dragon is directed by Wilson Yip, whose best known works to date are Bullets Over Summer, Juliet in Love and  . It stars Cecilia Cheung as Phoenix Black, a pretty, young, and vain student who thinks she has what it takes to woo the most desirable eligible bachelor in the land, Second Prince Tian Yang (Andy On).

==Plot==
In a twist departing from the standard superhero formula, White Dragon is a narcissist, forever worried about her good looks, even as shes fighting with Feather, a blind assassin nicknamed "Chicken Feathers" because of his propensity for using chicken feathers as his calling card.

Chicken Feathers is first challenged by White Dragon, an elderly woman with proficient skills in martial arts to almost match his, but not quite enough. Thinking that she has been fatally wounded, White Dragon transfers her kung-fu knowledge into empty shell Black Phoenix, turning the young woman into the prettiest martial arts expert around. The downside to having these extraordinary powers, which she does not fully understand, is bad acne, which she manages to prevent only by doing "noble" deeds like robbing from the rich and giving to the poor.

Phoenix is reluctant to take on her new role at first, but soon becomes interested in tracking down Chicken Feathers when she learns that her love interest, Second Prince Tian Yang, might become his next target. Using her flute-playing as bait, White Dragon finally faces Chicken Feathers in an attempt to defeat him before he can carry out the assassination. Chicken Feathers proves to be too good for the new White Dragon, however. When White Dragon tries to exploit his "weak points," she ends up injured(breaking her leg while trying to kick Feathers in the nuts) and dependent on Feathers, who does nurse her back to health after their heated battle.  While Chicken Feathers plays nurse, White Dragon seeks to find his true weak point to stop him once and for all. What she finds instead is that Chicken Feathers has fallen in love with her, and that he is growing on her as well.

After a while her leg healed and Chicken Feathers found a letter for Second Prince Tian Yang and had it read by the towns doctor. This letter made Chicken Feathers thinking that Black Phoenix already has a boyfriend, namely the Second Prince Tian Yang. Chicken Feathers confronted Phoenix Black with this and they ended up struggling. Accidentally the girl stabbed her flute into the back of Chicken Feathers, which gave him a moment of sight. She ran away.

The Third Prince, brother of the Second Prince, apparently hired Chicken Feathers to kill his brother, this was revealed at the very last scene.

==Cast==
*Cecilia Cheung as Black Phoenix / White Dragon Jr.
*Francis Ng as Chicken Feathers
**Xu Chen Junlei as young Chicken Feathers
*Andy On as Second Prince Tian Yang
*Patrick Tang as Gene
*Kitty Yuen as Tweetie
*Benz Hui as Deer Tail
*Suet Nei as Auntie / White Dragon Sr.
*Liu Lei as First Prince Tian Sheng
*Huang Xiaoyu as Linda
*Dang Chi-fung as Principal Wong
*Shi Zhangjin as Eagle
*Xia Taili as Emperor
*Meng Kai as Third Prince Tian Feng
*Zhang Jiajun as court official
*Liu Jingcheng as Uncle Black
*Wen Wen as Mandy
*Hu Danfeng as Daisy
*Bi Yuanbiao as Eunuch Chan
*Shen Zunying as Lindas schoolmate
*Hu Xiaolin as Lindas schoolmate
*Wang Yiqiu as Lindas schoolmate
*Huang Di as Lindas schoolmate
*Ding Xiaolong as imperial guard
*Zhu Mingming as imperial guard
*Li Suqin as imperial guard
*Zhang Dengke as imperial guard
*Dai Tian as imperial guard

==External links==
* 
* 

 

 
 
 
 
 
 