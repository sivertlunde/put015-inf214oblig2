Nomads (1986 film)
 
{{Infobox Film
| name           = Nomads
| image          = Nomadsposter.jpg
| caption        = Theatrical release poster
| director       = John McTiernan
| producer       = Thomas Coleman Michael Rosenblatt Cassian Elwes Elliott Kastner George Pappas
| writer         = John McTiernan
| starring = {{Plainlist|
* Lesley-Anne Down
* Pierce Brosnan
* Anna Maria Monticelli
}}
| music          = Bill Conti
| cinematography = Stephen Ramsey
| editing        = Michael John Bateman Atlantic Releasing Corporation
| released       = March 7, 1986
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2,278,264 (USA) 
| preceded_by    = 
| followed_by    = 
}} 1986 horror horror and thriller film which was written and directed by John McTiernan and stars Pierce Brosnan and Lesley-Anne Down.

The story involves a French anthropologist who is an expert on nomads. He stumbles across a group of urban nomads who turn out to be more than he expected.

==Cast==

*Pierce Brosnan as Jean Charles Pommier
*Lesley-Anne Down as Flax
*Anna Maria Monticelli as Niki
*Adam Ant as Number One
*Mary Woronov as Dancing Mary
*Nina Foch as the real estate sales lady

== Plot ==

=== The Beginning ===

The movie begins with the violent and painful death of its protagonist, French sociologist Jean-Charles Pommier (Pierce Brosnan). The moment he dies in the Emergency Room of a Los Angeles city hospital the physician treating him, Dr. Eileen Flax (Lesley-Anne Down), becomes possessed with his memories.

Dr. Flax relives the last week of Pommiers life until the moment of his death.

=== Pommiers Story ===

After travelling abroad and studying the spiritual beliefs and religious practices of primitive peoples, Pommier finally settles down with his patient wife Niki (Anna Maria Monticelli) in Los Angeles to teach at UCLA.

His home in the suburbs is vandalized one night by a gang of street punks who travel about in a black van. They are very interested in his house and he finds that they have built a macabre shrine in his garage to a murderer who recently killed two girls who lived in the house. He then starts to study them because they are an urban nomad culture that is strikingly similar to the ones he has studied.

He begins to observe them, following them around and covertly taking their pictures. He develops the pictures and is puzzled to find that they do not show up in them.

Then he begins to realize that they are actually the Einwetok, demonic Inuit trickster spirits that take human form, commit acts of violence and mischief, and who are attracted to places of violence and death. Now that they are aware of him, they plan to claim his soul to keep their existence a secret.

=== The End ===

Dr. Flax wakes in the bedroom of Pommiers house in the arms of his wife. They try to flee the city to escape the nomads but the street starts to fill with an army of leather-clad bikers and punks. They storm the house, forcing the women to flee to the attic. One of the nomads, Dancing Mary (Mary Woronov), breaks into the attic but leaves after scaring them.

Much later, the nomads have left the house and the ladies leave the attic to find the house a shambles. Packing bags, they flee the city.

The next day, as they are driving down a back road, a leather-clad man on a motorcycle rides around them. Flax warns Niki that whatever she sees, she should not stop. As they drive by, they are horrified to see that it is Pommier, now one of the nomads.

==Critical reception==
To date the film has earned a negative (13%) rating on movie review aggregate Rotten Tomatoes. {{cite web |url=http://www.rottentomatoes.com/m/nomads/
|title=Nomads Movie Reviews, Pictures |accessdate=2010-11-18 |publisher= Rotten Tomatoes |date= 2010-11-18 }} 

Jay Scott of The Globe and Mail described Nomads as "a breathlessly unself-conscious film (there is none of the self-congratulatory stylization of Blood Simple), the tone alternates maniacally between scaring the audience and making it giggle. Until the end. And then, via one of the funniest, cleverest and most unexpected conclusions to any movie in history, Nomads comes off the fence it has been sitting on with a bravura jump." Scott credited director John McTiernan, noting that "he has brought to his project a staggeringly resourceful technique. The sharply unpredictable editing, the hypnotic use of slow motion and rack focus (thats when the background and foreground reverse in clarity), the ominous rock music - everything adds up to a debut of singular confidence, full of fun and creepiness."   

In his memoir, Total Recall, Arnold Schwarzenegger stated that he was so impressed by the films tense atmosphere made with a low budget that he hired John McTiernan to direct Predator. 

==See also==
*List of ghost films

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 