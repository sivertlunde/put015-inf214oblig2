Jane Got a Gun
 
{{Infobox film
| name           = Jane Got a Gun
| image          = 
| caption        =  Gavin OConnor
| producer       = Chris Coen Terry Dougas Aleen Keshishian Scott LaStaiti Natalie Portman Regency Boies Zack Schiller Scott Steindorff     
| writer         = Brian Duffield Anthony Tambakis
| starring       = Natalie Portman Joel Edgerton Ewan McGregor Rodrigo Santoro
| music          = Lisa Gerrard Marcello De Francisci
| cinematography = Mandy Walker
| editing        = Alan Cody
| studio         = Handsomecharlie Films Scott Pictures Boise Schiller Film Group Straight Up Films
| distributor    = The Weinstein Company Relativity Media
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $50 million 
| gross          = 
}}
 Gavin OConnor and written by Brian Duffield. The film stars Natalie Portman as Jane Hammond, Ewan McGregor as John Bishop, Joel Edgerton as Dan Frost, and Rodrigo Santoro as Fitchum. The film is produced by Handsomecharlie Films, Scott Pictures, Boise Schiller Film Group, and Straight Up Films. It was shot in Santa Fe, New Mexico. The film is scheduled for a September 4, 2015 release. 

== Plot ==
Jane Hammond (Natalie Portman) has built a new life with her husband Bill "Ham" Hammond (Noah Emmerich) after being tormented by the Bishop Boys gang. She finds herself in the gang’s cross-hairs once again when Ham stumbles home riddled with bullets after dueling with the Boys and their relentless leader, Colin (Ewan McGregor). With the vengeful crew hot on Hams trail, Jane has nowhere to turn but to her former fiancé Dan Frost (Joel Edgerton) for help in defending her family against certain death. Haunted by old memories, Jane’s past meets the present in a heart-stopping battle for survival. 

== Cast ==
* Natalie Portman as Jane Hammond
* Joel Edgerton as Dan Frost
* Ewan McGregor as John Bishop
* Rodrigo Santoro as Fitchum
* Noah Emmerich as Bill Hammond
* Boyd Holbrook as Vic
* Alex Manette as Buck
* James Burnett as Cunny Charlie
* Sam Quinn as Slow Jeremiah

== Production == Gavin OConnor American Hustle.  On May 6, Ewan McGregor was announced to take over the role of John Bishop from Cooper.  On June 3, Boyd Holbrook was announced to be playing the younger brother of John Bishop. 

== Distribution and release ==
Jane Got a Gun will be distributed in the U.S by The Weinstein Company and Relativity Media.  The film was set to be released on 29 August 2014,  which studio cancelled on April 10, 2014.  On April 24, the studio set a February 20, 2015 release date for the film, which was later moved back to September 4.      

==Promotion==
In January 2015 the first imagery from the film was released online. 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 