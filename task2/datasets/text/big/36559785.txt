Adhey Kangal
{{Infobox film
| name           = Adhey Kangal
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = A. C. Tirulokchandar
| producer       = A.V. Meiyappan
| writer         = T. N. Balu (dialog)
| screenplay     = 
| story          = A. C. Tirulokchandar
| based on       =  
| narrator       = 
| starring       =  
| music          =   
| cinematography = S. Maruthi Rao
| editing        = R.G. Gope
| studio         =  
| distributor    = 
| released       = 1967  
| runtime        = 180 minutes India
| Tamil
| budget         = 
| gross          = 
}}
 Ravichandran and Kanchana in the lead roles.  The film was remade in Telugu Simultaneously as "Ave Kallu".

==Plot==
A woman gets ready to leave for a get together and finds her husband dead in their room. In no time, a masked man tries to kill her too . His attempt gets failed and he escapes. The woman is now in a state of shock and mentally paralysed. A murder case is registered and investigation takes place. Susi (Kanchana (actress)|Kanchana) a young college girl comes to her home for vacation along with her friends. She lives with younger paternal uncles Kamalanathan and Vimalanathan who are the younger brothers of the victim and her aunt who was supposed to be the victim in the hands of the murderer. Susi meets Baskar (Ravichandran (actor)|Ravichandran), a singer and both of them are attracted to each other at first sight.

Series of murders take place at Susis house with every time a smoking cigar bit being left by the murder intentionally. Police suspect visiting doctor, an old Siddha doctor who is like a brother to Kamalanathans family, butler etc. Since Susis aunt is the only eye witness of the murderer her life is in danger. In spite of tight protection she is killed by the murderer. The murderer frequently calls Susi and threatens that her time is up and he is nearing her to kill her. Susi is very frustrated by the incidents at her home and the threatening phone call.  Baskar hears her problem and promises to help her. Baskar start his investigation from Vimalanathan who is a drunkard. Then he follows Kamalnathan when he does something suspicious. Baskar follows him to a house which look like a haunted house and a woman who wanders like a ghost. She is actually lover of Kamalanathan who is rescued some years back by him when she tried to commit suicide. Kamalnathan keeps quiet as he wants Susi to get married first and then only marry his lover. Baskar sends everybody out of Susis house for a night and waits for anything to happen. As expected black masked man comes to Susis room to kill Susi. Baskar and the masked murderer fight and Baskar tries to uncover his face. In the attempt Baskar manages to grab the mask of the man and see the eyes alone of the man. The murderer escapes from him. After a while Baskar opens the main door of the house only to find Vimalanathan dead at the doorsteps.

Kamalanathan and Susi plan to vacate the house after celebrating the birthday party of Susi. On the day of the party a gunshot sound is heard and Susis birthday cake is shot on the missed target by masked man. Baskar chases the masked man but could not find him. He sees the old siddha doctor injured at a place who tells him that masked man attacked him and ran away. Everybody at the home does not know why the murders are happening and who kills every member of their family. Siddha doctor urges Kamalnathan to tell about his family which might help to find out who is the murderer. Kamalnathan tells that his father had an illegal affair with a woman and they had a son. But they are all dead in a fire accident fifteen years back. Actually Kamalnathans elder brother (who is first killed by the murderer) had set fire to kill the woman and her ten-year-old son. Baskar doubts what if the son had not died in the fire and turned out to be the murderer killing the members of the family to seek revenge. Also Baskar screams that he found who is the murderer as he can remember the eyes which is like fire which can kill as many as possible. He has seen the Same Eyes in that house itself and seeing the Same Eyes each and every second. He comes to a conclusion that the murderer is present in the hall where all are gathered. Hence he places the black mask in face of all men in the hall like family doctor, Kamalnathan and the old Siddha doctor to verify whose eyes matches with the eyes he had seen.

Who is the murderer among them? Is Baskars assumption about the murderers motivation is correct forms the climax of the story.

==Cast and crew== Ravichandran as Baskar Kanchana as Susila
* S. A. Ashokan as Kamalanathan
* Nagesh
* Madhavi Krishnan as Julie
* S.V.Ramdass as Vimalanathan
* K. Balaji as Doctor

The movie was produced by A. V. Meiyappan under his home banner and the story was scripted by Tirulokachander himself.    Cinematography was handled by S. Maruthi Rao. 

==Box-office==
The film was a major box-office success upon release.    

==References==
 

==External links==
*  

 

 

 
 
 
 
 
 
 
 
 