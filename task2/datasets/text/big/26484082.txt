The Prophet of Hunger
 
{{Infobox film
| name           = The Prophet of Hunger
| image          = Prophet of Hunger 1970 film.jpg
| caption        = Film poster
| director       = Maurice Capovila
| producer       = Odécio Lopes dos Santos
| writer         = Maurice Capovila Fernando Peixoto
| starring       = José Mojica Marins
| music          = Rinaldo Rossi
| cinematography = Jorge Bodanzky
| editing        = Silvio Renoldi
| studio         = Fotograma Filmes
| distributor    = Cinedistri
| released       =  
| runtime        = 93 minutes
| country        = Brazil
| language       = Portuguese
}}

The Prophet of Hunger ( ) is a 1970 Brazilian drama film directed by Maurice Capovila. It was entered into the 20th Berlin International Film Festival.   

==Cast==
* José Mojica Marins - Fakir Ali Khan
* Maurício do Valle - Lion Tamer
* Julia Miranda - Maria
* Sérgio Hingst - Dom José (circus owner)
* Joffre Soares - Priest
* Adauto Santos
* Eládio Brito
* Flávio Império
* Lenoir Bittencourt
* Mário Lima
* Angelo Mataran
* Luiz Abreu
* Jean-Claude Bernardet
* Hamilton de Almeida

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 