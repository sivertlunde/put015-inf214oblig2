Old Cow vs Tender Grass
 

{{Infobox film
| name           = Old Cow vs Tender Grass
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Chi Kai Fok
| producer       = Ng San San	
| writer         = Lim Teck Chi Kai Fok
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Henry Thia Crystal Lin Jack Lim
| music          = 
| cinematography = Wai Yin Chiu
| editing        = 
| studio         = 
| distributor    = Golden Village Pictures
| released       =   
| runtime        = 95 minutes
| country        = Singapore Hokkien Mandarin Chinese
| budget         = 
| gross          = 
}}
Old Cow vs Tender Grass ( ) is a 2010 Singaporean  . 

==Plot==
At 49, Singaporean taxi driver Moo (Henry Thia) is still trying to find a lifelong companion. He encounters the young but eccentric Moon (Crystal Lin), who is always seen with her pet, Bubbles (the dog), and feeding stray animals, after taking her as a passenger, and takes a liking. Meanwhile his best friend and colleague, also a taxi driver (Jack Lim), is also trying to find love and finds success when he meets a dashing beer girl from China with the name of Xiao Hong (Siau Jia Hui).

==Cast==
* Henry Thia as Moo 
* Crystal Lin as Moon 
* Jack Lim as Prince Gao
* Jia Hui Siau as Xiao Hong
* Nathaniel Ho as Nat Lee
* Luis Lim Yong Kun as Wu
* Tony Koh Beng Hoe as taxi driver
* Ix Shen as Traffic Policeman
* Dick Su as Unfilial Son
* Alaric Tay as Colleague

==References==
 

==External links==
 
* 