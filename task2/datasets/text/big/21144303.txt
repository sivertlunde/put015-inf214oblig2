Accused (2005 film)
{{Infobox film
| name           = Anklaget
| image          = Anklaget2005Poster.jpg
| caption        = Film poster
| director       = Jacob Thuesen
| producer       = Thomas Heinesen Kim Magnusson
| writer         = Kim Fupz Aakeson
| screenplay     = 
| story          = 
| based on       =  
| starring       = Troels Lyby Sofie Gråbøl Paw Henriksen Louise Mieritz
| music          = Nikolaj Egelund
| cinematography = Sebastian Blenkov
| editing        = Per K. Kirkegaard
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Denmark
| language       = Danish
| budget         = 
| gross          = 
}}

Anklaget (Accused) is a 2005 Danish drama film about a couple, Henrik and Nina Christofferson, whose happiness was shattered when their 14-year old daughter Stine accused her father of molesting her.

==Cast==
*Troels Lyby - Henrik 
*Sofie Gråbøl - Nina 
*Paw Henriksen -  Pede 
*Louise Mieritz -  Pernille 
*Kirstine Rosenkrands Mikkelsen -  Stine
*Søren Malling - Forsvarer (sollicitor) 

==External links==
* 

 
 
 
 
 

 