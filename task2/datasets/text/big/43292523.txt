The Snowbird
{{infobox film
| title          = The Snowbird
| image          =
| imagesize      =
| caption        =
| director       = Edwin Carewe
| producer       = B. A. Rolfe
| writer         = Mary Rider (scenario)
| starring       = Mabel Taliaferro Edwin Carewe James Cruze
| music          =
| cinematography =
| editing        =
| distributor    = Metro Pictures
| released       = May 1, 1916
| runtime        = 6 reels
| country        = USA
| language       = Silent film..(English intertitles)
}}
The Snowbird is an existing  1916 silent film drama directed by Edwin Carewe and starring Mabel Taliaferro. B. A. Rolfe produced while Metro Pictures distributed.  

==Cast==
*Mabel Taliaferro - Lois Wheeler
*Edwin Carewe - Jean Corteau
*James Cruze - Bruce Mitchell
*Warren Cook - John Wheeler
*Arthur Evers - Pierre
*Walter Hitchcock - Michael Flynn
*Kitty Stevens - Zoe
*John Melody - Magistrate Le Blanc

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 