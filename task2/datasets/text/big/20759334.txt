Wrong Side Up
{{Infobox film
| name           = Wrong Side Up
| image          = 
| caption        = 
| alt            = 
| director       = Petr Zelenka
| producer       = 
| writer         = Petr Zelenka
| starring       = Ivan Trojan Zuzana Šulajová Miroslav Krobot Nina Divíšková Jiří Bartoška
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes 
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
}}

Wrong Side Up ( ) is a 2005 Czech comedy-drama film written and directed by Petr Zelenka. It is an adaptation of Zelenkas play Tales of Common Insanity. It is a tale of people showing their internal loneliness by their choices in life. It was entered into the 27th Moscow International Film Festival.   

==Plot==
A former aircrew member (Petr) is no longer flying, but works in an aviation-related dead-end job, loading boxes at an air-cargo company.  He spends his working hours dreaming of re-winning the hand of his former fianceé (Jana), who has moved on to another man whose prospects seem better.  He spends his off hours surreptitiously observing a female neighbor.  

His parents also face personal problems: his mother has involved herself in causes promoting world peace but ignores her collapsing family; his father is fighting a midlife crisis by trying to promote an extramarital affair with a colleague.

==Cast==
*Ivan Trojan as Petr
*Zuzana Šulajová as Jana
*Miroslav Krobot as Father
*Nina Divíšková as Mother
*Jiří Bartoška as Jiří

==Awards==
*2006, Bronza Rosa Camuna - Best director, Bergamo Film Meeting
*2006, Czech Lions: Best sound
*2006, Czech Lions: Best supporting actor

==References==
 

==External links==
*  

 
 
 
 
 
 
 