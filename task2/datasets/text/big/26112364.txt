Sukhmani: Hope for Life
{{Infobox film
| name           = Sukhmani
| image          = Sukhmani_–_Hope_for_Life.jpg
| caption        =
| director       = Manjeet Maan
| producer       = B4U Productions Gurik Maan Sai Lok Sangeet
| writer         = Gurdas Maan
| starring       = Gurdas Maan Juhi Chawla
| music          = Anand-Milind
| music release  =
| country        = India
| cinematography =
| genre          = Drama
| distributor    = B4U Productions
| released       =  
| runtime        = 162 mins Punjabi
| budget         =
| gross          =
}}

Sukhmani – Hope for Life - is a Punjabi film Starring Gurdas Maan, Juhi Chawla, Divya Dutta and Bhagwant Mann. The film directed by Manjeet Maan, wife of Gurdas Maan and produced by Gurdas Maan and their son Gurikk Maan. The film has been made by the Maans own production company - Sai Lok Sangeet, with screenplay by Irshad Kamil, music by Jaidev Kumar and lyrics written by Gurdas Maan. Music to the film was released on 6 January 2010 and includes vocals from Gurdas Maan, Shreya Ghoshal and for the first time ever in her career - Juhi Chawla has sung alongside Gurdas Maan in a duet for the song "Preeto - Track 3" and "Nanhi Si Gudiya - Track 2".

The film is distributed by the UK BOX OFFICE and B4U Productions who really helped making the film.

==Synopsis==
Sukhmani- Hope For Life is a story based on the Journey of Major Kuldeep Singh, played by Gurdas Maan, a decorated officer of the Para Battalion, who overcomes personal trauma and social indignity to uphold the morals of the army and the honour of a woman rejected by society and family, while keeping alive the memory of his beloved daughter Sukhmani.

On that fateful day when the beautiful valley of Jammu & Kashmir is bloodied by the gruesome killings of his devoted wife and treasured daughter at the hands terrorists, Major. Kuldeep Singh’s life is thrown into disarray. To maintain his sanity and avoid falling in to a black hole of depression, he returns to his duty, fighting the terrorists with renewed vigor and the need to avenge his family. When for a second time he fails to save the life of an innocent girl who becomes a victim of the terrorism, he becomes a broken man. However, a soldier’s war is never over and when duty calls again, he goes out to repatriate innocent civilians from the Wagah Border. It is after meeting Reshma, he decides that protecting her from the social stigma of society as well as from the evil intentions of his own comrade has becomes his goal in life. He faces dishonour and social exclusion, but unwaveringly fights back for his rights, the rights of a woman dispelled by society and the future of a little girl who has nothing to do with terrorism and the hatred in this world.

Sukhmani shows us that both good and evil lie within each of us, however when faced with exceptional circumstances, each human acts differently. It shows us that sometimes we have to make choices that are not always accepted by society. Sukhmani shows us that there is hope for life...

==Cast==
* Gurdas Maan ..as Major Kuldeep Singh
* Juhi Chawla ..as Majors wife
* Divya Dutta
* Shammi ..Guest Appearance
* Roshan Abbas
* Anita Kanwar Bhagwant Maan ..as a military man

==Songs==
*1. Fauji - Gurdas Maan, Krishna & Arvinder Singh
*2. Nanhi Si Gudiya -Gurdas Maan & Juhi Chawla
*3. Preeto - Gurdas Maan & Juhi Chawla
*4. Yaad - Gurdas Maan (Alaap by Simerjit Kumar)
*5. Rabba - Shreya Ghoshal
*6. Ramji - Gurdas Maan
*7. friend -  

==Sai Lok Sangeet Pvt. Ltd.==
Sai Lok Sangeet Pvt. Ltd., a company incorporated in the year 2007 under the aegis of two most respected and loved people of Punjabi Cinema, Mr. Gurdas Maan and Mrs. Manjeet Maan.

The sole aim of this incorporation was to create meaningful and value for money films for a worldwide audience. The first step in this direction was taken this year when the company embarked on its maiden film production venture Sukhmani – Hope for Life, an army based movie revolving around human bonds and emotions.
 National Award winning films:
 National Award for the Best Punjabi Feature Film. Best Playback Singer and the film did reasonably well commercially. Special Jurys Award for its lead actor Gurdas Maan for the portrayal of his character in the film. National Awards as mentioned below:
*Best Art Direction Best Costume Designer
*Best Punjabi Feature Film Best Playback Singer
The other awards which this film also won were:
*Berlin Asia Film Festival - Gurdas Maan won an award as the Best Actor
*The movie also entered the 79th Academy Awards

Sai Lok Sangeet Pvt. Ltd. promises to deliver the same meaningful and quality movies to all Punjabi Cinema lovers.

== Awards ==

===PTC Punjabi Film Awards 2011===
At first PTC Punjabi Film Awards Sukhmani won various awards under different categories . 
* Best Director Award: Manjeet Mann
* Best Actress Award: Divya Dutta
* Best Editing Award: Omkarnath Bhakri
* Best Story Award: Suraj Sanim/Manoj Punj
* Critics Best Actor Award: Gurdas Mann
* Critics Best Film Award: Sukhmani – Hope for Life

== References ==
 

==Videos==
* 
*  - Vocals: Gurdas Maan & Juhi Chawla
*  - Vocals: Shreya Ghoshal

==External links==
*  
* 
* 
* 
*Divya Dutta
*Bhagwant Mann
*  

 
 