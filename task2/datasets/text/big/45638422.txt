China Versus Allied Powers
{{Infobox film
| name           = China Versus Allied Powers
| director       = Georges Méliès
| studio         = Star Film Company
| released       =  
| runtime        = 20 meters Approx. 1 minute 
| country        = France
| language       = Silent
}}
 short silent silent satirical film directed by Georges Méliès. It was released by Mélièss Star Film Company and is numbered 327 in its catalogues. 

==Plot==
According to a surviving catalogue description of the film:

 

==Production==
The film, probably made in the late summer of 1900, satirizes the allied coalition (Germany, Austria-Hungary, the United States, France, Italy, Japan, the United Kingdom, and Russia) that was then staging military interventions in China, shortly before the Boxer Rebellion. (The rebellion itself is not mentioned in the surviving description of the film, and probably postdates it.)    
 reconstructed newsreels" underdog (as The Dreyfus Affair), reveals a pro-China stance in this film; this was a highly unusual position among European filmmakers, and indeed no other known film made at the time about the Chinese conflict portrays China sympathetically. 

The film is currently presumed lost film|lost. 

==Related films== Albert E. dissolve technique used in the film from Méliès, who had pioneered it in his successful 1899 film Cinderella (1899 film)|Cinderella.  The magician in the Blackton version, having produced flags and personifications of the nations involved, allows his Allied soldiers to attack a nonmilitary "Chinese" character (actually a Vitagraph clerk, Morris Brenner) for a few moments before replacing the conflict with a huge American flag and a patriotic tableau.   

==References==
 

==External links==
* 

 
 
 