Livid (film)
{{Infobox film
| name        = Livid
| image       = Livid Poster.jpg
| caption     = French theatrical poster
| director    = Alexandre Bustillo Julien Maury
| producer    = Verane Frediani Franck Ribiere
| writer      = Alexandre Bustillo Julien Maury Catherine Jacob Marie-Claude Pietragalla Chloe Coulloud Chloe Marcq
| distributor = 
| cinematography = Laurent Bares
| editing = Baxter
| music = Raphaël Gesqua
| released    =  
| country     = France
| language    = French
| runtime     = 93 minutes
| budget      = 
| gross       = 
}}
Livid ( ) is a 2011 French supernatural horror film directed and written by Julien Maury and Alexandre Bustillo. It is their follow-up to the horror film Inside (2007 film)|Inside.

==Plot==
 stuffed animals, and finds Mrs. Wilson at the bed of the woman to whom she is attending, Mrs. Deborah Jessel. Mrs. Wilson informs her that Jessel was once a prominent ballet teacher, but is now bedridden and in a coma. Legend has it that Mrs. Jessel has a treasure of gold and jewels located somewhere on the property. After her first day on the job, Lucy meets her boyfriend, Will, and tells him of the treasure at Jessels house. Lucy, Will, and Wills brother Ben all decide to hunt for the treasure.

The three enter into the basement of the house through a window. After searching and finding a room filled with stuffed animals creepily gathered around a tea table, they find a single locked door. Lucy suspects that the key that was hanging around Jessels neck is the key to this door. Inside the room, they find a white sheet covering what looks like a stuffed girl. Lucy suggests that this is the corpse of Jessels mute daughter Anna, still dressed in a ballerina outfit. Lucy twists the key on the pedestal the corpse is standing on, and the corpse starts spinning slowly with music playing, like a ballerina on a music box.

They suddenly hear noises from the room upstairs and start to run out of the house, but find the basement window they entered from mysteriously covered by iron bars. Will tries to break a window open to escape, but the three of them get separated. Ben finds himself from an operating room with no door, with no idea how he got there, and is killed by girls wearing veils and ballet outfits appearing out of nowhere.

Lucy sees Mrs. Jessel sitting at what was once Annas tea party table. There is a flashback, showing that when Mrs Jessel was teaching ballet, she was a very strict ballet teacher. When one girl leaves the ballet class, Jessel finds her dead in Annas room. Anna had been drinking her blood. It is revealed that the Jessels are vampires.

Will, still trying to open a window, is then attacked by Ben, who tries to bite him. He manages to stab Ben with a pair of scissors, but is attacked and killed by Mrs. Jessel. Lucy discovers that Anna is not in fact dead. Anna attempts to drink Lucys blood but Lucy pushes her off. Suddenly Mrs. Wilson appears, punches and sedates Lucy.

With Wilson looking on, Mrs. Jessel implants the pupae of a moth into the sedated Lucy and into the neck of Anna—a ritual to exchange the souls of the two. When Lucy comes to, Jessel believes that she has put Annas soul into Lucys body, as the color of the eyes has shifted. Jessel orders Lucy to dance. When she doesnt do as Jessel says, Wilson comes over to punish her, but Anna stabs her with scissors. She then stabs Jessel with the scissors, and both women fall to the floor. Jessel, though bleeding heavily, manages to get up and try to bite Lucy. Anna whips her mother until she releases Lucy, and the two girls throw Jessel off the third floor balcony.

The next morning, Anna and Lucy escape the mansion and walk to the cliffs along the ocean side, holding hands. They unclasp their hands and Anna leans off a cliff. She does not go down into the water, rather, she flies into the sky as if to demonstrate a newfound peace. Her vampiric burn scars disappear as Lucy watches her float away.

==Production==
 The film went into production in 2009.    The film was initially intended to be Bustillo and Maurys English-language debut and shot in the United Kingdom but they moved to a lower-budget French production after they found that they were losing creative control over their story.  A promo was shown at the American Film Market. The film was described as being much more of a fantasy film than their previous film; "If Inside was meant to play as horror taken from fact then Livid plays as horror taken from fairy tale, the Grimm kind with all the bloody bits left in."
 

==Cast==
*Chloé Coulloud&nbsp;— Lucie
*Jérémy Kapone&nbsp;— Ben Catherine Jacob&nbsp;— Wilson
*Félix Moati&nbsp;— William
*Marie-Claude Pietragalla&nbsp;— Jessel
*Chloé Marcq&nbsp;— Anna
*Loïc Berthezene&nbsp;— Lucies father
*Béatrice Dalle&nbsp;— Lucies mother

==Release==
Livid was shown at the Toronto International Film Festival on September 11, 2011.  On  October 12, the film was shown at the Sitges Film Festival. Marc Thiébault won the award for Best Production Design at the festival for his work in Livid.  The film opened in France on December 7, 2011. 

==Reception==
Variety (magazine)|Variety compared the film unfavorably to the directors previous film Inside (2007 film)|Inside, stating that "the pic is so eager to go over the top that, in the end, it doesnt make much sense."  Screen Daily gave a positive review of the film, comparing it to the works of Guillermo del Toro and Dario Argento.  The Hollywood Reporter gave the film a negative review, stating that it was neither "gory nor eerie enough to function as veritable horror fare".   

Horror magazine Fangoria gave the film a positive review of three out of four, stating that the films "final moments are a bit stretched and its end is sillier than probably intended. It’s imperfect, yes, but entirely worth loving; likely and hopefully appreciated and adored over time." 

==Remake==
An English-language remake for the film was written by David Birke   and was to be directed by Nicholas McCarthy (director), but was not made. {{cite web|title=Permanent Midnight: NBD, Its Just Naya Rivera, Ashley Rickards, and Some Good Old-Fashioned Satanism
|url= http://www.complex.com/pop-culture/2014/08/interview-nicholas-mccarthy-at-the-devils-door |publisher=Complex.com|accessdate=11 February 2015}} 

==References==
 

==External links==
*  

 
 
 
 
 
 
 