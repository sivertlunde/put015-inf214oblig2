Attack the Gas Station 2
{{Infobox film
| name           = Attack the Gas Station 2
| image          = Attack the Gas Station 2-poster.jpg
| caption        = Promotional poster for Attack the Gas Station 2
| film name =     2
 | hanja          =     2
 | rr             = Juyuso seubgyuksageun 2 
 | mr             = Chuyuso sŭpkyŏksakŏn 2}}
| director       = Kim Sang-jin
| producer       = Kang Woo-suk 
| writer         = Baek Sang-yeol 
| starring       = Ji Hyun-woo Jo Han-sun Park Yeong-gyu
| music          =  
| cinematography = Kim Dong-cheon  
| editing        = Go Im-pyo
| studio         = Cinema Service
| distributor    = CJ Entertainment
| released       =  
| runtime        = 107 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Attack the Gas Station 2 ( ) is a 2010 South Korean action comedy film, starring Ji Hyun-woo and Jo Han-sun.  It is the sequel to Attack the Gas Station (1999), and was released on January 21, 2010.  

==Plot==
Its been 10 years since Mr. Parks gas station was attacked by motorcycle gangs. To get his revenge, Park hires a quartet of dodgy boys: a lethal puncher, a footballer with a killer high kick, a potbellied wrestler, and a video game addict who mastered the art of bluffing. But these employees turn out to be more dangerous when they demand their overdue salaries. 

==Cast==
*Ji Hyun-woo - One Punch
*Jo Han-sun - High Kick
*Moon Won-ju - Body-twist
*Jung Jae-hoon - Yaburi
*Baek Jeong-min - Jjangdol
*Park Yeong-gyu - gas station owner
*Park Sang-myun - Mang-chi
*Lee Hyeon-ji - Myeong-rang, female student
*Kwon Yong-woon - narrator manager
*Tak Teu-in - Beol-goo
*Lee Mi-do - Lee Mi-do
*Na Hae-ri - Na Hae-ri
*Lee Se-mi - Lee Se-mi
*Jo Deok-hyun - Jo-jung Daily reporter
*Ko In-beom - revival owner
*Lee Myeong-ho - Pal-bong
*Kim Yoon-sung - motorcycle gang leader
*Hwang Sang-kyung - motorcycle gang member 1
*Kim Jung-woon - motorcycle gang member 2
*Kim Seung-wan - prison officer
*Park Ji-yeon - woman thinking about the past 2
*Kim Do-yeon - woman thinking about the past 3
*Park Jin-young - One Punchs father
*Kim Dae-ho - chicken restaurant owner
*Jang Hang-joon - director
*Lee Mi-so - female student getting statutory rape
*Kim Sang-jin - soccer team coach
*Kim Su-ro - Ha Re-yi (cameo)
*Kim Sun-a - herself (cameo)

==References==
 

==External links==
* http://www.oilattack.co.kr/
*  
*  

 
 
 
 
 


 