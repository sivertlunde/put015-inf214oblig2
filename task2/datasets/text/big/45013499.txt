Lefteris Dimakopoulos
{{Infobox film
| name           = Lefteris Dimakopoulos
| image          =
| caption        =
| director       = Periklis Hoursoglou
| producer       =
| writer         = Periklis Hoursoglous
| starring       = Nikos Georgakis Maria Skoula  Nikos Orphanos
| music          = 
| cinematography = Stamatis Giannoulis 
| editing        = Takis Yannopoulos 
| distributor    =
| released       = September 1993
| runtime        = 
| country        = Greece
| language       = Greek
| gross          =
}}
Lefteris Dimakopoulos is a Greek film released in 1993, directed by Periklis Hoursoglou. The film stars Nikos Georgakis, Maria Skoula and Nikos Orphanos. The film won four awards in Greek State Film Awards, as well the best film award in Greek Film Critics Association Awards.

==Plot== Polytechnic University of Athens. In Athens, he lives with his girlfriend Dimitra, despite the opposition of his uncle who pay tuitions of his studies. Nevertheless, the next years, he feels that his relation with Dimitra becomes an obstacle to his ambitions. Finally he breaks up with his girlfriend. After many years he has become a successful engineer, but a visit of his best friend reminds him in the lost love with Dimitra. Then, he perceives that he hadnt won the happiness.    

==Cast==
*Nikos Georgakis as Lefteris
*Maria Skoula as Dimitra
*Nikos Orphanos as Panayiotis

==Awards==
{| class="wikitable"
|+ List of awards and nominations  
! Award !! Category !! Recipients and nominees !! Result
|- Greek State 1993 Greek Best Film||Periklis Hoursoglou|| 
|- Best Cinematography||Stamatis Giannoulis|| 
|- Best Production Anastasia Arseni|| 
|- Best First Periklis Hoursoglou|| 
|- Greek Film Best Film||Periklis Hoursoglou|| 
|}

==References==
 

==External links==
* 

 
 
 

 