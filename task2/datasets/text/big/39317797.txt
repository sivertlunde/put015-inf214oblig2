Vidiyum Munn
{{Infobox film
| name           = Vidiyum Munn
| image          = VidiyumMunn.jpg
| alt            =  
| caption        = theatrical poster
| director       = Balaji K. Kumar
| producer       = Javed Khayum
| starring       = Pooja Umashankar Malavika Manikuttan Vinod Kishan
| music          = Girishh Gopalakrishnan
| cinematography = Sivakumar Vijayan
| editing        = Sathyaraj
| studio         = 
| distributor    = PVR Pictures
| released       = November 29, 2013
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}}

Vidiyum Munn ( . The film stars Pooja Umashankar as Rekha, a prostitute trying to rescue a 12 year old girl, played by Malavika Manikuttan, from the clutches of prostitution. The music was composed by Girishh Gopalakrishnan. Vidiyum Munn was the fifth Indian film to be released in Auro 3D 11.1 surround sound system.  The film was released by PVR Pictures on November 29, 2013 to Critical acclaim but below average grosser at the box office.

== Synopsis ==
The plot unfolds as a taut thriller that examines the characters vulnerability and strengths. The subplot, however, explores a relationship between a woman & a girl; Rekha, the protagonist, is a sex worker who tries to rescue Nandhini, a twelve-year-old girl, from the clutches of prostitution. The journey takes us through the brutality and the hypocrisy that surrounds us, which we deliberately choose to ignore—broken homes, abuse, unfulfilled dreams, manipulation, blackmail, threat, hopelessness and paranoia. Its a dog-eat-dog world where nothing is as simple as it appears.

Rekha, an aging prostitute, is seen selling herself to old and low level people in order to eke out a living. During one of her sessions, she meets with her old pimp Singaram, who informs her that he needs her help to bring in a virgin, prepubescent girl for one of his major clients, Periayyah, an unnamed, wealthy liquor baron of India.  She refuses in the beginning and accepts reluctantly, after he agrees to split the profts with her.

Singaram takes Rekha to meet with her old pimp and ex-lover Dorasingam, who is now bedridden and furious at Rekha for leaving him. However Rekha convinces him to deliver the girl to her, on the condition that the girl be returned the very next day. After picking up the girl Nandini, Rekha travels with Singaram to get the girl ready for the night. Even though she tries to develop a bonding with the girl, Nadini shies away, avoiding to talk to Rekha. Although Rekha does not tell Nadini about what is going to happen to her, she is constantly tormented by her actions and feels responsible for the girl.

Everything goes according to Singarams plan and Rekha accompanies the girl to Periayyahs mansion. Perriayyah takes the girl upstairs and starts to tie her up. As the girl prostests, Perriayyah is seen taking up a razor blade, when suddenly Rekha breaks into the room and a struggle ensues, in which Perriayyah is killed. Rekha and Nandini escape the mansion and go on the run.

Singaram is then visited by the henchmen of Chinnayyah, Periayyahs son. They question her about the girl he sent and threaten him to find them within a day. An intimidated Singaram approaches Lankesh, a rouge detective with an eye for details. Lankesh agrees to help Singaram to track down the girl for a sum. In his search of Rekhas apartment, he discovers a photo of her friend taken in Srirangam, and concludes that the two are headed there.

As this happens, Dorasingams men spot the girl and Rekha on a train. They decide to kidnap her, but their attempts are thwarted and the two escape to Srirangam and meet with Devanayagi, Rekhas old friend.

Lankesh tracks down the house in which Devanayagi lives and how all loose ends tie together is the story.

== Cast ==
* Pooja Umashankar as Rekha
* Malavika Manikuttan as Nandhini
* Vinod Kishan as Chinniah
* Lakshmi Ramakrishnan as Devanayagi
* John Vijay as Lankesh
* R. Amarendran as Singaram
* Muthukumar as Mani
* Ruben as Albert

== Production ==
Javed Khayum listened to the script by Balaji K Kumar and decided to produce the film as the first feature film project of Khayum Studios. PVR Pictures joined as the Release Partner and Vidiyum Munn was the first south Indian feature film to be distributed by PVR Pictures. 

== Soundtrack ==
The music is composed by Girishh Gopalakrishnan. British musician Susheela Raman lent her voice for the song "Penne", marking her debut in Tamil cinema.  The lyrics were also penned by Girishh. The audio was launched on 16 August 2013, at Prasad Studios, under Sonys Saregama Label.

; Tracklist 

 

==Reception==
Vidiyum Munn received positive reviews.  Baradwaj Rangan wrote, "Balaji K. Kumar’s Vidiyum Munn is the most excitingly staged movie since Mysskin’s Onaayum Aattukuttiyum.Rajasekar of Cinemalead wrote "Vidiyum Mun is a perfectly executed riveting thriller which is sure to be in the top ten best films ever produced in Tamil cinema.  IANS gave the film 3 stars out of 5 and wrote, "Vidiyum Munn is a step closer to alternate Tamil cinema which majorly caters to those with some intellect and not to entertainment seekers. Vidiyum Munn stands out as one of the better thrillers of the recent past".    Sify wrote, "Debutant Balaji K Kumar’s Vidiyum Munn is one of the best new generation thrillers in recent times. It is not the usual masala thriller, but something different that grabs you with its taut narration, superb performances, great background score and a riveting climax".    Behindwoods gave it 3.5 stars out of 5 and called it "a work of a debut director which is gripping, hard hitting and even shocking but all the same, well made to give the right kind of viewing experience".  Rediff gave 3.5 stars out of 5 and wrote, "The stark simplicity of the dialogues, the well-etched characters, the intriguing plot, the captivating music, the cinematography, but most of all the brilliant twist at the end makes Director Balaji K Kumar’s Vidiyum Munn a must watch".  The Times of India gave 3.5 stars out of 5 and wrote, "There are some nail-biting moments and an uncomfortable one, and director Balaji Kumar extracts some fine performances from the cast. He is aided by the moody visuals of his cinematographer Sivakumar Vijayan  and an evocative score from composer Girish Gopalakrishnan, and manages to maintain the suspense and keep us engrossed as the past and present unfold. (There) are small lapses that we can willingly excuse in a film that is otherwise terrific".  The New Indian Express wrote, "Vidiyum Munn is a tastefully and a sensitively crafted depiction of the sleazy side of society". 

Critics noticed that the plot drew similarities to the British thriller London to Brighton.   

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 