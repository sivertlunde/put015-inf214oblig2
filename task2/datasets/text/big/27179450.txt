Fast and Loose (1954 film)
{{Infobox film
| name = Fast and Loose
| image = "Fast_and_Loose"_(1954).jpg
| caption = British poster by Eric Pulford Gordon Parry
| producer = Teddy Baird
| writer = {{Plainlist|
* Ben Travers
* A.R. Rawlinson
}}
| play   = Ben Travers
| narrator = 
| starring = {{Plainlist|
* Stanley Holloway
* Kay Kendall
* Brian Reece
}} Philip Green
| cinematography = {{Plainlist|
* Jack Asher
* Reginald H. Morris
}}
| studio         = Ealing Studios
| editing    = Frederick Wilson
| studio      = Group Film Productions Limited
| distributor = General Film Distributors 
| released = February 1954 (UK)
| runtime = 75 minutes
| country = United Kingdom
| language = English
}} Gordon Parry and starring Stanley Holloway, Kay Kendall and Brian Reece. It was based on A Cuckoo in the Nest by Ben Travers, the first of his Aldwych farces.

==Plot==
An unmarried couple are forced to adopt a series of pretexts when they stay at a country inn together with only one spare room. 

==Cast==
* Stanley Holloway - Major George Crabb
* Kay Kendall - Carol Hankin
* Brian Reece - Peter Wickham
* Charles Victor - Lumper
* June Thorburn - Barbara Babsie Wickham
* Reginald Beckwith - Reverend Tripp-Johnson
* Vida Hope - Gladys
* Joan Young - Mrs. Gullett, Inn Manageress
* Fabia Drake - Mrs Crabb
* Dora Bryan - Mary Rawlings, the maid
* Aubrey Mather - Noony
* Toke Townley - Alfred
* Alexander Gauge - Hankin
* Eliot Makeham - Railway porter
* John Warren - Chauffeur

==Critical reception== A Cuckoo in the Nest(1933)."  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 