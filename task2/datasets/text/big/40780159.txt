Easter (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Easter
|image=
| director       = Richard Caliban
| producer       = Will Scheffer Mark V. Olsen Christine K. Walker Ezra Swerdlow
| screenplay     = Will Scheffer Richard Caliban
| based on       =  
| starring       = Jodie Markell Barry Del Sherman Sean Runnette Max Wright
| music          = Jeff Danna
| cinematography = Claudio Rocha
| editing        = Susan Littenberg
| studio         = Anima Sola Productions, Inc.
| released       =   Temecula Valley International Film Festival (premiere)
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $500,000
}}
Easter is an independent film based on the play by the same name.

== Plot ==
Wilma Jodie Markell and Matthew Barry Del Sherman Ransom are a married couple trying to escape their past, but it always catches up with them.

Wilma struggles with indulging in her fantasies, causing her to see Herman Warm Sean Runnette.  She drives into town and stops at a local church where she buys a used wedding dress.  Next she buys candles and religious supplies from an old shopkeeper named Zaddock Pratt Max Wright.  Around this time, Matthew drives by the church to notice its on fire due to arson.  He quickly realizes that his wife is back to her old tricks of burning churches.

Matthew confronts Wilma about the burning church, and she tells him the reason why she burns churches.  She blames him for the death of their child that she miscarried, telling him she hates him.

== Production == Hastings Fire Department in the burning of an actual farmhouse.  The burning of the church was accomplished using visual effects.

==Filming locations== Hastings and Hastings High School.

==External links==
 
*  

 


 