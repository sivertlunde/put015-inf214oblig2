Achilles and the Tortoise (film)
{{Infobox film
| name           = Achilles and the Tortoise
| image          = AchillesandtheTortoise2008.jpg
| caption        = 
| director       = Takeshi Kitano Masayuki Mori
| writer         = Takeshi Kitano
| narrator       =  Beat Takeshi Kanako Higuchi Yurei Yanagi Kumiko Aso
| music          = Yuki Kajiura
| cinematography = Katsumi Yanagishima
| editing        = Takeshi Kitano
| studio         = Bandai Visual Tokyo FM TV Asahi WOWOW
| distributor    = Tokyo Theatres Office Kitano
| released       =  
| runtime        = 119 minutes Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2008 Japanese film written, directed and edited by Takeshi Kitano. The film is the third and final part of Kitanos surrealist autobiographical trilogy, starting with Takeshis and continuing with Glory to the Filmmaker!.
 Achilles and the Tortoise.

==Plot== Eri Tokunaga) and his wifes desertion. He tries to please the art critics, remaining penniless. He is caught up in a fire and almost dies. Losing all his previous works, he is left with a single half-burnt soda can, which he assesses at 200,000 yen and tries to sell. This ends up kicked carelessly away when his wife picks him up from the street. They walk away together, seemingly finally rid of his artistic obsession.

==Cast== Beat Takeshi as Machisu
* Kanako Higuchi as Sachiko
* Yurei Yanagi as Young Machisu
* Kumiko Aso as Young Sachiko
* Akira Nakao as Machisus father
* Mariko Tsutsui as Machisus stepmother
* Ren Osugi as Machisus uncle
* Susumu Terajima as Yakuza pimp Eri Tokunaga as Machisus daughter
* Nao Omori as Art dealer
* Masato Ibu as Art dealer

==Release==
The film premiered in competition at the 65th Venice Film Festival on August 28, 2008. 

==Reception==
Mark Schilling of The Japan Times gave the film 2 out of 5 stars. 

==Further reading==
* {{cite book
| url=https://books.google.com/books?id=mRrpfF6g3g0C&PA10
| title=Directory of World Cinema: Japan
| editor-last=Berra | editor-first=John
| publisher=Intellect Books
| year=2010
| isbn= 9781841503356
| page= 9-10
}}
==References==
 

==External links==
*    
*  

 

 
 
 
 
 
 